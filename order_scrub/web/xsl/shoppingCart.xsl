<?xml version="1.0" encoding="iso-8859-15" ?>
<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External Templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="tabs.xsl"/>
<xsl:import href="errorMessage.xsl"/>
<xsl:import href="altContact.xsl"/>
<xsl:import href="itemsSummary.xsl"/>
<xsl:import href="itemDiv.xsl"/>
<xsl:import href="footer.xsl"/>


<!-- Internal Templates
  addHeader
  addNavLinks
  addActionButtons
-->

<xsl:output method="html" encoding="ISO-8859-15" indent="yes"/>
<!-- Mode flags -->
<xsl:variable name="SCRUB" select="'S'"/>
<xsl:variable name="PENDING" select="'P'"/>
<xsl:variable name="REINSTATE" select="'R'"/>

<!-- Modes -->
<xsl:variable name="SCRUB_MODE" select="boolean(key('searchCriteria', 'sc_mode')/value=$SCRUB)"/>
<xsl:variable name="PENDING_MODE" select="boolean(key('searchCriteria', 'sc_mode')/value=$PENDING)"/>
<xsl:variable name="REINSTATE_MODE" select="boolean(key('searchCriteria', 'sc_mode')/value=$REINSTATE)"/>

<!-- Status codes -->
<xsl:variable name="invalidHeader" select='1002'/>
<xsl:variable name="validHeaderInvalidItem" select='1003'/>
<xsl:variable name="validHeader" select='1004'/>
<xsl:variable name="invalidItem" select='2002'/>
<xsl:variable name="validItemInvalidHeader" select='2003'/>
<xsl:variable name="removedItem" select='2004'/>
<xsl:variable name="pendingItem" select='2005'/>
<xsl:variable name="processedItem" select='2006'/>
<xsl:variable name="validItem" select='2007'/>
<xsl:variable name="abandonedItem" select='2010'/>
<xsl:variable name="isHeaderValid" select="boolean(root/order/items/item[item_status=$validItem]) or boolean(root/order/items/item[item_status=$processedItem])"/>
<xsl:variable name="isLocked" select="boolean(root/order/header[buyer_locked_by !=''])"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>
<xsl:key name="validationField" match="/root/validation/order/header/data" use="@field_name"/>
<xsl:key name="originationCodes" match="/root/originationCodes/data" use="name"/>
<xsl:key name="errorPopup" match="/root/errorPopup/data" use="name"/>
<!--   <xsl:key name="sympathyAlerts" match="/root/sympathyAlerts/data" use="name"/> -->
<xsl:key name="sympathyAlertsItem" match="/root/sympathyAlertsItem/ItemMessages" use="name"/>

<!-- Case comparision values -->
<xsl:variable name="upper_case">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="lower_case">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="isAmazonOrder" select="boolean(translate(root/order/header/origin, $lower_case, $upper_case) = translate(key('originationCodes', 'amazon_origin_internet')/value, $lower_case, $upper_case))"/>
<xsl:variable name="isMercentOrder" select="root/order/header/is_mercent_order = 'Y'"/>
<xsl:variable name="isWalmartOrder" select="boolean(translate(root/order/header/origin, $lower_case, $upper_case) = translate(key('originationCodes', 'walmart_origin_internet')/value, $lower_case, $upper_case))"/>
<xsl:variable name="isCspiOrder" select="boolean(translate(root/order/header/origin, $lower_case, $upper_case) = translate(key('originationCodes', 'cspi_origin_internet')/value, $lower_case, $upper_case))"/>
<xsl:variable name="isCCValid" select="not(boolean(/root/validation/order/header/data[@field_name='cc_type'] or /root/validation/order/header/data[@field_name='cc_number'] or /root/validation/order/header/data[@field_name='cc_exp_date'] or key('pageData', 'no_charge_username_msg')/value or key('pageData', 'no_charge_password_msg')/value))"/>
<xsl:variable name="isGCGDValid" select="not(boolean(/root/validation/order/header/data[@field_name='gc_payment_info'] or /root/validation/order/header/data[@field_name='gc_payment_amt'] ))"/>
<xsl:variable name="paymentMethodAll" select="/root/order/header/payment_method_all"/>
<xsl:variable name="is_gcgd_valid" select="/root/is_gcgd_valid"/>
<xsl:variable name="vip_customer_flag" select="/root/vip_customer_flag"/>
<xsl:variable name="hasMorePayments" select="boolean(string-length($paymentMethodAll) > 1)"/>
<xsl:variable name="isUnitedOrder" select="/root/order/header/payment_method_id = 'UA'"/> 
<xsl:variable name="isAafesOrder" select="/root/order/header/payment_method_id = 'MS'"/> 
<xsl:variable name="isPremierCircleMember" select="/root/order/header/premier_circle_details/pc_flag = 'Y'"/>
<xsl:output method="html" indent="yes"/>
<xsl:variable name="mercentIcon" select="/root/order/header/mrcnt_channel_icon"/>
<xsl:variable name="mercentChannelName" select="/root/order/header/mrcnt_channel_name"/>
<xsl:variable name="isPartnerOrder" select="root/order/header/is_partner_order = 'Y'"/>
<xsl:variable name="partnerImgUrl" select="/root/order/header/partner_image_url"/>
<xsl:variable name="partnerName" select="/root/order/header/partner_name"/>
<xsl:variable name="isRosesDotCom" select="boolean(translate(root/order/header/origin, $lower_case, $upper_case) = 'ROSES')"/>
<xsl:variable name="isAribaOrder" select="boolean(translate(root/order/header/origin, $lower_case, $upper_case) = 'ARI')"/>
<xsl:variable name="isJointCart" select="/root/order/header/is_joint_cart = 'Y'"/>
<xsl:template match="/root">


<xsl:variable name="cartHasPremierItem">
  <xsl:value-of select="count(order/items/item/premier_collection_flag[. = 'Y'])"/>
</xsl:variable>

<html>
<head>
    <title>FTD - Order Scrub</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
<style type="text/css">

a.tooltip2{
    position:relative;
    z-index:24}

a.tooltip2:hover{z-index:25; background-color:#fff}

a.tooltip2 span{display: none}

a.tooltip2:hover span{ 
    display:block;
    position:absolute;
    top:2em; left:2em; width:auto;
    border:1px solid #3669AC;
    background-color: #CCEFFF;
    color:#000000;
    text-align: left;
    padding-left: 4px;
    padding-right: 4px;
    font-family: arial,helvetica,sans-serif;
    font-size: 8pt;
    text-decoration:none;
    filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,strength=4,direction=135);
}

</style>
    
    <link rel="stylesheet" type="text/css" href="../css/ftdScrubTabs.css"/>
    <script type="text/javascript" language="javascript" src="../js/FormChek.js"/>
    <script type="text/javascript" language="javascript" src="../js/popup.js"/>
    <script type="text/javascript" language="javascript" src="../js/sourceCodeLookup.js"/>
    <script type="text/javascript" language="javascript" src="../js/floristLookup.js"/>
    <script type="text/javascript" language="javascript" src="../js/cityLookup.js"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript">
  /*
   *  Global Variables
   */
    var EXIT = "_EXIT_";
    var isHeaderValid = <xsl:value-of select="$isHeaderValid"/>;
    var isLocked = <xsl:value-of select="$isLocked"/>;
    


    var showActionPopup = <xsl:value-of select="boolean(key('pageData', 'popup_flag')/value)"/>;
    var editProductFlag = <xsl:value-of select="boolean(key('pageData', 'edit_product_flag')/value)"/>;
    var isItemValid = <xsl:value-of select="boolean(key('pageData', 'submit_item_flag')/value)"/>;
    var wasCartAction = <xsl:value-of select="boolean(key('pageData', 'cart_flag')/value='Y')"/>;
    var isAmazonOrder = <xsl:value-of select="$isAmazonOrder"/>;
    var isMercentOrder = <xsl:value-of select="$isMercentOrder"/>;
    var isPartnerOrder = <xsl:value-of select="$isPartnerOrder"/>;
    var isRosesDotCom = <xsl:value-of select="$isRosesDotCom"/>;
    var isAribaOrder = <xsl:value-of select="$isAribaOrder"/>;
    var isWalmartOrder = <xsl:value-of select="$isWalmartOrder"/>;
    var isCspiOrder    = <xsl:value-of select="$isCspiOrder"/>;
    var isReinstateMode = <xsl:value-of select="$REINSTATE_MODE"/>;
    var showErrorPopup = <xsl:value-of select="boolean(key('errorPopup', 'showErrorPopup')/value='Y')"/>;
    var errorPopupMessage = "<xsl:value-of select="key('errorPopup', 'errorPopupMessage')/value"/>";
    var sympathyAlertMessage = "<xsl:value-of select="key('sympathyAlertsItem', 'SympathyCheck')/value"/>";
    var errorPopupReason = "<xsl:value-of select="key('errorPopup', 'reason')/value"/>";
    var showPreEditPopup = "<xsl:value-of select="boolean(key('errorPopup', 'show_pre_edit_popup')/value='Y')"/>";
    var editAltPayOrderWarning = "<xsl:value-of select="key('errorPopup', 'edit_alt_pay_order_warning')/value"/>";
    var stateArray = new Array();
    var countryArray = new Array();
    var countryCounter = 1;
    var stateCounter = 1;
    var errorDivs = new Array();
    var gcgd_valid_flag = "<xsl:value-of select="$is_gcgd_valid"/>";
    var pays = <xsl:value-of select="$hasMorePayments"/>;
    var isGCGDValid = <xsl:value-of select="$isGCGDValid"/>;
    var gcAmt = <xsl:value-of select="format-number(/root/order/header/gift_certificates/gift_certificate/gc_amount, '#0.00')"/>;
    
    
    var mercentChannel = "<xsl:value-of select="$mercentChannelName"/>";
    var partnerName = "<xsl:value-of select="$partnerName"/>";
	var vipCustomerFlag = "<xsl:value-of select="$vip_customer_flag"/>";
    <!-- We set Preferred Partner (e.g., USAA) messages as JS var since it preserves embedded html formatting -->
    var noPermissionsForPreferredPartnerMsg  = '<xsl:for-each select="/root/noPermissionsForPreferredPartner/preferred_partner"><xsl:value-of select="value"/></xsl:for-each>';
    var noPermissionsForPreferredPartnerFlag = <xsl:value-of select="boolean(count(/root/noPermissionsForPreferredPartner/preferred_partner) > 0)"/>;
    <xsl:variable name="anyPreferredPartners" select="boolean(count(/root/order/header/preferred_partners/preferred_partner) > 0)"/>
    
    <xsl:for-each select="order/items/item">
      <xsl:sort select="status_order" data-type="number"/>
      <xsl:choose>
        <!-- Invalid items -->
        <xsl:when test="item_status=$invalidItem or item_status=$validItemInvalidHeader">errorDivs.push("<xsl:value-of select="line_number"/>");</xsl:when>

        <!-- Pending items -->
        <xsl:when test="$PENDING_MODE and item_status=$pendingItem">errorDivs.push("<xsl:value-of select="line_number"/>");</xsl:when>

        <!-- Removed and Abaonded items -->
        <xsl:when test="$REINSTATE_MODE and item_status=$removedItem or item_status=$abandonedItem">errorDivs.push("<xsl:value-of select="line_number"/>");</xsl:when>
      </xsl:choose>
    </xsl:for-each>

    var errorFields = new Array(<xsl:for-each select="validation/order/items/item/data[@status='error']">
                                  <xsl:choose>
                                    <xsl:when test="position()=last()">"<xsl:value-of select="@field_name"/>"</xsl:when>
                                    <xsl:otherwise>"<xsl:value-of select="@field_name"/>", </xsl:otherwise>
                                  </xsl:choose>
                                </xsl:for-each>);
    <xsl:for-each select="validation/order/header/data[@status='error']">errorFields.push("<xsl:value-of select="@field_name"/>");</xsl:for-each>

    var usStates = new Array( <xsl:for-each select="stateList/state[countrycode='']">
                                ["<xsl:value-of select="statemasterid"/>", "<xsl:value-of select="statename"/>"]
                                <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                              </xsl:for-each>);
    var caStates = new Array( <xsl:for-each select="stateList/state[countrycode='CAN']">
                                ["<xsl:value-of select="statemasterid"/>", "<xsl:value-of select="statename"/>"]
                                <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                              </xsl:for-each>);
    var naStates = new Array(["NA", "N/A"]);

    var cartFieldListeners = new Array("buyer_first_name", "buyer_daytime_phone", "buyer_work_ext", "buyer_evening_phone", "buyer_home_ext", "buyer_last_name", "buyer_email_address", "buyer_address1", "buyer_address2", "buyer_city", "buyer_state", "buyer_country", "buyer_postal_code", "buyer_address_etc", "membership_id", "membership_first_name", "membership_last_name");
    var cartFieldDisable = new Array("buyer_address_etc", "buyer_first_name", "buyer_last_name", "buyer_address1", "buyer_address2", "buyer_city", "buyer_state", "buyer_country", "buyer_postal_code", "membership_id", "membership_first_name", "membership_last_name");
    var cartWalmartAmazonFieldDisable = new Array("pendingCart", "removeCart");
    var itemFieldNames = new Array("recip_first_name", "recip_last_name", "recip_phone", "recip_phone_ext", "recip_city", "recip_address1", "recip_address2", "recip_postal_code", "recip_country", "card_signature", "delivery_date", "ship_to_type_name", "card_message", "special_instructions");
    var cartAribaFieldDisable  = new Array("submitCart");

    var recipSelectedCountry = new Object();
    var recipSelectedState = new Object();
    var invalidAmazonStates = new Array(["AK"], ["HI"]);
   <!--  var invalidMercentStates = new Array(["AK"], ["HI"]); -->
    var currentShipMethod = "";

  /*
   *  Initialization Functions
   */
    function init(){
      setNavigationHandlers();
      applyDefaultListeners();
      setErrorFields(errorFields);
      setFieldsAccess(cartFieldDisable, isHeaderValid);
      if (isAmazonOrder){ 
          setAmazonFieldsAccess();
      }	  	
      		
      <xsl:for-each select="order/items/item">
        <xsl:variable name="execute">
          <!-- Invalid items -->
          <xsl:choose>
            <xsl:when test="item_status=$invalidItem or item_status=$validItemInvalidHeader">
              <xsl:value-of select="true()"/>
            </xsl:when>

            <!-- Pending items -->
            <xsl:when test="$PENDING_MODE and item_status=$pendingItem">
              <xsl:value-of select="true()"/>
            </xsl:when>

            <!-- Removed and Abaonded items -->
            <xsl:when test="$REINSTATE_MODE and item_status=$removedItem or item_status=$abandonedItem">
              <xsl:value-of select="true()"/>
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="false()"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

				<xsl:if test="$execute='true'">
          setCountry('recip_country<xsl:value-of select="line_number"/>', '<xsl:value-of select="recip_country"/>', 'recipient' );
          populateStates('recip_country<xsl:value-of select="line_number"/>', 'recip_state<xsl:value-of select="line_number"/>', '<xsl:value-of select="recip_state"/>', 'recipient');
        </xsl:if>
        <!-- Defect 1659 Copied setCountry and populateStates methods to set the country and state
				     code associated with orders which have already been scrubbed.  Issue related to country
						 code not being set which caused the RecalculateOrderVO to exclude the scrubbed order
						 when calculating order total.  -->
				<xsl:if test="$execute='false'">
          setCountryScrubbed('recip_country<xsl:value-of select="line_number"/>', '<xsl:value-of select="recip_country"/>');
          populateStatesScrubbed('recip_country<xsl:value-of select="line_number"/>', 'recip_state<xsl:value-of select="line_number"/>', '<xsl:value-of select="recip_state"/>');
        </xsl:if>        
        if (isAmazonOrder){
        	setAmazonItemFieldsAccess(<xsl:value-of select="line_number"/>);
        }
      </xsl:for-each>

      setCountry('buyer_country', '<xsl:value-of select="order/header/buyer_country"/>', 'buyer');

      populateStates('buyer_country', 'buyer_state', '<xsl:value-of select="order/header/buyer_state"/>', 'buyer');<![CDATA[

      var tab = "shoppingCart";
      var focusObj = (isHeaderValid) ? "buyer_daytime_phone" : "buyer_first_name";



      if (isItemValid || showActionPopup || editProductFlag){
        tab = document.getElementById("_content").value;
        focusObj = document.getElementById("_focus_object").value;
      }
	  
	  
	  
      initializeTabs(tab);

      if(!isLocked){
          document.getElementById(focusObj).focus();
          }

      if (noPermissionsForPreferredPartnerFlag) {
        doPreferredPartnerMessage();
        showActionPopup = false;
      }


      if (showActionPopup)
        doActionPopup();

        /* Show popup error messages */
        if (showErrorPopup)
            alert(errorPopupMessage);

        /* Display reinstate alert for Amazon */
        if (isAmazonOrder && isReinstateMode)
        {
            alert("This is an Amazon order and may not be reinstated.");
        }
		if (isMercentOrder && isReinstateMode)
        {
            alert(mercentChannel + " orders may not be reinstated.");
        }
        /* Display reinstate alert for Walmart */
        if(isWalmartOrder && isReinstateMode)
        {
            alert("This is a Wal-Mart order and may not be reinstated.");
        }

        /* Display reinstate alert for Homeland Security */
        if(isCspiOrder && isReinstateMode)
        {
            alert("This is a CSPI order and may not be reinstated.");
        }
        /* Display reinstate alert for Amazon */
        if (isPartnerOrder && isReinstateMode)
        {
            alert(partnerName + " orders may not be reinstated.");
        }
        /* Display reinstate alert for Roses.com */
        if(isRosesDotCom && isReinstateMode)
        {
            alert("This is a Roses.com order and may not be reinstated.");
        }
        /* Disable pending and remove buttons for Walmart, Mercent, Amazon and Partners Orders */
        if ((isAmazonOrder || isWalmartOrder || isCspiOrder || isMercentOrder || isPartnerOrder || isRosesDotCom ||isAribaOrder) && isReinstateMode)
        {
            setFieldsAccess(cartWalmartAmazonFieldDisable, true);
        }
        else
        {
            setFieldsAccess(cartWalmartAmazonFieldDisable, isHeaderValid);
        }
        if (document.getElementById("double_dispatch_error").value != "")
        {
          alert (document.getElementById("double_dispatch_error").value);
          doNextOrder();
        }
        
        if(isGCGDValid > 0)
	  	{
	  	    document.getElementById("is_gcgd_valid").value = 'Y';
	  	}
		else
		{
			document.getElementById("is_gcgd_valid").value = 'N';
		}
		
		if(isAribaOrder && isReinstateMode)
        {
            alert("Ariba Orders may not be reinstated.");
            setFieldsAccess(cartAribaFieldDisable, true);
        }
}

    function applyDefaultListeners(){
      for (var i = 0; i < itemFieldNames.length; i++){
        for (var j = 0; j < errorDivs.length; j++){
            addDefaultListenersSingle(itemFieldNames[i] + errorDivs[j]);
        }
      }
      addDefaultListenersArray(cartFieldListeners);
      cartFieldListeners = null;
    }

    function setAmazonFieldsAccess() {    
          document.OrderScrubEditForm.buyer_first_name.readOnly=true;
          document.OrderScrubEditForm.buyer_first_name.style.color='#696969';
          document.OrderScrubEditForm.buyer_first_name.tabIndex=-1;
          		
          document.OrderScrubEditForm.buyer_last_name.readOnly=true;
          document.OrderScrubEditForm.buyer_last_name.style.color='#696969';
          document.OrderScrubEditForm.buyer_last_name.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_address_etc.readOnly=true;
          document.OrderScrubEditForm.buyer_address_etc.style.color='#696969';
          document.OrderScrubEditForm.buyer_address_etc.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_address1.readOnly=true;
          document.OrderScrubEditForm.buyer_address1.style.color='#696969';
          document.OrderScrubEditForm.buyer_address1.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_address2.readOnly=true;
          document.OrderScrubEditForm.buyer_address2.style.color='#696969';
          document.OrderScrubEditForm.buyer_address2.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_city.readOnly=true;
          document.OrderScrubEditForm.buyer_city.style.color='#696969';
          document.OrderScrubEditForm.buyer_city.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_state.disabled=true;
          
          document.OrderScrubEditForm.buyer_postal_code.readOnly=true;
          document.OrderScrubEditForm.buyer_postal_code.style.color='#696969';
          document.OrderScrubEditForm.buyer_postal_code.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_country.disabled=true;
          
          document.OrderScrubEditForm.buyer_daytime_phone.readOnly=true;
          document.OrderScrubEditForm.buyer_daytime_phone.style.color='#696969';
          document.OrderScrubEditForm.buyer_daytime_phone.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_work_ext.readOnly=true;
          document.OrderScrubEditForm.buyer_work_ext.style.color='#696969';
          document.OrderScrubEditForm.buyer_work_ext.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_evening_phone.readOnly=true;
          document.OrderScrubEditForm.buyer_evening_phone.style.color='#696969';
          document.OrderScrubEditForm.buyer_evening_phone.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_home_ext.readOnly=true;
          document.OrderScrubEditForm.buyer_home_ext.style.color='#696969';
          document.OrderScrubEditForm.buyer_home_ext.tabIndex=-1;
          	
          document.OrderScrubEditForm.buyer_email_address.readOnly=true;
          document.OrderScrubEditForm.buyer_email_address.style.color='#696969';
          document.OrderScrubEditForm.buyer_email_address.tabIndex=-1;      
    }
    
    function setAmazonItemFieldsAccess(lineNo) {          
        eval("document.OrderScrubEditForm.recip_first_name" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_first_name" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_first_name" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_last_name" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_last_name" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_last_name" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.ship_to_type" + lineNo + ".disabled=true;");
        
        eval("document.OrderScrubEditForm.ship_to_type_name" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.ship_to_type_name" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.ship_to_type_name" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_address1" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_address1" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_address1" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_address2" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_address2" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_address2" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_city" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_city" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_city" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_state" + lineNo + ".disabled=true;");
        
        eval("document.OrderScrubEditForm.recip_postal_code" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_postal_code" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_postal_code" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_country" + lineNo + ".disabled=true;");
        eval("document.OrderScrubEditForm.qms_override_flag" + lineNo + ".disabled=true;");
        
        eval("document.OrderScrubEditForm.recip_phone" + lineNo + ".readOnly=true;");
        eval("document.OrderScrubEditForm.recip_phone" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_phone" + lineNo + ".tabIndex=-1;");
        
        eval("document.OrderScrubEditForm.recip_phone_ext" + lineNo + ".readOnly=true;");        
        eval("document.OrderScrubEditForm.recip_phone_ext" + lineNo + ".style.color='#696969';");
        eval("document.OrderScrubEditForm.recip_phone_ext" + lineNo + ".tabIndex=-1;");
    }
    
    function setCountry(countrySelectBox, selectedCountry, buyerOrRecip){

      var selectBox = document.getElementById(countrySelectBox);

      for (var i = 0; i < selectBox.options.length; i++){
        if (selectBox[i].value == selectedCountry){
          selectBox.selectedIndex = i;
          recipSelectedCountry[countrySelectBox] = i;
          break;
        }
         // if(isAmazonOrder && selectedCountry != 'US' && buyerOrRecip == "recipient")
          //{
         //   alert("Deliveries cannont be made outside of the Continental United States.  This is an Amazon order and must be removed and a new order entereed.  Please have your credit card ready.  Amazon will refund you for the original order.  The new order must be entered under source code 9849.");
         // }
      }

      if(buyerOrRecip == 'recipient')
      {

        countryArray[countryCounter++] = selectedCountry;
      }

    }

    function setCountryScrubbed(countrySelectBox, selectedCountry){

      var selectBox = document.getElementById(countrySelectBox);

      for (var i = 0; i < selectBox.options.length; i++){
        if (selectBox[i].value == selectedCountry){
          selectBox.selectedIndex = i;
          break;
        }
      }

    }

    function populateStates(countrySelectBox, stateSelectBox, selectedState, buyerOrRecip){

      var form = document.forms[0];

      var selectedCountry = document.getElementById(countrySelectBox)[document.getElementById(countrySelectBox).selectedIndex].value;

      var toPopulate = (selectedCountry == "US") ? usStates : (selectedCountry == "CA") ? caStates : naStates;

      var states = document.getElementById(stateSelectBox);
      states.options.length = 0;
      for (var i = 0; i < toPopulate.length; i++){
        states.add(new Option(toPopulate[i][1], toPopulate[i][0], false, false));
      }
      setSelectedState(stateSelectBox, selectedState, buyerOrRecip);
    }

    function populateStatesScrubbed(countrySelectBox, stateSelectBox, selectedState){

      var form = document.forms[0];

      var selectedCountry = document.getElementById(countrySelectBox)[document.getElementById(countrySelectBox).selectedIndex].value;

      var toPopulate = (selectedCountry == "US") ? usStates : (selectedCountry == "CA") ? caStates : naStates;

      var states = document.getElementById(stateSelectBox);
      states.options.length = 0;
      for (var i = 0; i < toPopulate.length; i++){
        states.add(new Option(toPopulate[i][1], toPopulate[i][0], false, false));
      }
      setSelectedStateScrubbed(stateSelectBox, selectedState);
    }

    function setSelectedState(field, state, buyerOrRecip)
    {
        if (state == null || state == '')
        {
            recipSelectedState[field] = null;
            return;
        }//end if

        var states = document.getElementById(field);
        for (var i = 0; i < states.length; i++)
        {
            if (states[i].value == state)
            {
                states.selectedIndex = i;
                recipSelectedState[field] = i;
                break;
            }
        }

        if( buyerOrRecip == 'recipient'  )
        {
          stateArray[stateCounter++] = state
        }


    }


    function setSelectedStateScrubbed(field, state)
    {
        if (state == null || state == '')
        {
            recipSelectedState[field] = null;
            return;
        }//end if

        var states = document.getElementById(field);
        for (var i = 0; i < states.length; i++)
        {
            if (states[i].value == state)
            {
                states.selectedIndex = i;
                break;
            }
        }

    }


    function changeRecipCountry(countrySelectBox, stateSelectBox)
    {
        var country = document.getElementById(countrySelectBox);
        var selectedCountry = country[country.selectedIndex].value;

        if (isAmazonOrder && (selectedCountry != "US"))
        {
            country.selectedIndex = recipSelectedCountry[countrySelectBox];
      	    alert("Deliveries cannont be made outside of the Continental United States.  This is an Amazon order and must be removed and a new order entereed.  Please have your credit card ready.  Amazon will refund you for the original order.  The new order must be entered under source code 9849.");
        }//end if (Amazon order &  country != US)
        else
        {
      	    recipSelectedCountry[countrySelectBox] = country.selectedIndex;
      	    populateStates(countrySelectBox, stateSelectBox, 'recipient');
            setAddressChangedFlag();
        }//end else
    }//end function changeCountry()


    function changeRecipState(stateSelectBox)
    {
        var state = document.getElementById(stateSelectBox);
        var selectedState = state[state.selectedIndex].value;

        if (isAmazonOrder && isState(selectedState ,invalidAmazonStates))
        {
            state.selectedIndex = recipSelectedState[stateSelectBox];
            alert("Deliveries to Alaska and Hawaii require an additional delivery charge.  This is an Amazon order and must be removed and a new order entered.  Please have your credit card ready.  Amazon will refund you for the original order.  The new order must be entered under source code 9849.");
        }//end if (Amazon order && Invalid state)
        else
        {
             recipSelectedState[stateSelectBox] = state.selectedIndex;
             setAddressChangedFlag();
        }//end else ()

        return;
    }//end function changeRecipState()

    function isState(selectedState, stateList)
    {
        for (var i = 0; i < stateList.length; i++)
        {
            if (selectedState == stateList[i])
            {
                return true;
            }//end if ()
        }//end for

        return false;
    }//end function isState()

    function setAddressChangedFlag() {
        var form = document.forms[0];
        form.address_changed_flag.value = "Y";
    }


  /*
   *  Lookup Functions
   */
    function doSourceCodeLookup(){
      SourceCodePopup.setup(
        {
          displayArea:'sourceCodeDisplay'
        }
      );
    }

    function doItemSourceCodeLookup(itemCount){
      SourceCodePopup.setup(
        {
          itemIndex:itemCount,
          displayArea:'sourceCodeDisplay'
        }
      );
    }

    function doFloristLookup(itemCount){
      FloristPopup.setup(
        {
          itemIndex:itemCount,
          displayArea:'floristDisplay'+itemCount
        }
      );
    }
    function doCityLookup(itemCount){
      if (!isAmazonOrder) { 
        CityPopup.setup(
          {
            itemIndex:itemCount,
            displayArea:'cityLookupLink'+itemCount
          }
        );
        if (itemCount != "shoppingCart") {
          setAddressChangedFlag();
        }
      }
    }
    
  /*
   *  Popup Functions
   */
    function doPreEditPopup() {
        if(showPreEditPopup && editAltPayOrderWarning != null && editAltPayOrderWarning.length > 0) {
            alert(editAltPayOrderWarning);
        }
    }
   
    function doCommentsPopup(lineNumber){
      var form = document.forms[0];
      var url_source =  "CommentsServlet" +
                        "?order_guid=" + form.order_guid.value +
                        "&line_number=" + lineNumber;

      var modal_dim = "dialogWidth:750px; dialogHeight:430px; center:yes; status=0; help:no; scroll:no";
      var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);

      if (ret && ret != null && ret == EXIT)
        doExitAction();
    }

    function doPaymentPopup(){
        if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
            alert(errorPopupMessage);
    	}    	
    	else
    	{ 
              var form = document.forms[0];
              var url_source =  "PaymentServlet" +
                                "?source_code=" + form.source_code.value +
                                "&action=load" +
                                "&order_guid=" + form.order_guid.value +
                                "&is_gcgd_valid=" + form.is_gcgd_valid.value+
                                "&master_order_number=" + form.master_order_number.value +
                                "&buyer_full_name=" + form.buyer_first_name.value + " " + form.buyer_last_name.value +]]>
                                "&amp;disable_flag=" + "<xsl:value-of select="$isHeaderValid"/>" +
                                "&amp;payment_method=" + "<xsl:value-of select="order/header/payment_method"/>" +
                                "&amp;payment_method_all=" + "<xsl:value-of select="order/header/payment_method_all"/>" +
                                "&amp;payment_method_id=" + "<xsl:value-of select="order/header/payment_method_id"/>" +
                                "&amp;cc_number=" + "<xsl:value-of select="order/header/cc_number"/>" +
                                "&amp;cc_exp_month=" + "<xsl:value-of select="order/header/cc_exp_month"/>" +
                                "&amp;cc_exp_year=" + "<xsl:value-of select="order/header/cc_exp_year"/>" +
                                <xsl:for-each select="order/header/gift_certificates/gift_certificate">
                                "&amp;gc_number<xsl:value-of select="position()"/>=" + "<xsl:value-of select="gc_number"/>" +
                                "&amp;gc_amount<xsl:value-of select="position()"/>=" + "<xsl:value-of select="gc_amount"/>" +
                                </xsl:for-each>
                                "&amp;invoice_number=" + "<xsl:value-of select="order/header/invoice_number"/>" +
                                "&amp;invoice_amount=" + "<xsl:value-of select="order/header/invoice_amount"/>" +
                                "&amp;payment_method_all=" + "<xsl:value-of select="order/header/payment_method_all"/>" +
                                "&amp;order_amount=" + "<xsl:value-of select="order/header/order_amount"/>"+<![CDATA[
                                getSecurityParams(false);
        
              var modal_dim = "dialogWidth:650px; dialogHeight:430px; center:yes; status=0; help:no; scroll:no";
              var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);
        
              if (ret && ret != null && ret[0] != EXIT){
                form.payment_method.value = ret[0];
                form.payment_method_id.value = ret[1];
                form.cc_number.value = ret[2];
                form.cc_exp_month.value = ret[3];
                form.cc_exp_year.value = ret[4];
                form.no_charge_username.value = ret[5];
                form.is_gcgd_valid.value = ret[6];
                performAction("PaymentServlet");
              }
              else if (ret && ret != null && ret[0] == EXIT){
                doExitAction();
              }
        }
    }

    function doActionPopup(){
      var form = document.forms[0];
      var url_source =  form.calling_servlet.value +
                        "?order_guid=" + form.order_guid.value +
                        "&action=load" +
                        "&_content=" + currentTab.id +
                        "&_focus_object=" + currentTab.focusobj +
                        "&cart_flag=" + form.cart_flag.value +
                        "&item_number=" + form.item_number.value +
                        "&calling_servlet=" + form.calling_servlet.value +
                        "&master_order_number=" + form.master_order_number.value +
                        "&item_order_number=" + form.item_order_number.value +
                        "&buyer_full_name=" + form.buyer_first_name.value + " " + form.buyer_last_name.value +
                        "&order_origin=" + form.order_origin.value +
                        getSecurityParams(false);

      var modal_dim = "dialogWidth:650px; dialogHeight:450px; center:yes; status=0; help:no; scroll:yes";

      var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);
      if (ret && ret != null && ret[0] != EXIT){  // returning from Pending/Remove popup
        form.disposition.value = ret[0];
        form.called_flag.value = ret[1];
        form.email_flag.value = ret[2];
        form.comments.value = ret[3];
        if (ret[4] != null){                      // returning from Email popup
          form.message_id.value = ret[4];
          form.email_subject.value = ret[5];
          form.email_body.value = ret[6];
        }
        performAction(form.calling_servlet.value);
      }
      else if (ret && ret != null && ret[0] == EXIT){
        doExitAction();
      }
    }

    function doFraudPopup(checkbox){
        if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
    		alert(errorPopupMessage);
    	} 
        else { 
              var form = document.forms[0];
              if (checkbox.checked){
                var url_source =  "FraudServlet" +
                                  "?master_order_number=" + form.master_order_number.value +
                                  "&action=load"+
                                  "&buyer_full_name=" + form.buyer_first_name.value + " " + form.buyer_last_name.value +
                                  getSecurityParams(false);
        
                var modal_dim = "dialogWidth:650px; dialogHeight:430px; center:yes; status=0; help:no; scroll:no";
                var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);
        
                if (ret && ret != null && ret[0] != EXIT){
                  form.fraud_id.value = ret[0];
                  form.fraud_comments.value = ret[1];
                }
                else if (ret && ret != null && ret[0] == EXIT){
                  doExitAction();
                }
                else {
                  checkbox.checked = false;
                }
              }
              else {
                form.fraud_id.value = "";
                form.fraud_comments.value = "";
              }
        }
    }

    function doEditProductPopup(lineNumber) {
        if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
    		alert(errorPopupMessage);
    	}
    	else
    	{ 
              doPreEditPopup();
              var form = document.forms[0];
              var deliveryDate = document.getElementById("delivery_date" + lineNumber).value;
              var product_id = document.getElementById("productid" + lineNumber).value;
              var country = document.getElementById("recip_country" + lineNumber);
              var state = document.getElementById("recip_state" + lineNumber);
              var occasion = document.getElementById("occasion" + lineNumber);
              var shipping_method= document.getElementById("shipping_method" + lineNumber);
                        
              if (shipping_method == null){
              	shipping_method="";
              }
              else
              {
             	 shipping_method="&shipping_method=" + shipping_method[shipping_method.selectedIndex].value;
              }
              
              // Loop over all form elements to find addons for this lineNumber then add to
              // param list (with lineNumber removed since no longer relevant when editing product)
              var elem = document.getElementById('OrderScrubEditForm').elements;
              var addon_str = "";
              for(var i = 0; i < elem.length; i++) {
                  if (elem[i].name.indexOf("addonOrig_" + lineNumber + "_") > -1) {
                     var addonIdOnly = /addonOrig_\d+_(.+)/.exec(elem[i].name);
                     addon_str += "&addonOrig_" + addonIdOnly[1] + "=" + elem[i].value;
                  }
              }
        
              var url_source =  "CategoryServlet" +
                                "?servlet_action=" +
                                "&source_code=" + form.source_code.value +
                                "&company_id=" + form.company_id.value +
                                "&partner_id=" + form.partner_id.value +
                                "&city=" + document.getElementById("recip_city" + lineNumber).value +
                                "&state=" + state[state.selectedIndex].value +
                                "&postal_code=" + document.getElementById("recip_postal_code" + lineNumber).value +
                                "&country=" + country[country.selectedIndex].value +
                                "&reward_type=" + ((document.getElementById("reward_type" + lineNumber).value == '') ? document.getElementById("discount_type" + lineNumber).value : document.getElementById("reward_type" + lineNumber).value) +
                                "&page_name=test" +
                                "&master_order_number=" + form.master_order_number.value +
                                "&orig_date=" +form.orig_date.value +
                                "&item_order_number=" + document.getElementById("item_order_number" + lineNumber).value +
                                 shipping_method +
                                "&occasion_id=" + occasion[occasion.selectedIndex].value +
                                "&occasion_text=" + occasion[occasion.selectedIndex].text +
                                "&buyer_full_name=" + form.buyer_first_name.value + " " + form.buyer_last_name.value +
                                addon_str +                                 
                                "&delivery_date=" + deliveryDate + 
                                "&order_guid=" + form.order_guid.value +
                                getSecurityParams(false);
                                
                    if(isAribaOrder){            
                         url_source = prepareAribaRequest(form,product_id,occasion,lineNumber,state,country,addon_str,shipping_method,deliveryDate);
                        }

              var modal_dim = "dialogWidth:775px; dialogHeight:580px; center:yes; status=0; help:no; scroll:no;";
              var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);
              if (ret && ret != null && ret[0] != EXIT){
                form.item_number.value = lineNumber;
                form.product_id.value = ret[0];
                form.product_subcode_id.value = ret[1];
                form.color_first_choice.value = ret[2];
                form.color_second_choice.value = ret[3];
                form.size_choice.value = ret[4];
                form.product_price.value = ret[5];
                form.second_choice_auth.value = ret[6];
                form.personal_greeting_flag.value = ret[7];

                // If addons were returned, create dynamic hidden vars for them.
                // There should be two elements for each addon - first is name,
                // second is value.
                if (ret.length > 8) {
                    var gotName = false;
                    var varName = "";
                    for (var x=8; x < ret.length; x++) {
                        if (!gotName) {
                            // Get name of variable.  Next iteration should be value
                            gotName = true;
                            varName = ret[x];
                        } else {
                            // This should be value, so create hidden var in form
                            gotName = false;
                            var input = document.createElement("input");
                            input.setAttribute("type", "hidden");
                            input.setAttribute("name", varName);
                            input.setAttribute("value", ret[x]);
                            form.appendChild(input);
                        }
                    }
                }

                form._content.value = currentTab.id;
                form._focus_object.value = currentTab.focusobj;
                doEditProductAction();
              }
              else if (ret && ret != null && ret[0] == EXIT){
                doExitAction();
              }
        }
    }

    function spellCheck(value, name) {
      value = escape(value).replace(/\+/g,"%2B");
      var url = "SpellCheckServlet?content=" + value + "&callerName=" + name;
      window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300,help=no");
    }



    function rosesDotComAlert() {
        alert('This is a Roses.com order, products and shipping methods cannot be changed.  Please remove this order and place a new order if these items need to change.');    
    }
    
    function shipMethodSave(sel) {
        $currentShipMethod = sel.value;
    }
    
    function shipMethodRestore(sel) {
         sel.value = $currentShipMethod;
        $currentShipMethod = "";
    }


  /*
   *  Actions
   */
    function doEditProductAction(){
      var url = "EditProductServlet";
      performAction(url);
    }
    
    function doUpdateShipMethodAction(selectObj, itemNumber){
        if (isRosesDotCom && selectObj.name.indexOf('shipping_method') > -1) {
            shipMethodRestore(selectObj);
            rosesDotComAlert();
        }
        else if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
            alert(errorPopupMessage);
    	}
    	else
    	{ 
            doPreEditPopup();
            var url = "ItemServlet" +
                "?servlet_action=update_ship_method" +
                "&item_number=" + itemNumber +
                "&_content=" + currentTab.id +
                "&_focus_object=" + currentTab.focusobj;
               performAction(url);
        }
    }

    function doCheckShipMethod(itemNumber, shippingMethod)
    { 
            var shipSelect = document.getElementById('shipping_method'+itemNumber);

            for (var i = 0; i < shipSelect.options.length; i++)
            {
                if (shipSelect[i].value == shippingMethod)
                {
                    shipSelect.selectedIndex = i;
                    alert('This is an Amazon order and must be removed and a new order entered.  Please have your credit card ready.  Amazon will refund you for the original order.  The new order must be entered under source code 9849.');
                    break;
                }
            }


    }

    function doReturnToSearchAction(){
      var url = "ShoppingCartServlet" +
                "?servlet_action=search_exit";
      performAction(url);
    }
    function doReturnToResultListAction(){
      var url = "ShoppingCartServlet" +
                "?servlet_action=result_list";
      performAction(url);
    }
    function doSubmitOrderAction(){
    	
		var cont = true;
    	if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
    		alert(errorPopupMessage);
    	}
    	else
    	{
    	
    	 	var sympathyElement =  document.getElementById('SympathyCheck');
			if (typeof(sympathyElement) != 'undefined' && sympathyElement != null)
			{
				var sympathyErrorMessage = sympathyElement.value;
				 if (sympathyErrorMessage!="" && errorPopupReason == "" && errorPopupReason != "block_reinstate") {
	    	  			var modalArguments = new Object();
	    				modalArguments.modalText = sympathyErrorMessage;
	        			cont = window.showModalDialog("../continue.html", modalArguments, 'dialogWidth:500px; dialogHeight:150px; center:yes; status=0; help=no; resizable:no; scroll:yes');
	    	  	 }
			}
    		
    	 
    	  if(cont) {
	      var url = "ShoppingCartServlet" +
	                "?servlet_action=submit_cart";
	      			performAction(url);
	      }
	    }
    }
    function doCartAction(servlet){
    	if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
               alert(errorPopupMessage);
    	}
    	else
    	{      var form = document.forms[0];
               _doAction(servlet, form.master_order_number.value, form.master_order_number.value, "Y");
        }
    }
    function doSubmitItem(itemNumber){
    	var cont = true;
    	if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
    		alert(errorPopupMessage);
    	}
    	else
    	{
    	  var sympathyElement =  document.getElementById('SympathyAlerts_'+itemNumber);
			if (typeof(sympathyElement) != 'undefined' && sympathyElement != null)
			{
			      var sympathyErrorMessage = sympathyElement.value;
		    	  if (sympathyErrorMessage!=null && sympathyErrorMessage!="" && errorPopupReason == "" && errorPopupReason != "block_reinstate") {
		    	  			var modalArguments = new Object();
		    				modalArguments.modalText = sympathyErrorMessage;
		        			cont = window.showModalDialog("../continue.html", modalArguments, 'dialogWidth:500px; dialogHeight:150px; center:yes; status=0; help=no; resizable:no; scroll:yes');
		    	  }
			}
    	 
    	  if (cont) {
			var form = document.forms[0];
			form.item_number.value = itemNumber;
			form._content.value = currentTab.id;
			form._focus_object.value = currentTab.focusobj;
			performAction("ItemServlet");
				}
			}
    }
    function doItemAction(servlet, orderNumber, itemNumber){
        if (errorPopupReason != "" && errorPopupReason == "block_reinstate")
    	{
               alert(errorPopupMessage);
    	}
    	else
    	{
            _doAction(servlet, orderNumber, itemNumber, "N");
        }
    }
    function _doAction(servlet, orderNumber, itemNumber, cartFlag){
      var form = document.forms[0];
      form._content.value = currentTab.id;
      form._focus_object.value = currentTab.focusobj;
      form.cart_flag.value = cartFlag;
      form.item_number.value = itemNumber;
      form.item_order_number.value = orderNumber;
      form.calling_servlet.value = servlet;
      var url = servlet +
                "?servlet_action=save";
      performAction(url);
    }
    function doNextOrder(){
      var url = "ScrubSearchServlet" +
                "?servlet_action=next_order";
      performAction(url);
    }
    function doExitAction() {
      var url = "ShoppingCartServlet" +
                "?servlet_action=main_exit";
      performAction(url);
    }
    function performAction(url){
      var ppDiv = document.getElementById("preferredPartnerDiv");
      ppDiv.style.display = "none";
      showWaitMessage("outterContent", "wait");
      setFieldsAccess(cartFieldDisable, false);
      document.forms[0].action = url;
      document.forms[0].submit();
    }
    function doPreferredPartnerMessage(){
      var content = document.getElementById("outterContent");
      var height = content.offsetHeight;
      var ppDiv = document.getElementById("preferredPartnerDiv").style;
      content.style.display = "none";
      ppDiv.display = "block";
      ppDiv.height = height;
    }

    function doServicesInfoPopup(){
        var form = document.forms[0];
        var url_source =  "ServicesInfoServlet" +
                          "?email_address=" + form.buyer_email_address.value + 
                          "&company_id=" + form.company_id.value +
                          getSecurityParams(false);
        
        var modal_dim = "dialogWidth:650px; dialogHeight:430px; center:yes; status=0; help:no";
        var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);
        
    }

    function doDisplayPopup(action) {
        var form = document.forms[0];
        var url_source =  "DisplayPopupServlet" +
                          "?servlet_action=" + action +
                          getSecurityParams(false);
        var modal_dim = "dialogWidth:650px; dialogHeight:430px; center:yes; status=0; help:no";
        var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);
    }

  /*
   *  Navigation Button Functions
   */
    function doNavigationPress(button){
      document.attachEvent("onmouseup", checkNavigationButtons);
      document.LAST_BUTTON_PRESSED = button;
      button.isPressed = true;
      button.className = "pressed";
    }
    function doNavigationRelease(button){
      document.detachEvent("onmouseup", checkNavigationButtons);
      button.isPressed = false;
      button.className = "default";
    }
    function doNavigationOut(button){
      button.className = "default";
    }
    function doNavigationOver(button){
      if (event.src = button && button.isPressed)
        doNavigationPress(button);
    }
    function checkNavigationButtons() {
      var button = document.LAST_BUTTON_PRESSED;
      var el = event.src;
      for (; el != null && el != button; el = el.parentNode);
      if (el == null)
        doNavigationRelease(button);
    }
    function prepareAribaRequest(form,product_id,occasion,lineNumber,state,country,addon_str,shipping_method,deliveryDate)
    {
         var url_source="SearchServlet" +
                                "?product_id="+ product_id+
                                "&company_id=" + form.company_id.value +
                                "&partner_id=" + form.partner_id.value +
                                "&page_number=init" +
                                "&occasion_text=" + occasion[occasion.selectedIndex].text +
                                "&occasion_id=" + occasion[occasion.selectedIndex].value +
                                "&search=simpleproductsearch" + 
                                "&reward_type=" + ((document.getElementById("reward_type" + lineNumber).value == '') ? document.getElementById("discount_type" + lineNumber).value : document.getElementById("reward_type" + lineNumber).value) +
                                "&script_code="  +
                                "&source_code=" + form.source_code.value +
                                "&city=" + document.getElementById("recip_city" + lineNumber).value +
                                "&state=" + state[state.selectedIndex].value +
                                "&postal_code=" + document.getElementById("recip_postal_code" + lineNumber).value +
                                "&country=" + country[country.selectedIndex].value +
                                "&master_order_number=" + form.master_order_number.value +
                                "&orig_date=" +form.orig_date.value +
                                "&item_order_number=" + document.getElementById("item_order_number" + lineNumber).value +
                                "&buyer_full_name=" + form.buyer_first_name.value + " " + form.buyer_last_name.value +
                                addon_str + 
                                shipping_method+
                                "&delivery_date=" + deliveryDate + 
                                "&order_guid=" + form.order_guid.value +
                                "&aribaSourceCode=YES"+
                                getSecurityParams(false);
                                
                                return url_source;
    }
    ]]>
    </script>
</head>

<body onload="javascript:init();">
  <form name="OrderScrubEditForm" id="OrderScrubEditForm" method="post" action="">
  <input type="hidden" name="servlet_action"/>
  <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
  <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
  <input type="hidden" name="order_guid" value="{order/header/order_guid}"/>
  <input type="hidden" name="master_order_number" value="{order/header/master_order_number}"/>
  <input type="hidden" name="orig_date" value="{order/header/order_date}"/>
  <input type="hidden" name="order_origin" value="{order/header/origin}"/>
  <input type="hidden" name="payment_method" value="{order/header/payment_method}"/>
  <input type="hidden" name="payment_method_id" value="{order/header/payment_method_id}"/>
  <input type="hidden" name="cc_number" value="{order/header/cc_number}"/>
  <input type="hidden" name="cc_exp_month" value="{order/header/cc_exp_month}"/>
  <input type="hidden" name="cc_exp_year" value="{order/header/cc_exp_year}"/>
  <input type="hidden" name="no_charge_username" value="{order/header/no_charge_username}"/>
  <input type="hidden" name="partner_id" value="{order/header/partner_id}"/>
  <input type="hidden" name="company_id" value="{order/header/company_id}"/>
  <input type="hidden" name="fraud_id" value="{order/header/fraud_id}"/>
  <input type="hidden" name="fraud_comments" value="{order/header/fraud_comments}"/>
  <input type="hidden" name="calling_servlet" value="{key('pageData', 'calling_servlet')/value}"/>
  <input type="hidden" name="item_number" value="{key('pageData', 'item_number')/value}"/>
  <input type="hidden" name="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
  <input type="hidden" name="cart_flag" value="{key('pageData', 'cart_flag')/value}"/>
  <input type="hidden" name="_content" value="{key('pageData', '_content')/value}"/>
  <input type="hidden" name="_focus_object" value="{key('pageData', '_focus_object')/value}"/>
  <input type="hidden" name="sc_email_address" value="{key('searchCriteria', 'sc_email_address')/value}"/>
  <input type="hidden" name="sc_date" value="{key('searchCriteria', 'sc_date')/value}"/>
  <input type="hidden" name="sc_date_flag" value="{key('searchCriteria', 'sc_date_flag')/value}"/>
  <input type="hidden" name="sc_product_code" value="{key('searchCriteria', 'sc_product_code')/value}"/>
  <input type="hidden" name="sc_last_name" value="{key('searchCriteria', 'sc_last_name')/value}"/>
  <input type="hidden" name="sc_last_name_flag" value="{key('searchCriteria', 'sc_last_name_flag')/value}"/>
  <input type="hidden" name="sc_phone" value="{key('searchCriteria', 'sc_phone')/value}"/>
  <input type="hidden" name="sc_phone_flag" value="{key('searchCriteria', 'sc_phone_flag')/value}"/>
  <input type="hidden" name="sc_ship_to_type" value="{key('searchCriteria', 'sc_ship_to_type')/value}"/>
  <input type="hidden" name="sc_confirmation_number" value="{key('searchCriteria', 'sc_confirmation_number')/value}"/>
  <input type="hidden" name="sc_pro_confirmation_number" value="{key('searchCriteria', 'sc_pro_confirmation_number')/value}"/>
  <input type="hidden" name="sc_origin" value="{key('searchCriteria', 'sc_origin')/value}"/>
  <input type="hidden" name="sc_csr_id" value="{key('searchCriteria', 'sc_csr_id')/value}"/>
  <input type="hidden" name="sc_search_type" value="{key('searchCriteria', 'sc_search_type')/value}"/>
  <input type="hidden" name="sc_date_from" value="{key('searchCriteria', 'sc_date_from')/value}"/>
  <input type="hidden" name="sc_date_to" value="{key('searchCriteria', 'sc_date_to')/value}"/>
  <input type="hidden" name="sc_mode" value="{key('searchCriteria', 'sc_mode')/value}"/>
  <input type="hidden" name="sc_source_code" value="{key('searchCriteria', 'sc_source_code')/value}"/>
  <input type="hidden" name="sc_scrub_reason" value="{key('searchCriteria', 'sc_scrub_reason')/value}"/>
  <input type="hidden" name="sc_product_property" value="{key('searchCriteria', 'sc_product_property')/value}"/>
  <input type="hidden" name="sc_preferred_partner" value="{key('searchCriteria', 'sc_preferred_partner')/value}"/>
  <input type="hidden" name="sc_pc_membership_id" value="{key('searchCriteria', 'sc_pc_membership_id')/value}"/>
  <input type="hidden" name="double_dispatch_error" value="{/root/error/error/value}"/>
  <input type="hidden" name="vipCustUpdatePermission" value="{key('security', 'vipCustUpdatePermission')/value}"/>
  <input type="hidden" name="vipCustomerMessage" value="{key('security', 'vipCustomerMessage')/value}"/>
  <input type="hidden" name="vipCustUpdateDeniedMessage" value="{key('security', 'vipCustUpdateDeniedMessage')/value}"/>
  <input type="hidden" name="sc_hide_test_orders" value="{key('searchCriteria', 'sc_hide_test_orders')/value}"/>
  <xsl:for-each select="searchCriteria/criteria[starts-with(name, 'sc_guid')]">
  <input type="hidden" name="{name}" value="{value}"/>
  </xsl:for-each>
  <xsl:for-each select="sympathyAlertsItem/ItemMessages">
  <input type="hidden" name="{name}" id="{name}" value="{value}"/>
  </xsl:for-each>
  <input type="hidden" name="disposition"/>
  <input type="hidden" name="called_flag"/>
  <input type="hidden" name="email_flag"/>
  <input type="hidden" name="comments"/>
  <input type="hidden" name="message_id"/>
  <input type="hidden" name="email_subject"/>
  <input type="hidden" name="email_body"/>
  <input type="hidden" name="product_id"/>
  <input type="hidden" name="product_subcode_id"/>
  <input type="hidden" name="color_first_choice"/>
  <input type="hidden" name="color_second_choice"/>
  <input type="hidden" name="size_choice"/>
  <input type="hidden" name="baloon"/>
  <input type="hidden" name="baloon_quantity"/>
  <input type="hidden" name="bear"/>
  <input type="hidden" name="bear_quantity"/>
  <input type="hidden" name="funeral_banner"/>
  <input type="hidden" name="funeral_banner_quantity"/>
  <input type="hidden" name="chocolate"/>
  <input type="hidden" name="chocolate_quantity"/>
  <input type="hidden" name="greeting_card"/>
  <input type="hidden" name="second_choice_auth"/>
  <input type="hidden" name="product_price"/>
  <input type="hidden" name="address_changed_flag" value="N"/>
  <input type="hidden" name="personal_greeting_flag"/>
  <input type="hidden" id="is_gcgd_valid" name="is_gcgd_valid" value="Y"/>
  
  
  <!-- Header Template -->
  <xsl:call-template name="addHeader"/>

  <!-- Outter div needed to hold any content that will be blocked when an action is performed -->
  <div id="outterContent" style="display:block">

    <!-- Navigation Links -->
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="40%">
          <xsl:call-template name="addNavLinks"/>
        </td>
        <td width="20%" align="center">
          <span class="label">Category:&nbsp;&nbsp;</span>
          <span class="screenText" style="text-transform:uppercase;"><xsl:value-of select="order/header/origin"/></span>
        </td>
        <td width="40%" align="right" valign="middle">
          <button id="prevItem" onmousedown="javascript:doNavigationPress(this);" onmouseup="javascript:doNavigationRelease(this);" onmouseout="javascript:doNavigationOut(this);" onmouseover="javascript:doNavigationOver(this);" onclick="javascript:showPrevItem();"/>
          <button id="nextItem" onmousedown="javascript:doNavigationPress(this);" onmouseup="javascript:doNavigationRelease(this);" onmouseout="javascript:doNavigationOut(this);" onmouseover="javascript:doNavigationOver(this);" onclick="javascript:showNextItem();"/>
        </td>
      </tr>
    </table>

    <!-- Tabs template -->
    <xsl:call-template name="tabs"/>

    <!-- Shopping cart Div -->
    <ul id="content">

    	<div id="shoppingCartDiv">
        <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
          <tr>
            <td>

              <!-- Master Order Information -->
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td class="tblheader" colspan="3">Master Order Number <xsl:value-of select="order/header/master_order_number"/></td>
                </tr>
                <tr>
                  <td width="15%" valign="top" class="label">Source Code:</td>
                  <td width="50%" valign="top">
                    <table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td width="100%">
                          <span id="sourceCodeDisplay">
                            <xsl:value-of select="order/header/source_code"/>&nbsp;-&nbsp;<xsl:value-of select="order/header/source_code_description"/>&nbsp;&nbsp;&nbsp;
                          </span>
                          &nbsp;&nbsp;
                          <xsl:if test="not($isHeaderValid) and count(coBrandList/coBrand/info_description) = 0 and not($isAmazonOrder) and not($isMercentOrder) and not($isWalmartOrder) and not($isCspiOrder) and not($isUnitedOrder) and not($isPartnerOrder) and not($isRosesDotCom)">
                          	<a id="sourceCodeLink" class="link" href="#" onclick="javascript:doSourceCodeLookup();">Change Source Code</a>
                          </xsl:if>
                          <input type="hidden" name="source_code" value="{order/header/source_code}"/>
                          <input type="hidden" name="source_code_description" value="{order/header/source_code_description}"/>
                        </td>
                      </tr>
                      <xsl:if test="$cartHasPremierItem &gt; 0">
                      	<tr>
                      	<td colspan="3"><img border="0" src="../images/luxury.jpg" align="absmiddle"/></td>
                      	</tr>
                      </xsl:if>
                      <xsl:if test="key('pageData', 'source_update')/value = 'Y'">
                      	<tr>
                      	<td colspan="3">Source Code rewards or discounts will not apply to the Item of the Week orders</td>
                      	</tr>
                      </xsl:if>
                      <xsl:if test="key('validationField','source_code')/messages/message">
                      <tr>
                        <td width="100%" class="errorMessage">
                        <xsl:for-each select="key('validationField','source_code')/messages/message">
                          <xsl:value-of select="description"/><br/>
                        </xsl:for-each>
                        </td>
                      </tr>
                      </xsl:if>
                    </table>
                  </td>
                  <td width="45%" valign="top">
                  <xsl:if test="not($isAmazonOrder) and not($isMercentOrder) and not($isPartnerOrder) and not($isAafesOrder)">  
                    <table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <tr>
                        <td><a class="link" href="#" onclick="javascript:doPaymentPopup();">Payment Information</a></td>
                      </tr>
                      <xsl:if test="not($isCCValid)">
                        <tr>
                          <td class="errorMessage">Credit card Payment information contains errors</td>
                        </tr>
                      </xsl:if>
                      <xsl:if test="not($isGCGDValid)">
                        <tr>
                          <td class="errorMessage">Gift Certificate Payment information contains errors</td>
                        </tr>
                      </xsl:if>
                    </table>
                  </xsl:if>
                  </td>
                </tr>
              </table>

              <!-- Customer Information -->
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td class="tblheader" colspan="5">Customer Information</td>
                </tr>

               <xsl:choose>
               <xsl:when test="order/header/buyer_locked_by != '' ">
                <tr>
                  <td class="errorMessage">
                    Locked by:<xsl:value-of select="order/header/buyer_locked_by"/>
                    <input type="hidden" name="buyer_locked_by" value="{order/header/buyer_locked_by}"/>
                  </td>
                </tr>
               </xsl:when>
               </xsl:choose>


                <tr>
                  <td width="50%" valign="top">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                      <tr>
                        <td width="30%" class="label" style="vertical-align:top">First Name:</td>
                       <xsl:choose>
                       <xsl:when test="order/header/buyer_locked_by = '' ">
                            <td width="70%"><input type="text" name="buyer_first_name" style="vertical-align:top"  tabindex="1" size="30" maxlength="20" value="{order/header/buyer_first_name}"/>
                            <xsl:if test="$vip_customer_flag = 'Y' ">
							<span class="errorMessage" style="WIDTH: 140px; font-weight:normal; DISPLAY: inline-block; padding-left:17px">
								 <script type="text/javascript">
								 	document.write(document.getElementById('vipCustomerMessage').value);
								 </script>
							</span>
							</xsl:if>
                            </td>
                       </xsl:when>
                       <xsl:otherwise>
                            <input type="hidden" name="buyer_first_name" style="vertical-align:top" tabindex="1" size="30" maxlength="20" value="{order/header/buyer_first_name}"/>
                            <td width="70%" style="vertical-align:top"><xsl:value-of select="order/header/buyer_first_name"/>
                            <xsl:if test="$vip_customer_flag = 'Y' ">
							<span class="errorMessage" style="WIDTH: 140px; font-weight:normal; DISPLAY: inline-block; padding-left:17px">
								 <script type="text/javascript">
								 	document.write(document.getElementById('vipCustomerMessage').value);
								 </script>
							</span>
							</xsl:if>
                            </td>
                       </xsl:otherwise>
                       </xsl:choose>

                      </tr>
                      <xsl:if test="key('validationField','buyer_first_name')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_first_name')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">Last Name:</td>

                       <xsl:choose>
                       <xsl:when test="order/header/buyer_locked_by = '' ">
                        <td width="70%"><input type="text" name="buyer_last_name" tabindex="2" size="20" maxlength="20" value="{order/header/buyer_last_name}"/></td>
                       </xsl:when>
                       <xsl:otherwise>
                            <td width="70%"><xsl:value-of select="order/header/buyer_last_name"/></td>
                            <input type="hidden" name="buyer_last_name" tabindex="2" size="20" maxlength="20" value="{order/header/buyer_last_name}"/>
                       </xsl:otherwise>
                       </xsl:choose>

                      </tr>
                      <xsl:if test="key('validationField','buyer_last_name')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_last_name')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">Business Name:</td>

                       <xsl:choose>
                       <xsl:when test="order/header/buyer_locked_by = '' ">
                        <td width="70%"><input type="text" name="buyer_address_etc" tabindex="3" size="30" maxlength="30" value="{order/header/buyer_address_etc}"/></td>
                       </xsl:when>
                       <xsl:otherwise>
                            <td width="70%"><xsl:value-of select="order/header/buyer_address_etc"/></td>
                            <input type="hidden" name="buyer_address_etc" tabindex="3" size="30" maxlength="30" value="{order/header/buyer_address_etc}"/>
                       </xsl:otherwise>
                       </xsl:choose>

                      </tr>
                      <tr>
                        <td width="30%" class="label">Address 1:</td>

                        <xsl:choose>
			            <xsl:when test="key('validationField','buyer_address1')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH'] and key('validationField','buyer_address2')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH']">
			                <td width="70%"><input type="text" name="buyer_address1" tabindex="4" size="30" maxlength="30" value=""/></td>
			            </xsl:when>
			            <xsl:otherwise>

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
			                <td width="70%"><input type="text" name="buyer_address1" tabindex="4" size="30" maxlength="30" value="{order/header/buyer_address1}"/></td>
                           </xsl:when>
                           <xsl:otherwise>
                                <td width="70%"><xsl:value-of select="order/header/buyer_address1"/></td>
                                <input type="hidden" name="buyer_address1" tabindex="4" size="30" maxlength="30" value="{order/header/buyer_address1}"/>
                           </xsl:otherwise>
                           </xsl:choose>

			            </xsl:otherwise>
			            </xsl:choose>

                        <!--
                        <td width="70%"><input type="text" name="buyer_address1" tabindex="4" size="30" maxlength="30" value="{order/header/buyer_address1}"/></td>
                        -->


                      </tr>
                      <xsl:if test="key('validationField','buyer_address1')/messages/message">
                      	<xsl:choose>
	              		<xsl:when test="key('validationField','buyer_address1')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH'] and key('validationField','buyer_address2')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH']" />
	              		<xsl:otherwise>
	                		<xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_address1')"/>
                        </xsl:call-template>
	              		</xsl:otherwise>
	              	    </xsl:choose>

                      	<!--
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_address1')"/>
                        </xsl:call-template>
                        -->
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">Address 2:</td>

                        <xsl:choose>
			            <xsl:when test="key('validationField','buyer_address1')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH'] and key('validationField','buyer_address2')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH']">

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
			                <td width="70%"><input type="text" name="buyer_address2" tabindex="5" size="30" maxlength="30" value="{order/header/buyer_address2}"/></td>
                           </xsl:when>
                           <xsl:otherwise>
                                <td width="70%"><xsl:value-of select="order/header/buyer_address2"/></td>
                                <input type="hidden" name="buyer_address2" tabindex="5" size="30" maxlength="30" value="{order/header/buyer_address2}"/>
                           </xsl:otherwise>
                           </xsl:choose>

			            </xsl:when>
			            <xsl:otherwise>

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
			                <td width="70%"><input type="text" name="buyer_address2" tabindex="5" size="30" maxlength="30" value="{order/header/buyer_address2}"/></td>
                           </xsl:when>
                           <xsl:otherwise>
			                <td width="70%"></td>
                           </xsl:otherwise>
                           </xsl:choose>

			            </xsl:otherwise>
			            </xsl:choose>

			            <!--
                        <td width="70%"><input type="text" name="buyer_address2" tabindex="5" size="30" maxlength="30" value="{order/header/buyer_address2}"/></td>
                      	-->
                      </tr>
                      <xsl:if test="key('validationField','buyer_address2')/messages/message">
                      	<xsl:choose>
	              		<xsl:when test="key('validationField','buyer_address1')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH'] and key('validationField','buyer_address2')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH']" />
	              		<xsl:otherwise>
	                		<xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_address2')"/>
                        </xsl:call-template>
	              		</xsl:otherwise>
	              	    </xsl:choose>

                      	<!--
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_address2')"/>
                        </xsl:call-template>
                        -->
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">City, State:</td>
                        <td width="70%" >

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
                          <input type="text" name="buyer_city" tabindex="6" size="15" maxlength="30" value="{order/header/buyer_city}"/>,&nbsp;
                           </xsl:when>
                           <xsl:otherwise>
                                <xsl:value-of select="order/header/buyer_city"/>
                                <input type="hidden" name="buyer_city" tabindex="6" size="15" maxlength="30" value="{order/header/buyer_city}"/>
                           </xsl:otherwise>
                           </xsl:choose>

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
			                <select name="buyer_state" tabindex="7" ></select>
                           </xsl:when>
                           <xsl:otherwise>
                            <div id="invisibleState" style="position:absolute;visibility:hidden;">
			                <select name="buyer_state" tabindex="7" ></select>
                            </div>
                                , <xsl:value-of select="order/header/buyer_state"/>
                           </xsl:otherwise>
                           </xsl:choose>




                        </td>
                      </tr>
                      <xsl:if test="key('validationField','buyer_city')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_city')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <xsl:if test="key('validationField','buyer_state')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_state')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">
                          Zip/Postal Code:<br/>
                          <xsl:if test="not($isHeaderValid)">
                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                                    <a id="cityLookupLinkshoppingCart" class="link" tabindex="9" href="#" onclick="javascript:doCityLookup('shoppingCart');">Lookup by City</a>
                                   </xsl:when>
                                   </xsl:choose>
                          </xsl:if>
                        </td>
                        <td width="30%" >

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
                          <input type="text" name="buyer_postal_code" tabindex="8" size="5" maxlength="12" value="{order/header/buyer_postal_code}"/>
                           </xsl:when>
                           <xsl:otherwise>
                                <xsl:value-of select="order/header/buyer_postal_code"/>
                           </xsl:otherwise>
                           </xsl:choose>

                          &nbsp;Country:&nbsp;
                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
                          <select name="buyer_country" tabindex="10" onchange="javascript:populateStates('buyer_country', 'buyer_state', 'buyer');">
                            <xsl:for-each select="countryList/country">
                              <option value="{country_id}"><xsl:value-of select="name"/></option>
                            </xsl:for-each>
                          </select>
                           </xsl:when>
                           <xsl:otherwise>
                                <xsl:value-of select="order/header/buyer_country"/>
                          <div id="invisibleCountry" style="position:absolute;visibility:hidden;">
                          <select name="buyer_country" tabindex="10" onchange="javascript:populateStates('buyer_country', 'buyer_state', 'buyer');">
                            <xsl:for-each select="countryList/country">
                              <option value="{country_id}"><xsl:value-of select="name"/></option>
                            </xsl:for-each>
                          </select>
                            </div>
                           </xsl:otherwise>
                           </xsl:choose>








                        </td>
                      </tr>
                      <xsl:if test="key('validationField','buyer_postal_code')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_postal_code')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <xsl:if test="key('validationField','buyer_country')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_country')"/>
                        </xsl:call-template>
                      </xsl:if>
                    </table>
                  </td>
				<td valign="top">
				<xsl:if test="$isPremierCircleMember">
						<img src="../images/ftd_premier_circle_logo.gif" /> 
				</xsl:if>
				</td>
                <xsl:choose>
                <xsl:when test="$anyPreferredPartners">
                <td valign="top">
                    <xsl:for-each select="/root/order/header/preferred_partner_names/preferred_partner">
                    	<img src="../images/{.}_logo_large.gif"/><br/>
                    </xsl:for-each>
                    <xsl:if test="$isJointCart">
						<br/><img src="../images/partner_jointcart.gif"/>
					</xsl:if>
                </td>
                </xsl:when>
				<xsl:when test="$isAmazonOrder">
					<td valign="top">	
						<img src="../images/amazon_logo_large.gif" /> 
					</td>
				</xsl:when>   
				<xsl:when test="$isMercentOrder">
					<td valign="top">	
						<img src="{$mercentIcon}" />
					</td>
				</xsl:when> 
				<xsl:when test="$isPartnerOrder">
					<td valign="top">	
						<img src="{$partnerImgUrl}" />
					</td>
				</xsl:when>               
                <xsl:otherwise>
		            <td></td>
                </xsl:otherwise>
                </xsl:choose>

                  <td width="50%" valign="top">
                    <table width="100%" border="0" cellpadding="1" cellspacing="1">
                      <tr>
                        <td width="30%" class="label">Phone 1:</td>
                        <td width="70%">
                          <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="30%">
                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                                    <input type="text" name="buyer_daytime_phone" tabindex="11" size="15" maxlength="20" value="{order/header/buyer_daytime_phone}"/>
                                   </xsl:when>
                                   <xsl:otherwise>
                                        <xsl:value-of select="order/header/buyer_daytime_phone"/>
                                         <input type="hidden" name="buyer_daytime_phone" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_daytime_phone}"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
                              </td>
                              <td class="label">
                                &nbsp;&nbsp;Ext:&nbsp;
                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                                        <input type="text" name="buyer_work_ext" tabindex="12" size="6" maxlength="10" value="{order/header/buyer_daytime_ext}"/>
                                   </xsl:when>
                                   <xsl:otherwise>
                                        <xsl:value-of select="order/header/buyer_work_ext"/>
                                         <input type="hidden" name="buyer_work_ext" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_daytime_ext}"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <xsl:if test="key('validationField','buyer_daytime_phone')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_daytime_phone')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">Phone 2:</td>
                        <td width="70%">
                          <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="30%">
                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                                        <input type="text" name="buyer_evening_phone" tabindex="13" size="15" maxlength="20" value="{order/header/buyer_evening_phone}"/>
                                   </xsl:when>
                                   <xsl:otherwise>
                                        <xsl:value-of select="order/header/buyer_evening_phone"/>
                                         <input type="hidden" name="buyer_evening_phone" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_evening_phone}"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
                              </td>
                              <td class="label">
                                &nbsp;&nbsp;Ext:&nbsp;
                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                                        <input type="text" name="buyer_home_ext" tabindex="14" size="6" maxlength="10" value="{order/header/buyer_evening_ext}"/>
                                   </xsl:when>
                                   <xsl:otherwise>
                                         <input type="hidden" name="buyer_home_ext" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_evening_ext}"/>
                                        <xsl:value-of select="order/header/buyer_home_ext"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <xsl:if test="key('validationField','buyer_evening_phone')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_evening_phone')"/>
                        </xsl:call-template>
                      </xsl:if>
                      <tr>
                        <td width="30%" class="label">Email Address:</td>
                        <td width="70%">
                                   <xsl:choose>
                                   <xsl:when test="order/header/free_shipping_purchase_flag = 'Y'">
                                        <input type="hidden" name="buyer_email_address" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_email_address}"/>
                                        <xsl:value-of select="order/header/buyer_email_address"/>
                                   </xsl:when>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                                    <input type="text" name="buyer_email_address" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_email_address}"/>
                                   </xsl:when>
                                   <xsl:otherwise>
                                        <input type="hidden" name="buyer_email_address" tabindex="15" class="Error" size="30" maxlength="55" value="{order/header/buyer_email_address}"/>
                                        <xsl:value-of select="order/header/buyer_email_address"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
                      </td>
                      </tr>
                      <xsl:if test="key('validationField','buyer_email_address')/messages/message">
                        <xsl:call-template name="errorMessage">
                          <xsl:with-param name="node" select="key('validationField','buyer_email_address')"/>
                        </xsl:call-template>
                      </xsl:if>

                      <xsl:if test="key('validationField','buyer_address1')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH'] and key('validationField','buyer_address2')/messages/message[type='VALIDATION_ERROR_TYPE_LENGTH']">
                        <tr>
                          <td width="30%" class="errorMessage">Address too long</td>
                          <td width="70%"><xsl:value-of select="order/header/buyer_address1"/></td>
                        </tr>
                      </xsl:if>
                      
                      <tr>
                        <td colspan="2">
                          <a class="link" href="#" onclick="javascript:doServicesInfoPopup();">Services Info</a>
                        </td>
                      </tr>

                      <tr>
                        <td width="30%">&nbsp;</td>
                        <td width="70%">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="30%">&nbsp;</td>
                        <td width="70%">&nbsp;</td>
                      </tr>
                      <xsl:if test="membership_flag='Y' and count(coBrandList/coBrand) = 0">
                        <tr>
                          <td width="30%" class="label">Member ID:</td>
                          <td width="70%">
                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
                                <input type="text" name="membership_id" tabindex="16" size="30" maxlength="25" value="{order/header/membership_id}"/>
                           </xsl:when>
                           <xsl:otherwise>
                                <input type="hidden" name="membership_id" tabindex="16" size="30" maxlength="25" value="{order/header/membership_id}"/>
                                <xsl:value-of select="order/header/membership_id"/>
                           </xsl:otherwise>
                           </xsl:choose>

                          </td>
                        </tr>
                        <xsl:if test="key('validationField','membership_id')/messages/message">
                          <xsl:call-template name="errorMessage">
                            <xsl:with-param name="node" select="key('validationField','membership_id')"/>
                          </xsl:call-template>
                        </xsl:if>
                        <tr>
                          <td width="30%" class="label">Member First Name:</td>
                          <td width="70%">

                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
                            <input type="text" name="membership_first_name" tabindex="17" size="30" maxlength="25" value="{order/header/membership_first_name}"/>
                           </xsl:when>
                           <xsl:otherwise>
                            <input type="hidden" name="membership_first_name" tabindex="17" size="30" maxlength="25" value="{order/header/membership_first_name}"/>
                                <xsl:value-of select="order/header/membership_first_name"/>
                           </xsl:otherwise>
                           </xsl:choose>

                          </td>
                        </tr>
                        <tr>
                          <td width="30%" class="label">Member Last Name:</td>
                          <td width="70%">
                           <xsl:choose>
                           <xsl:when test="order/header/buyer_locked_by = '' ">
                            <input type="text" name="membership_last_name" tabindex="18" size="30" maxlength="25" value="{order/header/membership_last_name}"/>
                           </xsl:when>
                           <xsl:otherwise>
                            <input type="hidden" name="membership_last_name" tabindex="18" size="30" maxlength="25" value="{order/header/membership_last_name}"/>
                                <xsl:value-of select="order/header/membership_last_name"/>
                           </xsl:otherwise>
                           </xsl:choose>
                          </td>
                        </tr>
                      </xsl:if>


                      <xsl:if test="key('validationField','cost_center')/messages/message">
                          <xsl:call-template name="errorMessage">
                            <xsl:with-param name="node" select="key('validationField','cost_center')"/>
                          </xsl:call-template>
                      </xsl:if>


                      <xsl:for-each select="coBrandList/coBrand">
                        <xsl:variable name="id" select="info_description"/>
                        <xsl:variable name="coBrandPosition" select="position()"/>
                        <!-- Since cb_data may be mixed case, need to jump through hoops to ensure we find matches, thats why we setup cbDataValue here -->
                        <xsl:variable name="cbDataValue" 
                                        select="/root/order/header/co_brands/co_brand[contains(translate(cb_name,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 
                                                translate($id,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ'))]/cb_data"/>

                        <tr>
                        <xsl:choose>
                          <xsl:when test="boolean(info_display/text())">
                          <td width="30%" class="label"><xsl:value-of select="info_display"/></td>
                          </xsl:when>
                          <xsl:otherwise>
                          <td width="30%" class="label"><xsl:value-of select="info_description"/></td>
                          </xsl:otherwise>
                        </xsl:choose>
                          <input type="hidden" name="cb_name{position()}" value="{info_description}"/>
                          <td width="70%">
                            <xsl:choose>
                                    <xsl:when test="prompt_type = 'DROPDOWN'">

                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                          			<select tabindex="19" name="cb_data{position()}">
                          				<option value="">Please Select</option>
                          				<xsl:for-each select="/root/coBrandOptionList/coBrandOption[billinginfodesc = $id]">
											<option value="{optionvalue}">
												<xsl:if test="optionvalue = $cbDataValue"><xsl:attribute name="SELECTED"/></xsl:if>
												<xsl:value-of select="optiondisplay"/>
											</option>
										</xsl:for-each>
									</select>
                                   </xsl:when>
                                   <xsl:otherwise>
                                    <div id="invisibleCoBrand" style="position:absolute;visibility:hidden;">
                          			<select tabindex="19" name="cb_data{position()}">
                          				<option value="">Please Select</option>
                          				<xsl:for-each select="/root/coBrandOptionList/coBrandOption[billinginfodesc = $id]">
											<option value="{optionvalue}">
												<xsl:if test="optionvalue = $cbDataValue"><xsl:attribute name="SELECTED"/></xsl:if>
												<xsl:value-of select="optiondisplay"/>
											</option>
										</xsl:for-each>
									</select>
                                    </div>
                                        <xsl:value-of select="$cbDataValue"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
                                   

                                   </xsl:when>
                                    <xsl:when test="prompt_type = 'STATIC'">
                          			<input type="hidden" name="cb_data{position()}" value="{/root/coBrandOptionList/coBrandOption[billinginfodesc = $id]/optionvalue}"/>

                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
                          			<input type="text" name="cb_data_display{position()}" tabindex="19" size="30" maxlength="25" value="{/root/coBrandOptionList/coBrandOption[billinginfodesc = $id]/optionvalue}"><xsl:attribute name="disabled">true</xsl:attribute></input>
                                   </xsl:when>
                                   <xsl:otherwise>
                          			<input type="hidden" name="cb_data_display{position()}" tabindex="19" size="30" maxlength="25" value="{/root/coBrandOptionList/coBrandOption[billinginfodesc = $id]/optionvalue}"><xsl:attribute name="disabled">true</xsl:attribute></input>
                                        <xsl:value-of select="/root/coBrandOptionList/coBrandOption[billinginfodesc = $id]/optionvalue"/>
                                   </xsl:otherwise>
                                   </xsl:choose>

                          		</xsl:when>
                          		<xsl:otherwise>

                                   <xsl:choose>
                                   <xsl:when test="order/header/buyer_locked_by = '' ">
		                        	<input type="text" name="cb_data{position()}" tabindex="19" size="30" maxlength="25" value="{$cbDataValue}" onfocus="javascript:fieldFocus();" onblur="javascript:fieldBlur();"></input>
                                   </xsl:when>
                                   <xsl:otherwise>
		                        	<input type="hidden" name="cb_data{position()}" tabindex="19" size="30" maxlength="25" value="{$cbDataValue}" onfocus="javascript:fieldFocus();" onblur="javascript:fieldBlur();"></input>
                                        <xsl:value-of select="$cbDataValue"/>
                                   </xsl:otherwise>
                                   </xsl:choose>
		                        </xsl:otherwise>
                            </xsl:choose>

                            <xsl:if test="prompt_type != 'STATIC'">
                            	<xsl:for-each select="/root/validation/order/header/data[@field_name='billing_info']/messages/message">
			                    <xsl:if test="substring-before(description, '~') = $id">
			                    <tr><td></td><td class="errorMessage">
			                    <script>
			                      document.getElementById('cb_data<xsl:value-of select="$coBrandPosition"/>').className = "errorField";
			                    </script>
			                    <xsl:value-of select="substring-after(description, '~')"/>
			                    </td></tr>
			                    </xsl:if>
			                    </xsl:for-each>
			                    <script type="text/javascript" language="javascript">cartFieldDisable.push('cb_data<xsl:value-of select="position()"/>');</script>
                            </xsl:if>

                          </td>
                        </tr>
                      </xsl:for-each>


                    </table>
                  </td>
                </tr>
              </table>

              <!-- Show alternate contact information if neccessary -->
              <xsl:call-template name="altContact"/>

              <!-- Shopping Cart Summary / Line Items Information -->
              <xsl:call-template name="itemsSummary"/>

            </td>
          </tr>
        </table>

        <!-- Action Buttons -->
        <xsl:call-template name="addActionButtons"/>

        <!-- Footer Template -->
        <xsl:call-template name="footer"/>
	        <script type="text/javascript">
		   	if(document.getElementById('vipCustUpdatePermission').value == 'N'){
		      if(vipCustomerFlag =='Y')
		       	{
	       	        var modalArguments = new Object();
	                modalArguments.modalText = document.getElementById('vipCustUpdateDeniedMessage').value;
	                window.showModalDialog("../alert.html", modalArguments, 'dialogWidth:300px; dialogHeight:200px; center:yes; status=no; help=no; resizable:no; scroll:yes');
	       		}
		      }
		 	</script>
      </div>

      <!-- Item Divs -->
      <xsl:call-template name="itemDiv"/>

    </ul>
  </div>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>

      <!-- Footer Template -->
      <xsl:call-template name="footer"/>

    </table>
  </div>

  <!-- Preferred Partner (e.g., USAA) div -->
  <div id="preferredPartnerDiv" style="display:none">
    <table id="preferredPartnerTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="preferredPartnerMessage" align="center" width="80%" class="waitMessage">
    <script>
      <![CDATA[ document.write(noPermissionsForPreferredPartnerMsg);]]>
    </script>
              </td>
            </tr>
            <tr>
              <td align="center" width="80%" class="waitMessage">
              <input name="nextOrderCart" type="button" value="Next Order" tabindex="32" onclick="javascript:doNextOrder();"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <!-- Footer Template -->
      <xsl:call-template name="footer"/>

    </table>
  </div>

  </form>
</body>
</html>

</xsl:template>

<!-- Actual call to header template -->
<xsl:template name="addHeader">
  <xsl:call-template name="header">
    <xsl:with-param name="headerName">
      <xsl:if test="$SCRUB_MODE"><xsl:value-of select="'Order Scrub'"/></xsl:if>
      <xsl:if test="$PENDING_MODE"><xsl:value-of select="'Pending Order'"/></xsl:if>
      <xsl:if test="$REINSTATE_MODE"><xsl:value-of select="'Re-Instate Order'"/></xsl:if>
    </xsl:with-param>
    <xsl:with-param name="showExitButton" select="true()"/>
    <xsl:with-param name="showTime" select="true()"/>
  </xsl:call-template>
</xsl:template>

<!-- Naviagtion Links -->
<xsl:template name="addNavLinks">
  <xsl:if test="$SCRUB_MODE">
    <a class="link" href="javascript:doReturnToSearchAction();">Order Scrub Search</a>
  </xsl:if>
  <xsl:if test="$PENDING_MODE">
    <a class="link" href="javascript:doReturnToSearchAction();">Pending Order Search</a>
    &nbsp;:&nbsp;
    <a class="link" href="javascript:doReturnToResultListAction();">Back to Search Results</a>
  </xsl:if>
  <xsl:if test="$REINSTATE_MODE">
    <a class="link" href="javascript:doReturnToSearchAction();">Re-Instate Order Search</a>
    &nbsp;:&nbsp;
    <a class="link" href="javascript:doReturnToResultListAction();">Back to Search Results</a>
  </xsl:if>
</xsl:template>

<!-- Action Buttons -->
<xsl:template name="addActionButtons">
  <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
    <tr>
      <td width="10%" class="label">
        <input type="checkbox" name="fraud_flag" tabindex="-1" onclick="javascript:doFraudPopup(this);"/>
        Fraud
      </td>
      <td width="80%" align="center">
        <input name="submitCart" type="button" value="Submit Shopping Cart" tabindex="29" onclick="javascript:doSubmitOrderAction();">
            <xsl:if test="($isWalmartOrder or $isAmazonOrder or $isMercentOrder or $isCspiOrder or $isPartnerOrder or $isRosesDotCom) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
   		</input>&nbsp;
        <xsl:if test="$SCRUB_MODE or $PENDING_MODE or $REINSTATE_MODE">
          <input name="pendingCart" type="button" value="Pending" tabindex="30" onclick="javascript:doCartAction('PendingOrderServlet');">
            <xsl:if test="($isWalmartOrder or $isAmazonOrder  or $isMercentOrder or $isCspiOrder or $isPartnerOrder or $isRosesDotCom) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
          </input>&nbsp;
          <input name="removeCart" type="button" value="Remove" tabindex="31" onclick="javascript:doCartAction('RemoveOrderServlet');">
            <xsl:if test="($isWalmartOrder or $isAmazonOrder  or $isMercentOrder or $isCspiOrder or $isPartnerOrder or $isRosesDotCom) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
          </input>&nbsp;
          <input name="nextOrderCart" type="button" value="Next Order" tabindex="32" onclick="javascript:doNextOrder();"/>
        </xsl:if>
      </td>
      <td width="10%" align="right">
        <input name="exitButton" type="button" value="Exit" tabindex="33" onclick="javascript:doExitAction();"/>
      </td>
    </tr>
    <xsl:if test="key('validationField','novator_fraud')/messages/message">
        <tr>
	    <td width="100%" class="errorMessage" colspan="10">
	    <xsl:for-each select="key('validationField','novator_fraud')/messages/message">
	      <xsl:value-of select="description"/><br/>
	    </xsl:for-each>
	    </td>
	  </tr>
	  </xsl:if>
  </table>
</xsl:template>

</xsl:stylesheet>   