<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" indent="yes"/>

<xsl:template name="itemDiv">

  <xsl:for-each select="order/items/item">
    <xsl:sort select="status_order" data-type="number"/>

    <xsl:variable name="showFlag">
      <!-- Invalid items -->
      <xsl:if test="item_status=$invalidItem or item_status=$validItemInvalidHeader">
        <xsl:value-of select="'false'"/>
      </xsl:if>

      <!-- Pending items -->
      <xsl:if test="$PENDING_MODE and item_status=$pendingItem">
        <xsl:value-of select="'true'"/>
      </xsl:if>

      <!-- Removed and Abaonded items -->
      <xsl:if test="$REINSTATE_MODE and item_status=$removedItem or item_status=$abandonedItem">
        <xsl:value-of select="'true'"/>
      </xsl:if>
    </xsl:variable>

    <xsl:call-template name="addItemDiv">
      <xsl:with-param name="node" select="."/>
      <xsl:with-param name="showViewComments" select="$showFlag"/>
    </xsl:call-template>

  </xsl:for-each>
</xsl:template>

<xsl:template name="addItemDiv">
  <xsl:param name="showViewComments"/>
  <xsl:variable name="lineNumber" select="line_number"/>
  <xsl:variable name="errorNode" select="/root/validation/order/items/item[@line_number=$lineNumber]"/>
  <xsl:variable name="externalOrderNum" select="order_number"/>
  <xsl:variable name="orderDetailId" select="order_detail_id"/>
  <xsl:variable name="anyPreferredPartners" select="boolean(count(/root/order/header/preferred_partners/preferred_partner) > 0)"/>
  <xsl:variable name="showSurchargeExplanation" select="/root/order/surcharge/show_explanation"/>
  <xsl:variable name="recipientCountry" select = "recip_country"/>
  <xsl:variable name="order_date" select="/root/order/header/order_date"/>
	
  <script type="text/javascript" language="javascript"><![CDATA[
    document.write('<div id="item]]><xsl:value-of select="order_number"/><![CDATA[Div" align="center">');]]>
  </script>
  
  
  <input type="hidden" name="productid{$lineNumber}" value="{productid}"/>
  <input type="hidden" name="item_source_code{$lineNumber}" value="{item_source_code}"/>
  <input type="hidden" name="item_source_description{$lineNumber}" value="{item_source_description}"/>
  <input type="hidden" name="item_service_fee{$lineNumber}" value="{service_fee}"/>
  <input type="hidden" name="item_drop_ship_charges{$lineNumber}" value="{drop_ship_charges}"/>
  <input type="hidden" name="iow_flag{$lineNumber}" value="{iow_flag}"/>
  <input type="hidden" name="item_order_number{$lineNumber}" value="{order_number}"/>
  <input type="hidden" name="item_delivery_date{$lineNumber}" value="{delivery_date}"/>
  <input type="hidden" name="item_delivery_date_range_end{$lineNumber}" value="{second_delivery_date}"/>
  <input type="hidden" name="reward_type{$lineNumber}" value="{reward_type}"/>
  <input type="hidden" name="recip_add_too_long_flag{$lineNumber}" value="{boolean(/root/validation/order/items/item[@line_number=$lineNumber]/data[@field_name=concat('recip_address1', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH')}"/>
  <input type="hidden" name="discount_type{$lineNumber}">
    <xsl:attribute name="value">
      <xsl:if test="discount_type='D'">Dollar</xsl:if>
      <xsl:if test="discount_type='P'">Percent</xsl:if>
      <xsl:if test="discount_type='M'">Miles</xsl:if>
    </xsl:attribute>
  </input>
  <xsl:for-each select="add_ons/add_on">
    <input type="hidden" name="addonOrig_{$lineNumber}_{code}" value="{quantity}"/>
  </xsl:for-each>
  <input type="hidden" name="bin_source_changed_flag{$lineNumber}" value="{bin_source_changed_flag}"/>
  <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
      <td>

        <!-- Order Information -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="6">Order Detail for <xsl:value-of select="order_number"/></td>
          </tr>
          <tr>
            <xsl:call-template name="addCustomerInfo">
              <xsl:with-param name="itemNode" select="."/>
              <xsl:with-param name="showViewComments" select="$showViewComments"/>
            </xsl:call-template>
          </tr>
          <tr>
          	<td class="screenText" colspan="2"><span class="label">Source Code:</span>&nbsp; &nbsp; &nbsp;
          	<xsl:value-of select="item_source_code"/> - <xsl:value-of select="item_source_description"/>
                <xsl:if test="premier_collection_flag = 'Y'">
                   &nbsp;&nbsp;&nbsp;<img border="0" src="../images/luxury.jpg" align="absmiddle"/>
                </xsl:if>
          	<xsl:if test="iow_flag = 'Y' and not($isAmazonOrder) and not($isWalmartOrder) and not($isUnitedOrder) and not($isMercentOrder) and not($isPartnerOrder) and not($isRosesDotCom)">
          	  &nbsp; &nbsp; <a id="sourceCodeLink" class="link" href="#" onclick="javascript:doItemSourceCodeLookup('{$lineNumber}');">Change Source Code</a>
          	</xsl:if>
          	
          	
          	<script type="text/javascript" language="javascript">
			  
			  	function formatDate(date) {
			  		//sample: Mon Jul 18 19:25:52 CDT 2016
			  		//required: Monday Jul 18 2016 7:25:52 PM  CDT 
			  		
			  		
			  		
					var dArray = date.split(" ");
					
					var dayFull = dArray[0] == "Mon" ? "Monday" :(
			  					  dArray[0] == "Tue" ? "Tuesday" : (
			  					  dArray[0] == "Wed" ? "Wednesday" : (
			  					  dArray[0] == "Thu" ? "Thursday" : (
			  					  dArray[0] == "Fri" ? "Friday" : (
			  					  dArray[0] == "Sat" ? "Saturday" : (
			  					  dArray[0] == "Sun" ? "Sunday" : ""
			  					  ))))));
			  					  
					var dow = dayFull;
					var month = dArray[1];
					var date = dArray[2]; 
					var time = dArray[3];
					var year = dArray[5];
					
					var tArray = time.split(":");
					var hours = tArray[0];
					var minutes = tArray[1];
					var seconds = tArray[2];
					time = formatAMPM(hours, minutes, seconds);
					
					document.getElementById("ot").innerHTML = dow+' '+month+' '+date+', '+year+' '+time;
				}
				function formatAMPM(hours, minutes, seconds) {
                  
                  var ampm = hours >= 12 ? 'PM' : 'AM';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  var strTime = hours + ':' + minutes + ':' + seconds + ' ' + ampm;
                  return strTime;
                }
				
				
		    </script>
  
          	
          	</td>
          	<td class="screenText" colspan="2"><span class="label">Order Date:</span>&nbsp; &nbsp; &nbsp;
			    <span id="ot"></span>
			    <script type="text/javascript">formatDate('<xsl:value-of select="$order_date"/>');</script>
          	</td>
			
          </tr>
          <xsl:if test="$errorNode/data[@field_name=concat('item_source_code', $lineNumber)]">
                  <tr>
				    <td colspan="5" class="errorMessage">
				      <xsl:for-each select="$errorNode/data[@field_name=concat('item_source_code', $lineNumber)]/messages/message">
				        <xsl:value-of select="description"/><br/>
				      </xsl:for-each>
				    </td>
				  </tr>
          </xsl:if>
        </table>

        <!-- Recipient Information -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Recipient Information</td>
          </tr>
          <tr>
            <td width="50%" valign="top">
              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                <tr>
                  <td width="15%" class="label">First Name:</td>
                  <td width="35%"><input type="text" name="recip_first_name{$lineNumber}" tabindex="1" size="30" maxlength="20" value="{recip_first_name}"/></td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_first_name', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_first_name', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">Last Name:</td>
                  <td width="35%"><input type="text" name="recip_last_name{$lineNumber}" tabindex="2" size="30" maxlength="20" value="{recip_last_name}"/></td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_last_name', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_last_name', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">Delivery Location:</td>
                  <td width="35%">
                    <select name="ship_to_type{$lineNumber}" tabindex="3" onchange="javascript:doUpdateShipMethodAction(this, '{$lineNumber}');">
                      <xsl:variable name="shipType" select="ship_to_type"/>
                      <xsl:for-each select="/root/addressTypeList/addressType">
                        <option value="{address_type}">
                          <xsl:if test="address_type=$shipType"><xsl:attribute name="SELECTED"/></xsl:if>
                          <xsl:value-of select="description"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('ship_to_type', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('ship_to_type', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">Business Name:</td>
                  <td width="35%"><input type="text" name="ship_to_type_name{$lineNumber}" tabindex="4" size="30" maxlength="30" value="{ship_to_type_name}"/></td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('ship_to_type_name', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('ship_to_type_name', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">Address 1:</td>
                  <xsl:choose>
	              <xsl:when test="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH' or $errorNode/data[@field_name=concat('recip_address2', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH'">
	                <td width="35%" class="label"><input type="text" name="recip_address1{$lineNumber}" tabindex="5" size="30" maxlength="30" value=""  onchange="javascript:setAddressChangedFlag()"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td width="35%" class="label"><input type="text" name="recip_address1{$lineNumber}" tabindex="5" size="30" maxlength="30" value="{recip_address1}"  onchange="javascript:setAddressChangedFlag()"/></td>
	              </xsl:otherwise>
	              </xsl:choose>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]">
                	<!--
                	  <xsl:call-template name="errorMessage">
                        <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]"/>
                      </xsl:call-template>
                    -->
                    <xsl:choose>
	              		<xsl:when test="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH' or $errorNode/data[@field_name=concat('recip_address2', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH'" />
	              		<xsl:otherwise>
	                		<xsl:call-template name="errorMessage">
                        		<xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]"/>
                      		</xsl:call-template>
	              		</xsl:otherwise>
	              	</xsl:choose>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">Address 2:</td>
                  <xsl:choose>
	              <xsl:when test="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH' or $errorNode/data[@field_name=concat('recip_address2', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH'">
	                <td width="35%" class="label"><input type="text" name="recip_address2{$lineNumber}" tabindex="6" size="30" maxlength="30" value=""  onchange="javascript:setAddressChangedFlag()"/></td>
	              </xsl:when>
	              <xsl:otherwise>
	                <td width="35%" class="label"><input type="text" name="recip_address2{$lineNumber}" tabindex="6" size="30" maxlength="30" value="{recip_address2}"  onchange="javascript:setAddressChangedFlag()"/></td>
	              </xsl:otherwise>
	              </xsl:choose>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_address2', $lineNumber)]">
                	<!--
                	  <xsl:call-template name="errorMessage">
                        <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_address2', $lineNumber)]"/>
                      </xsl:call-template>
                    -->
                    <xsl:choose>
	              		<xsl:when test="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH' or $errorNode/data[@field_name=concat('recip_address2', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH'" />
	              		<xsl:otherwise>
	                		<xsl:call-template name="errorMessage">
                        		<xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_address2', $lineNumber)]"/>
                      		</xsl:call-template>
	              		</xsl:otherwise>
	              	</xsl:choose>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">City, State:</td>
                  <td width="35%" class="label">
                    <input type="text" name="recip_city{$lineNumber}" tabindex="7" size="15" maxlength="30" value="{recip_city}"  onchange="javascript:setAddressChangedFlag()"/>,&nbsp;
                    <select name="recip_state{$lineNumber}" tabindex="8" onchange="javascript:changeRecipState('recip_state{$lineNumber}');" ></select>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_city', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_city', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_state', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_state', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="15%" class="label">Zip/Postal Code:<br/><a tabindex="10" id="cityLookupLink{$lineNumber}" href="#" onclick="javascript:doCityLookup('{$lineNumber}');">Lookup By City</a></td>
                  <td width="35%" class="label">
                    <input type="text" name="recip_postal_code{$lineNumber}" tabindex="9" size="6" maxlength="12" value="{recip_postal_code}" onchange="javascript:setAddressChangedFlag()"/>
                    &nbsp;Country:&nbsp;
                    <select name="recip_country{$lineNumber}" tabindex="11" onchange="javascript:changeRecipCountry('recip_country{$lineNumber}', 'recip_state{$lineNumber}');">
                      <option value="0">-select a country-</option>
                      <xsl:for-each select="/root/countryList/country">
                        <option value="{country_id}"><xsl:value-of select="name"/></option>
                      </xsl:for-each>
                    </select>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_postal_code', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_postal_code', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_country', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_country', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="15%"></td>
                  <td width="35%" class="label">
                    <input type="checkbox" name="qms_override_flag{$lineNumber}">
                      <xsl:if test="qms_override_flag='Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                    </input>
                    Override Address Verification
                  </td>
                </tr>
              </table>
            </td>
			<td valign="top">
				<xsl:if test="$isPremierCircleMember">
						<img src="../images/ftd_premier_circle_logo.gif" /> 
				</xsl:if>
			</td>			
            <xsl:choose>
            <xsl:when test="$anyPreferredPartners">
            <td valign="top">
                <xsl:for-each select="/root/order/header/preferred_partner_names/preferred_partner">
                  <img src="../images/{.}_logo_large.gif"/><br/>
                </xsl:for-each>
				<xsl:if test="$isJointCart">
					<br/><img src="../images/partner_jointcart.gif"/>
				</xsl:if>
            </td>
            </xsl:when>
			<xsl:when test="$isAmazonOrder">
				<td valign="top">	
					<img src="../images/amazon_logo_large.gif" />
				</td>
			</xsl:when>   
			<xsl:when test="$isMercentOrder">
				<td valign="top">	
					<img src="{$mercentIcon}" />
				</td>
			</xsl:when>  
			<xsl:when test="$isPartnerOrder">
					<td valign="top">	
						<img src="{$partnerImgUrl}" />
					</td>
			</xsl:when>             			           
            <xsl:otherwise>
            <td></td>
            </xsl:otherwise>
            </xsl:choose>
            
            <td width="50%" valign="top">
              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                <tr>
                  <td width="15%" class="label">Recipient Phone:</td>
                  <td width="35%">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="35%"><input type="text" name="recip_phone{$lineNumber}" tabindex="12" size="15" maxlength="20" value="{recip_phone}"/></td>
                        <td class="label">
                          &nbsp;Ext:&nbsp;<input type="text" name="recip_phone_ext{$lineNumber}" tabindex="13" size="6" maxlength="10" value="{recip_phone_ext}"/>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_phone', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_phone', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <xsl:if test="$errorNode/data[@field_name=concat('recip_phone_ext', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('recip_phone_ext', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                <td height="20"></td>
                </tr>
                <tr>
                  <xsl:if test="$errorNode/data[@field_name=concat('recip_address1', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH' or $errorNode/data[@field_name=concat('recip_address2', $lineNumber)]/messages/message/type = 'VALIDATION_ERROR_TYPE_LENGTH'">
                    <tr>
                      <td width="30%" class="errorMessage">Address too long</td>
                      <td width="70%"><xsl:value-of select="recip_address1"/>&nbsp;<xsl:value-of select="recip_address2"/></td>
                    </tr>
                  </xsl:if>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <!-- Enclosure and Other Miscellaneous -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader">Enclosure Card</td>
            <td class="tblheader">Miscellaneous</td>
          </tr>
          <tr>
            <td width="50%" valign="top">
              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                <tr>
                  <td width="30%" class="label">Occasion:</td>
                  <td width="70%">
                    <select name="occasion{$lineNumber}" tabindex="15">
                      <xsl:variable name="occasionId" select="occasion"/>
                      <xsl:for-each select="/root/occasionList/occasion">
                        <option value="{occasionid}">
                          <xsl:if test="occasionid=$occasionId"><xsl:attribute name="SELECTED"/></xsl:if>
                          <xsl:value-of select="description"/>
                        </option>
                      </xsl:for-each>
                    </select>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('occasion', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('occasion', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="30%" class="label" valign="top">Gift Card Message:</td>
                  <td width="70%" rowspan="2">
                    <textarea name="card_message{$lineNumber}" tabindex="16" cols="30" rows="3" onkeypress="javascript:limitTextarea(this, 230);"><xsl:value-of select="normalize-space(card_message)"/></textarea>
                  </td>
                </tr>
                <tr>
                  <td width="30%">
                    <input type="button" name="spellcheckButton{$lineNumber}" tabindex="17" value="Spell Check" onclick="spellCheck(card_message{$lineNumber}.value, 'card_message{$lineNumber}');"/>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('card_message', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('card_message', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <td width="30%" class="label">Signature Line:</td>
                  <td width="70%"><input type="text" name="card_signature{$lineNumber}" tabindex="18" size="30" maxlength="20" value="{card_signature}"/></td>
                </tr>
              </table>
            </td>
            <td width="50%" valign="middle">
              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                <tr>
                  <td width="30%" class="label" valign="top">Special Instructions:</td>
                  <td width="70%">
                    <textarea name="special_instructions{$lineNumber}" tabindex="19" cols="30" rows="4" onkeypress="javascript:limitTextarea(this, 300);"><xsl:value-of select="normalize-space(special_instructions)"/></textarea>
                  </td>
                </tr>
                <xsl:if test="$errorNode/data[@field_name=concat('special_instructions', $lineNumber)]">
                  <xsl:call-template name="errorMessage">
                    <xsl:with-param name="node" select="$errorNode/data[@field_name=concat('special_instructions', $lineNumber)]"/>
                  </xsl:call-template>
                </xsl:if>
                <tr>
                  <xsl:if test="/root/product_data[line_number=$lineNumber]/@floristDeliveryFlag='Y' and /root/product_data[line_number=$lineNumber]/@domesticFlag='Y'">
                  <td width="30%" class="label">Florist:</td>
                  <td width="70%">
                    <span id="floristDisplay{$lineNumber}">&nbsp;<xsl:value-of select="florist_name"/></span>
                    &nbsp;&nbsp;<a class="link" tabindex="21" href="#" onclick="javascript:doFloristLookup('{$lineNumber}');">Florist Lookup</a>
                    <input type="hidden" name="florist_name{$lineNumber}" value="{florist_name}"/>
                    <input type="hidden" name="florist_number{$lineNumber}" value="{florist_number}"/>
                  </td>
                  </xsl:if>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <!-- Product Summary -->
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#006699">
          <tr>
            <td class="tblheader" colspan="8">Product Summary</td>
          </tr>
          <tr>
            <td width="12%" align="right" class="label">Delivery Date:</td>
            
 
            <!-- Nov 2006 - added this choose statement around the delivery date input textbox, so that personalization 
                 delivery dates cannot be modified; the "when" case is new and the "otherwise" case is the original code
                 that used to run unconditionally -->
            <xsl:choose>
               <xsl:when test="personalizations != ''">
                    <td width="12%">
                       <input type="hidden" name="delivery_date{$lineNumber}" value="{delivery_date}"  onchange="javascript:doUpdateShipMethodAction(this, '{$lineNumber}');"/>
                       <xsl:value-of select="delivery_date"/>
                    </td>  
               </xsl:when>
               <xsl:when test="product_sub_type='FREESHIP'">
                    <td width="12%">
                       <input type="hidden" name="delivery_date{$lineNumber}" value="{delivery_date}"  onchange="javascript:doUpdateShipMethodAction(this, '{$lineNumber}');"/>
                       <xsl:value-of select="delivery_date"/>
                    </td>  
               </xsl:when>
               <xsl:otherwise>
                  <td width="12%"><input type="text" name="delivery_date{$lineNumber}" tabindex="22" size="15" maxlength="10" value="{delivery_date}"  onchange="javascript:doUpdateShipMethodAction(this, '{$lineNumber}');"/></td>
               </xsl:otherwise>
            </xsl:choose>

	<!-- #11946 - BBN/ Morning Delivery flag -->
	<xsl:choose>
	
		 <xsl:when test="$isPartnerOrder">
		 	<td width="12%" align="right" class="label"> Morning Delivery: </td>
		 	<td width="12%">
				<input type="checkbox" name="morning_delivery{$lineNumber}">
					<xsl:choose>
			 			<xsl:when test="orig_order_has_mdf='Y' and morning_delivery_unavailable='N'">				
								<xsl:attribute name="CHECKED" />						
						</xsl:when>
						<xsl:otherwise>
								<xsl:attribute name="disabled">true</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>	
				</input>
			</td> 
		 </xsl:when>
		 
		<xsl:otherwise>
			<xsl:choose>		
				<xsl:when test="morning_delivery_unavailable='Y'">
					<td width="12%" align="right" class="label">
						Morning Delivery:
					</td>
					<td width="12%">
						<input type="checkbox" name="morning_delivery{$lineNumber}">
							<xsl:attribute name="disabled">true</xsl:attribute>
						</input>
					</td>
				</xsl:when>
				<xsl:otherwise>
					<td width="12%" align="right" class="label">
						Morning Delivery:				
					</td>
					<td width="12%">
						<input type="checkbox" name="morning_delivery{$lineNumber}">
							<xsl:if test="morning_delivery_flag='Y'">
								<xsl:attribute name="CHECKED" />
							</xsl:if>
						</input>
					</td>
				</xsl:otherwise>
			</xsl:choose>
    	</xsl:otherwise>
    	
   </xsl:choose>        
              
    
              <xsl:choose>
                <xsl:when test="/root/product_data[line_number=$lineNumber and @carrierDeliveryFlag='Y']">
                  <td width="12%" align="right" class="label">Ship Method:</td>
                  <td width="12%">
                    <select name="shipping_method{$lineNumber}" tabindex="23" onfocus="javascript:shipMethodSave(this);" onchange="javascript:doUpdateShipMethodAction(this, '{$lineNumber}');">
                    <option value=""></option>
                    <xsl:variable name="shippingMethod" select="shipping_method"/>
                    <xsl:for-each select="/root/product_data[line_number=$lineNumber]/shipMethodList/shipMethod">
                      <option value="{ship_method_id}">
                        <xsl:if test="ship_method_id=$shippingMethod"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="description"/>
                      </option>
                    </xsl:for-each>
                    </select>
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td width="12%">&nbsp;</td>
                  <td width="12%">&nbsp;</td>
                </xsl:otherwise>
              </xsl:choose>
            <xsl:choose>
              <xsl:when test="/root/product_data[line_number=$lineNumber]/@carrierDeliveryFlag='Y'">
                <td width="12%" align="right" class="label">Ship Date:</td>
                <xsl:if test="shipping_system != 'FTD WEST' ">
                <td width="12%" class="screenText"><xsl:value-of select="ship_date"/></td>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <td width="12%"></td>
                <td width="12%"></td>
              </xsl:otherwise>
            </xsl:choose>
          </tr>
          <tr>
            <td width="12%"></td>
            <td width="12%" colspan="1" class="errorMessage">
            <xsl:for-each select="$errorNode/data[@field_name=concat('delivery_date', $lineNumber)]/messages/message">
              <xsl:value-of select="description"/><br/>
            </xsl:for-each>
            </td>
            <td width="12%"></td>
            <td width="12%"></td>
            <td width="12%"></td>
            <td width="12%" colspan="1" class="errorMessage">
            <xsl:for-each select="$errorNode/data[@field_name=concat('shipping_method', $lineNumber)]/messages/message">
              <xsl:value-of select="description"/><br/>
            </xsl:for-each>
            </td>
            <td width="12%"></td>
            <td width="12%" colspan="1" class="errorMessage">
            <xsl:for-each select="$errorNode/data[@field_name=concat('ship_date', $lineNumber)]/messages/message">
              <xsl:value-of select="description"/><br/>
            </xsl:for-each>
            </td>
          </tr>
        </table>

      <xsl:if test="personalizations != ''">
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#006699">
          <tr>
            <td class="tblheader" colspan="6">Personalization</td>
          </tr>
          <tr>
            <td class="screenText" colspan="6">
              <xsl:choose>
                <xsl:when test="pdp_personalizations != ''"><xsl:value-of select="pdp_personalizations"/></xsl:when>
                <xsl:otherwise><xsl:for-each select="personalizations/personalization"><xsl:value-of select="name"/>=<xsl:value-of select="data"/>;</xsl:for-each></xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </xsl:if>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" bordercolor="#006699">
          <tr>
            <td class="colHeaderCenter">Product</td>
            <td width="15%" class="colHeaderCenter">
              Price
              <xsl:if test="reward_type!=''">
                <xsl:if test="reward_type!='TotSavings'">
                  (<xsl:value-of select="reward_type"/>)
                </xsl:if>
              </xsl:if>
              <xsl:if test="discount_type='D'">(Dollar)</xsl:if>
              <xsl:if test="discount_type='P'">(Percent)</xsl:if>
              <xsl:if test="discount_type='M'">(Miles)</xsl:if>
            </td>
            <td width="12%" class="colHeaderCenter">First Choice</td>
            <td width="13%" class="colHeaderCenter">Second Choice</td>
          </tr>
          <xsl:if test="$errorNode/data[@field_name=concat('product_price', $lineNumber)] or $errorNode/data[@field_name=concat('productid', $lineNumber)] or 
                         $errorNode/data[@field_name=concat('item_quantity', $lineNumber)] or $errorNode/data[@field_name=concat('first_color_choice', $lineNumber)] or 
                         $errorNode/data[@field_name=concat('second_color_choice', $lineNumber)] or $errorNode/data[@field_name=concat('product_addon', $lineNumber)]">
            <tr>
              <td align="center" class="errorMessage">
                <xsl:if test="$errorNode/data[@field_name=concat('productid', $lineNumber)]">
                  <xsl:for-each select="$errorNode/data[@field_name=concat('productid', $lineNumber)]/messages/message">
                    <xsl:value-of select="description"/><br/>
                  </xsl:for-each>
                </xsl:if>
                <xsl:if test="$errorNode/data[@field_name=concat('product_addon', $lineNumber)]">
                  <xsl:for-each select="$errorNode/data[@field_name=concat('product_addon', $lineNumber)]/messages/message">
                    <xsl:value-of select="description"/><br/>
                  </xsl:for-each>
                </xsl:if>
              </td>
              <xsl:if test="$isAmazonOrder or $isMercentOrder or $isCspiOrder or $isPartnerOrder">
                <td width="20%" align="center" class="errorMessage">
                  <xsl:if test="$errorNode/data[@field_name=concat('item_quantity', $lineNumber)]">
                    <xsl:for-each select="$errorNode/data[@field_name=concat('item_quantity', $lineNumber)]/messages/message">
                      <xsl:value-of select="description"/><br/>
                    </xsl:for-each>
                  </xsl:if>
                </td>
              </xsl:if>
              <td width="20%" align="center" class="errorMessage">
                <xsl:if test="$errorNode/data[@field_name=concat('product_price', $lineNumber)]">
                  <xsl:for-each select="$errorNode/data[@field_name=concat('product_price', $lineNumber)]/messages/message">
                    <xsl:value-of select="description"/><br/>
                  </xsl:for-each>
                </xsl:if>
              </td>
              <td width="20%" align="center" class="errorMessage">
                <xsl:if test="$errorNode/data[@field_name=concat('first_color_choice', $lineNumber)]">
                  <xsl:for-each select="$errorNode/data[@field_name=concat('first_color_choice', $lineNumber)]/messages/message">
                    <xsl:value-of select="description"/><br/>
                  </xsl:for-each>
                </xsl:if>
                <xsl:if test="$errorNode/data[@field_name=concat('second_color_choice', $lineNumber)]">
                  <xsl:for-each select="$errorNode/data[@field_name=concat('second_color_choice', $lineNumber)]/messages/message">
                    <xsl:value-of select="description"/><br/>
                  </xsl:for-each>
                </xsl:if>
              </td>
            </tr>
            <xsl:if test="$isAmazonOrder or $isMercentOrder or $isWalmartOrder  or $isPartnerOrder">
               <tr>
                <td align="left" class="label">
                  <input type="checkbox" name="price_override_flag{$lineNumber}">
                    <xsl:if test="price_override_flag='Y'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>
                    Override Price Differential
                </td>
                <td width="20%" align="center">
                </td>
                <td width="20%" align="center">
                </td>
               </tr>
             </xsl:if>
          </xsl:if>
          
          <xsl:if test="$isWalmartOrder">
	          <tr>
	            <td class="errorMessage" colspan="6">This is a Wal-Mart order. The product and shipping method may not be changed.</td>
	          </tr>
          </xsl:if>

          <!-- Show product detail information -->
          <tr>
            <td>
              Item:&nbsp;
              <xsl:choose>
                <xsl:when test="subcodeid!=''"><xsl:value-of select="subcodeid"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="productid"/></xsl:otherwise>
              </xsl:choose>
              &nbsp;&nbsp;<xsl:value-of disable-output-escaping="yes" select="product_description"/>,&nbsp;quantity:&nbsp;<xsl:value-of select="quantity"/>
            </td>
            <td align="right">
              <span id="productPrice">
                <xsl:if test="(discount_type='D' or discount_type='P') and string-length(discount_amount) != 0 and discount_amount != '' and discount_amount &gt; 0"><xsl:attribute name="class">discountPrice</xsl:attribute></xsl:if>
                $<xsl:value-of select="format-number(product_price,'#,###,##0.00')"/>
              </span>
              <xsl:if test="discount_type='D' and string-length(discount_amount) != 0 and discount_amount != '' and discount_amount &gt; 0">&nbsp;&nbsp;$<xsl:value-of select="format-number(discounted_product_price,'#,###,##0.00')"/>&nbsp;&nbsp;($<xsl:value-of select="format-number(discount_amount,'#,###,##0.00')"/>)</xsl:if>
              <xsl:if test="discount_type='P' and string-length(discount_amount) != 0 and discount_amount != '' and discount_amount &gt; 0">&nbsp;&nbsp;$<xsl:value-of select="format-number(discounted_product_price,'#,###,##0.00')"/>&nbsp;&nbsp;(<xsl:value-of select="percent_off"/>%)</xsl:if>
              <xsl:if test="discount_type='M'">&nbsp;&nbsp;(<xsl:value-of select="miles_points"/>)</xsl:if>
              <xsl:if test="reward_type='Miles' or reward_type='Points'">&nbsp;&nbsp;(<xsl:value-of select="miles_points"/>)</xsl:if>
              <xsl:if test="reward_type='Cash Back' or reward_type='Charity'">&nbsp;&nbsp;($<xsl:value-of select="format-number(miles_points,'#,###,##0.00')"/>)</xsl:if>
            </td>
            <td align="center"><xsl:value-of select="first_color_choice"/></td>
            <td align="center"><xsl:value-of select="second_color_choice"/></td>
          </tr>

          <!-- Show all Vase Add-ons first -->
          <xsl:for-each select="add_ons/add_on[type = '7']">
            <tr>
              <td>&nbsp;&nbsp;Vase:&nbsp;<xsl:value-of disable-output-escaping="yes" select="description"/>,&nbsp;quantity:&nbsp;<xsl:value-of select="quantity"/></td>
              <td align="right">
              <xsl:choose>
	              <xsl:when test="string-length(discountAmount) != 0 and discountAmount != '' and discountAmount &gt; 0">
		              	<span id="addOnPrice">
		              		<xsl:attribute name="class">discountPrice</xsl:attribute>
		              		$<xsl:value-of select="format-number((price*quantity + discountAmount*quantity), '#,###,##0.00')"/>
		              	</span>
		              	&nbsp;&nbsp;$<xsl:value-of select="format-number(price*quantity, '#,###,##0.00')"/>
		              	&nbsp;&nbsp;($<xsl:value-of select="format-number(discountAmount*quantity, '#,###,##0.00')"/>)
		           </xsl:when>
		           <xsl:otherwise>
		              		$<xsl:value-of select="format-number(price*quantity, '#,###,##0.00')"/>
		           </xsl:otherwise>
	           </xsl:choose>
              </td>
              <td colspan="2"></td>
            </tr>
          </xsl:for-each>

          <!-- Show all remaining Add-ons -->
          <xsl:for-each select="add_ons/add_on[type != '7']">
            <tr>
              <td>&nbsp;&nbsp;Add On:&nbsp;<xsl:value-of disable-output-escaping="yes" select="description"/>,&nbsp;quantity:&nbsp;<xsl:value-of select="quantity"/></td>
              <td align="right">
              	<xsl:choose>
	              <xsl:when test="string-length(discountAmount) != 0 and discountAmount != '' and discountAmount &gt; 0">
		              	<span id="addOnPrice">
		              		<xsl:attribute name="class">discountPrice</xsl:attribute>
		              		$<xsl:value-of select="format-number((price*quantity + discountAmount*quantity), '#,###,##0.00')"/>
		              	</span>
		              	&nbsp;&nbsp;$<xsl:value-of select="format-number(price*quantity, '#,###,##0.00')"/>
		              	&nbsp;&nbsp;($<xsl:value-of select="format-number(discountAmount*quantity, '#,###,##0.00')"/>)
		           </xsl:when>
		           <xsl:otherwise>
		              		$<xsl:value-of select="format-number((price*quantity), '#,###,##0.00')"/>
		           </xsl:otherwise>
	           </xsl:choose>
              </td>
              <td colspan="2"></td>
            </tr>
          </xsl:for-each>


          <tr>
          
      <xsl:choose>
      	<!-- When Morning Delivery included, show Service Fee text for FC and SDFC and Shipping Fee text for SPEGFT and SDG irrespective of Ship Method -->
		<xsl:when test="morning_delivery_unavailable='N'">
			<xsl:if test="product_type='FRECUT' or product_type='SDFC' ">
				<td>&nbsp;&nbsp;Service
					Fee 
				</td>
			</xsl:if>
	
			<xsl:if test="product_type='SPEGFT' or product_type='SDG' ">
				<td>&nbsp;&nbsp;Shipping
					Fee
				</td>
			</xsl:if>
		</xsl:when>
		<!-- When Morning Delivery is unavailable, existing code is used -->
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="shipping_method = 'SD' or shipping_method = '' ">
					<td>&nbsp;&nbsp;Service	Fee
					</td>
				</xsl:when>
				<xsl:otherwise>
					<td>&nbsp;&nbsp;Shipping Fee
					</td>
				</xsl:otherwise>
			</xsl:choose>		
		</xsl:otherwise>
	</xsl:choose>	

            <div id = "applySurchageCode" style="display: none"><xsl:value-of select="apply_surchage_code"/></div>
            <div id = "fuelSurchargeFee_{$orderDetailId}" style="display: none"><xsl:value-of select="format-number(surcharge_fee, '#,###,##0.00')"/></div>
            <div id = "fuelSurchargeDescription" style="display: none"><xsl:value-of select="surcharge_description"/></div>
            <div id = "sameDayUpchargeFee_{$orderDetailId}" style="display: none"><xsl:value-of select="format-number(same_day_upcharge_fee, '#,###,##0.00')"/></div>
            <div id = "morningDeliveryFee_{$orderDetailId}" style="display: none"><xsl:value-of select="format-number(morning_delivery_fee, '#,###,##0.00')"/></div>   
            <div id = "lateCutoff_{$orderDetailId}" style="display: none"><xsl:value-of select="format-number(late_cutoff_fee, '#,###,##0.00')"/></div>
            <div id = "sudayUpcharge_{$orderDetailId}" style="display: none"> <xsl:value-of select="format-number(vendor_sun_upcharge, '#,###,##0.00')"/> </div>
            <div id = "mondayUpcharge_{$orderDetailId}" style="display: none"> <xsl:value-of select="format-number(vendor_mon_upcharge, '#,###,##0.00')"/> </div>   
            <div id = "internationalFee_{$orderDetailId}" style="display: none"><xsl:value-of select="format-number(international_service_fee, '#,###,##0.00')"/></div>           
			<div id = "serviceFee_{$orderDetailId}" style="display: none"> <xsl:value-of select="format-number(service_fee, '#,###,##0.00')"/> </div>
                 
             <td align = "right" >
                <xsl:choose>
                  <xsl:when test="(service_fee + drop_ship_charges) >= surcharge_fee">
	                		 <a class="tooltip2" href="#" style="text-decoration: none; cursor:default; color: black;" onmouseover="javascript:displaySurchargeOnMouseOver({$orderDetailId}, this)" onclick="return false" >
                       <xsl:value-of select="format-number((service_fee + drop_ship_charges), '#,###,##0.00')"/>
                       <span id="spanMouseOver_{$orderDetailId}" ></span></a>
		  </xsl:when>
                  <xsl:otherwise>
                       <xsl:value-of select="format-number((service_fee + drop_ship_charges), '#,###,##0.00')"/>
                  </xsl:otherwise>
                </xsl:choose>
		</td>
		<td colspan="2"></td>
          </tr>


          <!-- Show tax and order total -->
          <xsl:choose>
            <xsl:when test="$isAmazonOrder" >
              <tr>
                <td>&nbsp;&nbsp;Tax</td>
                <td align="right"><u>
                <xsl:call-template name="calculate_tax" >
                  <xsl:with-param name="amazon_tax" select="tax_amount"/>
                  <xsl:with-param name="amazon_ship_tax" select="shipping_tax"/>
                </xsl:call-template> 
                </u></td>
                <td colspan="2"></td>
              </tr>
            </xsl:when>
            <xsl:when test="$isMercentOrder" >
              <tr>
                <td>&nbsp;&nbsp;Tax</td>
                <td align="right"><u>
                <xsl:call-template name="mercent_calculate_tax" >
                  <xsl:with-param name="mercent_tax" select="tax_amount"/>
                  <xsl:with-param name="mercent_ship_tax" select="shipping_tax"/>
                </xsl:call-template> 
                </u></td>
                <td colspan="2"></td>
              </tr>
            </xsl:when>
            <xsl:when test="$isPartnerOrder" >
              <tr>
                <td>&nbsp;&nbsp;Tax</td>
                <td align="right"><u>
                <xsl:call-template name="partner_calculate_tax" >
                  <xsl:with-param name="partner_tax" select="tax_amount"/>
                  <xsl:with-param name="partner_ship_tax" select="shipping_tax"/>
                </xsl:call-template> 
                </u></td>
                <td colspan="2"></td>
              </tr>
            </xsl:when>            
            <xsl:when test="$isRosesDotCom" >
              <xsl:for-each select="taxes/tax">
                <xsl:sort select="display_order"/>
                <xsl:if test="amount>0">
                <tr>
                  <td>&nbsp;&nbsp;Tax</td>
                  <td align="right">
                    <xsl:value-of select="format-number(amount,'#,###,##0.00')"/>
                  </td>
                      <td colspan="2"></td>
                </tr>
                </xsl:if>
              </xsl:for-each>
            </xsl:when>            
            <xsl:otherwise>
              <xsl:for-each select="taxes/tax">
                <xsl:sort select="display_order"/>
                <xsl:if test="amount>0 and display_order>0">
                <tr>
                  <td>&nbsp;&nbsp;<xsl:value-of select="description"></xsl:value-of></td>
                  <td align="right">
                    <xsl:value-of select="format-number(amount,'#,###,##0.00')"/>
                  </td>
                  <xsl:choose>
                    <xsl:when test="$showSurchargeExplanation != '' and $recipientCountry = 'CA'">
                      <td>&nbsp;&nbsp;<a class="link" href="#" onclick="javascript:doDisplayPopup('surcharge_explanation');">*Explanation</a></td>
                      <td>&nbsp;</td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td colspan="2"></td>
                    </xsl:otherwise>
                  </xsl:choose>
                </tr>
                </xsl:if>
              </xsl:for-each>
            </xsl:otherwise>
          </xsl:choose>
          <tr>
            <td align="right" class="totalLineRight">Total Order Amount:
            <xsl:if test="miles_points_amt &gt; 0">
              <BR/>Total Miles/Points:
            </xsl:if>
            </td>
            <td align="right" class="totalLineRight">
              $&nbsp;<xsl:value-of select="format-number(order_total,'#,###,##0.00')"/>
            <xsl:if test="miles_points_amt &gt; 0">
              <BR/><xsl:value-of select="format-number(miles_points_amt, '#,###,###')"/>
            </xsl:if>
            </td>
            <td colspan="2" class="totalLine" align="center">
              <xsl:choose>
              <xsl:when test="$isWalmartOrder" >
              <input type="button" name="editProductButton" value="Edit Product" tabindex="24" onclick="javascript:alert('This is a Wal-Mart order and the product may not be changed.');">
                <xsl:if test="$REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
              </input>
              </xsl:when>
              <xsl:when test="$isRosesDotCom" >
              <input type="button" name="editProductButton" value="Edit Product" tabindex="24" onclick="javascript:rosesDotComAlert();">
              </input>
              </xsl:when>
              <xsl:when test="$isAribaOrder" >
              <input type="button" name="editProductButton" value="Edit Product" tabindex="24" onclick="javascript:doEditProductPopup('{$lineNumber}', '{order_number}');">
              </input>
              </xsl:when>
              <xsl:when test="personalizations != '' and personalizations != 'NONE'">
              	 <input type="button" name="editProductButton" value="Edit Product" tabindex="24" onclick="javascript:alert('This is a PC order and the product cannot be changed.');"/>
              </xsl:when>
              <xsl:otherwise>
                 <input type="button" name="editProductButton" value="Edit Product" tabindex="24" onclick="javascript:doEditProductPopup('{$lineNumber}', '{order_number}');">
                <xsl:if test="($isCspiOrder or $isAmazonOrder or $isMercentOrder  or $isPartnerOrder) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
              </input>
              </xsl:otherwise>
              </xsl:choose>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <!-- Action Buttons -->
  <table width="98%"  align="center" border="0" cellpadding="2" cellspacing="2">
    <tr>
      <td width="10%"></td>
      <xsl:call-template name="addItemActionButtons"/>
      <td width="10%" align="right">
        <input type="button" name="exitButton" tabindex="29" value="Exit" onclick="javascript:doExitAction();"/>
      </td>
    </tr>
  </table>

  <!--The footer-->
  <xsl:call-template name="footer"/>
  <script type="text/javascript" language="javascript"><![CDATA[
    document.write('</div>');]]>
  </script>
</xsl:template>

<!-- Customer Information -->
<xsl:template name="addCustomerInfo">
  <xsl:param name="showViewComments"/>
  <xsl:param name="itemNode"/>
  <xsl:if test="ariba_po_number=''">
    <td width="15%" class="label" nowrap="true">Master Order Number:</td>
    <td width="35%" class="screenText"><xsl:value-of select="/root/order/header/master_order_number"/></td>
    <td width="30%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="30%" class="label">Customer:</td>
          <td width="70%" class="screenText"><xsl:value-of select="/root/order/header/buyer_first_name"/>&nbsp;<xsl:value-of select="/root/order/header/buyer_last_name"/></td>
        </tr>
      </table>
    </td>
  </xsl:if>
  <xsl:if test="ariba_po_number!=''">
    <td width="85%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20%" class="label">Master Order Number:</td>
          <td width="15%" class="screenText"><xsl:value-of select="/root/order/header/master_order_number"/></td>
          <td width="15%" align="center" class="label">Customer:</td>
          <td width="15%" class="screenText"><xsl:value-of select="/root/order/header/buyer_first_name"/>&nbsp;<xsl:value-of select="/root/order/header/buyer_last_name"/></td>
          <td width="15%" align="center" class="label">Ariba PO#:</td>
          <td width="15%" class="screenText"><xsl:value-of select="$itemNode/ariba_po_number"/></td>
        </tr>
      </table>
    </td>
  </xsl:if>
  <xsl:choose>
    <xsl:when test="$showViewComments = 'true'">
      <td align="right"><a class="link" href="#" onclick="javascript:doCommentsPopup('{line_number}');">View Comments</a></td>
    </xsl:when>
    <xsl:otherwise>
      <td width="20%"></td>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Action Buttons -->
<xsl:template name="addItemActionButtons">
  <td width="80%" align="center">
    <input type="button" name="submitItem" tabindex="25" value="Submit Order" onclick="javascript:doSubmitItem('{line_number}');">
        <xsl:if test="($isWalmartOrder or $isAmazonOrder or $isMercentOrder or $isCspiOrder or $isPartnerOrder or $isRosesDotCom or $isAribaOrder) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
    </input>&nbsp;
    <xsl:if test="$SCRUB_MODE or $PENDING_MODE or $REINSTATE_MODE">
      <input type="button" name="pendingItem" tabindex="26" value="Pending" onclick="javascript:doItemAction('PendingOrderServlet', '{order_number}', '{line_number}');">
        <xsl:if test="($isWalmartOrder or $isAmazonOrder or $isMercentOrder or $isCspiOrder or $isPartnerOrder or $isRosesDotCom or $isAribaOrder) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
      </input>&nbsp;
      <input type="button" name="removeItem" tabindex="27" value="Remove" onclick="javascript:doItemAction('RemoveOrderServlet', '{order_number}', '{line_number}');">
        <xsl:if test="($isWalmartOrder or $isAmazonOrder or $isMercentOrder or $isCspiOrder or $isPartnerOrder or $isRosesDotCom or $isAribaOrder) and $REINSTATE_MODE"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
      </input>&nbsp;
      <input type="button" name="nextOrderItem" tabindex="28" value="Next Order" onClick="javascript:doNextOrder();"/>
    </xsl:if>
  </td>
</xsl:template>
 <xsl:template name="calculate_tax">
    <xsl:param name="amazon_ship_tax"/>
    <xsl:param name="amazon_tax"/>
    <xsl:variable name="zero_value" select="'0'"/>
    <xsl:choose>
      <xsl:when test="string-length($amazon_ship_tax) &lt;= '0' and string-length($amazon_tax) &lt;= '0'" >
            <xsl:value-of select="$zero_value"/>
      </xsl:when>
      <xsl:when test="string-length($amazon_ship_tax) &gt; '0' and string-length($amazon_tax) &lt;= '0'">
            <xsl:value-of select="format-number($amazon_ship_tax,'#,###,##0.00')"/>
      </xsl:when>
      <xsl:when test="string-length($amazon_ship_tax) &lt;= '0' and string-length($amazon_tax) &gt; '0'">
            <xsl:value-of select="format-number($amazon_tax,'#,###,##0.00')"/>
      </xsl:when>
      <xsl:when test="string-length($amazon_ship_tax) &gt; '0' and string-length($amazon_tax) &gt; '0'">
            <xsl:value-of select="format-number(($amazon_tax + $amazon_ship_tax),'#,###,##0.00')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$zero_value"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="mercent_calculate_tax">
    <xsl:param name="mercent_ship_tax"/>
    <xsl:param name="mercent_tax"/>
    <xsl:variable name="zero_value" select="'0'"/>
    <xsl:choose>
      <xsl:when test="string-length($mercent_ship_tax) &lt;= '0' and string-length($mercent_tax) &lt;= '0'" >
            <xsl:value-of select="$zero_value"/>
      </xsl:when>
      <xsl:when test="string-length($mercent_ship_tax) &gt; '0' and string-length($mercent_tax) &lt;= '0'">
            <xsl:value-of select="format-number($mercent_ship_tax,'#,###,##0.00')"/>
      </xsl:when>
      <xsl:when test="string-length($mercent_ship_tax) &lt;= '0' and string-length($mercent_tax) &gt; '0'">
            <xsl:value-of select="format-number($mercent_tax,'#,###,##0.00')"/>
      </xsl:when>
      <xsl:when test="string-length($mercent_ship_tax) &gt; '0' and string-length($mercent_tax) &gt; '0'">
            <xsl:value-of select="format-number(($mercent_tax + $mercent_ship_tax),'#,###,##0.00')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$zero_value"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="partner_calculate_tax">
    <xsl:param name="partner_ship_tax"/>
    <xsl:param name="partner_tax"/>
    <xsl:variable name="zero_value" select="'0'"/>
    <xsl:choose>
      <xsl:when test="string-length($partner_ship_tax) &lt;= '0' and string-length($partner_tax) &lt;= '0'" >
            <xsl:value-of select="$zero_value"/>
      </xsl:when>
      <xsl:when test="string-length($partner_ship_tax) &gt; '0' and string-length($partner_tax) &lt;= '0'">
            <xsl:value-of select="format-number($partner_ship_tax,'#,###,##0.00')"/>
      </xsl:when>
      <xsl:when test="string-length($partner_ship_tax) &lt;= '0' and string-length($partner_tax) &gt; '0'">
            <xsl:value-of select="format-number($partner_tax,'#,###,##0.00')"/>
      </xsl:when>
      <xsl:when test="string-length($partner_ship_tax) &gt; '0' and string-length($partner_tax) &gt; '0'">
            <xsl:value-of select="format-number(($partner_tax + $partner_ship_tax),'#,###,##0.00')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$zero_value"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>  
</xsl:stylesheet>