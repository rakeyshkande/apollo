<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="footer">
 <script type="text/javascript" src="../js/copyright.js"/>
	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr><td>&nbsp;</td></tr>
    <tr>     
      	<td class="disclaimer" align="center">
     		<script>
     			<![CDATA[document.write("COPYRIGHT &copy; "+new Date().getFullYear()+". FTD INC. ALL RIGHTS RESERVED.");]]>
     		</script>          
      </td>
    </tr>
	</table>

</xsl:template>
</xsl:stylesheet>
