<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<xsl:output method="html"/>
<xsl:output indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Source Code Lookup</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"></link>
    <script language="javascript" src="../js/FormChek.js"/>
    <script language="javascript" src="../js/util.js"/>
    <script language="javascript">
	
	var email      = "<xsl:value-of select="key('pageData', 'email_address')/value"/>";   
	var buyer_company_id = "<xsl:value-of select="key('pageData', 'company_id')/value"/>";
	<!-- var buyer_secToken   = "<xsl:value-of select="key('pageData', 'security_token')/value"/>";
	var buyer_context    = "<xsl:value-of select="key('pageData', 'context')/value"/>";  -->
		
    function doExitAction(){
        top.close();
    }

    function doProgramInfoPopup(url) {
        var modal_dim = "dialogWidth:750px; dialogHeight:500px; center:yes; status=0; help:no";
        var ret = showModalDialogIFrame(url, "", modal_dim);
    }
	
	function getFsmDetailsForEmail() {
		
		var buyerEmailAddress = document.getElementById("popup_buyer_email_address").value;
		var url_source =  "ServicesInfoServlet";
        
        document.forms[0].email_address.value=buyerEmailAddress;
        document.forms[0].company_id.value=buyer_company_id;
      <!--   document.forms[0].securitytoken.value=buyer_secToken;   
        document.forms[0].context.value=buyer_context;   --> 
                              
		document.forms[0].action = url_source;
		document.forms[0].submit();                                                                            
	}
	
	function init() {																
		document.getElementById("popup_buyer_email_address").value = email;																
	}
    </script>
</head>

<body onload="javascript:init();">
<form name = "scrubServicesInfoForm">
	<input type="hidden" name="email_address"/>
	<input type="hidden" name="company_id"/>
<!-- 	<input type="hidden" name="securitytoken"/>
	<input type="hidden" name="context"/> -->
 <center>
     <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Services Information'"/>
      <xsl:with-param name="showTime" select="false()"/>
      <xsl:with-param name="showExitButton" select="true()"/>
    </xsl:call-template>
  <table border="0" cellpadding="2" cellspacing="2" align="center">
  	<tr align="center">
  		<td align="right" class="label">Email Address:&nbsp;</td>
  		<td align="left">
  			<input type="text" size="25" maxlength="50" id="popup_buyer_email_address" name="popup_buyer_email_address"/>
  		</td>
  		<td align="left">
			<input type="button" value="Search"  onclick="javascript:getFsmDetailsForEmail();"/>
		</td>
  	</tr>
  </table>
  <table width="98%" border="0" cellpadding="2" cellspacing="2">
    <xsl:choose>
      <xsl:when test="count(servicesInfoList/servicesInfo/display_name) > 0">
        <xsl:for-each select="servicesInfoList/servicesInfo">
          <tr>
            <td align="right" class="label" width="25%">Service:&nbsp;</td>
            <td align="left"><xsl:value-of disable-output-escaping="yes" select="display_name"/></td>
          </tr>         
          <tr>
            <td align="right" class="label">Status:&nbsp;</td>
            <td align="left">
            <xsl:value-of select="account_program_status"/>
            </td>
          </tr>
          <tr>
            <td align="right" class="label">Original Join date:&nbsp;</td>
            
            <td align="left">
            	<xsl:value-of select="start_date"/>
            </td>
          </tr>
          <tr>
            <xsl:choose>
          		<xsl:when test="account_program_status = 'Cancelled'">
            		<td align="right" class="label">Cancellation Date:&nbsp;</td>
            	</xsl:when>
            	<xsl:otherwise>
            		<td align="right" class="label">Expiration Date:&nbsp;</td>
            	</xsl:otherwise>
            </xsl:choose>            
            <td align="left">
              <xsl:value-of select="expiration_date"/>
            </td>
          </tr>
           <tr>
			<td align="right" class="label">Auto-renewal Status:&nbsp;</td>
			<td align="left"><xsl:value-of select="auto_renewal_status"/></td>
			</tr>
          <tr>
            <td align="right" class="label">Program Information:&nbsp;</td>
            <td align="left">
              <xsl:variable name="tempUrl" select="program_url"/>
              <a class="link" href="#" onclick="javascript:doProgramInfoPopup('{$tempUrl}');">
                <xsl:value-of select="program_url"/>
              </a>
            </td>
          </tr>          
          <tr>
            <td colspan="2"><hr/></td>
          </tr>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td colspan="2" align="center" class="label">Email address is not associated with any services</td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
    </table>

  </center>
 </form>
</body>
</html>

</xsl:template>

<xsl:template name="formatDate">
  <xsl:param name="date"/>
  <xsl:variable name="year">
    <xsl:value-of select="substring($date,1,4)"/>
  </xsl:variable>
  <xsl:variable name="month">
    <xsl:value-of select="substring($date,6,2)"/>
  </xsl:variable>
  <xsl:variable name="day">
    <xsl:value-of select="substring($date,9,2)"/>
  </xsl:variable>
  <xsl:value-of select="$month"/>/<xsl:value-of select="$day"/>/<xsl:value-of select="$year"/>
</xsl:template>

</xsl:stylesheet>