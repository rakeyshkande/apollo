<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:template name="pendingOrderSearch">
  <xsl:param name="root"/>

<html>
<head>
  	<title>FTD - Pending Order Search</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <link rel="stylesheet" type="text/css" href="../css/calendar.css"/>
    <script type="text/javascript" src="../js/util.js"/>
    <script type="text/javascript" src="../js/calendar.js"></script>
    <script type="text/javascript" language="JavaScript"><![CDATA[
  /*
   *  Global Variables
   */
    var fieldNames = new Array("sc_email_address", "sc_product_code", "sc_last_name", "sc_phone", "sc_confirmation_number", "sc_csr_id", "sc_pro_confirmation_number");
    var images = new Array("calendar");

  /*
   *  Initialization
   */
    function init(){
      addDefaultListenersArray(fieldNames);
      addImageCursorListener(images);
      document.getElementById("searchButton").focus();
    }

  /*
   *  Date functions
   */
    var dtCh= "/";


    function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
    }

    function stripCharsInBag(s, bag){
     var i;
        var returnString = "";
        // Search through string's characters one by one.
        // If character is not in bag, append to returnString.
        for (i = 0; i < s.length; i++){
            var c = s.charAt(i);
            if (bag.indexOf(c) == -1) returnString += c;
        }
        return returnString;
    }

    function daysInFebruary (year){
        return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
    }
    function DaysArray(n) {
     for (var i = 1; i <= n; i++) {
      this[i] = 31
      if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
      if (i==2) {this[i] = 29}
       }
       return this
    }

    function isDate(_dt)
    {
    
     dtStr = _dt.value;
     var daysInMonth = DaysArray(12)
     var pos1=dtStr.indexOf(dtCh)
     var pos2=dtStr.indexOf(dtCh,pos1+1)
     var strMonth=dtStr.substring(0,pos1)
     var strDay=dtStr.substring(pos1+1,pos2)
     var strYear=dtStr.substring(pos2+1)
     strYr=strYear
     if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
     if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
     month=parseInt(strMonth)
     day=parseInt(strDay)
     year= strYr;

     if (pos1==-1 || pos2==-1){
      alert("The date format should be : mm/dd/yy")
      return false
     }
     if (strMonth.length<1 || month<1 || month>12){
      alert("Please enter a valid month.")
      return false
     }
     if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
      alert("Please enter a valid day.")
      return false
     }
     if (strYear.length == 0 ||  strYear.length == 3 ||  strYear.length == 1 || strYear.length > 4 || year == 0){
      alert("Please enter a valid year.")
      return false
     }
     if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
      alert("Please enter a valid date.")
      return false
     }
     
      if(strDay < 10)
      strDay = "0" + strDay;
      if(strMonth < 10)
      strMonth = "0" + strMonth;
      if(strYear.length < 3)
      strYear = "20" + strYear;

      _dt.value = (strMonth + "/" + strDay + "/" + strYear);
      return true
    }

    function ValidateDate()
    {
      var dt = document.PendingSearchForm.sc_date
      if (isDate(dt) == false)
      {
        dt.focus()
        return false
      }

      return true
   }

    function dateFieldListener(date_box)
    {
      var input = event.keyCode;
      if(input == 46)
      {
        date_box.value = date_box.value + '/';
      }
      
      if ( (input < 47 || input > 57) && !(input == 43 || input == 45) )
          event.returnValue = false;
    }

  /*
   *  Actions
   */
    function doSearchAction(){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      performAction(document.forms[0].action);
    }

    function doExitAction(){
      var form = document.forms[0];
      var url = "ScrubSearchServlet" +
                "?servlet_action=main_exit" +
                getSecurityParams(false);

      performAction(url);
    }

    function performAction(url){

      if(document.PendingSearchForm.sc_date.value.length > 0)
      {
        if(ValidateDate())
        {
          showWaitMessage("content", "wait", "Searching");
          document.forms[0].action = url;
          document.forms[0].submit();
        }
      }
      else
      {
          showWaitMessage("content", "wait", "Searching");
          document.forms[0].action = url;
          document.forms[0].submit();
      }
      
    }]]>
    
    var originArray = new Array();
    var originList = '(';
    var originCounter = 0;
    
    <xsl:for-each select="searchOrigins/origins">
    	<xsl:if test="name!='WLMTI' and name!='AMZNI' and name!='ARI'">
    		originArray[originCounter] = '<xsl:text>\'</xsl:text><xsl:value-of select="name"/><xsl:text>\'</xsl:text>';
    		originCounter++;
    	</xsl:if>
    </xsl:for-each>
    <xsl:for-each select="searchMercentOrigins/mercentOrigins">    	
    		originArray[originCounter] = '<xsl:text>\'</xsl:text><xsl:value-of select="name"/><xsl:text>\'</xsl:text>';
    		originCounter++;
    </xsl:for-each>
    
    <![CDATA[
    for (i = 0; i < originArray.length; i++)
	{
		if(i != 0)
		{
			originList =  originList + ',';
		}
		
		originList = originList + originArray[i];
    }
    
    originList = originList + ')';    
    
     ]]>
    
    </script>
</head>
<body onload="javascript:init();">

  <form name="PendingSearchForm" method="post" action="ScrubSearchServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="sc_mode" value="{key('searchCriteria', 'sc_mode')/value}"/>
    <input type="hidden" name="sc_search_type" value="P"/>
    <input type="hidden" name="servlet_action" value="result_list"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Pending Order Search'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
    </xsl:call-template>

    <!-- Content to hide once search begins -->
    <div id="content" style="display:block">

      <!-- Action Buttons -->
      <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="45%" class="requiredFieldTxt">
            <xsl:if test="key('pageData', 'message_display')/value != ''">
              <xsl:value-of select="key('pageData', 'message_display')/value"/>
            </xsl:if>
          </td>
          <td width="10%" align="center">
            <input type="button" name="searchButton" tabindex="1" value="Continue" onclick="javascript:doSearchAction();"/>
          </td>
          <td width="45%"></td>
        </tr>
      </table>

      <!-- Main table -->
      <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
          <td>

            <!-- Search Criteria -->
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
              <tr>
                <td width="25%" class="label">E-Mail Address:</td>
                <td width="25%"><input type="text" name="sc_email_address" tabindex="2" maxlength="55" value="{key('searchCriteria', 'sc_email_address')/value}"/></td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="25%">
                  <span class="label">Date:<br/></span>
                  <span>MM/DD/YY or MM/DD/YYYY</span>
                </td>
                <td width="25%">
                  <input name="sc_date" type="text" tabindex="3" maxlength="10" value="{key('searchCriteria', 'sc_date')/value}" onblur="" onfocus="javascript:fieldFocus();" onkeypress="javascript:dateFieldListener(this);"/>&nbsp;&nbsp;
                  <img id="calendar" src="../images/calendar.gif" width="20" height="20" align="ABSMIDDLE"/>
                </td>
                <td width="30%" class="label">
                  <input type="radio" name="sc_date_flag" value="delivery" tabindex="4" checked="true">
                    <xsl:if test="key('searchCriteria', 'sc_date_flag')/value = 'delivery'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>Delivery
                  <input type="radio" name="sc_date_flag" value="ship" tabindex="5">
                    <xsl:if test="key('searchCriteria', 'sc_date_flag')/value = 'ship'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>Ship
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Product Code:</td>
                <td width="25%"><input name="sc_product_code" type="text" tabindex="6" maxlength="10" value="{key('searchCriteria', 'sc_product_code')/value}"/></td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Last Name:</td>
                <td width="25%"><input name="sc_last_name" type="text" tabindex="7" maxlength="20" value="{key('searchCriteria', 'sc_last_name')/value}"/></td>
                <td width="30%" class="label">
                  <input type="radio" name="sc_last_name_flag" value="customer" tabindex="8" checked="true">
                    <xsl:if test="key('searchCriteria', 'sc_last_name_flag')/value = 'customer'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>Customer
                  <input type="radio" name="sc_last_name_flag" value="recipient" tabindex="9">
                    <xsl:if test="key('searchCriteria', 'sc_last_name_flag')/value = 'recipient'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>Recipient
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Phone:</td>
                <td width="25%"><input name="sc_phone" type="text" tabindex="10" maxlength="20" value="{key('searchCriteria', 'sc_phone')/value}"/></td>
                <td width="30%" class="label">
                  <input type="radio" name="sc_phone_flag" value="customer" tabindex="11" checked="true">
                    <xsl:if test="key('searchCriteria', 'sc_phone_flag')/value = 'customer'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>Customer
                  <input type="radio" name="sc_phone_flag" value="recipient" tabindex="12">
                    <xsl:if test="key('searchCriteria', 'sc_phone_flag')/value = 'recipient'"><xsl:attribute name="CHECKED"/></xsl:if>
                  </input>Recipient
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Ship To Type:</td>
                <td width="25%">
                  <select name="sc_ship_to_type" tabindex="13">
                    <option value="">
                      <xsl:if test="key('searchCriteria', 'sc_ship_to_type')/value = ''"><xsl:attribute name="SELECTED"/></xsl:if>
                      ALL
                    </option>
                    <xsl:for-each select="addressTypeList/addressType">
                      <xsl:variable name="address_type" select="address_type"/>
                      <option value="{address_type}">
                        <xsl:if test="key('searchCriteria', 'sc_ship_to_type')/value = address_type"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="description"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Confirmation Number:</td>
                <td width="25%"><input name="sc_confirmation_number" type="text" tabindex="14" maxlength="20" value="{key('searchCriteria', 'sc_confirmation_number')/value}"/></td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">ProFlowers Confirmation Number:</td>
                <td width="25%"><input name="sc_pro_confirmation_number" type="text" tabindex="15" maxlength="20" value="{key('searchCriteria', 'sc_pro_confirmation_number')/value}"/></td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Category:</td>
                <td width="25%">
                  <select name="sc_origin" tabindex="16">
                    <script>
                    <![CDATA[ document.write("<option value=\"" + originList + "\">");]]>
                    <![CDATA[ document.write('ALL');]]>
                    <![CDATA[ document.write("</option>");]]> 
                    </script>
                    <xsl:for-each select="searchOrigins/origins">
                    <xsl:sort select="value"/>
                      <option value="('{name}')">
                        <xsl:if test="origin_id = key('searchCriteria', 'sc_origin')/value"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="value"/>
                      </option>
  					</xsl:for-each>
                  </select>
                </td>
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr>
                <td width="25%" class="label">Product Property:</td>
                <td width="25%">
                  <select name="sc_product_property" tabindex="17">
                    <option value="">
                        <xsl:if test="key('searchCriteria', 'sc_product_property')/value = ''"><xsl:attribute name="SELECTED"/></xsl:if>
                        &nbsp;
                    </option>
                    <xsl:for-each select="productProperties/productProperty">
                      <option value="{name}">
                        <xsl:if test="name = key('searchCriteria', 'sc_product_property')/value"><xsl:attribute name="SELECTED"/></xsl:if>
                        <xsl:value-of select="value"/>
                      </option>
                    </xsl:for-each>
                  </select>
                </td>
                <td colspan="2">&nbsp;</td>
              </tr>               
              <tr>
                <td width="25%" class="label">CSR ID:</td>
                <td width="25%"><input name="sc_csr_id" type="text" tabindex="18" maxlength="10" value="{key('searchCriteria', 'sc_csr_id')/value}"/></td>
                <td colspan="2">&nbsp;</td>
              </tr>
              
              <xsl:choose>
                 <xsl:when test="count(/root/preferred_partners_for_user/preferred_partner) > 0">
              <tr>
                <td width="25%" class="label">Order Type:</td>
                <td width="25%">
                  <select name="sc_preferred_partner" tabindex="19" >
                    <xsl:for-each select="/root/preferred_partners_for_user/preferred_partner">
                      <xsl:sort select="."/>
                      <option value="{name}">
                          <xsl:if test="name = key('searchCriteria', 'sc_preferred_partner')/value"><xsl:attribute name="SELECTED"/></xsl:if>
                          <xsl:value-of select="value"/>
                      </option>
                    </xsl:for-each>
                    <option value="NON_PREFERRED">
                    	<xsl:if test="'NON_PREFERRED' = key('searchCriteria', 'sc_preferred_partner')/value"><xsl:attribute name="SELECTED"/></xsl:if>
                    	FTD
                    </option>
                    <option value="VIP">
                    	<xsl:if test="'VIP' = key('searchCriteria', 'sc_preferred_partner')/value"><xsl:attribute name="SELECTED"/></xsl:if>
                    	VIP Customer
                    </option>
                  </select>
                </td>
				<td>
                </td>
                <td colspan="1">
                </td>
              </tr>
             </xsl:when>
             <xsl:otherwise>
             	<tr>
                <td width="25%" class="label">Order Type:</td>
                <td width="25%">
                  <select name="sc_preferred_partner" tabindex="19" >
                    <option value="VIP">
                    <xsl:if test="'VIP' = key('searchCriteria', 'sc_preferred_partner')/value"><xsl:attribute name="SELECTED"/></xsl:if>
                    VIP Customer</option>
                  </select>                  
                </td>
		<td>
                </td>               
                <td colspan="1">
                </td>
              </tr>    
             </xsl:otherwise>
             </xsl:choose>
              <tr>
                <td width="25%" class="label"></td>
                <td width="25%">
                </td>
                <td colspan="2" align="right" class="scrubTotal">
                  Total number of orders in pending:&nbsp;&nbsp;&nbsp;<xsl:value-of select="pageData/data[name='scrubCount']/value"/>
                </td>
              </tr>            
            </table>
          </td>
        </tr>
      </table>

      <!-- Action Buttons -->
      <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="20%">&nbsp;</td>
          <td width="60%" align="center">
            <input type="button" name="searchButton" tabindex="20" value="Continue" onclick="javascript:doSearchAction();"/>
          </td>
          <td width="20%" align="right"><input type="button" tabindex="21" value="Exit" onclick="javascript:doExitAction();"/></td>
        </tr>
      </table>
    </div>
  </form>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD"  width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Footer template -->
	<xsl:call-template name="footer"/>

</body>
</html>
<script type="text/javascript">
  Calendar.setup(
    {
      inputField  : "sc_date",  // ID of the input field
      ifFormat    : "mm/dd/y",  // the date format
      button      : "calendar"  // ID of the button
    }
  );
</script>

</xsl:template>
</xsl:stylesheet>