<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="pendingOrRemovedSearchResults.xsl"/>
<xsl:import href="abandonedSearchResults.xsl"/>
<xsl:import href="showProFlowersSearchResultList.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<!-- Variables -->
<xsl:variable name="REINSTATE" select="'R'"/>
<xsl:variable name="ABANDONED" select="'A'"/>
<xsl:variable name="SCRUBLIST" select="'S'"/>

<xsl:output method="html"/>
<xsl:output indent="yes"/>
<xsl:template match="/root">

  <xsl:choose>
    <xsl:when test="key('searchCriteria', 'sc_mode')/value=$REINSTATE and key('searchCriteria', 'sc_search_type')/value=$ABANDONED">
      <xsl:call-template name="abandonedSearchResults">
        <xsl:with-param name="root" select="/"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="key('searchCriteria', 'sc_mode')/value=$SCRUBLIST">
      <xsl:call-template name="showProFlowersSearchResultList">
        <xsl:with-param name="root" select="/"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="pendingOrRemovedSearchResults">
        <xsl:with-param name="root" select="/"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>
</xsl:stylesheet>