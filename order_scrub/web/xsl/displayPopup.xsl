<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<xsl:output method="html"/>
<xsl:output indent="yes"/>

<xsl:template match="/root">

<html>
<head>
    <title>FTD - Display Popup</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"></link>
    <script language="javascript" src="../js/FormChek.js"/>
    <script language="javascript" src="../js/util.js"/>
    <script language="javascript">
    
    function doExitAction(){
        top.close();
    }

    </script>
</head>

<body>

    <center>
    
    <xsl:call-template name="header">
        <xsl:with-param name="headerName" select="display_header"/>
        <xsl:with-param name="showTime" select="false()"/>
        <xsl:with-param name="showExitButton" select="true()"/>
    </xsl:call-template>
    
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td align="center"><xsl:value-of disable-output-escaping="yes" select="display_message"/></td>
        </tr>
    </table>

    </center>
</body>
</html>

</xsl:template>

</xsl:stylesheet>