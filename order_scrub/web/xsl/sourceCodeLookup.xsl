<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:output indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Source Code Lookup</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"></link>
    <script language="javascript" src="../js/FormChek.js"/>
    <script language="javascript" src="../js/util.js"/>
    <script language="javascript">
    <![CDATA[
        var fieldNames = new Array("sourceCodeInput");
        var images = new Array("sourceCodeSearch", "sourceCodeCloseTop", "sourceCodeCloseBottom", "sourceCodeSelectNone");

        function init()
        {
            addDefaultListenersArray(fieldNames);
            addImageCursorListener(images);
            window.name = "VIEW_SOURCE_CODE_LOOKUP";
            window.focus();
            document.forms[0].sourceCodeInput.focus();
        }

        function onKeyDown()
        {
          if (window.event.keyCode == 13)
            reopenPopup();
        }

        function enterReOpenSourceCodePopup()
        {
          if (window.event.keyCode == 13)
            reopenPopup();
        }

        function reopenPopup()
        {
          var form = document.forms[0];

          //First validate the Source Code input
          var sourceCode = document.forms[0].sourceCodeInput.value;
          sourceCode = stripWhitespace(sourceCode);

          if(sourceCode == "" || sourceCode.length < 2)
          {
            form.sourceCodeInput.focus();
            form.sourceCodeInput.style.backgroundColor = 'pink';
            alert("Please correct the marked fields");
          }
          else
          {
            var action = document.getElementById("action");
            action.value = "load";
            form.target = window.name;
            form.submit();
          }
        }

        function noPreferredPartnerPermissions(msg) 
        {
          var ppDiv = document.getElementById(msg).style;
          ppDiv.display = "block";
          window.scrollTo(0,0);
        }
        
        function closeSourceLookup(id)
        {
          if (window.event.keyCode == 13)
            populatePage(id);
        }

        function populatePage(id)
        {
          var sc, desc, part, company,_content,_focus_object,item_number,submit_item_flag,servlet_action;
          if (id > -1){
            sc = document.getElementById("sourceCode" + id).innerHTML;
            desc = document.getElementById("desc" + id).innerHTML;
            part = document.getElementById("partner_id" + id).value;
            company = document.getElementById("company_id" + id).value;

            _content = document.getElementById("_content").value;
            _focus_object = document.getElementById("_focus_object").value;
            item_number = document.getElementById("item_number").value;
            submit_item_flag = document.getElementById("submit_item_flag").value;
            servlet_action = document.getElementById("servlet_action").value;
          }

          top.returnValue = new Array(sc, desc, part, company, _content, _focus_object, item_number, submit_item_flag, servlet_action);
          top.close();
        }
    ]]>
    </script>
</head>

<body onLoad="javascript:init();">
<form name="SourceCodeLookupForm" method="get" action="">
  <input type="hidden" name="date_flag" value="{key('pageData', 'date_flag')/value}"/>
  <input type="hidden" name="company_id" value="{key('pageData', 'company_id')/value}"/>
  <input type="hidden" name="partner_id" value="{key('pageData', 'partner_id')/value}"/>
  <input type="hidden" name="item_number" value="{key('pageData', 'item_number')/value}"/>
  <input type="hidden" name="_content" value="{key('pageData', '_content')/value}"/>
  <input type="hidden" name="_focus_object" value="{key('pageData', '_focus_object')/value}"/>
  <input type="hidden" name="submit_item_flag" value="{key('pageData', 'submit_item_flag')/value}"/>
  <input type="hidden" name="servlet_action" value="{key('pageData', 'servlet_action')/value}"/>
  <input type="hidden" name="action" value="" id="action" />
  <center>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
      <tr>
        <td align="center" class="header">Source Code Lookup</td>
      </tr>
      <tr>
        <td nowrap="true" colspan="3" align="left">
          <span class="instruction">Enter Source Code or Description value</span>&nbsp;
          <input type="text" name="sourceCodeInput" tabindex="1" size="20" maxlength="50" value="" onkeypress="javascript:enterReOpenSourceCodePopup();"/>
          <span class="instruction"> and press </span>
          <img id="sourceCodeSearch" tabindex="2" onkeydown="javascript:onKeyDown();" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()"/>
        </td>
      </tr>
      <tr>
        <td><hr/></td>
      </tr>
    </table>
  </center>
</form>
 <center>
    <xsl:for-each select="/root/noPermissionsForPreferredPartner/preferred_partner">
       <div id="{name}Div" style="display:none">
         <span class="ErrorMessage"><xsl:value-of select="value"/></span>
       </div>
    </xsl:for-each>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
      <tr>
        <td align="right">
          <img id="sourceCodeCloseTop" tabindex="3" onkeydown="javascript:closeSourceLookup(-1)" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)"/>
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" border="1" class="LookupTable" cellpadding="2" cellspacing="2">
            <tr>
              <td width="5%" class="label">&nbsp;</td>
              <td class="label" valign="bottom">Name/Description</td>
              <td class="label" valign="bottom">Offer</td>
              <td class="label" align="center" valign="bottom">Service<br/>Charge</td>
              <td class="label" align="center" valign="bottom">Expiration<br/>Date</td>
              <td class="label" align="center" valign="bottom">Order<br/>Source</td>
              <td class="label" align="center" valign="bottom">Source<br/>Code</td>
              <td>
                <xsl:for-each select="sourceCodeList/sourceCode">
                <!-- Set variables to help determine if this is Preferred Partner source code but CSR doesn't have permission -->
                <xsl:variable name="ppForSourceCode" select="preferred_partner_name"/>
                <xsl:variable name="ppAlertDiv" select="concat($ppForSourceCode, 'Div')"/>
                <xsl:variable name="ppButNoPermission" select="boolean(preferred_partner_name != '' and (boolean(/root/preferred_partners_for_user[preferred_partner = $ppForSourceCode]) = false))"/>
                <tr>
                  <td>
                    <xsl:choose>
                      <xsl:when test="sourcecode = key('pageData', 'source_code')/value">
                         <xsl:choose>
                           <xsl:when test="$ppButNoPermission">
                             <img onclick="javascript:noPreferredPartnerPermissions('{$ppAlertDiv}')" tabindex="4" src="../images/selectButtonRight.gif"/>
                           </xsl:when>
                           <xsl:otherwise>
                             <img onclick="javascript:populatePage({@num})" onkeydown="javascript:closeSourceLookup({@num})" tabindex="4" src="../images/selectButtonRight.gif"/>
                           </xsl:otherwise>
                         </xsl:choose>
                      </xsl:when>
                      <xsl:when test="expiredflag='Y' or key('pageData', 'company_id')/value != company_id and key('pageData', 'company_id')/value != '' or valid_pay_method='IN' or valid_pay_method='PC'">
                        <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                      </xsl:when>
                      <xsl:when test="expiredflag='N'">
                        <xsl:choose>

                         <xsl:when test="key('pageData', 'submit_item_flag')/value = 'Y'">
                           <xsl:choose>
                            <xsl:when test="key('pageData', 'partner_id')/value = partner_id or partner_id = ''">
                               <xsl:choose>
                                 <xsl:when test="$ppButNoPermission">
                                     <img onclick="javascript:noPreferredPartnerPermissions('{$ppAlertDiv}')" tabindex="4" src="../images/selectButtonRight.gif"/>
                                 </xsl:when>
                                 <xsl:otherwise>
                                     <img onclick="javascript:populatePage({@num})" onkeydown="javascript:closeSourceLookup({@num})" tabindex="4" src="../images/selectButtonRight.gif"/>
                                 </xsl:otherwise>
                               </xsl:choose>
                            </xsl:when>
                           <xsl:otherwise>
                             <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                           </xsl:otherwise>
                           </xsl:choose>
                         </xsl:when>

                         <xsl:otherwise>
                           <xsl:choose>
                             <xsl:when test="$ppButNoPermission">
                                 <img onclick="javascript:noPreferredPartnerPermissions('{$ppAlertDiv}')" tabindex="4" src="../images/selectButtonRight.gif"/>
                             </xsl:when>
                             <xsl:otherwise>
                                 <img onclick="javascript:populatePage({@num})" onkeydown="javascript:closeSourceLookup({@num})" tabindex="4" src="../images/selectButtonRight.gif"/>
                             </xsl:otherwise>
                           </xsl:choose>
                         </xsl:otherwise>

                        </xsl:choose>

                      </xsl:when>
                    </xsl:choose>


                  </td>
                  <td id="desc1{@num}" align="left">
                    <div id="desc{@num}">
                    <xsl:value-of disable-output-escaping="yes" select="sourcedescription"/>
                    </div>
                    <xsl:if test="allow_free_shipping_flag != 'Y'">
                        <img src="../images/noFSicon.jpg" alt="No Free Shipping" border="0"/>
                    </xsl:if>
                  </td>
                  <td align="left"><xsl:value-of select="offerdescription"/></td>
                  <td align="center"><xsl:value-of select="servicecharge"/></td>
                  <td align="right"><xsl:value-of select="expirationdate"/></td>
                  <td align="center"><xsl:value-of select="ordersource"/></td>
                  <td id="sourceCode{@num}" align="center"><xsl:value-of select="sourcecode"/></td>
                  <input type="hidden" name="partner_id{@num}" value="{partner_id}"/>
                  <input type="hidden" name="company_id{@num}" value="{company_id}"/>
                </tr>
                </xsl:for-each>
              </td>
            </tr>
            <tr>
              <td width="5%" valign="center">
                <img id="sourceCodeSelectNone" tabindex="4" onkeydown="javascript:closeSourceLookup(-1)" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage(-1);"/>
              </td>
              <td colspan="7" valign="top">None of the above</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="right">
          <img id="sourceCodeCloseBottom" tabindex="5" onkeydown="javascript:closeSourceLookup(-1)" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)"/>
        </td>
      </tr>
    </table>
  </center>
</body>
</html>

</xsl:template>
</xsl:stylesheet>