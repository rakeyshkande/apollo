<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Order Comments</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"></script>
    <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Global vaiables
   */
    var EXIT = "_EXIT_";

  /*
   *  Initialization
   */
    function init(){
      setTimeout("setScrollingDivHeight()",100);
      window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight(){
      var commentsListingDiv = document.getElementById("commentsListing");

      commentsListingDiv.style.height = document.body.clientHeight - commentsListingDiv.getBoundingClientRect().top - 80;

    }

  /*
   *  Actions
   */
    function doCommentsAction(message){
      var modalDim = "dialogWidth:500px; dialogHeight:330px; center:yes; status=0; help=no; scrollbars:no; resizable:no; scroll:no";
      var ret = showModalDialogIFrame("../information.html", message, modalDim);
    }

    function doCloseAction(){
      top.close();
    }

    function doExitAction(){
      top.returnValue = EXIT;
      doCloseAction();
    }]]>
    </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Order Comments'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <form name="OrderCommentsForm" method="get" action="OrderCommentsServlet">

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Order Comments -->
          <div id="commentsListing" style="overflow:auto; width:100%; padding:0px; margin:0px">
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Comments 1</td>
            </tr>
            <tr>
              <td width="20%" class="colHeaderCenter">Comment Date</td>
              <td width="15%" class="colHeaderCenter">CSR ID</td>
              <td width="30%" class="colHeaderCenter">Disposition</td>
              <td width="35%" class="colHeaderCenter">Comments</td>
            </tr>
            
            <xsl:for-each select="orderDispositionList/orderDisposition">
            <tr>
	            <td align="center" valign="top"><xsl:value-of select="comment_date"/></td>
	            <td align="center" valign="top"><xsl:value-of select="csr_id"/></td>
	            <td align="center" valign="top">(<xsl:value-of select="disposition_id"/>)&nbsp;<xsl:value-of select="disposition_description"/></td>
	            <td align="center" valign="top"><xsl:value-of select="comments"/></td>
	        </tr>  
            </xsl:for-each>
                
          </table>
          </div>
          

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="closeButton" value="Close" tabindex="1" onClick="javascript:doCloseAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  </form>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>