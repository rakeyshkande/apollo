<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:variable name="standardLabel" select="key('pageData','standard_label')/value"/>
<xsl:variable name="deluxeLabel" select="key('pageData','deluxe_label')/value"/>
<xsl:variable name="premiumLabel" select="key('pageData','premium_label')/value"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
  <title>FTD - Product List</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script type="text/javascript" language="javascript" src="../js/util.js"></script>
  <script type="text/javascript" language="javascript">
  /*
   *  Global variables
   */
    var product_images = "<xsl:value-of select="key('pageData', 'product_images')/value"/>";
    var hasProducts = <xsl:value-of select="boolean(count(productList/products/product)!=0)"/>;<![CDATA[

  /*
   *  Initialization
   */
    function init() {
    	setNavigationHandlers();
      var form = document.forms[0];
      if (hasProducts){
        setScrollingDivHeight();
        document.getElementById("PrevPageSection").style.display = (form.page_number.value == "1") ? "none" : "";
        document.getElementById("NextPageSection").style.display = (form.page_number.value ==  form.total_pages.value) ? "none" : "";
      }
      window.name = "PRODUCT_LIST_VIEW";
    }

    function setScrollingDivHeight(){
      var productListingDiv = document.getElementById("productListing");
      productListingDiv.style.height = document.body.clientHeight - productListingDiv.getBoundingClientRect().top - 85;
    }

  /*
   *  Show large image of product
   */
    function viewLargeImage(prodId){
      var closeTag= "<a href=\"javascript:self.close();\" tabindex='-1'>close the window</a>";
      imageWindow = window.open("","enlarged_view","toolbar=no,resizable=no,scrollbars=no,width=340,height=470");
      imageWindow.document.write("<html>");
      imageWindow.document.write("<head>");
      imageWindow.document.write("<title>Large Image</title></head><body><form><center>");
      imageWindow.document.write("<br/>");
      imageWindow.document.write(closeTag);
      imageWindow.document.write("<br/>");
      imageWindow.document.write("<img ");
      imageWindow.document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
      imageWindow.document.write("if(imag != 'npi_2.jpg'){ ");
      imageWindow.document.write("this.src='" + product_images + "npi_2.jpg'}\" ");
      imageWindow.document.write("src='" + product_images + prodId + "_2.jpg'> ");
      imageWindow.document.write("</img>");
      imageWindow.document.write("<br/>");
      imageWindow.document.write(closeTag);
      imageWindow.document.write("</center></form></body></html>");
    }

  /*
   *  Actions
   */
    function paramsHelper(){
      var form = document.forms[0];
      var elem = document.getElementById('ProductListForm').elements;
      var addon_str = "";
      for(var i = 0; i < elem.length; i++) {
          if (elem[i].name.indexOf("addonOrig_") > -1) {
             addon_str += elem[i].name + "=" + elem[i].value;
          }
      }
      var params =  "&international_service_fee=" + form.international_service_fee.value +
                    "&domestic_service_fee=" + form.domestic_service_fee.value +
                    "&master_order_number=" + form.master_order_number.value +
                    "&item_order_number=" + form.item_order_number.value +
                    "&buyer_full_name=" + form.buyer_full_name.value +
                    "&price_point_id=" + form.price_point_id.value +
                    "&domestic_flag=" + form.domestic_flag.value +
                    "&occasion_text=" + form.occasion_text.value +
                    "&occasion_id=" + form.occasion_id.value +
                    "&reward_type=" + form.reward_type.value +
                    "&script_code=" + form.script_code.value +
                    "&source_code=" + form.source_code.value +
                    "&postal_code=" + form.postal_code.value +
                    "&search_type=" + form.search_type.value +
                    "&company_id=" + form.company_id.value +
                    "&partner_id=" + form.partner_id.value +
                    "&orig_date=" + form.orig_date.value +
                    "&page_name=" + form.page_name.value +
                    "&sort_type=" + form.sort_type.value +
                    "&list_flag=" + form.list_flag.value +
                    "&jcp_flag=" + form.jcp_flag.value +
                    "&country=" + form.country.value +
                    "&state=" + form.state.value +
                    "&city=" + form.city.value +
                    addon_str + 
                    "&delivery_date=" + form.delivery_date.value +
                    "&order_guid=" + form.order_guid.value +
                    getSecurityParams(false);


      return params;
    }

    function doOrderNowAction(inProdId){
      var url = "ProductDetailServlet" +
                "?product_id=" + inProdId +
                paramsHelper();

      performAction(url);
    }

    function doSortByPrice(){
      document.forms[0].sort_type.value = "priceSortAsc";
      var url = "ProductListServlet" +
                "?page_number=" + document.forms[0].page_number.value +
                paramsHelper();

      performAction(url);
    }

    function doPriceFilterAction(select){
      if (select[select.selectedIndex].value > 0) {
        var url = "ProductListServlet" +
                  "?page_number=1" +
                  "&price_point_id=" + select[select.selectedIndex].value +
                  paramsHelper();

        performAction(url);
      }
    }

    function doPrevPageAction(){
      var page = parseInt(document.forms[0].page_number.value);
      doViewPageAction(page - 1);
    }

    function doNextPageAction(){
      var page = parseInt(document.forms[0].page_number.value);
      doViewPageAction(page + 1);
    }

    function doViewPageAction(pageNum){
      document.forms[0].page_number.value = pageNum;
      var url = "ProductListServlet" +
                "?page_number" + pageNum +
                paramsHelper();

      performAction(url);
    }

    function doCategoriesAction(){
      var url = "CategoryServlet" +
                "?page_number=1" +
                paramsHelper();

      document.forms[0].method = "get";
      performAction(url);
    }

    function doCloseAction(){
      top.close();
    }

    function doExitAction(){
      top.returnValue = new Array("_EXIT_");
      doCloseAction();
    }

    function performAction(url){
      showWaitMessage("contentDiv", "wait");
      document.forms[0].action = url;
      document.forms[0].target = window.name;
      document.forms[0].submit();
    }]]>
  </script>
</head>

<body onload="javascript:init();">
  <form name="ProductListForm" id="ProductListForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="international_service_fee" value="{key('pageData', 'international_service_fee')/value}"/>
    <input type="hidden" name="domestic_service_fee" value="{key('pageData', 'domestic_service_fee')/value}"/>
    <input type="hidden" name="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="occasion_text" value="{key('pageData', 'occasion_text')/value}"/>
    <input type="hidden" name="domestic_flag" value="{key('pageData', 'domestic_flag')/value}"/>
    <input type="hidden" name="occasion_id" value="{key('pageData', 'occasion_id')/value}"/>
    <input type="hidden" name="postal_code" value="{key('pageData', 'postal_code')/value}"/>
    <input type="hidden" name="source_code" value="{key('pageData', 'source_code')/value}"/>
    <input type="hidden" name="script_code" value="{key('pageData', 'script_code')/value}"/>
    <input type="hidden" name="reward_type" value="{key('pageData', 'reward_type')/value}"/>
    <input type="hidden" name="search_type" value="{key('pageData', 'search_type')/value}"/>
    <input type="hidden" name="total_pages" value="{key('pageData', 'total_pages')/value}"/>
    <input type="hidden" name="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="company_id" value="{key('pageData', 'company_id')/value}"/>
    <input type="hidden" name="partner_id" value="{key('pageData', 'partner_id')/value}"/>
    <input type="hidden" name="orig_date" value="{key('pageData', 'orig_date')/value}"/>
    <input type="hidden" name="sort_type" value="{key('pageData', 'sort_type')/value}"/>
    <input type="hidden" name="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
    <input type="hidden" name="country" value="{key('pageData', 'country')/value}"/>
    <input type="hidden" name="state" value="{key('pageData', 'state')/value}"/>
    <input type="hidden" name="city" value="{key('pageData', 'city')/value}"/>
    <input type="hidden" name="delivery_date" value="{key('pageData', 'delivery_date')/value}"/>
    <xsl:for-each select="/root/AddonsOriginal/Addon">
      <input type="hidden" name="addonOrig_{addon_id}" value="{addon_qty}"/>
    </xsl:for-each>

    <input type="hidden" name="list_flag" value="Y"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Product List'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="false()"/>
    </xsl:call-template>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Order Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Order Information for <xsl:value-of select="key('pageData', 'item_order_number')/value"/></td>
            </tr>
            <tr>
              <td width="25%" class="label">Master Order Number:</td>
              <td width="25%"><xsl:value-of select="key('pageData','master_order_number')/value"/></td>
              <td width="15%" class="label">Customer:</td>
              <td width="35%"><xsl:value-of select="key('pageData','buyer_full_name')/value"/></td>
            </tr>
          </table>

          <!-- Product list or no matching products message -->
          <xsl:choose>
            <xsl:when test="count(productList/products/product)!=0">

            <!-- Filters -->
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td colspan="3" class="tblheader">Actions</td>
              </tr>
              <tr>
                <td width="30%" align="center">
                  <span class="textLink" onclick="javascript:doSortByPrice();">Sort By Price</span>
                </td>
                <td width="35%" align="center">
                  Filter By Price:&nbsp;
                  <select name="pricepointfilter" onchange="javascript:doPriceFilterAction(this);" tabindex="-1">
                    <option value="0"/>
                    <xsl:for-each select="pricePointList/pricePoint">
                      <option value="{@pricepointsid}"><xsl:value-of select="@pricepointdescription"/></option>
                    </xsl:for-each>
                  </select>
                </td>
                <td width="35%" align="center">
                  Page&nbsp;
                  <span id="PrevPageSection" class="textLink" onclick="javascript:doPrevPageAction();">Previous</span>
                  &nbsp;&nbsp;
                  <script language="javascript"><![CDATA[
                    for (var i = 0, x = 1; i < document.forms[0].total_pages.value; i++, x++){
                      if (x == document.forms[0].page_number.value)
                        document.write(x + "&nbsp;");
                      else
                        document.write("<span class=\"textLink\" onclick=\"javascript:doViewPageAction(" + x + ")\">" + x + "</span>&nbsp;");
                    }]]>
                  </script>
                  &nbsp;
                  <span id="NextPageSection" class="textLink" onclick="javascript:doNextPageAction();">Next</span>
                </td>
              </tr>
            </table>

            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td colspan="3" class="tblheader">
                  <xsl:if test="key('pageData', 'domestic_flag')/value='D'">
                    <span class="textLinkWhite" onclick="javascript:doCategoriesAction();">Categories</span>
                    &nbsp;>&nbsp;
                  </xsl:if>
                  Product List
                </td>
              </tr>
            </table>

            <!-- Content div -->
            <div id="contentDiv" style="display:block">

              <!-- Product List -->
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <xsl:variable name="rewardType" select="key('pageData', 'reward_type')/value"/>
                <tr>
                  <td class="colHeaderCenter" width="15%">&nbsp;</td>
                  <td class="colHeaderCenter" width="20%">
                    Price
                    <xsl:if test="$rewardType!=''">
                      <xsl:if test="$rewardType!='TotSavings'">
                        (<xsl:value-of select="$rewardType"/>)
                      </xsl:if>
                    </xsl:if>
                  </td>
                  <td class="colHeaderCenter" width="70%">Name / Description</td>
                </tr>
                <tr>
                  <td colspan="3" width="100%" align="center">

                  <!-- Scrolling div contiains product detail -->
                  <div id="productListing" style="overflow:auto; width:100%; padding:0px; margin:0px;">
                    <table width="100%" border="0" cellpadding="2" cellspacing="2">
                      <xsl:for-each select="productList/products/product">
                        <tr>

                          <!-- Product picture -->
                          <td width="15%" align="center" valign="top">
                            <table width="100%" border="0" cellpadding="1" cellspacing="1">
                              <tr>
                                <td align="center">
                                  <script language="javascript">
                                    // Exception display trigger variables
                                    var exceptionMessageTrigger = false;
                                    var selectedDate = new Date("<xsl:value-of select="key('pageData', 'requesteddeliverydate')/value"/>");
                                    var exceptionStart = new Date("<xsl:value-of select="@exceptionstartdate"/>");
                                    var exceptionEnd = new Date("<xsl:value-of select="@exceptionenddate"/>");
                                    var novator_id = "<xsl:value-of select="@novatorid"/>";<![CDATA[
                                    document.write("<img onclick=\"javascript: viewLargeImage(\'" + novator_id + "\')\" ");
                                    document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
                                    document.write("if(imag != 'npi_1.gif'){ ");
                                    document.write("this.src ='" + product_images + "npi_1.gif'}\" ");
                                    document.write("src='" + product_images + novator_id + "_1.gif'/> ");]]>
                                  </script>
                                </td>
                              </tr>
                              <tr>
                                <td align="center">
                                  <span class="textLink" onclick="javascript:doOrderNowAction('{@productid}');" onkeypress="javascript:doOrderNowAction('{@productid}');" tabindex="{position()}">Order Now</span>&nbsp;&nbsp;
                                </td>
                              </tr>
                              <xsl:if test="@firstdeliverymethod='florist'">
                                <tr>
                                  <td align="center" bgcolor="#C1DEF3">
                                    FTD <script><![CDATA[document.write("&#174;")]]></script> Florist
                                  </td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="@firstdeliverymethod='carrier'">
                                <tr>
                                  <td align="center" bgcolor="#FF9933"><xsl:value-of select="@carrier"/></td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>

                          <!-- Pricing -->
                          <td width="20%" valign="top">
                            <table width="100%" border="0" cellpadding="1" cellspacing="1">
                            <xsl:if test="@standardprice > 0">
                              <tr>
                                <td valign="top">
                                  <xsl:choose>
                                    <xsl:when test="@standarddiscountprice>0 and @standarddiscountprice!=@standardprice">
                                      <span style="color='red'"><strike>$<xsl:value-of select="format-number(@standardprice,'#,###,##0.00')"/></strike></span>
                                      $<xsl:value-of select="format-number(@standarddiscountprice,'#,###,##0.00')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="format-number(@standardprice,'#,###,##0.00')"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
				  <xsl:choose>
					<xsl:when test="$standardLabel != ''">
						&nbsp; - <xsl:value-of select="$standardLabel"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp; - Shown
					</xsl:otherwise>
				  </xsl:choose>
                                  <xsl:if test="@standardrewardvalue>0">
                                    <xsl:if test="$rewardType!='Percent' and $rewardType!='Dollars'">
                                      (<xsl:value-of select="@standardrewardvalue"/>)
                                    </xsl:if>
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:if test="@deluxeprice>0">
                              <tr>
                                <td valign="top">
                                  <xsl:choose>
                                    <xsl:when test="@deluxediscountprice>0 and @deluxediscountprice!=@deluxeprice">
                                      <span style="color='red'"><strike>$<xsl:value-of select="format-number(@deluxeprice,'#,###,##0.00')"/></strike></span>
                                      $<xsl:value-of select="format-number(@deluxediscountprice,'#,###,##0.00')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="format-number(@deluxeprice,'#,###,##0.00')"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
				  <xsl:choose>
					<xsl:when test="$deluxeLabel != ''">
						&nbsp; - <xsl:value-of select="$deluxeLabel"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp; - Deluxe
					</xsl:otherwise>
				  </xsl:choose>
                                  <xsl:if test="@deluxerewardvalue>0">
                                    <xsl:if test="$rewardType!='Percent' and $rewardType!='Dollars'">
                                      (<xsl:value-of select="@deluxerewardvalue"/>)
                                    </xsl:if>
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="@premiumprice>0">
                              <tr>
                                <td valign="top">
                                  <xsl:choose>
                                    <xsl:when test="@premiumdiscountprice>0 and @premiumdiscountprice!=@premiumprice">
                                      <span style="color='red'"><strike>$<xsl:value-of select="format-number(@premiumprice,'#,###,##0.00')"/></strike></span>
                                      $<xsl:value-of select="format-number(@premiumdiscountprice,'#,###,##0.00')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="format-number(@premiumprice,'#,###,##0.00')"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
				  <xsl:choose>
					<xsl:when test="$premiumLabel != ''">
						&nbsp; - <xsl:value-of select="$premiumLabel"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp; - Premium
					</xsl:otherwise>
				  </xsl:choose>
                                  <xsl:if test="@premiumrewardvalue>0">
                                    <xsl:if test="$rewardType!='Percent' and $rewardType!='Dollars'">
                                      (<xsl:value-of select="@premiumrewardvalue"/>)
                                    </xsl:if>
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                          </table>
                        </td>

                        <!-- Product description -->
                        <td width="65%" valign="top">
                          <table width="100%" border="0" cellpadding="1" cellspacing="1">
                            <tr>
                              <td colspan="2">
                                <span class="label"><xsl:value-of disable-output-escaping="yes" select="@productname"/>&nbsp;&nbsp;&nbsp;&nbsp; #<xsl:value-of select="@productid"/>
                                  <xsl:if test="@upsellflag='Y'">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<font color="red">This is an Upsell Product</font>
                                  </xsl:if>
                                </span><br/>
                                <xsl:value-of disable-output-escaping="yes" select="@longdescription"/><br/>
                                <xsl:value-of select="@arrangementsize"/>
                              </td>
                            </tr>

                            <tr>
                              <td width="22%" align="left" valign="top">
                                <b>Delivery Method:</b>
                              </td>
                              <td width="78%">We can deliver this product as early as
                                <xsl:value-of select="@deliverydate"/> &nbsp;
                                <xsl:if test="@firstdeliverymethod='florist'">using an FTD Florist</xsl:if>
                                <xsl:if test="@firstdeliverymethod='carrier'">using <xsl:value-of select="@carrier"/>.</xsl:if>

                                <xsl:variable name="devtype1" select="@deliverytype1"/>
                                <xsl:if test="@deliverytype1!='' and @deliverytype1=/root/shippingMethods/shippingMethod[@description=$devtype1]/@description">
                                  <tr>
                                    <td width="20%">
                                      <xsl:value-of select="@deliverytype1"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:variable name="devtype2" select="@deliverytype2"/>
                                <xsl:if test="@deliverytype2!='' and @deliverytype2=/root/shippingMethods/shippingMethod[@description=$devtype2]/@description">
                                  <tr>
                                    <td width="20%">
                                      <xsl:value-of select="@deliverytype2"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:variable name="devtype3" select="@deliverytype3"/>
                                <xsl:if test="@deliverytype3!='' and @deliverytype3=/root/shippingMethods/shippingMethod[@description=$devtype3]/@description">
                                  <tr>
                                    <td width="20%">
                                      <xsl:value-of select="@deliverytype3"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:variable name="devtype4" select="@deliverytype4"/>
                                <xsl:if test="@deliverytype4!='' and @deliverytype4=/root/shippingMethods/shippingMethod[@description=$devtype4]/@description">
                                  <tr>
                                    <td width="20%">
                                      <xsl:value-of select="@deliverytype4"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:variable name="devtype5" select="@deliverytype5"/>
                                <xsl:if test="@deliverytype5!='' and @deliverytype5=/root/shippingMethods/shippingMethod[@description=$devtype5]/@description">
                                  <tr>
                                    <td width="20%">
                                      <xsl:value-of select="@deliverytype5"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:variable name="devtype6" select="@deliverytype6"/>
                                <xsl:if test="@deliverytype6!='' and @deliverytype6=/root/shippingMethods/shippingMethod[@description=$devtype6]/@description">
                                  <tr>
                                    <td width="20%">
                                      <xsl:value-of select="@deliverytype6"/>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </td>
                            </tr>
                            <xsl:if test="@discountpercent>0">
                              <tr>
                                <td width="22%" align="left">
                                  <br>Discount:</br>
                                </td>
                                <td width="78%" align="left">
                                  <xsl:value-of select="@discountpercent"/>%
                                </td>
                              </tr>
                            </xsl:if>

                            <xsl:choose>
                              <xsl:when test="key('pageData', 'requesteddeliverydate')/value">
                                <xsl:if test="@exceptioncode='U'">
                                  <script language="javascript"><![CDATA[
                                    if (selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
                                      exceptionMessageTrigger = true;
                                      document.write("<tr><td colspan=\"2\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionenddate"/><![CDATA[</font></td></tr>");
                                    }]]>
                                  </script>
                                </xsl:if>
                                <xsl:if test="@exceptioncode='A'">
                                  <script language="javascript"><![CDATA[
                                    if (selectedDate < exceptionStart || selectedDate > exceptionEnd) {
                                      exceptionMessageTrigger = true;
                                      document.write("<tr><td colspan=\"2\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptionenddate"/><![CDATA[</font></td></tr>");
                                    }]]>
                                  </script>
                                </xsl:if>
                                <script language="javascript"><![CDATA[
                                  if (exceptionMessageTrigger == true)
                                    document.write("<tr><td colspan=\"2\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="@exceptionmessage"/><![CDATA[</font></td></tr>");
                                  else
                                    document.write("<tr><td colspan=\"2\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="@exceptionmessage"/><![CDATA[</font></td></tr>");]]>
                                </script>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:if test="@exceptioncode='A'">
                                  <tr>
                                    <td colspan="2" align="left"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionenddate"/></font> </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="@exceptioncode='U'">
                                  <tr>
                                    <td colspan="2" align="left"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptionenddate"/></font> </td>
                                  </tr>
                                </xsl:if>
                                <tr>
                                  <td colspan="2" align="left"><font color="red"><xsl:value-of select="@exceptionmessage"/></font></td>
                                </tr>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="@displayspecialfee='Y'">
                              <tr>
                                <td colspan="2" align="left"><font color="red">A $12.99 surcharge will  be added to deliver this item to Alaska or Hawaii</font></td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="@displaycablefee='Y'">
                              <tr>
                                <td colspan="2" align="left"><font color="red">A $12.99 cable fee will  be added to deliver this item to an International country</font></td>
                              </tr>
                            </xsl:if>
                          </table>
                        </td>
                      </tr>
                      <xsl:if test="position()!=last()">
                        <tr>
                          <td colspan="3"><hr/></td>
                        </tr>
                      </xsl:if>
                      </xsl:for-each>
                    </table>
                  </div>
                </td>
              </tr>
              </table>
            </div>
            </xsl:when>

            <!-- No products found -->
            <xsl:otherwise>
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td colspan="3" class="tblheader">
                    <span class="textLinkWhite" onclick="javascript:doCategoriesAction();">Categories</span>
                    &nbsp;>&nbsp;
                    Product List
                  </td>
                </tr>
              </table>
              
              <table id="contentDiv" width="100%" border="0" cellpadding="30" cellspacing="2">
                <tr>
                  <td align="center">
                    <br/>No products were found matching your criteria.<br/>&nbsp;
                  </td>
                </tr>
              </table>
            </xsl:otherwise>
          </xsl:choose>

          <!-- Processing message div -->
          <div id="waitDiv" style="display:none">
            <table id="waitTable" width="100%" height="100%" border="0" cellpadding="0" cellspacing="2">
              <tr>
                <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                <td id="waitTD"  width="50%" class="waitMessage"></td>
              </tr>
            </table>
          </div>

        </td>
      </tr>
    </table>
    <!-- End Main table -->

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="closeButton" value="Close" tabindex="30" onClick="javascript:doCloseAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="33" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>
  </form>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>