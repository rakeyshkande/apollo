<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="scrubOrderSearch.xsl"/>
<xsl:import href="pendingOrderSearch.xsl"/>
<xsl:import href="reinstateOrderSearch.xsl"/>

<!-- Variables -->
<xsl:variable name="REINSTATE" select="'R'"/>
<xsl:variable name="PENDING" select="'P'"/>

<xsl:output method="html"/>
<xsl:output indent="yes"/>
<xsl:template match="/root">

  <xsl:choose>
    <xsl:when test="searchCriteria/criteria[name='sc_mode']/value=$PENDING">
      <xsl:call-template name="pendingOrderSearch">
        <xsl:with-param name="root" select="/"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:when test="searchCriteria/criteria[name='sc_mode']/value=$REINSTATE">
      <xsl:call-template name="reinstateOrderSearch">
        <xsl:with-param name="root" select="/"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="scrubOrderSearch">
        <xsl:with-param name="root" select="/"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>
</xsl:stylesheet>