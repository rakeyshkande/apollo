<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="orderAction.xsl"/>
<xsl:import href="footer.xsl"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

  <html>
  <head>
    <title>FTD - Pending Order</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"/>
  </head>
  <body onload="javascript:init();">
	
    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Pending Order'"/>
      <xsl:with-param name="showTime" select="false()"/>
      <xsl:with-param name="showExitButton" select="true()"/>
    </xsl:call-template>

    <!-- Pending Order template -->
    <xsl:call-template name="orderAction">
      <xsl:with-param name="popupType" select="'pending'"/>
    </xsl:call-template>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

  </body>
  </html>

</xsl:template>
</xsl:stylesheet>