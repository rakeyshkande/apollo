<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	
  <productList>
    <products>
      <xsl:for-each select="products/product">
        <xsl:sort select="@standardprice" data-type="number"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </products>
    <xsl:copy-of select="//params"/>
    <xsl:copy-of select="//shippingMethods"/>
    <xsl:copy-of select="//holidays"/>
    <xsl:copy-of select="//dateranges"/>
  </productList>
    
</xsl:template>
</xsl:stylesheet>