<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="searchCriteria" match="/root/searchCriteria/criteria" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Order Being Scrubbed</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"/>
    <script type="text/javascript" language="JavaScript">

    function doReturnToSearchAction(){
      var url = "ScrubSearchServlet?servlet_action=search_exit";
      performAction(url);
    }
    
    function performAction(url) {
      document.forms[0].action = url;
      document.forms[0].submit();
    }

    function doExitAction() {
      var url = "ScrubSearchServlet?servlet_action=main_exit";
      performAction(url);
    }

    </script>

</head>
<body>

  <form name="ScrubSearchForm" method="post" action="ScrubSearchServlet">
    <input type="hidden" name="servlet_action"/>
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>

    <input type="hidden" name="sc_confirmation_number" value="{key('searchCriteria', 'sc_confirmation_number')/value}"/>
    <input type="hidden" name="sc_email_address" value="{key('searchCriteria', 'sc_email_address')/value}"/>
    <input type="hidden" name="sc_last_name_flag" value="{key('searchCriteria', 'sc_last_name_flag')/value}"/>
    <input type="hidden" name="sc_last_name" value="{key('searchCriteria', 'sc_last_name')/value}"/>
    <input type="hidden" name="sc_phone_flag" value="{key('searchCriteria', 'sc_phone_flag')/value}"/>
    <input type="hidden" name="sc_phone" value="{key('searchCriteria', 'sc_phone')/value}"/>
    <input type="hidden" name="sc_mode" value="{key('searchCriteria', 'sc_mode')/value}"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Order Being Scrubbed'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="true()"/>
    </xsl:call-template>

      <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="100%" align="center">
            <table width="75%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
              <tr>
                <td>
                  <table width="100%" border="0" align="left" cellpadding="2" cellspacing="0">
                    <tr>
                      <td colspan="4" class="ErrorMessage">
                        The order you are attempting to view is currently being processed by 
                        <xsl:value-of select="orders/order/csr_id" />
                        &nbsp;since&nbsp;
                        <xsl:value-of select="orders/order/timestamp" />
                      </td>
                    </tr>
                    <!--tr>
                      <td>
                        <xsl:value-of select="orders/order/item_state" />
                        <xsl:choose>
                          <xsl:when test="orders/order/item_state = '3005'">
                            &nbsp;-&nbsp;Scrub In
                          </xsl:when>
                          <xsl:when test="orders/order/item_state = '3006'">
                            &nbsp;-&nbsp;Scrub Out
                          </xsl:when>
                          <xsl:when test="orders/order/item_state = '3007'">
                            &nbsp;-&nbsp;Pending In
                          </xsl:when>
                          <xsl:when test="orders/order/item_state = '3008'">
                            &nbsp;-&nbsp;Pending Out
                          </xsl:when>
                          <xsl:when test="orders/order/item_state = '3009'">
                            &nbsp;-&nbsp;Reinstate In
                          </xsl:when>
                          <xsl:when test="orders/order/item_state = '3010'">
                            &nbsp;-&nbsp;Reinstate Out
                          </xsl:when>
                        </xsl:choose>
                      </td>
                    </tr-->
                    <tr>
                      <td class="ColHeader" width="15%">
                        Order Number
                      </td>
                      <td class="ColHeader" width="15%">
                        Status
                      </td>
                      <td class="ColHeader" width="15%">
                        Delivery Date
                      </td>
                      <td class="ColHeader" width="55%">
                        Product
                      </td>
                    </tr>
                    <xsl:for-each select="orders/order">
                      <tr>
                        <td>
                          <xsl:value-of select="external_order_number" />
                        </td>
                        <td>
                          <xsl:choose>
                            <xsl:when test="status='2002'">
                              Scrub
                            </xsl:when>
                            <xsl:when test="status='2003'">
                              Scrub
                            </xsl:when>
                            <xsl:when test="status='2004'">
                              Removed
                            </xsl:when>
                            <xsl:when test="status='2005'">
                              Pending
                            </xsl:when>
                            <xsl:when test="status='2006'">
                              Processed
                            </xsl:when>
                            <xsl:when test="status='2007'">
                              Processed
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="status" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </td>
                        <td>
                          <xsl:value-of select="delivery_date" />
                        </td>
                        <td>
                          <xsl:value-of select="product_id" />
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="100%" align="center">
            <input type="button" name="searchButton" tabindex="2" value="Return To Search" onclick="javascript:doReturnToSearchAction();"/>
          </td>
        </tr>
      </table>

  </form>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>