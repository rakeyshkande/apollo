<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:template match="/email_content">

<html>
<head>
<script type="text/javascript" language="javascript">

  loadEmailContent();

  function loadEmailContent() {
    var form = parent.document.forms[0];
    form.email_subject.value = "<xsl:value-of disable-output-escaping="yes" select="email_subject"/>";
    form.email_body.value = "<xsl:value-of disable-output-escaping="yes" select="email_body"/>";
  }
</script>
</head>
<body>
</body>
</html>

</xsl:template>
</xsl:stylesheet>