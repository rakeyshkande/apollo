<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:variable name="product_images" select="key('pageData', 'product_images')/value"/>
<xsl:variable name="selected_product" select="/root/searchResults/searchResult[@selectedaddonid=@addonid]"/>
<xsl:variable name="curPrice" select="format-number($selected_product/@price, '#0.00')"/>
<xsl:template match="/root">

<html>
<head>
  <title>Lookup Greeting Card</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script language="javascript" src="../js/FormChek.js"/>
  <script language="javascript" src="../js/util.js"/>
  <script language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
      window.name = "VIEW_GREETING_CARD_DETAIL";
    }

  /*
   *  Actions
   */
    function doCloseAction(cardId, cardPrice){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      var retValue = new Array();
      retValue[0] = cardId;
      retValue[1] = cardPrice;
      top.returnValue = retValue;
      top.close();
    }

   
    //This function passes parameters from this page to a function on the calling page
    function goBack(){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      doCloseAction("BACK","");
    }

    //This function closes both the current window and the lookup window
    function closePage(){
      doCloseAction("","");
    }

    function doReturnToMainMenuAction(){
        closePage();
    }

    function processClosePage(){
      if (window.event.keyCode == 13)
        closePage();
    }

    function populatePage(cardId, cardPrice){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;

      doCloseAction(cardId, cardPrice); 
    }


    //This function refreshes the current page with the selected card
    //Or reopens the list page with the proper occasion
    function openPopup(){
      var form = document.forms[0];
      var id = form.cardIdInput[form.cardIdInput.selectedIndex].value;
      if (id != "") {
        form.action = form.action + "?card_id=" + id;
        form.target = window.name;
        form.submit();
      }
    }]]>
  </script>
</head>

<body onload="javascript:init();">

  <form name="form" method="get" action="PopupServlet">
    <input type="hidden" name="POPUP_ID" value="LOOKUP_GREETING_CARD"/>
    <input type="hidden" name="occasion_id" value="{key('pageData', 'occasion_id')/value}"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Greeting Card Detail'"/>
      <xsl:with-param name="showExitButton" select="'true'"/>
    </xsl:call-template>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td>&nbsp;
        </td>
        <td align="right">
          <img tabindex="2" src="../images/button_back_to_cards.gif" alt="Back to Card Selections" border="0" onclick="javascript:goBack()" onkeydown="javascript:goBack()"/>&nbsp;
          <img tabindex="3" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:closePage()" onkeydown="javascript:processClosePage()"/>
        </td>
      </tr>
    </table>

    <table width="98%" class="LookupTable" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td width="34%">
          <table width="100%" border="1" cellpadding="2" cellspacing="2">
            <tr>
              <td>
                <img width="280" height="260" src="{$product_images}{$selected_product/@addonid}.jpg"/>
              </td>
            </tr>
          </table>
        </td>
        <td width="66%" valign="top">
          <table width="98%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="label">Greeting Card:</td>
              <td>
                <xsl:value-of select="$selected_product/@description"/>
              </td>
            </tr>
            <tr>
              <td class="label">Front:</td>
              <td>
                <xsl:value-of select="$selected_product/@description"/>
              </td>
            </tr>
            <tr>
              <td class="label">Inside:</td>
              <td>
                <xsl:value-of select="$selected_product/@inside"/>
              </td>
            </tr>
            <tr>
              <td class="label">Price:</td>
              <td>&nbsp;$<xsl:value-of select="format-number($selected_product/@price,'#,###,##0.00')"/></td>
            </tr>
            <tr>
              <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2" align="center">
                <img onclick="javascript:populatePage('{$selected_product/@addonid}','{$curPrice}');" onkeydown="javascript:populatePage('{$selected_product/@addonid}','{$curPrice}');" tabindex="4" src="../images/button_select_card.gif"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td align="right">
          <img tabindex="5" src="../images/button_back_to_cards.gif" alt="Back to Card Selections" border="0" onclick="javascript:goBack()" onkeydown="javascript:goBack()"/>&nbsp;
          <img tabindex="6" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:closePage()" onkeydown="javascript:processClosePage()"/>
        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

  </form>

</body>
</html>

</xsl:template>
</xsl:stylesheet>