<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template name="orderAction">

  <xsl:param name="popupType"/>

  <form name="OrderActionForm" method="post" action="">
  <input type="hidden" name="comments"/>
  <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
  <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
  <input type="hidden" name="order_guid" value="{key('pageData','order_guid')/value}"/>
  <input type="hidden" name="item_number" value="{key('pageData','item_number')/value}"/>
  <input type="hidden" name="calling_servlet" value="{key('pageData','calling_servlet')/value}"/>
  <input type="hidden" name="master_order_number" value="{key('pageData','master_order_number')/value}"/>
  <input type="hidden" name="item_order_number" value="{key('pageData','item_order_number')/value}"/>
  <input type="hidden" name="company_id" value="{key('pageData','company_id')/value}"/>
  <input type="hidden" name="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>
  <input type="hidden" name="cart_flag" value="{key('pageData','cart_flag')/value}"/>
  <input type="hidden" name="order_origin" value="{key('pageData','order_origin')/value}"/>

  <!-- Main table -->
  <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
      <td>

        <!-- Order Information -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="5">Order Information for <xsl:value-of select="key('pageData', 'item_order_number')/value"/></td>
          </tr>
          <tr>
            <td width="25%" class="label">Master Order Number:</td>
            <td width="25%"><xsl:value-of select="key('pageData','master_order_number')/value"/></td>
            <td width="15%" class="label">Customer:</td>
            <td width="35%"><xsl:value-of select="key('pageData','buyer_full_name')/value"/></td>
          </tr>
        </table>

        <!-- Reason -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Reason</td>
          </tr>
          <tr>
            <td width="25%" class="label">Disposition:</td>
            <td width="75%">
              <select name="disposition" tabindex="1">
                <xsl:if test="boolean(/root/pageData/data[name='disposition_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <option value="">Select One</option>
                <xsl:for-each select="/root/dispositionList/disposition">
                  <xsl:variable name="id" select="disposition_id"/>
                  <option value="{disposition_id}">
                    <xsl:if test="key('pageData','disposition')/value=$id"><xsl:attribute name="SELECTED"/></xsl:if>
                    <xsl:value-of select="disposition_description"/>
                  </option>
                </xsl:for-each>
              </select>
              <span class="requiredFieldTxt">&nbsp;&nbsp;***</span>
            </td>
          </tr>
          <tr>
            <td width="25%"></td>
            <td width="75%" class="errorMessage">
              <xsl:value-of select="key('pageData', 'disposition_msg')/value"/>
            </td>
          </tr>
        </table>

        <!-- Actions taken -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Actions</td>
          </tr>
          <tr>
            <td width="25%" class="label">Was the customer called?</td>
            <td width="75%">
              <input type="radio" name="called_flag" tabindex="2" value="Y">
                <xsl:if test="boolean(/root/pageData/data[name='called_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <xsl:if test="key('pageData','called_flag')/value='Y'"><xsl:attribute name="CHECKED"/></xsl:if>
              </input>Yes
              <input type="radio" name="called_flag" tabindex="3" value="N">
                <xsl:if test="boolean(/root/pageData/data[name='called_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <xsl:if test="key('pageData','called_flag')/value='N'"><xsl:attribute name="CHECKED"/></xsl:if>
              </input>No
              <span class="requiredFieldTxt">&nbsp;&nbsp;***</span>
            </td>
          </tr>
          <tr>
            <td width="25%"></td>
            <td width="75%" class="errorMessage">
              <xsl:value-of select="key('pageData', 'called_flag_msg')/value"/>
            </td>
          </tr>
          
          <!-- REMOVED THIS REQUIREMENT
               <xsl:choose>
	            <xsl:when test="key('pageData','order_origin')/value = 'WLMTI' and $popupType = 'remove'">
	            	<tr style="display:none">
		            <td width="25%" class="label">Send an Email?</td>
		            <td width="75%">
		              <input type="radio" name="email_flag" tabindex="4" value="Y">
		                <xsl:if test="boolean(/root/pageData/data[name='email_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
		              </input>Yes
		              <input type="radio" name="email_flag" tabindex="5" value="N">
		                <xsl:if test="boolean(/root/pageData/data[name='email_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
		              	<xsl:attribute name="CHECKED"/>
		              </input>No
		              <span class="requiredFieldTxt">&nbsp;&nbsp;***</span>
		            </td>
		          </tr>
	            </xsl:when>
	            <xsl:otherwise>
		          <tr>
		            <td width="25%" class="label">Send an Email?</td>
		            <td width="75%">
		              <input type="radio" name="email_flag" tabindex="4" value="Y">
		                <xsl:if test="boolean(/root/pageData/data[name='email_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
		                <xsl:if test="key('pageData','email_flag')/value='Y'"><xsl:attribute name="CHECKED"/></xsl:if>
		              </input>Yes
		              <input type="radio" name="email_flag" tabindex="5" value="N">
		                <xsl:if test="boolean(/root/pageData/data[name='email_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
		                <xsl:if test="key('pageData','email_flag')/value='N'"><xsl:attribute name="CHECKED"/></xsl:if>
		              </input>No
		              <span class="requiredFieldTxt">&nbsp;&nbsp;***</span>
		            </td>
		          </tr>
          		</xsl:otherwise>
	      </xsl:choose>
	      -->
	      
	      <tr>
            <td width="25%" class="label">Send an Email?</td>
            <td width="75%">
              <input type="radio" name="email_flag" tabindex="4" value="Y">
                <xsl:if test="boolean(/root/pageData/data[name='email_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <xsl:if test="key('pageData','email_flag')/value='Y'"><xsl:attribute name="CHECKED"/></xsl:if>
              </input>Yes
              <input type="radio" name="email_flag" tabindex="5" value="N">
                <xsl:if test="boolean(/root/pageData/data[name='email_flag_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <xsl:if test="key('pageData','email_flag')/value='N'"><xsl:attribute name="CHECKED"/></xsl:if>
              </input>No
              <span class="requiredFieldTxt">&nbsp;&nbsp;***</span>
            </td>
          </tr>
	      
          <tr>
            <td width="25%"></td>
            <td width="75%" class="errorMessage">
              <xsl:value-of select="key('pageData', 'email_flag_msg')/value"/>
            </td>
          </tr>
        </table>

        <!-- Comments -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Comments</td>
          </tr>
          <tr>
            <td width="55%">
              <textarea name="comments1" tabindex="6" rows="6" cols="60" onkeyup="javascript:limitTextarea(this, 200);">
                <xsl:if test="boolean(/root/pageData/data[name='comments_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <xsl:value-of disable-output-escaping="yes" select="key('pageData', 'comments')/value"/>
              </textarea>
            </td>
            <td width="45%" valign="top" class="requiredFieldTxt">
              &nbsp;&nbsp;***
            </td>
          </tr>
          <tr>
            <td width="55%" class="errorMessage">
              <xsl:value-of select="key('pageData', 'comments_msg')/value"/>
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <!-- Buttons -->
  <table width="98%" border="0" cellpadding="2" cellspacing="2">
    <tr>
      <td width="20%" class="requiredFieldTxt">
        *** Required Fields
      </td>
      <td width="60%" align="center">
        <input type="button" name="submitAction" tabindex="7" value="Submit" onclick="javascript:doSubmitAction();"/>&nbsp;
        <input type="button" name="closeAction" tabindex="8" value="Close" onclick="javascript:doCloseAction();"/>
      </td>
      <td width="20%" align="right">
        <input type="button" name="exitButton" tabindex="9" value="Exit" onclick="javascript:doExitAction();"/>
      </td>
    </tr>
  </table>
  </form>
  
  <script type="text/javascript" language="javascript">
    var isValid = <xsl:value-of select="boolean(pageData/data[name='validation_flag']/value)"/>;<![CDATA[
    
  /*
   *  Initialization
   */
    function init(){
      if (isValid)
        returnToCart();

      window.dialogHeight = "450px";
      window.name = "ITEM_ACTION_VIEW";
      document.forms[0].disposition.focus();
    }

  /*
   *  Actions
   */
    function doSubmitAction(){
      var form = document.forms[0];
      form.comments.value = form.comments1.value;
      form.action = form.calling_servlet.value + "?servlet_action=validation";
      form.target = window.name;
      form.submit();
    }
    
    function doCloseAction(){
      top.close();
    }

    function doExitAction(){
      top.returnValue = new Array("_EXIT_");
      doCloseAction();
    }

    function returnToCart(){
      var form = document.forms[0];
      form.comments.value = form.comments1.value;
      var call = form.called_flag;
      var email = form.email_flag;

      var ret = new Array();]]>
      ret[0] = "<xsl:value-of select="key('pageData', 'disposition')/value"/>";<![CDATA[
      ret[1] = (call[0].checked) ? call[0].value : (call[1].checked) ? call[1].value : "";
      ret[2] = (email[0].checked) ? email[0].value : (email[1].checked) ? email[1].value : "";
      ret[3] = form.comments.value;
      top.returnValue = ret;
      top.close();
    }]]>
  </script>
  
</xsl:template>
</xsl:stylesheet>