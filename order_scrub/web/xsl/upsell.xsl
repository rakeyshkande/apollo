<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<!-- Variables -->
<xsl:variable name="searchType" select="root/parameters/searchType"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<xsl:variable name="productAttributeExclusion" select="boolean(string-length(/root/productAttributeExclusion/alertMessages/value) > 0)"/>

<html>
<head>
  <title>FTD - Upsell Product</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script language="javascript" src="../js/FormChek.js"/>
  <script language="javascript" src="../js/util.js"/>
  <script type="text/javascript" language="javascript">
  /*
   *  Global variables
   */
    var novator_id = "<xsl:value-of select="productList/products/product/@novatorid"/>";
    var product_images = "<xsl:value-of select="key('pageData', 'product_images')/value"/>";<![CDATA[

  /*
   *  Actions
   */
    function paramsHelper(){
      var form = document.forms[0];
      var elem = document.getElementById('UpsellForm').elements;
      var addon_str = "";
      for(var i = 0; i < elem.length; i++) {
          if (elem[i].name.indexOf("addonOrig_") > -1) {
             addon_str += elem[i].name + "=" + elem[i].value;
          }
      }
      var params =  "&international_service_fee=" + form.international_service_fee.value +
                    "&domestic_service_fee=" + form.domestic_service_fee.value +
                    "&master_order_number=" + form.master_order_number.value +
                    "&item_order_number=" + form.item_order_number.value +
                    "&buyer_full_name=" + form.buyer_full_name.value +
                    "&price_point_id=" + form.price_point_id.value +
                    "&domestic_flag=" + form.domestic_flag.value +
                    "&occasion_text=" + form.occasion_text.value +
                    "&occasion_id=" + form.occasion_id.value +
                    "&reward_type=" + form.reward_type.value +
                    "&script_code=" + form.script_code.value +
                    "&source_code=" + form.source_code.value +
                    "&postal_code=" + form.postal_code.value +
                    "&search_type=" + form.search_type.value +
                    "&company_id=" + form.company_id.value +
                    "&partner_id=" + form.partner_id.value +
                    "&orig_date=" + form.orig_date.value +
                    "&page_name=" + form.page_name.value +
                    "&jcp_flag=" + form.jcp_flag.value +
                    "&country=" + form.country.value +
                    "&state=" + form.state.value +
                    "&city=" + form.city.value +
                    addon_str +
                    "&delivery_date=" + form.delivery_date.value +
                    "&order_guid=" + form.order_guid.value +
                    getSecurityParams(false);
                    
      return params;
    }
   
    function doCategoriesAction(){
      document.forms[0].method = "get";

      var url = "CategoryServlet" +
                "?page_number=1" +
                paramsHelper();

      performAction(url);
    }

    function doExitAction(){
      top.returnValue = new Array("_EXIT_");
      doCloseAction();
    }

    function doCloseAction(){
      top.close();
    }

    function doSumbitAction(){
      if (backupFlag){
        doCloseAction();
      }
      performAction(document.forms[0].action);
    }

    function performAction(url){
      showWaitMessage("contentDiv", "wait");
      document.forms[0].action = url;
      document.forms[0].target = window.name;
      document.forms[0].submit();
    }]]>
  </script>


  <!-- O L D  S T U F F -->
  <script language="javascript"><![CDATA[
    var disableFlag = false;
    var backupFlag = false;]]>

    var searchType = "<xsl:value-of select="$searchType"/>";

  /*
   *  Initialization
   */
    function init(){
    	setNavigationHandlers();
      disableFlag = false;
      setScrollingDivHeight();

      <xsl:choose>
        <xsl:when test="key('pageData', 'jcp_flag')/value = 'JP' and key('pageData', 'countryType')/value = 'I'">
          openJCPPopup();
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="key('pageData', 'jcp_flag')/value = 'JP' and productList/products/product/@jcpcategory != 'A'">
                openJcpFoodItem();
            </xsl:when>
            <xsl:otherwise>
              <xsl:if test="key('pageData', 'displaySpecGiftPopup')/value = 'Y'">
                //display the Yes/No Floral DIV Tag
                openNoFloral();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayNoProductPopup')/value = 'Y'">
                //display the No Products DIV Tag
                openNoProduct();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayFloristPopup')/value = 'Y'">
                //display the No Products DIV Tag
                openOnlyFloral();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayCodifiedSpecial')/value = 'Y'">
                // display the Codified Special DIV Tag
                openCodifiedSpecial();
              </xsl:if>
              <xsl:if test="key('pageData', 'displayProductUnavailable')/value = 'Y'">
                // display the Product Unavailable DIV Tag
                openProductUnavailable();
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>

      //Display an alert when the page refreshes or reloads if the recipient is in Alaska or Hawaii
      <xsl:if test="key('pageData', 'displaySpecialFee')/value = 'Y'">
        alertHIorAK();
      </xsl:if><![CDATA[
      
      window.name = "UPSELL_VIEW";

    }

    function setScrollingDivHeight(){
      var upsellDetailDiv = document.getElementById("upsellDetail");
      upsellDetailDiv.style.height = document.body.clientHeight - upsellDetailDiv.getBoundingClientRect().top - 85;
    }


    function JCPPopChoice(inChoice){
      //JC penney popup for international change
      if (inChoice == 'Y'){
        var url = "DNISChangeServlet?source=productDetailJCP";
        document.forms[0].source.value = 'productDetailJCP';
        document.forms[0].action = url;
        document.forms[0].method = "get";
        document.forms[0].submit();
      }
    }

    function openJCPPopup(){
      JCPPopV = document.getElementById("JCPPop").style;
      scroll_top = document.body.scrollTop;
      JCPPopV.top = scroll_top+document.body.clientHeight/2-100;

      JCPPopV.width = 290;
      JCPPopV.height = 100;
      JCPPopV.left = document.body.clientWidth/2 - 50;
      JCPPopV.visibility = "visible";
    }

    function closeJCPPopup(){
      JCPPopV.visibility = "hidden";
    }

    function validateForm(){
      size = document.all.price.length;
      checked = true;]]>

      <xsl:if test="extraInfo/subtypes/product"><![CDATA[
        document.forms[0].subCodeId.className="TblText";
        if (document.forms[0].subCodeId.value == ""){
          document.forms[0].subCodeId.className="Error";
          document.forms[0].subCodeId.focus();
          checked = false;
        }]]>
      </xsl:if>

      <xsl:if test="productList/products/product/@color1 !=''"><![CDATA[
        document.forms[0].color1.className="TblText";
        if (document.forms[0].color1.value == ""){
          document.forms[0].color1.className="Error";
          if (checked != false){
            document.forms[0].color1.focus();
            checked = false;
          }
        }]]>
      </xsl:if>

      <xsl:if test="productList/products/product/@color2 !=''"><![CDATA[
        document.forms[0].color2.className="TblText";
        if (document.forms[0].color2.value == ""){
          document.forms[0].color2.className="Error";
          if (checked != false){
            document.forms[0].color2.focus();
            checked = false;
          }
        }]]>
      </xsl:if><![CDATA[

      return checked;
     }

    function viewLargeImage(){
      largeImage = document.getElementById("showLargeImage").style;
      scroll_top = document.body.scrollTop;
      largeImage.top = scroll_top + document.body.clientHeight/2 - 200;

      largeImage.width = 340;
      largeImage.height = 400;
      largeImage.left = 200;
      largeImage.visibility = "visible";
    }

    function closeLargeImage(){
      largeImage.visibility= "hidden";
    }

    // Change image of continue button to disabled and prevent submitting the page
    // but only if the country is domestic and the key stroke isn't a tab
    function disableContinue(){
      if (window.event.keyCode != 9){]]>
      <xsl:if test="key('pageData', 'domestic_flag')/value='D'">
        <xsl:if test="productList/products/product/@status = 'A'">
          disableFlag = true;
          document.images['submitButton'].src  = '../images/button_continue_disabled.gif';
        </xsl:if>
      </xsl:if><![CDATA[
      }
    }

    // Change image of continue button to enabled and allow submitting the page, but only if domestic
    function enableContinue(){]]>
      <xsl:if test="key('pageData', 'domestic_flag')/value = 'D'">
        <xsl:if test="productList/products/product/@status = 'A'">
          <![CDATA[
              disableFlag = false;
              document.images['submitButton'].src  = '../images/button_continue.gif';
          ]]>
        </xsl:if>
      </xsl:if><![CDATA[
    }

    /*  **************************************************************************
        This group of functions is used to display/hide the Specialty Gift
        only available for zip code message box.
      **************************************************************************  */
    function openNoFloral(){
      disableContinue();

      noFloralV = document.getElementById("noFloral").style;
      scroll_top = document.body.scrollTop;
      noFloralV.top = scroll_top+document.body.clientHeight/2-100;

      noFloralV.width = 290;
      noFloralV.height = 100;
      noFloralV.left = document.body.clientWidth/2 - 100;
      noFloralV.visibility = "visible";
    }

    function closeNoFloral(){
      disableContinue();
      noFloral.style.visibility = "hidden";
    }

  /*  **************************************************************************
      This group of functions is used to display/hide the No Product
      available for zip code message box
    **************************************************************************  */
    function openNoProduct(){
      disableContinue();

      noProductV = document.getElementById("noProduct").style;
      scroll_top = document.body.scrollTop;
      noProductV.top = scroll_top+document.body.clientHeight/2-100;

      noProductV.width = 290;
      noProductV.height = 100;
      noProductV.left=document.body.clientWidth/2 - 50;
      noProductV.visibility = "visible";
    }

    function closeNoProduct(){
      noProductV.visibility = "hidden";
    }

    function noProduct(){
      closeNoProduct();
    }

    /*  **************************************************************************
        This group of functions is used to display/hide the Product Unavailable
        for zip code message box
      **************************************************************************  */
    function openProductUnavailable(){
      disableContinue();

      productUV = document.getElementById("productUnavailable").style;
      scroll_top = document.body.scrollTop;
      productUV.top = scroll_top+document.body.clientHeight/2-100;

      productUV.width = 290;
      productUV.height = 100;
      productUV.left = document.body.clientWidth/2 - 50;
      productUV.visibility = "visible";
    }

    function openJcpFoodItem(){
      productUV = document.getElementById("jcpFood").style;
      scroll_top = document.body.scrollTop;
      productUV.top = scroll_top+document.body.clientHeight/2-100;

      productUV.width = 290;
      productUV.height = 100;
      productUV.left = document.body.clientWidth/2 - 50;
      productUV.visibility = "visible";
    }

    function closeJcpFoodItem(){
      productUV.visibility = "hidden";
      document.forms[0].backButton.focus();
    }

    function closeProductUnavailable(){
      productUV.visibility = "hidden";
    }

  /*  **************************************************************************
      This group of functions is used to display/hide the Floral Only
      for zip code message box
    **************************************************************************  */
  function openOnlyFloral(){
    disableContinue();

    onlyFloralV = document.getElementById("onlyFloral").style;
    scroll_top = document.body.scrollTop;
    onlyFloralV.top = scroll_top+document.body.clientHeight/2-100;

    onlyFloralV.width = 290;
    onlyFloralV.height = 100;
    onlyFloralV.left = document.body.clientWidth/2 - 50;
    onlyFloralV.visibility = "visible";
  }

  function closeOnlyFloral(){
    onlyFloralV.visibility = "hidden";
  }

  /*  **************************************************************************
      This group of functions is used to display/hide the Codified Special
      for zip code message box
    **************************************************************************  */
  function openCodifiedSpecial(){
    disableContinue();

    codifiedSpecialV = document.getElementById("codifiedSpecial").style;
    scroll_top = document.body.scrollTop;
    codifiedSpecialV.top = scroll_top+document.body.clientHeight/2-100;

    codifiedSpecialV.width = 290;
    codifiedSpecialV.height = 100;
    codifiedSpecialV.left = document.body.clientWidth/2 - 50;
    codifiedSpecialV.visibility = "visible";

    document.all.submitButton.src = "../images/button_back.gif";
    backupFlag = true;
  }

  function closeCodifiedSpecial(){
    codifiedSpecialV.visibility = "hidden";
  }

  function codifiedSpecial(){
    closeCodifiedSpecial();
  }]]>
  </script>
</head>

<body onload="javascript:init();">

  <form name="UpsellForm" id="UpsellForm" method="post" action="ProductDetailServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="international_service_fee" value="{key('pageData', 'international_service_fee')/value}"/>
    <input type="hidden" name="domestic_service_fee" value="{key('pageData', 'domestic_service_fee')/value}"/>
    <input type="hidden" name="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="occasion_text" value="{key('pageData', 'occasion_text')/value}"/>
    <input type="hidden" name="domestic_flag" value="{key('pageData', 'domestic_flag')/value}"/>
    <input type="hidden" name="occasion_id" value="{key('pageData', 'occasion_id')/value}"/>
    <input type="hidden" name="postal_code" value="{key('pageData', 'postal_code')/value}"/>
    <input type="hidden" name="source_code" value="{key('pageData', 'source_code')/value}"/>
    <input type="hidden" name="script_code" value="{key('pageData', 'script_code')/value}"/>
    <input type="hidden" name="reward_type" value="{key('pageData', 'reward_type')/value}"/>
    <input type="hidden" name="search_type" value="{key('pageData', 'search_type')/value}"/>
    <input type="hidden" name="total_pages" value="{key('pageData', 'total_pages')/value}"/>
    <input type="hidden" name="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="company_id" value="{key('pageData', 'company_id')/value}"/>
    <input type="hidden" name="partner_id" value="{key('pageData', 'partner_id')/value}"/>
    <input type="hidden" name="orig_date" value="{key('pageData', 'orig_date')/value}"/>
    <input type="hidden" name="upsell_id" value="{key('pageData', 'product_id')/value}"/>
    <input type="hidden" name="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
    <input type="hidden" name="country" value="{key('pageData', 'country')/value}"/>
    <input type="hidden" name="state" value="{key('pageData', 'state')/value}"/>
    <input type="hidden" name="city" value="{key('pageData', 'city')/value}"/>
    <input type="hidden" name="upsell_flag" value="Y"/>
    <input type="hidden" name="delivery_date" value="{key('pageData', 'delivery_date')/value}"/>
    <xsl:for-each select="/root/AddonsOriginal/Addon">
      <input type="hidden" name="addonOrig_{addon_id}" value="{addon_qty}"/>
    </xsl:for-each>

    <input type="hidden" name="source" value=""/>

    <!-- Header Template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Upsell Product'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="false()"/>
    </xsl:call-template>

    <!-- Main table-->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Order Information-->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Order Information for <xsl:value-of select="key('pageData', 'item_order_number')/value"/></td>
            </tr>
            <tr>
              <td width="25%" class="label">Master Order Number:</td>
              <td width="25%"><xsl:value-of select="key('pageData','master_order_number')/value"/></td>
              <td width="15%" class="label">Customer:</td>
              <td width="35%"><xsl:value-of select="key('pageData','buyer_full_name')/value"/></td>
            </tr>
            <tr>
              <td colspan="4" class="tblheader">
                <span class="textLinkWhite" onclick="javascript:doCategoriesAction();">Categories</span>
                &nbsp;>&nbsp;
                Upsell Product
              </td>
            </tr>
          </table>

          <!-- Content div -->
          <div id="contentDiv" style="display:block">
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td>

                  <!-- Scrolling div contiains product detail -->
                  <div id="upsellDetail" style="overflow:auto; width:100%; padding:0px; margin:0px;">

                    <!-- Product Information or Products unavialable message -->
                    <xsl:choose>
                      <xsl:when test="$productAttributeExclusion">
                        <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2">
                          <tr>
                            <td align="center" class="waitMessage">
                                  <xsl:value-of select="/root/productAttributeExclusion/alertMessages/value"/>
                            </td>
                          </tr>
                        </table>
                      </xsl:when>                    

                      <xsl:when test="upsellMaster/upsellDetail[@name = 'masterStatus']/@value = 'Y'">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <script language="JavaScript">
                                // Exception display trigger variables
                                var exceptionMessageTrigger = false;
                                var selectedDate = new Date("<xsl:value-of select="key('pageData', 'requestedDeliveryDate')/value"/>");
                                var exceptionStart = new Date("<xsl:value-of select="productList/products/product/@exceptionstartdate"/>");
                                var exceptionEnd = new Date("<xsl:value-of select="productList/products/product/@exceptiondnddate"/>");<![CDATA[

                                document.write("&nbsp; &nbsp; <img  border=\"0\" ");
                                document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
                                document.write("if (imag != 'npi_1.gif'){ ");
                                document.write("this.src='" + product_images + "npi_1.gif'}\" ");
                                document.write("src='" + product_images + novator_id + "_1.gif'/> ");]]>
                              </script>
                            </td>

                            <td colspan="3" valign="top">
                              <span class="label">
                                <xsl:value-of select="upsellMaster/upsellDetail[@name='masterName']/@value"/>&nbsp;&nbsp;
                                #<xsl:value-of select="upsellMaster/upsellDetail[@name='masterId']/@value"/>
                              </span>
                              <br/>
                              <span class="description">
                                <xsl:value-of select="upsellMaster/upsellDetail[@name='masterDescription']/@value"  disable-output-escaping="yes"/>
                              </span>

                              <xsl:if test="upsellMaster/upsellDetail[@name = 'noneAvailable']/@value = 'Y' or upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'N'">
                                <br/><br/>
                                <span class="tblText"><font color="red">
                                  None of the products are currently available for this delivery location.</font>
                               </span>
                              </xsl:if>
                            </td>
                          </tr>

                          <tr>
                            <td colspan="3">
                            &nbsp; &nbsp;
                            <a tabindex="1" href="#" onclick="javascript:viewLargeImage();">View Larger Image</a>
                            </td>
                          </tr>

                          <tr>
                            <td height="10" colspan="6"></td>
                          </tr>

                          <xsl:variable name="rewardType" select="key('pageData', 'reward_type')/value"/>
                          <xsl:variable name="finalSequence" select="count(productList/products/product)"/>
                          <xsl:variable name="finalProductName" select="productList/products/product[@upsellsequence = $finalSequence]/@upsellproductname"/>

                          <!-- List of products -->
                          <xsl:for-each select="productList/products/product">
                            <tr>
                              <xsl:variable name="upsellSequence" select="@upsellsequence"/>
                              <td id="productRow_{$upsellSequence}" valign="top" colspan="4">

                              <xsl:if test="@status = 'U' or @specialunavailable = 'Y' or upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'N'">
                                <script language="JavaScript">
                                  var productSeq = "<xsl:value-of select="@upsellsequence"/>";<![CDATA[
                                  document.getElementById("productRow_" + productSeq).style.backgroundColor = '#CCCCCC';]]>
                                </script>
                              </xsl:if>

                              <table width="100%" border="0" cellpadding="1" cellspacing="1">
                                <tr>
                                  <td height="1" colspan="6" class="tblheader"></td>
                                </tr>

                                <xsl:if test="upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'Y' and @upsellsequence != $finalSequence and upsellMaster/upsellDetail[@name='showScripting']/@value = 'Y'">
                                  <xsl:if test="@status = 'U' or @specialunavailable = 'Y'">
                                    <tr>
                                      <td colspan="6" class="ScreenPrompt">
                                        <xsl:value-of select="upsellExtraList/scripting/script[@fieldname='BASE_NOT_AVAIL_1']/@scripttext"/>
                                        <xsl:value-of select="@upsellproductname"/>
                                        <xsl:value-of select="upsellExtraList/scripting/script[@fieldname='BASE_NOT_AVAIL_2']/@scripttext"/>
                                        <xsl:value-of select="$finalProductName"/>
                                        <xsl:value-of select="upsellExtraList/scripting/script[@fieldname='BASE_NOT_AVAIL_3']/@scripttext"/>
                                      </td>
                                    </tr>
                                  </xsl:if>
                                </xsl:if>

                                <tr>
                                  <td width="15%" class="tblText" align="left" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td>
                                          <xsl:choose>
                                            <xsl:when test="@status='U' or @specialunavailable='Y' or upsellMaster/upsellDetail[@name='baseAvailable']/@value='N'">
                                              <input type="radio" class="tblText" name="product_id" disabled="true"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              <input type="radio" class="tblText" name="product_id" value="{@productid}">
                                                <xsl:if test="position()=1"><xsl:attribute name="CHECKED"/></xsl:if>
                                              </input>
                                           </xsl:otherwise>
                                          </xsl:choose>
                                        </td>
                                        <td>
                                          &nbsp; &nbsp;
                                          <xsl:choose>
                                            <xsl:when test="@standarddiscountprice > 0 and @standarddiscountprice != @standardprice">
                                              <span style="color='red'">
                                                <strike>$<xsl:value-of select="@standardprice"/></strike>&nbsp;
                                              </span>
                                              $<xsl:value-of select="@standarddiscountprice"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                              $<xsl:value-of select="@standardprice"/>
                                            </xsl:otherwise>
                                          </xsl:choose>
                                          <xsl:if test="@standardrewardvalue > 0">
                                            <xsl:if test="$rewardType != 'Percent' and $rewardType != 'Dollars'">
                                               &nbsp; (<xsl:value-of select="@standardrewardvalue"/>)
                                            </xsl:if>
                                          </xsl:if>
                                           &nbsp; &nbsp;
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;&nbsp;</td>                                      
                                        <td>                                      
									    &nbsp; &nbsp; 
										  <xsl:if test="@allowfreeshippingflag = 'N'">
											<img src="../images/noFSicon.jpg" alt="No Free Shipping" border="0"/>
										  </xsl:if>
									    </td>
									  </tr>
                                    </table>
                                  </td>

                                  <td width="85%" colspan="2" class="tblText">
                                    <span class="label"> <xsl:value-of select="@upsellproductname" />&nbsp;&nbsp;&nbsp;&nbsp; #<xsl:value-of select="@productid"/> </span> <br/>
                                      <xsl:value-of select="@longdescription"  disable-output-escaping="yes"/>
                                      <br/>
                                      <xsl:value-of select="@arrangementsize"/>
                                  </td>
                                </tr>
                                <tr>
                                  <td></td>
                                  <td width="15%" class="label" valign="top">Delivery Method:</td>
                                  <td>We can deliver this product as early as
                                    <xsl:value-of select="@deliverydate"/>
                                    <xsl:if test="@shipmethodflorist = 'Y'">
                                      &nbsp;using an FTD Florist.
                                      <xsl:if test="@shipmethodcarrier = 'Y'">
                                        Or it can be delivered
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="@shipmethodcarrier = 'Y'">
                                      &nbsp;using <xsl:value-of select="@carrier"/>.
                                    </xsl:if>

                                    <xsl:variable name="devtype1" select="@deliverytype1"/>

                                    <xsl:if test="@deliverytype1 !='' and @deliverytype1 = shippingMethods/shippingMethod/method[@description=$devtype1]/@description">
                                      <tr>
                                        <td></td>
                                        <td class="tblText">
                                          <xsl:value-of select="@deliverytype1"/>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:variable name="devtype2" select="@deliverytype2"/>
                                    <xsl:if test="@deliverytype2 !='' and @deliverytype2 = shippingMethods/shippingMethod/method[@description=$devtype2]/@description">
                                      <tr>
                                      <td></td>
                                        <td class="tblText">
                                          <xsl:value-of select="@deliverytype2"/>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:variable name="devtype3" select="@deliverytype3"/>
                                    <xsl:if test="@deliverytype3 !='' and @deliverytype3 = shippingMethods/shippingMethod/method[@description=$devtype3]/@description">
                                      <tr>
                                      <td></td>
                                        <td class="tblText">
                                          <xsl:value-of select="@deliverytype3"/>
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:variable name="devtype4" select="@deliverytype4"/>
                                    <xsl:if test="@deliverytype4 !='' and @deliverytype4 = shippingMethods/shippingMethod/method[@description=$devtype4]/@description">
                                      <tr>
                                      <td></td>
                                        <td class="tblText">
                                          <xsl:value-of select="@deliverytype4"/>
                                        </td>
                                        <td class="tblText">
                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:variable name="devtype5" select="@deliverytype5"/>
                                    <xsl:if test="@deliverytype5 !='' and @deliverytype5 = shippingMethods/shippingMethod/method[@description=$devtype5]/@description">
                                      <tr>
                                      <td></td>
                                        <td class="tblText">
                                          <xsl:value-of select="@deliverytype5"/>
                                          </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:variable name="devtype6" select="@deliverytype6"/>
                                    <xsl:if test="@deliverytype6 !='' and @deliverytype6 = shippingMethods/shippingMethod/method[@description=$devtype6]/@description">
                                      <tr>
                                      <td></td>
                                        <td class="tblText">
                                          <xsl:value-of select="@deliverytype6"/>
                                        </td>
                                      </tr>
                                    </xsl:if>
                                  </td>
                                </tr>

                                <xsl:if test="@discountpercent > 0">
                                  <tr>
                                    <td class="TblTextBold" align="left">
                                      Discount: </td>
                                    <td class="tblText" align="left"> <xsl:value-of select="@discountpercent"/>% </td>
                                  </tr>
                                </xsl:if>

                                <xsl:if test="@status = 'U' or @specialunavailable = 'Y'">
                                  <xsl:if test="upsellMaster/upsellDetail[@name='baseAvailable']/@value = 'Y'">
                                    <script language="JavaScript"><![CDATA[
                                      document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for the selected delivery location.<![CDATA[</font></td></tr>");]]>
                                    </script>
                                  </xsl:if>
                                </xsl:if>

                                <xsl:choose>
                                  <xsl:when test="pageData/data[@name='requestedDeliveryDate']/@value">
                                    <xsl:if test="@exceptioncode = 'U'">
                                      <script language="JavaScript"><![CDATA[
                                        if (selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
                                          exceptionMessageTrigger = true;
                                          document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptiondnddate"/><![CDATA[</font></td></tr>");
                                        }]]>
                                      </script>
                                    </xsl:if>

                                    <xsl:if test="@exceptioncode = 'A'">
                                      <script language="JavaScript"><![CDATA[
                                        if (selectedDate < exceptionStart || selectedDate > exceptionEnd) {
                                          exceptionMessageTrigger = true;
                                          document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="@exceptionStartDate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="@exceptiondnddate"/><![CDATA[</font></td></tr>");
                                        }]]>
                                      </script>
                                    </xsl:if>

                                    <script language="JavaScript"><![CDATA[
                                      if (exceptionMessageTrigger == true)
                                        document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="@exceptionMessage"/><![CDATA[</font></td></tr>");
                                      else
                                        document.write("<tr><td></td><td colspan=\"2\" class=\"tblText\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="@exceptionMessage"/><![CDATA[</font></td></tr>");]]>
                                    </script>
                                  </xsl:when>

                                  <xsl:otherwise>
                                    <xsl:if test="@exceptioncode = 'A'">
                                      <tr>
                                        <td></td>
                                        <td colspan="3" class="tblText" align="left"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptiondnddate"/></font> </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="@exceptioncode = 'U'">
                                      <tr>
                                        <td></td>
                                        <td colspan="3" class="tblText" align="left"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="@exceptionStartDate"/>&nbsp;to&nbsp;<xsl:value-of select="@exceptiondnddate"/></font> </td>
                                      </tr>
                                    </xsl:if>

                                    <tr>
                                      <td></td>
                                      <td colspan="3" class="tblText" align="left"><font color="red"><xsl:value-of select="@exceptionMessage"/></font></td>
                                    </tr>
                                  </xsl:otherwise>
                                </xsl:choose>

                                <xsl:if test="@displayspecialfee = 'Y'">
                                  <tr>
                                    <td colspan="2" class="tblText" align="left"> <font color="red">A $12.99 surcharge will  be added to deliver this item to Alaska or Hawaii</font> </td>
                                  </tr>
                                </xsl:if>
                                <xsl:if test="@displaycablefee = 'Y'">
                                  <tr>
                                    <td colspan="2" class="tblText" align="left"> <font color="red">A $12.99 cable fee will  be added to deliver this item to an International country</font> </td>
                                  </tr>
                                </xsl:if>

                              </table>
                            </td>
                          </tr>
                          </xsl:for-each>
                        </table>
                      </xsl:when>

                      <!-- Product not avaialable -->
                      <xsl:otherwise>
                        <table width="100%" height="100%" border="0" cellpadding="2" cellspacing="2">
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="waitMessage"><center>This product is unavailable.</center></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                      </xsl:otherwise>
                    </xsl:choose>

                  </div>
                  <!-- End scrolling div -->

                </td>
              </tr>
            </table>
          </div>
          <!-- End Content div -->

          <!-- Processing message div -->
          <div id="waitDiv" style="display:none">
            <table id="waitTable" width="100%" border="0" cellpadding="0" cellspacing="2" height="100%">
              <tr>
                <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                <td id="waitTD"  width="50%" class="waitMessage"></td>
              </tr>
            </table>
          </div>

        </td>
      </tr>
    </table>
    <!-- End Main Table -->

    <!-- Action Buttons -->
      <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
        <tr>
          <td width="10%"></td>
          <td width="80%" align="center">
            <xsl:choose>
             <xsl:when test="$productAttributeExclusion">
                <input type="button" name="backButton" tabindex="37" value="Back" onclick="javascript:doCategoriesAction();"/>
             </xsl:when>
             <xsl:when test="productList/products/product[@upsellsequence = '1']/@status = 'A' and upsellMaster/upsellDetail[@name = 'masterStatus']/@value = 'Y'">
                <input type="button" name="submitButton" tabindex="37" value="Submit" onclick="javascript:doSumbitAction();"/>
              </xsl:when>
              <xsl:otherwise>
                <input type="button" name="backButton" tabindex="37" value="Back" onclick="javascript:doCategoriesAction();"/>
              </xsl:otherwise>
            </xsl:choose>
            &nbsp;<input type="button" name="closeButton" tabindex="37" value="Close" onclick="javascript:doCloseAction();"/>
          </td>
          <td width="10%" align="right">
            <input type="button" name="exitButton" value="Exit" tabindex="33" onclick="javascript:doExitAction();"/>
          </td>
        </tr>
      </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

  </form>

  <!-- Large image popup -->
  <div id="showLargeImage" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <center>
    <br/><br/>
    <script language="JavaScript"><![CDATA[
      document.write("<img ")
      document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
      document.write("if (imag!='npi_2.jpg'){ ")
      document.write("this.src='" + product_images + "npi_2.jpg'}\" ")
      document.write("src='" + product_images + novator_id + ".jpg'/> ");]]>
    </script>
    <br/><br/>
    <a href="#" onclick="javascript:closeLargeImage();">close</a>
    </center>
  </div>

  <!--No Floral DIV -->
  <div id="noFloral" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table>
            <tr>
              <td align="center">Floral Items can no longer be delivered to this zip/postal code area.
                <br/><br/>
                  Would you be interested in our specialty items?
              </td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_no.gif" onclick="javascript:closeNoFloral()"/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <img src="../images/button_yes.gif" onclick="javascript:cancelItem()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--No Product DIV -->
  <div id="noProduct" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                No Items can be delivered to this Zip/Postal Code at this time.
                <br/><br/>
                Please enter a new Zip/Postal Code or select <U>Occasion</U>&nbsp; to start over.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img  src="../images/button_ok.gif" onclick="javascript:noProduct()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Product Unavailable DIV -->
  <div id="productUnavailable" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                This item cannot be delivered to this Zip/Postal Code at this time.
                <br/><br/>
                Please enter a new Zip/Postal Code or select another item to order.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:closeProductUnavailable();"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--JCPenney Food DIV -->
  <div id="jcpFood" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                We're sorry, food products are unavailable for purchase using a JCPenney credit card.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img  src="../images/button_ok.gif" onclick="javascript:closeJcpFoodItem()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Floral only DIV -->
  <div id="onlyFloral" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                Only Floral items can be delivered to this Zip/Postal Code.
                <br/><br/>
                Would you be interested in selecting a floral item?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_no.gif" onclick="javascript:closeOnlyFloral()"/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <img src="../images/button_yes.gif" onclick="javascript:cancelItem()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Codified Special DIV -->
  <div id="codifiedSpecial" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                We're sorry the product you've selected is currently not available in the
                zip code you entered.  However, the majority of our flowers can be delivered
                to this zip code.
                <br/><br/>
                If this is an incorrect zip code, please re-enter the zip code
                below and "Click to Accept".  Otherwise, please click the "Back" button to
                return to shopping.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- JCP Popup DIV -->
  <div id="JCPPop" style="visibility: hidden; position: absolute; top: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                At this time, JC Penney accepts orders going to or coming from the 50 United States
                only.  We can process your Order using any major credit card through FTD.com.
                <br/><br/>
                Would you like to proceed?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <img src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

</body>
</html>

</xsl:template>
</xsl:stylesheet>