<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:template name="errorMessage">
<xsl:param name="node"/>

  <tr>
    <td width="30%"></td>
    <td width="70%" class="errorMessage">
      <xsl:for-each select="$node/messages/message">
        <xsl:value-of select="description"/><br/>
      </xsl:for-each>
    </td>
  </tr>

</xsl:template>
</xsl:stylesheet>