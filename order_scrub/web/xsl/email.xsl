<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
  <head>
    <title>FTD - Create Email</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/FormChek.js"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript">
      var isValid = <xsl:value-of select="boolean(key('pageData', 'validation_flag')/value)"/>;<![CDATA[

    /*
     *  Initialization
     */
      function init(){
        if (isValid)
          returnToCart();

        window.dialogHeight = "530px";
        window.name = "CREATE_EMAIL";
        document.forms[0].message_id.focus();
      }
      
    /*
     *  Actions
     */
      function doSubmitAction(){
        var url = "EmailServlet" +
                  "?servlet_action=validation";
        performAction(url);
      }
      
      function doCloseAction(){
        var url = "EmailServlet" +
                  "?servlet_action=close";
        
        performAction(url);
      }

      function doSpellCheckAction(value, name) {
        value = escape(value).replace(/\+/g,"%2B");
        var url = "SpellCheckServlet?content=" + value + "&callerName=" + name;
        window.open(url,"Spell_Check","toolbar=no,resizable=yes,scrollbars=yes,width=500,height=300");
      }

      function doExitAction(){
        top.returnValue = new Array("_EXIT_");
        top.close();
      }

      function returnToCart(){
        var form = document.forms[0];
        var ret = new Array();
        ret[0] = form.disposition.value;
        ret[1] = form.called_flag.value;
        ret[2] = form.email_flag.value;
        ret[3] = form.comments.value;]]>
        ret[4] = "<xsl:value-of select="key('pageData', 'message_id')/value"/>";<![CDATA[
        ret[5] = form.email_subject.value;
        ret[6] = form.email_body.value
        top.returnValue = ret;
        top.close();
      }

      function performAction(url){
        var form = document.forms[0];
        form.comments.value = form.comments.value;
      
        document.forms[0].action = url;
        document.forms[0].target = window.name;
        document.forms[0].submit();
      }

    /*
     *  Text transfer
     */
      var cursorLocation = 0;
      function saveCursorPosition(elem) {
        if ( elem.isTextEdit )
          elem.caretPos = document.selection.createRange();
      }
      function getCursorLocation() {
        var elem = document.forms[0].email_body;
        if ( elem.isTextEdit && elem.caretPos ) {
          var bookmark = "~";
          var orig = elem.value;
          elem.caretPos.text = bookmark;
          cursorLocation = elem.value.search( bookmark );
          elem.value = orig;
        }
        return cursorLocation;
      }
      function doTextTransfer(){
        var loc = getCursorLocation();
        var form = document.forms[0];
        var statement = form.statements;
        var body = form.email_body;

        var before, after, newBody;
        if (statement.selectedIndex > -1){
          before = body.value.substring(0, loc);
          after = body.value.substring(loc, body.length);
          newBody = before;
          newBody += statement[statement.selectedIndex].value;
          newBody += after;
          body.value = newBody;
        }
      }

    /*
     *  Email subject and body refresh
     */
      function doMessageRequest(select){
        var form = document.forms[0];
        if (select.selectedIndex > 0){
          var iframe = document.getElementById("messageRequestFrame");
          iframe.src =  "EmailServlet" +
                        "?servlet_action=load_content" +
                        "&order_guid=" + form.order_guid.value +
                        "&item_number=" + form.item_number.value +
                        "&message_id=" + select[select.selectedIndex].value +
                        "&cart_flag=" + form.cart_flag.value +
                        getSecurityParams(false);
        }
        else {
          form.email_subject.value = "";
          form.email_body.value = "";
        }
      }
    ]]>
    </script>
  </head>

  <body onload="javascript:init();">
  <form name="EmailEditForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="item_number" value="{key('pageData', 'item_number')/value}"/>
    <input type="hidden" name="calling_servlet" value="{key('pageData', 'calling_servlet')/value}"/>
    <input type="hidden" name="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="company_id" value="{key('pageData', 'company_id')/value}"/>
    <input type="hidden" name="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="disposition" value="{key('pageData', 'disposition')/value}"/>
    <input type="hidden" name="called_flag" value="{key('pageData', 'called_flag')/value}"/>
    <input type="hidden" name="email_flag" value="{key('pageData', 'email_flag')/value}"/>
    <input type="hidden" name="comments" value="{key('pageData', 'comments')/value}"/>
    <input type="hidden" name="cart_flag" value="{key('pageData','cart_flag')/value}"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Create Email'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="false()"/>
    </xsl:call-template>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <!-- Order Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr><td class="tblheader" colspan="4">Order Information for <xsl:value-of select="key('pageData', 'item_order_number')/value"/></td></tr>
            <tr>
              <td width="20%" class="label" nowrap="true">Master Order Number:</td>
              <td width="30%"><xsl:value-of select="key('pageData', 'master_order_number')/value"/></td>
              <td width="20%" class="label">Customer:</td>
              <td width="30%"><xsl:value-of select="key('pageData', 'buyer_full_name')/value"/></td>
            </tr>
          </table>

          <!-- Email Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr><td colspan="2" class="tblheader">Email</td></tr>
            <tr>
              <td colspan="2" align="center" class="errorMessage">
                <xsl:value-of select="key('pageData', 'email_send_msg')/value"/>
              </td>
            </tr>
            <tr>
              <td width="20%" class="label">Stock Email:</td>
              <td width="80%">
                <select name="message_id" tabindex="1" onchange="javascript:doMessageRequest(this);">
                  <xsl:if test="boolean(key('pageData', 'message_id_msg')/value)"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                  <option value="">Select a Stock Email</option>
                  <xsl:for-each select="messageDescList/messageDesc">
                    <option value="{stock_message_id}">
                      <xsl:if test="key('pageData', 'message_id')/value=stock_message_id"><xsl:attribute name="SELECTED"/></xsl:if>
                      <xsl:value-of select="description"/>
                    </option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
            <tr>
              <td width="20%"></td>
              <td width="80%" class="errorMessage">
                <xsl:value-of select="key('pageData', 'message_id_msg')/value"/>
              </td>
            </tr>
            <tr>
              <td width="20%" class="label">Subject:</td>
              <td width="80%">
                <input type="text" name="email_subject" tabindex="2" size="80" maxlength="80" value="{key('pageData', 'email_subject')/value}">
                  <xsl:if test="boolean(key('pageData', 'email_subject_msg')/value)"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                </input>
              </td> 
            </tr>
            <tr>
              <td width="20%"></td>
              <td width="80%" class="errorMessage">
                <xsl:value-of select="key('pageData', 'email_subject_msg')/value"/>
              </td>
            </tr>
            <tr>
              <td width="20%" class="label" valign="top">
                Message:<br/>
                <input type="button" name="spellCheckEmail" value="Spell Check" onclick="javascript:doSpellCheckAction(email_body.value, 'email_body');"/>
              </td>
              <td width="80%">
                <textarea name="email_body" tabindex="3" rows="10" cols="82" onkeypress="javascript:limitTextarea(this, 4000);" onselect="saveCursorPosition(this)" onclick="saveCursorPosition(this)" onkeyup="saveCursorPosition(this)" >
                  <xsl:if test="boolean(key('pageData', 'email_body_msg')/value)"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                  <xsl:value-of disable-output-escaping="yes" select="key('pageData', 'email_body')/value"/>
                </textarea>
              </td>
            </tr>
            <tr>
              <td width="20%"></td>
              <td width="80%" class="errorMessage">
                <xsl:value-of select="key('pageData', 'email_body_msg')/value"/>
              </td>
            </tr>
            <tr>
              <td width="20%">&nbsp;</td>
              <td width="80%">&nbsp;&nbsp;<input type="button" name="selectMessageButton" value="^" onclick="javascript:doTextTransfer();"/></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>
                <select name="statements" tabindex="4" size="4">
                  <xsl:for-each select="statementList/statement">
                    <option value="{.}"><xsl:value-of select="."/></option>
                  </xsl:for-each>
                </select>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- Buttons -->
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="60%" align="center">
          <input type="button" name="submitAction" tabindex="5" value="Submit" onclick="javascript:doSubmitAction();"/>&nbsp;
          <input type="button" name="closeAction" tabindex="6" value="Close" onclick="javascript:doCloseAction();"/>
        </td>
        <td width="20%" align="right">
          <input type="button" name="exitButton" tabindex="7" value="Exit" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

    <iframe id="messageRequestFrame" border="0px" height="0px" width="0px"></iframe>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

  </form>
  </body>
</html>

</xsl:template>
</xsl:stylesheet>