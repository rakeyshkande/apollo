<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:template match="/root">

<html>
<head>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
</head>
<body>
  <form name="StatusResetForm" method="post" action="">
  
  	<input type="hidden" name="order_guid" value="{search_result_list/search_result/order_guid}"/>
  
	<table width="100%"><tr><td>

	<!-- Results table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Search Results column headers -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="10%" class="colHeaderCenter">Master Order Number</td>
              <td width="5%" class="colHeaderCenter">Status</td>
              <td width="10%" class="colHeaderCenter">Customer Name</td>
              <td width="5%" class="colHeaderCenter">CSR ID</td>
              <td width="20%" class="colHeaderCenter">Status Update</td>
            </tr>
            <tr>
              <td colspan="7" width="100%" align="center">

                <!-- Scrolling div contiains search results -->
                <div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <xsl:for-each select="search_result_list/search_result">
                      <xsl:sort select="@sort_number" data-type="number"/>
                      <tr>
                        <td width="10%" align="center"><xsl:value-of select="master_order_number"/></td>
                        <td width="5%" align="center"><xsl:value-of select="order_status"/></td>
                        <td width="10%" align="center"><xsl:value-of select="buyer_first_name"/>&nbsp;<xsl:value-of select="buyer_last_name"/></td>
                        <td width="5%" align="center"><xsl:value-of select="csr_id"/></td>
                        <td width="20%" align="center">
                        	<select name="updated_status">
                        		<option value="1002">1002 - Invalid Header</option>
                        		<option value="1003">1003 - Valid Header / Invalid Item</option>
                        		<option value="1004">1004 - Valid</option>
                        		<option value="1005">1005 - Scrub</option>
                        		<option value="1006">1006 - Removed</option>
                        		<option value="1007">1007 - Pending</option>
                        		<option value="1008">1008 - Complete</option>
                        		<option value="1009">1009 - Bulk</option>
                        		<option value="1010">1010 - Abandoned</option>
                        		<option value="1013">1013 - Reinstate</option>
                        	</select>
                        </td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
            
          </table>

        </td>
      </tr>
    </table>
    </td></tr>
    <tr></tr>
        <tr>
        	<td colspan="4" align="middle"><input type="submit"/></td>
        </tr>
    </table>
    
  
  </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>