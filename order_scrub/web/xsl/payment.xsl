<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:key name="validationField" match="/root/validation/order/header/data" use="@field_name"/>

<!-- Variables -->
<xsl:variable name="creditCardExpYear" select="key('pageData', 'cc_exp_year')/value"/>
<xsl:variable name="creditCardExpMonth" select="key('pageData', 'cc_exp_month')/value"/>
<xsl:variable name="selectedPaymentMethod" select="key('pageData', 'payment_method_id')/value"/>
<xsl:variable name="selectedPaymentMethodType" select="key('pageData', 'payment_method')/value"/>
<xsl:variable name="isGCGDValid" select="not(boolean(/root/validation/order/header/data[@field_name='gc_payment_info'] or /root/validation/order/header/data[@field_name='gc_payment_amt'] ))"/>
<xsl:variable name="paymentMethodAll" select="key('pageData', 'payment_method_all')/value"/>
<xsl:variable name="hasMorePayments" select="boolean(string-length($paymentMethodAll) > 1)"/>
<xsl:variable name="isGCGDInfoValid" select="not(boolean(/root/validation/order/header/data[@field_name='gc_payment_info']))"/>
<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

  <html>
  <head>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript">
    /*
     *  Global variables
     * 
     */
      var isCCValid    = <xsl:value-of select="not(boolean(validation/order/header/data[@field_name='cc_type'] or validation/order/header/data[@field_name='cc_number'] or validation/order/header/data[@field_name='cc_exp_date'] or key('pageData', 'no_charge_username_msg')/value or key('pageData', 'no_charge_password_msg')/value))"/>;
      var isGCGDValid = <xsl:value-of select="not(boolean(validation/order/header/data[@field_name='gc_payment_info'] or validation/order/header/data[@field_name='gc_payment_amt'] ))"/>;
      var hasValidated = <xsl:value-of select="boolean(pageData/data[name='validation_flag']/value)"/>;
      var paymentMethodAll = "<xsl:value-of select="key('pageData', 'payment_method_all')/value"/>";
      var showNcDiv  = <xsl:value-of select="boolean($selectedPaymentMethod='NC')"/>;
      var showCcDiv  = <xsl:value-of select="boolean($selectedPaymentMethod!='IN') and boolean($selectedPaymentMethodType!='P')"/>;
      var showGcDiv  = <xsl:value-of select="boolean((contains(pageData/data[name='payment_method_all']/value,'G') and pageData/data[name='gc_number1'][value!='']) or (contains(pageData/data[name='payment_method_all']/value,'R')))"/>;
      var ccExpYear  = parseInt("<xsl:value-of select="key('pageData', 'cc_exp_year')/value"/>", 10);
      var ccExpMonth = parseInt("<xsl:value-of select="key('pageData', 'cc_exp_month')/value"/>", 10);
      var temp = <xsl:value-of select="$isGCGDInfoValid"/>;
      var temp_order_amount = "<xsl:value-of select="key('pageData', 'order_amount')/value"/>";
      var temp_gcgd_amount = 0;
      <xsl:for-each select="pageData/data[starts-with(name, 'gc_number')]">
              <xsl:variable name="index" select="substring(name,string-length(name),string-length(name))"/>
             temp_gcgd_amount = parseFloat(temp_gcgd_amount) + parseFloat("<xsl:value-of select="/root/pageData/data[name=concat('gc_amount', $index)]/value"/>", 10);
      </xsl:for-each>
      var fieldAccess = <xsl:value-of select="key('pageData', 'disable_flag')/value"/>;<![CDATA[
      var disableFields = new Array("payment_method_id", "cc_number_input", "cc_exp_month", "cc_exp_month", "cc_exp_year", "no_charge_username", "no_charge_password", "submitButton");
      
    /*
     *  Initialization
     */
      function init(){
        if (hasValidated && (isCCValid && (isGCGDValid || (!isGCGDValid && paymentMethodAll.length > 1))))
          returnToCart();

		var ord_amt = temp_order_amount;
		var gc_amt = temp_gcgd_amount;
		var showPaymentDiv = true;
		if(parseFloat(gc_amt) >= parseFloat(ord_amt)){			
			showPaymentDiv = false;
			showCcDiv = false;
		}			
		
		setVisibility("paymentMethodsDiv", showPaymentDiv);
        setVisibility("creditCardDiv", showCcDiv);
        setVisibility("giftCertificateDiv", showGcDiv);
        setVisibility("noChargeDiv", showNcDiv);
        setVisibility("ccTypeErrorDiv", true);
        setCCMonth();
        setCCYear();
        setFieldsAccess(disableFields, fieldAccess);
        window.name = "PAYMENT_EDIT";

        if (!fieldAccess && showPaymentDiv)
          document.forms[0].payment_method_id.focus();
      }

      function setCCMonth(){
        var months = document.forms[0].cc_exp_month;
        for (var i = 0; i < months.length; i++){
          if (months[i].value == ccExpMonth){
            months.selectedIndex = i;
            break;
          }
        }
      }

      function setCCYear(){
        var NUM_YEARS = 14;
        var form = document.forms[0];
        var year = new Date().getFullYear();
        
        for (var i = 0; i <= NUM_YEARS; i++, year++){
          if (year == ccExpYear)
            form.cc_exp_year.options[i] = new Option(year, year, true, true);
          else
            form.cc_exp_year.options[i] = new Option(year, year, false, false);
        }
      }
      
      function setVisibility(div, show){
        getStyleObject(div).display = (show) ? "block" : "none";
      }

      function doPaymentChange(selectbox){
        setVisibility("creditCardDiv", (selectbox[selectbox.selectedIndex].id != "I") && (selectbox[selectbox.selectedIndex].id != "P"));
        setVisibility("noChargeDiv", (selectbox[selectbox.selectedIndex].value == "NC"));
        setVisibility("ccTypeErrorDiv", (selectbox[selectbox.selectedIndex].id == "P"));

        document.getElementById("payment_method").value = selectbox[selectbox.selectedIndex].id;
      }

    /*
     *  Set correct Credit Card Number before submitting
     */
      function setCCNumber(){
        var form = document.forms[0];
        if (form.cc_number_input.defaultValue != form.cc_number_input.value)
          form.cc_number.value = form.cc_number_input.value;
      }

    /*
     *  Actions
     */
      function doSubmitAction(){
        showWaitMessage("outterContent", "wait");
        setCCNumber();
        var form = document.forms[0];
        form.target = window.name;
        form.action = "PaymentServlet?servlet_action=validation";
        form.submit();
      }

      function doCloseAction(){
        top.close();
      }

      function doExitAction(){
        top.returnValue = new Array("_EXIT_");
        doCloseAction();
      }]]>

      function returnToCart(){
        var ret = new Array();
        ret[0] = "<xsl:value-of select="key('pageData', 'payment_method')/value"/>";
        ret[1] = "<xsl:value-of select="key('pageData', 'payment_method_id')/value"/>";
        ret[2] = "<xsl:value-of select="key('pageData', 'cc_number')/value"/>";
        ret[3] = "<xsl:value-of select="key('pageData', 'cc_exp_month')/value"/>";
        ret[4] = "<xsl:value-of select="key('pageData', 'cc_exp_year')/value"/>";
        ret[5] = "<xsl:value-of select="key('pageData', 'no_charge_username')/value"/>";
        ret[6] = "<xsl:value-of select="key('pageData', 'is_gcgd_valid')/value"/>";
        top.returnValue = ret;
        top.close();
      }
    </script>
  </head>
  <body onload="javascript:init();">
    <form name="PaymentEditForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="source_code" value="{key('pageData', 'source_code')/value}"/>
    <input type="hidden" name="order_guid" value="{key('pageData', 'order_guid')/value}"/>
    <input type="hidden" name="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="order_amount" value="{key('pageData', 'order_amount')/value}"/>
    <input type="hidden" name="disable_flag" value="{key('pageData', 'disable_flag')/value}"/>
    <input type="hidden" name="payment_method" value="{key('pageData', 'payment_method')/value}"/>
    <input type="hidden" name="payment_method_all" value="{key('pageData', 'payment_method_all')/value}"/>
    
    
   	<xsl:choose>
   		<xsl:when test="not($isGCGDInfoValid)">
   			<input type="hidden" name="is_gcgd_valid" value="N"/>	
   		</xsl:when>
   		
   		<xsl:otherwise>
   			<input type="hidden" name="is_gcgd_valid" value="Y"/>
   		</xsl:otherwise>
   	</xsl:choose>
    
    
    <xsl:for-each select="pageData/data[starts-with(name, 'gc_number')]">
      <xsl:variable name="index" select="substring(name,string-length(name),string-length(name))"/>
      <input type="hidden" name="gc_number{position()}" value="{value}"/>
      <input type="hidden" name="gc_amount{position()}" value="{/root/pageData/data[name=concat('gc_amount',$index)]/value}"/>
    </xsl:for-each>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Payment Information'"/>
      <xsl:with-param name="showTime" select="false()"/>
      <xsl:with-param name="showExitButton" select="true()"/>
    </xsl:call-template>

    <!-- Main table -->
    <div id="outterContent" style="display:block">
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td>

          <!-- Order Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Order Information</td>
            </tr>
            <tr>
              <td width="25%" class="label">Master Order Number:</td>
              <td width="25%"><xsl:value-of select="key('pageData', 'master_order_number')/value"/></td>
              <td width="15%" class="label">Customer:</td>
              <td width="35%"><xsl:value-of select="key('pageData', 'buyer_full_name')/value"/></td>
            </tr>
          </table>

          <!-- Payment Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Payment</td>
            </tr>
          </table>

          <!-- Gift Certificate Information -->
          <table id="giftCertificateDiv" width="100%" border="0" cellpadding="2" cellspacing="2">
          	<xsl:for-each select="pageData/data[starts-with(name, 'gc_number')]">
              <xsl:variable name="index" select="substring(name,string-length(name),string-length(name))"/>
              <tr>
                <td width="25%" class="label">Gift Card/Certificate Number:</td>
                <td width="25%"><xsl:value-of select="value"/></td>
                <td width="25%" class="label">Gift Card/Certificate Amount:</td>
                <td width="25%">$<xsl:value-of select="/root/pageData/data[name=concat('gc_amount', $index)]/value"/></td>
              </tr>
            </xsl:for-each>
            <xsl:if test="not($isGCGDValid)">
				<xsl:for-each select="key('validationField','gc_payment_info')/messages/message">
					<tr>
						<td width="100%" class="errorMessage" colspan="4">
							<xsl:value-of select="description"/>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="key('validationField','gc_payment_amt')/messages/message">
					<tr>
						<td width="100%" class="errorMessage" colspan="4">
							<xsl:value-of select="description" />
							
						</td>
					</tr>
				</xsl:for-each>
	    	</xsl:if>	
            	
          </table>

            <!-- Payment Methods -->
          <table id="paymentMethodsDiv" width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="25%" class="label">Payment Type:</td>
              <td width="25%">
                <select name="payment_method_id" tabindex="1" onchange="javascript:doPaymentChange(this);">
                  <xsl:if test="key('validationField', 'cc_type')/messages/message">
                    <xsl:attribute name="class">errorField</xsl:attribute>
                  </xsl:if>
                  <option value=""></option>
                  <xsl:for-each select="paymentMethodList/paymentMethod">
                    <xsl:if test="payment_method_id != 'MS'">
                    <option id="{payment_type}" value="{payment_method_id}">
                      <xsl:if test="payment_method_id=$selectedPaymentMethod"><xsl:attribute name="SELECTED"/></xsl:if>
                      <xsl:value-of select="description"/>
                    </option>
                    </xsl:if>
                  </xsl:for-each>
                </select>
              </td>
              <td width="25%"></td>
              <td width="25%"></td>
            </tr>
            <tr>
                <td colspan="3">
                  <!-- cc_type error div -->
                  <table id="ccTypeErrorDiv" width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                      <td width="25%"></td>
                      <td>
                        <xsl:if test="key('validationField', 'cc_type')/messages/message">
                          <xsl:call-template name="error">
                            <xsl:with-param name="node" select="key('validationField', 'cc_type')"/>
                          </xsl:call-template>
                        </xsl:if>
                      </td>
                    </tr>
                  </table>
                </td>
            </tr>

          </table>
          
          

          <!-- Credit Card Information -->
          <table id="creditCardDiv" width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="25%" class="label">Credit Card Number:</td>
              <td width="25%">
                <input type="text" name="cc_number_input" tabindex="2" size="16" maxlength="16" value="{key('pageData', 'cc_number_input')/value}">
                  <xsl:if test="key('validationField', 'cc_number')/messages/message">
                    <xsl:attribute name="class">errorField</xsl:attribute>
                  </xsl:if>
                </input>
                <input type="hidden" name="cc_number" value="{key('pageData', 'cc_number')/value}"/>
              </td>
              <td width="50%" colspan="2"></td>
            </tr>
            <xsl:if test="key('validationField', 'cc_number')/messages/message">
              <xsl:call-template name="error">
                <xsl:with-param name="node" select="key('validationField','cc_number')"/>
              </xsl:call-template>
            </xsl:if>
            <tr>
              <td width="25%" class="label">Expiration Date:</td>
              <td width="25%">
                <select name="cc_exp_month" tabindex="3">
                  <xsl:if test="key('validationField', 'cc_exp_date')/messages/message">
                    <xsl:attribute name="class">errorField</xsl:attribute>
                  </xsl:if>
                  <option value="01">Jan</option>
                  <option value="02">Feb</option>
                  <option value="03">Mar</option>
                  <option value="04">Apr</option>
                  <option value="05">May</option>
                  <option value="06">Jun</option>
                  <option value="07">Jul</option>
                  <option value="08">Aug</option>
                  <option value="09">Sep</option>
                  <option value="10">Oct</option>
                  <option value="11">Nov</option>
                  <option value="12">Dec</option>
                </select>
                &nbsp; / &nbsp;
                <select name="cc_exp_year" tabindex="4">
                  <xsl:if test="key('validationField', 'cc_exp_date')/messages/message">
                    <xsl:attribute name="class">errorField</xsl:attribute>
                  </xsl:if>
                </select>
              </td>
              <td width="50%" colspan="2"></td>
            </tr>
            <xsl:if test="key('validationField', 'cc_exp_date')/messages/message">
              <xsl:call-template name="error">
                <xsl:with-param name="node" select="key('validationField', 'cc_exp_date')"/>
              </xsl:call-template>
            </xsl:if>
          </table>

          <!-- No Charge Information -->
          <table id="noChargeDiv" width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="25%" class="label">Approval ID:</td>
              <td width="25%">
                <input type="text" name="no_charge_username">
                  <xsl:if test="key('pageData', 'no_charge_username_msg')/value"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                </input>
              </td>
              <td width="50%" colspan="2"></td>
            </tr>
            <tr>
              <td width="25%"></td>
              <td width="75%" colspan="3" class="errorMessage">
                <xsl:value-of select="key('pageData', 'no_charge_username_msg')/value"/>
              </td>
            </tr>
            <tr>
              <td width="25%" class="label">Approval Password:</td>
              <td width="25%">
                <input type="password" name="no_charge_password">
                  <xsl:if test="key('pageData', 'no_charge_password_msg')/value"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                </input>
              </td>
              <td width="50%" colspan="2"></td>
            </tr>
            <tr>
              <td width="25%"></td>
              <td width="75%" colspan="3" class="errorMessage">
                <xsl:value-of select="key('pageData', 'no_charge_password_msg')/value"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr bgcolor="#006699">
        <td colspan="4" class="totalLineRight">Shopping Cart Total:&nbsp;&nbsp;$<xsl:value-of select="key('pageData', 'order_amount')/value"/></td>
      </tr>
    </table>

    <!-- Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%">&nbsp;</td>
        <td width="80%" align="center">
          <input type="button" name="submitButton" tabindex="5" value="Submit" onclick="javascript:doSubmitAction();"/>&nbsp;
          <input type="button" name="closeButton" tabindex="6" value="Close" onclick="javascript:doCloseAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" tabindex="7" value="Exit" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>
    </div>
    
    <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>

      <!-- Footer Template -->
      <xsl:call-template name="footer"/>

    </table>
  </div>

    </form>
  </body>
  </html>

</xsl:template>

<xsl:template name="error">
<xsl:param name="node"/>

  <tr>
    <td width="25%"></td>
    <td width="50%" colspan="3" class="errorMessage">
      <xsl:for-each select="$node/messages/message">
        <xsl:value-of select="description"/><br/>
      </xsl:for-each>
    </td>
  </tr>

</xsl:template>
</xsl:stylesheet>