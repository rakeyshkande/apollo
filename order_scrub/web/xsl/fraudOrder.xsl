<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Fraudulent Order</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript">
    var isValid = <xsl:value-of select="boolean(pageData/data[name='validation_flag']/value)"/>;<![CDATA[

  /*
   *  Initialization
   */
    function init(){
      if (isValid){
        returnToCart();
      }

      window.name = "FRAUD_POPUP";
      document.getElementById("fraud_id").focus();
    }

  /*
   *  Actions
   */
    function doSubmitAction(){
      var form = document.forms[0];
      form.action = "FraudServlet?servlet_action=validation";
      form.target = window.name;
      form.submit();
    }

    function doExitAction(){
      top.returnValue = new Array("_EXIT_");
      doCloseAction();
    }

    function doCloseAction(){
      top.close();
    }

    function returnToCart(){
      var form = document.forms[0];
      var id = form.fraud_id;
      var ret = new Array();
      ret[0] = id[id.selectedIndex].value;
      ret[1] = form.fraud_comments.value;
      top.returnValue = ret;
      top.close();
    }
    ]]>
    </script>
</head>
<body onload="javascript:init();">
  <form name="FraudOrderForm" method="post" action="">
  <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
  <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
  <input type="hidden" name="master_order_number" value="{key('pageData','master_order_number')/value}"/>
  <input type="hidden" name="buyer_full_name" value="{key('pageData','buyer_full_name')/value}"/>

  <!-- Header template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Fraud Order'"/>
    <xsl:with-param name="showTime" select="false()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <!-- Main table -->
  <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
    <tr>
      <td>

        <!-- Order Information -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Order Information</td>
          </tr>
          <tr>
            <td width="25%" class="label">Master Order Number:</td>
            <td width="25%"><xsl:value-of select="key('pageData','master_order_number')/value"/></td>
            <td width="15%" class="label">Customer:</td>
            <td width="35%"><xsl:value-of select="key('pageData','buyer_full_name')/value"/></td>
          </tr>
        </table>

        <!-- Reason -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Reason</td>
          </tr>
          <tr>
            <td width="25%" class="label">Disposition:</td>
            <td width="75%">
              <select name="fraud_id" tabindex="1">
                <xsl:if test="boolean(/root/pageData/data[name='fraud_id_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <option value="">Select One</option>
                <xsl:for-each select="/root/fraudDispositionList/fraudDisposition">
                  <xsl:variable name="id" select="fraud_id"/>
                  <option value="{fraud_id}">
                    <xsl:if test="key('pageData','fraud_id')/value=$id"><xsl:attribute name="SELECTED"/></xsl:if>
                    <xsl:value-of select="fraud_description"/>
                  </option>
                </xsl:for-each>
              </select>
              <span class="requiredFieldTxt">&nbsp;&nbsp;***</span>
            </td>
          </tr>
          <tr>
            <td width="25%"></td>
            <td width="75%" class="errorMessage">
              <xsl:value-of select="key('pageData', 'fraud_id_msg')/value"/>
            </td>
          </tr>
        </table>

        <!-- Comments -->
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td class="tblheader" colspan="4">Comments</td>
          </tr>
          <tr>
            <td width="10%">
              <textarea name="fraud_comments" tabindex="2" rows="6" cols="60" onkeypress="javascript:limitTextarea(this, 200);">
                <xsl:if test="boolean(/root/pageData/data[name='fraud_comments_msg'])"><xsl:attribute name="class">errorField</xsl:attribute></xsl:if>
                <xsl:value-of disable-output-escaping="yes" select="key('pageData', 'fraud_comments')/value"/>
              </textarea>
            </td>
            <!-- <td valign="top" class="requiredFieldTxt">***</td> -->
          </tr>
          <tr>
            <td colspan="2" class="errorMessage">
              <xsl:value-of select="key('pageData', 'fraud_comments_msg')/value"/>
            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <!-- Action Buttons -->
  <table width="98%" border="0" align="center" cellpadding="1" cellspacing="1">
    <tr>
      <td width="20%" class="requiredFieldTxt">
        *** Required Fields
      </td>
      <td width="60%" align="center">
        <input type="button" name="submitFraud" tabindex="3" value="Submit" onclick="javascript:doSubmitAction();"/>&nbsp;
        <input type="button" name="closebutton" tabindex="4" value="Close" onclick="javascript:doCloseAction();"/>
      </td>
      <td width="20%" align="right">
        <input type="button" name="exitButton" tabindex="5" value="Exit" onclick="javascript:doExitAction();"/>
      </td>
    </tr>
  </table>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

  </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>