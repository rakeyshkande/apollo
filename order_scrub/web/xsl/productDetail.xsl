<!DOCTYPE ACDemo [
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:variable name="standardLabel" select="key('pageData','standard_label')/value"/>
<xsl:variable name="deluxeLabel" select="key('pageData','deluxe_label')/value"/>
<xsl:variable name="premiumLabel" select="key('pageData','premium_label')/value"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<xsl:variable name="productAttributeExclusion" select="boolean(string-length(/root/productAttributeExclusion/alertMessages/value) > 0)"/>
<xsl:variable name="isaribaOrder" select="key('pageData', 'isAribaOrder')/value"/>

<html>
<head>
  <title>FTD - Product Detail</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script language="javascript" src="../js/FormChek.js"/>
  <script language="javascript" src="../js/util.js"/>
  <script language="javascript">
  /*
   *  Global variables
   */
    var disableFlag = false;
    var backupFlag = false;
    var product_images = "<xsl:value-of select="key('pageData', 'product_images')/value"/>";
    var novator_id = "<xsl:value-of select="productList/products/product/@novatorid"/>";
    var shipMethodFlorist = "<xsl:value-of select="productList/products/product/@shipmethodflorist"/>";
    var shipMethodCarrier = "<xsl:value-of select="productList/products/product/@shipmethodcarrier"/>";
    var personalGreetingFlag = "<xsl:value-of select="productList/products/product/@personalgreetingflag"/>";
    var listFlag = <xsl:value-of select="key('pageData', 'list_flag')/value='Y'"/>;
    var upsellFlag = <xsl:value-of select="key('pageData', 'upsell_flag')/value='Y'"/>;
    var domesticFlag = <xsl:value-of select="key('pageData', 'domestic_flag')/value='D'"/>;
    var jcpPopup = <xsl:value-of select="key('pageData', 'jcp_flag')/value='JP' and key('pageData', 'domestic_flag')/value = 'I'"/>;
    var jcpFoodPopup = <xsl:value-of select="key('pageData', 'jcp_flag')/value = 'Y' and productList/products/product/@jcpcategory!='A'"/>;
    var displaySpecialFee = <xsl:value-of select="key('pageData', 'displaySpecialFee')/value = 'Y'"/>;
    var displaySpecGiftPopup = <xsl:value-of select="key('pageData', 'displaySpecGiftPopup')/value = 'Y'"/>;
    var displayNoProductPopup = <xsl:value-of select="key('pageData', 'displayNoProductPopup')/value = 'Y'"/>;
    var displayCodifiedSpecial = <xsl:value-of select="key('pageData', 'displayCodifiedSpecial')/value = 'Y'"/>;
    var displayProductUnavailable = <xsl:value-of select="key('pageData', 'displayProductUnavailable')/value = 'Y'"/>;
    var displayPersonalizedProduct = <xsl:value-of select="key('pageData', 'displayPersonalizedProduct')/value = 'Y'"/>;
    var displayUpsellGnaddSpecial = <xsl:value-of select="key('pageData', 'displayUpsellGnaddSpecial')/value = 'Y'"/>;
    var isAribaOrder = <xsl:value-of select="$isaribaOrder"/>;<![CDATA[

  /*
   *  Initialization
   */
    function init(){
    	setNavigationHandlers();
      setTimeout("setScrollingDivHeight()",100);
     
      
      disableFlag = false;
      
      if (jcpPopup){
        openJCPPopup();                   //  JCP Order to International country
      }
      else{
        if(jcpFoodPopup){                 //  JCP Food Order
          openJcpFoodItem();
        }
        else{
          if(displaySpecGiftPopup)        //  Floral item unavailable in zip code
            openNoFloral();

          if(displayNoProductPopup)       //  No Products
            openNoProduct();

          if(displayCodifiedSpecial)      //  Codified Special
            openCodifiedSpecial();

          if(displayProductUnavailable)   //  Product Unavailable
            openProductUnavailable();

          if(displayPersonalizedProduct)  //  Personalized Product
            openPersonalizedProduct();

          if(displayUpsellGnaddSpecial)   //  GNADD upsell
            openGnaddUpsell();
        }
      }

      //Display an alert when the page refreshes or reloads if the recipient is in Alaska or Hawaii
      if (displaySpecialFee)
        alert("A $12.99 surcharge will be added to deliver this item to Alaska or Hawaii.");
        
    }

    function setScrollingDivHeight(){
      var productDetailDiv = document.getElementById("productDetail");
      productDetailDiv.style.height = document.body.clientHeight - productDetailDiv.getBoundingClientRect().top - 85;
    }

  /*
   *  Actions
   */
    function paramsHelper(){
      var form = document.forms[0];
      var elem = document.getElementById('ProductDetailForm').elements;
      var addon_str = "";
      for(var i = 0; i < elem.length; i++) {
          if (elem[i].name.indexOf("addonOrig_") > -1) {
             addon_str += elem[i].name + "=" + elem[i].value;
          }
      }
      var params =  "&international_service_fee=" + form.international_service_fee.value +
                    "&domestic_service_fee=" + form.domestic_service_fee.value +
                    "&master_order_number=" + form.master_order_number.value +
                    "&item_order_number=" + form.item_order_number.value +
                    "&buyer_full_name=" + form.buyer_full_name.value +
                    "&category_index=" + form.category_index.value +
                    "&price_point_id=" + form.price_point_id.value +
                    "&domestic_flag=" + form.domestic_flag.value +
                    "&occasion_text=" + form.occasion_text.value +
                    "&occasion_id=" + form.occasion_id.value +
                    "&reward_type=" + form.reward_type.value +
                    "&script_code=" + form.script_code.value +
                    "&source_code=" + form.source_code.value +
                    "&postal_code=" + form.postal_code.value +
                    "&search_type=" + form.search_type.value +
                    "&company_id=" + form.company_id.value +
                    "&partner_id=" + form.partner_id.value +
                    "&product_id=" + form.product_id.value +
                    "&orig_date=" + form.orig_date.value +
                    "&page_name=" + form.page_name.value +
                    "&sort_type=" + form.sort_type.value +
                    "&jcp_flag=" + form.jcp_flag.value +
                    "&country=" + form.country.value +
                    "&state=" + form.state.value +
                    "&city=" + form.city.value + 
                    addon_str + 
                    "&delivery_date=" + form.delivery_date.value +
                    getSecurityParams(false);

      return params;
    }
    
    function doCategoriesAction(){
      document.forms[0].method = "get";

      var url = "CategoryServlet" +
                "?page_number=1" +
                paramsHelper();

      performAction(url);
    }

    function doUpsellAction(){
      var form = document.forms[0];
      form.product_id.value = form.upsell_id.value;

      var url = "SearchServlet" +
                "?page_number=1" +
                paramsHelper();

      performAction(url);
    }

    function doProductListAction(){
      var url = "ProductListServlet" +
                "?search=category" +
                "&page_number=1" +
                paramsHelper();

      performAction(url);
    }

    function doLastPageAction(){
      if (upsellFlag){
        doUpsellAction();
        return;
      }

      if (listFlag){
        doProductListAction();
        return;
      }

      doCategoriesAction();
    }

    function doSubmitAction(){
      if (validateForm()){
        var form = document.forms[0];
        var standard = document.getElementById("price_standard");
        var deluxe = document.getElementById("price_deluxe");
        var premium = document.getElementById("price_premium");
        var retValue = new Array();

        for (var i = 0; i <= 16; i++)
          retValue[i] = "";

        retValue[0] = form.product_id.value;
        retValue[1] = (form.subcode_id) ? form.subcode_id[form.subcode_id.selectedIndex].value : "";
        retValue[2] = (form.color_first_choice) ? form.color_first_choice[form.color_first_choice.selectedIndex].value : "";
        retValue[3] = (form.color_second_choice) ? form.color_second_choice[form.color_second_choice.selectedIndex].value : "";

        if (standard && standard.checked){                                //  Pricing
          retValue[4] = standard.value;
          retValue[5] = form.standard_price.value;
        }
        else if (deluxe && deluxe.checked){
          retValue[4] = deluxe.value;
          retValue[5] = form.deluxe_price.value;
        }
        else if (premium && premium.checked){
          retValue[4] = premium.value;
          retValue[5] = form.premium_price.value;
        }

        if (!!form.second_choice_auth){                                   //  Substitutions allowed
          retValue[6] = form.second_choice_auth.checked;
        }
        retValue[7] = personalGreetingFlag;

        // Ouch, this hurts. Gotta save all addons in array so we can extract upstream (e.g., in shoppingCart.xsl).
        // Each addon (or addon quantity/price) takes up two entries in array - first is name of variable, 2nd is value.
        //
        var rcnt = 8;        
        var elem = document.getElementById('ProductDetailForm').elements;
        for(var aa = 0; aa < elem.length; aa++) {
          if ((elem[aa].name.indexOf("addonNew_") > -1) && (elem[aa].name.indexOf("_quantity") < 0) && (elem[aa].name.indexOf("_price") < 0)) {
             // If vase 
             if (elem[aa].name.indexOf("addonNew_optVase") > -1) {
                 retValue[rcnt++] = elem[aa].name;
                 retValue[rcnt++] = elem[aa].value;
             // If card 
             } else if (elem[aa].name.indexOf("addonNew_optCard") > -1) {
                 retValue[rcnt++] = elem[aa].name;
                 retValue[rcnt++] = elem[aa].value;
             // Only bother if addon was checked
             } else if (elem[aa].checked) {
                 retValue[rcnt++] = elem[aa].name;
                 retValue[rcnt++] = elem[aa].value;
                 var quantity = document.getElementById(elem[aa].name + "_quantity");
                 if (quantity) {
                     retValue[rcnt++] = quantity.name;
                     retValue[rcnt++] = quantity.value;
                 }
                 var price = document.getElementById(elem[aa].name + "_price");
                 if (price) {
                     retValue[rcnt++] = price.name;
                     retValue[rcnt++] = price.value;
                 }
             }
          }
        }

        top.returnValue = retValue;
        doCloseAction();
      }
      else{
        alert("Please correct the marked fields.");
      }
    }

    function doExitAction(){
      top.returnValue = new Array("_EXIT_");
      doCloseAction();
    }

    function doCloseAction(){
      top.close();
    }

    function performAction(url){
      showWaitMessage("contentDiv", "wait");
      document.forms[0].action = url;
      document.forms[0].target = window.name;
      document.forms[0].submit();
    }

  /*
   *  Validation
   */
    function validateForm(){
      checked = true;

      if (!!document.forms[0].subcode_id){
        document.forms[0].subcode_id.className = "TblText";
        if (document.forms[0].subcode_id.value == ""){
          document.forms[0].subcode_id.className = "ErrorField";
          document.forms[0].subcode_id.focus();
          checked = false;
        }
      }

      if (!!document.forms[0].color_first_choice){
        document.forms[0].color_first_choice.className = "TblText";
        if (document.forms[0].color_first_choice.value == ""){
          document.forms[0].color_first_choice.className = "ErrorField";
          if (checked != false){
            document.forms[0].color_first_choice.focus();
            checked = false;
          }
        }
      }

      if (!!document.forms[0].color_second_choice){
        document.forms[0].color_second_choice.className = "TblText";
        if (document.forms[0].color_second_choice.value == ""){
          document.forms[0].color_second_choice.className = "ErrorField";
          if (checked != false){
            document.forms[0].color_second_choice.focus();
            checked = false;
          }
        }
      }

      return checked;
    }
/*
*    function submitForm(){
*      if (backupFlag == true){
*        backToShopping();
*        return;
*      }
*
*      if (disableFlag == false){
*        if (validateForm()){
*          openUpdateWin();
*          disableContinue();
*          form.submit();
*        }
*        else{
*          enableContinue();
*          alert("Please correct the marked fields.");
*        }
*      }
*    }
*/

  /*
   *  Utility Functions
   */
    function updateAddOnQuantity(checkbox, fieldName){
      var element = document.getElementById(fieldName);
      if (element) {
        if (checkbox.checked){
          element.disabled = false;
        } else {
          element.disabled = true;
        }
      }
    }

   function convertHtmlEntity(input) {
     var div = document.createElement('div');
     div.innerHTML = input;
     return div.innerHTML;
   }

    function updateCheckbox(textfield, checkbox){
      checkbox.checked = parseInt(textfield.value) > 0;
    }


  /*
   *  Popups
   */
    function openGreetingCardPopup(){
      var form = document.forms[0];
      var url_source =  "PopupServlet" +
                        "?POPUP_ID=LOOKUP_GREETING_CARDS" +
                        "&occasion_id=" + form.occasion_id.value +
                        getSecurityParams(false);

      var modal_dim = "dialogWidth:800px; dialogHeight:530px; center:yes; status=0; help:no";
      var ret =  showModalDialogIFrameServlet(url_source, "", modal_dim);

      if (ret && ret != null && ret[0] != '') {
        form.addonNew_optCard.value = ret[0] + "---" + ret[1];  
      }
    }

    function acceptGnaddUpsell(){]]>
      var searchType = "<xsl:value-of select="key('pageData', 'search_type')/value"/>";
      var servletCall = (searchType == "simpleproductsearch") ? "SearchServlet" : "ProductDetailServlet";
      var url = servletCall +
                "?upsell_flag=" + "<xsl:value-of select="key('pageData', 'upsell_flag')/value"/><![CDATA[" +
                "&search=" + searchType +
                "&productId=" + document.forms[0].product_id.value +
                "&soonerOverride=Y";

      document.location = url;
    }

    function JCPPopChoice(inChoice){    //  JC penney popup for international change
      if (inChoice == "Y"){
        var url = "DNISChangeServlet?source=productDetailJCP";
        document.forms[0].source.value = "productDetailJCP";
        document.forms[0].action = url;
        document.forms[0].method = "get";
        document.forms[0].submit();
      }
      else {
        document.forms[0].zip.value = "";
        refreshPage();
        closeJCPPopup();
      }
    }

    function openJCPPopup(){
      JCPPopV = document.getElementById("JCPPop").style;
      if(isAribaOrder){
         setMargins(JCPPopV,"");
      }
      else{
        scroll_top = document.body.scrollTop;
        JCPPopV.top = scroll_top+document.body.clientHeight/2-100;
        JCPPopV.width = 290;
        JCPPopV.height = 100;
        JCPPopV.left = document.body.clientWidth/2 - 50;
      }
      JCPPopV.visibility = "visible";
    }

    function closeJCPPopup(){
      JCPPopV.visibility = "hidden";
    }

    function doDeliveryPolicyPopup(){
      var url_source = "deliveryPolicy_floral.htm";

      if ( shipMethodFlorist == "Y" )
        url_source = "deliveryPolicy_floral.htm";

      if ( shipMethodCarrier == "Y" )
        url_source = "deliveryPolicy_dropship.htm";

      var modal_dim = "dialogWidth:800px; dialogHeight:530px; center:yes; status=0; help:no";
      showModalDialogIFrame(url_source, "delivery_policy", modal_dim);
    }

    function doLargeImagePopup(){
      largeImage = document.getElementById("showLargeImage").style;
      scroll_top = document.body.scrollTop;

      largeImage.top = scroll_top + document.body.clientHeight/2 - 200;
      largeImage.width = 340;
      largeImage.height = 400;
      largeImage.left = 200;
      largeImage.display = "block";
      hideShowCovered(showLargeImage);
    }

    function closeLargeImage(){
      largeImage.display = "none";
      hideShowCovered(showLargeImage);
    }

    //  Change image of continue button to disabled and prevent submitting the page
    //  but only if the country is domestic and the key stroke isn't a tab
    function disableContinue(){
      if (window.event.keyCode != 9){]]>
      <xsl:if test="key('pageData', 'countryType')/value='D'">
        <xsl:if test="productList/products/product/@status = 'A'"><![CDATA[
          disableFlag = true;
          document.getElementById("submitButton").disabled = true;]]>
        </xsl:if>
      </xsl:if><![CDATA[
      }
    }

    // Change image of continue button to enabled and allow submitting the page but only if domestic
    function enableContinue(){]]>
      <xsl:if test="key('pageData', 'countryType')/value='D'">
        <xsl:if test="productList/products/product/@status = 'A'"><![CDATA[
          disableFlag = false;
          document.getElementById("submitButton").disabled = false;]]>
        </xsl:if>
      </xsl:if><![CDATA[
    }

  /*  **************************************************************************
      This group of functions is used to display/hide the Specialty Gift
      only available for zip code message box.
    **************************************************************************  */
  function openNoFloral(){
    disableContinue();

    noFloralV = document.getElementById("noFloral").style;
    if(isAribaOrder){
         setMargins(noFloralV,"");
     }else{
    scroll_top = document.body.scrollTop;
    noFloralV.top = scroll_top+document.body.clientHeight/2-100;

    noFloralV.width = 290;
    noFloralV.height = 100;
    noFloralV.left = document.body.clientWidth/2 - 100;
    }
    noFloralV.visibility = "visible";
  }

  function closeNoFloral(){
    noFloral.style.visibility = "hidden";
  }

  /*  **************************************************************************
      This group of functions is used to display/hide the No Product
      available for zip code message box
    **************************************************************************  */
  function openNoProduct(){
    disableContinue();

    noProductV = document.getElementById("noProduct").style;
    if(isAribaOrder){
    setMargins(noProductV,"");
    }else{
    scroll_top = document.body.scrollTop;
    noProductV.top = scroll_top+document.body.clientHeight/2-100;

    noProductV.width = 290;
    noProductV.height = 100;
    noProductV.left = document.body.clientWidth/2 - 50;
    }
    noProductV.visibility = "visible";
  }

  function closeNoProduct(){
    noProductV.visibility = "hidden";
  }

  // Called when a same day gift has no codified florists for this zip code
  function openNoCodifiedFloristHasCommonCarrier(){
    disableContinue();

    noCodifiedFloristHasCommonCarrierV = document.getElementById("noCodifiedFloristHasCommonCarrier").style;
    scroll_top = document.body.scrollTop;
    noCodifiedFloristHasCommonCarrierV.top = scroll_top+document.body.clientHeight/2-100;

    noCodifiedFloristHasCommonCarrierV.width = 290;
    noCodifiedFloristHasCommonCarrierV.height = 100;
    noCodifiedFloristHasCommonCarrierV.left = document.body.clientWidth/2 - 50;
    noCodifiedFloristHasCommonCarrierV.visibility = "visible";
  }

  function closeNoCodifiedFloristHasCommonCarrier(){
    noCodifiedFloristHasCommonCarrierV.visibility = "hidden";
    enableContinue();
  }

  function noCodifiedFloristHasCommonCarrier(){
    closeNoCodifiedFloristHasCommonCarrier();
  }

  // Called when a same day gift has no florists for this zip code
  function openNoFloristHasCommonCarrier(){
    disableContinue();

    noFloristHasCommonCarrierV = document.getElementById("noFloristHasCommonCarrier").style;
    scroll_top = document.body.scrollTop;
    noFloristHasCommonCarrierV.top = scroll_top+document.body.clientHeight/2-100;

    noFloristHasCommonCarrierV.width = 290
    noFloristHasCommonCarrierV.height = 100;
    noFloristHasCommonCarrierV.left = document.body.clientWidth/2 - 50;
    noFloristHasCommonCarrierV.visibility = "visible";
  }

  function closeNoFloristHasCommonCarrier(){
    noFloristHasCommonCarrierV.visibility = "hidden";
    enableContinue();
  }

  function noFloristHasCommonCarrier(){
    closeNoFloristHasCommonCarrier();
  }

  function closeGnaddUpsell(){
    hideShowCovered(gnaddUpsell);
    gnaddUpsellUV.visibility = "hidden";
    if (document.all.zip && document.all.zip.enabled == true)
      document.forms[0].zip.focus();

    enableContinue();
  }

  function gnaddUpsell(){
    closeGnaddUpsell();
  }

  /*  **************************************************************************
      This group of functions is used to display/hide the Product Unavailable
      for zip code message box
    **************************************************************************  */
  function openProductUnavailable(){
    disableContinue();
    productUV = document.getElementById("productUnavailable").style;
    if(isAribaOrder){
    document.forms[0].backButton.style.display="none";
        var prUnablve=document.getElementById("msg").innerText;
        var message=prUnablve.split(".");			
        document.getElementById("msg").innerText=message[0]+".";
        setMargins(productUV,"msg");
    }
    else{
	    scroll_top = document.body.scrollTop;
	    productUV.top = scroll_top+document.body.clientHeight/2-100;
	    productUV.width = 290;
	    productUV.height = 100;
	    productUV.left = document.body.clientWidth/2 - 50;
    }
    productUV.visibility = "visible";
  }
  
  function setMargins(productUV,id){
		productUV.top = 230;
		productUV.width = 180;
		productUV.height = 75;
		productUV.left = 250;
  }
  function openPersonalizedProduct() {
    disableContinue();
    
    productUV = document.getElementById("personalizedProduct").style;
    if(isAribaOrder){
        document.forms[0].backButton.style.display="none";
        var prUnablve=document.getElementById("personalize").innerText;
        var message=prUnablve.split(".");			
        document.getElementById("personalize").innerText=message[0]+".";
        setMargins(productUV,"personalize");
    }
    else{
    scroll_top = document.body.scrollTop;
    productUV.top = scroll_top+document.body.clientHeight/2-100;

    productUV.width = 290;
    productUV.height = 100;
    productUV.left = document.body.clientWidth/2 - 50;
    }
    productUV.visibility = "visible";
  }

  function openGnaddUpsell(){
    hideShowCovered(gnaddUpsell);
    disableContinue();
    
    gnaddUpsellUV = document.getElementById("gnaddUpsell").style;
    if(isAribaOrder){
       setMargins(gnaddUpsellUV,"");
    }else{
    scroll_top = document.body.scrollTop;
    gnaddUpsellUV.top = scroll_top+document.body.clientHeight/2-100;

    gnaddUpsellUV.width = 290;
    gnaddUpsellUV.height = 100;
    gnaddUpsellUV.left = document.body.clientWidth/2 - 50;
    }
    gnaddUpsellUV.visibility = "visible";
  }

  function openJcpFoodItem(){
    productUV = document.getElementById("jcpFood").style;
    if(isAribaOrder){
       setMargins(productUV,"");
    }
    else{
      scroll_top = document.body.scrollTop;
      productUV.top = scroll_top+document.body.clientHeight/2-100;
      productUV.width = 290;
      productUV.height = 100;
      productUV.left = document.body.clientWidth/2 - 50;
    }
    productUV.visibility = "visible";
  }

  function closeJcpFoodItem(){
    productUV.visibility = "hidden";
    document.forms[0].backButton.focus();
  }

  function closeProductUnavailable(){
    productUV.visibility = "hidden";
  }
  
  function closePersonalizedProduct(){
    productUV.visibility = "hidden";
  }

  function productUnavailable(){
    closeProductUnavailable();
  }

  /*  **************************************************************************
      This group of functions is used to display/hide the Codified Special
      for zip code message box
    **************************************************************************  */
  function openCodifiedSpecial(){
    disableContinue();
    
    codifiedSpecialV = document.getElementById("codifiedSpecial").style;
    if(isAribaOrder){
         setMargins(codifiedSpecialV,"");
      }else{
    scroll_top = document.body.scrollTop;
    codifiedSpecialV.top = scroll_top+document.body.clientHeight/2-100;

    codifiedSpecialV.width = 290;
    codifiedSpecialV.height = 100;
    codifiedSpecialV.left = document.body.clientWidth/2 - 50;
    }
    codifiedSpecialV.visibility = "visible";

    backupFlag = true;
  }

  function closeCodifiedSpecial(){
    codifiedSpecialV.visibility = "hidden";
  }

  function codifiedSpecial(){
    closeCodifiedSpecial();
  }]]>
  </script>
</head>

<body onload="javascript:init();">

  <!-- Header template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Product Detail'"/>
    <xsl:with-param name="showExitButton" select="true()"/>
    <xsl:with-param name="showTime" select="false()"/>
  </xsl:call-template>

  <form name="ProductDetailForm" id="ProductDetailForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="international_service_fee" value="{key('pageData', 'international_service_fee')/value}"/>
    <input type="hidden" name="domestic_service_fee" value="{key('pageData', 'domestic_service_fee')/value}"/>
    <input type="hidden" name="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
    <input type="hidden" name="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
    <input type="hidden" name="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
    <input type="hidden" name="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
    <input type="hidden" name="category_index" value="{key('pageData', 'category_index')/value}"/>
    <input type="hidden" name="occasion_text" value="{key('pageData', 'occasion_text')/value}"/>
    <input type="hidden" name="domestic_flag" value="{key('pageData', 'domestic_flag')/value}"/>
    <input type="hidden" name="occasion_id" value="{key('pageData', 'occasion_id')/value}"/>
    <input type="hidden" name="postal_code" value="{key('pageData', 'postal_code')/value}"/>
    <input type="hidden" name="source_code" value="{key('pageData', 'source_code')/value}"/>
    <input type="hidden" name="script_code" value="{key('pageData', 'script_code')/value}"/>
    <input type="hidden" name="reward_type" value="{key('pageData', 'reward_type')/value}"/>
    <input type="hidden" name="search_type" value="{key('pageData', 'search_type')/value}"/>
    <input type="hidden" name="total_pages" value="{key('pageData', 'total_pages')/value}"/>
    <input type="hidden" name="page_number" value="{key('pageData', 'page_number')/value}"/>
    <input type="hidden" name="upsell_flag" value="{key('pageData', 'upsell_flag')/value}"/>
    <input type="hidden" name="company_id" value="{key('pageData', 'company_id')/value}"/>
    <input type="hidden" name="partner_id" value="{key('pageData', 'partner_id')/value}"/>
    <input type="hidden" name="product_id" value="{key('pageData', 'product_id')/value}"/>
    <input type="hidden" name="list_flag" value="{key('pageData', 'list_flag')/value}"/>
    <input type="hidden" name="orig_date" value="{key('pageData', 'orig_date')/value}"/>
    <input type="hidden" name="sort_type" value="{key('pageData', 'sort_type')/value}"/>
    <input type="hidden" name="page_name" value="{key('pageData', 'page_name')/value}"/>
    <input type="hidden" name="upsell_id" value="{key('pageData', 'upsell_id')/value}"/>
    <input type="hidden" name="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
    <input type="hidden" name="country" value="{key('pageData', 'country')/value}"/>
    <input type="hidden" name="state" value="{key('pageData', 'state')/value}"/>
    <input type="hidden" name="city" value="{key('pageData', 'city')/value}"/>
    <input type="hidden" name="delivery_date" value="{key('pageData', 'delivery_date')/value}"/>
    <xsl:for-each select="/root/AddonsOriginal/Addon">
      <input type="hidden" name="addonOrig_{addon_id}" value="{addon_qty}"/>
    </xsl:for-each>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Order Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Order Information for <xsl:value-of select="key('pageData', 'item_order_number')/value"/></td>
            </tr>
            <tr>
              <td width="25%" class="label">Master Order Number:</td>
              <td width="25%"><xsl:value-of select="key('pageData', 'master_order_number')/value"/></td>
              <td width="15%" class="label">Customer:</td>
              <td width="35%"><xsl:value-of select="key('pageData', 'buyer_full_name')/value"/></td>
            </tr>
          </table>

          <!-- Product detail -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">
                <xsl:if test="key('pageData', 'domestic_flag')/value='D'">
                  <span class="textLinkWhite" onclick="javascript:doCategoriesAction();">Categories</span>
                  &nbsp;>&nbsp;
                </xsl:if>
                <xsl:if test="key('pageData', 'upsell_flag')/value='Y'">
                  <span class="textLinkWhite" onclick="javascript:doUpsellAction();">Upsell Product</span>
                  &nbsp;>&nbsp;
                </xsl:if>
                <xsl:if test="key('pageData', 'list_flag')/value='Y'">
                  <span class="textLinkWhite" onclick="javascript:doProductListAction();">Product List</span>
                  &nbsp;>&nbsp;
                </xsl:if>
                Product Detail
              </td>
            </tr>
          </table>

          <!-- Content div -->
          <div id="contentDiv" style="display:block">
            <table width="100%" border="0" cellpadding="2" cellspacing="2">
              <tr>
                <td colspan="3" width="100%" align="center">

                  <!-- Scrolling div contiains product detail or no products found message -->
                  <div id="productDetail" style="overflow:auto; width:100%; height:300; padding:0px; margin:0px">

                    <!-- Product detail information or No Products Found -->
                    <xsl:choose>
                      <xsl:when test="$productAttributeExclusion">
                        <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2">
                          <tr>
                            <td align="center" class="waitMessage">
                                  <xsl:value-of select="/root/productAttributeExclusion/alertMessages/value"/>
                            </td>
                          </tr>
                        </table>
                      </xsl:when>
                      
                      <xsl:when test="productList/products/product[@productid!='']">

                        <table width="100%" border="0" cellspacing="2" cellpadding="2">

                          <!-- Product name and id -->
                          <tr>
                            <td colspan="3">
                              <span class="header">
                                <xsl:value-of disable-output-escaping="yes" select="productList/products/product/@novatorname"/>
                              </span>
                              <span class="label">
                                &nbsp;&nbsp;#<xsl:value-of select="productList/products/product/@productid"/>
                              </span>
                            </td>
                          </tr>

                          <!-- Product image, description, delivery policy, etc. -->
                          <tr valign="top">
                            <td align="center">
                              <script language="javascript">
                                // Exception display trigger variables
                                var exceptionMessageTrigger = false;
                                var selectedDate = new Date("<xsl:value-of select="key('pageData', 'requestedDeliveryDate')/value"/>");
                                var exceptionStart = new Date("<xsl:value-of select="productList/products/product/@exceptionstartdate"/>");
                                var exceptionEnd = new Date("<xsl:value-of select="productList/products/product/@exceptionenddate"/>");<![CDATA[
                                document.write("<img  border=\"0\" height=\"120\" width=\"120\" ");
                                document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ");
                                document.write("if (imag!='npi_1.gif'){ ");
                                document.write("this.src='" + product_images + "npi_1.gif\'}\" ");
                                document.write("src='" + product_images + novator_id + "_1.gif'/> ");]]>
                              </script>
                              <br/>
                              <a tabindex="1" href="#" onclick="javascript:doLargeImagePopup();">View Larger Image</a>
                              <br/>
                            </td>
                            <td colspan="2" align="left">
                              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                  <td class="description" colspan="2">
                                    <xsl:value-of select="productList/products/product/@longdescription"  disable-output-escaping="yes"/>
                                  </td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td class="description">
                                    <a tabindex="2" href="#" onclick="javascript:doDeliveryPolicyPopup();">Delivery Policies</a>
                                  </td>
                                </tr>
                                <xsl:if test="productList/products/product/@allowfreeshippingflag != 'Y'">
                                  <tr>
                                    <td colspan="2">
                                      <img src="../images/noFSicon.jpg" alt="No Free Shipping" border="0"/>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:choose>
                                  <xsl:when test="key('pageData', 'requestedDeliveryDate')/value">

                                    <xsl:if test="productList/products/product/@exceptioncode = 'U'">
                                      <script language="JavaScript"><![CDATA[
                                        if (selectedDate >= exceptionStart && selectedDate <= exceptionEnd) {
                                          exceptionMessageTrigger = true;
                                          document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"><font color=\"red\">This product is not available for delivery from&nbsp;]]><xsl:value-of select="productList/products/product/@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="productList/products/product/@exceptionenddate"/><![CDATA[</font></td></tr>");
                                        }]]>
                                      </script>
                                    </xsl:if>
                                    <xsl:if test="productList/products/product/@exceptioncode = 'A'">
                                      <script language="javascript"><![CDATA[
                                        if (selectedDate < exceptionStart || selectedDate > exceptionEnd) {
                                          exceptionMessageTrigger = true;
                                          document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"><font color=\"red\">This product is only available for delivery from&nbsp;]]><xsl:value-of select="productList/products/product/@exceptionstartdate"/><![CDATA[&nbsp;to&nbsp;]]><xsl:value-of select="productList/products/product/@exceptionenddate"/><![CDATA[</font></td></tr>");
                                        }]]>
                                      </script>
                                    </xsl:if>

                                    <script language="javascript"><![CDATA[
                                      if (exceptionMessageTrigger == true)
                                        document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"> <font color=\"red\">]]><xsl:value-of select="productList/products/product/@exceptionmessage"/><![CDATA[</font></td></tr>");
                                      else
                                        document.write("<tr><td colspan=\"2\" class=\"labelleft\" align=\"left\"> <font color=\"blue\">]]><xsl:value-of select="productList/products/product/@exceptionmessage"/><![CDATA[</font></td></tr>");
                                    ]]>
                                    </script>

                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:if test="productList/products/product/@exceptioncode = 'A'">
                                      <tr>
                                        <td colspan="2" class="labelleft"><font color="red">This product is only available for delivery from&nbsp;<xsl:value-of select="productList/products/product/@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="productList/products/product/@exceptionenddate"/></font></td>
                                      </tr>
                                    </xsl:if>
                                    <xsl:if test="productList/products/product/@exceptionCode = 'U'">
                                      <tr>
                                        <td colspan="2" class="labelleft"><font color="red">This product is not available for delivery from&nbsp;<xsl:value-of select="productList/products/product/@exceptionstartdate"/>&nbsp;to&nbsp;<xsl:value-of select="productList/products/product/@exceptionenddate"/></font></td>
                                      </tr>
                                    </xsl:if>
                                      <tr>
                                        <td colspan="2" class="labelleft"><font color="red"><xsl:value-of select="productList/products/product/@exceptionmessage"/></font></td>
                                      </tr>
                                  </xsl:otherwise>
                                </xsl:choose>

                                <xsl:if test="extraInfo/subtypes/product">
                                  <tr>
                                    <td width="20%" class="label"><xsl:value-of select="productList/products/product/@productname"/>:</td>
                                    <td align="left">
                                      <select name="subcode_id">
                                        <option value="">Please Select</option>
                                        <xsl:for-each select="extraInfo/subtypes/product">
                                          <option value="{@productsubcodeid}"><xsl:value-of select="@subcodedescription"/> - $<xsl:value-of select="@subcodeprice"/></option>
                                        </xsl:for-each>
                                      </select>
                                    </td>
                                  </tr>
                                </xsl:if>
                              </table>
                            </td>
                          </tr>
                          <xsl:choose>
    							<xsl:when test="productList/products/product/@producttype = 'SDFC'">
    									<tr>
    									<td align="center" bgcolor="#860303" style="color:#FFFFFF">
			                               Flex&nbsp;Fill
			                              </td>
			                            </tr>
    							</xsl:when>
    							<xsl:otherwise>
			                          <xsl:if test="productList/products/product/@shipmethodflorist = 'Y'">
			                            <tr>
			                              <td align="center" bgcolor="#C1DEF3">
			                                FTD <script><![CDATA[document.write("&#174;")]]></script> Florist
			                              </td>
			                            </tr>
			                          </xsl:if>
			                          <xsl:if test="productList/products/product/@shipmethodcarrier = 'Y'">
			                            <tr>
			                              <td align="center" bgcolor="#FF9933"><xsl:value-of select="productList/products/product/@carrier"/></td>
			                            </tr>
			                          </xsl:if>
			                   </xsl:otherwise>
			             </xsl:choose>

                          <!-- Product pricing -->
                          <xsl:if test="productList/products/product/@standardprice > 0">
                            <tr>
                              <td class="screenPrompt" colspan="3">Price</td>
                            </tr>
                            <tr>
                              <td class="labelright">Price:&nbsp;</td>
                              <td align="left">
                                <input type="radio" id="price_standard" name="price" tabindex="3" value="A" checked="true"/>
                                <input type="hidden" name="standard_price" value="{productList/products/product/@standardprice}"/>
                                <xsl:choose>
                                  <xsl:when test="productList/products/product/@standarddiscountprice > 0 and productList/products/product/@standardprice != productList/products/product/@standarddiscountprice">
                                    <span class="strikePrice">
                                      $<xsl:value-of select="format-number(productList/products/product/@standardprice,'#,###,##0.00')"/>
                                    </span>&nbsp;
                                    $<xsl:value-of select="format-number(productList/products/product/@standarddiscountprice,'#,###,##0.00')"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    $<xsl:value-of select="format-number(productList/products/product/@standardprice,'#,###,##0.00')"/>
                                  </xsl:otherwise>
                                </xsl:choose>
				<xsl:choose>
					<xsl:when test="$standardLabel != ''">
						&nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$standardLabel"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;&nbsp; - &nbsp;&nbsp;Shown
					</xsl:otherwise>
				</xsl:choose>
                                <xsl:if test="productList/products/product/@standardrewardvalue > 0">
                                  <xsl:if test="key('pageData', 'reward_type')/value ='Dollars'">
                                    ($<xsl:value-of select="format-number(productList/products/product/@standardrewardvalue,'#,###,##0.00')"/>&nbsp;Off)
                                  </xsl:if>
                                  <xsl:if test="key('pageData', 'reward_type')/value ='Charity'">
                                    ($<xsl:value-of select="format-number(productList/products/product/@standardrewardvalue,'#,###,##0.00')"/>&nbsp;Charity)
                                  </xsl:if>
                                  <xsl:if test="key('pageData', 'reward_type')/value ='Cash Back'">
                                    ($<xsl:value-of select="format-number(productList/products/product/@standardrewardvalue,'#,###,##0.00')"/>&nbsp;Cash Back)
                                  </xsl:if>
                                  <xsl:if test="key('pageData', 'reward_type')/value ='Percent'">
                                    (<xsl:value-of select="productList/products/product/@standardrewardvalue"/>%)
                                  </xsl:if>
                                  <xsl:if test="key('pageData', 'reward_type')/value ='Miles'">
                                    (<xsl:value-of select="productList/products/product/@standardrewardvalue"/>&nbsp;Miles)
                                  </xsl:if>
                                  <xsl:if test="key('pageData', 'reward_type')/value ='Points'">
                                    (<xsl:value-of select="productList/products/product/@standardrewardvalue"/>&nbsp;Points)
                                  </xsl:if>
                                </xsl:if>
                              </td>
                              <td align="left" class="Instruction" valign="top">
                                <xsl:value-of select="extraInfo/scripting/script[@fieldname='PRICE']/@instructiontext"/>
                              </td>
                            </tr>
                          </xsl:if>
                          <xsl:if test="key('pageData', 'upsell_flag')/value != 'Y'">
                            <xsl:if test="productList/products/product/@deluxeprice > 0">
                              <tr>
                                <td class="labelright">&nbsp;</td>
                                <td align="left">
                                  <input tabindex="4" type="radio" id="price_deluxe" name="price" value="B"/>
                                  <input type="hidden" name="deluxe_price" value="{productList/products/product/@deluxeprice}"/>
                                  <xsl:choose>
                                    <xsl:when test="productList/products/product/@deluxediscountprice > 0 and productList/products/product/@deluxeprice != productList/products/product/@deluxediscountprice">
                                      <span class="strikePrice">
                                        $<xsl:value-of select="format-number(productList/products/product/@deluxeprice,'#,###,##0.00')"/>
                                      </span>&nbsp;
                                      $<xsl:value-of select="format-number(productList/products/product/@deluxediscountprice,'#,###,##0.00')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="format-number(productList/products/product/@deluxeprice,'#,###,##0.00')"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
				<xsl:choose>
					<xsl:when test="$deluxeLabel != ''">
						&nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$deluxeLabel"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;&nbsp; - &nbsp;&nbsp;Deluxe
					</xsl:otherwise>
				</xsl:choose>
                                  <xsl:if test="productList/products/product/@deluxerewardvalue > 0">
                                    (<xsl:value-of select="productList/products/product/@deluxerewardvalue"/>)
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                            <xsl:if test="productList/products/product/@premiumprice > 0">
                              <tr>
                                <td class="labelright">&nbsp;</td>
                                <td align="left">
                                  <input tabindex="5" type="radio" id="price_premium" name="price" value="C"/>
                                  <input type="hidden" name="premium_price" value="{productList/products/product/@premiumprice}"/>
                                  <xsl:choose>
                                    <xsl:when test="productList/products/product/@premiumdiscountprice > 0 and productList/products/product/@premiumprice != productList/products/product/@premiumdiscountprice">
                                      <span class="strikePrice">
                                        $<xsl:value-of select="format-number(productList/products/product/@premiumprice,'#,###,##0.00')"/>
                                      </span>&nbsp;
                                      $<xsl:value-of select="format-number(productList/products/product/@premiumdiscountprice,'#,###,##0.00')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                      $<xsl:value-of select="format-number(productList/products/product/@premiumprice,'#,###,##0.00')"/>
                                    </xsl:otherwise>
                                  </xsl:choose>
				<xsl:choose>
					<xsl:when test="$premiumLabel != ''">
						&nbsp;&nbsp; - &nbsp;&nbsp;<xsl:value-of select="$premiumLabel"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;&nbsp; - &nbsp;&nbsp;Premium
					</xsl:otherwise>
				</xsl:choose>
                                  <xsl:if test="productList/products/product/@premiumrewardvalue > 0">
                                    (<xsl:value-of select="productList/products/product/@premiumrewardvalue"/>)
                                  </xsl:if>
                                </td>
                              </tr>
                            </xsl:if>
                          </xsl:if>
                          <tr>
                            <td colspan="3">&nbsp;</td>
                          </tr>


                          <!-- Product second choice -->
                          <xsl:if test="productList/products/product/@secondchoicecode!='0' and productList/products/product/@shipmethodflorist='Y'">
                            <tr>
                              <td class="ScreenPrompt" colspan="3"> <xsl:value-of select="extraInfo/scripting/script[@fieldname='COLOR']/@scripttext"/>
                               &nbsp;<xsl:value-of select="productList/products/product/@secondchoice"/>.
                              </td>
                            </tr>
                            <tr>
                              <td><br/></td>
                            </tr>
                            <tr>
                              <td class="labelright">Allow substitution:</td>
                              <td align="left"><input type="checkbox" name="second_choice_auth" tabindex="10"/></td>
                            </tr>
                            <tr>
                              <td><br/></td>
                            </tr>
                          </xsl:if>

                          <!-- Add-ons -->
                          <tr>
                            <td class="ScreenPrompt" colspan="3">Add-ons</td>
                          </tr>
                          
                          <!-- vase addons -->
                          <xsl:if test="extraInfo/add_ons/vase/AddOnVO">
                            <tr>
                              <td class="labelright">Vase:</td>
                              <td colspan="2">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="50%">
                                      <select name="addonNew_optVase" id="addonNew_optVase" tabindex="15">
                                        <xsl:for-each select="extraInfo/add_ons/vase/AddOnVO">
                                          <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                          <xsl:variable name="curAaPrice" select="format-number(addOnPrice, '#0.00')"/> 
                                          <option value="{addOnId}---{$curAaPrice}">
                                            <xsl:if test="boolean(/root/AddonsOriginal/Addon[addon_id = $curLoopAddonId])"><xsl:attribute name="SELECTED"/></xsl:if>
                                            <script>document.write(convertHtmlEntity('<xsl:value-of select="concat(addOnDescription, ' - $', $curAaPrice)"/>'))</script>
                                          </option>
                                        </xsl:for-each>
                                      </select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </xsl:if>
                          
                          <!-- normal addons -->
                          <xsl:if test="extraInfo/add_ons/add_on/AddOnVO">
                             <xsl:for-each select="extraInfo/add_ons/add_on/AddOnVO">
                                <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                <xsl:variable name="curLoopMaxQuantity" select="maxQuantity"/>
                                <xsl:variable name="orderQuantity" select="/root/AddonsOriginal/Addon[addon_id = $curLoopAddonId]/addon_qty"/>
                                <xsl:variable name="tabindex" select="20 + (position() * 2)"/>
                                <xsl:variable name="curPrice" select="format-number(addOnPrice, '#0.00')"/> 
                                <xsl:variable name="curChecked" select="boolean(/root/AddonsOriginal/Addon[addon_id = $curLoopAddonId]) or boolean($curLoopAddonId = 'FC')"/>
                                <tr>
                                  <td class="labelright"><xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>:</td>
                                  <td colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="50%">
                                          <input type="checkbox" name="addonNew_{addOnId}" id="addonNew_{addOnId}" tabindex="{$tabindex}" value="{addOnId}" onclick="javascript:updateAddOnQuantity(this, 'addonNew_{addOnId}_quantity');">
                                            <xsl:if test="$curChecked"><xsl:attribute name="CHECKED"/></xsl:if>
                                          </input>
                                          $<xsl:value-of select="format-number(addOnPrice, '#,###,##0.00')"/>
                                          <input type="hidden" name="addonNew_{addOnId}_price" value="{$curPrice}"/>
                                        </td>
                                        <td width="20%" class="Labelright">Quantity:&nbsp;</td>
                                        <td width="30%">
                                          <select name="addonNew_{addOnId}_quantity" id="addonNew_{addOnId}_quantity" class="tblText" tabindex="{$tabindex+1}">
                                            <xsl:if test="$curChecked = false"><xsl:attribute name="disabled"/></xsl:if>
                                            <xsl:call-template name="recursiveOptionList">
                                              <xsl:with-param name="start"    select="1"/>
                                              <xsl:with-param name="end"      select="$curLoopMaxQuantity"/>
                                              <xsl:with-param name="selected" select="$orderQuantity"/>
                                            </xsl:call-template>
                                          </select>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                             </xsl:for-each>
                          </xsl:if>
                          
                          <!-- banner addon -->
                          <xsl:if test="extraInfo/add_ons/banner/AddOnVO">
                             <xsl:for-each select="extraInfo/add_ons/banner/AddOnVO">
                                <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                <xsl:variable name="tabindex" select="40 + position()"/>
                                <xsl:variable name="curPrice" select="format-number(addOnPrice, '#0.00')"/> 
                                <tr>
                                  <td class="labelright"><xsl:value-of select="addOnDescription" disable-output-escaping="yes"/>:</td>
                                  <td colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td width="50%">
                                          <input type="checkbox" name="addonNew_{addOnId}" id="addonNew_{addOnId}" tabindex="{$tabindex}" value="{addOnId}">
                                            <xsl:if test="boolean(/root/AddonsOriginal/Addon[addon_id = $curLoopAddonId])"><xsl:attribute name="CHECKED"/></xsl:if>
                                          </input>
                                          <input type="hidden" name="addonNew_{addOnId}_quantity" id="addonNew_{addOnId}_quantity" value="1"/>
                                          $<xsl:value-of select="format-number(addOnPrice, '#,###,##0.00')"/>
                                          <input type="hidden" name="addonNew_{addOnId}_price" value="{$curPrice}"/>
                                        </td>
                                        <td colspan="2">&nbsp;</td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                             </xsl:for-each>
                          </xsl:if>
                          
                          <!-- card addon -->
                          <xsl:if test="extraInfo/add_ons/card/AddOnVO">
                            <tr>
                              <td class="labelright">Card:</td>
                              <td colspan="2">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <tr>
                                    <td width="50%">
                                      <select name="addonNew_optCard" id="addonNew_optCard" tabindex="45">
                                          <option value="noCard---0.00">No Card</option>
                                        <xsl:for-each select="extraInfo/add_ons/card/AddOnVO">
                                          <xsl:variable name="curLoopAddonId" select="addOnId"/>
                                          <xsl:variable name="curAaPrice" select="format-number(addOnPrice, '#0.00')"/> 
                                          <option value="{addOnId}---{$curAaPrice}">
                                            <xsl:if test="boolean(/root/AddonsOriginal/Addon[addon_id = $curLoopAddonId])"><xsl:attribute name="SELECTED"/></xsl:if>
                                            <script>document.write(convertHtmlEntity('<xsl:value-of select="concat(addOnDescription, ' - $', format-number(addOnPrice, '#,###,##0.00'))"/>'))</script>
                                          </option>
                                        </xsl:for-each>
                                      </select>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                             <tr>
                               <td>&nbsp;</td>
                               <td>
                                 <a tabindex="49" href="#" onclick="javascript:openGreetingCardPopup();">View and Select cards</a>
                               </td>
                               <td class="instruction">&nbsp;</td>
                             </tr>
                          </xsl:if>
                          

                          <!-- Addon error messages - Check if any current Add-ons no longer allowed -->
                            
                          <xsl:for-each select="/root/AddonsOriginal/Addon">
                              <xsl:variable name="curOrigAddonId" select="addon_id"/>
                              <xsl:variable name="curOrigAddonQty" select="addon_qty"/>
                              <xsl:variable name="curOrigAddonDesc" select="description"/>
                              <xsl:if test="(boolean(/root/extraInfo/add_ons/vase/AddOnVO[addOnId = $curOrigAddonId]) = false()) and
                                            (boolean(/root/extraInfo/add_ons/add_on/AddOnVO[addOnId = $curOrigAddonId]) = false()) and
                                            (boolean(/root/extraInfo/add_ons/banner/AddOnVO[addOnId = $curOrigAddonId]) = false()) and
                                            (boolean(/root/extraInfo/add_ons/card/AddOnVO[addOnId = $curOrigAddonId]) = false())
                                           ">
                                <tr>
                                  <td colspan="3" class="requiredFieldTxt">A previously selected add-on, <xsl:value-of select="$curOrigAddonDesc" disable-output-escaping="yes"/>, is unavailable for this product.</td>
                                </tr>
                              </xsl:if>
                              <xsl:if test="(boolean(/root/extraInfo/add_ons/add_on/AddOnVO[addOnId = $curOrigAddonId]) = true()) and
                                            ($curOrigAddonQty > /root/extraInfo/add_ons/add_on/AddOnVO[addOnId = $curOrigAddonId]/maxQuantity)">
                                <tr>
                                  <td colspan="3" class="requiredFieldTxt">Quantity for <xsl:value-of select="$curOrigAddonDesc" disable-output-escaping="yes"/> is no longer valid, please select a new quantity for this add-on.</td>
                                </tr>
                              </xsl:if>
                          </xsl:for-each>
                            
                        </table>
                      </xsl:when>

                      <!-- No matching products -->
                      <xsl:otherwise>
                        <table width="100%" height="100%" border="0" cellspacing="2" cellpadding="2">
                          <tr>
                            <td align="center" class="waitMessage">
                                No products were found matching your criteria.
                            </td>
                          </tr>
                        </table>
                      </xsl:otherwise>
                    </xsl:choose>
                  </div>
                  <!-- End scrolling div -->

                </td>
              </tr>
            </table>
          </div>
          <!-- End content div -->

          <!-- Processing message div -->
          <div id="waitDiv" style="display:none">
            <table id="waitTable" width="100%" border="0" cellpadding="0" cellspacing="2" height="100%">
              <tr>
                <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                <td id="waitTD"  width="50%" class="waitMessage"></td>
              </tr>
            </table>
          </div>

        </td>
      </tr>
    </table>
    <!-- End main table -->

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="20%">&nbsp;</td>
        <td width="60%" align="center">
          <xsl:choose>
            <xsl:when test="productList/products/product/@status = 'A'">
              <xsl:choose>
                <xsl:when test="$productAttributeExclusion">
                  &nbsp;
                </xsl:when>

                <!-- JCP Food Order -->
                <xsl:when test="/root/pageHeader/headerDetail[@name='jcp_flag']/@value = 'JP' and productList/products/product/@jcpCategory != 'A'">
                  <input type="button" name="backButton" value="Back" tabindex="50" onclick="javascript:doLastPageAction();"/>
                </xsl:when>

                <!-- Floral gifts can no longer be delivered to this zip code -->
                <xsl:when test="key('pageData', 'displaySpecGiftPopup')/value = 'Y'">
                  <input type="button" name="backButton" value="Back" tabindex="50" onclick="javascript:doLastPageAction();"/>
                </xsl:when>
                
                <!-- Personalized products cannot be selected. -->
                <xsl:when test="key('pageData', 'displayPersonalizedProduct')/value = 'Y'">
                  <input type="button" name="backButton" value="Back" tabindex="50" onclick="javascript:doLastPageAction();"/>
                </xsl:when>
                
                <xsl:otherwise>
                  <input type="button" name="submitButton" value="Submit" tabindex="50" onclick="javascript:doSubmitAction();"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <input type="button" name="backButton" value="Back" tabindex="50" onclick="javascript:doLastPageAction();"/>
            </xsl:otherwise>
          </xsl:choose>
          &nbsp;
          <input type="button" name="closeButton" value="Close" tabindex="51" onclick="javascript:doCloseAction();"/>
        </td>
        <td width="20%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="52" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

  <!-- Large image popup -->
  <div id="showLargeImage" style="display:none; position:absolute; border:1px solid Black; border-color:Black; background:#FFF8DC;">
    <center>
    <br/><br/>
    <script language="JavaScript"><![CDATA[
      document.write("<img ")
      document.write("ONERROR=\"var imag = this.src.substring(this.src.length-9, this.src.length); ")
      document.write("if (imag!='npi_2.jpg'){ ")
      document.write("this.src='" + product_images + "npi_2.jpg'}\" ")
      document.write("src='" + product_images + novator_id + ".jpg'/> ");]]>
    </script>
    <br/><br/>
    <a href="#" onclick="javascript:closeLargeImage();">close</a>
    </center>
  </div>

  <!--Begin no codified florist but has common carrier DIV -->
  <div id="noCodifiedFloristHasCommonCarrier" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                I'm sorry but the product you have selected is not currently available in your delivery area.
                <br/><br/>
                This item can be delivered via FedEx as early as DATE. Would you like to select a shipping option or shop for another item?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:noCodifiedFloristHasCommonCarrier();"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin no florist but has common carrier DIV -->
  <div id="noFloristHasCommonCarrier" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                I'm sorry but the zip code you have provided is not currently serviced by an FTD florist for floral deliveries.
                <br/><br/>
                This item can be delivered via FedEx as early as DATE. Would you like to select a shipping option or shop for another item?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:noFloristHasCommonCarrier();"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin GNADD Upsell DIV -->
  <div id="gnaddUpsell" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                NEED IT THERE SOONER?<br/>
              </td>
            </tr>
            <tr>
              <td>
                <xsl:value-of select="key('pageData', 'upsellBaseName')/value"/>&nbsp;is available for delivery as soon as&nbsp;<xsl:value-of select="key('pageData', 'upsellBaseDeliveryDate')/value"/>.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_yes.gif" onclick="javascript:acceptGnaddUpsell();"/>&nbsp;&nbsp;
                <img src="../images/button_no.gif" onclick="javascript:gnaddUpsell();"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin Product Unavailable DIV -->
  <div id="productUnavailable" style="VISIBILITY:hidden; POSITION:absolute; TOP:225px; border:1px solid Black; border-color:Black; background:#FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <xsl:choose>
              <xsl:when test="productList/products/product/@status = 'A'">
                <tr>
                  <td align="center">
                    I'm sorry but the product you have selected is not currently available in your delivery area.
                    <br/><br/>
                    Would you like to shop for another item?
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                    <img src="../images/button_ok.gif" onclick="javascript:productUnavailable();"/>
                  </td>
                </tr>
              </xsl:when>
              <xsl:otherwise>
                  <tr>
                  <td align="center" id="msg">
                    Product is currently unavailable.  Please select another product.
                    <br/>
                  </td>
                  </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="center">
                    <img src="../images/button_ok.gif" onclick="javascript:productUnavailable()"/>
                  </td>
                </tr>
              </xsl:otherwise>
            </xsl:choose>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin Product Unavailable DIV -->
  <div id="personalizedProduct" style="VISIBILITY:hidden; POSITION:absolute; TOP:225px; border:1px solid Black; border-color:Black; background:#FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center" id="personalize">
                Product is currently unavailable.  Please select another product.
                <br/>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:closePersonalizedProduct()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin No Floral DIV -->
  <div id="noFloral" style="visibility:hidden; position:absolute; TOP:225px; border:1px solid Black; border-color:Black; background:#FFF8DC;">
  <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
    <tr>
      <td>
        <table>
          <tr>
            <td align="center">
              Floral Items can no longer be delivered to this zip/postal code area.
  						<br/><br/>
              Please select a specialty item.
            </td>
          </tr>
          <tr>
            <td align="center">
              <input type="button" name="okButton" value="OK" onclick="javascript:closeNoFloral();"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    </table>
  </div>

  <!--Begin No Product DIV -->
  <div id="noProduct" style="VISIBILITY:hidden; POSITION:absolute; TOP:225px; border:1px solid Black; border-color:Black; background:#FFF8DC;">
  <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
    <tr>
      <td>
        <table width="100%" border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td align="center">
  						I'm sorry but the zip code you have provided is not currently serviced by an FTD florist or FedEx.
  						<br/><br/>
              Would you like to shop for another item?
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="center">
              <input type="button" name="okButton" value="OK" onclick="javascript:closeNoProduct();"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </div>


  <!--Begin JCPenney Food DIV -->
  <div id="jcpFood" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                We're sorry, food products are unavailable for purchase using a JCPenney credit card.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:closeJcpFoodItem()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin Codified Special DIV -->
  <div id="codifiedSpecial" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                We're sorry the product you've selected is currently not available in the
                zip code you entered.  However, the majority of our flowers can be delivered
                to this zip code.
                <br/>
                If this is an incorrect zip code, please re-enter the zip code
                below and "Click to Accept".  Otherwise, please click the "Back" button to
                return to shopping.
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_ok.gif" onclick="javascript:codifiedSpecial()"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!--Begin JCP Popup DIV -->
  <div id="JCPPop" style="visibility: hidden; position: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table border="3" bordercolor="maroon" cellpadding="1" cellspacing="1">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td align="center">
                At this time, JC Penney accepts orders going to or coming from the 50 United States
                only.  We can process your Order using any major credit card through FTD.com.
                <br/><br/>
                Would you like to proceed?
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="center">
                <img src="../images/button_no.gif" onclick="javascript:JCPPopChoice('N')"/>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <img src="../images/button_yes.gif" onclick="javascript:JCPPopChoice('Y')"/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  </form>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>

<!-- Internal template to help display select options for addons.  Needed because xsl is sooo lame -->
<xsl:template name="recursiveOptionList">
      <xsl:param name="start"    select="1"/>
      <xsl:param name="end"      select="1"/>
      <xsl:param name="selected" select="1"/>
      <xsl:if test="$end >= $start">
        <option value="{$start}">
        <xsl:if test="($start = $selected) or (($selected >= $end) and ($start = $end))"><xsl:attribute name="SELECTED"/></xsl:if>
        <xsl:value-of select="$start"/></option>
        <xsl:call-template name="recursiveOptionList">
          <xsl:with-param name="start"    select="$start + 1"/>
          <xsl:with-param name="end"      select="$end"/>
          <xsl:with-param name="selected" select="$selected"/>
        </xsl:call-template>
      </xsl:if>
</xsl:template>

</xsl:stylesheet>