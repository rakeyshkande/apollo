<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="tabs">

  <table id="itemTabsTable" width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td>
        <div id="tabs">
          <!-- Shopping Cart Tab -->
          <script type="text/javascript" language="javascript"><![CDATA[
            document.write('<button id="shoppingCart" content="shoppingCartDiv" focusobj="' + ((isHeaderValid) ? "buyer_daytime_phone" : "buyer_first_name") + '" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="showItemAt(SHOPPING_CART_INDEX);">Shopping Cart</button>');]]>
          </script>

          <!-- Item Tabs - need to be in sequential order. -->
          <script type="text/javascript" language="javascript">
            var tabsArray = new Array(document.getElementById("shoppingCart"));
            var _tabCount = 1;
          </script>
          <xsl:for-each select="order/items/item">
            <xsl:sort select="status_order" data-type="number"/>
            <xsl:choose>
              <!-- Invalid items -->
              <xsl:when test="item_status=$invalidItem or item_status=$validItemInvalidHeader">
                <xsl:call-template name="addTab"/>
              </xsl:when>

              <!-- Pending items -->
              <xsl:when test="$PENDING_MODE and item_status=$pendingItem">
                <xsl:call-template name="addTab"/>
              </xsl:when>

              <!-- Removed and Abaonded items -->
              <xsl:when test="$REINSTATE_MODE and item_status=$removedItem or item_status=$abandonedItem">
                <xsl:call-template name="addTab"/>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>

          <!-- More Tab -->
          <div id="moreItemsTab" style="position:absolute;visibility:hidden;">
            <button id="moreTab" content="shoppinCartDiv" class="tabText" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="javascript:showItemAt(SHOPPING_CART_INDEX);scrollToSummary();">More...</button>
          </div>
        </div>
      </td>
    </tr>
  </table>

  <script type="text/javascript" language="javascript"><![CDATA[
    var SHOPPING_CART_INDEX = 0;
    var TAB_WIDTH = 92;
    var MAX_TABS = 5;
    var minItem = 1;
    var maxItem = (tabsArray.length > MAX_TABS) ? minItem+MAX_TABS : tabsArray.length;
    var itemTabsTableOffset = document.getElementById("itemTabsTable").getBoundingClientRect().left + document.body.scrollLeft;
    var currentTabIndex = 0;
    var currentTab;
    var currentContent;

    function tabMouseOut(tab){
    	if(tab.className != "selected")
        tab.className = "button";
    }

    function tabMouseOver(tab){
    	if(tab.className != "selected")
        tab.className = "hover";
    }

    function showTab(tab){
    	if(currentTab){
      	currentTab.className = "button";   
    }
    	tab.className = "selected";
    	currentTab = tab;
    	showTabContent(tab);
    }

    function showTabContent(tab){
    	if(currentContent)
      	currentContent.className = "hidden";

    	currentContent = document.getElementById(tab.content);
    	currentContent.className = "visible";
      document.getElementById(tab.focusobj).focus();
    }

    function initializeTabs(tabToDisplay){
    	currentTab = document.getElementById(tabToDisplay);
    	currentTab.className = "selected";
    	currentContent = document.getElementById(currentTab.content);
    	currentContent.className = "visible";

      displayItems();
      checkPrevNextStatus();
    }
    
    function displayItems() {
      for (var i = minItem, k = 0; i < maxItem; i++, k++){
        tabsArray[i].style.left = (k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset;
        tabsArray[i].style.visibility = "visible";
      }
      showHideMoreTab( ((k * TAB_WIDTH) + TAB_WIDTH + itemTabsTableOffset) , "visible");
    }

    function hideItems() {
      for (var i = minItem; i < maxItem; i++){
        tabsArray[i].style.left = 0;
        tabsArray[i].style.visibility = "hidden";
      }
      showHideMoreTab(0, "hidden");
    }

    function showHideMoreTab(offset, visibility){
      var more = getStyleObject("moreItemsTab");
      more.left = offset;
      more.visibility = visibility;
    }

    function showNextItem(){
      currentTabIndex++;
      showItemAt(currentTabIndex);
    }

    function showPrevItem(){
      currentTabIndex--;
      showItemAt(currentTabIndex);
    }

    function showItemAt(index){
      currentTabIndex = index;
      if ( (currentTabIndex >= maxItem) || (currentTabIndex < minItem) ) {
        hideItems();

        if (currentTabIndex >= maxItem){
          if (tabsArray.length-currentTabIndex < MAX_TABS){
            if ((tabsArray.length-1)%MAX_TABS == 0){
              minItem = currentTabIndex - (MAX_TABS-(tabsArray.length-currentTabIndex));
            }
            else {
              minItem = currentTabIndex - (((tabsArray.length-1)%MAX_TABS)-(tabsArray.length-currentTabIndex));
            }
          }else{
            minItem = parseInt(currentTabIndex/MAX_TABS)*MAX_TABS+1;
          }
        }
        else if (currentTabIndex < minItem){
          if (currentTabIndex < MAX_TABS)
            minItem = 1;
          else
            minItem -= MAX_TABS;
        }
        maxItem = (minItem+MAX_TABS > tabsArray.length) ? tabsArray.length : minItem+MAX_TABS;

        displayItems();
      }//if ( (currentTabIndex >= maxItem) || (currentTabIndex < minItem) )
        showTab(tabsArray[currentTabIndex]);
        checkPrevNextStatus();
      if( isAmazonOrder || isMercentOrder){
        invalidCountryState(currentTabIndex);
      }
    }//end showItemAt()

    function checkPrevNextStatus(){
      document.getElementById("prevItem").disabled = (currentTabIndex <= SHOPPING_CART_INDEX);
      document.getElementById("nextItem").disabled = (currentTabIndex+1 >= tabsArray.length);
    }

    function scrollToSummary(){
      document.location.hash = "itemsSummary";
      document.getElementById("item_link_1").focus();
    }
    
    function invalidCountryState(currentTabIndex)
    { 
      if( currentTabIndex != 0 )
      {    
          if(isAmazonOrder && (stateArray[currentTabIndex] == invalidAmazonStates[0] || stateArray[currentTabIndex] == invalidAmazonStates[1]))
          { 
            alert("Deliveries to Alaska and Hawaii require an additional delivery charge.  This is an Amazon order and must be removed and a new order entered.  Please have your credit card ready.  Amazon will refund you for the original order.  The new order must be entered under source code 9849.");  
            return;   
          }
          
       /* -- Commented the below code As per defect 17686 -- Since we are accepting orders to AK/HI for Mercent  
        if(isMercentOrder && stateArray[currentTabIndex] == invalidMercentStates[0] || stateArray[currentTabIndex] == invalidMercentStates[1])
          { 
            alert("Deliveries to Alaska and Hawaii require an additional delivery charge.  This is a Mercent order and must be removed and a new order entered.  Please have your credit card ready.  Mercent will refund you for the original order.  The new order must be entered under source code 9849.");  
            return;   
          }  */
          
          if(isMercentOrder && countryArray[currentTabIndex] != 'US' && countryArray[currentTabIndex] != '' )
          { 
            alert(mercentChannel+" orders may not be sent to an international address. Please remove this order.");  
            return;   
          }
          
       } 
    }
    
    
    ]]>
  </script>
</xsl:template>

<xsl:template name="addTab">
  <script type="text/javascript" language="javascript">
    document.write('<button id="item{order_number}" focusobj="recip_first_name{line_number}" content="item{order_number}Div" onmouseover="tabMouseOver(this);" onmouseout="tabMouseOut(this);" onclick="javascript:showItemAt(' + _tabCount + ');" style="position:absolute;visibility:hidden;" class="tabText">');
    document.write('  <xsl:value-of select="order_number"/>');
    document.write('</button>');
    tabsArray[_tabCount] = document.getElementById('item<xsl:value-of select="order_number"/>');
    _tabCount++;
  </script>
</xsl:template>
</xsl:stylesheet>
