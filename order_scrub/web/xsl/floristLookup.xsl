<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
    <!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:param name="phoneInput" select="root/pageData/data[name='phoneInput']/value"/>
<xsl:param name="institutionInput" select="root/pageData/data[name='institutionInput']/value"/>
<xsl:param name="addressInput" select="root/pageData/data[name='addressInput']/value"/>
<xsl:param name="cityInput" select="root/pageData/data[name='cityInput']/value"/>
<xsl:param name="stateInput" select="root/pageData/data[name='stateInput']/value"/>
<xsl:param name="zipCodeInput" select="root/pageData/data[name='zipCodeInput']/value"/>
<xsl:param name="countryInput" select="root/pageData/data[name='countryInput']/value"/>
<xsl:param name="productId" select="root/pageData/data[name='productId']/value"/>
<xsl:param name="sourceCodeInput" select="root/pageData/data[name='sourceCodeInput']/value"/>
<xsl:param name="orderNumber" select="root/pageData/data[name='orderNumber']/value"/>
<xsl:param name="deliveryDate" select="root/pageData/data[name='deliveryDate']/value"/>
<xsl:param name="deliveryDateEnd" select="root/pageData/data[name='deliveryDateEnd']/value"/>


<xsl:template match="/root">

<html>
<head>
    <title>FTD - Florist Lookup</title>
    <script type="text/javascript" language="javascript" src="../js/FormChek.js"/>
    <script type="text/javascript" language="javascript" src="../js/util.js"/>
    <script type="text/javascript" language="javascript">
    <![CDATA[
      var fieldNames = new Array("institutionInput", "phoneInput", "addressInput", "cityInput", "stateInput", "zipCodeInput", "countryInput", "productId", "sourceCodeInput", "orderNumber", "deliveryDate", "deliveryDateEnd");
      var images = new Array("floristSearch", "floristCloseTop", "floristCloseBottom", "floristSelectNone");

      function init()
      {
        window.name = "VIEW_FLORIST_LOOKUP";
        window.focus();
        addDefaultListenersArray(fieldNames);
        addImageCursorListener(images);
        document.forms[0].institutionInput.focus();
      }

      function onKeyDown()
      {
        if (window.event.keyCode == 13)
            reopenPopup();
      }

      function reopenPopup()
      {
          var form = document.forms[0];
          check = true;
          business = stripWhitespace(form.institutionInput.value);
          city = stripWhitespace(form.cityInput.value);
          state = stripWhitespace(form.stateInput.value);
          zip = stripWhitespace(form.zipCodeInput.value);
          phone = stripWhitespace(form.phoneInput.value);

          //state is required if business is given
          if ((business.length > 0) & (state.length == 0))
          {
              if (check == true)
              {
                  form.stateInput.focus();
                  check = false;
              }
              form.stateInput.style.backgroundColor = 'pink';
          }

          //city is required if no zip and phone are given
          if ((city.length == 0) & (zip.length == 0) & (phone.length == 0))
          {
              if (check == true)
              {
                  form.cityInput.focus();
                  check = false;
              }
              form.cityInput.style.backgroundColor = 'pink';
          }

          //zip is required if no city, state and phone are given
          if ((zip.length == 0) & (city.length == 0) & (state.length == 0) & (phone.length == 0))
          {
              if (check == true)
              {
                  form.zipCodeInput.focus();
                  check = false;
              }
              form.zipCodeInput.style.backgroundColor = 'pink';
          }

          if (!check)
          {
            alert("Please correct the marked fields")
          }
          else
          {
            form.target = window.name;
            form.submit();
          }
      }

      function closeFloristLookup(id)
      {
        if (window.event.keyCode == 13)
          populatePage(id);
      }

      function populatePage(id)
      {
        var code = "";
        var name = "";
        if (id > -1){
          code = document.getElementById("floristId" + id).innerHTML;
          name = document.getElementById("floristName" + id).innerHTML;
        }

        top.returnValue = new Array(code, name);
        top.close();
      }
    ]]>
    </script>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"></link>
</head>

<body onload="javascript:init();">
    <form name="form" method="get" action="">
    <table  width="98%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td class="header">Florist Lookup</td>
        </tr>
        <tr>
            <td width="100%" valign="top">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <input type="hidden" name="countryInput"    id="countryInput"     value="{$countryInput}"/>
                    <input type="hidden" name="productId"       id="productId"        value="{$productId}"/>
                    <input type="hidden" name="sourceCodeInput" id="sourceCodeInput"  value="{$sourceCodeInput}"/>
                    <input type="hidden" name="orderNumber" id="orderNumber"  value="{$orderNumber}"/>
                    <input type="hidden" name="deliveryDate" id="deliveryDate"  value="{$deliveryDate}"/>
                    <input type="hidden" name="deliveryDateEnd" id="deliveryDateEnd"  value="{$deliveryDateEnd}"/>
                    <tr>
                        <td width="15%" class="labelright">Name:</td>
                        <td width="35%"><input type="text" name="institutionInput" tabindex="0" class="TblText" size="30" maxlength="75" value="{$institutionInput}"/></td>
                        <td width="15%" class="labelright">Phone:</td>
                        <td width="35%"><input type="text" name="phoneInput" tabindex="1" class="TblText" size="20" maxlength="20" value="{$phoneInput}"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="labelright">Address:</td>
                        <td width="35%"><input type="text" name="addressInput" tabindex="2" class="TblText" size="20" maxlength="75" value="{$addressInput}"/></td>
                        <td width="15%" class="labelright">City:</td>
                        <td width="35%"><input type="text" name="cityInput" tabindex="3" class="TblText" maxlength="50" size="20" value="{$cityInput}"/></td>
                    </tr>
                    <tr>
                      <td width="15%" class="labelright">State:</td>
                      <xsl:choose>
                        <xsl:when test="$countryInput='US'">
                          <td width="35%">
                            <select tabindex="4" class="TblText" name="stateInput">
                              <option value=""></option>
                              <xsl:for-each select="stateList/state[countrycode='']">
                                <option value="{statemasterid}"><xsl:value-of select="statename"/></option>
                              </xsl:for-each>
                            </select>
                          </td>
                        </xsl:when>
                        <xsl:when test="$countryInput='CA'">
                          <td width="35%">
                            <select tabindex="4" class="tblText" name="stateInput">
                              <option value=""></option>
                              <xsl:for-each select="stateList/state[countrycode='CAN']">
                                <option value="{statemasterid}"><xsl:value-of select="statename"/></option>
                              </xsl:for-each>
                            </select>
                          </td>
                        </xsl:when>
                        <xsl:otherwise>
                          <td width="35%">
                            <input type="text" name="stateInput" tabindex="4" class="TblText" maxlength="30" size="6" value="{$stateInput}"/>
                          </td>
                        </xsl:otherwise>
                      </xsl:choose>
                      <td width="15%" nowrap="true" class="labelright"> Zip/Postal Code: &nbsp;</td>
                      <td><input type="text" name="zipCodeInput" tabindex="5" class="TblText" maxlength="6" size="6" value="{$zipCodeInput}"/></td>
                    </tr>
                    <tr>
                      <td colspan="4" align="center"><img id="floristSearch" tabindex="6" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()"/></td>
                    </tr>
                    <tr>
                      <td colspan="10"><hr/></td>
                    </tr>
                    <table width="100%" id="FormContainer">
                      <tr>
                          <td class="instruction">&nbsp;Please click on an arrow to select a customer.</td>
                          <td align="right"><img id="floristCloseTop" tabindex="7" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)" onkeydown="javascript:closeFloristLookup(-1)"/></td>
                      </tr>

                      <tr>
                        <td colspan="2">
                          <table>
                            <tr>
                              <td style="vertical-align:center; width:5%;"><img border="0" src="../images/primaryFlorist.gif"/></td>
                              <td style="vertical-align:center; text-align:left; width:95%;">&nbsp;=&nbsp;Primary Florist assigned to source code and zip code</td>
                            </tr>
                          </table>
                        </td>
                      </tr>

                      <tr>
                        <td colspan="2">
                          <table>
                            <tr>
                              <td style="vertical-align:center; width:5%;"><img border="0" src="../images/backupFlorist.gif"/></td>
                              <td style="vertical-align:center; text-align:left; width:95%;">&nbsp;=&nbsp;Backup Florist assigned to source code and zip code</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      
                      <tr>
                          <table class="LookupTable" width="100%">
                              <tr>
                                  <td width="5%" valign="bottom"> &nbsp; </td>
                                  <td width="5%" class="label" valign="bottom">Weight</td>
                                  <td class="label" valign="bottom">&nbsp;</td>
                                  <td class="label" valign="bottom">Member<br/>Number</td>
                                  <td class="label" valign="bottom">Name / Address</td>
                                  <td class="label" valign="bottom">Phone</td>
                                  <td class="label" valign="bottom" align="center">Goto<br/>Flag</td>
                                  <td class="label" valign="bottom" align="center">Sunday<br/>Delivery</td>
                                  <td class="label" valign="bottom" align="center">Mercury<br/>Flag</td>
                                  <td class="label" valign="bottom">Hours</td>
                              </tr>
                              <tr>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                                  <td><hr/></td>
                              </tr>
                              <tr>
                                  <td>
                                      <xsl:for-each select="floristList/florist">
                                          <tr>
                                              <td>
                                                  <xsl:choose>
                                                  <xsl:when test = "floristblocktype != ''">
                                                      <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                                                  </xsl:when>
                                                  <xsl:when test = "florist_blocked = 'Y'">
                                                      <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                                                  </xsl:when>
                                                  <xsl:when test = "florist_suspended = 'Y'">
                                                      <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                                                  </xsl:when>
                                                  <xsl:when test = "status = 'Inactive'">
                                                      <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                      <img onclick="javascript: populatePage('{@num}')" onkeydown="javascript:closeFloristLookup('{@num}')" tabindex="8" src="../images/selectButtonRight.gif"/>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                              </td>
                                              <xsl:choose>
                                                  <xsl:when test="floristzipcode = $zipCodeInput">
                                                      <td align="center" class="matchedSearchResult"><xsl:value-of select="floristweight"/></td>
                                                      <td align="center" class="matchedSearchResult">
                                                        <xsl:choose>
                                                          <xsl:when test="priority = '0'">&nbsp;</xsl:when>
                                                          <xsl:when test="priority = '1'"><img border="0" src="../images/primaryFlorist.gif"/></xsl:when>
                                                          <xsl:otherwise><img border="0" src="../images/backupFlorist.gif"/></xsl:otherwise>
                                                        </xsl:choose>
                                                      </td>
                                                      <td id="floristId{@num}" align="center" class="matchedSearchResult"><xsl:value-of select="floristid"/></td>
                                                      <td class="matchedSearchResult"><span id="floristName{@num}"><xsl:value-of select="floristname" disable-output-escaping="yes"/></span>&nbsp;<xsl:value-of select="address"/>&nbsp;<xsl:value-of select="city"/>&nbsp;<xsl:value-of select="state"/>&nbsp;<xsl:value-of select="floristzipcode"/></td>
                                                      <td class="matchedSearchResult"><xsl:value-of select="phonenumber"/></td>
                                                      <td align="center" class="matchedSearchResult"><xsl:value-of select="gotoflag"/></td>
                                                      <td align="center" class="matchedSearchResult"><xsl:value-of select="sundayflag"/></td>
                                                      <td align="center" class="matchedSearchResult"><xsl:value-of select="mercuryflag"/></td>
                                                      <td align="center" class="matchedSearchResult"><xsl:value-of select="hours"/></td>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                      <td align="center"><xsl:value-of select="floristweight"/></td>
                                                      <td align="center">
                                                        <xsl:choose>
                                                          <xsl:when test="priority = '0'">&nbsp;</xsl:when>
                                                          <xsl:when test="priority = '1'"><img border="0" src="../images/primaryFlorist.gif"/></xsl:when>
                                                          <xsl:otherwise><img border="0" src="../images/backupFlorist.gif"/></xsl:otherwise>
                                                        </xsl:choose>
                                                      </td>
                                                      <td id="floristId{@num}" align="center"><xsl:value-of select="floristid"/></td>
                                                      <td><span id="floristName{@num}"><xsl:value-of select="floristname" disable-output-escaping="yes"/></span>&nbsp;<xsl:value-of select="address"/>&nbsp;<xsl:value-of select="city"/>&nbsp;<xsl:value-of select="state"/>&nbsp;<xsl:value-of select="floristzipcode"/></td>
                                                      <td><xsl:value-of select="phonenumber"/></td>
                                                      <td align="center"><xsl:value-of select="gotoflag"/></td>
                                                      <td align="center"><xsl:value-of select="sundayflag"/></td>
                                                      <td align="center"><xsl:value-of select="mercuryflag"/></td>
                                                      <td align="center"><xsl:value-of select="hours"/></td>
                                                  </xsl:otherwise>
                                              </xsl:choose>
                                          </tr>
                                          <tr>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                            <td><hr/></td>
                                          </tr>
                                      </xsl:for-each>
                                  </td>
                              </tr>
                              <tr>
                                <td>
                                  <img id="floristSelectNone" tabindex="9" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage(-1);" onkeydown="javascript:closeFloristLookup(-1);"/>
                                </td>
                                <td colspan="9"> None of the above </td>
                              </tr>
                          </table>
                      </tr>
                      <tr>
                        <td align="right">
                          <img id="floristCloseBottom" tabindex="10" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage(-1)" onkeydown="javascript:closeFloristLookup(-1);"/>
                        </td>
                      </tr>
                    </table>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>