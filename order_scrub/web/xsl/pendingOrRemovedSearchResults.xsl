<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Variables -->
<xsl:variable name="REMOVED" select="'R'"/>
<xsl:variable name="PENDING" select="'P'"/>

<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:template name="pendingOrRemovedSearchResults">

<html>
<head>
  <title>
    <xsl:if test="key('searchCriteria', 'sc_mode')/value=$PENDING">FTD - Pending Orders Search Results</xsl:if>
    <xsl:if test="key('searchCriteria', 'sc_mode')/value=$REMOVED">FTD - Removed Orders Search Results</xsl:if>
  </title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script type="text/javascript" language="javascript" src="../js/util.js"/>
  <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Initialization
   */
    function init(){
    	setNavigationHandlers();
      setScrollingDivHeight();
      window.onresize = setScrollingDivHeight;
    }

    function setScrollingDivHeight(){
      var searchResultsDiv = document.getElementById("searchResults");
      var newHeight = document.body.clientHeight - searchResultsDiv.getBoundingClientRect().top - 100;
      if (newHeight > 15)
        searchResultsDiv.style.height = newHeight;
    }

  /*
   *  Actions
   */
    function doExitAction(){
      var url = "ScrubSearchServlet" +
                "?servlet_action=main_exit" +
                getSecurityParams(false);

      performAction(url);
    }

    function doReturnToSearchAction(){
      var form = document.forms[0];
      form.method = "get";

      var url = "ScrubSearchServlet" +
                "?sc_mode=" + form.sc_mode.value +
                getSecurityParams(false);

      performAction(url);
    }

    function doOrderAction(inValue){
      document.forms[0].selected_master_order_number.value = inValue;
      performAction("ScrubSearchServlet");
    }

    function performAction(url){
      showWaitMessage("content", "wait", "Searching");
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
  </script>
</head>

<body onload="javascript:init();">

  <!-- Header Template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName">
      <xsl:if test="key('searchCriteria', 'sc_mode')/value=$PENDING"><xsl:value-of select="'Pending Search Results'"/></xsl:if>
      <xsl:if test="key('searchCriteria', 'sc_mode')/value=$REMOVED"><xsl:value-of select="'Removed Search Results'"/></xsl:if>
    </xsl:with-param>
    <xsl:with-param name="showExitButton" select="true()"/>
    <xsl:with-param name="showTime" select="true()"/>
  </xsl:call-template>

  <form name="SearchResultsForm" method="post" action="">
  	<div id="content" style="display:block">
  
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="sc_email_address" value="{key('searchCriteria', 'sc_email_address')/value}"/>
    <input type="hidden" name="sc_date" value="{key('searchCriteria', 'sc_date')/value}"/>
    <input type="hidden" name="sc_date_flag" value="{key('searchCriteria', 'sc_date_flag')/value}"/>
    <input type="hidden" name="sc_product_code" value="{key('searchCriteria', 'sc_product_code')/value}"/>
    <input type="hidden" name="sc_last_name" value="{key('searchCriteria', 'sc_last_name')/value}"/>
    <input type="hidden" name="sc_last_name_flag" value="{key('searchCriteria', 'sc_last_name_flag')/value}"/>
    <input type="hidden" name="sc_phone" value="{key('searchCriteria', 'sc_phone')/value}"/>
    <input type="hidden" name="sc_phone_flag" value="{key('searchCriteria', 'sc_phone_flag')/value}"/>
    <input type="hidden" name="sc_ship_to_type" value="{key('searchCriteria', 'sc_ship_to_type')/value}"/>
    <input type="hidden" name="sc_confirmation_number" value="{key('searchCriteria', 'sc_confirmation_number')/value}"/>
    <input type="hidden" name="sc_pro_confirmation_number" value="{key('searchCriteria', 'sc_pro_confirmation_number')/value}"/>
    <input type="hidden" name="sc_origin" value="{key('searchCriteria', 'sc_origin')/value}"/>
    <input type="hidden" name="sc_csr_id" value="{key('searchCriteria', 'sc_csr_id')/value}"/>
    <input type="hidden" name="sc_search_type" value="{key('searchCriteria', 'sc_search_type')/value}"/>
    <input type="hidden" name="sc_date_from" value="{key('searchCriteria', 'sc_date_from')/value}"/>
    <input type="hidden" name="sc_date_to" value="{key('searchCriteria', 'sc_date_to')/value}"/>
    <input type="hidden" name="sc_mode" value="{key('searchCriteria', 'sc_mode')/value}"/>
    <input type="hidden" name="sc_product_property" value="{key('searchCriteria', 'sc_product_property')/value}"/>
    <input type="hidden" name="sc_preferred_partner" value="{key('searchCriteria', 'sc_preferred_partner')/value}"/>
    <input type="hidden" name="selected_master_order_number"/>

    <!-- Navigation Links -->
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="100%">
          <xsl:if test="key('searchCriteria', 'sc_mode')/value=$PENDING">
            <a class="link" href="#" onclick="javascript:doReturnToSearchAction();">Pending Order Search</a>
          </xsl:if>
          <xsl:if test="key('searchCriteria', 'sc_mode')/value=$REMOVED">
            <a class="link" href="#" onclick="javascript:doReturnToSearchAction();">Re-Instate Order Search</a>
          </xsl:if>
        </td>
      </tr>
    </table>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Search Results column headers -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td colspan="7" width="100%" align="center">

                <!-- Scrolling div contiains search results -->
                <div id="searchResults" style="overflow:auto; width:100%; padding:0px; margin:0px">
                  <table width="100%" border="0" cellpadding="2" cellspacing="2">
                  	<tr>
		              <td width="10%" class="colHeaderCenter">Master Order Number</td>
		              <td width="20%" class="colHeaderCenter">Customer Name</td>
		              <td width="15%" class="colHeaderCenter">Customer City, State</td>
		              <td width="20%" class="colHeaderCenter">Order Date</td>
		              <td width="10%" class="colHeaderCenter">Items In Cart</td>
		              <td width="10%" class="colHeaderCenter">Total Cart Amount</td>
		              <td width="10%" class="colHeaderCenter">CSR ID</td>
		            </tr>
                    <xsl:for-each select="search_result_list/search_result">
                      <xsl:sort select="@sort_number" data-type="number"/>
                      <xsl:variable name="ppAssociatedWithOrder" select="preferred_partner_name"/>
                      <xsl:variable name="ppAlertMessage" select="/root/noPermissionsForPreferredPartner/preferred_partner[name = $ppAssociatedWithOrder]/value"/>
                      <tr>
                        <xsl:choose>
                          <xsl:when test="boolean(/root/preferred_partners_for_user/preferred_partner[name = $ppAssociatedWithOrder])">
                            <!-- Preferred Partner order and CSR has permission -->
                            <td width="10%" align="center"><a tabindex="1" class="link" href="javascript:doOrderAction('{master_order_number}');"><xsl:value-of select="master_order_number"/></a>
                            &nbsp;&nbsp;<span class="LabelPartnerName"><xsl:value-of select="/root/preferred_partners_for_user/preferred_partner[name = $ppAssociatedWithOrder]/value"/></span>
                            </td>
                            <td width="20%" align="center"><xsl:value-of select="buyer_first_name"/>&nbsp;<xsl:value-of select="buyer_last_name"/></td>
                          </xsl:when>
                          <xsl:when test="preferred_partner_name != ''">
                            <!-- Preferred Partner order but CSR lacks permission -->
                            <td width="10%" align="center"><a tabindex="1" class="link" href="javascript:doOrderAction('{master_order_number}');"><xsl:value-of select="master_order_number"/></a>
                            </td>
                            <td width="20%" align="center" class="LabelPartnerName"><xsl:value-of select="preferred_partner_name"/></td>
                          </xsl:when>
                          <xsl:otherwise>
                            <!-- Normal order -->
                            <td width="10%" align="center"><a tabindex="1" class="link" href="javascript:doOrderAction('{master_order_number}');"><xsl:value-of select="master_order_number"/></a></td>
                            <td width="20%" align="center"><xsl:value-of select="buyer_first_name"/>&nbsp;<xsl:value-of select="buyer_last_name"/></td>
                          </xsl:otherwise>
                        </xsl:choose>
                        <td width="15%" align="center"><xsl:value-of select="buyer_city"/>,&nbsp;<xsl:value-of select="buyer_state"/></td>
                        <td width="20%" align="center"><xsl:value-of select="order_date"/></td>
                        <td width="10%" align="center"><xsl:value-of select="item_count"/></td>
                        <td width="10%" align="center">$<xsl:value-of select="format-number(order_total,'#,###,##0.00')"/></td>
                        <td width="10%" align="center"><xsl:value-of select="csr_id"/></td>
                      </tr>
                    </xsl:for-each>
                  </table>
                </div>

              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="2" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>
    
  </div>
  </form>
  
  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD"  width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Footer Template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>