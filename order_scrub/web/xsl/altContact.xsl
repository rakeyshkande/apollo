<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>
<xsl:template name="altContact">

<xsl:if test="order/header/contact_first_name/text() or order/header/contact_last_name/text() or order/header/contact_phone/text() or order/header/contact_phone_ext/text() or order/header/contact_email/text()">
  <table width="100%" border="0" cellpadding="2" cellspacing="2">
    <tr>
      <td class="tblheader" colspan="4">Alternate Contact</td>
    </tr>
    <tr>
      <td width="15%" class="label">First Name:</td>
      <td width="35%"><input type="text" name="contact_first_name" tabindex="20" size="30" maxlength="20" value="{order/header/contact_first_name}" onblur="javascript:fieldBlur();" onfocus="javascript:fieldFocus();"/></td>
      <td width="15%" class="label">Phone:</td>
      <td width="35%">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="35%"><input type="text" name="contact_phone" tabindex="22" size="15" maxlength="20" value="{order/header/contact_phone}"  onblur="javascript:fieldBlur();" onfocus="javascript:fieldFocus();"/></td>
            <td class="label">&nbsp;&nbsp;Ext:&nbsp;<input type="text" name="contact_phone_ext" tabindex="23" size="6" maxlength="10" value="{order/header/contact_phone_ext}"  onblur="javascript:fieldBlur();" onfocus="javascript:fieldFocus();"/>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td width="15%" class="label">Last Name:</td>
      <td width="35%"><input type="text" name="contact_last_name" tabindex="21" size="30" maxlength="20" value="{order/header/contact_last_name}"  onblur="javascript:fieldBlur();" onfocus="javascript:fieldFocus();"/></td>
      <td width="15%" class="label">Email Address:</td>
      <td width="35%"><input type="text" name="contact_email" tabindex="24" size="30" maxlength="55" value="{order/header/contact_email}"  onblur="javascript:fieldBlur();" onfocus="javascript:fieldFocus();"/></td>
    </tr>
  </table>
</xsl:if>

</xsl:template>
</xsl:stylesheet>