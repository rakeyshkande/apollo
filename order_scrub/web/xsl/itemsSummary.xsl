<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

<xsl:template name="itemsSummary">
  <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" bordercolor="#006699">
    <tr>
      <td class="tblheader" colspan="7"><a name="itemsSummary"/>Shopping Cart</td>
    </tr>
    <xsl:if test="key('validationField', 'order_amount')/messages/message">
    <tr>
      <td class="errorMessage" colspan="7">
        <xsl:for-each select="key('validationField', 'order_amount')/messages/message">
          <xsl:value-of select="description"/><br/>
        </xsl:for-each>
      </td>
    </tr>
    </xsl:if>
    <tr>
      <td class="colHeaderCenter">Status</td>
      <td class="colHeaderCenter">Order Number</td>
      <td class="colHeaderCenter">Recipient Name</td>
      <td class="colHeaderCenter">Item</td>
      <td class="colHeaderCenter">Delivery Date</td>
      <td class="colHeaderCenter">Description</td>
      <td class="colHeaderRight">Total</td>
    </tr>

    <!-- The links in the summary must be sequential to match tabs and divs. -->
    <script type="text/javascript" language="javascript">var _tabCount = 1;</script>

    <xsl:for-each select="order/items/item">
      <xsl:sort select="status_order" data-type="number"/>
      <xsl:choose>
        <!-- Invalid items -->
        <xsl:when test="item_status=$invalidItem or item_status=$validItemInvalidHeader">
          <xsl:call-template name="addSummary">
            <xsl:with-param name="useClass" select="false()"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Pending items -->
        <xsl:when test="$PENDING_MODE and item_status=$pendingItem">
          <xsl:call-template name="addSummary">
            <xsl:with-param name="useClass" select="false()"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Removed and Abaonded items -->
        <xsl:when test="$REINSTATE_MODE and item_status=$removedItem or item_status=$abandonedItem">
          <xsl:call-template name="addSummary">
            <xsl:with-param name="useClass" select="false()"/>
          </xsl:call-template>
        </xsl:when>

        <!-- Gray out text and no link to item -->
        <xsl:otherwise>
          <xsl:call-template name="addSummary">
            <xsl:with-param name="useClass" select="true()"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>

    <tr bgcolor="#006699">
      <td colspan="6" align="right" class="shoppingCartTotal">Shopping Cart Total:</td>
      <td align="right" class="shoppingCartTotal">$<xsl:value-of select="format-number(order/header/order_amount,'#,###,##0.00')"/></td>
    </tr>
  </table>
</xsl:template>

<xsl:template name="addSummary">
  <xsl:param name="useClass"/>
  <tr>
    <td align="left">
      <table width="100%">
        <tr>
        <xsl:choose>
          <xsl:when test="item_status=$invalidItem">
            <td width="20%"><img src="../images/cancel.gif" align="absmiddle"/></td>
            <td>
              <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
              Error
            </td>
          </xsl:when>
          <xsl:when test="item_status=$removedItem or item_status=$abandonedItem">
            <td width="20%"><img src="../images/removed.gif" align="absmiddle"/></td>
            <td>
              <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
              Removed
            </td>
          </xsl:when>
          <xsl:when test="item_status=$pendingItem">
            <td width="20%"><img src="../images/warning.gif" align="absmiddle"/></td>
            <td>
              <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
              Pending
            </td>
          </xsl:when>
          <xsl:when test="item_status=$validItem or item_status=$processedItem">
            <td width="20%"><img src="../images/GreenThumbsUp.jpg" height="18" width="18"/></td>
            <td>
              <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
              Processed
            </td>
          </xsl:when>
          <xsl:when test="item_status=$validItemInvalidHeader">
            <td width="20%"><img src="../images/valid.gif"/></td>
            <td>
              <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
              Valid
            </td>
          </xsl:when>
        </xsl:choose>
        </tr>
      </table>
    </td>
    <xsl:choose>
      <xsl:when test="boolean($useClass)">
        <td class="gray" align="center"><xsl:value-of select="order_number"/></td>
      </xsl:when>
      <xsl:otherwise>
        <td align="center">
          <script type="text/javascript" language="javascript">
            document.write('<a id="item_link_' + _tabCount + '" tabindex="24" href="#" onclick="javascript:showItemAt(' + _tabCount + '); " onkeypress="javascript:showItemAt(' + _tabCount + ');">');
            document.write('<xsl:value-of select="order_number"/>');
            document.write('</a>');
            _tabCount++;
          </script>
          <xsl:if test="premier_collection_flag = 'Y'"> &nbsp;&nbsp;&nbsp;<img border="0" src="../images/luxury.jpg" align="absmiddle"/></xsl:if>
        </td>
      </xsl:otherwise>
    </xsl:choose>
    <td align="center">
      <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
      <xsl:value-of select="recip_first_name"/>&nbsp;<xsl:value-of select="recip_last_name"/>
    </td>
    <td align="center">
      <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
      <xsl:choose>
        <xsl:when test="subcodeid!=''"><xsl:value-of select="subcodeid"/></xsl:when>
        <xsl:otherwise><xsl:value-of select="productid"/></xsl:otherwise>
      </xsl:choose>
    </td>
    <td align="center">
      <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
      <xsl:value-of select="delivery_date"/>
    </td>
    <td align="center">
      <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
      <xsl:value-of disable-output-escaping="yes" select="product_description"/>
    </td>
    <td align="right">
      <xsl:if test="boolean($useClass)"><xsl:attribute name="class">gray</xsl:attribute></xsl:if>
      <xsl:value-of select="format-number(order_total,'#,###,##0.00')"/>
    </td>
  </tr>
</xsl:template>
</xsl:stylesheet>