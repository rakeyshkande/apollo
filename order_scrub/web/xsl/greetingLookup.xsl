<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>
<xsl:variable name="product_images" select="key('pageData', 'product_images')/value"/>
<xsl:template match="/root">

<html>

<head>
  <title>FTD - Lookup Greeting Card</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script language="javascript" src="../js/FormChek.js"/>
  <script language="javascript" src="../js/util.js"/>
  <script language="javascript">
  /*
   *  Global variables
   */
    var product_images = "<xsl:value-of select="$product_images"/>";<![CDATA[

  /*
   *  Actions
   */
    function doCloseAction(cardId, cardPrice){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 13)
            return false;
            
      var retValue = new Array();
      retValue[0] = cardId;
      retValue[1] = cardPrice;
      top.returnValue = retValue;
      top.close();
    }

    function doShowDetailAction(cardId){
      if (window.event.keyCode == 13)
        openDetailPopup(cardId);
    }

    function doReturnToMainMenuAction(){
        doCloseAction("","");
    }

  /*
   *  Popups
   */
    function openDetailPopup(cardId){
      var form = document.forms[0];
      var url_source =  "PopupServlet" +
                        "?POPUP_ID=LOOKUP_GREETING_CARD" +
                        "&card_id=" + cardId +
                        "&occasion_id=" + form.occasion_id.value +
                        getSecurityParams(false);

      var modal_dim="dialogWidth:600px; dialogHeight:630px; center:yes; scroll:no";
      var ret = showModalDialogIFrameServlet(url_source, "", modal_dim);

      //if the return value is BACK then the back button was clicked, so keep this window open
      if (ret && ret != null && ret[0] == "BACK") {
        return false;
      }

      //if a return value is received from the detail page, then populate the main page
      if (ret && ret != null && ret[0] != '') {
        doCloseAction(ret[0], ret[1]);
      }

      //if a blank return value is received from the detail page then also close this window
      if(ret && ret[0] == '') {
        doCloseAction("","");
      }

      //the X icon was clicked, so keep this window open
      if (!ret) {
        return false;
      }
    }]]>
  </script>
</head>

<body>
  <form name="form" method="get" action="PopupServlet">
    <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
    <input type="hidden" name="occasion_id" value="{key('pageData', 'occasion_id')/value}"/>
    <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
    <input type="hidden" name="POPUP_ID" value="LOOKUP_GREETING_CARDS"/>

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Lookup Greeting Card'"/>
      <xsl:with-param name="showExitButton" select="'true'"/>
    </xsl:call-template>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td class="instruction" colspan="3">
          Click on image or link for larger photo &amp; details or click on the button to select the card.<br/>
          The card you select will be filled in automatically on the order checkout form.
        </td>
      </tr>
      <tr>
        <td align="right">
          <img tabindex="1" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:doCloseAction('','');" onkeydown="javascript:doCloseAction('','')"/>
        </td>
      </tr>
    </table>

    <table width="98%" class="LookupTable" align="center">
      <tr valign="top">
        <td>
          <table width="98%" border="1" cellpadding="2" cellspacing="2">
            <tr valign="top">
              <script language="JavaScript">
                var counter = 0;
                var price = "";
              </script>
              <xsl:for-each select="searchResults/searchResult">
                <xsl:variable name="curPrice" select="format-number(@price, '#0.00')"/>
                <td align="center" width="33%" valign="top">
                  <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="40%" align="center" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="center" valign="top">
                              <img onclick="javascript:openDetailPopup('{@addonid}');" onkeydown="javascript:doShowDetailAction('{@addonid}');" tabindex="2" src="{$product_images}{@addonid}_1.gif"/>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="top">
                              <a style="cursor:hand;" onclick="javascript:openDetailPopup('{@addonid}');" onkeydown="javascript:doShowDetailAction('{@addonid}');" tabindex="2">
                                <span class="textLink"><xsl:value-of disable-output-escaping="yes" select="@description"/></span>
                              </a>
                            </td>
                          </tr>
                          <tr>
                            <td align="center" valign="top">
                              <img onclick="javascript:doCloseAction('{@addonid}','{$curPrice}');" onkeydown="javascript:doCloseAction('{@addonid}','{$curPrice}');" tabindex="2" src="../images/button_select.gif" width='45' height='15'/>
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td rowspan="3" valign="top">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td colspan="2" class="label">Front:</td>
                          </tr>
                          <tr>
                            <td colspan="2"><xsl:value-of disable-output-escaping="yes" select="@description"/></td>
                          </tr>
                          <tr>
                            <td colspan="2" class="label">&nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="2" class="label">Inside:</td>
                          </tr>
                          <tr>
                            <td colspan="2"><xsl:value-of disable-output-escaping="yes" select="@inside"/><br/></td>
                          </tr>
                          <tr>
                            <td colspan="2" class="label">&nbsp;</td>
                          </tr>
                          <tr>
                            <td width="20%" class="label">Price:</td>
                            <td width="80%">&nbsp;$<xsl:value-of select="format-number(@price,'#,###,##0.00')"/></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                <script language="javascript"><![CDATA[
                  //start a new row with each 4th card
                  counter = counter + 1;
                  if ((counter % 3) == 0)
                    document.write("</tr><tr>");]]>
                </script>
              </xsl:for-each>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table width="98%" border="0" cellpadding="2" cellspacing="2" align="center">
      <tr>
        <td align="right">
          <img tabindex="3" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:doCloseAction('','');" onkeydown="javascript:doCloseAction('','');"/>
        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

  </form>

</body>
</html>

</xsl:template>
</xsl:stylesheet>