<!DOCTYPE ACDemo[
  <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- External templates -->
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>

<!-- Keys -->
<xsl:key name="security" match="/root/security/data" use="name"/>
<xsl:key name="pageData" match="/root/pageData/data" use="name"/>

<xsl:output method="html" indent="yes"/>
<xsl:template match="/root">

<html>
<head>
  <title> FTD - Product Categories</title>
  <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
  <script type="text/javascript" language="javascript" src="../js/util.js"/>
  <script type="text/javascript" language="javascript"><![CDATA[
  /*
   *  Global variables
   */
    var subcategoryHTML = new Array();
    var subcategoryHTMLMapping = new Array();
    var c_counter = 0;
    var catwinopen = false;
    var images = new Array("goButton", "searchBy", "flowerSearch", "customSearch");

  /*
   *  Initialization
   */
    function init(){
    	setNavigationHandlers();
      // Need to pause this, or it doesn't work, some timing flake
      setTimeout("setScrollingDivHeight()",100);
      addImageCursorListener(images);
      document.forms[0].search_input.focus();
      window.name = "CATEGORIES_VIEW";
    }

    function setScrollingDivHeight(){
      var productListingDiv = document.getElementById("productListing");
      productListingDiv.style.height = document.body.clientHeight - productListingDiv.getBoundingClientRect().top - 85;
    }

    function document.onkeydown(){
      if (window.event.keyCode == 13)
        return false;
    }

  /*
   *  Events
   */
    function doCategoryOverEvent(selection){
      selection.className = "EditProductCellHighlight";
    }

    function doCategoryOutEvent(selection){
      selection.className = selection.className.defaultValue;
    }

  /*
   *  Actions
   */
    function doCloseAction(){
      top.close();
    }

    function doExitAction(){
      top.returnValue = new Array("_EXIT_");
      doCloseAction();
    }

    function doSearchAction(){
      if (window.event.keyCode)
        if (window.event.keyCode != 13)
          return false;

      var form = document.basicSearchForm;
      var url = "SearchServlet" +
                "?product_id=" + form.search_input.value +
                paramsHelper();

      performAction(url);
    }

    function doCategorySearchAction(index){
      if (window.event)
        if (window.event.keyCode)
          if (window.event.keyCode != 32)
            return false;

      var url = "ProductListServlet" +
                "?search=category" +
                "&category_index=" + index +
                "&price_point_id=" +
                paramsHelper();

      performAction(url);
    }

    function paramsHelper(){
      var form = document.basicSearchForm;
      
      var elem = document.getElementById('basicSearchForm').elements;
      var addon_str = "";
      for(var i = 0; i < elem.length; i++) {
          if (elem[i].name.indexOf("addonOrig_") > -1) {
             addon_str += elem[i].name + "=" + elem[i].value;
          }
      }
      var params =  "&company_id=" + form.company_id.value +
                    "&partner_id=" + form.partner_id.value +
                    "&page_name=" + form.page_name.value +
                    "&page_number=init" +
                    "&occasion_text=" + form.occasion_text.value +
                    "&occasion_id=" + form.occasion_id.value +
                    "&search=simpleproductsearch" + 
                    "&reward_type=" + form.reward_type.value +
                    "&script_code=" + form.script_code.value +
                    "&source_code=" + form.source_code.value +
                    "&city=" + form.city.value +
                    "&state=" + form.state.value +
                    "&postal_code=" + form.postal_code.value +
                    "&country=" + form.country.value +
                    "&master_order_number=" + form.master_order_number.value +
                    "&orig_date=" + form.orig_date.value +
                    "&item_order_number=" + form.item_order_number.value +
                    "&buyer_full_name=" + form.buyer_full_name.value +
                    "&domestic_flag=" + form.domestic_flag.value +
                    "&international_service_fee=" + form.international_service_fee.value +
                    "&price_point_id=" + form.price_point_id.value +
                    "&jcp_flag=" + form.jcp_flag.value +
                    "&domestic_service_fee=" + form.domestic_service_fee.value + 
                    addon_str + 
                    "&shipMethod=" + form.ship_method.value +
                    "&delivery_date=" + form.delivery_date.value +
                    "&shipping_method=" + form.shipping_method.value +
                    "&order_guid=" + form.order_guid.value +
                    getSecurityParams(false);

      return params;
    }

    function performAction(url){
      showWaitMessage("contentDiv", "wait");
      document.basicSearchForm.action = url;
      document.basicSearchForm.target = window.name;
      document.basicSearchForm.submit();
    }

  /*
   *  Subcat box loading, displaying, and dragging
   */
    var isWinMoving = false;
    var curX = 0;
    var curY = 0;

    function startSubCategoryHTML(_categoryName){
      subcategoryHTML[c_counter] = "";
      subcategoryHTMLMapping[c_counter] = _categoryName;
      subcategoryHTML[c_counter] = subcategoryHTML[c_counter] + '<span class="popupHeader">' + _categoryName + ' List</span><br>';
    }

    function addSubCategoryHTML(_name, _index){
      subcategoryHTML[c_counter] = subcategoryHTML[c_counter] + '<table cellpadding="0" cellspacing="0"><tr><td tabindex="6" onclick="javascript:doCategorySearchAction(' + _index + ');" onkeypress="javascript:doCategorySearchAction(' + _index + ');"><u>' + _name + '</u></td></tr></table>';
    }

    function endSubCategoryHTML(){
      c_counter++;
    }

    function doShowCategory(sender){
      cen = document.body.clientWidth/2 - 155;
      subcategorywin = document.getElementById("subcategory").style;
      scroll_top = document.body.scrollTop;

      for (var i = 0; i < c_counter; i++){
        if (sender.id == subcategoryHTMLMapping[i]){
          document.getElementById("subcategorydetail").innerHTML = subcategoryHTML[i];
          break;
        }
      }

      if (curX == 0 && curX == 0){
        subcategorywin.top = scroll_top + document.body.clientHeight/2 - 155;
        subcategorywin.left = document.body.clientWidth/2 - 200/2;
      }
      else{
        subcategorywin.top = scroll_top + curY;
        subcategorywin.left = curX;
      }

      subcategorywin.height = 200;
      subcategorywin.display = "block";
      hideShowCovered(subcategory);
      catwinopen = true;
    }

    function getMousePosition(e){
      if (isWinMoving){
        _x = event.clientX;
        _y = event.clientY;
        curX = subcategorywin.left = _x - 15;
        curY = subcategorywin.top = _y - 10;
        hideShowCovered(subcategory);
      }
      return true;
    }

    function moveBtn_onmouseclick(button){
      button.style.cursor = "move";
      isWinMoving = !isWinMoving;
      
      if (isWinMoving)
        document.attachEvent("onmousemove", getMousePosition);
      else
        document.detachEvent("onmousemove", getMousePosition);
    }

    function dismisssubcatbox(){
      catwinopen = false;
      subcategorywin.display = "none";
      hideShowCovered(subcategory);
    }

  /*
   *  Occasion, Product, Price column changes
   */
    function showByHelper(occasionFlag, productFlag){
      setOccasionColumn(occasionFlag);
      setProductColumn(productFlag);

      /**
       *  Not currently using price point category search functionality.
       *
       *  setPriceColumn(priceFlag);
       */
    }

    function doShowByProduct(){
      showByHelper(false, true);
    }
    function setProductColumn(selected){
      document.getElementById("productCol").className = (selected) ? "EditProductColHeaderSelected" : "EditProductColHeaderDeselect";
      document.getElementById("productLayer").className = (selected) ? "EditProductCellSelected" : "EditProductCellDeselect";
    }

    function doShowByOccasion(){
      showByHelper(true, false);
    }
    function setOccasionColumn(selected)  {
      document.getElementById("occasionCol").className = (selected) ? "EditProductColHeaderSelected" : "EditProductColHeaderDeselect";
      document.getElementById("occasionLayer").className = (selected) ? "EditProductCellSelected" : "EditProductCellDeselect";
    }]]>
  </script>
</head>

<body onload="javascript:init();">

    <!-- Header template -->
    <xsl:call-template name="header">
      <xsl:with-param name="headerName" select="'Edit Product'"/>
      <xsl:with-param name="showExitButton" select="true()"/>
      <xsl:with-param name="showTime" select="false()"/>
    </xsl:call-template>

    <!-- Main table -->
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>

          <!-- Order Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="5">Order Information for <xsl:value-of select="key('pageData', 'item_order_number')/value"/></td>
            </tr>
            <tr>
              <td width="25%" class="label">Master Order Number:</td>
              <td width="25%"><xsl:value-of select="key('pageData','master_order_number')/value"/></td>
              <td width="15%" class="label">Customer:</td>
              <td width="35%"><xsl:value-of select="key('pageData','buyer_full_name')/value"/></td>
            </tr>
            <tr>
              <td colspan="4" class="tblheader">Categories for '<xsl:value-of select="key('pageData', 'occasion_text')/value"/>'</td>
            </tr>
          </table>

          <!-- Content div -->
          <div id="contentDiv" style="display:block">

            <!-- Basic Search Form -->
            <form name="basicSearchForm" id="basicSearchForm" method="post" action="">
              <input type="hidden" name="securitytoken" value="{key('security', 'securitytoken')/value}"/>
              <input type="hidden" name="context" value="{key('security', 'context')/value}"/>
              <input type="hidden" name="international_service_fee" value="{key('pageData', 'international_service_fee')/value}"/>
              <input type="hidden" name="domestic_service_fee" value="{key('pageData', 'domestic_service_fee')/value}"/>
              <input type="hidden" name="master_order_number" value="{key('pageData', 'master_order_number')/value}"/>
              <input type="hidden" name="item_order_number" value="{key('pageData', 'item_order_number')/value}"/>
              <input type="hidden" name="buyer_full_name" value="{key('pageData', 'buyer_full_name')/value}"/>
              <input type="hidden" name="price_point_id" value="{key('pageData', 'price_point_id')/value}"/>
              <input type="hidden" name="occasion_text" value="{key('pageData', 'occasion_text')/value}"/>
              <input type="hidden" name="domestic_flag" value="{key('pageData', 'domestic_flag')/value}"/>
              <input type="hidden" name="occasion_id" value="{key('pageData', 'occasion_id')/value}"/>
              <input type="hidden" name="postal_code" value="{key('pageData', 'postal_code')/value}"/>
              <input type="hidden" name="source_code" value="{key('pageData', 'source_code')/value}"/>
              <input type="hidden" name="script_code" value="{key('pageData', 'script_code')/value}"/>
              <input type="hidden" name="reward_type" value="{key('pageData', 'reward_type')/value}"/>
              <input type="hidden" name="order_guid" value="{key('pageData', 'order_guid')/value}"/>
              <input type="hidden" name="company_id" value="{key('pageData', 'company_id')/value}"/>
              <input type="hidden" name="partner_id" value="{key('pageData', 'partner_id')/value}"/>
              <input type="hidden" name="orig_date" value="{key('pageData', 'orig_date')/value}"/>
              <input type="hidden" name="page_name" value="{key('pageData', 'page_name')/value}"/>
              <input type="hidden" name="jcp_flag" value="{key('pageData', 'jcp_flag')/value}"/>
              <input type="hidden" name="country" value="{key('pageData', 'country')/value}"/>
              <input type="hidden" name="state" value="{key('pageData', 'state')/value}"/>
              <input type="hidden" name="city" value="{key('pageData', 'city')/value}"/>
              <input type="hidden" name="category_index"/>
              <input type="hidden" name="delivery_date" value="{key('pageData', 'delivery_date')/value}"/>
              <input type="hidden" name="shipping_method" value="{key('pageData', 'shipping_method')/value}"/>
              <xsl:for-each select="/root/AddonsOriginal/Addon">
                <input type="hidden" name="addonOrig_{addon_id}" value="{addon_qty}"/>
              </xsl:for-each>
              <input type="hidden" name="ship_method" value="{key('pageData', 'ship_method')/value}"/>

              <!-- Category and Search Filters -->
              <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td width="100%" align="center" class="label">
                    Item Number:
                    &nbsp;&nbsp;&nbsp;
                    <input type="text" name="search_input" tabindex="2" onkeydown="javascript:doSearchAction();" onFocus="javascript:fieldFocus();" onblur="javascript:fieldBlur();"/>
                    &nbsp;&nbsp;&nbsp;
                    <img id="goButton" src="../images/button_go.gif" tabindex="3" align="absmiddle" onkeydown="javascript:doSearchAction();" onclick="javascript:doSearchAction();"/>
                  </td>
                </tr>
              </table>

              <!-- Scrolling div contiains categories -->
              <div id="productListing" style="overflow:auto; width:100%; padding:0px; margin:0px;">

                <!-- Occasion, Product, Price -->
                <table id="basicSearchTable" width="100%" border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td width="100%" align="center">
                      <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">
                        <tr>

                          <!-- Occasion column -->
                          <td id="occasionLayer" width="33%" valign="top" style="border-left: thin solid #F0E68C; border-bottom: thin solid #F0E68C;" onmouseover="javascript:doShowByOccasion();">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="33%" id="occasionCol" class="EditProductColHeaderDeselect">Occasion</td>
                              </tr>
                              <xsl:for-each select="/root/occasion_indexes/index">
                                <xsl:variable name="occasionindexid" select="@indexid"/>
                                <tr>
                                  <xsl:if test="@subindexesexist='N'">
                                    <td id="{@name}" tabindex="6" onclick="javascript:doCategorySearchAction('{@indexid}');" onkeypress="javascript:doCategorySearchAction('{@indexid}');" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<u><xsl:value-of disable-output-escaping="yes" select="@name"/></u>
                                    </td>
                                  </xsl:if>
                                  <xsl:if test="@subindexesexist='Y'">
                                    <td id="{@name}" tabindex="6" onclick="javascript:doShowCategory(this);" onkeypress="javascript:doShowCategory(this);" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<xsl:value-of disable-output-escaping="yes" select="@name"/>
                                      <script type="text/javascript">
                                        startSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>");
                                        <xsl:for-each select="/root/subindexes/index[@parentindexid=$occasionindexid]">
                                          addSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>", "<xsl:value-of select="@indexid"/>");
                                        </xsl:for-each>
                                        endSubCategoryHTML();
                                      </script>
                                    </td>
                                  </xsl:if>
                                </tr>
                              </xsl:for-each>
                            </table>
                          </td>

                          <!-- Product column -->
                          <td id="productLayer" width="33%" valign="top" style="border-left: thin solid #F0E68C; border-right: thin solid #F0E68C; border-bottom: thin solid #F0E68C;" onmouseover="javascript:doShowByProduct();">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="33%" id="productCol" class="EditProductColHeaderDeselect">Product</td>
                              </tr>
                              <xsl:for-each select="/root/product_indexes/index">
                                <xsl:variable name="productindexid" select="@indexid"/>
                                <tr>
                                  <xsl:if test="@subindexesexist='N'">
                                    <td id="{@name}" tabindex="6" onclick="javascript:doCategorySearchAction('{@indexid}');" onkeypress="javascript:doCategorySearchAction('{@indexid}');" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<u><xsl:value-of disable-output-escaping="yes" select="@name"/></u>
                                    </td>
                                  </xsl:if>
                                  <xsl:if test="@subindexesexist='Y'">
                                    <td id="{@name}" tabindex="6" onclick="javascript:doShowCategory(this);" onkeypress="javascript:doShowCategory(this);" onfocus="javascript:doCategoryOverEvent(this);" onblur="javascript:doCategoryOutEvent(this);" onmouseover="javascript:doCategoryOverEvent(this);" onmouseout="javascript:doCategoryOutEvent(this);">
                                      &nbsp;&nbsp;<xsl:value-of disable-output-escaping="yes" select="@name"/>
                                      <script type="text/javascript">
                                        startSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>");
                                        <xsl:for-each select="/root/subindexes/index[@parentindexid=$productindexid]">
                                          addSubCategoryHTML("<xsl:value-of disable-output-escaping="yes" select="@name"/>", "<xsl:value-of select="@indexid"/>");
                                        </xsl:for-each>
                                        endSubCategoryHTML();
                                      </script>
                                    </td>
                                  </xsl:if>
                                </tr>
                              </xsl:for-each>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </div>

              <!--  Subcategory Div Popup -->
              <div id="subcategory" style="display:none; position:absolute; border:1px solid black; border-color:black; background:#FFF8DC; cursor:default;">
                <button id="moveBtn" style="border: none; font-family: Verdana; font-style: normal; width: 99px; height: 22; text-align: left; font-weight: normal; background-color: #808080; text-decoration: none;" onClick="javascript:moveBtn_onmouseclick(this)"><font color="#FFFFFF">Move Window</font></button>
                <button id="closeBtn" style="border: none; font-family: Verdana; font-style: normal; width: 99px; height: 22; text-align: left; font-weight: normal; background-color: #808080; text-decoration: none;"  onClick="javascript:dismisssubcatbox();"><font color="#FFFFFF">Close Window</font></button>
                <table cellspacing="20" border="0">
                  <tr>
                    <td>
                      <div id="subcategorydetail"/>
                    </td>
                  </tr>
                </table>
              </div>
            </form>
            <!-- End Basic Search form -->
          </div>

          <!-- Processing message div -->
          <div id="waitDiv" style="display:none">
            <table id="waitTable" width="100%" border="0" cellpadding="0" cellspacing="2" height="100%">
              <tr>
                <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
                <td id="waitTD"  width="50%" class="waitMessage"></td>
              </tr>
            </table>
          </div>

        </td>
      </tr>
    </table>
    
    <!-- Action Buttons -->
    <table width="98%" border="0" align="center" cellpadding="2" cellspacing="2">
      <tr>
        <td width="10%"></td>
        <td width="80%" align="center">
          <input type="button" name="closeButton" value="Close" tabindex="30" onClick="javascript:doCloseAction();"/>
        </td>
        <td width="10%" align="right">
          <input type="button" name="exitButton" value="Exit" tabindex="33" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

    <!-- Footer template -->
    <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>