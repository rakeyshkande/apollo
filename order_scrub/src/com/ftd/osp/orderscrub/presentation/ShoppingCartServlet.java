package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.SympathyControls;
import com.ftd.osp.orderscrub.business.ServiceOrderActionBO;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.presentation.util.TransformationHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.DispatchHelper;
import com.ftd.osp.orderscrub.util.LockUtil;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.orderscrub.util.OrderSaver;
import com.ftd.osp.orderscrub.util.SearchManager;
import com.ftd.osp.orderscrub.util.StatsHelper;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

/**
 * This servlet provides the functionality to handle cart level processing for 
 * the Scrub system.  This includes most of the exit points on the shopping cart 
 * pages and the primary submit from the main cart page.  The primary submit does 
 * persist the order to the database.
 *
 * @author Brian Munter
 */

public class ShoppingCartServlet extends HttpServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.ShoppingCartServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";
    private final static String XSL_SEARCH_RESULT_LIST = ".../xsl/searchResults.xsl";
    private final static String SEARCH_SERVLET_LOCATION = "/servlet/ScrubSearchServlet";

  //custom parameters returned from database procedure
  private static final String STATUS_PARAM = "RegisterOutParameterStatus";
  private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";    

    private final static String CLEAN_DISPOSITION_SCRUB = "In-Scrub";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Loads the shopping cart and validation for display.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        if (SecurityHelper.isValidToken(request, response)) 
        {
            this.loadShoppingCart(request, response);          
        }
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. search_exit: This is action is called to handle the exit to the search page 
     * for the current mode the system is in.<br/><br/>
     * 
     * 2. result_list: This action is called for pending and reinstate orders which 
     * need an exit point to return to the result list page.<br/><br/>
     * 
     * 3. main_exit: This is action is called to handle the exit to the main menu 
     * page driven off of security.<br/><br/>
     * 
     * 4. submit_cart: This action is called for handling the cart level order 
     * submission.  This will try to process every item in the order and if 
     * if successful move to the next search result.  If unsuccessful, it will 
     * return to the current order with errors identified.<br/><br/>
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        if (SecurityHelper.isValidToken(request, response)) 
        {
            this.submitShoppingCart(request, response);          
        }
    }

    private void loadShoppingCart(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        PartnerMappingVO partnerMappingVO = null;
        boolean isPartnerOrder = false;  
        
        try
        {        
            // Load order guid from scrub for processing
            String orderGuid = request.getParameter("order_guid");
            //Get is_gcgd_valid from request
            //String is_gcgd_valid = request.getParameter("is_gcgd_valid");
            
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
        
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load complete order from dao
            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
            OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
          
            // Clone the order.  Use the clone for comparisons and replacement.
            OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
          
            // Massage user data to fit scrub
            DataMassager dataMassager = new DataMassager();
            dataMassager.massageOrderData(order, dataRequest.getConnection());

            // Validate order
           // OrderValidator orderValidator = new OrderValidator();
           MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
             
           OrderHelper orderHelper = new OrderHelper();

            if(!StringUtils.isEmpty(order.getOrderOrigin())) {
    			partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());    			
            }            
            boolean isAmazonOrder = order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,
                    ConfigurationConstants.AMAZON_ORIGIN_INTERNET)); 
            boolean isMercentOrder = mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin());
                     
            if(partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()){
            	isPartnerOrder = true;
            }            
            
            OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                    isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));

            // Convert validation to xml and append to response document
            ValidationXAO validationXAO = new ValidationXAO();
            DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());


            // Convert order to xml and append to response document
            TransformationHelper transformationHelper = new TransformationHelper();
            transformationHelper.processOrder(responseDocument, order);
            
            // Add error popup messages.
            OrderLoader orderLoader = new OrderLoader();
            HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
            DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 

            // Load base cart data
            BaseCartBuilder cartBuilder = new BaseCartBuilder();
            DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

            File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    private void submitShoppingCart(HttpServletRequest request, HttpServletResponse response)
    {

        DataRequest dataRequest = null;
        InitialContext context = null;
        UserTransaction userTransaction = null;
        
        String action = request.getParameter("servlet_action");
               
        // Update order status to remove scrub lock if next order
        if(action != null && action.equals("search_exit"))
        {
            try
            {
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Process exit point logic
                this.processExitPoint(request, dataRequest);
            
                // Load return page
                String securityParams = "?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);
                String searchExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + SEARCH_SERVLET_LOCATION + securityParams + "&sc_mode=" + request.getParameter("sc_mode");
                response.sendRedirect(searchExitURL);
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else if(action != null && action.equals("result_list"))
        {
            try
            {
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Process exit point logic
                this.processExitPoint(request, dataRequest);

                // Redirect to result list
                request.getRequestDispatcher(SEARCH_SERVLET_LOCATION).forward(request, response);
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else if(action != null && action.equals("main_exit"))
        {
            try
            {
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Process exit point logic
                this.processExitPoint(request, dataRequest);
                
                // Load return page
                String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

                String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT);
                String adminAction = (String) request.getSession().getAttribute("adminAction"); 
                if(adminAction != null && adminAction.length() > 0) 
                { 
                    mainExitURL = mainExitURL + adminAction;
                } 
                else
                {
                    mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
                }
                
                response.sendRedirect(mainExitURL + securityParams);
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else if(action != null && action.equals("submit_cart")) {
        	
        	boolean isMercentOrder = false;
        	boolean isPartnerOrder = false;
        	PartnerMappingVO partnerMappingVO = null;
        	
            try {      	
            
                // Retrieve guid
                String orderGuid = request.getParameter("order_guid");
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
                boolean hasMilitaryStarPayment = false;
                OrderHelper orderHelper = new OrderHelper();
                String errorPopupMessage = null;                

                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Create the validation document
                //Document validationDocument = DOMUtil.getDocument();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
                
                // 11/28/2006 by chu. Defect 2297,2548. Do not allow AAFES
                // orders with MS payment and ariba orders to have price increase.
                // Clone the order.  Use the clone for comparisons and replacement.
                // 03/08/2007 tschmig Defect 3234 Removed Ariba restrictions
                OrderVO originalOrder = 
                        (OrderVO)(new ObjectCopyUtil().deepCopy(order));                

                // Build order
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());
                hasMilitaryStarPayment = orderHelper.hasMilitaryStarPayment(order);
                
                partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
				if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
					isPartnerOrder = true;
				} else if (mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
					isMercentOrder = true;
				}
                
                // Check to see if has military star (MS) payment or if it's ariba order.
                if(hasMilitaryStarPayment)
                {            
                    new RecalculateOrderBO().recalculate(dataRequest.getConnection(), order);
                    BigDecimal origTotal = new BigDecimal(originalOrder.getOrderTotal());
                    BigDecimal newTotal = new BigDecimal(order.getOrderTotal());
                    
                    if(newTotal.compareTo(origTotal) > 0) {
                        //restore original order
                        order = originalOrder;
                        
                        errorPopupMessage = 
                            ConfigurationUtil.getInstance().getProperty(
                                CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                                CustomMessageConstants.EDIT_AAFES_ORDER_AMOUNT_INCREASE);
              
                    }                                
                }                
                
                //Check if we are processing any items that are currently in the
                //removed of pending state.  If we are change change their disposition
                //in the clean schame to "in scurb" because the user is in the process
                //of altering the order.
                //for(int i = 0; i < order.getOrderDetail().size(); i++) 
               // {
                    //Get item
                    //OrderDetailsVO orderDetailVO = (OrderDetailsVO) order.getOrderDetail().get(i);                
                    
                    
                    //check if removed or pending
                    //if(orderDetailVO.getStatus() != null &&
                    //        (orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.REMOVED_ITEM)) ||
                    //        orderDetailVO.getStatus().equals(Integer.toString(OrderStatus.PENDING_ITEM)) ))
                    //        {
                                //get user id
                                //while running in test mode set default values
                                //String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
                                //String csrId = null;
                                //if(standAloneFlag != null && standAloneFlag.equals("Y"))
                               // {
                                    //csrId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, "TEST_CSR");
                                //}else
                                //{
                                    //get user information
                                   // String securityToken = request.getParameter("securitytoken");
                                    //SecurityManager securityManager = SecurityManager.getInstance();
                                   // UserInfo userInfo = securityManager.getUserInfo(securityToken);      
                                    //csrId = userInfo.getUserID();
                                //}
                            
                                //Commenting this out for PhaseIII QA defect#568.
                                //update status in clean
                                //updateCleanDisposition(dataRequest.getConnection(),orderDetailVO.getOrderDetailId(),CLEAN_DISPOSITION_SCRUB,csrId);
                       //     }
                //}//end order detail loop
                
                
                // Validate order
                boolean isAmazonOrder = order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,
                                        ConfigurationConstants.AMAZON_ORIGIN_INTERNET));
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
                

                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());
                
                // Update status for each item in cart
                boolean validItem;
                boolean validHeader;

                String mode = null;
                OrderDetailsVO item = null;
                DispatchHelper dispatchHelper = new DispatchHelper();
                OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                StatusManagerBO statusManager = new StatusManagerBO();

                // Retrieve a user transaction
                context = new InitialContext();
                String jndiTransactionEntry = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.JNDI_TRANSACTION_ENTRY);
                userTransaction = (UserTransaction)  context.lookup(jndiTransactionEntry);

                // Start the transaction
                userTransaction.begin();
                
                ServiceOrderActionBO serviceOrderActionBO = new ServiceOrderActionBO();
                String csrId = getCsrID(request);
                
           
                for(int i = 0; i < order.getOrderDetail().size(); i++) 
                {
                    // Update item status
                    item = (OrderDetailsVO) order.getOrderDetail().get(i);
                    
                    if (dispatchHelper.isOrderAlreadyDispatched(item.getOrderDetailId(), dataRequest.getConnection()))
                    {
                      // if the order is already dispatched correctly, add an error to the response document
                      responseDocument = dispatchHelper.addOrderDoubleDispatchXML(responseDocument);
                      logger.debug("Scrub attempt to double dispatch: "+item.getOrderDetailId());
                    }
                    else
                    {
                      // Pull the current mode
                      mode = request.getParameter("sc_mode");
                      
                      if(item.getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM)) 
                          || item.getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER))
                          || (mode != null && mode.equals("P") && item.getStatus().equals(String.valueOf(OrderStatus.PENDING_ITEM)))
                          || (mode != null && mode.equals("R") && item.getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM))))
                      {
                          validHeader = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/header[@status='error']")) != null ? false : true;
                          validItem = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/items/item[@line_number=" + item.getLineNumber() + " and @status='error']")) != null ? false : true;
                          statusManager.updateItemStatus(validHeader, validItem, order, item, request.getParameter("sc_mode"));

                         if(item.getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM)))
                          {
                              if(mode != null 
                                  && mode.equals("R")
                                  && order.getPreviousStatus() != null
                                  && order.getPreviousStatus().equals("1008"))
                              {
                                  item.setReinstateFlag("Y");
                                  // Update the service status as the item status may have been changed
                                  serviceOrderActionBO.submitServiceOrderItem(dataRequest.getConnection(), item, order.getBuyerEmailAddress(), csrId);
                              }
                          
                              // Save order to scrub and frp with fraud
                              order.setResetCardinal(true);
                              orderSaver.saveOrder(order, item, true, request);
  
                              
                              //Locking was added for PhaseIII, emueller 4/19/05                               
                              //release buyer record lock
                              LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);
  
                              // Send order and item to dispatcher (not in tandalone)
                              dispatchHelper.dispatchOrder(order, item, "ES1005", request, dataRequest.getConnection());
                              
                              // Check to see if Amazon Order
                              if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                                 ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                                isAmazonOrder = true;
                              
//                              if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
//                            	  isMercentOrder = true;
//                              }

                              //Defect 12761 - Amazon; Use Case # 24156
                              //if CSR makes changes in Scrub that decrease the total order price
                              //insert Adjustment record for Amazon Order with cancel reason code of 'Z2'
                              logger.info("before amazon check");
								BigDecimal origExternalOrderTotal = null;
								BigDecimal externalOrderTotal = null;

								String strValue;
								strValue = item.getOrigExternalOrderTotal();
								if (strValue == null) {
									strValue = "0.00";
								}
								origExternalOrderTotal = new BigDecimal(
										strValue);
								logger.info("origExternalOrderTotal is "
										+ strValue);

								strValue = item.getExternalOrderTotal();
								if (strValue == null) {
									strValue = "0.00";
								}
								externalOrderTotal = new BigDecimal(strValue);
								logger.info("externalOrderTotal is " + strValue);
								
                              if(isAmazonOrder || isMercentOrder || isPartnerOrder)
                              {
                            	  logger.info("is amazon order");
                            	  try{
                              		 
                              	     
                              	    if(externalOrderTotal.compareTo(origExternalOrderTotal) < 0)
                              		{
                              	    	logger.info("calling partnerUtility");
                              	    	if(isAmazonOrder) {
                              	    		this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.AMAZON_ORIGIN, false);
                              	    	} else if (isMercentOrder) {                              	    		
                              	    		this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.MRCNT_ORD_INDICATOR, false);
        								} else if (isPartnerOrder && partnerMappingVO.isSendAdjustmentFeed()) {
        									this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.PTN_ORD_INDICATOR, false);
        								}                              	    	
                              	    	logger.info("after calling partnerUtility");
                              		}
                                    } 
                                    catch (Exception e) 
                                    { 
                                      logger.error("exception: " + e.toString());
                                      throw new Exception("createOrderAdjustment caught: " + e);  
                                    }    
								}
                              
                              	if (partnerMappingVO != null && partnerMappingVO.isSendOrdStatusUpdFeed()) {
                        			Integer priceDifference = origExternalOrderTotal.compareTo(externalOrderTotal);
                        			logger.debug("priceDifference:" + priceDifference + "isSendOrdStatus:" + partnerMappingVO.isSendOrdStatusUpdFeed() 
                        					+ "orderNumber:" + item.getExternalOrderNumber());
                        			if (priceDifference != 0 && partnerMappingVO.isSendOrdStatusUpdFeed()) {
      								new OrderLoader().sendPartnerOSU(item.getExternalOrderNumber(),"SCRUB_ADJUSTED");
      							}
                        		} 
							}
                        }
                        else
                        {
                            // Update the service status
                            //serviceOrderActionBO.submitServiceOrderItem(dataRequest.getConnection(), item, order.getBuyerEmailAddress(), csrId);
                            
                            // Save order to scrub and frp without fraud
                        	order.setResetCardinal(true);
                            orderSaver.saveOrder(order, item, false, request);
                        }
                    }
                }
                
                userTransaction.commit();
                /*for(String payload:items){
                	if(payload.indexOf(String.valueOf(OrderStatus.VALID_ITEM))>0 && payload.indexOf(String.valueOf("true"))>0 ){
                		new OrderLoader().createAdjustmentForAribaOrder(payload.split("~")[0]);
                	}
                }*/
                // If order is complete so load next order
                if(order.getStatus().equals(String.valueOf(OrderStatus.VALID)))
                {
                    // Retrieve guid from search criteria
                    SearchManager searchManager = new SearchManager();
                    SearchResultVO searchResult = searchManager.retrieveResult(request, dataRequest, false);

                    if(searchResult != null) 
                    {
                        // Create the initial document
                        responseDocument = DOMUtil.getDocument();

                        // Load new order to scrub
                        OrderLoader orderLoader = new OrderLoader();
                        orderLoader.loadOrder(searchResult.getGuid(), orderBeforeUpdate, responseDocument, dataRequest, request);

                        File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

                    }
                    else 
                    {
                        logger.debug("** NO ORDERS FOUND TO BE SCRUBBED **");

                        // Load search page
                        String securityParams = "?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);
                        String loadSearchURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + SEARCH_SERVLET_LOCATION + securityParams + "&sc_mode=" + request.getParameter("sc_mode");
                        response.sendRedirect(loadSearchURL);
                    }
                }
                else
                {
                    // Convert order to xml and append to response document
                    TransformationHelper transformationHelper = new TransformationHelper();
                    transformationHelper.processOrder(responseDocument, order);

                    // Load base cart data
                    BaseCartBuilder cartBuilder = new BaseCartBuilder();
                    DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));
                    
                    // Add error popup messages.
                    OrderLoader orderLoader = new OrderLoader();
                    HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                    if (errorPopupMessage != null) 
                    {
                        errorPopup.put("showErrorPopup", "Y");
                        errorPopup.put("errorPopupMessage", errorPopupMessage);
                    }//end if
                    DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true);
                    //Adding sympathy messages
            		SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                    HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                    if(sympathyAlerts!=null){
                    	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
                   } 
                    // Return to order to continue scrub
                    File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                }
            }
            catch(Exception e)
            {
                try  
                {
                    if (userTransaction != null)  
                    {
                        // Rollback the user transaction
                        userTransaction.rollback();
                        logger.debug("User transaction rolled back");
                    }
                } 
                catch (Exception ex)  
                {
                    logger.error(ex);
                } 
                finally  
                {
                }
                
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }

                    if(context != null)
                    {
                        context.close();
                    }
                }
                catch(Exception se) 
                {
                    logger.error(se);
                }
            }
        }
    }

//    private void updateCleanDisposition(Connection conn,long orderDetailId, String disposition, String csr)
//        throws Exception
//    {
//        DataRequest dataRequest = new DataRequest();
//        dataRequest.setConnection(conn);
//        List resultList = new ArrayList();
//        
//        logger.debug("Updated order detail " + orderDetailId + " to have a dispostion of " + disposition + " in Clean.");
//        
//        dataRequest.setStatementID("UPDATE_CLEAN_DISPOSITION");
//        Map paramMap = new HashMap();   
//        
//        paramMap.put("IN_ORDER_DETAIL_ID",Long.toString(orderDetailId));
//        paramMap.put("IN_DISPOSITION",disposition);        
//        paramMap.put("IN_UPDATED_BY",csr);        
//        
//        
//        dataRequest.setInputParams(paramMap);
//        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
//        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
//        
//        String status = (String) outputs.get(STATUS_PARAM);
//        if(status.equals("N"))
//          {
//              String message = (String) outputs.get(MESSAGE_PARAM);
//              throw new Exception(message);
//          }
//
//    }


    private void processExitPoint(HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        // Load order guid from scrub for processing
        String resetGuid = request.getParameter("order_guid");

        // Reset status to previous status
        dataRequest.reset();
        dataRequest.setStatementID("UPDATE_TO_PREVIOUS_STATUS_ANY");
        dataRequest.addInputParam("order_guid", resetGuid);
        DataAccessUtil.getInstance().execute(dataRequest);

        // Load the order for stats recording
        ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
        OrderVO order = scrubMapperDAO.mapOrderFromDB(resetGuid);
        
        //Locking was added for PhaseIII, emueller 4/19/05                               
        //release buyer record lock
        LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);

        // Record exit statistics
        StatsHelper.getInstance().updateOrderStatistics(order, GeneralConstants.STATS_OUT, request, dataRequest.getConnection());
    }


   /**
   * Retrieves the CSR Id from the request
   * @param request
   * @return
   */
    private String getCsrID(HttpServletRequest request)
    {
        String csrId = "SYSTEM";
        
        try
        {
        String securityToken = request.getParameter("securitytoken");
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securityToken);      
        csrId = userInfo.getUserID();      
        } catch (Exception e)
        {
          // NOP 
        }
        
        return csrId;
    }
    
    /**
     * Creates an adjustment feed record for a single item
     */
    private void createAdjustmentFeedRecord(OrderDetailsVO item, String csrID, DataRequest dataRequest, String partnerType, boolean fullAdjustment) throws Exception {
      
        String adjustmentReason = "Z2";      
        try {
            PartnerUtility partnerUtility = new PartnerUtility();
            if(ValidationConstants.AMAZON_ORIGIN.equals(partnerType)) {
            	partnerUtility.createAdjustmentFeedRecord(item, csrID, adjustmentReason, dataRequest.getConnection());
            } else if (ValidationConstants.MRCNT_ORD_INDICATOR.equals(partnerType)) {
            	partnerUtility.createMercentAdjustmentFeedRecord(item, csrID, adjustmentReason, fullAdjustment, dataRequest.getConnection());
            } else if (ValidationConstants.PTN_ORD_INDICATOR.equals(partnerType)) {
            	partnerUtility.createPartnerAdjustmentFeedRecord(item, csrID, adjustmentReason, fullAdjustment, dataRequest.getConnection());
            } 
          } catch (Exception e) { 
            logger.error("Error caught creating adjustment feed record: " + e.toString());
            throw new Exception("createOrderAdjustment caught: " + e);  
          }
    }
}
