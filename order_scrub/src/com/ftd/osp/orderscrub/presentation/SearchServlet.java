package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.presentation.util.EditProductConstants;
import com.ftd.osp.orderscrub.presentation.util.SearchUTIL;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.vo.ProductFlagsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/** This Servlet is used during the Edit Product process.  The servlet
 *  displays a product which matches the entered search criteria.
 *  @author Ed Mueller
 *  
 *  Note: This object was copied from Order Entry. */
public class SearchServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE       = "text/html; charset=windows-1252";
  private static final String XSL_PRODUCT_DETAIL = ".../xsl/productDetail.xsl";
  private static final String XSL_UPSELL         = ".../xsl/upsell.xsl";
  
  
    private ServletConfig config;
    private Logger logger;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
        logger = new Logger("com.ftd.osp.orderscrub.presentation.SearchServlet");                
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }    

    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String searchtype = request.getParameter("search");
        String sessionId = request.getParameter("sessionId");

        //if search type is null then simple search is the default
        if (searchtype == null)
        {
          searchtype = "simpleproductsearch";
        }

        try
        {

            if(searchtype.equals("simpleproductsearch"))
            {
                handleSimpleProductSearch(request, response);
            }
            else
            {
              //error
              throw new Exception("INVALID SEARCH REQUEST:" + searchtype);
            }
        
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
    }
    

    private void handleSimpleProductSearch(HttpServletRequest request, HttpServletResponse response) throws Exception
    {

        SearchUTIL searchUtil = new SearchUTIL();
        Enumeration enum1 = request.getParameterNames();
        String isAribaOrder ="false";
        String param = "";
        String value = "";
//        String soonerOverride = request.getParameter("soonerOverride");
//        if(soonerOverride == null) soonerOverride = "";


        String city = request.getParameter("city");
        String deliveryDate = request.getParameter("delivery_date");

        String searchType = request.getParameter("search_type");        
        String productid = request.getParameter("product_id");             
        String sourceCode = request.getParameter("source_code");
        String rewardType = request.getParameter("reward_type");
        String state = request.getParameter("state");
        String pricingCode = request.getParameter("pricing_code");
        String domesticServiceFee = request.getParameter("domestic_service_fee");
        String partnerId = request.getParameter("partner_id");
        ArrayList promotionList = new ArrayList();
        ProductFlagsVO flagsVO = new ProductFlagsVO();
        String pageName = request.getParameter("script_code");
        String zipCode = request.getParameter("postal_code");
        String domesitcIntlFlag = request.getParameter("domestic_flag");
        String jcpFlag = request.getParameter("jcp_flag");
        String internationalServiceFee = request.getParameter("international_service_fee");
        String occasion = request.getParameter("occasion_id");
        String company = request.getParameter("company_id");
        String categoryIndex = request.getParameter("category_index");
        String sortType = request.getParameter("sort_type");        
        String buyerName = request.getParameter("buyer_full_name");      
        String pricePointId = request.getParameter("price_point_id");
        String scriptCode = request.getParameter("script_code");
        String country = request.getParameter("country");
        String masterOrderNumber = request.getParameter("master_order_number");
        String origDate = request.getParameter("orig_date");
        String itemOrderNumber = request.getParameter("item_order_number");
        String occText = request.getParameter("occasion_text");
        String guid = request.getParameter("order_guid");
        String lineNumber = request.getParameter("line_number");
        String upsellFlag = request.getParameter("upsell_flag");
        String listFlag = request.getParameter("list_flag");
        String upsellID = request.getParameter("upsell_id");
        
		HashMap pageData = new HashMap();
		DataRequest dataRequest = null;
		
		Connection con = null;
		boolean isAribaSC = "YES".equalsIgnoreCase(request
				.getParameter("aribaSourceCode"));
		if (isAribaSC) {
			try {
				isAribaOrder = "true";
				boolean domesticFlag = new SearchUTIL().isDomestic(country);
				dataRequest = DataRequestHelper.getInstance().getDataRequest();
				con = dataRequest.getConnection();
				domesitcIntlFlag = domesticFlag ? "D" : "I";
				dataRequest.setStatementID("SOURCE_CODE_RECORD_LOOKUP");
				dataRequest.addInputParam("source_code", sourceCode);
				dataRequest.setConnection(con);
				CachedResultSet rs = (CachedResultSet) DataAccessUtil
						.getInstance().execute(dataRequest);
				String snhID = "";
				if (rs != null && rs.next()) {
					pricingCode = searchUtil.getValue(rs.getObject(7));
					snhID = searchUtil.getValue(rs.getObject(8));
					partnerId = searchUtil.getValue(rs.getObject(9));
					jcpFlag = searchUtil.getValue(rs.getObject(30));
				}
				if (jcpFlag != null && jcpFlag.equalsIgnoreCase("Y")) {
					scriptCode = ConfigurationUtil.getInstance().getProperty(
							ConfigurationConstants.SCRUB_CONFIG_FILE,
							ConfigurationConstants.JCPENNY_SCRIPT_CODE);
				}
				dataRequest.reset();
				dataRequest.setStatementID("GET_SNH_BY_ID");
				dataRequest.addInputParam("IN_SNH_ID", snhID);
				dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);
				dataRequest.setConnection(con);
				rs = (CachedResultSet) DataAccessUtil.getInstance().execute(
						dataRequest);
				if (rs != null && rs.next()) {
					domesticServiceFee = searchUtil.getValue(rs.getObject(3));
					internationalServiceFee = searchUtil.getValue(rs.getObject(6));
				} else {
					// default fees to zero if no data was found
					domesticServiceFee = "0";
					internationalServiceFee = "0";
				}
			} catch (Exception e) {
				if (dataRequest != null && dataRequest.getConnection() != null
						&& !dataRequest.getConnection().isClosed()) {
					dataRequest.getConnection().close();
				}
			}

		}
		pageData.put("isAribaOrder", isAribaOrder);
        //#587  - Flex-Fill 
        // Added the Shiping method of the original order. 
        String shippingMethod = request.getParameter("shipping_method");
        
        if(upsellFlag == null || upsellFlag.length() <=0)
        {
          upsellFlag = "N";
        }

        if(listFlag == null || listFlag.length() <=0)
        {
          listFlag = "N";
        }
        
       
        pageData.put("upsell_id",upsellID);                
        pageData.put("list_flag",listFlag);
        pageData.put("upsell_flag",upsellFlag);
        pageData.put("city",city);
        pageData.put("delivery_date",deliveryDate);
        pageData.put("search_type",searchType);
        pageData.put("product_id",productid);
        pageData.put("source_code",sourceCode);
        pageData.put("reward_type",rewardType);
        pageData.put("state",state);
        pageData.put("pricing_code",pricingCode);
        pageData.put("domestic_service_fee",domesticServiceFee);
        pageData.put("partner_id",partnerId);
        pageData.put("script_code",scriptCode);
        pageData.put("postal_code",zipCode);
        pageData.put("domestic_flag",domesitcIntlFlag);
        pageData.put("international_service_fee",internationalServiceFee);
        pageData.put("occasion_id",occasion);
        pageData.put("company_id",company);
        pageData.put("categoryIndex",categoryIndex);
        pageData.put("sort_type",sortType);
        pageData.put("buyer_full_name",buyerName);
        pageData.put("price_point_id",pricePointId);
        pageData.put("script_code",scriptCode);
        pageData.put("country",country);
        pageData.put("jcp_flag",jcpFlag);                
        pageData.put("master_order_number", masterOrderNumber);
        pageData.put("orig_date", origDate);
        pageData.put("item_order_number", itemOrderNumber);
        pageData.put("occasion_text", occText);
        pageData.put("order_guid", guid);
        pageData.put("line_number", lineNumber);
        //#587  - Flex-Fill 
        // Added the Shiping method of the original order. 
        pageData.put("shipping_method", shippingMethod);

        //get image location
        String imageLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, EditProductConstants.PRODUCT_IMAGE_LOCATION);          
        pageData.put("product_images",imageLocation);

        //productid = "BB02";
        //sourceCode = "6111";
        //zipCode = "60563";
        //company = "111";
        //deliveryDate = "12/13/03";
        //state = "IL";
        //city = "Naperville";
        //country = "US";
        //occasion = "8";

        boolean domesticFlag = searchUtil.isDomestic(country);              

        HashMap parms = new HashMap();
        while(enum1.hasMoreElements())
        {     
            param = (String) enum1.nextElement();
            value = request.getParameter(param);  
            parms.put(param, value);
        } 



        response.setHeader("Cache-Control", "no-cache");


        try
        {
         Map args = new HashMap();        
         Document xml = null;


         xml = searchUtil.getProductByID( sourceCode,  zipCode,  country,
               productid,  searchType,  domesticFlag, 
               state,  pricingCode,  domesticServiceFee,
               internationalServiceFee,  partnerId,  promotionList,
               rewardType,  flagsVO,  occasion,
               pageName,  company,  deliveryDate,
               city/*, String soonerOverride*/,  deliveryDate, origDate, pageData);       

            if(flagsVO.isUpsellExists()) 
            {
                String xslFileShort = XSL_UPSELL;

                if(flagsVO.getUpsellSkip() != null && !flagsVO.getUpsellSkip().equals("N"))
                {
                    args.put("productId", flagsVO.getUpsellSkip());

                     xml = searchUtil.getProductByID( sourceCode,  zipCode,  country,
                           flagsVO.getUpsellSkip(),  searchType,  domesticFlag, 
                           state,  pricingCode,  domesticServiceFee,
                           internationalServiceFee,  partnerId,  promotionList,
                           rewardType,  flagsVO,  occasion,
                           pageName,  company,  deliveryDate,
                           city/*, String soonerOverride*/,  deliveryDate, origDate, pageData);       
                    
                    xslFileShort = XSL_PRODUCT_DETAIL;
                }

                String xslFile = getServletContext().getRealPath(xslFileShort);
                HashMap params = new HashMap();
      
                params.put("searchType", "simpleproductsearch");
                params.put("productListServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("actionServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));

                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(xml, "security", "data", securityData, true);

                //fix HTML encoding
                searchUtil.fixXML(xml);

                //append addons
                xml = searchUtil.appendAddons(request,xml);
                
                //append product attribute restriction info
                xml = searchUtil.appendProductAttributeExclusions(productid, sourceCode, xml);
                
                //logger.debug(searchUtil.convertDocToString(xml));

                TraxUtil.getInstance().transform(request, response, xml, new File(xslFile), xslFileShort, params);      

            }
            else
            {
                String xslFileShort = XSL_PRODUCT_DETAIL;
                String xslFile = getServletContext().getRealPath(xslFileShort);
                HashMap params = new HashMap();

 
                    

      
                //params.put("persistentObjId", persistentObjId);
                //params.put("sessionId", sessionId);
                //params.put("upsellFlag", upsellFlag);
                //params.put("soonerOverride", soonerOverride);
                params.put("searchType", "simpleproductsearch");
                //params.put("cartItemNumber", itemCartNumber);
                params.put("productListServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));

                //fix HTML encoding
                searchUtil.fixXML(xml);

                //append addons
                xml = searchUtil.appendAddons(request,xml);
                
                //append product attribute restriction info
                xml = searchUtil.appendProductAttributeExclusions(productid, sourceCode, xml);
                 
                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(xml, "security", "data", securityData, true);

                //logger.debug(searchUtil.convertDocToString(xml));

                TraxUtil.getInstance().transform(request, response, xml, new File(xslFile), xslFileShort, params);      

            }
        }
        finally
        {
        }               
        
    }



}  
