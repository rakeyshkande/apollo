package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.messagegenerator.bo.OrderEmailGeneratorBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.GUID.GUIDGenerator;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.email.EmailVO;
import com.ftd.osp.utilities.order.OrderXAO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

/**
 * This servlet is used to manage the functionality of the email page for 
 * the Scrub system.  Email templates and canned statements are dynamically 
 * loaded from the database and this servlet drives that logic.
 *
 * @author Brian Munter
 */

public class EmailServlet extends HttpServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.EmailServlet";
    private final static String EMAIL_FROM_ADDRESS = "FromAddress";
    private final static String XSL_EMAIL = ".../xsl/email.xsl";
    private final static String XSL_EMAIL_CONTENT = ".../xsl/emailContentIframe.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. load_content: This is action is called from an IFrame to internally
     * load the email template without refreshing the entire page.  The email 
     * templates are stored in the database and are valid based on the current
     * company id.<br/><br/>
     * 
     * 2. default: Default functionality to load the email page and canned 
     * statements for intial display.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadEmail(request, response);          
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. close: This is action is called to redirect the user to the previous page.<br/><br/>
     * 
     * 2. default: Default functionality to validate and submit the email back to 
     * the shopping cart.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.submitEmail(request, response);          
    }

    private void loadEmail(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;

        String action = request.getParameter("servlet_action");
    
        if(action != null && action.equals("load_content"))
        {
            try
            {
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");
                String cartFlag = request.getParameter("cart_flag");
            
                // Create the initial document
                Document emailDocument = DOMUtil.getDefaultDocument();
                Element emailContentElement = emailDocument.createElement("email_content");
                emailDocument.appendChild(emailContentElement);
            
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                
              //#18485 - For Partner Orders, stock messages should have Partner_order_number instead of external_order_number
                if(order.isPartnerOrder()){
                	handlePartnerOrder(order,dataRequest.getConnection());
                }

                // Filter item for email content
                this.filterItem(order, itemNumber, cartFlag);
                
                // Convert order to xml and append to response document
                OrderXAO orderXAO = new OrderXAO();
                Document orderDocument = orderXAO.generateOrderXML(order);
                
                //retrieve the Amazon order number and attach this to the xml document
                //this is used to send to the customer in an email
                dataRequest.reset();
                dataRequest.setStatementID("VIEW_AZ_ORDER");
                dataRequest.addInputParam("master order number", order.getMasterOrderNumber());
                CachedResultSet az_orderResults = (CachedResultSet)DataAccessUtil.getInstance().execute(dataRequest);
                
                //Append the Amazon order number
                DOMUtil.addSection(orderDocument, generateAmazonOrderNumberDataElement(orderDocument,az_orderResults ));
                
                // Append additional company data
                dataRequest.reset();
                dataRequest.setStatementID("GET_EMAIL_COMPANY_DATA");
                dataRequest.addInputParam("company_id", order.getCompanyId());
                CachedResultSet companyDataResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                DOMUtil.addSection(orderDocument, this.generateCompanyDataElement(companyDataResults, orderDocument));
                    
                // Append Program Data
                DOMUtil.addSection(orderDocument, this.generateProgramMasterElement(orderDocument));

                // Append email data
                DOMUtil.addSection(orderDocument, "email_data", "data", this.loadEmailData(order, dataRequest, request), true);

                // Email subject and body
                dataRequest.reset();
                dataRequest.setStatementID("STOCK_MESG_BY_ID");
                dataRequest.addInputParam("message_id", request.getParameter("message_id"));
                CachedResultSet messageResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);           

                // Pull XSLs
                String messageSubjectXsl = "";
                String messageBodyXsl = "";

                if(messageResults != null && messageResults.getRowCount() > 0)
                {
                    while(messageResults.next())
                    {
                        messageSubjectXsl = (String) messageResults.getObject(3);
                        messageBodyXsl += (String) messageResults.getObject(5);
                     }
                }

                // Transform subject and body
                TraxUtil traxUtil = TraxUtil.getInstance();
                String messageSubjectText = null;
                String messageBodyText = null;

                try
                {
                    messageSubjectText = traxUtil.transform(orderDocument, messageSubjectXsl, null).trim();
                }
                catch(Exception e)
                {
                    messageSubjectText = "Subject Parse Error";
                }

                try
                {
                    messageBodyText = traxUtil.transform(orderDocument, messageBodyXsl, null).trim();
                }
                catch(Exception e)
                {
                    messageBodyText = "Body Parse Error";
                }

                Element emailSubjectElement = emailDocument.createElement("email_subject");
                emailSubjectElement.appendChild(emailDocument.createTextNode(messageSubjectText));
                emailContentElement.appendChild(emailSubjectElement);
                
                Element emailBodyElement = emailDocument.createElement("email_body");
                emailBodyElement.appendChild(emailDocument.createTextNode(messageBodyText));
                emailContentElement.appendChild(emailBodyElement);

                File xslFile = new File(getServletContext().getRealPath(XSL_EMAIL_CONTENT));
                TraxUtil.getInstance().transform(request, response, emailDocument, xslFile, XSL_EMAIL_CONTENT, null);
            }
            catch(Exception e)
            {
                logger.error(e);
            }
            finally
            {
                try 
                {
                    if(dataRequest != null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            try
            {
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");
                String cartFlag = request.getParameter("cart_flag");
            
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
            
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                
                //#18485 - For Partner Orders, stock messages should have Partner_order_number instead of external_order_number
                if(order.isPartnerOrder()){
                	handlePartnerOrder(order,dataRequest.getConnection());
                }

                // Filter item for email content
                this.filterItem(order, itemNumber, cartFlag);

                // Convert order to xml and append to response document
                OrderXAO orderXAO = new OrderXAO();
                Document orderDocument = orderXAO.generateOrderXML(order);

                // Append additional company data
                dataRequest.reset();
                dataRequest.setStatementID("GET_EMAIL_COMPANY_DATA");
                dataRequest.addInputParam("company_id", request.getParameter("company_id"));
                CachedResultSet companyDataResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                DOMUtil.addSection(orderDocument, this.generateCompanyDataElement(companyDataResults, orderDocument));
                    
                // Append Program Data
                DOMUtil.addSection(orderDocument, this.generateProgramMasterElement(orderDocument));                

                // Append email data
                DOMUtil.addSection(orderDocument, "email_data", "data", this.loadEmailData(order, dataRequest, request), true);

                // Email descriptions
                dataRequest.reset();
                dataRequest.setStatementID("STOCK_MESG_BY_COMPANY");
                dataRequest.addInputParam("origin_id", order.getOrderOrigin());
                String partnerName = ((OrderDetailsVO)order.getOrderDetail().get(0)).getPreferredProcessingPartner();
                dataRequest.addInputParam("partner_name", partnerName);
                CachedResultSet subjectResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                DOMUtil.addSection(responseDocument,  this.generateDescriptionElement(subjectResults, orderDocument, responseDocument));
                
                // Load canned statements                
                String cannedStatements = TraxUtil.getInstance().transform(orderDocument, this.loadCannedStatementsXsl(order, dataRequest), null);
                DOMUtil.addSection(responseDocument, DOMUtil.getDocument(cannedStatements).getChildNodes());                     

                 // Add page data to xml
                HashMap pageData = new HashMap();
                pageData.put("order_guid", orderGuid);
                pageData.put("item_number", itemNumber);
                pageData.put("cart_flag", cartFlag);
                pageData.put("master_order_number", request.getParameter("master_order_number"));
                pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
                pageData.put("item_order_number", request.getParameter("item_order_number"));
                pageData.put("company_id", request.getParameter("company_id"));
                pageData.put("calling_servlet", request.getParameter("calling_servlet"));
                pageData.put("disposition", request.getParameter("disposition"));
                pageData.put("called_flag", request.getParameter("called_flag"));
                pageData.put("email_flag", request.getParameter("email_flag"));
                pageData.put("comments", request.getParameter("comments"));
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                // Add security data to xml
                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

                File xslFile = new File(getServletContext().getRealPath(XSL_EMAIL));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_EMAIL, null);
            }
            catch(Exception e)
            {
                logger.error(e);
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
    }

    private void submitEmail(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        
        String action = request.getParameter("servlet_action");

        if(action != null && action.equals("close"))
        {
            try
            {
                // Load return page
                String loadCloseURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/servlet/" + request.getParameter("calling_servlet");
                loadCloseURL +=  "?master_order_number=" + request.getParameter("master_order_number");
                loadCloseURL +=  "&order_guid=" + request.getParameter("order_guid");
                loadCloseURL +=  "&item_number=" + request.getParameter("item_number");
                loadCloseURL +=  "&buyer_full_name=" + request.getParameter("buyer_full_name");
                loadCloseURL +=  "&item_order_number=" + request.getParameter("item_order_number");
                loadCloseURL +=  "&company_id=" + request.getParameter("company_id");
                loadCloseURL +=  "&calling_servlet=" + request.getParameter("calling_servlet");
                loadCloseURL +=  "&cart_flag=" + request.getParameter("cart_flag");
                loadCloseURL +=  "&disposition=" + request.getParameter("disposition");
                loadCloseURL +=  "&called_flag=" + request.getParameter("called_flag");
                loadCloseURL +=  "&email_flag=" + request.getParameter("email_flag");
                loadCloseURL +=  "&comments=" + URLEncoder.encode(request.getParameter("comments"));
                loadCloseURL +=  "&context=" + request.getParameter("context");
                loadCloseURL +=  "&securitytoken=" + request.getParameter("securitytoken");
                
                response.sendRedirect(loadCloseURL);
            }
            catch(Exception e)
            {
                logger.error(e);
            }
        }
        else
        {
            try
            {
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");
                String cartFlag = request.getParameter("cart_flag");
                
                // Create the initial document
                Document responseDocument = (Document) DOMUtil.getDocument();
            
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
        
                HashMap pageData = new HashMap();
                HashMap errorMessages = new HashMap();

                String messageId = request.getParameter("message_id");
                String emailSubject = request.getParameter("email_subject");
                String emailBody = request.getParameter("email_body");
                
                logger.info("cartFlag: " + cartFlag);
                String externalOrderNumber = null;
                List detailList = order.getOrderDetail();
                if (detailList != null && detailList.size() == 1) {
                	OrderDetailsVO odVO = (OrderDetailsVO) detailList.get(0);
                	externalOrderNumber = odVO.getExternalOrderNumber();
                } else if (cartFlag != null && cartFlag.equalsIgnoreCase("N")) {
                    String itemOrderNumber = request.getParameter("item_order_number");
                    externalOrderNumber = itemOrderNumber;
                }
                logger.info("externalOrderNumber: " + externalOrderNumber);
                
                // Load data for display
                pageData.put("cart_flag", cartFlag);
                pageData.put("order_guid", orderGuid);
                pageData.put("item_number", itemNumber);
                pageData.put("calling_servlet", request.getParameter("calling_servlet"));
                pageData.put("master_order_number", request.getParameter("master_order_number"));
                pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
                pageData.put("item_order_number", request.getParameter("item_order_number"));                 
                pageData.put("company_id", order.getCompanyId());logger.debug("EmailServlet###comp id###"+order.getCompanyId());
                pageData.put("disposition", request.getParameter("disposition"));
                pageData.put("called_flag", request.getParameter("called_flag"));
                pageData.put("email_flag", request.getParameter("email_flag"));
                pageData.put("comments", request.getParameter("comments"));
                pageData.put("message_id", messageId);
                pageData.put("email_subject", emailSubject);
                pageData.put("email_body", emailBody);

                // Add security data to xml
                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

                // Load validation messages and status
                if(messageId == null || messageId.trim().equals(""))
                {
                    errorMessages.put("message_id_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.EMAIL_MSG_TYPE_ERROR));  
                }
                if(emailSubject == null || emailSubject.trim().equals(""))
                {
                    errorMessages.put("email_subject_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.EMAIL_SUBJECT_ERROR));  
                }
                if(emailBody == null || emailBody.trim().equals(""))
                {
                    errorMessages.put("email_body_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.EMAIL_BODY_ERROR));  
                }

                // Send email
                if(errorMessages.size() == 0)
                {
                    try
                    {   
                        OrderHelper orderHelper = new OrderHelper();
                        String fromAddress = null;

                        // Load from address from email data
                        dataRequest.reset();
                        dataRequest.setStatementID("GET_EMAIL_COMPANY_DATA");
                        dataRequest.addInputParam("company_id", order.getCompanyId());
						logger.debug("###company_id: " + order.getCompanyId());
						logger.debug("###company_id: " + order.getCompanyId());
                        CachedResultSet companyDataResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

                        if ((order.getPreferredProcessingPartners() != null) && (order.getPreferredProcessingPartners().size() > 0))
                        {
                            String partnerName = order.getPreferredProcessingPartners().iterator().next();
                            fromAddress = FTDCommonUtils.getPreferredPartnerReplyEmail(partnerName);
                        }
						logger.debug("###fromAddress: " + fromAddress);
						logger.info("###fromAddress: " + fromAddress);
                        if (fromAddress == null)
                        {
							logger.debug("###fromAddress was null");
							logger.info("###fromAddress was null");
                            if(companyDataResults != null && companyDataResults.getRowCount() > 0)
                            {          
								logger.debug("###companyDataResults not null");
								logger.info("###companyDataResults not null");
                                String dataName = null;
                                while(companyDataResults.next())
                                {
                                    dataName = (String) companyDataResults.getObject(1);
									logger.debug("###dataName: " +dataName);
									logger.info("###dataName: " +dataName);
                                    if(dataName != null && dataName.equals(EMAIL_FROM_ADDRESS))
                                    {
                                        fromAddress = (String) companyDataResults.getObject(2);
										logger.info("###if fromAddress: " +fromAddress);
										logger.debug("###if fromAddress: " +fromAddress);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                fromAddress = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.EMAIL_FROM_ADDRESS);
								logger.info("###else fromAddress: " + fromAddress);
								logger.debug("###else fromAddress: " + fromAddress);
                            }
                        }

                        this.sendEmail(orderHelper.getPrimaryBuyerEmail(order), fromAddress, emailSubject, emailBody, order.getGUID(), dataRequest.getConnection(), order, externalOrderNumber);
                    }
                    catch(Exception e)
                    {
                        logger.error(e);
                        errorMessages.put("email_send_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.EMAIL_SEND_ERROR));
                    }
                }

                if(errorMessages.size() > 0)
                {
                    pageData.putAll(errorMessages);

                    // Filter item for email content
                    this.filterItem(order, itemNumber, cartFlag);

                    // Convert order to xml and append to response document
                    OrderXAO orderXAO = new OrderXAO();
                    Document orderDocument = orderXAO.generateOrderXML(order);

                    // Append additional company data
                    dataRequest.reset();
                    dataRequest.setStatementID("GET_EMAIL_COMPANY_DATA");
                    dataRequest.addInputParam("company_id", request.getParameter("company_id"));
                    CachedResultSet companyDataResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                    DOMUtil.addSection(orderDocument, this.generateCompanyDataElement(companyDataResults, orderDocument));
                    
                    // Append Program Data
                    DOMUtil.addSection(orderDocument, this.generateProgramMasterElement(orderDocument));

                    // Append email data
                    DOMUtil.addSection(orderDocument, "email_data", "data", this.loadEmailData(order, dataRequest, request), true);
                    
                    // Load canned statements
                    String cannedStatements = TraxUtil.getInstance().transform(orderDocument, this.loadCannedStatementsXsl(order, dataRequest), null);
                    DOMUtil.addSection(responseDocument, DOMUtil.getDocument(cannedStatements).getChildNodes());
                    
                    // Email descriptions
                    dataRequest.reset();
                    dataRequest.setStatementID("STOCK_MESG_BY_COMPANY");
                    CachedResultSet subjectResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                    DOMUtil.addSection(responseDocument,  this.generateDescriptionElement(subjectResults, orderDocument, responseDocument));
                
                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_EMAIL));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_EMAIL, null);
                }
                else
                {
                    // Load pending page
                    pageData.put("validation_flag", "Y");

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_EMAIL));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_EMAIL, null);
                }
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
    }

    private void filterItem(OrderVO order, String lineNumber, String cartFlag)
    {
        List singleItemList = new ArrayList();
    
        if(cartFlag != null && cartFlag.equals("Y"))
        {
            OrderDetailsVO item = null;
            boolean firstItemFound = false;

            for(int i = 0; !firstItemFound; i++)
            {
                for(int x = 0; x < order.getOrderDetail().size(); x++)
                {
                    item = (OrderDetailsVO) order.getOrderDetail().get(x);
                    if(item.getLineNumber() != null && item.getLineNumber().equals(new Integer(i).toString()))
                    {
                        singleItemList.add(item);
                        firstItemFound = true;
                        break;
                    }
                }
            }
        }
        else
        {
            OrderDetailsVO item = null;
            for(int i = 0; i < order.getOrderDetail().size(); i++)
            {
                item = (OrderDetailsVO) order.getOrderDetail().get(i);
                if(item.getLineNumber() != null && item.getLineNumber().equals(lineNumber))
                {
                    singleItemList.add(item);
                    break;
                }
            }
        }

        order.setOrderDetail(singleItemList);
    }

    private void sendEmail(String toAddress, String fromAddress, String subject, String content, String orderguid, Connection connection, OrderVO order, String externalOrderNumber) throws Exception
    {
        EmailVO email = new EmailVO();
        email.setGuid(GUIDGenerator.getInstance().getGUID());
        email.setRecipient(toAddress);
        email.setSender(fromAddress);
        email.setSubject(subject);
        email.appendContent(content);

        logger.debug("Generating scrub email " + email.getGuid() + " for order " + orderguid +
        		" / " + externalOrderNumber);

        OrderEmailGeneratorBO emailBuilder = new OrderEmailGeneratorBO(connection);
        emailBuilder.generateScrubEmail(email, order, externalOrderNumber);
    }

    private HashMap loadEmailData(OrderVO order, DataRequest dataRequest, HttpServletRequest request) throws Exception
    {
        HashMap emailData = new HashMap();

        // Calculate and pass canadian price to email
        BigDecimal canadianPrice = new BigDecimal(0);
                    
        dataRequest.reset();   
        dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP");
        CachedResultSet globalParmsResult = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
        if(globalParmsResult != null && globalParmsResult.getRowCount() > 0)
        {
            globalParmsResult.next();
            BigDecimal canadaExchangeRate = (BigDecimal) globalParmsResult.getObject(20);

            if(canadaExchangeRate != null)
            {
                OrderDetailsVO item = (OrderDetailsVO) order.getOrderDetail().get(0);
                BigDecimal productAmount = new BigDecimal(item.getProductsAmount());
                canadianPrice = productAmount.multiply(canadaExchangeRate);
            }
        }

        emailData.put("canadian_price", canadianPrice.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());

        // Pass csr data to email
        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);

        if(standAloneFlag != null && standAloneFlag.equals("N"))
        {
            String securityToken = request.getParameter("securitytoken");
            SecurityManager securityManager = SecurityManager.getInstance();
            UserInfo userInfo = securityManager.getUserInfo(securityToken);

            emailData.put("csr_firstname", userInfo.getFirstName());
            emailData.put("csr_lastname", userInfo.getLastName());
        }
        
        return emailData;
    }

    private String loadCannedStatementsXsl(OrderVO order, DataRequest dataRequest) throws Exception
    {
        // Load canned statements
        String cannedStatementsXsl = null;
                
        dataRequest.reset();
        dataRequest.setStatementID("STOCK_MESG_BY_ID");
        dataRequest.addInputParam("message_id", "Default");
        CachedResultSet messageResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);           

        if(messageResults != null && messageResults.getRowCount() > 0)
        {
            while(messageResults.next())
            {
                cannedStatementsXsl = (String) messageResults.getObject(5);
            }
        }

        return cannedStatementsXsl;   
    }

    private NodeList generateDescriptionElement(CachedResultSet subjectResults, Document orderDocument, Document responseDocument) throws Exception
    {
        TraxUtil traxUtil = TraxUtil.getInstance();
        
        Element messageDescRoot = responseDocument.createElement("messageDescRoot");
        Element messageDescListElement = responseDocument.createElement("messageDescList");
        messageDescRoot.appendChild(messageDescListElement);
        
        if(subjectResults != null && subjectResults.getRowCount() > 0)
        {
            String messageSubjectXsl = null;
            String messageSubjectText = null;

            Element messageDescElement = null;
            Element messageIdElement = null;
            Element descriptionElement = null;
                    
            while(subjectResults.next())
            {
                try
                {
                    messageSubjectXsl = (String) subjectResults.getObject(2);
                    messageSubjectText = traxUtil.transform(orderDocument, messageSubjectXsl, null);
                }
                catch(Exception e)
                {
                    messageSubjectText = "Description Parse Error";
                }
                
                messageDescElement = responseDocument.createElement("messageDesc");
                messageIdElement = responseDocument.createElement("stock_message_id");
                descriptionElement = responseDocument.createElement("description");

                messageIdElement.appendChild(responseDocument.createTextNode((String) subjectResults.getObject(1)));
                descriptionElement.appendChild(responseDocument.createTextNode(messageSubjectText));

                messageDescElement.appendChild(messageIdElement);
                messageDescElement.appendChild(descriptionElement);
                messageDescListElement.appendChild(messageDescElement);
            }
        }

        return messageDescRoot.getChildNodes();
    }

    private NodeList generateCompanyDataElement(CachedResultSet companyDataResults, Document orderDocument) throws Exception
    {
        TraxUtil traxUtil = TraxUtil.getInstance();
        
        Element companyDataRoot = orderDocument.createElement("company_data_root");
        Element companyDataListElement = orderDocument.createElement("company_data_list");
        companyDataRoot.appendChild(companyDataListElement);
        
        if(companyDataResults != null && companyDataResults.getRowCount() > 0)
        {
            Element companyDataElement = null;
            Element dataNameElement = null;
            Element dataElement = null;
                    
            while(companyDataResults.next())
            {
                companyDataElement = orderDocument.createElement("company_data");
                dataNameElement = orderDocument.createElement("data_name");
                dataElement = orderDocument.createElement("data");

                dataNameElement.appendChild(orderDocument.createTextNode((String) companyDataResults.getObject(1)));
                dataElement.appendChild(orderDocument.createTextNode((String) companyDataResults.getObject(2)));

                companyDataElement.appendChild(dataNameElement);
                companyDataElement.appendChild(dataElement);
                companyDataListElement.appendChild(companyDataElement);
            }
        }

        return companyDataRoot.getChildNodes();
    }

  //Adds the Amazon Order number to the XML.
  //Some of the stock messages take advantage of this order number being on the xml.
  private NodeList generateAmazonOrderNumberDataElement( Document orderDocument, CachedResultSet results )
  {
      TraxUtil traxUtil = TraxUtil.getInstance();
      
      Element az_orderNumberDataRoot = orderDocument.createElement("amazon_data_root");
      Element az_orderNumberDataList = orderDocument.createElement("amazon_data_list");
      az_orderNumberDataRoot.appendChild(az_orderNumberDataList);
  
      if (results != null && results.getRowCount() > 0 )
      {
         Element amazonDataElement = null;
         amazonDataElement = orderDocument.createElement("amazon_data");
         
          while( results.next() )
          {
            amazonDataElement.appendChild(orderDocument.createTextNode( (String) results.getObject(1)));
          }
          
          az_orderNumberDataList.appendChild(amazonDataElement);
          
      }
      
      return az_orderNumberDataRoot.getChildNodes(); 
  }


  /**
   * Adds information from Account.Program_Master to the Order document. 
   * @param orderDocument
   * @return
   * @throws Exception
   */
  private NodeList generateProgramMasterElement(Document orderDocument) throws Exception
  {
    logger.debug("Starting generateProgramMasterElement");
    
    Element programMasterRoot = orderDocument.createElement("accountprogrammaster_root");
    Element programMasterListElement = orderDocument.createElement("accountprogrammaster_list");
    programMasterRoot.appendChild(programMasterListElement);
    
    CacheUtil cacheUtil = CacheUtil.getInstance();
    List<AccountProgramMasterVO> accountProgramMasterList = null;
    AccountProgramMasterVO accountProgramMasterVO = accountProgramMasterVO = cacheUtil.getFSAccountProgramDetailsFromContent();
    



    logger.debug("Completed generateProgramMasterElement");
    return programMasterRoot.getChildNodes();
  }
  
  private void handlePartnerOrder(OrderVO order, Connection conn) {
	  for(OrderDetailsVO ord : (List<OrderDetailsVO>)order.getOrderDetail()){
		  CachedResultSet result = new PartnerUtility()
		  .getPtnOrderDetailByConfNumber(ord.getExternalOrderNumber(), order.getOrderOrigin(), conn);
		  if (result != null && result.next()) {
			  if (!StringUtils.isEmpty(result.getString("PARTNER_ORDER_ITEM_NUMBER"))) {
				  ord.setExternalOrderNumber(result.getString("PARTNER_ORDER_ITEM_NUMBER"));
			  }
		  }
	  }

  }
}