package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.ftdutilities.SympathyControls;
import com.ftd.osp.orderscrub.business.ServiceOrderActionBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.orderscrub.util.OrderSaver;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.OrderXAO;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


/**
 * This servlet is used to save the selected product & addons to the order.
 * The serlvet is invoked after a product has been selected.
 */

public class EditProductServlet extends HttpServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.EditProductServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";

    private final static int COLOR_CODE = 1;
    private static final int PRODUCT_MASTER_PRODUCT_TYPE =8;
    

    /** 
     * Servlet init
     * 
     * @param SerlvetConfig config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Servlet get...currently not used
     * 
     * @param HttpServletRequest request
     * @param HttpServletResponse response
     * @throws ServletException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {    
    }

    /**
     * Servlet Post.  
     * @param HttpServletRequest request
     * @param HttpServletResponse response
     * @throws ServletException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        if (SecurityHelper.isValidToken(request, response)) 
        {
            this.submitProduct(request, response);
        }
    }

    /*
     * This is the main method which controsl the saving of the product to the 
     * order.
     * @param HttpServletRequest request
     * @param HttpServletResponse response
     */
    private void submitProduct(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null; 
        //boolean isMercentOrder = false;
        boolean isPartnerOrder = false; 
        PartnerMappingVO partnerMappingVO = new PartnerMappingVO();
        String errorPopupMessage = null;
        OrderHelper orderHelper = new OrderHelper();
        boolean hasMilitaryStarPayment = false;
        
        try
        {
            // Load order guid from scrub for processing
            String orderGuid = request.getParameter("order_guid");
            String lineNumber = request.getParameter("item_number");
            
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
        
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            //SN: Changed the position of this.
            ServiceOrderActionBO serviceOrderActionBO = new ServiceOrderActionBO();
            
            // Load complete order from dao
            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
            OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);

            // Clone the order.  Use the clone for comparisons and replacement.
            OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
            MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
            // convert line values to numerics
            OrderDetailsVO originalLine = this.getItem(orderBeforeUpdate, 
                                                       lineNumber);
/*            if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
            	isMercentOrder = true;
            } else {*/
        	partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(), order.getSourceCode(), dataRequest.getConnection());
        	if(partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour())  { 
        		isPartnerOrder = true;            		 
        	}            	
            //}
            
            // Update order with form values
            OrderBuilder orderBuilder = new OrderBuilder();
            orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());

            // Update item information with new product data
            OrderDetailsVO line = this.updateProduct(order, dataRequest, request);
            
            String personalGreetingFlag = request.getParameter("personal_greeting_flag");
            if (!this.isSameProduct(orderBeforeUpdate, order, lineNumber) && line.getPersonalGreetingId() != null 
                        && (personalGreetingFlag == null || !personalGreetingFlag.equalsIgnoreCase("Y"))) {
                line.setPersonalGreetingId(null);
                logger.debug("Personal Greeting Id removed");
            }
            
            OrderLoader orderLoader = new OrderLoader();

            // AAFES orders and Ariba orders cannot change amounts to be greater.
            // 11/27/2006 by chu. Defect 2297, 2548.
            // 03/08/2007 tschmig Defect 3234 Removed Ariba restrictions
            hasMilitaryStarPayment = orderHelper.hasMilitaryStarPayment(order);
            if(hasMilitaryStarPayment) {
                new RecalculateOrderBO().recalculate(dataRequest.getConnection(), order);
                BigDecimal origTotal = new BigDecimal(orderBeforeUpdate.getOrderTotal());
                BigDecimal newTotal = new BigDecimal(order.getOrderTotal());
                
                if(newTotal.compareTo(origTotal) > 0) {
                    //restore original order
                    order = orderBeforeUpdate;
                    line = originalLine;
                    
                    errorPopupMessage = 
                        ConfigurationUtil.getInstance().getProperty(
                            CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                            CustomMessageConstants.EDIT_AAFES_ORDER_AMOUNT_INCREASE);
                }
            }
            // Amazon Order specific logic
 /*           if(isAmazonOrder(order)) 
            {
                    OrderSaver orderSaver = 
                        new OrderSaver(dataRequest.getConnection());
                    
                        try
                        {
                            new RecalculateOrderBO().recalculate(dataRequest.getConnection(), order);
                            
                            //save order to DB
                            orderSaver = new OrderSaver(dataRequest.getConnection());
                            orderSaver.saveOrder(order, line, false, request);
                
                            // Reload complete order from dao...
                            //the order is reload because the scrub dao will return 
                            //additionial info
                            scrubMapperDAO = 
                                        new ScrubMapperDAO(dataRequest.getConnection());
                            order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                        }//end try
                        catch(Exception e)
                        {
                          // lpuckett 06/28/2006: Add throw to figure out what's broken in RecalculateOrderBO
                          try
                          {
                            StringWriter stringWriter = new StringWriter();
                            e.printStackTrace(new PrintWriter(stringWriter));
                            String error = "Recalculate Order Exception for order guid "+order.getGUID() +" : " + stringWriter.toString();
                            logger.error(error);
                            SystemMessengerVO smVO = new SystemMessengerVO();
                            smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                            smVO.setType("ERROR");
                            smVO.setSource("ORDER SCRUB");
                            smVO.setMessage(error);
                            SystemMessenger.getInstance().send(smVO, dataRequest.getConnection(), false);
                          }
                          catch(Exception ex)
                          {
                            logger.error("Error sending System Message.", ex);
                          }
                         }//end catch
    
                        //get original order values from az_order_detail
                        Map azOrderDetails = getAmazonOrderDetails(originalLine.getExternalOrderNumber());
                                                
                        double originalPrincipal = 
                            (azOrderDetails.get("principalAmt") == null) ? 0.0
                                : ((BigDecimal) azOrderDetails.get("principalAmt")).doubleValue();
                        double updatedPrincipal = 
                            (line.getProductsAmount() == null) ? 0.0 
                                : Double.parseDouble(line.getProductsAmount());
                        double originalTax = 
                        	(azOrderDetails.get("taxAmt") == null) ? 0.0
                                    : ((BigDecimal) azOrderDetails.get("taxAmt")).doubleValue();
                        double updatedTax = 
                            (line.getTaxAmount() == null) ? 0.0 
                                : Double.parseDouble(line.getTaxAmount());
                                
                        double originalShipping = 0.0;
                        double updatedShipping = 0.0;
                                                        
                        originalShipping = 
                      	  (azOrderDetails.get("shippingAmt") == null) ? 0.0
                                    : ((BigDecimal) azOrderDetails.get("shippingAmt")).doubleValue();
                        //updatedShipping = lineShippingFee + lineServiceFee
                        updatedShipping = ((line.getShippingFeeAmount() == null) ? 0.0 : Double.parseDouble(line.getShippingFeeAmount())) + 
                        		((line.getServiceFeeAmount() == null) ? 0.0 : Double.parseDouble(line.getServiceFeeAmount()));
                        
                        double originalShippingTax = 
                        	(azOrderDetails.get("shippingTaxAmt") == null) ? 0.0
                                    : ((BigDecimal) azOrderDetails.get("shippingTaxAmt")).doubleValue();
                        double updatedShippingTax = 
                            (line.getShippingTax() == null) ? 0.0
                                : Double.parseDouble(line.getShippingTax());
                        
                        double originalAddOnAmount = 
                            (originalLine.getAddOnAmount() == null) ? 0.0
                                : Double.parseDouble(originalLine.getAddOnAmount());
                        double updatedAddOnAmount = 
                            (line.getAddOnAmount() == null) ? 0.0
                                : Double.parseDouble(line.getAddOnAmount());
                        
                        double originalSum = originalPrincipal 
                                             + originalTax 
                                             + originalShipping 
                                             + originalShippingTax
                                             + originalAddOnAmount;
                        double updatedSum = updatedPrincipal 
                                            + updatedTax 
                                            + updatedShipping 
                                            + updatedShippingTax
                                            + updatedAddOnAmount;
                        
                        boolean isGoodOrder = true;
                        if(originalPrincipal < updatedPrincipal){
                        	isGoodOrder = false;
                        	errorPopupMessage = ConfigurationUtil.getInstance().getProperty(
                                    CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                                    CustomMessageConstants.
                                    EDIT_AMAZON_PRODUCT_AMOUNT_INCREASE);
                        }
                        else if(originalShipping < updatedShipping){
                        	isGoodOrder = false;
                        	errorPopupMessage = ConfigurationUtil.getInstance().getProperty(
                                    CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                                    CustomMessageConstants.
                                    EDIT_AMAZON_SHIPPING_AMOUNT_INCREASE);
                        }
                        else if((originalTax+originalShippingTax) < (updatedTax+updatedShippingTax)){
                        	isGoodOrder = false;
                        	errorPopupMessage = ConfigurationUtil.getInstance().getProperty(
                                    CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                                    CustomMessageConstants.
                                    EDIT_AMAZON_TAX_AMOUNT_INCREASE);
                        }

                        // if original is less than new total, set error message
                        else if (originalSum < updatedSum)
                        {
                        	isGoodOrder = false;
                            errorPopupMessage = 
                                    ConfigurationUtil.getInstance().getProperty(
                                    CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                                    CustomMessageConstants.
                                        CANCEL_ORDER_ERROR_MESSAGE_PRICE_DIFF);
                        }
                        
                        if(!isGoodOrder)
                        {
                            //restore original order
                            order = orderBeforeUpdate;
                            line = originalLine;
                            order.setChangeFlags(true);
                            //save order to DB
                            orderSaver = 
                                    new OrderSaver(dataRequest.getConnection());
                            orderSaver.saveOrder(order, 
                                                 line, 
                                                 false, 
                                                 request);
                
                            // Reload complete order from dao...the order is 
                            // reload because the scrub dao will return 
                            // additionial info
                            scrubMapperDAO = 
                                new ScrubMapperDAO(dataRequest.getConnection());
                            order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                            
                        }//end if                  
                   
            }//end if (amazon order)
            else*/
            if(isPartnerOrder || (null!=partnerMappingVO && !partnerMappingVO.isIncrTotalAllowed())) {
              logger.debug("isIncrTotalAllowed flag:" + partnerMappingVO.isIncrTotalAllowed());
              OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
              try
                {
                   new RecalculateOrderBO().recalculate(dataRequest.getConnection(), order);
                   orderSaver = new OrderSaver(dataRequest.getConnection());
                   this.suppressAddOnDiscount(order, line);
                   orderSaver.saveOrder(order, line, false, request);
                   scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                   order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                }//end try
                catch(Exception e)
                {                       
                   try
                   {
                     StringWriter stringWriter = new StringWriter();
                     e.printStackTrace(new PrintWriter(stringWriter));
                     String error = "Recalculate Order Exception for order guid " + order.getGUID() + " : " + stringWriter.toString();
                     logger.error(error);
                     SystemMessengerVO smVO = new SystemMessengerVO();
                     smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                     smVO.setType("ERROR");
                     smVO.setSource("ORDER SCRUB");
                     smVO.setMessage(error);
                     SystemMessenger.getInstance().send(smVO, dataRequest.getConnection(), false);
                   }
                   catch(Exception ex)
                   {
                      logger.error("Error sending System Message.", ex);
                   }
                }//end catch
 
                //get original order values from mrcnt_order_detail
                String partnerName = "";
                Map orderDetailMap = null;
                boolean isGoodOrder = true;
                
                double originalSum = 0;
                double updatedSum = 0;
                double originalPrincipal = 0;
                double originalTax = 0;
                double originalShipping = 0.0;
                double updatedShipping = 0.0;
                double originalShippingTax  = 0;
                double updatedShippingTax = 0;
                double originalAddOnAmount = 0;
                double updatedAddOnAmount = 0;
                double updatedPrincipal = 0;
                double updatedTax = 0;
                
                /*if(isMercentOrder) {
                	orderDetailMap = getMercentOrderDetail(originalLine.getExternalOrderNumber());
                	partnerName = order.getMrcntChannelName();
                	
                } else*/ 
                
                if(isPartnerOrder) {
                	
                	orderDetailMap = getPartnerOrderDetail(originalLine.getExternalOrderNumber());
                	partnerName = order.getPartnerName();

					originalPrincipal = (orderDetailMap.get("principalAmt") == null) ? 0.0
							: ((BigDecimal) orderDetailMap.get("principalAmt")).doubleValue();

					originalTax = (orderDetailMap.get("taxAmt") == null) ? 0.0
							: ((BigDecimal) orderDetailMap.get("taxAmt")).doubleValue();

					originalShipping = (orderDetailMap.get("shippingAmt") == null) ? 0.0
							: ((BigDecimal) orderDetailMap.get("shippingAmt")).doubleValue();

					originalShippingTax = (orderDetailMap.get("shippingTaxAmt") == null) ? 0.0
							: ((BigDecimal) orderDetailMap.get("shippingTaxAmt")).doubleValue();

					originalAddOnAmount = (orderDetailMap.get("addonAmt") == null) ? 0.0
							: ((BigDecimal) orderDetailMap.get("addonAmt")).doubleValue();
                     
					originalSum = originalPrincipal + originalTax
							+ originalShipping + originalShippingTax + originalAddOnAmount; 
                   
                } else if (!partnerMappingVO.isIncrTotalAllowed()) {
                	originalSum = Double.parseDouble(originalLine.getOrigExternalOrderTotal());  
                	partnerName = partnerMappingVO.getPartnerName() == null ? "partner" : partnerMappingVO.getPartnerName();
                }
                
				
				if (line.getDiscountedProductPrice() != null && !isEmpty(line.getDiscountedProductPrice())) {
					updatedPrincipal = Double.parseDouble(line.getDiscountedProductPrice());
				} else {
					updatedPrincipal = (line.getProductsAmount() == null) ? 0.0 : Double.parseDouble(line.getProductsAmount());
				}
				
				updatedTax = (line.getTaxAmount() == null) ? 0.0 : Double.parseDouble(line.getTaxAmount());

				updatedShipping = ((line.getShippingFeeAmount() == null) ? 0.0 : Double.parseDouble(line.getShippingFeeAmount()))
						+ ((line.getServiceFeeAmount() == null) ? 0.0 : Double.parseDouble(line.getServiceFeeAmount()));

				updatedShippingTax = (line.getShippingTax() == null) ? 0.0 : Double.parseDouble(line.getShippingTax());

				if (line.getAddOnAmount() != null && line.getAddOnDiscountAmount() != null) {
					updatedAddOnAmount = Double.parseDouble(line.getAddOnAmount()) - Double.parseDouble(line.getAddOnDiscountAmount());
				} else {
					updatedAddOnAmount = (line.getAddOnAmount() == null) ? 0.0 : Double.parseDouble(line.getAddOnAmount());
				}

				updatedSum = updatedPrincipal + updatedTax + updatedShipping
						+ updatedShippingTax + updatedAddOnAmount;
                
				logger.debug("OrderNumber:"+line.getExternalOrderNumber()+"originalSum:"+originalSum+" updatedSum:"+updatedSum);
				 //FEPHII-6
                if((originalPrincipal + originalAddOnAmount < updatedPrincipal + updatedAddOnAmount) && isPartnerOrder) {
                	isGoodOrder = false;
                	errorPopupMessage = "This is an order from " + partnerName +". The Product and add-on amount cannot be increased from the original product and add-on amount. Please select another product and/or remove add-ons.";
                }
                else if(((originalTax + originalShippingTax) < (updatedTax + updatedShippingTax)) && isPartnerOrder) {
                	isGoodOrder = false;
                	errorPopupMessage = "This is an order from " +  partnerName + ". The tax amount cannot be increased from the original tax amount.";
                }
                // if original is less than new total, set error message                
                else if (originalSum < updatedSum ) {
					isGoodOrder = false;
					if(isPartnerOrder)
					    errorPopupMessage = "This is an order from " + partnerName+ ". The order total cannot be increased from the original total. Please select another product and/or remove add-ons.";
					else
						errorPopupMessage = "This is an Ariba order. The product and add-on amount cannot increase from the original product and add-on amount.";
                }                
                
				if (!isGoodOrder) {
					// restore original order
					order = orderBeforeUpdate;
					line = originalLine;
					order.setChangeFlags(true);
					// save order to DB
					orderSaver = new OrderSaver(dataRequest.getConnection());
					orderSaver.saveOrder(order, line, false, request);
					// Reload complete order from dao...the order is re load because the scrub dao will return additional info
					scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
					order = scrubMapperDAO.mapOrderFromDB(orderGuid);
				}// end if  
                     
                    
            }
            else // (!amazon order and !mercent order)
            {
                //save order to DB
                OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                orderSaver.saveOrder(order, line, false, request);
    
                // Reload complete order from dao...the order is reload because the scrub dao will return additionial info
                scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                order = scrubMapperDAO.mapOrderFromDB(orderGuid);
    
                // Check if Product has Changed, update the Status of Service/Programs
                // Do this prior to recalculation, so recalculation reflects the removal 
                // Of the Service/Program
                String csrId = getCsrID(request);                        
                serviceOrderActionBO.editProductTypeForServiceOrderItem(dataRequest.getConnection(), getItem(order, lineNumber), order.getBuyerEmailAddress(), csrId);

                // Do order recalculation - unless origin indicates it should be skipped.
                // This was introduced with Roses.com project to bypass recalculations in scrub.
                if (!orderLoader.checkSkipRecalcForOrigin(order.getOrderOrigin())) {                
                    try
                    {
                      new RecalculateOrderBO().recalculate(dataRequest.getConnection(), order);
                    }//end try
                    catch(Exception e)
                    {
                      try 
                      {
                        // lpuckett 06/28/2006: Add throw to figure out what's broken in RecalculateOrderBO
                        StringWriter stringWriter = new StringWriter();
                        e.printStackTrace(new PrintWriter(stringWriter));
                        String error = "Recalculate Order Exception for order guid "+order.getGUID() +" : " + stringWriter.toString();
                        logger.error(error);
                        SystemMessengerVO smVO = new SystemMessengerVO();
                        smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
                        smVO.setType("ERROR");
                        smVO.setSource("ORDER SCRUB");
                        smVO.setMessage(error);
                        SystemMessenger.getInstance().send(smVO, dataRequest.getConnection(), false);
                      }
                      catch (Exception ex)
                      {
                        logger.error("Error sending System Message.", ex);
                      }
                    }//end catch
                }
                
            }//end else (! amazon order)
            
            // Convert order to xml and append to response document
            OrderXAO orderXAO = new OrderXAO();
            DOMUtil.addSection(responseDocument, orderXAO.generateOrderXML(order).getChildNodes());

            // Validate order
            OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                    isAmazonOrder(order), isPartnerOrder, mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin()), 
                    RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));

            // Convert validation to xml and append to response document
            ValidationXAO validationXAO = new ValidationXAO();
            DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

            // Load base cart data
            BaseCartBuilder cartBuilder = new BaseCartBuilder();
            DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request,true));

            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

            HashMap pageData = new HashMap();
            pageData.put("_content", request.getParameter("_content"));
            pageData.put("_focus_object", request.getParameter("_focus_object"));
            pageData.put("edit_product_flag", "Y");
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            // Add error popup messages.
            HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
            if (errorPopupMessage != null) 
            {
                errorPopup.put("showErrorPopup", "Y");
                errorPopup.put("errorPopupMessage", errorPopupMessage);
            }//end if
            DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true);
            
            //Sympathy Controls
            SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
            HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
            if(sympathyAlerts!=null){
            	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
            }
            
            File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    private OrderDetailsVO updateProduct(OrderVO order, DataRequest dataRequest, HttpServletRequest request)
    throws Exception
    {
        String lineNumber = request.getParameter("item_number");
        OrderHelper orderHelper = new OrderHelper();

        OrderDetailsVO item = orderHelper.getItem(order, lineNumber);
       
        item.setProductId(request.getParameter("product_id"));
        item.setProductsAmount(request.getParameter("product_price"));
        //added pdb price to send updated product value to mercury
        item.setPdbPrice(item.getProductsAmount());
        item.setProductSubCodeId(request.getParameter("product_subcode_id"));

        item.setShipMethod(getDefaultShipMethod(item.getProductId(),item.getShipMethod()));
      
        //item.setProductsAmount("0");

        String color1 = request.getParameter("color_first_choice");
        if(color1 != null && color1.length() > 0){
            color1 = getColorCode(color1.toUpperCase());
        }
        item.setColorFirstChoice(color1);

        String color2 = request.getParameter("color_second_choice");
        if(color2 != null && color2.length() > 0){
            color2 = getColorCode(color2.toUpperCase());
        }
        item.setColorSecondChoice(color2);        

        item.setSizeChoice(request.getParameter("size_choice"));

        // Handle addons
        List addonList = new ArrayList();
        Enumeration reqParams = request.getParameterNames();
        while(reqParams.hasMoreElements()) {
            String curParam = (String) reqParams.nextElement();
            if (curParam != null) {

                if (curParam.equals("addonNew_optVase")) {

                    // Vase addon
                    String curVase = (String) request.getParameter(curParam);
                    Pattern regex = Pattern.compile("(.+)---(.+)");
                    Matcher m = regex.matcher(curVase);
                    if (m.find()) {
                        AddOnsVO addon = new AddOnsVO();
                        addon.setAddOnCode(m.group(1));
                        addon.setAddOnQuantity("1");
                        addon.setPrice(m.group(2));
                        addonList.add(addon);
                        logger.debug("Addon vase parameter/price: " + m.group(1) + "/" + m.group(2)); 
                    }

                } else if (curParam.equals("addonNew_optCard")) {

                    // Card addon
                    String curCard = (String) request.getParameter(curParam);
                    Pattern regex = Pattern.compile("(.+)---(.+)");
                    Matcher m = regex.matcher(curCard);
                    if (m.find() && !(m.group(1).equals("noCard"))) {
                        AddOnsVO addon = new AddOnsVO();
                        addon.setAddOnCode(m.group(1));
                        addon.setAddOnQuantity("1");
                        addon.setPrice(m.group(2));
                        addonList.add(addon);
                        logger.debug("Addon card parameter/price: " + m.group(1) + "/" + m.group(2)); 
                    }

                } else if ((curParam.indexOf("addonNew_") > -1) && (! curParam.endsWith("_quantity")) && (! curParam.endsWith("_price"))) {
                
                    // Normal addons
                    String curAddon = (String) request.getParameter(curParam);
                    String curQuantity = (String) request.getParameter(curParam + "_quantity");
                    String curPrice = (String) request.getParameter(curParam + "_price");
                    logger.debug("Addon parameter/quantity/price: " + curAddon + "/" + curQuantity + "/" + curPrice); 
                    try {
                        if(Integer.parseInt(curQuantity) <= 0) {
                          curQuantity = "1";
                        }
                    } catch(NumberFormatException nfe) {
                      curQuantity = "1";
                    }
                    AddOnsVO addon = new AddOnsVO();
                    addon.setAddOnCode(curAddon);
                    addon.setAddOnQuantity(curQuantity);
                    addon.setPrice(curPrice);
                    addonList.add(addon);
                }
            }
        }
        //place addons onto the order
        item.setAddOns(addonList);

        String secondChoiceAuth = request.getParameter("second_choice_auth");
        if(secondChoiceAuth != null && (secondChoiceAuth.equalsIgnoreCase("true") || secondChoiceAuth.equalsIgnoreCase("Y")))
        {
            secondChoiceAuth = "Y";
        }
        else
        {
            secondChoiceAuth = "N";
        }
        
        
        item.setSubstituteAcknowledgement(secondChoiceAuth);
        
        
        return item;

    }

    private boolean isEmpty(String value)
    {
      return ((value == null) || (value.trim().length() <= 0));
    }

    private String getColorCode(String colorDescription)
    throws  Exception
  {

      // Initialize data request
      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      String colorCode = "";

      try{
          dataRequest.reset();
          dataRequest.setStatementID("GET_COLOR_BY_DESCRIPTION");
          dataRequest.addInputParam("IN_DESCRIPTION",colorDescription);
          CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);                 

          if(rs == null || !rs.next())
          {
            String msg = "Color not found in database.[" + colorDescription + "]";
            logger.error(msg);
          }
          else
          {
            colorCode = rs.getObject(COLOR_CODE).toString();            
          }
      }
      finally
      {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }        
      }
      
      return colorCode;

  }

  private String getDefaultShipMethod(String productID, String oldShipMethod) throws Exception
  {

      // Initialize data request
      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

      String productType = "";
      String shipMethod = null;

      try{
          dataRequest.reset();
          dataRequest.setStatementID("GET_PRODUCT_BY_ID");
          dataRequest.addInputParam("IN_PRODUCT_ID",productID);
          CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);                 

          if(rs == null || !rs.next())
          {
            String msg = "Product id not found in database.[" + productID + "]";
            logger.error(msg);
          }
          else
          {
            productType = rs.getObject(PRODUCT_MASTER_PRODUCT_TYPE).toString();  

            ConfigurationUtil config = ConfigurationUtil.getInstance();

            //set default type based on product type
            if(productType == null)
              shipMethod = null;
            else if(productType.equalsIgnoreCase("FLORAL"))
              shipMethod = config.getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,ConfigurationConstants.FLORAL_SHIPMETHOD_DEFAULT);
            else if(productType.equalsIgnoreCase("SPEGFT"))
              shipMethod = config.getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,ConfigurationConstants.SPEGFT_SHIPMETHOD_DEFAULT);
            else if(productType.equalsIgnoreCase("FRECUT"))
              shipMethod = config.getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,ConfigurationConstants.FRECUT_SHIPMETHOD_DEFAULT);
            else if(productType.equalsIgnoreCase("NONE"))
              shipMethod = config.getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,ConfigurationConstants.NONE_SHIPMETHOD_DEFAULT);
            else if(productType.equalsIgnoreCase("SDG"))
              shipMethod = config.getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,ConfigurationConstants.SDG_SHIPMETHOD_DEFAULT);
            else if(productType.equalsIgnoreCase("SDFC"))
                // Defect #782 -  If the New Product is SDFC then retain the Ship Method of the Old Product.
            	shipMethod = oldShipMethod; 
            else 
              shipMethod = config.getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE,ConfigurationConstants.OTHER_SHIPMETHOD_DEFAULT);

            //check for null
            if(shipMethod != null && shipMethod.equalsIgnoreCase("NULL"))
            {
              shipMethod = null;
            }

            //If old shipmethod is a carrier type and new shipmethod is carrier type, then do not change ship method
            //Null and SD are the only methods which are NOT carrier ship methods
            if(oldShipMethod !=null && !oldShipMethod.equals("SD") && shipMethod !=null && !shipMethod.equals("SD"))
            {
              shipMethod = oldShipMethod;              
            }
            
          }
      }
      finally
      {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }        
      }
      
      return shipMethod;    
  }
  
  
    /*
     * Retrieves lineItem from the request object
     */
    private OrderDetailsVO getItem(OrderVO order, String lineNumber)
                                                            throws Exception
    {
        OrderHelper orderHelper = new OrderHelper();

        
        return orderHelper.getItem(order, lineNumber);
    }//end method getItem()
    
    
    /*
     * Determines if updated order lineItem is the same as the original order
     * lineItem.
     */
    private boolean isSameProduct(OrderVO originalOrder, 
                                  OrderVO updatedOrder, 
                                  String lineNumber) throws Exception
    {
        String originalItem = 
                        this.getItem(originalOrder, lineNumber).getProductId();
                            
        String updatedItem = 
                        this.getItem(updatedOrder, lineNumber).getProductId();
        
        if (updatedItem == null) 
        {
            throw new Exception("New item does not have a product ID.");
        }//end if
        
        return updatedItem.equalsIgnoreCase(originalItem);
    }//end method isSameProduct(OrderVO, OrderVO, HttpServletRequest)
    
    
    /*
     * Determine if the order originated from Amazon
     */
    private boolean isAmazonOrder(OrderVO order) throws Exception
    {
        String amazonOrigin = ConfigurationUtil.getInstance().getProperty(
                            ConfigurationConstants.SCRUB_CONFIG_FILE, 
                            ConfigurationConstants.AMAZON_ORIGIN_INTERNET);
                            
        return order.getOrderOrigin().equalsIgnoreCase(amazonOrigin);
    }//end method isAmazonOrder
    
   /**
   * Retrieves the CSR Id from the request
   * @param request
   * @return
   */
    private String getCsrID(HttpServletRequest request)
    {
        String csrId = "SYSTEM";
        
        try
        {
        String securityToken = request.getParameter("securitytoken");
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securityToken);      
        csrId = userInfo.getUserID();      
        } catch (Exception e)
        {
          // NOP 
        }
        
        return csrId;
    } 
   
    /**
     * Retrieves order details from az_order_detail
     * @param confirmationNumber
     * @return Map
     */
    private Map getAmazonOrderDetails(String confirmationNumber) throws Exception{
    	logger.debug("getAmazonOrderDetails()");

    	DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
        dataRequest.reset();
        dataRequest.setStatementID("GET_AMAZON_ORDER_DETAILS");
        dataRequest.addInputParam("IN_CONFIRMATION_NUMBER", confirmationNumber);
        DataAccessUtil dau = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        rs.reset();
        HashMap orderDetailsMap = new HashMap();
        // Only one record (or none) should return.
        if (rs.next()) {
        	orderDetailsMap.put("principalAmt", rs.getBigDecimal("PRINCIPAL_AMT"));
        	orderDetailsMap.put("shippingAmt", rs.getBigDecimal("SHIPPING_AMT"));
        	orderDetailsMap.put("taxAmt", rs.getBigDecimal("TAX_AMT"));
        	orderDetailsMap.put("shippingTaxAmt", rs.getBigDecimal("SHIPPING_TAX_AMT"));
        }
        
        return orderDetailsMap;
    }
    

	private Map<String, BigDecimal> getMercentOrderDetail(String confirmationNumber) throws Exception {

		logger.debug("getMercentOrderDetails()");
		DataRequest dataRequest = null;

		try {
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
			dataRequest.reset();
			dataRequest.setStatementID("GET_MERCENT_ORDER_DETAILS");
			dataRequest.addInputParam("IN_CONFIRMATION_NUMBER",
					confirmationNumber);
			DataAccessUtil dau = DataAccessUtil.getInstance();
			CachedResultSet rs = (CachedResultSet) dau.execute(dataRequest);
			rs.reset();
			Map<String, BigDecimal> orderDetailsMap = new HashMap<String, BigDecimal>();
			if (rs.next()) {
				orderDetailsMap.put("principalAmt", rs.getBigDecimal("PRINCIPAL_AMT"));
				orderDetailsMap.put("shippingAmt", rs.getBigDecimal("SHIPPING_AMT"));
				orderDetailsMap.put("taxAmt", rs.getBigDecimal("TAX_AMT"));
				orderDetailsMap.put("shippingTaxAmt", rs.getBigDecimal("SHIPPING_TAX_AMT"));
			}
			
			return orderDetailsMap;
			
		} catch (Exception e) {
			logger.error("Unable to get the mercent order details - getMercentOrderDetails");
		} finally {
			try {
				if (dataRequest != null && dataRequest.getConnection() != null) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException se) {
				logger.error(se);
			}
		}
		return null;
	}
	

	private Map<String, BigDecimal> getPartnerOrderDetail(String confirmationNumber) throws Exception {

		logger.debug("getPartnerOrderDetail()");
		DataRequest dataRequest = null;

		try {
			dataRequest = DataRequestHelper.getInstance().getDataRequest();
			dataRequest.reset();
			dataRequest.setStatementID("GET_PTN_ORD_ITEM_DETAIL");
			dataRequest.addInputParam("IN_CONFIRMATION_NUMBER",
					confirmationNumber);
			DataAccessUtil dau = DataAccessUtil.getInstance();
			CachedResultSet rs = (CachedResultSet) dau.execute(dataRequest);
			rs.reset();
			Map<String, BigDecimal> orderDetailsMap = new HashMap<String, BigDecimal>();
			if (rs.next()) {
				orderDetailsMap.put("principalAmt", rs.getBigDecimal("SALE_PRICE"));
				orderDetailsMap.put("addonAmt", rs.getBigDecimal("ADD_ON_SALE_PRICE"));
				orderDetailsMap.put("shippingAmt", rs.getBigDecimal("SHIPPING_AMT"));
				orderDetailsMap.put("taxAmt", rs.getBigDecimal("TAX_AMT"));
				//orderDetailsMap.put("shippingTaxAmt", rs.getBigDecimal("SHIPPING_TAX_AMT"));
			}
			
			return orderDetailsMap;
			
		} catch (Exception e) {
			logger.error("Unable to get the partner order detail - getMercentOrderDetails");
		} finally {
			try {
				if (dataRequest != null && dataRequest.getConnection() != null) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException se) {
				logger.error(se);
			}
		}
		return null;
	}
	
	private void suppressAddOnDiscount(OrderVO order, OrderDetailsVO lineItem) {
		
		if (order != null && lineItem != null) {
			if (lineItem.getAddOnDiscountAmount() != null) {
				BigDecimal orderAddOnDiscount = new BigDecimal(order.getAddOnDiscountAmount());
				BigDecimal itemAddOnDiscount = new BigDecimal(order.getAddOnDiscountAmount());
				
				if (orderAddOnDiscount != null && orderAddOnDiscount.compareTo(itemAddOnDiscount) > 0) {
					orderAddOnDiscount = orderAddOnDiscount.subtract(itemAddOnDiscount);
					order.setAddOnDiscountAmount(String.valueOf(orderAddOnDiscount));
				}
				
				lineItem.setAddOnDiscountAmount("0.00");
			}
		}
		
	}
	
}//end class EditProductServlet