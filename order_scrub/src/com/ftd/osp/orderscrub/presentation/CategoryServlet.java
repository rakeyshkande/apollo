package com.ftd.osp.orderscrub.presentation;

import com.ftd.osp.orderscrub.presentation.util.SearchUTIL;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.presentation.util.EditProductConstants;
import com.ftd.osp.orderscrub.vo.ProductFlagsVO;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.ParserConfigurationException;

/** This Servlet is used during the Edit Product process.  The servlet
 *  displays a list of categories\occasions if the recipient is domestic.
 *  If the recipient is internationial then this servlet will display
 *  a list of products available to that country 
 *  @author Ed Mueller
 *  
 *  Note: This object was copied from Order Entry. */
public class CategoryServlet extends HttpServlet 
{
    private static final String CATEGORY_XSL = ".../xsl/categories.xsl";
    private static final String PRODUCT_LIST_XSL = ".../xsl/productList.xsl";
    private static final String YES = "Y";
    private static final String NO = "N";

    ServletConfig config;
    Logger logger;

    /**
     * Servlet init
     * @param config ServletConfig
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        this.config = config;
        logger = new Logger("com.ftd.osp.orderscrub.presentation.CategoryServlet");        
    }

    /**
     * Servlet get
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //check if the logged user has been authenticated
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }

    /**
     * This method handles the processing for a servlet request.  The
     * SearchUtil object handles the majority of the work.
     * 
     * @HttpServletRequest request
     * @HttpServletResponse response
     * @throws ServletException
     */
    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
  
        SearchUTIL searchUtil = new SearchUTIL();
 
        response.setHeader("Cache-Control", "no-cache");    

        try
        {

          //determine if the passed in country is domestic or intl
           String country = request.getParameter("country");
           boolean domesticFlag = new SearchUTIL().isDomestic(country);  

           //based on country type set xml file
           String xslFileName = "";
           if(domesticFlag)
           {
             xslFileName = CATEGORY_XSL;
           }
           else
           {
             xslFileName = PRODUCT_LIST_XSL;
           }


            //get xml to be displayed
            Document xml = getCategoryXML(request, domesticFlag);

            //add security section to xml
            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(xml, "security", "data", securityData, true);

            //append addons
            xml = searchUtil.appendAddons(request,xml);

            //throw the output xml to the log
            logger.debug(searchUtil.convertDocToString(xml));

            //transform and display
            HashMap params = new HashMap();
            File xslFile = new File(getServletContext().getRealPath(xslFileName));
            TraxUtil.getInstance().transform(request, response, xml, xslFile, xslFileName, params);      
        }
    catch(Exception e)
    {
        try 
        {
            logger.error(e);
            request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
        }
        catch(Exception re) 
        {
            logger.error(re);
        }
    }
}


    /**
     * Servlet Post
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //check if the logged user has been authenticated
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }


    /* 
     * This method gets the xml to be displayed on the page. 
     * 
     * @param HttpServlet request
     * @param boolean domesticFlag 
     * @throws Exception
     * */
    private Document getCategoryXML(HttpServletRequest request,boolean domesticFlag) 
        throws ParserConfigurationException, Exception
  	{

        Document responseDocument = DOMUtil.getDocument();

        SearchUTIL searchUtil = new SearchUTIL();

        //Get all the parameters from the page.  Even though some of these
        //are not used in this servlet they must be retreived and passed
        //to the pages that follow.
        String categoryindex = request.getParameter("category_index");
        String pageNumber = request.getParameter("page_number");
        String sortType = request.getParameter("sort_type");
        String customerName = request.getParameter("buyer_full_name");
        String pricePointId = request.getParameter("price_point_id");     
        String scriptCode = request.getParameter("script_code");
        String masterOrderNumber = request.getParameter("master_order_number");
        String origDate = request.getParameter("orig_date");
        String item = request.getParameter("item_order_number");
        String occText = request.getParameter("occasion_text");
        String occasionId = request.getParameter("occasion_id");
        String companyId = request.getParameter("company_id");
        String sourceCode = request.getParameter("source_code");
        String pageName = request.getParameter("page_name");
        String country = request.getParameter("country");
        String state = request.getParameter("state");
        String rewardType = request.getParameter("reward_type");     
        String zipCode = request.getParameter("postal_code");
        String guid = request.getParameter("order_guid");
        String lineNumber = request.getParameter("line_number");
        //#587  - Flex-Fill 
        // Added the Shiping method of the original order. 
        String shippingMethod = request.getParameter("shipping_method");

        String deliveryDate = request.getParameter("delivery_date");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	try
	{
	    deliveryDate = sdf.format(new java.sql.Date(sdf.parse(deliveryDate).getTime()));
	} catch (Exception e) {
	    deliveryDate = "";
	}
       
      // Trim zip code to 5 digits
      if (zipCode != null && zipCode.length() > 5)
        zipCode = zipCode.substring(0, 5);

      //Put all the requset parameters into a map.  This map will
      //later be inserted into the xml which is returned.
      HashMap pageDataMap = new HashMap();
      pageDataMap.put("postal_code",zipCode);
      pageDataMap.put("category_index", categoryindex);
      pageDataMap.put("page_number", pageNumber);
      pageDataMap.put("sort_type", sortType);
      pageDataMap.put("master_order_number", masterOrderNumber);
      pageDataMap.put("orig_date", origDate);
      pageDataMap.put("buyer_full_name", customerName);
      pageDataMap.put("item_order_number", item);
      pageDataMap.put("occasion_text", occText);
      pageDataMap.put("occasion_id",occasionId);
      pageDataMap.put("company_id", companyId);
      pageDataMap.put("source_code", sourceCode);
      pageDataMap.put("page_name", pageName);
      pageDataMap.put("country", country);   
      pageDataMap.put("state",state);
      pageDataMap.put("reward_type",rewardType);   
      pageDataMap.put("master_order_number", masterOrderNumber);
      pageDataMap.put("postal_code", zipCode);
      pageDataMap.put("order_guid", guid);
      pageDataMap.put("line_number", lineNumber);
      pageDataMap.put("shipping_method", shippingMethod);
      //#587  - Flex-Fill 
      // Added the Shiping method of the original order. 
      pageDataMap.put("delivery_date", deliveryDate);

      //Declare other values which will later be placed in the pagedata map.
      //These values are either obtained from the DB or calculated.
      String pricingCode = "";
      String partnerId = "";
      String domesticServiceFee = "";
      String internationalServiceFee= "";
      List promotionList = new ArrayList();
      ProductFlagsVO flagsVO = new ProductFlagsVO();
      String jcpFlag = "";

      // Initialize data request
      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
          
        try {
        
          //get source code info         
          dataRequest.reset();
          dataRequest.setStatementID("SOURCE_CODE_RECORD_LOOKUP");
          dataRequest.addInputParam("source_code",sourceCode);
          CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);                 
          String snhID = "";
          if(rs != null && rs.next()){
              pricingCode = getValue(rs.getObject(7));
              snhID = getValue(rs.getObject(8));          
              partnerId = getValue(rs.getObject(9));
              jcpFlag = getValue(rs.getObject(30));
          }
          pageDataMap.put("pricing_code", pricingCode);
          pageDataMap.put("snh_id", snhID);
          pageDataMap.put("partner_id", partnerId);      
          pageDataMap.put("jcp_flag",jcpFlag);

          //if jcpenney order obtain script code from property file
          if(jcpFlag != null && jcpFlag.equalsIgnoreCase("Y")){
              scriptCode = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.JCPENNY_SCRIPT_CODE);
          }
          pageDataMap.put("script_code",scriptCode);

          //get image location
          String imageLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, EditProductConstants.PRODUCT_IMAGE_LOCATION);          
          pageDataMap.put("product_images",imageLocation);

          //get fee info
          logger.debug("get_snh_by_id: " + snhID + " " + deliveryDate);
          dataRequest.reset();
          dataRequest.setStatementID("GET_SNH_BY_ID");
          dataRequest.addInputParam("IN_SNH_ID",snhID);
          dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);
          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);                 
          if(rs != null && rs.next()){
              domesticServiceFee = getValue(rs.getObject(3));
              internationalServiceFee = getValue(rs.getObject(6));
          }
          else
          {
              //default fees to zero if no data was found
              domesticServiceFee = "0";
              internationalServiceFee = "0";
          }
          pageDataMap.put("domestic_service_fee", domesticServiceFee);
          pageDataMap.put("international_service_fee", internationalServiceFee);

            String domesticIntlFlag = null;
            if ( domesticFlag )
            {
                domesticIntlFlag = "D";
            }
            else
            {
                domesticIntlFlag = "I";
            }
            pageDataMap.put("domestic_flag",domesticIntlFlag);      
            
            // Add page data to xml
            DOMUtil.addSection(responseDocument, "pageData", "data", pageDataMap, true);
        
            // Load Category Page for Domestic
            if ( domesticFlag )
            {


                //Load Script
                dataRequest.reset();
                dataRequest.setStatementID("GET_SCRIPT_BY_PAGE_SFMB");
                dataRequest.addInputParam("IN_COMPANY_ID", companyId);
                dataRequest.addInputParam("IN_PAGE_ID", pageName);
                Object doc = DataAccessUtil.getInstance().execute(dataRequest); 
                if(doc != null){
                  DOMUtil.addSection(responseDocument, ((Document)DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
                }

                //Load Super Index List
                dataRequest.reset();
                dataRequest.setStatementID("GET_SUPER_INDEX_LIST_SFMB");
                dataRequest.addInputParam("IN_LIVE_FLAG", YES);
                dataRequest.addInputParam("IN_OCCASIONS_FLAG", NO);
                dataRequest.addInputParam("IN_PRODUCTS_FLAG", YES);
                dataRequest.addInputParam("IN_DROPSHIP_FLAG", NO);
                dataRequest.addInputParam("IN_COUNTRY_ID", null);
                dataRequest.addInputParam("IN_SOURCE_CODE_ID", sourceCode);            
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

                //Load Super Index List with other date passed in
                dataRequest.reset();
                dataRequest.setStatementID("GET_SUPER_INDEX_LIST_SFMB_OCC");
                dataRequest.addInputParam("IN_LIVE_FLAG", YES);
                dataRequest.addInputParam("IN_OCCASIONS_FLAG", YES);
                dataRequest.addInputParam("IN_PRODUCTS_FLAG", NO);
                dataRequest.addInputParam("IN_DROPSHIP_FLAG", NO);
                dataRequest.addInputParam("IN_COUNTRY_ID", null);
                dataRequest.addInputParam("IN_SOURCE_CODE_ID", sourceCode);            
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
                
                //Load sub index list
                dataRequest.reset();
                dataRequest.setStatementID("GET_SUB_INDEX_LIST_SFMB");
                dataRequest.addInputParam("IN_LIVE_FLAG", YES);
                dataRequest.addInputParam("IN_DROPSHIP_FLAG", NO);
                dataRequest.addInputParam("IN_COUNTRY_ID", null);
                dataRequest.addInputParam("IN_SOURCE_CODE_ID", sourceCode);
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
                
                //Load price points
                dataRequest.reset();
                dataRequest.setStatementID("GET_PRICE_POINTS_LIST");
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

                //Load product categories
                dataRequest.reset();
                dataRequest.setStatementID("GET_PRODUCT_CATEGORIES");
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

                //Load recipient search list
                dataRequest.reset();
                dataRequest.setStatementID("GET_RECIPIENT_SEARCH_LIST");
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

                //Load flower list
                dataRequest.reset();
                dataRequest.setStatementID("GET_FLOWER_LIST");
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

                //Load color list
                dataRequest.reset();
                dataRequest.setStatementID("GET_COLORS_LIST"); 
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());             

                //Load occasions
                dataRequest.reset();
                dataRequest.setStatementID("GET_PRODUCT_OCCASIONS");
                DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());             

                //Load occasion list
                dataRequest.reset();
                dataRequest.setStatementID("OCCASION_LIST_LOOKUP");
                Document occList = (Document) DataAccessUtil.getInstance().execute(dataRequest);
                DOMUtil.addSection(responseDocument, occList.getChildNodes());             

            }
            else
            //Else Internationial..show list of products
            {



                //Load Super Index List
                dataRequest.reset();
                dataRequest.setStatementID("GET_SUPER_INDEX_LIST_SFMB");
                dataRequest.addInputParam("IN_LIVE_FLAG", YES);
                dataRequest.addInputParam("IN_OCCASIONS_FLAG", NO);
                dataRequest.addInputParam("IN_PRODUCTS_FLAG", NO);
                dataRequest.addInputParam("IN_DROPSHIP_FLAG", NO);
                dataRequest.addInputParam("IN_COUNTRY_ID", country);
                dataRequest.addInputParam("IN_SOURCE_CODE_ID", sourceCode);            
                Document xmlResponse = (Document) DataAccessUtil.getInstance().execute(dataRequest);

                // Pull the super index details for the selected country
                Element superIndexData = null;
                String  indexId = null;
                String xpath = "//index";
                NodeList nl = DOMUtil.selectNodes(xmlResponse, xpath);
                if (nl.getLength() > 0)
                {
                  superIndexData = (Element) nl.item(0);
                  indexId = superIndexData.getAttribute("indexid");
                }


              //clear out zipcode for internationial searchers
              zipCode = "";
            
              //obtain a list of products for this country
              responseDocument = searchUtil.getProductsByCategory( pageNumber, indexId,
                     zipCode,  domesticFlag, 
                     sourceCode, pricePointId, country,
                     scriptCode,  domesticServiceFee,  internationalServiceFee,
                     state,  pricingCode,  partnerId,
                     promotionList,  rewardType,   flagsVO,
                     sortType,  customerName,  pageDataMap, getServletContext(), deliveryDate, origDate);
            } 

        } 
        finally
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }
        }

        return responseDocument;
	}




  /* This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*/
  private String getValue(Object obj) throws Exception
  {

    String value = null;

    if(obj == null)
    {
      return null;
    }

    if(obj instanceof BigDecimal) {
      value = obj.toString();      
    }
    else if(obj instanceof String) {
      value = (String)obj;
    }
    else if(obj instanceof Timestamp) {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    return value;
  }

  
 
}  
