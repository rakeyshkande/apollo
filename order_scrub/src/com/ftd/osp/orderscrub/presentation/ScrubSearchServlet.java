package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.ftdutilities.SympathyControls;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.orderscrub.constants.SecurityConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.LockUtil;
import com.ftd.osp.orderscrub.util.SearchManager;
import com.ftd.osp.orderscrub.util.StatsHelper;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;


/**
 * This servlet provides the functionality to load the Scrub search page and
 * manage the search logic execution.  It makes calls to generate the standard
 * single Scrub result as well as the result list for reinstate and pending
 * searches.  The search process maintains a list of viewed order while the user
 * is in the scrubbing process to prevent viewing the same order multiple times
 * from the current search request.  The search criteria is also maintained
 * throughout the scrubbing process.  Please reference SearchManager and SrubFilterBO
 * classes for more detail on search business logic.
 *
 * @author Brian Munter
 */

public class ScrubSearchServlet extends HttpServlet 
{    
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=ISO-8859-15";
    private static final String CHARACTER_ENCODING = "ISO-8859-15";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.ScrubSearchServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";
    private final static String XSL_SEARCH = ".../xsl/orderSearch.xsl";
    private final static String XSL_SEARCH_RESULT_LIST = ".../xsl/searchResults.xsl";
    private final static String XSL_ORDER_BEING_SCRUBBED = ".../xsl/orderBeingScrubbed.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Loads the data to display the Scrub search page.  Data returned is dependent 
     * and custom to the search mode selected(scrub, pending, reinstate).  Also origin 
     * or category display is dependent on security roles.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding(CHARACTER_ENCODING);
        if (SecurityHelper.isValidToken(request, response)) 
        {
            // Load search page
            this.loadSearch(request, response, null);  
        }
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. main_exit: This is action is called to handle the primary exit point 
     * on the search page<br/><br/>
     * 
     * 2. result_list: This action is called for pending and reinstate orders which 
     * require a preliminary result list for the user to chose from.  This is also 
     * called when a user selects an abandoned from the list which pops removes the 
     * order from the scrub system, refreshes the result list, and pops the order up 
     * in a order entry window.
     * 
     * 3. default: Default functionality to submit the order search.  There are 
     * parameters within this method from the request which determine how this 
     * method executes.  If the next_order parameter is passed, the process will  
     * reset the status of the previous order so it can be scrubbed later.  If the 
     * selected_master_order_number parameter is passed, the search is doing a 
     * straight lookup on a specific order.  Otherwise the search servlet will 
     * execute in a normal pattern and return to the search page if no results 
     * are found.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        request.setCharacterEncoding(CHARACTER_ENCODING);
        if (SecurityHelper.isValidToken(request, response)) 
        {
            this.submitSearch(request, response);
        }
    }

    private void loadSearch(HttpServletRequest request, HttpServletResponse response, String messageDisplay) throws ServletException, IOException
    {
        DataRequest dataRequest = null;

        // Retrieve adminAction from request and place it in session
        this.setAdminAction(request);

        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
        
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load destination types
            dataRequest.reset();
            dataRequest.setStatementID("ADDRESS_TYPES_LOOKUP");
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load order count
            dataRequest.reset();
            BigDecimal countResult = null;
            if(request.getParameter("sc_mode") != null && request.getParameter("sc_mode").equals("S"))
            {
                // Display count of orders in standard scrub
                dataRequest.setStatementID("GET_SCRUB_COUNT");
                countResult = (BigDecimal) DataAccessUtil.getInstance().execute(dataRequest);
            }
            else if(request.getParameter("sc_mode") != null && request.getParameter("sc_mode").equals("P"))
            {
                // Pending should display only pending order count
                dataRequest.setStatementID("GET_PENDING_COUNT");
                countResult = (BigDecimal) DataAccessUtil.getInstance().execute(dataRequest);
            }

            // Create page data
            HashMap pageData = new HashMap();
            
            if(countResult != null)
            {
                pageData.put("scrubCount", countResult.toString());
            }
                
            if(messageDisplay != null)
            {
                pageData.put("message_display", messageDisplay);
            }
            
            if(request.getParameter("abandoned_popup_flag") != null && request.getParameter("abandoned_popup_flag").equals("Y"))
            {
                // Add abandon flag to xml
                pageData.put("abandoned_popup_flag", request.getParameter("abandoned_popup_flag"));
                pageData.put("order_guid", request.getParameter("order_guid"));

                // Remove message display
                pageData.remove("message_display");
            }

            // Add xml to document
            DOMUtil.addSection(responseDocument, "searchCriteria", "criteria", this.generateSearchCriteria(request), true);
            DOMUtil.addSection(responseDocument, "searchOrigins", "origins", this.generateCategoryList(request, dataRequest), true);
            DOMUtil.addSection(responseDocument, "searchMercentOrigins", "mercentOrigins", this.generateMercentOrigins(request, dataRequest), true);
            DOMUtil.addSection(responseDocument, "productProperties", "productProperty", this.generateProductProperties(), true);
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            
            SecurityHelper secHelp = new SecurityHelper();
            HashMap pp = secHelp.getPartnerDisplayNamesForUser(request);
            DOMUtil.addSection(responseDocument, "preferred_partners_for_user", "preferred_partner", pp, true);
            
            // Add base data
            BaseCartBuilder cartBuilder = new BaseCartBuilder();
            DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(null, dataRequest, request, false));
            
            File xslFile = new File(getServletContext().getRealPath(XSL_SEARCH));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SEARCH, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    private void submitSearch(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

        DataRequest dataRequest = null;

        long currentTime = System.currentTimeMillis();

        String action = request.getParameter("servlet_action");
        logger.info("action: " + action);

        if(action != null && action.equals("main_exit"))
        {
            try
            {

                //Locking was added for PhaseIII, emueller 4/19/05                               
                //release buyer record lock
                String guid = request.getParameter("order_guid");
                logger.info("guid: " + guid);

                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
                
                // Load the order
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(guid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                //OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
        
                LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);

                // Load return page
                String securityParams = "&securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

                String mainExitURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT);
                String adminAction = (String) request.getSession().getAttribute("adminAction"); 
                if(adminAction != null && adminAction.length() > 0) 
                { 
                    mainExitURL = mainExitURL + adminAction;
                } 
                else
                {
                    mainExitURL = mainExitURL + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
                }

                response.sendRedirect(mainExitURL + securityParams);
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }            
        }
        else if(action != null && action.equals("search_exit")) {
            // Load search page
            this.loadSearch(request, response, null);  
        }
        else if(action != null && action.equals("result_list"))   
        {
            // Pending and reinstate orders
            try
            {
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
                
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                if(request.getParameter("abandoned_popup_flag") != null && request.getParameter("abandoned_popup_flag").equals("Y"))
                {
                    // Delete order from scrub
                    dataRequest.reset();
                    dataRequest.setStatementID("REMOVE_ORDER_FROM_SCRUB");
                    dataRequest.addInputParam("order_guid", request.getParameter("order_guid"));
                    DataAccessUtil.getInstance().execute(dataRequest);

                    // Add abandon flag to xml
                    HashMap pageData = new HashMap();
                    pageData.put("abandoned_popup_flag", request.getParameter("abandoned_popup_flag"));
                    pageData.put("order_guid", request.getParameter("order_guid"));
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                }

                // Retrieve sorted search result list and add to xml
                SearchManager searchManager = new SearchManager();
                ArrayList resultList = searchManager.retrieveResultMap(request, dataRequest);

                if(resultList != null && resultList.size() > 0)
                {
                    HashSet<String> preferredPartnerList = this.appendResultList(resultList, responseDocument);

                    // If any Preferred Partner orders (e.g., USAA) in result set, retrieve and add any associated permissions for this CSR.
                    // Also include any alert message text.
                    Iterator ppit = null;
                    if (preferredPartnerList != null) {
                        SecurityHelper secHelp = new SecurityHelper();
                        HashMap pp = secHelp.getPartnerDisplayNamesForUser(request);
                        DOMUtil.addSection(responseDocument, "preferred_partners_for_user", "preferred_partner", pp, true);

                        ppit = preferredPartnerList.iterator();
                        HashMap ppAlerts = new HashMap();
                        while(ppit.hasNext()) {
                            String ppName = (String)ppit.next();
                            String alertMessage = ConfigurationUtil.getInstance().getContentWithFilter(dataRequest.getConnection(), GeneralConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                         GeneralConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                         ppName, null);
                            ppAlerts.put(ppName, alertMessage);
                        }
                        DOMUtil.addSection(responseDocument, "noPermissionsForPreferredPartner", "preferred_partner", ppAlerts, true);
                    }

                    // Add base data
                    BaseCartBuilder cartBuilder = new BaseCartBuilder();
                    DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(null, dataRequest, request, false));

                    File xslFile = new File(getServletContext().getRealPath(XSL_SEARCH_RESULT_LIST));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SEARCH_RESULT_LIST, null);
                }
                else if (request.getParameter("sc_pro_confirmation_number") != null 
                		&& !request.getParameter("sc_pro_confirmation_number").equals("") 
                		&& !(resultList !=null && resultList.size() > 0)){
                	this.loadSearch(request, response, ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.SEARCH_NO_RESULTS));
                }
                else
                {
                    // No orders found with the specified search criteria
                    SearchResultVO searchResult = searchManager.retrieveOrderBeingScrubbed(request, dataRequest);
                    if (searchResult != null && searchResult.getOrderXML() != null) {
                        // Add xml to document
                        DOMUtil.addSection(responseDocument, searchResult.getOrderXML().getChildNodes());

                        // Add base data
                        BaseCartBuilder cartBuilder = new BaseCartBuilder();
                        DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(null, dataRequest, request, false));

                        File xslFile = new File(getServletContext().getRealPath(XSL_ORDER_BEING_SCRUBBED));
                        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_ORDER_BEING_SCRUBBED, null);
                    } else {
                        // No orders found with the specified search criteria
                        this.loadSearch(request, response, ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.SEARCH_NO_RESULTS));
                    }
                }
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            try
            {
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Update order status to remove scrub lock if next order
                if(action != null && action.equals("next_order"))
                {
                    // Load order guid from scrub for processing
                    String resetGuid = request.getParameter("order_guid");

                    // Reset status to previous status
                    dataRequest.reset();
                    dataRequest.setStatementID("UPDATE_TO_PREVIOUS_STATUS_ANY");
                    dataRequest.addInputParam("order_guid", resetGuid);
                    DataAccessUtil.getInstance().execute(dataRequest);

                    // Load the order for stats recording
                    ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                    OrderVO order = scrubMapperDAO.mapOrderFromDB(resetGuid);
            
                    //relese lock on buyer
                    LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);

                    // Record exit statistics
                    logger.info("Search Execution Time 1: " + new Long(System.currentTimeMillis() - currentTime).toString());
                    
                    StatsHelper.getInstance().updateOrderStatistics(order, GeneralConstants.STATS_OUT, request, dataRequest.getConnection());

                    logger.info("Search Execution Time 2: " + new Long(System.currentTimeMillis() - currentTime).toString());
                }

                String messageDisplay = null;
                SearchResultVO searchResult = null;
                SearchManager searchManager = new SearchManager();
                ArrayList proResults = null;
                boolean showProFlowersScreen = false;
                logger.info("selected_master_order_number: " + request.getParameter("selected_master_order_number"));
                if(request.getParameter("selected_master_order_number") != null && !request.getParameter("selected_master_order_number").equals(""))
                {
                    // Retrieve guid for specific order number
                    searchResult = searchManager.retrieveByOrderNumber(request.getParameter("selected_master_order_number"), request, dataRequest);
                    if (searchResult == null) {
                        searchResult = searchManager.retrieveOrderBeingScrubbed(request, dataRequest);
                    }
                } else if(request.getParameter("sc_pro_confirmation_number")!=null 
                		&& !("").equals(request.getParameter("sc_pro_confirmation_number")) 
                		&& (action == null || !action.equals("next_order"))) {
	                	proResults = searchManager.retrieveResultMap(request, dataRequest);
	                	if(proResults != null && proResults.size() > 0) {
	                		//Re direct to the new page
	                		showProFlowersScreen = true;
	                	}
                	else {
                         messageDisplay = ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.SEARCH_NO_RESULTS);
                	}
                	// step1: get the confirmation number
                	//check the size of cursor.if 1, 
                	//if many
                }
                else
                {
                    // Retrieve guid from search criteria
                    searchResult = searchManager.retrieveResult(request, dataRequest, false);

                    if(searchResult == null && (action == null || !action.equals("next_order")))
                    {
                        searchResult = searchManager.retrieveOrderBeingScrubbed(request, dataRequest);
                        if (searchResult == null) {
                            messageDisplay = ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.SEARCH_NO_RESULTS);
                        }
                    }
                }
                   
                if(searchResult != null && searchResult.getGuid() != null && !searchResult.getGuid().equals("") && !showProFlowersScreen) 
                {
                    logger.info("searchResult.getGuid(): " + searchResult.getGuid());
                    // Create the initial document
                    Document responseDocument = DOMUtil.getDocument();
                    // Load new order to scrub
                    OrderLoader orderLoader = new OrderLoader();
                    orderLoader.loadOrder(searchResult.getGuid(), null, responseDocument, dataRequest, request);
                    
                    ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                    OrderVO order = scrubMapperDAO.mapOrderFromDB(searchResult.getGuid());
                    
                    //Sympathy Controls
                    SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                    HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                    if(sympathyAlerts!=null){
                    	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
                    	
                    }
                    
                    File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                } 
                else if (showProFlowersScreen) {
                	Document responseDocument = DOMUtil.getDocument();
                	Iterator proit = null;
                	HashSet<String> proFlowerList = this.appendResultList(proResults, responseDocument);
                    if (proFlowerList != null) {
                        SecurityHelper secHelp = new SecurityHelper();
                        HashMap pp = secHelp.getPartnerDisplayNamesForUser(request);
                        DOMUtil.addSection(responseDocument, "preferred_partners_for_user", "preferred_partner", pp, true);

                        proit = proFlowerList.iterator();
                        HashMap proAlerts = new HashMap();
                        while(proit.hasNext()) {
                            String ppName = (String)proit.next();
                            String alertMessage = ConfigurationUtil.getInstance().getContentWithFilter(dataRequest.getConnection(), GeneralConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                         GeneralConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                         ppName, null);
                            proAlerts.put(ppName, alertMessage);
                        }
                        DOMUtil.addSection(responseDocument, "noPermissionsForPreferredPartner", "preferred_partner", proAlerts, true);
                    }
                    BaseCartBuilder cartBuilder = new BaseCartBuilder();
                    DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(null, dataRequest, request, false));

                    File xslFile = new File(getServletContext().getRealPath(XSL_SEARCH_RESULT_LIST));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SEARCH_RESULT_LIST, null);
                }
                else
                {
                    if (searchResult != null && searchResult.getOrderXML() != null) {
                        Document responseDocument = DOMUtil.getDocument();
                        // Add xml to document
                        DOMUtil.addSection(responseDocument, searchResult.getOrderXML().getChildNodes());

                        // Add base data
                        BaseCartBuilder cartBuilder = new BaseCartBuilder();
                        DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(null, dataRequest, request, false));

                        File xslFile = new File(getServletContext().getRealPath(XSL_ORDER_BEING_SCRUBBED));
                        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_ORDER_BEING_SCRUBBED, null);
                    } else {
                        // No orders found with the specified search criteria
                        this.loadSearch(request, response, messageDisplay);
                    }
                }  
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }

        logger.info("Search Execution Time 3: " + new Long(System.currentTimeMillis() - currentTime).toString());
    }

    private HashSet<String> appendResultList(ArrayList resultList, Document responseDocument) throws Exception
    {
        SearchResultVO searchResult = null;

        DateFormat formatter=new SimpleDateFormat("mm/dd/yyyy");  

        Element resultElement = null;
        Element guidElement = null;
        Element orderNumberElement = null;
        Element externalOrderNumberElement = null;
        Element lastNameElement = null;
        Element firstNameElement = null;
        Element recipLastNameElement = null;
        Element recipFirstNameElement = null;
        Element cityElement = null;
        Element stateElement = null;
        Element orderDateElement = null;
        Element deliveryDateElement = null;
        Element itemCountElement = null;
        Element orderTotalElement = null;
        Element csrIdElement = null;
        Element preferredPartnerElement = null;

        Element resultListElement = responseDocument.createElement("search_result_list");
        HashSet<String> preferredPartnerList = null;
        
        if(resultList != null && resultList.size() > 0)
        {
            for(int i = 0; i < resultList.size(); i++)
            {
                searchResult = (SearchResultVO) resultList.get(i);

                resultElement = responseDocument.createElement("search_result");
                resultElement.setAttribute("sort_number", searchResult.getSortOrder());

                guidElement = responseDocument.createElement("order_guid");
                if(searchResult.getGuid() != null)
                {
                    guidElement.appendChild(responseDocument.createTextNode(searchResult.getGuid()));
                    resultElement.appendChild(guidElement);
                }

                orderNumberElement = responseDocument.createElement("master_order_number");
                if(searchResult.getMasterOrderNumber() != null)
                {
                    orderNumberElement.appendChild(responseDocument.createTextNode(searchResult.getMasterOrderNumber()));
                    resultElement.appendChild(orderNumberElement);
                }
                
                externalOrderNumberElement = responseDocument.createElement("external_order_number");
                if(searchResult.getExternalOrderNumber() != null)
                {
                	externalOrderNumberElement.appendChild(responseDocument.createTextNode(searchResult.getExternalOrderNumber()));
                    resultElement.appendChild(externalOrderNumberElement);
                }

                lastNameElement = responseDocument.createElement("buyer_last_name");
                if(searchResult.getBuyerLastName() != null)
                {
                    lastNameElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerLastName()));
                    resultElement.appendChild(lastNameElement);
                }

                firstNameElement = responseDocument.createElement("buyer_first_name");
                if(searchResult.getBuyerFirstName() != null)
                {
                    firstNameElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerFirstName()));
                    resultElement.appendChild(firstNameElement);
                }
                
                recipLastNameElement = responseDocument.createElement("recip_last_name");
                if(searchResult.getRecipLastName() != null)
                {
                	recipLastNameElement.appendChild(responseDocument.createTextNode(searchResult.getRecipLastName()));
                    resultElement.appendChild(recipLastNameElement);
                }

                recipFirstNameElement = responseDocument.createElement("recip_first_name");
                if(searchResult.getRecipFirstName() != null)
                {
                	recipFirstNameElement.appendChild(responseDocument.createTextNode(searchResult.getRecipFirstName()));
                    resultElement.appendChild(recipFirstNameElement);
                }

                cityElement = responseDocument.createElement("buyer_city");
                if(searchResult.getBuyerCity() != null)
                {
                    cityElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerCity()));
                    resultElement.appendChild(cityElement);
                }

                stateElement = responseDocument.createElement("buyer_state");
                if(searchResult.getBuyerState() != null)
                {
                    stateElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerState()));
                    resultElement.appendChild(stateElement);
                }

                orderDateElement = responseDocument.createElement("order_date");
                if(searchResult.getOrderDate() != null)
                {
                    orderDateElement.appendChild(responseDocument.createTextNode(searchResult.getOrderDate()));
                    resultElement.appendChild(orderDateElement);
                }
                
                deliveryDateElement = responseDocument.createElement("delivery_date");
                if(searchResult.getDeliveryDate() != null)
                {
                	deliveryDateElement.appendChild(responseDocument.createTextNode(formatter.format(searchResult.getDeliveryDate().getTime())));
                    resultElement.appendChild(deliveryDateElement);
                }

                itemCountElement = responseDocument.createElement("item_count");
                if(searchResult.getItemCount() != null)
                {
                    itemCountElement.appendChild(responseDocument.createTextNode(searchResult.getItemCount()));
                    resultElement.appendChild(itemCountElement);
                }

                orderTotalElement = responseDocument.createElement("order_total");
                if(searchResult.getOrderTotal() != null)
                {
                    orderTotalElement.appendChild(responseDocument.createTextNode(searchResult.getOrderTotal()));
                    resultElement.appendChild(orderTotalElement);
                }

                csrIdElement = responseDocument.createElement("csr_id");
                if(searchResult.getCsrId() != null)
                {
                    csrIdElement.appendChild(responseDocument.createTextNode(searchResult.getCsrId()));
                    resultElement.appendChild(csrIdElement);
                }

                preferredPartnerElement = responseDocument.createElement("preferred_partner_name");  // e.g., USAA
                if(searchResult.getPreferredProcessingPartner() != null)
                {
                    preferredPartnerElement.appendChild(responseDocument.createTextNode(searchResult.getPreferredProcessingPartner()));
                    resultElement.appendChild(preferredPartnerElement);
                    if (preferredPartnerList == null) {
                        preferredPartnerList = new HashSet<String>();
                    }
                    preferredPartnerList.add(searchResult.getPreferredProcessingPartner());
                }

                resultListElement.appendChild(resultElement);
            }
        }

        responseDocument.getFirstChild().appendChild(resultListElement);
        return preferredPartnerList;
    }

    private HashMap generateSearchCriteria(HttpServletRequest request)
    {
        HashMap searchCriteriaMap = new HashMap();
        
        searchCriteriaMap.put("sc_email_address", request.getParameter("sc_email_address"));
        searchCriteriaMap.put("sc_product_code", request.getParameter("sc_product_code"));
        searchCriteriaMap.put("sc_ship_to_type", request.getParameter("sc_ship_to_type"));
        searchCriteriaMap.put("sc_confirmation_number", request.getParameter("sc_confirmation_number"));//sc_pro_confirmation_number
        searchCriteriaMap.put("sc_pro_confirmation_number", request.getParameter("sc_pro_confirmation_number"));
        searchCriteriaMap.put("sc_origin", request.getParameter("sc_origin"));
        searchCriteriaMap.put("sc_date_flag", request.getParameter("sc_date_flag"));
        searchCriteriaMap.put("sc_date", request.getParameter("sc_date"));
        searchCriteriaMap.put("sc_last_name_flag", request.getParameter("sc_last_name_flag"));
        searchCriteriaMap.put("sc_last_name", request.getParameter("sc_last_name"));
        searchCriteriaMap.put("sc_phone_flag", request.getParameter("sc_phone_flag"));
        searchCriteriaMap.put("sc_phone", request.getParameter("sc_phone"));
        searchCriteriaMap.put("sc_mode", request.getParameter("sc_mode"));
        searchCriteriaMap.put("sc_search_type", request.getParameter("sc_search_type"));
        searchCriteriaMap.put("sc_product_property", request.getParameter("sc_product_property"));
        searchCriteriaMap.put("sc_pc_membership_id", request.getParameter("sc_pc_membership_id"));
        searchCriteriaMap.put("sc_hide_test_orders", request.getParameter("sc_hide_test_orders"));
        return searchCriteriaMap;
    }

    private HashMap generateCategoryList(HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        HashMap categoryMap = new HashMap();
        
        SecurityManager securityManager = SecurityManager.getInstance();
        String securityToken = request.getParameter("securitytoken");
        
        dataRequest.reset();
        dataRequest.setStatementID("ORIGIN_LIST_LOOKUP");
        CachedResultSet originResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        String originId = null;
        String originType = null;
        String originDescription = null;
        if(originResultSet != null && originResultSet.getRowCount() > 0)
        {
            while(originResultSet.next())
            {
                originId = (String) originResultSet.getObject(1);
                originDescription = (String) originResultSet.getObject(2);
                originType = (String) originResultSet.getObject(3);

                if (originType.equals(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ARIBA_ORIGIN_TYPE))
                    && !ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG).equals("Y"))
                {
                    // If ariba categories permissions needed
                    if(securityManager.assertPermission(request.getParameter("context"), securityToken, 
                        SecurityConstants.RESOURCE_ARIBA_CATEGORY, SecurityConstants.PERMISSION_VIEW))
                    {
                        categoryMap.put(originId, originDescription);
                    }
                }
                else if(originType.equals(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.TEST_ORIGIN_TYPE))
                        && !ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG).equals("Y"))
                {
                    // If test categories permissions needed
                    if(securityManager.assertPermission(request.getParameter("context"), securityToken, 
                        SecurityConstants.RESOURCE_TEST_CATEGORY, SecurityConstants.PERMISSION_VIEW))
                    {
                        categoryMap.put(originId, originDescription);
                    }
                }
                else
                {
                    // Standard categories with no special permissions
                    categoryMap.put(originId, originDescription);
                }
            }
        }
        categoryMap = processMercentOrigins(categoryMap, request, dataRequest);
        return categoryMap;
    }
    
    private HashMap processMercentOrigins(HashMap categoryMap, HttpServletRequest request, DataRequest dataRequest) {
    	
    	MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
    	List<String> mercentOriginsList = mercentOrderPrefixes.getMercentOrigins();
    	for(String mercentOrigin : mercentOriginsList) {
    		if(categoryMap.containsKey(mercentOrigin))
    		{
    			categoryMap.remove(mercentOrigin);
    		}
    	} 
    	categoryMap.put("MERCENT","Mercent Marketplace");
    	return categoryMap;
    }
    private HashMap generateMercentOrigins(HttpServletRequest request, DataRequest dataRequest) throws Exception {
    	
    	HashMap categoryMap = new HashMap();
    	MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
    	List<String> mercentOriginsList = mercentOrderPrefixes.getMercentOrigins();
    	for(String mercentOrigin : mercentOriginsList) {
    		categoryMap.put(mercentOrigin, "Mercent Internet Orders");
    	}    	
    	return categoryMap;
    }
    
    private HashMap generateProductProperties() throws Exception {
        return FTDCommonUtils.getPriorityProductProperties();   
    }

    private void setAdminAction(HttpServletRequest request)
    {
        String adminAction = request.getParameter("adminAction");
        if(adminAction != null && adminAction.length() > 0)
        {       
            request.getSession().setAttribute("adminAction", adminAction);
        }
    }
}