package com.ftd.osp.orderscrub.presentation;

import com.ftd.osp.orderscrub.util.ISpellCheck;
import com.ftd.osp.orderscrub.util.SpellCheckImpl;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SpellCheckServlet extends HttpServlet
{
    private ISpellCheck spell = null;
    private Logger logger;
    private ServletConfig servletConfig;
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init( ServletConfig config )throws ServletException
    {
        servletConfig = config;
	    this.logger = new Logger("com.ftd.osp.orderscrub.presentation.SpellCheckServlet");

        Enumeration enum1 = config.getInitParameterNames();
        Properties props = new Properties();
        while ( enum1.hasMoreElements() )
        {
            String param = (String)enum1.nextElement();
            logger.debug("Accessing init parameter :: " + param + " value :: " + config.getInitParameter( param ));
            props.put( param, config.getInitParameter( param ) );
        }

        try
        {
            spell = new SpellCheckImpl( props );
        }
        catch ( IOException ioe )
        {
            logger.error(ioe);
            throw new ServletException(ioe);
            //spell = null;
        }
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        logger.debug("Calling processCheckSpell()");
        processCheckSpell( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        logger.debug("Calling processCheckSpell()");
        processCheckSpell( request, response );
    }

/**
  * check the word to see if it is valid. If not, return suggestions
  * The returned suggestion format must comply with the javaScript rules.
  * @param word String to be checked
  * @param int end the absolute position in the original string
  * @return String suggestions
  */
    private String getSuggestions( String word, int end ) throws IOException
    {
        if ( word == null || word.length() == 0 )
            return null;
        if ( spell.spellCheck( word ) )
            return null;
        List suggest = spell.suggest( word );
        int start = end - word.length();
        StringBuffer result = new StringBuffer();
        result.append( "{start:" );
        result.append( start );
        result.append( ",end:" );
        result.append( end );
        result.append( ",suggestions:[" );
        if ( suggest != null )
        {
            for ( int i=0; i<suggest.size(); i++ )
            {
                result.append( "\"" + (String)suggest.get(i) + "\"");
                if ( i < suggest.size() -1 )
                    result.append( "," );
            }
        }
        result.append( "]}" );
        return result.toString();
    }

    private void processCheckSpell( HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        String original = request.getParameter( "content" );

        if ( original == null )
            return;

        original = parseValue( original );

        String temp = original + " " ;

        ArrayList suggestions = new ArrayList();

        StringBuffer sb = new StringBuffer();
        String suggest = null;
        for (int location=0; location<temp.length(); location++)
        {
            char c = temp.charAt( location );
            if ( charOmitted( c ) ) {
                if ( !spell.spellCheck( sb.toString() ) ) {
                    suggest = getSuggestions( sb.toString(), location );

                    if ( suggest != null ) {
                        suggestions.add( suggest );
					}
                }
                // reset
                sb = new StringBuffer();

            }
            else {
                sb.append( c );
            }
        }

        String corrections = "[";
        for (int i=0; i<suggestions.size(); i++) {
            corrections += (String)suggestions.get(i);
            if ( i < suggestions.size() - 1 )
                corrections += ",";
        }
        corrections += "]";

        StringBuffer sbOriginal = new StringBuffer();

        for (int i=0; i<original.length(); i++)
        {
            char c = original.charAt( i );
            if ( c == '"' )
                sbOriginal.append( "\\" );
            sbOriginal.append( c );
        }

        request.setAttribute( "original", sbOriginal.toString() );
        request.setAttribute( "text", sbOriginal.toString() );
        request.setAttribute( "corrections", corrections );
        request.setAttribute( "callerName", request.getParameter("callerName"));
        request.getRequestDispatcher("/response.jsp").forward( request, response );
    }

/**
  * Omit the char in the string to be spell checked
  * All the character that do not want to be checked should go here
  * @param c the char to be checked.
  * @return true if the passed character should be omitted.
  */
    private static boolean charOmitted ( char c )
    {
        return ( Character.isWhitespace( c ) || (c == ' ') || (c == '"') || ( c =='.') || ( c == ':' )
                 || ( c == ';' ) || ( c == '$') || ( c == '!' ) || ( c == '?' ) || ( c == ',') );
    }
/**
  * Change end of line character to html <br> tag.
  * @param String value to be changed
  * @return the  new formatted String
  */
    private String parseValue( String value )
    {
        StringBuffer sb = new StringBuffer();
		logger.debug("parseValue(): value :: " + value);
        for (int i=0; i<value.length(); i++)
        {
            char c = value.charAt( i );
            if ( c == '\r' )
                sb.append( "" );
            else if ( c == '\n' )
                sb.append( " <br> " );
            else
                sb.append( c );
        }
        return sb.toString();
    }
}
