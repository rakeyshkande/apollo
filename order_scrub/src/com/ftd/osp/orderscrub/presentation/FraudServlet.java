package com.ftd.osp.orderscrub.presentation;

import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;


/**
 * This servlet is used to generate the fraud page and validate the submitted data.
 *
 * @author Brian Munter
 */

public class FraudServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.FraudServlet";
    private final static String XSL_FRAUD = ".../xsl/fraudOrder.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Execute the servlet.  The action is checked to see if the request is for a load or a submit.
     * The default is submit if no action is passed.
     * @param request
     * @param response
     * 
     * @exception ServletException
     * @exception IOException
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        String action = request.getParameter(ACTION);
    
        if (action != null && action.equals(ACTION_LOAD))    
        {
            this.loadFraud(request, response);     
        }
        else
        {
            this.submitFraud(request, response);    
        }
    }
    

    private void loadFraud(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
    
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load fraud dispositions
            dataRequest.reset();
            dataRequest.setStatementID("FRAUD_DISPOSITION_LIST_LOOKUP");
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

            // Add page data to xml
            HashMap pageData = new HashMap();
            pageData.put("master_order_number", request.getParameter("master_order_number"));
            pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            File xslFile = new File(getServletContext().getRealPath(XSL_FRAUD));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_FRAUD, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    private void submitFraud(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;

        String action = request.getParameter("servlet_action");

        if(action != null && action.equals("validation"))
        {
            try
            {
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
            
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
        
                HashMap pageData = new HashMap();
                HashMap errorMessages = new HashMap();

                String fraudId = request.getParameter("fraud_id");
                String comments = request.getParameter("fraud_comments");

                // Load data for display
                pageData.put("master_order_number", request.getParameter("master_order_number"));
                pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
                pageData.put("fraud_id", fraudId);
                pageData.put("fraud_comments", comments);

                // Load validation messages and status
                if(fraudId == null || fraudId.trim().equals(""))
                {
                    errorMessages.put("fraud_id_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.FRAUD_DISPOSITION_ERROR));  
                }
                /*if(comments == null || comments.trim().equals(""))
                {
                    errorMessages.put("fraud_comments_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.FRAUD_COMMENTS_ERROR));
                }*/

                if(errorMessages.size() > 0)
                {
                    pageData.putAll(errorMessages);

                    // Load fraud dispositions
                    dataRequest.reset();
                    dataRequest.setStatementID("FRAUD_DISPOSITION_LIST_LOOKUP");
                    DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_FRAUD));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_FRAUD, null);
                }
                else
                {
                    // Return to shopping cart
                    pageData.put("validation_flag", "Y");

                    // Load fraud dispositions
                    dataRequest.reset();
                    dataRequest.setStatementID("FRAUD_DISPOSITION_LIST_LOOKUP");
                    DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_FRAUD));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_FRAUD, null);
                }

            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
    }
}