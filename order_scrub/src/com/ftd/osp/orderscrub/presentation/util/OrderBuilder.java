package com.ftd.osp.orderscrub.presentation.util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.qmsvalidation.QMSValidation;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.AVSAddressVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.DestinationsVO;
import com.ftd.osp.utilities.order.vo.FraudCommentsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * This is the main utility used to update the order with the data which 
 * the user submitted from the shopping cart.  It also calls QMS and the 
 * DataMassager to finalize the updated order.
 *
 * @author Brian Munter
 */

public class OrderBuilder 
{
    private Logger logger;
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.util.OrderBuilder";
    private static final String INSERT_APP_COUNTER = "INSERT_APP_COUNTER";
    private boolean isSCChangeRequest;    

    /** 
     * Constructor
     */
    public OrderBuilder()
    {
        logger = new Logger(LOGGER_CATEGORY);
    }

    public OrderBuilder(boolean isSCChangeRequest) {		
		this.logger = new Logger(LOGGER_CATEGORY);
		this.isSCChangeRequest = isSCChangeRequest;
	}

    /**
     * Primary method which updates entire order object with data submitted from 
     * cart.  Uses QMS and DataMassager after all updates are complete to ensure 
     * data integrity.
     * 
     * @param order OrderVO
     * @param request HttpServletRequest
     * @param connection Connection
     * @exception Exception
     */
    public OrderVO updateOrderFromRequest(OrderVO order, HttpServletRequest request, Connection connection) throws Exception
    {
     
        boolean isAmazonOrder = false;
        boolean isWalmartOrder = false;
        boolean isMercentOrder = false;
        boolean isPartnerOrder = true;
        boolean clearMilesPointsPayment = false;
        
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(connection);
        
        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                  isAmazonOrder = true;
        
        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                  isWalmartOrder = true;
        
        if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	isMercentOrder = true;
        } else if(new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), connection)) {
        	isPartnerOrder = true;
        }
        
        if(request.getParameter("source_code") != null)
        {
            order.setSourceCode(request.getParameter("source_code"));
        }
        if(request.getParameter("source_code_description") != null)
        {
            order.setSourceDescription(request.getParameter("source_code_description"));
        }
        if(request.getParameter("partner_id") != null)
        {
            order.setPartnerId(request.getParameter("partner_id"));
        }
        if(request.getParameter("company_id") != null)
        {
            order.setCompanyId(request.getParameter("company_id"));
        }

        // Buyer
        List buyerList = new ArrayList();
        buyerList.add(this.getBuyer(order, request, order.getOrderOrigin()));
        order.setBuyer(buyerList); 

        // Bulk order fields
        List contactInfoList = new ArrayList();
        OrderContactInfoVO contactInfo = this.getContactInfo(order, request);
        if(contactInfo != null) 
        {
            contactInfoList.add(contactInfo);
        }
        order.setOrderContactInfo(contactInfoList);

        // Fraud flag
        if(request.getParameter("fraud_flag") != null && request.getParameter("fraud_flag").equals("on"))
        {
            order.setFraudFlag("Y");
        }
        else
        {
            order.setFraudFlag("N");
        }

        // Fraud fields
        List fraudList = new ArrayList();
        FraudCommentsVO fraudComments = this.getFraudComments(order, request);
        if(fraudComments != null)
        {
            fraudList.add(fraudComments);
            //order.setFraudFlag("Y");
        }
        else
        {
            //order.setFraudFlag("N");
        }
        order.setFraudComments(fraudList);

        // Membership fields
        List membershipList = new ArrayList();
        MembershipsVO membership = this.getMembership(order, request);
        if(membership != null)
        {
            membershipList.add(membership);
        }
        order.setMemberships(membershipList);

        // Payments
        List  paymentList = new ArrayList();
        
        PaymentsVO payment = new PaymentsVO();
        PaymentsVO currentPayment = null;

        if(order.getPayments() != null)
        {
            for(int i = 0; i < order.getPayments().size(); i++)
            {
                currentPayment = (PaymentsVO) order.getPayments().get(i);
                
                /*if(currentPayment.getPaymentMethodType() != null && currentPayment.getPaymentMethodType().equals("G"))
                {
                    // Add gift certificate to payment list
                    paymentList.add(currentPayment);
                } 
                else if(currentPayment.getPaymentMethodType() != null && currentPayment.getPaymentMethodType().equals("R"))
                {
                    // Add gift card to payment list
                    paymentList.add(currentPayment);
                } */
                if (currentPayment.getPaymentMethodType() != null && currentPayment.getPaymentMethodType().equals("P")
                    && request.getParameter("payment_method") != null && request.getParameter("payment_method").equals("P") ) {
                    // If existing payment is alt pay and has not changed, add it to payment list.
                    paymentList.add(currentPayment);
                }
                /*
                 * In case payment type is GC/GD and the order has just GC/GD payment details are invalid then set amount to zero.
                 * 
                 */
                else if (currentPayment.getPaymentMethodType() != null && (currentPayment.getPaymentMethodType().equals("G") || currentPayment.getPaymentMethodType().equals("R")))
                {
                	if(request.getParameter("is_gcgd_valid").equals("Y"))
                	{
                		if("G".equalsIgnoreCase(currentPayment.getPaymentMethodType()) && currentPayment.getGcCouponIssueAmount() != null)
                			if(order.getOrderTotal() == null || new BigDecimal(order.getOrderTotal()).compareTo(new BigDecimal("0")) == 0 || new BigDecimal(order.getOrderTotal()).compareTo(currentPayment.getGcCouponIssueAmount()) == 1)
                			{
                				currentPayment.setAmount(currentPayment.getGcCouponIssueAmount().toString());
                			}else{
                				currentPayment.setAmount(order.getOrderTotal());
                			}
                		else if("R".equalsIgnoreCase(currentPayment.getPaymentMethodType()))
                		{
                			if(order.getOrderTotal() == null 
                					|| new BigDecimal(order.getOrderTotal()).compareTo(new BigDecimal("0")) == 0 
                					|| (currentPayment.getOrigAuthAmount()!= null && new BigDecimal(order.getOrderTotal()).compareTo(new BigDecimal(currentPayment.getOrigAuthAmount())) == 1))
                			{
                				currentPayment.setAmount(currentPayment.getOrigAuthAmount());
                			}else{
                				currentPayment.setAmount(order.getOrderTotal());
                			}
                		}

                			
                		paymentList.add(currentPayment);
                	}
                	else if(request.getParameter("is_gcgd_valid").equals("N"))
                	{
                		currentPayment.setAmount("0");
                		paymentList.add(currentPayment);
                	}
                	
                }
                else 
                {
                    // Clear out current credit card or invoice payment for update
                    payment = currentPayment;
                    Boolean retValResult[] = paymentChanged(currentPayment,request);
                    if(retValResult!=null && !(retValResult[0])){
                        payment.setAafesTicket("");
                        payment.setAcqReferenceNumber("");
                        payment.setAmount("");
                        payment.setAuthNumber("");
                        payment.setAuthResult("");
                        payment.setAvsCode("");
                        payment.setDescription("");
                        payment.setGiftCertificateId("");
                        payment.setInvoiceNumber("");
                        payment.setPaymentMethodType("");
                        payment.setPaymentsType("");
                        payment.setApAccount("");
                        payment.setApAuth("");
                        payment.setApAccountId("");
                        payment.setMilesPointsAmt("");
                        payment.setCscResponseCode("");
                        payment.setCscValidatedFlag("N");
                        payment.setCscFailureCount(0);
                        if(retValResult[1]){
                        	payment.setWalletIndicator("");
                        }
                        clearMilesPointsPayment = true;  // Set flag since we need to clear from orders and orderdetail too
                    }
                }
            }
        }
        payment.setNcApprovalCode(request.getParameter("no_charge_username"));

        if (clearMilesPointsPayment) {
            order.setMpRedemptionRateAmt("");  // Since no longer miles/points payment, clear rate too
        }
        if(request.getParameter("payment_method") != null && request.getParameter("payment_method").equals("C"))
        {
            payment.setPaymentMethodType("C");
            
            List creditCardList = new ArrayList();
            CreditCardsVO creditCard = new CreditCardsVO();

            if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
            {
                creditCard = (CreditCardsVO) payment.getCreditCards().get(0);
            }

            if(request.getParameter("payment_method_id") != null)
            {
                payment.setPaymentsType(request.getParameter("payment_method_id"));
                creditCard.setCCType(request.getParameter("payment_method_id"));
            }
            if(request.getParameter("cc_number") != null &&  ! request.getParameter("cc_number").matches("\\*{12}.*"))
            {
                creditCard.setCCNumber(request.getParameter("cc_number"));
                creditCard.setCcNumberChanged(true);
            }
            if(request.getParameter("cc_exp_month") != null && request.getParameter("cc_exp_year") != null)
            {
                try
                {
                    creditCard.setCCExpiration(request.getParameter("cc_exp_month") + "/" + request.getParameter("cc_exp_year").substring(2,4));
                }
                catch(StringIndexOutOfBoundsException e)
                {
                    creditCard.setCCExpiration(request.getParameter("cc_exp_month"));
                }
            }

            creditCardList.add(creditCard);
            payment.setCreditCards(creditCardList);
            paymentList.add(payment);
        }
        else if(request.getParameter("payment_method") != null && request.getParameter("payment_method").equals("I"))
        {
            payment.setPaymentMethodType("I");
            payment.setCreditCardId(0);

            if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
            {
                CreditCardsVO creditCard = (CreditCardsVO) payment.getCreditCards().get(0);
                creditCard.setCCExpiration("");
                creditCard.setCCNumber("");
                creditCard.setCCType("");
            }
            
            if(request.getParameter("payment_method_id") != null)
            {
                payment.setPaymentsType(request.getParameter("payment_method_id"));
            }
            
            paymentList.add(payment);
        }

        order.setPayments(paymentList);

        // Co-Brands
        HashMap coBrandWorkMap = new HashMap();
        CoBrandVO coBrand = null;
        String currentName = null;
        String currentData = null;
        int coBrandCount = 1;

        if(order.getCoBrand() != null)
        {
            for(int i = 0; i < order.getCoBrand().size(); i++)
            {
                coBrand = (CoBrandVO) order.getCoBrand().get(i);
                //coBrand.setInfoData("");

                coBrandWorkMap.put(coBrand.getInfoName(), coBrand);
            }
        }

        // Cycle co-brands to use previously existing keys
        while(true)
        {
            currentName = request.getParameter("cb_name" + coBrandCount);
            currentData = request.getParameter("cb_data" + coBrandCount);

            if(currentName != null && currentData != null)
            {
                if(coBrandWorkMap.containsKey(currentName))
                {
                    coBrand = (CoBrandVO) coBrandWorkMap.get(currentName);
                    coBrand.setInfoData(currentData);
                }
                else
                {
                    coBrand = new CoBrandVO();
                    coBrand.setInfoName(currentName);
                    coBrand.setInfoData(currentData);
                }

                coBrandWorkMap.put(coBrand.getInfoName(), coBrand);
            }
            else 
            {
                break;
            }

            coBrandCount++;
        }

        List coBrandList = new ArrayList(coBrandWorkMap.values());
        order.setCoBrand(coBrandList);
        
        String addressChangedFlag = request.getParameter("address_changed_flag");
        logger.debug("addressChangedFlag: " + addressChangedFlag);
        boolean overrideChanged = false;

        // Items
        OrderDetailsVO item = null;
        String lineNumber = null;
        
        if(order.getOrderDetail() != null)
        {
            List itemList = order.getOrderDetail();
        
            for(int i = 0; i < itemList.size(); i++)
            {
                item = (OrderDetailsVO) itemList.get(i);
                
                lineNumber = item.getLineNumber();
                
                if(request.getParameter("florist_number" + lineNumber) != null)
                {
                    item.setFloristNumber(request.getParameter("florist_number" + lineNumber));
                }
                if(request.getParameter("florist_name" + lineNumber) != null)
                {
                    item.setFloristName(request.getParameter("florist_name" + lineNumber));
                }
                if(request.getParameter("occasion" + lineNumber) != null)
                {
                    item.setOccassionId(request.getParameter("occasion" + lineNumber));
                }
                if(request.getParameter("card_message" + lineNumber) != null)
                {
                    item.setCardMessage(request.getParameter("card_message" + lineNumber));
                }
                if(request.getParameter("card_signature" + lineNumber) != null)
                {
                    item.setCardSignature(request.getParameter("card_signature" + lineNumber));
                }
                if(request.getParameter("special_instructions" + lineNumber) != null)
                {
                    item.setSpecialInstructions(request.getParameter("special_instructions" + lineNumber));
                }
                if(request.getParameter("shipping_method" + lineNumber) != null)
                {
                    item.setShipMethod(request.getParameter("shipping_method" + lineNumber));
                }
                if(request.getParameter("special_instructions" + lineNumber) != null)
                {
                    item.setSpecialInstructions(request.getParameter("special_instructions" + lineNumber));
                }
                if(request.getParameter("delivery_date" + lineNumber) != null)
                {
                    item.setDeliveryDate(request.getParameter("delivery_date" + lineNumber));
                }
                if(request.getParameter("item_source_code" + lineNumber) != null)
                {
                    item.setSourceCode(request.getParameter("item_source_code" + lineNumber));
                }
                if(request.getParameter("item_source_description" + lineNumber) != null)
                {
                    item.setSourceDescription(request.getParameter("item_source_description" + lineNumber));
                }
                if(request.getParameter("iow_flag" + lineNumber) != null)
                {
                    item.setItemOfTheWeekFlag(request.getParameter("iow_flag" + lineNumber));
                }
                //price override flag only applies to Amazon orders only
                if(isWalmartOrder || isAmazonOrder || isMercentOrder || isPartnerOrder)
                {
                  if(request.getParameter("price_override_flag" + lineNumber) != null)
                  {
                     item.setPriceOverrideFlag("Y");
                  }
                }
                if (clearMilesPointsPayment) {
                    item.setMilesPointsAmt("");
                }
                if (request.getParameter("bin_source_changed_flag" + lineNumber) != null) {
                    item.setBinSourceChangedFlag(request.getParameter("bin_source_changed_flag" + lineNumber));
                }
                
                // #11946 - Item Morning delivery opted            
                if(request.getParameter("morning_delivery" + lineNumber) != null && 
                		"on".equalsIgnoreCase(request.getParameter("morning_delivery" + lineNumber))) {
                	logger.debug("#11946 - BBN flag set to true indicating customer opted for BBN on order scrub");
                	item.setMorningDeliveryOpted("Y");
                } else {
                	logger.debug("#11946 - BBN flag set to false indicating customer did not opt for BBN on order scrub");
                	item.setMorningDeliveryOpted("N");
                }
                
				// #defect 12883/12952/12908 - Source code when changed, 
				// should be updated in the order details object before recalculation is performed in DataMassager.				
				if (isSCChangeRequest) {
					// don't change the source code if it was already changed by
					// bin processing skip if old flag is N and new flag is Y
					String oldBinFlag = request.getParameter("bin_source_changed_flag" + item.getLineNumber());
					String newBinFlag = item.getBinSourceChangedFlag();
					if(logger.isDebugEnabled()) {
						logger.debug("bin flags - old: " + oldBinFlag + " new: " + newBinFlag);
					}

					if (oldBinFlag != null && oldBinFlag.equals("N") && newBinFlag != null && newBinFlag.equals("Y")) {
						if(logger.isDebugEnabled()) {
							logger.debug("Source code was changed by bin processing, ignoring");
						}
					} else {
						if (item != null && item.getItemOfTheWeekFlag() != null && item.getItemOfTheWeekFlag().equals("Y")) {
							if(logger.isDebugEnabled()) {
								logger.debug("IOTW item : not updating source code");
							}
						} else {
							item.setSourceCode(request.getParameter("source_code"));
							item.setSourceDescription(request.getParameter("source_code_description"));
						}
					}
				}

                // Recipient
                List recipientList = new ArrayList();
                recipientList.add(this.getRecipient(item, request, lineNumber));
                item.setRecipients(recipientList);

                // AVS address
                if (!overrideChanged){
                    String itemOverride = GeneralConstants.NO;
                    if (item.getAvsAddress() != null)
                        itemOverride = item.getAvsAddress().getOverrideflag();
                    String requestOverride = request.getParameter("qms_override_flag" + lineNumber);
                    if ((requestOverride != null && itemOverride.equals(GeneralConstants.NO)) ||
                            (requestOverride == null && itemOverride.equals(GeneralConstants.YES)))
                        overrideChanged = true;
                }
                
                if (overrideChanged || (addressChangedFlag != null && addressChangedFlag.equals("Y")))
                    item.setAvsAddress(this.getAVSAddress(item, request, lineNumber));
            }
        }

        // Validate and update avs address
        if (overrideChanged || (addressChangedFlag != null && addressChangedFlag.equals("Y"))) {
            logger.debug("Calling qmsValidation");
            QMSValidation qmsValidation = new QMSValidation();
            qmsValidation.qmsAddress(order);

            logger.debug("shipMethodCarrierFlag: " + item.getShipMethodCarrierFlag());
            if (item.getShipMethodCarrierFlag() != null && item.getShipMethodCarrierFlag().equals("Y")) {
                DataAccessUtil dau = DataAccessUtil.getInstance();
                DataRequest dataRequest = new DataRequest();
                dataRequest.setConnection(connection);
                dataRequest.addInputParam("IN_APPLICATION_NAME", "QMS");
                dataRequest.setStatementID(INSERT_APP_COUNTER);
                Map outputs = (Map) dau.execute(dataRequest);
                dataRequest.reset();
            }

        }

        // Massage user data to fit scrub
        DataMassager dataMassager = new DataMassager();
        dataMassager.massageOrderData(order, connection);
        
        return order;
    }

    private BuyerVO getBuyer(OrderVO order, HttpServletRequest request, String origin)
    {
        BuyerVO buyer = null;
        
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO) order.getBuyer().get(0);
        }
        else
        {
            buyer = new BuyerVO();
        }

        //get lock value.  Locking added for PhaseIII.  emueller, 4/20/05
        if(request.getParameter("buyer_locked_by") != null)
        {
            buyer.setLockedBy(request.getParameter("buyer_locked_by"));
        }

        if(request.getParameter("buyer_first_name") != null)
        {
            buyer.setFirstName(request.getParameter("buyer_first_name"));
        }
        if(request.getParameter("buyer_last_name") != null)
        {
            buyer.setLastName(request.getParameter("buyer_last_name"));
        }

        // Buyer address
        List addressList = new ArrayList();
        addressList.add(this.getBuyerAddress(buyer, request, origin));
        buyer.setBuyerAddresses(addressList);

        // Buyer phone numbers
        List buyerPhoneList = new ArrayList();
        if(request.getParameter("buyer_evening_phone") != null)
        {
            BuyerPhonesVO buyerEveningPhone = this.getBuyerPhone(buyer, "HOME");
            buyerEveningPhone.setPhoneNumber(request.getParameter("buyer_evening_phone"));    
            buyerEveningPhone.setExtension(request.getParameter("buyer_home_ext"));

            buyerPhoneList.add(buyerEveningPhone);
        }

        if(request.getParameter("buyer_daytime_phone") != null)
        {
            BuyerPhonesVO buyerDaytimePhone = this.getBuyerPhone(buyer, "WORK");
            buyerDaytimePhone.setPhoneNumber(request.getParameter("buyer_daytime_phone"));    
            buyerDaytimePhone.setExtension(request.getParameter("buyer_work_ext"));

            buyerPhoneList.add(buyerDaytimePhone);
        }

        if(request.getParameter("buyer_fax") != null)
        {
            BuyerPhonesVO buyerFax = this.getBuyerPhone(buyer, "FAX");
            buyerFax.setPhoneNumber(request.getParameter("buyer_fax")); 

            buyerPhoneList.add(buyerFax);
        }
        
        buyer.setBuyerPhones(buyerPhoneList);

        // Buyer emails
        List buyerEmailList = new ArrayList();
        if(request.getParameter("buyer_email_address") != null)
        {
            BuyerEmailsVO buyerEmail = this.getBuyerEmail(buyer, request);
            buyerEmailList.add(buyerEmail);
            order.setBuyerEmailAddress(StringUtils.deleteWhitespace(request.getParameter("buyer_email_address")));
        }
        
        buyer.setBuyerEmails(buyerEmailList);

        return buyer;
    }

    private BuyerPhonesVO getBuyerPhone(BuyerVO buyer, String phoneType)
    {
        BuyerPhonesVO buyerPhone = null;
        BuyerPhonesVO currentBuyerPhone = null;
        
        if(buyer.getBuyerPhones() != null)
        {
            Iterator buyerPhoneIterator = buyer.getBuyerPhones().iterator();
            while(buyerPhoneIterator.hasNext())
            {
                currentBuyerPhone = (BuyerPhonesVO) buyerPhoneIterator.next();
                if(currentBuyerPhone.getPhoneType() != null && currentBuyerPhone.getPhoneType().equals(phoneType))
                {
                    buyerPhone = currentBuyerPhone;
                }
            }
        }

        if(buyerPhone == null)
        {
            buyerPhone = new BuyerPhonesVO();
            buyerPhone.setPhoneType(phoneType);
        }

        return buyerPhone;
    }

    private RecipientsVO getRecipient(OrderDetailsVO item, HttpServletRequest request, String lineNumber)
    {
        RecipientsVO recipient = null;
    
        if(item.getRecipients() != null && item.getRecipients().size() > 0)
        {
            recipient = (RecipientsVO) item.getRecipients().get(0);
        }
        else
        {
            recipient = new RecipientsVO();
        }

        if(request.getParameter("recip_first_name" + lineNumber) != null)
        {
            recipient.setFirstName(request.getParameter("recip_first_name" + lineNumber));
        }
        if(request.getParameter("recip_last_name" + lineNumber) != null)
        {
            recipient.setLastName(request.getParameter("recip_last_name" + lineNumber));
        }

        // Recipient address
        List addressList = new ArrayList();
        addressList.add(this.getRecipientAddress(recipient, request, lineNumber));
        recipient.setRecipientAddresses(addressList);

        if(request.getParameter("recip_phone" + lineNumber) != null)
        {
            List recipientPhoneList = new ArrayList();
                        
            RecipientPhonesVO recipientPhone = this.getRecipientPhone(recipient, "WORK");
            if(recipientPhone == null || StringUtils.isEmpty(recipientPhone.getPhoneNumber())) {
            	 recipientPhone = this.getRecipientPhone(recipient, "HOME");
            }
            recipientPhone.setPhoneNumber(request.getParameter("recip_phone" + lineNumber));    
            recipientPhone.setExtension(request.getParameter("recip_phone_ext" + lineNumber));

            recipientPhoneList.add(recipientPhone);
            recipient.setRecipientPhones(recipientPhoneList);
        }

        return recipient;
    }

    private DestinationsVO getDestination(RecipientsVO recipient, HttpServletRequest request, String lineNumber)
    {
        DestinationsVO destination = null;
        
        if(recipient.getDestinations() != null && recipient.getDestinations().size() > 0)
        {
            destination = (DestinationsVO) recipient.getDestinations().get(0);
        }
        else
        {
            destination = new DestinationsVO();
        }

        if(request.getParameter("ship_to_type" + lineNumber) != null)
        {
            destination.setDestinationType(request.getParameter("ship_to_type" + lineNumber));
        }
        if(request.getParameter("ship_to_type_name" + lineNumber) != null)
        {
            destination.setName(request.getParameter("ship_to_type_name" + lineNumber));
        }
        if(request.getParameter("ship_to_type_info" + lineNumber) != null)
        {
            destination.setInfo(request.getParameter("ship_to_type_info" + lineNumber));
        }

        return destination;
    }

    private RecipientPhonesVO getRecipientPhone(RecipientsVO recipient, String phoneType)
    {
        RecipientPhonesVO recipientPhone = null;
        RecipientPhonesVO currentRecipientPhone = null;
        
        if(recipient.getRecipientPhones() != null)
        {
            Iterator recipientPhoneIterator = recipient.getRecipientPhones().iterator();
            while(recipientPhoneIterator.hasNext())
            {
                currentRecipientPhone = (RecipientPhonesVO) recipientPhoneIterator.next();
                if(currentRecipientPhone.getPhoneType() != null && currentRecipientPhone.getPhoneType().equals(phoneType))
                {
                    recipientPhone = currentRecipientPhone;
                }
            }
        }

        if(recipientPhone == null)
        {
            recipientPhone = new RecipientPhonesVO();
            recipientPhone.setPhoneType(phoneType);
        }

        return recipientPhone;
    }

    private FraudCommentsVO getFraudComments(OrderVO order, HttpServletRequest request)
    {
        FraudCommentsVO fraudComments = null;
    
        if(request.getParameter("fraud_id") != null && !request.getParameter("fraud_id").equals(""))
        {
            fraudComments = new FraudCommentsVO();
        
            if(request.getParameter("fraud_id") != null)
            {
                fraudComments.setFraudID(request.getParameter("fraud_id"));
            }
            if(request.getParameter("fraud_comments") != null)
            {
                fraudComments.setCommentText(request.getParameter("fraud_comments"));
            }
        }

        return fraudComments;
    }

    private OrderContactInfoVO getContactInfo(OrderVO order, HttpServletRequest request)
    {
        OrderContactInfoVO contactInfo = null;
    
        if(order.getOrderContactInfo() != null && order.getOrderContactInfo().size() > 0)
        {
            contactInfo = (OrderContactInfoVO) order.getOrderContactInfo().get(0);
            contactInfo.setFirstName("");
            contactInfo.setLastName("");
            contactInfo.setEmail("");
            contactInfo.setPhone("");
            contactInfo.setExt("");
        }
        else if(order.getOrderOrigin() != null && order.getOrderOrigin().equalsIgnoreCase("bulk"))
        {
            contactInfo = new OrderContactInfoVO();
        }

        if(contactInfo != null)
        {
            if(request.getParameter("contact_first_name") != null)
            {
                contactInfo.setFirstName(request.getParameter("contact_first_name"));
            }
            if(request.getParameter("contact_last_name") != null)
            {
                contactInfo.setLastName(request.getParameter("contact_last_name"));
            }
            if(request.getParameter("contact_phone") != null)
            {
                contactInfo.setPhone(request.getParameter("contact_phone"));
            }
            if(request.getParameter("contact_phone_ext") != null)
            {
                contactInfo.setExt(request.getParameter("contact_phone_ext"));
            }
            if(request.getParameter("contact_email") != null)
            {
                contactInfo.setEmail(request.getParameter("contact_email"));
            }
        }

        return contactInfo;
    }

    private MembershipsVO getMembership(OrderVO order, HttpServletRequest request)
    {
        MembershipsVO membership = null;
    
        if(order.getMemberships() != null && order.getMemberships().size() > 0)
        {
            membership = (MembershipsVO) order.getMemberships().get(0);
            membership.setFirstName("");
            membership.setLastName("");
            membership.setMembershipIdNumber("");
        }
        else if(order.getPartnerId() != null && !order.getPartnerId().equals(""))
        {
            membership = new MembershipsVO();
        }

        if(membership != null)
        { 
            membership.setMembershipType(order.getPartnerId());
        
            if(request.getParameter("membership_id") != null)
            {
                membership.setMembershipIdNumber(request.getParameter("membership_id"));
            }
            if(request.getParameter("membership_first_name") != null)
            {
                membership.setFirstName(request.getParameter("membership_first_name"));
            }
            if(request.getParameter("membership_last_name") != null)
            {
                membership.setLastName(request.getParameter("membership_last_name"));
            }
        }
        
        return membership;
    }

    private BuyerAddressesVO getBuyerAddress(BuyerVO buyer, HttpServletRequest request, String origin)
    {
        BuyerAddressesVO buyerAddress = null;
        
        if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
        {   
            buyerAddress = (BuyerAddressesVO) buyer.getBuyerAddresses().get(0);
        }

        if(buyerAddress == null)
        {
            buyerAddress = new BuyerAddressesVO();
        }

        if(request.getParameter("buyer_address1") != null)
        {
            buyerAddress.setAddressLine1(request.getParameter("buyer_address1"));
        }
        if(request.getParameter("buyer_address2") != null)
        {
            buyerAddress.setAddressLine2(request.getParameter("buyer_address2"));
        }
        if(request.getParameter("buyer_city") != null)
        {
            buyerAddress.setCity(request.getParameter("buyer_city"));
        }
        if(request.getParameter("buyer_state") != null)
        {
            buyerAddress.setStateProv(request.getParameter("buyer_state"));
        }
        if(request.getParameter("buyer_postal_code") != null)
        {
            buyerAddress.setPostalCode(request.getParameter("buyer_postal_code"));
        }
        if(request.getParameter("buyer_country") != null)
        {
            buyerAddress.setCountry(request.getParameter("buyer_country"));
        }
        if(request.getParameter("buyer_address_etc") != null)
        {
            buyerAddress.setAddressEtc(request.getParameter("buyer_address_etc"));
        }

        return buyerAddress;
    }

    private BuyerEmailsVO getBuyerEmail(BuyerVO buyer, HttpServletRequest request)
    {
        BuyerEmailsVO buyerEmail = null;
        BuyerEmailsVO currentBuyerEmail = null;
        
        if(buyer.getBuyerEmails() != null)
        {  
            Iterator buyerEmailIterator = buyer.getBuyerEmails().iterator();
            while(buyerEmailIterator.hasNext())
            {
                currentBuyerEmail = (BuyerEmailsVO) buyerEmailIterator.next();
                // defect 3480
                if(buyerEmail == null) {
                    buyerEmail = currentBuyerEmail;
                    buyerEmail.setPrimary("Y");
                }
                
                if(currentBuyerEmail.getPrimary() != null && currentBuyerEmail.getPrimary().equals("Y"))
                {
                    buyerEmail = currentBuyerEmail;
                }
            }
        }

        if(buyerEmail == null)
        {
            buyerEmail = new BuyerEmailsVO();
            buyerEmail.setPrimary("Y");
        }

        buyerEmail.setEmail(StringUtils.deleteWhitespace(request.getParameter("buyer_email_address")));
        buyerEmail.setNewsletter("Y");

        return buyerEmail;
    }

    private RecipientAddressesVO getRecipientAddress(RecipientsVO recipient, HttpServletRequest request, String lineNumber)
    {
        RecipientAddressesVO recipientAddress = null;

        if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
        {   
            recipientAddress = (RecipientAddressesVO) recipient.getRecipientAddresses().get(0);
        }

        if(recipientAddress == null)
        {
            recipientAddress = new RecipientAddressesVO();
        }

        if(request.getParameter("ship_to_type" + lineNumber) != null)
        {
            recipientAddress.setAddressType(request.getParameter("ship_to_type" + lineNumber));
        }
        if(request.getParameter("ship_to_type_name" + lineNumber) != null)
        {
            recipientAddress.setName(request.getParameter("ship_to_type_name" + lineNumber));
        }
        if(request.getParameter("ship_to_type_info" + lineNumber) != null)
        {
            recipientAddress.setInfo(request.getParameter("ship_to_type_info" + lineNumber));
        }
        
        if(request.getParameter("recip_address1" + lineNumber) != null)
        {
            if(request.getParameter("recip_add_too_long_flag" + lineNumber) != null &&
               request.getParameter("recip_add_too_long_flag" + lineNumber).equals("true") &&
               request.getParameter("recip_address1" + lineNumber).equals(""))
            {
                // Leave address as is and do not override with empty string
            }
            else
            {
                recipientAddress.setAddressLine1(request.getParameter("recip_address1" + lineNumber));
            }
        }
        if(request.getParameter("recip_address2" + lineNumber) != null)
        {
            if(request.getParameter("recip_add_too_long_flag" + lineNumber) != null &&
               request.getParameter("recip_add_too_long_flag" + lineNumber).equals("true") &&
               request.getParameter("recip_address2" + lineNumber).equals(""))
            {
                // Leave address as is and do not override with empty string
            }
            else
            {
                recipientAddress.setAddressLine2(request.getParameter("recip_address2" + lineNumber));
            }
        }
        if(request.getParameter("recip_city" + lineNumber) != null)
        {
            recipientAddress.setCity(request.getParameter("recip_city" + lineNumber));
        }
        if(request.getParameter("recip_state" + lineNumber) != null)
        {
            recipientAddress.setStateProvince(request.getParameter("recip_state" + lineNumber));
        }
        if(request.getParameter("recip_postal_code" + lineNumber) != null)
        {
            recipientAddress.setPostalCode(request.getParameter("recip_postal_code" + lineNumber));
        }
        if(request.getParameter("recip_country" + lineNumber) != null)
        {
            recipientAddress.setCountry(request.getParameter("recip_country" + lineNumber));
        }
        if(request.getParameter("recip_international" + lineNumber) != null)
        {
            recipientAddress.setInternational(request.getParameter("recip_international" + lineNumber));
        }

        return recipientAddress;
    }

    private AVSAddressVO getAVSAddress(OrderDetailsVO item, HttpServletRequest request, String lineNumber)
    {
        AVSAddressVO avsAddress = item.getAvsAddress();

        // always create a new AVS address prior to calling QMS - it will only have the override flag initialized
        avsAddress = new AVSAddressVO();

        if(request.getParameter("qms_override_flag" + lineNumber) != null)
        {
           avsAddress.setOverrideFlag(GeneralConstants.AVS_YES);
        }
        else
        {
            avsAddress.setOverrideFlag(GeneralConstants.AVS_NO);
        }
        
        return avsAddress;
    }

  private Boolean[] paymentChanged(PaymentsVO payment, HttpServletRequest request)
  {
    boolean isSame = true;
    boolean vResultForWI = true;
      
    List cardList = payment.getCreditCards();
    if(cardList == null || cardList.size() <= 0)
    {
    	isSame = false;
    	vResultForWI = false;
    }
    else {
    CreditCardsVO card = (CreditCardsVO)payment.getCreditCards().get(0);

    String requestNumber = request.getParameter("cc_number");
    String requestMonth = request.getParameter("cc_exp_month");
    String requestYear = request.getParameter("cc_exp_year");
    String requestType = request.getParameter("payment_method_id");

    if(requestMonth == null)
    {
      requestMonth = "";
    }
    if(requestYear == null)
    {
      requestYear = "";
    }
    else
    {
      if(requestYear.length() >= 4)
      {
        requestYear = requestYear.substring(2,4);        
      }
    }
    
    String requestExpDate = requestMonth + "/" + requestYear;

    if(! requestNumber.matches("\\*{12}.*") && !checkEqual(requestNumber,card.getCCNumber())) {
      isSame = false;
    }else if(!checkEqual(requestType,card.getCCType())){
      isSame = false;
      vResultForWI = false;
    }else if(!checkEqual(requestExpDate,card.getCCExpiration())){
      isSame = false;
      vResultForWI = false;
    }
    }
    Boolean retValResult[]={isSame,vResultForWI};   
    return retValResult;
  }

  private boolean checkEqual(String value1, String value2)
  {
    if(value1 == null)
    {
      value1 = "";
    }
    if(value2 == null)
    {
      value2 = "";
    }
    return value1.equals(value2);
  }

}