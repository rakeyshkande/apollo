package com.ftd.osp.orderscrub.presentation.util;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.dao.RecalculateOrderDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.pac.util.PACUtil;
import com.ftd.security.SecurityManager;

/**
 * This class builds the general dynamic data for the shopping cart (ie states, 
 * countries, occasions, etc).  It also passes along any data that needs to be 
 * retained throughout all cart submits such as search data.
 *
 * @author Brian Munter
 */
 
public class BaseCartBuilder 
{
    private Logger logger;

    /** 
     * Constructor
     */
    public BaseCartBuilder()
    {
        logger = new Logger("com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder");
    }

    /**
     * Primary method which loads all generic data for the cart.
     * 
     * @param order OrderVO
     * @param dataRequest DataRequest
     * @param request HttpServletRequest
     * @param cartLevel boolean
     * @exception Exception
     */
    public NodeList loadGenericCartData(OrderVO order, DataRequest dataRequest, HttpServletRequest request, boolean cartLevel) throws Exception
    {
        Document cartDocument = DOMUtil.getDocument();

        if(cartLevel)
        {
            // Load delivery types for cart items
            this.generateItemData(cartDocument, order, dataRequest);

            // Set the membership flag based on partner id
            Element membershipFlagElement = cartDocument.createElement("membership_flag");
            if(order.getPartnerId() != null && !order.getPartnerId().equals(""))
            {
                membershipFlagElement.appendChild(cartDocument.createTextNode("Y"));
            }
            else
            {
                membershipFlagElement.appendChild(cartDocument.createTextNode("N"));
            }
            
            Element vipCustomerElement = cartDocument.createElement("vip_customer_flag");
            dataRequest.reset();
            dataRequest.setStatementID("IS_VIP_CUSTOMER_ORDER");
            dataRequest.addInputParam("IN_ORDER_GUID", order.getGUID());
			String vipCustomer = (String) DataAccessUtil.getInstance().execute(dataRequest);
            if("Y".equalsIgnoreCase(vipCustomer))
            {
            	vipCustomerElement.appendChild(cartDocument.createTextNode("Y"));
            }else
            {	
            	vipCustomerElement.appendChild(cartDocument.createTextNode("N"));
            }
            
            Element isGCGDInfoValid = cartDocument.createElement("is_gcgd_valid");
            isGCGDInfoValid.appendChild(cartDocument.createTextNode(request.getParameter("is_gcgd_valid")));
        
            cartDocument.getFirstChild().appendChild(cartDocument.importNode(membershipFlagElement, true));
            cartDocument.getFirstChild().appendChild(cartDocument.importNode(isGCGDInfoValid, true));
            cartDocument.getFirstChild().appendChild(cartDocument.importNode(vipCustomerElement, true));
            
            // Load co brand data
            dataRequest.reset();
            dataRequest.setStatementID("BILLING_INFO_LOOKUP");
            dataRequest.addInputParam("source_code", order.getSourceCode());
            DOMUtil.addSection(cartDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load co brand options data (e.g., static cost center values)
            dataRequest.reset();
            dataRequest.setStatementID("BILLING_INFO_OPTIONS_LOOKUP");
            dataRequest.addInputParam("source_code", order.getSourceCode());
            DOMUtil.addSection(cartDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load occasions
            dataRequest.reset();
            dataRequest.setStatementID("OCCASION_LIST_LOOKUP");
            DOMUtil.addSection(cartDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            //Load states
            dataRequest.reset();
            dataRequest.setStatementID("STATE_LIST_LOOKUP");
            DOMUtil.addSection(cartDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load countries
            dataRequest.reset();
            dataRequest.setStatementID("COUNTRY_LIST_LOOKUP");
            DOMUtil.addSection(cartDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load address types
            dataRequest.reset();
            dataRequest.setStatementID("ADDRESS_TYPES_LOOKUP");
            DOMUtil.addSection(cartDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
        }

        // Load search criteria        
        DOMUtil.addSection(cartDocument, "searchCriteria", "criteria", this.generateSearchCriteria(order, request), true);

        // Load security data       
        DOMUtil.addSection(cartDocument, "security", "data", this.generateSecurityData(request, dataRequest.getConnection()), true);
 
        // Add origin comparision codes to xml
        HashMap originationCodes = new HashMap();
        originationCodes.put("amazon_origin_internet", 
                     ConfigurationUtil.getInstance().getProperty(
                            ConfigurationConstants.SCRUB_CONFIG_FILE, 
                            ConfigurationConstants.AMAZON_ORIGIN_INTERNET));
        originationCodes.put("mercent_origin_internet", 
                ConfigurationUtil.getInstance().getProperty(
                       ConfigurationConstants.SCRUB_CONFIG_FILE, 
                       ConfigurationConstants.MERCENT_ORIGIN_INTERNET));
        originationCodes.put("walmart_origin_internet", 
                     ConfigurationUtil.getInstance().getProperty(
                            ConfigurationConstants.SCRUB_CONFIG_FILE, 
                            ConfigurationConstants.WALMART_ORIGIN_INTERNET));
      originationCodes.put("cspi_origin_internet", 
                   ConfigurationUtil.getInstance().getProperty(
                          ConfigurationConstants.SCRUB_CONFIG_FILE, 
                          ConfigurationConstants.CSPI_ORIGIN_INTERNET));                            
                            
        DOMUtil.addSection(cartDocument, "originationCodes", "data", originationCodes, true);
            
        return cartDocument.getFirstChild().getChildNodes();
    }

    private void generateItemData(Document cartDocument, OrderVO order, DataRequest dataRequest) throws Exception
    {
        Element productElement = null;
        Element lineNumberElement = null;
        Element shipDataRootElement = null;

        String country = null;
        
        List itemList = order.getOrderDetail();
        for (int i = 0; i < itemList.size(); i++) 
        {
            OrderDetailsVO item = (OrderDetailsVO) itemList.get(i);

            productElement = cartDocument.createElement("product_data");
            lineNumberElement = cartDocument.createElement("line_number");
            shipDataRootElement = cartDocument.createElement("root");

            if((item.getShipMethodCarrierFlag() != null && !item.getShipMethodCarrierFlag().equals(""))
                || (item.getShipMethodFloristFlag() != null && !item.getShipMethodFloristFlag().equals("")))
            {
                productElement.setAttribute("carrierDeliveryFlag", item.getShipMethodCarrierFlag());
                productElement.setAttribute("floristDeliveryFlag", item.getShipMethodFloristFlag());
            }
            else
            {
                logger.error("Invalid delivery flags for product: " + item.getProductId());
            }

            // Check domestic flag
            OrderHelper orderHelper = new OrderHelper();
            RecipientAddressesVO recipientAddress = orderHelper.getRecipientAddress(item);
            
            if(this.isDomestic(recipientAddress.getCountry(), dataRequest))
            {
                productElement.setAttribute("domesticFlag", "Y");
            }
            else
            {
                productElement.setAttribute("domesticFlag", "N");
            }
            

            lineNumberElement.appendChild(cartDocument.createTextNode(item.getLineNumber()));
            productElement.appendChild(lineNumberElement);
            
            
            RecalculateOrderDAO recalculateOrderDAO = new RecalculateOrderDAO();
            ProductVO productVO = recalculateOrderDAO.getProduct(dataRequest.getConnection(), item.getProductId());
            PACUtil pacUtil = new PACUtil();
            boolean isPCOrder = false;
            if(!StringUtils.isEmpty(item.getFtdwAllowedShipping())) {
            	 productElement.appendChild(this.generateFTDWShipMethod(cartDocument, item.getFtdwAllowedShipping()));
            } else if(PACUtil.isFTDWOrder(productVO.getShippingSystem(), productVO.getProductType(), item.getShipMethod())) { // if Recal did not happen/ Edit product overrides PAC ship method set to VO/ Edit source code etc
            	String ftdWshipMethod = null; 
            	if(!item.isApplyOrigFee()) {
	            	if (productVO != null && !StringUtils.isEmpty(productVO.getPersonalizationTemplate())) {
						isPCOrder = true;
					}
	            	ftdWshipMethod = pacUtil.getFTDWShipmethod(item, order.getOrderDateTime(), productVO.getNovatorId(), isPCOrder);
            	}
            	if(StringUtils.isEmpty(ftdWshipMethod)) {
            		logger.info("FTDW shipping method cannot be calculated - Displaying Item ship emthod if it is not null");
            		ftdWshipMethod = item.getShipMethod();
            	}
            	productElement.appendChild(this.generateFTDWShipMethod(cartDocument, ftdWshipMethod));
            } else {
	            dataRequest.reset();
	            dataRequest.setStatementID("SHIP_METHODS_BY_PRODUCT_ID");
	            dataRequest.addInputParam("product_id", item.getProductId());
	            productElement.appendChild(cartDocument.importNode(((Document) DataAccessUtil.getInstance().execute(dataRequest)).getFirstChild(), true));
            }

            // Add additional ship method for same day gift
            if((item.getProductType() != null)
                && (item.getProductType().equals("SDG") || item.getProductType().equals("SDFC"))
                && (item.getShipMethodFloristFlag() != null && item.getShipMethodFloristFlag().equals("Y")))
            {
                productElement.appendChild(this.generateSameDayElement(cartDocument));
            }

            shipDataRootElement.appendChild(productElement);

            DOMUtil.addSection(cartDocument, shipDataRootElement.getChildNodes()); 
        }
    }

    private Node generateFTDWShipMethod(Document cartDocument, String shipMethod) {  
    	
    	Element shipMethodListElement = cartDocument.createElement("shipMethodList");
        Element shipMethodElement = cartDocument.createElement("shipMethod");
        Element shipMethodIDElement = cartDocument.createElement("ship_method_id");
        Element shipMethodDescriptionElement = cartDocument.createElement("description");
        shipMethodIDElement.appendChild(cartDocument.createTextNode(shipMethod));
        
		if (GeneralConstants.DELIVERY_NEXT_DAY_CODE.equals(shipMethod)) {
			shipMethodDescriptionElement.appendChild(cartDocument.createTextNode(GeneralConstants.DELIVERY_NEXT_DAY));
		} else if (GeneralConstants.DELIVERY_TWO_DAY_CODE.equals(shipMethod)) {
			shipMethodDescriptionElement.appendChild(cartDocument.createTextNode(GeneralConstants.DELIVERY_TWO_DAY));
		} else if (GeneralConstants.DELIVERY_STANDARD_CODE.equals(shipMethod)) {
			shipMethodDescriptionElement.appendChild(cartDocument.createTextNode(GeneralConstants.DELIVERY_STANDARD));
		} else if (GeneralConstants.DELIVERY_SATURDAY_CODE.equals(shipMethod)) {
			shipMethodDescriptionElement.appendChild(cartDocument.createTextNode(GeneralConstants.DELIVERY_SATURDAY));
		} else if (GeneralConstants.DELIVERY_SUNDAY_CODE.equals(shipMethod)) {
			shipMethodDescriptionElement.appendChild(cartDocument.createTextNode(GeneralConstants.DELIVERY_SUNDAY));
		}
             
        shipMethodElement.appendChild(shipMethodIDElement);
        shipMethodElement.appendChild(shipMethodDescriptionElement);
        shipMethodListElement.appendChild(shipMethodElement);

        return shipMethodListElement;
	}

	private HashMap generateSearchCriteria(OrderVO order, HttpServletRequest request)
    {
        HashMap searchCriteriaMap = new HashMap();
        
        searchCriteriaMap.put("sc_email_address", request.getParameter("sc_email_address"));
        searchCriteriaMap.put("sc_product_code", request.getParameter("sc_product_code"));
        searchCriteriaMap.put("sc_ship_to_type", request.getParameter("sc_ship_to_type"));
        searchCriteriaMap.put("sc_confirmation_number", request.getParameter("sc_confirmation_number"));
        searchCriteriaMap.put("sc_pro_confirmation_number", request.getParameter("sc_pro_confirmation_number"));
        searchCriteriaMap.put("sc_origin", request.getParameter("sc_origin"));
        searchCriteriaMap.put("sc_date_flag", request.getParameter("sc_date_flag"));
        searchCriteriaMap.put("sc_date", request.getParameter("sc_date"));
        searchCriteriaMap.put("sc_last_name_flag", request.getParameter("sc_last_name_flag"));
        searchCriteriaMap.put("sc_last_name", request.getParameter("sc_last_name"));
        searchCriteriaMap.put("sc_phone_flag", request.getParameter("sc_phone_flag"));
        searchCriteriaMap.put("sc_phone", request.getParameter("sc_phone"));
        searchCriteriaMap.put("sc_csr_id", request.getParameter("sc_csr_id"));
        searchCriteriaMap.put("sc_mode", request.getParameter("sc_mode"));
        searchCriteriaMap.put("sc_date_from", request.getParameter("sc_date_from"));
        searchCriteriaMap.put("sc_date_to", request.getParameter("sc_date_to"));
        searchCriteriaMap.put("sc_search_type", request.getParameter("sc_search_type"));
        searchCriteriaMap.put("sc_source_code", request.getParameter("sc_source_code"));
        searchCriteriaMap.put("sc_scrub_reason", request.getParameter("sc_scrub_reason"));
        searchCriteriaMap.put("sc_product_property", request.getParameter("sc_product_property"));
        searchCriteriaMap.put("sc_preferred_partner", request.getParameter("sc_preferred_partner"));
        searchCriteriaMap.put("sc_pc_membership_id", request.getParameter("sc_pc_membership_id"));
        searchCriteriaMap.put("sc_hide_test_orders", request.getParameter("sc_hide_test_orders"));

        return searchCriteriaMap;
    }

    private Element generateSameDayElement(Document cartDocument)
    {
        Element shipMethodListElement = cartDocument.createElement("shipMethodList");
        Element shipMethodElement = cartDocument.createElement("shipMethod");
        Element shipMethodIDElement = cartDocument.createElement("ship_method_id");
        Element shipMethodDescriptionElement = cartDocument.createElement("description");

        shipMethodIDElement.appendChild(cartDocument.createTextNode("SD"));
        shipMethodDescriptionElement.appendChild(cartDocument.createTextNode("Florist Delivery"));

        shipMethodElement.appendChild(shipMethodIDElement);
        shipMethodElement.appendChild(shipMethodDescriptionElement);
        shipMethodListElement.appendChild(shipMethodElement);

        return shipMethodListElement;
    }

    public boolean isDomestic(String country, DataRequest dataRequest) throws Exception
    {
        boolean domesticFlag = false;

        dataRequest.reset();
        dataRequest.setStatementID("GET_COUNTRY_MASTER_BY_ID");
        dataRequest.addInputParam("IN_COUNTRY_ID", country);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);    
        
        if(rs == null || !rs.next())
        {
            // Default to domsetic
            domesticFlag = true;
        }
        else
        {
            // Get value based on what is in DB
            String domValue = (rs.getObject(2)).toString();    
            
            if(domValue == null || domValue.equals("D"))
            {
              domesticFlag = true;
            }
        }
      
        return domesticFlag;
    }
    
    private HashMap generateSecurityData(HttpServletRequest request, Connection con)
    {
        HashMap securityDataMap = new HashMap();
        String vipCustUpdateDeniedMessage = null;
        String vipCustomerMessage = null;
        securityDataMap.put("context", request.getParameter("context"));
        securityDataMap.put("securitytoken", request.getParameter("securitytoken"));
        try {
			if(SecurityManager.getInstance().assertPermission(request.getParameter("context"), request.getParameter("securitytoken"), "VIP Customer Update", "Yes"))
			{
				securityDataMap.put("vipCustUpdatePermission", "Y");
			}
			else
			{
				securityDataMap.put("vipCustUpdatePermission", "N");
			}
			vipCustUpdateDeniedMessage = ConfigurationUtil.getInstance().getContentWithFilter(con, "VIP_CUSTOMER", "CUSTOMER_ORDER_MANAGEMENT", "VIP_CUSTOMER_UPDATE_DENIED_MESSAGE", null);
			securityDataMap.put("vipCustUpdateDeniedMessage", vipCustUpdateDeniedMessage);
			
			vipCustomerMessage = ConfigurationUtil.getInstance().getContentWithFilter(con, "VIP_CUSTOMER", "CUSTOMER_ORDER_MANAGEMENT", "VIP_CUSTOMER_MESSAGE", null);
			securityDataMap.put("vipCustomerMessage", vipCustomerMessage);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return securityDataMap;
    }

    
}