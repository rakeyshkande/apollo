package com.ftd.osp.orderscrub.presentation.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.orderscrub.constants.SecurityConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PartnerHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.PartnerVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;
import com.ftd.security.exceptions.ExpiredCredentialsException;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

/**
 * This class is used to help facilitate security for Scrub.  All security 
 * conditions and responses are managed within this class.
 *
 * @author Brian Munter
 */

public class SecurityHelper 
{
    private static Logger logger = new Logger("com.ftd.osp.orderscrub.util.SecurityHelper");

    public static final String EMPTY_STRING = "";
    public static final String MSG_VALID = "Valid";
    public static final String MSG_NO_TOKEN = "No security token found";
    public static final String MSG_NO_CONTEXT = "No security context found";
    public static final String MSG_LOGON_FAILURE = "Invalid username and password";
    public static final String MSG_EXPIRED_IDENTIY = "Expired identity";
    public static final String MSG_EXPIRED_SESSION = "Expired session";
    public static final String MSG_INVALID_SESSION = "Invalid session";
    public static final String MSG_GENERAL_EXCEPTION = "General Exception";


    /**
     * Returns true if passed Preferred Partner Resource (e.g., USAA) is associated with the current CSR.
     * 
     * @param request
     * @param preferredResource
     * @return
     */
    public boolean checkPreferredPermissionsForUser(HttpServletRequest request, String preferredResource) {
        String securityToken = request.getParameter("securitytoken");
        try {
            return SecurityManager.getInstance().assertPermission(SecurityConstants.RESOURCE_PREFERRED_PARTNER_CONTEXT, 
                                                                   securityToken, preferredResource, SecurityConstants.PERMISSION_VIEW);
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
    }

    /**
     * Returns list of Preferred Partner names (e.g., USAA) that current CSR has permissions to access
     * 
     * @param request
     * @param preferredResource
     * @return
     */
    public HashSet getPreferredPartnersForUser(HttpServletRequest request) throws Exception {
        HashSet<String> retHash = null;
        
        PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(GeneralConstants.PREFERRED_PARTNER_CACHE);
        HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
        Iterator ppIterator = ppMap.keySet().iterator();
        while(ppIterator.hasNext()) {
            String ppName = (String)ppIterator.next();
            PartnerVO pvo = (PartnerVO)ppMap.get(ppName);
            if (checkPreferredPermissionsForUser(request, pvo.getPreferredProcessingResource())) {
                if (retHash == null) {
                    retHash = new HashSet<String>();
                }
                retHash.add(pvo.getPreferredProcessingResource());
                logger.debug("User has permission for " + ppName + " via " + pvo.getPreferredProcessingResource());  // ??? Remove this
            }
        }
        return retHash;
    }
    
    /**
     * Returns list of Preferred Partner names (e.g., USAA) that current CSR has permissions to access
     * 
     * @param request
     * @return
     */
    public HashMap getPartnerDisplayNamesForUser(HttpServletRequest request) throws Exception {
        HashMap retHash = new HashMap();
        
        PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(GeneralConstants.PREFERRED_PARTNER_CACHE);
        HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
        Iterator ppIterator = ppMap.keySet().iterator();
        while(ppIterator.hasNext()) {
            String ppName = (String)ppIterator.next();
            PartnerVO pvo = (PartnerVO)ppMap.get(ppName);
            if (checkPreferredPermissionsForUser(request, pvo.getPreferredProcessingResource())) {
                retHash.put(pvo.getPreferredProcessingResource(), pvo.getDisplayValue());
                logger.debug("User has permission for " + ppName + " / display: " + pvo.getDisplayValue());
            }
        }
        return retHash;
    }
    

    /**
     * Checks passed list of Preferred Partners (associated with order) and includes appropriate alert message text
     * for those partners the current CSR does not have permissions for.
     * 
     * @param request
     * @param partnersOnOrder
     * @return
     * @throws Exception
     */
    public HashMap checkPreferredPermissionsForOrder(HttpServletRequest request, DataRequest dataRequest, HashSet partnersOnOrder) throws Exception {
        HashMap ppAlerts = new HashMap();
        if ((partnersOnOrder != null) && !partnersOnOrder.isEmpty()) {
            PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(GeneralConstants.PREFERRED_PARTNER_CACHE);
            HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
            Iterator ppIterator = ppMap.keySet().iterator();
            while(ppIterator.hasNext()) {
                String ppName = (String)ppIterator.next();
                PartnerVO pvo = (PartnerVO)ppMap.get(ppName);
                if (partnersOnOrder.contains(ppName) && !checkPreferredPermissionsForUser(request, pvo.getPreferredProcessingResource())) {
                    ConfigurationUtil cu = ConfigurationUtil.getInstance();
                    String errorMessage = cu.getContentWithFilter(dataRequest.getConnection(), GeneralConstants.PREFERRED_PARTNER_CONTEXT, 
                                                                  GeneralConstants.PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION, 
                                                                  pvo.getPreferredProcessingResource(), null);
                    if (errorMessage == null || errorMessage.equals("")) {
                        ppAlerts.put(ppName, "Preferred Partner permissions configuration error - contact Apollo support.  Partner: " + ppName);
                        logger.error("Preferred Partner permissions configuration error - no permission alert message defined for: " + ppName);
                    } else {
                        ppAlerts.put(ppName, errorMessage);
                    }
                }
            }
        }
        return ppAlerts;
    }
    

    /**
     * This is the primary method which validates the security for the current 
     * user and produces a response based on that conditions result.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     */
    public static boolean isValidToken(HttpServletRequest request, HttpServletResponse response)
    {
        String securityMessage = SecurityHelper.securityController(request, response);

        if(securityMessage != null && securityMessage.equals(MSG_VALID))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * This is a helper method to load the current user id from the session.
     * 
     * @param request HttpServletRequest
     * @param securityToken String
     * @exception Exception
     */
    public static String getUserId(HttpServletRequest request) throws Exception
    {
        String userId = EMPTY_STRING;
        String securityToken = request.getParameter("securitytoken");
        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);

        if(standAloneFlag != null && standAloneFlag.equals("N"))
        {
            if(((String) request.getSession().getAttribute(securityToken)) != null)
            {
                userId = (String) request.getSession().getAttribute(securityToken);
            }
            else
            {
                SecurityManager securityManager = SecurityManager.getInstance();
                UserInfo userInfo = securityManager.getUserInfo(securityToken);

                if(userInfo != null && userInfo.getUserID() != null)
                {
                    userId = userInfo.getUserID();
                    request.getSession().setAttribute(securityToken, userId);
                }
            }
        }
        else
        {
            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.DEFAULT_CSR);
        }
        
        return userId;
    }

    /**
     * This is a helper method to load the current admin action from the session.
     * 
     * @param request HttpServletRequest
     * @param securityToken String
     * @exception Exception
     */
    public static String getAdminAction(HttpServletRequest request, String securityToken) throws Exception
    {
        String adminAction = EMPTY_STRING;

        if(((String) request.getSession().getAttribute("adminAction")) != null)
        {
            adminAction = (String) request.getSession().getAttribute("adminAction");
        }
        else
        {
            adminAction = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION);
        }
        
        return adminAction;
    }

    private static String securityController(HttpServletRequest request, HttpServletResponse response)
    {
        String standAloneFlag = null;

        try
        {
            standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
        }
        catch(Exception e)
        {
            logger.error(e);
        }
        
        if(standAloneFlag != null && standAloneFlag.equals("N"))
        {
            try
            {
                SecurityManager securityManager = null;
                String securityContext = request.getParameter("context");
                String securityToken = request.getParameter("securitytoken");
            
                String tokenParam = "&securitytoken=" + securityToken;
                String contextParam = "&context=" + securityContext;
                String appContextParam = "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);

                try
                {
                    if (securityContext == null || (securityContext.equals("")))  
                    {
                        response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                        return MSG_NO_CONTEXT;
                    }

                    if (securityToken == null || (securityToken.equals("")))  
                    {
                        response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                        return MSG_NO_TOKEN;
                    }
        
                    logger.debug("Authenticating security token: " + securityToken);
                    securityManager = SecurityManager.getInstance();

                    if(securityManager.authenticateSecurityToken(securityContext, "ORDER SCRUB", securityToken))
                    {
                        return MSG_VALID;
                    }
                    else
                    {
                        response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                        return MSG_LOGON_FAILURE;
                    }
                }
                catch(ExpiredIdentityException e)
                {
                    logger.error(e);
                    response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                    return MSG_EXPIRED_IDENTIY;                 
                }
                catch(ExpiredSessionException e)
                {
                    logger.error(e);
                    response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                    return MSG_EXPIRED_SESSION;              
                }
                catch(InvalidSessionException e)
                {
                    logger.error(e);
                    response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                    return MSG_INVALID_SESSION;             
                }   
                catch(ExpiredCredentialsException e)
                {
                    logger.error(e);
                    response.sendRedirect(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SECURITY_LOCATION) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_REDIRECT) + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_DEFAULT_LOCATION) + tokenParam + contextParam + appContextParam);
                    return MSG_INVALID_SESSION;             
                }   
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                    return MSG_GENERAL_EXCEPTION;
                }
                catch(Exception re) 
                {
                    logger.error(re);
                    return MSG_GENERAL_EXCEPTION;
                }
            }
        }
        else
        {
            return MSG_VALID;
        }
    }
}
