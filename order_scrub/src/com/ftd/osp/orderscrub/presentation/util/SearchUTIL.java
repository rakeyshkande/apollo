package com.ftd.osp.orderscrub.presentation.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.OEParameters;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.vo.ProductFlagsVO;
import com.ftd.osp.utilities.AddOnUtility;
import com.ftd.osp.utilities.FTDFuelSurchargeUtilities;
import com.ftd.osp.utilities.PASServiceUtil;
import com.ftd.osp.utilities.SourceProductUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.vo.MercentChannelMappingVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.AddOnVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.XMLEncoder;
import com.ftd.pas.client.util.PASParamUtil;
import com.ftd.pas.core.domain.ProductAvailVO;
import com.ftd.pas.core.utils.DateUtil;

/** This Servlet is used during the Edit Product process.  The servlet
 *  contains methods which are used by the Edit Product servlets.
 *
 *  @author Ed Mueller
 *
 *  Note: This object was copied from Order Entry.
 *        The object was modified to use the OSP dataobjects.
 *        OE references, such as the Order object, were also removed.
 *        
 */
public class SearchUTIL
{
    public static final int GET_OCCASION_DESCRIPTION = 0;
    public static final int GET_COUNTRY_DESCRIPTION = 1;
    private static Logger logger;

    private static final int PROMOTION_BASE_POINTS =2;
    private static final int PROMOTION_UNIT_POINTS =3;
    private static final int PROMOTION_UNITS =4;
    private static final int PROMOTION_TYPE=7;
  
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

    private String status;
    private String message;

    private SimpleDateFormat orderDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
  
    /** Constructor.   Creates an instance of the logger. */
    public SearchUTIL()
    {
        logger = new Logger("com.ftd.osp.orderscrub.presentation.util.SearchUTIL");
    }

    /** This method takes in an XML document which contains holiday elements.  
     *  For each holiday element it creates an OEDelvieryDate object which contain
     *  delivery flags.  All of the OEDeliveryDate objects are put into a map and
     *  returned.
     *  @params Document XML Document containing holidays
     *  @return holidayMap - Map of OEDeliveryDate objects
     *  @throws Exception */
    public Map getHolidays(Document doc) throws Exception
    {
        SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
        Map holidayMap = new HashMap();

        String xpath = "//holiday";
        NodeList nl = DOMUtil.selectNodes(doc, xpath);
        Date holidayUtilDate;
        Element element = null;
        OEDeliveryDate holidayDate = null;
        
        if ( nl.getLength() > 0 )
        {
            for ( int i = 0; i < nl.getLength(); i++ )
            {
                element = (Element) nl.item(i);

                holidayDate = new OEDeliveryDate();
                holidayDate.setDeliveryDate(element.getAttribute("holidaydate"));
                holidayDate.setHolidayText(element.getAttribute("holidaydescription"));
                holidayDate.setDeliverableFlag(element.getAttribute("deliverableflag"));
                holidayDate.setShippingAllowed(element.getAttribute("shippingallowed"));
                holidayUtilDate = sdfInput.parse(holidayDate.getDeliveryDate());
                holidayMap.put(holidayDate.getDeliveryDate(), holidayDate);
            }
        }
        return holidayMap;
    }

    /**
     * This method builds the XML for a list of products or for one product.  
     * This is used from the product list page and the product detail page. 
     *
     * This method contains the logic to calculate prices, discounts, reward
     * miles, shipping costs, services fees, delivery methods...   This procedure also sets
     * flags (ProductFlagVO) which are used by the xsl page to determine
     * when popup messages need to be shown.
     * 
     * @param isList Tells if the productList parameter is actually a list or just
     * one product
     * @param loadDeliveryDates Switch to turn off loading of delivery dates
     * @param productList An Document of product details
     * @param displayCount The number of products to display per page
     * @param argMonth The month
     * @param argYear The year 
     * @param isCustomerDomesticFlag true if recip is domestic
     * @param domesticServiceFee The domestic service free
     * @param internationalServiceFee String
     * @param sendToCustomerState String State of the recipient
     * @param sendToCustomerCountry String
     * @param pricingCode String
     * @param partnerId String
     * @param promotionList List
     * @param rewardType String
     * @param zipCode String
     * @param flagVO ProductFlagsVO Various product flags which are used by the XSL page
     * @param sourceCode String
     * @return This method builds an Element object for the product list and 
     * product detail pages.
     * 
     * @author Jeff Penney
     *
     */
    private Element pageBuilder(boolean isList, boolean loadDeliveryDates, 
        Document productList, int displayCount, String argMonth, String argYear,
          boolean isCustomerDomesticFlag, String domesticServiceFee, String internationalServiceFee,
          String sendToCustomerState, String sendToCustomerCountry,String pricingCode,
          String partnerId, List promotionList,String rewardType, String zipCode,
          ProductFlagsVO flagVO, String sourceCode, String origDate) throws Exception
    {
        Document xmlDocument = DOMUtil.getDefaultDocument();
        Element rootElement = xmlDocument.createElement("root");

        DataRequest dataRequest = null;
        Connection con = null;
        try {


            HashMap shipMethodsMap = new HashMap();

            // Create initial node structure
            Element listElement = xmlDocument.createElement("productList");
            Element productsElement = xmlDocument.createElement("products");
            xmlDocument.appendChild(rootElement);
            rootElement.appendChild(listElement);
            listElement.appendChild(productsElement);

            // Storage for delivery dates if product detail
            Element deliveryDatesListElement = null;
            Element shipMethodElement = null;

            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
        	con = dataRequest.getConnection();

            // Pull out elements for the page
            Element element = null;
            NodeList nl = null;
            String xpath = null;

            //get global params
            Document globalParmsXML = DOMUtil.getDefaultDocument();
            dataRequest.reset();
            dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP_XML");
            dataRequest.setConnection(con);
            globalParmsXML = (Document) DataAccessUtil.getInstance().execute(dataRequest); 

            // retrieve the Global Parameter information
            String freshCutSrvcChargeTrigger = "";
            String freshCutSrvcCharge = "";
            String globalGNADDLevel = "";
            nl = DOMUtil.selectNodes(globalParmsXML, "params/parm");
            if (nl.getLength() > 0)
            {
              Element globalData = (Element) nl.item(0);
              freshCutSrvcChargeTrigger = globalData.getAttribute("freshcutssvcchargetrigger");
              freshCutSrvcCharge = globalData.getAttribute("freshcutssvccharge");
              globalGNADDLevel =  globalData.getAttribute("gnaddlevel");
            }

            // Get fuel surcharge
            Date originalOrderDate = orderDateFormat.parse(origDate);
            BigDecimal fuelSurchargeAmt = new BigDecimal(FTDFuelSurchargeUtilities.getFuelSurcharge(originalOrderDate, con));
            logger.debug("Fuel surcharge for order date: " + origDate + " is $" + fuelSurchargeAmt.toString());
                      
            // retrieve the product information
            nl = DOMUtil.selectNodes(productList, "//product");

            int pageSplitter = 0;

            if (nl.getLength() > 0)
            {
                double currentPrice;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;
                String stateId = null;
                String productType = null;
                BigDecimal feeWithFuelSurcharge;

                for(int i=pageSplitter; i < (pageSplitter + displayCount) && i < nl.getLength(); i++)
                {            
                    element = (Element) nl.item(i);

                    // Fuel surcharge is added to servicecharge here in case it doesn't get set in any conditionals below
                    String serviceChargeAttr = element.getAttribute("servicecharge");
                    if ((serviceChargeAttr != null) && (!serviceChargeAttr.equals(""))) {
                        feeWithFuelSurcharge = fuelSurchargeAmt.add(new BigDecimal(serviceChargeAttr));
                        element.setAttribute("servicecharge", feeWithFuelSurcharge.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                    }

                    // Determine if there is a special fee assoc with state and product type
                    flagVO.setSpecialFeeFlag("N");
                    if ( (sendToCustomerState != null) &&
                         (sendToCustomerState.equals("AK") ||
                          sendToCustomerState.equals("HI")) )
                    {
                        if ( (element.getAttribute("producttype") != null) &&
                             (element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SPEGFT)))
                        {
                            element.setAttribute("displayspecialfee", "Y");
                            flagVO.setSpecialFeeFlag("Y");
                        }
                    }

                    String codifiedAvailable = element.getAttribute("codifiedavailable");
                    String codifiedProduct = element.getAttribute("codifiedproduct");
                    String codifiedSpecial = element.getAttribute("codifiedspecial");
                    String codifiedDeliverable = element.getAttribute("codifieddeliverable");
                    
                    // Set unavail flag if the status is U or its an unavailable codified product
                    if ( // product is unavaiable if status is "U"
                         (element.getAttribute("status") != null &&
                          element.getAttribute("status").equals("U")) ||

                        // product is unavaiable if it is a regular codified product
                        // and it is set as unavailable for this zip code in the
                        // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                         (codifiedAvailable != null &&
                          codifiedAvailable.equals(EditProductConstants.NO) &&
                          (element.getAttribute("shipmethodcarrier") != null &&
                           !element.getAttribute("shipmethodcarrier").equals("Y"))) ||

                        // product is unavaiable if it is a regular codified product 
                        // and GNADD is at level 3
                         (codifiedProduct != null &&
                          codifiedProduct.equals(EditProductConstants.YES) &&
                          globalGNADDLevel.equals("3"))
                       )
                    {
                        flagVO.setDisplayProductUnavailable(EditProductConstants.YES);
                    }
                    else
                    {
                        flagVO.setDisplayProductUnavailable(EditProductConstants.NO);
                    }

                    // Personalized products should not be selectable
                    if ( element.getAttribute("personalizationtemplateid") != null &&
                        !element.getAttribute("personalizationtemplateid").equals(StringUtils.EMPTY) &&
                        !element.getAttribute("personalizationtemplateid").equalsIgnoreCase("NONE")) {
                    
                        flagVO.setDisplayPersonalizedProduct(EditProductConstants.YES);        
                    }
                    else {
                        flagVO.setDisplayPersonalizedProduct(EditProductConstants.NO);           
                    }

                    // Special Codified products should be deliverable if GNADD is on.
                    if (element.getAttribute("status") != null &&
                        !element.getAttribute("status").equals("U") &&
                        codifiedSpecial != null &&
                        codifiedSpecial.equals(EditProductConstants.YES) &&
                        codifiedDeliverable != null &&
                        (!codifiedDeliverable.equals("N") &&
                         !codifiedDeliverable.equals("D"))
                       )
                    {
                        flagVO.setDisplayProductUnavailable(EditProductConstants.NO);
                    }
            
                    // Do not allow freshcuts and specialty gift to be shipped to Canada
                    if(sendToCustomerState != null && sendToCustomerCountry != null)
                    {                       
                        if(((sendToCustomerCountry != null && sendToCustomerCountry.equals(EditProductConstants.COUNTRY_CODE_CANADA_CA)) ||
                            (sendToCustomerState != null && (sendToCustomerState.equals(EditProductConstants.STATE_CODE_PUERTO_RICO) || sendToCustomerState.equals(EditProductConstants.STATE_CODE_VIRGIN_ISLANDS)))) && 
                            (element.getAttribute("producttype") != null) &&
                            ((element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_FRECUT) || element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SPEGFT))))
                        {
                            flagVO.setDisplayProductUnavailable("Y");
                        }
                    }

                    //LoadDeliveryDates is a flag which is passed into this procedure.  Delivery date loads
                    //is not always needed, and since it is time consuming this flag was put in. (e.g. 
                    //delivery date loads and needed by the getProductById method)
                    if(loadDeliveryDates) 
                    {
                        // retreive first available delivery date information for product list
                        if ( isList )
                        {
							ProductAvailVO firstAvailablePASVO = null;

                            //This method returns the first available delivery date.
							List<ProductAvailVO> dates = this.getItemDeliveryDates(
									    con,
                                        productList, 
                                        element.getAttribute("productid"),
                                        true, zipCode, isCustomerDomesticFlag, 
                                        domesticServiceFee, internationalServiceFee,
                                        flagVO, sendToCustomerState, sendToCustomerCountry, sourceCode); 

							firstAvailablePASVO = dates.get(0);
							//QE-1
                            // Determine which carrier to show (florist or fed ex)
                            //c2
							// #PAS STUFF
							if (firstAvailablePASVO != null ) {
							List<String> shippingMethods = PASParamUtil.getShippingMethods(firstAvailablePASVO);
							for (int ii = 0; ii < shippingMethods.size(); ii++)
							{
								String shipMethod = (String) shippingMethods.get(ii);
								if (shipMethod.equals(EditProductConstants.DELIVERY_FLORIST_CODE) || shipMethod.equals(EditProductConstants.DELIVERY_SAME_DAY_CODE)) 
								{                                        
                                    element.setAttribute("firstdeliverymethod", "florist");

                                    // If this is SDG and a florist can deliver then remove all other shipping methods
                                    if(element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SDG) ||
                                       element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SDFC))
                                    {
                                        element.setAttribute("deliverytype1", "");
                                        element.setAttribute("deliverytype2", "");
                                        element.setAttribute("deliverytype3", "");
                                        element.setAttribute("deliverytype4", "");
                                        element.setAttribute("deliverytype5", "");
                                    }
                                    break;
                                }
                                else
                                {
                                    element.setAttribute("firstdeliverymethod", "carrier");
                                }
                            }
                            
							element.setAttribute("deliverydate", DateUtil.getFormattedDate(firstAvailablePASVO.getDeliveryDate(), null));
							}
                        }
                        // retrieve delivery date information for selected product
             /*  ---      QE-1 -- the else block doesn�t seem to get invoked, because the else block works only for (boolean isList=False, boolean loadDeliveryDates= true
              *  
              *   else
                        {
                            List dateList = getItemDeliveryDates(productList, 
                                  element.getAttribute("productid"),  
                                  false, zipCode, isCustomerDomesticFlag, 
                                        domesticServiceFee, internationalServiceFee,
                                        flagVO, sendToCustomerState, sendToCustomerCountry, sourceCode); 

                            deliveryDatesListElement = xmlDocument.createElement("deliverydates");
                            Element deliveryDateElement = null;
                            boolean firstDate = true;

                            for(int z = 0; z < dateList.size(); z++) {
                                deliveryDateElement = xmlDocument.createElement("deliverydate");

                                OEDeliveryDate deliveryDate = (OEDeliveryDate) dateList.get(z);
                                if ( deliveryDate.getDeliverableFlag().equals("Y") )
                                {
                                    if (firstDate) {
                                        element.setAttribute("deliverydate", deliveryDate.getDeliveryDate());
                                        firstDate = false;
                                    }

                                    deliveryDateElement.setAttribute("date", deliveryDate.getDeliveryDate());
                                    deliveryDateElement.setAttribute("dayofweek", deliveryDate.getDayOfWeek());

                                    // Loop through the delivery methods and concat them
                                    StringBuffer sbTypes = new StringBuffer();
                                    ShippingMethod shippingMethod = null;
                                    for (int d = 0; d < deliveryDate.getShippingMethods().size(); d++)
                                    {
                                        shippingMethod = (ShippingMethod)deliveryDate.getShippingMethods().get(d);
                                        sbTypes.append(shippingMethod.getCode()).append(":");
                                        shipMethodsMap.put(shippingMethod.getCode(), shippingMethod.getDeliveryCharge());
                                    }
                                    deliveryDateElement.setAttribute("types", sbTypes.toString());

                                    deliveryDateElement.setAttribute("displaydate", deliveryDate.getDisplayDate());
                                    deliveryDatesListElement.appendChild(deliveryDateElement);
                                }
                            }

                            if ( element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_FRECUT) ||
                                 element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SPEGFT) )
                            {
                                if ( shipMethodsMap.size() > 0 )
                                {
                                    shipMethodElement = xmlDocument.createElement("shippingmethod");

                                    Collection methodCollection = shipMethodsMap.keySet();
                                    Iterator iter = methodCollection.iterator();

                                    while ( iter.hasNext() )
                                    {
                                        String description = (String) iter.next();
                                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(description);

                                        Element entry = xmlDocument.createElement("method");
                                        entry.setAttribute("description", description);
                                        entry.setAttribute("cost", cost.toString());

                                        shipMethodElement.appendChild(entry);
                                    }
                                }
                            }
                        }*/
                    }

                    // Convert prices to XX.XX format
                    String price = null;
                    price = element.getAttribute("standardprice");                  
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("standardprice", price);
                    }

                    price = element.getAttribute("deluxeprice");                 
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("deluxeprice", price);
                    }
                    price = element.getAttribute("premiumprice");                    
                    if(price != null && price.length() > 0)
                    {
                        price = new BigDecimal(price).setScale(2).toString();
                        element.setAttribute("premiumprice", price);
                    }

                    // Calculate discounts for product
                    standardDiscountAmount = element.getAttribute("standarddiscountamt");
                    deluxeDiscountAmount = element.getAttribute("deluxediscountamt");
                    premiumDiscountAmount = element.getAttribute("premiumdiscountamt");

                    if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                        element.setAttribute("standardrewardvalue", standardDiscountAmount);
                    }

                    if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                        element.setAttribute("deluxerewardvalue", deluxeDiscountAmount);
                    }

                    if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                        element.setAttribute("premiumrewardvalue", premiumDiscountAmount);
                    }

                    // Determine if its a partner or standard discount
                    if(pricingCode != null && pricingCode.equals(EditProductConstants.PARTNER_PRICE_CODE) && partnerId != null && !partnerId.equals("")) {

                        if(element.getAttribute("standardprice") != null && !element.getAttribute("standardprice").equals("")) {
                            currentPrice = new BigDecimal(element.getAttribute("standardprice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("standardrewardvalue", retrievePromotionValue(sourceCode, currentPrice));
                        }
                        if(element.getAttribute("deluxeprice") != null && !element.getAttribute("deluxeprice").equals("")) {
                            currentPrice =  new BigDecimal(element.getAttribute("deluxeprice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("deluxerewardvalue", retrievePromotionValue(sourceCode, currentPrice));
                        }
                        if(element.getAttribute("premiumPrice") != null && !element.getAttribute("premiumprice").equals("")) {
                            currentPrice = new BigDecimal(element.getAttribute("premiumprice")).setScale(0, BigDecimal.ROUND_UP).doubleValue();
                            element.setAttribute("premiumrewardvalue", retrievePromotionValue(sourceCode, currentPrice));
                        }
                    }

                    else {

                        if(rewardType != null)
                        {
                            // Discount type of dollars off
                            if(rewardType.equals("Dollars")) {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardprice"));
                                    currentPrice = currentPrice - Double.parseDouble(standardDiscountAmount);
                                    element.setAttribute("standarddiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxeprice"));
                                    currentPrice = currentPrice - Double.parseDouble(deluxeDiscountAmount);
                                    element.setAttribute("deluxediscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumprice"));
                                    currentPrice = currentPrice - Double.parseDouble((premiumDiscountAmount));
                                    element.setAttribute("premiumdiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }

                            // Discount type of percent off
                            else if(rewardType.equals("Percent")) {
                                if(standardDiscountAmount != null && !standardDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("standardprice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(standardDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("standarddiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(deluxeDiscountAmount != null && !deluxeDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("deluxeprice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(deluxeDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("deluxediscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }

                                if(premiumDiscountAmount != null && !premiumDiscountAmount.equals("")) {
                                    currentPrice = Double.parseDouble(element.getAttribute("premiumprice"));
                                    currentPrice = currentPrice - new BigDecimal((currentPrice * (Double.parseDouble(premiumDiscountAmount) / 100))).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                                    element.setAttribute("premiumdiscountprice", new BigDecimal(currentPrice).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
                                }
                            }
                        }
                    }

                    productsElement.appendChild(xmlDocument.importNode(element, true));

                    // Do only if product detail (only one cycle in loop)
                    if ( !isList && loadDeliveryDates )
                    {
                        rootElement.appendChild(deliveryDatesListElement);

                        Document deliveryDateDocument = DOMUtil.getDefaultDocument();
                        Element deliveryDateElement = deliveryDateDocument.createElement("deliverydatelist");
                        deliveryDateElement.appendChild(deliveryDateDocument.importNode(deliveryDatesListElement, true));
                        deliveryDateDocument.appendChild(deliveryDateElement);
                    }

                    // get state id for later processing
                    stateId = element.getAttribute("stateid");
                    productType = element.getAttribute("producttype");
                } // end of For...Loop

                // only output for Product List
                Element shipMethods = xmlDocument.createElement("shippingmethods");

                // If zip code state is Alaska (AK) or Hawaii (HI) output valid
                // delivery types for state for presentation layer to use
                if  ( (stateId != null) &&
                      (stateId.equals("AK") ||
                       stateId.equals("HI")) )
                {
                    // if 2-day delivery is not one of the ship methods, then do not show any methods
                    // and set the product as undeliverable
                    if ( isList ||
                         shipMethodsMap.containsKey(EditProductConstants.DELIVERY_TWO_DAY) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(EditProductConstants.DELIVERY_TWO_DAY);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = xmlDocument.createElement("shippingMethod");
                        Element entry = xmlDocument.createElement("method");
                        entry.setAttribute("description", EditProductConstants.DELIVERY_TWO_DAY);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if ( productType.equals(EditProductConstants.PRODUCT_TYPE_FLORAL) )
                    {
                        BigDecimal cost = (BigDecimal) shipMethodsMap.get(EditProductConstants.DELIVERY_STANDARD);
                        if ( cost == null )
                        {
                            cost = new BigDecimal("0.00");
                        }

                        shipMethodElement = xmlDocument.createElement("shippingMethod");
                        Element entry = xmlDocument.createElement("method");
                        entry.setAttribute("description", EditProductConstants.DELIVERY_STANDARD);
                        entry.setAttribute("cost", cost.toString());

                        shipMethodElement.appendChild(entry);
                    }
                    else if(productType.equals(EditProductConstants.PRODUCT_TYPE_FRECUT))
                    // If the state is AK or HI and this is a freshcut then
                    // delivery is not available unless to day is available
                    {
                        flagVO.setDisplayProductUnavailable(EditProductConstants.YES);
                    }
                }
                // product list, show all delivery methods
                else if ( isList )
                {
                    // retrieve the Global Parameter information
                    nl = DOMUtil.selectNodes(productList, "//shippingMethod");

                    if ( nl.getLength() > 0 )
                    {
                        shipMethodElement = xmlDocument.createElement("shippingMethod");

                        for (int z = 0; z < nl.getLength(); z++)
                        {
                            element = (Element) nl.item(z);

                            Element entry = xmlDocument.createElement("method");
                            entry.setAttribute("description", element.getAttribute("description"));
                            entry.setAttribute("cost", element.getAttribute("cost"));
                            shipMethodElement.appendChild(entry);
                        }
                    }
                }
                // product detail / delivery information, only show available delivery methods
                if ( shipMethodElement != null )
                {
                    shipMethods.appendChild(shipMethodElement);
                }
                rootElement.appendChild(shipMethods);
            }
            else 
            {
              //no products in the list
              logger.debug("No products in the list");
            }
        }

        finally
        {
        if(con !=null)
        {
            con.close();
        }        }
        return rootElement;
    }
    

    /** 
     * This method determines if a product is available for shipping for the given
     * day of the week.  This method only takes into account the day or the week, not the
     * date.
     * 
     * @param currentDayOfWeek int The day of the week to check (1-sunday, 2-monday...)
     * @param productElement Element The product XML Element
     * @returns true if product is available for delivery
     */
    private boolean isAvailableForShipping(int currentDayOfWeek, Element productElement) {

        String availableFlag = "N";

        switch (currentDayOfWeek)
        {
            case 1:     // sunday
                availableFlag = productElement.getAttribute("sundayflag");
                break;
            case 2:     // monday
                availableFlag = productElement.getAttribute("mondayflag");
                break;
            case 3:     // tuesday
                availableFlag = productElement.getAttribute("tuesdayflag");
                break;
            case 4:     // wednesday
                availableFlag = productElement.getAttribute("wednesdayflag");
                break;
            case 5:     // thursday
                availableFlag = productElement.getAttribute("thursdayflag");
                break;
            case 6:     // friday
                availableFlag = productElement.getAttribute("fridayflag");
                break;
            case 7:     // saturday
                availableFlag = productElement.getAttribute("saturdayflag");
                break;
        }

        if(availableFlag.equals("Y"))
            return true;
        else
            return false;
    }

  /* public Element convertLookupAnchors(Document sourceLookUpXML) 
        throws Exception{

        NodeList nl = null;
        String anchor = null;
        LinkedList anchorList = new LinkedList();
        Document xmlDocument = DOMUtil.getDefaultDocument();
        Element rootElement = rootElement = xmlDocument.createElement("root");


            String xpath = "sourceCodeLookup/searchResults/searchResult";

            nl = DOMUtil.selectNodes(sourceLookUpXML, xpath);

            if (nl.getLength() > 0)
            {
                Element element = null;

                for(int i = 0; i < nl.getLength(); i++) {
                    element = (Element) nl.item(i);
                    anchor = element.getAttribute("anchor");

                    if(!anchorList.contains(anchor)) {
                        anchorList.add(anchor);
                    }
                }

                Collections.sort(anchorList);

                Element anchorListElement = xmlDocument.createElement("anchors");
                rootElement.appendChild(anchorListElement);
                Element anchorElement = null;

                Iterator anchorIterator = anchorList.iterator();
                while(anchorIterator.hasNext()) {
                    anchorElement = xmlDocument.createElement("anchor");
                    anchorElement.setAttribute("value", (String) anchorIterator.next());
                    anchorListElement.appendChild(anchorElement);
                }
            }
        
    
        return rootElement;
    }*/

    /* This method returns a list of promotions for the specified source code. */
//    public String retrievePromotionValue(List promotionList, double price) {
//      return "";
//    }

  /** 
   * This method returns the number of reward points that will give given for the 
   * passed in source code and price. 
   * 
   * @param sourceCode String source code associated with this order
   * @param price double of the product
   * @return String The number of reward points to award*/
    public String retrievePromotionValue(String sourceCode, double price) 
          throws Exception{

        logger.debug("retrievePromotionValue: " + sourceCode + " " + price);

        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
        dataRequest.setStatementID("GET_MILES_POINTS");
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
        dataRequest.addInputParam("IN_PRODUCT_AMOUNT", price);
        dataRequest.addInputParam("IN_ORDER_AMOUNT", price);

        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputs = (Map) dataAccessUtil.execute(dataRequest);
         
        String milesPoints = (String) outputs.get("OUT_MILES_POINTS");
        if (milesPoints == null) milesPoints = "0";
        String rewardType = (String) outputs.get("OUT_REWARD_TYPE");

        logger.debug("rewardType: " + rewardType + " - value: " + milesPoints);

        return milesPoints;
    }
    

    /**
     * Depending on the type passed in, this method returns either
     * an occasion description or a country description.
     * 
     * @param code String Occasion or country code
     * @param searchType int (GET_OCCASION_DESCRIPTION or GET_COUNTRY_DESCRIPTION)
     * @return ret String description
     */
    public String getDescriptionForCode(String code, int searchType) throws Exception
    {
        String ret = null;

        switch(searchType)
        {
            case GET_OCCASION_DESCRIPTION:
                ret = getOccasionDescription(code);
                break;
            case GET_COUNTRY_DESCRIPTION:
                ret = getCountryDescription(code);
                break;
            default:
                break;
        }

        return ret;
    }

    /** 
     * This methods returns the description for the passed in occasion code.
     * 
     * @param occasionId String
     * @returns String occasion description
     * @throws Exception
     */
    private String getOccasionDescription(String occasionId) throws Exception
    {

        // Initialize data request
        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

        String ret = "";
        
        try{
    
        dataRequest.setStatementID("GET_OCCASION");
        dataRequest.addInputParam("IN_OCCASION_ID",  occasionId);


        Document dataResponse = (Document)DataAccessUtil.getInstance().execute(dataRequest);
       
        String xpath = "//occasion";
        NodeList nl = DOMUtil.selectNodes(dataResponse, xpath);

        if (nl.getLength() > 0)
        {
            Element occasionData = (Element) nl.item(0);
            ret = occasionData.getAttribute("description");
        }

        }
        finally
        {
          if(dataRequest !=null && dataRequest.getConnection() != null)
          {
              dataRequest.getConnection().close();
          }        
        }


        return ret;
    }


    /**
     * This method calls a stored procedure to get all the available ship methods
     * for a product.  The stored procedure returns the ship method code, ship
     * method description and the shipping cost.  This method will take all the data
     * returned from the stored procedure and create objects from it..the objects
     * are then placed into a hashmap and returned.
     * 
     * @param productId String
     * @param standardPrice String
     * @param shippingKeyId String
     * @return ret Hashmap of ShippingMethod objects
     * @thorws Exception
     */
    public HashMap getProductShippingMethods(String productId, String standardPrice, String shippingKeyId) throws Exception
    {

        // Initialize data request
        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

        HashMap ret = new HashMap();

        try{
        dataRequest.addInputParam("IN_PRODUCT_ID",  productId);
        dataRequest.addInputParam("IN_SHIPPING_KEY_ID",  new Long(shippingKeyId));
        dataRequest.addInputParam("IN_STANDARD_PRICE", new Double(standardPrice) );

        dataRequest.setStatementID("GET_SHIPPING_METHODS");
        Document dataResponse = (Document) DataAccessUtil.getInstance().execute(dataRequest);

        NodeList nl = DOMUtil.selectNodes(dataResponse, "//shipMethod");

        Element shipMethodElement = null;
        ShippingMethod shipMethod = null;
        for(int i = 0; i < nl.getLength(); i++)
        {
            shipMethod = new ShippingMethod();
            shipMethodElement = (Element) nl.item(i);
            shipMethod.setCode(shipMethodElement.getAttribute("shipmethodcode"));
            shipMethod.setDescription(shipMethodElement.getAttribute("shipmethoddesc"));
            shipMethod.setDeliveryCharge(new BigDecimal(shipMethodElement.getAttribute("shipcost")));

            ret.put(shipMethod.getDescription(), shipMethod);
        }

        }
        finally
        {
          if(dataRequest !=null && dataRequest.getConnection() != null)
          {
              dataRequest.getConnection().close();
          }                }        

        return ret;
    }


    /**
     * This method returns the country description for the passed in
     * country code.
     * 
     * @param code String country code
     * @returns String country description
     * @throws Exception
     */
    private String getCountryDescription(String code) throws Exception
    {

      // Initialize data request
      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
    
        String ret = "";

        try{
        Document dataResponse = null;

        dataRequest.setStatementID("GET_COUNTRY_MASTER_BY_ID");
        dataRequest.addInputParam("IN_COUNTRY_ID",  code);


        dataResponse = (Document)DataAccessUtil.getInstance().execute(dataRequest);

        String xpath = "countryLookup/searchResults/searchResult";
        NodeList nl = DOMUtil.selectNodes(dataResponse, xpath);

        if (nl.getLength() > 0)
        {
            Element occasionData = (Element) nl.item(0);
            ret = occasionData.getAttribute("countryname");
        }

        }
        finally
        {
              if(dataRequest !=null && dataRequest.getConnection() != null)
              {
                  dataRequest.getConnection().close();
              }
        }

        return ret;
    }

    /**
      * This method takes in the XML document which contains all the products
      * for the selected category and the XML document which contains only the
      * products for the current page.  The method checks if any products
      * have been removed from the current page XML document do to product
      * unavailablity.  This method will remove unavailable products for the 
      * xml which contains the entire product list.
      * 
      * This method also copies the page data from one XML document to the other.
      * 
      * @param Document searchResultsIndex Paginated list of products
      * @param Document searchResultsPerpage Non-paginated list of products
      * @param int currentPage
      * @param int pageSize
      * @returns void
      * @throws Exception
     */
    public static void rebuildSearchResultsIndex(Document searchResultsIndex,
                                               Document searchResultsPerPage,
                                               int currentPage,
                                               int pageSize) throws Exception{

      // assumes that the index is pre-paginated
      NodeList nodesOnIndex =
          DOMUtil.selectNodes(searchResultsIndex, "//product[@page='"+ currentPage +"']");
      int nodeCountOnIndex = nodesOnIndex.getLength();

      // assumes the search results have not been paginated
      NodeList nodesOnsearchResultsPerPage =
          DOMUtil.selectNodes(searchResultsPerPage, "//product");
      int nodeCountOnsearchResultsPerPage = nodesOnsearchResultsPerPage.getLength();

       // check to see if any products have been made unavailable
        if (nodeCountOnIndex > nodeCountOnsearchResultsPerPage)  
        {
            // remove nodes from the search results index that are missing in the search results
            Element nodeOnIndex = null;
            String productID = null;
            NodeList productNodeOnsearchResultsPerPageNL = null;

            for (int i = 0; i < nodeCountOnIndex; i++)  
            {
                nodeOnIndex = (Element) nodesOnIndex.item(i);

                if (nodeOnIndex != null)  
                {
                    // get the product id from the index
                    productID = nodeOnIndex.getAttribute("productid");

                    // check to see if the product id exists on search results
                    productNodeOnsearchResultsPerPageNL =
                        DOMUtil.selectNodes(searchResultsPerPage, "//product[@productid='"+ productID +"']");

                
                    // if the product id on the index cannot be found on the search results
                    if (productNodeOnsearchResultsPerPageNL.getLength() == 0)  
                    {
                        nodeOnIndex.getParentNode().removeChild(nodeOnIndex);
                    }

                }
             }
        }

      // insert the pagination node on the search results DOM
      //paginate(searchResultsPerPage, currentPage, pageSize);
        Element pageData = (Element) DOMUtil.selectNodes(searchResultsIndex, "//pageData").item(0);
        if (pageData != null)  {
            Element importedNode = (Element) searchResultsPerPage.importNode(pageData, true);
            searchResultsPerPage.getDocumentElement().appendChild(importedNode);
        }
        else {
            System.out.println("Element 'pageData' not found");
        }
    }


  /**
   * This method takes in a product list and adds page number elements
   * to each of the products.
   * 
   * @params Document doc An xml document containing all the products which are available
   * @params int currentPage, the page that the user is currently on
   * @params int pageSize the number of products which can be displayed on a page
   * @return total page count
   */
  public static long paginate(Document doc,  int currentPage, int pageSize) throws Exception{

      NodeList nodeList =   DOMUtil.selectNodes(doc, "//product");
      int page = 1, totalPages = 1; // start at 1
      int counter = 0;

      Element node = null;

      for (int i = 0; i < nodeList.getLength(); i++,counter++)  {
          // control pagination
          if (counter >= pageSize)  {
              page++; // increment the page number
              totalPages = page;
              counter = 0; // reset counter
          }

          node = (Element) nodeList.item(i);
          // rewrite the page attribute
          node.setAttribute("page", String.valueOf(page));
          // set the status attribute to available
          node.setAttribute("status", "available");

      }

      // check if the current page is greater that the total page count
      // reset it to the last page
      if (currentPage > totalPages)  {
          currentPage = totalPages;
      }

      // set up pagination nodes on the search results
      Element pageData = (Element) DOMUtil.selectNodes(doc, "//pageData").item(0);
      Element data = null;

      if (pageData == null)  {
          pageData = doc.createElement("pageData");
          // total pages
          data = (Element) doc.createElement("data");
          data.setAttribute("name", "totalPages" );
          data.setAttribute("value", String.valueOf(totalPages) );
          pageData.appendChild(data);

          // current page
          data = (Element) doc.createElement("data");
          data.setAttribute("name", "currentPage" );
          data.setAttribute("value", String.valueOf(currentPage) );
          pageData.appendChild(data);

          // append the page data node to the root node
          doc.getDocumentElement().appendChild(pageData);
      }
      else {
          // total pages
          data = (Element) DOMUtil.selectNodes(doc, "//pageData/data[@name='totalPages']").item(0);
          data.setAttribute("value", String.valueOf(totalPages) );
          // current page
          data = (Element) DOMUtil.selectNodes(doc, "//pageData/data[@name='currentPage']").item(0);
          data.setAttribute("value", String.valueOf(currentPage) );
      }

      //DOMUtil.print(doc, System.out);

      return totalPages;
  }


    /** 
     * @param Document searchResultsIndex List of available products to display in list 
     * @param String strCurrentPage Current page number
     * @param String pricePointId Price filtering code..if any
     * @param String sourceCode Current source code
     * @param boolean isDomesticFlag domestic flag
     * @param String sendToCustomerZipCode recipient zip code
     * @param String sendToCountry Recipient country
     * @param String scriptCode Script code
     * @param String daysOut Value from Global Params table
     */
    public Document getProductsByIDs(Document searchResultsIndex, 
          String strCurrentPage, String pricePointId, String sourceCode,
          boolean isDomesticFlag,String sendToCustomerZipCode, String sendToCountry,
          String scriptCode, String daysOut, String deliveryDate) throws Exception
    {
        //Get the list of products for the current page
        NodeList nodeList=null;
        String strProductId;
        String strPage;

        Element node = null;
        Document searchResults = null;

        nodeList=DOMUtil.selectNodes(searchResultsIndex, "//product");
        StringBuffer sb = new StringBuffer();
        HashMap productOrderMap = new HashMap();
        String productOrder = null;
        String productId = null;
        for (int i = 0; i < nodeList.getLength(); i++ )
        {
            // control pagination
            node = (Element) nodeList.item(i);
            strProductId = node.getAttribute("novatorid");
            productId = node.getAttribute("productid");
            strPage = node.getAttribute("page");
            productOrder = node.getAttribute("displayorder");
            productOrderMap.put(productId, productOrder);

            //if this product is part of hte current page add it to the string buffer
            if( strProductId!=null && strPage!=null && strPage.compareTo(strCurrentPage)==0 )
            {
                sb.append("'");
                sb.append(strProductId);
                sb.append("'");
                sb.append(",");
            }
        }


        //delete trailing comma if the string buffer contains data
        if(sb.length() > 0)
        {
            sb.deleteCharAt(sb.length()-1);
        }
        else
        {
            //buffer does not contain anything, put in an empty quoted string
            sb.append("''");
        }

        // Initialize data request
        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{

        //Get the product detail for the list of products, the string buffer contains
        //a comma delmited list of products to retrieve
        dataRequest.addInputParam("IN_INDEX_ID", sb.toString());

        //price point will have a value if the user choose to filter product list by price
        if(pricePointId != null && pricePointId.length() == 0)
        {
            pricePointId = null;
        }

        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);

        if ( isDomesticFlag )
        {
            dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG", EditProductConstants.DELIVERY_TYPE_DOMESTIC);
            dataRequest.addInputParam("IN_ZIP_CODE",sendToCustomerZipCode);
        }
        else
        {
            dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG", EditProductConstants.DELIVERY_TYPE_INTERNATIONAL);
            dataRequest.addInputParam("IN_ZIP_CODE", null);
        }

        Calendar deliveryEndDate = Calendar.getInstance();
        deliveryEndDate.add(Calendar.DATE, Integer.parseInt(daysOut));
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dataRequest.addInputParam("IN_DELIVERY_END_DATE", dateFormat.format(deliveryEndDate.getTime()));
        dataRequest.addInputParam("IN_COUNTRY_ID", sendToCountry);
        dataRequest.addInputParam("IN_SCRIPT_CODE", scriptCode);
        if (deliveryDate == null) {
            deliveryDate = "";
        }
        dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);

        if(pricePointId == null || pricePointId.length() <= 0)
        {
            dataRequest.addInputParam("IN_PRICE_POINT_ID",  new Integer(java.sql.Types.INTEGER));
            dataRequest.setStatementID("GET_PRODUCTS_BY_ID_SDG_NULLPRICEPPOINT");          
        }
        else
        {
            dataRequest.addInputParam("IN_PRICE_POINT_ID", new Long(pricePointId));
            dataRequest.setStatementID("GET_PRODUCTS_BY_ID_SDG");                    
        }
        
         //this procedure gets the detailed information about each product in the list
         searchResults = (Document) DataAccessUtil.getInstance().execute(dataRequest);


         //append global params
         dataRequest.reset();
         dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP_XML");
         DOMUtil.addSection(searchResults, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
                
         //append holidays for the recipient country
         dataRequest.reset();
         dataRequest.setStatementID("GET_HOLIDAYS_BY_COUNTRY");
         dataRequest.addInputParam("IN_COUNTRY_ID",sendToCountry);
         DOMUtil.addSection(searchResults, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
         
         //append ship methods
         dataRequest.reset();
         dataRequest.setStatementID("GET_ALL_SHIPPING_METHODS");
         DOMUtil.addSection(searchResults, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

         //get delivery data ranges.  These are date ranges in which products cannot be delivered.        
         dataRequest.reset();
         dataRequest.setStatementID("GET_DELIVERY_DATE_RANGES");
         DOMUtil.addSection(searchResults, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 


        //For each of the products returned from the product detail query set the 
        //display order.  The display order is obtained from the XML which was passed
        //into this method.  Basically the displayOrder id being transferred from one
        //XML document to another.
        nodeList=DOMUtil.selectNodes(searchResults, "//product");
        Element element = null;
        productId = null;
        for(int i = 0; i < nodeList.getLength(); i++)
        {
            element = (Element)nodeList.item(i);
            productId = element.getAttribute("productid");
            element.setAttribute("displayorder", (String)productOrderMap.get(productId));
        }


        }
        finally
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }
        }
        return searchResults;
    }


    /**
     * This method retrieves the row of data in the FTD_APPS.GLOBAL_PARMS table
     * and put all the data into OEParameter object.
     * @returns OEParameters
     * @throws Exception */
    public OEParameters getGlobalParms() throws Exception
    {
        OEParameters oeGlobalParms = new OEParameters();

        // Initialize data request
        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

        try
        {

            dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP_XML");
            Document globalXML = (Document) DataAccessUtil.getInstance().execute(dataRequest);
                    
            Element globalData = null;
            NodeList nl = DOMUtil.selectNodes(globalXML, "params/parm");

            if (nl.getLength() > 0)
            {
                SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");

                globalData = (Element) nl.item(0);

                oeGlobalParms.setDeliveryDaysOut(Integer.parseInt(globalData.getAttribute("deliverydaysout")));
                oeGlobalParms.setExoticCutoff(globalData.getAttribute("exoticcutoff"));
                oeGlobalParms.setFreshCutCutoff(globalData.getAttribute("freshcutscutoff"));
                oeGlobalParms.setFridayCutoff(globalData.getAttribute("fridaycutoff"));
                oeGlobalParms.setGNADDDate(sdfInput.parse(globalData.getAttribute("gnadddate")));
                oeGlobalParms.setGNADDLevel(globalData.getAttribute("gnaddlevel"));
                oeGlobalParms.setIntlAddOnDays(Integer.parseInt(globalData.getAttribute("intloverridedays")));
                oeGlobalParms.setIntlCutoff(globalData.getAttribute("internationalcutoff"));
                oeGlobalParms.setMondayCutoff(globalData.getAttribute("mondaycutoff"));
                oeGlobalParms.setSaturdayCutoff(globalData.getAttribute("saturdaycutoff"));
                oeGlobalParms.setSpecialtyGiftCutoff(globalData.getAttribute("specialtygiftcutoff"));
                oeGlobalParms.setSundayCutoff(globalData.getAttribute("sundaycutoff"));
                oeGlobalParms.setThursdayCutoff(globalData.getAttribute("thursdaycutoff"));
                oeGlobalParms.setTuesdayCutoff(globalData.getAttribute("tuesdaycutoff"));
                oeGlobalParms.setWednesdayCutoff(globalData.getAttribute("wednesdaycutoff"));
                oeGlobalParms.setFreshCutSrvcChargeTrigger(globalData.getAttribute("freshcutssvcchargetrigger"));
                if(globalData.getAttribute("freshcutssvccharge") != null && !globalData.getAttribute("freshcutssvccharge").equals("")) {
                    oeGlobalParms.setFreshCutSrvcCharge(new BigDecimal(globalData.getAttribute("freshcutssvccharge")));
                }
                if(globalData.getAttribute("specialsvccharge") != null && !globalData.getAttribute("specialsvccharge").equals("")) {
                    oeGlobalParms.setSpecialSrvcCharge(new BigDecimal(globalData.getAttribute("specialsvccharge")));
                }

            }
        }
        finally
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }
        }

        return oeGlobalParms;
    }

    /** 
     * This method is used to sort the product list.  It uses the 
     * passed in xslFile to sort the passed in XML document.
     * 
     * @param File sortXSLFile
     * @param Document xml
     * @returns Document, sort products
     * @throws Exception*/
    public Document sortProductList(File sortXSLFile, Document xml)
        throws Exception {
        Document result = null;

            TransformerFactory factory = TransformerFactory.newInstance();
            //String sortSheet = xslLocation + sortType + ".xsl";

            Templates pss = factory.newTemplates(new StreamSource(sortXSLFile));
            //Templates pss = factory.newTemplates(new StreamSource(new File(sortSheet)));
            Transformer transformer = pss.newTransformer();

            // Ivan Vojinovic: this fix for Defect #898 in Phase III
            // was recomended by JP Puzon 08/23/2005:
            // Construct string representation of xml document for escape substitution.
            StringWriter sw = new StringWriter(); 
            DOMUtil.print(xml, new PrintWriter(sw));
            String xmlString = sw.toString().replaceAll("'", "&#39;");
            // Use the escaped XML doc for transformation.
            Document escapedXml = DOMUtil.getDocument(xmlString);

            StringWriter sortOutput = new StringWriter();
            transformer.transform(new DOMSource(escapedXml), new StreamResult(sortOutput));
            result = createDocument(sortOutput.toString());
   

        return result;
    }

    /**
     * Performs a product list search with the paramers passed.  There is a large
     * amount of business logic in the stored procdures.  For each product the
     * procedure returns the product id, standard price and the display order.
     * 
     * See the stored procudure documentation and code for more infomration.  The
     * following text was copied from the stored procdure comment section:
     * 
      -- Description:   Searches PRODUCT_MASTER by the supplied index ID using PRODUCT_INDEX_XREF.
      --                Price point ID specifies a desired price range within which returned
      --                products should fall (by standard price).
      --
      --                Source code, domestic/international flag (D or I) and the recipient
      --                zip code must be provided.  Some of the product information is source
      --                code dependent or depends on domestic vs. international.  Zip code
      --                is used to exclude products not sold in the recipient's state.

     * @param String queryName Contains the data constant referring to a search procedure 
     * @param String sourceCode current source code
     * @param String indexId category id
     * @param zipCode the zip code
     * @param String pricePointId Price filtering info
     * @param String countryID Recipient country id
     * @param String scriptCode Used when displaying script on page
     * @param String deliveryEndDate
     * @param String domesticFlag I=Internationial, D=Domestic
     * @exception Exception if a SQL Exception occures
     * @author Jeff Penney
     *
     */
    public static Document preformProductListSearch(String queryName,
        String sourceCode, String indexID, String zipCode, String pricePointID,
        String countryID, String scriptCode, String deliveryEndDate,
        String domesticFlag) throws Exception
    {
        Document ret = null;
        Document result = null;

        // Initialize data request
        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

        try{

        //if blank null it out
        if (zipCode != null && zipCode.length() <=0)
        {
          zipCode = null;
        }
        if (indexID != null && indexID.length() <=0)
        {
          indexID = null;
        }        

        // All searches
        dataRequest.addInputParam("IN_INDEX_ID", indexID);
        dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
        dataRequest.addInputParam("IN_ZIP_CODE", zipCode);
        dataRequest.addInputParam("IN_COUNTRY_ID", countryID);
        dataRequest.addInputParam("IN_SCRIPT_CODE", scriptCode);
        dataRequest.addInputParam("IN_DELIVERY_END_DATE", deliveryEndDate);
        dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG", domesticFlag);

        // Advanced Search
        //dataRequest.addArgument("inOccasionId", (String)arguments.get("inOccasionId"));
        //dataRequest.addArgument("inCategory", (String)arguments.get("inCategory"));
        //dataRequest.addArgument("inRecipient", (String)arguments.get("inRecipient"));
        //dataRequest.addArgument("inColor", (String)arguments.get("inColor"));
        //dataRequest.addArgument("inName", (String)arguments.get("inName"));

        // Keyword Search
        //dataRequest.addArgument("searchKeywordList", (String)arguments.get("searchKeywordList"));

        //Load Super Index List
        //dataRequest.addInputParam("IN_LIVE_FLAG", EditProductConstants.YES);
        //dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG", EditProductConstants.NO);
        //dataRequest.addInputParam("IN_PRODUCTS_FLAG", EditProductConstants.YES);
        //dataRequest.addInputParam("IN_DROPSHIP_FLAG", EditProductConstants.NO);
        //dataRequest.addInputParam("IN_COUNTRY_ID", null);
        //dataRequest.addInputParam("IN_SOURCE_CODE_ID", sourceCode);            


        if(pricePointID == null || pricePointID.length() <= 0)
        {          
          dataRequest.addInputParam("IN_PRICE_POINT_ID", new Integer(java.sql.Types.INTEGER));
          dataRequest.setStatementID("GET_PRODUCT_LIST_BY_ID_NULLPRICEPPOINT");          
        }
        else
        {
           dataRequest.addInputParam("IN_PRICE_POINT_ID", new Long(pricePointID));
           dataRequest.setStatementID("GET_PRODUCT_LIST_BY_ID");          
        }


      
        result =  (Document) DataAccessUtil.getInstance().execute(dataRequest);

        }
        finally
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }
        }

        return result;
    }

    /**
     * This method tokenizes a Map into a String
     * @param inMap A Map of search criteria that will be converted to a String
     * @author Jeff Penney
     *
     */
    public static String getSearchCriteriaString(Map inMap)
    {
        StringBuffer ret = new StringBuffer();

        Set keySet = inMap.keySet();
        Iterator it = keySet.iterator();
        String key = null;
        String value = null;
        while(it.hasNext())
        {
            key = (String)it.next();
            value = (String)inMap.get(key);
            if(value == null)
            {
                value = "null";
            }

            ret.append(key).append(":").append(value).append(" ");
        }

        return ret.toString();
    }

    /**
     * This method tokenizes a String into a Map
     * @param inString A String of search criteria that will be converted to a Map
     * @author Jeff Penney
     *
     */
    public static Map getSearchCriteriaMap(String inString)
    {
        Map ret = new HashMap();
        StringTokenizer tokenizer1 = new StringTokenizer(inString, " ");
        String token = null;
        StringTokenizer tokenizer2 = null;
        String key = null;
        String value = null;
        
        while(tokenizer1.hasMoreTokens())
        {
            token = tokenizer1.nextToken();

            tokenizer2 = new StringTokenizer(token, ":");
            key = tokenizer2.nextToken();
            value = tokenizer2.nextToken();
            if(value.equals("null"))
            {
                value = null;
            }
            ret.put(key, value);
        }
        return ret;
    }

    /** 
     * This method sets flags in in the ProductFlagsVO that indicate 
     * what messages should be displayed to the user related to 
     * product availablity. 
     * 
     * @param OEDeliveryDateParam delparm Delivery Dates
     * @param List shipMethods Possible ship methods
     * @param ProductFlagsVO flagVO Flags
     * @param String sendToState Recipient state
     * @param String sendToCountry Recipient country
     * @return void
     **/
    private void checkForSDGAvailable(OEDeliveryDateParm delParms, List shipMethods,
            ProductFlagsVO flagVO, String sendToState, String sendToCountry)
    {
        String codifiedAvailable = delParms.getCodifiedAvailable();
        if(codifiedAvailable == null) codifiedAvailable = "";
        String codifiedProduct = delParms.getCodifiedProduct();
        if(codifiedProduct == null) codifiedProduct = "";
        String codifiedSpecial = delParms.getCodifiedSpecialFlag();
        if(codifiedSpecial == null) codifiedSpecial = "";
        String codifiedDeliverable = delParms.getCodifiedDeliverable();
        if(codifiedDeliverable == null) codifiedDeliverable = "";
        
        if(codifiedProduct.equals(GeneralConstants.YES) && 
           (delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
            delParms.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))            
        {
            // if the delivery country is Canada, the Virgin Islands or
            // Puerto Rico and thier is no codified florist then set the
            // displayProductUnavailable flag
            if(codifiedAvailable.equals(GeneralConstants.NO) &&
               (sendToCountry.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
               sendToState.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
               sendToState.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
               codifiedDeliverable.equals("N"))
            {
                flagVO.setDisplayProductUnavailable(GeneralConstants.YES);
                flagVO.setDisplayNoProduct(GeneralConstants.NO);
            }
            // if the delivery country is Canada, the Virgin Islands or
            // Puerto Rico and thier is no florist coverage then set the
            // displayNoProduct flag            
            else if(codifiedAvailable.equals(GeneralConstants.NO) &&
                    (sendToCountry.equals(GeneralConstants.COUNTRY_CODE_CANADA_CA) ||
                     sendToState.equals(GeneralConstants.STATE_CODE_PUERTO_RICO) ||
                     sendToState.equals(GeneralConstants.STATE_CODE_VIRGIN_ISLANDS)) &&
                     codifiedDeliverable.equals("D"))
            {
                flagVO.setDisplayNoProduct(GeneralConstants.YES);
                flagVO.setDisplayProductUnavailable(GeneralConstants.NO);
            }
            // This case is when the product only has two day delivery (ex. AK, HI)
            else if(codifiedAvailable.equals(GeneralConstants.NO) && shipMethods.size() == 1 && 
                    shipMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE))
            {
                flagVO.setDisplayCodifiedFloristHasTwoDayDeliveryOnly(GeneralConstants.YES);
            }            
            // if the zip code can still take orders but just not for this product
            else if(codifiedAvailable.equals(GeneralConstants.NO) &&
                    codifiedDeliverable.equals("N") && shipMethods.size() > 0)
            {
                flagVO.setDisplayNoCodifiedFloristHasCommonCarrier(GeneralConstants.YES);
                flagVO.setDisplayNoProduct(GeneralConstants.NO);
            }
            // if the zip code is shut down
            else if(codifiedAvailable.equals(GeneralConstants.NO) &&
                    codifiedDeliverable.equals("D") && shipMethods.size() > 0)
            {
                flagVO.setDisplayNoFloristHasCommonCarrier(GeneralConstants.YES);
                flagVO.setDisplayNoProduct(GeneralConstants.NO);
            }
            // No florist and no carrier delivery methods
            else if(codifiedAvailable.equals(GeneralConstants.NO) && shipMethods.size() == 0)
            {
                flagVO.setDisplayProductUnavailable(GeneralConstants.YES);
            }
            else
            {
                flagVO.setDisplayNoProduct("N");
                flagVO.setDisplayNoCodifiedFloristHasCommonCarrier("N");
                flagVO.setDisplayNoFloristHasCommonCarrier("N");
            }
        }
    }

    /**
     * This method gets the Delivery dates for a line item in an order. This
     * method contains the business logic which determines when a product is
     * deliverable.
     * 
     * @param Document document The XML document containing product information
     * @param String productId The product ID that the delivery dates will be calculated for 
     * @param boolean getFirstAvailableDate Determines if all delivery dates should be calculated or just the first available
     * @param String zipCode
     * @param boolean isDomesticFlag
     * @param String domesticServiceFee
     * @param String internationialServiceFee
     * @param ProductFlagsVO flagVO
     * @param String sendToState
     * @param String sendToCountry
     * @param String sourceCode
     * @return A List of Delivery date objects
     * @throws Exception
     * @author Jeff Penney
     *
     */
    public List getItemDeliveryDates(Connection con, Document document, String productId, 
          boolean getFirstAvailableDate, String zipCode, boolean isDomesticFlag,
          String domesticServiceFee, String internationalServiceFee,
          ProductFlagsVO flagVO, String sendToState, String sendToCountry, String sourceCode)
           throws Exception
    {
    	List<ProductAvailVO> deliveryDates = null;
        try
        {
            SimpleDateFormat sdfInput = new SimpleDateFormat("MM/dd/yyyy");
            DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL();
            SearchUTIL searchUTIL = new SearchUTIL();
            OEDeliveryDateParm deliveryDateParm = new OEDeliveryDateParm();
            Map holidayMap = null; 

            String zipSundayFlag = null;

            flagVO.setDisplayFloral(EditProductConstants.NO);
            flagVO.setDisplayNoCodifiedFloristHasCommonCarrier(EditProductConstants.NO);
            flagVO.setDisplayNoFloristHasCommonCarrier(EditProductConstants.NO);
            flagVO.setDisplayNoProduct(EditProductConstants.NO);
            flagVO.setDisplayProductUnavailable(EditProductConstants.NO);
            flagVO.setDisplaySpecialtyGift(EditProductConstants.NO);
            flagVO.setDisplayCodifiedFloristHasTwoDayDeliveryOnly(EditProductConstants.NO);

            // parse global parameters from the returned XML
            OEParameters oeGlobalParms = getGlobalParms();

            // retrieve the holiday date information for the country
            holidayMap = getHolidays(document);
            
            // retrieve the product information
            //DOMUtil.print(document, System.out);
            String xpath = "//products/product[@productid = '" + productId + "']";
            NodeList nl = DOMUtil.selectNodes(document, xpath);
            boolean floralServiceChargeSet = false;
            if (nl.getLength() > 0)
            {
                double currentPrice;
                double currentDiscountAmount;

                String standardDiscountAmount = null;
                String deluxeDiscountAmount = null;
                String premiumDiscountAmount = null;
                String cntryAddOnDays = null;

                Element element = (Element) nl.item(0);

//                deliveryDateParm.setOrder(order);
                deliveryDateParm.setGlobalParms(oeGlobalParms);
                deliveryDateParm.setDeliverTodayFlag(Boolean.TRUE);
                deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                deliveryDateParm.setProductType(element.getAttribute("producttype"));
                deliveryDateParm.setProductSubType(element.getAttribute("productsubtype"));
                deliveryDateParm.setProductId(element.getAttribute("productid"));
                cntryAddOnDays = element.getAttribute("addondays");

                if ( (cntryAddOnDays != null) &&
                     (!cntryAddOnDays.trim().equals("")) )
                {
                    deliveryDateParm.setCntryAddOnDays(Integer.parseInt(cntryAddOnDays));
                }
                else
                {
                    deliveryDateParm.setCntryAddOnDays(0);
                }

                // Use zip code information if exists
                if ( zipCode != null && !zipCode.equals("") )
                {
                    deliveryDateParm.setZipTimeZone(element.getAttribute("timezone"));
                    deliveryDateParm.setZipCodeGNADDFlag(element.getAttribute("zipgnaddflag").trim());
                    deliveryDateParm.setZipCodeGotoFloristFlag(element.getAttribute("zipgotofloristflag").trim());
                    zipSundayFlag = element.getAttribute("zipsundayflag").trim();
                }
                // zip code information does not exist, use default values
                else {
                    deliveryDateParm.setZipTimeZone(EditProductConstants.TIMEZONE_DEFAULT);
                    deliveryDateParm.setZipCodeGNADDFlag(EditProductConstants.NO);
                    deliveryDateParm.setZipCodeGotoFloristFlag(EditProductConstants.YES);
                    zipSundayFlag = EditProductConstants.YES;
                }

                // zip code does support Sunday delivery
                if ( zipSundayFlag != null &&
                     zipSundayFlag.equals(EditProductConstants.YES) )
                {
                    // codified products follow different rules under normal processing
                    if ( element.getAttribute("codifiedproduct") != null &&
                         element.getAttribute("codifiedproduct").equals(EditProductConstants.YES) &&
                         oeGlobalParms.getGNADDLevel().equals("0") )
                    {
                        // Valid valued are: N-codified; NA-not codified/not available
                        //                   Y-codified (No Sunday); S-codified (Sunday)
                        if ( element.getAttribute("codifiedavailable") != null &&
                             element.getAttribute("codifiedavailable").equals("S") )
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.TRUE);
                        }
                        else
                        {
                            deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                        }
                    }
                }
                // zip code does NOT support Sunday delivery
                else
                {
                   deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                }

                // Exotic floral items cannot be delivered same day
                if ( element.getAttribute("productsubtype").equals(EditProductConstants.PRODUCT_SUBTYPE_EXOTIC) ||
                     element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_FRECUT) ||
                     element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SPEGFT) ||
                     element.getAttribute("deliverytype").equals(EditProductConstants.DELIVERY_TYPE_INTERNATIONAL) )
                {
                    deliveryDateParm.setDeliverTodayFlag(Boolean.FALSE);
                }

                // set the exception dates for product
                deliveryDateParm.setExceptionCode(element.getAttribute("exceptioncode"));
                try {
                    deliveryDateParm.setExceptionFrom(sdfInput.parse(element.getAttribute("exceptionstartdate")));
                    deliveryDateParm.setExceptionTo(sdfInput.parse(element.getAttribute("exceptionenddate")));
                }
                catch (Exception e) {
                }

                // set the vendor no delivery dates for product
                HashMap vendorNoDeliverFrom = new HashMap();
                HashMap vendorNoDeliverTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendordelivblockstart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                    {
                        vendorNoDeliverFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendordelivblockstart" + y)));
                        vendorNoDeliverTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendordelivblockend" + y)));
                    }
                }

                if ( vendorNoDeliverFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoDeliverFrom(vendorNoDeliverFrom);
                    deliveryDateParm.setVendorNoDeliverTo(vendorNoDeliverTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoDeliverFlag(Boolean.FALSE);
                }

                // set the vendor no ship dates for product
                HashMap vendorNoShipFrom = new HashMap();
                HashMap vendorNoShipTo = new HashMap();
                for ( int y=1; y < 7; y++ )
                {
                    String vendorFrom = element.getAttribute("vendorshipblockstart" + y);

                    if ( vendorFrom != null && !vendorFrom.trim().equals("") )
                    {
                        vendorNoShipFrom.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorshipblockstart" + y)));
                        vendorNoShipTo.put(String.valueOf(y-1), sdfInput.parse(element.getAttribute("vendorshipblockend" + y)));
                    }
                }

                if ( vendorNoShipFrom.size() > 0 )
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.TRUE);
                    deliveryDateParm.setVendorNoShipFrom(vendorNoShipFrom);
                    deliveryDateParm.setVendorNoShipTo(vendorNoShipTo);
                }
                else
                {
                    deliveryDateParm.setVendorNoShipFlag(Boolean.FALSE);
                }

                Date firstArrivalDate = null;

                // set Florist delivery information
                if ( element.getAttribute("shipmethodflorist") != null &&
                     element.getAttribute("shipmethodflorist").equals(EditProductConstants.YES) )
                {
                    deliveryDateParm.setShipMethodFlorist(true);

                    if ( element.getAttribute("servicecharge") != null &&
                          !element.getAttribute("servicecharge").trim().equals("") )
                    {   
                            floralServiceChargeSet = true;
                            deliveryDateParm.setFloralServiceCharge(new BigDecimal(element.getAttribute("servicecharge")));
                    }
                }

                // Check delivery type for carrier ship methods
                if ( element.getAttribute("shipmethodcarrier") != null &&
                     element.getAttribute("shipmethodcarrier").equals(EditProductConstants.YES) )
                {
                    // Only set Sunday Flag to false if this is not a same day gift product
                    if(!deliveryDateParm.getProductType().equals(EditProductConstants.PRODUCT_TYPE_SDG) &&
                       !deliveryDateParm.getProductType().equals(EditProductConstants.PRODUCT_TYPE_SDFC))
                    {
                        deliveryDateParm.setSundayDelivery(Boolean.FALSE);
                    }
                    deliveryDateParm.setShipMethodCarrier(true);
                    // Get the shipping methods for this product
                    HashMap shippingMethods = searchUTIL.getProductShippingMethods(element.getAttribute("productid"), element.getAttribute("standardprice"), element.getAttribute("shippingkey"));
                    deliveryDateParm.setShipMethods(shippingMethods);

                    // build values of No Ship Days for product
                    HashSet noShipDays = new HashSet();
                    if ( element.getAttribute("sundayflag") != null &&
                         element.getAttribute("sundayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.SUNDAY));
                    }
                    if ( element.getAttribute("mondayflag") != null &&
                         element.getAttribute("mondayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.MONDAY));
                    }
                    if ( element.getAttribute("tuesdayflag") != null &&
                         element.getAttribute("tuesdayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.TUESDAY));
                    }
                    if ( element.getAttribute("wednesdayflag") != null &&
                         element.getAttribute("wednesdayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.WEDNESDAY));
                    }
                    if ( element.getAttribute("thursdayflag") != null &&
                         element.getAttribute("thursdayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.THURSDAY));
                    }
                    if ( element.getAttribute("fridayflag") != null &&
                         element.getAttribute("fridayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.FRIDAY));
                    }
                    if ( element.getAttribute("saturdayflag") != null &&
                         element.getAttribute("saturdayflag").equals(EditProductConstants.NO) )
                    {
                        noShipDays.add(new Integer(Calendar.SATURDAY));
                    }
                    deliveryDateParm.setNoShipDays(noShipDays);
                }

                // if the product is not available or is codified and not available in the zip code
                // set flag on order for later processing
                String codifiedAvailable = element.getAttribute("codifiedavailable");
                String codifiedSpecialFlag = element.getAttribute("codifiedspecial");
                String deliverableFlag = element.getAttribute("codifieddeliverable");
                String codifiedProduct = element.getAttribute("codifiedproduct");
                String status = element.getAttribute("status");

                // set codified flags in delParms
                deliveryDateParm.setCodifiedProduct(codifiedProduct);
                deliveryDateParm.setCodifiedAvailable(codifiedAvailable);
                deliveryDateParm.setCodifiedSpecialFlag(codifiedSpecialFlag);
                deliveryDateParm.setCodifiedDeliverable(deliverableFlag);

                if (
                    // product is unavaiable if status is "U"
                    (status != null &&
                      status.equals("U")) ||

                    // product is unavaiable if it is a regular codified product
                    // and it is set as unavailable for this zip code in the
                    // CODIFIED_PRODUCTS table.  It also must not be carrier delivered
                     (codifiedAvailable != null &&
                      codifiedAvailable.equals(GeneralConstants.NO) &&
                      (codifiedSpecialFlag != null &&
                       codifiedSpecialFlag.equals(GeneralConstants.NO)) &&
                       !deliveryDateParm.isShipMethodCarrier()) ||

                    // product is unavaiable if it is a regular codified product 
                    // and GNADD is at level 3
                     (codifiedProduct != null &&
                      codifiedProduct.equals(GeneralConstants.YES) &&
                      codifiedSpecialFlag != null &&
                      codifiedSpecialFlag.equals(GeneralConstants.NO) &&
                      oeGlobalParms.getGNADDLevel().equals("3"))
                    )
                {
                    flagVO.setDisplayProductUnavailable(EditProductConstants.YES);
                }
                else
                {
                    flagVO.setDisplayProductUnavailable(EditProductConstants.NO);
                }


                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                if (isDomesticFlag && 
                    (deliveryDateParm.getZipCodeGNADDFlag().equals("Y") &&
                     deliveryDateParm.getGlobalParms().getGNADDLevel().equals("0")))
                {
                    // only display if has not been displayed before
                    if ( deliveryDateParm.getProductType().equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        flagVO.setDisplaySpecialtyGift("Y");
                    }
                }
                
            }

            String orderIntlFlag = "";
            if(isDomesticFlag)
            {
              orderIntlFlag = "N";
            }
            else
            {
              orderIntlFlag = "Y";
            }

            OrderVO order = buildOSPVO(productId, orderIntlFlag, sourceCode);
            deliveryDateParm.setOrder(order);
            deliveryDateParm.setItemNumber(1);



            deliveryDateParm.setGlobalParms(oeGlobalParms);
            deliveryDateParm.setHolidayDates((HashMap)holidayMap);

            // retrieve the delivery range information from the XML and put into the
            // delivery date parameter object
            deliveryDateUtil.getDeliveryRanges(EditProductConstants.XML_PRODUCT_LIST, document, deliveryDateParm);
            boolean needsToCloseConn = false;
            try {
            	
            	//This is added to check the connection details for mercent order calls.
                if(deliveryDateParm.getConn() == null) {
                	processConnection(deliveryDateParm);
                	needsToCloseConn = true;
                }
	            if(getFirstAvailableDate)
	            {
	                deliveryDates = new ArrayList();
	                deliveryDates.add(PASServiceUtil.getPASFirstAvailableDate(productId, null, zipCode, sendToCountry));	                
	            }
	            else
	            {
	            	int days = deliveryDateParm.getGlobalParms().getDeliveryDaysOut() + 1;
					deliveryDates = PASServiceUtil.getProductAvailableDates(productId, null, zipCode, sendToCountry, sourceCode, days);
	            }
            } catch(Exception e) {
            	logger.error("Error:",e);
            } finally {
            	if(needsToCloseConn){
            		Connection conn = deliveryDateParm.getConn();
            		if(conn != null) {
            			try {
            				conn.close();
            			} catch(SQLException e) {
            				
            			}
            		}
            	}
            }
            
            // This method will check all the same day gift rules and
            // set the availability flags on the order
            List shipMethods = getPASDeliveryMethodsFromDateList(deliveryDates);
            checkForSDGAvailable(deliveryDateParm, shipMethods,
                  flagVO,sendToState,sendToCountry);            

        }
        finally{ }
        
        return deliveryDates;
    }

    /**
     * This method takes in a list of dates objects which contain
     * ship methods.  This method then returns a distince list of 
     * all the ship methods found in those objects.
     * 
     * @param List dates Delivery Dates
     * @returns List Delivery Date Methods
     */
	public List getPASDeliveryMethodsFromDateList(List<ProductAvailVO> PASdeliveryDates) 
	{
		boolean foundFlorist = false;
		boolean foundGround = false;
		boolean foundTwoDay = false;
		boolean foundNextDay = false;
		boolean foundSaturday = false;
		List shippingMethodsToReturn = new ArrayList();

		for (int i = 0; i < PASdeliveryDates.size(); i++) 
		{
			ProductAvailVO productAvailVo = PASdeliveryDates.get(i);

			List<String> shippingMethods = PASParamUtil.getShippingMethods(productAvailVo);

			if ((shippingMethods.contains(GeneralConstants.DELIVERY_SAME_DAY_CODE))
					&& !shippingMethodsToReturn.contains(GeneralConstants.DELIVERY_FLORIST_CODE)) 
			{
				foundFlorist = true;
				shippingMethodsToReturn.add(GeneralConstants.DELIVERY_FLORIST_CODE);
			} 
			else if (shippingMethods.contains(GeneralConstants.DELIVERY_STANDARD_CODE)
					   && !shippingMethodsToReturn.contains(GeneralConstants.DELIVERY_STANDARD_CODE)) 
			{
				foundGround = true;
				shippingMethodsToReturn.add(GeneralConstants.DELIVERY_STANDARD_CODE);
			} 
			else if (shippingMethods.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE)
					&& !shippingMethodsToReturn.contains(GeneralConstants.DELIVERY_TWO_DAY_CODE)) 
			{
				foundTwoDay = true;
				shippingMethodsToReturn.add(GeneralConstants.DELIVERY_TWO_DAY_CODE);
			} 
			else if (shippingMethods.contains(GeneralConstants.DELIVERY_NEXT_DAY_CODE)
					&& !shippingMethodsToReturn.contains(GeneralConstants.DELIVERY_NEXT_DAY_CODE)) 
			{
				foundNextDay = true;
				shippingMethodsToReturn.add(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
			}
			else if (shippingMethods.contains(GeneralConstants.DELIVERY_NEXT_DAY_CODE)
					&& !shippingMethodsToReturn.contains(GeneralConstants.DELIVERY_SATURDAY_CODE)) 
			{
				foundSaturday = true;
				shippingMethodsToReturn.add(GeneralConstants.DELIVERY_SATURDAY_CODE);
			}

			if (foundFlorist && foundGround && foundTwoDay && foundNextDay
					&& foundSaturday)
			{
				break;
			}
		}
		return shippingMethodsToReturn;
	}

	/**
     * This method creates an XML document.
     * 
     * @param String xml root name. Name use for XML root element.
     * @returns Document 
     */
    public static Document createDocument(String xml)
        throws Exception
    {

        if(xml == null || xml.equals(""))
            return null;

        Document document = DOMUtil.getDocument(xml);

        return document;
    }



   /** This method converts an XML document to a String. 
     * @param Document document to convert 
     * @return String version of XML */
    public static String convertDocToString(Document xmlDoc) throws Exception
    {
            String xmlString = "";       
          
            StringWriter s = new StringWriter();
            DOMUtil.print(xmlDoc, new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }    


  /** 
   * This mehtod creates an Order Srub Project order VO using the
   * passed in data. 
   * 
   * @String item id
   * @String reciepientIntlFlag
   * @param String sourceCode
   * @returns OrderVO
   * */
  private OrderVO buildOSPVO(String item, String recipientIntlFlag, String sourceCode) throws Exception
  {
      OrderVO order = new OrderVO();
      //TODO The origin is fetched from amazon schema which
      // ideally should not happen. Added mercent logic to 
      //fetch the origin details from the channel mapping details.
      String origin = getOrigin(sourceCode);
      if(origin == null || origin == "") {
    	origin = getMercentOriginBySourceCode(sourceCode); 
      }
      order.setOrderOrigin(origin);

      OrderDetailsVO detail = new OrderDetailsVO();
      List details = new ArrayList();
      detail.setProductId(item);

      RecipientAddressesVO addr = new RecipientAddressesVO();
      addr.setInternational(recipientIntlFlag);
      List addrs = new ArrayList();
      addrs.add(addr);

      RecipientsVO recip = new RecipientsVO();
      List recips = new ArrayList();
      recip.setRecipientAddresses(addrs);
      recips.add(recip);

      detail.setRecipients(recips);
      details.add(detail);

      order.setOrderDetail(details);

      return order;
  }
  
   /**
     * Retrieve the origin based on the source code passed in.
     * We needed to add this because of code that was added to the Delivery Date UTIL class.
     * There are now checks in there because of the Amazon project, which check specifically
     * for the Amazon origin.  There is functionality in place in that class
     * that only gets executed for Amazon orders only.  That is the only reason why
     * we need to set the origin in the order that we pass to the Delivery Date UTIL class.
     * 
     * @param String sourceCode
     * @return String 
     * 
     * 
     * emueller, 5/25/2005.
     * I added a try-finally block to the close the DB connection. This was 
     * causing a connection leak.  This was done as part of the PHASE_III 
     * changes.
     * 
     */
    private String getOrigin(String sourceCode) throws IOException, ParserConfigurationException, 
            SQLException, Exception
    {

        DataRequest dataRequest = null;
        String origin = "";
          
      try{

          dataRequest = DataRequestHelper.getInstance().getDataRequest();

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          //get source code info                    
          dataRequest.setStatementID("GET_ORIGIN_FROM_SOURCE");
          dataRequest.addInputParam("IN_SOURCE_CODE",sourceCode);
          
          Map outputs = (Map) dataAccessUtil.execute(dataRequest);
      
          status = (String) outputs.get(STATUS_PARAM);
          if(status.equals("N"))
          {
              message = (String) outputs.get(MESSAGE_PARAM);
              
              throw new Exception(message);
          }
          else
          {
              if( outputs.get("RegisterOutParameterOrigin") != null)
                origin= outputs.get("RegisterOutParameterOrigin").toString();
          }
      }
      finally{
          if(dataRequest !=null && dataRequest.getConnection() != null)
          {
              dataRequest.getConnection().close();
          }      
      }
          return origin;
    }

  /** This method takes in a country code and returns
   * a flag indicating if the passed in country is domestic
   * or not.
   * 
   * @param String country code
   * @returns Boolean  True=Domestic 
   * */
  public boolean isDomestic(String country)
    throws IOException, ParserConfigurationException, 
            SQLException, Exception
  {

      boolean domesticFlag = false;


      Map paramMap = new HashMap();
      paramMap.put("IN_COUNTRY_ID",country);

      // Get the order header infomration
      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

      try{

        dataRequest.setStatementID("GET_COUNTRY_MASTER_BY_ID");
        dataRequest.setInputParams(paramMap);
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
        if(rs == null || !rs.next())
        {
            //default to domsetic
            domesticFlag = true;
        }
        else
        {
            //get value based on what is in DB
            String domValue = (rs.getObject(2)).toString();          
            if(domValue == null || domValue.equals(EditProductConstants.DELIVERY_TYPE_DOMESTIC))
            {
              domesticFlag = true;
            }
        }
      }
      finally{
          if(dataRequest !=null && dataRequest.getConnection() != null)
          {
              dataRequest.getConnection().close();
          }      
      }
      
      return domesticFlag;

  }

    /**
     * Get product detail information.  This method will get product or upsell 
     * product information.  It generates the xml used on the product detail page.
     * 
     * @param String sourceCode
     * @param String zipCode
     * @param String country
     * @param String productId
     * @param String searchType
     * @boolean domesticFlag
     * @String state
     * @String pricingCode
     * @String domesticServiceFee
     * @String internationalServiceFee
     * @String partnerId
     * @List promotionList
     * @String rewardType
     * @String ProductFlagsVO
     * @String occasion
     * @String pageName
     * @String company
     * @String deliveryDateDisplay
     * @String city
     * @String deliveryDate
     * @String origDate
     * @HashMap pageData
     * @returns Document
     * @throws Exception
     */
    public Document getProductByID(String sourceCode, String zipCode, String country,
              String productId, String searchType, boolean domesticFlag, 
              String state, String pricingCode, String domesticServiceFee,
              String internationalServiceFee, String partnerId, List promotionList,
              String rewardType, ProductFlagsVO flagsVO, String occasion,
              String pageName, String company, String deliveryDateDisplay,
              String city, String deliveryDate, String origDate, HashMap pageData) throws Exception
	{
        Document document = null;

        long searchCount = 0;

        //reset flag
         flagsVO.setUpsellExists(false);        

         DataRequest dataRequest = null;
      
        try
        {

            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            SearchUTIL searchUTIL = new SearchUTIL();

            boolean simpleSearch = true;

            //get global parameter values from FTD_APPS
            OEParameters oeParms = searchUTIL.getGlobalParms();

            //is this guy domestic?
            String domIntlFlag = "";
            if ( domesticFlag )
            {
                domIntlFlag =  EditProductConstants.DELIVERY_TYPE_DOMESTIC;
            }
            else
            {
                 domIntlFlag =  EditProductConstants.DELIVERY_TYPE_INTERNATIONAL;
            }        


             /* This methods creates an XML document which contains the following:
             *    Product Master data for the given product
             *    Global Paramater record
             *    Holidays for the given country
             *    Zip code flags
             *    Delivery Date range restrictions
             *    All the shipping methods in FTD_APPS*/
            Document productList = getProductDetail( productId,  sourceCode,  domIntlFlag,
                                           zipCode,  country, deliveryDate);

            
            //get count of items in list
            searchCount =  DOMUtil.selectNodes(productList, "//product").getLength();

            document = DOMUtil.getDocument();


            //The page builder method contains all the business logic to calculate prices,
            //discounts, ship methods... for each of the products.  The page builder
            //methods puts all this information into the XML.  The method also sets
            //various flags that indicate whether or not popup messages needs to be 
            //shown to the user.
            Element productDetailElementList = searchUTIL.pageBuilder(false, false, 
              productList, 1, null, null,
              domesticFlag, domesticServiceFee, internationalServiceFee,
              state, country, pricingCode,
              partnerId, promotionList,rewardType,zipCode,
              flagsVO, sourceCode, origDate);

            XMLEncoder.addSection(document, productDetailElementList.getChildNodes());
            
            String  isFloristAvailable = "N";
            String  isDropshipAvailable = "N";
            
            String shippingMethod=null;


            if ( simpleSearch )
            {
                String countryId = null;
                String zipGnaddFlag = null;
                String productType = null;
                String gnaddLevel = null;
                String productSubType = null;

                Element element = null;
                NodeList nl = null;
                String xpath = null;

                nl = productDetailElementList.getElementsByTagName("product");
                Element productDetailElement = (Element) nl.item(0);
                if ( productDetailElement != null )
                {
                    productId = productDetailElement.getAttribute("productid");
                    pageData.put("product_id",productId);
                }

                // retrieve the Global Parameter information
                xpath = "//parm";

                nl = DOMUtil.selectNodes(productList, xpath);

                if ( nl.getLength() > 0 )
                {
                    element = (Element) nl.item(0);
                    gnaddLevel = element.getAttribute("gnaddlevel");
                }
                else
                {
                    gnaddLevel = "0";
                }



                // retrieve the product information
                xpath = "//product";
                //q = new XPathQuery();
                nl = DOMUtil.selectNodes(productList, xpath);

                if (nl.getLength() > 0)
                {
                    element = (Element) nl.item(0);
                    zipGnaddFlag = element.getAttribute("zipgnaddflag").trim();
                    productType = element.getAttribute("producttype");
                }
                else
                {
                    zipGnaddFlag = "N";
                }
 
                logger.info("SN: getting productsubtype " + element.getAttribute("productsubtype"));
                productSubType = element.getAttribute("productsubtype");
                
                // country is Canada, Puerto Rico, Virgin Islands, or International
                if ( countryId != null &&
                     (countryId.equals("CA") ||       // Canada
                     countryId.equals("PR") ||       // Puerto Rico
                     countryId.equals("VI") ))        // Virgin Islands
                {
                    // Zip code does not exist or is shut down,
                    // so can not delivery any products
                    if ( (zipGnaddFlag.equals("Y")) &&
                         (gnaddLevel.equals("0")) )
                    {
                        // only display if has not been displayed before
                         pageData.put("displayNoProductPopup", "Y");


                    }
                    // can only delivery floral products
                    else if ( productType != null && !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) &&
                             (!productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) &&
                              !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                    {
                        pageData.put("displayFloristPopup", "Y");

                        // set order flag so will not be displayed unless zip code changed later

                    }
                }
                
                // zip code does not exist or is shut down
                // so can only delivery Carrier delivered products
                else if ( (zipGnaddFlag.equals("Y")) &&
                          (gnaddLevel.equals("0")) )
                {
                    // only display if has not been displayed before
                    if ( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
                    {
                        pageData.put("displaySpecGiftPopup", "Y");

                   }
                }

                // Logic for same day gift pop-ups
                if(productType != null && 
                   (productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDG) ||
                    productType.equals(GeneralConstants.OE_PRODUCT_TYPE_SDFC)))
                {
                    pageData.put("displayNoCodifiedFloristHasCommonCarrier", flagsVO.getDisplayNoCodifiedFloristHasCommonCarrier());
                    pageData.put("displayNoFloristHasCommonCarrier", flagsVO.getDisplayNoFloristHasCommonCarrier());
                    pageData.put("displayProductUnavailable", flagsVO.getDisplayProductUnavailable());

                }
                
                //SN: Added these lines for Freeship product                
                if(productSubType != null && (productSubType.equals(GeneralConstants.OE_PRODUCT_SUBTYPE_FREESHIP))){
                	flagsVO.setDisplayPersonalizedProduct(EditProductConstants.YES);
                	pageData.put("displayPersonalizedProduct",flagsVO.getDisplayPersonalizedProduct());
                    logger.info("SN: productType: " + productSubType + " displayPersonalizedProduct: " + flagsVO.getDisplayPersonalizedProduct());
                }
                
               
                //#587  - Flex-Fill 
                // Added the changes as per requirement
                // Displaying of Addon in Edit product screen In Order Scrub
                // Displaying (vendor/ floral ) addon based on the delivery type.
                // In Edit Product -If the user will be searching is SDFC product- then check the ship method of the order
                // Else it will check with the carrier flag/florist flag based on ship method.
                
                shippingMethod = (String)pageData.get("shipping_method");
				if ((element.getAttribute("producttype") != null) && (element.getAttribute("producttype").equals(EditProductConstants.PRODUCT_TYPE_SDFC)))
				{
				if (shippingMethod == null || ("").equalsIgnoreCase(shippingMethod) || (EditProductConstants.DELIVERY_SAME_DAY_CODE).equalsIgnoreCase(shippingMethod)) {
						isFloristAvailable = EditProductConstants.YES;
					}
				
				if ((EditProductConstants.DELIVERY_NEXT_DAY_CODE).equalsIgnoreCase(shippingMethod) || (EditProductConstants.DELIVERY_TYPE_TWO_DAY_CODE).equalsIgnoreCase(shippingMethod)
							|| (EditProductConstants.DELIVERY_STANDARD_CODE).equalsIgnoreCase(shippingMethod) 
							|| (EditProductConstants.DELIVERY_SATURDAY_CODE).equalsIgnoreCase(shippingMethod))
				{
						isDropshipAvailable = EditProductConstants.YES;
				}
				}

				else {
					if (element.getAttribute("shipmethodflorist") != null
							&& element.getAttribute("shipmethodflorist")
									.equals(EditProductConstants.YES)) {
						isFloristAvailable = EditProductConstants.YES;
					}

					if (element.getAttribute("shipmethodcarrier") != null
							&& element.getAttribute("shipmethodcarrier")
									.equals(EditProductConstants.YES)) {
						isDropshipAvailable = EditProductConstants.YES;
					}
				}
            }

            // indicates whether product is codified special and should be displayed
            pageData.put("displayCodifiedSpecial", getDisplayCodifiedSpecial("productList", productList));
            if(!((String)pageData.get("displayCodifiedSpecial")).equals("Y"))
            {
                pageData.put("displayProductUnavailable", flagsVO.getDisplayProductUnavailable());
            }

            // personalized products
            pageData.put("displayPersonalizedProduct", flagsVO.getDisplayPersonalizedProduct());

            
            //This method creates an XML document containing scripting information, produ
            //occasion information, and the product's subcode information.
           Document extraProdDetail = getExtraProductDetail( company,  pageName,  occasion,  productId, sourceCode ,isFloristAvailable ,isDropshipAvailable);
           
            XMLEncoder.addSection(document, (extraProdDetail.getChildNodes()));

            //get global params
            Document globalParmsXML = DOMUtil.getDefaultDocument();
            dataRequest.reset();
            dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP_XML");
            globalParmsXML =(Document) DataAccessUtil.getInstance().execute(dataRequest);
            logger.debug("globalParms: " + convertDocToString(globalParmsXML));
            
            String standardLabel = null;
            String deluxeLabel = null;
            String premiumLabel = null;
            NodeList n = DOMUtil.selectNodes(globalParmsXML, "/params/parm");
            if (n.getLength() > 0)
            {
              Element globalData = (Element) n.item(0);
              standardLabel = globalData.getAttribute("standardlabel");
              deluxeLabel = globalData.getAttribute("deluxelabel");
              premiumLabel = globalData.getAttribute("premiumlabel");
            }

            pageData.put("standard_label", standardLabel);
            pageData.put("deluxe_label", deluxeLabel);
            pageData.put("premium_label", premiumLabel);

            pageData.put("displaySpecialFee", flagsVO.getSpecialFeeFlag());

            //add all the other page data fields
            DOMUtil.addSection(document, "pageData", "data", pageData, true);

            //If no products were returned, check if it is an upsell product
            String xpath = "//product";
            NodeList nl = DOMUtil.selectNodes(productList, xpath);
            if (nl.getLength() == 0)
            {
                dataRequest.reset();
                dataRequest.addInputParam("IN_UPSELL_MASTER_ID", productId);
                dataRequest.setStatementID("GET_UPSELL_DETAILS");
                Document upsellDetailXML = (Document) DataAccessUtil.getInstance().execute(dataRequest);

                xpath = "//upsellDetail";
                nl = DOMUtil.selectNodes(upsellDetailXML, xpath);

                //if upsell was found
                if(nl.getLength() > 0) {
                    HashMap upsellMasterData = new HashMap();                 

                    String upsellProductId = null;
                    String upsellProductName = null;
                    Element upsellElement = null;
                   
                    String skipUpsell = "N";
                    String noneAvailable = "Y";
                    String baseAvailable = "Y";
                    String showScripting = "N";

                    document = DOMUtil.getDocument();
                    DOMUtil.addSection(document, "pageData", "data", pageData, true);                    
                    flagsVO.setUpsellExists(true);

                    //for each upsell in the list
                    for(int i=0; i < nl.getLength(); i++) {
                        upsellElement = (Element) nl.item(i);
                        upsellProductId = upsellElement.getAttribute("upselldetailid");
                        upsellProductName = upsellElement.getAttribute("upselldetailname");
                        upsellProductName = upsellProductName.replaceAll("\\<.*?>","");

                        //set domestic flag
                        String domIntlValue = "";
                        if ( domesticFlag )
                        {
                            domIntlValue = GeneralConstants.OE_DELIVERY_TYPE_DOMESTIC;
                        }
                        else
                        {
                            domIntlValue = GeneralConstants.OE_DELIVERY_TYPE_INTERNATIONAL;
                        }

                         /* This methods creates an XML document which contains the following:
                         *    Product Master data for the given product
                         *    Global Paramater record
                         *    Holidays for the given country
                         *    Zip code flags
                         *    Delivery Date range restrictions
                         *    All the shipping methods in FTD_APPS*/
                        Document searchResultList = getProductDetail( upsellProductId,  sourceCode,  domIntlValue,
                                           zipCode,  country, deliveryDate);
            
                        //The page builder method contains all the business logic to calculate prices,
                        //discounts, ship methods... for each of the products.  The page builder
                        //methods puts all this information into the XML.  The method also sets
                        //various flags that indicate whether or not popup messages needs to be 
                        //shown to the user.
                        productDetailElementList = searchUTIL.pageBuilder(true,true, 
                            searchResultList, 1, null, null,
                               domesticFlag,  domesticServiceFee,  internationalServiceFee,
                               state,  country, pricingCode,
                               partnerId,  promotionList, rewardType,  zipCode,
                               flagsVO, sourceCode, origDate);
              

                        Element productDetailElement = (Element) productDetailElementList.getElementsByTagName("product").item(0);

                        //if nothing found exist
                        if(productDetailElement == null)
                        {
                          return document;
                        }
                        
                        productDetailElement.setAttribute("upsellsequence", new Integer(i + 1).toString());
                        productDetailElement.setAttribute("upsellproductname", upsellProductName);

                        //This method sets the 'specialunavailable' attribute for the productDetailElement
                        createUpsellDetail(productDetailElement, searchResultList, country,state,flagsVO);
               
                        // Check if upsell page should be skipped
                        if(productDetailElement.getAttribute("upsellsequence") != null && productDetailElement.getAttribute("upsellsequence").equals("1"))
                        {
                            upsellMasterData.put("masterId", upsellElement.getAttribute("upsellmasterid"));
                            upsellMasterData.put("masterName", upsellElement.getAttribute("upsellmastername"));
                            upsellMasterData.put("masterDescription", upsellElement.getAttribute("upsellmasterdescription"));
                            upsellMasterData.put("masterStatus", upsellElement.getAttribute("upsellmasterstatus"));

                            //if unavailable skip the upsell
                            if((productDetailElement.getAttribute("status") != null
                               && productDetailElement.getAttribute("status").equals("U"))
                               || (productDetailElement.getAttribute("specialunavailable") != null
                               && productDetailElement.getAttribute("specialunavailable").equals("Y")))
                            {
                                skipUpsell = "N";
                                baseAvailable = "N";
                            }
                            else
                            {
                                skipUpsell = upsellProductId;
                                noneAvailable = "N";
                            }
                        }
                        else
                        {
                            if(productDetailElement.getAttribute("status") != null
                               && productDetailElement.getAttribute("status").equals("A")
                               && productDetailElement.getAttribute("specialunavailable") != null
                               && productDetailElement.getAttribute("specialunavailable").equals("N"))
                            {
                                skipUpsell = "N";
                                noneAvailable = "N";

                                // If the last product is available show scripting
                                if(i+1 == nl.getLength()) {
                                    showScripting = "Y";
                                }
                            }
                        }

                        XMLEncoder.addSection(document, productDetailElementList.getChildNodes());
                    }


                    //get scripting
                    dataRequest.reset();
                    dataRequest.addInputParam("IN_PAGE_ID", "upsell");
                    dataRequest.addInputParam("IN_COMPANY_ID", company);
                    dataRequest.setStatementID("GET_SCRIPT_BY_PAGE_SFMB");
                    extraProdDetail = (Document) DataAccessUtil.getInstance().execute(dataRequest);

                    //set upsell flags
                    flagsVO.setUpsellSkip(skipUpsell);
                    upsellMasterData.put("skipUpsell", skipUpsell);
                    upsellMasterData.put("noneAvailable", noneAvailable);
                    upsellMasterData.put("baseAvailable", baseAvailable);
                    upsellMasterData.put("showScripting", showScripting);

                    XMLEncoder.addSection(document, (extraProdDetail.getChildNodes()));
                  //  XMLEncoder.addSection(document, "orderData", "data", orderDataMap);
                    XMLEncoder.addSection(document, "upsellMaster", "upsellDetail", upsellMasterData);

                }
            }

        }
        finally
        {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }          
        }

        return document;
	}
    
    /**
     * Check product attribute source restriction.
     * @return the error message if applies.
     */
    public Document appendProductAttributeExclusions(String productId, String sourceCode, Document xml) throws Exception
    {
    	
    	DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
    	try{
        // Validate product attributes.
        SourceProductUtility spUtil = new SourceProductUtility();
        String productAttrRestricError = "";
        productAttrRestricError = spUtil.getProductAttributeRestrictionMsg(sourceCode, productId, dataRequest.getConnection());

        if(productAttrRestricError != null && productAttrRestricError.length() > 0) {
            HashMap exclusionMap = new HashMap();
            exclusionMap.put("productAttributeExclusion", productAttrRestricError);
            DOMUtil.addSection(xml, "productAttributeExclusion", "alertMessages", exclusionMap, true);
        }

        
    	}catch(Exception e){
    		logger.error("Error occured in SearchUTIL :: appendProductAttributeExclusions");
    		throw(e);
        }finally{
        	//closing the connection to fix the connection leaks.
        	if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()){
        		dataRequest.getConnection().close();
        	}
        }
        return xml;
    }  

  /**
   * This method creates an XML document containing scripting information, 
   * occasion information, and the product's subcode information.
   * 
   * @param String company
   * @param String pageId
   * @param String occasionid
   * @param String productId
   * @returns Document
   * 
   */
  private Document getExtraProductDetail(String company, String pageId, String occasionId, String productId, String sourceCode ,String isFloristAvailable , String isDropshipAvailable) throws Exception
  {
      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

      Document responseDocument = null;
      
      try{
          javax.xml.parsers.DocumentBuilder docBuilder = DOMUtil.getDocumentBuilder();
          Document doc = docBuilder.newDocument();
          Element root = (Element) doc.createElement("extraInfo");
          doc.appendChild(root);
          responseDocument = (Document)doc;

          String nullString = null;

          //get scripting
          dataRequest.reset();
          dataRequest.setStatementID("GET_SCRIPT_BY_PAGE_SFMB");
          dataRequest.addInputParam("IN_COMPANY_ID", company);
          dataRequest.addInputParam("IN_PAGE_ID", pageId);
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

          //subcode information
          dataRequest.reset();
          dataRequest.setStatementID("GET_PRODUCT_SUBCODES_ACTIVE");
          dataRequest.addInputParam("IN_PRODUCT_ID", productId);
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());           

          //get addon information
          AddOnUtility aau = new AddOnUtility();
          int occasion;
          try {
            occasion = Integer.parseInt(occasionId);
          } catch (Exception e) {
            logger.error("getExtraProductDetail - Occasion ID is non-numeric: " + occasionId);
            occasion = 0;
          }
          HashMap aaMap = aau.getActiveAddonListByProductIdAndOccasionAndSourceCodeAndDeliveryType(productId, occasion, sourceCode, false, isFloristAvailable , isDropshipAvailable , dataRequest.getConnection());
          DOMUtil.addSection(responseDocument, ((Document) aau.convertAddOnMapToXML(aaMap)).getChildNodes()); 
      }
      finally
      {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }
      }

      return responseDocument;
  }


  /**
   * This methods creates an XML document which contains the following:
   *    Product Master data for the given product
   *    Global Paramater record
   *    Holidays for the given country
   *    Zip code flags
   *    Delivery Date range restrictions
   *    All the shipping methods in FTD_APPS
   *    
   * @String productId
   * @String sourceCode
   * @String domIntlFlag
   * @String zipCode
   * @String country
   * @returns Document
   */
  private Document getProductDetail(String productId, String sourceCode, String domIntlFlag,
                          String zipCode, String country, String deliveryDate) throws Exception
  {

      DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();

      Document responseDocument = null;
      
      try{
          responseDocument = DOMUtil.getDocument();

          String nullString = null;
          if (deliveryDate == null) deliveryDate = "";

          //product details
          logger.debug("get_product_details_sdg: " + productId + " " + sourceCode + " " + 
                          domIntlFlag + " " + zipCode + " " + deliveryDate);
          dataRequest.reset();
          dataRequest.setStatementID("GET_PRODUCT_DETAILS_SDG");
          dataRequest.addInputParam("IN_PRODUCT_ID", productId);
          dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
          dataRequest.addInputParam("IN_DOMESTIC_INTL_FLAG", domIntlFlag);
          dataRequest.addInputParam("IN_ZIP_CODE", zipCode);
          dataRequest.addInputParam("IN_COUNTRY_ID", nullString);
          dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

          //globals
          dataRequest.reset();
          dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP_XML");
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

          //holidays by country
          dataRequest.reset();
          dataRequest.setStatementID("GET_HOLIDAYS_BY_COUNTRY");
          dataRequest.addInputParam("IN_COUNTRY_ID", country);
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

          //zipcode info
          dataRequest.reset();
          dataRequest.setStatementID("GET_ZIPCODE_INFO");
          dataRequest.addInputParam("IN_ZIP_CODE_ID", zipCode);
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

          //delivery date ranges
          dataRequest.reset();
          dataRequest.setStatementID("GET_DELIVERY_DATE_RANGES");
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());           

          //ship methods
          dataRequest.reset();
          dataRequest.setStatementID("GET_ALL_SHIPPING_METHODS");
          DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());           
      }
      finally
      {
            if(dataRequest !=null && dataRequest.getConnection() != null)
            {
                dataRequest.getConnection().close();
            }
      }

        return  responseDocument;
  }


    /**
     * This method returns a flag related to codified products.
     * 
     * @params String elementName Not currently used
     * @params XMLDoucment which contains product information
     * @returns String "Y" or "N"
     */
    private String getDisplayCodifiedSpecial(String elementName, Document xmlResponse) throws Exception
    {

        // Get global params
        SearchUTIL searchUTIL = new SearchUTIL();
        OEParameters oeGlobalParms = searchUTIL.getGlobalParms();

        // retrieve the product special codified flag
        String xpath = "//product";
        NodeList nl = DOMUtil.selectNodes(xmlResponse, xpath);
        Element product = null;
        String codifiedSpecialFlag = "N";
        String deliverableFlag = "N";
        if(nl.getLength() > 0)
        {
            product = (Element) nl.item(0);
            codifiedSpecialFlag = product.getAttribute("codifiedspecial");
            if(codifiedSpecialFlag == null) codifiedSpecialFlag = "N";

            deliverableFlag = product.getAttribute("codifieddeliverable");
            if(deliverableFlag == null) deliverableFlag = "N";
        }

        // retrieve the florist information
        //xpath = "productList/floristListData/data";
        //q = new XPathQuery();
        //NodeList floristNodelist = q.query(dataResponse.getDataVO().getData().toString(), xpath);
        String ret = "N";

        // check for at least one florist that has this product available
        if(codifiedSpecialFlag.equals("Y") && (deliverableFlag.equals("N")) /*&& oeGlobalParms.getGNADDLevel().equals("0")*/)
        {
            ret = "Y";
        }

        return ret;
    }

    /**
     * This method sets the 'specialunavailable' attribute for the productDetailElement
     * 
     * @param Element productDetailElement
     * @param XMLdocument ProductList
     * @param String country
     * @param String state
     * @param ProductFlagsVO flagsVO
     * @returns void
     * @throws Exception
     */
    private void createUpsellDetail(Element productDetailElement, Document productList,
            String country, String state, ProductFlagsVO flagsVO) throws Exception {
        String countryId = null;
        String zipGnaddFlag = null;
        String productType = null;
        String gnaddLevel = null;

        Element element = null;
        NodeList nl = null;
        String xpath = null;

        productDetailElement.setAttribute("specialunavailable","N");

        //obtain the gnadd level
        xpath = "//parm";
        nl = DOMUtil.selectNodes(productList, xpath);
        if ( nl.getLength() > 0 )
        {
            element = (Element) nl.item(0);
            gnaddLevel = element.getAttribute("gnaddlevel");
        }
        else
        {
            gnaddLevel = "0";
        }

        //get the product
        xpath = "//product";
        nl = DOMUtil.selectNodes(productList, xpath);
        if (nl.getLength() > 0)
        {
            element = (Element) nl.item(0);
            zipGnaddFlag = element.getAttribute("zipgnaddflag").trim();
            productType = element.getAttribute("producttype");
        }
        else
        {
            zipGnaddFlag = "N";
        }

        if ( countryId != null &&
             (countryId.equals("CA") ||       // Canada
             countryId.equals("PR") ||        // Puerto Rico
             countryId.equals("VI") ))        // Virgin Islands
        {
            if ( (zipGnaddFlag.equals("Y")) &&
                 (gnaddLevel.equals("0")) )
            {
                productDetailElement.setAttribute("specialunavailable","Y");
            }
             else if ( productType != null && !productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
            {
                productDetailElement.setAttribute("specialUnavailable","Y");
            }
        }
        else if ( (zipGnaddFlag.equals("Y")) &&
                  (gnaddLevel.equals("0")) )
        {
            if ( productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FLORAL) )
            {
                productDetailElement.setAttribute("specialunavailable","Y");
            }
        }

        if(getDisplayCodifiedSpecial("productlist", productList).equals("Y")) {
            productDetailElement.setAttribute("specialunavailable","Y");
        }

        if(flagsVO.getDisplayProductUnavailable() != null && flagsVO.getDisplayProductUnavailable().equals("Y"))
        {
            productDetailElement.setAttribute("specialunavailable","Y");
        }

        if (state != null && (state.equals("AK") || state.equals("HI")))
        {
            if(productType != null && productType.equals(GeneralConstants.OE_PRODUCT_TYPE_FRECUT))
            {
                productDetailElement.setAttribute("specialunavailable","Y");
            }
        }
    }    


    /** This method returns a list of prducts.
     * @param String pageNumber The current page number
     * @param String indexId The category index id
     * @param String orderDeliveryZipCode The zip code to where product is being delivered
     * @param boolean isSendToCustomerDomestic Flag indicates if customer is domestic
     * @param String sourceCode The source code
     * @param String pricePointId The price point id for the the product
     * @param String sendToCustomerCounter The country to where product is being sent
     * @param String scriptCode The script code for the page
     * @param String domesticServiceFee Domestic Service Fee
     * @param String internationialServiceFee Intl Service Fee
     * @param String sendToCustomerState Send to customer state
     * @param String pricingCode the products pricing code
     * @param String partnerId The partner id related to the this source code
     * @param List promotionList List or promotions available to this source code
     * @param String rewardType The reward type associated with this order 
     * @param ProductFlagsVO flagVO Flags used by the XSL page
     * @param String sortType sort value
     * @param String customerName Name of customer
     * @param HashMap pageDataMap Data which needs to be passed to the XSL page
     * @param ServletContext context
     * @throws Exception*/
    public Document getProductsByCategory(String pageNumber,String indexId,
          String orderDeliveryZipCode, boolean isSendToCustomerDomestic, 
          String sourceCode,String pricePointId,String sendToCustomerCountry,
          String scriptCode, String domesticServiceFee, String internationalServiceFee,
          String sendToCustomerState, String pricingCode, String partnerId,
          List promotionList, String rewardType,  ProductFlagsVO flagVO,
          String sortType, String customerName, HashMap pageDataMap, ServletContext context,
          String deliveryDate, String origDate)
          throws Exception
	{


      Document document = null;
      DataRequest dataRequest = null;

      long pageCount = 0;
                
        try {

            logger.info("getProductsByCategory()");
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();


            SearchUTIL searchUTIL = new SearchUTIL();

            document = DOMUtil.getDocument();   


            String intDomFlag = null;
            if ( isSendToCustomerDomestic )
            {
                intDomFlag = EditProductConstants.DELIVERY_TYPE_DOMESTIC;
            }
            else
            {
                intDomFlag = EditProductConstants.DELIVERY_TYPE_INTERNATIONAL;
            }


            //the days out from the global params
            //get global params
            Document globalParmsXML = DOMUtil.getDefaultDocument();
            dataRequest.reset();
            dataRequest.setStatementID("GLOBAL_PARMS_LOOKUP_XML");
            globalParmsXML =(Document) DataAccessUtil.getInstance().execute(dataRequest);
            logger.debug("globalParms: " + convertDocToString(globalParmsXML));
             
            String daysOut = null;
            String standardLabel = null;
            String deluxeLabel = null;
            String premiumLabel = null;
            NodeList nl = DOMUtil.selectNodes(globalParmsXML, "/params/parm");
            if (nl.getLength() > 0)
            {
              Element globalData = (Element) nl.item(0);
              daysOut = globalData.getAttribute("deliverydaysout");
              standardLabel = globalData.getAttribute("standardlabel");
              deluxeLabel = globalData.getAttribute("deluxelabel");
              premiumLabel = globalData.getAttribute("premiumlabel");
            }

            //determine the end date string by adding the daysout to the current date
            Calendar deliveryEndDate = Calendar.getInstance();
            deliveryEndDate.add(Calendar.DATE, Integer.parseInt(daysOut));
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            String endDateStr = dateFormat.format(deliveryEndDate.getTime());
            
            // Initial search hit loads all products matching the query for all pages
            if ( (pageNumber == null) || (pageNumber.equals("init")|| (pageNumber.equals("1")) ))
            {

                String categoryName = "";
                String parentCategoryName = "";

                //Load Super Index List by index id.  The index id is the
                //id of the selected category.  This stored proc retruns the
                //name of selected index id.  The proc also returns the name of the
                //parent index if one exists.
                dataRequest.reset();
                dataRequest.setStatementID("GET_PRODUCT_INDEX_BY_ID");
                dataRequest.addInputParam("IN_INDEX_ID", indexId);
                //dataRequest.addInputParam("IN_INDEX_ID", new Long(indexId));
                Document searchIndexDetails = (Document) DataAccessUtil.getInstance().execute(dataRequest);

                nl = DOMUtil.selectNodes(searchIndexDetails, "/indexdetail/data");

                //if an index was found get the category and parent category name
                if (nl.getLength() > 0)
                {
                    Element indexElement = (Element) nl.item(0);
                    categoryName = indexElement.getAttribute("indexname");
                    if(indexElement.getAttribute("parentindexname") != null && !indexElement.getAttribute("parentindexname").equals("")) {
                        parentCategoryName = indexElement.getAttribute("parentindexname");
                        categoryName = " (" + categoryName + ")";
                    }
                }


                //clear out zip code if needed
                if(orderDeliveryZipCode.equals("N/A"))
                {
                    orderDeliveryZipCode = "";
                }
                       
                // set arguments
                Map searchArgs = new HashMap();
                searchArgs.put(EditProductConstants.SEARCH_INDEX_ID, indexId);//category id
                searchArgs.put(EditProductConstants.SEARCH_SOURCE_CODE, sourceCode);
                searchArgs.put(EditProductConstants.SEARCH_ZIP_CODE, orderDeliveryZipCode);
                searchArgs.put(EditProductConstants.SEARCH_PRICE_POINT_ID, pricePointId);//this will contain a value if the products filtered by price
                searchArgs.put(EditProductConstants.SEARCH_DOMESTIC_INTL_FLAG, intDomFlag);
                searchArgs.put(EditProductConstants.SEARCH_COUNTRY_ID, sendToCustomerCountry);
                searchArgs.put("scriptCode", scriptCode);//indicates what script to display on page
                searchArgs.put(EditProductConstants.SEARCH_DELIVERY_END_DATE, endDateStr);//current date + days out

                //Perform search.  This method returns all the products which will
                //be displayed on the pages  (not just this one page).
                Document searchResultsIndex = SearchUTIL.preformProductListSearch( 
                      EditProductConstants.SEARCH_GET_PRODUCTS_BY_INDEX,
                      sourceCode, indexId, orderDeliveryZipCode, pricePointId,
                      sendToCustomerCountry, scriptCode, endDateStr, intDomFlag);

                        
                //Converts the search criteria map into a tokenized string
                String searchCriteria = searchUTIL.getSearchCriteriaString(searchArgs);

                //Sort the entire product list..this the ENTIRE list of products which
                //fall under the selected category
                if(sortType != null && !(sortType.equals("")))
                {                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));            
                    searchResultsIndex = searchUTIL.sortProductList(xslFile, searchResultsIndex);

                }
                else{
                    String filename = "/xsl/sortDisplayOrder.xsl";
                    File xslFile = new File(context.getRealPath(filename));            
                    searchResultsIndex = searchUTIL.sortProductList(xslFile, searchResultsIndex);

                }


                //This method adds page numbers to each of the product elements.  It returns
                //the total count of pages.
                pageCount = SearchUTIL.paginate(searchResultsIndex,1, EditProductConstants.PRODUCTS_PER_PAGE);


                //This method takes in a list of the available products and returns
                //detailed product information for the products on the current page 
                //(only products for this page! --this where all products for the other
                //page get filterd out).
                Document searchResultsPerPage= searchUTIL.getProductsByIDs(searchResultsIndex, "1", pricePointId, sourceCode,
                    isSendToCustomerDomestic,orderDeliveryZipCode, sendToCustomerCountry,
                    scriptCode, daysOut, deliveryDate);

                //Now sort the products that will be displayed on the current page.
                //At this point the sort which was done on the ENTIRE list is lost
                if(sortType != null && !(sortType.equals("")))
                {                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));            
                    searchResultsPerPage = searchUTIL.sortProductList(xslFile, searchResultsPerPage);

                }
                else{
                    String filename = "/xsl/sortDisplayOrder.xsl";
                    File xslFile = new File(context.getRealPath(filename));            
                    searchResultsPerPage = searchUTIL.sortProductList(xslFile, searchResultsPerPage);

                }

                //This method removes unavailable prodcuts from the searchResultsIndex document
                //This method also copes the PageData from teh SearchResultsIndext to the SearchResultsPerPage doc
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, 1, EditProductConstants.PRODUCTS_PER_PAGE);

                Element data = (Element) DOMUtil.selectNodes(searchResultsIndex, "//pageData/data[@name='totalPages']").item(0);

                //The page builder method contains all the business logic to calculate prices,
                //discounts, ship methods... for each of the products.  The page builder
                //methods puts all this information into the XML.  The method also sets
                //various flags that indicate whether or not popup messages needs to be 
                //shown to the user.
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(
                      true, true, searchResultsPerPage,  
                      EditProductConstants.PRODUCTS_PER_PAGE, null, null,
                      isSendToCustomerDomestic,  domesticServiceFee,  internationalServiceFee,
                      sendToCustomerState,  sendToCustomerCountry, pricingCode,
                      partnerId,  promotionList, rewardType,  orderDeliveryZipCode,
                      flagVO, sourceCode, origDate).getChildNodes());



            }
            else
            {

                //Set serach results index to null.  In OE the search results
                //were stored in a persitiant dataobject.  Currently that
                //is not being done here.
                Document searchResultsIndex = null;

                // If search results are null then perform another search.
                // This could happen if the session was loaded from the database and it did not already
                // exist in the app server cache
                if(searchResultsIndex == null)
                {

                // set arguments
                Map searchCiteria = new HashMap();
                searchCiteria.put(EditProductConstants.SEARCH_INDEX_ID, indexId);
                searchCiteria.put(EditProductConstants.SEARCH_SOURCE_CODE, sourceCode);
                searchCiteria.put(EditProductConstants.SEARCH_ZIP_CODE, orderDeliveryZipCode);
                searchCiteria.put(EditProductConstants.SEARCH_PRICE_POINT_ID, pricePointId);
                searchCiteria.put(EditProductConstants.SEARCH_DOMESTIC_INTL_FLAG, intDomFlag);
                searchCiteria.put(EditProductConstants.SEARCH_COUNTRY_ID, sendToCustomerCountry);
                searchCiteria.put("scriptCode", scriptCode);
                searchCiteria.put(EditProductConstants.SEARCH_DELIVERY_END_DATE, endDateStr);

                // perform search
                searchResultsIndex = SearchUTIL.preformProductListSearch( 
                      EditProductConstants.SEARCH_GET_PRODUCTS_BY_INDEX,
                      sourceCode, indexId, orderDeliveryZipCode, pricePointId,
                      sendToCustomerCountry, scriptCode, endDateStr, intDomFlag);


                
                    pageCount = SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), EditProductConstants.PRODUCTS_PER_PAGE);
                }

                // if we are sorting by price then we need to paginate the sorted list and save it
                // to the order

                if(sortType != null && !(sortType.equals("")))
                {

                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));            
                    searchResultsIndex = searchUTIL.sortProductList(xslFile, searchResultsIndex);
                    pageCount = SearchUTIL.paginate(searchResultsIndex,Integer.valueOf(pageNumber).intValue(), EditProductConstants.PRODUCTS_PER_PAGE);


                }

                // Get the product details for the current page
                Document searchResultsPerPage= searchUTIL.getProductsByIDs(searchResultsIndex, pageNumber, pricePointId, sourceCode,
                    isSendToCustomerDomestic,orderDeliveryZipCode, sendToCustomerCountry,
                    scriptCode, daysOut, deliveryDate);


                // if we are not sorting by anything then the default sort is by
                // the order in the product index
                if(sortType == null || (sortType.equals("")))
                {
                    String filename = "/xsl/sortDisplayOrder.xsl";
                    File xslFile = new File(context.getRealPath(filename));                            
                    searchResultsPerPage = searchUTIL.sortProductList(xslFile, searchResultsPerPage);


                }
                // else sort by whatever type is passed in the arguments
                else if(sortType != null && !(sortType.equals("")))
                {
                    String filename = "/xsl/"+sortType+".xsl";
                    File xslFile = new File(context.getRealPath(filename));                                            
                    searchResultsPerPage = searchUTIL.sortProductList(xslFile, searchResultsPerPage);


                }

                int iCurrentPage = Integer.parseInt(pageNumber);

                //This method removes unavailable prodcuts from the searchResultsIndex document
                //This method also copes the PageData from the SearchResultsIndext to the SearchResultsPerPage doc
                SearchUTIL.rebuildSearchResultsIndex(searchResultsIndex, searchResultsPerPage, iCurrentPage, EditProductConstants.PRODUCTS_PER_PAGE);

                //The page builder method contains all the business logic to calculate prices,
                //discounts, ship methods... for each of the products.  The page builder
                //methods puts all this information into the XML.  The method also sets
                //various flags that indicate whether or not popup messages needs to be 
                //shown to the user.
                XMLEncoder.addSection(document, searchUTIL.pageBuilder(
                      true, true, searchResultsPerPage,  
                      EditProductConstants.PRODUCTS_PER_PAGE, null, null,
                      isSendToCustomerDomestic,  domesticServiceFee,  internationalServiceFee,
                      sendToCustomerState,  sendToCustomerCountry, pricingCode,
                      partnerId,  promotionList, rewardType,  orderDeliveryZipCode,
                      flagVO, sourceCode, origDate).getChildNodes());


           } 

          
            dataRequest.setStatementID("GET_PRICE_POINTS_LIST");            
            Document dataResponse = (Document) DataAccessUtil.getInstance().execute(dataRequest);
            // Encode previously loaded price points
            XMLEncoder.addSection(document, dataResponse.getChildNodes());

            dataRequest.reset();
            dataRequest.setStatementID("GET_ALL_SHIPPING_METHODS");            
            dataResponse = (Document) DataAccessUtil.getInstance().execute(dataRequest);
            XMLEncoder.addSection(document, dataResponse.getChildNodes());

            if(pageNumber == null || pageNumber.length() <=0 || pageNumber.equals("init"))
            {
              pageNumber = "1";
            }


            // Encode page data for product list

            pageDataMap.put("total_pages", Long.toString(pageCount));
            pageDataMap.put("reward_type", rewardType);
            pageDataMap.put("category_index", indexId);
            pageDataMap.put("page_number", pageNumber);
            pageDataMap.put("sort_type", sortType);
            pageDataMap.put("buyer_full_name", customerName);
            pageDataMap.put("price_point_id", pricePointId);
            pageDataMap.put("source_code", sourceCode);
            pageDataMap.put("script_code", scriptCode);
            pageDataMap.put("country", sendToCustomerCountry);
            pageDataMap.put("state", sendToCustomerState);
            pageDataMap.put("postal_code", orderDeliveryZipCode);            
            pageDataMap.put("partner_id", partnerId);            
            pageDataMap.put("domestic_service_fee", domesticServiceFee);            
            pageDataMap.put("international_service_fee", internationalServiceFee);            
            pageDataMap.put("pricing_code", pricingCode);            
            pageDataMap.put("domestic_flag", intDomFlag);
            pageDataMap.put("orig_date", origDate);
            pageDataMap.put("standard_label", standardLabel);
            pageDataMap.put("deluxe_label", deluxeLabel);
            pageDataMap.put("premium_label", premiumLabel);

            HashMap headerMap = new HashMap();
            headerMap.put("sourcecodeselect",sourceCode);
            headerMap.put("customerFirstName",customerName);
            DOMUtil.addSection(document, "pageData", "data", pageDataMap, true);

           
            XMLEncoder.addSection(document, "pageHeader", "headerDetail", headerMap);


        } 
        finally
        {

          if(dataRequest !=null && dataRequest.getConnection() != null)
          {
              dataRequest.getConnection().close();
          }             
          

        }

        return document;
	}

  /**
   * This method descodes certain values in an Document.  This is done because the DB
   * objects return the XML values encoded and they values need to be 
   * decoded when they are displayed on the XSL page.  This is important 
   * when displaying fields like product description which may have contain
   * symbols like the register trademark.  
   * 
   * @param String before
   * @return String de-coded value
   */
  public static Document fixXML(Document xml) throws Exception
  {

    Document fixed = xml;
    NodeList nl = DOMUtil.selectNodes(xml, "//product");
    for ( int i = 0; i < nl.getLength(); i++ )
    {
        Element element = (Element) nl.item(i);
        String before = element.getAttribute("longdescription");
        element.setAttribute("longdescription",fixHTMLEncoding(before));
        before = element.getAttribute("novatorname");
        element.setAttribute("novatorname",fixHTMLEncoding(before));
        before = element.getAttribute("exceptionmessage");
        element.setAttribute("exceptionmessage",fixHTMLEncoding(before));
        before = element.getAttribute("secondchoice");
        element.setAttribute("secondchoice",fixHTMLEncoding(before)); 
        before = element.getAttribute("masterDescription");
        element.setAttribute("masterDescription",fixHTMLEncoding(before));         

    }

  
    nl = DOMUtil.selectNodes(xml, "//upsellDetail");
    for ( int i = 0; i < nl.getLength(); i++ )
    {

        Element element = (Element) nl.item(i);
        String before = element.getAttribute("value");
        element.setAttribute("value",fixHTMLEncoding(before));         

    }


    return fixed;
  }

  /**
   * This method decodes the XML encoding.  This is done because the DB
   * objects return the XML values encoded and they values need to be 
   * decoded when they are displayed on the XSL page.  This is important 
   * when displaying fields like product description which may have contain
   * symbols like the register trademark.
   * 
   * @param String before
   * @return String de-coded value
   */
  public static String fixHTMLEncoding(String before)
  {
    //System.out.println("-----------------------------------------------------------------------");
    //System.out.println(before);
    String fixed = FieldUtils.replaceAll(before,"&lt;","<");    
    fixed = FieldUtils.replaceAll(fixed,"&gt;",">");        
    fixed = FieldUtils.replaceAll(fixed,"&amp;reg;","&reg;");        
    fixed = FieldUtils.replaceAll(fixed,"&#39;","'");        
    fixed = FieldUtils.replaceAll(fixed,"&amp;","&");        
    //System.out.println(fixed);
    //System.out.println("-----------------------------------------------------------------------");
    return fixed;
  }

  /**
   * Main method used for testing.
   * @param String[] args
   * @returns void
   */
  public static void main(String[] args)
  {
    String before = "Send special wishes to those who have family members serving in the military,  or just brighten someone&amp;#39;s day with this vibrant yellow bouquet with yellow ribbon. Bouquet contains yellow lilies, yellow freesia, white roses, yellow alstroemeria and more.&amp;lt;BR&amp;gt;";
    String after = fixHTMLEncoding(before);
    System.out.println(after);

  }

  /**
   * Get a string representation for the value contained in the object.
   * @param Object
   * @returns String
   */
  public String getValue(Object obj) 
  {

    String value = null;

    if(obj == null)
    {
      return null;
    }

    if(obj instanceof BigDecimal) {
      value = obj.toString();      
    }
    else if(obj instanceof String) {
      value = (String)obj;
    }
    else if(obj instanceof Timestamp) {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    return value;
  }  

 /**
  * This method creates an Document which contains all the addon
  * information found in the request object.
  * 
  * @param HttpServletRequest request
  * @param Document xmlDoc
  * @returns Document
  */
 public Document appendAddons(HttpServletRequest request, Document xmlDoc) throws Exception
 {
      Element temp = null;
      Element addon = null;
      String addonId = null;
      AddOnUtility aau = new AddOnUtility();
      DataRequest dataRequest = null;

      try {
          dataRequest = DataRequestHelper.getInstance().getDataRequest();
          
          // Addons are hidden vars named: addonOrig_{addonId}
          // e.g., addonOrig_B, addonOrig_RC99
          Pattern regex = Pattern.compile("addonOrig_(.+)");
    
          // Header
          Element addonHeader = createElement(xmlDoc,"AddonsOriginal");
    
          // Loop over all request params looking for the addon vars
          Enumeration paramNames = request.getParameterNames();
          while(paramNames.hasMoreElements()) {
            String paramName = (String)paramNames.nextElement();
            if (paramName.indexOf("addonOrig_") > -1) {
                Matcher m = regex.matcher(paramName);
                if (m.find()) {
                    addonId = m.group(1);
                    addon = createElement(xmlDoc,"Addon");
                    temp = createElement(xmlDoc,"addon_id",addonId);
                    addon.appendChild(temp);            
                    temp = createElement(xmlDoc,"addon_qty",request.getParameter(paramName));
                    addon.appendChild(temp);   
                    AddOnVO aavo = aau.getAddOnById(addonId, false, false, dataRequest.getConnection());
                    if (aavo != null) {
                        temp = createElement(xmlDoc,"description",aavo.getAddOnDescription());
                        addon.appendChild(temp);   
                    }
                    addonHeader.appendChild(addon);
                }
            }
          }
          xmlDoc.getDocumentElement().appendChild(addonHeader);

      } finally {
          if(dataRequest !=null && dataRequest.getConnection() != null)
          {
              dataRequest.getConnection().close();
          }        
      }
      return xmlDoc;
 }


  /** This method creates an XML element using the passed in field name and value.
   *  The passed in value is is appened to the the element as a Text node.
   *  The created element is NOT added to the passed in XML document.
   *  @param Document the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @param String the text value that should be added to the element
   *  @returns the created Element with the attached Text value */
    public static Element createElement(Document xmlDoc, String field, String value)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);
            Text textNode = xmlDoc.createTextNode(value);

            //append the text
            fieldElement.appendChild(textNode);

            return fieldElement;      
    }    

  /** This method creates an XML element using the passed in field name.
   *  A value\textnode is not appended to this element.
   *  The created element is NOT added to the passed in XML document.
   *  @param Document the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @returns the created Element with the attached Text value */
    public static Element createElement(Document xmlDoc, String field)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);

            return fieldElement;      
    }
    
    private String getMercentOriginBySourceCode(String sourceCode) throws Exception {
    
      logger.info("**** Fetching mercent origin based on source code****");
      DataRequest dataRequest = null;
      String mrcntSourceCode = new String();
      try { 
    	  dataRequest = DataRequestHelper.getInstance().getDataRequest();
    	  MercentOrderPrefixes mrcntOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
    	  List<MercentChannelMappingVO> channelVOList = mrcntOrderPrefixes.getMercentChannelDetails();
   	  
    	  for(MercentChannelMappingVO channelVO : channelVOList) {
    		  if(channelVO.getDefaultSrcCode().equals(sourceCode)) {
   			  mrcntSourceCode = channelVO.getFtdOriginMapping();
    		  }
    	  }
      } finally {
      		try 
              {
                  if(dataRequest !=null && dataRequest.getConnection() != null)
                  {
                      dataRequest.getConnection().close();
                  }
              }
              catch(SQLException se) 
              {
                  logger.error(se);
              }
      	}
   	  return mrcntSourceCode;
    }
    
    private OEDeliveryDateParm processConnection(OEDeliveryDateParm deliveryDateParm) throws Exception {
    	
    	logger.info("No connection object. Setting a new one");
    	DataRequest dataRequest = null;
     	dataRequest = DataRequestHelper.getInstance().getDataRequest();
    	deliveryDateParm.setConn(dataRequest.getConnection());
    	
    	return deliveryDateParm;
    }
            
}

