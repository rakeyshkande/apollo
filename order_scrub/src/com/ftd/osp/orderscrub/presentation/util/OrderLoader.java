package com.ftd.osp.orderscrub.presentation.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.LockUtil;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.orderscrub.util.StatsHelper;
import com.ftd.osp.ordervalidator.dao.OrderValidatorDAO;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentMethodVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.vo.ProductVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.pac.util.PACUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


/**
 * This class is used to load a new shopping cart into the scrub interface.
 *
 * @author Brian Munter
 */



public class OrderLoader 
{
    private Logger logger;
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.util.OrderLoader";
    Connection con = null;
    
    /** 
     * Constructor
     */
    public OrderLoader()
    {
            logger = new Logger(LOGGER_CATEGORY);
    }

    /**
     * Primary method which loads the entire shopping cart for display in the 
     * Scrub user interface.
     * 
     * @param searchGuid String
     * @param responseDocument XMLDocument
     * @param dataRequest DataRequest
     * @param request HttpServletRequest
     * @exception Exception
     */
    public void loadOrder(String searchGuid, OrderVO orderBeforeUpdate, Document responseDocument, DataRequest dataRequest, HttpServletRequest request) throws Exception
    {
        con = dataRequest.getConnection();
        boolean isAmazonOrder = false;
        boolean isWalmartOrder = false;
        boolean isMercentOrder = false;
        boolean isPartnerOrder = false;
        
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(con);
        PartnerUtility partnerUtil = new PartnerUtility();        
        // Load complete order from dao
        ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
        OrderVO order = scrubMapperDAO.mapOrderFromDB(searchGuid);
        
        //get buyer VO and lock it if a buyer exists on the order
        //Locking was added for PhaseIII, emueller 4/19/05
        if(order.getBuyer() != null && order.getBuyer().size() > 0){
            BuyerVO buyerVO = (BuyerVO)order.getBuyer().get(0);
            
            //if buyer record exists in clean then lock it
            if(buyerVO.getCleanCustomerId() != null){
                    String cleanBuyerId = buyerVO.getCleanCustomerId().toString();
                    
                    //get user information
                    String securityToken = request.getParameter("securitytoken");
                    SecurityManager securityManager = SecurityManager.getInstance();
                    UserInfo userInfo = securityManager.getUserInfo(securityToken);        
                    
                    //Lock the buyer record
                    String lockedBy = LockUtil.getLock(dataRequest.getConnection(),LockUtil.BUYER_LOCK, cleanBuyerId, securityToken, userInfo.getUserID());
                    buyerVO.setLockedBy(lockedBy);
            }
        }        
        
        // Check to see if Amazon Order
        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
           ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
           isAmazonOrder = true;
        
        // Check to see if Walmart Order
        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
           ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
           isWalmartOrder = true;
        
        //Check to see if it is mercent order.
        if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	isMercentOrder = true;
        }
        
		// if partner order, set the partner image URL and partner name to order.
        if(!StringUtils.isEmpty(order.getOrderOrigin())) {
			PartnerMappingVO partnerMappingVO = partnerUtil.getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), con);
			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
				isPartnerOrder = true;
				order.setPartnerOrder(true);
	            ConfigurationUtil cu = ConfigurationUtil.getInstance();
	            String imageUrl = cu.getFrpGlobalParm("OE_CONFIG", "IMAGE_SERVER_URL");
				order.setPartnerImgURL(imageUrl + "/" + partnerMappingVO.getPartnerImage());
				order.setPartnerName(partnerMappingVO.getPartnerName());
			}
        }
        
        /* Took out because no need to re-massage or re-qms on load        
         
        Validate and update qms address
        QMSValidation qmsValidation = new QMSValidation();
        qmsValidation.qmsAddress(order);

        Massage user data to fit scrub
        DataMassager dataMassager = new DataMassager();
        dataMassager.massageOrderData(order, dataRequest.getConnection());
        */

        // Bypass Roses.com recalculations in scrub
        boolean isSkipOrigin = this.checkSkipRecalcForOrigin(order.getOrderOrigin());                

        // Do order recalculation - unless any flags indicate it should be skipped
        RecalculateOrderBO recalculateOrderBO = new RecalculateOrderBO();
        try
        {
            if(!isWalmartOrder && !isAmazonOrder && !isMercentOrder && !isPartnerOrder && !isSkipOrigin)
            {
                recalculateOrderBO.recalculate(dataRequest.getConnection(), order);
            }
        }
        catch(RecalculateException rpe)
        {
          try 
          {
          // lpuckett 06/28/2006: Add throw to figure out what's broken in RecalculateOrderBO
          StringWriter stringWriter = new StringWriter();
          rpe.printStackTrace(new PrintWriter(stringWriter));
          String error = "Recalculate Order Exception for order guid "+order.getGUID() +" : " + stringWriter.toString();
          logger.error(error);
          SystemMessengerVO smVO = new SystemMessengerVO();
          smVO.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
          smVO.setType("ERROR");
          smVO.setSource("ORDER SCRUB");
          smVO.setMessage(error);
          SystemMessenger.getInstance().send(smVO, dataRequest.getConnection(), false);
          }
          catch (Exception ex)
          {
            logger.error("Error sending system message.", ex);
          }
        }
        
        // Calculate percent off for Walmart orders if it is percentage discount
        if(isWalmartOrder)
        {
            OrderDetailsVO item = null;
            Iterator itemIterator = order.getOrderDetail().iterator();
                
            while(itemIterator.hasNext())
            {
                item = (OrderDetailsVO) itemIterator.next();
                
                if(item.getDiscountType() != null
                    && item.getDiscountAmount() != null
                    && item.getProductsAmount() != null
                    && item.getDiscountType().equals("P"))
                {
                    BigDecimal walmartPercentOff = new BigDecimal(item.getDiscountAmount()).divide(new BigDecimal(item.getProductsAmount()), 2, BigDecimal.ROUND_HALF_UP);
                    item.setPercentOff((walmartPercentOff.multiply(new BigDecimal(100))).setScale(0, BigDecimal.ROUND_HALF_UP).toString());
                }
            }
        }
            
        // Validate order
        OrderHelper orderHelper = new OrderHelper();
        OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));

        // Convert validation to xml and append to response document
        ValidationXAO validationXAO = new ValidationXAO();
        DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

        // Update minor item status for display
        StatusManagerBO statusManager = new StatusManagerBO();
        statusManager.updateMinorEveryItemStatus(order, responseDocument);

        HashMap errorPopup = this.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
        DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true);

        // Check if Preferred Partner (e.g., USAA) order and include message if CSR doesn't have permission to view
        SecurityHelper secHelp = new SecurityHelper();
        HashMap ppAlerts = secHelp.checkPreferredPermissionsForOrder(request, dataRequest, order.getPreferredProcessingPartners());
        DOMUtil.addSection(responseDocument, "noPermissionsForPreferredPartner", "preferred_partner", ppAlerts, true);
        
        if(isMercentOrder) {	
        	order.setIsMercentOrder("Y");
            ConfigurationUtil cu = ConfigurationUtil.getInstance();
            String imageUrl = cu.getFrpGlobalParm("OE_CONFIG", "IMAGE_SERVER_URL");
            String mercentIcon = mercentOrderPrefixes.getMercentIconForChannel(order.getOrderOrigin());
	        order.setMrcntChannelIcon(imageUrl + "/" + mercentIcon);
        }
        
        // Convert order to xml and append to response document
        TransformationHelper transformationHelper = new TransformationHelper();
        transformationHelper.processOrder(responseDocument, order);

        // Load base cart data
        BaseCartBuilder cartBuilder = new BaseCartBuilder();
        DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));
        //logger.info("#11946 - OrderLoader : " + SearchUTIL.convertDocToString(responseDocument));

        // Record exit statistics
        StatsHelper.getInstance().updateOrderStatistics(order, GeneralConstants.STATS_IN, request, dataRequest.getConnection());      
    }

    /**
     * This method will check if reinstate should be allowed
     * 
     * @param OrderVO oVO
     * @exception Exception
     */
    private String isReinstateAllowed(OrderVO oVO) throws Exception
    {
      String reinstateMessage = null;
      OrderHelper oHelper = new OrderHelper(); 
      
      // Use case 23699-CSR is not allowed to reinstate a Gift Card Order
      List gdPaymentList = oHelper.getPaymentsByType(oVO,"GD");
      if(gdPaymentList != null && gdPaymentList.size() > 0) {    	   
    	  return oHelper.getGDReInstateMessage();
      }
      
      String days = 
        ConfigurationUtil.getInstance().getFrpGlobalParm( ConfigurationConstants.SCRUB_CONFIG_CONTEXT, 
                                                        ConfigurationConstants.DAYS_LOCK_PENDING_REMOVED_ORDER);
      if (days != null && days != ""){
         days = days.trim();
      } else {
         days = "0";
      }
      
      boolean cartBilled = oHelper.isCartBilled(oVO);
      boolean orderDateBlockReinstate = oHelper.isOrderDateBlockReinstate(oVO, new Integer(days).intValue());
      if (cartBilled)
        reinstateMessage = oHelper.getCartBilledMessage(); 
      else if (orderDateBlockReinstate)
        reinstateMessage = oHelper.getOrderDateBlockReinstateMessage(days);
      
      return reinstateMessage; 
      
    }


    public HashMap addErrorPopupMessages(HttpServletRequest request, OrderVO order, OrderVO orderBeforeUpdate, DataRequest dataRequest) throws Exception
    {
        HashMap errorPopup = new HashMap();
        
        //Check if the order should be allowed to be Reinstated
        this.addBlockReinstateErrorPopup(request, order, errorPopup, dataRequest);
        
        //Check if is Alt Pay order. 
        this.addAltPayWarningPopup(request, order, errorPopup, dataRequest);
        
        //Send warnings on charge increases
        this.addNoChangeFeeIncreaseWarningPopup(request, order, errorPopup);
        this.addFeeIncreaseWarningPopup(request, order, orderBeforeUpdate, errorPopup);
        
        // BBN -11946 - onLoad check for BBN availability.
        this.addBBNAllowedErrorPopup(request, order, errorPopup, dataRequest);
         
        return errorPopup; 
    }
    
	private void addBlockReinstateErrorPopup(HttpServletRequest request, OrderVO order, HashMap errorPopup, DataRequest dataRequest) throws Exception 
    {
        String scMode = request.getParameter("sc_mode");
        String errorPopupMessage = null;
        
        if (scMode != null && scMode.equalsIgnoreCase("R"))
          errorPopupMessage = isReinstateAllowed(order); 

        if (errorPopupMessage != null && !errorPopupMessage.equalsIgnoreCase(""))
        {
          //generate popup node
          errorPopup.put("reason", "block_reinstate");
          errorPopup.put("showErrorPopup", "Y");
          errorPopup.put("errorPopupMessage", errorPopupMessage);
        }
    }
    
	private void addNoChangeFeeIncreaseWarningPopup(HttpServletRequest request, OrderVO order, HashMap errorPopup) throws Exception {
		String servletAction = request.getParameter("servlet_action");
		
		if (servletAction == null || !servletAction.equalsIgnoreCase("save")) {
			Collection items = order.getOrderDetail();
			Iterator it = items.iterator();
			StringBuffer displayMessage = new StringBuffer("");
			String displayMessagePart1 = "";

			while (it.hasNext()) {
				OrderDetailsVO item = (OrderDetailsVO) it.next();
				logger.debug("item.getProductType:" + item.getProductType());				
			
				// Only message if order has not been processed.
				if ((item.getStatus() == null 
						|| !item.getStatus().equals(Integer.toString(OrderStatus.PROCESSED_ITEM))) && item.getProductType() != null) {
					
					String productType = item.getProductType();	
					
					// only message if non services products
					if (item.getProductType().equals(ValidationConstants.PRODUCT_TYPE_FRESHCUT)
							|| productType.equals(ValidationConstants.PRODUCT_TYPE_SAMEDAY_FRESHCUT)
							|| productType.equals(ValidationConstants.PRODUCT_TYPE_FLORAL)
							|| productType.equals(ValidationConstants.PRODUCT_TYPE_SPECIALTY_GIFT)
							|| productType.equals(ValidationConstants.PRODUCT_TYPE_SAMEDAY_GIFT)) {
						
						logger.debug("item.isChargeReleatedItemChanged:" + item.isChargeRelatedItemChanged());
						logger.debug("item.getOrigTotalFeeAmount:" + item.getOrigTotalFeeAmount());
						logger.debug("item.getShippingFeeAmount:" + item.getShippingFeeAmount());
						logger.debug("item.getServiceFeeAmount:" + item.getServiceFeeAmount());
						
						
						boolean showAlert = false;
						// There is increase in fee recalculated
						if (item.getOrigTotalFeeAmount() != null && item.getShippingFeeAmount() != null && item.getServiceFeeAmount() != null) {
							
							// only message if No charge related changed but there is increase in fee recalculated
							if (!item.isChargeRelatedItemChanged()) { 
								showAlert = true;
							} else {
								ProductVO productVO = getProductDetail(item.getProductId());
								
								if (productVO != null && !StringUtils.isEmpty(productVO.getPersonalizationTemplate())) {
									
									// if only for ship method changed from GR to SA and delivery date is 'SA' day
									if(item.isOnlyShipmethodChanged() && PACUtil.isSaturdayDelivery(item.getDeliveryDate())) { 
										
										if(com.ftd.ftdutilities.GeneralConstants.DELIVERY_STANDARD_CODE.equals(item.getOrigShipMethod()) &&
												com.ftd.ftdutilities.GeneralConstants.DELIVERY_SATURDAY_CODE.equals(item.getShipMethod())) {
											showAlert = true;
										}
									}								
								}
							}
							
							if(showAlert) {
								BigDecimal originalTotalFeeAmount = new BigDecimal(item.getOrigTotalFeeAmount());
								BigDecimal shippingFeeAmount = new BigDecimal(item.getShippingFeeAmount());
								BigDecimal serviceFeeAmount = new BigDecimal(item.getServiceFeeAmount());
								BigDecimal totalFeeAmount = shippingFeeAmount.add(serviceFeeAmount);
								
								logger.debug("totalFeeAmount:" + totalFeeAmount.toString());

								if (originalTotalFeeAmount.compareTo(totalFeeAmount) < 0) {
									logger.info("The order charges have increased over original amounts while not changing any of the charge modifying attributes. " + "Order detail: " + item.getOrderDetailId());
									displayMessagePart1 = ValidationConstants.RESPONSE_SCRUB_NO_CHANGE_SERVICE_FEE_INCREASE_PART1;
									NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);
									
									// Avoid regex errors if the string is malformed
									if (displayMessagePart1.contains("XXX") && displayMessagePart1.contains("YYY")) {
										displayMessagePart1 = displayMessagePart1.replace("XXX", currencyFormat.format(originalTotalFeeAmount.doubleValue()));
										displayMessagePart1 = displayMessagePart1.replace("YYY", currencyFormat.format(totalFeeAmount.doubleValue()));
									}

									if (displayMessagePart1.contains("{ORDER_NUMBER}")) {
										displayMessagePart1 = displayMessagePart1.replace("{ORDER_NUMBER}", item.getExternalOrderNumber());
									}
									
									displayMessage.append(displayMessagePart1);
								}
							}							
						}
					}
				}
			}

			if (displayMessage != null && displayMessage.length() > 0) {
				displayMessage.append(ValidationConstants.RESPONSE_SCRUB_NO_CHANGE_SERVICE_FEE_INCREASE_PART2);
				logger.info(displayMessage.toString());
				
				errorPopup.put("showErrorPopup", "Y");
				errorPopup.put("errorPopupMessage", displayMessage.toString());
			}
		}
	}
    
    private void addFeeIncreaseWarningPopup(HttpServletRequest request, OrderVO order, OrderVO orderBeforeUpdate, HashMap errorPopup) throws Exception 
    {
      if (order != null && orderBeforeUpdate != null)
      {
        List orderAfterUpdateDetailList = order.getOrderDetail();
        List orderBeforeUpdateDetailList = orderBeforeUpdate.getOrderDetail();
        
        //A sanity check to make sure that the sizes are the same to avoid null pointer errors
        if (orderAfterUpdateDetailList.size() == orderBeforeUpdateDetailList.size() )
        {
          logger.debug("Checking " + orderBeforeUpdateDetailList.size() + " order(s) in the cart for service fee increases.");
        
          for(int i = 0; i < orderAfterUpdateDetailList.size(); i++)
          {
            OrderDetailsVO orderAfterUpdateDetailItem = (OrderDetailsVO) orderAfterUpdateDetailList.get(i);
            OrderDetailsVO orderBeforeUpdateDetailItem = (OrderDetailsVO) orderBeforeUpdateDetailList.get(i);
         
            if (orderAfterUpdateDetailItem.getProductType() != null 
                && orderAfterUpdateDetailItem.getStatus() != null
                && !orderAfterUpdateDetailItem.getStatus().equals(OrderStatus.REMOVED_ITEM)
              )
            {        
              String productType = orderAfterUpdateDetailItem.getProductType();
              if (orderAfterUpdateDetailItem.getProductType().equals(ValidationConstants.PRODUCT_TYPE_FRESHCUT)
                       || productType.equals(ValidationConstants.PRODUCT_TYPE_SAMEDAY_FRESHCUT)
                       || productType.equals(ValidationConstants.PRODUCT_TYPE_FLORAL))
              {
                if (orderAfterUpdateDetailItem.getDeliveryDate() == null)
                {
                  logger.info("Null after update delivery date recieved. Cannot verify if charges were increased.");
                }
                else if (orderBeforeUpdateDetailItem.getDeliveryDate() == null)
                {
                  logger.info("Null before update delivery date recieved. Cannot verify if charges were increased.");                 
                }
                else if (orderAfterUpdateDetailItem.getShipMethod() == null)
                {
                  logger.info("Null after update ship method recieved. Cannot verify if charges were increased.");                 
                }
                else if (orderBeforeUpdateDetailItem.getShipMethod() == null && 
                			!(orderAfterUpdateDetailItem.getFtdwAllowedShipping() != null 
                			&& !orderAfterUpdateDetailItem.getFtdwAllowedShipping().equals(orderAfterUpdateDetailItem.getOrigShipMethod())))
                {
                  logger.info("Null before update ship method recieved. Cannot verify if charges were increased.");                 
                }  
                else if (orderBeforeUpdateDetailItem.getOrigTotalFeeAmount() == null)
                {
                  logger.info("Null before update total fee recieved. Cannot verify if charges were increased.");                 
                }  
                else 
                {
                  //If the ship method or delivery date have changed, there may be a charge change
                  if ( !(orderAfterUpdateDetailItem.getDeliveryDate().equals(orderBeforeUpdateDetailItem.getDeliveryDate()) )  
                    || !(orderAfterUpdateDetailItem.getShipMethod().equals(orderBeforeUpdateDetailItem.getShipMethod())     ) )
                  {
                    
                    BigDecimal beforeUpdateOriginalFeeAmount = new BigDecimal(orderBeforeUpdateDetailItem.getOrigTotalFeeAmount());
                    
                    BigDecimal afterUpdateShippingFeeAmount = new BigDecimal(orderAfterUpdateDetailItem.getShippingFeeAmount());
                    BigDecimal afterUpdateServiceFeeAmount = new BigDecimal(orderAfterUpdateDetailItem.getServiceFeeAmount());
                    BigDecimal afterUpdateTotalFeeAmount = afterUpdateShippingFeeAmount.add(afterUpdateServiceFeeAmount);
                    
                    //Because the updates are not always persisted in the database, eg. there was an error somewhere
                    //the service and delivery charges need to be retrived from what is currently web page as well
                    BigDecimal webShippingFeeAmount = null;
                    BigDecimal webServiceFeeAmount = null;
                    BigDecimal webTotalFeeAmount = null;
                    
                  
                    if (orderAfterUpdateDetailItem.getLineNumber() != null)
                    {
                      logger.debug("Item line number " + orderAfterUpdateDetailItem.getLineNumber());
                      try
                      {
                        webShippingFeeAmount = new BigDecimal(request.getParameter("item_service_fee" + orderAfterUpdateDetailItem.getLineNumber()));
                        webServiceFeeAmount = new BigDecimal(request.getParameter("item_drop_ship_charges" + orderAfterUpdateDetailItem.getLineNumber()));
                        webTotalFeeAmount = webShippingFeeAmount.add(webServiceFeeAmount);
                      }
                      catch (NumberFormatException nfe)
                      {
                        logger.error("Error parsing item_service_fee and item_drop_ship_charges from request data.");
                        logger.error("Detection of charge discrepencies may not occur correctly.");
                        logger.error(nfe);
                        //Clear the values for the web amounts
                        webShippingFeeAmount = null;
                        webServiceFeeAmount = null;
                        webTotalFeeAmount = null;                     
                      }
                    }
                
                    if (logger.isDebugEnabled())
                    {

                      logger.debug("beforeUpdateOriginalFeeAmount:" + beforeUpdateOriginalFeeAmount.toString()); 
                      
                      logger.debug("afterUpdateShippingFeeAmount:" + afterUpdateShippingFeeAmount.toString()); 
                      logger.debug("afterUpdateServiceFeeAmount:" + afterUpdateServiceFeeAmount.toString()); 
                      logger.debug("afterUpdateTotalFeeAmount:" + afterUpdateTotalFeeAmount.toString()); 
                      if (webShippingFeeAmount != null &&
                          webServiceFeeAmount != null &&
                          webTotalFeeAmount != null)
                      {
                        logger.debug("webShippingFeeAmount:" + webShippingFeeAmount.toString()); 
                        logger.debug("webServiceFeeAmount:" + webServiceFeeAmount.toString()); 
                        logger.debug("webTotalFeeAmount:" + webTotalFeeAmount.toString());
                      }
                    }
                    
                    //It is possible that the fee displayed to the user in scrub wasn't saved to the database because there
                    //was an error in the form
                    //In this case, they have already seen the warning message about fee increases, so don't display it again
                    if (webTotalFeeAmount != null && afterUpdateTotalFeeAmount.compareTo(webTotalFeeAmount) != 0)
                    {
                      if (beforeUpdateOriginalFeeAmount.compareTo(afterUpdateTotalFeeAmount) < 0)
                      {
                        logger.info("The order charges have increased over current order charges. Order detail: " + 
                                    orderAfterUpdateDetailItem.getOrderDetailId());
                       
                        String displayMessage = ValidationConstants.RESPONSE_SCRUB_CHANGE_SERVICE_FEE_INCREASE;
                        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.US);  
                        //Avoid regex errors if the string is malformed
                        if (displayMessage.contains("XXX") && displayMessage.contains("YYY"))
                        {
                          displayMessage = displayMessage.replace("XXX",currencyFormat.format(beforeUpdateOriginalFeeAmount.doubleValue()));
                          displayMessage = displayMessage.replace("YYY",currencyFormat.format(afterUpdateTotalFeeAmount.doubleValue()));
                        }
                        logger.info (displayMessage);
                        errorPopup.put("showErrorPopup", "Y");
                        errorPopup.put("errorPopupMessage", displayMessage);     
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    
    private void addAltPayWarningPopup(HttpServletRequest request, OrderVO order, HashMap errorPopup, DataRequest dataRequest) throws Exception 
    {
        String flag = 
          ConfigurationUtil.getInstance().getFrpGlobalParm( ConfigurationConstants.SCRUB_CONFIG_CONTEXT, 
                                                          ConfigurationConstants.ALT_PAY_PRE_EDIT_WARNING_FLAG);
        // Only enable the popup if the flag is on.
        if ("Y".equals(flag)){
            String scMode = request.getParameter("sc_mode");
            String errorPopupMessage = null;

            if (scMode != null && scMode.equalsIgnoreCase("S"))
            {
                OrderHelper orderHelper = new OrderHelper();
                String altPayPaymentType = orderHelper.getAltPayPaymentType(order);
                if(altPayPaymentType != null){
                    PaymentMethodVO pmvo = FTDCommonUtils.getPaymentMethodById(altPayPaymentType, dataRequest.getConnection());
                    errorPopupMessage = ConfigurationUtil.getInstance().getProperty(
                                            CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                                            CustomMessageConstants.EDIT_ALT_PAY_ORDER_WARNING)  + pmvo.getPaymentTypeDescription();
                }
              
            }
    
            if (errorPopupMessage != null && !errorPopupMessage.equalsIgnoreCase(""))
            {
              //generate popup node
              errorPopup.put("show_pre_edit_popup", "Y");
              errorPopup.put("edit_alt_pay_order_warning", errorPopupMessage);
            }
        }
    }
    
	/** Checks if the BBN is available for the given order in Apollo 
	 * (When there is no change in the order placed in APOLLO).
	 * @param request
	 * @param order
	 * @param errorPopup
	 * @throws Exception
	 */
	private void addBBNAllowedErrorPopup(HttpServletRequest request, OrderVO order, 
			HashMap errorPopup, DataRequest dataRequest) throws Exception {

		String itemNumber = request.getParameter("item_number");
		OrderHelper orderHelper = new OrderHelper();
		OrderDetailsVO item = null;
		
		if (StringUtils.isEmpty(itemNumber)) {			
			List<OrderDetailsVO> orderItems = order.getOrderDetail();
			for (OrderDetailsVO detailVO : orderItems) {
				checkMDFUnavailability(detailVO, errorPopup, dataRequest, request.getParameter("servlet_action"), order.getOrderOrigin());
			}
		} else {
			item = orderHelper.getItem(order, itemNumber);
			checkMDFUnavailability(item, errorPopup, dataRequest, request.getParameter("servlet_action"), order.getOrderOrigin());
		}
	}

	/** Check if there is no change in the price related item before setting error message.
	 *  it means that BBN is not available in Apollo
	 * @param item
	 * @param errorPopup
	 * @param dataRequest
	 * @param orderOrigin 
	 */
	private void checkMDFUnavailability(OrderDetailsVO item, HashMap<String, String> errorPopup, DataRequest dataRequest, String servletAction, String orderOrigin) {		
		
		if(item == null) {
			logger.error("Unable to check Morning delivery availability in Apollo - item is null");
			return;
		}
		
		// Apollo updates the morning delivery fee in scrub.order_details. if it is updated to null without any 
		// change in the order in scrub, and if original_order has morning delivery flag opted, show error pop-up
		if (servletAction == null && !item.isChargeRelatedItemChanged()) {
			if ((item.getStatus() == null || !item.getStatus().equals(Integer.toString(OrderStatus.PROCESSED_ITEM)))) {
				if ("Y".equals(item.getOriginalOrderHasMDF()) && item.getMorningDeliveryFee() == null) {
					item.setMorningDeliveryOpted("Y");
					if(item.getMorningDeliveryAvailable() == null){ item.setMorningDeliveryAvailable("N"); }	
				}
			}
		}
		
		// If partner order, as we do not recalculate, item MDF availability is not set. Check explicitly.	
		ProductVO productVO = getProductDetail(item.getProductId());
		if(productVO != null && new PartnerUtility().isPartnerOrder(orderOrigin,item.getSourceCode(), con)) {
			if("Y".equals(productVO.getMorningDeliveryFlag())) {
				logger.info("BBN available for the order, " + item.getMorningDeliveryOpted());
				item.setMorningDeliveryAvailable("Y");
			}
		}
		
		// Recalculation has checked for BBN availability.
		if("Y".equals(item.getMorningDeliveryOpted()) && !"Y".equals(item.getMorningDeliveryAvailable())) {
			logger.info("BBN Opted and is unavailable - show error message.");			
			StringBuffer errMsg = new StringBuffer(ValidationConstants.RESPONSE_BBN_UNAVAILABLE);
			errorPopup.put("showErrorPopup", "Y");
			errorPopup.put("errorPopupMessage", errMsg.toString());
			if(!new PartnerUtility().isPartnerOrder(orderOrigin,item.getSourceCode(), con)) {
				item.setMorningDeliveryFee(null);
			}					
		}	
		
		// BBN Order Has Monday Delivery Date
		java.util.Date deliveryDate = null;
		String deliveryDateStr = item.getDeliveryDate();
        if(deliveryDateStr == null) deliveryDateStr = "";
        Calendar calDeliveryDate = null;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        try
        {
            deliveryDate = sdf.parse(deliveryDateStr);
            calDeliveryDate = Calendar.getInstance();
            calDeliveryDate.setTime(deliveryDate);
        }
        catch(ParseException pe)
        {
            logger.error("ValidateDeliveryDate: Could not parse delivery date...");
        }
		
        // Check to see if this is a morning delivery order and delivery date equals Monday                     
        boolean orderHasMorningDelivery = true;
		if (item.getMorningDeliveryFee() == null) {
		    orderHasMorningDelivery = false;
		}
		try {				
			if(orderHasMorningDelivery && (calDeliveryDate != null && calDeliveryDate.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)) {
				boolean isFTDWOrder = false;				
				if(productVO != null && PACUtil.isFTDWOrder(productVO.getShippingSystem(), productVO.getProductType(), item.getShipMethod())) {
					isFTDWOrder = true;
				}
				if(!isFTDWOrder) {
					logger.info("BBN Order Has Monday Delivery Date.");			
					StringBuffer errMsg = new StringBuffer(ValidationConstants.RESPONSE_DELIVERY_DATE_NO_MONDAY_FOR_BBN);
					errorPopup.put("showErrorPopup", "Y");
					errorPopup.put("errorPopupMessage", errMsg.toString());	
				}
	        }
		} catch (Exception e) {
			logger.error("Unable to determine morning delivery monday availability, " + e.getMessage());			
		}
			
	}

	/** Get the product detail
	 * @param productId
	 * @return
	 */
	private ProductVO getProductDetail(String productId) { 
		DataRequest dataRequest = null;
		try {			
			dataRequest = DataRequestHelper.getInstance().getDataRequest();			
			OrderValidatorDAO dao = new OrderValidatorDAO(dataRequest.getConnection());
			return dao.getProductDetails(productId);
		} catch (Exception e) {
			logger.error("Error caught getting the product detail, " + e.getMessage());
		} finally {
			try {
				if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {
					dataRequest.getConnection().close();
				}
			} catch (SQLException e) {
				logger.error("Unable to close connection" + e.getMessage());
			}
		}
		return null;
	}

    /**
     * This logic was introduced with FlyingFlowers project to bypass FlyingFlowers recalculations in scrub.
     * Global parm was added as a temporary measure for phase 1 in case they wanted to control recalc.
     * Made it generic so it could be used for other origins as well.
     * Eventually this should be replaced (Hah, yeah right - if it does, see me, I'll give you a $1).
     * 
     * @param origin
     * @return true if recalc should be skipped
     */
	public boolean checkSkipRecalcForOrigin(String origin) throws Exception {
        boolean skipRecalc = false;
        String skipRecalcOrigins = StringUtils.trim(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, "SKIP_RECALC_ORIGINS"));
        if (StringUtils.isNotBlank(skipRecalcOrigins)) {
            String tmpOrigin = " " + StringUtils.trim(origin) + " ";  // Add spaces to prevent sub-string matches
            skipRecalcOrigins = " " + skipRecalcOrigins + " ";
            if (skipRecalcOrigins.indexOf(tmpOrigin) >= 0) {
                logger.info("Recalculate Order is turned off for origin:" + tmpOrigin);
                skipRecalc = true;
            }
        }
        return skipRecalc;	    
	}
	/** Send JMS Message for given input
	 * @param status
	 * @param corrId
	 * @param message
	 * @return
	 */
	public  boolean  sendJMSMessage(String status, String corrId, String message) {
		boolean success = true;
		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus(status);
			messageToken.setJMSCorrelationID(corrId);
			messageToken.setMessage(message);
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} catch (Exception e) {
			logger.error(e);
			success = false;
		}
		return success;
	}
	/**
	 * This method inserts payload in ojms.partner_orders table to trigger feed to esb.
	 */
	 public boolean sendPartnerOSU(String externalOrderNo,String statusType) 
	    {
		   StringBuffer payLoad = null;
		   boolean isProcessed = false;
		   try{
			    payLoad = new StringBuffer().append("orderNumbers:").append(externalOrderNo).append("|").append("operation:").append(statusType);									
			    isProcessed = sendJMSMessage("SUCCESS", "PROCESS_CREATE_ORD_STATUS_UPDATES", payLoad.toString()); 
				logger.debug("Inserted JMS message: "+payLoad+" : for PROCESS_CREATE_ORD_STATUS_UPDATES and jms message status is :" +isProcessed);
		   }catch(Exception exp){
			   logger.error("Error while sending jms messsage to Queue");
		   }
		   return isProcessed;
		}
}