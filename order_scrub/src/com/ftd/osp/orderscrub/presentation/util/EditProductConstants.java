package com.ftd.osp.orderscrub.presentation.util;

public interface EditProductConstants 
{

    public final static String PRODUCT_IMAGE_LOCATION = "PRODUCT_IMAGE_LOCATION";

    /* Global Parm processing constants */
    public final static String XML_OCCASION_DATA = "occasionData";
    public final static String XML_SUPER_INDEX_DATA = "superIndexData";
    public final static String XML_DELIVERY_DATA = "deliveryData";
    public final static String XML_PRODUCT_LIST = "productList";
    public final static String XML_DATE_RANGE_DATA = "dateRangeList";

    /* Time Zone Constants */
    public final static String TIMEZONE_INTERNATIONAL = "0";
    public final static String TIMEZONE_EASTERN = "1";
    public final static String TIMEZONE_CENTRAL = "2";
    public final static String TIMEZONE_MOUNTAIN = "3";
    public final static String TIMEZONE_PACIFIC = "4";
    public final static String TIMEZONE_HAWAII = "5";
    public final static String TIMEZONE_ALASKA = "5";
    public final static String TIMEZONE_DEFAULT = "5";


    /* Product Constants */
    public final static int PRODUCTS_PER_PAGE = 9;

    /* Product delivery type constants */
    public final static String DELIVERY_TYPE_DOMESTIC = "D";
    public final static String DELIVERY_TYPE_INTERNATIONAL = "I";

    public final static String SEARCH_GET_PRODUCTS_BY_INDEX = "search service::get products by index";

    /* General Order Arguments */
    public final static String PERSISTENT_OBJECT = "_PERSISTENT_OBJECT_";
    public final static String DNIS = "dnis";
    public final static String SOURCE_CODE = "SOURCE_CODE";
    public final static String OCCASION = "OCCASION";
    public final static String OCCASION_LOWER = "occasion";
    public final static String OCCASION_DESCRIPTION = "occasionDescription";    
    public final static String CUSTOMER_EXISTS_FLAG = "CUSTOMER_EXISTS_FLAG";
    public final static String CALL_CENTER_ID = "callCenterId";
    public final static String CSR_FIRST_NAME = "userFirstName";
    public final static String CSR_LAST_NAME = "userLastName";
    public final static String USER_ID = "userId";
    public final static String DISPLAY_EXPIRED = "displayExpired";
    public final static String COMPANY = "company";
    public final static String CUSTOMER_ID = "customerId";
    public final static String YELLOW_PAGES_CODE = "yellowPagesCode";
    public final static String FLAG = "flag";
    public final static String COUNTRY_CODE = "countryCode";
    public final static String COUNTRY = "country";
    public final static String ZIP_CODE = "zipcode";
    public final static String DELIVERY_DATE_DISPLAY = "deliveryDateDisplay";
    public final static String DELIVERY_DATE_LOWER = "deliveryDate";
    public final static String UPDATE_ZIP = "updzip";
    public final static String PAGE_NAME = "pagename";
    public final static String PHONE = "phone";
    public final static String ITEM_CART_NUMBER_LOWER = "itemCartNumber";
    public final static String RECIPIENT_ID = "recipientId";
    public final static String COMMAND = "command";


    /* Payment Arguments */
    public final static String GIFT_CERTIFICATE_ID = "GIFT_CERTIFICATE_ID";
    public final static String GIFT_CERTIFICATE_AMOUNT = "GIFT_CERTIFICATE_AMOUNT";
    public final static String CREDIT_CARD_TYPE = "CREDIT_CARD_TYPE";
    public final static String CREDIT_CARD_NUMBER = "CREDIT_CARD_NUMBER";
    public final static String CREDIT_CARD_EXPIRATION_NUMBER = "CREDIT_CARD_EXPIRATION_NUMBER";
    public final static String INVOICE_NUMBER = "INVOICE_NUMBER";
    
    /* Contact Arguments */
    public final static String CONTACT_FIRST_NAME = "CONTACT_FIRST_NAME";
    public final static String CONTACT_LAST_NAME = "CONTACT_LAST_NAME";
    public final static String CONTACT_ADDRESS_1 = "CONTACT_ADDRESS_1";
    public final static String CONTACT_ADDRESS_2 = "CONTACT_ADDRESS_2";
    public final static String CONTACT_CITY = "CONTACT_CITY";
    public final static String CONTACT_STATE = "CONTACT_STATE";
    public final static String CONTACT_ZIP = "CONTACT_ZIP";
    public final static String CONTACT_COUNTRY = "CONTACT_COUNTRY";
    public final static String CONTACT_HOME_PHONE = "CONTACT_HOME_PHONE";
    public final static String CONTACT_WORK_PHONE = "CONTACT_WORK_PHONE";
    public final static String CONTACT_WORK_PHONE_EXT = "CONTACT_WORK_PHONE_EXT";
    public final static String CONTACT_FAX_NUMBER = "CONTACT_FAX_NUMBER";
    public final static String CONTACT_EMAIL = "CONTACT_EMAIL";
    public final static String CONTACT_MEMBER_ID = "CONTACT_MEMBER_ID";
    public final static String CONTACT_MEMBER_FIRST_NAME = "CONTACT_MEMBER_FIRST_NAME";
    public final static String CONTACT_MEMBER_LAST_NAME = "CONTACT_MEMBER_LAST_NAME";

    /* Cart Item Arguments */
    public final static String ITEM_PRODUCT_TYPE = "ITEM_PRODUCT_TYPE";
    public final static String ITEM_NAME = "ITEM_NAME";
    public final static String ITEM_NUMBER = "ITEM_NUMBER";
    public final static String ITEM_CART_NUMBER = "ITEM_CART_NUMBER";
    public final static String ITEM_REGULAR_PRICE = "ITEM_REGULAR_PRICE";
    public final static String ITEM_DISCOUNTED_PRICE = "ITEM_DISCOUNTED_PRICE";
    public final static String ITEM_DISCOUNT_AMOUNT = "ITEM_DISCOUNT_AMOUNT";
    public final static String ITEM_DISCOUNT_DESCRIPTION = "ITEM_DISCOUNT_DESCRIPTION";
    public final static String ITEM_SKU = "ITEM_SKU";
    public final static String ITEM_COLOR_1 = "ITEM_COLOR_1";
    public final static String ITEM_COLOR_2 = "ITEM_COLOR_2";
    public final static String ITEM_SERVICE_FEE = "ITEM_SERVICE_FEE";
    public final static String ITEM_ADD_ON_TOTAL_PRICE = "ITEM_ADD_ON_TOTAL_PRICE";
    public final static String ITEM_TAX = "ITEM_TAX";
    public final static String ITEM_TOTAL_PRICE = "ITEM_TOTAL_PRICE";
    public final static String ITEM_MESSAGE = "ITEM_MESSAGE";
    public final static String ITEM_MESSAGE_SIGNATURE = "ITEM_MESSAGE_SIGNATURE";
    public final static String ITEM_COMMENTS = "ITEM_COMMENTS";
    public final static String ITEM_ADDON_BALLOON = "ITEM_ADDON_BALLOON";
    public final static String ITEM_ADDON_BALLOON_PRICE = "ITEM_ADDON_BALLOON_PRICE";
    public final static String ITEM_ADDON_BALLOON_QTY = "ITEM_ADDON_BALLOON_QTY";
    public final static String ITEM_ADDON_BEAR = "ITEM_ADDON_BEAR";
    public final static String ITEM_ADDON_BEAR_PRICE = "ITEM_ADDON_BEAR_PRICE";
    public final static String ITEM_ADDON_BEAR_QTY = "ITEM_ADDON_BEAR_QTY";
    public final static String ITEM_ADDON_CARD = "ITEM_ADDON_CARD";
    public final static String ITEM_ADDON_CARD_TYPE = "ITEM_ADDON_CARD_TYPE";
    public final static String ITEM_ADDON_CARD_PRICE = "ITEM_ADDON_CARD_PRICE";

    /* Delivery Arguments */
    public final static String DELIVERY_DATE = "DELIVERY_DATE";
    public final static String DELIVERY_SHIPPING_METHOD = "DELIVERY_SHIPPING_METHOD";
    public final static String DELIVERY_SHIPPING_COST = "DELIVERY_SHIPPING_COST";
    public final static String LOCATION_TYPE = "LOCATION_TYPE";
    public final static String LOCATION_TYPE_ID = "LOCATION_TYPE_ID";
    public final static String LOCATION_NAME = "LOCATION_NAME";
    public final static String LOCATION_EXTRA_INFO = "LOCATION_EXTRA_INFO";
    public final static String LOCATION_TIME_FROM = "LOCATION_TIME_FROM";
    public final static String LOCATION_TIME_TO = "LOCATION_TIME_TO";     
    public final static String LOCATION_DOMESTIC_FLAG = "LOCATION_DOMESTIC_FLAG";    

    /* Admin Constants */
    public final static String USERPROFILE_USER_ID = "userID";
    public final static String USERPROFILE_ROLE_ID = "roleID";
    public final static String USERPROFILE_AUTHENTICATED = "authenticated";
    public final static String USERPROFILE_FIRST_NAME = "firstName";
    public final static String USERPROFILE_LAST_NAME = "lastName";
    public final static String USERPROFILE_ACTIVE_FLAG = "activeFlag";
    public final static String USERPROFILE_CALL_CENTER_ID = "callCenterID";
    public final static String USERPROFILE_LOGON_ATTEMPTS = "logonAttempts";
    public final static String USERPROFILE_CURRENT_PASSWORD = "currentPassword";
    public final static String USERPROFILE_CURRENT_PASSWORD_DATE = "currentPasswordDate";
    public final static String USERPROFILE_LAST_UDPATE_DATE = "lastUpdateDate";
    public final static String USERPROFILE_LAST_UPDATE_USER = "lastUpdateUser";
    public final static String USERPROFILE_HOME_PHONE = "homePhone";
    public final static String USERPROFILE_ADDRESS_1 = "address1";
    public final static String USERPROFILE_ADDRESS_2 = "address2";
    public final static String USERPROFILE_CITY = "city";
    public final static String USERPROFILE_STATE = "state";
    public final static String USERPROFILE_POSTAL_CODE = "postalCode";

    public final static String ROLE_ROLE_ID = "roleID";
    public final static String ROLE_DESCRIPTION = "description";
    public final static String ROLE_LAST_UPDATE_DATE = "lastUpdateDate";
    public final static String ROLE_LAST_UPDATE_USER = "lastUpdateUser";
     
    public final static String ROLEFUNCTION_ROLE_ID = "roleID";
    public final static String ROLEFUNCTION_FUNCTION_ID = "functionID";
    public final static String ROLEFUNCTION_DESCRIPTION = "description";

    /* Calendar Constats */
    public final static String CALENDAR_MONTH = "month";
    public final static String CALENDAR_YEAR = "year";

    /* product search constants */
    public final static String SEARCH_PRODUCT_ID = "productId";
    public final static String SEARCH_SOURCE_CODE = "sourceCode";
    public final static String SEARCH_ZIP_CODE = "zipCode";
    public final static String SEARCH_DOMESTIC_INTL_FLAG = "domesticIntlFlag";
    public final static String SEARCH_COUNTRY_ID = "countryId";
    public final static String SEARCH_INDEX_ID = "indexId";
    public final static String SEARCH_PRICE_POINT_ID = "inPricePointId";
    public final static String SEARCH_DELIVERY_END_DATE = "inDeliveryEndDate";
    public final static String SEARCH_PRODUCT_ID_LIST = "productIdList";
    public final static String SEARCH_DELIVERY_END = "deliveryEnd";
    public final static String SEARCH_IN_ZIP_CODE = "inZipCode";
    public final static String SEARCH_IN_COUNTRY_ID = "inCountryId";
    public final static String SEARCH_SCRIPT_CODE = "scriptCode";
    
    /* Upsell Constants */
    public final static String UPSELL_EXISTS = "upsellExists";
    public final static String UPSELL_SKIP = "skipUpsell";
    public final static String UPSELL_BASE_ID = "upsellBaseId";
    public final static String UPSELL_BASE_CARRIER = "upsellBaseCarrier";
    public final static String UPSELL_BASE_DATE = "upsellBaseDate";
    public final static String UPSELL_BASE_NAME = "upsellBaseName";

    /* Product Type & Sub-Type constants */
    public final static String PRODUCT_TYPE_FLORAL = "FLORAL";
    public final static String PRODUCT_TYPE_FRECUT = "FRECUT";
    public final static String PRODUCT_TYPE_SPEGFT = "SPEGFT";
    public final static String PRODUCT_TYPE_SDG = "SDG";
    public final static String PRODUCT_TYPE_SDFC = "SDFC";    
    public final static String PRODUCT_SUBTYPE_EXOTIC = "EXOTIC";
    public final static String PRODUCT_SUBTYPE_NONE = "NONE";
    public final static String PRODUCT_TYPE_SERVICE = "SERVICES";
    public final static String PRODUCT_SUBTYPE_SERVICE = "FREESHIP";
   /* Delivery Constants */ 
    public final static String DELIVERY_NEXT_DAY = "Next Day Delivery";
    public final static String DELIVERY_TWO_DAY = "Two Day Delivery";
    public final static String DELIVERY_STANDARD = "Standard Delivery";
    public final static String DELIVERY_SATURDAY = "Saturday Delivery";

    public final static String DELIVERY_FLORIST = "Florist Delivery";
    public final static String DELIVERY_NEXT_DAY_CODE = "ND";
    public final static String DELIVERY_TWO_DAY_CODE = "2D";
    public final static String DELIVERY_TYPE_TWO_DAY_CODE = "2F";
    public final static String DELIVERY_STANDARD_CODE = "GR";
    public final static String DELIVERY_SATURDAY_CODE = "SA";
    public final static String DELIVERY_FLORIST_CODE = "FL";
    public final static String DELIVERY_SAME_DAY_CODE = "SD";


    /* Country Code Constants */
    public final static String COUNTRY_CODE_US = "US";
    public final static String COUNTRY_CODE_CANADA_CA = "CA";
    public final static String COUNTRY_CODE_CANADA_CAN = "CAN";
    public final static String STATE_CODE_VIRGIN_ISLANDS = "VI";
    public final static String STATE_CODE_PUERTO_RICO = "PR";
    public final static String STATE_CODE_ALASKA = "AK";
    public final static String STATE_CODE_HAWAII = "HI";
    public final static String STATE_CODE_ARIZONA = "AZ";

    public static final String YES = "Y";
    public static final String NO = "N";    

    /* Source Code and DNIS Constants */
    public final static String JC_PENNEY_CODE = "JP";
    public final static String PARTNER_PRICE_CODE = "ZZ";
    public final static String DISCOVER_CODE = "DISC1";
    public final static String CORP15_CODE = "CORP15";
    public final static String CORP20_CODE = "CORP20";
    public final static String ADVO_CODE = "ADVO";    
}
    