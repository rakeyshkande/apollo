package com.ftd.osp.orderscrub.presentation.util;

import com.ftd.osp.utilities.order.OrderXAO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.Iterator;

import org.w3c.dom.Document;

/**
 * This object is used to add another layer of control for the display of the 
 * order data on the presentation side.  This allows you to change order data 
 * outside of the business logic for display purposes only.
 *
 * @author Brian Munter
 */

public class TransformationHelper 
{
    /** 
     * Constructor
     */
    public TransformationHelper()
    {
    }

    /**
     *  This is used to perform any modifications to the order just before 
     *  transformation. The changes are for display only and no business logic is 
     *  driven from the new order updates performed in this method.
     * 
     * @param responseDocument XMLDocument
     * @param order OrderVO
     * 
     * @exception Exception
     */
    public void processOrder(Document responseDocument, OrderVO order) throws Exception
    {

        this.preProcessForDisplay(order);
    
        // Convert order to xml and append to response document
        OrderXAO orderXAO = new OrderXAO();
        DOMUtil.addSection(responseDocument, orderXAO.generateOrderXML(order).getChildNodes());
    }

    private void preProcessForDisplay(OrderVO order)
    {
        Iterator paymentIterator = null;
        PaymentsVO payment = null;
        CreditCardsVO creditCard = null;

        paymentIterator = order.getPayments().iterator();
        while(paymentIterator.hasNext())
        {
            payment = (PaymentsVO) paymentIterator.next();
                                                            
            if("C".equalsIgnoreCase(payment.getPaymentMethodType()))
            {
                if(payment.getCreditCards() != null && payment.getCreditCards().size() > 0)
                {
                    creditCard = (CreditCardsVO) payment.getCreditCards().get(0);
                    if (creditCard != null && creditCard.getCCNumber() != null &&  ! creditCard.isCcNumberChanged() && creditCard.getCCNumber().length() > 4)
                    {
                        creditCard.setCCNumber("************" + creditCard.getCCNumber().substring(creditCard.getCCNumber().length() -4, creditCard.getCCNumber().length()));
                    }
                }
            }
        }
    }

}