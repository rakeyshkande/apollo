package com.ftd.osp.orderscrub.presentation.util;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/**
 * This class is used to create generic page data from the request.
 *
 * @author Brian Munter
 */
 
public class PageDataBuilder 
{
    /** 
     * Constructor
     */
    public PageDataBuilder()
    {
    }

    /**
     * Creates a new HashMap of page data for all parameters from request.
     * 
     * @param request HttpServletRequest
     */
    public HashMap createData(HttpServletRequest request)
    {
        return createData(request, new HashMap());
    }

     /**
     * Creates a HashMap of page data for all parameters from request utilizing 
     * a HashMap previously created.
     * 
     * @param request HttpServletRequest
     * @param map HashMap
     */
    public HashMap createData(HttpServletRequest request, HashMap map)
    {
        HashMap toReturn = map;
        String key;
        for (Enumeration e = request.getParameterNames(); e.hasMoreElements();)
        {
            key = (String) e.nextElement();
            toReturn.put(key, request.getParameter(key));
        }
        return toReturn;
    }
}