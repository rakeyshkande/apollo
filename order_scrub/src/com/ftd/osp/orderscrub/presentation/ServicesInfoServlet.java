package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.utilities.vo.CustomerFSMDetails;
import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.ftdutilities.FTDCommonUtils;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.CacheUtil;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.GlobalParmHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.vo.AccountProgramMasterVO;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This servlet is used to load the source code list based on the inputted search 
 * criteria.  It also provides the functionlaity to update the source code which 
 * needs to reload the cart because of the amount of data dependent on the source 
 * code.
 *
 * @author Brian Munter
 */

public class ServicesInfoServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.ServicesInfoServlet";
    private final static String XSL_SERVICES_INFO = ".../xsl/servicesInfo.xsl";

    /**
     * Initializes servlet. 
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }


    /**
     * Execute the servlet.  The action is checked to see if the request is for a load or a submit.
     * The default is submit if no action is passed.
     * @param request
     * @param response
     * 
     * @exception ServletException
     * @exception IOException
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        DataRequest dataRequest = null;
        
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            String emailAddress = request.getParameter("email_address");
            String companyId = request.getParameter("company_id");
            logger.info(emailAddress + " " + companyId);
                      
            //CAMS service call
            Document xmlDocument = processServiceCall(emailAddress);
            StringWriter sw = new StringWriter();
            DOMUtil.print(xmlDocument, sw);
            logger.info(sw.toString());
            DOMUtil.addSection(responseDocument, xmlDocument.getChildNodes()); 

            // Load data for display
            HashMap pageData = new HashMap();
            pageData.put("company_id", companyId);
            pageData.put("email_address", emailAddress);
            //pageData.put("security_token", request.getParameter("securitytoken"));
            //pageData.put("context", request.getParameter("context"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            File xslFile = new File(getServletContext().getRealPath(XSL_SERVICES_INFO));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SERVICES_INFO, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
	/**
	 * This method makes the CAMS service call to retrieve the free shipping
	 * membership details for the given email address
	 * 
	 * @param emailAddress
	 * @return xml Document that contains free shipping membership details
	 * @throws Exception
	 */
	private Document processServiceCall(String emailAddress) throws Exception {
		CustomerFSMDetails fsmResponse;
		try {
			Map<String, CustomerFSMDetails> cfsmDetailsMap = new HashMap<String, CustomerFSMDetails>();
			if (!emailAddress.equals("") && emailAddress != null) {				 
				fsmResponse = FTDCAMSUtils.getFSMDetails(emailAddress);
				//logger.info("fsmResponse: " + fsmResponse);
				if (fsmResponse != null) {
					cfsmDetailsMap.put(emailAddress, fsmResponse);
				} else {
					cfsmDetailsMap = new HashMap<String, CustomerFSMDetails>();
				}
			}
			return convertToDOM(cfsmDetailsMap);
			
		} catch (Exception e) {
			logger.error("Error occured while connection to CAMS", e);
			Connection conn = DataSourceUtil.getInstance().getConnection("ORDER SCRUB");
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource("SCRUB_CAMS_Service");
			sysMessage.setType("System Exception");
			sysMessage.setMessage("Error calling CAMS from ServicesInfoServlet->ProcessServiceCall");
			SystemMessenger.getInstance().send(sysMessage, conn);
			Map<String, CustomerFSMDetails> cfsmDetailsMap = new HashMap<String, CustomerFSMDetails>();
			return convertToDOM(cfsmDetailsMap);
		}

	}
    
    /**
     * This method creates the XML document having customer free shipping membership details.
     * @param cfsmDetailsMap
     * @return
     * @throws Exception
     */
    private Document convertToDOM(Map<String,CustomerFSMDetails> cfsmDetailsMap) throws Exception{
    	    	
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setNamespaceAware(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document xmlDocument = builder.newDocument();
        
        CacheUtil cacheUtil = CacheUtil.getInstance();
        AccountProgramMasterVO acctProgramMasterVO = new AccountProgramMasterVO(); 
        acctProgramMasterVO = cacheUtil.getFSAccountProgramDetailsFromContent();  
        
        Element rootElement = xmlDocument.createElement("servicesInfoList");
        xmlDocument.appendChild(rootElement);
        if(cfsmDetailsMap.size() > 0) {
	        Iterator<Map.Entry<String,CustomerFSMDetails>> fsmIterator = cfsmDetailsMap.entrySet().iterator();
	        //To maintain number of records in xml
	        int i = 0;
	        while(fsmIterator.hasNext()) {
	        	
	        	Map.Entry<String,CustomerFSMDetails> mapEntry=(Map.Entry<String,CustomerFSMDetails>)fsmIterator.next(); 
	        	CustomerFSMDetails cfsmDetails = (CustomerFSMDetails)mapEntry.getValue();
	        	
	        	Element servicesInfo = xmlDocument.createElement("servicesInfo");
		        servicesInfo.setAttribute("id",new Integer(++i).toString());
		        rootElement.appendChild(servicesInfo);
		        
		        Element programName = xmlDocument.createElement("program_name");
		        programName.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getProgramName()));
		        servicesInfo.appendChild(programName);
		        
		        Element displayName = xmlDocument.createElement("display_name");
		        displayName.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getDisplayName()));
		        servicesInfo.appendChild(displayName);
		        
		        Element autoRenewalStatusNode = xmlDocument.createElement("auto_renewal_status");
				 String autorenewStatus = "";
				 if("1".equals(cfsmDetails.getFsAutoRenew())) {
					 autorenewStatus = "Opted In";
				 } else if("0".equals(cfsmDetails.getFsAutoRenew())) {
					 autorenewStatus = "Opted Out";
				 }
				 autoRenewalStatusNode.appendChild(xmlDocument.createTextNode(autorenewStatus));
				 servicesInfo.appendChild(autoRenewalStatusNode);
		        
		        Element accountprogramStatus = xmlDocument.createElement("account_program_status");
		        String programStatus = "";
		        if("1".equals(cfsmDetails.getFsMember())) {
		        	programStatus = "Active";
		        } else if("0".equals(cfsmDetails.getFsMember())) {
		        	programStatus = "Expired";
		        } else if("2".equals(cfsmDetails.getFsMember())) {
		        	programStatus = "Cancelled";
		        }
		        accountprogramStatus.appendChild(xmlDocument.createTextNode(programStatus));
		        servicesInfo.appendChild(accountprogramStatus);
		        
		       
				
		        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");				
				if(cfsmDetails.getFsStartDate() != null) {
					Calendar sdCal = Calendar.getInstance();
					sdCal.setTime(cfsmDetails.getFsStartDate());
					sdCal.add(sdCal.HOUR, 1);
				}
				if(cfsmDetails.getFsEndDate() != null) {
					Calendar edCal = Calendar.getInstance();
					edCal.setTime(cfsmDetails.getFsEndDate());
					edCal.add(edCal.HOUR, 1);
					Element expirationDate = xmlDocument.createElement("expiration_date");
				    expirationDate.appendChild(xmlDocument.createTextNode(dateFormat.format(edCal.getTime())));
				    servicesInfo.appendChild(expirationDate);				        
				}				
				if(cfsmDetails.getFsJoinDate() != null) {
					Calendar jdCal = Calendar.getInstance();
					jdCal.setTime(cfsmDetails.getFsJoinDate());
					jdCal.add(jdCal.HOUR, 1);
					Element sratDate = xmlDocument.createElement("start_date");			       
			        sratDate.appendChild(xmlDocument.createTextNode(dateFormat.format(jdCal.getTime())));
			        servicesInfo.appendChild(sratDate);			        
				}		        
		       
		        Element programUrl = xmlDocument.createElement("program_url");
		        programUrl.appendChild(xmlDocument.createTextNode(acctProgramMasterVO.getProgramUrl()));
		        servicesInfo.appendChild(programUrl);
		        
		        /*Element externalorderNumber = xmlDocument.createElement("external_order_number");
		        externalorderNumber.appendChild(xmlDocument.createTextNode(cfsmDetails.getFsmOrderNumber()));
		        servicesInfo.appendChild(externalorderNumber);      */      	           	            		
	         }
        }
        return xmlDocument;
    }

}