package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.SympathyControls;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.presentation.util.TransformationHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.ordervalidator.OrderValidator;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.handlers.PartnerHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This servlet is used to load the source code list based on the inputted search 
 * criteria.  It also provides the functionlaity to update the source code which 
 * needs to reload the cart because of the amount of data dependent on the source 
 * code.
 *
 * @author Brian Munter
 */

public class SourceCodeServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.SourceCodeServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";
    private final static String XSL_SOURCE_CODE = ".../xsl/sourceCodeLookup.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }


    /**
     * Execute the servlet.  The action is checked to see if the request is for a load or a submit.
     * The default is submit if no action is passed.
     * @param request
     * @param response
     * 
     * @exception ServletException
     * @exception IOException
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        String action = request.getParameter(ACTION);
    
        if (action != null && action.equals(ACTION_LOAD))    
        {
            this.loadSourceCodeSearch(request, response);     
        }
        else
        {
            this.submitSourceCodeSearch(request, response);    
        }
    }
    
    /**
     * Loads the source code list based on search critria specified by the user.
     * 
     * @param request
     * @param response
     */
    private void loadSourceCodeSearch(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load source code list from database
            dataRequest.reset();
            dataRequest.setStatementID("GET_SOURCECODELIST_BY_VALUE");
            dataRequest.addInputParam("source_code", request.getParameter("sourceCodeInput").toUpperCase());
            dataRequest.addInputParam("description", request.getParameter("sourceCodeInput").toUpperCase());
            dataRequest.addInputParam("date_flag", "Y");
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load data for display
            HashMap pageData = new HashMap();
            pageData.put("submit_item_flag", request.getParameter("submit_item_flag"));
            pageData.put("source_code", request.getParameter("source_code"));
            pageData.put("company_id", request.getParameter("company_id"));
            pageData.put("partner_id", request.getParameter("partner_id"));
            pageData.put("item_number", request.getParameter("item_number"));
            pageData.put("_content", request.getParameter("_content"));
            pageData.put("_focus_object", request.getParameter("_focus_object"));
            pageData.put("servlet_action", request.getParameter("servlet_action"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            // Add any Preferred Partner (e.g., USAA) permissions for this CSR.
            Element preferredPartnersElement = responseDocument.createElement("preferred_partners_for_user");
            Iterator ppit = null;
            SecurityHelper secHelp = new SecurityHelper();
            HashSet<String> pp = secHelp.getPreferredPartnersForUser(request);
            if (pp != null) {
                ppit = pp.iterator();
                while(ppit.hasNext()) {
                  String ppName = (String)ppit.next();
                  Element elem = responseDocument.createElement("preferred_partner");
                  elem.appendChild(responseDocument.createTextNode(ppName));
                  preferredPartnersElement.appendChild(elem);
                }
            }
            responseDocument.getFirstChild().appendChild(preferredPartnersElement);
            
            // Also include any Preferred Partner (e.g., USAA) alert message text.
            PartnerHandler partnerHandler = (PartnerHandler)CacheManager.getInstance().getHandler(GeneralConstants.PREFERRED_PARTNER_CACHE);
            HashMap ppMap = (HashMap)partnerHandler.getPreferredPartner();
            Iterator ppIterator = ppMap.keySet().iterator();
            HashMap ppAlerts = new HashMap();
            while(ppIterator.hasNext()) {
                String ppName = (String)ppIterator.next();
                String alertMessage = ConfigurationUtil.getInstance().getContentWithFilter(dataRequest.getConnection(), GeneralConstants.PREFERRED_PARTNER_CONTEXT, 
                                                             GeneralConstants.PREFERRED_PARTNER_SOURCE_CODE_RESTRICTION, 
                                                             ppName, null);
                ppAlerts.put(ppName, alertMessage);
            }
            DOMUtil.addSection(responseDocument, "noPermissionsForPreferredPartner", "preferred_partner", ppAlerts, true);
            
            File xslFile = new File(getServletContext().getRealPath(XSL_SOURCE_CODE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SOURCE_CODE, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
    
    /**
     * Updates the order with the source code selected and reloads the current order 
     * due to the amount of data which is dependent on the source code.  It also 
     * does a minor status update(not persisted) to accuratly display any data which 
     * may have been fixed by the source code update.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    private void submitSourceCodeSearch(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;

        String action = request.getParameter("servlet_action");
        logger.debug("servlet_action: " + action);
               
        // Update order status to remove scrub lock if next order
        if(action != null && action.equals("item_source"))
        {
            try
            {
                // Load order guid from scrub for processing
                String orderGuid = request.getParameter("order_guid");
            
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
        
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));

                // Build order
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());
                
                // Validate order
                OrderValidator orderValidator = new OrderValidator();
                OrderValidationNode orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_SCRUB, dataRequest.getConnection());

                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Update minor items status for display
                StatusManagerBO statusManager = new StatusManagerBO();
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // Convert order to xml and append to response document
                TransformationHelper transformationHelper = new TransformationHelper();
                transformationHelper.processOrder(responseDocument, order);

                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                // Load data for display
                HashMap pageData = new HashMap();
                pageData.put("submit_item_flag", request.getParameter("submit_item_flag"));
                pageData.put("source_code", request.getParameter("source_code"));
                pageData.put("company_id", request.getParameter("company_id"));
                pageData.put("partner_id", request.getParameter("partner_id"));
                pageData.put("_focus_object", request.getParameter("_focus_object"));
                pageData.put("item_number", request.getParameter("item_number"));
                pageData.put("_content", request.getParameter("_content"));
                pageData.put("servlet_action", request.getParameter("servlet_action"));
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                
                // Add error popup messages.
                OrderLoader orderLoader = new OrderLoader();
                HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 
                
                //Adding sympathy messages
        		SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                if(sympathyAlerts!=null){
                	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
                }

                File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART,null);

            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            try
            {
                // Load order guid from scrub for processing
                String orderGuid = request.getParameter("order_guid");
            
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
        
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));

                // Build order
                OrderBuilder orderBuilder = new OrderBuilder(true);
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());
                
                // Set source codes across all non-IOW orders and determine IOW order
                boolean iowOrder = false;
                if(order.getOrderDetail() != null)
                {
                    OrderDetailsVO item = null;
                
                    for(int i = 0; i < order.getOrderDetail().size(); i++)
                    {
                        item = (OrderDetailsVO) order.getOrderDetail().get(i);

                        // don't change the source code if it was already changed by bin processing
                        // skip if old flag is N and new flag is Y
                        String oldBinFlag = request.getParameter("bin_source_changed_flag" + item.getLineNumber());
                        String newBinFlag = item.getBinSourceChangedFlag();
                        logger.debug("bin flags - old: " + oldBinFlag + " new: " + newBinFlag);

                        if (oldBinFlag != null && oldBinFlag.equals("N") && newBinFlag != null && newBinFlag.equals("Y")) {
                            logger.debug("Source code was changed by bin processing, ignoring");
                        } else {
                            if( item != null && item.getItemOfTheWeekFlag() != null && item.getItemOfTheWeekFlag().equals("Y")) {
                                iowOrder = true;
                            } else {
                                item.setSourceCode(request.getParameter("source_code"));
                                item.setSourceDescription(request.getParameter("source_code_description"));
                            }
                        }
                    }

                }

                // Validate order
                OrderValidator orderValidator = new OrderValidator();
                OrderValidationNode orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_SCRUB, dataRequest.getConnection());

                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Update minor items status for display
                StatusManagerBO statusManager = new StatusManagerBO();
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // Convert order to xml and append to response document
                TransformationHelper transformationHelper = new TransformationHelper();
                transformationHelper.processOrder(responseDocument, order);
                
                // Add error popup messages.
                OrderLoader orderLoader = new OrderLoader();
                HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 

                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                // Load data for display
                HashMap pageData = new HashMap();
                pageData.put("company_id", request.getParameter("company_id"));
                pageData.put("partner_id", request.getParameter("partner_id"));

                
                if(iowOrder)
                {
                    pageData.put("source_update", "Y");
                }
                
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                
                
                //Adding sympathy messages
        		SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                if(sympathyAlerts!=null){
                	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
                }
                
                File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                
                
                

            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
    }
}