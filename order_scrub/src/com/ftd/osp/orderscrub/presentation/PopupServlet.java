package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.presentation.util.EditProductConstants;
import com.ftd.osp.orderscrub.presentation.util.SearchUTIL;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/** This Servlet is used during the Edit Product process.  The servlet
 *  displays the greeting card pages.
 *  @author Ed Mueller
 *  
 *  Note: This object was copied from Order Entry. */
public class PopupServlet extends BaseScrubServlet
{
    private static final String CONTENT_TYPE = "text/xml; charset=windows-1252";
    private ServletConfig config;
    private Logger logger;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
        logger = new Logger("com.ftd.osp.orderscrub.presentation.PopupServlet");        
    }

    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }

    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setHeader("Cache-Control", "no-cache");

        String popupId = request.getParameter("POPUP_ID");


        if(popupId.equalsIgnoreCase("LOOKUP_GREETING_CARD")){
            lookupGreetingCard(request, response);
        }
        else if(popupId.equalsIgnoreCase("LOOKUP_GREETING_CARDS")){
            lookupGreetingCards(request, response);
        }
        else
        {
          logger.error("Unknown popup id:" + popupId);
        }

    }





    private void lookupGreetingCard(HttpServletRequest request, HttpServletResponse response)
    {

      SearchUTIL searchUTIL = new SearchUTIL();

        String cardId = request.getParameter("card_id");
        String occasion = request.getParameter("occasion_id");
        String guid = request.getParameter("order_guid");
        String lineNumber = request.getParameter("line_number");
        
        //if card id is null check if card was selected from drop down
        if(cardId == null)
        {
          cardId = request.getParameter("cardIdInput");
        }

        /** HARDCODED FOR TESTING */
        //occasion = "3";
        //cardId="RC58";
        /** END HARD CODING FOR TESTING */

      HashMap pageDataMap = new HashMap();
      pageDataMap.put("card_id", cardId);
      pageDataMap.put("occasion_id", occasion);
      pageDataMap.put("order_guid", guid);
      pageDataMap.put("line_number", lineNumber);        



        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }


        if(cardId == null)
        {
            cardId = "";
        }

        if(occasion == null)
        {
            occasion = "";
        }

        try
        {

            //get image location
            String imageLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, EditProductConstants.PRODUCT_IMAGE_LOCATION);          
            pageDataMap.put("product_images",imageLocation);        

            String xslFileShort = ".../xsl/greetingDetail.xsl";

            Document cardXML = getGreetingCards(occasion,cardId);
            String xslFile = getServletContext().getRealPath(xslFileShort);



            Document xml = DOMUtil.getDocument();
            DOMUtil.addSection(xml, cardXML.getChildNodes()); 

            // Add page data to xml
            DOMUtil.addSection(xml, "pageData", "data", pageDataMap, true);

            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(xml, "security", "data", securityData, true);

            logger.debug(searchUTIL.convertDocToString(xml));

            TraxUtil.getInstance().transform(request, response, xml, new File(xslFile), xslFileShort, params);      

        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        } 
    }




    private void lookupGreetingCards(HttpServletRequest request, HttpServletResponse response)
    {

        String cardId = request.getParameter("card_id");
        String occasion = request.getParameter("occasion_id");
        String guid = request.getParameter("order_guid");
        String lineNumber = request.getParameter("line_number");

        SearchUTIL searchUtil = new SearchUTIL();
        
        //card is null in this proc
        //cardId = null;       

        HashMap pageDataMap = new HashMap();
        pageDataMap.put("card_id", cardId);
        pageDataMap.put("occasion_id", occasion);
        pageDataMap.put("order_guid", guid);
        pageDataMap.put("line_number", lineNumber);        

       
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();

        
        if(occasion == null)
        {
            occasion = "";
        }

        try
        {

            //get image location
            String imageLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, EditProductConstants.PRODUCT_IMAGE_LOCATION);          
            pageDataMap.put("product_images",imageLocation);

            Document cardXML = getGreetingCards(occasion,cardId);
            String xslFileShort = ".../xsl/greetingLookup.xsl";
            String xslFile = getServletContext().getRealPath(xslFileShort);



            Document xml = DOMUtil.getDocument();
            DOMUtil.addSection(xml, cardXML.getChildNodes()); 

            // Add page data to xml
            DOMUtil.addSection(xml, "pageData", "data", pageDataMap, true);

            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(xml, "security", "data", securityData, true);

            logger.debug(searchUtil.convertDocToString(xml));
            
            TraxUtil.getInstance().transform(request, response, xml, new File(xslFile), xslFileShort, params);      

        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
    }

    private Document getGreetingCards(String occasion, String cardId)
        throws Exception
	{

        SearchUTIL searchUtil = new SearchUTIL();

        // Initialize data request
        DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
        Document addonsXML = null;
        
        try
        {
            // if we have all three arguments then search for the specific
            // card plus all other cards for this occasion.
            if(occasion != null && occasion.length() > 0 && cardId != null && cardId.length() > 0)
            {
                dataRequest.reset();
                dataRequest.addInputParam("IN_ADDON_TYPE", "4");
                dataRequest.addInputParam("IN_OCCASION_ID", occasion);
                dataRequest.addInputParam("IN_ADDON_ID", cardId);
                dataRequest.setStatementID("GET_ADDONS_BY_TYPE_OCCASION");
            }
            else
            {
                dataRequest.reset();
                dataRequest.addInputParam("IN_ADDON_TYPE", "4");
                dataRequest.addInputParam("IN_OCCASION_ID", occasion);
                if(cardId != null)
                {
                    cardId = cardId.toUpperCase();
                }
                dataRequest.addInputParam("IN_ADDON_ID", cardId);
                dataRequest.setStatementID("GET_ADDON_BY_TYPE_OCCASION");                
            }

            addonsXML = (Document)DataAccessUtil.getInstance().execute(dataRequest); 
            logger.debug(searchUtil.convertDocToString(addonsXML));

        }
        catch(Exception e)
        {
            logger.error(e);
        }
        finally
        {
          //close the connection
          try
          {
            dataRequest.getConnection().close();
          }
          catch(Exception e)
          {}
        }
      

        return addonsXML;
	}



}
