package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.ftdutilities.SympathyControls;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.constants.SecurityConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.TransformationHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.BillingInfoHandler;
import com.ftd.osp.utilities.cacheMgr.handlers.PaymentBinMasterHandler;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;

/**
 * This servlet is used to display and validate payment information.  It also 
 * contains functionality to handle "No Charge" validation.  All validation 
 * occurs on a dummy order created within this servlet which contains only 
 * payment information.
 *
 * @author Brian Munter
 */

public class PaymentServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.PaymentServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";
    private final static String XSL_PAYMENT = ".../xsl/payment.xsl";
    private final static String NO_CHARGE = "NC";
    private final static String CREDIT_CARD_HIDE = "************";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Checks which action to perform and either loads or submits.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        
        // What used to come in as a get comes in as a post now
        // So we need to check first to see if we should load or submit
        String action = request.getParameter(ACTION);
        if (action != null && action.equals(ACTION_LOAD))
        {
            this.loadPayment(request, response);          
        }
        else
        {
            this.submitPayment(request, response);          
        }

        
    }

    /**
     * Loads the data to display the payments page.  This method also pre-validates 
     * payment information before page loads to facilitate display of all payment 
     * related validation messages.  It is also integrated with the Security component 
     * to authorize "No Charge" and manage display of full credit card number.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    private void loadPayment(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        boolean isAmazonOrder = false;
        boolean isWalmartOrder = false;
        boolean isMercentOrder = false;
        boolean isPartnerOrder = false;

        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load order guid from scrub for processing
            String orderGuid = request.getParameter("order_guid");
            
            // Load order
            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
            OrderVO paymentOrder = scrubMapperDAO.mapOrderFromDB(orderGuid);
            MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
            // Check to see if Amazon Order
            if(paymentOrder.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
               ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
               isAmazonOrder = true;
            
            // Check to see if Walmart Order
            if(paymentOrder.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
               ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
               isWalmartOrder = true;
            
            if(mercentOrderPrefixes.isMercentOrder(paymentOrder.getOrderOrigin())) {
            	isMercentOrder = true;
            }
            
            if(new PartnerUtility().isPartnerOrder(paymentOrder.getOrderOrigin(),paymentOrder.getSourceCode(), dataRequest.getConnection())) {
            	isPartnerOrder = true;
            }
            // Validate credit card
            this.loadPaymentOrder(request, paymentOrder);
            OrderHelper orderHelper = new OrderHelper();
            OrderValidationNode creditCardValidationNode = orderHelper.validateOrder(paymentOrder, dataRequest.getConnection(), 
                    isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(paymentOrder.getOrderOrigin()));
 
            // Load payment page data
            DOMUtil.addSection(responseDocument, this.loadGenericPaymentData(paymentOrder, request, dataRequest, false));

            // Convert validation to xml and append to response document
            ValidationXAO validationXAO = new ValidationXAO();
            DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(creditCardValidationNode).getChildNodes());

            File xslFile = new File(getServletContext().getRealPath(XSL_PAYMENT));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_PAYMENT, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. validation: This is action is called to validate all payment information 
     * submitted from the payments page.  The validation occurs off of a dummy order 
     * object created within this method.  This also validates "No Charge" authenication 
     * using the Security component.<br/><br/>
     * 
     * 2. default: Default functionality to submit the updated payment information 
     * to the order.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    private void submitPayment(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        DataRequest dataRequest = null;
        boolean isAmazonOrder = false;
        boolean isWalmartOrder = false;
        boolean isMercentOrder = false;
        boolean isPartnerOrder = false;
        String action = request.getParameter("servlet_action");
        
        if(action != null && action.equals("validation"))
        {
            try
            {
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();

                // Load order guid from scrub for processing
                String orderGuid = request.getParameter("order_guid");

                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load order
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO paymentOrder = scrubMapperDAO.mapOrderFromDB(orderGuid);
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
                // Check to see if Amazon Order
                if(paymentOrder.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                   isAmazonOrder = true;
                
                // Check to see if Walmart Order
                if(paymentOrder.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;
                
                if(mercentOrderPrefixes.isMercentOrder(paymentOrder.getOrderOrigin())) {
                	isMercentOrder = true;
                }
                
                if(new PartnerUtility().isPartnerOrder(paymentOrder.getOrderOrigin(),paymentOrder.getSourceCode(), dataRequest.getConnection())) {
                	isPartnerOrder = true;
                }
                
                // Validate credit card
                this.loadPaymentOrder(request, paymentOrder);
                OrderHelper orderHelper = new OrderHelper();
                OrderValidationNode creditCardValidationNode = orderHelper.validateOrder(paymentOrder, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(paymentOrder.getOrderOrigin()));

                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(creditCardValidationNode).getChildNodes());
                
                // Load payment page data
                DOMUtil.addSection(responseDocument, this.loadGenericPaymentData(paymentOrder, request, dataRequest, true));     
                File xslFile = new File(getServletContext().getRealPath(XSL_PAYMENT));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_PAYMENT, null);

            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            try
            {
                // Load order guid from scrub for processing
                String orderGuid = request.getParameter("order_guid");
            
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
        
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
                
                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                   isAmazonOrder = true;
                
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;

                if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
                	isMercentOrder = true;
                }
                if(new PartnerUtility().isPartnerOrder(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection())) {
                	isPartnerOrder = true;
                }
                // Update order with form values
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());

                // Validate order
                OrderHelper orderHelper = new OrderHelper();
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
    
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Update minor items status for display
                StatusManagerBO statusManager = new StatusManagerBO();
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // Convert order to xml and append to response document
                TransformationHelper transformationHelper = new TransformationHelper();
                // Do not mask the CC here.  This will save the number to the form correctly so it will be updated.
                // All the communication will be done over https, and if the CSR can see the card number it is because they entered it
                transformationHelper.processOrder(responseDocument, order);
                
                // Add error popup messages.
                OrderLoader orderLoader = new OrderLoader();
                HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 
                
                //Add sympathy error messages
                SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                if(sympathyAlerts!=null){
                	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
                }
                
                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));
                File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
    }

    /**
     * Defect 1962 changes:
     * 1. setting order total to the new calculated total to correct the $0 order total validation
     * 2. setting credit card and PayPal payment amount to the difference of total and gc amount
     * 
     * @param request
     * @param order
     */
    private void loadPaymentOrder(HttpServletRequest request, OrderVO order) throws Exception
    {
        List paymentList = new ArrayList();
        
        String sOrderAmount = request.getParameter("order_amount");
        String sGcAmount = request.getParameter("gc_amount1");
        if (sOrderAmount == null || sOrderAmount.equals("")) {
            sOrderAmount = "0";
        }            
        if (sGcAmount == null || sGcAmount.equals("") || (sGcAmount != null && !sGcAmount.equals("") && Double.parseDouble(sGcAmount) == 0)) {
            sGcAmount = "0";
        }else
        {
        	//Get issued for gc and orig_auth_amount for gd
            Iterator pi = order.getPayments().iterator();
            while(pi.hasNext())
            {
            	PaymentsVO p = (PaymentsVO) pi.next();
            	if("G".equals(p.getPaymentMethodType()))
            	{
            		if(p.getGcCouponIssueAmount() != null )
            		{
            			if(sOrderAmount != null && new BigDecimal(sOrderAmount).compareTo(new BigDecimal(sGcAmount))==1)
            			{
            				sGcAmount = p.getGcCouponIssueAmount().toString();
            			}
            		}
            			
            	}
            	else if("R".equals(p.getPaymentMethodType())) 
            	{
            		if(p.getOrigAuthAmount() != null)
            		{
            			if(sOrderAmount != null && new BigDecimal(sOrderAmount).compareTo(new BigDecimal(sGcAmount))==1)
            			{
            				sGcAmount = p.getOrigAuthAmount().toString();
            			}
            		}
            	}
            }
        }
        
        
        
        BigDecimal orderAmount = new BigDecimal(sOrderAmount);
        BigDecimal gcAmount = new BigDecimal(sGcAmount);
        BigDecimal remainedAmount = orderAmount.subtract(gcAmount);
        
        CreditCardsVO creditCard = null;
        List creditCardList = null;
        
        String paymentMethod = request.getParameter("payment_method");
        String paymentMethodAll = request.getParameter("payment_method_all");
              
        PaymentsVO payment = new PaymentsVO();
        payment.setPaymentsType(request.getParameter("payment_method_id"));

        if(paymentMethodAll == null)
        {
            paymentMethodAll = "";
        }
        
     // Null or empty payment method types default to credit card
        if((paymentMethod == null || paymentMethod.equals("")) && paymentMethodAll.equals(""))
        {
        	
            paymentMethod = "C";
        }

        payment.setPaymentMethodType(paymentMethod);
        
        //Add GC and GD payment type to list.
        Iterator pi = order.getPayments().iterator();
        while(pi.hasNext())
        {
        	PaymentsVO p = (PaymentsVO) pi.next();
        	if("G".equals(p.getPaymentMethodType()) || "R".equals(p.getPaymentMethodType())) {
        		p.setAmount(sOrderAmount);
        		paymentList.add(p);
        	}
        }
            
        if(paymentMethod.indexOf("C") >= 0 || paymentMethodAll.indexOf("C") >= 0)
        {
            creditCardList = new ArrayList();
            creditCard = new CreditCardsVO();
                    
            if(request.getParameter("payment_method_id") != null)
            {
                creditCard.setCCType(request.getParameter("payment_method_id"));
            }

            if(request.getParameter("cc_number") != null && ! request.getParameter("cc_number").matches("\\*{12}.*"))
            {
                creditCard.setCCNumber(request.getParameter("cc_number"));
                request.setAttribute("cc_number_actual", request.getParameter("cc_number"));
            }
            else
            {    
                Iterator paymentIterator = null;
                CreditCardsVO orderCreditCard = new CreditCardsVO();
                PaymentsVO orderPayment = new PaymentsVO();

                paymentIterator = order.getPayments().iterator();
                while(paymentIterator.hasNext())
                {
                    orderPayment = (PaymentsVO) paymentIterator.next();
                    if("C".equalsIgnoreCase(orderPayment.getPaymentMethodType()))
                    {
                        if(orderPayment.getCreditCards() != null && orderPayment.getCreditCards().size() > 0)
                        {
                            orderCreditCard = (CreditCardsVO) orderPayment.getCreditCards().get(0);
                            if (orderCreditCard != null && orderCreditCard.getCCNumber() != null)
                            {
                                creditCard.setCCNumber(orderCreditCard.getCCNumber());
                                request.setAttribute("cc_number_actual", orderCreditCard.getCCNumber());
                            }
                        }
                    }
                }
            }
            if(request.getParameter("cc_exp_month") != null && request.getParameter("cc_exp_year") != null)
            {   
                try
                {
                    creditCard.setCCExpiration(request.getParameter("cc_exp_month") + "/" + request.getParameter("cc_exp_year").substring(2,4));
                }
                catch(StringIndexOutOfBoundsException e)
                {
                    creditCard.setCCExpiration(request.getParameter("cc_exp_month"));
                }
            }
                    
            creditCardList.add(creditCard);
            payment.setCreditCards(creditCardList);
            payment.setAmount(remainedAmount.toString());
            paymentList.add(payment);
        }
        else if(paymentMethod.indexOf("I") >= 0 || paymentMethodAll.indexOf("I") >= 0)
        {
            payment.setInvoiceNumber("");
            payment.setAmount("");
                        
            paymentList.add(payment);
        }
        else if(paymentMethod.indexOf("P") >= 0 || paymentMethodAll.indexOf("P") >= 0)
        {
            // Retrieve original payments of this type from order. Not resetting from request.
            OrderHelper orderHelper = new OrderHelper();
            List payments = orderHelper.getPaymentsByMethodType(order, paymentMethod);
            
            // There should be only one payment of the type.
            PaymentsVO p = (PaymentsVO)payments.get(0);          
            
            // set payment amount to be the difference of order amount and gc amount.           
            p.setAmount(remainedAmount.toString());
            paymentList.add(p);
        }
        order.setPayments(paymentList);
        order.setOrderTotal(orderAmount.toString());
    }

    private NodeList loadGenericPaymentData(OrderVO paymentOrder, HttpServletRequest request, DataRequest dataRequest, boolean validation) throws Exception
    {
        Document paymentDocument = DOMUtil.getDocument();

        // Load source code
        dataRequest.reset();
        dataRequest.setStatementID("SOURCE_CODE_RECORD_LOOKUP");
        dataRequest.addInputParam("source_code", request.getParameter("source_code"));
        CachedResultSet sourceResult = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

        String validPayMethod = null;
        if(sourceResult != null && sourceResult.getRowCount() > 0)
        {
            while(sourceResult.next())
            {
                validPayMethod = (String) sourceResult.getObject(10);
            }
        }

        boolean giftCertOverride = false;
        BillingInfoHandler billingInfoHandler = (BillingInfoHandler)CacheManager.getInstance().getHandler("CACHE_NAME_BILLING_INFO");
        if(validPayMethod != null 
            && validPayMethod.equals(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.NO_CHARGE_CODE))
            && billingInfoHandler.getBillingInfoBySourceAndPrompt(request.getParameter("source_code"), ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.GIFT_CERT_CODE)) != null)
        {
            giftCertOverride = true;
        }
        
        if(validPayMethod != null 
            && !validPayMethod.equals("") 
            && !validPayMethod.equals(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.CORP_PURCHASE_CODE)) 
            && !giftCertOverride)
        {
            // Load only valid pay method
            dataRequest.reset();
            dataRequest.setStatementID("GET_PAYMENT_METHOD_BY_ID");
            dataRequest.addInputParam("payment_method_id", validPayMethod);
            DOMUtil.addSection(paymentDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
        }
        else
        {
            // Load credit card types
            dataRequest.reset();
            dataRequest.setStatementID("GET_PAYMENT_METHODS_EX_CB");
            dataRequest.addInputParam("payment_type_id", "C");
            DOMUtil.addSection(paymentDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Load invoice types
            dataRequest.reset();
            dataRequest.setStatementID("GET_PAYMENT_METHODS_BY_TYPE");
            dataRequest.addInputParam("payment_type_id", "I");
            DOMUtil.addSection(paymentDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
        }
        
        // add alt pay payment type if payment type is alt pay.
        OrderHelper orderHelper = new OrderHelper();
        String altPayPaymentType = orderHelper.getAltPayPaymentType(paymentOrder);
        if(altPayPaymentType != null && !altPayPaymentType.equals("")) {
            // Load PayPal payment type
            dataRequest.reset();
            dataRequest.setStatementID("GET_PAYMENT_METHOD_BY_ID");
            dataRequest.addInputParam("payment_method_id", altPayPaymentType);
            DOMUtil.addSection(paymentDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 
        }

        // Add page data to xml
        HashMap pageData = new HashMap();
        pageData.put("order_guid", request.getParameter("order_guid"));
        pageData.put("master_order_number", request.getParameter("master_order_number"));
        pageData.put("source_code", request.getParameter("source_code"));
        pageData.put("order_amount", request.getParameter("order_amount"));
        pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
        pageData.put("payment_method", request.getParameter("payment_method"));
        pageData.put("payment_method_id", request.getParameter("payment_method_id"));
        pageData.put("cc_exp_month", request.getParameter("cc_exp_month"));
        pageData.put("cc_exp_year", request.getParameter("cc_exp_year"));
        pageData.put("disable_flag", request.getParameter("disable_flag"));
        pageData.put("no_charge_username", request.getParameter("no_charge_username"));
        pageData.put("payment_method_all", getAllPaymentTypes(paymentOrder));
        
        String entireCreditCardNumber = null;

        SecurityManager securityManager = SecurityManager.getInstance();
        if (securityManager.assertPermission(request.getParameter("context"), request.getParameter("securitytoken"), 
             SecurityConstants.RESOURCE_CREDIT_CARD, SecurityConstants.PERMISSION_YES))
        {
            entireCreditCardNumber = (String) request.getAttribute("cc_number_actual");
        }
        else
        {
            entireCreditCardNumber = request.getParameter("cc_number");
        }

        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
        if(standAloneFlag != null && standAloneFlag.equals("N"))
        {
            if (securityManager.assertPermission(request.getParameter("context"), request.getParameter("securitytoken"), 
                 SecurityConstants.RESOURCE_CREDIT_CARD, SecurityConstants.PERMISSION_YES))
            {
                pageData.put("cc_number", entireCreditCardNumber);
                pageData.put("cc_number_input", entireCreditCardNumber);
            }
            else
            {
                if(entireCreditCardNumber != null && entireCreditCardNumber.length() > 3)
                {
                    pageData.put("cc_number", entireCreditCardNumber);
                    pageData.put("cc_number_input", CREDIT_CARD_HIDE + entireCreditCardNumber.substring((entireCreditCardNumber.length() - 4), entireCreditCardNumber.length()));
                }
            }
        }
        else
        {
            if(entireCreditCardNumber != null && entireCreditCardNumber.length() > 3)
            {
                pageData.put("cc_number", entireCreditCardNumber);
                pageData.put("cc_number_input", CREDIT_CARD_HIDE + entireCreditCardNumber.substring((entireCreditCardNumber.length() - 4), entireCreditCardNumber.length()));
            }
        }

        // Add security data to xml
        HashMap securityData = new HashMap();
        securityData.put("context", request.getParameter("context"));
        securityData.put("securitytoken", request.getParameter("securitytoken"));
        DOMUtil.addSection(paymentDocument, "security", "data", securityData, true);

        int gcCount = 1;
        String gcNumber = null;
        String gcAmount = null;
        String ordAmount = null;
        String issuedAmount = null;
        //Get GC/GD authorized amount for sake of displaying on payments screen
        issuedAmount = getAuthorizedGCGDAmount(paymentOrder, pageData);
        
        while(true)
        {   
            gcNumber = request.getParameter("gc_number" + gcCount);
            gcAmount = request.getParameter("gc_amount" + gcCount);
            ordAmount = request.getParameter("order_amount");
            if(issuedAmount != null && ordAmount != null){
            	if(Double.parseDouble(ordAmount) < Double.parseDouble(issuedAmount))
            	{
            		gcAmount = ordAmount;
            	}
            	else
            	{
            		gcAmount = issuedAmount;
            	}
            }
            if((gcNumber != null && !gcNumber.equals("")) && (gcAmount != null && !gcAmount.equals("")) && request.getParameter("payment_method_all").contains("G"))
            {
                pageData.put("gc_number" + gcCount, request.getParameter("gc_number" + gcCount));
                pageData.put("gc_amount" + gcCount, gcAmount);
                
            }else if(request.getParameter("payment_method_all") != null && request.getParameter("payment_method_all").contains("R"))
            {
            	pageData.put("gc_number" + gcCount, request.getParameter("gc_number" + gcCount));
                pageData.put("gc_amount" + gcCount, gcAmount);
                break;
            }
            else
            {
                break;
            }
            
            gcCount++;
        }
        
        if(validation)
        {
            // Validate No Charge
            if(request.getParameter("payment_method_id") != null && request.getParameter("payment_method_id").equals(NO_CHARGE))
            {
                logger.debug("Authenticating No Charge");

                String noChargeToken = null;
                String noChargeUser = request.getParameter("no_charge_username");
                String noChargePassword = request.getParameter("no_charge_password");
                
                try
                {
                    // Verify username/password
                    noChargeToken = (String) securityManager.authenticateIdentity(request.getParameter("context"), "ORDER SCRUB", noChargeUser, noChargePassword);
                }
                catch(Exception e)
                {
                    logger.debug("The No Charge Username/Password is Invalid");
                    pageData.put("no_charge_username_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PAYMENT_NC_USER_ID_ERROR));
                    pageData.put("no_charge_password_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PAYMENT_NC_USER_ID_ERROR));
                }

                if(noChargeToken != null)
                {
                    // Verify that role is correct and supports no charge
                    if (!securityManager.assertPermission(request.getParameter("context"), noChargeToken, 
                        SecurityConstants.RESOURCE_NO_CHARGE, SecurityConstants.PERMISSION_YES))
                    {
                        logger.debug("The No Charge Username/Password is Invalid");
                        pageData.put("no_charge_username_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PAYMENT_NC_PERMISSION_ERROR));
                        pageData.put("no_charge_password_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PAYMENT_NC_PERMISSION_ERROR));
                    }
                }
            }
        
            pageData.put("validation_flag", "Y");
        }
        
        DOMUtil.addSection(paymentDocument, "pageData", "data", pageData, true);

        DOMUtil.print(paymentDocument, System.out);
        return paymentDocument.getFirstChild().getChildNodes();
        
    }
    /*
     * This method will returns authorized amount for GC/GD
     */
    private String getAuthorizedGCGDAmount(OrderVO orderVO, Map PageData){
    	String issuedAmt = null;
    	String is_gcgd_valid = "Y";
    	Date today = new Date();
    	//Find GC/GD payment types and get issued amount return.
    	if(orderVO != null && orderVO.getPayments() != null)
    	{   
    		int index = 0;
	        Iterator pi = orderVO.getPayments().iterator();
	        while(pi.hasNext())
	        {
	        	PaymentsVO p = (PaymentsVO) pi.next();
	        	if("G".equals(p.getPaymentMethodType())) {
	        		logger.info("p.getGcRedeemableStatus(): " + p.getGcRedeemableStatus());
	        		if(p.getGcCouponIssueAmount() != null)
	        			issuedAmt = p.getGcCouponIssueAmount().toString();
	        		if(p.getGcCouponStatus() == null || p.getGcCouponStatus().equals("")|| (p.getGcCouponStatus() != null && !p.getGcRedeemableStatus()) )
                	{
                		issuedAmt = "0";
                		is_gcgd_valid = "N";
                	}
                	else if(p.getGcCouponExpirationDate() != null){
                		if(p.getGcCouponExpirationDate().before(today)){
                			issuedAmt = "0";
                    		is_gcgd_valid = "N";
                		}
                	}
	        	}
	        	else if("R".equals(p.getPaymentMethodType()))
	        	{
	        		issuedAmt = p.getOrigAuthAmount();
	        		CreditCardsVO creditCard = (CreditCardsVO)p.getCreditCards().get(0);
                	if(creditCard.getPin() == null)
                	{
                		issuedAmt = "0";
                		is_gcgd_valid = "N";
                	}
                	else if(p.getAuthNumber() == null)
                	{
                		issuedAmt = "0";
                		is_gcgd_valid = "N";
                	}
                	else if(creditCard.getCCNumber() == null)
                	{
                		issuedAmt = "0";
                		is_gcgd_valid = "N";
                	}
                	else if(creditCard.getCCNumber() != null && creditCard.getCCNumber().length() != 19)
                	{
                		issuedAmt = "0";
                		is_gcgd_valid = "N";
                	}
                	PaymentBinMasterHandler paymentBinHdlr = (PaymentBinMasterHandler) CacheManager.getInstance().getHandler(CacheMgrConstants.CACHE_GET_PAYMENT_BIN_MASTER);
                	String payMethodType = paymentBinHdlr.getPaymentMethodIdByNumber(creditCard.getCCNumber());
                	if(!"GD".equalsIgnoreCase(payMethodType)){
                		issuedAmt = "0";
                		is_gcgd_valid = "N";
                	}
	        	}
	        }
    	}
    	PageData.put("is_gcgd_valid", is_gcgd_valid);
    	return issuedAmt;
    }
    
    /*
     * This method will loop through all payments in order and construct a string with all payment types
     * and return the string. 
     */
    private String getAllPaymentTypes(OrderVO orderVO){
    	String allPayments = "";
    	if(orderVO != null && orderVO.getPayments() != null)
    	{
	        Iterator pi = orderVO.getPayments().iterator();
	        while(pi.hasNext())
	        {
	        	PaymentsVO p = (PaymentsVO) pi.next();
	        	if(p.getPaymentMethodType() != null )
                {
	        		allPayments = allPayments + p.getPaymentMethodType();
                }
	        }
    	}
    	return allPayments;
    }

}