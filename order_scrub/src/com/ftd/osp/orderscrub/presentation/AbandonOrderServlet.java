package com.ftd.osp.orderscrub.presentation;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This servlet is used to forward users to the order entry system to work on 
 * abandoned orders.
 *
 * @author Brian Munter
 */

public class AbandonOrderServlet extends HttpServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.AbandonOrderServlet";
    private final static String ORDER_ENTRY_CART_SERVLET = "/servlet/ShoppingCartServlet";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Redirects order to Order Entry system based on configuration parameters 
     * specified in the Scrub system.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadAbandoned(request, response);          
    }

    /**
     * Currently not used.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);   
    }

    private void loadAbandoned(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
    
        try
        {
            // Forward to Order Entry
            String orderEntryCartLocation = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.ORDER_ENTRY_LOCATION) + ORDER_ENTRY_CART_SERVLET;
            String orderEntrySession = "?sessionId=" + request.getParameter("securitytoken");
            String orderEntryPersistentId = "&persistentObjId=" + request.getParameter("order_guid");

            logger.debug("Forarding to Order Entry location:" + orderEntryCartLocation + orderEntrySession + orderEntryPersistentId);

            response.sendRedirect(orderEntryCartLocation + orderEntrySession + orderEntryPersistentId);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
}

