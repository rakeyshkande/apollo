package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.orderscrub.business.GDAuthClearBO;
import com.ftd.osp.orderscrub.business.ServiceOrderActionBO;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.constants.GDAuthClearConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.TransformationHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.DispatchHelper;
import com.ftd.osp.orderscrub.util.LockUtil;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.orderscrub.util.OrderSaver;
import com.ftd.osp.orderscrub.util.SearchManager;
import com.ftd.osp.orderscrub.vo.GDAuthClearRequestVO;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.ordervalidator.OrderValidator;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.DispositionsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This servlet provides all of the functionality for handling removed orders.
 * It handles both cart and item level removed submits and performs the dispatching
 * to the Order Dispatcher component.  Cart level submits will use the first item
 * data for all item level information in the removed process.  This process does
 * persist to the database.
 *
 * @author Brian Munter
 */

public class RemoveOrderServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.RemoveOrderServlet";
    private final static String REMOVE_MODE = "R";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";
    private final static String XSL_REMOVE = ".../xsl/removeOrder.xsl";
    
    private final static String SOURCE = "ORDER_SCRUB";
    
    private static final String CSP_CANCEL_CODE = "300";
    private static final String NO_MESSSAGE_VALUE = "no message";
    private static final String ORDER_RESPONSE_ROOT = "CC_TRANSMISSION_RESPONSE";
    private static final String ORDER_ELEMENT = "ORDER";
    private static final String CSP_ORDER_NUMBER_ATTRIBUTE = "CSP_ORDER_NO";
    private static final String VENDOR_ORDER_NUMBER_ATTRIBUTE = "VENDOR_ORDER_NO";
    private static final String STATUS_ELEMENT = "STATUS";
    private static final String STATUS_CODE_ATTRIBUTE = "STATUS_CODE";
    private static final String MESSAGE_ATTRIBUTE = "MESSAGE";
    private static final String DATE_ATTRIBUTE = "DATE";
    private static final String REFUND_PROD_ATTRIBUTE = "REFUND_PROD";
    private static final String REFUND_SHIP_ATTRIBUTE = "REFUND_SHIP";
    private static final String REFUND_TAX_ATTRIBUTE = "REFUND_TAX";
    private static final String CSP_REFUND_DEFAULT_AMOUNT= "0";
    private Document responseXML;
    private static final SimpleDateFormat sdfResp = new SimpleDateFormat("MM/dd/yyyy");
    

    private final static HashMap DispositionAdjustmentMap = new HashMap(16);
    
    //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Execute the servlet.  The action is checked to see if the request is for a load or a submit.
     * The default is submit if no action is passed.
     * @param request
     * @param response
     * 
     * @exception ServletException
     * @exception IOException
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        String action = request.getParameter(ACTION);
    
        if (action != null && action.equals(ACTION_LOAD))    
        {
            this.loadRemove(request, response);    
        }
        else
        {
            this.submitRemove(request, response);    
        }
    }




    private void loadRemove(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
    
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load disposition list
            dataRequest.reset();
            dataRequest.setStatementID("DISPOSITION_LIST_LOOKUP");
            dataRequest.addInputParam("mode", REMOVE_MODE);
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

            // Add page data to xml
            HashMap pageData = new HashMap();

            pageData.put("calling_servlet", request.getParameter("calling_servlet"));
            pageData.put("order_guid", request.getParameter("order_guid"));
            pageData.put("item_number", request.getParameter("item_number"));
            pageData.put("master_order_number", request.getParameter("master_order_number"));
            pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
            pageData.put("item_order_number", request.getParameter("item_order_number"));
            pageData.put("company_id", request.getParameter("company_id"));
            pageData.put("disposition", request.getParameter("disposition"));
            pageData.put("called_flag", request.getParameter("called_flag"));
            pageData.put("email_flag", request.getParameter("email_flag"));
            pageData.put("comments", request.getParameter("comments"));
            pageData.put("cart_flag", request.getParameter("cart_flag"));
            pageData.put("order_origin", request.getParameter("order_origin"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            // Add security data to xml
            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

            File xslFile = new File(getServletContext().getRealPath(XSL_REMOVE));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_REMOVE, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. validation: This action is called to validate the data inputted by 
     * the user.  This also handles the forward to email functionality if email 
     * is selected.<br/><br/>
     * 
     * 2. save: This is action is called to save the current order data to the 
     * database.  This is used so we do not need to pass all order information 
     * up to the remove and email pages.<br/><br/>
     * 
     * 3. default: Default functionality to submit the removed order.  This method 
     * handles both cart and item level submits based on a cart flag from the 
     * request.  This method also dispatches the removed order message to the 
     * Order Dispatcher to complete processing.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    private void submitRemove(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        InitialContext context = null;
        UserTransaction userTransaction = null;
        boolean isAmazonOrder = false;
        boolean isWalmartOrder = false;
        boolean isMercentOrder = false;
        boolean isCSPOrder = false;
        boolean isPartnerOrder = false;
        PartnerMappingVO partnerMappingVO = null;
        
        String action = request.getParameter("servlet_action");

        if(action != null && action.equals("validation"))
        {
            try
            {
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
            
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
        
                HashMap pageData = new HashMap();
                HashMap errorMessages = new HashMap();

                String disposition = request.getParameter("disposition");
                String calledFlag = request.getParameter("called_flag");
                String emailFlag = request.getParameter("email_flag");
                String comments = request.getParameter("comments");

                // Load data for display
                pageData.put("calling_servlet", request.getParameter("calling_servlet"));
                pageData.put("order_guid", request.getParameter("order_guid"));
                pageData.put("item_number", request.getParameter("item_number"));
                pageData.put("master_order_number", request.getParameter("master_order_number"));
                pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
                pageData.put("item_order_number", request.getParameter("item_order_number"));
                pageData.put("company_id", request.getParameter("company_id"));
                pageData.put("cart_flag", request.getParameter("cart_flag"));
                pageData.put("order_origin", request.getParameter("order_origin"));
                pageData.put("disposition", disposition);
                pageData.put("called_flag", calledFlag);
                pageData.put("email_flag", emailFlag);
                pageData.put("comments", comments);

                // Add security data to xml
                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

                // Load validation messages and status
                if(disposition == null || disposition.trim().equals(""))
                {
                    errorMessages.put("disposition_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.REMOVE_DISPOSITION_ERROR));  
                }
                if(calledFlag == null || calledFlag.trim().equals(""))
                {
                    errorMessages.put("called_flag_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.REMOVE_CALL_FLAG_ERROR));  
                }
                if(emailFlag == null || emailFlag.trim().equals(""))
                {
                    errorMessages.put("email_flag_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.REMOVE_EMAIL_FLAG_ERROR));  
                }
                if(comments == null || comments.trim().equals(""))
                {
                    errorMessages.put("comments_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.REMOVE_COMMENTS_ERROR));  
                }

                if(errorMessages.size() > 0)
                {
                    pageData.putAll(errorMessages);

                    // Load disposition list
                    dataRequest.reset();
                    dataRequest.setStatementID("DISPOSITION_LIST_LOOKUP");
                    dataRequest.addInputParam("mode", REMOVE_MODE);
                    DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_REMOVE));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_REMOVE, null);
                }
                else if(emailFlag != null && emailFlag.equals("Y"))
                {
                    // Load email page
                    String loadEmailURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/servlet/EmailServlet";
                    loadEmailURL +=  "?master_order_number=" + request.getParameter("master_order_number");
                    loadEmailURL +=  "&order_guid=" + request.getParameter("order_guid");
                    loadEmailURL +=  "&item_number=" + request.getParameter("item_number");
                    loadEmailURL +=  "&buyer_full_name=" + request.getParameter("buyer_full_name");
                    loadEmailURL +=  "&item_order_number=" + request.getParameter("item_order_number");
                    loadEmailURL +=  "&company_id=" + request.getParameter("company_id");
                    loadEmailURL +=  "&calling_servlet=" + request.getParameter("calling_servlet");
                    loadEmailURL +=  "&cart_flag=" + request.getParameter("cart_flag");
                    loadEmailURL +=  "&disposition=" + disposition;
                    loadEmailURL +=  "&called_flag=" + calledFlag;
                    loadEmailURL +=  "&email_flag=" + emailFlag;
                    loadEmailURL +=  "&comments=" + URLEncoder.encode(comments);
                    loadEmailURL +=  "&context=" + request.getParameter("context");
                    loadEmailURL +=  "&securitytoken=" + request.getParameter("securitytoken");

                    response.sendRedirect(loadEmailURL);
                }
                else
                {
                    // Load remove page
                    pageData.put("validation_flag", "Y");

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_REMOVE));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_REMOVE, null);
                }
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else if(action != null && action.equals("save"))
        {
            try
            {
                OrderHelper orderHelper = new OrderHelper();
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");

                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();

                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
                
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                   isAmazonOrder = true;
                   
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;
                
                if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
                	isMercentOrder = true;
                }
                
                // if partner order, set the partner image URL and partner name to order.  
                
                if(!StringUtils.isEmpty(order.getOrderOrigin())) {
        			partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());
        			
        			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
        				isPartnerOrder = true;
        				order.setPartnerOrder(true);
        				order.setPartnerImgURL(partnerMappingVO.getPartnerImage());
        				order.setPartnerName(partnerMappingVO.getPartnerName());
        			}
                }
                
                // Update order with form values
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());

                // Validate order
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
    
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                // Load data for display
                HashMap pageData = new HashMap();
                pageData.put("calling_servlet", request.getParameter("calling_servlet"));
                pageData.put("item_order_number", request.getParameter("item_order_number"));
                pageData.put("cart_flag", request.getParameter("cart_flag"));
                pageData.put("_content", request.getParameter("_content"));
                pageData.put("_focus_object", request.getParameter("_focus_object"));
                pageData.put("item_number", itemNumber);
                pageData.put("popup_flag", "Y");

                // Add page data to xml
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                if(request.getParameter("cart_flag") != null && request.getParameter("cart_flag").equals("Y"))
                {
                    // Save entire order to scrub and frp
                    OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                    orderSaver.saveOrder(order, null, false, request);
                }
                else
                {
                    // Save order and item to scrub and frp
                    OrderDetailsVO item = orderHelper.getItem(order, itemNumber);
                    OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                    orderSaver.saveOrder(order, item, false, request);
                }

                // Update minor items status for display
                StatusManagerBO statusManager = new StatusManagerBO();
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // Convert order to xml and append to response document
                TransformationHelper transformationHelper = new TransformationHelper();
                transformationHelper.processOrder(responseDocument, order);
                
                // Add error popup messages.
                OrderLoader orderLoader = new OrderLoader();
                HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 

                // Return to cart
                File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            try
            {
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");
                String cartFlag = request.getParameter("cart_flag");
            
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
        
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                   isAmazonOrder = true;
                
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;
                
                if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
                	isMercentOrder = true;
                }
                // if partner order, set the partner image URL and partner name to order.                
                if(!StringUtils.isEmpty(order.getOrderOrigin())) {
        			partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());
        			
        			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
        				isPartnerOrder = true;
        				order.setPartnerOrder(true);
        				order.setPartnerImgURL(partnerMappingVO.getPartnerImage());
        				order.setPartnerName(partnerMappingVO.getPartnerName());
        			}
                }
        		
                // Check to see if CSP Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.CSPI_ORIGIN_INTERNET)))
                   isCSPOrder = true;

                // Update order with form values
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());

                OrderDetailsVO item = null;

                if(cartFlag != null && cartFlag.equals("Y"))
                {
                    // Update all items status to pending and set dispositions
                    for(int i = 0; i < order.getOrderDetail().size(); i++)
                    {
                        item = (OrderDetailsVO) order.getOrderDetail().get(i);
                        item.setDispositions(this.loadDispositionList(request));
                        item.setStatus(String.valueOf(OrderStatus.REMOVED_ITEM)); 
                    }//end for
                }
                else
                {
                    // Update item status to pending and set dispositions
                    OrderHelper orderHelper = new OrderHelper();
                    item = orderHelper.getItem(order, itemNumber);
                    item.setDispositions(this.loadDispositionList(request));
                    item.setStatus(String.valueOf(OrderStatus.REMOVED_ITEM)); 
                }

                // Validate order
                OrderValidator orderValidator = new OrderValidator();
                OrderHelper orderHelper = new OrderHelper();
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
    
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Update order status
                StatusManagerBO statusManager = new StatusManagerBO();
                boolean validHeader = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/header[@status='error']")) != null ? false : true;
                boolean itemsComplete = statusManager.updateOrderStatus(validHeader, order, false, request.getParameter("sc_mode"));
                
                // Final processing for order
                OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                DispatchHelper dispatchHelper = new DispatchHelper();

                // Retrieve a user transaction
                context = new InitialContext();
                String jndiTransactionEntry = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.JNDI_TRANSACTION_ENTRY);
                userTransaction = (UserTransaction)  context.lookup(jndiTransactionEntry);

                // Start the transaction
                userTransaction.begin();
                
                ServiceOrderActionBO serviceOrderActionBO = new ServiceOrderActionBO();
                
                if(cartFlag != null && cartFlag.equals("Y"))
                {
                    serviceOrderActionBO.removeServiceOrder(dataRequest.getConnection(), order, getCsrID(request));
                    //send cancel order update status to cams for all the items in the cart when order cart gets removed from scrub
                    serviceOrderActionBO.cancelOrder(dataRequest.getConnection(), order);
                    // Save entire order to scrub and frp
                    orderSaver.saveOrder(order, null, true, request);

                    // Send order and all items to dispatcher
                    for(int i = 0; i < order.getOrderDetail().size(); i++)
                    {
                        item = (OrderDetailsVO) order.getOrderDetail().get(i);
                        if (dispatchHelper.isOrderAlreadyDispatched(item.getOrderDetailId(), dataRequest.getConnection()))
                        {
                          // if the order is already dispatched correctly, add an error to the response document
                          responseDocument = dispatchHelper.addOrderDoubleDispatchXML(responseDocument);
                          logger.debug("Scrub attempt to double dispatch: "+item.getOrderDetailId());
                        }
                        else
                        {

                          dispatchHelper.dispatchOrder(order, item, "ES1005", request, dataRequest.getConnection());
  
                          // for Walmart orders, insert a settlement message
                          if(isWalmartOrder || isCSPOrder)
                          {
                              this.createSettlementMessage(order,
                                                          item,
                                                          dataRequest);
                          }
      
                        
                          	// for Amazon/ MRCNT/ partner orders, create adjustment feed record for price decrease
							if (isAmazonOrder) {
								if (order.getOrderDetail().size() > 1) {
									this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.AMAZON_ORIGIN, true);
								} else {
									orderValidator.insertAmazonAcknowledgement(dataRequest.getConnection(), item.getExternalOrderNumber(), false, request.getParameter("disposition"));
								}
							} else if (isMercentOrder) {
								this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.MRCNT_ORD_INDICATOR, true);
							} else if (isPartnerOrder && (partnerMappingVO != null && partnerMappingVO.isSendAdjustmentFeed())) {
								this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.PTN_ORD_INDICATOR, true);
							} 
							if(partnerMappingVO != null && partnerMappingVO.isSendOrdStatusUpdFeed()) {
								this.sendPartnerOrdStatusJMS(dataRequest.getConnection(), item.getExternalOrderNumber());
							}
                        }
                    }//end for
                }//end if
                else
                {
                    serviceOrderActionBO.removeServiceOrderItem(dataRequest.getConnection(), item, order.getBuyerEmailAddress(), getCsrID(request));
                    //send cancel order update to cams when order item gets delete from scrub.
                    serviceOrderActionBO.cancelOrderItem(dataRequest.getConnection(), item);
                    // Save order and item to scrub and frp
                    orderSaver.saveOrder(order, item, true, request);

                    // Send order and item to dispatcher
                    dispatchHelper.dispatchOrder(order, item, "ES1005", request, dataRequest.getConnection());
                    
                    // for Walmart orders, insert a settlement message
                    if(isWalmartOrder || isCSPOrder)
                    {
                        this.createSettlementMessage(order,
                                                    item,
                                                    dataRequest);
                    }
                    
                    // for Amazon orders, create adjustment feed record
                    if (isAmazonOrder) {
                    	if (order.getOrderDetail().size() > 1) {
                    		this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.AMAZON_ORIGIN, true);  
                    	} else {
                        	orderValidator.insertAmazonAcknowledgement(dataRequest.getConnection(),item.getExternalOrderNumber(), false, request.getParameter("disposition"));                            
                    	}
                    } else if (isMercentOrder) {
						this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.MRCNT_ORD_INDICATOR, true);
					} else if (isPartnerOrder && (partnerMappingVO != null && partnerMappingVO.isSendAdjustmentFeed())) {
						this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.PTN_ORD_INDICATOR, true);
					} else if(partnerMappingVO != null && partnerMappingVO.isSendOrdStatusUpdFeed()) {
						this.sendPartnerOrdStatusJMS(dataRequest.getConnection(), item.getExternalOrderNumber());
					}

                }//end else

                // Commit the transaction
                userTransaction.commit();

                // If order is complete so load next order
                if(itemsComplete)
                {
                	// Use case 23031 - if all the orders from scrub are removed.
                	performGDAuthClear(order, dataRequest.getConnection());
                	
                    // Retrieve guid from search criteria
                    SearchManager searchManager = new SearchManager();
                    SearchResultVO searchResult = searchManager.retrieveResult(request, dataRequest, false);

                    if(searchResult != null) 
                    {
                        // Create the initial document
                        responseDocument = DOMUtil.getDocument();

                        //relese lock on buyer
                        LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);

                        // Load new order to scrub
                        OrderLoader orderLoader = new OrderLoader();
                        orderLoader.loadOrder(searchResult.getGuid(), orderBeforeUpdate, responseDocument, dataRequest, request);

                        File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

                    }
                    else 
                    {
                        logger.debug("** NO ORDERS FOUND TO BE SCRUBBED **");

                        // Load search page
                        String securityParams = "?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);
                        String loadSearchURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/servlet/ScrubSearchServlet" + securityParams + "&sc_mode=" + request.getParameter("sc_mode");
                        response.sendRedirect(loadSearchURL);
                    }
                }
                else
                {
                    // Update minor items status for display
                    statusManager.updateMinorEveryItemStatus(order, responseDocument);
                
                    // Convert order to xml and append to response document
                    TransformationHelper transformationHelper = new TransformationHelper();
                    transformationHelper.processOrder(responseDocument, order);

                    // Load base cart data
                    BaseCartBuilder cartBuilder = new BaseCartBuilder();
                    DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                    // Add error popup messages.
                    OrderLoader orderLoader = new OrderLoader();
                    HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                    DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 
                    
                    // Return to order to continue scrub
                    File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                }
            }
            catch(Exception e)
            {
                try  
                {
                    if (userTransaction != null)  
                    {
                        // Rollback the user transaction
                        userTransaction.rollback();
                        logger.debug("User transaction rolled back");
                    }
                } 
                catch (Exception ex)  
                {
                    logger.error(ex);
                } 
                finally  
                {
                }
                
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                    if(context != null)
                    {
                        context.close();
                    }
                }
                catch(Exception se) 
                {
                    logger.error(se);
                }
            }
        }
    }


	private List loadDispositionList(HttpServletRequest request)
    {
        List dispositionList = new ArrayList();
        DispositionsVO disposition = new DispositionsVO();

        disposition.setDispositionID(request.getParameter("disposition"));
        disposition.setComments(request.getParameter("comments"));
        disposition.setSentEmailFlag(request.getParameter("email_flag"));
        disposition.setCalledCustomerFlag(request.getParameter("called_flag"));
        disposition.setStockMessageID(request.getParameter("message_id"));
        disposition.setEmailSubject(request.getParameter("email_subject"));
        disposition.setEmailMessage(request.getParameter("email_body"));
        
        dispositionList.add(disposition);

        return dispositionList;
    }
    
    /**
     * Creates an adjustment feed record for a single item
     */
    private void createAdjustmentFeedRecord(OrderDetailsVO item, String csrID, DataRequest dataRequest, String partnerType, boolean fullAdjustment) throws Exception {
      
        String adjustmentReason = ((DispositionsVO)item.getDispositions().get(0)).getDispositionID();    
        if(adjustmentReason == null) {
        	logger.error("Invalid adjustment reason");
        	return;
        }
        try {
            PartnerUtility partnerUtility = new PartnerUtility();
            if(ValidationConstants.AMAZON_ORIGIN.equals(partnerType)) {
            	partnerUtility.createAdjustmentFeedRecord(item, csrID, adjustmentReason.toString(), dataRequest.getConnection());
            } else if (ValidationConstants.MRCNT_ORD_INDICATOR.equals(partnerType)) {
            	partnerUtility.createMercentAdjustmentFeedRecord(item, csrID, adjustmentReason.toString(), fullAdjustment, dataRequest.getConnection());
            } else if (ValidationConstants.PTN_ORD_INDICATOR.equals(partnerType)) {
            	partnerUtility.createPartnerAdjustmentFeedRecord(item, csrID, adjustmentReason, fullAdjustment, dataRequest.getConnection());
            } 
          } catch (Exception e) { 
            logger.error("Error caught creating adjustment feed record: " + e.toString());
            throw new Exception("createOrderAdjustment caught: " + e);  
          }
    }
	
	private String createSettlementMessage(OrderVO order,
                                            OrderDetailsVO item,
                                            DataRequest dataRequest) throws Exception
    {
        // Get disposition for the origin
        DispositionsVO distVO = (DispositionsVO) item.getDispositions().get(0);
        String disposition = distVO.getDispositionID();
        String convertedDisposition = this.getConvertedDisposition(order.getOrderOrigin(), disposition, dataRequest);
        if (convertedDisposition == null) {
            convertedDisposition = disposition;
        }
        
        //retrieve the CSP Order Number
        String partnerOrderNumber = null;
        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
             ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.CSPI_ORIGIN_INTERNET))) {
             
            partnerOrderNumber = this.getOrderNumberFromMappping(order.getMasterOrderNumber(),dataRequest);  
            
          
         }
        
        // Insert scrub message
        dataRequest.reset();
        dataRequest.setStatementID("INSERT_SCRUB_MESSAGES");
        dataRequest.addInputParam("IN_PARTNER_ID", order.getOrderOrigin());
        dataRequest.addInputParam("IN_MASTER_ORDER_NUMBER", order.getMasterOrderNumber());
        dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", item.getExternalOrderNumber());
        dataRequest.addInputParam("IN_CREATED_BY", order.getCsrId());
        dataRequest.addInputParam("IN_ERROR_DESCRIPTION", "");

        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
            ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET))) {

            dataRequest.addInputParam("IN_DATA", convertedDisposition);
            dataRequest.addInputParam("IN_MESSAGE_TYPE", "STATUS");
            dataRequest.addInputParam("IN_STATUS", "");
        }
        if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
            ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.CSPI_ORIGIN_INTERNET))) {

            dataRequest.addInputParam("IN_MESSAGE_TYPE", "REFUND");
            dataRequest.addInputParam("IN_STATUS", "NOT SENT");
            
            String refundData = null;
            refundData = this.buildResponseXML(partnerOrderNumber);
            dataRequest.addInputParam("IN_DATA", refundData);
        }
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        Map outputParameters = (Map) dataAccessUtil.execute(dataRequest);
        
        String status = (String) outputParameters.get(STATUS_PARAM);
        if(status != null && status.equals("N"))
        {
            String message = (String) outputParameters.get(MESSAGE_PARAM);
            throw new Exception(message);
        }    
        
        return ((BigDecimal) outputParameters.get("OUT_SCRUB_MESSAGE_ID")).toString();
    }
    
    private String getConvertedDisposition(String orderOrigin, String disposition, DataRequest dataRequest) throws Exception
    {
        dataRequest.reset();
        dataRequest.setStatementID("GET_ORIGIN_DISPOSITION");
        dataRequest.addInputParam("IN_ORIGIN_ID", orderOrigin);
        dataRequest.addInputParam("IN_FTD_DISPOSITION_ID", disposition);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        String convertedDisposition = (String) dataAccessUtil.execute(dataRequest);
        
        return convertedDisposition;
    }
    
    /**
    * This method queries the INBOUND_ORDER_MAPPING table and returns order mapping information
    * for the passed in ftd order number.
    * @param masterOrderNumber ftdOrderNumber
    * @return String partner order number
    * @throws java.lang.Exception
    */
     private String getOrderNumberFromMappping(String masterOrderNumber, DataRequest dataRequest) throws Exception
     {
         String partnerOrderNumber = null;
          
         dataRequest.reset();
         dataRequest.setStatementID("GET_ORDER_MAP_BY_FTD_NUMBER");
         dataRequest.addInputParam("IN_FTD_ORDER_NUMBER", masterOrderNumber);
                  
         DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
         CachedResultSet results = new CachedResultSet();
         results = (CachedResultSet) dataAccessUtil.execute(dataRequest);
        
         while( results.next() )
         {
             partnerOrderNumber =(String)results.getObject("PARTNER_ORDER_NUMBER");
         }
                  
         return partnerOrderNumber;
     
     }
     
    /**
    *
    *  This method will build the response XML to CSP
    * @param partnerOrderNumber String
    * @return  responseXML String
    */
    private String buildResponseXML(String partnerOrderNumber) throws Exception {
        logger.debug("In buildResponseXML() method");
        
        this.createRoot();
        this.addOrder(partnerOrderNumber);
        this.addStatus();
        
        return (this.convertDocToStringWithProlog(responseXML, "UTF-8", null));
    }
    
    /**
    * This method will create the root of the response XML
    * to CSP
    * 
    * @throws Exception
    */
    public void createRoot() throws Exception {
        
        this.responseXML = DOMUtil.getDocumentBuilder().newDocument();
        Element root = this.responseXML.createElement(this.ORDER_RESPONSE_ROOT);
        this.responseXML.appendChild(root);
     }
    
    /**
    *
    *  This method will build the order portion of the XML to CSP
    *
    * @param cspOrderNumber String 
    *
    * @throws Exception
    */
    public void addOrder(String cspOrderNumber) throws Exception {
        
        //retrieve the partner order number
        
        Element orderNode = responseXML.createElement(this.ORDER_ELEMENT);
        orderNode.setAttribute(this.CSP_ORDER_NUMBER_ATTRIBUTE, cspOrderNumber);
        orderNode.setAttribute(this.VENDOR_ORDER_NUMBER_ATTRIBUTE, "");
        Element root = getSingleElement(responseXML, this.ORDER_RESPONSE_ROOT );
        root.appendChild(orderNode);
    }
    
    /**
    *
    *  This method will build the status portion of the XML to CSP
    *
    *
    * @throws Exception
    */
    public void addStatus() throws Exception {
       
        Element statusNode = responseXML.createElement(this.STATUS_ELEMENT);
        statusNode.setAttribute(this.STATUS_CODE_ATTRIBUTE, this.CSP_CANCEL_CODE);
        statusNode.setAttribute(this.MESSAGE_ATTRIBUTE, this.NO_MESSSAGE_VALUE);
        
        String formattedDate = sdfResp.format(new java.util.Date());
        statusNode.setAttribute(this.DATE_ATTRIBUTE, formattedDate);
        
        Element root = getSingleElement(this.responseXML, this.ORDER_RESPONSE_ROOT );
        root.appendChild(statusNode);

    }

    
    /**
     * Returns a single XML element with the given tag name.  If the the tagname
     * does not exist an exception is thrown.
     * 
     * @param element
     * @param tagName
     * @return 
     * @throws java.lang.Exception
     */    
    public static Element getSingleElement(Document xmlDocument,String xpath)  throws Exception
    {
                NodeList dateNodes=null;
                Element element=null;
                assert xmlDocument!=null;
                assert xpath!=null;
                
                xpath = xpath.trim();
                assert xpath.length()>0;


                dateNodes = DOMUtil.selectNodes(xmlDocument, xpath);

                if(dateNodes == null && dateNodes.getLength() == 0)
                {
                	throw new Exception("XMLNode " + xpath + " does not exist.");
                }

                element = (Element)dateNodes.item(0);    

        return element;
    }
    
    /** This method converts an XML document to a String and includes the prolog. 
     * @param XMLDocument document to convert 
     * @return String version of XML */
    public static String convertDocToStringWithProlog(Document xmlDoc, String encoding, String CDATANodes) throws Exception
    {
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = transFactory.newTransformer();
        
        //Sets the format as XML 
        transformer.setOutputProperty(OutputKeys.METHOD, "xml"); 
        //It will indent the xml files 
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        //Set the encoding
        transformer.setOutputProperty(OutputKeys.ENCODING, encoding);
        //Transform the CDATA fields (if necessary
        if(CDATANodes != null) {
            transformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS, CDATANodes);
        }
        
        DOMSource dSource = new DOMSource(xmlDoc);
        StringWriter sw = new StringWriter();
        StreamResult sr = new StreamResult(sw);
        transformer.transform(dSource, sr);
        StringWriter anotherSW = (StringWriter) sr.getWriter();
        StringBuffer sBuffer = anotherSW.getBuffer();
        return (sBuffer.toString()); 
    }  
    
    /**
	 * Use case 23031 - Perform GD Authorization Clear Convenient method to check if GD
	 * payment exists and call the cancelAuthoriaztion payment service.
	 * 
	 * @param order
	 * @param conn
	 * @throws Throwable
	 */
	private void performGDAuthClear(OrderVO order, Connection conn) {
		logger.debug("** performGDAuthClear - entered");
		if (order.getPayments() != null && order.getPayments().size() > 0) {
			logger.debug("** performGDAuthClear - payments exist");
			List<PaymentsVO> payments = order.getPayments();
			for (PaymentsVO payment : payments) {
				if (payment != null && payment.getPaymentType() != null && payment.getPaymentType().equals(GDAuthClearConstants.GD_PAYMENT_TYPE)) {
					logger.debug("** performGDAuthClear - found gift card");
					
					try {
						if(new OrderHelper().areAllItemsRemoved(payment.getGuid())) {
							logger.debug("All the items are completed and are in removed state.Performing Auth clear.");
							GDAuthClearRequestVO gdVO = getGDDetailVO(payment);
							new GDAuthClearBO(gdVO, conn).perform();
						} else {
							logger.debug("** performGDAuthClear -GD auth clear is not performed because  All items are not in removed state");
						}
					} catch (Exception e) {
						logger.error("Error caught checking if all items are removed - Send a system mesage" + e);
						try {
							new GDAuthClearBO().sendSystemMessage(new Exception(GDAuthClearConstants.APOLLO_AUTH_CLEAR_FAILED + ", " + e.getMessage()), false);
						} catch (Throwable t) {
							logger.error("** performGDAuthClear could not send system message " + t);
						}
					}
				}
			}			
		}
	}
	
	/** populate GDAuthClear Request with required details.
	 * GD Authorization clear is used to construct the cancelAuthorization request
	 * set Perform to true and set Send  message to false for the initial request.
	 * @param paymentVo
	 * @return
	 */
	private GDAuthClearRequestVO getGDDetailVO(PaymentsVO paymentVo) {
		GDAuthClearRequestVO gdVO = new GDAuthClearRequestVO();
		CreditCardsVO card = (CreditCardsVO) paymentVo.getCreditCards().get(0);
		gdVO.setAccountNumber(card.getCCNumber());
		gdVO.setAuthNumber(paymentVo.getAuthNumber());
		gdVO.setCaptureTxt(paymentVo.getApAuth());
		gdVO.setOrderGuid(paymentVo.getGuid());
		gdVO.setPaymentAmt(new BigDecimal(paymentVo.getAmount()));
		gdVO.setPaymentId(String.valueOf(paymentVo.getPaymentId()));
		gdVO.setPin(card.getPin());
		gdVO.setTransactionAmt(new BigDecimal(0));
		
		gdVO.setPerform(true);
		gdVO.setSendSysMessage(false);
		
		return gdVO;
	}
	
    /** Send Partner Order Status Update JMS
     * @param conn
     * @param orderNumber
     */
    private void sendPartnerOrdStatusJMS(Connection conn, String orderNumber) {
    	boolean isPOSUSuccess = false;
    	StringBuffer payLoad = null;
    	try {			 
			payLoad = new StringBuffer().append("orderNumbers:").append(orderNumber).append("|").append("operation:").append("REMOVED");
			isPOSUSuccess = sendJMSMessage("SUCCESS", "PROCESS_CREATE_ORD_STATUS_UPDATES", payLoad.toString());			 
		} catch (Exception e) {
			logger.error("Unable to send JMS message for partner order status update feed, ", e);								
		} finally {
			if(!isPOSUSuccess) {
				String posuErrorMessage = "Unable to send JMS message for partner order status update feed. Payload: " + payLoad;
				logger.error(posuErrorMessage);
				sendSystemMessage(conn, posuErrorMessage, "PARTNER INTEGRATION", "PI Order Status Update Error");
			}
		}
	}
    
    /** Send JMS Message for given input
	 * @param status
	 * @param corrId
	 * @param message
	 * @return
	 */
	public boolean sendJMSMessage(String status, String corrId, String message) {
		boolean success = true;
		try {
			MessageToken messageToken = new MessageToken();
			messageToken.setStatus(status);
			messageToken.setJMSCorrelationID(corrId);
			messageToken.setMessage(message);
			Dispatcher dispatcher = Dispatcher.getInstance();
			dispatcher.dispatchTextMessage(new InitialContext(), messageToken);
		} catch (Exception e) {
			logger.error(e);
			success = false;
		}
		return success;
	}
	
	/** Method to send a system message if there is any error processing feed request/scheduled jobs.
	 * @param connection
	 * @param message
	 * @param source
	 * @param Subject 
	 */
	public void sendSystemMessage(Connection connection, String message, String source, String subject) {
		String messageId;
		try {
			logger.error("Sending System Message: " + message);
			
			SystemMessengerVO sysMessage = new SystemMessengerVO();
			sysMessage.setLevel(SystemMessengerVO.LEVEL_PRODUCTION);
			sysMessage.setSource(source);
			sysMessage.setType("ERROR");
			sysMessage.setMessage(message);
			sysMessage.setSubject(subject);

			SystemMessenger sysMessenger = SystemMessenger.getInstance();
			messageId = sysMessenger.send(sysMessage, connection, false);

			if (messageId == null) {				
				logger.error("Error occured while attempting to send a system message. System message is not sent ");
			}
		} catch(Exception e) {
			logger.error("Error caught sending system sytem message for partner integration Feed Error," + message + ", " + e);
		}
	}  

    

}//end class