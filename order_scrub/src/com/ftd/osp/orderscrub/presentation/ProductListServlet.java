package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.presentation.util.EditProductConstants;
import com.ftd.osp.orderscrub.presentation.util.SearchUTIL;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.vo.ProductFlagsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/** This Servlet is used during the Edit Product process.  The servlet
 *  displays a list products based on the previously selected category or country.
 *  @author Ed Mueller
 *  
 *  Note: This object was copied from Order Entry. */
public class ProductListServlet extends HttpServlet 
{
    private ServletConfig config;

    private Logger logger;

    private static final String PRODUCT_LIST_XSL = ".../xsl/productList.xsl";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
        logger = new Logger("com.ftd.osp.orderscrub.presentation.ProductListServlet");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {}

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }

    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
 
        response.setHeader("Cache-Control", "no-cache");

        String partnerId = request.getParameter("partner_id");
        String categoryindex = request.getParameter("category_index");
        String pageNumber = request.getParameter("page_number");
        String sortType = request.getParameter("sort_type");
        String customerName = request.getParameter("buyer_full_name");
        String pricePointId = request.getParameter("price_point_id");     
        String orderDeliveryZipCode = request.getParameter("postal_code");     
        String sourceCode = request.getParameter("source_code");
        String scriptCode = request.getParameter("script_code");
        String rewardType = request.getParameter("reward_type");
        String companyId = request.getParameter("company_id");
        String sendToCustomerCountry = request.getParameter("country");
        String sendToCustomerState = request.getParameter("state");
        String masterOrderNumber = request.getParameter("master_order_number");
        String origDate = request.getParameter("orig_date");
        String item = request.getParameter("item_order_number");
        String occText = request.getParameter("occasion_text");
        String occasionId = request.getParameter("occasion_id");
        String pageName = request.getParameter("page_name");
        String jcpFlag = request.getParameter("jcp_flag");
        String guid = request.getParameter("order_guid");
        String lineNumber = request.getParameter("line_number");
        String domesticIntlFlag = request.getParameter("domestic_flag");
        String deliveryDate = request.getParameter("delivery_date");
        
      // Load data for display
      HashMap pageDataMap = new HashMap();
      pageDataMap.put("domestic_flag",domesticIntlFlag);
      pageDataMap.put("category_index", categoryindex);
      pageDataMap.put("page_number", pageNumber);
      pageDataMap.put("sort_type", sortType);
      pageDataMap.put("master_order_number", masterOrderNumber);
      pageDataMap.put("orig_date", origDate);
      pageDataMap.put("buyer_full_name", customerName);
      pageDataMap.put("item_order_number", item);
      pageDataMap.put("occasion_text", occText);
      pageDataMap.put("occasion_id",occasionId);
      pageDataMap.put("company_id", companyId);
      pageDataMap.put("source_code", sourceCode);
      pageDataMap.put("page_name", pageName);
      pageDataMap.put("country", sendToCustomerCountry);   
      pageDataMap.put("state",sendToCustomerState);
      pageDataMap.put("reward_type",rewardType);
      pageDataMap.put("jcp_flag",jcpFlag);      
      pageDataMap.put("order_guid", guid);
      pageDataMap.put("line_number", lineNumber);
      pageDataMap.put("delivery_date", deliveryDate);

        //values retrieved from DB
        String internationalServiceFee = "";
        String domesticServiceFee = "";
        String pricingCode = "";
        boolean isSendToCustomerDomestic = true;
        
        ProductFlagsVO flagVO = new ProductFlagsVO();
        List promotionList = new ArrayList();        

        SearchUTIL searchUTIL = new SearchUTIL();

        DataRequest dataRequest = null;

        try
        {  

          //get image location
          String imageLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, EditProductConstants.PRODUCT_IMAGE_LOCATION);          
          pageDataMap.put("product_images",imageLocation);


        if(categoryindex == null) categoryindex = "";

     

          dataRequest = DataRequestHelper.getInstance().getDataRequest();

          DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();

          //get source code info                    
          dataRequest.setStatementID("SOURCE_CODE_RECORD_LOOKUP");
          dataRequest.addInputParam("source_code",sourceCode);
          CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);                 
          String snhID = "";
          if(rs != null && rs.next()){
              pricingCode = getValue(rs.getObject(7));
              snhID = getValue(rs.getObject(8));          
              partnerId = getValue(rs.getObject(9));
          }

          //get fee info
          logger.debug("get_snh_by_id: " + snhID + " " + deliveryDate);
          dataRequest.reset();
          dataRequest.setStatementID("GET_SNH_BY_ID");
          dataRequest.addInputParam("IN_SNH_ID",snhID);
          dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate);
          rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);                 
          if(rs != null && rs.next()){
              domesticServiceFee = getValue(rs.getObject(3));
              internationalServiceFee = getValue(rs.getObject(6));
          }
          else
          {
              domesticServiceFee = "0";
              internationalServiceFee = "0";
          }
          
        isSendToCustomerDomestic = searchUTIL.isDomestic(sendToCustomerCountry);

        Document xml = searchUTIL.getProductsByCategory( pageNumber, categoryindex,
           orderDeliveryZipCode,  isSendToCustomerDomestic, 
           sourceCode, pricePointId, sendToCustomerCountry,
           scriptCode,  domesticServiceFee,  internationalServiceFee,
           sendToCustomerState,  pricingCode,  partnerId,
           promotionList,  rewardType, flagVO, sortType, customerName, pageDataMap,
           getServletContext(), deliveryDate, origDate);


            HashMap params = new HashMap();
 
            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(xml, "security", "data", securityData, true);

            //fix HTML encoding
            searchUTIL.fixXML(xml);

            //append addons
            xml = searchUTIL.appendAddons(request,xml);

            logger.debug(searchUTIL.convertDocToString(xml));

            File xslFile = new File(getServletContext().getRealPath(PRODUCT_LIST_XSL));
            TraxUtil.getInstance().transform(request, response, xml, xslFile, PRODUCT_LIST_XSL,params);      
   
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
          try{
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
          }
          catch(Exception e)  {} 
        }
    }








 
  /* This method takes in an object and returns
   * it's string value.
   * @param Object
   * @return String value of object
   *         If object is null, null is returned*/
  private String getValue(Object obj) throws Exception
  {

    String value = null;

    if(obj == null)
    {
      return null;
    }

    if(obj instanceof BigDecimal) {
      value = obj.toString();      
    }
    else if(obj instanceof String) {
      value = (String)obj;
    }
    else if(obj instanceof Timestamp) {
      value = obj.toString();
    }
    else
    {
      value = obj.toString();
    }

    return value;
  }

}