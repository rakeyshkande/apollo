package com.ftd.osp.orderscrub.presentation;

import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Base servlet class for the common functionality of the servlets in order scrub.
 */
public abstract class BaseScrubServlet extends HttpServlet
{
    protected final static String CONTENT_TYPE = "text/html; charset=windows-1252";
    protected final static String ACTION = "action";
    protected final static String ACTION_LOAD = "load";

    protected abstract void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

    /**
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.execute(request, response);          
    }

    /**
     * Perform the processing of a POST to the servlet.  The call is delegated to the execute 
     * method.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        this.execute(request, response);
    }


   /**
   * Retrieves the CSR Id from the request
   * @param request
   * @return
   */
    public String getCsrID(HttpServletRequest request)
    {
        String csrId = "SYSTEM";
        
        try
        {
        String securityToken = request.getParameter("securitytoken");
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securityToken);      
        csrId = userInfo.getUserID();      
        } catch (Exception e)
        {
          // NOP 
        }
        
        return csrId;
    }
}