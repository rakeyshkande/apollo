package com.ftd.osp.orderscrub.presentation;

import com.ftd.legacy.dao.LegacySourceCodeDAO;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * This servlet is used to generate the florist listing page which is composed 
 * of florists matching the inputted search criteria.
 *
 * @author Brian Munter
 */

public class FloristLookupServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.FloristLookupServlet";
    private final static String XSL_FLORIST_LOOKUP = ".../xsl/floristLookup.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Loads the data to display the florist listing page based off of the user 
     * search criteria.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadFloristSearch(request, response);          
    }

    private void loadFloristSearch(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        String legacyId = null;
        String sourceCode = null;
        
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            logger.info ("FloristLookupServlet -- "
              + " institutionInput = " + request.getParameter("institutionInput")
              + " AND phoneInput = " + request.getParameter("phoneInput")
              + " AND addressInput = " + request.getParameter("addressInput")
              + " AND cityInput = " + request.getParameter("cityInput")
              + " AND stateInput = " + request.getParameter("stateInput")
              + " AND zipCodeInput = " + request.getParameter("zipCodeInput")
              + " AND countryInput = " + request.getParameter("countryInput")
              + " AND productId = " + request.getParameter("productId")
              + " AND sourceCodeInput = " + request.getParameter("sourceCodeInput")
              + " AND orderNumber = " + request.getParameter("orderNumber")
              + " AND deliveryDate = " + request.getParameter("deliveryDate")
              + " AND deliveryDateRangeEnd = " + request.getParameter("deliveryDateRangeEnd")
                        );
            
            sourceCode = request.getParameter("sourceCodeInput");
            
            ConfigurationUtil configUtil = new ConfigurationUtil();
			
			String lpsfCheck = configUtil.getFrpGlobalParm("PI_CONFIG","PRMY_BCKP_FLORIST_LEGACY_ID_LOOK_UP");
            
            //Setting legacyId
			if(request.getParameter("orderNumber") != null){
				legacyId = getLegacyId(request.getParameter("orderNumber"),dataRequest);
			}
            logger.info("Legacy Id  : "+legacyId);
            logger.info("LPSF Check Value "+lpsfCheck);
			LegacySourceCodeDAO legacyDAO = new LegacySourceCodeDAO(dataRequest.getConnection());
            if(lpsfCheck.equals("Y") && legacyId != null ){
            	sourceCode = legacyDAO.getSourceCode(legacyId);
            	logger.info("Source code for Legacy Id set to : "+sourceCode);
            }
                        
            // Add page data to xml
            HashMap pageData = new HashMap();
            pageData.put("institutionInput", request.getParameter("institutionInput"));
            pageData.put("phoneInput", request.getParameter("phoneInput"));
            pageData.put("addressInput", request.getParameter("addressInput"));
            pageData.put("cityInput", request.getParameter("cityInput"));
            pageData.put("stateInput", request.getParameter("stateInput"));
            pageData.put("zipCodeInput", request.getParameter("zipCodeInput"));
            pageData.put("countryInput", request.getParameter("countryInput"));
            pageData.put("productId", request.getParameter("productId"));
            pageData.put("sourceCodeInput", sourceCode);
            pageData.put("orderNumber", request.getParameter("orderNumber"));
            pageData.put("deliveryDate", request.getParameter("deliveryDate"));
            pageData.put("deliveryDateRangeEnd", request.getParameter("deliveryDateRangeEnd"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            
            

            //For canada zipcodes only use the first 3 chars
            String country = request.getParameter("countryInput");
            String zip = request.getParameter("zipCodeInput");
            if(country.equals("CA")){
                if(zip !=null && zip.length() > 3)
                {
                  zip = zip.substring(0, 3);
                }
            }
            
    		String dDate = request.getParameter("deliveryDate");
    		String dDateEnd = request.getParameter("deliveryDateRangeEnd");
    		Date deliveryDate = null;
    		if(dDate != null && !dDate.isEmpty() && !dDate.equalsIgnoreCase("")){
    			deliveryDate = new Date(dDate);
    		}
    		Date deliveryDateEnd = null;
    		if(dDateEnd != null && !dDateEnd.isEmpty() && !dDateEnd.equalsIgnoreCase("")){
    			deliveryDateEnd = new Date(dDateEnd);
    		}
    		
            // Load source code list from database
            dataRequest.reset();
            dataRequest.setStatementID("GET_FLORIST_LIST");
            dataRequest.addInputParam("city", request.getParameter("cityInput"));
            dataRequest.addInputParam("state", request.getParameter("stateInput"));
            dataRequest.addInputParam("zip_code", zip);
            dataRequest.addInputParam("florist_name", request.getParameter("institutionInput"));
            dataRequest.addInputParam("phone", request.getParameter("phoneInput"));
            dataRequest.addInputParam("address", request.getParameter("addressInput"));
            dataRequest.addInputParam("product_id", request.getParameter("productId"));
            dataRequest.addInputParam("IN_SOURCE_CODE", sourceCode);
            dataRequest.addInputParam("IN_DELIVERY_DATE", deliveryDate == null ? null : new java.sql.Date(deliveryDate.getTime()));
            dataRequest.addInputParam("IN_DELIVERY_DATE_END", deliveryDateEnd == null ? null : new java.sql.Date(deliveryDateEnd.getTime()));
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            //Load states
            dataRequest.reset();
            dataRequest.setStatementID("STATE_LIST_LOOKUP");
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            File xslFile = new File(getServletContext().getRealPath(XSL_FLORIST_LOOKUP));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_FLORIST_LOOKUP, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

	private String getLegacyId(String orderNumber,DataRequest dataRequest) {
		
		String legacyID = null;
		dataRequest.setConnection(dataRequest.getConnection());
		dataRequest.setStatementID("LEGACY_ID_FOR_ORDERNUMBER");
		dataRequest.addInputParam("IN_EXTERNAL_ORDER_NUMBER", orderNumber);

		DataAccessUtil dataAccessUtil;
		try {
			dataAccessUtil = DataAccessUtil.getInstance();
			CachedResultSet rs = (CachedResultSet) dataAccessUtil.execute(dataRequest);
	        
		       if(rs!=null && rs.next()){
		    	   legacyID = rs.getString("LEGACY_ID");
		       }
		} catch (Exception e) {
			logger.error("Error obtaining LegacyId for "+orderNumber);
			e.printStackTrace();
		}
		return legacyID;
	}
}
