package com.ftd.osp.orderscrub.presentation;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

/**
 * This servlet is used to generate the order comments page to allow users 
 * to enter comments for the current order.
 *
 * @author Brian Munter
 */

public class CommentsServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.CommentsServlet";
    private final static String XSL_COMMENTS = ".../xsl/orderComments.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Loads the data to display the order comments page for the current order.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadComments(request, response);          
    }


    private void loadComments(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
    
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load fraud dispositions
            dataRequest.reset();
            dataRequest.setStatementID("VIEW_ORDER_DISPOSITIONS");
            dataRequest.addInputParam("order_guid", request.getParameter("order_guid"));
            dataRequest.addInputParam("line_number", request.getParameter("line_number"));
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

            File xslFile = new File(getServletContext().getRealPath(XSL_COMMENTS));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_COMMENTS, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

}