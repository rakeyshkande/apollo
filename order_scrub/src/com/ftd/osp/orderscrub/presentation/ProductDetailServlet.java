package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.presentation.util.EditProductConstants;
import com.ftd.osp.orderscrub.presentation.util.SearchUTIL;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.vo.ProductFlagsVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/** This Servlet is used during the Edit Product process.  The servlet
 *  displays information regarding a selected product.
 *  @author Ed Mueller
 *  
 *  Note: This object was copied from Order Entry. */
public class ProductDetailServlet extends HttpServlet 
{
    private ServletConfig config;
    private Logger logger ;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
        logger = new Logger("com.ftd.osp.orderscrub.presentation.ProductDetailServlet");                
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (SecurityHelper.isValidToken(request, response)) 
        {
            process(request,response);
        }
    }


    public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setHeader("Cache-Control", "no-cache");

        String sessionId = request.getParameter("sessionId");
        String command = request.getParameter("command");
        if(command == null) command = "";




            handleDefaultAction(request, response);

    }



    private void handleDefaultAction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

        String city = request.getParameter("city");
        String deliveryDate = request.getParameter("delivery_date");

        String searchType = request.getParameter("search_type");        
        String productid = request.getParameter("product_id");             
        String sourceCode = request.getParameter("source_code");
        String rewardType = request.getParameter("reward_type");
        String state = request.getParameter("state");
        String pricingCode = request.getParameter("pricing_code");
        String domesticServiceFee = request.getParameter("domestic_service_fee");
        String partnerId = request.getParameter("partner_id");
        ArrayList promotionList = new ArrayList();
        ProductFlagsVO flagsVO = new ProductFlagsVO();
        String pageName = request.getParameter("script_code");
        String zipCode = request.getParameter("postal_code");
        String domesitcIntlFlag = request.getParameter("domestic_flag");
        boolean domesticFlag = domesitcIntlFlag.equals("D");
        String guid = request.getParameter("order_guid");
        String lineNumber = request.getParameter("line_number");
        String internationalServiceFee = request.getParameter("international_service_fee");
        String occasion = request.getParameter("occasion_id");
        String company = request.getParameter("company_id");
        String categoryIndex = request.getParameter("category_index");
        String sortType = request.getParameter("sort_type");        
        String buyerName = request.getParameter("buyer_full_name");  
        String masterOrderNumber = request.getParameter("master_order_number");  
        String origDate = request.getParameter("orig_date");
        String itemOrderNumber = request.getParameter("item_order_number");
        String pricePointId = request.getParameter("price_point_id");
        String scriptCode = request.getParameter("script_code");
        String country = request.getParameter("country");
        String jcpFlag = request.getParameter("jcp_flag");
        String occText = request.getParameter("occasion_text");
        String upsellFlag = request.getParameter("upsell_flag");
        String listFlag = request.getParameter("list_flag");       
        String upsellID = request.getParameter("upsell_id");
        //#587  - Flex-Fill 
        // Added the Shiping method of the original order. 
        String shippingMethod = request.getParameter("shipping_method");
       
        
        if(upsellFlag == null || upsellFlag.length() <=0)
        {
          upsellFlag = "N";
        }
        if(listFlag == null || listFlag.length() <=0)
        {
          listFlag = "N";
        }
        
        HashMap pageData = new HashMap();
        pageData.put("upsell_id",upsellID);        
        pageData.put("list_flag",listFlag);        
        pageData.put("upsell_flag",upsellFlag);        
        pageData.put("city",city);
        pageData.put("delivery_date",deliveryDate);
        pageData.put("search_type",searchType);
        pageData.put("product_id",productid);
        pageData.put("source_code",sourceCode);
        pageData.put("reward_type",rewardType);
        pageData.put("state",state);
        pageData.put("pricing_code",pricingCode);
        pageData.put("domestic_service_fee",domesticServiceFee);
        pageData.put("partner_id",partnerId);
        pageData.put("script_code",scriptCode);
        pageData.put("postal_code",zipCode);
        pageData.put("domestic_flag",domesitcIntlFlag);
        pageData.put("international_service_fee",internationalServiceFee);
        pageData.put("occasion_id",occasion);
        pageData.put("company_id",company);
        pageData.put("category_index",categoryIndex);
        pageData.put("sort_type",sortType);
        pageData.put("buyer_full_name",buyerName);
        pageData.put("master_order_number", masterOrderNumber);
        pageData.put("orig_date", origDate);
        pageData.put("item_order_number", itemOrderNumber);
        pageData.put("price_point_id",pricePointId);
        pageData.put("script_code",scriptCode);
        pageData.put("country",country);
        pageData.put("jcp_flag",jcpFlag);
        pageData.put("occasion_text", occText);
        pageData.put("order_guid", guid);
        pageData.put("line_number", lineNumber);
        pageData.put("delivery_date", deliveryDate);
        //#587  - Flex-Fill 
        // Added the Shiping method of the original order. 
        pageData.put("shipping_method", shippingMethod);

          HashMap args = new HashMap();
          args.put("company", "FTDP");
          args.put("pagename", "ProductDTLFloral");
          args.put("productId", productid);

          SearchUTIL searchUtil = new SearchUTIL();

        String error = null;

        try
        {
            //get image location
            String imageLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, EditProductConstants.PRODUCT_IMAGE_LOCATION);          
            pageData.put("product_images",imageLocation);

            args = new HashMap();
            Document xml = searchUtil.getProductByID( sourceCode,  zipCode,  country,
               productid,  searchType,  domesticFlag, 
               state,  pricingCode,  domesticServiceFee,
               internationalServiceFee,  partnerId,  promotionList,
               rewardType,  flagsVO,  occasion,
               pageName,  company, deliveryDate,
               city ,  deliveryDate, origDate, pageData);

            if(args != null && args.containsKey(EditProductConstants.UPSELL_EXISTS)) 
            {
                String xslFile = ".../xsl/upsell.xsl";

                if(args.containsKey(EditProductConstants.UPSELL_SKIP) 
                    && !((String) args.get(EditProductConstants.UPSELL_SKIP)).equals("N"))
                {
                    args.put("productId", (String) args.get(EditProductConstants.UPSELL_SKIP));

                    xml = searchUtil.getProductByID( sourceCode,  zipCode,  country,
                               productid,  searchType,  domesticFlag, 
                               state,  pricingCode,  domesticServiceFee,
                               internationalServiceFee,  partnerId,  promotionList,
                               rewardType,  flagsVO,  occasion,
                               pageName,  company, deliveryDate,
                               city/*,  soonerOverride*/,  deliveryDate, origDate, pageData);

            
                    xslFile = ".../xsl/productDetail.xsl";
                }

                xslFile = getServletContext().getRealPath(xslFile);
                HashMap params = new HashMap();

                params.put("actionServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));//"/servlet/XSLTestServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("searchType", searchType);  

                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(xml, "security", "data", securityData, true);

                //fix HTML encoding
                searchUtil.fixXML(xml);

                //append addons
                xml = searchUtil.appendAddons(request,xml);
                
                //append product attribute restriction info
                xml = searchUtil.appendProductAttributeExclusions(productid, sourceCode, xml);
                
                logger.debug(searchUtil.convertDocToString(xml));

                File file = new File(getServletContext().getRealPath(xslFile));
                TraxUtil.getInstance().transform(request, response, xml, file, xslFile, params);      
            }
            else 
            {
                String xslFile = ".../xsl/productDetail.xsl";
                HashMap params = new HashMap();

                params.put("actionServlet", (request.getContextPath() + "/servlet/DeliveryServlet"));//"/servlet/XSLTestServlet"));
                params.put("refreshServlet", (request.getContextPath() + "/servlet/ProductDetailServlet"));
                params.put("cancelItemServlet", (request.getContextPath() + "/servlet/CancelItemServlet"));
                params.put("cancelItemShoppingServlet", (request.getContextPath() + "/servlet/CancelItemShoppingServlet"));
                params.put("searchType", searchType);  

                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(xml, "security", "data", securityData, true);

                //fix HTML encoding
                searchUtil.fixXML(xml);

                //append addons
                xml = searchUtil.appendAddons(request,xml);
                
                //append product attribute restriction info
                xml = searchUtil.appendProductAttributeExclusions(productid, sourceCode, xml);

                logger.debug(searchUtil.convertDocToString(xml));

                File file = new File(getServletContext().getRealPath(xslFile));
                TraxUtil.getInstance().transform(request, response, xml, file, xslFile, params);      

            }
            
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }

    }





/**
 * Description: Takes a Util Date in the format yyyy-mm-dd and formats it
 *              as MM/dd/yyyy (which is the format we show in the screens).
 *
 * @param java.util.Date date to convert
 * @return String
 * @throws ParseException
 */
public static String formatUtilDateToString(java.util.Date utilDate) throws ParseException
{
	String strDate = "";
	String inDateFormat = "";
	java.util.Date newDate = null;

	if (utilDate != null) {
        SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

          newDate = dfOut.parse(dfOut.format(utilDate));
          strDate = dfOut.format(newDate);

    } 
    else {
        strDate = "";
    }

	return strDate;
}




}