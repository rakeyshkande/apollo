package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.CacheManager;
import com.ftd.osp.utilities.cacheMgr.constant.CacheMgrConstants;
import com.ftd.osp.utilities.cacheMgr.handlers.ContentHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.JAXPUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This servlet is used to generate a display message based on the inputed criteria.
 *
 * @author Tim Schmig
 */

public class DisplayPopupServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.DisplayPopupServlet";
    private final static String XSL_DISPLAY_POPUP = ".../xsl/displayPopup.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }


    /**
     * Execute the servlet.  The action is checked to see if the request is for a load or a submit.
     * The default is submit if no action is passed.
     * @param request
     * @param response
     * 
     * @exception ServletException
     * @exception IOException
     */
    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        try
        {
            // Create the initial document
            Document responseDoc = JAXPUtil.createDocument();
            String responseString = "";
            String responseHeader = "";

            Element root = responseDoc.createElement("root");
            responseDoc.appendChild(root);

            Element displayHeader = responseDoc.createElement("display_header");
            root.appendChild(displayHeader);
            
            Element displayElement = responseDoc.createElement("display_message");
            root.appendChild(displayElement);  
            
            String action = request.getParameter("servlet_action");
            logger.info("action: " + action);
            
            if (action != null && action.equalsIgnoreCase("surcharge_explanation")) {
                CacheManager cacheMgr = CacheManager.getInstance();
                ContentHandler h = (ContentHandler)cacheMgr.getHandler(CacheMgrConstants.CACHE_NAME_CONTENT_WITH_FILTER);
                responseString = h.getContentWithFilter("TAX", "TAX_EXPLANATION", null, null);
                responseHeader = "Explanation";
            }

            displayHeader.appendChild(responseDoc.createCDATASection(responseHeader));
            displayElement.appendChild(responseDoc.createCDATASection(responseString));

            StringWriter sw = new StringWriter();
            DOMUtil.print(responseDoc, sw);
            logger.info(sw.toString());

            File xslFile = new File(getServletContext().getRealPath(XSL_DISPLAY_POPUP));
            TraxUtil.getInstance().transform(request, response, responseDoc, xslFile, XSL_DISPLAY_POPUP, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
    }

}