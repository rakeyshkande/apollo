package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.orderscrub.business.ServiceOrderActionBO;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.TransformationHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.DispatchHelper;
import com.ftd.osp.orderscrub.util.LockUtil;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.orderscrub.util.OrderSaver;
import com.ftd.osp.orderscrub.util.SearchManager;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.ordervalidator.OrderValidator;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.DispositionsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This servlet provides all of the functionality for handling pending orders. 
 * It handles both cart and item level pending submits and performs the dispatching 
 * to the Order Dispatcher component.  Cart level submits will use the first item 
 * data for all item level information in the pending process.  This process does 
 * persist to the database.
 *
 * @author Brian Munter
 */

public class PendingOrderServlet extends BaseScrubServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.PendingOrderServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";
    private final static String XSL_PENDING = ".../xsl/pendingOrder.xsl";
    private final static String PENDING_MODE = "P";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }


    /**
     * Loads the data to display the pending page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
    
        String action = request.getParameter(ACTION);
        if (action != null && action.equals(ACTION_LOAD))
        {
            this.loadPending(request, response);          
        }
        else
        {
            this.submitPending(request, response);          
        }
    }

    /**
     * Loads the data to display the pending page.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    private void loadPending(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
    
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load disposition list
            dataRequest.reset();
            dataRequest.setStatementID("DISPOSITION_LIST_LOOKUP");
            dataRequest.addInputParam("mode", PENDING_MODE);
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

            // Add page data to xml
            HashMap pageData = new HashMap();
            pageData.put("calling_servlet", request.getParameter("calling_servlet"));
            pageData.put("order_guid", request.getParameter("order_guid"));
            pageData.put("item_number", request.getParameter("item_number"));
            pageData.put("master_order_number", request.getParameter("master_order_number"));
            pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
            pageData.put("item_order_number", request.getParameter("item_order_number"));
            pageData.put("company_id", request.getParameter("company_id"));
            pageData.put("disposition", request.getParameter("disposition"));
            pageData.put("called_flag", request.getParameter("called_flag"));
            pageData.put("email_flag", request.getParameter("email_flag"));
            pageData.put("comments", request.getParameter("comments"));
            pageData.put("cart_flag", request.getParameter("cart_flag"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

            // Add security data to xml
            HashMap securityData = new HashMap();
            securityData.put("context", request.getParameter("context"));
            securityData.put("securitytoken", request.getParameter("securitytoken"));
            DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

            File xslFile = new File(getServletContext().getRealPath(XSL_PENDING));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_PENDING, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. validation: This action is called to validate the data inputted by 
     * the user.  This also handles the forward to email functionality if email 
     * is selected.<br/><br/>
     * 
     * 2. save: This is action is called to save the current order data to the 
     * database.  This is used so we do not need to pass all order information 
     * up to the pending and email pages.<br/><br/>
     * 
     * 3. default: Default functionality to submit the pending order.  This method 
     * handles both cart and item level submits based on a cart flag from the 
     * request.  This method also dispatches the pending order message to the 
     * Order Dispatcher to complete processing.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    private void submitPending(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        InitialContext context = null;
        UserTransaction userTransaction = null;
        boolean isAmazonOrder = false;
        boolean isWalmartOrder = false;
        boolean isMercentOrder = false;
        boolean isPartnerOrder = false;
        String action = request.getParameter("servlet_action");
        
        if(action != null && action.equals("validation"))
        {
            try
            {
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
            
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();
        
                HashMap pageData = new HashMap();
                HashMap errorMessages = new HashMap();

                String disposition = request.getParameter("disposition");
                String calledFlag = request.getParameter("called_flag");
                String emailFlag = request.getParameter("email_flag");
                String comments = request.getParameter("comments");
                String pro = request.getParameter("sc_pro_confirmation_number");
                // Load data for display
                pageData.put("calling_servlet", request.getParameter("calling_servlet"));
                pageData.put("order_guid", request.getParameter("order_guid"));
                pageData.put("item_number", request.getParameter("item_number"));
                pageData.put("master_order_number", request.getParameter("master_order_number"));
                pageData.put("buyer_full_name", request.getParameter("buyer_full_name"));
                pageData.put("item_order_number", request.getParameter("item_order_number"));
                pageData.put("company_id", request.getParameter("company_id"));
                pageData.put("cart_flag", request.getParameter("cart_flag"));
                pageData.put("disposition", disposition);
                pageData.put("called_flag", calledFlag);
                pageData.put("email_flag", emailFlag);
                pageData.put("comments", comments);

                // Add security data to xml
                HashMap securityData = new HashMap();
                securityData.put("context", request.getParameter("context"));
                securityData.put("securitytoken", request.getParameter("securitytoken"));
                DOMUtil.addSection(responseDocument, "security", "data", securityData, true);

                // Load validation messages and status
                if(disposition == null || disposition.trim().equals(""))
                {
                    errorMessages.put("disposition_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PENDING_DISPOSITION_ERROR));  
                }
                if(calledFlag == null || calledFlag.trim().equals(""))
                {
                    errorMessages.put("called_flag_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PENDING_CALL_FLAG_ERROR));  
                }
                if(emailFlag == null || emailFlag.trim().equals(""))
                {
                    errorMessages.put("email_flag_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PENDING_EMAIL_FLAG_ERROR));  
                }
                if(comments == null || comments.trim().equals(""))
                {
                    errorMessages.put("comments_msg", ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.PENDING_COMMENTS_ERROR));  
                }

                if(errorMessages.size() > 0)
                {
                    pageData.putAll(errorMessages);

                    // Load disposition list
                    dataRequest.reset();
                    dataRequest.setStatementID("DISPOSITION_LIST_LOOKUP");
                    dataRequest.addInputParam("mode", PENDING_MODE);
                    DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes());

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_PENDING));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_PENDING, null);
                }
                else if(emailFlag != null && emailFlag.equals("Y"))
                {
                    // Load email page
                    String loadEmailURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/servlet/EmailServlet";
                    loadEmailURL +=  "?master_order_number=" + request.getParameter("master_order_number");
                    loadEmailURL +=  "&order_guid=" + request.getParameter("order_guid");
                    loadEmailURL +=  "&item_number=" + request.getParameter("item_number");
                    loadEmailURL +=  "&buyer_full_name=" + request.getParameter("buyer_full_name");
                    loadEmailURL +=  "&item_order_number=" + request.getParameter("item_order_number");
                    loadEmailURL +=  "&company_id=" + request.getParameter("company_id");
                    loadEmailURL +=  "&calling_servlet=" + request.getParameter("calling_servlet");
                    loadEmailURL +=  "&cart_flag=" + request.getParameter("cart_flag");
                    loadEmailURL +=  "&disposition=" + disposition;
                    loadEmailURL +=  "&called_flag=" + calledFlag;
                    loadEmailURL +=  "&email_flag=" + emailFlag;
                    loadEmailURL +=  "&comments=" + URLEncoder.encode(comments);
                    loadEmailURL +=  "&context=" + request.getParameter("context");
                    loadEmailURL +=  "&securitytoken=" + request.getParameter("securitytoken");
                    response.sendRedirect(loadEmailURL);
                }
                else
                {
                    // Load pending page
                    pageData.put("validation_flag", "Y");

                    // Add page data to xml
                    DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                    File xslFile = new File(getServletContext().getRealPath(XSL_PENDING));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_PENDING, null);
                }
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else if(action != null && action.equals("save"))
        {
            try
            {
                OrderHelper orderHelper = new OrderHelper();
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");

                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();

                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                   isAmazonOrder = true;
                
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;
                
                if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
                	isMercentOrder = true;
                }  
                
                if(!StringUtils.isEmpty(order.getOrderOrigin())) {
        			PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());
        			
        			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
        				isPartnerOrder = true;
        				order.setPartnerOrder(true);
        				order.setPartnerImgURL(partnerMappingVO.getPartnerImage());
        				order.setPartnerName(partnerMappingVO.getPartnerName());
        			}
                }
               
                // Update order with form values
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());

                // Validate order
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
    
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                // Load data for display
                HashMap pageData = new HashMap();
                pageData.put("calling_servlet", request.getParameter("calling_servlet"));
                pageData.put("item_order_number", request.getParameter("item_order_number"));
                pageData.put("cart_flag", request.getParameter("cart_flag"));
                pageData.put("_content", request.getParameter("_content"));
                pageData.put("_focus_object", request.getParameter("_focus_object"));
                pageData.put("item_number", itemNumber);
                pageData.put("popup_flag", "Y");

                // Add page data to xml
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);

                if(request.getParameter("cart_flag") != null && request.getParameter("cart_flag").equals("Y"))
                {
                    // Save entire order to scrub and frp
                    OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                    orderSaver.saveOrder(order, null, false, request);
                }
                else
                {
                    // Save order and item to scrub and frp
                    OrderDetailsVO item = orderHelper.getItem(order, itemNumber);
                    OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                    orderSaver.saveOrder(order, item, false, request);
                }

                // Update minor items status for display
                StatusManagerBO statusManager = new StatusManagerBO();
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // Convert order to xml and append to response document
                TransformationHelper transformationHelper = new TransformationHelper();
                transformationHelper.processOrder(responseDocument, order);
                
                // Add error popup messages.
                OrderLoader orderLoader = new OrderLoader();
                HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 
                
                // Return to cart
                File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                
            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            try
            {
                String orderGuid = request.getParameter("order_guid");
                String itemNumber = request.getParameter("item_number");
                String cartFlag = request.getParameter("cart_flag");
                
                // Create the initial document
                Document responseDocument = DOMUtil.getDocument();
        
                // Initialize data request
                dataRequest = DataRequestHelper.getInstance().getDataRequest();

                // Load complete order from dao
                ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
                OrderVO order = scrubMapperDAO.mapOrderFromDB(orderGuid);
              
                // Clone the order.  Use the clone for comparisons and replacement.
                OrderVO orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
  
                MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
                                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                   isAmazonOrder = true;
                
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;

                if(mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
                	isMercentOrder = true;
                } 
                
                if(!StringUtils.isEmpty(order.getOrderOrigin())) {
        			PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());
        			
        			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
        				isPartnerOrder = true;
        				order.setPartnerOrder(true);
        				order.setPartnerImgURL(partnerMappingVO.getPartnerImage());
        				order.setPartnerName(partnerMappingVO.getPartnerName());
        			}
                }
                
                // Update order with form values
                OrderBuilder orderBuilder = new OrderBuilder();
                orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());

                OrderDetailsVO item = null;

                if(cartFlag != null && cartFlag.equals("Y"))
                {
                    // Update all items status to pending
                    for(int i = 0; i < order.getOrderDetail().size(); i++)
                    {
                        item = (OrderDetailsVO) order.getOrderDetail().get(i);
                        item.setDispositions(this.loadDispositionList(request));
                        item.setStatus(String.valueOf(OrderStatus.PENDING_ITEM)); 
                    }
                }
                else
                {
                    // Update item status to pending
                    OrderHelper orderHelper = new OrderHelper();
                    item = orderHelper.getItem(order, itemNumber);
                    item.setDispositions(this.loadDispositionList(request));
                    item.setStatus(String.valueOf(OrderStatus.PENDING_ITEM)); 
                }
                
                // Validate order
                OrderHelper orderHelper = new OrderHelper();
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
    
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Update order status
                StatusManagerBO statusManager = new StatusManagerBO();
                boolean validHeader = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/header[@status='error']")) != null ? false : true;
                boolean itemsComplete = statusManager.updateOrderStatus(validHeader, order, false, request.getParameter("sc_mode"));

                // Final processing for order
                String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
                OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                DispatchHelper dispatchHelper = new DispatchHelper();

                // Retrieve a user transaction
                context = new InitialContext();
                String jndiTransactionEntry = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.JNDI_TRANSACTION_ENTRY);
                userTransaction = (UserTransaction)  context.lookup(jndiTransactionEntry);

                // Start the transaction
                userTransaction.begin();
                
                ServiceOrderActionBO serviceOrderActionBO = new ServiceOrderActionBO();
                              
                
                if(cartFlag != null && cartFlag.equals("Y"))
                {
                    //serviceOrderActionBO.reactivateServiceOrder(dataRequest.getConnection(), order, getCsrID(request));  
                    
                    // Save entire order to scrub and frp
                    orderSaver.saveOrder(order, null, true, request);


                    // Send order and all items to dispatcher
                    for(int i = 0; i < order.getOrderDetail().size(); i++)
                    {
                        item = (OrderDetailsVO) order.getOrderDetail().get(i);
                        if (dispatchHelper.isOrderAlreadyDispatched(item.getOrderDetailId(), dataRequest.getConnection()))
                        {
                          // if the order is already dispatched correctly, add an error to the response document
                          responseDocument = dispatchHelper.addOrderDoubleDispatchXML(responseDocument);
                          logger.debug("Scrub attempt to double dispatch: "+item.getOrderDetailId());
                        }
                        else
                        {
                          dispatchHelper.dispatchOrder(order, item, "ES1005", request, dataRequest.getConnection());
                        }
                    }
                }
                else
                {
                    //serviceOrderActionBO.reactivateServiceOrderItem(dataRequest.getConnection(), item, order.getBuyerEmailAddress() ,getCsrID(request));  
                    if(request.getParameter("sc_mode") != null 
                        && request.getParameter("sc_mode").equals("R")
                        && order.getPreviousStatus() != null
                        && order.getPreviousStatus().equals("1008"))
                    {
                        item.setReinstateFlag("Y");
                    }
                    if (dispatchHelper.isOrderAlreadyDispatched(item.getOrderDetailId(), dataRequest.getConnection()))
                    {
                      // if the order is already dispatched correctly, add an error to the response document
                      responseDocument = dispatchHelper.addOrderDoubleDispatchXML(responseDocument);
                      logger.debug("Scrub attempt to double dispatch: "+item.getOrderDetailId());
                    }
                    else
                    {                        
                      // Save order and item to scrub and frp
                      orderSaver.saveOrder(order, item, true, request);

                      // Send order and item to dispatcher
                      dispatchHelper.dispatchOrder(order, item, "ES1005", request, dataRequest.getConnection());
                    }
                }

                // Commit the transaction
                userTransaction.commit();

                // If order is complete so load next order
                if(itemsComplete)
                {                
                   
                    // Retrieve guid from search criteria
                    SearchManager searchManager = new SearchManager();
                    SearchResultVO searchResult = searchManager.retrieveResult(request, dataRequest, false);

                    if(searchResult != null) 
                    {
                        // Create the initial document
                        responseDocument = DOMUtil.getDocument();

                        //relese lock on buyer
                        LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);

                        // Load new order to scrub
                        OrderLoader orderLoader = new OrderLoader();
                        orderLoader.loadOrder(searchResult.getGuid(), orderBeforeUpdate, responseDocument, dataRequest, request);

                        File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

                    }
                    else 
                    {
                        logger.debug("** NO ORDERS FOUND TO BE SCRUBBED **");
                        
                        // Load search page
                        String securityParams = "?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);
                        String loadSearchURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/servlet/ScrubSearchServlet" + securityParams + "&sc_mode=" + request.getParameter("sc_mode");
                        response.sendRedirect(loadSearchURL);
                    }
                }
                else
                {
                    // Update minor items status for display
                    statusManager.updateMinorEveryItemStatus(order, responseDocument);
                
                    // Convert order to xml and append to response document
                    TransformationHelper transformationHelper = new TransformationHelper();
                    transformationHelper.processOrder(responseDocument, order);
                
                    // Add error popup messages.
                    OrderLoader orderLoader = new OrderLoader();
                    HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                    DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true); 
                    
                    // Load base cart data
                    BaseCartBuilder cartBuilder = new BaseCartBuilder();
                    DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                    // Return to order to continue scrub
                    File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                }
            }
            catch(Exception e)
            {
                try  
                {
                    if (userTransaction != null)  
                    {
                        // Rollback the user transaction
                        userTransaction.rollback();
                        logger.debug("User transaction rolled back");
                    }
                } 
                catch (Exception ex)  
                {
                    logger.error(ex);
                } 
                finally  
                {
                }
            
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }

                    if(context != null)
                    {
                        context.close();
                    }
                }
                catch(Exception se) 
                {
                    logger.error(se);
                }
            }
        }
    }

    private List loadDispositionList(HttpServletRequest request)
    {
        List dispositionList = new ArrayList();
        DispositionsVO disposition = new DispositionsVO();

        disposition.setDispositionID(request.getParameter("disposition"));
        disposition.setComments(request.getParameter("comments"));
        disposition.setSentEmailFlag(request.getParameter("email_flag"));
        disposition.setCalledCustomerFlag(request.getParameter("called_flag"));
        disposition.setStockMessageID(request.getParameter("message_id"));
        disposition.setEmailSubject(request.getParameter("email_subject"));
        disposition.setEmailMessage(request.getParameter("email_body"));
        
        dispositionList.add(disposition);

        return dispositionList;
    }

}

