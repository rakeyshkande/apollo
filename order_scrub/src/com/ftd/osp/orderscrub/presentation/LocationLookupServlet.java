package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

/**
 * This servlet is used to load the location list based on the inputted search 
 * criteria.
 *
 * @author Brian Munter
 */

public class LocationLookupServlet extends HttpServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.LocationLookupServlet";
    private final static String XSL_LOCATION_LOOKUP = ".../xsl/cityLookup.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Loads the data to display the location listing page based off of the user 
     * search criteria.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadLocationSearch(request, response); 
    }

    /**
     * Currently not used.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
        this.loadLocationSearch(request, response); 
    }

    private void loadLocationSearch(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        
        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load source code list from database
            dataRequest.reset();
            dataRequest.setStatementID("GET_ZIP_CODE_LIST");
            dataRequest.addInputParam("zip_code", request.getParameter("zipCodeInput"));
            dataRequest.addInputParam("city", request.getParameter("cityInput"));
            dataRequest.addInputParam("state", request.getParameter("stateInput"));
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            // Add page data to xml
            HashMap pageData = new HashMap();
            pageData.put("zipCodeInput", request.getParameter("zipCodeInput"));
            pageData.put("cityInput", request.getParameter("cityInput"));
            pageData.put("stateInput", request.getParameter("stateInput"));
            pageData.put("countryInput", request.getParameter("countryInput"));
            DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
            
            //Load states
            dataRequest.reset();
            dataRequest.setStatementID("STATE_LIST_LOOKUP");
            DOMUtil.addSection(responseDocument, ((Document) DataAccessUtil.getInstance().execute(dataRequest)).getChildNodes()); 

            File xslFile = new File(getServletContext().getRealPath(XSL_LOCATION_LOOKUP));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_LOCATION_LOOKUP, null);
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }
}