package com.ftd.osp.orderscrub.presentation;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.ftdutilities.SympathyControls;
import com.ftd.osp.orderscrub.business.ServiceOrderActionBO;
import com.ftd.osp.orderscrub.business.StatusManagerBO;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.presentation.util.BaseCartBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderBuilder;
import com.ftd.osp.orderscrub.presentation.util.OrderLoader;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.presentation.util.TransformationHelper;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.util.DispatchHelper;
import com.ftd.osp.orderscrub.util.LockUtil;
import com.ftd.osp.orderscrub.util.OrderHelper;
import com.ftd.osp.orderscrub.util.OrderSaver;
import com.ftd.osp.orderscrub.util.SearchManager;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.ordervalidator.util.ValidationXAO;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;


/**
 * This servlet is used to drive item level submit functionality for the Scrub
 * system.  It also contains functionality to manage changes associated with
 * updating delivery methods.
 *
 *
 * @author Brian Munter
 */

public class ItemServlet extends HttpServlet 
{
    private Logger logger;
    
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.presentation.ItemServlet";
    private final static String XSL_SHOPPING_CART = ".../xsl/shoppingCart.xsl";

    /**
     * Initializes servlet.
     * 
     * @param config ServletConfig
     * 
     * @exception ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        logger = new Logger(LOGGER_CATEGORY);
        super.init(config);
    }

    /**
     * Currently not used.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);
    }

    /**
     * Performs multiple pieces of functionality based on the type of action.<br/><br/>
     * 
     * 1. update_ship_method: This is action is called when the shipping method is 
     * updated on the item level.  The updated method requires revalidation and 
     * massaging due to the amount of dependencies on the ship method.<br/><br/>
     * 
     * 2. default: Default functionality which persists order data to the database 
     * and submits the item to validation.  If validation passes, the servlet 
     * will dispatch the item to the Order Dispatcher component to complete 
     * processing for this item.  If validation does not pass, the servlet will 
     * return to the current item with validation data.
     * 
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * 
     * @exception ServletException
     * @exception IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType(CONTENT_TYPE);

        if (SecurityHelper.isValidToken(request, response)) 
        {
            this.submitItem(request, response);          
        }
    }

    private void submitItem(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;
        InitialContext context = null;
        UserTransaction userTransaction = null;
        boolean isAmazonOrder = false;
        boolean isMercentOrder = false;
        boolean isPartnerOrder = false; 
        boolean isWalmartOrder = false;
        boolean hasMilitaryStarPayment = false;
        OrderHelper orderHelper = new OrderHelper();
        String errorPopupMessage = null;
        Document responseDocument = null;
        OrderVO order = null;
        OrderVO orderBeforeUpdate = null;
        boolean isFeedAllowedtoSend = false;
    
        try 
        {
            // Load order guid from scrub for processing
            String orderGuid = request.getParameter("order_guid");
            
            // Create the initial document
            responseDocument = DOMUtil.getDocument();
            
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            // Load complete order from dao
            ScrubMapperDAO scrubMapperDAO = new ScrubMapperDAO(dataRequest.getConnection());
            order = scrubMapperDAO.mapOrderFromDB(orderGuid);        
        
            // Clone the order.  Use the clone for comparisons and replacement.
            orderBeforeUpdate = (OrderVO)(new ObjectCopyUtil().deepCopy(order));
            // Build order
            OrderBuilder orderBuilder = new OrderBuilder();
            orderBuilder.updateOrderFromRequest(order, request, dataRequest.getConnection());                    
                                
            // 11/28/2006 by chu. Defect 2297,2548. Do not allow AAFES
            // orders with MS payment and ariba orders to have price increase.
            // 03/08/2007 tschmig Defect 3234 Removed Ariba restrictions
            hasMilitaryStarPayment = orderHelper.hasMilitaryStarPayment(order);
            
            // Check to see if has military star (MS) payment or if it's ariba order.
            if(hasMilitaryStarPayment)
            {                  
                new RecalculateOrderBO().recalculate(dataRequest.getConnection(), order);
                BigDecimal origTotal = new BigDecimal(orderBeforeUpdate.getOrderTotal());
                BigDecimal newTotal = new BigDecimal(order.getOrderTotal());
                
                if(newTotal.compareTo(origTotal) > 0) {
                    //restore original order
                    order = orderBeforeUpdate;
                    
                    errorPopupMessage = 
                        ConfigurationUtil.getInstance().getProperty(
                            CustomMessageConstants.CUSTOM_MESSAGE_FILE,
                            CustomMessageConstants.EDIT_AAFES_ORDER_AMOUNT_INCREASE);
                        
                    // Add error data to document
                    HashMap errorPopup = new HashMap();
                    if (errorPopupMessage != null) 
                    {
                        errorPopup.put("showErrorPopup", "Y");
                        errorPopup.put("errorPopupMessage", errorPopupMessage);
                    }//end if
                    DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true);
                                  
                } 
            }
        }    
        catch(Exception e) 
        {
            try 
            {
                logger.error(e);
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        MercentOrderPrefixes mercentOrderPrefixes = new MercentOrderPrefixes(dataRequest.getConnection());
        String partnerName = "";
        
        PartnerMappingVO partnerMappingVO = new PartnerUtility().getPartnerOriginsInfo(order.getOrderOrigin(),order.getSourceCode(), dataRequest.getConnection());
        
        if(partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
        	isPartnerOrder = true;
        	partnerName = order.getPartnerName();
        } else if (mercentOrderPrefixes.isMercentOrder(order.getOrderOrigin())) {
        	isMercentOrder = true;
        	partnerName = order.getMrcntChannelName();
        }
        
        if(isMercentOrder || isPartnerOrder) {
        	 BigDecimal origProductAmt = new BigDecimal(orderBeforeUpdate.getProductsTotal());
        	 BigDecimal newProductAmt = new BigDecimal(order.getProductsTotal());
        	 BigDecimal origAddonAmt = new BigDecimal(orderBeforeUpdate.getAddOnAmountTotal());
        	 BigDecimal newAddonAmt = new BigDecimal(order.getAddOnAmountTotal());
        	 BigDecimal origTaxTotal = new BigDecimal(orderBeforeUpdate.getTaxTotal());
        	 BigDecimal newTaxTotal = new BigDecimal(order.getTaxTotal());
        	 BigDecimal origTotal = new BigDecimal(orderBeforeUpdate.getOrderTotal());
             BigDecimal newTotal = new BigDecimal(order.getOrderTotal());
             
             if((origProductAmt.add(origAddonAmt)).compareTo(newProductAmt.add(newAddonAmt)) < 0){
            	//restore original order
                 order = orderBeforeUpdate;
                 errorPopupMessage = "This is an order from " + partnerName +". The Product and add-on amount cannot be increased from the original product and add-on amount. Please select another product and/or remove add-ons.";
             }
             else if(origTaxTotal.compareTo(newTaxTotal) < 0){
            	//restore original order
                 order = orderBeforeUpdate;
                 errorPopupMessage = "This is an order from " +  partnerName + ". The tax amount cannot be increased from the original tax amount.";
             }
             else if(newTotal.compareTo(origTotal) > 0) {
                 //restore original order
                 order = orderBeforeUpdate;
                 errorPopupMessage = "This is an order from " +  partnerName + ". The order total cannot be increased from the original total. Please select another product and/or remove add-ons.";
             }
        }
        
        String action = request.getParameter("servlet_action");
               
        // Update order status to remove scrub lock if next order
        if(action != null && action.equals("update_ship_method"))
        {
        
            try
            {                
                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                  isAmazonOrder = true;
                
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;
                
                // Validate order
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
                   
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Load data for display
                HashMap pageData = new HashMap();
                pageData.put("submit_item_flag", "Y");
                pageData.put("item_number", request.getParameter("item_number"));
                pageData.put("_content", request.getParameter("_content"));
                pageData.put("_focus_object", request.getParameter("_focus_object"));
                // Add page data to xml
                DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);  
                
                // Add error popup messages.
                OrderLoader orderLoader = new OrderLoader();
                HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                if (errorPopupMessage != null) 
                {
                    errorPopup.put("showErrorPopup", "Y");
                    errorPopup.put("errorPopupMessage", errorPopupMessage);
                }
                DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true);                
                
                // Update minor items status for display
                StatusManagerBO statusManager = new StatusManagerBO();
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // Convert order to xml and append to response document
                TransformationHelper transformationHelper = new TransformationHelper();
                transformationHelper.processOrder(responseDocument, order);
            
                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));
                //Add sympathy related alerts
                SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                if(sympathyAlerts!=null){
               	 DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
               } 

                File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);

            }
            catch(Exception e)
            {
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }
                }
                catch(SQLException se) 
                {
                    logger.error(se);
                }
            }
        }
        else
        {
            
            try
            {
                // Retrieve item
                String itemNumber = request.getParameter("item_number");

                // Check to see if Amazon Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.AMAZON_ORIGIN_INTERNET)))
                  isAmazonOrder = true;
                  
                // Check to see if Walmart Order
                if(order.getOrderOrigin().equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(
                   ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.WALMART_ORIGIN_INTERNET)))
                   isWalmartOrder = true;
                
                // Validate order
                OrderValidationNode orderValidationNode = orderHelper.validateOrder(order, dataRequest.getConnection(), 
                        isAmazonOrder, isPartnerOrder, isMercentOrder, RosesDotComUtil.isRosesInternetOrder(order.getOrderOrigin()));
                                
                // Convert validation to xml and append to response document
                ValidationXAO validationXAO = new ValidationXAO();
                DOMUtil.addSection(responseDocument, validationXAO.generateValidationXML(orderValidationNode).getChildNodes());

                // Update item status
                OrderDetailsVO item = orderHelper.getItem(order, itemNumber);
            
                // Update item status
                StatusManagerBO statusManager = new StatusManagerBO();
                boolean validHeader = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/header[@status='error']")) != null ? false : true;
                boolean validItem = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/items/item[@line_number=" + itemNumber + " and @status='error']")) != null ? false : true;
                statusManager.updateItemStatus(validHeader, validItem, order, item, request.getParameter("sc_mode"));

                // Load base cart data
                BaseCartBuilder cartBuilder = new BaseCartBuilder();
                DOMUtil.addSection(responseDocument, cartBuilder.loadGenericCartData(order, dataRequest, request, true));

                // Save order to scrub and frp
                OrderSaver orderSaver = new OrderSaver(dataRequest.getConnection());
                DispatchHelper dispatchHelper = new DispatchHelper();

                // Retrieve a user transaction
                context = new InitialContext();
                String jndiTransactionEntry = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.JNDI_TRANSACTION_ENTRY);
                userTransaction = (UserTransaction)  context.lookup(jndiTransactionEntry);

                // Start the transaction
                userTransaction.begin();
                
                ServiceOrderActionBO serviceOrderActionBO = new ServiceOrderActionBO();
                String csrId = getCsrID(request);
                
                
                if (dispatchHelper.isOrderAlreadyDispatched(item.getOrderDetailId(), dataRequest.getConnection()))
                {
                  // if the order is already dispatched correctly, add an error to the response document
                  responseDocument = dispatchHelper.addOrderDoubleDispatchXML(responseDocument);
                  logger.debug("Scrub attempt to double dispatch: "+item.getOrderDetailId());
                }
                else
                {            
                  if(item.getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM)))
                  {
                      if(request.getParameter("sc_mode") != null 
                          && request.getParameter("sc_mode").equals("R")
                          && order.getPreviousStatus() != null
                          && order.getPreviousStatus().equals("1008"))
                      {
                          item.setReinstateFlag("Y");
                          serviceOrderActionBO.submitServiceOrderItem(dataRequest.getConnection(), item, order.getBuyerEmailAddress(), csrId);
                      }
                  
                      // Submission save order to scrub and frp
                      order.setResetCardinal(true);
                      orderSaver.saveOrder(order, item, true, request);
  
                         
                      // Send order and item to dispatcher
                      dispatchHelper.dispatchOrder(order, item, "ES1005", request, dataRequest.getConnection());
                      
                      //Defect 12761 - Amazon; Use Case # 24156
                      //if CSR makes changes in Scrub that decrease the total order price
                      //insert Adjustment record for Amazon Order with cancel reason code of 'Z2'
                      BigDecimal origExternalOrderTotal = null;
             	     BigDecimal externalOrderTotal = null;
             		 
             	     
             	     String strValue;
             	     strValue=item.getOrigExternalOrderTotal();
             	     if(strValue==null)
             	     {
             	         strValue="0.00";
             	     }
             	     origExternalOrderTotal = new BigDecimal(strValue);
             	     logger.info("origExternalOrderTotal is "+strValue);
             	     
             	     strValue=item.getExternalOrderTotal();
             	     if(strValue==null)
             	     {
             	         strValue="0.00";
             	     }
             	     externalOrderTotal = new BigDecimal(strValue);
             	     logger.info("externalOrderTotal is "+strValue);
             	     
                      logger.info("before amazon or mercent check");
                      if(isAmazonOrder || isMercentOrder || isPartnerOrder)
                      {
                    	  logger.info("is amazon or mercent or partner order");
                    	  try{
                      		
                      	     
                      	    if(externalOrderTotal.compareTo(origExternalOrderTotal) < 0)
                      		{
                      	    	logger.info("calling partnerUtility");                      	    	
                      	    	if(isAmazonOrder) {
                      	    		this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.AMAZON_ORIGIN, false);
                      	    	} else if (isMercentOrder) {
                      	    		logger.debug("partnerUtility.createMercentAdjustmentFeedRecord");
                      	    		this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.MRCNT_ORD_INDICATOR, false);
								} else if (isPartnerOrder && partnerMappingVO.isSendAdjustmentFeed()) {
									this.createAdjustmentFeedRecord(item, order.getCsrId(), dataRequest, ValidationConstants.PTN_ORD_INDICATOR, false);
								}
                      	    	logger.info("after calling partnerUtility");
                      		}
                            } 
                            catch (Exception e) 
                            { 
                              logger.error("exception: " + e.toString());
                              throw new Exception("createOrderAdjustment caught: " + e);  
                            }    
                      }

                  		if (partnerMappingVO != null && partnerMappingVO.isSendOrdStatusUpdFeed()) {
                  			Integer priceDifference = origExternalOrderTotal.compareTo(externalOrderTotal);
                  			logger.debug("priceDifference:" + priceDifference + "isSendOrdStatus:" + partnerMappingVO.isSendOrdStatusUpdFeed() 
                  					+ "orderNumber:" + item.getExternalOrderNumber());
                  			if (priceDifference != 0 && partnerMappingVO.isSendOrdStatusUpdFeed()) {
                  				new OrderLoader().sendPartnerOSU(item.getExternalOrderNumber(),"SCRUB_ADJUSTED");
							}
                  		} 

                  }
                  else
                  {
                      // Standard save order to scrub and frp
                	  order.setResetCardinal(true);
                      orderSaver.saveOrder(order, item, false, request);
              
                      // Mark item still invalid
                      HashMap pageData = new HashMap();
                      pageData.put("submit_item_flag", "Y");
                      pageData.put("item_number", itemNumber);
                      pageData.put("_content", request.getParameter("_content"));
                      pageData.put("_focus_object", request.getParameter("_focus_object"));
                      DOMUtil.addSection(responseDocument, "pageData", "data", pageData, true);
                  }
                }

                // Commit the transaction
                userTransaction.commit();
                
               
                // Update minor items status for display
                statusManager.updateMinorEveryItemStatus(order, responseDocument);

                // If order is complete so load next order
                if(order.getStatus().equals(String.valueOf(OrderStatus.VALID)))
                {
                	 
                    // Retrieve guid from search criteria
                    SearchManager searchManager = new SearchManager();
                    SearchResultVO searchResult = searchManager.retrieveResult(request, dataRequest, false);

                    //Locking was added for PhaseIII, emueller 4/19/05                               
                    //release buyer record lock
                    LockUtil.releaseBuyerLock(dataRequest.getConnection(),order,request);

                    if(searchResult != null) 
                    {
                        // Create the initial document
                        responseDocument = DOMUtil.getDocument();

                        // Load new order to scrub
                        OrderLoader orderLoader = new OrderLoader();
                        orderLoader.loadOrder(searchResult.getGuid(), orderBeforeUpdate, responseDocument, dataRequest, request);

                        File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                        TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                    }
                    else 
                    {
                        logger.debug("** NO ORDERS FOUND TO BE SCRUBBED **");

                        // Load search page
                        String securityParams = "?securitytoken=" + request.getParameter("securitytoken") + "&context=" + request.getParameter("context") + "&applicationcontext=" + ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.SECURITY_APP_CONTEXT);
                        String loadSearchURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/servlet/ScrubSearchServlet" + securityParams + "&sc_mode=" + request.getParameter("sc_mode");
                        response.sendRedirect(loadSearchURL);
                    }
                }
                else
                {
                    // Convert order to xml and append to response document
                    TransformationHelper transformationHelper = new TransformationHelper();
                    transformationHelper.processOrder(responseDocument, order);
                    
                    // Add error popup messages.
                    OrderLoader orderLoader = new OrderLoader();
                    HashMap errorPopup = orderLoader.addErrorPopupMessages(request, order, orderBeforeUpdate, dataRequest);
                    if (errorPopupMessage != null) 
                    {
                        errorPopup.put("showErrorPopup", "Y");
                        errorPopup.put("errorPopupMessage", errorPopupMessage);
                    }
                    DOMUtil.addSection(responseDocument, "errorPopup", "data", errorPopup, true);
                    //Add sympathy related alerts
                    SympathyControls sympathyControls = new SympathyControls(dataRequest.getConnection());
                    HashMap sympathyAlerts = sympathyControls.addCartLevelSympathyAlerts(order);
                    if(sympathyAlerts!=null){
                    	DOMUtil.addSection(responseDocument, "sympathyAlertsItem", "ItemMessages", sympathyAlerts, true);
                    }
                    
                    // Return to order to continue scrub
                    File xslFile = new File(getServletContext().getRealPath(XSL_SHOPPING_CART));
                    TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, XSL_SHOPPING_CART, null);
                }
            }
            catch(Exception e)
            {
                try  
                {
                    if (userTransaction != null)  
                    {
                        // Rollback the user transaction
                        userTransaction.rollback();
                        logger.debug("User transaction rolled back");
                    }
                } 
                catch (Exception ex)  
                {
                    logger.error(ex);
                } 
                finally  
                {
                }
                
                try 
                {
                    logger.error(e);
                    request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
                }
                catch(Exception re) 
                {
                    logger.error(re);
                }
            }
            finally
            {
                try 
                {
                    if(dataRequest !=null && dataRequest.getConnection() != null)
                    {
                        dataRequest.getConnection().close();
                    }

                    if(context != null)
                    {
                        context.close();
                    }
                }
                catch(Exception se) 
                {
                    logger.error(se);
                }
            }
        }
    }


/**
   * Retrieves the CSR Id from the request
   * @param request
   * @return
   */
    private String getCsrID(HttpServletRequest request)
    {
        String csrId = "SYSTEM";
        
        try
        {
        String securityToken = request.getParameter("securitytoken");
        SecurityManager securityManager = SecurityManager.getInstance();
        UserInfo userInfo = securityManager.getUserInfo(securityToken);      
        csrId = userInfo.getUserID();      
        } catch (Exception e)
        {
          // NOP 
        }
        
        return csrId;
    }
    
    /**
     * Creates an adjustment feed record for a single item
     */
    private void createAdjustmentFeedRecord(OrderDetailsVO item, String csrID, DataRequest dataRequest, String partnerType, boolean fullAdjustment) throws Exception {
      
        String adjustmentReason = "Z2";      
        try {
            PartnerUtility partnerUtility = new PartnerUtility();
            if(ValidationConstants.AMAZON_ORIGIN.equals(partnerType)) {
            	partnerUtility.createAdjustmentFeedRecord(item, csrID, adjustmentReason, dataRequest.getConnection());
            } else if (ValidationConstants.MRCNT_ORD_INDICATOR.equals(partnerType)) {
            	partnerUtility.createMercentAdjustmentFeedRecord(item, csrID, adjustmentReason, fullAdjustment, dataRequest.getConnection());
            } else if (ValidationConstants.PTN_ORD_INDICATOR.equals(partnerType)) {
            	partnerUtility.createPartnerAdjustmentFeedRecord(item, csrID, adjustmentReason, fullAdjustment, dataRequest.getConnection());
            } 
          } catch (Exception e) { 
            logger.error("Error caught creating adjustment feed record: " + e.toString());
            throw new Exception("createOrderAdjustment caught: " + e);  
          }
    }
}

