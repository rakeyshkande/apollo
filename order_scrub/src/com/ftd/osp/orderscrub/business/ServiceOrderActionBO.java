package com.ftd.osp.orderscrub.business;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FTDCommonUtils;

import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.ProductVO;

import java.sql.Connection;

import java.util.List;


/**
 * Provides business methods for processing Service Orders
 */
public class ServiceOrderActionBO
{
  private static Logger logger = new Logger(ServiceOrderActionBO.class.getName());

 

  private static final String PROGRAM_STATUS_ACTIVE = "A";
  private static final String PROGRAM_STATUS_INACTIVE = "I";
  private static final String PRODUCT_TYPE_SERVICES = "SERVICES";
  private static final String PRODUCT_SUB_TYPE_SERVICES = "FREESHIP";

 

  /**
   * If the Submitted Item Contains a Service Program Membership, ensure the Buyer�s Service  Program is Active.
   *    If the program is inactive, activate it, and send the Feed to the Website.
   * If the Original Order contained a Service Program Membership, but the Submitted Item is not a service. (Item edited, removed)
   *    If the Buyer�s Service  Program is active, inactivate it, and send the Feed to the Website.
   * @param connection
   * @param submittedOrder
   * @param submittedItem
   * @param userId
   */
  public void submitServiceOrderItem(Connection connection, OrderDetailsVO submittedItem, String emailAddress, String userId)
  {
    logger.debug("Submitting Order Item: " + submittedItem.getExternalOrderNumber());

    if (!submittedItem.getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM)))
    {
      // Item stays in its current status
      logger.debug("Item status is not valid, no change to status: " + submittedItem.getStatus());
      return;
    }

    // Item has passed Validation. Is it a service Order ?
    String serviceProgramType = getServiceProgramType(connection, submittedItem.getProductId());
    String payLoad;
    if (serviceProgramType != null)
    {
      logger.debug("Ensuring Service is Active: " + serviceProgramType);

      
      payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+submittedItem.getExternalOrderNumber()+"</externalOrderNumber>"
  		+"<emailAddress>"+emailAddress+"</emailAddress>"
  		+"<membershipStatus>"+PROGRAM_STATUS_ACTIVE+"</membershipStatus>"
  		+ "</fsMembershipVO></accountTask>";
      try {
		FTDCommonUtils.insertJMSMessage(connection, payLoad, "ojms.ACCOUNT", "", 3);
      } catch (Exception e) {
		logger.error("Error occured while sending ht emessage to the jms queue");
		
      }

    }
    else
    {
      // Was this previously a 'Service' item, where the product type was changed?
      String originalProductId = submittedItem.getOrigProductId();
      String previousProgramType = getServiceProgramType(connection, originalProductId);

      if (previousProgramType != null)
      {
        logger.debug("Service Product was Edited to Non Service Product, Inactivating the Service: " + previousProgramType);
        // Ensure the Service is inactivated
     
        payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+submittedItem.getExternalOrderNumber()+"</externalOrderNumber>"
  		+"<emailAddress>"+emailAddress+"</emailAddress>"
  		+"<membershipStatus>"+PROGRAM_STATUS_INACTIVE+"</membershipStatus>"
  		+ "</fsMembershipVO></accountTask>";
      try {
		FTDCommonUtils.insertJMSMessage(connection, payLoad, "ojms.ACCOUNT", "", 3);
      } catch (Exception e) {
		logger.error("Error occured while sending ht emessage to the jms queue");
		
      }
      }
    }
  }

  /**
   * Handles the scenario where the Product has changed. Currently, this handles inactivating the service program
   * since for Free Shipping, we can select a non free shipping product, but cannot select an alternate free shipping
   * product.
   * @param connection
   * @param editedItem
   * @param userId
   */
  public void editProductTypeForServiceOrderItem(Connection connection, OrderDetailsVO editedItem, String emailAddress, String userId)
  {
      logger.debug("Edited Order Item: " + editedItem.getExternalOrderNumber());
      
      String productId = editedItem.getProductId();
      String originalProductId = editedItem.getOrigProductId();
      
      if(productId.equals(originalProductId)) 
      {
        return; // No Change
      }
      
      // Was this previously a 'Service' item, where the product type was changed?      
      String previousProgramType = getServiceProgramType(connection, originalProductId);
      String payLoad;
      if (previousProgramType != null)
      {
        logger.debug("Service Product was Edited to Non Service Product, Inactivating the Service: " + previousProgramType);
      
        if(PRODUCT_TYPE_SERVICES.equalsIgnoreCase(editedItem.getProductType()) && PRODUCT_SUB_TYPE_SERVICES.equalsIgnoreCase(editedItem.getProductSubType()))
        {
        	 payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+editedItem.getExternalOrderNumber()+"</externalOrderNumber>"
       		+"<emailAddress>"+emailAddress+"</emailAddress>"
       		+"<membershipStatus>"+PROGRAM_STATUS_ACTIVE+"</membershipStatus>"
       		+ "</fsMembershipVO></accountTask>";
        }
        else
        {
        	payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+editedItem.getExternalOrderNumber()+"</externalOrderNumber>"
       		+"<emailAddress>"+emailAddress+"</emailAddress>"
       		+"<membershipStatus>"+PROGRAM_STATUS_INACTIVE+"</membershipStatus>"
       		+ "</fsMembershipVO></accountTask>";
        }
       
      try {
		FTDCommonUtils.insertJMSMessage(connection, payLoad, "ojms.ACCOUNT", "", 3);
      } catch (Exception e) {
		logger.error("Error occured while sending ht emessage to the jms queue");
		
      }
    
      }
  }
  
  /**
   * If the Buyer has an Inactive Service Program tied to the Order, reactivate the Service  Program,
   * and send the Feed to the Website
   * @param connection
   * @param orderVO
   * @param userId
   */
  public void reactivateServiceOrder(Connection connection, OrderVO orderVO, String userId)
  {
    logger.debug("reactivateServiceOrder");
    updateServiceOrderStatus(connection, orderVO, true, userId);
  }

  /**
   * If the Buyer has an Inactive Service Program tied to the Order, reactivate the Service  Program,
   * and send the Feed to the Website
   * @param connection
   * @param orderDetailsVO
   * @param userId
   */
  public void reactivateServiceOrderItem(Connection connection, OrderDetailsVO orderDetailsVO, String emailAddress, String userId)
  {
    logger.debug("reactivateServiceOrder");
    updateServiceOrderItemStatus(connection, orderDetailsVO, true, emailAddress, userId);
  }

  /**
   * If the Buyer has an Active Service Program tied to the order, inactivate the Service  Program, and
   * send the Feed to the Website.
   * @param connection
   * @param orderVO
   * @param userId
   */
  public void removeServiceOrder(Connection connection, OrderVO orderVO, String userId)
  {
    logger.debug("removeServiceOrder");
    updateServiceOrderStatus(connection, orderVO, false, userId);
  }
  
  /**
   * This method is used to loop through all the items in the order and send cancel order status update to CAMS via JMS
   * @param connection
   * @param orderVO
   */
  	public void cancelOrder(Connection connection, OrderVO orderVO){
	  	logger.debug("CancelServiceOrder");
	  	List<OrderDetailsVO> orderDetailsList = orderVO.getOrderDetail();
		if (orderDetailsList == null)
		{
		  return;
		}
		
		for (OrderDetailsVO orderDetailsVO: orderDetailsList)
		{
			cancelOrderItem(connection, orderDetailsVO);
		}
  	}

  /**
   * If the Buyer has an Active Service Program tied to the order, inactivate the Service  Program, and
   * send the Feed to the Website.
   * @param connection
   * @param orderDetailsVO The item being removed
   * @param userId
   */
  public void removeServiceOrderItem(Connection connection, OrderDetailsVO orderDetailsVO, String emailAddress, String userId)
  {
    logger.debug("removeServiceOrderItem");
    updateServiceOrderItemStatus(connection, orderDetailsVO, false, emailAddress, userId);
  }
  
  /**
   * This method is used to send cancel order status update to cams via jms
   * @param connection
   * @param orderDetailsVO
   */
  public void cancelOrderItem(Connection connection, OrderDetailsVO orderDetailsVO)
  {
	  String payLoad = "<accountTask type=\"CANCEL_ORDER_UPDATE\"><fsMembershipVO><externalOrderNumber>"+orderDetailsVO.getExternalOrderNumber()+"</externalOrderNumber>"
  	  + "</fsMembershipVO></accountTask>";
  try {
		FTDCommonUtils.insertJMSMessage(connection, payLoad, "ojms.ACCOUNT", "", 3);
	} catch (Exception e) {
		logger.error("Error occured while sending ht emessage to the jms queue", e);
		
	}
  }

  /**
   * Updates the status of the program for service orders to active or inactive
   * @param connection
   * @param orderVO
   * @param active  If <code>true</code> Sets the Service Status to 'A' else, sets it to 'I'
   * @param userId
   */
  private void updateServiceOrderStatus(Connection connection, OrderVO orderVO, boolean active, String userId)
  {
    List<OrderDetailsVO> orderDetailsList = orderVO.getOrderDetail();
    if (orderDetailsList == null)
    {
      return;
    }

    for (OrderDetailsVO orderDetailsVO: orderDetailsList)
    {
      updateServiceOrderItemStatus(connection, orderDetailsVO, active, orderVO.getBuyerEmailAddress(), userId);
    }
  }

  /**
   * Updates the status of the program for service orders to active or inactive
   * @param connection
   * @param orderVO
   * @param active  If <code>true</code> Sets the Service Status to 'A' else, sets it to 'I'
   * @param userId
   */
  private void updateServiceOrderItemStatus(Connection connection, OrderDetailsVO orderDetailsVO, boolean active, String emailAddress,
                                            String userId)
  {
	String payLoad;
    String status = active? PROGRAM_STATUS_ACTIVE: PROGRAM_STATUS_INACTIVE;
    String serviceProgramType = getServiceProgramType(connection, orderDetailsVO.getProductId());
    if (serviceProgramType == null)
    {
      return;
    }

    if (logger.isDebugEnabled())
    {
      logger.debug("Updating Program Item Status for Order: " + orderDetailsVO.getExternalOrderNumber() + " to: " + status);
    }
    
    payLoad = "<accountTask type=\"FS_MEMBERSHIP_STATUS_UPDATE\"><fsMembershipVO><externalOrderNumber>"+orderDetailsVO.getExternalOrderNumber()+"</externalOrderNumber>"
    	+"<emailAddress>"+emailAddress+"</emailAddress>"
        +"<membershipStatus>"+status+"</membershipStatus>"
        + "</fsMembershipVO></accountTask>";
    try {
		FTDCommonUtils.insertJMSMessage(connection, payLoad, "ojms.ACCOUNT", "", 3);
	} catch (Exception e) {
		logger.error("Error occured while sending ht emessage to the jms queue");
		
	}
  }


  /**
   * Returns the Program Type/Name of the ProductId. If the product
   * is not a service, returns null.
   *
   * @param productId
   * @return
   */
  private String getServiceProgramType(Connection connection, String productId)
  {
    String retVal = null;

    if (productId == null || productId.trim().length() == 0)
    {
      return null;
    }

    try
    {
      DeliveryDateUTIL ddutil = new DeliveryDateUTIL();
      ProductVO productVO = ddutil.retrieveProductInformation(connection, productId);

      if (PRODUCT_TYPE_SERVICES.equalsIgnoreCase(productVO.getProductType()))
      {
        retVal = productVO.getProductSubType();
      }
    }
    catch (Exception e)
    {
      logger.error("Failed to retrieve Product Information: " + e.getMessage(), e);
      throw new RuntimeException("Failed to retrieve Product Information: " + e.getMessage(), e);
    }

    return retVal;
  }

}
