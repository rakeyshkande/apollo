/**

 * 
 */
package com.ftd.osp.orderscrub.business;

import java.net.URL;
import java.sql.Connection;

import com.ftd.osp.orderscrub.constants.GDAuthClearConstants;
import com.ftd.osp.orderscrub.util.SystemMessager;
import com.ftd.osp.orderscrub.vo.GDAuthClearRequestVO;
import com.ftd.osp.orderscrub.vo.GDAuthClearResponseVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.cacheMgr.exception.CacheException;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.ps.webservice.AmountVO;
import com.ftd.ps.webservice.CardVO;
import com.ftd.ps.webservice.PaymentRequest;
import com.ftd.ps.webservice.PaymentResponse;
import com.ftd.ps.webservice.PaymentServiceClient;
import com.ftd.ps.webservice.PaymentServiceClientSVSImplService;

/**
 * @author smeka
 * 
 */
public class GDAuthClearBO {
	private GDAuthClearRequestVO gdReqVo;
	private GDAuthClearResponseVO gdRespVo;
	private Connection conn;
	private PaymentServiceClient psc;


	private static Logger logger = new Logger("com.ftd.osp.orderscrub.business.GDAuthClearBO");
	
	/** Initialize GDAuthClear
	 * @param conn
	 */
	public GDAuthClearBO(GDAuthClearRequestVO gdReqVo, Connection conn) {
		this.gdReqVo = gdReqVo;
		this.conn = conn;
	}
	
	public GDAuthClearBO() {
		super();
	}

	/**
	 * Performs Auth clear. 
	 * retry is set to false before calling the cancelAuthorization.
	 * If there is any error in the request, identified by PS, retry is set to true, to fix the error and retry. 
	 */
	public void perform() {
		printDebugMessage("*** Entering Perform ***");
		StringBuffer systemErrMsg = new StringBuffer();
		try {
			int currentCount = 0;
			configurePS();

			while(gdReqVo.isPerform() && currentCount++ < gdReqVo.getRetryCount()) {
				if(currentCount > 1) {
					printDebugMessage("Waiting for seconds : " + gdReqVo.getDelayTime());
					Thread.sleep(gdReqVo.getDelayTime() * 1000);
				}
				logger.info("Performing auth clear, attempt : " + currentCount);
				processCancelAuth();
			}
			
			if(currentCount == gdReqVo.getRetryCount() && !GDAuthClearConstants.GD_AUTH_CLEAR_SUCCESS.equals(gdReqVo.getStatus())) {
				logger.error("Svs transaction failed even after : " + currentCount + " attempts, send system message");
				gdReqVo.setSendSysMessage(true);
			} 
			if(gdReqVo.isSendSysMessage()) {
				systemErrMsg.append(GDAuthClearConstants.PS_AUTH_CLEAR_FAILED);
				systemErrMsg.append(" OrderGUID: " + gdReqVo.getOrderGuid());
				systemErrMsg.append(", transactionId: " + gdReqVo.getCaptureTxt());
				if(gdRespVo.getErrorCode() != null) {
					systemErrMsg.append(", PaymentService Error Code: " + gdRespVo.getErrorCode());
					systemErrMsg.append(", PaymentService Error Message: " + gdRespVo.getErrorMessage());
				}
				if(gdRespVo.getResponseCode() != null) {
					systemErrMsg.append(", SVS Response Code: " + gdRespVo.getResponseCode());
					systemErrMsg.append(", SVS Response Message: " + gdRespVo.getResponseMessage());
				}			
			}
		} catch(Throwable t) {
			logger.error("Error caught performing cancelAuthorization : " + t + ". Send system message");	
			gdReqVo.setSendSysMessage(true);
			systemErrMsg.append(GDAuthClearConstants.APOLLO_AUTH_CLEAR_FAILED +  t.getMessage());
		} finally {			
			
			if(gdReqVo.isSendSysMessage()) {
				try{
					logger.info("Sending system message --" + systemErrMsg);
					sendSystemMessage(new Exception(systemErrMsg.toString()), false);
				} catch(Throwable t) {
					logger.error("Error caught sending system message");
					logger.error(t);
				}
			}
			printDebugMessage("*** Exiting Perform ***");
		}
	}
	
	/**
	 *  gets the configuration details for the payment service.
	 *  Any exception caught while getting the configuration details, will not process the cancelAuth request.
	 */
	private void configurePS() throws Exception {	
		printDebugMessage("*** Entering configurePS ***");
		
		ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
		gdReqVo.setClientId(configUtil.getSecureProperty(
				GDAuthClearConstants.SVS_SERVICE_CONTEXT, GDAuthClearConstants.CLIENT_ID_PROP_NAME));
		
		gdReqVo.setClientPassword(configUtil.getSecureProperty(
				GDAuthClearConstants.SVS_SERVICE_CONTEXT, GDAuthClearConstants.CLIENT_PWD_PROP_NAME));
		
		gdReqVo.setServiceUrl(configUtil
				.getFrpGlobalParm(GDAuthClearConstants.SVS_SERVICE_CONTEXT, GDAuthClearConstants.PS_SERVICE_URL));
		
		gdReqVo.setRetryCount(Integer.parseInt(configUtil.getFrpGlobalParm(
				GDAuthClearConstants.ACCOUNTING_CONFIG_CONTEXT, GDAuthClearConstants.GD_RETRY_PROP_NAME)));
		
		gdReqVo.setDelayTime(Long.parseLong(configUtil.getFrpGlobalParm(
				GDAuthClearConstants.ACCOUNTING_CONFIG_CONTEXT, GDAuthClearConstants.GD_DELAY_TIME_PROP_NAME)));
		
		printDebugMessage("*** Exiting configurePS ***");		
	}
	
	/** Performs following - Creates cancelAuth payment request
	 * loads the service provider
	 * performs cancelAuth
	 * if an exception occurs, retries canceling the GD Auth for retryCount times.
	 * @param paymentVO
	 */
	private void processCancelAuth() throws Exception {					
		printDebugMessage("*** Entering processCancelAuth ***");
		gdReqVo.setPerform(false);
		// Create the payment service request object
		PaymentRequest paymentRequest = createCancelAuthRequest();
		
		// Get handle to cancel authorization web service
		getPaymentServiceClient();
		
		// perform cancel
		cancel(paymentRequest);	
		
		printDebugMessage("*** Exiting processCancelAuth ***");
	}
	
	/** Populates the mandatory fields into paymentRequest object
	 *  
	 * @param paymentVO
	 * @param configUtil
	 */
	private PaymentRequest createCancelAuthRequest() {
		printDebugMessage("*** Entering createCancelAuthRequest ***");
		 PaymentRequest reqVo = new PaymentRequest();
		 
		 reqVo.setClientUserName(gdReqVo.getClientId());
		 reqVo.setClientPassword(gdReqVo.getClientPassword());
		 
		 // getAuthorization/preAuth performed by Web site will generate the auth_number/transactionId.
		 printDebugMessage("Auth Number " + gdReqVo.getAuthNumber() + " ap_auth_txt : " + gdReqVo.getCaptureTxt());
		 reqVo.setTransactionId(gdReqVo.getCaptureTxt());
		 
		 //Set card details - card number and pin
		 CardVO cardvo = new CardVO();
		 cardvo.setCardCurrency(GDAuthClearConstants.PAYSVC_CURRENCY);
		 cardvo.setCardNumber(gdReqVo.getAccountNumber());
		 cardvo.setPinNumber(gdReqVo.getPin());
		 reqVo.setCardVO(cardvo);
		 
		 // Set amount details - transaction amount is 0.00 for authorization clear
		 AmountVO amountvo = new AmountVO();
		 amountvo.setAmount(gdReqVo.getTransactionAmt().doubleValue());
		 amountvo.setCurrency(GDAuthClearConstants.PAYSVC_CURRENCY);
		 reqVo.setAmountVO(amountvo);
		 
		 printDebugMessage("*** Exiting createCancelAuthRequest ***");
		 return reqVo;		 
	}
	
	/**
	 * Loads the service provider
	 * 
	 * @return
	 * @throws CacheException
	 * @throws Exception
	 */
	private void getPaymentServiceClient() throws Exception {
		printDebugMessage("** getting PaymentServiceClient **");		
		if (psc == null) {
			URL psURL = new URL(gdReqVo.getServiceUrl());
			PaymentServiceClientSVSImplService ps = new PaymentServiceClientSVSImplService(psURL);
			psc = ps.getPaymentServiceClientSVSImplPort();
		} 
	}
	
	/**
	 * @param paymentRequest
	 * @throws Throwable
	 */
	private void cancel(PaymentRequest paymentRequest) throws Exception {
		printDebugMessage("** Performing CancelAuth **");
		
		PaymentResponse paymentResponse = psc.cancelAuthorization(paymentRequest);
		
		if (paymentResponse != null) {
			
			String respMsgs = "GiftCard cancelAuthorization for ---transaction id : " + paymentResponse.getTransactionId()
					+ "---Service error code/message: " + paymentResponse.getErrorCode() + "/" + paymentResponse.getErrorMessage() 
					+ " --- SVS response code/message: " + paymentResponse.getResponseCode() + "/" + paymentResponse.getResponseMessage();
		
			logger.info(respMsgs);
			
			getGDRespDetailVO(paymentResponse);
			
			if(paymentResponse.isIsSuccessful()) {
				logger.info("CancelAuth response : isSuccessful is true");
				CardVO cardVO = paymentResponse.getCardVO();
				if(cardVO == null) {
					logger.info("CancelAuth response doesnt have valid details. Payment service validation failed");
					printDebugMessage("Do not retry cancel auth, send a system message");
					gdReqVo.setSendSysMessage(true);					
				} else if ("01".equals(paymentResponse.getResponseCode())) {
					logger.info("****** GD Cancel Authorization performed succesfully for transaction ID : " + gdReqVo.getCaptureTxt());
					gdReqVo.setStatus(GDAuthClearConstants.GD_AUTH_CLEAR_SUCCESS);
					return;
				}
			} else {
				logger.info("CancelAuth response : isSuccessful is false");			
				if(paymentResponse.getErrorCode() != null) {
					logger.error("Payment Service Error : " + paymentResponse.getErrorMessage() + " and Error code is : " + paymentResponse.getErrorCode());
					printDebugMessage("Fix the error in request xml and retry");	
					gdReqVo.setPerform(true);
				} else if(paymentResponse.getResponseCode() != null) {
					logger.error("Payment Service processed request, SVS failed : " + paymentResponse.getResponseMessage() + " and response code is : " + paymentResponse.getResponseCode());
					printDebugMessage("Send a system message");	
					gdReqVo.setSendSysMessage(true);
				}
			}
		}
		printDebugMessage("** Exiting cancel **");
	}
	
	/** populate response details
	 * @param paymentResponse
	 */
	private void getGDRespDetailVO(PaymentResponse paymentResponse) {
		gdRespVo = new GDAuthClearResponseVO();
		gdRespVo.setErrorCode(paymentResponse.getErrorCode());
		gdRespVo.setErrorMessage(paymentResponse.getErrorMessage());
		gdRespVo.setResponseCode(paymentResponse.getResponseCode());
		gdRespVo.setResponseMessage(paymentResponse.getResponseMessage());
		gdRespVo.setSuccess(paymentResponse.isIsSuccessful());
	}
	
	/**
	 * @param message
	 */
	private void printDebugMessage(String message) {
		if(logger.isDebugEnabled()){
			logger.debug(message);
		}		
	}
	
	/**
	 * Send system message
	 * 
	 * @param t
	 * @param page
	 * @throws Throwable
	 */
	public void sendSystemMessage(Throwable t, boolean page) throws Throwable {
		try {
			logger.info("Sending system message for GD auth clear.");
			String source = "";
			String logMessage = t.getMessage();
			if (page) {
				source = GDAuthClearConstants.GD_AUTH_CLEAR_SOURCE;
			} else {
				source = GDAuthClearConstants.GD_NOPAGE_SOURCE;
			}
			SystemMessager.send(logMessage, t, source, SystemMessager.LEVEL_PRODUCTION,
					GDAuthClearConstants.GD_AUTH_CLEAR_ERROR_TYPE, GDAuthClearConstants.GD_AUTH_CLEAR_SUBJECT, conn);
		} catch (Throwable ex) {
			logger.error(ex);
		} finally {
			printDebugMessage("Exiting sendSystemMessage for GD auth clear");
		}
	}
}
