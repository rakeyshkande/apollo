package com.ftd.osp.orderscrub.business;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class ScrubFilterBO enforces the sort business logic for scrub orders.
 *
 * @author Tim Peterson
 *
 */
public class ScrubFilterBO  
{
    private boolean bTest;
    private Logger logger;
    public static final int DELIVERYDATEINCREMENT = 100;
    public static final String EMPTY_STRING = "";
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.business.ScrubFilterBO";
    
    /**
     * Contructor
     */
	public ScrubFilterBO()
    {
        bTest=false;
        logger = new Logger(LOGGER_CATEGORY);
    }

    /**
     * Changes the objects mode to test.  In this mode, the method
     * retrieveGuid will not update the status of the selected order
     * 
     * @param test Will the test mode be set.
     */
    public void setTest(boolean test)
    {
        this.bTest=test;
    }

    /**
     * Gets the next order to be scrubbed based on a list of available 
     * scrub orders.  If the variable bTest is set to false, order is 
     * marked as being scrubbed.
     * @param resultList A list of ScrubOrderVO that represents orders
     * that are in scrub, but, are not being processed.
     * @return The guid for the next order to be scrubbed.
     * @throws java.lang.Exception
     */
	public SearchResultVO retrieveGuid(Map resultMap, Set usedGuidSet, String securityToken) throws Exception
    {
    	SearchResultVO searchResult = null;
    	Object o = null;
        SearchResultVO svo = null;
        long lValue = 0;

        if(resultMap != null && resultMap.size() > 0)
        {
            if(usedGuidSet != null && usedGuidSet.size() > 0)
            {
                String usedGuid = null;
                Iterator usedGuidIterator = usedGuidSet.iterator();
                while(usedGuidIterator.hasNext())
                {
                    usedGuid = (String) usedGuidIterator.next();
                    if(resultMap.containsKey(usedGuid))
                    {
                        resultMap.remove(usedGuid);
                    }
                }
            }

            List resultList = new ArrayList(resultMap.values());

            if( resultList != null && resultList.size() > 0 )
            {
                HashMap hash = new HashMap(resultList.size());
                long[] ss = new long[resultList.size()];
	
                Iterator it = resultList.iterator();
	
                //Sort the objects
                //Create a long value for each object...lowest number has most significance
                int ctr = 0;
                while( it.hasNext() )
                {
                    o = it.next();
                    if( o == null || 
                        !o.getClass().getName().equals(SearchResultVO.class.getName()) )
                    {
                        continue;
                    }
	            
                    svo = (SearchResultVO) o;
                    lValue = 0;
                    Calendar date;
	          
                    if( svo.getShipMethod() == null ) //florist delivery
                    {
                        //RULE: use delivery date for florist delivery
                        date = svo.getDeliveryDate();
                    
                        //RULE: show drop ship before florist orders
                        lValue += DELIVERYDATEINCREMENT;
                    }
                    else
                    {
                        date = svo.getShipDate();
                    }
	
                    //Clear out the time element
                    date.set(Calendar.HOUR, 0);
                    date.set(Calendar.MINUTE, 0);
                    date.set(Calendar.SECOND, 0);
                    date.set(Calendar.MILLISECOND, 0);
                    lValue += date.getTime().getTime();
	
                    //RULE: order by timezone
                    lValue += svo.getTimeZone();

                    hash.put(new Long(lValue), svo);
                    ss[ctr++] = lValue;
                }
	
                //Sort the array of long values
                Arrays.sort(ss);

                if( bTest )
                {
                    searchResult = (SearchResultVO)hash.get(new Long(ss[0]) );
                }
                else
                {
                    // Try and grab the order and change it's status
                    String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
                    DataRequest dataRequest = null;  
                
                    try
                    {  
                        // Initialize data request
                        dataRequest = DataRequestHelper.getInstance().getDataRequest();

                         String userId = EMPTY_STRING;
                         
                        // COMMENTED OUT CAUSE WE DONT USE THIS CLASS ANYMORE.
                        // IF WE START USING THIS AGAIN, PLEASE UNCOMMENT AND 
                        // FIX TO PULL FROM SESSION

                        /*
                        if(standAloneFlag != null && standAloneFlag.equals("N"))
                        {
                            userId = SecurityHelper.getUserId(securityToken);
                        }
                        else
                        {
                            userId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.DEFAULT_CSR);
                        }
                        */
	        
                        for( int idx = 0; idx < ctr; idx++ )
                        {
                            svo = (SearchResultVO)hash.get(new Long(ss[idx]));

                            dataRequest.reset();
                            dataRequest.setStatementID("UPDATE_ORDER_TO_SCRUB_STATUS");
                            dataRequest.addInputParam("order_guid", svo.getGuid());
                            dataRequest.addInputParam("csr_id", userId);

                            HashMap statusResultMap = (HashMap) DataAccessUtil.getInstance().execute(dataRequest);

                            if(((String) statusResultMap.get("RegisterOutParameterStatus")).equals("Y"))
                            {
                                // Valid result found ready for processing
                                searchResult = svo;
                                break;
                            }
                            /* 
                            else if(resultList.size() == 1)
                            {
                                // Display who has the order currently locked if only one result is returned
                                searchResult = svo;
                                break;
                            }
                            */
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        try 
                        {
                            if(dataRequest !=null && dataRequest.getConnection() != null)
                            {
                                dataRequest.getConnection().close();
                            }
                        }
                        catch(SQLException se) 
                        {
                            logger.error(se);
                        }
                    }
                }   
            }
        }

        return searchResult;
    }

    /**
     * Sorts the results passed into the method according to the sort order 
     * specified in the business logic.  Does not perform any locking and is used 
     * primarily for the result list for reinstate and pending modes.
     * @param resultList A list of ScrubOrderVO that represents orders
     * that are in scrub, but, are not being processed.
     * @return The guid for the next order to be scrubbed.
     * @throws java.lang.Exception
     */
    public ArrayList sortResults(Map resultMap) throws Exception
    {
    	ArrayList sortedResults = null;
        SearchResultVO svo = null;
    	Object o = null;
        long lValue = 0;

        if(resultMap != null && resultMap.size() > 0)
        {
            List resultList = new ArrayList(resultMap.values());

            if( resultList != null && resultList.size() > 0 )
            {
                sortedResults = new ArrayList();
                long[] ss = new long[resultList.size()];
	
                Iterator it = resultList.iterator();
	
                //Sort the objects
                //Create a long value for each object...lowest number has most significance
                int ctr = 0;
                while( it.hasNext() )
                {
                    o = it.next();
                    if( o == null || 
                        !o.getClass().getName().equals(SearchResultVO.class.getName()) )
                    {
                        continue;
                    }
	            
                    svo = (SearchResultVO) o;
                    lValue = 0;
                    Calendar date;
	          
                    if( svo.getShipMethod() == null ) //florist delivery
                    {
                        //RULE: use delivery date for florist delivery
                        date = svo.getDeliveryDate();
                    
                        //RULE: show drop ship before florist orders
                        lValue += DELIVERYDATEINCREMENT;
                    }
                    else
                    {
                        date = svo.getShipDate();
                    }
	
                    //Clear out the time element
                    date.set(Calendar.HOUR, 0);
                    date.set(Calendar.MINUTE, 0);
                    date.set(Calendar.SECOND, 0);
                    date.set(Calendar.MILLISECOND, 0);
                    lValue += date.getTime().getTime();
	
                    //RULE: order by timezone
                    lValue += svo.getTimeZone();

                    svo.setSortOrder(new Long(lValue).toString());
                    sortedResults.add(svo);
                    ss[ctr++] = lValue;
                }
            }
        }

        return sortedResults;
    }

    public static void main(String[] args)
    {
        /*
         
        ScrubFilterBO filter = new ScrubFilterBO();
        filter.setTest(true);
        ArrayList list = new ArrayList(10);
        Calendar shipDate = Calendar.getInstance();
        Calendar deliveryDate = Calendar.getInstance();

        shipDate.setTime(new java.util.Date("11/05/2003"));
        deliveryDate.setTime(new java.util.Date("11/03/2003"));
        SearchResultVO svo = new SearchResultVO("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "novator", "NF",
            (Integer)null, 
            shipDate,
            deliveryDate);
        list.add(svo);

        shipDate = Calendar.getInstance();
        deliveryDate = Calendar.getInstance();
        shipDate.setTime(new java.util.Date("11/05/2003"));
        deliveryDate.setTime(new java.util.Date("11/04/2003"));
        svo = new SearchResultVO("BAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "novator", (String)null,
            (Integer)null, 
            shipDate,
            deliveryDate);
        list.add(svo);

        shipDate = Calendar.getInstance();
        deliveryDate = Calendar.getInstance();
        shipDate.setTime(new java.util.Date("11/07/2003"));
        deliveryDate.setTime(new java.util.Date("11/07/2003"));
        svo = new SearchResultVO("CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "novator", (String)null,
            1, 
            shipDate,
            deliveryDate);
        list.add(svo);

        shipDate = Calendar.getInstance();
        deliveryDate = Calendar.getInstance();
        shipDate.setTime(new java.util.Date("11/07/2003"));
        deliveryDate.setTime(new java.util.Date("11/07/2003"));
        svo = new SearchResultVO("DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "novator", (String)null,
            0, 
            shipDate,
            deliveryDate);
        list.add(svo);

        shipDate = Calendar.getInstance();
        deliveryDate = Calendar.getInstance();
        shipDate.setTime(new java.util.Date("11/01/2003"));
        deliveryDate.setTime(new java.util.Date("11/01/2003"));
        svo = new SearchResultVO("EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "novator", (String)null,
            (Integer)null, 
            shipDate,
            deliveryDate);
        list.add(svo);

        shipDate = Calendar.getInstance();
        deliveryDate = Calendar.getInstance();
        shipDate.setTime(new java.util.Date("11/01/2003"));
        deliveryDate.setTime(new java.util.Date("11/01/2003"));
        svo = new SearchResultVO("FAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
            "novator", "NF",
            (Integer)null, 
            shipDate,
            deliveryDate);
        list.add(svo);

        try
        {
            //String retval = filter.retrieveGuid(list, null);
            //System.out.println("The guid returned is: " + retval);
        }
        catch (Exception e)
        {
        }

        */
    }
}