package com.ftd.osp.orderscrub.business;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.OrderStatus;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.orderscrub.util.StatusSortUtil;

/**
 * This object is used to manage order and item status based on validation
 * flags generated from the validation component.  This object also updates 
 * the sort order of the status based on the update.
 *
 * @author Brian Munter
 */

public class StatusManagerBO 
{
    private Logger logger;
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.business.StatusManagerBO";

    /** 
     * Constructor
     */
    public StatusManagerBO()
    {
        logger = new Logger(LOGGER_CATEGORY);
    }

    /**
     * Updates the item status based on the method parameters.  It also returns 
     * a boolean flag which determines if all items in the current order have 
     * been processed.
     * 
     * @param validHeader boolean
     * @param validItem boolean
     * @param order OrderVO
     * @param item OrderDetailsVO
     * @param mode String
     * 
     * @exception Exception
     */
    public boolean updateItemStatus(boolean validHeader, boolean validItem, OrderVO order, OrderDetailsVO item, String mode) throws Exception
    { 
        boolean itemsComplete = true;
    
        if(validItem && !validHeader)
        {
            // Valid item with invalid header
            item.setStatus(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER));
        }
        else if(!validItem)
        {
            // Invalid item
            item.setStatus(String.valueOf(OrderStatus.INVALID_ITEM));
        }
        else
        {
            // Valid item
            item.setStatus(String.valueOf(OrderStatus.VALID_ITEM));
        }

        // Check if there are any error or unsubmitted items in the order
        for(int i = 0; i < order.getOrderDetail().size(); i++)
        {
            if(mode != null && mode.equals("P"))
            {
                // Pending order completion
                if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.PENDING_ITEM)))
                {
                    itemsComplete = false;
                }
            }
            else if(mode != null && mode.equals("R"))
            {
                // Removed order completion
                if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
                {
                    itemsComplete = false;
                }
            }
            else
            {
                // Scrub or default order completion
                if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER)))
                {
                    itemsComplete = false;
                }
            }
        }

        if(validHeader && itemsComplete)
        {
            // Valid header
            order.setStatus(String.valueOf(OrderStatus.VALID));
        }

        // Update sort order associated with updated status
        item.setStatusSortOrder((String) StatusSortUtil.getInstance().get(item.getStatus()));

        return itemsComplete;
    }

    /**
     * Updates the order status based on the method parameters.  It also returns 
     * a boolean flag which determines if all items in the current order have 
     * been processed.  This is the main method used for status updates when 
     * moving between orders and submitting from the cart level. 
     * 
     * @param validHeader boolean
     * @param order OrderVO
     * @param exitPoint boolean
     * @param mode String
     * 
     * @exception Exception
     */
    public boolean updateOrderStatus(boolean validHeader, OrderVO order, boolean exitPoint, String mode) throws Exception
    {
        boolean itemsComplete = true;
        boolean containsErrorItems = false;

        // Check if there are any error items in the order
        for(int i = 0; i < order.getOrderDetail().size(); i++)
        {
            if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM)))
            {
                containsErrorItems = true;
            }

            if(mode != null && mode.equals("P"))
            {
                // Pending order completion
                if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.PENDING_ITEM)))
                {
                    itemsComplete = false;
                }
            }
            else if(mode != null && mode.equals("R"))
            {
                // Removed order completion
                if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
                {
                    itemsComplete = false;
                }
            }
            else
            {
                // Scrub or default order completion
                if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM))
                    || ((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER)))
                {
                    itemsComplete = false;
                }
            }
        }
        
        if(exitPoint)
        {
            // Set order status to error to prevent valid on exit points
            if(validHeader && containsErrorItems)
            {
                order.setStatus(String.valueOf(OrderStatus.VALID_HEADER_INVALID_ITEM));
            }
            else
            {
                order.setStatus(String.valueOf(OrderStatus.INVALID_HEADER));
            }
        }
        else
        {
            if(validHeader && itemsComplete)
            {
                order.setStatus(String.valueOf(OrderStatus.VALID));
            }
        }

        if(!containsErrorItems)
        {
            String orginalStatus = order.getStatus();
        
            // Check if all are removed
            if(order.getStatus().equals(orginalStatus))
            {
                for(int i = 0; i < order.getOrderDetail().size(); i++)
                {
                    if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.REMOVED_ITEM)))
                    {
                        order.setStatus(String.valueOf(OrderStatus.REMOVED));
                    }
                    else
                    {
                        order.setStatus(orginalStatus);
                        break;
                    }
                }
            }
            
            // Check if all are pending
            if(order.getStatus().equals(orginalStatus))
            {
                for(int i = 0; i < order.getOrderDetail().size(); i++)
                {
                    if(((OrderDetailsVO) order.getOrderDetail().get(i)).getStatus().equals(String.valueOf(OrderStatus.PENDING_ITEM)))
                    {
                        order.setStatus(String.valueOf(OrderStatus.PENDING));
                    }
                    else
                    {
                        order.setStatus(orginalStatus);
                        break;
                    }
                }
            }
        }

        if(!itemsComplete && !exitPoint)
        {
            order.setStatus(String.valueOf(OrderStatus.SCRUB));
        }
        
        return itemsComplete;
    }

    /**
     *  This is used primarily for updating item statuses for display purposes 
     *  only.  It has no effect on the order and these status updates should occur 
     *  after database persistence.  An example of where this is used is source 
     *  code update.
     * 
     * @param order OrderVO
     * @param responseDocument XMLDocument
     * 
     * @exception Exception
     */
    public void updateMinorEveryItemStatus(OrderVO order, Document responseDocument) throws Exception
    { 
        
        boolean validItem;
        boolean warningItem;

        OrderDetailsVO item = null;
        for(int i = 0; i < order.getOrderDetail().size(); i++) 
        {
            // Update item status
            item = (OrderDetailsVO) order.getOrderDetail().get(i);
                    
            if(item.getStatus().equals(String.valueOf(OrderStatus.INVALID_ITEM)) || item.getStatus().equals(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER)))
            {
                validItem = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/items/item[@line_number=" + item.getLineNumber() + " and @status='error']")) != null ? false : true;
                warningItem = ((Element) DOMUtil.selectSingleNode(responseDocument, "/root/validation/order/items/item[@line_number=" + item.getLineNumber() + " and @status='warning']")) != null ? true : false;
        
                if(validItem && !warningItem)
                {
                    // Valid item with no warnings
                    item.setStatus(String.valueOf(OrderStatus.VALID_ITEM_INVALID_HEADER));
                }
                else if(validItem && warningItem)
                {
                    // Valid item with warnings (display as error)
                    item.setStatus(String.valueOf(OrderStatus.INVALID_ITEM));
                }
                else if(!validItem)
                {
                    // Invalid item
                    item.setStatus(String.valueOf(OrderStatus.INVALID_ITEM));
                }
            }

            // Update sort order associated with new status
            item.setStatusSortOrder((String) StatusSortUtil.getInstance().get(item.getStatus()));
        }
        
    }

}