package com.ftd.osp.orderscrub.constants;

public interface SecurityConstants 
{
    public static final String RESOURCE_NO_CHARGE = "NoCharge";
    public static final String RESOURCE_CREDIT_CARD = "CreditCard";
    public static final String RESOURCE_TEST_CATEGORY = "ScrubTestCategory";
    public static final String RESOURCE_ARIBA_CATEGORY = "ScrubAribaCategory";

    public static final String PERMISSION_YES = "Yes";
    public static final String PERMISSION_VIEW = "View";
    
    public static final String RESOURCE_PREFERRED_PARTNER_CONTEXT = "Order Proc";
}