package com.ftd.osp.orderscrub.constants;

public interface CustomMessageConstants 
{
    public static final String CUSTOM_MESSAGE_FILE = "custom_messages.xml";

    public static final String SEARCH_NO_RESULTS = "SEARCH_NO_RESULTS";
    public static final String SEARCH_USER_LOCKED = "SEARCH_USER_LOCKED";

    public static final String EMAIL_SEND_ERROR = "EMAIL_SEND_ERROR";
    public static final String EMAIL_MSG_TYPE_ERROR = "EMAIL_MSG_TYPE_ERROR";
    public static final String EMAIL_SUBJECT_ERROR = "EMAIL_SUBJECT_ERROR";
    public static final String EMAIL_BODY_ERROR = "EMAIL_BODY_ERROR";

    public static final String FRAUD_DISPOSITION_ERROR = "FRAUD_DISPOSITION_ERROR";
    public static final String FRAUD_COMMENTS_ERROR = "FRAUD_COMMENTS_ERROR";

    public static final String PENDING_DISPOSITION_ERROR = "PENDING_DISPOSITION_ERROR";
    public static final String PENDING_CALL_FLAG_ERROR = "PENDING_CALL_FLAG_ERROR";
    public static final String PENDING_EMAIL_FLAG_ERROR = "PENDING_EMAIL_FLAG_ERROR";
    public static final String PENDING_COMMENTS_ERROR = "PENDING_COMMENTS_ERROR";

    public static final String REMOVE_DISPOSITION_ERROR = "REMOVE_DISPOSITION_ERROR";
    public static final String REMOVE_CALL_FLAG_ERROR = "REMOVE_CALL_FLAG_ERROR";
    public static final String REMOVE_EMAIL_FLAG_ERROR = "REMOVE_EMAIL_FLAG_ERROR";
    public static final String REMOVE_COMMENTS_ERROR = "REMOVE_COMMENTS_ERROR";

    public static final String PAYMENT_NC_USER_ID_ERROR = "PAYMENT_NC_USER_ID_ERROR";
    public static final String PAYMENT_NC_PERMISSION_ERROR = "PAYMENT_NC_PERMISSION_ERROR";
    
    public static final String CANCEL_ORDER_ERROR_MESSAGE_PRICE_DIFF = 
                                "CANCEL_ORDER_ERROR_MESSAGE_PRICE_DIFF";
    
    public static final String EDIT_AMAZON_PRODUCT_AMOUNT_INCREASE = "EDIT_AMAZON_PRODUCT_AMOUNT_INCREASE";
    public static final String EDIT_AMAZON_SHIPPING_AMOUNT_INCREASE = "EDIT_AMAZON_SHIPPING_AMOUNT_INCREASE";
    public static final String EDIT_AMAZON_TAX_AMOUNT_INCREASE = "EDIT_AMAZON_TAX_AMOUNT_INCREASE";
    
    public static final String EDIT_MERCENT_PRODUCT_AMOUNT_INCREASE = "EDIT_MERCENT_PRODUCT_AMOUNT_INCREASE";
    public static final String EDIT_MERCENT_SHIPPING_AMOUNT_INCREASE = "EDIT_MERCENT_SHIPPING_AMOUNT_INCREASE";
    public static final String EDIT_MERCENT_TAX_AMOUNT_INCREASE = "EDIT_MERCENT_TAX_AMOUNT_INCREASE";
    
    public static final String EDIT_AAFES_ORDER_AMOUNT_INCREASE =
                                "EDIT_AAFES_ORDER_AMOUNT_INCREASE";

    public static final String EDIT_ARIBA_ORDER_AMOUNT_INCREASE =
                                "EDIT_ARIBA_ORDER_AMOUNT_INCREASE";

    public static final String BLOCK_REINSTATE_ORDER_DATE ="BLOCK_REINSTATE_ORDER_DATE";
    public static final String BLOCK_REINSTATE_ORDER_BILLED ="BLOCK_REINSTATE_ORDER_BILLED";
    public static final String BLOCK_REINSTATE_ORDER_DATE_REPLACE_TOKEN ="BLOCK_REINSTATE_ORDER_DATE_REPLACE_TOKEN";

    public static final String EDIT_ALT_PAY_ORDER_WARNING = "EDIT_ALT_PAY_ORDER_WARNING";    
    
    public static final String BLOCK_REINSTATE_GIFTCARD_ORDER = "BLOCK_REINSTATE_GIFTCARD_ORDER";

}