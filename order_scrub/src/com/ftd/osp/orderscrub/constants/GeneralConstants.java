package com.ftd.osp.orderscrub.constants;

public interface GeneralConstants 
{
    public static final String STATS_IN  = "IN";
    public static final String STATS_OUT = "OUT";
    public static final String PREFERRED_PARTNER_CONTEXT = "PREFERRED_PARTNER";
    public static final String PREFERRED_PARTNER_ORDER_ACCESS_RESTRICTION = "ORDER_ACCESS_RESTRICTION";
    public static final String PREFERRED_PARTNER_SOURCE_CODE_RESTRICTION = "SOURCE_CODE_RESTRICTION"; 
    public static final String PREFERRED_PARTNER_CACHE = "CACHE_NAME_PARTNER_HANDLER";
}