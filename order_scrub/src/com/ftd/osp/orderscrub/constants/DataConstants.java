package com.ftd.osp.orderscrub.constants;

public interface DataConstants 
{
    public static final String STOCK_MESG_BY_ID = "STOCK_MESG_BY_ID";
    public static final String STOCK_MESG_BY_COMPANY = "STOCK_MESG_BY_COMPANY";
    public static final String GLOBAL_PARMS_LOOKUP = "GLOBAL_PARMS_LOOKUP";
    public static final String GET_FLORIST_LIST = "GET_FLORIST_LIST";
    public static final String STATE_LIST_LOOKUP = "STATE_LIST_LOOKUP";
    public static final String FRAUD_DISPOSITION_LIST_LOOKUP = "FRAUD_DISPOSITION_LIST_LOOKUP";
    public static final String GET_ZIP_CODE_LIST = "GET_ZIP_CODE_LIST";
    public static final String SOURCE_CODE_RECORD_LOOKUP = "SOURCE_CODE_RECORD_LOOKUP";
    public static final String GET_PAYMENT_METHOD_BY_ID = "GET_PAYMENT_METHOD_BY_ID";
    public static final String GET_PAYMENT_METHODS_BY_TYPE = "GET_PAYMENT_METHODS_BY_TYPE";
    public static final String DISPOSITION_LIST_LOOKUP = "DISPOSITION_LIST_LOOKUP";
    public static final String ADDRESS_TYPES_LOOKUP = "ADDRESS_TYPES_LOOKUP";
    public static final String ORIGIN_LIST_LOOKUP = "ORIGIN_LIST_LOOKUP";
    public static final String GET_SCRUB_COUNT = "GET_SCRUB_COUNT";
    public static final String GET_SOURCECODELIST_BY_VALUE = "GET_SOURCECODELIST_BY_VALUE";
    public static final String BILLING_INFO_LOOKUP = "BILLING_INFO_LOOKUP";
    public static final String OCCASION_LIST_LOOKUP = "OCCASION_LIST_LOOKUP";
    public static final String COUNTRY_LIST_LOOKUP = "COUNTRY_LIST_LOOKUP";
    public static final String SHIP_METHODS_BY_PRODUCT_ID = "SHIP_METHODS_BY_PRODUCT_ID";
    public static final String SEARCH_FOR_ORDERS = "SEARCH_FOR_ORDERS";
    public static final String UPDATE_ORDER_TO_SCRUB_STATUS = "UPDATE_ORDER_TO_SCRUB_STATUS";
}