package com.ftd.osp.orderscrub.constants;

/**
 * @author smeka
 *
 */
public class GDAuthClearConstants {
	
	public static final String GD_AUTH_CLEAR_ERROR_TYPE = "System Exception";
	
	public static final String GD_AUTH_CLEAR_SOURCE = "ORDER SCRUB";
	
	public static final String PAYSVC_CURRENCY = "USD";
	
	public static final String GD_PAYMENT_TYPE = "GD";
	
	public static final String GD_NOPAGE_SOURCE = "GD Authorization clear";
	
	public static final String GD_AUTH_CLEAR_SUBJECT = "GD Auth Clear System Message";
	
	public static final String PS_AUTH_CLEAR_FAILED = "SVS transaction failed when order with GD payment is removed from scrub. Request Details are, ";
	
	//public static final String SVS_RETRY_TRANSACTION_FAILED = "SVS transaction failed when order with GD payment is removed from scrub. Request Details are, ";
	
	public static final String APOLLO_AUTH_CLEAR_FAILED = "Apollo could not perform GD auth clear.";
	
	public static final String GD_AUTH_CLEAR_SUCCESS = "Success";
	
	public static final String GD_AUTH_CLEAR_FAILURE = "Failure";
	
	public static final String SVS_SERVICE_CONTEXT = "SERVICE";
	
	public static final String ACCOUNTING_CONFIG_CONTEXT = "ACCOUNTING_CONFIG";
	
	public static final String CLIENT_ID_PROP_NAME = "SVS_CLIENT";
	
	public static final String CLIENT_PWD_PROP_NAME = "SVS_HASHCODE";
	
	public static final String PS_SERVICE_URL = "PAYMENT_SERVICE_URL";
	
	public static final String GD_RETRY_PROP_NAME = "GIFT_CARD_RETRY_COUNT";
	
	public static final String GD_DELAY_TIME_PROP_NAME = "GIFT_CARD_ENQUEUE_DELAY";
	
	

}
