package com.ftd.osp.orderscrub.constants;

public interface ConfigurationConstants 
{
    public static final String SCRUB_CONFIG_FILE = "scrub_config.xml";
    public static final String SCRUB_CONFIG_CONTEXT = "SCRUB_CONFIG";
    public static final String METADATA_CONFIG_FILE = "metadata_config.xml";
    
    public static final String STANDALONE_FLAG = "STANDALONE_FLAG";
    public static final String EMAIL_FROM_ADDRESS = "EMAIL_FROM_ADDRESS";
    public static final String JCPENNY_SCRIPT_CODE = "JCPENNY_SCRIPT_CODE";
    public static final String SECURITY_APP_CONTEXT = "SECURITY_APP_CONTEXT";
    public static final String DEFAULT_CSR = "DEFAULT_CSR";
    public static final String CORP_PURCHASE_CODE = "CORP_PURCHASE_CODE";
    public static final String ARIBA_ORIGIN_TYPE = "ARIBA_ORIGIN_TYPE";
    public static final String ORDER_ORIGIN_ARIBA1 = "ORDER_ORIGIN_ARIBA1";
    public static final String ORDER_ORIGIN_ARIBA2 = "ORDER_ORIGIN_ARIBA2";
    public static final String TEST_ORIGIN_TYPE = "TEST_ORIGIN_TYPE";
    public static final String ERROR_PAGE_LOCATION = "ERROR_PAGE_LOCATION";
    public static final String ORDER_ENTRY_LOCATION = "ORDER_ENTRY_LOCATION";
    public static final String SCRUB_LOCATION = "SCRUB_LOCATION";
    public static final String SECURITY_LOCATION = "SECURITY_LOCATION";
    public static final String OPS_ADMIN_LOCATION = "OPS_ADMIN_LOCATION";
    public static final String SECURITY_REDIRECT = "SECURITY_REDIRECT";
    public static final String SECURITY_DEFAULT_LOCATION = "SECURITY_DEFAULT_LOCATION";
    public static final String DATASOURCE_NAME = "DATASOURCE_NAME";
    public static final String DATASOURCE_LOCATION = "DATASOURCE_LOCATION";
 	  public static final String JNDI_TRANSACTION_ENTRY = "JNDI_TRANSACTION_ENTRY";
    public static final String NO_CHARGE_CODE = "NO_CHARGE_CODE";
    public static final String GIFT_CERT_CODE = "GIFT_CERT_CODE";
    public static final String AMAZON_ORIGIN_INTERNET  = "AMAZON_ORIGIN_INTERNET";
    public static final String WALMART_ORIGIN_INTERNET = "WALMART_ORIGIN_INTERNET";
    public static final String MERCENT_ORIGIN_INTERNET  = "MERCENT_ORIGIN_INTERNET";
    public static final String CSPI_ORIGIN_INTERNET    = "CSPI_ORIGIN_INTERNET"; 
    public static final String DAYS_LOCK_PENDING_REMOVED_ORDER    = "DAYS_LOCK_PENDING_REMOVED_ORDER"; 
    public static final String ALT_PAY_PRE_EDIT_WARNING_FLAG    = "ALT_PAY_PRE_EDIT_WARNING_FLAG"; 
    
    //edit product configs
    public static final String FLORAL_SHIPMETHOD_DEFAULT = "FLORAL_SHIPMETHOD_DEFAULT";
    public static final String SPEGFT_SHIPMETHOD_DEFAULT = "SPEGFT_SHIPMETHOD_DEFAULT";
    public static final String FRECUT_SHIPMETHOD_DEFAULT = "FRECUT_SHIPMETHOD_DEFAULT";
    public static final String NONE_SHIPMETHOD_DEFAULT = "NONE_SHIPMETHOD_DEFAULT";
    public static final String SDG_SHIPMETHOD_DEFAULT = "SDG_SHIPMETHOD_DEFAULT";
    public static final String SDFC_SHIPMETHOD_DEFAULT = "SDFC_SHIPMETHOD_DEFAULT";
    public static final String OTHER_SHIPMETHOD_DEFAULT = "OTHER_SHIPMETHOD_DEFAULT";

    
}