package com.ftd.osp.orderscrub.maintenance;

import com.ftd.ftdutilities.FieldUtils;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;

import java.sql.SQLException;

import java.util.Calendar;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class StatusResetServlet extends HttpServlet 
{
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        response.setContentType(CONTENT_TYPE);  
        this.loadScrubOrder(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException
    {
        response.setContentType(CONTENT_TYPE);
        this.submitStatusChange(request, response);
        
    }

    private void loadScrubOrder(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;

        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
        
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            
            // Load order from scrub
            SearchResultVO searchResult = this.retrieveByOrderNumber(request.getParameter("order_number"), request, dataRequest);

            // Transform result to xml and append to base xml
            this.appendResultList(searchResult, responseDocument);

            File xslFile = new File(getServletContext().getRealPath("/xsl/statusResetResult.xsl"));
            TraxUtil.getInstance().transform(request, response, responseDocument, xslFile, null);
            
        }
        catch(Exception e)
        {
            try 
            {
                e.printStackTrace();
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                e.printStackTrace();
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                se.printStackTrace();
            }
        }
    }

    private void submitStatusChange(HttpServletRequest request, HttpServletResponse response)
    {
        DataRequest dataRequest = null;

        try
        {
            // Create the initial document
            Document responseDocument = DOMUtil.getDocument();
        
            // Initialize data request
            dataRequest = DataRequestHelper.getInstance().getDataRequest();

            dataRequest.reset();
            dataRequest.setStatementID("UPDATE_ORDER_STATUS");
            dataRequest.addInputParam("order_guid", request.getParameter("order_guid"));
            dataRequest.addInputParam("order_status", request.getParameter("updated_status"));
            DataAccessUtil.getInstance().execute(dataRequest); 

            String statusResetURL = ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.SCRUB_LOCATION) + "/statusReset.html?update=y";
            response.sendRedirect(statusResetURL);
        }
        catch(Exception e)
        {
            try 
            {
                e.printStackTrace();
                request.getRequestDispatcher(ConfigurationUtil.getInstance().getFrpGlobalParm(ConfigurationConstants.SCRUB_CONFIG_CONTEXT, ConfigurationConstants.ERROR_PAGE_LOCATION)).forward(request, response);
            }
            catch(Exception re) 
            {
                e.printStackTrace();
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                se.printStackTrace();
            }
        }
            
    }

    private void appendResultList(SearchResultVO searchResult, Document responseDocument) throws Exception
    {
        Element resultElement = null;
        Element guidElement = null;
        Element orderNumberElement = null;
        Element lastNameElement = null;
        Element firstNameElement = null;
        Element cityElement = null;
        Element stateElement = null;
        Element orderDateElement = null;
        Element itemCountElement = null;
        Element orderTotalElement = null;
        Element csrIdElement = null;
        Element statusElement = null;

        Element resultListElement = responseDocument.createElement("search_result_list");

        if(searchResult != null)
        {
            resultElement = responseDocument.createElement("search_result");
            resultElement.setAttribute("sort_number", searchResult.getSortOrder());

            guidElement = responseDocument.createElement("order_guid");
            if(searchResult.getGuid() != null)
            {
                guidElement.appendChild(responseDocument.createTextNode(searchResult.getGuid()));
                resultElement.appendChild(guidElement);
            }

            orderNumberElement = responseDocument.createElement("master_order_number");
            if(searchResult.getMasterOrderNumber() != null)
            {
                orderNumberElement.appendChild(responseDocument.createTextNode(searchResult.getMasterOrderNumber()));
                resultElement.appendChild(orderNumberElement);
            }

            lastNameElement = responseDocument.createElement("buyer_last_name");
            if(searchResult.getBuyerLastName() != null)
            {
                lastNameElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerLastName()));
                resultElement.appendChild(lastNameElement);
            }

            firstNameElement = responseDocument.createElement("buyer_first_name");
            if(searchResult.getBuyerFirstName() != null)
            {
                firstNameElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerFirstName()));
                resultElement.appendChild(firstNameElement);
            }

            cityElement = responseDocument.createElement("buyer_city");
            if(searchResult.getBuyerCity() != null)
            {
                cityElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerCity()));
                resultElement.appendChild(cityElement);
            }

            stateElement = responseDocument.createElement("buyer_state");
            if(searchResult.getBuyerState() != null)
            {
                stateElement.appendChild(responseDocument.createTextNode(searchResult.getBuyerState()));
                resultElement.appendChild(stateElement);
            }

            orderDateElement = responseDocument.createElement("order_date");
            if(searchResult.getOrderDate() != null)
            {
                orderDateElement.appendChild(responseDocument.createTextNode(searchResult.getOrderDate()));
                resultElement.appendChild(orderDateElement);
            }

            itemCountElement = responseDocument.createElement("item_count");
            if(searchResult.getItemCount() != null)
            {
                itemCountElement.appendChild(responseDocument.createTextNode(searchResult.getItemCount()));
                resultElement.appendChild(itemCountElement);
            }

            orderTotalElement = responseDocument.createElement("order_total");
            if(searchResult.getOrderTotal() != null)
            {
                orderTotalElement.appendChild(responseDocument.createTextNode(searchResult.getOrderTotal()));
                resultElement.appendChild(orderTotalElement);
            }

            csrIdElement = responseDocument.createElement("csr_id");
            if(searchResult.getCsrId() != null)
            {
                csrIdElement.appendChild(responseDocument.createTextNode(searchResult.getCsrId()));
                resultElement.appendChild(csrIdElement);
            }

            statusElement = responseDocument.createElement("order_status");
            if(searchResult.getOrderStatus() != null)
            {
                statusElement.appendChild(responseDocument.createTextNode(searchResult.getOrderStatus()));
                resultElement.appendChild(statusElement);
            }

            resultListElement.appendChild(resultElement);
        }
        

        responseDocument.getFirstChild().appendChild(resultListElement);
    }

    private SearchResultVO retrieveByOrderNumber(String orderNumber, HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        SearchResultVO searchResult = null;
    
        // Load list of order data from scrub
        dataRequest.reset();
        dataRequest.setStatementID("SEARCH_FOR_ORDERS");
        dataRequest.addInputParam("confirmation_number", orderNumber);
        dataRequest.addInputParam("order_origin", "('FOX','BULK','GIFTI','SFMBI','ARI','CAT','TEST')");
        dataRequest.addInputParam("delivery_date", null);   
        dataRequest.addInputParam("buyers_last_name", null);
        dataRequest.addInputParam("buyers_phone_number", null);
        dataRequest.addInputParam("email", null);
        dataRequest.addInputParam("ship_date", null);
        dataRequest.addInputParam("product_id", null);
        dataRequest.addInputParam("recipients_last_name", null);
        dataRequest.addInputParam("recipients_phone_number", null);
        dataRequest.addInputParam("destination_type", null);
        dataRequest.addInputParam("csr_id", null);
        dataRequest.addInputParam("date_from", null);
        dataRequest.addInputParam("date_to", null);
        dataRequest.addInputParam("mode", "E");
        dataRequest.addInputParam("user_id", null);
        dataRequest.addInputParam("sort_type", null);
        dataRequest.addInputParam("source_code", null);
        dataRequest.addInputParam("scrub_reason", null);
        dataRequest.addInputParam("product_property", null);
        
        CachedResultSet searchResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest); 
                
        if(searchResultSet != null && searchResultSet.getRowCount() > 0)
        {
            Calendar shipCalendar = null;
            Calendar deliveryCalendar = null;
            
            while(searchResultSet.next())
            {
                shipCalendar = Calendar.getInstance();
                deliveryCalendar = Calendar.getInstance();
                
                if(searchResultSet.getObject(4) != null)
                {
                    try
                    {
                        shipCalendar.setTime(FieldUtils.formatStringToUtilDate((String) searchResultSet.getObject(4)));
                    }
                    catch(Exception e)
                    {
                        // Invalid ship date format
                        shipCalendar = null;
                    }
                }
                else
                {
                    shipCalendar = null;
                }

                if(searchResultSet.getObject(5) != null)
                {
                    try
                    {
                        deliveryCalendar.setTime(FieldUtils.formatStringToUtilDate((String) searchResultSet.getObject(5)));
                    }
                    catch(Exception e)
                    {
                        // Invalid delivery date format
                        deliveryCalendar = null;
                    }
                        
                }
                else
                {
                    deliveryCalendar = null;
                }
                
                searchResult = new SearchResultVO();

                searchResult.setGuid((String) searchResultSet.getObject(1));
                searchResult.setOrderStatus((String) searchResultSet.getObject(3));
                searchResult.setCsrId((String) searchResultSet.getObject(4));
                searchResult.setMasterOrderNumber((String) searchResultSet.getObject(5));
                searchResult.setOrderDate((String) searchResultSet.getObject(6));
                searchResult.setItemCount((String) searchResultSet.getObject(7));
                searchResult.setOrderTotal((String) searchResultSet.getObject(8));
                searchResult.setBuyerLastName((String) searchResultSet.getObject(9));
                searchResult.setBuyerFirstName((String) searchResultSet.getObject(10));
                searchResult.setBuyerCity((String) searchResultSet.getObject(11));
                searchResult.setBuyerState((String) searchResultSet.getObject(12));
            }
        }

        return searchResult;
    }
}

