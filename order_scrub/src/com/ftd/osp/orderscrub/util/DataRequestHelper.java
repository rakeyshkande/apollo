package com.ftd.osp.orderscrub.util;

import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;

import java.sql.Connection;

/**
 * This class is a singleton helper utility for generating DataRequest objects.
 *
 * @author Brian Munter
 */

public class DataRequestHelper 
{
    private static DataRequestHelper DATA_REQUEST_HELPER;

    /**
     * Returns an instance of DataRequestHelper.
     * 
     * @exception Exception
     */
    public static synchronized DataRequestHelper getInstance() throws Exception
    {
        if (DATA_REQUEST_HELPER == null)
        {
            DATA_REQUEST_HELPER = new DataRequestHelper();
            return DATA_REQUEST_HELPER;
        }
        else 
        {
            return DATA_REQUEST_HELPER;
        }
    }

    /**
     * Retrieves a DataRequest object based on connection properties specified 
     * in the Scrub configuration file.
     * 
     * @exception Exception
     */
    public DataRequest getDataRequest() throws Exception
    {
        String dataSourceName = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.DATASOURCE_NAME);
        // String dataSourceLocation = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.DATASOURCE_LOCATION);
    
        Connection connection =  DataSourceUtil.getInstance().getConnection(dataSourceName);
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(connection);

        return dataRequest;
    }
}