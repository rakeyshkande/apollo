package com.ftd.osp.orderscrub.util;

import java.io.StringWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import com.ftd.ftdutilities.FTDCAMSUtils;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.orderscrub.vo.SearchResultVO;
import com.ftd.osp.ordervalidator.util.ValidationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.PartnerUtility;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.MercentOrderPrefixes;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.PartnerMappingVO;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * This class is a utility to facilitate order searching for the Srub system. 
 * It ties into the ScrubFilterBO to perform certain logic for the results of 
 * the search.
 *
 * @author Brian Munter
 */

public class SearchManager 
{
    private Logger logger;
    private final static String SEARCH_DATE_FORMAT = "MM/dd/yyyy";
    private final static String SEARCH_DELIVERY_TYPE = "D";
    private final static String SEARCH_SHIP_TYPE = "S";
    public static final String EMPTY_STRING = "";
    
    private final static String AMAZON_ORDER_PREFIX = "A";
    private final static String WALMART_ORDER_PREFIX = "W";
    private final static String AMAZON_ORDER_SEPARATOR = "-";
    
    private final static String NON_PREFERRED = "NON_PREFERRED";
    private final static String VIP = "VIP";

    /** 
     * Constructor
     */
    public SearchManager()
    {
        logger = new Logger("com.ftd.osp.orderscrub.util.SearchManager");
    }

    /**
     * Returns a SearchResultVO object based on the submitted search criteria. 
     * The individual object return is based on filtering criteria specified in 
     * the ScrubFilterBO object.
     * 
     * @param request HttpServletRequest
     * @param dataRequest DataRequest
     * @exception Exception
     */
    public SearchResultVO retrieveResult(HttpServletRequest request, DataRequest dataRequest, boolean retrieveOrderBeingScrubbed) throws Exception {
    	logger.debug("INSIDE retrieveResult");
    	
    	String proConfirmationNumber = request.getParameter("sc_pro_confirmation_number");
        if (!StringUtils.isEmpty(proConfirmationNumber)) {
        	return retrieveProSearchOrder(dataRequest, request);
        }
        
       
        String securityToken = request.getParameter("securitytoken");
        String emailAddress = request.getParameter("sc_email_address");
        String productCode = request.getParameter("sc_product_code");
        String shipToType = request.getParameter("sc_ship_to_type");
        String confirmationNumber = request.getParameter("sc_confirmation_number");
        String origin = request.getParameter("sc_origin");
        String interfaceMode = request.getParameter("sc_mode");
        String productProperty = request.getParameter("sc_product_property");  
        String preferredPartner = request.getParameter("sc_preferred_partner");
        String pcMembershipId = request.getParameter("sc_pc_membership_id");
       
        String sortType = null;
        String shipDate = EMPTY_STRING;
        String deliveryDate = EMPTY_STRING;
        String searchPreferredPartner = null;
        

        
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	origin = hideTestOrders(origin);
    	}
        if(request.getParameter("sc_date") != null && !request.getParameter("sc_date").equals(EMPTY_STRING))
        {
            if(request.getParameter("sc_date_flag") != null && request.getParameter("sc_date_flag").equals("delivery"))
            {
                sortType = SEARCH_DELIVERY_TYPE;
                deliveryDate = formatSearchDate(request.getParameter("sc_date"));
            }
            else if(request.getParameter("sc_date_flag") != null && request.getParameter("sc_date_flag").equals("ship"))
            {
                sortType = SEARCH_SHIP_TYPE;
                shipDate = formatSearchDate(request.getParameter("sc_date"));
            }
        }
        
        String buyerLastName = EMPTY_STRING;
        String recipientLastName = EMPTY_STRING;
        if(request.getParameter("sc_last_name_flag") != null && request.getParameter("sc_last_name_flag").equals("customer"))
        {
            buyerLastName = request.getParameter("sc_last_name");
        }
        else if(request.getParameter("sc_last_name_flag") != null && request.getParameter("sc_last_name_flag").equals("recipient"))
        {
            recipientLastName = request.getParameter("sc_last_name");
        }

        String buyerPhone = EMPTY_STRING;
        String recipientPhone = EMPTY_STRING;
        if(request.getParameter("sc_phone_flag") != null && request.getParameter("sc_phone_flag").equals("customer"))
        {
            if(request.getParameter("sc_phone") != null)
            {
                buyerPhone = this.removeAllSpecialChars(request.getParameter("sc_phone"));
            }
        }
        else if(request.getParameter("sc_phone_flag") != null && request.getParameter("sc_phone_flag").equals("recipient"))
        {
            if(request.getParameter("sc_phone") != null)
            {
                recipientPhone = this.removeAllSpecialChars(request.getParameter("sc_phone"));
            }
        }

        String dateFrom = EMPTY_STRING;
        String dateTo = EMPTY_STRING;
        if(request.getParameter("sc_date_from") != null && !request.getParameter("sc_date_from").equals(""))
        {
            dateFrom = request.getParameter("sc_date_from");
        }
        if(request.getParameter("sc_date_to") != null && !request.getParameter("sc_date_to").equals(""))
        {
            dateTo = request.getParameter("sc_date_to");
        }

        String newSearchFlag = null;
        if(request.getParameter("new_search_flag") != null && !request.getParameter("new_search_flag").equals(""))
        {
            newSearchFlag = request.getParameter("new_search_flag");
        }
        else
        {
            newSearchFlag = "N";
        }

        String csrId = EMPTY_STRING;
        if(request.getParameter("sc_csr_id") != null)
        {
            csrId = request.getParameter("sc_csr_id");
        }
        
        // If any of below fields are set we must include any Preferred Parter (e.g., USAA)
        // orders in our search.  Otherwise, search based on selection (if any) 
        // or default to non-preferred orders only.
        //
        if ((confirmationNumber != null && !confirmationNumber.equals("")) ||
        	(pcMembershipId != null && !pcMembershipId.equals("")) ||
            (emailAddress != null && !emailAddress.equals("")) ||        
            (!buyerLastName.equals("")) || (!recipientLastName.equals("")) ||
            (!buyerPhone.equals("")) || (!recipientPhone.equals(""))
            )
        {
          searchPreferredPartner = null;             // Search all
        }
        else if(VIP.equalsIgnoreCase(preferredPartner))
        	searchPreferredPartner = VIP;    //VIP Customer Orders
        else if(NON_PREFERRED.equalsIgnoreCase(preferredPartner)){
            searchPreferredPartner = NON_PREFERRED;    // Search non-preferred only
        }
        else if (preferredPartner != null && !preferredPartner.equals("") && !VIP.equalsIgnoreCase(preferredPartner) && !NON_PREFERRED.equalsIgnoreCase(preferredPartner) ) {
            searchPreferredPartner = preferredPartner; // Search based on selection
        }
        String sourceCode = request.getParameter("sc_source_code");        
        String reasonSelected = request.getParameter("sc_scrub_reason");
        String scrubReason = getReasons(reasonSelected);
        
        origin = getMercentOrigins(origin, dataRequest);
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	origin = hideTestOrders(origin);
    	}
        
        
        /*No More Premier Circle Membership Profile Service Integration
        String camsGeneratedId = EMPTY_STRING;
        if(pcMembershipId != null && pcMembershipId != EMPTY_STRING)
        {
        	camsGeneratedId = FTDCAMSUtils.getPCMembershipIdByMembershipNumber(pcMembershipId);
        	if(camsGeneratedId == null){
        		return null;
        	}
        }        
        //End Premier Circle Membership
        */
        
        //For Partner Order - skip mercent order check if the given number is partner order number, include the category/origin for search.
        String originsTemp = request.getParameter("sc_origin");
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	originsTemp = hideTestOrders(originsTemp);
    	}
        String[] originsList = getOriginsAsArrayOfStrings(originsTemp);
        CachedResultSet partnerCrs = null;
        boolean isPartnerOrderNumber = false;
        boolean partnerOrigin = false;
        PartnerUtility partnerUtil = new PartnerUtility();
        
        if(originsList == null || originsList.length > 1) {
        	partnerCrs = partnerUtil.getPartnerOrderInfoByOrderNumber(confirmationNumber, null, dataRequest.getConnection());        	
        } else {        	
        	partnerCrs = partnerUtil.getPartnerOrderInfoByOrderNumber(confirmationNumber, originsList[0], dataRequest.getConnection()); 
        	if(!StringUtils.isEmpty(originsList[0]) && sourceCode!=null) {
    			PartnerMappingVO partnerMappingVO = partnerUtil.getPartnerOriginsInfo(originsList[0],sourceCode, dataRequest.getConnection());
    			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
    				partnerOrigin = true;
    			}
            }    
        	else{
        		logger.info("Source code not found for the Partner order. Unable to get Partner Mapping");
        	}
        }
        
        if(partnerCrs != null && partnerCrs.next()){
        	confirmationNumber = partnerCrs.getString("MASTER_ORDER_NUMBER"); 
        	isPartnerOrderNumber = true;
        }  
        
        //End of partner Order logic
		
        // Load list of order data from scrub
        dataRequest.reset();
        dataRequest.setStatementID("GET_FIRST_SCRUB_ORDER");
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	origin = hideTestOrders(origin);
    	}
        if(!isPartnerOrderNumber) {
        	dataRequest.addInputParam("confirmation_number", this.formatConfirmationNumber(confirmationNumber.trim()));
        } else {
        	dataRequest.addInputParam("confirmation_number", confirmationNumber.trim());
        }
        if(!partnerOrigin) {
        	dataRequest.addInputParam("order_origin", formatValueByOrderNumber(origin, confirmationNumber));
        } else {
        	dataRequest.addInputParam("order_origin", origin);
        }
        dataRequest.addInputParam("delivery_date", deliveryDate.trim());   
        dataRequest.addInputParam("buyers_last_name", buyerLastName.trim());
        dataRequest.addInputParam("buyers_phone_number", buyerPhone.trim());
        dataRequest.addInputParam("email", emailAddress.trim());
        dataRequest.addInputParam("ship_date", shipDate.trim());
        dataRequest.addInputParam("product_id", productCode.trim());
        dataRequest.addInputParam("recipients_last_name", recipientLastName.trim());
        dataRequest.addInputParam("recipients_phone_number", recipientPhone.trim());
        dataRequest.addInputParam("destination_type", shipToType);
        dataRequest.addInputParam("csr_id", csrId.trim());
        dataRequest.addInputParam("date_from", dateFrom.trim());
        dataRequest.addInputParam("date_to", dateTo.trim());
        dataRequest.addInputParam("mode", interfaceMode);
        dataRequest.addInputParam("user_id", SecurityHelper.getUserId(request));
        dataRequest.addInputParam("sort_type", sortType);
        dataRequest.addInputParam("new_search_flag", newSearchFlag);
        dataRequest.addInputParam("source_code", sourceCode);
        dataRequest.addInputParam("scrub_reason", scrubReason);
        dataRequest.addInputParam("product_property", formatValueByOrderNumber(productProperty, confirmationNumber));    
        dataRequest.addInputParam("preferred_partner", searchPreferredPartner);
        dataRequest.addInputParam("premier_circle_membership_id", "");

        try
        {
          if (logger.isDebugEnabled())
          {
            logger.debug("confirmation_number = " + this.formatConfirmationNumber(confirmationNumber.trim()));
            logger.debug("order_origin = " + formatValueByOrderNumber(origin, confirmationNumber));
            logger.debug("delivery_date = " + deliveryDate.trim());   
            logger.debug("buyers_last_name = " + buyerLastName.trim());
            logger.debug("buyers_phone_number = " + buyerPhone.trim());
            logger.debug("email = " + emailAddress.trim());
            logger.debug("ship_date = " + shipDate.trim());
            logger.debug("product_id = " + productCode.trim());
            logger.debug("recipients_last_name = " + recipientLastName.trim());
            logger.debug("recipients_phone_number = " + recipientPhone.trim());
            logger.debug("destination_type = " + shipToType);
            logger.debug("csr_id = " + csrId.trim());
            logger.debug("date_from = " + dateFrom.trim());
            logger.debug("date_to = " + dateTo.trim());
            logger.debug("mode = " + interfaceMode);
            logger.debug("user_id = " + SecurityHelper.getUserId(request));
            logger.debug("sort_type = " + sortType);
            logger.debug("new_search_flag = " + newSearchFlag);
            logger.debug("source_code = " + sourceCode);
            logger.debug("scrub_reason = " + scrubReason);
            logger.debug("product_property = " + formatValueByOrderNumber(productProperty, confirmationNumber));    
            logger.debug("preferred_partner = " + searchPreferredPartner);
          }
        }
        catch (Exception e)
        {}
        
        String orderGuid = null;

        if (retrieveOrderBeingScrubbed) {
            if (confirmationNumber != null || emailAddress != null || buyerLastName != null ||
                buyerPhone != null || recipientLastName != null || recipientPhone != null) {

                logger.info("Calling SEARCH_FOR_ORDERS");
                dataRequest.setStatementID("SEARCH_FOR_ORDERS");
                dataRequest.addInputParam("include_being_scrubbed", "Y");

                CachedResultSet crs = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
                
                logger.debug("Total number of records returned = " + crs.getRowCount());
                while (crs.next()) {
                    orderGuid = crs.getString("order_guid");
                    logger.info("1005 order guid: " + orderGuid);
                }

            }
        } else {
            orderGuid = (String) DataAccessUtil.getInstance().execute(dataRequest); 
        }
        
        SearchResultVO searchResult = null;
        if(orderGuid != null && orderGuid.trim().length() > 0)
        {
            searchResult = new SearchResultVO();
            searchResult.setGuid(orderGuid);
        }

        return searchResult;
    }

    /*
     * Return a comma delmited list of scrub reasons that are releated to the
     * passed in scrub reason type selected on the UI.
     */
     private String getReasons(String reasonSelected) throws Exception
     {
        String scrubReason = null;
     
        if(reasonSelected != null && reasonSelected.length() > 0)
        {
            if(reasonSelected.equals("INVALIDADDRESS"))
            {
                scrubReason = "'" + ValidationConstants.RESPONSE_QMS_ADDRESS_INVALID + "'";
                scrubReason = scrubReason + ",'" + ValidationConstants.RESPONSE_QMS_CITY_STATE_INVALID + "'";
                scrubReason = scrubReason + ",'" + ValidationConstants.RESPONSE_QMS_ZIP_INVALID + "'";
                scrubReason = scrubReason + ",'" + ValidationConstants.RESPONSE_QMS_CITY_INVALID + "'";
            }
            else if(reasonSelected.equals("PRODUCTUNAVAILABLE"))
            {
                scrubReason = "'" + ValidationConstants.RESPONSE_PROD_INVALID + "'";
                scrubReason = scrubReason + ",'" + ValidationConstants.RESPONSE_PRODUCT_NOT_AVAIL_IN_ZIP + "'";
                scrubReason = scrubReason + ",'" + ValidationConstants.RESPONSE_PROD_UNAVAILABLE + "'";
            }
            else
            {
                throw new Exception("Unknown scrub reason selected:" + reasonSelected);
            }
        }       
        
        return scrubReason;
     }

    /**
     * Returns an ArrayList of SearchResultVO objects based on the submitted 
     * search criteria. The list passes through the ScrubFilterBO object to be 
     * sorted accordingly before it is returned.
     * 
     * @param request HttpServletRequest
     * @param dataRequest DataRequest
     * @exception Exception
     */
    public ArrayList retrieveResultMap(HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
    	String proConfirmationNumber = request.getParameter("sc_pro_confirmation_number");
    	if (!StringUtils.isEmpty(proConfirmationNumber)) {
    		return retrieveProSearchList(dataRequest, request); // Map should always get the list of rows
    	}
    	
        String securityToken = request.getParameter("securitytoken");
        String emailAddress = request.getParameter("sc_email_address");
        String productCode = request.getParameter("sc_product_code");
        String shipToType = request.getParameter("sc_ship_to_type");
        String confirmationNumber = request.getParameter("sc_confirmation_number");
       
        String origin = request.getParameter("sc_origin");
        String searchType = request.getParameter("sc_search_type")!=null? request.getParameter("sc_search_type") :"S";
        String productProperty = request.getParameter("sc_product_property");
        String preferredPartner = request.getParameter("sc_preferred_partner");
        String pcMembershipId = request.getParameter("sc_pc_membership_id");
        CachedResultSet searchProFlowersResultSet = null;
        ArrayList sortedResults = new ArrayList();
        
        String sortType = null;
        String shipDate = EMPTY_STRING;
        String deliveryDate = EMPTY_STRING;
        String searchPreferredPartner = null;
        
        String csrId = EMPTY_STRING;
        
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	origin = hideTestOrders(origin);
    	}
        
        if(request.getParameter("sc_csr_id") != null)
        {
            csrId = request.getParameter("sc_csr_id");
        }
        
        String newSearchFlag = null;
        if(request.getParameter("new_search_flag") != null && !request.getParameter("new_search_flag").equals(""))
        {
            newSearchFlag = request.getParameter("new_search_flag");
        }
        else
        {
            newSearchFlag = "N";
        }

        
        if (proConfirmationNumber!=null && !("").equals(proConfirmationNumber)) {
        	String proConf = proConfirmationNumber.concat(ConfigurationUtil.getInstance().getFrpGlobalParm("GLOBAL_CONFIG", "PRO_ORDER_SEARCH_APPENDER"));
        	dataRequest.reset();
        	dataRequest.setStatementID("GET_PROFLOWERS_ORDERS");
    		dataRequest.addInputParam("IN_WEB_ORDER_ID", proConf.toString());
    		dataRequest.addInputParam("IN_CSR_ID_SEARCH",csrId.trim());
    		dataRequest.addInputParam("IN_STATUS_FLAG",searchType);
    		dataRequest.addInputParam("IN_CSR",SecurityHelper.getUserId(request));
    		dataRequest.addInputParam("IN_NEW_SEARCH_FLAG",newSearchFlag);
    		dataRequest.addInputParam("IN_EXCLUDE_FLAG","Y");
    		dataRequest.addInputParam("IN_INCLUDE_BEING_SCRUBBED","N");
    		searchProFlowersResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
    		logger.debug("The row count is: " +searchProFlowersResultSet.getRowCount());

    		
    		if(searchProFlowersResultSet != null && searchProFlowersResultSet.getRowCount() > 0)
            {   
        		SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
        		String dDate = null;
        		Calendar calDDate = null;
                while(searchProFlowersResultSet.next())
                {
                	
                	dDate = searchProFlowersResultSet.getString("delivery_date");
                	if (dDate!=null) {
                		calDDate = Calendar.getInstance();
                		calDDate.setTime(sdf.parse((dDate)));
                	}
                    // Result list display fields
                	SearchResultVO searchProFlowersResultSetVO = new SearchResultVO();
                    
                	searchProFlowersResultSetVO.setGuid( searchProFlowersResultSet.getString("order_guid"));
                	searchProFlowersResultSetVO.setCsrId(searchProFlowersResultSet.getString("csr_id"));
                	searchProFlowersResultSetVO.setMasterOrderNumber( searchProFlowersResultSet.getString("master_order_number"));
                	searchProFlowersResultSetVO.setExternalOrderNumber(searchProFlowersResultSet.getString("external_order_number"));
                	searchProFlowersResultSetVO.setOrderDate(searchProFlowersResultSet.getString("order_date"));
                	searchProFlowersResultSetVO.setDeliveryDate(calDDate);
                	searchProFlowersResultSetVO.setPreferredProcessingPartner("ProFlowers");
                	searchProFlowersResultSetVO.setOrderTotal(searchProFlowersResultSet.getString("order_total"));
                	searchProFlowersResultSetVO.setBuyerCity(searchProFlowersResultSet.getString("city"));
                	searchProFlowersResultSetVO.setBuyerState(searchProFlowersResultSet.getString("STATE_PROVINCE"));
                	searchProFlowersResultSetVO.setBuyerFirstName(searchProFlowersResultSet.getString("buyer_first_name"));
                	searchProFlowersResultSetVO.setBuyerLastName(searchProFlowersResultSet.getString("buyer_last_name"));//PRODUCTS_TOTAL
                	searchProFlowersResultSetVO.setItemCount(searchProFlowersResultSet.getString("PRODUCTS_TOTAL"));//PRODUCTS_TOTAL
                	//String recipName = (String) searchProFlowersResultSet.getString("first_name") + " "+ (String) searchProFlowersResultSet.getString("last_name");
                	searchProFlowersResultSetVO.setRecipFirstName(searchProFlowersResultSet.getString("recip_first_name"));
                	searchProFlowersResultSetVO.setRecipLastName(searchProFlowersResultSet.getString("recip_last_name"));
                    sortedResults.add(searchProFlowersResultSetVO);
                }
                
            }
           	return sortedResults;
        }

        if(request.getParameter("sc_date") != null && !request.getParameter("sc_date").equals(EMPTY_STRING))
        {
            if(request.getParameter("sc_date_flag") != null && request.getParameter("sc_date_flag").equals("delivery"))
            {
                sortType = SEARCH_DELIVERY_TYPE;
                deliveryDate = formatSearchDate(request.getParameter("sc_date"));
            }
            else if(request.getParameter("sc_date_flag") != null && request.getParameter("sc_date_flag").equals("ship"))
            {
                sortType = SEARCH_SHIP_TYPE;
                shipDate = formatSearchDate(request.getParameter("sc_date"));
            }
        }

        String buyerLastName = EMPTY_STRING;
        String recipientLastName = EMPTY_STRING;
        if(request.getParameter("sc_last_name_flag") != null && request.getParameter("sc_last_name_flag").equals("customer"))
        {
            buyerLastName = request.getParameter("sc_last_name");
        }
        else if(request.getParameter("sc_last_name_flag") != null && request.getParameter("sc_last_name_flag").equals("recipient"))
        {
            recipientLastName = request.getParameter("sc_last_name");
        }

        String buyerPhone = EMPTY_STRING;
        String recipientPhone = EMPTY_STRING;
        if(request.getParameter("sc_phone_flag") != null && request.getParameter("sc_phone_flag").equals("customer"))
        {
            if(request.getParameter("sc_phone") != null)
            {
                buyerPhone = this.removeAllSpecialChars(request.getParameter("sc_phone"));
            }
        }
        else if(request.getParameter("sc_phone_flag") != null && request.getParameter("sc_phone_flag").equals("recipient"))
        {
            if(request.getParameter("sc_phone") != null)
            {
                recipientPhone = this.removeAllSpecialChars(request.getParameter("sc_phone"));
            }
        }

        String dateFrom = EMPTY_STRING;
        String dateTo = EMPTY_STRING;
        if(request.getParameter("sc_date_from") != null && !request.getParameter("sc_date_from").equals(""))
        {
            dateFrom = request.getParameter("sc_date_from");
        }
        if(request.getParameter("sc_date_to") != null && !request.getParameter("sc_date_to").equals(""))
        {
            dateTo = request.getParameter("sc_date_to");
        }

        // If any of below fields are set we must include any Preferred Parter (e.g., USAA)
        // orders in our search.  Otherwise, search based on selection (if any) 
        // or default to non-preferred orders only.
        //
        if ((confirmationNumber != null && !confirmationNumber.equals("")) ||
        	(pcMembershipId != null && !pcMembershipId.equals("")) ||
            (emailAddress != null && !emailAddress.equals("")) ||        
            (!buyerLastName.equals("")) || (!recipientLastName.equals("")) ||
            (!buyerPhone.equals("")) || (!recipientPhone.equals("")) ||(preferredPartner == null) || ("".equals(preferredPartner))
            )
        {
          searchPreferredPartner = null;             // Search all
        } else if (preferredPartner != null && !preferredPartner.equals("") && !VIP.equalsIgnoreCase(preferredPartner) && !NON_PREFERRED.equalsIgnoreCase(preferredPartner) ) {
            searchPreferredPartner = preferredPartner;// Search based on selection
        }
        else if(VIP.equalsIgnoreCase(preferredPartner))
        	searchPreferredPartner = VIP;    //VIP Customer Orders
        else if(NON_PREFERRED.equalsIgnoreCase(preferredPartner)){
            searchPreferredPartner = NON_PREFERRED;    // Search non-preferred only
        }

        String sourceCode = request.getParameter("sc_source_code");        
        String reasonSelected = request.getParameter("sc_scrub_reason");
        String scrubReason = getReasons(reasonSelected);        
    	origin = getMercentOrigins(origin, dataRequest);    
	
        /*
    	//Premier Circle Membership
        String camsGeneratedId = EMPTY_STRING;
        if(pcMembershipId != null && pcMembershipId != EMPTY_STRING)
        {
        	camsGeneratedId = FTDCAMSUtils.getPCMembershipIdByMembershipNumber(pcMembershipId);
        	if(camsGeneratedId == null){
        		return null;
        	}
        }        
        //End Premier Circle Membership
        */
         
        //For Partner Order - skip mercent order check if the given number is partner order number, include the category/origin for search.
        String originTemp = request.getParameter("sc_origin");
        
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	originTemp = hideTestOrders(originTemp);
    	}
        String[] originsList = getOriginsAsArrayOfStrings(originTemp);
        CachedResultSet partnerCrs = null;
        boolean isPartnerOrderNumber = false;
        boolean partnerOrigin = false;
        PartnerUtility partnerUtil = new PartnerUtility();
        
        if(originsList == null || originsList.length > 1) {
        	partnerCrs = partnerUtil.getPartnerOrderInfoByOrderNumber(confirmationNumber, null, dataRequest.getConnection());         	
        } else {          	
        	partnerCrs = partnerUtil.getPartnerOrderInfoByOrderNumber(confirmationNumber, originsList[0], dataRequest.getConnection()); 
        	if(!StringUtils.isEmpty(originsList[0]) && sourceCode != null) {
    			PartnerMappingVO partnerMappingVO = partnerUtil.getPartnerOriginsInfo(originsList[0],sourceCode, dataRequest.getConnection());
    			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
    				partnerOrigin = true;
    			}
    			
            }  
        	else{
				logger.info("Source code not found for the Partner order. Unable to get Partner Mapping");
			}
        }
        
        if(partnerCrs != null && partnerCrs.next()){
        	confirmationNumber = partnerCrs.getString("MASTER_ORDER_NUMBER"); 
        	isPartnerOrderNumber = true;
        } 
        // Load list of order data from scrub
        dataRequest.reset();
        dataRequest.setStatementID("SEARCH_FOR_ORDERS");
        
        if(!isPartnerOrderNumber) {
        	dataRequest.addInputParam("confirmation_number", this.formatConfirmationNumber(confirmationNumber.trim()));
        } else {
        	dataRequest.addInputParam("confirmation_number", confirmationNumber.trim());
        }
        
        if(!partnerOrigin) {
        	dataRequest.addInputParam("order_origin", formatValueByOrderNumber(origin, confirmationNumber));
        } else {
        	dataRequest.addInputParam("order_origin", origin);
        }
        dataRequest.addInputParam("delivery_date", deliveryDate.trim());   
        dataRequest.addInputParam("buyers_last_name", buyerLastName.trim());
        dataRequest.addInputParam("buyers_phone_number", buyerPhone.trim());
        dataRequest.addInputParam("email", emailAddress.trim());
        dataRequest.addInputParam("ship_date", shipDate.trim());
        dataRequest.addInputParam("product_id", productCode.trim());
        dataRequest.addInputParam("recipients_last_name", recipientLastName.trim());
        dataRequest.addInputParam("recipients_phone_number", recipientPhone.trim());
        dataRequest.addInputParam("destination_type", shipToType);
        dataRequest.addInputParam("csr_id", csrId.trim());
        dataRequest.addInputParam("date_from", dateFrom.trim());
        dataRequest.addInputParam("date_to", dateTo.trim());
        dataRequest.addInputParam("mode", searchType);
        dataRequest.addInputParam("user_id", SecurityHelper.getUserId(request));
        dataRequest.addInputParam("sort_type", sortType);
        dataRequest.addInputParam("source_code", sourceCode);
        dataRequest.addInputParam("scrub_reason", scrubReason);
        dataRequest.addInputParam("product_property", formatValueByOrderNumber(productProperty, confirmationNumber));
        dataRequest.addInputParam("preferred_partner", searchPreferredPartner);
        dataRequest.addInputParam("include_being_scrubbed", "N");
        dataRequest.addInputParam("premier_circle_membership_id", "");

        try
        {
          if (logger.isDebugEnabled())
          {
            logger.debug("INSIDE retrieveResultMap");
            logger.debug("confirmation_number = " + this.formatConfirmationNumber(confirmationNumber.trim()));
            logger.debug("order_origin = " + formatValueByOrderNumber(origin, confirmationNumber));
            logger.debug("delivery_date = " + deliveryDate.trim());   
            logger.debug("buyers_last_name = " + buyerLastName.trim());
            logger.debug("buyers_phone_number = " + buyerPhone.trim());
            logger.debug("email = " + emailAddress.trim());
            logger.debug("ship_date = " + shipDate.trim());
            logger.debug("product_id = " + productCode.trim());
            logger.debug("recipients_last_name = " + recipientLastName.trim());
            logger.debug("recipients_phone_number = " + recipientPhone.trim());
            logger.debug("destination_type = " + shipToType);
            logger.debug("csr_id = " + csrId.trim());
            logger.debug("date_from = " + dateFrom.trim());
            logger.debug("date_to = " + dateTo.trim());
            logger.debug("mode = " + searchType);
            logger.debug("user_id = " + SecurityHelper.getUserId(request));
            logger.debug("sort_type = " + sortType);
            logger.debug("source_code = " + sourceCode);
            logger.debug("scrub_reason = " + scrubReason);
            logger.debug("product_property = " + formatValueByOrderNumber(productProperty, confirmationNumber));
            logger.debug("preferred_partner = " + searchPreferredPartner);
            logger.debug("include_being_scrubbed = " + "N");
          }
        }
        catch (Exception e)
        {}

        CachedResultSet searchResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest); 
        logger.debug("Total number of records returned = " + searchResultSet.getRowCount());
       
        // HashMap resultMap = new HashMap();

        SearchResultVO searchResult = null;
        
        if(searchResultSet != null && searchResultSet.getRowCount() > 0)
        {            
            while(searchResultSet.next())
            {
                // Result list display fields
                searchResult = new SearchResultVO();
                
                searchResult.setGuid((String) searchResultSet.getObject(1));
                searchResult.setCsrId((String) searchResultSet.getObject(4));
                searchResult.setMasterOrderNumber((String) searchResultSet.getObject(5));
                searchResult.setOrderDate((String) searchResultSet.getObject(6));
                searchResult.setItemCount((String) searchResultSet.getObject(7));
                searchResult.setOrderTotal((String) searchResultSet.getObject(8));
                searchResult.setBuyerLastName((String) searchResultSet.getObject(9));
                searchResult.setBuyerFirstName((String) searchResultSet.getObject(10));
                searchResult.setBuyerCity((String) searchResultSet.getObject(11));
                searchResult.setBuyerState((String) searchResultSet.getObject(12));
                searchResult.setPreferredProcessingPartner((String) searchResultSet.getObject(13));

                sortedResults.add(searchResult);
            }
        }
        
        return sortedResults;
    }

    /**
     * Returns a SearchResultVO object based on the order number passed in. The 
     * individual object returned is based on filtering criteria specified in 
     * the ScrubFilterBO object.
     * 
     * @param orderNumber String
     * @param request HttpServletRequest
     * @param dataRequest DataRequest
     * @exception Exception
     */
    public SearchResultVO retrieveByOrderNumber(String orderNumber, HttpServletRequest request, DataRequest dataRequest) throws Exception
    {
        logger.debug("INSIDE retrieveByOrderNumber");
        String securityToken = request.getParameter("securitytoken");
        String origin = request.getParameter("sc_origin");
        String productProperty = request.getParameter("sc_product_property");
        
        origin = getMercentOrigins(origin, dataRequest);
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	origin = hideTestOrders(origin);
    	}
        
        //For Partner Order - skip mercent order check if the given number is partner order number, include the category/origin for search.
        String originTemp =  request.getParameter("sc_origin");
        if("Y".equalsIgnoreCase(request.getParameter("sc_hide_test_orders")))
    	{
        	// Q4SP16-21 when Hide Test orders checkbox is checked
        	originTemp = hideTestOrders(originTemp);
    	}
        String[] originsList = getOriginsAsArrayOfStrings(originTemp);
        CachedResultSet partnerCrs = null;
        String sourceCode = null;
        boolean isPartnerOrderNumber = false;
        boolean partnerOrigin = false;
        PartnerUtility partnerUtil = new PartnerUtility();
        
        if(originsList == null || originsList.length > 1) {
        	partnerCrs = partnerUtil.getPartnerOrderInfoByOrderNumber(orderNumber, null, dataRequest.getConnection());         	
        } else {          	
        	partnerCrs = partnerUtil.getPartnerOrderInfoByOrderNumber(orderNumber, originsList[0], dataRequest.getConnection()); 
        	sourceCode = partnerCrs.getString("DEFAULT_SOURCE_CODE"); 
        	if(!StringUtils.isEmpty(originsList[0]) && sourceCode != null) {
    			PartnerMappingVO partnerMappingVO = partnerUtil.getPartnerOriginsInfo(originsList[0],sourceCode, dataRequest.getConnection());
    			if (partnerMappingVO != null && partnerMappingVO.isDefaultBehaviour()) {
    				partnerOrigin = true;
    			}
            }        	
        }
        
        if(partnerCrs != null && partnerCrs.next()){
        	orderNumber = partnerCrs.getString("MASTER_ORDER_NUMBER"); 
        	isPartnerOrderNumber = true;
        } 
        
        // Load list of order data from scrub
        dataRequest.reset();
        dataRequest.setStatementID("GET_FIRST_SCRUB_ORDER");
        if(!isPartnerOrderNumber) {
        	dataRequest.addInputParam("confirmation_number", this.formatConfirmationNumber(orderNumber.trim()));
        } else {
        	dataRequest.addInputParam("confirmation_number", orderNumber.trim());
        }
        if(!partnerOrigin) {
        	dataRequest.addInputParam("order_origin", formatValueByOrderNumber(origin, orderNumber));
        } else {
        	dataRequest.addInputParam("order_origin", origin);
        }
        dataRequest.addInputParam("delivery_date", null);   
        dataRequest.addInputParam("buyers_last_name", null);
        dataRequest.addInputParam("buyers_phone_number", null);
        dataRequest.addInputParam("email", null);
        dataRequest.addInputParam("ship_date", null);
        dataRequest.addInputParam("product_id", null);
        dataRequest.addInputParam("recipients_last_name", null);
        dataRequest.addInputParam("recipients_phone_number", null);
        dataRequest.addInputParam("destination_type", null);
        dataRequest.addInputParam("csr_id", null);
        dataRequest.addInputParam("date_from", null);
        dataRequest.addInputParam("date_to", null);
        dataRequest.addInputParam("mode", request.getParameter("sc_mode"));
        dataRequest.addInputParam("user_id", SecurityHelper.getUserId(request));
        dataRequest.addInputParam("sort_type", null);
        dataRequest.addInputParam("new_search_flag", "Y");
        dataRequest.addInputParam("source_code", null);
        dataRequest.addInputParam("scrub_reason", null);        
        dataRequest.addInputParam("product_property", formatValueByOrderNumber(productProperty, orderNumber));
        dataRequest.addInputParam("preferred_partner", null);
        dataRequest.addInputParam("premier_circle_membership_id", null);
        String orderGuid = (String) DataAccessUtil.getInstance().execute(dataRequest); 
        
        SearchResultVO searchResult = null;
        if(orderGuid != null && orderGuid.trim().length() > 0)
        {
            searchResult = new SearchResultVO();
            searchResult.setGuid(orderGuid);
        }

        return searchResult;
    }

    private String formatSearchDate(String inputDate)
    {
        String formattedDate = null;

        // Initial validation is done with client side javascript and this 
        // is only to convert to a 2 digit year format.
        if(inputDate != null)
        {
            try
            {
                if(inputDate.length() == 8)
                {
                    // Change to four-digit year format
                    formattedDate = inputDate.substring(0,6) + "20" + inputDate.substring(6,8);
                }
                else if(inputDate.length() == 10)
                {
                    // Correct format entered
                    formattedDate = inputDate;
                }
                else
                {
                    formattedDate = inputDate;
                }
            }
            catch(Exception e)
            {
                logger.error(e);
                formattedDate = inputDate;
            }
        }

        return formattedDate;
    }

    private String removeAllSpecialChars(String inStr)
    {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < inStr.length(); i++)
        {
            if(Character.isLetterOrDigit(inStr.charAt(i)))
            {
                sb.append(inStr.charAt(i));                
            }
        }
        
        return sb.toString();
    }


    /*
     * converts an Amazon and Walmart order IDs into a FTD master order number.
     */
    private String formatConfirmationNumber(String confirmationNumber) throws Exception
    {
    	//Check for the mercent orders in mercent schema and return master order number
        String masterOrderNumberForMercent = null;
        masterOrderNumberForMercent = getOrderDetailsByMercentOrdNumber(confirmationNumber); 
        if(masterOrderNumberForMercent != null) {
        	return masterOrderNumberForMercent;
        } 
        // Amazon check
        if (confirmationNumber.indexOf(this.AMAZON_ORDER_SEPARATOR) > 0)
        {
            StringBuffer returnValue = new StringBuffer();
            StringTokenizer st = new StringTokenizer(confirmationNumber,
                                                     this.AMAZON_ORDER_SEPARATOR);
           
           	returnValue.append(this.AMAZON_ORDER_PREFIX);
           
            
            while (st.hasMoreTokens()) 
            {
                returnValue.append(st.nextToken());
            }//end while
                
            return returnValue.toString();
        }
        // Normal confirmation number
        else 
        {
            return confirmationNumber;
        }
        
    }//end method formatConfirmationNumber(String)
    
    private String formatValueByOrderNumber(String value, String confirmationNumber)
    {
        // If confirmation number exists pass null for the value so value is 
        // ignored in search criteria
        if(confirmationNumber != null && confirmationNumber.trim().length() > 0)
        {
            return null;
        }
        else
        {
            return value;
        }
    }
    
    private boolean hasCharacters(String inString) 
    {
        char c;
    
        for (int i = 0; i < inString.length(); ) 
        {
            c =  inString.charAt(i);

            if (!Character.isDigit(c)) 
            {
                return true;
            }
            else 
            {
                i++;
            }
        }
        
        return false;
    }
    
    public SearchResultVO retrieveOrderBeingScrubbed(HttpServletRequest request, DataRequest dataRequest) throws Exception {

        logger.info("retrieveOrderBeingScrubbed()");

        String orderGuid = null;
        SearchResultVO searchResult = retrieveResult(request, dataRequest, true);
        
        if (searchResult != null) {
            orderGuid = searchResult.getGuid();
            logger.info("orderGuid: " + orderGuid);
            if (orderGuid != null) {
                logger.info("Calling GET_STATS_OSP");
                dataRequest.reset();
                dataRequest.setStatementID("GET_STATS_OSP");
                dataRequest.addInputParam("in_order_guid", orderGuid);
                Document xmlDoc = (Document) DataAccessUtil.getInstance().execute(dataRequest);

                searchResult = new SearchResultVO();
                searchResult.setOrderXML(xmlDoc);

                StringWriter s = new StringWriter();
                DOMUtil.print(xmlDoc, s);
                logger.info(s.toString());
            }
        }
        
        return searchResult;
    }
    
    private Map getAmazonOrderDetails(String confirmationNumber) throws Exception{
    	logger.debug("getAmazonOrderDetails()");

    	DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
        dataRequest.reset();
        dataRequest.setStatementID("GET_AMAZON_ORDER_DETAILS");
        dataRequest.addInputParam("IN_CONFIRMATION_NUMBER", confirmationNumber);
        DataAccessUtil dau = DataAccessUtil.getInstance();
        CachedResultSet rs = (CachedResultSet)dau.execute(dataRequest);
        rs.reset();
        HashMap orderDetailsMap = new HashMap();
        // Only one record (or none) should return.
        if (rs.next()) {
        	orderDetailsMap.put("principalAmt", rs.getBigDecimal("PRINCIPAL_AMT"));
        	orderDetailsMap.put("shippingAmt", rs.getBigDecimal("SHIPPING_AMT"));
        	orderDetailsMap.put("taxAmt", rs.getBigDecimal("TAX_AMT"));
        	orderDetailsMap.put("shippingTaxAmt", rs.getBigDecimal("SHIPPING_TAX_AMT"));
        }
        
        return orderDetailsMap;
    }
    
    private String getOrderDetailsByMercentOrdNumber(String confirmationNumber) {
    	
    	logger.debug("getMercentOrderDetails()");
    	DataRequest dataRequest = null;
    	
    	try {
    		
    		dataRequest = DataRequestHelper.getInstance().getDataRequest();
    		dataRequest.reset();

	        dataRequest.setStatementID("GET_MERCENT_ORDER_NUMBER");
	        dataRequest.addInputParam("IN_CHANNEL_ORDER_ID", confirmationNumber);
	        DataAccessUtil dau = DataAccessUtil.getInstance();
	        return (String)dau.execute(dataRequest);
	        
    	} catch (Exception e) {
			logger.error("Unable to get the resukt - getOrderDetailsByMercentOrdNumber");
		} finally {
			try {
				if(dataRequest != null && dataRequest.getConnection() != null && !dataRequest.getConnection().isClosed()) {				
					dataRequest.getConnection().close();
				}
			} catch (SQLException e) {
				logger.error("Unable to close the connction - getOrderDetailsByMercentOrdNumber");
			}			
		}
		return null;
    }
    
    @SuppressWarnings("unchecked")
	private String getMercentOrigins(String origin, DataRequest dataReq) throws Exception {
    	
    	String inputOrigin =  "(\'MERCENT\')";
    	String mercentOrigins = "(";
    	List<String> originsList = new ArrayList<String>();
    	if(origin.equals(inputOrigin)){
    		MercentOrderPrefixes mrcnMercentOrderPrefixes = new MercentOrderPrefixes(dataReq.getConnection());
    		originsList = mrcnMercentOrderPrefixes.getMercentOrigins();
    		for(int i = 0; i < originsList.size() ; i++) {
    			if(i!= 0) {
    				mercentOrigins = mercentOrigins + ",";
    			}
    			mercentOrigins = mercentOrigins + "\'" + originsList.get(i) + "\'";
    		}
    		mercentOrigins = mercentOrigins + ")";
    		origin = mercentOrigins;
    	}
    	return origin;
    }
     
    /**
     * This method parses the input string of origins separated by comma(,) and prepended/appended by single quote (') 
     * and returns an array of Strings of the origins 
     * @param origins Ex: "('FDIRECTI','CSPI','FUSAI')"
     * @return array of Strings Ex: ['FDIRECTI','CSPI','FUSAI']
     */
    private String [] getOriginsAsArrayOfStrings(String origins){
    	logger.debug("origins : "+origins);
    	String[] splitString=null;
    	if(origins!=null && !origins.equals("")){
    	splitString = (origins.split("\',\'"));
    	splitString[0] = splitString[0].substring(2);	
    	splitString[splitString.length-1] = splitString[splitString.length-1].substring(0,splitString[splitString.length-1].lastIndexOf('\''));
    	}
    	return splitString;    	
    }
    
    /**
     * @param dataRequest
     * @param request
     * @param removeCSREntities
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
	private ArrayList retrieveProSearchList(DataRequest dataRequest, HttpServletRequest request) throws Exception {
    	
    	CachedResultSet searchProFlowersResultSet = null;    	
    	ArrayList sortedResults = null;
    	
    	if(logger.isDebugEnabled()) {
    		logger.debug("retrieveProSearchList()");
    	}
    	
    	String proConfirmationNumber = request.getParameter("sc_pro_confirmation_number");
    	String searchType = request.getParameter("sc_search_type")!=null? request.getParameter("sc_search_type") :"S";
    	
       
    	String proConfNumberWithAppender = proConfirmationNumber.trim().concat(ConfigurationUtil.getInstance().getFrpGlobalParm("GLOBAL_CONFIG", "PRO_ORDER_SEARCH_APPENDER"));
    	
    	dataRequest.reset();
    	dataRequest.setStatementID("GET_PROFLOWERS_ORDERS");
		dataRequest.addInputParam("IN_WEB_ORDER_ID", proConfNumberWithAppender);
		dataRequest.addInputParam("IN_STATUS_FLAG", searchType);
		dataRequest.addInputParam("IN_CSR_ID",SecurityHelper.getUserId(request));    		
		dataRequest.addInputParam("IN_EXCLUDE_FLAG","Y"); 
		dataRequest.addInputParam("IN_INCLUDE_BEING_SCRUBBED","N"); 
		
		searchProFlowersResultSet = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);
		logger.debug("The row count for PRO search is: " +searchProFlowersResultSet.getRowCount());

		
		if(searchProFlowersResultSet != null && searchProFlowersResultSet.getRowCount() > 0) {      		
			SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
    		String dDate = null;
    		Calendar calDDate = null;
    		sortedResults = new ArrayList<SearchResultVO>();
    		
            while(searchProFlowersResultSet.next()) {
            	
            	dDate = searchProFlowersResultSet.getString("delivery_date");
            	if (dDate != null) {
            		calDDate = Calendar.getInstance();
            		calDDate.setTime(sdf.parse((dDate)));
            	}
            	
                // Result list display fields
            	SearchResultVO searchProFlowersResultSetVO = new SearchResultVO();                    
            	searchProFlowersResultSetVO.setGuid( searchProFlowersResultSet.getString("order_guid"));
            	searchProFlowersResultSetVO.setCsrId(searchProFlowersResultSet.getString("csr_id"));
            	searchProFlowersResultSetVO.setMasterOrderNumber( searchProFlowersResultSet.getString("master_order_number"));
            	searchProFlowersResultSetVO.setExternalOrderNumber(searchProFlowersResultSet.getString("external_order_number"));
            	searchProFlowersResultSetVO.setOrderDate(searchProFlowersResultSet.getString("order_date"));
            	searchProFlowersResultSetVO.setDeliveryDate(calDDate);
            	searchProFlowersResultSetVO.setPreferredProcessingPartner("ProFlowers");
            	searchProFlowersResultSetVO.setOrderTotal(searchProFlowersResultSet.getString("order_total"));
            	searchProFlowersResultSetVO.setBuyerCity(searchProFlowersResultSet.getString("city"));
            	searchProFlowersResultSetVO.setBuyerState(searchProFlowersResultSet.getString("STATE_PROVINCE"));
            	searchProFlowersResultSetVO.setBuyerFirstName(searchProFlowersResultSet.getString("buyer_first_name"));
            	searchProFlowersResultSetVO.setBuyerLastName(searchProFlowersResultSet.getString("buyer_last_name"));//PRODUCTS_TOTAL
            	searchProFlowersResultSetVO.setItemCount(searchProFlowersResultSet.getString("PRODUCTS_TOTAL"));//PRODUCTS_TOTAL 
            	searchProFlowersResultSetVO.setRecipFirstName(searchProFlowersResultSet.getString("recip_first_name"));
            	searchProFlowersResultSetVO.setRecipLastName(searchProFlowersResultSet.getString("recip_last_name"));
                sortedResults.add(searchProFlowersResultSetVO);
            }
        }
       	return sortedResults;
    }
    
    /**
     * @param dataRequest
     * @param request
     * @param removeCSREntities
     * @return
     * @throws Exception
     */
    @SuppressWarnings("rawtypes")
	private SearchResultVO retrieveProSearchOrder(DataRequest dataRequest, HttpServletRequest request) throws Exception {
    	
    	String proOrderGuid = null;  
    	SearchResultVO searchProFlowersResultSetVO = null;
    	
    	if(logger.isDebugEnabled()) {
    		logger.debug("retrieveProSearchOrder()");
    	}
    	
    	String proConfirmationNumber = request.getParameter("sc_pro_confirmation_number");
    	String searchType = !StringUtils.isEmpty(request.getParameter("sc_search_type")) ? request.getParameter("sc_search_type") :"S";
    	
       
    	String proConfNumberWithAppender = proConfirmationNumber.trim().concat(ConfigurationUtil.getInstance().getFrpGlobalParm("GLOBAL_CONFIG", "PRO_ORDER_SEARCH_APPENDER"));
    	
    	dataRequest.reset();
    	dataRequest.setStatementID("GET_PROFLOWERS_ORDER");
		dataRequest.addInputParam("IN_WEB_ORDER_ID", proConfNumberWithAppender);
		dataRequest.addInputParam("IN_STATUS_FLAG", searchType);
		dataRequest.addInputParam("IN_CSR_ID",SecurityHelper.getUserId(request));    		
		dataRequest.addInputParam("IN_EXCLUDE_FLAG","Y"); 
		dataRequest.addInputParam("IN_INCLUDE_BEING_SCRUBBED","N"); 
		
		proOrderGuid = (String) DataAccessUtil.getInstance().execute(dataRequest);
		
		 if(!StringUtils.isEmpty(proOrderGuid)) { 
        	searchProFlowersResultSetVO = new SearchResultVO();                    
        	searchProFlowersResultSetVO.setGuid(proOrderGuid);            
         }
       	 return searchProFlowersResultSetVO;
    }
    
    /** Method to remove TEST origin from the origin string
     * @param origin
     * @return origin
     */
    public String hideTestOrders(String origin)
    {
        origin = origin.replace("'TEST',", "");
    	
        return origin;
    }
    
}//end class