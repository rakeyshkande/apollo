package com.ftd.osp.orderscrub.util;

import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.math.BigDecimal;

import java.util.HashMap;

import java.sql.SQLException;

/**
 * This class is a singleton object used to store the sort order based off of 
 * the item status.
 *
 * @author Brian Munter
 */

public class StatusSortUtil extends HashMap
{
    private Logger logger;
    private static StatusSortUtil STATUS_SORT_UTIL;
    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.util.StatusSortUtil";

    private StatusSortUtil() throws Exception
    {
        logger = new Logger(LOGGER_CATEGORY);
        this.loadStatusSort();
    }

    /**
     * Returns an instance of StatusSortUtil.
     * 
     * @exception Exception
     */
    public static synchronized StatusSortUtil getInstance() throws Exception
    {
        if (STATUS_SORT_UTIL == null)
        {
            STATUS_SORT_UTIL = new StatusSortUtil();
            return STATUS_SORT_UTIL;
        }
        else 
        {
            return STATUS_SORT_UTIL;
        }
    }

    private void loadStatusSort()
    {  
        DataRequest dataRequest = null;
        
        try
        {
            dataRequest = DataRequestHelper.getInstance().getDataRequest();
            dataRequest.setStatementID("VIEW_STATUS_MAPPING");

            CachedResultSet statusSortResults = (CachedResultSet) DataAccessUtil.getInstance().execute(dataRequest);

            if(statusSortResults != null && statusSortResults.getRowCount() > 0)
            {
                while(statusSortResults.next())
                {
                    // Sort exists for this status
                    if(statusSortResults.getObject(2) != null)
                    {
                        // Put status and sort order in object
                        this.put((String) statusSortResults.getObject(1), ((BigDecimal) statusSortResults.getObject(3)).toString());
                    }
                }
            }
        }
        catch(Exception e)
        {
            try 
            {
                logger.error(e);
            }
            catch(Exception re) 
            {
                logger.error(re);
            }
        }
        finally
        {
            try 
            {
                if(dataRequest !=null && dataRequest.getConnection() != null)
                {
                    dataRequest.getConnection().close();
                }
            }
            catch(SQLException se) 
            {
                logger.error(se);
            }
        }
    }

    /**
     * Updates the sort orders stored within this object from the database.
     * 
     * @exception Exception
     */
    public void refresh() throws Exception
    {
        // Reload statuses
        this.loadStatusSort();
    }

}