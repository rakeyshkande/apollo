package com.ftd.osp.orderscrub.util;

import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.stats.OrderDetailStates;
import com.ftd.osp.utilities.stats.StatsUtil;
import com.ftd.osp.utilities.plugins.Logger;

import javax.servlet.http.HttpServletRequest;

import java.sql.Connection;

/**
 * This class is a singleton helper utility for recording statistics to the 
 * database for the Scrub system.
 *
 * @author Brian Munter
 */

public class StatsHelper 
{
    private static StatsHelper STATS_HELPER;
    private Logger logger;

    private StatsHelper()
    {
        logger = new Logger("com.ftd.osp.orderscrub.util.StatsHelper");
    }

    /**
     * Returns an instance of StatsHelper.
     * 
     * @exception Exception
     */
    public static synchronized StatsHelper getInstance() throws Exception
    {
        if (STATS_HELPER == null)
        {
            STATS_HELPER = new StatsHelper();
            return STATS_HELPER;
        }
        else 
        {
            return STATS_HELPER;
        }
    }

    /**
     * Updates the statistics for an individual item in the specified order.
     * 
     * @param order OrderVO
     * @param item OrderDetailsVO
     * @param baseState String
     * @param request HttpServletRequest
     * @exception Exception
     */
    public void updateItemStatistics(OrderVO order, OrderDetailsVO item, String baseState, HttpServletRequest request, Connection connection) throws Exception
    { 
        // Set stats with current csr id
        order.setCsrId(SecurityHelper.getUserId(request));

        // Pull correct stats state
        int state = this.retrieveOrderState(request.getParameter("sc_mode"), baseState);

        // Insert into database
        if(state != 0)
        {
            StatsUtil statsUtil = new StatsUtil();
            statsUtil.setOrder(order);        
            statsUtil.setItem(item);
            statsUtil.setState(state);
            statsUtil.setRelator(null);
            statsUtil.insert(connection);
        }
    }

    /**
     * Updates the statistics for all items in an order.
     * 
     * @param order OrderVO
     * @param baseState String
     * @param mode String
     * @exception Exception
     */
    public void updateOrderStatistics(OrderVO order, String baseState, HttpServletRequest request, Connection connection) throws Exception
    {   
        OrderDetailsVO item = null;
        for(int i=0; i < order.getOrderDetail().size(); i++)
        {
            item = (OrderDetailsVO) order.getOrderDetail().get(i);
            this.updateItemStatistics(order, item, baseState, request, connection);
        }
    }

    private int retrieveOrderState(String mode, String baseState)
    {
        int orderState = 0;

        if(baseState.equals(GeneralConstants.STATS_IN))
        {
            if(mode.equals("S"))
            {
                orderState = OrderDetailStates.SCRUB_IN;
            }
            else if(mode.equals("P"))
            {
                orderState = OrderDetailStates.PENDING_IN;
            }
            else if(mode.equals("R"))
            {
                orderState = OrderDetailStates.REINSTATE_IN;
            }
        }
        else if(baseState.equals(GeneralConstants.STATS_OUT))
        {
            if(mode.equals("S"))
            {
                orderState = OrderDetailStates.SCRUB_OUT;
            }
            else if(mode.equals("P"))
            {
                orderState = OrderDetailStates.PENDING_OUT;
            }
            else if(mode.equals("R"))
            {
                orderState = OrderDetailStates.REINSTATE_OUT;
            }
        }

        return orderState;
    }
}