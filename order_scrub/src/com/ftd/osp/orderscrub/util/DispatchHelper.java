package com.ftd.osp.orderscrub.util;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;

import org.w3c.dom.Document;

import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.GeneralConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;
import com.ftd.security.SecurityManager;
import com.ftd.security.cache.vo.UserInfo;

/**
 * This class is a helper utility for dispatching messages withing the JMS 
 * framework.
 *
 * @author Brian Munter
 */

public class DispatchHelper 
{
   //custom parameters returned from database procedure
    private static final String STATUS_PARAM = "RegisterOutParameterStatus";
    private static final String MESSAGE_PARAM = "RegisterOutParameterMessage";
    
    private static final String ORDER_EXTENSION_SCRUBBED_BY = "SCRUBBED_BY";
    private static final String ORDER_EXTENSION_SCRUBBED_ON = "SCRUBBED_ON";
    
    /** 
     * Constructor
     */
    public DispatchHelper()
    {
    }

    /**
     * Primary method which generates and dispatches a message within the JMS 
     * framework.  It also updates the statistic tracking in the database.
     * 
     * @param order OrderVO
     * @param item OrderDetailsVO
     * @param status String
     * @param request HttpServletRequest
     * @exception Exception
     */
    public void dispatchOrder(OrderVO order, OrderDetailsVO item, String status, HttpServletRequest request, Connection connection) throws Exception
    {
          String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
  
          //update the order to indicate who got the order out of scrub
          //emueller, 4/29/05, phaseIII
          udpateScrubbedInfo(connection,request, item.getOrderDetailId());
  
          if(standAloneFlag != null && standAloneFlag.equals("N"))
          {
              MessageToken token = new MessageToken(); 
              token.setMessage(this.generateMessage(order, item)); 
              token.setStatus(status); 
   
              Dispatcher dispatcher = Dispatcher.getInstance();
              InitialContext initContext = new InitialContext();
              dispatcher.dispatch(initContext, token);
          }
  
          // Record exit statistics
          StatsHelper.getInstance().updateItemStatistics(order, item, GeneralConstants.STATS_OUT, request, connection);     
    }

    /**
     * Returns true the order has already been dispatched out of scrub, false
     * if it needs to be dispatched
     * 
     * @param String orderDetailId
     * @return boolean 
     */
    public boolean isOrderAlreadyDispatched(long orderDetailId, Connection connection) throws Exception
    {
        String result = null;
        boolean isOrderAlreadyDispatched = false;
        DataRequest dataRequest = new DataRequest();
        
        HashMap messageMap = new HashMap();
        messageMap.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
        dataRequest.setConnection(connection);
        dataRequest.setStatementID("IS_ORDER_DTL_DISPATCHED_FULL");
        dataRequest.setInputParams(messageMap);
    
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        result = (String) dataAccessUtil.execute(dataRequest); 
        if (result != null && result.equalsIgnoreCase("Y"))
        {
          isOrderAlreadyDispatched =  true;
        }
        return isOrderAlreadyDispatched;
    }
    
    /**
     * Adds doubleDispatchXML to response document to throw error on shoppingCart.xml
     * 
     * @param String responseDocument
     * @return responseDocument 
     */
    public Document addOrderDoubleDispatchXML(Document responseDocument)
    {
      // Load data for display
      HashMap pageData = new HashMap();
      pageData.put("doubleDispatch", "This order has already been scrubbed by another user.  You will be redirected to the next applicable order.");

      // Add page data to xml
      DOMUtil.addSection(responseDocument, "error", "error", pageData, true);
      return responseDocument;
    }
    
    /**
     * Update the scrubbed by and on values for the order.
     * 
     * @param request
     * @param orderDetailId
     * @throws java.lang.Exception
     */
    private void udpateScrubbedInfo(Connection conn,HttpServletRequest request, long orderDetailId) throws Exception
    {
        
        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);        
        String csrId = null;        
        if(standAloneFlag != null && standAloneFlag.equals("Y"))
        {
            csrId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, "TEST_CSR");
        }    
        else
        {
            //get user information
            String securityToken = request.getParameter("securitytoken");
            SecurityManager securityManager = SecurityManager.getInstance();
            UserInfo userInfo = securityManager.getUserInfo(securityToken);      
            csrId = userInfo.getUserID();
        }        
        
        SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String formattedDate = dfOut.format(new java.util.Date());

        updateExtensionData(conn, orderDetailId,ORDER_EXTENSION_SCRUBBED_BY, csrId);
        updateExtensionData(conn, orderDetailId,ORDER_EXTENSION_SCRUBBED_ON, formattedDate);
        
    }
    
    
    
    /**
     * Update extension date on order
     * 
     * @param conn
     * @param orderDetailId
     * @param infoName
     * @param infoValue
     * @throws java.lang.Exception
     */
    private void updateExtensionData(Connection conn,long orderDetailId, String infoName, String infoValue) throws Exception
    {
        DataRequest dataRequest = new DataRequest();
        
        HashMap messageMap = new HashMap();
        
        messageMap.put("IN_ORDER_DETAIL_ID", new Long(orderDetailId));
        messageMap.put("IN_INFO_NAME",infoName);
        messageMap.put("IN_INFO_DATA",infoValue);
        
        dataRequest.setConnection(conn);
        dataRequest.setStatementID("SCRUB.INSERT_ORDER_DETAIL_EXT");
        dataRequest.setInputParams(messageMap);
        
        DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        
        Map outputs = (Map) dataAccessUtil.execute(dataRequest); 
        String status = (String)outputs.get(STATUS_PARAM);
        if(status.equals("N") )
        {
         String message = (String)outputs.get(MESSAGE_PARAM);
         throw new Exception(message.toString());   
        }                
    }
    

    private String generateMessage(OrderVO order, OrderDetailsVO item)
    {
        return order.getGUID() + "/" + item.getLineNumber();
    }

}