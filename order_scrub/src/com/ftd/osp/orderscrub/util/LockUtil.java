package com.ftd.osp.orderscrub.util;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.security.SecurityManager;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.cache.vo.UserInfo;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * This class handles the locking and unlocking of records
 */
public class LockUtil 
{

    private final static String LOGGER_CATEGORY = "com.ftd.osp.orderscrub.util.LockUtil";
    public final static String BUYER_LOCK = "CUSTOMER";

    public LockUtil()
    {
    }
    
    /**
     * This procedure will attempt to get a lock.  If the lock was obtained a 
     * null will be returned.  If a lock was not obtained the userid of whoever
     * currently has the lock will be returned.
     */

    public static String getLock(Connection conn,String entityType, String entityId, String securityToken, String csrId)
        throws Exception
    {
    
        //while running in test mode set default values
        String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
        if(standAloneFlag != null && standAloneFlag.equals("Y"))
        {
            securityToken = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, "TEST_TOKEN");
            csrId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, "TEST_CSR");
        }
    
    
         Logger logger = new Logger(LOGGER_CATEGORY);
         logger.debug("Requesting lock. EntityType="+entityType+" EntityId="+entityId+" csrId="+csrId+" Token=" + securityToken);
         
         String lockedUser = "";
         
           DataRequest dataRequest = new DataRequest();
           
           HashMap messageMap = new HashMap();
          
           messageMap.put("IN_ENTITY_TYPE", entityType);
           messageMap.put("IN_ENTITY_ID",entityId);
           messageMap.put("IN_SESSION_ID",securityToken);
           messageMap.put("IN_CSR_ID", csrId);
           messageMap.put("IN_ORDER_LEVEL", new String());
           
           
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("UPDATE_CSR_LOCKED_ENTITIES");
            dataRequest.setInputParams(messageMap);
      
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);

            //check if DB error occured
            if (!outputs.get("OUT_STATUS").equals("Y"))
            {
                throw new Exception((String)outputs.get("OUT_ERROR_MESSAGE"));
            }

            //check if a lock was obtained
            String lockObtained = (String)outputs.get("OUT_LOCK_OBTAINED");
            if( lockObtained != null && lockObtained.equals("N")  )
            {
                lockedUser = (String)outputs.get("OUT_LOCKED_CSR_ID");
                logger.debug("Item already locked by " + lockedUser);
            }
            else
            {
                logger.debug("Lock obtained.");
            }
         
          return lockedUser;
    
    }

    public static void releaseBuyerLock(Connection conn,OrderVO order, HttpServletRequest request) throws Exception
    {    
    

    
        if(order.getBuyer() != null && order.getBuyer().size() > 0){
        
        
            BuyerVO buyerVO = (BuyerVO)order.getBuyer().get(0);
            
            if(buyerVO.getCleanCustomerId() != null){
            
                String cleanBuyerId = buyerVO.getCleanCustomerId().toString();   
               
                String securityToken = null;
                String csrId = null;
                
                //while running in test mode set default values
                String standAloneFlag = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, ConfigurationConstants.STANDALONE_FLAG);
                if(standAloneFlag != null && standAloneFlag.equals("Y"))
                {
                    securityToken = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, "TEST_TOKEN");
                    csrId = ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.SCRUB_CONFIG_FILE, "TEST_CSR");
                }
                else
                {
                    //get user information
                    securityToken = request.getParameter("securitytoken");
                    SecurityManager securityManager = SecurityManager.getInstance();
                    UserInfo userInfo = securityManager.getUserInfo(securityToken);      
                    csrId = userInfo.getUserID();
                }
                
                //release buyer record lock
                LockUtil.releaseLock(conn,LockUtil.BUYER_LOCK, cleanBuyerId, securityToken, csrId);
            }
         }   
    }
    
    
    private static void releaseLock(Connection conn,String entityType, String entityId, String securityToken, String csrId)
        throws Exception
    {
    
         Logger logger = new Logger(LOGGER_CATEGORY);
         logger.debug("Releasing lock. EntityType="+entityType+" EntityId="+entityId+" csrId="+csrId+" Token=" + securityToken);
         
         String lockedUser = null;
         
           DataRequest dataRequest = new DataRequest();
           
           HashMap messageMap = new HashMap();
          
           messageMap.put("IN_ENTITY_TYPE", entityType);
           messageMap.put("IN_ENTITY_ID",entityId);
           messageMap.put("IN_SESSION_ID",securityToken);
           messageMap.put("IN_CSR_ID", csrId);
           
            dataRequest.setConnection(conn);
            dataRequest.setStatementID("DELETE_CSR_LOCKED_ENTITIES");
            dataRequest.setInputParams(messageMap);
      
            DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
 
            Map outputs = (Map) dataAccessUtil.execute(dataRequest);
            String status = (String)outputs.get("OUT_STATUS");
            if( status.equals("N")  )
            {
               String message = (String) outputs.get("OUT_MESSAGE");
               
               //Only throw an error if the text contains the word Exception,
               //in all other cases an application/sql exception did not occur.
               //For example the message will contain text when an attempt is made
               //to release a lock which the current user does not have.
               if(message != null)
               {
                   if(message.indexOf("Exception") > 0)
                   {
                       throw new Exception(message);
                   }
               }
            }
          
    
    }    
    
    
    
}