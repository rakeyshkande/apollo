package com.ftd.osp.orderscrub.util;

import com.ftd.osp.orderscrub.constants.ConfigurationConstants;
import com.ftd.osp.orderscrub.constants.CustomMessageConstants;
import com.ftd.osp.orderscrub.util.DataRequestHelper;
import com.ftd.osp.ordervalidator.OrderValidator;
import com.ftd.osp.ordervalidator.vo.OrderValidationNode;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.RosesDotComUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.sql.Connection;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;


/**
 * This class is a helper utility for pulling specific objects from within an
 * order.
 *
 * @author Brian Munter
 */

public class OrderHelper 
{
    /** 
     * Constructor
     */
    public OrderHelper()
    {
    }

    /**
     * Returns an individual item from the order based on the item line number.
     * 
     * @param order OrderVO
     * @param lineNumber String
     * @exception Exception
     */
    public OrderDetailsVO getItem(OrderVO order, String lineNumber) throws Exception
    {
        OrderDetailsVO item = null;
        List itemList = order.getOrderDetail();

        if(itemList != null && itemList.size() > 0)
        {
            OrderDetailsVO currentItem = null;
            for(int i = 0; i < itemList.size(); i++)
            {
                item = (OrderDetailsVO) itemList.get(i);
                if(item.getLineNumber() != null && item.getLineNumber().equals(lineNumber))
                {
                    return item;
                }
            }
        }

        return null;
    }

    /**
     * Returns the buyer for the order.
     * 
     * @param order OrderVO
     * @exception Exception
     */
    public BuyerVO getBuyer(OrderVO order) throws Exception
    {
        BuyerVO buyer = null;
        List buyerList = order.getBuyer();

        if(buyerList != null && buyerList.size() > 0)
        {
            return (BuyerVO) buyerList.get(0);
        }

        return null;
    }

    /**
     * Returns the primary email address for the buyer of the order.
     * 
     * @param order OrderVO
     * @exception Exception
     */
    public String getPrimaryBuyerEmail(OrderVO order) throws Exception
    {
        BuyerEmailsVO emailAddress = null;
        
        BuyerVO buyer = this.getBuyer(order);
        if(buyer != null)
        {
            if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() > 0)
            {
                for(int i = 0; i < buyer.getBuyerEmails().size(); i++)
                {
                    emailAddress = (BuyerEmailsVO) buyer.getBuyerEmails().get(i);
                    
                    if(emailAddress != null && emailAddress.getPrimary() != null && emailAddress.getPrimary().equals("Y"))
                    {
                        return emailAddress.getEmail();
                    }
                }
            }
        }
        
        return null;
    }

    public RecipientAddressesVO getRecipientAddress(OrderDetailsVO item) throws Exception
    {
        RecipientsVO recipient = null;
        RecipientAddressesVO recipientAddresses = null;
        
        if(item.getRecipients() != null && item.getRecipients().size() > 0)
        {
            recipient = (RecipientsVO) item.getRecipients().get(0);

            if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
            {
                recipientAddresses = (RecipientAddressesVO) recipient.getRecipientAddresses().get(0);
            }
        }
    
        return recipientAddresses;
    }

    public boolean isCompleteOrder(OrderVO order)
    {
        boolean completeOrder = true;
        OrderDetailsVO item = null;
        
        for(int i=0; i < order.getOrderDetail().size(); i++)
        {
            item = (OrderDetailsVO) order.getOrderDetail().get(i);
            if(item.getStatus() != null 
                && !item.getStatus().equals("2006") 
                && !item.getStatus().equals("2004"))
            {
                completeOrder = false;
                break;
            }
        }

        return completeOrder;
    }
    
    /**
     * Determine if the order is military card payment
     **/
    public boolean hasMilitaryStarPayment(OrderVO order) throws Exception
    {
       List paymentList = order.getPayments();
       PaymentsVO payment = null;
       Iterator paymentIterator = paymentList.iterator();
       boolean hasMilitaryStarPayment = false;
       
       while(paymentIterator.hasNext())
       {
           payment = (PaymentsVO) paymentIterator.next();
           if ("MS".equals(payment.getPaymentType())) {
               hasMilitaryStarPayment = true;
               break;
           }
       }
       return hasMilitaryStarPayment;
    }//end method hasMilitaryStarPayment
    
     /**
      * Determine if the order is of ariba origin.
      **/
     public boolean isAribaOrder(OrderVO order) throws Exception
     {
         String originType = order.getOrderOrigin();
         
         if (originType == null) {
            originType = "";
         }
         return (originType.equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.METADATA_CONFIG_FILE, ConfigurationConstants.ORDER_ORIGIN_ARIBA1))
         || originType.equalsIgnoreCase(ConfigurationUtil.getInstance().getProperty(ConfigurationConstants.METADATA_CONFIG_FILE, ConfigurationConstants.ORDER_ORIGIN_ARIBA2)));
     }//end method isAribaOrder        
     
      /**
       * Retrieves the alt pay paymen type
       * Returns payment type if the order is of alt pay paymen type. Return null otherwise.
       * in ftd_apps.payment_methods
       **/
       public String getAltPayPaymentType(OrderVO order) throws Exception
       {
           Iterator it = null;
           Collection payments = order.getPayments();
           ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
           String paymentTypeAP = configUtil.getProperty(ConfigurationConstants.METADATA_CONFIG_FILE, "PAYMENT_TYPE_ALT_PAY");
       
           if(payments != null && payments.size() > 0)
           {
               // Get the total of all gift certificates
               it = payments.iterator();
               PaymentsVO payment = null;
               while(it.hasNext())
               {
                   payment = (PaymentsVO)it.next();
                   if (paymentTypeAP.equals(payment.getPaymentMethodType())) {
                       return payment.getPaymentType();
                   }
               }
           }
           return null;
       }//end method getAltPayPaymentType    

        /**
         * Returns a list of payments with given payment method id for the order.
         * 
         **/
         public List getPaymentsByMethodType(OrderVO order, String paymentMethodType)
         {
             Iterator it = null;
             Collection payments = order.getPayments();
             List paymentList = new ArrayList();
             
             if(payments != null && payments.size() > 0)
             {
                 // Get the total of all gift certificates
                 it = payments.iterator();
                 PaymentsVO payment = null;
                 while(it.hasNext())
                 {
                     payment = (PaymentsVO)it.next();
                     if (payment != null && paymentMethodType.equals(payment.getPaymentMethodType())) {
                         paymentList.add(payment);
                     }
                 }
             }
             return paymentList;
         }//end method getPaymentsByType 
    
  /**********************************************************************************************
  * Determine if the order is fully (or at least partially billed).  If yes, return true. Else
  * return false. 
  **********************************************************************************************/
  public boolean isCartBilled(OrderVO oVO) throws Exception
  {
    // Initialize data request
    DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
    dataRequest.reset();
    dataRequest.setStatementID("IS_CART_BILLED");
    dataRequest.addInputParam("IN_ORDER_GUID", oVO.getGUID());

    DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
    String searchResults = (String) dataAccessUtil.execute(dataRequest);

    boolean cartBilled = searchResults.equalsIgnoreCase("Y")?true:false; 

    return cartBilled; 

  } //end method isOrderItemCartBilled        


  /**********************************************************************************************
  * Return the message for when the order is fully (or at least partially) billed.
  **********************************************************************************************/
  public String getCartBilledMessage() throws Exception
  {
    //retrieve the mesage
    String message = 
      ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, 
                                                  CustomMessageConstants.BLOCK_REINSTATE_ORDER_BILLED);

    return message; 

  } //end method getCartBilledMessage
  

  /**********************************************************************************************
  * Determine if the order date is less than (current date - allowance), and if so, return true. 
  * Else return false. 
  **********************************************************************************************/
  public boolean isOrderDateBlockReinstate(OrderVO oVO, int allowance) throws Exception
  {
    /**retrieve and massage the order date**/
    String sOrderDate = oVO.getOrderDate(); 

  	//Define the format
    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

    //Create a Calendar Object
    Calendar cOrderDate = Calendar.getInstance();

    //Set the Calendar Object using the date retrieved in sOrderDate
    cOrderDate.setTime(sdf.parse(sOrderDate));

		//set date time to 00:00:00
		cOrderDate.set(Calendar.HOUR, 0);
		cOrderDate.set(Calendar.MINUTE, 0);
		cOrderDate.set(Calendar.SECOND, 0);
		cOrderDate.set(Calendar.MILLISECOND, 0);
		cOrderDate.set( Calendar.AM_PM, Calendar.AM );
    
    /**set a Calendar object that will represent (current date - allowance)**/
    Calendar cDateMinusAllowance = Calendar.getInstance();

		//set date time to 00:00:00
		cDateMinusAllowance.set(Calendar.HOUR, 0);
		cDateMinusAllowance.set(Calendar.MINUTE, 0);
		cDateMinusAllowance.set(Calendar.SECOND, 0);
		cDateMinusAllowance.set(Calendar.MILLISECOND, 0);
		cDateMinusAllowance.set( Calendar.AM_PM, Calendar.AM );

		//subtract the allowance from the current date
    cDateMinusAllowance.add(cDateMinusAllowance.DATE, -(allowance));    

    /**get difference in days**/
    int diffInDays = getDiffInDays(cDateMinusAllowance.getTime(), cOrderDate.getTime());

    boolean orderDateLess = diffInDays<0?true:false;
    
    return orderDateLess; 

  } //end method isOrderItemCartBilled        


  /**********************************************************************************************
  * Return the message for when the order date is less than (current date - allowance)
  **********************************************************************************************/
  public String getOrderDateBlockReinstateMessage(String days) throws Exception
  {
    //retrieve the message
    String message = 
      ConfigurationUtil.getInstance().getProperty(CustomMessageConstants.CUSTOM_MESSAGE_FILE, 
                                                  CustomMessageConstants.BLOCK_REINSTATE_ORDER_DATE);

    //replace the token with the days as per Global parms: DAYS_LOCK_PENDING_REMOVED_ORDER
    message = StringUtils.replace(message,
                        CustomMessageConstants.BLOCK_REINSTATE_ORDER_DATE_REPLACE_TOKEN,
                        days);
                      
    return message; 

  } //end method getCartBilledMessage        


  /*************************************************************************************************************
   * Returns the difference in days between the startDate and endDate.
   * 
   * @param startDate
   * @param endDate
   * @return The difference in days between the startDate and endDate
   * @throws IllegalArgumentException if either startDate or endDate are null
   *************************************************************************************************************/
  public static int getDiffInDays(Date startDate, Date endDate) {

    // null check inputs
    if ( startDate == null || endDate == null ) {
      throw new IllegalArgumentException("Invalid parameter: one or both of startDate='" + startDate + "', endDate='" + endDate + "' were null.");
    }

    long diffInMillis = endDate.getTime() - startDate.getTime();

    int diffInDays = (int)(diffInMillis / DateUtils.MILLIS_PER_DAY);

    return diffInDays;
  }
  
  /** Method to get the custom message for blocking GD orders to be reinstated
	 * @return
	 * @throws Exception
	 */
	public String getGDReInstateMessage() throws Exception {
		String message = ConfigurationUtil.getInstance().getProperty(
				CustomMessageConstants.CUSTOM_MESSAGE_FILE, CustomMessageConstants.BLOCK_REINSTATE_GIFTCARD_ORDER);
		return message;
	}
	
	/**
	 * Returns a list of payments with given payment type for the order.
	 * 
	 **/
	public List getPaymentsByType(OrderVO order, String paymentType) {
		List<PaymentsVO> payments = order.getPayments();
		List<PaymentsVO> paymentList = new ArrayList<PaymentsVO>();
		for (PaymentsVO paymentsVO : payments) {
			if (paymentsVO != null && paymentType.equals(paymentsVO.getPaymentType())) {
				paymentList.add(paymentsVO);
			}
		}		
		return paymentList;
	} 
	
	/**
	 * Checks is any of the order items in scrub is not in removed state - 2004
	 * 
	 * @param orderGUID
	 * @return 
	 */
	public boolean areAllItemsRemoved(String orderGUID) throws Exception {
		DataRequest dataRequest = DataRequestHelper.getInstance().getDataRequest();
		String searchResults;
		try {
		dataRequest.reset();
		dataRequest.setStatementID("ARE_ALL_ITEMS_REMOVED");
		dataRequest.addInputParam("IN_ORDER_GUID", orderGUID);

		DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();		
		searchResults = (String) dataAccessUtil.execute(dataRequest);
		
		} catch(Exception e) {			
			throw new Exception("Error caught:" + e);
		} finally {
			dataRequest.getConnection().close();
		}
				
		boolean cartBilled = "Y".equals(searchResults) ? true : false;
		
		return cartBilled;
	}

	public OrderValidationNode validateOrder(OrderVO order, Connection conn, 
	                                         boolean isAmazonOrder, boolean isPartnerOrder, boolean isMercentOrder, boolean isRoses) throws Exception {
        OrderValidator orderValidator = new OrderValidator();
        OrderValidationNode orderValidationNode = null;
        if(isAmazonOrder) {
          orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_AMAZON, conn);  
        } else if(isMercentOrder) {
          orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_MERCENT, conn);
        } else if(isPartnerOrder) {
            orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_PARTNER, conn);
        } else if(isRoses) {
            orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_ROSES, conn);
        } else {
          orderValidationNode = orderValidator.validateOrder(order, OrderValidator.VALIDATION_SCRUB, conn);
        }
	    return orderValidationNode;
	}


}
