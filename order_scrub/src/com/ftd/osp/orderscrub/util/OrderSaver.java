package com.ftd.osp.orderscrub.util;

import com.ftd.osp.orderscrub.presentation.util.SecurityHelper;
import com.ftd.osp.utilities.ObjectCopyUtil;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;

import java.sql.Connection;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * This class is a utility to facilitate the saving of an order to the database. 
 * All business processing in this class is done off a clone to prevent data 
 * corruption on the main order object.
 *
 * @author Brian Munter
 */

public class OrderSaver 
{
    private ObjectCopyUtil objectCopy = null;
    private ScrubMapperDAO scrubMapperDAO = null;

    public static final String EMPTY_STRING = "";

    /** 
     * Constructor
     * 
     * @param connection Connection
     */
    public OrderSaver(Connection connection)
    {
        // Initialize utilities and dao
        objectCopy = new ObjectCopyUtil();
        scrubMapperDAO = new ScrubMapperDAO(connection);
    }

    /**
     * Primary method which saves the order to the Scrub schema. This method 
     * contains logic to distinguish whether fraud data should be 
     * saved, whether it is a single item to be saved ot the entire cart, and 
     * also executes the DataFixer utility before saving to FRP.
     * 
     * @param order OrderVO
     * @param item OrderDetailsVO
     * @param includeFraud boolean
     * @exception Exception
     */
    public void saveOrder(OrderVO order, OrderDetailsVO item, boolean includeFraud, HttpServletRequest request) throws Exception
    {
        // Update order with current user
        order.setCsrId(SecurityHelper.getUserId(request));
    
        // Create a working copy of the order
        OrderVO orderCopy = (OrderVO) objectCopy.deepCopy(order);

        // Check to save fraud data
        if(!includeFraud)
        {
            orderCopy.setFraudComments(null);
            orderCopy.setFraudFlag("N");
        }
    
        // Update order with individual item
        if(item != null)
        {
            List singleItemList = new ArrayList();
            singleItemList.add((OrderDetailsVO) objectCopy.deepCopy(item));
            orderCopy.setOrderDetail(singleItemList);
        }

        // Update scrub database
        boolean scrubSuccess = scrubMapperDAO.updateOrder(orderCopy);
    }
}