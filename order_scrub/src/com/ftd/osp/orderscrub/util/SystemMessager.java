package com.ftd.osp.orderscrub.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ftd.osp.orderscrub.constants.GDAuthClearConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.systemmessenger.SystemMessenger;
import com.ftd.osp.utilities.systemmessenger.SystemMessengerVO;
import com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException;
/**
 * Sends system messages to the system messages table
 * 
 * @author Christy Hu
 * @version 1.0
 */

public class SystemMessager 
{
    public static final int LEVEL_DEBUG = SystemMessengerVO.LEVEL_DEBUG;
    public static final int LEVEL_PRODUCTION = SystemMessengerVO.LEVEL_PRODUCTION;
    private static Logger logger  = new Logger("com.ftd.accountingreporting.util.SystemMessager");

 /**
   * Logs a message in the system messages table. No subject messages
   * will default to 'System Message' in the subject of page.
   * 
   * @param message
   * @param source
   * @param level
   * @param type
   * @param connection
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
    public static void send(String message
                          , Throwable exception
                          , String source
                          , int level
                          , String type
                          , Connection connection) 
      throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException
    {    
        try {
            send(message, exception, source, level, type, "", connection);
        } catch (Exception e) {
            logger.error(e);
            throw new SystemMessengerException(e.getMessage());
        }
    }

 /**
   * Logs a message in the system messages table
   * The paging system looks for source of 'End of Day'.
   * For any messages with that souce, it will send a nopage email rather than
   * paging or include in the no page summary.
   * @param message
   * @param source
   * @param level
   * @param type
   * @param subject
   * @param connection
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws com.ftd.osp.utilities.systemmessenger.exception.SystemMessengerException
   */
    public static void send(String message
                          , Throwable exception
                          , String source
                          , int level
                          , String type
                          , String subject
                          , Connection connection) 
      throws SAXException, ParserConfigurationException, IOException, SQLException, SystemMessengerException, Exception
    {
        
        // create system message from the passed in message and the exception 
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        if(exception!=null){
           exception.printStackTrace(pw);
        }
        String logMessage = "";
        if(message != null && !message.equals("")){
            logMessage = message + "\n" + sw.toString();
        }
        else
        {
            logMessage = sw.toString();    
        }
        pw.close();  
        
        // create the System Messenger VO and send out the system message 
        SystemMessengerVO systemMessengerVO = new SystemMessengerVO();
        systemMessengerVO.setLevel(level);
        systemMessengerVO.setSource(source);
        systemMessengerVO.setType(type);
        systemMessengerVO.setSubject(subject);
        systemMessengerVO.setMessage(logMessage);
        String result = SystemMessenger.getInstance().send(systemMessengerVO, connection, false);
        if(result != null) {
            logger.info("result is:" + result);
        }
        /*
        if(result != null) {
            throw new Exception("Inserting system message failed.");
        }
        */
    }
    
    /**
     * Send a generic system message page.
     * @param con
     * @param t
     */
    public static void sendSystemMessage(Connection con, Throwable t)
    {
        sendSystemMessage(con, GDAuthClearConstants.GD_AUTH_CLEAR_SOURCE, t);
    } 
    
    /**
     * Send a generic system message page for given source.
     * @param con
     * @param t
     */
    public static void sendSystemMessage(Connection con, String source, Throwable t)
    {
        if(logger.isDebugEnabled()) {
            logger.debug("Entering sendSystemMessage");
        }
        try {
            String logMessage = t.getMessage();  
      
            send(logMessage, t, source, 
                SystemMessager.LEVEL_PRODUCTION, 
                GDAuthClearConstants.GD_AUTH_CLEAR_ERROR_TYPE, con);
        
        } catch (Exception ex) {
            logger.error(ex);
        } finally {
            if(logger.isDebugEnabled()){
                logger.debug("Exiting sendSystemMessage");
            } 
        }   
    }    
}