package com.ftd.osp.orderscrub.vo;

import java.math.BigDecimal;

/** This class holds all the fields that are required to perform GD authorization clear.
 * @author smeka
 *
 */
public class GDAuthClearRequestVO {

	private String orderGuid;

	private String paymentId;
	// amount in DB
	private BigDecimal paymentAmt;
	// amount to send
	private BigDecimal transactionAmt;
	// Authorization number
	private String authNumber;
	// Represents the users GiftCard number
	private String accountNumber;
	// Represents the users PIN number
	private String pin;
	// Represents the actual transaction ID for calls to SVS (needed by SVS for
	// dup-check processing)
	private String captureTxt;

	private String clientId;

	private String clientPassword;

	private String serviceUrl;

	private int retryCount;

	private long delayTime;

	private String status;

	private boolean perform;

	private boolean sendSysMessage;

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public long getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(long delayTime) {
		this.delayTime = delayTime;
	}

	public int getCurrentCount() {
		return currentCount;
	}

	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}

	private int currentCount;

	public String getOrderGuid() {
		return orderGuid;
	}

	public void setOrderGuid(String orderGuid) {
		this.orderGuid = orderGuid;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public BigDecimal getPaymentAmt() {
		return paymentAmt;
	}

	public void setPaymentAmt(BigDecimal paymentAmt) {
		this.paymentAmt = paymentAmt;
	}

	public BigDecimal getTransactionAmt() {
		return transactionAmt;
	}

	public void setTransactionAmt(BigDecimal transactionAmt) {
		this.transactionAmt = transactionAmt;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getCaptureTxt() {
		return captureTxt;
	}

	public void setCaptureTxt(String captureTxt) {
		this.captureTxt = captureTxt;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientPassword() {
		return clientPassword;
	}

	public void setClientPassword(String clientPassword) {
		this.clientPassword = clientPassword;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public boolean isPerform() {
		return perform;
	}

	public void setPerform(boolean perform) {
		this.perform = perform;
	}

	public boolean isSendSysMessage() {
		return sendSysMessage;
	}

	public void setSendSysMessage(boolean sendSysMessage) {
		this.sendSysMessage = sendSysMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
