package com.ftd.osp.orderscrub.vo;

/** Class defines all the possible SVS response codes and error messages.
 *  
 * @author smeka
 *
 */
public class GDAuthClearResponseVO {
	
	private String errorCode;
	
	private String errorMessage;
	
	private String responseCode;
	
	private String responseMessage;
	
	private boolean isSuccess;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}	
	
}



