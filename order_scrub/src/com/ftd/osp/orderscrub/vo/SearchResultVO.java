/*
 *	Class ScrubFilterBO
 */
 package com.ftd.osp.orderscrub.vo;

import java.util.Calendar;

import org.w3c.dom.Document;

/**
 * Class SearchResultVO holds scrub order search results for
 * sorting in ScrubFilterBO
 *
 * @author Tim Peterson
 *
 */
public class SearchResultVO 
{
	private String guid;
	private String origin;
	private String shipMethod;
	private int timeZone;
	private Calendar shipDate;
	private Calendar deliveryDate;
    private String masterOrderNumber;
    private String externalOrderNumber;
    private String buyerFirstName;
    private String buyerLastName;
    private String RecipFirstName;
    private String RecipLastName;
    private String buyerCity;
    private String buyerState;
    private String orderDate;
    private String itemCount;
    private String orderTotal;
    private String csrId;
    private String sortOrder;
    private String orderStatus;
    private String preferredProcessingPartner;
    private Document orderXML;

	public SearchResultVO()
	{
		guid="";
		origin="";
		shipMethod=null;
		timeZone=0;     //east coast
		shipDate = Calendar.getInstance();
		deliveryDate = Calendar.getInstance();
	}

	public String getGuid()
	{
	    return guid;
	}

	public void setGuid(String sGuid)
	{
	  if( sGuid!=null )
	    this.guid = sGuid;
	}

	public String getOrigin()
	{
	    return origin;
	}
	
	public void setOrigin(String sOrigin)
	{
        if( sOrigin!=null )
            this.origin = sOrigin;
	}
	
	public String getShipMethod()
	{
        return shipMethod;
	}
	
    public void setShipMethod(String sShipMethod) 
    {
        this.shipMethod=sShipMethod;
    }
	
	public int getTimeZone() 
    {
        return timeZone;
	}
	
	public void setTimeZone(int iTimeZone)
	{
        if( iTimeZone>-1 )
            this.timeZone=iTimeZone;
        else
            this.timeZone=0;
	}
	
    public void setTimeZone(Integer iTimeZone)
    {
        if( iTimeZone==null )
            this.timeZone=0;
        else if( iTimeZone.intValue()>-1 )
            this.timeZone=iTimeZone.intValue();
        else
            this.timeZone=0;
	}

	public Calendar getShipDate() 
	{
        return shipDate;
	}
	
    public void setShipDate(Calendar cShipDate) 
	{
        if( cShipDate==null )
            this.shipDate=Calendar.getInstance();
        else
	  		this.shipDate=cShipDate;
	}
	
	public Calendar getDeliveryDate() 
	{
        return deliveryDate;
	}
	
	public void setDeliveryDate(Calendar cDeliveryDate)
	{
        if( cDeliveryDate==null )
            this.deliveryDate=Calendar.getInstance();
        else
	  		this.deliveryDate=cDeliveryDate;
	}

    public String getMasterOrderNumber()
    {
        return masterOrderNumber;
    }

    public void setMasterOrderNumber(String newMasterOrderNumber)
    {
        masterOrderNumber = newMasterOrderNumber;
    }

    public String getExternalOrderNumber() {
		return externalOrderNumber;
	}

	public void setExternalOrderNumber(String newExternalOrderNumber) {
		externalOrderNumber = newExternalOrderNumber;
	}

	public String getBuyerFirstName()
    {
        return buyerFirstName;
    }

    public void setBuyerFirstName(String newBuyerFirstName)
    {
        buyerFirstName = newBuyerFirstName;
    }

    public String getBuyerLastName()
    {
        return buyerLastName;
    }

    public void setBuyerLastName(String newBuyerLastName)
    {
        buyerLastName = newBuyerLastName;
    }

    public String getRecipFirstName() {
		return RecipFirstName;
	}

	public void setRecipFirstName(String recipFirstName) {
		RecipFirstName = recipFirstName;
	}

	public String getRecipLastName() {
		return RecipLastName;
	}

	public void setRecipLastName(String recipLastName) {
		RecipLastName = recipLastName;
	}

	public String getBuyerCity()
    {
        return buyerCity;
    }

    public void setBuyerCity(String newBuyerCity)
    {
        buyerCity = newBuyerCity;
    }

    public String getBuyerState()
    {
        return buyerState;
    }

    public void setBuyerState(String newBuyerState)
    {
        buyerState = newBuyerState;
    }

    public String getOrderDate()
    {
        return orderDate;
    }

    public void setOrderDate(String newOrderDate)
    {
        orderDate = newOrderDate;
    }

    public String getItemCount()
    {
        return itemCount;
    }

    public void setItemCount(String newItemCount)
    {
        itemCount = newItemCount;
    }

    public String getOrderTotal()
    {
        return orderTotal;
    }

    public void setOrderTotal(String newOrderTotal)
    {
        orderTotal = newOrderTotal;
    }

    public String getCsrId()
    {
        return csrId;
    }

    public void setCsrId(String newCsrId)
    {
        csrId = newCsrId;
    }

    public String getSortOrder()
    {
        return sortOrder;
    }

    public void setSortOrder(String newSortOrder)
    {
        sortOrder = newSortOrder;
    }

    public String getOrderStatus()
    {
        return orderStatus;
    }

    public void setOrderStatus(String newOrderStatus)
    {
        orderStatus = newOrderStatus;
    }

    public void setPreferredProcessingPartner(String preferredProcessingPartner) {
        this.preferredProcessingPartner = preferredProcessingPartner;
    }

    public String getPreferredProcessingPartner() {
        return preferredProcessingPartner;
    }

    public void setOrderXML(Document orderXML) {
        this.orderXML = orderXML;
    }

    public Document getOrderXML() {
        return orderXML;
    }
}
