 package com.ftd.osp.orderscrub.vo;

import java.math.BigDecimal;

public class PromotionPOVO 
{
    private String persistenceId;
    private String promotionId;
    private Long basePoints;
    private BigDecimal variablePoints;
    private Double lowPrice;
    private Double highPrice;
    private String pointDriver;

    public PromotionPOVO()
    {
    }



    public Long getBasePoints()
    {
        return basePoints;
    }

    public void setBasePoints(Long newBasePoints)
    {
        basePoints = newBasePoints;
    }

    public BigDecimal getVariablePoints()
    {
        return variablePoints;
    }

    public void setVariablePoints(BigDecimal newVariablePoints)
    {
        variablePoints = newVariablePoints;
    }

    public Double getLowPrice()
    {
        return lowPrice;
    }

    public void setLowPrice(Double newLowPrice)
    {
        lowPrice = newLowPrice;
    }

    public Double getHighPrice()
    {
        return highPrice;
    }

    public void setHighPrice(Double newHighPrice)
    {
        highPrice = newHighPrice;
    }

    public String getPointDriver()
    {
        return pointDriver;
    }

    public void setPointDriver(String newPointDriver)
    {
        pointDriver = newPointDriver;
    }

    public String getPromotionId()
    {
        return promotionId;
    }

    public void setPromotionId(String newPromotionId)
    {
        promotionId = newPromotionId;
    }

    public String getPersistenceId()
    {
        return persistenceId;
    }

    public void setPersistenceId(String newPersistenceId)
    {
        persistenceId = newPersistenceId;
    }
}