package com.ftd.osp.orderscrub.vo;

public class ProductFlagsVO 
{
  private String DisplayProductUnavailable;
  String DisplayNoProduct;
  private String DisplayCodifiedFloristHasTwoDayDeliveryOnly;
  String DisplayNoCodifiedFloristHasCommonCarrier;
  private String setDisplayNoProduct;
  private String DisplayNoFloristHasCommonCarrier;
  private String SpecialFeeFlag;
  private boolean Upsell;
  private String upsellSkip;
  private boolean upsellExists;
  private String DisplayFloral;
  private String DisplaySpecialtyGift;
  private String DisplayPersonalizedProduct;

  public ProductFlagsVO()
  {
  }

  public String getDisplayProductUnavailable()
  {
    return DisplayProductUnavailable;
  }

  public void setDisplayProductUnavailable(String newDisplayProductUnavailable)
  {
    DisplayProductUnavailable = newDisplayProductUnavailable;
  }

  public String getDisplayNoProduct()
  {
    return DisplayNoProduct;
  }

  public void setDisplayNoProduct(String newDisplayNoProduct)
  {
    DisplayNoProduct = newDisplayNoProduct;
  }

  public String getDisplayCodifiedFloristHasTwoDayDeliveryOnly()
  {
    return DisplayCodifiedFloristHasTwoDayDeliveryOnly;
  }

  public void setDisplayCodifiedFloristHasTwoDayDeliveryOnly(String newDisplayCodifiedFloristHasTwoDayDeliveryOnly)
  {
    DisplayCodifiedFloristHasTwoDayDeliveryOnly = newDisplayCodifiedFloristHasTwoDayDeliveryOnly;
  }

  public String getDisplayNoCodifiedFloristHasCommonCarrier()
  {
    return DisplayNoCodifiedFloristHasCommonCarrier;
  }

  public void setDisplayNoCodifiedFloristHasCommonCarrier(String newDisplayNoCodifiedFloristHasCommonCarrier)
  {
    DisplayNoCodifiedFloristHasCommonCarrier = newDisplayNoCodifiedFloristHasCommonCarrier;
  }

  public String getSetDisplayNoProduct()
  {
    return setDisplayNoProduct;
  }

  public void setSetDisplayNoProduct(String newSetDisplayNoProduct)
  {
    setDisplayNoProduct = newSetDisplayNoProduct;
  }

  public String getDisplayNoFloristHasCommonCarrier()
  {
    return DisplayNoFloristHasCommonCarrier;
  }

  public void setDisplayNoFloristHasCommonCarrier(String newDisplayNoFloristHasCommonCarrier)
  {
    DisplayNoFloristHasCommonCarrier = newDisplayNoFloristHasCommonCarrier;
  }

  public String getSpecialFeeFlag()
  {
    return SpecialFeeFlag;
  }

  public void setSpecialFeeFlag(String newSpecialFeeFlag)
  {
    SpecialFeeFlag = newSpecialFeeFlag;
  }

  public boolean isUpsell()
  {
    return Upsell;
  }

  public String getUpsellSkip()
  {
    return upsellSkip;
  }

  public void setUpsellSkip(String newUpsellSkip)
  {
    upsellSkip = newUpsellSkip;
  }

  public boolean isUpsellExists()
  {
    return upsellExists;
  }

  public void setUpsellExists(boolean newUpsellExists)
  {
    upsellExists = newUpsellExists;
  }

  public String getDisplayFloral()
  {
    return DisplayFloral;
  }

  public void setDisplayFloral(String newDisplayFloral)
  {
    DisplayFloral = newDisplayFloral;
  }

  public String getDisplaySpecialtyGift()
  {
    return DisplaySpecialtyGift;
  }

  public void setDisplaySpecialtyGift(String newDisplaySpecialtyGift)
  {
    DisplaySpecialtyGift = newDisplaySpecialtyGift;
  }

    public void setDisplayPersonalizedProduct(String displayPersonalizedProduct) {
        this.DisplayPersonalizedProduct = displayPersonalizedProduct;
    }

    public String getDisplayPersonalizedProduct() {
        return DisplayPersonalizedProduct;
    }
}
