<%@ page import="java.sql.*" %>
<html>
<head>
   <SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.ReportMenu.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>
  <title>Report Menu</title>
</head>
<body onLoad="ValidateLogin()">
<FORM name="ReportMenu" action="">
<%
//Set session variable for Change Password Processing
session.setAttribute("menu","Report");

//Retrieve User Information

String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String Territory = (String) session.getAttribute("territory");
String Region = (String) session.getAttribute("region");

//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>" >
<input type="hidden" name="usernme" value="<%= UserName %>" >
<input type="hidden" name="role" value="<%= UserRole %>" >
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwla" scope="session" class="FTDI.DWLoginAttempts" />

<%
//Open connection to database
Connection con = db.Connect();

//Update Login Attempts to 0 after successful login
dwla.ResetLoginAttempt(con, UserID);
if(con!=null)
    db.Close(con);
%>

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<%
if ( !(Territory.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Territory:</b>&nbsp;<%= Territory %></div>
	 <%
}
if ( !(Region.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Region:</b>&nbsp;<%= Region %></div>
	 <%
}	 
%>
<div align="right"><a href="ChangePassword.jsp">Change Data Warehouse Password</a></div>
<div align="right"><a href="http://ftdinet01.ftdi.com/fbc.asp">Goto FBC Source</a></div>
<BR><BR><BR><BR>
<h2>Data Warehouse Reports</h2>
<BR>
<%
if ( UserRole.equals("FBC") )
{
 	%>
        <!-- uncomment when it goes live for FBC's & RVP's 
         <a href="FBCCompensationScreen.jsp">Compensation Reports</a>
	 <BR> -->
	 <a href="MemberServicesReportsMenu.jsp">Member Services Reports</a>
	 <BR>
	 <a href="MercuryTechnologyReportsMenu.jsp">Mercury Technology Reports</a>
	 <BR>
	 <%
}
else
{
  if ( UserRole.equals("INT") )
  {
  	%>
                 <a href="MarketplaceReportsMenu.jsp">MarketPlace Reports</a>
		 <BR>
	 	 <a href="MemberServicesReportsMenu.jsp">Member Services Reports</a>
		 <BR>
		 <a href="MercuryTechnologyReportsMenu.jsp">Mercury Technology Reports</a>
		 <BR> 
		 <a href="ARReportsMenu.jsp">Accounts Receivable Reports</a>
		 <BR> 
	  <%
  }
   else   
  {
     if ( UserRole.equals("RVP") )
      {
         %>
            <!-- uncomment when it goes live for FBC's & RVP's  
                 <a href="FBCCompensationScreen.jsp">Compensation Reports</a>
	         <BR> -->
		 <a href="MarketplaceReportsMenu.jsp">MarketPlace Reports</a>
		 <BR>
	         <a href="MemberServicesReportsMenu.jsp">Member Services Reports</a>
		 <BR>
		 <a href="MercuryTechnologyReportsMenu.jsp">Mercury Technology Reports</a>
		 <BR> 
		 <a href="ARReportsMenu.jsp">Accounts Receivable Reports</a>
		 <BR>
		 <a href="DWActivityScreen.jsp">Data Warehouse Activity Reports</a>
		 <BR> 
		<%
      }
    else
      {
         %>
                 <a href="FBCCompensationScreen.jsp">Compensation Reports</a>
	         <BR>
		 <a href="MarketplaceReportsMenu.jsp">MarketPlace Reports</a>
		 <BR>
	         <a href="MemberServicesReportsMenu.jsp">Member Services Reports</a>
		 <BR>
		 <a href="MercuryTechnologyReportsMenu.jsp">Mercury Technology Reports</a>
		 <BR> 
		 <a href="ARReportsMenu.jsp">Accounts Receivable Reports</a>
		 <BR>
		 <a href="DWActivityScreen.jsp">Data Warehouse Activity Reports</a>
		 <BR> 
		<%
       }  
   }    
} 
%>
<BR>
<INPUT type="button" name="Logout" value="Logout" onClick="window.location='Logout.jsp'">
<BR><BR>
</center>
</form>
</body>
</html>

