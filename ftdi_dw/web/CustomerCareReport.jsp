<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>Customer Care Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
              {
                        if ( document.CCRTest.role.value == "FBC")
                        {
                                 if ( document.CCRTest.memcode.value == "" && document.CCRTest.fbcterr.value != "True") 
                                 {
                                        alert('You must enter a Member Code or select to run the report for your Territory');
                                        close();
                                 }
                                 else
                                 {
                                         document.CCR.submit();
                                 }
                        }
                        else if ( document.CCRTest.role.value == "RVP")
                        {
                                 if ( document.CCRTest.memcode.value == "" && document.CCRTest.terr.value == "")
                                 {
                                        alert('You must enter a Member Code or select a Territory');
                                        close();
                                 }
                                 else
                                 {
                                         document.CCR.submit();
                                 }
                        }
                        else if ( document.CCRTest.role.value == "INT" || document.CCRTest.role.value == "MGT" )
                        {
                                 if ( document.CCRTest.memcode.value == "" && document.CCRTest.terr.value == "" && document.CCRTest.reg.value == "" )
                                 {
                                        alert('You must enter a Member Code or select a Territory or Region');
                                        close();
                                 }
                                 else
                                 {
                                         document.CCR.submit();
                                 }
                        }
                        else
                        {
                                alert('You do not have permissions to run this report');
                                close();
                        }                                                   
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="CCRTest"  action="CustomerCareReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String MemberCode = request.getParameter("MemberCode").toUpperCase();
String Territory = request.getParameter("Territory");
String Region = request.getParameter("Region");
String RunFBCTerritory = request.getParameter("RunFBCTerritory");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="memcode" value="<%= MemberCode %>">
<input type="hidden" name="fbcterr" value="<%= RunFBCTerritory %>">
<input type="hidden" name="terr" value="<%= Territory %>">
<input type="hidden" name="reg" value="<%= Region %>">
<input type="hidden" name="role" value="<%= UserRole %>">

</FORM>
<FORM name="CCR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "";

if ( UserRole.equals("FBC") )
{
   if ( !(MemberCode.equals("")) || RunFBCTerritory != null )
     {
            ReportCall = "CustomerCareReport.rdf";
            if ( MemberCode.equals("") )
            {
                 ReportCall = ReportCall + "&p_territory=" + UserTerritory;
            }
            
            if ( RunFBCTerritory == null )
            {
                 ReportCall = ReportCall + "&p_member=" + MemberCode;
            }
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
            <%
        }
}
else if ( UserRole.equals("RVP") )
{
   if ( !(MemberCode.equals("")) || !(Territory.equals("")) )
     {
            ReportCall = "CustomerCareReport.rdf";
            if ( MemberCode.equals("") )
            {
                 if ( Territory.equals("All") )
                 {
                        ReportCall = ReportCall + "&p_division=" + UserRegion;
                 }
                 else
                 {
                      ReportCall = ReportCall + "&p_territory=" + Territory;
                 }
            }
            
            if ( Territory.equals("") )
            {
                 ReportCall = ReportCall + "&p_member=" + MemberCode;
            }
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
            <%
        }
}
else if ( UserRole.equals("INT") || UserRole.equals("MGT") )
{
   if ( !(MemberCode.equals("")) || !(Territory.equals(""))  || !(Region.equals("")) )
     {
            ReportCall = "CustomerCareReport.rdf";
            if ( !(MemberCode.equals("")) )
            {
                 ReportCall = ReportCall + "&p_member=" + MemberCode;
            }
            
            if ( !(Territory.equals("")) )
            {
                      ReportCall = ReportCall + "&p_territory=" + Territory;
          }

            if ( !(Region.equals("")) )
            {
                      ReportCall = ReportCall + "&p_division=" + Region;
          }
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
            <%
        }
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
