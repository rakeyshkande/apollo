<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>Member Profile Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
              {
                        if ( document.MPRTest.memcode.value == "" )
                        {
                                alert('You must enter a Member Code');
                                close();
                        }
                        else if ( document.MPRTest.month.value == "" )
                        {
                                 alert('You must select a Month');
                                 close();
                        }
                        else if ( document.MPRTest.year.value == "" )
                        {
                                 alert('You must select a Year');
                                 close();
                        }
                        else if ( document.MPRTest.reporttype.value == "" )
                        {
                                alert('You must select a Report Option');
                                close();
                        }
                        else
                        {
                                document.MPR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="MPRTest" action="MemberProfileReport.jsp" >
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String MemberCode = request.getParameter("MemberCode").toUpperCase();
String Month = request.getParameter("Month");
String Year = request.getParameter("Year");
String ReportType = request.getParameter("ReportType");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="memcode" value="<%= MemberCode %>">
<input type="hidden" name="reporttype" value="<%= ReportType %>">
<input type="hidden" name="role" value="<%= UserRole %>">
<input type="hidden" name="year" value="<%= Year %>">
<input type="hidden" name="month" value="<%= Month %>">

</FORM>
<FORM name="MPR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall;

if ( UserRole.equals("FBC") )
{
         if ( !(MemberCode.equals("")) && !(ReportType.equals("")) )
         {
                if ( ReportType.equals("SML") || ReportType.equals("CPCL") )
                {
                     ReportCall = "MemberProfileReport.rdf";
                     ReportCall = ReportCall + "&p_member=" + MemberCode.trim();
                     ReportCall = ReportCall + "&p_month=" + Month;
                     ReportCall = ReportCall + "&p_year=" + Year;
                     ReportCall = ReportCall + "&p_report_type=" + ReportType;
                     ReportCall = ReportCall + "&p_territory=" + UserTerritory;
		     ReportCall = ReportCall + "&p_division=" + UserRegion;
                }
                else
                {
                   ReportCall = "MemberProfileRollupReport.rdf";
                     ReportCall = ReportCall + "&p_member=" + MemberCode.trim();
                     ReportCall = ReportCall + "&p_month=" + Month;
                     ReportCall = ReportCall + "&p_year=" + Year;
                     ReportCall = ReportCall + "&p_territory=" + UserTerritory;
		     ReportCall = ReportCall + "&p_division=" + UserRegion;
                }
                %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                     Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
                <%
         }
}
else if ( UserRole.equals("RVP") )
{
         if ( !(MemberCode.equals("")) && !(ReportType.equals("")) )
         {
                if ( ReportType.equals("SML") || ReportType.equals("CPCL") )
                {
                         ReportCall = "MemberProfileReport.rdf";
                         ReportCall = ReportCall + "&p_member=" + MemberCode.trim();
                         ReportCall = ReportCall + "&p_month=" + Month;
                         ReportCall = ReportCall + "&p_year=" + Year;
                         ReportCall = ReportCall + "&p_report_type=" + ReportType;
			 ReportCall = ReportCall + "&p_territory=" + UserTerritory;
                         ReportCall = ReportCall + "&p_division=" + UserRegion;
                }
                else
                {
                         ReportCall = "MemberProfileRollupReport.rdf";
                         ReportCall = ReportCall + "&p_member=" + MemberCode;
                         ReportCall = ReportCall + "&p_month=" + Month;
                         ReportCall = ReportCall + "&p_year=" + Year;
			 ReportCall = ReportCall + "&p_territory=" + UserTerritory;
                         ReportCall = ReportCall + "&p_division=" + UserRegion;
                }
                %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>

                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
                <%
         }
}
else if ( UserRole.equals("INT") || UserRole.equals("MGT") )
{
         if ( !(MemberCode.equals("")) && !(ReportType.equals("")) )
         {
                if ( ReportType.equals("SML") || ReportType.equals("CPCL") )
                {
                         ReportCall = "MemberProfileReport.rdf";
                         ReportCall = ReportCall + "&p_member=" + MemberCode.trim();
                         ReportCall = ReportCall + "&p_month=" + Month;
                         ReportCall = ReportCall + "&p_year=" + Year;
                         ReportCall = ReportCall + "&p_report_type=" + ReportType;
                         ReportCall = ReportCall + "&p_territory=All";
                         ReportCall = ReportCall + "&p_division=All";
                }
                else
                {
                         ReportCall = "MemberProfileRollupReport.rdf";
                         ReportCall = ReportCall + "&p_member=" + MemberCode.trim();
                         ReportCall = ReportCall + "&p_month=" + Month;
                         ReportCall = ReportCall + "&p_year=" + Year;
                         ReportCall = ReportCall + "&p_territory=All";
                         ReportCall = ReportCall + "&p_division=All";
                }
                %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="130">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
                <%
         }
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
