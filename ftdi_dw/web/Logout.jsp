<html>
<head>
	  <title> Data Warehouse Logout </title>
	  <script type="text/javascript" language="JavaScript">
	  function LogoutSubmit()
	  {
	   	   document.Logout.submit();
	  }
	  </SCRIPT>
</head>
<body onLoad="LogoutSubmit()">
<FORM name="Logout" action="LoginScreen.jsp">
   <%
   //Clear all session variables
   session.setAttribute("userid", null);
   session.setAttribute("username",null);
   session.setAttribute("role",null);
   session.setAttribute("menu",null);
	 session.setAttribute("territory",null);
	 session.setAttribute("region",null);
   %> 
</FORM>
</body>
</html>

