<%@ page import="java.sql.*" %>
<html>
<head>
<SCRIPT type="text/javascript" Language="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	 if (document.SC.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
		 else
		 {
 		 		 document.getElementById('BeginDateRow').style.visibility = "hidden";
 				 document.getElementById('EndDateRow').style.visibility = "hidden";
		 		 document.getElementById('SourceCodeRow').style.visibility = "hidden";
		 		 document.getElementById('ReportTypeRow').style.visibility = "hidden";
 		 		 document.SC.RunReport.style.visibility = "hidden";
		 }
  }
</SCRIPT>
<title>Source Codes Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="SC" method="post" action="SourceCodesReport.jsp" target="SCR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Source Codes Report</H2>
<BR>

<jsp:useBean id="ssc" scope="session" class="FTDI.SingleSourceCode" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>

<%
//Open connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
 	 if ( !(UserRole == null) )
	 {
   		if ( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
   		{
 	 	 	 	%>
				<TABLE>
				<TR><td><b>Select:</b></td></TR>
				<TR>
				<TD><font color="#FF0000">*</font>&nbsp;Report:</TD>
				<TD>
				<SELECT name="Report" onChange="handleChange()">
				<OPTION SELECTED value="">
				<OPTION value="All">All Source Codes
				<OPTION value="Multi">Single or Multiple Source Codes
				</SELECT>
				</TD>					
				</TR>
				<TR id="BeginDateRow">
		  		<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Beginning Date:</TD>
		  		<TD><input type="text" name="BeginDate" size="10" maxlength="10" onBlur="ValidateBeginDate()">
		  		<a href="javascript:doNothing()" onClick="setDateField(document.SC.BeginDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')"> 
				<img src="calendar.gif" alt="" border=0></a>
		  		<font size="2"><i>(MM/DD/YYYY)</i></font></TD>
		  		</TR>
		  		<TR id="EndDateRow">
		  		<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Ending Date:</TD>
		  		<TD><input type="text" name="EndDate" size="10" maxlength="10" onBlur="ValidateEndDate()">
		  		<a href="javascript:doNothing()" onClick="setDateField(document.SC.EndDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')"> 
				<img src="calendar.gif" alt="" border=0></a>
		  		<font size="2"><i>(MM/DD/YYYY)</i></font></TD>
		  		</TR>
				<TR id="ReportTypeRow">
    			        <TD></TD>
                                <TD><font color="#FF0000">*</font>&nbsp;Report Type:</TD>
				<TD>
		  		<Select name="ReportType">
		  		<OPTION SELECTED value="">
				<OPTION value="Dollars">By Dollars
				<OPTION value="Units">By Units
				<OPTION value="Both">By Units and Dollars
		  		</SELECT>
		 		</TD>			
				</TR>
					 
				 <!-- Pull source code list from JDE_user_Codes -->
				 <TR id="SourceCodeRow">
				 <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Source Code:</TD>
				 <TD>
				 <SELECT MULTIPLE size="4" name="SourceCode" onchange="CreateList()">
		  		 <OPTION SELECTED value="">
					 <%
					 ResultSet rs = ssc.SourceCodes(con);
					 while (rs.next())
					 {
			 				 String SourceCode = rs.getString("User_Defined_Code");
							 String Description = rs.getString("Description");
							 %>
							 <OPTION value="<%= SourceCode %>"><%= SourceCode %>&nbsp;-&nbsp;<%= Description %>
					  	 <%
					 }
					 %>
				 </SELECT>
		  		 </TD>
				 </TR>
				 <TR>
					 <!-- Store list of Source Codes Chosen -->
				 <TD></TD><TD><INPUT type="hidden" name="SourceCodeList" ></TD> 
				 </TR>
  				 </TABLE>
				 <BR><BR>
		 		 <INPUT name="RunReport" TYPE="submit" VALUE="Run Report">
		 		 <INPUT TYPE="button" VALUE="MarketPlace Menu" onClick="window.location='MarketplaceReportsMenu.jsp'">
		 		 <BR><BR>
		 		 <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field		 		
		 	<%
			}
			else
			{
			%>
	 	 			Currently your account does not have the correct role permissions to run this report.
		 			<BR>
		 			Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 			<BR><BR>
		 			<INPUT TYPE=Button VALUE="MarketPlace Menu" onClick="window.location='MarketplaceReportsMenu.jsp'">		 			
			<%
			}
	 }
}
else
{
%>
   	  <BR><BR>
		  Connection has been lost to the Data Warehouse Application.
		  <BR>
		  Please contact the Help Desk for assistance.
		  <BR><BR>
		  <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">		
<%
}

//Close Database Connection
if(con!=null)
    db.Close(con);
%>
</center>
</form>
</body>
</html>

<SCRIPT type="text/javascript" Language="JavaScript">

function CreateList()
{
 		var counter = 0;;
		var ListLength = 0;
		
  	document.SC.SourceCodeList.value = "";
					
		for (var counter=0; counter < document.SC.SourceCode.options.length; counter++)
		{
			 if (document.SC.SourceCode.options[counter].selected)
			 {
			 		if (document.SC.SourceCode.options[counter].value != "")
		  		{
			 	 	 	 document.SC.SourceCodeList.value = document.SC.SourceCodeList.value + document.SC.SourceCode.options[counter].value + ",";
						 ListLength++;
			  	}
			 }
		}

		if (ListLength > 40)
		{
		 	 alert('Only a maximum of 40 Source Codes may be selected at one time');
			 document.SC.SourceCodeList.value = "";
			 document.SC.SourceCode.selectedIndex = 0;
		} 
}

function handleChange() 
{

	if (document.SC.Report.selectedIndex == 3 || document.SC.Report.selectedIndex == 2)
	{
 		 document.getElementById('BeginDateRow').style.visibility = "hidden";
 		 document.getElementById('EndDateRow').style.visibility = "hidden";
		 document.getElementById('SourceCodeRow').style.visibility = "visible";
		 document.getElementById('ReportTypeRow').style.visibility = "visible";
		 document.SC.ReportType.selectedIndex = 0;
		 document.SC.SourceCode.selectedIndex = 0;
 		 document.SC.RunReport.style.visibility = "visible";
	}
	else if (document.SC.Report.selectedIndex == 1)
	{
 		 document.getElementById('BeginDateRow').style.visibility = "visible";
 		 document.getElementById('EndDateRow').style.visibility = "visible";
		 document.getElementById('SourceCodeRow').style.visibility = "hidden";
		 document.getElementById('ReportTypeRow').style.visibility = "visible";
 		 document.SC.RunReport.style.visibility = "visible";
		 document.SC.ReportType.selectedIndex = 0;
		 document.SC.SourceCode.selectedIndex = 0;
		 		 
	}
	else
	{
 		 document.getElementById('BeginDateRow').style.visibility = "hidden";
 		 document.getElementById('EndDateRow').style.visibility = "hidden";
		 document.getElementById('SourceCodeRow').style.visibility = "hidden";
		 document.getElementById('ReportTypeRow').style.visibility = "hidden";
 		 document.SC.RunReport.style.visibility = "hidden";	
		 document.SC.ReportType.selectedIndex = 0;
		 document.SC.SourceCode.selectedIndex = 0;
		 	 
	}
}

function ValidateBeginDate()
{
 				 var dt = document.SC.BeginDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
			 

				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Beginning Date in format specified');
							 document.SC.BeginDate.focus();
							 document.SC.BeginDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Beginning Date in format specified');
									 document.SC.BeginDate.focus();
									 document.SC.BeginDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Beginning Date in format specified');
							document.SC.BeginDate.focus();
             	 			document.SC.BeginDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.SC.BeginDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

function ValidateEndDate()
{
 				 var dt = document.SC.EndDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
		 		
				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Ending Date in format specified');
							 document.SC.EndDate.focus();
							 document.SC.EndDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Ending Date in format specified');
									 document.SC.EndDate.focus();
									 document.SC.EndDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Ending Date in format specified');
							document.SC.EndDate.focus();
             	 			document.SC.EndDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.SC.EndDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}
</SCRIPT>

