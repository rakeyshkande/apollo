<%@ page import="java.sql.*" %>
<html>
<head>
  <SCRIPT type="text/javascript" Language="JavaScript" SRC="calendar.js"></SCRIPT>
  <SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
 	   if (document.PSI.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>  
<title>Product Sales vs. Inventory</title>
</head>
<body onLoad="ValidateLogin()">
<FORM name="PSI" method="post" action="ProductSalesInventoryReport.jsp" target="PSIR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Product Sales vs. Inventory Report</H2>
<BR>

<jsp:useBean id="psi" scope="session" class="FTDI.ProductSalesInventory" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<%
//Open connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
 	 if ( !(UserRole == null) )
	 { 
   	 	if ( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
   		{
   		%>
      	 <TABLE>
		  	 <TR>
		  	 <TD><font color="#FF0000">*</font>&nbsp;Beginning Date:</TD>
		  	 <TD><input type="text" name="BeginDate" size="10" maxlength="10" onBlur="ValidateBeginDate()">
		  	 <a href="javascript:doNothing()" onClick="setDateField(document.PSI.BeginDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                         <img src="calendar.gif" alt="" border=0></a>
		  	 <font size="2"><i>(MM/DD/YYYY)</i></font></TD>
		  	 </TR>
		  	 <TR>
		  	 <TD><font color="#FF0000">*</font>&nbsp;Ending Date:</TD>
		  	 <TD><input type="text" name="EndDate" size="10" maxlength="10" onBlur="ValidateEndDate()">
		  	 <a href="javascript:doNothing()" onClick="setDateField(document.PSI.EndDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                         <img src="calendar.gif" alt="" border=0></a>
		  	 <font size="2"><i>(MM/DD/YYYY)</i></font></TD>
		  	 </TR>
   			 <TR>
   			 <TD><font color="#FF0000">*</font>&nbsp;Order Status:</TD>
   			 <TD>
		  	 <Select name="OrderStatus">
		  	 <OPTION SELECTED value="All">All Orders 
		  	 <OPTION value="Open">Open
		  	 <OPTION value="Closed">Closed		  
		  	 </SELECT>
		  	 </TD>
   			 </TR>
		  	 <TR>
		  	 <TD><font color="#FF0000">*</font>&nbsp;Codified Product:</TD>
		  	 <TD>
		  	 <SELECT name="CodifiedProducts">
		  	 <OPTION SELECTED value="All">All Codified Products
		  	 <%
		  	 //Retrieve List of Codified Products
		  	 ResultSet rs = psi.CodifiedProducts(con);
		  
		  	 while(rs.next())
		  	 {
		     			String CodifiedSymbol = rs.getString("Codified_Symbol");
			   			%>
			   			<OPTION value="<%= CodifiedSymbol %>"><%= CodifiedSymbol %>
			   			<%
		  	 }
		  	 %>
		  	 </SELECT>
		  	 </TD>
		  	 </TR>
				 <TR>
   			 <TD><font color="#FF0000">*</font>&nbsp;Codified Year:</TD>
   			 <TD>
  			 <SELECT name="year">
		  	 <OPTION SELECTED value="">--Select Year--
		  	 <%
		  	 //Get Codification Years from Codification Table
				 rs = null;
		  	 rs = psi.CodifiedYears(con);
		  	 while(rs.next())
		  	 {
		   	  	 String CodifiedYear = rs.getString("Codified_Year");
			     	 %>
		  	   	 <OPTION value="<%= CodifiedYear %>"><%= CodifiedYear %>
		  	   	 <%
		  	 }
		  	 %>
		  	 </SELECT>
		  	 </TD>
   			 </TR>
   	  	 </TABLE>
	    	 <BR><BR>
	    	 <INPUT TYPE="submit" VALUE="Run Report">
	    	 <INPUT TYPE="button" VALUE="MarketPlace Menu" onClick="window.location='MarketplaceReportsMenu.jsp'">
      	 <BR><BR>
	    	 <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field	    	
   	  <%
   		}
   		else
   		{
     	%>
   	  		<BR><BR>
		  		Currently your account does not have the correct role permissions to run this report.
		 			<BR>
		  		Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		  		<BR><BR>
		  		<INPUT TYPE="button" VALUE="MarketPlace Menu" onClick="window.location='MarketplaceReportsMenu.jsp'">		  		
			<%
			}
	}
}
else
{
%>
   	  <BR><BR>
		  Connection has been lost to the Data Warehouse Application.
		  <BR>
		  Please contact the Help Desk for assistance.
		  <BR><BR>
		  <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">		 
<%
}

if(con!=null)
    db.Close(con);
%>

</CENTER> 
</FORM>
</body>
</html>

<SCRIPT type="text/javascript" Language="JavaScript">
function ValidateBeginDate()
{
 				 var dt = document.PSI.BeginDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
			 

				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Beginning Date in format specified');
							 document.PSI.BeginDate.focus();
							 document.PSI.BeginDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Beginning Date in format specified');
									 document.PSI.BeginDate.focus();
									 document.PSI.BeginDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Beginning Date in format specified');
							document.PSI.BeginDate.focus();
             	 			document.PSI.BeginDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.PSI.BeginDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

function ValidateEndDate()
{
 				 var dt = document.PSI.EndDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
		 		
				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Ending Date in format specified');
							 document.PSI.EndDate.focus();
							 document.PSI.EndDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Ending Date in format specified');
									 document.PSI.EndDate.focus();
									 document.PSI.EndDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Ending Date in format specified');
							document.PSI.EndDate.focus();
             	 			document.PSI.EndDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.PSI.EndDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

</SCRIPT>


