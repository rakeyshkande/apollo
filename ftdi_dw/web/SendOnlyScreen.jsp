<html>
<head>
  <SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.OG.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>
<title>Send Only</title>
</head>
<body onLoad="ValidateLogin()">
<FORM name="OG" method="post" action="SendOnlyReport.jsp" target="OGR">
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}
String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String Territory = (String) session.getAttribute("territory");
String Region = (String) session.getAttribute("region");
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<%
if ( !(Territory.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Territory:</b>&nbsp;<%= Territory %></div>
	 <%
}
if ( !(Region.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Region:</b>&nbsp;<%= Region %></div>
	 <%
}	 
%>

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities" />

<%
// Get Current Month and Year
String Month = dateutil.CurrentMonthText();
String MonthDigit = dateutil.CurrentMonthDigit();
int DayDigit = dateutil.CurrentDayDigit();
String CurYear = dateutil.CurrentYear();
%>

<BR></BR><BR></BR><BR></BR><BR></BR>
<h2>Send Only Report</h2>

<%
if ( DayDigit > 6 )  // This must be 6 for prod change for testing only if working in first week of the month
{
 	 if ( !(UserRole == null) && ( UserRole.equals("INT") || UserRole.equals("MGT") || UserRole.equals("RVP")) )
	 {
   		%>
      <TABLE>
     	<TR><TD><b>Select:</b></TD></TR> 
		  <TR>
	    <TD></TD>
	    <TD><font color="#FF0000">*</font>&nbsp;Country:</TD>
		  <TD>
		    <SELECT name="Country">
		    <OPTION SELECTED value="">-- Select Country --
		    <OPTION value="US">United States
		    <OPTION value="CA">Canada
		  </SELECT>
		  </TD>
		  </TR>
		  <TR>
			<TD></TD>
		  <TD><font color="#FF0000">*</font>&nbsp;Monthly Orders Out Total:</TD>
		  <TD>
	            <INPUT type="text" name="OrderOutTotal" maxlength="4" size="5" onBlur="CheckOrderTotal()">&nbsp;<i>(9999)</i>
		  </TD>
		  </TR>
			
 		  <TR>
			<TD></TD>
 		  <TD><font color="#FF0000">*</font>&nbsp;Group By:</TD>
		  <TD>
		    <SELECT name="GroupBy">
		    <OPTION SELECTED value="">-- Select Group By --
		    <OPTION value="CM">Collection Manager
		    <OPTION value="DM">Division Manager
		  </SELECT>
		  </TD>
		  </TR>
	 	 </TABLE>
		 <BR><BR>
		 <INPUT TYPE="submit" VALUE="Run Report">
		 <INPUT TYPE="button" VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
		 <BR><BR>
		 <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
		 <BR><BR><BR>		
		 <%
	}
	else
	{
	%>
	 	 Currently your account does not have the correct role permissions to run this report.
		 <BR>
		 Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 <BR><BR>
		 <INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
	<%
	}
}
else
{
 	%>
	<BR>
	<i>The current month Wire Orders data is currently not available due to the Statement processing.
	<BR>
	This period lasts between the 1st and 6th of each month.</i>
  <BR><BR>
	<INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
	<%
}
%>
</CENTER>
</FORM>
</body>
</html>

<SCRIPT type="text/javascript" Language="JavaScript">
function CheckAvailability()
{
   alert('Function hit');
}
function CheckOrderTotal()
{
  if (document.OG.OrderOutTotal.value != "")
    {
/* Test for value less than 1 */
      if (document.OG.OrderOutTotal.value < 1)
        {
           alert('You must enter a value from 1 to 9999 for the Order Out Total.');
	   document.OG.OrderOutTotal.value = "";
	}
      else
        {
/* Test for non-decimal value */
   	   var OrderOutTotalValue = document.OG.OrderOutTotal.value;
	   var DecimalFound = OrderOutTotalValue.indexOf(".");
	   if (DecimalFound >= 0)
	     {
	        alert('You must enter a value from 1 to 9999 for the Order Out Total based on the input mask shown.');
		document.OG.OrderOutTotal.value = "";
	     }
	}
    }		
}

</SCRIPT>
