<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>AR Aged Balances Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
              {
                        if ( document.AARTest.arreport.value == "" )
                        {
                             alert('You must select a AR Report to run');
                             close();
                        }
                        else if ( document.AARTest.arterritory.value == "" && document.AARTest.arregion.value == "" 
												           && document.AARTest.arcollection.value == "" )
                        {
                             alert('You must select a Territory, Region or Collection Manager');
                             close();
                        }
                      else
                        {   
                            document.AAR.submit();
                        }
                }
        </script>
</head>
<body onLoad="SubmitMe()">
<FORM name="AARTest" action="ARAgedBalanceReport.jsp">
<%
String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
         <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String ARTerritory = "NA";
String ARRegion = "NA";
String ARCollection = "NA";
String UserRole = (String) session.getAttribute("role");
String ARReport = request.getParameter("ARReport");
%>
<input type="hidden" name="arreport" value="<%= ARReport %>">
<input type="hidden" name="userrole" value="<%= UserRole %>">
<%
if ( UserRole.equals("INT") || UserRole.equals("MGT") )
{
     ARTerritory = request.getParameter("Territory");
     ARRegion = request.getParameter("Region");
		 ARCollection = request.getParameter("Collection");
     %>
     <input type="hidden" name="arterritory" value="<%= ARTerritory %>">
     <input type="hidden" name="arregion" value="<%= ARRegion %>">
	 <input type="hidden" name="arcollection" value="<%= ARCollection %>">
     <%
}
else
{
     %>
     <input type="hidden" name="arterritory" value="NOTINT">
     <input type="hidden" name="arregion" value="NOTINT">
	 <input type="hidden" name="arcollection" value="NOTINT">
     <%
} 
%>
</FORM>
<form name="AAR" action="<bean:message key="report_url" />" method="post"> 
  <%
String Territory = (String) session.getAttribute("territory");
String Region = (String) session.getAttribute("region");
String Collection = (String) session.getAttribute("collection");
String ARMonth = request.getParameter("month");
String ARYear = request.getParameter("year");
String ARCurrency = request.getParameter("CurrencyCode");
String ARReportType = request.getParameter("ReportType");
String ReportCall = "ARAgingReport.jsp";

if (ARReport.equals("All_Members"))
{
     ReportCall = ReportCall + "&P_Search_Type=C";
     ReportCall = ReportCall + "&P_Member_Status=ALL";
     ReportCall = ReportCall + "&P_Currency_Code=" + ARCurrency;
     ReportCall = ReportCall + "&P_Curr_Amnt_Due=" + ARReportType;
     ReportCall = ReportCall + "&P_Report_Title=" + ARReport;
     ReportCall = ReportCall + "&P_Month=" + ARMonth;
     ReportCall = ReportCall + "&P_Year=" + ARYear;
}
else if (ARReport.equals("FTD_Member"))
{
     ReportCall = ReportCall + "&P_Search_Type=C";
     ReportCall = ReportCall + "&P_Member_Status=ACTIVE";
     ReportCall = ReportCall + "&P_Currency_Code=" + ARCurrency;
     ReportCall = ReportCall + "&P_Curr_Amnt_Due=" + ARReportType;
     ReportCall = ReportCall + "&P_Report_Title=" + ARReport;
     ReportCall = ReportCall + "&P_Month=" + ARMonth;
     ReportCall = ReportCall + "&P_Year=" + ARYear;
}
else if (ARReport.equals("FTD_Non-Active_Member"))
{
     ReportCall = ReportCall + "&P_Search_Type=C";
     ReportCall = ReportCall + "&P_Member_Status=NONACTIVE";
     ReportCall = ReportCall + "&P_Currency_Code=" + ARCurrency;
     ReportCall = ReportCall + "&P_Curr_Amnt_Due=" + ARReportType;
     ReportCall = ReportCall + "&P_Report_Title=" + ARReport;
     ReportCall = ReportCall + "&P_Month=" + ARMonth;
     ReportCall = ReportCall + "&P_Year=" + ARYear;
}
else if (ARReport.equals("VNS_Aging"))
{
     ReportCall = ReportCall + "&P_Search_Type=VNS";
     ReportCall = ReportCall + "&P_Member_Status=ALL";
     ReportCall = ReportCall + "&P_Currency_Code=" + ARCurrency;
     ReportCall = ReportCall + "&P_Curr_Amnt_Due=" + ARReportType;
     ReportCall = ReportCall + "&P_Report_Title=" + ARReport;
     ReportCall = ReportCall + "&P_Month=" + ARMonth;
     ReportCall = ReportCall + "&P_Year=" + ARYear;
}
else 
{
     ReportCall = ReportCall + "&P_Search_Type=NON";
     ReportCall = ReportCall + "&P_Member_Status=ALL";
     ReportCall = ReportCall + "&P_Currency_Code=" + ARCurrency;
     ReportCall = ReportCall + "&P_Curr_Amnt_Due=" + ARReportType;
     ReportCall = ReportCall + "&P_Report_Title=" + ARReport;
     ReportCall = ReportCall + "&P_Month=" + ARMonth;
     ReportCall = ReportCall + "&P_Year=" + ARYear;
}


if ( UserRole.equals("FBC") )
{
     ReportCall = ReportCall + "&P_Territory=" + Territory;
     %>
     <BR><BR><BR><BR>
     <center>
     <H2>Processing Report.....</H2>
     <BR><BR><BR><BR><BR>
     <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
     <BR> 
     you have requested your report may take several minutes to run.
     </i></font>
     </center>
     <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
     <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="180">
     <%
         Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
         Iterator it = queryPairs.entrySet().iterator();
         while (it.hasNext()) {
             Map.Entry pair = (Map.Entry) it.next();
     %>
     <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
     <%
         }
     %>
     <input type="hidden" name="desformat" value="pdf">
     <input type="hidden" name="destype" value="cache">
     <%
}
else if ( UserRole.equals("RVP") )
{
     ReportCall = ReportCall + "&P_Region=" + Region;
     %>
     <BR><BR><BR><BR>
     <center>
     <H2>Processing Report.....</H2>
     <BR><BR><BR><BR><BR>
     <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
     <BR> 
     you have requested your report may take several minutes to run.
     </i></font>
     </center>
     <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
     <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>">
     <%
         Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
         Iterator it = queryPairs.entrySet().iterator();
         while (it.hasNext()) {
             Map.Entry pair = (Map.Entry) it.next();
     %>
     <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
     <%
         }
     %>
     <input type="hidden" name="desformat" value="pdf">
     <input type="hidden" name="destype" value="cache">
     <%
}
else if ( (UserRole.equals("INT") || UserRole.equals("MGT")) && !(ARReport.equals("")) && (!(ARTerritory.equals("")) || !(ARRegion.equals("")) || !(ARCollection.equals(""))) )
{
     if ( !(ARTerritory.equals("")) )
     {
            ReportCall = ReportCall + "&P_Territory=" + ARTerritory;
     }
     
     if ( !(ARRegion.equals("")) )
     {
            ReportCall = ReportCall + "&P_Region=" + ARRegion;
     }
		 
		 if ( !(ARCollection.equals("")) )
     {
            ReportCall = ReportCall + "&P_Collection=" + ARCollection;
     }
     %>
     <BR><BR><BR><BR>
     <center>
     <H2>Processing Report.....</H2>
     <BR><BR><BR><BR><BR>
     <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
     <BR> 
     you have requested your report may take several minutes to run.
     </i></font>
     </center>
     <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
     <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
     <%
         Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
         Iterator it = queryPairs.entrySet().iterator();
         while (it.hasNext()) {
             Map.Entry pair = (Map.Entry) it.next();
     %>
     <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
     <%
         }
     %>
     <input type="hidden" name="desformat" value="pdf">
     <input type="hidden" name="destype" value="cache">
     <%
}
else
{
}
%>
</form>
</body>
</html>
