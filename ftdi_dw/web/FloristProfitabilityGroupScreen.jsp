<%@ page import="java.sql.*" %>
<html>
<head>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
       if (document.GFP.userid.value == "null")
       {
          window.location="LoginScreen.jsp";
       }
  }
</SCRIPT>
<title>Group Florist Profitability Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="GFP" method="post" action="FloristProfitabilityGroupRptCall.jsp" target="GPR">

<%
String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
     <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>FTD Group Florist Profitability Report</H2>
<BR>
<i>The prior month Florist Profitability data is not available between the
<BR> 
1st and 6th of the current month due to the Statement processing.</i>
<BR>
<BR>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="td" scope="session" class="FTDI.TerritoryDetail" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<%
//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
     if ( !(UserRole == null) )
     {
          ResultSet rs = null;
          %>
             <TABLE>  
             <TR><td>
             <font color="#FF0000">*</font>&nbsp;<b>Select a Report Group:</b> 
             </td></TR>
              <%
                // Retrieve All Territory Information from Database
                rs = td.GetTerritoryInfo(con, UserID, UserRole);
              %>
               <TR>
               <TD></TD>
               <TD Align=RIGHT> Territory:</TD>
               <TD>
               <SELECT name="Territory" onChange="CheckRegRanking()"> 
               <OPTION SELECTED value="">
               <OPTION value="All">All Territories
              <%
               while(rs.next())
               {
                String Territory = rs.getString("Territory");
                String FBCName = rs.getString("FBC_Name");
              %>
                <OPTION value="<%= Territory %>"><%= Territory + '-' + FBCName %>
              <%
               }
              %> 
                </SELECT>
              <%
              //Retrieve Region Information from Database
              rs = td.GetRegionInfo(con, UserID, UserRole);
           %>
           </td>
	   <%
            if((UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
            {
            %>
             <TR>
             <TD></TD>
	     <TD ALIGN=RIGHT>Region:</TD>
             <TD>
             <SELECT name="Region" onChange="CheckTerrRanking()"> 
             <OPTION SELECTED value="">
             <OPTION value="All">All Regions
           <%
              while(rs.next())
              {
                String Region = rs.getString("Region");
                String ManagerName = rs.getString("Manager_Name");
           %>
             <OPTION value="<%= Region %>"><%= Region + '-' + ManagerName %>
           <%
              }
           %>
             </SELECT>
             </TD>
             <TD></TD>
             </TR>
          <%
            }//end of (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT")
            %>
	     <TR>
             <TD></TD>
	     <TD Align=RIGHT valign=middle>Sending Rank:  Start</TD>
             <TD Align=LEFT valign=middle>
             <INPUT type="text" name="fromrank" size="4" onChange="CheckRegionTerritory()" >
              &nbsp;End&nbsp;
             <INPUT type="text" name="torank" size="4" onChange="CheckRegionTerritory()">
             </TD>
             </TR>     
             <TR>
	     <TD></TD>
	     <TD>&nbsp;</TD></TR>
             <TR>
	     <TD align=LEFT><font color="#FF0000">*</font></TD>
	     <TD align=left><b>LTM Ending:</b></TD>
             <TD>
             <Select name="Month">
             <OPTION SELECTED value="">-- Select a Month --
             <OPTION value="01">January
             <OPTION value="02">February
             <OPTION value="03">March
             <OPTION value="04">April
             <OPTION value="05">May
             <OPTION value="06">June
             <OPTION value="07">July
             <OPTION value="08">August
             <OPTION value="09">September
             <OPTION value="10">October
             <OPTION value="11">November
             <OPTION value="12">December
             </SELECT>
             </TD>
             </TR>
             <TR>
             <TD align=left><font color="#FF0000">*</font></TD>
             <TD align=left><b>Year:</b></TD>
             <TD>
             <Select name="Year">
             <OPTION SELECTED value="">-- Select a Year --
               <%
                 rs = null;
                 rs = td.MemberRevenueYears(con);
                 while (rs.next())
                 {
                  String RevenueYear = rs.getString("RevenueYear");
               %>
                  <OPTION value="<%= RevenueYear %>"><%= RevenueYear %>
               <%
                 }
               %>
             </SELECT>
             </TD>           
             </TR>
             <TR>
             <TD align=left><font color="#FF0000">*</font></TD><TD align=left><B>Report Option:</B></TD>
             <TD>
             <Select name="ReportType">
             <OPTION SELECTED value="">-- Select a Report -- 
             <OPTION value="TERR">Territory Rollup
             <%
              if((UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
               {
             %>
              <OPTION value="REG">Region Rollup
              <%
               }
              %>
	      <OPTION value="SR">Sending Rank Rollup
              </SELECT>
              </TD>
              </TR>
              </TABLE>
              <BR><BR>
              <INPUT TYPE="submit" VALUE="Run Report">
              <INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
              <BR><BR>
              <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
           <%
    }//!(UserRole == null)
}
else
{
%>
      <BR><BR>
          Connection has been lost to the Data Warehouse Application.
          <BR>
          Please contact the Help Desk for assistance.
          <BR><BR>
          <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
          </CENTER> 
          </FORM>
<%
}

//Close Database Connection
if(con!=null)
    db.Close(con);
%>

<script type="text/javascript" language="JavaScript">

function CheckTerrRanking()
{
 		 if (document.GFP.Territory.selectedIndex != 0)
		   {
		  	 document.GFP.Territory.selectedIndex = 0;
		   }	 
		 if (document.GFP.fromrank.value != ' ') 
		   {
		  	 document.GFP.fromrank.value = ' ';		  	
		   }
		 if (document.GFP.torank.value != ' ')
		   {
		  	 document.GFP.torank.value = ' ';
		   }
}

function CheckRegRanking()
{
     if( document.GFP.role.value == 'MGT' || document.GFP.role.value == 'INT')
     {
         if (document.GFP.Region.selectedIndex != 0)
           {
             document.GFP.Region.selectedIndex = 0;
           } 	 
     }
		 if (document.GFP.fromrank.value != ' ') 
		   {
		  	 document.GFP.fromrank.value = ' ';		  	
		   }
		 if (document.GFP.torank.value != ' ')
		   {
		  	 document.GFP.torank.value = ' ';
		   }
}

function CheckRegionTerritory()
{
     if( document.GFP.role.value == 'MGT' || document.GFP.role.value == 'INT')
     {
       if (document.GFP.Region.selectedIndex != 0)
       {
           document.GFP.Region.selectedIndex = 0;
       }
    }
     if (document.GFP.Territory.selectedIndex != 0)
		 {
		  	 document.GFP.Territory.selectedIndex = 0;
		 }
}
</SCRIPT>

</body>
</html>

