<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Member Notes Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
              {
                        if ( document.MNRTest.memcode.value == "" )
                        {
                                alert('Enter Member Code');
                                close();
                        }
                        else if ( document.MNRTest.bdate.value == "" )
                        {
                                alert('Enter Beginning Date in format specified');
                                close();
                        }
                                                else if ( document.MNRTest.vbdate.value == "false" )
                        {
                                alert('Enter valid Beginning Date in format specified');
                                close();
                        }
                        else if ( document.MNRTest.edate.value == "" )
                        {
                                alert('Enter Ending Date in format specified');
                                close();
                        }
                                                else if ( document.MNRTest.vedate.value == "false" )
                        {
                                alert('Enter valid Ending Date in format specified');
                                close();
                        }
                        else
                        {
                                document.MNR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="MNRTest" action="MemberNotesReport.jsp" >
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String MemberCode = request.getParameter("MemberCode").toUpperCase();
String BeginDate = request.getParameter("BeginDate");
String EndDate = request.getParameter("EndDate");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="memcode" value="<%= MemberCode %>">
<input type="hidden" name="bdate" value="<%= BeginDate %>">
<input type="hidden" name="edate" value="<%= EndDate %>">
<input type="hidden" name="userter" value="<%= UserTerritory %>">
<input type="hidden" name="userreg" value="<%= UserRegion %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retBeginVal = "false";
String retEndVal   = "false";

//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
    // Validate Dates
         retBeginVal = dateutil.validateDate(con, BeginDate);
         retEndVal   = dateutil.validateDate(con, EndDate);
         %>
         <input type="hidden" name="vbdate" value="<%= retBeginVal %>" >
         <input type="hidden" name="vedate" value="<%= retEndVal %>" >
         <%
    db.Close(con);
}
%>
         
</FORM>
<FORM name="MNR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "MemberNotes.rdf";

if ( UserRole.equals("FBC") )
{
         if ( !(MemberCode.equals("")) && !(BeginDate.equals("")) && !(EndDate.equals("")) &&
                            (retBeginVal.equals("true")) && (retEndVal.equals("true")) )
         {
                ReportCall = ReportCall + "&p_member=" + MemberCode;
                ReportCall = ReportCall + "&p_date1=" + BeginDate;
                ReportCall = ReportCall + "&p_date2=" + EndDate;
                ReportCall = ReportCall + "&p_territory=" + UserTerritory;
                %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                     Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
                <%
         }
}
else if ( UserRole.equals("RVP") )
{
         if ( !(MemberCode.equals("")) && !(BeginDate.equals("")) && !(EndDate.equals("")) &&
                            (retBeginVal.equals("true")) && (retEndVal.equals("true")) )
         {
                ReportCall = ReportCall + "&p_member=" + MemberCode;
                ReportCall = ReportCall + "&p_date1=" + BeginDate;
                ReportCall = ReportCall + "&p_date2=" + EndDate;
                ReportCall = ReportCall + "&p_division=" + UserRegion;
                %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
                <%
         }
}
else if ( UserRole.equals("INT") || UserRole.equals("MGT") )
{
         if ( !(MemberCode.equals("")) && !(BeginDate.equals("")) && !(EndDate.equals("")) &&
                            (retBeginVal.equals("true")) && (retEndVal.equals("true")) )
         {
                ReportCall = ReportCall + "&p_member=" + MemberCode;
                ReportCall = ReportCall + "&p_date1=" + BeginDate;
                ReportCall = ReportCall + "&p_date2=" + EndDate;
                %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
                <%
         }
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
