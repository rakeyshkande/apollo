<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>FBC Compensation Reports</title>
<script type="text/javascript" language="JavaScript">
     function SubmitMe()
           {
              if ( document.FBCRTest.month.value == "" )
                {
                   alert('You must select a Month');
                   close();
                }
              else if ( document.FBCRTest.year.value == "" )
                {
                   alert('You must select a Year');
                   close();
                }
              else if (document.FBCRTest.territory.value == "" &&
                       document.FBCRTest.region.value == "")
                {
	           alert('You must select a Territory or a Region');
	           close();
                }  
	      else if ( document.FBCRTest.reporttype.value == "" )
                {
                   alert('You must select a Report Option');
                   close();
                }
              else if (document.FBCRTest.role.value == "RVP" && 
                       document.FBCRTest.reporttype.value == "DAR" &&
                      (document.FBCRTest.territory.value == "ALL" ||
                       document.FBCRTest.region.value != ""))
                {
	           alert('You must select a Territory for the Detail Adds Report');
	           close();
                }  
              else if (document.FBCRTest.role.value == "MGT" && 
                       document.FBCRTest.reporttype.value == "DAR" &&
                      (document.FBCRTest.territory.value == " " ||
                       document.FBCRTest.territory.value == "NOFBC" ||
                       document.FBCRTest.territory.value == "FTD" ||
                       document.FBCRTest.region.value != ""))
                {
	           alert('You must select a Territory for the Detail Adds Report');
	           close();
                }
              else if (document.FBCRTest.role.value == "MGT" && 
                       document.FBCRTest.reporttype.value == "PPR" &&
                      document.FBCRTest.territory.value == "NOFBC" )
                {
	           alert('This report is not available for the "No FBC" territory');
	           close();
                }
              else 
                {  
	        document.FBCR.submit();  
 		}    
	  }			 			 
</SCRIPT>
</head>

<body onLoad="SubmitMe()">
<FORM name="FBCRTest" action="FBCCompensationRptCall.jsp">
<%
String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
       <jsp:forward page="SessionTimeout.jsp"/>
<% 
}
String Month = request.getParameter("Month");
String Year = request.getParameter("Year");
String Territory = request.getParameter("Territory");
String Region = request.getParameter("Region");
String ReportType = request.getParameter("ReportType");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String UserRole = (String) session.getAttribute("role");
%>
<input type="hidden" name="month" value="<%= Month %>">
<input type="hidden" name="year" value="<%= Year %>">
<input type="hidden" name="territory" value="<%= Territory %>">
<input type="hidden" name="region" value="<%= Region %>">
<input type="hidden" name="reporttype" value="<%= ReportType%>">
<input type="hidden" name="role" value="<%= UserRole %>">
</FORM>
<FORM name="FBCR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "";
ReportCall = ReportCall + "&p_month=" + Month;
ReportCall = ReportCall + "&p_year=" + Year;

if ((UserRole.equals("FBC")) || (UserRole.equals("RVP")) || (UserRole.equals("MGT")) )	
  { 
     if (UserRole.equals("FBC")) 
       {
         ReportCall = ReportCall + "&p_territory=" + UserTerritory;
         if (ReportType.equals("CSR"))
           {
             ReportCall = "FBCRVPCompensationSummaryRpt.rdf" + ReportCall;
           } 
         else if (ReportType.equals("TSR")) 
           {
             ReportCall = "FBCTerritorySummaryRpt.rdf" + ReportCall;
           } 
         else if (ReportType.equals("PPR"))
           {
             ReportCall = "FBCRVPPromotionalProgramsRpt.rdf" + ReportCall;
           }  
         else if (ReportType.equals("DAR"))
           {
             ReportCall = "FBCPromoAddsDetailRpt.rdf" + ReportCall;
           }
         else if (ReportType.equals("LTMR"))
           {
             ReportCall = "FBCRVPMonthlyLTMTrendsRpt.rdf" + ReportCall;
           }
       }
     else if (UserRole.equals("RVP")) 
         {
          if (Territory.equals("ALL"))  
            {
              ReportCall = ReportCall + "&p_division=" + UserRegion;
            }
          else  
            {
	       ReportCall = ReportCall + "&p_territory=" + Territory;
            }   
          if (ReportType.equals("CSR"))
            {
              ReportCall = "FBCRVPCompensationSummaryRpt.rdf" + ReportCall;
            } 
          else if (ReportType.equals("TSR") && !(Territory.equals("ALL"))) 
            {
              ReportCall = "FBCTerritorySummaryRpt.rdf" + ReportCall;
            } 
          else if (ReportType.equals("TSR") && Territory.equals("ALL")) 
            {
              ReportCall = "RVPTerritorySummaryRpt.rdf" + ReportCall;
            } 
          else if (ReportType.equals("PPR"))
            {
              ReportCall = "FBCRVPPromotionalProgramsRpt.rdf" + ReportCall;
            }  
          else if (ReportType.equals("DAR") && !(Territory.equals("ALL")))
            {
              ReportCall = "FBCPromoAddsDetailRpt.rdf" + ReportCall;
            }
          else if (ReportType.equals("LTMR"))
            {
              ReportCall = "FBCRVPMonthlyLTMTrendsRpt.rdf" + ReportCall;
            }
         }   
     else if (UserRole.equals("MGT"))
       {
	 if (Territory.equals("NOFBC") )
          {
            ReportCall = ReportCall + "&p_territory=NOFBC";
          }
         else if (Territory.equals("FTD") )
          {
            ReportCall = ReportCall + "&p_territory=FTD";
          }
         else if (!(Territory.equals("")))
          {
             ReportCall = ReportCall + "&p_territory=" + Territory;
          }   
         else if (Region.equals("FTD"))
          {
              ReportCall = ReportCall + "&p_division=FTD";
          }   
         else if (!(Region.equals("")))   
          {
              ReportCall = ReportCall + "&p_division=" + Region;
	  }  
//     
// Format Balance of Management reports   
//
        if (Territory.equals("NOFBC")) 
         {
	  if (ReportType.equals("CSR"))
	       {
	         ReportCall = "NoFBCCompensationSummaryRpt.rdf" + ReportCall;
	       }	
	  else if (ReportType.equals("TSR"))
	       {
	         ReportCall = "NoFBCTerritorySummaryRpt.rdf" + ReportCall;
	       }  
          else if (ReportType.equals("LTMR"))
	       {
	         ReportCall = "NoFBCMonthlyLTMTrendsRpt.rdf" + ReportCall;
               }
         }		  
        else if (Territory.equals("FTD") || Region.equals("FTD")) 
         {
          if (ReportType.equals("CSR"))
	    {
	       ReportCall = "COCTRYCompensationSummaryRpt.rdf" + ReportCall;
	    }	
	  else if (ReportType.equals("TSR"))
	   {
	      ReportCall = "COCTRYTerritorySummaryRpt.rdf" + ReportCall;
	   }    
	  else if (ReportType.equals("PPR"))
	   {
	      ReportCall = "COCTRYPromotionalProgramsRpt.rdf" + ReportCall;
	   }
	  else if (ReportType.equals("LTMR"))
	    {
	      ReportCall = "FTDMonthlyLTMTrendsRpt.rdf" + ReportCall;
            } 
	 }
	else if (!(Territory.equals("")) || !(Region.equals("")) )  
	   {
	    if (ReportType.equals("CSR"))
	      {
	        ReportCall = "FBCRVPCompensationSummaryRpt.rdf" + ReportCall;
	      } 
	    else if (ReportType.equals("TSR") && Region.equals("")) 
	      {
	        ReportCall = "FBCTerritorySummaryRpt.rdf" + ReportCall;
	      } 
	    else if (ReportType.equals("TSR") && Territory.equals("")) 
	      {
	        ReportCall = "RVPTerritorySummaryRpt.rdf" + ReportCall;
	      }
	    else if (ReportType.equals("DAR") && !(Territory.equals("")))
              {
                ReportCall = "FBCPromoAddsDetailRpt.rdf" + ReportCall;
              }  
	    else if (ReportType.equals("PPR"))
	      {
	        ReportCall = "FBCRVPPromotionalProgramsRpt.rdf" + ReportCall;
	      }  
	    else if (ReportType.equals("LTMR"))
	      {
	        ReportCall = "FBCRVPMonthlyLTMTrendsRpt.rdf" + ReportCall;
              } 
          } 
       } 	  
 %>
      <BR><BR><BR><BR>
      <center>
      <H2>Processing Report.....</H2>
      <BR><BR><BR><BR><BR>
      <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
      <BR> 
        you have requested your report may take several minutes to run.
      </i></font>
      </center>
      <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
      <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="150">
      <%
         Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
         Iterator it = queryPairs.entrySet().iterator();
         while (it.hasNext()) {
             Map.Entry pair = (Map.Entry) it.next();
     %>
     <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
     <%
         }
     %>
     <input type="hidden" name="desformat" value="pdf">
      <input type="hidden" name="destype" value="cache">
     <%
  }
else
  {
  // do not display
  }  
%>   
</FORM>
</body>
</html>