<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<html>
<head>
  <title> Data Warehouse Password Maintenance</title>
  <SCRIPT type="text/javascript" Language="JavaScript">
	  	function SubmitMe()
	  	{
		   	if(document.ValidateChangePassword.NewPasswordLength.value=="INVALID")
				{
			 	 		alert('Your New Password is less than 6 characters long.\n\t    Please try again.');
				 		window.location="ChangePassword.jsp";
				}
		    
				else if(document.ValidateChangePassword.NewPasswordLetter.value=="INVALID")
				{
			 	 		alert('The first character of the New Password is not an alpha (A-Z) character.\n\t\tPlease try again.');
				 		window.location="ChangePassword.jsp";
				}	

				else if(document.ValidateChangePassword.OldPassVerify.value=="INVALID")
				{
  			 		alert('The password you entered for the Old Password does not match your current password.\n\t\t\tPlease try again.');
				 		window.location="ChangePassword.jsp";
				}
			
				else if(document.ValidateChangePassword.NewRetypedVerify.value=="INVALID")
				{
			   	 	alert('The values you entered for the New and Retyped Passwords do not match.\n\t\t      Please try again.');
			   	 	window.location="ChangePassword.jsp";
				}
						
				else if(document.ValidateChangePassword.NewOldPassEqual.value=="INVALID")
				{
			 	 		alert('Your new password cannot be the same as your current password.\n\t\tPlease try again.');
				 		window.location="ChangePassword.jsp";
				}
			
				else if(document.ValidateChangePassword.NewPassUserEqual.value=="INVALID")
				{
			 	 		alert('Your new password cannot be the same as your UserID.\n\t            Please try again.');
				 		window.location="ChangePassword.jsp";
				}					

				else
				{
			 	 		alert('Your Password has successfully been changed.\n  Please login in using your new password.');
				 		document.ValidateChangePassword.submit();
				}
		}
	  </SCRIPT> 

</head>
<body onLoad="SubmitMe()">
<FORM name="ValidateChangePassword" action="Logout.jsp">

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="pwd" scope="session" class="FTDI.PasswordUtilities" />
<jsp:useBean id="ChangePass" scope="session" class="FTDI.ChangePassword" />
	 
   <% 
   //Create AlphaChars String
   String AlphaChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
   
   //Get the Old, New, Retyped New Password that was entered 
   String OldPassword = (request.getParameter("OldPass")).toUpperCase();
   String NewPassword = (request.getParameter("NewPass")).toUpperCase();
   String RetypePassword = (request.getParameter("RetypeNewPass")).toUpperCase();

   //Get UserId from session information
   String UserId = (String) session.getAttribute("userid");
   
   //Encrypt Old Password for comparison against one stored in database
	 byte[] OldUserPassword = pwd.EncryptPassword(OldPassword);

   //Open connection to database  
   Connection con = db.Connect();

   //Retrieve information for user from database
   ResultSet rs =  ChangePass.DWPassword(con, UserId);
   rs.next();
   byte[] StoredPassword = rs.getBytes("DW_PASSWORD");

   //Verify Length of New Password
   if (NewPassword.length() >= 6)
   {
   	%>
	  <input type="hidden" name="NewPasswordLength" value="VALID">
	  <%
	  //Verify first letter of new Password is a character A-Z
   	  if ( AlphaChars.indexOf(NewPassword.charAt(0)) >= 0 )
   	  {
   	  	   %>
   	  	   <input type="hidden" name="NewPasswordLetter" value="VALID">
		  		 <%
		       //Verify old password matches one on database for user id
   		     if (MessageDigest.isEqual(OldUserPassword, StoredPassword))
   		     {
   	  	   	  %>
   	  	   	  <input type="hidden" name="OldPassVerify" value="VALID">
   		   	    <%
		   	      //Verify New Password equals Retyped Password
		   	      if ( NewPassword.equals(RetypePassword) )
		   	      {
  	   	   	   	 %>
  	   	   	  	 <input type="hidden" name="NewRetypedVerify" value="VALID">
	   	   	  	   <%
   	   	   	  	 //Verify New Password does not equal Old Password
	   	   	       if ( !(NewPassword.equals(OldPassword)) )
	   	   	 	     {
	 	   	   	  	    %>
	  	   	   	  	  <input type="hidden" name="NewOldPassEqual" value="VALID">
		   	   	  	    <%
					          //Verify New Password does not equal User ID
			   	 					if ( !(NewPassword.equals(UserId)) )
    		   	  	 		{
			   			  		 	 %>
				  		  			 <input type="hidden" name="NewPassUserEqual" value="VALID">
						    			 <%
				  		  			 //Update Password on Database

	   					  			 //Encrypt New Password
											 byte[] NewUserPassword = pwd.EncryptPassword(NewPassword);

						    			 //Update Password on Database
						    			 ChangePass.UpdateDWPassword(con, UserId, NewUserPassword);
			   	     			}
			   	  	 	else
			   	  	 	{
			   	      	 %>
				  		 		 <input type="hidden" name="NewPassUserEqual" value="INVALID">
						 			 <%
				  	 		}
		   	  	 }
		   	  	 else
		   	  	 {
		   	   	  	 %>
		   		  	 	 <input type="hidden" name="NewOldPassEqual" value="INVALID">
		   		  	 	 <%
		   	  	 }
	       	}
		   	  else
		   	  {
		 	   	  	%>
		  	   	  <input type="hidden" name="NewRetypedVerify" value="INVALID">
			   	  	<%
		   	  }
   	    }
   		  else
   		  {
   		 	  %>
   	  	  <input type="hidden" name="OldPassVerify" value="INVALID">
   			  <%
   		  }
   	  }
   	  else
   	  {
   	   	%>
	   	  <input type="hidden" name="NewPasswordLetter" value="INVALID">
	   	  <%
   	  }
   }
   else
   {
   	%>
	  <input type="hidden" name="NewPasswordLength" value="INVALID">
	  <%
   }

   // Close Database Connetion
   if(con!=null)
       db.Close(con);
   %>
</FORM>
</body>
</html>

