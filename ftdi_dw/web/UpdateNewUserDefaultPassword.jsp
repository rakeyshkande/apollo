<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<html>
<head>
  <title> Update New User Default Password</title>
</head>
<body>
<FORM name="UpdateNewUserDefaultPassword" action="">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="center">
<center>
<BR><BR><BR><BR>
<jsp:useBean id="passutil" scope="session" class="FTDI.PasswordUtilities" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<% 
//Get the New Default Password that was entered 
String DefaultPassword = (request.getParameter("DefaultPassword")).toUpperCase();

//Open connection to database  
Connection con = db.Connect();

//Update Default Password on Global Parm table
passutil.NewUserDefaultPassword(con, DefaultPassword);
%>

New User Default Password has been reset
<BR><BR>
<input type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">

<%
// Close Database Connetion
if(con!=null)
    db.Close(con);
%>
</center>
</FORM>
</body>
</html>

