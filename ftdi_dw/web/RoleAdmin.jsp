<html>
<head>
  <title>Data Warehouse Role Maintenance</title>
  <SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.RoleAdmin.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>  
</head>
<body onLoad="ValidateLogin()">
<FORM name="RoleAdmin" action=""> 
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<center>
<BR><BR><BR><BR>
<h2>Role Administration</h2>
<BR><BR>
<a href="AddRoleMapping.jsp">Add AS400 To DW Role Mapping</a>
<BR>
<a href="ModifyRoleMapping.jsp">Modify AS400 To DW Role Mapping</a>  
<BR>
<a href="DeleteRoleMapping.jsp">Delete AS400 To DW Role Mapping</a>  
<BR><BR>
<INPUT type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">

</center>
</form> 
</body>
</html>

