<html>
<head>
  <script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.MercuryTechnologyReportsMenu.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>
  <title>Mercury Technology Reports Menu</title>
</head>
<body onLoad="ValidateLogin()">
<FORM name="MercuryTechnologyReportsMenu" action="">
<%
//Retrieve User Information
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String Territory = (String) session.getAttribute("territory");
String Region = (String) session.getAttribute("region");

//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">


<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="pcr" scope="session" class="FTDI.ContractStatus" />

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<%
if ( !(Territory.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Territory:</b>&nbsp;<%= Territory %></div>
	 <%
}
if ( !(Region.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Region:</b>&nbsp;<%= Region %></div>
	 <%
}	 
%>
<BR><BR><BR><BR>
<h2>Mercury Technology Reports</h2>
<BR>
<BR>
<BR>
<a href="MemberProfile.jsp">Member Profile</a>
<BR>
<a href="MercuryDirectInstall.jsp">Mercury Direct Installations</a>
<BR>
<BR>
<INPUT type="button" name="MainMenu" value="Main Menu" onClick="window.location='ReportMenu.jsp'">
<BR><BR>
  
</center>
</FORM>
</body>
</html>

