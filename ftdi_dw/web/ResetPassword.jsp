<%@ page import="java.sql.*" %>
<html>
<head>
  <title>Data Warehouse Password Maintenance</title>
   <SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.ResetPassword.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>  
</head>
<body onLoad="ValidateLogin()">
<FORM name="ResetPassword" method="post" action="ValidateResetPassword.jsp"> 
<%
//Set session variable for Change Password Processing
session.setAttribute("menu","Reset");

String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwla" scope="session" class="FTDI.DWLoginAttempts" />

<%
//Open connection to database
Connection con = db.Connect();

//Update Login Attempts to 0 after successful login
dwla.ResetLoginAttempt(con, UserID);

if(con!=null)
    db.Close(con);
%>

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>

<center>
<BR><BR><BR><BR>
<h2>Reset Password for Data Warehouse</h2>
<BR>
Enter Caller's AS/400 User ID.  
<BR><BR>
<TABLE>
<TR>
<TD>User ID:</TD>
<TD><input type="text" name="UserID" size="10"></TD>
</TR>
</TABLE>
<BR><BR>
<INPUT type="submit" name="Submit" value="Verify User">
<INPUT type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">
</center>
</FORM> 
</body>
</html>

