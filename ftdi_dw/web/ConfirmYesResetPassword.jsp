<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<html>
<head>
  <title> Data Warehouse Password Maintenance</title>
</head>
<body>
<FORM name="ConfirmYesResetPassword" method="post" action="LoginScreen.jsp">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="center">
<center>
<h2>Reset Password for Data Warehouse</h2>
<BR><BR><BR><BR>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="cyrp" scope="session" class="FTDI.ConfirmYesResetPass" />
<jsp:useBean id="pwd" scope="session" class="FTDI.PasswordUtilities" />

<% 
//Get the UserID that was entered 
String UserID = (String)session.getAttribute("userid");
	 
//Check for session Timeout
if (UserID==null)
{
%>
   <jsp:forward page="LoginScreen.jsp"/>
<%
}
   //Get UserID to reset password
     String ResetUserID = (String)session.getAttribute("resetuserid");
	 
   //Open connection to database  
     Connection con = db.Connect();

   //Encrypt User ID
	 byte[] EncryptedPassword = pwd.EncryptPassword(ResetUserID.toUpperCase());

   //Update Password to UserID and Data Warehouse Password Change Date to current date.
   cyrp.ResetDWPass(con, EncryptedPassword, ResetUserID); 
   
   // Close Database Connetion
   if(con!=null)
      db.Close(con);
   %>
   Caller's password has been reset to the User Id.  
   <BR>
   Caller will be asked to change the password when logging in.
   <BR><BR>
	 <%
	 if (UserID.equalsIgnoreCase("admin"))
	 {
	 %>
	    <input type="button" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">
	 <%
	 }
	 else
	 {
	 %>
 	    <input type="submit" value="Logout" onClick="window.location='Logout.jsp'">
	 <%
	 }
	 %>   
   </center>
</FORM>
</body>
</html>

