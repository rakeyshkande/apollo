<%@ page import="java.sql.*" %>
<html>
<head>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.CC.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
</SCRIPT>
<title>Customer Care Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="CC" method="post" action="CustomerCareReport.jsp" target="CCR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String UserTerritory = (String) session.getAttribute("territory");
//Validate User Logged In
%>

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Customer Care Report</H2>
<BR>
<BR>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="cs" scope="session" class="FTDI.ContractStatus" />

<%
//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
 	 if ( !(UserRole == null) )
	 {
   		if( (UserRole.trim()).equals("FBC") )
   		{
 	 	 	 		%>
					<TABLE>
					<TR><td><b>Select:</b></td></TR>
					<TR>
					<TD>
					<font color="#0000FF">*</font>&nbsp;Member Code:&nbsp;
					<INPUT type="text" name="MemberCode" size="10" onblur="CheckFBCTerritory()"><font size="2"><i>(XX-XXXXAA)</i></font>
					</TD>
					</TR>
					<TR>
					<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Or</TD>
					</TR>
					<TR>
					<TD><font color="#0000FF">*</font><input type="checkbox" name="RunFBCTerritory" value="True" onclick="CheckMemberCode()">&nbsp;Run for Territory&nbsp;<%= UserTerritory %></TD>
					</TR>
  				</TABLE>
					<BR><BR>
		 			<INPUT TYPE="submit" VALUE="Run Report">
		 			<INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		 			<BR><BR>
		 			<font color="#0000FF">*</font>&nbsp;&nbsp;Denotes Optional field
		 	<%
			}
			else if ( (UserRole.trim()).equals("RVP") )
			{
 	 	 	 		%>
					<TABLE>
					<TR><td><b>Select:</b></td></TR>
					<TR>
					<TD><font color="#0000FF">*</font>&nbsp;Member Code:&nbsp;<INPUT type="text" name="MemberCode" size="10" onblur="CheckTerritory()"><font size="2"><i>(XX-XXXXAA)</i></font></TD>
					</TR>
					<TR>
					<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Or</TD>
					</TR>
					<TR></tr>
					<%
					//Retrieve FBC Informaiton from DB
		  		ResultSet rs = cs.FBCInfo(con, UserID);
		  		%>
					<TR>
		  		<TD><font color="#0000FF">*</font>&nbsp;Territory:&nbsp;
		  		<SELECT name="Territory" onchange="CheckMemberCode()"> 
		  		<OPTION SELECTED value="">-- Select a Territory -- 
					<OPTION value="All">All Territories
		  		<%
		  		while(rs.next())
	 	  		{
		 	 				String Territory = rs.getString("Territory");
							String FBCName = rs.getString("FBC_Name");
					  	%>
							<OPTION value="<%= Territory %>"><%= Territory + '-' + FBCName %>
						  <%
		  	  }
		  		%>
		  		</SELECT>
  				</TD>
					</TR>
  				</TABLE>
					<BR><BR>
		 			<INPUT TYPE="submit" VALUE="Run Report">
		 			<INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		 			<BR><BR>
		 			<font color="#0000FF">*</font>&nbsp;&nbsp;Denotes Optional field
		 	<%
			}
			else if ( (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
			{
 		 		  %>
					<TABLE>
					<TR><td><b>Select:</b></td></TR>
					<TR>
					<TD><font color="#0000FF">*</font>&nbsp;Member Code:&nbsp;<INPUT type="text" name="MemberCode" size="10" onblur="CheckTerritoryRegion()"><font size="2"><i>(XX-XXXXAA)</i></font></TD>
					</TR>
					<TR>
					<TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Or</TD>
					</TR>
					<TR></tr>
					<%
			 	 	//Retrieve Territory Information from Database
					ResultSet rs = cs.TerritoryInfo(con);
					%>
                                        <tr>
					<TD><font color="#0000FF">*</font>&nbsp;Territory:&nbsp;
					<SELECT name="Territory" onChange="CheckRegionMember()"> 
					<OPTION SELECTED value="">
					<OPTION value="All">All Territories
					<%
					while(rs.next())
					{
		 					String Territory = rs.getString("Territory");
							String FBCName = rs.getString("FBC_Name");
							%>
							<OPTION value="<%= Territory %>"><%= Territory + '-' + FBCName %>
							<%
					}
					%>
					</SELECT>
					<%
					//Retrieve Region Information from Database
		  		rs = null;
		  		rs = cs.RegionInfo(con);
		  		%>
					</TR>
					<TR><TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Or</TD></TR>
					<TR>
					<TD><font color="#0000FF">*</font>&nbsp;Region:&nbsp;
		  		<SELECT name="Region" onChange="CheckTerritoryMember()"> 
		  		<OPTION SELECTED value="">
		  		<OPTION value="All">All Regions
		  		<%
		  		while(rs.next())
	 	  		{
		 	 		   	String Region = rs.getString("Region");
							String ManagerName = rs.getString("Manager_Name");
			    		%>
							<OPTION value="<%= Region %>"><%= Region + '-' + ManagerName %>
							<%
		  		}
		  		%>
		  		</SELECT>
		  		</TD>
		  		</TR>
  				</TABLE>
					<BR><BR>
		 			<INPUT TYPE="submit" VALUE="Run Report">
		 			<INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		 			<BR><BR>
		 			<font color="#0000FF">*</font>&nbsp;&nbsp;Denotes Optional field
		 	<%
			}
			else
			{
			%>
	 	 		  Currently your account does not have the correct role permissions to run this report.
		 			<BR>
		 			Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 			<BR><BR>
		 			<INPUT TYPE=Button VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
			<%
			}
	}
}
else
{
%>
   	  <BR><BR>
		  Connection has been lost to the Data Warehouse Application.
		  <BR>
		  Please contact the Help Desk for assistance.
		  <BR><BR>
		  <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
<%
}

//Close Database Connection
if(con!=null)
    db.Close(con);
%>

</CENTER> 
</FORM>
</body>
</html>

<script type="text/javascript" language="JavaScript">
function CheckFBCTerritory()
{
 				 var tmp_mbr = document.CC.MemberCode.value;
				 
 				 if (document.CC.MemberCode.value != "")
  			 {
				  		document.CC.RunFBCTerritory.checked = false;
				 }
        
         if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
              ! tmp_mbr.match(/^[0-9]+$/) )
         {
             alert('Enter a valid member code in the format specified');
             document.CC.MemberCode.select();
         }
}

function CheckMemberCode()
{
 				 if (document.CC.role.value == "FBC")
				 {
 				 		if (document.CC.RunFBCTerritory.checked == true)
						{
				 		 	 document.CC.MemberCode.value = "";
				 		}
				}
				
				if (document.CC.role.value == "RVP")
				 {
 				 		if (document.CC.Territory.selectedIndex != 0)
						{
				 		 	 document.CC.MemberCode.value = "";
				 		}
				} 
}

function CheckTerritory()
{
 		var tmp_mbr = document.CC.MemberCode.value;
		
 		if (document.CC.MemberCode.value != "")
		{
		 	 document.CC.Territory.selectedIndex = 0;
		}
		
		if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
         ! tmp_mbr.match(/^[0-9]+$/) )
    {
         alert('Enter a valid member code in the format specified');
         document.CC.MemberCode.select();
    }
}

function CheckTerritoryRegion()
{
 		var tmp_mbr = document.CC.MemberCode.value;
		
 		if (document.CC.MemberCode.value != "")
		{
		 	 document.CC.Territory.selectedIndex = 0;
 		 	 document.CC.Region.selectedIndex = 0;
		}

		if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
         ! tmp_mbr.match(/^[0-9]+$/) )
    {
         alert('Enter a valid member code in the format specified');
         document.CC.MemberCode.select();
    }		
}

function CheckTerritoryMember()
{
 		if (document.CC.Region.selectedIndex != 0)
		{
		 	 document.CC.MemberCode.value = "";
 		 	 document.CC.Territory.selectedIndex = 0;
		}
}

function CheckRegionMember()
{
 		if (document.CC.Territory.selectedIndex != 0)
		{
		 	 document.CC.MemberCode.value = "";
 		 	 document.CC.Region.selectedIndex = 0;
		}
}

</SCRIPT>



