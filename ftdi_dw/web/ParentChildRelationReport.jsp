<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>Parent-Child Relationship Report</title>
<SCRIPT type="text/javascript" Language="JavaScript">
            function SubmitMe()
              {
                        if ( document.PCRTest.memcode.value == "" )
                        {
                                alert('You must enter a Member Code');
                                close();
                        }
                        else
                        {
                                document.PCR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="PCRTest" action="ParentChildRelationReport.jsp" >
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String MemberCode = request.getParameter("MemberCode").toUpperCase();
%>

<input type="hidden" name="memcode" value="<%= MemberCode %>">

</FORM>
<FORM name="PCR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "ParentChild.jsp";

if ( !(MemberCode.equals("")) )
{
         ReportCall = ReportCall + "&p_member_code=" + MemberCode;
         %>
         <BR><BR><BR><BR>
         <center>
         <H2>Processing Report.....</H2>
         <BR><BR><BR><BR><BR>
         <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
         <BR> 
         you have requested your report may take several minutes to run.
         </i></font>
         </center>
         <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
         <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
         <%
             Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
             Iterator it = queryPairs.entrySet().iterator();
             while (it.hasNext()) {
                 Map.Entry pair = (Map.Entry) it.next();
         %>
         <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
         <%
             }
         %>
         <input type="hidden" name="desformat" value="pdf">
         <input type="hidden" name="destype" value="cache">
<%
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
