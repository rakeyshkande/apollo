<%@ page import="java.sql.*" %>
<html>
<head>
<script type="text/javascript" language="JavaScript" SRC="calendar.js"></SCRIPT>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
       if (document.MSC.userid.value == "null")
       {
          window.location="LoginScreen.jsp";
       }
  }
</SCRIPT>
<title>Member Change Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="MSC" method="post" action="MemberStatusReport.jsp" target="MSR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
     <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Member Change Report</H2>
<BR>

 
<jsp:useBean id="msc" scope="session" class="FTDI.MemberStatusChange" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<%
//Open connection to database
Connection con = db.Connect();

if (con != null)
{
     if ( !(UserRole == null) )
     {
        if( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
        {
                %>
                    <TABLE>
                    <TR><td><b>Select:</b></td><TR>
                    <TR>
                    <TD></TD><TD><font color="#0000FF">*</font>&nbsp;Member Code:</TD>
                    <TD><INPUT type="text" name="MemberCode" size="10" maxlength="10" onBlur="ValidateMemberCode()"><font size="2"><i>(XX-XXXXAA)</i></font></TD>
                    </TR>
                    <TR><TD></TD><TD>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Or</b></TD></TR>
                    <TR>
                    <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Beginning Date:</TD>
                    <TD><input type="text" name="BeginDate" size="10" maxlength="10" onBlur="ValidateBeginDate()">
                    <a href="javascript:doNothing()" onClick="setDateField(document.MSC.BeginDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                    <img src="calendar.gif" alt="" border=0></a>
                    <font size="2"><i>(MM/DD/YYYY)</i></font></TD>
                    </TR>
                    <TR>
                    <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Ending Date:</TD>
                    <TD><input type="text" name="EndDate" size="10" onBlur="ValidateEndDate()">
                    <a href="javascript:doNothing()" onClick="setDateField(document.MSC.EndDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                    <img src="calendar.gif" alt="" border=0></a>
                    <font size="2"><i>(MM/DD/YYYY)</i></font></TD>
                    </TR>
                    <%

                    if( (UserRole.trim()).equals("RVP"))
                    {
                            //Retrieve FBC Info from Database
                        ResultSet rs = msc.FBCInfo(con, UserID);
                            %>
                            <TR>
                            <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
                            <TD>
                            <SELECT name="Territory"> 
                            <OPTION SELECTED value="All">All Territories
                            <%
                            while(rs.next())
                            {
                                     String Territory = rs.getString("Territory");
                                     String FBC_Name = rs.getString("FBC_Name");
                                     %>
                                     <OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
                                     <%
                            }
                            %>
                            </SELECT>
                            </TD>
                            </TR>
                    <%
                    }

                    if ( (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT") )
                    {
                         //Retrieve All Territory Info from Database
                     ResultSet rs = msc.TerritoryInfo(con);
                         %>
                         <TR>
                         <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
                         <TD>
                         <SELECT name="Territory" onChange="CheckRegion()"> 
                         <OPTION SELECTED value="">
                         <OPTION value="All">All Territories
                         <%
                         while(rs.next())
                         {
                                    String Territory = rs.getString("Territory");
                                    String FBC_Name = rs.getString("FBC_Name");
                                    %>
                                    <OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
                                    <%
                         }
                         %>
                         </SELECT>
                         </TD>
                         <TD>&nbsp;&nbsp;Or&nbsp;&nbsp;</TD>
                         <%
                         //Retrieve All Region Info from Database
                         rs = null;
                     rs = msc.RegionInfo(con);
                         %>
                         <TD>Region:</TD>
                         <TD>
                         <SELECT name="Region" onChange="CheckTerritory()"> 
                         <OPTION SELECTED value="">
                         <OPTION value="All">All Regions
                         <%
                         while(rs.next())
                         {
                                    String Region = rs.getString("Region");
                                    String Mgr_Name = rs.getString("Manager_Name");
                                    %>
                                    <OPTION value="<%= Region %>"><%= Region + '-' + Mgr_Name %>
                                    <%
                         }
                         %>
                         </SELECT>
                         </TD>
                         </TR>
                    <%
                    }
                    %>
                    </TABLE>
                    <BR><BR>
                    <INPUT TYPE="submit" VALUE="Run Report">
                    <INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
                    <BR><BR>
                    <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
                    <BR>
                    <font color="#0000FF">*</font>&nbsp;&nbsp;Enter a specific Member Code or leave blank for all members.
                  <%
            }
        else
            {
            %>
                Currently your account does not have the correct role permissions to run this report.
                <BR>
                Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
                <BR><BR>
                <INPUT TYPE=Button VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
         <%
         }
    }
}
else
{
%>
      <BR><BR>
          Connection has been lost to the Data Warehouse Application.
          <BR>
          Please contact the Help Desk for assistance.
          <BR><BR>
          <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
<%
}

//Close Database Connection
if(con!=null)
    db.Close(con);
%>
</CENTER> 
</FORM>
</body>
</html>

<script type="text/javascript" language="JavaScript">
function CheckRegion()
{
         if (document.MSC.Region.selectedIndex != 0)
         {
             document.MSC.Region.selectedIndex = 0;
         }
}

function CheckTerritory()
{
         if (document.MSC.Territory.selectedIndex != 0)
         {
             document.MSC.Territory.selectedIndex = 0;
         }
}

function ValidateMemberCode()
{
         var tmp_mbr = document.MSC.MemberCode.value;
         
         if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
              ! tmp_mbr.match(/^[0-9]+$/) )
         {
             alert('Enter a valid member code in the format specified');
			 document.MSC.MemberCode.focus();
             document.MSC.MemberCode.select();
         }
}

function ValidateBeginDate()
{
 				 var dt = document.MSC.BeginDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
			 

				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Beginning Date in format specified');
							 document.MSC.BeginDate.focus();
							 document.MSC.BeginDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Beginning Date in format specified');
									 document.MSC.BeginDate.focus();
									 document.MSC.BeginDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Beginning Date in format specified');
							document.MSC.BeginDate.focus();
             	 			document.MSC.BeginDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.MSC.BeginDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

function ValidateEndDate()
{
 				 var dt = document.MSC.EndDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
		 		
				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Ending Date in format specified');
							 document.MSC.EndDate.focus();
							 document.MSC.EndDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Ending Date in format specified');
									 document.MSC.EndDate.focus();
									 document.MSC.EndDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Ending Date in format specified');
							document.MSC.EndDate.focus();
             	 			document.MSC.EndDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.MSC.EndDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}
</SCRIPT>
 


