<html>
<head>
 <script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
       if (document.MemberServicesMenu.userid.value == "null")
       {
          window.location="LoginScreen.jsp";
       }
  }
  </SCRIPT>
  <title>Member Services Menu</title>
</head>
<body onLoad="ValidateLogin()">
<FORM name="MemberServicesMenu" action="">
<%
//Retrieve User Information
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
     <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String Territory = (String) session.getAttribute("territory");
String Region = (String) session.getAttribute("region");

//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<%
if ( !(Territory.equals("NA")) )
{
     %>
     <div align="right"><b>Territory:</b>&nbsp;<%= Territory %></div>
     <%
}
if ( !(Region.equals("NA")) )
{
     %>
     <div align="right"><b>Region:</b>&nbsp;<%= Region %></div>
     <%
}    
%>
<BR><BR><BR><BR>
<h2>Member Services Reports</h2>
<BR>
<BR>
<table colspec="C25 C25 C25">   

<!-- Title Line -->
<TR>
<TD><b>Member Information</b></TD><TD></TD><TD><b>Contract Information</b></TD><TD></TD><TD><b>Territory Detail</b></TD>
</TR>

<!-- Line 1 -->
<TR>
<TD><a href="MemberStatusChange.jsp">Member Change</a></TD><TD></TD>
<TD><a href="ContractStatus.jsp">Contract Status</a></TD><TD></TD>
<%
if (UserRole.equals("MGT") || UserRole.equals("INT") || UserRole.equals("RVP") )
{
%>
     <TD><a href="OrderGatherer.jsp">Order Gatherer Reports</a></TD>
<%
}
else if (UserRole.equals("FBC") )
{
%>
        <TD><a href="MonthlyFeeRevenue.jsp">Monthly Fee Revenue</a></TD>
<%
}
%>
</TR>

<!-- Line 2 -->
<TR>
<TD><a href="MemberProfile.jsp">Member Profile</a></TD><TD></TD>
<TD><a href="ContractRollup.jsp">Contract Rollup</a></TD><TD></TD>
<%
if (UserRole.equals("MGT") || UserRole.equals("RVP") )
{
%>
     <TD><a href="MonthlyFeeRevenue.jsp">Monthly Fee Revenue</a></TD>
<%
}
else if (UserRole.equals("FBC") )
{
%>
     <TD><a href="TransmissionRevenue.jsp">Transmission Revenue</a></TD>
<%
}
else
{
%>
        <TD></TD>
<%
}
%>
</TR>

<!-- Line 3 -->
<TR>
<TD><a href="MemberNotes.jsp">Member Notes</a></TD><TD></TD>
<TD></TD><TD></TD>
<%
if (UserRole.equals("MGT") || UserRole.equals("RVP") )
{
%>
        <TD><a href="TransmissionRevenue.jsp">Transmission Revenue</a></TD>      
<%
}
else if (UserRole.equals("FBC"))
{
%>
        <TD><a href="HardSoftRevenue.jsp">Hardware/Software Revenue</a></TD>
<%
}
else
{
%>
        <TD></TD>
<%
}
%>
</TR>

<!-- Line 4 -->
<TR>
<TD><a href="ParentChildRelationship.jsp">Parent-Child Relationship</a></TD>
<TD></TD><TD></TD><TD></TD>
<%
if (UserRole.equals("MGT") || UserRole.equals("RVP") )
{
%>
         <TD><a href="HardSoftRevenue.jsp">Hardware/Software Revenue</a></TD>
<%
}
else if (UserRole.equals("FBC"))
{
%>
        <TD><a href="SendingRevenue.jsp">Sending Revenue</a></TD>
<%
}
else
{
%>
        <TD></TD>
<%
}
%>
</TR>

<!-- Line 5 -->
<TR>
<TD><a href="FloristProfitabilityScreen.jsp">Member Florist Profitability</a></TD>
<TD></TD><TD></TD><TD></TD>
<%
if (UserRole.equals("MGT") || UserRole.equals("RVP") )
{
%>
   <TD><a href="SendingRevenue.jsp">Sending Revenue</a></TD>
<%
}
else
{
%>
   <td></TD>
<%
}	
%> 
</TR>

<!-- Line 6 -->

<%
if (UserRole.equals("MGT") || UserRole.equals("RVP") || UserRole.equals("INT"))
{
%>
    <TR>
    <TD>
    <a href="FloristProfitabilityGroupScreen.jsp">Group Florist Profitability</a></td>
		<TD></TD>
<%
}
%>	
</TR>
</TABLE>
<BR>
<BR>
<INPUT type="button" name="MainMenu" value="Main Menu" onClick="window.location='ReportMenu.jsp'">
<BR><BR>

</center>
</FORM> 
</body>
</html>

