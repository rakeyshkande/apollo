<%@ page import="java.sql.*" %>
<html>
<head>
  <title>Administrator Main Menu</title>
  <script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.AMM.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT> 

</head>
<body onLoad="ValidateLogin()">
<FORM name="AMM" action=""> 
<%
//Set session variable for Change Password Processing
session.setAttribute("menu","AdminReport");

//Retrieve User Information
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwla" scope="session" class="FTDI.DWLoginAttempts" />

<%
//Open connection to database
Connection con = db.Connect();

//Update Login Attempts to 0 after successful login
dwla.ResetLoginAttempt(con, UserID);

if(con!=null)
    db.Close(con);
%>

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<%
if (UserRole.equals("ADMIN"))
{
%>
<div align="right"><a href="ChangePassword.jsp">Change Password</a></div>
<BR><BR><BR><BR>
<h2>Data Warehouse Maintenance</h2>
<div align="center"><a href="EnableDWAccount.jsp">Enable Data Warehouse User Account</a></div>
<BR>
<div align="center"><a href="ResetPassword.jsp">Reset User Password</a></div>
<BR>
<div align="center"><a href="RoleAdmin.jsp">Role Administration</a></div>
<BR>
<div align="center"><a href="GlobalParmAdmin.jsp">Global Parameters Administration</a></div>
<%
}
else
{
%>
<BR><BR><BR><BR><BR>
<h2>Data Warehouse Maintenance</h2>
<div align="center"><a href="EnableDWAccount.jsp">Enable Data Warehouse User Account</a></div>
<BR>
<div align="center"><a href="ResetPassword.jsp">Reset User Password</a></div>
<BR><BR>
<%
}
%>
<BR>
<BR>
<INPUT type="button" name="Logout" value="Logout" onClick="window.location='Logout.jsp'">
<BR><BR>

</center>
</form> 
</body>
</html>

