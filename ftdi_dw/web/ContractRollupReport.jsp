<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Contract Rollup Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
              {
                        if ( document.CRRTest.bdate.value == "" )
                        {
                                alert('Enter Beginning Date in format specified');
                                close();
                                                }
                        else if ( document.CRRTest.vbdate.value == "false" )
                        {
                                alert('Enter valid Beginning Date in format specified');
                                close();
                        }
                        else if ( document.CRRTest.edate.value == "" )
                        {
                                alert('Enter Ending Date in format specified');
                                close();
                        }
                        else if ( document.CRRTest.vedate.value == "false" )
                        {
                                alert('Enter valid Ending Date in format specified');
                                close();
                        }
                        else if ( document.CRRTest.role.value == "INT" || document.CRRTest.role.value == "MGT")
                        {
                                 if (document.CRRTest.terr.value == "" && document.CRRTest.reg.value == "")
                                 {
                                        alert('Select a Territory or Region.');
                                        close();
                                 }
                                 else
                                 {
                                        document.CRR.submit(); 
                                 }
                        }
                        else
                        {
                                document.CRR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="CRRTest" action="ContractRollupReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String BeginDate = request.getParameter("BeginDate");
String EndDate = request.getParameter("EndDate");
String Territory = request.getParameter("Territory");
String Region = request.getParameter("Region");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="bdate" value="<%= BeginDate %>">
<input type="hidden" name="edate" value="<%= EndDate %>">
<input type="hidden" name="terr" value="<%= Territory %>">
<input type="hidden" name="reg" value="<%= Region %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/> 
<%
String retBeginVal = "false";
String retEndVal = "false";

//Open Connection to database
Connection con = db.Connect();

// Test database Connection
if (con != null)
{
 // Validate Dates
        retBeginVal = dateutil.validateDate(con,BeginDate);
        retEndVal   = dateutil.validateDate(con,EndDate);
        %>
        <input type="hidden" name= "vbdate" value="<%= retBeginVal %>">
        <input type="hidden" name= "vedate" value="<%= retEndVal %>">
        <%
}
%>
     
</FORM>
<FORM name="CRR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "ContractRollup.jsp";

if ( UserRole.equals("FBC") )
{
     if ( !(BeginDate.equals("")) && !(EndDate.equals("")) && 
                    retBeginVal.equals("true") && retEndVal.equals("true") )
     {
            ReportCall = ReportCall + "&p_date1=" + BeginDate;
            ReportCall = ReportCall + "&p_date2=" + EndDate;
            ReportCall = ReportCall + "&p_territory=" + UserTerritory;
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
            <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
     <%
     }
}
else if( UserRole.equals("RVP") )
{
     if ( !(BeginDate.equals("")) && !(EndDate.equals(""))&&
                retBeginVal.equals("true") && retEndVal.equals("true") ) 
       {
            ReportCall = ReportCall + "&p_date1=" + BeginDate;
            ReportCall = ReportCall + "&p_date2=" + EndDate;
            if ( Territory.equals("All") )
            {
                 ReportCall = ReportCall + "&p_division=" + UserRegion;
            }
            else
            {
                 ReportCall = ReportCall + "&p_territory=" + Territory;
            }
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
            <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
     <%
     }
}
else if ( UserRole.equals("INT") || UserRole.equals("MGT") )
{
     if ( !(BeginDate.equals("")) && !(EndDate.equals(""))&&
            (retBeginVal.equals("true")) && (retEndVal.equals("true")) &&
                ( !(Territory.equals("")) || !(Region.equals("")))  )
     {
            ReportCall = ReportCall + "&p_date1=" + BeginDate;
            ReportCall = ReportCall + "&p_date2=" + EndDate;
            if ( !(Territory.equals("")) &&  !(Territory.equals("All")) )
            {
                 ReportCall = ReportCall + "&p_territory=" + Territory;
            }
            
            if ( !(Region.equals("")) && !(Region.equals("All")) )
            {
                 ReportCall = ReportCall + "&p_division=" + Region;
            }
            
            if ( Region.equals("All") || Territory.equals("All") )
            {
                 ReportCall = ReportCall + "&p_territory=All&p_division=All";
            }

            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
            <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
     <%
     }
}
else
{
        //Do not display anything on page
}
if(con!=null)
    db.Close(con);
%>
</form>
</body>
</html>
