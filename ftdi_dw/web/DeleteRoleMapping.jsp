<%@ page import="java.sql.*" %>
<html>
<head>
  <title>Data Warehouse Role Maintenance</title>
  <script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	 if (document.DeleteMapping.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>  
</head>
<body onLoad="ValidateLogin()">
<FORM name="DeleteMapping" method="post" action="ConfirmDeleteRoleMapping.jsp"> 
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<center>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwr" scope="session" class="FTDI.DWRoleAdmin" />

<%
//Open connection to database  
Connection con = db.Connect();
%>

<BR><BR><BR><BR>
<h2>Delete AS400 To DW Role Mapping</h2>
<BR><BR>
<%
//Retrieve AS400 Roles from Database
ResultSet rs = dwr.AS400Roles(con);
%>
Select AS400 Role:&nbsp;<Select name="AS400Roles">
<OPTION SELECTED value="">
<%
while (rs.next())
{
 		String AS400RoleValue = rs.getString("AS400_Role");
		%>
		<OPTION value="<%= AS400RoleValue %>"><%= AS400RoleValue %>
		<%
}
if(con!=null)
    db.Close(con);
%>
</SELECT>
<BR><BR><BR>
<INPUT type="submit" name="Delete" value="Delete Mapping">
<INPUT type="button" name="Cancel" value="Cancel" onClick="window.location='RoleAdmin.jsp'">

</center>
</FORM> 
</body>
</html>

<script type="text/javascript" language="JavaScript">
	
	function DWRolesChange()
	{
			if (document.DeleteMapping.DWRoles.selectedIndex != 0)
			{
			 	 document.DeleteMapping.NewDWRole.value = "";
			}
	}
	
	function NewDWRoleChange()
	{		
			if (document.DeleteMapping.NewDWRole.value != "")
			{ 	 
					 document.DeleteMapping.DWRoles.selectedIndex = 0;
			}
	}
</Script>

