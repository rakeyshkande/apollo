<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Source Codes Report</title>
<SCRIPT type="text/javascript" Language="JavaScript">
            function SubmitMe()
              {
                        if ( document.SCRTest.report.value == "" )
                        {
                             alert('Select Source Code Report to run.');
                             close(); 
                        }
                        else if ( document.SCRTest.report.value == "All" )
                        {
                                 if ( document.SCRTest.bdate.value == "" )
                                 {
                                        alert('Enter Beginning Date in format specified.');
                                        close();
                                 }
                                                                 else if ( document.SCRTest.vbdate.value == "false" )
                                 {
                                        alert('Enter valid Beginning Date in format specified.');
                                        close();
                                 }
                                 else if ( document.SCRTest.edate.value == "" )
                                 {
                                        alert('Enter Ending Date in format specified.');
                                        close();
                                 }
                                                                 else if ( document.SCRTest.vedate.value == "false" )
                                 {
                                        alert('Enter valid Ending Date in format specified.');
                                        close();
                                 }
                                 else if ( document.SCRTest.reporttype.value == "" )
                                 {
                                        alert('Select Report Type.');
                                        close();
                                 }
                                 else
                                 {
                                         document.SCR.submit();
                                 }                               
                        }
                        else if ( document.SCRTest.report.value == "Multi" )
                        {
                                 if ( document.SCRTest.reporttype.value == "" )
                                 {
                                        alert('Select Report Type.');
                                        close();
                                 }
                                 else if ( document.SCRTest.sourcecodelist.value == "" )
                                 {
                                        alert('Select at least one Source Code.');
                                        close();
                                 }
                                 else
                                 {
                                         document.SCR.submit();
                                 }                               
                        }
                        else
                        {
                                 document.SCR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="SCRTest" action="SourceCodesReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String BeginDate = request.getParameter("BeginDate");
String EndDate = request.getParameter("EndDate");
String Report = request.getParameter("Report");
String SourceCodeList = request.getParameter("SourceCodeList");
String ReportType = request.getParameter("ReportType");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="bdate" value="<%= BeginDate %>">
<input type="hidden" name="edate" value="<%= EndDate %>">
<input type="hidden" name="report" value="<%= Report %>">
<input type="hidden" name="reporttype" value="<%= ReportType %>">
<input type="hidden" name="sourcecodelist" value="<%= SourceCodeList %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retBeginVal = "false";
String retEndVal   = "false";

//Open Connection to database
Connection con = db.Connect();

// Test database connection
if (con != null)
{
     // Validate Dates
            retBeginVal = dateutil.validateDate(con, BeginDate);
            retEndVal   = dateutil.validateDate(con, EndDate);
            db.Close(con);
            %>
            <input type="hidden" name="vbdate" value="<%= retBeginVal %>">
            <input type="hidden" name="vedate" value="<%= retEndVal %>">
            <%
}
%>          


</FORM>
<FORM name="SCR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall;

if ( (UserRole.equals("FBC") || UserRole.equals("RVP") || UserRole.equals("INT") || UserRole.equals("MGT")) && !(Report.equals("")) )
{
     if ( Report.equals("All") )
     {
            if ( !(ReportType.equals("")) && !(BeginDate.equals("")) && !(EndDate.equals("")) 
                           && (retBeginVal.equals("true")) && (retEndVal.equals("true")) )
            {
                 ReportCall = "SourceCodeReport.rdf";
                 ReportCall = ReportCall + "&p_date1=" + BeginDate;
                 ReportCall = ReportCall + "&p_date2=" + EndDate;
                 ReportCall = ReportCall + "&p_report_type=" + ReportType;
                 %>
                 <BR><BR><BR><BR>
                 <center>
                 <H2>Processing Report.....</H2>
                 <BR><BR><BR><BR><BR>
                 <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                 <BR> 
                 you have requested your report may take several minutes to run.
                 </i></font>
                 </center>
                 <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                 <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                 <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                     Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                 <input type="hidden" name="destype" value="cache">
                 <%
            }
     }
     else
     {
          if ( !(ReportType.equals("")) && !(SourceCodeList.equals("")) )
            {
                 ReportCall = "SMSourceCodeReport.rdf";
                 ReportCall = ReportCall + "&p_report_type=" + ReportType;
                 String FinalList = SourceCodeList.substring(0, (SourceCodeList.length()-1) );
                 ReportCall = ReportCall + "&p_source_code=" + FinalList;
                 %>
                 <BR><BR><BR><BR>
                 <center>
                 <H2>Processing Report.....</H2>
                 <BR><BR><BR><BR><BR>
                 <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                 <BR> 
                 you have requested your report may take several minutes to run.
                 </i></font>
                 </center>
                 <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                 <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                 <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                     Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                 <input type="hidden" name="destype" value="cache">
                 <%
            }
     }
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
