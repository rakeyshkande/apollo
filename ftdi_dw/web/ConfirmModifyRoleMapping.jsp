<%@ page import="java.sql.*" %>
<html>
<head>
  <title>Data Warehouse Role Maintenance</title>
</head>
<body>
<FORM name="ConfirmModifyMapping" action=""> 
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<center>
<BR><BR><BR><BR>
<h2>Modify AS400 To DW Role Mapping</h2>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwr" scope="session" class="FTDI.DWRoleAdmin" />

<%
//Open connection to database  
Connection con = db.Connect();

String AS400Role = request.getParameter("AS400Roles");
String DWRoleSelect = request.getParameter("DWRoles");
String NewDWRole = request.getParameter("NewDWRole");

if (DWRoleSelect.equals(""))
{
 	 //Modify Mapping using new DW Role
	 dwr.ModifyDWRoleMapping(con, NewDWRole, AS400Role);
	 %>
	 The AS400 Role: <%= AS400Role %> has been mapped to the DW Role: <%= NewDWRole %>
	 <%
}
else
{
 	 //Modify Mapping using Old DW Role
	 dwr.ModifyDWRoleMapping(con, DWRoleSelect, AS400Role);
	 %>
	 The AS400 Role: <%= AS400Role %> has been mapped to the DW Role: <%= DWRoleSelect %>
	 <%
}
if(con!=null)
    db.Close(con);
%>

<BR><BR>
<INPUT type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">

</center>
</form> 
</body>
</html>

