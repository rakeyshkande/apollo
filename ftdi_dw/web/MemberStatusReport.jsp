<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Member Change Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
                {
                      var begindate = document.MSRTest.mscbd.value;
                      var enddate   = document.MSRTest.msced.value;
                        
                        if (document.MSRTest.mscmember.value == "")
                        {
                                if(document.MSRTest.mscbd.value == "")
                                {
                                        alert('Enter Beginning Date in format specified');
                                        close();
                                }
                                                                else if(document.MSRTest.vbdate.value == "false")
                                {
                                        alert('Enter valid Beginning Date in format specified');
                                        close();
                                }
                                else if(document.MSRTest.msced.value == "")
                                {
                                         alert('Enter Ending Date in format specified');
                                         close();
                                }
                                                                else if(document.MSRTest.vedate.value == "false")
                                {
                                        alert('Enter valid Ending Date in format specified');
                                        close();
                                }
                                else if(document.MSRTest.urole.value == "INT" || document.MSRTest.urole.value == "MGT")
                                {
                                         if( (document.MSRTest.mscter.value == "") && (document.MSRTest.mscreg.value == "") )
                                         {
                                                 alert('Select a Territory or Region');
                                                 close();
                                         }
                                         else
                                         {
                                                 document.MSR.submit();
                                         }   
                                }
                                else
                                {
                                        document.MSR.submit();
                                }
                        }
                        else
                        {
                                document.MSR.submit();
                        }
                }
        </script>
</head>
<body onLoad="SubmitMe()">
<FORM name="MSRTest" action="MemberStatusReport.jsp">
<%
String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String UserRole = (String) session.getAttribute("role");
String MSCBeginDate = (request.getParameter("BeginDate")).trim();
String MSCEndDate = (request.getParameter("EndDate")).trim();
String MSCTerritory = request.getParameter("Territory");
String MSCRegion = request.getParameter("Region");
String MSCMemberCode = ((request.getParameter("MemberCode")).trim()).toUpperCase();
%>
<input type="hidden" name="mscbd" value="<%= MSCBeginDate %>">
<input type="hidden" name="msced" value="<%= MSCEndDate %>">
<input type="hidden" name="mscter" value="<%= MSCTerritory %>">
<input type="hidden" name="mscreg" value="<%= MSCRegion %>">
<input type="hidden" name="mscmember" value="<%= MSCMemberCode %>">
<input type="hidden" name="urole" value="<%= UserRole %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retBeginVal = "false";
String retEndVal = "false";

//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
     //Validate Dates
                            retBeginVal = dateutil.validateDate(con, MSCBeginDate);
                            retEndVal   = dateutil.validateDate(con, MSCEndDate);
                            %> 
                            <input type="hidden" name= "vbdate" value="<%= retBeginVal %>">
                            <input type="hidden" name= "vedate" value="<%= retEndVal %>">
                            <%
     db.Close(con);
}
%>                      

</FORM>
<FORM name="MSR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = null;
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");


//Build Parms for FBC Report
if ( UserRole.equals("FBC") )
{
     ReportCall = "MemberChangeReportTerr.jsp";
     
     if ( MSCMemberCode.equals("") )
     {
            ReportCall = ReportCall + "&p_date1=" +  MSCBeginDate;
            ReportCall = ReportCall + "&p_date2=" +  MSCEndDate;
            ReportCall = ReportCall + "&p_territory=" + UserTerritory;
     }
     else
     {
             ReportCall = ReportCall + "&p_member=" + MSCMemberCode;
             ReportCall = ReportCall + "&p_territory=" + UserTerritory;
             if( !(MSCBeginDate.equals("")) && (retBeginVal.equals("true")) )
             {
                     ReportCall = ReportCall + "&p_date1=" + MSCBeginDate;
             }
             if( !(MSCEndDate.equals("")) && (retEndVal.equals("true")) )
             {
                     ReportCall = ReportCall + "&p_date2=" + MSCEndDate;
             }
        }
}
else if ( UserRole.equals("RVP") )
{
     ReportCall = "MemberChangeReportDiv.jsp";
     
     if ( MSCMemberCode.equals("") )
     {
            ReportCall = ReportCall + "&p_date1=" +  MSCBeginDate;
            ReportCall = ReportCall + "&p_date2=" +  MSCEndDate;
            if ( MSCTerritory.equals("All") )
            {
                 ReportCall = ReportCall + "&p_division=" + UserRegion;
            }
            else
            {
                    ReportCall = ReportCall + "&p_territory=" + MSCTerritory;
            }
     }
     else
     {
            ReportCall = ReportCall + "&p_member=" + MSCMemberCode;
            if ( MSCTerritory.equals("All") )
            {
                 ReportCall = ReportCall + "&p_division=" + UserRegion;
            }
            else
            {
                    ReportCall = ReportCall + "&p_territory=" + MSCTerritory;
            }
            if( !(MSCBeginDate.equals("")) && (retBeginVal.equals ("true")) )
            {
                     ReportCall = ReportCall + "&p_date1=" + MSCBeginDate;
            }
            if( !(MSCEndDate.equals(""))&& (retEndVal.equals ("true")) )
            {
                     ReportCall = ReportCall + "&p_date2=" + MSCEndDate;
            }
        }
}
else
{
        ReportCall = "MemberChangeReportInt.jsp";
        
        if ( MSCMemberCode.equals("") )
        {
            ReportCall = ReportCall + "&p_date1=" +  MSCBeginDate;
            ReportCall = ReportCall + "&p_date2=" +  MSCEndDate;
            if ( !(MSCTerritory.equals("")) )
            {
                        ReportCall = ReportCall + "&p_territory=" + MSCTerritory;
            }
            
            if ( !(MSCRegion.equals("")) )
            {
                        ReportCall = ReportCall + "&p_division=" + MSCRegion;
            }
        }
        else
        {
            ReportCall = ReportCall + "&p_Member=" + MSCMemberCode;
            
            if ( !(MSCBeginDate.equals("")) && (retBeginVal.equals ("true")))
            {
                    ReportCall = ReportCall + "&p_date1=" +  MSCBeginDate;
            }
            
            if  ( !(MSCEndDate.equals(""))&& (retEndVal.equals ("true")) )
            {
                    ReportCall = ReportCall + "&p_date2=" +  MSCEndDate;
            }
            
            if ( !(MSCTerritory.equals("")) && !(MSCTerritory.equals("All")) )
            {
                        ReportCall = ReportCall + "&p_territory=" + MSCTerritory;
            }
            else
            {
                    if ( (MSCRegion.equals("")) || (MSCRegion.equals("All")) )
                    {
                         ReportCall = ReportCall + "&p_territory=All";
                    }
            }
            
            if ( !(MSCRegion.equals("")) && !(MSCRegion.equals("All")) )
            {
                    ReportCall = ReportCall + "&p_division=" + MSCRegion;
            }
            else
            {
                    if ( (MSCTerritory.equals("")) || (MSCTerritory.equals("All")) )
                    {
                         ReportCall = ReportCall + "&p_division=All";
                    }
            }
        }
}

if ( UserRole.equals("FBC") || UserRole.equals("RVP") ) 
{
     if ( MSCMemberCode.equals("") && !(MSCBeginDate.equals("")) && !(MSCEndDate.equals("")) &&
            (retBeginVal.equals("true")) && (retEndVal.equals("true")) )
     {
     %>
                 <BR><BR><BR><BR>
                 <center>
                 <H2>Processing Report.....</H2>
                 <BR><BR><BR><BR><BR>
                 <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                 <BR> 
                 you have requested your report may take several minutes to run.
                 </i></font>
                 </center>
                 <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                 <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                 <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                     Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                 <input type="hidden" name="destype" value="cache">
        <%
        }
        else if ( !(MSCMemberCode.equals("")) )
        {
        %>
                 <BR><BR><BR><BR>
                 <center>
                 <H2>Processing Report.....</H2>
                 <BR><BR><BR><BR><BR>
                 <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                 <BR> 
                 you have requested your report may take several minutes to run.
                 </i></font>
                 </center>
                 <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                 <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                 <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                 <input type="hidden" name="destype" value="cache">
       <%
        }
        else
        {
    }
}
else if( UserRole.equals("INT") || UserRole.equals("MGT") )
{
        if ( MSCMemberCode.equals("") && !(MSCBeginDate.equals("")) && !(MSCEndDate.equals("")) &&
                     (retBeginVal.equals("true")) && (retEndVal.equals("true")) &&
                   ( !(MSCTerritory.equals("")) || !(MSCRegion.equals(""))) )
        {
          %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
        <%
        }
        else if ( !(MSCMemberCode.equals("")) )
        {
        %>
                <BR><BR><BR><BR>
                <center>
                <H2>Processing Report.....</H2>
                <BR><BR><BR><BR><BR>
                <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                <BR> 
                you have requested your report may take several minutes to run.
                </i></font>
                </center>
                <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                <input type="hidden" name="destype" value="cache">
    <%
        }
        else
        {
        }
}
else
{
 //Do not display anything on page
}
%>
</FORM>
</body>
</html>
