<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<html>
<head>
  <title> Data Warehouse User Account Maintenance</title>
</head>
<body>
<FORM name="ConfirmYesUserAccount" method="post" action="AdminMainMenu.jsp">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="center">
<center>
<h2>Enable User Account for Data Warehouse</h2>
<BR><BR><BR><BR>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwla" scope="session" class="FTDI.DWLoginAttempts" />

   <% 
   //Get the UserID that was entered 
   String UserID = (String)session.getAttribute("userid");
	 
	 //Check for session Timeout
	 if (UserID==null)
	 {
	 %>
	 	 	<jsp:forward page="LoginScreen.jsp"/>
	 <% 
	 }

   //Open connection to database  
   Connection con = db.Connect();

   //Update DW Login Attempts to enable DW account.
   dwla.ResetLoginAttempt(con, UserID);
   
   // Close Database Connetion
   if(con!=null)
   	 db.Close(con);
   %>
   Caller's Data Warehouse Account has been enabled.  
   <BR><BR>
   <input type="submit" value="Main Menu">   
   </center>
</FORM>
</body>
</html>

