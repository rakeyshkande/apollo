<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>Sending Revenue Report</title>
      <SCRIPT type="text/javascript" Language="JavaScript">
            function SubmitMe()
              {
                        if ( document.SRRTest.bmonth.value == "" )
                        {
                                alert('You must select a Beginning Month');
                                close();
                        }
                        else if ( document.SRRTest.byear.value == "" )
                        {
                                alert('You must select a Beginning Year');
                                close();
                        }
                        else if ( document.SRRTest.emonth.value == "" )
                        {
                                alert('You must select a Ending Month');
                                close();
                        }
                        else if ( document.SRRTest.eyear.value == "" )
                        {
                                alert('You must select a Ending Year');
                                close();
                        }
                        else if ( document.SRRTest.role.value == "MGT")
                        {
                                 if (document.SRRTest.terr.value == "" && document.SRRTest.reg.value == "")
                                 {
                                        alert('You must select a Territory or Region.');
                                        close();
                                 }
                                 else
                                 {
                                         document.SRR.submit();
                                 }
                        }
                        else
                        {
                                document.SRR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="SRRTest" action="SendingRevenueReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String BeginMonth = request.getParameter("BeginMonth");
String BeginYear = request.getParameter("BeginYear");
String EndMonth = request.getParameter("EndMonth");
String EndYear = request.getParameter("EndYear");
String Territory = request.getParameter("Territory");
String Region = request.getParameter("Region");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="bmonth" value="<%= BeginMonth %>">
<input type="hidden" name="byear" value="<%= BeginYear %>">
<input type="hidden" name="emonth" value="<%= EndMonth %>">
<input type="hidden" name="eyear" value="<%= EndYear %>">
<input type="hidden" name="terr" value="<%= Territory %>">
<input type="hidden" name="reg" value="<%= Region %>">
<input type="hidden" name="role" value="<%= UserRole %>">
</FORM>


<FORM name="SRR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "SendingRevenueReport.jsp";

if ( UserRole.equals("FBC") )
{
     if ( !(BeginMonth.equals("")) && !(BeginYear.equals("")) && !(EndMonth.equals("")) && !(EndYear.equals("")) )
     {
            ReportCall = ReportCall + "&p_month1=" + BeginMonth;
            ReportCall = ReportCall + "&p_year1=" + BeginYear;
            ReportCall = ReportCall + "&p_month2=" + EndMonth;
            ReportCall = ReportCall + "&p_year2=" + EndYear;
            ReportCall = ReportCall + "&p_territory=" + UserTerritory;
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
            <%
    }
}
else if( UserRole.equals("RVP") )
{
     if ( !(BeginMonth.equals("")) && !(BeginYear.equals("")) && !(EndMonth.equals("")) && !(EndYear.equals("")) )
     {
            ReportCall = ReportCall + "&p_month1=" + BeginMonth;
            ReportCall = ReportCall + "&p_year1=" + BeginYear;
            ReportCall = ReportCall + "&p_month2=" + EndMonth;
            ReportCall = ReportCall + "&p_year2=" + EndYear;

            if ( Territory.equals("All") )
            {
                 ReportCall = ReportCall + "&p_division=" + UserRegion;
            }
            else
            {
                    ReportCall = ReportCall + "&p_territory=" + Territory;
            }
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
            <%
    }
}
else if ( UserRole.equals("MGT") )
{
     if ( !(BeginMonth.equals("")) && !(BeginYear.equals("")) && !(EndMonth.equals("")) && !(EndYear.equals("")) && ( !(Territory.equals("")) || !(Region.equals(""))) )
     {
            ReportCall = ReportCall + "&p_month1=" + BeginMonth;
            ReportCall = ReportCall + "&p_year1=" + BeginYear;
            ReportCall = ReportCall + "&p_month2=" + EndMonth;
            ReportCall = ReportCall + "&p_year2=" + EndYear;

            if ( !(Territory.equals("")) )
            {
                 ReportCall = ReportCall + "&p_territory=" + Territory;
            }
            
            if ( !(Region.equals("")) )
            {
                    ReportCall = ReportCall + "&p_division=" + Region;
            }
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
            <%
    }
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
