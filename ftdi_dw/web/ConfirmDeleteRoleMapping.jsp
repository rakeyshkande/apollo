<%@ page import="java.sql.*" %>
<html>
<head>
  <title>Data Warehouse Role Maintenance</title>
</head>
<body>
<FORM name="ConfirmDeleteMapping" action=""> 
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<center>
<BR><BR><BR><BR>
<h2>Delete AS400 To DW Role Mapping</h2>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwr" scope="session" class="FTDI.DWRoleAdmin" />

<%
//Open connection to database  
Connection con = db.Connect();

String AS400Role = request.getParameter("AS400Roles");

//Delete AS400 Mapping 
dwr.DeleteDWRoleMapping(con, AS400Role);
if(con!=null)
    db.Close(con);
%>

The AS400 Role <%= AS400Role %> has been deleted from the Data Warehouse

<BR><BR>
<INPUT type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">

</center>
</form> 
</body>
</html>

