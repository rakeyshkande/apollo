<%@ page import="java.sql.*" %>
<html>
<head>
  <title>Data Warehouse Role Maintenance</title>
  <script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	 if (document.AddMapping.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>  
</head>
<body onLoad="ValidateLogin()">
<FORM name="AddMapping" method="post" action="ConfirmAddRoleMapping.jsp"> 
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<center>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dwr" scope="session" class="FTDI.DWRoleAdmin" />

<%
//Open connection to database  
Connection con = db.Connect();
%>

<BR><BR><BR><BR>
<h2>Add AS400 To DW Role Mapping</h2>
<BR><BR>
<%
//Retrieve AS400 Roles from Database
ResultSet rs = dwr.AS400PWDRoles(con);
%>
Select AS400 Role:&nbsp;<Select name="AS400Roles">
<OPTION SELECTED value="">
<%
while (rs.next())
{
 		String AS400Role = rs.getString("Group_Profile");
		%>
		<OPTION value="<%= AS400Role %>"><%= AS400Role %>
		<%
}
%>
</SELECT>
<BR><BR><BR>
<%
//Retrieve Current DW Roles from Database
rs = null;
rs = dwr.DWRoles(con);
%>
Select A Current DW Role:&nbsp;<SELECT name="DWRoles" onChange="DWRolesChange()">
<OPTION SELECTED value="">
<%
while (rs.next())
{
 			String DWRole = rs.getString("DW_Role");
			%>
			<OPTION value="<%= DWRole %>"><%= DWRole %>
			<%
}

if(con!=null)
    db.Close(con);
%>
</SELECT>
<BR>
Or
<BR>
Enter a new DW Role:&nbsp;<input type="text" name="NewDWRole" size="10" onChange="NewDWRoleChange()"> 
<BR><BR>
<INPUT type="submit" name="Add" value="Add Mapping">
<INPUT type="button" name="Cancel" value="Cancel" onClick="window.location='RoleAdmin.jsp'">
  
</center>
</FORM>
</body>
</html>

<script type="text/javascript" language="JavaScript">
	
	function DWRolesChange()
	{
			if (document.AddMapping.DWRoles.selectedIndex != 0)
			{
			 	 document.AddMapping.NewDWRole.value = "";
			}
	}
	
	function NewDWRoleChange()
	{		
			if (document.AddMapping.NewDWRole.value != "")
			{ 	 
					 document.AddMapping.DWRoles.selectedIndex = 0;
			}
	}
</Script>


