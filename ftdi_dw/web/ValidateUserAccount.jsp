<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<html>
<head>
	  <title> Data Warehouse User Account Maintenance</title>
</head>
<body>
<FORM name="ValidateUserAccount" action="">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="center">
<center>
<h2>Enable User Account for Data Warehouse</h2>
<BR><BR><BR><BR>

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="vua" scope="session" class="FTDI.ValidateUserAccount" />
   
   <% 
   //Get the UserID that was entered 
   String UserId = (request.getParameter("UserID")).toUpperCase();
   session.setAttribute("userid", UserId);
   
   //Open connection to database  
   Connection con = db.Connect();

   //Retrieve AS/400 account status and User Name
   ResultSet rs = vua.DWAcctInfo(con, UserId);
   
   if( rs.next() != false)
   {
   	   String AS400Status = (rs.getString("STATUS")).trim();

   	   if ( AS400Status.equalsIgnoreCase("DISABLED") )
   	   {
   	   	  	%>
	   	  	Caller's AS/400 account is disabled.
		  	<BR>  
		  	Caller must contact AS/400 Administrator to enable their AS/400 account.
		  	<BR><BR>
			  <input type="button" name="Logout" value="Logout" onClick="window.location='LoginScreen.jsp'">
	   	  	  <%
	    }
   		else
   		{
   	   	 	%>
	   	 	Confirm Caller's name is: <%= rs.getString("USER_NAME") %>
		 	<BR><BR>
			<TABLE>
			<TR>
			<TD>  
		 	<input type="button" name="ConfirmYes" value="Yes" onClick="window.location='ConfirmYesUserAccount.jsp'">
			</TD>
			<TD></TD>
			<TD>
		 	<input type="button" name="ConfirmNo" value=" No " onClick="window.location='ConfirmNoUserAccount.jsp'">
			</TD>
			</TR>
			</TABLE>		 	
	   		<%
   		}
	}
	else
	{
	 	%>
		Caller's AS/400 account is not found on the database.
		<BR>  
		Please have the Caller contact their manager to request an AS/400 account.
		<BR><BR>
		<input type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">		
	   	<%
   	}

   // Close Database Connetion
   if(con!=null)
       db.Close(con);
   %>
</center>
</FORM>
</body>
</html>

