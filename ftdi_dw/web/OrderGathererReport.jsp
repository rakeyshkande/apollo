<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head>
<title>Order Gatherer Report</title>
      <SCRIPT Language="JavaScript">
            function SubmitMe()
              {
                        if ( document.OGRTest.role.value == "INT" || document.OGRTest.role.value == "MGT" || document.OGRTest.role.value == "RVP")
                        { 
                                if ( document.OGRTest.ordertotal.value == "" )
                                {
                                 alert('You must enter a value for the Orders Out Total.');
                                     close();
                                }
                                else if ( document.OGRTest.ratio.value == "" )
                                {
                                     alert('You must enter a value for the Ratio of Orders.');
                                     close();
                                }
                                else if ( document.OGRTest.group.value == "" )
                                {
                                 alert('You must select a Group By option.');
                                     close();
                                }
                                else
                                {
                                        document.OGR.submit();
                                }
                        }
                        else
                        {
                                alert('You do not have the correct Role Permissions to run this report');
                                close();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="OGRTest">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String ReportName = request.getParameter("ReportName");
String Month = request.getParameter("CurrentMonth");
String Year = request.getParameter("CurrentYear");
String Country = request.getParameter("Country");
String OrderOutTotal = request.getParameter("OrderOutTotal");
String RatioOrders = request.getParameter("RatioOrders");
int DecimalLocation = RatioOrders.indexOf(".");
String DecimalRatio = "";
if (DecimalLocation > 0)
{
     DecimalRatio = RatioOrders.substring(DecimalLocation+1, RatioOrders.length());
     if (DecimalRatio.length() == 1)
     {
             RatioOrders = RatioOrders + "0";
   }
}
else if (DecimalLocation == 0)
{
         RatioOrders = "0" + RatioOrders;
}
else
{
     RatioOrders = RatioOrders + ".00";
}
String GroupBy = (String) request.getParameter("GroupBy");
String UserRole = (String) session.getAttribute("role");
%>

<input type="hidden" name="ordertotal" value="<%= OrderOutTotal %>">
<input type="hidden" name="ratio" value="<%= RatioOrders %>">
<input type="hidden" name="group" value="<%= GroupBy %>">
<input type="hidden" name="role" value="<%= UserRole %>">

</FORM>
<FORM name="OGR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall;

if ( UserRole.equals("MGT") || UserRole.equals("INT") || UserRole.equals("RVP"))
{
     if ( !(OrderOutTotal.equals("")) && !(RatioOrders.equals("")) && !(GroupBy.equals("")) )
     {
            ReportCall = ReportName + GroupBy + ".jsp";
            ReportCall = ReportCall + "&p_country_code=" + Country;
            ReportCall = ReportCall + "&p_out_order_total=" + OrderOutTotal;
            ReportCall = ReportCall + "&p_ratio=" + RatioOrders;
            %>
            <BR><BR><BR><BR>
            <center>
            <H2>Processing Report.....</H2>
            <BR><BR><BR><BR><BR>
            <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
            <BR> 
            you have requested your report may take several minutes to run.
            </i></font>
            </center>
            <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
            <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="130">
            <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
            <input type="hidden" name="desformat" value="pdf">
            <input type="hidden" name="destype" value="cache">
     <%
     }
}
else
{
        //Do not display anything on page
}
%>
</body>
</html>
