<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Product Sales vs. Inventory Report</title>
 <SCRIPT type="text/javascript" Language="JavaScript">
            function SubmitMe()
              {
                     //   var begindate = document.PSIRTest.bdate.value;
                     //   var enddate = document.PSIRTest.edate.value;
                      
                        if ( document.PSIRTest.bdate.value == "" )
                        {
                             alert('Enter Beginning Date in format specified');
                             close();
                        }
                        else if ( document.PSIRTest.vbdate.value == "false" )
                        {
                             alert('Enter valid Beginning Date in format specified');
                             close();
                        }
                        else if ( document.PSIRTest.edate.value == "" )
                        {
                             alert('Enter Ending Date in format specified');
                             close();
                        }
                        else if ( document.PSIRTest.vedate.value == "false" )
                        {
                             alert('Enter valid Ending Date in format specified');
                             close();
                        }
                        else if ( document.PSIRTest.codeyr.value == "" )
                        {
                                alert('Select Codified Year');
                                close();
                        }
                        else
                        {   
                             document.PSIR.submit();
                        }
                }
        </script>
</head>
<body onLoad="SubmitMe()">
<FORM name="PSIRTest" action="ProductSalesInventoryReport.jsp" >
<%
String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String PSIBDate = request.getParameter("BeginDate");
String PSIEDate = request.getParameter("EndDate");
String PSICodifiedYear = request.getParameter("year");
%>
<input type="hidden" name="bdate" value="<%= PSIBDate %>">
<input type="hidden" name="edate" value="<%= PSIEDate %>">
<input type="hidden" name="codeyr" value="<%= PSICodifiedYear %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retBeginVal = "false";
String retEndVal   = "false";

//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
     //Validate Dates
                            retBeginVal = dateutil.validateDate(con, PSIBDate);
                            retEndVal   = dateutil.validateDate(con, PSIEDate); 
                            %>
                            <input type="hidden" name="vbdate" value="<%= retBeginVal %>">
                            <input type="hidden" name="vedate" value="<%= retEndVal %>">
                            <%
     db.Close(con);
}
%>
 
</FORM>
<FORM name="PSIR" action="<bean:message key="report_url" />" method="post">
<%
String PSIOrderStatus = request.getParameter("OrderStatus");
String PSICodifiedProd = request.getParameter("CodifiedProducts");

if ( !(PSIBDate.equals("")) && (!(PSIEDate.equals(""))) &&
     (retBeginVal.equals("true")) && (retEndVal.equals("true")) &&
   (!(PSICodifiedYear.equals(""))) )
{
      String ReportCall = "ProductSalesInventoryReport.jsp";
      ReportCall = ReportCall + "&P_DATE1=" + PSIBDate;
      ReportCall = ReportCall + "&P_DATE2=" + PSIEDate;
      ReportCall = ReportCall + "&P_STATUS=" + PSIOrderStatus;
      ReportCall = ReportCall + "&p_CODIFIED_SYMBOL=" + PSICodifiedProd;
      ReportCall = ReportCall + "&p_codified_Year=" + PSICodifiedYear;
      %>
         <BR><BR><BR><BR>
         <center>
         <H2>Processing Report.....</H2>
         <BR><BR><BR><BR><BR>
         <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
         <BR> 
         you have requested your report may take several minutes to run.
        </i></font>
        </center>
        <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
        <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>">
        <%
             Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
             Iterator it = queryPairs.entrySet().iterator();
             while (it.hasNext()) {
                 Map.Entry pair = (Map.Entry) it.next();
         %>
         <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
         <%
             }
         %>
         <input type="hidden" name="desformat" value="html">
        <input type="hidden" name="destype" value="cache">
        <%
}
%>
</form>
</body>
</html>
