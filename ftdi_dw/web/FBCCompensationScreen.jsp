<%@ page import="java.sql.*" %>
<html>
<head>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
       if (document.FBC.userid.value == "null")
       {
          window.location="LoginScreen.jsp";
       }
  }
  function CheckRegion()  // entered a territory
  {
       if (document.FBC.Territory.selectedIndex != 0)
  	 {
            document.FBC.Region.selectedIndex = 0;
            document.FBC.Region.value = "";
         }
  }    
  function CheckTerr()  // entered a region
  {   
      if (document.FBC.Region.selectedIndex != 0)
        {
    	 document.FBC.Territory.selectedIndex = 0;
    	 document.FBC.Territory.value = "";
        }
  }

</SCRIPT>
<title>FBC Compensation Reports</title>
</head>
<body onload="ValidateLogin()">
<FORM name="FBC" method="post" action="FBCCompensationRptCall.jsp" target="FBCR">

<%
String UserID = (String) session.getAttribute("userid");

// Check for session Timeout
if (UserID==null)
{
%>
     <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");

//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>FBC Compensation Reports</H2>
<BR>

<i>The prior month FBC Compensation data is not available between the
<BR> 
1st and the 6th of the current month due to the Statement processing.</i>
<BR>
<BR>

<jsp:useBean id="td" scope="session" class="FTDI.TerritoryDetail" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<%
//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
   {
     if ( !(UserRole == null) )
     {
       if ( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("MGT") )
         {
%>
	   <TABLE>
           <TR><TD><b>Select:</b></TD></TR>           
           <TR>
           <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Month:</TD> 
           <TD>
           <Select name="Month">
           <OPTION SELECTED value="">-- Select a Month --
           <OPTION value="01">January
           <OPTION value="02">February
           <OPTION value="03">March
           <OPTION value="04">April
           <OPTION value="05">May
           <OPTION value="06">June
           <OPTION value="07">July
           <OPTION value="08">August
           <OPTION value="09">September
           <OPTION value="10">October
           <OPTION value="11">November
           <OPTION value="12">December
           </SELECT>
           </TD>
           </TR>
           
           <TR>
           <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Year:</TD>
           <TD>
           <Select name="Year">
           <OPTION SELECTED value="">-- Select a Year --
           <%
             ResultSet rs = td.MemberRevenueYears(con);
             while (rs.next())
               {
                 String RevenueYear = rs.getString("RevenueYear");
           %>
                 <OPTION value="<%= RevenueYear %>"><%= RevenueYear %>
           <%  
               }
           %> 
           </SELECT>
           </TD>           
           </TR>
      <%
       if ((UserRole.trim()).equals("RVP")) 
         {
           // Retrieve FBC Info from Database
              rs = null;
              rs = td.TerritoryInfo(con, UserID);
      %>
          <TR>
          <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
          <TD>
          <SELECT name="Territory"> 
          <OPTION SELECTED value="ALL">All Territories
      <%
        while(rs.next())
         {
	          String Territory = rs.getString("Territory");
	          String FBC_Name = rs.getString("FBC_Name");
      %>
          <OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
      <%  
         }
      %> 
          </SELECT>
          </TD>           
          </TR>
      <%
        }

       if ( (UserRole.trim()).equals("MGT") )
	 {
	   //Retrieve All Territory Info from Database
	   rs = null;
	   rs = td.AllTerritoryInfo(con);
      %>
  	 <TR>
	 <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
	 <TD>
	 <SELECT name="Territory" onChange="CheckRegion()"> 
	 <OPTION SELECTED value="">
	 <OPTION value="NOFBC">No FBC
         <OPTION value="FTD">All Territories / FTD
      <%
          while(rs.next())
	  {
	    String Territory = rs.getString("Territory");
	    String FBC_Name = rs.getString("FBC_Name");
      %>
         <OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
      <%
    	  }
      %>
	 </SELECT>
	 &nbsp;&nbsp;Or&nbsp;&nbsp;Region:
      <%
	 //Retrieve All Region Info from Database
	 rs = null;
	 rs = td.AllRegionInfo(con);
      %>
	 <SELECT name="Region" onChange="CheckTerr()"> 
	 <OPTION SELECTED value="">
	 <OPTION value="FTD">All Regions / FTD
      <%
       	  while(rs.next())
	 {
     	   String Region = rs.getString("Region");
	   String Mgr_Name = rs.getString("Manager_Name");
      %>
  	 <OPTION value="<%= Region %>"><%= Region + '-' + Mgr_Name %>
      <%
	 }
      %>
	 </SELECT>
	 </TD>
	 </TR>
     <%
         }
     %>
   <TR>
   <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Report Option:</TD>
   <TD>
   <Select name="ReportType">
   <OPTION SELECTED value="">-- Select a Report -- 
   <OPTION value="CSR">Compensation Summary 
   <OPTION value="TSR">Territory Summary 
   <OPTION value="PPR">Promotional Programs Summary
   <OPTION value="DAR">Detail Adds - Individual Territories Only
   <OPTION value="LTMR">LTM Summary
   </SELECT>
   </TD>
   </TR>
   </table>
          
    <BR><BR> 
    <INPUT TYPE="submit" VALUE="Run Report">
    <INPUT TYPE="button" VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
    <BR><BR>
    <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
    <%  
    }
  else
    {
  %>
        Currently your account does not have the correct role permissions to run this report.
        <BR>
        Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
        <BR><BR>
        <INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
<%
     }
   }
  } // (UserRole == null)
else  
  {
%>
      <BR><BR>
      Connection has been lost to the Data Warehouse Application.
      <BR>
      Please contact the Help Desk for assistance.
      <BR><BR>
      <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
 <%
 }

//Close Database Connection
if(con!=null)
    db.Close(con);
 %>
</CENTER>
</FORM> 
</body>
</html>





