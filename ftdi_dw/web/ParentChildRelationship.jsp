<html>
<head>
<title>Parent-Child Relationship Report</title>
<SCRIPT type="text/javascript" Language="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.PCR.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
</SCRIPT>
</head>
<body onload="ValidateLogin()">
<FORM name="PCR" method="post" action="ParentChildRelationReport.jsp" target="PCRR">
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Parent-Child Relationship Report</H2>
<BR><BR><BR>
   
<%
if ( !(UserRole == null) )
{
   if( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
   {
 	 	 	%>
		 <TABLE>
		 <TR>
		 <TD><font color="#FF0000">*</font>&nbsp;Member Code:&nbsp;&nbsp;</TD>
		 <TD><INPUT type="text" name="MemberCode" size="10" onBlur="ValidateMemberCode()"></TD>
		 <TD><font size="2"><i>(XX-XXXXAA)</i></font></TD>
		 </TR>
		 <TR>
		 <TD align="center">or</TD><TD></TD><TD align="center">or</TD>
		 </TR>
		 <TR>
		 <TD align="center">Internal Link</TD><TD></TD><TD align="center"><font size="2"><i>(XXXX)</i></font></TD>
		 </TR>
		 </TABLE>
		 <BR><BR><BR>
		 <INPUT TYPE="submit" VALUE="Run Report">
		 <INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		 <BR><BR>
		 <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field		
		 <%
   }
   else
   {
	%>
	 	 Currently your account does not have the correct role permissions to run this report.
		 <BR>
		 Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 <BR><BR>
		 <INPUT TYPE=Button VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">		
	<%
   }
}
%>
</center>
</form>
</body>
</html>

<SCRIPT type="text/javascript" Language="JavaScript">
function ValidateMemberCode()
{
         var tmp_mbr = document.PCR.MemberCode.value;
         
         if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
              ! tmp_mbr.match(/^[0-9]+$/) )
         {
             alert('Enter a valid member code in the format specified');
             document.PCR.MemberCode.select();
         }
}

</SCRIPT>
 


