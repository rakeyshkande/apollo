<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Mercury Direct Installations Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
            {
               var begindate = document.MDITest.bdate.value;
               var enddate   = document.MDITest.edate.value;
							 
               if ( document.MDITest.mdmember.value == "")
		   {	
			if ( document.MDITest.bdate.value == "" )
                  {
                      alert('Enter Beginning Date in format specified');
                       close();
                  }
                else if ( document.MDITest.vbdate.value == "false" )
                  {
                      alert('Enter valid Beginning Date in format specified');
                      close();
                  }
                else if ( document.MDITest.edate.value == "" )
                  {
                      alert('Enter Ending Date in format specified');
                      close();
                  }
                else if ( document.MDITest.vedate.value == "false" )
                  {
                      alert('Enter valid Ending Date in format specified');
                      close();
                  }
                else if ( document.MDITest.urole.value == "INT" || document.MDITest.urole.value == "MGT")
                {
                     if ( document.MDITest.mdter.value == "" && 
										 			document.MDITest.mdreg.value == "" )
                     {
                         alert('Select Territory or Region.');
                         close();
                     }
                     else // MGT & INT
							       {
                        document.MDI.submit();
                     }
								}
		    else // FBC & RVP
		{			 
		      document.MDI.submit();
                }
		}		
		    else	// not blank member	
               {
		     document.MDI.submit();
               }
	  }	  
</script>
</head>
<body onLoad="SubmitMe()">
<FORM name="MDITest" action="MercuryDirectInstallReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String BeginDate = request.getParameter("BeginDate");
String EndDate = request.getParameter("EndDate");
String Territory = request.getParameter("Territory");
String Region = request.getParameter("Region");
String MemberCode = ((request.getParameter("MemberCode")).trim()).toUpperCase(); 
String UserRole = (String) session.getAttribute("role");
%>
<input type="hidden" name="bdate" value="<%= BeginDate %>">
<input type="hidden" name="edate" value="<%= EndDate %>">
<input type="hidden" name="mdter" value="<%= Territory %>">
<input type="hidden" name="mdreg" value="<%= Region %>">
<input type="hidden" name="urole" value="<%= UserRole %>">
<input type="hidden" name="mdmember" value="<%= MemberCode %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retBeginVal = "false";
String retEndVal   = "false";

//Open Connection to database
Connection con = db.Connect();

// Test database Connection
if (con != null)
{
     // Validate dates
            retBeginVal = dateutil.validateDate(con,BeginDate);
            retEndVal   = dateutil.validateDate(con,EndDate);
            %>
            <input type="hidden" name="vbdate" value="<%= retBeginVal %>">
            <input type="hidden" name="vedate" value="<%= retEndVal %>">
            <%
     db.Close(con);
}
%>               

</FORM>

<FORM name="MDI" action="<bean:message key="report_url" />" method="post"> 
<%
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String ReportCall = "MercuryDirectInstalls.jsp";

if ( UserRole.equals("FBC") )
{
   ReportCall = ReportCall + "&p_territory=" + UserTerritory;
	 if ( MemberCode.equals("") ) 
	 {  
		 ReportCall = ReportCall + "&p_date1=" + BeginDate;
     ReportCall = ReportCall + "&p_date2=" + EndDate; 
	 } 
   else 
   {
      ReportCall = ReportCall + "&p_member=" + MemberCode;
			if ( !(BeginDate.equals("")) && (retBeginVal.equals("true"))  )
      {
       ReportCall = ReportCall + "&p_date1=" + BeginDate;
      } 
		  if ( !(EndDate.equals("")) &&	 (retEndVal.equals("true")) )
      {
        ReportCall = ReportCall + "&p_date2=" + EndDate; 
	    } 
	 }	
}
else if ( UserRole.equals("RVP") )
{
  if (MemberCode.equals("") ) 
  { 
	  ReportCall = ReportCall + "&p_date1=" + BeginDate;
    ReportCall = ReportCall + "&p_date2=" + EndDate;
  	if (!(Territory.equals("")) )
	  {
		   if (!(Territory.equals("All")) )
       {
         ReportCall = ReportCall + "&p_territory=" + Territory;
		   }	           
       else
		   {
		     ReportCall = ReportCall + "&p_division=" + UserRegion;
       }
	  }	 
  }    
  else
  {      
      ReportCall = ReportCall + "&p_member=" + MemberCode;
			if ( !(BeginDate.equals("")) && (retBeginVal.equals("true")) )
      {
        ReportCall = ReportCall + "&p_date1=" + BeginDate;
      } 
		  if ( !(EndDate.equals("")) &&	 (retEndVal.equals("true")) )
      {
        ReportCall = ReportCall + "&p_date2=" + EndDate; 
	    } 
	    if (!(Territory.equals("")) )
	    {
		   if (!(Territory.equals("All")) )
       {
         ReportCall = ReportCall + "&p_territory=" + Territory;
		   }	           
       else
		   {
		     ReportCall = ReportCall + "&p_division=" + UserRegion;
       }
			}  
	}	 	
}
else  
{
 if ( MemberCode.equals("") ) 
 { 
    ReportCall = ReportCall + "&p_date1=" + BeginDate;
    ReportCall = ReportCall + "&p_date2=" + EndDate;
    if ( !(Territory.equals("")))
    {
         ReportCall = ReportCall + "&p_territory=" + Territory;
	  }	           
    if ( !(Region.equals("")) )
	  {
		    ReportCall = ReportCall + "&p_division=" + Region;
    }
	}
	else
  {
	  ReportCall = ReportCall + "&p_member=" + MemberCode;
		if ( !(BeginDate.equals("")) && (retBeginVal.equals("true")) )
    {
       ReportCall = ReportCall + "&p_date1=" + BeginDate;
    } 
		if ( !(EndDate.equals("")) &&	 (retEndVal.equals("true")) )
    {
        ReportCall = ReportCall + "&p_date2=" + EndDate; 
	  } 
		if ( !(Territory.equals("")) && !(Territory.equals("All")) )
    {
         ReportCall = ReportCall + "&p_territory=" + Territory;
	  }	
		else
		{           
       if ( (Region.equals("")) || (Region.equals("All")) )
	     {
		    ReportCall = ReportCall + "&p_territory=All";
       }
		}
		if ( !(Region.equals("")) && !(Region.equals("All")) )
	  {
		    ReportCall = ReportCall + "&p_division=" + Region;
    }
		else
		{
		   if ( (Territory.equals("")) || (Territory.equals("All")) )
	     {
		       ReportCall = ReportCall + "&p_division=All";
       }
		}	 
	 }		 
}
if ( UserRole.equals("FBC") || UserRole.equals("RVP") )  
{
     if ( MemberCode.equals("") && 
         !(BeginDate.equals("")) && (retBeginVal.equals("true")) && 
         !(EndDate.equals("")) && (retEndVal.equals("true")) )
     {
     %>
             <BR><BR><BR><BR>
             <center>
             <H2>Processing Report.....</H2>
             <BR><BR><BR><BR><BR>
             <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
             <BR> 
             you have requested your report may take several minutes to run.
             </i></font>
             </center>
             <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
             <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
             <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
             <input type="hidden" name="destype" value="cache">
        <%
        }
        else if ( !( MemberCode.equals("")))
        {
        %>
                 <BR><BR><BR><BR>
                 <center>
                 <H2>Processing Report.....</H2>
                 <BR><BR><BR><BR><BR>
                 <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                 <BR> 
                 you have requested your report may take several minutes to run.
                 </i></font>
                 </center>
                 <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                 <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                 <%
                     Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                     Iterator it = queryPairs.entrySet().iterator();
                     while (it.hasNext()) {
                         Map.Entry pair = (Map.Entry) it.next();
                 %>
                 <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
                 <%
                     }
                 %>
                 <input type="hidden" name="desformat" value="pdf">
                 <input type="hidden" name="destype" value="cache">
        <%
        }
}
else if ( UserRole.equals("INT")  || UserRole.equals("MGT") )
{
     if ( (MemberCode.equals("")) &&
             !(BeginDate.equals("")) && !(EndDate.equals("")) && 
                  (retBeginVal.equals("true")) && (retEndVal.equals("true")) &&
                (!(Territory.equals("")) || !(Region.equals(""))))
     {
     %>
             <BR><BR><BR><BR>
             <center>
             <H2>Processing Report.....</H2>
             <BR><BR><BR><BR><BR>
             <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
             <BR> 
             you have requested your report may take several minutes to run.
             </i></font>
             </center>
             <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
             <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
             <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
             <input type="hidden" name="destype" value="cache">
        <%
        }
        else if ( !(MemberCode.equals("")) )
        {
        %>
             <BR><BR><BR><BR>
             <center>
             <H2>Processing Report.....</H2>
             <BR><BR><BR><BR><BR>
             <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
             <BR> 
             you have requested your report may take several minutes to run.
             </i></font>
             </center>
             <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
             <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
             <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
             <input type="hidden" name="destype" value="cache">
        <%
        }
        else
        {
        }
}
else
{
             //Do not display anything on page
}
%>
</FORM>
</body>
</html>
