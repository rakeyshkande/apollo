<%@ page import="java.sql.*" %>
<html>
<head>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
       if (document.MP.userid.value == "null")
       {
          window.location="LoginScreen.jsp";
       }
  }
</SCRIPT>
<title>Member Profile Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="MP" method="post" action="MemberProfileReport.jsp" target="MPR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
     <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Member Profile Report</H2>
<BR>
<i>The prior month Member Profile data is not available between the
<BR> 
1st and the 6th of the current month due to the Statement processing.</i>
<BR>
<BR>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="td" scope="session" class="FTDI.TerritoryDetail" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<%
//Open Connection to database
Connection con = db.Connect();

//Test database Connection
if (con != null)
{
     if ( !(UserRole == null) )
     {
        if( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
        {
                    %>
                    <TABLE>
                    <TR><td><b>Select:</b></td></TR>
                    <TR>
                    <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Member Code:</TD>
                    <TD><INPUT type="text" name="MemberCode" size="10" onBlur="ValidateMemberCode()"><font size="2"><i>(XX-XXXXAA)</i></font></TD>
                    </TR>
                    <TR>
                    <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Month:</TD>
                    <TD>
                <Select name="Month">
                <OPTION SELECTED value="">-- Select a Month --
                <OPTION value="01">January
                <OPTION value="02">February
                <OPTION value="03">March
                <OPTION value="04">April
                <OPTION value="05">May
                <OPTION value="06">June
                <OPTION value="07">July
                <OPTION value="08">August
                <OPTION value="09">September
                <OPTION value="10">October
                <OPTION value="11">November
                <OPTION value="12">December
                </SELECT>
                </TD>
                    </TR>
                    <TR>
                <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Year:</TD>
                    <TD>
                <Select name="Year">
                <OPTION SELECTED value="">-- Select a Year --
                    <%
                    ResultSet rs = td.MemberRevenueYears(con);
                    while (rs.next())
                    {
                            String RevenueYear = rs.getString("RevenueYear");
                            %>
                            <OPTION value="<%= RevenueYear %>"><%= RevenueYear %>
                            <%
                }
                    %>
                </SELECT>
                    </TD>           
                    </TR>
                    <TR>
                    <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Report Option:</TD>
                    <TD>
                <Select name="ReportType">
                <OPTION SELECTED value="">-- Select a Report -- 
                    <OPTION value="SML">Single Member  
                <OPTION value="CPCL">Complete Parent-Child Listing
                    <OPTION value="MRL">Complete Parent-Child Rollup
                </SELECT>
                </TD>
                    </TR>
                </TABLE>
                    <BR><BR>
                    <INPUT TYPE="submit" VALUE="Run Report">
                    <INPUT TYPE="button" VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
                    <BR><BR>
                    <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
            <%
            }
            else
            {
            %>
                  Currently your account does not have the correct role permissions to run this report.
                    <BR>
                    Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
                    <BR><BR>
                    <INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
            <%
            }
    }
}
else
{
%>
      <BR><BR>
          Connection has been lost to the Data Warehouse Application.
          <BR>
          Please contact the Help Desk for assistance.
          <BR><BR>
          <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
<%
}

//Close Database Connection
if(con!=null)
    db.Close(con);
%>
</CENTER> 
</FORM>
</body>
</html>

<script type="text/javascript" language="JavaScript">

function ValidateMemberCode()
{
         var tmp_mbr = document.MP.MemberCode.value;
         
         if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
              ! tmp_mbr.match(/^[0-9]+$/) )
         {
             alert('Enter a valid member code in the format specified');
             document.MP.MemberCode.select();
         }
}

</SCRIPT>


