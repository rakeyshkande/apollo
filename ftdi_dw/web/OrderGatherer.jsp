<html>
<head>
  <title>Order Gatherer</title>
  <SCRIPT Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.OG.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>  
</head>
<body onLoad="ValidateLogin()">
<FORM name="OG" method="post" action="OrderGathererReport.jsp" target="OGR">
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}
String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String Territory = (String) session.getAttribute("territory");
String Region = (String) session.getAttribute("region");
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<%
if ( !(Territory.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Territory:</b>&nbsp;<%= Territory %></div>
	 <%
}
if ( !(Region.equals("NA")) )
{
 	 %>
	 <div align="right"><b>Region:</b>&nbsp;<%= Region %></div>
	 <%
}	 
%>

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities" />

<%
// Get Current Month and Year
String Month = dateutil.CurrentMonthText();
String MonthDigit = dateutil.CurrentMonthDigit();
int DayDigit = dateutil.CurrentDayDigit();
String CurYear = dateutil.CurrentYear();
%>

<BR><BR><BR><BR>
<h2>Order Gatherer Reports</h2>

<%
if ( DayDigit > 6 )
{
 	 if ( !(UserRole == null) && ( UserRole.equals("INT") || UserRole.equals("MGT") || UserRole.equals("RVP")) )
	 {
   		%>
      <TABLE>
	    <TR>
		  <TD><font color="#FF0000">*</font>&nbsp;Report:</TD>
		  <TD>
		  <SELECT name="ReportName">
		  <OPTION SELECTED value="OrderGatherer">Daily Order Gatherer
		  <OPTION value="OrderGathererHistory">Order Gatherer History
		  </SELECT>
			</TD>
			</TR>
		  <TR>
		  <TD><font color="#FF0000">*</font>&nbsp;Country:</TD>
		  <TD>
		  <SELECT name="Country">
		  <OPTION SELECTED value="US">United States
		  <OPTION value="CA">Canada
		  </SELECT>
			</TD>
			</TR>
			<TR>
		  <TD><font color="#FF0000">*</font>&nbsp;Monthly Orders Out Total:</TD>
		  <TD>
		  <INPUT type="text" name="OrderOutTotal" maxlength="4" size="5" onBlur="CheckOrderTotal()">&nbsp;<i>(9999)</i>
			</TD>
			</TR>
			<TR>
		  <TD><font color="#FF0000">*</font>&nbsp;Ratio Of Outgoing Orders:</TD>
		  <TD>
		  <INPUT type="text" name="RatioOrders" maxlength="5" size="5" onBlur="CheckRatioOrders()">&nbsp;<i>(99.99)</i> to 1 Incoming
			</TD>
			</TR>
			<TR>
			<TD><font color="#FF0000">*</font>&nbsp;Group By:</TD>
			<TD>
			<SELECT name="GroupBy">
			<OPTION SELECTED value="">-- Select Group By --
			<OPTION value="CM">Collection Manager
			<OPTION value="DM">Division Manager
			</SELECT>
			</TD>
			</TR>
	 	 </TABLE>
		 <BR><BR>
		 <INPUT TYPE="submit" VALUE="Run Report">
		 <INPUT TYPE="button" VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
		 <BR><BR>
		 <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
		 <BR><BR><BR>
		 </CENTER> 
		 </FORM>
		 <%
	}
	else
	{
	%>
	 	 Currently your account does not have the correct role permissions to run this report.
		 <BR>
		 Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 <BR><BR>
		 <INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
		 </CENTER> 
		 </FORM>
	<%
	}
}
else
{
 	%>
	<BR>
	<i>The current month Wire Orders data is currently not available due to the Statement processing.
	<BR>
	This period lasts between the 1st and 6th of each month.</i>
  <BR><BR>
	<INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
	</CENTER>
	</FORM>
	<%
}
%>

<SCRIPT Language="JavaScript">
function CheckAvailability()
{
 				 alert('Function hit');
}


function CheckOrderTotal()
{
 	  if (document.OG.OrderOutTotal.value != "")
		{
 		 	 /* Test for value less than 1 */
 			 if (document.OG.OrderOutTotal.value < 1)
			 {
		 	 		alert('You must enter a value from 1 to 9999 for the Order Out Total.');
			 		document.OG.OrderOutTotal.value = "";
			 }
			 else
			 {
		 			 /* Test for non-decimal value */
					 var OrderOutTotalValue = document.OG.OrderOutTotal.value;
					 var DecimalFound = OrderOutTotalValue.indexOf(".");
					 if (DecimalFound >= 0)
					 {
		 	 	 	 		alert('You must enter a value from 1 to 9999 for the Order Out Total based on the input mask shown.');
		 			 		document.OG.OrderOutTotal.value = "";
					 }
			}
	 }		
}

function CheckRatioOrders()
{
 		if (document.OG.RatioOrders.value != "")
		{
				/* Test for value less than 00.01 */
				if (document.OG.RatioOrders.value <= 0) 
				{
		 		 	  alert('You must enter a value from 00.01 to 99.99 for the Ratio of Orders.');
						document.OG.RatioOrders.value = "";
				}
		
				/* Test for Decimal Value */
				var RatioOrdersValue = document.OG.RatioOrders.value;
				var NonDecimalFound = RatioOrdersValue.indexOf(".");
				if (NonDecimalFound < 0 || NonDecimalFound == 4)
				{
		 	 	 	 alert('You must enter a value from 00.01 to 99.99 for the Ratio of Orders based on the input mask shown.');
					 document.OG.RatioOrders.value = "";
				}
				else
				{
						 /* Check for 2 decimal places */
						 var DecimalValue = RatioOrdersValue.substring(NonDecimalFound+1);
						 if (DecimalValue.length > 2)
						 {
		 	 	 		 		 alert('You must enter a value from 00.01 to 99.99 for the Ratio of Orders based on the input mask shown.');
								 document.OG.RatioOrders.value = "";
						 }
				}
		}		
}
</SCRIPT>
</body>
</html>