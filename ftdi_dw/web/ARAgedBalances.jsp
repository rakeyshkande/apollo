<%@ page import="java.sql.*" %>
<html>
<head>
  <title>AR Aged Balances</title>
  <script type="text/javascript" language="JavaScript">
function ValidateLogin()
  {
   	   if (document.AAB.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }

function CheckRegionColl()
  {
 		 if (document.AAB.Region.selectedIndex != 0)
		 {
		  	 document.AAB.Region.selectedIndex = 0;
		 }

     if (document.AAB.Collection.selectedIndex != 0)
		 {
		  	 document.AAB.Collection.selectedIndex = 0;
		 }
  }

function CheckTerritoryColl()
  {
		 if (document.AAB.Territory.selectedIndex != 0)
		 {
		  	 document.AAB.Territory.selectedIndex = 0;
		 }
     if (document.AAB.Collection.selectedIndex != 0)
		 {
		  	 document.AAB.Collection.selectedIndex = 0;
		 }
  }

function CheckRegionTerritory()
  {
 		 if (document.AAB.Region.selectedIndex != 0)
		 {
		  	 document.AAB.Region.selectedIndex = 0;
		 }

     if (document.AAB.Territory.selectedIndex != 0)
		 {
		  	 document.AAB.Territory.selectedIndex = 0;
		 }
  }

</SCRIPT> 
 
</head>
<body onLoad="ValidateLogin()">
<FORM name="AAB" method="post" action="ARAgedBalanceReport.jsp" target="AAR">
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities" />
<jsp:useBean id="aab" scope="session" class="FTDI.ARAgedBalances" />

<%
//Open connection to database
Connection con = db.Connect();
%>
   
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>AR Aged Balances Report</H2>
<BR>

<%
//Check for Connection to Database
if (con != null)
{
 	 // Get Current Month and Year
	 String Month = dateutil.CurrentMonthText();
	 String MonthDigit = dateutil.CurrentMonthDigit();
	 String CurYear = dateutil.CurrentYear();
	 int BaseYear = 2003;
	 
	 if ( !(UserRole == null) )
	 {
   		if ( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
   		{
   		%>
      	 <TABLE>
	    	 <TR>
		  	 <TD><font color="#FF0000">*</font>&nbsp;AR Report:</TD>
		  	 <TD>
		  	 <SELECT name="ARReport">
		  	 <OPTION SELECTED value="">-- Select a Report --
			 <OPTION value="All_Members">All Members
		  	 <OPTION value="FTD_Member">FTD Member
		  	 <OPTION value="FTD_Non-Active_Member">FTD Non-Active Member
		  	 <OPTION value="VNS_Aging">VNS Aging
  	  	         <OPTION value="Other_Aging">Other Aging
		  	 </SELECT>
		  	 </TD>
		  	 </TR>
				 <TR>
		  	 <TD><font color="#FF0000">*</font>&nbsp;Report Type:</TD>
		  	 <TD>
		  	 <SELECT name="ReportType">
		  	 <OPTION SELECTED value="Debit">Debit
		  	 <OPTION value="Credit">Credit
			 <OPTION value="Both">Credit & Debit
		  	 </SELECT>
		  	 </TD>
			 </TR>
			 <TR>
		  	 <TD><font color="#FF0000">*</font>&nbsp;Currency Code:</TD>
		  	 <TD>
		  	 <SELECT name="CurrencyCode">
		  	 <OPTION SELECTED value="USD">US Dollars
		  	 <OPTION value="CAD">Canadian Dollars
		  	 </SELECT>
		  	 </TD>
				 </TR>
   	  	 <TR>
   	  	 <TD><font color="#FF0000">*</font>&nbsp;Month:</TD>
   	  	 <TD>
		  	 <Select name="month">
		  	 <OPTION SELECTED value="">
		  	 <OPTION value="01">January
		  	 <OPTION value="02">February
		  	 <OPTION value="03">March
		  	 <OPTION value="04">April
		  	 <OPTION value="05">May
		  	 <OPTION value="06">June
		  	 <OPTION value="07">July
		  	 <OPTION value="08">August
		  	 <OPTION value="09">September
		  	 <OPTION value="10">October
		  	 <OPTION value="11">November
		  	 <OPTION value="12">December
		  	 </SELECT>
		     &nbsp;&nbsp;Year:&nbsp;&nbsp;
			 <SELECT name="year">
			 <OPTION SELECTED value="">
		  	 <OPTION value="<%= CurYear %>"><%= CurYear %>
				 <%
				 int CurYr = Integer.parseInt(CurYear);
				 while(BaseYear != CurYr)
				 { 
				 	 %>
					 <OPTION value="<%= BaseYear %>"><%= BaseYear %>
					 <%
					 BaseYear++;
				 }
				 %>
				 </SELECT>
				 </TD>
				 </TR>
        
				 <%
				 if ( (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
 				
				 {
						//Retrieve All Territory Info from Database
	   				ResultSet rs = aab.TerritoryInfo(con);
						%>
            <tr></tr>
            <tr></tr>
            <tr>
            <td>SELECT ONE OF THE FOLLOWING:</td>
            </tr>
            <tr></tr>
            <tr></tr>
            <TR>
						<TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
						<TD>
						<SELECT name="Territory" onChange="CheckRegionColl()"> 
						<OPTION SELECTED value="">
						<OPTION value="All">All Territories
						<OPTION value="NA">Not Assigned
						<%
						while(rs.next())
						{
		 			 				String Territory = rs.getString("Territory");
									String FBC_Name = rs.getString("FBC_Name");
									%>
									<OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
									<%
						}
						%>
						</SELECT>
            </td>
						</TR>
						<%
						//Retrieve All Region Info from Database
						rs = null;
	   				rs = aab.RegionInfo(con);
						%>
						<TR>
						<TD><font color="#FF0000">*</font>&nbsp;Region:</TD>
						<TD>
						<SELECT name="Region" onChange="CheckTerritoryColl()"> 
						<OPTION SELECTED value="">
						<OPTION value="All">All Regions
						<OPTION value="NA">Not Assigned
						<%
						while(rs.next())
						{
		 			 			String Region = rs.getString("Region");
								String Mgr_Name = rs.getString("Manager_Name");
								%>
								<OPTION value="<%= Region %>"><%= Region + '-' + Mgr_Name %>
								<%
						}
						%>
						</SELECT>
						</TD>
						</TR>
						
					<%
						//Retrieve All Collection Info from Database
						rs = null;
	   				rs = aab.CollectionInfo(con);
						%>
						<TR>
						<TD><font color="#FF0000">*</font>&nbsp;Collection Manager:</TD>
						<TD>
						<SELECT name="Collection" onChange="CheckRegionTerritory()"> 
						<OPTION SELECTED value="">
						<OPTION value="All">All Collections
						<%
						while(rs.next())
						{
		 			 			String Collection = rs.getString("Collection");
								String Coll_Mgr_Name = rs.getString("Manager_name");
								%>
								<OPTION value="<%= Collection %>"><%= Collection + '-' + Coll_Mgr_Name %>
								<%
						}
						%>
						</SELECT>
						</TD>
						</TR>
				<%
				}
				 		%>
   	  			</TABLE>
	    			<BR><BR>
	    			<INPUT TYPE="submit" VALUE="Run Report" >
		  			<INPUT TYPE=Button VALUE="AR Main Menu" onClick="window.location='ARReportsMenu.jsp'">
      			<BR><BR>
	    			<font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
	    					<%
   			}
   			else
  			{
   			%>
   	  			<BR><BR>
		  			Currently your account does not have the correct role permissions to run this report.
		  			<BR>
		 				Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		  			<BR><BR>
		  			<INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
			<%
				}
		}
}
else
{
%>
   	  <BR><BR>
		  Connection has been lost to the Data Warehouse Application.
		  <BR>
		  Please contact the Help Desk for assistance.
		  <BR><BR>
		  <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
		
<%
}

if(con!=null)
	db.Close(con);
%>

</CENTER> 
</FORM>
</body>
</html>

