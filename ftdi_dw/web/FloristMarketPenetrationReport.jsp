<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Florist Market Penetration Report</title>
      <SCRIPT type="text/javascript" Language="JavaScript">
            function SubmitMe()
              {
                        if ( document.FMPRTest.bdate.value == "" )
                        {
                                alert('Enter Beginning Date');
                                close();
                        }
                        else if (document.FMPRTest.vbdate.value == "false")
                        {
                                alert('Enter valid Beginning Date in format specified');
                                close();
                        }
                        else if ( document.FMPRTest.edate.value == "" )
                        {
                                alert('Enter Ending Date');
                                close();
                        }
                        else if (document.FMPRTest.vedate.value == "false")
                        {
                                alert('Enter valid Ending Date in format specified');
                                close();
                        }
                        else
                        {
                                 document.FMPR.submit();
                        }
                }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="FMPRTest" action="FloristMarketPenetrationReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String BeginDate = request.getParameter("BeginDate");
String EndDate = request.getParameter("EndDate");
String InvoicedGroupsList = request.getParameter("GroupList");
%>

<input type="hidden" name="bdate" value="<%= BeginDate %>">
<input type="hidden" name="edate" value="<%= EndDate %>">
<input type="hidden" name="invoicedlist" value="<%= InvoicedGroupsList %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retEndVal = "false";
String retBeginVal = "false";

//Open Connection to database
Connection con = db.Connect();

// Test database Connection
if (con != null)
{

                //Validate Begin Date
                retBeginVal = dateutil.validateDate(con, BeginDate);
                retEndVal = dateutil.validateDate(con, EndDate);
                %>  
                <input type="hidden" name="vbdate" value="<%= retBeginVal %>">
                <input type="hidden" name="vedate" value="<%= retEndVal %>"> 
                <%
                db.Close(con);
}
%>
 
</FORM>
<FORM name="FMPR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "MarketPenetrationReport.rdf";

if ( !(BeginDate.equals("")) && !(EndDate.equals("")) && retEndVal.equals("true") && retBeginVal.equals("true") )
{
     ReportCall = ReportCall + "&p_date1=" + BeginDate;
     ReportCall = ReportCall + "&p_date2=" + EndDate;
     if ( !(InvoicedGroupsList.equals("")) )
     {
            String FinalList = InvoicedGroupsList.substring(0, (InvoicedGroupsList.length()-1) );
            ReportCall = ReportCall + "&p_user_groups=" + FinalList;
     }
     %>
     <BR><BR><BR><BR>
     <center>
     <H2>Processing Report.....</H2>
     <BR><BR><BR><BR><BR>
     <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
     <BR> 
     you have requested your report may take several minutes to run.
     </i></font>
     </center>
     <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
     <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="180">
     <%
         Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
         Iterator it = queryPairs.entrySet().iterator();
         while (it.hasNext()) {
             Map.Entry pair = (Map.Entry) it.next();
     %>
     <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
     <%
         }
     %>
     <input type="hidden" name="desformat" value="pdf">
     <input type="hidden" name="destype" value="cache">
     <%
}
else
{
        //Do not display anything on page
}
%>
</form>
</body>
</html>
