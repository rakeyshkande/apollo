<%@ page import="java.sql.*" %>
<html>
<head>
<SCRIPT type="text/javascript" LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
<SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.TR.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
</SCRIPT>
<title>Transmission Revenue Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="TR" method="post" action="TransmissionRevenueReport.jsp" target="TRR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Transmission Revenue Report</H2>
<BR>
 
<jsp:useBean id="td" scope="session" class="FTDI.TerritoryDetail" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities" />

<%
//Open connection to database
Connection con = db.Connect();

//Test Database Connection
if (con != null)
{
 	 if ( !(UserRole == null) )
	 {
   		if( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("MGT") )
   		{
   		 		%>
					<TABLE>
					<TR><td><b>Select:</b></td></TR>
					<TR>
					<TD></TD>
					<TD><font color="#FF0000">*</font>&nbsp;Beginning Month:</TD>
					<TD>
					<SELECT name="BeginMonth">
					<OPTION SELECTED value="">
					<OPTION value="01">January
					<OPTION value="02">February
					<OPTION value="03">March
					<OPTION value="04">April
					<OPTION value="05">May
					<OPTION value="06">June
					<OPTION value="07">July
					<OPTION value="08">August
					<OPTION value="09">September
					<OPTION value="10">October
					<OPTION value="11">November
					<OPTION value="12">December
					</SELECT>
					&nbsp;&nbsp;Year:&nbsp;&nbsp;
					<SELECT name="BeginYear">
					<OPTION SELECTED value="">
					<%
					ResultSet rs = td.MemberRevenueYears(con);
					while (rs.next())
					{
		 	 				String RevenueYear = rs.getString("RevenueYear");
							%>
							<OPTION value="<%= RevenueYear %>"><%= RevenueYear %>
							<%
		  		}
					%>
					</SELECT>
					</TD>
					</TR>
					<TR>
					<TD></TD>
					<TD><font color="#FF0000">*</font>&nbsp;Ending Month:</TD>
					<TD>
					<SELECT name="EndMonth">
					<OPTION SELECTED value="">
					<OPTION value="01">January
					<OPTION value="02">February
					<OPTION value="03">March
					<OPTION value="04">April
					<OPTION value="05">May
					<OPTION value="06">June
					<OPTION value="07">July
					<OPTION value="08">August
					<OPTION value="09">September
					<OPTION value="10">October
					<OPTION value="11">November
					<OPTION value="12">December
					</SELECT>
					&nbsp;&nbsp;Year:&nbsp;&nbsp;			
					<SELECT name="EndYear">
					<OPTION SELECTED value="">
					<%
					rs = null;
					rs = td.MemberRevenueYears(con);
					while (rs.next())
					{
		 	 				String RevenueYear = rs.getString("RevenueYear");
							%>
							<OPTION value="<%= RevenueYear %>"><%= RevenueYear %>
							<%
		  		}
					%>
					</SELECT>
					</TD>
					</TR>
					<TR>
					<TD></TD><TD><font color="#FF0000">*</font>Report Type:</TD>
					<TD>
					<SELECT name="ReportType">
					<OPTION SELECTED value="All">All Members
					<OPTION value="SDM">Special Deal Members
					<OPTION value="NSDM">Non-Special Deal Members
					</SELECT>
					</TD>
					</TR>
					<TR>
					<TD></TD><TD><font color="#FF0000">*</font>Sort By:</TD>
					<TD>
					<SELECT name="SortBy">
					<OPTION SELECTED value="SN">Shop Name (A->Z)
					<OPTION value="TCHL">Current Period Transmission Count (Highest->Lowest)
					<OPTION value="TCLH">Current Period Transmission Count (Lowest->Highest)
					</SELECT>
					</TD>
					</TR>
					<%

					if( (UserRole.trim()).equals("RVP"))
					{
			 		 		//Retrieve FBC Info from Database
	   					rs = null;
							rs = td.TerritoryInfo(con, UserID);
							%>
							<TR>
							<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
							<TD>
							<SELECT name="Territory"> 
							<OPTION SELECTED value="All">All Territories
							<%
							while(rs.next())
							{
		 			 		 			String Territory = rs.getString("Territory");
										String FBC_Name = rs.getString("FBC_Name");
										%>
										<OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
										<%
							}
							%>
							</SELECT>
							</TD>
							</TR>
					<%
					}

					if ( (UserRole.trim()).equals("MGT") )
					{
					 	 	 //Retrieve All Territory Info from Database
	   					 rs = null;
							 rs = td.AllTerritoryInfo(con);
							 %>
							 <TR>
							 <TD></TD><TD><font color="#FF0000">*</font>&nbsp;Territory:</TD>
							 <TD>
							 <SELECT name="Territory" onChange="CheckRegion()"> 
							 <OPTION SELECTED value="">
							 <OPTION value="All">All Territories
							 <%
							 while(rs.next())
							 {
		 			 		 			String Territory = rs.getString("Territory");
							 			String FBC_Name = rs.getString("FBC_Name");
										%>
										<OPTION value="<%= Territory %>"><%= Territory + '-' + FBC_Name %>
										<%
							 }
							 %>
							 </SELECT>
							 &nbsp;&nbsp;Or&nbsp;&nbsp;Region:
							 <%
							 //Retrieve All Region Info from Database
							 rs = null;
	   					 rs = td.AllRegionInfo(con);
							 %>
							 <SELECT name="Region" onChange="CheckTerritory()"> 
							 <OPTION SELECTED value="">
							 <OPTION value="All">All Regions
							 <%
							 while(rs.next())
							 {
		 			 		 			String Region = rs.getString("Region");
										String Mgr_Name = rs.getString("Manager_Name");
										%>
										<OPTION value="<%= Region %>"><%= Region + '-' + Mgr_Name %>
										<%
								}
								%>
								</SELECT>
								</TD>
								</TR>
					<%
					}
	 				%>
	 	 			</TABLE>
		 			<BR><BR>
		 			<INPUT TYPE="submit" VALUE="Run Report">
		 			<INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		 			<BR><BR>
		 			<font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
		  <%
	 	 }
   		else
	 	 {
  	          %>
	 	 			Currently your account does not have the correct role permissions to run this report.
		 			<BR>
		 			Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 			<BR><BR>
		 			<INPUT TYPE=Button VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		  <%
	 	 }
	}
}
else
{
%>
   	  <BR><BR>
		  Connection has been lost to the Data Warehouse Application.
		  <BR>
		  Please contact the Help Desk for assistance.
		  <BR><BR>
		  <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
<%
}

//Close Database Connection
if(con!=null)
    db.Close(con);
%>
</center>
</form>
</body>
</html>

<SCRIPT type="text/javascript" LANGUAGE="JavaScript">
function CheckRegion()
{
 		 if (document.TR.Region.selectedIndex != 0)
		 {
		  	 document.TR.Region.selectedIndex = 0;
		 }
}

function CheckTerritory()
{
		 if (document.TR.Territory.selectedIndex != 0)
		 {
		  	 document.TR.Territory.selectedIndex = 0;
		 }
}

</SCRIPT>
 


