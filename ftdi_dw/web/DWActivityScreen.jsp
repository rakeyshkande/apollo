<%@ page import="java.sql.*" %>
<html>
<head>
<SCRIPT type="text/javascript" language="JavaScript" SRC="calendar.js"></SCRIPT>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
       if (document.DW.userid.value == "null")
       {
          window.location="LoginScreen.jsp";
       }
  }
</SCRIPT>  
<title>Data Warehouse Activity Report</title>
</head>
<body onLoad="ValidateLogin()">
<FORM name="DW" method="post" action="DWActivityRptCall.jsp" target="DWA">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
 {
%>
   <jsp:forward page="LoginScreen.jsp"/>
<% 
 }
  String UserName = (String) session.getAttribute("username");
  String UserRole = (String) session.getAttribute("role");
	String UserRegion = (String) session.getAttribute("region");

//Validate User Logged In
%> 

<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">
<input type="hidden" name="reg" value="<%= UserRegion %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Data Warehouse Activity Report</H2>
<BR>

<jsp:useBean id="dw" scope="session" class="FTDI.DWActivity" />
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />

<%
//Open connection to database
Connection con = db.Connect();

//Test Database Connection
if (con != null)
 {
   if ( !(UserRole == null) )
    {
     if((UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("MGT"))
      {
       %>
        <table>

       <%
			  if((UserRole.trim()).equals("RVP"))  
			   { 
       %>
			    <TR>
         	</TR>
					<TR>
					<TD colspan=2><input type="checkbox" name="RunRSMRegion" value="True"
					    >&nbsp;Run for Region/Division:&nbsp;<%=UserRegion%>
							
					</TD>
				  </TR>
				 <%		 
         }
				else if((UserRole.trim()).equals("MGT"))
	       {
				 %>

           <%
              //Retrieve Region Information from Database
              ResultSet rs = dw.RegionInfo(con);
           %>
					    <TR>
							<td align=center colspan=2><b>Select one of the following groups:</b></td>
              </TR>
							<TR><TD>&nbsp;</TD></TR>
						  <TR>
             	<TD>Region/Division:</TD>
     					<TD>
              <SELECT name="Region" onChange="CheckUsers()"> 
              <OPTION SELECTED value="">
              <OPTION value="All">All Regions
            <%
              while(rs.next())
              {
               String Region = rs.getString("Region");
               String ManagerName = rs.getString("Manager_Name");
            %>
               <OPTION value="<%= Region %>"><%= Region + '-' + ManagerName %>
            <%
              }
            %>
               </SELECT>
               </TD>
               </TR>
							 <TR>
							 <TD>Other Users:&nbsp;&nbsp;</TD>
		           <TD>
							 <SELECT name="OtherUsers" onChange="CheckRegion()">
							 <OPTION SELECTED value="">
							 <OPTION value="MGT">Management Users
							 <OPTION value="INT">Internal Users
				<%
				 }
				%>            
       </SELECT>
       </TD>
       </TR>
       </TABLE>
       <BR><BR>
       <INPUT TYPE="submit" VALUE="Run Report">
       <INPUT TYPE="button" VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
       <BR><BR>
     <%
      }
      else
      {
     %>
        Currently your account does not have the correct role permissions to run this report.
        <BR>
        Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
        <BR><BR>
        <INPUT TYPE=Button VALUE="Main Menu" onClick="window.location='ReportMenu.jsp'">
 <%
      }
	  }		
  }
  else
  {	
 %>
    <BR><BR>
    Connection has been lost to the Data Warehouse Application.
    <BR>
    Please contact the Help Desk for assistance.
    <BR><BR>
    <INPUT TYPE=Button VALUE="Logout" onClick="window.location='Logout.jsp'">
   <%
 }
 
//Close Database Connection
if(con!=null)
    db.Close(con);
%>

        </CENTER> 
        </FORM>

<script type="text/javascript" language="JavaScript">

function CheckRegion()
{         
         if (document.DW.OtherUsers.value != " ")
         {
            document.DW.Region.selectedIndex = 0;
         }
}

function CheckUsers()
{         
				 if (document.DW.Region.selectedIndex != 0)  
				 {
            document.DW.OtherUsers.value = " ";
				 }
}


</SCRIPT>
 
</body>
</html>

