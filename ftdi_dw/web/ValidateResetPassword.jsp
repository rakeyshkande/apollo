<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.lang.*" %>
<html>
<head>
	  <title> Data Warehouse Password Maintenance</title>
</head>
<body>
<FORM name="ValidateResetPassword" action="">
<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="center">
<center>
<BR><BR><BR><BR>
<h2>Reset Password for Data Warehouse</h2>
<jsp:useBean id="vrp" scope="session" class="FTDI.ValidateResetPassword" />
   <% 
	 //Get UserID performing reset
	 String UserID = (String) session.getAttribute("userid");
	 
   //Get the UserID that was entered 
   String ResetUserId = (request.getParameter("UserID")).toUpperCase();
   session.setAttribute("resetuserid", ResetUserId);
   
   //Open connection to database  
   %>
   <jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
   <%
   Connection con = db.Connect();

   //Retrieve AS/400 account status and User Name
   ResultSet rs = vrp.DWAcctInfo(con, ResetUserId);
   
   if( rs.next() != false)
   {
   	   String AS400Status = (rs.getString("STATUS")).trim();
			 int DWLoginAttempts = rs.getInt("DW_LOGIN_ATTEMPTS");

   	   if ( AS400Status.equalsIgnoreCase("DISABLED") )
   	   {
   	   %>
	   	  	Caller's AS/400 account is disabled.
		  		<BR>  
		  		Caller must contact AS/400 Administrator to enable their AS/400 account.
		  		<BR><BR>
					<%
					if (UserID.equalsIgnoreCase("admin"))
					{
					%>
					<input type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">                                    
					<%
					}
					else
					{
					%>
					<input type="button" name="Logout" value="Logout" onClick="window.location='LoginScreen.jsp'">                                        		  		
					<%
					}
		  }
			else if ( DWLoginAttempts >= 3)
			{
			%>
	   	  	Caller's FTDI Data Warehouse account is disabled.
		  		<BR>  
		  		The FTDI Data Warehouse account must be enabled prior to resetting the password.
		  		<BR><BR>
					
				<input type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">		  		
			<%
			}
   		else
   		{
   	  %>
	   	 		Confirm Caller's name is: <%= rs.getString("USER_NAME") %>
		 			<BR><BR>
					<TABLE>
					<TR>
					<TD>  
		 			<input type="button" name="ConfirmYes" value="Yes" onClick="window.location='ConfirmYesResetPassword.jsp'">
					</TD>
					<TD></TD>
					<TD>
		 			<input type="button" name="ConfirmNo" value=" No " onClick="window.location='ConfirmNoResetPassword.jsp'">
					</TD>
					</TR>
					</TABLE>		 			
	   	<%
   		}
	}
	else
	{
	 	%>
		Caller's AS/400 account is not found on the database.
		<BR>  
		Please have the Caller contact their manager to request an AS/400 account.
		<BR><BR>
	
	  <input type="button" name="MainMenu" value="Main Menu" onClick="window.location='AdminMainMenu.jsp'">	
   <%
  }

   // Close Database Connetion
   if(con!=null)
       db.Close(con);
   %>
</center>
</FORM>
</body>
</html>

