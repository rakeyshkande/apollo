<html>
<head>
  <title>Florist Market Penetration</title>
  <SCRIPT type="text/javascript" LANGUAGE="JavaScript" SRC="calendar.js"></SCRIPT>
  <SCRIPT type="text/javascript" Language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.FMP.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT> 
</head>
<body onLoad="ValidateLogin()">
<FORM name="FMP" method="post" action="FloristMarketPenetrationReport.jsp" target="FMPR">
<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Florist Market Penetration Report</H2>
<BR>
<input type="hidden" name="GroupList" value="" size="100">

<%
if( !(UserRole == null) )
{
   if ( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
   {
   %>
      <TABLE>
			<TR>
			<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Beginning Date:</TD>
			<TD><input type="text" name="BeginDate" size="10" maxlength="10" onBlur="ValidateBeginDate()">
			<a href="javascript:doNothing()" onClick="setDateField(document.FMP.BeginDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                        <img src="calendar.gif" alt="" border=0></a>
			<font size="2"><i>(MM/DD/YYYY)</i></font></TD>
			</TR>
			<TR>
			<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Ending Date:</TD>
			<TD><input type="text" name="EndDate" size="10" maxlength="10" onBlur="ValidateEndDate()">
			<a href="javascript:doNothing()" onClick="setDateField(document.FMP.EndDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                        <img src="calendar.gif" alt="" border=0></a>
			<font size="2"><i>(MM/DD/YYYY)</i></font></TD>
			</TR>
			<TR>
			<TD></TD><TD><font color="#0000FF">*</font>&nbsp;Total Invoiced Count Included Groups:</TD>
                        <TD>
			<SELECT MULTIPLE size="4" name="InvoicedGroups" onChange="CreateList()">
		        <OPTION value="CTN">Containers			
		        <OPTION value="DRP">Drop Ship
			<OPTION value="FSG">Floral Selections Guide
			<OPTION value="MFF">Member Fresh Flowers
			<OPTION value="MKT">Member Marketing
			<OPTION value="NFF">NM Fresh Flowers
			<OPTION value="REN">MP Renaissance
			<OPTION value="WAR">Warehouse Containers
		        </SELECT>
			</TD>
			</TR>
			</TABLE>
	    <BR><BR>
	    <INPUT TYPE="submit" VALUE="Run Report">
	    <INPUT TYPE="button" VALUE="MarketPlace Menu" onClick="window.location='MarketplaceReportsMenu.jsp'">
            <BR><BR>
	    <font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
		<font color="#0000FF">*</font>&nbsp;&nbsp;Denotes Optional field	   
   <%
   }
   else
   {
   %>
   	 <BR><BR>
		 Currently your account does not have the correct role permissions to run this report.
		 <BR>
		 Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 <BR><BR>
		 <INPUT TYPE=Button VALUE="MarketPlace Menu" onClick="window.location='MarketplaceReportsMenu.jsp'">
    <%
    }
}
%>
</center>
</FORM>
</body>
</html>

<SCRIPT type="text/javascript" LANGUAGE="JavaScript">
function ValidateBeginDate()
{
 				 var dt = document.FMP.BeginDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
			 

				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Beginning Date in format specified');
							 document.FMP.BeginDate.focus();
							 document.FMP.BeginDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Beginning Date in format specified');
									 document.FMP.BeginDate.focus();
									 document.FMP.BeginDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
						   	alert('Enter valid Beginning Date in format specified');
							document.FMP.BeginDate.focus();		
             	 			                document.FMP.BeginDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.FMP.BeginDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

function ValidateEndDate()
{
 				 var dt = document.FMP.EndDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
		 		
				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Ending Date in format specified');
							 document.FMP.EndDate.focus();
							 document.FMP.EndDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Ending Date in format specified');
									 document.FMP.EndDate.focus();
									 document.FMP.EndDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
						   		alert('Enter valid Ending Date in format specified');
								document.FMP.EndDate.focus();
             	 			    document.FMP.EndDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.FMP.EndDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

function CreateList()
{
 		var counter = 0;;
		var ListLength = 0;
		
  	document.FMP.GroupList.value = "";
					
		for (var counter=0; counter < document.FMP.InvoicedGroups.options.length; counter++)
		{
			 if (document.FMP.InvoicedGroups.options[counter].selected)
			 {
			 		if (document.FMP.InvoicedGroups.options[counter].value != "")
		  		{
			 	 	 	 document.FMP.GroupList.value = document.FMP.GroupList.value + document.FMP.InvoicedGroups.options[counter].value + ",";
						 ListLength++;
			  	}
			 }
		}
		

		if (ListLength > 40)
		{
		 	 alert('Only a maximum of 40 Source Codes may be selected at one time');
			 document.FMP.GroupList.value = "";
			 document.FMP.InvoicedGroups.selectedIndex = 0;
		} 
}
</SCRIPT>


