<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ taglib uri="/tags/struts-html" prefix="html" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.security.*" %>
<%@ page import="com.ftdi.common.FtdiUser" %>
<%@ page import="FTDI.ValidateLogin" %>
<html>
<head>
<title> Data Warehouse Login</title>
<SCRIPT type="text/javascript" Language="JavaScript">
	function SubmitMe()
	{
		if (document.ValidateLogin.connection.value=="NOCON")
		{
			alert('Unable to connect to Data Warehouse Application.\n       Please contact the System Administrator.');
			window.location="LoginScreen.jsp";
		}
		else if (document.ValidateLogin.UserFound.value=="FALSE")
		{
			alert('Your login information was not found on the database.\nPlease contact AS/400 Administrator to create an account.');
			window.location="LoginScreen.jsp";
		}
		else if (document.ValidateLogin.LoginAttempts.value=="INVALID")
		{
			alert('Your account has been disabled because of too many invalid login attempts.\n\tPlease contact the Help Desk at 866-971-4357.');
			window.location="LoginScreen.jsp";
		}
		else if (document.ValidateLogin.password.value=="INVALID")
		{
			alert('Your login information is invalid.  Please try again');
			window.location="LoginScreen.jsp";
		}
		else if (document.ValidateLogin.Admin.value=="TRUE")
		{
			alert('You are currently logging in as Administrator.');
			window.location="AdminMainMenu.jsp";
		}
		else if (document.ValidateLogin.status.value=="DISABLED")
		{
			alert('Your AS/400 account is disabled.  Please contact the AS/400 Administrator.');
			window.location="LoginScreen.jsp";
		}
		else if (document.ValidateLogin.DWPassDate.value=="EXPIRED")
		{
			alert('Your Data Warehouse password has expired.  Please follow directions to change your password.');
			window.location="ChangePassword.jsp";
		}
		else if (document.ValidateLogin.helpdesk.value=="TRUE")
		{
			if (document.ValidateLogin.DWNumOfDays.value <= 10)
			{
				alert('Your Data Warehouse Password will expire in ' + document.ValidateLogin.DWNumOfDays.value + ' day(s).');
			}
			window.location="AdminMainMenu.jsp";
		}
		else 
		{
			//Popups for days til password expires for DW
			if (document.ValidateLogin.DWNumOfDays.value <= 10)
			{
				alert('Your Data Warehouse Password will expire in ' + document.ValidateLogin.DWNumOfDays.value + ' day(s).');
			}
			if (document.ValidateLogin.DWRole.value == "INVALID")
			{
				alert('Currently, your AS/400 role does not have any Permissions to run reports on the Data Warehouse.\n\t\tPlease contact the AS400 Administrator for more details.');
			}
      
			if (document.ValidateLogin.DWRole.value == "SUPER")
			{
        document.ValidateLogin.action="UATMainMenu.jsp";
        document.ValidateLogin.submit();
        
			}
  			document.ValidateLogin.submit();		  
		}
	}

</script>
</head>
<body onLoad="SubmitMe()">
<FORM name="ValidateLogin" action="ReportMenu.jsp">
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin" />
<jsp:useBean id="pwd" scope="session" class="FTDI.PasswordUtilities" />

<jsp:useBean id="ftdiuser" scope="session" class="com.ftdi.common.FtdiUser" />

<% 
//<jsp:useBean id="as400" scope="session" class="FTDI.ValidateLogin" />
//Get the UserID and Password that was entered 
String UserID = (request.getParameter("UserID")).toUpperCase();
String password = (request.getParameter("Pass")).toUpperCase();
boolean loginSuccess = true;

//Encrypt Password for comparison against one stored in database
byte[] EncryptedPassword = pwd.EncryptPassword(password);

session.setAttribute("UATError", "");
ValidateLogin as400 = ValidateLogin.getInstance();
//Retrieve information for user from database
ftdiuser = as400.AS400UserInfo(UserID);

//keep this for other pages    
//Open connection to database  
Connection con = db.Connect();

if (con != null)
{
 %>
        <input type="hidden" name="connection" value="CON">
      <%

	//Check to see if the user was found on the database
	if (ftdiuser != null)
	{
      %>
      <input type="hidden" name="UserFound" value="TRUE">
      <%
      //Save information to session Object
      String AS400Role = ftdiuser.getGroupProfile();
      
      session.setAttribute("userid", UserID);
      session.setAttribute("username", ftdiuser.getUserName());
      session.setAttribute("ftdiuser", ftdiuser);

      //Validate Password
      if(MessageDigest.isEqual(EncryptedPassword, ftdiuser.getUserStoredPassword()))
      {
        //Password is valid
        %><input type="hidden" name="password" value="VALID"> <%
			//Check to see if account is disabled
        if ( (ftdiuser.getDWLoginAttempts()) >= 3)
        {
          loginSuccess = false;
          %>
          <input type="hidden" name="LoginAttempts" value="INVALID">
          <%
        }else{
          %>
          <input type="hidden" name="LoginAttempts" value="VALID">
          <%
        }
   		}else{
        loginSuccess = false;
        if( (ftdiuser.getDWLoginAttempts()) < 3 )
        {
          //Update Login Attempts
          as400.UpdateLoginAttempts(UserID);
          %>
          <input type="hidden" name="password" value="INVALID">
          <input type="hidden" name="LoginAttempts" value="VALID">
          <%
        }else{
          %>
           <input type="hidden" name="password" value="INVALID">
           <input type="hidden" name="LoginAttempts" value="INVALID">
           <%
        }
   		}

   		//If login was for Helpdesk, allow in without any further checks
   		if (UserID.equals("DWHELPDESK") )
   		{
   		 	 %>
                         <input type="hidden" name="helpdesk" value="TRUE">
                         <%
   		}else{
   		 	%>
                        <input type="hidden" name="helpdesk" value="FALSE">
                        <%
   		}

      //If Login is Administrator, allow in without any further checks
      if ( UserID.equals("ADMIN") )
      {
      %>
      <input type="hidden" name="Admin" value="TRUE">
      <%
      }else{
        %>
        <input type="hidden" name="Admin" value="FALSE">
        <%
      }

   		//Validate that the account is enabled on AS400
   		if ( (ftdiuser.getStatus().trim().equalsIgnoreCase("ENABLED")) )
   		{
     	 	//Status is Enabled
   		 	%>
                        <input type="hidden" name="status" value="ENABLED">
                        <%
   		}else{
        loginSuccess = false;
        //Status is Disabled
        %>
        <input type="hidden" name="status" value="DISABLED">
        <%
   		}

   		//Check To see if DW password is expired
   		java.util.Date curDate = new java.util.Date();
   		if ( ftdiuser.getDWPasswordChangeDate().compareTo(curDate) > 0)
      {
        //DW Password Expire Date has not passed
        %>
        <input type="hidden" name="DWPassDate" value="NONEXPIRED">
        <input type="hidden" name="DWNumOfDays" value="<%= ftdiuser.getDWDays() %>">
        <%
      }	else{
        //DW Password Expire Date is today or has passed
        %>
        <input type="hidden" name="DWPassDate" value="EXPIRED">
        <input type="hidden" name="DWNumOfDays" value="<%= ftdiuser.getDWDays() %>">
        <%
      }
			if (loginSuccess)
			{
        //Insert into Logon Audit table
        as400.InsertUserAudit(UserID);
			}

      session.setAttribute("territory", "NA");
      session.setAttribute("region","NA");	 	
          
      if(ftdiuser.isSuperUser())
      {//super user should be redirect to different page
           session.setAttribute("role",ftdiuser.getDWRole());
           %>
           <input type="hidden" name="DWRole" value="SUPER">
           <%
      }else{
          //Get Role Information for User
          if(ftdiuser.getDWRole() != null || !ftdiuser.getDWRole().trim().equals(""))
          {
              session.setAttribute("role",ftdiuser.getDWRole());
              %>
              <input type="hidden" name="DWRole" value="VALID">
              <%
          }else{
              session.setAttribute("role","");
              %>
              <input type="hidden" name="DWRole" value="INVALID">
              <%
          }
      }
    
          //Get Territory or Region Number if applicable
          if (!ftdiuser.getUserTerritory().trim().equals(""))
              session.setAttribute("territory", ftdiuser.getUserTerritory());
              
          if (!ftdiuser.getUserRegion().trim().equals(""))
              session.setAttribute("region", ftdiuser.getUserRegion());
	 }
	 else
	 {
 	 		%>
                        <input type="hidden" name="UserFound" value="FALSE">
                        <%
	 }
	 // Close Database Connetion
         if(con!=null)
             db.Close(con);
}else{
 		%>
                <input type="hidden" name="connection" value="NOCON">
                <%
}
%>

</FORM>
</body>
</html>
