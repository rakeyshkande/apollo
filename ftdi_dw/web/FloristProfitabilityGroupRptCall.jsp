<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.util.*" %>
<html>
<head><title>Florist Profitability Group Rollup Report</title>
<script type="text/javascript" language="JavaScript">
    function SubmitMe()
      {
                if ( (document.GPRtest.fromrank.value == "" ) && (document.GPRtest.torank.value == "" ) 
                   && (document.GPRtest.terr.value == "" ) && (document.GPRtest.reg.value == "" ))
                {
                        alert('You must select a Report Group');
                        close();
                }
                else if ( ( document.GPRtest.fromrank.value == "" ) && ( document.GPRtest.torank.value != "" ) 
                       || ( document.GPRtest.fromrank.value != "" ) && ( document.GPRtest.torank.value == "" ) )		
                {
                        alert('You must select both a Start and End Sending Rank');
                        close();
                }
                else if ( document.GPRtest.month.value == "" )
                {
                         alert('You must select a Month');
                         close();
                }
                else if ( document.GPRtest.year.value == "" )
                {
                         alert('You must select a Year');
                         close();
                }
                else if ( document.GPRtest.reporttype.value == "" )
                {
                        alert('You must select a Report Option');
                        close();
                }
                else if ((document.GPRtest.terr.value == "" ) && (document.GPRtest.reporttype.value == "TERR") )
                {
                        alert('You must select a Territory Group with Territory Rollup');
                        close();  
                } 
                else if ((document.GPRtest.role.value == "MGT" || document.GPRtest.role.value == "INT") 
                            && (document.GPRtest.reg.value == "" ) 
                            && (document.GPRtest.reporttype.value == "REG"))
                { 
                        alert('You must select a Region Group with Region Rollup');
                        close();
                }
                else if ( (document.GPRtest.role.value == "MGT" || document.GPRtest.role.value == "INT")  &&
                          ((document.GPRtest.terr.value != "" ) || (document.GPRtest.reg.value != "" ))
                            && (document.GPRtest.reporttype.value == "SR") )
                {   
                        alert('You must enter a Sending Rank Group with Sending Rank Rollup');
                        close();
                }else if( document.GPRtest.role.value == "RVP" &&
                            document.GPRtest.terr.value != "" &&
                            document.GPRtest.reporttype.value == "SR")
                {
                      alert('You must enter a Sending Rank Group with Sending Rank Rollup');
                      close();
                }
                else
                {
                      document.GPR.submit();
                }
        }
        </SCRIPT>
</head>
<body onLoad="SubmitMe()">
<FORM name="GPRtest" action="FloristProfitabilityGroupRptCall.jsp">
<%
String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}
String Month =  request.getParameter("Month");
String Year = request.getParameter("Year");
String FromRank =  request.getParameter("fromrank").trim();
String ToRank = request.getParameter("torank").trim();
String ReportType = request.getParameter("ReportType");
String Territory = request.getParameter("Territory");
String Region = request.getParameter("Region");

String UserRole = (String) session.getAttribute("role");
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
%>
<input type="hidden" name="month" value="<%= Month %>">
<input type="hidden" name="year" value="<%= Year %>">
<input type="hidden" name="fromrank" value="<%= FromRank %>">
<input type="hidden" name="torank" value="<%= ToRank %>">
<input type="hidden" name="reporttype" value="<%= ReportType %>">
<input type="hidden" name="role" value="<%= UserRole %>">
<input type="hidden" name="terr" value="<%= Territory %>">
<input type="hidden" name="reg" value="<%= Region %>">
</FORM>

<FORM name="GPR" action="<bean:message key="report_url" />" method="post">
<%
String ReportCall = "FloristProfitGroupRollupRpt.rdf";
ReportCall = ReportCall + "&p_month=" + Month;
ReportCall = ReportCall + "&p_year=" + Year;
ReportCall = ReportCall + "&p_report_type=" + ReportType; 

if ( UserRole.equals("RVP"))
 { 
   if  ( ReportType.equals("TERR")) 
     {
       if (( !(Territory.equals(""))) && ( !(Territory.equals("All"))) )
          {
            ReportCall = ReportCall + "&p_division=blank";
            ReportCall = ReportCall + "&p_territory=" + Territory;
          }
       else
          {
            ReportCall = ReportCall + "&p_division=" + UserRegion;
            ReportCall = ReportCall + "&p_territory=blank";
          }
     }
   else
     {
        ReportCall = ReportCall + "&p_division=" + UserRegion;
        ReportCall = ReportCall + "&p_territory=blank";
        ReportCall = ReportCall + "&p_fromrank=" + FromRank;
	ReportCall = ReportCall + "&p_torank=" + ToRank;
     } 
  }
else if ( UserRole.equals("INT") || UserRole.equals("MGT") )
     {
       if  ( ReportType.equals("TERR")) 
         {
            ReportCall = ReportCall + "&p_division=blank";
            if (( !(Territory.equals(""))) && ( !(Territory.equals("All"))) )
             {
               ReportCall = ReportCall + "&p_territory=" + Territory;
             }
            else
              {
                ReportCall = ReportCall + "&p_territory=All";
              }
         }    
       else if  ( ReportType.equals("REG"))
             {
               if ( ( !(Region.equals(""))) && ( !(Region.equals("All"))) )
                 {
                   ReportCall = ReportCall + "&p_division=" + Region;
                 }
               else
                 {
                  ReportCall = ReportCall + "&p_division=All";
                 }
               ReportCall = ReportCall + "&p_territory=blank";
             }  
            else
               {
                ReportCall = ReportCall + "&p_division=blank";
                ReportCall = ReportCall + "&p_territory=blank";
                ReportCall = ReportCall + "&p_fromrank=" + FromRank;
		 	          ReportCall = ReportCall + "&p_torank=" + ToRank;
               } 
     }
   else
     {
   //Do not display anything on page
     }
%>
          <BR><BR><BR><BR>
          <center>
          <H2>Processing Report.....</H2>
          <BR><BR><BR><BR><BR>
          <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
          <BR> 
           you have requested your report may take several minutes to run.
          </i></font>
          </center>
          <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
          <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="130">
          <%
             Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
             Iterator it = queryPairs.entrySet().iterator();
             while (it.hasNext()) {
                 Map.Entry pair = (Map.Entry) it.next();
         %>
         <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
         <%
             }
         %>
         <input type="hidden" name="desformat" value="pdf">
          <input type="hidden" name="destype" value="cache"> 

</Form>
</body>
</html>
