<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Contract Sales Report</title>
<script type="text/javascript" language="JavaScript">
            function SubmitMe()
              {
                      var begindate = document.CSRTest.bdate.value;
                      var enddate   = document.CSRTest.edate.value;
                                                                    
                        if ( document.CSRTest.csmember.value == "" )
                        {
                         if ( document.CSRTest.bdate.value == "" )
                             {
                                    alert('Enter Beginning Date in format specified');
                                    close();
                             }
                                                         else if ( document.CSRTest.vbdate.value == "false" )
                             {
                                    alert('Enter valid Beginning Date in format specified');
                                    close();
                             }
                             else if ( document.CSRTest.edate.value == "" )
                             {
                                    alert('Enter Ending Date in format specified');
                                    close();
                             }
                                                         else if ( document.CSRTest.vedate.value == "false" )
                             {
                                    alert('Enter valid Ending Date in format specified');
                                    close();
                             }
                             else if ( document.CSRTest.csrt.value == "")
                             {
                                    alert('Select Report');
                                    close();
                             }
                             else if ( document.CSRTest.urole.value == "INT" || document.CSRTest.urole.value == "MGT")
                             {
                                 if ( document.CSRTest.cster.value == "" && document.CSRTest.csreg.value == "" )
                                 {
                                         alert('Select Territory or Region.');
                                         close();
                                 }
                                 else
                                 {
                                        document.CSR.submit();
                                 }
                            }
                            else
                            {
                                 document.CSR.submit();
                            }
                    }
                    else
                    {
                            if ( document.CSRTest.csrt.value == "")
                            {
                                    alert('Select Report');
                                    close();
                            }
                            else
                            {
                                    document.CSR.submit();
                            }
                    }
                }
        </script>
</head>
<body onLoad="SubmitMe()">
<FORM name="CSRTest" action="ContractStatusReport.jsp">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
             <jsp:forward page="SessionTimeout.jsp"/>
<% 
}

String CSBDate = request.getParameter("BeginDate");
String CSEDate = request.getParameter("EndDate");
String CSTerritory = request.getParameter("Territory");
String CSRegion = request.getParameter("Region");
String CSReportType = request.getParameter("ReportType");
String CSMemberCode = ((request.getParameter("MemberCode")).trim()).toUpperCase(); 
String UserRole = (String) session.getAttribute("role");
%>
<input type="hidden" name="bdate" value="<%= CSBDate %>">
<input type="hidden" name="edate" value="<%= CSEDate %>">
<input type="hidden" name="cster" value="<%= CSTerritory %>">
<input type="hidden" name="csreg" value="<%= CSRegion %>">
<input type="hidden" name="urole" value="<%= UserRole %>">
<input type="hidden" name="csrt" value="<%= CSReportType %>">
<input type="hidden" name="csmember" value="<%= CSMemberCode %>">

<jsp:useBean id="dateutil" scope="session" class="FTDI.DateUtilities"/>
<jsp:useBean id="db" scope="session" class="FTDI.DWLogin"/>
<%
String retBeginVal = "false";
String retEndVal   = "false";

//Open Connection to database
Connection con = db.Connect();

// Test database Connection
if (con != null)
{
     // Validate dates
            retBeginVal = dateutil.validateDate(con,CSBDate);
            retEndVal   = dateutil.validateDate(con,CSEDate);
            %>
            <input type="hidden" name="vbdate" value="<%= retBeginVal %>">
            <input type="hidden" name="vedate" value="<%= retEndVal %>">
            <%

    db.Close(con);
}
%>               

</FORM>

<FORM name="CSR" action="<bean:message key="report_url" />" method="post">
<%
String UserTerritory = (String) session.getAttribute("territory");
String UserRegion = (String) session.getAttribute("region");
String ReportCall = null;

if ( UserRole.equals("FBC") )
{
     ReportCall = CSReportType + "ContractStatus.jsp";
     ReportCall = ReportCall + "&p_territory=" + UserTerritory;
     if( CSMemberCode.equals("") )
     {
             ReportCall = ReportCall + "&p_date1=" + CSBDate;
             ReportCall = ReportCall + "&p_date2=" + CSEDate;
     }
     else
     {
            ReportCall = ReportCall + "&p_member=" + CSMemberCode;
            if( !(CSBDate.equals("")) && (retBeginVal.equals("true")) )
            {
                    ReportCall = ReportCall + "&p_date1=" + CSBDate;
            }
            if( !(CSEDate.equals("")) && (retEndVal.equals("true")) )
            {
                    ReportCall = ReportCall + "&p_date2=" + CSEDate;
            }   
     }
}
else if ( UserRole.equals("RVP") )
{
     ReportCall = CSReportType + "ContractStatus.jsp";
     
     if( CSMemberCode.equals("") )
     {
             ReportCall = ReportCall + "&p_date1=" + CSBDate;
             ReportCall = ReportCall + "&p_date2=" + CSEDate;
             if ( !(CSTerritory.equals("")) )
             {
                    if ( !(CSTerritory.equals("All")) )
                    {
                         ReportCall = ReportCall + "&p_territory=" + CSTerritory;
                    }
                    else
                    {
                            ReportCall = ReportCall + "&p_division=" + UserRegion;
                    }
            }
    }
    else
    {
            ReportCall = ReportCall + "&p_member=" + CSMemberCode;
            
            if( !(CSBDate.equals("")) && (retBeginVal.equals("true")) )
            {
                    ReportCall = ReportCall + "&p_date1=" + CSBDate;
            }
            if( !(CSEDate.equals("")) && (retEndVal.equals("true")) )
            {
                    ReportCall = ReportCall + "&p_date2=" + CSEDate;
            }
            
            if ( !(CSTerritory.equals("")) )
            {
                    if ( !(CSTerritory.equals("All")) )
                    {
                         ReportCall = ReportCall + "&p_territory=" + CSTerritory;
                    }
                    else
                    {
                            ReportCall = ReportCall + "&p_division=" + UserRegion;
                    }
            }
    }
}
else
{
        ReportCall = CSReportType + "ContractStatus.jsp";
        
        if ( CSMemberCode.equals("") )
        {
             ReportCall = ReportCall + "&p_date1=" + CSBDate;
             ReportCall = ReportCall + "&p_date2=" + CSEDate;
             if ( !(CSTerritory.equals("")))
             {
                         ReportCall = ReportCall + "&p_territory=" + CSTerritory;
             }
             
             if ( !(CSRegion.equals("")) )
             {
                         ReportCall = ReportCall + "&p_division=" + CSRegion;
             }
        }
        else
        {
            ReportCall = ReportCall + "&p_member=" + CSMemberCode;
            
            if( !(CSBDate.equals("")) && (retBeginVal.equals("true")) )
            {
                    ReportCall = ReportCall + "&p_date1=" + CSBDate;
            }
            if( !(CSEDate.equals(""))  && (retEndVal.equals("true")) )
            {
                    ReportCall = ReportCall + "&p_date2=" + CSEDate;
            }
            
            if ( !(CSTerritory.equals("")) && !(CSTerritory.equals("All")) )
            {
                         ReportCall = ReportCall + "&p_territory=" + CSTerritory;
            }
            else
            {
                    if ( (CSRegion.equals("")) || (CSRegion.equals("All")) )
                    {
                         ReportCall = ReportCall + "&p_territory=All";
                    }
            }
             
            if ( !(CSRegion.equals("")) && !(CSRegion.equals("All")) )
            {
                         ReportCall = ReportCall + "&p_division=" + CSRegion;
            }
            else
            {
                    if ( (CSTerritory.equals("")) || (CSTerritory.equals("All")) )
                    {
                         ReportCall = ReportCall + "&p_division=All";
                    }
            }
        }
}

if ( UserRole.equals("FBC") || UserRole.equals("RVP") )  
{
     if ( CSMemberCode.equals("") && !(CSReportType.equals("")) &&
            !(CSBDate.equals("")) && (retBeginVal.equals("true")) && 
                !(CSEDate.equals("")) && (retEndVal.equals("true")) )
     {
     %>
             <BR><BR><BR><BR>
             <center>
             <H2>Processing Report.....</H2>
             <BR><BR><BR><BR><BR>
             <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
             <BR> 
             you have requested your report may take several minutes to run.
             </i></font>
           </center>
             <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
             <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
             <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
             <input type="hidden" name="destype" value="cache">
        <%
        }
        else if ( !(CSMemberCode.equals("")) && !(CSReportType.equals("")))
        {
        %>
                 <BR><BR><BR><BR>
                 <center>
                 <H2>Processing Report.....</H2>
                 <BR><BR><BR><BR><BR>
                 <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
                 <BR> 
                 you have requested your report may take several minutes to run.
                 </i></font>
                 </center>
                 <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
                 <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
                 <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
                 <input type="hidden" name="destype" value="cache">
       <%
        }
}
else if ( UserRole.equals("INT")  || UserRole.equals("MGT") )
{
     if ( (CSMemberCode.equals("")) &&
             !(CSBDate.equals("")) && !(CSEDate.equals("")) && 
                  (retBeginVal.equals("true")) && (retEndVal.equals("true")) &&
                (!(CSTerritory.equals("")) || !(CSRegion.equals(""))) && 
                 !(CSReportType.equals(""))  )
     {
     %>
             <BR><BR><BR><BR>
             <center>
             <H2>Processing Report.....</H2>
             <BR><BR><BR><BR><BR>
             <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
             <BR> 
             you have requested your report may take several minutes to run.
             </i></font>
             </center>
             <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
             <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
             <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
             <input type="hidden" name="destype" value="cache">
        <%
        }
        else if ( !(CSMemberCode.equals("")) && !(CSReportType.equals("")) )
        {
        %>
             <BR><BR><BR><BR>
             <center>
             <H2>Processing Report.....</H2>
             <BR><BR><BR><BR><BR>
             <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
             <BR> 
             you have requested your report may take several minutes to run.
             </i></font>
             </center>
             <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
             <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="100">
             <%
                 Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
                 Iterator it = queryPairs.entrySet().iterator();
                 while (it.hasNext()) {
                     Map.Entry pair = (Map.Entry) it.next();
             %>
             <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
             <%
                 }
             %>
             <input type="hidden" name="desformat" value="pdf">
             <input type="hidden" name="destype" value="cache">
        <%
        }
        else
        {
        }
}
else
{
             //Do not display anything on page
}
%>
</form>
</body>
</html>
