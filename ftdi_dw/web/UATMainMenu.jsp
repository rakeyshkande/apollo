<%@ taglib uri="/tags/struts-bean" prefix="bean"%>
<%@ taglib uri="/tags/struts-html" prefix="html"%>
<%@ taglib uri="/tags/struts-logic" prefix="logic"%>

<%@ page import="com.ftdi.common.FtdiUser" %>
<%@ page import="java.util.ArrayList" %>
<jsp:useBean id="ftdiuser" scope="session" class="com.ftdi.common.FtdiUser" />
<jsp:useBean id="UATMainMenuForm" scope="request" class="com.ftdi.forms.UATMainMenuForm" />

<html>
<head>
  <title>UAT Main Menu</title>
</head>
<body>
<Center>
 <img src="ftd_logo.gif" width="133" height="120" alt="" border="0" align="">
 <BR><BR>
 <H2>FTDI Data Warehouse UAT Main Menu</H2>
<%
//Set session variable 
session.setAttribute("ftdiuser",ftdiuser);
session.setAttribute("AppMode", "UAT");

ArrayList lListOfTerritories = UATMainMenuForm.getUATListOfTerritories();
ArrayList lListOfRegions = UATMainMenuForm.getUATListOfRegions();

//temporary solution to display helpful message. need to build a error handling architecture here
String sUATError = (String) session.getAttribute("UATError");
if(sUATError == null)
{
  sUATError ="";
}
%>    
    <Form name="UATMainMenuForm" action="UATMainMenu.do" method="post" >     
    <!-- setting these values are for the existing pages. All new pages should pass around the FtdiUser Object-->
    <input type="hidden" name="UserID" value="<%= ftdiuser.getUserID() %>" />
    <input type="hidden" name="userid" value="<%= ftdiuser.getUserID() %>">
    <input type="hidden" name="territory" value="<%= ftdiuser.getUserTerritory() %>">
    <input type="hidden" name="region" value="<%= ftdiuser.getUserRegion() %>">
    <input type="hidden" name="role" value="<%= ftdiuser.getDWRole() %>">
    <TABLE BORDER=0>
    <TR>
    <TD colspan=2><span style='color:red'><center><B><%= sUATError %></B></center></span></TD>
    </TR>
    <TR>
    <TD align="RIGHT">User ID:</TD>
    <TD><bean:write name="ftdiuser" property="userName" /> - <bean:write name="ftdiuser" property="DWRole" />
    </TD>
    </TR>
    <TR>
    <TD align="RIGHT">UAT Password:</TD>
    <TD><input type="password" name="UATPassword" />
    </TD>
    </TR>
    <TR><TD Align=RIGHT>Territory:</TD>
    <TD>
    <SELECT name="UATTerritory" onChange="CheckRegion()"> 
    <OPTION SELECTED value="">
   <%
      for(int x = 0; x<lListOfTerritories.size(); x++)
      {
   %>
      <OPTION value="<%=((ArrayList)lListOfTerritories.get(x)).get(1)%>">Region <%=((ArrayList)lListOfTerritories.get(x)).get(0)%> (<%=((ArrayList)lListOfTerritories.get(x)).get(1)%>) <%=((ArrayList)lListOfTerritories.get(x)).get(2)%>
   <%
      }
   %>
     </SELECT>
     </TD>
     </tr>
     <TR><TD COLSPAN=2 ALIGN=CENTER>OR</TD></TR>
     <TR>
     <TD ALIGN=RIGHT>Region:</TD>
     <TD>
     <SELECT name="UATRegion" onChange="CheckTerritory()"> 
     <OPTION SELECTED value="">
      <%
      for(int x = 0; x<lListOfRegions.size(); x++)
      {
      %>
      <OPTION value="<%=((ArrayList)lListOfRegions.get(x)).get(0)%>"><%=((ArrayList)lListOfRegions.get(x)).get(0)%>-<%=((ArrayList)lListOfRegions.get(x)).get(1)%>
      <%
      }
      %>
      </SELECT>
      </TD>
      </TR>
	   </TABLE>
    <!-- setting values for the form bean -->
	   <BR>
	   <INPUT type="button" name="switch" value="Normal Login" onClick="window.location='ReportMenu.jsp'">
     <INPUT type="submit" name="submit" value="Switch to Selected UAT User" >
     <INPUT type="button" name="logout" value="Logout" onClick="window.location='Logout.jsp'">
</form>
</Center>
</body>
</html>

<script type="text/javascript" language="JavaScript">
function CheckTerritory()
{
     if (document.UATMainMenuForm.UATTerritory.selectedIndex != 0)
		 {
		  	 document.UATMainMenuForm.UATTerritory.selectedIndex = 0;
		 }
}

function CheckRegion()
{
     if (document.UATMainMenuForm.UATRegion.selectedIndex != 0)
		 {
		  	 document.UATMainMenuForm.UATRegion.selectedIndex = 0;
		 }
}
</SCRIPT>


