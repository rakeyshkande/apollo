<%@ taglib uri="/tags/struts-bean" prefix="bean" %>
<%@ page import="java.sql.*,java.util.*" %>
<html>
<head>
<title>Data Warehouse Activity Reports</title>
      <SCRIPT Language="JavaScript">
         function SubmitMe()
           { 
					  if ( document.DWARpt.role.value == "RVP")
						  {
							 if ( document.DWARpt.rsmreg.value != "True")
					       {
                   alert('You must click on checkbox. ');
                   close();
                 }
               else
                 {
                   document.DWA.submit(); 
                 }
							} 
            else if ( document.DWARpt.role.value == "MGT")
							{
							   if ( document.DWARpt.reg.value == "" 
								    && document.DWARpt.other.value == "" )
					        {
                    alert('You must select a Region/Division or Other User.');
                    close();
                  }
								 else
                   {
                    document.DWA.submit(); 
                   }
							}			
           }
        </script>
</head>
<body onLoad="SubmitMe()">
<FORM name="DWARpt">
<%

String UserID = (String) session.getAttribute("userid");
//Check for session Timeout
if (UserID==null)
{
%>
    <jsp:forward page="SessionTimeout.jsp"/>
<% 
}
String Region = request.getParameter("Region");
String RunRSMRegion = (String)request.getParameter("RunRSMRegion");
String OtherUsers = (String)request.getParameter("OtherUsers");
String UserRole = (String) session.getAttribute("role");
String UserRegion = (String) session.getAttribute("region");

%>

<input type="hidden" name="reg" value="<%= Region %>">
<input type="hidden" name="rsmreg" value="<%= RunRSMRegion %>">
<input type="hidden" name="role" value="<%= UserRole %>">
<input type="hidden" name="other" value="<%= OtherUsers %>">


</FORM>
<FORM name="DWA" action="<bean:message key="report_url" />"  method="post">
<%

String ReportCall = "DWActivityReports.rdf";

if ( UserRole.equals("RVP") )
 {
  if (RunRSMRegion != null)
  {
    ReportCall = ReportCall + "&p_division=" + UserRegion;
  }
 }	
else
 {
   if ( !(Region.equals("")) )
	   {
		  if ( !(Region.equals("All")) )
		    {
	       ReportCall = ReportCall + "&p_division=" + Region;
        }
	    else
	      {   
	       ReportCall = ReportCall + "&p_division=All";
        }
     }   
   else 
	   {	   
      if ( !(OtherUsers.equals("")) )
        {
			   ReportCall = ReportCall + "&p_user=" + OtherUsers;
        }
	   }
 }		
if ( UserRole.equals("RVP") || UserRole.equals("MGT") )
  {
   %>
     <BR><BR><BR><BR>
     <center>
     <H2>Processing Report.....</H2>
     <BR><BR><BR><BR><BR>
     <font size="3"><i>Please wait while your report runs.&nbsp;&nbsp;Depending on the amount of information
     <BR> 
     you have requested your report may take several minutes to run.
     </i></font>
     </center>

     <input type="hidden" name="hidden_run_parameters" value="ftdilogin">
     <input type="hidden" name="report" value="<%= com.ftdi.common.FtdiUtils.getReportUrlReportName(ReportCall) %>" size="120">
     <%
         Map queryPairs = com.ftdi.common.FtdiUtils.getReportUrlQueryPairs(ReportCall);
         Iterator it = queryPairs.entrySet().iterator();
         while (it.hasNext()) {
             Map.Entry pair = (Map.Entry) it.next();
     %>
     <input type="hidden" name="<%= (String) pair.getKey() %>" value="<%= (String) pair.getValue() %>">
     <%
         }
     %>
     <input type="hidden" name="desformat" value="pdf">
     <input type="hidden" name="destype" value="cache">
</FORM>
   <%
  }
  else
    {
             //Do not display anything on page
    }
%>

</body>
</html>
