<html>
<head>
  <title>Data Warehouse Password Maintenance</title>
  <script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.ChangePassword.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
  
  function CheckReturnWindow()
  {
   	   if (document.ChangePassword.screen.value == "AdminReport")
	   {
	   	  window.location="AdminMainMenu.jsp";
	   }   		   
	   else if (document.ChangePassword.screen.value == "Report")
	   {
	   	  window.location="ReportMenu.jsp";
	   }
	   else if (document.ChangePassword.screen.value == "Reset")
	   {
	   	  window.location="ResetPassword.jsp";
	   }
	   else
	   {
	   	   window.location="LoginScreen.jsp";
	   }
  }
  </SCRIPT>
</head>
<body onLoad="ValidateLogin()">
<FORM name="ChangePassword" method="post" action="ValidateChangePassword.jsp"> 
<%
//Get all session attributes
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
String Screen = (String) session.getAttribute("menu");

//Validate User Logged In
%>
<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">
<input type="hidden" name="screen" value= "<%= Screen %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<BR><BR><BR><BR>
<h2>Change Password for Data Warehouse</h2>
<BR>
Your new password must be at least 6 characters long with a maximum of 8 characters.  
<BR>The first character of your new password must be an alpha (A-Z or a-z) character.
<BR><BR>
<TABLE>
<TR>
<TD><font color="#FF0000">*</font>&nbsp;Old Password:</TD>
<TD><input type="password" name="OldPass" size="10" maxlength="10"></TD>
</TR>
<TR>
<TD><font color="#FF0000">*</font>&nbsp;New Password:</TD>
<TD><input type="password" name="NewPass" size="10" maxlength="8"></TD>
</TR>
<TR>
<TD><font color="#FF0000">*</font>&nbsp;Retype New Password:</TD>
<TD><input type="password" name="RetypeNewPass" size="10" maxlength="8"></TD>
</TR>
</TABLE>
<BR><BR>
<INPUT type="submit" name="Submit" value="Change Password">&nbsp;&nbsp;
<INPUT type="button" name="Cancel" value="Cancel" onClick="CheckReturnWindow()">
<BR><BR>
<font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field

</center>
</form>
</body>
</html>

