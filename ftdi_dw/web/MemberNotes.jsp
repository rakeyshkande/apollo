<html>
<head>
<script type="text/javascript" language="JavaScript" SRC="calendar.js"></SCRIPT>
<script type="text/javascript" language="JavaScript">
  function ValidateLogin()
  {
   	   if (document.MN.userid.value == "null")
	   {
	   	  window.location="LoginScreen.jsp";
	   }
  }
</SCRIPT>
<title>Member Notes Report</title>
</head>
<body onload="ValidateLogin()">
<FORM name="MN" method="post" action="MemberNotesReport.jsp" target="MNR">

<%
String UserID = (String) session.getAttribute("userid");

//Check for session Timeout
if (UserID==null)
{
%>
	 <jsp:forward page="LoginScreen.jsp"/>
<% 
}

String UserName = (String) session.getAttribute("username");
String UserRole = (String) session.getAttribute("role");
//Validate User Logged In
%>

<input type="hidden" name="userid" value="<%= UserID %>">
<input type="hidden" name="usernme" value="<%= UserName %>">
<input type="hidden" name="role" value="<%= UserRole %>">

<img src="ftd_logo.gif" width="80" height="69" alt="" border="0" align="left">
<center>
<div align="right"><b>User:</b>&nbsp;<%= UserName %></div>
<div align="right"><b>Role:</b>&nbsp;<%= UserRole %></div>
<BR><BR><BR><BR>
<H2>Member Notes Report</H2>
<BR>

<%
 	 if ( !(UserRole == null) )
	 {
   		if( (UserRole.trim()).equals("FBC") || (UserRole.trim()).equals("RVP") || (UserRole.trim()).equals("INT") || (UserRole.trim()).equals("MGT"))
   		{
   		 		%>
					<TABLE>
					<TR><td><b>Select:</b></td><TR>
					</TR>
					<TR>
					<TD></TD><TD><font color="FF0000">*</font>&nbsp;Member Code:</TD>
					<TD><INPUT type="text" name="MemberCode" size="10" onblur="ValidateMemberCode()"><font size="2"><i>(XX-XXXXAA)</i></font></TD>
					</TR>
					<TR>
					<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Beginning Date:</TD>
					<TD><input type="text" name="BeginDate" size="10" maxlength="10" onBlur="ValidateBeginDate()">
					<a href="javascript:doNothing()" onClick="setDateField(document.MN.BeginDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                                        <img src="calendar.gif" alt="" border=0></a>
					<font size="2"><i>(MM/DD/YYYY)</i></font></TD>
					</TR>
					<TR>
					<TD></TD><TD><font color="#FF0000">*</font>&nbsp;Ending Date:</TD>
					<TD><input type="text" name="EndDate" size="10" maxlength="10" onBlur="ValidateEndDate()">
					<a href="javascript:doNothing()" onClick="setDateField(document.MN.EndDate);top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
                                        <img src="calendar.gif" alt="" border=0></a>
					<font size="2"><i>(MM/DD/YYYY)</i></font></TD>
					</TR>
   	 		</TABLE>
		 		<BR><BR>
		 		<INPUT TYPE="submit" VALUE="Run Report">
		 		<INPUT TYPE="button" VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		 		<BR><BR>
		 		<font color="#FF0000">*</font>&nbsp;&nbsp;Denotes Required field
		 		<BR>
		 		<%
	 	 }
   	 else
	 	 {
	 	 %>
	 	 	  Currently your account does not have the correct role permissions to run this report.
		 		<BR>
		 		Please contact your manager and AS/400 administrator to have the correct role permissions assigned.
		 		<BR><BR>
		 		<INPUT TYPE=Button VALUE="Member Services Menu" onClick="window.location='MemberServicesReportsMenu.jsp'">
		  <%
	 	 }
	}
%>

</CENTER> 
</FORM>
</body>
</html>

<script type="text/javascript" language="JavaScript">
function CheckRegion()
{
 		 if (document.MN.Region.selectedIndex != 0)
		 {
		  	 document.MN.Region.selectedIndex = 0;
		 }
}

function CheckTerritory()
{
		 if (document.MN.Territory.selectedIndex != 0)
		 {
		  	 document.MN.Territory.selectedIndex = 0;
		 }
}

function ValidateBeginDate()
{
 				 var dt = document.MN.BeginDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
			 

				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Beginning Date in format specified');
							 document.MN.BeginDate.focus();
							 document.MN.BeginDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Beginning Date in format specified');
									 document.MN.BeginDate.focus();
									 document.MN.BeginDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Beginning Date in format specified');
							document.MN.BeginDate.focus();
             	 			document.MN.BeginDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.MN.BeginDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}

function ValidateEndDate()
{
 				 var dt = document.MN.EndDate.value;
				 var month = dt.substring(0, dt.indexOf("/"));
				 var dayindx = dt.indexOf("/")+1;
				 var day = dt.substring(dayindx, dt.lastIndexOf("/"));
				 var year = dt.substring(dt.lastIndexOf("/")+1);
		 		
				 if (dt != "")
				 {
				 		if ( (dt.indexOf("/")==-1) && (dt.lastIndexOf("/")==-1))
				 		{
				 		 	 alert('Enter valid Ending Date in format specified');
							 document.MN.EndDate.focus();
							 document.MN.EndDate.select();
				 	  }
				 		else
				 		{		 
				 		 	 if (year.length==3 )
						 	 {
				 		 			 alert('Enter valid Ending Date in format specified');
									 document.MN.EndDate.focus();
									 document.MN.EndDate.select();
						 	 }
						 	 else if (!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{2}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{1}\/[0-9]{2}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{1}\/[0-9]{4}$/)) &&
											!(dt.match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/)) )

         			 {
							alert('Enter valid Ending Date in format specified');
							document.MN.EndDate.focus();
             	 			document.MN.EndDate.select();
							 }
							 else
							 {
 			   			 		 if (year.length==2)
				 			 		 {
 			 		   	 		 		if ( year < 90)
							 				{
						 	 		 	 	 	 newyear = "20" + year;
							 				}
							 				else
							 				{
						 	 			 	 	 newyear = '19' + year;
							 				}
				 			 		}
 			 			   		else
				 			 		{
				 		 	 		 		newyear = year;
				 			 		}
						
				 			 		document.MN.EndDate.value = month + '/' + day + '/' + newyear;
					 		}
					}
		}
}
function ValidateMemberCode()
{
         var tmp_mbr = document.MN.MemberCode.value;
         
         if ( tmp_mbr != "" && ! tmp_mbr.match(/^[0-9]{2}-[0-9]{4}[a-zA-Z]{2}$/) &&
              ! tmp_mbr.match(/^[0-9]+$/) )
         {
             alert('Enter a valid member code in the format specified');
			 document.MN.MemberCode.focus();
             document.MN.MemberCode.select();
         }
}
</SCRIPT>



