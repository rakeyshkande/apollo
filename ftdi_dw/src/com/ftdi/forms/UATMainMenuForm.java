package com.ftdi.forms;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.Action;

import com.ftdi.common.FtdiUser;
import java.util.ArrayList;
import com.ftdi.common.FtdiUtils;
/**
 * UATMainMenuForm contains variables for the UAT Login Main Menu Page
 * 
 * History
 * 2005.04.28   ESO   Creation
 */
public class UATMainMenuForm extends ActionForm
{
  private FtdiUser loginUser;
  private String UATPassword;
  private String UATRegion;
  private String UATTerritory;
  private String UATUserID;
  private String UserID;
  private ArrayList UATListOfRegions;
  private ArrayList UATListOfTerritories;


  public UATMainMenuForm()
  {
    super();
    resetFields();
  }
  
  /**
   * reset - clear out the fields
   * @param mapping
   * @param request
   */
  public void reset(ActionMapping mapping, HttpServletRequest request)
  {
    //clear out the fields
    resetFields();
  }
  
  /**
   * Reset the fileds back to their defaults
   */
  protected void resetFields()
  {
    this.loginUser = null;
    this.UATPassword = null;
    this.UATRegion = null;
    this.UATTerritory = null;
    this.UATUserID = null;
    this.UserID = null;
    this.UATListOfRegions = new ArrayList();
    this.UATListOfTerritories = new ArrayList();
  }

  public FtdiUser getLoginUser()
  {
    return loginUser;
  }

  public void setLoginUser(FtdiUser loginUser)
  {
    this.loginUser = loginUser;
  }

  public String getUATPassword()
  {
    return UATPassword;
  }

  public void setUATPassword(String uatPassword)
  {
    this.UATPassword = uatPassword;
  }

  public String getUATRegion()
  {
    return UATRegion;
  }

  public void setUATRegion(String UATRegion)
  {
    this.UATRegion = UATRegion;
  }

  public String getUATTerritory()
  {
    return UATTerritory;
  }

  public void setUATTerritory(String UATTerritory)
  {
    this.UATTerritory = UATTerritory;
  }

  public String getUATUserID()
  {
    return UATUserID;
  }

  public void setUATUserID(String UATUserID)
  {
    this.UATUserID = UATUserID;
  }
  
  public String getUserID()
  {
    return UserID;
  }

  public void setUserID(String UserID)
  {
    this.UserID = UserID;
  }

  public ArrayList getUATListOfRegions()
  {
    if(UATListOfRegions == null || UATListOfRegions.isEmpty())
    {
      UATListOfRegions = FtdiUtils.getUATListOfRegions();
    }
    return UATListOfRegions;
  }

  public ArrayList getUATListOfTerritories()
  {
    if(UATListOfTerritories == null || UATListOfTerritories.isEmpty())
    {
      UATListOfTerritories = FtdiUtils.getUATListOfTerritories();
    }
    return UATListOfTerritories;
  }

}