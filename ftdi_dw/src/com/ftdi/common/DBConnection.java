package com.ftdi.common;

import javax.sql.DataSource;
import java.util.ResourceBundle;
import com.ftd.osp.utilities.plugins.Logger;

/**
 * Singleton Class to retrive Database connection from a datasource using JNDI Lookup.
 *
 * @author Elsie So
 * @version 1.0
 *
 * History
 * 2005.04.26   ESO   Creation
 *                    - the entire application should use the data source define on the container
 *                    Developer: setup database connections in your jdeveloper called FtdiWeb
 *                    Application container: setup database connections called FtdiWeb
 */

public class DBConnection {
		
   private static DBConnection theInstance;	
   private javax.sql.DataSource dbDataSource = null;

   Logger logger = new Logger("com.ftdi.common.DBConnection");
	
  /**
   * DBConnection constructor
   * 
   * @throws DBConnectionException
   */
   private DBConnection() throws DBConnectionException{
   	
       try {
           logger.debug("DBConnection-In Constructor, looking up datasource.");
           // Retrieve a DataSource through the JNDI Naming Service
	   //java.util.Properties parms = new java.util.Properties();

	   // Create the Initial Naming Context
	   javax.naming.Context ctx = new javax.naming.InitialContext();

           //String sSourceName = ResourceBundle.getBundle("com.ftdi.common.ApplicationResources").getString("DataSourceName");
           String sSourceName = ResourceBundle.getBundle("ApplicationResources").getString("DataSourceName");

	   // Lookup through the naming service to retrieve a DataSource object
	   dbDataSource = (DataSource) ctx.lookup("jdbc/"+sSourceName);

           //use this if connection pooling 
           //dbDataSource = (javax.sql.ConnectionPoolDataSource) ctx.lookup("jdbc/HOPOracleCoreDS");
       } catch(javax.naming.NamingException ne) {
           logger.error("DBConnections throw exception->" + ne.getMessage());
           ne.printStackTrace();
           throw new DBConnectionException(ne.getMessage());
       }     
   }

  /**
   * getInstance(): Returns an instance of DBConnection.
   * 
   * @return DBConnection
   * @throws DBConnectionException
   */
   public static DBConnection getInstance() throws DBConnectionException{

       if(theInstance == null)
           theInstance = new DBConnection();
       return theInstance;

   }		
	
	
   /**
	 * getConnection: returns a java.sql.Connection object. 
   * 
   * It is very important to
   * Perform a close() on the Connection object after use.
	 * 
   * @return java.sql.Connection Database connection object
	 * @throws DBConnectionException
	 * 
	 */
   public java.sql.Connection getConnection() throws DBConnectionException {

       java.sql.Connection dbConnection =  null;

       try {
           logger.debug("Getting connection");
           dbConnection = dbDataSource.getConnection();

           //if doing connection pooling here      
           //dbConnection = dbDataSource.getPooledConnection().getConnection();
       } catch(java.sql.SQLException se) {
           logger.error("DBConnection throw SQLException->" + se.getMessage());
           se.printStackTrace();
           throw new DBConnectionException(se.getMessage());
       }      	     		  
       return dbConnection; 

   }//end getConnection().

}
