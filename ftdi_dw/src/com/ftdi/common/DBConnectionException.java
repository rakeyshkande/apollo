package com.ftdi.common;
/**
 * Class DBConnectionException.
 * This exception is thrown by the DBConnection class to indicate a problem
 * with obtaining a DB Connection from the JNDI datasource. 
 * 
 * @author Elsie So
 * @version 1.0
 * 
 * History
 * 2005.04.26   ESO   Creation
 */
public class DBConnectionException extends Exception {

	/**
	 * Constructor for DBConnectionException
	 * 
	 */
	public DBConnectionException() {
		super();
	}

	/**
	 * Constructor for DBConnectionException
	 */
	public DBConnectionException(String arg0) {
		super(arg0);
	}

}

