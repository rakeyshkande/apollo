package com.ftdi.common;

import java.util.Date;
import java.text.DateFormat;
import java.util.Locale;

/**
 * FtdiUser: Holds the FTDI DW User information. This is a new object it will be stored in session
 * after user authentication.
 * 
 * History
 * 2005.04.27   ESO Creation
 * 
 */
public class FtdiUser 
{
  String UserID;
  String UserClass;
  String GroupProfile;
  String GroupAuthority;
  String GroupProfileIndicator;
  String Status;
  String UserName;
  String DWStatus;
  String DWRole;
  int DWLoginAttempts;
  Date DWLastLogin;
  Date DWPasswordChangeDate;
  int DWDays;
  String UserTerritory;
  String UserRegion;
  int AS400Date;
  byte[] UserStoredPassword;

  public FtdiUser()
  {
  }

  public String getUserID()
  {
    return UserID;
  }

  public void setUserID(String UserID)
  {
    this.UserID = UserID;
  }

  public String getUserClass()
  {
    return UserClass;
  }

  public void setUserClass(String UserClass)
  {
    this.UserClass = UserClass;
  }

  public String getGroupProfile()
  {
    return GroupProfile;
  }

  public void setGroupProfile(String GroupProfile)
  {
    this.GroupProfile = GroupProfile;
  }

  public String getGroupAuthority()
  {
    return GroupAuthority;
  }

  public void setGroupAuthority(String GroupAuthority)
  {
    this.GroupAuthority = GroupAuthority;
  }

  public String getGroupProfileIndicator()
  {
    return GroupProfileIndicator;
  }

  public void setGroupProfileIndicator(String GroupProfileIndicator)
  {
    this.GroupProfileIndicator = GroupProfileIndicator;
  }

  public String getStatus()
  {
    return Status;
  }

  public void setStatus(String Status)
  {
    this.Status = Status;
  }

  public String getUserName()
  {
    return UserName;
  }

  public void setUserName(String UserName)
  {
    this.UserName = UserName;
  }

  public String getDWStatus()
  {
    return DWStatus;
  }

  public void setDWStatus(String DWStatus)
  {
    this.DWStatus = DWStatus;
  }

  public boolean isSuperUser()
  {
    boolean bIsSuper = false;
    if(DWStatus.trim().toUpperCase().equals("SUPER"))
    {
      bIsSuper = true;
    }
    return bIsSuper;
  }

  public String getDWRole()
  {
    return DWRole;
  }

  public void setDWRole(String DWRole)
  {
    this.DWRole = DWRole;
  }
  
  public int getDWLoginAttempts()
  {
    return DWLoginAttempts;
  }

  public void setDWLoginAttempts(int DWLoginAttempts)
  {
    this.DWLoginAttempts = DWLoginAttempts;
  }

  public Date getDWLastLogin()
  {
    return DWLastLogin;
  }

  public void setDWLastLogin(Date DWLastLogin)
  {
    this.DWLastLogin = DWLastLogin;
  }

  /**
   * getDWLastLoginStr
   * @return String in MON/DD/YYYY
   */
    public String getDWLastLoginStr() {
        DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
        
        if (getDWLastLogin() == null) {
            return "";
        } else {
            return fmt.format(getDWLastLogin());
        }        
    }

  public Date getDWPasswordChangeDate()
  {
    return DWPasswordChangeDate;
  }

  public void setDWPasswordChangeDate(Date DWPasswordChangeDate)
  {
    this.DWPasswordChangeDate = DWPasswordChangeDate;
  }

  /**
   * getDWPasswordChangeDate
   * @return String in MON/DD/YYYY
   */
    public String getDistributionStartDateStr() {
        DateFormat fmt = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
        
        if (getDWPasswordChangeDate() == null) {
            return "";
        } else {
            return fmt.format(getDWPasswordChangeDate());
        }        
    }

  public int getDWDays()
  {
    return DWDays;
  }

  public void setDWDays(int DWDays)
  {
    this.DWDays = DWDays;
  }

  public String getUserTerritory()
  {
    return UserTerritory;
  }

  public void setUserTerritory(String UserBelongToTerrority)
  {
    this.UserTerritory = UserBelongToTerrority;
  }

  public String getUserRegion()
  {
    return UserRegion;
  }

  public void setUserRegion(String UserRegion)
  {
    this.UserRegion = UserRegion;
  }

  public int getAS400Date()
  {
    return AS400Date;
  }

  public void setAS400Date(int AS400Date)
  {
    this.AS400Date = AS400Date;
  }

  public byte[] getUserStoredPassword()
  {
    return UserStoredPassword;
  }

  public void setUserStoredPassword(byte[] UserStoredPassword)
  {
    this.UserStoredPassword = UserStoredPassword;
  }
}