package com.ftdi.common;

import com.ftd.osp.utilities.plugins.Logger;

import oracle.sql.NUMBER;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * FtdiUtils class contains common utilities method 
 * 
 * History
 * 2005.04.27 ESo   Creation
 * 
 */
public class FtdiUtils  extends Object
{

  public static Logger logger = new Logger("com.ftdi.common.FtdiUtils");

  public FtdiUtils()
  {
  }
  private static ResourceBundle  ftdiResource = null;
  
  public static String notNull(String in) {
    return in==null? "": in;
  }

  public static String notNull(NUMBER in) {
    return in==null? new NUMBER().toString(): in.toString();
  }

  
  public static boolean hasContent(String in) {
      boolean ret = false;
      if(in == null) {
          ret = false;
      } else if(in.length() > 0 && !in.equals(" ")) {
          ret = true;
      }
      
      return ret;
  }

  public static String[] notNull(String[] in) {
    String[] temp = new String[10];
    return in==null ? temp : in;
  }  
 
  public static ResourceBundle getFtdiResourceBundle()
  {
    if (ftdiResource == null)
    {
        //ftdiResource = ResourceBundle.getBundle("com.ftdi.common.ApplicationResources");
        ftdiResource = ResourceBundle.getBundle("ApplicationResources");
    }
    return ftdiResource;
  }
  
  /**
   * ValidateUATUser method takes the password and validate it
   * @param sPassword
   * @return boolean 
   */
  public static boolean validateUATUser(String sPassword)
  {
    boolean bValid = false;
    String sCorrectPassword = getFtdiResourceBundle().getString("UATPASS");
    if(sPassword.trim().equalsIgnoreCase(sCorrectPassword))
    {
      bValid = true;
    }
    return bValid;
  }

 /**
   * Get UAT List of Regions - return a list of UAT approved regions
   * Return ArrayList contains another ArrayList with: REGION, MANAGER_NAME, MANGER_USERID
   *
   * @return ArrayList
   */
  public static ArrayList getUATListOfRegions()
  {
        ArrayList lListOfRegions = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sTestRegions = getFtdiResourceBundle().getString("UATREGIONS");
    
        String sqlStatement = "SELECT REGION, MANAGER_NAME, MANAGER_USERID " +
                              "FROM REGION_INFO " +
                              "WHERE REGION IN "+ sTestRegions + " ORDER BY REGION,MANAGER_NAME " ;
                              
        ArrayList lRegionInfo = new ArrayList();
        try{
            logger.debug("Calling getConnection()");
            conn = DBConnection.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStatement);
            if(rs != null)
            {
                while(rs.next())
                {
                    lRegionInfo = new ArrayList() ;
                    lRegionInfo.add(FtdiUtils.notNull(rs.getString("REGION")));
                    lRegionInfo.add(FtdiUtils.notNull(rs.getString("MANAGER_NAME")));
                    lRegionInfo.add(FtdiUtils.notNull(rs.getString("MANAGER_USERID")));
                    lListOfRegions.add(lRegionInfo);
                }
            }
        }catch (DBConnectionException e)
        {
          logger.error("FtdiUtil.getUATListOfRegions catch DBConnectionException->" + e.toString());
        }catch (Exception ex)
        {
          logger.error("FtdiUtil.getUATListOfRegions catch Exception->" + ex.toString());
        }finally
        {
          try{
            logger.debug("Calling close()");
            conn.close();
          if(stmt != null)
            stmt.close();
          if(rs != null)
            rs.close();
          }catch(Exception e){
            logger.error("Error closing connection: " + e);
          }
        }
      return lListOfRegions;
  }
  
 /**
   * Get UAT List of Territories - return a list of UAT approved regions and their territories
   * Return ArrayList contains another ArrayList with: REGION, TERRITORY, FBC_NAME, FBC_USERID
   *
   * @return ArrayList
   */
   public static ArrayList getUATListOfTerritories()
   {
        ArrayList lListOfTerr = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sTestRegions = getFtdiResourceBundle().getString("UATREGIONS");
  
        String sqlStatement = "SELECT T.REGION, TI.TERRITORY, FBC_NAME, FBC_USERID " + 
                              "FROM TERRITORY_INFO TI, REGION_TO_TERRITORY T " + 
                              "WHERE TI.TERRITORY = T.TERRITORY " + 
                              "AND T.REGION IN "+ sTestRegions + " ORDER BY T.REGION, TI.TERRITORY, FBC_NAME " ;
                              
        ArrayList lTerrInfo = new ArrayList();
        try{
            logger.debug("Calling getConnection()");
            conn = DBConnection.getInstance().getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStatement);
            if(rs != null)
            {
                while(rs.next())
                {
                    lTerrInfo = new ArrayList() ;
                    lTerrInfo.add(FtdiUtils.notNull(rs.getString("REGION")));
                    lTerrInfo.add(FtdiUtils.notNull(rs.getString("TERRITORY")));
                    lTerrInfo.add(FtdiUtils.notNull(rs.getString("FBC_NAME")));
                    lTerrInfo.add(FtdiUtils.notNull(rs.getString("FBC_USERID")));
                    lListOfTerr.add(lTerrInfo);
                }
            }
        }catch (DBConnectionException e)
        {
          logger.error("FtdiUtil.getListOfTerritories catch DBConnectionException->" + e.toString());
        }catch (Exception ex)
        {
          logger.error("FtdiUtil.getListOfTerritories catch Exception->" + ex.toString());
        }finally
        {
          try{
            logger.debug("Calling close()");
            conn.close();
          if(stmt != null)
            stmt.close();
          if(rs != null)
            rs.close();
          }catch(Exception e){
            logger.error("Error closing connection: " + e);
          }
        }
      return lListOfTerr;
    }
    
    public static String getReportUrlReportName(String reportUrl) {
        int maxIndex = reportUrl.indexOf("&") == -1 ? reportUrl.length() : reportUrl.indexOf("&");
        return reportUrl.substring(0, maxIndex);
    }
    
    public static Map getReportUrlQueryPairs(String reportUrl) {
        
        logger.debug("getReportUrlQueryPairs: Complete report URL: " + reportUrl);
        logger.debug("getReportUrlQueryPairs: Report URL " + reportUrl + " REPORT NAME: " + FtdiUtils.getReportUrlReportName(reportUrl));
        Map queryPairs = new HashMap();
        
        if (reportUrl != null && reportUrl.length() > 0) {
        
            String reportName =  FtdiUtils.getReportUrlReportName(reportUrl);
            
            if (reportName.length() != reportUrl.length()) {
        
        String[] props = reportUrl.substring(reportName.length() + 1).split("&");
        for (int i = 0; i < props.length; i++) {
            String[] nameValue = props[i].split("=");
            queryPairs.put(nameValue[0], nameValue[1]);
            logger.debug("getReportUrlQueryPairs: Report URL " + reportUrl + " KEY: " + nameValue[0] + " VALUE: " + nameValue[1]);
        }
        }
        }
        
        return queryPairs;
    }
}







