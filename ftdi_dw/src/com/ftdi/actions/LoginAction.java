package com.ftdi.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.util.ArrayList;
import com.ftdi.forms.UATMainMenuForm;
import com.ftdi.common.FtdiUser;
import com.ftdi.common.FtdiUtils;
import FTDI.ValidateLogin;

import com.ftd.osp.utilities.plugins.Logger;

/**
 * LoginAction is action class for UATMainMenu - contains logics checking UAT user
 *
 * History
 * 2005.04.28   ESO   Creation
 *
 */
public class LoginAction extends Action
{

    /**
     *  ActionForward will be called by the UATMainMenu.jsp page
     * 
     * @param mapping
     * @param form
     * @param request
     * @param response
     * @return  ActionForward 
     * @throws java.lang.Exception
    */
    public ActionForward execute(
		ActionMapping mapping,
		ActionForm form,
		HttpServletRequest request,
		HttpServletResponse response)
		throws Exception {

    	ActionForward forward = new ActionForward();
        Logger logger = new Logger("com.ftdi.actions.LoginAction");
    
        //get the information enter by the user
	UATMainMenuForm UatForm = (UATMainMenuForm) form;
        //Login User Information
        FtdiUser loginUser = null;
        //Login User ID
        String sUserID = null;
    
        //Select UAT User - either from territory or region
        String sSelectedUATUser = null;
   
        // the data from request    
        if(UatForm == null && request.getSession().getAttribute("userid")== null)
        {
            logger.error("Critial Application Error: LoginAction.ActionForward catch error unknown userID");
            request.getSession().setAttribute("UATError", "Your user login is invalid. Please contact system administrator.");   
            forward = mapping.findForward("failure");		
        }
        //Identified Login User id
        sUserID = (String) request.getSession().getAttribute("userid");
        if(UatForm.getLoginUser() == null)
        {
            UatForm.setLoginUser(ValidateLogin.getInstance().AS400UserInfo(sUserID));
        }
        loginUser = UatForm.getLoginUser();
        if(loginUser == null)
        { 
            //temporary error handling need to build a better architecture
            logger.error("Critial Application Error: LoginAction.ActionForward catch error invalid UserID");
            request.getSession().setAttribute("UATError", "Invalid User ID. Please contact System Administrator.");   
            // return to input page
            return (new ActionForward(mapping.getInput()));
        }
        request.getSession().setAttribute("ftdiUser", loginUser);   
        //we have all the information we need about our Login User
        //now check whether UAT password is correct
        if (!FtdiUtils.validateUATUser(UatForm.getUATPassword()))
        {
            logger.error("Application Error: LoginAction.ActionForward catch error invalid password userid->"+sUserID);
            request.getSession().setAttribute("UATError", "You have entered an incorrect UAT password, please try again.");   
            // return to input page
            return (new ActionForward(mapping.getInput()));
        }

        //If Territory find territory user id and switch FtdiUser object to test user object 
        if(UatForm.getUATTerritory()!= null && !UatForm.getUATTerritory().trim().equals(""))
        {
            ArrayList lValidTerritories = FtdiUtils.getUATListOfTerritories();
            int i = 0;
            while(sSelectedUATUser == null || i <lValidTerritories.size())
            {
                if(UatForm.getUATTerritory().equals(((ArrayList) lValidTerritories.get(i)).get(1)))
                {
                    sSelectedUATUser = (String) ((ArrayList) lValidTerritories.get(i)).get(3);
                }
                i++;
            }
        } else if (UatForm.getUATRegion()!= null && !UatForm.getUATRegion().trim().equals(""))
        {
            ArrayList lValidRegions = FtdiUtils.getUATListOfRegions();
            int i = 0;
            while(sSelectedUATUser == null || i <lValidRegions.size())
            {
                if(UatForm.getUATRegion().equals(((ArrayList) lValidRegions.get(i)).get(0)))
                {
                    sSelectedUATUser = (String) ((ArrayList) lValidRegions.get(i)).get(2);
                }
                i++;
            }
        } else
        {
            //user didn't select  a territory or region
            logger.error("Warning: User didn't select a region or territory");
            request.getSession().setAttribute("UATError", "Please select either a territory or region.");   
      
            // return to input page
            return (new ActionForward(mapping.getInput()));
        }
    
        //If Region find region user id and switch FtdiUser object to test user object 
        loginUser = ValidateLogin.getInstance().AS400UserInfo(sSelectedUATUser);

        request.getSession().setAttribute("ftdiUser", loginUser);   
        request.getSession().setAttribute("userid", loginUser.getUserID());   
        request.getSession().setAttribute("username", loginUser.getUserName());   
    
        if (loginUser.getUserTerritory().trim().equals(""))
        {
            request.getSession().setAttribute("territory", "NA");
        } else
        {
            request.getSession().setAttribute("territory", loginUser.getUserTerritory());     
        }
      
        if (loginUser.getUserRegion().trim().equals(""))
        {
            request.getSession().setAttribute("region","NA");
        } else
        {
            request.getSession().setAttribute("region", loginUser.getUserRegion());         
        }
        request.getSession().setAttribute("role", loginUser.getDWRole());  

        forward = mapping.findForward("success");				
	return (forward);
    }

    public LoginAction()
    {
        super();
    }
}