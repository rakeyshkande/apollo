package FTDI;
import java.io.Serializable;
import java.sql.*;

public class ChangePassword implements Serializable
{
  public ChangePassword()
  {
  }

  public ResultSet DWPassword(Connection con, String UserID) throws SQLException
  {
      String SQL = "SELECT DW_Password from AS400PWD where User_Profile_Name = '" + UserID + "'";
      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }

  public void UpdateDWPassword(Connection con, String UserID, byte[] NewUserPassword) throws SQLException
  {
      String SQL = "UPDATE AS400PWD SET DW_Password = ?, DW_Password_Change_Date = TO_Date(SYSDATE+90) WHERE User_Profile_Name = ?";
			PreparedStatement pstmt = con.prepareStatement(SQL);
   	  pstmt.setBytes(1,NewUserPassword);
      pstmt.setString(2,UserID);
	   	pstmt.executeUpdate();
  }
}