package FTDI;
import java.io.Serializable;
import java.sql.*;

/**
 * 
 * History
 * Creation
 * 2005.03.24     ESO Create GetTerritoryInfo and GetRegionInfo
 */
public class TerritoryDetail implements Serializable
{
  public TerritoryDetail()
  {
  }

  /**
   * Get Territory Information
   * If user is MGT or INT role, user can see all Territory and regions.  
   * If user is RVP, they can only see his own territory in his region.
   * This method only return the correct territory list.
   * 
   * @param con     Database Connection
   * @param UserID  Login user
   * @param RoleName Login user Role
   * @return 
   * @throws java.sql.SQLException
   */
  public ResultSet GetTerritoryInfo(Connection con, String UserID, String RoleName) throws SQLException
  {
    ResultSet rs = null;
    String SQL = "SELECT I.TERRITORY, I.FBC_NAME ";
    
    if(RoleName.trim().equals("MGT") || RoleName.trim().equals("INT"))
    {
      SQL = SQL + " FROM Territory_Info I ";
      SQL = SQL + " WHERE I.Territory != ' ' AND I.Territory != 'REP' ";
    }else if (RoleName.trim().equals("RVP"))
    {
      SQL = SQL + " FROM Region_Info a1, Territory_Info I, Region_to_Territory c1 ";
      SQL = SQL + " WHERE a1.Manager_UserID = '" + UserID.toUpperCase().trim() + "' ";
      SQL = SQL + " AND a1.Region = c1.Region AND c1.Territory = I.Territory ";
    }else
    {
     //return nothing if role name not found
      return rs;
    }
    SQL = SQL + " ORDER BY I.Territory ";    
    Statement stmt = con.createStatement();
    rs = stmt.executeQuery(SQL);
    return rs;
  }

  /**
   * Get Region Information
   * If user is MGT or INT role, user can see all Territory and regions.  
   * If user is RVP, they can only see his own territory in his region.
   * This method only return the correct region list.
   * 
   * @param con     Database Connection
   * @param UserID  Login user
   * @param RoleName Login user Role
   * @return 
   * @throws java.sql.SQLException
   */
  public ResultSet GetRegionInfo(Connection con, String UserID, String RoleName) throws SQLException
  {
    ResultSet rs = null;
    String SQL = "SELECT R.REGION, R.MANAGER_NAME ";
    
    if(RoleName.trim().equals("MGT") || RoleName.trim().equals("INT"))
    {
      SQL = SQL + " FROM REGION_INFO R ";
      SQL = SQL + " WHERE R.REGION !=  ' ' ";
    }else if (RoleName.trim().equals("RVP"))
    {
      SQL = SQL + " FROM REGION_INFO R, Territory_Info I, Region_to_Territory c1 ";
      SQL = SQL + " WHERE R.Manager_UserID = '" + UserID.toUpperCase().trim() + "' ";
      SQL = SQL + " AND R.Region = c1.Region AND c1.Territory = I.Territory ";
    }else
    {
     //return nothing if role name not found
      return rs;
    }
    SQL = SQL + " ORDER BY R.REGION ";
    
    Statement stmt = con.createStatement();
    rs = stmt.executeQuery(SQL);
    return rs;
  }
  
  public ResultSet TerritoryInfo(Connection con, String UserID) throws SQLException
  {
    String SQL = "SELECT b1.Territory, b1.FBC_Name ";
		SQL = SQL + "FROM Region_Info a1, Territory_Info b1, Region_to_Territory c1 ";
		SQL = SQL + "WHERE a1.Manager_UserID = '" + UserID + "' ";
		SQL = SQL + "AND a1.Region = c1.Region ";
		SQL = SQL + "AND c1.Territory = b1.Territory ";
    SQL = SQL + "ORDER BY Territory";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }

  public ResultSet AllTerritoryInfo(Connection con) throws SQLException
  {
    String SQL = "SELECT * FROM Territory_Info ";
    SQL = SQL + "WHERE Territory != ' ' ";
    SQL = SQL + "AND Territory != 'REP' ";
    SQL = SQL + "ORDER BY Territory";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }

    public ResultSet AllRegionInfo(Connection con) throws SQLException
  {
    String SQL = "SELECT * FROM Region_Info ";
    SQL = SQL + "WHERE Region != ' ' ";
    SQL = SQL + "ORDER BY Region";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }

  public ResultSet MemberRevenueYears(Connection con) throws SQLException
  {

      String SQL = "SELECT DISTINCT TO_CHAR(Effective_Month_Date, 'YYYY') AS RevenueYear ";
      SQL = SQL + "FROM JDE_Member_Revenue_Summary ";
      SQL = SQL + "ORDER BY TO_CHAR(Effective_Month_Date, 'YYYY')";

      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }
}