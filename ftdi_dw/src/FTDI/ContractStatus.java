package FTDI;
import java.io.Serializable;
import java.sql.*;

public class ContractStatus implements Serializable 
{
  public ContractStatus()
  {
  }

  public ResultSet FBCInfo(Connection con, String UserID) throws SQLException
  {
    String SQL = "SELECT b1.Territory, b1.FBC_Name ";
		SQL = SQL + "FROM Region_Info a1, Territory_Info b1, Region_to_Territory c1 ";
		SQL = SQL + "WHERE a1.Manager_UserID = '" + UserID + "' ";
		SQL = SQL + "AND a1.Region = c1.Region ";
		SQL = SQL + "AND c1.Territory = b1.Territory ";
    SQL = SQL + "ORDER BY b1.Territory";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }

  public ResultSet TerritoryInfo(Connection con) throws ClassNotFoundException,
                                                                       SQLException
  {
    String SQL = "SELECT * FROM Territory_Info ";
    SQL = SQL + "WHERE Territory != ' ' ";
    SQL = SQL + "ORDER BY Territory";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }

  public ResultSet RegionInfo(Connection con) throws ClassNotFoundException,
                                                                    SQLException
  {
    String SQL = "SELECT * FROM Region_Info ";
    SQL = SQL + "WHERE Region != ' ' ";
    SQL = SQL + "ORDER BY Region";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }
  
}