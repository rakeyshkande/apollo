package FTDI;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.Serializable;
import java.sql.*;

/**
* ValidateUserAccount
*
* History
* 2005.06.20 ESO Removed database connection close() statments
*/
public class ValidateUserAccount implements Serializable
{
  public ValidateUserAccount()
  {
  }

  public ResultSet DWAcctInfo(Connection con, String UserID) throws SQLException
  {
      Logger logger = new Logger("com.ftdi.common.DBConnection");

      String SQL = "SELECT Status,User_Name from AS400PWD where User_Profile_Name = '" + UserID + "'";
      Statement stmt = null;
      ResultSet rs =  null;
      try{
          stmt = con.createStatement();
       	  rs = stmt.executeQuery(SQL);
      }catch(SQLException ex){
          logger.error("ValidateUserAccount catch SQLException->" + ex.toString());
          throw ex;
      }
      return(rs);
  }
}