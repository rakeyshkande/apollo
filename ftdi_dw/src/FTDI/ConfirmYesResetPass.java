package FTDI;
import java.io.Serializable;
import java.sql.*;

public class ConfirmYesResetPass implements Serializable
{
  public ConfirmYesResetPass()
  {
  }
  public void ResetDWPass(Connection con, byte[] UserPassword, String UserID) throws SQLException
  {
   String SQL = "Update AS400PWD set DW_Password = ?, DW_Password_Change_Date = SYSDATE WHERE User_Profile_Name = ?";
   PreparedStatement pstmt = con.prepareStatement(SQL);
   pstmt.setBytes(1,UserPassword);
   pstmt.setString(2,UserID);
   pstmt.executeUpdate();

  }
}