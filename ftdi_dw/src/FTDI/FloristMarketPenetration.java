package FTDI;
import java.io.Serializable;
import java.sql.*;


public class FloristMarketPenetration implements Serializable
{
  public FloristMarketPenetration()
  {
  }

  public ResultSet MinInvoiceDateYear (Connection con) throws SQLException
  {
      String SQL = "SELECT TO_NUMBER(TO_CHAR(MIN(Invoice_Date), 'YYYY')) AS Min_Year ";
      SQL = SQL + "FROM JDE_Sales_Order_Detail";
      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }

  public ResultSet MaxInvoiceDateYear (Connection con) throws SQLException
  {
      String SQL = "SELECT TO_NUMBER(TO_CHAR(MAX(Invoice_Date), 'YYYY')) AS Max_Year ";
      SQL = SQL + "FROM JDE_Sales_Order_Detail";
      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }
}