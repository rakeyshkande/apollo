package FTDI;
import java.io.Serializable;
import java.sql.*;

public class ARAgedBalances implements Serializable 
{
  public ARAgedBalances()
  {
  }

    public ResultSet TerritoryInfo(Connection con) throws SQLException
  {
    String SQL = "SELECT * FROM Territory_Info ";
    SQL = SQL + "WHERE Territory != ' ' ";
    SQL = SQL + "ORDER BY Territory";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }

  public ResultSet RegionInfo(Connection con) throws SQLException
  {
    String SQL = "SELECT * FROM Region_Info ";
    SQL = SQL + "WHERE Region != ' ' ";
    SQL = SQL + "ORDER BY Region";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }
   public ResultSet CollectionInfo(Connection con) throws SQLException
  {
    String SQL = "SELECT * FROM Collection_Info ";
    SQL = SQL + "WHERE Collection != ' ' ";
    SQL = SQL + "ORDER BY Collection";

    Statement stmt = con.createStatement();
    ResultSet rs = stmt.executeQuery(SQL);

    return(rs);
  }
}