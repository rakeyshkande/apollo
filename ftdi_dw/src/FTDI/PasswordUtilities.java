package FTDI;
import java.io.Serializable;
import java.sql.*;
import java.util.*;
import java.security.*;

public class PasswordUtilities implements Serializable
{
  public PasswordUtilities()
  {
  }

  public byte[] EncryptPassword (String Password) throws NoSuchAlgorithmException
  {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] msg = Password.getBytes();
      md.update(msg);
      byte[] UserPassword = md.digest();

      return UserPassword;
  }

  public void NewUserDefaultPassword (Connection con, String DefaultPassword) throws NoSuchAlgorithmException,
                                                                                     SQLException
  {
      MessageDigest md = MessageDigest.getInstance("MD5");
      byte[] msg = DefaultPassword.getBytes();
      md.update(msg);
      byte[] NewDefaultPassword = md.digest();

      String SQL = "UPDATE FTDI_Global_Parms SET Default_Password = ? WHERE FTDI_GLOBAL_PARMS_KEY = 1";
			PreparedStatement pstmt = con.prepareStatement(SQL);
   	  pstmt.setBytes(1,NewDefaultPassword);
	   	pstmt.executeUpdate();
  }
}