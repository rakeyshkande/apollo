package FTDI;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.Serializable;
import java.sql.*;


public class DateUtilities implements Serializable
{
  public DateUtilities()
  {
  }

    public String CurrentMonthText()
  {
      java.util.Date today = new java.util.Date();
      SimpleDateFormat MonthText = new SimpleDateFormat("MMMM");
      return(MonthText.format(today));
  }

  public String CurrentMonthDigit()
  {
      java.util.Date today = new java.util.Date();  
      SimpleDateFormat MonthDigit = new SimpleDateFormat("MM");
      return(MonthDigit.format(today));
  }

  public int CurrentDayDigit()
  {
      java.util.Date today = new java.util.Date();  
      SimpleDateFormat DayDigit = new SimpleDateFormat("d");
      int iDay = Integer.parseInt(DayDigit.format(today));
      return(iDay);
  }

  public String CurrentYear()
  {
     java.util.Date today = new java.util.Date();
     SimpleDateFormat YearFMT = new SimpleDateFormat("yyyy");
     return(YearFMT.format(today));

  }
  public String validateDate (Connection con, String indate) 
  {
    try
    {
      String SQL = "SELECT to_date ('" + indate + "', 'MM/DD/YYYY')";
      SQL = SQL + "From dual";
      Statement stmt = con.createStatement();
      ResultSet rs = stmt.executeQuery(SQL);
    }
    catch(SQLException se)
    {
        return("false");
    }
    return("true");
  }
}