package FTDI;
import java.io.Serializable;
import java.sql.*;

public class DWLoginAttempts implements Serializable
{
  public DWLoginAttempts()
  {
  }

  public void ResetLoginAttempt(Connection con, String UserID) throws SQLException
  {
      String SQL = "UPDATE AS400PWD SET DW_Login_Attempts = 0, ";
      SQL = SQL + "DW_Last_Login = SYSDATE ";
      SQL = SQL + "WHERE User_Profile_Name = ?";
      PreparedStatement pstmt = null;
      try{
      pstmt = con.prepareStatement(SQL);
      pstmt.setString(1,UserID);
      pstmt.executeUpdate();
      }catch (SQLException ex)
      {
        throw ex;
      }finally
      {
        con.close();
        if(pstmt != null)
          pstmt.close();
      }
  }
}