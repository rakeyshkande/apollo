package FTDI;

import com.ftd.osp.utilities.plugins.Logger;

import java.io.Serializable;
import java.sql.*;

import com.ftdi.common.DBConnection;
import com.ftdi.common.DBConnectionException;

/**
 * DWLogin create connection for the application
 * 
 * History
 * 2003   Creation
 * 2005   ESO   Modified to use the JNDI to lookup data source instead of hard coding 
 *              the connection string here
 * 
 */
public class DWLogin implements Serializable
{

  public DWLogin()
  {
  }
 
  public Connection Connect() throws ClassNotFoundException, SQLException
  {
      Logger logger = new Logger("FTDI.DWLogin");
      Connection conn = null;
      try
      {
          logger.debug("Calling getConnection()");
          conn = DBConnection.getInstance().getConnection();
      }
      catch(DBConnectionException ex)
      {
          logger.error("DWLogin catch DBConnectionException ->" + ex.toString());
      } 
    return conn;     
  }
  
  public void Close(Connection con) throws ClassNotFoundException, SQLException {
      Logger logger = new Logger("FTDI.DWLogin");
      try {
          logger.debug("Closing connection");
          con.close();
      }
      catch (Exception e) {
          logger.error("Error closing connection: " + e);
      }
  }
 
 }
