package FTDI;
import java.io.Serializable;
import java.sql.*;

public class ProductSalesInventory implements Serializable 
{
  public ProductSalesInventory()
  {
  }

  public ResultSet CodifiedProducts(Connection con) throws SQLException
  {
      String SQL = "Select distinct Codified_Symbol from Codification_Info";
		  Statement stmt = con.createStatement();
	   	ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }

  public ResultSet CodifiedYears(Connection con) throws SQLException
  {
		  String SQL = "Select distinct Codified_Year from Codification_Info";
 		  Statement stmt = con.createStatement();
   	  ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }
}