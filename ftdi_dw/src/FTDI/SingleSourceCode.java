package FTDI;
import java.io.Serializable;
import java.sql.*;


public class SingleSourceCode implements Serializable
{
  public SingleSourceCode()
  {
  }

  public ResultSet SourceCodes(Connection con) throws SQLException
  {
      String SQL = "SELECT DISTINCT User_Defined_Code, Description";
      SQL = SQL + " FROM JDE_User_Codes";
      SQL = SQL + " WHERE System_Code = '42'";
      SQL = SQL + " AND User_Defined_Codes = 'SC'";
      SQL = SQL + " AND User_Defined_Code NOT LIKE ' %'";
      SQL = SQL + " ORDER BY User_Defined_code";
		  Statement stmt = con.createStatement();
	   	ResultSet rs = stmt.executeQuery(SQL);

      return(rs);
  }
}