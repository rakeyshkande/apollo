package FTDI;
import java.io.Serializable;
import java.sql.*;

public class DWRoleAdmin implements Serializable
{
  public DWRoleAdmin()
  {
  }

  public ResultSet DWRoles(Connection con) throws SQLException
  {
       String SQL = "SELECT DISTINCT DW_Role from AS400_To_DW_Roles ";
       SQL = SQL + "WHERE DW_Role IS NOT NULL ";
       SQL = SQL + "ORDER BY DW_Role";
       Statement stmt = con.createStatement();
       ResultSet rs = stmt.executeQuery(SQL);

       return(rs);
  }

  public ResultSet AS400PWDRoles(Connection con) throws SQLException
  {
       String SQL = "SELECT DISTINCT Group_Profile ";
       SQL = SQL + "FROM AS400PWD ";
       SQL = SQL + "WHERE Group_Profile NOT IN (SELECT AS400_Role FROM AS400_To_DW_Roles) ";
       SQL = SQL + "ORDER BY Group_Profile";
       Statement stmt = con.createStatement();
       ResultSet rs = stmt.executeQuery(SQL);

       return(rs);
  }

  public ResultSet AS400Roles(Connection con) throws SQLException
  {
       String SQL = "SELECT DISTINCT AS400_Role FROM AS400_To_DW_Roles ";
       SQL = SQL + "ORDER BY AS400_Role";
       Statement stmt = con.createStatement();
       ResultSet rs = stmt.executeQuery(SQL);

       return(rs);
  }

  public void ModifyDWRoleMapping(Connection con, String DWRole, String AS400Role) throws SQLException
  {
      String SQL = "UPDATE AS400_To_DW_Roles SET DW_Role = ? ";
      SQL = SQL + "WHERE AS400_Role = ?";
      PreparedStatement pstmt = con.prepareStatement(SQL);
      pstmt.setString(1, DWRole);
      pstmt.setString(2, AS400Role);
      pstmt.executeUpdate();
  }

  public void AddDWRoleMapping(Connection con, String DWRole, String AS400Role) throws SQLException
  {
      String SQL = "INSERT INTO AS400_To_DW_Roles VALUES (?, ?)";
      PreparedStatement pstmt = con.prepareStatement(SQL);
      pstmt.setString(1, AS400Role);
      pstmt.setString(2, DWRole);
      pstmt.execute();
  }

  public void DeleteDWRoleMapping(Connection con, String AS400Role) throws SQLException
  {
      String SQL = "DELETE FROM AS400_To_DW_Roles WHERE AS400_Role = ?";
      PreparedStatement pstmt = con.prepareStatement(SQL);
      pstmt.setString(1, AS400Role);
      pstmt.execute();
  }
  
}