package FTDI;

import com.ftd.osp.utilities.plugins.Logger;

import java.sql.*;

import com.ftdi.common.DBConnection;
import com.ftdi.common.DBConnectionException;
import com.ftdi.common.FtdiUser;
import com.ftdi.common.FtdiUtils;

/**
 * ValidateLogin - controls security of the application
 * 
 * History
 * 2003   Creation
 * 2005.04.27   ESO Added a super user in this application for testing purpose 
 */
public class ValidateLogin //implements Serializable 
{
  static ValidateLogin ValidateLoginManager = null;
  Logger logger = new Logger("FTDI.ValidateLogin");
  
  protected ValidateLogin()
  {
  }

  public static ValidateLogin getInstance()
  {
    if (ValidateLoginManager==null)
    {
      ValidateLoginManager = new ValidateLogin();
    }
    return ValidateLoginManager; 
  }

      private Connection conn = null;
      private Statement stmt = null;
      private ResultSet rs = null;
      
      /*public ValidateLogin()
      {
      }*/
  /**
   * AS400UserInfo is the prefer method to get user information.  
   * The other method required passing in database connection which is outdated.
   * 
   * @param UserID
   * @return ResultSet AS400PWD 
   */
  public FtdiUser AS400UserInfo(String UserID)
  {
      FtdiUser loginFtdiUser = null;
      String SQL = "SELECT Group_Profile, User_Name, DW_Password, dw_login_attempts, Status, Password_Change_Date, "
      + "round(Password_Change_Date - TRUNC(SYSDATE)) AS AS400Days, DW_Password_Change_Date, "
      + "round(DW_Password_Change_Date - TRUNC(SYSDATE)) AS DWDays, "
      + "USER_CLASS , GROUP_AUTHORITY, GROUP_PROFILE_INDICATOR, DW_STATUS, "
      + "R.DW_ROLE AS DW_ROLE , DW_LAST_LOGIN, "
      + "T.TERRITORY AS USERTERRITORY, RI.REGION AS USERREGION "
      + "FROM AS400PWD A, AS400_TO_DW_ROLES R, TERRITORY_INFO T, REGION_INFO RI "
      + "WHERE A.GROUP_PROFILE =  R.AS400_ROLE  "
      + "AND A.USER_PROFILE_NAME = T.FBC_USERID (+) "
      + "AND A.USER_PROFILE_NAME = RI.MANAGER_USERID (+) "
      + "AND A.USER_PROFILE_NAME =  '" + UserID + "'";
      rs = null;
      try
      {
          logger.debug("Calling getConnection()");
          conn = DBConnection.getInstance().getConnection();
          stmt = conn.createStatement();
          rs = stmt.executeQuery(SQL);
          if(rs != null && rs.next())
          {
            loginFtdiUser = new FtdiUser();
            loginFtdiUser.setUserID(FtdiUtils.notNull(UserID));
            loginFtdiUser.setUserClass(FtdiUtils.notNull(rs.getString("USER_CLASS")));
            loginFtdiUser.setGroupProfile(FtdiUtils.notNull(rs.getString("GROUP_PROFILE")));
            loginFtdiUser.setGroupAuthority(FtdiUtils.notNull(rs.getString("GROUP_AUTHORITY")));
            loginFtdiUser.setGroupProfileIndicator(FtdiUtils.notNull(rs.getString("GROUP_PROFILE_INDICATOR")));
            loginFtdiUser.setStatus(FtdiUtils.notNull(rs.getString("STATUS")));
            loginFtdiUser.setUserName(FtdiUtils.notNull(rs.getString("USER_NAME")));
            loginFtdiUser.setDWStatus(FtdiUtils.notNull(rs.getString("DW_STATUS")));
            loginFtdiUser.setDWRole(FtdiUtils.notNull(rs.getString("DW_ROLE")));
            loginFtdiUser.setDWLoginAttempts(rs.getInt("DW_LOGIN_ATTEMPTS"));
            loginFtdiUser.setDWLastLogin(rs.getDate("DW_LAST_LOGIN"));
            loginFtdiUser.setDWPasswordChangeDate(rs.getDate("DW_Password_Change_Date"));
            loginFtdiUser.setDWDays(rs.getInt("DWDAYS"));
            loginFtdiUser.setUserTerritory(FtdiUtils.notNull(rs.getString("USERTERRITORY")));
            loginFtdiUser.setUserRegion(FtdiUtils.notNull(rs.getString("USERREGION")));
            loginFtdiUser.setUserStoredPassword(rs.getBytes("DW_PASSWORD"));
          }
      }catch (DBConnectionException dbex){
          logger.error("ValidateLogin AS400UserInfo catch DBConnectionException->" + dbex.toString());
      }catch (Exception ex){
          logger.error("ValidateLogin AS400UserInfo catch Exception->" + ex.toString());
      }finally{
      //always close connection
        try{
          logger.debug("Calling close()");
          conn.close();
          stmt.close();
          rs.close();
        } catch (Exception ex)
        {
            logger.error("Error closing connection: " + ex);
        }
      }
    return loginFtdiUser;
  }

  public ResultSet AS400UserInfo(Connection con, String UserID) throws SQLException
  {
         String SQL = "SELECT Group_Profile, User_Name,DW_Password,dw_login_attempts,Status,Password_Change_Date,";
         SQL = SQL + "round(Password_Change_Date - TRUNC(SYSDATE)) AS AS400Days,DW_Password_Change_Date,";
         SQL = SQL + "round(DW_Password_Change_Date - TRUNC(SYSDATE)) AS DWDays ";
         SQL = SQL + "FROM AS400PWD ";
         SQL = SQL + "WHERE User_Profile_Name = '" + UserID + "'";

        Statement stmt =  null;
        ResultSet rs =  null;
         try{
         stmt = con.createStatement();
         rs = stmt.executeQuery(SQL);
         }catch (SQLException ex)
         {
           throw ex;
         }finally
         {
           con.close();
           if(stmt != null)
              stmt.close();
         }
         return(rs);
  }

  public void UpdateLoginAttempts(Connection con, String UserID) throws SQLException
  {
      String SQL = "UPDATE AS400PWD SET DW_LOGIN_ATTEMPTS =(DW_LOGIN_ATTEMPTS+1) WHERE User_Profile_Name = ?";
      
   		PreparedStatement pstmt = null;
      try{
      con.prepareStatement(SQL);
      pstmt.setString(1,UserID);
	   	pstmt.executeUpdate();
      } catch (SQLException ex)
      {
          throw ex;        
      }finally
      {
        con.close();
        if(pstmt != null)
          pstmt.close();
      }
  }
  
  public void UpdateLoginAttempts(String UserID) throws SQLException
  {
      String SQL = "UPDATE AS400PWD SET DW_LOGIN_ATTEMPTS =(DW_LOGIN_ATTEMPTS+1) WHERE User_Profile_Name = ?";
      
   		PreparedStatement pstmt = null;
      try
      {
          logger.debug("Calling getConnection()");
          conn = DBConnection.getInstance().getConnection();
          pstmt = conn.prepareStatement(SQL);
          pstmt.setString(1,UserID);
          pstmt.executeUpdate();  
      }catch (DBConnectionException dbex){
          logger.error("ValidateLogin UpdateLoginAttempts UserID="+UserID+" catch DBConnectionException->" + dbex.toString());
      }catch (Exception ex){
          logger.error("ValidateLogin UpdateLoginAttempts UserID="+UserID+" catch Exception->" + ex.toString());
      }finally{
      //always close connection
        try{
          logger.debug("Calling close()");
          conn.close();
          if(pstmt != null)
            pstmt.close();
        } catch (Exception ex)
        {
            logger.error("Error closing connection: " + ex);
        }
      }
  }
  public ResultSet DWRoleInfo(Connection con, String AS400Role) throws SQLException
  {
      String SQL = "SELECT DW_Role FROM AS400_To_DW_Roles WHERE AS400_Role = '" + AS400Role + "'";
      Statement stmt = null;
      ResultSet rs = null;
      try{
      stmt = con.createStatement();
      rs = stmt.executeQuery(SQL);
      }catch (SQLException ex)
      {
        throw ex;
      }finally
      {
        con.close();
        if (stmt != null)
          stmt.close();
      }
      return(rs);
  }

  public ResultSet TerritoryNumber(Connection con, String UserID) throws SQLException
  {
      String SQL = "SELECT Territory FROM Territory_Info WHERE FBC_UserID = '" +  UserID + "'";
      Statement stmt = null;
      ResultSet rs =  null;
      try{
      stmt = con.createStatement();
      rs = stmt.executeQuery(SQL);
      }catch (SQLException ex)
      {
        throw ex;
      }finally
      {
        con.close();
        if (stmt != null)
          stmt.close();
      }
      return(rs);
   }

   public ResultSet RegionNumber(Connection con, String UserID) throws SQLException
   {
      String SQL = "SELECT Region FROM Region_Info WHERE Manager_UserID = '" +  UserID + "'";
      Statement stmt = null;
      ResultSet rs =  null;
      try{
      stmt = con.createStatement();
      rs = stmt.executeQuery(SQL);
      }catch (SQLException ex)
      {
        throw ex;
      }finally
      {
        con.close();
        if (stmt != null)
          stmt.close();
      }
      return(rs);
   }

  public void InsertUserAudit(Connection con, String UserID) throws SQLException
  {
      String SQL = "INSERT INTO USER_LOG (user_id, session_id, logon_day, application) VALUES (?, NULL, SYSDATE, 'CANNED')";
   		PreparedStatement pstmt = null;
      try{
        pstmt = con.prepareStatement(SQL);
        pstmt.setString(1,UserID);
        pstmt.executeUpdate();
      }catch(SQLException ex)
      {
        throw ex;
      }finally
      {
        con.close();
        if(pstmt != null)
          pstmt.close();
      }
  }
  
  public void InsertUserAudit(String UserID) throws SQLException
  {
      String SQL = "INSERT INTO USER_LOG (user_id, session_id, logon_day, application) VALUES (?, NULL, SYSDATE, 'CANNED')";
   		
   		PreparedStatement pstmt = null;
      try
      {
          logger.debug("Calling getConnection()");
          conn = DBConnection.getInstance().getConnection();
          pstmt = conn.prepareStatement(SQL);
          pstmt.setString(1,UserID);
          pstmt.executeUpdate();  
      }catch (DBConnectionException dbex){
          logger.error("ValidateLogin InsertUserAudit UserID="+UserID+" catch DBConnectionException->" + dbex.toString());
      }catch (Exception ex){
          logger.error("ValidateLogin InsertUserAudit UserID="+UserID+" catch Exception->" + ex.toString());
      }finally{
      //always close connection
        try{
          logger.debug("Calling close()");
          conn.close();
          if(pstmt != null)
            pstmt.close();
        } catch (Exception ex)
        {
            logger.error("Error closing connection: " + ex);
        }
      }
  }
  
  
  
   /**
   * 
   * isSuperUser - this method check whether user is a super user 
   * 
   * @param sUserID
   * @return 
   * 2005.04.27 ESO   Added 
   */
   public boolean isSuperUser(String sUserID)
   {
     boolean bIsSuperUser = false;
     String SQL = " SELECT USER_PROFILE_NAME, DW_STATUS FROM AS400PWD " + 
                  " WHERE PASSWORD_EXPIRED = 'NO' " + 
                  " AND GROUP_PROFILE = '@SUPER' " + 
                  " AND STATUS = 'ENABLED' " + 
                  " AND DW_STATUS = 'SUPER' " + 
                  " AND USER_PROFILE_NAME = '" + sUserID +"' ";
      rs = null;
      try
      {
          logger.debug("Calling getConnection()");
          conn = DBConnection.getInstance().getConnection();
          stmt = conn.createStatement();
          rs = stmt.executeQuery(SQL);
          if(rs != null && rs.getFetchSize() > 0)
              bIsSuperUser = true;
      }catch (DBConnectionException dbex){
          logger.error("ValidateLogin: catch DBConnectionException->" + dbex.toString());
      }catch (Exception ex){
          logger.error("ValidateLogin: catch Exception->" + ex.toString());
      }finally{
      //always close connection
        try{
          logger.debug("Calling close()");
          conn.close();
          if(rs != null)
            rs.close();
        } catch (Exception ex)
        {
            logger.error("Error closing connection: " + ex);
        }
      }
     return bIsSuperUser ;
   }
}
