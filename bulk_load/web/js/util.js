/*
   Calls helper methods to limit navigation options.
*/
function setNavigationHandlers()
{
   document.oncontextmenu = contextMenuHandler;
   document.onkeydown = backKeyHandler;
}

/*
   Disables the right button mouse click.
*/
function contextMenuHandler()
{
   return false;
}

/*
   Disables the various navigation keyboard commands.
*/
function backKeyHandler()
{
   // backspace
   if (window.event && window.event.keyCode == 8){
      var formElement = false;
      
      for(i = 0; i < document.forms[0].elements.length; i++){
         if(document.forms[0].elements[i].name == document.activeElement.name){
            formElement = true;
            break;
         }
      }
      
      if(formElement == false || (document.activeElement.type != "text" && document.activeElement.type != "textarea" && document.activeElement.type != "password")){
         window.event.cancelBubble = true;
         window.event.returnValue = false;
         return false;
      }
   }

   // F5 and F11
   if (window.event.keyCode == 122 || window.event.keyCode == 116){
      window.event.keyCode = 0;
      event.returnValue = false;
      return false;
   }

   // Alt + Left Arrow
   if(window.event.altiLeft){
      window.event.returnValue = false;
      return false;
   }

   // Alt + Right Arrow
   if(window.event.altKey){
      window.event.returnValue = false;
      return false;
   }     
}

/*
   Returns the style object for the given element's field name.
*/
function getStyleObject(fieldName)
{
    if (document.getElementById && document.getElementById(fieldName))
        return document.getElementById(fieldName).style;
}

/*
   Limits input to digits only.      
*/
function digitOnlyListener()
{
    var input = event.keyCode;
    if (input < 48 || input > 57){
        event.returnValue = false;
    }
}

/*
   The following function changes the control's style sheet to 'errorField'.
   elements - an Array of control names on the form
*/
function setErrorFields(){
    for (var i = 0; i < errorFields.length; i++){
        var element = document.getElementById(errorFields[i]);
        if (element != null){
            element.className = "errorField";
        }
    }
}

/*
    The following functions attached the 'onfocus' and 'onblur' events the the input
    elements.  When a control on the page gains focus, the control's border changes to red.
    When focus is lost, the control's border changes back to default.

    elements - an Array of control names on the form
    element - a control name on the form
*/
function addDefaultListeners(elements)
{
    var element;
    for (var i = 0; i < elements.length; i++){
        element = document.getElementById(elements[i]);
        if (element != null){
           element.attachEvent("onfocus", fieldFocus);
           element.attachEvent("onblur", fieldBlur);
        }
    }
}
function fieldFocus()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = 'red';
}

function fieldBlur()
{
    var element = event.srcElement;
    element.style.borderWidth = 2;
    element.style.borderColor = document.body.style.backgroundColor;
}


/*
    The following functions change the cursor UI when an image triggers a 'mouseover' event.

    elements - an Array of control names on the form
*/
function addImageCursorListener(elements)
{
    for (var i = 0; i < elements.length; i++){
        document.getElementById(elements[i]).attachEvent("onmouseover", imageOver);
        document.getElementById(elements[i]).attachEvent("onmouseout", imageOut);
    }
}
function imageOver()
{
    document.forms[0].style.cursor = "hand";
}

function imageOut()
{
    document.forms[0].style.cursor = "default";
}


/*
  The following methods are used to display a wait message to the user
  while an action is being executed.  The following coding convention should
  be used for the wait div's table structure:

  waitDiv - id of the wait div
  waitMessage - id of the TD which will contain the message
  waitTD - id of the TD which will contain the dots (...)

  Parameters
  content - The div id containing the content to be hidden
  wait - The div id containing the wait message
  message - Optional, if provided the message to be displayed, otherwise, "Processing"
            will be the message
*/
function showWaitMessage(content, wait, message){
   var content = document.getElementById(content);
   var height = content.offsetHeight;
   var waitDiv = document.getElementById(wait + "Div").style;
   var waitMessage = document.getElementById(wait + "Message");
   _waitTD = document.getElementById(wait + "TD");

   content.style.display = "none";
   waitDiv.display = "block";
   waitDiv.height = height;
   waitMessage.innerHTML = (message) ? message : "Processing";
   clearWaitMessage();
   updateWaitMessage();
}

var _waitTD = "";
function clearWaitMessage(){
   _waitTD.innerHTML = "";
   setTimeout("clearWaitMessage()", 5000);
}
function updateWaitMessage(){
   _waitTD.innerHTML += " . ";
   setTimeout("updateWaitMessage()", 1000);
}