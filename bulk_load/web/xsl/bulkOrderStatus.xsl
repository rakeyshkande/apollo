<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:param name="applicationcontext"/>
<xsl:param name="securitytoken"/>
<xsl:param name="adminAction"/>
<xsl:param name="exitpage"/>
<xsl:param name="context"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Bulk Order Status</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script language="javascript" type="text/javascript">

      
      
      var context_js = '<xsl:value-of select="$applicationcontext"/>';
      var securityParam = '<xsl:value-of select="$securitytoken"/>';
      var adminAction = '<xsl:value-of select="$adminAction"/>';
      var securityContext = '<xsl:value-of select="$context"/>';
      var exitpage = '<xsl:value-of select="$exitpage"/>';<![CDATA[



        function openBulkOrderDetail(masterOrderNumber){
            document.forms[0].action = "\ViewBulkOrderStatusDetailServlet?masterordernumber=" + masterOrderNumber;
            document.forms[0].submit();
        }

      
      function doNew(){

        performAction("\SetupBulkOrder?securitytoken=" + securityParam + "&context=" + securityContext + "&applicationcontext="+context_js+"&adminAction" + adminAction);
        //document.forms[0].action = "\SetupBulkOrder?securitytoken=" + securityParam + "&context=" + securityContext + "&applicationcontext="+context_js+"&adminAction" + adminAction;
        //document.forms[0].submit();
		}      
      
      function doExitAction(){
        var url = exitpage +
                  "?securitytoken=" + securityParam +
                  "&context=" + securityContext +
                  "&applicationcontext=" + context_js + 
                  "&adminAction=" + adminAction;

        performAction(url);
      }
      
      function performAction(url){
        //showWaitMessage("content", "wait")
        //document.forms[0].action = url;
        //document.forms[0].submit();
	document.location = url;      
      }      

    ]]>
    </script>
</head>
<body>
    <form name="BulkOrderStatusForm" method="get">
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>   
    <input type="hidden" name="adminAction" value="{$adminAction}"/>       
    <xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Bulk Orders Status'"/>
	<xsl:with-param name="showTime" select="'true'"/>
	<xsl:with-param name="showExitButton" select="'true'"/>
    </xsl:call-template>
<table width="98%" border="0" align="center" cellpadding="1" cellspacing="1">
    <tr>
        <td width="12%">
            <a href="#" onclick="doNew();">New Bulk Order</a>
        </td>
    </tr>
</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="12%" class="colHeaderCenter">Processed Date</td>
                        <td width="12%" class="colHeaderCenter">Master Order Number</td>
                        <td width="10%" class="colHeaderCenter">Source Code</td>
                        <td width="18%" class="colHeaderCenter">Billing Name</td>
                        <td width="18%" class="colHeaderCenter">Business Name</td>
                        <td width="5%" class="colHeaderCenter">Submitted Orders</td>
                        <td width="5%" class="colHeaderCenter">Processed Orders</td>
                        <td width="5%" class="colHeaderCenter">Scrub Orders</td>
                        <td width="5%" class="colHeaderCenter">Pending\Removed Orders</td>
                        <td width="15%" class="colHeaderCenter">Status</td>
                    </tr>
                    <xsl:for-each select="orders/order">
                    <tr>
                        <td width="12%" class="tblDataCenterSmall"><xsl:value-of select="@order_date"/></td>
                        <td width="12%" class="tblDataCenterSmall"><xsl:value-of select="@master_order_number"/></td>
                        <td width="10%" class="tblDataCenterSmall"><xsl:value-of select="@source_code"/></td>
                        <td width="18%" class="tblDataCenterSmall"><xsl:value-of select="@first_name"/>&nbsp;<xsl:value-of select="@last_name"/></td>
                        <td width="18%" class="tblDataCenterSmall"><xsl:value-of select="@address_etc"/></td>
                        <td width="5%" class="tblDataCenterSmall"><xsl:value-of select="@submitted_lines_total"/></td>
                        <td width="5%" class="tblDataCenterSmall"><xsl:value-of select="@process_total"/></td>
                        <td width="5%" class="tblDataCenterSmall"><xsl:value-of select="@scrub_total"/></td>
                        <td width="5%" class="tblDataCenterSmall"><xsl:value-of select="@remove_pending_total"/></td>
                        <td width="15%" class="tblDataCenterSmall"><xsl:value-of select="@status"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </td>
        </tr>
    </table>
  <!-- Buttons -->
  <table width="98%" border="0" cellpadding="2" cellspacing="2">
    <tr>
      <td width="25%" class="requiredFieldTxt">
      </td>
      <td width="50%" align="center">
      </td>
      <td width="25%" align="right">
        <input id="exitButton" type="button" name="exit" value="Exit" onclick="javascript:doExitAction();"/>
      </td>
    </tr>
  </table>
	<xsl:call-template name="footer"/>
    </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>