<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:param name="securitytoken"/>
<xsl:param name="exitpage"/>
<xsl:template match="/ROOT">

<html>
<head>
    <title>FTD - Bulk Order Status Detail</title>
    <link rel="stylesheet" type="text/css" href="../css/ftd.css"/>
    <script language="javascript" type="text/javascript">
    <![CDATA[

    ]]>
    </script>
</head>
<body>
    <form name="BulkOrderStatusDetailForm" method="get">
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>   
    <xsl:call-template name="header">
        <xsl:with-param name="headerName" select="'Bulk Orders Status Detail'"/>
    </xsl:call-template>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="12%" class="colHeaderCenter">Order Number</td>
                        <td width="10%" class="colHeaderCenter">Item Number</td>
                        <td width="18%" class="colHeaderCenter">Recipient</td>
                        <td width="10%" class="colHeaderCenter">Delivery Date</td>
                        <td width="25%" class="colHeaderCenter">Street Address</td>
                        <td width="10%" class="colHeaderCenter">Status</td>
                    </tr>
                    <xsl:for-each select="orders/order">
                    <tr>
                        <td width="12%" class="tblDataCenterSmall"><xsl:value-of select="@orderid"/></td>
                        <td width="10%" class="tblDataCenterSmall"><xsl:value-of select="@productid"/></td>
                        <td width="18%" class="tblDataCenterSmall"><xsl:value-of select="@name"/></td>
                        <td width="10%" class="tblDataCenterSmall"><xsl:value-of select="@deliverydate"/></td>
                        <td width="25%" class="tblDataCenterSmall"><xsl:value-of select="@address"/></td>
                        <td width="10%" class="tblDataCenterSmall"><xsl:value-of select="@status"/></td>
                    </tr>
                    </xsl:for-each>
                </table>
            </td>
        </tr>
    </table>
	<xsl:call-template name="footer"/>
    </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>