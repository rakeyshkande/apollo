<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>
<xsl:param name="applicationcontext"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Source Code Lookup</title>
    <script language="JavaScript" src="/{$applicationcontext}/js/FormChek.js"/>
    <script language="JavaScript" src="/{$applicationcontext}/js/util.js"/>
    <script language="JavaScript">
    <![CDATA[
        var fieldNames = new Array("sourceCodeInput");
        var images = new Array("sourceCodeSearch", "sourceCodeCloseTop", "sourceCodeCloseBottom", "sourceCodeSelectNone");

        function init()
        {
            addDefaultListeners(fieldNames);
            addImageCursorListener(images);
            window.name = "VIEW_SOURCE_CODE_LOOKUP";
            document.forms[0].sourceCodeInput.focus();
        }

        function onKeyDown()
        {
            if (window.event.keyCode == 13)
                reopenPopup();
        }

        function enterReOpenSourceCodePopup()
        {
            if (window.event.keyCode == 13)
                reopenPopup();
        }

        function reopenPopup()
        {
            var form = document.forms[0];

            //First validate the Source Code input
            var sourceCode = document.forms[0].sourceCodeInput.value;
            var sourceCode = stripWhitespace(sourceCode);

            if(sourceCode == "" || sourceCode.length < 2)
            {
               form.sourceCodeInput.focus();
               form.sourceCodeInput.style.backgroundColor = 'pink';
               alert("Please correct the marked fields");
            }
            else 
            {
                form.action = "";
                form.method = "get";
                form.target = "VIEW_SOURCE_CODE_LOOKUP";
                form.submit();
            }
        }

        function closeSourceLookup(sourceCode)
        {
            if (window.event.keyCode == 13)
                populatePage(sourceCode);
        }

        function populatePage(sourceCode)
        {
            window.returnValue = sourceCode;
            window.close();
        }
    ]]>
    </script>
    <link rel="STYLESHEET" type="text/css" href="/{$applicationcontext}/css/ftd.css"></link>
</head>

<body onLoad="javascript:window.focus(); init();">
<form name="form" method="get" action="">
<input type="hidden" name="POPUP_ID" value="LOOKUP_SOURCE_CODE"/>
<center>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td align="center" class="header">Source Code Lookup</td>
        </tr>
        <tr>
            <td nowrap="true" colspan="3" align="left">
                <span class="instruction">Enter Source Code or Description value</span>&nbsp;
                <input type="text" name="sourceCodeInput" tabindex="1" size="20" maxlength="50" value="" onkeypress="javascript:enterReOpenSourceCodePopup();"/>
                <span class="instruction"> and press </span>
                <img id="sourceCodeSearch" tabindex="2" onkeydown="javascript:onKeyDown();" src="/{$applicationcontext}/images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()"/>
            </td>
        </tr>
        <tr>
            <td><hr/></td>
        </tr>
    </table>
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td align="right">
                <img id="sourceCodeCloseTop" tabindex="3" onkeydown="javascript:closeSourceLookup('')" src="/{$applicationcontext}/images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')"/>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="1" class="LookupTable" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="5%" class="label">&nbsp;</td>
                        <td class="label" valign="bottom">Name/Description</td>
                        <td class="label" valign="bottom">Offer</td>
                        <td class="label" align="center" valign="bottom">Service<br/>Charge</td>
                        <td class="label" align="center" valign="bottom">Expiration<br/>Date</td>
                        <td class="label" align="center" valign="bottom">Order<br/>Source</td>
                        <td class="label" align="center" valign="bottom">Source<br/>Code</td>
                        <td>
                            <xsl:for-each select="searchResults/searchResult">
                            <tr>
                                <td>
                                    <xsl:choose>
                                       <xsl:when test="@expiredflag='Y'">
                                            <img alt="disabled" src="/{$applicationcontext}/images/selectButtonRight_disabled.gif"/>
                                        </xsl:when>
                                        <xsl:when test="@expiredflag='N'">
                                            <img onclick="javascript:populatePage('{@sourcecode}')" onkeydown="javascript:closeSourceLookup('{@sourceCode}')" tabindex="4" src="../images/selectButtonRight.gif"/>
                                        </xsl:when>
                                    </xsl:choose>
                                </td>
                                <td align="left"><xsl:value-of disable-output-escaping="yes" select="@sourcedescription"/></td>
                                <td align="left"><xsl:value-of select="@offerdescription"/></td>
                                <td align="center"><xsl:value-of select="@servicecharge"/></td>
                                <td align="right"><xsl:value-of select="@expirationdate"/></td>
                                <td align="center"><xsl:value-of select="@ordersource"/></td>
                                <td align="center"><xsl:value-of select="@sourcecode"/></td>
                            </tr>
                            </xsl:for-each>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" valign="center">
                            <img id="sourceCodeSelectNone" tabindex="4" onkeydown="javascript:closeSourceLookup('')" src="/{$applicationcontext}/images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('');"/>
                        </td>
                        <td colspan="7" valign="top">None of the above</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <img id="sourceCodeCloseBottom" tabindex="5" onkeydown="javascript:closeSourceLookup('')" src="/{$applicationcontext}/images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')"/>
            </td>
        </tr>
    </table>
</center>
</form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>