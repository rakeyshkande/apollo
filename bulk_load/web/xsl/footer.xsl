<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="footer">

	<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
	    <tr><td>&nbsp;</td></tr>
	    <tr>
	        <td class="disclaimer">
                <div align="center">
					COPYRIGHT
					<script>
					<![CDATA[
    					document.write("&#174;");
					]]>
					</script>
                    2003. FTD INC. ALL RIGHTS RESERVED.
                </div>
	        </td>
	    </tr>
	</table>

</xsl:template>
</xsl:stylesheet>