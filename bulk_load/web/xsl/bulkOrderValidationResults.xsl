<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:output method="html" indent="yes"/>
<xsl:param name="masterOrderNumber"/>
<xsl:param name="applicationcontext"/>
<xsl:param name="securitytoken"/>
<xsl:param name="adminAction"/>
<xsl:param name="exitpage"/>
<xsl:param name="context"/>
<xsl:template match="/root">


<html>
<head>
  <title>FTD - Bulk Order Load Results</title>
  <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/css/ftd.css"/>
  <script type="text/javascript" src="/{$applicationcontext}/js/util.js"></script>
  <script type="text/javascript" language="JavaScript">
  /*
   *  Global Variables
   */
    var valid = <xsl:choose>
                    <xsl:when test="SUMMARIES/SUMMARY[@STATUS='VALID']">true;</xsl:when>
                    <xsl:otherwise>false;</xsl:otherwise>
                </xsl:choose>
    var warn = <xsl:choose>
                    <xsl:when test="SUMMARIES/SUMMARY[@STATUS='WARN']">true;</xsl:when>
                    <xsl:otherwise>false;</xsl:otherwise>
                </xsl:choose>
    var error = <xsl:choose>
                    <xsl:when test="SUMMARIES/SUMMARY[@STATUS='ERROR']">true;</xsl:when>
                    <xsl:otherwise>false;</xsl:otherwise>
                </xsl:choose>

    var context_js = '<xsl:value-of select="$applicationcontext"/>';
    var securityParam = '<xsl:value-of select="$securitytoken"/>';
    var adminAction = '<xsl:value-of select="$adminAction"/>';
    var securityContext = '<xsl:value-of select="$context"/>';
    var exitpage = '<xsl:value-of select="$exitpage"/>';<![CDATA[

  /*
   *  Initialization
   */
    function init(){
      setNavigationHandlers();
    }

  /*
   *  Actions
   */
    function doExitAction(){
      var url = exitpage +
                "?securitytoken=" + securityParam +
                "&context=" + securityContext +
                "&applicationcontext=" + context_js +
                "&adminAction" + adminAction;

      performAction(url);
    }

    function openBulkOrderStatus(){
      var url = "/" + context_js + "/bulkload/ViewBulkOrderStatus" +
                "?securitytoken=" + securityParam +
                "&context=" + securityContext +
                "&applicationcontext=" + context_js +
                "&adminAction" + adminAction;

      performAction(url);
    }
    
    function processWarnOrder(){
      var url = "/" + context_js + "/bulkload/ProcessBulkOrderWarn" +
                "?securitytoken=" + securityParam +
                "&context=" + securityContext +
                "&applicationcontext=" + context_js +
                "&adminAction" + adminAction;

      performAction(url);
    }
    
    function setupBulkOrder(){
      var url = "/" + context_js + "/bulkload/SetupBulkOrder" +
                "?securitytoken=" + securityParam +
                "&context=" + securityContext +
                "&applicationcontext=" + context_js +
                "&adminAction" + adminAction;

      performAction(url);
    }

      function doStatus(){
        document.forms[0].action = "\ViewBulkOrderStatusServlet?securitytoken=" + securityParam + "&context=" + securityContext + "&applicationcontext="+context_js+"&adminAction" + adminAction;
        document.forms[0].submit();
		}      

    function performAction(url){
      showWaitMessage("content", "wait")
      document.forms[0].action = url;
      document.forms[0].submit();
    }]]>
  </script>
</head>

<body onload="javascript:init();">

  <!-- Header template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Bulk Order Vaildation Results'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <!-- Content div needed to hold any content that will be blocked when an action is performed -->
  <div id="content" style="display:block">

    <form name="BulkOrderValidationResultsForm" method="post" action="">
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="adminAction" value="{$adminAction}"/>    
    <input type="hidden" name="guid" value="{FIELDS/FIELD[@fieldname='guid']/@value}"/>


  <script type="text/javascript" language="javascript"><![CDATA[
  if (!error && !warn){
    document.write('<table width="98%" border="0" align="center" cellpadding="1" cellspacing="1"><tr><td width="12%"><a href="#" onclick="doStatus();">Bulk Order Status</a></td></tr></table>');
  } ]]>
  </script>





    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblHeader" colspan="3">
                Validation Results
                <xsl:if test="$masterOrderNumber!=''">
                &nbsp;for Master Order&nbsp;<xsl:value-of select="$masterOrderNumber"/>
                </xsl:if>
              </td>
            </tr>
            <tr>
              <td width="15%" class="colHeaderCenter">Record Count</td>
              <td width="15%" class="colHeaderCenter">Status</td>
              <td width="80%" class="colHeader">Reason</td>
            </tr>
            <xsl:for-each select="SUMMARIES/SUMMARY">
             	<xsl:sort select="@STATUS" />
              <tr>
                <td width="15%" class="tblDataCenter"><xsl:value-of select="@COUNT"/></td>
                <xsl:choose>
                  <xsl:when test="@STATUS='VALID'">
                    <td width="15%" class="tblDataCenter" style="color:green;">Valid</td>
                    <td width="80%" class="tblDataLeft"><xsl:value-of select="@MESSAGE"/></td>
                  </xsl:when>
                  <xsl:when test="@STATUS='ERROR'">
                    <td width="15%" class="tblDataCenter" style="color:red;">Error</td>
                    <td width="80%" class="tblDataLeft"><xsl:value-of select="@MESSAGE"/></td>
                  </xsl:when>
                  <xsl:when test="@STATUS='WARN'">
                    <td width="15%" class="tblDataCenter" style="color:orange;">Warn</td>
                    <td width="80%" class="tblDataLeft"><xsl:value-of select="@MESSAGE"/></td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td width="15%" class="tblDataCenter"><xsl:value-of select="@STATUS"/></td>
                    <td width="80%" class="tblDataLeft"><xsl:value-of select="@MESSAGE"/></td>
                  </xsl:otherwise>
                </xsl:choose>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>
    </table>

    <!-- Action Buttons -->
    <table width="98%" border="0" cellpadding="0" cellspacing="0">
      <tr><td>&nbsp;</td></tr>
      <tr>
        <td align="center">
          <script type="text/javascript" language="javascript"><![CDATA[
          if (error){
            document.write('<input type="button" name="reprocessButton" value="Reprocess Bulk Order" onclick="javascript:setupBulkOrder();"/>&nbsp;&nbsp;');
          } else {
            if (warn){
              document.write('<input type="button" name="reprocessButton" value="Reprocess Bulk Order" onclick="javascript:setupBulkOrder();"/>&nbsp;&nbsp;');
              document.write('<input type="button" name="warnButton" value="Submit" onclick="javascript:processWarnOrder();"/>&nbsp;&nbsp;');
            } else{
              document.write('<input type="button" name="newButton" value="OK" onclick="javascript:setupBulkOrder();"/>&nbsp;&nbsp;');
              //document.write('<input type="button" name="statusButton" value="Bulk Order Status" onclick="javascript:openBulkOrderStatus();"/>');
            }
          }]]>
          </script>
        </td>
      </tr>
    </table>

    </form>
  </div>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>