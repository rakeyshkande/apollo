<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="sourceCodeLookupDiv">

    <div id="sourceCodeLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
        <tr>
            <td colspan="2" class="popupHeader">Source Code Lookup</td>
        </tr>
        <tr>
            <td class="label" nowrap="true">Source Code or Description:</td>
            <td><input type="text" name="sourceCodeInput" tabindex="98" onkeydown="javascript:EnterToSourcePopup()" size="30" maxlength="50" onFocus="javascript:fieldFocus('sourceCodeInput');" onblur="javascript:fieldBlur('sourceCodeInput');"/></td>
        </tr>
        <tr>
            <td align="right" colspan="2">
                <img id="sourceCodeSearch" tabindex="99" onkeydown="javascript:EnterToSourcePopup();" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchSourceCode();"/>
                <img id="sourceCodeClose" tabindex="100" onkeydown="javascript:CloseSourceLookup();" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelSourceCode();"/>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
    </table>
    </div>

    <script language="javascript">
    <![CDATA[

        function openSourceCodeLookup()
        {
          var form = document.forms[0];

          // Close florist lookup div
          document.getElementById("floristLookup").style.visibility = "hidden";

          // In case the DIV is opened a second time
          form.sourceCodeInput.style.backgroundColor='white';

          // Set the div's bounding properties
          boundingRect = form.sourceCode.getBoundingClientRect();
          sourcecodesearch = document.getElementById("sourceCodeLookup").style;
          sourcecodesearch.top = document.body.scrollTop + boundingRect.bottom;
          sourcecodesearch.left = document.body.scrollLeft + boundingRect.right + 5;
          sourcecodesearch.width = 290;
          sourcecodesearch.height = 100;
          sourcecodesearch.visibility = "visible";

          //Populate the source code input with the source code on the page and set the focus
          form.sourceCodeInput.value = form.sourceCode.value;
          form.sourceCodeInput.focus();
        }

        function EnterToSourcePopup()
        {
          if (window.event.keyCode == 13)
            openSourceCodePopup();
        }

        function goSearchSourceCode()
        {
          var form = document.forms[0];

          //First validate the Source Code input
          var check = true;
          var sourceCode = form.sourceCodeInput.value;
          sourceCode = stripWhitespace(sourceCode);

          if(sourceCode == "" || sourceCode.length < 2)
          {
            form.sourceCodeInput.focus();
            form.sourceCodeInput.style.backgroundColor = "pink";
            check = false;
            alert("Please correct the marked fields");
          }

          if (check)
            openSourceCodePopup();
        }

        function openSourceCodePopup()
        {
          var form = document.forms[0];
          var val = form.sourceCodeInput.value;
          var url_source = "PopupServlet?POPUP_ID=LOOKUP_SOURCE_CODE&sourceCodeInput=" + val;
          var modal_dim = "dialogWidth:800px; dialogHeight:650px; center:yes; status=0";
          var ret = window.showModalDialog(url_source,"", modal_dim);

          goCancelSourceCode();

          if (ret && ret != ''){
            form.sourceCode.value = ret;
            doSourceCodeAction();
          }
        }

        function CloseSourceLookup()
        {
          if (window.event.keyCode == 13)
            goCancelSourceCode();
        }

        function goCancelSourceCode()
        {
          sourcecodesearch.visibility = "hidden";
        }
    ]]>
    </script>

</xsl:template>
</xsl:stylesheet>