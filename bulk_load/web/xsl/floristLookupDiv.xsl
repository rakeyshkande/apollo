<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="floristLookupDiv">

    <div id="floristLookup" style="VISIBILITY: hidden; POSITION: absolute; TOP: 225px; border: 1px solid Black; border-color: Black; background: #FFF8DC;">
        <table width="98%" border="0" cellspacing="0" cellpadding="2">
            <tr>
                <td width="100%" valign="top" class="popupHeader">Florist Lookup</td>
            </tr>
            <tr>
                <td width="100%">
                    <table  width="100%" border="0" cellpadding="2" cellspacing="2">
                        <tr>
                            <td>
                                <table width="98%" border="0" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td width="25%" class="labelright">Name:</td>
                                        <td width="75%"><input type="text" name="floristBusinessInput" onkeydown="javascript:EnterToFloristPopup()" tabindex ="49" class="TblText" size="30" maxlength="75"/></td>
                                    </tr>
                                    <tr>
                                        <td class="labelright">Phone:</td>
                                        <td><input type="text" name="floristPhoneInput" onkeydown="javascript:EnterToFloristPopup()" tabindex ="50" class="TblText" size="20" maxlength="20"/></td>
                                    </tr>
                                    <tr>
                                        <td class="labelright">Address:</td>
                                        <td><input type="text" name="floristAddressInput" onkeydown="javascript:EnterToFloristPopup()" tabindex ="51" class="TblText" size="20" maxlength="75"/></td>
                                    </tr>
                                    <tr>
                                        <td class="labelright">City:</td>
                                        <td><input type="text" name="floristCityInput" onkeydown="javascript:EnterToFloristPopup()" tabindex ="52" class="TblText" maxlength="50" size="20"/></td>
                                    </tr>
                                        <td class="labelright">State:</td>
                                        <td>
                                            <select name="floristStateInput" tabindex="53">
                                                <option value=""> </option>
                                                <xsl:for-each select="/root/STATES/STATE">
                                                    <option value="{@statemasterid}"><xsl:value-of select="@statename"/></option>
                                                </xsl:for-each>
                                            </select>
                                        </td>
                                    <tr>
                                        <td nowrap="true" class="labelright">Zip/Postal Code:</td>
                                        <td><input type="text" name="floristZipInput" onkeydown="javascript:EnterToFloristPopup()" tabindex ="54" class="TblText" maxlength="6" size="6" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right">
                                            <img id="floristSearch" onkeydown="javascript:EnterToFloristPopup()" tabindex ="55" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:goSearchFlorist()"/>
                                            <img id="floristClose" onkeydown="javascript:closeFloristLookup()" tabindex ="56" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:goCancelFlorist()"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <script language="javascript">
    <![CDATA[

        function openFloristLookup()
        {
            var form = document.forms[0];

            // Close souce code lookup div
            document.getElementById("sourceCodeLookup").style.visibility = "hidden";
        
            //In case the DIV is opened a second time
            form.floristBusinessInput.style.backgroundColor='white';
            form.floristCityInput.style.backgroundColor='white';
            form.floristStateInput.style.backgroundColor='white';
            form.floristZipInput.style.backgroundColor='white';
            form.floristPhoneInput.style.backgroundColor='white';

            // Set the div's bounding properties
            boundingRect = form.floristCode.getBoundingClientRect();
            floristsearch = document.getElementById("floristLookup").style;
            floristsearch.height = 100;
            floristsearch.width = 350;
            floristsearch.top = document.body.scrollTop + boundingRect.bottom;
            floristsearch.left = document.body.scrollLeft + boundingRect.right - 270;
            floristsearch.visibility = "visible";

            form.floristBusinessInput.focus();
        }

        function EnterToFloristPopup()
        {
            if (window.event.keyCode == 13)
                openFloristPopup();
        }

    	function goSearchFlorist()
    	{
    		openFloristPopup();
    	}

        function openFloristPopup()
        {
            var form = document.forms[0];
        	//First validate that the inputs are valid
            check = true;
       		business = stripWhitespace(form.floristBusinessInput.value);
            city = stripWhitespace(form.floristCityInput.value);
            state = stripWhitespace(form.floristStateInput.value);
            zip = stripWhitespace(form.floristZipInput.value);
            phone = stripWhitespace(form.floristPhoneInput.value);

            //state is required if business is given
            if ((business.length > 0) ]]> &amp; <![CDATA[(state.length == 0))
            {
                if (check == true)
                {
                    form.floristStateInput.focus();
                    check = false;
                }
                form.floristStateInput.style.backgroundColor='pink';
            }

            //city is required if no zip and phone are given
            if ((city.length == 0) ]]>&amp; <![CDATA[(zip.length == 0)]]> &amp; <![CDATA[(phone.length == 0))
            {
                if (check == true)
                {
                    form.floristCityInput.focus();
                    check = false;
                }
                form.floristCityInput.style.backgroundColor='pink';
            }

            //zip is required if no city, state, and phone are given
            if ((zip.length == 0) ]]>&amp; <![CDATA[(city.length == 0) ]]>&amp; <![CDATA[(state.length == 0) ]]>&amp; <![CDATA[(phone.length == 0))
            {
                if (check == true)
                {
                    form.floristZipInput.focus();
                    check = false;
                }
                form.floristZipInput.style.backgroundColor='pink';
            }

            if (!check)
            {
                alert("Please correct the marked fields")
                return false;
            }

            //Now that everything is valid, open the popup
    	    var url_source="PopupServlet?POPUP_ID=LOOKUP_FLORIST" +
    	    "&institutionInput=" + form.floristBusinessInput.value +
    	    "&phoneInput=" + form.floristPhoneInput.value +
            "&addressInput=" + form.floristAddressInput.value +
            "&cityInput=" + form.floristCityInput.value +
            "&stateInput=" + form.floristStateInput.value +
            "&zipCodeInput=" + form.floristZipInput.value +
            "&countryInput=" + form.billingCountry[form.billingCountry.selectedIndex].value;

    	    //Open the popup
    	    var modal_dim = "dialogWidth:800px; dialogHeight:500px; center:yes; status=0";
    	    var ret = window.showModalDialog(url_source,"", modal_dim);

    		//in case the X icon is clicked
    		if (!ret)
    			ret = '';

    		if (ret != '')
    			form.floristCode.value = ret;

    		goCancelFlorist();
    	}

        function closeFloristLookup()
        {
            if (window.event.keyCode == 13)
                goCancelFlorist();
        }

    	function goCancelFlorist()
    	{
    		floristsearch.visibility = "hidden";
    	}

    ]]>
    </script>

</xsl:template>
</xsl:stylesheet>