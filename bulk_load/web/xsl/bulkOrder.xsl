<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;"> 
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="header.xsl"/>
<xsl:import href="footer.xsl"/>
<xsl:import href="sourceCodeLookupDiv.xsl"/>
<xsl:import href="floristLookupDiv.xsl"/>
<xsl:output method="html" indent="yes"/>

<xsl:key name="field" match="root/FIELDS/FIELD" use="@fieldname"/>
<xsl:key name="error" match="root/ERRORS/ERROR" use="@fieldname"/>
<xsl:variable name="selectedPaymentType" select="key('field', 'paymentType')/@value"/>
<xsl:variable name="selectedBillingState" select="key('field', 'billingState')/@value"/>
<xsl:variable name="selectedBillingCountry" select="key('field', 'billingCountry')/@value"/>
<xsl:param name="applicationcontext"/>
<xsl:param name="context"/>
<xsl:param name="securitytoken"/>
<xsl:param name="adminAction"/>
<xsl:param name="exitpage"/>
<xsl:template match="/root">

<html>
<head>
  <title>FTD - Bulk Order</title>
  <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/css/ftd.css"/>
  <script type="text/javascript" src="/{$applicationcontext}/js/FormChek.js"/>
  <script type="text/javascript" src="/{$applicationcontext}/js/util.js"/>
  <script type="text/javascript" language="JavaScript">
  /*
   *  Global Variables
   */
      var errorFields = new Array(  <xsl:for-each select="ERRORS/ERROR">
                                      <xsl:choose>
                                        <xsl:when test="position()=last()">"<xsl:value-of select="@fieldname"/>"</xsl:when>
                                        <xsl:otherwise>"<xsl:value-of select="@fieldname"/>", </xsl:otherwise>
                                      </xsl:choose>
                                    </xsl:for-each>);

      var usStates = new Array( <xsl:for-each select="STATES/STATE[@countrycode='']">
                                  ["<xsl:value-of select="@statemasterid"/>", "<xsl:value-of select="@statename"/>"]
                                  <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                                </xsl:for-each>);
      var caStates = new Array( <xsl:for-each select="STATES/STATE[@countrycode='CAN']">
                                  ["<xsl:value-of select="@statemasterid"/>", "<xsl:value-of select="@statename"/>"]
                                  <xsl:choose><xsl:when test="position()!=last()">,</xsl:when></xsl:choose>
                                </xsl:for-each>);
      var naStates = new Array(["N/A", "NA"]);

      var selectedState = '<xsl:value-of select="key('field', 'billingState')/@value"/>';

      <xsl:choose>
        <xsl:when test="$selectedPaymentType='IN' or $selectedPaymentType='' or not(boolean($selectedPaymentType))">
          var paymentDisyplayStyle = "none";
        </xsl:when>
        <xsl:otherwise>
          var paymentDisyplayStyle = "block";
        </xsl:otherwise>
      </xsl:choose>

      var ccExpMonth = <xsl:value-of select="FIELDS/FIELD[@fieldname='creditCardMonth']/@value"/>+0;
      var ccExpYear  = <xsl:value-of select="FIELDS/FIELD[@fieldname='creditCardYear']/@value"/>+0;
      var context_js = '<xsl:value-of select="$applicationcontext"/>';
      var securityParam = '<xsl:value-of select="$securitytoken"/>';
      var adminAction = '<xsl:value-of select="$adminAction"/>';
      var securityContext = '<xsl:value-of select="$context"/>';
      var exitpage = '<xsl:value-of select="$exitpage"/>';<![CDATA[

      var fieldNames = new Array(
          "sourceCode", "floristCode", "customerFirstname", "customerLastname",
          "customerPhone", "customerPhoneExt", "customerEmail", "billingFirstname",
          "billingLastname", "businessName", "billingEmail", "billingAddressLine1",
          "billingPhone", "billingPhoneExt", "billingAddressLine2", "billingState", 
          "billingCity", "billingCountry", "billingZip", "paymentType",
          "creditCardNumber", "filename", "floristBusinessInput", "floristPhoneInput",
          "floristAddressInput", "floristCityInput", "floristZipInput", "floristStateInput");

      var images = new Array("floristSearch", "floristClose", "sourceCodeSearch", "sourceCodeClose");


    /*
     *  Initialization
     */
      function init(){
        setNavigationHandlers();
        setErrorFields();
        addDefaultListeners(fieldNames);
        addImageCursorListener(images);
        populateStates('billingCountry', 'billingState', selectedState);
        setCCMonth();
        setCCYear();
        setCreditCardVisibility();
        document.forms[0].sourceCode.focus();
      }

      function setCreditCardVisibility(){
        getStyleObject("creditCardDiv").display = paymentDisyplayStyle;
      }

      function populateStates(countrySelectBox, stateSelectBox, selectedState){
        var form = document.forms[0];
        var selectedCountry = document.getElementById(countrySelectBox)[document.getElementById(countrySelectBox).selectedIndex].value;
        var toPopulate = (selectedCountry == "US") ? usStates : (selectedCountry == "CA") ? caStates : naStates;

        var states = document.getElementById(stateSelectBox);
        states.options.length = 0;
        states.add(new Option("Select One", "", false, false));
        for (var i = 0; i < toPopulate.length; i++){
            states.add(new Option(toPopulate[i][1], toPopulate[i][0], false, false));
        }
        setSelectedState(stateSelectBox, selectedState);
      }

      function setSelectedState(field, state){
        if (state == null || state == '')
          return;

        var states = document.getElementById(field);
        for (var i = 0; i < states.length; i++){
          if (states[i].value == state){
            states.selectedIndex = i;
            break;
          }
        }
      }

      function billingSameAsCustomer(checkbox){
        var form = document.forms[0];
        if (checkbox.checked) {
          form.billingFirstname.value = form.customerFirstname.value;
          form.billingLastname.value = form.customerLastname.value;
          form.billingEmail.value = form.customerEmail.value;
          form.billingPhone.value = form.customerPhone.value;
          form.billingPhoneExt.value = form.customerPhoneExt.value;
        } else {
          form.billingFirstname.value = "";
          form.billingLastname.value = "";
          form.billingEmail.value = "";
          form.billingPhone.value = "";
          form.billingPhoneExt.value = "";
        }
      }

      function doPaymentChange(){
        var form = document.forms[0];
        var selectbox = form.paymentType;
        if (selectbox[selectbox.selectedIndex].id == "I"){
          getStyleObject("creditCardDiv").display = "none";
          getStyleObject("creditCardErrorMessagesDiv").display = "none";
          form.creditCardNumber.value = "";
          form.creditCardMonth.selectedIndex = 0;
          form.creditCardYear.selectedIndex = 0;
        } else {
          getStyleObject("creditCardDiv").display = "block";
          getStyleObject("creditCardErrorMessagesDiv").display = "block";
        }
      }

      function setCCMonth(){
        var months = document.forms[0].creditCardMonth;
        for (var i = 0; i < months.length; i++){
          if (months[i].value == ccExpMonth){
            months.selectedIndex = i;
            break;
          }
        }
      }
      
      function setCCYear(){
        var NUM_YEARS = 10;
        var form = document.forms[0];
        var year = new Date().getFullYear();

        form.creditCardYear.options[0] = new Option("", "", false, false);
        for (var i = 1; i <= NUM_YEARS+1; i++, year++){
          if (year == ccExpYear)
            form.creditCardYear.options[i] = new Option(year, year, true, true);
          else
            form.creditCardYear.options[i] = new Option(year, year, false, false);
        }
      }

    /*
     *  Source Code Checking
     */
      var _prevSourceCode = "";
      function storeSourceCode(field){
        _prevSourceCode = field.value;
      }
      function setButtonsAccess(){
        document.forms[0].sourceCodeAccept.disabled = false;
        document.forms[0].submitButton.disabled = true;
      }

    /*
     *  Actions
     */
      function doSourceCodeAction(field){
        var url = "/" + context_js + "/bulkload/CheckDataServlet" +
                  "?securitytoken=" + securityParam +
                  "&context=" + securityContext +
                  "&applicationcontext=" + context_js + 
                  "&adminAction=" + adminAction;

        performAction(url);
      }
      function doSubmitAction(){
        var url = "/" + context_js + "/bulkload/ProcessBulkOrderServlet" +
                  "?securitytoken=" + securityParam +
                  "&context=" + securityContext +
                  "&applicationcontext=" + context_js + 
                  "&adminAction=" + adminAction;

        performAction(url);
      }
      function doExitAction(){
        var url = exitpage +
                  "?securitytoken=" + securityParam +
                  "&context=" + securityContext +
                  "&applicationcontext=" + context_js + 
                  "&adminAction=" + adminAction;

        performAction(url);
      }

      function doStatus(){
        document.forms[0].action = "\ViewBulkOrderStatusServlet?securitytoken=" + securityParam + "&context=" + securityContext + "&applicationcontext="+context_js+ "&adminAction=" + adminAction;
        document.forms[0].submit();
		}      

      function performAction(url){
        showWaitMessage("content", "wait")
        document.forms[0].action = url;
        document.forms[0].submit();
      }]]>
  </script>
</head>
<body onload="javascript:init();">

  <!-- Header template -->
  <xsl:call-template name="header">
    <xsl:with-param name="headerName" select="'Bulk Order'"/>
    <xsl:with-param name="showTime" select="true()"/>
    <xsl:with-param name="showExitButton" select="true()"/>
  </xsl:call-template>

  <!-- Content div needed to hold any content that will be blocked when an action is performed -->
  <div id="content" style="display:block">

    <form name="form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="guid" value="{FIELDS/FIELD[@fieldname='guid']/@value}"/>
    <input type="hidden" name="securitytoken" value="{$securitytoken}"/>
    <input type="hidden" name="adminAction" value="{$adminAction}"/>

<table width="98%" border="0" align="center" cellpadding="1" cellspacing="1">
    <tr>
        <td width="12%">
            <a href="#" onclick="doStatus();">Bulk Order Status</a>
        </td>
    </tr>
</table>
    <table width="98%" border="3" align="center" cellpadding="1" cellspacing="1" bordercolor="#006699">
      <tr>
        <td>
          <!-- Customer Contact -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td width="15%" class="label">
                Source Code:  <span class="requiredFieldTxt">***</span><br/>
                <input type="button" name="sourceCodeAccept" value="Click to Accept" tabindex="2" disabled="true" onclick="javascript:doSourceCodeAction();"/>
              </td>
              <td width="35%">
                <input type="text" name="sourceCode" tabindex="1" size="10" maxlength="10" value="{FIELDS/FIELD[@fieldname='sourceCode']/@value}" onfocus="javascript:storeSourceCode(this);" onkeyup="javascript:setButtonsAccess();"/>&nbsp;&nbsp;
                <a tabindex="2" href="javascript:openSourceCodeLookup();" class="link">Lookup Source Code</a>
              </td>
              <td width="15%" class="label">Florist Code:</td>
              <td width="35%"><input type="text" name="floristCode" tabindex="3" size="10" maxlength="9" value="{FIELDS/FIELD[@fieldname='floristCode']/@value}"/>&nbsp;&nbsp;<a tabindex="4" href="javascript:openFloristLookup();" class="link">Select Florist</a></td>
            </tr>
            <tr>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="ERRORS/ERROR[@fieldname='sourceCode']/@description"/></td>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"></td>
            </tr>
            <tr>
              <td colspan="4" class="tblheader">Customer Contact</td>
            </tr>
            <tr>
              <td width="15%" class="label">First Name:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%"><input type="text" name="customerFirstname" tabindex="5" size="30"  maxlength="20" value="{key('field', 'customerFirstname')/@value}"/></td>
              <td width="15%" class="label">Phone:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="35%"><input type="text" name="customerPhone" tabindex="7" size="15" maxlength="20" onkeypress="digitOnlyListener();" value="{key('field', 'customerPhone')/@value}"/></td>
                    <td width="61%" class="label">&nbsp;&nbsp;Ext:&nbsp;<input type="text" name="customerPhoneExt" tabindex="8" size="6" maxlength="10" value="{key('field', 'customerPhoneExt')/@value}"/></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'customerFirstname')/@description"/></td>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'customerPhone')/@description"/></td>
            </tr>
            <tr>
              <td width="15%" class="label">Last Name:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%"><input type="text" name="customerLastname" tabindex="6" size="30"  maxlength="20" value="{key('field', 'customerLastname')/@value}"/></td>
              <td width="15%" class="label">Email Address:</td>
              <td width="35%"><input type="text" name="customerEmail" tabindex="9" size="30"  maxlength="55" value="{key('field', 'customerEmail')/@value}"/></td>
            </tr>
            <tr>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'customerLastname')/@description"/></td>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'customerEmail')/@description"/></td>
            </tr>
          </table>

          <!-- Billing Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="2">Billing Information</td>
            </tr>
            <tr>
              <td colspan="2" class="label">
                <input type="checkbox" name="billingSame" tabindex="10" onclick="billingSameAsCustomer(this);"/>Same as above
              </td>
            </tr>
            <tr>
              <td width="50%" valign="top">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td width="30%" class="label">First Name:  <span class="requiredFieldTxt">***</span></td>
                    <td width="70%"><input type="text" name="billingFirstname" tabindex="11" size="30"  maxlength="20" value="{key('field', 'billingFirstname')/@value}"/></td>
                  </tr>
                  <tr>
                    <td width="30%"></td>
                    <td width="70%" class="errorMessage"><xsl:value-of select="key('error', 'billingFirstname')/@description"/></td>
                  </tr>
                  <tr>
                    <td width="30%" class="label">Last Name:  <span class="requiredFieldTxt">***</span></td>
                    <td width="70%"><input type="text" name="billingLastname" tabindex="12" size="30"  maxlength="20" value="{key('field', 'billingLastname')/@value}"/></td>
                  </tr>
                  <tr>
                    <td width="30%"></td>
                    <td width="70%" class="errorMessage"><xsl:value-of select="key('error', 'billingLastname')/@description"/></td>
                  </tr>
                  <tr>
                    <td width="30%" class="label">Business Name:</td>
                    <td width="70%"><input type="text" name="businessName" tabindex="13" maxlength="30" size="35" value="{key('field', 'businessName')/@value}"/></td>
                  </tr>
                  <tr>
                    <td width="30%"></td>
                    <td width="70%" class="errorMessage"><xsl:value-of select="key('error', 'businessName')/@description"/></td>
                  </tr>
                  <tr>
                    <td width="30%" class="label">Address Line 1:  <span class="requiredFieldTxt">***</span></td>
                    <td width="70%"><input type="text" name="billingAddressLine1" tabindex="14" maxlength="30" size="35" value="{key('field', 'billingAddressLine1')/@value}"/></td>
                  </tr>
                  <tr>
                    <td width="30%"></td>
                    <td width="70%" class="errorMessage"><xsl:value-of select="key('error', 'billingAddressLine1')/@description"/></td>
                  </tr>
                  <tr>
                    <td width="30%" class="label">Address Line 2:</td>
                    <td width="70%"><input type="text" name="billingAddressLine2" tabindex="15" maxlength="30" size="35" value="{key('field', 'billingAddressLine2')/@value}"/></td>
                  </tr>
                  <tr>
                    <td width="30%" class="label">City:  <span class="requiredFieldTxt">***</span></td>
                    <td width="70%" class="label">
                      <input type="text" name="billingCity" tabindex="16" maxlength="30" value="{key('field', 'billingCity')/@value}"/>
                      &nbsp;State:&nbsp;<span class="requiredFieldTxt">***</span>&nbsp;
                      <select name="billingState" tabindex="17"></select>
                    </td>
                  </tr>
                  <tr>
                    <td width="30%"></td>
                    <td width="70%">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'billingCity')/@description"/></td>
                          <td width="10%">&nbsp;</td>
                          <td width="30%" class="errorMessage"><xsl:value-of select="key('error', 'billingState')/@description"/></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td width="30%" class="label">Zip/Postal Code:  <span class="requiredFieldTxt">***</span></td>
                    <td width="70%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="25%"><input type="text" name="billingZip" tabindex="18" size="6"  maxlength="10" value="{key('field', 'billingZip')/@value}"/></td>
                        <td width="15%" class="label">&nbsp;Country:&nbsp;<span class="requiredFieldTxt">***</span>&nbsp;</td>
                        <td width="60%">
                          <select name="billingCountry" tabindex="19" onchange="javascript:populateStates('billingCountry', 'billingState', selectedState);">
                            <xsl:for-each select="COUNTRIES/COUNTRY">
                              <xsl:choose>
                                <xsl:when test="@country_id=$selectedBillingCountry">
                                  <option value="{@country_id}" selected="true"><xsl:value-of select="@name"/></option>
                                </xsl:when>
                                <xsl:otherwise>
                                  <option value="{@country_id}"><xsl:value-of select="@name"/></option>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:for-each>
                          </select>
                        </td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                  <tr>
                    <td width="30%"></td>
                    <td width="70%">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="25%" class="errorMessage"><xsl:value-of select="key('error', 'billingZip')/@description"/></td>
                          <td width="15%">&nbsp;</td>
                          <td width="60%" class="errorMessage"><xsl:value-of select="key('error', 'billingCountry')/@description"/></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
              <td width="50%" valign="top">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                  <tr>
                    <td width="35%" class="label">Phone:  <span class="requiredFieldTxt">***</span></td>
                    <td width="65%">
                      <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="35%"><input type="text" name="billingPhone" tabindex="20" size="15" maxlength="20" onkeypress="digitOnlyListener();" value="{key('field', 'billingPhone')/@value}"/></td>
                          <td width="61%" class="label">&nbsp;&nbsp;Ext:&nbsp;<input type="text" name="billingPhoneExt" tabindex="21" size="6" maxlength="10" value="{key('field', 'billingPhoneExt')/@value}"/></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td width="35%"></td>
                    <td width="65%" class="errorMessage"><xsl:value-of select="key('error', 'billingPhone')/@description"/></td>
                  </tr>
                  <tr>
                    <td width="35%" class="label">Email Address:  <span class="requiredFieldTxt">***</span></td>
                    <td width="65%"><input type="text" name="billingEmail" tabindex="22" size="30"  maxlength="55" value="{key('field', 'billingEmail')/@value}"/></td>
                  </tr>
                  <tr>
                    <td width="35%"></td>
                    <td width="65%" class="errorMessage"><xsl:value-of select="key('error', 'billingEmail')/@description"/></td>
                  </tr>
                  <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="35%">&nbsp;</td>
                    <td width="65%">&nbsp;</td>
                  </tr>
                  <xsl:if test="FIELDS/FIELD[@fieldname='allowMembershipInfo']/@value='Y'">
                    <tr>
                      <td width="35%" class="label">Membership ID:  <span class="requiredFieldTxt">***</span></td>
                      <td width="65%"><input type="text" name="membershipId" tabindex="23" maxlength="20" size="20" value="{key('field', 'membershipId')/@value}" onfocus="javascript:fieldFocus();" onblur="javascript:fieldBlur();"/></td>
                    </tr>
                    <tr>
                      <td width="35%"></td>
                      <td width="65%" class="errorMessage"><xsl:value-of select="key('error', 'membershipId')/@description"/></td>
                    </tr>
                    <tr>
                      <td width="35%" class="label">Membership First Name:  <span class="requiredFieldTxt">***</span></td>
                      <td width="65%"><input type="text" name="membershipFirstName" tabindex="24" maxlength="20" size="20" value="{key('field', 'membershipFirstName')/@value}" onfocus="javascript:fieldFocus();" onblur="javascript:fieldBlur();"/></td>
                    </tr>
                    <tr>
                      <td width="35%"></td>
                      <td width="65%" class="errorMessage"><xsl:value-of select="key('error', 'membershipFirstName')/@description"/></td>
                    </tr>
                    <tr>
                      <td width="35%" class="label">Membership Last Name:  <span class="requiredFieldTxt">***</span></td>
                      <td width="65%"><input type="text" name="membershipLastName" tabindex="25" maxlength="20" size="20" value="{key('field', 'membershipLastName')/@value}" onfocus="javascript:fieldFocus();" onblur="javascript:fieldBlur();"/></td>
                    </tr>
                    <tr>
                      <td width="35%"></td>
                      <td width="65%" class="errorMessage"><xsl:value-of select="key('error', 'membershipLastName')/@description"/></td>
                    </tr>
                  </xsl:if>
                </table>
              </td>
            </tr>
          </table>

          <!-- Payment Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2">
            <tr>
              <td class="tblheader" colspan="4">Payment Information</td>
            </tr>
            <tr>
              <td width="15%" class="label">Payment Type:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%">
                <select name="paymentType" tabindex="26" onchange="doPaymentChange();">
                  <option id="" value=""></option>
                  <xsl:for-each select="PAYMENT_TYPES/PAYMENT_TYPE">
                    <xsl:choose>
                      <xsl:when test="@payment_method_id=$selectedPaymentType">
                        <option id="{@payment_type}" value="{@payment_method_id}" selected="true"><xsl:value-of select="@description"/></option>
                      </xsl:when>
                      <xsl:otherwise>
                        <option id="{@payment_type}" value="{@payment_method_id}"><xsl:value-of select="@description"/></option>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </select>
              </td>
              <td width="15%"></td>
              <td width="35%"></td>
            </tr>
            <tr>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'paymentType')/@description"/></td>
              <td width="15%"></td>
              <td width="35%"></td>
            </tr>
            <tr id="creditCardDiv">
              <td width="15%" class="label">Credit Card Number:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%"><input type="text" name="creditCardNumber" tabindex="27"  maxlength="20" onkeypress="digitOnlyListener();" value="{key('field', 'creditCardNumber')/@value}"/></td>
              <td width="20%" class="label">Credit Card Expiration Date:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%" class="label">
                <select name="creditCardMonth" tabindex="28">
                  <option value=""></option>
                  <option value="01">January</option>
                  <option value="02">February</option>
                  <option value="03">March</option>
                  <option value="04">April</option>
                  <option value="05">May</option>
                  <option value="06">June</option>
                  <option value="07">July</option>
                  <option value="08">August</option>
                  <option value="09">September</option>
                  <option value="10">October</option>
                  <option value="11">November</option>
                  <option value="12">December</option>
                </select>
                &nbsp;<b>/</b>&nbsp;
                <select name="creditCardYear" tabindex="29"/>
              </td>
            </tr>
            <tr id="creditCardErrorMessagesDiv">
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'creditCardNumber')/@description"/></td>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'creditCardExpDate')/@description"/></td>
            </tr>
          </table>
          <!-- File Information -->
          <table width="100%" border="0" cellpadding="2" cellspacing="2" bordercolor="#006699">
            <tr>
              <td class="tblheader" colspan="4">File Information</td>
            </tr>
            <tr>
              <td width="15%" class="label">File:  <span class="requiredFieldTxt">***</span></td>
              <td width="35%">
                <input type="file" name="filename" tabindex="30"/>
              </td>
              <td width="15%"></td>
              <td width="35%"></td>
            </tr>
            <tr>
              <td width="15%"></td>
              <td width="35%" class="errorMessage"><xsl:value-of select="key('error', 'filename')/@description"/></td>
              <td width="15%"></td>
              <td width="35%"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <!-- Buttons -->
    <table width="98%" border="0" cellpadding="2" cellspacing="2">
      <tr>
        <td width="25%" class="requiredFieldTxt">
          *** Required Fields
        </td>
        <td width="50%" align="center">
          <input type="button" id="submitButton" name="validate" value="Process Bulk Order" tabindex="31" onclick="javascript:doSubmitAction();">
            <xsl:if test="not(FIELDS/FIELD)"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
            <xsl:if test="ERRORS/ERROR[@fieldname='sourceCode']"><xsl:attribute name="disabled">true</xsl:attribute></xsl:if>
          </input>
        </td>
        <td width="25%" align="right">
          <input type="button" id="exitButton" name="exit" value="Exit" tabindex="32" onclick="javascript:doExitAction();"/>
        </td>
      </tr>
    </table>

    <xsl:call-template name="sourceCodeLookupDiv"/>
    <xsl:call-template name="floristLookupDiv"/>

    </form>
  </div>

  <!-- Processing message div -->
  <div id="waitDiv" style="display:none">
    <table id="waitTable" width="98%" border="3" height="100%" align="center" cellpadding="30" cellspacing="1" bordercolor="#006699">
      <tr>
        <td width="100%">
          <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td id="waitMessage" align="right" width="50%" class="waitMessage"></td>
              <td id="waitTD" width="50%" class="waitMessage"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>

  <!-- Footer template -->
  <xsl:call-template name="footer"/>

</body>
</html>

</xsl:template>
</xsl:stylesheet>