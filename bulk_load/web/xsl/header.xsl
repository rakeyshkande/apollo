<!DOCTYPE ACDemo[
	<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template name="header">
<xsl:param name="headerName"/>
<xsl:param name="showTime"/>
<xsl:param name="showExitButton"/>
<xsl:param name="applicationcontext"/>
    <script type="text/javascript" src="../js/clock.js"/>
    <table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="20%" align="left"><img border="0" height="32" src="../images/wwwftdcom_131x32.gif" width="131"/></td>
        <td width="60%" align="center" colspan="1" class="header"><xsl:value-of select="$headerName"/></td>
        <td width="20%" align="right" class="label">
          <table width="100%" cellspacing="0" cellpadding="0">
            <xsl:choose>
              <xsl:when test="$showTime">
                <tr>
                  <td id="time" align="right" class="label"></td>
                  <script type="text/javascript">startClock();</script>
                </tr>
              </xsl:when>
            </xsl:choose>
            <xsl:choose>
              <xsl:when test="$showExitButton">
                <tr>
                  <td align="right">
                    <input type="button" name="exitButtonHeader" value="Exit" onclick="javascript:doExitAction();"/>
                  </td>
                </tr>
              </xsl:when>
            </xsl:choose>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3"><hr/></td>
      </tr>
    </table>

</xsl:template>
</xsl:stylesheet>