<!DOCTYPE ACDemo[
    <!ENTITY nbsp "&#160;">
    <!ENTITY copy "&#169;">
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="no"/>
<xsl:output indent="yes"/>

<xsl:param name="phoneInput" select="phoneInput"/>
<xsl:param name="institutionInput" select="institutionInput"/>
<xsl:param name="addressInput" select="addressInput"/>
<xsl:param name="cityInput" select="cityInput"/>
<xsl:param name="stateInput" select="stateInput"/>
<xsl:param name="zipCodeInput" select="zipCodeInput"/>
<xsl:param name="countryInput" select="countryInput"/>
<xsl:param name="applicationcontext"/>
<xsl:template match="/root">

<html>
<head>
    <title>FTD - Florist Lookup</title>
    <script language="JavaScript" src="/{$applicationcontext}/js/FormChek.js"/>
    <script language="JavaScript" src="/{$applicationcontext}/js/util.js"/>
    <script language="JavaScript">
        <![CDATA[
        var fieldNames = new Array(
            "institutionInput", "phoneInput", "addressInput", "cityInput", 
            "stateInput", "zipCodeInput");

        var images = new Array("floristSearch", "floristCloseTop", "floristCloseBottom", "floristSelectNone");

        function init()
        {
            addDefaultListeners(fieldNames);
            addImageCursorListener(images);
            window.name = "VIEW_FLORIST_LOOKUP";
            document.forms[0].institutionInput.focus();
        }

        function onKeyDown()
        {
            if (window.event.keyCode == 13)
                reopenPopup();
        }

        function reopenPopup()
        {
            var form = document.forms[0];
            check = true;
            business = stripWhitespace(form.institutionInput.value);
            city = stripWhitespace(form.cityInput.value);
            state = stripWhitespace(form.stateInput.value);
            zip = stripWhitespace(form.zipCodeInput.value);
            phone = stripWhitespace(form.phoneInput.value);

            //state is required if business is given
            if ((business.length > 0) & (state.length == 0))
            {
                if (check == true)
                {
                    form.stateInput.focus();
                    check = false;
                }
                form.stateInput.style.backgroundColor = 'pink';
            }

            //city is required if no zip and phone are given
            if ((city.length == 0) & (zip.length == 0) & (phone.length == 0))
            {
                if (check == true)
                {
                    form.cityInput.focus();
                    check = false;
                }
                form.cityInput.style.backgroundColor = 'pink';
            }

            //zip is required if no city, state and phone are given
            if ((zip.length == 0) & (city.length == 0) & (state.length == 0) & (phone.length == 0))
            {
                if (check == true)
                {
                    form.zipCodeInput.focus();
                    check = false;
                }
                form.zipCodeInput.style.backgroundColor = 'pink';
            }

            if (!check)
            {
                alert("Please correct the marked fields")
            }
            else
            {
                form.action = "";
                form.method = "get";
                form.target = "VIEW_FLORIST_LOOKUP";
                form.submit();
            }
        }

        function closeFloristLookup(floristId)
        {
            if (window.event.keyCode == 13)
                populatePage(floristId);
        }

        function populatePage(floristId)
        {
            window.returnValue = floristId;
            window.close();
        }
    ]]>
    </script>
    <link rel="stylesheet" type="text/css" href="/{$applicationcontext}/css/ftd.css"></link>
</head>

<body onLoad="javascript:window.focus(); init();">
    <form name="form" method="get" action="">
    <input type="hidden" name="POPUP_ID" value="LOOKUP_FLORIST"/>
    <table  width="98%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td class="header">Florist Lookup</td>
        </tr>
        <tr>
            <td width="100%" valign="top">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td width="15%" class="labelright">Name:</td>
                        <td width="35%"><input type="text" name="institutionInput" tabindex="0" class="TblText" size="30" maxlength="75" value="{$institutionInput}"/></td>
                        <td width="15%" class="labelright">Phone:</td>
                        <td width="35%"><input type="text" name="phoneInput" tabindex="1" class="TblText" size="20" maxlength="20" value="{$phoneInput}"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="labelright">Address:</td>
                        <td width="35%"><input type="text" name="addressInput" tabindex="2" class="TblText" size="20" maxlength="75" value="{$addressInput}"/></td>
                        <td width="15%" class="labelright">City:</td>
                        <td width="35%"><input type="text" name="cityInput" tabindex="3" class="TblText" maxlength="50" size="20" value="{$cityInput}"/></td>
                    </tr>
                    <tr>
                        <td width="15%" class="labelright">State:</td>
                        <xsl:choose>
                            <xsl:when test="$countryInput='US'">
                                <td width="35%">
                                    <select tabindex="4" class="TblText" name="stateInput">
                                        <option value=""></option>
                                        <xsl:for-each select="STATES/STATE[@countrycode='']">
                                            <option value="{@statemasterid}"> <xsl:value-of select="@statename"/> </option>
                                        </xsl:for-each>
                                    </select>
                                </td>
                            </xsl:when>
                            <xsl:when test="$countryInput='CA'">
                                <td width="35%">
                                    <select tabindex="4" class="TblText" name="stateInput">
                                        <option value=""></option>
                                        <xsl:for-each select="STATES/STATE[@countrycode='CAN']">
                                            <option value="{@statemasterid}"> <xsl:value-of select="@statename"/> </option>
                                        </xsl:for-each>
                                    </select>
                                </td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td width="35%">
                                    <input type="text" name="stateInput" tabindex="4" class="TblText" maxlength="30" size="6" value="{$stateInput}"/>
                                </td>
                            </xsl:otherwise>
                        </xsl:choose>
                        <td width="15%" nowrap="true" class="labelright"> Zip/Postal Code: &nbsp;</td>
                        <td><input type="text" name="zipCodeInput" tabindex="5" class="TblText" maxlength="6" size="6" value="{$zipCodeInput}"/></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><img id="floristSearch" tabindex="6" src="../images/button_search.gif" alt="Search" border="0" onclick="javascript:reopenPopup()" onkeydown="javascript:onKeyDown()"/></td>
                    </tr>
                    <tr>
                        <td colspan="10"><hr/></td>
                    </tr>
                    <table width="100%" id="FormContainer">
                        <tr>
                            <td class="instruction">&nbsp;Please click on an arrow to select a customer.</td>
                            <td align="right"><img id="floristCloseTop" tabindex="7" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeFloristLookup('')"/></td>
                        </tr>
                        <tr>
                            <table class="LookupTable" width="100%">
                                <tr>
                                    <td width="5%" valign="bottom"> &nbsp; </td>
                                    <td width="5%" class="label" valign="bottom">Weight</td>
                                    <td class="label" valign="bottom">Member<br/>Number</td>
                                    <td class="label" valign="bottom">Name / Address</td>
                                    <td class="label" valign="bottom">Phone</td>
                                    <td class="label" valign="bottom" align="center">Goto<br/>Flag</td>
                                    <td class="label" valign="bottom" align="center">Sunday<br/>Delivery</td>
                                    <td class="label" valign="bottom" align="center">Mercury<br/>Flag</td>
                                    <td class="label" valign="bottom">Hours</td>
                                </tr>
                                <tr>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                    <td><hr/></td>
                                </tr>
                                <tr>
                                    <td>
                                        <xsl:for-each select="searchResults/searchResult">
                                            <tr>
                                                <td>
                                                    <xsl:choose>
                                                    <xsl:when test = "@floristblocktype != ''">
                                                        <img alt="disabled" src="../images/selectButtonRight_disabled.gif"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <img onclick="javascript: populatePage('{@floristid}')" onkeydown="javascript:closeFloristLookup('{@floristid}')" tabindex="8" src="../images/selectButtonRight.gif"/>
                                                    </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                <xsl:choose>
                                                    <xsl:when test="@floristzipcode = $zipCodeInput">
                                                        <td align="center" class="matchedSearchResult"><xsl:value-of select="@floristweight"/></td>
                                                        <td align="center" class="matchedSearchResult"><xsl:value-of select="@floristid"/></td>
                                                        <td class="matchedSearchResult"><xsl:value-of select="@floristname" disable-output-escaping="yes"/> &nbsp;<xsl:value-of select="@address"/>&nbsp;<xsl:value-of select="@city"/>&nbsp;<xsl:value-of select="@state"/>&nbsp;<xsl:value-of select="@floristzipcode"/></td>
                                                        <td class="matchedSearchResult"><xsl:value-of select="@phonenumber"/></td>
                                                        <td align="center" class="matchedSearchResult"><xsl:value-of select="@gotoflag"/></td>
                                                        <td align="center" class="matchedSearchResult"><xsl:value-of select="@sundayflag"/></td>
                                                        <td align="center" class="matchedSearchResult"><xsl:value-of select="@mercuryflag"/></td>
                                                        <td align="center" class="matchedSearchResult"><xsl:value-of select="@hours"/></td>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td align="center"><xsl:value-of select="@floristweight"/></td>
                                                        <td align="center"><xsl:value-of select="@floristid"/></td>
                                                        <td><xsl:value-of select="@floristname" disable-output-escaping="yes"/> &nbsp;<xsl:value-of select="@address"/>&nbsp;<xsl:value-of select="@city"/>&nbsp;<xsl:value-of select="@state"/>&nbsp;<xsl:value-of select="@floristzipcode"/></td>
                                                        <td><xsl:value-of select="@phonenumber"/></td>
                                                        <td align="center"><xsl:value-of select="@gotoflag"/></td>
                                                        <td align="center"><xsl:value-of select="@sundayflag"/></td>
                                                        <td align="center"><xsl:value-of select="@mercuryflag"/></td>
                                                        <td align="center"><xsl:value-of select="@hours"/></td>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </tr>
                                            <tr>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                                <td><hr/></td>
                                            </tr>
                                        </xsl:for-each>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img id="floristSelectNone" tabindex="9" src="../images/selectButtonRight.gif" border="0" onclick="javascript:populatePage('');" onkeydown="javascript:closeFloristLookup('');"/>
                                    </td>
                                    <td colspan="8"> None of the above </td>
                                </tr>
                            </table>
                        </tr>
                        <tr>
                            <td align="right">
                                <img id="floristCloseBottom" tabindex="10" src="../images/button_close.gif" alt="Close screen" border="0" onclick="javascript:populatePage('')" onkeydown="javascript:closeFloristLookup('');"/>
                            </td>
                        </tr>
                    </table>
                </table>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

</xsl:template>
</xsl:stylesheet>