package com.ftd.osp.bulkload.vo;


/** This class contains line item information.  Each instance of this vo 
 * represents one field.
 * @author Ed Mueller */
public class LineItemVO 
{
  private String fieldName;
  private String value;

  public LineItemVO()
  {
  }

  public LineItemVO(String fieldName, String value)
  {
    this.fieldName = fieldName;
    this.value = value;
  }

  public String getFieldName()
  {
    return fieldName;
  }

  public void setFieldName(String newFieldName)
  {
    fieldName = newFieldName;
  }

 
  public String getValue()
  {
    return value;
  }

  public void setValue(String newValue)
  {
    value = newValue;
  }
}