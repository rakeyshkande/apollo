package com.ftd.osp.bulkload.vo;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import com.ftd.osp.bulkload.vo.LineItemVO;

/** This class represents a line item as found in the Excel spreadsheet.
 *  Each instance of this VO represents a line in the order\spreadsheet. 
 *  @author Ed Mueller*/
public class LineVO 
{
  private int lineNumber;
  private List lineItems;

  protected LineItemVO lineItemVO0[];

  public LineVO()
  {
  }

  public int getLineNumber()
  {
    return lineNumber;
  }

  public void setLineNumber(int newLineNumber)
  {
    lineNumber = newLineNumber;
  }

  public List getLineItems()
  {
    return lineItems;
  }

 public Map getLineItemsMap()
  {
    HashMap map = new HashMap();

    //put all lines into map
    for(int i=0;i<lineItems.size();i++)
    {
      LineItemVO line = (LineItemVO)lineItems.get(i);
      map.put(line.getFieldName(),line.getValue());
    }

    return map;
  }

  public void setLineItems(List newFields)
  {
    lineItems = newFields;
  }

  /** This method sets the line items in the vo.
   * @param Map map of data */
  public void setLineItems(Map map)
  {
    List list = new ArrayList();
    Set keys = map.keySet();
    Iterator it = keys.iterator();
    while(it.hasNext())
    {
        String key = (String)it.next();
        String value =  (String)map.get(key);
        LineItemVO vo = new LineItemVO(key,value);
        list.add(vo);
    }    

    this.setLineItems(list);

  }  
}