package com.ftd.osp.bulkload.vo;

/** This vo represents validation informatin for one field.
 * @author Ed Mueller */
public class ValidationVO 
{
  private String field;
  private int count;
  private String message;
  private String type;

  public ValidationVO()
  {
  }

  /** This method increments the count by 1
   * @returns void */
   public void increment()
   {
     count++;
   }

  public String getField()
  {
    return field;
  }

  public void setField(String newField)
  {
    field = newField;
  }

  public int getCount()
  {
    return count;
  }

  public void setCount(int newCount)
  {
    count = newCount;
  }

  public String getMessage()
  {
    return message;
  }

  public void setMessage(String newMessage)
  {
    message = newMessage;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String newType)
  {
    type = newType;
  }
}