package com.ftd.osp.bulkload.vo;

import com.ftd.osp.bulkload.utilities.CommonUtility;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

/** This VO reprsents an order 
 * @author Ed Mueller */
public class OrderDocVO 
{
  private String contactFirstName;
  private String contactPhone;
  private String contactLastName;
  private String contactExt;
  private String sourceCode;
  private String firstName;
  private String lastName;
  private String businessName;
  private String addressLine1;
  private String addressLine2;
  private String phone;
  private String ext;
  private String city;
  private String state;
  private String emailAddress;
  private String zipCode;
  private String country;
  private String creditCardNumber;
  private String file;
  private List lines;
  private String expirationDate;
  private String paymentFirstName;
  private String paymentLastName;
  private String floristNumber;
  private String guid;
  private String contactEmail;
  private String paymentType;
  double orderTotal;
  private String creditCardMonth;
  private String creditCardYear;
  private String membershipID;
  String membershipFirstName;
  private String membershipLastName;
  private String MembershipType;
  private String allowMembershipInfo = "N";//default to no
  private boolean SourceCodeValidated = false;
  private String paymentMethodId;
  private String MasterOrderNumber;
  private String csrId;

  public String getContactFirstName()
  {
    return contactFirstName;
  }

  public void setContactFirstName(String value)
  {
    contactFirstName = value;
  }

  public String getContactLastName()
  {
    return contactLastName;
  }

  public void setContactLastName(String value)
  {
    contactLastName = value;
  }
  
  public String getContactPhone()
  {
    return contactPhone;
  }

  public void setContactPhone(String value)
  {
    contactPhone = value;
  }

  public String getContactExt()
  {
    return contactExt;
  }

  public void setContactExt(String value)
  {
    contactExt = value;
  }

  

  public String getSourceCode()
  {
    return sourceCode;
  }

  public void setSourceCode(String value)
  {
    sourceCode = value;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String newFirstname)
  {
    firstName = newFirstname;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String newLastName)
  {
    lastName = newLastName;
  }

  public String getBusinessName()
  {
    return businessName;
  }

  public void setBusinessName(String newBusinessName)
  {
    businessName = newBusinessName;
  }

  public String getAddressLine1()
  {
    return addressLine1;
  }

  public void setAddressLine1(String newAddressLine1)
  {
    addressLine1 = newAddressLine1;
  }

  public String getAddressLine2()
  {
    return addressLine2;
  }

  public void setAddressLine2(String newAddressLine2)
  {
    addressLine2 = newAddressLine2;
  }

  public String getPhone()
  {
    return phone;
  }

  public void setPhone(String newPhone)
  {
    phone = newPhone;
  }

  public String getExt()
  {
    return ext;
  }

  public void setExt(String newExt)
  {
    ext = newExt;
  }

  public String getCity()
  {
    return city;
  }

  public void setCity(String newCity)
  {
    city = newCity;
  }

  public String getState()
  {
    return state;
  }

  public void setState(String newState)
  {
    state = newState;
  }

  public String getEmailAddress()
  {
    return emailAddress;
  }

  public void setEmailAddress(String newEmailAddress)
  {
    emailAddress = newEmailAddress;
  }

  public String getZipCode()
  {
    return zipCode;
  }

  public void setZipCode(String newZipCode)
  {
    zipCode = newZipCode;
  }

  public String getCountry()
  {
    return country;
  }

  public void setCountry(String newCountry)
  {
    country = newCountry;
  }

  public String getCreditCardNumber()
  {
    return creditCardNumber;
  }

  public void setCreditCardNumber(String newCreditCard)
  {
    creditCardNumber = newCreditCard;
  }

  public String getFile()
  {
    return file;
  }

  public void setFile(String newFile)
  {
    file = newFile;
  }

  public List getLines()
  {
    return lines;
  }

  public void setLines(List newLines)
  {
    lines = newLines;
  }

  public String getExpirationDate()
  {
    return expirationDate;
  }

  public void setExpirationDate(String newExpirationDate)
  {
    expirationDate = newExpirationDate;

    updateMonthYear();

  }



  public String getPaymentFirstName()
  {
    return paymentFirstName;
  }

  public void setPaymentFirstName(String newPaymentFirstName)
  {
    paymentFirstName = newPaymentFirstName;
  }

  public String getPaymentLastName()
  {
    return paymentLastName;
  }

  public void setPaymentLastName(String newPaymentLastName)
  {
    paymentLastName = newPaymentLastName;
  }

  public String getFloristNumber()
  {
    return floristNumber;
  }

  public void setFloristNumber(String newFloristNumber)
  {
    floristNumber = newFloristNumber;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String newGuid)
  {
    guid = newGuid;
  }

  public String getContactEmail()
  {
    return contactEmail;
  }

  public void setContactEmail(String newContactEmail)
  {
    contactEmail = newContactEmail;
  }

  public String getPaymentType()
  {
    return paymentType;
  }

  public void setPaymentType(String newPaymentType)
  {
    paymentType = newPaymentType;
  }

  public double getOrderTotal()
  {
    return orderTotal;
  }

  public void setOrderTotal(double newOrderTotal)
  {
    orderTotal = newOrderTotal;
  }

  public String getCreditCardMonth()
  {
    return creditCardMonth;
  }

  public void setCreditCardMonth(String newCreditCardMonth)
  {
    creditCardMonth = newCreditCardMonth;
    updateExpirationDate();
  }

  public String getCreditCardYear()
  {
    return creditCardYear;
  }

  public void setCreditCardYear(String newCreditCardYear)
  {
    creditCardYear = newCreditCardYear;
    updateExpirationDate();
  }

  public String getMembershipID()
  {
    return membershipID;
  }

  public void setMembershipID(String newMembershipID)
  {
    membershipID = newMembershipID;
  }

  public String getMembershipFirstName()
  {
    return membershipFirstName;
  }

  public void setMembershipFirstName(String newMembershipFirstName)
  {
    membershipFirstName = newMembershipFirstName;
  }

  public String getMembershipLastName()
  {
    return membershipLastName;
  }

  public void setMembershipLastName(String newMembershipLastName)
  {
    membershipLastName = newMembershipLastName;
  }

  /* This method updates the expiration date whenever the credit card month
   * or date are changed. */
   private void updateExpirationDate()
   {
     if(creditCardMonth != null && creditCardYear != null)
     {
       expirationDate = creditCardMonth + "/01/" + creditCardYear;
     }

   }

  public String getMembershipType()
  {
    return MembershipType;
  }

  public void setMembershipType(String newMemershipType)
  {
    MembershipType = newMemershipType;
  }

  public String getAllowMembershipInfo()
  {
    return allowMembershipInfo;
  }

  public void setAllowMembershipInfo(String newAllowMembershipInfo)
  {
    allowMembershipInfo = newAllowMembershipInfo;
  }

  public boolean isSourceCodeValidated()
  {
    return SourceCodeValidated;
  }

  public void setSourceCodeValidated(boolean newSourceCodeValidated)
  {
    SourceCodeValidated = newSourceCodeValidated;
  }

  /* Update month and year values*/
  private void updateMonthYear()
  {
     SimpleDateFormat dfOutMonth = new SimpleDateFormat("MM");
     SimpleDateFormat dfOutYear = new SimpleDateFormat("yy");

     try{
       Date expDate = CommonUtility.formatStringToUtilDate(expirationDate);
       creditCardMonth = dfOutMonth.format(expDate);    
       creditCardYear = dfOutYear.format(expDate);    
     }
     catch(Exception e)
     {
       //clear otu month and date
       creditCardMonth = "";    
       creditCardYear = "";           
     }
  }

  public String getPaymentMethod()
  {
    return paymentMethodId;
  }

  public void setPaymentMethod(String newPaymentMethodId)
  {
    paymentMethodId = newPaymentMethodId;
  }

  public String getMasterOrderNumber()
  {
    return MasterOrderNumber;
  }

  public void setMasterOrderNumber(String newMasterOrderNumber)
  {
    MasterOrderNumber = newMasterOrderNumber;
  }


  public void setCsrId(String csrId)
  {
    this.csrId = csrId;
  }


  public String getCsrId()
  {
    return csrId;
  }

}