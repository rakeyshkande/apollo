package com.ftd.osp.bulkload.utilities;

/** This class defines a bulk order exception. 
 *  These exceptions are bad news, but not so bad that
 *  we want to hide from the user using a generic page.
 *  When these exceptions occur we display the reason for the error.
 *  @author Ed Mueller */
public class BulkOrderException extends Exception 
{

  public BulkOrderException()
  {
    super();
  }
  
  public BulkOrderException(String e)
  {
    super(e);
  }
}