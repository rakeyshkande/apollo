package com.ftd.osp.bulkload.utilities;

import java.util.Date;
import java.util.Calendar;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.rmi.server.UID;
import java.util.Map;
import java.util.Iterator;
import java.util.List;

/* This class contains common utility methods. 
 * @author Ed Mueller */
public class CommonUtility 
{
  private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.utilities.CommonUtility";
  
    /** This method check a string for a null value.  If the value is null
     * the method returns an empty string, otherwise the value is returned as is.
     *   @param String to check for null
     *   @return  String "" if null, else the passed in string */
    public static String formatNull(String s){
             return s==null ? "" : s;
    }


    /** This method obtains a string value from a Map.  If the value in the
     *  map is null then an empty string is returned.
     *  @param Map the map to get the value from
     *  @param String the key used to get the value from the Map
     *  @return String the map value.  Empty String returned if Map value is null */
    public static String getParam(Map map, String param)
    {
      String value = (String)map.get(param);
      return value == null ? "" : value;
    }        

    /** This method obtains a double value from a Map.  If the value in the
     *  map is null then zero is returned.
     *  @param Map the map to get the value from
     *  @param String the key used to get the value from the Map
     *  @return String the map value.  Empty String returned if Map value is null */
    public static double getParamDouble(Map map, String param)
    {
      double returnValue = 0.00;
      String value = (String)map.get(param);
      if(value != null && value.length() > 0)
      {
        returnValue = new Double(value).doubleValue();
      }

      return returnValue;
    }        


    
  /** This method converts a string into a boolean value. 
   *  Values of 'true' or 'on' will be returned as boolean true, all other
   *  values will cause the method to return a boolean false.
   *   @param String. Values of 'true' or 'on' will be returned as boolean true.        
   *   @return boolean */
     public static boolean stringToBoolean(String value){
        return (value != null && (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("on"))) ? true : false;
        }



    /** This method returns true of the passed in value 
     *  is empty or null. 
     *  @param String value to check
     *  @boolean true if value is empty or null*/
    public static boolean isEmpty(String value)
    {
      return value == null || value.trim().length() == 0 ? true : false;
      
    }    


/**
  * A unitity method to generate a unique 10-digit number
  * @return long a 10-digit unique number
  */
    public static long getUniqueNumber()
    {
        UID uid = new UID();
        long hashCode = Math.abs( uid.hashCode() );
        return  (hashCode < (long)1000000000) ? ( hashCode + (long)1000000000 ) : hashCode;
    }

/**
 * Description:   From Phase 1 - wbet delivery.
 *                Replaces ALL occurences of a string(what) in a string(source)
 *                with a given string (with).
 * @param String source
 * @param String what to replace
 * @param String what to replace with
 * @return java.lang.String
 */
 public static String replaceAll(String source, String what, String with)
	{
		String strReturn = null;

		strReturn = formatNull(source);
		strReturn = formatNull(strReturn);
		int index = -1;

		if (with == null) {
			return strReturn;
		}

		index = strReturn.indexOf (what);
		while (index >=0)
			{
				strReturn = strReturn.substring (0,index) + with + strReturn.substring (index + what.length (),strReturn.length ());
				index = strReturn.indexOf (what,index + with.length ());
			}

		return strReturn;
	}

/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				nd converts it to a Util date of yyyy-mm-dd.
 * @param String data string
 * @return java.sql.Date  (returns null if invalid date)
 * @throws ParseException
 */
public static java.util.Date formatStringToUtilDate(String strDate) 
                throws ParseException{

    java.util.Date utilDate = null;
    String inDateFormat = "";
    int dateLength = 0;
    int firstSep = 0;
    int lastSep = 0;

    if ((strDate != null) && (!strDate.trim().equals(""))) {

		    // set input date format
		    dateLength = strDate.length();
		    int firstTimeSep = strDate.indexOf(":");
		    int firstDashSep = strDate.indexOf("-");
        if(dateLength > 20){
            inDateFormat = "EEE MMM dd HH:mm:ss zzz yyyy";               
        }        
		    else if ( firstTimeSep > 0) {
			    inDateFormat = "yyyy-MM-dd hh:mm:ss";
		    } 
        else if(firstDashSep > 0){
			    int lastDashSep = strDate.lastIndexOf("-");          
          //check how many in years
          if(strDate.length() - lastDashSep == 4)
          {//4 digit year
            if(lastDashSep - firstDashSep ==3)
            {
              //abbreviated month
              inDateFormat = "dd-MMM-yyyy";
            }
            else
            {
              //non-abbreviated month
              inDateFormat = "dd-MMMMMM-yyyy";              
            }            
          }
          else
          {//2 digit year
            if(lastDashSep - firstDashSep ==3)
            {
              //abbreviated month
              inDateFormat = "dd-MMM-yy";
            }
            else
            {
              //non-abbreviated month
              inDateFormat = "dd-MMMMMM-yy";              
            }            
          }
        }
        else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
    		SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat );
		    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

		    java.util.Date date = sdfInput.parse(strDate);
		    String outDateString = sdfOutput.format(date);

			// now that we have no errors, use the string to make a Util date
	 		utilDate = sdfOutput.parse(outDateString);


    }

	return utilDate;
}



/**
 * Description: Takes a Util Date in the format yyyy-mm-dd and formats it
 *              as MM/dd/yyyy (which is the format we show in the screens).
 *
 * @param java.util.Date date to convert
 * @return String
 * @throws ParseException
 */
public static String formatUtilDateToString(java.util.Date utilDate)
          throws ParseException
{
	String strDate = "";
	String inDateFormat = "";
	java.util.Date newDate = null;

	if (utilDate != null) {
        SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

          newDate = dfOut.parse(dfOut.format(utilDate));
          strDate = dfOut.format(newDate);

    } 
    else {
        strDate = "";
    }

	return strDate;
}    


/**
 * Description: Takes a Util Date in the format yyyy-mm-dd and formats it
 *              as MM/yy (which is the format we show in the screens).
 *
 * @param java.util.Date date to convert
 * @return String
 * @throws ParseException
 */
public static String formatUtilDateToCreditCardString(java.util.Date utilDate)
          throws ParseException
{
	String strDate = "";
	String inDateFormat = "";
	java.util.Date newDate = null;

	if (utilDate != null) {
        SimpleDateFormat dfOut = new SimpleDateFormat("MM/yy");

          newDate = dfOut.parse(dfOut.format(utilDate));
          strDate = dfOut.format(newDate);

    } 
    else {
        strDate = "";
    }

	return strDate;
}  

/** This class returns the request url with all the parameters. 
 *  It is used in the development to aid in making JUnit test cases.
 *  @param HttpServletRequest request object
 *  @return String the request string*/
public static String getEncodedRequestString(HttpServletRequest request)
{
    List items = null;

    String requestParams = "";

    // Create a new file upload handler
    DiskFileUpload upload = new DiskFileUpload();
    
    //Parse the request..this method generates a list of all the items 
    //found in the HTTPServlet Request
    try{
        items = upload.parseRequest(request);    
    }
    catch(Exception e)
    {
      System.out.println("parse exception");
    }
 
    //loop through each item in the request
    Iterator iter = items.iterator();
    List valList = null;
    FileItem theFile = null;
    String x = "";
    String value = "";
    while (iter.hasNext()) {
System.out.println(requestParams);
        //get next item
        FileItem item = (FileItem) iter.next();

        //determine if item is a file or form parameter
        if (item.isFormField()) {
            x = requestParams.equals("") ? "?" : "&";
            requestParams = requestParams + x + item.getFieldName() + "='" + item.getString() + "'";
        } else {
            //save a referance to the file...the line values come in as file
            x = requestParams.equals("") ? "?" : "&";
            requestParams = requestParams + x + "theFile" + "='" + item.getName() + "'";
        }        
    }// end while        

  return requestParams;
}

/** This function removes the time values from a Date.
 * @param Date input date
 * @returns Date date without time*/
public static Date removeTime(Date inDate)
{
      Calendar cal = java.util.Calendar.getInstance();   
      cal.setTime(inDate);

      cal.set(java.util.Calendar.SECOND, 0);
      cal.set(java.util.Calendar.MINUTE, 0);
      cal.set(java.util.Calendar.HOUR_OF_DAY, 0);  
      cal.set(java.util.Calendar.MILLISECOND, 0);  
       

      return cal.getTime();
}

public static String removeNonChars(String value)
{

				char c;
				Character ch = null;

        StringBuffer after = new StringBuffer(value);

				for (int i = 0; i < after.length(); ) {
					c =  after.charAt(i);

					if ( ch.isISOControl(c)) {
//						after.delete(i,i+1);
						after.replace(i,i+1," ");
					} else {
							i++;
					}
				}

        return after.toString();
}

public static void main(String[] args) throws Exception
{
  try{
      String before = "look forward to a successful 2004!  ";
  }catch (Throwable t)
  {
    t.printStackTrace();
  }
}


}