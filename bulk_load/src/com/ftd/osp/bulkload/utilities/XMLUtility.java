package com.ftd.osp.bulkload.utilities;

import com.ftd.ftdutilities.FieldUtils;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.io.IOException;
import com.ftd.osp.utilities.xml.DOMUtil;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.w3c.dom.Node;
import oracle.xml.parser.v2.XMLNode;
import oracle.xml.parser.v2.XSLException;
import java.util.Map;
import java.util.Iterator;
import oracle.xml.parser.v2.XMLDocument;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.FileWriter;
import com.ftd.osp.utilities.plugins.Logger;

/* This class contains utility methods. 
 * @author Ed Mueller */
public class XMLUtility 
{

  private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.utilites.XMLUtility";  

    /** This method writes the passed in XML document to a file as well
     * as System.out.  This method is used for debugging puroses. 
     * @param XMLDocument document to print */
    public static void writeXMLtoFile(XMLDocument xmlDoc) 
    {

            try
            {            
                String resultXML = null;
                StringWriter s = new StringWriter();
                xmlDoc.print(new PrintWriter(s));
                resultXML = s.toString();
                System.out.println("XML:"+resultXML);
                FileWriter fv = new FileWriter("c:\\tmp\\out.xm");
                fv.write(resultXML);
                fv.close();
            }
            catch (Exception e)
            {
               new Logger(LOGGER_CATEGORY).error("Error writing XML to file:" + e);
            }
        }    

    /** This method converts an XML document to a String. 
     * @param XMLDocument document to convert 
     * @return String version of XML */
    public static String convertDocToString(XMLDocument xmlDoc) throws Exception
    {
            String xmlString = "";       
          
            StringWriter s = new StringWriter();
            xmlDoc.print(new PrintWriter(s));
            xmlString = s.toString();

            return xmlString;
        }    



  /** This method creates an XML element using the passed in field name and value.
   *  The passed in value is is appened to the the element as a Text node.
   *  The created element is NOT added to the passed in XML document.
   *  @param XMLDocument the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @param String the text value that should be added to the element
   *  @returns the created Element with the attached Text value */
    public static Element createElement(XMLDocument xmlDoc, String field, String value)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);
            Text textNode = xmlDoc.createTextNode(value);

            //append the text
            fieldElement.appendChild(textNode);

            return fieldElement;      
    }    

  /** This method creates an XML element using the passed in field name and value.
   *  The passed in value is is appened to the the element as a Text node.
   *  The created element is NOT added to the passed in XML document.
   *  @param XMLDocument the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @param double the field value that should be added to the element
   *  @returns the created Element with the attached Text value */
    public static Element createElement(XMLDocument xmlDoc, String field, double value)
    {
      return createElement(xmlDoc,field,Double.toString(value));
    }
  /** This method creates an XML element using the passed in field name.
   *  A value\textnode is not appended to this element.
   *  The created element is NOT added to the passed in XML document.
   *  @param XMLDocument the document to which this element will be part of
   *  @param String field the name of the element to be added
   *  @returns the created Element with the attached Text value */
    public static Element createElement(XMLDocument xmlDoc, String field)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);

            return fieldElement;      
    }    

   /** This methods creates an XML Element and ADDS it to the XML document.
     * @param XMLDocument the document to add the element to
     * @param Element the parent element to which this node should be added
     * @param String field the field\element name to add
     * @param String value the value\textnode appened to the Element
     * @param Map map of attributes to add to the element */
    public static void addField(XMLDocument xmlDoc, Element parent,String field, String value, Map attributes)
    {
            //build the field
            Element fieldElement = xmlDoc.createElement(field);

            //if a values was passed in
            Text textNode = null;
            if(value != null){
              textNode = xmlDoc.createTextNode(value);
            }

            //append the attributes if any were passed in
            if(attributes != null){
               Iterator keyIterator = attributes.keySet().iterator();
               while(keyIterator.hasNext())
                      {
                          String key = (String)keyIterator.next();
                          fieldElement.setAttribute(key, (String)attributes.get(key));
                      }
            }

            //if a values was passed in append text node
            if(value != null){
                fieldElement.appendChild(textNode);
            }

            //if parent was passed append under parent, else put on doc root
            if(parent == null){
              xmlDoc.getDocumentElement().appendChild(fieldElement);
            }
            else{
              parent.appendChild(fieldElement);
            }

    }   

   /** This methods creates an XML Element and ADDS it to the XML document.
     * @param XMLDocument the document to add the element to
     * @param String field the field\element name to add
     * @param Map map of attributes to add to the element */
    public static void addField(XMLDocument xmlDoc, String field, Map attributes)
    {
      addField(xmlDoc,null,field,null,attributes);
    }

   /** This methods creates an XML Element and ADDS it to the XML document.
     * @param XMLDocument the document to add the element to
     * @param String field the field\element name to add
     * @param String value the value\textnode appened to the Element  */
    public static void addField(XMLDocument xmlDoc, String field, String value)
    {
            addField(xmlDoc, null,field, value, null);
    }       

   /** This methods creates an XML Element and ADDS it to the XML document.
     * @param XMLDocument the document to add the element to
     * @param Element the parent element to which this node should be added
     * @param String field the field\element name to add
     * @param String value the value\textnode appened to the Element
     * @param Map map of attributes to add to the element */
    public static void addField(XMLDocument xmlDoc, String field, String value, Map attributes)
    {
      addField(xmlDoc, null,field, value, attributes);
    }

   /** This methods creates an XML Element and ADDS it to the XML document.
     * @param XMLDocument the document to add the element to
     * @param Element the parent element to which this node should be added
     * @param String field the field\element name to add
     * @param String value the value\textnode appened to the Element
     * @param Map map of attributes to add to the element */
    public static void addField(XMLDocument xmlDoc, Element parent, String field, Map attributes)
    {
      addField(xmlDoc, parent,field, null, attributes);
    }
    

    /** Creates a new XML document.
     * @param String root element name
     * @return XMLDocument a new document 
     * @throws IOException
     * @thorws SAXException
     * @thorws ParserConfigurationException*/
    public static XMLDocument createXMLDocument(String type)
        throws IOException, SAXException,ParserConfigurationException
    {

        return (XMLDocument) DOMUtil.getDocument();
    }


/** This method returns the text of a given XML element.
 *  @param XMLNocde Starting to node from which to find the nodename
 *  @param String nodeName name from which to get the value
 *  @returns String text value in node 
 *  @throws XSLException*/
public static String getSingleNode(XMLNode xmlNode, String nodeName)
    throws XSLException
{
    Node itemNode = null;
    String nodeValue = "";
    
    itemNode =  xmlNode.selectSingleNode(nodeName);
    if(itemNode != null)
    {
      itemNode = xmlNode.selectSingleNode(nodeName);
      nodeValue = itemNode.getNodeValue();
    } 



 
  return nodeValue; 
}

    /** This method adds a section to the passed in document.
     *  This method was copied from the Order Entry Phase 1 framework
     *  @param XMLDocument document to add section to
     *  @param String element name to create
     *  @param String entry name
     *  @HashMap attributes */
    public static void addSection(XMLDocument document, String elementName, String entry, HashMap values)
    {
        Element element = document.createElement(elementName);
        Element tmpElement = null;
        String key = null;
        for(Iterator keyIterator = values.keySet().iterator(); keyIterator.hasNext(); element.appendChild(tmpElement))
        {
            tmpElement = document.createElement(entry);
            key = (String)keyIterator.next();
            tmpElement.setAttribute("name", key);
            tmpElement.setAttribute("value", (String)values.get(key));
        }

        document.getDocumentElement().appendChild(element);
    }

    /** This method add a list of nodes to an xml document.
     *  This method was copied from the Order Entry Phase 1 framework
     *  @param XMLDocument document
     *  @param NodeList list of nodes to add */
    public static void addSection(XMLDocument document, NodeList nodeList)
    {
        for(int i = 0; i < nodeList.getLength(); i++)
            document.getDocumentElement().appendChild(document.importNode(nodeList.item(i), true));

    }

  /** This method returns an attribute value for the passed in XPath element
   * and the passed in attribute name.
   * @param XMLDocument document to get data from
   * @param String Xpath
   * @param String attribute name
   * @return String attribute value (null if not found)*/
   public static String getAttributeValue(XMLDocument doc, String xpath, String attribute)
   {   
        String value = null;

        try{
              XPathQuery q = new XPathQuery();
              NodeList nl = q.query(doc, xpath);
              if (nl.getLength() > 0)
              {
                  Element data = (Element) nl.item(0);
                  value = data.getAttribute(attribute);
              }        
        }
        catch(Exception e)
        {
            //could not locate xpath query..null will be returned
        }
        return value;
   }


  /**
   * This method descodes certain values in an XMLDocument.  This is done because the DB
   * objects return the XML values encoded and they values need to be 
   * decoded when they are displayed on the XSL page.  This is important 
   * when displaying fields like product description which may have contain
   * symbols like the register trademark.  
   * 
   * @param String before
   * @return String de-coded value
   */
  public static XMLDocument fixXML(XMLDocument xml) throws Exception
  {

    XMLDocument fixed = xml;
    NodeList nl = xml.selectNodes("//order");
    for ( int i = 0; i < nl.getLength(); i++ )
    {
        Element element = (Element) nl.item(i);
        String before = element.getAttribute("first_name");
        element.setAttribute("first_name",fixHTMLEncoding(before));
        before = element.getAttribute("last_name");
        element.setAttribute("last_name",fixHTMLEncoding(before));
        before = element.getAttribute("address_etc");
        element.setAttribute("address_etc",fixHTMLEncoding(before));
    }

  


    return fixed;
  }

  /**
   * This method decodes the XML encoding.  This is done because the DB
   * objects return the XML values encoded and they values need to be 
   * decoded when they are displayed on the XSL page.  This is important 
   * when displaying fields like product description which may have contain
   * symbols like the register trademark.
   * 
   * @param String before
   * @return String de-coded value
   */
  public static String fixHTMLEncoding(String before)
  {
    //System.out.println("-----------------------------------------------------------------------");
    //System.out.println(before);
    String fixed = FieldUtils.replaceAll(before,"&lt;","<");    
    fixed = FieldUtils.replaceAll(fixed,"&gt;",">");        
    fixed = FieldUtils.replaceAll(fixed,"&amp;reg;","&reg;");        
    fixed = FieldUtils.replaceAll(fixed,"&#39;","'");        
    fixed = FieldUtils.replaceAll(fixed,"&amp;","&");        
    //System.out.println(fixed);
    //System.out.println("-----------------------------------------------------------------------");
    return fixed;
  }


   
}