package com.ftd.osp.bulkload.utilities;

/** This class defined an HTTP Exception. 
 *  @author York Davis */
public class HTTPProxyException extends Exception 
{

  public HTTPProxyException()
  {
    super();
  }
  
  public HTTPProxyException(String e)
  {
    super(e);
  }
}