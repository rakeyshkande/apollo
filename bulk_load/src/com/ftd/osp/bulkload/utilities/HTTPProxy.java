package com.ftd.osp.bulkload.utilities;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The purpose of this class is to transmit the passed in data
 * to the passed in URL.
 * @author York Davis
 */
public class HTTPProxy 
{



/**
 * Send the passed in data to the passed in URL
 * @params String url to send data to
 * @params Map containing the parameter and paramerter values to po to URL
 * @params String Content Type * 
 */
public static String send(String urlString, Map params, String contentType) 
        throws HTTPProxyException{

        URLConnection con = null;
        boolean firstx      = true;
        
        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection(); 
            con.setRequestProperty("Content-Type", " " + contentType);
            con.setDoOutput(true); 

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }
            
                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());                
            out.flush();
            out.close();

        	InputStream is = con.getInputStream();

            //8/21/03...add this so the response would be returned
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();
        
        }          
         catch (java.net.MalformedURLException e)  {
                throw new HTTPProxyException(e.toString());
        }
         catch (java.io.IOException e)  {
                throw new HTTPProxyException(e.toString());
        }
        finally{
            con = null;
        }
    
    }//end, send

/**
 * Send the passed in data to the passed in URL
 * @params String url to send data to
 * @params ArrayList containing the values to pass to URL
 * @params String Content Type
 */
public static void send(String urlString, List params, String contentType) 
        throws HTTPProxyException{

        URLConnection con = null;
        boolean firstx      = true;
        
        try {
            URL url = new URL(urlString);

            //Send data
            Iterator iter = params.listIterator();

            while(iter.hasNext()) {                      

                //create connection to URL
                con = url.openConnection(); 
                con.setRequestProperty("Content-Type", " " + contentType);
                con.setDoOutput(true); 

                OutputStream out = con.getOutputStream();
                out.write(((String)iter.next()).getBytes());                
                out.flush();
                out.close();

              	InputStream is = con.getInputStream();                


                con = null;
            }

                
        }          
         catch (java.net.MalformedURLException e)  {
                throw new HTTPProxyException(e.toString());
        }
         catch (java.io.IOException e)  {
                throw new HTTPProxyException(e.toString());
        }
        finally{
            con = null;
        }
    
    }//end, send


/**
 * Send the passed in data to the passed in URL
 * @params String url to send data to
 * @params String containing the value to pass to URL
 * @params String Content Type
 */
public static void send(String urlString, String data, String contentType) 
        throws HTTPProxyException{
        ArrayList dataArray = new ArrayList();
        dataArray.add(data);
        send(urlString,dataArray,contentType);
        }

/**
 * Send the passed in date to the passed in URL.  This method
 * takes in a single value an parameter name.
 * @params String url to send data to
 * @params String parameter name
 * @params String data to be sent
 * @params String content Type
 * @returns String HTTP Response
 * 
 * Created 9/4/03 Ed Mueller
 */
  public String send(String url, String parameter, String data, String contentType) throws HTTPProxyException
  {
      HashMap map = new HashMap();
      map.put(parameter,data);
      return send(url,map,contentType);
  }

  /**
   * Get connection to URL. Construct request and get back a response.
   */
	public synchronized String send(String urlString, Map params)
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;

        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection();
            con.setDoOutput(true);

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }

                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());
            out.flush();
            out.close();

            InputStream is = con.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();

        }
         catch (java.net.MalformedURLException e)  {
                e.printStackTrace();
                throw e;
        }
         catch (java.io.IOException e)  {
                e.printStackTrace();
                throw e;
        }
        finally{
            con = null;
        }

    }//end, send

public static void main(String[] args)
{
          Map map = new HashMap();
          map.put("Order","GETTHIS?");
try{
  String url = "http://192.168.111.145:8988/OSP/OrderGatherer";
  String response = new HTTPProxy().send(url,map);
  System.out.println("::"+response);
}
  catch(Throwable t)
  {
    System.out.println(t);
  }
}

}//end class