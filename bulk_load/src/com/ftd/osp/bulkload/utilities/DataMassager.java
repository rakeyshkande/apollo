package com.ftd.osp.bulkload.utilities;

import com.ftd.ftdutilities.DeliveryDateUTIL;
import com.ftd.ftdutilities.FieldUtils;
import com.ftd.ftdutilities.GeneralConstants;
import com.ftd.ftdutilities.OEDeliveryDate;
import com.ftd.ftdutilities.OEDeliveryDateParm;
import com.ftd.ftdutilities.ShippingMethod;
import com.ftd.ftdutilities.ValidateMembership;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.order.RecalculateException;
import com.ftd.osp.utilities.order.RecalculateOrderBO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.CharacterIterator;
import java.text.ParseException;
import java.text.StringCharacterIterator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import oracle.xml.parser.v2.XSLException;

import org.xml.sax.SAXException;

/**
 * This object was copied from the order Validator package.  Bulk went into
 * product before the DataMasager was ready for product, for that reason this
 * object had to be copied into this projecct.  This object will not be used when
 * the order scrub project goes into product.
 * @deprecated
 */
public class DataMassager implements BulkOrderConstants
{
    private static final String METADATA_CONFIG_FILE_NAME = "metadata_config.xml";
    private static final String GET_HP_PRODUCT_ID = "GET_HP_PRODUCT_ID";
    private static final String GET_OCCASION_LIST = "GET_OCCASION_LIST";
    private static final String GET_PRODUCT_DETAILS = "GET PRODUCT DETAILS";
    private static final String GET_COLORS = "GET_COLORS";    
    private static final String GET_COUNTRY_LIST = "GET_COUNTRY_LIST";
    private static final String GET_STATE_LIST = "GET_STATE_LIST";
    private static final String GET_PRODUCT_SUBCODE = "GET_PRODUCT_SUBCODE";
    private static final String GET_ADDRESS_TYPES = "GET_ADDRESS_TYPES";

    private static final String PRODUCT_ID = "PRODUCT_ID";

    private static Logger logger= new Logger("com.ftd.osp.ordervalidator.util.DataMassager");

    /**
     * Massage order
     * @deprecated
     * 
     * Use massager found in OrderValidator package.
     */
    public static OrderVO massageOrderData(OrderVO order, Connection con) throws IOException, SAXException,
            ParserConfigurationException, TransformerException, XSLException, SQLException,
            ParseException, Exception
    {
        // Get metadata
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String originNovator = "NOTUSED";
        String originBulk = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, BULK_ORDER_ORGIN_PROPERTY);
        String canadaCountryCode = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, COUNTRY_CODE_CANADA_PROPERTY);        
        String usCountryCode = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, COUNTRY_CODE_UNITED_STATES_PROPERTY);
        String paymentTypeGC = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, PAYMENT_TYPE_GIFT_CERT_PROPERTY);
        String paymentTypeCC = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, CREDIT_CARD_TYPE_PROPERTY);
        String addressTypeB = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, "ADDRESS_TYPE_BUSINESS");
        String addressTypeO = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, "ADDRESS_TYPE_OTHER");
        String addressTypeR = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE, "ADDRESS_TYPE_RESIDENTIAL");
        
        // BEGIN Convert novator product ID to HP product ID
        Iterator it = order.getOrderDetail().iterator();
        OrderDetailsVO item = null;
        String hpProdId = null;
        DataAccessUtil dau = DataAccessUtil.getInstance();
        DataRequest dataRequest = new DataRequest();
        dataRequest.setConnection(con);
        CachedResultSet rs = null;
        BuyerVO buyer = null;
        RecipientsVO recipient = null;
        RecipientAddressesVO recipAddress = null;

        if(order.getOrderOrigin() != null && order.getOrderOrigin().equals(originNovator))
        {
            while(it.hasNext())
            {
                item = (OrderDetailsVO)it.next();

                // Get HP product ID
                dataRequest.setStatementID(GET_HP_PRODUCT_ID);
                dataRequest.addInputParam(PRODUCT_ID, item.getProductId());
                rs = (CachedResultSet)dau.execute(dataRequest);
                dataRequest.reset();

                while(rs.next())
                {
                    hpProdId = (String)rs.getObject(1);
                }

                // Set HP product ID
                if(hpProdId != null)
                {
                    item.setProductId(hpProdId);
                }
            }
        }
        // END Convert novator product ID to HP product ID

        // BEGIN Convert occasion name to id
        String occasionId = null;
        Map occasionMap = new HashMap();
        dataRequest.setStatementID(GET_OCCASION_LIST);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        Object occasionCode = null;
        String occasionDesc = null;
        Iterator occasionIt = null;
        String desc = null;
        String code = null;
        while(rs.next())
        {
            occasionCode = rs.getObject(1);
            occasionDesc = (String)rs.getObject(2);

            if(occasionCode != null)
            {
                occasionMap.put(occasionCode.toString(), occasionDesc);
            }
        }
        
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            occasionId = item.getOccassionId();
            if(occasionId == null) occasionId = "";
            occasionId = occasionId.toUpperCase();

            occasionIt = occasionMap.keySet().iterator();
            while(occasionIt.hasNext())
            {
                code = (String)occasionIt.next();
                desc = ((String)occasionMap.get(code)).toUpperCase();

                if(desc.equals(occasionId))
                {
                    item.setOccassionId(code);
                    break;
                }
            }



            
        }
        // END Convert occasion name to id

        // BEGIN remove dashes from phone numbers and for recipient phones also
        // convert none, n/a, na, unknown to 10 zeros
        // Buyer phone numbers
        buyer = null;
        String phoneNumber = null;
        if(order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            // Get the buyer phone numbers
            BuyerPhonesVO buyerPhone = null;
            phoneNumber = null;

            if(buyer.getBuyerPhones() != null)
            {
                for(int i = 0; i < buyer.getBuyerPhones().size(); i++)
                {
                    buyerPhone = (BuyerPhonesVO)buyer.getBuyerPhones().get(i);

                    phoneNumber = buyerPhone.getPhoneNumber();
                    phoneNumber = FieldUtils.replaceAll(phoneNumber, "-", "");
                    buyerPhone.setPhoneNumber(phoneNumber);

                    // Default to one space if blank
                    if(buyerPhone.getPhoneNumber().length() == 0)
                    {
                        buyerPhone.setPhoneNumber(" ");
                    }
                }
            }
        }

        // Recipient phone numbers        
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            if(item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                // Get the buyer phone numbers
                RecipientPhonesVO recipPhone = null;

                if(recipient.getRecipientPhones() != null)
                {
                    for(int i = 0; i < recipient.getRecipientPhones().size(); i++)
                    {
                        recipPhone = (RecipientPhonesVO)recipient.getRecipientPhones().get(i);

                        phoneNumber = recipPhone.getPhoneNumber();
                        if(phoneNumber == null) phoneNumber = "";
                        phoneNumber = FieldUtils.replaceAll(phoneNumber, "-", "");

                        // Convert none, n/a, na, unknown to 10 zeros
                        if(phoneNumber.toUpperCase().indexOf("NONE") != -1 || phoneNumber.toUpperCase().indexOf("N/A") != -1 ||
                           phoneNumber.toUpperCase().indexOf("NA") != -1 || phoneNumber.toUpperCase().indexOf("UNKNOWN") != -1)
                        {
                            phoneNumber = "0000000000";
                        }
                     
                        recipPhone.setPhoneNumber(phoneNumber);

                        // Default to one space if blank
                        if(recipPhone.getPhoneNumber().length() == 0)
                        {
                            recipPhone.setPhoneNumber(" ");
                        }
                    }
                }
            }
        }
        // END remove dashes from phone numbers


        // BEGIN remove dashes from zip codes
        // Buyer zip codes
        BuyerAddressesVO buyerAddress = null;
        String zipCode = null;
        if(order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);

                zipCode = buyerAddress.getPostalCode();
                zipCode = FieldUtils.replaceAll(zipCode, "-", "");
                buyerAddress.setPostalCode(zipCode);
            }
        }

        // Recipient zip codes and default address type
        recipAddress = null;
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                    zipCode = recipAddress.getPostalCode();
                    zipCode = FieldUtils.replaceAll(zipCode, "-", "");
                    recipAddress.setPostalCode(zipCode);

                    // set default address type if not set
                    if(recipAddress.getAddressType() == null)
                    {
                        recipAddress.setAddressType(addressTypeR);
                    }
                }
            }
        }
        // END remove dashes from zip codes


        // BEGIN Convert country to a country code
        // Get the country list
        dataRequest.setConnection(con);
        dataRequest.setStatementID(GET_COUNTRY_LIST);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        Map countryMap = new HashMap();
        String countryId = null;
        String countryDesc = null;
        while(rs.next())
        {
            countryId = (String)rs.getObject(1);
            countryDesc = (String)rs.getObject(2);
            
            if(countryId != null)
            {
                countryId = countryId.toUpperCase();
                countryDesc = countryDesc.toUpperCase();
                countryMap.put(countryDesc, countryId);
            }
        }
        
        String country = null;        
        String countryCode = null;
        boolean foundCode = false;
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                    country = recipAddress.getCountry();
                    if(country == null) country = "";
                    country = country.toUpperCase();
                    
                    if(country.length() == 2)
                    {
                        // Check to make sure the code exists
                        if(!countryMap.containsValue(country))
                        {
                            // If not found then set to blank String
                            recipAddress.setCountry("");
                        }
                    }
                    else
                    {
                        // Try converting to a code
                        countryCode = (String)countryMap.get(country);
                        if(countryCode == null) countryCode = "";

                        recipAddress.setCountry(countryCode);
                    }
                }
            }
        }
        // END Convert country to a country code


        // BEGIN Convert state to a code if US or CA
        // Get the state list
        dataRequest.setConnection(con);
        dataRequest.setStatementID(GET_STATE_LIST);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        Map stateMap = new HashMap();
        String stateId = null;
        String stateDesc = null;
        while(rs.next())
        {
            stateId = (String)rs.getObject(1);
            stateDesc = (String)rs.getObject(2);

            if(stateId != null)
            {
                stateId = stateId.toUpperCase();
                stateDesc = stateDesc.toUpperCase();
                stateMap.put(stateDesc, stateId);
            }
        }

        String state = null;
        String stateCode = null;
        buyerAddress = null;
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);

                country = buyerAddress.getCountry();
                if(country == null) country = "";

                if(country.equals(canadaCountryCode) || country.equals(usCountryCode))
                {
                    state = buyerAddress.getStateProv();
                    if(state == null) state = "";

                    state = state.toUpperCase();
                        
                    if(state.length() == 2)
                    {
                     // Check to make sure the code exists
                        if(!stateMap.containsValue(state))
                        {
                            // If not found then set to blank String
                            buyerAddress.setStateProv("NA");
                        }
                    }
                    else
                    {
                        // Try converting to a code
                        stateCode = (String)stateMap.get(state);
                        if(stateCode == null) stateCode = "NA";

                        buyerAddress.setStateProv(stateCode);
                    }
                }
            }
        }
        
        state = null;
        stateCode = null;
        recipAddress = null;
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            if(item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);

                    country = recipAddress.getCountry();
                    if(country == null) country = "";

                    if(country.equals(canadaCountryCode) || country.equals(usCountryCode))
                    {
                        state = recipAddress.getStateProvince();
                        if(state == null) state = "";

                        state = state.toUpperCase();
                        
                        if(state.length() == 2)
                        {
                         // Check to make sure the code exists
                            if(!stateMap.containsValue(state))
                            {
                                // If not found then set to blank String
                                recipAddress.setStateProvince("NA");
                            }
                        }
                        else
                        {
                            // Try converting to a code
                            stateCode = (String)stateMap.get(state);
                            if(stateCode == null) stateCode = "NA";

                            recipAddress.setStateProvince(stateCode);
                        }
                    }
                }
            }
        }     
        // END Convert state to a code if US or CA
        

        // BEGIN translation of colors to codes
        // Get all colors for comparison
        dataRequest.setConnection(con);
        dataRequest.setStatementID(GET_COLORS);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();
        Map colorMap = new HashMap();
        String colorCode = null;
        String colorDesc = null;
        while(rs.next())
        {
            colorCode = (String)rs.getObject(1);
            colorDesc = (String)rs.getObject(2);

            colorMap.put(colorDesc, colorCode);
        }
        
        it = order.getOrderDetail().iterator();
        String color1 = null;
        String color2 = null;
        String colorCode1 = null;
        String colorCode2 = null;
        
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            color1 = item.getColorFirstChoice();
            if(color1 == null) color1 = "";
            color1 = color1.toUpperCase();

            color2 = item.getColorFirstChoice();
            if(color2 == null) color2 = "";
            color2 = color2.toUpperCase();

            if(colorMap.containsKey(color1))
            {
                colorCode1 = (String)colorMap.get(color1);
            }
            else if(colorMap.containsValue(color1))
            {
                colorCode1 = color1;
            }

            if(colorMap.containsKey(color2))
            {
                colorCode2 = (String)colorMap.get(color2);
            }
            else if(colorMap.containsValue(color2))
            {
                colorCode2 = color2;
            }

            // If we can't translate the colors into codes then set them to null
            item.setColorFirstChoice(colorCode1);
            item.setColorSecondChoice(colorCode2);
        }   
        // END translation of colors to codes


        // BEGIN Breaking address line one into 2 lines if longer than 30 characters
        // Check buyer address
        String address1 = null;
        String address2 = null;        
        buyer = null;
        buyerAddress = null;
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);

                address1 = buyerAddress.getAddressLine1();
                if(address1 == null) address1 = "";
                address2 = buyerAddress.getAddressLine2();
                if(address2 == null) address2 = "";

                String[] addresses = breakUpAddress(address1, address2);

                buyerAddress.setAddressLine1(addresses[0]);
                buyerAddress.setAddressLine2(addresses[1]);                
            }
        }
        
        // Check recipient address
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();
            if(item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    address1 = recipAddress.getAddressLine1();
                    if(address1 == null) address1 = "";
                    address2 = recipAddress.getAddressLine2();
                    if(address2 == null) address2 = "";

                    String[] addresses = breakUpAddress(address1, address2);

                    recipAddress.setAddressLine1(addresses[0]);
                    recipAddress.setAddressLine2(addresses[1]);
                }
            }
        }
        // END Breaking address line one into 2 lines if longer than 30 characters
        

        DeliveryDateUTIL deliveryDateUtil = new DeliveryDateUTIL();
        String shipDate = "";
        String shipMethod = null;
        String deliveryDate = null;
        it = order.getOrderDetail().iterator();
        String prodSubCodeId = null;
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            // BEGIN Check for this product being a sub-code
            // If this is a subcode then fill in the sub-code ID, Master ID,
            // active flag (status),
            prodSubCodeId = item.getProductSubCodeId();
            if(prodSubCodeId != null && prodSubCodeId.length() > 1)
            {
                prodSubCodeId = item.getProductSubCodeId();
            }
            else
            {
                prodSubCodeId = item.getProductId();
            }    
            
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_PRODUCT_SUBCODE);
            dataRequest.addInputParam(PRODUCT_ID, prodSubCodeId);
            rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();

            String subCodeId = null;
            String masterId = null;
            String activeFlag = null;
            Object price = null;

            while(rs.next())
            {
                subCodeId = (String)rs.getObject(2);
                masterId = (String)rs.getObject(1);
                activeFlag = (String)rs.getObject(7);
                price = rs.getObject(4);
            }

            if(subCodeId != null)
            {
                item.setProductId(masterId);
                item.setProductSubCodeId(subCodeId);
                //item.setProductsAmount(price.toString());
            }
            // END Check for this product being a sub-code

            // Get product details
            dataRequest.setConnection(con);
            dataRequest.setStatementID(GET_PRODUCT_DETAILS);
            dataRequest.addInputParam(PRODUCT_ID, item.getProductId());
            rs = (CachedResultSet)dau.execute(dataRequest);
            dataRequest.reset();

            BigDecimal standardPrice = null;
            BigDecimal deluxePrice = null;
            BigDecimal premiumPrice = null;
            Object standard = null;
            Object deluxe = null;
            Object premium = null;
            String shipMethodFlorist = null;
            String shipMethodCarrier = null;

            while(rs.next())
            {
                standard = rs.getObject(11);
                deluxe = rs.getObject(12);
                premium = rs.getObject(13);

                shipMethodCarrier = (String)rs.getObject(56);
                shipMethodFlorist = (String)rs.getObject(57);
            }

            if(shipMethodCarrier == null) shipMethodCarrier = "N";
            if(shipMethodFlorist == null) shipMethodFlorist = "N";
            
            if(standard == null)
            {
                standardPrice = new BigDecimal("0");
            }
            else
            {
                standardPrice = new BigDecimal(standard.toString());
            }

            if(deluxe == null)
            {
                deluxePrice = new BigDecimal("0");
            }
            else
            {
                deluxePrice = new BigDecimal(deluxe.toString());
            }

            if(premium == null)
            {
                premiumPrice = new BigDecimal("0");
            }
            else
            {
                premiumPrice = new BigDecimal(premium.toString());
            }    

            // If the product is a sub-code then set the standard price
            if(item.getProductSubCodeId() != null && item.getProductSubCodeId().length() > 1)
            {
                if(price == null || Float.parseFloat(price.toString()) == 0)
                {
                    //standardPrice = "0";
                }
                else
                {
                    standardPrice = new BigDecimal(price.toString());
                }
            }

            // BEGIN Lookup of the price type (standard, premuim or deluxe)
            // Also fill in price for bulk orders.  Pick the least expensive option
            if(order.getOrderOrigin() != null && order.getOrderOrigin().equals(originBulk))
            {
                item.setProductsAmount(standardPrice.toString());
            }

            String productsAmountStr = item.getProductsAmount();
            if(productsAmountStr == null) productsAmountStr = "0";
            BigDecimal productsAmount = new BigDecimal(productsAmountStr);

            if(standardPrice != null && standardPrice.compareTo(productsAmount) == 0)
            {
                item.setSizeChoice("A");
            }
            else if(deluxePrice != null && deluxePrice.compareTo(productsAmount) == 0)
            {
                item.setSizeChoice("B");
            }
            else if(premiumPrice != null && premiumPrice.compareTo(productsAmount) == 0)
            {
                item.setSizeChoice("C");
            }
            // END Lookup of the price type (standard, premuim or deluxe)


            // BEGIN Ship date calculation
            // Also calculate the least expensive ship method for bulk orders
            // Setup shipping method objects
            ShippingMethod standardShipping = new ShippingMethod();
            standardShipping.setCode(GeneralConstants.DELIVERY_STANDARD_CODE);
            standardShipping.setDeliveryCharge(new BigDecimal(0));
            standardShipping.setDescription(GeneralConstants.DELIVERY_STANDARD);
            ShippingMethod twoDayShipping = new ShippingMethod();
            twoDayShipping.setCode(GeneralConstants.DELIVERY_TWO_DAY_CODE);
            twoDayShipping.setDeliveryCharge(new BigDecimal(0));
            twoDayShipping.setDescription(GeneralConstants.DELIVERY_TWO_DAY);
            ShippingMethod nextDayShipping = new ShippingMethod();
            nextDayShipping.setCode(GeneralConstants.DELIVERY_NEXT_DAY_CODE);
            nextDayShipping.setDeliveryCharge(new BigDecimal(0));
            nextDayShipping.setDescription(GeneralConstants.DELIVERY_NEXT_DAY);
            ShippingMethod sdShipping = new ShippingMethod();
            sdShipping.setCode(GeneralConstants.DELIVERY_FLORIST_CODE);
            sdShipping.setDeliveryCharge(new BigDecimal(0));
            sdShipping.setDescription(GeneralConstants.DELIVERY_FLORIST);
            ShippingMethod satShipping = new ShippingMethod();
            satShipping.setCode(GeneralConstants.DELIVERY_SATURDAY_CODE);
            satShipping.setDeliveryCharge(new BigDecimal(0));
            satShipping.setDescription(GeneralConstants.DELIVERY_SATURDAY);                        
            
            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses() != null && recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    
                    // For bulk orders also calulate the ship method
                    if(order.getOrderOrigin() != null && order.getOrderOrigin().equals(originBulk))
                    {
                        shipMethod = null;
                        OEDeliveryDateParm parms = null;
                        OEDeliveryDate deliveryDateVO = null;
                        ShippingMethod shippingMethod = null;
                        List shippingMethods = null;
                        
                        // Check for carrier delivered
                        if(shipMethodCarrier.equals("Y"))
                        {                            
                            parms = DeliveryDateUTIL.getParameterData(recipAddress.getCountry(), item.getProductId(), recipAddress.getPostalCode(), canadaCountryCode, usCountryCode, item.getLineNumber(), item.getOccassionId(), con,recipAddress.getStateProvince());

                            parms.setOrder(order);
                                   
                            List dates = new DeliveryDateUTIL().getOrderProductDeliveryDates(parms);

                            // Loop through dates looking for this delivery date
                            boolean foundDate = false;
                            for (int i = 0; i < dates.size(); i++) 
                            {
                                deliveryDateVO = (OEDeliveryDate)dates.get(i);

                                if(deliveryDateVO.getDeliveryDate().equals(item.getDeliveryDate()) && deliveryDateVO.getDeliverableFlag().equals("Y"))
                                {
                                    foundDate = true;
                                    shippingMethods = deliveryDateVO.getShippingMethods();
                                    break;
                                }
                            }
                            
                            if(foundDate)
                            {
                                // Find the cheapest ship method
                                if(shippingMethods != null)
                                {
                                    if(shippingMethods.contains(standardShipping))
                                    {
                                        shipMethod = standardShipping.getCode();
                                    }
                                    else if(shippingMethods.contains(twoDayShipping))
                                    {
                                        shipMethod = twoDayShipping.getCode();
                                    }
                                    else if(shippingMethods.contains(sdShipping))
                                    {
                                        shipMethod = GeneralConstants.DELIVERY_SAME_DAY_CODE;
                                        //shipMethod = null;
                                    }                                                                                                            
                                    else if(shippingMethods.contains(nextDayShipping))
                                    {
                                        shipMethod = nextDayShipping.getCode();
                                    }
                                    else if(shippingMethods.contains(satShipping))
                                    {
                                        shipMethod = satShipping.getCode();
                                    }                                    
                                }                                                                
                            }
                            
                            item.setShipMethod(shipMethod);
                        }
                    }

                    // Only get ship date for carrier delivered
                    if(item.getShipMethod() != null && shipMethodCarrier.equals("Y"))
                    {
                        if(item.getShipMethod().equals(GeneralConstants.DELIVERY_SAME_DAY_CODE))
                        {
                            // For same day florist delivery use the delivery date
                            shipDate = item.getDeliveryDate();                            
                        }
                        else
                        {
                            shipDate = deliveryDateUtil.getShipDate(item.getShipMethod(), item.getDeliveryDate(), recipAddress.getStateProvince(), recipAddress.getCountry(), item.getProductId(), con);
                        }
                    }
                    item.setShipDate(shipDate);
                }
                else
                {
                    item.setShipDate(shipDate);
                }
            }
            else
            {
                item.setShipDate(shipDate);
            }
            // END Ship date calculation            
        }
        

        // BEGIN recalculate prices
        RecalculateOrderBO recalcVO = new RecalculateOrderBO();
        try
        {
            recalcVO.recalculate(con, order);
        }
        catch(RecalculateException rpe)
        {
            logger.error(rpe.getMessage());
        }
        // END recalculate prices

/* Moved to FRP Fixer
        // Set same day ship method (SD) to null
        // becuase we do not store the SD record in the database
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            if(item.getShipMethod() != null && item.getShipMethod().equals(GeneralConstants.DELIVERY_SAME_DAY_CODE))
            {
                item.setShipMethod(null);
            }
        }
*/

        // BEGIN Setting auto hold flag to "N" on all buyers and recipients, the
        // best customer flag and the sender info release flag
        // This is done because auto hold will not be used until phase 3
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            // Check Auto hold
            if(buyer.getAutoHold() == null || buyer.getAutoHold().length() < 1)
            {
                buyer.setAutoHold("N");
            }

            // Check best customer
            if(buyer.getBestCustomer() == null || buyer.getBestCustomer().length() < 1)
            {
                buyer.setBestCustomer("N");
            }
        }

        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            if(item.getSenderInfoRelease() == null)
            {
                item.setSenderInfoRelease("N");
            }

            // Set recipient auto hold flag to "N" if blank or null
            if(item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient != null && (recipient.getAutoHold() == null || recipient.getAutoHold().length() < 1))
                {
                    recipient.setAutoHold("N");
                }
            }
        }
        // END Setting auto hold flag to "N" on all buyers and recipients


        // BEGIN If the CC type is missing and the CC number exists then
        // try to figure out what the type is
        Collection payments = order.getPayments();
        if(payments != null && payments.size() > 0)
        {
            // Get order credit cards
            it = payments.iterator();
            PaymentsVO payment = null;
            CreditCardsVO creditCard = null;
            String ccType = null;
            String ccNumber = null;
            String ccExpiration = null;
            while(it.hasNext())
            {
                payment = (PaymentsVO)it.next();
                if(payment.getPaymentMethodType().equals(paymentTypeCC))
                {
                    creditCard = (CreditCardsVO)payment.getCreditCards().get(0);
                    
                    ccType = creditCard.getCCType();
                    if(ccType == null) ccType = "";
                    ccNumber = creditCard.getCCNumber();
                    if(ccNumber == null) ccNumber = "";

                    if(ccType.length() < 1 && ccNumber.length() > 0)
                    {
                        ccType = findCCType(ccNumber);

                        creditCard.setCCType(ccType);
                    }
                }
            }
        }        
        // END If the CC type is missing and the CC number exists then
        // try to figure out what the type is


        // BEGIN translating ADDRESS TYPE to its code
        dataRequest.setConnection(con);
        dataRequest.setStatementID(GET_ADDRESS_TYPES);
        rs = (CachedResultSet)dau.execute(dataRequest);
        dataRequest.reset();

        Map addressTypes = new HashMap();
        String addressType = null;
        String typeCode = null;
/*
        addressTypes.put("HOME", "R");        
        addressTypes.put("OTHER", "B");
        addressTypes.put("BUSINESS", "B");
        addressTypes.put("HOSPITAL", "H");
        addressTypes.put("FUNERAL HOME", "F");
        addressTypes.put("NURSING HOME", "H");
*/
                    
        while(rs.next())
        {
            addressType = (String)rs.getObject(1);
            typeCode = (String)rs.getObject(2);

            if(addressType != null)
            {
                addressTypes.put(addressType, typeCode);
            }
        }

        // This section converts address type codes to address types so they
        // conform to the database constraints
        String addType = null;
        buyer = null;
        buyerAddress = null;
        Iterator typeIt = null;
        String typeKey = null;
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                addType = buyerAddress.getAddressType();
                if(addType == null) addType = "";
                addType = addType.toUpperCase();

                // Try to convert it if one character
                if(addType.length() == 1)
                {
                    if(addressTypes.containsValue(addType))
                    {
                        typeIt = addressTypes.keySet().iterator();
                        while(typeIt.hasNext())
                        {
                            typeKey = (String)typeIt.next();
                            if(addressTypes.get(typeKey).equals(addType))
                            {
                                buyerAddress.setAddressType(typeKey);
                                break;
                            }
                        }
                    }

                    // If the type is O convert to B
                    if(addType.equals("O"))
                    {
                        buyerAddress.setAddressType(addressTypeB);
                    }                    
                }
                // if longer than one check if it exists.  If does not exist default to home
                else if(!addressTypes.containsKey(addType))
                {
                    buyerAddress.setAddressType(addressTypeR);
                }
            }
        }

        addType = null;
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    addType = recipAddress.getAddressType();
                    if(addType == null) addType = "";
                    addType = addType.toUpperCase();

                    // Try to convert it if one character
                    if(addType.length() == 1)
                    {
                        if(addressTypes.containsValue(addType))
                        {
                            typeIt = addressTypes.keySet().iterator();
                            while(typeIt.hasNext())
                            {
                                typeKey = (String)typeIt.next();
                                if(addressTypes.get(typeKey).equals(addType))
                                {
                                    recipAddress.setAddressType(typeKey);
                                    break;
                                }
                            }
                        }

                        // If the type is O convert to B
                        if(addType.equals("O"))
                        {
                            recipAddress.setAddressType(addressTypeB);
                        }                    
                    }
                    // if longer than one check if it exists.  If does not exist default to home
                    else if(!addressTypes.containsKey(addType))
                    {
                        recipAddress.setAddressType(addressTypeR);
                    }
                }
            }
        }
        
/*
        // Buyer address type
        String addType = null;
        buyer = null;
        buyerAddress = null;
        if(order.getBuyer() != null && order.getBuyer().size() > 0)
        {
            buyer = (BuyerVO)order.getBuyer().get(0);

            if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() > 0)
            {
                buyerAddress = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
                addType = buyerAddress.getAddressType();
                if(addType == null) addType = "";

                // Try to convert it if longer than 1 character
                if(addType.length() > 1)
                {
                    if(addressTypes.containsKey(addType.toUpperCase()))
                    {
                       buyerAddress.setAddressType((String)addressTypes.get(addType.toUpperCase())); 
                    }
                }

                // If the type is O convert to B
                if(buyerAddress.getAddressType() != null && buyerAddress.getAddressType().equals(addressTypeO))
                {
                    buyerAddress.setAddressType(addressTypeB);
                }
            }
        }
        
        // Recipient address type
        addType = null;
        it = order.getOrderDetail().iterator();
        while(it.hasNext())
        {
            item = (OrderDetailsVO)it.next();

            if(item.getRecipients() != null && item.getRecipients().size() > 0)
            {
                recipient = (RecipientsVO)item.getRecipients().get(0);

                if(recipient.getRecipientAddresses().size() > 0)
                {
                    recipAddress = (RecipientAddressesVO)recipient.getRecipientAddresses().get(0);
                    addType = recipAddress.getAddressType();
                    if(addType == null) addType = "";

                    // Try to convert it if longer than 1 character
                    if(addType.length() > 1)
                    {
                        if(addressTypes.containsKey(addType.toUpperCase()))
                        {
                           recipAddress.setAddressType((String)addressTypes.get(addType.toUpperCase())); 
                        }
                    }

                    // If the type is O convert to B
                    if(recipAddress.getAddressType() != null && recipAddress.getAddressType().equals(addressTypeO))
                    {
                        recipAddress.setAddressType(addressTypeB);
                    }
                }
            }
        }
*/        
        // END translating ADDRESS TYPE to its code
        
        return order;
    }

    private static String findCCType(String ccNumber)
    {
        String ccType = null;

        if(ccNumber != null && ccNumber.length() > 0)
        {
            int idBegin = new ValidateMembership().StringToInt(ccNumber.substring(0,2));

            if ((idBegin == 30) | (idBegin == 36) | (idBegin == 38))
                ccType = "DC";

            if ((idBegin == 34) | (idBegin == 37))
                ccType = "AX";

            if ((idBegin >=40) & (idBegin <=49))
                ccType = "VI";

            if ((idBegin >=50) & (idBegin <=59))
                ccType = "MC";

            if ((idBegin >=60) & (idBegin <=69))
                ccType = "DI";
        }
        
        return ccType;
    }

    private static String[] breakUpAddress(String address1, String address2)
    {
        StringCharacterIterator cIt = null;
        String[] addresses = new String[2];
        if(address1 == null) address1 = "";
        if(address2 == null) address2 = "";
        String origAddress1 = address1;

        address1 = address1.trim();
        address2 = address2.trim();
                    
        if(address1.length() > 30)
        {
            // Go back to first space
            int spaceIndex = 30;
            cIt = new StringCharacterIterator(address1);
            cIt.setIndex(30);
            for(char c = cIt.current(); c != CharacterIterator.DONE; c = cIt.previous()) 
            {                            
                if(c == ' ')
                {
                    break;       
                }
                spaceIndex--;
            }

            if(spaceIndex != -1)
            {
                address1 = origAddress1.substring(0, spaceIndex).trim();
                address2 = (origAddress1.substring(spaceIndex) + address2).trim();

                // if address 2 still over 30 then set address to null so this order goes to scrub
                if(address2.length() > 30)
                {
                    //DO NOT CLEAR THIS OUT, LET SCRUB REJECT IT
                    //address1 = null;
                    //address2 = null;
                }
            }
        }

        addresses[0] = address1;
        addresses[1] = address2;

        return addresses;
    }    
}
