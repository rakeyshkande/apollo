package com.ftd.osp.bulkload.utilities;

import java.io.IOException;
import java.text.ParseException;
import oracle.xml.parser.v2.XSLException;
import javax.xml.transform.TransformerException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import com.ftd.osp.utilities.ConfigurationUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ArrayList;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.vo.LineVO;
import com.ftd.osp.bulkload.vo.LineItemVO;

/** This class contains utility methods used to work with an Excel spreadsheet. 
     @author Ed Mueller
     */
public class SpreadSheetUtility implements FieldConstants,BulkOrderConstants
{
    private Logger logger;
    private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.PopupServlet";
    
    private static final String BULK_ORDER_XML_HEADER = "<?xml version='1.0' encoding='utf-8'?>";

    private static final String BAD_FILE_MESSAGE = "bad_file";

    private static final int EXCEL_START_ROW = 1; 
  
    //columns in Excel spreadsheet
    private static final short EXCEL_FIRST_NAME = 0;
    private static final short EXCEL_LAST_NAME = 1;
    private static final short EXCEL_BUSINESS = 2;
    private static final short EXCEL_ADDRESS = 3;
    private static final short EXCEL_CITY = 4;
    private static final short EXCEL_STATE = 5;
    private static final short EXCEL_ZIPCODE = 6;
    private static final short EXCEL_COUNTRY = 7;
    private static final short EXCEL_PHONE_NUMBER = 8;
    private static final short EXCEL_PHONE_EXTENSION = 9;
    private static final short EXCEL_PRODUCT_ID = 10;
    private static final short EXCEL_DELIVERY_DATE = 11;
    private static final short EXCEL_CARD_MESSAGE = 12;
    private static final short EXCEL_CARD_SIGNATURE = 13;
    private static final short MAX_EXCEL_COLUMNS = 12;


    private static final String CELL_ERROR_PROPERTY = "cell_error_message";
   
    /** Public constructor.  
     *  It creates a logger object. */
    public SpreadSheetUtility() {
        logger = new Logger(LOGGER_CATEGORY);
    }

 


  /* This method gets the value from a cell.  If the cell is null then
        an empty string is returned. 
        @param HSSFRow row from which the value should be obtained
        @param short column from which the value should be obtained
        @return String the value 
        @throws BulkOrderException
        @throws IOException could not read from property file
        @throws TransformerException count not read from property file
        @throws SAXException count not read property file
        @throws ParserConfigurationException
        @throws XSLException error reading property file*/
    private String getCellValue(HSSFRow row, short column) 
        throws BulkOrderException,IOException,TransformerException,
              SAXException,ParserConfigurationException, XSLException{
        HSSFCell cell = row.getCell(column);

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        String cellError = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,CELL_ERROR_PROPERTY);

        Logger logger = new Logger(LOGGER_CATEGORY);

        //if cell is null return empty string
        if(cell == null)
        {
          return "";
        }

        //The format value returned for a date is 14
        short EXCEL_DATE_FORMAT = 14;

        String value = "";

        switch(cell.getCellType())
        {
          //type string
          case HSSFCell.CELL_TYPE_STRING :
                value = cell.getStringCellValue();
                break;

          //type string
          case HSSFCell.CELL_TYPE_NUMERIC :
                if(cell.getCellStyle().getDataFormat() == EXCEL_DATE_FORMAT){
                    try{
                        value = CommonUtility.formatUtilDateToString(cell.getDateCellValue());
                    }
                    catch(ParseException pe)
                    {
                        String msg = cellError + " Row:" + row.getRowNum() + " Column:" + column + 1;
                        logger.error(msg);
                        logger.error(pe);
                        throw new BulkOrderException(msg);                        
                    }
                }
                else
                {
                    //treat double as int
                    long dbl = new Double(cell.getNumericCellValue()).longValue();                  
                    value = new Long(dbl).toString();
                    
                }
                break;
          //blank cell                
          case HSSFCell.CELL_TYPE_BLANK :
                value = "";
                break;
          default:
                String msg = cellError + " Row:" + row.getRowNum() + " Column:" + column + 1;
                logger.error(msg);
                logger.error("Cell type=" + cell.getCellType());
                throw new BulkOrderException(msg);
        }
    
        return CommonUtility.removeNonChars(value);
        }

    /* This method is used generated the VO for an order line item.
        @param HSSFRow Excel row 
        @param int line number
        @return LineItemVO line item vo
        @throws BulkOrderException Could not parse file
        @throws IOException Could not obtain an input stream
        @throws SAXException Could not parse property file
        @throws TransformerException Could not parse property file
        @throws ParserConfigurationException
        @throws XSLException
        */
    private LineVO getLine(HSSFRow row,int lineNumber) 
        throws BulkOrderException, IOException, SAXException, 
            TransformerException,ParserConfigurationException,XSLException{

        //declare line vo        
        LineVO line = new LineVO();
        line.setLineNumber(lineNumber);
      
        //build lines
        List lineItems = new ArrayList();
        lineItems.add(new LineItemVO(FIELD_FIRST_NAME,getCellValue(row,EXCEL_FIRST_NAME)));
        lineItems.add(new LineItemVO(FIELD_LAST_NAME,getCellValue(row,EXCEL_LAST_NAME)));
        lineItems.add(new LineItemVO(FIELD_BUSINESS,getCellValue(row,EXCEL_BUSINESS)));
        lineItems.add(new LineItemVO(FIELD_ADDRESS,getCellValue(row,EXCEL_ADDRESS)));
        lineItems.add(new LineItemVO(FIELD_CITY,getCellValue(row,EXCEL_CITY)));
        lineItems.add(new LineItemVO(FIELD_STATE,getCellValue(row,EXCEL_STATE).toUpperCase()));
        lineItems.add(new LineItemVO(FIELD_ZIPCODE,getCellValue(row,EXCEL_ZIPCODE)));
        lineItems.add(new LineItemVO(FIELD_COUNTRY,getCellValue(row,EXCEL_COUNTRY).toUpperCase()));
        lineItems.add(new LineItemVO(FIELD_PHONE_NUMBER,getCellValue(row,EXCEL_PHONE_NUMBER)));
        lineItems.add(new LineItemVO(FIELD_PHONE_EXTENSION,getCellValue(row,EXCEL_PHONE_EXTENSION)));
        lineItems.add(new LineItemVO(FIELD_PRODUCT_ID,getCellValue(row,EXCEL_PRODUCT_ID).toUpperCase()));
        lineItems.add(new LineItemVO(FIELD_CARD_MESSAGE,getCellValue(row,EXCEL_CARD_MESSAGE)));
//        lineItems.add(new LineItemVO(FIELD_CARD_SIGNATURE,getCellValue(row,EXCEL_CARD_SIGNATURE)));
        lineItems.add(new LineItemVO(FIELD_DELIVERY_DATE,getCellValue(row,EXCEL_DELIVERY_DATE)));
       
        line.setLineItems(lineItems);

        return line;
    }


    /* This method determines of the Excel spreedsheet EOF has been reached.
        @param HSSFRow Excel Row
        @return boolean True=EOF
        @throws BulkOrderException Could not parse file
        @throws IOException Could not obtain an input stream
        @throws SAXException Could not parse property file
        @throws TransformerException Could not parse property file
        @throws ParserConfigurationException
        @throws XSLException*/
    private boolean eof(HSSFRow row) 
        throws BulkOrderException, IOException, SAXException, 
            TransformerException,ParserConfigurationException,XSLException{

        //check if there is any date in this row
        StringBuffer sb = new StringBuffer("");

        //if row is not null (null row means eof)
        if(row != null){
                for(short i=0;i< MAX_EXCEL_COLUMNS;i++){
                        HSSFCell cell = row.getCell(i);
                        if (cell != null)
                                sb.append(getCellValue(row,i));
                        }//end for
        }
        
       return sb.toString().equals("") ? true : false;
        }



    /**  This method creates order XML from an Excel spreedsheet & OrderHeaderVO
      @param InputStream spreadsheet to parse
      @return List of LineVOs
      @throws BulkOrderException Could not parse file
      @throws IOException Could not obtain an input stream
      @throws SAXException Could not parse property file
      @throws TransformerException Could not parse property file
      @throws ParserConfigurationException
      @throws XSLException
            */
    public List parse(InputStream spreadSheetStream) 
        throws BulkOrderException, IOException, SAXException, 
            TransformerException,ParserConfigurationException,XSLException{

            //order XML
            String order = "";

            List lineItems = new ArrayList();

            //get data from configuration file
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
            String badFileMsg = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,BAD_FILE_MESSAGE);

             //get a referance to the spreedsheet sheet
            HSSFSheet sheet = null;
            try{
                POIFSFileSystem fs = new POIFSFileSystem(spreadSheetStream);
                HSSFWorkbook wb = new HSSFWorkbook(fs);
                sheet = wb.getSheetAt(0);
            }
            catch(IOException ie)
            {   
                Logger logger = new Logger(LOGGER_CATEGORY);
                logger.error(badFileMsg);
                logger.error(ie);
                throw new BulkOrderException(badFileMsg);                                      
            }
            
            //set row counter
            int rowNumber = EXCEL_START_ROW;

            //get first row in file
            HSSFRow row = sheet.getRow(rowNumber);

            //while not at end of file
            while (!eof(row)){

                    //get vo representation of line item--parse Exel doc
                    lineItems.add(getLine(row,rowNumber));
                    
                    //increment line counter
                    rowNumber++;

                    //get next row
                    row = sheet.getRow(rowNumber);

            }//end while

            
        //return order
        return lineItems;
    }

}