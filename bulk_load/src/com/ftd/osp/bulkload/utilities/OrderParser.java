package com.ftd.osp.bulkload.utilities;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import com.ftd.osp.utilities.order.vo.CreditCardsVO;
import com.ftd.osp.utilities.order.vo.AddOnsVO;
import com.ftd.osp.utilities.order.vo.RecipientsVO;
import com.ftd.osp.utilities.order.vo.MembershipsVO;
import com.ftd.osp.utilities.order.vo.OrderDetailsVO;
import com.ftd.osp.utilities.order.vo.BuyerPhonesVO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.order.vo.CoBrandVO;
import com.ftd.osp.utilities.order.vo.DestinationsVO;
import com.ftd.osp.utilities.order.vo.RecipientPhonesVO;
import com.ftd.osp.utilities.order.vo.BuyerEmailsVO;
import com.ftd.osp.utilities.order.vo.OrderContactInfoVO;
import com.ftd.osp.utilities.order.vo.BuyerAddressesVO;
import com.ftd.osp.utilities.order.vo.PaymentsVO;
import com.ftd.osp.utilities.order.vo.RecipientAddressesVO;
import com.ftd.osp.utilities.order.vo.BuyerVO;
import java.text.ParseException;
import java.util.Date;
import java.io.InputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import com.ftd.osp.utilities.plugins.Logger;
import oracle.xml.parser.v2.XSLException;
import org.xml.sax.SAXException;
import javax.xml.transform.TransformerException;
import com.ftd.osp.utilities.ConfigurationUtil;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import java.util.List;
import java.util.Iterator;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.DefaultFileItemFactory;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileUploadException;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import javax.servlet.http.HttpServletRequest;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import com.ftd.osp.bulkload.dao.LookupDAO;
import java.util.Map;
import java.util.HashMap;
import org.w3c.dom.Element;
import com.ftd.osp.bulkload.vo.LineVO;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.constants.XMLXSLConstants;

/** This class contains functions used to parase\reformat order objects.
 * @author Ed Mueller */
public class OrderParser implements FieldConstants,XMLXSLConstants,BulkOrderConstants
{

  private static final String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.utilities.OrderParser";
  private static final String DOLLAR_AMOUNT_FORMAT = "0.00";
  private static final String PRODUCT_PATH = "/PRODUCTS/PRODUCT";
  private static final String PRODUCT_AMOUNT_ATTRIBUTE = "standard_price";
  private static final String STATE_PATH = "/STATES/STATE";
  private static final String STATE_TAX_RATE = "tax_rate";

  private static final String FOL_VALUE = "FTD";

  private static final String MASTER_ORDER_PREFIX_PROPERTY = "master_order_prefix";
  private static final String ORDER_PREFIX_PROPERTY = "order_prefix";
  private static final String OCCASION_DEFAULT_PROPERTY = "default_occasion";

  private static final String COBRAND_FIRST_NAME = "FNAME";
  private static final String COBRAND_LAST_NAME = "LNAME";
  
   /** This method creates an XML order document from an OrderDocVO
    *  @param OrderDocVO Order document to convert
    *  @return XMLDocument the order document 
    *  @throws SQLException, IOException,ParserConfigurationException,Exception */
    public static XMLDocument getXMLforGatherer(OrderDocVO vo) 
        throws SQLException, IOException, ParserConfigurationException,Exception
    {

    
            //create doc
            XMLDocument xmlDoc = new XMLDocument();
            Element root = xmlDoc.createElement("order");
            xmlDoc.appendChild(root);

            //add the header
            addHeader(xmlDoc,vo);

            //add lines
            addLines(xmlDoc,vo);

            return xmlDoc;
    }



    /* This method adds the header elements to the order document.
     * @param XMLDocument document to which the header is added
     * @param OrderDocVO contains the header data 
     * @throws IOException
     * @throws TransformerException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XSLException
     * @throws ParseException*/
    private static void addHeader(XMLDocument xmlDoc,OrderDocVO orderVO)
        throws IOException,TransformerException,SAXException,
          ParserConfigurationException,XSLException, ParseException
    {

            //get data from configuration file
            ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
            String tranDateFormat = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,TRANS_DATE_FORMAT_PROPERTY);
            String bulkOrgin = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,BULK_ORDER_ORGIN_PROPERTY);
            String masterOrderPrefix = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,MASTER_ORDER_PREFIX_PROPERTY);

            String lineCount = new Integer(orderVO.getLines().size()).toString();
   
            ///get master order number
            String masterOrderNumber = masterOrderPrefix + CommonUtility.getUniqueNumber();
            orderVO.setMasterOrderNumber(masterOrderNumber);

            //get & format transaction date
            SimpleDateFormat sdfTranDate = new SimpleDateFormat(tranDateFormat);
            String transactionDate = sdfTranDate.format(new java.util.Date());

            //attribute list
            Map attributes = new HashMap();

            XMLUtility.writeXMLtoFile(xmlDoc);

            attributes= new HashMap();

            //header
            Element temp = null;
            Element header = XMLUtility.createElement(xmlDoc,"header");

            temp = XMLUtility.createElement(xmlDoc,"master-order-number",masterOrderNumber);
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"source-code",CommonUtility.formatNull(orderVO.getSourceCode()));
            header.appendChild(temp);

           
            temp = XMLUtility.createElement(xmlDoc,"origin",bulkOrgin);
            header.appendChild(temp);

            temp = XMLUtility.createElement(xmlDoc,"co-brand-credit-card-code","N");
            header.appendChild(temp);

                        
            temp = XMLUtility.createElement(xmlDoc,"order-count",lineCount);
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"order-amount","" + orderVO.getOrderTotal());
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"transaction-date",transactionDate);
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"socket-timestamp");
            header.appendChild(temp);

            temp = XMLUtility.createElement(xmlDoc,"contact-first-name",CommonUtility.formatNull(orderVO.getContactFirstName()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"contact-last-name",CommonUtility.formatNull(orderVO.getContactLastName()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"contact-phone",CommonUtility.formatNull(orderVO.getContactPhone()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"contact-ext",CommonUtility.formatNull(orderVO.getContactExt()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"contact-email-address",CommonUtility.formatNull(orderVO.getEmailAddress()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-first-name",CommonUtility.formatNull(orderVO.getFirstName()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-last-name",CommonUtility.formatNull(orderVO.getLastName()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-business",CommonUtility.formatNull(orderVO.getBusinessName()));
            header.appendChild(temp);

            temp = XMLUtility.createElement(xmlDoc,"buyer-address1",CommonUtility.formatNull(orderVO.getAddressLine1()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-address2",CommonUtility.formatNull(orderVO.getAddressLine2()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-city",CommonUtility.formatNull(orderVO.getCity()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-state",CommonUtility.formatNull(orderVO.getState()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-postal-code",CommonUtility.formatNull(orderVO.getZipCode()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-country",CommonUtility.formatNull(orderVO.getCountry()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-daytime-phone",CommonUtility.formatNull(orderVO.getPhone()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-evening-phone");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-work-ext",CommonUtility.formatNull(orderVO.getExt()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-fax");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"buyer-email-address",CommonUtility.formatNull(orderVO.getEmailAddress()));
            header.appendChild(temp);

      
           
            temp = XMLUtility.createElement(xmlDoc,"ariba-buyer-cookie");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"ariba-asn-buyer-number");
            header.appendChild(temp);

            temp = XMLUtility.createElement(xmlDoc,"ariba-payload");
            header.appendChild(temp);
           
            temp = XMLUtility.createElement(xmlDoc,"news-letter-flag");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-type",CommonUtility.formatNull(orderVO.getPaymentType()));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-number",CommonUtility.formatNull(orderVO.getCreditCardNumber()));
            header.appendChild(temp);
            
            //get card expiration date
            String exprDate = "";
            if(orderVO.getExpirationDate() != null){
                Date exprDateAsDate= CommonUtility.formatStringToUtilDate(orderVO.getExpirationDate());
                exprDate = CommonUtility.formatUtilDateToCreditCardString(exprDateAsDate);            
            }

            temp = XMLUtility.createElement(xmlDoc,"cc-exp-date",CommonUtility.formatNull(exprDate));
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-approval-code");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-approval-amt");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-approval-verbage");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-approval-action-code");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-avs-result");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"cc-acq-data");    
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"aafes-ticket-number");
            header.appendChild(temp);
            
            temp = XMLUtility.createElement(xmlDoc,"gift-certificates");
            header.appendChild(temp);
            
            Element cobrands = XMLUtility.createElement(xmlDoc,"co-brands");
            if(orderVO.getMembershipID() != null && orderVO.getMembershipID().length() > 0)
            {
              //membership type & id
              Element cobrand = XMLUtility.createElement(xmlDoc,"co-brand");
              temp = XMLUtility.createElement(xmlDoc,"name",CommonUtility.formatNull(orderVO.getMembershipType()));
              cobrand.appendChild(temp);
              temp = XMLUtility.createElement(xmlDoc,"data",CommonUtility.formatNull(orderVO.getMembershipID()));
              cobrand.appendChild(temp);
              cobrands.appendChild(cobrand);

              //membership first name
              cobrand = XMLUtility.createElement(xmlDoc,"co-brand");
              temp = XMLUtility.createElement(xmlDoc,"name","FNAME");
              cobrand.appendChild(temp);
              temp = XMLUtility.createElement(xmlDoc,"data",CommonUtility.formatNull(orderVO.getMembershipFirstName()));
              cobrand.appendChild(temp);
              cobrands.appendChild(cobrand);

              //membership last name
              cobrand = XMLUtility.createElement(xmlDoc,"co-brand");
              temp = XMLUtility.createElement(xmlDoc,"name","LNAME");
              cobrand.appendChild(temp);
              temp = XMLUtility.createElement(xmlDoc,"data",CommonUtility.formatNull(orderVO.getMembershipLastName()));
              cobrand.appendChild(temp);
              cobrands.appendChild(cobrand);

            }
            header.appendChild(cobrands);       

            xmlDoc.getDocumentElement().appendChild(header);
    }

    /* This method adds the line elements to the order document.
     * @param XMLDocument document to which the lines are added
     * @param OrderDocVO contains the line data 
     * @throws SQLException,IOException,ParserConfigurationException, Exception */
    private static void addLines(XMLDocument xmlDoc,OrderDocVO orderVO)
        throws SQLException, IOException, ParserConfigurationException,Exception
    {
      //get line items
      List lines = orderVO.getLines();

      //get data from configuration file
      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
      String orderPrefix = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ORDER_PREFIX_PROPERTY);
      String occasionDefault = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,OCCASION_DEFAULT_PROPERTY);

      //create items node
      Element itemsNode = XMLUtility.createElement(xmlDoc,"items");
      
      //loop through each item
      for(int i=0;i<lines.size();i++)
      {           
        String orderNumber = orderPrefix + new LookupDAO().getNextOrderNumber();

     
        LineVO lineVO = (LineVO)lines.get(i);
        Map lineMap = lineVO.getLineItemsMap();

        Element line = XMLUtility.createElement(xmlDoc,"item");
        Element tmp = null;        

        tmp = XMLUtility.createElement(xmlDoc,"order-number",orderNumber);
        line.appendChild(tmp);

        //3/22/04, this is for item of the week..there is now a source code on the detail level
        tmp = XMLUtility.createElement(xmlDoc,"item-source-code",orderVO.getSourceCode());
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"item-of-the-week","N");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"order-total",CommonUtility.getParamDouble(lineMap,FIELD_LINE_TOTAL));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"tax-amount",CommonUtility.getParamDouble(lineMap,FIELD_TAX_AMOUNT));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"service-fee",CommonUtility.getParamDouble(lineMap,FIELD_SERVICE_FEE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"extra-shipping-fee");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"drop-ship-charges",CommonUtility.getParamDouble(lineMap,FIELD_SHIPPING_FEE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"retail-variable-price",CommonUtility.getParamDouble(lineMap,FIELD_RETAIL_PRICE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"productid",CommonUtility.getParam(lineMap,FIELD_PRODUCT_ID));
        line.appendChild(tmp);

        tmp = XMLUtility.createElement(xmlDoc,"product-price",CommonUtility.getParamDouble(lineMap,FIELD_PRODUCT_PRICE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"color-size");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"add-ons");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-first-name",CommonUtility.getParam(lineMap,FIELD_FIRST_NAME));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-last-name",CommonUtility.getParam(lineMap,FIELD_LAST_NAME));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-address1",CommonUtility.getParam(lineMap,FIELD_ADDRESS));    
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-address2",CommonUtility.getParam(lineMap,FIELD_ADDRESS2));    
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-city",CommonUtility.getParam(lineMap,FIELD_CITY));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-state",CommonUtility.getParam(lineMap,FIELD_STATE));    
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-postal-code",CommonUtility.getParam(lineMap,FIELD_ZIPCODE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-country",CommonUtility.getParam(lineMap,FIELD_COUNTRY));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-international");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-phone",CommonUtility.getParam(lineMap,FIELD_PHONE_NUMBER));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"recip-phone-ext",CommonUtility.getParam(lineMap,FIELD_PHONE_EXTENSION));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"ship-to-type");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"ship-to-type-name",CommonUtility.getParam(lineMap,FIELD_BUSINESS));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"ship-to-type-info");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"occassion",occasionDefault);
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"delivery-date",CommonUtility.getParam(lineMap,FIELD_DELIVERY_DATE));    
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"second-delivery-date");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"card-message",CommonUtility.getParam(lineMap,FIELD_CARD_MESSAGE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"card-signature",CommonUtility.getParam(lineMap,FIELD_CARD_SIGNATURE));
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"special-instructions");
        line.appendChild(tmp);
        
        tmp = XMLUtility.createElement(xmlDoc,"shipping-method",CommonUtility.getParam(lineMap,FIELD_SHIPPING_METHOD));
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"lmg-flag");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"lmg-email-address");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"lmg-email-signature");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"fol-indicator",FOL_VALUE);
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"sunday-delivery-flag");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"sender-release-flag");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-result-code");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-address1");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-address2");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-city");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-state");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-postal-code");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-firm-name");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-latitude");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-longitude");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-override-flag");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"qms-usps-range-record-type");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"ariba-po-number");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"ariba-cost-center");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"ariba-ams-project-code");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"ariba-unspsc-code");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"product-substitution-acknowledgement");
        line.appendChild(tmp);
        tmp = XMLUtility.createElement(xmlDoc,"florist",CommonUtility.formatNull(orderVO.getFloristNumber()));
        line.appendChild(tmp);

        //add to items node
        itemsNode.appendChild(line);
      }
      //append lines to document
      xmlDoc.getDocumentElement().appendChild(itemsNode);      
    }









  /** This method generates an order document from  request data.
   * The request contains String parameters as well as an uploaded Excel file.
   * @params XMLDocument document
   * @param OrderDocVo The order vo which will be populated
   * @params HttpServletRequest request object
   * @params boolean ParseSpreadSheet flag true=parse it
   * @return OrderDocVO order document. 
   * @throws BulkLoadException
   * @throws FileUploadException
   * @throws IOException
   * @throws TransformerException
   * @throws SAXException
   * @throws XSLException
   * @throws ParserConfigurationException*/
   public static String createOrderObject(XMLDocument xmlDoc,OrderDocVO order, HttpServletRequest request, boolean parseSheet) 
    throws BulkOrderException,FileUploadException, IOException, 
          TransformerException,SAXException,XSLException,ParserConfigurationException
   {
    List items = null;

    String securityToken = "";

    // Create a new file upload handler
    DiskFileUpload upload = new DiskFileUpload();
    
    //Parse the request..this method generates a list of all the items 
    //found in the HTTPServlet Request
    boolean multipartRequest = true;
    try{
        items = upload.parseRequest(request);    
    }
    catch(Exception e)
    {
        //this request is not multipart, attempt to process an non-multpart request
        //this type of requests are received during testing
        new Logger(LOGGER_CATEGORY).info("Bulk Load received non-mulipart request.");        
        multipartRequest = false;
    }

    //spreadsheet parser
    SpreadSheetUtility sheetUtility = new SpreadSheetUtility();

    //file object which points to spreadsheet    
    FileItem theFile = null;
    InputStream spreadSheetStream = null;
    
    //if received a mutipart request
    if(multipartRequest){ 
        //loop through each item in the request
        Iterator iter = items.iterator();
        List valList = null;
        while (iter.hasNext()) {

            //get next item
            FileItem item = (FileItem) iter.next();

            //determine if item is a file or form parameter
            if (item.isFormField()) {
                //if form paremeter and it to VO...the header values come in as
                //form parameters.
                processFormField(item.getFieldName(),item.getString(),order);

                //if security token..save it
                if(item.getFieldName().equals(HTML_PARAM_SECURITY_TOKEN))
                {
                  securityToken = item.getString();
                }
                
            } else {
                //save a referance to the file...the line values come in as file
                theFile = item;
            }        
        }// end while        

        //Now get the values from the Excel file and add them to the VO
        order.setFile(theFile.getName());    

        //get a stream for the file
        if(order.getFile() != null && order.getFile().length() > 0){
            spreadSheetStream = theFile.getInputStream();
        }

    }
    else
    {
          //this is not a mulipart requset

          //grab parameters out of request
         // Map paramMap = request.getParameterMap().entrySet();
          Iterator iter = request.getParameterMap().entrySet().iterator();
          while(iter.hasNext()) {          
              Map.Entry entry = (Map.Entry)iter.next();
              String key = (String)entry.getKey();
              String value = ((String[])entry.getValue())[0];
              processFormField(key,value,order);
          }
      
          DefaultFileItemFactory factory = new DefaultFileItemFactory();
          File f = new File(order.getFile());
          spreadSheetStream = new FileInputStream(f);
    } //end else not-multipart

    //parse spreadsheet only if the inputstream is not null
    //if stream is null then we do not have a file
    try{
        if(spreadSheetStream != null && parseSheet){
          order.setLines(sheetUtility.parse(spreadSheetStream));
       }    
    }
    catch(Exception e)
    {
      //invalid file...set file name to INVALID
      order.setFile("INVALID");
    }

    return securityToken;

   }    

    /* This method takes in a form field name and form value and populates 
     * the appropiate VO field.
        @param String field name
        @param String field value
        @param OrderDocVO document which to populate 
        @returns OrderDocVO the update order document */
    private static OrderDocVO processFormField(String field, String value, OrderDocVO order){

        //convert field name to lower case
        String fieldName = field.toLowerCase();

        //find vo value to populate
        if(field.equals(HTML_PARAM_CONTACT_FIRSTNAME))
          order.setContactFirstName(value);
        else if(field.equals(HTML_PARAM_CONTACT_LASTNAME))
          order.setContactLastName(value);
        else if(field.equals(HTML_PARAM_CONTACT_PHONE))
          order.setContactPhone(value);
        else if(field.equals(HTML_PARAM_CONTACT_EXT))
          order.setContactExt(value);          
        else if(field.equals(HTML_PARAM_SOURCE_CODE))        
          order.setSourceCode(value);          
        else if(field.equals(HTML_PARAM_CONTACT_EMAIL))        
          order.setContactEmail(value);
        else if(field.equals(HTML_PARAM_FIRST_NAME))
          order.setFirstName(value);
        else if(field.equals(HTML_PARAM_LAST_NAME))
          order.setLastName(value);
        else if(field.equals(HTML_PARAM_BUSINESS_NAME))
          order.setBusinessName(value);
        else if(field.equals(HTML_PARAM_ADDRESS_LINE1))
          order.setAddressLine1(value);
        else if(field.equals(HTML_PARAM_ADDRESS_LINE2))
          order.setAddressLine2(value);
        else if(field.equals(HTML_PARAM_CITY))
          order.setCity(value);
        else if(field.equals(HTML_PARAM_COUNTRY))
          order.setCountry(value);
        else if(field.equals(HTML_PARAM_EMAIL_ADDRESS))
          order.setEmailAddress(value);
        else if(field.equals(HTML_PARAM_PHONE))
          order.setPhone(value);
        else if(field.equals(HTML_PARAM_EXT))
          order.setExt(value);
        else if(field.equals(HTML_PARAM_ZIPCODE))
          order.setZipCode(value);
        else if(field.equals(HTML_PARAM_CREDITCARD_NUMBER))
          order.setCreditCardNumber(value);
        else if(field.equals(HTML_PARAM_EXPIRATION_DATE))
          order.setExpirationDate(value);
        else if(field.equals(HTML_PARAM_PAYMENT_TYPE))
          order.setPaymentType(value);
        else if(field.equals(HTML_PARAM_FLORIST_NUMBER))
          order.setFloristNumber(value);
        else if(field.equals(HTML_PARAM_GUID))
          order.setGuid(value);
        else if(field.equals(HTML_PARAM_STATE))
          order.setState(value);   
        else if(field.equals(HTML_PARAM_CREDIT_CARD_YEAR))
          order.setCreditCardYear(value);   
        else if(field.equals(HTML_PARAM_CREDIT_CARD_MONTH))
          order.setCreditCardMonth(value);   
        else if(field.equals(HTML_PARAM_MEMBERSHIP_ID))
          order.setMembershipID(value);   
        else if(field.equals(HTML_PARAM_MEMBERSHIP_FIRST_NAME))
          order.setMembershipFirstName(value);   
        else if(field.equals(HTML_PARAM_MEMBERSHIP_LAST_NAME))
          order.setMembershipLastName(value);             
        else if(field.equals(HTML_PARAM_ALLOW_MEMBERSHIP_INFO))
          order.setMembershipLastName(value);             
        else if(field.equals(HTML_PARAM_FILENAME))
          order.setFile(value);   

      return order;
    }   


  /** The method takes in an order object and creates an xml document
   *  to be used for the webpage. 
   *  @param XMLDocument for which element is created for
   *  @param OrderDocVO order document
   *  @returns Element XML document element containing order*/
   public static Element getXMLforWeb(XMLDocument xmlDoc,OrderDocVO order)
    throws ParseException
   {
      //create doc element
      Element parent = XMLUtility.createElement(xmlDoc,XML_ELEMENT_FIELD_GROUP);

      //add the header if order was passed in
      if(order != null){
        addHeaderForWebXML(xmlDoc,parent,order);
      }

      return parent;

   }

    /* This method adds the header elements to the order document.
     * @param XMLDocument document to which the header is added
     * @param Element parent object to which lements should be added
     * @param OrderDocVO contains the header data */
    private static void addHeaderForWebXML(XMLDocument xmlDoc,Element parent,OrderDocVO orderVO)
          throws ParseException
    {
            //attribute list
            Map attributes = new HashMap();

          //display credit card year as 4 digit year.

         String expYear = "";
          try{
              if(orderVO.getExpirationDate() != null && orderVO.getExpirationDate().length() > 0){
                SimpleDateFormat dfOut = new SimpleDateFormat("yyyy");
                expYear = dfOut.format(CommonUtility.formatStringToUtilDate(orderVO.getExpirationDate()));
              }
          }
          catch(Exception e)
          {
            //if an error occured parsing the exception then continue on.  
            //let the validation code catch invalid data.
          }


            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_ALLOW_MEMBERSHIP_INFO);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getAllowMembershipInfo());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CONTACT_FIRSTNAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getContactFirstName());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CONTACT_LASTNAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getContactLastName());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CONTACT_EMAIL);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getContactEmail());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CONTACT_PHONE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getContactPhone());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CONTACT_EXT);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getContactExt());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    
            
            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_SOURCE_CODE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getSourceCode());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_FIRST_NAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getFirstName());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_LAST_NAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getLastName());
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_BUSINESS_NAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getBusinessName());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_ADDRESS_LINE1);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getAddressLine1());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_ADDRESS_LINE2);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getAddressLine2());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CITY);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getCity());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_STATE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getState());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_COUNTRY);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getCountry());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_EMAIL_ADDRESS);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getEmailAddress());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_PHONE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getPhone());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_EXT);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getExt());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                        

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_ZIPCODE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getZipCode());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CREDITCARD_NUMBER);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getCreditCardNumber());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes); 

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_EXPIRATION_DATE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getExpirationDate()  );            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CREDIT_CARD_MONTH);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getCreditCardMonth());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                            

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_CREDIT_CARD_YEAR);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,expYear);            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                        


            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_PAYMENT_TYPE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getPaymentType());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_FLORIST_NUMBER);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getFloristNumber());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_GUID);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getGuid());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_MEMBERSHIP_TYPE);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getMembershipType());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_MEMBERSHIP_ID);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getMembershipID());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                    

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_MEMBERSHIP_FIRST_NAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getMembershipFirstName());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                                

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_MEMBERSHIP_LAST_NAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getMembershipLastName());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                                

            attributes= new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_FILENAME);
            attributes.put(XML_ATTRIBUTE_FIELDVALUE,orderVO.getFile());            
            XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,attributes);                                                    



    }   

  /** This method gets a guid, puts the guid in the order object & document
   * and then returns the guid.
   * @param XMLDocument document to add guid to
   * @param OrderDocVO order object to add guid to
   * @return String guid */
   public static String addGuid(XMLDocument xmlDoc, OrderDocVO order)
   {

      String orderGuid = "";
      
      //if we have an order object & it contains a guid, use that guid
      if(order == null || order.getGuid().equalsIgnoreCase(""))
      {
        orderGuid = BULK_GUID_PREFIX +  new Long(CommonUtility.getUniqueNumber()).toString();
      }
      else
      {
        orderGuid = order.getGuid();
      }
   
      //get xml objects
      Element parent = XMLUtility.createElement(xmlDoc,XML_ELEMENT_FIELD_GROUP);        
      HashMap guidMap = new HashMap();
      guidMap.put(XML_ATTRIBUTE_FIELDNAME,HTML_PARAM_GUID);
      guidMap.put(XML_ATTRIBUTE_FIELDVALUE,orderGuid);      
      XMLUtility.addField(xmlDoc,parent,XML_ELEMENT_FIELD,guidMap);
      xmlDoc.getDocumentElement().appendChild(parent);

      //put guid in vo
      if(order != null){
          order.setGuid(orderGuid);
      }

      return orderGuid;
   }    

  /** This method converts a bulk order object to a scrub order object.
   * 
   * Not several empty objects need to be added to the order VO inorder
   * for the scrub dao to accept it.  So where you see empty VO's added
   * to the order object below, that is why.
   * 
   * @param OrderDocVO Bulk Order object
   * @returns OrderVO Order Scrub Order Object */
  public static OrderVO convertOrder(OrderDocVO bulkOrder) throws Exception
  {



      ConfigurationUtil config = ConfigurationUtil.getInstance();            
      String bulkOrgin = config.getProperty(BULK_ORDER_PROPERTY_FILE,BULK_ORDER_ORGIN_PROPERTY);  
      String occasionDefault = config.getProperty(BULK_ORDER_PROPERTY_FILE,OCCASION_DEFAULT_PROPERTY);
      String lineBulkStatus = config.getProperty(BULK_ORDER_PROPERTY_FILE,LINE_BULK_STATUS_PROPERTY);
      String orderBulkStatus = config.getProperty(BULK_ORDER_PROPERTY_FILE,ORDER_BULK_STATUS_PROPERTY);
      
      OrderVO ospOrder = new OrderVO();

      //contact
      OrderContactInfoVO contact = new OrderContactInfoVO();
      contact.setFirstName(bulkOrder.getContactFirstName());
      contact.setLastName(bulkOrder.getContactLastName());
      contact.setPhone(bulkOrder.getContactPhone());
      contact.setExt(bulkOrder.getContactExt());
      contact.setEmail(bulkOrder.getContactEmail());
      List contactList = new ArrayList();
      contactList.add(contact);

      //buyer
      BuyerVO buyer = new BuyerVO();
      buyer.setFirstName(bulkOrder.getFirstName());
      buyer.setLastName(bulkOrder.getLastName());
      BuyerAddressesVO baddr = new BuyerAddressesVO();
      baddr.setAddressEtc(bulkOrder.getBusinessName());
      baddr.setAddressLine1(bulkOrder.getAddressLine1());
      baddr.setAddressLine2(bulkOrder.getAddressLine2());      
      baddr.setCity(bulkOrder.getCity());
      baddr.setStateProv(bulkOrder.getState());
      baddr.setPostalCode(bulkOrder.getZipCode());
      baddr.setCountry(bulkOrder.getCountry());
      BuyerPhonesVO bphone = new BuyerPhonesVO();
      bphone.setPhoneNumber(bulkOrder.getPhone());
      bphone.setExtension(bulkOrder.getExt());
      BuyerEmailsVO bemail = new BuyerEmailsVO();
      bemail.setEmail(bulkOrder.getEmailAddress());
      List bAddrList = new ArrayList();
      bAddrList.add(baddr);
      List bPhoneList = new ArrayList();
      bPhoneList.add(bphone);
      List bEmailList = new ArrayList();
      bEmailList.add(bemail);
      buyer.setBuyerAddresses(bAddrList);      
      buyer.setBuyerPhones(bPhoneList);
      buyer.setBuyerEmails(bEmailList);
      List buyerList = new ArrayList();
      buyerList.add(buyer);

      //payment
      PaymentsVO payment = new PaymentsVO();
      payment.setPaymentsType(bulkOrder.getPaymentType());

      //determine method for this payment type
//      XMLDocument payDoc = lookup.getPaymentType(order.getPaymentType());
//      String paymentMethod = XMLUtility.getAttributeValue(payDoc,PAYMENT_PATH,PAYMENT_TYPE_ATTRIBUTE);      
      
      payment.setPaymentMethodType(bulkOrder.getPaymentMethod());
      if(bulkOrder.getCreditCardNumber() != null & bulkOrder.getCreditCardNumber().length() > 0)
      {
        CreditCardsVO card = new CreditCardsVO();
        card.setCCNumber(bulkOrder.getCreditCardNumber());
        card.setCCExpiration(bulkOrder.getExpirationDate());
        List cards = new ArrayList();
        cards.add(card);
        payment.setCreditCards(cards);
      }
      else
      {
        //insert empty credit card
        CreditCardsVO card = new CreditCardsVO();
        List cards = new ArrayList();
        cards.add(card);
        payment.setCreditCards(cards);        
      }
      List paymentList = new ArrayList();
      paymentList.add(payment);

      //membership----insert as cobrands
      if(bulkOrder.getMembershipID() != null && bulkOrder.getMembershipID().length() > 0)
      {
          //co brand first name
          CoBrandVO coFName = new CoBrandVO();
          coFName.setInfoName(COBRAND_FIRST_NAME);
          coFName.setInfoData(bulkOrder.getMembershipFirstName());

          //co brand last name
          CoBrandVO coLName = new CoBrandVO();
          coLName.setInfoName(COBRAND_LAST_NAME);
          coLName.setInfoData(bulkOrder.getMembershipLastName());

          //co brand info
          CoBrandVO coType = new CoBrandVO();
          coType.setInfoName(bulkOrder.getMembershipType());
          coType.setInfoData(bulkOrder.getMembershipID());
          
          List coList = new ArrayList();
          coList.add(coFName);
          coList.add(coLName);
          coList.add(coType);          
          ospOrder.setCoBrand(coList);        
      }
      else
      {
        //empty cobrand vo
        ospOrder.setCoBrand(new ArrayList());
      }

      
      //header
      ospOrder.setSourceCode(bulkOrder.getSourceCode());   
      ospOrder.setPayments(paymentList);
      ospOrder.setBuyer(buyerList);
      ospOrder.setOrderContactInfo(contactList);
      ospOrder.setGUID(bulkOrder.getGuid());
      ospOrder.setOrderOrigin(bulkOrgin);
      ospOrder.setStatus(orderBulkStatus);
      ospOrder.setOrderTotal(Double.toString(bulkOrder.getOrderTotal()));
      ospOrder.setCsrId(bulkOrder.getCsrId());
      //for each on the order
      List lines = bulkOrder.getLines();
      ArrayList ospLineList = new ArrayList();
      if(lines != null){
        for (int i = 0; i < lines.size(); i++) 
        {
           LineVO bulkLine = (LineVO)lines.get(i);
           OrderDetailsVO ospLine = new OrderDetailsVO();

           //line data
           Map lineMap = bulkLine.getLineItemsMap();

           //recipient
           RecipientsVO recip = new RecipientsVO();
           RecipientAddressesVO raddr = new RecipientAddressesVO();
           RecipientPhonesVO rphone = new RecipientPhonesVO();
           recip.setFirstName(CommonUtility.getParam(lineMap,FIELD_FIRST_NAME));          
           recip.setLastName(CommonUtility.getParam(lineMap,FIELD_LAST_NAME));          
           raddr.setName(CommonUtility.getParam(lineMap,FIELD_BUSINESS));          
           raddr.setAddressLine1(CommonUtility.getParam(lineMap,FIELD_ADDRESS));          
           raddr.setCity(CommonUtility.getParam(lineMap,FIELD_CITY));          
           raddr.setCountry(CommonUtility.getParam(lineMap,FIELD_COUNTRY));          
           raddr.setPostalCode(CommonUtility.getParam(lineMap,FIELD_ZIPCODE));          
           raddr.setStateProvince(CommonUtility.getParam(lineMap,FIELD_STATE)); 

           //recipient internationial flag must be set.
           //this is need for the data massager
           boolean domestic = new LookupDAO().isDomestic(raddr.getCountry());
           if(domestic)
           {
             raddr.setInternational("N");
           }
           else
           {
             raddr.setInternational("Y");
           }

           rphone.setPhoneNumber(CommonUtility.getParam(lineMap,FIELD_PHONE_NUMBER)); 
           rphone.setExtension(CommonUtility.getParam(lineMap,FIELD_PHONE_EXTENSION)); 
           List raddrList = new ArrayList();
           raddrList.add(raddr);
           List rphoneList = new ArrayList();
           rphoneList.add(rphone);
           recip.setRecipientAddresses(raddrList);
           recip.setRecipientPhones(rphoneList);
           DestinationsVO dest = new DestinationsVO();
           List distList = new ArrayList();
           distList.add(dest);
           recip.setDestinations(distList);
           List recipList = new ArrayList();
           recipList.add(recip);

           //detail
           ospLine.setProductId(CommonUtility.getParam(lineMap,FIELD_PRODUCT_ID)); 
           ospLine.setDeliveryDate(CommonUtility.getParam(lineMap,FIELD_DELIVERY_DATE)); 
           ospLine.setCardMessage(CommonUtility.getParam(lineMap,FIELD_CARD_MESSAGE)); 
           ospLine.setRecipients(recipList);
           ospLine.setFloristNumber(bulkOrder.getFloristNumber());
           ospLine.setOccassionId(occasionDefault);
           ospLine.setLineNumber(new Integer(i+1).toString());
           ospLine.setStatus(lineBulkStatus);

           //Empty addon
           AddOnsVO addon = new AddOnsVO();
           List addList = new ArrayList();
           ospLine.setAddOns(addList);

          ospLineList.add(ospLine);           

        }//end for loop  
      }
      else
      //else order does not contain any details
      {
          //add 1 detail vo so that florist number is stored
           OrderDetailsVO ospLine = new OrderDetailsVO();          
           ospLine.setFloristNumber(bulkOrder.getFloristNumber());
           ospLineList.add(ospLine);           
      }

      ospOrder.setOrderDetail(ospLineList);

      return ospOrder;
  }

  /** This method converts a osp order object to a bulk order object.
   * @param OrderVO 
   * @returns OrderDocVO  */
  public static OrderDocVO convertOrder(OrderVO ospOrder)
  {

  
      OrderDocVO bulkOrder = new OrderDocVO();

      //contact
      if(ospOrder.getOrderContactInfo() != null && ospOrder.getOrderContactInfo().size() >0){
          OrderContactInfoVO contact = (OrderContactInfoVO)ospOrder.getOrderContactInfo().get(0);
          bulkOrder.setContactFirstName(contact.getFirstName());
          bulkOrder.setContactLastName(contact.getLastName());
          bulkOrder.setContactPhone(contact.getPhone());
          bulkOrder.setContactExt(contact.getExt());
          bulkOrder.setContactEmail(contact.getEmail());
      }

      //buyer
      if(ospOrder.getBuyer() != null && ospOrder.getBuyer().size() >0){
        BuyerVO buyer = (BuyerVO)ospOrder.getBuyer().get(0);
        bulkOrder.setFirstName(buyer.getFirstName());
        bulkOrder.setLastName(buyer.getLastName());
        if(buyer.getBuyerAddresses() != null && buyer.getBuyerAddresses().size() >0){
            BuyerAddressesVO baddr = (BuyerAddressesVO)buyer.getBuyerAddresses().get(0);
            bulkOrder.setBusinessName(baddr.getAddressEtc());
            bulkOrder.setAddressLine1(baddr.getAddressLine1());
            bulkOrder.setAddressLine2(baddr.getAddressLine2());      
            bulkOrder.setCity(baddr.getCity());
            bulkOrder.setState(baddr.getStateProv());
            bulkOrder.setZipCode(baddr.getPostalCode());
            bulkOrder.setCountry(baddr.getCountry());
        }
        if(buyer.getBuyerPhones() != null && buyer.getBuyerPhones().size() >0){        
            BuyerPhonesVO bphone = (BuyerPhonesVO)buyer.getBuyerPhones().get(0);
            bulkOrder.setPhone(bphone.getPhoneNumber());
            bulkOrder.setExt(bphone.getExtension());
        }
        if(buyer.getBuyerEmails() != null && buyer.getBuyerEmails().size() >0){                
            BuyerEmailsVO bemail = (BuyerEmailsVO)buyer.getBuyerEmails().get(0);
            bulkOrder.setEmailAddress(bemail.getEmail());
        }
      }//end if buyers

      //payment
      if(ospOrder.getPayments() != null && ospOrder.getPayments().size() >0){      
          PaymentsVO payment = (PaymentsVO)ospOrder.getPayments().get(0);
          bulkOrder.setPaymentType(payment.getPaymentType());
          bulkOrder.setPaymentMethod(payment.getPaymentMethodType());
          if(payment.getCreditCards() != null && payment.getCreditCards().size() >0){        
              CreditCardsVO card = (CreditCardsVO)payment.getCreditCards().get(0);
              bulkOrder.setCreditCardNumber((card.getCCNumber()));
              bulkOrder.setExpirationDate((card.getCCExpiration()));
          }
      }


      //membership
      if(ospOrder.getMemberships() != null && ospOrder.getMemberships().size() >0){            
          MembershipsVO member = (MembershipsVO)ospOrder.getMemberships().get(0);
          bulkOrder.setMembershipFirstName((member.getFirstName()));
          bulkOrder.setMembershipLastName((member.getLastName()));
          bulkOrder.setMembershipID((member.getMembershipIdNumber()));
          bulkOrder.setMembershipType((member.getMembershipType()));
      }
      else
      {
        //check if membership info exists in the cobrand objects
        if(ospOrder.getCoBrand() != null){
          for (int i = 0; i < ospOrder.getCoBrand().size() ; i++) 
          {
            CoBrandVO co = (CoBrandVO)ospOrder.getCoBrand().get(i);            
            if(co.getInfoName().equals(COBRAND_FIRST_NAME))
                bulkOrder.setMembershipFirstName(co.getInfoData());
            else if(co.getInfoName().equals(COBRAND_LAST_NAME))
                bulkOrder.setMembershipLastName(co.getInfoData());
            else{
                bulkOrder.setMembershipType(co.getInfoName());             
                bulkOrder.setMembershipID(co.getInfoData());                
            }
            }//end for co brand loop
          }//end if have co brands          
        }//end else check for co brands

      double orderTotal = 0;
      try
      {
        orderTotal = Double.parseDouble(ospOrder.getOrderTotal());
      }
      catch(Throwable t)
      {
        //if order total contains unparsable value, then just leave it at zero
      }

      //header
      bulkOrder.setSourceCode((ospOrder.getSourceCode()));   
      bulkOrder.setOrderTotal(orderTotal);
      bulkOrder.setCsrId(ospOrder.getCsrId());

      //for each on the order
      List lines = ospOrder.getOrderDetail();
      ArrayList bulkLineList = new ArrayList();
      if(lines != null){
        for (int i = 0; i < lines.size(); i++) 
        {
           LineVO bulkLine = new LineVO();
           OrderDetailsVO ospLine = (OrderDetailsVO)lines.get(i);

           bulkOrder.setFloristNumber(ospLine.getFloristNumber());
           
           //create line mape
           Map lineMap = new HashMap();

           //recipient
           if(ospLine.getRecipients() != null && ospLine.getRecipients().size() > 0){
               RecipientsVO recip = (RecipientsVO)ospLine.getRecipients().get(0);
               lineMap.put(FIELD_FIRST_NAME,recip.getFirstName());          
               lineMap.put(FIELD_LAST_NAME,recip.getLastName());   
               if(recip.getRecipientAddresses() != null && recip.getRecipientAddresses().size() >0){
                   RecipientAddressesVO raddr = (RecipientAddressesVO)recip.getRecipientAddresses().get(0);
                   lineMap.put(FIELD_BUSINESS,raddr.getName());          
                   lineMap.put(FIELD_ADDRESS,raddr.getAddressLine1());          
                   lineMap.put(FIELD_ADDRESS2,raddr.getAddressLine2());          
                   lineMap.put(FIELD_CITY,raddr.getCity());          
                   lineMap.put(FIELD_COUNTRY,raddr.getCountry());          
                   lineMap.put(FIELD_ZIPCODE,raddr.getPostalCode());          
                   lineMap.put(FIELD_STATE,raddr.getStateProvince()); 
               }
               if(recip.getRecipientPhones() != null && recip.getRecipientPhones().size() >0){           
                   RecipientPhonesVO rphone = (RecipientPhonesVO)recip.getRecipientPhones().get(0);
                   lineMap.put(FIELD_PHONE_NUMBER,rphone.getPhoneNumber()); 
                   lineMap.put(FIELD_PHONE_EXTENSION,rphone.getExtension()); 
               }
           }//end if recipients
    
           //detail
           if(ospLine.getProductSubCodeId() != null && ospLine.getProductSubCodeId().length() > 0)
           {
             lineMap.put(FIELD_PRODUCT_ID,ospLine.getProductSubCodeId());              
           }
           else
           {
             lineMap.put(FIELD_PRODUCT_ID,ospLine.getProductId()); 
           }

           lineMap.put(FIELD_DELIVERY_DATE,ospLine.getDeliveryDate()); 
           lineMap.put(FIELD_CARD_MESSAGE,ospLine.getCardMessage()); 
           lineMap.put(FIELD_LINE_TOTAL,ospLine.getExternalOrderTotal());
           lineMap.put(FIELD_TAX_AMOUNT,ospLine.getTaxAmount());
           lineMap.put(FIELD_SERVICE_FEE,ospLine.getServiceFeeAmount());
           lineMap.put(FIELD_PRODUCT_PRICE,ospLine.getProductsAmount()) ;          
           lineMap.put(FIELD_SHIPPING_FEE,ospLine.getShippingFeeAmount());                      
           lineMap.put(FIELD_SHIPPING_VIA,ospLine.getShipVia());
           lineMap.put(FIELD_SHIPPING_METHOD,ospLine.getShipMethod());
         

           bulkLine.setLineItems(lineMap);
           
           bulkLineList.add(bulkLine);           
           
        }  
      }      

      bulkOrder.setLines(bulkLineList);

      return bulkOrder;
  }
   
}