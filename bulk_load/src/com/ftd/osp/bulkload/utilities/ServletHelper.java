package com.ftd.osp.bulkload.utilities;

import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.security.SecurityManager;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;

import java.io.IOException;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * Collection of methods used by the Servlets.
 */
public class ServletHelper  implements BulkOrderConstants,FieldConstants {
    private static Logger logger = new 
Logger("com.ftd.osp.bulkload.utilities.ServletHelper");

    /**
     * Indicates whether or not the security token and security context 
are valid.
     *
     * @param request the http request object
     * @exception ExpiredIdentityException
     * @exception ExpiredSessionException
     * @exception InvalidSessionException
     * @exception SAXException
     * @exception ParserConfigurationException
     * @exception IOException
     * @exception SQLException
     * @exception Exception
     * @return whether or not the security token and the security 
context are valid
     */
    public static boolean isValidToken(HttpServletRequest request)
      throws ExpiredIdentityException,
             ExpiredSessionException,
             InvalidSessionException,
             SAXException,
             ParserConfigurationException,
             IOException,
             SQLException,
             Exception{

          //get data from configuration file
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          String unit = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,"UNIT");    
        

      String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT); 
// change to use a constant here
      if (securityContext == null || (securityContext.equals("")))  {
          logger.debug("No Security Context found");
          return false;
      }
      logger.debug(securityContext);

      String securityToken = 
request.getParameter(FieldConstants.HTML_PARAM_SECURITY_TOKEN);
      if (securityToken == null || (securityToken.equals("")))  {
          logger.debug("No Security Token found");
          return false;
      }
       logger.debug(securityToken);

      logger.debug("authenticating security token");
      SecurityManager securityManager = SecurityManager.getInstance();
//      return securityManager.authenticate(securityContext, securityToken);
      return securityManager.authenticateSecurityToken(securityContext, unit, securityToken);
  }


  /**
   * Returns CSR id
   * @throws Exception
   * @throws SQLException
   * @param String securityToken
   * @return csrId
   */
  public static String getUserId(String securityToken) throws Exception, SQLException{
    //get data from configuration file
    if (securityToken.equals(""))
      return "";
    SecurityManager securityManager = SecurityManager.getInstance();
    return securityManager.getUserInfo(securityToken).getUserID();
  }
}



