

package com.ftd.osp.bulkload.utilities;

import com.ftd.osp.utilities.plugins.Logger;
import java.io.StringReader;
import java.util.Date;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/* This object contains XPath utility functions.
 * @author Copied from Order Entry Phase1 Framework. */
public class XPathQuery
{
    private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.utilities.XPathQuery";
    private static Logger lm = new Logger(LOGGER_CATEGORY);
    
    public XPathQuery()
    {
    }

    public NodeList query(XMLDocument document, String xpath)
        throws Exception
    {
        Date startDate = new Date();
        if(document == null || xpath == null || xpath.equals(""))
            return null;
        NodeList nodeList = null;
        nodeList = document.selectNodes(xpath);
        long crTime = System.currentTimeMillis() - startDate.getTime();
        if(crTime > (long)1000)
        {
            lm.debug("XML Doc search time: " + crTime);
            lm.debug("XPath = " + xpath);
        }
        return nodeList;
    }

    /**
     * @deprecated Method query is deprecated
     */

    public NodeList query(String xml, String xpath)
        throws Exception
    {
        NodeList nodeList = null;
        XMLDocument document = null;
        try
        {
            document = createDocument(xml);
            nodeList = query(document, xpath);
        }
        catch(Exception e)
        {
            throw new Exception("Unable to execute Xpath Query.Specific reason: " + e.toString());
        }
        return nodeList;
    }

    /**
     * @deprecated Method createDocument is deprecated
     */

    public static XMLDocument createDocument(String xml)
        throws Exception
    {
        Date startDate = new Date();
        if(xml == null || xml.equals(""))
            return null;
        DOMParser parser = null;
        XMLDocument document = null;
        try
        {
            parser = new DOMParser();
            parser.parse(new InputSource(new StringReader(xml)));
            document = parser.getDocument();
        }
        catch(Exception e)
        {
            lm.error("Could not parse document... " + xml);
            throw new Exception(e.toString());
        }
        long crTime = System.currentTimeMillis() - startDate.getTime();
        if(crTime > (long)1000)
        {
            lm.debug("XML Doc creation time: " + crTime);
            lm.debug("XML Doc = " + xml.substring(0, 256));
        }
        return document;
    }



}