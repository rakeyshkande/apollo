package com.ftd.osp.bulkload.constants;

public interface FieldConstants  
{
    //field names as found in HTML parameters (OrderSVC, OrderParser_
    public static final String HTML_PARAM_CREDIT_CARD_YEAR = "creditCardYear";
    public static final String HTML_PARAM_CREDIT_CARD_MONTH = "creditCardMonth";
    public static final String HTML_PARAM_EXPIRATION_DATE = "creditCardExpDate";
    public static final String HTML_PARAM_MEMBERSHIP_ID = "membershipId";
    public static final String HTML_PARAM_MEMBERSHIP_TYPE = "membershipType";
    public static final String HTML_PARAM_MEMBERSHIP_FIRST_NAME = "membershipFirstName";
    public static final String HTML_PARAM_MEMBERSHIP_LAST_NAME = "membershipLastName";
    public static final String HTML_PARAM_ALLOW_MEMBERSHIP_INFO = "allowMembershipInfo";
    public static final String HTML_PARAM_MASTER_ORDER = "masterOrderNumber";
    public static final String HTML_PARAM_CONTACT_FIRSTNAME= "customerFirstname";
    public static final String HTML_PARAM_CONTACT_LASTNAME= "customerLastname";
    public static final String HTML_PARAM_CONTACT_PHONE= "customerPhone";
    public static final String HTML_PARAM_CONTACT_EXT= "customerPhoneExt";
    public static final String HTML_PARAM_CONTACT_EMAIL = "customerEmail";
    public static final String HTML_PARAM_SOURCE_CODE= "sourceCode";
    public static final String HTML_PARAM_FIRST_NAME= "billingFirstname";
    public static final String HTML_PARAM_LAST_NAME= "billingLastname";
    public static final String HTML_PARAM_BUSINESS_NAME= "businessName";
    public static final String HTML_PARAM_ADDRESS_LINE1= "billingAddressLine1";
    public static final String HTML_PARAM_ADDRESS_LINE2= "billingAddressLine2";
    public static final String HTML_PARAM_CITY = "billingCity";
    public static final String HTML_PARAM_STATE = "billingState";
    public static final String HTML_PARAM_COUNTRY= "billingCountry";
    public static final String HTML_PARAM_EMAIL_ADDRESS= "billingEmail";
    public static final String HTML_PARAM_PHONE= "billingPhone";
    public static final String HTML_PARAM_EXT= "billingPhoneExt";
    public static final String HTML_PARAM_ZIPCODE= "billingZip";
    public static final String HTML_PARAM_CREDITCARD_NUMBER= "creditCardNumber";
    public static final String HTML_PARAM_PAYMENT_TYPE= "paymentType"; 
    public static final String HTML_PARAM_FLORIST_NUMBER = "floristCode";
    public static final String HTML_PARAM_GUID = "guid";
    public static final String HTML_PARAM_FILENAME = "filename";
    public static final String HTML_PARAM_MASTER_ORDER_NUMBER = "masterOrderNumber";
    public static final String HTML_PARAM_EXIT_PAGE = "exitpage";
    public static final String HTML_PARAM_SECURITY_CONTEXT = "context";
    public static final String HTML_PARAM_SECURITY_TOKEN = "securitytoken";
    public static final String HTML_PARAM_APP_CONTEXT = "applicationcontext";
    public static final String HTML_PARAM_IDENTITY = "identity";
    public static final String HTML_PARAM_ADMIN_ACTION = "adminAction";

    //used for search windows (SearchSVC, PopupServlet)
    public final static String SEARCH_SOURCE_CODE_LOOKUP = "SEARCH_SOURCE_CODE_LOOKUP";
    public final static String SEARCH_GET_FLORIST = "SEARCH_GET_FLORIST";


    
    //field names as returned in XML (SpreadSheetUtility, OrderParser)
    public static final String FIELD_FIRST_NAME = "Recipient First Name";
    public static final String FIELD_LAST_NAME = "Recipient Last Name";
    public static final String FIELD_ADDRESS = "Recipient Address";
    public static final String FIELD_ADDRESS2 = "Recipient Address2";
    public static final String FIELD_BUSINESS = "Business Name";
    public static final String FIELD_CITY = "Recipient City";
    public static final String FIELD_STATE = "Recipient State";
    public static final String FIELD_ZIPCODE = "Recipient Zip Code";
    public static final String FIELD_COUNTRY = "Recipient Country";
    public static final String FIELD_PHONE_NUMBER = "Recipient Phone Number";
    public static final String FIELD_PHONE_EXTENSION = "Recipient Phone Extension";
    public static final String FIELD_PRODUCT_ID = "Product ID";
    public static final String FIELD_CARD_MESSAGE = "Card Message";
    public static final String FIELD_CARD_SIGNATURE = "Card Signature";
    public static final String FIELD_DELIVERY_DATE = "Delivery Date";
    public static final String FIELD_CONTACT_FIRSTNAME = "Contact First Name";
    public static final String FIELD_CONTACT_LASTNAME = "Contact Last Name";
    public static final String FIELD_CONTACT_PHONE = "Contact Phone";
    public static final String FIELD_CONTACT_EXTENSION = "Contact Extension";
    public static final String FIELD_SOURCE_CODE = "Source Code";
    public static final String FIELD_CUSTOMER_FIRST_NAME = "Customer First Name";
    public static final String FIELD_CUSTOMER_LAST_NAME = "Customer Last Name";
    public static final String FIELD_CUSTOMER_BUSINESS_NAME = "Customer Business Name";
    public static final String FIELD_CUSTOMER_ADDRESS_LINE1 = "Customer Address Line 1";
    public static final String FIELD_CUSTOMER_ADDRESS_LINE2 = "Customer Address Line 2";
    public static final String FIELD_CUSTOMER_EMAIL_ADDRESS = "Customer Email Address";
    public static final String FIELD_CUSTOMER_CITY = "Customer City";
    public static final String FIELD_CUSTOMER_STATE = "Customer State";
    public static final String FIELD_CUSTOMER_ZIPCODE = "Customer Zip Code";
    public static final String FIELD_CUSTOMER_COUNTRY = "Customer Country";
    public static final String FIELD_CUSTOMER_CREDIT_CARD = "Customer Credit Card";
    public static final String FIELD_CUSTOMER_FILE = "File";
    public static final String FIELD_CUSTOMER_EXPIRATION_DATE = "Credit Card Expiration Date";
    public static final String FIELD_CREDITCARD_TYPE = "Credit Card Type";
    public static final String FIELD_PAYMENT_FIRST_NAME = "Payment First Name";
    public static final String FIELD_PAYMENT_LAST_NAME ="Payment Last Name";
    public static final String FIELD_FILE = "File Name";
    public static final String FIELD_LINE_TOTAL = "LineTotal";
    public static final String FIELD_TAX_AMOUNT = "TaxAmount";
    public static final String FIELD_SERVICE_FEE = "ServiceFee";
    public static final String FIELD_RETAIL_PRICE = "RetailPrice";
    public static final String FIELD_PRODUCT_PRICE = "ProductPrice";
    public static final String FIELD_ADDRESSES = "Addresses";
    public static final String FIELD_SHIPPING_FEE = "ShippingFee";
    public static final String FIELD_SHIPPING_VIA = "ShippingVia";
    public static final String FIELD_SHIPPING_METHOD = "ShippingMethod";    
}