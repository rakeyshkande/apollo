package com.ftd.osp.bulkload.constants;

public interface XMLXSLConstants 
{
    //xml element names
    public static final String XML_ELEMENT_FIELD_ERROR_GROUP = "ERRORS";
    public static final String XML_ELEMENT_FIELD_ERROR_ITEM = "ERROR";
    public static final String XML_ELEMENT_ORDER = "ORDER";
    public static final String XML_ELEMENT_FIELD_GROUP = "FIELDS";
    public static final String XML_ELEMENT_FIELD = "FIELD";
    public static final String XML_ATTRIBUTE_FIELDNAME = "fieldname";
    public static final String XML_ATTRIBUTE_FIELDVALUE = "value";
    public static final String XML_ATTRIBUTE_DESCRIPTION = "description";

    //xsl file (ProcessBulkOrderServlet, SetupBulkOrderServlet)
    public static final String XSL_FILE_BULK_MAIN = "/xsl/bulkOrder.xsl";
    public static final String XSL_VALIDATION_RESULTS = "/xsl/bulkOrderValidationResults.xsl";    

}