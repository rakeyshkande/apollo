package com.ftd.osp.bulkload.constants;

/** This class contains the constants used for Bulk Order
 *  @author Ed Mueller */
public interface BulkOrderConstants 
{

    //determine whether or not security is checked
    public static final boolean SECURITY_OFF = false; //true indicates security is not being enforced

    //prefiex put on order guid
    public static final String BULK_GUID_PREFIX = "BULKTEMP_";

    //bulk order property file
    public static final String BULK_ORDER_PROPERTY_FILE = "bulkorder.xml";
    public static final String BULK_ORDER_PROPERTY_CONTEXT = "BULK_LOAD_CONFIG";

    //property value names
    public static final String STATUS_WARN_PROPERTY = "status_warn";
    public static final String STATUS_VALID_PROPERTY = "status_valid";
    public static final String STATUS_ERROR_PROPERTY = "status_error";
    public static final String ORDER_GATHERER_SUCCESS_PROPERTY = "order_gatherer_success";
    public static final String SYSTEM_ERROR_PAGE_PROPERTY = "system_error_page"; 
    public static final String CONTENT_TYPE_PROPERTY = "page_content_type";
    public static final String TRANS_DATE_FORMAT_PROPERTY ="transaction_date_format";
    public static final String BULK_ORDER_ORGIN_PROPERTY = "bulk_order_orgin";
    public static final String HTTP_ERROR_RESPONSE_PROPERTY = "http_error_response";  
    public static final String ERROR_INVALID_FILE_PROPERTY = "invalid_file_message";
    public static final String APPLICATION_CONTEXT_PROPERTY = "applicationcontext";
    public static final String ERROR_MISSING_VALUE_PROPERTY = "error_missing_value";

    public static final String ERROR_SOURCE_CODE_NOT_ACTIVE_PROPERTY = "source_code_not_active";
    public static final String ERROR_SOURCE_CODE_EXPIRED_PROPERTY = "source_code_expired";

    public static final String ERROR_INVALID_VALUE_PROPERTY  = "error_invalid_value";
    public static final String ERROR_INVALID_SOURCE_CODE_PROPERTY = "error_invalid_source_code";
    public static final String COUNTRY_CODE_CANADA_PROPERTY = "COUNTRY_CODE_CANADA";
    public static final String COUNTRY_CODE_UNITED_STATES_PROPERTY = "COUNTRY_CODE_UNITED_STATES";
    public static final String CREDIT_CARD_TYPE_PROPERTY = "payment_type_credit_card";
    public static final String PAYMENT_TYPE_GIFT_CERT_PROPERTY = "PAYMENT_TYPE_GIFT_CERT";

    public static final String LOGON_PAGE_PROPERTY = "logonpage";    
    public static final String EXIT_PAGE_PROPERTY = "exitpage";    
    public static final String ALL_ROWS_VALID_PROPERTY = "all_rows_valid_message";
    public static final String DATASOURCE_PROPERTY = "datasource";
    public static final String CONTEXT_PROPERTY = "initialContextLocation";
    public static final String LINE_BULK_STATUS_PROPERTY = "LINE_BULK_STATUS";
    public static final String ORDER_BULK_STATUS_PROPERTY = "ORDER_BULK_STATUS";    
}