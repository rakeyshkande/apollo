package com.ftd.osp.bulkload.test;

import junit.framework.TestCase;
import com.ftd.osp.bulkload.dao.OrderStore;

/** This class tests the OrderStore */
public class TestOrderStore extends TestCase  {  

  /** 
   * Create a constructor that take a String parameter and passes it 
   * to the super class 
   **/ 
  public TestOrderStore(String name) { 
      super(name);       
  } 

   /** 
    * Insert an object into the store
    **/ 
  public void testInsertOrder() throws Exception{  
      OrderStore store = OrderStore.getInstance();
      store.put("keyaa","sometext");
    }

   /** 
    * Retrieve an order from the store
    **/ 
  public void testGetOrder() throws Exception{  

      
  
      OrderStore store = OrderStore.getInstance();
      String value = (String)store.get("keyaa");
      System.out.println("Store value:" + value);
      assertTrue("Data not found in store",(value.equals("sometext"))); 
  }
  
}