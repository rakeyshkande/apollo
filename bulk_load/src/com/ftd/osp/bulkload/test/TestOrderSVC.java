package com.ftd.osp.bulkload.test;

import com.ftd.osp.bulkload.vo.OrderDocVO;
import junit.framework.TestCase;

public class TestOrderSVC  extends TestCase  
{
  /** 
   * Create a constructor that take a String parameter and passes it 
   * to the super class 
   **/ 
  public TestOrderSVC(String name) { 
      super(name);       
  } 

   /** 
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection 
    **/ 
  public void testAddDropDownData(){  
    }  

   /** 
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection 
    **/ 
  public void testGetErrorDoc(){  
    }  

   /** 
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection 
    **/ 
  public void testProcessOrder(){  
    }  

   /** 
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection 
    **/ 
  public void testValidateDetails(){  
    }  

   /** 
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection 
    **/ 
  public void testValidateHeader(){  
    }      



  private OrderDocVO createOrder(String orderType)
  {
    OrderDocVO order = new OrderDocVO();

    if(orderType == "VALID"){
      order.setAddressLine1("addr1");
      order.setAddressLine2("addr2");
      order.setBusinessName("busname");
      order.setCity("city");
      order.setContactEmail("contactemail");
      order.setContactExt("contactext");
      order.setContactFirstName("contactfname");
      order.setContactLastName("contactlname");
      order.setContactPhone("contactphone");
      order.setCountry("contactcountry");
      order.setCreditCardNumber("creditcardnumber");
      order.setEmailAddress("emailaddr");
      order.setExpirationDate("expdate");
      order.setExt("ext");
      order.setFile("file");
      order.setGuid("guid");
      order.setLastName("lastname");
      order.setPaymentFirstName("paymentfname");
      order.setPaymentLastName("plname");
      order.setPaymentType("payment type");
      order.setPhone("phone");
      order.setSourceCode("sourcecode");
      order.setState("IL");
      order.setZipCode("60563");
    }


    return order;
  }
    
}