package com.ftd.osp.bulkload.test;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.framework.Test;

/** 
 * This class contains the test suites used for the bulk order process
 * @author Ed Mueller
 **/ 
public class BulkTest extends TestCase  { 

  /** 
   * Create a constructor that take a String parameter and passes it 
   * to the super class 
   **/ 
  public BulkTest(String name) { 
      super(name); 
  } 

  /** 
   * Override setup() to initialize variables 
   **/ 
  protected void setUp(){ 

  } 

  /** 
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */ 
   public void tearDown(){ 
   } 


 
  /** 
   * Test cases for the look up DAO
   **/ 
  public static Test lookupDAOSuite(){ 
    TestSuite suite = new TestSuite(); 
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection 
    suite.addTest(new TestLookupDAO("testCountryList")); 
    suite.addTest(new TestLookupDAO("testFloristZipExists")); 
    suite.addTest(new TestLookupDAO("testGetOrder")); 
    suite.addTest(new TestLookupDAO("testNextOrderNumber")); 
    suite.addTest(new TestLookupDAO("testPaymentList")); 
    suite.addTest(new TestLookupDAO("testPaymentType")); 
    suite.addTest(new TestLookupDAO("testSourceCode")); 
    suite.addTest(new TestLookupDAO("testStateList"));     
    return suite; 
  } 

  /** 
   * Test cases for the order store
   **/ 
  public static Test orderStoreSuite(){ 
    TestSuite suite = new TestSuite(); 
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection 
    suite.addTest(new TestOrderStore("testInsertOrder")); 
    suite.addTest(new TestOrderStore("testGetOrder")); 
    
    return suite; 
  } 

  /** 
   * Test cases for the common utility object
   **/ 
  public static Test commonUtilitySuite(){ 
    TestSuite suite = new TestSuite(); 
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection 
    suite.addTest(new TestCommonUtility("testCountryList")); 
    suite.addTest(new TestCommonUtility("testFormatDates")); 
    suite.addTest(new TestCommonUtility("testFormatNull")); 
    suite.addTest(new TestCommonUtility("testGetParam")); 
    suite.addTest(new TestCommonUtility("testGetParamDouble")); 
    suite.addTest(new TestCommonUtility("testGetUniqueNumber")); 
    suite.addTest(new TestCommonUtility("testIsEmpty")); 
    suite.addTest(new TestCommonUtility("testReplaceAll")); 
    suite.addTest(new TestCommonUtility("testStringToBoolean")); 
    return suite; 
  } 

  /** 
   * Test cases to test at higher lever.  These test cases test the actuall
   * processing of a bulk order from beginning to end.
   **/ 
  public static Test highLevelSuite(){ 
    TestSuite suite = new TestSuite(); 
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection 

    suite.addTest(new TestHighLevel("testProcessValid")); 
    suite.addTest(new TestHighLevel("testEmptyFile")); 
    suite.addTest(new TestHighLevel("testNoLines"));     
    suite.addTest(new TestHighLevel("testFloristDiffAddress")); 
    suite.addTest(new TestHighLevel("testFloristSameAddress")); 
    suite.addTest(new TestHighLevel("testFloristNotInZip")); 
    suite.addTest(new TestHighLevel("testNotExcelFile")); 
    suite.addTest(new TestHighLevel("testInvalidSourceCode")); 
    suite.addTest(new TestHighLevel("testInvalidSourceCodeForInvoice")); 
    suite.addTest(new TestHighLevel("testMissingDataInFile")); 
    suite.addTest(new TestHighLevel("testReqFieldsOnPage")); 

    return suite; 
  } 

 
  /** 
   * This class runs the test suites
   **/ 
  public static void main(String[] args){ 
    // pass in the class of the test that needs to be run 

    //this this is to use the GUI...but it dosen't seem to work
    //junit.swingui.TestRunner.run(BulkTest.class); 

    junit.textui.TestRunner.run(lookupDAOSuite()); 
    junit.textui.TestRunner.run(orderStoreSuite()); 
    junit.textui.TestRunner.run(commonUtilitySuite()); 
    junit.textui.TestRunner.run(highLevelSuite()); 
   
   
  } 
  
  
} 