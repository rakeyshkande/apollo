package com.ftd.osp.bulkload.test;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.URL;
import java.util.HashMap;
import com.ftd.osp.bulkload.utilities.HTTPProxyException;
import com.ftd.osp.bulkload.utilities.HTTPProxy;
import junit.framework.TestCase;

/** This class test the high lever funcations of bulk order (processing orders). 
 * @author Ed Mueller*/
public class TestHighLevel  extends TestCase  
{

    private static final String VALIDATION_RESULTS_PAGE = "Bulk Order Vaildation Results";
    private static final String VALIDATION_ERROR = ">Error<";
    private static final String VALIDATION_WARN = ">Warn<";

    private static final String  URL = "http://192.168.110.212:8988/wwwroot/servlet/ProcessBulkOrderServlet";
    private static final String  CONTENT = "multipart/form-data";      
    private static final String  ENCRYPT = "multipart/form-data";      

    private static final String GOOD_FILE = "BulkOrderGoodFile.xls";
    private static final String EMPTY_FILE = "BulkOrderEmptyFile.xls";
    private static final String HEADER_NO_DATA_FILE = "BulkOrderOnlyHeaders.xls";
    private static final String MISSING_DATA_FILE = "BulkOrderMissingData.xls";
    private static final String DIFFERENT_ADDRESSES_FILE = "BulkOrderDifferentAddress.xls";
    private static final String SAME_ADDRESSES_FILE = "BulkOrderSameAddress.xls";
    private static final String NOT_EXCEL_FILE = "BulkOrderNotExcel.doc";

    private static final String INVALID_CELL = "Error reading data from spreadsheet cell";
    private static final String VALID_FLORIST_FOR_ZIP_60563 = "13-3215AC";
    private static final String SPEADSHEET_HAS_NO_LINES = "Spreadsheet does not contain any lines";
    private static final String ADDRESS_NOT_SAME = "All addresses are not the same";
    private static final String FLORIST_NOT_IN_ZIP = "The florist specfied does not operate in the entered zipcode";
    private static final String BAD_FILE = "Could not read spreadsheet file";
    private static final String INVALID_SOURCE_CODE = "Invalid Source Code";
    private static final String MISSING_VALUE = "Missing Value";
    private static final String INVALID_SOURCE_CODE_FOR_INVOICE = "Invalid source code for payment type";
    private static final String SOME_TEXT_FROM_MAIN_PAGE = "Lookup Source Code";

    private static final String FILE_PATH = "C:\\aPhase2\\BulkOrder\\";

    private static final String IGNORE_THIS_ERROR = "The order processing system could not process the bulk order";

  /** 
   * Create a constructor that take a String parameter and passes it 
   * to the super class 
   **/ 
  public TestHighLevel(String name) { 
      super(name); 
  }
  
   /** 
    * Process valid order
    **/ 
  public void testProcessValid() throws Exception{  
      HTTPProxy http = new HTTPProxy();
      String response = send(URL,getValidMap(),CONTENT,ENCRYPT);
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if((response.indexOf(VALIDATION_ERROR) > 0 && response.indexOf(IGNORE_THIS_ERROR) <= 0) || response.indexOf(VALIDATION_WARN) > 0){
          assertTrue("Unexpected errors.",false);
       }
       
  }      


   /** 
    * Test sending an empty file...should get error about having no lines
    **/ 
  public void testEmptyFile() throws Exception{  

      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + EMPTY_FILE);
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(SPEADSHEET_HAS_NO_LINES) <=0){
          assertTrue("Did not get error.",false);
       }
       
  }    

   /** 
    * Test sending an file with only header..should get error about no lines
    **/ 
  public void testNoLines() throws Exception{  

      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + HEADER_NO_DATA_FILE);
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(SPEADSHEET_HAS_NO_LINES) <=0){
          assertTrue("Did not get error for having no lines.",false);
       }
       
  }    

   /** 
    * Test sending order that specifies florist, but all addreses
    * are not the same...this is an error condition.
    **/ 
  public void testFloristDiffAddress() throws Exception{  

      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + DIFFERENT_ADDRESSES_FILE);
      map.put("floristCode",VALID_FLORIST_FOR_ZIP_60563);
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(ADDRESS_NOT_SAME) <=0){
          assertTrue("Did not get error.",false);
       }
       
  }    

   /** 
    * Test order with florist specified and all addreses are the same.
    **/ 
  public void testFloristSameAddress() throws Exception{  

      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + SAME_ADDRESSES_FILE);
      map.put("floristCode",VALID_FLORIST_FOR_ZIP_60563);
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if((response.indexOf(VALIDATION_ERROR) > 0 && response.indexOf(IGNORE_THIS_ERROR) <= 0) || response.indexOf(VALIDATION_WARN) > 0){
          assertTrue("Unexpected errors.",false);
       }
       
  }    


   /** 
    * Test sending an order with a florist specifed that does
    * not service the entered zip code.  this is a warn condition.
    **/ 
  public void testFloristNotInZip() throws Exception{  

      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + SAME_ADDRESSES_FILE);
      map.put("floristCode","X");
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(FLORIST_NOT_IN_ZIP) <=0){
          assertTrue("Did not get error.",false);
       }

      //reponse should indicate the file was processed ok
       if((response.indexOf(VALIDATION_ERROR) > 0 && response.indexOf(IGNORE_THIS_ERROR) <= 0)){
          assertTrue("Unexpected errors.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(VALIDATION_WARN) <= 0){
          assertTrue("Did not get warn.",false);
       }       
       
  }    

  /** Test uploading a non-excel file*/
  public void testNotExcelFile() throws Exception{  
      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + NOT_EXCEL_FILE);
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(BAD_FILE) <= 0){
          assertTrue("Did not get error msg.",false);
       }
       
  }      

  /** test entering an invalid source code*/
  public void testInvalidSourceCode() throws Exception{  
      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("sourceCode","X");
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(INVALID_SOURCE_CODE) <= 0){
          assertTrue("Did not get error msg.",false);
       }
       
  }      

  /** test entering a source code and payment type of invoice.
   *  the source code should not be valid for type of invoice. */
  public void testInvalidSourceCodeForInvoice() throws Exception{  
      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("sourceCode","5643");
      map.put("paymentType","IN");
      String response = send(URL,map,CONTENT,ENCRYPT);    
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf(INVALID_SOURCE_CODE_FOR_INVOICE) <= 0){
          assertTrue("Did not get error msg.",false);
       }
       
  }      



   /** 
    * Test sending an order with missing values in the spreadsheet
    **/ 
  public void testMissingDataInFile() throws Exception{  

      //get a valid map and change the file name
      Map map = getValidMap();
      map.put("filename",FILE_PATH + MISSING_DATA_FILE);
  
      String response = send(URL,map,CONTENT,ENCRYPT);    
      
      System.out.println(response);
      
      //response should go to validate page
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("Directed to wrong page.",false);
       }

      //reponse should indicate the file was processed ok
       if(response.indexOf("Missing Value: Recipient Country") <=0){
          assertTrue("Missing Value: Recipient Country",false);
       }
       if(response.indexOf("Missing Value: Delivery Date") <=0){
          assertTrue("Missing Value: Delivery Date",false);
       }
       if(response.indexOf("Missing Value: Recipient Phone Number") <=0){
          assertTrue("Missing Value: Recipient Phone Number",false);
       }
       if(response.indexOf("Missing Value: Card Message") <=0){
          assertTrue("Missing Value: Card Message",false);
       }
       if(response.indexOf("Missing Value: Recipient City") <=0){
          assertTrue("Missing Value: Recipient City",false);
       }
       if(response.indexOf("Missing Value: Product ID") <=0){
          assertTrue("Missing Value: Product ID",false);
       }
       if(response.indexOf("Missing Value: Product ID") <=0){
          assertTrue("Missing Value: Product ID",false);
       }
       if(response.indexOf("Missing Value: Recipient Address") <=0){
          assertTrue("Missing Value: Recipient Address",false);
       }
       if(response.indexOf("All Information Found") <=0){
          assertTrue("All Information Found",false);
       }       
       if(response.indexOf("Missing Value: Recipient Zip Code") <=0){
          assertTrue("Missing Value: Recipient Zip Code",false);
       }       
       if(response.indexOf("Missing Value: Recipient Last Name") <=0){
          assertTrue("Missing Value: Recipient Last Name",false);
       }       
       if(response.indexOf("Missing Value: Card Signature") <=0){
          assertTrue("Missing Value: Card Signature",false);
       }              
  }    

  /** test all fields on page to make sure user is prompted
   * when they did not enter something */
 public void testReqFieldsOnPage() throws Exception{  

      //first send 1 valid, then take turns removing data

      Map map = getValidMap();  
      String response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("VALID.Directed to wrong page.",false);
       }

      map = getValidMap();  
      map.put("sourceCode","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("sourceCode.Directed to wrong page.",false);
       }


      map = getValidMap();  
      map.put("customerFirstname","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("customerFirstname.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("customerLastname","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("customerLastname.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("customerPhone","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("customerPhone.Directed to wrong page.",false);
       }       


        map = getValidMap();  
       map.put("billingFirstname","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingFirstname.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("billingLastname","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingLastname.Directed to wrong page.",false);
       }       

  
       map = getValidMap();  
       map.put("billingEmail","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingEmail.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("billingAddressLine1","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingAddressLine1.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("billingPhone","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingPhone.Directed to wrong page.",false);
       }       

       map = getValidMap();  
       map.put("billingState","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingState.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("billingCountry","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingCountry.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("billingCity","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingCity.Directed to wrong page.",false);
       }

       map = getValidMap();  
       map.put("billingZip","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("billingZip.Directed to wrong page.",false);
       }       

       map = getValidMap();  
       map.put("paymentType","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("paymentType.Directed to wrong page.",false);
       }

      //need expiration for credit card
       map = getValidMap();  
       map.put("paymentType","AX");
       map.put("creditCardExpDate","");
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("need expiration for credit card.Directed to wrong page.",false);
       }

      //need card number for credit card
       map = getValidMap();  
       map.put("paymentType","AX");
       map.put("creditCardNumber","");       
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) > 0){
          assertTrue("need card number for credit card.Directed to wrong page.",false);
       }       

       //don't need card num or exp date for invoice
      //need expiration for credit card
       map = getValidMap();  
       map.put("paymentType","IN");
       map.put("sourceCode","2377");       
       map.put("creditCardNumber","");       
       map.put("creditCardExpDate","");
       response = send(URL,map,CONTENT,ENCRYPT);  
       System.out.println(response);
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("don't need card num or exp date for invoice.Directed to wrong page.",false);
       }       
       
      //end with a valid one
       map = getValidMap();  
       response = send(URL,map,CONTENT,ENCRYPT);          
       if(response.indexOf(VALIDATION_RESULTS_PAGE) <= 0){
          assertTrue("VALID2.Directed to wrong page.",false);
       }              
  }      


  private Map getValidMap()
  {
    Map map = new HashMap();
    map.put("guid","");
    map.put("sourceCode","5643");
    map.put("floristCode","");
    map.put("customerFirstname","CustFName");
    map.put("customerLastname","CLName");
    map.put("customerPhone","CPhone");
    map.put("customerPhoneExt","CExt");
    map.put("customerEmail","CEmail@mail.com");
    map.put("billingFirstname","BillFName");
    map.put("billingLastname","BillLName");
    map.put("businessName","BusName");
    map.put("billingEmail","BusEmail");
    map.put("billingAddressLine1","BilAddr1");
    map.put("billingPhone","BillPhone");
    map.put("billingPhoneExt","BillPhoneExt");
    map.put("billingAddressLine2","BillAddr2");
    map.put("billingState","BillState");
    map.put("billingCity","BillCity");    
    map.put("billingCountry","BillCountry");
    map.put("billingZip","BillZip");
    map.put("paymentType","AX");    
    map.put("creditCardNumber","CreditNum");
    map.put("creditCardExpDate","Exp");    
    map.put("filename","C:\\aPhase2\\BulkOrder\\BulkOrderGoodFile.xls");
    return map;
  }

  private Map getValidMap2()
  {
    Map map = new HashMap();
    map.put("guid","");
    map.put("sourceCode","5643");
    map.put("floristCode","");
    map.put("customerFirstname","CustFName");
    map.put("customerLastname","CLName");
    map.put("customerPhone","CPhone");
    map.put("customerPhoneExt","CExt");
    map.put("customerEmail","CEmail@mail.com");
    map.put("billingFirstname","BillFName");
    map.put("billingLastname","BillLName");
    map.put("businessName","BusName");
    map.put("billingEmail","BusEmail");
    map.put("billingAddressLine1","BilAddr1");
    map.put("billingPhone","BillPhone");
    map.put("billingPhoneExt","BillPhoneExt");
    map.put("billingAddressLine2","BillAddr2");
    map.put("billingState","BillState");
    map.put("billingCity","BillCity");    
    map.put("billingCountry","BillCountry");
    map.put("billingZip","BillZip");
    map.put("paymentType","AX");    
    map.put("creditCardNumber","CreditNum");
    map.put("creditCardExpDate","Exp");    
    map.put("filename","C:\\aPhase2\\BulkOrder\\BulkOrderTemplate.xls");
    return map;
  }

private synchronized String send(String urlString, Map params, String contentType, String encrypt) 
        throws Exception{

        URLConnection con = null;
        boolean firstx      = true;
        
        try {
            URL url = new URL(urlString);

            //create connection to URL
            con = url.openConnection(); 
            //con.setRequestProperty("Content-Type", " " + contentType);
            con.setRequestProperty("enctype", " " + encrypt);
            con.setDoOutput(true); 

            //Send data
            OutputStream out = con.getOutputStream();
            Set entries = params.entrySet();
            Iterator iter = entries.iterator();

            StringBuffer paramString = new StringBuffer();
            while(iter.hasNext()) {
                if (firstx)
                {
                  firstx = false;
                }
                else
                {
                    paramString.append('&');
                }
            
                Map.Entry entry = (Map.Entry)iter.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();

                paramString.append(key);
                paramString.append('=');
                paramString.append(URLEncoder.encode(value));
            }

            out.write(paramString.toString().getBytes());                
            out.flush();
            out.close();

        	InputStream is = con.getInputStream();

            //8/21/03...add this so the response would be returned
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line=null;
            StringBuffer response = new StringBuffer("");
            while( (line = br.readLine())!=null)
            {
                response.append(line);
            }

            return response.toString();
        
        }          
         catch (java.net.MalformedURLException e)  {
                throw new HTTPProxyException(e.toString());
        }
         catch (java.io.IOException e)  {
                throw new HTTPProxyException(e.toString());
        }
        finally{
            con = null;
        }
    
    }//end, send      



}