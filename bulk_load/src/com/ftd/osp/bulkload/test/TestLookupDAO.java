package com.ftd.osp.bulkload.test;


import com.ftd.osp.bulkload.dao.LookupDAO;
import com.ftd.osp.bulkload.utilities.XPathQuery;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import oracle.xml.parser.v2.XMLDocument;
import junit.framework.TestCase;

/** 
 * This class tests the lookup DAO.
 * @author Ed Mueller
 **/ 
public class TestLookupDAO extends TestCase  { 

  private static LookupDAO lookup;

  /** 
   * Create a constructor that take a String parameter and passes it 
   * to the super class 
   **/ 
  public TestLookupDAO(String name) { 
      super(name); 
      
  } 

 /** 
   * Override setup() to initialize variables 
   **/ 
  protected void setUp(){ 
    if(lookup == null)
    {
      lookup = new LookupDAO(); 
    }
  } 


   /** 
    * Obtain a list of countries and make sure the required elements in the XML.
    **/ 
  public void testCountryList() throws Exception{  
 
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        //countries
        doc = lookup.getCountries();
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/COUNTRIES/COUNTRY";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("No countries found",false);
        }        
        value = data.getAttribute("country_id");
        assertTrue("No ID",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("name");
        assertTrue("No Value",!(value == null || value.equalsIgnoreCase("")));      
  } 

   /** 
    * Obtain a list of staes and make sure the required elements in the XML.
    **/ 
  public void testStateList() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        doc = lookup.getStates();
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/STATES/STATE";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("No States Found",false);
        }        
        value = data.getAttribute("statemasterid");
        assertTrue("No state_master_id",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("statename");
        assertTrue("No state_name",!(value == null || value.equalsIgnoreCase("")));      


  } 
 
   /** 
    * Obtain a given payment type and make sure the required elements in the XML.
    **/ 
  public void testPaymentType() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        doc = lookup.getPaymentType("VI");
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/PAYMENT_TYPES/PAYMENT_TYPE";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("No Payment type of VI found",false);
        }        
        value = data.getAttribute("paymentmethodid");
        assertTrue("No paymentmethodid",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("description");
        assertTrue("No description",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("paymenttype");
        assertTrue("No paymenttype",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("cardid");
        assertTrue("No cardid",!(value == null || value.equalsIgnoreCase("")));      

  }  

   /** 
    * Do a query to determine if a florist exists for the given zipcode.
    **/ 
  public void testFloristZipExists() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        boolean exists = lookup.floristZipExist("01-3532AA","35242");
        assertTrue("Florist Zip Not Found",exists);

  } 

   /** 
    * Test obtaining the next order number
    **/ 
  public void testNextOrderNumber() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        value = lookup.getNextOrderNumber();
        System.out.println("Order Number:" + value);
        assertTrue("No Order Number",!(value == null || value.equalsIgnoreCase("")));      

  }   

   /** 
    * Obtain a list of orders and the details regarding and order
    * from the database and make sure the required elements in the XML.
    **/ 
  public void testGetOrder() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        //get order list
        doc = lookup.getOrders(new java.sql.Date(new java.util.Date().getTime()));
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/orders/order";
        q = new XPathQuery();
        nl = q.query(doc, xpath);      
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("No bulk orders found in database.",false);
        }
        value = data.getAttribute("createdon");
        assertTrue("No createdon",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("masterordernumber");
        String masterOrderNumber = value;
        assertTrue("No masterordernumber",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("sourcecode");
        assertTrue("No sourcecode",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("billingname");
        assertTrue("No billingname",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("submitted_total");
        assertTrue("No submitted_total",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("processed_total");
        assertTrue("No processed_total",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("scrubbed_total");
        assertTrue("No scrubbed_total",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("status");
        assertTrue("No status",!(value == null || value.equalsIgnoreCase("")));             

        //get details for that order
        doc = lookup.getOrderDetails(masterOrderNumber);
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/orders/order";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);        
        if (data==null) 
        {
          assertTrue("Detailes do not exist for master order " + masterOrderNumber,false);
        }
        value = data.getAttribute("orderid");
        assertTrue("No orderid",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("productid");
        assertTrue("No productid",!(value == null || value.equalsIgnoreCase("")));     
        value = data.getAttribute("name");
        assertTrue("No name",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("deliverydate");
        assertTrue("No deliverydate",!(value == null || value.equalsIgnoreCase("")));     
        value = data.getAttribute("address");
        assertTrue("No address",!(value == null || value.equalsIgnoreCase("")));             
        value = data.getAttribute("status");
        assertTrue("No status",!(value == null || value.equalsIgnoreCase("")));             

   }     

   /** 
    * Obtain a list of payment types and make sure the required elements in the XML.
    **/ 
  public void testPaymentList() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        doc = lookup.getPaymentTypes(null);
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/PAYMENT_TYPES/PAYMENT_TYPE";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("No Payment types found",false);
        }        
        value = data.getAttribute("payment_type");
        assertTrue("No payment_type",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("payment_method_id");
        assertTrue("No payment_method_id",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("description");
        assertTrue("No description",!(value == null || value.equalsIgnoreCase("")));      

  }     

   /** 
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection 
    **/ 
 /* public void testProduct() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        doc = lookup.getProduct("7038");
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/PRODUCTS/PRODUCT";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("Product 7038 not found",false);
        }        
        value = data.getAttribute("product_id");
        assertTrue("No product_id",!(value == null || value.equalsIgnoreCase("")));
        //to many fields to check

  } */  

   /** 
    * Obtain a given source code and make sure the required elements in the XML.
    **/ 
  public void testSourceCode() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

        doc = lookup.getSourceCode("5539");
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/SOURCECODES/SOURCECODE";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("Source Code 5539 not found",false);
        }        
        value = data.getAttribute("valid_pay_method");
        assertTrue("No valid_pay_method",!(value == null || value.equalsIgnoreCase("")));
        //to many fields to check

  }  

   /** 
    * Obtain a given state and make sure the required elements in the XML.
    **/ 
  public void testState() throws Exception{   
        String xpath = "";
        XPathQuery q = null;
        String value = "";
        Element data = null; 
        NodeList nl = null;
        XMLDocument doc = null;

    //    doc = lookup.getState("AB");
        System.out.println(XMLUtility.convertDocToString(doc));
        xpath = "/STATES/STATE";
        q = new XPathQuery();
        nl = q.query(doc, xpath);
        data = (Element) nl.item(0);
        if (data==null) 
        {
          assertTrue("State AB not Found",false);
        }        
        value = data.getAttribute("id");
        assertTrue("No ID",!(value == null || value.equalsIgnoreCase("")));
        value = data.getAttribute("value");
        assertTrue("No Value",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("countrycode");
        assertTrue("No countrycode",!(value == null || value.equalsIgnoreCase("")));      
        value = data.getAttribute("tax_rate");
        assertTrue("No tax_rate",!(value == null || value.equalsIgnoreCase("")));      
        
  } 
  
} 