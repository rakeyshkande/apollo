package com.ftd.osp.bulkload.test;

import java.text.ParseException;
import java.util.Map;
import java.util.HashMap;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import junit.framework.TestCase;

/** This class contains test routines for the common utility
 * @author Ed Mueller */
public class TestCommonUtility  extends TestCase  { 

  /** 
   * Create a constructor that take a String parameter and passes it 
   * to the super class 
   **/ 
  public TestCommonUtility(String name) { 
      super(name); 
  }

   /** 
    * This method should return an empty string when a null is passed 
    * to it.  If any other value is passed to it should return that value back.
    **/ 
  public void testFormatNull() {    

      String stringValue = "XXX";
      String stringNull = null;
      String emptyValue = "";
      String newValue;

      newValue = CommonUtility.formatNull(stringValue);      
      assertTrue("Fail with value test",(newValue.equals(stringValue))); 

      newValue = CommonUtility.formatNull(stringNull);            
      assertTrue("Fail null test",(newValue.equals(emptyValue))); 

      newValue = CommonUtility.formatNull(emptyValue);   
      assertTrue("Fail empty test",(newValue.equals(emptyValue)));       
  }

   /** 
    * This method tests the methods used to format a date from a string to 
    * a java.util.Date and also for doing the reverse operation.
    **/ 
  public void testFormatDates() throws ParseException{  
      java.util.Date today = new java.util.Date();

      //convert to string
      String todayString = CommonUtility.formatUtilDateToString(today);

      //convert back to date
      java.util.Date todayDate = CommonUtility.formatStringToUtilDate(todayString);

      //convert to string
      String todayNewString = CommonUtility.formatUtilDateToString(todayDate);

      //should be the same
      assertTrue("Date format error",(todayString.equals(todayNewString)));       

      //invalid date return null
      java.util.Date invalidDate = CommonUtility.formatStringToUtilDate("XX");
      assertTrue("Date format null error",(invalidDate == null));       
      
  }


   /** 
    * This method gets a string parameter from the passed in map of strings.
    * If the map does not contain the value it returns an empty string.
    **/ 
  public void testGetParam() {    

    Map map = new HashMap();
    map.put("1","111");
    map.put("2",null);

    String value;
    value = CommonUtility.getParam(map,"1");
    assertTrue("Fail..map has value",(value.equals("111")));       
    value = CommonUtility.getParam(map,"2");
    assertTrue("Fail...map has null value",(value.equals("")));       
    value = CommonUtility.getParam(map,"3");    
    assertTrue("Fail...map does not have value",(value.equals("")));       
  }

   /** 
    * This method gets a double value form a map.  If the map does not
    * contain the value it will return a zero.  If the requested value
    * is not a double then an exception is thrown.
    **/ 
  public void testGetParamDouble() {    

    Map map = new HashMap();
    map.put("1","1.11");
    map.put("2",null);
    map.put("3","A");
    map.put("4","");

    double value;
    value = new Double(CommonUtility.getParamDouble(map,"1")).doubleValue();
    assertTrue("Fail..map has value",(value== 1.11));       
    value = new Double(CommonUtility.getParamDouble(map,"2")).doubleValue();
    assertTrue("Fail...map has null value",(value== 0)); 

    boolean errorHappened = false;
    try{
        value = new Double(CommonUtility.getParamDouble(map,"3")).doubleValue();
    }
    catch(Exception e)
    {
      //exception expected
      errorHappened = true;
    }
    assertTrue("Fail...map does not have double",errorHappened);       
        
    value = new Double(CommonUtility.getParamDouble(map,"4")).doubleValue();
    assertTrue("Fail...map has empty value",(value== 0));       
    value = new Double(CommonUtility.getParamDouble(map,"5")).doubleValue();
    assertTrue("Fail...map does not have value",(value== 0));       
  }


   /** 
    * This method generates a unique number.
    **/ 
  public void testGetUniqueNumber() {    

    long value1 = CommonUtility.getUniqueNumber();
    long value2 = CommonUtility.getUniqueNumber();
    assertTrue("Nothing returned 1",!(value1 == 0));    
    assertTrue("Nothing returned 2",!(value2 == 0));    
    assertTrue("Same value returned",!(value1 == value2));        
  }

   /** 
    * This method should return false if the passed in value is an empty string,
    * null, or all spaces.  Else is should return true.
    **/ 
  public void testIsEmpty() {    
  
      String testValue = "XX";
      String testNull = null;
      String testEmpty = "";
      String testSpaces = "  ";

      if(CommonUtility.isEmpty(testValue))
      {
        assertTrue("Fail..value was passed in",false);
      }

      if(!CommonUtility.isEmpty(testNull))
      {
        assertTrue("Fail..value was null",false);        
      }

      if(!CommonUtility.isEmpty(testEmpty))
      {
        assertTrue("Fail..value was empty",false);        
      }

      if(!CommonUtility.isEmpty(testSpaces))
      {
        assertTrue("Fail...value was spaces",false);        
      }


  }

   /** 
    * This method should replace chars in a given string with a given value.
    **/ 
  public void testReplaceAll() {    

    String start1 = "XXXXAAAXXXX";
    String replace1 = "A";
    String replaceWith1 = "Z";
    String end1 = "XXXXZZZXXXX";

    String start2 = "EDWASHERE";
    String replace2 = "WAS";
    String replaceWith2 = "IS";
    String end2 = "EDISHERE";
    
    String value;

    value = CommonUtility.replaceAll(start1,replace1,replaceWith1);
    assertTrue("Single char replace fail",(value.equals(end1))); 
    value = CommonUtility.replaceAll(start2,replace2,replaceWith2);
    assertTrue("Multi char replace fail",(value.equals(end2)));     

  }

   /** 
    * This method should return boolean TRUE if the passed in value
    * is a string of TRUE(any case) or ON(any case).  Else it should
    * return false.
    **/ 
  public void testStringToBoolean() {    

      String testTrue1 = "true";
      String testTrue2 = "TRUE";
      String testOn1 = "On";
      String testFalse1 = "false";
      String testOther1 = "XXX";      
      String testEmpty = "";      
      String testNull = null;          

      assertTrue("lower case true fail",(CommonUtility.stringToBoolean(testTrue1)));     
      assertTrue("upper case true fail",(CommonUtility.stringToBoolean(testTrue2)));     
      assertTrue("on case fail",(CommonUtility.stringToBoolean(testOn1)));     
      assertTrue("false case fail",!(CommonUtility.stringToBoolean(testFalse1)));     
      assertTrue("other case fail",!(CommonUtility.stringToBoolean(testOther1)));     
      assertTrue("empty caes fail",!(CommonUtility.stringToBoolean(testEmpty)));     
      assertTrue("null case fail",!(CommonUtility.stringToBoolean(testNull)));     

      
  }

  
  
}