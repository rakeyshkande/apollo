package com.ftd.osp.bulkload.services;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import com.ftd.osp.bulkload.dao.LookupDAO;
import java.util.Map;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.bulkload.constants.FieldConstants;

/** This object contains the service methods needed to 
 *  conduct the source code and florist lookup.
 *  @author Copied From OrderEntry PhaseI
 *  
 *  Note: Some methods were removed from the PhaseI version
 *  of this object.  A change was also made to the database
 *  connection logic. Ed Mueller
 */
public class SearchSVC implements FieldConstants
{

    private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.services.SearchSVC";


    

  /** Constructor.   */   
    public SearchSVC()
    {

    }


    /** This method is the starting point for all search service
     *  requests.  This method will excute the proper search
     *  method based on the command passed in.
     *  @param String command The search to perform
     *  @param Map The arguements
     *  @returns XMLDocument the search results
     *  @throws SQLException 
     *  @throws IOException
     *  @throws ParserConfigurationException
     *  @throws Exception*/
    public XMLDocument process(String command, Map arguments) 
      throws SQLException,IOException,ParserConfigurationException,Exception
	{

        XMLDocument doc = null;
        LookupDAO lookup = new LookupDAO();

        if(command.equals(SEARCH_SOURCE_CODE_LOOKUP)) {
            doc = lookup.getSourceCodeByValue(arguments);
        }
        else if(command.equals(SEARCH_GET_FLORIST)) {
            doc = lookup.getFlorist(arguments);
        }

        return doc;
	}

	public  void initialize()
	{}

	public  String toString()
	{
		return null;
	}







}