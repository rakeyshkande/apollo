package com.ftd.osp.bulkload.services;

import com.ftd.osp.ordervalidator.util.DataMassager;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.constants.XMLXSLConstants;
import com.ftd.osp.bulkload.dao.LookupDAO;
import com.ftd.osp.bulkload.utilities.BulkOrderException;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import com.ftd.osp.bulkload.utilities.HTTPProxy;
import com.ftd.osp.bulkload.utilities.OrderParser;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.utilities.XPathQuery;
import com.ftd.osp.bulkload.vo.LineItemVO;
import com.ftd.osp.bulkload.vo.LineVO;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import com.ftd.osp.bulkload.vo.ValidationVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.text.ParseException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XSLException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;



/** This class contains the functions\services performed on an order.
 * @author Ed Mueller */
public class OrderSVC implements FieldConstants,BulkOrderConstants,XMLXSLConstants
{
 private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.services.OrderSVC";

    private static final String SOURCE_CODE_PATH = "/SOURCECODES/SOURCECODE";

    private static final String SOURCE_STARTDATE_ATTRIBUTE = "start_date";
    private static final String SOURCE_ENDDATE_ATTRIBUTE = "end_date";
    private static final String SOURCE_CODE_ATTRIBUTE = "source_code";
    private static final String SOURCE_PAY_TYPE_ATTRIBUTE = "valid_pay_method";
    private static final String SOURCE_PARTNER_ATTRIBUTE = "partner_id";
    private static final String PAYMENT_PATH = "/PAYMENT_TYPES/PAYMENT_TYPE";
    private static final String PAYMENT_TYPE_ATTRIBUTE = "payment_type";

    private static final String CONSTANT_TRANSACTION_DATE_FORMAT="yyyyMMddHHmmsszzz";

    //pay types which should not be displayed on bulk order screen
    private static final String SKIP_PAY_TYPES_PROPERTY = "donotshowpaymenttypes";

    private Logger logger;

     //list of field which are not required to have values
    private String FIELDS_NOT_REQUIRED;

    //property file values
    private static final String VALID_ROW_MESSAGE_PROPERTY = "valid_row_message";
    private static final String VALID_FIELD_MESSAGE_PROPERTY = "valid_field_message";
    private static final String ERROR_ADDRESS_MATCH_MESSAGE_PROPERTY = "error_address_match_message";
    private static final String ERROR_ZIPFLORIST_MESSAGE_PROPERTY = "error_zipflorist_message";
    private static final String ERROR_FILE_DOES_NOT_EXIST_PROPERTY = "error_file_does_not_exist";
    private static final String ERROR_SOURCE_CODE_NOT_VALID_FOR_INVOICE_PROPERTY = "error_source_code_not_valid_for_invoice";
    private static final String ERROR_SOURCE_CODE_NOT_VALID_FOR_CREDIT_PROPERTY = "error_source_code_not_valid_for_credit";
    private static final String ERROR_INVALID_DATE_PROPERTY = "error_invalid_date";
    private static final String ERROR_DELIVERY_DATE_PAST_PROPERTY = "error_delivery_date_past";

    private static final String INVOICE_TYPE_PROPERTY = "payment_type_invoice";
    private static final String ORDER_GATHERER_HTTP_PARAMETER = "order_gatherer_param";
    private static final String ERROR_NO_LINES_MESSAGE_PROPERTY = "no_lines_error";

    //xml names
    private static final String XML_SUMMARIES = "SUMMARIES";
    private static final String XML_SUMMARY = "SUMMARY";

    private static final String PAYMENT_XPATH = "/PAYMENT_TYPES/PAYMENT_TYPE";
    private static final String PAYMENT_ID_ATTRIBUTE = "payment_method_id";

    private static final String ORDER_GATHERER_PROPERTY = "ordergathererurl";

    //date from configuration file
    private String statusWarn;
    private String statusError;
    private String statusValid;
    private String orderGathererSuccess;
    private String systemErrorPage;
    private String contentType;
    private String creditCardType;
    private String invoiceType;
    private String orderGathererParam;

    /** Constructor */
    public OrderSVC() throws Exception
    {
        //get data from configuration file
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        statusValid = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_VALID_PROPERTY);
        statusWarn = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_WARN_PROPERTY);
        statusError = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_ERROR_PROPERTY);
        contentType = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,CONTENT_TYPE_PROPERTY);
        systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);
        creditCardType = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,INVOICE_TYPE_PROPERTY);
        invoiceType = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,CREDIT_CARD_TYPE_PROPERTY);
        orderGathererParam = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ORDER_GATHERER_HTTP_PARAMETER);

        logger =  new Logger(LOGGER_CATEGORY);

    }

    /** This method processes an order.  This involves generating order XML
     * and send the XML to an HTTP Servlet (Order Gatherer)
     * @param OrderDocVO order to send
     * @param String url to send order to
     * @returns String the http response message
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws TransformerException
     * @throws XSLException
     * @throws SQLException
     * @throws BulkOrderException
     * @throws Exception
     * */
    public String processOrder(OrderDocVO orderVO)
        throws IOException, SAXException, ParserConfigurationException,
                TransformerException,XSLException,SQLException,
                BulkOrderException,Exception
    {

        //get order gatherer url
        ConfigurationUtil cnfig = ConfigurationUtil.getInstance();
        String url = cnfig.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,ORDER_GATHERER_PROPERTY);
        String orderGathererParam = cnfig.getProperty(BULK_ORDER_PROPERTY_FILE,ORDER_GATHERER_HTTP_PARAMETER);
        String datasource = cnfig.getProperty(BULK_ORDER_PROPERTY_FILE,DATASOURCE_PROPERTY);
        String initialConextLocation = cnfig.getProperty(BULK_ORDER_PROPERTY_FILE,CONTEXT_PROPERTY);


        //get connection
         DataSourceUtil dsUtil = DataSourceUtil.getInstance();
         Connection conn = dsUtil.getConnection(datasource, initialConextLocation);

        //convert order to OrderScrub version of VO
        try{

            OrderVO ospVO = OrderParser.convertOrder(orderVO);
            //massage it
            ospVO = DataMassager.massageOrderData(ospVO,conn);
            //now convert back to bulk version of vo
            orderVO = OrderParser.convertOrder(ospVO);
        }
        catch(Throwable t)
        {
          //if an error is thrown during massaging keep going, let the order go to scrub
          logger.error("Order could not be massaged.  Processing will continue.");
          logger.error(t);
        }

        finally
        {
          //close the connection
          try
          {
            conn.close();
          }
          catch(Exception e)
          {}
        }
  
        //convert order to XML
        XMLDocument orderXML = null;
        orderXML = OrderParser.getXMLforGatherer(orderVO);

        //get string representatin of XML
        String orderString = "";

        orderString = XMLUtility.convertDocToString(orderXML);

        //send order to order gatherer
        HTTPProxy http = new HTTPProxy();
        String httpResponse = "";
        try
        {
          logger.debug("Sending to order gatherer:" + orderString);
          Map map = new HashMap();
          map.put(orderGathererParam,orderString);

          //this was added to help in testing
          if(!url.equals("TEST")){
            httpResponse = http.send(url,map);
          }
          else
          {
            httpResponse = "NOTHING WAS SENT..IN TEST MODE";
          }

          logger.debug("HTTP Response:" + httpResponse);
        }
        catch(Throwable e)
        {
          String msg = "Error sending order to order processing system.";
          logger.error(msg);
          logger.error(e);
          throw new BulkOrderException(msg);
        }

        return orderVO.getMasterOrderNumber();
    }

    /** Append the drop down data to the passed in XMLDocument.
     * This is the drop down data is that is used to populate
     * the drop downs on the web page
     * @param XMLDocument The xml document to paste the data onto
     * @param boolean add state data
     * @param boolean add country data
     * @param boolean add payment data
     * @param orderDocVO current order..used to determine contents of some drop downs
     * @throws SQLException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws TransformerException
     * @throws Exception*/
     public void addDropDownData(XMLDocument xmlDoc,boolean addState,
          boolean addCountry, boolean addPayment, OrderDocVO order)
          throws SQLException,IOException,ParserConfigurationException,
              SAXException,TransformerException,Exception
     {
          Logger logger = new Logger(LOGGER_CATEGORY);
          LookupDAO lookup = new LookupDAO();

          //determine the current source code...default to null if we do not have one
          String sourceCode = null;
          if(order != null && order.getSourceCode() != null && order.getSourceCode().length() > 0)
          {
            sourceCode = order.getSourceCode();
          }

          if(addState){
              //retreive the states
              XMLDocument states = null;
              states = lookup.getStates();
              xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(states.getDocumentElement(),true));
          }

          if(addCountry){
              //retrieve the countries
              XMLDocument countries = null;
              countries = lookup.getCountries();
              xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(countries.getDocumentElement(),true));
          }

          if(addPayment){

              //get list of payment types not to display
              String skipPayTypes = "";
              ConfigurationUtil config = ConfigurationUtil.getInstance();
              skipPayTypes = config.getProperty(BULK_ORDER_PROPERTY_FILE,SKIP_PAY_TYPES_PROPERTY);

              //retrieve the payment types
              XMLDocument payments = null;

              payments = lookup.getPaymentTypes(sourceCode);

              //remove certain payment types which should not be displayed
              Element data = null;
              String xpath = PAYMENT_XPATH;
              XPathQuery q = new XPathQuery();
              NodeList nl = q.query(payments, xpath);
              for (int i = 0; i < nl.getLength() ; i++)
              {
                  data = (Element) nl.item(i);
                  String value = data.getAttribute(PAYMENT_ID_ATTRIBUTE);
                  if(skipPayTypes.indexOf(value) >= 0){
                    payments.getFirstChild().removeChild(data);
                  }
              }


              xmlDoc.getDocumentElement().appendChild(xmlDoc.importNode(payments.getDocumentElement(),true));

          }

     }

  /** This method generates an error xml document based on the
   *  passed in exception.
   *  @param String Exception error that occured
   *  @return XMLDocument error document
   *  @throws IOException*/
   public XMLDocument getErrorDoc(String msg)
      throws IOException
   {
      XMLDocument doc = null;
      try{
        doc = XMLUtility.createXMLDocument("error");
      }
      catch(Exception e)
      {
        //return null if object cannot be created.
        return null;
      }

      //create validation object
      ValidationVO val = new ValidationVO();
      val.setField("ORDER");
      val.setMessage(msg);
      val.setType(statusError);

      //create map containing error
      Map data = new HashMap();
      data.put("error",val);

      //put validation in XML
      addValidationMapToDoc(doc,data);

      return doc;
   }

     /** This method get a list of validation details for the passed in order object.
      * This method validates the line items.
      * @param OrderDocVO order to be validated
      * @param XMLDocument which will be populated with validation results
      * @return String indicates valdiation results of ERROR,WARN,VALID
      * @throws IOException
      * @throws SAXException
      * @throws TransformerException
      * @throws ParserConfigurationException
      * @throws XSLException
      * @throws ParseException
      * @throws SQLException
      * @throws Exception*/
    public String validateDetails(OrderDocVO order, XMLDocument xmlDoc)
        throws IOException,SAXException,TransformerException,
                ParserConfigurationException,XSLException,ParseException,
                SQLException,Exception{

            //these are the line item fields which are NOT required on the spreadsheet.
            FIELDS_NOT_REQUIRED = FIELD_PHONE_EXTENSION + "," + FIELD_BUSINESS;

            int validCount = 0;

            ConfigurationUtil config = ConfigurationUtil.getInstance();
            String missingValue = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_MISSING_VALUE_PROPERTY);
            String invalidValue = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_INVALID_VALUE_PROPERTY);

            //boolean indicates whether or not errors were found on this order
            boolean errorOnOrder = false;
            boolean warnOnOrder = false;

            //used to lookup values in DB
            LookupDAO lookup = new LookupDAO();

            //list of messages which will be returned
            List errors = new ArrayList();

            //get order lines
            List lines = order.getLines();

            String val = "";

            //create line validation detail element
            Element lineDetailElement = XMLUtility.createElement(xmlDoc,"LINE_DETAIL");

            //create hashmap to store line validations...used to create summary element
            Map lineValidationMap = new HashMap();

            boolean elementsContainsData = true;

            //check each line
            for(int i=0;i<lines.size();i++)
            {
                LineVO line = (LineVO)lines.get(i);

                boolean errorWithLine = false;


                //check each line items
                List lineItems = line.getLineItems();
                for(int x=0;x<lineItems.size();x++)
                {
                  LineItemVO lineItem = (LineItemVO)lineItems.get(x);

                  //reset missing value flag
                  elementsContainsData = true;

                  //if field is required
                  if(FIELDS_NOT_REQUIRED.indexOf(lineItem.getFieldName()) < 0)
                  //if(lineItem.getFieldName().indexOf(FIELDS_NOT_REQUIRED) < 0)
                  {
                      //check for missing value
                      if(lineItem.getValue().trim().length() == 0)
                      {
                        elementsContainsData = false;
                        addValidationToMap(lineValidationMap, lineItem.getFieldName(),missingValue + ": " + lineItem.getFieldName(),statusError);
                        addLineElement(xmlDoc,lineDetailElement,i,missingValue + ": " + lineItem.getFieldName(),statusError,lineItem.getFieldName());
                        errorWithLine = true;//indicates and error was found on this line
                      }//end if missing value
                  }//end if required

                  //Entered state must be valid
                  if(elementsContainsData && lineItem.getFieldName().equals(FIELD_STATE))
                  {
                     //check if values exists in DB
                     if(lookup.getState(lineItem.getValue()).getRowCount() <= 0)
                     {
                        addValidationToMap(lineValidationMap, lineItem.getFieldName(),invalidValue + ": " + lineItem.getFieldName(),statusError);
                        addLineElement(xmlDoc,lineDetailElement,i,invalidValue + ": " + lineItem.getFieldName(),statusError,lineItem.getFieldName());
                        errorWithLine = true;//indicates and error was found on this line
                     }
                  }

                  //Entered country must be valid
                  if(elementsContainsData && lineItem.getFieldName().equals(FIELD_COUNTRY))
                  {
                     //check if values exists in DB
                     if(lookup.getCountry(lineItem.getValue()).getRowCount() <= 0)
                     {
                        addValidationToMap(lineValidationMap, lineItem.getFieldName(),invalidValue + ": " + lineItem.getFieldName(),statusError);
                        addLineElement(xmlDoc,lineDetailElement,i,invalidValue + ": " + lineItem.getFieldName(),statusError,lineItem.getFieldName());
                        errorWithLine = true;//indicates and error was found on this line
                     }
                  }

                  //Entered product must be valid
                  if(elementsContainsData && lineItem.getFieldName().equals(FIELD_PRODUCT_ID))
                  {
                     //check if values exists in DB
                     if(lookup.getProduct(lineItem.getValue()).getRowCount() <= 0)
                     {

                        //maybe it's a subcode, check that table
                        if(lookup.getSubcode(lineItem.getValue()).getRowCount() <= 0){
                          addValidationToMap(lineValidationMap, lineItem.getFieldName(),invalidValue + ": " + lineItem.getFieldName(),statusError);
                          addLineElement(xmlDoc,lineDetailElement,i,invalidValue + ": " + lineItem.getFieldName(),statusError,lineItem.getFieldName());
                          errorWithLine = true;//indicates and error was found on this line
                        }
                     }
                  }

                  //delivery date must be a valid date which is not in the past
                  if(lineItem.getFieldName().equals(FIELD_DELIVERY_DATE) &&
                        lineItem.getValue().trim().length() > 0)
                  {
                      String value = lineItem.getValue();
                      Date deliveryDate = null;
                      try{
                          deliveryDate = CommonUtility.formatStringToUtilDate(value);
                      }
                      catch(Throwable t)
                      {
                        //catch invalid date parsing, but continue processing
                        logger.error("Invalid date found, processing will continue.");
                        logger.error(t);
                      }
                      //if returned value is null then date is invalid
                      if(deliveryDate == null)
                      {
                        String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_INVALID_DATE_PROPERTY);
                        addValidationToMap(lineValidationMap, lineItem.getFieldName(),msg + ": " + lineItem.getFieldName(),statusError);
                        addLineElement(xmlDoc,lineDetailElement,i,missingValue + ": " + lineItem.getFieldName(),statusError,lineItem.getFieldName());
                        errorWithLine = true;//indicates and error was found on this line
                      }
                      else
                      {
                        //make sure date is not in the past
                        deliveryDate = CommonUtility.removeTime(deliveryDate);
                        Date rightNow = CommonUtility.removeTime(new Date());
                        if(deliveryDate.before(rightNow)){
                            String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_DELIVERY_DATE_PAST_PROPERTY);
                            addValidationToMap(lineValidationMap, lineItem.getFieldName(),msg + ": " + lineItem.getFieldName(),statusError);
                            addLineElement(xmlDoc,lineDetailElement,i,missingValue + ": " + lineItem.getFieldName(),statusError,lineItem.getFieldName());
                            errorWithLine = true;//indicates and error was found on this line
                        }
                      }
                  }

                }//end for each line item

              //if errors occured on the line
              if(errorWithLine){
                //set order level error flag
                errorOnOrder = true;
              }
              else
              {
                validCount++;
                //addLineElement(xmlDoc,lineDetailElement,i,msg,statusValid,"");
              }



            }//end for each line


        //if a florist number was specified then all addresses must be the
        //same and the florist number entered must be valid for the zipcode
        if(order.getFloristNumber() != null && order.getFloristNumber().length() > 0){

              //check that all addresses are the same
              if(isAddressErrors(order,xmlDoc) )
              {
                String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_ADDRESS_MATCH_MESSAGE_PROPERTY);
                addValidationToMap(lineValidationMap,FIELD_ADDRESSES,msg,statusError);
                errorOnOrder = true;
              }

              //check that zip code is valid
              //**Note...this is a warning condition
              if(isFloristZipError(order))
              {
                String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_ZIPFLORIST_MESSAGE_PROPERTY);
                addValidationToMap(lineValidationMap, FIELD_ZIPCODE,msg,statusWarn);
                warnOnOrder = true;
              }
        }//end florist number validation

          //where all the rows valid..if so say something about the orders being sent
          //add valid element to doc & map
          String validMsg = null;
          if(lineValidationMap.size() == 0){
              validMsg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ALL_ROWS_VALID_PROPERTY);
          }
          else {
              validMsg = config.getProperty(BULK_ORDER_PROPERTY_FILE,VALID_ROW_MESSAGE_PROPERTY);
              }
          if(validCount > 0){
            addValidationToMap(lineValidationMap, "",validMsg,statusValid,validCount);
          }


        //if order contains no lines display error
        if(lines.size() == 0)
        {
          String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_NO_LINES_MESSAGE_PROPERTY);
          addValidationToMap(lineValidationMap, "",msg,statusError);
          errorOnOrder = true;
        }



        //add line summary(the map) to document
        addValidationMapToDoc(xmlDoc,lineValidationMap);

        //determine order status
        String status = statusValid;
        if(warnOnOrder)
        {
          status = statusWarn;
        }
        if(errorOnOrder)
        {
          status = statusError;
        }

        return status;
    }


    /* This method adds a validation map to the validation XML
     * @param XMLDocument document to which validation is added.
     * @param Map contains validation VOs */
    private void addValidationMapToDoc(XMLDocument xmlDoc, Map valMap)
    {

        Element lineParentElement = xmlDoc.createElement(XML_SUMMARIES);

        Collection valCollection = valMap.values();
        Iterator iter = valCollection.iterator();
        while(iter.hasNext())
        {
           ValidationVO vo = (ValidationVO)iter.next();

            //build the summary line
            Element lineElement = xmlDoc.createElement(XML_SUMMARY);
            lineElement.setAttribute(XML_ELEMENT_FIELD,vo.getField());
            lineElement.setAttribute("MESSAGE",vo.getMessage());
            lineElement.setAttribute("STATUS",vo.getType());
            lineElement.setAttribute("COUNT",new Integer(vo.getCount()).toString());

            //put field element under line
            lineParentElement.appendChild(lineElement);
        }

        xmlDoc.getDocumentElement().appendChild(lineParentElement);
    }

    /* This method adds the line error to a hashmap.
     * If the error already exists in the hashamp
     * ** count is not a param
     * then the error count will be increment.
     * @param String field name
     * @param String error message
     * @param String type */
     private void addValidationToMap(Map errorMap, String field, String msg, String type)
     {
      addValidationToMap(errorMap,field,msg,type,1);
     }

    /* This method adds the line error to a hashmap.
     * If the error already exists in the hashamp
     * then the error count will be increment.
     * @param String field name
     * @param String error message
     * @param String type
     * @count number to put in map*/
     private void addValidationToMap(Map errorMap, String field, String msg, String type, int count)
     {

        //key is a concatentation of message and field name
        String key = field + msg;

        //attempt to get error from map
        ValidationVO vo = (ValidationVO)errorMap.get(key);

        //if not found add it
        if(vo == null)
        {
          vo = new ValidationVO();
          vo.setMessage(msg);
          vo.setType(type);
          vo.setCount(count);
          vo.setField(field);
          errorMap.put(key,vo);
        }
        else
        {
          vo.increment();
        }
     }


    /* This method checks to make sure that the zipcode code enteres is valid
     * for the florist entered.  Only the zipcode of the first line is checked.
     * The zipcodes on every line should be the same.
     * @returns true if error found
     * @throws SQLException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws Exception*/
     private boolean isFloristZipError(OrderDocVO order)
        throws SQLException,IOException,ParserConfigurationException,
                Exception
     {
        boolean error = false;

       //get lines from the order
       List lines = order.getLines();

        //if no lines return false
        if(lines == null || lines.size() < 1)
        {
          return error;
        }

       //get first zipcode
       LineVO line = (LineVO)lines.get(0);
       Map itemMap = line.getLineItemsMap();
       String firstZipCode = (String)itemMap.get(FIELD_ZIPCODE);

       //check db for zip code & florist #
       LookupDAO lookup = new LookupDAO();
       if(!lookup.floristZipExist(order.getFloristNumber(),firstZipCode))
       {
         error = true;
       }

       return error;

     }

    /* This method checks to make sure every address in the order is the same.
     * @param OrderDocVO the order to validate
     * @param XMLDocument xml file to put errors in
     * @returns true if error found */
     private boolean isAddressErrors(OrderDocVO order, XMLDocument xmlDoc)
     {
        boolean error = false;

        //string which contains all lines not matching the first
        String errorLines = "";

       //get lines from the order
       List lines = order.getLines();

        //if no lines return false
        if(lines == null || lines.size() < 1)
        {
          return error;
        }

       //get first address
       LineVO line = (LineVO)lines.get(0);
       Map itemMap = line.getLineItemsMap();
       String firstAddress1 = (String)itemMap.get(FIELD_ADDRESS);
       String firstCity = (String)itemMap.get(FIELD_CITY);
       String firstState = (String)itemMap.get(FIELD_STATE);
       String firstZipCode = (String)itemMap.get(FIELD_ZIPCODE);
       String firstCountry = (String)itemMap.get(FIELD_COUNTRY);

       //go through each line
       for(int i=1;i<lines.size();i++){
          line = (LineVO)lines.get(i);
          itemMap = line.getLineItemsMap();

          String compareAddress1 = (String)itemMap.get(FIELD_ADDRESS);
          String compareCity = (String)itemMap.get(FIELD_CITY);
          String compareState = (String)itemMap.get(FIELD_STATE);
          String compareZipCode = (String)itemMap.get(FIELD_ZIPCODE);
          String compareCountry = (String)itemMap.get(FIELD_COUNTRY);

          //compare values
          if(!(compareAddress1.trim().equalsIgnoreCase(firstAddress1) &&
             compareCity.trim().equalsIgnoreCase(firstCity) &&
             compareState.trim().equalsIgnoreCase(firstState) &&
             compareZipCode.trim().equalsIgnoreCase(firstZipCode) &&
             compareCountry.trim().equalsIgnoreCase(firstCountry)) )
          {
            //add comma if needed
            if(!errorLines.equals(""))
            {
              errorLines = errorLines + ",";
            }
            errorLines = errorLines + (i+1);
          }

       }

      if (!errorLines.equals(""))
      {
        error = true;
      }

        return error;
     }




    /** This method peforms the validation on the order header.  Validation
     * messages will be added to the XML document.
     * @param OrderDocVO order to be validated
     * @param XMLDocument document to add validation elements to
     * @throws BulkOrderException
     * @returns boolean true if error found
     * @throws IOException
     * @throws SAXException
     * @throws TransformerException
     * @throws ParserConfigurationException\
     * @throws XSLException
     * @throws SQLException
     * @throws Exception*/
    public boolean validateHeader(OrderDocVO order, XMLDocument xmlDoc)
        throws IOException,SAXException,TransformerException,
            ParserConfigurationException,XSLException,SQLException,Exception
    {
      boolean errorFound = false;
      LookupDAO lookup = new LookupDAO();

      ConfigurationUtil config = ConfigurationUtil.getInstance();
      String missingValue = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_MISSING_VALUE_PROPERTY);
      String httpErrorResponse = config.getProperty(BULK_ORDER_PROPERTY_FILE,HTTP_ERROR_RESPONSE_PROPERTY);
      String creditCardType = config.getProperty(BULK_ORDER_PROPERTY_FILE,CREDIT_CARD_TYPE_PROPERTY);
      String invoiceType = config.getProperty(BULK_ORDER_PROPERTY_FILE,INVOICE_TYPE_PROPERTY);

      //parent header validation element
      Element errorsParent = XMLUtility.createElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_GROUP);

      //required field - Contact First Name
      if (CommonUtility.isEmpty(order.getContactFirstName())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                    missingValue,statusError,HTML_PARAM_CONTACT_FIRSTNAME);
        errorFound = true;
      }

      //required field - Contact Last Name
      if (CommonUtility.isEmpty(order.getContactLastName())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_CONTACT_LASTNAME);
        errorFound = true;
      }

      //required field - Contact Phone
      if (CommonUtility.isEmpty(order.getContactPhone())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_CONTACT_PHONE);
        errorFound = true;
      }

      //required field - Billing Phone
      if (CommonUtility.isEmpty(order.getPhone())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_PHONE);
        errorFound = true;
      }

      //required field - Source code
      String sourceCodeValidPaymentType = "";
      if (CommonUtility.isEmpty(order.getSourceCode())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_SOURCE_CODE);
        errorFound = true;
      }
      else
      {
        //obtain source code record
        XMLDocument sourceCodeDoc = null;

        sourceCodeDoc = lookup.getSourceCode(order.getSourceCode());
        boolean sourceCodeError = false;
        String sourceCode = XMLUtility.getAttributeValue(sourceCodeDoc,
                    SOURCE_CODE_PATH,SOURCE_CODE_ATTRIBUTE);
        if(sourceCode == null || sourceCode.length() < 1)
        {
          String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_INVALID_SOURCE_CODE_PROPERTY);
          addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
              msg,statusError,HTML_PARAM_SOURCE_CODE);
          errorFound = true;
        }
        else
        {
          //obtain payment type value for future validation
          sourceCodeValidPaymentType = XMLUtility.getAttributeValue(sourceCodeDoc,
                SOURCE_CODE_PATH,SOURCE_PAY_TYPE_ATTRIBUTE);

          //check if source code requires membership information
          String partnerId = XMLUtility.getAttributeValue(sourceCodeDoc,
                SOURCE_CODE_PATH,SOURCE_PARTNER_ATTRIBUTE);

          //store partner in order
          order.setMembershipType(partnerId);

          //obtain source code dates
          String sourceCodeStartString = XMLUtility.getAttributeValue(sourceCodeDoc,
                SOURCE_CODE_PATH,SOURCE_STARTDATE_ATTRIBUTE);
          String sourceCodeEndString = XMLUtility.getAttributeValue(sourceCodeDoc,
                SOURCE_CODE_PATH,SOURCE_ENDDATE_ATTRIBUTE);
          Date sourceStartDate = null;
          Date sourceEndDate = null;
          try{
              if(sourceCodeStartString != null && sourceCodeStartString.length() > 0){
                  sourceStartDate = CommonUtility.formatStringToUtilDate(sourceCodeStartString);
                  sourceStartDate = CommonUtility.removeTime(sourceStartDate);
              }
              if(sourceCodeEndString != null && sourceCodeEndString.length() > 0){
                  sourceEndDate = CommonUtility.formatStringToUtilDate(sourceCodeEndString);
                  sourceEndDate = CommonUtility.removeTime(sourceEndDate);
              }
          }
          catch(Throwable t)
          {
            //catch invalid date parsing, but continue processing
            logger.error("Invalid source code date found, processing will continue.");
            logger.error(t);
          }

          //get today's date
          Date today = new Date();
          today = CommonUtility.removeTime(today);

          //check if source code is not valid yet
          if(!sourceCodeError && sourceStartDate != null && sourceStartDate.after(today))
          {
            //error
            sourceCodeError = true;
            String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_SOURCE_CODE_NOT_ACTIVE_PROPERTY);
            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                msg,statusError,HTML_PARAM_SOURCE_CODE);
            errorFound = true;
          }

          //check if source code is not valid yet
          if(!sourceCodeError && sourceEndDate != null && sourceEndDate.before(today))
          {
            //error
            sourceCodeError = true;
            String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_SOURCE_CODE_EXPIRED_PROPERTY);
            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                msg,statusError,HTML_PARAM_SOURCE_CODE);
            errorFound = true;
          }


          //if partner id not null membership info must be entered
          if(!sourceCodeError && partnerId != null && partnerId.length() > 0)
          {
            order.setAllowMembershipInfo("Y");
            if (CommonUtility.isEmpty(order.getMembershipID())) {
              addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                  missingValue,statusError,HTML_PARAM_MEMBERSHIP_ID);
              errorFound = true;
            }

            if (CommonUtility.isEmpty(order.getMembershipFirstName())) {
              addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                  missingValue,statusError,HTML_PARAM_MEMBERSHIP_FIRST_NAME);
              errorFound = true;
            }

            if (CommonUtility.isEmpty(order.getMembershipLastName())) {
              addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                  missingValue,statusError,HTML_PARAM_MEMBERSHIP_LAST_NAME);
              errorFound = true;
            }

          }//partner not null
          else
          {
            //no membership info
            order.setAllowMembershipInfo("N");
          }

        }

      }

      //required field - Customer First Name
      if (CommonUtility.isEmpty(order.getFirstName())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_FIRST_NAME);
        errorFound = true;
      }

      //required field - Customer Last Name
      if (CommonUtility.isEmpty(order.getLastName())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_LAST_NAME);
        errorFound = true;
      }

      //required field - Customer address 1
      if (CommonUtility.isEmpty(order.getAddressLine1())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_ADDRESS_LINE1);
        errorFound = true;
      }

      //required field - customer city
      if (CommonUtility.isEmpty(order.getCity())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_CITY);
        errorFound = true;
      }

      //required field - customer state
      if (CommonUtility.isEmpty(order.getState())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_STATE);
        errorFound = true;
      }

      //required field - customer zip code
      if (CommonUtility.isEmpty(order.getZipCode())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_ZIPCODE);
        errorFound = true;
      }

      //required field - customer email
      if (CommonUtility.isEmpty(order.getEmailAddress())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
              missingValue,statusError,HTML_PARAM_EMAIL_ADDRESS);
        errorFound = true;
      }

      //required field - customer country
      if (CommonUtility.isEmpty(order.getCountry())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_COUNTRY);
        errorFound = true;
      }

      //required field - payment type
      if (CommonUtility.isEmpty(order.getPaymentType()))  {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_PAYMENT_TYPE);
        errorFound = true;
      }
      else
      {
        //if payment type is credit card then a credit number and expiration date
        //must be entered
        String paymentType = "";

        XMLDocument payDoc = lookup.getPaymentType(order.getPaymentType());
        paymentType = XMLUtility.getAttributeValue(payDoc,PAYMENT_PATH,PAYMENT_TYPE_ATTRIBUTE);

        //store the payment method
        order.setPaymentMethod(paymentType);

        if(paymentType.equalsIgnoreCase(creditCardType)){

            //verify that credit card is valid for this source code
            //if valid payment type is empty then all payment types are allowed
            if(sourceCodeValidPaymentType!= null && sourceCodeValidPaymentType.length() > 0 && !sourceCodeValidPaymentType.equalsIgnoreCase(creditCardType))
                        {
                        String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_SOURCE_CODE_NOT_VALID_FOR_CREDIT_PROPERTY);
                        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                            msg,
                            statusError,HTML_PARAM_SOURCE_CODE);
                        errorFound = true;
                        }


          //required field - customer credit card
          if (CommonUtility.isEmpty(order.getCreditCardNumber()))  {
            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                missingValue,statusError,HTML_PARAM_CREDITCARD_NUMBER);
            errorFound = true;
          }

          //required field - customer credit card expiration date
          boolean expirationError = false;
          if (CommonUtility.isEmpty(order.getCreditCardMonth()))  {
            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                missingValue,statusError,HTML_PARAM_CREDIT_CARD_MONTH);
            errorFound = true;
            expirationError = true;
          }
          if (CommonUtility.isEmpty(order.getCreditCardYear()))  {
            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                missingValue,statusError,HTML_PARAM_CREDIT_CARD_YEAR);
            errorFound = true;
            expirationError = true;
          }
          //if either month or yeas was invalid place expiration error in XML
          if(expirationError)
          {
            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                missingValue,statusError,HTML_PARAM_EXPIRATION_DATE);
          }
        }
        //else if payment types is invoice
        else if(order.getPaymentType().equals(invoiceType))
        {
            //verify that invoice is valid for this source code
            //if valid payment type is empty then all payment types are allowed
                if(sourceCodeValidPaymentType.length() > 0 && !sourceCodeValidPaymentType.equalsIgnoreCase(invoiceType))
                            {
                            String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_SOURCE_CODE_NOT_VALID_FOR_INVOICE_PROPERTY);
                            addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                                msg,
                                statusError,HTML_PARAM_SOURCE_CODE);
                            errorFound = true;
                            }
        }
      }//end if payment type blank

      //required field - file
      if (CommonUtility.isEmpty(order.getFile())) {
        addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
            missingValue,statusError,HTML_PARAM_FILENAME);
        errorFound = true;
      }
      else
      {
        //check if file was determined to be invalid
        if(order.getFile().equals("INVALID"))
        {
          String msg = config.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_INVALID_FILE_PROPERTY);
          addHeaderElement(xmlDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
              msg,statusError,HTML_PARAM_FILENAME);
          errorFound = true;
        }
      }

      //append errors to doc
      xmlDoc.getDocumentElement().appendChild(errorsParent);

      return errorFound;
    }

    /** This methods adds a validation field\header element to the XML document.
     * @param XMLDocument the document to add the element to
     * @param String name of the element to add
     * @param Element parent to which this node should be appended
     * @param String msg the validation message
     * @param String status the status message
     * @fieldname String the field name */
    public static void addHeaderElement(XMLDocument xmlDoc,
        String elementName,Element parent, String msg, String status,
        String fieldName)
    {
            Map attributes = new HashMap();
            attributes.put(XML_ATTRIBUTE_FIELDNAME,fieldName);
            //attributes.put("STATUS",status);
            attributes.put(XML_ATTRIBUTE_DESCRIPTION,msg);

            //put this guy into document
            XMLUtility.addField(xmlDoc,parent,elementName,attributes);
    }

    /* This methods adds a validation line element to the XML document.
     * @param XMLDocument the document to add the element to
     * @param int the line number that corresonds to row in Excel spreadsheet
     * @param String msg the validation message
     * @param String status the status message
     * @fieldname String the field name */
    private void addLineElement(XMLDocument xmlDoc, Element parent,int lineNumber, String msg, String status,String fieldName)
    {
            //build the line
            Element lineElement = xmlDoc.createElement("LINE");
            lineElement.setAttribute("NUMBER", Integer.toString(lineNumber + 1));

            //build the field
            Element fieldElement = xmlDoc.createElement(XML_ELEMENT_FIELD);
            fieldElement.setAttribute(XML_ATTRIBUTE_FIELDNAME,fieldName);
            fieldElement.setAttribute("STATUS",status);
            fieldElement.setAttribute("DESCRIPTION",msg);

            //put field element under line
            lineElement.appendChild(fieldElement);

            //put this guy into document
            //xmlDoc.getDocumentElement().appendChild(lineElement);
            parent.appendChild(lineElement);

    }






}