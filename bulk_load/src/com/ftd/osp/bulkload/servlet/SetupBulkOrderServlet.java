package com.ftd.osp.bulkload.servlet;

import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.constants.XMLXSLConstants;
import com.ftd.osp.bulkload.dao.LookupDAO;
import com.ftd.osp.bulkload.dao.OrderStore;
import com.ftd.osp.bulkload.services.OrderSVC;
import com.ftd.osp.bulkload.utilities.BulkOrderException;
import com.ftd.osp.bulkload.utilities.OrderParser;
import com.ftd.osp.bulkload.utilities.ServletHelper;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;

public class SetupBulkOrderServlet 
    extends HttpServlet implements FieldConstants,XMLXSLConstants,BulkOrderConstants
{

  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.SetupBulkOrderServlet";
  private ServletConfig config;
    
  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger(LOGGER_CATEGORY);
    this.config = config;
    super.init(config);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    process(request,response);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    process(request,response);
  }
  
  public void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {  

        XMLDocument doc = null;
        String xslFile = "";
        OrderSVC orderSVC = null;
        String systemErrorPage = null;
        String logonPage = null;
        String exitPage = null;
        String applicationContext = null;
        String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
        if(securityContext == null)
        {
          securityContext = "";
        }

        String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
        if(securityToken == null)
        {
          securityToken = "";
        }
        String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
        if(adminAction == null)
        {
          adminAction = "";
        }


        logger.debug("securityContext=" + securityContext);
        logger.debug("securityToken=" + securityToken);
        logger.debug("adminAction=" + adminAction);        
    
        try{

          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);          
          applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    
          logonPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,EXIT_PAGE_PROPERTY);    
          
          if (SECURITY_OFF || ServletHelper.isValidToken(request)) 
          {
              
              LookupDAO lookup = new LookupDAO();
              orderSVC = new OrderSVC();

              //get order from store
              String orderGuid = request.getParameter(HTML_PARAM_GUID);
              OrderDocVO order = null;
              if(orderGuid != null && orderGuid.length() > 0){
                order = OrderStore.getInstance().getOrder(orderGuid);  

                //if order is not null check if there is membership data
                if(order != null)
                {
                  if(order.getMembershipID() != null && order.getMembershipID().length() > 0)
                  {
                    order.setAllowMembershipInfo("Y");
                  }
                }
              }

              //combine documents results
              doc = XMLUtility.createXMLDocument("bulkorder");
              orderSVC.addDropDownData(doc,true,true,true,order);

              //add order if ones was found
              Element orderElement = OrderParser.getXMLforWeb(doc,order);
              doc.getDocumentElement().appendChild(orderElement);     
    
            //do xsl translation
            logger.debug(XMLUtility.convertDocToString(doc));

            xslFile = getServletContext().getRealPath(XSL_FILE_BULK_MAIN);


          }
          else
          {
            //failed logon
            logger.error("Logon Failed");
            response.sendRedirect(logonPage);
            return;         
          }
        }
        catch(BulkOrderException e)
        {
          //error occured redirect to validation page and show error
          logger.error(e);
          xslFile = getServletContext().getRealPath(XSL_VALIDATION_RESULTS);
          doc = orderSVC.getErrorDoc(e.getMessage());
        }
      catch(ExpiredIdentityException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;                  
      }
      catch(ExpiredSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
        catch(Throwable t)
        {
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;        
        }

        try{
          //generate response
          File f = new File(xslFile);
          HashMap params = new HashMap();
          TraxUtil trax = TraxUtil.getInstance();   
          params.put(HTML_PARAM_ADMIN_ACTION, adminAction);              
          params.put(HTML_PARAM_APP_CONTEXT, applicationContext);   
          params.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);   
          params.put(HTML_PARAM_SECURITY_TOKEN,securityToken);
          params.put(HTML_PARAM_EXIT_PAGE,exitPage);          
          trax.transform(request,response,doc,f,params);      
        }
        catch(Throwable t)
        {
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;        
        }
  }
}
  


