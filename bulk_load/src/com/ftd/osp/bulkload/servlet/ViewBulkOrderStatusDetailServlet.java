package com.ftd.osp.bulkload.servlet;

import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.bulkload.utilities.ServletHelper;
import com.ftd.osp.utilities.ConfigurationUtil;
import javax.servlet.RequestDispatcher;
import java.io.File;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.dao.LookupDAO;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.osp.utilities.plugins.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.bulkload.constants.FieldConstants;

public class ViewBulkOrderStatusDetailServlet  
    extends HttpServlet implements BulkOrderConstants,FieldConstants
{
  private Logger logger;
  private final static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.ViewBulkOrderStatusDetailServlet";
  private final static String XSL_FILE_BULK_STATUSDETAIL = "/xsl/bulkOrderStatusDetail.xsl";
  
  /** This method creates a logger objects.
   *  @param ServletConfig */
  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger(LOGGER_CATEGORY);
    super.init(config);
  }

  /** Receives servlet post requests. 
   * @param HttpServletRequest request
   * @param HttpServletReponse reponse */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {  
    process(request,response);
  }

  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {
    process(request,response);
  }

  /** This method get and displays bulk order details.  It is called from both
   * the doGet & doPost methods.
   * @param HTTPServletRequest request
   * @param HTTPServletResponse response */
   private void process(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException  
      {

    String systemErrorPage = "";
    String logonPage = "";    
    String applicationContext = "";


    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    if(securityContext == null)
    {
      securityContext = "";
    }
    
    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);    

    try{
      if (SECURITY_OFF || ServletHelper.isValidToken(request)) 
      {    


        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);    
        logonPage= configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,LOGON_PAGE_PROPERTY);    
        applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    
        
        //get master order number from request
        String masterOrderNumber = request.getParameter(HTML_PARAM_MASTER_ORDER_NUMBER);

        //retrieve orders with this master order
        XMLDocument doc = XMLUtility.createXMLDocument("viewbulkorder");

        XMLDocument data = new LookupDAO().getOrderDetails(masterOrderNumber);
        doc.getDocumentElement().appendChild(doc.importNode(data.getDocumentElement(),true));      

        //generate output via xsl translation
        logger.debug(XMLUtility.convertDocToString(doc));
        HashMap params = new HashMap();
        TraxUtil trax = TraxUtil.getInstance();      
        String xslFile = getServletContext().getRealPath(XSL_FILE_BULK_STATUSDETAIL);
        params.put(HTML_PARAM_APP_CONTEXT, applicationContext);    
        params.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);    
        params.put(HTML_PARAM_SECURITY_TOKEN,securityToken);        
        trax.transform(request,response,doc,new File(xslFile),params);    
      }//end if secure
     else{
            //failed logon
            logger.error("Logon Failed");
            response.sendRedirect(logonPage);
            return;                 
            }      
    }
      catch(ExpiredIdentityException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;                  
      }
      catch(ExpiredSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
    catch(Throwable e)
    {
      logger.error(e);
      RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
      dispatcher.forward(request, response);        
      return;      
    }
      
      }
}