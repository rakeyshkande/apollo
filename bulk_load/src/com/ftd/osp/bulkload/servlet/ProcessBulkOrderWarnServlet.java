package com.ftd.osp.bulkload.servlet;

import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.bulkload.utilities.ServletHelper;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import com.ftd.osp.bulkload.utilities.BulkOrderException;
import com.ftd.osp.utilities.ConfigurationUtil;
import javax.servlet.RequestDispatcher;
import com.ftd.osp.utilities.xml.TraxUtil;
import java.util.HashMap;
import java.io.File;
import com.ftd.osp.bulkload.utilities.XMLUtility;
//import com.ftd.osp.bulkload.utilities.OrderParser;
import org.w3c.dom.Element;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.bulkload.services.OrderSVC;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.dao.OrderStore;
import com.ftd.osp.bulkload.constants.XMLXSLConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;

/** This servlet receives requests to process a bulk order which contains a warning.
 * @author Ed Mueller. */
public class ProcessBulkOrderWarnServlet 
    extends HttpServlet implements XMLXSLConstants,BulkOrderConstants,FieldConstants
{

  private Logger logger;
  private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.ProcessBulkOrderWarnServlet";

  private static final String VALID_SUMMARY_ROW_MESSAGE = "Valid Row";

  //from property file
  private String statusWarn;
  private String statusValid;  
  private String systemErrorPage;
  private String applicationContext;
  private String logonPage;
  private String exitPage;
    
  /** Servlet Init method. Log object created. 
   * @param ServletConfig config*/
  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger(LOGGER_CATEGORY);
    super.init(config);
  }

  /** Receives servlet post requests. 
   * @param HttpServletRequest request
   * @param HttpServletReponse reponse */
  public void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {  

    String xslFile = getServletContext().getRealPath(XSL_VALIDATION_RESULTS);
    String orderGuid = "";
    OrderDocVO order = null;
    OrderSVC orderSVC = null;
    XMLDocument orderXML = null;

    String masterOrderNumber = "";
    String csrId = "";

    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    if(securityContext == null)
    {
      securityContext = "";
    }

    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN); 
    if(securityToken == null)
    {
      securityToken = "";
    }       
    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
    if(adminAction == null)
    {
      adminAction = "";
    }

    
    try{    

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();
        String orderGathererSuccess = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ORDER_GATHERER_SUCCESS_PROPERTY);
        statusWarn = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_WARN_PROPERTY);
        statusValid = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_VALID_PROPERTY);
        systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);    
        applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    

        logonPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,LOGON_PAGE_PROPERTY); 
        String allGoodMessage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ALL_ROWS_VALID_PROPERTY);                                  
        exitPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,EXIT_PAGE_PROPERTY);    
          
      if (SECURITY_OFF || ServletHelper.isValidToken(request)) 
      {       
       
        //get order from store
        orderGuid = request.getParameter(HTML_PARAM_GUID);
        order = OrderStore.getInstance().getOrder(orderGuid);

        orderSVC = new OrderSVC();

        //process order
        masterOrderNumber = orderSVC.processOrder(order);
        csrId = ServletHelper.getUserId(securityToken);
        
        //create summary xml
        orderXML = createXMLSummary(order.getLines().size(),allGoodMessage);          

      }//end if secure
     else{
            //failed logon
            logger.error("Logon Failed");
            response.sendRedirect(logonPage);
            return;                 
            }      

    }
      catch(BulkOrderException e)
      {
        //error occured redirect to validation page and show error
        OrderStore.getInstance().setOrder(orderGuid,order);
        logger.error("Error processing order:" + e);
        xslFile = getServletContext().getRealPath(XSL_VALIDATION_RESULTS);
        orderXML = orderSVC.getErrorDoc(e.getMessage());        
      }  
      catch(ExpiredIdentityException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;                  
      }
      catch(ExpiredSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(Throwable t)
      {
          //catch all other errors
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;
      }

      //make sure master order number is not null
      if (masterOrderNumber == null)
      {
        masterOrderNumber = "";
      }

      try{
          //generate output via xsl translation
          logger.debug(XMLUtility.convertDocToString(orderXML));
          File f = new File(xslFile);      
          HashMap params = new HashMap();
          TraxUtil trax = TraxUtil.getInstance();  
          params.put(HTML_PARAM_ADMIN_ACTION, adminAction);              
          params.put(HTML_PARAM_MASTER_ORDER_NUMBER,masterOrderNumber);          
          params.put(HTML_PARAM_APP_CONTEXT, applicationContext);  
          params.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);
          params.put(HTML_PARAM_SECURITY_TOKEN,securityToken);          
          params.put(HTML_PARAM_EXIT_PAGE,exitPage);          
          trax.transform(request,response,orderXML,new File(xslFile),params); 
      }
      catch(Throwable t)
      {
          //catch all other errors
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;
      }
  }

  /* Create document containining validation summary.
   * @int number of lines that were valid.
   * @throws IOException 
   * @throws ParserConfigurationException
   * @throws SAXException*/
  private XMLDocument createXMLSummary(int lineCount, String msg)
      throws IOException, ParserConfigurationException, SAXException
  {

    //validation document
    XMLDocument doc = XMLUtility.createXMLDocument("root");
  
    //create valid order xml grouping
    Element lineParentElement = doc.createElement("SUMMARIES");

    //build the summary line
    Element lineElement = doc.createElement("SUMMARY");
    lineElement.setAttribute("FIELD","");
    lineElement.setAttribute("MESSAGE",msg);
    lineElement.setAttribute("STATUS",statusValid);
    lineElement.setAttribute("COUNT",new Integer(lineCount).toString());
            
    //put field element under line
    lineParentElement.appendChild(lineElement);    

    //put it all together
    doc.getDocumentElement().appendChild(lineParentElement);

    return doc;
  }


  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>Servlet</title></head>");
    out.println("<body>");
    out.println("<p>The " + this.toString() + 
            " servlet has received a GET. This is the reply.</p>");
    out.println("</body></html>");
    out.close();
  }


  
}