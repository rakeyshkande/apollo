package com.ftd.osp.bulkload.servlet;

import java.util.Date;
import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.bulkload.utilities.ServletHelper;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.constants.XMLXSLConstants;
import com.ftd.osp.bulkload.dao.LookupDAO;
import com.ftd.osp.bulkload.services.OrderSVC;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import com.ftd.osp.bulkload.utilities.OrderParser;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.utilities.XPathQuery;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/** This servlet receives an order request and then sets flags
 *  as needed. 
 * @author Ed Mueller. */
public class CheckDataServlet 
    extends HttpServlet implements FieldConstants,XMLXSLConstants,BulkOrderConstants
{

  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  private static final String CONSTANT_FTD_HTTP_PARAMETER = "Order";
  private static final String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.CheckDataServlet";
  private static final String PAYMENT_XPATH = "/PAYMENT_TYPES/PAYMENT_TYPE";
  private static final String PAYMENT_ID_ATTRIBUTE = "payment_method_id";
    
  private Logger logger;

    //xml tags
    private static final String XML_ELEMENT_FIELD_GROUP = "FIELDS";
    private static final String XML_ELEMENT_FIELD = "FIELD";
    private static final String XML_ATTRIBUTE_FIELDNAME = "fieldname";
    private static final String XML_ATTRIBUTE_FIELDVALUE = "value";

    //XPATHS
    private static final String SOURCE_CODE_XPATH = "/SOURCECODES/SOURCECODE";

    //date from configuration file
    String statusWarn;
    String statusError;
    String orderGathererSuccess;
    String systemErrorPage;
    String logonPage;
    String applicationContext;
    String exitPage;

    
  /** Servlet Init method. Log object created. 
   * @param ServletConfig config*/
  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger(LOGGER_CATEGORY);
    super.init(config);
  }




  /** Receives servlet post requests. 
   * @param HttpServletRequest request
   * @param HttpServletReponse reponse */
  public void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {  

    String orderGuid = "";
    OrderSVC orderSVC = null;
    XMLDocument responseDoc = null;
    OrderDocVO order = new OrderDocVO();

    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    if(securityContext == null)
    {
      securityContext = "";
    }

    String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
    if(securityToken == null)
    {
      securityToken = "";
    }

    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
    if(adminAction == null)
    {
      adminAction = "";
    }
        
    //this flag is used to indicate if an error occured with the source code
    boolean sourceCodeError = false;
    
    //xsl file to be used for transformation
    String xslFile = "";    

    try{

          //get data from configuration file
          ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
          systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);    
          applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    
          String missingValue = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_MISSING_VALUE_PROPERTY);            
          logonPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,LOGON_PAGE_PROPERTY);    
          exitPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,EXIT_PAGE_PROPERTY);    


      if (SECURITY_OFF || ServletHelper.isValidToken(request)) 
      {    
          orderSVC = new OrderSVC();

   
          //determine guid & set it
          orderGuid =  BULK_GUID_PREFIX + new Long(CommonUtility.getUniqueNumber()).toString();
          order.setGuid(orderGuid);
    
          //xml document which will contain the response
          responseDoc = XMLUtility.createXMLDocument("bulkorder");

          //parent header validation element
          Element errorsParent = XMLUtility.createElement(responseDoc,XML_ELEMENT_FIELD_ERROR_GROUP);        
    
          //create order object from request
          OrderParser.createOrderObject(responseDoc,order,request,false);

          //lookup source code to deterime if membership info can be entered
          String allowMembershipInfo = "N";        
          if(order.getSourceCode() != null && order.getSourceCode().length() > 0){
          XMLDocument source = new LookupDAO().getSourceCode(order.getSourceCode());
          NodeList nl = new XPathQuery().query(source, SOURCE_CODE_XPATH);
          Element data = (Element) nl.item(0);
          if(data != null){
            String partnerId = data.getAttribute("partner_id");
            if(partnerId != null && partnerId.length() > 0)
            {
              //source code contains partner id, allow membership into to be added
              allowMembershipInfo = "Y";
            }
            else
            {
              //clear out member info if we got any
              order.setMembershipID("");
              order.setMembershipFirstName("");
              order.setMembershipLastName("");
              order.setMembershipType("");
            }

          //obtain source code dates
          String sourceCodeStartString = XMLUtility.getAttributeValue(source,
                SOURCE_CODE_XPATH,"start_date");
          String sourceCodeEndString = XMLUtility.getAttributeValue(source,
                SOURCE_CODE_XPATH,"end_date");
          Date sourceStartDate = null;
          Date sourceEndDate = null;
          try{
              if(sourceCodeStartString != null && sourceCodeStartString.length() > 0){
                  sourceStartDate = CommonUtility.formatStringToUtilDate(sourceCodeStartString);
                  sourceStartDate = CommonUtility.removeTime(sourceStartDate);
              }
              if(sourceCodeEndString != null && sourceCodeEndString.length() > 0){
                  sourceEndDate = CommonUtility.formatStringToUtilDate(sourceCodeEndString);
                  sourceEndDate = CommonUtility.removeTime(sourceEndDate);
              }
          }
          catch(Throwable t)
          {
            //catch invalid date parsing, but continue processing
            logger.error("Invalid source code date found, processing will continue.");
            logger.error(t);
          }

          //get today's date
          Date today = new Date();
          today = CommonUtility.removeTime(today);

          //check if source code is not valid yet
          if(!sourceCodeError && sourceStartDate != null && sourceStartDate.after(today))
          {
            //error
            sourceCodeError = true;
            String msg = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_SOURCE_CODE_NOT_ACTIVE_PROPERTY);            
            OrderSVC.addHeaderElement(responseDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                msg,statusError,HTML_PARAM_SOURCE_CODE); 
          }

          //check if source code is not valid yet
          if(!sourceCodeError && sourceEndDate != null && sourceEndDate.before(today))
          {
            //error
            sourceCodeError = true;
            String msg = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_SOURCE_CODE_EXPIRED_PROPERTY);            
            OrderSVC.addHeaderElement(responseDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                msg,statusError,HTML_PARAM_SOURCE_CODE); 
          }          


            
          }
          //else not found in DB
          else
          {          
            String msg = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ERROR_INVALID_SOURCE_CODE_PROPERTY);            
            OrderSVC.addHeaderElement(responseDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
                msg,statusError,HTML_PARAM_SOURCE_CODE);             
            sourceCodeError = true;
          }
          }
          //else source code is empty
          else
          {
          OrderSVC.addHeaderElement(responseDoc,XML_ELEMENT_FIELD_ERROR_ITEM,errorsParent,
              missingValue,statusError,HTML_PARAM_SOURCE_CODE);   
          sourceCodeError = true;
          }


          //set allow memebership flag        
          order.setAllowMembershipInfo(allowMembershipInfo);


          //check if credit card type is valid for this source code
          XMLDocument payments = new LookupDAO().getPaymentTypes(order.getSourceCode());          
          Element data = null;
          String xpath = PAYMENT_XPATH;
          XPathQuery q = new XPathQuery();
          NodeList nl = q.query(payments, xpath);
          boolean paymentFound = false;
          for (int i = 0; i < nl.getLength() ; i++)
          {
              data = (Element) nl.item(i);
              String value = data.getAttribute(PAYMENT_ID_ATTRIBUTE);
              if(order.getPaymentType().indexOf(value) >= 0){
                paymentFound = true;
              }
          }                  
          if(!paymentFound)
          {
            order.setExpirationDate("");
            order.setCreditCardMonth("");
            order.setCreditCardYear("");
            order.setCreditCardNumber("");
          }

          //place order in xml
          Element orderElement = OrderParser.getXMLforWeb(responseDoc,order);
          responseDoc.getDocumentElement().appendChild(orderElement);

          //if source code in error set it null before sending it to dropdown
          //routine.  this will cause it to display all payment types
          if(sourceCodeError)
          {
              order.setSourceCode(null);
          }

          //put drop down data in xml
          orderSVC.addDropDownData(responseDoc,true,true,true,order);    

          //set transformation file equal to main bulk order page
          xslFile = getServletContext().getRealPath(XSL_FILE_BULK_MAIN);

          //append errors to doc  
          responseDoc.getDocumentElement().appendChild(errorsParent);
          }//end if secure
     else{
            //failed logon
            logger.error("Logon Failed");
            response.sendRedirect(logonPage);
            return;                 
            }
      }
      catch(ExpiredIdentityException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;                  
      }
      catch(ExpiredSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(Throwable t)
      {
          //catch all other errors
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;          
      }


      try{
          //generate response
          logger.debug(XMLUtility.convertDocToString(responseDoc));
          File f = new File(xslFile);      
          HashMap params = new HashMap();
          params.put(HTML_PARAM_GUID,orderGuid);
          params.put(HTML_PARAM_APP_CONTEXT, applicationContext);    
          params.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);    
          params.put(HTML_PARAM_ADMIN_ACTION, adminAction);    
          params.put(HTML_PARAM_SECURITY_TOKEN,securityToken); 
          params.put(HTML_PARAM_EXIT_PAGE,exitPage);
          TraxUtil trax = TraxUtil.getInstance();      
          trax.transform(request,response,responseDoc,new File(xslFile),params);    
      }
     catch(Throwable t)
     {
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;                
     }

  }



  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {
     response.setContentType(CONTENT_TYPE); 
     PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>Servlet</title></head>");
    out.println("<body>");
    out.println("<p>The " + this.toString() + 
            " servlet has received a GET. This is the reply.</p>");
    out.println("</body></html>");
    out.close();
  }
}