package com.ftd.osp.bulkload.servlet;

import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.bulkload.utilities.ServletHelper;
import com.ftd.osp.bulkload.utilities.BulkOrderException;
import com.ftd.osp.utilities.ConfigurationUtil;
import javax.servlet.RequestDispatcher;
import com.ftd.osp.bulkload.dao.OrderStore;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import com.ftd.osp.bulkload.services.OrderSVC;
import java.util.HashMap;
import java.io.File;
import com.ftd.osp.utilities.xml.TraxUtil;
import org.w3c.dom.Element;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.bulkload.utilities.OrderParser;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.constants.XMLXSLConstants;

/** This servlet receives requests to process a bulk order.
 *  Processing a bulk order includes validating that the order
 *  header and lines are valid, converting the order to XML, 
 *  and then sending the XML to an HTTP Servlet.
 * @author Ed Mueller. */
public class ProcessBulkOrderServlet 
    extends HttpServlet implements FieldConstants,XMLXSLConstants,BulkOrderConstants
{

  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  private static final String CONSTANT_FTD_HTTP_PARAMETER = "Order";
  private static final String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.ProcessBulkOrderServlet";

  private Logger logger;

    //xml tags
    private static final String XML_ELEMENT_FIELD_GROUP = "FIELDS";
    private static final String XML_ELEMENT_FIELD = "FIELD";
    private static final String XML_ATTRIBUTE_FIELDNAME = "fieldname";
    private static final String XML_ATTRIBUTE_FIELDVALUE = "value";

    //date from configuration file
    String statusWarn;
    String statusError;
    String orderGathererSuccess;
    String systemErrorPage;
    String logonPage;
    String applicationContext;
    String exitPage;

    
  /** Servlet Init method. Log object created. 
   * @param ServletConfig config*/
  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger(LOGGER_CATEGORY);
    super.init(config);
  }




  /** Receives servlet post requests. 
   * @param HttpServletRequest request
   * @param HttpServletReponse reponse */
  public void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {  

    String orderGuid = "";
    OrderSVC orderSVC = null;
    XMLDocument responseDoc = null;
    OrderDocVO order = new OrderDocVO();
    String securityToken = "";

    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    if(securityContext == null)
    {
      securityContext = "";
    }
    
    securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);
    if(securityToken == null)
    {
      securityToken = "";
    }

    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
    if(adminAction == null)
    {
      adminAction = "";
    }
        
    //xsl file to be used for transformation
    String xslFile = "";    
    String masterOrderNumber = "";
    String csrId = "";
    try{

        //get data from configuration file
        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        statusWarn = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_WARN_PROPERTY);
        statusError = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,STATUS_ERROR_PROPERTY);
        orderGathererSuccess = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,ORDER_GATHERER_SUCCESS_PROPERTY);
        systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);    
        applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    
        logonPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,LOGON_PAGE_PROPERTY);    
        exitPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,EXIT_PAGE_PROPERTY);    
        csrId = ServletHelper.getUserId(securityToken);         
        if (SECURITY_OFF || ServletHelper.isValidToken(request)) 
        {    

        orderSVC = new OrderSVC();
    
       
        //determine guid & set it
        orderGuid = BULK_GUID_PREFIX + new Long(CommonUtility.getUniqueNumber()).toString();
        order.setGuid(orderGuid);
        order.setCsrId(csrId);
        //xml document which will contain the response
        responseDoc = XMLUtility.createXMLDocument("bulkorder");
    
        //create order object from request..returns security token
        OrderParser.createOrderObject(responseDoc,order,request,true);


              //validate the order header header    
              boolean errorOnOrder = orderSVC.validateHeader(order,responseDoc);
              //if there is an error on the header do not continue, user goes back to main screen
              if(errorOnOrder)
              {
                //put drop down data in xml
                orderSVC.addDropDownData(responseDoc,true,true,true,order);    
                //place order in xml
                Element orderElement = OrderParser.getXMLforWeb(responseDoc,order);
                responseDoc.getDocumentElement().appendChild(orderElement);
                //set transformation file equal to main bulk order page
                xslFile = getServletContext().getRealPath(XSL_FILE_BULK_MAIN);
              }
              else
              {   
                  //set transformation file equal to validation page
                  xslFile = getServletContext().getRealPath(XSL_VALIDATION_RESULTS);
        
                  //generate order guid and place it in object & request
                  orderGuid = OrderParser.addGuid(responseDoc,order);
                  //Validate spreadsheet
                  String validFlag = "";
                  validFlag = new OrderSVC().validateDetails(order,responseDoc);

                  //if status is WARN or ERROR then place order in store
                  //The order will be retrieved from store if\when user chooses to continue
                  if(validFlag.equals(statusWarn) || validFlag.equals(statusError)){      
                     OrderStore store = OrderStore.getInstance();
                     store.setOrder(orderGuid,order);           
                  }
                  else{
                      //document is valid, process it
                        masterOrderNumber = orderSVC.processOrder(order);

                          //orders have been processed...we're done here..bring user back to main page
                          //put drop down data in xml
                          //responseDoc = XMLUtility.createXMLDocument("bulkorder");                    
                          //orderSVC.addDropDownData(responseDoc,true,true,true,order);    
                          //set transformation file equal to main bulk order page
                          //xslFile = getServletContext().getRealPath(XSL_FILE_BULK_MAIN);

                          //directing to validation page
                          xslFile = getServletContext().getRealPath(XSL_VALIDATION_RESULTS);

                          //directing to status page
                          //response.sendRedirect("ViewBulkOrderStatusServlet");
                          //return;
                      }
               }

      }//end if secure
     else{
            //failed logon
            logger.error("Logon Failed");
            response.sendRedirect(logonPage);
            return;                 
            }      
      }
      catch(BulkOrderException e)
      {
        //application error occured redirect to validation page and show error
        //to user
        logger.error(e);
        xslFile = getServletContext().getRealPath(XSL_VALIDATION_RESULTS);
        responseDoc = orderSVC.getErrorDoc(e.getMessage());
        orderGuid = OrderParser.addGuid(responseDoc,order);        
        OrderStore.getInstance().setOrder(orderGuid,order);
      }
      catch(ExpiredIdentityException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;                  
      }
      catch(ExpiredSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(Throwable t)
      {
          //catch all other errors
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;          
      }

      //make sure master order number is not null
      if (masterOrderNumber == null)
      {
        masterOrderNumber = "";
      }

      try{
          //generate response
          logger.debug(XMLUtility.convertDocToString(responseDoc));
          File f = new File(xslFile);      
          HashMap params = new HashMap();
          params.put(HTML_PARAM_GUID,orderGuid);
          TraxUtil trax = TraxUtil.getInstance();  
          params.put(HTML_PARAM_MASTER_ORDER_NUMBER,masterOrderNumber);
          params.put(HTML_PARAM_APP_CONTEXT, applicationContext);          
          params.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);          
          params.put(HTML_PARAM_SECURITY_TOKEN,securityToken); 
          params.put(HTML_PARAM_ADMIN_ACTION, adminAction);              
          params.put(HTML_PARAM_EXIT_PAGE,exitPage);          
          trax.transform(request,response,responseDoc,new File(xslFile),params);    
      }
     catch(Throwable t)
     {
          logger.error(t);
          RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
          dispatcher.forward(request, response);        
          return;                
     }

  }



  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {
     response.setContentType(CONTENT_TYPE); 
     PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>Servlet</title></head>");
    out.println("<body>");
    out.println("<p>The " + this.toString() + 
            " servlet has received a GET. This is the reply.</p>");
    out.println("</body></html>");
    out.close();
  }
}