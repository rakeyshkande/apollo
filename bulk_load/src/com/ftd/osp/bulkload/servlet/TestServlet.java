package com.ftd.osp.bulkload.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.IOException;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;

public class TestServlet extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }




  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {  
    List items = null;


    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>TestServlet</title></head>");
    out.println("<body>");
    out.println("<p>The File Upload Servlet has received a POST. </p>");
    out.println("</body></html>");
    out.close();
  }

    private void processUploadedFile(FileItem fileItem){
    InputStream uploadedStream = null;
            try{    
            //  Write this to a file
            File uploadedFile = new File("uploadedFile.txt");
            fileItem.write(uploadedFile);

            //get a stream for the file
             uploadedStream = fileItem.getInputStream();


            //get value from work book
            POIFSFileSystem fs = new POIFSFileSystem(uploadedStream);
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row = sheet.getRow(2);
            HSSFCell cell = row.getCell((short)3);            
            String theValue = cell.getStringCellValue();

            System.out.println("The cell value is:" + theValue);
            uploadedStream.close(); 
             }
             catch(Exception e){
                    System.out.println("Could not process uploaded file.");
                    }
           
            
            }


    


  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    response.setContentType(CONTENT_TYPE);    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head><title>TestServlet</title></head>");
    out.println("<body>");
    out.println("<p>The servlet has received a GET. This is the reply.</p>");
    out.println("</body></html>");
    out.close();
  }
}