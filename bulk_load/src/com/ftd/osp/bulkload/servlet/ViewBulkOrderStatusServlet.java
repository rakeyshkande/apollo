package com.ftd.osp.bulkload.servlet;

import com.ftd.security.exceptions.ExpiredIdentityException;
import com.ftd.security.exceptions.ExpiredSessionException;
import com.ftd.security.exceptions.InvalidSessionException;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.dao.LookupDAO;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import com.ftd.osp.bulkload.utilities.ServletHelper;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.utilities.XPathQuery;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.TraxUtil;

import java.io.File;
import java.io.IOException;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ViewBulkOrderStatusServlet  
  extends HttpServlet implements BulkOrderConstants, FieldConstants
{
  private Logger logger;
  private final static int NUMBER_OF_DAYS_BACK = 30;
  private final static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.ViewBulkOrderStatusServlet";
  private final static String XSL_FILE_BULK_STATUS = "/xsl/bulkOrderStatus.xsl";
  private final static String ORDER_XPATH = "/orders/order";
  private final static String ORDER_DATE_ATTRIBUTE = "order_date";


  private final static String LINE_ITEMS_ATTRIBUTE = "submitted_lines_total";
  private final static String PROCESSED_ITEMS_ATTRIBUTE = "process_total";  
  private final static String INSCRUB_ITEMS_ATTRIBUTE = "scrub_total";  
  private final static String RECEIVED_ITEMS_ATTRIBUTE = "receive_total";  
  private final static String ABANDON_ITEMS_ATTRIBUTE = "abandon_total";  
  private final static String REMOVED_ITEMS_ATTRIBUTE = "remove_pending_total";  
  private final static String STATUS_ATTRIBUTE = "status";  
  
  
  /** This method creates a logger objects.
   *  @param ServletConfig */
  public void init(ServletConfig config) throws ServletException
  {
    logger = new Logger(LOGGER_CATEGORY);
    super.init(config);
  }

  /** Receives servlet post requests. 
   * @param HttpServletRequest request
   * @param HttpServletReponse reponse */
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {  
    process(request,response);
  }

  /** Receives servlet get requests.  Not Used. */
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException
  {
    process(request,response);
  }

  /** This method get and displays bulk orders.  It is called from both
   * the doGet & doPost methods.
   * @param HTTPServletRequest request
   * @param HTTPServletResponse response 
   * @throws ServletException
   * @throws IOException
   * @throws ParserConfigurationException*/
   private void process(HttpServletRequest request, HttpServletResponse response)
       throws ServletException, IOException
   {

     String securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN);

    //determine what date you want to view bulk orders up to
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(new java.util.Date());
    calendar.add(Calendar.DATE,-NUMBER_OF_DAYS_BACK);
    java.sql.Date sqlDate = new java.sql.Date(calendar.getTime().getTime());

    String systemErrorPage = null;
    String logonPage = null;
    String exitPage = null;
    String applicationContext = null;

    String securityContext = request.getParameter(HTML_PARAM_SECURITY_CONTEXT);
    if(securityContext == null)
    {
      securityContext = "";
    }

    securityToken = request.getParameter(HTML_PARAM_SECURITY_TOKEN); 
    if(securityToken == null)
    {
      securityToken = "";
    }
    String adminAction = request.getParameter(HTML_PARAM_ADMIN_ACTION);
    if(adminAction == null)
    {
      adminAction = "";
    }
        
    try{

        ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
        systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);    
        applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    
        logonPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,LOGON_PAGE_PROPERTY);    
        exitPage = configUtil.getFrpGlobalParm(BULK_ORDER_PROPERTY_CONTEXT,EXIT_PAGE_PROPERTY);    
          
      if (SECURITY_OFF || ServletHelper.isValidToken(request)) 
      {    
    
        XMLDocument doc = XMLUtility.createXMLDocument("viewbulkorderdetail");

        XMLDocument data = new LookupDAO().getOrders(sqlDate);

        //clean up the xml for display purposes
        cleanUp(data);

        //now clean up the formatting
        data = XMLUtility.fixXML(data);
        
        doc.getDocumentElement().appendChild(doc.importNode(data.getDocumentElement(),true));      
    
        //generate output via xsl translation
        logger.debug(XMLUtility.convertDocToString(doc));    
        HashMap params = new HashMap();
        TraxUtil trax = TraxUtil.getInstance();      
        String xslFile = getServletContext().getRealPath(XSL_FILE_BULK_STATUS);
        params.put(HTML_PARAM_APP_CONTEXT, applicationContext);  
        params.put(HTML_PARAM_SECURITY_CONTEXT, securityContext);  
        params.put(HTML_PARAM_SECURITY_TOKEN,securityToken);    
        params.put(HTML_PARAM_ADMIN_ACTION, adminAction);            
        params.put(HTML_PARAM_EXIT_PAGE,exitPage);        
        trax.transform(request,response,doc,new File(xslFile),params);    
      }//end if secure
     else{
            //failed logon
            logger.error("Logon Failed");
            response.sendRedirect(logonPage);
            return;                 
            }      
    }
      catch(ExpiredIdentityException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;                  
      }
      catch(ExpiredSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
      catch(InvalidSessionException e )
      {
          //logon error
          logger.error(e);
          response.sendRedirect(logonPage);
          return;              
      }
    catch(Throwable t)
    {
      logger.error(t);
      RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
      dispatcher.forward(request, response);        
      return;      
    }
   }

   /* clean up xml data...make it look pretty*/
   private void cleanUp(XMLDocument doc) throws Exception
   {

      ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            

      String SUBMITTED_ORDER_STATUS = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,"SUBMITTED_ORDER_STATUS"); 
      String PROCESSED_ORDER_STATUS = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,"PROCESSED_ORDER_STATUS"); 
      String ERROR_ORDER_STATUS = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,"ERROR_ORDER_STATUS"); 
      String ABANDON_STATUS = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,"ABANDON_STATUS"); 
      String COUNT_ERROR_STATUS = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,"COUNT_ERROR_STATUS"); 

   
      //for each order in the document
      Element data = null;
      XPathQuery q = new XPathQuery();
      NodeList nl = q.query(doc, ORDER_XPATH);

      for (int i = 0; i < nl.getLength() ; i++)
      {

          //format the date
          data = (Element) nl.item(i);
          String value = data.getAttribute(ORDER_DATE_ATTRIBUTE);
          Date dateAsDate = CommonUtility.formatStringToUtilDate(value);
          String dateAsString = CommonUtility.formatUtilDateToString(dateAsDate);
          data.setAttribute(ORDER_DATE_ATTRIBUTE,dateAsString);


          //format the status value
          String status = "";
          int lineItems = 0;
          int processed = 0;
          int inscrub = 0;
          int removed = 0;
          int received = 0;
          int abandon = 0;
          try
          {            
            lineItems = Integer.parseInt(data.getAttribute(LINE_ITEMS_ATTRIBUTE));
            processed = Integer.parseInt(data.getAttribute(PROCESSED_ITEMS_ATTRIBUTE));
            inscrub = Integer.parseInt(data.getAttribute(INSCRUB_ITEMS_ATTRIBUTE));
            removed = Integer.parseInt(data.getAttribute(REMOVED_ITEMS_ATTRIBUTE));            
            received = Integer.parseInt(data.getAttribute(RECEIVED_ITEMS_ATTRIBUTE));            
            abandon = Integer.parseInt(data.getAttribute(ABANDON_ITEMS_ATTRIBUTE));            

            //total should match, if not the stored proc should be looked at
            if(lineItems != (processed+inscrub+removed+received+abandon)) {
              status = COUNT_ERROR_STATUS;
            }            
            //if everything was processed
            else if(lineItems==(processed+removed) ){
                status = PROCESSED_ORDER_STATUS;
            }
            //if any in scrub
            else if(inscrub > 0){
                status = ERROR_ORDER_STATUS;
            }
            //if any abandon
            else if(abandon > 0){
                status = ABANDON_STATUS;
            }
            //else, we are submitted and waiting for stuff to happen
            else
            {
                status = SUBMITTED_ORDER_STATUS;                          
            }
            
          }
          catch(Exception e)
          {
            logger.error("Could not parse line counts.");
            logger.error(e);
            status = "UNKNOWN";
          }

          data.setAttribute(STATUS_ATTRIBUTE,status);          

      }                        
   }
  
}  
