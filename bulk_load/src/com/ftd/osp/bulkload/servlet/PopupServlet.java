package com.ftd.osp.bulkload.servlet;

import com.ftd.osp.utilities.ConfigurationUtil;
import javax.servlet.RequestDispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.utilities.xml.TraxUtil;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;
import java.io.IOException;
import java.io.File;
import oracle.xml.parser.v2.XMLDocument;
import com.ftd.osp.bulkload.utilities.CommonUtility;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.constants.FieldConstants;
import com.ftd.osp.bulkload.services.SearchSVC;
import com.ftd.osp.bulkload.services.OrderSVC;


/** This servlet is executed when the user chooses to do a florist
 *  or sourcecode lookup from the webpage.
 *  @author This servlet was taken from Order Entry Phase I
 *  *
 *  Since this application does not use the same framework as PhaseI
 *  some minor changes were made to this servlet. Ed Mueller */
public class PopupServlet extends HttpServlet 
    implements FieldConstants,BulkOrderConstants
{
    private static final String CONTENT_TYPE = "text/xml; charset=windows-1252";
    private ServletConfig config;
    private static String LOGGER_CATEGORY = "com.ftd.osp.bulkorder.servlet.PopupServlet";

    //used for look up windows
    private static final String POPUP_ID_KEY = "POPUP_ID";
    private static final String LOOKUP_SOURCE_CODE_KEY = "LOOKUP_SOURCE_CODE";
    private static final String LOOKUP_FLORIST_KEY = "LOOKUP_FLORIST";
    private static final String SOURCE_CODE_SEARCH_KEY = "sourceCodeInput";

    private static final String INSTITUTION_SEARCH_KEY = "institutionInput";
    private static final String PHONE_NUMBER_SEARCH_KEY = "phoneInput";
    private static final String CUSTOMER_ID_SEARCH_KEY = "customerIdInput";
    private static final String ADDRESS1_SEARCH_KEY = "addressInput";
    private static final String ADDRESS2_SEARCH_KEY = "address2Input";
    private static final String ZIP_CODE_SEARCH_KEY = "zipCodeInput";
    private static final String CITY_SEARCH_KEY = "cityInput";
    private static final String STATE_SEARCH_KEY = "stateInput";
    private static final String OCCASION_SEARCH_KEY = "occasionInput";
    private static final String CATEGORY_SEARCH_KEY = "categoryInput";
    private static final String GREETING_CARD_SEARCH_KEY = "cardIdInput";
    private static final String MONTH_SEARCH_KEY = "month";
    private static final String YEAR_SEARCH_KEY = "year";
    private static final String COUNTRY_TYPE_SEARCH_KEY = "countryTypeInput";
    private static final String INST_TYPE_SEARCH_KEY = "institutionTypeInput";

    //xsl files
    private static final String XSL_SOURCE_CODE_LOOKUP = "/xsl/sourceCodeLookup.xsl";
    private static final String XSL_FLORIST_CODE_LOOKUP = "/xsl/floristLookup.xsl";

    private Logger logger;
    private String systemErrorPage;
    private String applicationContext;
    
    /** Servlet Init */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        this.config = config;
        logger = new Logger(LOGGER_CATEGORY);        
        
    }

    /** Process get request.
     * @param HttpServletRequest request
     * @param HttpServletResponse response */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        String systemErrorPage = "";

        try{    

              //get data from configuration file
              ConfigurationUtil configUtil = ConfigurationUtil.getInstance();            
              systemErrorPage = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,SYSTEM_ERROR_PAGE_PROPERTY);    
              applicationContext = configUtil.getProperty(BULK_ORDER_PROPERTY_FILE,APPLICATION_CONTEXT_PROPERTY);    
        
              response.setHeader("Cache-Control", "no-cache");
              String popupId = request.getParameter(POPUP_ID_KEY);
              if(popupId.equals(LOOKUP_SOURCE_CODE_KEY))
              {
                  lookupSourceCode(request, response);
              }
              else if(popupId.equals(LOOKUP_FLORIST_KEY))
              {
                  lookupFlorist(request, response);
              }
        }
      catch(Throwable t)
      {
        //if any errors occured log it and redirect
        logger.error("Error executing popup servlet");
        logger.error(t);  
        RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
        dispatcher.forward(request, response);        
        return;                  
      }

    }

    /* Call service object to do a souce code lookup.
     * @param HttpServletRequest request
     * @param HttpServletResponse response */
    private void lookupSourceCode(HttpServletRequest request, HttpServletResponse response)
    {

        String searchString = request.getParameter(SOURCE_CODE_SEARCH_KEY);

        if(searchString == null)
        {
            searchString = "";
        }

        try
        {
            Map args = new HashMap();
            args.put("searchvaluecode", searchString);
            args.put("searchvaluedesc", searchString);
            args.put("displayexpired", "Y");

            String xslFile = XSL_SOURCE_CODE_LOOKUP;
            XMLDocument xml = new SearchSVC().process(SEARCH_SOURCE_CODE_LOOKUP,args);
            
            xslFile = getServletContext().getRealPath(xslFile);
            HashMap params = new HashMap();
            params.put("applicationcontext", applicationContext);
            new Logger(LOGGER_CATEGORY).debug(XMLUtility.convertDocToString(xml));

            TraxUtil trax = TraxUtil.getInstance();      
            trax.transform(request,response,xml,new File(xslFile),params);    
      
        }
        catch(Exception e)
        {
            new Logger(LOGGER_CATEGORY).error(e.toString());
        }
    }

    /* Call service object to do a florist lookup.
     * @param HttpServletRequest request
     * @param HttpServletResponse response 
     * @throws IOException
     * @throws ServletException*/
    private void lookupFlorist(HttpServletRequest request, HttpServletResponse response)
        throws IOException,ServletException
    {
        //Util util = new Util();
        Enumeration enum1 = request.getParameterNames();
        String param = "";
        String value = "";
        HashMap params = new HashMap();
        while(enum1.hasMoreElements())
        {
            param = (String) enum1.nextElement();
            value = request.getParameter(param);
            params.put(param, value);
        }

        String instName = request.getParameter(INSTITUTION_SEARCH_KEY);
        if(instName == null) instName = "";
        instName = CommonUtility.replaceAll(instName, "'", "&apos;");
        params.put(INSTITUTION_SEARCH_KEY, instName);

        String phoneNumber = request.getParameter(PHONE_NUMBER_SEARCH_KEY);
        String countryType = request.getParameter(COUNTRY_TYPE_SEARCH_KEY);

        if(countryType == null)
        {
            countryType = "";
        }

        if(phoneNumber == null)
        {
            phoneNumber = "";
        }

        String institution = request.getParameter(INSTITUTION_SEARCH_KEY);
        if(institution != null)
        {
            institution = institution.toUpperCase();
        }
        else
        {
            institution = "";
        }

        String address1 = request.getParameter(ADDRESS1_SEARCH_KEY);
        if(address1 == null)
        {
            address1 = "";
        }

        String city = request.getParameter(CITY_SEARCH_KEY);
        if(city != null)
        {
            city = city.toUpperCase();
        }
        else
        {
            city = "";
        }

        String state = request.getParameter(STATE_SEARCH_KEY);
        if(state != null)
        {
            state = state.toUpperCase();
        }
        else
        {
            state = "";
        }

        String zipCode = request.getParameter(ZIP_CODE_SEARCH_KEY);
        if(zipCode == null)
        {
            zipCode = "";
        }

        String productId = request.getParameter("productId");
        if(productId == null)
        {
            productId = "";
        }

        try
        {
            Map args = new HashMap();         
            args.put("inPhone", phoneNumber);
            args.put("floristName", institution);
            args.put("address1", address1);
            args.put("inCity", city);
            args.put("inState", state);
            args.put("inZipCode", zipCode);
            args.put("productId", productId);

            String xslFile = XSL_FLORIST_CODE_LOOKUP;

            XMLDocument xml = new SearchSVC().process(SEARCH_GET_FLORIST,args);

            //add drop down data
            String sourceCode = request.getParameter(SOURCE_CODE_SEARCH_KEY);
            OrderDocVO order = new OrderDocVO();
            order.setSourceCode(sourceCode);
            new OrderSVC().addDropDownData(xml,true,false,false,order);
            
            xslFile = getServletContext().getRealPath(xslFile);
            params.put("applicationcontext", applicationContext);
            TraxUtil trax = TraxUtil.getInstance();      
            trax.transform(request,response,xml,new File(xslFile),params);    
        }
        catch(Exception e)
        {
            //send user to validation page with error
            new Logger(LOGGER_CATEGORY).error(e.toString());
            RequestDispatcher dispatcher = request.getRequestDispatcher(systemErrorPage);
            dispatcher.forward(request, response);                    
        }
    }


}
