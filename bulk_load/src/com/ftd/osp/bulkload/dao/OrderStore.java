package com.ftd.osp.bulkload.dao;

import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.utilities.OrderParser;
import com.ftd.osp.bulkload.vo.OrderDocVO;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.order.ScrubMapperDAO;
import com.ftd.osp.utilities.order.vo.OrderVO;
import com.ftd.osp.utilities.plugins.Logger;

import java.sql.Connection;

import java.util.HashMap;

/** This class stores an order object.
 *  @author Ed Mueller */
public class OrderStore extends HashMap  implements BulkOrderConstants
{
  private DataAccessUtil dataAccess = null;
  private static OrderStore ORDERSTORE = null;
  private String datasource;
  private String initialConextLocation;
  private Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.osp.bulkload.dao.Lookup";

  
  /** Constructor */
  private OrderStore()
  {
      logger = new Logger(LOGGER_CATEGORY);
  
      try{
        dataAccess = DataAccessUtil.getInstance();

        //get connection info
        ConfigurationUtil config = ConfigurationUtil.getInstance();            
        datasource = config.getProperty(BULK_ORDER_PROPERTY_FILE,DATASOURCE_PROPERTY);
        initialConextLocation = config.getProperty(BULK_ORDER_PROPERTY_FILE,CONTEXT_PROPERTY);

      }
      catch(Exception e)
      {
          logger.error(e);
      }
  }

  /** Returns and instace of the OrderStore object
   * @return OrderStore instance */
  public static OrderStore getInstance()
  {
      if(ORDERSTORE == null)
        ORDERSTORE = new OrderStore();

      return ORDERSTORE;
  }

  /** Get stored order
   * @return OrderDocVO the order */
   public OrderDocVO getOrder(String key) 
   {
     //return (OrderDocVO)ORDERSTORE.remove(key);;
     OrderVO order;
     try{
       order = getScrubOrder(key);
     }
     catch(Exception e)
     {
       order = null;
       logger.error(e);
     }
     return OrderParser.convertOrder(order);
   }

   /** Store order
    * @param String key used to identify the order
    * @param OrderDocVO order to store */
    public void setOrder(String key, OrderDocVO newOrder)
    {
//      ORDERSTORE.put(key,newOrder);        
      try{ 
        insertScrubOrder(OrderParser.convertOrder(newOrder));
      }
      catch(Exception e)
      {
        //order not placed in store
        logger.error(e);
      }
    }

 /**
  * Get connection to the database
  * @return Connection
  */
   private Connection getDBConnection() throws Exception
     {
       Connection con = null;       
     
       DataSourceUtil dsUtil = DataSourceUtil.getInstance();
       con = dsUtil.getConnection(datasource);        
         
       return con;         
     }


  /* Retrieve order from scrub */
  private OrderVO getScrubOrder(String guid) throws Exception
  {
    Connection conn = getDBConnection();
    OrderVO order = null;
    try{
      order =  new ScrubMapperDAO(conn).mapOrderFromDB(guid);
    }
    finally{
      if(conn != null){
          conn.close();
      }
    }
    return order;
  }

  /* Insert order into scrub */
  private void insertScrubOrder(OrderVO order) throws Exception
  {
    Connection conn = null;
    try
    {
      conn = getDBConnection();  
      new ScrubMapperDAO(conn).mapOrderToDB(order);
    }
    finally
    {
      //close the connection
      try
      {
        conn.close();
      }
      catch(Exception e)
      {}
    }
  }
}