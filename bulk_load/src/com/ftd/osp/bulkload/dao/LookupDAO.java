package com.ftd.osp.bulkload.dao;

import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.bulkload.constants.BulkOrderConstants;
import com.ftd.osp.bulkload.utilities.XMLUtility;
import com.ftd.osp.bulkload.utilities.XPathQuery;
import com.ftd.osp.utilities.ConfigurationUtil;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import java.util.HashMap;
import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/** This class performs database lookups.
 *  @author Ed Mueller */
public class LookupDAO implements BulkOrderConstants
{
  private DataAccessUtil dataAccess = null;
  private Logger logger;
  private static final String LOGGER_CATEGORY = "com.ftd.osp.bulkload.dao.Lookup";
  private static final String STATEMENT_STATE_LIST = "state_list";
  private static final String STATEMENT_COUNTRY_LIST = "country_list";
  private static final String STATEMENT_PAYMENT_LIST = "payment_list";
  private static final String STATEMENT_BULK_SUMMARY = "bulk_summary";
  private static final String STATEMENT_BULK_DETAIL = "bulk_detail";
  private static final String STATEMENT_STATE = "state_bystatecode";
  private static final String STATEMENT_PRODUCT = "product_byproductid";
  private static final String STATEMENT_FLORIST_SEARCH = "florist_search";
  private static final String STATEMENT_SOURCECODE_SEARCH = "sourcecode_search";
  private static final String GET_STATE_MASTER_BY_ID_STATEMENT = "GET_STATE_DETAILS";
  private static final String GET_PRODUCT_BY_ID_STATEMENT = "GET_PRODUCT_BY_ID";
  private static final String GET_SUBCODE_BY_ID_STATEMENT = "GET_PRODUCT_SUBCODE";
  
  private static final int COUNTRY_MASTER_COUNTRY_TYPE =2;
  private static final String GET_COUNTRY_MASTER_BY_ID_STATEMENT = "GET_COUNTRY_MASTER_BY_ID";

  private static final String DOMESTIC = "D";
  
  //pay types which should not be displayed on bulk order screen
  private static final String SKIP_PAY_TYPES_PROPERTY = "donotshowpaymenttypes";
  private String datasource;
  private String initialConextLocation;
      
  /** Public constuctor gets an instace of the data access utility. */
  public LookupDAO() 
  {
      logger = new Logger(LOGGER_CATEGORY);
  
      try{
        dataAccess = DataAccessUtil.getInstance();

        //get connection info
        ConfigurationUtil config = ConfigurationUtil.getInstance();            
        datasource = config.getProperty(BULK_ORDER_PROPERTY_FILE,DATASOURCE_PROPERTY);
        initialConextLocation = config.getProperty(BULK_ORDER_PROPERTY_FILE,CONTEXT_PROPERTY);

      }
      catch(Exception e)
      {
          logger.error(e);
      }


          
  }

  /** This method returns all the states located in the database.
   * @returns XMLDocument XML containing list of state names and abbreviations
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
   public XMLDocument getStates() 
      throws SQLException, IOException, ParserConfigurationException, Exception
   {
    Connection con = null;
    XMLDocument doc = null;

     try{
       con = getDBConnection();
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(con);
       dataRequest.setStatementID(STATEMENT_STATE_LIST);
       doc = (XMLDocument)dataAccess.execute(dataRequest);
     }
     finally
     {
       if (con != null){
          con.close();
       }
     }    

     return doc;
   }

 




  /** This method returns all the payment types located in the database.
   * @returns XMLDocument XML containing list of state names and abbreviations 
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
   public XMLDocument getPaymentTypes(String sourceCode) 
      throws SQLException, IOException, ParserConfigurationException, Exception
   {
    Connection con = null;
    XMLDocument doc = null;
     try{
       con = getDBConnection();   
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(con);
       dataRequest.addInputParam("sourcecode", sourceCode);                 
       dataRequest.setStatementID(STATEMENT_PAYMENT_LIST);
       doc = (XMLDocument)dataAccess.execute(dataRequest);
     } 
     finally
     {
       if (con != null){     
       con.close();   
       }
     } 
     return doc;
   }

  /** This method returns all the countries located in the database.
   * @returns XMLDocument XML containing list of country names and abbreviation 
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
   public XMLDocument getCountries() 
        throws SQLException, IOException, ParserConfigurationException, Exception
   {
    Connection con = null;
    XMLDocument doc = null;
     try{
       con = getDBConnection();
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(con);
       dataRequest.setStatementID(STATEMENT_COUNTRY_LIST);
       doc = (XMLDocument)dataAccess.execute(dataRequest);
     } 
     finally
     {
            if (con != null){
       con.close();       
            }
     } 
     return doc;
   }  

  /** This method returns all the bulk orders which were
   *  received after the passed in time frame.
   * @returns XMLDocument XML containing list of orders 
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
   public XMLDocument getOrders(java.sql.Date fromDate) 
      throws SQLException, IOException, ParserConfigurationException, Exception
   {
    Connection con = null;
    XMLDocument doc = null;
     try{
       con = getDBConnection();
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(con);
       dataRequest.setStatementID(STATEMENT_BULK_SUMMARY);
       dataRequest.addInputParam("fromdate", fromDate);          
       doc = (XMLDocument)dataAccess.execute(dataRequest);
     } 
     finally
     {
       if (con != null){
       con.close();   
       }
     } 
     return doc;
   }  

  /** This method returns all the order details for the given master order number.
   * @param String mater order number
   * @returns XMLDocument XML containing list of country names and abbreviation 
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
   public XMLDocument getOrderDetails(String masterOrderNumber) 
      throws SQLException, IOException, ParserConfigurationException, Exception
   {
    Connection con = null;
    XMLDocument doc = null;
     try{
       con = getDBConnection();   
       DataRequest dataRequest = new DataRequest();
       dataRequest.setConnection(con);
       dataRequest.setStatementID(STATEMENT_BULK_DETAIL);
       dataRequest.addInputParam("masterordernumber", masterOrderNumber);     
       doc = (XMLDocument)dataAccess.execute(dataRequest);
     } 
     finally
     {
       if (con != null){     
       con.close();   
       }
     } 
     return doc;
   }     


   
  /** This method returns the next order number.
   * @returns String XML containing a order number 
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
   public String getNextOrderNumber() 
      throws SQLException, IOException, ParserConfigurationException, Exception
   {
    Connection con = null;
    String orderNumber = null;

     try{
       con = getDBConnection();
       DataRequest dataRequest = new DataRequest();
       dataRequest.setStatementID("next_order_number");
       dataRequest.setConnection(con);
     
       orderNumber = dataAccess.execute(dataRequest).toString();
//       orderNumber = (CachedResultSet)dataAccess.execute(dataRequest).toString();
//       if(rs.next())
//       {
//           orderNumber = rs.getObject(1).toString();
//       }       
     } 
     finally
     {
       if (con != null){     
       con.close();   
       }
     } 
     return  orderNumber;
   }   

  /** This methods checks the database to determine if the 
   * florist # & zipcode combiation exist.
   * @param String florist
   * @param String zipcode
   * @return boolean true if exists 
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
  public boolean floristZipExist(String florist, String zipcode) 
      throws SQLException, IOException, ParserConfigurationException, Exception
  {
     DataRequest dataRequest = new DataRequest();
    Connection con = null;
    XMLDocument doc = null;
    int count = 0;
     try{
       con = getDBConnection();

       dataRequest.setStatementID("florist_zipcode");
       dataRequest.setConnection(con);
       dataRequest.addInputParam("florist", florist);
       dataRequest.addInputParam("zipcode", zipcode);     
       count = Integer.parseInt(dataAccess.execute(dataRequest).toString());       

     } 
     finally
     {
       if (con != null){     
       con.close();   
       }
     } 


     return count > 0 ? true : false;
  }

  /** This methods returns the payment type from the database.
   * @param String payment id
   * @return XMLDocument containing the document
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects 
   * */
  public XMLDocument getPaymentType(String paymentID) 
      throws SQLException, IOException, ParserConfigurationException, Exception
  {
     DataRequest dataRequest = new DataRequest();
    Connection con = null;
    XMLDocument doc = null;
     try{
       con = getDBConnection();
       dataRequest.setStatementID("payment_bypaymentid");
       dataRequest.setConnection(con);
       dataRequest.addInputParam("paymentid", paymentID);
       doc = (XMLDocument)dataAccess.execute(dataRequest);

     } 
     finally
     {
       if (con != null){     
       con.close();   
       }
     } 
     
     return doc;
  }
  
    /** The method retrieves a source code from the database.
     * @param String source code
     * @return XMLDocument containing the source code 
     * @throws SQLException Database error occured
     * @throws IOException Database IO Error
     * @throws ParserConfigurationException Error parsing database results
     * @throws Exception Thrown by utility database objects 
     *  */
     public XMLDocument getSourceCode(String sourceCode) 
        throws SQLException, IOException, ParserConfigurationException, Exception
     {
      DataRequest dataRequest = new DataRequest();
      Connection con = null;
      XMLDocument doc = null;
     try{
       con = getDBConnection();
       dataRequest.setStatementID("sourcecode_bysourcecode");
       dataRequest.setConnection(con);
       dataRequest.addInputParam("sourcecode", sourceCode);
       doc = (XMLDocument)dataAccess.execute(dataRequest);
     } 
     finally
     {
       if (con != null){     
       con.close();   
       }
     } 
     return doc;       
     }
     


  /** This method performs the lookup for source codes.
   * @param Map arguements
   * @returns XMLDocument the search results
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects*/
    public XMLDocument getSourceCodeByValue(Map arguments) 
        throws SQLException, IOException, ParserConfigurationException, Exception
	{
      Connection con = null;
      XMLDocument document = null;
     try{
        con = getDBConnection();         

        DataRequest dataRequest = new DataRequest();
        dataRequest.setStatementID(STATEMENT_SOURCECODE_SEARCH);
        dataRequest.setConnection(con);
     
        dataRequest.addInputParam("searchvaluecode", ((String) arguments.get("searchvaluecode")).toUpperCase());
        dataRequest.addInputParam("searchvaluedesc", ((String) arguments.get("searchvaluedesc")).toUpperCase());
        dataRequest.addInputParam("displayexpired", "Y");
     
        XMLDocument dataResponse = (XMLDocument)dataAccess.execute(dataRequest);


        document = XMLUtility.createXMLDocument("SOURCE CODE LOOKUP PAGE VO");


        XMLUtility.addSection(document, dataResponse.getChildNodes());


        XMLUtility.addSection(document, (convertLookupAnchors(dataResponse)).getChildNodes());



       } 
     finally
     {
       if (con != null){
       con.close();   
       }
     } 
        return document;
	}

  /** This method performs the lookup for florists.
   * @param Map arguements
   * @returns XMLDocument the search results
   * @throws SQLException Database error occured
   * @throws IOException Database IO Error
   * @throws ParserConfigurationException Error parsing database results
   * @throws Exception Thrown by utility database objects*/
    public XMLDocument getFlorist(Map arguments) 
        throws SQLException, IOException, ParserConfigurationException, Exception
	{

            Connection con = getDBConnection();
            XMLDocument document = null;

        try{
            DataRequest dataRequest = new DataRequest();
            dataRequest.setStatementID(STATEMENT_FLORIST_SEARCH);

            dataRequest.setConnection(con);
            
            dataRequest.addInputParam("floristName", (String) arguments.get("floristName"));
            dataRequest.addInputParam("address1", (String) arguments.get("address"));
            dataRequest.addInputParam("inCity", (String) arguments.get("inCity"));
            dataRequest.addInputParam("inState", (String) arguments.get("inState"));
            dataRequest.addInputParam("inZipCode", (String) arguments.get("inZipCode"));
            dataRequest.addInputParam("inPhone", (String) arguments.get("inPhone"));
            dataRequest.addInputParam("productId", (String) arguments.get("productId"));

            XMLDocument dataResponse = (XMLDocument)dataAccess.execute(dataRequest);
            document = XMLUtility.createXMLDocument("FLORIST LOOKUP PAGE VO");
     
            XMLUtility.addSection(document, dataResponse.getChildNodes());
        }
        finally
        {
            if (con != null){
                con.close();
            }
        }
        return document;
	}


  /*  todo:figure out what this method does
   * @parms XMLDocument 
   * @returns Element */
    private Element convertLookupAnchors(XMLDocument sourceLookUpXML) {

        NodeList nl = null;
        String anchor = null;
        LinkedList anchorList = new LinkedList();
        XMLDocument xmlDocument = new XMLDocument();
        Element rootElement = rootElement = xmlDocument.createElement("root");

        try {
            String xpath = "sourceCodeLookup/searchResults/searchResult";
            XPathQuery q = new XPathQuery();
            nl = q.query(sourceLookUpXML, xpath);

            if (nl.getLength() > 0)
            {
                Element element = null;

                for(int i = 0; i < nl.getLength(); i++) {
                    element = (Element) nl.item(i);
                    anchor = element.getAttribute("anchor");

                    if(!anchorList.contains(anchor)) {
                        anchorList.add(anchor);
                    }
                }

                Collections.sort(anchorList);

                Element anchorListElement = xmlDocument.createElement("anchors");
                rootElement.appendChild(anchorListElement);
                Element anchorElement = null;

                Iterator anchorIterator = anchorList.iterator();
                while(anchorIterator.hasNext()) {
                    anchorElement = xmlDocument.createElement("anchor");
                    anchorElement.setAttribute("value", (String) anchorIterator.next());
                    anchorListElement.appendChild(anchorElement);
                }
            }
        }
        catch(Exception e) {
            new Logger(LOGGER_CATEGORY).error(e.toString());
        }

        return rootElement;
    }

/** This method takes in a country code and returns
 * a flag indicating if the passed in country is domestic
 * or not.
 * @param String country code
 * @returns Boolean  True=Domestic */
  public boolean isDomestic(String country)
    throws IOException, ParserConfigurationException, 
            SQLException, Exception
  {

      boolean domesticFlag = false;

      Connection con = null;
      try
      {
        


      con = getDBConnection();

      Map paramMap = new HashMap();
      paramMap.put("IN_COUNTRY_ID",country);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(con);
      dataRequest.setStatementID(GET_COUNTRY_MASTER_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      CachedResultSet  rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
      if(rs == null || !rs.next())
      {
          //default to domsetic
          domesticFlag = true;
      }
      else
      {
          //get value based on what is in DB
          String domValue = (rs.getObject(COUNTRY_MASTER_COUNTRY_TYPE)).toString();          
          if(domValue == null || domValue.equals(DOMESTIC))
          {
            domesticFlag = true;
          }
      }

      }
      finally
      {
       if (con != null){      
       con.close();
       }
      }
      
      return domesticFlag;

  }

/** This method takes in a country code and returns
 * the country data from the database.
 * @param String country code
 * @returns CachedResultSet*/
  public CachedResultSet getCountry(String country)
    throws IOException, ParserConfigurationException, 
            SQLException, Exception
  {

      Connection con = null;
      CachedResultSet  rs = null;
    try{

      con = getDBConnection();

      Map paramMap = new HashMap();
      paramMap.put("IN_COUNTRY_ID",country);

      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(con);
      dataRequest.setStatementID(GET_COUNTRY_MASTER_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
        rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       
    }
    finally
    {
       if (con != null){    
      con.close();
       }
    }
      
      return rs;

  }



 /**
  * Get connection to the database
  * @return Connection
  */
   private Connection getDBConnection() throws Exception
     {
       Connection con = null;       
     
       DataSourceUtil dsUtil = DataSourceUtil.getInstance();
       con = dsUtil.getConnection(datasource);        
         
       return con;         
     }

/* This method retrieves the state for the given state id.
 * @param String state code
 * @return CachedResultSet*/
  public CachedResultSet getState(String state)
    throws IOException, ParserConfigurationException, 
            SQLException, Exception
  {

      Connection con = null;
      CachedResultSet  rs = null;
      try
      {
        


      con = getDBConnection();
  
      Map paramMap = new HashMap();
      paramMap.put("IN_STATE_MASTER_ID",state);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(con);
      dataRequest.setStatementID(GET_STATE_MASTER_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       rs = (CachedResultSet)dataAccessUtil.execute(dataRequest);       

      }
      finally{
       if (con != null){      
      con.close();
       }
      }
      return rs;

  }

  /* Retrieve the product results set from the datatbase.
   * @param String product id
   * @returns CacchedResultSet*/
  public CachedResultSet getProduct(String productId)  
    throws IOException, ParserConfigurationException, SQLException, Exception
  {

      Connection con = null;
      CachedResultSet  rs = null;
      try{


      con = getDBConnection();

      Map paramMap = new HashMap();

      paramMap.put("IN_PRODUCT_ID",productId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(con);
      dataRequest.setStatementID(GET_PRODUCT_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      Map orderMap = null;
      
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
       rs =  (CachedResultSet)dataAccessUtil.execute(dataRequest);   

      }
      finally{
       if (con != null){      
      con.close();
       }
      }
      return rs;   
  }

  /* Retrieve the subcode results set from the datatbase.
   * @param String product id
   * @returns CachedResultSet*/
  public CachedResultSet getSubcode(String subcodeId)  
    throws IOException, ParserConfigurationException, Exception
  {

      Connection con = null;
      CachedResultSet  rs = null;
      try{
  

      con = getDBConnection();

      Map paramMap = new HashMap();

      paramMap.put("PRODUCT_ID",subcodeId);

      // Get the order header infomration
      DataRequest dataRequest = new DataRequest();
      dataRequest.setConnection(con);
      dataRequest.setStatementID(GET_SUBCODE_BY_ID_STATEMENT);
      dataRequest.setInputParams(paramMap);

      DataAccessUtil dau = DataAccessUtil.getInstance();
       rs = (CachedResultSet)dau.execute(dataRequest);
      }
      finally{
       if (con != null){      
      con.close();
       }
      }
      
      return rs;   
   
  }


  public static void main(String args[])
  {
  try{

    String sourceCode = "8261";
    String test = new LookupDAO().getNextOrderNumber();


        System.out.println("::" +     test);

    }
    catch(Throwable t)
    {
      t.printStackTrace();
      
    }
  }

    
}

  
    
