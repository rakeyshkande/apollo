package com.ftd.eventhandling.tests;

import com.ftd.eventhandling.events.EventHandler;
import com.ftd.eventhandling.events.EventHandlerFactory;
import com.ftd.osp.utilities.plugins.Logger;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * The test class will extend junit.framework.TestCase
 *
 * @author Anshu Gaind
 * @version $Id: EventHandlerFactoryTest.java,v 1.2 2006/05/26 15:49:10 mkruger Exp $
 */

public class EventHandlerFactoryTest extends TestCase  {
  private static Logger logger  = new Logger("com.ftd.eventhandling.tests.EventHandlerFactoryTest");

  /**
   * Create a constructor that take a String parameter and passes it
   * to the super class
   **/
  public EventHandlerFactoryTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp
   */
   public void tearDown(){
   }


   /**
    * Write the test case method in the fixture class. Be sure to make it public,
    * or it can't be invoked through reflection
    **/
    public void testEventHandlerCreation(){
      try  {
        EventHandlerFactory factory = EventHandlerFactory.getInstance();
        EventHandler handler = factory.getEventHandler("SEND-PRODUCT-FEED");
        handler.invoke("");

        handler = factory.getEventHandler("SEND-PRODUCT-FEED", "context");
        handler.invoke("");

        handler = factory.getEventHandler("SEND-PRICING-FEED");
        handler.invoke("");

        handler = factory.getEventHandler("SEND-PRICING-FEED", "context");
        handler.invoke("");
      } catch (Throwable ex)  {
        logger.error(ex);
      } finally  {
      }

    }


  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together.
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method
   * suite that returns a test suite.
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case
    // When the test is run, the name of the test is used to look up the method
    // to run, using reflection
    suite.addTest(new EventHandlerFactoryTest("testEventHandlerCreation"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(FrameworkTest.class);
    junit.textui.TestRunner.run(suite());
  }


}