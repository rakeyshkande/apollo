package com.ftd.eventhandling.eventmanager;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.eventhandling.events.EventHandlerFactory;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import java.net.URL;

import java.sql.Connection;

import java.util.Date;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.schema.XSDBuilder;
import oracle.xml.parser.v2.XMLDocument;

import org.w3c.dom.Node;

import org.xml.sax.InputSource;


/**
 *
 * @author Anshu Gaind
 * @version $Id: EventManagerBean.java,v 1.2 2006/05/26 15:49:10 mkruger Exp $
 */
public class EventManagerBean implements MessageDrivenBean, MessageListener
{
  private static Logger logger;
  private MessageDrivenContext messageDrivenContext;
  private String eventsLogDataSourceName;
  private DataSource eventsLogDataSource;
  private XMLSchema schema;

  /**
   * A container invokes this method before it ends the life of the message-driven object.
   * This happens when a container decides to terminate the message-driven object.
   * This method is called with no transaction context.
   *
   * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
   */
  public void ejbCreate()
  {
  }

  /**
   * A container invokes this method before it ends the life of the
   * message-driven object. This happens when a container decides to terminate
   * the message-driven object.
   * This method is called with no transaction context.
   *
   * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
   */
  public void ejbRemove()
  {
  }

  /**
   * Set the associated message-driven context. The container calls this method after the instance creation.
   * The enterprise Bean instance should store the reference to the context object in an instance variable.
   * This method is called with no transaction context.
   *
   * @param ctx A MessageDrivenContext interface for the instance.
   * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx)
  {
    this.messageDrivenContext = ctx;
    InitialContext initContext = null;

    try  {
      logger = new Logger("com.ftd.eventhandling.eventmanager.EventManager");
      initContext = new InitialContext();
      Context myenv = (Context) initContext.lookup("java:comp/env");
      logger.debug("Begin Registering Component");
      eventsLogDataSourceName = (String) myenv.lookup("Events Log Data Source Name");
      ResourceNameRegister.register("Events Log Data Source Name", eventsLogDataSourceName);
      logger.debug("Events Log Data Source Name :: " + eventsLogDataSourceName);
      eventsLogDataSource = (DataSource) myenv.lookup(eventsLogDataSourceName);
      String eventsSchemaName = (String) myenv.lookup("Events Schema Name");
      ResourceNameRegister.register("Events Schema Name", eventsSchemaName);
      URL schemaURL = getClass().getClassLoader().getResource(eventsSchemaName);      
      XSDBuilder builder = new XSDBuilder();
      // Build XML Schema Object
      schema = (XMLSchema)builder.build(schemaURL); 
      logger.debug("Created events schema :: " + eventsSchemaName );
      logger.debug("End Registering Component");

    } catch (Exception ex)  {
      logger.error(ex);
    } finally  {
      try  {
        initContext.close();
      } catch (Exception ex)  {
        logger.error(ex);
      } finally  {
      }
      logger.info("Message Driven Context Set");
    }
  }
  /**
   * Passes a message to the listener.
   *
   * @param message the message passed to the listener
   */
  public void onMessage(Message msg)
  {
    String eventName = null;
    String context = null;
    String payload = null;
    // timestamp the start of the event
    Date started = new Date();
    Date ended = null;
    String status = "SUCCESS";
    String log_message = null;
    Connection con = null;
    try
    {
      logger.debug("***********************************************************");
      logger.debug("* BEGIN TRANSACTION *");
      logger.debug("***********************************************************");

      /**
       * acquire database connection to check whether or not the event is active
       * and to log the event
       */    
      con = eventsLogDataSource.getConnection();
      
      String textMessage = ((TextMessage) msg).getText();

      StringReader sr = new StringReader(textMessage);
      InputSource inputSource = new InputSource(sr);
      XMLErrorHandler errorHandler = new XMLErrorHandler();
      XMLDocument messageDoc = EventHandlingUtility.validateSchema(schema, inputSource, errorHandler);

      if (errorHandler.isValid())
      {
        logger.debug("The 'event' document is valid");
        EventHandlerFactory factory = EventHandlerFactory.getInstance();

       // obtain the appropriate handler, while setting the message driven context
        Node eventNameNode = messageDoc.selectSingleNode("/event/event-name");
        Node contextNode = messageDoc.selectSingleNode("/event/context");
        Node payloadNode = messageDoc.selectSingleNode("/event/payload");
        
        eventName = (eventNameNode.getFirstChild() != null)?eventNameNode.getFirstChild().getNodeValue():null;
        context = (contextNode.getFirstChild() != null)?contextNode.getFirstChild().getNodeValue():null;
        payload = (payloadNode != null && payloadNode.getFirstChild() != null)?payloadNode.getFirstChild().getNodeValue():"";
        
        if (eventName == null)
        {
            throw new UnsupportedOperationException("The event name could not be found on the message");
        }
        if (context == null)
        {
            throw new UnsupportedOperationException("The context could not be found on the message");
        }

        // CHECK WHETHER OR NOT THE EVENT IS ACTIVE
        if (! EventHandlingUtility.isEventActive(eventName, context, con)) 
        {
            throw new Exception("The event "  + eventName + " is not active");
        } 
        
        EventHandler eventHandler = factory.getEventHandler( eventName
                                              , this.messageDrivenContext);
        logger.debug("* BEGIN INVOKING EVENT HANDLER "
                        + eventHandler.getClass().getName()
                        + " FOR EVENT "
                        + eventName
                        + " *");
        
        MessageToken messageToken = new MessageToken();
        
        messageToken.setMessage(payload.trim());
        
        eventHandler.invoke(messageToken);
        
        logger.debug("* END INVOKING EVENT HANDLER "
                        + eventHandler.getClass().getName()
                        + " FOR EVENT "
                        + eventName
                        + " *");
      }
      else
      {
        throw new Exception("The 'event' document is not valid:: " + errorHandler.getParseException().toString());
      }
    } catch (Throwable t)
    {
      status = "FAILURE";
      try 
      {
        // write the stack trace to the log_message variable
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        log_message = sw.toString();
        pw.close();        
      } catch (Exception ex) 
      {
        logger.error(ex);
      } finally 
      {
      }
      
      logger.error(t);
      logger.error("FAILURE :: Rolling back transaction");
      // Rollback the entire transaction
      messageDrivenContext.setRollbackOnly();

    } finally
    {
      // timestamp the end of the event
      ended = new Date();
      EventHandlingUtility.logEvent(eventName, context, started, ended, status, log_message, con);
      
      try 
      {
        if (con != null && (!con.isClosed())) 
        {
            con.close();
            logger.debug(this.eventsLogDataSourceName + " Connection Released");
        }
        
      } catch (Throwable t2) 
      {
        logger.error(t2);
      } 

      logger.debug("***********************************************************");
      logger.debug("* END TRANSACTION *");
      logger.debug("***********************************************************");
    }
    
  }

  

}