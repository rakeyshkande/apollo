package com.ftd.eventhandling.eventmanager;
import oracle.xml.parser.v2.NSResolver;

/**
 * @deprecated
 * @author Anshu Gaind
 * @version $Id: FTDNSResolver.java,v 1.2 2006/05/26 15:49:10 mkruger Exp $
 */

class FTDNSResolver implements NSResolver

{
  public FTDNSResolver()
  {
  }

  /**
   * Find the namespace definition in scope for a given namespace prefix.
   *
   * @param prefix
   * @return the resolved Namespace (null, if prefix could not be resolved)
   */
  public String resolveNamespacePrefix(String prefix)
  {
    if (prefix == "ftd")
    {
        return "http://www.ftd.com/schema";
    }
    else
    {
      return null;
    }


  }
}