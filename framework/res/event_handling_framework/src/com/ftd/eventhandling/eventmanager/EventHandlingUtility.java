package com.ftd.eventhandling.eventmanager;

import com.ftd.eventhandling.events.Event;
import com.ftd.osp.utilities.dataaccess.DataAccessUtil;
import com.ftd.osp.utilities.dataaccess.valueobjects.CachedResultSet;
import com.ftd.osp.utilities.dataaccess.valueobjects.DataRequest;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.schema.XMLSchema;
import oracle.xml.parser.v2.DOMParser;
import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XMLParser;

import oracle.xml.parser.v2.XSLException;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class EventHandlingUtility 
{
  private static Logger logger = new Logger("com.ftd.eventhandling.eventmanager.EventHandlingUtility");


  /**
   * Returns a transactional resource from the EJB container.
   * 
   * @param jndiName
   * @return 
   * @throws javax.naming.NamingException
   */
  public static Object lookupResource(String jndiName)
              throws NamingException
  {
    InitialContext initContext = null;
    try
    {
      initContext = new InitialContext();
      Context myenv = (Context) initContext.lookup("java:comp/env");
      logger.info("Calling resource " + jndiName);
      return myenv.lookup(jndiName);      
    }finally  {
      try  {
        initContext.close();
      } catch (Exception ex)  {
        logger.error(ex);
      } finally  {
      }
    }
  }


  /**
   * Returns the information for the specified event
   * 
   * @param eventName
   * @param context
   * @param con
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
  public static Event getEvent(String eventName, String context, Connection con)
    throws SAXException, ParserConfigurationException, IOException, SQLException, XSLException
  {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      // create the data request object
      DataRequest request = new DataRequest();
      // set database connection
      request.setConnection(con);
      // set statement id
      request.setStatementID("EVENTS.GET_EVENT");
      // set the input parameters
      HashMap inputParams = new HashMap();
      inputParams.put("IN_EVENT_NAME", eventName); 
      inputParams.put("IN_CONTEXT_NAME", context); 
      request.setInputParams(inputParams);

      XMLDocument document = (XMLDocument)dataAccessUtil.execute(request);
      Node eventNameNode = document.selectSingleNode("//event_name/text()");
      Event event = null;
      
      if (eventNameNode != null) 
      {
        event = new Event();
        event.setEventName(document.selectSingleNode("//event_name/text()").getNodeValue());   
        event.setContextName(document.selectSingleNode("//context_name/text()").getNodeValue());   
        event.setDescription(document.selectSingleNode("//description/text()").getNodeValue());        
        boolean active =  document.selectSingleNode("//active/text()").getNodeValue().equalsIgnoreCase("Y")?true:false;
        event.setActive(active);             
      }
            
      return event;
  }


  /**
   * Indicates whether or not the event is active
   * 
   * @param eventName
   * @param context
   * @param con
   * @return 
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   * @throws java.lang.UnsupportedOperationException
   */
  public static boolean isEventActive(String eventName, String context, Connection con) 
          throws SAXException, ParserConfigurationException, IOException, SQLException, XSLException, UnsupportedOperationException
  {
      Event event =  EventHandlingUtility.getEvent(eventName, context, con);
      if (event == null) 
      {
          throw new UnsupportedOperationException( context + "::" + eventName + " could not be found. Please define the event in the EVENTS schema" );
      }
      
      return event.isActive();
  }
  
  /**
   * Logs the event in the event log. This runs as an autonomous transaction.
   * Success or failure of this transaction will not impact the main transaction.
   * 
   * @param eventName
   * @param context
   * @param started
   * @param ended
   * @param status
   * @param message
   * @param con
   */
  public static void logEvent(String eventName, 
                        String context,
                        Date started, 
                        Date ended, 
                        String status, 
                        String log_message, 
                        Connection con)
  {
    try 
    {
      DataAccessUtil dataAccessUtil = DataAccessUtil.getInstance();
      // create the data request object
      DataRequest request = new DataRequest();
      // set database connection
      request.setConnection(con);
      // set statement id
      request.setStatementID("EVENTS.LOG_EVENT");
      // set the input parameters
      HashMap inputParams = new HashMap();
      inputParams.put("IN_EVENT_NAME", eventName); 
      inputParams.put("IN_CONTEXT_NAME", context); 
      inputParams.put("IN_STARTED", new Timestamp(started.getTime())); 
      inputParams.put("IN_ENDED", new Timestamp(ended.getTime())); 
      inputParams.put("IN_STATUS", status); 
      log_message = ((log_message != null) && (log_message.length() > 4000) )?log_message.substring(0,3999):log_message;
      inputParams.put("IN_MESSAGE", log_message); 
      request.setInputParams(inputParams);
      
      Map outputParameters = (Map) dataAccessUtil.execute(request);
      String execution_status = (String)outputParameters.get("OUT_STATUS");
      String message = (String)outputParameters.get("OUT_MESSAGE");
      if (execution_status.equalsIgnoreCase("N")) 
      {
          logger.error("Error logging event " + eventName + ". The transaction will not be rolled back");
          logger.error(message);
      } else
      {
        logger.debug("The event "+ eventName + " was logged successfully");
      }
      
    } catch (Throwable t1) 
    {
      logger.error(t1);
    }     
  }

  /**
   * Validates the XML document against its schema
   * 
   * @param inputSource
   * @param handler
   * @return 
   * @throws java.io.IOException
   * @throws org.xml.sax.SAXException
   */
  public static XMLDocument validateSchema(XMLSchema schema, InputSource inputSource, DefaultHandler handler) throws IOException, SAXException {
    DOMParser domParser = new DOMParser();
    domParser.setValidationMode(XMLParser.SCHEMA_VALIDATION);
    domParser.setErrorHandler(handler);
    domParser.setXMLSchema(schema);
    domParser.showWarnings(true);
    domParser.parse(inputSource);
    logger.debug("The 'event' document was parsed and validated");
    return domParser.getDocument();
	}
  

}