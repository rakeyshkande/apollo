package com.ftd.eventhandling.eventmanager;
import java.util.HashMap;

public class ResourceNameRegister 
{
  private static HashMap register = new HashMap();
  
  /**
   * Get the value of a registered name
   * @param name
   * @return 
   */
  public static String getRegisteredValue(String name)
  {
    return (String) register.get(name);
  }
  
  /**
   * Register a name
   * @param name
   * @param value
   */
  public static void register(String name, String value)
  {
    register.put(name, value);
  }
}