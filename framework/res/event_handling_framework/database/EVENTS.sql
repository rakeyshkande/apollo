CREATE TABLE "EVENTS_LOG"
(
"EVENT_NAME" VARCHAR2 (255) NOT NULL,
"CONTEXT_NAME" VARCHAR2 (255) NOT NULL,
"STARTED" DATE NOT NULL,
"ENDED" DATE NOT NULL,
"STATUS" VARCHAR2 (255) NOT NULL,
"MESSAGE" VARCHAR2 (4000)
)
;
CREATE TABLE "EVENTS"
(
"EVENT_NAME" VARCHAR2 (255) NOT NULL,
"CONTEXT_NAME" VARCHAR2 (255) NOT NULL,
"DESCRIPTION" VARCHAR2 (4000),
"ACTIVE" CHAR (1) DEFAULT 'Y' NOT NULL
)
;
CREATE TABLE "CONTEXT"
(
"CONTEXT_NAME" VARCHAR2 (255) NOT NULL,
"DESCRIPTION" VARCHAR2 (4000),
"ACTIVE" CHAR (1) DEFAULT 'Y'
)
;
ALTER TABLE "EVENTS"
ADD CONSTRAINT "EVENTS_PK1" PRIMARY KEY
(
"EVENT_NAME",
"CONTEXT_NAME"
)
 ENABLE
;
ALTER TABLE "EVENTS"
ADD CONSTRAINT "EVENTS_UK1" UNIQUE
(
"EVENT_NAME",
"CONTEXT_NAME"
)
 ENABLE
;
ALTER TABLE "CONTEXT"
ADD CONSTRAINT "CONTEXT_PK1" PRIMARY KEY
(
"CONTEXT_NAME"
)
 ENABLE
;
ALTER TABLE "CONTEXT"
ADD CONSTRAINT "CONTEXT_UK1" UNIQUE
(
"CONTEXT_NAME"
)
 ENABLE
;
COMMENT ON COLUMN "EVENTS_LOG".STARTED IS 'Time Started'
;
COMMENT ON COLUMN "EVENTS_LOG".ENDED IS 'Time Ended'
;
COMMENT ON COLUMN "EVENTS_LOG".STATUS IS 'From STATUS table'
;
COMMENT ON COLUMN "EVENTS_LOG".MESSAGE IS 'Optional message'
;
COMMENT ON TABLE "EVENTS_LOG" IS 'Log all details relating to timed events'
;
COMMENT ON TABLE "EVENTS" IS 'Detailed description of all timed events'
;
COMMENT ON TABLE "CONTEXT" IS 'Define all contexts here'
;
ALTER TABLE "EVENTS"
ADD CONSTRAINT "EVENTS_CONTEXT_FK1" FOREIGN KEY
(
"CONTEXT_NAME"
)
REFERENCES "CONTEXT"
(
"CONTEXT_NAME"
) ENABLE
;
ALTER TABLE "EVENTS_LOG"
ADD CONSTRAINT "EVENTS_LOG_CHK1" CHECK
(STATUS = 'SUCCESS' 
OR 
STATUS = 'FAILURE')
 ENABLE
;
ALTER TABLE "EVENTS"
ADD CONSTRAINT "EVENTS_CHK1" CHECK
(ACTIVE = 'Y'
 OR 
ACTIVE = 'N')
 ENABLE
;
ALTER TABLE "CONTEXT"
ADD CONSTRAINT "CONTEXT_CHK1" CHECK
(ACTIVE = 'Y'
 OR 
ACTIVE = 'N')
 ENABLE
;
