package com.ftd.framework;

/**
 * Contains all the constant values used within the framework.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class FrameworkConstants {
  public final static String APPLICATION_REALM_FILE_XML = "realm.xml";
  //public final static String APPLICATION_PROPERTIES_DTD = "properties.dtd";
  public static final String SQLSTRING_PLACEHOLDER_CONSTANT = "?";
}
