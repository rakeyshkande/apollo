package com.ftd.framework.tests;
import com.ftd.framework.services.*;
//import com.ftd.framework.FrameworkConstants;
import java.sql.*;
import oracle.jdbc.*;


public class ThreadTester extends Thread  {
  private static int threadCount;
  private int threadNumber;
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager(rm.getProperty("ftd.framework.configuration", "framework.log4j.config.category.name"));

  public ThreadTester() {
     System.out.println("Thread :: " + ++threadCount);
     threadNumber = threadCount;
  }

  public void run(){
       try 
       {      
         DataManager dm = DataManager.getInstance();
         Connection con = null; 
         CallableStatement stmt = null;
         ResultSet rs = null;
         for (int i=0 ;i <1 ;i++ )  {
           con = dm.getConnection(); 
           if (con != null) 
           { 
              System.out.println("Connected");
              stmt = con.prepareCall("{ ? = call SP_GET_FEDEX_REQUEST_DATA ( )}");
              stmt.registerOutParameter(1, OracleTypes.CURSOR);
              lm.debug("PROD");
              lm.debug("Before Execute :: " + System.currentTimeMillis());
              stmt.execute();        
              lm.debug("After Execute :: " +System.currentTimeMillis());

              rs = (ResultSet)stmt.getObject(1);
              lm.debug("Result Set Retrieved :: " + System.currentTimeMillis());
              while(rs.next()){
                lm.debug(rs.getString(3));
              }
              rs.close();
              stmt.close(); 
              con.close(); 


           
/*              System.out.println("Connected");
              stmt = con.createStatement();
              rs = stmt.executeQuery("SELECT STATE_NAME FROM TN_STATE");        
              while (rs.next()){
                  lm.debug("Thread Number :: "+ threadNumber + "::" +rs.getString(1));
              } 
              rs.close(); 
              stmt.close(); 
              con.close(); 
*/           } 
           else 
           { 
             System.out.println("Not Connected"); 
           }
        }//end for
       } 
       catch (Exception e) 
       { 
         e.printStackTrace(); 
       } 
      
    }
    
}
