
package com.ftd.framework.tests;
import junit.framework.*;
//import com.ftd.framework.*;
import com.ftd.framework.services.*;

/**
 * The test class will extend junit.framework.TestCase
 **/
public class ResourceManagerTest extends TestCase  {

  /**
   * Create a constructor that take a String parameter and passes it 
   * to the super class
   **/
  public ResourceManagerTest(String name) {
      super(name);
  }

  /**
   * Override setup() to initialize variables
   **/   
  protected void setUp(){
  }

  /**
   * Override tearDown() to release any permanent resources you allocated in setUp 
   */
   public void tearDown(){
   }

   /**
    * Write the test case method in the fixture class. Be sure to make it public, 
    * or it can't be invoked through reflection
    **/    
  public void testResourceManager(){ 
    try {
      ResourceManager rm = ResourceManager.getInstance();
      System.out.println ("****frameworkconfig::db_user = " + rm.getProperty("ftd.framework.configuration",       "application.db.user"));
      System.out.println ("****interfacemapping::IProjectSVCBusiness = " + rm.getProperty("ftd.application.interfacemapping", "IProjectSVCBusiness"));
    } catch (Exception e){
      e.printStackTrace();
    }
  }

  /**
   * To run several tests at once JUnit provides an object, TestSuite which runs
   * any number of test cases together. 
   * Each test is run as a separate instance of the test class
   * You make your suite accessible to a TestRunner tool with a static method 
   * suite that returns a test suite. 
   **/
  public static Test suite(){
    TestSuite suite = new TestSuite();
    // Create an instance of of Test class that will run this test case 
    // When the test is run, the name of the test is used to look up the method 
    // to run, using reflection
    suite.addTest(new ResourceManagerTest("testResourceManager"));
    return suite;
  }

  /**
   * You can either user the text ui or swing ui to run the test
   **/
  public static void main(String[] args){
    // pass in the class of the test that needs to be run
    //junit.swingui.TestRunner.run(ResourceManagerTest.class);
    junit.textui.TestRunner.run(suite());
  }
  
  
}
