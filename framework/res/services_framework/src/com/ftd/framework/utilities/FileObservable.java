package com.ftd.framework.utilities;
import java.util.Observable;
import java.io.File;

  public class FileObservable extends Observable{
    private File observedFile;
    private long lastModified = 0L;

    /**
     * Creates an observable for a File object
     */
    public FileObservable(){
      super();
    }


    /**
     * Sets the observed file
     * @param observedFile the observed file
     */
    public void setObservable(File observedFile){
      this.observedFile = observedFile;
      lastModified = observedFile.lastModified();
      System.out.println("Setting observable " + observedFile.getAbsolutePath());
    }


    /**
     * gets the observed file
     * @return the observed file
     */
    public File getObservable(){
      return observedFile;
    }

    
    /**
     * Tests if this object has changed.
     *
     * @return true if and only if the setChanged method has been called more 
     *          recently than the clearChanged method on this object; false otherwise.
     */
    public boolean hasChanged(){
      boolean changed = false;
      if (observedFile != null && observedFile.exists())  {
        if (lastModified != observedFile.lastModified())  {
            lastModified = observedFile.lastModified();
            super.setChanged();
            changed = true;
            System.out.println("Observable " + observedFile.getAbsolutePath() + " has changed");
        } 
      }
      return changed;
    }

  }
