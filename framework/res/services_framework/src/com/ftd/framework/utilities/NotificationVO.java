package com.ftd.framework.utilities;
import com.ftd.framework.base.ValueObject;

public class NotificationVO extends ValueObject  {
  private String SMTPHost;
  private String messageContent;
  private String messageSubject;
  private String messageMimeType;
  private String messageFromAddress;
  private String messageTOAddress;
  private String messageCCAddress;
  private String messageBCCAddress;
  private String addressSeparator;

  public NotificationVO() {
    super();
  }

  public String getSMTPHost() {
    return SMTPHost;
  }

  public void setSMTPHost(String newSMTPHost) {
    SMTPHost = newSMTPHost;
  }

  public String getMessageContent() {
    return messageContent;
  }

  public void setMessageContent(String newMessageContent) {
    messageContent = newMessageContent;
  }

  public String getMessageSubject() {
    return messageSubject;
  }

  public void setMessageSubject(String newMessageSubject) {
    messageSubject = newMessageSubject;
  }

  public String getMessageMimeType() {
    return messageMimeType;
  }

  public void setMessageMimeType(String newMessageMimeType) {
    messageMimeType = newMessageMimeType;
  }

  public String getMessageFromAddress() {
    return messageFromAddress;
  }

  public void setMessageFromAddress(String newMessageFromAddress) {
    messageFromAddress = newMessageFromAddress;
  }

  public String getMessageTOAddress() {
    return messageTOAddress;
  }

  public void setMessageTOAddress(String newMessageTOAddress) {
    messageTOAddress = newMessageTOAddress;
  }

  public String getMessageCCAddress() {
    return messageCCAddress;
  }

  public void setMessageCCAddress(String newMessageCCAddress) {
    messageCCAddress = newMessageCCAddress;
  }

  public String getMessageBCCAddress() {
    return messageBCCAddress;
  }

  public void setMessageBCCAddress(String newMessageBCCAddress) {
    messageBCCAddress = newMessageBCCAddress;
  }

  public String getAddressSeparator() {
    return addressSeparator;
  }

  public void setAddressSeparator(String newAddressSeparator) {
    addressSeparator = newAddressSeparator;
  }
}