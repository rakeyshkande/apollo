package com.ftd.framework.utilities;
import java.util.*;

/**
 * Generic Date Utilities
 * 
 * @author Anshu Gaind
 **/
public class DateUtilities  {

    /**
     * Converts a GregorianCalendar object to a string, and in the specified format
     * Currently supports the following formats: <b>mm/dd/yyyy</b> <b>dd/mm/yyyy</b>
     * <b>yyyy-mm-dd</b>
     * 
     * @param gCal the Gregorian Calendar
     * @param format the format of the date string that will be returned
     * @return the calendar formatted as a date string
     **/
    public static String convertCalendarToString(GregorianCalendar gCal, String format){
      if (gCal == null || format == null){
        return null;
      }
      StringBuffer date = new StringBuffer(); 
      String temp = null;
      if (format.equals("mm/dd/yyyy")){
        temp = String.valueOf(gCal.get(gCal.MONTH) + 1);
        date.append( GenericUtilities.lpadValue(temp, 2, "0") );
        date.append("/");
        temp = String.valueOf(gCal.get(gCal.DAY_OF_MONTH));
        date.append( GenericUtilities.lpadValue(temp, 2, "0") );
        date.append("/");
        date.append(gCal.get(gCal.YEAR));
      } else if (format.equals("dd/mm/yyyy")){
        temp = String.valueOf(gCal.get(gCal.DAY_OF_MONTH));
        date.append( GenericUtilities.lpadValue(temp, 2, "0") );
        date.append("/");
        temp = String.valueOf(gCal.get(gCal.MONTH) + 1);
        date.append( GenericUtilities.lpadValue(temp, 2, "0") );
        date.append("/");
        date.append(gCal.get(gCal.YEAR));
      } else if (format.equals("yyyy-mm-dd")){
        date.append(gCal.get(gCal.YEAR));
        date.append("-");
        temp = String.valueOf(gCal.get(gCal.MONTH) + 1);
        date.append( GenericUtilities.lpadValue(temp, 2, "0") );
        date.append("-");
        temp = String.valueOf(gCal.get(gCal.DAY_OF_MONTH));
        date.append( GenericUtilities.lpadValue(temp, 2, "0") );
      } else {
        return null;
      }
      return date.toString();
  }

    /**
     * Converts a date string to a Calendar object, and in the specified format
     * Currently supports the following formats: <b>mm/dd/yyyy</b> <b>dd/mm/yyyy</b>
     * <b>yyyy-mm-dd</b>
     * 
     * @param dateString the date string
     * @param format the format of the date string
     * @return the string formatted as a Calendar
     **/

  public static Calendar convertStringToCalendar(String dateString, String format){
      if (dateString == null || format == null){
        return null;
      }
      if (dateString.length() != 10){
        return null;
      }
      GregorianCalendar gCal = new GregorianCalendar(getYear(dateString, format),
                                                     getMonth(dateString, format) - 1,
                                                     getDate(dateString, format));
      return gCal;
  }


    /**
     * Converts a date string from its current format to the required format
     * Currently supports the following formats: <b>mm/dd/yyyy</b> <b>dd/mm/yyyy</b>
     * <b>yyyy-mm-dd</b>
     * 
     * @param dateString the date string
     * @param currentFormat the format of the date string
     * @param requiredFormat the desired format for the date string
     * @return the reformatted string
     **/
  public static String convertDateFormat(String dateString, 
                                         String currentFormat, 
                                         String requiredFormat){
      if (dateString == null || currentFormat == null || requiredFormat == null){
        return null;
      }
      if (dateString.length() != 10){
        return dateString;
      }
      GregorianCalendar gCal = new GregorianCalendar(getYear(dateString, currentFormat), 
                                     getMonth(dateString, currentFormat) - 1, 
                                     getDate(dateString, currentFormat));
      return convertCalendarToString(gCal, requiredFormat);                                     
  }

    /**
     * Returns the month from a formatted date string
     * Currently supports the following formats: <b>mm/dd/yyyy</b> <b>dd/mm/yyyy</b>
     * <b>yyyy-mm-dd</b>
     * 
     * @param dateString the date string
     * @format format the format of the date String
     * @return the month
     **/
  public static int getMonth(String dateString, String format){
      if (dateString == null || format == null){
        return 0;
      }
      if (dateString.length() != 10){
        return 0;
      }
      if (format.equals("mm/dd/yyyy")){
        return Integer.valueOf(dateString.substring(0,2)).intValue();
      } else if (format.equals("dd/mm/yyyy")){
        return Integer.valueOf(dateString.substring(3,5)).intValue();
      } else if (format.equals("yyyy-mm-dd")){
        return Integer.valueOf(dateString.substring(5,7)).intValue();
      } else{
        return 0;
      }
  }

    /**
     * Returns the date from a formatted date string
     * Currently supports the following formats: <b>mm/dd/yyyy</b> <b>dd/mm/yyyy</b>
     * <b>yyyy-mm-dd</b>
     * 
     * @param dateString the date string
     * @format format the format of the date String
     * @return the date
     **/
  public static int getDate(String dateString, String format){
      if (dateString == null || format == null){
        return 0;
      }
      if (dateString.length() != 10){
        return 0;
      }
      if (format.equals("mm/dd/yyyy")){
        return Integer.valueOf(dateString.substring(3,5)).intValue();
      } else if (format.equals("dd/mm/yyyy")){
        return Integer.valueOf(dateString.substring(0,2)).intValue();
      } else if (format.equals("yyyy-mm-dd")){
        return Integer.valueOf(dateString.substring(8,10)).intValue();
      } else{
        return 0;
      }
  }

    /**
     * Returns the year from a formatted date string
     * Currently supports the following formats: <b>mm/dd/yyyy</b> <b>dd/mm/yyyy</b>
     * <b>yyyy-mm-dd</b>
     * 
     * @param dateString the date string
     * @format format the format of the date String
     * @return the year
     **/
  public static int getYear(String dateString, String format){
      if (dateString == null || format == null){
        return 0;
      }
      if (dateString.length() != 10){
        return 0;
      }
      if (format.equals("mm/dd/yyyy")){
        return Integer.valueOf(dateString.substring(6,10)).intValue();
      } else if (format.equals("dd/mm/yyyy")){
        return Integer.valueOf(dateString.substring(6,10)).intValue();
      } else if (format.equals("yyyy-mm-dd")){
        return Integer.valueOf(dateString.substring(0,4)).intValue();
      } else{
        return 0;
      }
  }


}