package com.ftd.framework.utilities;
import javax.naming.InitialContext;

/**
 * @deprecated
 */

public class InitialContextObject  {

  private InitialContext context;
  private double GUID;
  
  public InitialContextObject(InitialContext context) {
    this.context = context;
    this.GUID = Math.random();
  }

  public InitialContext getContext() {
    return context;
  }

  public double getGUID(){
    return GUID;
  }

  public void setContext(InitialContext newContext) {
    context = newContext;
  }
  /**
   * Overrides the equal method in the object Object
   * @param object object
   */
  public boolean equals(Object obj){
    InitialContextObject newInitialContextObject = (InitialContextObject) obj;
    double newGUID = newInitialContextObject.getGUID();

    if (newGUID == this.GUID)  {
      return true;
    }else {
      return false;
    }
    
  }
}