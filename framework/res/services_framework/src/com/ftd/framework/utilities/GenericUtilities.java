package com.ftd.framework.utilities;


/**
 * Generic Utilities
 *
 * @author Anshu Gaind
 **/
public class GenericUtilities  {
  /**
   * Pads a string with the specified pad string. It appends the pad string to the
   * left of the specified string.
   *
   * @param value the string that will be padded
   * @param length the desired length of the string
   * @param padString the string that will be appended to the original string
   * @return the padded string
   **/
  public static String lpadValue(String value, int length, String padString){
      if (value == null || length == 0 || padString == null){
        return null;
      }
    int diff = length - value.length();
    StringBuffer paddedValue = new StringBuffer();
    if ( diff > 0 ){
      for (int i = 0 ; i < diff; i++){
        paddedValue.append(padString);
      }
      paddedValue.append(value);
    } else {
      return value;
    }
    return paddedValue.toString();
  }

  /**
   * Pads a string with the specified pad string. It appends the pad string to the
   * right of the specified string.
   *
   * @param value the string that will be padded
   * @param length the desired length of the string
   * @param padString the string that will be appended to the original string
   * @return the padded string
   **/
  public static String rpadValue(String value, int length, String padString){
      if (value == null || length == 0 || padString == null){
        return null;
      }
    int diff = length - value.length();
    StringBuffer paddedValue = new StringBuffer();
    if ( diff > 0 ){
      paddedValue.append(value);
      for (int i = 0 ; i < diff; i++){
        paddedValue.append(padString);
      }
    } else {
      return value;
    }
    return paddedValue.toString();
  }

  /**
   * Removes a character from a string
   * 
   * @param  s the string
   * @param c the character that will be removed from the string
   * @return the processed string
   * @author Albert Kho
   **/
   public static String removeChar(String s, char c) {
      if (s == null){
        return null;
      }
     StringBuffer r = new StringBuffer();
     for (int i = 0; i < s.length(); i ++) {
        if (s.charAt(i) != c) {
          r.append(s.charAt(i));
        }
     }
     return r.toString();
  }

   /**
   * Removes a non digits from a string
   * 
   * @param  s the string
   * @return the processed string
   * @author Albert Kho
   **/
   public static String removeNonDigit(String s) {
      if (s == null){
        return null;
      }
     StringBuffer r = new StringBuffer();
     for (int i = 0; i < s.length(); i ++) {
        if (Character.isDigit(s.charAt(i))) {
          r.append(s.charAt(i));
        }
     }
     return r.toString();
  }

}