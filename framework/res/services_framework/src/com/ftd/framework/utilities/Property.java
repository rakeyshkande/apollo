package com.ftd.framework.utilities;

/**
 * Represents a property as configured in the xml files
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class Property
{
  private String name;
  private String value;
  /**
   * This property has been marked as IMPLIED in the DTD. Therefore it has
   * a default value
   */
  private String isThreadSafe = "no";

  /**
   * The default constructor
   **/
  public Property(){
  }

  /*
   * Constructor
   */
  public Property(String newName, String newValue)
  {
    this.name = newName;
    this.value = newValue;
  }

  /*
   * Constructor
   */
  public Property(String newName, String newValue, String isThreadSafe)
  {
    this.name = newName;
    this.value = newValue;
    this.isThreadSafe = isThreadSafe;
  }
  
  /**
   * Return the name of the property
   *
   * @return property name
   **/
  public String getName(){
    return name;
  }

  /** 
   * Sets the property name
   *
   * @param newName the property name
   **/
  public void setName(String newName){
    name = newName;
  }

  /**
   * Returns the property value
   *
   * @return the property value
   **/
  public String getValue(){
    return value;
  }

  /**
   * Sets the property value
   *
   * @param newValue the property value
   **/
  public void setValue(String newValue){
    value = newValue;
  }

  /**
   * Gets whether or not the implementation object is threadsafe
   *
   * @return the property value
   */
  public String getIsThreadSafe() {
    return isThreadSafe;
  }

  /**
   * Sets whether or not the implementation object is threadsafe
   *
   * @param newIsThreadSafe the property value
   */
  public void setIsThreadSafe(String newIsThreadSafe) {
    isThreadSafe = newIsThreadSafe;
  }



}