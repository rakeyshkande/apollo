package com.ftd.framework.utilities;
import com.ftd.framework.services.*;
import java.util.HashMap;
import java.util.Observable;
import java.io.File;
import java.util.Observer;

/**
 * Watches all configuration files that have been registered, for any changes.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/

public class ConfigurationObserver implements Observer {
  private HashMap observables = new HashMap();
  
  public ConfigurationObserver() {
    super();
  }

  /**
   * This method is called whenever the observed object is changed. 
   * An application calls an Observable object's notifyObservers method to have 
   * all the object's observers notified of the change.
   * 
   * @param observable the observable object
   * @param arg an argument passed to the notifyObservers method
   */
   public void update(Observable observable, Object arg){
      File observedFile = ((FileObservable)observable).getObservable();
      System.out.println("Observer notified of a change on " + observedFile.getAbsolutePath());
      ((ResourceManager)arg).resetRealms();
   }

}