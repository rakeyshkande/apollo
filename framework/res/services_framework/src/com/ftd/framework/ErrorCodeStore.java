// Unit Tested
// need rework on findValue(1, 2);
package com.ftd.framework;

import java.util.*;
import java.io.*;
import java.text.MessageFormat;

import com.ftd.framework.services.*;

/**
 * This class loads all error codes from the error string file and has 
 * methods to convert error codes to error strings.
 * 
 * @author: Anshu Gaind
 * @version 1.0 
 */
 
public class ErrorCodeStore {
	
	protected static ResourceBundle ERRORLISTBUNDLE;
	private static ErrorCodeStore ERRORCODESTORE;
	private static FileInputStream FIS;

	
  /**
   * Ensures a singleton and loads the error codes
   *
   */
  private ErrorCodeStore() {
    super();
  }

  /**
  *  Used to get the only instance of ErrorCodeStore.
  *
  * @return the error code store object
  */
  public static ErrorCodeStore getInstance() {
      if (ERRORCODESTORE == null) {
        ERRORCODESTORE = new ErrorCodeStore();
        loadErrorCodes();
        return ERRORCODESTORE;
      } else {
        return ERRORCODESTORE;
      }	
  }

  /**
   * Returns the error message associated with an error code
   *
   * @param msgCode the error code
   * @return the error message
   **/
  public String findValue(int msgCode)  {
    StringBuffer prtStr = new StringBuffer(msgCode);
    prtStr.append('|');

    try {
      if (ERRORLISTBUNDLE != null) {
        prtStr.append(ERRORLISTBUNDLE.getString(Integer.toString(msgCode)));
      } else {
        prtStr.append("Unknown message code(no ResourceBundle)");
        prtStr.append(msgCode);
      }
    } catch (MissingResourceException mre) {
      prtStr.append("Unknown message code: ");
      prtStr.append(msgCode);
    }
	  return prtStr.toString();
  }

  /**
   * Returns the custom error message associated with an error code.
   * The parameters passed in are inserted into the error message and 
   * in the same order.
   * 
   * @param msgCode the error code
   * @param args[] the error message parameters
   * @return the custom error message
   **/
  public String findValue(int msgCode, String args[])  {
    StringBuffer prtStr = new StringBuffer(msgCode);
    prtStr.append('|');

    try {
      if (ERRORLISTBUNDLE != null) {
        prtStr.append(ERRORLISTBUNDLE.getString(Integer.toString(msgCode)));
      } else {
        prtStr.append("Unknown message code(no ResourceBundle)");
        prtStr.append(msgCode);
      }
    } catch (MissingResourceException mre) {
      prtStr.append("Unknown message code: ");
      prtStr.append(msgCode);
    }
	
    //append the arguments if there are any.	
    if (args != null) 
    {
/*      
        for (int i = 0; i < args.length; i++) {
        prtStr.append('|');
        prtStr.append(args[i]);
        }
*/
        MessageFormat format = new MessageFormat(escape(prtStr.toString()));
        prtStr = new StringBuffer(format.format(args));
    }
    return prtStr.toString();
  }

  /**
   * Loads the error codes.
   * 
   */
  private synchronized static void loadErrorCodes() {
    ResourceManager resourceManager = ResourceManager.getInstance();
    String fileLoc = resourceManager.getProperty("ftd.framework.configuration", "application.error.strings");

    try {
      if (fileLoc != null){
        ERRORLISTBUNDLE = ResourceBundle.getBundle(fileLoc);		
      }else{
        System.out.println("***************************************************");
        System.out.println("*      Could not load error strings file          *");
        System.out.println("***************************************************");
      }
    } catch (Exception e) {
      //printing to System.err since we cannot use LogManager for this operation
      System.out.println("***************************************************");
      System.out.println("* Could not load error codes in ErrorCodeStore    *");
      System.out.println("* Exception:: " + e.getClass().getName());         
      System.out.println("***************************************************");
      e.printStackTrace();
    }
  }

    /**
     * Escape any single quote characters that are included in the specified
     * message string.
     *
     * @param string The string to be escaped
     */
    protected String escape(String string) {

        if ((string == null) || (string.indexOf('\'') < 0))
            return (string);
        int n = string.length();
        StringBuffer sb = new StringBuffer(n);
        for (int i = 0; i < n; i++) {
            char ch = string.charAt(i);
            if (ch == '\'')
                sb.append('\'');
            sb.append(ch);
        }
        return (sb.toString());

    }
}
