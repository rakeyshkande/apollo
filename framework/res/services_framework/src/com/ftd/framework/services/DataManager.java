package com.ftd.framework.services;

import com.ftd.framework.exception.BadConnectionException;
import com.ftd.framework.exception.FTDSystemException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;
import com.ftd.framework.utilities.*;

/**
 * Encapsulates all interactions with a database
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class DataManager  {
  private static DataManager DATAMANAGER;
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager("com.ftd.framework.services.DataManager");
  private CacheManager cacheManager = CacheManager.getInstance();
  
  /**
   * The private constructor
   **/
   private DataManager(){
    super();
   }

 /**
  * The static initializer for the Data Manager. It ensures that
  * at a given time there is only a single instance  
  * 
  * @return the data manager
  * @author Anshu Gaind
  **/
  public static DataManager getInstance()
  {
    if ( DATAMANAGER == null )
    {
      DATAMANAGER = new DataManager();
      return DATAMANAGER;
    }
    else
    {
      return DATAMANAGER;
    }
  }
   
  /**
   * Returns a default connection from the connection pool. If user name and 
   * password are defined in the framework configuration file, they will override the user
   * name and password set up for the data source.
   * 
   * @return connection to the database
   * @throws BadConnectionException
   * @throws FTDSystemException
   * @author Anshu Gaind
   **/
  public Connection getConnection() throws BadConnectionException, FTDSystemException {
      DataSource ds = (DataSource)cacheManager.getCache(rm.getProperty("ftd.framework.configuration", "application.db.datasource"));
    	Connection con=null;

      String userName = rm.getProperty("ftd.framework.configuration", 
                                                    "application.db.user");
      String password = rm.getProperty("ftd.framework.configuration", 
                                                    "application.db.password");

      try{
        if(ds==null) {
          String dataSource = rm.getProperty("ftd.framework.configuration", "application.db.datasource");
          if ((dataSource != null) || (! dataSource.equals("")) ){    
            ds=(DataSource)  new InitialContext().lookup( dataSource );
            // cache the data source in application context
            cacheManager.setCache(dataSource, ds);
          } else {
            lm.error("DataManager:: Please specify a valid data source ");
            throw new BadConnectionException(5);
          }
        }

        // use the values defined in the framework configuration file first
        if( (userName != null) && (!userName.equals("")) ){
          if ( (password != null) && (! password.equals("") ) ){
            con=ds.getConnection(userName, password);
          }  
        }	else {
          // use the values defined in the data source
          con = ds.getConnection();
        }
      } catch (NamingException ne) {
          lm.error(ne);
         throw new BadConnectionException(6, ne);		
      } catch (SQLException se){
          lm.error(se);
         throw new BadConnectionException(7, se);
      }
      return con;

  }
  

  /**
   * Returns a connection from the connection pool, for the specified data source.
   *
   * @param dataSource the name of the data source
   * @param userName the user name
   * @param password the password
   * @return connection to the database
   * @throws BadConnectionException
   * @throws FTDSystemException
   * @author Anshu Gaind
   **/
  public Connection getConnection(String dataSource, String userName, String password) 
        throws BadConnectionException, FTDSystemException {
    	Connection con = null;
      try{
        if ( (dataSource == null) || (dataSource.equals("")) ){
          lm.error("DataManager:: DataSource  has no value ");
          throw new BadConnectionException(5);
        }
        if ( (userName == null) || (userName.equals("")) ){
          lm.error("DataManager:: UserName has no value ");
          throw new BadConnectionException(5);
        }
        if ( (password == null) || (password.equals("")) ){
          lm.error("DataManager:: Password has no value ");
          throw new BadConnectionException(5);
        }

        DataSource ds = (DataSource)cacheManager.getCache(dataSource);
        // try cache
        if(ds==null) {
            ds=(DataSource) new InitialContext().lookup(dataSource);
            // cache the data source in application context
            cacheManager.setCache(dataSource,ds);
            // get the connection  
            con = ds.getConnection(userName, password);
        } else {
          // get the connection  
          con = ds.getConnection(userName, password);
        }
      } catch (NamingException ne) {
          lm.error(ne);
         throw new BadConnectionException(6, ne);		
      } catch (SQLException se){
          lm.error(se);
         throw new BadConnectionException(7, se);
      }
      return con;

  }


  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(ResultSet resultSet) throws BadConnectionException {
      try{
        if (resultSet != null){
          resultSet.close();
        }
      } catch (SQLException se){
          lm.error(se);
         throw new BadConnectionException(7, se);
      }
      return true;
  }

  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(Statement statement) throws BadConnectionException {
      try{
        if (statement != null){
          statement.close();
        }
      } catch (SQLException se){
          lm.error(se);
         throw new BadConnectionException(7, se);
      }
      return true;
  }

  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(ResultSet resultSet, 
                                 Statement statement) throws BadConnectionException {
      try{
        if (resultSet != null){
          resultSet.close();
        }
        if (statement != null){
          statement.close();
        }
      } catch (SQLException se){
         lm.error(se); 
         throw new BadConnectionException(7, se);
      }
      return true;
  }


  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @param connection the connection 
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(Statement statement, 
                                Connection connection) throws BadConnectionException {
      try{
        if (statement != null){
          statement.close();
        }
        if (connection != null){
          connection.close();
        }
      } catch (SQLException se){
         lm.error(se);
         throw new BadConnectionException(7, se);
      }
      return true;
  }

  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @param connection the connection 
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(ResultSet resultSet, 
                                 Statement statement, 
                                Connection connection) throws BadConnectionException {
      try{
        if (resultSet != null){
          resultSet.close();
        }
        if (statement != null){
          statement.close();
        }
        if (connection != null){
          connection.close();
        }
      } catch (SQLException se){
         lm.error(se);
         throw new BadConnectionException(7, se);
      }
      return true;
  }


}
