package com.ftd.framework.services;

//import com.ftd.framework.*;
import com.ftd.framework.utilities.InitialContextObject;

import java.util.Hashtable;
import java.util.Stack;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * Manages a pool of Initial Contexts
 *
 * @author Anshu Gaind
 * @version 1.0 
 * @deprecated
 **/

public class InitialContextPoolManager  {
  private static InitialContextPoolManager POOLMANAGER;
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager(rm.getProperty("ftd.framework.configuration", "framework.log4j.config.category.name"));
  private Stack freePool = new Stack();
  
  private InitialContextPoolManager() {
     super();
  }

 /**
  * The static initializer for the pool manager. It ensures that
  * at a given time there is only a single instance 
  *
  * @return the pool manager
  * @author Anshu Gaind
  **/
  public static InitialContextPoolManager getInstance()  
  {
    if ( POOLMANAGER == null )
    {
      POOLMANAGER = new InitialContextPoolManager();
      POOLMANAGER.createPool();
      return POOLMANAGER;
    }
    else
    {
      return POOLMANAGER;
    }    
  }

  /**
   * Returns the initial context to the naming server
   *
   * @param contextKey the context key
   * @return the initial context
   * @throws NamingException if the context key is not set up or is null
   **/
  public synchronized InitialContextObject getContextObject(String contextKey) throws NamingException {
    InitialContextObject initialContextObject = null;

    if (freePool.empty())  {
        incrementPool();
    }  
    initialContextObject = (InitialContextObject) freePool.pop();
    lm.debug("Free Pool Size = " + freePool.size());
    return initialContextObject;
  }

  /**
   * Returns the context back to the pool
   *
   * @param context the initial context
   */
  public synchronized void returnContextObject(InitialContextObject initialContextObject){
    freePool.push(initialContextObject);
  }
  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
   private synchronized void createPool(){
      int poolSize = Integer.valueOf(rm.getProperty("ftd.framework.configuration", 
                                       "initial.context.pool.size")).intValue();                                       
      InitialContext context = null;
      InitialContextObject initialContextObject = null;
      try { 
        for (int i=0 ;i<poolSize ;i++ )  {
          context = createInitialContext(getInitialContextHashtable());
          initialContextObject = new InitialContextObject(context);
          freePool.push(initialContextObject);
        }
        lm.debug("Creating an Initial Context Pool of Size " + poolSize);
      }catch (NamingException ne) {
        lm.error("ServiceLocator:: Unable to create context for the default context");
        lm.error(ne);
      }                                             
   }

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
   private synchronized void incrementPool(){
      int poolSizeIncrementValue = Integer.valueOf(rm.getProperty("ftd.framework.configuration", 
                                       "initial.context.pool.size.increment.value")).intValue();                                       
      InitialContext context = null;
      InitialContextObject initialContextObject = null;
      try { 
        for (int i=0 ;i<poolSizeIncrementValue ;i++ )  {
          context = createInitialContext(getInitialContextHashtable());
          initialContextObject = new InitialContextObject(context);
          freePool.push(initialContextObject);
        }
        lm.debug("Incrementing Initial Context Pool Size by " + poolSizeIncrementValue);
      }catch (NamingException ne) {
        lm.error("ServiceLocator:: Unable to create context for the default context");
        lm.error(ne);
      }                                             
   }

  /**
   * Creates the initial context
   *
   * @param contextFactory the factory name
   * @param providerURL the provider url
   * @param securityPrincipal the security principal
   * @param securityCredentials the security credentials
   * @return the initial context
   * @throws NamingException
   **/
  private synchronized InitialContext createInitialContext( Hashtable env) 
                                            throws NamingException {
      InitialContext initialContext = new InitialContext(env);
      return initialContext;
  }

  
  /**
   * Returns the initial context connection hashtable
   */
  private Hashtable getInitialContextHashtable(){
      String contextFactory = rm.getProperty("ftd.framework.configuration", "jndi.context.factory");
      String providerURL = rm.getProperty("ftd.framework.configuration", "jndi.provider.url");
      String securityPrincipal = rm.getProperty("ftd.framework.configuration", "jndi.context.security.principal");
      String securityCredentials = rm.getProperty("ftd.framework.configuration", "jndi.context.security.credentials");

      Hashtable env = new Hashtable();
      env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
      env.put(Context.PROVIDER_URL, providerURL);
      env.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
      env.put(Context.SECURITY_CREDENTIALS, securityCredentials);

      return env;
  }
}