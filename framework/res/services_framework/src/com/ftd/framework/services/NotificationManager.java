package com.ftd.framework.services;
import com.ftd.framework.exception.FTDException;
import com.ftd.framework.utilities.NotificationVO;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class NotificationManager  {
  private static NotificationManager NOTIFICATIONMANAGER;
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager("com.ftd.framework.services.NofificationManager");
  
  /**
   * The private constructor
   **/
   private NotificationManager(){
    super();
   }

 /**
  * The static initializer for the Notification Manager. It ensures that
  * at a given time there is only a single instance  
  * 
  * @return the data manager
  * @author Anshu Gaind
  **/
  public static NotificationManager getInstance()
  {
    if ( NOTIFICATIONMANAGER == null )
    {
      NOTIFICATIONMANAGER = new NotificationManager();
      return NOTIFICATIONMANAGER;
    }
    else
    {
      return NOTIFICATIONMANAGER;
    }
  }

  /**
   * Notifies via email to all registered addresses
   * 
   * @param notificationVO The notification value object
   * @exception FTDException
   */
   public void notify(NotificationVO notificationVO) throws FTDException{
    try  {
      Properties props = new Properties();
      // fill props with any information
      // Setup mail server
      props.put("mail.smtp.host", notificationVO.getSMTPHost());
      Session session = Session.getDefaultInstance(props, null);
      MimeMessage message = new MimeMessage(session);
      Address fromAddress = new InternetAddress(notificationVO.getMessageFromAddress()); 
      message.setFrom(fromAddress);

      // TO
      if (hasMultipleAddresses(Message.RecipientType.TO, notificationVO))  {
        Address toAddress[] = handleMultipleAddresses(Message.RecipientType.TO, notificationVO);
        message.addRecipients(Message.RecipientType.TO, toAddress);
      } else {
        if (   (   notificationVO.getMessageTOAddress() != null   ) 
            && ( ! notificationVO.getMessageTOAddress().equals("")) )  {
          Address toAddress = new InternetAddress(notificationVO.getMessageTOAddress());
          message.addRecipient(Message.RecipientType.TO, toAddress);            
        }        
      }
      // CC
      if (hasMultipleAddresses(Message.RecipientType.CC, notificationVO))  {
        Address ccAddress[] = handleMultipleAddresses(Message.RecipientType.CC, notificationVO);
        message.addRecipients(Message.RecipientType.CC, ccAddress);
      } else {
        if (   (  notificationVO.getMessageCCAddress() != null   ) 
            && ( ! notificationVO.getMessageCCAddress().equals("")) )  {
            Address ccAddress = new InternetAddress(notificationVO.getMessageCCAddress());
            message.addRecipient(Message.RecipientType.CC, ccAddress);
        }
      }
      // BCC
      if (hasMultipleAddresses(Message.RecipientType.BCC, notificationVO))  {
        Address bccAddress[] = handleMultipleAddresses(Message.RecipientType.BCC, notificationVO);
        message.addRecipients(Message.RecipientType.BCC, bccAddress);
      } else {
        if (   (   notificationVO.getMessageBCCAddress() != null   ) 
            && ( ! notificationVO.getMessageBCCAddress().equals("")) )  {
            Address bccAddress = new InternetAddress(notificationVO.getMessageBCCAddress());
            message.addRecipient(Message.RecipientType.BCC, bccAddress);
        }
      }

      message.setSubject(notificationVO.getMessageSubject());
      message.setContent(notificationVO.getMessageContent()
                        , notificationVO.getMessageMimeType());
      Transport.send(message);

    } catch (Exception ex)  {
      lm.error(ex);
    } finally  {
    }
    
   }
  /**
   * Indicates whether or not there are multiple  Addresses
   */
   private boolean hasMultipleAddresses(Message.RecipientType  recipientType, NotificationVO notificationVO){
      String separator = notificationVO.getAddressSeparator();
      String addresses = null;
      
      if (recipientType == Message.RecipientType.TO)  {
        addresses = notificationVO.getMessageTOAddress();
      } else if (recipientType == Message.RecipientType.CC)  {
        addresses = notificationVO.getMessageCCAddress();                    
      } else if (recipientType == Message.RecipientType.BCC){
        addresses = notificationVO.getMessageBCCAddress();
      }

      if ( (addresses != null) && (!addresses.equals("")) && (addresses.indexOf(separator) != -1) )  {
          return true;
      } else {
        return false;
      }
   }

   /**
    * Returns multiple  Addresses as an Address array
    *
    * @return multiple addresses as an Address array
    */
   private Address[] handleMultipleAddresses(Message.RecipientType  recipientType, NotificationVO notificationVO) throws AddressException{
      ArrayList list = new ArrayList();
      String addresses = null;
      Address addressArray[] = null;
      
      if (recipientType == Message.RecipientType.TO)  {
        addresses = notificationVO.getMessageTOAddress();
      } else if (recipientType == Message.RecipientType.CC)  {
        addresses = notificationVO.getMessageCCAddress();                       
      } else if (recipientType == Message.RecipientType.BCC){
        addresses = notificationVO.getMessageBCCAddress();   
      }
      
      if ( (addresses != null) && (!addresses.equals("")) )  {
        StringTokenizer st = new StringTokenizer(addresses, notificationVO.getAddressSeparator());
      
        while (st.hasMoreTokens())  {
          list.add(new InternetAddress(st.nextToken()));
        }
      
        addressArray = new Address[list.size()];
        for (int i = 0; i < addressArray.length ; i++)  {
          addressArray[i] = (Address)list.get(i);
        }
      }
      lm.debug("Sending Email Notifications to :: " + addresses);
      return addressArray;                      
   }
   
}