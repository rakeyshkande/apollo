package com.ftd.framework.services;
import java.util.HashMap;
//import com.ftd.framework.FrameworkConstants;

/**
 * Encapsulates all interactions with the application cache
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/

public class CacheManager  {
  private static CacheManager CACHEMANAGER;
  private LogManager lm = new LogManager("com.ftd.framework.services.CacheManager");
  private HashMap cache = new HashMap();


  /**
   * The private constructor
   **/
  private CacheManager() {
    super();
  }


 /**
  * The static initializer for the Cache Manager. It ensures that
  * at a given time there is only a single instance  
  * 
  * @return the cache manager
  * @author Anshu Gaind
  **/
  public static CacheManager getInstance()
  {
    if ( CACHEMANAGER == null )
    {
      CACHEMANAGER = new CacheManager();
      return CACHEMANAGER;
    }
    else
    {
      return CACHEMANAGER;
    }
  }



  /**
   * Set an object in the cache
   * 
   * @param name the logical name of the object to be cached
   * @param value the object that will be cached
   */
  public synchronized void setCache(String name, Object value){
    cache.put(name, value);
    lm.debug("Adding " + name + " to cache");
  }


  /**
   * Get an object from the cache
   * 
   * @param name the logical name of the object to be retrieved from cache
   * @return the cached object
   */
  public Object getCache(String name){
    Object rv = cache.get(name);
    if (rv == null) {
      lm.debug("Object " + name + " not found in cache");
    } else {
      lm.debug("Retrieving " + name + " from cache");
    }
    return rv;
  }


  /**
   * Invalidates an object in cache
   * 
   * @param name the logical name of the object to invalidated in the cache
   */
  public synchronized void invalidateCache(String name){
    cache.remove(name);
    lm.debug("Invalidating " + name + " in cache");
  }


  /**
   * Invalidates the entire cache
   * 
   */
  public synchronized void invalidateCache(){
    cache.clear();
    lm.debug("Invalidating entire cache");
  }

}