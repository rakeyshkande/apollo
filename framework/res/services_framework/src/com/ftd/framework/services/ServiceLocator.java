package com.ftd.framework.services;

import com.ftd.framework.exception.BadServiceException;
import com.ftd.framework.interfaces.IService;
import com.ftd.framework.interfaces.IServiceHome;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * This class abstracts the JNDI lookups of EJBs.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
 
public class ServiceLocator  {
  private static ServiceLocator SERVICELOCATOR;
  private static HashMap contextMap = new HashMap();
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager("com.ftd.framework.services.ServiceLocator");
  
 /**
  * The private constructor
  *
  * @author Anshu Gaind
  **/
  private ServiceLocator() {
    super();
  }
  
 /**
  * The static initializer for the service locator. It ensures that
  * at a given time there is only a single instance 
  *
  * @return the service locator
  * @author Anshu Gaind
  **/
  public static ServiceLocator getInstance()  
  {
    if ( SERVICELOCATOR == null )
    {
      SERVICELOCATOR = new ServiceLocator();
      return SERVICELOCATOR;
    }
    else
    {
      return SERVICELOCATOR;
    }
  }
  
  
  /**
   * Returns the home interface of the ejb. The object will then need to 
   * be downcasted to the appropriate home interface.
   *
   * @param serviceName the jndi name of the service for which to obtain the home interface
   * @return the service home interface
   * @throws BadServiceException if the service name is not registered with the naming server, or if it is null.
   **/
  public IServiceHome getServiceHome(String serviceName) throws BadServiceException{
    IServiceHome svc = null;

    if (serviceName == null) {
      lm.error("ServiceLocator:: Service name cannot be null");
      throw new BadServiceException(1, new String[]{"Service name cannot be null"});	
    }

    try {

      svc = (IServiceHome) new InitialContext().lookup(serviceName);

    } catch(NamingException ne) {
        lm.error(ne);
        throw new BadServiceException(1, ne);
    }	catch (ClassCastException cce){
        lm.error(cce);
        throw new BadServiceException(1, new String[]{"This problem is likely due to an incorrect JNDI Name"}, cce);
    }
    return svc;
  }


  /**
   * Returns the home interface of the ejb. The object will then need to 
   * be downcasted to the appropriate home interface.
   * @deprecated
   * @param serviceHome the ejb home object
   * @return the service remote interface
   * @throws BadServiceException if the service name is not registered with the naming server, or if it is null.
   **/
  public IService getService(IServiceHome serviceHome) throws BadServiceException{
    IService svc = null;
  /*
    if (serviceHome == null) {
      lm.error("ServiceLocator:: Service name cannot be null");
      throw new BadServiceException(1, new String[]{"Service Home cannot be null"});	
    }

    try {

      svc = (IService) serviceHome.create();
      
    }	catch (ClassCastException cce){
        lm.error("ServiceLocator:: ClassCastException");
        lm.error(cce);
        throw new BadServiceException(1, new String[]{"This problem is likely due to an incorrect JNDI Name"}, cce);
    } catch (CreateException ce){
        lm.error("ServiceLocator:: CreateException");
        lm.error(ce);
        throw new BadServiceException(1, new String[]{"Could not create the EJBObject"}, ce);
    }
    */
    return svc;
  }



}
