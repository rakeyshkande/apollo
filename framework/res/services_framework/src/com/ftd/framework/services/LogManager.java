// Unit Tested
package com.ftd.framework.services;
import com.ftd.framework.ErrorCodeStore;

import java.net.URL;

import org.apache.log4j.Priority;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This class acts as a facade to Log4J
 * 
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class LogManager  {
	protected String categoryName;
	protected transient org.apache.log4j.Logger logger;
  protected transient ResourceManager rm = ResourceManager.getInstance();
	protected static ErrorCodeStore ERRORCODESTORE;
	private static boolean SUCCESS;

	/**
	 * LogManager constructor.
	 * Creates a system dependent file path for FTDLog4J.properties file.
	 * This file should not be bundled in any 'jar' files in the application class path.
	 * It should be set at the root (ear) level in order to create a system dependent file path.
	 * 
	 * @param myCategoryName the category name
   * @throws java.lang.IllegalArgumentException if the passed categoryName is null.
	 */
	public LogManager(String myCategoryName)  {
		super();
    ERRORCODESTORE = ErrorCodeStore.getInstance();
    init(myCategoryName);
	}

  /**
   * Initialize the settings
   **/ 
  public synchronized void init(String myCategoryName){
    // Load the properties file once for the entire class
		if (!SUCCESS){
			String propFile = null;			
			try { 
				propFile = rm.getProperty("ftd.framework.configuration", "application.log4j.config");
        
        Class class_v = this.getClass();
        ClassLoader classLoader = class_v.getClassLoader();
        URL url = classLoader.getResource(propFile);
        
				System.out.println( "LogManager Configuration File URL: " + url.toString() );
        if (propFile.endsWith("xml"))  {
          //DOMConfigurator.configureAndWatch(systemDependentPath_sb.toString());
          DOMConfigurator.configureAndWatch(url.getFile());
        } else {
          //PropertyConfigurator.configureAndWatch(systemDependentPath_sb.toString());        
          PropertyConfigurator.configureAndWatch(url.getFile());
        }
				SUCCESS = true;
				
			} catch (Exception e){
					System.out.println("**************************");
					System.out.println("ERROR: UNABLE TO LOCATE THE CONFIGURATION FILE: " + propFile);
					System.out.println("**************************");
          e.printStackTrace();
      } finally{
				if (!SUCCESS){
					System.out.println("**************************");
					System.out.println("ERROR: UNABLE TO START FTDLOG4j CONFIGURATION MANAGER");
					System.out.println("USING PROPERTY FILE: "+ propFile);
					System.out.println("**************************");
				}			
			}
		}// end loading properties file for class
		
		if (myCategoryName == null) {
			System.out.println("**************************");
			System.out.println("ERROR: PASSED CATEGORY NAME IS NULL IN LOG MANAGER");
			System.out.println("**************************");
			
			throw new IllegalArgumentException("The category name passed to LogManger cannot be null");
		} else {
			categoryName = myCategoryName;

      logger = org.apache.log4j.LogManager.exists(myCategoryName);
      if (logger == null ){
				throw new IllegalArgumentException("The passed category name, "+myCategoryName+" is not valid");
			}
		}
  }

  
/******************************************************************************/
  
  /**
   * Log a message object with the DEBUG level
   *
   * @param message the message
   **/
	public void debug(String message) {
		logger.debug(message);
	}

  /**
   * Log a message object with the DEBUG level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void debug(Throwable t) {
		logger.debug(t.getMessage(), t);
	}

  /**
   * Log a message object with the DEBUG level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void debug(String message, Throwable t) {
		logger.debug(message, t);
	}

/******************************************************************************/

  /**
   * Log a message object with the ERROR level
   *
   * @param message the message
   **/
	public void error(String message) {
		logger.error(message);
	}	

  /**
   * Log a message object with the ERROR level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void error(Throwable t) {
		logger.error(t.getMessage(), t);
	}

  /**
   * Log a message object with the ERROR level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void error(String message, Throwable t) {
		logger.error(message, t);
	}


/******************************************************************************/

  /**
   * Log a message object with the FATAL level
   *
   * @param message the message
   **/
	public void fatal(String message) {
		logger.fatal(message);
	}

  /**
   * Log a message object with the FATAL level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void fatal(Throwable t) {
		logger.fatal(t.getMessage(), t);
	}
  /**
   * Log a message object with the FATAL level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void fatal(String message, Throwable t) {
		logger.fatal(message, t);
	}

/******************************************************************************/
  /**
   * Log a message object with the INFO level
   *
   * @param message the message
   **/
	public void info(String message) {
		logger.info(message);
	}

  /**
   * Log a message object with the INFO level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void info(Throwable t) {
		logger.warn(t.getMessage(), t);
	}
  /**
   * Log a message object with the INFO level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void info(String message, Throwable t) {
		logger.info(message, t);
	}

/******************************************************************************/

  /**
   * Log a message object with the WARN level
   *
   * @param message the message
   **/
	public void warn(String message) {
		logger.warn(message);
	}

  /**
   * Log a message object with the WARN level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void warn(Throwable t) {
		logger.warn(t.getMessage(), t);
	}

  /**
   * Log a message object with the WARN level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void warn(String message, Throwable t) {
		logger.warn(message, t);
	}
/******************************************************************************/
	
	/**
	 *	Indicates whether debug messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if debug messages are enabled
	 */
	public boolean isDebugEnabled(){
		boolean test = logger.isDebugEnabled();
		return test;
	}

	/**
	 *	Indicates whether info messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if info messages are enabled
	 */
	public boolean isInfoEnabled() {
		boolean test = logger.isInfoEnabled();
		return test;
	}
	
	/**
	 *	Indicates whether warn messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if warn messages are enabled
	 */
	public boolean isWarnEnabled() {
		return logger.isEnabledFor(Priority.WARN);
	}
	
	/**
	 *	Indicates whether error messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if error messages are enabled
	 */
	public boolean isErrorEnabled(){
		return logger.isEnabledFor(Priority.ERROR);
	}
	
	/**
	 *	Indicates whether fatal messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if fatal messages are enabled
	 */
	public boolean isFatalEnabled() {
		return logger.isEnabledFor(Priority.FATAL);
	}

	
	/**
	 * This method restores the state of the LogManager after serialization.
	 * It takes the categoryName that is part of the serialized LogManager
	 * and uses it to create a new LogManager
	 * 
	 * @return java.lang.Object the new Log Manager
	 * @throws ObjectStreamException if there is a problem with the deserialization
	 */
	private Object readResolve() throws java.io.ObjectStreamException  {
		String myCategoryName = categoryName;
        LogManager lm = null;
        lm = new LogManager(myCategoryName);	
        return lm;
	}


}
