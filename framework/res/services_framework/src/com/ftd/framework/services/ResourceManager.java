package com.ftd.framework.services;

import com.ftd.framework.FrameworkConstants;
import com.ftd.framework.exception.FTDApplicationException;
import com.ftd.framework.exception.RealmNotFoundException;
import com.ftd.framework.exception.ResourceNotFoundException;
import com.ftd.framework.utilities.ConfigurationObserver;
import com.ftd.framework.utilities.FileObservable;
import com.ftd.framework.utilities.Property;
import com.ftd.framework.utilities.Realm;
import com.ftd.framework.utilities.Realms;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.util.*;


 /**
  * Responsible for obtaining all resources external to the application.
  * 
  * @author Anshu Gaind
  * @version 1.0 
  **/
public class ResourceManager
{
  private HashMap realms = new HashMap();
  private String realmName;
  private long lastModified = 0L;
  private File realmFile;
  private FileObservable observable;
  
  private static ResourceManager RESOURCEMANAGER;
  private static boolean observersSet;
  
 /**
  * The private constructor
  *
  * @author Anshu Gaind
  **/
  private ResourceManager() {
    super();
  }

  /**
   * Initialize the Resource Manager
   */
   private void init(){
    try {
      loadRealms();
    } catch (Exception e){
      // cannot log this exception owing to circular dependency with log manager
      System.out.println(e.getMessage());
      System.out.println(this.getStackTrace(e));
    }

   }
 /**
  * The static initializer for the resource manager. It ensures that
  * at a given time there is only a single instance 
  *
  * @return the resource manager
  * @author Anshu Gaind
  **/
  public synchronized static ResourceManager getInstance() {
    if ( RESOURCEMANAGER == null ){
      RESOURCEMANAGER = new ResourceManager();
      RESOURCEMANAGER.init();
      return RESOURCEMANAGER;
    } else {
      return RESOURCEMANAGER;
    }
  }


  /**
   * Finds a resource with a given name. It returns a URL to the resource
   *
   * @param name the name of the resource
   * @return a URL to the resource
   * @throws ResourceNotFoundException
   * @author Anshu Gaind
   **/
  public URL getResource(String name) throws ResourceNotFoundException {
    Class class_v = this.getClass();
    ClassLoader classLoader = class_v.getClassLoader();

    URL url = classLoader.getResource(name);
    System.out.println("***************************************************");
    System.out.println("* Getting Resource:: " + url.toString());
    System.out.println("***************************************************");
    if (url == null){
      throw new ResourceNotFoundException(1);
    }
    return url;
  }

  /**
   * Finds a resource with a given name. It returns the resource as an 
   * InputStream
   *
   * @param name the name of the resource
   * @return the resource as a stream.
   * @throws ResourceNotFoundException
   * @author Anshu Gaind
   **/
  public InputStream getResourceAsStream(String name) throws ResourceNotFoundException{
    Class class_v = this.getClass();
    ClassLoader classLoader = class_v.getClassLoader();

    InputStream is = classLoader.getResourceAsStream(name);
    System.out.println("***************************************************");
    System.out.println("* Getting Resource:: " + name + " as Stream:: " + is.toString());
    System.out.println("***************************************************");
    if (is == null){
      throw new ResourceNotFoundException(1);
    }
    return is;
  }

  /**
   * Returns an implementation of the interface that is passed in as an argument.
   * The imterface name should be mapped to the implementation in the interface 
   * mapping file for the application. The implementation should have a no-args 
   * constructor.
   * 
   * If the implementation has been described as thread safe in the interface
   * mapping file, it will then be cached.
   *
   * @param interfaceName the name of the interface as set in interface mappings
   * @return the implementation of the interface
   * @throws ResourceNotFoundException
   **/
   public Object getImplementation(String interfaceName) throws ResourceNotFoundException {
      try{
        CacheManager cacheManager = CacheManager.getInstance();
        String implementationName = null;
        Class implementationClass = null;
        Object implementationObject = null;

        // get the associated property object
        Property property = getPropertyObject("ftd.application.interfacemapping"
                                                , interfaceName);

        if (property == null) 
        {
            return null;
        }
        
        // check to see if the implementation has been marked thread safe
        if ( property.getIsThreadSafe().equals("yes") )  {
            // get object from cache
            Object cachedObject = cacheManager.getCache(interfaceName);
            // if object is not in cache
            if (cachedObject == null)  {
              // get class name
              implementationName = property.getValue();
              // create new object
              if (implementationName != null){
                implementationClass = Class.forName(implementationName);
                implementationObject = implementationClass.newInstance();
                // store in cache  
                cacheManager.setCache(interfaceName, implementationObject);
              }
            } else {
              // return the cached object
              implementationObject = cachedObject;
            }
        } else {
            // get class name
            implementationName = property.getValue();
            // create new object
            if (implementationName != null){
              implementationClass = Class.forName(implementationName);
              implementationObject = implementationClass.newInstance();
            }
        }
        return implementationObject;
      } catch(ClassNotFoundException cnfe){
          System.out.println(this.getStackTrace(cnfe));
          throw new ResourceNotFoundException(1);
      } catch(IllegalAccessException iae){
          System.out.println(this.getStackTrace(iae));
          throw new ResourceNotFoundException(1);
      } catch (InstantiationException ie){
          System.out.println(this.getStackTrace(ie));
          throw new ResourceNotFoundException(1);
      }      
   }
  /**
   * Returns the value of a property from a properties file. 
   * Each property file is set up as a realm in the realm.properties
   * file. 
   *
   * @param realmName the realm name or the key name of the properties file as set up in realm.properties
   * @param propertyName the key within that realm, for which the value needs to be obtained.
   * @return the value of the specified key, and within a realm
   **/
  public String getProperty(String realmName, String propertyName){
    String rv = null;
    // returns a property object
    Property property = getPropertyObject(realmName, propertyName);
    rv = property.getValue();
    return rv;
  }



  /**
   * Returns the property from a properties file. 
   * Each property file is set up as a realm in the realm.properties
   * file. 
   *
   * @param realmName the realm name or the key name of the properties file as set up in realm.properties
   * @param propertyName the key within that realm, for which the value needs to be obtained.
   * @return the property object  of the specified key, and within a realm
   **/
  public Property getPropertyObject(String realmName, String propertyName){
    Property property = null;
    try { 
        // set observers
        if (!observersSet)  {
            setObservers();
        }                
        // check to see if the realm.xml has changed
        if (observable.hasChanged())  {
            observable.notifyObservers(this);
        }
        // returns a property object
        property = ((Realm)realms.get(realmName)).getProperty(propertyName);

    } catch (Exception e){
      // cannot log this exception owing to circular dependency with log manager
      System.out.println(e.getMessage());
      System.out.println(this.getStackTrace(e));
    }
    
    return property;
  }


  
  /**
   * Loads all the realm as indicated in the realm.properies file into 
   * an internal structure
 * @throws XPathExpressionException 
   *  
   **/
  private synchronized void loadRealms() 
            throws ResourceNotFoundException, FTDApplicationException, XPathExpressionException {	
      InputStream input = null;
      try {
        System.out.println("***************************************************");
        System.out.println("*              Loading Realms...                  *");
        System.out.println("***************************************************");

        DocumentBuilder realms_db = getDocumentBuilder(false, false, false,false, false, false);
        URL realms_url = this.getClass().getClassLoader().getResource(FrameworkConstants.APPLICATION_REALM_FILE_XML);
        File realmFile = new File(realms_url.getFile());
        if (! realmFile.exists()) 
        {
            throw new IOException("The configuration file " + realmFile.getAbsolutePath() +" was not found.");
        }
        
        Document realmsDoc = getDocument(realms_db, new FileInputStream(realms_url.getFile()));

        NodeList realms_nl = DOMUtil.selectNodes(realmsDoc,"/properties/property");
        Element realms_property = null;

        Realm realm = null;
        DocumentBuilder realm_db = null;
        Document realmDoc = null;
        NodeList realm_nl = null;
        Element realm_property = null;
        URL realm_url = null;

        // load properties for each realm
        for (int i = 0; i < realms_nl.getLength(); i++) 
        {
          realms_property = (Element) realms_nl.item(i);
          realm = new Realm(realms_property.getAttribute("name"));

          realm_db = getDocumentBuilder(false, false, false,false, false, false);
          realmDoc = DOMUtil.getDocument(realm_db, getResourceAsStream(realms_property.getAttribute("value")));
          realm_nl = DOMUtil.selectNodes(realmDoc,"/properties/property");

          // for each property within a realm
          for (int j = 0; j < realm_nl.getLength(); j++) 
          {
            realm_property = (Element) realm_nl.item(j);
            realm.setProperty(new Property(realm_property.getAttribute("name"), realm_property.getAttribute("value"), realm_property.getAttribute("isThreadSafe")));            
          }
          // add the realm to the collection
          realms.put(realm.getName(), realm);
        }        

      } catch (SAXException se){
        System.out.println(se);
        System.out.println(this.getStackTrace(se));
        String params[] = new String [1];
        params[0] = FrameworkConstants.APPLICATION_REALM_FILE_XML;
        throw new FTDApplicationException(8,params, se );
      } catch (ParserConfigurationException pce){
        System.out.println(pce);
        System.out.println(this.getStackTrace(pce));
        String params[] = new String [1];
        params[0] = FrameworkConstants.APPLICATION_REALM_FILE_XML;
        throw new FTDApplicationException(8,params, pce );
      } catch (IOException ioe){
        System.out.println(ioe);
        System.out.println(this.getStackTrace(ioe));
        throw new ResourceNotFoundException(1);
      } finally {
      }
  }  


   /**
    * Resets all the realms
    */
    public synchronized void resetRealms(){
      CacheManager cacheManager = CacheManager.getInstance();
      realms.clear();
      // reset the application cache
      cacheManager.invalidateCache();
      try {
        loadRealms();
      } catch (Exception e){
        // cannot log this exception owing to circular dependency with log manager
        System.out.println(e.getMessage());
        System.out.println(this.getStackTrace(e));
      }
    }

  /**
   * Sets observers on observable files
   */
  private void setObservers(){
    try  {
      URL url = this.getResource(FrameworkConstants.APPLICATION_REALM_FILE_XML);
      realmFile = new File(url.getPath());
      if (! realmFile.exists()) 
      {
          throw new IOException("The configuration file " + realmFile.getAbsolutePath() +" was not found.");
      }
      
      observable = new FileObservable();
      observable.setObservable(realmFile);
      observable.addObserver(new ConfigurationObserver());
      observersSet = true;      
    } catch (Exception e)  {
        // cannot log this exception owing to circular dependency with log manager
        System.out.println(e.getMessage());
        System.out.println(this.getStackTrace(e));
    } finally  {
    }    
  }

    /**
     * Returns a document builder based on the parameters specified
     * 
     * @param coalescing Specifies that the parser produced by this code will convert CDATA nodes to Text nodes and append it to the adjacent (if any) text node.
     * @param expandEntityReferences Specifies that the parser produced by this code will expand entity reference nodes.
     * @param ignoringComments Specifies that the parser produced by this code will ignore comments.
     * @param ignoringElementContentWhitespace Specifies that the parsers created by this factory must eliminate whitespace in element content (sometimes known loosely as 'ignorable whitespace') when parsing XML documents (see XML Rec 2.10).
     * @param namespaceAware Specifies that the parser produced by this code will provide support for XML namespaces.
     * @param validating Specifies that the parser produced by this code will validate documents as they are parsed.
     * @exception ParserConfigurationException 
     * @return the document builder
     */
    public static DocumentBuilder getDocumentBuilder(boolean coalescing,
                                                     boolean expandEntityReferences,
                                                     boolean ignoringComments,
                                                     boolean ignoringElementContentWhitespace, 
                                                     boolean namespaceAware, 
                                                     boolean validating) 
        throws ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(coalescing);
        factory.setExpandEntityReferences(expandEntityReferences);
        factory.setIgnoringComments(ignoringComments);
        factory.setIgnoringElementContentWhitespace(ignoringElementContentWhitespace);
        factory.setNamespaceAware(namespaceAware);
        factory.setValidating(validating);

        return factory.newDocumentBuilder();    
    }

    /**
     * Returns a DOM document for the input stream using the specified document builder.
     * 
     * @param docBuilder the document builder
     * @param is the input stream
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the input stream.
     */
    public static Document getDocument(DocumentBuilder docBuilder, InputStream is) throws IOException, SAXException, ParserConfigurationException
    {
        Document doc = null;
        BufferedInputStream bis = null;
        
        try  
        {
            bis = new BufferedInputStream(is);
            InputSource inputSource = new InputSource(is);      
            doc = (Document) docBuilder.parse(inputSource);      
        } 
        finally  
        {
            if (bis != null)  
            {
                bis.close();
            }      
        }
        
        return doc;            
    }
  
  /**
   * Returns a stack trace as a string
   * @param t
   * @return 
   */
    public String getStackTrace(Throwable t)
    {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw, true);
      t.printStackTrace(pw);
      String log_message = sw.toString();
      pw.close();        
      return log_message;
    }
}
