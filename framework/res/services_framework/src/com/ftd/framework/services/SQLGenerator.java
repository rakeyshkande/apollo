package com.ftd.framework.services;
import com.ftd.framework.FrameworkConstants;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class SQLGenerator  {
  private static SQLGenerator SQLGENERATOR;
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager("com.ftd.framework.services.SQLGenerator");
 /**
  * The static initializer for the SQL Generator. It ensures that
  * at a given time there is only a single instance 
  *
  * @return the SQL Generator
  * @author Anshu Gaind
  * @deprecated
  **/
  public static SQLGenerator getInstance()  
  {
    if ( SQLGENERATOR == null )
    {
      SQLGENERATOR = new SQLGenerator();
      return SQLGENERATOR;
    }
    else
    {
      return SQLGENERATOR;
    }
  }

 /**
  * The private constructor
  *
  * @author Anshu Gaind
  **/
  private SQLGenerator() {
    super();
  }

  /**
   * Parses the SQL string containing place holders, replaces them with the 
   * specified parameters, and in the same order. It returns an empty string
   * if the input sql string is null or is empty. It returns the unprocessed
   * sql string if the input parameters are empty, null, or don't match the
   * number of place holders in the sql string.
   * Usually works with the Statement Object.
   *
   * @param sqlString the sql string with place holders
   * @param params the list of parameters that will replace the place holders, in the same order
   * @return the complete sql string
   **/    
  public String generateSQL(String sqlString, ArrayList params){
    // perform checks on the input parameters
    if (sqlString == null || sqlString == ""){
      return "";
    }
    if (params == null){
      return sqlString;
    } else if (params.size() == 0){
      return sqlString;
    }
    StringTokenizer st = new StringTokenizer(sqlString, 
                            FrameworkConstants.SQLSTRING_PLACEHOLDER_CONSTANT);


    
    StringBuffer tempSQLString = new StringBuffer();

    for( int paramCounter = 0;(st.hasMoreTokens());paramCounter ++){
      tempSQLString.append(st.nextToken());
      if (paramCounter < params.size()){
        tempSQLString.append(params.get(paramCounter));
      }
    }
    String rv = tempSQLString.toString();
    lm.debug(rv);
    return rv;
  }

  /**
   * Converts any string that contains the "<b>'</b>" character to a sql safe
   * format. It assigns an escape character around "<b>'</b>". So a string like 
   * "Arby's" will be converted to "Arby''s". It is a good practice to convert
   * all non-numeric data to sql safe format. If the imput string does not contain
   * the special character it will be returned as is, with minimal performance impact.
   * It returns an empty string if the input string is null or empty.
   *
   * @param sqlUnsafeString a string containing the "<b> ' </b>" character
   * @return the converted string
   **/
  public String convertToSQLSafeString(String sqlUnsafeString){
    // perform checks on the input parameter
    if (sqlUnsafeString == null || sqlUnsafeString == ""){
      return "";
    }
    String specialCharString = "'";
    if (sqlUnsafeString.indexOf(specialCharString) != -1 ) {
      String escapeCharString = "''";
      StringTokenizer st = new StringTokenizer(sqlUnsafeString, specialCharString);
      StringBuffer sqlSafeString = new StringBuffer();

      while(st.hasMoreTokens()){
        sqlSafeString.append(st.nextToken());
        if (st.hasMoreTokens()){
          sqlSafeString.append(escapeCharString);
        }
      }
      return sqlSafeString.toString();
    } else {
      return sqlUnsafeString;
    }
  }
}