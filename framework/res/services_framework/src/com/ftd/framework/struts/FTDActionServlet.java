package com.ftd.framework.struts;
import com.ftd.framework.UserProfile;
import com.ftd.framework.services.LogManager;
import com.ftd.framework.services.ResourceManager;

import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.MessageResourcesFactory;

/**
 * The main servlet that handles all user requests.
 *
 * @author Anshu Gaind
 * @version 1.0
 **/

public class FTDActionServlet extends ActionServlet {
  private ResourceManager rm = ResourceManager.getInstance();
  private LogManager lm = new LogManager("com.ftd.framework.struts.FTDActionServlet");
  private ServletConfig config;
  
  /**
   * The default constructor
   **/
   public FTDActionServlet()   {
    super();
   }

  /**
   * Called by the servlet container to indicate to a servlet that the servlet
   * is being placed into service.
   *
   * @throws ServletException - if an exception occurs that interrupts the servlet's normal operation
   **/
  public void init(ServletConfig config) throws ServletException
  {
      //super.init();
      this.config = config;
      lm.debug("Begin initializing FTDActionServlet");
      this.initApplication(config);
      lm.debug("Created FTDActionServlet");
   }


  /**
   * Creates the representation of the user as a user profile object.
   *
   * @param request the request object
   **/
   private void createUserProfile(HttpServletRequest request){
      HttpSession session = request.getSession(true);
      UserProfile userProfile = (UserProfile)session.getAttribute(rm.getProperty("ftd.framework.configuration",
                                                                      "framework.user.profile.name"));

      if (userProfile == null){
        // create a user profile
        userProfile = new UserProfile();
        //put User Profile back in session.
        session.setAttribute(rm.getProperty("ftd.framework.configuration", "framework.user.profile.name")
                             , userProfile);
        lm.debug("Created User Profile");
      } else {
        lm.debug("Using Existing User Profile");
      }
   }

  /**
   * Processes requests used with the Post command.
   *
   * @param request javax.servlet.http.HttpServletRequest
   * @param response javax.servlet.http.HttpServletResponse
   * @throws javax.servlet.ServletException The exception description.
   * @throws java.io.IOException The exception description.
   */
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    lm.debug("Begin doGet");
    // make sure that a user profile is in session.
    createUserProfile(request);
    //process request
    super.doGet(request, response);
    lm.debug("End doGet");
  }


  /**
   * Processes requests used with the Post command.
   *
   * @param request javax.servlet.http.HttpServletRequest
   * @param response javax.servlet.http.HttpServletResponse
   * @throws javax.servlet.ServletException The exception description.
   * @throws java.io.IOException The exception description.
   */
  public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
    lm.debug("Begin doPost");
    // make sure that a user profile is in session.
    createUserProfile(request);
    //process request
    super.doPost(request, response);
    lm.debug("End doPost");
  }

    /**
     * Responsible for loading other properties file that can be referenced 
     * within Struts as resource bundles
     * 
     * @throws ServletException
     */
    protected void initApplication(ServletConfig config) throws ServletException{
        //call ActionServlet's initApplication method.
        lm.debug("Calling ActionServlet's initApplication method");
        System.out.println("Calling ActionServlet's initApplication method");
        super.init(config);
        lm.debug("Begin initApplication");
        System.out.println("Begin initApplication");
        ServletConfig servletConfig = getServletConfig();
        Enumeration configNames = servletConfig.getInitParameterNames();
        String currConfigName = null, currConfigValue = null;
        
        while (configNames.hasMoreElements()){
            //get Config name
            currConfigName = (String) configNames.nextElement();
            // if the config name ends with "Messages" 
            // read the value and attempt to create a ResourceBundle
            if (currConfigName != null && currConfigName.endsWith("Messages")){
                currConfigValue = servletConfig.getInitParameter(currConfigName);
                //make sure that the name returned a value
                if (currConfigValue != null){
                    lm.debug(internal.getMessage("applicationLoading", currConfigName));	
                    System.out.println(internal.getMessage("applicationLoading", currConfigName));
                    MessageResources resources = null;
                    //ResourceBundle resourceBundle = ResourceBundle.getBundle(currConfigValue);
                    try{
                         //getServletContext().setAttribute(currConfigName, resourceBundle);
                         String oldFactory = MessageResourcesFactory.getFactoryClass();
                         //set factory class (from init param)
                         String factory = servletConfig.getInitParameter("factory");
                         if (factory != null){
                            MessageResourcesFactory.setFactoryClass(factory);
                         }
                         MessageResourcesFactory factoryObject = MessageResourcesFactory.createFactory();
                         resources = factoryObject.createResources(currConfigValue);
                         //reset the default factory class
                         MessageResourcesFactory.setFactoryClass(oldFactory);
                         //add the message bundle to the ServletContext
                         getServletContext().setAttribute(currConfigName, resources);                        
                         lm.debug("Loaded into ServletContext: key = " +currConfigName+" :: bundle = " + currConfigValue);
                         System.out.println("Loaded into ServletContext: key = " +currConfigName+" :: bundle = " + currConfigValue);
                    }catch (Throwable e){
                         lm.error("Unable to load messages for key: "+currConfigName+" in FTDActionServlet");
                         System.out.println("Unable to load messages for key: "+currConfigName+" in FTDActionServlet");
                         lm.error(e);
                    }
                }
            }
        }
    }
  /**
  * Overrides the log method provided by GenericServlet so that it uses
  * LogManager instead of System.out.println
  * 
  * @param message the message string that will be logged
  */
  public void log(String message){
    lm.debug(message);
  }

}
