package com.ftd.framework.struts;

import com.ftd.framework.services.LogManager;

import org.apache.struts.action.ActionForm;
/**
 * The base class for all ActionForm classes
 *
 * @author Anshu Gaind
 * @version 1.0
 **/
public class FTDActionForm extends ActionForm  {
  private LogManager lm;
  /**
   * The default constructor
   *
   * @param loggerCategory the logger category
   **/
   public FTDActionForm(String loggerCategory){
    super();
    lm = new LogManager(loggerCategory);
   }

  /**
   * Returns the log manager
   * 
   * @return the log manager
   **/
  public LogManager getLogManager(){
    return lm;
  }

}
