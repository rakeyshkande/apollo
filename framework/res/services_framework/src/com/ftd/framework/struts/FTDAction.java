package com.ftd.framework.struts;

import com.ftd.framework.UserProfile;
import com.ftd.framework.exception.FTDApplicationException;
import com.ftd.framework.exception.FTDEJBException;
import com.ftd.framework.exception.FTDSystemException;
import com.ftd.framework.services.LogManager;
import com.ftd.framework.services.ResourceManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
/**
 * The base class for all action classes. It extends the Struts Action class.
 *
 * @author Anshu Gaind
 * @version 1.0
 **/
public class FTDAction  extends Action{
  private LogManager lm; 
  private ResourceManager rm = ResourceManager.getInstance();
  /**
   * The default constructor.
   * 
   * @param loggerCategory logger category
   **/
  public FTDAction(String loggerCategory) {
    super();
    lm = new LogManager(loggerCategory);
  }

  /**
   * Returns the log manager
   * 
   * @return the log manager
   **/
  public LogManager getLogManager(){
    return lm;
  }

  /**
   * Returns a user profile object from session
   *
   * @param request the request object
   * @return the user profile
   * @throws FTDApplicationException
   **/
  protected UserProfile getUserProfile(HttpServletRequest request) throws FTDApplicationException{
    UserProfile userProfile = null;
    //make sure request is not null
    if (request == null) {
      FTDApplicationException e = new FTDApplicationException(4);
      lm.error(e);
      throw e;
    }
    //get Session from HttpServletRequest
    HttpSession session = request.getSession(false);

    //make sure the session is not null
    if (session == null) {
      FTDApplicationException fae = new FTDApplicationException(4);
      lm.error(fae);
      throw fae;
    } else {
      //get User Profile from Session
      userProfile = (UserProfile) session.getAttribute(rm.getProperty("ftd.framework.configuration",
                                                                      "framework.user.profile.name"));
		
      if (userProfile == null) {
        FTDApplicationException fae = new FTDApplicationException(4);
        lm.error(fae);
        throw fae;
      }
    }
    return userProfile;   
  }


  /**
   * Handles exceptions that come from the application. 
   * Retrieves the error code from the exception, creates an Action Error
   * object, and adds it to the request. It then returns the appropriate action
   * forward. 
   * It also set the bundle that can be printed on a default error page. JSPs that
   * handle ActionError objects can get the bundle by calling request.getAttribute(MESSAGE_BUNDLE)
   *
   * @param request the request object
   * @param mapping the action mapping
   * @param exception the exception
   * @param bundle the bundle used for displaying error messages If bundle is null, the default bundle will be used.
   * @return ActionForward determines what the next page will be
   **/
  protected ActionForward handleException(HttpServletRequest request, 
                                          ActionMapping mapping, 
                                          Exception exception, 
                                          String bundle){

      ActionErrors errors = new ActionErrors();
      //set the bundle name in the request.
      //saveBundleName(request, bundle);
      //unwrap the exception
      exception = unwrapException(exception);
      //put the exception into the request object
      if (exception != null){
        request.setAttribute(rm.getProperty("ftd.framework.configuration", "framework.exception.object.name")
                             , exception);
      }

      //handle the exception
      if (exception == null) {
        lm.debug("Exception is not defined in handleException!");
        errors.add(rm.getProperty("ftd.framework.configuration", "framework.system.category.name"),
                    new ActionError("error.000", "The exception in handleException() was null!"));
        saveErrors(request, errors);
        return mapping.findForward(rm.getProperty("ftd.framework.configuration", "framework.system.category.name"));
      } else if (exception instanceof FTDApplicationException) {
          FTDApplicationException appEx = (FTDApplicationException) exception;
          //handle the exception
          return handleApplicationException(request, mapping, errors, appEx);
      } else if (exception instanceof FTDSystemException) {
          FTDSystemException sysEx = (FTDSystemException) exception;
          //handle the exception
          return handleSystemException(request, mapping, errors, sysEx);
      } else {
          //handle the exception
          return handleUnexpectedException(request, mapping, errors, exception);
      }
      
  }

  /**
   * Handles all application exceptions that come from the application. 
   *
   * @param request the request object
   * @param mapping the action mapping
   * @param errors the action errors object
   * @param appEx the application exception
   * @return ActionForward determines what the next page will be
   **/
  protected ActionForward handleApplicationException(HttpServletRequest request,
                                                      ActionMapping mapping, 
                                                      ActionErrors errors, 
                                                      FTDApplicationException appEx){
      //make sure passed variables are all valid
      if (request == null || mapping == null || errors == null || appEx == null){

        IllegalArgumentException e = new IllegalArgumentException("Invalid passed arguments in handleApplicationException(): request, mapping, errors, or appEx is null");
        lm.error(e);
        throw e;
      }

      //get exception's arguments
      String[] argList = appEx.getArgList();

      //create the ActionError with the errorCode (used for the error message.)
      //call the ActionError constructor that corresponds to the number of exception args
      if (argList == null || argList.length == 0){
        errors.add(rm.getProperty("ftd.framework.configuration", "framework.application.category.name"), new ActionError(String.valueOf(appEx.getErrorCode())));
      } else if (argList.length == 1) {
        errors.add(rm.getProperty("ftd.framework.configuration", "framework.application.category.name"), new ActionError(String.valueOf(appEx.getErrorCode()), argList[0]));
      } else if (argList.length == 2) {
        errors.add(rm.getProperty("ftd.framework.configuration", "framework.application.category.name"), new ActionError(String.valueOf(appEx.getErrorCode()), argList[0], argList[1]));
      } else if (argList.length == 3) {
        errors.add(rm.getProperty("ftd.framework.configuration", "framework.application.category.name"), new ActionError(String.valueOf(appEx.getErrorCode()), argList[0], argList[1], argList[2]));
      } else{
        errors.add(rm.getProperty("ftd.framework.configuration", "framework.application.category.name"), new ActionError(String.valueOf(appEx.getErrorCode()), argList[0], argList[1], argList[2], argList[3]));
      }

      //put the errors into the request
      saveErrors(request, errors);

      //go to the application error page.
      return mapping.findForward(rm.getProperty("ftd.framework.configuration", "framework.application.error.name"));                                                        
  }                                                      


  /**
   * Handles all system exceptions that come from the application. 
   *
   * @param request the request object
   * @param mapping the action mapping
   * @param errors the action errors object
   * @param sysEx the system exception
   * @return ActionForward determines what the next page will be
   **/
  protected ActionForward handleSystemException(HttpServletRequest request, 
                                                ActionMapping mapping, 
                                                ActionErrors errors, 
                                                FTDSystemException sysEx) {
    //make sure passed vars are all valid
    if (request == null || mapping == null || errors == null || sysEx == null){
      IllegalArgumentException e = new IllegalArgumentException("Invalid passed arguments: request, mapping, errors, or appEx is null");
      lm.error(e);
      throw e;
    }

    //create the ActionError with the errorCode (used for the error message.)
    errors.add(rm.getProperty("ftd.framework.configuration", "framework.system.category.name"), new ActionError( String.valueOf( sysEx.getErrorCode() )) );

    //put the errors into the request
    saveErrors(request, errors);

    //go to the application error page.
    return mapping.findForward(rm.getProperty("ftd.framework.configuration", "framework.system.error.name"));
  }

  /**
   * Handles all system exceptions that come from the application. 
   *
   * @param request the request object
   * @param mapping the action mapping
   * @param errors the action errors object
   * @param exception the exception
   * @return ActionForward determines what the next page will be
   **/
  protected ActionForward handleUnexpectedException(HttpServletRequest request, 
                                                        ActionMapping mapping, 
                                                        ActionErrors errors, 
                                                        Exception exception) {
    //make sure passed vars are all valid
    if (request == null || mapping == null || errors == null) {
      IllegalArgumentException e = new IllegalArgumentException("Invalid passed arguments: request, mapping, or errors is null");
      lm.error(e);
      throw e;
    }
      //create the ActionError with the errorCode (used for the error message.)
      errors.add(rm.getProperty("ftd.framework.configuration", "framework.system.category.name"), new ActionError("error.000", exception.getClass().toString(), exception.getMessage()));

      //put the errors into the request
      saveErrors(request, errors);

      //go to the system error page.
      return mapping.findForward(rm.getProperty("ftd.framework.configuration", "framework.system.error.name"));
  }

  /**
   * Unwraps the exception.  This method attempts to get the nested exception
   * out of wrapper exceptions.
   *
   * @param exception the exception object
   * @return the unwrapped exceptions
   */
  protected Exception unwrapException(Exception exception)
  {
    Exception originalEx = exception;

    if (exception instanceof java.rmi.ServerException){
      lm.debug("Got a ServerException");
      java.rmi.ServerException serverEx = (java.rmi.ServerException) exception;

      if (serverEx.detail == null){
        lm.debug("Nested exception is null.  Returning original exception");
        return originalEx;
      } else if (serverEx.detail instanceof java.rmi.RemoteException){
        lm.debug("Got another RemoteException");
        java.rmi.RemoteException remoteEx = (java.rmi.RemoteException) serverEx.detail;

        if (remoteEx.detail == null) {
          lm.debug("RemoteException detail was null.  Returning originalException");
          return originalEx;
        } else {
          if (remoteEx.detail instanceof Exception) {
            exception = (Exception)remoteEx.detail;
          }
        }
      }
    }


    //if the exception is an EJBException, unpack the nested exception.
    if (exception instanceof FTDEJBException) {
      lm.debug("Got a FTDEJBException");
      FTDEJBException tempEx = (FTDEJBException) exception;

      if (tempEx != null){
        exception = tempEx.getNestedException();
      }
    }

    return exception;
  }

  /**
   * Sets the bundle name that will be used to print Error Codes from a messages file.
   *
   * @param request the request object
   * @param bundle the message bundle
   */
  protected void saveBundleName(HttpServletRequest request, String bundle){
    //set the bundle name in the request.
    if (bundle != null){
      request.setAttribute(rm.getProperty("ftd.framework.configuration", "framework.message.bundle.name"), bundle);
    }
  }
   
}
