package com.ftd.framework.exception;

public class RealmNotFoundException extends FTDApplicationException  {

/**
 * Used when the exception does not need a custom message with it.
 *
 * @param errorCode the error code
 * @author Anshu Gaind
 */
  public RealmNotFoundException(int errorCode){
    super(errorCode);
  }

/**
 * Used when the error code has an associated error message.
 *
 * @param errorCode the error code
 * @param args[] the arguments that go in the custom error message
 * @author Anshu Gaind
 **/
  public RealmNotFoundException(int errorCode, String[] args){
    super(errorCode, args);
  }

/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 **/
  public RealmNotFoundException(int errorCode, Throwable nestedThrowable){
    super(errorCode, nestedThrowable);
  }


/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param args[] the arguments that go in the custom error message
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 **/
  public RealmNotFoundException(int errorCode, String[] args, Throwable nestedThrowable){
    super(errorCode, args, nestedThrowable);
  }



}