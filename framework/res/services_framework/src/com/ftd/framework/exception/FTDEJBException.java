package com.ftd.framework.exception;

import javax.ejb.EJBException;
/**
 * The base class for all EJBExceptions.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class FTDEJBException extends EJBException implements java.io.Serializable{

	private Exception nestedException;
	private java.lang.String stackTraceString ="";

  /**
   * The default constructor
   **/
	public FTDEJBException() {
		super();
	}

  /**
   * This constructor wraps another exception
   *
   * @param exp exception
   **/
	public FTDEJBException(Exception exp) {
		super();
		setNestedException(exp);
	}
	/**
	 * This is overridden equals function of Object class
	 * 
	 * @param obj java.lang.Object
	 */
	public boolean equals(Object obj) {
		boolean retVal=false;
		if(obj instanceof FTDEJBException)
		{
			FTDEJBException copy= (FTDEJBException) obj;
			if((copy.nestedException != null) && (nestedException != null))
			{
				if((copy.nestedException instanceof FTDException) && (nestedException instanceof FTDException)) {
					if(copy.nestedException.equals(nestedException))
						retVal=true;
				}
				else {
					if(copy.nestedException == nestedException)
						retVal=true;
				}
			}
		}
		return retVal;
	}

  /**
   * Returns the error code
   *
   * @return the error code associated with the exception
   **/
	public int getErrorCode() {
		if(nestedException == null)
			return 0;
		if(nestedException instanceof FTDException) {
			FTDException tmp= (FTDException) nestedException;
			return tmp.getErrorCode();
		}
		return 0;
	}

  /**
   * Returns the message string
   *
   * @return the message string associated with the exception
   **/
	public String getMessage() {
		
		String errorMsg="";
		StringBuffer msg=null;
		
		if(nestedException != null)
			errorMsg=nestedException.getMessage();
		else
			errorMsg=super.getMessage();
		if(stackTraceString != null) {
			msg=new StringBuffer(errorMsg);
			msg.append("|nested Exception Stack Trace: [");
			msg.append(stackTraceString);
			msg.append("]");
		}
		if(msg != null)
			return msg.toString();	
		return errorMsg;
	}

  /**
   * Returns the nested exception
   *
   * @return the nested exception
   **/
  public Exception getNestedException() {
    return nestedException;
  }

  /**
   * Returns the stack trace
   *
   * @return the stack trace string
   **/
	public String getStackTraceString() {
		return stackTraceString;
	}

  /**
   * Sets a nested exception
   *
   * @param exp the nested exception
   **/
	public void setNestedException(Exception exp) {
		nestedException=exp;
	}

  /**
   * Overridden method from the Object class
   *
   * @return the string representation of the object
   **/
	public String toString() {
		String msg="";
		if(nestedException != null) 
			msg="[FTDEJBException[nestedException: " + nestedException.toString() + "]]";
		else
			msg=super.toString();	
		return msg;	
	}

}
