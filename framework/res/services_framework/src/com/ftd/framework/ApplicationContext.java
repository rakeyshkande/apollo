 // Unit Tested
package com.ftd.framework;

import java.util.HashMap;
import java.util.Iterator;

/**
 * The context of the application. It can be used to store objects
 * that are common across multiple requests. The application context can
 * be reset externally so the application code should refresh the application
 * context if the desired value is not found.
 * @deprecated
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class ApplicationContext 
{
  private HashMap context = new HashMap();
  private static ApplicationContext APPLICATIONCONTEXT;

 /**
  * The private constructor
  *
  * @author Anshu Gaind   
  **/
  private ApplicationContext()
  {
  }
  
 /**
  * The static initializer for the application context. It ensures that
  * at a given time there is only a single instance of the application 
  * context.
  * @deprecated
  * @return the application context
  * @author Anshu Gaind
  **/
  public static ApplicationContext getInstance()
  {
    if ( APPLICATIONCONTEXT == null )
    {
      APPLICATIONCONTEXT = new ApplicationContext();
      return APPLICATIONCONTEXT;
    }
    else
    {
      return APPLICATIONCONTEXT;
    }
  }

  /**
   * Returns the attribute with the given name, or null if there is
   * no attribute by that name.
   * @deprecated
   * @param name the name of the attribute
   * @return an object containing the value of the attribute
   * @author Anshu Gaind
   **/
  public Object getAttribute(String name)
  {
    return context.get(name);
  }

  /**
   * Set the attribute with the given name in the application context.
   * @deprecated
   * @param name the name of the attribute
   * @param object the object corresponding to the attribute name
   * @author Anshu Gaind
   **/
  public synchronized void setAttribute(String name, Object object)
  {
    context.put(name, object);
  }

  /**
   * Removes the specified attribute from the application context.
   * @deprecated
   * @param name the name of the attribute
   * @author Anshu Gaind
   **/
  public synchronized void removeAttribute(String name)
  {
    context.remove(name);
  }

  /**
   * Returns an iterator of all attributes in the application context.
   * @deprecated
   * @return an iterator of all attributes
   * @author Anshu Gaind
   **/
  public Iterator getAttributeNames()
  {
    return context.keySet().iterator();
  }

  /**
   * Resets the Application Context
   * @deprecated
   **/
   public synchronized void resetApplicationContext()
   {
     context.clear();   
   }

}
