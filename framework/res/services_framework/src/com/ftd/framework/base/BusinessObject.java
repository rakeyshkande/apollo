package com.ftd.framework.base;
import java.io.Serializable;
import com.ftd.framework.services.*;

/**
 * All business objects will extend this base class.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class BusinessObject  implements Serializable{

  private LogManager lm;
  /**
   * The constructor
   *
   * @param loggerCategory the logger category
   **/
   public BusinessObject(String loggerCategory) {
      lm = new LogManager(loggerCategory);
   }

  /**
   * Returns the log manager
   * 
   * @return the log manager
   **/
  public LogManager getLogManager(){
    return lm;
  }

  /**
   * Responsible for creating  the business object
   **/
  public abstract void create();

  /**
   * Responsible for saving the business object
   **/
  public abstract void save();

  /**
   * Indicates whether some other object is "equal to" this one.
   * The subclass should ensure that the argument is an instance of LWBO
   *
   * @param obj the object that will be compared
   * @return boolean
   **/
  public abstract boolean equals(Object obj);

  /**
   * Returns a string representation of the object.
   * The default implementation will be comma separated name value pairs.
   *
   * @return a string representation of the object
   **/
  public abstract String toString();

}
