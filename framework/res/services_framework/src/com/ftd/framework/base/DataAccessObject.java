package com.ftd.framework.base;
import com.ftd.framework.services.*;

/**
 * DAOs are reponsible for interacting with the database.
 * All data access objects will extend this base class.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class DataAccessObject {
   private LogManager lm;
  /**
   * The constructor
   *
   * @param loggerCategory the logger category
   **/
   public DataAccessObject(String loggerCategory) {
      lm = new LogManager(loggerCategory);
   }

  /**
   * Returns the log manager
   * 
   * @return the log manager
   **/
   public LogManager getLogManager(){
    return lm;
   }

}
