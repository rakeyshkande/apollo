package com.ftd.framework.base;

import java.io.Serializable;

/**
 * LWBOs are composite attributes on a business object. An example of a 
 * composite attribute is the address object that contains street, city, zip etc.
 * All LWBOs are associated with a BO, and it is the responsibility of the BO to
 * persist LWBOs.
 * All light weight business objects will implement this base class.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class LightWeightBusinessObject implements Serializable {

  /**
   * Indicates whether some other object is "equal to" this one.
   * The subclass should ensure that the argument is an instance of LWBO
   *
   * @param obj the object that will be compared
   * @return boolean
   **/
  public abstract boolean equals(Object obj);

  /**
   * Returns a string representation of the object.
   * The default implementation will be comma separated name value pairs.
   *
   * @return a string representation of the object
   **/
  public abstract String toString();
}
