package com.ftd.framework.base;
import java.io.Serializable;

/**
 * The base class for all entity bean id objects.
 * ID objects are representations of the entity bean primary key.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class EntityBeanID implements Serializable {
}
