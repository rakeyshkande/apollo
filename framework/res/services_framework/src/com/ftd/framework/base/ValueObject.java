package com.ftd.framework.base;

import java.io.Serializable;

/**
 * Value objects are data containers that can be passed to the service layer, 
 * and are returned from the service layer. All value objects will extend this base class.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class ValueObject implements Serializable {
}
