package com.ftd.framework.base;

import com.ftd.framework.exception.BadServiceException;
import com.ftd.framework.services.*;
/**
 * The base class for all the delegates to the service layer.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class ServiceDelegate {
  private LogManager lm;
  
  /**
   * The constructor
   *
   * @ param loggerCategory logger category
   **/
  public ServiceDelegate(String loggerCategory) throws BadServiceException {
      lm = new LogManager(loggerCategory);
  }

  /**
   * Returns the log manager
   * 
   * @return the log manager
   **/
  public LogManager getLogManager(){
    return lm;
  }


}
