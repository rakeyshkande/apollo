package com.ftd.framework.interfaces;

/**
 * The base interface for all home methods of an EJB
 * Every EJB home interface will extend this interface. 
 * 
 * @author Anshu Gaind
 * @version 1.0 
 **/ 
public interface IServiceHome {

}
