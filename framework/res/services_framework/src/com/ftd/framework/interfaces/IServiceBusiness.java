package com.ftd.framework.interfaces;

/**
 * The nature of EJB development is such that the bean implementation does not
 * directly implement its remote interface. If all business methods are included
 * in the remote interface then at compile time it is difficult to ensure that 
 * all those have been implemented. 
 * The solution to that problem is to use a business interface, that 
 * contains all the business methods. The EJB remote interface then extends this
 * interface, and the bean implementation implements this interface. This arrangement
 * ensures that all business methods are implemented and it can be verified at 
 * compile time.
 * 
 * @author Anshu Gaind
 * @version 1.0 
 **/ 
 public interface IServiceBusiness {
 }
