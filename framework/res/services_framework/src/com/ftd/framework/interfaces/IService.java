package com.ftd.framework.interfaces;

/**
 * The base interface for all remote methods of an EJB.
 * Every EJB remote interface will extend this interface.
 * 
 * @author Anshu Gaind
 * @version 1.0 
 **/ 
public interface IService  {

}
