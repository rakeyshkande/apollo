package com.ftd.framework.interfaces;

/**
 * In a distributed development environment developers should code against an interface and 
 * not an implementation. Therefore all data access objects will require to 
 * publish an interface that contains all public methods.
 * This is the the base interface for all interfaces of data access objects. 
 *  
 * @author Anshu Gaind
 * @version 1.0
 **/
public interface IDataAccessObject  {
}
