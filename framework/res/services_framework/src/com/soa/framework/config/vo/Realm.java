package com.soa.framework.config.vo;
import java.util.*;
import com.soa.framework.config.vo.Property;
import com.soa.framework.interfaces.IValueObject;
/**
 * Represents a realm
 *
 * @author Anshu Gaind
 **/
public class Realm implements IValueObject {
  private HashMap realmProperties;
  private String realmName;

  /**
   * Constructor
   * 
   * @param realmName the realm name
   **/
  public Realm(String realmName){
    super();
    realmProperties = new HashMap();
    this.realmName = realmName;
  }

  /**
   * Returns the realm name
   *
   * @return the realm name
   **/
  public String getName(){
    return  realmName;
  }

  /**
   * Returns a property from the realm
   *
   * @param key the key for the property
   * @return the property
   **/
  public Property getProperty(String key) {
    if (realmProperties.containsKey(key)) {
        return (Property)realmProperties.get(key);
    } else {
        // do not throw exception
        return null;
    }
  }

  /**
   * Sets a property in the realm
   *
   * @param property the property
   **/
  public void setProperty(Property property){
    realmProperties.put(property.getName().trim(), property);
  }
}