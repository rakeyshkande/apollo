package com.soa.framework.config.vo;
import com.soa.framework.interfaces.IValueObject;

/**
 * Represents a property as configured in the xml files
 *
 * @author Anshu Gaind
 **/
public class Property implements IValueObject
{
  private String name;
  private String value;

  /**
   * The default constructor
   **/
  public Property(){
    super();
  }

  /**
   * Return the name of the property
   *
   * @return property name
   **/
  public String getName(){
    return name;
  }

  /** 
   * Sets the property name
   *
   * @param newName the property name
   **/
  public void setName(String newName){
    name = newName.trim();
  }

  /**
   * Returns the property value
   *
   * @return the property value
   **/
  public String getValue(){
    return value;
  }

  /**
   * Sets the property value
   *
   * @param newValue the property value
   **/
  public void setValue(String newValue){
    value = newValue.trim();
  }





}