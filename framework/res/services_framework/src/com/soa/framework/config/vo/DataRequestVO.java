package com.soa.framework.config.vo;
import java.util.ArrayList;
/**
 * Represents a request made to the Data Access Service
 *
 * @author Anshu Gaind
 */
 
public class DataRequestVO  {
    private String statementID;
    ArrayList inputParams;

    /**
     * The default constructor
     */
    
    public DataRequestVO() {
        super();
    }
    
    public String getStatementID() {
        return statementID;
    }

    public void setStatementID(String newStatementID) {
        statementID = newStatementID;
    }

    public void setInputParams(ArrayList newInputParams) {
        inputParams = newInputParams;
    }

    public ArrayList getInputParams(){
        return inputParams;
    }
}