package com.soa.framework.config.vo;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Iterator;

/**
 * Acts as a disconnected result set, using a similar API
 *
 * @author Anshu Gaind
 */
 
public class CachedResultSet  {
    private ArrayList records;
    private ArrayList currentRecord;
    private Iterator iterator;

    /**
     * The default constructor
     */
     public CachedResultSet(){
        super();
        records = new ArrayList();
     }

     
    /**
     * Populates itself from the contents of a result set
     *
     * @param resultSet the result set object
     */
    public void populate(ResultSet resultSet) throws Exception{
        ArrayList record = null;
        ResultSetMetaData rsetMetaData = resultSet.getMetaData();
        while (resultSet.next())  {
            // create a new record
            record = new ArrayList();

            //populate the record; result set starts at 1
            for (int i = 1; i <= rsetMetaData.getColumnCount(); i++)  {
              record.add(resultSet.getObject(i));
            }
            
            //add it to the collection of records
            records.add(record);
        }        
    }

    /**
     * Moves the cursor position to the next record
     */
    public boolean next() {
      if (iterator == null)  {
          iterator = records.iterator();
      }
      if (iterator.hasNext())  {
        currentRecord = (ArrayList) iterator.next();
        return true;
      } else {
        // reset the currentRecord
        currentRecord = null;
        return false;
      }
    }
    
    /**
     * Return the field from the indicated column position. The first column
     * begins at index 1, like in the java.sql.ResultSet object
     *
     * @param index the indicated column position
     * @return the field from the current record
     **/
    public Object getObject(int index){
      if (currentRecord != null)  {
         // 
          return currentRecord.get(--index);
      } else {
        return null;
      }
    }


    /**
     * Returns a count of the number of columns in the result set
     *
     **/
     public int getColumnCount(){
        if (records != null)  {
            ArrayList topRecord = (ArrayList)records.get(0);
            if (topRecord != null)  {
              return topRecord.size();  
            } else {
              return 0;
            }            
        } else {
            return 0;
        }
     }


    /**
     * Returns a count of the number of columns in the result set
     *
     **/
     public int getRowCount(){
        if (records != null)  {
            return records.size();
        } else {
            return 0;
        }
     }

    /**
     * Reset the cached result set to the beginning of the record set.
     */
    public void reset(){
      iterator = records.iterator();
    }
    
}