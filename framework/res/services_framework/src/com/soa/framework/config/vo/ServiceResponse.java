package com.soa.framework.config.vo;
import com.soa.framework.interfaces.IValueObject;

/**
 *  Represents an Input tag on the configuration files
 *
 * @author Anshu Gaind
 */
public class ServiceResponse implements IValueObject { 
  private Object output;

    /**
     * The default constructor
     */
    public ServiceResponse(){
      super();
    }
    
  public Object getOutput() {
    return output;
  }

  public void setOutput(Object newOutput) {
    output = newOutput;
  }
}