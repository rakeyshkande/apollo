package com.soa.framework.config.vo;
import com.soa.framework.interfaces.IValueObject;

/**
 *  Represents an Output tag as defined in the configuration files
 *
 * @author Anshu Gaind
 */
public class Output implements IValueObject {
    private String name;
    private String type;

    /**
     * The default constructor
     */
    public Output() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String newType) {
        type = newType.trim();
    }
}