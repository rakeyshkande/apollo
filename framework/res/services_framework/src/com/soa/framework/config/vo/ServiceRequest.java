package com.soa.framework.config.vo;
import java.util.List;
import java.util.ArrayList;
import com.soa.framework.interfaces.IValueObject;

/**
 *  Represents an Input tag on the configuration files
 *
 * @author Anshu Gaind
 */

public class ServiceRequest implements IValueObject {
  private String service;
  private String operation;
  private ArrayList inputs;
  private String operationID;

    /**
     * The default constructor
     */
  public ServiceRequest(){
    super();
    inputs = new ArrayList();
  }
  
  public String getService() {
    return service;
  }

  public void setService(String newService) {
    service = newService.trim();
  }

  public void setService(String newService, String newOperationID, String newOperation){
    service = newService.trim();
    operationID = newOperationID.trim();
    operation = newOperation.trim();
  }
  

  public String getOperation() {
    return operation;
  }

  public void setOperation(String newOperation ) {
    operation = newOperation.trim();
  }

  public void setOperation(String newOperationID, String newOperation ) {
    operationID = newOperationID.trim();
    operation = newOperation.trim();
  }

  public List getInputs() {
    return inputs;
  }

  public Object getInput(int index){
    return inputs.get(index);
  }

  public void setInput(int index, Object object){
    inputs.add(index, object);
  }

  public String getOperationID() {
    return operationID;
  }

  public void setOperationID(String newOperationID) {
    operationID = newOperationID.trim();
  }
}