package com.soa.framework.config.vo;
import java.util.HashMap;
import com.soa.framework.interfaces.IValueObject;
import com.soa.framework.config.vo.Operation;
import com.soa.framework.plugins.Logger;

/**
 *  Represents a service configuration as defined in services.xml
 *
 * @author Anshu Gaind
 */

public class ServiceConfig implements IValueObject {
  private HashMap properties;
  private HashMap attributes;
  private HashMap operations;
  private Logger logger;
  private Object addlConfig;
  
    /**
     * The default constructor
     */
  public ServiceConfig() {
    super();
    properties = new HashMap();
    attributes = new HashMap();
    operations = new HashMap();
    logger = new Logger("framework.service.manager");
  }

  /**
   * Returns an empty string if the required property is not found
   */
  public String getProperty(String name) {
    String rv = (String)properties.get(name);
    if (rv == null)  {
       return "";     
    } else {    
       return rv;
    }
  }

  public void setProperty(String name, String value) {
    properties.put(name.trim(), value.trim());
    logger.debug("Setting Property " + name);
  }

    public String getAttribute(String name) {
        return (String)attributes.get(name);
    }

    public void setAttribute(String name, String value) {
      attributes.put(name.trim(), value.trim());
      logger.debug("Setting Attribute " + name);
    }

    public Operation getOperation(String ID) {
        return (Operation)operations.get(ID);
    }

    public void setOperation(Operation operation) {
        operations.put(operation.getID().trim(), operation);
        logger.debug("Setting Operation " + operation.getName() + " ID " + operation.getID());
    }

    public boolean containsOperation(String ID){
      return operations.containsKey(ID);
    }

    public Object getAddlConfig() {
        return addlConfig;
    }

    public void setAddlConfig(Object newAddlConfig) {
        addlConfig = newAddlConfig;
    }

}