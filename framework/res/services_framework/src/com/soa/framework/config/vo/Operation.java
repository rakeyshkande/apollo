package com.soa.framework.config.vo;
import com.soa.framework.interfaces.IValueObject;
import com.soa.framework.config.vo.Input;
import com.soa.framework.config.vo.Output;
import java.util.List;
import java.util.ArrayList;
import com.soa.framework.plugins.Logger;

/**
 * Represents an operation tag as defined in the configuration files
 *
 * @author Anshu Gaind
 */
public class Operation implements IValueObject {
    private String name;
    private ArrayList inputs;
    private Output output;
    private Logger logger;
    private String ID;

    /**
     * The default constructor
     */
    public Operation(){
        super();
        inputs = new ArrayList();
        logger = new Logger("framework.service.manager");
    }
    
    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName.trim();
    }

    public void setInput(Input input){
        this.inputs.add(input);
        logger.debug("Setting Input " + input.getName() + " for Operation " + name);
    }

    public Input getInput(int index){
        return (Input)this.inputs.get(index);
    }

    public List getInputs(){
      return inputs;
    }

    public int getInputSize(){
      return inputs.size();
    }

    public void setOutput(Output output){
        this.output = output;
        logger.debug("Setting Output " + output.getName() + " for Operation " + name);
    }

    public Output getOutput(){
        return output;
    }

    public String getID() {
      return ID;
    }

    public void setID(String newID) {
      ID = newID.trim();
    }
 

}