package com.soa.framework.config.vo;
import com.soa.framework.interfaces.IValueObject;
import java.io.File;
import java.util.List;

public class NotificationVO implements IValueObject  {
  private String SMTPHost;
  private String messageContent;
  private String messageSubject;
  private String messageMimeType;
  private String messageFromAddress;
  private String messageTOAddress;
  private String messageCCAddress;
  private String messageBCCAddress;
  private String addressSeparator;
  private File fileAttachment;
  private List fileAttachments;

    /**
     * The default constructor
     */
  public NotificationVO() {
    super();
  }

  public String getSMTPHost() {
    return SMTPHost;
  }

  public void setSMTPHost(String newSMTPHost) {
    SMTPHost = newSMTPHost.trim();
  }

  public String getMessageContent() {
    return messageContent;
  }

  public void setMessageContent(String newMessageContent) {
    messageContent = newMessageContent.trim();
  }

  public String getMessageSubject() {
    return messageSubject;
  }

  public void setMessageSubject(String newMessageSubject) {
    messageSubject = newMessageSubject.trim();
  }

  public String getMessageMimeType() {
    return messageMimeType;
  }

  public void setMessageMimeType(String newMessageMimeType) {
    messageMimeType = newMessageMimeType.trim();
  }

  public String getMessageFromAddress() {
    return messageFromAddress;
  }

  public void setMessageFromAddress(String newMessageFromAddress) {
    messageFromAddress = newMessageFromAddress.trim();
  }

  public String getMessageTOAddress() {
    return messageTOAddress;
  }

  public void setMessageTOAddress(String newMessageTOAddress) {
    messageTOAddress = newMessageTOAddress.trim();
  }

  public String getMessageCCAddress() {
    return messageCCAddress;
  }

  public void setMessageCCAddress(String newMessageCCAddress) {
    messageCCAddress = newMessageCCAddress.trim();
  }

  public String getMessageBCCAddress() {
    return messageBCCAddress;
  }

  public void setMessageBCCAddress(String newMessageBCCAddress) {
    messageBCCAddress = newMessageBCCAddress.trim();
  }

  public String getAddressSeparator() {
    return addressSeparator;
  }

  public void setAddressSeparator(String newAddressSeparator) {
    addressSeparator = newAddressSeparator.trim();
  }

  public File getFileAttachment() {
    return fileAttachment;
  }

  public void setFileAttachment(File newFileAttachment) {
    fileAttachment = newFileAttachment;
  }




  public boolean hasAttachment(){
    if (fileAttachment != null 
        || fileAttachments != null)  {
        return true;
    }
    return false;
  }

  public List getFileAttachments() {
    return fileAttachments;
  }

  public void setFileAttachments(List newFileAttachments) {
    fileAttachments = newFileAttachments;
  }



}