
package com.soa.framework.config.vo;
import java.util.ArrayList;
import java.util.List;
import com.soa.framework.interfaces.IValueObject;
import com.soa.framework.plugins.Logger;

/**
 *  Represents a statement configuration as defined in statements.xml
 *
 * @author Anshu Gaind
 */

public class StatementConfig implements IValueObject {
    private String name;
    private ArrayList inputs;
    private Output output;
    private String ID;
    private String type;
    private String SQL;
    private Logger logger;
  
    /**
     * The default constructor
     */
  public StatementConfig() {
    super();
    inputs = new ArrayList();
    logger = new Logger("framework.service.manager");
  }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName.trim();
    }

    public void setInput(Input input){
        this.inputs.add(input);
        logger.debug("Setting Input " + input.getName() + " for Statement " + name);
    }

    public Input getInput(int index){
        return (Input)this.inputs.get(index);
    }

    public List getInputs(){
      return inputs;
    }

    public int getInputSize(){
      return inputs.size();
    }

    public void setOutput(Output output){
        this.output = output;
        logger.debug("Setting Output " + output.getName() + " for Statement " + name);
    }

    public Output getOutput(){
        return output;
    }

    public String getID() {
      return ID;
    }

    public void setID(String newID) {
      ID = newID.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String newType) {
        type = newType.trim();
    }

    public String getSQL() {
        return SQL;
    }

    public void setSQL(String newSQL) {
        SQL = newSQL.trim();
    }
}