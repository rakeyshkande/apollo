<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
	/*
		This file imports all of the tag libraries needed by the application
		Author: 	Anshu Gaind
		Version:	Aug 19, 2002
	*/
%>

<%@ taglib uri='/WEB-INF/struts-html.tld' 		prefix='html' %>
<%@ taglib uri='/WEB-INF/struts-template.tld' 	prefix='template' %>
<%@ taglib uri='/WEB-INF/struts-bean.tld'     	prefix='bean' %>
<%@ taglib uri='/WEB-INF/struts-form.tld' 		prefix='form' %>
<%@ taglib uri='/WEB-INF/struts-logic.tld' 		prefix='logic' %>
