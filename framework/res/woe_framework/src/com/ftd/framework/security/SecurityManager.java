package com.ftd.framework.security;

import oracle.jdbc.*;
import java.util.*;
import java.sql.*;
import java.text.*;
import java.security.MessageDigest;
import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.framework.common.valueobjects.FTDRole;
import com.ftd.framework.common.valueobjects.FTDSecurityCertificate;
import com.ftd.framework.common.valueobjects.FTDArguments;

public class SecurityManager
{
    private static SecurityManager sm = null;
    private DataManager dataManager = null;


    private SecurityManager()
    {
      dataManager = DataManager.getInstance();
    }

	public LogManager getLogManager()
	{
		return new LogManager("com.ftd.oe");
	}

    public static SecurityManager getInstance()
    {
        if(sm == null)
        {
            sm = new SecurityManager();
        }

        return sm;
    }

    public void logout(String sessionId) throws Exception
    {
        Connection conn = null;
        CallableStatement callableStatement = null;

        try
        {
            conn =  dataManager.getConnection();

            callableStatement = conn.prepareCall("{ call OE_DELETE_CERTIFICATE( ? ) }");
            callableStatement.setString(1, sessionId);
            callableStatement.execute();
        }
        catch (Exception e)
        {
            throw new Exception(e.toString());
        }
        finally
        {
			dataManager.closeResources(null, callableStatement, conn);
        }
    }

    public FTDSecurityCertificate authenticateSession(String sessionId) throws Exception
    {
        FTDSecurityCertificate cert = null;
        Connection conn = null;
        CallableStatement callableStatement = null;
        ResultSet rs = null;
        String firstName = null;
        String lastName = null;
        String storedUsername = null;
        String callCenterID = null;
        String userName = null;
        String roleID = null;

        try
        {
            conn =  dataManager.getConnection();

            callableStatement = conn.prepareCall("{ ? = call OE_GET_CERTIFICATE( ? ) }");
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setString(2, sessionId);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            if ( rs.next() )
            {
                firstName = rs.getString("firstName");
                lastName = rs.getString("lastName");
                callCenterID = rs.getString("callCenterId");
                roleID = rs.getString("roleID");
                userName = rs.getString("userName");
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.toString());
        }
        finally
        {
			dataManager.closeResources(rs, callableStatement, conn);
        }

        if(lastName != null && firstName != null && userName != null)
        {
            cert = new FTDSecurityCertificate();
            cert.setSessionId(sessionId);
            cert.setLastName(lastName);
            cert.setFirstName(firstName);
            cert.setCallCenterID(callCenterID);
            cert.setUserName(userName);

            // retrieve functions that role has access to
            com.ftd.framework.common.valueobjects.FTDRole role = getRole(roleID);
            cert.setRole(role);
        }

        return cert;
    }

    public FTDSecurityCertificate authenticateUsername(String username, String password) throws Exception
    {
        return authenticate(username, password, false);
    }

    public FTDSecurityCertificate authenticate(String username, String password, boolean addToCertTable) throws Exception
    {
        FTDSecurityCertificate cert = null;
        Connection conn = null;
        CallableStatement callableStatement = null;
        ResultSet rs = null;
        String firstName = null;
        String lastName = null;
        String activeFlag = null;
        String deletedFlag = null;
        String storedPassword = null;
        String storedUsername = null;
        String sessionId = null;
        String callCenterID = null;
        String roleID = null;
        byte[] bytesStoredPassword = null;

        try
        {
            conn =  dataManager.getConnection();

            callableStatement = conn.prepareCall("{ ? = call OE_GET_USER( ? ) }");
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setString(2, username);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            if ( rs.next() )
            {
                bytesStoredPassword = rs.getBytes("currentPassword");

                storedUsername = rs.getString("userId");
                firstName = rs.getString("firstName");
                lastName = rs.getString("lastName");
                activeFlag = rs.getString("activeFlag");
                deletedFlag = rs.getString("deletedFlag");
                callCenterID = rs.getString("callCenterId");
                roleID = rs.getString("roleID");
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.toString());
        }
        finally
        {
			dataManager.closeResources(rs, callableStatement, conn);
		}

        byte[] bytesInputPassword = null;
        if ( !(storedUsername == null) )
        {
            try
            {
                bytesInputPassword = this.encryptText(password);
            }
            catch(Exception e)
            {
                throw e;
            }

            if(MessageDigest.isEqual(bytesStoredPassword, bytesInputPassword))
            {
                if ( deletedFlag.equals("N") )
                {
                    if ( activeFlag.equals("A") )
                    {
                        cert = new FTDSecurityCertificate();
                        cert.setSessionId((GUIDGenerator.getInstance()).getGUID());
                        cert.setLastName(lastName);
                        cert.setFirstName(firstName);
                        cert.setCallCenterID(callCenterID);
                        cert.setUserName(storedUsername);

                        // retrieve functions that role has access to
                        com.ftd.framework.common.valueobjects.FTDRole role = getRole(roleID);
                        cert.setRole(role);

                        // put user in SECURITY_CERT table
                        if(addToCertTable)
                        {
                            addCertToStore(cert);
                        }
                    }
                    else
                    {
                        // account is Inactive
                        throw new Exception ("This account is inactive");
                    }
                }
                else
                {
                    // Account was deleted
                    throw new Exception ("This account has been deleted");
                }
            }
            else
            {
                // passwords do not match
                throw new Exception ("Invalid password Exception");
            }
        }

        return cert;
    }

    public void updateUserPassword(FTDArguments arguments) throws Exception
    {
        Connection conn = null;
        CallableStatement callableStatement = null;
        ResultSet rs = null;
        String firstName = null;
        String lastName = null;
        String activeFlag = null;
        String deletedFlag = null;
        String storedUsername = null;
        String callCenterID = null;
        String lastUpdateBy = null;
        String lastUpdateDate = null;
        String roleID = null;

        try
        {
            conn =  dataManager.getConnection();

            callableStatement = conn.prepareCall("{ ? = call OE_GET_USER( ? ) }");
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setString(2, (String)arguments.get(SecurityConstants.USERPROFILE_USER_ID));
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            if ( rs.next() )
            {
                storedUsername = rs.getString("userId");
                firstName = rs.getString("firstName");
                lastName = rs.getString("lastName");
                activeFlag = rs.getString("activeFlag");
                deletedFlag = rs.getString("deletedFlag");
                callCenterID = rs.getString("callCenterId");
                roleID = rs.getString("roleID");
                lastUpdateBy = rs.getString("lastUpdatedBy");
                lastUpdateDate = rs.getString("lastUpdatedDate");

                SimpleDateFormat sdfOutput = new SimpleDateFormat( "MM/dd/yyyy HH:mm:ss" );
                SimpleDateFormat sdfInput = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss" );

                java.util.Date date = sdfInput.parse( lastUpdateDate );
                lastUpdateDate = sdfOutput.format( date );
            }
        }
        catch (Exception e)
        {
            throw new Exception(e.toString());
        }
        finally
        {
			dataManager.closeResources(rs, callableStatement, conn);
		}

        arguments.put(SecurityConstants.USERPROFILE_USER_ID, storedUsername);
        arguments.put(SecurityConstants.USERPROFILE_ROLE_ID, roleID);
        arguments.put(SecurityConstants.USERPROFILE_CALL_CENTER_ID, callCenterID);
        arguments.put(SecurityConstants.USERPROFILE_FIRST_NAME, firstName);
        arguments.put(SecurityConstants.USERPROFILE_LAST_NAME, lastName);
        arguments.put(SecurityConstants.USERPROFILE_HOME_PHONE, "");
        arguments.put(SecurityConstants.USERPROFILE_ADDRESS_1, "");
        arguments.put(SecurityConstants.USERPROFILE_ADDRESS_2, "");
        arguments.put(SecurityConstants.USERPROFILE_CITY, "");
        arguments.put(SecurityConstants.USERPROFILE_STATE, "");
        arguments.put(SecurityConstants.USERPROFILE_POSTAL_CODE, "");
        arguments.put(SecurityConstants.USERPROFILE_ACTIVE_FLAG, activeFlag);
        arguments.put(SecurityConstants.USERPROFILE_LAST_UPDATE_USER, lastUpdateBy);
        arguments.put(SecurityConstants.USERPROFILE_LAST_UDPATE_DATE, lastUpdateDate);

        updateUser(arguments);
    }

    public void updateUser(FTDArguments arguments) throws Exception
    {

        String userID = null;
        Connection conn = null;
        CallableStatement callableStatement = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;

        try
        {
            conn =  dataManager.getConnection();

            userID = ((String) arguments.get(SecurityConstants.USERPROFILE_USER_ID)).toUpperCase();
            String roleId = (String) arguments.get(SecurityConstants.USERPROFILE_ROLE_ID);
            String callCenterId = (String) arguments.get(SecurityConstants.USERPROFILE_CALL_CENTER_ID);
            String firstName = (String) arguments.get(SecurityConstants.USERPROFILE_FIRST_NAME);
            String lastName = (String) arguments.get(SecurityConstants.USERPROFILE_LAST_NAME);
            String homePhone = (String) arguments.get(SecurityConstants.USERPROFILE_HOME_PHONE);
            String address1 = (String) arguments.get(SecurityConstants.USERPROFILE_ADDRESS_1);
            String address2 = (String) arguments.get(SecurityConstants.USERPROFILE_ADDRESS_2);
            String city = (String) arguments.get(SecurityConstants.USERPROFILE_CITY);
            String state = (String) arguments.get(SecurityConstants.USERPROFILE_STATE);
            String postalCode = (String) arguments.get(SecurityConstants.USERPROFILE_POSTAL_CODE);
            byte[] bytes = sm.encryptText((String) arguments.get(SecurityConstants.USERPROFILE_CURRENT_PASSWORD));
            String encryptPassword = (String) arguments.get("encryptPassword");

            String activeFlag = (String) arguments.get(SecurityConstants.USERPROFILE_ACTIVE_FLAG);
            String lastUpdateUser = (String) arguments.get(SecurityConstants.USERPROFILE_LAST_UPDATE_USER);

            // Check to see if this user exists
            callableStatement = conn.prepareCall("{ ? = call OE_GET_USER( ? ) }");
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setString(2, userID);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);
            String sql = null;
            if ( rs.next() )
            {
                sql = "UPDATE USER_PROFILE SET FIRST_NAME='" + FrameworkFieldUtils.translateNullToString(firstName) + "'" +
                            ", LAST_NAME='" + FrameworkFieldUtils.translateNullToString(lastName) + "'" +
                            ", ADDRESS_LINE_1='" + FrameworkFieldUtils.translateNullToString(address1) + "'" +
                            ", ADDRESS_LINE_2='" + FrameworkFieldUtils.translateNullToString(address2) + "'" +
                            ", CITY='" + FrameworkFieldUtils.translateNullToString(city) + "'" +
                            ", STATE_CODE='" + FrameworkFieldUtils.translateNullToString(state) + "'" +
                            ", POSTAL_CODE='" + FrameworkFieldUtils.translateNullToString(postalCode) + "'" +
                            ", HOME_PHONE='" + FrameworkFieldUtils.translateNullToString(homePhone) + "'" +
                            ", CALL_CENTER_ID='" + FrameworkFieldUtils.translateNullToString(callCenterId) + "'" +
                            ", ACTIVE_FLAG='" + FrameworkFieldUtils.translateNullToString(activeFlag) + "'" +
                            ", ROLE_ID='" + FrameworkFieldUtils.translateNullToString(roleId) + "'" +
                            ", LAST_UPDATED_BY='" + FrameworkFieldUtils.translateNullToString(lastUpdateUser) + "'" +
                            " WHERE USER_ID='" + userID + "'";

                preparedStatement = conn.prepareStatement(sql);
                preparedStatement.execute();
                preparedStatement.close();

                // only set the password if told to do so
                if(encryptPassword != null && encryptPassword.equals("false"))
                {
                }
                else
                {
                    sql = "UPDATE USER_PROFILE SET CURRENT_PASSWORD = ? WHERE USER_ID = ?";
                    preparedStatement = conn.prepareStatement(sql);
                    preparedStatement.setBytes(1, bytes);
                    preparedStatement.setString(2, userID);
                    preparedStatement.execute();
                }
            }
            else // Insert a new user
            {
                SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy HH:mm:ss" );
                java.util.Date date = new java.util.Date();
                String strDate = sdfInput.format(date);


                /* JMP 04/01/03
                 * Took out so we can set the password to whatever is wanted by the user
                // set password to password if reset box not checked
                // only set the password if told to do so
                if(encryptPassword != null && encryptPassword.equals("false"))
                {
                }
                else
                {
                    bytes = sm.encryptText("password");
                }
                */

                sql = "INSERT INTO USER_PROFILE (USER_ID, CURRENT_PASSWORD, FIRST_NAME, LAST_NAME, ADDRESS_LINE_1, " +
                                                "ADDRESS_LINE_2, CITY, STATE_CODE, POSTAL_CODE, HOME_PHONE, " +
                                                "CALL_CENTER_ID, ACTIVE_FLAG, ROLE_ID, LAST_UPDATED_BY, CURRENT_PASSWORD_DATE, " +
                                                "LOGIN_ATTEMPTS, CREATED_BY, CREATED_DATE, DELETED_FLAG, LAST_UPDATED_DATE) " +
                                                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                preparedStatement = conn.prepareStatement(sql);
                preparedStatement.setString(1, FrameworkFieldUtils.translateNullToString(userID));
                preparedStatement.setBytes(2, bytes);
                preparedStatement.setString(3, FrameworkFieldUtils.translateNullToString(firstName));
                preparedStatement.setString(4, FrameworkFieldUtils.translateNullToString(lastName));
                preparedStatement.setString(5, FrameworkFieldUtils.translateNullToString(address1));
                preparedStatement.setString(6, FrameworkFieldUtils.translateNullToString(address2));
                preparedStatement.setString(7, FrameworkFieldUtils.translateNullToString(city));
                preparedStatement.setString(8, FrameworkFieldUtils.translateNullToString(state));
                preparedStatement.setString(9, FrameworkFieldUtils.translateNullToString(postalCode));
                preparedStatement.setString(10, FrameworkFieldUtils.translateNullToString(homePhone));
                preparedStatement.setString(11,  FrameworkFieldUtils.translateNullToString(callCenterId));
                preparedStatement.setString(12, FrameworkFieldUtils.translateNullToString(activeFlag));
                preparedStatement.setString(13, FrameworkFieldUtils.translateNullToString(roleId));
                preparedStatement.setString(14, FrameworkFieldUtils.translateNullToString(lastUpdateUser));
                preparedStatement.setTimestamp(15, FrameworkFieldUtils.formatStringToSQLTimestamp(strDate));
                preparedStatement.setInt(16, 0);
                preparedStatement.setString(17, FrameworkFieldUtils.translateNullToString(lastUpdateUser));
                preparedStatement.setTimestamp(18, FrameworkFieldUtils.formatStringToSQLTimestamp(strDate));
                preparedStatement.setString(19, FrameworkFieldUtils.translateNullToString("N"));
                preparedStatement.setTimestamp(20, FrameworkFieldUtils.formatStringToSQLTimestamp(strDate));
                preparedStatement.execute();

            }
/*
            callableStatement = conn.prepareCall("{ call SP_UPDATE_USER( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ) }");
            userID = (String) arguments.get(SecurityConstants.USERPROFILE_USER_ID);

            callableStatement.setString(1, userID);
            callableStatement.setString(2, (String) arguments.get(SecurityConstants.USERPROFILE_ROLE_ID));
            callableStatement.setString(3, (String) arguments.get(SecurityConstants.USERPROFILE_CALL_CENTER_ID));
            callableStatement.setString(4, (String) arguments.get(SecurityConstants.USERPROFILE_FIRST_NAME));
            callableStatement.setString(5, (String) arguments.get(SecurityConstants.USERPROFILE_LAST_NAME));
            callableStatement.setString(6, (String) arguments.get(SecurityConstants.USERPROFILE_HOME_PHONE));
            callableStatement.setString(7, (String) arguments.get(SecurityConstants.USERPROFILE_ADDRESS_1));
            callableStatement.setString(8, (String) arguments.get(SecurityConstants.USERPROFILE_ADDRESS_2));
            callableStatement.setString(9, (String) arguments.get(SecurityConstants.USERPROFILE_CITY));
            callableStatement.setString(10, (String) arguments.get(SecurityConstants.USERPROFILE_STATE));
            callableStatement.setString(11, (String) arguments.get(SecurityConstants.USERPROFILE_POSTAL_CODE));

            String currPswd = (String) arguments.get(SecurityConstants.USERPROFILE_CURRENT_PASSWORD);
            String password = null;
            String encryptPassword = (String) arguments.get("encryptPassword");
            if(encryptPassword != null && encryptPassword.equals("false"))
            {
                password = (String) arguments.get(SecurityConstants.USERPROFILE_CURRENT_PASSWORD);
            }
            else
            {
                password = sm.encryptText((String) arguments.get(SecurityConstants.USERPROFILE_CURRENT_PASSWORD));
            }

            byte[] bytes = null;
            bytes  = password.getBytes();
            callableStatement.setBytes(12, bytes);

            callableStatement.setString(13, (String) arguments.get(SecurityConstants.USERPROFILE_ACTIVE_FLAG));
            callableStatement.setString(14, (String) arguments.get(SecurityConstants.USERPROFILE_LAST_UPDATE_USER));
            callableStatement.setTimestamp(15, FrameworkFieldUtils.formatStringToSQLTimestamp((String) arguments.get(SecurityConstants.USERPROFILE_LAST_UDPATE_DATE)));

            callableStatement.execute();

            callableStatement.close();
            callableStatement = null;
*/
        } catch(Exception e) {
            if ( e.getMessage().startsWith("ORA-20001: User update for ID " + userID + " failed due to last_update date mismatch.") )
            {
                throw new Exception(e.toString());
            }
            else
            {
                this.getLogManager().error(e.getMessage(), e);
            }
        }
        finally
        {
			dataManager.closeResources(null, preparedStatement, null);
			dataManager.closeResources(rs, callableStatement, conn);
		}
    }

    public void removeUser(FTDArguments arguments) throws Exception
    {
        String userID = null;
        Connection conn = null;
        CallableStatement callableStatement = null;

        try {
            // connect to database
           conn =  dataManager.getConnection();

            // build sql statement
            // Get data from the Zip Code and State Master tables
            callableStatement = conn.prepareCall("{ call SP_DELETE_USER( ?,?,? ) }");

            userID = (String) arguments.get(SecurityConstants.USERPROFILE_USER_ID);

            callableStatement.setString(1, userID);
            callableStatement.setString(2, (String) arguments.get(SecurityConstants.USERPROFILE_LAST_UPDATE_USER));
            callableStatement.setTimestamp(3, FrameworkFieldUtils.formatStringToSQLTimestamp((String) arguments.get(SecurityConstants.USERPROFILE_LAST_UDPATE_DATE)));

            callableStatement.execute();

        }
        catch(Exception e) {
            if ( e.getMessage().startsWith("ORA-20001: User delete for ID " + userID + " failed due to last_update date mismatch.") )
            {
                throw new Exception(e.toString());
            }
            else
            {
                this.getLogManager().error(e.getMessage(), e);
            }
        }
        finally
        {
			dataManager.closeResources(null, callableStatement, conn);
        }
    }

    public void updateRole(FTDRole roleInfo)
        throws Exception
    {

        Connection conn = null;
        CallableStatement updateRole = null;
        String roleID = null;

        try {

            conn = dataManager.getConnection();

            // Add/Update main Role information
            updateRole = conn.prepareCall("{ call SP_UPDATE_ROLE( ?,?,?,? ) }");
            roleID = roleInfo.getRoleID();

            updateRole.setString(1, roleID);
            updateRole.setString(2, roleInfo.getDescription());
            updateRole.setString(3, roleInfo.getLastUpdateUser());
            updateRole.setTimestamp(4, FrameworkFieldUtils.formatStringToSQLTimestamp(roleInfo.getLastUpdateDate()));

            updateRole.execute();
            updateRole.close();
            updateRole = null;

            // Delete existing Role Function information
            updateRole = conn.prepareCall("{ call SP_DELETE_ROLE_FUNCTIONS( ? ) }");
            updateRole.setString(1, roleID);

            updateRole.execute();
            updateRole.close();
            updateRole = null;

            // Add New Role Function information
            updateRole = conn.prepareCall("{ call SP_UPDATE_ROLE_FUNCTIONS( ?,? ) }");
            updateRole.setString(1, roleID);

            List functions = roleInfo.getFunction();

//            Collection functions = roleInfo.getFunction().values();
            Iterator iter = functions.iterator();

            while ( iter.hasNext() )
            {
                String functionID = (String) iter.next();

                updateRole.setString(2, functionID);
                updateRole.execute();
            }

            updateRole.close();
            updateRole = null;

            conn.close();
            conn = null;

        } catch(Exception e) {
            if ( e.getMessage().startsWith("ORA-20001: Role update for ID " + roleID + " failed due to last_update date mismatch.") )
            {
                throw new Exception(e.toString());
            }
            else
            {
                this.getLogManager().error(e.getMessage(), e);
            }
        }
        finally
        {
            if(updateRole != null)
            {
                updateRole.close();
            }

            if(conn != null)
            {
                conn.close();
            }
        }
    }

    public void removeRole(FTDArguments arguments) throws Exception
    {
        Connection conn = null;
        CallableStatement callableStatement = null;
        String roleID = null;

        try {

            conn = dataManager.getConnection();

            roleID = (String) arguments.get(SecurityConstants.ROLE_ROLE_ID);

            // build sql statement
            callableStatement = conn.prepareCall("{ call SP_DELETE_ROLE( ?,? ) }");

            callableStatement.setString(1, roleID);
            callableStatement.setTimestamp(2, FrameworkFieldUtils.formatStringToSQLTimestamp((String) arguments.get(SecurityConstants.ROLE_LAST_UPDATE_DATE)));

            callableStatement.execute();

            callableStatement.close();
            callableStatement = null;

            conn.close();
            conn = null;

        } catch(Exception e) {
            if ( e.getMessage().startsWith("ORA-20001: Role delete for ID " + roleID + " failed due to last_update date mismatch.") )
            {
                throw new Exception(e.toString());
            }
            else
            {
                this.getLogManager().error(e.getMessage(), e);
            }
        }
        finally
        {
            if(callableStatement != null)
            {
                callableStatement.close();
            }

            if(conn != null)
            {
                conn.close();
            }
        }
    }

    private FTDRole getRole (String roleID) throws Exception
    {
        FTDRole role = new FTDRole();
        CallableStatement callableStatement = null;
        ResultSet rs = null;
        Connection conn = null;

        try {
            // connect to database

            conn = dataManager.getConnection();

            // build sql statement
            callableStatement = conn.prepareCall("{ ? = call OE_GET_ROLE( ? ) }");
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.setString(2, roleID);
            callableStatement.execute();

            rs = (ResultSet) callableStatement.getObject(1);

            if ( rs.next() )
            {
                role.setRoleID(roleID);
                role.setDescription(rs.getString("roleDescription"));
                role.setLastUpdateUser(rs.getString("updateUser"));
                role.setLastUpdateDate(FrameworkFieldUtils.formatSQLDateToString(rs.getDate("updateDate")));

                // build list of functions that role has access to
                List functions = new ArrayList();
                functions.add(rs.getString("functionId"));
                while ( rs.next() )
                {
                    functions.add(rs.getString("functionId"));
                }

                role.setFunction(functions);
            }

        }
        catch(Exception e) {
            if ( e.getMessage().startsWith("ORA-20001: Role delete for ID " + roleID + " failed due to last_update date mismatch.") )
            {
                throw new Exception(e.toString());
            }
            else
            {
                this.getLogManager().error(e.getMessage(), e);
            }
        }
        finally
        {
			dataManager.closeResources(rs, callableStatement, conn);
        }

        return role;
    }

    private byte[] encryptText(String text)
        throws Exception
    {
        String encryptedText = null;
         byte[] aMessageDigest = null;
        try
        {
            // Create a Message Digest from a Factory method
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] msg = text.getBytes("ISO8859_1");

            // Update the message digest with some more bytes
            // This can be performed multiple times before creating the hash
            md.update(msg);

            // Create the digest from the message
            aMessageDigest = md.digest();
        }
        catch (Exception e)
        {
            this.getLogManager().error(e.getMessage(), e);

            String msg[] = new String[1];
            msg[0] = e.getMessage();

            throw new Exception("Unable to encrypt password");
        }

        return aMessageDigest;
    }

    private void addCertToStore(FTDSecurityCertificate cert) throws Exception
    {
        Connection conn = null;
        CallableStatement callableStatement = null;

        try {

            conn =  dataManager.getConnection();

            callableStatement = conn.prepareCall("{ call OE_UPDATE_CERTIFICATE( ?,?,?,?,?,? ) }");

            callableStatement.setString(1, cert.getSessionId());
            callableStatement.setString(2, cert.getFirstName());
            callableStatement.setString(3, cert.getLastName());
            callableStatement.setString(4, cert.getCallCenterID());
            callableStatement.setString(5, cert.getRole().getRoleID());
            callableStatement.setString(6, cert.getUserName());

            callableStatement.execute();
        }
        catch(Exception e)
        {
            this.getLogManager().error(e.getMessage(), e);
        }
        finally
        {
			dataManager.closeResources(null, callableStatement, conn);
        }
    }
}