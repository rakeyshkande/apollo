package com.ftd.framework.dataaccessservices;
import java.util.ArrayList;

public class DataServiceClientProperty 
{
  private String clientName = null;
  private String label = null;
  private ArrayList procedures = null;

  public DataServiceClientProperty()
  {
      this.procedures = new ArrayList();
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public String getLabel()
  {
    return this.label;
  }

  public void setName(String clientName)
  {
    this.clientName = clientName;
  }

  public String getName()
  {
    return this.clientName;
  }

  public void setProcedures(ArrayList procedures)
  {
      this.procedures = procedures;
  }

  public ArrayList getProcedures()
  {
      return this.procedures;
  }
}