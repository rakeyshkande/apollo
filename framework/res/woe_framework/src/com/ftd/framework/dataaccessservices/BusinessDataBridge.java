package com.ftd.framework.dataaccessservices;

import com.ftd.framework.dataaccessservices.*;
import com.ftd.framework.common.valueobjects.*;

public abstract class BusinessDataBridge implements IDataServiceBase 
{
  private DataManager dm = null;
  
  public BusinessDataBridge()
  {}

  public void setDataManager(DataManager dm)
  {
      this.dm = dm;
  }
  public abstract FTDDataResponse execute(FTDDataRequest request);
}