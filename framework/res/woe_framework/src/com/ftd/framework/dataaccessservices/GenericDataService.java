package com.ftd.framework.dataaccessservices;

import java.sql.*;
import java.io.*;
import java.util.Date;
import oracle.jdbc.OracleTypes;
import java.security.MessageDigest;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import org.xml.sax.*;
import org.w3c.dom.*;


public class GenericDataService implements IDataServiceBase
{
    private ResultsetToXmlConverter converter = new ResultsetToXmlConverter();
    private DataServiceClientProperty clientProperty = null;
    private DataManager dataManager = null;
    private Connection conn = null;
    private String driver = "oracle.jdbc.driver.OracleDriver";
	private String database = "jdbc:oracle:thin:@delphi.ftdi.com:1521:DEV1";
	private String user = "ftd_apps";
	private String password = "athens";
    private String type = null;

    public GenericDataService()
    {}

    public void setClientProperty(DataServiceClientProperty clientProperty)
    {
        this.clientProperty = clientProperty;
    }

    public void setDataManager(DataManager dataManager)
    {
        this.dataManager = dataManager;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public FTDDataResponse execute(FTDDataRequest request) throws Exception
    {
        return this.executeRequest(request);
    }

    public FTDDataResponse executeRequest(FTDDataRequest request) throws Exception
    {
        Date timerDate = new Date();
        LogManager lm = new LogManager("com.ftd.oe");
        oracle.xml.parser.v2.XMLDocument xmlDoc = null;
        String xml = null;
        if(this.clientProperty == null)
        {
            throw new Exception("Unable to execute request for \"" + this.type + "\"client type. No such client property defined in daomapping xml");
        }

        try
        {
        this.openConnection();
        xmlDoc = this.processRequest(request);          
        }
        catch(Exception e)
        {
            throw new Exception("Unable to execute Data Request. Specific reason: " + e.toString());
        }
        finally
        {
            this.closeConnection();
        }

        FTDDataResponse response = new FTDDataResponse();
        FTDDataVO vo = new FTDDataVO();
        vo.setData(xmlDoc);
        response.setDataVO(vo);
        response.setStatusMessage("OK");

        lm.debug("GenericDataService: execute " + this.type + " took " + (System.currentTimeMillis() - timerDate.getTime()) + " ms");
        return response;
    }

    private void closeConnection() throws Exception
    {
        try
        {
            if(conn != null)
            {
                conn.close();
            }
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    private void openConnection() throws Exception
    {
        try
        {
            conn = this.dataManager.getConnection();
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private oracle.xml.parser.v2.XMLDocument processRequest(FTDDataRequest request) throws Exception
    {
        DataServiceProcedureProperty procedureProperty = null;
        DataServiceArgumentProperty procedureArgument = null;
        ResultSet rs = null;
        int argIndex = 0;
        String argValue = null;
        String type = null;
        oracle.xml.parser.v2.XMLDocument domDoc = null;

        converter.setRoot(clientProperty.getLabel());
        converter.createDoc();
        for(int i = 0; i < clientProperty.getProcedures().size(); i++)
        {
            procedureProperty = (DataServiceProcedureProperty)clientProperty.getProcedures().get(i);
            converter.setTableName(procedureProperty.getRecordsetLabel());
            converter.setRecordName(procedureProperty.getRecordLabel());
            CallableStatement statement = conn.prepareCall(procedureProperty.getName());

            for(int j = 0; j < procedureProperty.getArguments().size(); j++)
            {
                argIndex = Integer.valueOf(((DataServiceArgumentProperty)(procedureProperty.getArguments().get(j))).getIndex() ).intValue();
                if(request.getArguments().containsKey(((DataServiceArgumentProperty) (procedureProperty.getArguments().get(j))).getName() ))
                {
                    argValue = (String)( request.getArguments().get(((DataServiceArgumentProperty)(procedureProperty.getArguments().get(j))).getName()) );
                }
                else
                {
                    throw new Exception("Argument " + ((DataServiceArgumentProperty)(procedureProperty.getArguments().get(j))).getName() + "missing. Procedure: " + ((DataServiceArgumentProperty)(procedureProperty.getArguments().get(j))).getName());
                }
                type = ((DataServiceArgumentProperty)(procedureProperty.getArguments().get(j))).getType();
                try
                {
                    if(type.equals("string"))
                    {
                        statement.setString(argIndex, argValue);
                    }
                    else if(type.equals("integer"))
                    {
                        if(argValue != null)
                            statement.setInt(argIndex, Integer.valueOf(argValue).intValue());
                        else
                        {
                            //statement.setInt(argIndex, OracleTypes.NULL);
                            statement.setNull(argIndex, Types.VARCHAR);
                        }
                    }
                    else if(type.equals("long"))
                    {
                        statement.setLong(argIndex, Integer.valueOf(argValue).longValue());
                    }
                }
                catch(Exception e)
                {
                    throw e;
                }
            } //close inner for loop

            try
            {
                statement.registerOutParameter(1, OracleTypes.CURSOR);
                statement.execute();
                rs = (ResultSet)statement.getObject(1);
                converter.processResultSet(rs);
                
            }
            catch(Exception e)
            {
                dataManager.closeResources(rs, statement, null);
                throw e;
            }
            finally
            {
                dataManager.closeResources(rs, statement, null);
            }
    
        } //close outter for loop

        domDoc = XPathQuery.createDocument(converter.getXML().toString());
        return domDoc;
    }
}