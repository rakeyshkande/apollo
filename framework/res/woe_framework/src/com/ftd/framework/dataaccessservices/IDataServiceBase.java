package com.ftd.framework.dataaccessservices;

import com.ftd.framework.common.valueobjects.*;

public interface IDataServiceBase 
{
  public void setDataManager(DataManager dm);
  public FTDDataResponse execute(FTDDataRequest request) throws Exception;
}