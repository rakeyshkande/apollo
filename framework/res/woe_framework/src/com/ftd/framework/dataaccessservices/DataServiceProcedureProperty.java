package com.ftd.framework.dataaccessservices;

import java.util.ArrayList;

public class DataServiceProcedureProperty 
{
  private ArrayList arguments = null;
  private String procedure = null;
  private String recordsetLabel = null;
  private String recordLabel = null;
  private String id = null;
  
  public DataServiceProcedureProperty()
  {
    this.arguments = new ArrayList();
  }

  public void setID(String id)
  {
    this.id = id;
  }

  public String getID()
  {
    return this.id;
  }

  public void setRecordsetLabel(String label)
  {
    this.recordsetLabel = label;
  }

  public String getRecordsetLabel()
  {
    return this.recordsetLabel;
  }

  public void setRecordLabel(String label)
  {
    this.recordLabel = label;
  }

  public String getRecordLabel()
  {
    return this.recordLabel;
  }

  public void setName(String procedure)
  {
    this.procedure = procedure;
  }

  public String getName()
  {
    return this.procedure;
  }

  public void setArguments(ArrayList arguments)
  {
    this.arguments = arguments;
  }

  public ArrayList getArguments()
  {
    return this.arguments;
  }
}