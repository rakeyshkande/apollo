package com.ftd.framework.businessservices.serviceprovider;

public class ServiceProperty  
{
	private String description;
	private String className;
	private String poolSize;
	
	public ServiceProperty() 
	{}
	
  public ServiceProperty(String className, String description, String poolSize)
  {
    this.className = className;
    this.description = description;
    this.poolSize = poolSize;
  }
  
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String newDescription) 
	{
		description = newDescription;
	}
	
	public String getClassName() 
	{
		return className;
	}
	
	public void setClassName(String newClassName) 
	{
		className = newClassName;
	}
	
	public String getPoolSize() 
	{
		return poolSize;
	}
	
	public void setPoolSize(String newPoolSize) 
	{
		poolSize = newPoolSize;
	}
}