package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.framework.common.exceptions.*;
import com.ftd.framework.common.valueobjects.*;


public class ServiceManager implements IServiceObserverListener
{
	static private ServiceManager serviceManager = new ServiceManager();
	private LogManager lm = new LogManager("com.ftd.framework.businessservices.serviceprovider.ServiceManager");
	private ServicePool servicePool = null;
	//private BusyServicesPool busyServicePool = null;
	private ServiceObserver serviceObserver = null;
	private boolean suspended = false;
	private ArrayList suspendedServices = null;

	private ServiceManager()
	{
		//busyServicePool =  new BusyServicesPool();
		serviceObserver = ServiceObserver.getInstance();
		serviceObserver.registerListener(this);

        // Get the initial size from config file
        ResourceManager rm = ResourceManager.getInstance();
        String strSize = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "InitialServicePoolSize");
        if(strSize == null)
        {
            strSize = "10";
        }
        int size = Integer.parseInt(strSize);        
		servicePool = new ServicePool(size);
		suspendedServices = new ArrayList();

		this.loadServices();
	}

	public static ServiceManager getInstance()
	{
		return serviceManager;
	}

	private void loadServices()
	{
		this.servicePool.loadPool();
    lm.debug("Services Loaded");
	}

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void notifyServiceTaskCompleted(IService service)
	{
/*
		IService service = (IService)(busyServicePool.getService(serviceID));
		busyServicePool.removeService(serviceID);
*/
    servicePool.returnService(service);

	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public IService getService(String description) throws Exception
	{
		if(this.suspended)
			throw new Exception("ALL Services are currently suspended");

		//check the list of suspended services and make sure you do not return
		//a suspended service


    IService service = null;
		try
		{            
        service = servicePool.leaseService(description);

        if(service == null)
        {
            throw new Exception("Exceeded the MAX_SERVICE in the configuration file");
        }
/*
        else
        {
            busyServicePool.addService(service);
        }
*/
    }
		catch(Exception e)
		{
			lm.error(e);
      throw new Exception("At service manager: " + e.toString());
		}

		return service;
	}

  /**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void reloadServices() throws Exception
	{
		//servicePool.reloadPool();
	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void suspend() throws Exception
	{
		this.suspended = true;
	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void resume() throws Exception
	{
		this.suspended = false;
	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public synchronized void suspendService(String description) throws Exception
	{
		suspendedServices.add(description);
	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public synchronized void resumeService(String description) throws Exception
	{
		suspendedServices.remove(description);
	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public String servicePoolStatus()
	{
		return null;
	}
}