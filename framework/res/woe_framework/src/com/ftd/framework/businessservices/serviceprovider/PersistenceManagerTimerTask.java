package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;

public class PersistenceManagerTimerTask extends TimerTask 
{   
    public PersistenceManagerTimerTask()
    {
    }

    public void run()
    {
        PersistenceManager.cleanUpCache();
    }
}