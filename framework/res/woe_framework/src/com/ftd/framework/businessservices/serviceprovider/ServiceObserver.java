package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;

public class ServiceObserver //implements IServiceObserver
{
	private ArrayList listeners = null;
	private static ServiceObserver serviceObserver = null;
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	private ServiceObserver()
	{
		listeners = new ArrayList();
	}
	
	public static ServiceObserver getInstance()
	{
		if(serviceObserver == null)
			serviceObserver = new ServiceObserver();
		
		return serviceObserver;			
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	synchronized void notifyLock(IService service)
	{}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	synchronized void notifyUnlock(IService service)
	{	
		IServiceObserverListener listener = null;
		
		for(int i = 0; i < listeners.size(); i++)
		{			
			listener = (IServiceObserverListener)(listeners.get(i));
			listener.notifyServiceTaskCompleted(service);			
		} 
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	void registerListener(IServiceObserverListener notificationListener)
	{
		listeners.add(notificationListener);
	}		
}