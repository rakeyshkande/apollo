package com.ftd.framework.businessservices.serviceprovider;

import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;
import com.ftd.framework.common.utilities.FrameworkConstants;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.framework.common.utilities.LogManager;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XSLException;

import org.apache.struts.digester.Digester;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ServiceLoader  
{
  private ArrayList serviceMap = null;
  private ResourceManager rm = null;
  private LogManager lm = new LogManager("com.ftd.framework.businessservices.serviceprovider.ServiceLoader");
  
  public ServiceLoader() 
  {
  	 this.rm = ResourceManager.getInstance();
  	 this.serviceMap = new ArrayList();
  }
  /**
   * Returns all service mappings
   *
   * @return the service mappings
   * @throws ResourceNotFoundException
   * @throws FTDApplicationException
   **/
  public ArrayList getServiceMappings() throws ResourceNotFoundException, FTDApplicationException
  {
    loadServices();
    return serviceMap;
  }

  

  /**
   * Loads all the services as indicated in the services.xml file into 
   * an internal structure
   *  
   **/
  private void loadServices() 
            throws ResourceNotFoundException, FTDApplicationException {	
      InputStream input = null;
      try {
        lm.debug("Loading Services...");

        DocumentBuilder services_db = ResourceManager.getDocumentBuilder(false, false, false,false, false, false);
        URL services_url = this.getClass().getClassLoader().getResource(FrameworkConstants.APPLICATION_SERVICES_FILE_XML);
        XMLDocument servicesDoc = ResourceManager.getDocument(services_db, new FileInputStream(services_url.getFile()));

        NodeList services_nl = servicesDoc.selectNodes("/services/service");

        ServiceProperty serviceProperty = null;
        Element service_property = null;

        // load properties for each service
        for (int i = 0; i < services_nl.getLength(); i++) 
        {
          service_property = (Element) services_nl.item(i);
          serviceProperty = new ServiceProperty(service_property.getAttribute("className"), 
                                                service_property.getAttribute("description"), 
                                                service_property.getAttribute("poolSize"));            

          // add the service to the collection
          lm.debug("Loading service " + service_property.getAttribute("className"));
          serviceMap.add(serviceProperty);
        }        

      } catch (XSLException xe){
        lm.error(xe);
        throw new FTDApplicationException( xe );
      } catch (SAXException se){
        lm.error(se);
        throw new FTDApplicationException( se );
      } catch (ParserConfigurationException pce){
        lm.error(pce);
        throw new FTDApplicationException( pce );
      } catch (IOException ioe){
        lm.error(ioe);
        throw new ResourceNotFoundException(ioe);
      } finally {
      }
  }  


}