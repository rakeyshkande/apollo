package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;

public class BusyServicesPool extends HashMap
{	
  /**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public BusyServicesPool()
	{}
	
	/**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void addService(IService service)
	{
		this.put(service.getID(), service); 	
	}
		
	/**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void removeService(String id)
	{
		this.remove(id);
	}
	
	/**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public IService getService(String id)
	{		
		return (IService)(this.get(id));
	}  	
}