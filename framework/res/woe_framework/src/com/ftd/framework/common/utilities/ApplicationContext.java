package com.ftd.framework.common.utilities;

import java.util.Hashtable;
import java.util.Enumeration;
import java.net.InetAddress;

/**
 * The context of the application. It can be used to store objects
 * that common across multiple requests.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
 
public class ApplicationContext 
{
  private Hashtable context = new Hashtable();
  private static ApplicationContext APPLICATIONCONTEXT;

 /**
  * The private constructor
  *
  * @author Anshu Gaind   
  **/
  private ApplicationContext()
  {
    try 
    {
        context.put("HOST_NAME", InetAddress.getLocalHost().getHostName());
    }
    catch(Exception e)
    {
        e.printStackTrace();
    }
  }
  
 /**
  * The static initializer for the application context. It ensures that
  * at a given time there is only a single instance of the application 
  * context.
  *
  * @return the application context
  * @author Anshu Gaind
  **/
  public static ApplicationContext getInstance()
  {
    if ( APPLICATIONCONTEXT == null )
    {
      APPLICATIONCONTEXT = new ApplicationContext();
      return APPLICATIONCONTEXT;
    }
    else
    {
      return APPLICATIONCONTEXT;
    }
  }

  /**
   * Returns the attribute with the given name, or null if there is
   * no attribute by that name.
   *
   * @param name the name of the attribute
   * @return an object containing the value of the attribute
   * @author Anshu Gaind
   **/
  public Object getAttribute(String name)
  {
    return context.get(name);
  }

  /**
   * Set the attribute with the given name in the application context.
   *
   * @param name the name of the attribute
   * @param object the object corresponding to the attribute name
   * @author Anshu Gaind
   **/
  public void setAttribute(String name, Object object)
  {
    context.put(name, object);
  }

  /**
   * Removes the specified attribute from the application context.
   *
   * @param name the name of the attribute
   * @author Anshu Gaind
   **/
  public void removeAttribute(String name)
  {
    context.remove(name);
  }

  /**
   * Returns an enumeration of all attributes in the application context.
   *
   * @return an enumeration of all attributes
   * @author Anshu Gaind
   **/
  public Enumeration getAttributeNames()
  {
    return context.elements();
  }

}
