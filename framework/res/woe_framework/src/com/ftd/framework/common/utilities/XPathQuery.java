package com.ftd.framework.common.utilities;

import java.util.*;
import java.net.URL;
import com.ftd.framework.common.exceptions.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import org.w3c.dom.*;
import java.io.*;
import java.util.*;
import org.xml.sax.InputSource;

public class XPathQuery
{
    //private oracle.xml.parser.v2.XMLDocument document = null;
    private static LogManager lm = new LogManager("com.ftd.oe");

    public XPathQuery()
    {
    }

	/**
	 * Returns a node list that has the results of the xpath query
	 *
	 * @param doc the oracle.xml.parser.v2.XMLDocument
	 * @param xpath the xpath query
     */
    public NodeList query(XMLDocument document, String xpath) throws Exception
    {
        Date startDate = new Date();
        if ( (document == null) || (xpath == null) || (xpath.equals("")) )  {
		            return null;
        }

        NodeList nodeList = null;

        nodeList = document.selectNodes(xpath);

        long crTime = (System.currentTimeMillis() - startDate.getTime());
        if(crTime > 1000)
        {
            lm.debug("XML Doc search time: " + crTime);
            lm.debug("XPath = " + xpath);
        }

        return nodeList;
    }

    /**
     * @deprecated
     */
    public NodeList query(String xml, String xpath) throws Exception
    {
        NodeList nodeList = null;
        XMLDocument document = null;

        try
        {
            document = createDocument(xml);
            nodeList = this.query(document, xpath);
        }
        catch(Exception e)
        {
            throw new Exception("Unable to execute Xpath Query.Specific reason: " +  e.toString());
        }

        return nodeList;
    }

    /**
     * Creates an oracle.xml.parser.v2.XMLDocument from a well formatted xml string
	 *
     * @param xmlString the xml string
     * @deprecated
     */
    public static XMLDocument createDocument(String xml) throws Exception
    {
        Date startDate = new Date();

		if (xml == null || xml.equals(""))
		{
          return null;
      	}

        DOMParser parser = null;
        XMLDocument document = null;
        try
        {
//            lm.debug("Creating XML document...");
            parser = new DOMParser();
            parser.parse( new InputSource( new StringReader(xml) ) );
            document = parser.getDocument();
//        }
//        catch(XMLParseException xmle)
//        {            
            // try to re-parse after encoding characters
//            try
//            {
//                lm.error("Reparsing document after character encoding: " + xml);
//                xml = XMLUtility.encodeChars(xml);
//                parser = new DOMParser();
//                parser.parse( new InputSource( new StringReader(xml) ) );
//                document = parser.getDocument();
//            }
//            catch(Exception e)
//            {
//                lm.error("Could not parse document... " + xml);
//                throw new Exception(e.toString());
//            }
        }
        catch(Exception e)
        {
            lm.error("Could not parse document... " + xml);
            throw new Exception(e.toString());
        }

        long crTime = (System.currentTimeMillis() - startDate.getTime());
        if(crTime > 1000)
        {
            lm.debug("XML Doc creation time: " + crTime);
            lm.debug("XML Doc = " + xml.substring(0, 256));
        }

        return document;
    }

    public static String getXMLString(XMLDocument doc)  throws Exception
    {
        if(doc == null)
        {
            return null;
        }
        Date startDate = new Date();
        StringWriter s = new StringWriter();
        doc.print(new PrintWriter(s));
        long crTime = (System.currentTimeMillis() - startDate.getTime());
        if(crTime > 1000)
        {
            lm.debug("XML Doc getXMLString: " + crTime);
        }
        return s.toString();
    }
}