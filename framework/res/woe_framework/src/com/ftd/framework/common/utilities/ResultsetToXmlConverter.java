package com.ftd.framework.common.utilities;

import java.sql.*;

public class ResultsetToXmlConverter 
{
	public final static String PROLOG = "<?xml version=\"1.0\"?>\n\n";
	public final static String OPEN_START = "<";
	public final static String SIMPLE_CLOSE = "/>";
	public final static String OPEN_END = "</";
	public final static String CLOSE = ">";
	public final static String NEWLINE = "\n";
	public final static String INDENT = "\t";
  
  private LogManager lm = new LogManager("com.ftd.framework.common.utilities.ResultsetToXmlConverter");
	
	private String table = new String("table");
	private String recordname = new String("record");
	private String doctypeline = null;
    private String doctype = null;
    private String root = null;
    private StringBuffer xml = null; 
	
	public ResultsetToXmlConverter()
	{}


    public void setRoot(String root)
    {
        this.root = root;
    }
	
	public void setTableName(String s) 
	{
		this.table = s;
	}

  public void setRecordName(String s) 
	{
		this.recordname = s;
	}
	
	public void createDoc() 
	{
      xml = new StringBuffer(); 
	  xml.append(NEWLINE + OPEN_START + this.root + CLOSE);
	}

	public StringBuffer getXML()
    {
      xml.append(NEWLINE + OPEN_END + this.root + CLOSE);
      return this.xml;
    }

	public void processResultSet(ResultSet rs) 
	{
  
    xml.append(NEWLINE + OPEN_START + this.table + CLOSE);
	String data;
	
	try 
	{
		ResultSetMetaData rsmeta = rs.getMetaData();
		int nfields = rsmeta.getColumnCount();
		Object temp = null;
		while (rs.next()) 
		{
			xml.append(NEWLINE + INDENT + OPEN_START + this.recordname + " ");
			for (int i = 1; i < nfields + 1; i++) 
			{
				xml.append(rsmeta.getColumnName(i) + "=\"");
                temp = rs.getObject(i);
                if(temp != null)
                { 
                    data = XMLUtility.encodeChars(temp.toString());	
                    //data = temp.toString();
                    xml.append(data + "\" ");
                }
                else
                {
                    data ="";
                    xml.append(data + "\" ");
                }
			}
			xml.append(SIMPLE_CLOSE);
		}
	} 
	catch (Exception e) 
	{
		lm.error(e);
	}
  xml.append(NEWLINE + OPEN_END + this.table + CLOSE + NEWLINE);
	} 
}