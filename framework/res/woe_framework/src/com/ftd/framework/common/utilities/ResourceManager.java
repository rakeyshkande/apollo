// Unit Tested
package com.ftd.framework.common.utilities;

import com.ftd.framework.common.exceptions.FTDApplicationException;
import com.ftd.framework.common.exceptions.ResourceNotFoundException;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.net.URL;

import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.xml.parser.v2.XMLDocument;
import oracle.xml.parser.v2.XSLException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

 /**
  * Responsible for obtaining all resources external to the application.
  * 
  * @author Anshu Gaind
  * @version 1.0 
  **/
public class ResourceManager
{
  private HashMap realms = new HashMap();
  private String realmName;
  private LogManager logger = new LogManager("com.ftd.framework.common.utilities.ResourceManager");

  private static ResourceManager RESOURCEMANAGER = new ResourceManager();
  
 /**
  * The private constructor
  *
  * @author Anshu Gaind
  **/
  private ResourceManager() {
    super();
    try {
      loadRealms();
    } catch (Exception e){
      logger.error(e);
    }
  }
  
 /**
  * The static initializer for the resource manager. It ensures that
  * at a given time there is only a single instance 
  *
  * @return the resource manager
  * @author Anshu Gaind
  **/
  public static ResourceManager getInstance() {
      return RESOURCEMANAGER;
  }


  /**
   * Finds a resource with a given name. It returns a URL to the resource
   *
   * @param name the name of the resource
   * @return a URL to the resource
   * @throws ResourceNotFoundException
   * @author Anshu Gaind
   **/
  public URL getResource(String name) throws ResourceNotFoundException {
    Class class_v = this.getClass();
    ClassLoader classLoader = class_v.getClassLoader();

    URL url = classLoader.getResource(name);
    logger.debug("Getting Resource:: " + name);
    if (url == null){
      throw new ResourceNotFoundException(name + " not found ");
    }
    return url;
  }

  /**
   * Finds a resource with a given name. It returns the resource as an 
   * InputStream
   *
   * @param name the name of the resource
   * @return the resource as a stream.
   * @throws ResourceNotFoundException
   * @author Anshu Gaind
   **/
  public InputStream getResourceAsStream(String name) throws ResourceNotFoundException{
    Class class_v = this.getClass();
    ClassLoader classLoader = class_v.getClassLoader();

    InputStream is = classLoader.getResourceAsStream(name);
    logger.debug("Getting Resource:: " + name + " as Stream");
    if (is == null){
      throw new ResourceNotFoundException(name + " not found ");
    }
    return is;
  }

  /**
   * Returns an implementation of the interface that is passed in as an argument.
   * The imterface name should be mapped to the implementation in the interface
   * mapping file for the application. The implementation should have a no-args
   * constructor.
   *
   * @param interfaceName the name of the interface as set in interface mappings
   * @return the implementation of the interface
   * @throws ResourceNotFoundException
   **/
   public Object getImplementation(String interfaceName) throws ResourceNotFoundException {
      try{
        String implementationName = null;
        Class implementationClass = null;
        Object implementationObject = null;
        implementationName =
          getProperty(FrameworkConstants.APPLICATION_INTERFACE_MAPPING_REALM_KEY,
                      interfaceName);

        if (implementationName != null){
          implementationClass = Class.forName(implementationName);
          implementationObject = implementationClass.newInstance();
        }
        return implementationObject;
      } catch(ClassNotFoundException cnfe){
          logger.error(cnfe);
          throw new ResourceNotFoundException(cnfe);
      } catch(IllegalAccessException iae){
          logger.error(iae);
          throw new ResourceNotFoundException(iae);
      } catch (InstantiationException ie){
          logger.error(ie);
          throw new ResourceNotFoundException(ie);
      }
   }
   
   
  /**
   * Returns the value of a property from a properties file. 
   * Each property file is set up as a realm in the realm.properties
   * file. 
   *
   * @param realmName the realm name or the key name of the properties file as set up in realm.properties
   * @param propertyName the key within that realm, for which the value needs to be obtained.
   * @return the value of the specified key, and within a realm
   **/
  public String getProperty(String realmName, String propertyName){
    String rv = null;
    // returns a property object
    Property property = getPropertyObject(realmName, propertyName);
    rv = property.getValue();
    
    if (rv == null) 
    {
        rv = "";
    }
    
    return rv;
    
  }


  /**
   * Returns the property from a properties file. 
   * Each property file is set up as a realm in the realm.properties
   * file. 
   *
   * @param realmName the realm name or the key name of the properties file as set up in realm.properties
   * @param propertyName the key within that realm, for which the value needs to be obtained.
   * @return the property object  of the specified key, and within a realm
   **/
  public Property getPropertyObject(String realmName, String propertyName){
    Property property = null;
    try { 
        // returns a property object
        property = ((Realm)realms.get(realmName)).getProperty(propertyName);

    } catch (Exception e){
        logger.error(e);
    }
    
    
    return property;
  }


  
  /**
   * Loads all the realm as indicated in the realm.properies file into 
   * an internal structure
   *  
   **/
  private synchronized void loadRealms() 
            throws ResourceNotFoundException, FTDApplicationException {	
      InputStream input = null;
      try {
        logger.debug("Loading Realms...");

        DocumentBuilder realms_db = this.getDocumentBuilder(false, false, false,false, false, false);
        URL realms_url = this.getClass().getClassLoader().getResource(FrameworkConstants.APPLICATION_REALM_FILE_XML);
        XMLDocument realmsDoc = this.getDocument(realms_db, new FileInputStream(realms_url.getFile()));

        NodeList realms_nl = realmsDoc.selectNodes("/properties/property");
        Element realms_property = null;

        Realm realm = null;
        DocumentBuilder realm_db = null;
        XMLDocument realmDoc = null;
        NodeList realm_nl = null;
        Element realm_property = null;
        URL realm_url = null;

        // load properties for each realm
        for (int i = 0; i < realms_nl.getLength(); i++) 
        {
          realms_property = (Element) realms_nl.item(i);
          realm = new Realm(realms_property.getAttribute("name"));
          logger.debug("Loading Realm " + realms_property.getAttribute("name"));

          realm_url = this.getClass().getClassLoader().getResource(realms_property.getAttribute("value"));
          realm_db = this.getDocumentBuilder(false, false, false,false, false, false);
          realmDoc = this.getDocument(realm_db, new FileInputStream(realm_url.getFile()));
          realm_nl = realmDoc.selectNodes("/properties/property");

          // for each property within a realm
          for (int j = 0; j < realm_nl.getLength(); j++) 
          {
            realm_property = (Element) realm_nl.item(j);
            realm.setProperty(new Property(realm_property.getAttribute("name"), realm_property.getAttribute("value"), realm_property.getAttribute("isThreadSafe")));            
            logger.debug("Loading property " + realm_property.getAttribute("name"));
          }
          // add the realm to the collection
          realms.put(realm.getName(), realm);
        }        

      } catch (XSLException xe){
        logger.error(xe);
        throw new FTDApplicationException(xe );
      } catch (SAXException se){
        logger.error(se);
        throw new FTDApplicationException(se );
      } catch (ParserConfigurationException pce){
        logger.error(pce);
        throw new FTDApplicationException(pce );
      } catch (IOException ioe){
        logger.error(ioe);
        throw new ResourceNotFoundException(ioe);
      } finally {
      }
  }  

   /**
    * Resets all the realms
    */
    public synchronized void resetRealms(){
      realms.clear();
      try {
        loadRealms();
      } catch (Exception e){
        logger.error(e);
      }
    }



    /**
     * Returns a document builder based on the parameters specified
     * 
     * @param coalescing Specifies that the parser produced by this code will convert CDATA nodes to Text nodes and append it to the adjacent (if any) text node.
     * @param expandEntityReferences Specifies that the parser produced by this code will expand entity reference nodes.
     * @param ignoringComments Specifies that the parser produced by this code will ignore comments.
     * @param ignoringElementContentWhitespace Specifies that the parsers created by this factory must eliminate whitespace in element content (sometimes known loosely as 'ignorable whitespace') when parsing XML documents (see XML Rec 2.10).
     * @param namespaceAware Specifies that the parser produced by this code will provide support for XML namespaces.
     * @param validating Specifies that the parser produced by this code will validate documents as they are parsed.
     * @exception ParserConfigurationException 
     * @return the document builder
     */
    public static DocumentBuilder getDocumentBuilder(boolean coalescing,
                                                     boolean expandEntityReferences,
                                                     boolean ignoringComments,
                                                     boolean ignoringElementContentWhitespace, 
                                                     boolean namespaceAware, 
                                                     boolean validating) 
        throws ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setCoalescing(coalescing);
        factory.setExpandEntityReferences(expandEntityReferences);
        factory.setIgnoringComments(ignoringComments);
        factory.setIgnoringElementContentWhitespace(ignoringElementContentWhitespace);
        factory.setNamespaceAware(namespaceAware);
        factory.setValidating(validating);

        return factory.newDocumentBuilder();    
    }

    /**
     * Returns a DOM document for the input stream using the specified document builder.
     * 
     * @param docBuilder the document builder
     * @param is the input stream
     * @exception IOException 
     * @exception SAXException 
     * @exception ParserConfigurationException 
     * @return a DOM document for the input stream.
     */
    public static XMLDocument getDocument(DocumentBuilder docBuilder, InputStream is) throws IOException, SAXException, ParserConfigurationException
    {
        XMLDocument doc = null;
        BufferedInputStream bis = null;
        
        try  
        {
            bis = new BufferedInputStream(is);
            InputSource inputSource = new InputSource(is);      
            doc = (XMLDocument) docBuilder.parse(inputSource);      
        } 
        finally  
        {
            if (bis != null)  
            {
                bis.close();
            }      
        }
        
        return doc;            
    }
}
