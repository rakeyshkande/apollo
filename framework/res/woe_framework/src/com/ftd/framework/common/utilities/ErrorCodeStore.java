// Unit Tested
// need rework on findValue(1, 2);
package com.ftd.framework.common.utilities;

import java.util.*;
import java.io.*;
import com.ftd.framework.common.exceptions.*;



/**
 * This class loads all error codes from the error string file and has 
 * methods to convert error codes to error strings.
 * 
 * @author: Anshu Gaind
 * @version 1.0 
 * @deprecated
 */
 
public class ErrorCodeStore {
	
	protected static ResourceBundle ERRORLISTBUNDLE;
	private static ErrorCodeStore ERRORCODESTORE;
	private static FileInputStream FIS;

	
  /**
   * Ensures a singleton and loads the error codes
   *
   */
  private ErrorCodeStore() {
    super();
  }

  /**
  *  Used to get the only instance of ErrorCodeStore.
  *
  * @return the error code store object
  */
  public static ErrorCodeStore getInstance() {
      if (ERRORCODESTORE == null) {
        ERRORCODESTORE = new ErrorCodeStore();
        loadErrorCodes();
        return ERRORCODESTORE;
      } else {
        return ERRORCODESTORE;
      }	
  }

  /**
   * Returns the error message associated with an error code
   *
   * @param msgCode the error code
   * @return the error message
   **/
  public String findValue(int msgCode)  {
    StringBuffer prtStr = new StringBuffer(msgCode);
    prtStr.append('|');

    try {
      if (ERRORLISTBUNDLE != null) {
        prtStr.append(ERRORLISTBUNDLE.getString(Integer.toString(msgCode)));
      } else {
        prtStr.append("Unknown message code(no ResourceBundle)");
        prtStr.append(msgCode);
      }
    } catch (MissingResourceException mre) {
      prtStr.append("Unknown message code: ");
      prtStr.append(msgCode);
    }
	  return prtStr.toString();
  }

  /**
   * Returns the custom error message associated with an error code.
   * The parameters passed in are inserted into the error message and 
   * in the same order.
   * 
   * @param msgCode the error code
   * @param args[] the error message parameters
   * @return the custom error message
   **/
  public String findValue(int msgCode, String args[])  {
    StringBuffer prtStr = new StringBuffer(msgCode);
    prtStr.append('|');

    try {
      if (ERRORLISTBUNDLE != null) {
        prtStr.append(ERRORLISTBUNDLE.getString(Integer.toString(msgCode)));
      } else {
        prtStr.append("Unknown message code(no ResourceBundle)");
        prtStr.append(msgCode);
      }
    } catch (MissingResourceException mre) {
      prtStr.append("Unknown message code: ");
      prtStr.append(msgCode);
    }
	
    //append the arguments if there are any.	
    if (args != null) {
      for (int i = 0; i < args.length; i++) {
        prtStr.append('|');
        prtStr.append(args[i]);
      }
    }
    return prtStr.toString();
  }

  /**
   * Loads the error codes.
   * 
   */
  private static void loadErrorCodes() {
    ResourceManager resourceManager = ResourceManager.getInstance();
    String fileLoc = resourceManager.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, FrameworkConstants.APPLICATION_ERROR_STRINGS_KEY);

    try {
      ERRORLISTBUNDLE = ResourceBundle.getBundle(fileLoc);		
    } catch (Exception ioe) {
      //printing to System.err since we cannot use LogManager for this operation
      System.out.println("Could not load error codes in ErrorCodeStore");
    }
  }

}
