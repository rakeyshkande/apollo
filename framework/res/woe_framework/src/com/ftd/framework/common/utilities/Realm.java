package com.ftd.framework.common.utilities;
import java.util.*;
/**
 * Represents a realm
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class Realm  {
  private HashMap realmProperties = new HashMap();
  private String realmName;

  /**
   * Constructor
   * 
   * @param realmName the realm name
   **/
  public Realm(String realmName){
    this.realmName = realmName;
  }

  /**
   * Returns the realm name
   *
   * @return the realm name
   **/
  public String getName(){
    return  realmName;
  }

  /**
   * Returns a property from the realm
   *
   * @param key the key for the property
   * @return the property
   **/
  public Property getProperty(String key) {
    if (realmProperties.containsKey(key)) {
        return (Property) realmProperties.get(key);
    } else {
        // do not throw exception
        return null;
    }
  }

  /**
   * Sets a property in the realm
   *
   * @param property the property
   **/
  public void setProperty(Property property){
    realmProperties.put(property.getName(), property);
  }
}