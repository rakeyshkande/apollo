package com.ftd.framework.common.exceptions;

/**
 * Wraps any exception that is thrown when a resource like file etc is not found
 *
 * @author Anshu Gaind
 * @version 1.0
 **/
public class ResourceNotFoundException extends FTDApplicationException  {

  public ResourceNotFoundException()
  {
    super();
  }


 public ResourceNotFoundException(String message)
  {
    super(message);
  }

/**
 * Wraps the original exception object
 */
  public ResourceNotFoundException(Throwable t)
  {
    super(t);
  }


/**
 * Used when the exception does not need a custom message with it.
 *
 * @param errorCode the error code
 * @author Anshu Gaind
 * @deprecated
 */
  public ResourceNotFoundException(int errorCode){
    super();
  }

/**
 * Used when the error code has an associated error message.
 *
 * @param errorCode the error code
 * @param args[] the arguments that go in the custom error message
 * @author Anshu Gaind
 * @deprecated
 **/
  public ResourceNotFoundException(int errorCode, String[] args){
    super();
  }

/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 * @deprecated
 **/
  public ResourceNotFoundException(int errorCode, Throwable nestedThrowable){
    super(nestedThrowable);
  }


/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param args[] the arguments that go in the custom error message
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 * @deprecated
 **/
  public ResourceNotFoundException(int errorCode, String[] args, Throwable nestedThrowable){
    super(nestedThrowable);
  }



}
