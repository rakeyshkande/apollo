package com.ftd.framework.common.valueobjects;


/**
 * @author Sasha Secivan
 * @version 1.0 
 **/
public class FTDDataResponse implements IValueObject
{
	private FTDDataVO vo = null; 
	private String message = null;
	
	public FTDDataResponse() 
	{}
	
	public void setStatusMessage(String message)
	{
		this.message = message; 	
	}
	
	public String getStatusMessage()
	{
		return this.message;	
	}
	
	public void setDataVO(FTDDataVO vo)
	{
		this.vo = vo;
	}
	
	public FTDDataVO getDataVO()
	{
		return this.vo;
	}
}