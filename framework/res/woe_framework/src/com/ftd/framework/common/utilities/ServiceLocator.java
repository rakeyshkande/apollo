package com.ftd.framework.common.utilities;

import javax.naming.InitialContext;
import javax.naming.NamingException;
/**
 * This class abstracts access to the initial context
 *
 * @author Anshu Gaind
 * @version 1.0 
 * @deprecated
 **/
 
public class ServiceLocator  {

  private static ServiceLocator SERVICELOCATOR = new ServiceLocator();

 /**
  * The private constructor
  *
  * @author Anshu Gaind
  **/
  private ServiceLocator() {
    super();
  }
  
 /**
  * The static initializer for the service locator. It ensures that
  * at a given time there is only a single instance 
  *
  * @return the service locator
  * @author Anshu Gaind
  **/
  public static ServiceLocator getInstance()  
  {
      return SERVICELOCATOR;
  }
  

  /**
   * Returns the initial context to the naming server
   *
   * @param contextKey the context key
   * @return the initial context
   * @throws NamingException if the context key is not set up or is null
   **/
  public InitialContext getContext(String contextKey) throws NamingException {
    return new InitialContext();
  }


  /**
   * Returns the initial context to the naming server
   *
   * @return the initial context
   * @throws NamingException if the context key is not set up or is null
   **/
  public InitialContext getContext() throws NamingException {
    return new InitialContext();
  }


}
