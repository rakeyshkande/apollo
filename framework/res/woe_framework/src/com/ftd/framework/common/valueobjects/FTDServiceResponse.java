package com.ftd.framework.common.valueobjects;


/**
 *
 * @author Sasha Secivan
 * @version 1.0 
 **/
 
public class FTDServiceResponse implements IValueObject 
{	 
	private FTDSystemVO vo = null; 
	private String persistanceID = null; 
  private String message = null;

	public FTDServiceResponse()
	{}

  public void setStatusMessage(String message)
	{
		this.message = message; 	
	}
	
	public String getStatusMessage()
	{
		return this.message;	
	}
	
	public void setSystemValueObject(FTDSystemVO vo)
	{
		this.vo = vo;	
	}
	
	public FTDSystemVO getSystemValueObject()
	{
		return this.vo;	
	} 
		
	public boolean isServicePersistent()
	{
		if(this.persistanceID == null)
			return false;
			
		return true;	
	}
	
	public void setPersistanceID(String persistanceID)
	{
		this.persistanceID = persistanceID;	
	}
	
	public String getPersistanceID()
	{
		return this.persistanceID;
	}
}
