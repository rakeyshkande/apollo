package com.ftd.framework.common.valueobjects;

import java.util.*;

/**
 * @author Sasha Secivan
 * @version 1.0 
 **/
 
public class FTDCommand implements IValueObject 
{	
	private String command = null;
	
	public FTDCommand()
	{}
	
	public void setCommand(String command)
	{
		this.command = command;
	}
	
	public String getCommand()
	{
		return this.command;	
	}
}