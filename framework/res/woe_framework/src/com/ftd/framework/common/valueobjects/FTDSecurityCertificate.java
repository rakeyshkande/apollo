package com.ftd.framework.common.valueobjects;

//import java.util.*;

/**
 * @author Sasha Secivan
 * @version 1.0 
 **/
 
public class FTDSecurityCertificate implements IValueObject 
{	
	private String firstName = null;
	private	String lastName  = null;
	private	String sessionId = null;
    private FTDRole role;
    private String callCenterID = null;
    private String userName = null;
	
	public FTDSecurityCertificate()
	{}
	
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getFirstName()
	{
		return this.firstName; 
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getLastName()
	{
		return this.lastName; 
	}

	public void setSessionId(String sessionId)
	{
		this.sessionId = sessionId;
	}
	
	public String getSessionId()
	{
		return this.sessionId; 
	}

    public FTDRole getRole()
    {
        return role;
    }

    public void setRole(FTDRole newRole)
    {
        role = newRole;
    }

    public String getCallCenterID()
    {
        return callCenterID;
    }

    public void setCallCenterID(String newCallCenterID)
    {
        callCenterID = newCallCenterID;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String newUserName)
    {
        userName = newUserName;
    }
}