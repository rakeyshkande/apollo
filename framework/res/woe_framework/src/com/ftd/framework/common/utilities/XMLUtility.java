package com.ftd.framework.common.utilities;

public class XMLUtility 
{
  public XMLUtility()
  {
  }


   private static String escapeChar(char c)
	{
		switch(c)
		{
			case('<')  : return "&lt;";
			case('>')  : return "&gt;";  
			case('&')  : return "&amp;"; 
			case('\'') : return "&#39;"; 
			case('"') : return "&quot;";                         
		} 
		return null;    
	}
	
	/**
   * @deprecated
   */
	public static String encodeChars(String string)
	{
    if ( string == null || string.equals("") )  
    {
        return "";
    }
    
		char[] characters = string.toCharArray();
		StringBuffer encoded = new StringBuffer();
		String escape;
		
		for(int i = 0;i<string.length();i++)
		{
			escape = escapeChar(characters[i]);
			if(escape == null) 
      {
        encoded.append(characters[i]);
      }
			else 
      {
         encoded.append(escape);
      }
		}
		
		return encoded.toString();
	}
}