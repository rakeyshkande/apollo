package com.ftd.framework.common.valueobjects;

public class FTDPersistentObjectDecorater implements IValueObject
{
    private FTDPersistentObject persistentObject;
    private long maxAge = 0L;

    public FTDPersistentObjectDecorater()
    {
    }

    public FTDPersistentObject getPersistentObject()
    {
        return persistentObject;
    }

    public void setPersistentObject(FTDPersistentObject newPersistentObject)
    {
        persistentObject = newPersistentObject;
    }

    public long getMaxAge()
    {
        return maxAge;
    }

    public void setMaxAge(long newMaxAge)
    {
        maxAge = newMaxAge;
    }

    
}