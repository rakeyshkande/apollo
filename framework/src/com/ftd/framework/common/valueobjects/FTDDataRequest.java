package com.ftd.framework.common.valueobjects;

import java.util.*;

/**
 * @author Sasha Secivan
 * @version 1.0 
 **/
public class FTDDataRequest implements IValueObject
{
   private String clientID = null;
   private HashMap arguments = null;

   public FTDDataRequest(String clientID) 
   {
      this.clientID = clientID;
      this.arguments = new HashMap();
   }

   public FTDDataRequest()
   {
      this.arguments = new HashMap();
   }
 
   public void addArgument(String name, String value)
   {
   		this.arguments.put(name, value);
   }
   
   public String getArgument(String name)
   {
   		return (String)(this.arguments.get(name));
   }

   public HashMap getArguments()
   {
      return this.arguments;
   }
    
   public void setClientID(String clientID)
   {
   		this.clientID = clientID;
   }
   
   public String getClientID()
   {
   		return this.clientID;
   }
}
