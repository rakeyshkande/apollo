package com.ftd.framework.common.valueobjects;


/**
 *
 * @author Sasha Secivan
 * @version 1.0 
 **/
 
public class FTDServiceRequest implements IValueObject 
{

	public final static int NONE = 0;
	public final static int CREATE_OBJECT = 1;
	public final static int USE_OBJECT = 2;
	public final static int REMOVE_OBJECT = 3;	
	private String service = null;
	private FTDCommand command = null;
	private FTDArguments arguments = null; 
	private String persistenceID = null; 
	private int persistence = 0;

	public FTDServiceRequest()
	{
		arguments = new FTDArguments();
		command = new FTDCommand();
	}
	
	public void setPersistence(int type)
	{
		this.persistence = type; 	
	}
	
	public int getPersistenceType()
	{		 
		return this.persistence;	
	}
	
	public boolean hasPersistanceID()
	{
		if(this.persistenceID == null)
			return false;
	
		return true;	
	}
		
	public void setPersistenceID(String persistenceID)
	{
		this.persistenceID = persistenceID;	
	}
	
	public String getPersistenceID()
	{
		return this.persistenceID;
	}	
	
	public void setServiceDescription(String service)
	{
		this.service = service;	
	}
	
	public String getServiceDescription()
	{
		return this.service;	
	}
	
	public void setCommand(String command)
	{		
		this.command.setCommand(command);
	}
	
	public FTDCommand getCommand()
	{
		return this.command;
	}
	
	public void addArgument(String name, Object value)
	{
		//MAYBE ONLY ALLOW STRINGS!!!!!!!!

		this.arguments.put(name, value);	
	}
	
	public FTDArguments getArguments()
	{
		return this.arguments;	
	}
}