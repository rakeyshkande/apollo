package com.ftd.framework.common.valueobjects;


public class FTDPersistentObject implements IValueObject
{

  private long timeStamp = 0;
  private String guid = null;
  private String hostName;

  public FTDPersistentObject()
  {}

  public void setTimeStamp(long timeStamp)
  {
    this.timeStamp = timeStamp;
  }

  public long getTimeStamp()
  {
    return this.timeStamp;
  }

  public void setGUID(String guid)
  {
    this.guid = guid;
  }

  public String getGUID()
  {
    return this.guid;
  }

  public String getHostName()
  {
    return hostName;
  }

  public void setHostName(String newHostName)
  {
    hostName = newHostName;
  }
}