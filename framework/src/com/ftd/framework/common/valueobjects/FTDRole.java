package com.ftd.framework.common.valueobjects;

import java.util.List;

public class FTDRole implements IValueObject
{
    private String roleID;
    private String description;
    private String lastUpdateUser;
    private String lastUpdateDate;
    private List function;

    public FTDRole()
    {
    }

    public String getRoleID()
    {
        return roleID;
    }

    public void setRoleID(String newRoleID)
    {
        roleID = newRoleID;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }

    public String getLastUpdateUser()
    {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(String newLastUpdateUser)
    {
        lastUpdateUser = newLastUpdateUser;
    }

    public String getLastUpdateDate()
    {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String newLastUpdateDate)
    {
        lastUpdateDate = newLastUpdateDate;
    }

    public List getFunction()
    {
        return function;
    }

    public void setFunction(List newFunction)
    {
        function = newFunction;
    }
}