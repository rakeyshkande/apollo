package com.ftd.framework.common.valueobjects;

/**
 * Value objects are data containers that can be passed to the service layer, 
 * and are returned from the service layer. All value objects will extend this base class.
 *
 * @author Sasha Secivan
 * @version 1.0 
 **/
 
public class FTDSystemVO implements IValueObject 
{
	private String dataType = null;
	private Object xml = null;
	
	public FTDSystemVO()
	{}
	
	public void setDataType(String dataType)
	{
		this.dataType = dataType;			
	}
	
	public String getDataType()
	{
		return this.dataType;	
	}
	
	public void setXML(Object xml)
	{
		this.xml = xml;			
	}
	
	public Object getXML()
	{
		return this.xml;	
	}
	
}
