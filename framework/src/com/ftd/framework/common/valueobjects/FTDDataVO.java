package com.ftd.framework.common.valueobjects;


/**
 * @author Sasha Secivan
 * @version 1.0 
 **/
public class FTDDataVO implements IValueObject
{	
  private Object data = null;

	public FTDDataVO() 
	{}

  public void setData(Object data)
  {
    this.data = data;
  }

  public Object getData()
  {
    return this.data;
  }

  public String toString()
  {
    return this.data.toString();
  }
}	