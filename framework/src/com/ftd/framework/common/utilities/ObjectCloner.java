package com.ftd.framework.common.utilities;


import java.io.*;
import java.util.*;
import java.awt.*;

public class ObjectCloner
{

   private ObjectCloner(){}

   synchronized static public Object deepCopy(Object oldObj) throws Exception
   {
      ObjectOutputStream oos = null;
      ObjectInputStream ois = null;
      
      try
      {
         ByteArrayOutputStream bos = 
               new ByteArrayOutputStream(); 
         oos = new ObjectOutputStream(bos); 

         oos.writeObject(oldObj);  
         oos.flush();             
         ByteArrayInputStream bin = 
               new ByteArrayInputStream(bos.toByteArray()); // E
         ois = new ObjectInputStream(bin);                  // F

         return ois.readObject(); 
      }
      catch(Exception e)
      {
         throw(e);
      }
      finally
      {
         oos.close();
         ois.close();
      }
   }
   
}

