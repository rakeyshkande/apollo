package com.ftd.framework.common.utilities;

import java.util.*;
import com.ftd.framework.common.exceptions.*;

/**
 * Represents all the realms within the application
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class Realms {
  private HashMap realms = new HashMap();
  private HashMap realmProperties = new HashMap();

  /**
   * The default constructor
   **/
  public Realms(){
  }

  /**
   * Returns the realm
   * 
   * @param realmName the realm name
   * @return the realm value
   **/
  public String getRealm(String realmName){
    return (String)realms.get(realmName);
  }

  /**
   * Returns all realms
   *
   **/
   public Map getRealms(){
     return realms;
   }
   
  /**
   * Sets the realm
   *
   * @param realm the realm as a property object
   **/
  public void setRealm(Property realm){
    realms.put(realm.getName(), realm.getValue());
    // create an entry in the realmProperties Map
    // Realm.xml is the basis for creation of a realm
    realmProperties.put(realm.getName(), new Realm(realm.getName()));
  }

  /**
   * Sets properties for a realm. Realm.xml contains the list of all realms that
   * have been loaded. If the realm name provided is not setup in Realm.xml, it 
   * will throw a RealmNotFoundException.
   *
   * @param realmName the realm name
   * @param property the property for that realm
   * @throws RealmNotFoundException when the realm has not yet been created
   **/
  public void setRealmProperties(String realmName, Property property)
        throws RealmNotFoundException {
    Realm realm = null;
    if (realmProperties.containsKey(realmName)){
        realm = (Realm)realmProperties.get(realmName);
        realm.setProperty(property);
        realmProperties.put(realmName, realm);
    } else {
        throw new RealmNotFoundException(realmName + " not found");
    }
  }

  /**
   * Returns a property value from a given realm. It returns a null if the 
   * property does not exist.
   *
   * @param realmName the realm name
   * @param realmProperty the property key within that realm
   * @return the property within that realm
   * @throws RealmNotFoundException when the realm has not yet been created
   **/
  public Property getProperty(String realmName, String realmProperty)
        throws RealmNotFoundException {
    if (realmProperties.containsKey(realmName)){
        Realm realm = (Realm) realmProperties.get(realmName);
        return (Property) realm.getProperty(realmProperty);
    } else {
        throw new RealmNotFoundException(realmName + " not found");
    }
  }


  /**
   * Resets all realms
   */
   public void resetRealms(){
    realms.clear();
    realmProperties.clear();
   }
}