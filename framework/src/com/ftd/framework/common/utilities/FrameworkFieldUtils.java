package com.ftd.framework.common.utilities;

import java.text.*;
import java.math.BigDecimal;
import java.util.Calendar;

import com.ftd.framework.common.exceptions.*;

/**
 * Formats a big decimal as string with () and $.
 *       Also sets precision to length given.
 *       Calls overloaded method to do all the work.
 *
 * @return java.lang.String
 */


public class FrameworkFieldUtils {

 private static LogManager lm = new LogManager("com.ftd.framework.common.utilities.FrameworkFieldUtils");
/**
 * FieldFormatters constructor comment.
 */
private FrameworkFieldUtils() {
	super();
}

public static String formatDoubleNoRound(double doubleValue)
{
    BigDecimal formattedResult = new BigDecimal(doubleValue);
    return formattedResult.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();
}

public static String formatBigDecimalAsCurrency(java.math.BigDecimal amount, int scale)
{

	String strFormattedCurrency = null;
	StringBuffer formattedCurrency = null;
 	boolean negNumber = false;

	return formatBigDecimalAsCurrency(amount, scale, true);

}
/**
 * Description: Method for format big decimal as string with (), and optional
 *              $  as needed. Also sets precision to length given
 *
 *
 * @return java.lang.String
 */
public static String formatBigDecimalAsCurrency(
    BigDecimal amount,
    int scale,
    boolean dollarSign) {

    String strFormattedCurrency = null;
    StringBuffer formattedCurrency = null;
    boolean negNumber = false;

	// make sure that amount is not null
    if (amount != null) {
	    if (amount.signum() < 0) // negative
	        {
	        negNumber = true;
	        amount = amount.abs();
	    }
	} else {
		amount = new BigDecimal("0");
	}

    // set scale as user requested via SCALE parm
    amount = amount.setScale(scale, BigDecimal.ROUND_HALF_UP);

    strFormattedCurrency = amount.toString();

    // get whole number portion
    int idx = strFormattedCurrency.indexOf(".");

    if (idx == -1) /// no decimal so set idx to length
        {
        idx = strFormattedCurrency.length();
    }

    formattedCurrency = new StringBuffer(strFormattedCurrency);

    int pos = 3;

    while (pos < idx) {
        formattedCurrency.insert(idx - pos, ",");
        pos = pos + 3;
    } // while

    if (negNumber == true) {
        // negative # -  add parens
        if (dollarSign) {
            formattedCurrency.insert(0, "($");
        } else {
            formattedCurrency.insert(0, "(");
        }

        formattedCurrency.append(")");
    } else {
        if (dollarSign)
            formattedCurrency.insert(0, "$");
    }

    return formattedCurrency.toString();

}
/**
 * Description: Method for format big decimal as string with and optional
 *              %  as needed. Also sets precision to length given.
 *
 * @return java.lang.String
 */
public static String formatBigDecimalAsPercentage(
    BigDecimal amount)
{
	return formatBigDecimalAsPercentage(amount, 2, true);
}
/**
 * Description: Method for format big decimal as string with and optional
 *              %  as needed. Also sets precision to length given.
 *
 *
 * @return java.lang.String
 */
public static String formatBigDecimalAsPercentage(
    BigDecimal amount,
    int scale)
{
	return formatBigDecimalAsPercentage(amount, scale, true);
}
/**
 * Description: Method for format big decimal as string with and optional
 *              %  as needed. Also sets precision to length given.
 *
 *
 * @return java.lang.String
 */
public static String formatBigDecimalAsPercentage(
    BigDecimal amount,
    int scale,
    boolean percentSign) {

    String strFormattedPercentage = null;
    StringBuffer formattedPercentage = null;
    boolean negNumber = false;
    String outPercentage = null;

	// make sure that amount is not null
    if (amount != null) {

	    // move 2 places to left and set scale as user requested via SCALE parm
	    amount = amount.multiply(new BigDecimal("100"));
	    amount = amount.setScale(scale, BigDecimal.ROUND_FLOOR);

	    strFormattedPercentage = amount.toString();

	    // get whole number portion
	    int idx = strFormattedPercentage.indexOf(".");

	    if (idx == -1) /// no decimal so set idx to length
	        {
	        idx = strFormattedPercentage.length();
	    }

	    formattedPercentage = new StringBuffer(strFormattedPercentage);

	    int pos = 3;

	    while (pos < idx) {
	        formattedPercentage.insert(idx - pos, ",");
	        pos = pos + 3;
	    } // while

        if ( percentSign ) {
            formattedPercentage.append("%");
	    }

	    outPercentage = formattedPercentage.toString();

	} else {
		outPercentage = "-";
	}

    return outPercentage;

}
/**
 * Description: Formats a big decimal as string with () and $
 *              as needed. Also sets precision to length given.
 *              Calls overloaded method to do all the work.
 *
 *
 * @return java.lang.String
 */
public static String formatFloat(Float amount, int scale)
	throws FTDApplicationException
{
	return formatFloat(amount, scale, true);
}
/**
 * Description: Method for format float as string with (), and optional
 *              $  as needed. Also sets precision to length given. DO NOT USE THIS FOR
 *              UUPDATABLE NUMBERS ARE PRECISION IS NOT DEALT WITH MATHMATICALLY. ITS LOPPED
 *              OFF!
 *
 * @return java.lang.String
 */
public static String formatFloat(Float amount, int scale, boolean dollarSign)
	throws FTDApplicationException {

	return formatBigDecimalAsCurrency(new BigDecimal(amount.doubleValue()), 2, dollarSign);

}
/**
 * Description:
 * @return java.lang.String
 */
public static String formatInteger(int amount)
throws FTDApplicationException
{
	return formatInteger(amount, false);

}
/**
 * Description: 	Formats an integer with commas, () if negative and optionally with $.
 *
 * @return java.lang.String
 */
public static String formatInteger(int amount, boolean dollarSign)
	throws FTDApplicationException {

    String strFormattedInteger = null;
    StringBuffer bufFormattedInteger = null;
    boolean negNumber = false;

    strFormattedInteger = Integer.toString(amount);
    bufFormattedInteger = new StringBuffer(strFormattedInteger);

    // see if a negative number. Use string because buf does not have this method
    int negativeIndex = strFormattedInteger.indexOf("-");
	// we should probably add in checks for () eventually?
    if (negativeIndex == 0) {
	    bufFormattedInteger.delete(negativeIndex, negativeIndex+1);
	    negNumber = true;
    } else
    	if (negativeIndex != -1) {
	    	throw new FTDApplicationException ("negative index");
		}

    // format with commas
    int pos = 3;
    int len = bufFormattedInteger.length();
    while (pos < len) {
        bufFormattedInteger.insert(len - pos, ",");
        pos = pos + 3;
    } // while

    // deal with negative and $ needs
    if (negNumber == true) {
        // negative # -  add parens
        if (dollarSign) {
            bufFormattedInteger.insert(0, "($");
        } else {
            bufFormattedInteger.insert(0, "(");
        }

        bufFormattedInteger.append(")");
    } else {
        if (dollarSign)
            bufFormattedInteger.insert(0, "$");
    }

    return bufFormattedInteger.toString();

}
/**
 * Description: Method to take the parts of a name and format them into
 *				last name, first name middle initial
 * Creation date: 04/04/2002
 *
 * @return String
 */
public static String formatName(String firstName, String middleInit, String lastName)
	throws Exception {

	String name = null;

	if ( firstName == null && middleInit == null && lastName == null ) {
		name = " ";

	} else {

		StringBuffer formattedName = new StringBuffer();

		// output last name
		if ( lastName != null && !lastName.trim().equals("") ) {
			formattedName.append(lastName);
		}

		// output first name
		if ( firstName != null && !firstName.trim().equals("") ) {
			if ( formattedName.length() > 0 ) {
				formattedName.append(", ");
			}
			formattedName.append(firstName);
		}

		// only output the middle initial if there is something else there
		if ( middleInit != null && !middleInit.trim().equals("") ) {
			if ( formattedName.length() > 0 ) {
				formattedName.append(" ");
				formattedName.append(middleInit);
			}
		}

		name = formattedName.toString();

	}

	return name;
}
/**
 * Description: Takes a SQL Date in the format yyyy-mm-dd and formats it
 *              as MM/dd/yyyy (which is the format we show in the screens).
 *
 *
 * @return String
 */
public static String formatSQLDateToString(java.sql.Date sqlDate)
throws FTDApplicationException
{
	String strDate = "";
	java.util.Date utilDate = null;

	if (sqlDate != null) {
		strDate = sqlDate.toString();
		SimpleDateFormat dfIn = new SimpleDateFormat ("yyyy-MM-dd");
		SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

		try {
			utilDate = dfIn.parse(strDate);
		} catch (ParseException pe) {
      lm.error(pe);
			throw new FTDApplicationException (pe);
		}

		strDate = dfOut.format(utilDate);
	}

	return strDate;
}
/**
 * Description: Takes a SQL Date in the format yyyy-mm-dd hh:mm:ss and formats it
 *              as MM/dd/yyyy hh:mm:ss (which is the format we show in the screens).
 *
 *
 * @return String
 */
public static String formatSQLTimestampToString(java.sql.Timestamp sqlDate)
	throws FTDApplicationException
{
	String strDate = "";
	java.util.Date utilDate = null;

	if (sqlDate != null) {
		strDate = sqlDate.toString();
		SimpleDateFormat dfIn = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			utilDate = dfIn.parse(strDate);
		} catch (ParseException pe) {
      lm.error(pe);
			throw new FTDApplicationException (pe);
		}

		strDate = dfOut.format(utilDate);
	}

	return strDate;
}
/**
 * Description: Method to make sure a SSN if formatted nicely.
 *              First removes all non-numeric and then formats as nnn-nn-nnnn.
 *              Probably do not really need to strip these off
 *              when saving data in the handler for updates because
 *              its a string after all.  We can do more validation and
 *              exception checking here but I did not bother for now...
 * @return java.lang.String
 */
public static String formatSSN(String inputSSN)
throws Exception
{

	String SSN = null;

	if ( ( inputSSN != null ) && ( !inputSSN.trim().equals("") )) {

		StringBuffer formattedSSN = new StringBuffer(inputSSN);

		if ( (formattedSSN.length() > 0 ) &&
			 ( formattedSSN != null ) &&
			 ( !formattedSSN.equals("") ) ) {

				// remove all non numeric - same as the strip method
				// on main handler but can't use that because where it sits
				char c;
				Character ch = null;

				for (int i = 0; i < formattedSSN.length(); ) {
					c =  formattedSSN.charAt(i);

					if (! ch.isDigit(c)) {
						formattedSSN.delete(i,i+1);
					} else {
							i++;
					}
				}

				if (formattedSSN.length() > 3) {
					formattedSSN.insert(3, "-");
				}

				if (formattedSSN.length() > 6) {
					formattedSSN.insert(6, "-");
				}

				SSN = formattedSSN.toString();
		}

	} else {
		SSN = " ";
	}

	return SSN;
}
/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				in the form of mm/dd/yyyy and converts it
 * 				to a SQL date of yyyy-mm-dd.
 *
 *
 * @return java.sql.Date
 */
public static java.sql.Date formatStringToSQLDate(String strDate)
    throws FTDApplicationException {

    java.sql.Date sqlDate = null;

    if ((strDate != null) && (!strDate.trim().equals(""))) {
	    try {
		    if (strDate.length() < 10) {
			   throw new FTDApplicationException("Date length is less than 10");
		    }
			SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy" );
		    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

		    java.util.Date date = sdfInput.parse( strDate );
		    String outDateString = sdfOutput.format( date );

			// now that we have no errors, use the string to make a SQL date
			sqlDate = sqlDate.valueOf(outDateString);

			} catch (Exception e) {
          lm.error(e);
	     	   throw new FTDApplicationException(e);
	    }
    }

	return sqlDate;
}
/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				in the form of mm/dd/yyyy hh:mm:ss and converts it
 * 				to a SQL date of yyyy-mm-dd hh:mm:ss.
 *
 * @return java.sql.Date
 */
public static java.sql.Timestamp formatStringToSQLTimestamp(String strDate)
    throws FTDApplicationException {

    java.sql.Timestamp sqlDate = null;

    if ((strDate != null) && (!strDate.trim().equals(""))) {
	    try {
		    if (strDate.length() < 19) {
			   throw new FTDApplicationException("Date length is less than 19");
		    }
			SimpleDateFormat sdfInput = new SimpleDateFormat( "MM/dd/yyyy HH:mm:ss" );
		    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd HH:mm:ss" );

		    java.util.Date date = sdfInput.parse( strDate );
		    String outDateString = sdfOutput.format( date );

			// now that we have no errors, use the string to make a SQL date
			sqlDate = sqlDate.valueOf(outDateString);

			} catch (Exception e) {
          lm.error(e);
	     	   throw new FTDApplicationException(e);
	    }
    }

	return sqlDate;
}
/**
 * Description: Takes in a string Date, checks for valid formatting
 * 				nd converts it to a Util date of yyyy-mm-dd.
 * @return java.sql.Date
 */
public static java.util.Date formatStringToUtilDate(String strDate)
    throws FTDApplicationException {

    java.util.Date utilDate = null;
    String inDateFormat = "";
    int dateLength = 0;
    int firstSep = 0;
    int lastSep = 0;

    if ((strDate != null) && (!strDate.trim().equals(""))) {
	    try {

		    // set input date format
		    dateLength = strDate.length();
		    if ( dateLength > 10) {
			    inDateFormat = "yyyy-MM-dd hh:mm:ss";
		    } else {
			    firstSep = strDate.indexOf("/");
			    lastSep = strDate.lastIndexOf("/");

			    switch ( dateLength ) {
				    case 10:
			    		inDateFormat = "MM/dd/yyyy";
				    	break;
				    case 9:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yyyy";
				    	} else {
					    	inDateFormat = "MM/d/yyyy";
			    		}
				    	break;
				    case 8:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/d/yyyy";
				    	} else {
					    	inDateFormat = "MM/dd/yy";
			    		}
				    	break;
				    case 7:
				    	if ( firstSep == 1 ) {
					    	inDateFormat = "M/dd/yy";
				    	} else {
					    	inDateFormat = "MM/d/yy";
			    		}
				    	break;
				    case 6:
				    	inDateFormat = "M/d/yy";
				    	break;
				    default:
				    	break;
			    }
		    }
    		SimpleDateFormat sdfInput = new SimpleDateFormat( inDateFormat );
		    SimpleDateFormat sdfOutput = new SimpleDateFormat ( "yyyy-MM-dd" );

		    java.util.Date date = sdfInput.parse(strDate);
		    String outDateString = sdfOutput.format(date);

			// now that we have no errors, use the string to make a Util date
	 		utilDate = sdfOutput.parse(outDateString);

			} catch (Exception e) {
          lm.error(e);
	     	   throw new FTDApplicationException(e);
	    }
    }

	return utilDate;
}

/**
 * Description: Takes in a string time (ex. 8:15) and stores it in a
 *              java.util.Date object.
 * @return java.util.Date
 */
public static java.util.Date formatStringTimeToUtilDate(String strTime)
    throws FTDApplicationException {

    java.util.Date utilDate = null;

    try {
        int hour = Integer.parseInt(strTime.substring(0, strTime.lastIndexOf(":")).trim());
        int minute = Integer.parseInt(strTime.substring(strTime.lastIndexOf(":")+1, strTime.lastIndexOf(":")+3).trim());
        String amPm = strTime.substring(strTime.lastIndexOf(":")+3, strTime.length()).trim();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);

        utilDate = calendar.getTime();

    } catch (Exception e) {
        lm.error(e);
    }

    return utilDate;
}

/**
 * Description:
 *
 *
 * @return java.lang.String
 */
public static String formatUSPhone(String phoneNotFormatted)
	throws Exception {

	if ((phoneNotFormatted != null) && (!phoneNotFormatted.trim().equals(""))) {
		StringBuffer formattedPhone = new StringBuffer( stripNonNumeric( phoneNotFormatted ) );

		if ( (formattedPhone.length() > 0) &&
			 (formattedPhone != null) && (!formattedPhone.equals("")) ) {
			formattedPhone.insert(0, "(");

			if (formattedPhone.length() > 3) {
				formattedPhone.insert( 4, ')');
				formattedPhone.insert( 5, ' ');

				if (formattedPhone.length() > 7) {
					formattedPhone.insert (9,'-');
				}
			}
			return formattedPhone.toString();
		}
	}

	return "";
}
/**
 * Description: Takes a Util Date in the format yyyy-mm-dd and formats it
 *              as MM/dd/yyyy (which is the format we show in the screens).
 *
 *
 * @return String
 */
public static String formatUtilDateToString(java.util.Date utilDate)
throws FTDApplicationException
{
	String strDate = "";
	String inDateFormat = "";
	java.util.Date newDate = null;

	if (utilDate != null) {
		SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy");

		try {
			newDate = dfOut.parse(dfOut.format(utilDate));
		} catch (ParseException pe) {
      lm.error(pe);
			throw new FTDApplicationException (pe);
		}

		strDate = dfOut.format(newDate);

	} else {
		strDate = "";
	}

	return strDate;
}
/**
 * Description:
 *
 *
 */
  public static void main(String[] param) {
        FrameworkFieldUtils bean = new FrameworkFieldUtils();
        try {
		//this does not work! java.util.Date utilDate = null;
        //SimpleDateFormat dfIn = new SimpleDateFormat ("yyyy-MM-dd");
		//SimpleDateFormat dfOut = new SimpleDateFormat("yyyy-MM-dd");
		//utilDate = dfIn.parse("1954-01-20");
		//.formatUtilDateToString(utilDate);

//          bean.formatStringToSQLDate("2/13/01");
//	   		java.sql.Date sqlDate = java.sql.Date.valueOf ("1954-01-20");
//	        bean.formatSQLDateToString(sqlDate);
//			bean.formatSSN("");
//			bean.formatFloat(new Float(-12345),0);
//			bean.formatInteger(-12345, true);

        } catch (Exception e) {
        }
    }
/**
 * Description:   From Phase 1 - wbet delivery.
 *                Replaces ALL occurences of a string(what) in a string(source)
 *                with a given string (with).
 *
 * @return java.lang.String
 */
 public static String replaceAll(String source, String what, String with)
	{
		String strReturn = null;

		strReturn = translateNullToString(source);
		strReturn = translateNullToString(strReturn);
		int index = -1;

		if (with == null) {
			return strReturn;
		}

		index = strReturn.indexOf (what);
		while (index >=0)
			{
				strReturn = strReturn.substring (0,index) + with + strReturn.substring (index + what.length (),strReturn.length ());
				index = strReturn.indexOf (what,index + with.length ());
			}

		return strReturn;
	}
/**
 * Description:   Copied from Phase 1 - wbet delivery and modified to replace only 1
 *                occurence. Replaces a string(what) in a string(source)
 *                with a given string (with).
 *
 *
 * @return java.lang.String
 */
 public static String replaceOnce(String source,String what,String with)
	{
		String strReturn = null;

		strReturn = translateNullToString(source);
		strReturn = translateNullToString(strReturn);
		int index = -1;
		if (with == null) {
			return strReturn;
		}

		index = strReturn.indexOf (what);

		if (index >=0)
			{
				strReturn = strReturn.substring (0,index) + with + strReturn.substring (index + what.length (),strReturn.length ());
				index = strReturn.indexOf (what,index + with.length ());
			}

		return strReturn;
	}
public static String stripNonNumeric(String inString) {

	char c;
	Character ch = null;

	if ((inString != null) && (!inString.trim().equals(""))) {

		StringBuffer outString = new StringBuffer(inString);

		for (int i = 0; i < outString.length(); ) {
			c =  outString.charAt(i);

			if (! ch.isDigit(c)) {
				outString.delete(i,i+1);

			}
			else {
				i++;
			}
		}

		return outString.toString();
	}
	else
	{
		return null;
	}

}
public static String stripNonNumericForDecimal(String inString) {

	char c;
	Character ch = null;
	boolean negative = false;

	if ((inString != null) && (!inString.trim().equals(""))) {
		StringBuffer outString = new StringBuffer(inString);

		for (int i = 0; i < outString.length(); ) {
		c =  outString.charAt(i);

		if (! ch.isDigit(c))
		{

			if (c == '.'){
				i++;
			}else {
				if (((c == '(') || (c == '-')) && (i == 0)) {
					negative =true;
				}
				outString.delete(i,i+1);
			}
		}else {
			i++;
		}

	}

	if (negative == true)
		return "-" + outString.toString();

		else  return outString.toString();

	} else // null inString
		return null;
}
/**
 * Insert the method's description here.
 * @return java.lang.String
 */
public static String translateEmptyStringToNbsp(String a_sInput) {
	if (a_sInput == null)
	{
		a_sInput = "";
	}
	return a_sInput.trim().length() == 0 ? "&nbsp;" : a_sInput;
}
/**
 * Insert the method's description here.
 * @return java.lang.String
 */
public static String translateEmptyStringToSpace(String a_sInput)
{
	if (a_sInput == null)
	{
		a_sInput = "";
	}
	return a_sInput.equals("") ? " " : a_sInput;
}
/**
 * Insert the method's description here.
 * @return java.lang.String
 */
public static String translateNbspToSpace(String a_sInput)
{
	return a_sInput.equals("&nbsp;") ? " " : a_sInput;
}
/**
 * Description:   From Phase 1 - wbet delivery.
 *                returns "" vs null string for null string
 *
 * @return java.lang.String
 */
public static String translateNullToString(Object source)
	{
		String strReturn = "";
		if (source == null)
			strReturn = "";
		else
			strReturn = "" + source;
		return (String)strReturn;
	}
/**
 * Description:   From Phase 1 - wbet delivery.
 *                returns "" vs null string for null string
 *
 *
 * @return java.lang.String
 */

public static String translateNullToString(String source)
	{
		String strReturn = "";
		if (source == null)
			strReturn = "";
		else
			strReturn = source;
		return strReturn;
	}
/**
 * Insert the method's description here.
 * @return java.lang.String
 */
public static String truncateString(String a_sInput) {
	if (a_sInput == null)
	{
		a_sInput = "";
	}
	return a_sInput.trim();
}

/**
 * Insert the method's description here.
 * @return java.lang.String
 * @param utilDate java.util.Date
 */
public static String formatUtilDateToStringWithTimestamp(java.util.Date utilDate) throws Exception {
	String strDate = "";
	String inDateFormat = "";
	java.util.Date newDate = null;

	if (utilDate != null) {
		SimpleDateFormat dfOut = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

		try {
			newDate = dfOut.parse(dfOut.format(utilDate));
		} catch (ParseException pe) {
      lm.error(pe);
			throw new FTDApplicationException (pe);
		}

		strDate = dfOut.format(newDate);

	} else {
		strDate = "";
	}

	return strDate;
}
}
