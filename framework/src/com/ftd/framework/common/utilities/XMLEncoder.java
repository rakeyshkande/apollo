package com.ftd.framework.common.utilities;

import java.util.HashMap;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * 
 * @deprecated Switch to {@link com.ftd.osp.utilities.xml.DOMUtil} for XML Functionality
 */
public class XMLEncoder
{
/*
  private oracle.xml.parser.v2.XMLDocument document = null;
  private Element root = null;

  public XMLEncoder(String type)
  {
      document = new XMLDocument();
      root = (Element) document.createElement("root");
      root.setAttribute("type", type);
      document.appendChild(root);
  }
*/

    public static Document createXMLDocument(String type) throws Exception
    {
        Document document = DOMUtil.getDocument();
        
        Element root = (Element) document.createElement("root");
        root.setAttribute("type", type);
        document.appendChild(root);

        return document;
    }

  /**
   * 
   */
  public static void addSection(Document document, String elementName, String entry, HashMap values)
  {
      Element element = (Element) document.createElement(elementName);
      Element tmpElement = null;
      String key = null;

      for (Iterator keyIterator = (values.keySet()).iterator(); keyIterator.hasNext(); )
      {
          tmpElement = (Element) document.createElement(entry);
          key = (String)keyIterator.next();
          tmpElement.setAttribute("name", key);
          tmpElement.setAttribute("value", (String)values.get(key));
          element.appendChild(tmpElement);
      }
      document.getDocumentElement().appendChild(element);
  }

/*   
  public void addSection(String section) throws Exception
  {
      XMLDocument tmpDoc = new XMLDocument();
      DOMParser parser = null;

      try
      {
        parser = new DOMParser();
        parser.parse( new InputSource( new StringReader(section) ));
        tmpDoc = parser.getDocument();
      }
      catch(Exception e)
      {
        throw new Exception("Unable to add section.Specific reason: " + e.toString());
      }

      //tmpDoc.print(System.out);
      NodeList tmpNodeList = tmpDoc.getChildNodes();
      for(int i = 0; i < tmpNodeList.getLength(); i++)
      {
        root.appendChild(document.importNode(tmpNodeList.item(i), true));
      }
  }
*/
/*
    public XMLDocument getXML() throws Exception
    {
        return document;
    }
*/

  public static void addSection(Document document, NodeList nodeList)
  {
      for(int i = 0; i < nodeList.getLength(); i++)
      {
        document.getDocumentElement().appendChild(document.importNode(nodeList.item(i), true));
      }
  }
}