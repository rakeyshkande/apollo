package com.ftd.framework.common.utilities;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.ftd.osp.utilities.xml.DOMUtil;

/**
 * @deprecated Switch to {@link com.ftd.osp.utilities.xml.DOMUtil} for XML Functionality
 */
public class XPathQuery
{
    //private oracle.xml.parser.v2.XMLDocument document = null;
    private static LogManager lm = new LogManager("com.ftd.oe");

    public XPathQuery()
    {
    }

	/**
	 * Returns a node list that has the results of the xpath query
	 *
	 * @param doc the oracle.xml.parser.v2.XMLDocument
	 * @param xpath the xpath query
     */
    public NodeList query(Document document, String xpath) throws Exception
    {
        Date startDate = new Date();
        if ( (document == null) || (xpath == null) || (xpath.equals("")) )  {
		            return null;
        }

        NodeList nodeList = null;

        nodeList = DOMUtil.selectNodes(document, xpath);

        long crTime = (System.currentTimeMillis() - startDate.getTime());
        if(crTime > 1000)
        {
            lm.debug("XML Doc search time: " + crTime);
            lm.debug("XPath = " + xpath);
        }

        return nodeList;
    }

    /**
     * @deprecated
     */
    public NodeList query(String xml, String xpath) throws Exception
    {
        NodeList nodeList = null;
        Document document = null;

        try
        {
            document = createDocument(xml);
            nodeList = this.query(document, xpath);
        }
        catch(Exception e)
        {
            throw new Exception("Unable to execute Xpath Query.Specific reason: " +  e.toString());
        }

        return nodeList;
    }

    /**
     * Creates an oracle.xml.parser.v2.XMLDocument from a well formatted xml string
	 *
     * @param xmlString the xml string
     * @deprecated
     */
    public static Document createDocument(String xml) throws Exception
    {
        Date startDate = new Date();

		if (xml == null || xml.equals(""))
		{
          return null;
      	}

        Document  document = DOMUtil.getDocument(xml);
        
        long crTime = (System.currentTimeMillis() - startDate.getTime());
        if(crTime > 1000)
        {
            lm.debug("XML Doc creation time: " + crTime);
            lm.debug("XML Doc = " + xml.substring(0, 256));
        }

        return document;
    }

    public static String getXMLString(Document doc)  throws Exception
    {
        if(doc == null)
        {
            return null;
        }
        Date startDate = new Date();
        StringWriter s = new StringWriter();
        DOMUtil.print(doc, new PrintWriter(s));
        long crTime = (System.currentTimeMillis() - startDate.getTime());
        if(crTime > 1000)
        {
            lm.debug("XML Doc getXMLString: " + crTime);
        }
        return s.toString();
    }
}