package com.ftd.framework.common.utilities;

/**
 * Contains all the constant values used within the framework.
 *
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class FrameworkConstants {

  public final static String APPLICATION_DAO_MAPPING_XML = "daomapping.xml";
  public final static String APPLICATION_DAO_MAPPING_DTD = "daomapping.dtd";
  public final static String APPLICATION_REALM_FILE_XML = "realm.xml";
  public final static String APPLICATION_PROPERTIES_DTD = "properties.dtd";
  public final static String APPLICATION_SERVICES_FILE_XML = "services.xml";
  public final static String APPLICATION_SERVICES_DTD = "services.dtd";
  public static final String APPLICATION_FRAMEWORK_CONFIG_REALM_KEY = "ftd.framework.configuration";
  public final static String APPLICATION_INTERFACE_MAPPING_REALM_KEY = "ftd.application.interfacemapping";
  public static final String APPLICATION_DB_DATASOURCE_KEY = "application.db.datasource";
  public static final String APPLICATION_DB_USER_KEY = "application.db.user";
  public static final String APPLICATION_DB_PASSWORD_KEY = "application.db.password";
  public static final String APPLICATION_ERROR_STRINGS_KEY = "application.error.strings";
  public static final String APPLICATION_LOG4J_CONFIG_KEY = "application.log4j.config";

  public static final String JNDI_PROVIDER_URL_KEY = "jndi.provider.url";
  public static final String JNDI_CONTEXT_FACTORY_KEY = "jndi.context.factory";
  public static final String JNDI_SECURITY_PRINCIPAL_KEY = "jndi.context.security.principal";
  public static final String JNDI_SECURITY_CREDENTIALS_KEY = "jndi.context.security.credentials";

  public final static String FRAMEWORK_CONFIG_CATEGORY_NAME = "com.ftd.framework.config";
  public final static String FRAMEWORK_EXCEPTIONS_CATEGORY = "com.ftd.framework.exceptions";
  public final static String FRAMEWORK_STRUTS_SERVLET_CATEGORY_NAME = "com.ftd.framework.struts.servlet";
  
  public static final String DEFAULT_CONTEXT_CONSTANT = "default_context";
  public static final String USER_PROFILE_CONSTANT = "UserProfile";
  public static final String SYSTEM_ERROR_CONSTANT = "SystemError";
  public static final String APPLICATION_ERROR_CONSTANT = "ApplicationError";
  public static final String MESSAGE_BUNDLE_CONSTANT = "MessageBundle";
  public static final String APPLICATION_CATEGORY_CONSTANT = "Application";
  public static final String SYSTEM_CATEGORY_CONSTANT = "System";
  public static final String EXCEPTION_OBJECT_CONSTANT = "ExceptionObject";

  public static final String SQLSTRING_PLACEHOLDER_CONSTANT = "?";
}
