package com.ftd.framework.common.utilities;
import java.io.Serializable;

/**
 * This class stores all user related information. This information can be 
 * persisted for the duration of the user session.
 * 
 * @author Anshu Gaind
 * @version 1.0 
 **/
public class UserProfile implements Serializable{
  private String userName;
  private String securityRole;
  private boolean authenticated;
  
  /**
   * The default constructor
   **/
  public UserProfile() {
  }

  /**
   * Returns the user name
   *
   * @return the user name
   **/
  public String getUserName() {
    return userName;
  }

  /**
   * Sets the user name
   *
   * @param newUserName the user name
   **/
  public void setUserName(String newUserName) {
    userName = newUserName;
  }

  /**
   * Returns the security role of the user
   *
   * @return the security role
   **/
  public String getSecurityRole() {
    return securityRole;
  }

  /** 
   * Sets the security role
   *
   * @param newSecurityRole the security role
   **/
  public void setSecurityRole(String newSecurityRole) {
    securityRole = newSecurityRole;
  }

  /**
   * Indicates whether or not a user is operating in a security role
   *
   * @param newSecurityRole the security role
   * @return whether or not the user is in the specified role
   **/
   public boolean isUserInRole(String newSecurityRole){
    return securityRole.equals(newSecurityRole);
   }

   /**
    * Sets whether or not the user has been authenticated
    *
    * @param newAuthenticated whether or not the user has been authenticated
    **/
   public void setAuthenticated(boolean newAuthenticated){
    authenticated = newAuthenticated;
   }

   /**
    * Returns whether or not the user has been authenticated
    *
    * @return whether or not the user has been authenticated
    **/
   public boolean isUserAuthenticated(){
    return authenticated;
   }
}
