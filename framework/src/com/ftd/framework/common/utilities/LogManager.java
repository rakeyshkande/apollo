// Unit Tested
package com.ftd.framework.common.utilities;
import com.ftd.osp.utilities.plugins.Logger;


/**
 * This class acts as a facade to com.ftd.osp.utilities.plugins.Logger.
 * All log4j properties are extracted from log4j.properties
 * 
 * @author Anshu Gaind
 * @version 2.0 
 **/
public class LogManager  {

  private Logger logger;

	/**
	 * LogManager constructor.
	 * 
	 * @param myCategoryName the category name
	 */
	public LogManager(String myCategoryName)  {
		super();
    logger = new Logger(myCategoryName);        
	}

  /******************************************************************************/
  
  /**
   * Log a message object with the DEBUG level
   *
   * @param message the message
   **/
	public void debug(String message) {
		logger.debug(message);
	}

  /**
   * Log a message object with the DEBUG level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void debug(Throwable t) {
		logger.debug(t.getMessage(), t);
	}

  /**
   * Log a message object with the DEBUG level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void debug(String message, Throwable t) {
		logger.debug(message, t);
	}

/******************************************************************************/

  /**
   * Log a message object with the ERROR level
   *
   * @param message the message
   **/
	public void error(String message) {
		logger.error(message);
	}	

  /**
   * Log a message object with the ERROR level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void error(Throwable t) {
		logger.error(t.getMessage(), t);
	}

  /**
   * Log a message object with the ERROR level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void error(String message, Throwable t) {
		logger.error(message, t);
	}


/******************************************************************************/

  /**
   * Log a message object with the FATAL level
   *
   * @param message the message
   **/
	public void fatal(String message) {
		logger.fatal(message);
	}

  /**
   * Log a message object with the FATAL level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void fatal(Throwable t) {
		logger.fatal(t.getMessage(), t);
	}
  /**
   * Log a message object with the FATAL level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void fatal(String message, Throwable t) {
		logger.fatal(message, t);
	}

/******************************************************************************/
  /**
   * Log a message object with the INFO level
   *
   * @param message the message
   **/
	public void info(String message) {
		logger.info(message);
	}

  /**
   * Log a message object with the INFO level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void info(Throwable t) {
		logger.warn(t.getMessage(), t);
	}
  /**
   * Log a message object with the INFO level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void info(String message, Throwable t) {
		logger.info(message, t);
	}

/******************************************************************************/

  /**
   * Log a message object with the WARN level
   *
   * @param message the message
   **/
	public void warn(String message) {
		logger.warn(message);
	}

  /**
   * Log a message object with the WARN level
   *
   * @param t the exception to log, including its stack trace.
   **/
	public void warn(Throwable t) {
		logger.warn(t.getMessage(), t);
	}

  /**
   * Log a message object with the WARN level
   *
   * @param message the message
   * @param t the exception to log, including its stack trace.
   **/
	public void warn(String message, Throwable t) {
		logger.warn(message, t);
	}
/******************************************************************************/

	/**
	 *	Indicates whether debug messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if debug messages are enabled
	 */
	public boolean isDebugEnabled(){
		return logger.isDebugEnabled();
	}

	/**
	 *	Indicates whether info messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if info messages are enabled
	 */
	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}
	
	/**
	 *	Indicates whether warn messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if warn messages are enabled
	 */
	public boolean isWarnEnabled() {
		return logger.isWarnEnabled();
	}
	
	/**
	 *	Indicates whether error messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if error messages are enabled
	 */
	public boolean isErrorEnabled(){
		return logger.isErrorEnabled();
	}
	
	/**
	 *	Indicates whether fatal messages are turned on for the category
	 *  of this class.
	 * 
	 *  @return boolean true if fatal messages are enabled
	 */
	public boolean isFatalEnabled() {
		return logger.isErrorEnabled();
	}

	
}
