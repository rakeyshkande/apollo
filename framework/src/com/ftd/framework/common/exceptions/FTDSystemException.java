package com.ftd.framework.common.exceptions;

/**
 * The base class for all system exceptions.
 *
 * @author Anshu Gaind
 * @version 1.0
 **/
public class FTDSystemException extends FTDException {


  public FTDSystemException()
  {
    super();
  }


  public FTDSystemException(String message)
  {
    super(message);
  }

/**
 * Wraps the original exception object
 */
 public FTDSystemException(Throwable t)
 {
   super(t);
 }


/**
 * Used when the exception does not need a custom message with it.
 *
 * @param errorCode the error code
 * @author Anshu Gaind
 * @deprecated
 */
  public FTDSystemException(int errorCode){
    super();
  }

/**
 * Used when the error code has an associated error message.
 *
 * @param errorCode the error code
 * @param args[] the arguments that go in the custom error message
 * @author Anshu Gaind
 * @deprecated
 **/
  public FTDSystemException(int errorCode, String[] args){
    super();
  }

/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 * @deprecated
 **/
  public FTDSystemException(int errorCode, Throwable nestedThrowable){
    super(nestedThrowable);
  }


/**
 * Used when there is a need to embed a nested exception
 *
 * @param errorCode the error code
 * @param args[] the arguments that go in the custom error message
 * @param nestedThrowable the nested exception
 * @author Anshu Gaind
 * @deprecated
 **/
  public FTDSystemException(int errorCode, String[] args, Throwable nestedThrowable){
    super(nestedThrowable);
  }


}
