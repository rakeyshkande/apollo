package com.ftd.framework.common.exceptions;

import javax.ejb.EJBException;
/**
 * The base class for all EJBExceptions.
 *
 * @author Anshu Gaind
 * @version 1.0 
 * @deprecated
 **/
public class FTDEJBException extends EJBException implements java.io.Serializable{


  /**
   * The default constructor
   **/
	public FTDEJBException() {
		super();
	}


}
