package com.ftd.framework.common.exceptions;

import java.io.StringWriter;
import java.io.PrintWriter;
//import com.ftd.framework.*;
/**
 * The base exception class for all exceptions. All constructors are protected
 * so that only inherited classes can use this class.
 * @author Anshu Gaind
 * @version 1.0 
 **/
public abstract class FTDException extends Exception implements java.io.Serializable {

  public FTDException()
  {
    super();
  }


  public FTDException(String message)
  {
    super(message);
  }

/**
 * Wraps the original exception object
 */
  protected FTDException(Throwable t)
  {
    super(t);
  }
  


}
