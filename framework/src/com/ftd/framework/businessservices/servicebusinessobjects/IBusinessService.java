package com.ftd.framework.businessservices.servicebusinessobjects;

import com.ftd.framework.common.valueobjects.FTDArguments;
import com.ftd.framework.common.valueobjects.FTDCommand;
import com.ftd.framework.common.valueobjects.FTDSystemVO;

import java.io.Serializable;

/**
 * In a distributed development environment developers should code against an interface and 
 * not an implementation. Therefore all business objects will require to 
 * publish an interface that contains all public methods.
 * This is the the base interface for all interfaces of business objects. 
 *  
 * @author Anshu Gaind, Sasha Secivan
 * @version 1.0
 **/
public interface IBusinessService extends Serializable 
{
	public FTDSystemVO main(FTDCommand command, FTDArguments arguments);
  public void resetBusinessService();
  public void setServiceName(String serviceName);
  public String getServiceName();
  public void registerCommand(FTDCommand command);
}