package com.ftd.framework.businessservices.servicebusinessobjects;

import com.ftd.framework.businessservices.serviceprovider.PersistenceManager;
import com.ftd.framework.common.utilities.LogManager;
import com.ftd.framework.common.valueobjects.FTDArguments;
import com.ftd.framework.common.valueobjects.FTDCommand;
import com.ftd.framework.common.valueobjects.FTDPersistentObject;
import com.ftd.framework.common.valueobjects.FTDSystemVO;
import com.ftd.framework.dataaccessservices.BusinessDataBridge;
import com.ftd.framework.dataaccessservices.DataManager;
import com.ftd.framework.dataaccessservices.FTDDataAccessManager;
import com.ftd.framework.dataaccessservices.GenericDataService;
import com.ftd.framework.dataaccessservices.IDataServiceBase;

import java.util.*;
import oracle.toplink.expressions.ExpressionBuilder;
import oracle.toplink.queryframework.ReadObjectQuery;
import oracle.toplink.sessions.UnitOfWork;

/**
 *
 * @author Sasha Secivan
 * @author Anshu Gaind
 * @version 2.0 
 **/
public abstract class BusinessService implements IBusinessService
{
    private UnitOfWork uow;
    private int uowCount;
    private LogManager lm = new LogManager("com.ftd.framework.businessservices.servicebusinessobjects.BusinessService");
    private String serviceName;
    private String commandName;
    private int serviceID = this.generateServiceID();;
    
    public BusinessService() 
    {}
      
    public LogManager getLogManager()
    {
      return lm;
    }

    public FTDPersistentObject getPersistentServiceObject(String sessionID, Class objectClass) throws Exception
    {

        lm.debug(this.getServiceName() 
                  + "::" + this.serviceID + "::" + this.commandName);

        // Pull a unit of work for this service
        uow = DataManager.getInstance().getUnitOfWork();
        this.uowCount++;
        lm.debug(this.getServiceName() 
                  + "::" + this.serviceID + "::" + this.commandName 
                  + "::" + "TopLink Unit of Work Acquired" 
                  + "::" + this.uowCount);
        
        long currentTime = System.currentTimeMillis();

        // Read timestamp from database to compare against cache to check for server change
        ReadObjectQuery timeCheckQuery = new ReadObjectQuery(objectClass);
        timeCheckQuery.addPartialAttribute("timeStamp");
        timeCheckQuery.setSelectionCriteria(new ExpressionBuilder().get("guid").equal(sessionID));
        timeCheckQuery.dontMaintainCache();
        lm.debug(this.getServiceName() 
                  + "::" + this.serviceID + "::" + this.commandName 
                  + "::" + "Building ReadObjectQuery to read timestamp from database to compare against cache to check for server change" 
                  + "::" + this.uowCount);
        FTDPersistentObject databaseObject = (FTDPersistentObject) uow.executeQuery(timeCheckQuery);
        lm.debug(this.getServiceName() 
                  + "::" + this.serviceID + "::" + this.commandName 
                  + "::" + "ReadObjectQuery Executed Successfully." 
                  + "::" + this.uowCount);
        FTDPersistentObject poClone = (FTDPersistentObject) uow.readObject(objectClass, new ExpressionBuilder().get("guid").equal(sessionID));
        lm.debug(this.getServiceName() 
                  + "::" + this.serviceID + "::" + this.commandName 
                  + "::" + "FTDPersistentObject has been cloned" 
                  + "::" + this.uowCount);

        if(databaseObject.getTimeStamp() != poClone.getTimeStamp())
        {
            lm.info(this.getServiceName() 
                    + "::" + this.serviceID + "::" + this.commandName 
                    + "::" + "REFRESHING PERSISTENT OBJECT");
            uow.refreshObject(poClone);
        }
        
        lm.info(this.getServiceName() 
                + "::" + this.serviceID + "::" + this.commandName 
                + "::" + "PO READ TIME : " + (System.currentTimeMillis() - currentTime));
              
        return poClone;
	  }

    public void returnPersistentServiceObject(FTDPersistentObject po)
    {
        long currentTime = System.currentTimeMillis();
        
        po.setTimeStamp(System.currentTimeMillis());
                
        if (uow.isActive()) 
        {
          uow.commit();
          this.uowCount--;
          lm.debug(this.getServiceName() 
                    + "::" + this.serviceID + "::" + this.commandName 
                    + "::" + "TopLink Unit of Work was committed successfully."
                    + "::" + this.uowCount);
        }
        else
        {
          this.uowCount--;
          lm.error(this.getServiceName() 
                    + "::" + this.serviceID + "::" + this.commandName 
                    + "::" + "Attempting to commit a TopLink Unit of Work again. "
                    + "A TopLink Exception will be thrown. "
                    + "Please check your code!!!"
                    + "::" + this.uowCount);
          uow.commit();
        }
                        
        lm.info(this.getServiceName() 
                + "::" + this.serviceID + "::" + this.commandName 
                + "::" + "PO WRITE TIME : " 
                + (System.currentTimeMillis() - currentTime));
    }
	
    
    private void checkTopLinkIntegrity()
    {
      lm.debug(this.getServiceName() 
                + "::" + this.serviceID + "::" + this.commandName 
                + "::" +"BEGIN PERFORMING TOPLINK INTEGRITY CHECK");
      if (this.uowCount > 0) 
      {
          lm.error(this.getServiceName() 
                    + "::" + this.serviceID + "::" + this.commandName 
                    + "::" +"TopLink Unit of Work has not been committed. Please check your code!!! " 
                    +"::"+ this.uowCount);
      }
      else if (this.uowCount < 0) 
      {
          lm.error(this.getServiceName() 
                    + "::" + this.serviceID + "::" + this.commandName 
                    + "::" +"More than one attempt was made to commit the TopLink Unit of Work. Please check your code!!! "
                    +"::"+ this.uowCount);        
      } 
      else if (this.uowCount == 0) 
      {
          lm.debug(this.getServiceName() 
                    + "::" + this.serviceID + "::" + this.commandName 
                    + "::" +"Integrity check successful. " 
                    +"::"+ this.uowCount);
      }
      lm.debug(this.getServiceName() 
                + "::" + this.serviceID + "::" + this.commandName 
                + "::" +"END PERFORMING TOPLINK INTEGRITY CHECK");
      
    }
  
  /**
   * The correct way of overriding this implementation is to first invoke the
   * super.resetBusinessService() method.
   */
  public void resetBusinessService()
  {
    this.checkTopLinkIntegrity();
    if ( (this.uow != null) && (this.uow.isActive()) ) 
    {
        lm.error(this.getServiceName() 
                  + "::" + this.serviceID + "::" + this.commandName
                  + "::" +"Releasing an uncommitted unit of work");
        this.uow.release();
    }
    this.uowCount = 0;
    lm.debug(this.getServiceName() 
              + "::" + this.serviceID + "::" + this.commandName 
              + "::" +"Business Service Reset Complete");    
  }
  
  /**
   * Set the name of the service
   * 
   * @param serviceName the name of the service
   */
   public void setServiceName(String serviceName)
   {
     this.serviceName = serviceName;
   }
   
   /**
    * Returns the name of the service.
    * 
    * @return the name of the service
    */
   public String getServiceName()
   {
     return this.serviceName;
   }
  
  /**
   * Register the FTDCommand
   * 
   * @param command the FTDCommand
   */
   public void registerCommand(FTDCommand command)
   {     
     if (command != null) 
     {
        this.commandName = command.getCommand();
     }
   }
   
	public FTDPersistentObject getServiceObject(String sessionID) throws Exception
	{
		PersistenceManager pm = PersistenceManager.getInstance();
        FTDPersistentObject po = (FTDPersistentObject)pm.get(sessionID); 
        if(po == null)
        {
            throw new Exception("No such object in the persistance store");
        }
    
        return po;
	}
	
	public GenericDataService getGenericDataService(String clientID) throws Exception
	{
        FTDDataAccessManager dam = new FTDDataAccessManager(); 
        GenericDataService service = dam.getGenericDataService(clientID);
        if(service == null)
        {
            throw new Exception("No such data service");
        }
    
		return service;	
	}

    public void registerDataBridge(BusinessDataBridge bridge)
    {
        FTDDataAccessManager dam = new FTDDataAccessManager(); 
        dam.registerDataBridge(bridge);
    }

    public IDataServiceBase getCustomDataService(String serviceName) throws Exception
    {
        IDataServiceBase service = null;
      
        try
        {
            FTDDataAccessManager dam = new FTDDataAccessManager(); 
            service = dam.getCustomDataService(serviceName);
        }
        catch(Exception e)
        {
            lm.error(e);
            throw e;
        }

        return service;
    }


  /**
   * Returns a unique service id
   */
   private int generateServiceID()
   {
     return new Random().nextInt();
   }
   
  /**
   * 
   */
	public abstract FTDSystemVO main(FTDCommand command, FTDArguments arguments);
}