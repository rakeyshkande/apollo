package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.businessservices.servicebusinessobjects.*;

/**
 * The base interface for all services.
 * 
 * @author Anshu Gaind, Sasha Secivan
 * @version 1.0 
 **/ 

public interface IService  
{
	public void setServiceObserver(ServiceObserver serviceObserver);
	public void setServiceImlementation(IBusinessService businessService);
  public void resetServiceImplementation();
	public void setPersistantManager(IPersistenceManager persistenceManager);
	public void setDescription(String description);
	public String getID();
	public String getDescription();
	public void lock();
	public void unlock();
	public FTDServiceResponse executeCommand(FTDServiceRequest request) throws Exception;		 	
}