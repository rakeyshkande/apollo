package com.ftd.framework.businessservices.serviceprovider;

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
interface IServiceObserver
{
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	void notifyLock(IService service);
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	void notifyUnlock(IService service);
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	void registerManager(IServiceObserverListener observerListener);		
}