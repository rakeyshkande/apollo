package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;
import com.ftd.framework.common.exceptions.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.framework.businessservices.servicebusinessobjects.*;

public class ServicePool extends Hashtable
{			
	private LogManager lm = new LogManager("com.ftd.oe");	
	private HashMap serviceMappings = new HashMap();
	private ServiceObserver serviceObserver = ServiceObserver.getInstance();
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
    public ServicePool(int initSize)
    {
      super(initSize);
    }

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   *
	protected IService leaseService(String description) throws Exception
	{
		String requestedService = (String)(this.serviceMappings.get(description));
		if(requestedService == null)
		{
			throw new Exception("No such service in the pool");		
		}
				
    Stack serviceGroup = (Stack)(this.get(description));
		IService service = null;
		
    /**
     * Instead of obtaining a lock to the entire service pool, 
     * obtain a lock to the specific service group. 
     * This way other threads can still access other service groups.
     *
    synchronized(serviceGroup)
    {
        if (serviceGroup.empty())
        {	            
          String serviceClassName = (String)(this.serviceMappings.get(description));
          Class businessServiceClassImpl = null;
          Object businessServiceObject = null;
          
          service = new Service();
          service.setServiceObserver(this.serviceObserver);
          service.setPersistantManager(PersistenceManager.getInstance());
          service.setDescription(description);
          
          try 
          {		
            businessServiceClassImpl = Class.forName(serviceClassName);
            businessServiceObject = businessServiceClassImpl.newInstance();
          } 
          catch(ClassNotFoundException cnfe)
          {
            lm.error(cnfe);
          } 
          catch(IllegalAccessException iae)
          {
            lm.error(iae);
          } 
          catch (InstantiationException ie)
          {
            lm.error(ie);
          }
              
          if(businessServiceObject != null)
          {
            ((IBusinessService) businessServiceObject).setServiceName(description);
            service.setServiceImlementation((IBusinessService)businessServiceObject);					
            serviceGroup.add(service);
            lm.debug("Adding new " + description + " to the service pool.");
          }
          
        }
        else
        {
          service = (IService)(serviceGroup.pop());
          lm.debug("Retrieving " + description + " from the service pool.");
        }
    }
    
    lm.debug("THE SIZE OF THE SERVICES POOL IS : " + serviceGroup.size());			
		service.lock();
		
		//find the appropriate service
		//get service
		//call lock on the service
		return service;
	}
*/

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	protected IService leaseService(String description) throws Exception
	{
		String requestedService = (String)(this.serviceMappings.get(description));
		if(requestedService == null)
		{
			throw new Exception("No such service in the pool");		
		}
				
		IService service = null;
		
    String serviceClassName = (String)(this.serviceMappings.get(description));
    Class businessServiceClassImpl = null;
    Object businessServiceObject = null;
    
    service = new Service();
    service.setServiceObserver(this.serviceObserver);
    service.setPersistantManager(PersistenceManager.getInstance());
    service.setDescription(description);
          
    try 
    {		
      businessServiceClassImpl = Class.forName(serviceClassName);
      businessServiceObject = businessServiceClassImpl.newInstance();
    } 
    catch(ClassNotFoundException cnfe)
    {
      lm.error(cnfe);
    } 
    catch(IllegalAccessException iae)
    {
      lm.error(iae);
    } 
    catch (InstantiationException ie)
    {
      lm.error(ie);
    }
        
    if(businessServiceObject != null)
    {
      ((IBusinessService) businessServiceObject).setServiceName(description);
      service.setServiceImlementation((IBusinessService)businessServiceObject);					
      lm.debug("Creating new " + description );
    }
       
		service.lock();
		
		//find the appropriate service
		//get service
		//call lock on the service
		return service;
	}


	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   *
	protected void returnService(IService service)
	{	
        if(service != null)
        {
            //find appropriate holder
            //return to the service to the holder
            Stack serviceGroup = (Stack)(this.get(service.getDescription()));
            service.resetServiceImplementation();
            
          /**
           * Instead of obtaining a lock to the entire service pool, 
           * obtain a lock to the specific service group. 
           * This way other threads can still access other service groups.
           * 
            synchronized(serviceGroup)
            {
              serviceGroup.push(service);
              lm.debug("Returning " 
                        + service.getDescription() 
                        + " back to the service pool.");
            }
            
        }        
	}  	
*/

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	protected void returnService(IService service)
	{	
    service.resetServiceImplementation();
    lm.debug("Destroying " + service.getDescription());
	}  	

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	protected void loadPool()
	{
		this.initServicePool();	
	}

  /**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	protected void reloadPool()
	{}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	private void addServiceGroup(String description, Stack serviceGroup)
	{		
		this.put(description, serviceGroup); 	
	}

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	private synchronized void initServicePool()
	{
		ArrayList rawServices = null;
    Stack serviceGroup = null;
		String serviceDescription = null;
		ServiceProperty serviceProperty = null;
		Class businessServiceClassImpl = null;
		Object businessServiceObject = null;
		Service service = null;
		
		int poolSize = 0;
		try 
    	{
      		ServiceLoader sl = new ServiceLoader();
      		rawServices = sl.getServiceMappings();
    	} 
    	catch (ResourceNotFoundException rnfe)
    	{
      		lm.error(rnfe);
    	} 
    	catch (FTDApplicationException fae)
    	{
      		lm.error(fae);
    	}

						
		for (int j = 0; j < rawServices.size(); j++)
		{
      serviceGroup = new Stack();
			serviceProperty = (ServiceProperty)(rawServices.get(j));
			poolSize = Integer.valueOf(serviceProperty.getPoolSize()).intValue();
			
			serviceDescription = serviceProperty.getDescription();
			serviceMappings.put(serviceDescription, serviceProperty.getClassName());
							
			for(int i=0; i < poolSize; i++)
			{	
				service = new Service();
				service.setServiceObserver(this.serviceObserver);
				service.setPersistantManager(PersistenceManager.getInstance());
				service.setDescription(serviceDescription);
				
				try 
				{		
					businessServiceClassImpl = Class.forName(serviceProperty.getClassName());
					businessServiceObject = businessServiceClassImpl.newInstance();
				} 
				catch(ClassNotFoundException cnfe)
				{
					lm.error(cnfe);
				} 
				catch(IllegalAccessException iae)
				{
					lm.error(iae);
				} 
				catch (InstantiationException ie)
				{
					lm.error(ie);
				}
									
				if(businessServiceObject != null)
				{
					((IBusinessService) businessServiceObject).setServiceName(serviceDescription);
          service.setServiceImlementation((IBusinessService)businessServiceObject);					
					serviceGroup.add(service);
				}
			}
			
      // if the service group is not empty
      if (! serviceGroup.isEmpty())  {
          this.addServiceGroup(serviceDescription, serviceGroup);
      }
      
			 
		}      
	}


}