package com.ftd.framework.businessservices.serviceprovider;

import com.ftd.framework.common.utilities.ApplicationContext;
import com.ftd.osp.utilities.order.OrderIdGenerator;
import com.ftd.framework.common.utilities.LogManager;
import com.ftd.framework.common.valueobjects.FTDPersistentObject;
import com.ftd.framework.common.valueobjects.FTDPersistentObjectDecorater;
import com.ftd.framework.dataaccessservices.DataManager;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.Timer;



import oracle.toplink.sessions.UnitOfWork;

/**
 * 
 * @author Sasha Secivan
 * @version 1.0 
 **/ 

public class PersistenceManager implements IPersistenceManager  
{
    private static Hashtable persistedObjects = null;
    private static int count = 0;
    private OrderIdGenerator orderIdGenerator = null;
    private DataManager dataManager  = null;
    private Object objTemp = null;
    private Connection connection = null;

    // init here for safety
    private long maxAge = 1000 * 60 * 30 ;
    private static boolean firstRun = true;
    private static final String LOG_MANAGER = "com.ftd.oe";

    private static PersistenceManagerTimerTask task;
    private static Timer timer;
    

  private LogManager lm = new LogManager("com.ftd.framework.businessservices.serviceprovider.PersistenceManager");
    
  /**
   * 
   */
	private PersistenceManager()
	{
        orderIdGenerator = orderIdGenerator.getInstance();
        dataManager = DataManager.getInstance();

        /*
        count++;
        ResourceManager rm = ResourceManager.getInstance();

        if(persistedObjects == null)
        {            
            String strSize = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "InitialPersistedObjectsPoolSize");
            if(strSize == null)
            {
                strSize = "10";
            }
            int size = Integer.parseInt(strSize);
      
            this.persistedObjects = new Hashtable(size);
        }

        // Get the max object age from config file
        String strMaxAge = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "PersistedObjectMaxAge");
        if(strMaxAge == null)
        {
            maxAge = (1000 * 60 * 30);
        }
        else
        {
            maxAge = Long.parseLong(strMaxAge);   
        }
        
        synchronized(this)
        {
            if(firstRun)
            {

                // Get the time between object cache cleanup
                long cleanupInterval;
                String strCacheCleaningInterval = rm.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, "PersistedObjectCleanupInterval");
                if(strCacheCleaningInterval == null)
                {
                    cleanupInterval = (1000 * 60 * 60);
                }
                else
                {
                    cleanupInterval = Long.parseLong(strCacheCleaningInterval);   
                }
        
                task = new PersistenceManagerTimerTask();
                timer = new Timer();
                timer.scheduleAtFixedRate(task, new java.util.Date(), cleanupInterval);
                firstRun = false;
            }
        }
        */
	}
    /**
     * 
     * @see #aaa
     * @param paramName comments
     * @exception XxxxxxException if ...
     * @return comments
     */
    public static PersistenceManager getInstance()
    {
        //THIS IS ONLY TO ASSURE BACKWARDS COMPATIBILITY
        PersistenceManager persistenceManager = new PersistenceManager();
        return persistenceManager;
    }

    /**
     * 
     * @see #aaa
     * @param paramName comments
     * @exception XxxxxxException if ...
     * @return comments
     */
    
    public int getCount()
    {
        return count;
    }
    /**
     * 
     * @see #aaa
     * @param paramName comments
     * @exception XxxxxxException if ...
     * @return comments
     */
	public Object get(String key)
	{        
		Object obj = null;
        FTDPersistentObjectDecorater pODObj = null;
        Connection conn = null;
        byte[] serObj;
        Statement statement = null;
        ResultSet resultSet = null;
        ObjectInputStream objectInputStream = null;
        boolean usingCache = false;
        long dbTimeStamp = 0;
        long cacheTimeStamp = 1;   
        FTDPersistentObject cacheObj = null;
        
        try
        {
            //conn = dataManager.getDumbConnection();
            conn = dataManager.getConnection();
            statement = conn.createStatement();

            FTDPersistentObjectDecorater persistentObjectDecorater = (FTDPersistentObjectDecorater)this.persistedObjects.get(key);
            if(persistentObjectDecorater != null)
            {
                cacheObj = persistentObjectDecorater.getPersistentObject();
            }
      
            if(cacheObj != null)
            {
                cacheTimeStamp = cacheObj.getTimeStamp();
                resultSet = statement.executeQuery("SELECT ORDER_TIMESTAMP FROM ORDER_BLOB WHERE FTD_GUID ='" + key + "'");
                if(resultSet.next())
                {
                    dbTimeStamp = Long.valueOf(resultSet.getString(1)).longValue();
                }
                // Close Result Set
                dataManager.closeResources(resultSet);                
            }

        
            if(dbTimeStamp == cacheTimeStamp)
            {
                long now = System.currentTimeMillis();
                // check to see if the cached object is still fresh
                if ( ( cacheObj.getTimeStamp() + persistentObjectDecorater.getMaxAge() ) > now )  
                {
                    pODObj = (FTDPersistentObjectDecorater)this.persistedObjects.get(key);
                    obj = pODObj.getPersistentObject();
                    usingCache = true;
                    lm.debug("PersistenceManager Cache hit");
                }                
            }
            else
            {
                lm.debug("PersistenceManager Database hit");
                resultSet = statement.executeQuery("SELECT ORDER_CONTENT FROM ORDER_BLOB WHERE FTD_GUID ='" + key + "'");
                if(resultSet.next())
                {
                    objectInputStream = new ObjectInputStream(resultSet.getBlob("ORDER_CONTENT").getBinaryStream());
                    obj = objectInputStream.readObject();  
                }
                else
                {
                    obj = null;
                }
                pODObj = new FTDPersistentObjectDecorater();
            }
        }
        catch(Exception e)
        {
            lm.error("EXCEPTION RETRIEVING ORDER : " + e);
        }
        finally
        {
            lm.debug("Get - Persisted Object cache size: " + persistedObjects.size());
            try
            {
                dataManager.closeResources(resultSet, statement, conn);
                //lm.debug("CLOSED ALL JDBC RESOURCES");
            }
            catch(Exception ex)
            {
                lm.error("ERROR CLOSING RESOURCES", ex);
            }
        }

        if(!usingCache && obj != null)
        {
            this.persistedObjects.remove(key);
            // set the max age
            pODObj.setMaxAge(maxAge);
            pODObj.setPersistentObject((FTDPersistentObject)obj);
            this.persistedObjects.put(key, pODObj);
        }
    
        return ((FTDPersistentObjectDecorater)this.persistedObjects.get(key)).getPersistentObject();
    }

  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void update(String key) 
	{        
        FTDPersistentObjectDecorater pODObj = (FTDPersistentObjectDecorater)this.persistedObjects.get(key);
        if(pODObj == null)
            return;
        Object obj = pODObj.getPersistentObject();
        if(obj == null)
            return; //do throw exception at this point
    
        long _timeStamp = System.currentTimeMillis();
        //String timeStamp = String.valueOf(_timeStamp);
        Connection conn = null;
        byte[] serObj;
        PreparedStatement preparedStatement  = null;
        PreparedStatement preparedStatementUpdate  = null;
        ResultSet rs = null;

        ((FTDPersistentObject)obj).setTimeStamp(_timeStamp);
        pODObj.setMaxAge(maxAge);
        
        try
        {
            serObj = this.serializeObj(obj);
            //conn = dataManager.getDumbConnection();
            conn = dataManager.getConnection();
            conn.setAutoCommit(false);

            preparedStatement = conn.prepareStatement("SELECT ORDER_CONTENT FROM ORDER_BLOB WHERE FTD_GUID = ? FOR UPDATE"); 
            preparedStatement.setString(1, key);
            rs = preparedStatement.executeQuery();

            if(rs.next())
            {
                oracle.sql.BLOB dbBlob = (oracle.sql.BLOB)rs.getBlob(1);
                preparedStatementUpdate = conn.prepareStatement("UPDATE ORDER_BLOB SET ORDER_CONTENT = ?, ORDER_TIMESTAMP = ? WHERE FTD_GUID = ?");
                dbBlob.putBytes(1, serObj);
                preparedStatementUpdate.setBlob(1, dbBlob);
                preparedStatementUpdate.setString(2, String.valueOf(_timeStamp));
                //preparedStatementUpdate.setLong(2, _timeStamp);
                preparedStatementUpdate.setString(3, key);
                preparedStatementUpdate.executeUpdate();      

                conn.commit();
            }
        }
        catch(Exception e)
        {
            lm.error("EXCEPTION UPDATING ORDER : ", e);
        }
        finally
        {
            lm.debug("Update - Persisted Object cache size: " + persistedObjects.size());
            try
            {
                dataManager.closeResources(null, preparedStatementUpdate, null);
                if(conn != null)
                {
                    conn.setAutoCommit(true);
                }
                dataManager.closeResources(rs, preparedStatement, conn);
            }
            catch(Exception ex)
            {
                lm.error("EXCEPTION CLOSING RESOURCES : " + ex);
            }
        }
    }

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void remove(String key)
	{
        Connection conn = null;
        PreparedStatement preparedStatement  = null;

        try
        {
            //conn = dataManager.getDumbConnection();
            conn = dataManager.getConnection();
            preparedStatement = conn.prepareStatement("UPDATE ORDER_BLOB SET STATUS = ? WHERE FTD_GUID = ?");
            preparedStatement.setString(1, "DONE");
            preparedStatement.setString(2, key);
            preparedStatement.executeUpdate();      
        }
        catch(Exception e)
        {
            lm.error("EXCEPTION REMOVING ORDER : ", e);
        }
        finally
        {            
            try
            {
                dataManager.closeResources(null, preparedStatement, conn);
            }
            catch(Exception ex)
            {
                lm.error("ERROR CLOSING RESOURCES: ", ex);
            }
        }    

		persistedObjects.remove(key);
        lm.debug("Remove - Persisted Object cache size: " + persistedObjects.size());
    }

    public String createPersistentObject(Object obj)
	{
        UnitOfWork uow = null;
        String orderId = null;
        Connection conn = null;
        
        try 
        {
            conn = dataManager.getConnection();
            orderId = orderIdGenerator.getOrderId(conn);
        }
        catch(Throwable e)
        {
            lm.error("Error creating order id for persistent object");
            lm.error(e);
        }
        finally
        {
            try
            {
            dataManager.closeResources(null, null, conn);
            }
            catch(Throwable e)
            {
                lm.error("Error closing persistent datamanager object");
                lm.error(e);
            }
            
        }

        try
        
        {

            FTDPersistentObject persistentObject = (FTDPersistentObject) obj;
            persistentObject.setGUID(orderId);
            persistentObject.setTimeStamp(System.currentTimeMillis());
            persistentObject.setHostName((String) ApplicationContext.getInstance().getAttribute("HOST_NAME"));
         
            uow = dataManager.getUnitOfWork();
            uow.registerObject(persistentObject);
            lm.debug("Unit of Work " + uow.getName() + " Registered");
            uow.commit();              
            lm.debug("Unit of Work " + uow.getName() + " Committed");
        } 
        catch(Exception e) {
           lm.error("Error creating persistent object");
           lm.error(e);
        }

        return orderId;
    }

    
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public String create(Object obj)
	{		
            
        String orderId = null;
    
        long _timeStamp = System.currentTimeMillis();
        //String timeStamp = String.valueOf(_timeStamp);
        Connection conn = null;
        byte[] serObj;
        PreparedStatement preparedStatement  = null;

        FTDPersistentObjectDecorater persistentObjectDecorater = new FTDPersistentObjectDecorater();
        persistentObjectDecorater.setPersistentObject((FTDPersistentObject)obj);


        try
        {
            serObj = this.serializeObj(obj);
            //conn = dataManager.getDumbConnection();
            conn = dataManager.getConnection();
            orderId = orderIdGenerator.getOrderId(conn);
            

            ((FTDPersistentObject)obj).setGUID(orderId);
            //((FTDPersistentObject)obj).setTimeStamp(System.currentTimeMillis());
            ((FTDPersistentObject)obj).setTimeStamp(_timeStamp);
            persistentObjectDecorater.setMaxAge(maxAge);
            

            persistedObjects.put(orderId, persistentObjectDecorater);
            
            
            
            
            preparedStatement = conn.prepareStatement("INSERT INTO ORDER_BLOB (FTD_GUID, ORDER_TIMESTAMP, ORDER_CONTENT, STATUS ) VALUES (?, ?, ?, ?)");
            preparedStatement.setString(1, orderId);
            preparedStatement.setString(2, String.valueOf(_timeStamp));
            //preparedStatement.setLong(2, _timeStamp);
            preparedStatement.setBytes(3, serObj);
            preparedStatement.setString(4, "INPROC");
            preparedStatement.execute();      
            preparedStatement.close();
            conn.close();
        }
        catch(Exception e)
        {
            lm.error("EXCEPTION CREATING ORDER : ", e);
        }
        finally
        {
            lm.debug("Create - Persisted Object cache size: " + persistedObjects.size());
            try
            {
                dataManager.closeResources(null, preparedStatement, conn);
            }
            catch(Exception ex)
            {
                lm.error("ERROR CLOSING RESOURCES: ", ex);
            }
        }
    
		return orderId;
	}

    public static void cleanUpCache()
    {
        LogManager lm = new LogManager(LOG_MANAGER);
        lm.info("persistedObjects cache size before cleanup " + persistedObjects.size());

        Set keySet = persistedObjects.keySet();
        List keys = new ArrayList(keySet);

        //Iterator it = keySet.iterator();
        java.util.Date now = null;
        String key = null;
        FTDPersistentObjectDecorater pODObj = null;
        FTDPersistentObject cacheObj = null;
        
        synchronized(persistedObjects)
        {
            //while(it.hasNext())
            for(int i = 0; i < keys.size(); i++)
            {
                //key = (String)it.next();
                key = (String)keys.get(i);
                pODObj = (FTDPersistentObjectDecorater)persistedObjects.get(key);
                cacheObj = pODObj.getPersistentObject();
                now = new java.util.Date();
                if((now.getTime() - cacheObj.getTimeStamp()) > pODObj.getMaxAge())
                {
                    // remove object
                    persistedObjects.remove(key);
                }

                now = null;
            }
        }        
        lm.info("Cleaned up persistedObjects cache");
        lm.info("persistedObjects cache size after cleanup " + persistedObjects.size());        
    }
    
  /**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
  private byte[] serializeObj(Object obj) throws Exception 
  { 
    ByteArrayOutputStream buf = null;
    ObjectOutputStream oos = null;
    byte[] byteArray = null;
  
    try 
      {  
           buf = new ByteArrayOutputStream();
           oos = new ObjectOutputStream(new BufferedOutputStream(buf)); 
           oos.writeObject(obj); 
           oos.flush(); 
           byteArray = buf.toByteArray();
      }  
      catch (Exception se) 
      { 
        throw se;
      }
     finally 
     {
         if (oos != null) 
         {
            oos.close();
         }
          
         
     }
      return byteArray;
  } 

    /**
     * @deprecated
     */
    public synchronized void stopTimerTask()
    {        
        task.cancel();
        timer.cancel();
        lm.info(" Persistence Manager cache cleanup task stopped");
    }
  
}