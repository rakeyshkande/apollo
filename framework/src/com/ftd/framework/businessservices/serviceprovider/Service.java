package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;
import java.io.*;
import java.io.Serializable;
import com.ftd.framework.common.valueobjects.*;
import com.ftd.framework.common.utilities.*;
import com.ftd.framework.businessservices.servicebusinessobjects.*;

/**
 * The base class for all business services
 * 
 * @author Anshu Gaind, Sasha Secivan
 * @version 1.0 
 **/ 

public class Service implements IService, Serializable  
{
	private IBusinessService businessService = null;
	private ServiceObserver serviceObserver = null;
	private IPersistenceManager persistenceManager = null;
	private String description = null;
	private boolean locked = false;
	private String guid = null;
	private GUIDGenerator guidgenerator;

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public Service()
	{
		guidgenerator = GUIDGenerator.getInstance();
		this.guid = guidgenerator.getGUID();
		guidgenerator = null;	
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void setPersistantManager(IPersistenceManager persistenceManager)
	{
		this.persistenceManager = persistenceManager; 	
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void setServiceImlementation(IBusinessService businessService)
	{
		this.businessService = businessService;		
	}
	
  /**
   * Resets the business service
   * 
   */
  public void resetServiceImplementation()
  {
    this.businessService.resetBusinessService();
  }
  

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void setServiceObserver(ServiceObserver serviceObserver)
	{
		this.serviceObserver = serviceObserver; 	
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public String getID()
	{
		return this.guid;	
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public void setDescription(String description)
	{
		this.description = description;	
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */	
	public String getDescription()
	{
		return this.description;	
	}

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void lock()
	{
		this.locked = true;	
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public void unlock()
	{
		this.locked = false;
		this.serviceObserver.notifyUnlock(this);		
	}
	
	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public boolean isLocked()
	{
		return this.locked;	
	}

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	public FTDServiceResponse executeCommand(FTDServiceRequest request) throws Exception
	{				
    String persID = null;
		int persistenceFlag = request.getPersistenceType();
		FTDServiceResponse response = new FTDServiceResponse();
		IBusinessService busvrc = null;
		FTDSystemVO vo = null;
		FTDCommand command = request.getCommand();
		FTDArguments arguments = request.getArguments();  

    try 
    {
          if(persistenceFlag == FTDServiceRequest.USE_OBJECT || persistenceFlag == FTDServiceRequest.REMOVE_OBJECT)
          {			
            if(request.getPersistenceID() == null)
              throw new Exception("Client requested 'USE_OBJECT/REMOVE_OBJECT' but did not supply persistance id"); 	
            
                  if(this.persistenceManager == null)
                  throw new Exception("Persistance manager doesn't exist");
      
                  persID = request.getPersistenceID(); 
                  arguments.put("_PERSISTENT_OBJECT_", persID);	
          }
          else
          if(persistenceFlag == FTDServiceRequest.CREATE_OBJECT)
          {
                  if(this.persistenceManager == null)
                      throw new Exception("Persistance manager doesn't exist");
  
                  // persID = this.persistenceManager.create(this.createPersistentObject());     
            persID = this.persistenceManager.createPersistentObject(this.createPersistentObject());
                  
                  arguments.put("_PERSISTENT_OBJECT_", persID);	
          }
      
          try
          {
              // busvrc = (IBusinessService)(this.deepCopy(this.businessService)); 	
              busvrc = (IBusinessService) this.businessService; 	
          }
          catch(Exception e)
          {
            throw new Exception("Unable to clone business service. Specific: " + e.toString());	
          }
              
          try
          {			
            busvrc.registerCommand(command);
            vo = busvrc.main(command, arguments);
          }
          catch(Exception e)
          {
            throw new Exception("Command execution failed. Error occured within \nSERVICE: " + this.description + "\nCOMMAND " + request.getCommand().getCommand() + "\nSPECIFIC ERROR: " + e.toString());
          }
      
          if(persistenceFlag == FTDServiceRequest.REMOVE_OBJECT)
          {
              if(this.persistenceManager == null)
              throw new Exception("Persistance manager doesn't exist. Unable to remove object");
      
              // this.persistenceManager.remove(persID);
              persID = null;
          }
          else if(persistenceFlag == FTDServiceRequest.CREATE_OBJECT || persistenceFlag == FTDServiceRequest.USE_OBJECT)
          {
            if(this.persistenceManager == null)
              throw new Exception("Persistance manager doesn't exist. Unable to update object");
              // this.persistenceManager.update(persID);
          }
           
        response.setSystemValueObject(vo);	
        response.setPersistanceID(persID);  
      
    } 
    finally 
    {
      this.unlock();
    }
    
	  return response;	
	}	

	/**
   * 
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */

    /*
	private Object createPersistentObject() throws Exception
	{		
      Class c = null;
      Object o = null;
      try 
      {		
        c = Class.forName("com.ftd.applications.oe.common.OEOrder");
        o = c.newInstance();
      } 
      catch(ClassNotFoundException cnfe)
      {
  //      lm.error(cnfe);
        throw new Exception("Unable to create persistant object");
      } 
      catch(IllegalAccessException iae)
      {
   //     lm.error(iae);
        throw new Exception("Unable to create persistant object");
      } 
      catch (InstantiationException ie)
      {
    //    lm.error(ie);
        throw new Exception("Unable to create persistant object");
      }
      return o;
	}
    */
    

    private Object createPersistentObject() throws Exception
	{		
        Class c = null;
        Object o = null;
        
        try 
        {		
            c = Class.forName("com.ftd.applications.oe.common.persistent.OrderPO");
            o = c.newInstance();
        } 
        catch(ClassNotFoundException cnfe)
        {
            throw new Exception("Unable to create persistant object");
        } 
        catch(IllegalAccessException iae)
        {
            throw new Exception("Unable to create persistant object");
        } 
        catch (InstantiationException ie)
        {
            throw new Exception("Unable to create persistant object");
        }
        
        return o;
	}

  /**
   * @deprecated
   * @see #aaa
   * @param paramName comments
   * @exception XxxxxxException if ...
   * @return comments
   */
	private Object deepCopy(Object oldObj) throws Exception
	{
		ObjectOutputStream out = null;
		ObjectInputStream in = null;
		Object newobj = null;
		
		try
		{		
			ByteArrayOutputStream buf = new ByteArrayOutputStream();
      		out = new ObjectOutputStream(buf);
        	out.writeObject(oldObj);
      		in = new ObjectInputStream(new ByteArrayInputStream(buf.toByteArray()));     

        	newobj = in.readObject();
			return newobj;		 
		}
		catch(Exception e)
		{
			throw(e);
		}
		finally
		{
			out.close();
			in.close();
		}
	}	 	

}