package com.ftd.framework.businessservices.serviceprovider;

import java.util.*;
import com.ftd.framework.common.valueobjects.*;

/**
 * The base interface for all services.
 * 
 * @author Sasha Secivan
 * @version 1.0 
 **/ 

interface IPersistenceManager  
{
    public String createPersistentObject(Object obj);
	public String create(Object obj);
	public Object get(String id);
	public void update(String key);
	public void remove(String key);		 	
}