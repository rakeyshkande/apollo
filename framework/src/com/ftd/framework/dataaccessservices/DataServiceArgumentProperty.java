package com.ftd.framework.dataaccessservices;

public class DataServiceArgumentProperty 
{
  private String name = null;
  private String type = null;
  private String index = null;

  public DataServiceArgumentProperty()
  {}

  public void setName(String name)
  {
    this.name = name;
  }

  public String getName()
  {
    return this.name;
  }

  public void setType(String type)
  {
    this.type = type; 
  }

  public String getType()
  {
    return this.type;
  }

  public void setIndex(String index)
  {
    this.index = index;
  }

  public String getIndex()
  {
    return this.index;
  }
}