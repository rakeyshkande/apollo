package com.ftd.framework.dataaccessservices;

import com.ftd.applications.oe.common.persistent.OrderPO;
import oracle.toplink.publicinterface.Descriptor;
import oracle.toplink.publicinterface.UnitOfWork;
import oracle.toplink.threetier.ClientSession;
import oracle.toplink.threetier.ServerSession;
import oracle.toplink.tools.sessionconfiguration.XMLSessionConfigLoader;
import oracle.toplink.tools.sessionmanagement.SessionManager;

public class TestTopLink
{
    public TestTopLink()
    {
    }
    
    public void testIt()
    {
      try
      {
          oracle.toplink.internal.helper.ConversionManager.getDefaultManager().setShouldUseClassLoaderFromCurrentThread(true);
          ServerSession toplinkSession = (ServerSession) SessionManager.getManager().getSession(new XMLSessionConfigLoader(), "OrderPersistence", this.getClass().getClassLoader(), true, false);
          
          System.out.println("TopLink session acquired successfully");

          java.util.Map descriptors = toplinkSession.getDescriptors();
          java.util.Iterator descIt = descriptors.values().iterator();
          while (descIt.hasNext())
          {
              Descriptor descriptor = (Descriptor) descIt.next();
              System.out.println("  Descriptor = " + descriptor.getJavaClassName() + " " + descriptor.isFullyInitialized());
          }

          
          ClientSession clientSession = toplinkSession.acquireClientSession();
          UnitOfWork uow = clientSession.acquireUnitOfWork();

          OrderPO po = new OrderPO();
          uow.registerObject(po);

      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }

    
    public static void main(String[] args)
    {
        TestTopLink ttl = new TestTopLink();
        ttl.testIt();
    }
}
