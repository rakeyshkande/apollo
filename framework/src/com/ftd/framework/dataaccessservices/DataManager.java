package com.ftd.framework.dataaccessservices;

import com.ftd.framework.common.exceptions.BadConnectionException;
import com.ftd.framework.common.exceptions.FTDSystemException;
import com.ftd.framework.common.utilities.FrameworkConstants;
import com.ftd.framework.common.utilities.LogManager;
import com.ftd.framework.common.utilities.ResourceManager;
import com.ftd.osp.utilities.j2ee.DataSourceUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import oracle.toplink.jndi.JNDIConnector;
import oracle.toplink.logging.DefaultSessionLog;
import oracle.toplink.publicinterface.Descriptor;
import oracle.toplink.sessions.UnitOfWork;
import oracle.toplink.threetier.*;
import oracle.toplink.threetier.ServerSession;
import oracle.toplink.tools.sessionconfiguration.XMLLoader;
import oracle.toplink.tools.sessionconfiguration.XMLSessionConfigLoader;
import oracle.toplink.tools.sessionmanagement.SessionManager;


/*
 * Encapsulates all interactions with a database. Acts as a facade to 
 * com.ftd.osp.utilities.j2ee.DataSourceUtil
 *
 * @author Anshu Gaind
 * @version 1.0
 **/
public class DataManager  {

  private static DataManager DATAMANAGER = new DataManager();
  //private DataSource ds;
  private static ServerSession toplinkSession;
  private LogManager logger = new LogManager("com.ftd.framework.dataaccessservices.DataManager");

  /**
   * The private constructor
   **/
   private DataManager(){
    super();
   }

 /**
  * The static initializer for the Data Manager. It ensures that
  * at a given time there is only a single instance
  *
  * @return the data manager
  * @author Anshu Gaind
  **/
  public static DataManager getInstance()
  {
      return DATAMANAGER;
  }

  /**
   * @deprecated
   */
  public Connection getDumbConnection() throws Exception
  {
        return this.getConnection();
  }


  /**
   * Returns a default connection from the connection pool. It has been refactored
   * to act as an adapter to the DataSourceUtil
   *
   * @return connection to the database
   * @throws BadConnectionException
   * @throws FTDSystemException
   * @author Anshu Gaind
   **/
  public Connection getConnection() throws BadConnectionException, FTDSystemException {
       
      try 
      {
        ResourceManager resourceManager = ResourceManager.getInstance();
        String dataSource = resourceManager.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, FrameworkConstants.APPLICATION_DB_DATASOURCE_KEY);
        
        logger.debug("Returning database connection");
        return DataSourceUtil.getInstance().getConnection(dataSource);
        
      } catch (Exception ex) 
      {
        logger.error(ex);
        throw new BadConnectionException(ex);
      }       

  }

  /**
   * Returns a connection from the connection pool, for the specified data source. 
   * It has been refactored to act as an adapter to the DataSourceUtil
   *
   * @param dataSource the name of the data source
   * @param userName the user name
   * @param password the password
   * @return connection to the database
   * @throws BadConnectionException
   * @throws FTDSystemException
   * @author Anshu Gaind
   **/
  public Connection getConnection(String dataSource, String userName, String password) throws BadConnectionException, FTDSystemException {
      try 
      {
        
        logger.debug("Returning database connection");
        return DataSourceUtil.getInstance().getConnection(dataSource); 
        
      } catch (Exception ex) 
      {
        logger.error(ex);
        throw new BadConnectionException(ex);
      }       

  }


  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(ResultSet resultSet) throws BadConnectionException {
      try{
        if (resultSet != null){
          resultSet.close();
        }
      } catch (SQLException se){
         logger.error(se);
         throw new BadConnectionException(se);
      }
      return true;
  }

  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(ResultSet resultSet,
                                 Statement statement) throws BadConnectionException {
      try{
        if (resultSet != null){
          resultSet.close();
        }
        if (statement != null){
          statement.close();
        }
      } catch (SQLException se){
         logger.error(se);
         throw new BadConnectionException(se);
      }
      return true;
  }

  /**
   * Closes all open JDBC resources
   *
   * @param resultSet the result set
   * @param statement the statement
   * @param connection the connection
   * @throws BadConnectionException
   * @returns whether or not all resources were closed
   **/
  public boolean closeResources(ResultSet resultSet,
                                 Statement statement,
                                Connection connection) throws BadConnectionException {
      try{
        if (resultSet != null){
          resultSet.close();
        }
        if (statement != null){
          statement.close();
        }
        if (connection != null){
          connection.close();
        }
      } catch (SQLException se){
         logger.error(se);
         throw new BadConnectionException(se);
      }

      return true;
  }

  /**
   * Returns a TopLink Server Session
   */
  public ServerSession getSession()
  {
    try
    {
      // if the toplink server session is null or not connected
      if( toplinkSession == null || (! toplinkSession.isConnected()) )
      {
//        ResourceManager resourceManager = ResourceManager.getInstance();
//        String dataSource = resourceManager.getProperty(FrameworkConstants.APPLICATION_FRAMEWORK_CONFIG_REALM_KEY, FrameworkConstants.APPLICATION_DB_DATASOURCE_KEY);

//        DataSource ds = DataSourceUtil.getInstance().getDataSource(dataSource);
        oracle.toplink.internal.helper.ConversionManager.getDefaultManager().setShouldUseClassLoaderFromCurrentThread(true);
        toplinkSession = (ServerSession) SessionManager.getManager().getSession(new XMLSessionConfigLoader(), "OrderPersistence", this.getClass().getClassLoader(), true, false);
//        toplinkSession = (ServerSession) SessionManager.getManager().getSession(new XMLLoader(), "OrderPersistence", this.getClass().getClassLoader(), false, false);
//        toplinkSession.getLogin().setConnector(new DataSourceConnector(ds));
//        toplinkSession.getLogin().setConnector(new JNDIConnector(ds));
//        toplinkSession.getLogin().setUserName("ftd_apps");
//        toplinkSession.getLogin().setPassword("ftd_apps");
//        toplinkSession.login();

        java.util.Map descriptors = toplinkSession.getDescriptors();
        java.util.Iterator descIt = descriptors.values().iterator();
        while (descIt.hasNext())
        {
            Descriptor descriptor = (Descriptor) descIt.next();
            logger.debug("  Descriptor = " + descriptor.getJavaClassName());
        }
        
        logger.debug("TopLink session acquired successfully");
      }
    }
    catch(Exception e)
    {
      logger.error(e);
    }
            
    return toplinkSession;
  }


  /**
   * Returns a TopLink Unit of Work
   */
  public UnitOfWork getUnitOfWork()
  {
    ClientSession clientSession = this.getSession().acquireClientSession();
    UnitOfWork uow = clientSession.acquireUnitOfWork();
    logger.debug("Acquiring TopLink Unit of Work from Client Session");
    return uow;
    
  }

}
