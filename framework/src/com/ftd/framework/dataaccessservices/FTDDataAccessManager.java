package com.ftd.framework.dataaccessservices;

import java.util.*;

public class FTDDataAccessManager
{
    private static HashMap serviceProperties = null;
    private DataManager dataManager = null; 
		
	public FTDDataAccessManager()
	{
      dataManager = DataManager.getInstance();
      if(serviceProperties == null)
      {
        DataServicePropertyLoader loader = new DataServicePropertyLoader();
        loader.loadClientProperties();
        serviceProperties = loader.getServiceProperties();
      }
    }

    public void registerDataBridge(BusinessDataBridge bridge)
    {
      bridge.setDataManager(dataManager);
    }
	
	public GenericDataService getGenericDataService(String clientID)
	{
		GenericDataService service = new GenericDataService();
        service.setClientProperty((DataServiceClientProperty)serviceProperties.get(clientID));
        service.setDataManager(dataManager);
        service.setType(clientID);
			
		return service; 	
   }

   public IDataServiceBase getCustomDataService(String serviceName) throws Exception 
   {
      Class dataServiceClassImpl = null;
      Object dataServiceObject = null;
      boolean exception = false;
      String exceptionMessage = null;

      try 
			{		
					dataServiceClassImpl = Class.forName(serviceName);
					dataServiceObject = dataServiceClassImpl.newInstance();
			} 
			catch(ClassNotFoundException cnfe)
			{
          exception = true;
          exceptionMessage = cnfe.toString();
					//lm.error(cnfe);
			} 
			catch(IllegalAccessException iae)
			{
          exception = true;
          exceptionMessage = iae.toString();
					//lm.error(iae);
			} 
			catch (InstantiationException ie)
			{
          exception = true;
          exceptionMessage = ie.toString();
					//lm.error(ie);
			}

      if(exception)
      {
        throw new Exception("No such data service. Specific reason: " + exceptionMessage);
      }

      return (IDataServiceBase)dataServiceObject;
   }
}