package com.ftd.framework.dataaccessservices;

import com.ftd.framework.common.valueobjects.FTDDataRequest;
import com.ftd.framework.common.valueobjects.FTDDataResponse;

public abstract class DataServiceBase 
{
  public abstract void setDataManager(DataManager dm);
  public abstract FTDDataResponse execute(FTDDataRequest request);
}