package com.ftd.framework.dataaccessservices;

import java.util.*;
import java.net.URL;
import com.ftd.framework.common.exceptions.*;
import com.ftd.framework.common.utilities.*;
import oracle.xml.parser.v2.*;
import org.w3c.dom.*;

public class DataServicePropertyLoader  
{
  private oracle.xml.parser.v2.XMLDocument document = null;
  private LogManager lm = new LogManager("com.ftd.framework.dataaccessservices.DataServicePropertyLoader");
  
  public DataServicePropertyLoader() 
  {}

  public void loadClientProperties()
  {
    this.loadSettingsFile();
  }

  public HashMap getServiceProperties()
  {
    Element element = null;
    NodeList nl = null;
    HashMap clients = new HashMap();
    String query_string = null;
    DataServiceClientProperty clientProperty = null;
    
    try
    {
        query_string = 	"/commands/command";
        nl = document.selectNodes(query_string);

        if (nl.getLength() > 0)
        {
          for(int i=0; i < nl.getLength(); i++)
          {
            element = (Element)nl.item(i);
            clientProperty = new DataServiceClientProperty();
            clientProperty.setLabel(element.getAttribute("procsetlbl"));
            clientProperty.setName(element.getAttribute("name"));
            clientProperty.setProcedures(this.getProcedureProperties(element.getAttribute("name")));
            clients.put(element.getAttribute("name"), clientProperty);
          }
        }
    }
    catch(Exception e)
    {
      e.toString();
      lm.error(e);
    }
    return clients;
  }

  private ArrayList getProcedureProperties(String commandName)
  {
    ArrayList procedures = new ArrayList();
    Element element = null;
    NodeList nl = null;
    String query_string = null;
    DataServiceProcedureProperty procedureProperty =  null;

    try
    {
        query_string = 	  "/commands/command[@name='" + commandName + "']/procedure";
        nl = document.selectNodes(query_string);

        if (nl.getLength() > 0)
        {
          for(int i=0; i < nl.getLength(); i++)
          {
            element = (Element)nl.item(i);
            procedureProperty = new DataServiceProcedureProperty();
            procedureProperty.setRecordLabel(element.getAttribute("recordlbl"));
            procedureProperty.setRecordsetLabel(element.getAttribute("recordsetlbl"));
            procedureProperty.setName(element.getAttribute("value"));
            procedureProperty.setID(element.getAttribute("id"));
            procedureProperty.setArguments(this.getArgumentProperties(commandName, element.getAttribute("id")));
            procedures.add(procedureProperty);
          }
      }
    }
    catch(Exception e)
    {
      e.toString();
      lm.error(e);
    }

    return procedures;
  }

  private ArrayList getArgumentProperties(String command, String procedureID)
  {
    Element element = null;
    NodeList nl = null;
    String query_string = null;
    ArrayList arguments = new ArrayList();
    DataServiceArgumentProperty argumentProperty =  null;

    try
    {
        query_string = 	  "/commands/command[@name='" + command + "']/procedure[@id='" + procedureID + "']/argument";
        nl = document.selectNodes(query_string);

        if (nl.getLength() > 0)
        {
          for(int i=0; i < nl.getLength(); i++)
          {
            element = (Element)nl.item(i);
            argumentProperty = new DataServiceArgumentProperty(); 
            argumentProperty.setName(element.getAttribute("name"));
            argumentProperty.setIndex(element.getAttribute("index"));
            argumentProperty.setType(element.getAttribute("type"));
            arguments.add(argumentProperty);
          }
      }
    }
    catch(Exception e)
    {
      lm.error(e);
    }

    return arguments;
  }

  private void loadSettingsFile()
  {
    URL url = null;
    try
    {
      url = (ResourceManager.getInstance()).getResource(FrameworkConstants.APPLICATION_DAO_MAPPING_XML);
    }
    catch(Exception e)
    {
      e.toString();
      lm.error(e);
    }
    DOMParser parser = null; 

    try
    {
         parser = new DOMParser();
         parser.parse(url.toString());
         document = parser.getDocument();
    }
    catch(Exception e)
    {
        lm.error(e);
    }
 
  } 
}