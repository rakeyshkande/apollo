package com.ftd.framework.security;

public class SecurityConstants 
{
    public final static String USERPROFILE_USER_ID = "userID";
    public final static String USERPROFILE_ROLE_ID = "roleID";
    public final static String USERPROFILE_AUTHENTICATED = "authenticated";
    public final static String USERPROFILE_FIRST_NAME = "firstName";
    public final static String USERPROFILE_LAST_NAME = "lastName";
    public final static String USERPROFILE_ACTIVE_FLAG = "activeFlag";
    public final static String USERPROFILE_CALL_CENTER_ID = "callCenterID";
    public final static String USERPROFILE_LOGON_ATTEMPTS = "logonAttempts";
    public final static String USERPROFILE_CURRENT_PASSWORD = "currentPassword";
    public final static String USERPROFILE_CURRENT_PASSWORD_DATE = "currentPasswordDate";
    public final static String USERPROFILE_LAST_UDPATE_DATE = "lastUpdateDate";
    public final static String USERPROFILE_LAST_UPDATE_USER = "lastUpdateUser";
    public final static String USERPROFILE_HOME_PHONE = "homePhone";
    public final static String USERPROFILE_ADDRESS_1 = "address1";
    public final static String USERPROFILE_ADDRESS_2 = "address2";
    public final static String USERPROFILE_CITY = "city";
    public final static String USERPROFILE_STATE = "state";
    public final static String USERPROFILE_POSTAL_CODE = "postalCode";

    public final static String ROLE_ROLE_ID = "roleID";
    public final static String ROLE_DESCRIPTION = "description";
    public final static String ROLE_LAST_UPDATE_DATE = "lastUpdateDate";
    public final static String ROLE_LAST_UPDATE_USER = "lastUpdateUser";
     
    public final static String ROLEFUNCTION_ROLE_ID = "roleID";
    public final static String ROLEFUNCTION_FUNCTION_ID = "functionID";
    public final static String ROLEFUNCTION_DESCRIPTION = "description";
}