package com.ftd.eventhandling.events;
import java.io.Serializable;

public class Event implements Serializable
{
  public String eventName;
  private String contextName;
  private String description;
  private boolean active;

  public Event()
  {
  }



  public String getEventName()
  {
    return eventName;
  }

  public void setEventName(String eventName)
  {
    this.eventName = eventName;
  }

  public String getContextName()
  {
    return contextName;
  }

  public void setContextName(String contextName)
  {
    this.contextName = contextName;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public boolean isActive()
  {
    return active;
  }
  
  public void setActive(boolean active)
  {
    this.active = active;
  }
}