
package com.ftd.eventhandling.events;
import com.ftd.eventhandling.eventmanager.EventHandlingUtility;
import com.ftd.eventhandling.eventmanager.ResourceNameRegister;
import com.ftd.osp.utilities.plugins.Logger;

import java.io.IOException;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import javax.sql.DataSource;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 *
 * @author Anshu Gaind
 * @version $Id: EventHandler.java,v 1.3 2011/06/30 15:05:59 gsergeycvs Exp $
 */

public abstract class EventHandler
{
  private Logger logger = new Logger("com.ftd.eventhandling.events.EventHandler");
  /**
   * Invoke the event handler
   *
   * @payload payload
   * @throws java.lang.Throwable
   */
  public abstract void invoke(Object payload) throws Throwable;

  /**
   * Returns a transactional resource from the EJB container.
   * 
   * @param jndiName
   * @return 
   * @throws javax.naming.NamingException
   */
  public Object lookupResource(String jndiName)
              throws NamingException
  {
    return EventHandlingUtility.lookupResource(jndiName);
  }
  
  
  /**
   * Returns the information for the specified event
   * 
   * @param eventName
   * @param context
   * @return 
   * @throws javax.naming.NamingException
   * @throws org.xml.sax.SAXException
   * @throws javax.xml.parsers.ParserConfigurationException
   * @throws java.io.IOException
   * @throws java.sql.SQLException
   */
  public Event getEvent(String eventName, String context) 
    throws NamingException,SAXException, ParserConfigurationException, IOException, SQLException, Exception
  {
    Connection con = null;
    Event event = null;
    String eventsLogDataSourceName = null;
    try 
    {
      eventsLogDataSourceName = (String)ResourceNameRegister.getRegisteredValue("Events Log Data Source Name");
      DataSource dataSource = (DataSource) EventHandlingUtility.lookupResource(eventsLogDataSourceName);
      con = dataSource.getConnection();
      event = EventHandlingUtility.getEvent(eventName, context, con);
    } finally 
    {
        if (con != null && (!con.isClosed())) 
        {
            con.close();
            logger.debug(eventsLogDataSourceName + " Connection Released");
        }
    }    
    return event;    
  }
}