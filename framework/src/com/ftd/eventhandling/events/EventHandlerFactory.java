package com.ftd.eventhandling.events;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.xml.DOMUtil;

/**
 *
 * @author Anshu Gaind
 * @version $Id: EventHandlerFactory.java,v 1.3 2011/06/30 15:05:59 gsergeycvs Exp $
 */
public class EventHandlerFactory
{
  private static EventHandlerFactory EVENTHANDLERFACTORY = new EventHandlerFactory();
  private static final String configFileName = "event-handlers.xml";
  private static boolean SINGLETON_INITIALIZED;
  private static Logger logger  = new Logger("com.ftd.eventhandling.events.EventHandlerFactory");

  private File configFile;
  private long lastModified;
  /**
   * Collection of all event handlers
   * @link aggregationByValue
   * @associates <{com.ftd.eventhandling.events.EventHandler}>
   */
  protected Map eventHandlers;

  /**
   * The private construtor
   */
  private EventHandlerFactory()
  {
    super();
  }

  /**
   * Returns a reference to the event handler factory
   *
   * @return the event handler factory
   */
  public static EventHandlerFactory getInstance() throws Exception
  {
    if (! SINGLETON_INITIALIZED )
    {
        EVENTHANDLERFACTORY.init();
    }

    return EVENTHANDLERFACTORY;
  }

  /**
   * Initialize the factory
   * @throws java.lang.Exception
   */
  private synchronized void init() throws Exception
  {
    if (! SINGLETON_INITIALIZED)
    {
      URL url = ResourceUtil.getInstance().getResource(configFileName);
      if (url != null)
      {
        // load xml configuration
        configFile = new File(url.getFile());
        Document eventHandlersDoc = (Document)DOMUtil.getDocument(ResourceUtil.getInstance().getResourceAsStream(configFileName));
        // load the xml configuration into a java object
        eventHandlers = new HashMap();
        NodeList eventHandlerList = DOMUtil.selectNodes(eventHandlersDoc, "/event-handlers/event-handler");
        String eventHandler = null, event = null;
        NodeList eventList = null;
        Element eventHandlerNode = null;
        Node eventNode = null;
        // for each event handler
        for (int i = 0; i < eventHandlerList.getLength(); i++)
        {
          eventHandlerNode = (Element)eventHandlerList.item(i);
          eventHandler = eventHandlerNode.getAttribute("class");
          eventList = DOMUtil.selectNodes(eventHandlerNode, "events/event-name");
          // for each event
          for (int j = 0; j < eventList.getLength(); j++)
          {
            eventNode = eventList.item(j);
            event = (String)DOMUtil.selectSingleNode(eventNode, "text()").getNodeValue();
            eventHandlers.put(event, eventHandler);
            logger.info("Assigning Event " + event + " to handler " + eventHandler);
          }
        }
        // set the last modified timestamp
        this.lastModified = configFile.lastModified();
        SINGLETON_INITIALIZED = true;
      }
      else
      {
        throw new IOException("The configuration file " + configFile + " was not found.");
      }
    }
  }

  /**
   * Reloads the configuration file
   *
   * @throws java.lang.Exception
   */
  private void reloadConfig() throws Exception
  {
      SINGLETON_INITIALIZED = false;
      EVENTHANDLERFACTORY.init();
  }


  /**
   * Returns the appropriate event handler
   *
   * @param event the name of the event
   * @return the appropriate event handler
   * @throws java.lang.Exception
   */
  public EventHandler getEventHandler(String event) throws Exception
  {
    // Check timestamp of modification
    if (this.lastModified < this.configFile.lastModified())
    {
      this.reloadConfig();
    }
    /**
     * An object pool offers no significant performance benefits in the later
     * JVMs. Creating a brand new instance avoids thread synchronization issues.
     */
    return (EventHandler) Class.forName((String)eventHandlers.get(event)).newInstance();
  }

  /**
   * Returns the appropriate event handler.
   *
   * The context is used to pass any runtime context to the event handler.
   * The event handler can then utilize the context to obtain transactinal
   * resources like database connections, JMS sessions, EJB contexts, etc.
   *
   * @param event the name of the event
   * @param context the context for the event handler
   * @return
   * @throws java.lang.Exception
   */
  public EventHandler getEventHandler(String event, Object context) throws Exception
  {
    // Check timestamp of modification
    if (this.lastModified < this.configFile.lastModified())
    {
      this.reloadConfig();
    }
    /**
     * An object pool offers no significant performance benefits in the later
     * JVMs. Creating a brand new instance avoids thread synchronization issues.
     */
    Class eventHandlerClass =  Class.forName((String)eventHandlers.get(event));
    EventHandler eventHandler = (EventHandler) eventHandlerClass.newInstance();
    return eventHandler;
  }

}