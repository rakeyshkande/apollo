package com.ftd.eventhandling.eventmanager;

/**
 * @deprecated
 * @author Anshu Gaind
 * @version $Id: FTDNSResolver.java,v 1.3 2011/06/30 15:05:58 gsergeycvs Exp $
 */

class FTDNSResolver 

{
  public FTDNSResolver()
  {
  }

  /**
   * Find the namespace definition in scope for a given namespace prefix.
   *
   * @param prefix
   * @return the resolved Namespace (null, if prefix could not be resolved)
   */
  public String resolveNamespacePrefix(String prefix)
  {
    if (prefix == "ftd")
    {
        return "http://www.ftd.com/schema";
    }
    else
    {
      return null;
    }


  }
}