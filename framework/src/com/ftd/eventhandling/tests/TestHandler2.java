package com.ftd.eventhandling.tests;
import com.ftd.eventhandling.events.Event;
import com.ftd.eventhandling.events.EventHandler;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import com.ftd.osp.utilities.xml.DOMUtil;
import javax.sql.DataSource;

import org.w3c.dom.Document;

/**
 * 
 * @author Anshu Gaind
 * @version $Id: TestHandler2.java,v 1.3 2011/06/30 15:05:59 gsergeycvs Exp $
 */
public class TestHandler2 extends EventHandler
{
  private static Logger logger  = new Logger("com.ftd.eventhandling.tests.TestHandler2");
  private Object context;
  
  public TestHandler2()
  {
    super();
  }

  /**
   * Invoke the event handler
   * 
   * @param payload
   * @throws java.lang.Throwable
   */
  public void invoke(Object payload) throws Throwable
  {
    logger.info("Test Handler 2 Invoked");
    String message = (String)((MessageToken)payload).getMessage();    
    logger.info(message);
    Document doc = DOMUtil.getDocument(message);
    DOMUtil.print(doc, System.out);
    DataSource ds = (DataSource) super.lookupResource("jdbc/EVENTS_QDS");
    Event event = super.getEvent("SEND-PRICING-FEED", "TEST");
    logger.info(event.getEventName());
    logger.info(event.getContextName());
    logger.info(event.getDescription());
    logger.info(String.valueOf(event.isActive()));
}
  

}