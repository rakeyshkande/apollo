
package com.ftd.osp.framework.receiver;
import com.ftd.osp.framework.dispatcher.Dispatcher;
import com.ftd.osp.utilities.interfaces.IBusinessObject;
import com.ftd.osp.utilities.j2ee.InitialContextUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.BusinessConfig;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;

import java.util.Iterator;

import javax.ejb.MessageDrivenBean;
import javax.ejb.MessageDrivenContext;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import javax.naming.Context;
import javax.naming.InitialContext;

import javax.sql.DataSource;

public class ReceiverMDB implements MessageDrivenBean, MessageListener  {
  protected MessageDrivenContext  messageDrivenContext;
  protected String         businessObjectClassName;
  protected String         rollbackStatusName;
  protected String         rollbackWarningStatus;
  protected Boolean        boRequiresJDBCConnection;
  protected Boolean        dispatchOrderToQueue;
  protected Boolean        persistMessageToken;
  protected Logger         logger;
  protected String         userName;
  protected String         password;
  protected String         dataSourceName;
  protected DataSource     dataSource;
  protected Context        myenv;
  protected String         componentRole;

  protected static int     count;


  public void ejbCreate() {
  }

  /**
   * A container invokes this method before it ends the life of the message-driven object.
   * This happens when a container decides to terminate the message-driven object.
   * This method is called with no transaction context.
   *
   * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
   */
  public void ejbRemove() {
  }

  /**
   * Set the associated message-driven context. The container calls this method after the instance creation.
   * The enterprise Bean instance should store the reference to the context object in an instance variable.
   * This method is called with no transaction context.
   *
   * @param ctx A MessageDrivenContext interface for the instance.
   * @exception EJBException Thrown by the method to indicate a failure caused by a system-level error.
   */
  public void setMessageDrivenContext(MessageDrivenContext ctx) {
    this.messageDrivenContext = ctx;
    InitialContext initContext = null;

    try  {
      logger = new Logger("com.ftd.osp.framework.receiver.Receiver");
      initContext = new InitialContext();
      logger.info("Begin Registering Component");
      businessObjectClassName = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Business Object Class Name", String.class);
      logger.debug("Business Object Class Name :: " + businessObjectClassName);
      rollbackStatusName = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Rollback Status Name", String.class);
      logger.debug("Rollback Status Name :: " + rollbackStatusName);
      rollbackWarningStatus = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Rollback Warning Status", String.class);
      logger.debug("Rollback Warning Status :: " + rollbackWarningStatus);
      boRequiresJDBCConnection = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Business Object Requires JDBC Connection", Boolean.class);
      logger.debug("Business Object Requires JDBC Connection :: " + boRequiresJDBCConnection);
      if (boRequiresJDBCConnection.booleanValue())  {
        dataSourceName = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Data Source Name", String.class);
        logger.debug("Data Source Name :: " + dataSourceName);
        dataSource = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,dataSourceName, DataSource.class);
      }

      dispatchOrderToQueue = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Dispatch Order To Queue", Boolean.class);
      logger.info("Dispatch Order To Queue :: " + dispatchOrderToQueue);
      persistMessageToken  = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Persist Message Token", Boolean.class);
      logger.debug("Persist Message Token :: " + persistMessageToken);
      componentRole  = InitialContextUtil.getInstance().lookupEnvironmentEntry(initContext,"Component Role", String.class);
      logger.info("Component Role :: " + componentRole);
      logger.info("End Registering Component");

    } catch (Exception ex)  {
      logger.error(ex);
    } finally  {
      ++count;
      logger.debug("MDB Count " + count);
      try  {
        initContext.close();
      } catch (Exception ex)  {
        logger.error(ex);
      } finally  {
      }
    }

  }


  /**
   * Passes a message to the listener.
   *
   * @param message the message passed to the listener
   */
  public void onMessage(Message msg) {
    Connection connection = null;
    MessageToken inboundMessageToken = null;
    boolean warningRollbackOnly = false;

    try  {
      logger.debug("***********************************************************");
      logger.info("* BEGIN " + componentRole + " TRANSACTION *");
      logger.debug("***********************************************************");
      /*********** BEGIN MESSAGE EXTRACTION ***********/
      // create the business config object
      BusinessConfig businessConfig = new BusinessConfig();

      // acquire JDBC connection
      if (boRequiresJDBCConnection.booleanValue())  {
        // rely on the user name and password that is configured on the data source
        connection = dataSource.getConnection();
        logger.debug("JDBC Connection acquired");
        businessConfig.setConnection(connection);
      }

      // get the message token from the message
      inboundMessageToken = this.getMessageToken(msg);

      // set the order progression message on the message token
      inboundMessageToken.setProgressionMessage(componentRole);

      // set  message token on the business config object
      businessConfig.setInboundMessageToken(inboundMessageToken);
      /*********** END MESSAGE EXTRACTION ***********/

      /*********** BEGIN MESSAGE PROCESSING ***********/
      // call the appropriate business object
      logger.info("Calling business object " + businessObjectClassName);
      Class boClass = Class.forName(businessObjectClassName);
      IBusinessObject businessObject = (IBusinessObject) boClass.newInstance();
      businessObject.execute(businessConfig);
      /*********** END MESSAGE PROCESSING ***********/

      /*********** BEGIN MESSAGE DISPATCH ***********/
        // Call the dispatcher for each message token.
        Dispatcher dispatcher = null;
        Object temp = null;
        MessageToken outboundMessageToken = null;

        for (Iterator iter = businessConfig.getOutboundMessageTokens(); iter.hasNext(); )  {
            temp = iter.next();
            // check to seee if the message token is null
            if (temp == null) {
                throw new Exception ("The Message Token is null");
            }

            outboundMessageToken = (MessageToken)temp;
            // check to see if the status on the message token is null
            if (outboundMessageToken.getStatus() == null) {
                throw new Exception ("The status on the Message Token is null");
            } else {
              logger.debug("Received status " + outboundMessageToken.getStatus() + " from " + businessObjectClassName);

              // Check for rollback called by the business object
              if (outboundMessageToken.getStatus().equalsIgnoreCase(rollbackStatusName))  {
                   throw new Exception("The Business Object has requested a transaction rollback");
              }
              if (outboundMessageToken.getStatus().equalsIgnoreCase(rollbackWarningStatus))  {
                   warningRollbackOnly = true;
                   throw new Exception("The Business Object has requested a warning transaction rollback");
              }

              // check to see if the component has been configured to send messages any further
              if (dispatchOrderToQueue.booleanValue())  {
                  if (dispatcher == null)  {
                      dispatcher = Dispatcher.getInstance();
                  }
                  logger.debug("Calling Dispatcher for token with status of " + outboundMessageToken.getStatus());
                  // check for the null message queue status
                  if (!outboundMessageToken.getStatus().equalsIgnoreCase("0000"))  {
                      dispatcher.dispatch(myenv, outboundMessageToken);
                  }
			        } else {
			          logger.debug( "Receiver has been configured to not dispatch the message any further.");
              }

              // persist message token
              if (persistMessageToken.booleanValue())  {
                  this.persistMessageToken(outboundMessageToken);
              }
            }
        }


      /*********** END MESSAGE DISPATCH ***********/

    } catch (Throwable t)  {

      if (warningRollbackOnly)
      {
          logger.error(t.getMessage());
      }
      else
      {
        logger.error(t);
        try
        {
          this.logError(componentRole, inboundMessageToken, t, connection);
        }
        catch (Exception e)
        {
          logger.error(t);
        }
      }

      // Rollback the entire transaction
      // Do this here because we are invoking logError above 
      // Which calls a stored procedure which occurs is declared with PRAGMA AUTONOMOUS_TRANSACTION
      // Once we call setRollbackOnly, the connections cannot be utilized any further.
      messageDrivenContext.setRollbackOnly();

    } finally  {
      // close JDBC connection
      try  {
        if (connection != null && (! connection.isClosed() ) )  {
            connection.close();
            logger.debug(dataSourceName + " Connection Released");
        }
      } catch (Exception ex)  {
        logger.error(ex);
      } finally  {

      }
      logger.debug("***********************************************************");
      logger.info("* END " + componentRole + " TRANSACTION *");
      logger.debug("***********************************************************");
    }
  }

  /**
   * Returns the message token from the JMS message
   */
  protected MessageToken getMessageToken(Message msg) throws JMSException{
    ObjectMessage objectMessage = (ObjectMessage) msg;
    MessageToken inboundMessageToken = (MessageToken) objectMessage.getObject();
    return inboundMessageToken;
  }

  /**
   * Not implemented yet
   */
  protected void persistMessageToken(MessageToken messageToken) throws Exception{

  }

  /**
   * log the error as an autonomous transaction
   */
  protected void logError(String componentRole, MessageToken messageToken, Throwable t, Connection connection)
  {
    CallableStatement cstmt = null;
    PrintWriter pw = null;
    StringWriter sw = null;
    String errorMessage = null;

    try
    {
      cstmt = connection.prepareCall("{call SCRUB.MISC_PKG.INSERT_ORDER_EXCEPTIONS(?, ?, ?, ?, ?)}");
      cstmt.setString(1, componentRole + "/" + messageToken.getStatus());
      cstmt.setString(2, (String)messageToken.getMessage());

      sw = new StringWriter();
      t.printStackTrace(new PrintWriter(sw));
      errorMessage = sw.toString();
      if (errorMessage.length() > 4000)
      {
          errorMessage = errorMessage.substring(0,4000);
      }
      // convert the error message to an xml safe format
      errorMessage = DOMUtil.encodeChars(errorMessage);

      cstmt.setString(3, errorMessage);

      cstmt.registerOutParameter(4, Types.VARCHAR);
      cstmt.registerOutParameter(5, Types.VARCHAR);

      cstmt.execute();

      String statusFromDB = (String) cstmt.getObject(4);
      String messageFromDB = (String) cstmt.getObject(5);
      logger.debug("Log Error Status:: " + statusFromDB);
      logger.debug("Log Error Message:: " + messageFromDB);

    } catch (Exception ex)
    {
      logger.error(ex);
    } finally
    {
      if (cstmt != null)
      {
        try
        {
          cstmt.close();
          if (pw != null)
          {
              pw.close();
          }

        } catch (Exception ex)
        {
          logger.error(ex);
        }

      }

    }
  }
}
