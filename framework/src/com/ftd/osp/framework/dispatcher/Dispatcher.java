package com.ftd.osp.framework.dispatcher;

import com.ftd.osp.utilities.j2ee.ResourceUtil;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;
import com.ftd.osp.utilities.xml.DOMUtil;

import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Session;

import javax.naming.Context;
import javax.naming.InitialContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The Dispatcher allows dispatching a Message Token to the desired JMS
 * destination. The destination information is configured in the dispatcher.xml
 * file.
 *
 * @author Anshu Gaind
 * 
 * Updated version of Dispatcher that does not depend on the
 * Oracle XML Parser
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class Dispatcher  {
  private static Dispatcher DISPATCHER = new Dispatcher();
  private static Logger logger = new Logger(Dispatcher.class.getName());
  private static Document config;
  private static String configFile = "dispatcher.xml";
  private static boolean SINGLETON_INITIALIZED;

 /**
  * The static initializer for the Dispatcher. It ensures that
  * at a given time there is only a single instance
  *
  * @return the dispatcher
  **/
  public static Dispatcher getInstance() throws Exception{
    if (! SINGLETON_INITIALIZED){
        initConfig();
    }
    return DISPATCHER;
  }


  /**
   * The private Constructor
   **/
  protected Dispatcher(){
    super();
  }


  /**
   * Initialize the configuration
   */
  private static synchronized void initConfig()
        throws Exception {
    if (! SINGLETON_INITIALIZED)
    {
        InputStream configStream = ResourceUtil.getInstance().getResourceFileAsStream(configFile);
      if (configStream == null){
        throw new IOException("The configuration file was not found.");
      }

      config = DOMUtil.getDocument(configStream);
      SINGLETON_INITIALIZED = true;
    }
  }

  /**
   * Dispatches the message token as an object message to the destination locations
   * configured in the dispatcher.xml.
   *
   * Each pipeline has a sequence of locations that are all considered as a
   * single transaction.
   *
   * @param context to extract the JMS queues
   * @param messageToken the message token
   * @throws JMSException
   * @throws Exception
   */
  public void dispatch(Context context, MessageToken messageToken) throws JMSException, Exception{
    logger.debug("Class = Dispatcher, method = dispatch(context, messageToken)");

    List queueSenderList = null;
    SimpleQueueSender simpleQueueSender = null;

    try  {

        queueSenderList = this.getQueueSenders(context, messageToken);
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.sendObjectMessage(messageToken);
          simpleQueueSender.close();
        }

    } catch (Exception ex)  {
      logger.error(ex);
      throw ex;
    } finally  {
      // close the JMS connection
      if (simpleQueueSender != null)  {
          simpleQueueSender.close();
      }
    }
  }

  /**
   * Dispatches the message in the message token, as a text message, to the
   * destination locations  configured in the dispatcher.xml.
   *
   * Each pipeline has a sequence of locations that are all considered as a
   * single transaction.
   *
   * @param context to extract the JMS queues
   * @param messageToken the message token
   * @throws JMSException
   * @throws Exception
   */
  public void dispatchTextMessage(Context context, MessageToken messageToken) throws JMSException, Exception{

        // Print out a bunch of debug stuff to see what is going on.
        logger.info("messageToken ");
        List tokenList = new ArrayList();
        tokenList.add(messageToken);
        dispatchTextMessages(context,tokenList);
  }

//  private void dumpContext(Context context, String name) throws JMSException, Exception
//  {
//    javax.naming.NamingEnumeration nenum = context.list(name);
//    StringBuffer sb = new StringBuffer();
//    sb.append("\n");
//    while (nenum.hasMore())
//    {
//        javax.naming.NameClassPair pair = (javax.naming.NameClassPair) nenum.next();
//        sb.append("    --> " + name + "/" + pair.getName() + " " + pair.getClassName() + "\n");
//    }
//    logger.info(sb.toString());
//  }

  /**
   * Dispatches the message in the message token, as a text message, to the
   * destination locations  configured in the dispatcher.xml.
   *
   * Each pipeline has a sequence of locations that are all considered as a
   * single transaction.
   *
   * @param context to extract the JMS queues
   * @param messageToken the message token
   * @throws JMSException
   * @throws Exception
   */
  public void dispatchTextMessages(Context context, List tokenList) throws JMSException, Exception{
    List queueSenderList = null;
    SimpleQueueSender simpleQueueSender = null;

    try  {

        queueSenderList = this.getQueueSenders(context, tokenList);
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.sendTextMessages(tokenList);
          simpleQueueSender.close();
        }

    } catch (Exception ex)  {
      logger.error(ex);
      throw ex;
    } finally  {
      // close the JMS connection
      if (simpleQueueSender != null)  {
          simpleQueueSender.close();
      }
    }
  }

  /**
   * Dequeues JMS Messages
   *
   * @param context
   * @param messageToken
   * @throws javax.jms.JMSException
   * @throws java.lang.Exception
   */
  public void dequeueMessage(Context context, MessageToken messageToken)
    throws JMSException, Exception
  {
    List queueSenderList = null;
    SimpleQueueSender simpleQueueSender = null;

    try  {

        queueSenderList = this.getQueueSenders(context, messageToken);
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.dequeMessage();
          simpleQueueSender.close();
        }

    } catch (Exception ex)  {
      logger.error(ex);
      throw ex;
    } finally  {
      // close the JMS connection
      if (simpleQueueSender != null)  {
          simpleQueueSender.close();
      }
    }
  }


  /**
   * Dequeues JMS Messages
   *
   * NOTE: Do not call this method for container managed transactions.
   * This method starts a new transaction, which cannot be coordinated with any
   * other application transactions.
   *
   * @param messageToken the message token
   * @throws JMSException
   * @throws Exception
   */
  public void dequeueMessage( MessageToken messageToken) throws JMSException, Exception{
    List queueSenderList = null;
    SimpleQueueSender simpleQueueSender = null;

    try  {
          // get the initial context
        Context  context = new InitialContext();

        queueSenderList = this.getQueueSenders(context, messageToken);

        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.dequeMessage();
        }

        // commit all messages
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.commit();
        }


    } catch (Exception ex)  {
      logger.error("Rolling back all messages");
      logger.error(ex);
      // rollback all messages
      if (queueSenderList != null)
      {
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.rollback();
        }
      }
      throw ex;
    } finally  {
      // close the JMS connections
      if (queueSenderList != null)
      {
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.close();
        }
      }
    }

  }

  /**
   * Returns the names of the queues associated with the specified pipeline
   *
   * @param pipeLine
   * @return
   */

  public List getQueueNames(MessageToken messageToken) throws Exception
  {
    ArrayList list = new ArrayList();
    String status = messageToken.getStatus();

    Element sequenceE = ( (Element) DOMUtil.selectSingleNode(config,"//pipeline[@on-status='" + status + "']/sequence"));

    if (sequenceE == null)  {
        throw new Exception("Could not obtain the pipeline associated to the status: " + status);
    }
    NodeList locationNL = sequenceE.getElementsByTagName("location");


    String locationName = null, destinationLocation = null;
    Element locationE = null;
    Node location = null;

    // for all locations in the sequence
    for (int i = 0; i < locationNL.getLength(); i++)  {
      locationE = (Element) locationNL.item(i);
      locationName = locationE.getAttribute("name");
      logger.debug("Begin executing location " + locationName);

      // extract all information for that location
      location = DOMUtil.selectSingleNode( config, "//locations/location[@name='" + locationName + "']");
      NamedNodeMap nnm = location.getAttributes();
      destinationLocation = nnm.getNamedItem("destination-location").getNodeValue();
      destinationLocation = destinationLocation.substring(destinationLocation.lastIndexOf("/")+1);
      list.add(destinationLocation);
    }

    return list;
  }


//  public static void main(String[] args)
//  {
//    try
//    {
//      MessageToken messageToken = new MessageToken();
//      messageToken.setStatus("FLORISTSUSPSERV QUEUE");
//      List list = Dispatcher.getInstance().getQueueNames(messageToken);
//      for(Iterator iter = list.iterator(); iter.hasNext();)
//      {
//        System.out.println(iter.next());
//      }
//    } catch (Exception ex)
//    {
//      ex.printStackTrace();
//    } finally
//    {
//    }
//
//  }

  /**
   * Creates the appropriate queue sender and configures the message token
   *
   * @param context
   * @param messageToken
   * @return a list of queue sender, one for each destination in the pipeline
   * @throws javax.jms.JMSException
   * @throws java.lang.Exception
   */
  private List getQueueSenders( Context context, MessageToken messageToken)
      throws JMSException, Exception
      {
          List tokenList = new ArrayList();
          tokenList.add(messageToken);
          return getQueueSenders(context,tokenList);
      }


  /**
   * Creates the appropriate queue sender and configures the message token
   *
   * @param context
   * @param messageToken
   * @return a list of queue sender, one for each destination in the pipeline
   * @throws javax.jms.JMSException
   * @throws java.lang.Exception
   */
  private List getQueueSenders( Context context, List tokenList)
      throws JMSException, Exception
  {
    //get first token from list.
    MessageToken messageToken = (MessageToken)tokenList.get(0);

    ArrayList queueSenderList = new ArrayList();
    SimpleQueueSender simpleQueueSender = null;

    String status = messageToken.getStatus();

    if (status == null)  {
        throw new Exception("The status on the message token is null");
    }

    Element sequenceE = ( (Element) DOMUtil.selectSingleNode(config, "//pipeline[@on-status='" + status + "']/sequence"));

    if (sequenceE == null)  {
        throw new Exception("Could not obtain the pipeline associated to the status: " + status);
    }

    logger.info("Begin executing pipeline " + status);
    NodeList locationNL = sequenceE.getElementsByTagName("location");
    NodeList propertyNL = null;

    String locationName = null, connectionFactoryLocation = null, destinationLocation = null;
    Element locationE = null;
    Element location = null, property = null;

    String propertyName = null, propertyValue = null, propertyType = null;

    // for all locations in the sequence
    for (int i = 0; i < locationNL.getLength(); i++)  {
      locationE = (Element) locationNL.item(i);
      locationName = locationE.getAttribute("name");
      logger.info("Begin executing location " + locationName);

      // extract all information for that location
      location = (Element) ( DOMUtil.selectSingleNode(config, "//locations/location[@name='" + locationName + "']"));
      NamedNodeMap nnm = location.getAttributes();
      connectionFactoryLocation = nnm.getNamedItem("connection-factory-location").getNodeValue();
      destinationLocation = nnm.getNamedItem("destination-location").getNodeValue();

      /**
       * Set properties on the message
       */
       propertyNL = DOMUtil.selectNodes(location, "properties/property");
       for (int j = 0; j < propertyNL.getLength(); j++)
       {
          property = (Element) propertyNL.item(j);
          NamedNodeMap propertynnm = property.getAttributes();
          propertyName = propertynnm.getNamedItem("name").getNodeValue();
          propertyValue = propertynnm.getNamedItem("value").getNodeValue();
          propertyType = propertynnm.getNamedItem("type").getNodeValue();

          //set properties for all tokens
          Iterator iter = tokenList.iterator();
          while(iter.hasNext())
          {
              MessageToken token = (MessageToken)iter.next();
              token.setProperty(propertyName, propertyValue, propertyType);
          }

       }

      // create the queue sender
      simpleQueueSender = new SimpleQueueSender( context,
                                                 connectionFactoryLocation,
                                                 destinationLocation,
                                                 false, // transaction controlled by the container
                                                 Session.AUTO_ACKNOWLEDGE);
      queueSenderList.add(simpleQueueSender);
    }
    return queueSenderList;
  }


  /**
   * Dispatches the message token as an object message to the destination locations configured in
   * the dispatcher.xml. It uses the default initial context that is specified
   * in dispatcher.xml.
   *
   * Each pipeline has a sequence of locations that are all considered as a
   * single transaction.
   *
   * NOTE: Do not call this method for container managed transactions.
   * This method starts a new transaction, which cannot be coordinated with any
   * other application transactions.
   *
   * @param messageToken the message token
   * @throws JMSException
   * @throws Exception
   */
  public void dispatch( MessageToken messageToken) throws JMSException, Exception{
    logger.debug("Class = Dispatcher, method = dispatch(messageToken)");
    List queueSenderList = null;
    SimpleQueueSender simpleQueueSender = null;

    try  {
          // get the initial context
        Context  context = new InitialContext();

        queueSenderList = this.getQueueSenders(context, messageToken);

        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.sendObjectMessage(messageToken);
        }

        // commit all messages
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.commit();
        }


    } catch (Exception ex)  {
      logger.error("Rolling back all messages");
      logger.error(ex);

      // rollback all messages
      if (queueSenderList != null)
      {
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.rollback();
        }
      }
      throw ex;
    } finally  {
      // close the JMS connections
      if (queueSenderList != null)
      {
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.close();
        }
      }
    }

  }

  /**
   * Dispatches the message in the message token as a text message to the destination locations configured in
   * the dispatcher.xml. It uses the default initial context that is specified
   * in dispatcher.xml.
   *
   * Each pipeline has a sequence of locations that are all considered as a
   * single transaction.
   *
   * NOTE: Do not call this method for container managed transactions.
   * This method starts a new transaction, which cannot be coordinated with any
   * other application transactions.
   *
   * @param messageToken the message token
   * @throws JMSException
   * @throws Exception
   */
  public void dispatchTextMessage( MessageToken messageToken) throws JMSException, Exception{
    List queueSenderList = null;
    SimpleQueueSender simpleQueueSender = null;

    try  {
          // get the initial context
        Context  context = new InitialContext();

        queueSenderList = this.getQueueSenders(context, messageToken);

        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.sendTextMessage(messageToken);
        }

        // commit all messages
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.commit();
        }


    } catch (Exception ex)  {
      logger.error("Rolling back all messages");
      logger.error(ex);
      // rollback all messages
      if (queueSenderList != null)
      {
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.rollback();
        }
      }
      throw ex;
    } finally  {
      // close the JMS connections
      if (queueSenderList != null)
      {
        for (int i = 0; i < queueSenderList.size(); i++)
        {
          simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
          simpleQueueSender.close();
        }
      }
    }

  }

    public void dispatchTextMessageWithProperty(Context context, MessageToken messageToken,
        String propertyName, String propertyValue) throws JMSException, Exception{

          // Print out a bunch of debug stuff to see what is going on.
          List tokenList = new ArrayList();
          tokenList.add(messageToken);
          dispatchTextMessagesWithProperty(context,tokenList, propertyName, propertyValue);
    }

    public void dispatchTextMessagesWithProperty(Context context, List tokenList,
        String propertyName, String propertyValue) throws JMSException, Exception{
      List queueSenderList = null;
      SimpleQueueSender simpleQueueSender = null;

      try  {

          queueSenderList = this.getQueueSenders(context, tokenList);
          for (int i = 0; i < queueSenderList.size(); i++)
          {
            simpleQueueSender = (SimpleQueueSender)queueSenderList.get(i);
            simpleQueueSender.sendTextMessagesWithProperty(tokenList, propertyName, propertyValue);
            simpleQueueSender.close();
          }

      } catch (Exception ex)  {
        logger.error(ex);
        throw ex;
      } finally  {
        // close the JMS connection
        if (simpleQueueSender != null)  {
            simpleQueueSender.close();
        }
      }
    }


}
