package com.ftd.osp.framework.dispatcher;
import com.ftd.osp.utilities.plugins.Logger;
import com.ftd.osp.utilities.vo.MessageToken;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;

import javax.naming.Context;
import javax.naming.NamingException;

import javax.xml.transform.TransformerException;

/**
 * Sends Message Token to a JMS Queue
 * 
 * @author Anshu Gaind
 */

public class SimpleQueueSender {
    private Logger                  logger;
    private Context                 jndiContext;
    private QueueConnectionFactory  queueConnectionFactory;
    private QueueConnection         queueConnection;
    private QueueSession            queueSession;
    private Queue                   queue;
    private QueueSender             queueSender;
    private QueueBrowser            queueBrowser;
    private QueueReceiver           queueReceiver;
    private Message                 message;
    private String                  connectionFactoryLocation;
    private String                  destinationLocation;
    
    /**
     * Constructor
     * 
     * @param context the context 
     * @param newConnectionFactoryLocation the connection factory location
     * @param newDestinationLocation the destination location
     * @param acknowledgeMode the acknowledgement mode
     * @exception JMSException 
     * @exception NamingException
     * @exception TransformerException
     * @exception Exception
     */
    public SimpleQueueSender(Context context,
                              String newConnectionFactoryLocation,
                              String newDestinationLocation,
                              boolean transacted,
                              int acknowledgeMode) 
      throws JMSException, NamingException, TransformerException, Exception {

        logger = new Logger("com.ftd.osp.framework.dispatcher.QueueSender");        
        this.init(context,
                  newConnectionFactoryLocation,
                  newDestinationLocation,
                  transacted,
                  acknowledgeMode);
        logger.info("SimpleQueueSender is connected to "
                      + newConnectionFactoryLocation
                      + " and queue "
                      + newDestinationLocation);        
    }

  /**
   * Initialize the queue sender
   * 
   * @param context
   * @param newConnectionFactoryLocation
   * @param newDestinationLocation
   * @param transacted
   * @param acknowledgeMode
   * @throws javax.jms.JMSException
   * @throws javax.naming.NamingException
   * @throws javax.xml.transform.TransformerException
   * @throws java.lang.Exception
   */
    private void init(Context context,
                      String newConnectionFactoryLocation,
                      String newDestinationLocation,
                      boolean transacted,
                      int acknowledgeMode) throws JMSException, NamingException, TransformerException, Exception{
          logger.debug("BEGIN CONFIGURATION FOR " + newConnectionFactoryLocation + ":" + newDestinationLocation);
          connectionFactoryLocation  = newConnectionFactoryLocation;
          destinationLocation = newDestinationLocation;
          queueConnectionFactory = (QueueConnectionFactory)
                context.lookup(newConnectionFactoryLocation);
          queue = (Queue) context.lookup(newDestinationLocation);
          logger.debug("Creating Queue Connection");
          queueConnection =
              queueConnectionFactory.createQueueConnection();
          logger.debug("Starting Queue Connection");
          queueConnection.start();
          logger.debug("Creating Queue Session");
          queueSession = queueConnection.createQueueSession(transacted, acknowledgeMode);
          logger.debug("Creating Queue Sender");
          queueSender = queueSession.createSender(queue);
          logger.debug("END CONFIGURATION FOR " + newConnectionFactoryLocation + ":" + newDestinationLocation);
    }


    /**
     * Sends the message token to the desired JMS queue
     * 
     * @param messageToken the message token
     * @exception JMSException 
     */

    public void sendObjectMessage(MessageToken messageToken) throws JMSException {
        ObjectMessage  message = queueSession.createObjectMessage();
        this.message = message;

        this.configureMessage(message, messageToken);
        message.setObject(messageToken);        
        
        int JMSDeliveryMode = (messageToken.getJMSDeliveryMode() != 0)?messageToken.getJMSDeliveryMode():Message.DEFAULT_DELIVERY_MODE;
        int JMSPriority = (messageToken.getJMSPriority() != 0)?messageToken.getJMSPriority():Message.DEFAULT_PRIORITY;
        long JMSExpiration = (messageToken.getJMSExpiration() != 0L)?messageToken.getJMSExpiration():Message.DEFAULT_TIME_TO_LIVE;
        
        // send message
        queueSender.send(message,
                          JMSDeliveryMode,
                          JMSPriority,
                          JMSExpiration );
        logger.info("Object message sent");
    }

    public void sendTextMessage(MessageToken messageToken) throws JMSException 
    {
        List tokenList = new ArrayList();
        tokenList.add(messageToken);
        sendTextMessages(tokenList);
    }
    


    /**
     * Sends the text message from the message token to the desired JMS queue
     * 
     * @param messageToken the message token
     * @exception JMSException 
     */
    public void sendTextMessages(List tokenList) throws JMSException {

        //send each message that was passed in
        Iterator iter = tokenList.iterator();
        while(iter.hasNext())
        {
            MessageToken messageToken = (MessageToken)iter.next();
        
            TextMessage  message = queueSession.createTextMessage();
            this.message = message;
    
            this.configureMessage(message, messageToken);
    
            message.setText((String)messageToken.getMessage());        
    
            int JMSDeliveryMode = (messageToken.getJMSDeliveryMode() != 0)?messageToken.getJMSDeliveryMode():Message.DEFAULT_DELIVERY_MODE;
            int JMSPriority = (messageToken.getJMSPriority() != 0)?messageToken.getJMSPriority():Message.DEFAULT_PRIORITY;
            long JMSExpiration = (messageToken.getJMSExpiration() != 0L)?messageToken.getJMSExpiration():Message.DEFAULT_TIME_TO_LIVE;
            
            // send message
            queueSender.send(message,
                              JMSDeliveryMode,
                              JMSPriority,
                              JMSExpiration );
            logger.debug("Text message sent");            
        }


    }

    public void sendTextMessagesWithProperty(List tokenList, String propertyName, String propertyValue) throws JMSException {

        //send each message that was passed in
        Iterator iter = tokenList.iterator();
        while(iter.hasNext())
        {
            MessageToken messageToken = (MessageToken)iter.next();
        
            TextMessage  message = queueSession.createTextMessage();
            this.message = message;
    
            this.configureMessage(message, messageToken);
    
            message.setText((String)messageToken.getMessage());  
            if (propertyName != null) {
                message.setStringProperty(propertyName, propertyValue);
            }
    
            int JMSDeliveryMode = (messageToken.getJMSDeliveryMode() != 0)?messageToken.getJMSDeliveryMode():Message.DEFAULT_DELIVERY_MODE;
            int JMSPriority = (messageToken.getJMSPriority() != 0)?messageToken.getJMSPriority():Message.DEFAULT_PRIORITY;
            long JMSExpiration = (messageToken.getJMSExpiration() != 0L)?messageToken.getJMSExpiration():Message.DEFAULT_TIME_TO_LIVE;
            
            // send message
            queueSender.send(message,
                              JMSDeliveryMode,
                              JMSPriority,
                              JMSExpiration );
            logger.debug("Text message sent");            
        }


    }

  /**
   * Consumes messages from a JMS queue
   * 
   * @throws javax.jms.JMSException
   */
  public void dequeMessage() throws JMSException
  {
    queueBrowser = queueSession.createBrowser(queue);
    queueReceiver = queueSession.createReceiver(queue);
    int messageCount = 0;

    Enumeration enum1 = queueBrowser.getEnumeration();
    while (enum1.hasMoreElements()) 
    {
      enum1.nextElement();
      messageCount++;
    }
    
    
    int ctr;
    for (ctr = 0; ctr < messageCount; ctr++) 
    {
      queueReceiver.receiveNoWait();      
    }
    logger.debug(ctr + " messages dequeued");
            
  }
  
  /**
   * Sets properties on the message
   * 
   * @param messageToken
   * @param message
   * @throws javax.jms.JMSException
   */
  private void configureMessage(Message message, MessageToken messageToken) throws JMSException
  {
    // set JMS correlation id
    if (messageToken.getJMSCorrelationID() != null) 
    {
        message.setJMSCorrelationID(messageToken.getJMSCorrelationID());
    }
        
    // set properties
    Set names = messageToken.getPropertyNames();
    MessageToken.MessageProperty messageProperty = null;
    
    for (Iterator iter = names.iterator(); iter.hasNext(); ) 
    {
        messageProperty = messageToken.getProperty((String)iter.next());
        
        if (messageProperty.type.equals("boolean")) 
        {
            message.setBooleanProperty(messageProperty.name, Boolean.getBoolean((String)messageProperty.value));
        } else if (messageProperty.type.equals("byte")) 
        {
            message.setByteProperty(messageProperty.name, Byte.parseByte((String)messageProperty.value));
        } else if (messageProperty.type.equals("double")) 
        {
            message.setDoubleProperty(messageProperty.name, Double.parseDouble((String)messageProperty.value));
        } else if (messageProperty.type.equals("float")) 
        {
            message.setFloatProperty(messageProperty.name, Float.parseFloat((String)messageProperty.value));
        } else if (messageProperty.type.equals("int")) 
        {
            message.setIntProperty(messageProperty.name, Integer.parseInt((String)messageProperty.value));
        } else if (messageProperty.type.equals("long")) 
        {
            message.setLongProperty(messageProperty.name, Long.parseLong((String)messageProperty.value));
        } else if (messageProperty.type.equals("short")) 
        {
            message.setShortProperty(messageProperty.name, Short.parseShort((String)messageProperty.value));
        } else if (messageProperty.type.equals("java.lang.String")) 
        {
            message.setStringProperty(messageProperty.name, (String)messageProperty.value);
        } else if (messageProperty.type.equals("java.lang.Object")) 
        {
            message.setObjectProperty(messageProperty.name, messageProperty.value);
        }
    }
    
  }
    /**
     * Rolls back the changes made in the JMS session
     * 
     * @exception JMSException 
     */

    public void rollback() throws JMSException{
      queueSession.rollback();
    }

    /**
     * Called to commit the message
     * 
     * @exception JMSException 
     */
    public void commit() throws JMSException{
      this.message.acknowledge();
      queueSession.commit();
    }

    /**
     * Called to close the JMS session
     * 
     * @exception JMSException 
     */
    public void close() throws JMSException{
        if (queueConnection != null) {
            if (this.queueBrowser != null) 
            {
                logger.debug("Closing Queue Browser " + connectionFactoryLocation + ":" + destinationLocation);
                this.queueBrowser.close();
            }
            if (this.queueReceiver != null) 
            {
                logger.debug("Closing Queue Receiver " + connectionFactoryLocation + ":" + destinationLocation);
                this.queueReceiver.close();                
            }
            if (this.queueSender != null) 
            {
                logger.debug("Closing Queue Sender " + connectionFactoryLocation + ":" + destinationLocation);
                this.queueSender.close();                
            }
            if (this.queueSession != null) 
            {
                logger.debug("Closing Queue Session " + connectionFactoryLocation + ":" + destinationLocation);
                this.queueSession.close();
            }
            logger.debug("Closing Queue Connection " + connectionFactoryLocation + ":" + destinationLocation);
            this.queueConnection.close();
        }
    }
    
  /**
   * This method cleans up AQ connections so that they don't get leaked when 
   * this object is unexpectedly destroyed
   * 
   * @throws java.lang.Throwable
   */
    protected void finalize() throws Throwable
    {
      logger.debug("Cleaning up SimpleQueueSender");
      this.queueBrowser.close();
      this.queueConnection.close();
      this.queueReceiver.close();
      this.queueSender.close();
      this.queueSession.close();
      
      this.close();
    }

}
