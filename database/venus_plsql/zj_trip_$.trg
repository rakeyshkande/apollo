CREATE OR REPLACE
TRIGGER venus.zj_trip_$
AFTER INSERT OR UPDATE OR DELETE ON venus.zj_trip 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO venus.zj_trip$ (
         TRIP_ID,                        
         TRIP_ORIGINATION_DESC,          
         INJECTION_HUB_ID,              
         DELIVERY_DATE,                  
         DEPARTURE_DATE,                 
         TRIP_STATUS_CODE,               
         SORT_CODE_SEQ,              
         SORT_CODE_STATE_MASTER_ID,
         SORT_CODE_DATE_MMDD,            
         TOTAL_PLT_QTY,               
         FTD_PLT_QTY,  
         FTD_PLT_USED_QTY,
         THIRD_PARTY_PLT_QTY,   
         ZONE_JUMP_TRAILER_NUMBER,
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :NEW.TRIP_ID,                        
         :NEW.TRIP_ORIGINATION_DESC,          
         :NEW.INJECTION_HUB_ID,              
         :NEW.DELIVERY_DATE,                  
         :NEW.DEPARTURE_DATE,                 
         :NEW.TRIP_STATUS_CODE,               
         :NEW.SORT_CODE_SEQ,              
         :NEW.SORT_CODE_STATE_MASTER_ID,
         :NEW.SORT_CODE_DATE_MMDD,            
         :NEW.TOTAL_PLT_QTY,               
         :NEW.FTD_PLT_QTY,                 
         :NEW.FTD_PLT_USED_QTY,
         :NEW.THIRD_PARTY_PLT_QTY,         
         :NEW.ZONE_JUMP_TRAILER_NUMBER,
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO venus.zj_trip$ (
         TRIP_ID,                        
         TRIP_ORIGINATION_DESC,          
         INJECTION_HUB_ID,              
         DELIVERY_DATE,                  
         DEPARTURE_DATE,                 
         TRIP_STATUS_CODE,               
         SORT_CODE_SEQ,              
         SORT_CODE_STATE_MASTER_ID,
         SORT_CODE_DATE_MMDD,            
         TOTAL_PLT_QTY,               
         FTD_PLT_QTY,                 
         FTD_PLT_USED_QTY,
         THIRD_PARTY_PLT_QTY,         
         ZONE_JUMP_TRAILER_NUMBER,
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :OLD.TRIP_ID,                        
         :OLD.TRIP_ORIGINATION_DESC,          
         :OLD.INJECTION_HUB_ID,              
         :OLD.DELIVERY_DATE,                  
         :OLD.DEPARTURE_DATE,                 
         :OLD.TRIP_STATUS_CODE,               
         :OLD.SORT_CODE_SEQ,              
         :OLD.SORT_CODE_STATE_MASTER_ID,
         :OLD.SORT_CODE_DATE_MMDD,            
         :OLD.TOTAL_PLT_QTY,               
         :OLD.FTD_PLT_QTY,                 
         :OLD.FTD_PLT_USED_QTY,
         :OLD.THIRD_PARTY_PLT_QTY,         
         :OLD.ZONE_JUMP_TRAILER_NUMBER,
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',SYSDATE);

      INSERT INTO venus.zj_trip$ (
         TRIP_ID,                        
         TRIP_ORIGINATION_DESC,          
         INJECTION_HUB_ID,              
         DELIVERY_DATE,                  
         DEPARTURE_DATE,                 
         TRIP_STATUS_CODE,               
         SORT_CODE_SEQ,              
         SORT_CODE_STATE_MASTER_ID,
         SORT_CODE_DATE_MMDD,            
         TOTAL_PLT_QTY,               
         FTD_PLT_QTY,                 
         FTD_PLT_USED_QTY,
         THIRD_PARTY_PLT_QTY,         
         ZONE_JUMP_TRAILER_NUMBER,
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :NEW.TRIP_ID,                        
         :NEW.TRIP_ORIGINATION_DESC,          
         :NEW.INJECTION_HUB_ID,              
         :NEW.DELIVERY_DATE,                  
         :NEW.DEPARTURE_DATE,                 
         :NEW.TRIP_STATUS_CODE,               
         :NEW.SORT_CODE_SEQ,              
         :NEW.SORT_CODE_STATE_MASTER_ID,
         :NEW.SORT_CODE_DATE_MMDD,            
         :NEW.TOTAL_PLT_QTY,               
         :NEW.FTD_PLT_QTY,                 
         :NEW.FTD_PLT_USED_QTY,
         :NEW.THIRD_PARTY_PLT_QTY,         
         :NEW.ZONE_JUMP_TRAILER_NUMBER,
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO venus.zj_trip$ (
         TRIP_ID,                        
         TRIP_ORIGINATION_DESC,          
         INJECTION_HUB_ID,              
         DELIVERY_DATE,                  
         DEPARTURE_DATE,                 
         TRIP_STATUS_CODE,               
         SORT_CODE_SEQ,              
         SORT_CODE_STATE_MASTER_ID,
         SORT_CODE_DATE_MMDD,            
         TOTAL_PLT_QTY,               
         FTD_PLT_QTY,                 
         FTD_PLT_USED_QTY,
         THIRD_PARTY_PLT_QTY,         
         ZONE_JUMP_TRAILER_NUMBER,
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :OLD.TRIP_ID,                        
         :OLD.TRIP_ORIGINATION_DESC,          
         :OLD.INJECTION_HUB_ID,              
         :OLD.DELIVERY_DATE,                  
         :OLD.DEPARTURE_DATE,                 
         :OLD.TRIP_STATUS_CODE,               
         :OLD.SORT_CODE_SEQ,              
         :OLD.SORT_CODE_STATE_MASTER_ID,
         :OLD.SORT_CODE_DATE_MMDD,            
         :OLD.TOTAL_PLT_QTY,               
         :OLD.FTD_PLT_QTY,                 
         :OLD.FTD_PLT_USED_QTY,
         :OLD.THIRD_PARTY_PLT_QTY,         
         :OLD.ZONE_JUMP_TRAILER_NUMBER,
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',SYSDATE);

   END IF;

END;
/
