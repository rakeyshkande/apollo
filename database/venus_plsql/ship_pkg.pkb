create or replace package body venus.SHIP_PKG

AS

PROCEDURE UPDATE_VENDOR_CARRIER_REF
(
IN_VENDOR_ID                    IN VENDOR_CARRIER_REF.VENDOR_ID%TYPE,
IN_CARRIER_ID                   IN VENDOR_CARRIER_REF.CARRIER_ID%TYPE,
IN_USAGE_PERCENT                IN VENDOR_CARRIER_REF.USAGE_PERCENT%TYPE,
IN_OVERRIDE_START               IN VENDOR_CARRIER_REF.OVERRIDE_START%TYPE,
IN_OVERRIDE_END                 IN VENDOR_CARRIER_REF.OVERRIDE_END%TYPE,
IN_OVERRIDE_PERCENTAGE          IN VENDOR_CARRIER_REF.OVERRIDE_PERCENTAGE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating records into
        the vendor carrier reference table

Input:
        vendor_id                       varchar2
        carrier_id                      varchar2
        usage_percent                   number
        override_start                  date
        override_end                    date
        override_percentage             number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
        SELECT  'Y'
        FROM    vendor_carrier_ref
        WHERE   vendor_id = in_vendor_id
        AND     carrier_id = in_carrier_id;

v_exists        char(1) := 'N';

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

IF v_exists = 'Y' THEN
        UPDATE  vendor_carrier_ref
        SET     usage_percent = in_usage_percent,
                override_start = trunc(IN_OVERRIDE_START),
                override_end = trunc(IN_OVERRIDE_END),
                override_percentage = IN_OVERRIDE_PERCENTAGE
        WHERE   vendor_id = in_vendor_id
        AND     carrier_id = in_carrier_id;
ELSE
        INSERT INTO vendor_carrier_ref
        (
                vendor_id,
                carrier_id,
                usage_percent
        )
        VALUES
        (
                in_vendor_id,
                in_carrier_id,
                in_usage_percent
        );
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_CARRIER_REF;



PROCEDURE VIEW_VENDOR_CARRIER_REF
(
IN_VENDOR_ID                    IN VENDOR_CARRIER_REF.VENDOR_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the vendor carrier
        reference records

Input:
        vendor_id                       varchar2

Output:
        cursor containing records from vendor_carrier_ref for the vendor_id

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                vc.vendor_id,
                vc.carrier_id,
                vc.usage_percent,
                c.carrier_name,
                c.ground_indicator,
                TO_CHAR(vc.override_start,'MM/DD/YYYY') override_start,
                TO_CHAR(vc.override_end,'MM/DD/YYYY') override_end,
                vc.override_percentage
        FROM    vendor_carrier_ref vc
        JOIN    carriers c
        ON      vc.carrier_id = c.carrier_id
        WHERE   vc.vendor_id = in_vendor_id;

END VIEW_VENDOR_CARRIER_REF;


PROCEDURE VIEW_VNDR_CARRIER_BY_VENUS_ID
(
IN_VENUS_ID                     IN VENUS.VENUS_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the vendor carrier
        reference records by venus id

Input:
        venus_id                       varchar2

Output:
        cursor containing records from vendor_carrier_ref for the vendus_id

-----------------------------------------------------------------------------*/

CURSOR venus_cur IS
        SELECT  ship_method, trunc(ship_date), trunc(delivery_date)
        FROM    venus.venus
        WHERE   venus_id = in_venus_id;

v_ship_method venus.ship_method%type;
v_ship_date venus.ship_date%type;
v_delivery_date venus.delivery_date%type;
v_ship_days carrier_delivery.max_ship_days%type;

BEGIN

OPEN venus_cur;
FETCH venus_cur INTO v_ship_method,v_ship_date,v_delivery_date;
CLOSE venus_cur;

/*
   Since Saturday delivery can be 2F or PO,
   calculate ship days based on delivery and
   ship dates and convert ship method to 'SA'
*/
IF TO_CHAR(v_delivery_date,'D') = '7' THEN
  v_ship_method := 'SA';
  v_ship_days := v_delivery_date-v_ship_date;
ELSE
  /*
    Except for Saturday delivery, days in transit
    is based on the passed ship methods
  */
  CASE v_ship_method
  WHEN '2F' THEN
    v_ship_days := 2;
  WHEN 'GR' THEN
    v_ship_days := 3;
  ELSE /* defaults to one day (PO or ND) shipping */
    v_ship_days := 1;
  END CASE;
END IF;

OPEN OUT_CUR FOR
     SELECT
                vc.vendor_id,
                vc.carrier_id,
                /* is there an override for the specified ship day */
                (CASE
                    WHEN v_ship_date >= vc.override_start AND v_ship_date <= vc.override_end THEN
                        vc.override_percentage
                    ELSE
                        vc.usage_percent
                    END) AS "USAGE_PERCENT",
                c.carrier_name,
                c.ground_indicator,
                v.reference_number
         FROM    venus.vendor_carrier_ref vc, ftd_apps.product_master pm,  venus.venus v, venus.carriers c,
                       ftd_apps.product_subcodes faps, venus.carrier_delivery cd
         WHERE  v.venus_id = IN_VENUS_ID
         AND v.product_id = faps.product_subcode_id(+)
         AND nvl(faps.product_id, v.product_id) = pm.product_id
         AND vc.vendor_id = v.vendor_id
         AND vc.carrier_id = c.carrier_id
         AND c.carrier_id NOT IN (SELECT carrier_id
                                  FROM venus.carrier_excluded_zips
                                  WHERE NVL(v.zip,'NULLVALUE') = zip_code)
         AND cd.carrier_id = c.carrier_id
         AND v_ship_method = cd.ship_method
         AND v_ship_days <= cd.MAX_SHIP_DAYS
         ORDER BY "USAGE_PERCENT";

END VIEW_VNDR_CARRIER_BY_VENUS_ID;


PROCEDURE VIEW_ACTIVE_VENDORS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the active vendor records

Input:
        none
Output:
        cursor containing active records from vendor_master
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR

SELECT vmc.vendor_id, vmc.vendor_name, z.counter, vmc.carrier_id, nvl(vcr.usage_percent,0) AS "USAGE_PERCENT"
FROM
   (SELECT DISTINCT vendor_id, vendor_name, carrier_id
    FROM ftd_apps.vendor_master vm, venus.carriers c
    where vm.active = 'Y' and vendor_type = 'ESCALATE' and c.active = 'Y'
    order by 2, 1, 3) vmc,
   vendor_carrier_ref vcr,
   (select vendor_id, vendor_name, count(*) AS counter
    from ftd_apps.vendor_master vm1, venus.carriers c1
    where vm1.active = 'Y' and vendor_type = 'ESCALATE' and c1.active = 'Y'
    group by vendor_id, vendor_name) z
WHERE (vmc.vendor_id = vcr.vendor_id(+))
AND   (vmc.carrier_id = vcr.carrier_id(+))
AND    vmc.vendor_id = z.vendor_id
order by 2, 1;

END VIEW_ACTIVE_VENDORS;


PROCEDURE EMPTY_FEDEX_STAGING_TABLES
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure truncates the fedex ground schedule stage table.

Input:
        none

Output:
        status                  varchar - Y/N if error occurred
        message                 varchar - error message
-----------------------------------------------------------------------------*/

BEGIN

-- truncate the stage table before the load of the xml records
execute immediate ('truncate table venus.fedex_ground_schedule_stage');

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END EMPTY_FEDEX_STAGING_TABLES;


PROCEDURE INSERT_FEDEX_SCHEDULE_STAGE
(
IN_SCHED_XML                    IN CLOB,
OUT_ERRORS_XML                  OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a records from the passed in xml
        document into the fedex ground schedule stage table.

Input:
        sched_xml               clob

Output:
        errors_xml              clob - specific errors that occurred on the insert of the records
        status                  varchar - Y/N if error occurred
        message                 varchar - error message

Notes:
schedule xml example:
<FedExScheduleList>
        <FedExSchedule>
          <originZip>01776</originZip>
          <originTerminal>16</originTerminal>
          <destZip>01092</destZip>
          <destTerminal>3011</destTerminal>
          <serviceDays>1</serviceDays>
          <onTimeService>0.96</onTimeService>
          <serviceType>GND</serviceType>
          <fileLine>0</fileLine>
        </FedExSchedule>
</FedExScheduleList>

error xml example:
<FedExScheduleList>
        <FedExSchedule>
          <originZip>01776</originZip>
          <originTerminal>16</originTerminal>
          <destZip>01092</destZip>
          <destTerminal>3011</destTerminal>
          <serviceDays>1</serviceDays>
          <onTimeService>0.96</onTimeService>
          <serviceType>GND</serviceType>
          <fileLine>0</fileLine>
          <error>ORA-1000 Primary key violation</error>
        </FedExSchedule>
</FedExScheduleList>

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

CURSOR xml_cur IS
        SELECT  extractValue(value(i), '/FedExSchedule/originZip') origin_zip,
                extractValue(value(i), '/FedExSchedule/originTerminal') origin_terminal,
                extractValue(value(i), '/FedExSchedule/destZip') dest_zip,
                extractValue(value(i), '/FedExSchedule/destTerminal') dest_terminal,
                extractValue(value(i), '/FedExSchedule/serviceDays') service_days,
                extractValue(value(i), '/FedExSchedule/onTimeService') on_time_service,
                extractValue(value(i), '/FedExSchedule/serviceType') service_type,
                extract(value(i), '/FedExSchedule/originZip').getclobval() origin_zip_node,
                extract(value(i), '/FedExSchedule/originTerminal').getclobval() origin_terminal_node,
                extract(value(i), '/FedExSchedule/destZip').getclobval() dest_zip_node,
                extract(value(i), '/FedExSchedule/destTerminal').getclobval() dest_terminal_node,
                extract(value(i), '/FedExSchedule/serviceDays').getclobval() service_days_node,
                extract(value(i), '/FedExSchedule/onTimeService').getclobval() on_time_service_node,
                extract(value(i), '/FedExSchedule/serviceType').getclobval() service_type_node,
                extract(value(i), '/FedExSchedule/fileLine').getclobval() file_line_node
        FROM    TABLE (xmlsequence (extract(xmltype (in_sched_xml), '/FedExScheduleList/FedExSchedule'))) i;

v_errors_xml            clob := '<FedExScheduleList>';
v_errors_xml_node       varchar2(20) := '<FedExSchedule>';
v_errors_xml_node_end   varchar2(20) := '</FedExSchedule>';
v_errors_xml_end        varchar2(20) := '</FedExScheduleList>';

v_insert_cnt            integer := 0;
v_error_cnt             integer := 0;

BEGIN

-- insert the records from the xml document
FOR x IN xml_cur LOOP
BEGIN
        INSERT INTO fedex_ground_schedule_stage
        (
                origin_zip,
                origin_terminal,
                dest_zip,
                dest_terminal,
                service_days,
                on_time_service,
                service_type
        )
        VALUES
        (
                x.origin_zip,
                x.origin_terminal,
                x.dest_zip,
                x.dest_terminal,
                x.service_days,
                x.on_time_service,
                x.service_type
        );

        v_insert_cnt := v_insert_cnt + 1;

        -- if there is an error inserting the record, build the error xml with the given
        -- node plus the oracle error
        EXCEPTION WHEN OTHERS THEN
                v_error_cnt := v_error_cnt + 1;
                v_errors_xml := v_errors_xml ||
                                v_errors_xml_node ||
                                x.origin_zip_node ||
                                x.origin_terminal_node ||
                                x.dest_zip_node ||
                                x.dest_terminal_node ||
                                x.service_days_node ||
                                x.on_time_service_node ||
                                x.service_type_node ||
                                x.file_line_node ||
                                '<error>' || sqlerrm || '</error>' ||
                                v_errors_xml_node_end;

END;
END LOOP;

out_status := 'Y';

COMMIT;

-- return the process counts and any errors that occurred during the load
v_errors_xml := v_errors_xml ||
                '<counts><insert_count>' || to_char (v_insert_cnt) || '</insert_count>' ||
                '<error_count>' || to_char (v_error_cnt) || '</error_count></counts>';
v_errors_xml := v_errors_xml || v_errors_xml_end;
OPEN out_errors_xml FOR
        select  v_errors_xml errors_xml from dual;

EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    ROLLBACK;

END INSERT_FEDEX_SCHEDULE_STAGE;



FUNCTION GET_CAR_METHOD_OF_PMT_BY_OD (
  in_carrier_delivery    IN carrier_delivery.carrier_delivery%TYPE )
RETURN carriers.method_of_payment%TYPE
IS

v_data  carriers.method_of_payment%TYPE;

BEGIN

BEGIN
SELECT c.method_of_payment
INTO v_data
FROM carriers c,
     carrier_delivery cd
WHERE c.carrier_id = cd.carrier_id
  AND cd.carrier_delivery = in_carrier_delivery;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
   v_data := NULL;
END;

RETURN v_data;

END GET_CAR_METHOD_OF_PMT_BY_OD;


FUNCTION GET_CAR_NAME_BY_OD (
  in_carrier_delivery    IN carrier_delivery.carrier_delivery%TYPE )
RETURN carriers.carrier_name%TYPE
IS

v_data  carriers.carrier_name%TYPE;

BEGIN

BEGIN
SELECT c.carrier_name
INTO v_data
FROM carriers c,
     carrier_delivery cd
WHERE c.carrier_id = cd.carrier_id
  AND cd.carrier_delivery = in_carrier_delivery;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
   v_data := NULL;
END;

RETURN v_data;

END GET_CAR_NAME_BY_OD;

FUNCTION GET_CARRIER_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_CARRIER_LIST
-- Type:    Function
-- Syntax:  GET_CARRIER_LIST ()
-- Returns: ref_cursor for
--          CARRIER_ID   VARCHAR2
--          CARRIER_NAME VARCHAR2
--
--
-- Description:   Queries the CARRIERS table
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    carrier_cursor types.ref_cursor;
BEGIN
    OPEN carrier_cursor FOR
        SELECT
          "CARRIER_ID",
          "CARRIER_NAME"
        FROM "CARRIERS"
        ORDER BY "CARRIER_NAME";

    RETURN carrier_cursor;
END GET_CARRIER_LIST;

FUNCTION GET_SHIPPING_VENDORS (
  in_product_subcode_id IN FTD_APPS.Vendor_Product.PRODUCT_SUBCODE_ID%TYPE,
  in_delivery_date IN VENUS.DELIVERY_DATE%TYPE,
  in_ship_date IN VENUS.SHIP_DATE%TYPE)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_SHIPPING_VENDORS
-- Type:    Function
-- Syntax:  GET_SHIPPING_VENDORS (product_subcode_id,delivery_date,ship_date)
-- Returns: ref_cursor for
--          VENDOR_ID           VARCHAR2
--          MEMBER_NUMBER       VARCHAR2
--          PRODUCT_SUBCODE_ID  VARCHAR2
--          VENDOR_COST         NUMBER
--          VENDOR_SKU          VARCHAR2
--          AVAILABLE           CHAR(1)
--          SUBCODE_DESCRIPTION VARCHAR2
--
--
-- Description:   Checks for availability of vendors to fulfill orders
--                based on the passed parameters.
--
--==============================================================================
AS

vendor_cursor types.ref_cursor;
v_temp_string        VARCHAR2(100);
v_backend_delay      NUMBER;


BEGIN

  -- get the back-end cutoff delay
  BEGIN
    select value 
    into   v_temp_string 
    from   frp.global_parms 
    where  context = 'SHIPPING_PARMS' 
    and    name = 'BACKEND_CUTOFF_DELAY';
    
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_temp_string := '30';
  END;
  v_backend_delay := TO_NUMBER(v_temp_string);


  OPEN vendor_cursor FOR
    SELECT  DISTINCT vm."VENDOR_ID" "VENDOR_ID",
                vm."MEMBER_NUMBER" "MEMBER_NUMBER",
                vp."PRODUCT_SUBCODE_ID" "PRODUCT_SUBCODE_ID",
                vp."VENDOR_COST" "VENDOR_COST",
                vp."VENDOR_SKU" "VENDOR_SKU",
                vp."AVAILABLE" "AVAILABLE",
                ps."SUBCODE_DESCRIPTION" "SUBCODE_DESCRIPTION",
                vm."ADDRESS1" "ADDRESS1",
                vm."ADDRESS2" "ADDRESS2",
                vm."CITY" "CITY",
                vm."STATE" "STATE",
                vm."ZIP_CODE" "ZIP_CODE"
    FROM FTD_APPS."VENDOR_MASTER" vm
    JOIN FTD_APPS."VENDOR_PRODUCT" vp ON vm."VENDOR_ID" = vp."VENDOR_ID"
    LEFT OUTER JOIN FTD_APPS."PRODUCT_SUBCODES" ps
        ON vp."PRODUCT_SUBCODE_ID" = ps."PRODUCT_SUBCODE_ID"		
    WHERE vp."PRODUCT_SUBCODE_ID"= IN_PRODUCT_SUBCODE_ID 
      AND vp."AVAILABLE"='Y' 
      AND VP.VENDOR_ID IN (
              select distinct inTr.vendor_id from ftd_apps.inv_trk inTr where inTr.product_id = vp.PRODUCT_SUBCODE_ID 
              and IN_SHIP_DATE BETWEEN  inTr.START_DATE and inTr.END_DATE  AND inTr.STATUS = 'A'
              )

    AND NOT EXISTS (
        SELECT 1 FROM FTD_APPS."VENDOR_DELIV_BLOCK_LIST_VW" dvw
        WHERE dvw."VENDOR_ID" = vm."VENDOR_ID" AND dvw."START_DATE" = in_delivery_date
    )

    AND NOT EXISTS (
        SELECT 1 FROM FTD_APPS."VENDOR_SHIP_BLOCK_LIST_VW" svw
        WHERE svw."VENDOR_ID" = vm."VENDOR_ID" AND svw."START_DATE" = in_ship_date
    )
    AND  vm.VENDOR_ID in 
    (
      SELECT 
        CASE 
          WHEN  trunc(IN_SHIP_DATE) <= trunc(sysdate) THEN 
          (
            SELECT vm1.VENDOR_ID 
            FROM   FTD_APPS.VENDOR_MASTER vm1  
            WHERE  vm1.VENDOR_ID = vm.VENDOR_ID 
            AND (TRUNC(SYSDATE)+ (TO_NUMBER(SUBSTR(vm1.CUTOFF_TIME,1,2))*(1/24))+ (TO_NUMBER(SUBSTR (vm1.CUTOFF_TIME,4,2))*(1/1440)) + (v_backend_delay*(1/1440)) ) > sysdate
          )
          ELSE 
          (
            SELECT vm1.VENDOR_ID 
            FROM   FTD_APPS.VENDOR_MASTER vm1  
            WHERE  vm1.VENDOR_ID = vm.VENDOR_ID 
          )
        END
      from dual
    );



  RETURN vendor_cursor;
END GET_SHIPPING_VENDORS;

FUNCTION GET_SDS_PRODUCT_SHIP_METHODS (
  in_carrier_id IN CARRIERS.CARRIER_ID%TYPE,
  in_vendor_id IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
  in_product_subcode_id IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_SDS_PRODUCT_SHIP_METHODS
-- Type:    Function
-- Syntax:  GET_SDS_PRODUCT_SHIP_METHODS (carrier_id,vendor_id,product_subcode_id)
-- Returns: ref_cursor for
--          SHIP_METHOD          VARCHAR2
--          CARRIER_ID           VARCHAR2
--
--
-- Description:  Looks for carrier ship methods that are available for the passed
--               in product/subcode id.  If the passed in carrier method is null,
--               then ship methods for all carriers are reurned.
--
--==============================================================================
AS
  method_cursor types.ref_cursor;
  v_hasSubCodes VARCHAR2(1);
BEGIN
  BEGIN
    SELECT 'Y' INTO v_hasSubCodes
      FROM FTD_APPS."PRODUCT_SUBCODES"
      WHERE ("PRODUCT_SUBCODE_ID" = in_product_subcode_id OR "PRODUCT_ID" = in_product_subcode_id)
        AND ROWNUM=1;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_hasSubCodes := 'N';
  END;

 IF( v_hasSubCodes = 'Y' ) THEN
  OPEN method_cursor FOR
     SELECT distinct sx."SDS_SHIP_METHOD", sx."CARRIER_ID"
       FROM VENUS."SDS_SHIP_METHOD_XREF" sx
       JOIN VENUS."VENDOR_CARRIER_REF" vcx ON sx."CARRIER_ID" = vcx."CARRIER_ID"
       JOIN FTD_APPS."VENDOR_PRODUCT" vp ON vcx."VENDOR_ID" = vp."VENDOR_ID"
       JOIN FTD_APPS."PRODUCT_SUBCODES" ps ON
         (vp."PRODUCT_SUBCODE_ID" = ps."PRODUCT_SUBCODE_ID" OR vp."PRODUCT_SUBCODE_ID" = ps."PRODUCT_ID")
       JOIN FTD_APPS."PRODUCT_MASTER" pm ON ps."PRODUCT_ID" = pm."PRODUCT_ID"
       JOIN FTD_APPS."PRODUCT_SHIP_METHODS" psm ON pm."PRODUCT_ID" = psm."PRODUCT_ID"
       WHERE
         (sx."CARRIER_ID"=in_carrier_id OR in_carrier_id IS NULL) AND
         vcx."VENDOR_ID"=in_vendor_id  AND
         (ps."PRODUCT_ID"=in_product_subcode_id OR ps."PRODUCT_SUBCODE_ID"=in_product_subcode_id) AND
         pm."PRODUCT_ID"=ps."PRODUCT_ID" AND
         (pm."EXPRESS_SHIPPING_ONLY" = 'N' OR sx."AIR_METHOD" = 'Y') AND
         psm."SHIP_METHOD_ID" = sx."FTD_SHIP_METHOD_ID" AND
         vcx."USAGE_PERCENT" = -1 AND
         sx."ACTIVE" = 'Y' AND
         vp."AVAILABLE" = 'Y';
ELSE
  OPEN method_cursor FOR
     SELECT distinct sx."SDS_SHIP_METHOD", sx."CARRIER_ID"
       FROM VENUS."SDS_SHIP_METHOD_XREF" sx
       JOIN VENUS."VENDOR_CARRIER_REF" vcx ON sx."CARRIER_ID" = vcx."CARRIER_ID"
       JOIN FTD_APPS."VENDOR_PRODUCT" vp ON vcx."VENDOR_ID" = vp."VENDOR_ID"
       JOIN FTD_APPS."PRODUCT_MASTER" pm ON vp."PRODUCT_SUBCODE_ID" = pm."PRODUCT_ID"
       JOIN FTD_APPS."PRODUCT_SHIP_METHODS" psm ON pm."PRODUCT_ID" = psm."PRODUCT_ID"
       WHERE
        (sx."CARRIER_ID"=in_carrier_id OR in_carrier_id IS NULL) AND
         vcx."VENDOR_ID"=in_vendor_id  AND
         vp."PRODUCT_SUBCODE_ID"=in_product_subcode_id  AND
         (pm."EXPRESS_SHIPPING_ONLY" = 'N' OR sx."AIR_METHOD" = 'Y') AND
         psm."SHIP_METHOD_ID" = sx."FTD_SHIP_METHOD_ID" AND
         vcx."USAGE_PERCENT" = -1 AND
         sx."ACTIVE" = 'Y' AND
         vp."AVAILABLE" = 'Y';
END IF;
  return method_cursor;
END GET_SDS_PRODUCT_SHIP_METHODS;

FUNCTION GET_SDS_VENDOR_CARRIERS (
  in_vendor_id IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
  in_postal_code IN CARRIER_EXCLUDED_ZIPS.ZIP_CODE%TYPE)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_SDS_VENDOR_CARRIERS
-- Type:    Function
-- Syntax:  GET_SDS_VENDOR_CARRIERS (vendor_id)
-- Returns: ref_cursor for
--          CARRIER_ID           VARCHAR2
--          POSTAL_CODE          VARCHAR2
--
--
-- Description:  Looks for carriers for the passed in vendor id
--
--==============================================================================
AS
  method_cursor types.ref_cursor;
BEGIN
  OPEN method_cursor FOR
     SELECT vcx."CARRIER_ID" FROM VENUS."VENDOR_CARRIER_REF" vcx
       JOIN VENUS."CARRIERS" c ON vcx."CARRIER_ID" = c."CARRIER_ID"
       WHERE vcx."VENDOR_ID" = in_vendor_id AND
       vcx."USAGE_PERCENT" = -1 AND
       c."ACTIVE"='Y' AND
       c.carrier_id NOT IN (SELECT carrier_id
         FROM venus.carrier_excluded_zips
         WHERE NVL(in_postal_code,'NULLVALUE') = zip_code)
     ORDER BY CARRIER_ID;

  return method_cursor;
END GET_SDS_VENDOR_CARRIERS;

FUNCTION GET_VENDOR_CARRIERS_TO_CLOSE (
  IN_VENDOR              IN VARCHAR2,
  IN_CARRIER_ID          IN CARRIERS.CARRIER_ID%TYPE, 
  IN_ZJ_FLAG             IN CHAR,
  IN_DAYS_IN_TRANSIT_QTY IN ZJ_TRIP.DAYS_IN_TRANSIT_QTY%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDOR_CARRIERS_TO_CLOSE
-- Type:    Function
-- Syntax:  GET_VENDOR_CARRIERS_TO_CLOSE
-- Returns: ref_cursor for
--          MEMBER_NUMBER        VARCHAR2
--          CARRIER_ID           VARCHAR2
--          ZJ_FLAG              VARCHAR2
--
--
-- Description:  Returns
--               If vendor and carrier_id is null it returns a ref cursor from
--               a call to GET_VENDOR_CARRIERS_TO_CLOSE().  Otherwise, a record
--               will be returned for every vendor/carrier combination.
--
--==============================================================================
AS
  carrier_cursor types.ref_cursor;
BEGIN
  IF( IN_ZJ_FLAG IS NOT NULL AND IN_ZJ_FLAG = 'Y' ) THEN
    carrier_cursor := GET_TRAILERS_TO_CLOSE(IN_DAYS_IN_TRANSIT_QTY);
  ELSE
    IF( IN_VENDOR IS NULL AND IN_CARRIER_ID IS NULL ) THEN
      carrier_cursor := GET_VENDOR_CARRIERS_TO_CLOSE();
    ELSE
      IF( IN_VENDOR IS NULL AND IN_CARRIER_ID IS NOT NULL ) THEN
        carrier_cursor := GET_VENDOR_CARRIERS_TO_CLOSE(IN_CARRIER_ID);
      ELSE
        OPEN carrier_cursor FOR
          SELECT DISTINCT v.FILLING_VENDOR, v.FINAL_CARRIER, v.ZONE_JUMP_TRAILER_NUMBER from venus.venus v
            JOIN FTD_APPS.VENDOR_MASTER vm ON v.FILLING_VENDOR = vm.MEMBER_NUMBER
            JOIN VENUS."CARRIERS" c ON v.FINAL_CARRIER = c."CARRIER_ID"
          WHERE
            v.SHIP_DATE >= trunc(sysdate-1) AND
            v.SHIP_DATE < trunc(sysdate+1) AND
            v.SDS_STATUS = 'PRINTED' AND
            v.SHIPPING_SYSTEM = 'SDS' AND
            (vm."VENDOR_ID" = IN_VENDOR OR vm."MEMBER_NUMBER" = IN_VENDOR) AND
            (c."CARRIER_ID" = IN_CARRIER_ID OR IN_CARRIER_ID IS NULL)
        ORDER BY v.FILLING_VENDOR, v.FINAL_CARRIER;
      END IF;
    END IF;
  END IF;

  return carrier_cursor;
END GET_VENDOR_CARRIERS_TO_CLOSE;

FUNCTION GET_VENDOR_CARRIERS_TO_CLOSE
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDOR_CARRIERS_TO_CLOSE
-- Type:    Function
-- Syntax:  GET_VENDOR_CARRIERS_TO_CLOSE
-- Returns: ref_cursor for
--          MEMBER_NUMBER        VARCHAR2
--          CARRIER_ID           VARCHAR2
--
--
-- Description:  Returns a record for every vendor that a carrier has available
--               if the vendor cutoff time is in the past for that day.
--
--==============================================================================
AS
  carrier_cursor types.ref_cursor;
BEGIN
  OPEN carrier_cursor FOR
  SELECT DISTINCT v.FILLING_VENDOR, v.FINAL_CARRIER, v.ZONE_JUMP_TRAILER_NUMBER from venus.venus v
    JOIN FTD_APPS.VENDOR_MASTER vm ON v.FILLING_VENDOR = vm.MEMBER_NUMBER
    JOIN VENUS."CARRIERS" c ON v.FINAL_CARRIER = c."CARRIER_ID"
    WHERE
      v.SHIP_DATE >= trunc(sysdate-1) AND
      v.SHIP_DATE < trunc(sysdate+1) AND
      v.SDS_STATUS = 'PRINTED' AND
      v.SHIPPING_SYSTEM = 'SDS' AND
      TO_NUMBER(SUBSTR(vm."CUTOFF_TIME",1,2)||SUBSTR(vm."CUTOFF_TIME",4,2))  < to_number(to_char(sysdate, 'HH24MI'))
    ORDER BY v.FILLING_VENDOR, v.FINAL_CARRIER;

  return carrier_cursor;
END GET_VENDOR_CARRIERS_TO_CLOSE;

FUNCTION GET_VENDOR_CARRIERS_TO_CLOSE (
  IN_CARRIER_ID IN CARRIERS.CARRIER_ID%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDOR_CARRIERS_TO_CLOSE
-- Type:    Function
-- Syntax:  GET_VENDOR_CARRIERS_TO_CLOSE
-- Returns: ref_cursor for
--          CARRIER_ID           VARCHAR2
--
--
-- Description:  Returns
--               a record
--               will be returned for every active vendor who ships with the
--               passed in carrier.  Cutoff time will be checked.
--
--==============================================================================
AS
  carrier_cursor types.ref_cursor;
BEGIN
  OPEN carrier_cursor FOR
  SELECT DISTINCT v.FILLING_VENDOR, v.FINAL_CARRIER, v.ZONE_JUMP_TRAILER_NUMBER from venus.venus v
    JOIN FTD_APPS.VENDOR_MASTER vm ON v.FILLING_VENDOR = vm.MEMBER_NUMBER
    JOIN VENUS."CARRIERS" c ON v.FINAL_CARRIER = c."CARRIER_ID"
    WHERE
      v.SHIP_DATE >= trunc(sysdate-1) AND
      v.SHIP_DATE < trunc(sysdate+1) AND
      v.SDS_STATUS = 'PRINTED' AND
      v.SHIPPING_SYSTEM = 'SDS' AND
      TO_NUMBER(SUBSTR(vm."CUTOFF_TIME",1,2)||SUBSTR(vm."CUTOFF_TIME",4,2))  < to_number(to_char(sysdate, 'HH24MI')) AND
      c."CARRIER_ID" = IN_CARRIER_ID
    ORDER BY v.FILLING_VENDOR, v.FINAL_CARRIER;

  return carrier_cursor;
END GET_VENDOR_CARRIERS_TO_CLOSE;


--==============================================================================
--
-- Name:    GET_TRAILERS_TO_CLOSE
-- Type:    Function
-- Syntax:  GET_TRAILERS_TO_CLOSE
-- Returns: ref_cursor for
--
--
-- Description:  Returns
--               a record
--               will be returned for every active vendor who ships with the
--               passed in carrier.  Cutoff time will be checked.
--
--==============================================================================
FUNCTION GET_TRAILERS_TO_CLOSE 
(
  IN_DAYS_IN_TRANSIT_QTY IN ZJ_TRIP.DAYS_IN_TRANSIT_QTY%TYPE
)
RETURN TYPES.ref_cursor
AS
  carrier_cursor types.ref_cursor;
BEGIN
  IF(IN_DAYS_IN_TRANSIT_QTY IS NOT NULL AND IN_DAYS_IN_TRANSIT_QTY = 0 ) THEN
    OPEN carrier_cursor FOR
      SELECT  DISTINCT null as FILLING_VENDOR, null as FINAL_CARRIER, v.ZONE_JUMP_TRAILER_NUMBER
        FROM  VENUS.VENUS v
       WHERE  TRUNC(v.ZONE_JUMP_LABEL_DATE) =  TRUNC(SYSDATE)
         AND  TRUNC(v.ZONE_JUMP_LABEL_DATE) =  TRUNC(v.SHIP_DATE)  
         AND  v.ZONE_JUMP_FLAG IS NOT NULL
         AND  v.ZONE_JUMP_FLAG = 'Y'
         AND  v.SDS_STATUS = 'PRINTED' 
         AND  v.SHIPPING_SYSTEM = 'SDS' 
    ORDER BY  v.ZONE_JUMP_TRAILER_NUMBER;
  ELSE
    OPEN carrier_cursor FOR
      SELECT  DISTINCT null as FILLING_VENDOR, null as FINAL_CARRIER, v.ZONE_JUMP_TRAILER_NUMBER
        FROM  VENUS.VENUS v
       WHERE  TRUNC(v.ZONE_JUMP_LABEL_DATE) =  TRUNC(SYSDATE - 1 )
         AND  v.ZONE_JUMP_FLAG IS NOT NULL
         AND  v.ZONE_JUMP_FLAG = 'Y'
         AND  v.SDS_STATUS = 'PRINTED' 
         AND  v.SHIPPING_SYSTEM = 'SDS' 
    ORDER BY  v.ZONE_JUMP_TRAILER_NUMBER;
  END IF;

  return carrier_cursor;

END GET_TRAILERS_TO_CLOSE;


FUNCTION CAN_VENDOR_SHIP_ON_DATE (
  in_vendor_id IN FTD_APPS."VENDOR_SHIP_BLOCK_LIST_VW".VENDOR_ID%TYPE,
  in_ship_date IN FTD_APPS."VENDOR_SHIP_BLOCK_LIST_VW".START_DATE%TYPE)
RETURN VARCHAR2
--==============================================================================
--
-- Name:    CAN_VENDOR_SHIP_ON_DATE
-- Type:    Function
-- Syntax:  CAN_VENDOR_SHIP_ON_DATE( vendor_id, ship_date)
-- Returns: VARCHAR2 ('Y' or 'N')
--
--
-- Description:  Returns 'N' if the passed vendor is blocked from shipping on
--               the passed ship date.  Otherwise, it returns 'Y'.
--
--==============================================================================
AS
  v_canShip VARCHAR2(1) := 'Y';
BEGIN
  BEGIN
    SELECT 'N' INTO v_canShip
         FROM FTD_APPS."VENDOR_SHIP_BLOCK_LIST_VW"
         WHERE "VENDOR_ID" = in_vendor_id  AND "START_DATE" = TRUNC(in_ship_date);
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
   v_canShip := 'Y';
  END;

  return v_canShip;
END CAN_VENDOR_SHIP_ON_DATE;

FUNCTION GET_SDS_ERROR_PROCESSING (
  IN_ERROR_ID IN VARCHAR2,
  IN_SOURCE IN SDS_ERROR.SOURCE%TYPE)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_SDS_ERROR_PROCESSING
-- Type:    Function
-- Syntax:  GET_SDS_ERROR_PROCESSING( error_id, source)
-- Returns: ref cursor:
--                     error_id number
--                     source varchar2
--                     disposition varchar2
--                     page_support varchar2
--                     order_comment varchar2
--
--
-- Description:  Returns the VENUS.SDS_ERROR record with the passed in values.
--
--==============================================================================
AS
  error_cursor types.ref_cursor;
BEGIN
  OPEN error_cursor FOR
       SELECT
         se."ERROR_ID",
         se."SOURCE",
         se."DISPOSITION",
         se."PAGE_SUPPORT",
         se."ORDER_COMMENT"
       FROM VENUS."SDS_ERROR" se
       WHERE ABS("ERROR_ID") = ABS(TO_NUMBER(IN_ERROR_ID)) and "SOURCE" = IN_SOURCE and ROWNUM = 1;

  RETURN error_cursor;

END GET_SDS_ERROR_PROCESSING;

FUNCTION GET_FTD_SHIP_METHOD (
  IN_CARRIER_ID IN SDS_SHIP_METHOD_XREF.CARRIER_ID%TYPE,
  IN_SHIP_METHOD IN SDS_SHIP_METHOD_XREF.SDS_SHIP_METHOD%TYPE )
RETURN VARCHAR2
--==============================================================================
--
-- Name:    GET_FTD_SHIP_METHOD
-- Type:    Function
-- Syntax:  GET_FTD_SHIP_METHOD( carrier_id, ship_method)
-- Returns: String ftd ship method
--
--
-- Description:  Returns the ftd ship method for the passed in values.
--
--==============================================================================
AS
  v_ftdShipMethod VARCHAR2(10);
BEGIN
     SELECT "FTD_SHIP_METHOD_ID" INTO v_ftdShipMethod FROM VENUS."SDS_SHIP_METHOD_XREF" WHERE "CARRIER_ID" = IN_CARRIER_ID AND "SDS_SHIP_METHOD" = IN_SHIP_METHOD;
     RETURN v_ftdShipMethod;
END GET_FTD_SHIP_METHOD;

FUNCTION GET_SDS_SHIP_VIA (
  IN_SHIP_METHOD IN FTD_APPS.SHIP_METHODS.SHIP_METHOD_ID%TYPE,
  IN_AIR_ONLY IN VARCHAR2
)
RETURN VARCHAR2
--==============================================================================
--
-- Name:    GET_SDS_SHIP_VIA
-- Type:    Function
-- Syntax:  GET_SDS_SHIP_VIA(ship_method, airVia)
-- Returns: varchar2 ship via (varchar2) and ship via air (varchar2)
--
--
-- Description:  Returns ship via or ship via air  for the passed in parameters.
--
--==============================================================================
AS
  v_shipVia FTD_APPS.SHIP_METHODS.SDS_SHIP_VIA%TYPE;
BEGIN
  IF IN_AIR_ONLY = 'Y' THEN
    SELECT "SDS_SHIP_VIA_AIR" INTO v_shipVia
      FROM FTD_APPS."SHIP_METHODS"
      WHERE "SHIP_METHOD_ID" = IN_SHIP_METHOD;
  ELSE
    SELECT "SDS_SHIP_VIA" INTO v_shipVia
      FROM FTD_APPS."SHIP_METHODS"
      WHERE "SHIP_METHOD_ID" = IN_SHIP_METHOD;
  END IF;

  return v_shipVia;

END GET_SDS_SHIP_VIA;

PROCEDURE INSERT_SDS_STATUS_MESSAGE
(
    IN_CARTON_NUMBER      IN  SDS_STATUS_MESSAGE.CARTON_NUMBER%TYPE,
    IN_TRACKING_NUMBER    IN  SDS_STATUS_MESSAGE.TRACKING_NUMBER%TYPE,
    IN_BATCH_NUMBER       IN  SDS_STATUS_MESSAGE.PRINT_BATCH_NUMBER%TYPE,
    IN_BATCH_SEQUENCE     IN  SDS_STATUS_MESSAGE.PRINT_BATCH_SEQUENCE%TYPE,
    IN_SHIP_DATE          IN  SDS_STATUS_MESSAGE.SHIP_DATE%TYPE,
    IN_STATUS             IN  SDS_STATUS_MESSAGE.STATUS%TYPE,
    IN_STATUS_DATE        IN  SDS_STATUS_MESSAGE.STATUS_DATE%TYPE,
    IN_PROCESSED          IN  SDS_STATUS_MESSAGE.PROCESSED%TYPE,
    IN_DIRECTION          IN  SDS_STATUS_MESSAGE.MESSAGE_DIRECTION%TYPE,
    OUT_MSG_ID            OUT NUMBER,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table venus.SDS_STATUS_MESSAGE.

Input:
        carton_number    varchar2,
        tracking_number  varchar2,
        batch_number     varchar2,
        batch_sequence   number,
        ship_date        date,
        status           varchar2,
        status_date      date,
        processed        char,
        direction        varchar2

Output:
        msg_id
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  SELECT SDS_STATUS_MESSAGE_SQ.NEXTVAL
    INTO out_msg_id
    FROM DUAL;

  INSERT INTO VENUS.SDS_STATUS_MESSAGE (
    "ID",
    "CARTON_NUMBER",
    "TRACKING_NUMBER",
    "PRINT_BATCH_NUMBER",
    "PRINT_BATCH_SEQUENCE",
    "SHIP_DATE",
    "STATUS",
    "STATUS_DATE",
    "PROCESSED",
    "CREATED_ON",
    "UPDATED_ON",
    "MESSAGE_DIRECTION"
)
VALUES(
    OUT_MSG_ID,
    IN_CARTON_NUMBER,
    IN_TRACKING_NUMBER,
    IN_BATCH_NUMBER,
    IN_BATCH_SEQUENCE,
    IN_SHIP_DATE,
    IN_STATUS,
    IN_STATUS_DATE,
    IN_PROCESSED,
    SYSDATE,
    SYSDATE,
    IN_DIRECTION);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
  --It's already in the database, so report success
  OUT_STATUS := 'Y';
  OUT_MESSAGE := 'DUPLICATE';
  OUT_MSG_ID := -1;
WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SDS_STATUS_MESSAGE;

PROCEDURE MARK_SDS_MESSAGE_PROCESSED
(
    IN_ID                 IN  SDS_STATUS_MESSAGE.ID%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the PROCESSED column in the table
        venus.SDS_STATUS_MESSAGE for the passed in ID.

Input:
        id                number

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE "SDS_STATUS_MESSAGE"
    SET "PROCESSED" = 'Y',
         "UPDATED_ON" = SYSDATE
    WHERE ID = IN_ID;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END MARK_SDS_MESSAGE_PROCESSED;

FUNCTION GET_SDS_STATUS_MESSAGE
(
  IN_ID      IN SDS_STATUS_MESSAGE.ID%TYPE
) RETURN TYPES.ref_cursor
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the sds_status_message for
        the passed in id

Input:
        id number

Output:
        cursor containing record from sds_status_message for the id

-----------------------------------------------------------------------------*/
OUT_CUR TYPES.ref_cursor;

BEGIN

OPEN OUT_CUR FOR
  SELECT
    "ID",
    "CARTON_NUMBER",
    "TRACKING_NUMBER",
    "PRINT_BATCH_NUMBER",
    "PRINT_BATCH_SEQUENCE",
    "SHIP_DATE",
    "STATUS_DATE",
    "PROCESSED",
    "STATUS",
    "MESSAGE_DIRECTION"
  FROM "SDS_STATUS_MESSAGE" WHERE "ID" = IN_ID;

  return OUT_CUR;
END GET_SDS_STATUS_MESSAGE;

PROCEDURE UPDATE_VENUS_SDS
(
IN_VENUS_ID               IN VENUS.VENUS_ID%TYPE,
IN_VENUS_STATUS             IN VENUS.VENUS_STATUS%TYPE,
IN_FILLING_VENDOR           IN VENUS.FILLING_VENDOR%TYPE,
IN_PRICE                    IN VENUS.PRICE%TYPE,
IN_MESSAGE_TEXT           IN VENUS.MESSAGE_TEXT%TYPE,
IN_ORDER_PRINTED          IN VENUS.ORDER_PRINTED%TYPE,
IN_ERROR_DESCRIPTION          IN VENUS.ERROR_DESCRIPTION%TYPE,
IN_CANCEL_REASON_CODE       IN VENUS.CANCEL_REASON_CODE%TYPE,
IN_VENDOR_SKU             IN VENUS.VENDOR_SKU%TYPE,
IN_EXTERNAL_SYSTEM_STATUS     IN VENUS.EXTERNAL_SYSTEM_STATUS%TYPE,
IN_VENDOR_ID              IN VENUS.VENDOR_ID%TYPE,
IN_TRACKING_NUMBER          IN VENUS.TRACKING_NUMBER%TYPE,
IN_PRINTED_DATE             IN VENUS.PRINTED%TYPE,
IN_SHIPPED_DATE             IN VENUS.SHIPPED%TYPE,
IN_CANCELLED_DATE           IN VENUS.CANCELLED%TYPE,
IN_REJECTED_DATE            IN VENUS.REJECTED%TYPE,
IN_FINAL_CARRIER            IN VENUS.FINAL_CARRIER%TYPE,
IN_FINAL_SHIP_METHOD          IN VENUS.FINAL_SHIP_METHOD%TYPE,
IN_FORCED_SHIPMENT          IN VENUS.FORCED_SHIPMENT%TYPE,
IN_RATED_SHIPMENT           IN VENUS.RATED_SHIPMENT%TYPE,
IN_GET_SDS_RESPONSE         IN VENUS.GET_SDS_RESPONSE%TYPE,
IN_ORIG_SHIP_DATE           IN VENUS.ORIG_SHIP_DATE%TYPE,
IN_SECONDARY_SHIP_DATE      IN VENUS.SECONDARY_SHIP_DATE%TYPE,
IN_SHIP_DATE            IN VENUS.SHIP_DATE%TYPE,
IN_FTDWEST_ORDER_ID         IN VENUS.FTDWEST_ORDER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating record(s) into
        the venus.venus table

Input:
        venus_id                varchar2
        venus_status              varchar2
        filling_vendor            varchar2
        price                     number
        message_text              varchar2
        order_printed             char
        error_description         varchar2
        cancel_reason_code          varchar2
        vendor_sku              varchar2
        external_system_status      varchar2
        vendor_id               varchar2
        tracking_number           varchar2
        printed_date              date
        shipped_date              date
        cancelled_date            date
        rejected_date             date
        final_carrier           varchar2
        final_ship_method         varchar2
        forced_shipment           varchar2
        rated_shipment            varchar2
        get_sds_response          varchar2
        orig_ship_date        date
        secondary_ship_date   date
        ship_date         varchar2
        ftdwest_order_id number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN


   UPDATE venus
      SET venus_status = in_venus_status,
         filling_vendor = in_filling_vendor,
         price = in_price,
         message_text = in_message_text,
         order_printed = in_order_printed,
         error_description = in_error_description,
         cancel_reason_code = in_cancel_reason_code,
         vendor_sku = in_vendor_sku,
         external_system_status = in_external_system_status,
         vendor_id = in_vendor_id,
         tracking_number = in_tracking_number,
         printed = in_printed_date,
         shipped = in_shipped_date,
         cancelled = in_cancelled_date,
         rejected = in_rejected_date,
         final_carrier = in_final_carrier,
         final_ship_method = in_final_ship_method,
         forced_shipment = in_forced_shipment,
         rated_shipment = in_rated_shipment,
         get_sds_response = in_get_sds_response,
         orig_ship_date = IN_ORIG_SHIP_DATE,
         secondary_ship_date = IN_SECONDARY_SHIP_DATE,
         ship_date = IN_SHIP_DATE,
         ftdwest_order_id = IN_FTDWEST_ORDER_ID,
         updated_on = sysdate
    WHERE venus_id = in_venus_id;

  OUT_STATUS := 'Y';
  OUT_MESSAGE := NULL;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENUS.SHIP_PKG.UPDATE_VENUS_SDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENUS_SDS;

PROCEDURE UPDATE_VENUS_STATUS
(
 IN_VENUS_ID               IN venus.venus_id%type,
 IN_VENUS_STATUS           IN venus.venus_status%type,
 IN_EXTERNAL_STATUS        IN venus.external_system_status%type,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure the venus status and external status in table venus
        for the id passed in.

Input:
        venus_id        VARCHAR2
        venus_status    VARCHAR2
        external_status VARCHAR2

Output:
        status          VARCHAR2
        message         VARCHAR2

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE venus
    SET venus_status = in_venus_status,
        external_system_status = in_external_status,
        updated_on = SYSDATE
    WHERE venus_id = in_venus_id;

  IF SQL%ROWCOUNT = 0 THEN
     out_status := 'N';
     out_message := 'No record found/updated for venus id ' || in_venus_id || '.';
  ELSE
     out_status := 'Y';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_VENUS_STATUS;

PROCEDURE MARK_VENUS_MSG_CANCELLED
(
IN_VENUS_ID                     IN VENUS.VENUS_ID%TYPE,
IN_CANCELLED_TIME               IN VENUS.SHIPPED%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the cancelled timestamp on
        the venus.venus record for the passed in id

Input:
        venus_id                        varchar2
        cancelled_time                  varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE VENUS SET "CANCELLED" = IN_CANCELLED_TIME, "UPDATED_ON"=SYSDATE WHERE "VENUS_ID" = IN_VENUS_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END MARK_VENUS_MSG_CANCELLED;

PROCEDURE MARK_VENUS_MSG_REJECTED
(
IN_VENUS_ID                     IN VENUS.VENUS_ID%TYPE,
IN_REJECTED_TIME                IN VENUS.REJECTED%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the rejected timestamp on
        the venus.venus record for the passed in id

Input:
        venus_id                        varchar2
        rejected_time                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE VENUS SET "REJECTED" = IN_REJECTED_TIME, "UPDATED_ON"=SYSDATE WHERE "VENUS_ID" = IN_VENUS_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END MARK_VENUS_MSG_REJECTED;

PROCEDURE MARK_VENUS_MSG_SHIPPED
(
IN_VENUS_ID                     IN VENUS.VENUS_ID%TYPE,
IN_SHIPPED_TIME                 IN VENUS.SHIPPED%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the shipped timestamp on
        the venus.venus record for the passed in id.  If it's an FTD West 
        shipping system also update the ship_date on the venus record.

Input:
        venus_id                        varchar2
        shipped_time                    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_shipping_system             varchar2(100) := 'SDS';

BEGIN

select shipping_system into v_shipping_system from venus where venus_id = in_venus_id;

IF v_shipping_system = 'FTD WEST' THEN
	UPDATE VENUS SET "SHIPPED" = IN_SHIPPED_TIME, "SHIP_DATE" = trunc(IN_SHIPPED_TIME), "UPDATED_ON"=SYSDATE WHERE "VENUS_ID" = IN_VENUS_ID;
ELSE
	UPDATE VENUS SET "SHIPPED" = IN_SHIPPED_TIME, "UPDATED_ON"=SYSDATE WHERE "VENUS_ID" = IN_VENUS_ID;
END IF;	

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END MARK_VENUS_MSG_SHIPPED;

PROCEDURE MARK_VENUS_MSG_PRINTED
(
IN_VENUS_ID                     IN VENUS.VENUS_ID%TYPE,
IN_PRINTED_TIME                 IN VENUS.PRINTED%TYPE,
IN_BATCH_NUMBER                 IN VENUS.BATCH_NUMBER%TYPE,
IN_BATCH_SEQUENCE               IN VENUS.BATCH_SEQUENCE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the printed timestamp and flag on
        the venus.venus record for the passed in id

Input:
        venus_id                        varchar2
        printed_time                    varchar2
        batch_number                    varchar2
        batch_sequence                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE VENUS.VENUS
  SET
       "PRINTED" = IN_PRINTED_TIME,
       "ORDER_PRINTED" = 'Y',
       "BATCH_NUMBER" = IN_BATCH_NUMBER,
       "BATCH_SEQUENCE" = IN_BATCH_SEQUENCE,
       "UPDATED_ON"=sysdate
  WHERE "VENUS_ID" = IN_VENUS_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END MARK_VENUS_MSG_PRINTED;

PROCEDURE INSERT_SDS_TRANSACTION
(
    IN_VENUS_ID           IN  SDS_TRANSACTION.VENUS_ID%TYPE,
    IN_REQUEST            IN  SDS_TRANSACTION.REQUEST%TYPE,
    IN_RESPONSE           IN  SDS_TRANSACTION.RESPONSE%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_SDS_TRANSACTION
 Type:    Procedure
 Syntax:  INSERT_SDS_TRANSACTION(venus_id, request, response)

Description:
        This stored procedure inserts a record into table venus.SDS_TRANSACTION.

Input:
        venus_id         varchar2,
        request          clob,
        response         clob

Output:
        status
        error message

-----------------------------------------------------------------------------*/
v_newId number;
BEGIN

  SELECT SDS_STATUS_MESSAGE_SQ.NEXTVAL
    INTO v_newId
    FROM DUAL;

  INSERT INTO VENUS.SDS_TRANSACTION (
    "ID",
    "VENUS_ID",
    "REQUEST",
    "RESPONSE",
    "CREATED_ON"
  )
  VALUES(
    v_newId,
    IN_VENUS_ID,
    IN_REQUEST,
    IN_RESPONSE,
    SYSDATE);

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SDS_TRANSACTION;

FUNCTION GET_SDS_TRANS_BY_VENUS_ID
(
    IN_VENUS_ID           IN  SDS_TRANSACTION.VENUS_ID%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_SDS_TRANS_BY_VENUS_ID
-- Type:    Function
-- Syntax:  GET_SDS_TRANS_BY_VENUS_ID
-- Returns: ref_cursor for
--          id           varchar2
--          venus_id     request
--          request      clob
--          response     clob
--
-- Description:  Return all records in the SDS_TRANSACTIONS table for the passed
--               in venus id.
--
--==============================================================================
AS
  trans_cursor types.ref_cursor;
BEGIN
  OPEN trans_cursor FOR
    SELECT
      s1.ID,
      s1.VENUS_ID,
      s1.REQUEST,
      s1.RESPONSE
    FROM VENUS."SDS_TRANSACTION" s1
    WHERE s1.VENUS_ID = IN_VENUS_ID
    AND s1.ID = (
      SELECT MAX(s2.ID)
        FROM VENUS.SDS_TRANSACTION s2
      WHERE s2.Venus_Id = IN_VENUS_ID);

  return trans_cursor;
END GET_SDS_TRANS_BY_VENUS_ID;

FUNCTION GET_FTD_SHIP_DATA
(
  IN_CARRIER_ID IN SDS_SHIP_METHOD_XREF.CARRIER_ID%TYPE,
  IN_SHIP_METHOD IN SDS_SHIP_METHOD_XREF.SDS_SHIP_METHOD%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_FTD_SHIP_DATA
-- Type:    Function
-- Syntax:  GET_FTD_SHIP_DATA
-- Returns: ref_cursor for
--          carrier_id            varchar2
--          ship_method           varchar2
--          carrier_delivery      varchar2
--
--==============================================================================
AS
  out_cur types.ref_cursor;
BEGIN
  OPEN out_cur FOR
    SELECT x."CARRIER_ID", x."FTD_SHIP_METHOD_ID", c."CARRIER_DELIVERY"
           FROM VENUS."SDS_SHIP_METHOD_XREF" x, VENUS."CARRIER_DELIVERY" c
           WHERE x."CARRIER_ID" = IN_CARRIER_ID
             AND x."SDS_SHIP_METHOD" = IN_SHIP_METHOD
             AND c."CARRIER_ID" = IN_CARRIER_ID
             AND c."SHIP_METHOD" = x."FTD_SHIP_METHOD_ID";

  return out_cur;
END GET_FTD_SHIP_DATA;

FUNCTION GET_CARRIER_SCAN_FORMAT
(
  IN_CARRIER_ID IN CARRIER_SCANS.CARRIER_ID%TYPE
)
RETURN VARCHAR2
--==============================================================================
--
-- Name:    GET_CARRIER_SCAN_FORMAT
-- Type:    Function
-- Syntax:  GET_CARRIER_SCAN_FORMAT
-- Returns: carrier_id            varchar2
--
--==============================================================================
AS
  v_data CARRIERS.SCAN_FORMAT%TYPE;
BEGIN

  BEGIN
  SELECT CARRIERS.SCAN_FORMAT INTO v_data FROM VENUS.CARRIERS WHERE CARRIERS.CARRIER_ID = IN_CARRIER_ID;

  EXCEPTION
   WHEN NO_DATA_FOUND THEN
   v_data := NULL;
  END;

RETURN v_data;
END GET_CARRIER_SCAN_FORMAT;

FUNCTION GET_ACTIVE_CARRIER_SCANS
(
    IN_CARRIER_ID           IN CARRIER_SCANS.CARRIER_ID%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_ACTIVE_CARRIER_SCANS
-- Type:    Function
-- Syntax:  GET_ACTIVE_CARRIER_SCANS
-- Returns: ref_cursor for
--          carrier_id   varchar2
--
-- Description:  Return all records in the CARRIER_SCANS table for the passed
--               in carrier id which are active.
--
--==============================================================================
AS
  trans_cursor types.ref_cursor;
BEGIN
  OPEN trans_cursor FOR
    SELECT
           CARRIER_ID,
           SCAN_TYPE,
           COLLECT,
           DELIVERY_SCAN
    FROM VENUS.CARRIER_SCANS
    WHERE CARRIER_SCANS.CARRIER_ID = IN_CARRIER_ID AND COLLECT = 'Y'
    ORDER BY SCAN_TYPE;

  return trans_cursor;
END GET_ACTIVE_CARRIER_SCANS;

PROCEDURE INSERT_SCAN
(
    IN_CARRIER_ID         IN  SCANS.CARRIER_ID%TYPE,
    IN_SCAN_TYPE          IN  SCANS.SCAN_TYPE%TYPE,
    IN_FILE_NAME          IN  SCANS.FILE_NAME%TYPE,
    IN_SCAN_TIMESTAMP     IN  SCANS.SCAN_TIMESTAMP%TYPE,
    IN_PROCESSED          IN  SCANS.PROCESSED%TYPE,
    IN_ORDER_ID           IN  SCANS.ORDER_ID%TYPE,
    IN_TRACKING_NUMBER    IN  SCANS.TRACKING_NUMBER%TYPE,
    OUT_ID                OUT NUMBER,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table venus.SCANS.  This
        transaction is AUTONOMOUS so that a JMS message can be fired immediately
        after this procedure returns.

Input:
        carrier_id      varchar2,
        scan_type       varchar2,
        file_name       varchar2,
        scan_timestamp  date,
        processed       varchar2,
        order_id        varchar2,
        tracking_number varchar2

Output:
        msg_id
        status
        error message

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

  SELECT SCANS_SQ.NEXTVAL
    INTO out_id
    FROM DUAL;

  INSERT INTO VENUS.SCANS (
    "ID",
    "CARRIER_ID",
    "SCAN_TYPE",
    "FILE_NAME",
    "SCAN_TIMESTAMP",
    "PROCESSED",
    "ORDER_ID",
    "TRACKING_NUMBER",
    "CREATED_ON",
    "UPDATED_ON"
)
VALUES(
    OUT_ID,
    IN_CARRIER_ID,
    IN_SCAN_TYPE,
    IN_FILE_NAME,
    IN_SCAN_TIMESTAMP,
    IN_PROCESSED,
    IN_ORDER_ID,
    IN_TRACKING_NUMBER,
    SYSDATE,
    SYSDATE);

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END INSERT_SCAN;

PROCEDURE PROCESS_SCAN
(
    IN_SCAN_ID                    IN SCANS.ID%TYPE,
    OUT_STATUS                   OUT VARCHAR2,
    OUT_MESSAGE                  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the LAST_SCAN and DELIVERY_SCAN fields in the
        VENUS.VENUS table for the FTD record of the passed in VENUS_ORDER_NUMBER.

Input:
        scan_id                   number

Output:
        status                    varchar2
        error message             varchar2

-----------------------------------------------------------------------------*/
v_new_last_scan          venus.last_scan%type;
v_new_delivery_scan      venus.delivery_scan%type;
v_venus_id               venus.venus_id%type;
v_reference_number       venus.reference_number%type;
v_last_scan              venus.last_scan%type;
v_delivery_scan          venus.delivery_scan%type;
v_ship_date              venus.ship_date%type;
v_sds_status             venus.sds_status%type;
v_delivery_scan_exists   char(1) := 'N';
v_scans_id               scans.id%type;
v_scans_order_id         scans.order_id%type;
v_scans_carrier_id       scans.carrier_id%type;
v_scans_tracking_number  scans.tracking_number%type;
v_scans_scan_type        scans.scan_type%type;
v_scans_timestamp        scans.scan_timestamp%type;
v_cs_delivery_scan       carrier_scans.delivery_scan%type;
v_cs_collect_scan        carrier_scans.collect%type;
v_send_dcon_email		 char(1) := 'Y';

BEGIN
    --Get the venus.scans record
    BEGIN
        SELECT s.id, s.order_id, s.carrier_id, s.tracking_number, s.scan_type, s.scan_timestamp
          INTO v_scans_id,v_scans_order_id,v_scans_carrier_id,v_scans_tracking_number,v_scans_scan_type,v_scans_timestamp
          FROM venus.SCANS s
          WHERE s.ID = IN_SCAN_ID FOR UPDATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'SCANS record not found for id '||IN_SCAN_ID;
      RETURN;
    END;

    IF v_scans_carrier_id = 'FEDEX' AND LENGTH(v_scans_tracking_number) > 15 THEN
      v_scans_tracking_number := SUBSTR(v_scans_tracking_number, -15);
    END IF;

    --Locate the venus.venus record
    BEGIN
        SELECT v.venus_id, v.reference_number, v.last_scan, v.delivery_scan, v.ship_date, v.sds_status
          INTO v_venus_id, v_reference_number, v_last_scan, v_delivery_scan, v_ship_date, v_sds_status
        FROM venus v
        WHERE v.TRACKING_NUMBER = v_scans_tracking_number AND
              v.MSG_TYPE = 'FTD'
              AND v.VENUS_ID IN (
                  SELECT MAX(v2.VENUS_ID)
                    FROM VENUS v2
                    WHERE v2.TRACKING_NUMBER = v_scans_tracking_number AND
                      v2.MSG_TYPE = 'FTD')
        FOR UPDATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      OUT_STATUS := 'N';
      --Do not change the verbage of the out_message.  The java code is looking
      --for this specific message.
      OUT_MESSAGE := 'VENUS order record not found for scan id '||IN_SCAN_ID;
      RETURN;
    END;

    --Locate the carrier_scans record (insert if not found)
    BEGIN
      SELECT cs.collect, cs.delivery_scan
        INTO v_cs_collect_scan, v_cs_delivery_scan
        FROM carrier_scans cs
        WHERE
          cs.carrier_id = v_scans_carrier_id AND
          cs.scan_type = v_scans_scan_type;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      INSERT INTO carrier_scans (carrier_id, scan_type, collect, delivery_scan)
             VALUES( v_scans_carrier_id, v_scans_scan_type, 'Y', 'N');
      SELECT cs.collect, cs.delivery_scan
        INTO v_cs_collect_scan, v_cs_delivery_scan
        FROM carrier_scans cs
        WHERE
          cs.carrier_id = v_scans_carrier_id AND
          cs.scan_type = v_scans_scan_type;
    END;
    
    --If the scan is not to be collected, then just return
    IF v_cs_collect_scan = 'N' THEN
      --Update the SCANS record
      UPDATE SCANS SET SCANS.PROCESSED = 'Y', SCANS.UPDATED_ON = SYSDATE WHERE scans.id = v_scans_id;
      OUT_STATUS := 'Y';
      return;
    END IF;

    --Which scan should be used for the last scan?
    IF v_last_scan IS NULL OR v_scans_timestamp - v_last_scan > 0 THEN
       v_new_last_scan := v_scans_timestamp;
    ELSE
       v_new_last_scan := v_last_scan;
    END IF;

    --Does a delivery scan already exist?
    IF v_delivery_scan IS NULL THEN
        v_delivery_scan_exists := 'N';
    ELSE
        v_delivery_scan_exists := 'Y';
    END IF;

    --Which scan should be used for the delivery scan?
    IF v_cs_delivery_scan = 'Y' THEN
      IF v_delivery_scan_exists = 'Y' THEN
        v_new_delivery_scan := v_delivery_scan;
      ELSE
        v_new_delivery_scan := v_scans_timestamp;
      END IF;
    ELSE
      v_new_delivery_scan := v_delivery_scan;
    END IF;

    --Update the venus record
    UPDATE VENUS SET
      LAST_SCAN = v_new_last_scan,
      DELIVERY_SCAN = v_new_delivery_scan,
      UPDATED_ON = sysdate
    WHERE VENUS_ID = v_venus_id;

    --Update the SCANS record
    UPDATE SCANS SET SCANS.PROCESSED = 'Y', SCANS.UPDATED_ON = SYSDATE WHERE scans.id = v_scans_id;

    OUT_STATUS := 'Y';
    
    -- Get the Partner mapping flag for SEND_DELIVERY_CONF_EMAIL from the reference number
    BEGIN
	    select pm.send_delivery_conf_email into v_send_dcon_email
		from ptn_pi.partner_mapping pm join ptn_pi.partner_order_detail pod on pm.partner_id = pod.partner_id
		join clean.order_details cod on pod.confirmation_number = cod.external_order_number and cod.order_detail_id = v_reference_number;
	EXCEPTION WHEN NO_DATA_FOUND THEN
		v_send_dcon_email := 'Y';
	END;

    --Enqueue JMS message if necessary
    IF v_cs_delivery_scan = 'Y' AND 
       v_delivery_scan_exists = 'N' AND
       trunc(sysdate)-trunc(v_ship_date) < 30 AND  --defect 3769
       trunc(sysdate)-trunc(v_ship_date) >= 0 AND  --defect 3769
       v_sds_status = 'SHIPPED'                    --defect 3769  
       AND v_send_dcon_email = 'Y'				   --defect 19421
      THEN
       events.POST_A_MESSAGE_FLEX('OJMS.SDS_DELIVERY_CONFIRM',v_reference_number,v_reference_number,5,OUT_STATUS,OUT_MESSAGE);
    END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END PROCESS_SCAN;

PROCEDURE INSERT_SCAN_FILE
(
    IN_CARRIER_ID         IN  SCAN_FILES.CARRIER_ID%TYPE,
    IN_FILE_NAME          IN  SCAN_FILES.FILE_NAME%TYPE,
    IN_USER_ID            IN  SCAN_FILES.CREATED_BY%TYPE,
    IN_CONTENTS           IN  SCAN_FILES.CONTENT_TXT%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_SCAN_FILE
 Type:    Procedure
 Syntax:  INSERT_SCAN_FILE(carrier_id, file_name, user_id, contents, status, message)

Description:
        This stored procedure inserts a record into table venus.scan_files.

Input:
        carrier_id         varchar2,
        file_name          varchar2,
        user               varchar2
        contents           clob,

Output:
        status
        error message

-----------------------------------------------------------------------------*/
v_newId number;
BEGIN

  SELECT SCAN_FILES_SQ.NEXTVAL
    INTO v_newId
    FROM DUAL;

  INSERT INTO VENUS.SCAN_FILES (
    SCAN_FILES.SCAN_FILES_ID,
    SCAN_FILES.CARRIER_ID,
    SCAN_FILES.FILE_NAME,
    SCAN_FILES.CREATED_ON,
    SCAN_FILES.CREATED_BY,
    SCAN_FILES.CONTENT_TXT)
  VALUES(
    v_newId,
    IN_CARRIER_ID,
    IN_FILE_NAME,
    SYSDATE,
    IN_USER_ID,
    IN_CONTENTS);

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_SCAN_FILE;

FUNCTION GET_SCAN_FILE
(
    IN_CARRIER_ID         IN  SCAN_FILES.CARRIER_ID%TYPE,
    IN_FILE_NAME          IN  SCAN_FILES.FILE_NAME%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_SCAN_FILE
-- Type:    Function
-- Syntax:  GET_SCAN_FILE
-- Returns: ref_cursor for
--          content_txt  clob
--
-- Description:  Return most current record in the SCAN_FILES table for the passed
--               in venus id.
--
--==============================================================================
AS
  trans_cursor types.ref_cursor;
BEGIN
  OPEN trans_cursor FOR
    SELECT
      s1.CONTENT_TXT
    FROM VENUS."SCAN_FILES" s1
    WHERE s1.CARRIER_ID = IN_CARRIER_ID AND s1.FILE_NAME = IN_FILE_NAME
    AND s1.SCAN_FILES_ID = (
      SELECT MAX(s2.SCAN_FILES_ID)
        FROM VENUS.SCAN_FILES s2
      WHERE s2.CARRIER_ID = IN_CARRIER_ID AND s2.FILE_NAME = IN_FILE_NAME);

  return trans_cursor;
END GET_SCAN_FILE;

FUNCTION GET_MISSING_STATUS
(
    IN_DAYS_TO_PROCESS   IN  NUMBER
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_MISSING_STATUS
-- Type:    Function
-- Syntax:  GET_MISSING_STATUS
-- Returns: ref_cursor for
--          carton_number  varchar2
--
-- Description:  Returns venus.venus_order_numbers for orders have missing
--               status.  Days to go back are filtered by the DAYS_TO_PROCESS
--               parm.  A value of "1" indicates to get today's only.
--
-- Input:
--        days_to_process    number
--
-- Output:
--        ref_cursor containing the venus.external_order_number
--
--==============================================================================
AS
  carton_cursor types.ref_cursor;
  v_days_in_past number;
  v_today DATE := TRUNC(SYSDATE);
BEGIN
  IF IN_DAYS_TO_PROCESS IS NULL OR IN_DAYS_TO_PROCESS < 1 THEN
    v_days_in_past := 0;
  ELSE
    v_days_in_past := IN_DAYS_TO_PROCESS - 1;
  END IF;

  OPEN carton_cursor FOR
    SELECT v.VENUS_ORDER_NUMBER AS CARTON_NUMBER
      FROM VENUS v
      WHERE
        (v.SHIP_DATE BETWEEN v_today-v_days_in_past and v_today)
        AND v.SDS_STATUS IN ('NEW','AVAILABLE','PRINTED')
        AND v.SHIPPING_SYSTEM = 'SDS'
        AND v.MSG_TYPE = 'FTD'
        AND v.VENUS_STATUS = 'VERIFIED';

  return carton_cursor;
END GET_MISSING_STATUS;

FUNCTION GET_BLOCKED_ZIPS
(
    IN_ZIP_CODE     IN  CARRIER_EXCLUDED_ZIPS.ZIP_CODE%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_BLOCKED_ZIPS
-- Type:    Function
-- Syntax:  GET_BLOCKED_ZIPS
-- Returns: ref_cursor for
--          zip_code   varchar2
--
-- Description:  Returns records from the venus.carrier_excluded zips table
--               for the passed in parameters.
--
-- Input:
--        carrier_id    varchar2
--        zip_code      varchar2
--
-- Output:
--        ref_cursor containing the matching carrier_id's and zip codes
--
--==============================================================================
AS
  zip_cursor types.ref_cursor;
BEGIN

  OPEN zip_cursor FOR
    SELECT z.CARRIER_ID, z.ZIP_CODE FROM VENUS.CARRIER_EXCLUDED_ZIPS z
      WHERE z.ZIP_CODE LIKE IN_ZIP_CODE || '%'
      ORDER BY z.ZIP_CODE;

  return zip_cursor;
END GET_BLOCKED_ZIPS;

PROCEDURE DELETE_BLOCKED_ZIP (
    IN_CARRIER_ID         IN  CARRIER_EXCLUDED_ZIPS.CARRIER_ID%TYPE,
    IN_ZIP_CODE           IN  CARRIER_EXCLUDED_ZIPS.ZIP_CODE%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Delete the record in the carrier_excluded_zips table for the passed in
        parameters.  Return 'Y' on success or 'N' on failure.

Input:
        carrier_id        varchar2
        zip_code          varchar2

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
BEGIN

  DELETE FROM  "CARRIER_EXCLUDED_ZIPS"
    WHERE CARRIER_ID = IN_CARRIER_ID AND ZIP_CODE = IN_ZIP_CODE;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END DELETE_BLOCKED_ZIP;

PROCEDURE INSERT_BLOCKED_ZIP (
    IN_CARRIER_ID         IN  CARRIER_EXCLUDED_ZIPS.CARRIER_ID%TYPE,
    IN_ZIP_CODE           IN  CARRIER_EXCLUDED_ZIPS.ZIP_CODE%TYPE,
    IN_USER               IN  CARRIER_EXCLUDED_ZIPS.UPDATED_BY%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts or updates record in the carrier_excluded_zips table for the passed in
        parameters.  Return 'Y' on success or 'N' on failure.

Input:
        carrier_id        varchar2
        zip_code          varchar2
        user              varchar2

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
BEGIN
  BEGIN
    INSERT INTO CARRIER_EXCLUDED_ZIPS (
      CARRIER_ID,
      ZIP_CODE,
      CREATED_ON,
      CARRIER_PROVIDED_FLAG,
      UPDATED_BY,
      UPDATED_ON)
     VALUES (
       IN_CARRIER_ID,
       IN_ZIP_CODE,
       SYSDATE,
       'N',
       IN_USER,
       SYSDATE
   );
   EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
     UPDATE VENUS.CARRIER_EXCLUDED_ZIPS
       SET
         CARRIER_EXCLUDED_ZIPS.UPDATED_BY = IN_USER,
         CARRIER_EXCLUDED_ZIPS.UPDATED_ON = SYSDATE
       WHERE
         CARRIER_EXCLUDED_ZIPS.CARRIER_ID = IN_CARRIER_ID AND
         CARRIER_EXCLUDED_ZIPS.ZIP_CODE = IN_ZIP_CODE;
   END;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END INSERT_BLOCKED_ZIP;

PROCEDURE MONITOR_SDS_REJECTS (
   IN_MAX_REJECTS     IN  NUMBER,
   IN_MINUTES_IN_PAST IN  NUMBER,
   OUT_STATUS         OUT VARCHAR2,
   OUT_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Query venus.venus table for the count of rejects received in a specified
        amount of time.
        Return 'Y' on success or 'N' on failure.

Input:
        max_rejects       number
        minutes_in_past   number

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
v_max_rejects number;
v_time_to_check number;
v_reject_count number;
BEGIN
  v_max_rejects := to_number(IN_MAX_REJECTS);
  v_time_to_check := to_number(IN_MINUTES_IN_PAST)/3600;

  SELECT COUNT(*) INTO v_reject_count
    FROM VENUS v
    WHERE
      v.SHIPPING_SYSTEM = 'SDS' AND
      v.MSG_TYPE = 'REJ' AND
      v.CREATED_ON > (SYSDATE - v_time_to_check);

  IF v_reject_count >= v_max_rejects THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := to_char(v_reject_count) || ' Argo rejects have occurred in the last ' || to_char(v_time_to_check*3600) || ' minutes.';
    END;
  ELSE
    BEGIN
      OUT_STATUS := 'Y';
      OUT_MESSAGE := 'Rejects are within operating limits.';
    END;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END MONITOR_SDS_REJECTS;

FUNCTION GET_VENDOR_ORDER_REJECT_COUNT (
  in_reference_number IN VENUS.REFERENCE_NUMBER%TYPE
)
RETURN NUMBER
--==============================================================================
--
-- Name:    GET_VENDOR_ORDER_REJECT_COUNT
-- Type:    Function
-- Syntax:  GET_VENDOR_ORDER_REJECT_COUNT (in_reference_number)
-- Returns: number of vendor initiated rejects that have occurred on an order
--
--==============================================================================
AS
v_count NUMBER;
BEGIN
  SELECT COUNT(*) 
    INTO v_count FROM VENUS.VENUS v 
    WHERE 
      v.MSG_TYPE = 'REJ' AND 
      v.SHIPPING_SYSTEM = 'SDS' AND 
      v.VENUS_STATUS != 'ERROR' AND 
      v.REFERENCE_NUMBER = in_reference_number;
      
  return v_count;
END GET_VENDOR_ORDER_REJECT_COUNT;

FUNCTION GET_SYSTEM_ORDER_REJECT_COUNT (
  in_reference_number IN VENUS.REFERENCE_NUMBER%TYPE
)
RETURN NUMBER
--==============================================================================
--
-- Name:    GET_SYSTEM_ORDER_REJECT_COUNT
-- Type:    Function
-- Syntax:  GET_SYSTEM_ORDER_REJECT_COUNT (in_reference_number)
-- Returns: number of system rejects that have occurred on an order
--
--==============================================================================
AS
v_count NUMBER;
BEGIN
  SELECT COUNT(*) 
    INTO v_count FROM VENUS.VENUS v 
    WHERE 
      v.MSG_TYPE = 'REJ' AND 
      v.SHIPPING_SYSTEM = 'SDS' AND 
      v.VENUS_STATUS = 'ERROR' AND 
      v.ERROR_DESCRIPTION != '-83173' AND
      v.REFERENCE_NUMBER = in_reference_number;
      
  return v_count;
END GET_SYSTEM_ORDER_REJECT_COUNT;

FUNCTION GET_CARRIER_ORDER_REJECT_COUNT (
  in_reference_number IN VENUS.REFERENCE_NUMBER%TYPE
)
RETURN NUMBER
--==============================================================================
--
-- Name:    GET_CARRIER_ORDER_REJECT_COUNT
-- Type:    Function
-- Syntax:  GET_CARRIER_ORDER_REJECT_COUNT (in_reference_number)
-- Returns: number of carrier based rejects that have occurred on an order
--
--==============================================================================
AS
v_count NUMBER;
BEGIN
  SELECT COUNT(*) 
    INTO v_count FROM VENUS.VENUS v 
    WHERE 
      v.MSG_TYPE = 'REJ' AND 
      v.SHIPPING_SYSTEM = 'SDS' AND 
      v.VENUS_STATUS = 'ERROR' AND 
      v.ERROR_DESCRIPTION = '-83173' AND
      v.REFERENCE_NUMBER = in_reference_number;
      
  return v_count;
END GET_CARRIER_ORDER_REJECT_COUNT;

PROCEDURE INSERT_SHIPMENT_OPTION
(
IN_SDS_VENUS_ORDER_NUMBER IN SHIPMENT_OPTION.SDS_VENUS_ORDER_NUMBER%TYPE,
IN_SDS_CARRIER_ID   IN SHIPMENT_OPTION.SDS_CARRIER_ID%TYPE,
IN_SDS_SHIP_METHOD_ID   IN SHIPMENT_OPTION.SDS_SHIP_METHOD_ID%TYPE,
IN_SDS_ZONE_CODE    IN SHIPMENT_OPTION.SDS_ZONE_CODE%TYPE,
IN_SDS_RATE_WEIGHT    IN SHIPMENT_OPTION.SDS_RATE_WEIGHT%TYPE,
IN_SDS_RATE_CHARGE    IN SHIPMENT_OPTION.SDS_RATE_CHARGE%TYPE,
IN_SDS_HANDLING_CHARGE    IN SHIPMENT_OPTION.SDS_HANDLING_CHARGE%TYPE,
IN_SDS_TOTAL_CHARGE   IN SHIPMENT_OPTION.SDS_TOTAL_CHARGE%TYPE,
IN_SDS_FREIGHT_CHARGE   IN SHIPMENT_OPTION.SDS_FREIGHT_CHARGE%TYPE,
IN_SDS_RATE_CALC_TYPE   IN SHIPMENT_OPTION.SDS_RATE_CALC_TYPE%TYPE,
IN_SDS_DATE_SHIPPED   IN SHIPMENT_OPTION.SDS_DATE_SHIPPED%TYPE,
IN_SDS_EXPECTED_DELIVERY_DATE IN SHIPMENT_OPTION.SDS_EXPECTED_DELIVERY_DATE%TYPE,
IN_SDS_DAYS_IN_TRANSIT    IN SHIPMENT_OPTION.SDS_DAYS_IN_TRANSIT%TYPE,
IN_SDS_DISTRIBUTION_CENTER  IN SHIPMENT_OPTION.SDS_DISTRIBUTION_CENTER%TYPE,
IN_SDS_SHIP_POINT_ID    IN SHIPMENT_OPTION.SDS_SHIP_POINT_ID%TYPE,
IN_SDS_ERROR_TXT    IN SHIPMENT_OPTION.SDS_ERROR_TXT%TYPE,
IN_ZONE_JUMP_FLAG   IN SHIPMENT_OPTION.ZONE_JUMP_FLAG%TYPE,
IN_IDEAL_RANK_NUMBER    IN SHIPMENT_OPTION.IDEAL_RANK_NUMBER%TYPE,
IN_ACTUAL_RANK_NUMBER   IN SHIPMENT_OPTION.ACTUAL_RANK_NUMBER%TYPE,
OUT_STATUS      OUT VARCHAR2,
OUT_MESSAGE     OUT VARCHAR2
)
/*-----------------------------------------------------------------------------
Description:
        Inserts a record into VENUS.SHIPMENT_OPTION table.

Input:
        sds_venus_order_number    varchar2
  sds_carrier_id      varchar2
  sds_ship_method_id    varchar2
  sds_zone_code     number
  sds_rate_weight     number
  sds_rate_charge     number
  sds_handling_charge   number
  sds_total_charge    number
  sds_freight_charge    number
  sds_rate_calc_type    varchar2
  sds_date_shipped    date
  sds_expected_delivery_date  date
  sds_days_in_transit   number
  sds_distribution_center   varchar2
  sds_ship_point_id   varchar2
  sds_error_txt     varchar2
  zone_jump_flag      varchar2,
  ideal_rank_number   number,
  actual_rank_number    number,

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
AS
BEGIN
  OUT_STATUS := 'Y';

  INSERT INTO VENUS.SHIPMENT_OPTION
  (
    SHIPMENT_OPTION_ID,
    SDS_VENUS_ORDER_NUMBER,
    SDS_CARRIER_ID,
    SDS_SHIP_METHOD_ID,
    SDS_ZONE_CODE,
    SDS_RATE_WEIGHT,
    SDS_RATE_CHARGE,
    SDS_HANDLING_CHARGE,
    SDS_TOTAL_CHARGE,
    SDS_FREIGHT_CHARGE,
    SDS_RATE_CALC_TYPE,
    SDS_DATE_SHIPPED,
    SDS_EXPECTED_DELIVERY_DATE,
    SDS_DAYS_IN_TRANSIT,
    SDS_DISTRIBUTION_CENTER,
    SDS_SHIP_POINT_ID,
    SDS_ERROR_TXT,
    ZONE_JUMP_FLAG,
    IDEAL_RANK_NUMBER,
    ACTUAL_RANK_NUMBER,
    CREATED_ON
  )
  VALUES (SHIPMENT_OPTION_SQ.NEXTVAL,
    IN_SDS_VENUS_ORDER_NUMBER,
    IN_SDS_CARRIER_ID,
    IN_SDS_SHIP_METHOD_ID,
    IN_SDS_ZONE_CODE,
    IN_SDS_RATE_WEIGHT,
    IN_SDS_RATE_CHARGE,
    IN_SDS_HANDLING_CHARGE,
    IN_SDS_TOTAL_CHARGE,
    IN_SDS_FREIGHT_CHARGE,
    IN_SDS_RATE_CALC_TYPE,
    IN_SDS_DATE_SHIPPED,
    IN_SDS_EXPECTED_DELIVERY_DATE,
    IN_SDS_DAYS_IN_TRANSIT,
    IN_SDS_DISTRIBUTION_CENTER,
    IN_SDS_SHIP_POINT_ID,
    IN_SDS_ERROR_TXT,
    IN_ZONE_JUMP_FLAG,
    IN_IDEAL_RANK_NUMBER,
    IN_ACTUAL_RANK_NUMBER,
    SYSDATE
  );
  
  EXCEPTION WHEN OTHERS THEN
      BEGIN
          OUT_STATUS := 'N';
          OUT_MESSAGE := 'ERROR OCCURRED in SHIP_PKG.INSERT_SHIPMENT_OPTION [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_SHIPMENT_OPTION;


--==============================================================================
--
-- Name:    GET_SHIPPING_VENDORS_ALL
-- Type:    Procedure
-- Syntax:  GET_SHIPPING_VENDORS_ALL (PRODUCT_SUBCODE_ID, DELIVERY_DATE, SHIP_DATE, OUT_CUR)
-- Returns: ref_cursor for
--          VENDOR_ID           VARCHAR2
--          MEMBER_NUMBER       VARCHAR2
--          PRODUCT_SUBCODE_ID  VARCHAR2
--          VENDOR_COST         NUMBER
--          VENDOR_SKU          VARCHAR2
--          AVAILABLE           CHAR(1)
--          REMOVED             CHAR(1)
--          SUBCODE_DESCRIPTION VARCHAR2
--
--
-- Description:   Checks for vendors to fulfill orders based on the passed parameters.
--
--==============================================================================
PROCEDURE GET_SHIPPING_VENDORS_ALL
(
IN_PRODUCT_SUBCODE_ID       IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
IN_DELIVERY_DATE            IN VENUS.DELIVERY_DATE%TYPE,
IN_SHIP_DATE                IN VENUS.SHIP_DATE%TYPE,
OUT_CUR                    OUT TYPES.REF_CURSOR
)
AS

v_temp_string        VARCHAR2(100);
v_backend_delay      NUMBER;

BEGIN

  -- get the back-end cutoff delay
  BEGIN
    select value 
    into   v_temp_string 
    from   frp.global_parms 
    where  context = 'SHIPPING_PARMS' 
    and    name = 'BACKEND_CUTOFF_DELAY';
    
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_temp_string := '30';
  END;
  v_backend_delay := TO_NUMBER(v_temp_string);


  OPEN OUT_CUR FOR
    SELECT vm.vendor_id,
           vm.member_number,
           vm.address1,
           vm.address2,
           vm.city,
           vm.state,
           vm.zip_code,
           vp.product_subcode_id,
           vp.vendor_cost,
           vp.vendor_sku,
           nvl(decode(vp.available, 'N', 'N', (select 'Y' from ftd_apps.inv_trk it
               where it.product_id = vp.product_subcode_id
               and it.vendor_id = vp.vendor_id
               and IN_SHIP_DATE between it.start_date and it.end_date
               and it.status = 'A')), 'N') as "available",
           vp.removed,
           ps.subcode_description
    FROM   FTD_APPS.VENDOR_MASTER vm, FTD_APPS.VENDOR_PRODUCT vp, FTD_APPS.PRODUCT_SUBCODES ps
    WHERE  vm.VENDOR_ID = vp.VENDOR_ID
    AND    vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID(+)
    AND    vp.PRODUCT_SUBCODE_ID = IN_PRODUCT_SUBCODE_ID 
    AND    vp.REMOVED = 'N'
    AND    NOT EXISTS 
    (
      SELECT 1 
      FROM   FTD_APPS.VENDOR_DELIV_BLOCK_LIST_VW dvw
      WHERE  dvw.VENDOR_ID = vm.VENDOR_ID 
      AND    dvw.START_DATE = IN_DELIVERY_DATE
    )
    AND    NOT EXISTS 
    (
      SELECT 1 
      FROM   FTD_APPS.VENDOR_SHIP_BLOCK_LIST_VW svw
      WHERE  svw.VENDOR_ID = vm.VENDOR_ID 
      AND    svw.START_DATE = IN_SHIP_DATE
    )
    AND  vm.VENDOR_ID in 
    (
      SELECT 
        CASE 
          WHEN  trunc(IN_SHIP_DATE) <= trunc(sysdate) THEN 
          (
            SELECT vm1.VENDOR_ID 
            FROM   FTD_APPS.VENDOR_MASTER vm1  
            WHERE  vm1.VENDOR_ID = vm.VENDOR_ID 
            AND (TRUNC(SYSDATE)+ (TO_NUMBER(SUBSTR(vm1.CUTOFF_TIME,1,2))*(1/24))+ (TO_NUMBER(SUBSTR (vm1.CUTOFF_TIME,4,2))*(1/1440)) + (v_backend_delay*(1/1440)) ) > sysdate
          )
          ELSE 
          (
            SELECT vm1.VENDOR_ID 
            FROM   FTD_APPS.VENDOR_MASTER vm1  
            WHERE  vm1.VENDOR_ID = vm.VENDOR_ID 
          )
        END
      from dual
    );
    

END GET_SHIPPING_VENDORS_ALL;

PROCEDURE GET_SDS_METHODS_BY_CARRIER
(
 IN_CARRIER_ID  IN  VARCHAR2,
 OUT_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------------------------
Description:
        This stored procedurereturns a cursor containing sds ship methods
        for carrier id passed in.

Input:
        carrier_id       			VARCHAR2
        
Output:
	cursor containing SDS ship methods
------------------------------------------------------------------------------------------------*/
BEGIN
  OPEN OUT_CUR FOR
    SELECT SDS_SHIP_METHOD
      FROM VENUS.SDS_SHIP_METHOD_XREF ssmx
     WHERE ssmx.ACTIVE='Y' AND ssmx.CARRIER_ID=IN_CARRIER_ID
     ORDER BY ssmx.SDS_SHIP_METHOD;
END GET_SDS_METHODS_BY_CARRIER;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a 'Y' or 'N'.  It will return
        'Y' if a ship method block exists for the given input parmaters, or an 'N' if 
        the ship method block does not exist

Input:
        IN_VENDOR_ID                FTD_APPS.VENDOR_PRODUCT_OVERRIDE.VENDOR_ID
        IN_PRODUCT_SUBCODE_ID       FTD_APPS.VENDOR_PRODUCT_OVERRIDE.PRODUCT_SUBCODE_ID
        IN_DELIVERY_OVERRIDE_DATE   FTD_APPS.VENDOR_PRODUCT_OVERRIDE.DELIVERY_OVERRIDE_DATE
        IN_CARRIER_ID               FTD_APPS.VENDOR_PRODUCT_OVERRIDE.CARRIER_ID
        IN_SDS_SHIP_METHOD_ID       FTD_APPS.VENDOR_PRODUCT_OVERRIDE.SDS_SHIP_METHOD_ID

Output:
        Y/N

-----------------------------------------------------------------------------*/
FUNCTION SHIP_METHOD_BLOCK_EXISTS
(
IN_VENDOR_ID                           IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.VENDOR_ID%TYPE,
IN_PRODUCT_SUBCODE_ID                  IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.PRODUCT_SUBCODE_ID%TYPE,
IN_DELIVERY_OVERRIDE_DATE              IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.DELIVERY_OVERRIDE_DATE%TYPE,
IN_CARRIER_ID                          IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.CARRIER_ID%TYPE,
IN_SDS_SHIP_METHOD                     IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.SDS_SHIP_METHOD%TYPE
)
RETURN VARCHAR2

AS

v_exists char(1) := 'N';

BEGIN

  SELECT  decode (count (*), 0, 'N', 'Y')
    INTO  v_exists
    FROM  FTD_APPS.VENDOR_PRODUCT_OVERRIDE
   WHERE  VENDOR_ID               = IN_VENDOR_ID
     AND  PRODUCT_SUBCODE_ID      = IN_PRODUCT_SUBCODE_ID
     AND  DELIVERY_OVERRIDE_DATE  = IN_DELIVERY_OVERRIDE_DATE
     AND  CARRIER_ID              = IN_CARRIER_ID
     AND  SDS_SHIP_METHOD         = IN_SDS_SHIP_METHOD
    ;

  RETURN v_exists;

END SHIP_METHOD_BLOCK_EXISTS;

PROCEDURE UPDATE_VENUS_ADD_ONS
(
 IN_VENUS_ID               IN VARCHAR2,
 IN_ADDON_ID               IN VARCHAR2,
 IN_VENDOR_SKU             IN VARCHAR2,
 IN_PER_ADDON_PRICE        IN NUMBER,
 IN_UPDATED_BY		   IN VARCHAR2,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record in table venus_add_ons.

Input:
        venus_id, add_on_id, per_addon_price, updated_by

Output:
        status, message

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE venus.venus_add_ons
    SET vendor_sku = in_vendor_sku,
        per_addon_price = in_per_addon_price,
        updated_by = in_updated_by,
        updated_on = SYSDATE
    WHERE venus_id = in_venus_id
    AND addon_id = in_addon_id;

  IF SQL%ROWCOUNT = 0 THEN
     out_message := 'NO RECORD FOUND/UPDATED';
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_VENUS_ADD_ONS;

PROCEDURE VENDOR_CAN_STILL_SHIP
(
in_product_subcode_id 		IN FTD_APPS.Vendor_Product.PRODUCT_SUBCODE_ID%TYPE,
in_secondary_ship_date  	IN VENUS.SHIP_DATE%TYPE,
in_vendor_id 			IN VENUS.VENDOR_ID%TYPE,
OUT_VENDOR_CAN_SHIP_FLAG        OUT varchar2
)
AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        vendor can still ship the order based off of the secondary ship date passed in.
        Y will be returned if the vendor can still ship the order, otherwise N will be returned.


Input:
        in_product_subcode_id       VARCHAR2
        in_secondary_ship_date	    DATE
        in_vendor_id		    VARCHAR2

Output:
        OUT_VENDOR_CAN_SHIP_FLAG    VARCHAR2

-----------------------------------------------------------------------------*/
v_count     number(4);
v_backend_delay      NUMBER;
v_temp_string        VARCHAR2(100);
BEGIN
     
    -- get the back-end cutoff delay
    BEGIN
      select value 
      into   v_temp_string 
      from   frp.global_parms 
      where  context = 'SHIPPING_PARMS' 
      and    name = 'BACKEND_CUTOFF_DELAY';
    
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_temp_string := '30';
    END;
    v_backend_delay := TO_NUMBER(v_temp_string);
    
    SELECT count(*)
      INTO v_count
    FROM FTD_APPS.VENDOR_MASTER vm
    JOIN FTD_APPS.VENDOR_PRODUCT vp ON vm.VENDOR_ID = vp.VENDOR_ID
    LEFT OUTER JOIN FTD_APPS.PRODUCT_SUBCODES ps
        ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID		
    WHERE vp.PRODUCT_SUBCODE_ID= IN_PRODUCT_SUBCODE_ID
      AND vp.VENDOR_ID = IN_VENDOR_ID
      AND vp.AVAILABLE='Y' 
      AND VP.VENDOR_ID IN (
              select distinct inTr.vendor_id from ftd_apps.inv_trk inTr where inTr.product_id = vp.PRODUCT_SUBCODE_ID 
              and in_secondary_ship_date BETWEEN  inTr.START_DATE and inTr.END_DATE  AND inTr.STATUS = 'A'
              )
    AND NOT EXISTS (
      SELECT 1 FROM FTD_APPS.VENDOR_SHIP_BLOCK_LIST_VW svw
      WHERE svw.VENDOR_ID = vm.VENDOR_ID AND svw.START_DATE = in_secondary_ship_date
    )
    AND NOT EXISTS (
      SELECT 1 FROM FTD_APPS.HOLIDAY_COUNTRY hc
      WHERE trunc(hc.holiday_date) = trunc(in_secondary_ship_date)
      and hc.country_id = 'US'
    )
    AND  vm.VENDOR_ID in 
    (
      SELECT 
        CASE 
          WHEN  trunc(in_secondary_ship_date) <= trunc(sysdate) THEN 
          (
            SELECT vm1.VENDOR_ID 
            FROM   FTD_APPS.VENDOR_MASTER vm1  
            WHERE  vm1.VENDOR_ID = vm.VENDOR_ID 
            AND (TRUNC(SYSDATE)+ (TO_NUMBER(SUBSTR(vm1.CUTOFF_TIME,1,2))*(1/24))+ (TO_NUMBER(SUBSTR (vm1.CUTOFF_TIME,4,2))*(1/1440)) + (v_backend_delay*(1/1440)) ) > sysdate
          )
          ELSE 
          (
            SELECT vm1.VENDOR_ID 
            FROM   FTD_APPS.VENDOR_MASTER vm1  
            WHERE  vm1.VENDOR_ID = vm.VENDOR_ID 
          )
        END
      from dual
    );
  
    
    IF v_count > 0 THEN
      OUT_VENDOR_CAN_SHIP_FLAG := 'Y';
    ELSE
      OUT_VENDOR_CAN_SHIP_FLAG := 'N';
    END IF;     

END VENDOR_CAN_STILL_SHIP;

/** DI-14/DSI - start **/

FUNCTION GET_VENDOR_ORDER_STATUS
(
  IN_ORDER_STATUS_ID		IN VENDOR_ORDER_STATUS.ORDER_STATUS_ID%TYPE
) RETURN TYPES.ref_cursor
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the vendor_order_status for
        the passed in id

Input:
        id number

Output:
        cursor containing record from vendor_order_status for the id

-----------------------------------------------------------------------------*/
OUT_CUR TYPES.ref_cursor;

BEGIN
OPEN OUT_CUR FOR
	SELECT
		"ORDER_STATUS_ID",
		"VENUS_ORDER_NUMBER",
		"PARTNER_ORDER_NUMBER",
		"PARTNER_ID",
		"TRACKING_NUMBER",
		"CARRIER",
		"ORDER_STATUS",
		"PRINTED",
		"SHIPPED",
		"DELIVERED", 
		"REJECTED", 
		"REJECT_CODE",
		"REJECT_MESSAGE",
		"STATUS"
		FROM "VENDOR_ORDER_STATUS" WHERE "ORDER_STATUS_ID" = IN_ORDER_STATUS_ID and "STATUS" = 'NEW';

  return OUT_CUR;
END GET_VENDOR_ORDER_STATUS;

PROCEDURE UPDATE_VENDOR_ORDER_STATUS
(
  IN_ORDER_STATUS_ID		IN VENDOR_ORDER_STATUS.ORDER_STATUS_ID%TYPE,
  OUT_STATUS  				OUT VARCHAR2,
  OUT_MESSAGE 				OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the  status of the record as processed 
		VENDOR_ORDER_STATUS table.

Input:
        in_order_status_id		Order status id 
 
Output:
		out_status  				Output Status
		out_message 				Output message (if error)

-----------------------------------------------------------------------------*/


BEGIN
  
	UPDATE "VENDOR_ORDER_STATUS"
    SET "STATUS" = 'PROCESSED',
         "UPDATED_ON" = SYSDATE
    WHERE "ORDER_STATUS_ID" = IN_ORDER_STATUS_ID;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'Error occured in UPDATE_VENDOR_ORDER_STATUS procedure. Error details: [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END UPDATE_VENDOR_ORDER_STATUS;

PROCEDURE UPDATE_TRACKING_DETAILS
(
 IN_VENUS_ORDER_NUMBER		IN VARCHAR2,
 IN_TRACKING_NUMBER			IN VARCHAR2,
 IN_TRACKING_COMMENT		IN VARCHAR2,
 IN_CARRIER					IN VARCHAR2,
 OUT_STATUS  				OUT VARCHAR2,
 OUT_MESSAGE 				OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the tracking number to FTD record, 
		and tracking comments to the first ANS message in VENUS table.

Input:
        IN_VENUS_ORDER_NUMBER		Venus Order Number
		IN_TRACKING_NUMBER			Tracking Number
		IN_TRACKING_COMMENT			Tracking Comments
		IN_CARRIER					Carrier (FEDEX/UPS/USPS/DHL)
 
Output:
		out_status  				Output Status
		out_message 				Output message (if error)

-----------------------------------------------------------------------------*/

	numFTDRecords NUMBER;
	numANSRecords NUMBER;	

BEGIN
  
	SELECT count(*) INTO numFTDRecords FROM venus WHERE venus_order_number = IN_VENUS_ORDER_NUMBER AND msg_type = 'FTD';
	
	IF (numFTDRecords > 0) THEN
		
		IF ( IN_CARRIER IS NOT NULL AND UPPER(IN_CARRIER) != 'UNKNOWN' ) THEN
		
			UPDATE venus 
				SET tracking_number = IN_TRACKING_NUMBER, final_carrier = IN_CARRIER  
				WHERE venus_order_number = IN_VENUS_ORDER_NUMBER AND msg_type = 'FTD';
				
		END IF;
			
		OUT_STATUS := 'Y';
		OUT_MESSAGE := null;
	ELSE
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'Error occured in UPDATE_TRACKING_DETAILS procedure. No FTD record found/updated for venus order number  ' || IN_VENUS_ORDER_NUMBER || '.';
	END IF;
	
	IF (out_status = 'Y') THEN
	
		SELECT COUNT(*) INTO numANSRecords FROM venus WHERE venus_order_number = IN_VENUS_ORDER_NUMBER AND msg_type = 'ANS';
		
		IF (numANSRecords > 0) THEN
		
			IF ( IN_CARRIER IS NOT NULL AND UPPER(IN_CARRIER) != 'UNKNOWN' ) THEN	-- Update comments only when the passed carrier argument has a valid value
			
				UPDATE venus v SET v.message_text = IN_TRACKING_COMMENT, v.comments = IN_TRACKING_COMMENT WHERE v.venus_order_number = IN_VENUS_ORDER_NUMBER AND v.msg_type = 'ANS'
				AND v.created_on = (SELECT MIN(v2.created_on) FROM venus v2 WHERE v2.venus_order_number = IN_VENUS_ORDER_NUMBER AND v2.msg_type = 'ANS' );
				
			END IF;
			
			OUT_STATUS := 'Y';
			OUT_MESSAGE := null;
		
		ELSE
			OUT_STATUS := 'N';
			OUT_MESSAGE := 'Error occured in UPDATE_TRACKING_DETAILS procedure. No ANS records found/updated for venus order number  ' || IN_VENUS_ORDER_NUMBER || '.';
		END IF;
	
	END IF;
	
	EXCEPTION WHEN OTHERS THEN
	BEGIN
		ROLLBACK;
		out_status := 'N';
		OUT_MESSAGE := 'Error occured in UPDATE_TRACKING_DETAILS procedure. Error Details: [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;

END UPDATE_TRACKING_DETAILS;

PROCEDURE UPDATE_VENUS_DELIVERY_SCAN
(
 IN_VENUS_ORDER_NUMBER		IN VARCHAR2,
 IN_DELIVERY_DATE			in date,
 OUT_STATUS  				OUT VARCHAR2,
 OUT_MESSAGE 				OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the FTD record's DELIVERY_SCAN field 
		in VENUS table with the provided in_delivery_date.

Input:
        IN_VENUS_ORDER_NUMBER		Venus Order Number
		IN_DELIVERY_DATE			Actual Delivery date of the order
 
Output:
		OUT_STATUS  				Output Status
		OUT_MESSAGE 				Output message (if error)

-----------------------------------------------------------------------------*/

BEGIN

	UPDATE venus SET delivery_scan = IN_DELIVERY_DATE WHERE venus_order_number = IN_VENUS_ORDER_NUMBER AND msg_type = 'FTD';
			
	OUT_STATUS := 'Y';
	OUT_MESSAGE := null;

	EXCEPTION WHEN OTHERS THEN
	BEGIN
		ROLLBACK;
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'Error occured in UPDATE_VENUS_DELIVERY_SCAN procedure. Error Details: [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;


END UPDATE_VENUS_DELIVERY_SCAN;

PROCEDURE GET_WEST_MISSING_STATUS
(
    OUT_CUR			OUT TYPES.REF_CURSOR
)
--==============================================================================
--
-- Name:    GET_WEST_MISSING_STATUS
-- Type:    Procedure
-- Syntax:  GET_WEST_MISSING_STATUS
-- Returns: ref_cursor for
--          FTDWEST_ORDER_ID
--
-- Description:  Returns FTD west's order ids for orders have missing
--               status.  
--
-- Output:
--        ref_cursor containing the venus.external_order_number
--
-- Author: vbejawar
--
--==============================================================================
AS
BEGIN  
  OPEN OUT_CUR FOR
    SELECT v.VENUS_ID as VENUS_ID, v.FTDWEST_ORDER_ID AS FTDWEST_ORDER_ID
      FROM VENUS v
      WHERE
        TRUNC(v.DELIVERY_DATE) = TRUNC(SYSDATE)
        AND v.SDS_STATUS IN ('NEW','AVAILABLE','PRINTED')
        AND v.SHIPPING_SYSTEM = 'FTD WEST'
        AND v.MSG_TYPE = 'FTD'
        AND v.VENUS_STATUS = 'VERIFIED';

  
END GET_WEST_MISSING_STATUS;

PROCEDURE INSERT_VENDOR_ORDER_STATUS
(
  
	IN_VENUS_ORDER_NUMBER VENDOR_ORDER_STATUS.VENUS_ORDER_NUMBER%TYPE,
	IN_PARTNER_ORDER_NUMBER VENDOR_ORDER_STATUS.PARTNER_ORDER_NUMBER%TYPE,
	IN_PARTNER_ID VENDOR_ORDER_STATUS.PARTNER_ID%TYPE,
	IN_TRACKING_NUMBER VENDOR_ORDER_STATUS.TRACKING_NUMBER%TYPE,
	IN_CARRIER VENDOR_ORDER_STATUS.CARRIER%TYPE,
	IN_ORDER_STATUS VENDOR_ORDER_STATUS.ORDER_STATUS%TYPE,
	IN_PRINTED VENDOR_ORDER_STATUS.PRINTED%TYPE,
	IN_SHIPPED VENDOR_ORDER_STATUS.SHIPPED%TYPE,
	IN_DELIVERED VENDOR_ORDER_STATUS.DELIVERED%TYPE,
	IN_REJECTED VENDOR_ORDER_STATUS.REJECTED%TYPE,
	IN_REJECT_CODE VENDOR_ORDER_STATUS.REJECT_CODE%TYPE,
	IN_REJECT_MESSAGE VENDOR_ORDER_STATUS.REJECT_MESSAGE%TYPE,
	
	OUT_STATUS  				OUT VARCHAR2,
	OUT_MESSAGE 				OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a new record with provided input values into
		VENDOR_ORDER_STATUS table.

Input:
        IN_VENUS_ORDER_NUMBER			Venus Order Number
		IN_PARTNER_ORDER_NUMBER			Partner Order Number
		IN_PARTNER_ID					Partner Id
		IN_TRACKING_NUMBER				Tracking Number
		IN_CARRIER						Carrier Name
		IN_ORDER_STATUS					Order Status (PRINTED/SHIPPED/DELIVERED/REJECTED)
		IN_PRINTED						Printed Date
		IN_SHIPPED						Shipped Date
		IN_DELIVERED					Delivered Date
		IN_REJECTED						Rejected Date
		IN_REJECT_CODE					Reject Code
		IN_REJECT_MESSAGE				Reject Reason
 
Output:
		out_status  				Output Status
		out_message 				Output message (if error)

-----------------------------------------------------------------------------*/
	
	v_seq_number NUMBER;


BEGIN

	SELECT vendor_order_status_id_sq.NEXTVAL
    INTO v_seq_number
    FROM DUAL;

	INSERT INTO "VENDOR_ORDER_STATUS" 
		(
			ORDER_STATUS_ID, VENUS_ORDER_NUMBER, PARTNER_ORDER_NUMBER, PARTNER_ID, TRACKING_NUMBER, CARRIER, 
			ORDER_STATUS, PRINTED, SHIPPED, DELIVERED, REJECTED, REJECT_CODE, REJECT_MESSAGE, STATUS, 
			CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY 
		) 
	VALUES 
		(
			v_seq_number, IN_VENUS_ORDER_NUMBER, IN_PARTNER_ORDER_NUMBER, IN_PARTNER_ID, IN_TRACKING_NUMBER, IN_CARRIER, 
			IN_ORDER_STATUS, IN_PRINTED, IN_SHIPPED, IN_DELIVERED, IN_REJECTED, IN_REJECT_CODE, IN_REJECT_MESSAGE, 'NEW', 
			SYSDATE, 'FTD WEST', SYSDATE, 'FTD WEST'
		);

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'Error occured in INSERT_VENDOR_ORDER_STATUS procedure. Error details: [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
	
END INSERT_VENDOR_ORDER_STATUS;

/** DI-14/DSI - end **/


/*-----------------------------------------------------------------------------------------------
Description:
        This stored procedurereturns a cursor containing Custome Shipping Carrier
        for Source code passed in.

Input:
        Source Code       			VARCHAR2

Output:
	cursor containing Custome Shipping Carrier
------------------------------------------------------------------------------------------------*/
PROCEDURE GET_CUST_SHIPPING_CARRIER
(
	In_Source_Code In Ftd_Apps.Source_Master.Source_Code%Type,
  OUT_CUST_SHIPPING_CARRIER  Out varchar2
)
As 
BEGIN 
     SELECT Custom_Shipping_Carrier INTO OUT_CUST_SHIPPING_CARRIER
     From Ftd_Apps.Source_Master
     Where Source_Code = In_Source_Code;
     
END GET_CUST_SHIPPING_CARRIER;


END SHIP_PKG;
.
/
grant execute on venus.ship_pkg to ftd_apps;
