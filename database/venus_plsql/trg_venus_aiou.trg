CREATE OR REPLACE
TRIGGER venus.trg_venus_aiou
AFTER INSERT OR UPDATE OF VENUS_STATUS, EXTERNAL_SYSTEM_STATUS
ON venus.venus
FOR EACH ROW

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***
***          Modified By: Steve Yeazel
***          Created On : 07/28/2006
***              Reason : Enhancement895: Send venus_id to queue on selected Inserts
***
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE

   lchr_current_status   frp.order_state.status%TYPE := NULL;
   lchr_new_status   frp.order_state.status%TYPE := NULL;
   lchr_trigger_name   frp.order_state_history.trigger_name%TYPE := 'TRG_VENUS_AIOU';
   lnum_order_detail_id   frp.order_state.order_detail_id%TYPE := TO_NUMBER(:NEW.reference_number);

   enqueue_options      dbms_aq.enqueue_options_t;
   message_properties   dbms_aq.message_properties_t;
   message_handle       raw(2000);
   message              sys.aq$_jms_text_message;
   out_status           varchar2(1000);
   out_message          varchar2(1000);
   ship_queue           varchar2(100) := 'OJMS.SHIP_PROCESSING';

BEGIN

   -- fetch the current status
   lchr_current_status := frp.fun_get_current_status(lnum_order_detail_id);

   -- fetch the order_detail_id if null
   IF :NEW.reference_number IS NULL THEN
      lnum_order_detail_id := TO_NUMBER(venus_get_order_detail_id(:NEW.venus_order_number));
   END IF;

   IF INSERTING THEN

      -- initialize the new status
      IF (:NEW.msg_type = 'FTD' AND :NEW.venus_status = 'SHIP') THEN
         lchr_new_status := 'FTD_CREATED';
      ELSIF (:NEW.msg_type = 'FTD' AND :NEW.venus_status = 'OPEN'
         AND :NEW.filling_vendor IN ('90-9086WI', '90-9086SM')) THEN
         lchr_new_status := 'FTD_CREATED_FTP';
      END IF;

      -- populate the order_state tables
      IF lchr_new_status IS NOT NULL THEN

	 -- insert or update into frp.order_state table
	 frp.prc_pop_order_state (
	    lnum_order_detail_id,
	    lchr_new_status);

	 -- insert into frp.order_state_history table
	 frp.prc_pop_order_state_history (
	    lnum_order_detail_id,
	    lchr_new_status,
	    lchr_current_status,
	    lchr_trigger_name);
      END IF;


      IF ((:NEW.msg_type = 'CAN'        AND 
           :NEW.shipping_system IN ('SDS','FTD WEST') AND
           :NEW.venus_status = 'OPEN') OR
          (:NEW.msg_type = 'REJ'        AND 
           :NEW.shipping_system IN ('SDS','FTD WEST') AND
           :NEW.venus_status = 'OPEN') ) 
          THEN

         --Create a new JMS Message


--         message := sys.aq$_jms_text_message.construct();
--         message.set_text(payload => :NEW.venus_id );
--         message_properties.CORRELATION := :NEW.venus_id;

--         message_properties.exception_queue := 'OJMS.OSP_APP_EXCEPTION';

--         dbms_aq.enqueue (queue_name => 'OJMS.SHIP_PROCESSING',
--                        enqueue_options => enqueue_options,
--                        message_properties => message_properties,
--                        payload => message,
--                        msgid => message_handle);
         --check which ship processing queue the message should be sent to
         IF :NEW.new_shipping_system = 'Y' THEN
         	ship_queue := 'OJMS.SHIP_SHIP_PROCESSING';
         END IF;
         
         events.POST_A_MESSAGE_FLEX(ship_queue,:NEW.venus_id,:NEW.venus_id,0,OUT_STATUS,OUT_MESSAGE);
         
      END IF;

   END IF;

   IF UPDATING THEN

      -- initialize the new status
      IF (:NEW.msg_type = 'FTD' AND :NEW.venus_status = 'OPEN'
         AND (:NEW.venus_status <> :OLD.venus_status)) THEN
	 lchr_new_status := 'CARRIER_SELECTED';
      ELSIF (:NEW.venus_status = 'VERIFIED'
         AND (:NEW.venus_status <> :OLD.venus_status)) THEN
	 lchr_new_status := 'FTD_SENT';
      END IF;

      IF (:NEW.external_system_status = 'VERIFIED'
         AND (:NEW.external_system_status <> :OLD.external_system_status)) THEN
	 lchr_new_status := 'FTD_VERIFIED';
      END IF;

      -- Populate the order_state tables
      IF lchr_new_status IS NOT NULL THEN

         -- insert or update into frp.order_state table
         frp.prc_pop_order_state (
  	    lnum_order_detail_id,
	    lchr_new_status);

	 -- insert into frp.order_state_history table
	 frp.prc_pop_order_state_history (
	    lnum_order_detail_id,
	    lchr_new_status,
	    lchr_current_status,
	    lchr_trigger_name);

      END IF;

   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In VENUS.TRG_VENUS_AIOU: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_venus_aiou;
.
/
