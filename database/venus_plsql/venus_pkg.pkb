CREATE OR REPLACE PACKAGE BODY VENUS.VENUS_PKG
AS

PROCEDURE INSERT_VENUS_MESSAGE
(
 IN_VENUS_ORDER_NUMBER     IN VARCHAR2,
 IN_VENUS_STATUS           IN VARCHAR2,
 IN_MSG_TYPE               IN VARCHAR2,
 IN_SENDING_VENDOR         IN VARCHAR2,
 IN_FILLING_VENDOR         IN VARCHAR2,
 IN_ORDER_DATE             IN DATE,
 IN_RECIPIENT              IN VARCHAR2,
 IN_BUSINESS_NAME          IN VARCHAR2,
 IN_ADDRESS_1              IN VARCHAR2,
 IN_ADDRESS_2              IN VARCHAR2,
 IN_CITY                   IN VARCHAR2,
 IN_STATE                  IN VARCHAR2,
 IN_ZIP                    IN VARCHAR2,
 IN_PHONE_NUMBER           IN VARCHAR2,
 IN_DELIVERY_DATE          IN DATE,
 IN_FIRST_CHOICE           IN VARCHAR2,
 IN_PRICE                  IN NUMBER,
 IN_CARD_MESSAGE           IN VARCHAR2,
 IN_SHIP_DATE              IN DATE,
 IN_OPERATOR               IN VARCHAR2,
 IN_COMMENTS               IN VARCHAR2,
 IN_TRANSMISSION_TIME      IN DATE,
 IN_REFERENCE_NUMBER       IN VARCHAR2,
 IN_PRODUCT_ID             IN VARCHAR2,
 IN_COMBINED_REPORT_NUMBER IN VARCHAR2,
 IN_ROF_NUMBER             IN VARCHAR2,
 IN_ADJ_REASON_CODE        IN VARCHAR2,
 IN_OVER_UNDER_CHARGE      IN NUMBER,
 IN_PROCESSED_INDICATOR    IN CHAR,
 IN_MESSAGE_TEXT           IN VARCHAR2,
 IN_SEND_TO_VENUS          IN CHAR,
 IN_ORDER_PRINTED          IN CHAR,
 IN_SUB_TYPE               IN CHAR,
 IN_ERROR_DESCRIPTION      IN VARCHAR2,
 IN_COUNTRY                IN VARCHAR2,
 IN_CANCEL_REASON_CODE     IN VARCHAR2,
 IN_COMP_ORDER             IN CHAR,
 IN_OLD_PRICE              IN NUMBER,
 IN_SHIP_METHOD            IN VARCHAR2,
 IN_VENDOR_SKU             IN VARCHAR2,
 IN_PRODUCT_DESCRIPTION    IN VARCHAR2,
 IN_PRODUCT_WEIGHT         IN VARCHAR2,
 IN_MESSAGE_DIRECTION      IN VARCHAR2,
 IN_EXTERNAL_SYSTEM_STATUS IN VARCHAR2,
 IN_SHIPPING_SYSTEM        IN VARCHAR2,
 IN_TRACKING_NUMBER        IN VARCHAR2,
 IN_PRINTED                IN DATE,
 IN_SHIPPED                IN DATE,
 IN_CANCELLED              IN DATE,
 IN_FINAL_CARRIER          IN VARCHAR2,
 IN_FINAL_SHIP_METHOD      IN VARCHAR2,
 IN_REJECTED               IN DATE,
 IN_VENDOR_ID              IN VARCHAR2,
 IN_PERSONAL_GREETING_ID   IN VARCHAR2,
 IN_ORIG_COMPLAINT_COMM_TYPE_ID IN VARCHAR2,
 IN_NOTI_COMPLAINT_COMM_TYPE_ID IN VARCHAR2,
 IN_NEW_SHIPPING_SYSTEM    IN VARCHAR2,
 OUT_VENUS_ID             OUT NUMBER,
 OUT_STATUS OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table venus.

Input:
        venus_order_number, venus_status, msg_type,
        sending_vendor, filling_vendor, order_date, recipient,
        business_name, address_1, address_2, city, state, zip, phone_number,
        delivery_date, first_choice, price, card_message, ship_date,
        operator, comments, transmission_time, reference_number, product_id,
        combined_report_number, rof_number, adj_reason_code,
        over_under_charge, processed_indicator, message_text, send_to_venus,
        order_printed, sub_type, country, cancel_reason_code, comp_order,
        old_price, ship_method, vendor_sku, product_description, product_weight,
        message_direction, external_system_status, shipping_system, tracking_number,
        printed, shipped, cancelled, final_carrier, final_ship_method, origin_complaint_comm_type_id,
        notif_complaint_comm_type_id, new_shipping_system

Output:
        venus_id
        status
        error message

-----------------------------------------------------------------------------*/

CURSOR ADDON_CURSOR IS
   SELECT CO.ADD_ON_CODE,
      CO.ADD_ON_QUANTITY,
      NVL(PM.DIM_WEIGHT, 0) dim_weight,
      CO.ADD_ON_HISTORY_ID
   FROM CLEAN.ORDER_ADD_ONS CO
   LEFT OUTER JOIN FTD_APPS.ADDON A
      ON A.ADDON_ID = CO.ADD_ON_CODE
   LEFT OUTER JOIN FTD_APPS.PRODUCT_MASTER PM
      ON PM.PRODUCT_ID = A.PRODUCT_ID
   WHERE CO.ORDER_DETAIL_ID = TO_NUMBER(IN_REFERENCE_NUMBER);

   v_shipping_system				venus.shipping_system%TYPE;

BEGIN

  SELECT venus_id.NEXTVAL
    INTO out_venus_id
    FROM DUAL;

  IF in_shipping_system IS NULL
     AND in_msg_type <> 'FTD' THEN
     v_shipping_system := get_vendor_shipping_system(in_venus_order_number);
  ELSE
     v_shipping_system := in_shipping_system;
  END IF;

  INSERT INTO venus
   (venus_id,
    venus_order_number,
    venus_status,
    msg_type,
    sending_vendor,
    filling_vendor,
    order_date,
    recipient,
    business_name,
    address_1,
    address_2,
    city,
    state,
    zip,
    phone_number,
    delivery_date,
    first_choice,
    price,
    card_message,
    ship_date,
    operator,
    comments,
    transmission_time,
    reference_number,
    product_id,
    combined_report_number,
    rof_number,
    adj_reason_code,
    over_under_charge,
    processed_indicator,
    message_text,
    send_to_venus,
    order_printed,
    sub_type,
    error_description,
    country,
    cancel_reason_code,
    comp_order,
    old_price,
    ship_method,
    created_on,
    updated_on,
    vendor_sku,
    product_description,
    product_weight,
    message_direction,
    external_system_status,
    vendor_id,
    shipping_system,
    tracking_number,
    printed,
    shipped,
    cancelled,
    final_carrier,
    final_ship_method,
    rejected,
    personal_greeting_id,
    origin_complaint_comm_type_id,
    notif_complaint_comm_type_id,
    new_shipping_system
    )
   VALUES
    (out_venus_id,
     in_venus_order_number,
     in_venus_status,
     in_msg_type,
     in_sending_vendor,
     in_filling_vendor,
     in_order_date,
     regexp_replace(in_recipient,'[^A-Z^a-z^0-9( )]'),
     regexp_replace(in_business_name,'[^A-Z^a-z^0-9( )]'),
     regexp_replace(in_address_1,'[^A-Z^a-z^0-9( )]'),
     regexp_replace(in_address_2,'[^A-Z^a-z^0-9( )]'),
     regexp_replace(in_city,'[^A-Z^a-z^0-9( )]'),
     in_state,
     in_zip,
     replace (translate (nvl (in_phone_number, '9999999999'), '/()', '---' ), '-', ''),
     in_delivery_date,
     in_first_choice,
     in_price,
     in_card_message,
     in_ship_date,
     in_operator,
     in_comments,
     in_transmission_time,
     in_reference_number,
     in_product_id,
     in_combined_report_number,
     in_rof_number,
     in_adj_reason_code,
     in_over_under_charge,
     in_processed_indicator,
     in_message_text,
     in_send_to_venus,
     in_order_printed,
     in_sub_type,
     in_error_description,
     in_country,
     in_cancel_reason_code,
     in_comp_order,
     in_old_price,
     in_ship_method,
     SYSTIMESTAMP,
     SYSDATE,
     in_vendor_sku,
     in_product_description,
     in_product_weight,
     in_message_direction,
     in_external_system_status,
     in_vendor_id,
     v_shipping_system,
     in_tracking_number,
     in_printed,
     in_shipped,
     in_cancelled,
     in_final_carrier,
     in_final_ship_method,
     in_rejected,
     in_personal_greeting_id,
     in_orig_complaint_comm_type_id,
     in_noti_complaint_comm_type_id,
     in_new_shipping_system
    );

  out_status := 'Y';
  
  IF in_msg_type = 'CAN'
     AND v_shipping_system IN ('SDS','FTD WEST') 
     AND in_venus_status = 'OPEN' THEN
     BEGIN
        UPDATE venus.venus
           SET cancelled = SYSDATE
         WHERE venus_order_number = in_venus_order_number
           AND msg_type = 'FTD';
        
        out_status := 'Y';
     EXCEPTION
        WHEN OTHERS THEN
           out_status := 'Y';
           out_error_message := 'ERROR OCCURRED IN VENUS.VENUS_PKG.INSERT_VENUS_MESSAGE-UPDATE CANCELLED [' || SQLCODE || '] ' || SUBSTR(SQLERRM,1,256);
     END;
  END IF;

  IF in_msg_type = 'REJ'
     AND v_shipping_system IN ('SDS','FTD WEST') THEN
     BEGIN
        UPDATE venus.venus
           SET rejected = SYSDATE
         WHERE venus_order_number = in_venus_order_number
           AND msg_type = 'FTD';
        
        out_status := 'Y';
     EXCEPTION
        WHEN OTHERS THEN
           out_status := 'Y';
           out_error_message := 'ERROR OCCURRED IN VENUS.VENUS_PKG.INSERT_VENUS_MESSAGE-UPDATE REJECTED [' || SQLCODE || '] ' || SUBSTR(SQLERRM,1,256);
     END;
  END IF;
  
  IF in_msg_type = 'FTD' THEN
     BEGIN
        FOR REC IN ADDON_CURSOR LOOP
           INSERT INTO VENUS_ADD_ONS (
              VENUS_ID,
              ADDON_ID,
              ADD_ON_HISTORY_ID,
              ADDON_QTY,
              PER_ADDON_PRICE,
              PER_ADDON_WEIGHT,
              CREATED_ON,
              CREATED_BY,
              UPDATED_ON,
              UPDATED_BY)
           VALUES (
              OUT_VENUS_ID,
              REC.ADD_ON_CODE,
              REC.ADD_ON_HISTORY_ID,
              REC.ADD_ON_QUANTITY,
              0,
              REC.DIM_WEIGHT,
              sysdate,
              'ORDER_PROCESSING',
              sysdate,
              'ORDER_PROCESSING');
        END LOOP;
        
        out_status := 'Y';
     EXCEPTION
        WHEN OTHERS THEN
           out_status := 'N';
           out_error_message := 'ERROR OCCURRED IN VENUS.VENUS_PKG.INSERT_VENUS_MESSAGE-UPDATE CANCELLED [' || SQLCODE || '] ' || SUBSTR(SQLERRM,1,256);
     END;
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_venus_id := NULL;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_VENUS_MESSAGE;


PROCEDURE UPDATE_VENUS
(
 IN_VENUS_ID               IN VARCHAR2,
 IN_VENUS_ORDER_NUMBER     IN VARCHAR2,
 IN_VENUS_STATUS           IN VARCHAR2,
 IN_MSG_TYPE               IN VARCHAR2,
 IN_SENDING_VENDOR         IN VARCHAR2,
 IN_FILLING_VENDOR         IN VARCHAR2,
 IN_ORDER_DATE             IN DATE,
 IN_RECIPIENT              IN VARCHAR2,
 IN_BUSINESS_NAME          IN VARCHAR2,
 IN_ADDRESS_1              IN VARCHAR2,
 IN_ADDRESS_2              IN VARCHAR2,
 IN_CITY                   IN VARCHAR2,
 IN_STATE                  IN VARCHAR2,
 IN_ZIP                    IN VARCHAR2,
 IN_PHONE_NUMBER           IN VARCHAR2,
 IN_DELIVERY_DATE          IN DATE,
 IN_FIRST_CHOICE           IN VARCHAR2,
 IN_PRICE                  IN NUMBER,
 IN_CARD_MESSAGE           IN VARCHAR2,
 IN_SHIP_DATE              IN DATE,
 IN_OPERATOR               IN VARCHAR2,
 IN_COMMENTS               IN VARCHAR2,
 IN_TRANSMISSION_TIME      IN DATE,
 IN_REFERENCE_NUMBER       IN VARCHAR2,
 IN_PRODUCT_ID             IN VARCHAR2,
 IN_COMBINED_REPORT_NUMBER IN VARCHAR2,
 IN_ROF_NUMBER             IN VARCHAR2,
 IN_ADJ_REASON_CODE        IN VARCHAR2,
 IN_OVER_UNDER_CHARGE      IN NUMBER,
 IN_PROCESSED_INDICATOR    IN VARCHAR2,
 IN_MESSAGE_TEXT           IN VARCHAR2,
 IN_SEND_TO_VENUS          IN CHAR,
 IN_ORDER_PRINTED          IN CHAR,
 IN_SUB_TYPE               IN CHAR,
 IN_ERROR_DESCRIPTION      IN VARCHAR2,
 IN_COUNTRY                IN VARCHAR2,
 IN_CANCEL_REASON_CODE     IN VARCHAR2,
 IN_COMP_ORDER             IN CHAR,
 IN_OLD_PRICE              IN NUMBER,
 IN_SHIP_METHOD            IN VARCHAR2,
 IN_VENDOR_SKU             IN VARCHAR2,
 IN_PRODUCT_DESCRIPTION    IN VARCHAR2,
 IN_PRODUCT_WEIGHT         IN VARCHAR2,
 IN_SHIPPING_SYSTEM        IN VARCHAR2,
 IN_TRACKING_NUMBER        IN VARCHAR2,
 IN_PRINTED                IN DATE,
 IN_SHIPPED                IN DATE,
 IN_CANCELLED              IN DATE,
 IN_FINAL_CARRIER          IN VARCHAR2,
 IN_FINAL_SHIP_METHOD      IN VARCHAR2,
 IN_PERSONAL_GREETING_ID   IN VARCHAR2,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record in table venus.

Input:
        venus_id, venus_order_number, venus_status, msg_type,
        sending_vendor, filling_vendor, order_date, recipient,
        business_name, address_1, address_2, city, state, zip, phone_number,
        delivery_date, first_choice, price number, card_message, ship_date,
        operator, comments, transmission_time, reference_number, product_id,
        combined_report_number, rof_number, adj_reason_code,
        over_under_charge, processed_indicator, message_text, send_to_venus,
        order_printed, sub_type, error_description, country, cancel_reason_code,
        comp_order, old_price, ship_method, vendor_sku, product_description,
        product_weight

Output:
        status, message

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE venus
    SET venus_order_number = in_venus_order_number,
        venus_status       = in_venus_status,
        msg_type           = in_msg_type,
        sending_vendor  = in_sending_vendor,
        filling_vendor  = in_filling_vendor,
        order_date      = in_order_date,
        recipient       = regexp_replace(in_recipient,'[^A-Z^a-z^0-9( )]'),
        business_name   = regexp_replace(in_business_name,'[^A-Z^a-z^0-9( )]'),
        address_1       = regexp_replace(in_address_1,'[^A-Z^a-z^0-9( )]'),
        address_2       = regexp_replace(in_address_2,'[^A-Z^a-z^0-9( )]'),
        city            = regexp_replace(in_city,'[^A-Z^a-z^0-9( )]'),
        state           = in_state,
        zip             = in_zip,
        phone_number    = replace (translate (nvl (in_phone_number, '9999999999'), '/()', '---' ), '-', ''),
        delivery_date   = in_delivery_date,
        first_choice    = in_first_choice,
        price   = in_price,
        card_message    = in_card_message,
        ship_date       = in_ship_date,
        operator        = in_operator,
        comments        = in_comments,
        transmission_time       = in_transmission_time,
        reference_number        = in_reference_number,
        product_id              = in_product_id,
        combined_report_number  = in_combined_report_number,
        rof_number              = in_rof_number,
        adj_reason_code         = in_adj_reason_code,
        over_under_charge       = in_over_under_charge,
        processed_indicator     = in_processed_indicator,
        message_text            = in_message_text,
        send_to_venus           = in_send_to_venus,
        order_printed           = in_order_printed,
        sub_type                = in_sub_type,
        error_description    = in_error_description,
        country              = in_country,
        cancel_reason_code = in_cancel_reason_code,
        comp_order         = in_comp_order,
        old_price          = in_old_price,
        ship_method     = in_ship_method,
        updated_on = SYSDATE,
        vendor_sku      = in_vendor_sku,
        product_description = in_product_description,
        product_weight  = in_product_weight,
        shipping_system = in_shipping_system,
        tracking_number = in_tracking_number,
        printed = in_printed,
        shipped = in_shipped,
        cancelled = in_cancelled,
        final_carrier = in_final_carrier,
        final_ship_method = in_final_ship_method,
        personal_greeting_id = in_personal_greeting_id
    WHERE venus_id = in_venus_id;

  IF SQL%ROWCOUNT = 0 THEN
     out_message := 'NO RECORD FOUND/UPDATED';
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_VENUS;


PROCEDURE UPDATE_VENUS_STATUS
(
 IN_VENUS_ID               IN VARCHAR2,
 IN_VENUS_STATUS           IN VARCHAR2,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure the status in table venus for the id passed in.

Input:
        venus_id       VARCHAR2
        venus_status   VARCHAR2

Output:
        status         VARCHAR2
        message        VARCHAR2

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE venus
    SET venus_status = in_venus_status,
        updated_on = SYSDATE
    WHERE venus_id = in_venus_id;

  IF SQL%ROWCOUNT = 0 THEN
     out_status := 'N';
     out_message := 'No record found/updated for venus id ' || in_venus_id || '.';
  ELSE
     out_status := 'Y';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_VENUS_STATUS;


PROCEDURE GET_VENUS_MESSAGE
(
 IN_VENUS_ORDER_NUMBER IN VARCHAR2,
 IN_MESSAGE_TYPE       IN VARCHAR2,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table venus
        for a particular venus order number and message type.

Input:
        message_id, messeage_type

Output:
        cursor containing venus record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT venus_id,
           venus_order_number,
           venus_status,
           msg_type,
           sending_vendor,
           filling_vendor,
           order_date,
           recipient,
           business_name,
           address_1,
           address_2,
           city,
           state,
           zip,
           country,
           phone_number,
           delivery_date,
           first_choice,
           price,
           card_message,
           ship_date,
           operator,
           comments,
           transmission_time,
           reference_number,
           product_id,
           combined_report_number,
           rof_number,
           adj_reason_code,
           over_under_charge,
           processed_indicator,
           message_text,
           send_to_venus,
           order_printed,
           sub_type,
           error_description,
           cancel_reason_code,
           comp_order,
           old_price,
           ship_method,
           vendor_sku,
           product_description,
           product_weight,
           shipping_system,
           tracking_number,
           printed,
           shipped,
           cancelled,
           final_carrier,
           final_ship_method,
           sds_status,
           forced_shipment,
           rated_shipment,
           country,
           vendor_id,
           rejected,
           get_sds_response,
           external_system_status,
           delivery_scan,
           zone_jump_flag,
	   zone_jump_label_date,
	   zone_jump_trailer_number,
           personal_greeting_id,
           new_shipping_system, 
           ftdwest_order_id
       FROM venus
       WHERE venus_order_number = in_venus_order_number
         AND msg_type = in_message_type
       ORDER BY venus_id;

END GET_VENUS_MESSAGE;


PROCEDURE GET_VENUS_MESSAGE_BY_VENUS_ID
(
 IN_VENUS_ID  IN VARCHAR2,
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table venus
        for a particular venus id.

Input:
        venus_id    VARCHAR2

Output:
        cursor containing venus record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT v.venus_id,
           v.venus_order_number,
           v.venus_status,
           v.msg_type,
           v.sending_vendor,
           v.filling_vendor,
           v.order_date,
           v.recipient,
           v.business_name,
           v.address_1,
           v.address_2,
           v.city,
           v.state,
           v.zip,
           v.country,
           v.phone_number,
           v.delivery_date,
           v.first_choice,
           v.price,
           v.card_message,
           v.ship_date,
           v.operator,
           v.comments,
           v.transmission_time,
           v.reference_number,
           v.product_id,
           v.combined_report_number,
           v.rof_number,
           v.adj_reason_code,
           v.over_under_charge,
           v.processed_indicator,
           v.message_text,
           v.send_to_venus,
           v.order_printed,
           v.sub_type,
           v.error_description,
           v.cancel_reason_code,
           v.comp_order,
           v.old_price,
           v.ship_method,
           v.shipping_system,
           v.tracking_number,
           v.printed,
           v.shipped,
           v.cancelled,
           v.final_carrier,
           v.final_ship_method,
           v.sds_status,
           v.forced_shipment,
           v.rated_shipment,
           v.country,
           v.product_description,
           v.product_weight,
           v.rejected,
           v.vendor_sku,
           v.external_system_status,
           v.vendor_id,
           v.get_sds_response,
	   o.origin_id,      
           v.zone_jump_flag,
	   v.zone_jump_label_date,
	   v.zone_jump_trailer_number,
	   v.inv_trk_count_date,
	   v.inv_trk_assigned_count_date,
           v.personal_greeting_id,
           v.new_shipping_system,
           v.ftdwest_order_id
       FROM venus v
       JOIN clean.order_details od
       ON v.reference_number = to_char(od.order_detail_id)
       JOIN clean.orders o
       ON o.order_guid = od.order_guid
       WHERE v.venus_id = in_venus_id;

END GET_VENUS_MESSAGE_BY_VENUS_ID;


PROCEDURE GET_VENUS_MESS_BY_ORDER_DETAIL
(
 IN_ORDER_DETAIL_ID IN VARCHAR2,
 IN_MESSAGE_TYPE    IN VARCHAR2,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table venus
        for a particular message type and is related to an order_detail
        for the order detail id passed in.

Input:
        order_detail_id  VARCHAR2
        messeage_type    VARCHAR2

Output:
        cursor containing venus record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT venus_id,
           venus_order_number,
           venus_status,
           msg_type,
           sending_vendor,
           filling_vendor,
           order_date,
           recipient,
           business_name,
           address_1,
           address_2,
           city,
           state,
           zip,
           country,
           phone_number,
           delivery_date,
           first_choice,
           price,
           card_message,
           ship_date,
           operator,
           comments,
           transmission_time,
           reference_number,
           product_id,
           combined_report_number,
           rof_number,
           adj_reason_code,
           over_under_charge,
           processed_indicator,
           message_text,
           send_to_venus,
           order_printed,
           sub_type,
           error_description,
           cancel_reason_code,
           comp_order,
           old_price,
           ship_method,
           vendor_sku,
           product_description,
           product_weight,
           shipping_system,
           get_sds_response,
           zone_jump_flag,
           zone_jump_label_date,
           zone_jump_trailer_number,
           personal_greeting_id,
           orig_ship_date,
           secondary_ship_date
       FROM venus
       WHERE reference_number = in_order_detail_id
         AND msg_type = in_message_type
       ORDER BY created_on desc;

END GET_VENUS_MESS_BY_ORDER_DETAIL;



PROCEDURE UPDATE_VENUS_VP
(
IN_VENUS_ID                     IN VENUS.VENUS_ID%TYPE,
IN_PROCESSED_INDICATOR          IN VENUS.PROCESSED_INDICATOR%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the given venus record
        with the given processed indicator and a status of VP.

Input:
        mercury_status                  varchar2
        outbound_id                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;
RECORD_LOCKED EXCEPTION;
PRAGMA EXCEPTION_INIT (RECORD_LOCKED, -54);

CURSOR  venus_cur IS
        SELECT  venus_status,
                processed_indicator,
                updated_on
        FROM    venus
        WHERE   venus_id = in_venus_id
        AND     venus_status IN ('OPEN', 'FTPERROR')
        FOR UPDATE OF   venus_status,
                        processed_indicator,
                        updated_on NOWAIT;

v_venus_status          venus.venus_status%type;
v_processed_indicator   venus.processed_indicator%type;
v_updated_on            venus.updated_on%type;

BEGIN

OPEN venus_cur;

FETCH venus_cur INTO v_venus_status, v_processed_indicator, v_updated_on;

IF venus_cur%found THEN
        UPDATE  venus
        SET     venus_status = 'VP',
                updated_on = sysdate,
                processed_indicator = in_processed_indicator
        WHERE   CURRENT OF venus_cur;

        COMMIT;
        OUT_STATUS := 'Y';
ELSE
        RAISE RECORD_LOCKED;
END IF;

CLOSE venus_cur;

-- error - record already locked
EXCEPTION WHEN RECORD_LOCKED THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'RECORD LOCKED';
        IF venus_cur%isopen THEN
                CLOSE venus_cur;
        END IF;
END;    -- end record locked exception

WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        IF venus_cur%isopen THEN
                CLOSE venus_cur;
        END IF;
END;    -- end exception block

END UPDATE_VENUS_VP;



PROCEDURE GET_VENUS_FTD_ORDERS
(
 IN_ROWNUM         IN NUMBER,
 IN_VENDOR_TYPE    IN VARCHAR2,
 IN_VENDOR_ID      IN VARCHAR2,
 OUT_CURSOR        OUT TYPES.REF_CURSOR,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a maximum number of FTD records from
        table venus.

Input:
        rownum (the maximum number of records to return)

Output:
        cursor containing venus record,
        status, error message

-----------------------------------------------------------------------------*/
v_new_processed_flag venus.processed_indicator%TYPE;
v_record_number integer := 0;
v_venus_id      venus.venus_id%type;
v_nextval       number;

CURSOR venus_cur IS
  SELECT v.venus_id
    FROM venus v,
         ftd_apps.vendor_master vm
    WHERE v.shipping_system = in_vendor_type
      AND (vm.vendor_id = in_vendor_id or in_vendor_id IS NULL)
      AND v.filling_vendor = vm.member_number
      AND v.msg_type = 'FTD'
      AND v.venus_status IN ('OPEN', 'FTPERROR')
  ORDER BY v.ship_date;

BEGIN

  /* A little history here: v_new_processed_flag was formerly produced by a random number
     generator.  Not only was this kludgey, it wasn't as random as we'd hoped.  So we
     replaced the random numbers with a sequence.  We kept the PRCSSNG_ prefix because it
     looked so specific, we weren't confident that no one else was using it in some other
     part of the application.  We added a ":" separator to keep the old PRCSSNG_<sequence>
     numbers from colliding with the new numbers.  The new format is
         PRCSSNG_:yyyymmddhh24mi:nnnnnn
     where the "n"s represent the numbers of the sequence.  The sequence has a maxvalue of
     999,999 and will cycle.  The timestamp should serve to keep it unique. */

  SELECT processed_flag_sq.NEXTVAL INTO v_nextval FROM dual;
  v_new_processed_flag := 'PRCSSNG_:' || TO_CHAR(SYSDATE,'yyyymmddhh24mi:') || LPAD(v_nextval,6,'0');


OPEN venus_cur;
FETCH venus_cur INTO v_venus_id;
out_status := 'Y';
out_error_message := null;

WHILE venus_cur%found AND v_record_number < in_rownum LOOP
        -- set the record to the given process and VP status
        UPDATE_VENUS_VP
        (
                v_venus_id,
                v_new_processed_flag,
                out_status,
                out_error_message
        );

        -- increment the record count
        IF out_status = 'Y' THEN
                v_record_number := v_record_number + 1;

        -- venus message was taken by another processes
        ELSIF out_error_message = 'RECORD LOCKED' THEN
                null;

        -- error occurred exit the process
        ELSE
                exit;
        END IF;


        -- get the next available venus message
        FETCH venus_cur INTO v_venus_id;
        out_status := 'Y';
        out_error_message := null;
END LOOP;
CLOSE venus_cur;


  OPEN out_cursor FOR
    SELECT venus_id,
           venus_order_number,
           venus_status,
           msg_type,
           sending_vendor,
           filling_vendor,
           order_date,
           translate (recipient, chr(9)||chr(10)||chr(13), '   ') recipient,
           translate (business_name, chr(9)||chr(10)||chr(13), '   ') business_name,
           translate (address_1, chr(9)||chr(10)||chr(13), '   ') address_1,
           translate (address_2, chr(9)||chr(10)||chr(13), '   ') address_2,
           translate (city, chr(9)||chr(10)||chr(13), '   ') city,
           translate (state, chr(9)||chr(10)||chr(13), '   ') state,
           translate (zip, chr(9)||chr(10)||chr(13), '   ') zip,
           translate (country, chr(9)||chr(10)||chr(13), '   ') country,
           translate (substr (RPAD(phone_number,10,'0'), -10) , chr(9)||chr(10)||chr(13), '   ') phone_number,
           delivery_date,
           translate (first_choice, chr(9)||chr(10)||chr(13), '   ') first_choice,
           price,
           translate (card_message, chr(9)||chr(10)||chr(13), '   ') card_message,
           ship_date,
           operator,
           translate (comments, chr(9)||chr(10)||chr(13), '   ') comments,
           transmission_time,
           reference_number,
           product_id,
           combined_report_number,
           rof_number,
           adj_reason_code,
           over_under_charge,
           processed_indicator,
           translate (message_text, chr(9)||chr(10)||chr(13), '   ') message_text,
           send_to_venus,
           order_printed,
           sub_type,
           error_description,
           cancel_reason_code,
           comp_order,
           old_price,
           ship_method,
           vendor_sku,
           translate (product_description, chr(9)||chr(10)||chr(13), '   ') product_description,
           product_weight,
           personal_greeting_id
       FROM venus
       WHERE processed_indicator = v_new_processed_flag;

  EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END GET_VENUS_FTD_ORDERS;


FUNCTION GET_NUM_OF_TIMES_MESSAGE_SENT
(
 IN_EXTERNAL_ORDER_NUMBER IN VENUS.REFERENCE_NUMBER%TYPE,
 IN_MESSAGE_TYPE          IN VARCHAR2
)
 RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves the number of messages sent to the VENUS
        system for a particular reference_number (external order number).

Input:
        reference_number

Output:
        message count

-----------------------------------------------------------------------------*/

out_count NUMBER;

BEGIN

  SELECT COUNT(*) INTO out_count
     FROM venus
     WHERE reference_number = in_external_order_number
       AND msg_type = in_message_type;

  RETURN out_count;

END GET_NUM_OF_TIMES_MESSAGE_SENT;


PROCEDURE GET_MESSAGE_TYPE_COUNTS
(
 IN_EXTERNAL_ORDER_NUMBER IN VENUS.REFERENCE_NUMBER%TYPE,
 IN_VENUS_ID              IN VARCHAR2,
 OUT_FTD_COUNT     OUT NUMBER,
 OUT_CON_COUNT     OUT NUMBER,
 OUT_REJ_COUNT     OUT NUMBER,
 OUT_CAN_COUNT     OUT NUMBER,
 OUT_PRINTED_COUNT OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the number of four different messages,
        (ftd, con, rej, can) and the number of times the different messages
        were printed base on particular reference_number (external order number)
        but exluding from all the counts the venus id passed in.

Input:
        reference_number (external order number), venus id

Output:
        ftd message count, con message count, rej message count, can message count,
        printed count.

-----------------------------------------------------------------------------*/

BEGIN

  SELECT COUNT(*) INTO out_ftd_count
     FROM venus
     WHERE reference_number = in_external_order_number
       AND venus_id <> in_venus_id
       AND msg_type = 'FTD';

  SELECT COUNT(*) INTO out_con_count
     FROM venus
     WHERE reference_number = in_external_order_number
       AND venus_id <> in_venus_id
       AND msg_type = 'CON';

  SELECT COUNT(*) INTO out_rej_count
     FROM venus
     WHERE reference_number = in_external_order_number
       AND venus_id <> in_venus_id
       AND msg_type = 'REJ';

  SELECT COUNT(*) INTO out_can_count
     FROM venus
     WHERE reference_number = in_external_order_number
       AND venus_id <> in_venus_id
       AND msg_type = 'CAN'
       AND venus_status = 'VERIFIED';

  SELECT COUNT(*) INTO out_printed_count
     FROM venus
     WHERE reference_number = in_external_order_number
       AND venus_id <> in_venus_id
       AND order_printed = 'Y';

END GET_MESSAGE_TYPE_COUNTS;


PROCEDURE INSERT_FTP_TRANSMISSION_HIST
(
 IN_VENDOR_ID           IN VARCHAR2,
 IN_TRANSMISSION_TYPE  IN VARCHAR2,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table
        ftp_transmission_history.

Input:
        vendor_id          VARCHAR2
        transmission_type VARCHAR2


Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  INSERT INTO ftp_transmission_history
   (ftp_transmission_history_id,
    vendor_id,
    transmission_type,
    created_on)
   VALUES
    (ftp_transmission_history_id_sq.NEXTVAL,
     in_vendor_id,
     in_transmission_type,
     SYSDATE);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_FTP_TRANSMISSION_HIST;


FUNCTION GET_DAILY_FTP_COUNT
(
 IN_VENDOR_ID          IN FTP_TRANSMISSION_HISTORY.VENDOR_ID%TYPE,
 IN_TRANSMISSION_TYPE  IN FTP_TRANSMISSION_HISTORY.TRANSMISSION_TYPE%TYPE
)
 RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure will return the number of records that have the given
        vendor id and ftp type and have a transmission date equal to
        the current date.

Input:
        vendor_id          - VARCHAR2
        transmission_type  - VARCHAR2

Output:
        message count

-----------------------------------------------------------------------------*/

out_count NUMBER;

BEGIN

  SELECT COUNT(*) INTO out_count
    FROM ftp_transmission_history
   WHERE vendor_id = in_vendor_id
     AND transmission_type = in_transmission_type
     AND TRUNC(created_on) = TRUNC(SYSDATE);

  RETURN out_count;

END GET_DAILY_FTP_COUNT;


PROCEDURE GET_CUSTOMERS_LAST_MONTH
(
 IN_VENDOR_ID  IN VARCHAR2,
 IN_MONTH      IN NUMBER,
 IN_YEAR       IN NUMBER,
 OUT_CURSOR   OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will return a list of customers that placed
        orders for the vendor id, month and year passed in.
        The daytime phone number will be included otherwise the evening phone
        number will be if the daytime phone number doesn't exist.

Input:
        venus_id   VARCHAR2
        month      NUMBER
        year       NUMBER

Output:
        cursor containing venus record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT v.venus_order_number,
           c.first_name,
           c.last_name,
           c.address_1,
           c.address_2,
           c.city,
           c.state,
           c.zip_code,
           NVL(cpd.phone_number,cpe.phone_number) "phone_number",
           ode.info_data "birthday",
           c.business_name
       FROM clean.customer_phones cpe,
            clean.customer_phones cpd,
            clean.customer c,
            clean.order_detail_extensions ode,
            clean.order_details od,
            venus v,
            ftd_apps.vendor_master vm
       WHERE vm.vendor_id = in_vendor_id
         AND v.filling_vendor = vm.member_number
         AND TO_NUMBER(TO_CHAR(v.order_date,'MM')) = in_month
         AND TO_NUMBER(TO_CHAR(v.order_date,'IYYY')) = in_year
         AND TO_CHAR(od.order_detail_id) = v.reference_number
         AND ode.order_detail_id = od.order_detail_id
         AND ode.info_name = 'Customer Birthdate'
         AND c.customer_id = od.recipient_id
         AND cpd.customer_id (+) = c.customer_id
         AND cpd.phone_type (+) = 'Day'
         AND cpe.customer_id (+) = c.customer_id
         AND cpe.phone_type (+) = 'Evening';

END GET_CUSTOMERS_LAST_MONTH;


PROCEDURE GET_FEDEX_COMMITMENT_DETAILS
(
 IN_ZIPCODE      IN FEDEX_COMMITMENT_ZIP_MAPPING.ZIPCODE%TYPE,
 IN_SHIP_METHOD  IN FEDEX_COMMITMENT_DETAILS.SHIP_METHOD%TYPE,
 IN_DAY_OF_WEEK  IN FEDEX_COMMITMENT_DETAILS.DAY_OF_WEEK%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will return a record from table
        fedex_commitment_details for the given the passed in data.

Input:
        zipcode        VARCHAR2
        ship_method    VARCHAR2
        day_of_week    CHAR

Output:
        cursor containing fedex_commitment_details record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT fcd.commitment_indicator,
           fcd.ship_method,
           fcd.day_of_week,
           fcd.delivery_available,
           fcd.delivery_business_days,
           fcd.delivery_time,
           fcd.add_one_business_day_for_hi,
           fcd.sat_hi_delivery_available
      FROM fedex_commitment_details fcd,
           fedex_commitment_zip_mapping fczm
      WHERE fczm.zipcode = in_zipcode
        AND fczm.commitment_indicator = fcd.commitment_indicator
        AND fcd.ship_method = in_ship_method
        AND fcd.day_of_week = in_day_of_week;

END GET_FEDEX_COMMITMENT_DETAILS;


FUNCTION GET_VENDOR_SHIP_METHOD
(
IN_VENDOR_ID            IN VENDOR_SHIP_METHODS.VENDOR_ID%TYPE,
IN_FTD_SHIP_METHOD      IN VENDOR_SHIP_METHODS.FTD_SHIP_METHOD%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the vendor ship method for the given
        vendor id and ftd ship method

Input:
        vendor_id               varchar2
        ftd_ship_method         varchar2

Output:
        vendor ship method
-----------------------------------------------------------------------------*/

CURSOR sm_cur IS
        SELECT  vendor_ship_method
        FROM    vendor_ship_methods
        WHERE   vendor_id = in_vendor_id
        AND     ftd_ship_method = in_ftd_ship_method;

v_vendor_ship_method    vendor_ship_methods.vendor_ship_method%type;

BEGIN

OPEN sm_cur;
FETCH sm_cur INTO v_vendor_ship_method;
CLOSE sm_cur;

RETURN v_vendor_ship_method;

END GET_VENDOR_SHIP_METHOD;


PROCEDURE INSERT_FEDEX_SCHEDULE_STAGE
(
 IN_SERVICE_TYPE           	IN VARCHAR2,
 IN_ORIGIN_ZIP           	IN VARCHAR2,
 IN_ORIGIN_TERMINAL         IN VARCHAR2,
 IN_DEST_ZIP           		IN VARCHAR2,
 IN_DEST_TERMINAL           IN VARCHAR2,
 IN_SERVICE_DAYS           	IN VARCHAR2,
 IN_ON_TIME_SERVICE         IN VARCHAR2,
 OUT_STATUS        			OUT VARCHAR2,
 OUT_MESSAGE 				OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table
        fedex_ground_stage.

Input:
		IN_SERVICE_TYPE           	IN VARCHAR2
		IN_ORIGIN_ZIP           	IN VARCHAR2
		IN_ORIGIN_TERMINAL          IN VARCHAR2
		IN_DEST_ZIP           		IN VARCHAR2
		IN_DEST_TERMINAL            IN VARCHAR2
		IN_SERVICE_DAYS           	IN VARCHAR2
		IN_ON_TIME_SERVICE          IN VARCHAR2

Output:
        OUT_STATUS        			OUT VARCHAR2,
		OUT_MESSAGE 				OUT VARCHAR2

-----------------------------------------------------------------------------*/
BEGIN

  INSERT INTO fedex_ground_schedule_stage
   (service_type,
    origin_zip,
    origin_terminal,
    dest_zip,
    dest_terminal,
    service_days,
    on_time_service)
   VALUES
    (in_service_type,
     in_origin_zip,
     in_origin_terminal,
     in_dest_zip,
     in_dest_terminal,
     in_service_days,
     in_on_time_service);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_FEDEX_SCHEDULE_STAGE;

PROCEDURE UPDATE_VENUS_COMMENT
(
  IN_VENUS_ID      IN VARCHAR2,
  IN_COMMENTS   IN VARCHAR2,
  OUT_STATUS     OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/* -------------------------------------------------------------------------
DESCRIPTION :
	THIS PROCEDURE IS UPDATING THE COMMENTS ON THE VENUS TABLE FOR THE
	GIVEN VENUS ID.

INPUT :
	VENUS_ID 		VARCHAR2
	COMMENTS		VARCHAR2

OUTPUT :
	STATUS			VARCHAR2
	MESSAGE			VARCHAR2

----------------------------------------------------------------------------*/
BEGIN

UPDATE VENUS V
    SET V.COMMENTS=IN_COMMENTS
    WHERE V.VENUS_ID=IN_VENUS_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURED[' || SQLCODE || ']' || SUBSTR (SQLERRM,1,256);
    ROLLBACK;

END;

END UPDATE_VENUS_COMMENT;


PROCEDURE VIEW_Q_ITEM_DATA_BY_VENUS_ID
(
IN_VENUS_ID                     IN  VENUS.VENUS_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the venus queue
        reference data by venus id

Input:
        venus_id                       varchar2

Output:
        cursor containing data from venus.venus, clean.order_details,
        claen.orders for the vendus_id

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  v.venus_order_number,
                v.reference_number,
                od.order_guid,
                od.external_order_number,
                o.master_order_number
         FROM   venus.venus v, clean.order_details od, clean.orders o
         WHERE  v.venus_id         = in_venus_id
         AND    v.reference_number = od.order_detail_id
         AND    od.order_guid      = o.order_guid;

END VIEW_Q_ITEM_DATA_BY_VENUS_ID;


PROCEDURE VIEW_VENUS_MSG_DATA_BY_VNS_ID
(
IN_VENUS_ID                     IN  VENUS.VENUS_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the venus message
        reference data by venus id

Input:
        venus_id                       varchar2

Output:
        cursor containing data from venus.venus, clean.order_details,
        clean.orders, clean.customer, clean.customer_phones

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  v.venus_order_number,
                v.reference_number,
                v.sending_vendor,
                v.filling_vendor,
                o.order_date,
                c.first_name,
                c.last_name,
                c.business_name,
                c.address_1,
                c.address_2,
                c.city,
                c.state,
                c.zip_code,
                p.phone_number,
                od.external_order_number,
                o.master_order_number
         FROM   venus.venus v, clean.order_details od, clean.orders o, clean.customer c, clean.customer_phones p
         WHERE  v.venus_id         = in_venus_id
         AND    v.reference_number = od.order_detail_id
         AND    od.order_guid      = o.order_guid
         AND    od.recipient_id    = c.customer_id
         AND    p.customer_id      (+)= c.customer_id;

END VIEW_VENUS_MSG_DATA_BY_VNS_ID;


PROCEDURE GET_FTD_DELIVERY_DATE
(
IN_VENUS_ORDER_NUMBER           IN  VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the FTD delivery date for the specified
        venus order number.

Input:
        venus_order_number      varchar2

Output:
        cursor containing the value of the FTD delivery date
-----------------------------------------------------------------------------*/
BEGIN

OPEN    OUT_CUR FOR
SELECT  delivery_date
FROM    venus.venus
WHERE   msg_type = 'FTD' AND 
        venus_order_number = IN_VENUS_ORDER_NUMBER;

END GET_FTD_DELIVERY_DATE;


FUNCTION GET_VENDOR_SHIPPING_SYSTEM
(
IN_VENUS_ORDER_NUMBER         IN VENUS.VENUS_ORDER_NUMBER%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the vendor shipping system for the given
        vendor order number

Input:
        venus_order_number      varchar2

Output:
        shipping_sytem
-----------------------------------------------------------------------------*/

	CURSOR ss_cur IS
   SELECT shipping_system
     FROM venus.venus
    WHERE venus_order_number = in_venus_order_number
      AND msg_type = 'FTD';

v_shipping_system    venus.shipping_system%type;

BEGIN

	OPEN ss_cur;
	FETCH ss_cur INTO v_shipping_system;
	CLOSE ss_cur;

	RETURN v_shipping_system;

END GET_VENDOR_SHIPPING_SYSTEM;

PROCEDURE FIND_FTD_MESS_BY_ORDER_DETAIL
(
 IN_ORDER_DETAIL_ID IN VENUS.REFERENCE_NUMBER%TYPE,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table venus
        for a FTD message type that does not have an 'CAN' or 'REJ' message
        associated and is related to an order_detail
        for the order detail id passed in.
Input:
        order_detail_id  VARCHAR2
Output:
        cursor containing venus record
-----------------------------------------------------------------------------*/
BEGIN
  OPEN out_cursor FOR
 SELECT
v1.VENUS_ID,
v1.VENUS_ORDER_NUMBER,
v1.MSG_TYPE,
v1.ORDER_DATE,
v1.DELIVERY_DATE,
v1.SHIP_DATE,
v1.REFERENCE_NUMBER,
v1.SHIP_METHOD,
v1.TRACKING_NUMBER,
v1.FINAL_CARRIER,
v1.FINAL_SHIP_METHOD,
v1.LAST_SCAN,
v1.DELIVERY_SCAN,
v1.SDS_STATUS
 from venus.venus v1
WHERE v1.venus_id = 
( select max(v2.venus_id) from venus.venus v2 WHERE v2.msg_type = 'FTD' and v2.reference_number = in_order_detail_id )
ORDER BY CREATED_ON DESC;
END FIND_FTD_MESS_BY_ORDER_DETAIL;


PROCEDURE UPDATE_VENUS_MESSAGE_FOR_ZJ
(
 IN_VENUS_ID               	IN VARCHAR2,
 IN_ZONE_JUMP_FLAG         	IN CHAR,
 IN_ZONE_JUMP_LABEL_DATE   	IN DATE,
 IN_ZONE_JUMP_TRAILER_NUMBER	IN VARCHAR2,
 OUT_STATUS  			OUT VARCHAR2,
 OUT_MESSAGE 			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the zone jump fields in table venus for the id passed in.

Input:
        venus_id       			VARCHAR2
        zone_jump_flag  		CHAR
        zone_jump_label_date		DATE
        zone_jump_trailer_number	VARCHAR2
        updated_by			VARCHAR2

Output:
        status         VARCHAR2
        message        VARCHAR2

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE venus
    SET zone_jump_flag = in_zone_jump_flag,
        zone_jump_label_date = in_zone_jump_label_date,
        zone_jump_trailer_number = in_zone_jump_trailer_number,
        updated_on = SYSDATE
    WHERE venus_id = in_venus_id;

  IF SQL%ROWCOUNT = 0 THEN
     out_status := 'N';
     out_message := 'No record found/updated for venus id ' || in_venus_id || '.';
  ELSE
     out_status := 'Y';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_VENUS_MESSAGE_FOR_ZJ;

PROCEDURE GET_VENUS_MESSAGE_FOR_ZJ
(
   IN_VENUS_ID       IN VARCHAR2,
   OUT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------------------------
Description:
        This stored procedure retrieves zone jump flag, zone jump label date and sds status
        for venus id passed in.

Input:
        venus_id       			VARCHAR2
        
Output:
	cursor containing zj venus record fields
------------------------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT v.zone_jump_flag,
               v.zone_jump_label_date,
               v.sds_status
        FROM venus.venus v
	WHERE v.venus_id = in_venus_id;

END GET_VENUS_MESSAGE_FOR_ZJ;

PROCEDURE UPDATE_VENDOR_SKU
(
  IN_VENUS_ID      		IN VARCHAR2,
  IN_VENDOR_SKU   		IN VARCHAR2,
  OUT_STATUS     		OUT VARCHAR2,
  OUT_MESSAGE 			OUT VARCHAR2
)
AS

/* -------------------------------------------------------------------------
DESCRIPTION :
	THIS PROCEDURE UPDATED THE VENDOR SKU ONN THE VENUS TABLE FOR THE GIVEN VENUS ID.

INPUT :
	VENUS_ID 		VARCHAR2
	VENDOR_SKU		VARCHAR2

OUTPUT :
	STATUS			VARCHAR2
	MESSAGE			VARCHAR2

----------------------------------------------------------------------------*/
BEGIN

UPDATE VENUS V
   SET V.VENDOR_SKU=IN_VENDOR_SKU
 WHERE V.VENUS_ID=IN_VENUS_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURED[' || SQLCODE || ']' || SUBSTR (SQLERRM,1,256);
    ROLLBACK;

END;

END UPDATE_VENDOR_SKU;

PROCEDURE UPDATE_NEW_SHIPPING_SYSTEM
(
 IN_VENUS_ID               IN VARCHAR2,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure sets the new shipping system flag to 'Y' in 
        table venus for the id passed in.

Input:
        venus_id       VARCHAR2

Output:
        status         VARCHAR2
        message        VARCHAR2

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE venus
    SET new_shipping_system = 'Y',
        updated_on = SYSDATE
    WHERE venus_id = in_venus_id;

  IF SQL%ROWCOUNT = 0 THEN
     out_status := 'N';
     out_message := 'No record found/updated for venus id ' || in_venus_id || '.';
  ELSE
     out_status := 'Y';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_NEW_SHIPPING_SYSTEM;

/*-------------------------------------------------------------------------------------------------------------------------------
Description:
		This procedure accepts the fields read from the vendor updates file sent by FTDWest and updates records in venus.venus 
		and venus.venus_add_ons tables.
Input:
	IN_VENUS_ORDER_NUMBER 	VARCHAR2
	IN_FILLING_VENDOR     	VARCHAR2
	IN_SKU                	VARCHAR2
	IN_UNIT_COST          	NUMBER
	IN_INDICATOR          	VARCHAR2

Output:	
	OUT_STATUS				VARCHAR2
	OUT_MESSAGE 			VARCHAR2
---------------------------------------------------------------------------------------------------------------------------------*/
PROCEDURE UPDATE_FTDW_VENDOR_ORDER
(
	IN_VENUS_ORDER_NUMBER 	IN VARCHAR2,
	IN_FILLING_VENDOR     	IN VARCHAR2,
	IN_PID                	IN VARCHAR2,
	IN_UNIT_COST          	IN NUMBER,
	IN_INDICATOR          	IN VARCHAR2,
	OUT_STATUS				OUT VARCHAR2,
	OUT_MESSAGE 			OUT VARCHAR2
)
AS
	v_vendor_id   			ftd_apps.vendor_master.vendor_id%type;
	v_venus_order_exists	number;
	v_venus_id        		venus.venus_id%type;
	v_valid_pid				number;
	v_reference_number      venus.REFERENCE_NUMBER%type;
	
BEGIN
	
	-- Check if the venus_order_number read from the file exists in Apollo. If not, return an appropriate error message.
	begin
		select 	count(*) 
		into	v_venus_order_exists
		from 	venus.venus 
		where 	venus_order_number = IN_VENUS_ORDER_NUMBER;
	exception
	when others then
		OUT_STATUS := 'E';
		OUT_MESSAGE := 'Error occured while looking for venus_order_number:'||IN_VENUS_ORDER_NUMBER||' in venus.venus table';
		return;
	end;
	
	if(v_venus_order_exists = 0) then
		OUT_STATUS := 'E';
		OUT_MESSAGE := 'Venus order number:'||IN_VENUS_ORDER_NUMBER||' not found';
		return;
	end if;

	-- Check if the vendor-code read from the file exists in Apollo. If not, return an appropriate error message.
	begin
		select  vendor_id
		into    v_vendor_id
		from    ftd_apps.vendor_master
		where   member_number = IN_FILLING_VENDOR;
	exception
	when no_data_found then
		OUT_STATUS := 'E';
		OUT_MESSAGE := 'Vendor '||IN_FILLING_VENDOR||' not found. Venus order number:'||IN_VENUS_ORDER_NUMBER;
		return;
	when others then
		OUT_STATUS := 'E';
		OUT_MESSAGE := 'Error occured while looking for vendor code:'||IN_FILLING_VENDOR||' in ftd_apps.vendor_master table';
		return;
	end;
	
	-- If the record is of Product related, update filling_vendor and price columns in venus.venus table by looking up respective venus_order_number.
	if(IN_INDICATOR = 'P') then 
		begin
		
			-- Check if the PID received from West is the one associated with the order in Apollo.
			begin
				select	count(*)
				into	v_valid_pid
				from	venus.venus
				where 	venus_order_number = IN_VENUS_ORDER_NUMBER
						and msg_type = 'FTD'
						and vendor_sku = IN_PID;
			exception
			when others then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Error occured while checking the validity of the PID: '||IN_PID||' for venus_order_number:'||IN_VENUS_ORDER_NUMBER||' in venus.venus table';
				return;
			end;
			
			if(v_valid_pid = 0) then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Invalid PID:'||IN_PID||' for venus order number:'||IN_VENUS_ORDER_NUMBER;
				return;
			end if;
			
			-- Filling vendor should be updated to all the records with the venus_order_number irrespective of msg_type.
			update  venus.venus
			set     filling_vendor = IN_FILLING_VENDOR,
					vendor_id = v_vendor_id,
					updated_on = sysdate	
			where   VENUS_ORDER_NUMBER = IN_VENUS_ORDER_NUMBER;
			
			-- If no records are updated, that implies venus order number is not found. Rollback the transaction and return an appropriate error message.
			if(sql%rowcount = 0) then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Could not find venus order number '||IN_VENUS_ORDER_NUMBER||' to update filling vendor';
				rollback;
				return;
			end if;
		exception
		when others then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Error occured while updating filling vendor. Details: Venus Order Number:'||IN_VENUS_ORDER_NUMBER||'; Filling Vendor:'||IN_FILLING_VENDOR;
			rollback;
			return;
		end;

		begin
			/*---------------------------------------------------------------------------------------------------------------
			
			Price should be applied to the records with msg_type = 'FTD' and 'ADJ' for the venus_order_number.
			Third party fulfilled orders are not expected to appear in the file received immediately after their delivery
			date. FTDWest confirmed that there could be a delay of 2 or 3 days to receive invoice for those orders from respective
			third party vendors and those orders will then be included in the following day's file.
			The price received is applied to ADJ message too, to address above scenario, to avoid discrepancies in the
			West Adjusted Order Report ---> Argo report.
			-----------------------------------------------------------------------------------------------------------------*/
			update  venus.venus
			set     price = IN_UNIT_COST
			where   VENUS_ORDER_NUMBER = IN_VENUS_ORDER_NUMBER
					and msg_type in ('FTD','ADJ');  
					
			-- If no records are updated, that implies a record with msg_type = 'FTD' for the venus order number is not found. Rollback the transaction and return an appropriate error message.
			if(sql%rowcount = 0) then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Could not find a record with msg_type = FTD to update filling price. Venus order number:'||IN_VENUS_ORDER_NUMBER;
				rollback;
				return;
			end if;
		exception
			when others then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Error occured while updating price. Details: Venus Order Number:'||IN_VENUS_ORDER_NUMBER||'; Price:'||IN_UNIT_COST;
			rollback;
			return;
		end;
		
	/*-------------------------------------------------------------------------------------------------------
	 Filling vendor should be updated in all the tables with the venus_order_number with the msg_type FTD 
	 --------------------------------------------------------------------------------------------------------*/		
		begin
			select REFERENCE_NUMBER
			INTO v_reference_number	
			from   VENUS.VENUS
			where  VENUS_ORDER_NUMBER = IN_VENUS_ORDER_NUMBER and MSG_TYPE = 'FTD';
		exception
		   when others then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Error occured while fetching the reference number for the Venus Order Number:'||IN_VENUS_ORDER_NUMBER;
			rollback;
			return;
		end;
		
		begin					    			
			update  CLEAN.ORDER_DETAILS
			set     FLORIST_ID = IN_FILLING_VENDOR,
					updated_on = sysdate
			where   ORDER_DETAIL_ID = v_reference_number;			 
			 
			-- If no records are updated, that implies order detail not found venus order number is not found. Rollback the transaction and return an appropriate error message.
			if(sql%rowcount = 0) then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Could not update the filling vendor in ORDER_DETAILS table. Details: Order Detail Id:'||v_reference_number||'; Filling Vendor:'||IN_FILLING_VENDOR;				
				rollback;
				return;
			end if;
      
            update  CLEAN.ORDER_FLORIST_USED
			set     FLORIST_ID = IN_FILLING_VENDOR
			where   ORDER_DETAIL_ID = v_reference_number;
			 
			-- If no records are updated, that implies venus order number is not found. Rollback the transaction and return an appropriate error message.
			if(sql%rowcount = 0) then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Could not update the filling vendor in ORDER_FLORIST_USED table . Details: Order Detail Id:'||v_reference_number||'; Filling Vendor:'||IN_FILLING_VENDOR;
				rollback;
				return;
			end if;
		exception
		when others then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Error occured while updating filling vendor. Details: Venus Order Number:'||IN_VENUS_ORDER_NUMBER||'; Filling Vendor:'||IN_FILLING_VENDOR;
			rollback;
			return;
		end;
		
	-- If the record is of Addon related, update per_addon_price column in venus.venus_add_ons table by looking up respective venus_id and addon_id.
	elsif(IN_INDICATOR = 'A') then
		begin
			-- Look up for a venus_id for respective venus order number with msg_type = 'FTD'.
			select  venus_id
			into    v_venus_id
			from    venus.venus
			where   VENUS_ORDER_NUMBER = IN_VENUS_ORDER_NUMBER
					and msg_type = 'FTD';  
		exception
		when no_data_found then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Could not find a record with msg_type = FTD. Venus Order Number:'||IN_VENUS_ORDER_NUMBER;
			return;
		when others then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Error occured while finding a record with msg_type = FTD. Venus Order Number:'||IN_VENUS_ORDER_NUMBER;
			return;
		end;
		
		-- Update per_addon_price for the venus_id and addon_id retrieved above.	
		begin
			update  venus.venus_add_ons
			set		per_addon_price = IN_UNIT_COST,
					updated_on = sysdate,
					updated_by = 'SHIP'
			where	venus_id = v_venus_id
					and addon_id in (select addon_id from ftd_apps.addon where pquad_accessory_id = IN_PID);
			
			-- If no records are updated, the addon_id passed is not matching with the one in venus.venus_add_ons table. 
			-- Rollback the transaction and return an appropriate error message.
			if(sql%rowcount = 0) then
				OUT_STATUS := 'E';
				OUT_MESSAGE := 'Invalid/Extra addon record found with PID :'||IN_PID||' for venus order number:'||IN_VENUS_ORDER_NUMBER;
				rollback;
				return;
			end if;
		exception
		when others then
			OUT_STATUS := 'E';
			OUT_MESSAGE := 'Error occured while updating unit cost for addon with PID:'||IN_PID||' for venus order number:'||IN_VENUS_ORDER_NUMBER;
			rollback;
			return;
		end;
	else
		-- If the indicator is neither A nor P, return an appropriate error message.
		OUT_STATUS := 'E';
		OUT_MESSAGE := 'Unknown indicator:'||IN_INDICATOR||'. Venus Order Number:'||IN_VENUS_ORDER_NUMBER;
		return;
	end if;  

	OUT_STATUS := 'S';
	OUT_MESSAGE := 'Success';

	commit;
   
EXCEPTION
when others then
    OUT_STATUS := 'E';
    OUT_MESSAGE := 'Error occured in procedure VENUS_PKG.UPDATE_FTDW_VENDOR_ORDER. Details:'||sqlerrm;
    rollback;
END UPDATE_FTDW_VENDOR_ORDER;

END VENUS_PKG;
.
/
