CREATE OR REPLACE
TRIGGER venus.trg_vendor_order_status_ai
AFTER INSERT 
ON venus.vendor_order_status
FOR EACH ROW

DECLARE
  
--   enqueue_options      dbms_aq.enqueue_options_t;
--   message_properties   dbms_aq.message_properties_t;
--   message_handle       raw(2000);
--   message              sys.aq$_jms_text_message;
   out_status           varchar2(1000);
   out_message          varchar2(1000);
   v_payload       varchar2(2000);
BEGIN
      --   Create a new JMS Message
      --   message := sys.aq$_jms_text_message.construct();
      --   message.set_text(payload => :NEW.order_status_id );
      --   message_properties.CORRELATION := :NEW.order_status_id;

      --   message_properties.exception_queue := 'OJMS.OSP_APP_EXCEPTION';

      --   dbms_aq.enqueue (queue_name => 'OJMS.SDS_INBOUND_PROCESSING',
      --                  enqueue_options => enqueue_options,
      --                  message_properties => message_properties,
      --                  payload => message,
      --                  msgid => message_handle);
         v_payload := :NEW.order_status_id || '|WEST';
         
         events.POST_A_MESSAGE_FLEX('OJMS.SHIP_SDS_INBOUND_PROCESS',:NEW.order_status_id,v_payload,0,OUT_STATUS,OUT_MESSAGE);


EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In VENUS.TRG_VENDOR_ORDER_STATUS_AI: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_vendor_order_status_ai;
.
/