CREATE OR REPLACE PACKAGE BODY VENUS.MONITOR_PKG 
AS

PROCEDURE MONITOR_CARRIER_SCANS (
  IN_HOURS_TO_CHECK  IN  NUMBER,
  OUT_STATUS         OUT VARCHAR2,
  OUT_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is a monitor for the venus.scans and venus.scan_files 
        tables.  It looks to see if any records have been inserted into the
        tables in the past "IN_HOURS_TO_CHECK" hours

Input:
        hours_to_check                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message/email body)

-----------------------------------------------------------------------------*/

CURSOR carrier_cur IS
  SELECT c.carrier_id 
    FROM CARRIERS c 
    WHERE 
      c.SCAN_FORMAT IS NOT NULL AND 
      c.active = 'Y' 
    ORDER BY c.carrier_id;
      
v_file_count NUMBER;
v_scan_count NUMBER;
v_error_msg VARCHAR2(40) := chr(09)||'(check ship processing logs for errors)';

BEGIN
  OUT_STATUS := 'Y';
  OUT_MESSAGE := 'Carrier scan processing for the last '||
                 to_char(IN_HOURS_TO_CHECK)||
                 ' hours:'||chr(10)||chr(10)||
                 'CARRIER'||chr(09)||
                 'FILES'||chr(09)||chr(09)||
                 'SCANS'||chr(10)||
                 '-------'||chr(09)||
                 '-----'||chr(09)||chr(09)||
                 '-----'||chr(10);

  FOR carrier_rec IN carrier_cur
  LOOP
    SELECT count(*) INTO v_file_count 
      FROM SCAN_FILES f 
      WHERE 
        f.CARRIER_ID = carrier_rec.carrier_id AND 
        f.created_on > SYSDATE-(IN_HOURS_TO_CHECK/24);
        
    SELECT count(*) INTO v_scan_count 
      FROM SCANS s 
      WHERE 
        s.CARRIER_ID = carrier_rec.carrier_id AND 
        s.created_on > SYSDATE-(IN_HOURS_TO_CHECK/24);
    
        OUT_MESSAGE := OUT_MESSAGE || 
                   carrier_rec.carrier_id || chr(09) ||  chr(09) ||
                   v_file_count || chr(09) || chr(09) || 
                   v_scan_count;
    
    IF v_file_count = 0 OR v_scan_count = 0 THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := OUT_MESSAGE || v_error_msg || chr(10);
    ELSE
      OUT_MESSAGE := OUT_MESSAGE || chr(10);
    END IF;
    
  END LOOP;
  
EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);      
END MONITOR_CARRIER_SCANS;

PROCEDURE MONITOR_DEL_SCANS_ON_UPSHIPPED (
  IN_SCAN_HOURS_TO_CHECK       IN NUMBER,
  IN_DELIVERY_DAYS_TO_CHECK    IN NUMBER,
  OUT_STATUS                  OUT VARCHAR2,
  OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is a monitor for the venus.scans and venus.scan_files 
        tables.  It looks to see if any delivery scan records have been inserted 
        into the tables in the past "IN_SCAN_HOURS_TO_CHECK" hours for orders 
        which have been not been shipped.

Input:
        hours_to_check                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message/email body)

-----------------------------------------------------------------------------*/

CURSOR carrier_cur IS
  SELECT c.carrier_id 
    FROM CARRIERS c 
    WHERE 
      c.SCAN_FORMAT IS NOT NULL AND 
      c.active = 'Y' 
    ORDER BY c.carrier_id;
      
v_file_count NUMBER;
v_scan_count NUMBER;
v_error_msg VARCHAR2(36) := chr(09)||'(notify customer service)';

BEGIN
  OUT_STATUS := 'Y';
  OUT_MESSAGE := 'Delivery scans received for cancelled/rejected orders in the last '||
                 to_char(IN_SCAN_HOURS_TO_CHECK)||
                 ' hours'||
                 ' for orders delivered in the last ' ||
                 to_char(IN_DELIVERY_DAYS_TO_CHECK) ||
                 ' days:' ||
                 chr(10)||chr(10)||
                 'CARRIER'||chr(09)||
                 'SCANS'||chr(10)||
                 '-------'||chr(09)||
                 '-----'||chr(10);

  FOR carrier_rec IN carrier_cur
  LOOP
    SELECT count(*) INTO v_file_count 
      FROM SCAN_FILES f 
      WHERE 
        f.CARRIER_ID = carrier_rec.carrier_id AND 
        f.created_on > SYSDATE-(IN_SCAN_HOURS_TO_CHECK/24);
        
    SELECT count(s.CARRIER_ID) INTO v_scan_count
      FROM VENUS.SCANS s 
      JOIN VENUS.VENUS v on s.TRACKING_NUMBER = v.TRACKING_NUMBER
      JOIN CLEAN.ORDER_DETAILS od on TO_NUMBER(v.REFERENCE_NUMBER) = od.ORDER_DETAIL_ID
      WHERE 
        s.created_on > SYSDATE-(IN_SCAN_HOURS_TO_CHECK/24) AND 
        (s.SCAN_TYPE='LD' OR s.SCAN_TYPE = '09' OR s.SCAN_TYPE = '20') AND 
        v.MSG_TYPE = 'FTD' AND
        v.SDS_STATUS != 'SHIPPED' AND
        s.CARRIER_ID = carrier_rec.carrier_id AND
        v.DELIVERY_DATE > trunc(sysdate)-IN_DELIVERY_DAYS_TO_CHECK;
    
        OUT_MESSAGE := OUT_MESSAGE || 
                   carrier_rec.carrier_id || 
                   chr(09) ||
                   chr(09) ||
                   v_scan_count;
    
    IF v_scan_count > 0 THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := OUT_MESSAGE || v_error_msg || chr(10);
    ELSE
      OUT_MESSAGE := OUT_MESSAGE || chr(10);
    END IF;
    
  END LOOP;
  
EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);      
END MONITOR_DEL_SCANS_ON_UPSHIPPED;

PROCEDURE MONITOR_ZJ_VENDOR_REJECTS(
  IN_MINUTES_TO_CHECK  IN  NUMBER,
  OUT_STATUS           OUT VARCHAR2,
  OUT_MESSAGE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is a BA monitor for the ZONE JUMP printed orders on a 
        cancelled trip or belonging to cancelled vendors that have not yet been
        rejected after "IN_MINUTES_TO_CHECK" minutes from the cancellation of the 
        trip or cancellation of vendor

Input:
        minutes_to_check                  number

Output:
        status                            varchar2 (Y or N)
        message                           varchar2 (error message/email body)

-----------------------------------------------------------------------------*/

-- static variables
v_current_time          date := SYSDATE;
v_minutes_in_a_day      number := 1440;

CURSOR cur_summary IS
SELECT count(vv.venus_order_number) order_cnt,
		 vm.vendor_name vendor,
		 vv.zone_jump_trailer_number trip,
       tvo.trip_id, 
		 tvo.vendor_id
  FROM venus.zj_trip_vendor tv, 
  		 venus.zj_trip_vendor_order tvo, 
  		 venus.venus vv,
  		 clean.order_details od,
  		 ftd_apps.vendor_master vm
 WHERE tv.trip_vendor_status_code = 'X' 
   AND tv.updated_on BETWEEN (v_current_time - 1) AND (v_current_time - (in_minutes_to_check / v_minutes_in_a_day))
   --AND tvo.order_filled_flag = 'Y'
   AND vv.sds_status = 'PRINTED'
   AND tv.trip_id = tvo.trip_id
   AND tv.vendor_id = tvo.vendor_id
   AND vv.venus_id = tvo.venus_id
   AND vv.reference_number = TO_CHAR(od.order_detail_id)
   AND tvo.vendor_id = vm.vendor_id
   AND not exists (
   	 select 1 from venus.venus v1 where v1.venus_order_number = vv.venus_order_number and v1.msg_type = 'REJ' and v1.venus_status = 'VERIFIED')
 GROUP BY
    vm.vendor_name,
    vv.zone_jump_trailer_number,
    tvo.trip_id, 
    tvo.vendor_id
 ORDER BY
 	 trip_id, 
 	 vendor_id;

BEGIN
	
	
  OUT_STATUS := 'Y';
  OUT_MESSAGE := 'Count of orders on following trip(s) not yet rejected by following vendor(s) after '||to_char(IN_MINUTES_TO_CHECK)||
                 ' minutes from cancellation of trip and/or vendor:'||chr(10);
	
	-- all printed orders for a cancelled vendor in the last 24 hours
	FOR rec_sum IN cur_summary
	LOOP
	   OUT_STATUS := 'N';
	   OUT_MESSAGE := OUT_MESSAGE || to_char(rec_sum.order_cnt) || ' -- ' || rec_sum.trip || ' -- ' || rec_sum.vendor ||chr(10);
	END LOOP;
	
	out_message := out_message || chr(10) || 'Query to retrieve the list of orders [default value for <in_minutes_to_check> is 90] : ' || chr(10) || chr(10);
	
	out_message := out_message || 
		' SELECT od.external_order_number,
				 vv.venus_order_number,
				 vm.vendor_name vendor,
				 vv.zone_jump_trailer_number trip
		  FROM venus.zj_trip_vendor tv, 
				 venus.zj_trip_vendor_order tvo, 
				 venus.venus vv,
				 clean.order_details od,
				 ftd_apps.vendor_master vm
		 WHERE tv.trip_vendor_status_code = ''X'' 
			AND tv.updated_on BETWEEN (sysdate - 1) AND (sysdate - (<in_minutes_to_check> / 1440))
			AND vv.sds_status = ''PRINTED''
			AND tv.trip_id = tvo.trip_id
			AND tv.vendor_id = tvo.vendor_id
			AND vv.venus_id = tvo.venus_id
			AND vv.reference_number = TO_CHAR(od.order_detail_id)
			AND tvo.vendor_id = vm.vendor_id
			AND not exists (
				 select 1 from venus.venus v1 where v1.venus_order_number = vv.venus_order_number 
				 and v1.msg_type = ''REJ'' and v1.venus_status = ''VERIFIED'')
		 ORDER BY tv.trip_id, vm.vendor_id;';
   
	--dbms_output.put_line(out_message);
  
EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN VENUS.MONITOR_PKG.MONITOR_ZJ_VENDOR_REJECTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256); 
END MONITOR_ZJ_VENDOR_REJECTS;

END MONITOR_PKG;
.
/
