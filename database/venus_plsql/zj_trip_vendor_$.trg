CREATE OR REPLACE
TRIGGER venus.zj_trip_vendor_$
AFTER INSERT OR UPDATE OR DELETE ON venus.zj_trip_vendor 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO venus.zj_trip_vendor$ (
         TRIP_ID,                        
         VENDOR_ID,                      
         TRIP_VENDOR_STATUS_CODE,        
         ORDER_CUTOFF_TIME,              
         PLT_CUBIC_INCH_ALLOWED_QTY,     
         FULL_PLT_QTY,                   
         FULL_PLT_ORDER_QTY,             
         PRTL_PLT_IN_USE_FLAG,           
         PRTL_PLT_ORDER_QTY,             
         PRTL_PLT_CUBIC_IN_USED_QTY,     
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :NEW.TRIP_ID,                        
         :NEW.VENDOR_ID,                      
         :NEW.TRIP_VENDOR_STATUS_CODE,        
         :NEW.ORDER_CUTOFF_TIME,              
         :NEW.PLT_CUBIC_INCH_ALLOWED_QTY,     
         :NEW.FULL_PLT_QTY,                   
         :NEW.FULL_PLT_ORDER_QTY,             
         :NEW.PRTL_PLT_IN_USE_FLAG,           
         :NEW.PRTL_PLT_ORDER_QTY,             
         :NEW.PRTL_PLT_CUBIC_IN_USED_QTY,     
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO venus.zj_trip_vendor$ (
         TRIP_ID,                        
         VENDOR_ID,                      
         TRIP_VENDOR_STATUS_CODE,        
         ORDER_CUTOFF_TIME,              
         PLT_CUBIC_INCH_ALLOWED_QTY,     
         FULL_PLT_QTY,                   
         FULL_PLT_ORDER_QTY,             
         PRTL_PLT_IN_USE_FLAG,           
         PRTL_PLT_ORDER_QTY,             
         PRTL_PLT_CUBIC_IN_USED_QTY,     
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :OLD.TRIP_ID,                        
         :OLD.VENDOR_ID,                      
         :OLD.TRIP_VENDOR_STATUS_CODE,        
         :OLD.ORDER_CUTOFF_TIME,              
         :OLD.PLT_CUBIC_INCH_ALLOWED_QTY,     
         :OLD.FULL_PLT_QTY,                   
         :OLD.FULL_PLT_ORDER_QTY,             
         :OLD.PRTL_PLT_IN_USE_FLAG,           
         :OLD.PRTL_PLT_ORDER_QTY,             
         :OLD.PRTL_PLT_CUBIC_IN_USED_QTY,     
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',SYSDATE);


      INSERT INTO venus.zj_trip_vendor$ (
         TRIP_ID,                        
         VENDOR_ID,                      
         TRIP_VENDOR_STATUS_CODE,        
         ORDER_CUTOFF_TIME,              
         PLT_CUBIC_INCH_ALLOWED_QTY,     
         FULL_PLT_QTY,                   
         FULL_PLT_ORDER_QTY,             
         PRTL_PLT_IN_USE_FLAG,           
         PRTL_PLT_ORDER_QTY,             
         PRTL_PLT_CUBIC_IN_USED_QTY,     
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :NEW.TRIP_ID,                        
         :NEW.VENDOR_ID,                      
         :NEW.TRIP_VENDOR_STATUS_CODE,        
         :NEW.ORDER_CUTOFF_TIME,              
         :NEW.PLT_CUBIC_INCH_ALLOWED_QTY,     
         :NEW.FULL_PLT_QTY,                   
         :NEW.FULL_PLT_ORDER_QTY,             
         :NEW.PRTL_PLT_IN_USE_FLAG,           
         :NEW.PRTL_PLT_ORDER_QTY,             
         :NEW.PRTL_PLT_CUBIC_IN_USED_QTY,     
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',SYSDATE);


   ELSIF DELETING  THEN

      INSERT INTO venus.zj_trip_vendor$ (
         TRIP_ID,                        
         VENDOR_ID,                      
         TRIP_VENDOR_STATUS_CODE,        
         ORDER_CUTOFF_TIME,              
         PLT_CUBIC_INCH_ALLOWED_QTY,     
         FULL_PLT_QTY,                   
         FULL_PLT_ORDER_QTY,             
         PRTL_PLT_IN_USE_FLAG,           
         PRTL_PLT_ORDER_QTY,             
         PRTL_PLT_CUBIC_IN_USED_QTY,     
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :OLD.TRIP_ID,                        
         :OLD.VENDOR_ID,                      
         :OLD.TRIP_VENDOR_STATUS_CODE,        
         :OLD.ORDER_CUTOFF_TIME,              
         :OLD.PLT_CUBIC_INCH_ALLOWED_QTY,     
         :OLD.FULL_PLT_QTY,                   
         :OLD.FULL_PLT_ORDER_QTY,             
         :OLD.PRTL_PLT_IN_USE_FLAG,           
         :OLD.PRTL_PLT_ORDER_QTY,             
         :OLD.PRTL_PLT_CUBIC_IN_USED_QTY,     
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',SYSDATE);

   END IF;

END;
/
