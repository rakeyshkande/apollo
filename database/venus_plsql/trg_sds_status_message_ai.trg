CREATE OR REPLACE
TRIGGER venus.trg_sds_status_message_ai
AFTER INSERT 
ON venus.sds_status_message
FOR EACH ROW

/*****************************************************************************************
***          Created By : Tim Peterson
***          Created On : 09/16/2006
***              Reason : Forward status messages received from SDS
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE
  
--   enqueue_options      dbms_aq.enqueue_options_t;
--   message_properties   dbms_aq.message_properties_t;
--   message_handle       raw(2000);
--   message              sys.aq$_jms_text_message;
   out_status           varchar2(1000);
   out_message          varchar2(1000);
BEGIN
         --Create a new JMS Message
      --   message := sys.aq$_jms_text_message.construct();
      --   message.set_text(payload => :NEW.id );
      --   message_properties.CORRELATION := :NEW.id;

      --   message_properties.exception_queue := 'OJMS.OSP_APP_EXCEPTION';

      --   dbms_aq.enqueue (queue_name => 'OJMS.SDS_INBOUND_PROCESSING',
      --                  enqueue_options => enqueue_options,
      --                  message_properties => message_properties,
      --                  payload => message,
      --                  msgid => message_handle);
         events.POST_A_MESSAGE_FLEX('OJMS.SHIP_SDS_INBOUND_PROCESS',:NEW.id,:NEW.id,0,OUT_STATUS,OUT_MESSAGE);


EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In VENUS.TRG_SDS_STATUS_MESSAGE_AI: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_sds_status_message_ai;
.
/
