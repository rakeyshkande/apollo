create or replace
PACKAGE BODY       VENUS.INV_TRK_PKG as

/******************************************************************************
UPDATE_INV_TRK_COUNT

Description: This procedure is responsible for updating product message count.
It calls the overloaded UPDATE_INV_TRK_COUNT procedure passing
product_id and ship_date retrieved from the FTD venus record.

******************************************************************************/
PROCEDURE UPDATE_INV_TRK_COUNT
(
IN_UPDATE_AMOUNT    IN NUMBER,
IN_VENUS_ID         IN VENUS.VENUS_ID%TYPE,
IN_MSG_TYPE         IN VENUS.MSG_TYPE%TYPE,
OUT_STATUS         OUT VARCHAR2,
OUT_MESSAGE        OUT VARCHAR2
)
AS

v_ftd_count           inv_trk_count.ftd_msg_count%type := 0;
v_can_count           inv_trk_count.can_msg_count%type := 0;
v_rej_count           inv_trk_count.rej_msg_count%type := 0;
v_updated_date        venus.inv_trk_count_date%type;
v_ftd_updated_date    venus.inv_trk_count_date%type;
v_product_id          venus.product_id%type;
v_ship_date           venus.ship_date%type;
v_need_update         varchar2(1);

BEGIN

  v_need_update := 'N';
  OUT_STATUS := 'Y';
  OUT_MESSAGE := '';

  IF IN_MSG_TYPE = 'FTD' THEN
    select inv_trk_count_date, product_id, ship_date
    into   v_ftd_updated_date, v_product_id, v_ship_date
    from   venus.venus
    where  venus_id = IN_VENUS_ID;

    -- Make sure the FTD message has NOT updated count.
    IF v_ftd_updated_date IS NULL THEN
      v_need_update := 'Y';
      v_ftd_count := IN_UPDATE_AMOUNT; 
    END IF;

  ELSIF IN_MSG_TYPE in ('CAN', 'REJ') THEN
    select inv_trk_count_date
    into   v_updated_date
    from   venus.venus
    where  venus_id = IN_VENUS_ID;

    select inv_trk_count_date, product_id, ship_date
    into   v_ftd_updated_date, v_product_id, v_ship_date
    from   venus.venus v
    where  v.msg_type = 'FTD'
    and    v.venus_order_number = 
      (select venus_order_number
       from   venus.venus v2
       where  v2.venus_id = IN_VENUS_ID
      );

    -- Make sure the FTD message has updated count the the CAN or REJ has not updated count.
    IF v_ftd_updated_date IS NOT NULL and v_updated_date IS NULL THEN
      v_need_update := 'Y';

      IF IN_MSG_TYPE = 'CAN' THEN
        v_can_count := IN_UPDATE_AMOUNT; 
      ELSE
        v_rej_count := IN_UPDATE_AMOUNT; 
      END IF;

    END IF;
  
  ELSE
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'Message type is unknown:' || IN_MSG_TYPE;

  END IF;

  -- only update count if count has not been updated.
  IF OUT_STATUS = 'Y' AND v_need_update = 'Y' THEN
    INSERT_UPDATE_INV_TRK_COUNT 
    (
      IN_PRODUCT_ID => v_product_id,
      IN_SHIP_DATE  => v_ship_date,
      IN_FTD_MSG_COUNT => v_ftd_count,
      IN_CAN_MSG_COUNT => v_can_count,
      IN_REJ_MSG_COUNT => v_rej_count,
      OUT_STATUS => OUT_STATUS,
      OUT_MESSAGE => OUT_MESSAGE
    );

    IF OUT_STATUS = 'Y' THEN
      update venus.venus
      set    inv_trk_count_date = sysdate,
             updated_on = sysdate
      where  venus_id = IN_VENUS_ID;
    END IF;

  END IF;


END UPDATE_INV_TRK_COUNT;


/******************************************************************************
INSERT_UPDATE_INV_TRK_COUNT

Description: This procedure is responsible for inserting a count record
for the product_id and ship_date.

******************************************************************************/
PROCEDURE INSERT_UPDATE_INV_TRK_COUNT
(
IN_PRODUCT_ID     IN INV_TRK_COUNT.PRODUCT_ID%TYPE,
IN_SHIP_DATE      IN INV_TRK_COUNT.SHIP_DATE%TYPE,
IN_FTD_MSG_COUNT  IN INV_TRK_COUNT.FTD_MSG_COUNT%TYPE,
IN_CAN_MSG_COUNT  IN INV_TRK_COUNT.CAN_MSG_COUNT%TYPE,
IN_REJ_MSG_COUNT  IN INV_TRK_COUNT.REJ_MSG_COUNT%TYPE,
OUT_STATUS       OUT VARCHAR2,
OUT_MESSAGE      OUT VARCHAR2
)
AS
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

  OUT_STATUS := 'Y';
  OUT_MESSAGE := ''; 

  insert into venus.inv_trk_count
  (
    product_id,
    ship_date,
    ftd_msg_count,
    can_msg_count,
    rej_msg_count
  )
  values
  (
    IN_PRODUCT_ID,
    IN_SHIP_DATE,
    IN_FTD_MSG_COUNT,
    IN_CAN_MSG_COUNT,
    IN_REJ_MSG_COUNT
  );
  commit;
  
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
     BEGIN
        update venus.inv_trk_count
        set    ftd_msg_count = ftd_msg_count + IN_FTD_MSG_COUNT
              ,can_msg_count = can_msg_count + IN_CAN_MSG_COUNT
              ,rej_msg_count = rej_msg_count + IN_REJ_MSG_COUNT
        where  product_id = IN_PRODUCT_ID
        and    ship_date = IN_SHIP_DATE;
        commit;

     EXCEPTION
        WHEN OTHERS THEN
           rollback;
           OUT_STATUS := 'N';
           OUT_MESSAGE := 'ERROR (upd) OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;
    
  WHEN OTHERS THEN
    rollback;
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR (ins) OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_UPDATE_INV_TRK_COUNT;


/******************************************************************************
UPDATE_INV_TRK_ASSIGNED_COUNT

Description: This procedure is responsible for updating product message count.
It calls the overloaded UPDATE_INV_TRK_ASSIGNED_COUNT procedure passing
product_id,vendor_id, and ship_date retrieved from the FTD venus record.

******************************************************************************/
PROCEDURE UPDATE_INV_TRK_ASSIGNED_COUNT
(
IN_UPDATE_AMOUNT    IN NUMBER,
IN_VENUS_ID         IN VENUS.VENUS_ID%TYPE,
IN_MSG_TYPE         IN VENUS.MSG_TYPE%TYPE,
OUT_STATUS         OUT VARCHAR2,
OUT_MESSAGE        OUT VARCHAR2
)
AS

v_ftd_count           inv_trk_assigned_count.ftd_msg_count%type := 0;
v_can_count           inv_trk_assigned_count.can_msg_count%type := 0;
v_rej_count           inv_trk_assigned_count.rej_msg_count%type := 0;
v_updated_date        venus.inv_trk_assigned_count_date%type;
v_ftd_updated_date    venus.inv_trk_assigned_count_date%type;
v_product_id          venus.product_id%type;
v_vendor_id           venus.vendor_id%type;
v_ship_date           venus.ship_date%type;
v_need_update         varchar2(1);

BEGIN

  v_need_update := 'N';
  OUT_STATUS := 'Y';
  OUT_MESSAGE := ''; 

  IF IN_MSG_TYPE = 'FTD' THEN
    select inv_trk_assigned_count_date, product_id, vendor_id, ship_date
    into   v_ftd_updated_date, v_product_id, v_vendor_id, v_ship_date
    from   venus.venus
    where  venus_id = IN_VENUS_ID;

    -- Make sure the FTD message has NOT updated count.
    IF v_ftd_updated_date IS NULL THEN
      v_need_update := 'Y';
      v_ftd_count := IN_UPDATE_AMOUNT; 
    END IF;

  ELSIF IN_MSG_TYPE in ('CAN', 'REJ') THEN
    select inv_trk_assigned_count_date
    into   v_updated_date
    from   venus.venus
    where  venus_id = IN_VENUS_ID;

    select inv_trk_assigned_count_date, product_id, vendor_id, ship_date
    into   v_ftd_updated_date, v_product_id, v_vendor_id, v_ship_date
    from   venus.venus v
    where  v.msg_type = 'FTD'
    and    v.venus_order_number = 
      (select venus_order_number
       from   venus.venus v2
       where  v2.venus_id = IN_VENUS_ID
      );

    -- Make sure the FTD message has updated count the the CAN or REJ has not updated count.
    IF v_ftd_updated_date IS NOT NULL and v_updated_date IS NULL THEN
      v_need_update := 'Y';

      IF IN_MSG_TYPE = 'CAN' THEN
        v_can_count := IN_UPDATE_AMOUNT; 
      ELSE
        v_rej_count := IN_UPDATE_AMOUNT; 
      END IF;

    END IF;

  ELSE
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'Message type is unknown:' || IN_MSG_TYPE;

  END IF;

  -- only update count if count has not been updated.
  IF OUT_STATUS = 'Y' AND v_need_update = 'Y' THEN
    ISRT_UPDT_INV_TRK_ASGN_COUNT 
    (
      IN_PRODUCT_ID => v_product_id,
      IN_VENDOR_ID => v_vendor_id,
      IN_SHIP_DATE  => v_ship_date,
      IN_FTD_MSG_COUNT => v_ftd_count,
      IN_CAN_MSG_COUNT => v_can_count,
      IN_REJ_MSG_COUNT => v_rej_count,
      OUT_STATUS => OUT_STATUS,
      OUT_MESSAGE => OUT_MESSAGE
    );

    IF OUT_STATUS = 'Y' THEN
      update venus.venus
      set    inv_trk_assigned_count_date = sysdate,
             updated_on = sysdate
      where  venus_id = IN_VENUS_ID;
    END IF;

  END IF;


END UPDATE_INV_TRK_ASSIGNED_COUNT;


/******************************************************************************
ISRT_UPDT_INV_TRK_ASGN_COUNT

Description: This procedure is responsible for inserting a count record
for the product_id, vendor_id, and ship_date.

******************************************************************************/
PROCEDURE ISRT_UPDT_INV_TRK_ASGN_COUNT
(
IN_PRODUCT_ID     IN INV_TRK_ASSIGNED_COUNT.PRODUCT_ID%TYPE,
IN_VENDOR_ID      IN INV_TRK_ASSIGNED_COUNT.VENDOR_ID%TYPE,
IN_SHIP_DATE      IN INV_TRK_ASSIGNED_COUNT.SHIP_DATE%TYPE,
IN_FTD_MSG_COUNT  IN INV_TRK_ASSIGNED_COUNT.FTD_MSG_COUNT%TYPE,
IN_CAN_MSG_COUNT  IN INV_TRK_ASSIGNED_COUNT.CAN_MSG_COUNT%TYPE,
IN_REJ_MSG_COUNT  IN INV_TRK_ASSIGNED_COUNT.REJ_MSG_COUNT%TYPE,
OUT_STATUS       OUT VARCHAR2,
OUT_MESSAGE      OUT VARCHAR2
)
AS
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

  OUT_STATUS := 'Y';
  OUT_MESSAGE := ''; 

  insert into venus.inv_trk_assigned_count
  (
    product_id,
    vendor_id,
    ship_date,
    ftd_msg_count,
    can_msg_count,
    rej_msg_count
  )
  values
  (
    IN_PRODUCT_ID,
    IN_VENDOR_ID,
    IN_SHIP_DATE,
    IN_FTD_MSG_COUNT,
    IN_CAN_MSG_COUNT,
    IN_REJ_MSG_COUNT
  );
  commit;

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
     BEGIN
        update venus.inv_trk_assigned_count
        set    ftd_msg_count = ftd_msg_count + IN_FTD_MSG_COUNT
              ,can_msg_count = can_msg_count + IN_CAN_MSG_COUNT
              ,rej_msg_count = rej_msg_count + IN_REJ_MSG_COUNT
        where  product_id = IN_PRODUCT_ID
        and    vendor_id = IN_VENDOR_ID
        and    ship_date = IN_SHIP_DATE;
        commit;

     EXCEPTION
        WHEN OTHERS THEN
           rollback;
           OUT_STATUS := 'N';
           OUT_MESSAGE := 'ERROR (upd) OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);        
     END;

  WHEN OTHERS THEN
     rollback;
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR (ins) OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END ISRT_UPDT_INV_TRK_ASGN_COUNT;

/******************************************************************************
REALLOCATE_INV_TRK_COUNT

Description: This procedure is responsible for reallocating inventory from 
original ship date to secondary ship date returned by ScanData
for the product_id passed in.

******************************************************************************/
PROCEDURE REALLOCATE_INV_TRK_COUNT
(
IN_PRODUCT_ID     	IN INV_TRK_COUNT.PRODUCT_ID%TYPE,
IN_ORIG_SHIP_DATE       IN INV_TRK_COUNT.SHIP_DATE%TYPE,
IN_SECONDARY_SHIP_DATE  IN INV_TRK_COUNT.SHIP_DATE%TYPE,
OUT_STATUS       OUT VARCHAR2,
OUT_MESSAGE      OUT VARCHAR2
)
AS
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

  OUT_STATUS := 'Y';
  OUT_MESSAGE := ''; 
  
  update venus.inv_trk_count
        set    ftd_msg_count = ftd_msg_count - 1
        where  product_id = IN_PRODUCT_ID
        and    ship_date = IN_ORIG_SHIP_DATE;
  commit;  

  insert into venus.inv_trk_count
  (
    product_id,
    ship_date,
    ftd_msg_count,
    can_msg_count,
    rej_msg_count
  )
  values
  (
    IN_PRODUCT_ID,
    IN_SECONDARY_SHIP_DATE,
    1,
    0,
    0
  );
  commit;
  
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
     BEGIN
        update venus.inv_trk_count
        set    ftd_msg_count = ftd_msg_count + 1
        where  product_id = IN_PRODUCT_ID
        and    ship_date = IN_SECONDARY_SHIP_DATE;
        commit;

     EXCEPTION
        WHEN OTHERS THEN
           rollback;
           OUT_STATUS := 'N';
           OUT_MESSAGE := 'ERROR (upd) OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;
    
  WHEN OTHERS THEN
    rollback;
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR (ins) OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END REALLOCATE_INV_TRK_COUNT;



END INV_TRK_PKG;
/