CREATE OR REPLACE TRIGGER venus.trg_venus_biou
 BEFORE INSERT OR UPDATE OF TRACKING_NUMBER, PRINTED, SHIPPED, CANCELLED, REJECTED
 ON VENUS.VENUS
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
 
BEGIN

   IF INSERTING OR UPDATING THEN
      -- populate the SDS_STATUS column
      IF :NEW.msg_type = 'FTD' AND :NEW.shipping_system IN ('SDS','FTD WEST') THEN
         IF :NEW.rejected IS NOT NULL THEN
            :NEW.sds_status := 'REJECTED';
         ELSIF :NEW.cancelled IS NOT NULL THEN
            :NEW.sds_status := 'CANCELLED';
         ELSIF :NEW.shipped IS NOT NULL THEN
            :NEW.sds_status := 'SHIPPED';
         ELSIF :NEW.printed IS NOT NULL THEN
            :NEW.sds_status := 'PRINTED';
         ELSIF :NEW.tracking_number IS NOT NULL THEN
            :NEW.sds_status := 'AVAILABLE';
         ELSE 
            :NEW.sds_status := 'NEW';
         END IF;
      END IF;
   END IF;
   
EXCEPTION
   
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In VENUS.TRG_VENUS_BIOU: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_venus_biou;

/