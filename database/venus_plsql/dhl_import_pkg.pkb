CREATE OR REPLACE
PACKAGE BODY venus.DHL_IMPORT_PKG
AS

PROCEDURE TRUNC_STG_CARRIER_EXCL_ZIPS

AS
 /*-----------------------------------------------------------------------------
 Description:
         This stored procedure truncates the stage_carrier_excluded_zip table.

 Input:

 Output:
         status
         error message

-----------------------------------------------------------------------------*/
 OUT_STATUS 		    VARCHAR2(10);
 OUT_ERROR_MESSAGE 	VARCHAR2(400);

 BEGIN

 EXECUTE IMMEDIATE 'TRUNCATE TABLE VENUS.STAGE_CARRIER_EXCLUDED_ZIPS';

 OUT_STATUS := 'Y';

 COMMIT;

 DBMS_OUTPUT.PUT_LINE('Truncate table stage_carrier_excluded_zips successful.');

 EXCEPTION WHEN OTHERS THEN
   BEGIN
     ROLLBACK;
     out_status := 'N';
     out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     dbms_output.put_line('Truncate table stage_carrier_excluded_zips Failed. Reason : ' || out_error_message);
  END;

  END TRUNC_STG_CARRIER_EXCL_ZIPS;

  PROCEDURE UPDATE_CARRIER_UPDATE_ZIPS

  AS
  /*-----------------------------------------------------------------------------
   Description:
           This stored procedure deletes carrier_update_zips where carrier is DHL and carrier provided flag is Y.
           It then reads out of stage carrier excluded zip table and updates the carrier excluded zips table.

   Input:

   Output:
           status
           error message

-----------------------------------------------------------------------------*/
OUT_STATUS 		    VARCHAR2(10);
OUT_ERROR_MESSAGE 	VARCHAR2(400);
V_ZIP_CODE 	CARRIER_EXCLUDED_ZIPS.ZIP_CODE%TYPE;

CURSOR RD_STG_CARR_EXCL_ZIPS_CUR IS
	SELECT lpad(ZIP_CODE,5,'0') FROM STAGE_CARRIER_EXCLUDED_ZIPS WHERE
	CARRIER_ID='DHL';

BEGIN

 DELETE CARRIER_EXCLUDED_ZIPS
 WHERE
 CARRIER_ID='DHL' AND CARRIER_PROVIDED_FLAG = 'Y';

 OPEN RD_STG_CARR_EXCL_ZIPS_CUR;
 LOOP

   FETCH RD_STG_CARR_EXCL_ZIPS_CUR INTO V_ZIP_CODE;
   EXIT WHEN RD_STG_CARR_EXCL_ZIPS_CUR%NOTFOUND;

   INSERT INTO CARRIER_EXCLUDED_ZIPS
   	(CARRIER_ID,ZIP_CODE,CARRIER_PROVIDED_FLAG,CREATED_ON,UPDATED_ON)
   VALUES
   	('DHL',V_ZIP_CODE,'Y',SYSDATE,TO_CHAR(SYSDATE,'DD-MON-YY'));

 END LOOP;

 CLOSE RD_STG_CARR_EXCL_ZIPS_CUR;

 COMMIT;

 DBMS_OUTPUT.PUT_LINE('Update carrier_excluded_zips successful.');

 EXCEPTION WHEN OTHERS THEN
   BEGIN
     ROLLBACK;
     out_status := 'N';
     out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     dbms_output.put_line('Update carrier_excluded_zips Failed. Reason : ' || out_error_message);
  END;

  END UPDATE_CARRIER_UPDATE_ZIPS;

END DHL_IMPORT_PKG;
.
/
