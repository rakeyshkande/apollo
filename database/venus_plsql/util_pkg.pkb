CREATE OR REPLACE
PACKAGE BODY venus.UTIL_PKG
AS

  FUNCTION GET_DOCUMENT_SEQUENCE
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
     Returns the next document sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT venus_document_sequence.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_DOCUMENT_SEQUENCE;


  FUNCTION GET_BATCH_SEQUENCE
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
     Returns the next batch sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT venus_batch_sequence.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_BATCH_SEQUENCE;


  FUNCTION GET_NEXT_VENUS_ORDER_NUMBER_SQ
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
    Returns the next venus order number sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT venus_order_number_sq.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_NEXT_VENUS_ORDER_NUMBER_SQ;


  FUNCTION GET_NEXT_WINE_ORDER_NUMBER_SQ
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
    Returns the next wine order number sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT wine_order_number_sq.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_NEXT_WINE_ORDER_NUMBER_SQ;

  FUNCTION GET_NEXT_SAMS_ORDER_NUMBER_SQ
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
    Returns the next sams order number sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT sams_order_number_sq.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_NEXT_SAMS_ORDER_NUMBER_SQ;

  FUNCTION GET_NEXT_PM_ORDER_NUMBER_SQ
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
    Returns the next personalization mall order number sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT pm_order_number_sq.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_NEXT_PM_ORDER_NUMBER_SQ;
  
  FUNCTION GET_NEXT_PC_ORDER_NUMBER_SQ
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
    Returns the next personal creations order number sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT pc_order_number_sq.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_NEXT_PC_ORDER_NUMBER_SQ;
  
  FUNCTION GET_NEXT_WEST_ORDER_NUMBER_SQ
  RETURN NUMBER
  IS
  /*------------------------------------------------------------------------------
  Description:
    Returns the next ftd west order number sequence number.
  ------------------------------------------------------------------------------*/
  v_seq_number NUMBER;

  BEGIN
     SELECT west_order_number_sq.NEXTVAL INTO v_seq_number FROM DUAL;
     RETURN v_seq_number;
  END GET_NEXT_WEST_ORDER_NUMBER_SQ;

END UTIL_PKG;
.
/
