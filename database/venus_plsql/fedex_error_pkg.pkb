CREATE OR REPLACE
PACKAGE BODY venus.FEDEX_ERROR_PKG
AS

 FUNCTION GET_FEDEX_ERROR
 (
  IN_ERROR_CODE IN VARCHAR2
 )
 RETURN VARCHAR2
 IS
 /*-----------------------------------------------------------------------------
 Description:
         This stored procedure retrieves the description from table
         fedex_no_page_errors for a particular error_code.

 Input:
         error_code

 Output:
         description

 -----------------------------------------------------------------------------*/
 out_description fedex_no_page_errors.description%TYPE;

 BEGIN

   BEGIN
     SELECT description INTO out_description
       FROM fedex_no_page_errors
       WHERE error_code = in_error_code;

     EXCEPTION WHEN NO_DATA_FOUND THEN out_description := NULL;
   END;

   RETURN out_description;

 END GET_FEDEX_ERROR;

END FEDEX_ERROR_PKG;
.
/
