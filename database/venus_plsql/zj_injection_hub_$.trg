CREATE OR REPLACE
TRIGGER venus.zj_injection_hub_$
AFTER INSERT OR UPDATE OR DELETE ON venus.zj_injection_hub 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO venus.zj_injection_hub$ (
         INJECTION_HUB_ID,
         CITY_NAME,
         STATE_MASTER_ID,
         CARRIER_ID,
         ADDRESS_DESC,
         ZIP_CODE,
         SDS_ACCOUNT_NUM,
         SHIP_POINT_ID,
         CREATED_BY,
         CREATED_ON,
         UPDATED_BY,
         UPDATED_ON,      
         operation$, timestamp$
      ) VALUES (
         :NEW.INJECTION_HUB_ID,
         :NEW.CITY_NAME,
         :NEW.STATE_MASTER_ID,
         :NEW.CARRIER_ID,
         :NEW.ADDRESS_DESC,
         :NEW.ZIP_CODE,
         :NEW.SDS_ACCOUNT_NUM,
         :NEW.SHIP_POINT_ID,
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO venus.zj_injection_hub$ (
         INJECTION_HUB_ID,
         CITY_NAME,
         STATE_MASTER_ID,
         CARRIER_ID,
         ADDRESS_DESC,
         ZIP_CODE,
         SDS_ACCOUNT_NUM,
         SHIP_POINT_ID,
         CREATED_BY,
         CREATED_ON,
         UPDATED_BY,
         UPDATED_ON,      
         operation$, timestamp$
      ) VALUES (
         :OLD.INJECTION_HUB_ID,
         :OLD.CITY_NAME,
         :OLD.STATE_MASTER_ID,
         :OLD.CARRIER_ID,
         :OLD.ADDRESS_DESC,
         :OLD.ZIP_CODE,
         :OLD.SDS_ACCOUNT_NUM,
         :OLD.SHIP_POINT_ID,
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',SYSDATE);

      INSERT INTO venus.zj_injection_hub$ (
         INJECTION_HUB_ID,
         CITY_NAME,
         STATE_MASTER_ID,
         CARRIER_ID,
         ADDRESS_DESC,
         ZIP_CODE,
         SDS_ACCOUNT_NUM,
         SHIP_POINT_ID,
         CREATED_BY,
         CREATED_ON,
         UPDATED_BY,
         UPDATED_ON,      
         operation$, timestamp$
      ) VALUES (
         :NEW.INJECTION_HUB_ID,
         :NEW.CITY_NAME,
         :NEW.STATE_MASTER_ID,
         :NEW.CARRIER_ID,
         :NEW.ADDRESS_DESC,
         :NEW.ZIP_CODE,
         :NEW.SDS_ACCOUNT_NUM,
         :NEW.SHIP_POINT_ID,
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO venus.zj_injection_hub$ (
         INJECTION_HUB_ID,
         CITY_NAME,
         STATE_MASTER_ID,
         CARRIER_ID,
         ADDRESS_DESC,
         ZIP_CODE,
         SDS_ACCOUNT_NUM,
         SHIP_POINT_ID,
         CREATED_BY,
         CREATED_ON,
         UPDATED_BY,
         UPDATED_ON,      
         operation$, timestamp$
      ) VALUES (
         :OLD.INJECTION_HUB_ID,
         :OLD.CITY_NAME,
         :OLD.STATE_MASTER_ID,
         :OLD.CARRIER_ID,
         :OLD.ADDRESS_DESC,
         :OLD.ZIP_CODE,
         :OLD.SDS_ACCOUNT_NUM,
         :OLD.SHIP_POINT_ID,
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',SYSDATE);

   END IF;

END;
/
