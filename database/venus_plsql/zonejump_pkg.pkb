CREATE OR REPLACE PACKAGE BODY VENUS.ZONEJUMP_PKG as

/******************************************************************************
                          GET_INJECTION_HUB_INFO

Description: This procedure is responsible for retrieving box info

******************************************************************************/
PROCEDURE GET_INJECTION_HUB_INFO
(
  IN_INJECTION_HUB_ID           IN ZJ_INJECTION_HUB.INJECTION_HUB_ID%TYPE,
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS
BEGIN

OPEN OUT_CUR FOR
        SELECT ih.INJECTION_HUB_ID,
               ih.CITY_NAME,
               ih.STATE_MASTER_ID,
               ih.CARRIER_ID,
               ih.ADDRESS_DESC,
               ih.ZIP_CODE,
               ih.SDS_ACCOUNT_NUM,
               ih.SHIP_POINT_ID,
               ih.CREATED_BY,
               ih.CREATED_ON,
               ih.UPDATED_BY,
               ih.UPDATED_ON
          FROM VENUS.ZJ_INJECTION_HUB ih
         WHERE ( (IN_INJECTION_HUB_ID IS NULL) OR (ih.INJECTION_HUB_ID = IN_INJECTION_HUB_ID) );

END GET_INJECTION_HUB_INFO;


/******************************************************************************
                          INSERT_INJECTION_HUB_INFO

Description: This procedure is responsible for inserting hub info

******************************************************************************/
PROCEDURE INSERT_INJECTION_HUB_INFO
(
  IN_CITY_NAME                  IN ZJ_INJECTION_HUB.CITY_NAME%TYPE,
  IN_STATE_MASTER_ID            IN ZJ_INJECTION_HUB.STATE_MASTER_ID%TYPE,
  IN_CARRIER_ID                 IN ZJ_INJECTION_HUB.CARRIER_ID%TYPE,
  IN_ADDRESS_DESC               IN ZJ_INJECTION_HUB.ADDRESS_DESC%TYPE,
  IN_ZIP_CODE                   IN ZJ_INJECTION_HUB.ZIP_CODE%TYPE,
  IN_SDS_ACCOUNT_NUM            IN ZJ_INJECTION_HUB.SDS_ACCOUNT_NUM%TYPE,
  IN_SHIP_POINT_ID              IN ZJ_INJECTION_HUB.SHIP_POINT_ID%TYPE,
  IN_CREATED_BY                 IN ZJ_INJECTION_HUB.CREATED_BY%TYPE,
  IN_UPDATED_BY                 IN ZJ_INJECTION_HUB.UPDATED_BY%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS
BEGIN

      INSERT INTO VENUS.ZJ_INJECTION_HUB
      (
          INJECTION_HUB_ID,
          CITY_NAME,
          STATE_MASTER_ID,
          CARRIER_ID,
          ADDRESS_DESC,
          ZIP_CODE,
          SDS_ACCOUNT_NUM,
          SHIP_POINT_ID,
          CREATED_BY,
          CREATED_ON,
          UPDATED_BY,
          UPDATED_ON
      )
      VALUES
      (
          key.keygen ('ZJ_INJECTION_HUB'),
          IN_CITY_NAME,
          IN_STATE_MASTER_ID,
          IN_CARRIER_ID,
          IN_ADDRESS_DESC,
          IN_ZIP_CODE,
          IN_SDS_ACCOUNT_NUM,
          IN_SHIP_POINT_ID,
          IN_CREATED_BY,
          SYSDATE,
          IN_UPDATED_BY,
          SYSDATE
      );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_INJECTION_HUB_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_INJECTION_HUB_INFO;


/******************************************************************************
                          UPDATE_INJECTION_HUB_INFO

Description: This procedure is responsible for updating hub info

******************************************************************************/
PROCEDURE UPDATE_INJECTION_HUB_INFO
(
  IN_INJECTION_HUB_ID           IN ZJ_INJECTION_HUB.INJECTION_HUB_ID%TYPE,
  IN_CITY_NAME                  IN ZJ_INJECTION_HUB.CITY_NAME%TYPE,
  IN_STATE_MASTER_ID            IN ZJ_INJECTION_HUB.STATE_MASTER_ID%TYPE,
  IN_CARRIER_ID                 IN ZJ_INJECTION_HUB.CARRIER_ID%TYPE,
  IN_ADDRESS_DESC               IN ZJ_INJECTION_HUB.ADDRESS_DESC%TYPE,
  IN_ZIP_CODE                   IN ZJ_INJECTION_HUB.ZIP_CODE%TYPE,
  IN_SDS_ACCOUNT_NUM            IN ZJ_INJECTION_HUB.SDS_ACCOUNT_NUM%TYPE,
  IN_SHIP_POINT_ID              IN ZJ_INJECTION_HUB.SHIP_POINT_ID%TYPE,
  IN_UPDATED_BY                 IN ZJ_INJECTION_HUB.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS
BEGIN

      UPDATE VENUS.ZJ_INJECTION_HUB
         SET CITY_NAME          = IN_CITY_NAME,
             STATE_MASTER_ID    = IN_STATE_MASTER_ID,
             CARRIER_ID         = IN_CARRIER_ID,
             ADDRESS_DESC       = IN_ADDRESS_DESC,
             ZIP_CODE           = IN_ZIP_CODE,
             SDS_ACCOUNT_NUM    = IN_SDS_ACCOUNT_NUM,
             SHIP_POINT_ID      = IN_SHIP_POINT_ID,
             UPDATED_BY         = IN_UPDATED_BY,
             UPDATED_ON         = SYSDATE
       WHERE INJECTION_HUB_ID   = IN_INJECTION_HUB_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_INJECTION_HUB_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_INJECTION_HUB_INFO;


/******************************************************************************
                          GET_TRIP_STATUS

Description: This procedure is responsible for retrieving valid status for trips

******************************************************************************/
PROCEDURE GET_TRIP_STATUS
(
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

BEGIN
OPEN OUT_CUR FOR
        SELECT TRIP_STATUS_CODE,
               TRIP_STATUS_DESC,
               CREATED_BY,
               CREATED_ON,
               UPDATED_BY,
               UPDATED_ON
          FROM VENUS.ZJ_TRIP_STATUS;

END GET_TRIP_STATUS;


/******************************************************************************
                          GET_TRIP_VENDOR_STATUS

Description: This procedure is responsible for retrieving valid trip/vendor status

******************************************************************************/
PROCEDURE GET_TRIP_VENDOR_STATUS
(
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

BEGIN
OPEN OUT_CUR FOR
        SELECT TRIP_VENDOR_STATUS_CODE,
               TRIP_VENDOR_STATUS_DESC,
               CREATED_BY,
               CREATED_ON,
               UPDATED_BY,
               UPDATED_ON
         FROM  VENUS.ZJ_TRIP_VENDOR_STATUS;

END GET_TRIP_VENDOR_STATUS;


/******************************************************************************
                          GET_TRIP_VENDOR_INFO

Description: This procedure is responsible for retrieving info for ALL trips

******************************************************************************/
PROCEDURE GET_TRIP_VENDOR_INFO
(
  IN_TRIP_STATUS_CODE           IN ZJ_TRIP.TRIP_STATUS_CODE%TYPE,
  OUT_TRIP_VENDOR_CUR          OUT TYPES.REF_CURSOR,
  OUT_TRIP_STATUS_CUR          OUT TYPES.REF_CURSOR
)
AS

-- variables
v_sql                  varchar2 (4000) := '';
v_select_from          varchar2 (4000) := '';
v_where                varchar2 (4000) := '';
v_and                  varchar2 (4000) := '';

BEGIN

CASE

  WHEN IN_TRIP_STATUS_CODE = 'All' THEN
    v_select_from  := ' select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := '  where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '    and z1t.created_on > sysdate - 90 ';

    v_sql          := v_select_from || v_where || v_and;

  WHEN IN_TRIP_STATUS_CODE is null or IN_TRIP_STATUS_CODE = 'Accepting Orders' THEN
    v_select_from  := 'select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := ' where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '   and z1t.TRIP_STATUS_CODE = ''A'' ';
    v_and := v_and || '   and z1tv.TRIP_VENDOR_STATUS_CODE != ''X'' ';
    v_and := v_and || '   and ( ( ( (z1tv.ORDER_CUTOFF_TIME is null) or ( z1tv.ORDER_CUTOFF_TIME >= ( select to_char(sysdate, ''hh24mi'') from dual) ) )  and trunc(z1t.departure_date) = trunc(sysdate) )';
    v_and := v_and || '         OR trunc(z1t.departure_date) > trunc(sysdate) ';
    v_and := v_and || '       ) ';
    v_and := v_and || '   and z1t.created_on > sysdate - 90 ';

    v_sql          := v_select_from || v_where || v_and;

  WHEN IN_TRIP_STATUS_CODE = 'Cutoff Reached' THEN
/* Find the records where cutoff has been reached */
    v_select_from  := 'select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := ' where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '   and z1t.TRIP_STATUS_CODE = ''A'' ';
    v_and := v_and || '   and ( ';
    v_and := v_and || '          (z1tv.ORDER_CUTOFF_TIME < ( select to_char(sysdate, ''hh24mi'') from dual) and trunc(z1t.departure_date) = trunc(sysdate) ) ';
    v_and := v_and || '       OR (trunc(z1t.departure_date) < trunc(sysdate) ) ';
    v_and := v_and || '       ) ';
    v_and := v_and || '   and z1t.created_on > sysdate - 90 ';

/* MINUS */
    v_sql          := v_select_from || v_where || v_and;
    v_sql := v_sql || ' MINUS ';

/* Find the records where cutoff has not been reached -- this is to get rid of any trips where 1 vendor is
passed cutoff but another can still accept an order*/
    v_select_from  := 'select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := ' where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '   and z1t.TRIP_STATUS_CODE = ''A'' ';
    v_and := v_and || '   and z1tv.TRIP_VENDOR_STATUS_CODE != ''X'' ';
    v_and := v_and || '   and ( ( ( (z1tv.ORDER_CUTOFF_TIME is null) or ( z1tv.ORDER_CUTOFF_TIME >= ( select to_char(sysdate, ''hh24mi'') from dual) ) )  and trunc(z1t.departure_date) = trunc(sysdate) )';
    v_and := v_and || '         OR trunc(z1t.departure_date) > trunc(sysdate) ';
    v_and := v_and || '       ) ';
    v_and := v_and || '   and z1t.created_on > sysdate - 90 ';

    v_sql          := v_sql || v_select_from || v_where || v_and;

  WHEN IN_TRIP_STATUS_CODE = 'F' THEN
    v_select_from  := 'select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := ' where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '   and z1t.TRIP_STATUS_CODE = ''F'' ';
    v_and := v_and || '   and z1t.created_on > sysdate - 90 ';

    v_sql          := v_select_from || v_where || v_and;

  WHEN IN_TRIP_STATUS_CODE = 'X' THEN
    v_select_from  := 'select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := ' where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '   and z1t.TRIP_STATUS_CODE = ''X'' ';
    v_and := v_and || '   and z1t.created_on > sysdate - 90 ';

    v_sql          := v_select_from || v_where || v_and;

  WHEN IN_TRIP_STATUS_CODE = 'C' THEN
    v_select_from  := 'select distinct z1t.trip_id from VENUS.ZJ_TRIP z1t, VENUS.ZJ_TRIP_VENDOR z1tv ';
    v_where        := ' where z1t.TRIP_ID = z1tv.TRIP_ID ';
    v_and          := '   and z1t.TRIP_STATUS_CODE = ''C'' ';
    v_and := v_and || '   and z1t.created_on > sysdate - 90 ';

    v_sql          := v_select_from || v_where || v_and;

END CASE;


OPEN OUT_TRIP_VENDOR_CUR FOR
       'SELECT t1.TRIP_ID,
               t1.TRIP_ORIGINATION_DESC,
               t1.INJECTION_HUB_ID,
               t1.DELIVERY_DATE,
               t1.DEPARTURE_DATE,
               t1.TRIP_STATUS_CODE,
               t1.SORT_CODE_SEQ,
               t1.SORT_CODE_STATE_MASTER_ID,
               t1.SORT_CODE_DATE_MMDD,
               t1.ZONE_JUMP_TRAILER_NUMBER,
               t1.TOTAL_PLT_QTY,
               t1.FTD_PLT_QTY,
               t1.THIRD_PARTY_PLT_QTY,
               t1.FTD_PLT_USED_QTY,
               t1.CREATED_BY                       T_CREATED_BY,
               t1.CREATED_ON                       T_CREATED_ON,
               t1.UPDATED_BY                       T_UPDATED_BY,
               t1.UPDATED_ON                       T_UPDATED_ON,
               (select max(ORDER_CUTOFF_TIME) from VENUS.ZJ_TRIP_VENDOR tv2 where tv2.TRIP_ID = t1.TRIP_ID and tv2.TRIP_VENDOR_STATUS_CODE != ''X'' ) MAX_ORDER_CUTOFF_TIME,
               (select sum(tv4.FULL_PLT_ORDER_QTY) + sum(tv4.PRTL_PLT_ORDER_QTY) 
                  from VENUS.ZJ_TRIP_VENDOR tv4 
                 where tv4.trip_id = t1.TRIP_ID
               )  TOTAL_ORDER_ON_TRUCK,
               tv1.VENDOR_ID,
               vm.VENDOR_NAME,
               vm.ADDRESS1,
               vm.CITY,
               vm.STATE,
               tv1.TRIP_VENDOR_STATUS_CODE,
               tv1.ORDER_CUTOFF_TIME,
               tv1.PLT_CUBIC_INCH_ALLOWED_QTY,
               tv1.FULL_PLT_QTY,
               tv1.FULL_PLT_ORDER_QTY,
               tv1.PRTL_PLT_IN_USE_FLAG,
               tv1.PRTL_PLT_ORDER_QTY,
               tv1.PRTL_PLT_CUBIC_IN_USED_QTY,
               tv1.CREATED_BY                      TV_CREATED_BY,
               tv1.CREATED_ON                      TV_CREATED_ON,
               tv1.UPDATED_BY                      TV_UPDATED_BY,
               tv1.UPDATED_ON                      TV_UPDATED_ON,
               (SELECT decode (count (1), 0, ''N'', ''Y'')
                  FROM VENUS.VENUS v, VENUS.ZJ_TRIP_VENDOR_ORDER tvo, VENUS.ZJ_TRIP_VENDOR tv3
                 WHERE tvo.TRIP_ID = tv1.TRIP_ID
                   AND tv3.TRIP_ID = tv1.TRIP_ID
                   AND tvo.TRIP_ID = tv3.TRIP_ID
                   AND tvo.VENDOR_ID = tv1.VENDOR_ID
                   AND tv3.VENDOR_ID = tv1.VENDOR_ID
                   AND tvo.VENUS_ID = v.VENUS_ID
                   AND tv3.TRIP_VENDOR_STATUS_CODE = ''X''
                   AND v.SDS_STATUS = ''PRINTED''
               )                                  TV_REJECT_PRINTED_EXIST,
               ih.CITY_NAME,
               ih.STATE_MASTER_ID,
               ih.CARRIER_ID,
               ih.ADDRESS_DESC,
               ih.ZIP_CODE,
               ih.SDS_ACCOUNT_NUM,
               ih.CREATED_BY                       IH_CREATED_BY,
               ih.CREATED_ON                       IH_CREATED_ON,
               ih.UPDATED_BY                       IH_UPDATED_BY,
               ih.UPDATED_ON                       IH_UPDATED_ON,
               t1.DAYS_IN_TRANSIT_QTY
          FROM VENUS.ZJ_TRIP t1, VENUS.ZJ_TRIP_VENDOR tv1, VENUS.ZJ_INJECTION_HUB ih, FTD_APPS.VENDOR_MASTER vm
         WHERE t1.TRIP_ID = tv1.TRIP_ID
           AND tv1.VENDOR_ID = vm.VENDOR_ID
           AND t1.TRIP_ID IN ('       ||
                                v_sql ||
                            ')
           AND t1.INJECTION_HUB_ID = ih.INJECTION_HUB_ID
      ORDER BY t1.DELIVERY_DATE, t1.TRIP_ID, tv1.VENDOR_ID';

GET_TRIP_STATUS (OUT_TRIP_STATUS_CUR);


END GET_TRIP_VENDOR_INFO;


/******************************************************************************
                          GET_TRIP_VENDOR_ORDER_INFO

Description: This procedure is responsible for retrieving info for ALL trips

******************************************************************************/
PROCEDURE GET_TRIP_VENDOR_ORDER_INFO
(
  IN_TRIP_ID                            IN ZJ_TRIP.TRIP_ID%TYPE,
  IN_SORT_BY                            IN VARCHAR2,
  OUT_TRIP_CUR                         OUT TYPES.REF_CURSOR,
  OUT_INJECTION_HUB_CUR                OUT TYPES.REF_CURSOR,
  OUT_TRIP_VENDOR_ORDER_CUR            OUT TYPES.REF_CURSOR,
  OUT_TRIP_STATUS_CUR                  OUT TYPES.REF_CURSOR,
  OUT_TRIP_VENDOR_STATUS_CUR           OUT TYPES.REF_CURSOR
)
AS
BEGIN

IF IN_TRIP_ID IS NOT NULL THEN
  OPEN OUT_TRIP_CUR FOR
        SELECT t.TRIP_ID,
               t.TRIP_ORIGINATION_DESC,
               t.INJECTION_HUB_ID,
               t.DELIVERY_DATE,
               t.DEPARTURE_DATE,
               t.TRIP_STATUS_CODE,
               t.SORT_CODE_SEQ,
               t.SORT_CODE_STATE_MASTER_ID,
               t.SORT_CODE_DATE_MMDD,
               t.ZONE_JUMP_TRAILER_NUMBER,
               t.TOTAL_PLT_QTY,
               t.FTD_PLT_QTY,
               t.THIRD_PARTY_PLT_QTY,
               t.FTD_PLT_USED_QTY,
               (SELECT decode (count (1), 0, 'N', 'Y')
                  FROM VENUS.ZJ_TRIP_VENDOR_ORDER tvo, VENUS.VENUS v
                 WHERE tvo.VENUS_ID = v.VENUS_ID
                   AND v.SDS_STATUS = 'PRINTED'
                   AND tvo.ORDER_FILLED_FLAG = 'Y'
                   AND tvo.TRIP_ID = t.TRIP_ID
               )                                   TV_PRINTED_EXIST,
               t.CREATED_BY                        T_CREATED_BY,
               t.CREATED_ON                        T_CREATED_ON,
               t.UPDATED_BY                        T_UPDATED_BY,
               t.UPDATED_ON                        T_UPDATED_ON,
               (select max(ORDER_CUTOFF_TIME) from VENUS.ZJ_TRIP_VENDOR tv where tv.TRIP_ID = t.TRIP_ID and tv.TRIP_VENDOR_STATUS_CODE != 'X' ) MAX_ORDER_CUTOFF_TIME,
               (select sum(tv.FULL_PLT_ORDER_QTY) + sum(tv.PRTL_PLT_ORDER_QTY) 
                  from VENUS.ZJ_TRIP_VENDOR tv 
                 where tv.trip_id = t.TRIP_ID
               )  TOTAL_ORDER_ON_TRUCK,
               t.DAYS_IN_TRANSIT_QTY
         FROM  VENUS.ZJ_TRIP t
        WHERE  t.TRIP_ID = IN_TRIP_ID;

  OPEN OUT_TRIP_VENDOR_ORDER_CUR FOR
        'SELECT vm.VENDOR_ID,
               vm.VENDOR_NAME,
               vm.ADDRESS1,
               vm.CITY,
               vm.STATE,
               temp.TRIP_ID,
               temp.TV_TRIP_VENDOR_STATUS_CODE,
               temp.TV_ORDER_CUTOFF_TIME,
               temp.TV_PLT_CUBIC_INCH_ALLOWED_QTY,
               temp.TV_FULL_PLT_QTY,
               temp.TV_FULL_PLT_ORDER_QTY,
               temp.TV_PRTL_PLT_IN_USE_FLAG,
               temp.TV_PRTL_PLT_ORDER_QTY,
               temp.TV_PRTL_PLT_CUBIC_IN_USED_QTY,
               temp.TV_CREATED_BY,
               temp.TV_CREATED_ON,
               temp.TV_UPDATED_BY,
               temp.TV_UPDATED_ON,
               (SELECT decode (count (1), 0, ''N'', ''Y'')
                  FROM FTD_APPS.VENDOR_MASTER vm1
                 WHERE vm1.VENDOR_ID = temp.vendor_id
                   AND temp.TVO_SDS_STATUS = ''PRINTED''
                   AND temp.TV_TRIP_VENDOR_STATUS_CODE = ''X''
               )                                   TVO_REJECT_PRINTED_EXIST,
               temp.TVO_ORDER_DETAIL_ID,
               temp.TVO_VENUS_ID,
               temp.TVO_VENUS_ORDER_NUMBER,
               temp.TVO_CREATED_BY,
               temp.TVO_CREATED_ON,
               temp.TVO_UPDATED_BY,
               temp.TVO_UPDATED_ON,
               temp.TVO_SDS_STATUS
          FROM FTD_APPS.VENDOR_MASTER vm,
               (
                  SELECT tv.TRIP_ID                       TRIP_ID,
                         tv.VENDOR_ID                     VENDOR_ID,
                         tv.TRIP_VENDOR_STATUS_CODE       TV_TRIP_VENDOR_STATUS_CODE,
                         tv.ORDER_CUTOFF_TIME             TV_ORDER_CUTOFF_TIME,
                         tv.PLT_CUBIC_INCH_ALLOWED_QTY    TV_PLT_CUBIC_INCH_ALLOWED_QTY,
                         tv.FULL_PLT_QTY                  TV_FULL_PLT_QTY,
                         tv.FULL_PLT_ORDER_QTY            TV_FULL_PLT_ORDER_QTY,
                         tv.PRTL_PLT_IN_USE_FLAG          TV_PRTL_PLT_IN_USE_FLAG,
                         tv.PRTL_PLT_ORDER_QTY            TV_PRTL_PLT_ORDER_QTY,
                         tv.PRTL_PLT_CUBIC_IN_USED_QTY    TV_PRTL_PLT_CUBIC_IN_USED_QTY,
                         tv.CREATED_BY                    TV_CREATED_BY,
                         tv.CREATED_ON                    TV_CREATED_ON,
                         tv.UPDATED_BY                    TV_UPDATED_BY,
                         tv.UPDATED_ON                    TV_UPDATED_ON,
                         tvo.ORDER_DETAIL_ID              TVO_ORDER_DETAIL_ID,
                         tvo.VENUS_ID                     TVO_VENUS_ID,
                         v.VENUS_ORDER_NUMBER             TVO_VENUS_ORDER_NUMBER,
                         tvo.CREATED_BY                   TVO_CREATED_BY,
                         tvo.CREATED_ON                   TVO_CREATED_ON,
                         tvo.UPDATED_BY                   TVO_UPDATED_BY,
                         tvo.UPDATED_ON                   TVO_UPDATED_ON,
                         v.SDS_STATUS                     TVO_SDS_STATUS
                    FROM VENUS.ZJ_TRIP_VENDOR tv, VENUS.ZJ_TRIP_VENDOR_ORDER tvo, VENUS.VENUS v
                   WHERE tv.VENDOR_ID = tvo.VENDOR_ID(+)
                     AND tv.TRIP_ID = tvo.TRIP_ID(+)
                     AND tvo.VENUS_ID = v.VENUS_ID(+)
                     AND tv.TRIP_ID = ' || IN_TRIP_ID || '
               ) temp
         WHERE vm.VENDOR_ID = temp.VENDOR_ID(+)
           AND vm.ACTIVE = ''Y''
           AND vm.ZONE_JUMP_ELIGIBLE_FLAG = ''Y''
      ORDER BY ' || IN_SORT_BY;

ELSE
  /*Return an empty cursor*/
  OPEN OUT_TRIP_CUR FOR SELECT * FROM DUAL WHERE 1 = 2;

  OPEN OUT_TRIP_VENDOR_ORDER_CUR FOR
        'SELECT vm.VENDOR_ID,
               vm.VENDOR_NAME,
               vm.ADDRESS1,
               vm.CITY,
               vm.STATE
          FROM FTD_APPS.VENDOR_MASTER vm
         WHERE vm.ACTIVE = ''Y''
           AND vm.ZONE_JUMP_ELIGIBLE_FLAG = ''Y''
      ORDER BY ' || IN_SORT_BY ;

END IF;

GET_INJECTION_HUB_INFO (NULL, OUT_INJECTION_HUB_CUR);
GET_TRIP_STATUS (OUT_TRIP_STATUS_CUR);
GET_TRIP_VENDOR_STATUS (OUT_TRIP_VENDOR_STATUS_CUR);

END GET_TRIP_VENDOR_ORDER_INFO;


/******************************************************************************
                          INSERT_ZJ_TRIP

Description: This procedure is responsible for inserting a trip

******************************************************************************/
PROCEDURE INSERT_ZJ_TRIP
(
  IN_TRIP_ORIGINATION_DESC        IN ZJ_TRIP.TRIP_ORIGINATION_DESC%TYPE,
  IN_INJECTION_HUB_ID             IN ZJ_TRIP.INJECTION_HUB_ID%TYPE,
  IN_DELIVERY_DATE                IN ZJ_TRIP.DELIVERY_DATE%TYPE,
  IN_DEPARTURE_DATE               IN ZJ_TRIP.DEPARTURE_DATE%TYPE,
  IN_TRIP_STATUS_CODE             IN ZJ_TRIP.TRIP_STATUS_CODE%TYPE,
  IN_SORT_CODE_STATE_MASTER_ID    IN ZJ_TRIP.SORT_CODE_STATE_MASTER_ID%TYPE,
  IN_SORT_CODE_DATE_MMDD          IN ZJ_TRIP.SORT_CODE_DATE_MMDD%TYPE,
  IN_TOTAL_PLT_QTY                IN ZJ_TRIP.TOTAL_PLT_QTY%TYPE,
  IN_FTD_PLT_QTY                  IN ZJ_TRIP.FTD_PLT_QTY%TYPE,
  IN_THIRD_PARTY_PLT_QTY          IN ZJ_TRIP.THIRD_PARTY_PLT_QTY%TYPE,
  IN_FTD_PLT_USED_QTY             IN ZJ_TRIP.FTD_PLT_USED_QTY%TYPE,
  IN_CREATED_BY                   IN ZJ_TRIP.CREATED_BY%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP.UPDATED_BY%TYPE,
  IN_DAYS_IN_TRANSIT_QTY          IN ZJ_TRIP.DAYS_IN_TRANSIT_QTY%TYPE,
  OUT_TRIP_ID                    OUT ZJ_TRIP.TRIP_ID%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS

v_sort_code_seq      number := 0;
v_trailer_number     varchar2(20);

BEGIN

OUT_TRIP_ID                 := key.keygen ('ZJ_TRIP');

IF OUT_TRIP_ID is null or OUT_TRIP_ID = 0 THEN
    RAISE_APPLICATION_ERROR(-20001, 'Trip Id could not be generated.');
END IF;


SELECT decode( max(SORT_CODE_SEQ), null, 0,  max(SORT_CODE_SEQ)) + 1
  INTO v_sort_code_seq
  FROM VENUS.ZJ_TRIP
 WHERE SORT_CODE_DATE_MMDD = IN_SORT_CODE_DATE_MMDD
   AND SORT_CODE_STATE_MASTER_ID = IN_SORT_CODE_STATE_MASTER_ID;

v_trailer_number := 'Z'; 
IF v_sort_code_seq < 10 THEN
  v_trailer_number := v_trailer_number || '0'; 
END IF; 
v_trailer_number := v_trailer_number || v_sort_code_seq || '-' || IN_SORT_CODE_STATE_MASTER_ID || '-' || IN_SORT_CODE_DATE_MMDD; 


      INSERT INTO VENUS.ZJ_TRIP
      (
          TRIP_ID,
          TRIP_ORIGINATION_DESC,
          INJECTION_HUB_ID,
          DELIVERY_DATE,
          DEPARTURE_DATE,
          TRIP_STATUS_CODE,
          SORT_CODE_SEQ,
          SORT_CODE_STATE_MASTER_ID,
          SORT_CODE_DATE_MMDD,
          ZONE_JUMP_TRAILER_NUMBER,
          TOTAL_PLT_QTY,
          FTD_PLT_QTY,
          THIRD_PARTY_PLT_QTY,
          FTD_PLT_USED_QTY,
          CREATED_BY,
          CREATED_ON,
          UPDATED_BY,
          UPDATED_ON,
          DAYS_IN_TRANSIT_QTY
      )
      VALUES
      (
          OUT_TRIP_ID,
          IN_TRIP_ORIGINATION_DESC,
          IN_INJECTION_HUB_ID,
          IN_DELIVERY_DATE,
          IN_DEPARTURE_DATE,
          IN_TRIP_STATUS_CODE,
          v_sort_code_seq,
          IN_SORT_CODE_STATE_MASTER_ID,
          IN_SORT_CODE_DATE_MMDD,
          v_trailer_number,
          IN_TOTAL_PLT_QTY,
          IN_FTD_PLT_QTY,
          IN_THIRD_PARTY_PLT_QTY,
          IN_FTD_PLT_USED_QTY,
          IN_CREATED_BY,
          SYSDATE,
          IN_UPDATED_BY,
          SYSDATE,
          IN_DAYS_IN_TRANSIT_QTY
      );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ZJ_TRIP [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ZJ_TRIP;


/******************************************************************************
                          UPDATE_ZJ_TRIP

Description: This procedure is responsible for updating a trip

******************************************************************************/
PROCEDURE UPDATE_ZJ_TRIP
(
  IN_TRIP_ID                      IN ZJ_TRIP.TRIP_ID%TYPE,
  IN_TRIP_ORIGINATION_DESC        IN ZJ_TRIP.TRIP_ORIGINATION_DESC%TYPE,
  IN_INJECTION_HUB_ID             IN ZJ_TRIP.INJECTION_HUB_ID%TYPE,
  IN_DELIVERY_DATE                IN ZJ_TRIP.DELIVERY_DATE%TYPE,
  IN_DEPARTURE_DATE               IN ZJ_TRIP.DEPARTURE_DATE%TYPE,
  IN_TRIP_STATUS_CODE             IN ZJ_TRIP.TRIP_STATUS_CODE%TYPE,
  IN_SORT_CODE_STATE_MASTER_ID    IN ZJ_TRIP.SORT_CODE_STATE_MASTER_ID%TYPE,
  IN_SORT_CODE_DATE_MMDD          IN ZJ_TRIP.SORT_CODE_DATE_MMDD%TYPE,
  IN_TOTAL_PLT_QTY                IN ZJ_TRIP.TOTAL_PLT_QTY%TYPE,
  IN_FTD_PLT_QTY                  IN ZJ_TRIP.FTD_PLT_QTY%TYPE,
  IN_THIRD_PARTY_PLT_QTY          IN ZJ_TRIP.THIRD_PARTY_PLT_QTY%TYPE,
  IN_FTD_PLT_USED_QTY             IN ZJ_TRIP.FTD_PLT_USED_QTY%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP.UPDATED_BY%TYPE,
  IN_DAYS_IN_TRANSIT_QTY          IN ZJ_TRIP.DAYS_IN_TRANSIT_QTY%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS

v_sort_code_seq                number := 0;
v_sort_code_date_mmdd          varchar2(4);
v_sort_code_state_master_id    varchar2(2);
v_trailer_number               varchar2(20);


BEGIN

SELECT SORT_CODE_SEQ, SORT_CODE_STATE_MASTER_ID, SORT_CODE_DATE_MMDD
  INTO v_sort_code_seq, v_sort_code_state_master_id, v_sort_code_date_mmdd
  FROM VENUS.ZJ_TRIP
 WHERE TRIP_ID = IN_TRIP_ID;


IF v_sort_code_state_master_id <> IN_SORT_CODE_STATE_MASTER_ID or v_sort_code_date_mmdd <> IN_SORT_CODE_DATE_MMDD THEN
  SELECT decode( max(SORT_CODE_SEQ), null, 0,  max(SORT_CODE_SEQ)) + 1
    INTO v_sort_code_seq
    FROM VENUS.ZJ_TRIP
   WHERE SORT_CODE_DATE_MMDD = IN_SORT_CODE_DATE_MMDD
     AND SORT_CODE_STATE_MASTER_ID = IN_SORT_CODE_STATE_MASTER_ID;
END IF;

v_trailer_number := 'Z'; 
IF v_sort_code_seq < 10 THEN
  v_trailer_number := v_trailer_number || '0'; 
END IF; 
v_trailer_number := v_trailer_number || v_sort_code_seq || '-' || IN_SORT_CODE_STATE_MASTER_ID || '-' || IN_SORT_CODE_DATE_MMDD; 


      UPDATE VENUS.ZJ_TRIP
         SET TRIP_ORIGINATION_DESC      = IN_TRIP_ORIGINATION_DESC,
             INJECTION_HUB_ID           = IN_INJECTION_HUB_ID,
             DELIVERY_DATE              = IN_DELIVERY_DATE,
             DEPARTURE_DATE             = IN_DEPARTURE_DATE,
             TRIP_STATUS_CODE           = IN_TRIP_STATUS_CODE,
             SORT_CODE_SEQ              = v_sort_code_seq,
             SORT_CODE_STATE_MASTER_ID  = IN_SORT_CODE_STATE_MASTER_ID,
             SORT_CODE_DATE_MMDD        = IN_SORT_CODE_DATE_MMDD,
             ZONE_JUMP_TRAILER_NUMBER   = v_trailer_number,
             TOTAL_PLT_QTY              = IN_TOTAL_PLT_QTY,
             FTD_PLT_QTY                = IN_FTD_PLT_QTY,
             THIRD_PARTY_PLT_QTY        = IN_THIRD_PARTY_PLT_QTY,
             FTD_PLT_USED_QTY           = IN_FTD_PLT_USED_QTY,
             UPDATED_BY                 = IN_UPDATED_BY,
             UPDATED_ON                 = sysdate,
             DAYS_IN_TRANSIT_QTY        = IN_DAYS_IN_TRANSIT_QTY
       WHERE TRIP_ID                    = IN_TRIP_ID;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ZJ_TRIP [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ZJ_TRIP;


/******************************************************************************
                          UPDATE_ZJ_TRIP_STATUS

Description: This procedure is responsible for updating trip STATUS only

******************************************************************************/
PROCEDURE UPDATE_ZJ_TRIP_STATUS
(
  IN_TRIP_ID                      IN ZJ_TRIP.TRIP_ID%TYPE,
  IN_ZONE_JUMP_TRAILER_NUMBER     IN ZJ_TRIP.ZONE_JUMP_TRAILER_NUMBER%TYPE,
  IN_TRIP_STATUS_CODE             IN ZJ_TRIP.TRIP_STATUS_CODE%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP.UPDATED_BY%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS


BEGIN

IF IN_TRIP_ID IS NULL AND IN_ZONE_JUMP_TRAILER_NUMBER IS NULL THEN
  RAISE_APPLICATION_ERROR(-20001, 'Trip Id or Trailer Number must be provided to update trip status');
END IF; 

IF IN_TRIP_ID IS NOT NULL AND IN_ZONE_JUMP_TRAILER_NUMBER IS NOT NULL THEN
      UPDATE VENUS.ZJ_TRIP
         SET TRIP_STATUS_CODE           = IN_TRIP_STATUS_CODE,
             UPDATED_BY                 = IN_UPDATED_BY,
             UPDATED_ON                 = sysdate
       WHERE TRIP_ID                    = IN_TRIP_ID
         AND ZONE_JUMP_TRAILER_NUMBER   = IN_ZONE_JUMP_TRAILER_NUMBER; 
ELSE 
  IF IN_TRIP_ID IS NOT NULL THEN
      UPDATE VENUS.ZJ_TRIP
         SET TRIP_STATUS_CODE           = IN_TRIP_STATUS_CODE,
             UPDATED_BY                 = IN_UPDATED_BY,
             UPDATED_ON                 = sysdate
       WHERE TRIP_ID                    = IN_TRIP_ID;
  ELSE 
      UPDATE VENUS.ZJ_TRIP
         SET TRIP_STATUS_CODE           = IN_TRIP_STATUS_CODE,
             UPDATED_BY                 = IN_UPDATED_BY,
             UPDATED_ON                 = sysdate
       WHERE ZONE_JUMP_TRAILER_NUMBER   = IN_ZONE_JUMP_TRAILER_NUMBER;
  END IF;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ZJ_TRIP_STATUS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ZJ_TRIP_STATUS;


/******************************************************************************
                          CANCEL_ZJ_TRIP

Description: This procedure is responsible for cancelling the ENTIRE trip

******************************************************************************/
PROCEDURE CANCEL_ZJ_TRIP
(
  IN_TRIP_ID                      IN ZJ_TRIP.TRIP_ID%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP.UPDATED_BY%TYPE,
  OUT_CANCEL_CUR                 OUT TYPES.REF_CURSOR,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS

BEGIN

/* select all the orders that will be updated when a user cancels a trip */
OPEN OUT_CANCEL_CUR FOR
      SELECT tvo.TRIP_ID, 
             tvo.VENDOR_ID, 
             tvo.ORDER_DETAIL_ID, 
             v.VENUS_ID,
             v.SDS_STATUS, 
             v.BUSINESS_NAME, 
             v.CARD_MESSAGE, 
             v.FILLING_VENDOR, 
             v.ORDER_DATE,
             v.PRODUCT_ID,
             v.PRICE, 
             v.ADDRESS_1, 
             v.CITY, 
             v.COUNTRY, 
             v.RECIPIENT, 
             v.PHONE_NUMBER, 
             v.STATE, 
             v.ZIP, 
             v.SHIP_METHOD, 
             v.SHIP_DATE, 
             v.VENUS_ORDER_NUMBER
        FROM VENUS.ZJ_TRIP_VENDOR_ORDER tvo, VENUS.VENUS v
       WHERE tvo.TRIP_ID = IN_TRIP_ID
         AND tvo.VENUS_ID = v.VENUS_ID
         AND v.SDS_STATUS = 'AVAILABLE'
         AND tvo.ORDER_FILLED_FLAG = 'Y';

      /* for a given trip id, update all the orders thereby marking them not-filled */
      UPDATE VENUS.ZJ_TRIP_VENDOR_ORDER
         SET ORDER_FILLED_FLAG  = 'N',
             UPDATED_BY         = IN_UPDATED_BY,
             UPDATED_ON         = SYSDATE
       WHERE TRIP_ID = IN_TRIP_ID
         AND ORDER_FILLED_FLAG = 'Y';

      /* for a given trip id, update all the vendors thereby marking them Canceled */
      UPDATE VENUS.ZJ_TRIP_VENDOR
         SET TRIP_VENDOR_STATUS_CODE     = 'X',
             FULL_PLT_QTY                = 0, 
             FULL_PLT_ORDER_QTY          = 0, 
             PRTL_PLT_IN_USE_FLAG        = 'N',
             PRTL_PLT_ORDER_QTY          = 0,
             PRTL_PLT_CUBIC_IN_USED_QTY  = 0,
             UPDATED_BY                  = IN_UPDATED_BY,
             UPDATED_ON                  = SYSDATE
       WHERE TRIP_ID = IN_TRIP_ID
         AND TRIP_VENDOR_STATUS_CODE != 'X';

      /* for a given trip id, update the trip thereby marking it Canceled */
      UPDATE VENUS.ZJ_TRIP
         SET TRIP_STATUS_CODE        = 'X',
             FTD_PLT_USED_QTY        = 0,
             UPDATED_BY              = IN_UPDATED_BY,
             UPDATED_ON              = SYSDATE
       WHERE TRIP_ID = IN_TRIP_ID
         AND TRIP_STATUS_CODE != 'X';

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN CANCEL_ZJ_TRIP [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CANCEL_ZJ_TRIP;


/******************************************************************************
                          INSERT_ZJ_TRIP_VENDOR

Description: This procedure is responsible for inserting a trip vendor

******************************************************************************/
PROCEDURE INSERT_ZJ_TRIP_VENDOR
(
  IN_TRIP_ID                      IN ZJ_TRIP_VENDOR.TRIP_ID%TYPE,
  IN_VENDOR_ID                    IN ZJ_TRIP_VENDOR.VENDOR_ID%TYPE,
  IN_TRIP_VENDOR_STATUS_CODE      IN ZJ_TRIP_VENDOR.TRIP_VENDOR_STATUS_CODE%TYPE,
  IN_ORDER_CUTOFF_TIME            IN ZJ_TRIP_VENDOR.ORDER_CUTOFF_TIME%TYPE,
  IN_PLT_CUBIC_INCH_ALLOWED_QTY   IN ZJ_TRIP_VENDOR.PLT_CUBIC_INCH_ALLOWED_QTY%TYPE,
  IN_FULL_PLT_QTY                 IN ZJ_TRIP_VENDOR.FULL_PLT_QTY%TYPE,
  IN_FULL_PLT_ORDER_QTY           IN ZJ_TRIP_VENDOR.FULL_PLT_ORDER_QTY%TYPE,
  IN_PRTL_PLT_IN_USE_FLAG         IN ZJ_TRIP_VENDOR.PRTL_PLT_IN_USE_FLAG%TYPE,
  IN_PRTL_PLT_ORDER_QTY           IN ZJ_TRIP_VENDOR.PRTL_PLT_ORDER_QTY%TYPE,
  IN_PRTL_PLT_CUBIC_IN_USED_QTY   IN ZJ_TRIP_VENDOR.PRTL_PLT_CUBIC_IN_USED_QTY%TYPE,
  IN_CREATED_BY                   IN ZJ_TRIP_VENDOR.CREATED_BY%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP_VENDOR.UPDATED_BY%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS
BEGIN

      INSERT INTO VENUS.ZJ_TRIP_VENDOR
      (
          TRIP_ID,
          VENDOR_ID,
          TRIP_VENDOR_STATUS_CODE,
          ORDER_CUTOFF_TIME,
          PLT_CUBIC_INCH_ALLOWED_QTY,
          FULL_PLT_QTY,
          FULL_PLT_ORDER_QTY,
          PRTL_PLT_IN_USE_FLAG,
          PRTL_PLT_ORDER_QTY,
          PRTL_PLT_CUBIC_IN_USED_QTY,
          CREATED_BY,
          CREATED_ON,
          UPDATED_BY,
          UPDATED_ON
      )
      VALUES
      (
          IN_TRIP_ID,
          IN_VENDOR_ID,
          IN_TRIP_VENDOR_STATUS_CODE,
          IN_ORDER_CUTOFF_TIME,
          IN_PLT_CUBIC_INCH_ALLOWED_QTY,
          IN_FULL_PLT_QTY,
          IN_FULL_PLT_ORDER_QTY,
          IN_PRTL_PLT_IN_USE_FLAG,
          IN_PRTL_PLT_ORDER_QTY,
          IN_PRTL_PLT_CUBIC_IN_USED_QTY,
          IN_CREATED_BY,
          SYSDATE,
          IN_UPDATED_BY,
          SYSDATE
      );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ZJ_TRIP_VENDOR [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ZJ_TRIP_VENDOR;

PROCEDURE INSERT_ZJ_TRIP_VENDOR_ORDER
(
  IN_TRIP_ID                      IN ZJ_TRIP_VENDOR_ORDER.TRIP_ID%TYPE,
  IN_VENDOR_ID                    IN ZJ_TRIP_VENDOR_ORDER.VENDOR_ID%TYPE,
  IN_ORDER_DETAIL_ID              IN ZJ_TRIP_VENDOR_ORDER.ORDER_DETAIL_ID%TYPE,
  IN_VENUS_ID                     IN ZJ_TRIP_VENDOR_ORDER.VENUS_ID%TYPE,
  IN_ORDER_FILLED_FLAG            IN ZJ_TRIP_VENDOR_ORDER.ORDER_FILLED_FLAG%TYPE,
  IN_CREATED_BY                   IN ZJ_TRIP_VENDOR_ORDER.CREATED_BY%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP_VENDOR_ORDER.UPDATED_BY%TYPE,
  IN_BOX_CUBIC_IN_QTY             IN ZJ_TRIP_VENDOR_ORDER.BOX_CUBIC_IN_QTY%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS
BEGIN

      INSERT INTO VENUS.ZJ_TRIP_VENDOR_ORDER
      (
          TRIP_ID,
          VENDOR_ID,
          ORDER_DETAIL_ID,
          VENUS_ID,
          ORDER_FILLED_FLAG,
          CREATED_BY,
          CREATED_ON,
          UPDATED_BY,
          UPDATED_ON,
          BOX_CUBIC_IN_QTY
      )
      VALUES
      (
          IN_TRIP_ID,
          IN_VENDOR_ID,
          IN_ORDER_DETAIL_ID,
          IN_VENUS_ID,
          IN_ORDER_FILLED_FLAG,
          IN_CREATED_BY,
          SYSDATE,
          IN_UPDATED_BY,
          SYSDATE, 
          IN_BOX_CUBIC_IN_QTY
      );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ZJ_TRIP_VENDOR_ORDER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INSERT_ZJ_TRIP_VENDOR_ORDER;

/******************************************************************************
                          UPDATE_TRIP_VNDR_CUBIC_INCHES

Description: This procedure is responsible for updating cubic inches that a pallet
             hold for ALL active trips.

******************************************************************************/
PROCEDURE UPDATE_TRIP_VNDR_CUBIC_INCHES
(
  IN_PLT_CUBIC_INCH_ALLOWED_QTY   IN ZJ_TRIP_VENDOR.PLT_CUBIC_INCH_ALLOWED_QTY%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP_VENDOR.UPDATED_BY%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS
BEGIN

      UPDATE VENUS.ZJ_TRIP_VENDOR
         SET PLT_CUBIC_INCH_ALLOWED_QTY    = IN_PLT_CUBIC_INCH_ALLOWED_QTY,
             UPDATED_BY                    = IN_UPDATED_BY,
             UPDATED_ON                    = sysdate
       WHERE TRIP_ID                      IN (select trip_id from ZJ_TRIP where TRIP_STATUS_CODE = 'A');

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_TRIP_VNDR_CUBIC_INCHES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_TRIP_VNDR_CUBIC_INCHES;


/******************************************************************************
                          UPDATE_ZJ_TRIP_VENDOR

Description: This procedure is responsible for updating a trip vendor

******************************************************************************/

PROCEDURE UPDATE_ZJ_TRIP_VENDOR
(
  IN_TRIP_ID                      IN ZJ_TRIP_VENDOR.TRIP_ID%TYPE,
  IN_VENDOR_ID                    IN ZJ_TRIP_VENDOR.VENDOR_ID%TYPE,
  IN_TRIP_VENDOR_STATUS_CODE      IN ZJ_TRIP_VENDOR.TRIP_VENDOR_STATUS_CODE%TYPE,
  IN_ORDER_CUTOFF_TIME            IN ZJ_TRIP_VENDOR.ORDER_CUTOFF_TIME%TYPE,
  IN_PLT_CUBIC_INCH_ALLOWED_QTY   IN ZJ_TRIP_VENDOR.PLT_CUBIC_INCH_ALLOWED_QTY%TYPE,
  IN_FULL_PLT_QTY                 IN ZJ_TRIP_VENDOR.FULL_PLT_QTY%TYPE,
  IN_FULL_PLT_ORDER_QTY           IN ZJ_TRIP_VENDOR.FULL_PLT_ORDER_QTY%TYPE,
  IN_PRTL_PLT_IN_USE_FLAG         IN ZJ_TRIP_VENDOR.PRTL_PLT_IN_USE_FLAG%TYPE,
  IN_PRTL_PLT_ORDER_QTY           IN ZJ_TRIP_VENDOR.PRTL_PLT_ORDER_QTY%TYPE,
  IN_PRTL_PLT_CUBIC_IN_USED_QTY   IN ZJ_TRIP_VENDOR.PRTL_PLT_CUBIC_IN_USED_QTY%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP_VENDOR.UPDATED_BY%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS
BEGIN

      UPDATE VENUS.ZJ_TRIP_VENDOR
         SET TRIP_VENDOR_STATUS_CODE       = IN_TRIP_VENDOR_STATUS_CODE,
             ORDER_CUTOFF_TIME             = IN_ORDER_CUTOFF_TIME,
             PLT_CUBIC_INCH_ALLOWED_QTY    = IN_PLT_CUBIC_INCH_ALLOWED_QTY,
             FULL_PLT_QTY                  = IN_FULL_PLT_QTY,
             FULL_PLT_ORDER_QTY            = IN_FULL_PLT_ORDER_QTY,
             PRTL_PLT_IN_USE_FLAG          = IN_PRTL_PLT_IN_USE_FLAG,
             PRTL_PLT_ORDER_QTY            = IN_PRTL_PLT_ORDER_QTY,
             PRTL_PLT_CUBIC_IN_USED_QTY    = IN_PRTL_PLT_CUBIC_IN_USED_QTY,
             UPDATED_BY                    = IN_UPDATED_BY,
             UPDATED_ON                    = sysdate
       WHERE TRIP_ID                       = IN_TRIP_ID
         AND VENDOR_ID                     = IN_VENDOR_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ZJ_TRIP_VENDOR [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ZJ_TRIP_VENDOR;


/******************************************************************************
                          CANCEL_ZJ_TRIP_VENDOR

Description: This procedure is responsible for cancelling a vendor on a trip

******************************************************************************/
PROCEDURE CANCEL_ZJ_TRIP_VENDOR
(
  IN_TRIP_ID                      IN ZJ_TRIP_VENDOR.TRIP_ID%TYPE,
  IN_VENDOR_ID                    IN ZJ_TRIP_VENDOR.VENDOR_ID%TYPE,
  IN_UPDATED_BY                   IN ZJ_TRIP_VENDOR.UPDATED_BY%TYPE,
  OUT_CANCEL_CUR                 OUT TYPES.REF_CURSOR,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
)
AS

CURSOR  out_cur IS
        SELECT tv.FULL_PLT_QTY, tv.PRTL_PLT_IN_USE_FLAG, t.TRIP_STATUS_CODE
          FROM VENUS.ZJ_TRIP_VENDOR tv, VENUS.ZJ_TRIP t
         WHERE tv.TRIP_ID = t.TRIP_ID
           AND tv.TRIP_ID = IN_TRIP_ID
           AND tv.VENDOR_ID = IN_VENDOR_ID
           AND TRIP_VENDOR_STATUS_CODE != 'X';


v_full_plt_qty             number := 0;
v_total_plt_qty            number := 0;
v_prtl_plt_in_use_flag     varchar2(1);
v_trip_status_code         varchar2(1);

BEGIN

OPEN out_cur;
FETCH out_cur INTO v_full_plt_qty, v_prtl_plt_in_use_flag, v_trip_status_code;
CLOSE out_cur;

/* determine the amount of pallets to be put back on the trip */
v_total_plt_qty := v_full_plt_qty;
IF v_prtl_plt_in_use_flag = 'y' or v_prtl_plt_in_use_flag = 'Y' THEN
  v_total_plt_qty := v_total_plt_qty + 1;
END IF;

/* if the total # of pallets to be put back on the trip is > 0, reset the trip status to be Active */
IF v_total_plt_qty > 0 THEN
  v_trip_status_code := 'A';
END IF;

/* select all the orders that will be updated when a user cancels a trip */
OPEN OUT_CANCEL_CUR FOR
      SELECT tvo.TRIP_ID, 
             tvo.VENDOR_ID, 
             tvo.ORDER_DETAIL_ID, 
             v.VENUS_ID,
             v.SDS_STATUS, 
             v.BUSINESS_NAME, 
             v.CARD_MESSAGE, 
             v.FILLING_VENDOR, 
             v.ORDER_DATE, 
             v.PRODUCT_ID,
             v.PRICE, 
             v.ADDRESS_1, 
             v.CITY, 
             v.COUNTRY, 
             v.RECIPIENT, 
             v.PHONE_NUMBER, 
             v.STATE, 
             v.ZIP, 
             v.SHIP_METHOD, 
             v.SHIP_DATE, 
             v.VENUS_ORDER_NUMBER
        FROM VENUS.ZJ_TRIP_VENDOR_ORDER tvo, VENUS.VENUS v
       WHERE tvo.TRIP_ID = IN_TRIP_ID
         AND tvo.VENDOR_ID = IN_VENDOR_ID
         AND tvo.VENUS_ID = v.VENUS_ID
         AND v.SDS_STATUS = 'AVAILABLE'
         AND tvo.ORDER_FILLED_FLAG = 'Y';


      /* for a given trip id, update all the orders thereby marking them not-filled */
      UPDATE VENUS.ZJ_TRIP_VENDOR_ORDER
         SET ORDER_FILLED_FLAG  = 'N',
             UPDATED_BY         = IN_UPDATED_BY,
             UPDATED_ON         = SYSDATE
       WHERE TRIP_ID = IN_TRIP_ID
         AND VENDOR_ID = IN_VENDOR_ID
         AND ORDER_FILLED_FLAG = 'Y';


      /* for the given trip id, put the pallet quantities that the vendor was using 
         back on the trip, and set the status 
      */
      UPDATE VENUS.ZJ_TRIP
         SET TRIP_STATUS_CODE     = NVL(v_trip_status_code, TRIP_STATUS_CODE),
             FTD_PLT_USED_QTY     = (FTD_PLT_USED_QTY - v_total_plt_qty),
             UPDATED_BY           = IN_UPDATED_BY,
             UPDATED_ON           = SYSDATE
       WHERE TRIP_ID              = IN_TRIP_ID;


      /* for a given trip and vendor id, update the vendor thereby marking it as
         Canceled and update the quantities
      */
      UPDATE VENUS.ZJ_TRIP_VENDOR
         SET TRIP_VENDOR_STATUS_CODE     = 'X',
             FULL_PLT_QTY                = 0, 
             FULL_PLT_ORDER_QTY          = 0, 
             PRTL_PLT_IN_USE_FLAG        = 'N',
             PRTL_PLT_ORDER_QTY          = 0,
             PRTL_PLT_CUBIC_IN_USED_QTY  = 0,
             UPDATED_BY                  = IN_UPDATED_BY,
             UPDATED_ON                  = SYSDATE
       WHERE TRIP_ID = IN_TRIP_ID
         AND VENDOR_ID = IN_VENDOR_ID
         AND TRIP_VENDOR_STATUS_CODE != 'X';

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN CANCEL_ZJ_TRIP_VENDOR [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CANCEL_ZJ_TRIP_VENDOR;


PROCEDURE ZJ_TRIP_EXISTS
(
IN_VENDOR_ID  IN VARCHAR2,
IN_BLOCK_DATE IN VARCHAR2,
IN_BLOCK_TYPE IN VARCHAR2,
OUT_VENDORS   OUT TYPES.REF_CURSOR
)
AS
/*----------------------------------------------------------------------
Description:
        The proc will return a list of vendors assigned to an active zone jump trip
        based on the data passed in

Input:
    IN_VENDOR_ID    VARCHAR2
  IN_BLOCK_DATE     VARCHAR2
  IN_BLOCK_TYPE     VARCHAR2

Output:
        OUT_VENDORS  REFCURSOR

-----------------------------------------------------------------------*/
--variables
v_sql           varchar2 (6000) := '';
v_select_from    varchar2 (4000) := '';
v_where         varchar2 (4000) := '';
v_and           varchar2 (4000) := '';
v_count        number(4);

BEGIN

CASE

  WHEN IN_VENDOR_ID is null THEN
  v_select_from  := 'SELECT distinct ztv.vendor_id FROM venus.zj_trip zt, venus.zj_trip_vendor ztv ';
        IF in_block_type    = 'Delivery' THEN
        v_where        := ' WHERE TRUNC(zt.DELIVERY_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy''))';
        ELSIF in_block_type    = 'Shipping' THEN
      v_where        := ' WHERE TRUNC(zt.DEPARTURE_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy''))';
        ELSIF in_block_type = 'Both' THEN
        v_where        := ' WHERE (TRUNC(zt.DEPARTURE_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy'')) OR TRUNC(zt.DELIVERY_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy''))) ';
        END IF;

        v_and          := ' AND zt.TRIP_STATUS_CODE = ''A'' ';
        v_and := v_and || ' AND ( (ztv.trip_vendor_status_code = ''A'') OR (ztv.trip_vendor_status_code = ''U'') ) ';
        v_and := v_and || ' AND zt.trip_id = ztv.trip_id  ';

        v_sql          := v_select_from || v_where || v_and;

  WHEN IN_VENDOR_ID is not null THEN
  v_select_from         := 'SELECT distinct ztv.vendor_id FROM venus.zj_trip zt, venus.zj_trip_vendor ztv ';
  IF in_block_type    = 'Delivery' THEN
        v_where        := ' WHERE TRUNC(zt.DELIVERY_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy''))';
  ELSIF in_block_type    = 'Shipping' THEN
      v_where        := ' WHERE TRUNC(zt.DEPARTURE_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy''))';
        ELSIF in_block_type = 'Both' THEN
        v_where        := ' WHERE (TRUNC(zt.DEPARTURE_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy'')) OR TRUNC(zt.DELIVERY_DATE) = trunc(to_date(''' || IN_BLOCK_DATE || ''', ''mm/dd/yyyy''))) ';
        END IF;

  v_and          := 'AND zt.trip_id = ztv.trip_id ';
  v_and := v_and || 'AND ztv.vendor_id = ''' || in_vendor_id || ''' ';
  v_and := v_and || 'AND zt.TRIP_STATUS_CODE = ''A'' ';
  v_and := v_and || 'AND ( (ztv.trip_vendor_status_code = ''A'') OR (ztv.trip_vendor_status_code = ''U'') ) ';

  v_sql          := v_select_from || v_where || v_and;
END CASE;

OPEN out_vendors FOR v_sql;

END ZJ_TRIP_EXISTS;

PROCEDURE REMOVE_ORDER_FROM_VENDOR
(
IN_VENUS_ID  IN VARCHAR2,
IN_UPDATED_BY  IN VARCHAR2,
OUT_STATUS  OUT VARCHAR2,
OUT_MESSAGE     OUT VARCHAR2
)
AS
/*----------------------------------------------------------------------
Description:
        The proc will remove an order from being filled by a vendor and
        remove the box from the vendor's pallet count so the space can be
        used for another package

Input:
  IN_VENUS_ID  IN VARCHAR2
  IN_UPDATED_BY  IN VARCHAR2

Output:
        OUT_STATUS  OUT VARCHAR2
  OUT_MESSAGE     OUT VARCHAR2

-----------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_vendor_id varchar2(100);
v_trip_id number;
v_box_cubic_in_qty number;


BEGIN

      UPDATE VENUS.ZJ_TRIP_VENDOR_ORDER
      SET ORDER_FILLED_FLAG = 'N', UPDATED_BY = IN_UPDATED_BY, UPDATED_ON = SYSDATE
      WHERE VENUS_ID = IN_VENUS_ID;

  IF SQL%ROWCOUNT = 0 THEN
     out_message := 'NO ZJ_TRIP_VENDOR_ORDER RECORD FOUND/UPDATED';
  ELSIF SQL%ROWCOUNT > 0 THEN

      SELECT DISTINCT ZTVO.TRIP_ID, ZTVO.VENDOR_ID, ZTVO.BOX_CUBIC_IN_QTY
        into v_trip_id, v_vendor_id, v_box_cubic_in_qty
        FROM VENUS.ZJ_TRIP_VENDOR_ORDER ZTVO
       WHERE ZTVO.VENUS_ID = IN_VENUS_ID;

      UPDATE VENUS.ZJ_TRIP_VENDOR ztv
      SET ZTV.PRTL_PLT_ORDER_QTY = ZTV.PRTL_PLT_ORDER_QTY - 1, 
          ZTV.PRTL_PLT_CUBIC_IN_USED_QTY = ZTV.PRTL_PLT_CUBIC_IN_USED_QTY - v_box_cubic_in_qty,
          UPDATED_BY = IN_UPDATED_BY, UPDATED_ON = SYSDATE
      WHERE ztv.trip_id = v_trip_id
      AND ztv.vendor_id = v_vendor_id
      AND ztv.PRTL_PLT_IN_USE_FLAG = 'Y'
      AND ztv.PRTL_PLT_ORDER_QTY > 0;


      IF SQL%ROWCOUNT = 0 THEN
       out_message := 'NO ZJ_TRIP_VENDOR RECORD FOUND/UPDATED';
      END IF;

  END IF;

  COMMIT;
  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED IN REMOVE_ORDER_FROM_VENDOR [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END REMOVE_ORDER_FROM_VENDOR;

PROCEDURE ASSIGN_ORDER_TO_VENDOR
(
IN_TRIP_ID    IN NUMBER,
IN_VENDOR_ID    IN VARCHAR2,
IN_ORDER_DETAIL_ID  IN NUMBER,
IN_VENUS_ID    IN VARCHAR2,
IN_CREATED_BY    IN VARCHAR2,
OUT_STATUS  OUT VARCHAR2,
OUT_MESSAGE     OUT VARCHAR2
)
AS
/*----------------------------------------------------------------------
Description:
        The proc will assign an order to be filled by a vendor and
        add the box to the vendor's pallet count

Input:
  IN_TRIP_ID    IN NUMBER
  IN_VENDOR_ID    IN VARCHAR2
  IN_ORDER_DETAIL_ID  IN NUMBER
  IN_VENUS_ID    IN VARCHAR2
  IN_CREATED_BY    IN VARCHAR2

Output:
        OUT_STATUS  OUT VARCHAR2
  OUT_MESSAGE     OUT VARCHAR2

-----------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_height number;
v_width number;
v_length number;
v_box_cubic_in_qty number;

v_prtl_plt_in_use_flag      zj_trip_vendor.prtl_plt_in_use_flag%TYPE := 'N';
v_plt_cubic_inch_allowed_qty  zj_trip_vendor.plt_cubic_inch_allowed_qty%TYPE := 0;
v_prtl_plt_cubic_in_used_qty  zj_trip_vendor.prtl_plt_cubic_in_used_qty%TYPE := 0;

v_ftd_plt_used_qty        zj_trip.ftd_plt_used_qty%TYPE := 0;
v_ftd_plt_qty            zj_trip.ftd_plt_qty%TYPE := 0;
v_cnt                  NUMBER(10) := 0;


BEGIN

      select b.height_inch_qty, b.width_inch_qty, b.length_inch_qty into v_height, v_width, v_length
      from ftd_apps.box b, ftd_apps.product_master pm, clean.order_details od
      where od.order_detail_id = in_order_detail_id
      and od.product_id = pm.product_id
      and pm.box_id = b.box_id;
  
      v_box_cubic_in_qty := v_height * v_width * v_length; 
  
      SELECT prtl_plt_in_use_flag,
              plt_cubic_inch_allowed_qty
      INTO v_prtl_plt_in_use_flag,
        v_plt_cubic_inch_allowed_qty
      FROM venus.zj_trip_vendor tv
      WHERE trip_id = in_trip_id
      AND vendor_id = in_vendor_id;

     -- if vendor currently has partial pallet assigned
     IF v_prtl_plt_in_use_flag = 'Y' THEN
      
  -- increment the prtl_plt_order_qty and the prtl_plt_cubic_in_used_qty accordingly  
        UPDATE venus.zj_trip_vendor ztv
  SET ztv.prtl_plt_order_qty = ztv.prtl_plt_order_qty + 1, 
      ztv.prtl_plt_cubic_in_used_qty = ztv.prtl_plt_cubic_in_used_qty + v_box_cubic_in_qty,
      updated_by = in_created_by,
      updated_on = SYSDATE
  WHERE ztv.trip_id = IN_TRIP_ID
        AND ztv.vendor_id = IN_VENDOR_ID
        RETURNING ztv.prtl_plt_cubic_in_used_qty INTO v_prtl_plt_cubic_in_used_qty;

  IF SQL%ROWCOUNT = 0 THEN
  ROLLBACK;
     out_status := 'N';
     out_message := 'FAILED TO UPDATE EXISTING PARTIAL PALLET INFO FOR VENDOR ' || IN_VENDOR_ID;
  END IF;
        
        -- check to see if the partial pallet is full now  
        IF v_prtl_plt_cubic_in_used_qty >= v_plt_cubic_inch_allowed_qty THEN
        
           -- reset prtl_plt fields and increment full_plt fields appropriately.
     UPDATE venus.zj_trip_vendor ztv
     SET prtl_plt_in_use_flag = 'N',
         ztv.full_plt_order_qty = ztv.full_plt_order_qty + ztv.prtl_plt_order_qty,
         ztv.prtl_plt_order_qty = 0, 
         ztv.prtl_plt_cubic_in_used_qty = 0,
         ztv.full_plt_qty = ztv.full_plt_qty + 1,
         updated_by = in_created_by,
         updated_on = SYSDATE
     WHERE ztv.trip_id = IN_TRIP_ID
     AND ztv.vendor_id = IN_VENDOR_ID;

     IF SQL%ROWCOUNT = 0 THEN
     ROLLBACK;
            out_status := 'N';
      out_message := 'FAILED TO UPDATE FULL PARTIAL PALLET INFO FOR VENDOR ' || IN_VENDOR_ID;
     END IF;
     
     SELECT ftd_plt_used_qty,
            ftd_plt_qty
        INTO v_ftd_plt_used_qty,
            v_ftd_plt_qty
        FROM venus.zj_trip t
       WHERE t.trip_id = in_trip_id;
           
      SELECT COUNT(1)
        INTO v_cnt
        FROM venus.zj_trip_vendor ztv
     WHERE ztv.TRIP_VENDOR_STATUS_CODE = 'A'
       AND ztv.PRTL_PLT_IN_USE_FLAG = 'Y'
       AND ztv.trip_id = IN_TRIP_ID;
        
     -- check to see if there are no more pallets available to assign       
           IF v_ftd_plt_used_qty = v_ftd_plt_qty AND v_cnt = 0 THEN
             UPDATE venus.zj_trip t
             SET t.trip_status_code = 'F',
           updated_by = in_created_by,
           updated_on = SYSDATE
             WHERE t.trip_id = IN_TRIP_ID;
             
             IF SQL%ROWCOUNT = 0 THEN
             ROLLBACK;
               out_status := 'N';
         out_message := 'FAILED TO UPDATE TRIP STATUS';
             END IF;
           END IF;
           
        
        END IF;
        
        IF NVL(out_status, 'Y') <> 'N' THEN

     -- make an entry for that order in zj_trip_vendor_order 
     INSERT_ZJ_TRIP_VENDOR_ORDER
     (
       IN_TRIP_ID,
       IN_VENDOR_ID,
       IN_ORDER_DETAIL_ID,
       IN_VENUS_ID,
       'Y',
       IN_CREATED_BY,
       IN_CREATED_BY,
       v_box_cubic_in_qty,
       OUT_STATUS,
       OUT_MESSAGE 
      );
        
       END IF;
     
     --if vendor currently does not have partial pallet assigned
     ELSE
       
       SELECT ftd_plt_used_qty,
        ftd_plt_qty
       INTO v_ftd_plt_used_qty,
      v_ftd_plt_qty
       FROM venus.zj_trip t
       WHERE t.trip_id = in_trip_id;
        
        
       IF v_ftd_plt_used_qty < v_ftd_plt_qty THEN
       -- check to see if there are currently any pallets available to assign vendor
    
         -- assing a pallet to the vendor and update prtl_plt fields accordingly 
         UPDATE venus.zj_trip_vendor ztv
         SET prtl_plt_in_use_flag = 'Y',
          ztv.prtl_plt_cubic_in_used_qty = ztv.prtl_plt_cubic_in_used_qty + v_box_cubic_in_qty,
       ztv.prtl_plt_order_qty = ztv.prtl_plt_order_qty + 1,
       updated_by = in_created_by,
       updated_on = SYSDATE
         WHERE ztv.trip_id = IN_TRIP_ID
         AND ztv.vendor_id = IN_VENDOR_ID;

         IF SQL%ROWCOUNT = 0 THEN
         ROLLBACK;
           out_status := 'N';
     out_message := 'FAILED TO UPDATE NEW PARTIAL PALLET INFO FOR VENDOR ' || IN_VENDOR_ID;
         END IF;
        
         -- increment ftd_plt_used_qty
         UPDATE venus.zj_trip t
         SET t.ftd_plt_used_qty = t.ftd_plt_used_qty + 1,
       updated_by = in_created_by,
       updated_on = SYSDATE
         WHERE t.trip_id = IN_TRIP_ID;  
  
         IF SQL%ROWCOUNT = 0 THEN
         ROLLBACK;
      out_status := 'N';
      out_message := 'FAILED TO UPDATE NEW FULL PALLETS USED INFO FOR VENDOR ' || IN_VENDOR_ID;
         END IF;  
       
         IF NVL(out_status, 'Y') <> 'N' THEN 
           -- make an entry for that order in zj_trip_vendor_order 
     INSERT_ZJ_TRIP_VENDOR_ORDER
     (
       IN_TRIP_ID,
       IN_VENDOR_ID,
       IN_ORDER_DETAIL_ID,
       IN_VENUS_ID,
       'Y',
       IN_CREATED_BY,
       IN_CREATED_BY,
       v_box_cubic_in_qty,
       OUT_STATUS,
       OUT_MESSAGE 
      );
    
      END IF;
      
      -- no pallets available to assign to vendor  
      ELSE
  ROLLBACK;
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'NO PALLETS AVAILABLE TO ASSIGN TO VENDOR';
     END IF;
      
 END IF;

  IF OUT_STATUS = 'Y' THEN
	  COMMIT;
  ELSE
     OUT_STATUS := 'N';
  	  ROLLBACK;
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED IN ZONEJUMP_PKG.ASSIGN_ORDER_TO_VENDOR [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END ASSIGN_ORDER_TO_VENDOR;

FUNCTION GET_ZONE_JUMP_HUBS (
IN_PRODUCT_SUBCODE_ID IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
IN_DELIVERY_DATE      IN ZJ_TRIP.DELIVERY_DATE%TYPE,
IN_ORDER_DETAIL_ID    IN ZJ_TRIP_VENDOR_ORDER.ORDER_DETAIL_ID%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_ZONE_JUMP_CARRIERS
-- Type:    Function
-- Syntax:  GET_ZONE_JUMP_CARRIERS (product_subcode_id,delivery_date,order_detail_id)
-- Returns: ref_cursor for
--
-- Description:   Checks for availability of zone jump injection hubs for order
--                based on the passed parameters.
-- Assumptions:   Vendor delivery trip setup has taken into account of
--      vendor ship block and vendor delivery block.
-- 
-- 11/14/2008: Shipment Diversion changes (#5388) - Do not check for vendor product
--	       availability. Exlude removed vendor product.
--
--==============================================================================
AS
  carrier_cursor types.ref_cursor;
BEGIN
    OPEN carrier_cursor FOR
    SELECT DISTINCT
                zih.CARRIER_ID,
                zih.SHIP_POINT_ID,
                zt.DEPARTURE_DATE "ZONE_JUMP_DATE",
                zt.DEPARTURE_DATE + zt.DAYS_IN_TRANSIT_QTY "SHIP_DATE"
    FROM FTD_APPS.VENDOR_MASTER vm
    JOIN FTD_APPS.VENDOR_PRODUCT vp ON vm.VENDOR_ID = vp.VENDOR_ID
    JOIN VENUS.ZJ_TRIP_VENDOR ztv ON ztv.VENDOR_ID = vm.VENDOR_ID
     AND ztv.TRIP_VENDOR_STATUS_CODE = 'A'
    JOIN VENUS.ZJ_TRIP zt ON zt.TRIP_ID = ztv.TRIP_ID
     AND zt.TRIP_STATUS_CODE = 'A'
    JOIN VENUS.ZJ_INJECTION_HUB zih ON zih.INJECTION_HUB_ID = zt.INJECTION_HUB_ID
    WHERE vp.PRODUCT_SUBCODE_ID = in_product_subcode_id
      AND vp.REMOVED = 'N'
      AND EXISTS 
      (
        select 1
        from FTD_APPS.PRODUCT_MASTER pm1
        where pm1.ZONE_JUMP_ELIGIBLE_FLAG = 'Y'
        and pm1.PRODUCT_ID =
        coalesce 
        (
          (
            select distinct ps1.PRODUCT_ID
            from FTD_APPS.PRODUCT_SUBCODES ps1, FTD_APPS.VENDOR_PRODUCT vp1
            where vp1.PRODUCT_SUBCODE_ID = ps1.PRODUCT_SUBCODE_ID(+)
            and vp1.PRODUCT_SUBCODE_ID = IN_PRODUCT_SUBCODE_ID
          ),
          IN_PRODUCT_SUBCODE_ID
        )
      )
      AND vm.ZONE_JUMP_ELIGIBLE_FLAG = 'Y'
      AND zt.DELIVERY_DATE = in_delivery_date
      AND (zt.departure_date > SYSDATE
       OR (TRUNC(zt.departure_date) = TRUNC(SYSDATE)
      AND TO_DATE(TO_CHAR(SYSDATE, 'HH24MI'), 'HH24MI') < to_date(ztv.order_cutoff_time, 'HH24MI')))
    ORDER BY zih.carrier_id
    ;


  RETURN carrier_cursor;
END GET_ZONE_JUMP_HUBS;

FUNCTION GET_ZONE_JUMP_VENDOR_BY_HUB
(
IN_PRODUCT_SUBCODE_ID IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
IN_SHIP_DATE          IN VENUS.SHIP_DATE%TYPE,
IN_CARRIER_ID         IN ZJ_INJECTION_HUB.CARRIER_ID%TYPE,
IN_SHIP_POINT_ID      IN ZJ_INJECTION_HUB.SHIP_POINT_ID%TYPE,
IN_DELIVERY_DATE      IN ZJ_TRIP.DELIVERY_DATE%TYPE,
IN_SDS_SHIP_METHOD    IN SDS_SHIP_METHOD_XREF.SDS_SHIP_METHOD%TYPE
)
RETURN TYPES.ref_cursor
--==============================================================================
--
-- Name:    GET_ZONE_JUMP_VENDOR_BY_HUB
-- Type:    Function
-- Syntax:  GET_ZONE_JUMP_VENDOR_BY_HUB (product_subcode_id,delivery_date,carrier_id, ship_point_id)
-- Returns: ref_cursor for
--
--
-- Description:   Checks for availability of zone jump vendors to fulfill orders
--                based on the passed parameters.
--
--==============================================================================
AS
  vendor_cursor types.ref_cursor;
  v_height number;
  v_width number;
  v_length number;

BEGIN

  OPEN vendor_cursor FOR
    SELECT      vm.VENDOR_ID "VENDOR_ID",
                vm.MEMBER_NUMBER "MEMBER_NUMBER",
                vp.PRODUCT_SUBCODE_ID "PRODUCT_SUBCODE_ID",
                vp.VENDOR_COST "VENDOR_COST",
                vp.VENDOR_SKU "VENDOR_SKU",
                vp.AVAILABLE "AVAILABLE",
                ps.SUBCODE_DESCRIPTION "SUBCODE_DESCRIPTION",
                zih.CARRIER_ID "OVERRIDE_CARRIER",
                zt.TRIP_ID,
                zt.ZONE_JUMP_TRAILER_NUMBER,
                zih.SDS_ACCOUNT_NUM "BILLING_ACCOUNT",
                to_char(zt.DEPARTURE_DATE, 'MM/dd/yyyy') DEPARTURE_DATE,
                vm.address1,
                vm.address2,
                vm.city,
                vm.state,
                vm.zip_code
    FROM FTD_APPS.VENDOR_MASTER vm
    JOIN FTD_APPS.VENDOR_PRODUCT vp ON vm.VENDOR_ID = vp.VENDOR_ID
    JOIN VENUS.ZJ_TRIP_VENDOR ztv ON ztv.VENDOR_ID = vm.VENDOR_ID
     AND ztv.TRIP_VENDOR_STATUS_CODE = 'A'
    JOIN VENUS.ZJ_TRIP zt ON zt.TRIP_ID = ztv.TRIP_ID
     AND zt.TRIP_STATUS_CODE = 'A'
    JOIN VENUS.ZJ_INJECTION_HUB zih ON zih.INJECTION_HUB_ID = zt.INJECTION_HUB_ID
    LEFT OUTER JOIN FTD_APPS.PRODUCT_SUBCODES ps
      ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
    join ftd_apps.inv_trk it
      on it.product_id = vp.product_subcode_id
     and it.vendor_id = vm.vendor_id
     and zt.DEPARTURE_DATE between it.start_date and it.end_date
     and it.status = 'A'
   WHERE vp.PRODUCT_SUBCODE_ID= in_product_subcode_id
     AND vp.AVAILABLE='Y'
      AND EXISTS 
      (
        select 1
        from FTD_APPS.PRODUCT_MASTER pm1
        where pm1.ZONE_JUMP_ELIGIBLE_FLAG = 'Y'
        and pm1.PRODUCT_ID =
        coalesce 
        (
          (
            select distinct ps1.PRODUCT_ID
            from FTD_APPS.PRODUCT_SUBCODES ps1, FTD_APPS.VENDOR_PRODUCT vp1
            where vp1.PRODUCT_SUBCODE_ID = ps1.PRODUCT_SUBCODE_ID(+)
            and vp1.PRODUCT_SUBCODE_ID = IN_PRODUCT_SUBCODE_ID
          ),
          IN_PRODUCT_SUBCODE_ID
        )
      )
     AND vm.ZONE_JUMP_ELIGIBLE_FLAG = 'Y'
     AND zih.CARRIER_ID = in_carrier_id
     AND zih.SHIP_POINT_ID = in_ship_point_id 
     AND zt.DEPARTURE_DATE = in_ship_date - zt.DAYS_IN_TRANSIT_QTY
     AND zt.DELIVERY_DATE = in_delivery_date
     AND (  zt.departure_date > SYSDATE
            OR (      TRUNC(zt.departure_date) = TRUNC(SYSDATE)
                  AND TO_DATE(TO_CHAR(SYSDATE, 'HH24MI'), 'HH24MI') < to_date(ztv.order_cutoff_time, 'HH24MI')
               )
         )
     AND (ztv.PRTL_PLT_IN_USE_FLAG = 'Y' or
          (ztv.PRTL_PLT_IN_USE_FLAG = 'N' and ((zt.ftd_plt_qty - ftd_plt_used_qty) > 0))
         )

     AND NOT EXISTS 
     (
        SELECT 1 FROM FTD_APPS.VENDOR_PRODUCT_OVERRIDE vpo
         WHERE vpo.PRODUCT_SUBCODE_ID = vp.PRODUCT_SUBCODE_ID
           AND vpo.VENDOR_ID = vp.VENDOR_ID
           AND vpo.DELIVERY_OVERRIDE_DATE = IN_DELIVERY_DATE
           AND vpo.SDS_SHIP_METHOD = IN_SDS_SHIP_METHOD
           AND vpo.CARRIER_ID = IN_CARRIER_ID
     )
     ORDER BY zt.ZONE_JUMP_TRAILER_NUMBER;
 RETURN vendor_cursor;

END GET_ZONE_JUMP_VENDOR_BY_HUB;

END ZONEJUMP_PKG;
.
/
