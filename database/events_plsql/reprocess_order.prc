CREATE OR REPLACE
PROCEDURE events.REPROCESS_ORDER (
in_order_detail_id varchar2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
/*-----------------------------------------------------------------------------
 This procedure will place an event in to the JMS queue.  An empty payload is
 inserted.
-----------------------------------------------------------------------------*/
AS
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
BEGIN

      --Reset the order reject-retry count to zero to prevent automatic assignment to REJ queue.
      UPDATE clean.order_details
      SET reject_retry_count = 0, op_status = 'Pending', florist_id = null
      WHERE order_detail_id = in_order_detail_id;

      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      message.set_text(payload => in_order_detail_id );

      message_properties.CORRELATION := in_order_detail_id;

dbms_aq.enqueue (queue_name => 'OJMS.PROCESS_ORDER',
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_DELIVERY_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END REPROCESS_ORDER;
.
/