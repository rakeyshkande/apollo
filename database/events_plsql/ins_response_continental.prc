CREATE OR REPLACE
PROCEDURE events.ins_response_continental IS
/* Insert an event to pick up Continental Airlines response file.
   The job context is INS_RESPONSE_CONTINENTAL. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-CONTINENTAL-AIRLINES',
      in_payload         => '<![CDATA[CONTINENTAL AIRLINES|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_continental;
.
/
