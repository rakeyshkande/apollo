CREATE OR REPLACE
PROCEDURE events.ins_novator_send_ask_messages IS
/* Insert an event to start the Novator send-ask messages process.
   The job context is ins_novator_send_ask_messages. */
BEGIN
   enqueue (
      in_context_name    => 'NOVATOR_EMAIL_REQUEST',
      in_event_name      => 'SEND-ASK-MESSAGES',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_novator_send_ask_messages;
.
/
