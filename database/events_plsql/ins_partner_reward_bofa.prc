  CREATE OR REPLACE PROCEDURE "EVENTS"."INS_PARTNER_REWARD_BOFA"  IS
/* Insert an event to bankofamerica partner reward program.
   The job context is ins_partner_reward_bofa. */

   v_start_date VARCHAR2(8);
   v_end_date   VARCHAR2(8);
   v_start_date_offset varchar(8);
   

BEGIN

   -- last day of the previous month.
   v_end_date := to_char(trunc(sysdate,'mm') - 1,'mmddyyyy');
   v_start_date_offset := frp.misc_pkg.get_global_parm_value('PARTNER_REWARD_CONFIG','BOFA_START_DATE_OFFSET');

   -- if offset is 30, the report start date should be the first day of last month.
   if v_start_date_offset = '30' then
   	v_start_date := to_char(add_months(trunc(sysdate,'mm'),-1), 'mmddyyyy');
   else 
   	v_start_date := to_char(trunc(sysdate,'mm') - to_number(v_start_date_offset),'mmddyyyy');
   end if;
   
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-BOFA-DATA',
      in_payload         => '<![CDATA[BOFA|'||v_start_date||'|'||v_end_date||']]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END INS_PARTNER_REWARD_BOFA;
/