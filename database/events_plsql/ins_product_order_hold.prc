CREATE OR REPLACE
PROCEDURE events.ins_product_order_hold IS
/* Insert an event to initiate product order hold processing.
   The job context is ins_product_order_hold. */
BEGIN
   enqueue (
      in_context_name    => 'VENUS',
      in_event_name      => 'PRODUCT_ORDER_HOLD',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_product_order_hold;
.
/
