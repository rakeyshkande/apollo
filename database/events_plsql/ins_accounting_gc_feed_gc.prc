CREATE OR REPLACE
PROCEDURE events.ins_accounting_gc_feed_gc   IS
/* Insert an event for Accounting GC outbound inventory feed.
   The job context is ins_accounting_gc_feed_gc. */

BEGIN

   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'GC-FEED-GC',
      in_payload         => '<![CDATA[<root><event>GC-FEED-GC</event><partner-program>DISC2</partner-program></root>]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_accounting_gc_feed_gc;
.
/
