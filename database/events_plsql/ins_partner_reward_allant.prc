CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_allant IS
/* Insert an event to Allant partner reward program.
   The job context is ins_partner_reward_allant. */

   v_start_date VARCHAR2(8);
   v_end_date   VARCHAR2(8);

BEGIN

   v_start_date := to_char(last_day(add_months(sysdate,-2))+1,'MMDDYYYY');
   v_end_date   := to_char(last_day(add_months(sysdate,-1)),  'MMDDYYYY');
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-ALLANT-ORDER-DATA',
      in_payload         => '<![CDATA[ALLANT|'||v_start_date||'|'||v_end_date||']]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_allant;
.
/
