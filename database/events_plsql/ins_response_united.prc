CREATE OR REPLACE
PROCEDURE events.ins_response_united    IS
/* Insert an event to United Airlines partner response file.
   The job context is INS_RESPONSE_UNITED. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-UNITED-AIRLINES',
      in_payload         => '<![CDATA[UNITED AIRLINES|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_united;
.
/
