CREATE OR REPLACE
PROCEDURE events.ins_susres IS
/* Insert an event to initiate Mercury suspend/resume florist report upload.
   The job context is ins_susres. */
BEGIN
   enqueue (
      in_context_name    => 'ORDER_PROCESSING',
      in_event_name      => 'SUSRES',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_susres;
.
/
