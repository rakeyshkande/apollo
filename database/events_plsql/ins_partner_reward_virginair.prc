CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_virginair IS
/* Insert an event to Virgin Airways partner reward program.
   The job context is INS_PARTNER_REWARD_VIRGINAIR. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-VIRGIN-AIRWAYS',
      in_payload         => '<![CDATA[VIRGIN AIRWAYS]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_virginair;
.
/
