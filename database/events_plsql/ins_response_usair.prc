CREATE OR REPLACE
PROCEDURE events.ins_response_usair     IS
/* Insert an event to USAirways partner response file.
   The job context is INS_RESPONSE_USAIR. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-US-AIRWAYS',
      in_payload         => '<![CDATA[US AIRWAYS|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_usair;
.
/
