CREATE OR REPLACE
PROCEDURE events.ins_amazon_remittance_process IS
/* Insert an event to initiate Amazon remittance processing.
   The job context is ins_amazon_remittance_process. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'AMAZON-REMITTANCE-PROCESS',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_amazon_remittance_process;
.
/
