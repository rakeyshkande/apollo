  CREATE OR REPLACE PROCEDURE "EVENTS"."INS_PARTNER_REWARD_SECONDBITE" IS
/* Insert an event to Second Bite partner reward program.
   The job context is ins_partner_reward_secondbite. */

   v_start_date VARCHAR2(8);
   v_end_date   VARCHAR2(8);

BEGIN

   v_start_date := to_char(trunc(sysdate) - 3,'MMDDYYYY');
   v_end_date   := to_char(trunc(sysdate), 'MMDDYYYY');
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-SECOND-BITE-EMAIL-DATA',
      in_payload         => '<![CDATA[SECOND BITE|'||v_start_date||'|'||v_end_date||']]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END INS_PARTNER_REWARD_SECONDBITE;
.
/