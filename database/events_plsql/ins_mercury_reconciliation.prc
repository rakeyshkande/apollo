CREATE OR REPLACE
PROCEDURE events.ins_mercury_reconciliation IS
/* Insert an event to initiate Mercury reconciliation.
   The job context is ins_mercury_reconciliation. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'RECON-MERCURY',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_mercury_reconciliation;
.
/
