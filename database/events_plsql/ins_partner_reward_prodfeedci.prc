CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_prodfeedci IS
/* Insert an event to Product Data Feed partner reward program.
   The job context is ins_partner_reward_productfeed. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-PROD-DATA-FEED-CI',
      in_payload         => '<![CDATA[PRODFEEDCI]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_prodfeedci;
/
