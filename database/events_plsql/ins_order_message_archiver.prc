CREATE OR REPLACE
PROCEDURE events.ins_order_message_archiver IS
/* Insert an event to initiate archiving of Mercury/Venus reconciled messages.
   The job context is ins_order_message_archiver. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'RECON-ARCHIVE-MESSAGE',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_order_message_archiver;
.
/
