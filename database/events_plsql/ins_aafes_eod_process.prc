CREATE OR REPLACE
PROCEDURE events.ins_aafes_eod_process IS
/* Insert an event to initiate AAFES end-of-day processing.
   The job context is ins_aafes_eod_process. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'AAFES-EOD-PROCESS',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_aafes_eod_process;
.
/
