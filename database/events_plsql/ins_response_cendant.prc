CREATE OR REPLACE
PROCEDURE events.ins_response_cendant IS
/* Insert an event to pick up Cendant response file.
   The job context is INS_RESPONSE_CENDANT.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-CENDANT-HOTELS',
      in_payload         => '<![CDATA[CENDANT HOTELS|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_cendant;
.
/
