CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_upromise IS
/* Insert an event to Upromise partner reward program.
   The job context is INS_PARTNER_REWARD_UPROMISE. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-UPROMISE',
      in_payload         => '<![CDATA[UPROMISE]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_upromise;
.
/
