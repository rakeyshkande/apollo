CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_quixtar IS
/* Insert an event to Quixtar partner reward program.
   The job context is ins_partner_reward_quixtar. */

   v_start_date VARCHAR2(8);

BEGIN

   v_start_date := to_char(last_day(add_months(sysdate,-1)),'MMDDYYYY');
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-QUIXTAR/FLORAGIFT',
      in_payload         => '<![CDATA[QUIXTAR/FLORAGIFT|'||v_start_date||']]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_quixtar;
.
/
