CREATE OR REPLACE
PROCEDURE events.INSERT_EVENT  (
  IN_CONTEXT_NAME IN EVENTS.CONTEXT_NAME%TYPE,
  IN_EVENT_NAME IN EVENTS.EVENT_NAME%TYPE,
  IN_TIMEOUT_SECONDS IN NUMBER
)
/*-----------------------------------------------------------------------------
 This procedure will place an event in to the JMS queue.  An empy payload is
 inserted.
-----------------------------------------------------------------------------*/
as
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'AMAZON');

      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>' || IN_EVENT_NAME || '</event-name><context>' || IN_CONTEXT_NAME || '</context><payload></payload></event>' );

      message_properties.CORRELATION := 'AMAZON ' || IN_EVENT_NAME;

      --Sex seconds before message expires
      message_properties.EXPIRATION := IN_TIMEOUT_SECONDS;

dbms_aq.enqueue (queue_name => events_pkg.get_queue_name(in_context_name),
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;
.
/
