CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_benevolink IS
/* Insert an event to run the Benevolink post file.
   The job context is ins_partner_reward_benevolink. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-BENEVOLINK',
      in_payload         => '<![CDATA[BENEVOLINK]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_benevolink;
.
/
