CREATE OR REPLACE
PACKAGE BODY events.EVENTS_PKG AS

PROCEDURE LOG_EVENT (
    IN_EVENT_NAME     EVENTS.EVENT_NAME%TYPE,
    IN_CONTEXT_NAME   EVENTS.CONTEXT_NAME%TYPE,
    IN_STARTED EVENTS_LOG.STARTED%TYPE,
    IN_ENDED EVENTS_LOG.ENDED%TYPE,
    IN_STATUS EVENTS_LOG.STATUS%TYPE,
    IN_MESSAGE EVENTS_LOG.MESSAGE%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2
  )
AS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  INSERT INTO EVENTS_LOG (
              EVENT_NAME,
              CONTEXT_NAME,
              STARTED,
              ENDED,
              STATUS,
              MESSAGE)
        VALUES(
              IN_EVENT_NAME,
              IN_CONTEXT_NAME,
              IN_STARTED,
              IN_ENDED,
              IN_STATUS,
              IN_MESSAGE);

OUT_STATUS := 'Y';
OUT_MESSAGE := 'SUCCESS';

COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END; -- end exception block

END LOG_EVENT;


/*-----------------------------------------------------------------------------*/

PROCEDURE GET_EVENT (
    IN_EVENT_NAME     EVENTS.EVENT_NAME%TYPE,
    IN_CONTEXT_NAME   EVENTS.CONTEXT_NAME%TYPE,
    OUT_CUR           OUT TYPES.REF_CURSOR
  )AS

-- Declare variables here
V_IS_CONTEXT_ACTIVE CHAR;

BEGIN
OPEN OUT_CUR FOR
    SELECT EVENTS.EVENT_NAME,
           EVENTS.CONTEXT_NAME,
           EVENTS.DESCRIPTION,
           DECODE (CONTEXT.ACTIVE, 'Y', EVENTS.ACTIVE, 'N') AS ACTIVE
    FROM EVENTS, CONTEXT
    WHERE EVENTS.EVENT_NAME = IN_EVENT_NAME
        AND EVENTS.CONTEXT_NAME = IN_CONTEXT_NAME
        AND EVENTS.CONTEXT_NAME = CONTEXT.CONTEXT_NAME;

END GET_EVENT;

FUNCTION get_queue_name (in_context context.context_name%type) return context.queue_name%type is
   v_out_queue context.queue_name%type;
begin
   select queue_name
   into   v_out_queue
   from   events.context
   where  context_name = in_context;

   return v_out_queue;
end get_queue_name;

END EVENTS_PKG;
.
/
