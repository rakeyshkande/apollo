CREATE OR REPLACE
PROCEDURE events.ins_ua_redemption_reject IS
/* Insert a message to ALTPAY_EOD queue. */
v_status varchar2(1);
v_message varchar2(2000);

BEGIN

   post_a_message_flex(
	IN_QUEUE_NAME     => 'OJMS.ALTPAY_EOD', 
  	IN_CORRELATION_ID => 'UA_REJECT',
  	IN_PAYLOAD        => 'ua_reject', 
  	IN_DELAY_SECONDS  => 0,
  	OUT_STATUS        => v_status, 
  	OUT_MESSAGE       => v_message
   );
   commit;


END ins_ua_redemption_reject;
.
/
