CREATE OR REPLACE
PROCEDURE events.ins_response_american_billing IS
/* Insert an event to pick up American Airlines billing response file.
   The job context is INS_RESPONSE_AMERICAN_BILLING. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-AMERICAN-AIRLINES-BILLING',
      in_payload         => '<![CDATA[AMERICAN AIRLINES|nodeResponseExtractBilling]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_american_billing;
.
/
