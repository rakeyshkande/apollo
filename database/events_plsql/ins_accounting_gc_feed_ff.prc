CREATE OR REPLACE
PROCEDURE events.ins_accounting_gc_feed_ff   IS
/* Insert an event for Accounting GC inbound fulfillment feed.
   The job context is ins_accounting_gc_feed_ff. */

BEGIN

   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'GC-FEED-FF',
      in_payload         => '<![CDATA[<root><event>GC-FEED-FF</event><partner-program>DISC2</partner-program></root>]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_accounting_gc_feed_ff;
.
/
