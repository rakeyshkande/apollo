CREATE OR REPLACE
PROCEDURE events.POST_A_MESSAGE (
in_order_detail_id varchar2,
in_queue           varchar2
)
/*-----------------------------------------------------------------------------
 This procedure will place an event in to the JMS queue.  An empy payload is
 inserted.
-----------------------------------------------------------------------------*/
as
enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;
begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      message.set_text(payload => in_order_detail_id );

      message_properties.CORRELATION := in_order_detail_id;

dbms_aq.enqueue (queue_name => in_queue,
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);
commit;
end;
.
/
