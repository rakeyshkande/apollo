CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_usair IS
/* Insert an event to US Airways partner reward program.
   The job context is INS_PARTNER_REWARD_USAIR. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-US-AIRWAYS',
      in_payload         => '<![CDATA[US AIRWAYS]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_usair;
.
/
