CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_aaa IS
/* Insert an event to AAA partner reward program.
   The job context is ins_partner_reward_aaa. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-AMERICAN-AUTO-ASSOC.',
      in_payload         => '<![CDATA[AMERICAN AUTO ASSOC.]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_aaa;
.
/
