CREATE OR REPLACE
PROCEDURE events.JOB_VENUS_SAMS_CUSTOMER_FILE
AS

/*-----------------------------------------------------------------------------
Description:
             This stored procedure when executed will insert a record into
             the events queue table.

Input:

Output:

-----------------------------------------------------------------------------*/

enqueue_options dbms_aq.enqueue_options_t;
message_properties dbms_aq.message_properties_t;
message_handle raw(2000);
message sys.aq$_jms_text_message;

BEGIN
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'VENUS');

      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>VENUS_CUSTOMER_FILE</event-name><context>VENUS</context><payload>00119</payload></event>' );

dbms_aq.enqueue (queue_name => events_pkg.get_queue_name('VENUS'),
enqueue_options => enqueue_options,
message_properties => message_properties,
payload => message,
msgid => message_handle);


COMMIT;


END JOB_VENUS_SAMS_CUSTOMER_FILE;
.
/
