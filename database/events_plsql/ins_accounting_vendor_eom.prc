CREATE OR REPLACE
PROCEDURE events.ins_accounting_vendor_eom IS
/* Insert an event to initiate Vendor End of Month process.
   The job context is ins_accounting_vendor_eom. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'VENDOR-EOM',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_accounting_vendor_eom;
.
/
