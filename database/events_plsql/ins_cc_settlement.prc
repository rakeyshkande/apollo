CREATE OR REPLACE
PROCEDURE events.ins_cc_settlement IS
/* Insert an event to initiate credit card settlement processing.
   The job context is ins_cc_settlement. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'RECON-CC',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_cc_settlement;
.
/
