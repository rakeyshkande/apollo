CREATE OR REPLACE
PROCEDURE events.ins_sequence_check IS
/* Insert an event to run the sequence check utility. */

BEGIN

   enqueue (
      in_context_name    => 'SEQUENCE-CHECK',
      in_event_name      => 'RUN-SEQUENCE-CHECK',
      in_payload         => '<![CDATA[SEQUENCE-CHECK]]>',
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_sequence_check;
.
/
