  CREATE OR REPLACE PROCEDURE "EVENTS"."INS_PARTNER_REWARD_MYPOINTS"  IS
/* Insert an event to My Points partner reward program.
   The job context is ins_partner_reward_mypoints. */

   v_start_date VARCHAR2(8);
   v_end_date   VARCHAR2(8);
   
   w_days_back  NUMBER;

BEGIN

   w_days_back := frp.misc_pkg.get_global_parm_value('ins_partner_reward_mypoints','DAYS_BACK_TO_SEARCH');
   if w_days_back is null then
      w_days_back := 7;
   end if;

   v_start_date := to_char(trunc(sysdate) - w_days_back,'MMDDYYYY');
   v_end_date   := to_char(trunc(sysdate), 'MMDDYYYY');
   
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-MYPOINTS-DATA',
      in_payload         => '<![CDATA[MYPOINTS|'||v_start_date||'|'||v_end_date||']]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END INS_PARTNER_REWARD_MYPOINTS;
/