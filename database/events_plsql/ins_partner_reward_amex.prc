CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_amex IS
/* Insert an event to American Express partner reward program.
   The job context is INS_PARTNER_REWARD_AMEX. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-AMERICAN-EXPRESS',
      in_payload         => '<![CDATA[AMERICAN EXPRESS]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_amex;
.
/
