CREATE OR REPLACE
PACKAGE BODY events.daemon_pkg AS

   pg_this_package VARCHAR2(61) := 'events.daemon_pkg';

FUNCTION CURRENTLY_RUNNING (p_job_name IN VARCHAR2) RETURN BOOLEAN IS

/*-----------------------------------------------------------------------------
Description:
        Function looks for the daemon in the DBMS_JOB queue.  Returns
        a TRUE if it is found, or a FALSE if it is not.

Parameter:
        the name of the job, as found in user_jobs.what, including any parameters.

Returns:
        BOOLEAN value indicating if job is already in the queue.
-----------------------------------------------------------------------------*/

   v_check    NUMBER;
   v_return   BOOLEAN;

BEGIN

   SELECT COUNT(*)
   INTO   v_check
   FROM   user_jobs
   WHERE  lower(what) like lower(p_job_name)||'%';

   IF v_check > 0 THEN
      v_return := TRUE;
   ELSE
      v_return := FALSE;
   END IF;

   RETURN V_RETURN;

END CURRENTLY_RUNNING;

PROCEDURE submit (p_global_context IN VARCHAR2,
                  p_events_context IN events.context_name%TYPE,
                  p_timeout_seconds IN NUMBER) IS

/*-----------------------------------------------------------------------------
Description:
        submit a scheduled job under the Events schema
        p_global_context is required.  This should match frp.global_parms.context
           where the global parameters for this job are stored, which should also
           match the name of the actual job.
        p_events_context is optional, but must be specified if p_timeout_seconds is specified.
           This is the Events framework context, if used.
        p_timeout_seconds is optional.  This is the timeout period for the job, if used.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(300);
   v_submission_time       DATE;

BEGIN

   if p_timeout_seconds is null then
      if p_events_context is null then
         v_submit_string := p_global_context||';';
      else
         v_submit_string := p_global_context||'('''||p_events_context||''');';
      end if;
   else
      v_submit_string := p_global_context||'('''||p_events_context||''','||to_char(p_timeout_seconds)||');';
   end if;

   -- Make sure daemon is not already running in the queue.
   IF currently_running (p_global_context) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(p_global_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit;



PROCEDURE remove (p_global_context IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        This will look for a user job LIKE p_global_context, without
        taking any parameters into account.  If there's more than one, it
        removes the first one (lowest job number).
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  lower(what) LIKE lower(p_global_context||'%');

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_global_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||p_global_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove;


PROCEDURE sb_event_walmart_refund IS

/*-----------------------------------------------------------------------------
Description:
        submit job_pop_event_walmart_refund
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'sb_event_walmart_refund';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   v_submit_string := 'JOB_POP_EVENT_WALMART_REFUND;';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value('event_walmart_refund','INTERVAL');
      dbms_job.submit(v_job, v_submit_string, SYSDATE, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END sb_event_walmart_refund;



PROCEDURE rm_event_walmart_refund IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'rm_event_walmart_refund';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = 'JOB_POP_EVENT_WALMART_REFUND;';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' JOB_POP_EVENT_WALMART_REFUND REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' JOB_POP_EVENT_WALMART_REFUND NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END rm_event_walmart_refund;


FUNCTION venus_rich_outbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24mi'));
   IF v_time < 900 THEN
      v_return := TRUNC(SYSDATE)+(9/24);
   ELSIF v_time < 1058 THEN
      v_return := TRUNC(SYSDATE)+(10/24)+(58/1440);
   ELSIF v_time < 1200 THEN
      v_return := TRUNC(SYSDATE)+(12/24);
   ELSIF v_time < 1400 THEN
      v_return := TRUNC(SYSDATE)+(14/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(9/24);
   END IF;
   RETURN v_return;
END venus_rich_outbound_interval;


FUNCTION venus_wine_outbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24mi'));
   IF v_time < 900 THEN
      v_return := TRUNC(SYSDATE)+(9/24);
   ELSIF v_time < 1058 THEN
      v_return := TRUNC(SYSDATE)+(10/24)+(58/1440);
   ELSIF v_time < 1200 THEN
      v_return := TRUNC(SYSDATE)+(12/24);
   ELSIF v_time < 1400 THEN
      v_return := TRUNC(SYSDATE)+(14/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(9/24);
   END IF;
   RETURN v_return;
END venus_wine_outbound_interval;


FUNCTION venus_sams_outbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24mi'));
   IF v_time < 730 THEN
      v_return := TRUNC(SYSDATE)+(7/24)+(30/1440);
   ELSIF v_time < 1030 THEN
      v_return := TRUNC(SYSDATE)+(10/24)+(30/1440);
   ELSIF v_time < 1400 THEN
      v_return := TRUNC(SYSDATE)+(14/24);
   ELSIF v_time < 1700 THEN
      v_return := TRUNC(SYSDATE)+(17/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(7/24)+(30/1440);
   END IF;
   RETURN v_return;
END venus_sams_outbound_interval;


FUNCTION partner_reward_delta_interval RETURN DATE IS
   v_date NUMBER;
   v_return DATE;
BEGIN
   v_date := TO_NUMBER(TO_CHAR(SYSDATE, 'dd'));
   IF v_date < 7 THEN
      v_return := TRUNC(SYSDATE,'Month')+6+(10/1440);
   ELSIF v_date < 21 THEN
      v_return := TRUNC(SYSDATE,'Month')+20+(10/1440);
   ELSE
      v_return := TRUNC(LAST_DAY(SYSDATE))+7+(10/1440);
   END IF;
   RETURN v_return;
END partner_reward_delta_interval;


FUNCTION partner_reward_amex_interval RETURN DATE IS
   v_date NUMBER;
   v_return DATE;
BEGIN
   v_date := TO_NUMBER(TO_CHAR(SYSDATE, 'dd'));
   IF v_date < 3 THEN
      v_return := TRUNC(SYSDATE,'Month')+2+(10/1440);
   ELSIF v_date < 18 THEN
      v_return := TRUNC(SYSDATE,'Month')+17+(10/1440);
   ELSE
      v_return := TRUNC(LAST_DAY(SYSDATE))+3+(10/1440);
   END IF;
   RETURN v_return;
END partner_reward_amex_interval;


FUNCTION partner_continental_interval RETURN DATE IS
   v_date NUMBER;
   v_return DATE;
BEGIN
   return case when to_number(to_char(sysdate,'dd')) < 5 then trunc(sysdate,'mm')+4+(20/1440)
               when to_number(to_char(sysdate,'dd')) >= 15 then add_months(trunc(sysdate,'mm'),1)+4+(20/1440)
               else trunc(sysdate,'mm')+14+(20/1440)
          end;
END partner_continental_interval;


FUNCTION response_continental_interval RETURN DATE IS
   v_date NUMBER;
   v_return DATE;
BEGIN
   return case when to_number(to_char(sysdate,'dd')) < 7 then trunc(sysdate,'mm')+6+(12/24)+(20/1440)
               when to_number(to_char(sysdate,'dd')) >= 17 then add_months(trunc(sysdate,'mm'),1)+6+(12/24)+(20/1440)
               else trunc(sysdate,'mm')+16+(12/24)+(20/1440)
          end;
END response_continental_interval;

FUNCTION venus_pm_outbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24'));
   IF v_time < 6 THEN
      v_return := TRUNC(SYSDATE)+(6/24);
   ELSIF v_time < 11 THEN
      v_return := TRUNC(SYSDATE)+(11/24);
   ELSIF v_time < 14 THEN
      v_return := TRUNC(SYSDATE)+(14/24);
   ELSIF v_time < 18 THEN
      v_return := TRUNC(SYSDATE)+(18/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(6/24);
   END IF;
   RETURN v_return;
END venus_pm_outbound_interval;


FUNCTION venus_pm_inbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24'));
   IF v_time < 7 THEN
      v_return := TRUNC(SYSDATE)+(7/24);
   ELSIF v_time < 12 THEN
      v_return := TRUNC(SYSDATE)+(12/24);
   ELSIF v_time < 15 THEN
      v_return := TRUNC(SYSDATE)+(15/24);
   ELSIF v_time < 20 THEN
      v_return := TRUNC(SYSDATE)+(20/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(7/24);
   END IF;
   RETURN v_return;
END venus_pm_inbound_interval;

FUNCTION venus_pc_outbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24'));
   IF v_time < 6 THEN
      v_return := TRUNC(SYSDATE)+(6/24);
   ELSIF v_time < 11 THEN
      v_return := TRUNC(SYSDATE)+(11/24);
   ELSIF v_time < 14 THEN
      v_return := TRUNC(SYSDATE)+(14/24);
   ELSIF v_time < 18 THEN
      v_return := TRUNC(SYSDATE)+(21/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(6/24);
   END IF;
   RETURN v_return;
END venus_pc_outbound_interval;


FUNCTION venus_pc_inbound_interval RETURN DATE IS
   v_time NUMBER;
   v_return DATE;
BEGIN
   v_time := TO_NUMBER(TO_CHAR(SYSDATE,'hh24'));
   IF v_time < 7 THEN
      v_return := TRUNC(SYSDATE)+(7/24);
   ELSIF v_time < 12 THEN
      v_return := TRUNC(SYSDATE)+(12/24);
   ELSIF v_time < 15 THEN
      v_return := TRUNC(SYSDATE)+(15/24);
   ELSIF v_time < 19 THEN
      v_return := TRUNC(SYSDATE)+(22/24);
   ELSE
      v_return := TRUNC(SYSDATE+1)+(7/24);
   END IF;
   RETURN v_return;
END venus_pc_inbound_interval;


END daemon_pkg;
/
