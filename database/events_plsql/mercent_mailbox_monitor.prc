create or replace
PROCEDURE   events.MERCENT_MAILBOX_MONITOR  AS
/* Fetch the events and start the Novator mailbox monitor for each event.
   The job context is ins_novator_mailbox_monitor. */
   v_mailbox_name VARCHAR2(100);
   OUT_MAILBOX_CUR      types.REF_CURSOR;
BEGIN
OPEN OUT_MAILBOX_CUR FOR
	SELECT  MAILBOX_NAME
		from PTN_MERCENT.MRCNT_CHANNEL_MAPPING;
LOOP
FETCH  OUT_MAILBOX_CUR INTO v_mailbox_name;
EXIT WHEN OUT_MAILBOX_CUR%NOTFOUND;
  INS_NOVATOR_MAILBOX_MONITOR(v_mailbox_name);
END LOOP;
CLOSE OUT_MAILBOX_CUR;
END MERCENT_MAILBOX_MONITOR;
.
/