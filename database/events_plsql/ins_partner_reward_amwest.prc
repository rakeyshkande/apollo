CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_amwest IS
/* Insert an event to America West partner reward program.
   The job context is INS_PARTNER_REWARD_AMWEST. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-AMERICA-WEST',
      in_payload         => '<![CDATA[AMERICA WEST]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_amwest;
.
/
