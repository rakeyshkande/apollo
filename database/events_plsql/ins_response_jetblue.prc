CREATE OR REPLACE
PROCEDURE events.ins_response_jetblue IS
/* Insert an event to pick up JetBlue Airlines response file(s).
   The job context is INS_RESPONSE_JETBLUE.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-JETBLUE',
      in_payload         => '<![CDATA[JETBLUE|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_jetblue;
.
/
