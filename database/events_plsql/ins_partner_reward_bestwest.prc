CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_bestwest IS
/* Insert an event to Best Western partner reward program.
   The job context is INS_PARTNER_REWARD_BESTWEST. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-BEST-WESTERN',
      in_payload         => '<![CDATA[BEST WESTERN]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_bestwest;
.
/
