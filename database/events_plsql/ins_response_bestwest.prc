CREATE OR REPLACE
PROCEDURE events.ins_response_bestwest IS
/* Insert an event to pick up Best Western response file.
   The job context is INS_RESPONSE_BESTWEST.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-BEST-WESTERN',
      in_payload         => '<![CDATA[BEST WESTERN|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_bestwest;
.
/
