CREATE OR REPLACE
PROCEDURE events.ins_venus_reconciliation IS
/* Insert an event to initiate Venus reconciliation.
   The job context is ins_venus_reconciliation. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'RECON-VENUS',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_venus_reconciliation;
.
/
