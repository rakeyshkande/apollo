CREATE OR REPLACE
PROCEDURE events.INS_AZ_PRICE_FEED_EVENT  (
  IN_CONTEXT_NAME IN EVENTS.CONTEXT_NAME%TYPE,
  IN_TIMEOUT_SECONDS IN NUMBER
)
as

/*-----------------------------------------------------------------------------
  This procedure will create an Amazon price feed event by calling
  INSERT_EVENT.
-----------------------------------------------------------------------------*/
begin
 INSERT_EVENT(IN_CONTEXT_NAME,'PRICE-FEED', IN_TIMEOUT_SECONDS);
end INS_AZ_PRICE_FEED_EVENT;
.
/
