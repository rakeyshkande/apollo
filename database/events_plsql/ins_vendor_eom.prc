create or replace procedure events.ins_vendor_eom as

   enqueue_options dbms_aq.enqueue_options_t;
   message_properties dbms_aq.message_properties_t;
   message_handle raw(2000);
   message sys.aq$_jms_text_message;

begin
      --Create a new JMS Message
      message := sys.aq$_jms_text_message.construct();

      --Set the properties in the message
      message.set_string_property(property_name => 'CONTEXT',
                                  property_value => 'ACCOUNTING');
      message.set_text(payload => '<?xml version="1.0" encoding="utf-8"?><event xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="event.xsd"><event-name>VENDOR-EOM</event-name><context>ACCOUNTING</context><payload></payload></event>' );

      dbms_aq.enqueue (queue_name => 'OJMS.EM_ACCOUNTING',
                       enqueue_options => enqueue_options,
                       message_properties => message_properties,
                       payload => message,
                       msgid => message_handle);
      commit;
end;
/
