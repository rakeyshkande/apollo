CREATE OR REPLACE
PROCEDURE events.ins_gc_feed_vanilla (
      partnerProgram in varchar2,
      status         in varchar2,
      filename       in varchar2) IS

/* Insert an event for specified partner program.
   This is not part of the job scheduler framework. */

BEGIN

   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'GC-FEED-VANILLA-INBOUND',
      in_payload         => '<![CDATA[<root><event>GC-FEED-VANILLA-INBOUND</event>'||
                            '<partner-program>'||partnerProgram||'</partner-program>'||
                            '<status>'||status||'</status>'||
                            '<filename>'||filename||'</filename></root>]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

   commit;

END ins_gc_feed_vanilla;
.
/
