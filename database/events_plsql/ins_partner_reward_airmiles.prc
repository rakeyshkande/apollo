CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_airmiles IS
/* Insert an event to AirMiles partner reward program.
   The job context is INS_PARTNER_REWARD_AIRMILES. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-AIRMILES',
      in_payload         => '<![CDATA[AIR MILES]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_airmiles;
.
/