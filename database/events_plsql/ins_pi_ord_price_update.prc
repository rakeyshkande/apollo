CREATE OR REPLACE
PROCEDURE events.ins_pi_ord_price_update IS
/* Insert a message to PARTNER_ORDERS queue. */
v_status varchar2(1);
v_message varchar2(2000);

BEGIN

   post_a_message_flex(
	 IN_QUEUE_NAME     => 'OJMS.PARTNER_ORDERS', 
  	 IN_CORRELATION_ID => 'PROCESS_CREATE_ORD_STATUS_UPDATES',
  	 IN_PAYLOAD        => 'orderNumbers:|operation:ADJUSTED', 
  	 IN_DELAY_SECONDS  => 0,
  	 OUT_STATUS        => v_status, 
  	 OUT_MESSAGE       => v_message
   );
   commit;

END ins_pi_ord_price_update;
.
/