CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_cendant IS
/* Insert an event to United partner reward program.
   The job context is INS_PARTNER_REWARD_CENDANT. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-CENDANT-HOTELS',
      in_payload         => '<![CDATA[CENDANT HOTELS]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_cendant;
.
/
