CREATE OR REPLACE
PROCEDURE events.ins_response_aircan_error IS
/* Insert an event to pick up Air Canada error response file.
   The job context is INS_RESPONSE_AIRCAN_ERROR.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-AIR-CANADA-ERROR',
      in_payload         => '<![CDATA[AIR CANADA|nodeResponseExtractError]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_aircan_error;
.
/
