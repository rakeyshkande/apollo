CREATE OR REPLACE
PROCEDURE events.ins_response_marriott IS
/* Insert an event to run the Marriott response file.
   The job context is ins_response_marriott. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-MARRIOTT',
      in_payload         => '<![CDATA[MARRIOTT]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_marriott;
.
/
