CREATE OR REPLACE PROCEDURE events.ins_partner_reward_continental IS
/* Insert an event to run the florist zip count report.
   The job context is ins_partner_reward_zipcount. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-CONTINENTAL-AIRLINES',
      in_payload         => '<![CDATA[CONTINENTAL AIRLINES]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_continental;
.
/
