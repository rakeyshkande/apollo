CREATE OR REPLACE
PROCEDURE events.ins_expire_coupon IS
/* Insert an event to process coupon expirations.
   The job context is INS_EXPIRE_COUPON. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'EXPIRE-COUPON',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_expire_coupon;
.
/
