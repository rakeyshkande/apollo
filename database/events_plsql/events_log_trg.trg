CREATE OR REPLACE TRIGGER   events.events_log_trg
AFTER INSERT ON events.events_log
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
/* Send out pages for entries in the events_log table.
/* To add, delete or modify recipients, look at the table sitescope.pagers. */

   c utl_smtp.connection;
   v_status    varchar2(1000);
   v_message   varchar2(1000);

   CURSOR pagers_q IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = 'TEST';
   CURSOR pagers_c IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = 'TEST';
   pagers_r pagers_c%ROWTYPE;

BEGIN

   IF :new.event_name = 'LOAD-FLORIST-DATA' and :new.status = 'FAILURE' THEN

      ops$oracle.mail_pkg.init_email(c,'DEV_MGR_ALERTS','Load Florist Data Failure',v_status, v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, 'For LOAD-FLORIST-DATA, load from the file failed.');

      ops$oracle.mail_pkg.close_email(c,v_status,v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

   ELSIF :new.event_name = 'LOAD-FLORIST-DATA-TO-PRODUCTION' and :new.status = 'FAILURE' THEN

      ops$oracle.mail_pkg.init_email(c,'DEV_MGR_ALERTS','Failure of Load Florist Data to Prod',v_status, v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, 'For LOAD-FLORIST-DATA-TO-PRODUCTION, move from staging to production failed.');

      ops$oracle.mail_pkg.close_email(c,v_status,v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

   ELSIF :new.event_name = 'LOAD-FLORIST-WEIGHT-CALCULATION-DATA' and :new.status = 'FAILURE' THEN

      ops$oracle.mail_pkg.init_email(c,'DEV_MGR_ALERTS','Failure of Load Florist Weight Calc Data File Load',v_status, v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, 'For LOAD-FLORIST-WEIGHT-CALCULATION-DATA, file load failed.');

      ops$oracle.mail_pkg.close_email(c,v_status,v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

   ELSIF :new.event_name = 'LOAD-FLORIST-WEIGHT-CALCULATION-DATA-TO-PRODUCTION' and :new.status = 'FAILURE' THEN

      ops$oracle.mail_pkg.init_email(c,'DEV_MGR_ALERTS','Failure of Load Florist Weight Calc Data Production Load',v_status, v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, 'For LOAD-FLORIST-WEIGHT-CALCULATION-DATA-TO-PRODUCTION, move from staging to production failed.');

      ops$oracle.mail_pkg.close_email(c,v_status,v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

   ELSIF :new.event_name = 'LOAD-FLORIST-LINKING-DATA' and :new.status = 'FAILURE' THEN

      ops$oracle.mail_pkg.init_email(c,'DEV_MGR_ALERTS','Failure of Load Linking File',v_status, v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, 'For LOAD-FLORIST-LINKING-DATA, move from staging to production failed.');

      ops$oracle.mail_pkg.close_email(c,v_status,v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

   END IF;

EXCEPTION

   WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

      BEGIN

         utl_smtp.quit(c);

      EXCEPTION

         WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

            NULL; -- When the SMTP server is down or unavailable, we don't have
            -- a connection to the server. The quit call will raise an
            -- exception that we can ignore.

      END;

      raise_application_error(-20000, 'Failed to send mail due to the following error: ' || sqlerrm);

END;
/
