create or replace procedure events.trigger_florist_reports_email
as
begin
	rpt.report_pkg.send_florist_reports_email(sysdate);	
end;
/