CREATE OR REPLACE
PROCEDURE events.ins_florist_forward_update IS
/* Insert an event to initiate florist forward update processing.
   The job context is ins_florist_forward_update. */
BEGIN
   enqueue (
      in_context_name    => 'VENUS',
      in_event_name      => 'FLORIST_FORWARD_UPDATE',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_florist_forward_update;
.
/
