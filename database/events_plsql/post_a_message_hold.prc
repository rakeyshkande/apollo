create or replace procedure EVENTS.POST_A_MESSAGE_HOLD (
  IN_JMS_QUEUE_ID   IN  VARCHAR2, 
  IN_CORRELATION_ID IN  VARCHAR2,
  IN_PAYLOAD_TXT    IN  VARCHAR2, 
  IN_DELAY_SECONDS  IN  NUMBER,
  OUT_STATUS        OUT VARCHAR2, 
  OUT_MESSAGE       OUT VARCHAR2
)
AS 
/*-----------------------------------------------------------------------------
Description:
        Writes a JMS message with the passed in parameters to a hold table
        for processing a JMS message.

Input:
        queue_name        varchar2
        correlation_id    varchar2
        payload           varchar2                
        delay_seconds     number

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
v_dequeue_datetime   Date;
v_seq                Number;

BEGIN

    IF IN_DELAY_SECONDS = 0 THEN
        v_dequeue_datetime := null;
    ELSE
        select sysdate + IN_DELAY_SECONDS / 86400
        into v_dequeue_datetime
        from dual;
    END IF;

    select jms_message_hold_sq.nextval into v_seq from dual;
 
    INSERT INTO JMS_MESSAGE_HOLD (
      JMS_MESSAGE_HOLD_ID,
      JMS_QUEUE_ID,
      PAYLOAD_TXT,
      CORRELATION_ID,
      DEQUEUE_DATETIME,
      UPDATED_BY,
      UPDATED_ON,
      CREATED_BY,
      CREATED_ON
    ) VALUES (
      v_seq,
      IN_JMS_QUEUE_ID,
      IN_PAYLOAD_TXT,
      IN_CORRELATION_ID,
      v_DEQUEUE_DATETIME,
      'sys',
      sysdate,
      'sys',
      sysdate
    );

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END;
.
/
