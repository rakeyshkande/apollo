CREATE OR REPLACE
PROCEDURE events.ins_gc_feed_intuit_activate IS
/* Insert an event for Accounting GC inbound Intuit activate feed.
   The job context is ins_gc_feed_intuit_activate. */

BEGIN

   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'GC-FEED-INTUIT-INBOUND-ACTIVATION',
      in_payload         => '<![CDATA[<root><event>GC-FEED-INTUIT-INBOUND-ACTIVATION</event><partner-program>INTUIT</partner-program><status>Issued Active</status></root>]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

   commit;

END ins_gc_feed_intuit_activate;
.
/
