CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_productfeed IS
/* Insert an event to Product Data Feed partner reward program.
   The job context is ins_partner_reward_productfeed. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-PRODUCT-DATA-FEED',
      in_payload         => '<![CDATA[PRODUCTFEED]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_productfeed;
/
