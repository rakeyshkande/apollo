CREATE OR REPLACE
PROCEDURE events.ins_response_koreanair    IS
/* Insert an event to Korean Airlines partner response file.
   The job context is INS_RESPONSE_KOREANAIR. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-KOREAN-AIR',
      in_payload         => '<![CDATA[KOREAN AIR|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_koreanair;
/
