CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_goldpoints IS
/* Insert an event to Gold Points partner reward program.
   The job context is ins_partner_reward_goldpoints. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-GOLD-POINTS',
      in_payload         => '<![CDATA[GOLD POINTS]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_goldpoints;
.
/
