CREATE OR REPLACE
PROCEDURE events.ins_gc_feed_intuit_deactivate IS
/* Insert an event for Accounting GC inbound Intuit deactivate feed.
   The job context is ins_gc_feed_intuit_deactivate. */

BEGIN

   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'GC-FEED-INTUIT-INBOUND-DEACTIVATION',
      in_payload         => '<![CDATA[<root><event>GC-FEED-INTUIT-INBOUND-DEACTIVATION</event><partner-program>INTUIT</partner-program><status>Cancelled</status></root>]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

   commit;

END ins_gc_feed_intuit_deactivate;
.
/
