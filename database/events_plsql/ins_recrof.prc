CREATE OR REPLACE
PROCEDURE events.ins_recrof IS
/* Insert an event to initiate Mercury manual reconciliation file download.
   The job context is ins_recrof. */
BEGIN
   enqueue (
      in_context_name    => 'RECROF',
      in_event_name      => 'RECROF',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_recrof;
.
/
