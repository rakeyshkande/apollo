CREATE OR REPLACE
PROCEDURE events.ins_response_aircan_billing IS
/* Insert an event to pick up Air Canada billing response file.
   The job context is INS_RESPONSE_AIRCAN_BILLING. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-AIR-CANADA-BILLING',
      in_payload         => '<![CDATA[AIR CANADA|nodeResponseExtractBilling]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_aircan_billing;
.
/
