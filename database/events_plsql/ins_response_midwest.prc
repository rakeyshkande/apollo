CREATE OR REPLACE
PROCEDURE events.ins_response_midwest IS
/* Insert an event to pick up Midwest Airlines response file.
   The job context is INS_RESPONSE_MIDWEST.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-MIDWEST-AIRLINES',
      in_payload         => '<![CDATA[MIDWEST AIRLINES|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_midwest;
.
/
