CREATE OR REPLACE
PROCEDURE events.ins_response_bass_error IS
/* Insert an event to pick up Bass Hotels error response file.
   The job context is INS_RESPONSE_BASS_ERROR.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-BASS-HOTELS-ERROR',
      in_payload         => '<![CDATA[BASS HOTELS|nodeResponseExtractError]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_bass_error;
.
/
