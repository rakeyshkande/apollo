CREATE OR REPLACE
PROCEDURE events.ins_scheduled_reports IS
/* Insert an event to run scheduled reports.
   The job context is INS_SCHEDULED_REPORTS. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'SCHEDULED_REPORT_PROCESS',
      in_payload         => to_char(sysdate-1,'mm/dd/yyyy'),
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_scheduled_reports;
.
/
