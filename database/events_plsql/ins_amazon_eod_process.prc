CREATE OR REPLACE
PROCEDURE events.ins_amazon_eod_process IS
/* Insert an event to initiate Amazon end-of-day processing.
   The job context is ins_amazon_eod_process. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'AMAZON-EOD-PROCESS',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_amazon_eod_process;
.
/
