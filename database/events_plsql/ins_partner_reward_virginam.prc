CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_virginam IS
/* Insert an event to VIRGIN AMERICA Airlines partner reward program.
   The job context is INS_PARTNER_REWARD_VIRGINAMERICA. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'POST-FILE-VIRGIN-AMERICA',
      in_payload         => '<![CDATA[VIRGIN AMERICA]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_virginam;
.
/
