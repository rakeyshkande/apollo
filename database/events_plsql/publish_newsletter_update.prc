CREATE OR REPLACE
PROCEDURE events.PUBLISH_NEWSLETTER_UPDATE
(
 IN_ORIGIN_APP_NAME  IN VARCHAR2,
 IN_EMAIL_ADDRESS    IN VARCHAR2,
 IN_COMPANY_ID       IN VARCHAR2,
 IN_STATUS           IN VARCHAR2,
 IN_OPERATOR_ID      IN VARCHAR2,
 IN_DATE             IN DATE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure procedure invoked remotely from database DW1
        on server.

Input:
        in_origin_app_name  VARCHAR2
        in_email_address    VARCHAR2
	in_company_id       VARCHAR2
	in_status           VARCHAR2
	in_operator_id      VARCHAR2
        in_date             DATE

Output:
        status
        error message
-----------------------------------------------------------------------------*/

 v_text               VARCHAR2(32767);
 v_originTimestamp    VARCHAR2(20);
 v_agent              sys.aq$_agent   := sys.aq$_agent(' ', null, 0);
 v_message            sys.aq$_jms_text_message;
 v_enqueue_options    dbms_aq.enqueue_options_t;
 v_message_properties dbms_aq.message_properties_t;
 v_msgid              raw(16);

BEGIN

  v_message := sys.aq$_jms_text_message.construct;
  v_message.set_string_property('ORIGIN_APP_NAME_KEY', in_origin_app_name);

  -- The following calculation determines how many milliseconds have passed
  --  since between in_date and Jan 1, 1970, by first calculating how many
  --  days have passed and then multiplying it by how many milliseconds
  --  there are in a day.
  v_originTimestamp := TO_CHAR((TRUNC(in_date) - TO_DATE('01/01/1970','MM/DD/YYYY')) * 86400000);

  v_text := CONCAT(v_text, '<root><com.ftd.newsletterevents.core.NewsletterEventVO Status="');
  v_text := CONCAT(v_text, in_status);
  v_text := CONCAT(v_text, '" OriginAppName="');
  v_text := CONCAT(v_text, in_origin_app_name);
  v_text := CONCAT(v_text, '" EmailAddress="');
  v_text := CONCAT(v_text, in_email_address);
  v_text := CONCAT(v_text, '" CompanyId="');
  v_text := CONCAT(v_text, in_company_id);
  v_text := CONCAT(v_text, '" OperatorId="');
  v_text := CONCAT(v_text, in_operator_id);
  v_text := CONCAT(v_text, '" OriginTimestamp="');
  v_text := CONCAT(v_text, v_originTimestamp);
  v_text := CONCAT(v_text, '"/></root>');


  v_message.set_text(v_text);

  v_message_properties.priority := 5;

  dbms_aq.enqueue(queue_name => 'ojms.nl_subscribe_control',
                  enqueue_options => v_enqueue_options,
                  message_properties => v_message_properties,
                  payload => v_message,
                  msgid => v_msgid);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END PUBLISH_NEWSLETTER_UPDATE;
.
/
