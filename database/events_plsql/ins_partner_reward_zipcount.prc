CREATE OR REPLACE
PROCEDURE events.ins_partner_reward_zipcount IS
/* Insert an event to run the florist zip count report.
   The job context is ins_partner_reward_zipcount. */

BEGIN

   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'LOAD-FLORIST-ZIP-DATA',
      in_payload         => '<![CDATA[FLORIST ZIP CODE COUNT]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_partner_reward_zipcount;
.
/
