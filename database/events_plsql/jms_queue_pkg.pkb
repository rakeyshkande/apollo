CREATE OR REPLACE 
PACKAGE BODY EVENTS.JMS_QUEUE_PKG AS

PROCEDURE GET_JMS_QUEUE_LIST 
(
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves the list of all jms queues

Input:

Output:
        out_cur      Cursor containin the queue names

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  select jms_queue_id
  from events.jms_queue;
     
END GET_JMS_QUEUE_LIST;

PROCEDURE INSERT_JMS_MESSAGE_HOLD
(
    IN_JMS_QUEUE_ID             IN JMS_MESSAGE_HOLD.JMS_QUEUE_ID%TYPE,
    IN_PAYLOAD_TXT              IN JMS_MESSAGE_HOLD.PAYLOAD_TXT%TYPE,
    IN_CORRELATION_ID           IN JMS_MESSAGE_HOLD.CORRELATION_ID%TYPE,
    IN_DEQUEUE_DATETIME         IN JMS_MESSAGE_HOLD.DEQUEUE_DATETIME%TYPE,
    IN_UPDATED_BY               IN JMS_MESSAGE_HOLD.UPDATED_BY%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the JMS_MESSAGE_HOLD table

Input:
    IN_JMS_QUEUE_ID             JMS Queue Name
    IN_PAYLOAD_TXT              JMS Payload
    IN_CORRELATION_ID           JMS Correlation ID
    IN_DEQUEUE_DATETIME         Time to Dequeue, can be null
    IN_UPDATED_BY               Updated by

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/
v_jms_message_hold_id  number;

BEGIN
    SELECT jms_message_hold_sq.nextval into v_jms_message_hold_id from dual;

    INSERT INTO JMS_MESSAGE_HOLD (
      JMS_MESSAGE_HOLD_ID,
      JMS_QUEUE_ID,
      PAYLOAD_TXT,
      CORRELATION_ID,
      DEQUEUE_DATETIME,
      UPDATED_BY,
      UPDATED_ON,
      CREATED_BY,
      CREATED_ON
    ) VALUES (
      v_jms_message_hold_id,
      IN_JMS_QUEUE_ID,
      IN_PAYLOAD_TXT,
      IN_CORRELATION_ID,
      IN_DEQUEUE_DATETIME,
      IN_UPDATED_BY,
      sysdate,
      IN_UPDATED_BY,
      sysdate
    );

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_JMS_MESSAGE_HOLD;


PROCEDURE GET_JMS_MESSAGE_HOLD_LIST 
(
    IN_MAX_NUMBER          IN NUMBER,

    OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a list of messages in the JMS_MESSAGE_HOLD table.

Input:
    IN_MAX_NUMBER          max number of results to return

Output:
        out_cur      Cursor containin the message items

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  select jms_message_hold_id,
         jms_queue_id,
         payload_txt,
         correlation_id,
         dequeue_datetime,
         updated_by,
         updated_on,
         created_by,
         created_on
  from events.jms_message_hold
  where 1=1
    and (dequeue_datetime is null or dequeue_datetime > sysdate);
     
END GET_JMS_MESSAGE_HOLD_LIST;

PROCEDURE DELETE_JMS_MESSAGE_HOLD
(
    IN_JMS_MESSAGE_HOLD_ID      IN JMS_MESSAGE_HOLD.JMS_QUEUE_ID%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Deletes a row from the JMS_MESSAGE_HOLD table

Input:
    IN_JMS_MESSAGE_HOLD_ID      JMS Hold Id 

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

BEGIN

    DELETE FROM JMS_MESSAGE_HOLD
    WHERE JMS_MESSAGE_HOLD_ID = IN_JMS_MESSAGE_HOLD_ID;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DELETE_JMS_MESSAGE_HOLD;

PROCEDURE                      PHOENIX_EMAIL_RETRY 
(
  START_DATE      IN varchar2  
, END_DATE        IN varchar2
, OUT_STATUS      OUT varchar2
, OUT_MESSAGE     OUT varchar2
) AS 
v_payload        varchar2(2000);
v_msgid          varchar2(200);
v_stat           varchar2(10);
v_mess           varchar2(2000);

  

BEGIN
  if start_date is null or end_date is null 
  then
    FOR rec in (select m.msgid, m.user_data.text_vc payload from OJMS.message_generator m where 
    m.state = 3 
    and m.q_name='AQ$_MESSAGE_GENERATOR_E' 
    and trunc(m.enq_time) between trunc(sysdate-1) and trunc(sysdate))
    LOOP
      v_payload := rec.payload;
      v_msgid := rec.msgid;
      
      
      DELETE FROM OJMS.message_generator WHERE msgid = v_msgid;
      post_a_message_flex('OJMS.message_generator', NULL, v_payload, 5, v_stat, v_mess);
      commit;
    END LOOP;
  else
    FOR rec in (select m.msgid, m.user_data.text_vc payload from OJMS.message_generator m where 
    m.state = 3 
    and m.q_name='AQ$_MESSAGE_GENERATOR_E' 
    and trunc(m.enq_time) between to_date(start_date, 'MM/DD/YYYY') and to_date(end_date, 'MM/DD/YYYY'))
    LOOP
      v_payload := rec.payload;
      v_msgid := rec.msgid;
      
      
      DELETE FROM OJMS.message_generator WHERE msgid = v_msgid;
      post_a_message_flex('OJMS.message_generator', NULL, v_payload, 5, v_stat, v_mess);
      commit;
    END LOOP;
  end if;
    
   
   out_status := 'Y';
   out_message := 'Success';

   EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256); 

END PHOENIX_EMAIL_RETRY;

END JMS_QUEUE_PKG;
/
