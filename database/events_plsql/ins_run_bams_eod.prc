CREATE OR REPLACE
PROCEDURE events.ins_run_bams_eod IS
/* Insert an event to start the end of day process.
   The job context is ins_run_eod. */
BEGIN
   enqueue (
      in_context_name    => 'ACCOUNTING',
      in_event_name      => 'RUN-EOD',
      in_payload         => 'BAMS',
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_run_bams_eod;
.
/
