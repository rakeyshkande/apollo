CREATE OR REPLACE
PROCEDURE events.ins_response_virginam IS
/* Insert an event to pick up VIRGIN AMERICA Airlines response file(s).
   The job context is INS_RESPONSE_VIRGINAMERICA.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-VIRGIN-AMERICA',
      in_payload         => '<![CDATA[VIRGIN AMERICA|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_virginam;
.
/
