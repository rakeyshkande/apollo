CREATE OR REPLACE
PROCEDURE events.ins_response_northwest IS
/* Insert an event to Northwest Airlines partner response file.
   The job context is INS_RESPONSE_NORTHWEST. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-NORTHWEST-AIRLINES',
      in_payload         => '<![CDATA[NORTHWEST AIRLINES|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_northwest;
.
/
