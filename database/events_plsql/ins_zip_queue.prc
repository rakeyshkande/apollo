CREATE OR REPLACE
PROCEDURE events.ins_zip_queue IS
/* Insert an event to initiate Zip Queue Automatic Handler.
   The job context is ins_zip_queue. */
BEGIN
   enqueue (
      in_context_name    => 'ORDER_PROCESSING',
      in_event_name      => 'ZIP_QUEUE_AUTO',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_zip_queue;
.
/
