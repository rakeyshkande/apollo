create or replace procedure EVENTS.POST_A_MESSAGE_PRIORITY (
  IN_QUEUE_NAME     IN  VARCHAR2, 
  IN_CORRELATION_ID IN  VARCHAR2,
  IN_PAYLOAD        IN  VARCHAR2, 
  IN_DELAY_SECONDS  IN  NUMBER,
  IN_PRIORITY       IN  INTEGER,
  OUT_STATUS        OUT VARCHAR2, 
  OUT_MESSAGE       OUT VARCHAR2
)
AS 
/*-----------------------------------------------------------------------------
Description:
        Enqueues a JMS message with the passed in parameters.
        THIS VERSION PROVIDES MORE FLEXIBILITY THAN "POST_A_MESSAGE"

Input:
        queue_name        varchar2
        correlation_id    varchar2
        payload           varchar2                
        delay_seconds     number

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
enqueue_options      dbms_aq.enqueue_options_t;
message_properties   dbms_aq.message_properties_t;
message_handle       raw(2000);
message              sys.aq$_jms_text_message;

BEGIN
  --Create a new JMS Message
  message := sys.aq$_jms_text_message.construct();
  message.set_text(payload => IN_PAYLOAD );
  message_properties.CORRELATION := IN_CORRELATION_ID;
  message_properties.PRIORITY := IN_PRIORITY;
  
  IF( IN_DELAY_SECONDS IS NULL OR IN_DELAY_SECONDS < 1 ) THEN
      message_properties.delay := DBMS_AQ.NO_DELAY;
  ELSE
      message_properties.delay := IN_DELAY_SECONDS;
  END IF;
  
  message_properties.exception_queue := 'OJMS.OSP_APP_EXCEPTION';
  
  dbms_aq.enqueue (queue_name => IN_QUEUE_NAME,
                enqueue_options => enqueue_options,
                message_properties => message_properties,
                payload => message,
                msgid => message_handle);
  --commit;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END;
.
/
