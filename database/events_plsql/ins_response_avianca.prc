CREATE OR REPLACE
PROCEDURE events.ins_response_avianca    IS
/* Insert an event to AviancaTaca partner response file.
   The job context is INS_RESPONSE_AVIANCA. */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-AVIANCA',
      in_payload         => '<![CDATA[AVIANCA T|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_avianca;
.
/