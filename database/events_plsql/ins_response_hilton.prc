CREATE OR REPLACE
PROCEDURE events.ins_response_hilton IS
/* Insert an event to pick up Hilton Hotels response file.
   The job context is INS_RESPONSE_HILTON.   */
BEGIN
   enqueue (
      in_context_name    => 'PARTNER-REWARD',
      in_event_name      => 'RESPONSE-FILE-HILTON-HOTELS',
      in_payload         => '<![CDATA[HILTON HOTELS|nodeResponseExtract]]>',
      in_message_expires => 'Y',
      in_timeout_seconds => (TRUNC(SYSDATE+1)-SYSDATE)*24*60*60);

END ins_response_hilton;
.
/
