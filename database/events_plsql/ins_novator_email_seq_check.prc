CREATE OR REPLACE
PROCEDURE events.ins_novator_email_seq_check IS
/* Insert an event to start the Novator email sequence check process.
   The job context is ins_novator_email_seq_check. */
BEGIN
   enqueue (
      in_context_name    => 'NOVATOR_EMAIL_REQUEST',
      in_event_name      => 'RUN-NOVATOR-SEQUENCE-CHECK',
      in_payload         => NULL,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_novator_email_seq_check;
.
/
