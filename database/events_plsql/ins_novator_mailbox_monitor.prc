CREATE OR REPLACE
PROCEDURE events.ins_novator_mailbox_monitor (IN_MAILBOX_NAME IN VARCHAR2) IS
/* Insert an event to start the Novator mailbox monitor.
   The job context is ins_novator_mailbox_monitor. */
   v_payload VARCHAR2(3000);
BEGIN

   v_payload := '<![CDATA[ <mailsettings><mailbox_name>' || IN_MAILBOX_NAME || '</mailbox_name></mailsettings> ]]>';

   enqueue (
      in_context_name    => 'NOVATOR_EMAIL_REQUEST',
      in_event_name      => 'START_MAILBOX_MONITOR',
      in_payload         => v_payload,
      in_message_expires => 'N',
      in_timeout_seconds => 0);

END ins_novator_mailbox_monitor;
.
/
