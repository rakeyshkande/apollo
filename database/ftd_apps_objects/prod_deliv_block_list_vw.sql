CREATE OR REPLACE VIEW FTD_APPS.PROD_DELIV_BLOCK_LIST_VW
(PRODUCT_ID, START_DATE, END_DATE, COUNT)
AS 
select pdr.product_id, pdr.start_date, pdr.end_date, count(*) ship_block_rank
from   product_delivery_restrict_vw pdr, product_delivery_restrict_vw pdr2
where  pdr2.product_id = pdr.product_id
and    pdr2.start_date <= pdr.start_date
and    pdr2.end_date >= trunc(sysdate)
group by pdr.product_id, pdr.start_date, pdr.end_date;