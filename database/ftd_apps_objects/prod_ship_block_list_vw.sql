
CREATE OR REPLACE VIEW FTD_APPS.PROD_SHIP_BLOCK_LIST_VW
(PRODUCT_ID, START_DATE, END_DATE, COUNT)
AS 
select psr.product_id, psr.start_date, psr.end_date, count(*) ship_block_rank
from   product_shipping_restrict_vw psr, product_shipping_restrict_vw psr2
where  psr2.product_id = psr.product_id
and    psr2.start_date <= psr.start_date
and    psr2.end_date >= trunc(sysdate)
group by psr.product_id, psr.start_date, psr.end_date;