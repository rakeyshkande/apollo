CREATE OR REPLACE VIEW FTD_APPS.PROD_DELIV_BLOCK_CNT_VW
(PRODUCT_ID, COUNT)
AS 
select pdr.product_id, count(distinct pdr.start_date) ship_block_count
from   product_delivery_restrict_vw pdr
where  pdr.end_date >= trunc(sysdate)
group by pdr.product_id;