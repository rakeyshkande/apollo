CREATE OR REPLACE VIEW FTD_APPS.PROD_SHIP_BLOCK_CNT_VW
(PRODUCT_ID, COUNT)
AS
select psr.product_id, count(distinct psr.start_date) ship_block_count
from   product_shipping_restrict_vw psr
where  psr.end_date >= trunc(sysdate)
group by psr.product_id;