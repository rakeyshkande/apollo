CREATE OR REPLACE VIEW FTD_APPS.PRODUCT_SHIPPING_RESTRICT_VW
(PRODUCT_ID, START_DATE, END_DATE, BLOCKED_VENDOR_COUNT)
AS
SELECT * FROM
   (SELECT pm1.product_id product_id,
           vsr1.start_date start_date,
           vsr1.end_date end_date,
           count(distinct vp1.vendor_id) as blocked_vendor_count
    FROM FTD_APPS.VENDOR_PRODUCT vp1,
         FTD_APPS.vendor_shipping_restrictions vsr1,
         FTD_APPS.PRODUCT_MASTER pm1
    WHERE vsr1.START_DATE >= trunc(sysdate)
      AND vp1.VENDOR_ID = vsr1.VENDOR_ID
      AND vp1.AVAILABLE='Y'
      AND vp1.PRODUCT_SUBCODE_ID = pm1.PRODUCT_ID
    --Filter out blocks where not all vendors are blocked
    GROUP BY pm1.product_id,
             vsr1.start_date, vsr1.end_date
    UNION
    SELECT pm1.product_id product_id,
           vsr1.start_date start_date,
           vsr1.end_date end_date,
           count(distinct vp1.vendor_id) as blocked_vendor_count
    FROM FTD_APPS.VENDOR_PRODUCT vp1,
         FTD_APPS.vendor_shipping_restrictions vsr1,
         FTD_APPS.PRODUCT_MASTER pm1,
         FTD_APPS.PRODUCT_SUBCODES ps
    WHERE vsr1.START_DATE >= trunc(sysdate)
      AND vp1.VENDOR_ID = vsr1.VENDOR_ID
      AND vp1.AVAILABLE='Y'
      AND vp1.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
      AND pm1.PRODUCT_ID = ps.PRODUCT_ID
    --Filter out blocks where not all vendors are blocked
    GROUP BY pm1.product_id,
             vsr1.start_date, vsr1.end_date)  tab1
WHERE TAB1.BLOCKED_VENDOR_COUNT =
   (SELECT count(distinct vp.vendor_id)
     FROM FTD_APPS.VENDOR_PRODUCT vp,
          FTD_APPS.PRODUCT_SUBCODES ps
     WHERE (vp.PRODUCT_SUBCODE_ID = TAB1.PRODUCT_ID
        OR (vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
           and TAB1.PRODUCT_ID  = ps.PRODUCT_ID))
       AND vp.AVAILABLE='Y')
;
