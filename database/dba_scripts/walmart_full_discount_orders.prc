create or replace PROCEDURE  ops$oracle.walmart_full_discount_orders  is

----------------------------------------------------------------------------------
-- Proc    : walmart_full_discount_orders
--
-- Descr   : Started as defect 3045 Fix, and continues to run.
--           Send ship notification to Walmart for fully discounted orders.
--
----------------------------------------------------------------------------------

   ins_cnt  number := 0;

   cursor c is
      select o.master_order_number,
             od.order_detail_id,
             od.external_order_number,
             od.delivery_date,
             od.order_disp_code,
             ob.order_bill_id,
             ob.bill_status,
             ob.bill_date
      from clean.orders o
      join clean.order_details od
      on o.order_guid=od.order_guid
      join clean.order_bills ob
      on ob.order_detail_id=od.order_detail_id
      join (
            select  cob.order_detail_id
            from    clean.order_bills cob,
                    clean.order_details cod,
                    clean.orders co
            where   cob.product_amount + cob.add_on_amount +
                    cob.service_fee + cob.shipping_fee -
                    cob.discount_amount + cob.shipping_tax + cob.tax = 0
              and co.origin_id='WLMTI'
              and co.order_guid=cod.order_guid
              and cod.order_detail_id=cob.order_detail_id
            ) a
      on a.order_detail_id=od.order_detail_id
      WHERE o.origin_id = 'WLMTI'
      AND od.order_disp_code = 'Processed'
      AND TRUNC(od.delivery_date) <= TRUNC(sysdate)
      AND ob.bill_status = 'Unbilled'
      AND ob.bill_date is null
      AND (od.ship_method = 'SD'
        OR od.ship_method IS NULL)
      and not exists (select 1 from pop.items_shipped pi
                      where pi.master_order_number=o.master_order_number) ;


   conn               UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   crlf               VARCHAR2(2) := chr(13) || chr(10);

   pager_cursor       types.ref_cursor;
   type pager_type is table of sitescope.pagers.pager_number%type;
   pager_table        pager_type;

   first_loop         varchar2(1) := 'Y';
   errors_found       varchar2(1) := 'N';


begin
   conn := demo_mail.begin_mail(recipient_project => 'B.A. NOPAGE ALERTS',
                                subject           => 'NOPAGE Fully Discounted Walmart Orders Monitor for ' || to_char(sysdate,'MM/DD/YYYY HH24:MI'),
                                mime_type         => demo_mail.MULTIPART_MIME_TYPE);

   --
   -- Send the report headers
   --
   demo_mail.attach_mb_text(conn => conn,
                            data => '<center><h2><font color=blue>Fully Discounted Walmart Orders</font></h2></center>',
                            mime_type => 'text/html; charset=iso-8859-1' );


   demo_mail.write_text(conn => conn,
                        message => '<u>For all orders found</u> ' 
                                    || '<br>1)  Insert <I>NOT SENT</I> Status to POP.Items_Shipped.'
                                    || '<br>2)  Set Clean.Order_Details.Order_Disp_Code to <I>Shipped</I> ' 
                                    || '<br>3)  Set Clean.Order_Bills.Bill_Status to <I>Billed</I><br>');
 

   --
   -- Loop through all orders found, Send to report and process data
   --
   for x in c loop
   
      if first_loop = 'Y' then
         
         -- Build Table and Header
         demo_mail.write_text(conn => conn,
                        message => '<br><TABLE BORDER BGCOLOR=#E0E0E0>' ||
                                   '<CAPTION ALIGN=CENTER><FONT COLOR=green SIZE=5>Orders Found</FONT></CAPTION>' ||
                                   '<TR ALIGN=CENTER>'                 ||
                                      '<TD WIDTH=130><I>Master Order</I></TD>'          ||
                                      '<TD WIDTH=130><I>Order Detail ID</I></TD>'       ||
                                      '<TD WIDTH=160><I>External Order Number</I></TD>' ||
                                    '</TR>');

         first_loop := 'N';
      end if;

      demo_mail.write_text(conn => conn,
                        message => '<TR ALIGN=CENTER>'    ||
                                      '<TD>' || x.master_order_number   || '</TD>' ||
                                      '<TD>' || x.order_detail_id       || '</TD>' ||
                                      '<TD>' || x.external_order_number || '</TD>' ||
                                    '</TR>');

      begin
         INSERT INTO pop.items_shipped
         (partner_id,
          ship_date,
          master_order_number,
          external_order_number,
          shipped,
          status,
          updated_on,
          created_on,
          error_description)
         VALUES
         ('WLMTI',
          x.delivery_date,
          x.master_order_number,
          x.external_order_number,
          'Y',
          'NOT SENT',
          sysdate,
          sysdate,
          null);

         update clean.order_details
         set order_disp_code='Shipped',
            updated_on=sysdate,
            updated_by='defect_3045_data_fix_daily'
         where order_detail_id=x.order_detail_id;

         update clean.order_bills
         set bill_status='Billed',
             bill_date=sysdate,
             updated_on=sysdate,
         updated_by='defect_3045_data_fix_daily'
         where order_detail_id=x.order_detail_id;

         ins_cnt := ins_cnt + 1;

      exception
         when others then
            demo_mail.write_text(conn => conn,
                        message => '</table><br><font color=red size=4>UNEXEPECTED ERROR DURING ' ||
                                   'DATA UPDATES - ROLLBACK!!</font>' ||
                        '<br><font size=2 color=red>Sqlcode=' || sqlcode || '<br>Msg=' || sqlerrm || '</font>');
             rollback;
             errors_found := 'Y';
             exit;
      end;
   end loop;


   if errors_found = 'N' THEN
      commit;

      if first_loop = 'N' then    -- Close the table if orders were found.
         demo_mail.write_text(conn => conn, message => '</TABLE>');
      end if;

      if ins_cnt = 0 then
         demo_mail.write_text(conn => conn,
                        message => crlf || '<br><font color=green><I>No qualifying orders found.</I></font>');
      else
         demo_mail.write_text(conn => conn,
                        message => crlf || '<br><font color=green>Number of Walmart orders processed = ' || ins_cnt || '</font>');
      end if;
   else
      -- Table end tag already sent
      demo_mail.write_text(conn => conn,
                        message => crlf || '<br><br><font color=green><I>Orders processed were rolled back!!</I></font>');
   end if;


   demo_mail.end_mail(conn => conn);      /* Must call this for anything to work */

end walmart_full_discount_orders;
/

