rem This script identifies orders that are in scrub, but have already been processed.  It changes their status to take
rem them out of scrub.
UPDATE scrub.orders
SET    status = '1008'
WHERE  order_guid IN 
      (SELECT order_guid
       FROM   scrub.order_details
       WHERE  status <> '2004'
       AND    order_detail_id IN
             (SELECT a.order_detail_id
              FROM  (SELECT order_detail_id
                     FROM   scrub.order_details
                     WHERE  status IN ('2001','2002','2003','2005','2007','2008','2009','2011')) a, 
                    (SELECT order_detail_id 
                     FROM   clean.order_details
                     WHERE  op_status = 'Processed'
                     AND    created_on > TRUNC(SYSDATE - 20)) b
              WHERE  a.order_detail_id = b.order_detail_id));

UPDATE scrub.order_details
SET    status = '2006'
WHERE  status <> '2004'
AND    order_detail_id IN
      (SELECT a.order_detail_id
       FROM  (SELECT order_detail_id
              FROM   scrub.order_details
              WHERE  status IN ('2001', '2002','2003','2005','2007','2008','2009','2011')) a, 
             (SELECT order_detail_id
              FROM   clean.order_details
              WHERE  op_status = 'Processed'
              AND    created_on > TRUNC(SYSDATE - 20)) b
       WHERE  a.order_detail_id = b.order_detail_id);
commit;
