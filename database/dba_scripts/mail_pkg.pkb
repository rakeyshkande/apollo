CREATE OR REPLACE PACKAGE BODY OPS$ORACLE.MAIL_PKG AS

PROCEDURE INIT_EMAIL (in_conn     IN OUT utl_smtp.connection,
                      in_project  IN     varchar2,
                      in_subject  IN     varchar2,
                      out_status  OUT    varchar2,
                      out_message OUT    varchar2) AS
/*-----------------------------------------------------------------------------
Description:
   Config Mail - Open connection, Set up Recipients and Subject
                 After this just write the messages and close/quit
                 
Input/Output  IN_CONN      = utl_smtp conection pointer
   
Input         IN_PROJECT   = from sitescope.pagers
              IN_SUBJECT   = Subject of Email
                 
Output        OUT_STATUS   = Y (success) or N (failed)
              OUT_MESSAGE  = error message returned
-----------------------------------------------------------------------------*/

   v_func       varchar2(1000) := NULL;   -- store location to pass back if error
   v_ms_name    sitescope.projects.mailserver_name%type;
   v_domain     sitescope.projects.mailserver_name%type;   

   CURSOR pagers_c (p_project IN VARCHAR2) IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = p_project;
   pagers_r pagers_c%ROWTYPE;

   PROCEDURE send_header(name IN VARCHAR2, header IN VARCHAR2) AS
   BEGIN
      utl_smtp.write_data(in_conn, name || ': ' || header || utl_tcp.CRLF);
   END;

BEGIN

   v_func := 'Getting mailserver_name from sitescope.projects';
   select mailserver_name into v_ms_name
   from sitescope.projects
   where project = in_project;


   v_func := 'Calling utl_smtp.connection';
   in_conn := utl_smtp.open_connection(v_ms_name);

   --
   -- If mailserver is "mailserver.ftdi.com", this sends "ftdi.com"
   --
   v_func := 'Calling utl_smtp.helo';
   v_domain := substr(v_ms_name, instr(v_ms_name,'.') + 1);
   utl_smtp.helo(in_conn, v_domain);


   v_func := 'Calling utl_smtp.mail';
   utl_smtp.mail(in_conn, use_this_from_address(v_domain));

   --
   -- Query recipients and add to distribution
   --
   v_func := 'Building recipient list';
   FOR pagers_r IN pagers_c (in_project) LOOP
      utl_smtp.rcpt(in_conn, pagers_r.pager_number);
   END LOOP;

   v_func := 'Calling utl_smtp.open_data';
   utl_smtp.open_data(in_conn);

   --send_header('From', frp.use_this_from_address);  -- Not necessary

   v_func := 'Building TO List';
   FOR pagers_r IN pagers_c (in_project) LOOP
      send_header('To', '"'||pagers_r.pager_number||'" '||pagers_r.pager_number);
   END LOOP;

   v_func := 'Building SUBJECT';
   send_header('Subject', in_subject);

   out_status := 'Y';


EXCEPTION
   WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED IN INIT_EMAIL while ' || v_func || '   [' ||
                        SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END INIT_EMAIL;



PROCEDURE CLOSE_EMAIL (in_conn    IN OUT utl_smtp.connection,
                      out_status  OUT    varchar2,
                      out_message OUT    varchar2) AS
/*-----------------------------------------------------------------------------
Description:
   Config Mail - Open connection, Set up Recipients and Subject
                 After this just write the messages and close/quit
                 
Input/Output  IN_CONN      = utl_smtp conection pointer
   
Input         None

Output        OUT_STATUS   = Y (success) or N (failed)
              OUT_MESSAGE  = error message returned
-----------------------------------------------------------------------------*/

   v_func       varchar2(1000) := NULL;   -- store location to pass back if error


BEGIN

   v_func := 'Calling utl_smtp.close_data';
   utl_smtp.close_data(in_conn);

   v_func := 'Calling utl_smtp.quit';
   utl_smtp.quit(in_conn);


   out_status := 'Y';


EXCEPTION
   WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED IN CLOSE_EMAIL while ' || v_func || '   [' ||
                        SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END CLOSE_EMAIL;



FUNCTION USE_THIS_FROM_ADDRESS (in_domain IN varchar2) 
     RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------
Description:
        Grab the db_name, format and attach to domain the return the value.
        
Input:
        IN_DOMAIN    - from sitescope.projects - should have been looked up prior

Output:
        From Address - value to use in email header

-----------------------------------------------------------------------------*/

   v_db      VARCHAR2(30);

BEGIN

   SELECT SYS_CONTEXT('USERENV','DB_NAME')
   INTO   v_db
   FROM   DUAL;

   RETURN v_db || '_database@' || in_domain;
   
END USE_THIS_FROM_ADDRESS;

END MAIL_PKG;
/




grant execute on ops$oracle.mail_pkg to amazon;
grant execute on ops$oracle.mail_pkg to clean;
grant execute on ops$oracle.mail_pkg to ops$oracle;
grant execute on ops$oracle.mail_pkg to events;
grant execute on ops$oracle.mail_pkg to ftd_apps;
grant execute on ops$oracle.mail_pkg to mercury;
grant execute on ops$oracle.mail_pkg to ojms;
grant execute on ops$oracle.mail_pkg to pop;
grant execute on ops$oracle.mail_pkg to scrub;
grant execute on ops$oracle.mail_pkg to sitescope;
grant execute on ops$oracle.mail_pkg to venus;
grant execute on ops$oracle.mail_pkg to frp;










