CREATE OR REPLACE PROCEDURE OPS$ORACLE.SEND_DASHBOARD_RESULTS is

   v_file_name        VARCHAR2(100);        /* ascii file attachment */
   v_file_handle      UTL_FILE.FILE_TYPE;
   v_directory_name   VARCHAR2(100) := 'DICTIONARY_OUTPUT';
   v_line             VARCHAR2(5000);

   conn               UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   mesg               VARCHAR2(32767);
   crlf               VARCHAR2(2) := chr(13) || chr(10);
   err_num            NUMBER;
   err_msg            VARCHAR2(100);

   pager_cursor       types.ref_cursor;
   type pager_type is table of sitescope.pagers.pager_number%type;
   pager_table        pager_type;

   cursor dashboard_cursor is
SELECT  /*+ RULE */
                'ALL'||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') AND (OD.SHIP_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY') OR OD.DELIVERY_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY')) THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' AND (OD.SHIP_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY') OR OD.DELIVERY_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY')) THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2004', '2008') AND OD.UPDATED_ON BETWEEN TRUNC (SYSDATE) AND TRUNC (SYSDATE + 1) - 1/86400 THEN OD.ORDER_GUID END) 
        FROM    SCRUB.ORDER_DETAILS OD
        WHERE   OD.STATUS IN ('2002', '2003', '2004', '2005', '2008')
        UNION
        SELECT  O.ORDER_ORIGIN||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') AND (OD.SHIP_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY') OR OD.DELIVERY_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY')) THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' AND (OD.SHIP_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY') OR OD.DELIVERY_DATE = TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY')) THEN OD.ORDER_GUID END)||','||
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2004', '2008') AND OD.UPDATED_ON BETWEEN TRUNC (SYSDATE) AND TRUNC (SYSDATE + 1) - 1/86400 THEN OD.ORDER_GUID END) 
        FROM    SCRUB.ORDERS O
        JOIN    SCRUB.ORDER_DETAILS OD
        ON      O.ORDER_GUID = OD.ORDER_GUID
        WHERE   OD.STATUS IN ('2002', '2003', '2004', '2005', '2008')
        AND     O.ORDER_ORIGIN IS NOT NULL
        GROUP BY O.ORDER_ORIGIN;
                  
BEGIN
   v_file_name := 'dashboard_results_'||to_char(sysdate,'yyyymmdd')||'.csv';

   BEGIN
      /* Fill in to-from and subject info here */
      conn := demo_mail.begin_mail(
         recipient_project => 'Dashboard Results',
         subject => 'Hourly Dashboard Results',
         mime_type => demo_mail.MULTIPART_MIME_TYPE);
   END begin_mail;

   BEGIN
      /* The first call to attach_text will be the body of the mess*/
      /* You can format the message with HTML formatting codes */
      demo_mail.attach_mb_text(
            conn => conn,
            data => 'The hourly dashboard results are attached.',
            mime_type => 'text/plain; charset=iso-8859-1' );
   END attach_text;

   BEGIN
      /* You must call begin_attachment and end_attachment */
      /* for any files you wish to attach to the message */
      /* The first example here is for a plain text attachment */
      /* It makes repeated calls to demo_mail.write_text */
      demo_mail.begin_attachment(conn => conn,
                                 mime_type => 'text/plain; charset=iso-8859-1',
                                 inline => TRUE,
                                 filename => v_file_name,
                                 transfer_enc => '8 bit');

      demo_mail.write_text(conn => conn, message => 'Order Origin,Unscrubbed Count,'    ||
               'Unscrub Ship Today Count,Pending Count,Pending Ship Today Count,'       ||
               'Removed Count'||crlf);

      open dashboard_cursor;
      loop
         fetch dashboard_cursor into v_line;

         exit when dashboard_cursor%notfound;

         mesg := v_line || crlf;
         demo_mail.write_text(conn => conn, message => mesg);
      end loop;

      close dashboard_cursor;
      demo_mail.end_attachment(conn => conn );
   END begin_attachment;

   /* Must call this for anything to work */
   demo_mail.end_mail(conn => conn);

/* Trouble? */
EXCEPTION
   when no_data_found then

      demo_mail.end_attachment( conn => conn );
   when others then
      demo_mail.end_attachment( conn => conn );
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('Error number is ' || err_num);
      dbms_output.put_line('Error message is ' || err_msg);
END send_dashboard_results;
/
