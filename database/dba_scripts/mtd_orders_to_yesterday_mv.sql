

-- Materialized view to collect MTD orders to the end of yesterday.
-- Used in send_com_hourly_stats proc for holiday monitoring.
drop materialized view ops$oracle.mtd_orders_to_yesterday_mv;
create materialized view ops$oracle.mtd_orders_to_yesterday_mv
using  no index
as
select trunc(sysdate-(1/24),'month') mtd_beg_date,
       trunc(sysdate-(1/24)) mtd_end_date,
       sum(decode(cat.transaction_type,'Order',1,0)) order_count
from   clean.order_details cod
join   clean.orders co on (co.order_guid=cod.order_guid)
join   ftd_apps.origins fao on (co.origin_id = fao.origin_id)
join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
where  fao.origin_type in ('phone','bulk','ariba','internet')
and    cat.transaction_date >= trunc(sysdate-(1/24),'month')
and    cat.transaction_date < trunc(sysdate-(1/24))
and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
and    cat.transaction_type = 'Order'
and    cat.payment_type <> 'NC'
and    co.origin_id not in ('TEST', 'test')
and    co.source_code not in ('12743');
