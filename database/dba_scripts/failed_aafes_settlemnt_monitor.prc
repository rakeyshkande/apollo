create or replace procedure ops$oracle.failed_aafes_settlemnt_monitor (in_days_back number default 0) is

   v_file_name                VARCHAR2(100) := 'Failed_Aafes_Settlements_'||to_char(sysdate,'yyyymmdd_hh24')||'.csv';   /* ascii file attachment */
   v_file_handle              utl_file.file_type;
   v_file_handle_2            utl_file.file_type;
   v_line                     VARCHAR2(5000);
   v_body                     VARCHAR2(4000);
   
   conn                       UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   crlf                       VARCHAR2(2) := chr(13) || chr(10);

   cursor aafes_cursor  is
      select abd.payment_id                          ||','||
             o.master_order_number                   ||','||
             '="' || abd.ticket_number || '"'         ||','||
             '="' || abd.auth_code     || '"'         ||','||
             p.payment_indicator                     ||','||
             p.auth_date                             ||','||
             to_char(p.credit_amount,'999,999.99')   ||','||
             '"' || substr(REPLACE(TRANSLATE(TRANSLATE(TRANSLATE(abd.return_message, chr(10), ' '), chr(13), ' '), chr(9), ' '),'"','""'),1,3500) || '"'  ||','||
             abd.created_on                          ||','||
             abd.updated_on
      from clean.aafes_billing_details abd
      join clean.payments p
        on abd.payment_id=p.payment_id
      join clean.orders o
        on o.order_guid=p.order_guid
      where p.payment_indicator='P'
        and abd.return_message is not null
        and abd.created_on>=trunc(sysdate-in_days_back)
      UNION
      select abd.payment_id                               ||','||
             o.master_order_number                        ||','||
             '="' || abd.ticket_number || '"'              ||','||
             '="' || abd.auth_code     || '"'              ||','||
             p.payment_indicator                          ||','||
             p.auth_date                                  ||','||
             to_char((-1)*p.debit_amount,'999,999.99')    ||','||
             '"' || substr(REPLACE(TRANSLATE(TRANSLATE(TRANSLATE(abd.return_message, chr(10), ' '), chr(13), ' '), chr(9), ' '),'"','""'),1,3500) || '"'  ||','||
             abd.created_on                               ||','||
             abd.updated_on 
      from clean.aafes_billing_details abd
      join clean.payments p
        on abd.payment_id=p.payment_id
      join clean.orders o
        on o.order_guid=p.order_guid
      where p.payment_indicator='R'
        and abd.return_message is not null
        and abd.created_on>trunc(sysdate-in_days_back);
 

BEGIN

   dbms_output.put_line('Starting failed_aafes_settlemnt_monitor proc at ' || to_char(sysdate,'MM/DD/YYYY HH24:MI:SS') ||
                        ' looking for orders since ' || trunc(sysdate-in_days_back));

   open aafes_cursor;

   fetch aafes_cursor into v_line;

   if aafes_cursor%rowcount > 0 THEN
      dbms_output.put_line('Found orders - create csv file and send email');

      --#######################################################################
      --##  CREATE THE FILE
      --#######################################################################

      v_file_handle := utl_file.fopen(location => 'LOG_FILE_OUTPUT',
                                      filename => v_file_name,
                                      open_mode => 'w',
                                      max_linesize => 5002);

      while NOT aafes_cursor%notfound loop
         utl_file.put_line (file      => v_file_handle, 
                            buffer    => v_line, 
                            autoflush => TRUE);

         fetch aafes_cursor into v_line;
      end loop;

      utl_file.fclose(v_file_handle);


      --#######################################################################
      --##  ATTACH AND SEND THE FILE
      --#######################################################################

      conn := demo_mail.begin_mail(recipient_project => 'B.A. ONCALL PAGER ALERTS',
                                   subject           => 'AAFES Settlement Errors since ' || trunc(sysdate-in_days_back),
                                   mime_type         => demo_mail.MULTIPART_MIME_TYPE);

      -- THE BA Team has asked that the SQL be included in the email body.  Attempts
      -- to make use dynamic SQL so the string can be used for the query AND the email
      -- body were unsuccessful (the text in the email was not very readable).
      -- So, IF CHANGES ARE MADE TO THE SQL ABOVE, IT NEEDS REFLECTED HERE!!!

      v_body := '<h3>The attached spreadsheet lists <font color=red>orders/refunds which ' ||
                'failed to settle</font> with AAFES. <u>Please investigate</u>.</h3>'      || crlf || crlf ||
                '<h4><br><font color=blue>Use the following SQL to extract the orders found in the spreadsheet...</font><pre>'    ||
          'select abd.payment_id                          ||'',''||          '              || crlf||'<br>' ||
          '       o.master_order_number                   ||'',''||          '              || crlf||'<br>' ||
          '       ''"'' || abd.ticket_number || ''"''         ||'',''||      '              || crlf||'<br>' ||
          '       ''"'' || abd.auth_code     || ''"''         ||'',''||      '              || crlf||'<br>' ||
          '       p.payment_indicator                     ||'',''||          '              || crlf||'<br>' ||
          '       p.auth_date                             ||'',''||          '              || crlf||'<br>' ||
          '       to_char(p.credit_amount,''999,999.99'')   ||'',''||        '              || crlf||'<br>' ||
          '       ''"'' || substr(REPLACE(TRANSLATE(TRANSLATE(TRANSLATE(abd.return_message, chr(10), '' ''), chr(13), '' ''), chr(9), '' ''),''"'',''""''),1,3500) || ''"''  ||'',''||   '  || crlf||'<br>' ||
          '       abd.created_on                          ||'',''||          '              || crlf||'<br>' ||
          '       abd.updated_on                                             '              || crlf||'<br>' ||
          'from clean.aafes_billing_details abd                              '              || crlf||'<br>' ||
          'join clean.payments p                                             '              || crlf||'<br>' ||
          '  on abd.payment_id=p.payment_id                                  '              || crlf||'<br>' ||
          'join clean.orders o                                               '              || crlf||'<br>' ||
          '  on o.order_guid=p.order_guid                                    '              || crlf||'<br>' ||
          'where p.payment_indicator=''P''                                   '              || crlf||'<br>' ||
          '  and abd.return_message is not null                              '              || crlf||'<br>' ||
          '  and abd.created_on>=trunc(sysdate-' || in_days_back || ')       '              || crlf||'<br>' ||
          'UNION'                                                                           || crlf||'<br>' ||
          'select abd.payment_id                            ||'',''||        '              || crlf||'<br>' ||
          '       o.master_order_number                     ||'',''||        '              || crlf||'<br>' ||
          '       ''"'' || abd.ticket_number || ''"''           ||'',''||    '              || crlf||'<br>' ||
          '       ''"'' || abd.auth_code     || ''"''           ||'',''||    '              || crlf||'<br>' ||
          '       p.payment_indicator                       ||'',''||        '              || crlf||'<br>' ||
          '       p.auth_date                               ||'',''||        '              || crlf||'<br>' ||
          '       to_char((-1)*p.debit_amount,''999,999.99'') ||'',''||      '              || crlf||'<br>' ||
          '       ''"'' || substr(REPLACE(TRANSLATE(TRANSLATE(TRANSLATE(abd.return_message, chr(10), '' ''), chr(13), '' ''), chr(9), '' ''),''"'',''""''),1,3500) || ''"''  ||'',''||     '  || crlf||'<br>' ||
          '       abd.created_on                            ||'',''||        '              || crlf||'<br>' ||
          '       abd.updated_on                                             '              || crlf||'<br>' ||
          'from clean.aafes_billing_details abd                              '              || crlf||'<br>' ||
          'join clean.payments p                                             '              || crlf||'<br>' ||
          '  on abd.payment_id=p.payment_id                                  '              || crlf||'<br>' ||
          'join clean.orders o                                               '              || crlf||'<br>' ||
          '  on o.order_guid=p.order_guid                                    '              || crlf||'<br>' ||
          'where p.payment_indicator=''R''                                   '              || crlf||'<br>' ||
          '  and abd.return_message is not null                              '              || crlf||'<br>' ||
          '  and abd.created_on>trunc(sysdate-' || in_days_back || ');       '              || crlf||'<br>' ||
        '</h4></pre>';


      demo_mail.attach_mb_text(conn      => conn,
                               data      => crlf || v_body,
                               mime_type => 'text/html; charset=iso-8859-1' );
                        --     mime_type => 'text/plain; charset=iso-8859-1' );


      demo_mail.begin_attachment(conn         => conn,
                                 mime_type    => 'text/plain; charset=iso-8859-1',
                                 inline       => TRUE,
                                 filename     => v_file_name,
                                 transfer_enc => '8 bit');



      demo_mail.write_text(conn   => conn, 
                          message => 'PAYMENT_ID,MASTER_ORDER_NUMBER,TICKET_NUMBER,AUTH_CODE,' ||
                                     'PAYMENT_INDICATOR,AUTH_DATE,AMOUNT,RETURN_MESSAGE,' ||
                                     'FIRST_ATTEMPTED_ON,LAST_ATTEMPTED_ON'||crlf);
                                     


      v_file_handle_2 := utl_file.fopen(location     => 'LOG_FILE_OUTPUT',
                                        filename     => v_file_name,
                                        open_mode    => 'r',
                                        max_linesize => 5000);

      begin
         loop
            utl_file.get_line(v_file_handle_2, v_line, 5000);
            demo_mail.write_text(conn    => conn, 
                                 message => v_line || crlf);
         end loop;
      exception when others then null;
      end;

      utl_file.fclose(v_file_handle_2);

      demo_mail.end_attachment(conn => conn);
      demo_mail.end_mail(conn => conn);

   else
         dbms_output.put_line('Found no orders - do not send email');
   end if;   /* aafes_cursor%rowcount > 0 */

   close aafes_cursor;

end failed_aafes_settlemnt_monitor;
/
