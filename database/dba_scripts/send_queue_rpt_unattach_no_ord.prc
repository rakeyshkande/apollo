create or replace PROCEDURE ops$oracle.SEND_QUEUE_RPT_UNATTACH_NO_ORD   is

   -- This selects a different subset of data than the send_queue_report job.
   -- It was requested during Val2008 post-holiday processing and should be
   -- dbms_job scheduled per the wishes of the customer.

   -- The purpose of the report is to send Unattached No Order Found emails.


   -- We want to create a CSV file, so some considerations are:
   --    * could be embedded single-quotes, so double-quotes need to surround string
   --    * any double-quotes in the string need replaced with 2 double-quotes
   --    * CLOB translations:  char(9)  = Tab = replaced with a space
   --                          char(10) = LF  = replaced with a space
   --                          char(13) = CR  = replaced with a space

   v_file_name        VARCHAR2(100);        /* ascii file attachment */
   v_file_handle      UTL_FILE.FILE_TYPE;
   v_directory_name   VARCHAR2(100) := 'DICTIONARY_OUTPUT';
   v_line             VARCHAR2(5000);

   conn               UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   mesg               VARCHAR2(32767);
   crlf               VARCHAR2(2) := chr(13) || chr(10);
   err_num            NUMBER;
   err_msg            VARCHAR2(100);

   pager_cursor       types.ref_cursor;
   type pager_type is table of sitescope.pagers.pager_number%type;
   pager_table        pager_type;

   cursor queue_cursor is
      select distinct q.system                         ||','||
                      q.message_id                     ||','||
                      q.queue_type                     ||','||
                      q.message_type                   ||','||
                      q.message_timestamp              ||','||
                      q.mercury_number                 ||','||
                      q.external_order_number          ||','||
                      '"' || SUBSTR(REPLACE(TRANSLATE(TRANSLATE(TRANSLATE(substr(poc.body,1,4000), chr(10),' '), chr(13), ' '), chr(9), ' '),'"','""'),1,3500) || '"' email_txt
      from clean.queue q
      join clean.point_of_contact poc 
        on q.point_of_contact_id = poc.point_of_contact_id
      where q.master_order_number is null
        and trunc(q.message_timestamp) = trunc(poc.created_on);


BEGIN
   v_file_name := 'Queue_Rpt_Unattach_No_Order_'||to_char(sysdate,'yyyymmdd')||'.csv';

   open queue_cursor;
   fetch queue_cursor into v_line;


   BEGIN
      /* Fill in to-from and subject info here */
      conn := demo_mail.begin_mail(recipient_project => 'Queue Report No Orders',
                                   subject => 'Unattached No Order Emails',
                                   mime_type => demo_mail.MULTIPART_MIME_TYPE);
   END begin_mail;

   BEGIN
      /* The first call to attach_text will be the body of the mess*/
      /* You can format the message with HTML formatting codes */
      demo_mail.attach_mb_text(conn => conn,
                               data => 'A report of "Unattached No Order Found" emails is attached.',
                               mime_type => 'text/plain; charset=iso-8859-1' );
   END attach_text;

   BEGIN
      /* You must call begin_attachment and end_attachment */
      /* for any files you wish to attach to the message */
      /* The first example here is for a plain text attachment */
      /* It makes repeated calls to demo_mail.write_text */
      demo_mail.begin_attachment(conn => conn,
                                 mime_type => 'text/plain; charset=iso-8859-1',
                                 inline => TRUE,
                                 filename => v_file_name,
                                 transfer_enc => '8 bit');

      demo_mail.write_text(conn => conn, 
                          message => 'System,Message ID,Queue Type,'   ||
                                     'Message Type,Message Timestamp,Mercury Number,External Order Number,' ||
                                     'Message' || crlf);

      
      while NOT queue_cursor%notfound loop

         mesg := v_line || crlf;
         demo_mail.write_text(conn => conn, message => mesg);
         fetch queue_cursor into v_line;
      end loop;

      close queue_cursor;
      demo_mail.end_attachment(conn => conn );
   END begin_attachment;

   /* Must call this for anything to work */
   demo_mail.end_mail(conn => conn);

/* Trouble? */
EXCEPTION
   when no_data_found then
      demo_mail.end_attachment( conn => conn );
   when others then
      demo_mail.end_attachment( conn => conn );
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('Error number is ' || err_num);
      dbms_output.put_line('Error message is ' || err_msg);
END SEND_QUEUE_RPT_UNATTACH_NO_ORD;
/