CREATE OR REPLACE PROCEDURE OPS$ORACLE.SEND_hourly_REFUND_REPORT is

   -- REPORT INTRODUCED DURING VAL2008 - RUN HOURLY THE DAYS AFTER THE HOLIDAY

   v_file_name        VARCHAR2(100);        /* ascii file attachment */
   v_file_handle      UTL_FILE.FILE_TYPE;
   v_directory_name   VARCHAR2(100) := 'DICTIONARY_OUTPUT';
   v_line             VARCHAR2(5000);

   conn               UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   mesg               VARCHAR2(32767);
   crlf               VARCHAR2(2) := chr(13) || chr(10);
   err_num            NUMBER;
   err_msg            VARCHAR2(100);

   pager_cursor       types.ref_cursor;
   type pager_type is table of sitescope.pagers.pager_number%type;
   pager_table        pager_type;

   cursor refund_cursor is select
       r.REFUND_ID                                         ||','||
       r.REFUND_DISP_CODE                                  ||','||
       r.PERCENT                                           ||','||
       to_char(od.DELIVERY_DATE,'MM/DD/YYYY')              ||','||
       to_char(r.CREATED_ON,'MM/DD/YYYY HH24:MI')          ||','||
       r.CREATED_BY                                        ||','||
       to_char(r.UPDATED_ON,'MM/DD/YYYY HH24:MI')          ||','||
       r.UPDATED_BY                                        ||','||
       to_char(r.REFUND_PRODUCT_AMOUNT,'9,999.99')         ||','||
       to_char(r.REFUND_ADDON_AMOUNT, '9,999.99')          ||','||
       to_char(r.REFUND_SERVICE_FEE,'9,999.99')            ||','||
       to_char(r.REFUND_TAX,'9,999.99')                    ||','||
       r.ORDER_DETAIL_ID                                   ||','||
       r.RESPONSIBLE_PARTY                                 ||','||
       r.REFUND_STATUS                                     ||','||
       to_char(r.REFUND_DATE,'MM/DD/YYYY HH24:MI')         ||','||
       to_char(r.REVIEWED_ON,'MM/DD/YYYY HH24:MI')         ||','||
       r.REVIEWED_BY                                       ||','||
       to_char(r.SETTLED_DATE,'MM/DD/YYYY HH24:MI')        ||','||
       to_char(r.REFUND_ADMIN_FEE,'9,999.99')              ||','||
       to_char(r.REFUND_SHIPPING_FEE,'9,999.99')           ||','||
       to_char(r.REFUND_SERVICE_FEE_TAX,'9,999.99')        ||','||
       to_char(r.REFUND_SHIPPING_TAX ,'9,999.99')          ||','||
       to_char(r.REFUND_DISCOUNT_AMOUNT,'9,999.99')        ||','||
       to_char(r.REFUND_COMMISSION_AMOUNT,'9,999.99')      ||','||
       to_char(r.REFUND_WHOLESALE_AMOUNT ,'9,999.99')      ||','||
       to_char(r.REFUND_WHOLESALE_SERVICE_FEE,'9,999.99')  ||','||
       r.POP_REFUND_FLAG                                   ||','||
       '"' || SUBSTR(REPLACE(TRANSLATE(TRANSLATE(TRANSLATE(r.AP_REFUND_TRANS_TXT, chr(10),' '), chr(13), ' '), chr(9), ' '),'"','""'),1,3500) || '"'     ||','||
       decode(pm.preferred_partner_flag, 'Y', pm.partner_name, '') refund_txt
   from clean.refund r
   join clean.order_details od
     on r.order_detail_id=od.order_detail_id
   LEFT OUTER JOIN
   (
      select  spr.source_code, spr.program_name
      from    ftd_apps.source_program_ref spr
      where   spr.start_date =
      (
         select  max(b.start_date)
         from    ftd_apps.source_program_ref b
         where   b.source_code = spr.source_code
      )
   ) spr
      on od.source_code = spr.source_code
   left outer join ftd_apps.partner_program pp
      on pp.program_name = spr.program_name
   left outer join ftd_apps.partner_master pm
      on pm.partner_name = pp.partner_name
   where r.created_on>trunc(sysdate);


BEGIN
   v_file_name := 'Hourly_Refund_Reason_'||to_char(sysdate,'yyyymmdd-HH24MI')||'.csv';

   open refund_cursor;
   fetch refund_cursor into v_line;

   BEGIN
      /* Fill in to-from and subject info here */
      conn := demo_mail.begin_mail(
         recipient_project => 'Refund Reason',
         subject => 'Hourly Refund Report for ' || to_char(sysdate,'MM/DD/YYYY HH24:MI'),                          
         mime_type => demo_mail.MULTIPART_MIME_TYPE);
   END begin_mail;

   BEGIN
      /* The first call to attach_text will be the body of the mess*/
      /* You can format the message with HTML formatting codes */
      demo_mail.attach_mb_text(
            conn => conn,
            data => 'The HOURLY refund reason report is attached.',
            mime_type => 'text/plain; charset=iso-8859-1' );
   END attach_text;

   BEGIN
      /* You must call begin_attachment and end_attachment */
      /* for any files you wish to attach to the message */
      /* The first example here is for a plain text attachment */
      /* It makes repeated calls to demo_mail.write_text */
      demo_mail.begin_attachment(conn => conn,
                                 mime_type => 'text/plain; charset=iso-8859-1',
                                 inline => TRUE,
                                 filename => v_file_name,
                                 transfer_enc => '8 bit');

      demo_mail.write_text(conn => conn, message => 'Refund ID,Disp Code,Percent,' ||
               'Delivery Date,Created On,Created By,Updated On,Updated By,Produt Amt,' ||
               'Addon Amt,Service Fee,Tax,Order Detail ID,Responsible Party,'||
               'Status,Refund Date,Reviewed On,Reviewed By,Settled Date,Admin Fee,' ||
               'Shipping Fee,Service Fee Tax,Shipping Tax,Discount Amt,' ||
               'Commission Amt,Wholesale Amt,Wholesale Service Fee,POP Refund Flag,'||
               'AP Refund Trans Txt, Preferred Partner'||crlf);


      while NOT refund_cursor%notfound loop
         mesg := v_line || crlf;
         demo_mail.write_text(conn => conn, message => mesg);
         fetch refund_cursor into v_line;
      end loop;

      close refund_cursor;
      demo_mail.end_attachment(conn => conn );
   END begin_attachment;

   /* Must call this for anything to work */
   demo_mail.end_mail(conn => conn);

/* Trouble? */
EXCEPTION
   when no_data_found then

      demo_mail.end_attachment( conn => conn );
   when others then
      demo_mail.end_attachment( conn => conn );
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('Error number is ' || err_num);
      dbms_output.put_line('Error message is ' || err_msg);
END send_hourly_refund_report;
/
