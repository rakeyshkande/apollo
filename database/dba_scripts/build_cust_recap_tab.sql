set echo on timing on
conn / as sysdba
drop table clean.cust_recap_tab;

create table clean.cust_recap_tab(
master_order_number varchar2(100),
customer_id        number,
company_id         varchar2(12),
order_date         date,
order_detail_id    number,
concat_id          varchar2(82),
order_disp_code    varchar2(20),
payment_id         number,
payment_type       varchar2(2),
additional_bill_id number,
refund_id          number,
refund_disp_code   varchar2(5))
tablespace clean_data nologging;

grant select on clean.cust_recap_tab to rpt;

insert /*+ APPEND */ into clean.cust_recap_tab
select o.master_order_number, o.customer_id,
       decode(o.company_id, 'GSC', 'FTD', 'SFMB', 'FTD', o.company_id) company_id,
       o.order_date, od.order_detail_id, c.concat_id, od.order_disp_code, p.payment_id, p.payment_type,
       p.additional_bill_id, p.refund_id, r.refund_disp_code
from   clean.orders o
join   clean.order_details od
on     o.order_guid = od.order_guid
and    od.order_disp_code not in ('In-Scrub','Pending','Removed')
join   clean.customer c
on     o.customer_id = c.customer_id
join   clean.payments p
on     p.order_guid = o.order_guid
left outer join clean.refund r on r.order_detail_id = od.order_detail_id;

commit;

@trim_cust_recap_tab
exit
