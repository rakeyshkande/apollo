set echo on timing on
create index clean.cust_recap_tab_n2 on clean.cust_recap_tab (payment_type) tablespace clean_indx nologging compute statistics;

create index clean.cust_recap_tab_n1 on clean.cust_recap_tab (master_order_number) tablespace clean_indx nologging
compute statistics;

delete from clean.cust_recap_tab where master_order_number in (
select master_order_number
from   clean.cust_recap_tab
where  payment_type = 'NC'
and    additional_bill_id is null
and    refund_id is null);

drop index clean.cust_recap_tab_n2;

create index clean.cust_recap_tab_n3 on clean.cust_recap_tab (order_detail_id) tablespace clean_indx nologging compute statistics;

create index clean.cust_recap_tab_n4 on clean.cust_recap_tab (refund_disp_code) tablespace clean_indx nologging compute statistics;

delete from clean.cust_recap_tab where order_detail_id in (
select order_detail_id
from   clean.cust_recap_tab
where  refund_disp_code like 'A%');

drop index clean.cust_recap_tab_n4;

alter index clean.cust_recap_tab_n1 rebuild compute statistics;

alter index clean.cust_recap_tab_n3 rebuild compute statistics;

create index clean.cust_recap_tab_n5 on clean.cust_recap_tab (order_date) tablespace clean_indx nologging compute statistics;

exit
