create or replace procedure ops$oracle.send_product_list is
   v_file_name VARCHAR2(100); /* ascii file attachment */
   v_file_handle UTL_FILE.FILE_TYPE;
   v_directory_name VARCHAR2(100) := 'DICTIONARY_OUTPUT';
   v_line VARCHAR2(5000);
   conn UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   mesg VARCHAR2(32767);
   crlf VARCHAR2(2) := chr(13) || chr(10);
   err_num NUMBER;
   err_msg VARCHAR2(100);
   pager_cursor types.ref_cursor;
   type pager_type is table of sitescope.pagers.pager_number%type;
   pager_table pager_type;
   cursor product_cursor is
select florist_id||','||
"06-CP"||','||
"0-DRM"||','||
"00-FF"||','||
"06-GL"||','||
"0-LLL"||','||
"04-G1"||','||
"06-V2"||','||
"06-M3"||','||
"06-S1"||','||
"06-SW"||','||
"06-B1"||','||
"06-G1"
from (
select cod.florist_id, 
    sum(decode(product_id,'06CP',1,0)) "06-CP",
    sum(decode(product_id,'0DRM',1,0)) "0-DRM",
    sum(decode(product_id,'00FF',1,0)) "00-FF",
    sum(decode(product_id,'00GL',1,0)) "06-GL",
    sum(decode(product_id,'0LLL',1,0)) "0-LLL",
    sum(decode(product_id,'04G1',1,0)) "04-G1",
    sum(decode(product_id,'06V2',1,0)) "06-V2",
    sum(decode(product_id,'06M3',1,0)) "06-M3",
    sum(decode(product_id,'06S1',1,0)) "06-S1",
    sum(decode(product_id,'06SW',1,0)) "06-SW",
    sum(decode(product_id,'06B1',1,0)) "06-B1",
    sum(decode(product_id,'06G1',1,0)) "06-G1"
    from clean.orders co,
    clean.order_details cod
    where cod.product_id in 
    (   '06CP',
        '0DRM',
        '00FF',
        '00GL',
        '0LLL',
        '04G1',
        '06V2',
        '06M3',
        '06S1',
        '06SW',
        '06B1',
        '06G1'
        )
    and co.order_guid = cod.order_guid
    and  co.order_date >= to_date('05/01/2006','MM/DD/YYYY')
    and  cod.florist_id is not null
    group by cod.florist_id    UNION
    (select 'Total',
    sum(decode(product_id,'06CP',1,0)) "06-CP",
    sum(decode(product_id,'0DRM',1,0)) "0-DRM",
    sum(decode(product_id,'00FF',1,0)) "00-FF",
    sum(decode(product_id,'00GL',1,0)) "06-GL",
    sum(decode(product_id,'0LLL',1,0)) "0-LLL",
    sum(decode(product_id,'04G1',1,0)) "04-G1",	
    sum(decode(product_id,'06V2',1,0)) "06-V2",
    sum(decode(product_id,'06M3',1,0)) "06-M3",
    sum(decode(product_id,'06S1',1,0)) "06-S1",
    sum(decode(product_id,'06SW',1,0)) "06-SW",
    sum(decode(product_id,'06B1',1,0)) "06-B1",
    sum(decode(product_id,'06G1',1,0)) "06-G1"
    from clean.orders co,
    clean.order_details cod
    where cod.product_id in 
    (   '06CP',
        '0DRM',
        '00FF',
        '00GL',
        '0LLL',
        '04G1',
        '06V2',
        '06M3',
        '06S1',
        '06SW',
        '06B1',
        '06G1'
        )
    and co.order_guid = cod.order_guid
    and  co.order_date >= to_date('05/01/2006','MM/DD/YYYY')
    and  cod.florist_id is not null
    )
    order by FLORIST_ID asc);


BEGIN
   v_file_name := 'Product_List_'||to_char(sysdate,'yyyymmdd')||'.csv';
   BEGIN
      /* Fill in to-from and subject info here */
      conn := demo_mail.begin_mail(
         recipient_project => 'Product List',
         subject => 'Daily Product List',
         mime_type => demo_mail.MULTIPART_MIME_TYPE);
   END begin_mail;
   BEGIN
      /* The first call to attach_text will be the body of the mess*/
      /* You can format the message with HTML formatting codes */
      demo_mail.attach_mb_text(
            conn => conn,
            data => 'The daily product list is attached.',
            mime_type => 'text/plain; charset=iso-8859-1' );
   END attach_text;
   BEGIN
      /* You must call begin_attachment and end_attachment */
      /* for any files you wish to attach to the message */
      /* The first example here is for a plain text attachment */
      /* It makes repeated calls to demo_mail.write_text */
      demo_mail.begin_attachment(conn => conn,
                                 mime_type => 'text/plain; charset=iso-8859-1',
                                 inline => TRUE,
                                 filename => v_file_name,
                                 transfer_enc => '8 bit');
      demo_mail.write_text(conn => conn, message => 'Florist ID,06-CP,0-DRM,00-FF,06-GL,0-LLL,04-G1,06-V2,06-M3,06-S1,06-SW,06-B1,06-G1'||crlf);
      open product_cursor;
      loop
         fetch product_cursor into v_line;
         exit when product_cursor%notfound;
         mesg := v_line || crlf;
         demo_mail.write_text(conn => conn, message => mesg);
      end loop;
      close product_cursor;
      demo_mail.end_attachment(conn => conn );
   END begin_attachment;
   /* Must call this for anything to work */
   demo_mail.end_mail(
   conn => conn);
/* Trouble? */
EXCEPTION
   when no_data_found then
      demo_mail.end_attachment( conn => conn );
   when others then
      demo_mail.end_attachment( conn => conn );
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('Error number is ' || err_num);
      dbms_output.put_line('Error message is ' || err_msg);
END send_product_list;
/
