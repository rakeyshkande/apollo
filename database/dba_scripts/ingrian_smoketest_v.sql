

grant execute on global.encryption to ops$oracle;
grant execute on frp.misc_pkg to ops$oracle;
create or replace view ops$oracle.ingrian_smoketest_v as 
select ingrian_result from
  (select 'If the next row is legible, Ingrian encryption/decryption is functioning:' ingrian_result,
        1 col_order from dual
   UNION
   select global.encryption.decrypt_it(global.encryption.encrypt_it('Encryption Test Successful!!',
                                                    frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')),
                                     frp.misc_pkg.get_global_parm_value('Ingrian','Current Key')), 2 from dual
   order by col_order);




