set serveroutput on size 1000000
set echo on
spool recrypt_clean_billing_detail.lst
select count(*) from clean.billing_detail where cc_number is null;
select key_name, count(*) from clean.billing_detail group by key_name;
declare
   cursor c1 is
      select billing_detail_id
      from   clean.billing_detail
      where  key_name is null
      and    cc_number is not null;
   r1 c1%rowtype;
   v_count number;
   v_key_name varchar2(30);
begin
   v_count := 0;
   v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');
   open c1;
   loop
      fetch c1 into r1;
      exit when c1%notfound;
      update clean.billing_detail
      set    cc_number = global.encryption.encrypt_it(cc_number, v_key_name),
             key_name = v_key_name
      WHERE  BILLING_detail_id = r1.billing_detail_id;
      v_count := v_count + 1;
      if v_count >= 10000 then
         commit;
         v_count := 0;
      end if;
   end loop;
   close c1;
end;
/
select count(*) from clean.billing_detail where cc_number is null;
select key_name, count(*) from clean.billing_detail group by key_name;
commit;
exit
