create or replace procedure ops$oracle.dump_ask_ans_di is
   v_file_name VARCHAR2(100); /* ascii file attachment */
   v_file_handle UTL_FILE.FILE_TYPE;
   v_directory_name VARCHAR2(100) := 'DICTIONARY_OUTPUT';
   v_line VARCHAR2(5000);
   conn UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   mesg VARCHAR2(32767);
   crlf VARCHAR2(2) := chr(13) || chr(10);
   err_num NUMBER;
   err_msg VARCHAR2(100);
   pager_cursor types.ref_cursor;
   type pager_type is table of sitescope.pagers.pager_number%type;
   pager_table pager_type;
   cursor dump_cursor is
      select q.message_id||','||q.queue_type||','||q.message_type||','||q.message_timestamp||','||q.mercury_number||','||
             q.external_order_number||',,'||m.sending_florist||','||m.price||','||m2.price||',"'||
             TRANSLATE(M.operator, chr(10)||chr(13)||chr(9)||'"', '   ''')||'","'||
             TRANSLATE(M.COMMENTS, chr(10)||chr(13)||chr(9)||'"', '   ''')||'",'||
             m2.reference_number
      from   clean.queue q, mercury.mercury m , mercury.mercury m2
      where  queue_type IN ('ASK', 'ANS')
      and    message_type NOT IN ('ASKPI','ANSPI')
      and    m.ask_answer_code is null
      and    q.mercury_number = m.mercury_message_number
      and    m2.mercury_order_number = m.mercury_order_number
      and    m2.msg_type = 'FTD'
      and    m.message_direction = 'INBOUND'
      UNION
      select q.message_id||','||q.queue_type||','||q.message_type||','||q.message_timestamp||','||q.mercury_number||','||
             q.external_order_number||',,'||v.filling_vendor||','||v.price||','||v2.price||',"'||
             TRANSLATE(v.operator, chr(10)||chr(13)||chr(9)||'"', '   ''')||'","'||
             TRANSLATE(v.COMMENTS, chr(10)||chr(13)||chr(9)||'"', '   ''')||'",'||
             v2.reference_number
      from   clean.queue q, venus.venus v, venus.venus v2
      where  queue_type IN ('ASK', 'ANS')
      and    message_type NOT IN ('ASKPI','ANSPI')
      and    q.mercury_number = v.venus_order_number
      and    v2.venus_order_number = v.venus_order_number
      and    v2.msg_type = 'FTD'
      and    v.message_direction = 'INBOUND'
      UNION
      select q.message_id||','||q.queue_type||','||q.message_type||','||q.message_timestamp||','||q.mercury_number||','||
             q.external_order_number||','||
            (select to_char(delivery_date,'MM/DD/YYYY')
             from   clean.order_details cod
             where  cod.external_order_number = q.external_order_number)||',,,,'||'N/A'||',"'||
             TRANSLATE(poc.body, chr(10)||chr(13)||chr(9)||'"', '   ''')||'",'
      from   clean.queue q, clean.point_of_contact poc
      where  queue_type IN ('DI','OT')
      and    poc.point_of_contact_id = q.point_of_contact_id;

BEGIN
   v_file_name := 'Dump_ASK_ANS_DI_OT_'||to_char(sysdate,'yyyymmdd')||'.csv';
   BEGIN
      /* Fill in to-from and subject info here */
      conn := demo_mail.begin_mail(
         recipient_project => 'Dump_ASK_ANS_DI_OT',
         subject => 'Daily ASK/ANS/DI/OT Dump',
         mime_type => demo_mail.MULTIPART_MIME_TYPE);
   END begin_mail;
   BEGIN
      /* The first call to attach_text will be the body of the mess*/
      /* You can format the message with HTML formatting codes */
      demo_mail.attach_mb_text(
            conn => conn,
            data => 'The daily dump of ASK/ANS/DI/OT messages is attached.',
            mime_type => 'text/plain; charset=iso-8859-1' );
   END attach_text;
   BEGIN
      /* You must call begin_attachment and end_attachment */
      /* for any files you wish to attach to the message */
      /* The first example here is for a plain text attachment */
      /* It makes repeated calls to demo_mail.write_text */
      demo_mail.begin_attachment(conn => conn,
                                 mime_type => 'text/plain; charset=iso-8859-1',
                                 inline => TRUE,
                                 filename => v_file_name,
                                 transfer_enc => '8 bit');
      demo_mail.write_text(conn => conn, message => 'Message ID,Queue Type,Message Type,Timestamp,Mercury Number,'||
         'External Order Number,Delivery Date,Sending Florist,New Price,Original Price,Operator,Comments,Reference Number'||crlf);
      open dump_cursor;
      loop
         fetch dump_cursor into v_line;
         exit when dump_cursor%notfound;
         mesg := v_line || crlf;
         demo_mail.write_text(conn => conn, message => mesg);
      end loop;
      close dump_cursor;
      demo_mail.end_attachment(conn => conn );
   END begin_attachment;
   /* Must call this for anything to work */
   demo_mail.end_mail(
   conn => conn);
/* Trouble? */
EXCEPTION
   when no_data_found then
      demo_mail.end_attachment( conn => conn );
   when others then
      demo_mail.end_attachment( conn => conn );
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      dbms_output.put_line('Error number is ' || err_num);
      dbms_output.put_line('Error message is ' || err_msg);
END dump_ask_ans_di;
/
