set serveroutput on size 1000000
set echo on
spool recrypt_clean_credit_cards.lst
select count(*) from clean.credit_cards where cc_number is null;
select key_name, count(*) from clean.credit_cards group by key_name;
declare
   cursor c1 is
      select cc_id
      from   clean.credit_cards
      where  key_name is null
      and    cc_number is not null;
   r1 c1%rowtype;
   v_count number;
   v_key_name varchar2(30);
begin
   v_count := 0;
   v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');
   open c1;
   loop
      fetch c1 into r1;
      exit when c1%notfound;
      update clean.credit_cards
      set    cc_number = global.encryption.encrypt_it(cc_number, v_key_name),
             key_name = v_key_name
      WHERE  cc_id = r1.cc_id;
      v_count := v_count + 1;
      if v_count >= 10000 then
         commit;
         v_count := 0;
      end if;
   end loop;
   close c1;
end;
/
select count(*) from clean.credit_cards where cc_number is null;
select key_name, count(*) from clean.credit_cards group by key_name;
commit;
exit
