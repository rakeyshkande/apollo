

-- Materialized view to collect last-year month-to-date orders.
-- Used in send_com_hourly_stats proc for holiday monitoring.
drop materialized view ops$oracle.ly_mtd_orders_mv;
create materialized view ops$oracle.ly_mtd_orders_mv
using  no index
as
select decode(trunc(sysdate),
              last_day(trunc(sysdate)),
                     trunc(sysdate-365, 'month')+1,
              trunc(sysdate, 'month'),
                     trunc(sysdate-365,'month')+1,
                     trunc(sysdate-364,'month')+1) mtd_beg_date,
       trunc(sysdate)-363 mtd_end_date,
       sum(decode(cat.transaction_type,'Order',1,0)) order_count
from   clean.order_details cod
join   clean.orders co on (co.order_guid=cod.order_guid)
join   ftd_apps.origins fao on (co.origin_id = fao.origin_id)
join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
where  fao.origin_type in ('phone','bulk','ariba','internet')
and    cat.transaction_date >= decode(trunc(sysdate),
                                      last_day(trunc(sysdate)),
                                               trunc(sysdate-365,'month')+1,
                                      trunc(sysdate,'month'),
                                               trunc(sysdate-365,'month')+1,
                                               trunc(sysdate-364,'month')+1)
and    cat.transaction_date < trunc(sysdate)-363
and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
and    cat.transaction_type = 'Order'
and    cat.payment_type <> 'NC'
and    co.origin_id not in ('TEST', 'test')
and    co.source_code not in ('12743');
