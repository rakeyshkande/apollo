CREATE OR REPLACE
TRIGGER ptn_amazon.az_product_master_aiu_trg
BEFORE INSERT OR UPDATE OR DELETE ON ptn_amazon.az_product_master
REFERENCING new as new old as old
FOR EACH ROW

DECLARE

    v_count         number := 0;
    v_other_feeds   varchar2(10) := 'N';

BEGIN

    select count(*) into v_count
    from ptn_amazon.az_product_feed
    where product_id = :new.product_id
    and feed_status = 'NEW';

    if v_count = 0 then

        insert into ptn_amazon.az_product_feed (
            AZ_PRODUCT_FEED_ID,
            PRODUCT_ID,
            FEED_STATUS,
            CREATED_ON,
            CREATED_BY,
            UPDATED_ON,
            UPDATED_BY)
        values (
            ptn_amazon.az_product_feed_id_sq.nextval,
            :new.product_id,
            'NEW',
            sysdate,
            'SYS',
            sysdate,
            'SYS');

    end if;

    if :old.catalog_status_standard is null or (:old.catalog_status_standard = 'I' and :new.catalog_status_standard = 'A') then
        :new.sent_to_amazon_standard_flag := 'N';
        v_other_feeds := 'Y';
    end if;

    if :old.catalog_status_deluxe is null or (:old.catalog_status_deluxe = 'I' and :new.catalog_status_deluxe = 'A') then
        :new.sent_to_amazon_deluxe_flag := 'N';
        v_other_feeds := 'Y';
    end if;

    if :old.catalog_status_premium is null or (:old.catalog_status_premium = 'I' and :new.catalog_status_premium = 'A') then
        :new.sent_to_amazon_premium_flag := 'N';
        v_other_feeds := 'Y';
    end if;

    if v_other_feeds = 'Y' then

        select count(*) into v_count
        from ptn_amazon.az_image_feed
        where product_id = :new.product_id
        and feed_status = 'NEW';

        if v_count = 0 then

            insert into ptn_amazon.az_image_feed (
                AZ_IMAGE_FEED_ID,
                PRODUCT_ID,
                FEED_STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY)
            values (
                ptn_amazon.az_image_feed_id_sq.nextval,
                :new.product_id,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS');

        end if;

        select count(*) into v_count
        from ptn_amazon.az_price_feed
        where product_id = :new.product_id
        and feed_status = 'NEW';

        if v_count = 0 then

            insert into ptn_amazon.az_price_feed (
                AZ_PRICE_FEED_ID,
                PRODUCT_ID,
                FEED_STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY)
            values (
                ptn_amazon.az_price_feed_id_sq.nextval,
                :new.product_id,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS');

        end if;

        select count(*) into v_count
        from ptn_amazon.az_inventory_feed
        where product_id = :new.product_id
        and feed_status = 'NEW';

        if v_count = 0 then

            insert into ptn_amazon.az_inventory_feed (
                AZ_INVENTORY_FEED_ID,
                PRODUCT_ID,
                FEED_STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY)
            values (
                ptn_amazon.az_inventory_feed_id_sq.nextval,
                :new.product_id,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS');

        end if;

    end if;

END AZ_PRODUCT_MASTER_AIU_TRG;
/
