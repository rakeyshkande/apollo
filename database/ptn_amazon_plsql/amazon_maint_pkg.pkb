CREATE OR REPLACE
PACKAGE BODY PTN_AMAZON.AMAZON_MAINT_PKG AS

PROCEDURE INSERT_AZ_ORDER
(
  IN_AZ_ORDER_NUMBER            IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
  IN_MASTER_ORDER_NUMBER        IN AZ_ORDER.MASTER_ORDER_NUMBER%TYPE,
  IN_AZ_REPORT_ID               IN AZ_ORDER.AZ_REPORT_ID%TYPE,
  IN_AZ_XML                     IN AZ_ORDER.AZ_XML%TYPE,
  IN_FTD_XML                    IN AZ_ORDER.FTD_XML%TYPE,
  IN_ORDER_STATUS               IN AZ_ORDER.ORDER_STATUS%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_ORDER table.

Input:
        IN_AZ_ORDER_NUMBER              varchar2
        IN_MASTER_ORDER_NUMBER          varchar2
        IN_AZ_XML                       clob
        IN_FTD_XML                      clob
        IN_ORDER_STATUS                 varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  BEGIN
    INSERT INTO AZ_ORDER (
      AZ_ORDER_NUMBER,
      MASTER_ORDER_NUMBER,
      AZ_REPORT_ID,
      AZ_XML,
      FTD_XML,
      ORDER_STATUS,
      CREATED_ON,
      CREATED_BY,
      UPDATED_ON,
      UPDATED_BY
    ) VALUES (
      IN_AZ_ORDER_NUMBER,
      IN_MASTER_ORDER_NUMBER,
      IN_AZ_REPORT_ID,
      IN_AZ_XML,
      IN_FTD_XML,
      IN_ORDER_STATUS,
      SYSDATE,
      'SYS',
      SYSDATE,
      'SYS'
    );

    OUT_STATUS := 'Y';

END;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AZ_ORDER;


PROCEDURE INSERT_AZ_ORDER_REPORT
(
  IN_ORDER_REPORT_ID             IN AZ_ORDER_REPORT.AZ_REPORT_ID%TYPE,
  IN_ORDER_REPORT                IN AZ_ORDER_REPORT.REPORT%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This new stored procedure will insert a new record into the AZ_ORDER_REPORT
    table.

Input:
        order report id                 varchar2
        order report                    varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
  INSERT INTO AZ_ORDER_REPORT 
  (
    AZ_ORDER_REPORT_ID,
    AZ_REPORT_ID,
    REPORT,
    CREATED_ON,
    CREATED_BY,
    UPDATED_ON,
    UPDATED_BY
  ) 
  VALUES 
  (
    AZ_ORDER_REPORT_ID_SQ.nextval,
    IN_ORDER_REPORT_ID,
    IN_ORDER_REPORT,
    SYSDATE,
    'SYS',
    SYSDATE,
    'SYS'
  );

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AZ_ORDER_REPORT;

PROCEDURE UPDATE_AZ_PRODUCT_MASTER
(
  IN_PRODUCT_ID                  IN AZ_PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_CATALOG_STATUS_STANDARD     IN AZ_PRODUCT_MASTER.CATALOG_STATUS_STANDARD%TYPE,
  IN_PRODUCT_NAME_STANDARD       IN AZ_PRODUCT_MASTER.PRODUCT_NAME_STANDARD%TYPE,
  IN_PRODUCT_DESC_STANDARD       IN AZ_PRODUCT_MASTER.PRODUCT_DESCRIPTION_STANDARD%TYPE,
  IN_CATALOG_STATUS_DELUXE       IN AZ_PRODUCT_MASTER.CATALOG_STATUS_DELUXE%TYPE,
  IN_PRODUCT_NAME_DELUXE         IN AZ_PRODUCT_MASTER.PRODUCT_NAME_DELUXE%TYPE,
  IN_PRODUCT_DESC_DELUXE         IN AZ_PRODUCT_MASTER.PRODUCT_DESCRIPTION_DELUXE%TYPE,
  IN_CATALOG_STATUS_PREMIUM      IN AZ_PRODUCT_MASTER.CATALOG_STATUS_PREMIUM%TYPE,
  IN_PRODUCT_NAME_PREMIUM        IN AZ_PRODUCT_MASTER.PRODUCT_NAME_PREMIUM%TYPE,
  IN_PRODUCT_DESC_PREMIUM        IN AZ_PRODUCT_MASTER.PRODUCT_DESCRIPTION_PREMIUM%TYPE,
  IN_ITEM_TYPE			 IN AZ_PRODUCT_MASTER.ITEM_TYPE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

CURSOR exists_cur IS
        SELECT  product_id
        FROM    az_product_master
        WHERE   product_id = in_product_id;

v_product_id az_product_master.product_id%type;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur INTO v_product_id;
  CLOSE exists_cur;

  IF v_product_id IS NOT NULL THEN

      UPDATE AZ_PRODUCT_MASTER
      SET CATALOG_STATUS_STANDARD = IN_CATALOG_STATUS_STANDARD,
          PRODUCT_NAME_STANDARD = IN_PRODUCT_NAME_STANDARD,
          PRODUCT_DESCRIPTION_STANDARD = IN_PRODUCT_DESC_STANDARD,
          CATALOG_STATUS_DELUXE = IN_CATALOG_STATUS_DELUXE,
          PRODUCT_NAME_DELUXE = IN_PRODUCT_NAME_DELUXE,
          PRODUCT_DESCRIPTION_DELUXE = IN_PRODUCT_DESC_DELUXE,
          CATALOG_STATUS_PREMIUM = IN_CATALOG_STATUS_PREMIUM,
          PRODUCT_NAME_PREMIUM = IN_PRODUCT_NAME_PREMIUM,
          PRODUCT_DESCRIPTION_PREMIUM = IN_PRODUCT_DESC_PREMIUM,
          UPDATED_ON = sysdate,
          UPDATED_BY = 'SYS',
          ITEM_TYPE = IN_ITEM_TYPE
      WHERE PRODUCT_ID = IN_PRODUCT_ID;

  ELSE

      IF IN_CATALOG_STATUS_STANDARD = 'A' OR IN_CATALOG_STATUS_DELUXE = 'A' OR IN_CATALOG_STATUS_PREMIUM = 'A' THEN

          INSERT INTO AZ_PRODUCT_MASTER (
              PRODUCT_ID,
              CATALOG_STATUS_STANDARD,
              PRODUCT_NAME_STANDARD,
              PRODUCT_DESCRIPTION_STANDARD,
              SENT_TO_AMAZON_STANDARD_FLAG,
              CATALOG_STATUS_DELUXE,
              PRODUCT_NAME_DELUXE,
              PRODUCT_DESCRIPTION_DELUXE,
              SENT_TO_AMAZON_DELUXE_FLAG,
              CATALOG_STATUS_PREMIUM,
              PRODUCT_NAME_PREMIUM,
              PRODUCT_DESCRIPTION_PREMIUM,
              SENT_TO_AMAZON_PREMIUM_FLAG,
              ITEM_TYPE,
              CREATED_ON,
              CREATED_BY,
              UPDATED_ON,
              UPDATED_BY
          ) VALUES (
              IN_PRODUCT_ID,
              IN_CATALOG_STATUS_STANDARD,
              IN_PRODUCT_NAME_STANDARD,
              IN_PRODUCT_DESC_STANDARD,
              'N',
              IN_CATALOG_STATUS_DELUXE,
              IN_PRODUCT_NAME_DELUXE,
              IN_PRODUCT_DESC_DELUXE,
              'N',
              IN_CATALOG_STATUS_PREMIUM,
              IN_PRODUCT_NAME_PREMIUM,
              IN_PRODUCT_DESC_PREMIUM,
              'N',
              IN_ITEM_TYPE,
              sysdate,
              'SYS',
              sysdate,
              'SYS');

      END IF;

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_AZ_PRODUCT_MASTER;

PROCEDURE REMOVE_AZ_PRODUCT_ATTR
(
  IN_PRODUCT_ID                  IN AZ_PRODUCT_ATTRIBUTES.PRODUCT_ID%TYPE,
  IN_ATTRIBUTE_TYPE              IN AZ_PRODUCT_ATTRIBUTES.PRODUCT_ATTRIBUTE_TYPE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

BEGIN

  DELETE FROM AZ_PRODUCT_ATTRIBUTES
  WHERE PRODUCT_ID = IN_PRODUCT_ID
  AND PRODUCT_ATTRIBUTE_TYPE = IN_ATTRIBUTE_TYPE;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    -- end exception block

END REMOVE_AZ_PRODUCT_ATTR;

PROCEDURE INSERT_AZ_PRODUCT_ATTRIBUTES
(
  IN_PRODUCT_ID                  IN AZ_PRODUCT_ATTRIBUTES.PRODUCT_ID%TYPE,
  IN_ATTRIBUTE_TYPE              IN AZ_PRODUCT_ATTRIBUTES.PRODUCT_ATTRIBUTE_TYPE%TYPE,
  IN_ATTRIBUTE_VALUE             IN AZ_PRODUCT_ATTRIBUTES.PRODUCT_ATTRIBUTE_VALUE%TYPE,
  IN_SEQUENCE                    IN AZ_PRODUCT_ATTRIBUTES.PRODUCT_ATTRIBUTE_SEQUENCE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

BEGIN

  INSERT INTO AZ_PRODUCT_ATTRIBUTES (
      PRODUCT_ID,
      PRODUCT_ATTRIBUTE_TYPE,
      PRODUCT_ATTRIBUTE_VALUE,
      PRODUCT_ATTRIBUTE_SEQUENCE,
      CREATED_ON,
      CREATED_BY,
      UPDATED_ON,
      UPDATED_BY
  ) VALUES (
      IN_PRODUCT_ID,
      IN_ATTRIBUTE_TYPE,
      IN_ATTRIBUTE_VALUE,
      IN_SEQUENCE,
      sysdate,
      'SYS',
      sysdate,
      'SYS');

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    -- end exception block

END INSERT_AZ_PRODUCT_ATTRIBUTES;


PROCEDURE SAVE_IMAGE_FEED_STATUS 
(
IN_AZ_IMAGE_FEED_ID             IN AZ_IMAGE_FEED.AZ_IMAGE_FEED_ID%TYPE,
IN_PRODUCT_ID			IN AZ_IMAGE_FEED.PRODUCT_ID%TYPE,
IN_FEED_STATUS			IN AZ_IMAGE_FEED.FEED_STATUS%TYPE,
IN_SERVER			IN AZ_IMAGE_FEED.SERVER%TYPE,
IN_FILENAME			IN AZ_IMAGE_FEED.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID		IN AZ_IMAGE_FEED.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY		IN AZ_IMAGE_FEED.CREATED_BY%TYPE,
IN_UPDATED_BY		IN AZ_IMAGE_FEED.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the feed status for a given az_image_feed_id

Input:
                az_image_feed_id            number
		product_id 	            varchar2
		feed_status		    varchar2
		server			    varchar2
		filename		    varchar2
		az_transaction_id	    varchar2
		created_by		    date
		updated_by 		    date
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  AZ_IMAGE_FEED
SET     PRODUCT_ID	  = IN_PRODUCT_ID,
        FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
	UPDATED_BY	  = IN_UPDATED_BY
WHERE   AZ_IMAGE_FEED_ID  = IN_AZ_IMAGE_FEED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_IMAGE_FEED_STATUS;

PROCEDURE UPDATE_IMG_FEED_SUBM_STATUS  
(
IN_FEED_STATUS			IN AZ_IMAGE_FEED.FEED_STATUS%TYPE,
IN_SERVER			IN AZ_IMAGE_FEED.SERVER%TYPE,
IN_AZ_TRANSACTION_ID		IN AZ_IMAGE_FEED.AZ_TRANSACTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the feed submission status for a given az_transaction_id

Input:
		feed_status						varchar2
		server							varchar2
		az_transaction_id					varchar2
Output:
		
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  AZ_IMAGE_FEED
SET     FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER
WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_IMG_FEED_SUBM_STATUS; 

PROCEDURE SAVE_INVENTORY_FEED_STATUS
(
IN_AZ_INVENTORY_FEED_ID     IN AZ_INVENTORY_FEED.AZ_INVENTORY_FEED_ID%TYPE,
IN_PRODUCT_ID			IN AZ_INVENTORY_FEED.PRODUCT_ID%TYPE,
IN_FEED_STATUS			IN AZ_INVENTORY_FEED.FEED_STATUS%TYPE,
IN_SERVER				IN AZ_INVENTORY_FEED.SERVER%TYPE,
IN_FILENAME				IN AZ_INVENTORY_FEED.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID	IN AZ_INVENTORY_FEED.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY			IN AZ_INVENTORY_FEED.CREATED_BY%TYPE,
IN_UPDATED_BY			IN AZ_INVENTORY_FEED.UPDATED_BY%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the feed status for a given AZ_INVENTORY_FEED_ID

Input:
        AZ_INVENTORY_FEED_ID            number
		product_id 	            varchar2
		feed_status		    varchar2
		server			    varchar2
		filename		    varchar2
		az_transaction_id	varchar2
		created_by		    date
		updated_by 		    date
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  AZ_INVENTORY_FEED
SET     PRODUCT_ID	  	  = IN_PRODUCT_ID,
        FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
        UPDATED_BY	  = IN_UPDATED_BY,
        UPDATED_ON = sysdate
WHERE   AZ_INVENTORY_FEED_ID  = IN_AZ_INVENTORY_FEED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_INVENTORY_FEED_STATUS;

PROCEDURE UPDATE_FEED_SUBM_STATUS  
(
IN_FEED_TYPE			IN VARCHAR2,
IN_FEED_STATUS			IN VARCHAR2,
IN_SERVER				IN VARCHAR2,
IN_AZ_TRANSACTION_ID	IN VARCHAR2,
IN_UPDATED_BY			IN VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the feed submission status for a given az_transaction_id

Input:
		feed_status						varchar2
		server							varchar2
		az_transaction_id					varchar2
Output:
		
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  CASE IN_FEED_TYPE
        WHEN 'INVENTORY_FEED' THEN
          UPDATE  AZ_INVENTORY_FEED
          SET     FEED_STATUS 	  = IN_FEED_STATUS, SERVER	= IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
        WHEN 'IMAGE_FEED' THEN
          UPDATE  AZ_IMAGE_FEED
          SET     FEED_STATUS 	  = IN_FEED_STATUS,  SERVER	 = IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
       WHEN 'PRODUCT_FEED' THEN
          UPDATE  AZ_PRODUCT_FEED
          SET     FEED_STATUS 	  = IN_FEED_STATUS,  SERVER	 = IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
        WHEN 'PRICE_FEED' THEN
          UPDATE  AZ_PRICE_FEED
          SET     FEED_STATUS 	  = IN_FEED_STATUS,  SERVER	 = IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
		WHEN 'ORDER_ACK_FEED' THEN
          UPDATE  AZ_ORDER_ACKNOWLEDGEMENT
          SET     FEED_STATUS 	  = IN_FEED_STATUS,  SERVER	 = IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
		WHEN 'ORDER_ADJ_FEED' THEN
          UPDATE  AZ_ORDER_ADJUSTMENT
          SET     FEED_STATUS 	  = IN_FEED_STATUS,  SERVER	 = IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
         WHEN 'ORDER_FULFILL_FEED' THEN
          UPDATE  AZ_ORDER_FULFILLMENT
          SET     FEED_STATUS 	  = IN_FEED_STATUS,  SERVER	 = IN_SERVER, UPDATED_ON = sysdate, UPDATED_BY = IN_UPDATED_BY
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;    
  END CASE;
  
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FEED_SUBM_STATUS; 


PROCEDURE SAVE_PRODUCT_FEED_STATUS
(
IN_AZ_PRODUCT_FEED_ID     IN AZ_PRODUCT_FEED.AZ_PRODUCT_FEED_ID%TYPE,
IN_PRODUCT_ID			IN AZ_PRODUCT_FEED.PRODUCT_ID%TYPE,
IN_FEED_STATUS			IN AZ_PRODUCT_FEED.FEED_STATUS%TYPE,
IN_SERVER				IN AZ_PRODUCT_FEED.SERVER%TYPE,
IN_FILENAME				IN AZ_PRODUCT_FEED.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID	IN AZ_PRODUCT_FEED.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY			IN AZ_PRODUCT_FEED.CREATED_BY%TYPE,
IN_UPDATED_BY			IN AZ_PRODUCT_FEED.UPDATED_BY%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS
BEGIN

UPDATE  AZ_PRODUCT_FEED
SET     PRODUCT_ID	  	  = IN_PRODUCT_ID,
        FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
        UPDATED_BY	  = IN_UPDATED_BY,
        UPDATED_ON = sysdate
WHERE   AZ_PRODUCT_FEED_ID  = IN_AZ_PRODUCT_FEED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_PRODUCT_FEED_STATUS;

PROCEDURE UPDATE_PROD_FEED_SUBM_STATUS  
(
IN_PRODUCT_ID			IN VARCHAR2,
IN_STANDARD_FLAG			IN VARCHAR2,
IN_DELUXE_FLAG			IN VARCHAR2,
IN_PREMIUM_FLAG		IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
BEGIN


UPDATE  AZ_PRODUCT_MASTER
SET     SENT_TO_AMAZON_STANDARD_FLAG	  	  = IN_STANDARD_FLAG,
        SENT_TO_AMAZON_DELUXE_FLAG 	  = IN_DELUXE_FLAG,
        SENT_TO_AMAZON_PREMIUM_FLAG	    	  = IN_PREMIUM_FLAG,
        UPDATED_BY	  = '_SYSTEM_',
        UPDATED_ON = sysdate
WHERE   PRODUCT_ID	  	  = IN_PRODUCT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_PROD_FEED_SUBM_STATUS;

PROCEDURE SAVE_PRICE_FEED_STATUS 
(
IN_AZ_PRICE_FEED_ID             IN AZ_PRICE_FEED.AZ_PRICE_FEED_ID%TYPE,
IN_PRODUCT_ID			IN AZ_PRICE_FEED.PRODUCT_ID%TYPE,
IN_FEED_STATUS			IN AZ_PRICE_FEED.FEED_STATUS%TYPE,
IN_SERVER			IN AZ_PRICE_FEED.SERVER%TYPE,
IN_FILENAME			IN AZ_PRICE_FEED.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID		IN AZ_PRICE_FEED.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY		IN AZ_PRICE_FEED.CREATED_BY%TYPE,
IN_UPDATED_BY		IN AZ_PRICE_FEED.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the feed status for a given az_price_feed_id

Input:
                az_price_feed_id            number
		product_id 	            varchar2
		feed_status		    varchar2
		server			    varchar2
		filename		    varchar2
		az_transaction_id	    varchar2
		created_by		    date
		updated_by 		    date
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  AZ_PRICE_FEED
SET     PRODUCT_ID	  = IN_PRODUCT_ID,
        FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
	UPDATED_BY	  = IN_UPDATED_BY
WHERE   AZ_PRICE_FEED_ID  = IN_AZ_PRICE_FEED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_PRICE_FEED_STATUS;


PROCEDURE SAVE_ORDER_ACK_FEED_STATUS 
(
IN_AZ_ORDER_ACK_FEED_ID      IN AZ_ORDER_ACKNOWLEDGEMENT.AZ_ORDER_ACKNOWLEDGEMENT_ID%TYPE,
IN_FEED_STATUS			         IN AZ_ORDER_ACKNOWLEDGEMENT.FEED_STATUS%TYPE,
IN_SERVER			                IN AZ_ORDER_ACKNOWLEDGEMENT.SERVER%TYPE,
IN_FILENAME			              IN AZ_ORDER_ACKNOWLEDGEMENT.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID		      IN AZ_ORDER_ACKNOWLEDGEMENT.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY		              IN AZ_ORDER_ACKNOWLEDGEMENT.CREATED_BY%TYPE,
IN_UPDATED_BY		              IN AZ_ORDER_ACKNOWLEDGEMENT.UPDATED_BY%TYPE,
OUT_STATUS                    OUT VARCHAR2,
OUT_MESSAGE                   OUT VARCHAR2
)
AS
BEGIN

UPDATE  AZ_ORDER_ACKNOWLEDGEMENT
SET     FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
		UPDATED_BY	  = IN_UPDATED_BY
WHERE   AZ_ORDER_ACKNOWLEDGEMENT_ID  = IN_AZ_ORDER_ACK_FEED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_ORDER_ACK_FEED_STATUS;

PROCEDURE SAVE_ORDER_ADJ_FEED_STATUS 
(
IN_AZ_ORDER_ADJ_FEED_ID      IN AZ_ORDER_ADJUSTMENT.AZ_ORDER_ADJUSTMENT_ID%TYPE,
IN_FEED_STATUS			     IN AZ_ORDER_ADJUSTMENT.FEED_STATUS%TYPE,
IN_SERVER			         IN AZ_ORDER_ADJUSTMENT.SERVER%TYPE,
IN_FILENAME			         IN AZ_ORDER_ADJUSTMENT.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID		 IN AZ_ORDER_ADJUSTMENT.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY		         IN AZ_ORDER_ADJUSTMENT.CREATED_BY%TYPE,
IN_UPDATED_BY		         IN AZ_ORDER_ADJUSTMENT.UPDATED_BY%TYPE,
OUT_STATUS                   OUT VARCHAR2,
OUT_MESSAGE                  OUT VARCHAR2
)
AS
BEGIN

UPDATE  AZ_ORDER_ADJUSTMENT
SET     FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
		UPDATED_BY	  = IN_UPDATED_BY
WHERE   AZ_ORDER_ADJUSTMENT_ID  = IN_AZ_ORDER_ADJ_FEED_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_ORDER_ADJ_FEED_STATUS;

PROCEDURE SAVE_FULFILLMENT_FEED_STATUS 
(
IN_AZ_ORDER_FULFILLMENT_ID      IN AZ_ORDER_FULFILLMENT.AZ_ORDER_FULFILLMENT_ID%TYPE,
IN_FEED_STATUS			IN AZ_ORDER_FULFILLMENT.FEED_STATUS%TYPE,
IN_SERVER			IN AZ_ORDER_FULFILLMENT.SERVER%TYPE,
IN_FILENAME			IN AZ_ORDER_FULFILLMENT.FILENAME%TYPE,
IN_AZ_TRANSACTION_ID		IN AZ_ORDER_FULFILLMENT.AZ_TRANSACTION_ID%TYPE,
IN_CREATED_BY		        IN AZ_ORDER_FULFILLMENT.CREATED_BY%TYPE,
IN_UPDATED_BY		        IN AZ_ORDER_FULFILLMENT.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the feed status for a given az_order_fulfillment_id

Input:
                az_order_fulfillment_id     number
		feed_status		    varchar2
		server			    varchar2
		filename		    varchar2
		az_transaction_id	    varchar2
		created_by		    date
		updated_by 		    date
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  AZ_ORDER_FULFILLMENT
SET     FEED_STATUS 	  = IN_FEED_STATUS,
        SERVER	    	  = IN_SERVER,
        FILENAME    	  = IN_FILENAME,
        AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID,
        CREATED_BY	  = IN_CREATED_BY,
	UPDATED_BY	  = IN_UPDATED_BY
WHERE   AZ_ORDER_FULFILLMENT_ID  = IN_AZ_ORDER_FULFILLMENT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_FULFILLMENT_FEED_STATUS;

PROCEDURE INSERT_AMAZON_ACKNOWLEDGEMENT
(
IN_CONFIRMATION_NUMBER           IN VARCHAR2,
IN_STATUS                        IN VARCHAR2,
IN_CANCEL_REASON                 IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR exists_cur IS
        SELECT  az_order_item_number
        FROM    az_order_detail
        WHERE   confirmation_number = in_confirmation_number;

/*
Only insert a record if one doesn't exist for this item
or if the new status is 'Failure' and the existing status is 'Success'
*/
CURSOR duplicate_cur(in_az_order_item_number varchar2) IS
        SELECT az_order_acknowledgement_id
        FROM az_order_acknowledgement
        WHERE az_order_item_number = in_az_order_item_number
--        AND (status_code = 'Failure' OR status_code = IN_STATUS);
        AND NOT(status_code = 'Success' AND IN_STATUS = 'Failure');

v_az_order_item_number          az_order_detail.az_order_item_number%type;
v_az_order_acknowledgement_id   az_order_acknowledgement.az_order_acknowledgement_id%type;


BEGIN

    OPEN exists_cur;
    FETCH exists_cur INTO v_az_order_item_number;
    CLOSE exists_cur;

    IF v_az_order_item_number IS NOT NULL THEN

        OPEN duplicate_cur(v_az_order_item_number);
        FETCH duplicate_cur into v_az_order_acknowledgement_id;
        IF duplicate_cur%notfound then
            INSERT INTO AZ_ORDER_ACKNOWLEDGEMENT (
                AZ_ORDER_ACKNOWLEDGEMENT_ID,
                AZ_ORDER_ITEM_NUMBER,
                STATUS_CODE,
                CANCEL_REASON,
                FEED_STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY
            ) VALUES (
                AZ_ORDER_ACKNOWLEDGEMENT_ID_SQ.nextval,
                v_az_order_item_number,
                IN_STATUS,
                IN_CANCEL_REASON,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS'
            );
        END IF;

        OUT_STATUS := 'Y';

    ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'AZ_ORDER_DETAIL record not found for confirmation number ' || in_confirmation_number;
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_AMAZON_ACKNOWLEDGEMENT;

PROCEDURE SAVE_FTD_ORDER_XML 
(
IN_AZ_ORDER_NUMBER          IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
IN_FTD_XML					IN AZ_ORDER.FTD_XML%TYPE,
IN_ORDER_STATUS				IN AZ_ORDER.ORDER_STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for saving/updating FTD_XML and ORDER_STATUS to AZ_ORDER table

Input:
		AZ_ORDER_NUMBER 	            varchar2
		FTD_XML		    clob
		order_status	varchar2
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  PTN_AMAZON.AZ_ORDER
SET     FTD_XML	  = IN_FTD_XML,
		ORDER_STATUS = IN_ORDER_STATUS,
		UPDATED_BY	  = 'SYS',
		UPDATED_ON = sysdate
WHERE   AZ_ORDER_NUMBER  = IN_AZ_ORDER_NUMBER;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_FTD_ORDER_XML;

PROCEDURE SAVE_AZ_ORDER_DETAILS 
(
IN_AZ_ORDER_ITEM_NUMBER         IN AZ_ORDER_DETAIL.AZ_ORDER_ITEM_NUMBER%TYPE,
IN_AZ_ORDER_NUMBER		IN AZ_ORDER_DETAIL.AZ_ORDER_NUMBER%TYPE,
IN_CONFIRMATION_NUMBER		IN AZ_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
IN_AMAZON_PRODUCT_ID            IN AZ_ORDER_DETAIL.AMAZON_PRODUCT_ID%TYPE,
IN_PRINCIPAL_AMT                IN AZ_ORDER_DETAIL.PRINCIPAL_AMT%TYPE,
IN_SHIPPING_AMT                 IN AZ_ORDER_DETAIL.SHIPPING_AMT%TYPE,
IN_TAX_AMT                      IN AZ_ORDER_DETAIL.TAX_AMT%TYPE,
IN_SHIPPING_TAX_AMT             IN AZ_ORDER_DETAIL.SHIPPING_TAX_AMT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for saving/inserting order details to AZ_ORDER_DETAIL table

Input:
		AZ_ORDER_ITEM_NUMBER 	            varchar2
		AZ_ORDER_NUMBER 	            varchar2
		CONFIRMATION_NUMBER 	            varchar2
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO PTN_AMAZON.AZ_ORDER_DETAIL (
      AZ_ORDER_ITEM_NUMBER,
      AZ_ORDER_NUMBER,
      CONFIRMATION_NUMBER,
      AMAZON_PRODUCT_ID,
      CREATED_ON,
      CREATED_BY,
      UPDATED_ON,
      UPDATED_BY,
      PRINCIPAL_AMT,
      SHIPPING_AMT,
      TAX_AMT,
      SHIPPING_TAX_AMT
    ) VALUES (
      IN_AZ_ORDER_ITEM_NUMBER,
      IN_AZ_ORDER_NUMBER,
      IN_CONFIRMATION_NUMBER,
      IN_AMAZON_PRODUCT_ID,
      SYSDATE,
      'SYS',
      SYSDATE,
      'SYS',
      IN_PRINCIPAL_AMT,
      IN_SHIPPING_AMT,
      IN_TAX_AMT,
      IN_SHIPPING_TAX_AMT
    );


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_AZ_ORDER_DETAILS;

PROCEDURE UPDATE_AZ_ORDER_STATUS 
(
IN_AZ_ORDER_NUMBER          IN AZ_ORDER.AZ_ORDER_NUMBER%TYPE,
IN_ORDER_STATUS				IN AZ_ORDER.ORDER_STATUS%TYPE,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible updating ORDER_STATUS in AZ_ORDER table

Input:
		AZ_ORDER_NUMBER 	            varchar2
		order_status	varchar2
Output:
		
                status                       varchar2 (Y or N)
                message                      varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  PTN_AMAZON.AZ_ORDER
SET     ORDER_STATUS = IN_ORDER_STATUS,
		UPDATED_BY	  = 'SYS',
		UPDATED_ON = sysdate
WHERE   AZ_ORDER_NUMBER  = IN_AZ_ORDER_NUMBER;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_AZ_ORDER_STATUS;

PROCEDURE INSERT_AZ_SETTLEMENT_REPORT
(
  IN_AZ_REPORT_ID        		IN AZ_SETTLEMENT_REPORT.AZ_REPORT_ID%TYPE,
  IN_REPORT                     IN AZ_SETTLEMENT_REPORT.REPORT%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

/*-----------------------------------------------------------------------------
    This procedure will insert settlement report into the AZ_SETTLEMENT_REPORT table.

Input:
        IN_AZ_REPORT_ID          				NUMBER
        IN_REPORT                       		CLOB
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
    INSERT INTO AZ_SETTLEMENT_REPORT (
      AZ_SETTLEMENT_REPORT_ID,
      AZ_REPORT_ID,
      REPORT,
      CREATED_ON,
      CREATED_BY,
	  UPDATED_ON,
	  UPDATED_BY
    ) VALUES (
      PTN_AMAZON.AZ_SETTLEMENT_REPORT_ID_SQ.nextval,
      IN_AZ_REPORT_ID,
      IN_REPORT,
      sysdate,
      'SYS',
	  sysdate,
	  'SYS'
    );

    OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AZ_SETTLEMENT_REPORT;

PROCEDURE MARK_SETTLEMENT_PROCESSED
(
  IN_SETTLEMENT_ID              IN SETTLEMENT_DATA.SETTLEMENT_DATA_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

BEGIN

    UPDATE SETTLEMENT_DATA
    SET PROCESSED_DATE = sysdate,
    UPDATED_ON = sysdate,
    UPDATED_BY = 'SYS'
    WHERE SETTLEMENT_DATA_ID = IN_SETTLEMENT_ID;

    OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END MARK_SETTLEMENT_PROCESSED;

PROCEDURE INSERT_AMAZON_ADJUSTMENT 
(
  IN_CONFIRMATION_NUMBER IN VARCHAR2  
, IN_ADJUSTMENT_REASON IN VARCHAR2  
, IN_PRINCIPAL_AMT IN NUMBER  
, IN_SHIPPING_AMT IN NUMBER  
, IN_TAX_AMT IN NUMBER  
, IN_SHIPPING_TAX_AMT IN NUMBER
, IN_CREATED_BY VARCHAR2
, IN_UPDATED_BY VARCHAR2
, OUT_STATUS OUT VARCHAR2  
, OUT_MESSAGE OUT VARCHAR2  
) AS 
  
CURSOR exists_cur IS
  SELECT  az_order_item_number
  FROM    az_order_detail
  WHERE   confirmation_number = in_confirmation_number;
  
v_az_order_item_number          az_order_detail.az_order_item_number%type;     

BEGIN

    OPEN exists_cur;
    FETCH exists_cur INTO v_az_order_item_number;
    CLOSE exists_cur;

    IF v_az_order_item_number IS NOT NULL THEN       
        INSERT INTO AZ_ORDER_ADJUSTMENT (
            AZ_ORDER_ADJUSTMENT_ID,
            AZ_ORDER_ITEM_NUMBER,
            ADJUSTMENT_REASON,
            PRINCIPAL_AMT,
            SHIPPING_AMT,
            TAX_AMT,
            SHIPPING_TAX_AMT,
            FEED_STATUS,
            CREATED_BY,
            CREATED_ON,
            UPDATED_BY,
            UPDATED_ON
        ) VALUES (
                AZ_ORDER_ACKNOWLEDGEMENT_ID_SQ.nextval,
                v_az_order_item_number,
                IN_ADJUSTMENT_REASON,
                IN_PRINCIPAL_AMT,
                IN_SHIPPING_AMT,
                IN_TAX_AMT,
                IN_SHIPPING_TAX_AMT,
                'NEW',
                IN_CREATED_BY,
                sysdate,
                IN_UPDATED_BY,       
                sysdate
        );
        OUT_STATUS := 'Y';

    ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'AZ_ORDER_DETAIL record not found for confirmation number ' || in_confirmation_number;
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END INSERT_AMAZON_ADJUSTMENT;

PROCEDURE INSERT_PRICE_VARIANCE
(
  IN_CONFIRMATION_NUMBER        IN AZ_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  IN_FTD_PRODUCT_AMT            IN AZ_PRICE_VARIANCE.FTD_PRODUCT_AMT%TYPE,
  IN_FTD_SHIPPING_FEE_AMT       IN AZ_PRICE_VARIANCE.FTD_SHIPPING_FEE_AMT%TYPE,
  IN_FTD_TAX_AMT                IN AZ_PRICE_VARIANCE.FTD_TAX_AMT%TYPE,
  IN_FTD_SHIPPING_TAX_AMT       IN AZ_PRICE_VARIANCE.FTD_SHIPPING_TAX_AMT%TYPE,
  IN_AZ_PRODUCT_AMT             IN AZ_PRICE_VARIANCE.AZ_PRODUCT_AMT%TYPE,
  IN_AZ_SHIPPING_FEE_AMT        IN AZ_PRICE_VARIANCE.AZ_SHIPPING_FEE_AMT%TYPE,
  IN_AZ_TAX_AMT               	IN AZ_PRICE_VARIANCE.AZ_TAX_AMT%TYPE,
  IN_AZ_SHIPPING_TAX_AMT      	IN AZ_PRICE_VARIANCE.AZ_SHIPPING_TAX_AMT%TYPE,
  IN_CREATED_BY         		IN AZ_PRICE_VARIANCE.CREATED_BY%TYPE,
  IN_UPDATED_BY         		IN AZ_PRICE_VARIANCE.UPDATED_BY%TYPE,  
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
)
AS

az_order_item_number varchar2(32);

BEGIN

DECLARE CURSOR detail_cur IS
  SELECT  AZ_ORDER_ITEM_NUMBER
    FROM    PTN_AMAZON.AZ_ORDER_DETAIL
    WHERE   CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

BEGIN
  OPEN detail_cur;
  FETCH detail_cur INTO az_order_item_number;
  IF detail_cur%NOTFOUND THEN
    RAISE NO_DATA_FOUND;
  END IF;
  CLOSE detail_cur;
END;

INSERT
INTO
    PTN_AMAZON.AZ_PRICE_VARIANCE
    (
        AZ_ORDER_ITEM_NUMBER,
        FTD_PRODUCT_AMT,
        FTD_SHIPPING_FEE_AMT,
        FTD_TAX_AMT,
        FTD_SHIPPING_TAX_AMT,
        AZ_PRODUCT_AMT,
        AZ_SHIPPING_FEE_AMT,
        AZ_TAX_AMT,
        AZ_SHIPPING_TAX_AMT,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY
    )
    VALUES
    (
        az_order_item_number,
        IN_FTD_PRODUCT_AMT,
        IN_FTD_SHIPPING_FEE_AMT,
        IN_FTD_TAX_AMT,
        IN_FTD_SHIPPING_TAX_AMT,
        IN_AZ_PRODUCT_AMT,
        IN_AZ_SHIPPING_FEE_AMT,
        IN_AZ_TAX_AMT,
        IN_AZ_SHIPPING_TAX_AMT,
        SYSDATE,
        IN_CREATED_BY,
        SYSDATE,
        IN_UPDATED_BY
    );

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Unable to locate order detail for confirmation number ' || IN_CONFIRMATION_NUMBER;
END;    -- end exception block

WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'Y';
        OUT_MESSAGE := 'Variance already in database for ' || IN_CONFIRMATION_NUMBER;
END;    -- end exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END INSERT_PRICE_VARIANCE;

PROCEDURE INSERT_SETTLEMENT_DATA
(
  IN_SETTLEMENT_DATA_ID         IN  SETTLEMENT_DATA.SETTLEMENT_DATA_ID%TYPE,  
  IN_TOTAL_AMT                  IN  SETTLEMENT_DATA.TOTAL_AMT%TYPE,
  IN_START_DATE                 IN  SETTLEMENT_DATA.START_DATE%TYPE,
  IN_END_DATE                   IN  SETTLEMENT_DATA.END_DATE%TYPE,
  IN_DEPOSIT_DATE               IN  SETTLEMENT_DATA.DEPOSIT_DATE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

BEGIN

    INSERT INTO SETTLEMENT_DATA (
        SETTLEMENT_DATA_ID,
        TOTAL_AMT,
        START_DATE,
        END_DATE,
        DEPOSIT_DATE,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY
    ) VALUES (
        IN_SETTLEMENT_DATA_ID,
        IN_TOTAL_AMT,
        IN_START_DATE,
        IN_END_DATE,
        IN_DEPOSIT_DATE,
        SYSDATE,
        'SYS',
        SYSDATE,
        'SYS'
    );

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END INSERT_SETTLEMENT_DATA;

PROCEDURE INSERT_SETTLEMENT_DETAIL
(
  IN_SETTLEMENT_DATA_ID         IN  SETTLEMENT_DETAIL.SETTLEMENT_DATA_ID%TYPE,
  IN_AZ_ORDER_NUMBER            IN  SETTLEMENT_DETAIL.AZ_ORDER_NUMBER%TYPE,
  IN_AZ_ORDER_ITEM_NUMBER       IN  SETTLEMENT_DETAIL.AZ_ORDER_ITEM_NUMBER%TYPE,
  IN_TRANSACTION_TYPE           IN  SETTLEMENT_DETAIL.TRANSACTION_TYPE%TYPE,
  IN_TRANSACTION_NAME           IN  SETTLEMENT_DETAIL.TRANSACTION_NAME%TYPE,
  IN_TRANSACTION_AMT            IN  SETTLEMENT_DETAIL.TRANSACTION_AMT%TYPE,
  IN_POSTED_DATE                IN  SETTLEMENT_DETAIL.POSTED_DATE%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

BEGIN

    INSERT INTO SETTLEMENT_DETAIL (
        SETTLEMENT_DETAIL_ID,
        SETTLEMENT_DATA_ID,
        AZ_ORDER_NUMBER,
        AZ_ORDER_ITEM_NUMBER,
        TRANSACTION_TYPE,
        TRANSACTION_NAME,
        TRANSACTION_AMT,
        POSTED_DATE,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY
    ) VALUES (
        SETTLEMENT_DETAIL_ID_SQ.NEXTVAL,
        IN_SETTLEMENT_DATA_ID,
        IN_AZ_ORDER_NUMBER,
        IN_AZ_ORDER_ITEM_NUMBER,
        IN_TRANSACTION_TYPE,
        IN_TRANSACTION_NAME,
        IN_TRANSACTION_AMT,
        NVL(IN_POSTED_DATE, sysdate),
        SYSDATE,
        'SYS',
        SYSDATE,
        'SYS'
    );

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END INSERT_SETTLEMENT_DETAIL;

/*------------------------------------------------------------------------------
  This stored procedure provides the buyer sequence based on the sequence date.
  If the Sequence date global param has the current date, it will increment the 
  sequence value by 1 and returns. If the sequence date is not the current date,
  it resets the current sequence number and updates it to 1 in 
  global_parms table (BUYER_SEQUENCE value).
------------------------------------------------------------------------------*/
PROCEDURE GET_AMAZON_BUYER_SEQUENCE
(  
  OUT_BUYER_SEQUENCE            OUT VARCHAR,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

CURSOR seq_date_cur IS
  SELECT VALUE 
  FROM FRP.GLOBAL_PARMS 
  WHERE CONTEXT='AMAZON_CONFIG' AND NAME='BUYER_SEQUENCE_DATE';
  
CURSOR seq_cur IS
	SELECT VALUE 
	FROM FRP.GLOBAL_PARMS
	WHERE CONTEXT='AMAZON_CONFIG' AND NAME = 'BUYER_SEQUENCE';
	
  v_sequence_date FRP.global_parms.value%type;
  v_sequence FRP.global_parms.value%type;	
  v_temp_seq NUMBER;
  
BEGIN

  OPEN seq_date_cur;
  FETCH seq_date_cur INTO v_sequence_date;
  CLOSE seq_date_cur;
  
	OPEN 	seq_cur;
	FETCH seq_cur INTO v_sequence;
	CLOSE seq_cur;
  
  IF to_date(v_sequence_date,'DD-MON-YYYY') = to_date(SYSDATE, 'DD-MON-YYYY') THEN	
    v_temp_seq := TO_NUMBER(v_sequence);
    v_temp_seq := v_temp_seq + 1;
    UPDATE  FRP.GLOBAL_PARMS SET VALUE = TO_CHAR(v_temp_seq) WHERE CONTEXT='AMAZON_CONFIG' AND NAME='BUYER_SEQUENCE';
    OUT_STATUS := 'Y1';
    OUT_BUYER_SEQUENCE := TO_CHAR(v_temp_seq);
	
  ELSE
    UPDATE  FRP.GLOBAL_PARMS SET VALUE = TO_CHAR(1) WHERE CONTEXT='AMAZON_CONFIG' AND NAME='BUYER_SEQUENCE';
    UPDATE  FRP.GLOBAL_PARMS SET VALUE = SYSDATE WHERE CONTEXT='AMAZON_CONFIG' AND NAME='BUYER_SEQUENCE_DATE';
    OUT_STATUS := 'Y2';
    OUT_BUYER_SEQUENCE := TO_CHAR(1);
  END IF;
  
  EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END GET_AMAZON_BUYER_SEQUENCE;

PROCEDURE INSERT_FLORIST_FULFILL_ORDERS
( 
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting all the florist delivered orders for today that is still live
Output:
		
                out_status                       varchar2 (Y or N)
                out_message                      varchar2 (error message)
-----------------------------------------------------------------------------*/
CURSOR GET_ITEM_NUMBERS IS
SELECT  aod.az_order_item_number
    FROM ptn_amazon.az_order_detail aod 
    JOIN    clean.order_details od ON aod.confirmation_number = od.external_order_number                     
    JOIN mercury.mercury m on to_char(od.order_detail_id) = m.reference_number and m.mercury_status = 'MC' 
    and m.msg_type = 'FTD' and (m.delivery_date) <= trunc(sysdate)        
    and not exists (SELECT 'Y' FROM mercury.mercury m2             
        where m2.mercury_order_number = m.mercury_order_number
        and m2.msg_type in ('CAN', 'REJ')
        and m2.mercury_status = 'MC')
----To avoid insertion of duplicate orders
    and not exists (SELECT az_order_item_number FROM ptn_amazon.az_order_fulfillment aof
        where aof.az_order_item_number = aod.az_order_item_number);

		BEGIN

FOR rec in GET_ITEM_NUMBERS LOOP
    insert into ptn_amazon.az_order_fulfillment
        (az_order_fulfillment_id,
         az_order_item_number,
         fulfillment_date,
         shipping_method,
         carrier_name,
         tracking_number,
         feed_status,
         created_on,
         created_by,
         updated_on,
         updated_by) 
    values(
        ptn_amazon.az_order_fulfillment_id_sq.nextval,
        rec.az_order_item_number,
        trunc(sysdate),
        'SD',
        'Florist',
         null,
        'NEW',
        sysdate,
        'SYS',
        sysdate,
        'SYS');

   END LOOP;
   
    OUT_STATUS := 'Y';
		
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;

END INSERT_FLORIST_FULFILL_ORDERS;

PROCEDURE INSERT_DROPSHIP_CONFIRMATION
(
  IN_VENUS_ORDER_NUM            IN VENUS.VENUS.VENUS_ORDER_NUMBER%TYPE,
  IN_SHIP_DATE			IN PTN_AMAZON.AZ_ORDER_FULFILLMENT.FULFILLMENT_DATE%TYPE,
  IN_SHIPPING_METHOD		IN PTN_AMAZON.AZ_ORDER_FULFILLMENT.SHIPPING_METHOD%TYPE,
  IN_CARRIER_NAME		IN PTN_AMAZON.AZ_ORDER_FULFILLMENT.CARRIER_NAME%TYPE,
  IN_TRACKING_NUMBER		IN PTN_AMAZON.AZ_ORDER_FULFILLMENT.TRACKING_NUMBER%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------

 Name:    INSERT_DROPSHIP_CONFIRMATION
 Type:    Procedure
 Syntax:  INSERT_DROPSHIP_CONFIRMATIONs(venus_order_number, ship_date, shipping_method,carrier_name,tracking_number)

Description:
        This stored procedure inserts a record into table ptn_amazon.az_order_fulfillment

Input:
	venus_order_number varchar2,
	ship_date	date,
	shipping_method	shipping_method
	carrier_name	carrier_name
	tracking_number	tracking_number

Output:
        status              varchar2
        error message       varchar2
-----------------------------------------------------------------------------*/

CURSOR GET_ITEM_NUMBERS IS
    SELECT distinct aod.az_order_item_number
    FROM venus.venus v
    JOIN clean.order_details od ON od.order_detail_id = TO_NUMBER(v.REFERENCE_NUMBER)
    JOIN ptn_amazon.az_order_detail aod ON aod.confirmation_number = od.external_order_number
    WHERE v.venus_order_number = IN_VENUS_ORDER_NUM
    AND v.msg_type = 'FTD'
    AND not exists (SELECT az_order_item_number FROM ptn_amazon.az_order_fulfillment aof
        where aof.az_order_item_number = aod.az_order_item_number);

BEGIN

FOR rec in GET_ITEM_NUMBERS LOOP
    insert into ptn_amazon.az_order_fulfillment
        (az_order_fulfillment_id,
         az_order_item_number,
         fulfillment_date,
         shipping_method,
         carrier_name,
         tracking_number,
         feed_status,
         created_on,
         created_by,
         updated_on,
         updated_by) 
    values(
        ptn_amazon.az_order_fulfillment_id_sq.nextval,
        rec.az_order_item_number,
        in_ship_date,
        in_shipping_method,
        in_carrier_name,
        in_tracking_number,
        'NEW',
        sysdate,
        'SYS',
        sysdate,
        'SYS');

   END LOOP;
OUT_STATUS := 'Y';
		
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;  
  
END INSERT_DROPSHIP_CONFIRMATION;

PROCEDURE PROCESS_REFUNDS
(
  IN_USER_NAME                  IN VARCHAR,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
) AS

outStatus varchar2(1);
outMessage varchar2(100);
order_count Integer;
item_count Integer;
fully_refund_flag varchar2(1);
shipment_feed_count Integer;

cursor refund_cur is
    SELECT r.refund_id,
        od.external_order_number,
        od.order_detail_id,
        r.refund_disp_code,
        r.refund_product_amount,
        r.refund_addon_amount,
        r.refund_service_fee,
        r.refund_shipping_fee,
        r.refund_service_fee_tax,
        r.refund_shipping_tax,
        r.refund_discount_amount,
        r.refund_commission_amount,
        r.refund_wholesale_amount,
        r.refund_tax,
        (select count(*) from clean.order_details cod2 where cod2.order_guid = od.order_guid) as item_count,
        (select count(*) from clean.refund r2 where r2.order_detail_id = od.order_detail_id) as refund_count
    FROM clean.refund r,
        clean.order_details od,
        clean.orders o
    WHERE o.origin_id = 'AMZNI'
    AND od.order_guid = o.order_guid
    AND od.order_disp_code in ('Processed','Shipped','Printed', 'Validated')
    AND r.order_detail_id = od.order_detail_id
    AND r.refund_status = 'Unbilled'
    AND TRUNC(r.created_on) <= TRUNC(sysdate);
             
cursor tax_cur(in_external_order_number varchar2) is
    SELECT aod.tax_amt amazon_tax_amt,
        aod.shipping_tax_amt amazon_shipping_tax_amt,
        nvl((select sum(refund_tax) from clean.refund r
            where r.order_detail_id = od.order_detail_id
            and r.refund_status in ('Billed', 'Settled')), 0) previous_tax
    FROM ptn_amazon.az_order_detail aod,
        clean.order_details od
    WHERE aod.confirmation_number = in_external_order_number
    AND od.external_order_number = aod.confirmation_number;

v_amazon_tax_amt             number;
v_amazon_shipping_tax_amt    number;
v_previous_tax               number;
v_tax_total                  number;
v_diff                       number;

BEGIN
  
  FOR refund_rec IN refund_cur loop

      OPEN tax_cur(refund_rec.external_order_number);
      FETCH tax_cur INTO v_amazon_tax_amt, v_amazon_shipping_tax_amt, v_previous_tax;
      CLOSE tax_cur;

      v_tax_total := refund_rec.refund_tax + v_previous_tax;
      IF (v_tax_total > v_amazon_tax_amt) THEN
          v_diff := v_tax_total - v_amazon_tax_amt;
          refund_rec.refund_tax := refund_rec.refund_tax - v_diff;
          refund_rec.refund_shipping_tax := v_diff;
      END IF;
          
      fully_refund_flag := clean.refund_pkg.is_order_fully_refunded(refund_rec.order_detail_id);
      --check if the order is fully refunded.
      if fully_refund_flag = 'Y' and refund_rec.refund_count = 1 then
          --check if the order has multiple items in the cart
          if refund_rec.item_count > 1 then
              DBMS_OUTPUT.PUT_LINE('Multi Item Cart Order, send Adj feed regardless of shipment feed');
              --call PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ADJUSTMENT
              PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ADJUSTMENT
              (
                  IN_CONFIRMATION_NUMBER => refund_rec.external_order_number
                  , IN_ADJUSTMENT_REASON => refund_rec.refund_disp_code
                  , IN_PRINCIPAL_AMT => refund_rec.refund_product_amount - refund_rec.refund_discount_amount
                  , IN_SHIPPING_AMT => refund_rec.refund_shipping_fee + refund_rec.refund_service_fee
                  , IN_TAX_AMT => refund_rec.refund_tax
                  , IN_SHIPPING_TAX_AMT => refund_rec.refund_service_fee_tax + refund_rec.refund_shipping_tax
                  , IN_CREATED_BY => IN_USER_NAME
                  , IN_UPDATED_BY => IN_USER_NAME
                  , OUT_STATUS => outStatus
                  , OUT_MESSAGE => outMessage
                );
          else
              select count(*) into shipment_feed_count from PTN_AMAZON.AZ_ORDER_DETAIL aod, PTN_AMAZON.AZ_ORDER_FULFILLMENT aof
              where aod.az_order_item_number = aof.az_order_item_number
              and aod.confirmation_number = refund_rec.external_order_number;
              --check if the shipment feed has already been sent to Amazon.
              if shipment_feed_count > 0 then
                  --call PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ADJUSTMENT
                  PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ADJUSTMENT
                  (
                  IN_CONFIRMATION_NUMBER => refund_rec.external_order_number
                  , IN_ADJUSTMENT_REASON => refund_rec.refund_disp_code
                  , IN_PRINCIPAL_AMT => refund_rec.refund_product_amount - refund_rec.refund_discount_amount
                  , IN_SHIPPING_AMT => refund_rec.refund_shipping_fee + refund_rec.refund_service_fee
                  , IN_TAX_AMT => refund_rec.refund_tax
                  , IN_SHIPPING_TAX_AMT => refund_rec.refund_service_fee_tax + refund_rec.refund_shipping_tax
                  , IN_CREATED_BY => IN_USER_NAME
                  , IN_UPDATED_BY => IN_USER_NAME
                  , OUT_STATUS => outStatus
                  , OUT_MESSAGE => outMessage
                  );
              else
                  --call PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ACKNOWLEDGEMENT
                  PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ACKNOWLEDGEMENT
                  (
                  IN_CONFIRMATION_NUMBER => refund_rec.external_order_number
                  ,IN_STATUS => 'Failure'
                  ,IN_CANCEL_REASON => refund_rec.refund_disp_code
                  ,OUT_STATUS => outstatus
                  ,OUT_MESSAGE => outmessage
                  );
              end if;
            
          end if;
      else
          --call PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ADJUSTMENT
          PTN_AMAZON.AMAZON_MAINT_PKG.INSERT_AMAZON_ADJUSTMENT
          (
              IN_CONFIRMATION_NUMBER => refund_rec.external_order_number
              , IN_ADJUSTMENT_REASON => refund_rec.refund_disp_code
              , IN_PRINCIPAL_AMT => refund_rec.refund_product_amount - refund_rec.refund_discount_amount
              , IN_SHIPPING_AMT => refund_rec.refund_shipping_fee + refund_rec.refund_service_fee
              , IN_TAX_AMT => refund_rec.refund_tax
              , IN_SHIPPING_TAX_AMT => refund_rec.refund_service_fee_tax + refund_rec.refund_shipping_tax
              , IN_CREATED_BY => IN_USER_NAME
              , IN_UPDATED_BY => IN_USER_NAME
              , OUT_STATUS => outStatus
              , OUT_MESSAGE => outMessage
          );
      end if;

      update clean.refund
          set refund_status = 'Billed',
          refund_date = sysdate,
          refund_tax = refund_rec.refund_tax,
          refund_shipping_tax = refund_rec.refund_shipping_tax,
          updated_on = sysdate,
          updated_by = 'PROCESS_REFUNDS'
      where refund_id = refund_rec.refund_id;

      update clean.payments
          set bill_status = 'Billed',
          bill_date = sysdate,
          updated_on = sysdate,
          updated_by = 'PROCESS_REFUNDS'
      where refund_id = refund_rec.refund_id;

  end loop;

END PROCESS_REFUNDS;

PROCEDURE INSERT_FULL_AMAZON_ADJUSTMENT 
(
  IN_CONFIRMATION_NUMBER IN VARCHAR2,
  IN_ADJUSTMENT_REASON IN VARCHAR2,
  OUT_STATUS OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2  
) AS

CURSOR get_detail_cur IS
  SELECT az_order_item_number,
      principal_amt,
      shipping_amt,
      tax_amt,
      shipping_tax_amt
  FROM az_order_detail
  WHERE confirmation_number = in_confirmation_number;
  
BEGIN

    FOR az_rec IN get_detail_cur loop
        INSERT INTO AZ_ORDER_ADJUSTMENT (
            AZ_ORDER_ADJUSTMENT_ID,
            AZ_ORDER_ITEM_NUMBER,
            ADJUSTMENT_REASON,
            PRINCIPAL_AMT,
            SHIPPING_AMT,
            TAX_AMT,
            SHIPPING_TAX_AMT,
            FEED_STATUS,
            CREATED_BY,
            CREATED_ON,
            UPDATED_BY,
            UPDATED_ON
        ) VALUES (
            AZ_ORDER_ACKNOWLEDGEMENT_ID_SQ.nextval,
            AZ_REC.AZ_ORDER_ITEM_NUMBER,
            IN_ADJUSTMENT_REASON,
            AZ_REC.PRINCIPAL_AMT,
            AZ_REC.SHIPPING_AMT,
            AZ_REC.TAX_AMT,
            AZ_REC.SHIPPING_TAX_AMT,
            'NEW',
            'OrderValidator',
            sysdate,
            'OrderValidator',       
            sysdate
        );
    END LOOP;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_FULL_AMAZON_ADJUSTMENT;

PROCEDURE CHECK_PDB_EXCEPTION_DATES
(
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
) AS

CURSOR PRODUCT_CUR IS
    select pm.product_id
    from ftd_apps.product_master pm
    join ptn_amazon.az_product_master apm
    on apm.product_id = pm.product_id
    and (catalog_status_standard = 'A'
        or catalog_status_deluxe = 'A'
        or catalog_status_premium = 'A')
    where pm.exception_start_date is not null
    and pm.exception_start_date = trunc(sysdate + 1)
    or (pm.exception_end_date is not null
        and pm.exception_end_date = trunc(sysdate));

v_count number := 0;

BEGIN

    FOR product_rec IN product_cur LOOP

        select count(*) into v_count
        from ptn_amazon.az_INVENTORY_feed
        where product_id = product_rec.product_id
        and feed_status = 'NEW';

        if v_count = 0 then

            insert into ptn_amazon.az_inventory_feed (
                AZ_INVENTORY_FEED_ID,
                PRODUCT_ID,
                FEED_STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY)
            values (
                ptn_amazon.az_inventory_feed_id_sq.nextval,
                product_rec.product_id,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS');

        end if;

    END LOOP;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END CHECK_PDB_EXCEPTION_DATES;

END AMAZON_MAINT_PKG;
.
/
