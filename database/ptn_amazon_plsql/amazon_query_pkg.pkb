CREATE OR REPLACE
PACKAGE BODY PTN_AMAZON.AMAZON_QUERY_PKG AS

PROCEDURE GET_INVENTORY_FEED_BY_STATUS
(
    IN_FEED_STATUS     IN  VARCHAR2,
    OUT_CUR       OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR
    select aif.az_inventory_feed_id,
        aif.product_id, 
        apm.catalog_status_standard,
        apm.sent_to_amazon_standard_flag, 
        apm.catalog_status_deluxe,
        apm.sent_to_amazon_deluxe_flag, 
        apm.catalog_status_premium,
        apm.sent_to_amazon_premium_flag,
        case when pm.status = 'A' and
            (pm.exception_start_date is null
                or (pm.exception_start_date <= trunc(sysdate + 1)
                and pm.exception_end_date > trunc(sysdate)))
            then 'A'
            else 'U'
        end as "pm_status"
    from ptn_amazon.az_inventory_feed aif,
        ptn_amazon.az_product_master apm,
        ftd_apps.product_master pm
    where aif.feed_status = IN_FEED_STATUS
    AND aif.product_id = apm.product_id
    and pm.product_id = aif.product_id;

END GET_INVENTORY_FEED_BY_STATUS;

PROCEDURE GET_PRODUCT_FEED
(
    OUT_CUR       OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
    SELECT pf.product_id,
        pm.novator_name,
        pm.long_description
    FROM az_product_feed pf,
        ftd_apps.product_master pm
    WHERE pf.feed_status = 'NEW'
    AND pf.product_id = pm.product_id;

END GET_PRODUCT_FEED;

PROCEDURE GET_IMAGE_FEED
(
  OUT_CUR       OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR 
    SELECT imf.az_image_feed_id,imf.product_id,
    apm.sent_to_amazon_standard_flag, apm.sent_to_amazon_deluxe_flag, 
    apm.sent_to_amazon_premium_flag,apm.catalog_status_standard, 
    apm.catalog_status_deluxe, apm.catalog_status_premium,
    pm.novator_id
    FROM ptn_amazon.az_image_feed imf, ptn_amazon.az_product_master apm,
          ftd_apps.product_master pm
    WHERE imf.feed_status = 'NEW'
    AND imf.product_id = pm.product_id
    AND apm.product_id = imf.product_id;
    
 END GET_IMAGE_FEED; 

PROCEDURE GET_PRICE_FEED
(
  OUT_CUR       OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR 
    SELECT pf.az_price_feed_id,pf.product_id,
    apm.sent_to_amazon_standard_flag, apm.sent_to_amazon_deluxe_flag, 
    apm.sent_to_amazon_premium_flag,apm.catalog_status_standard, 
    apm.catalog_status_deluxe, apm.catalog_status_premium,
    pm.standard_price,pm.deluxe_price,pm.premium_price
    FROM ptn_amazon.az_price_feed pf, ptn_amazon.az_product_master apm,
          ftd_apps.product_master pm
    WHERE pf.feed_status = 'NEW'
    AND pf.product_id = pm.product_id
    AND apm.product_id = pf.product_id;
    
 END GET_PRICE_FEED; 

PROCEDURE GET_FEED_SENT_TO_AMAZON
(
    OUT_CUR       OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR
    select az_inventory_feed_id as feed_id, az_transaction_id, 'INVENTORY_FEED' as feed_type 
	from ptn_amazon.az_inventory_feed 
	where feed_status='SENT'
	union 
	select az_image_feed_id as feed_id, az_transaction_id, 'IMAGE_FEED' as feed_type 
	from ptn_amazon.az_image_feed 
	where feed_status='SENT'
  union 
	select az_product_feed_id as feed_id, az_transaction_id, 'PRODUCT_FEED' as feed_type 
	from ptn_amazon.az_product_feed 
	where feed_status='SENT'
        union 
	select az_price_feed_id as feed_id, az_transaction_id, 'PRICE_FEED' as feed_type 
	from ptn_amazon.az_price_feed 
	where feed_status='SENT'
	  	union
	select AZ_ORDER_ACKNOWLEDGEMENT_ID as feed_id, az_transaction_id, 'ORDER_ACK_FEED' as feed_type 
	from ptn_amazon.AZ_ORDER_ACKNOWLEDGEMENT 
	where feed_status='SENT'
	  	union
	select AZ_ORDER_ADJUSTMENT_ID as feed_id, az_transaction_id, 'ORDER_ADJ_FEED' as feed_type 
	from ptn_amazon.AZ_ORDER_ADJUSTMENT 
	where feed_status='SENT'
	  	union
    select AZ_ORDER_FULFILLMENT_ID as feed_id, az_transaction_id, 'ORDER_FULFILL_FEED' as feed_type
    from ptn_amazon.AZ_ORDER_FULFILLMENT
    where feed_status='SENT';

END GET_FEED_SENT_TO_AMAZON;

PROCEDURE GET_AZ_ORDER_XML
(
   IN_AZ_ORDER_NUMBER     IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a AZ_XML from AZ_ORDER table

Input:
        IN_AZ_ORDER_NUMBER       az_order_number        

Output:
        out_cur      Cursor containing the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select AZ_XML
from PTN_AMAZON.AZ_ORDER
where AZ_ORDER_NUMBER = IN_AZ_ORDER_NUMBER;

END GET_AZ_ORDER_XML;

PROCEDURE GET_PRODUCT_FEED_BY_STATUS
(
    IN_FEED_STATUS     IN  VARCHAR2,
    OUT_CUR       OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR
    Select aif.az_product_feed_id, aif.product_id, 
          Apm.catalog_status_standard, apm.sent_to_amazon_standard_FLAG, apm.PRODUCT_NAME_STANDARD,apm.PRODUCT_DESCRIPTION_STANDARD,
          apm.catalog_status_deluxe, apm.sent_to_amazon_deluxe_FLAG, apm.PRODUCT_NAME_deluxe,apm.PRODUCT_DESCRIPTION_deluxe,
          apm.catalog_status_premium, apm.SENT_TO_AMAZON_PREMIUM_FLAG, apm.PRODUCT_NAME_PREMIUM,apm.PRODUCT_DESCRIPTION_PREMIUM, apm.created_on,
          pm.status as "pm_status",apm.item_type    
  From ptn_amazon.az_product_feed aif, ptn_amazon.az_product_master apm, ftd_apps.product_master pm
  Where aif.feed_status = IN_FEED_STATUS AND aif.product_id = apm.product_id
          and pm.product_id = aif.product_id;

END GET_PRODUCT_FEED_BY_STATUS;

PROCEDURE GET_ATTRS_BY_PROD
(
  IN_PRODUCT_ID     				    IN  VARCHAR2,
  IN_AZ_PRODUCT_FEED_ID         IN  VARCHAR2,
  OUT_BULLET_POINTS_CUR       	OUT TYPES.REF_CURSOR,
	OUT_SEARCH_TERMS_CUR       		OUT TYPES.REF_CURSOR,
	OUT_INTENDED_USE_CUR       		OUT TYPES.REF_CURSOR,
	OUT_TARGET_AUDIENCE_CUR       OUT TYPES.REF_CURSOR,
	OUT_OTHER_ATTRIBUTES_CUR      OUT TYPES.REF_CURSOR,
	OUT_SUBJECT_MATTER_CUR       	OUT TYPES.REF_CURSOR
	
) AS

BEGIN
    OPEN OUT_BULLET_POINTS_CUR FOR
		select apf.az_product_feed_id, apf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_amazon.az_product_feed apf 
		left outer join ptn_amazon.az_product_attributes apa on apf.product_Id=apa.product_id and apa.product_attribute_type='BULLET_POINTS'
		where apf.product_id=IN_PRODUCT_ID and apf.az_product_feed_id = in_az_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_SEARCH_TERMS_CUR FOR
		select apf.az_product_feed_id, apf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_amazon.az_product_feed apf 
		left outer join ptn_amazon.az_product_attributes apa on apf.product_Id=apa.product_id and apa.product_attribute_type='SEARCH_TERMS'
		where apf.product_id=IN_PRODUCT_ID and apf.az_product_feed_id = in_az_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_INTENDED_USE_CUR FOR
		select apf.az_product_feed_id, apf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_amazon.az_product_feed apf 
		left outer join ptn_amazon.az_product_attributes apa on apf.product_Id=apa.product_id and apa.product_attribute_type='INTENDED_USE'
		where apf.product_id=IN_PRODUCT_ID and apf.az_product_feed_id = in_az_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_TARGET_AUDIENCE_CUR FOR
		select apf.az_product_feed_id, apf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_amazon.az_product_feed apf 
		left outer join ptn_amazon.az_product_attributes apa on apf.product_Id=apa.product_id and apa.product_attribute_type='TARGET_AUDIENCE'
		where apf.product_id=IN_PRODUCT_ID and apf.az_product_feed_id = in_az_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_OTHER_ATTRIBUTES_CUR FOR
		select apf.az_product_feed_id, apf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_amazon.az_product_feed apf 
		left outer join ptn_amazon.az_product_attributes apa on apf.product_Id=apa.product_id and apa.product_attribute_type='OTHER_ATTRIBUTES'
		where apf.product_id=IN_PRODUCT_ID and apf.az_product_feed_id = in_az_product_feed_id
		order by apa.product_attribute_sequence;
	
	OPEN OUT_SUBJECT_MATTER_CUR FOR
		select apf.az_product_feed_id, apf.product_id, apa.product_attribute_type, apa.product_attribute_value, apa.product_attribute_sequence 
		from ptn_amazon.az_product_feed apf 
		left outer join ptn_amazon.az_product_attributes apa on apf.product_Id=apa.product_id and apa.product_attribute_type='SUBJECT_MATTER'
		where apf.product_id=IN_PRODUCT_ID and apf.az_product_feed_id = in_az_product_feed_id
		order by apa.product_attribute_sequence;
	
	
END GET_ATTRS_BY_PROD;

PROCEDURE GET_ORDER_ACK_FEED_BY_STATUS
(
    IN_FEED_STATUS     	IN  VARCHAR2,
    OUT_CUR       		OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR
        select ack.AZ_ORDER_ACKNOWLEDGEMENT_ID,
            ack.AZ_ORDER_ITEM_NUMBER, 
            ack.STATUS_CODE, 
            CASE WHEN ack.CANCEL_REASON is not null THEN
                nvl((select crm.az_cancel_reason
                    from cancel_reason_mapping crm
                    where crm.apollo_cancel_code = ack.cancel_reason
                    and crm.apollo_cancel_type = 'Acknowledgement'),
                (select crm.az_cancel_reason
                    from cancel_reason_mapping crm
                    where crm.apollo_cancel_code = 'Default'
                    and crm.apollo_cancel_type = 'Acknowledgement'))
               else null
            END cancel_reason,
            ack.AZ_TRANSACTION_ID,
            od.AZ_ORDER_NUMBER,
            od.CONFIRMATION_NUMBER,
            o.MASTER_ORDER_NUMBER
        from AZ_ORDER_ACKNOWLEDGEMENT ack,
            AZ_ORDER_DETAIL od,
            AZ_ORDER o
        where ack.AZ_ORDER_ITEM_NUMBER=od.AZ_ORDER_ITEM_NUMBER
        and od.AZ_ORDER_NUMBER=o.AZ_ORDER_NUMBER
        and ack.feed_status = IN_FEED_STATUS;

END GET_ORDER_ACK_FEED_BY_STATUS;

PROCEDURE GET_ORDER_ADJ_FEED_BY_STATUS
(
    IN_FEED_STATUS     	IN  VARCHAR2,
    OUT_CUR       		OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR
        SELECT adj.AZ_ORDER_ADJUSTMENT_ID,
            adj.AZ_ORDER_ITEM_NUMBER,
            CASE WHEN adj.ADJUSTMENT_REASON is not null THEN
                nvl((select crm.az_cancel_reason
                    from cancel_reason_mapping crm
                    where crm.apollo_cancel_code = adj.adjustment_reason
                    and crm.apollo_cancel_type = 'Adjustment'),
                (select crm.az_cancel_reason
                    from cancel_reason_mapping crm
                    where crm.apollo_cancel_code = 'Default'
                    and crm.apollo_cancel_type = 'Adjustment'))
               else null
            END adjustment_reason,
            adj.PRINCIPAL_AMT,
            adj.SHIPPING_AMT,
            adj.TAX_AMT,
            adj.SHIPPING_TAX_AMT, 
            adj.AZ_TRANSACTION_ID,
            od.AZ_ORDER_NUMBER,
            od.CONFIRMATION_NUMBER,
            o.MASTER_ORDER_NUMBER
        FROM AZ_ORDER_ADJUSTMENT adj,
            az_order_detail od,
            AZ_ORDER o
        WHERE adj.AZ_ORDER_ITEM_NUMBER=od.AZ_ORDER_ITEM_NUMBER 
        AND od.AZ_ORDER_NUMBER=o.AZ_ORDER_NUMBER 
        AND adj.feed_status = IN_FEED_STATUS;

END GET_ORDER_ADJ_FEED_BY_STATUS;

PROCEDURE GET_FULFILLMENT_FEED_BY_STATUS
(
    IN_FEED_STATUS IN VARCHAR2,
    OUT_CUR        OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR 
    SELECT aof.az_order_fulfillment_id,aof.shipping_method,aof.carrier_name,
      aof.tracking_number,aof.fulfillment_date,aof.az_order_item_number,o.az_order_number
  FROM ptn_amazon.az_order_fulfillment aof,ptn_amazon.az_order_detail od,ptn_amazon.az_order o
  WHERE aof.az_order_item_number = od.az_order_item_number
  AND od.az_order_number = o.az_order_number
  AND aof.feed_status = IN_FEED_STATUS;

END GET_FULFILLMENT_FEED_BY_STATUS;

PROCEDURE GET_ORDER_CONFIRMATION_NUMBER
(
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Get confirmation / external order number

Input:     

Output:
        out_cur      Cursor containing the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select 'Z' || LPAD ( TO_CHAR(ptn_amazon.confirmation_number_sq.nextval), 10, '0') as confirmation_number 
from dual;

END GET_ORDER_CONFIRMATION_NUMBER;

PROCEDURE GET_PDB_DATA
(
  IN_PRODUCT_ID          IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  OUT_CUR                OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Gets the current price and novator id in product database for the passed
    product id.

Input:
        product_id                      varchar2

Output:
        out_cur      Cursor containing the rows
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    SELECT STANDARD_PRICE,
        DELUXE_PRICE,
        PREMIUM_PRICE,
        NOVATOR_ID,
        NVL(SHIP_METHOD_FLORIST, 'N') SHIP_METHOD_FLORIST,
        NVL(SHIP_METHOD_CARRIER, 'N') SHIP_METHOD_CARRIER
    FROM FTD_APPS.PRODUCT_MASTER
    WHERE PRODUCT_ID=IN_PRODUCT_ID;

END GET_PDB_DATA;

PROCEDURE GET_IOTW_SOURCE_CODE
(
  IN_SOURCE_CODE                IN JOE.IOTW.SOURCE_CODE%TYPE,
  IN_PRODUCT_ID                 IN JOE.IOTW.PRODUCT_ID%TYPE,
  IN_TIME_RECEIVED              IN JOE.IOTW.START_DATE%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Returns the IOTW source code that was in effect when the order was received.

Input:
        source_code                      varchar2
        product_id                   varchar2
        time_received                   varchar2

Output:
        out_cur      Cursor containing the row
-----------------------------------------------------------------------------*/
default_code varchar(10);
iotw_code varchar(10);
start_date date;
end_date date;
temp_source_code varchar(10);

BEGIN

DECLARE CURSOR iotw_cur IS
  SELECT  SOURCE_CODE,
          IOTW_SOURCE_CODE,
          TRUNC(START_DATE), --Truncate for price feed calculations
          END_DATE
  FROM JOE.IOTW
    WHERE SOURCE_CODE = IN_SOURCE_CODE and PRODUCT_ID = IN_PRODUCT_ID;

BEGIN
  OPEN iotw_cur;
  FETCH iotw_cur
        INTO    default_code,
                iotw_code,
                start_date,
                end_date;
  IF iotw_cur%NOTFOUND THEN
      temp_source_code := IN_SOURCE_CODE;
  ELSE
      IF (IN_TIME_RECEIVED-start_date>=0) AND (end_date-IN_TIME_RECEIVED>=0) THEN
          temp_source_code := iotw_code;
      ELSE
          temp_source_code := default_code;
      END IF;
  END IF;
  CLOSE iotw_cur;

END;

OPEN OUT_CUR FOR
  SELECT S.SOURCE_CODE as IOTW_SOURCE, D.DISCOUNT_AMT as DISCOUNT, D.DISCOUNT_TYPE as DISCOUNT_TYPE
  FROM  FTD_APPS.PRICE_HEADER_DETAILS D
  JOIN FTD_APPS.SOURCE S ON S.PRICING_CODE = D.PRICE_HEADER_ID
  WHERE S.SOURCE_CODE = temp_source_code;

END GET_IOTW_SOURCE_CODE;

PROCEDURE GET_PRICE_FEED_SALE_DATA
(
  IN_SOURCE_CODE                IN JOE.IOTW.SOURCE_CODE%TYPE,
  IN_PRODUCT_ID                 IN JOE.IOTW.PRODUCT_ID%TYPE,
  IN_PRODUCT_AMT                IN FTD_APPS.PRODUCT_MASTER.STANDARD_PRICE%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Returns the discount details, Start Date and End Date

Input:
        source_code                      varchar2
        product_id                   varchar2

Output:
        out_cur      Cursor containing the row
-----------------------------------------------------------------------------*/

cursor source_cur(v_source_code varchar2, v_product_amt number) is
    select phd.discount_amt, phd.discount_type
    from ftd_apps.source_master sm
    join ftd_apps.price_header_details phd
    on phd.price_header_id = sm.price_header_id
    and phd.min_dollar_amt <= v_product_amt
    and phd.max_dollar_amt >= v_product_amt
    where sm.source_code = v_source_code;

cursor iotw_cur(v_source_code varchar2, v_product_id varchar2, v_product_amt number) is
    select phd.discount_amt,
        phd.discount_type,
        i.start_date,
        i.end_date
    from joe.iotw i
    join ftd_apps.source_master sm
    on sm.source_code = i.iotw_source_code
    join ftd_apps.price_header_details phd
    on phd.price_header_id = sm.price_header_id
    and phd.min_dollar_amt <= v_product_amt
    and phd.max_dollar_amt >= v_product_amt
    where i.source_code = v_source_code
    and i.product_id = v_product_id;

source_discount_type  varchar2(10);
source_discount_amt   number;
source_start_date     date := trunc(sysdate);
iotw_discount_type    varchar2(10);
iotw_discount_amt     number;
iotw_start_date       date;
iotw_end_date         date;

begin

    open source_cur(in_source_code, in_product_amt);
    fetch source_cur into source_discount_amt, source_discount_type;
    close source_cur;

    open iotw_cur(in_source_code, in_product_id, in_product_amt);
    fetch iotw_cur into iotw_discount_amt, iotw_discount_type, iotw_start_date, iotw_end_date;
    close iotw_cur;

    OPEN out_cur FOR
        select source_discount_type "source_discount_type",
            source_discount_amt "source_discount_amt",
            source_start_date "source_start_date",
            iotw_discount_type "iotw_discount_type",
            iotw_discount_amt "iotw_discount_amt",
            iotw_start_date "iotw_start_date",
            iotw_end_date "iotw_end_date"
        from dual;

END GET_PRICE_FEED_SALE_DATA;

PROCEDURE GET_NEW_SETTLEMENT_DATA
(
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT SETTLEMENT_DATA_ID
        FROM SETTLEMENT_DATA
        WHERE PROCESSED_DATE IS NULL;

END GET_NEW_SETTLEMENT_DATA;

PROCEDURE GET_REMITTANCE_DETAIL
(
  IN_AZ_SETTLEMENT_DATA_ID       IN SETTLEMENT_DATA.SETTLEMENT_DATA_ID%TYPE,
  OUT_SETTLE_ORDER_ITEM_CURSOR  OUT TYPES.REF_CURSOR,
  OUT_SETTLE_ADJUST_ITEM_CURSOR OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_SETTLE_ORDER_ITEM_CURSOR FOR
        SELECT DISTINCT SD.AZ_ORDER_ITEM_NUMBER,
            AOD.CONFIRMATION_NUMBER
        FROM SETTLEMENT_DETAIL SD
        JOIN AZ_ORDER_DETAIL AOD
        ON AOD.AZ_ORDER_ITEM_NUMBER = SD.AZ_ORDER_ITEM_NUMBER
        WHERE SETTLEMENT_DATA_ID = IN_AZ_SETTLEMENT_DATA_ID
        AND TRANSACTION_TYPE = 'Order';

    OPEN OUT_SETTLE_ADJUST_ITEM_CURSOR FOR
        SELECT DISTINCT SD.AZ_ORDER_ITEM_NUMBER,
            AOD.CONFIRMATION_NUMBER
        FROM SETTLEMENT_DETAIL SD
        JOIN AZ_ORDER_DETAIL AOD
        ON AOD.AZ_ORDER_ITEM_NUMBER = SD.AZ_ORDER_ITEM_NUMBER
        WHERE SETTLEMENT_DATA_ID = IN_AZ_SETTLEMENT_DATA_ID
        AND TRANSACTION_TYPE = 'Adjustment';

END GET_REMITTANCE_DETAIL;

PROCEDURE GET_AZ_SETTLEMENT_XML
(
   IN_SETTLEMENT_REPORT_ID       IN VARCHAR2,
   OUT_CUR                      OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT REPORT
        FROM AZ_SETTLEMENT_REPORT
        WHERE AZ_REPORT_ID = IN_SETTLEMENT_REPORT_ID;

END GET_AZ_SETTLEMENT_XML;



PROCEDURE GET_FEED_FILENAME_BY_TXN_ID  
(
IN_FEED_TYPE			IN VARCHAR2,
IN_AZ_TRANSACTION_ID	IN VARCHAR2,
OUT_FILENAME			OUT VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS
	v_filename varchar(512);
BEGIN

  CASE IN_FEED_TYPE
        WHEN 'INVENTORY_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM AZ_INVENTORY_FEED
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
        WHEN 'IMAGE_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM  AZ_IMAGE_FEED
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
       WHEN 'PRODUCT_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM  AZ_PRODUCT_FEED
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
        WHEN 'PRICE_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM  AZ_PRICE_FEED
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
		WHEN 'ORDER_ACK_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM  AZ_ORDER_ACKNOWLEDGEMENT
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
		WHEN 'ORDER_ADJ_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM  AZ_ORDER_ADJUSTMENT
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;
         WHEN 'ORDER_FULFILL_FEED' THEN
          SELECT  DISTINCT FILENAME into v_filename FROM  AZ_ORDER_FULFILLMENT
          WHERE   AZ_TRANSACTION_ID = IN_AZ_TRANSACTION_ID;    
  END CASE;
  
OUT_FILENAME := v_filename;  
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_FEED_FILENAME_BY_TXN_ID; 


PROCEDURE GET_AMZ_PRODUCT_ID_BY_CNF_NO
(
IN_CONF_NO			IN VARCHAR2,
OUT_PRODUCT_ID  OUT VARCHAR2,
OUT_STATUS      OUT VARCHAR2,
OUT_MESSAGE     OUT VARCHAR2
)AS

v_amz_prod_id varchar(512);

BEGIN
  select amazon_product_id into v_amz_prod_id from ptn_amazon.az_order_detail where confirmation_number= in_conf_no; 
  OUT_STATUS := 'Y';
  OUT_PRODUCT_ID := v_amz_prod_id;

  EXCEPTION WHEN NO_DATA_FOUND THEN 
  BEGIN
  v_amz_prod_id := null;
  END; 
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_AMZ_PRODUCT_ID_BY_CNF_NO;

PROCEDURE GET_AMAZON_ORDER_DETAILS
(
  IN_CONFIRMATION_NUMBER          IN PTN_AMAZON.AZ_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Retrieves order details for the given confirmation number.

Input:
        CONFIRMATION_NUMBER                      varchar2

Output:
        out_cur      Cursor containing the rows
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    SELECT AZ_ORDER_ITEM_NUMBER,
        AZ_ORDER_NUMBER,
        AMAZON_PRODUCT_ID,
        PRINCIPAL_AMT,
        SHIPPING_AMT,
        TAX_AMT,
        SHIPPING_TAX_AMT
    FROM PTN_AMAZON.AZ_ORDER_DETAIL
    WHERE CONFIRMATION_NUMBER=IN_CONFIRMATION_NUMBER;

END GET_AMAZON_ORDER_DETAILS;

END AMAZON_QUERY_PKG;

.
/