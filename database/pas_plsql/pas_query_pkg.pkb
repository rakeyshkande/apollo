CREATE OR REPLACE 
PACKAGE BODY PAS.PAS_QUERY_PKG AS

PROCEDURE GET_NEXT_AVAIL_DELIVERY_DATE 
(
   IN_PRODUCT_ID         IN  VARCHAR2,
   IN_ZIP_CODE           IN  VARCHAR2,
   OUT_DELIVERY_DATE     IN OUT DATE
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves the next available delivery date for the product and
        the zip code.

Input:
        in_product_id  varchar2
        in_zip_code    varchar2

Output:
        out_delivery_date     date

-----------------------------------------------------------------------------*/

CURSOR florist_cursor IS
  select delivery_date
  from pas_florist_product_zip_dt_vw
  where product_id = IN_PRODUCT_ID
    and zip_code = upper(IN_ZIP_CODE)
    and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
  order by delivery_date;


CURSOR vendor_cursor IS
  select delivery_date
  from pas_vendor_product_zip_dt_vw
  where 1=1
  and product_id = IN_PRODUCT_ID
  and zip_code_id = upper(IN_ZIP_CODE)
  and delivery_date > trunc(sysdate)
  and (
       (
        ((ship_nd_date - trunc(sysdate)) > 0) or 
        (((ship_nd_date - trunc(sysdate)) = 0) and 
         (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_2d_date - trunc(sysdate)) > 0) or 
        (((ship_2d_date - trunc(sysdate)) = 0) and 
         (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_gr_date - trunc(sysdate)) > 0) or 
        (((ship_gr_date - trunc(sysdate)) = 0) and 
         (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_sat_date - trunc(sysdate)) > 0) or 
        (((ship_sat_date - trunc(sysdate)) = 0) and 
         (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
      )
  order by delivery_date;


v_florist_date    date;
v_vendor_date     date;

BEGIN
     
   OPEN florist_cursor;
   FETCH florist_cursor INTO v_florist_date;
   CLOSE florist_cursor;

   OPEN vendor_cursor;
   FETCH vendor_cursor INTO v_vendor_date;
   CLOSE vendor_cursor;

   IF v_florist_date is null THEN
      OUT_DELIVERY_DATE := v_vendor_date;
   ELSE
      IF v_vendor_date is null THEN
         OUT_DELIVERY_DATE := v_florist_date;
      ELSE
         IF v_vendor_date < v_florist_date THEN
            OUT_DELIVERY_DATE := v_vendor_date;
         ELSE
            OUT_DELIVERY_DATE := v_florist_date;
         END IF;
      END IF;
   END IF;

END GET_NEXT_AVAIL_DELIVERY_DATE;

PROCEDURE IS_PRODUCT_AVAILABLE 
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_AVAILABLE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Determines if a product is available in a zip for the delivery date.
        the zip code.

Input:
        in_product_id    varchar2
        in_zip_code      varchar2
        in_delivery_date Date

Output:
        out_available    varchar2 Y or N

-----------------------------------------------------------------------------*/

CURSOR florist_cursor IS
  select delivery_date
  from pas_florist_product_zip_dt_vw
  where product_id = IN_PRODUCT_ID
    and zip_code = upper(IN_ZIP_CODE)
    and delivery_date = trunc(IN_DELIVERY_DATE)
    and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
  order by delivery_date;


CURSOR vendor_cursor IS
  select delivery_date
  from pas_vendor_product_zip_dt_vw
  where 1=1
  and product_id = IN_PRODUCT_ID
  and zip_code_id = upper(IN_ZIP_CODE)
  and delivery_date = trunc(IN_DELIVERY_DATE)
  and delivery_date > trunc(sysdate)
  and (
       (
        ((ship_nd_date - trunc(sysdate)) > 0) or 
        (((ship_nd_date - trunc(sysdate)) = 0) and 
         (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_2d_date - trunc(sysdate)) > 0) or 
        (((ship_2d_date - trunc(sysdate)) = 0) and 
         (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_gr_date - trunc(sysdate)) > 0) or 
        (((ship_gr_date - trunc(sysdate)) = 0) and 
         (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_sat_date - trunc(sysdate)) > 0) or 
        (((ship_sat_date - trunc(sysdate)) = 0) and 
         (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
      )
  order by delivery_date;


v_florist_date    date;
v_vendor_date     date;

BEGIN
     
   OPEN florist_cursor;
   FETCH florist_cursor INTO v_florist_date;
   CLOSE florist_cursor;

   OPEN vendor_cursor;
   FETCH vendor_cursor INTO v_vendor_date;
   CLOSE vendor_cursor;

   OUT_AVAILABLE := 'N';
   IF v_vendor_date is not null THEN
	OUT_AVAILABLE := 'Y';
   END IF;
   IF v_florist_date is not null THEN
	OUT_AVAILABLE := 'Y';
   END IF;

END IS_PRODUCT_AVAILABLE;

PROCEDURE IS_ANY_PRODUCT_AVAILABLE 
(
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_AVAILABLE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Determines if a product is available in a zip for the delivery date.
        the zip code.

Input:
        in_zip_code      varchar2
        in_delivery_date Date

Output:
        out_available    varchar2 Y or N

-----------------------------------------------------------------------------*/

CURSOR florist_cursor IS
  select delivery_date
  from pas_florist_product_zip_dt_vw
  where zip_code = upper(IN_ZIP_CODE)
    and delivery_date = trunc(IN_DELIVERY_DATE)
    and product_id = pas.get_a_non_codified_product
    and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N';

CURSOR vendor_cursor IS
  select delivery_date
  from pas_vendor_product_zip_dt_vw
  where 1=1
  and zip_code_id = upper(IN_ZIP_CODE)
  and delivery_date = trunc(IN_DELIVERY_DATE)
  and delivery_date > trunc(sysdate)
  and (
       (
        ((ship_nd_date - trunc(sysdate)) > 0) or 
        (((ship_nd_date - trunc(sysdate)) = 0) and 
         (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_2d_date - trunc(sysdate)) > 0) or 
        (((ship_2d_date - trunc(sysdate)) = 0) and 
         (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_gr_date - trunc(sysdate)) > 0) or 
        (((ship_gr_date - trunc(sysdate)) = 0) and 
         (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_sat_date - trunc(sysdate)) > 0) or 
        (((ship_sat_date - trunc(sysdate)) = 0) and 
         (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
      );

v_florist_date    date;
v_vendor_date     date;

BEGIN
     
   OPEN florist_cursor;
   FETCH florist_cursor INTO v_florist_date;
   CLOSE florist_cursor;

   IF v_florist_date is null THEN
     OPEN vendor_cursor;
     FETCH vendor_cursor INTO v_vendor_date;
     CLOSE vendor_cursor;
   END IF;

   OUT_AVAILABLE := 'N';
   IF v_florist_date is not null THEN
	OUT_AVAILABLE := 'Y';
   END IF;   
   IF v_vendor_date is not null THEN
	OUT_AVAILABLE := 'Y';
   END IF;


END IS_ANY_PRODUCT_AVAILABLE;

PROCEDURE IS_INTL_PRODUCT_AVAILABLE 
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_COUNTRY_ID          IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_AVAILABLE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Determines if a product is available in a country for the delivery date.

Input:
        in_product_id    varchar2
        in_country_id    varchar2
        in_delivery_date Date

Output:
        out_available    varchar2 Y or N

-----------------------------------------------------------------------------*/

CURSOR intl_cursor IS
  select delivery_date
  from pas_country_product_dt
  where product_id = IN_PRODUCT_ID
    and country_id = IN_COUNTRY_ID
    and delivery_available_flag = 'Y'
    and delivery_date = trunc(IN_DELIVERY_DATE)
    and (
         ((cutoff_date - trunc(sysdate)) > 0) or 
         (((cutoff_date - trunc(sysdate)) = 0) and 
          (cutoff_time > to_char(sysdate,'HH24mi'))
         )
        )
  order by delivery_date;



v_intl_date    date;

BEGIN
     
   OPEN intl_cursor;
   FETCH intl_cursor INTO v_intl_date;
   CLOSE intl_cursor;

   OUT_AVAILABLE := 'N';
   IF v_intl_date is not null THEN
	OUT_AVAILABLE := 'Y';
   END IF;

END IS_INTL_PRODUCT_AVAILABLE;

PROCEDURE GET_PRODUCT_AVAILABILITY 
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Get the details of product availability for a product in a zip for a day

Input:
        in_product_id    varchar2
        in_zip_code      varchar2
        in_delivery_date Date

Output:
        out_cur          cursor Details

-----------------------------------------------------------------------------*/

CURSOR ship_method_cursor IS 
  select ship_method_florist, ship_method_carrier 
  from pas_product_dt
  where product_id = IN_PRODUCT_ID
  and delivery_date = trunc(IN_DELIVERY_DATE);


CURSOR florist_cursor IS
  select cutoff_time,
         (delivery_date - addon_days_qty) as cutoff_date
  from pas_florist_product_zip_dt_vw
  where product_id = IN_PRODUCT_ID
    and zip_code = upper(IN_ZIP_CODE)
    and delivery_date = trunc(IN_DELIVERY_DATE)
    and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
  order by cutoff_date, cutoff_time desc;


CURSOR vendor_cursor IS
  select 
         ship_nd_date,
         ship_nd_cutoff_time,
         ship_2d_date,
         ship_2d_cutoff_time,
         ship_gr_date,
         ship_gr_cutoff_time,
         ship_sat_date,
         ship_sat_cutoff_time 
  from pas_vendor_product_zip_dt_vw
  where 1=1
  and product_id = IN_PRODUCT_ID
  and zip_code_id = upper(IN_ZIP_CODE)
  and trunc(delivery_date) = trunc(IN_DELIVERY_DATE)
  and delivery_date > trunc(sysdate)
  and (
       (
        ((ship_nd_date - trunc(sysdate)) > 0) or 
        (((ship_nd_date - trunc(sysdate)) = 0) and 
         (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_2d_date - trunc(sysdate)) > 0) or 
        (((ship_2d_date - trunc(sysdate)) = 0) and 
         (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_gr_date - trunc(sysdate)) > 0) or 
        (((ship_gr_date - trunc(sysdate)) = 0) and 
         (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_sat_date - trunc(sysdate)) > 0) or 
        (((ship_sat_date - trunc(sysdate)) = 0) and 
         (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
      );


v_ship_method_florist pas_product_dt.ship_method_florist%type;
v_ship_method_carrier pas_product_dt.ship_method_carrier%type;
v_florist_cutoff_time pas_florist_product_zip_dt_vw.cutoff_time%type;
v_florist_cutoff_date date;
v_vendor_nd_date    date;
v_vendor_nd_cutoff  pas_vendor_product_zip_dt_vw.ship_nd_cutoff_time%type;
v_vendor_2d_date    date;
v_vendor_2d_cutoff  pas_vendor_product_zip_dt_vw.ship_nd_cutoff_time%type;
v_vendor_gr_date    date;
v_vendor_gr_cutoff  pas_vendor_product_zip_dt_vw.ship_nd_cutoff_time%type;
v_vendor_sa_date    date;
v_vendor_sa_cutoff  pas_vendor_product_zip_dt_vw.ship_nd_cutoff_time%type;

BEGIN
     
   OPEN ship_method_cursor;
   FETCH ship_method_cursor INTO v_ship_method_florist, v_ship_method_carrier;
   CLOSE ship_method_cursor;
     
   OPEN florist_cursor;
   FETCH florist_cursor INTO v_florist_cutoff_time, v_florist_cutoff_date;
   CLOSE florist_cursor;

   FOR vendor_rec in vendor_cursor LOOP

     IF vendor_rec.ship_nd_date is not null THEN
       IF v_vendor_nd_date is null THEN
         v_vendor_nd_date := vendor_rec.ship_nd_date;
         v_vendor_nd_cutoff := vendor_rec.ship_nd_cutoff_time;
       ELSE
         IF v_vendor_nd_date < vendor_rec.ship_nd_date THEN
           v_vendor_nd_date := vendor_rec.ship_nd_date;
           v_vendor_nd_cutoff := vendor_rec.ship_nd_cutoff_time;
         ELSIF  v_vendor_nd_date = vendor_rec.ship_nd_date THEN
           IF v_vendor_nd_cutoff < vendor_rec.ship_nd_cutoff_time THEN
             v_vendor_nd_cutoff := vendor_rec.ship_nd_cutoff_time;
           END IF;		
         END IF;		
       END IF;
     END IF;

     IF vendor_rec.ship_2d_date is not null THEN
       IF v_vendor_2d_date is null THEN
         v_vendor_2d_date := vendor_rec.ship_2d_date;
         v_vendor_2d_cutoff := vendor_rec.ship_2d_cutoff_time;
       ELSE
         IF v_vendor_2d_date < vendor_rec.ship_2d_date THEN
           v_vendor_2d_date := vendor_rec.ship_2d_date;
           v_vendor_2d_cutoff := vendor_rec.ship_2d_cutoff_time;
         ELSIF  v_vendor_nd_date = vendor_rec.ship_2d_date THEN
           IF v_vendor_2d_cutoff < vendor_rec.ship_2d_cutoff_time THEN
             v_vendor_2d_cutoff := vendor_rec.ship_2d_cutoff_time;
           END IF;		
         END IF;		
       END IF;
     END IF;

     IF vendor_rec.ship_gr_date is not null THEN
       IF v_vendor_gr_date is null THEN
         v_vendor_gr_date := vendor_rec.ship_gr_date;
         v_vendor_gr_cutoff := vendor_rec.ship_gr_cutoff_time;
       ELSE
         IF v_vendor_gr_date < vendor_rec.ship_gr_date THEN
           v_vendor_gr_date := vendor_rec.ship_gr_date;
           v_vendor_gr_cutoff := vendor_rec.ship_gr_cutoff_time;
         ELSIF  v_vendor_gr_date = vendor_rec.ship_gr_date THEN
           IF v_vendor_gr_cutoff < vendor_rec.ship_gr_cutoff_time THEN
             v_vendor_gr_cutoff := vendor_rec.ship_gr_cutoff_time;
           END IF;		
         END IF;		
       END IF;
     END IF;

     IF vendor_rec.ship_sat_date is not null THEN
       IF v_vendor_sa_date is null THEN
         v_vendor_sa_date := vendor_rec.ship_sat_date;
         v_vendor_sa_cutoff := vendor_rec.ship_sat_cutoff_time;
       ELSE
         IF v_vendor_sa_date < vendor_rec.ship_sat_date THEN
           v_vendor_sa_date := vendor_rec.ship_sat_date;
           v_vendor_sa_cutoff := vendor_rec.ship_sat_cutoff_time;
         ELSIF  v_vendor_sa_date = vendor_rec.ship_sat_date THEN
           IF v_vendor_sa_cutoff < vendor_rec.ship_sat_cutoff_time THEN
             v_vendor_sa_cutoff := vendor_rec.ship_sat_cutoff_time;
           END IF;		
         END IF;		
       END IF;
     END IF;


   END LOOP;

   OPEN OUT_CUR for
     select 
       v_florist_cutoff_time as florist_cutoff_time,
       v_florist_cutoff_date as florist_cutoff_date,
       v_vendor_nd_date as ship_nd_date, 
       v_vendor_nd_cutoff as ship_nd_date_cutoff,
       v_vendor_2d_date as ship_2d_date,
       v_vendor_2d_cutoff as ship_2d_date_cutoff,
       v_vendor_gr_date as ship_gr_date, 
       v_vendor_gr_cutoff as ship_gr_date_cutoff,
       v_vendor_sa_date as ship_sa_date, 
       v_vendor_sa_cutoff as ship_sa_date_cutoff,
       v_ship_method_florist as ship_method_florist,
       v_ship_method_carrier as ship_method_carrier 
     from dual;

END GET_PRODUCT_AVAILABILITY;


PROCEDURE GET_AVAILABLE_FLORISTS 
(
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all the florists still open for a zip and delivery date

Input:
        in_zip_code        Zip code to check for
        in_delivery_date   delivery date
Output:
        out_cur      Cursor containin the florists

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        select fz.florist_id
        from ftd_apps.florist_zips fz
        join ftd_apps.florist_master fm
            on fm.florist_id = fz.florist_id
        join frp.global_parms gp
            on gp.context = 'FTDAPPS_PARMS'
            and gp.name = decode(trim(to_char(in_delivery_date, 'DAY')),
                'SATURDAY', 'SATURDAY_CUTOFF', 
                'SUNDAY', 'SUNDAY_CUTOFF',
                'LATEST_CUTOFF')
        join ftd_apps.state_master sm
            on sm.state_master_id = (select state_id
              from ftd_apps.zip_code zc
              where zc.zip_code_id = fz.zip_code
              and rownum = 1)
        join pas.pas_timezone_dt pt
            on pt.time_zone_code = sm.time_zone
            and pt.delivery_date = in_delivery_date
        where fz.zip_code = upper(in_zip_code)
        and fm.florist_id = fz.florist_id
        and fm.status <> 'Inactive'
        and ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fz.florist_id, in_delivery_date) = 'Y'
        and ftd_apps.florist_query_pkg.is_florist_open_in_eros(fz.florist_id) = 'Y'
        and (fz.block_start_date is null
            or not(in_delivery_date between trunc(fz.block_start_date) and nvl(trunc(fz.block_end_date), in_delivery_date)))
        and ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, in_delivery_date, null) = 'N'
        and ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, in_delivery_date, null) = 'N'
        and get_cutoff(in_delivery_date, 0, least(nvl(fz.cutoff_time, '2359'), gp.value) + pt.delta_to_cst_hhmm) = 'N'
        order by fz.cutoff_time;

     
END GET_AVAILABLE_FLORISTS;

PROCEDURE GET_MOST_POPULAR_PRODUCTS 
(
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_COMPANY_ID          IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   IN_MAX_ROWS            IN  NUMBER,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the active products and a subset of the data for them 
        sorted by most popular.

Input:
        in_zip_code           Zip Code
        in_company_id         Company
        in_delivery_date      Delivery Date, optional

Output:
        out_cur      Cursor containin the products

-----------------------------------------------------------------------------*/
v_sc varchar2(2);

BEGIN

v_sc := pas.get_state_by_zip(IN_ZIP_CODE);


OPEN OUT_CUR FOR
  select a.* from (
  select pm.product_id as product_id,
         pm.popularity_order_cnt as popularity,
         trunc(delivery_date - addon_days_qty) as florist_cutoff_date,
         pas.cutoff_time as florist_cutoff_time,
         null as ship_nd_date,
         null as ship_nd_cutoff_time,
         null as ship_2d_date,
         null as ship_2d_cutoff_time,
         null as ship_gr_date,
         null as ship_gr_cutoff_time,
         null as ship_sat_date,
         null as ship_sat_cutoff_time
  from pas_florist_product_zip_dt_vw pas,
       ftd_apps.product_master pm,
       ftd_apps.product_company_xref pcx
  where 1 = 1
    and pm.product_id = pas.product_id
    and pm.product_id = pcx.product_id
    and pcx.company_id = IN_COMPANY_ID
    and zip_code = upper(IN_ZIP_CODE)
    and delivery_date = IN_DELIVERY_DATE
    and pas.get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
  union
  select 
         pm.product_id,
         pm.popularity_order_cnt as popularity,
         null,
         null,
         ship_nd_date,
         ship_nd_cutoff_time,
         ship_2d_date,
         ship_2d_cutoff_time,
         ship_gr_date,
         ship_gr_cutoff_time,
         ship_sat_date,
         ship_sat_cutoff_time
  from PAS_VENDOR_PRODUCT_STATE_DT pvpsd,
       ftd_apps.product_master pm,
       ftd_apps.product_company_xref pcx
  where 1=1
    and pm.product_id = pvpsd.product_id
    and pm.product_id = pcx.product_id
    and pcx.company_id = IN_COMPANY_ID
    and state_code = v_sc
    and delivery_date = IN_DELIVERY_DATE
    and pm.status = 'A'
    and (
         (
          ((ship_nd_date - trunc(sysdate)) > 0) or 
          (((ship_nd_date - trunc(sysdate)) = 0) and 
           (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
          )
         )
       OR
       (
        ((ship_2d_date - trunc(sysdate)) > 0) or 
        (((ship_2d_date - trunc(sysdate)) = 0) and 
         (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_gr_date - trunc(sysdate)) > 0) or 
        (((ship_gr_date - trunc(sysdate)) = 0) and 
         (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
       OR
       (
        ((ship_sat_date - trunc(sysdate)) > 0) or 
        (((ship_sat_date - trunc(sysdate)) = 0) and 
         (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
        )
       )
      )
order by popularity desc
) a
where rownum <= IN_MAX_ROWS;


END GET_MOST_POPULAR_PRODUCTS;

PROCEDURE GET_MOST_POPULAR_PRODUCTS 
(
   IN_COMPANY_ID          IN  VARCHAR2,
   IN_MAX_ROWS            IN  NUMBER,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves products sorted by most popular.

Input:
        in_company_id         Company

Output:
        out_cur      Cursor containin the products

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  select a.* from (
  select pm.product_id as product_id, 
         pm.popularity_order_cnt as popularity,
         null as florist_cutoff_date,
         null as florist_cutoff_time,
         null as ship_nd_date,
         null as ship_nd_cutoff_time,
         null as ship_2d_date,
         null as ship_2d_cutoff_time,
         null as ship_gr_date,
         null as ship_gr_cutoff_time,
         null as ship_sat_date,
         null as ship_sat_cutoff_time
  from ftd_apps.product_master pm,
       ftd_apps.product_company_xref pcx
  where pm.status = 'A'
    and pm.product_id = pcx.product_id
    and pcx.company_id = IN_COMPANY_ID
  order by popularity desc
  ) a
  where rownum <= IN_MAX_ROWS;

     
END GET_MOST_POPULAR_PRODUCTS;

PROCEDURE GET_VENDOR_DELIVERY_DATES 
(
   IN_PRODUCT_ID         IN  VARCHAR2,
   IN_ZIP_CODE           IN  VARCHAR2,
   IN_NUMBER_DAYS        IN  NUMBER,
   OUT_ND_CUR            OUT TYPES.REF_CURSOR,
   OUT_2D_CUR            OUT TYPES.REF_CURSOR,
   OUT_GR_CUR            OUT TYPES.REF_CURSOR,
   OUT_SAT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves products sorted by most popular.

Input:

Output:
        out_cur      Cursor containin the products

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_ND_CUR FOR
select distinct delivery_date
from
   (select delivery_date,ship_nd_date, ship_nd_cutoff_time
   from pas.pas_vendor_product_zip_dt_vw
   where 1=1
   and product_id = IN_PRODUCT_ID
   and delivery_date > trunc(sysdate)
   and delivery_date <= trunc(sysdate + IN_NUMBER_DAYS)
   and zip_code_id = upper(IN_ZIP_CODE)) try2
   where ((ship_nd_date > trunc(sysdate)) or 
          (ship_nd_date = trunc(sysdate) and 
           (ship_nd_cutoff_time > to_char(sysdate,'HH24mi')))
          )
order by delivery_date;


OPEN OUT_2D_CUR FOR
select distinct delivery_date
from
   (select delivery_date,ship_2d_date, ship_2d_cutoff_time
   from pas.pas_vendor_product_zip_dt_vw
   where 1=1
   and product_id = IN_PRODUCT_ID
   and delivery_date > trunc(sysdate)
   and delivery_date <= trunc(sysdate + IN_NUMBER_DAYS)
   and zip_code_id = upper(IN_ZIP_CODE)) try2
   where ((ship_2d_date > trunc(sysdate)) or 
          (ship_2d_date = trunc(sysdate) and 
           (ship_2d_cutoff_time > to_char(sysdate,'HH24mi')))
          )
order by delivery_date;

OPEN OUT_GR_CUR FOR
select distinct delivery_date
from
   (select delivery_date,ship_gr_date, ship_gr_cutoff_time
   from pas.pas_vendor_product_zip_dt_vw
   where 1=1
   and product_id = IN_PRODUCT_ID
   and delivery_date > trunc(sysdate)
   and delivery_date <= trunc(sysdate + IN_NUMBER_DAYS)
   and zip_code_id = upper(IN_ZIP_CODE)) try2
   where ((ship_gr_date > trunc(sysdate)) or 
          (ship_gr_date = trunc(sysdate) and 
           (ship_gr_cutoff_time > to_char(sysdate,'HH24mi')))
          )
order by delivery_date;

OPEN OUT_SAT_CUR FOR
select distinct delivery_date
from
   (select delivery_date,ship_sat_date, ship_sat_cutoff_time
   from pas.pas_vendor_product_zip_dt_vw
   where 1=1
   and product_id = IN_PRODUCT_ID
   and delivery_date > trunc(sysdate)
   and delivery_date <= trunc(sysdate + IN_NUMBER_DAYS)
   and zip_code_id = upper(IN_ZIP_CODE)) try2
   where ((ship_sat_date > trunc(sysdate)) or 
          (ship_sat_date = trunc(sysdate) and 
           (ship_sat_cutoff_time > to_char(sysdate,'HH24mi')))
          )
order by delivery_date;


     
END GET_VENDOR_DELIVERY_DATES;

PROCEDURE GET_ACTIVE_PRODUCTS 
(
   IN_PRODUCT_ID         IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the active products and a subset of the data for them 

Input:
        in_product_id Product id to filter by, if null all
Output:
        out_cur      Cursor containin the countries

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);
v_delivery_date varchar2(100);
v_date_count integer;

BEGIN

    v_delivery_date := to_char(in_delivery_date, 'MM/DD/YYYY');

    select count(*)
    into v_date_count
    from ftd_apps.holiday_products
    where delivery_date = in_delivery_date;
    
    v_main_sql := 
        'select pm.product_id,
                pm.product_type,
                pm.exception_code,
                pm.exception_start_date,
                pm.exception_end_date,
                pm.exotic_flag,
                pm.personalization_lead_days,
                psd.SUNDAY_FLAg,
                psd.MONDAY_FLAG,
                psd.TUESDAY_FLAG,
                psd.WEDNESDAY_FLAG,
                psd.thursday_flag,
                psd.friday_flag,
                psd.saturday_flag,
                cp.codification_id, 
                case
                    when pm.delivery_type <> ''D'' then pm.ship_method_florist
                    when ' || v_date_count || ' = 0 then pm.ship_method_florist
                    else
                        case
                            when hp.product_id is null then ''N''
                            else pm.ship_method_florist
                        end
                end ship_method_florist,
                pm.ship_method_carrier,
                pm.pquad_product_id
            from ftd_apps.product_master pm
            left outer join ftd_apps.product_ship_dates psd
            on psd.product_id = pm.product_id
            left outer join ftd_apps.codified_products cp
            on cp.product_id = pm.product_id
            left outer join ftd_apps.holiday_products hp
            on hp.product_id = pm.product_id
            and to_char(hp.delivery_date, ''MM/DD/YYYY'') = ''' || v_delivery_date || '''
         where pm.status = ''A''';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND pm.product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    open out_cur for v_main_sql;

     
END GET_ACTIVE_PRODUCTS;

PROCEDURE GET_ACTIVE_EXOTIC_PRODUCTS 
(
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the active exotic products and a subset of the data for them 

Input:

Output:
        out_cur      Cursor containin the countries

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pm.product_id,
       pm.product_type,
       pm.exception_code,
       pm.exception_start_date,
       pm.exception_end_date,
       pm.exotic_flag,
       pm.personalization_lead_days,
       psd.SUNDAY_FLAg,
       psd.MONDAY_FLAG,
       psd.TUESDAY_FLAG,
       psd.WEDNESDAY_FLAG,
       psd.thursday_flag,
       psd.friday_flag,
       psd.saturday_flag,
       cp.codification_id
from ftd_apps.product_master pm,
     ftd_apps.product_ship_dates psd,
     ftd_apps.codified_products cp
where pm.status = 'A'
  and psd.product_id (+)= pm.product_id
  and cp.product_id (+)= pm.product_id
  and pm.exotic_flag = 'Y';

     
END GET_ACTIVE_EXOTIC_PRODUCTS;

PROCEDURE GET_PRODUCT 
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the details for a single product for PAS

Input:
        in_product_id  varchar2

Output:
        out_cur      Cursor containin the countries

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pm.product_id,
       pm.product_type,
       pm.exception_code,
       pm.exception_start_date,
       pm.exception_end_date,
       pm.exotic_flag,
       pm.personalization_lead_days,
       psd.SUNDAY_FLAg,
       psd.MONDAY_FLAG,
       psd.TUESDAY_FLAG,
       psd.WEDNESDAY_FLAG,
       psd.thursday_flag,
       psd.friday_flag,
       psd.saturday_flag,
       cp.codification_id
from ftd_apps.product_master pm,
     ftd_apps.product_ship_dates psd,
     ftd_apps.codified_products cp
where pm.status = 'A'
  and psd.product_id (+)= pm.product_id
  and pm.product_id = IN_PRODUCT_ID
  and cp.product_id (+)= pm.product_id;

     
END GET_PRODUCT;

PROCEDURE GET_PRODUCT_STATE_EXCLUSIONS
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_STATE_CODE          IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the product state exclusions.  The query can be 
        filtered by the inputs.

Input:
        in_product_id    varchar2
        in_state_code    varchar2

Output:
        out_cur      Cursor containin the product state exclusions

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
        'select pes.product_id,
               pes.excluded_state,
               pes.sun,
               pes.mon,
               pes.tue,
               pes.wed,
               pes.thu,
               pes.fri,
               pes.sat, 
               sm.delivery_exclusion
        from ftd_apps.state_master sm,
             ftd_apps.product_excluded_states pes
        where sm.STATE_MASTER_ID = pes.EXCLUDED_STATE
          and delivery_exclusion = ''Y''';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND pes.product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    IF IN_STATE_CODE IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND pes.excluded_state  = ''' || IN_STATE_CODE  || '''';
    END IF;

    open out_cur for v_main_sql;

END GET_PRODUCT_STATE_EXCLUSIONS;

PROCEDURE GET_VENDORS_WITH_RESTRICTIONS
(
   IN_VENDOR_ID           IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the details of a vendor and it's restrictions needed
        by the product availability service

Input:
        in_product_id     varchar2
        in_delivery_date  date

Output:
        out_cur      Cursor containin the vendor details

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
   select vendor_id,
       substr(cutoff_time,1,2) || substr(cutoff_time,4,2) as cutoff_time,
       (select decode(count(*), 0, null, 'Y') 
        from ftd_apps.vendor_shipping_restrictions vsr 
        where vsr.vendor_id = vm.vendor_id 
          and vsr.START_DATE >= IN_DELIVERY_DATE 
          and vsr.end_date <= IN_DELIVERY_DATE) AS SHIP_RESTRICTION, 
       (select decode(count(*), 0, null, 'Y') 
        from ftd_apps.vendor_delivery_restrictions vsr 
        where vsr.vendor_id = vm.vendor_id 
        and vsr.START_DATE >= IN_DELIVERY_DATE 
        and vsr.end_date <= IN_DELIVERY_DATE) AS DELIVERY_RESTRICTION,
        vendor_type 
   from ftd_apps.vendor_master vm
   where active = 'Y';


END GET_VENDORS_WITH_RESTRICTIONS;

PROCEDURE GET_COUNTRY_PRODUCTS
(
   IN_COUNTRY_ID          IN  VARCHAR2,
   IN_PRODUCT_ID          IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the products for a country.

Input:
        in_country_id    Filter by country_id, optional
        in_product_id    Filter by product_id, optional

Output:
        out_cur      Cursor containin the product state exclusions

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
        'select picx.country_id, pix.product_id
         from ftd_apps.product_index pi,
              ftd_apps.product_index_xref pix,
              ftd_apps.country_master cm,
              ftd_apps.product_index_country_xref picx
         where pix.PRODUCT_INDEX_ID = pi.INDEX_ID
           and cm.status = ''Active''
           and pi.country_id = picx.index_country_id
           and picx.country_id = cm.country_id';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND pix.product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    IF IN_COUNTRY_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND picx.country_id  = ''' || IN_COUNTRY_ID  || '''';
    END IF;

    open out_cur for v_main_sql;


END GET_COUNTRY_PRODUCTS;


PROCEDURE GET_VENDOR_PRODUCTS
(
   IN_VENDOR_ID           IN  VARCHAR2,
   IN_PRODUCT_ID          IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the products and subcodes for a vendor and the available
        ship methods for each.

Input:
        in_vendor_id     Filter by vendor_id, optional
        in_product_id    Filter by product_id, optional

Output:
        out_cur      Cursor containin the vendor products

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
        'select vp.vendor_id, 
                vp.product_subcode_id, 
                null as product_id, 
                (select ''Y'' from ftd_apps.product_ship_methods psm 
	         where ((psm.product_id = vp.product_subcode_id)) 
	         and ship_method_id = ''ND'') as next_day_ship_available,
                (select ''Y'' from ftd_apps.product_ship_methods psm 
	         where ((psm.product_id = vp.product_subcode_id)) 
	         and ship_method_id = ''2F'') as two_day_ship_available,
                (select ''Y'' from ftd_apps.product_ship_methods psm 
	         where ((psm.product_id = vp.product_subcode_id)) 
	         and ship_method_id = ''GR'') as ground_ship_available,
                (select ''Y'' from ftd_apps.product_ship_methods psm 
	         where ((psm.product_id = vp.product_subcode_id)) 
	         and ship_method_id = ''SA'') as saturday_ship_available,
	         v.vendor_type
         from ftd_apps.vendor_product vp,
              ftd_apps.vendor_master v
         where vp.vendor_id = v.vendor_id
           and v.active = ''Y''
           and vp.available = ''Y''';


    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND (vp.product_subcode_id = ''' || IN_PRODUCT_ID || ''') ';
    END IF;

    IF IN_VENDOR_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND v.vendor_id  = ''' || IN_VENDOR_ID  || '''';
    END IF;

    open out_cur for v_main_sql;

END GET_VENDOR_PRODUCTS;


PROCEDURE GET_PAS_COUNTRY
(
   IN_COUNTRY_ID          IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a single row from PAS_COUNTRY

Input:
        in_country_id       country id
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pc.country_id,
       pc.delivery_date,
       pc.cutoff_time,
       pc.addon_days_qty,
       pc.ship_allowed_flag,
       pc.delivery_available_flag,
       pc.transit_allowed_flag,
       hc.deliverable_flag,
       hc.shipping_allowed
from pas_country_dt pc
left outer join ftd_apps.holiday_country hc
on hc.country_id = pc.country_id
and hc.holiday_date = pc.delivery_date
where pc.country_id = IN_COUNTRY_ID
  and pc.delivery_date = IN_DELIVERY_DATE;

END GET_PAS_COUNTRY;

PROCEDURE GET_PAS_VENDOR
(
   IN_VENDOR_ID           IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a single row from PAS_VENDOR

Input:
        in_vendor_id        vendor id (optional)
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pv.vendor_id,
       pv.delivery_date,
       pv.cutoff_time,
       pv.ship_allowed_flag,
       pv.delivery_available_flag,
       vm.vendor_type
from pas_vendor_dt pv, ftd_apps.vendor_master vm
where pv.delivery_date = IN_DELIVERY_DATE
  and pv.vendor_id = IN_VENDOR_ID
  and pv.vendor_id = vm.vendor_id;

END GET_PAS_VENDOR;

PROCEDURE GET_ALL_PAS_VENDOR
(
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a single row from PAS_VENDOR

Input:
        in_vendor_id        vendor id (optional)
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    select pv.vendor_id,
           pv.delivery_date,
           pv.cutoff_time,
           pv.ship_allowed_flag,
           pv.delivery_available_flag,
           vm.vendor_type
    from pas_vendor_dt pv, ftd_apps.vendor_master vm
    where pv.delivery_date = IN_DELIVERY_DATE
    and pv.vendor_id = vm.vendor_id;

END GET_ALL_PAS_VENDOR;

PROCEDURE GET_PAS_PRODUCT
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a single row from PAS_PRODUCT

Input:
        in_product_id       product id
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pp.product_id,
       pp.delivery_date,
       pp.cutoff_time,
       pp.addon_days_qty,
       pp.transit_days_qty,
       pp.ship_allowed_flag,
       pp.ship_restricted_flag,
       pp.delivery_available_flag,
       pp.codification_id,
       pp.product_type,
       pp.ship_method_florist,
       pp.ship_method_carrier 
from pas_product_dt pp
where pp.product_id = IN_PRODUCT_ID
  and pp.delivery_date = IN_DELIVERY_DATE;

END GET_PAS_PRODUCT;

PROCEDURE GET_PAS_PRODUCT_STATE
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_STATE_CODE          IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a single row from PAS_PRODUCT_STATE

Input:
        in_product_id       product id
        in_state_code       state code
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pps.product_id,
       pps.state_code,
       pps.delivery_date,
       pps.delivery_available_flag
from pas_product_state_dt pps
where pps.product_id = IN_PRODUCT_ID
  and pps.state_code = IN_STATE_CODE
  and pps.delivery_date = IN_DELIVERY_DATE;

END GET_PAS_PRODUCT_STATE;

PROCEDURE GET_PAS_VENDOR_PRODUCT_STATE
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_STATE_CODE          IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a cursor of rows from PAS_VENDOR_PRODUCT_STATE

Input:
        in_product_id       product id
        in_state_code       state code
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pvps.product_id,
       pvps.vendor_id,
       pvps.state_code,
       pvps.delivery_date,
       pvps.ship_nd_date,
       pvps.ship_nd_cutoff_time,
       pvps.ship_2d_date,
       pvps.ship_2d_cutoff_time,
       pvps.ship_gr_date,
       pvps.ship_gr_cutoff_time,
       pvps.ship_sat_date,
       pvps.ship_sat_cutoff_time
from pas_vendor_product_state_dt pvps
where pvps.product_id = IN_PRODUCT_ID
  and pvps.state_code = IN_STATE_CODE
  and pvps.delivery_date = IN_DELIVERY_DATE;

END GET_PAS_VENDOR_PRODUCT_STATE;

PROCEDURE GET_PAS_FLORIST_PRODUCT_ZIP
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a cursor of rows from PAS_FLORIST_PRODUCT_ZIP

Input:
        in_product_id       product id
        in_zip_code       zip code
        in_delivery_date    delivery date

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select pfpz.product_id,
       pfpz.zip_code,
       pfpz.delivery_date,
       pfpz.codification_id,
       pfpz.cutoff_time,
       pfpz.addon_days_qty
from pas_florist_product_zip_dt_vw pfpz
where pfpz.product_id = IN_PRODUCT_ID
  and pfpz.zip_code = upper(IN_ZIP_CODE)
  and pfpz.delivery_date = IN_DELIVERY_DATE;

END GET_PAS_FLORIST_PRODUCT_ZIP;

PROCEDURE GET_FLORIST_ZIPS_BY_FLORIST
(
   IN_FLORIST_ID          IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select zip_code
        from ftd_apps.florist_zips
        where florist_id = in_florist_id
        order by zip_code;

END GET_FLORIST_ZIPS_BY_FLORIST;

PROCEDURE GET_FLORIST_ZIPS_BY_ZIP
(
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

v_date     DATE;

BEGIN

    v_date := in_delivery_date;
    if (in_delivery_date = trunc(sysdate)) then
        v_date := sysdate;
    end if;

    OPEN OUT_CUR FOR
        select fz.florist_id, least(nvl(fz.cutoff_time, '2359'), gp.value) cutoff_time,
            ftd_apps.florist_query_pkg.is_florist_open_in_eros(fz.florist_id) open_in_eros
        from ftd_apps.florist_zips fz
        join ftd_apps.florist_master fm
            on fm.florist_id = fz.florist_id
        left outer join ftd_apps.florist_suspends fs
            on fs.florist_id = fz.florist_id
        join frp.global_parms gp
            on gp.context = 'FTDAPPS_PARMS'
            and gp.name = decode(trim(to_char(in_delivery_date, 'DAY')),
                'SATURDAY', 'SATURDAY_CUTOFF', 
                'SUNDAY', 'SUNDAY_CUTOFF',
                'LATEST_CUTOFF')
        left outer join frp.global_parms gp1
        on gp1.context = 'UPDATE_FLORIST_SUSPEND'
        and gp1.name = 'BUFFER_MINUTES'
        where fz.zip_code = in_zip_code
        and fm.florist_id = fz.florist_id
        and fm.status <> 'Inactive'
        and ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fz.florist_id, in_delivery_date) = 'Y'
        and (fz.block_start_date is null
            or not(in_delivery_date between trunc(fz.block_start_date) and nvl(trunc(fz.block_end_date), in_delivery_date)))
        and not exists (select 'Y' from ftd_apps.florist_blocks fb
            where fb.florist_id = fz.florist_id
            and in_delivery_date between trunc(fb.block_start_date) and nvl(trunc(fb.block_end_date), in_delivery_date))
        and (fs.suspend_start_date is null
            or ((not(v_date between fs.suspend_start_date - (gp1.value/1440) and nvl(fs.suspend_end_date, in_delivery_date))
                and not(sysdate between fs.suspend_start_date - (gp1.value/1440) and nvl(fs.suspend_end_date, in_delivery_date)))
            or (fs.suspend_type <> 'G' and fm.super_florist_flag ='Y')) )
        order by open_in_eros desc, fz.cutoff_time desc;

END GET_FLORIST_ZIPS_BY_ZIP;

PROCEDURE GET_ALL_FLORIST_ZIPS
(
   IN_ZIP_CODE            IN  VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

v_main_sql VARCHAR2(3000);

BEGIN

    v_main_sql := 
        'select distinct fz.zip_code
        from ftd_apps.florist_zips fz, ftd_apps.florist_master fm
        where fm.status <> ''Inactive''
        and fm.florist_id = fz.florist_id';

    IF IN_ZIP_CODE IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND fz.zip_code = ''' || IN_ZIP_CODE || '''';
    END IF;

    v_main_sql := v_main_sql || ' ORDER BY fz.zip_code';

    open out_cur for v_main_sql;

END GET_ALL_FLORIST_ZIPS;

PROCEDURE IS_PRODUCT_LIST_AVAILABLE
(
   IN_ZIP_CODE            IN VARCHAR2,
   IN_DELIVERY_DATE       IN VARCHAR2,
   IN_PRODUCT_LIST        IN VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

v_main_sql VARCHAR2(3000);

BEGIN

    v_main_sql := '
      select product_id
      from pas.pas_florist_product_zip_dt_vw
      where product_id in (' || IN_PRODUCT_LIST || ')
        and zip_code = ''' || IN_ZIP_CODE || '''
        and to_char(delivery_date, ''MM/DD/YYYY'') = ''' || IN_DELIVERY_DATE || '''
        and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = ''N''
      UNION
      select product_id 
      from pas.pas_vendor_product_zip_dt_vw
      where 1=1
      and product_id in (' || IN_PRODUCT_LIST || ')
      and zip_code_id = ''' || IN_ZIP_CODE || '''
      and to_char(delivery_date, ''MM/DD/YYYY'') = ''' || IN_DELIVERY_DATE || '''
      and delivery_date > trunc(sysdate)
      and (
           (
            ((ship_nd_date - trunc(sysdate)) > 0) or 
            (((ship_nd_date - trunc(sysdate)) = 0) and 
             (ship_nd_cutoff_time > to_char(sysdate,''HH24mi''))
            )
           )
           OR
           (
            ((ship_2d_date - trunc(sysdate)) > 0) or 
            (((ship_2d_date - trunc(sysdate)) = 0) and 
             (ship_2d_cutoff_time > to_char(sysdate,''HH24mi''))
            )
           )
           OR
           (
            ((ship_gr_date - trunc(sysdate)) > 0) or 
            (((ship_gr_date - trunc(sysdate)) = 0) and 
             (ship_gr_cutoff_time > to_char(sysdate,''HH24mi''))
            )
           )
           OR
           (
            ((ship_sat_date - trunc(sysdate)) > 0) or 
            (((ship_sat_date - trunc(sysdate)) = 0) and 
             (ship_sat_cutoff_time > to_char(sysdate,''HH24mi''))
            )
           )
          )
      order by product_id';

    open out_cur for v_main_sql;

END IS_PRODUCT_LIST_AVAILABLE;

PROCEDURE IS_INT_PRODUCT_LIST_AVAILABLE
(
   IN_COUNTRY_CODE        IN VARCHAR2,
   IN_DELIVERY_DATE       IN VARCHAR2,
   IN_PRODUCT_LIST        IN VARCHAR2,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

v_main_sql VARCHAR2(3000);

BEGIN

    v_main_sql := '
      select product_id
      from pas.pas_country_product_dt
      where product_id in (' || IN_PRODUCT_LIST || ')
        and country_id = ''' || IN_COUNTRY_CODE || '''
        and to_char(delivery_date, ''MM/DD/YYYY'') = ''' || IN_DELIVERY_DATE || '''
        and (
             ((cutoff_date - trunc(sysdate)) > 0) or 
             (((cutoff_date - trunc(sysdate)) = 0) and 
              (cutoff_time > to_char(sysdate, ''HH24mi'')   
              )
             )
            )
      order by product_id';

    open out_cur for v_main_sql;

END IS_INT_PRODUCT_LIST_AVAILABLE;

PROCEDURE GET_ALL_AVAILABLE_DATES
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_MAX_DELIVERY_DATE   IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select delivery_date,
            (delivery_date - addon_days_qty) florist_date,
            to_date(null) ship_nd_date,
            to_date(null) ship_2d_date,
            to_date(null) ship_gr_date,
            to_date(null) ship_sa_date
        from pas.pas_florist_product_zip_dt_vw
        where product_id = in_product_id
        and zip_code = upper(in_zip_code)
        and delivery_date <= in_max_delivery_date
        and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
    union
        select delivery_date,
            to_date(null) florist_date,
            case when ((ship_nd_date - trunc(sysdate)) > 0) or
                (((ship_nd_date - trunc(sysdate)) = 0) and
                (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
                )
                then ship_nd_date
                else to_date(null)
            end as ship_nd_date,
            case when ((ship_2d_date - trunc(sysdate)) > 0) or
                (((ship_2d_date - trunc(sysdate)) = 0) and
                (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
                )
                then ship_2d_date
                else to_date(null)
            end as ship_2d_date,
            case when ((ship_gr_date - trunc(sysdate)) > 0) or
                (((ship_gr_date - trunc(sysdate)) = 0) and
                (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
                )
                then ship_gr_date
                else to_date(null)
            end as ship_gr_date,
            case when ((ship_sat_date - trunc(sysdate)) > 0) or
                (((ship_sat_date - trunc(sysdate)) = 0) and
                (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
                )
                then ship_sat_date
                else to_date(null)
            end as ship_sa_date
        from pas.pas_vendor_product_zip_dt_vw
        where product_id = in_product_id
        and zip_code_id = upper(in_zip_code)
        and delivery_date <= in_max_delivery_date
        and delivery_date > trunc(sysdate)
        and (
            (
                ((ship_nd_date - trunc(sysdate)) > 0) or
                (((ship_nd_date - trunc(sysdate)) = 0) and
                (ship_nd_cutoff_time > to_char(sysdate,'HH24mi'))
            )
            )
            OR
            (
                ((ship_2d_date - trunc(sysdate)) > 0) or
                (((ship_2d_date - trunc(sysdate)) = 0) and
                (ship_2d_cutoff_time > to_char(sysdate,'HH24mi'))
            )
            )
            OR
            (
                ((ship_gr_date - trunc(sysdate)) > 0) or
                (((ship_gr_date - trunc(sysdate)) = 0) and
                (ship_gr_cutoff_time > to_char(sysdate,'HH24mi'))
            )
            )
            OR
            (
                ((ship_sat_date - trunc(sysdate)) > 0) or
                (((ship_sat_date - trunc(sysdate)) = 0) and
                (ship_sat_cutoff_time > to_char(sysdate,'HH24mi'))
            )
            )
        )
        order by delivery_date, florist_date;

END GET_ALL_AVAILABLE_DATES;

PROCEDURE GET_ALL_INT_AVAILABLE_DATES 
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_COUNTRY_CODE        IN  VARCHAR2,
   IN_MAX_DELIVERY_DATE   IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN
     
    OPEN OUT_CUR for
        select delivery_date,
            cutoff_date florist_cutoff_date
        from pas_country_product_dt
        where product_id = IN_PRODUCT_ID
        and country_id = IN_COUNTRY_CODE
        and delivery_available_flag = 'Y'
        and delivery_date <= trunc(IN_MAX_DELIVERY_DATE)
        and (
            ((cutoff_date - trunc(sysdate)) > 0) or 
            (((cutoff_date - trunc(sysdate)) = 0) and 
            (cutoff_time > to_char(sysdate,'HH24mi'))
            )
        )
        order by delivery_date;

END GET_ALL_INT_AVAILABLE_DATES;

PROCEDURE CHECK_VENDOR_SHUTDOWN
(
IN_PRODUCT_ID       IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_VENDOR_ID        IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
IN_SHIP_DATE        IN VENUS.INV_TRK_ASSIGNED_COUNT.SHIP_DATE%TYPE,
OUT_SHUTDOWN_LVL_REACHED  OUT VARCHAR2,
OUT_SHUTDOWN_TYPE   OUT VARCHAR2,
OUT_STATUS          OUT VARCHAR2,
OUT_MESSAGE         OUT VARCHAR2
)
AS
/*----------------------------------------------------------------------
Input:
IN_PRODUCT_ID  IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_VENDOR_ID   IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
IN_SHIP_DATE   IN VENUS.INV_TRK_ASSIGNED_COUNT.SHIP_DATE%TYPE

Output:
  OUT_SHUTDOWN_LVL_REACHED  OUT VARCHAR2,
  OUT_SHUTDOWN_TYPE         OUT VARCHAR2,
  OUT_STATUS                OUT VARCHAR2,
  OUT_MESSAGE               OUT VARCHAR2

-----------------------------------------------------------------------*/


v_shutdown_threshold_qty     FTD_APPS.INV_TRK.SHUTDOWN_THRESHOLD_QTY%TYPE;
v_revised_forecast_qty       FTD_APPS.INV_TRK.REVISED_FORECAST_QTY%TYPE;
v_start_date                 FTD_APPS.INV_TRK.START_DATE%TYPE;
v_end_date                   FTD_APPS.INV_TRK.END_DATE%TYPE;
v_ftd_msg_count              VENUS.INV_TRK_ASSIGNED_COUNT.FTD_MSG_COUNT%TYPE;
v_can_msg_count              VENUS.INV_TRK_ASSIGNED_COUNT.CAN_MSG_COUNT%TYPE;
v_rej_msg_count              VENUS.INV_TRK_ASSIGNED_COUNT.REJ_MSG_COUNT%TYPE;
v_inventory_count            number;
v_msg_id                     number;
v_msg_status                 varchar2(1);
v_msg_message                varchar2(256);


BEGIN

  OUT_SHUTDOWN_LVL_REACHED  := 'N';
  OUT_SHUTDOWN_TYPE := null;
  OUT_STATUS := 'N';

  BEGIN

    SELECT shutdown_threshold_qty, start_date, end_date, revised_forecast_qty
    INTO   v_shutdown_threshold_qty, v_start_date, v_end_date, v_revised_forecast_qty
    FROM   ftd_apps.inv_trk it,
           ftd_apps.product_master pm,
           ftd_apps.vendor_product vp
    WHERE  it.vendor_id = IN_VENDOR_ID
    AND    it.product_id = IN_PRODUCT_ID
    AND    IN_SHIP_DATE between it.start_date and it.end_date
    AND    it.status not in ('N', 'U', 'X')
    AND    pm.product_id = it.product_id
    AND    pm.status = 'A'
    AND    vp.vendor_id = it.vendor_id
    AND    vp.product_subcode_id = pm.product_id
    AND    vp.available = 'Y';

    EXCEPTION WHEN NO_DATA_FOUND THEN
      OUT_SHUTDOWN_LVL_REACHED := 'Y';
      OUT_SHUTDOWN_TYPE := 'NO INVENTORY RECORD FOUND';

  END;

  IF OUT_SHUTDOWN_LVL_REACHED = 'N' THEN

    BEGIN

      SELECT SUM(itac.ftd_msg_count), SUM(itac.can_msg_count), SUM(itac.rej_msg_count)
      INTO   v_ftd_msg_count, v_can_msg_count, v_rej_msg_count
      FROM   venus.inv_trk_assigned_count itac
      WHERE  itac.vendor_id = IN_VENDOR_ID
      AND    itac.product_id = IN_PRODUCT_ID
      AND    itac.ship_date between v_start_date and v_end_date;

    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_ftd_msg_count := 0;
      v_can_msg_count := 0;
      v_rej_msg_count := 0;

    END;

    v_inventory_count := v_revised_forecast_qty - v_ftd_msg_count + v_can_msg_count + v_rej_msg_count;

    --dbms_output.put_line('v_inventory_count: ' || v_inventory_count || '  v_shutdown_threshold_qty: ' || v_shutdown_threshold_qty);

    IF (v_inventory_count <= v_shutdown_threshold_qty) THEN
      OUT_SHUTDOWN_LVL_REACHED := 'Y';
      OUT_SHUTDOWN_TYPE := 'VENDOR';
    ELSE
      CHECK_PRODUCT_SHUTDOWN(IN_PRODUCT_ID, IN_SHIP_DATE, OUT_SHUTDOWN_LVL_REACHED, OUT_STATUS, OUT_MESSAGE);
      IF OUT_SHUTDOWN_LVL_REACHED = 'Y' THEN
        OUT_SHUTDOWN_TYPE := 'PRODUCT';
      END IF;
    END IF;

  END IF;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED IN CHECK_VENDOR_SHUTDOWN [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CHECK_VENDOR_SHUTDOWN;

PROCEDURE CHECK_PRODUCT_SHUTDOWN
(
IN_PRODUCT_ID  			IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_SHIP_DATE   			IN VENUS.INV_TRK_COUNT.SHIP_DATE%TYPE,
OUT_SHUTDOWN_LVL_REACHED 	OUT VARCHAR2,
OUT_STATUS     			OUT VARCHAR2,
OUT_MESSAGE    			OUT VARCHAR2
) AS

v_shutdown_threshold_qty     FTD_APPS.INV_TRK.SHUTDOWN_THRESHOLD_QTY%TYPE;
v_revised_forecast_qty       FTD_APPS.INV_TRK.REVISED_FORECAST_QTY%TYPE;
v_start_date                 FTD_APPS.INV_TRK.START_DATE%TYPE;
v_end_date                   FTD_APPS.INV_TRK.END_DATE%TYPE;
v_ftd_msg_count              VENUS.INV_TRK_COUNT.FTD_MSG_COUNT%TYPE;
v_can_msg_count              VENUS.INV_TRK_COUNT.CAN_MSG_COUNT%TYPE;
v_rej_msg_count              VENUS.INV_TRK_COUNT.REJ_MSG_COUNT%TYPE;
v_inventory_count            number;
v_ftd_msg_count_na           VENUS.INV_TRK_COUNT.FTD_MSG_COUNT%TYPE;
v_can_msg_count_na           VENUS.INV_TRK_COUNT.CAN_MSG_COUNT%TYPE;
v_rej_msg_count_na           VENUS.INV_TRK_COUNT.REJ_MSG_COUNT%TYPE;

BEGIN

  OUT_SHUTDOWN_LVL_REACHED  := 'N';

  BEGIN

    SELECT sum(shutdown_threshold_qty), sum(revised_forecast_qty), min(start_date), max(end_date)
    INTO   v_shutdown_threshold_qty, v_revised_forecast_qty, v_start_date, v_end_date
    FROM   ftd_apps.inv_trk it,
           ftd_apps.product_master pm,
           ftd_apps.vendor_product vp
    WHERE  it.product_id = IN_PRODUCT_ID
    AND    IN_SHIP_DATE between it.start_date and it.end_date
    AND    it.status not in ('N', 'U', 'X')
    AND    pm.product_id = it.product_id
    AND    pm.status = 'A'
    AND    vp.vendor_id = it.vendor_id
    AND    vp.product_subcode_id = pm.product_id
    AND    vp.available = 'Y';

    EXCEPTION WHEN NO_DATA_FOUND THEN
      OUT_SHUTDOWN_LVL_REACHED := 'Y';

  END;

  IF OUT_SHUTDOWN_LVL_REACHED = 'N' THEN

    BEGIN

      SELECT SUM(itc.ftd_msg_count), SUM(itc.can_msg_count), SUM(itc.rej_msg_count)
      INTO   v_ftd_msg_count, v_can_msg_count, v_rej_msg_count
      FROM   venus.inv_trk_count itc
      WHERE  itc.product_id = IN_PRODUCT_ID
      AND    itc.ship_date between v_start_date and v_end_date;

      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_ftd_msg_count := 0;
        v_can_msg_count := 0;
        v_rej_msg_count := 0;

    END;

    BEGIN

      SELECT NVL(SUM(itac.ftd_msg_count), 0), NVL(SUM(itac.can_msg_count), 0), NVL(SUM(itac.rej_msg_count), 0)
      INTO   v_ftd_msg_count_na, v_can_msg_count_na, v_rej_msg_count_na
      FROM   venus.inv_trk_assigned_count itac,
             ftd_apps.product_master pm,
             ftd_apps.vendor_master vm,
             ftd_apps.inv_trk it,
             ftd_apps.vendor_product vp
      WHERE  itac.product_id = IN_PRODUCT_ID
      AND    itac.ship_date between v_start_date and v_end_date
      AND    itac.vendor_id = vm.vendor_id
      AND    itac.product_id = pm.product_id
      AND    it.product_id = pm.product_id
      AND    it.vendor_id = vm.vendor_id
      AND    it.start_date = v_start_date
      AND    it.end_date = v_end_date
      AND    vp.vendor_id = it.vendor_id
      AND    vp.product_subcode_id = pm.product_id
      AND    (it.status in ('N', 'U', 'X')
             OR pm.status = 'U'
             OR vm.active = 'N'
             or vp.available = 'N');

      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_ftd_msg_count_na := 0;
        v_can_msg_count_na := 0;
        v_rej_msg_count_na := 0;

    END;

    v_ftd_msg_count := v_ftd_msg_count - v_ftd_msg_count_na;
    v_can_msg_count := v_can_msg_count - v_can_msg_count_na;
    v_rej_msg_count := v_rej_msg_count - v_rej_msg_count_na;

    v_inventory_count := v_revised_forecast_qty - v_ftd_msg_count + v_can_msg_count + v_rej_msg_count;

    --dbms_output.put_line('v_inventory_count: ' || v_inventory_count || '  v_shutdown_threshold_qty: ' || v_shutdown_threshold_qty);

    IF (v_inventory_count <= v_shutdown_threshold_qty) THEN
      OUT_SHUTDOWN_LVL_REACHED  := 'Y';
    END IF;

  END IF;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED IN CHECK_PRODUCT_SHUTDOWN [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CHECK_PRODUCT_SHUTDOWN;

PROCEDURE GET_ALL_FLORIST_DATES
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_MAX_DELIVERY_DATE   IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select delivery_date,
            (delivery_date - addon_days_qty) florist_date,
            to_date(null) ship_nd_date,
            to_date(null) ship_2d_date,
            to_date(null) ship_gr_date,
            to_date(null) ship_sa_date
        from pas.pas_florist_product_zip_dt_vw
        where product_id = in_product_id
        and zip_code = upper(in_zip_code)
        and delivery_date <= in_max_delivery_date
        and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
        order by delivery_date, florist_date;

END GET_ALL_FLORIST_DATES;

PROCEDURE GET_PRODUCT_AVAIL_FLORIST 
(
   IN_PRODUCT_ID          IN  VARCHAR2,
   IN_ZIP_CODE            IN  VARCHAR2,
   IN_DELIVERY_DATE       IN  DATE,
   OUT_CUR                OUT TYPES.REF_CURSOR
) AS

CURSOR ship_method_cursor IS 
  select ship_method_florist, ship_method_carrier 
  from pas_product_dt
  where product_id = IN_PRODUCT_ID
  and delivery_date = trunc(IN_DELIVERY_DATE);


CURSOR florist_cursor IS
  select cutoff_time,
         (delivery_date - addon_days_qty) as cutoff_date
  from pas_florist_product_zip_dt_vw
  where product_id = IN_PRODUCT_ID
    and zip_code = upper(IN_ZIP_CODE)
    and delivery_date = trunc(IN_DELIVERY_DATE)
    and get_cutoff(delivery_date, addon_days_qty, cutoff_time) = 'N'
  order by cutoff_date, cutoff_time desc;


v_ship_method_florist pas_product_dt.ship_method_florist%type;
v_ship_method_carrier pas_product_dt.ship_method_carrier%type;
v_florist_cutoff_time pas_florist_product_zip_dt_vw.cutoff_time%type;
v_florist_cutoff_date date;

BEGIN
     
   OPEN ship_method_cursor;
   FETCH ship_method_cursor INTO v_ship_method_florist, v_ship_method_carrier;
   CLOSE ship_method_cursor;
     
   OPEN florist_cursor;
   FETCH florist_cursor INTO v_florist_cutoff_time, v_florist_cutoff_date;
   CLOSE florist_cursor;

   OPEN OUT_CUR for
     select 
       v_florist_cutoff_time as florist_cutoff_time,
       v_florist_cutoff_date as florist_cutoff_date,
       v_ship_method_florist as ship_method_florist,
       v_ship_method_carrier as ship_method_carrier 
     from dual;

END GET_PRODUCT_AVAIL_FLORIST;

END PAS_QUERY_PKG;
/