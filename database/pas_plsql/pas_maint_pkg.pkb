CREATE OR REPLACE 
PACKAGE BODY PAS.PAS_MAINT_PKG AS

PROCEDURE INSERT_PAS_COUNTRY_DT
(
    IN_COUNTRY_ID               IN PAS_COUNTRY_DT.COUNTRY_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_COUNTRY_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_COUNTRY_DT.CUTOFF_TIME%TYPE,
    IN_ADDON_DAYS_QTY           IN PAS_COUNTRY_DT.ADDON_DAYS_QTY%TYPE,
    IN_SHIP_ALLOWED_FLAG        IN PAS_COUNTRY_DT.SHIP_ALLOWED_FLAG%TYPE,
    IN_DELIVERY_AVAILABLE_FLAG  IN PAS_COUNTRY_DT.COUNTRY_ID%TYPE,
    IN_TRANSIT_ALLOWED_FLAG     IN PAS_COUNTRY_DT.TRANSIT_ALLOWED_FLAG%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_COUNTRY_DT table

Input:
    IN_COUNTRY_ID               Country id
    IN_DELIVERY_DATE            Delivery DAte
    IN_CUTOFF_TIME              Cutoff time for the delivery date
    IN_ADDON_DAYS_QTY           Addon days for the country
    IN_SHIP_ALLOWED_FLAG        Is shipping allowed on the date
    IN_DELIVERY_AVAILABLE_FLAG  Is delivery allowed on the date
    IN_TRANSIT_ALLOWED_FLAG     Is this a valid transit day

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_COUNTRY_DT
    where  country_id = IN_COUNTRY_ID
    and    delivery_date = IN_DELIVERY_DATE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_COUNTRY_DT 
    SET 
      CUTOFF_TIME = IN_CUTOFF_TIME,
      ADDON_DAYS_QTY = IN_ADDON_DAYS_QTY,
      SHIP_ALLOWED_FLAG = IN_SHIP_ALLOWED_FLAG,
      DELIVERY_AVAILABLE_FLAG = IN_DELIVERY_AVAILABLE_FLAG,
      TRANSIT_ALLOWED_FLAG = IN_TRANSIT_ALLOWED_FLAG

    WHERE  country_id = IN_COUNTRY_ID
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_COUNTRY_DT (
      COUNTRY_ID,
      DELIVERY_DATE,
      CUTOFF_TIME,
      ADDON_DAYS_QTY,
      SHIP_ALLOWED_FLAG,
      DELIVERY_AVAILABLE_FLAG,
      TRANSIT_ALLOWED_FLAG
    ) VALUES (
      IN_COUNTRY_ID,
      IN_DELIVERY_DATE,
      IN_CUTOFF_TIME,
      IN_ADDON_DAYS_QTY,
      IN_SHIP_ALLOWED_FLAG,
      IN_DELIVERY_AVAILABLE_FLAG,
      IN_TRANSIT_ALLOWED_FLAG
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_COUNTRY_DT;

PROCEDURE INSERT_PAS_PRODUCT_DT
(
    IN_PRODUCT_ID               IN PAS_PRODUCT_DT.PRODUCT_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_PRODUCT_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_PRODUCT_DT.CUTOFF_TIME%TYPE,
    IN_ADDON_DAYS_QTY           IN PAS_PRODUCT_DT.ADDON_DAYS_QTY%TYPE,
    IN_TRANSIT_DAYS_QTY         IN PAS_PRODUCT_DT.TRANSIT_DAYS_QTY%TYPE,
    IN_SHIP_ALLOWED_FLAG        IN PAS_PRODUCT_DT.SHIP_ALLOWED_FLAG%TYPE,
    IN_SHIP_RESTRICTED_FLAG     IN PAS_PRODUCT_DT.SHIP_RESTRICTED_FLAG%TYPE,
    IN_DELIVERY_AVAILABLE_FLAG  IN PAS_PRODUCT_DT.DELIVERY_AVAILABLE_FLAG%TYPE,
    IN_CODIFICATION_ID          IN PAS_PRODUCT_DT.CODIFICATION_ID%TYPE,
    IN_PRODUCT_TYPE             IN PAS_PRODUCT_DT.PRODUCT_TYPE%TYPE,
    IN_SHIP_METHOD_FLORIST      IN PAS_PRODUCT_DT.SHIP_METHOD_FLORIST%TYPE,
    IN_SHIP_METHOD_CARRIER      IN PAS_PRODUCT_DT.SHIP_METHOD_CARRIER%TYPE, 

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_PRODUCT_DT table

Input:
    IN_PRODUCT_ID               Product Id
    IN_DELIVERY_DATE            Delivery DAte
    IN_CUTOFF_TIME              Cutoff time for the delivery date
    IN_ADDON_DAYS_QTY           Addon days for the country
    IN_TRANSIT_DAYS_QTY         Additional Transit Days
    IN_SHIP_ALLOWED_FLAG        Is shipping allowed on the date
    IN_SHIP_RESTRICTED_FLAG     Is shipping restricted on the date
    IN_DELIVERY_AVAILABLE_FLAG  Is delivery allowed on the date
    IN_CODIFICATION_ID          Codification Id
    IN_PRODUCT_TYPE             Product Type
    IN_SHIP_METHOD_FLORIST      Y if florist deliverable      
    IN_SHIP_METHOD_CARRIER      Y if carrier deliverable

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_PRODUCT_DT
    where  product_id = IN_PRODUCT_ID
    and    delivery_date = IN_DELIVERY_DATE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_PRODUCT_DT 
    SET 
      CUTOFF_TIME = IN_CUTOFF_TIME,
      ADDON_DAYS_QTY = IN_ADDON_DAYS_QTY,
      SHIP_ALLOWED_FLAG = IN_SHIP_ALLOWED_FLAG,
      DELIVERY_AVAILABLE_FLAG = IN_DELIVERY_AVAILABLE_FLAG,
      TRANSIT_DAYS_QTY = IN_TRANSIT_DAYS_QTY,
      SHIP_RESTRICTED_FLAG = IN_SHIP_RESTRICTED_FLAG,
      CODIFICATION_ID = IN_CODIFICATION_ID,
      PRODUCT_TYPE = IN_PRODUCT_TYPE, 
      SHIP_METHOD_FLORIST = IN_SHIP_METHOD_FLORIST, 
      SHIP_METHOD_CARRIER = IN_SHIP_METHOD_CARRIER
    WHERE  product_id = IN_PRODUCT_ID
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_PRODUCT_DT (
      PRODUCT_ID,
      DELIVERY_DATE,
      CUTOFF_TIME,
      ADDON_DAYS_QTY,
      TRANSIT_DAYS_QTY,
      SHIP_ALLOWED_FLAG,
      SHIP_RESTRICTED_FLAG,
      DELIVERY_AVAILABLE_FLAG,
      CODIFICATION_ID,
      PRODUCT_TYPE, 
      SHIP_METHOD_FLORIST, 
      SHIP_METHOD_CARRIER 
    ) VALUES (
      IN_PRODUCT_ID,
      IN_DELIVERY_DATE,
      IN_CUTOFF_TIME,
      IN_ADDON_DAYS_QTY,
      IN_TRANSIT_DAYS_QTY,
      IN_SHIP_ALLOWED_FLAG,
      IN_SHIP_RESTRICTED_FLAG,
      IN_DELIVERY_AVAILABLE_FLAG,
      IN_CODIFICATION_ID,
      IN_PRODUCT_TYPE, 
      IN_SHIP_METHOD_FLORIST, 
      IN_SHIP_METHOD_CARRIER 
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_PRODUCT_DT;

PROCEDURE INSERT_PAS_PRODUCT_STATE_DT
(
    IN_PRODUCT_ID               IN PAS_PRODUCT_STATE_DT.PRODUCT_ID%TYPE,
    IN_STATE_CODE               IN PAS_PRODUCT_STATE_DT.STATE_CODE%TYPE,
    IN_DELIVERY_DATE            IN PAS_PRODUCT_STATE_DT.DELIVERY_DATE%TYPE,
    IN_DELIVERY_AVAILABLE_FLAG  IN PAS_PRODUCT_STATE_DT.DELIVERY_AVAILABLE_FLAG%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_PRODUCT_STATE_DT table

Input:
    IN_PRODUCT_ID               Product Id
    IN_STATE_CODE               State Code
    IN_DELIVERY_DATE            Delivery DAte
    IN_DELIVERY_AVAILABLE_FLAG  Is delivery allowed on the date

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_PRODUCT_STATE_DT
    where  product_id = IN_PRODUCT_ID
    and    delivery_date = IN_DELIVERY_DATE
    and    state_code = IN_STATE_CODE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_PRODUCT_STATE_DT 
    SET 
      DELIVERY_AVAILABLE_FLAG = IN_DELIVERY_AVAILABLE_FLAG
    WHERE  product_id = IN_PRODUCT_ID
    AND    state_code = IN_STATE_CODE
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_PRODUCT_STATE_DT (
      PRODUCT_ID,
      DELIVERY_DATE,
      STATE_CODE,
      DELIVERY_AVAILABLE_FLAG
    ) VALUES (
      IN_PRODUCT_ID,
      IN_DELIVERY_DATE,
      IN_STATE_CODE,
      IN_DELIVERY_AVAILABLE_FLAG
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_PRODUCT_STATE_DT;


PROCEDURE INSERT_PAS_VENDOR_DT
(
    IN_VENDOR_ID                IN PAS_VENDOR_DT.VENDOR_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_VENDOR_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_VENDOR_DT.CUTOFF_TIME%TYPE,
    IN_SHIP_ALLOWED_FLAG        IN PAS_VENDOR_DT.SHIP_ALLOWED_FLAG%TYPE,
    IN_DELIVERY_AVAILABLE_FLAG  IN PAS_VENDOR_DT.DELIVERY_AVAILABLE_FLAG%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_VENDOR_DT table

Input:
    IN_VENDOR_ID                Vendor Id
    IN_DELIVERY_DATE            Delivery DAte
    IN_CUTOFF_TIME              Cutoff Time
    IN_SHIP_ALLOWED_FLAG        Is shipping allowed
    IN_DELIVERY_AVAILABLE_FLAG  Is delivery allowed on the date

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_VENDOR_DT
    where  vendor_id = IN_VENDOR_ID
    and    delivery_date = IN_DELIVERY_DATE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_VENDOR_DT 
    SET 
      CUTOFF_TIME = IN_CUTOFF_TIME,
      SHIP_ALLOWED_FLAG = IN_SHIP_ALLOWED_FLAG,
      DELIVERY_AVAILABLE_FLAG = IN_DELIVERY_AVAILABLE_FLAG
    WHERE  vendor_id = IN_VENDOR_ID
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_VENDOR_DT (
       VENDOR_ID,
       DELIVERY_DATE,
       CUTOFF_TIME,
       SHIP_ALLOWED_FLAG,
       DELIVERY_AVAILABLE_FLAG
    ) VALUES (
       IN_VENDOR_ID,
       IN_DELIVERY_DATE,
       IN_CUTOFF_TIME,
       IN_SHIP_ALLOWED_FLAG,
       IN_DELIVERY_AVAILABLE_FLAG
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_VENDOR_DT;

PROCEDURE INSERT_PAS_STATE_DT
(
    IN_STATE_CODE               IN PAS_STATE_DT.STATE_CODE%TYPE,
    IN_DELIVERY_DATE            IN PAS_STATE_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_STATE_DT.CUTOFF_TIME%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_STATE_DT table

Input:
    IN_STATE_CODE               State Code
    IN_DELIVERY_DATE            Delivery DAte
    IN_CUTOFF_TIME              Cutoff Time

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_STATE_DT
    where  delivery_date = IN_DELIVERY_DATE
    and    state_code = IN_STATE_CODE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_STATE_DT 
    SET 
      CUTOFF_TIME = IN_CUTOFF_TIME
    WHERE  state_code = IN_STATE_CODE
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_STATE_DT (
      STATE_CODE,
      DELIVERY_DATE,
      CUTOFF_TIME
    ) VALUES (
      IN_STATE_CODE,
      IN_DELIVERY_DATE,
      IN_CUTOFF_TIME
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_STATE_DT;

PROCEDURE INSERT_PAS_FLORIST_DT
(
    IN_FLORIST_ID               IN PAS_FLORIST_DT.FLORIST_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_FLORIST_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_FLORIST_DT.CUTOFF_TIME%TYPE,
    IN_STATUS_CODE              IN PAS_FLORIST_DT.STATUS_CODE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_FLORIST_DT table

Input:
    IN_FLORIST_ID               Florist Id
    IN_DELIVERY_DATE            Delivery DAte
    IN_CUTOFF_TIME              Cutoff Time
    IN_STATUS_CODE              Status Code

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_FLORIST_DT
    where  florist_id = IN_FLORIST_ID
    and    delivery_date = IN_DELIVERY_DATE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_FLORIST_DT 
    SET 
      CUTOFF_TIME = IN_CUTOFF_TIME,
      STATUS_CODE = IN_STATUS_CODE
    WHERE  florist_id = IN_FLORIST_ID
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_FLORIST_DT (
      FLORIST_ID,
      DELIVERY_DATE,
      CUTOFF_TIME,
      STATUS_CODE
    ) VALUES (
      IN_FLORIST_ID,
      IN_DELIVERY_DATE,
      IN_CUTOFF_TIME,
      IN_STATUS_CODE
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_FLORIST_DT;

PROCEDURE INSERT_PAS_VENDOR_PROD_ST_DT
(
    IN_VENDOR_ID               IN PAS_VENDOR_PRODUCT_STATE_DT.VENDOR_ID%TYPE,
    IN_PRODUCT_ID              IN PAS_VENDOR_PRODUCT_STATE_DT.PRODUCT_ID%TYPE,
    IN_STATE_CODE              IN PAS_VENDOR_PRODUCT_STATE_DT.STATE_CODE%TYPE,
    IN_DELIVERY_DATE           IN PAS_VENDOR_PRODUCT_STATE_DT.DELIVERY_DATE%TYPE,
    IN_SHIP_ND_DATE            IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_ND_DATE%TYPE,
    IN_SHIP_2D_DATE            IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_2D_DATE%TYPE,
    IN_SHIP_GR_DATE            IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_GR_DATE%TYPE,
    IN_SHIP_SAT_DATE           IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_SAT_DATE%TYPE,
    IN_SHIP_ND_CUTOFF_TIME     IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_ND_CUTOFF_TIME%TYPE,
    IN_SHIP_2D_CUTOFF_TIME     IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_2D_CUTOFF_TIME%TYPE,
    IN_SHIP_GR_CUTOFF_TIME     IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_GR_CUTOFF_TIME%TYPE,
    IN_SHIP_SAT_CUTOFF_TIME    IN PAS_VENDOR_PRODUCT_STATE_DT.SHIP_SAT_CUTOFF_TIME%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_VENDOR_PRODUCT_STATE_DT table

Input:
    IN_VENDOR_ID                Vendor Id
    IN_PRODUCT_ID               Product Id
    IN_STATE_CODE               State Code
    IN_DELIVERY_DATE            Delivery DAte
    IN_SHIP_ND_DATE             Ship Date for next day
    IN_SHIP_2D_DATE             Ship date for two day
    IN_SHIP_GR_DATE             Ship Date for Ground
    IN_SHIP_SAT_DATE            Ship Date for Saturday delivery
    IN_SHIP_ND_CUTOFF_TIME      Cutoff time on next day ship date
    IN_SHIP_2D_CUTOFF_TIME      Cutoff time on two day ship date
    IN_SHIP_GR_CUTOFF_TIME      Cutoff time on Ground ship date
    IN_SHIP_SAT_CUTOFF_TIME     Cutoff time on Saturday delivery ship date

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_VENDOR_PRODUCT_STATE_DT
    where  product_id = IN_PRODUCT_ID
    and    delivery_date = IN_DELIVERY_DATE
    and    vendor_id = IN_VENDOR_ID
    and    state_code = IN_STATE_CODE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_VENDOR_PRODUCT_STATE_DT 
    SET 
      SHIP_ND_DATE = IN_SHIP_ND_DATE,
      SHIP_2D_DATE = IN_SHIP_2D_DATE,
      SHIP_GR_DATE = IN_SHIP_GR_DATE,
      SHIP_SAT_DATE = IN_SHIP_SAT_DATE,
      SHIP_ND_CUTOFF_TIME = IN_SHIP_ND_CUTOFF_TIME,
      SHIP_2D_CUTOFF_TIME = IN_SHIP_2D_CUTOFF_TIME,
      SHIP_GR_CUTOFF_TIME = IN_SHIP_GR_CUTOFF_TIME,
      SHIP_SAT_CUTOFF_TIME = IN_SHIP_SAT_CUTOFF_TIME,
      UPDATED_ON = SYSDATE
    WHERE  product_id = IN_PRODUCT_ID
    AND    vendor_id = IN_VENDOR_ID
    AND    state_code = IN_STATE_CODE
    AND    delivery_date = IN_DELIVERY_DATE
    AND	((SHIP_ND_DATE is null AND IN_SHIP_ND_DATE is not null) 
		 OR (SHIP_ND_DATE is not null AND IN_SHIP_ND_DATE is null)
		 OR (SHIP_ND_DATE is not null AND IN_SHIP_ND_DATE is not null AND SHIP_ND_DATE <> IN_SHIP_ND_DATE)    
	 OR (SHIP_2D_DATE is null AND IN_SHIP_2D_DATE is not null) 
		 OR (SHIP_2D_DATE is not null AND IN_SHIP_2D_DATE is null)
		 OR (SHIP_2D_DATE is not null AND IN_SHIP_2D_DATE is not null AND SHIP_2D_DATE <> IN_SHIP_2D_DATE)    
	 OR (SHIP_GR_DATE is null AND IN_SHIP_GR_DATE is not null) 
		 OR (SHIP_GR_DATE is not null AND IN_SHIP_GR_DATE is null)
		 OR (SHIP_GR_DATE is not null AND IN_SHIP_GR_DATE is not null AND SHIP_GR_DATE <> IN_SHIP_GR_DATE)
	 OR (SHIP_SAT_DATE is null AND IN_SHIP_SAT_DATE is not null) 
		 OR (SHIP_SAT_DATE is not null AND IN_SHIP_SAT_DATE is null)
		 OR (SHIP_SAT_DATE is not null AND IN_SHIP_SAT_DATE is not null AND SHIP_SAT_DATE <> IN_SHIP_SAT_DATE)
	 OR SHIP_ND_CUTOFF_TIME <> IN_SHIP_ND_CUTOFF_TIME
	 OR SHIP_2D_CUTOFF_TIME <> IN_SHIP_2D_CUTOFF_TIME
	 OR SHIP_GR_CUTOFF_TIME <> IN_SHIP_GR_CUTOFF_TIME
	 OR SHIP_SAT_CUTOFF_TIME <> IN_SHIP_SAT_CUTOFF_TIME
        )
    ;

  ELSE

    INSERT INTO PAS_VENDOR_PRODUCT_STATE_DT (
      VENDOR_ID,
      PRODUCT_ID,
      STATE_CODE,
      DELIVERY_DATE,
      SHIP_ND_DATE,
      SHIP_2D_DATE,
      SHIP_GR_DATE,
      SHIP_SAT_DATE,
      SHIP_ND_CUTOFF_TIME,
      SHIP_2D_CUTOFF_TIME,
      SHIP_GR_CUTOFF_TIME,
      SHIP_SAT_CUTOFF_TIME,
      UPDATED_ON
    ) VALUES (
      IN_VENDOR_ID,
      IN_PRODUCT_ID,
      IN_STATE_CODE,
      IN_DELIVERY_DATE,
      IN_SHIP_ND_DATE,
      IN_SHIP_2D_DATE,
      IN_SHIP_GR_DATE,
      IN_SHIP_SAT_DATE,
      IN_SHIP_ND_CUTOFF_TIME,
      IN_SHIP_2D_CUTOFF_TIME,
      IN_SHIP_GR_CUTOFF_TIME,
      IN_SHIP_SAT_CUTOFF_TIME,
      SYSDATE
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION 
  WHEN DUP_VAL_ON_INDEX THEN
    -- Ignore the duplicate
    OUT_STATUS := 'Y';

  WHEN OTHERS THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_VENDOR_PROD_ST_DT;

PROCEDURE INSERT_PAS_COUNTRY_PRODUCT_DT
(
    IN_COUNTRY_ID               IN PAS_COUNTRY_PRODUCT_DT.COUNTRY_ID%TYPE,
    IN_PRODUCT_ID               IN PAS_COUNTRY_PRODUCT_DT.PRODUCT_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_COUNTRY_PRODUCT_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_DATE              IN PAS_COUNTRY_PRODUCT_DT.CUTOFF_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_COUNTRY_PRODUCT_DT.CUTOFF_TIME%TYPE,
    IN_DELIVERY_AVAILABLE_FLAG  IN PAS_COUNTRY_PRODUCT_DT.DELIVERY_AVAILABLE_FLAG%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_COUNTRY_PRODUCT_DT table

Input:
    IN_COUNTRY_ID               country Id
    IN_PRODUCT_ID               product Id 
    IN_DELIVERY_DATE            delivery date 
    IN_CUTOFF_DATE              cutoff date for ordering 
    IN_CUTOFF_TIME              cutoff time on cutoff date 
    IN_DELIVERY_AVAILABLE_FLAG  Is Delivery Available 

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_COUNTRY_PRODUCT_DT
    where  product_id = IN_PRODUCT_ID
    and    delivery_date = IN_DELIVERY_DATE
    and    country_id = IN_COUNTRY_ID;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_COUNTRY_PRODUCT_DT 
    SET 
      CUTOFF_DATE = IN_CUTOFF_DATE,
      CUTOFF_TIME = IN_CUTOFF_TIME,
      DELIVERY_AVAILABLE_FLAG = IN_DELIVERY_AVAILABLE_FLAG  
    WHERE  product_id = IN_PRODUCT_ID
    AND    country_id = IN_COUNTRY_ID
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_COUNTRY_PRODUCT_DT (
      COUNTRY_ID,
      PRODUCT_ID,
      DELIVERY_DATE,
      CUTOFF_DATE,
      CUTOFF_TIME,
      DELIVERY_AVAILABLE_FLAG  
    ) VALUES (
      IN_COUNTRY_ID,
      IN_PRODUCT_ID,
      IN_DELIVERY_DATE,
      IN_CUTOFF_DATE,
      IN_CUTOFF_TIME,
      IN_DELIVERY_AVAILABLE_FLAG  
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_COUNTRY_PRODUCT_DT;

PROCEDURE INSERT_PAS_TIMEZONE_DT
(
    IN_TIME_ZONE_CODE           IN PAS_TIMEZONE_DT.TIME_ZONE_CODE%TYPE,
    IN_DAYLIGHT_SAVINGS_FLAG    IN PAS_TIMEZONE_DT.DAYLIGHT_SAVINGS_FLAG%TYPE,
    IN_DELIVERY_DATE            IN PAS_TIMEZONE_DT.DELIVERY_DATE%TYPE,
    IN_DELTA_TO_CST_HHMM        IN PAS_TIMEZONE_DT.DELTA_TO_CST_HHMM%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the PAS_TIMEZONE_DT table

Input:
    IN_TIME_ZONE_CODE           time zone code
    IN_DAYLIGHT_SAVINGS_FLAG    Daylight savings, y=yes n=no
    IN_DELIVERY_DATE            Delivery DAte
    IN_DELTA_TO_CST_HHMM        Delta in HHMM to Central Time

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
    select count(1)
    from   PAS_TIMEZONE_DT
    where  time_zone_code = IN_TIME_ZONE_CODE
    and    delivery_date = IN_DELIVERY_DATE
    and    daylight_savings_flag = IN_DAYLIGHT_SAVINGS_FLAG;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_TIMEZONE_DT 
    SET 
      DELTA_TO_CST_HHMM = IN_DELTA_TO_CST_HHMM
    where  time_zone_code = IN_TIME_ZONE_CODE
    and    delivery_date = IN_DELIVERY_DATE
    and    daylight_savings_flag = IN_DAYLIGHT_SAVINGS_FLAG;

  ELSE

    INSERT INTO PAS_TIMEZONE_DT (
      TIME_ZONE_CODE,
      DELIVERY_DATE,
      DAYLIGHT_SAVINGS_FLAG,
      DELTA_TO_CST_HHMM
    ) VALUES (
      IN_TIME_ZONE_CODE,
      IN_DELIVERY_DATE,
      IN_DAYLIGHT_SAVINGS_FLAG,
      IN_DELTA_TO_CST_HHMM
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAS_TIMEZONE_DT;


PROCEDURE REMOVE_DELIVERY_DATE
(
    IN_DELIVERY_DATE            IN PAS_COUNTRY_PRODUCT_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Removes a delivery date from all of the PAS tables

Input:
    IN_DELIVERY_DATE            delivery date 

Output:
    OUT_STATUS                  Status of the Delete
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/


BEGIN

  DELETE FROM PAS_VENDOR_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_PRODUCT_STATE_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_COUNTRY_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_PRODUCT_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_VENDOR_PRODUCT_STATE_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_FLORIST_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_COUNTRY_PRODUCT_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_FLORIST_ZIPCODE_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  DELETE FROM PAS_TIMEZONE_DT
  WHERE  delivery_date <= IN_DELIVERY_DATE;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END REMOVE_DELIVERY_DATE;


PROCEDURE INVAL_PAS_COUNTRY_PRODUCT_DT
(
    IN_COUNTRY_ID               IN PAS_COUNTRY_PRODUCT_DT.COUNTRY_ID%TYPE,
    IN_PRODUCT_ID               IN PAS_COUNTRY_PRODUCT_DT.PRODUCT_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_COUNTRY_PRODUCT_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_COUNTRY_PRODUCT_DT

Input:
    IN_COUNTRY_ID               Country Id
    IN_PRODUCT_ID               Product Id
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/


v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_COUNTRY_PRODUCT_DT 
    SET 
      DELIVERY_AVAILABLE_FLAG = ''N''
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    IF IN_COUNTRY_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND country_id  = ''' || IN_COUNTRY_ID  || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_PAS_COUNTRY_PRODUCT_DT;

PROCEDURE INVAL_PAS_VENDOR_PRODUCT_ST_DT
(
    IN_VENDOR_ID                IN PAS_VENDOR_PRODUCT_STATE_DT.VENDOR_ID%TYPE,
    IN_PRODUCT_ID               IN PAS_VENDOR_PRODUCT_STATE_DT.PRODUCT_ID%TYPE,
    IN_STATE_CODE               IN PAS_VENDOR_PRODUCT_STATE_DT.STATE_CODE%TYPE,
    IN_DELIVERY_DATE            IN PAS_VENDOR_PRODUCT_STATE_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_VENDOR_PRODUCT_STATE_DT

Input:
    IN_VENDOR_ID                Vendor Id
    IN_PRODUCT_ID               Product Id
    IN_STATE_CODE               State Code
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_VENDOR_PRODUCT_STATE_DT 
    SET 
      SHIP_ND_DATE = null,
      SHIP_2D_DATE = null,
      SHIP_GR_DATE = null,
      SHIP_SAT_DATE = null,
      UPDATED_ON = sysdate 
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    IF IN_VENDOR_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND vendor_id  = ''' || IN_VENDOR_ID  || '''';
    END IF;

    IF IN_STATE_CODE IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND state_code = ''' || IN_STATE_CODE || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_PAS_VENDOR_PRODUCT_ST_DT;

PROCEDURE INVAL_PAS_VENDOR_DT
(
    IN_VENDOR_ID                IN PAS_VENDOR_DT.VENDOR_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_VENDOR_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_VENDOR_DT

Input:
    IN_VENDOR_ID                Vendor Id
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_VENDOR_DT 
    SET 
      DELIVERY_AVAILABLE_FLAG = ''N'',
      SHIP_ALLOWED_FLAG = ''N''
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_VENDOR_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND vendor_id  = ''' || IN_VENDOR_ID  || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_PAS_VENDOR_DT;

PROCEDURE INVAL_FLORIST_DT
(
    IN_FLORIST_ID               IN PAS_FLORIST_DT.FLORIST_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_FLORIST_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_FLORIST_DT

Input:
    IN_FLORIST_ID               Florist Id
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_FLORIST_DT 
    SET 
      STATUS_CODE = ''N''
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_FLORIST_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND florist_id = ''' || IN_FLORIST_ID || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_FLORIST_DT;

PROCEDURE INVAL_PAS_PRODUCT_DT
(
    IN_PRODUCT_ID               IN PAS_PRODUCT_DT.PRODUCT_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_PRODUCT_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_PRODUCT_DT

Input:
    IN_PRODUCT_ID               Product Id
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_PRODUCT_DT 
    SET 
      DELIVERY_AVAILABLE_FLAG = ''N''
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_PAS_PRODUCT_DT;

PROCEDURE INVAL_PAS_COUNTRY_DT
(
    IN_COUNTRY_ID               IN PAS_COUNTRY_DT.COUNTRY_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_COUNTRY_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_COUNTRY_DT

Input:
    IN_COUNTRY_ID               Country Id
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_COUNTRY_DT 
    SET 
      DELIVERY_AVAILABLE_FLAG = ''N'',
      SHIP_ALLOWED_FLAG = ''N''
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_COUNTRY_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND country_id = ''' || IN_COUNTRY_ID || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_PAS_COUNTRY_DT;

PROCEDURE INVAL_PAS_PRODUCT_STATE_DT
(
    IN_PRODUCT_ID               IN PAS_PRODUCT_STATE_DT.PRODUCT_ID%TYPE,
    IN_STATE_CODE               IN PAS_PRODUCT_STATE_DT.STATE_CODE%TYPE,
    IN_DELIVERY_DATE            IN PAS_PRODUCT_STATE_DT.DELIVERY_DATE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Invalidates the specified rows in PAS_PRODUCT_STATE_DT

Input:
    IN_PRODUCT_ID               Product Id
    IN_STATE_CODE               State Code
    IN_DELIVERY_DATE            Delivery DAte

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);


BEGIN

    v_main_sql := 
    'UPDATE PAS_PRODUCT_STATE_DT 
    SET 
      DELIVERY_AVAILABLE_FLAG = ''Y''
    WHERE  1=1
    AND delivery_date = ''' || IN_DELIVERY_DATE || '''';

    IF IN_PRODUCT_ID IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND product_id = ''' || IN_PRODUCT_ID || '''';
    END IF;

    IF IN_STATE_CODE IS NOT NULL THEN
        v_main_sql := v_main_sql || ' AND state_code = ''' || IN_STATE_CODE || '''';
    END IF;

    EXECUTE IMMEDIATE v_main_sql;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INVAL_PAS_PRODUCT_STATE_DT;

PROCEDURE INSERT_PAS_FLORIST_ZIPCODE_DT
(
    IN_ZIP_CODE_ID              IN PAS_FLORIST_ZIPCODE_DT.ZIP_CODE_ID%TYPE,
    IN_DELIVERY_DATE            IN PAS_FLORIST_ZIPCODE_DT.DELIVERY_DATE%TYPE,
    IN_CUTOFF_TIME              IN PAS_FLORIST_ZIPCODE_DT.CUTOFF_TIME%TYPE,
    IN_STATUS_CODE              IN PAS_FLORIST_ZIPCODE_DT.STATUS_CODE%TYPE,
    IN_FIT_STATUS_CODE          IN PAS_FLORIST_ZIPCODE_DT.FIT_STATUS_CODE%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS

CURSOR exists_cur IS
    select count(1)
    from   PAS_FLORIST_ZIPCODE_DT
    where  zip_code_id = IN_ZIP_CODE_ID
    and    delivery_date = IN_DELIVERY_DATE;

exists_check  number;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur into exists_check;

  IF exists_check > 0 THEN

    UPDATE PAS_FLORIST_ZIPCODE_DT
    SET CUTOFF_TIME = IN_CUTOFF_TIME,
      STATUS_CODE = IN_STATUS_CODE,
      FIT_STATUS_CODE = IN_FIT_STATUS_CODE
    WHERE  zip_code_id = IN_ZIP_CODE_ID
    AND    delivery_date = IN_DELIVERY_DATE;

  ELSE

    INSERT INTO PAS_FLORIST_ZIPCODE_DT (
      ZIP_CODE_ID,
      DELIVERY_DATE,
      CUTOFF_TIME,
      STATUS_CODE,
      FIT_STATUS_CODE
    ) VALUES (
      IN_ZIP_CODE_ID,
      IN_DELIVERY_DATE,
      IN_CUTOFF_TIME,
      IN_STATUS_CODE,
      IN_FIT_STATUS_CODE
    );

  END IF;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_PAS_FLORIST_ZIPCODE_DT;

PROCEDURE CHECK_FLORIST_OPEN_CLOSE
(
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS

v_date date;
v_local_time varchar2(100);

cursor get_pm_closed_same_day is
    select fh.florist_id
    from FTD_APPS.FLORIST_HOURS fh
    join ftd_apps.florist_master fm
    on fm.FLORIST_ID = fh.FLORIST_ID
    join FTD_APPS.STATE_MASTER sm
    on sm.STATE_MASTER_ID = fm.STATE
    join pas.pas_timezone_dt pt
    on pt.time_zone_code = sm.time_zone
    and pt.DELIVERY_DATE = trunc(sysdate)
    join frp.global_parms gp
    on gp.context = 'FTDAPPS_PARMS'
    and gp.name = 'PM_CLOSED_DELIVERY_CUTOFF'
    where fh.DAY_OF_WEEK = trim(to_char(sysdate, 'DAY'))
    and fh.open_close_code = 'P'
    and (nvl(fm.last_fhdc_open_close_status, 'N/A') not in ('PM_CLOSED_SD', 'PM_CLOSED_FUTURE') or
        fm.last_fhdc_open_close_date < sysdate - 11/12)
    and (gp.value + pt.delta_to_cst_hhmm) <= to_char(sysdate, 'hh24mi')
    and to_char(sysdate, 'hh24mi') < '2100'
    and exists (select 'Y'
        from ftd_apps.florist_zips fz
        where fz.florist_id = fh.florist_id);

cursor get_pm_closed_future is
    select fh.florist_id
    from FTD_APPS.FLORIST_HOURS fh
    join ftd_apps.florist_master fm
    on fm.FLORIST_ID = fh.FLORIST_ID
    join FTD_APPS.STATE_MASTER sm
    on sm.STATE_MASTER_ID = fm.STATE
    join pas.pas_timezone_dt pt
    on pt.time_zone_code = sm.time_zone
    and pt.DELIVERY_DATE = trunc(sysdate)
    join frp.global_parms gp
    on gp.context = 'FTDAPPS_PARMS'
    and gp.name = 'PM_CLOSED_ORDER_CUTOFF'
    where fh.DAY_OF_WEEK = trim(to_char(sysdate, 'DAY'))
    and fh.open_close_code = 'P'
    and (nvl(fm.last_fhdc_open_close_status, 'N/A') <> 'PM_CLOSED_FUTURE' or
        fm.last_fhdc_open_close_date < sysdate - 11/12)
    and (gp.value + pt.delta_to_cst_hhmm) <= to_char(sysdate, 'hh24mi')
    and to_char(sysdate, 'hh24mi') < '2100'
    and exists (select 'Y'
        from ftd_apps.florist_zips fz
        where fz.florist_id = fh.florist_id);

cursor get_am_open is
    select fh.florist_id
    from FTD_APPS.FLORIST_HOURS fh
    join ftd_apps.florist_master fm
    on fm.FLORIST_ID = fh.FLORIST_ID
    join FTD_APPS.STATE_MASTER sm
    on sm.STATE_MASTER_ID = fm.STATE
    join pas.pas_timezone_dt pt
    on pt.time_zone_code = sm.time_zone
    and pt.DELIVERY_DATE = trunc(sysdate)
    where fh.DAY_OF_WEEK = trim(to_char(sysdate, 'DAY'))
    and fh.open_close_code = 'A'
    and (nvl(fm.last_fhdc_open_close_status, 'N/A') <> 'AM_OPEN' or
        fm.last_fhdc_open_close_date < sysdate - 11/12)
    and ('1200' + pt.delta_to_cst_hhmm) <= to_char(sysdate, 'hh24mi')
    and to_char(sysdate, 'hh24mi') < '2100'
    and exists (select 'Y'
        from ftd_apps.florist_zips fz
        where fz.florist_id = fh.florist_id);

cursor get_closed_tomorrow is
    select fh.florist_id
    from FTD_APPS.FLORIST_HOURS fh
    join ftd_apps.florist_master fm
    on fm.FLORIST_ID = fh.FLORIST_ID
    join FTD_APPS.STATE_MASTER sm
    on sm.STATE_MASTER_ID = fm.STATE
    join pas.pas_timezone_dt pt
    on pt.time_zone_code = sm.time_zone
    and pt.DELIVERY_DATE = trunc(sysdate)
    join FTD_APPS.FLORIST_HOURS fh1
    on fh1.florist_id = fh.FLORIST_ID
    and fh1.day_of_week = trim(to_char(v_date+1, 'DAY'))
    join frp.global_parms gp
    on gp.context = 'FTDAPPS_PARMS'
    and gp.name = 'EOD_CLOSED_ORDER_CUTOFF'
    where fh.DAY_OF_WEEK = trim(to_char(v_date, 'DAY'))
    and (nvl(fm.last_fhdc_open_close_status, 'N/A') <> 'CLOSED' or
        fm.last_fhdc_open_close_date < sysdate - 11/12)
    and decode(fh.open_close_code, 'A', null, decode(fh.day_of_week, 'SUNDAY', null, fh.open_close_code)) is null
    and decode(fh1.open_close_code, 'P', null, decode(fh1.day_of_week, 'SUNDAY', null, fh1.open_close_code)) is not null
    and (gp.value + pt.delta_to_cst_hhmm) <= v_local_time
    and exists (select 'Y'
        from ftd_apps.florist_zips fz
        where fz.florist_id = fh.florist_id);

cursor get_open is
    select fh.florist_id
    from FTD_APPS.FLORIST_HOURS fh
    join ftd_apps.florist_master fm
    on fm.FLORIST_ID = fh.FLORIST_ID
    join FTD_APPS.STATE_MASTER sm
    on sm.STATE_MASTER_ID = fm.STATE
    join pas.pas_timezone_dt pt
    on pt.time_zone_code = sm.time_zone
    and pt.DELIVERY_DATE = trunc(sysdate)
    join FTD_APPS.FLORIST_HOURS fh1
    on fh1.florist_id = fh.FLORIST_ID
    and fh1.day_of_week = trim(to_char(v_date+1, 'DAY'))
    where fh.DAY_OF_WEEK = trim(to_char(v_date, 'DAY'))
    and (nvl(fm.last_fhdc_open_close_status, 'N/A') <> 'OPEN' or
        fm.last_fhdc_open_close_date < sysdate - 11/12)
    and decode(fh.open_close_code, 'A', null, decode(fh.day_of_week, 'SUNDAY', null, fh.open_close_code)) is not null
    and decode(fh1.open_close_code, 'P', null, decode(fh1.day_of_week, 'SUNDAY', null, fh1.open_close_code)) is null
    and ('2400' + pt.delta_to_cst_hhmm) <= v_local_time
    and exists (select 'Y'
        from ftd_apps.florist_zips fz
        where fz.florist_id = fh.florist_id);

BEGIN

    FOR rec in get_pm_closed_same_day LOOP
        PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', rec.florist_id || ' ' || to_char(sysdate, 'mmddyyyy') ||
            ' ' || to_char(sysdate, 'mmddyyyy'));
        update ftd_apps.florist_master
            set last_fhdc_open_close_status = 'PM_CLOSED_SD',
                last_fhdc_open_close_date = sysdate
            where florist_id = rec.florist_id;
    END LOOP;
    
    FOR rec in get_pm_closed_future LOOP
        PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', rec.florist_id);
        update ftd_apps.florist_master
            set last_fhdc_open_close_status = 'PM_CLOSED_FUTURE',
                last_fhdc_open_close_date = sysdate
            where florist_id = rec.florist_id;
    END LOOP;
    
    FOR rec in get_am_open LOOP
        PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', rec.florist_id);
        update ftd_apps.florist_master
            set last_fhdc_open_close_status = 'AM_OPEN',
                last_fhdc_open_close_date = sysdate
            where florist_id = rec.florist_id;
    END LOOP;
    
    v_date := trunc(sysdate);
    v_local_time := to_char(sysdate, 'hh24mi');
    if to_char(sysdate, 'hh24mi') < '0600' then
        v_local_time := '2400' + v_local_time;
        v_date := v_date - 1;
    end if;
--    if v_local_time >= '2400' then
--        v_date := v_date + 1;
--    end if;

    FOR rec in get_closed_tomorrow LOOP
        PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', rec.florist_id);
        update ftd_apps.florist_master
            set last_fhdc_open_close_status = 'CLOSED',
                last_fhdc_open_close_date = sysdate
            where florist_id = rec.florist_id;
    END LOOP;
    
    FOR rec in get_open LOOP
        PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', rec.florist_id);
        update ftd_apps.florist_master
            set last_fhdc_open_close_status = 'OPEN',
                last_fhdc_open_close_date = sysdate
            where florist_id = rec.florist_id;
    END LOOP;
    
    OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    
END CHECK_FLORIST_OPEN_CLOSE;

END PAS_MAINT_PKG;
/
