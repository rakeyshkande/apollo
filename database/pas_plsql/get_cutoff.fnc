create or replace function pas.get_cutoff (
    in_delivery_date date,
    in_addon_days int,
    in_cutoff_time varchar2
) return varchar2 as
out_cutoff_flag varchar2(10);

/*
    This function returns a Y if the current time is past the cutoff for the
    input delivery date. If it is not past cutoff, an N will be returned
*/

begin
    if in_cutoff_time < 0 then
        -- East coast availability when Central time cutoff is before 1:00am
        if in_delivery_date - trunc(sysdate+1) > in_addon_days then
            out_cutoff_flag := 'N';
        else
            if in_delivery_date - trunc(sysdate+1) < in_addon_days or
                in_delivery_date - trunc(sysdate) = in_addon_days then
                out_cutoff_flag := 'Y';
            else
                if to_char(sysdate, 'HH24mi') - in_cutoff_time < 2400 then
                    --out_cutoff_flag := 'N ' || (to_char(sysdate, 'HH24mi') - in_cutoff_time);
                    out_cutoff_flag := 'N';
                else
                    out_cutoff_flag := 'Y';
                end if;
            end if;
        end if;
    else
        -- old cutoff logic
        if (in_delivery_date - trunc(sysdate) > in_addon_days) or
             (in_delivery_date - trunc(sysdate) = in_addon_days and
                 lpad(in_cutoff_time, 4, '0') > to_char(sysdate, 'HH24mi')) then
            out_cutoff_flag := 'N';
        else
            out_cutoff_flag := 'Y';
        end if;
    end if;
    
    return out_cutoff_flag;
end;
/