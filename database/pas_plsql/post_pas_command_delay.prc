create or replace procedure PAS.POST_PAS_COMMAND_DELAY
(
  IN_COMMAND_NAME   IN  VARCHAR2, 
  IN_ARGUMENTS      IN  VARCHAR2,
  IN_DELAY          IN  INTEGER
)
AS 
/*-----------------------------------------------------------------------------
Description:
        Enqueues a JMS message on the PAS_COMMAND queue with the command
        and its arguments.  If the JMS message does not post properly
        a system message is sent.

        This is a wrapper around post_a_message_priority

Input:
        command_name        varchar2
        arguments           varchar2
        sleep time          integer

Output:

-----------------------------------------------------------------------------*/
payload              varchar2(1000);
status               varchar2(1);
message              varchar2(100);

v_message_id         number;
v_out_status         varchar2(1);
v_out_message        varchar2(256);

BEGIN
  payload := IN_COMMAND_NAME || ' ' || IN_ARGUMENTS;

  --Create the new JMS Message
  events.post_a_message_flex('ojms.PAS_COMMAND',
                      IN_COMMAND_NAME,
                      payload,
                      IN_DELAY,
                      status,
                      message);

  IF (status = 'N') THEN
    -- insert system message error
    FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
    (
      IN_SOURCE => 'POST_PAS_COMMAND_DELAY',
      IN_TYPE => 'EXCEPTION',
      IN_MESSAGE => 'Command::' || IN_COMMAND_NAME || ' -- ' || IN_ARGUMENTS,
      IN_COMPUTER => null,
      OUT_SYSTEM_MESSAGE_ID => v_message_id,
      OUT_STATUS => v_out_status,
      OUT_MESSAGE => v_out_message
    );

  END IF;

END;
.
/
