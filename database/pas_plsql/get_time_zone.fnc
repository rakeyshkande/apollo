create or replace function pas.get_time_zone (in_zip_code varchar2) return varchar2 as
   out_time_zone varchar2(5);
begin
   select distinct sm.time_zone
   into   out_time_zone
   from   ftd_apps.state_master sm
   join   ftd_apps.zip_code zc on sm.state_master_id = zc.state_id
   where  zc.zip_code_id = upper(in_zip_code);
   return out_time_zone;
end;
/