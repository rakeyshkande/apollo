create or replace function pas.get_state_by_zip (in_zip_code varchar2) return varchar2 as
   out_state_code varchar2(2);
begin
   select distinct zc.state_id
   into   out_state_code
   from   ftd_apps.zip_code zc
   where  zc.zip_code_id = upper(in_zip_code);
   
   return out_state_code;
end;
/