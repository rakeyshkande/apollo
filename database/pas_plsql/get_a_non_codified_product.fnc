CREATE OR REPLACE
FUNCTION pas.get_a_non_codified_product return varchar2 as
   out_product_id VARCHAR2(20);
BEGIN

   SELECT pm.product_id 
   INTO out_product_id
   FROM ftd_apps.product_master pm
   WHERE pm.status = 'A'
   AND pm.delivery_type = 'D'
   AND pm.exception_code is null
   AND pm.ship_method_florist = 'Y'
   AND NOT EXISTS
       (SELECT 'Y' from ftd_apps.codified_products cp
        WHERE cp.product_id = pm.product_id)
   AND rownum = 1;

   
   RETURN out_product_id;
end;
/