CREATE OR REPLACE
FUNCTION pas.is_country_delivery_available (in_delivery_date date, in_zip_code varchar2) return varchar2 as
   out_flag varchar2(1);
begin
   select distinct pc.delivery_available_flag
   into   out_flag
   from   pas.pas_country_dt pc
   join   ftd_apps.state_master sm on pc.country_id = decode(sm.country_code,null,'US','CAN','CA',sm.country_code)
   join   ftd_apps.zip_code zc on zc.state_id = sm.state_master_id
   where  pc.delivery_date = in_delivery_date
   and    zc.zip_code_id = upper(in_zip_code);
   return out_flag;
end;
/