CREATE OR REPLACE TRIGGER FTD_APPS.FLORIST_MASTER_PAS_FEED_TRG
AFTER UPDATE OF RECORD_TYPE,
                STATUS
             ON FTD_APPS.FLORIST_MASTER
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE

   procedure ins_pas(in_action        IN varchar2,
                     in_florist       IN varchar2,
                     in_parms         IN varchar2) is
   begin
      
     
      PAS.POST_PAS_COMMAND(in_action,   
                           in_florist       ||  
                           in_parms);

   end ins_pas;

   
BEGIN
   
   IF UPDATING  THEN
      declare
         v_start_date  date;
         v_end_date    date;

      begin
         
         IF :NEW.RECORD_TYPE = 'R' THEN

            IF -- Status changes to Inactive 
               (:OLD.status IS NULL       and
                :NEW.status = 'Inactive')                        OR
               (:OLD.status IS NOT NULL   and 
                :OLD.status != 'Inactive' and 
                :NEW.status = 'Inactive')                        OR
               --
               -- Status changes from Inactive to something else
               (:OLD.status IS NOT NULL   and 
                :OLD.status = 'Inactive'  and 
                (:NEW.status != 'Inactive' or 
                 :NEW.status IS NULL))                           THEN
                 
               -- If status change, send only 1 entry with NO Sunday Flag
               ins_pas('MODIFIED_FLORIST', :NEW.florist_id, NULL);

            END IF;       

         END IF;       
      end;
      
   END IF;

END;
/


select * from dba_errors where name = 'FLORIST_MASTER_PAS_FEED_TRG';