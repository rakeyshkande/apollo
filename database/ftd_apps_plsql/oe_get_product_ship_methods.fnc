CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PRODUCT_SHIP_METHODS (
 	   productId IN VARCHAR,
	   zipCode	 IN	VARCHAR2
)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_PRODUCT_SHIP_METHODS
-- Type:    Function
-- Syntax:  OE_GET_PRODUCT_SHIP_METHODS (
-- 			productId IN VARCHAR2
--			zipCode	  IN VARCHAR2)
-- Returns: ref_cursor for
--          DESCRIPTION  VARCHAR2(25)
--          COST         NUMBER(7.2)
--
-- Description:   Queries the PRODUCT_SHIP_METHODS table by PRODUCT_ID and SHIP_METHODS
--                by ship_method_id returning a ref cursor for resulting row. Excludes
--				  delivery types based on valued of state id associated with passed
--				  zip code
--
--===================================================
AS
    cur_cursor types.ref_cursor;
	stateId	ZIP_CODE.STATE_ID%TYPE := NULL;

BEGIN
    -- Select the time zone for this zip, if available
    BEGIN
        SELECT DISTINCT ZC.STATE_ID
		INTO stateId
        FROM ZIP_CODE zc, STATE_MASTER sm
        WHERE zc.ZIP_CODE_ID = UPPER(zipCode)
          AND ZC.STATE_ID = SM.STATE_MASTER_ID;
    EXCEPTION WHEN OTHERS THEN
        stateId := NULL;
    END;

    OPEN cur_cursor FOR
        SELECT sm.DESCRIPTION as "description",
               skc.SHIPPING_COST as "cost"
          FROM PRODUCT_MASTER pm,
               PRODUCT_SHIP_METHODS psm,
               SHIP_METHODS sm,
			   SHIPPING_KEY_DETAILS skd,
			   SHIPPING_KEY_COSTS skc
    	 WHERE pm.PRODUCT_ID = UPPER(productId)
		   AND pm.PRODUCT_ID = psm.PRODUCT_ID
		   AND psm.SHIP_METHOD_ID = sm.SHIP_METHOD_ID
		   AND pm.SHIPPING_KEY = skd.SHIPPING_KEY_ID
		   AND pm.STANDARD_PRICE >= skd.MIN_PRICE
		   AND pm.STANDARD_PRICE <= skd.MAX_PRICE
		   AND skd.SHIPPING_DETAIL_ID = skc.SHIPPING_KEY_DETAIL_ID
		   AND psm.SHIP_METHOD_ID = skc.SHIPPING_METHOD_ID
		   AND (stateId IS NULL
		    OR  stateId NOT IN ('AK','HI')
		    OR  psm.SHIP_METHOD_ID = '2F')
		ORDER BY skc.SHIPPING_COST;

    RETURN cur_cursor;
END
;
.
/
