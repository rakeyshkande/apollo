CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIPCODE_TIMEZONE (zipCode IN VARCHAR2)
RETURN types.ref_cursor
--=====================================================================
--
-- Name:    OE_GET_ZIPCODE_TIMEZONE
-- Type:    Function
-- Syntax:  OE_GET_ZIPCODE_TIMEZONE (zipCode IN varchar2)
-- Returns: ref_cursor for
--          TIMEZONE  VARCHAR2(2)
--
-- Description: Queries the ZIP CODE and STATE MASTER tables and returns
--              the time zone for the given zip code.
--
--======================================================================
AS
    zipcode_cursor types.ref_cursor;
	tmpZipCode VARCHAR2(30) := NULL;
BEGIN

	IF ( zipCode IS NOT NULL )
	THEN
		tmpZipCode := OE_CLEANUP_ALPHANUM_STRING(zipCode, 'Y');
	END IF;

    OPEN zipcode_cursor FOR
        SELECT SM.TIME_ZONE as "timeZone"
          FROM ZIP_CODE ZC, STATE_MASTER SM
          WHERE ZC.ZIP_CODE_ID = tmpZipCode
            AND ZC.STATE_ID = SM.STATE_MASTER_ID;

    RETURN zipcode_cursor;
END;
.
/
