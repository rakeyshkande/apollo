create or replace
procedure ftd_apps.sp_update_product_override
(
  IN_VENDOR_ID IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.VENDOR_ID%TYPE,
  IN_PRODUCT_SUBCODE_ID IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.PRODUCT_SUBCODE_ID%TYPE,
  IN_DELIVERY_OVERRIDE_DATE IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.DELIVERY_OVERRIDE_DATE%TYPE,
  IN_CARRIER_ID IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.CARRIER_ID%TYPE,
  IN_SDS_SHIP_METHOD IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.SDS_SHIP_METHOD%TYPE,
  OUT_STATUS OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
--==============================================================================
--
-- Name:    SP_UPDATES_PRODUCT_OVERRIDE
-- Type:    Function
-- Syntax:  SP_UPDATES_PRODUCT_OVERRIDE
--          VENDOR_ID IN VARCHAR2
--          PRODUCT_SUBCODE_ID VARCHAR2
--          OVERRIDE_DATE IN DATE
--          CARRIER_ID IN VARCHAR2
-- Returns:
--          OUT_STATUS          VARCHAR2(1)
--          OUT_MESSAGE         VARCHAR2
--
-- Description:   Updates VENDOR_PRODUCT_OVERRIDE table with the
--                passed in values.  In_Vendor_ID can take the value ALL_VENDORS,
--                in which case all vendors are processed.  Similarly, 
--                In_product_subcode_ID can take the value ALL_PRODUCTS.
--
--===================================================
is

TYPE vendor_id_table_type IS TABLE OF  FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE;
TYPE product_subcode_id_table_type IS TABLE OF  FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE;

CURSOR c_vendor_id IS (
    SELECT VENDOR_ID
       FROM VENDOR_MASTER
       WHERE ACTIVE = 'Y');

CURSOR c_product_id (p_vendor_id ftd_apps.vendor_master.vendor_id%TYPE) IS
  SELECT vp.PRODUCT_SUBCODE_ID
  FROM   FTD_APPS.VENDOR_PRODUCT vp
  JOIN   FTD_APPS.PRODUCT_MASTER pm ON vp.PRODUCT_SUBCODE_ID = pm.PRODUCT_ID
  WHERE  pm.STATUS = 'A'
  AND    vp.REMOVED = 'N'
  and    vp.vendor_id = p_vendor_id
  UNION
  SELECT vp.PRODUCT_SUBCODE_ID FROM FTD_APPS.VENDOR_PRODUCT vp
  JOIN   FTD_APPS.PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
  JOIN   FTD_APPS.PRODUCT_MASTER pm ON ps.PRODUCT_ID = pm.PRODUCT_ID
  WHERE  pm.STATUS = 'A'
  AND    vp.REMOVED = 'N'
  AND    vp.VENDOR_ID = p_vendor_id;

CURSOR c_single_product_id (p_vendor_id ftd_apps.vendor_master.vendor_id%TYPE,
                            p_product_subcode_id ftd_apps.vendor_product.product_subcode_id%TYPE) IS
  SELECT vp.PRODUCT_SUBCODE_ID
  FROM   FTD_APPS.VENDOR_PRODUCT vp
  JOIN   FTD_APPS.PRODUCT_MASTER pm ON vp.PRODUCT_SUBCODE_ID = pm.PRODUCT_ID
  WHERE  pm.STATUS = 'A'
  AND    vp.REMOVED = 'N'
  and    vp.vendor_id = p_vendor_id
  and    vp.product_subcode_id = p_product_subcode_id
  UNION
  SELECT vp.PRODUCT_SUBCODE_ID FROM FTD_APPS.VENDOR_PRODUCT vp
  JOIN   FTD_APPS.PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
  JOIN   FTD_APPS.PRODUCT_MASTER pm ON ps.PRODUCT_ID = pm.PRODUCT_ID
  WHERE  pm.STATUS = 'A'
  AND    vp.REMOVED = 'N'
  AND    vp.VENDOR_ID = p_vendor_id
  and    vp.product_subcode_id = p_product_subcode_id;

v_vendor_id            vendor_id_table_type;
v_product_subcode_id   product_subcode_id_table_type;

BEGIN

  OUT_STATUS := 'Y';

  IF (IN_VENDOR_ID = 'ALL_VENDORS') THEN
    OPEN c_vendor_id;
    FETCH c_vendor_id BULK COLLECT INTO v_vendor_id;
    CLOSE c_vendor_id;
  ELSE
    SELECT IN_VENDOR_ID
    BULK COLLECT INTO v_vendor_id
    FROM DUAL;
  END IF;

    FOR i in v_vendor_id.first .. v_vendor_id.last loop
  
      IF IN_PRODUCT_SUBCODE_ID = 'ALL_PRODUCTS' THEN
        OPEN c_product_id (v_vendor_id(i));
        FETCH c_product_id BULK COLLECT INTO v_product_subcode_id;
        CLOSE c_product_id;
      ELSE
        OPEN c_single_product_id (v_vendor_id(i), in_product_subcode_id);
        FETCH c_single_product_id BULK COLLECT INTO v_product_subcode_id;
        CLOSE c_single_product_id;
      END IF;
      if v_product_subcode_id.count > 0 then
  
        for j in v_product_subcode_id.first .. v_product_subcode_id.last loop
          BEGIN
            INSERT INTO FTD_APPS.VENDOR_PRODUCT_OVERRIDE (
                    VENDOR_ID,
                    PRODUCT_SUBCODE_ID,
                    DELIVERY_OVERRIDE_DATE,
                    CARRIER_ID,
                    SDS_SHIP_METHOD)
            VALUES( v_vendor_id(i),
                    v_product_subcode_id(j),
                    TRUNC(IN_DELIVERY_OVERRIDE_DATE),
                    IN_CARRIER_ID,
                    IN_SDS_SHIP_METHOD);
          EXCEPTION
             
            WHEN DUP_VAL_ON_INDEX THEN  
              --ignore duplicates and keep going on
              null;
          END;
        END LOOP;
       END IF;
    END LOOP;

EXCEPTION

WHEN OTHERS THEN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

end sp_update_product_override;
/
