CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_PRODUCT_INDEX (productIndexId in NUMBER)
as
--==============================================================================
--
-- Name:    SP_DELETE_PRODUCT_INDEX
-- Type:    Procedure
-- Syntax:  SP_DELETE_PRODUCT_INDEX ( productIndexId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_INDEX by PRODUCT_INDEX_ID.
--                Cascade also deletes any subindexes of the specified index,
--                and any PRODUCT_INDEX_XREF rows of the index or its' subindexes.
--
--==============================================================================
begin

  DELETE FROM PRODUCT_INDEX
  WHERE INDEX_ID = productIndexId;

  COMMIT;

end
;
.
/
