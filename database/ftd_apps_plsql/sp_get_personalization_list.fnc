create or replace
FUNCTION ftd_apps.SP_GET_PERSONALIZATION_LIST 
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PERSONALIZATION_LIST
-- Type:    Function
-- Syntax:  SP_GET_PERSONALIZATION_LIST ()
-- Returns: ref_cursor for
--          PERSONALIZATION_TEMPLATE_ID   VARCHAR2(10)
--          PERSONALIZATION_DESCRIPTION   VARCHAR2(100)
--
-- Description:   Queries the PERSONALIZATION_TEMPLATES table and
--                returns a ref cursor for resulting row.
--
--======================================================================

AS
    rec_cursor types.ref_cursor;
BEGIN
    OPEN rec_cursor FOR
        SELECT PERSONALIZATION_TEMPLATE_ID as "personalizationTemplateId",
               PERSONALIZATION_DESCRIPTION as "personalizationDescription"
        FROM FTD_APPS.PERSONALIZATION_TEMPLATES
        ORDER BY PERSONALIZATION_DESCRIPTION;
    RETURN rec_cursor;
END;
.
/
