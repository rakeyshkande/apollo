CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_VENDOR_SHIPBLOCK_END (vendorId IN varchar2,
   shipBlockRank IN NUMBER)
    RETURN DATE
--=========================================================================
--
-- Name:    OE_GET_VENDOR_SHIPBLOCK_END
-- Type:    Function
-- Syntax:  OE_GET_VENDOR_SHIPBLOCK_END(vendorId IN VARCHAR2,
--                                      shipBlockRank IN NUMBER)
-- Returns: tmp_blockend    DATE
--
-- Description:   Gets block end date for product shipment by vendor,
--                ordered by start date of the start / end date range.
--
--=========================================================================
AS
  tmp_blockend    DATE;
BEGIN
    -- Vendor ship block list view lists start / end date ranges
    --   for blocked vendor shipment of products, ordered by start date
    SELECT end_date
    INTO tmp_blockend
    FROM VENDOR_SHIP_BLOCK_LIST_VW
    WHERE vendor_id = vendorId
    AND ship_block_rank = shipBlockRank;

    RETURN tmp_blockend;

EXCEPTION WHEN OTHERS THEN
    RETURN null;
END
;
.
/
