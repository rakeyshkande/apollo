CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_LIST_HOLIDAY RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_LIST_HOLIDAY
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_LIST_HOLIDAY ()
-- Returns: ref_cursor for
--          PRODUCT_ID                 VARCHAR2(10)
--          NOVATOR_NAME               VARCHAR2(100)
--          HOLIDAY_PRICE              NUMBER(8,2)
--          HOLIDAY_SKU                VARCHAR2(6)
--
-- Description:   Queries the PRODUCT_MASTER table where values exist for
--                HOLIDAY_PRICE and HOLIDAY_SKU returning a ref cursor
--                to the resulting rows.
--
--=========================================================
AS
    product_list_cursor types.ref_cursor;
BEGIN
    OPEN product_list_cursor FOR
        SELECT PRODUCT_ID    as "productId",
               NOVATOR_NAME  as "novatorName",
               HOLIDAY_PRICE as "holidayPrice",
               HOLIDAY_SKU   as "holidaySku"
          FROM PRODUCT_MASTER
           WHERE HOLIDAY_PRICE is not NULL AND HOLIDAY_SKU is not NULL;

    RETURN product_list_cursor;
END
;
.
/
