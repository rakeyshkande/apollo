CREATE OR REPLACE
FUNCTION ftd_apps.OE_VALIDATE_USER (
userID IN varchar2,
userPwd IN RAW
)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_VALIDATE_USER
-- Type:    Function
-- Syntax:  OE_VALIDATE_USER (userID IN varchar2, userPwd IN RAW)
-- Returns: ref_cursor for
--          okFlag      VARCHAR2(2)    Always reads 'OK' on successful validate
-- Throws:  Raises ORA-20001 'Failed to validate user' if unsuccessful
--
-- Description:   Queries the USER_PROFILE table by user ID and checks the
--                supplied password against the currently stored password.
--                If the passwords mismatch or the user ID is not found,
--                raises an appropriate error.
--
--=============================================================
AS
    okCursor      types.ref_cursor;

    tmpPwd        USER_PROFILE.CURRENT_PASSWORD%TYPE;
BEGIN

    -- Select the current password for the user name specified
    SELECT CURRENT_PASSWORD INTO tmpPwd
        FROM USER_PROFILE
        WHERE user_id = userID
        AND deleted_flag = 'N';

    -- If current password and the supplied password do not match
    IF ( tmpPwd != userPwd )
    THEN
        -- Raise application error to indicate invalid user/pwd
        RAISE_APPLICATION_ERROR(-20001, 'Invalid password for user ' || userID);
    END IF;

    -- Password matched so return 'OK'
    OPEN okCursor FOR SELECT 'OK' as "okFlag" FROM DUAL;
    RETURN okCursor;

EXCEPTION
WHEN NO_DATA_FOUND THEN

    RAISE_APPLICATION_ERROR(-20001, 'User ' || userID || ' not found');

END
;
.
/
