CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SECOND_CHOICE_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SECOND_CHOICE_LIST
-- Type:    Function
-- Syntax:  SP_GET_SECOND_CHOICE_LIST ()
-- Returns: ref_cursor for
--          PRODUCT_SECOND_CHOICE_ID  VARCHAR2(2)
--          OE_DESCRIPTION1           VARCHAR2(80)
--          OE_DESCRIPTION2           VARCHAR2(80)
--          MERCURY_DESCRIPTION1      VARCHAR2(80)
--          MERCURY_DESCRIPTION2      VARCHAR2(80)
--
-- Description:   Queries the PRODUCT_SECOND_CHOICE table and
--                returns a ref cursor for resulting row.
--
--======================================================================

AS
    choice_cursor types.ref_cursor;
BEGIN
    OPEN choice_cursor FOR
        SELECT PRODUCT_SECOND_CHOICE_ID as "productSecondChoice",
               OE_DESCRIPTION1          as "oeDescription1",
               MERCURY_DESCRIPTION1     as "mercuryDescription1",
               OE_DESCRIPTION2          as "oeDescription2",
               MERCURY_DESCRIPTION2     as "mercuryDescription2"
        FROM PRODUCT_SECOND_CHOICE
        WHERE MERCURY_DESCRIPTION1 IS NOT NULL
        ORDER BY PRODUCT_SECOND_CHOICE_ID;

    RETURN choice_cursor;
END
;
.
/
