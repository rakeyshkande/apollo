CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_UPSELL_SOURCE (
  upsellMasterId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_UPSELL_SOURCE
-- Type:    Procedure
-- Syntax:  SP_REMOVE_UPSELL_SOURCE ( upsellMasterId in varchar2 )
--
-- Description:   Deletes all rows from UPSELL_SOURCE by UPSELL_MASTER_ID.
--
--==============================================================================
begin

 delete FROM UPSELL_SOURCE
  where UPSELL_MASTER_ID = upsellMasterId;

end
;
.
/
