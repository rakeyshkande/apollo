CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REBUILD_INDEXES  as
  CURSOR rebuildCur IS
  SELECT * FROM USER_INDEXES ORDER BY INDEX_NAME;

  rebuildIdx   rebuildCur%ROWTYPE;
  rebuildStmt  VARCHAR2(32767);
begin
  FOR rebuildIdx IN rebuildCur
  LOOP
  BEGIN
    rebuildStmt := 'ALTER INDEX ' || rebuildIdx.index_name || ' REBUILD COMPUTE STATISTICS';
    EXECUTE IMMEDIATE rebuildStmt;
  EXCEPTION WHEN OTHERS THEN
    dbms_output.put_line('Error while rebuilding ' || rebuildIdx.index_name);
  END;
  END LOOP;
end
;
.
/
