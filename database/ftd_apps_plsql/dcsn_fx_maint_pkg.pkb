create or replace
PACKAGE BODY FTD_APPS.DCSN_FX_MAINT_PKG AS
--using
PROCEDURE GET_DECISION_TYPE
  (
    IN_DECISION_TYPE            IN  dcsn_fx_decision_types.decision_type_code%type,
    OUT_NAME                    OUT dcsn_fx_decision_types.name%type,
    OUT_ADMIN_RESOURCE_ID       OUT dcsn_fx_decision_types.admin_resource_id%type,
    OUT_WIDGET_RESOURCE_ID      OUT dcsn_fx_decision_types.widget_resource_id%type,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2 

  )
  AS
  
v_name                dcsn_fx_decision_types.name%type;
v_admin_resource_id   dcsn_fx_decision_types.admin_resource_id%type;
v_widget_resource_id  dcsn_fx_decision_types.widget_resource_id%type;

  BEGIN
  
  select name, admin_resource_id, widget_resource_id into v_name, v_admin_resource_id, v_widget_resource_id from dcsn_fx_decision_types 
  where decision_type_code = in_decision_type;
  
  out_admin_resource_id := v_admin_resource_id;
  out_widget_resource_id := v_widget_resource_id;
  out_name := v_name;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN NULL; 
    
  END GET_DECISION_TYPE;
 
  PROCEDURE GET_RULE_TYPES
  (
    IN_DECISION_TYPE            IN dcsn_fx_rules.decision_type_code%type,
    OUT_CURSOR                  OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2 
  )
  
  AS
  
  BEGIN
  
  OPEN OUT_CURSOR FOR
        SELECT rule_code,
               decision_type_code,
               name,
               next_level_flag,
               display_order
               
        from dcsn_fx_rules where decision_type_code = IN_DECISION_TYPE and is_active_flag = 'Y' order by next_level_flag, display_order;
  
  END GET_RULE_TYPES;
  --using 
  PROCEDURE CREATE_DRAFT_CONFIGURATION
  (
    IN_DECISION_TYPE_CODE       IN dcsn_fx_decision_types.decision_type_code%type,
    IN_UPDATED_BY               IN dcsn_fx_config_value.updated_by%type,
    OUT_DECISION_CONFIG_ID      OUT dcsn_fx_config_value.decision_config_id%type,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  
  v_decision_config_id          dcsn_fx_config_value.decision_config_id%type;
  v_publish_config_id           dcsn_fx_config_value.decision_config_id%type;
  v_draft_config_id             dcsn_fx_config_value.decision_config_id%type;
  v_decision_name               dcsn_fx_config_value.name%type;
  
  BEGIN
  
  select decision_config_id into v_decision_config_id from dcsn_fx_decision_config where decision_type_code = in_decision_type_code and status_code = 'DRAFT';
  
  if v_decision_config_id > 0 THEN
    out_decision_config_id := v_decision_config_id;
  else
    select decision_config_id into v_publish_config_id from dcsn_fx_decision_config where decision_type_code = in_decision_type_code and status_code = 'PUBLISH';
    if v_publish_config_id > 0 THEN
      select dcsn_fx_decision_config_id_sq.nextval into v_decision_config_id from dual;
      --insert config
      insert into dcsn_fx_decision_config 
      (DECISION_CONFIG_ID, DECISION_TYPE_CODE, STATUS_CODE, CREATED_ON, CREATED_By, updated_on, updated_by) 
      values 
      (v_decision_config_id, in_decision_type_code, 'DRAFT', sysdate, in_updated_by, sysdate, in_updated_by);
      --insert values
      insert into DCSN_FX_CONFIG_VALUE (value_id, decision_config_id, name, 
      is_active_flag, parent_value_id, script_text, allow_comments_flag, group_order, created_on, created_by, updated_on, updated_by)  (
      select value_id, v_decision_config_id, name, is_active_flag, null, script_text, allow_comments_flag, group_order, sysdate, in_updated_by, sysdate, in_updated_by 
      from DCSN_FX_CONFIG_VALUE where decision_config_id = v_publish_config_id);
      
      for rec in (select value_id,  parent_value_id from DCSN_FX_CONFIG_VALUE where decision_config_id = v_publish_config_id) loop
        update DCSN_FX_CONFIG_VALUE set parent_value_id = rec.parent_value_id where value_id = rec.value_id and decision_config_id = v_decision_config_id;
      end loop;
            
      --insert rules
      insert into dcsn_fx_config_value_rules (value_id, decision_config_id, rule_code, created_on, created_by, updated_on, updated_by)
      (select value_id, v_decision_config_id, rule_code, sysdate, in_updated_by, sysdate, in_updated_by 
      from dcsn_fx_config_value_rules where decision_config_id = v_publish_config_id);
      out_decision_config_id := v_decision_config_id;
    else
      select dcsn_fx_decision_config_id_sq.nextval into v_decision_config_id from dual;
      --insert config
      insert into dcsn_fx_decision_config 
      (DECISION_CONFIG_ID, DECISION_TYPE_CODE, STATUS_CODE, CREATED_ON, CREATED_By, updated_on, updated_by) 
      values 
      (v_decision_config_id, in_decision_type_code, 'DRAFT', sysdate, in_updated_by, sysdate, in_updated_by);
      
      --insert value
      select name into v_decision_name from dcsn_fx_decision_types where decision_type_code = in_decision_type_code;
      
      insert into DCSN_FX_CONFIG_VALUE (value_id, decision_config_id, name, 
      is_active_flag, script_text, allow_comments_flag, group_order, created_on, created_by, updated_on, updated_by)  
      values 
      (dcsn_fx_value_id_sq.nextval, v_decision_config_id, v_decision_name,  'Y', ' ', 'Y', 1, sysdate, in_updated_by, sysdate, in_updated_by);
      out_decision_config_id := v_decision_config_id;
      
      
    end if;
    
  end if;
  out_status := 'Y';
  OUT_MESSAGE := '';
  EXCEPTION
    WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;  
  
  END CREATE_DRAFT_CONFIGURATION;
  
  PROCEDURE GET_CONFIG_BY_STATUS
  (
    IN_DECISION_TYPE_CODE               IN dcsn_fx_decision_types.decision_type_code%type,
    IN_STATUS_CODE                      IN dcsn_fx_decision_config.status_code%type,
    OUT_CURSOR                          OUT TYPES.REF_CURSOR,
    OUT_STATUS                          OUT VARCHAR2,
    OUT_MESSAGE                   		OUT VARCHAR2 

  )
  AS
  
  BEGIN
    OPEN OUT_CURSOR FOR 
      select decision_config_id, decision_type_code, status_code 
      from dcsn_fx_decision_config where decision_type_code = IN_DECISION_TYPE_CODE and status_code = in_status_code;
  
  END GET_CONFIG_BY_STATUS;
 
  PROCEDURE GET_CONFIG_BY_ID
  (
    IN_DECISION_CONFIG_ID               IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    OUT_CURSOR                          OUT TYPES.REF_CURSOR,
    OUT_STATUS                          OUT VARCHAR2,
    OUT_MESSAGE                   		OUT VARCHAR2

  )
  AS
    
  BEGIN
    OPEN OUT_CURSOR FOR 
      select decision_config_id, decision_type_code, status_code 
      from dcsn_fx_decision_config where decision_config_id = IN_DECISION_CONFIG_ID;
    
  END GET_CONFIG_BY_ID;
  
  PROCEDURE SAVE_CONFIG
  (
    IN_DECISION_CONFIG_ID               IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    IN_DECISION_TYPE_CODE               IN dcsn_fx_decision_types.decision_type_code%type,
    IN_STATUS_CODE                      IN dcsn_fx_decision_config.status_code%type,
    IN_UPDATED_BY                       IN dcsn_fx_config_value.updated_by%type,
    OUT_STATUS                          OUT VARCHAR2,
    OUT_MESSAGE                   		OUT VARCHAR2 

  )
  AS
    
  BEGIN
  
    insert into dcsn_fx_decision_config 
      (DECISION_CONFIG_ID, DECISION_TYPE_CODE, STATUS_CODE, CREATED_ON, CREATED_By, updated_on, updated_by) 
      values 
      (IN_DECISION_CONFIG_ID, IN_DECISION_TYPE_CODE, IN_STATUS_CODE, sysdate, in_updated_by, sysdate, in_updated_by); 
  
  END SAVE_CONFIG;
  
  --using 
  PROCEDURE DELETE_VALUE_NODE
  (
    IN_VALUE_ID                         IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID               IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    OUT_STATUS                          OUT VARCHAR2,
    OUT_MESSAGE                   		OUT VARCHAR2
  )
  AS
  
  BEGIN
    --delete all rules associated with the node
    DELETE_ALL_RULES_BY_VALUE(IN_VALUE_ID, IN_DECISION_CONFIG_ID, OUT_STATUS, OUT_MESSAGE);
    delete from dcsn_fx_config_value where value_id = in_value_id and decision_config_id = in_decision_config_id;
    COMMIT;
    EXCEPTION WHEN OTHERS THEN
      BEGIN
        ROLLBACK;
        out_status := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      END;
          
  END DELETE_VALUE_NODE;
  
  --using
  PROCEDURE SAVE_VALUE_NODE
    (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    IN_NAME                     IN DCSN_FX_CONFIG_VALUE.NAME%TYPE,
    IN_IS_ACTIVE_FLAG           IN DCSN_FX_CONFIG_VALUE.IS_ACTIVE_FLAG%TYPE,
    IN_PARENT_VALUE_ID          IN DCSN_FX_CONFIG_VALUE.PARENT_VALUE_ID%TYPE,
    IN_SCRIPT_TEXT              IN DCSN_FX_CONFIG_VALUE.SCRIPT_TEXT%TYPE,
    IN_ALLOW_COMMENTS_FLAG      IN DCSN_FX_CONFIG_VALUE.ALLOW_COMMENTS_FLAG%TYPE,
    IN_CHILDNODE_IDS            IN VARCHAR2,
    IN_VALUERULE_IDS            IN VARCHAR2,
    IN_UPDATED_BY               IN DCSN_FX_CONFIG_VALUE.UPDATED_BY%TYPE,
    OUT_VALUE_ID                OUT DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		  OUT VARCHAR2

  )
  AS

    v_update_variable_str   varchar2(2000) := ' ';
    v_group_order           DCSN_FX_CONFIG_VALUE.GROUP_ORDER%TYPE;
    v_str_valueRules        dbms_sql.varchar2s;
    v_value_id              DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE;

  BEGIN

   select max(group_order) into v_group_order  from dcsn_fx_config_value where parent_value_id = in_parent_value_id and decision_config_id = IN_DECISION_CONFIG_ID;
   IF v_group_order IS NULL THEN
    v_group_order := 1;
   ELSE
    v_group_order := v_group_order + 1;
   END IF;

    --insert new record
   select dcsn_fx_value_id_sq.nextval into v_value_id from dual;
   insert into DCSN_FX_CONFIG_VALUE (value_id, decision_config_id, name, 
    is_active_flag, parent_value_id, script_text, allow_comments_flag, group_order, created_on, created_by, updated_on, updated_by) 
    values 
    (v_value_id, IN_DECISION_CONFIG_ID, IN_NAME, IN_IS_ACTIVE_FLAG, IN_PARENT_VALUE_ID, 
    IN_SCRIPT_TEXT, IN_ALLOW_COMMENTS_FLAG, v_group_order, sysdate, in_updated_by, sysdate, in_updated_by);
  IF IN_VALUERULE_IDS IS NOT NULL AND LENGTH(IN_VALUERULE_IDS) > 0 THEN
    v_str_valueRules := tokenizer(IN_VALUERULE_IDS, ',');
    FOR i IN v_str_valueRules.FIRST..v_str_valueRules.LAST LOOP
      insert into dcsn_fx_config_value_rules (value_id, rule_code, decision_config_id, created_on, created_by, updated_on, updated_by)
      values (v_value_id, v_str_valueRules(i), in_decision_config_id, sysdate, in_updated_by, sysdate, in_updated_by);
    END LOOP;
  END IF;
  OUT_VALUE_ID := V_VALUE_ID;
  out_status := 'Y';
  COMMIT;
  EXCEPTION WHEN OTHERS THEN
    ROLLBACK;
    out_status := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

  END SAVE_VALUE_NODE;
 
  PROCEDURE SAVE_VALUE_RULE
  (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    IN_RULE_CODE                IN DCSN_FX_CONFIG_VALUE_RULES.RULE_CODE%TYPE,
    IN_UPDATED_BY               IN DCSN_FX_CONFIG_VALUE.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  
  v_rule_count      number := 0;
  
  BEGIN
    select count(*) into v_rule_count from dcsn_fx_config_value_rules where 
      value_id = in_value_id and
      decision_config_id = in_decision_config_id and
      rule_code = in_rule_code;
    if v_rule_count = 0 then
      insert into dcsn_fx_config_value_rules (value_id, decision_config_id, rule_code, created_on, created_by, updated_on, updated_by)
      values 
      (in_value_id, in_decision_config_id, in_rule_code, sysdate, in_updated_by, sysdate, in_updated_by);
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      out_status := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        
  END SAVE_VALUE_RULE;
   
  PROCEDURE DELETE_ALL_RULES_BY_VALUE
  (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  
  BEGIN
    delete from dcsn_fx_config_value_rules where value_id = in_value_id and decision_config_id = in_decision_config_id;
  EXCEPTION
    WHEN OTHERS THEN
      out_status := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  
  END DELETE_ALL_RULES_BY_VALUE;
  
  PROCEDURE GET_VALUE_NODE
  (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    OUT_CURSOR                  OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2

  )
  AS
    
  BEGIN
    OPEN OUT_CURSOR FOR 
      select value_id, decision_config_id, name, is_active_flag, parent_value_id, script_text, allow_comments_flag, group_order, created_on, created_by, updated_on, updated_by 
      from DCSN_FX_CONFIG_VALUE where value_id = in_value_id and decision_config_id = in_decision_config_id;
      
  END GET_VALUE_NODE;
  --using
  PROCEDURE GET_RULES_BY_VALUE
  (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    OUT_CURSOR                  OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2


  )
  AS
  
  BEGIN
  
    OPEN OUT_CURSOR FOR 
      select value_id, decision_config_id, rule_code, updated_on, updated_by
      from dcsn_fx_config_value_rules where value_id = in_value_id and decision_config_id = in_decision_config_id;
      
  
  END GET_RULES_BY_VALUE;
  
  
  PROCEDURE GET_VALUE_NODES_BY_STATUS
  (
    IN_DECISION_TYPE_CODE       IN dcsn_fx_decision_config.decision_type_code%type,
    IN_STATUS_CODE              IN dcsn_fx_decision_config.status_code%type,
    OUT_CURSOR                  OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2 

  )
  AS
  v_decision_config_id    dcsn_fx_decision_config.decision_config_id%type;
  BEGIN 
     select decision_config_id into v_decision_config_id from dcsn_fx_decision_config 
     where decision_type_code = in_decision_type_code  and status_code = in_status_code;
    
     OPEN OUT_CURSOR FOR 
      select value_id, decision_config_id, name, is_active_flag, parent_value_id, script_text, 
      allow_comments_flag, group_order, updated_on, updated_by, level 
      from DCSN_FX_CONFIG_VALUE start with parent_value_id is null and decision_config_id = v_decision_config_id connect by parent_value_id = prior value_id  
      and decision_config_id = v_decision_config_id order by level, group_order;
      


  END GET_VALUE_NODES_BY_STATUS;
  
  
  PROCEDURE GET_CHILD_NODES_FOR_VALUE_ID
  (
    IN_VALUE_ID                 IN NUMBER,
    IN_DECISION_CONFIG_ID       IN NUMBER,
    OUT_CURSOR                  OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  
  v_level_num     number := 0;
  v_parent_id     number := 0;
  
  BEGIN
    SELECT MAX(level) into v_level_num FROM DCSN_FX_CONFIG_VALUE START WITH value_id = in_value_id and decision_config_id = in_decision_config_id
    CONNECT BY value_id = PRIOR parent_value_id and decision_config_id = in_decision_config_id ORDER BY LEVEL DESC;
    
    select parent_value_id into v_parent_id from DCSN_FX_CONFIG_VALUE 
    where value_id = in_value_id and decision_config_id = in_decision_config_id;
    
    OPEN OUT_CURSOR FOR 
      select value_id, decision_config_id, name, is_active_flag, parent_value_id, script_text, 
      allow_comments_flag, group_order, updated_on, updated_by, level 
      from DCSN_FX_CONFIG_VALUE  START WITH parent_value_id = v_parent_id and decision_config_id = in_decision_config_id 
      CONNECT BY parent_value_id = prior value_id and decision_config_id = in_decision_config_id AND level = v_level_num ORDER BY LEVEL;
  
  END GET_CHILD_NODES_FOR_VALUE_ID;
  
  --using
  PROCEDURE GET_HIERARCHY_FOR_VALUE_ID
  (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    OUT_CURSOR                  OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  
  BEGIN
  OPEN OUT_CURSOR FOR
  SELECT value_id, parent_value_id, name, group_order, LEVEL FROM dcsn_fx_config_value 
  START WITH value_id = IN_VALUE_ID and decision_config_id = in_decision_config_id CONNECT BY value_id = prior parent_value_id AND 
  DECISION_CONFIG_ID = IN_DECISION_CONFIG_ID 
  --and IS_ACTIVE_FLAG = 'Y' 
  ORDER BY LEVEL desc;
  
  END GET_HIERARCHY_FOR_VALUE_ID;
  --using
  PROCEDURE UPDATE_VALUE_NODE
  (
    IN_VALUE_ID                 IN DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    IN_NAME                     IN DCSN_FX_CONFIG_VALUE.NAME%TYPE,
    IN_IS_ACTIVE_FLAG           IN DCSN_FX_CONFIG_VALUE.IS_ACTIVE_FLAG%TYPE,
    IN_PARENT_VALUE_ID          IN DCSN_FX_CONFIG_VALUE.PARENT_VALUE_ID%TYPE,
    IN_SCRIPT_TEXT              IN DCSN_FX_CONFIG_VALUE.SCRIPT_TEXT%TYPE,
    IN_ALLOW_COMMENTS_FLAG      IN DCSN_FX_CONFIG_VALUE.ALLOW_COMMENTS_FLAG%TYPE,
    IN_CHILDNODE_IDS            IN VARCHAR2,
    IN_VALUERULE_IDS            IN VARCHAR2,
    IN_UPDATED_BY               IN DCSN_FX_CONFIG_VALUE.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  
  )
  AS
    CURSOR c_childIds IS 
      SELECT value_id FROM dcsn_fx_config_value START WITH parent_value_id =IN_VALUE_ID and decision_config_id = in_decision_config_id
      CONNECT BY parent_value_id = prior value_id AND decision_config_id = IN_DECISION_CONFIG_ID;
      
    v_child_value_id        DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE;
    v_db_parent_id          DCSN_FX_CONFIG_VALUE.PARENT_VALUE_ID%TYPE;
    v_db_node_status        DCSN_FX_CONFIG_VALUE.IS_ACTIVE_FLAG%TYPE;
    v_group_order           DCSN_FX_CONFIG_VALUE.GROUP_ORDER%TYPE;
    v_str_valueRules        dbms_sql.varchar2s;
    v_str_childNodes        dbms_sql.varchar2s;
  BEGIN
    
    
    select parent_value_id, is_active_flag, group_order into v_db_parent_id, v_db_node_status, v_group_order from dcsn_fx_config_value where value_id = IN_VALUE_ID and decision_config_id = IN_DECISION_CONFIG_ID;
    if v_db_parent_id != 0 and v_db_parent_id != in_parent_value_id then
      select max(group_order) into v_group_order  from dcsn_fx_config_value where parent_value_id = in_parent_value_id and decision_config_id = IN_DECISION_CONFIG_ID;
      IF v_group_order IS NOT NULL THEN
        v_group_order := v_group_order + 1;
      ELSE
        v_group_order := 1;
      END IF;
        
    end if;
    
    delete from dcsn_fx_config_value_rules where value_id = IN_VALUE_ID and decision_config_id = IN_DECISION_CONFIG_ID;
    IF IN_PARENT_VALUE_ID = -1 THEN
      update dcsn_fx_config_value set
      NAME = IN_NAME,
      IS_ACTIVE_FLAG = IN_IS_ACTIVE_FLAG,
      SCRIPT_TEXT = IN_SCRIPT_TEXT,
      ALLOW_COMMENTS_FLAG = IN_ALLOW_COMMENTS_FLAG,
      UPDATED_BY = IN_UPDATED_BY,
      UPDATED_ON = sysdate
      where value_id = IN_VALUE_ID AND decision_config_id = IN_DECISION_CONFIG_ID;
    ELSE
      update dcsn_fx_config_value set
      NAME = IN_NAME,
      IS_ACTIVE_FLAG = IN_IS_ACTIVE_FLAG,
      PARENT_VALUE_ID = IN_PARENT_VALUE_ID,
      SCRIPT_TEXT = IN_SCRIPT_TEXT,
      ALLOW_COMMENTS_FLAG = IN_ALLOW_COMMENTS_FLAG,
      GROUP_ORDER = NVL(v_group_order, group_order),
      UPDATED_BY = IN_UPDATED_BY,
      UPDATED_ON = sysdate
      where value_id = IN_VALUE_ID AND decision_config_id = IN_DECISION_CONFIG_ID;
    END IF;
    IF IN_VALUERULE_IDS IS NOT NULL AND LENGTH(IN_VALUERULE_IDS) > 0 THEN 
      v_str_valueRules := tokenizer(IN_VALUERULE_IDS, ',');
      FOR i IN v_str_valueRules.FIRST..v_str_valueRules.LAST LOOP
        insert into dcsn_fx_config_value_rules (value_id, rule_code, decision_config_id, created_on, created_by, updated_on, updated_by) 
        values (in_value_id, v_str_valueRules(i), in_decision_config_id, sysdate, in_updated_by, sysdate, in_updated_by);
      END LOOP;
    END IF;
    
    IF IN_CHILDNODE_IDS IS NOT NULL AND LENGTH(IN_CHILDNODE_IDS) > 0 THEN
      v_str_childNodes := tokenizer(IN_CHILDNODE_IDS, ',');
      FOR i IN v_str_childNodes.FIRST..v_str_childNodes.LAST LOOP
        update dcsn_fx_config_value set group_order = i, UPDATED_BY = IN_UPDATED_BY,
        UPDATED_ON = sysdate where value_id = v_str_childNodes(i) and decision_config_id = IN_DECISION_CONFIG_ID;
      END LOOP;
    END IF;
    
    IF in_is_active_flag = 'N' AND in_is_active_flag != v_db_node_status THEN
      open c_childIds;
      loop
        fetch c_childIds into v_child_value_id;
        exit when c_childIds%NOTFOUND;
        update dcsn_fx_config_value set is_active_flag = in_is_active_flag, UPDATED_BY = IN_UPDATED_BY,
          UPDATED_ON = sysdate where value_id = v_child_value_id and decision_config_id = IN_DECISION_CONFIG_ID;
      END LOOP;
    END IF;
    out_status := 'Y';
    COMMIT;
   EXCEPTION
    WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;   
  
  END UPDATE_VALUE_NODE;
  --using
  PROCEDURE PUBLISH_DECISION_BY_TYPE
  (
    IN_DECISION_TYPE_CODE       IN DCSN_FX_DECISION_CONFIG.DECISION_TYPE_CODE%TYPE,
    IN_DECISION_CONFIG_ID       IN DCSN_FX_DECISION_CONFIG.DECISION_CONFIG_ID%TYPE,
    IN_UPDATED_BY               IN DCSN_FX_DECISION_CONFIG.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  
    v_publish_count                 number;
    v_next_decision_config_id       DCSN_FX_DECISION_CONFIG.DECISION_CONFIG_ID%TYPE;
    v_insert_config_value           varchar2(4000);
    v_insert_value_rules            varchar2(4000);
      
  
  BEGIN
    SELECT COUNT(*) INTO v_publish_count FROM dcsn_fx_decision_config WHERE 
    decision_type_code = in_decision_type_code AND status_code = 'PUBLISH';
    
    IF v_publish_count IS NOT NULL AND v_publish_count > 0 THEN
      UPDATE dcsn_fx_decision_config SET status_code = 'ARCHIVE', 
      updated_by = in_updated_by, updated_on = sysdate WHERE 
      decision_type_code = in_decision_type_code AND status_code = 'PUBLISH';
    END IF;
    
    UPDATE dcsn_fx_decision_config SET status_code = 'PUBLISH', 
      updated_by = in_updated_by, updated_on = sysdate WHERE 
      decision_type_code = in_decision_type_code AND decision_config_id = in_decision_config_id 
      AND status_code = 'DRAFT';
      
    select dcsn_fx_decision_config_id_sq.nextval into v_next_decision_config_id from dual;
    
    INSERT INTO dcsn_fx_decision_config (DECISION_CONFIG_ID, DECISION_TYPE_CODE, STATUS_CODE, 
      CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY) VALUES (v_next_decision_config_id, 
      in_decision_type_code, 'DRAFT', sysdate, in_updated_by, sysdate, in_updated_by);
      
    v_insert_config_value := 'INSERT INTO dcsn_fx_config_value (value_id, decision_config_id, name, '
      ||'is_active_flag, parent_value_id, script_text, allow_comments_flag, group_order, '
      ||'created_by, created_on, updated_by, updated_on) SELECT value_id,'|| v_next_decision_config_id||', name, is_active_flag, parent_value_id, script_text, '
      ||'allow_comments_flag, group_order,'''|| in_updated_by||''', sysdate, '''||in_updated_by||''', sysdate '
      ||'FROM DCSN_FX_CONFIG_VALUE START WITH parent_value_id IS NULL and decision_config_id = '||in_decision_config_id||' CONNECT BY parent_value_id = PRIOR value_id  AND '
      ||'decision_config_id = '||in_decision_config_id||' ORDER BY LEVEL, group_order';
    DBMS_OUTPUT.PUT_LINE(v_insert_config_value);  
    
    
    EXECUTE IMMEDIATE v_insert_config_value;
    
    v_insert_value_rules := 'INSERT INTO dcsn_fx_config_value_rules(value_id, decision_config_id, rule_code, created_by , created_on, updated_by, updated_on) '
                              ||'SELECT value_id, '||v_next_decision_config_id||', rule_code, '''|| in_updated_by||''', sysdate, '''||in_updated_by||''', sysdate '
                              ||'FROM dcsn_fx_config_value_rules where decision_config_id = '||in_decision_config_id;
   EXECUTE IMMEDIATE v_insert_value_rules;
  DBMS_OUTPUT.PUT_LINE(v_insert_value_rules);
    COMMIT;
    EXCEPTION
    WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;   
  
  END PUBLISH_DECISION_BY_TYPE;
  
  
  FUNCTION TOKENIZER
  (
    IN_STRING IN VARCHAR2,
    IN_SEPARATORS IN VARCHAR2
  )
  RETURN dbms_sql.varchar2s
  IS
  /*------------------------------------------------------------------------------
  Description:
     Returns a table of tokenized values.
     optional input input parameters.
  
  Input:
          in_string             VARCHAR2
          in_separators         VARCHAR2
  
  Output:
          table
  ------------------------------------------------------------------------------*/
  v_strs dbms_sql.varchar2s;
  BEGIN
    dbms_output.put_line('hai');
    WITH sel_string AS
        (SELECT in_string fullstring
           FROM DUAL)
    SELECT substr(fullstring, beg+1, end_p-beg-1) token
     BULK COLLECT INTO v_strs
     FROM (SELECT beg, lead(beg) over (order by beg) end_p, fullstring
             FROM (SELECT beg, fullstring
                     FROM (SELECT level beg, fullstring
                             FROM sel_string
                           connect by level <= length(fullstring))
                     WHERE instr(in_separators ,substr(fullstring,beg,1)) >0
                   UNION ALL
                   SELECT 0, fullstring FROM sel_string
                   UNION ALL
                   SELECT length(fullstring)+1, fullstring FROM sel_string))
      WHERE end_p IS NOT NULL
        AND end_p>beg+1;
--      FOR i IN v_strs.FIRST..v_strs.LAST LOOP
--        dbms_output.put_line(v_strs(i));    
--      END LOOP;
    RETURN v_strs;
  
  END TOKENIZER;
    
END DCSN_FX_MAINT_PKG;

.
/
