CREATE OR REPLACE
PACKAGE BODY ftd_apps.WEBOE_ORDERS_DISPATCHER_PKG AS


PROCEDURE WEBOE_ORDERS_DISPATCHER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for sending weboe orders to weboe dispatcher

Input:
	NONE

Output:
	NONE

-----------------------------------------------------------------------------*/

	v_order_id		ftd_apps.session_order_master.order_id%type;
   	v_confirmation_number	ftd_apps.session_order_master.order_status%type;
   	v_alert 		varchar2(50) := 'STOP WEBOE DISPATCHER';
   	v_timeout 		number := 2;
   	v_message 		varchar2(1800);
   	v_status 		integer;

BEGIN

   	dbms_alert.register(v_alert);
   	LOOP
		BEGIN
			-- pick up the number of seconds to be used in waitone call below
			SELECT 	value
			INTO	v_timeout
			FROM	frp.global_parms
			WHERE	context = 'WEBOE DISPATCHER'
				and name = 'WAIT TIME';

			IF (v_timeout IS NULL) THEN
				v_timeout := 2;
			END IF;

			-- in order to relieve the CPU, we wait here for v_timeout number of seconds
		  	dbms_alert.waitone(v_alert, v_message, v_status, v_timeout);
			-- while we were waiting we could have picked up the alert telling us to stop
	      		exit 	when (v_status = 0);

			-- pick up a single row to work with
			SELECT 	order_id, confirmation_number
			INTO	v_order_id, v_confirmation_number
			FROM 	ftd_apps.session_order_master
        		WHERE 	order_status = 'DONE'
				and dispatch_status = 'READY'
				and rownum = 1
			-- this locks the row without blocking other process from picking up other rows
       			FOR 	UPDATE SKIP LOCKED;

			-- prevent other instances from selecting the same row
			UPDATE	ftd_apps.session_order_master
			SET 	dispatch_status = 'SENT'
			WHERE	order_id = v_order_id;

			-- send a JMS message with v_order_id
			EVENTS.ENQUEUE('WEBOE_DISPATCHER', 'WEBOE_ORDER_TO_DISPATCH', v_order_id,'N',0);

			-- release the lock on the row
			COMMIT;

		EXCEPTION
			WHEN 	OTHERS THEN
				--make sure the locks are released and the updates are rolled back. Then continue
				ROLLBACK;
		END;
   	END LOOP;
   	dbms_alert.remove(v_alert);

END WEBOE_ORDERS_DISPATCHER;


PROCEDURE WEBOE_ORDERS_DISPATCHER_START
(
IN_INSTANCE_COUNT               NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for starting WEBOE_ORDERS_DISPATCHER
	stored procedure

Input:
	NONE

Output:
	NONE

-----------------------------------------------------------------------------*/
	v_count			number := 0;
	v_dispatch_order_flag	char(1);
	v_job			integer;
	v_procedure_name 	varchar2(200) := 'ftd_apps.WEBOE_ORDERS_DISPATCHER_PKG.WEBOE_ORDERS_DISPATCHER();';
BEGIN
	SELECT 	dispatch_order_flag
	INTO 	v_dispatch_order_flag
	FROM	ftd_apps.global_parms;

	-- This is a WebOE flag, meaning if it is 'N' WebOE will not dispatch, so we must
	IF (v_dispatch_order_flag = 'N') THEN
		BEGIN
			LOOP
				exit 	when (v_count = IN_INSTANCE_COUNT OR IN_INSTANCE_COUNT IS NULL);
				dbms_job.submit(job=>v_job, what=>v_procedure_name);
				v_count := v_count + 1;
			END LOOP;
		END;
	END IF;

END WEBOE_ORDERS_DISPATCHER_START;


PROCEDURE WEBOE_ORDERS_DISPATCHER_STOP
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for stoping WEBOE_ORDERS_DISPATCHER
	stored procedure

Input:
	NONE

Output:
	NONE

-----------------------------------------------------------------------------*/
   	v_name 		varchar2(50) := 'STOP WEBOE DISPATCHER';
   	v_message 	varchar2(1800) := NULL;
BEGIN
   	dbms_alert.signal(v_name, v_message);
	COMMIT;

END WEBOE_ORDERS_DISPATCHER_STOP;


END WEBOE_ORDERS_DISPATCHER_PKG;
.
/
