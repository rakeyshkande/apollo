CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_COLORS_LIST RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    SP_GET_COLORS_LIST
-- Type:    Function
-- Syntax:  SP_GET_COLORS_LIST ( )
-- Returns: ref_cursor for
--          COLOR_MASTER_ID     VARCHAR2(2)
--          DESCRIPTION         VARCHAR2(15)
--
-- Description:   Queries the color_master table and returns a ref cursor for row.
--
--===========================================================

AS
    color_cursor types.ref_cursor;
BEGIN
    OPEN color_cursor FOR
        SELECT COLOR_MASTER_ID as "colorMasterId",
               DESCRIPTION as "description",
               MARKER_TYPE as "markerType"
          FROM color_master;

    RETURN color_cursor;
END
;
.
/
