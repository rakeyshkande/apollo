CREATE OR REPLACE TRIGGER FTD_APPS.FLORIST_ZIPS_PAS_FEED_TRG
AFTER INSERT OR DELETE OR UPDATE OF BLOCK_START_DATE,                                    
                                    BLOCK_END_DATE,
                                    CUTOFF_TIME
                           ON FTD_APPS.FLORIST_ZIPS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
   procedure ins_pas(in_action      IN varchar2,
                     in_zip         IN varchar2,
                     in_start_date  IN date,
                     in_end_date    IN date) is
      v_date_str     varchar2(1000) := NULL;
      v_start_date   date;
      v_priority     integer := 0;
   begin
      
      if in_start_date is not null then
      
         -- Don't send dates < current date to PAS
         if trunc(in_start_date) < trunc(sysdate) then
            v_start_date := sysdate;
         else
            v_start_date := in_start_date;
         end if;
         
         v_date_str := v_date_str || ' ' || to_char(v_start_date,'MMDDYYYY');
         v_priority := trunc(v_start_date) - trunc(sysdate);

      end if;
      
      if in_end_date is not null then
         v_date_str := v_date_str || ' ' || to_char(in_end_date,'MMDDYYYY');
      end if;

      -- Don't even call PAS for old end dates
      if (in_end_date IS NOT NULL and
          trunc(in_end_date) >= trunc(sysdate))  OR
         (in_end_date IS NULL)                   then     

         PAS.POST_PAS_COMMAND_PRIORITY(in_action,   
                              in_zip       ||  
                              v_date_str,
                              v_priority);

      -- dbms_output.put_line('PAS_PAYLOAD="' || in_action || ',' || in_zip || v_date_str || '"' ); 
      end if;
                           
   end ins_pas;

   
BEGIN
   
   IF INSERTING THEN
      ins_pas('MODIFIED_FLORIST_ZIP', :NEW.zip_code, NULL, NULL);


   ELSIF UPDATING  THEN
      declare
         v_start_date  date;
         v_end_date    date;

      begin
         
         -- If Block start or block end or cutoff time changed, need to send a PAS msg
         if ((:OLD.block_start_date IS NULL     and :NEW.block_start_date IS NOT NULL) OR
             (:OLD.block_start_date IS NOT NULL and :NEW.block_start_date IS NULL)     OR
             (:OLD.block_start_date              != :NEW.block_start_date)             OR
             (:OLD.block_end_date   IS NULL     and :NEW.block_end_date   IS NOT NULL) OR
             (:OLD.block_end_date   IS NOT NULL and :NEW.block_end_date   IS NULL)     OR
             (:OLD.block_end_date                != :NEW.block_end_date)               OR
             (:OLD.cutoff_time      IS NULL     and :NEW.cutoff_time      IS NOT NULL) OR
	     (:OLD.cutoff_time      IS NOT NULL and :NEW.cutoff_time      IS NULL)     OR
	     (:OLD.cutoff_time                   != :NEW.cutoff_time)             
            ) THEN

 
            if :OLD.block_start_date IS NULL and :NEW.block_start_date IS NULL THEN
               v_start_date := NULL;
            elsif :OLD.block_start_date IS NULL THEN
               v_start_date := :NEW.block_start_date;
            elsif :NEW.block_start_date IS NULL THEN
               v_start_date := :OLD.block_start_date;
            else  -- Old AND New have values
               if :OLD.block_start_date < :NEW.block_start_date THEN
                  v_start_date := :OLD.block_start_date;
               else
                  v_start_date := :NEW.block_start_date;
               end if;
            end if;

            if :OLD.block_start_date IS NULL and :OLD.block_end_date IS NULL THEN
               v_end_date := :NEW.block_end_date;
            elsif :NEW.block_start_date IS NULL and :NEW.block_end_date IS NULL THEN
               v_end_date := :OLD.block_end_date;
            elsif :OLD.block_end_date IS NULL or :NEW.block_end_date IS NULL THEN
               v_end_date := NULL;
            else  -- Old AND New have values
               if :OLD.block_end_date > :NEW.block_end_date THEN
                  v_end_date := :OLD.block_end_date;
               else
                  v_end_date := :NEW.block_end_date;
               end if;
            end if;

            ins_pas('MODIFIED_FLORIST_ZIP', :NEW.zip_code, v_start_date, v_end_date);
         end if;
      end;
      
   ELSIF DELETING  THEN
      ins_pas('MODIFIED_FLORIST_ZIP', :OLD.zip_code, :OLD.block_start_date, :OLD.block_end_date);

   END IF;

END;
/


select * from dba_errors where name = 'FLORIST_ZIPS_PAS_FEED_TRG';