CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_UPDATE_FLORIST_PRODUCTS ( floristId in varchar2,
 productId in varchar2,
 blockFlag in varchar2)

as
--==============================================================================
--
-- Name:    OEL_UPDATE_FLORIST_PRODUCTS
-- Type:    Procedure
-- Syntax:  OEL_UPDATE_FLORIST_PRODUCTS ( floristId in varchar2,
--                                     productId in varchar2,
--                                     blockFlag in varchar2)
--
-- Description:   Attempts to insert a row into FLORIST_PRODUCTS.  If the row
--                already exists (by floristId), then updates the existing row.
--
--==============================================================================

begin

  -- Attempt to insert into FLORIST_PRODUCTS table
  INSERT INTO FLORIST_PRODUCTS
    ( FLORIST_ID,
      PRODUCT_ID,
      BLOCK_FLAG
    )
  VALUES
    ( floristId,
      productId,
      blockFlag
    );

  COMMIT;

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

  -- If a row already exists for this FLORIST_PRODUCTS then update
  UPDATE FLORIST_PRODUCTS
    SET BLOCK_FLAG = blockFlag
    WHERE FLORIST_ID = floristId AND PRODUCT_ID = UPPER(productId);

  COMMIT;
end
;
.
/
