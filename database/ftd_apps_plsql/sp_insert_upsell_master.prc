CREATE OR REPLACE
PROCEDURE ftd_apps.SP_INSERT_UPSELL_MASTER (
  masterIdIn in varchar2,
  nameIn in varchar2,
  description in varchar2,
  status in varchar2,
  searchPriority in varchar2,
  corporateSite in varchar2,
  gbbPopoverFlag in varchar2,
  gbbTitle in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSRT_UPSELL_MASTER
-- Type:    Procedure
-- Syntax:  SP_INSRT_UPSELL_MASTER ( masterIdIn,nameIn,description,status,searchPriority,corporateSite )
--
-- Description:   INSERT ROWS INTO UPSELL_MASTER.
--
--==============================================================================
begin

  UPDATE UPSELL_MASTER
     SET UPSELL_NAME = nameIn,
        UPSELL_DESCRIPTION  = description,
        UPSELL_STATUS = status,
        SEARCH_PRIORITY = searchPriority,
        CORPORATE_SITE = corporateSite,
        GBB_POPOVER_FLAG = gbbPopoverFlag,
        GBB_TITLE_TXT = gbbTitle
     WHERE upsell_master_id = masterIdIn;

  IF SQL%NOTFOUND THEN

    INSERT INTO UPSELL_MASTER
       (UPSELL_MASTER_ID,
        UPSELL_NAME,
        UPSELL_DESCRIPTION,
        UPSELL_STATUS,
        SEARCH_PRIORITY,
        CORPORATE_SITE,
        GBB_POPOVER_FLAG,
        GBB_TITLE_TXT)
    VALUES
       (masterIdIn,
        nameIn,
        description,
        status,
        searchPriority,
        corporateSite,
        gbbPopoverFlag,
        gbbTitle);

  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
  END;

end
;
.
/
