CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_OCC_ADDON_BY_OCC (
  occasionIn in number
)
as
--==============================================================================
--
-- Name:    SP_DELETE_OCC_ADDON_BY_OCC
-- Type:    Procedure
-- Syntax:  SP_DELETE_OCC_ADDON_BY_OCC ( occasionIn in number )
--
-- Description:   Deletes a row from OCCASION_ADDON by OCCASION_ID.
--
--==============================================================================
begin

 delete OCCASION_ADDON
 where OCCASION_ID = occasionIn;

end
;
.
/
