CREATE OR REPLACE
FUNCTION FTD_APPS.SP_GET_ADDON_BY_TYPE_DESC (IN_ADDON_TYPE_DESC ADDON_TYPE.DESCRIPTION%TYPE)
RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_ADDON_BY_TYPE_DESC
-- Type:    Function
-- Syntax:  SP_GET_ADDON_BY_TYPE_DESC (IN_ADDON_TYPE_DESC ADDON_TYPE.DESCRIPTION%TYPE)
-- Returns: ref_cursor for
--          ADDON_ID       VARCHAR2(10)
--          DESCRIPTION    VARCHAR2(50)
--
-- Description:   Queries the ADDON table to return all rows for the input ADDON_TYPE
--                where the description matches the inputted IN_ADDON_TYPE_DESC
--
--========================================================================================
AS
    addon_cursor types.ref_cursor;
BEGIN
    OPEN addon_cursor FOR
     SELECT FAA.ADDON_ID AS "addonId", FAA.DESCRIPTION as "description"
     FROM   FTD_APPS.ADDON FAA, FTD_APPS.ADDON_TYPE FAAT
     WHERE
       FAA.ADDON_TYPE = FAAT.ADDON_TYPE_ID
       AND FAAT.DESCRIPTION = IN_ADDON_TYPE_DESC
     ORDER BY FAA.ADDON_ID;

    RETURN addon_cursor;
END
;
.
/
