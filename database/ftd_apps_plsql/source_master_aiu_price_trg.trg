create or replace
TRIGGER ftd_apps.source_master_aiu_price_trg
AFTER UPDATE OF PRICE_HEADER_ID ON ftd_apps.source_master
REFERENCING new as new old as old
FOR EACH ROW

DECLARE

  v_amazon_source VARCHAR2(100);
  v_count         number := 0;    

  CURSOR amazon_product_cur IS
    select distinct product_id
    from ptn_amazon.az_product_master
		where 	catalog_status_standard='A'
				or catalog_status_deluxe='A'
				or catalog_status_premium='A';
               
  CURSOR iotw_cur IS
     SELECT i.product_id 
     FROM joe.iotw i, ptn_amazon.az_product_master pm
     WHERE  i.source_code = v_amazon_source
        AND i.iotw_source_code = :new.source_code
        AND (i.end_date IS NULL OR i.end_date >= trunc(sysdate))
        AND i.product_id = pm.product_id
        AND (catalog_status_standard='A'
          OR catalog_status_deluxe='A'
          OR catalog_status_premium='A');      

BEGIN

    SELECT VALUE INTO v_amazon_source FROM FRP.GLOBAL_PARMS WHERE NAME='default_source_code' AND CONTEXT = 'AMAZON_CONFIG';

    IF :new.source_code = v_amazon_source THEN
        FOR product_rec in amazon_product_cur LOOP

            select count(*) into v_count
            from ptn_amazon.az_price_feed
            where product_id = product_rec.product_id
            and feed_status = 'NEW';

            if v_count = 0 then

                insert into ptn_amazon.az_price_feed (
                    AZ_PRICE_FEED_ID,
                    PRODUCT_ID,
                    FEED_STATUS,
                    CREATED_ON,
                    CREATED_BY,
                    UPDATED_ON,
                    UPDATED_BY)
                values (
                    ptn_amazon.az_price_feed_id_sq.nextval,
                    product_rec.product_id,
                    'NEW',
                    sysdate,
                    'SYS',
                    sysdate,
                    'SYS');

            end if;

        END LOOP;
      END IF;
      
      FOR product_rec in iotw_cur LOOP

        select count(*) into v_count
        from ptn_amazon.az_price_feed
        where product_id = product_rec.product_id
        and feed_status = 'NEW';

        if v_count = 0 then

            insert into ptn_amazon.az_price_feed (
                AZ_PRICE_FEED_ID,
                PRODUCT_ID,
                FEED_STATUS,
                CREATED_ON,
                CREATED_BY,
                UPDATED_ON,
                UPDATED_BY)
            values (
                ptn_amazon.az_price_feed_id_sq.nextval,
                product_rec.product_id,
                'NEW',
                sysdate,
                'SYS',
                sysdate,
                'SYS');

        end if;

      END LOOP;

END source_master_aiu_price_trg;

.
/