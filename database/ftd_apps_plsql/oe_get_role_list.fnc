CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ROLE_LIST RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    SP_GET_ROLE_LIST
-- Type:    Function
-- Syntax:  SP_GET_ROLE_LIST( )
-- Returns: ref_cursor for
--          ROLE_ID      VARCHAR2(2)
--          DESCRIPTION  VARCHAR2(50)
--
-- Description:   Queries the role table and returns a ref cursor for row.
--
--===========================================================

AS
   role_cursor types.ref_cursor;
BEGIN
    OPEN role_cursor FOR
        SELECT ROLE_ID as "roleId",
               DESCRIPTION as "description"
          FROM role;

    RETURN role_cursor;
END
;
.
/
