CREATE OR REPLACE
FUNCTION          ftd_apps.SP_GET_PRODUCT_SOURCE (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_SOURCE
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_SOURCE ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--          SOURCE_CODE      VARCHAR2(25)
--
-- Description:   Queries the PRODUCT_SOURCE table by PRODUCT ID and returns
--                a ref cursor to the resulting row.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT PS.SOURCE_CODE as "sourceCode",
            SM.ORDER_SOURCE as "orderSource"
        FROM FTD_APPS.PRODUCT_SOURCE ps, FTD_APPS.SOURCE_MASTER SM
        WHERE PS.PRODUCT_ID = productId
        AND SM.SOURCE_CODE = PS.SOURCE_CODE;

    RETURN cur_cursor;
END
;
.
/
