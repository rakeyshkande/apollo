CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_CLIENT_URL (
  asnNumberIn in varchar2,
	clientNameIn in varchar2,
    urlIn in varchar2
)
as
--==============================================================================
--
-- Name:    CLIENT_URL
-- Type:    Procedure
-- Syntax:  CLIENT_URL ( asnNumberIn in varchar2,
--                                    clientNameIn in varchar2,
--                                      url in varchar2)
--
-- Description:   Updates a row to the BUYER_SOURCE_CODE table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 update CLIENT_URL
 Set CLIENT_NAME = clientNameIn,
     URL = urlIn
 WHERE ASN_NUMBER = asnNumberIn;

end
;
.
/
