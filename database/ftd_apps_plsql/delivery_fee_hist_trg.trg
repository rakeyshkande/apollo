CREATE OR REPLACE TRIGGER "FTD_APPS"."DELIVERY_FEE_HIST_TRG" 
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.DELIVERY_FEE REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW

BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.DELIVERY_FEE_HIST (
        delivery_fee_hist_id,
      	delivery_fee_id,
      	updated_on,
        morning_delivery_fee,
        expired_on
      ) VALUES (
        delivery_fee_hist_sq.nextval,
      	:NEW.delivery_fee_id,		
      	:NEW.updated_on,
        :NEW.morning_delivery_fee,
        null
      );

   ELSIF UPDATING  THEN

      update DELIVERY_FEE_HIST set expired_on = sysdate where delivery_fee_id = :new.delivery_fee_id and expired_on is null;

      INSERT INTO ftd_apps.DELIVERY_FEE_HIST (
        delivery_fee_hist_id,
      	delivery_fee_id,
      	updated_on,
        morning_delivery_fee,
        expired_on
      ) VALUES (
        delivery_fee_hist_sq.nextval,
      	:NEW.delivery_fee_id,		
      	:NEW.updated_on,
        :NEW.morning_delivery_fee,
        null
      );

   ELSIF DELETING  THEN

      update DELIVERY_FEE_HIST set expired_on = sysdate where delivery_fee_id = :old.delivery_fee_id and expired_on is null;

   END IF;

END;

/
ALTER TRIGGER "FTD_APPS"."DELIVERY_FEE_HIST_TRG" ENABLE;
