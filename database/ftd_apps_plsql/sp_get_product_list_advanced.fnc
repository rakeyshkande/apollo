CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_LIST_ADVANCED (
  inAvail        IN CHAR,
  inUnavail      IN CHAR,
  inDateRestrict IN CHAR,
  inOnHold       IN CHAR,
  pquadProductid IN NUMBER
  
)
RETURN types.ref_cursor

--=============================================================================
--
-- Name:    SP_GET_PRODUCT_LIST_ADVANCED
--
-- Description: Queries the PRODUCT_MASTER table and returns a ref cursor
--              to the resulting rows.  The input parameters determine
--              which filtering criteria should be applied to the query.
--              Only input parameters set to 'Y' will take effect (null or
--              other will be ignored).
--
--=========================================================
AS
    product_list_cursor types.ref_cursor;
BEGIN
    OPEN product_list_cursor FOR
        SELECT PRODUCT_ID            as "productId",
               NOVATOR_NAME          as "novatorName",
               STATUS                as "status",
               EXCEPTION_START_DATE  as "exceptionStartDate",
               EXCEPTION_END_DATE    as "exceptionEndDate",
               HOLD_UNTIL_AVAILABLE  as "holdUntilAvailable"
          FROM PRODUCT_MASTER
          WHERE
          -- Product is available
          (NVL(inAvail,'N') = 'Y' AND STATUS = 'A')
          OR
          -- Product is unavailable
          (NVL(inUnavail,'N') = 'Y' AND STATUS != 'A')
          OR
          -- Product is available and has date restrictions
          (NVL(inDateRestrict,'N') = 'Y' AND STATUS = 'A'
               AND (EXCEPTION_START_DATE IS NOT NULL OR EXCEPTION_END_DATE IS NOT NULL))
          OR
          -- Product is on hold
          (NVL(inOnHold,'N') = 'Y' AND HOLD_UNTIL_AVAILABLE = 'Y')
          
          OR
            PQUAD_PRODUCT_ID =pquadProductid
            
          ORDER BY STATUS, PRODUCT_ID;

    RETURN product_list_cursor;
END;
.
/
