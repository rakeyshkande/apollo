CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_CLIENT_URL (
  asnNumberIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_DELETE_CLIENT_URL
-- Type:    Procedure
-- Syntax:  SP_DELETE_CLIENT_URL ( asnNumberIn in varchar2)
--
-- Description:   Deletes a row in the CLIENT_URL table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 delete CLIENT_URL
 where ASN_NUMBER = asnNumberIn;

end
;
.
/
