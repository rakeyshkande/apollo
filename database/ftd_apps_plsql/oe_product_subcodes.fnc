CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODUCT_SUBCODES
    (inProductId IN VARCHAR2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_PRODUCT_SUBCODES
-- Type:    Function
-- Syntax:  OE_PRODUCT_SUBCODES(inProductId IN VARCHAR2)
-- Returns: ref_cursor for
--          productSubcodeId        VARCHAR2(10)
--          subcodeDescription      VARCHAR2(100)
--          subcodePrice            NUMBER(8,2)
--          subcodeReferenceNumber  VARCHAR2(40)
--          holidaySku              VARCHAR2(6)
--          holidayPrice            NUMBER(8,2)
--
-- Description:   Queries the PRODUCT_SUBCODES table by PRODUCT_ID.  Returns any
--                active subcodes for the product with detail information.
--
--===============================================================================
AS
    subcode_cursor types.ref_cursor;
BEGIN
    OPEN subcode_cursor FOR
        SELECT ps.PRODUCT_SUBCODE_ID as "productSubcodeId",
                ps.SUBCODE_DESCRIPTION as "subcodeDescription",
                TO_CHAR(ps.SUBCODE_PRICE, '9999999.00') as "subcodePrice",
                ps.SUBCODE_REFERENCE_NUMBER as "subcodeReferenceNumber",
                ps.HOLIDAY_SKU as "holidaySku",
                ps.HOLIDAY_PRICE as "holidayPrice"
        FROM PRODUCT_SUBCODES ps
        WHERE ps.PRODUCT_ID = UPPER(inProductId)
        AND NVL(ps.ACTIVE_FLAG, 'N') = 'Y'
        ORDER BY ps.SUBCODE_DESCRIPTION;

    RETURN subcode_cursor;
END
;
.
/
