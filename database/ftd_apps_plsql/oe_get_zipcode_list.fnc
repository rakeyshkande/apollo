CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIPCODE_LIST
    (zipIn IN varchar2, cityIn IN varchar2, stateNameIn IN
    varchar2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_ZIPCODE_LIST
-- Type:    Function
-- Syntax:  OE_GET_ZIPCODE_LIST(zipIn IN VARCHAR2,
--                              cityIn IN VARCHAR2,
--                              stateNameIn IN VARCHAR2)
-- Returns: ref_cursor for
--          ZIP_CODE_ID   VARCHAR2(10)
--          CITY          VARCHAR2(40)
--          STATE_NAME    VARCHAR2(30)
--
-- Description:   Queries ZIP_CODE and STATE_MASTER by any combination of
--                city/state/zipcode, returns a ref cursor to those three fields.
--
--===============================================================================
AS
    zipcode_cursor      types.ref_cursor;
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
BEGIN
    OPEN zipcode_cursor FOR
        SELECT Z.ZIP_CODE_ID as "zipCode",
               NLS_INITCAP(Z.CITY) as "city",
               S.STATE_MASTER_ID  as "state",
               S.COUNTRY_CODE as "countryCode"
           FROM STATE_MASTER S, ZIP_CODE Z
            WHERE Z.ZIP_CODE_ID = NVL(zipIn, Z.ZIP_CODE_ID)
            AND S.STATE_MASTER_ID = NVL(stateNameIn, S.STATE_MASTER_ID)
            AND Z.CITY like DECODE(cityIn, NULL, Z.CITY, UPPER(cityIn) || '%')
            AND Z.STATE_ID = S.STATE_MASTER_ID

            -- Do not allow invalid Canadian zips to return
            -- Canadian must start with <alpha><num><alpha>
            AND ( TRANSLATE(Z.ZIP_CODE_ID, alphaChars, alphaSub) NOT LIKE '%@%'
              OR  TRANSLATE(Z.ZIP_CODE_ID, alphaChars || numChars,
                            alphaSub || numSub) LIKE '@#@%'
            )

            ORDER BY Z.CITY, S.STATE_NAME, Z.ZIP_CODE_ID;

    RETURN zipcode_cursor;
END;
.
/
