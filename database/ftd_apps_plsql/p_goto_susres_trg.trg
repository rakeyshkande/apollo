CREATE OR REPLACE
TRIGGER ftd_apps.p_goto_susres_trg AFTER INSERT ON ftd_apps.hp_goto_susres
REFERENCING OLD AS old NEW AS new FOR EACH ROW
DECLARE
   v_stat VARCHAR2(1);
   v_mess VARCHAR2(1000);
   v_suspend_indicator VARCHAR2(1);
BEGIN
   IF UPPER(:new.SUSRES_TEXT) LIKE '%SUSPEND%' THEN
      v_suspend_indicator := 'Y';
      ftd_apps.florist_maint_pkg.update_florist_goto_suspend(
            in_florist_id              =>:new.florist_id,
            in_suspend_indicator       =>v_suspend_indicator,
            in_goto_florist_sus_merc_id=>:new.mercury_id,
            in_goto_florist_res_merc_id=>NULL,
            out_status                 =>v_stat,
            out_message                =>v_mess);
   ELSE
      v_suspend_indicator := 'N';
      ftd_apps.florist_maint_pkg.update_florist_goto_suspend(
            in_florist_id                =>:new.florist_id,
            in_suspend_indicator         =>v_suspend_indicator,
            in_goto_florist_sus_merc_id  =>NULL,
            in_goto_florist_res_merc_id  =>:new.mercury_id,
            out_status                   =>v_stat,
            out_message                  =>v_mess);
   END IF;
END;
.
/
