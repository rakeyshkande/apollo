CREATE OR REPLACE
PROCEDURE ftd_apps.delete_hp_actions (p_begin_range IN NUMBER, p_end_range IN NUMBER) IS
-- Delete a range of rows in hp_actions.  For support use only, not for application use.
-- Does not commit, does not trap errors.  The intention is to run this, then query the table to make sure it had the desired
-- effect, before committing.
BEGIN
   DELETE FROM hp_actions WHERE hp_update_id BETWEEN p_begin_range AND p_end_range;
END delete_hp_actions;
.
/
