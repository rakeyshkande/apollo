CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_STATE_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_STATE_LIST
-- Type:    Function
-- Syntax:  SP_GET_STATE_LIST ()
-- Returns: ref_cursor for
--          STATE_MASTER_ID VARCHAR2(2)
--          STATE_NAME      VARCHAR2(30)
--          COUNTRY_CODE    VARCHAR2(3)
--          TIME_ZONE       VARCHAR2(2)
--          TAX_RATE        NUMBER(5)
--
--
-- Description:   Queries the STATE_MASTER table and
--                returns a ref cursor for resulting row.
--
--=============================================================
AS
    state_cursor types.ref_cursor;
BEGIN
    OPEN state_cursor FOR
        SELECT STATE_MASTER_ID as "stateMasterId",
               STATE_NAME      as "stateName",
               COUNTRY_CODE    as "countryCode",
               TIME_ZONE       as "timeZone"
        FROM STATE_MASTER
        ORDER BY STATE_NAME;

    RETURN state_cursor;
END
;
.
/
