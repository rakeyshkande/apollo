CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_RECIPIENT_SRCH (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_RECIPIENT_SRCH
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_RECIPIENT_SRCH (productId in VARCHAR2 )
-- Returns: ref_cursor for
--          RECIPIENT_SEARCH_ID    VARCHAR2(2)
--
-- Description:   Queries the PRODUCT_RECIPIENT_SEARCH table by PRODUCT_ID and
--                returns a ref cursor for resulting row.
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT RECIPIENT_SEARCH_ID as "recipientSearchId"
          FROM PRODUCT_RECIPIENT_SEARCH
          WHERE PRODUCT_ID = productId;

    RETURN cur_cursor;
END
;
.
/
