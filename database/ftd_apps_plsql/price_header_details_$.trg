-- FTD_APPS.PRICE_HEADER_DETAILS_$ trigger to populate FTD_APPS.PRICE_HEADER_DETAILS$ shadow table

CREATE OR REPLACE TRIGGER ftd_apps.price_header_details_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.price_header_details REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.price_header_details$ (
      	price_header_id,
      	min_dollar_amt,
      	max_dollar_amt,
      	discount_amt,
      	discount_type,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
	      :NEW.price_header_id,
	      :NEW.min_dollar_amt,
	      :NEW.max_dollar_amt,
	      :NEW.discount_amt,
	      :NEW.discount_type,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,  	      
	      'INS',
	      SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.price_header_details$ (
      	price_header_id,
      	min_dollar_amt,
      	max_dollar_amt,
     		discount_amt,
      	discount_type,
       	created_by,
       	created_on,
       	updated_by,
       	updated_on,
       	operation$, 
      	timestamp$
      ) VALUES (
	      :OLD.price_header_id,
	      :OLD.min_dollar_amt,
	      :OLD.max_dollar_amt,
	      :OLD.discount_amt,
	      :OLD.discount_type,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,  	      
 	      'UPD_OLD',
 	      SYSDATE);

      INSERT INTO ftd_apps.price_header_details$ (
      	price_header_id,
      	min_dollar_amt,
      	max_dollar_amt,
      	discount_amt,
      	discount_type,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$ 	
      ) VALUES (
      	:NEW.price_header_id,
      	:NEW.min_dollar_amt,
      	:NEW.max_dollar_amt,
      	:NEW.discount_amt,
      	:NEW.discount_type,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,        	
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.price_header_details$ (
      	price_header_id,
      	min_dollar_amt,
      	max_dollar_amt,
      	discount_amt,
      	discount_type,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.price_header_id,
      	:OLD.min_dollar_amt,
      	:OLD.max_dollar_amt,
      	:OLD.discount_amt,
      	:OLD.discount_type,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,        	
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
