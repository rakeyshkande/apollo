CREATE OR REPLACE
PACKAGE BODY ftd_apps.FEES_PKG AS


PROCEDURE GET_FUEL_SURCHARGE
(
IN_START_DATE                    IN FUEL_SURCHARGE.START_DATE%TYPE,
OUT_FUEL_SURCHARGE_AMT          OUT FUEL_SURCHARGE.FUEL_SURCHARGE_AMT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
BEGIN

  IF IN_START_DATE is null THEN
    SELECT fuel_surcharge_amt
    INTO   OUT_FUEL_SURCHARGE_AMT
    FROM   ftd_apps.fuel_surcharge
    WHERE  end_date is null;
  ELSE
    SELECT fuel_surcharge_amt
    INTO   OUT_FUEL_SURCHARGE_AMT
    FROM   ftd_apps.fuel_surcharge
    WHERE  IN_START_DATE >= start_date
    AND    (end_date is null or IN_START_DATE <= end_date);
  END IF;

OUT_STATUS := 'Y';

EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      OUT_FUEL_SURCHARGE_AMT := 0;
    END;

  WHEN others THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_FUEL_SURCHARGE;

PROCEDURE UPDATE_FUEL_SURCHARGE
(
IN_FUEL_SURCHARGE_ON_OFF_FLAG  IN  VARCHAR2,
IN_FUEL_SURCHARGE_AMT          IN FUEL_SURCHARGE.FUEL_SURCHARGE_AMT%TYPE,
IN_FUEL_SURCHARGE_DESC         IN  VARCHAR2,
IN_SEND_SURCHARGE_TO_FLORIST   IN  VARCHAR2,
IN_DISPLAY_SURCHARGE           IN  VARCHAR2,
IN_UPDATED_BY			             IN FUEL_SURCHARGE.UPDATED_BY%TYPE,
OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                    OUT VARCHAR2
)
AS
/************************************************************************
This procecure is responsible for updating the fuel surcharge.
It facilitates tracking of update history.

INPUT:
	in_fuel_surcharge_amt	NUMBER
OUTPUT:
	out_status		VARCHAR2
	out_message		VARCHAR2

*************************************************************************/

v_last_end_date		FUEL_SURCHARGE.END_DATE%TYPE;
v_start_date		FUEL_SURCHARGE.START_DATE%TYPE;


BEGIN
	out_status := 'N';
	v_last_end_date := sysdate;
	
	-- next second
	v_start_date := v_last_end_date + 1/86400;
	

	BEGIN
		UPDATE fuel_surcharge
		SET end_date = v_last_end_date,
		    updated_by = IN_UPDATED_BY,
		    updated_on = v_last_end_date
		WHERE end_date is null
		;
		
		-- Ignore no_data_found exception
		EXCEPTION WHEN NO_DATA_FOUND THEN
		NULL;
	END;
	
	INSERT INTO fuel_surcharge
		    (start_date,
		     end_date,
         	     apply_surcharge_code,
		     fuel_surcharge_amt,
	             surcharge_description,
	             display_surcharge_flag,
	             send_surcharge_to_florist_flag,
		     created_on,
		     created_by,
		     updated_on,
		     updated_by
		     )
	VALUES (v_start_date,
		null,
    IN_FUEL_SURCHARGE_ON_OFF_FLAG,
		IN_FUEL_SURCHARGE_AMT,
    IN_FUEL_SURCHARGE_DESC,
    IN_DISPLAY_SURCHARGE,
    IN_SEND_SURCHARGE_TO_FLORIST, 
		v_start_date,
		IN_UPDATED_BY,
		v_start_date,
		IN_UPDATED_BY
		);

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        
END UPDATE_FUEL_SURCHARGE;

PROCEDURE GET_GENERIC_SURCHARGE_VALUES
(
IN_START_DATE                   IN FUEL_SURCHARGE.START_DATE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
BEGIN

  IF IN_START_DATE is null THEN
  OPEN out_cur for
    SELECT 
    apply_surcharge_code, 
    fuel_surcharge_amt, 
    surcharge_description,
    display_surcharge_flag, 
    send_surcharge_to_florist_flag 
    FROM   ftd_apps.fuel_surcharge
    WHERE  end_date is null;
  ELSE
  OPEN out_cur for
    SELECT 
    apply_surcharge_code, 
    fuel_surcharge_amt, 
    surcharge_description,
    display_surcharge_flag, 
    send_surcharge_to_florist_flag 
    FROM   ftd_apps.fuel_surcharge
    WHERE  IN_START_DATE >= start_date
    AND    (end_date is null or IN_START_DATE <= end_date);
  END IF;

END GET_GENERIC_SURCHARGE_VALUES;
END FEES_PKG;
.
/