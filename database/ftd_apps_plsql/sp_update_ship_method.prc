CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_SHIP_METHOD (
 productId in varchar2,
 shippingMethod in varchar2,
 defaultCarrier in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_SHIP_METHOD
-- Type:    Procedure
-- Syntax:  SP_UPDATE_SHIP_METHOD ( productId in varchar2,
--                                  shippingMethod in varchar2 )
--
-- Description:   Attempts to insert into PRODUCT_SHIP_METHODS.  If the product /
--                shipping method combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_SHIP_METHODS
  INSERT INTO PRODUCT_SHIP_METHODS
      ( PRODUCT_ID,
				SHIP_METHOD_ID,
				DATE_LAST_MODIFIED,
                DEFAULT_CARRIER
		  )
  VALUES
		  ( productId,
				shippingMethod,
				SYSDATE,
                defaultCarrier
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		  rollback;
end
;
.
/
