CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_CALL_CENTER_LIST RETURN types.ref_cursor
--==========================================================
--
-- Name:    OE_GET_CALL_CENTER_LIST
-- Type:    Function
-- Syntax:  OE_GET_CALL_CENTER_LIST( )
-- Returns: ref_cursor for
--          CALL_CENTER_ID    VARCHAR2(2)
--          DESCRIPTION       VARCHAR2(50)
--
-- Description:   Queries the call_center table for all rows.
--
--===========================================================

AS
   cc_cursor types.ref_cursor;
BEGIN
    OPEN cc_cursor FOR
        SELECT CALL_CENTER_ID as "callCenterId",
               DESCRIPTION as "description"
          FROM call_center;

    RETURN cc_cursor;
END
;
.
/
