CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_HOLIDAY_PRICING ( startDate in date,
 endDate in date,
 holidayPriceFlag in varchar2,
 deliveryDateFlag in varchar2 )

as
--==============================================================================
--
-- Name:    SP_UPDATE_HOLIDAY_PRICING
-- Type:    Procedure
-- Syntax:  SP_UPDATE_HOLIDAY_PRICING ( startDate in date,
--                                      endDate in date,
--                                      holidayPriceFlag in varchar2,
--                                      deliveryDateFlag in varchar2 )
--
-- Description:   Updates HOLIDAY_PRICING to set a new effective date range.
--
--==============================================================================
begin
 update HOLIDAY_PRICING
 set HOLIDAY_PRICING.START_DATE=startDate,
     HOLIDAY_PRICING.END_DATE=endDate,
     HOLIDAY_PRICING.HOLIDAY_PRICE_FLAG=holidayPriceFlag,
     HOLIDAY_PRICING.DELIVERY_DATE_FLAG=deliveryDateFlag;
end
;
.
/
