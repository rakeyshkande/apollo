CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_USER (username IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_USER
-- Type:    Function
-- Syntax:  OE_GET_USER (username IN varchar2)
-- Returns: ref_cursor for
--         USER_ID                VARCHAR2(8)
--         FIRST_NAME             VARCHAR2(25)
--         LAST_NAME              VARCHAR2(25)
--         CURRENT_PASSWORD_DATE  DATE
--         LAST_UPDATED_BY        VARCHAR2(8)
--         LAST_UPDATED_DATE      DATE
--         CALL_CENTER_ID         VARCHAR2(2)
--         ACTIVE_FLAG            VARCHAR2(1)
--         CURRENT_PASSWORD       RAW(30)
--         LOGIN_ATTEMPTS         NUMBER(10)
--
--
-- Description:   Queries the USER_PROFILE table by username = user_id and
--                returns a ref cursor for resulting row.
--
--=============================================================
AS
    user_cursor types.ref_cursor;
BEGIN
    OPEN user_cursor FOR
        SELECT USER_ID               as "userId",
               ROLE_ID               as "roleID",
               CALL_CENTER_ID        as "callCenterId",
               FIRST_NAME            as "firstName",
               LAST_NAME             as "lastName",
               CURRENT_PASSWORD      as "currentPassword",
               CURRENT_PASSWORD_DATE as "currentPasswordDate",
               ACTIVE_FLAG           as "activeFlag",
               DELETED_FLAG          as "deletedFlag",
               LOGIN_ATTEMPTS        as "loginAttempts",
               LAST_UPDATED_BY       as "lastUpdatedBy",
               LAST_UPDATED_DATE     as "lastUpdatedDate"
        FROM USER_PROFILE
    WHERE USER_ID = UPPER(username);

    RETURN user_cursor;
END
;
.
/
