CREATE OR REPLACE
PACKAGE BODY FTD_APPS.OSCAR_MAINT_PKG AS

  PROCEDURE SET_OSCAR_FLAG_BY_ZIP_CODE 
(
  IN_ZIP_CODE_LIST IN VARCHAR2, 
  IN_OSCAR_FLAG IN FTD_APPS.ZIP_CODE.OSCAR_ZIP_FLAG%TYPE, 
  IN_UPDATED_BY IN FTD_APPS.ZIP_CODE.UPDATED_BY%TYPE, 
  OUT_STATUS OUT VARCHAR2, 
  OUT_MESSAGE OUT VARCHAR2  
) AS
/*-----------------------------------------------------------------------------
Description:
    Sets the OSCAR_ZIP_FLAG to the value passed in the input parameter, IN_OSCAR_FLAG,
    for all the ZIP_CODE_IDs in the IN_ZIP_CODE_LIST
	
Input:
    IN_ZIP_CODE_LIST - Java String containing a comma-separated list of ZIP_CODE_IDs
    IN_OSCAR_FLAG - String containing the value of the flag to be set
    IN_UPDATED_BY

Output:
    OUT_STATUS - Y/N, indicating success or failure
    OUT_MESSAGE - Error message, including SQLCODE

-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

v_sql_stmt VARCHAR2(32767);
v_java_list VARCHAR2(32767);
v_sql_list VARCHAR2(32767);

BEGIN

    OUT_STATUS := 'Y';
    
    v_sql_list := convert_to_valid_sql_clause(IN_ZIP_CODE_LIST);

    v_sql_stmt := 'UPDATE ftd_apps.zip_code 
                   SET oscar_zip_flag = :p_oscar_zip,
                       updated_by = '''||IN_UPDATED_BY||''',
		       updated_on = sysdate 
                   WHERE zip_code_id IN (' || v_sql_list || ')
                   AND '''||IN_OSCAR_FLAG||''' is not null
                   AND oscar_zip_flag != '''||IN_OSCAR_FLAG||''' ';
	
    EXECUTE IMMEDIATE v_sql_stmt USING IN_OSCAR_FLAG;
    
    commit;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'SET_OSCAR_FLAG_BY_ZIP_CODE: ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END SET_OSCAR_FLAG_BY_ZIP_CODE;

  FUNCTION CONVERT_TO_VALID_SQL_CLAUSE 
(
    IN_LIST IN VARCHAR2
)RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a comma delimited list of strings for use in a SQL statement
    
Input:
    IN_LIST - comma delimited list of Java Strings

Output:
    - VARCHAR2 containing a comma separate list of text

-----------------------------------------------------------------------------*/

v_return_string VARCHAR2(32767);
v_list VARCHAR2(32767);
v_comma_pos NUMBER(3);
v_value VARCHAR2(100);
v_value_plus_comma VARCHAR(100);

BEGIN
    v_list := in_list;
--    v_value_plus_comma := in_list;
    
    WHILE INSTR(v_list,',') <> 0 LOOP
  	    v_comma_pos := INSTR(v_list,',');
  	    v_value := TRIM(SUBSTR(v_list,1,v_comma_pos-1));
  	    v_value_plus_comma := ''''||v_value||''',';
  	    v_return_string := v_return_string || v_value_plus_comma;
        v_list := SUBSTR(v_list, v_comma_pos+1);
    END LOOP;
    
    v_value := RTRIM(ltrim(v_list,' '),' ');
    v_return_string := v_return_string ||''''||v_value||'''';
    
    RETURN v_return_string;
  END CONVERT_TO_VALID_SQL_CLAUSE;

  PROCEDURE SET_OSCAR_FLAG_BY_CITY_STATE 
(
  IN_CITY_STATE_LIST IN VARCHAR2, 
  IN_OSCAR_FLAG IN ZIP_CODE.OSCAR_CITY_STATE_FLAG%TYPE,
  IN_UPDATED_BY IN FTD_APPS.ZIP_CODE.UPDATED_BY%TYPE,  
  OUT_STATUS OUT VARCHAR2, 
  OUT_MESSAGE OUT VARCHAR2  
) AS
/*-----------------------------------------------------------------------------
Description:
    Sets the OSCAR_CITY_STATE_FLAG to the value passed in the input parameter, IN_OSCAR_FLAG,
    for all the MATCHING CITY/STATE combinations in the IN_CITY_STATE_LIST
	
Input:
    IN_CITY_STATE_LIST - Java String containing a comma-separated list of ZIP_CODE_IDs
    IN_OSCAR_FLAG - String containing the value of the flag to be set
    IN_UPDATED_BY	

Output:
    OUT_STATUS - Y/N, indicating success or failure
    OUT_MESSAGE - Error message, including SQLCODE


-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

c_open_clause CONSTANT VARCHAR2(13) := ' (city = ''';
c_and_clause CONSTANT VARCHAR2(23) := ''' AND state_id = ''';
c_close_clause CONSTANT VARCHAR2(5) := ''') ';
c_or_clause CONSTANT VARCHAR2(2) := 'OR';

v_where_clause VARCHAR2(32767);
v_and_clause VARCHAR2(32767);
v_list VARCHAR2(32767);
v_semicolon_pos INTEGER;
v_comma_pos INTEGER;
v_city_state VARCHAR2(100);
v_city VARCHAR(40);
v_state VARCHAR(2);

v_sql_stmt VARCHAR2(32767);

BEGIN
    OUT_STATUS := 'Y';
    
    v_sql_stmt := 'UPDATE ftd_apps.zip_code
                   SET oscar_city_state_flag = :p_oscar_zip,
                   updated_by = '''||IN_UPDATED_BY||''',
                   updated_on = sysdate
                   WHERE ( ';
               

    
    v_list := IN_CITY_STATE_LIST;
    -- replace a single quote within a city name with an escaped quote(ie. two single quotes)
    v_list := REPLACE(v_list,'''','''''');
    
    WHILE INSTR(v_list,';') <> 0 LOOP
  	    v_semicolon_pos := INSTR(v_list,';');
  	    v_city_state := TRIM(SUBSTR(v_list,1,v_semicolon_pos-1));
        v_comma_pos := INSTR(v_city_state, ',', -1);
        v_city := UPPER(TRIM(SUBSTR(v_city_state,1,v_comma_pos-1)));
        v_state := UPPER(TRIM(SUBSTR(v_city_state, v_comma_pos+1)));
  	v_where_clause := v_where_clause || c_or_clause || c_open_clause || v_city || c_and_clause || v_state || c_close_clause;
        v_list := SUBSTR(v_list, v_semicolon_pos+1);
    END LOOP;

    -- process final entry    
    v_comma_pos := INSTR(v_list, ',', -1);
    IF (v_comma_pos > 1) THEN
    v_city := UPPER(TRIM(SUBSTR(v_list,1,v_comma_pos-1)));
    v_state := UPPER(TRIM(SUBSTR(v_list, v_comma_pos+1)));
    v_where_clause := v_where_clause || c_or_clause || c_open_clause || v_city || c_and_clause || v_state || c_close_clause;
    END IF;
    
    -- add logic to remove extra OR at beginning
    v_where_clause := SUBSTR(v_where_clause, LENGTH(c_or_clause)+2);
	
    --add final and clause to where clause
    v_and_clause := ') AND '''||IN_OSCAR_FLAG||''' is not null
                   AND oscar_city_state_flag != '''||IN_OSCAR_FLAG||''' ';
    v_where_clause := v_where_clause || v_and_clause;
    v_sql_stmt := v_sql_stmt || v_where_clause;
	
    EXECUTE IMMEDIATE v_sql_stmt USING IN_OSCAR_FLAG;
    
    commit;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'SET_OSCAR_FLAG_BY_CITY_STATE: ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END SET_OSCAR_FLAG_BY_CITY_STATE;

END;
.
/
