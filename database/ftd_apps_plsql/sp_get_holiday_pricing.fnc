CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_HOLIDAY_PRICING RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    SP_GET_HOLIDAY_PRICING
-- Type:    Function
-- Syntax:  SP_GET_HOLIDAY_PRICING ( )
-- Returns: ref_cursor for
--          START_DATE            DATE
--          END_DATE              DATE
--          HOLIDAY_PRICING_FLAG  VARCHAR2(1)
--          DELIVERY_DATE_FLAG    VARCHAR2(1)
--
-- Description:   Queries the HOLIDAY_PRICING table and returns a ref cursor for row.
--
--===========================================================

AS
    holiday_cursor types.ref_cursor;
BEGIN
    OPEN holiday_cursor FOR
        SELECT START_DATE         as "startDate",
               END_DATE           as "endDate",
               HOLIDAY_PRICE_FLAG as "holidayPriceFlag",
               DELIVERY_DATE_FLAG as "deliveryDateFlag"
          FROM HOLIDAY_PRICING;

    RETURN holiday_cursor;
END
;
.
/
