create or replace function ftd_apps.oe_get_prod_delivblock_start
(
  IN_PRODUCT_ID IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_DELIV_BLOCK_RANK IN NUMBER
) 
return DATE is
--==============================================================================
--
-- Name:    OE_GET_PROD_DELIVBLOCK_START
-- Type:    Function
-- Syntax:  OE_GET_PROD_DELIVBLOCK_START (product_id varchar2)
-- Returns: START_DATE DATE
--
--
-- Description:   START DATE FOR THE PASSED IN PARAMETERS
--
--==============================================================================
v_date DATE;
begin
     SELECT START_DATE INTO v_date
       FROM PROD_DELIV_BLOCK_LIST_VW  
       WHERE product_id = IN_PRODUCT_ID AND "COUNT" = IN_DELIV_BLOCK_RANK;
       
     return v_date;
end oe_get_prod_delivblock_start;
/
