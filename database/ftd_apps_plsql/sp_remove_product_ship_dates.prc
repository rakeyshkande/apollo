CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_PRODUCT_SHIP_DATES (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_PRODUCT_SHIP_DATES
-- Type:    Procedure
-- Syntax:  SP_REMOVE_PRODUCT_SHIP_DATES ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_SHIP_DATES by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_SHIP_DATES
  where PRODUCT_ID = productId;

end
;
.
/
