CREATE OR REPLACE
FUNCTION ftd_apps.OE_INDEX_DETAILS ( indexId IN NUMBER )
 RETURN types.ref_cursor
--==========================================================================
--
-- Name:    OE_INDEX_DETAILS
-- Type:    Function
-- Syntax:  OE_INDEX_DETAILS ()
-- Returns: ref_cursor for
--          indexId               NUMBER
--          name                  VARCHAR2(100)
--          productCount          NUMBER
--          subIndices            VARCHAR2
--          liveIndex             VARCHAR2(1)
--          searchIndex           VARCHAR2(1)
--          webLiveIndex          VARCHAR2(1)
--          callCenterLiveIndex   VARCHAR2(1)
--          webTest1Index         VARCHAR2(1)
--          webTest2Index         VARCHAR2(1)
--          webTest3Index         VARCHAR2(1)
--          callCenterTestIndex   VARCHAR2(1)
--
-- Description:   Queries the PRODUCT_INDEX table and returns a ref cursor
--                listing all rows with their subindices and product counts.
--
--==========================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    -- Ignore search indexes for purposes of this query
    OPEN cur_cursor FOR
        SELECT pi.index_id as "indexId",
              pi.index_name as "indexName",
              pt.index_name as "parentIndexName"
         FROM PRODUCT_INDEX pi, PRODUCT_INDEX pt
         WHERE pi.index_id = indexId
         AND pi.parent_index_id = pt.index_id (+);

    RETURN cur_cursor;
END;
.
/
