CREATE OR REPLACE
PROCEDURE ftd_apps.PRODUCT_HP_COUNT               AS

 v_row_count NUMBER := 0;
 nbt_mail_ip1   varchar2(40);
 nbt_connection   utl_smtp.connection;
 reply          utl_smtp.reply;
 nbt_body varchar2(30000) := '';
 v_sender       varchar2(1000);

 BEGIN

  select count(*) into v_row_count from product_hp;

  if v_row_count >= 5 then
    nbt_body := 'Product_HP count = ' || v_row_count;

    nbt_mail_ip1 := UTL_INADDR.GET_HOST_ADDRESS('ftd-mailer1');
    nbt_connection := utl_smtp.open_connection(nbt_mail_ip1);
    utl_smtp.helo(nbt_connection, nbt_mail_ip1);

    v_sender := '<tschmig@ftdi.com>';

    --specify the sender and recipient of the mail
    utl_smtp.mail(nbt_connection, v_sender);
    --  utl_smtp.mail(nbt_connection, in_sender_email);
    reply := utl_smtp.rcpt(nbt_connection, 'tschmig@ftdi.com');
    reply := utl_smtp.rcpt(nbt_connection, 'dehardt@ftdi.com');
    reply := utl_smtp.rcpt(nbt_connection, 'dross@ftdi.com');

    --open a data connection to create the mail
    utl_smtp.open_data(nbt_connection);

    --Write out the subject line and the Recipient (can be email address or name)
    utl_smtp.write_data(nbt_connection, 'Subject:  product_hp count'  || utl_tcp.CRLF);
    utl_smtp.write_data(nbt_connection, 'To:  tschmig@ftdi.com dehardt@ftdi.com dross@ftdi.com' || utl_tcp.CRLF);
    utl_smtp.write_data(nbt_connection, 'From:  oracle@sparta.ftdi.com' || utl_tcp.CRLF);

    --Write out the body.
    utl_smtp.write_data(nbt_connection, utl_tcp.CRLF || nbt_body);
    utl_smtp.write_data(nbt_connection, utl_tcp.CRLF);
    utl_smtp.write_data(nbt_connection, utl_tcp.CRLF || 'This message originated from Oracle procedure product_hp_count');

    --The body MUST end with a CRLF
    utl_smtp.write_data(nbt_connection, utl_tcp.CRLF);

    --Send the email
    reply := utl_smtp.close_data(nbt_connection);

    utl_smtp.quit(nbt_connection);

    dbms_output.enable(30000);
    dbms_output.put_line('Email sent - count = ' || v_row_count);

  end if;

EXCEPTION
WHEN OTHERS THEN
  raise_application_error(-20001, 'Error:  ' || SQLERRM);

END
;
.
/
