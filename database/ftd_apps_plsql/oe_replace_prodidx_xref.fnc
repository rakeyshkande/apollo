CREATE OR REPLACE
FUNCTION ftd_apps.OE_REPLACE_PRODIDX_XREF (originalProduct IN varchar2, newProduct IN varchar2)
    RETURN types.ref_cursor
--===================================================================
--
-- Name:    OE_REPLACE_PRODIDX_XREF
-- Type:    Function
-- Syntax:  OE_REPLACE_PRODIDX_XREF ( originalProduct IN varchar2,
--                                    newProduct IN varchar2 )
-- Returns: ref_cursor for
--          countUpdated    NUMBER
--
-- Description:   Updates rows on PRODUCT_INDEX_XREF with a PRODUCT_ID
--                of originalProduct to replace that product ID with
--                newProduct, returns the count of rows updated.
--
--===================================================================
AS
    count_updated     NUMBER := 0;
    count_cursor      types.ref_cursor;
BEGIN
    UPDATE PRODUCT_INDEX_XREF
      SET product_id = newProduct
    WHERE product_id = originalProduct;

    count_updated := SQL%ROWCOUNT;
    COMMIT;

    OPEN count_cursor FOR
      SELECT count_updated as "countUpdated"
      FROM DUAL;

    RETURN count_cursor;
END
;
.
/
