CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_UPSELL_DETAILS (inMasterId IN VARCHAR2)
 RETURN Types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_UPSELL_DETAILS
-- Type:    Function
-- Syntax:  OE_GET_UPSELL_DETAILS (inMasterId IN VARCHAR2 )
-- Returns: upsellMasterId                  VARCHAR2(30)
--          upsellMasterName                VARCHAR2(100)
--          upsellMasterDescription         VARCHAR2(512)
--          upsellDetailId                  VARCHAR2(4)
--          upsellDetailName                VARCHAR2(25)
--          upsellDetailSequence            NUMBER
--
-- Description:   Queries the STATE_MASTER table by STATE_MASTER_ID, returns the results
--
--
--==============================================================================
AS
    cur_cursor Types.ref_cursor;
BEGIN

  OPEN cur_cursor FOR
      SELECT um.UPSELL_MASTER_ID AS "upsellMasterId",
            um.UPSELL_NAME AS "upsellMasterName",
            um.UPSELL_DESCRIPTION AS "upsellMasterDescription",
			um.UPSELL_STATUS AS "upsellMasterStatus",
            ud.UPSELL_DETAIL_ID AS "upsellDetailId",
            ud.UPSELL_DETAIL_NAME AS "upsellDetailName",
            ud.UPSELL_DETAIL_SEQUENCE AS "upsellDetailSequence"
      FROM UPSELL_MASTER um, UPSELL_DETAIL ud
        WHERE um.UPSELL_MASTER_ID = UPPER(inMasterId)
          AND um.UPSELL_MASTER_ID = ud.UPSELL_MASTER_ID

      ORDER BY ud.UPSELL_DETAIL_SEQUENCE;

  RETURN cur_cursor;
END;
.
/
