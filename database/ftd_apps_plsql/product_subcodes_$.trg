-- FTD_APPS.PRODUCT_SUBCODES_$ trigger to populate FTD_APPS.PRODUCT_SUBCODES$ shadow table
CREATE OR REPLACE TRIGGER ftd_apps.product_subcodes_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.product_subcodes 
REFERENCING OLD AS old NEW AS new FOR EACH ROW
DECLARE

v_change_id ftd_apps.product_change$_acct.change_id%type;
v_operation ftd_apps.vendor_product$.operation$%TYPE;
v_now ftd_apps.vendor_product$.timestamp$%TYPE := SYSDATE;
v_change_unit_id ftd_apps.product_acctg_change_unit.acctg_change_unit_id%TYPE;
v_change_code ftd_apps.product_acctg_change_class.acctg_change_class_code%TYPE;

BEGIN

   -- Check if an accounting-pertinent metric has changed.
   IF (INSERTING OR
       DELETING OR
     (:OLD.PRODUCT_SUBCODE_ID != :NEW.PRODUCT_SUBCODE_ID) OR
     (:OLD.PRODUCT_ID != :NEW.PRODUCT_ID) OR
     (:OLD.DIM_WEIGHT != :NEW.DIM_WEIGHT) OR
     (:OLD.SUBCODE_PRICE != :NEW.SUBCODE_PRICE) OR
     (:OLD.VENDOR_PRICE != :NEW.VENDOR_PRICE)) THEN
      -- create change unit record
      select acctg_change_unit_id_sq.NEXTVAL into v_change_unit_id from dual;
     
      -- There's no availability change or delivery method change on the subcodes
      v_change_code := 'O';
	  
      INSERT INTO ftd_apps.product_acctg_change_unit
      (
      	acctg_change_unit_id,
      	acctg_change_class_code,
      	created_on
      )
      VALUES
      (
      	v_change_unit_id,
      	v_change_code,
      	sysdate
      );

     
   END IF;

   IF INSERTING THEN
      v_operation := 'INS';
      INSERT INTO ftd_apps.product_subcodes$ (
        PRODUCT_SUBCODE_ID, 
	PRODUCT_ID, 
	SUBCODE_DESCRIPTION, 
	SUBCODE_PRICE, 
	SUBCODE_REFERENCE_NUMBER, 
	HOLIDAY_SKU, 
	HOLIDAY_PRICE, 
	ACTIVE_FLAG, 
	VENDOR_PRICE, 
	VENDOR_SKU, 
	DIM_WEIGHT, 
	DISPLAY_ORDER,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id
      ) VALUES (
        :NEW.PRODUCT_SUBCODE_ID, 
	:NEW.PRODUCT_ID, 
	:NEW.SUBCODE_DESCRIPTION, 
	:NEW.SUBCODE_PRICE, 
	:NEW.SUBCODE_REFERENCE_NUMBER, 
	:NEW.HOLIDAY_SKU, 
	:NEW.HOLIDAY_PRICE, 
	:NEW.ACTIVE_FLAG, 
	:NEW.VENDOR_PRICE, 
	:NEW.VENDOR_SKU, 
	:NEW.DIM_WEIGHT, 
	:NEW.DISPLAY_ORDER,
        :NEW.CREATED_ON, 
	:NEW.CREATED_BY, 
	:NEW.UPDATED_ON, 
	:NEW.UPDATED_BY,
        v_operation,
        v_now,
        v_change_unit_id);
   ELSIF UPDATING  THEN
      v_operation := 'UPD';
      INSERT INTO ftd_apps.product_subcodes$ (
        PRODUCT_SUBCODE_ID, 
	PRODUCT_ID, 
	SUBCODE_DESCRIPTION, 
	SUBCODE_PRICE, 
	SUBCODE_REFERENCE_NUMBER, 
	HOLIDAY_SKU, 
	HOLIDAY_PRICE, 
	ACTIVE_FLAG, 
	VENDOR_PRICE, 
	VENDOR_SKU, 
	DIM_WEIGHT, 
	DISPLAY_ORDER,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id
      ) VALUES (
        :OLD.PRODUCT_SUBCODE_ID, 
	:OLD.PRODUCT_ID, 
	:OLD.SUBCODE_DESCRIPTION, 
	:OLD.SUBCODE_PRICE, 
	:OLD.SUBCODE_REFERENCE_NUMBER, 
	:OLD.HOLIDAY_SKU, 
	:OLD.HOLIDAY_PRICE, 
	:OLD.ACTIVE_FLAG, 
	:OLD.VENDOR_PRICE, 
	:OLD.VENDOR_SKU, 
	:OLD.DIM_WEIGHT, 
	:OLD.DISPLAY_ORDER,
        :OLD.CREATED_ON, 
	:OLD.CREATED_BY, 
	:OLD.UPDATED_ON, 
	:OLD.UPDATED_BY,
        'UPD_OLD',
        v_now,
        v_change_unit_id);
      INSERT INTO ftd_apps.product_subcodes$ (
        PRODUCT_SUBCODE_ID, 
	PRODUCT_ID, 
	SUBCODE_DESCRIPTION, 
	SUBCODE_PRICE, 
	SUBCODE_REFERENCE_NUMBER, 
	HOLIDAY_SKU, 
	HOLIDAY_PRICE, 
	ACTIVE_FLAG, 
	VENDOR_PRICE, 
	VENDOR_SKU, 
	DIM_WEIGHT, 
	DISPLAY_ORDER,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id
      ) VALUES (
        :NEW.PRODUCT_SUBCODE_ID, 
	:NEW.PRODUCT_ID, 
	:NEW.SUBCODE_DESCRIPTION, 
	:NEW.SUBCODE_PRICE, 
	:NEW.SUBCODE_REFERENCE_NUMBER, 
	:NEW.HOLIDAY_SKU, 
	:NEW.HOLIDAY_PRICE, 
	:NEW.ACTIVE_FLAG, 
	:NEW.VENDOR_PRICE, 
	:NEW.VENDOR_SKU, 
	:NEW.DIM_WEIGHT, 
	:NEW.DISPLAY_ORDER,
        :NEW.CREATED_ON, 
	:NEW.CREATED_BY, 
	:NEW.UPDATED_ON, 
	:NEW.UPDATED_BY,
        'UPD_NEW',
        v_now,
        v_change_unit_id);
   ELSIF DELETING  THEN
      v_operation := 'DEL';
      INSERT INTO ftd_apps.product_subcodes$ (
        PRODUCT_SUBCODE_ID, 
	PRODUCT_ID, 
	SUBCODE_DESCRIPTION, 
	SUBCODE_PRICE, 
	SUBCODE_REFERENCE_NUMBER, 
	HOLIDAY_SKU, 
	HOLIDAY_PRICE, 
	ACTIVE_FLAG, 
	VENDOR_PRICE, 
	VENDOR_SKU, 
	DIM_WEIGHT, 
	DISPLAY_ORDER,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id
      ) VALUES (
        :OLD.PRODUCT_SUBCODE_ID, 
	:OLD.PRODUCT_ID, 
	:OLD.SUBCODE_DESCRIPTION, 
	:OLD.SUBCODE_PRICE, 
	:OLD.SUBCODE_REFERENCE_NUMBER, 
	:OLD.HOLIDAY_SKU, 
	:OLD.HOLIDAY_PRICE, 
	:OLD.ACTIVE_FLAG, 
	:OLD.VENDOR_PRICE, 
	:OLD.VENDOR_SKU, 
	:OLD.DIM_WEIGHT, 
	:OLD.DISPLAY_ORDER,
        :OLD.CREATED_ON, 
	:OLD.CREATED_BY, 
	:OLD.UPDATED_ON, 
	:OLD.UPDATED_BY,
        'DEL',
        v_now,
        v_change_unit_id);
   END IF;

   -- Verify that an accounting-pertinent metric has changed.
   IF ((v_operation = 'DEL') OR
     (v_operation = 'INS') OR
     (:OLD.PRODUCT_SUBCODE_ID != :NEW.PRODUCT_SUBCODE_ID) OR
     (:OLD.PRODUCT_ID != :NEW.PRODUCT_ID) OR
     (:OLD.DIM_WEIGHT != :NEW.DIM_WEIGHT) OR
     (:OLD.SUBCODE_PRICE != :NEW.SUBCODE_PRICE) OR
     (:OLD.VENDOR_PRICE != :NEW.VENDOR_PRICE)) THEN

   -- Populate the accounting shadow table.
   -- Obtain a sequence-based surrogate key.
   select product_change$_acct_sq.NEXTVAL into v_change_id from dual;

   IF ((v_operation = 'DEL') OR
       (v_operation = 'UPD')) THEN

   IF (v_operation = 'UPD') THEN
      v_operation := 'UPD_OLD';
   END IF;

   insert into product_master$_acct
(CHANGE_ID,
PRODUCT_ID,
NOVATOR_ID,
STATUS,
PRODUCT_TYPE,
STANDARD_PRICE,
SUBCODE_PRICE,
DELUXE_PRICE,
PREMIUM_PRICE,
VENDOR_COST,
SUBCODE_VENDOR_COST,
DIM_WEIGHT,
SUBCODE_DIM_WEIGHT,
SHIP_METHOD_CARRIER,
SHIP_METHOD_FLORIST,
NO_TAX_FLAG)
select
v_change_id,
:OLD.PRODUCT_SUBCODE_ID,
pm.novator_id,
pm.status,
pm.product_type,
pm.standard_price,
:OLD.SUBCODE_PRICE,  
pm.deluxe_price, 
pm.premium_price,
pm.vendor_cost,
:OLD.VENDOR_PRICE, 
pm.dim_weight,
:OLD.DIM_WEIGHT,
pm.ship_method_carrier,
pm.ship_method_florist,
pm.no_tax_flag
from product_master pm
where pm.product_id = :OLD.PRODUCT_ID;

   -- Populate the remaining accounting audit tables.
   FTD_APPS.INSERT_PRODUCT_CHANGE_ACCT (
      'PRODUCT_SUBCODES',
      v_change_id,
      :OLD.PRODUCT_SUBCODE_ID,
      :OLD.UPDATED_BY,
      v_operation,
      v_now,
      null);
   

   IF (v_operation = 'UPD_OLD') THEN
   -- Increment the sequence for the UPD new record.
   select product_change$_acct_sq.NEXTVAL into v_change_id from dual;

   v_operation := 'UPD_NEW';
   END IF;

   END IF;

   IF (v_operation != 'DEL') THEN

   insert into product_master$_acct
(CHANGE_ID,
PRODUCT_ID,
NOVATOR_ID,
STATUS,
PRODUCT_TYPE,
STANDARD_PRICE,
SUBCODE_PRICE,
DELUXE_PRICE,
PREMIUM_PRICE,
VENDOR_COST,
SUBCODE_VENDOR_COST,
DIM_WEIGHT,
SUBCODE_DIM_WEIGHT,
SHIP_METHOD_CARRIER,
SHIP_METHOD_FLORIST,
NO_TAX_FLAG)
select
v_change_id,
:NEW.PRODUCT_SUBCODE_ID,
pm.novator_id,
pm.status,
pm.product_type,
pm.standard_price,
:NEW.SUBCODE_PRICE,  
pm.deluxe_price, 
pm.premium_price,
pm.vendor_cost,
:NEW.VENDOR_PRICE, 
pm.dim_weight,
:NEW.DIM_WEIGHT,
pm.ship_method_carrier,
pm.ship_method_florist,
pm.no_tax_flag
from product_master pm
where pm.product_id = :NEW.PRODUCT_ID;

   -- Populate the remaining accounting audit tables.
   FTD_APPS.INSERT_PRODUCT_CHANGE_ACCT (
      'PRODUCT_SUBCODES',
      v_change_id,
      :NEW.PRODUCT_SUBCODE_ID,
      :NEW.UPDATED_BY,
      v_operation,
      v_now,
      null);
   END IF;
   END IF;
END;
/
