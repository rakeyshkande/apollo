CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_ADDON_BY_TYPE_EX_OCC (typeIn varchar2, occasionIn IN varchar2)
RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_ADDON_BY_TYPE_EX_OCC
-- Type:    Function
-- Syntax:  SP_GET_ADDON_BY_TYPE_EX_OCC (typeIn VARCHAR2, occasionIn varchar2 )
-- Returns: ref_cursor for
--          ADDON_ID       NUMBER
--          DESCRIPTION    VARCHAR2(50)
--
-- Description:   Queries the ADDON table to return all rows for the input ADDON_TYPE
--                excluding those that have a row on the OCCASSION_ADDON for the input
--                OCCASION_ID
--
--========================================================================================
AS
    addon_cursor types.ref_cursor;
BEGIN
    OPEN addon_cursor FOR
         SELECT A.ADDON_ID as "addonId",
                A.DESCRIPTION as "description"
          FROM ADDON A
          WHERE A.ADDON_TYPE = typeIn
            AND A.ADDON_ID NOT IN
        ( SELECT OA.ADDON_ID
          FROM OCCASION_ADDON OA
          WHERE A.ADDON_ID = OA.ADDON_ID
            AND OA.OCCASION_ID = occasionIn );

    RETURN addon_cursor;
END
;
.
/
