CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_PRODUCT_HP (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_PRODUCT_HP
-- Type:    Procedure
-- Syntax:  SP_REMOVE_PRODUCT_HP ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_HP by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_HP
  where PRODUCT_ID = productId;

end
;
.
/
