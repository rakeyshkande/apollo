CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_ROLE_FUNCTIONS (
    roleID IN VARCHAR2
 )
as
--==============================================================================
--
-- Name:    SP_DELETE_ROLE_FUNCTIONS
-- Type:    Procedure
-- Syntax:  SP_DELETE_ROLE_FUNCTIONS ( roleID in varchar2 )
--
-- Description:   Deletes all functions associated with the provided role ID
--
--==============================================================================
begin

  DELETE FROM ROLE_FUNCTIONS
  WHERE ROLE_ID = roleID;

  COMMIT;

end
;
.
/
