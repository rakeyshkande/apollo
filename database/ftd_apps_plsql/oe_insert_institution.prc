CREATE OR REPLACE
PROCEDURE ftd_apps.OE_INSERT_INSTITUTION  ( businessIn in varchar2,
	typeIn       in varchar2,
	nameIn       in varchar2,
	addressIn    in varchar2,
	cityIn       in VARCHAR2,
	stateIn      in VARCHAR2,
	zipcodeIn    in VARCHAR2,
	phoneIn      in varchar2)
as
--==============================================================================
--
-- Name:    SP_INSERT_INSTITUTION
-- Type:    Procedure
-- Syntax:  SP_INSERT_INSTITUTION ( businessIn in varchar2,
--                                 	typeIn     in varchar2,
--                                	nameIn     in varchar2,
--                                	addressIn  in varchar2,
--                                	cityIn     in VARCHAR2,
--                                	stateIn    in VARCHAR2,
--                                	zipcodeIn  in VARCHAR2,
--                                	phoneIn    in varchar2)
--
-- Description:   Inserts a row to the BUSINESS table.
--
--==============================================================================
begin

 insert into BUSINESS (
     BUSINESS_ID,
     BUSINESS_TYPE,
     BUSINESS_NAME,
     ADDRESS,
     CITY,
     STATE,
     ZIP_CODE,
     PHONE_NUMBER)
 VALUES(
     businessIn,
     typeIn,
     nameIn,
     addressIn,
     cityIn,
     stateIn,
	   zipcodeIn,
	   phoneIn);

	commit;

end
;
.
/
