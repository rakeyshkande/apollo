-- FTD_APPS.SOURCE_TYPE_VAL_$ trigger to populate FTD_APPS.SOURCE_TYPE_VAL$ shadow table

CREATE OR REPLACE TRIGGER FTD_APPS.source_type_val_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.source_type_val REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.source_type_val$ (
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on,      	
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.source_type,
      	:NEW.status,
      	:NEW.created_on,
      	:NEW.created_by,
      	:NEW.updated_by,
      	:NEW.updated_on, 
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.source_type_val$ (
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on, 
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.source_type,
      	:OLD.status,
      	:OLD.created_on,
      	:OLD.created_by,
      	:OLD.updated_by,
      	:OLD.updated_on, 
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO ftd_apps.source_type_val$ (
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on, 
			operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.source_type,
      	:NEW.status,
      	:NEW.created_on,
      	:NEW.created_by,
      	:NEW.updated_by,
      	:NEW.updated_on, 
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.source_type_val$ (
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on, 
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.source_type,
      	:OLD.status,
      	:OLD.created_on,
      	:OLD.created_by,
      	:OLD.updated_by,
      	:OLD.updated_on, 
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
