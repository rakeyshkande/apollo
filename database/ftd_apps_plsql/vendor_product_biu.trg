CREATE OR REPLACE TRIGGER ftd_apps.vendor_product_biu
   BEFORE INSERT OR UPDATE ON ftd_apps.vendor_product 
   REFERENCING OLD AS old NEW AS new FOR EACH ROW
DECLARE

BEGIN

   -- Update vendor_cost_eff_date if vendor_cost has changed

   IF ( INSERTING ) OR
      (:OLD.VENDOR_COST != :NEW.VENDOR_COST) THEN  
         :NEW.VENDOR_COST_EFF_DATE := SYSDATE;
   END IF;

END VENDOR_PRODUCT_BIU;
/
