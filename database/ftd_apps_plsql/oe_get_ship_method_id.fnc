CREATE OR REPLACE
function ftd_apps.OE_GET_SHIP_METHOD_ID
 (shipMethodDescription IN VARCHAR2)
    RETURN types.ref_cursor
--====================================================================
--
-- Name:    OE_GET_SHIP_METHOD_ID
-- Type:    Function
-- Syntax:  OE_GET_SHIP_METHOD_ID (shipMethodDescription IN VARCHAR2)
-- Returns: ref_cursor for
--          SHIP_METHOD_ID  VARCHAR2(2)
--
-- Description:   Returns ship method ID for the given description
--
--====================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT SHIP_METHOD_ID as "shipMethodId"
        FROM SHIP_METHODS
        WHERE description = shipMethodDescription;

    RETURN cur_cursor;
END
;
.
/
