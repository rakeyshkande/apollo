CREATE OR REPLACE
PACKAGE BODY ftd_apps.set_last_cutoff_pkg AS
  /* Update Last Cutoff package */

  pg_update_last_cutoff_string  varchar2(40) := 'set_last_cutoff_pkg.update_last_cutoff;';  -- name of job in the DBMS_JOB stream
  pg_interval       varchar2(40) := 'sysdate + 1/288';          -- interval (i.e 1/24 = 1 hour)

  pg_package_c  constant varchar2(40) := 'set_last_cutoff_pkg';
  pg_version_c  constant varchar2(40) := '1.0.0.0';

--==============================================================================
-- Name: currently_running
-- Description  Function looks for the update_last_cutoff in the DBMS_JOB queue.  Returns
--              a TRUE if it is found, or a FALSE if it is not.
--
--
-- Returns: BOOLEAN value indicating if job is already in the queue.
--
--==============================================================================
FUNCTION currently_running return boolean is

  cursor check_cur is
    select 'x'
      from user_jobs
     where what = pg_update_last_cutoff_string;

  v_check    varchar2(1);
  v_return   boolean;

BEGIN

  open check_cur;
  fetch check_cur into v_check;
  if (check_cur%found) then

    v_return := true;

  else

    v_return := false;

  end if;

  close check_cur;

  return (v_return);

END currently_running;
--==============================================================================
-- Name: update_last_cutoff
-- Description: Update the last cutoff time in csz_avail
--
--==============================================================================
PROCEDURE update_last_cutoff AS

BEGIN

   update ftd_apps.csz_avail
      set latest_cutoff = '1200'
      where latest_cutoff in ('1400','1500');

   commit;

END update_last_cutoff;
--==============================================================================
-- Name: stop_update_last_cutoff
-- Description:  Search for and remove the update_last_cutoff from the DBMS JOB queue.
--
-- Syntax: stop_update_last_cutoff;
--
--
--==============================================================================
PROCEDURE stop_update_last_cutoff is

  cursor job_cur is
    select job
      from user_jobs
     where what = pg_update_last_cutoff_string;

  v_job    number;

BEGIN

  open job_cur;
  fetch job_cur into v_job;
  if (job_cur%found) then

    dbms_job.remove(v_job);
    commit;

  end if;
  close job_cur;

END stop_update_last_cutoff;
--==============================================================================
-- Name: start_update_last_cutoff
-- Description: Place the update_last_cutoff job in the DBMS JOB queue.
--
-- Syntax: start_update_last_cutoff;
--
--
--==============================================================================
PROCEDURE start_update_last_cutoff is

  v_job   binary_integer;

BEGIN

 -- Make sure update_last_cutoff is not already running in the queue.
 if currently_running then

   raise_application_error(-20102, 'Update Last Cutoff is already running');

 else

   dbms_job.submit(v_job, pg_update_last_cutoff_string, sysdate, pg_interval);
   commit;

 end if;

END start_update_last_cutoff;
--==============================================================================
-- Name: what_version
--
-- Description: Returns version information for this package.
--
-- Syntax: what_version;
--
-- Returns: A varchar2 containing the package version.
--==============================================================================
FUNCTION what_version return varchar2 is

BEGIN

  return pg_version_c;

END what_version;
--==============================================================================
END set_last_cutoff_pkg;
.
/
