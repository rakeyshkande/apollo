CREATE OR REPLACE
FUNCTION ftd_apps.OE_CLEANUP_ALPHANUM_STRING (inString IN VARCHAR2,
 alphaFlag IN CHAR)
return VARCHAR2
as
    returnString        VARCHAR2(1024) := NULL;
    origString          VARCHAR2(1024);
    tmpString           VARCHAR2(1024);
    goodChars           NUMBER;
    keepString          VARCHAR2(62);
begin
    if ( alphaFlag = 'Y' )
    then
        keepString := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    else
        keepString := '0123456789';
    end if;

    origString := inString;
    tmpString := ltrim(origString, keepString);

    while ( tmpString IS NOT NULL )
    loop
        goodChars := length(origString) - length(tmpString);
        if ( goodChars = 0 )
        then
            origString := substr(origString, 2);
        else
            returnString := returnString || substr(origString, 1, goodChars);
            origString := tmpString;
        end if;

        tmpString := ltrim(origString, keepString);
    end loop;

    returnString := returnString || origString;
    return returnString;

end
;
.
/
