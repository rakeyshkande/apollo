CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_ROLE  ( roleId   		    IN VARCHAR2,
   lastUpdateDate   IN DATE
)
as
--==============================================================================
--
-- Name:    SP_DELETE_ROLE
-- Type:    Procedure
-- Syntax:  SP_DELETE_ROLE ( <see args above> )
--
-- Description:   Deletes row from Role by ROLE-ID if no users assigned to role
--                Cascade also deletes any rows from Role_Function table by Role_id.
--
--==============================================================================

    delete_ok   BOOLEAN := true;

    -- Cursor handles row locking for deleting on ROLE table
    CURSOR role_lock_cur (p_role_id  IN role.role_id%TYPE) IS
        SELECT * FROM role
        WHERE ROLE_ID = p_role_id
        FOR UPDATE;
    role_delete_row    role_lock_cur%ROWTYPE;

begin

    -- Lock the existing row on ROLE for delete
    OPEN role_lock_cur(roleId);
    FETCH role_lock_cur INTO role_delete_row;

    -- Error if role does not exist
    If role_lock_cur%notfound then
       CLOSE role_lock_cur;
       COMMIT;
        RAISE_APPLICATION_ERROR(-20001, 'Role delete for ID ' || roleId || ' Role already deleted. - TABLE: ');
    END IF;

    -- Error if the last update date on existing row differs from the input parameter
    IF ( role_delete_row.last_updated_date <> lastUpdateDate ) then
       CLOSE role_lock_cur;
       COMMIT;
        RAISE_APPLICATION_ERROR(-20001, 'Role delete for ID ' || roleId || ' failed due to last_update date mismatch. - TABLE: ' || to_char(role_delete_row.last_updated_date,'MMDDYYYYHH24MISS') || ' PARM: ' || to_char(lastUpdateDate,'MMDDYYYYHH24MISS') || '*');
    END IF;

  DECLARE
  temp number;

  begin

   -- Error if users exist for input parameter
    SELECT 1 into temp
      FROM DUAL
      WHERE EXISTS
       (Select 1
         from USER_PROFILE
        where ROLE_ID = roleId);

    delete_ok := false;

   exception when
     no_data_found then
      null;
   end;

     If not delete_ok then
      CLOSE role_lock_cur;
      COMMIT;
        RAISE_APPLICATION_ERROR(-20001, 'Role delete for ID ' || roleId || ' failed due to user(s) assigned to role. - TABLE: ' ||  '*');
     end if;

-- process delete row

    DELETE FROM ROLE
    WHERE CURRENT OF role_lock_cur;

    -- Release the row lock on ROLE with a commit
    CLOSE role_lock_cur;
    COMMIT;

end
;
.
/
