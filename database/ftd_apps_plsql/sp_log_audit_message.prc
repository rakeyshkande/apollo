CREATE OR REPLACE
PROCEDURE ftd_apps.SP_LOG_AUDIT_MESSAGE (
  userId in varchar2,
	messageId in number,
	actionPerformed in varchar2
)
as
--==============================================================================
--
-- Name:    SP_LOG_AUDIT_MESSAGE
-- Type:    Procedure
-- Syntax:  SP_LOG_AUDIT_MESSAGE ( userId in varchar2,
--                                messageId in number,
--                                actionPerformed in varchar2 )
--
-- Description:   Inserts a row to AUDIT_TABLE.
--
--==============================================================================
begin

 insert into AUDIT_TABLE
 (AUDIT_TABLE_ID, TASK_ID, AUDIT_MESSAGE_ID, USER_ID, DATE_TIME, ACTION_PERFORMED)
 VALUES(SEQ_1.Nextval, 1, messageId, userId, SYSDATE, actionPerformed);

end
;
.
/
