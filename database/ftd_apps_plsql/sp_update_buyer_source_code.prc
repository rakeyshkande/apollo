CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_BUYER_SOURCE_CODE (
  asnNumberIn in varchar2,
	clientNameIn in varchar2,
    sourceCodeIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_BUYER_SOURCE_CODE
-- Type:    Procedure
-- Syntax:  SP_UPDATE_BUYER_SOURCE_CODE ( asnNumberIn in varchar2,
--                                    clientNameIn in varchar2,
--                                      sourceCodeIn in varchar2)
--
-- Description:   Updates a row to the BUYER_SOURCE_CODE table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 update BUYER_SOURCE_CODE
 Set CLIENT_NAME = clientNameIn,
     SOURCE_CODE = sourceCodeIn
 WHERE ASN_NUMBER = asnNumberIn;

end
;
.
/
