CREATE OR REPLACE
PACKAGE BODY ftd_apps.FLORIST_LOAD_AUDIT_PKG  AS

TYPE varchar_tab_typ IS TABLE OF VARCHAR2(100) INDEX BY PLS_INTEGER;
pg_rowcount     number;


PROCEDURE RESET_FLORIST_DATA_STAGE
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting all records from the
        florist_master_stage, florist_codification_stage, and
        efos_city_state_stage tables

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

--DELETE FROM florist_master_stage;
--DELETE FROM florist_codifications_stage;
--DELETE FROM efos_city_state_stage;

execute immediate 'TRUNCATE TABLE florist_master_stage';
execute immediate 'TRUNCATE TABLE florist_codifications_stage';
execute immediate 'TRUNCATE TABLE efos_city_state_stage';
execute immediate 'TRUNCATE TABLE codification_master_stage';
execute immediate 'TRUNCATE TABLE florist_hours_stage';

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END RESET_FLORIST_DATA_STAGE;


PROCEDURE RESET_FLORIST_WEIGHTS_STAGE
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting all records from the
        FLORIST_WEIGHTS_STAGE table

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM FLORIST_WEIGHTS_STAGE;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END RESET_FLORIST_WEIGHTS_STAGE;



PROCEDURE VIEW_CODIFIED_MASTER_FROM_FILE
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving codification information
        for the florist data load.

Input:
        none

Output:
        cursor containing all records from codification_master

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                codification_id,
                file_position,
                file_length
        FROM    codification_master
        WHERE   load_from_file = 'Y';

END VIEW_CODIFIED_MASTER_FROM_FILE;


PROCEDURE VIEW_ALL_FLORIST_IDS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all of the florist ids
        from florist_master

Input:
        none

Output:
        cursor containing florist_id

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.florist_id
        FROM    florist_master m
        WHERE   m.status <> 'Inactive';

END VIEW_ALL_FLORIST_IDS;


PROCEDURE VIEW_FLORIST_AUDIT_RPT_NAMES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the names of the florist
        audit reports that can be viewed by the user

Input:
        none

Output:
        cursor containing audit report names

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                to_char (creation_date, 'mm/dd/yyyy') || ' ' ||
                name || ' - ' ||
                status ||
                decode (status, 'New', null, ' - ' || to_char (updated_date, 'mm/dd/yyyy')) as audit_report,
                report_id
        FROM    audit_home.audit_report_header
        WHERE   type = 'Florist Audit'
        AND     to_char (creation_date, 'yyyymm') >= to_char (add_months (sysdate, -3), 'yyyymm')
        ORDER BY decode (status, 'New', 'A', 'B'), creation_date DESC;

END VIEW_FLORIST_AUDIT_RPT_NAMES;


PROCEDURE INSERT_EFOS_CITY_STATE_STG
(
IN_CITY_STATE_CODE              IN EFOS_CITY_STATE_STAGE.CITY_STATE_CODE%TYPE,
IN_CITY_NAME                    IN EFOS_CITY_STATE_STAGE.CITY_NAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting city state codes as found
        in the city state efos file.

Input:
        city_state_code                 varchar2
        city_name                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO efos_city_state_stage
(
        city_state_code,
        city_name
)
VALUES
(
        in_city_state_code,
        in_city_name
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_EFOS_CITY_STATE_STG;


PROCEDURE INSERT_FLORIST_MASTER_STG
(
IN_FLORIST_ID                   IN FLORIST_MASTER_STAGE.FLORIST_ID%TYPE,
IN_FLORIST_NAME                 IN FLORIST_MASTER_STAGE.FLORIST_NAME%TYPE,
IN_ADDRESS                      IN FLORIST_MASTER_STAGE.ADDRESS%TYPE,
IN_CITY                         IN FLORIST_MASTER_STAGE.CITY%TYPE,
IN_STATE                        IN FLORIST_MASTER_STAGE.STATE%TYPE,
IN_PHONE_NUMBER                 IN FLORIST_MASTER_STAGE.PHONE_NUMBER%TYPE,
IN_ZIP_CODE                     IN FLORIST_MASTER_STAGE.ZIP_CODE%TYPE,
IN_CITY_STATE_NUMBER            IN FLORIST_MASTER_STAGE.CITY_STATE_NUMBER%TYPE,
IN_RECORD_TYPE                  IN FLORIST_MASTER_STAGE.RECORD_TYPE%TYPE,
IN_MERCURY_FLAG                 IN FLORIST_MASTER_STAGE.MERCURY_FLAG%TYPE,
IN_OWNERS_NAME                  IN FLORIST_MASTER_STAGE.OWNERS_NAME%TYPE,
IN_MINIMUM_ORDER_AMOUNT         IN FLORIST_MASTER_STAGE.MINIMUM_ORDER_AMOUNT%TYPE,
IN_FLORIST_WEIGHT               IN FLORIST_MASTER_STAGE.FLORIST_WEIGHT%TYPE,
IN_INITIAL_WEIGHT               IN FLORIST_MASTER_STAGE.INITIAL_WEIGHT%TYPE,
IN_TOP_LEVEL_FLORIST_ID         IN FLORIST_MASTER_STAGE.TOP_LEVEL_FLORIST_ID%TYPE,
IN_PARENT_FLORIST_ID            IN FLORIST_MASTER_STAGE.PARENT_FLORIST_ID%TYPE,
IN_FAX_NUMBER                   IN FLORIST_MASTER_STAGE.FAX_NUMBER%TYPE,
IN_ALT_PHONE_NUMBER             IN FLORIST_MASTER_STAGE.ALT_PHONE_NUMBER%TYPE,
IN_INTERNAL_LINK_NUMBER         IN FLORIST_MASTER_STAGE.INTERNAL_LINK_NUMBER%TYPE,
IN_TERRITORY                    IN FLORIST_MASTER_STAGE.TERRITORY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting florist data found in the
        member data file.

Input:
        florist_id                      varchar2
        florist_name                    varchar2
        address                         varchar2
        city                            varchar2
        state                           varchar2
        phone_number                    varchar2
        zip_code                        varchar2
        city_state_number               varchar2
        record_type                     varchar2
        mercury_flag                    varchar2
        owners_name                     varchar2
        minimum_order_amount            number
        florist_weight                  number
        initial_weight                  number
        top_level_florist_id            varchar2
        parent_florist_id               varchar2
        fax_number                      varchar2
        alt_phone_number                varchar2
        internal_link_number            varchar2
        territory                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR check_state (p_state varchar2) IS
        SELECT  count (1)
        FROM    state_master
        WHERE   state_master_id = p_state;

v_check         number := 0;
v_state         florist_master.state%type;

BEGIN

-- change state code of 'NF' to 'NL'
IF in_state = 'NF' THEN
        v_state := 'NL';
ELSE
        v_state := in_state;
END IF;

-- check if the state code exists in the master table
-- if not set the state to 'NA'
OPEN check_state (v_state);
FETCH check_state INTO v_check;
CLOSE check_state;

IF v_check = 0 THEN
        v_state := 'NA';
END IF;

-- CR 1/2005 - set the city state code to null for send only florists (suffix begins with Z)
INSERT INTO florist_master_stage
(
        florist_id,
        florist_name,
        address,
        city,
        state,
        phone_number,
        zip_code,
        city_state_number,
        record_type,
        mercury_flag,
        owners_name,
        minimum_order_amount,
        florist_weight,
        initial_weight,
        top_level_florist_id,
        parent_florist_id,
        fax_number,
        alt_phone_number,
        internal_link_number,
        territory
)
VALUES
(
        in_florist_id,
        in_florist_name,
        in_address,
        in_city,
        v_state,
        in_phone_number,
        in_zip_code,
        decode (substr (in_florist_id, 8, 1), 'Z', null, in_city_state_number),
        in_record_type,
        in_mercury_flag,
        in_owners_name,
        in_minimum_order_amount,
        in_florist_weight,
        in_initial_weight,
        in_top_level_florist_id,
        in_parent_florist_id,
        in_fax_number,
        in_alt_phone_number,
        in_internal_link_number,
        in_territory
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_FLORIST_MASTER_STG;


PROCEDURE RESET_FLORIST_LINKING
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing all associations between
        R type florists that are currently linked.  This should happen
        before the new link (main/branch) data is processed to remove any
        links that do not exist in the file.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

-- this cursor gets all of the linked florists top level florist
CURSOR top_level IS
        select  top_level_florist_id
        from    florist_master
        where   status <> 'Inactive'
        group by top_level_florist_id having count (1) > 1;

v_to_level_florist_id   florist_master.top_level_florist_id%type;

BEGIN

OPEN top_level;
FETCH top_level INTO v_to_level_florist_id;

WHILE top_level%found LOOP

        -- unlink the level directly under the top florist
        UPDATE  florist_master
        SET     top_level_florist_id = florist_id,
                parent_florist_id = null
        WHERE   top_level_florist_id = v_to_level_florist_id
        AND     top_level_florist_id = parent_florist_id;

        -- unlink the bottem level from the top florist
        UPDATE  florist_master
        SET     top_level_florist_id = parent_florist_id
        WHERE   top_level_florist_id = v_to_level_florist_id
        AND     top_level_florist_id <> parent_florist_id;

        FETCH top_level INTO v_to_level_florist_id;
END LOOP;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END RESET_FLORIST_LINKING;


PROCEDURE UPDATE_FLORIST_MASTER
(
IN_FLORIST_ID                   IN FLORIST_MASTER_STAGE.FLORIST_ID%TYPE,
IN_TOP_LEVEL_FLORIST_ID         IN FLORIST_MASTER_STAGE.TOP_LEVEL_FLORIST_ID%TYPE,
IN_PARENT_FLORIST_ID            IN FLORIST_MASTER_STAGE.PARENT_FLORIST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating florist data with the
        linking data

Input:
        florist_id                      varchar2
        top_level_florist_id            varchar2
        parent_florist_id               varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  florist_master
SET     top_level_florist_id = in_top_level_florist_id,
        parent_florist_id = in_parent_florist_id
WHERE   florist_id = in_florist_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLORIST_MASTER;


PROCEDURE UPDATE_LINKED_ZIPS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing duplicate zip code
        coverage within associated florists.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

type florist_tab_typ is table of florist_master.florist_id%type index by pls_integer;

CURSOR dup_zips IS
        select  f.top_level_florist_id,
                z.zip_code
        from    florist_master f
        join    florist_zips z
        on      f.florist_id = z.florist_id
        where   f.status <> 'Inactive'
        and     f.record_type = 'R'
        group by f.top_level_florist_id, z.zip_code
        having count (*) > 1;

-- this cursor will get the top most parent who covers a duplicate zip.
-- keep the coverage at the top most parent, and delete the others.
-- if there are no record returned from this cursor then the duplicate
-- zip coverage exists in florists at the same level.  in that case
-- keep the first florist found using the zip_cur2.
CURSOR zip_cur1 (p_top_level_florist_id varchar2, p_zip_code varchar2) IS
        select  z.rowid
        from    florist_master f
        join    florist_zips z
        on      f.florist_id = z.florist_id
        and     z.zip_code = p_zip_code
        where   top_level_florist_id = p_top_level_florist_id
        and     status <> 'Inactive'
        and     record_type = 'R'
        and     exists
        (
                select  1
                from    florist_master f1
                join    florist_zips z1
                on      f1.florist_id = z1.florist_id
                where   f1.parent_florist_id = f.florist_id
                and     z1.zip_code = z.zip_code
                and     f1.status <> 'Inactive'
                and     f1.record_type = 'R'
        );

-- cursor to find the first florist with duplicate zip coverage
CURSOR zip_cur2 (p_top_level_florist_id varchar2, p_zip_code varchar2) IS
        select  z.rowid
        from    florist_master f
        join    florist_zips z
        on      f.florist_id = z.florist_id
        and     z.zip_code = p_zip_code
        where   f.top_level_florist_id = p_top_level_florist_id
        and     f.status <> 'Inactive'
        and     f.record_type = 'R'
        and     rownum = 1;


v_top_level_florist_id          florist_master.top_level_florist_id%type;
v_zip_code                      florist_zips.zip_code%type;
v_keep_rowid                    rowid;

BEGIN

OPEN dup_zips;
FETCH dup_zips INTO v_top_level_florist_id, v_zip_code;

WHILE dup_zips%found LOOP
        v_keep_rowid := null;
        OPEN zip_cur1 (v_top_level_florist_id, v_zip_code);
        FETCH zip_cur1 INTO v_keep_rowid;
        CLOSE zip_cur1;

        IF v_keep_rowid IS NULL THEN
                OPEN zip_cur2 (v_top_level_florist_id, v_zip_code);
                FETCH zip_cur2 INTO v_keep_rowid;
                CLOSE zip_cur2;
        END IF;

        IF v_keep_rowid IS NOT NULL THEN
                DELETE FROM florist_zips z
                WHERE   z.zip_code = v_zip_code
                AND     EXISTS
                (
                        select  1
                        from    florist_master f
                        where   f.florist_id = z.florist_id
                        and     f.top_level_florist_id = v_top_level_florist_id
                        and     f.status <> 'Inactive'
                        and     f.record_type = 'R'
                )
                AND     z.rowid <> v_keep_rowid;
        END IF;

        FETCH dup_zips INTO v_top_level_florist_id, v_zip_code;
END LOOP;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_LINKED_ZIPS;


PROCEDURE INSERT_FLORIST_CODIFIED_STG
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS_STAGE.FLORIST_ID%TYPE,
IN_CODIFICATION_ID              IN FLORIST_CODIFICATIONS_STAGE.CODIFICATION_ID%TYPE,
IN_HOLIDAY_PRICE_FLAG           IN FLORIST_CODIFICATIONS_STAGE.HOLIDAY_PRICE_FLAG%TYPE,
IN_MIN_PRICE                    IN FLORIST_CODIFICATIONS_STAGE.MIN_PRICE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting florist codifications
        as found in the member file.

Input:
        florist_id                      varchar2
        codification_id                 varchar2
        holiday_price_flag              varchar2
        min_price                       number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO florist_codifications_stage
(
        florist_id,
        codification_id,
        holiday_price_flag,
        min_price
)
VALUES
(
        in_florist_id,
        in_codification_id,
        in_holiday_price_flag,
        in_min_price
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_FLORIST_CODIFIED_STG;


PROCEDURE LOAD_EFOS_CITY_STATE
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the data from the
        efos_city_state_stage table to the efos_city_state table.  New
        city_state_codes will be inserted, existing codes will be updated.
        City state codes not found in the file will be kept in the table
        for history.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_city_state_tab        varchar_tab_typ;
v_city_name_tab         varchar_tab_typ;

-- cursor for getting new city state codes
CURSOR insert_cur IS
        SELECT
                n.city_state_code,
                n.city_name
        FROM    efos_city_state_stage n
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    efos_city_state o
                WHERE   n.city_state_code = o.city_state_code
        )
        AND     n.city_name is not null;

BEGIN

-- update existing city state records
UPDATE  efos_city_state o
SET     city_name = (   select  city_name
                        from    efos_city_state_stage n
                        where   o.city_state_code = n.city_state_code
                        and     o.city_name <> n.city_name
                    )
WHERE   EXISTS
(
        SELECT  1
        FROM    efos_city_state_stage n
        WHERE   o.city_state_code = n.city_state_code
        AND     o.city_name <> n.city_name
) RETURNING city_state_code BULK COLLECT INTO v_city_state_tab;

pg_rowcount := sql%rowcount;
dbms_output.put_line ('update city state codes :: ' || to_char (pg_rowcount));

out_status := 'Y';

-- insert the new city state codes
IF out_status = 'Y' THEN
        OPEN insert_cur;
        FETCH insert_cur BULK COLLECT INTO v_city_state_tab, v_city_name_tab;
        CLOSE insert_cur;

        -- insert new city state records
        FORALL x IN 1..v_city_state_tab.count
                INSERT INTO efos_city_state
                (
                        city_state_code,
                        city_name
                )
                VALUES
                (
                        v_city_state_tab (x),
                        v_city_name_tab (x)
                );

        pg_rowcount := sql%rowcount;
        dbms_output.put_line ('insert new city state codes :: ' || to_char (pg_rowcount));
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_EFOS_CITY_STATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOAD_EFOS_CITY_STATE;


PROCEDURE LOAD_FLORIST_COMMENTS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting comments for existing
        florists or inactive florists.

        Comments that are created:
        Profile disposition changes with florist_name, address, city, state,
        zip_code, phone_number, owners_name, alt phone, fax, inactived florists

        Codification disposition with updated minimum order amount,
        florist_hours open/close data, new or deleted codifications

Input:
        florist_id                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_comment_type                  FLORIST_COMMENTS.COMMENT_TYPE%TYPE;
v_null_value                    varchar2 (20) := 'check null value';

BEGIN

dbms_application_info.set_client_info('Insert update comments' || '::' || in_florist_id);
-- profile disposition changes with florist_name, address, city, state, zip_code,
-- phone_number, owners_name, alt phone, fax, added florists and inactived florists
v_comment_type := 'Profile';
INSERT INTO florist_comments
(
        florist_id,
        comment_date,
        comment_type,
        florist_comment,
        created_date,
        created_by
)
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Name was changed from ' || RTRIM (o.florist_name) || ' to ' || RTRIM (n.florist_name),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.florist_name, v_null_value) <> NVL(o.florist_name, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Address was changed from ' || RTRIM (o.address) || ' to ' || RTRIM (n.address),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.address, v_null_value) <> NVL(o.address, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'City was changed from ' || RTRIM (o.city) || ' to ' || RTRIM (n.city),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.city, v_null_value) <> NVL(o.city, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'State/Pro was changed from ' || RTRIM (o.state) || ' to ' || RTRIM (n.state),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.state, v_null_value) <> NVL(o.state, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Zip was changed from ' || RTRIM (o.zip_code) || ' to ' || RTRIM (decode (florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code)),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     OE_CLEANUP_ALPHANUM_STRING(NVL (o.zip_code, 'null value'), 'Y') <> OE_CLEANUP_ALPHANUM_STRING(NVL (decode (ftd_apps.florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code), 'null value'), 'Y')
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Phone was changed from ' || RTRIM (o.phone_number) || ' to ' || RTRIM (n.phone_number),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.phone_number, v_null_value) <> NVL(o.phone_number, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Contact was changed from ' || RTRIM (o.owners_name) || ' to ' || RTRIM (n.owners_name),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.owners_name, v_null_value) <> NVL(o.owners_name, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Alt Phone was changed from ' || RTRIM (o.alt_phone_number) || ' to ' || RTRIM (n.alt_phone_number),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.alt_phone_number, v_null_value) <> NVL(o.alt_phone_number, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Fax was changed from ' || RTRIM (o.fax_number) || ' to ' || RTRIM (n.fax_number),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.fax_number, v_null_value) <> NVL(o.fax_number, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Territory was changed from ' || RTRIM (o.territory) || ' to ' || RTRIM (n.territory),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL(n.territory, v_null_value) <> NVL(o.territory, v_null_value)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  o.florist_id,
        sysdate,
        v_comment_type,
        'Florist status set from Inactive to Active',
        sysdate,
        user
FROM    florist_master o
WHERE   o.florist_id = in_florist_id
AND     o.status = 'Inactive';

-- codification disposition with updated minimum order amount, open_close_code, new or deleted codifications
-- CR 1/2005 - added criteria to not include comment for codifications that are marked as codify all - these will not be deleted
v_comment_type := 'Codification';
INSERT INTO florist_comments
(
        florist_id,
        comment_date,
        comment_type,
        florist_comment,
        created_date,
        created_by
)
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        'Minimum Order was changed from ' || RTRIM (TO_CHAR (o.minimum_order_amount)) || ' to ' || RTRIM (TO_CHAR (n.minimum_order_amount)),
        sysdate,
        user
FROM    florist_master_stage n
JOIN    florist_master o
ON      n.florist_id = o.florist_id
AND     NVL (n.minimum_order_amount, -1) <> NVL (o.minimum_order_amount, -1)
WHERE   o.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        v_comment_type,
        RTRIM (n.codification_id) || ' Product has been added',
        sysdate,
        user
FROM    florist_codifications_stage n
WHERE   n.florist_id = in_florist_id
AND     NOT EXISTS
(
        SELECT  1
        FROM    florist_codifications o
        WHERE   n.florist_id = o.florist_id
        AND     n.codification_id = o.codification_id
)
AND     EXISTS
(
        SELECT 1
        FROM    florist_master o
        WHERE   n.florist_id = o.florist_id
)
UNION ALL
SELECT  o.florist_id,
        sysdate,
        v_comment_type,
        RTRIM (o.codification_id) || ' Product was deleted',
        sysdate,
        user
FROM    florist_codifications o
WHERE   o.florist_id = in_florist_id
AND     NOT EXISTS
(
        SELECT  1
        FROM    florist_codifications_stage n
        WHERE   n.florist_id = o.florist_id
        AND     n.codification_id = o.codification_id
)
AND     EXISTS
(
        SELECT 1
        FROM    florist_master m
        WHERE   m.florist_id = o.florist_id
)
AND     EXISTS
(
        SELECT  1
        FROM    codification_master c
        WHERE   c.codification_id = o.codification_id
        AND     c.codify_all_florist_flag = 'N'
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_FLORIST_COMMENTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOAD_FLORIST_COMMENTS;


PROCEDURE LOAD_NEW_FLORIST_COMMENTS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting comments for new
        florists.  This procedure is called after the new florists are
        loaded into florist master because of the referential integrity
        between florist master and florist comments.

        Comments that are created:
        Profile disposition for added florists

        Zip codes disposition for the zip codes coverage of the physical
        location

        Codification dispositions for the added codifications

Input:
        florist id table                table of new florist ids

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

dbms_application_info.set_client_info('Insert new comments' || '::' || in_florist_id);
-- add florist_zip record for all new florists
INSERT INTO florist_comments
(
        florist_id,
        comment_date,
        comment_type,
        florist_comment,
        created_date,
        created_by
)
SELECT  n.florist_id,
        sysdate,
        'Profile',
        RTRIM (n.florist_id) || ' Florist has been added',
        sysdate,
        user
FROM    florist_master n
WHERE   n.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        'Zip Codes',
        RTRIM (n.zip_code) || ' Zip Code has been added',
        sysdate,
        user
FROM    florist_zips n
WHERE   n.florist_id = in_florist_id
UNION ALL
SELECT  n.florist_id,
        sysdate,
        'Codification',
        RTRIM (n.codification_id) || ' Product has been added',
        sysdate,
        user
FROM    florist_codifications n
WHERE   n.florist_id = in_florist_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_NEW_FLORIST_COMMENTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOAD_NEW_FLORIST_COMMENTS;


PROCEDURE LOAD_FLORIST_CODIFICATIONS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the data from the
        florist_codifications_stage table to the florist_codifications table.
        New florists will be inserted, missing florists codifications
        will be deleted.

Input:
        florist_id                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_codification_id_tab   varchar_tab_typ;
v_null_value            varchar2 (20) := 'check null value';

CURSOR insert_cur IS
        SELECT
                n.codification_id,
                n.holiday_price_flag,
                n.min_price
        FROM    florist_codifications_stage n
        WHERE   n.florist_id = in_florist_id
        AND     NOT EXISTS
        (
                SELECT  1
                FROM    florist_codifications o
                WHERE   n.florist_id = o.florist_id
                AND     n.codification_id = o.codification_id
        );

BEGIN

out_status := 'Y';

-- update the holiday price flag and minimum price for the existing florist codifications
dbms_application_info.set_client_info('Update existing codifications' || '::' || in_florist_id);
UPDATE florist_codifications o
SET
(
        o.holiday_price_flag,
        o.min_price
) =
(
        SELECT  n.holiday_price_flag,
                n.min_price
        FROM    florist_codifications_stage n
        WHERE   n.florist_id = o.florist_id
        AND     n.codification_id = o.codification_id
)
WHERE   o.florist_id = in_florist_id
AND     EXISTS
(
        SELECT  1
        FROM    florist_codifications_stage n
        WHERE   n.florist_id = o.florist_id
        AND     n.codification_id = o.codification_id
        AND     (NVL(o.holiday_price_flag, v_null_value) <> NVL (n.holiday_price_flag, v_null_value)
        OR       NVL(o.min_price, -1) <> NVL(n.min_price, -1))
) RETURNING codification_id BULK COLLECT INTO v_codification_id_tab;

-- insert new florist codification records
IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Insert new codifications' || '::' || in_florist_id);
        FOR rec IN insert_cur LOOP
                INSERT INTO florist_codifications
                (
                        florist_id,
                        codification_id,
                        holiday_price_flag,
                        min_price,
                        codified_date
                )
                VALUES
                (
                        in_florist_id,
                        rec.codification_id,
                        rec.holiday_price_flag,
                        rec.min_price,
                        sysdate
                );
        END LOOP;

        -- for each florist add the new codified products to florist_products
        -- and notify the hp of the new codifications
        FOR x IN 1..v_codification_id_tab.count LOOP
                FLORIST_MAINT_PKG.UPDATE_FLORIST_PRODUCTS (in_florist_id, v_codification_id_tab (x), 'N', 'Insert', out_status, out_message);
                IF out_status = 'N' THEN
                        exit;
                END IF;

        END LOOP;
END IF;

IF out_status = 'Y' THEN

        dbms_application_info.set_client_info('Delete codifications' || '::' || in_florist_id);
        -- delete missing florist codification records for active florists
        -- do not delete history of inactive florists
        -- CR 1/2005 - do not delete florist codifications that are marked as codify all
        DELETE FROM florist_codifications o
        WHERE   o.florist_id = in_florist_id
        AND     NOT EXISTS
        (
                SELECT  1
                FROM    florist_codifications_stage n
                WHERE   n.florist_id = o.florist_id
                AND     n.codification_id = o.codification_id
                AND     EXISTS
                (
                        SELECT  1
                        FROM    florist_master m
                        WHERE   n.florist_id = m.florist_id
                        AND     m.status <> 'Inactive'
                )
        )
                AND     EXISTS
        (
                SELECT  1
                FROM    codification_master m
                WHERE   m.codification_id = o.codification_id
                AND     m.codify_all_florist_flag = 'N'
        )
        RETURNING codification_id BULK COLLECT INTO v_codification_id_tab;

        -- for each florist delete the codified products to florist_products
        FOR x IN 1..v_codification_id_tab.count LOOP
                FLORIST_MAINT_PKG.UPDATE_FLORIST_PRODUCTS (in_florist_id, v_codification_id_tab (x), null, 'Delete', out_status, out_message);
                IF out_status = 'N' THEN
                        exit;
                END IF;
        END LOOP;
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_FLORIST_CODIFICATIONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END LOAD_FLORIST_CODIFICATIONS;


PROCEDURE UPDATE_FLORIST_LOAD_LOCK
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_LOCK_INDICATOR               IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for locking or unlocking the all active
        florist during the florist data load.

Input:
        florist_id                      varchar2
        lock indicator                  varchar2 (Y or N)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;
v_locked_by     varchar2(100) := 'FLORIST LOAD PROCESS';

BEGIN

IF in_lock_indicator = 'Y' THEN
        UPDATE  florist_master
        SET
                locked_by = v_locked_by,
                locked_date = sysdate
        WHERE   florist_id = in_florist_id;

        OUT_STATUS := 'Y';
ELSE
        UPDATE  florist_master
        SET
                locked_by = null,
                locked_date = null
        WHERE   florist_id = in_florist_id
        AND     locked_by = v_locked_by;

        OUT_STATUS := 'Y';
END IF;

commit;

EXCEPTION WHEN OTHERS THEN
BEGIN
        rollback;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_LOAD_LOCK;


PROCEDURE LOAD_FLORIST_MASTER
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the data from the
        florist_master_stage table to the florist_master table.  New florists
        will be inserted, existing florists will be updated, missing florists
        will be updated with an inactive status.  For all new florists, a
        forist_zip record will be added with the florist's physical zip code.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

v_null_value            varchar2 (20) := 'check null value';
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);
v_florist_id            FLORIST_MASTER.FLORIST_ID%TYPE;
v_zip_code              FLORIST_ZIPS.ZIP_CODE%TYPE;
v_old_status            florist_master.status%type;
v_record_type           florist_master.record_type%type;
v_member_weight			number;
v_member_duration 		number;

v_codification_id_tab   varchar_tab_typ;

CURSOR update_cur IS
        SELECT  distinct o.florist_id
        FROM    florist_master_stage n
        JOIN    florist_master o
        ON      n.florist_id = o.florist_id
        AND
        (       NVL (o.florist_name, v_null_value) <> NVL (n.florist_name, v_null_value)
        OR      NVL (o.address , v_null_value)<> NVL (n.address, v_null_value)
        OR      NVL (o.city, v_null_value) <> NVL (n.city, v_null_value)
        OR      NVL (o.state, v_null_value) <> NVL (n.state, v_null_value)
        OR      NVL (o.phone_number, v_null_value) <> NVL (n.phone_number, v_null_value)
        OR      ftd_apps.OE_CLEANUP_ALPHANUM_STRING(NVL (o.zip_code, 'null value'), 'Y') <> ftd_apps.OE_CLEANUP_ALPHANUM_STRING(NVL (decode (ftd_apps.florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code), 'null value'), 'Y')
        OR      NVL (o.city_state_number, v_null_value) <> NVL (n.city_state_number, v_null_value)
        OR      NVL (o.record_type, v_null_value) <> NVL (n.record_type, v_null_value)
        OR      NVL (o.mercury_flag, v_null_value) <> NVL (n.mercury_flag, v_null_value)
        OR      NVL (o.owners_name, v_null_value) <> NVL (n.owners_name, v_null_value)
        OR      NVL (o.minimum_order_amount, -1) <> NVL (n.minimum_order_amount, -1)
        OR      NVL (o.alt_phone_number, v_null_value) <> NVL (n.alt_phone_number, v_null_value)
        OR      NVL (o.fax_number, v_null_value) <> NVL (n.fax_number, v_null_value)
        OR      NVL (o.internal_link_number, v_null_value) <> NVL (n.internal_link_number, v_null_value)
        OR      NVL (o.territory, v_null_value) <> NVL (n.territory, v_null_value)
        OR      NVL (o.parent_florist_id, v_null_value) <> NVL (n.parent_florist_id, v_null_value)
        OR      NVL (o.top_level_florist_id, v_null_value) <> NVL (n.top_level_florist_id, v_null_value)
        OR      o.status = 'Inactive'
        )
        UNION
        -- Include florists where codifications will be removed (or codification price changes for florist)
        SELECT  distinct o.florist_id
        FROM    florist_codifications o
        JOIN    codification_master c  ON c.codification_id = o.codification_id
        JOIN    florist_master fm      ON fm.florist_id = o.florist_id
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    florist_codifications_stage n
                WHERE   n.florist_id = o.florist_id
                AND     n.codification_id = o.codification_id
                AND     NVL(o.holiday_price_flag, v_null_value) = NVL (n.holiday_price_flag, v_null_value)
                AND     NVL(o.min_price, -1) = NVL (n.min_price, -1)
        )
        AND     fm.status <> 'Inactive'
        AND     c.codify_all_florist_flag <> 'Y'
        UNION
        -- Include florists that will have new codifications
        SELECT  distinct n.florist_id
        FROM    florist_codifications_stage n
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    florist_codifications o
                WHERE   n.florist_id = o.florist_id
                AND     n.codification_id = o.codification_id
        )
        AND     EXISTS
        (
                SELECT  1
                FROM    florist_master f
                WHERE   f.florist_id = n.florist_id
        )
        ORDER BY florist_id;

CURSOR new_cur IS
        SELECT  distinct n.florist_id
        FROM    florist_master_stage n
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    florist_master o
                WHERE   n.florist_id = o.florist_id
        )
        --and n.zip_code is not null
        ORDER BY n.florist_id;

-- CR 1/2005 - ignore special florists (ids beginning with 90 or 91), these should not be deleted
--           - future consideration: add a indicator to florist master to specify special florists
CURSOR inactive_cur IS
        SELECT  distinct o.florist_id
        FROM    florist_master o
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    florist_master_stage n
                WHERE   n.florist_id = o.florist_id
        )
        AND     o.status <> 'Inactive'
        AND     o.vendor_flag = 'N'
        AND     substr (florist_id, 1, 2) not in ('90', '91');

BEGIN

out_status := 'Y';

-- process updated florist data
OPEN update_cur;
FETCH update_cur INTO v_florist_id;

dbms_application_info.set_client_info('Process Update Florists' || '::' || v_florist_id);
WHILE update_cur%found LOOP
        -- lock the florist
        update_florist_load_lock (v_florist_id, 'Y', out_status, out_message);

        -- insert comments
        IF out_status = 'Y' THEN
                load_florist_comments (v_florist_id, out_status, out_message);
        END IF;

        IF out_status = 'Y' THEN
                select status, record_type
                into v_old_status, v_record_type
                from florist_master
                where florist_id = v_florist_id;
                
                -- update existing florist master records where the data has changed
                UPDATE  florist_master o
                SET
                (
                        o.florist_name,
                        o.address,
                        o.city,
                        o.state,
                        o.delivery_city,
                        o.delivery_state,
                        o.phone_number,
                        o.zip_code,
                        o.city_state_number,
                        o.record_type,
                        o.mercury_flag,
                        o.owners_name,
                        o.minimum_order_amount,
                        o.status,
                        o.last_updated_by,
                        o.last_updated_date,
                        o.alt_phone_number,
                        o.fax_number,
                        o.internal_link_number,
                        o.territory,
                        o.parent_florist_id,
                        o.top_level_florist_id
                ) =
                (       SELECT
                                n.florist_name,
                                n.address,
                                n.city,
                                n.state,
                                n.city,
                                n.state,
                                n.phone_number,
                                decode (florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code),
                                n.city_state_number,
                                n.record_type,
                                n.mercury_flag,
                                n.owners_name,
                                n.minimum_order_amount,
                                decode (o.status, 'Inactive', 'Active', o.status),
                                user,
                                to_char(sysdate,'dd-MON-yy'),
                                n.alt_phone_number,
                                n.fax_number,
                                n.internal_link_number,
                                n.territory,
                                n.parent_florist_id,
                                n.top_level_florist_id
                        FROM    florist_master_stage n
                        WHERE   n.florist_id = o.florist_id
                )
                WHERE o.florist_id = v_florist_id
                AND EXISTS
                (
                        SELECT  1
                        FROM    florist_master_stage n
                        WHERE   n.florist_id = o.florist_id
                        AND
                        (       NVL (o.florist_name, v_null_value) <> NVL (n.florist_name, v_null_value)
                        OR      NVL (o.address , v_null_value)<> NVL (n.address, v_null_value)
                        OR      NVL (o.city, v_null_value) <> NVL (n.city, v_null_value)
                        OR      NVL (o.state, v_null_value) <> NVL (n.state, v_null_value)
                        OR      NVL (o.phone_number, v_null_value) <> NVL (n.phone_number, v_null_value)
                        OR      NVL (o.zip_code, v_null_value) <> NVL (decode (florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code), v_null_value)
                        OR      NVL (o.city_state_number, v_null_value) <> NVL (n.city_state_number, v_null_value)
                        OR      NVL (o.record_type, v_null_value) <> NVL (n.record_type, v_null_value)
                        OR      NVL (o.mercury_flag, v_null_value) <> NVL (n.mercury_flag, v_null_value)
                        OR      NVL (o.owners_name, v_null_value) <> NVL (n.owners_name, v_null_value)
                        OR      NVL (o.minimum_order_amount, -1) <> NVL (n.minimum_order_amount, -1)
                        OR      NVL (o.alt_phone_number, v_null_value) <> NVL (n.alt_phone_number, v_null_value)
                        OR      NVL (o.fax_number, v_null_value) <> NVL (n.fax_number, v_null_value)
                        OR      NVL (o.internal_link_number, v_null_value) <> NVL (n.internal_link_number, v_null_value)
                        OR      NVL (o.territory, v_null_value) <> NVL (n.territory, v_null_value)
                        OR      NVL (o.parent_florist_id, v_null_value) <> NVL (n.parent_florist_id, v_null_value)
                        OR      NVL (o.top_level_florist_id, v_null_value) <> NVL (n.top_level_florist_id, v_null_value)
                        OR      o.status = 'Inactive'
                        )
                );

        END IF;

        -- update codifications
        IF out_status = 'Y' THEN
                load_florist_codifications (v_florist_id, out_status, out_message);
        END IF;

        -- add florist_zips record if previously Inactive
        IF out_status = 'Y' AND v_old_status = 'Inactive' AND v_record_type = 'R' THEN
            BEGIN
	            IF substr (v_florist_id, 8, 1) <> 'Z' THEN
	               	SELECT  decode (florist_query_pkg.get_country_from_zip(f.zip_code), 'CA',
	                                    substr (f.zip_code, 1, 3),    -- store only first 3 chars of Canadian zip
	                                    substr (f.zip_code, 1, 5) )    -- otherwise just store first 5 chars
	                INTO    v_zip_code
	                FROM    florist_master f
	                JOIN    state_master s
	                ON      s.state_master_id = f.state
	                WHERE   f.florist_id = v_florist_id
	                AND     f.record_type = 'R'
	                AND     (florist_query_pkg.get_country_from_zip(f.zip_code) = 'CA'
	                OR       EXISTS (select 1 from zip_code z where z.zip_code_id = substr (f.zip_code, 1, 5))
	                AND     (s.country_code IS NULL OR s.country_code <> 'INT')   -- Dont add zips for International florists
	                        );

	                INSERT INTO florist_zips
	                (
	                        florist_id,
	                        zip_code,
	                        last_update_date,
	                        cutoff_time
	                )
	                values
	                (
	                 		v_florist_id,
	                        UPPER(v_zip_code),
	                        sysdate,
	                        '1400'
	                );

	            END IF;
            EXCEPTION WHEN NO_DATA_FOUND THEN
                --do nothing...just continue on
                v_zip_code := null;
            END;
        END IF;

        -- commit florist update
        IF out_status = 'Y' THEN
                commit;
                -- unlock the florist
                update_florist_load_lock (v_florist_id, 'N', out_status, out_message);
        ELSE
                -- insert a system message
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST DATA LOAD',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'Update Florist ID:' || v_florist_id || ' -- ' || out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );

                rollback;
                -- unlock the florist
                update_florist_load_lock (v_florist_id, 'N', out_status, out_message);

                exit;

        END IF;

        FETCH update_cur INTO v_florist_id;
END LOOP;

CLOSE update_cur;
v_florist_id := null;

IF out_status = 'Y' THEN
        -- get and save the new florists ids
        
         BEGIN
        	select TO_NUMBER(value) into v_member_weight from frp.global_parms where context = 'LOAD_MEMBER_DATA_CONFIG' and name = 'MEMBER_WEIGHT';
			select TO_NUMBER(value) into v_member_duration from frp.global_parms where context = 'LOAD_MEMBER_DATA_CONFIG' and name = 'MEMBER_DURATION';
       
         EXCEPTION 
        	WHEN OTHERS THEN
	        	v_member_weight := 8;
	        	v_member_duration := 180;
         END;
        
        OPEN new_cur;
        FETCH new_cur INTO v_florist_id;

        WHILE new_cur%found LOOP
                dbms_application_info.set_client_info('Process Insert Florists' || '::' || v_florist_id);

                -- insert new florist master records
                INSERT INTO florist_master
                (
                        florist_id,
                        florist_name,
                        address,
                        city,
                        state,
                        delivery_city,
                        delivery_state,
                        phone_number,
                        zip_code,
                        city_state_number,
                        record_type,
                        mercury_flag,
                        owners_name,
                        minimum_order_amount,
                        top_level_florist_id,
                        status,
                        last_updated_by,
                        last_updated_date,
                        alt_phone_number,
                        fax_number,
                        internal_link_number,
                        territory,
                        vendor_flag,
                        adjusted_weight,
                        adjusted_weight_start_date,
                        adjusted_weight_end_date,
                        parent_florist_id
                )
                SELECT
                        n.florist_id,
                        n.florist_name,
                        n.address,
                        n.city,
                        n.state,
                        n.city,
                        n.state,
                        n.phone_number,
                        decode (florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code),
                        n.city_state_number,
                        n.record_type,
                        n.mercury_flag,
                        n.owners_name,
                        n.minimum_order_amount,
                        n.top_level_florist_id,
                        'Active',
                        user,
                        to_char(sysdate,'dd-MON-yy'),
                        alt_phone_number,
                        fax_number,
                        internal_link_number,
                        territory,
                        'N',
                        v_member_weight,                     -- new members get a weight of 12 for 3 months (defect 926)
                        sysdate,
                        TRUNC(SYSDATE + v_member_duration),
                        n.parent_florist_id
                FROM    florist_master_stage n
                WHERE   n.florist_id = v_florist_id;

                -- only add zip code for new florist with Regular listings
                -- CR 1/2005 - don't add zip for send only florists (suffix begins with Z)
                BEGIN
	                IF substr (v_florist_id, 8, 1) <> 'Z' THEN
	                	SELECT  decode (florist_query_pkg.get_country_from_zip(f.zip_code), 'CA',
	                                        substr (f.zip_code, 1, 3),    -- store only first 3 chars of Canadian zip
	                                        substr (f.zip_code, 1, 5) )    -- otherwise just store first 5 chars
	                      INTO v_zip_code
	                    FROM    florist_master f
	                    JOIN    state_master s
	                    ON      s.state_master_id = f.state
	                    WHERE   f.florist_id = v_florist_id
	                    AND     f.record_type = 'R'
	                    AND     (florist_query_pkg.get_country_from_zip(f.zip_code) = 'CA'
	                    OR       EXISTS (select 1 from zip_code z where z.zip_code_id = substr (f.zip_code, 1, 5))
	                    AND     (s.country_code IS NULL OR s.country_code <> 'INT')   -- Dont add zips for International florists
	                            );

                        dbms_application_info.set_client_info('Process Insert Florist Zips' || '::' || v_florist_id);
	                    INSERT INTO florist_zips
	                    (
	                            florist_id,
	                            zip_code,
	                            last_update_date,
	                            cutoff_time
	                    )
	                    values
	                    (
	                    		v_florist_id,
	                            UPPER(v_zip_code),
	                            sysdate,
	                            '1400'
	                    );

	                END IF;
            	EXCEPTION WHEN NO_DATA_FOUND THEN
            	    --do nothing...just continue on
            	    v_zip_code := null;
            	END;

                -- add codifications to the new florists
                IF out_status = 'Y' THEN
                    dbms_application_info.set_client_info('Process Insert Florist Codifications' || '::' || v_florist_id);
	                INSERT INTO florist_codifications
	                (
	                        florist_id,
	                        codification_id,
	                        holiday_price_flag,
	                        min_price,
	                        codified_date
	                )
	                SELECT  v_florist_id,
	                        codification_id,
	                        null,
	                        null,
	                        sysdate
	                FROM    codification_master
	                WHERE   codify_all_florist_flag = 'Y'
	                UNION
	                SELECT  florist_id,
	                        codification_id,
	                        holiday_price_flag,
	                        min_price,
	                        sysdate
	                FROM    florist_codifications_stage
	                WHERE   florist_id = v_florist_id;

                    dbms_application_info.set_client_info('Process Insert Florist Products' || '::' || v_florist_id);
	                -- add florist products for the new florists
	                INSERT INTO florist_products
	                (
	                        florist_id,
	                        product_id
	                )
	                SELECT  fc.florist_id,
	                        cp.product_id
	                FROM    florist_codifications fc
	                JOIN    codified_products cp
	                ON      fc.codification_id = cp.codification_id
	                WHERE   fc.florist_id = v_florist_id;

	                -- load the comments for the new florists
	                load_new_florist_comments (v_florist_id, out_status, out_message);
                END IF;

                -- commit florist update
                IF out_status = 'Y' THEN
                        commit;
                ELSE
                        rollback;
                        -- insert a system message
                        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                        (
                                IN_SOURCE => 'FLORIST DATA LOAD',
                                IN_TYPE => 'EXCEPTION',
                                IN_MESSAGE => 'Insert Florist ID:' || v_florist_id || ' -- ' || out_message,
                                IN_COMPUTER => null,
                                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                                OUT_STATUS => v_out_status,
                                OUT_MESSAGE => v_out_message
                        );

                        exit;

                END IF;

                FETCH new_cur INTO v_florist_id;

        END LOOP;
        CLOSE new_cur;
        v_florist_id := null;
END IF;

IF out_status = 'Y' THEN

        OPEN inactive_cur;
        FETCH inactive_cur INTO v_florist_id;

        dbms_application_info.set_client_info('Process Inactive Florists' || '::' || v_florist_id);
        WHILE inactive_cur%found LOOP

                -- lock the florist
                update_florist_load_lock (v_florist_id, 'Y', out_status, out_message);

                -- inactivate the florist
                UPDATE  florist_master o
                SET     o.status = 'Inactive',
                        o.last_updated_by = user,
                        o.last_updated_date = to_char(sysdate,'dd-MON-yy')
                WHERE   o.florist_id = v_florist_id;

                -- load comments for deleted florists
                INSERT INTO florist_comments
                (
                        florist_id,
                        comment_date,
                        comment_type,
                        florist_comment,
                        created_date,
                        created_by
                )
                VALUES
                (
                        v_florist_id,
                        sysdate,
                        'Profile',
                        RTRIM (v_florist_id) || ' Florist has been inactivated',
                        sysdate,
                        user
                );

                -- commit florist update
                IF out_status = 'Y' THEN
                        commit;
                        -- unlock the florist
                        update_florist_load_lock (v_florist_id, 'N', out_status, out_message);
                ELSE
                        -- insert a system message
                        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                        (
                                IN_SOURCE => 'FLORIST DATA LOAD',
                                IN_TYPE => 'EXCEPTION',
                                IN_MESSAGE => 'Inactive Florist ID:' || v_florist_id || ' -- ' || out_message,
                                IN_COMPUTER => null,
                                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                                OUT_STATUS => v_out_status,
                                OUT_MESSAGE => v_out_message
                        );

                        rollback;
                        -- unlock the florist
                        update_florist_load_lock (v_florist_id, 'N', out_status, out_message);

                        exit;

                END IF;

                FETCH inactive_cur INTO v_florist_id;
        END LOOP;

        CLOSE inactive_cur;
        v_florist_id := null;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_FLORIST_MASTER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

        rollback;
        -- insert a system message
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST DATA LOAD',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'Florist ID::' || v_florist_id || ' -- ' || out_message,
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );

        -- unlock the florist if it's still locked
        update_florist_load_lock (v_florist_id, 'N', v_out_status, v_out_message);

END LOAD_FLORIST_MASTER;


PROCEDURE LOAD_CODIFICATION_MASTER
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the data from the
        codification_master_stage table to the codification_master table.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- cursor for existing codifications
CURSOR update_cur IS
    SELECT cms.codification_id,
        cms.category_id,
        cms.description
    FROM codification_master_stage cms
    JOIN codification_master cm
      ON cm.codification_id = cms.codification_id;

-- cursor for new codifications
CURSOR insert_cur IS
    SELECT cms.codification_id,
        cms.category_id,
        cms.description
    FROM codification_master_stage cms
    WHERE codification_id not in (
        select codification_id
        FROM codification_master);

BEGIN

    -- delete florist codifications if the codification_master_stage
    -- record is not active for ftd.com

    delete from ftd_apps.florist_codifications_stage fcs
    where fcs.codification_id not in (
        select cms.codification_id
        from ftd_apps.codification_master_stage cms
        where cms.active_for_dotcom = 'true');


    -- update existing codifications

    for rec in update_cur loop

        update codification_master
        set category_id = rec.category_id,
            description = rec.description,
            load_from_file = 'Y',
            codify_all_florist_flag = 'N'
        where codification_id = rec.codification_id;

    end loop;


    -- insert new codifications

    for rec in insert_cur loop

        insert into codification_master (
            codification_id,
            category_id,
            description,
            load_from_file,
            codify_all_florist_flag,
            priority_flag,
            codification_required)
        values (
            rec.codification_id,
            rec.category_id,
            rec.description,
            'Y',
            'N',
            'N',
            'N');

    end loop;
    
    out_status := 'Y';
    commit;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_CODIFICATION_MASTER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOAD_CODIFICATION_MASTER;

PROCEDURE REMOVE_CODIFICATION_MASTER
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing unused codificiations
        from the codification_master table.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- cursor for removed codifications
CURSOR remove_cur IS
    select codification_id, category_id, description,
        (select count(*)
        from codified_products cp
        where cp.codification_id = cm.codification_id) cp_count,
        (select count(*)
        from florist_codifications fc
        where fc.codification_id = cm.codification_id) fc_count
    from codification_master cm
    where not exists (select 'Y'
        from codification_master_stage cms
        where cm.codification_id = cms.codification_id)
    and load_from_file = 'Y'
    order by codification_id;

v_remove_message varchar2(4000) := '';
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

BEGIN

    -- delete removed codifications
    
    v_remove_message := '';
    for rec in remove_cur loop

        if (rec.cp_count > 0 or rec.fc_count > 0) then

            v_remove_message := v_remove_message || rec.codification_id || ' ' || rec.description || chr(10);
            
            update codification_master
            set load_from_file = 'N'
            where codification_id = rec.codification_id;
                
        else
        
            delete from codification_master
            where codification_id = rec.codification_id;
            
        end if;

    end loop;
    
    if length(v_remove_message) > 0 then
        v_remove_message := 'The following codifications could not be removed because they still' ||
            ' have products assigned to them. The load_from_file value will be changed to N.' ||
            chr(10) || chr(10) || v_remove_message;
            
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
            IN_SOURCE => 'FLORIST DATA LOAD',
            IN_TYPE => 'EXCEPTION',
            IN_MESSAGE => v_remove_message,
            IN_COMPUTER => null,
            IN_EMAIL_SUBJECT => 'Codification Master Removal Errors',
            OUT_SYSTEM_MESSAGE_ID => v_message_id,
            OUT_STATUS => v_out_status,
            OUT_MESSAGE => v_out_message
        );
    end if;

    out_status := 'Y';
    commit;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN REMOVE_CODIFICATION_MASTER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END REMOVE_CODIFICATION_MASTER;


PROCEDURE REMOVE_MISSING_TOP_LEVELS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        The member file may define relationships of florists to top-level florists
        that do not exist.  This procedure is responsible for finding an such
        relationships and removing them.
        
        Note this is done on the staging table and is invoked at the start
        of the florist load approval process (before staging is moved to prod).

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_remove_message varchar2(4000) := '';
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

BEGIN

    -- If top-level florist of E or S type florist does not exist, update it so top-level is parent instead 
    --
    update ftd_apps.florist_master_stage fms
       set fms.top_level_florist_id = fms.parent_florist_id
    where fms.record_type in ('E','S')
    and fms.florist_id <> fms.top_level_florist_id
    and fms.parent_florist_id <> fms.top_level_florist_id
    and fms.top_level_florist_id in (
       -- All florists listed as top-level florists, but don't exist in Apollo
       select distinct fms2.top_level_florist_id
       from ftd_apps.florist_master_stage fms2
       where not exists(select 1 from ftd_apps.florist_master_stage fms3 where fms3.FLORIST_ID=fms2.TOP_LEVEL_FLORIST_ID)
       and   not exists(select 1 from ftd_apps.florist_master fm where fm.FLORIST_ID=fms2.TOP_LEVEL_FLORIST_ID)
    );

    -- If top-level florist of R type florist does not exist, update it so top-level is itself instead (i.e., standalone)
    --
    update ftd_apps.florist_master_stage fms
       set fms.top_level_florist_id = fms.florist_id,
       fms.parent_florist_id = null
    where fms.record_type = 'R'
    and fms.florist_id <> fms.top_level_florist_id
    and fms.top_level_florist_id in (
       -- All florists listed as top-level florists, but don't exist in Apollo
       select distinct fms2.top_level_florist_id
       from ftd_apps.florist_master_stage fms2
       where not exists(select 1 from ftd_apps.florist_master_stage fms3 where fms3.FLORIST_ID=fms2.TOP_LEVEL_FLORIST_ID)
       and   not exists(select 1 from ftd_apps.florist_master fm where fm.FLORIST_ID=fms2.TOP_LEVEL_FLORIST_ID)
    );

    out_status := 'Y';
    commit;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN REMOVE_MISSING_TOP_LEVELS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END REMOVE_MISSING_TOP_LEVELS;


PROCEDURE LOAD_FLORIST_DATA
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the florist and city/stage
        data from the staging table to the production tables.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

TYPE stage_typ IS VARRAY (10) OF VARCHAR2(30);
stage_tab       stage_typ := stage_typ ('florist_master_stage',
                                        'florist_codifications_stage',
                                        'efos_city_state_stage'
                                        );
TYPE cur_typ IS REF CURSOR;
cur             cur_typ;
v_count         number := 0;

BEGIN

-- check if any of the stage tables has zero records to prevent
-- accidental inactivating of all florists or deleting of all codifications.
out_status := 'Y';
FOR x in stage_tab.first..stage_tab.last LOOP
        open cur for 'select count (*) from ' || stage_tab(x);
        fetch cur into v_count;
        close cur;

        IF v_count = 0 THEN
                out_status := 'N';
                out_message := stage_tab(x) || ' has 0 records.';
                exit;
        END IF;
END LOOP;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Remove any non-existent top-level florists');
        remove_missing_top_levels (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Process Efos City State');
        load_efos_city_state (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Process Codification Data');
        load_codification_master (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Process Florist Data');
        load_florist_master (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Remove Codification Master Data');
        remove_codification_master (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Process Florist Hours');
        load_florist_hours (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Verify Member Load');
        verify_member_load (out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Update Message Forwarding Flag');
        FLORIST_MAINT_PKG.UPDATE_FLORIST_N_INSERT_CMMNTS(out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Update Linked Zips');
        update_linked_zips(out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Update Zip Master Florist ID');
        FLORIST_MAINT_PKG.update_zip_master_florist_id(out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Remove Inactive Florist Products Data');
        delete from florist_products fp
        where not exists (select 'Y'
            from codified_products cp
            where cp.product_id = fp.product_id)
        and not exists (select 'Y'
            from product_master pm
            where pm.product_id = fp.product_id
            and pm.status = 'A');
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Remove Inactive Florist Data');
        delete from florist_zips fz
        where exists (select 'Y'
            from florist_master fm
            where fm.florist_id = fz.florist_id
            and fm.status = 'Inactive');
        delete from florist_blocks fb
        where exists (select 'Y'
            from florist_master fm
            where fm.florist_id = fb.florist_id
            and fm.status = 'Inactive');
        delete from florist_suspends fs
        where exists (select 'Y'
            from florist_master fm
            where fm.florist_id = fs.florist_id
            and fm.status = 'Inactive');
        delete from florist_city_blocks fcb
        where exists (select 'Y'
            from florist_master fm
            where fm.florist_id = fcb.florist_id
            and fm.status = 'Inactive');
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Updating Parent/Child blocks and suspends');
        update_child_blocks_suspends(out_status, out_message);
END IF;

IF out_status = 'Y' THEN
        dbms_application_info.set_client_info('Florist Data Load Process Complete');
END IF;

END LOAD_FLORIST_DATA;


PROCEDURE BUILD_FLORIST_AUDIT
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for building the florist audit report and
        storing the data into the audit report tables.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_report_id             AUDIT_HOME.AUDIT_REPORT_HEADER.REPORT_ID%TYPE;
v_row_sequence          AUDIT_HOME.AUDIT_REPORT_DETAIL.ROW_SEQUENCE%TYPE := 1;

CURSOR florist_cur IS
        select  1 row_sequence,
                'Existing (Mbrs)' field_name,
                nvl(sum (decode(record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(record_type, 'E', 1, 0)), 0) value_3
        from    florist_master
        where   status <> 'Inactive'
        union
        select  2 row_sequence,
                'Read From Tape' field_name,
                nvl(sum (decode(record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(record_type, 'E', 1, 0)), 0) value_3
        from    florist_master_stage
        union
        select  3 row_sequence,
                'Adds (Tape)' field_name,
                nvl(sum (decode(s.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(s.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(s.record_type, 'E', 1, 0)), 0) value_3
        from    florist_master_stage s
        where   not exists
        (
                select  1
                from    florist_master f
                where   f.florist_id = s.florist_id
        )
        union
        select  4 row_sequence,
                'To Be Deleted' field_name,
                nvl(sum (decode(f.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(f.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(f.record_type, 'E', 1, 0)), 0) value_3
        from    florist_master f
        where   f.status <> 'Inactive'
        and     not exists
        (
                select  1
                from    florist_master_stage s
                where   f.florist_id = s.florist_id
        )
        AND     substr (f.florist_id, 1, 2) not in ('90', '91')
        union
        select  5 row_sequence,
                'Minimum Order Value Existing (Mbrs)' field_name,
                nvl(sum (decode(record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(record_type, 'E', 1, 0)), 0) value_3
        from    florist_master
        where   status <> 'Inactive'
        and     minimum_order_amount > 0
        union
        select  6 row_sequence,
                'Minimum Order Value Read From Tape' field_name,
                nvl(sum (decode(record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(record_type, 'E', 1, 0)), 0) value_3
        from    florist_master_stage
        where   minimum_order_amount > 0
        union
        select  7 row_sequence,
                'Minimum Order Value New (Tape)' field_name,
                nvl(sum (decode(s.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(s.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(s.record_type, 'E', 1, 0)), 0) value_3
        from    florist_master_stage s
        where   s.minimum_order_amount > 0
        and     exists
        (
                select  1
                from    florist_master f
                where   f.florist_id = s.florist_id
                and     (f.minimum_order_amount = 0 or f.minimum_order_amount is null)
        )
        union
        select  8 row_sequence,
                'Minimum Order Value To Be Deleted' field_name,
                nvl(sum (decode(f.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(f.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(f.record_type, 'E', 1, 0)), 0) value_3
        from    florist_master f
        where   f.status <> 'Inactive'
        and     f.minimum_order_amount > 0
        and     not exists
        (
                select  1
                from    florist_master_stage s
                where   f.florist_id = s.florist_id
                and     s.minimum_order_amount > 0
        )
        AND     substr (f.florist_id, 1, 2) not in ('90', '91')
        union
        select  9 row_sequence,
                'Mercury Existing (Mbrs)' field_name,
                nvl(sum (decode(record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(record_type, 'E', 1, 0)), 0) value_3
        from    florist_master
        where   status <> 'Inactive'
        and     mercury_flag = 'M'
        union
        select  10 row_sequence,
                'Mercury Read From Tape' field_name,
                nvl(sum (decode(record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(record_type, 'E', 1, 0)), 0) value_3
        from    florist_master_stage
        where   mercury_flag = 'M'
        union
        select  11 row_sequence,
                'Mercury New (Tape)' field_name,
                nvl(sum (decode(s.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(s.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(s.record_type, 'E', 1, 0)), 0) value_3
        from    florist_master_stage s
        where   s.mercury_flag = 'M'
        and     not exists
        (
                select  1
                from    florist_master f
                where   f.florist_id = s.florist_id
                and     f.mercury_flag = 'M'
        )
        order by row_sequence;
v_florist_row                   florist_cur%rowtype;

CURSOR codified_cur IS
        select  1 row_sequence,
                'Codified Item "' || c.codification_id || '" (Mbrs)' field_name,
                nvl(sum (decode(f.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(f.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(f.record_type, 'E', 1, 0)), 0) value_3,
                c.codification_id codification_id
        from    florist_codifications c
        join    florist_master f
        on      c.florist_id = f.florist_id
        and     f.status <> 'Inactive'
        join ftd_apps.codification_master_stage s
        on   s.codification_id = c.codification_id
        group by c.codification_id
        union
        select  2 row_sequence,
                'Codified Item "' || s.codification_id || '" Read From Tape' field_name,
                nvl(sum (decode(f.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(f.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(f.record_type, 'E', 1, 0)), 0) value_3,
                s.codification_id codification_id
        from    florist_codifications_stage s
        join    florist_master_stage f
        on      s.florist_id = f.florist_id
        join ftd_apps.codification_master_stage c
        on   c.codification_id = s.codification_id
        and c.active_for_dotcom = 'true'
        group by s.codification_id
        union
        select  3 row_sequence,
                'Codified Item "' || s.codification_id || '" New (Tape)' field_name,
                nvl(sum (decode(f.record_type, 'R', 1, 0)), 0) value_1,
                nvl(sum (decode(f.record_type, 'S', 1, 0)), 0) value_2,
                nvl(sum (decode(f.record_type, 'E', 1, 0)), 0) value_3,
                s.codification_id codification_id
        from    florist_codifications_stage s
        left outer join florist_master_stage f
        on      s.florist_id = f.florist_id
        and     not exists
        (
                select  1
                from    florist_codifications c
                where   c.florist_id = f.florist_id
                and     c.codification_id = s.codification_id
        )
        join ftd_apps.codification_master_stage cs
        on   cs.codification_id = s.codification_id
        and cs.active_for_dotcom = 'true'
        group by s.codification_id
        order by codification_id, row_sequence;
v_codified_row                  codified_cur%rowtype;
BEGIN

AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_HEADER
(
        'Florist Audit',
        'Florist Audit Report',
        'New',
        user,
        v_report_id,
        out_status,
        out_message
);

IF out_status = 'Y' THEN
        FOR v_florist_row in florist_cur LOOP
                AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_DETAIL
                (
                        v_report_id,
                        v_florist_row.field_name,
                        v_row_sequence,
                        null,
                        to_char(v_florist_row.value_1),
                        to_char(v_florist_row.value_2),
                        to_char(v_florist_row.value_3),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        out_status,
                        out_message
                );

                IF out_status = 'N' THEN
                        exit;
                ELSE
                        v_row_sequence := v_row_sequence + 1;
                END IF;
        END LOOP;
END IF;

IF out_status = 'Y' THEN
        FOR v_codified_row in codified_cur LOOP
                AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_DETAIL
                (
                        v_report_id,
                        v_codified_row.field_name,
                        v_row_sequence,
                        null,
                        to_char(v_codified_row.value_1),
                        to_char(v_codified_row.value_2),
                        to_char(v_codified_row.value_3),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        out_status,
                        out_message
                );

                IF out_status = 'N' THEN
                        exit;
                ELSE
                        v_row_sequence := v_row_sequence + 1;
                END IF;
        END LOOP;
END IF;

END BUILD_FLORIST_AUDIT;

PROCEDURE INSERT_CODIFICATION_MASTER_STG
(
IN_CODIFICATION_ID                IN CODIFICATION_MASTER_STAGE.CODIFICATION_ID%TYPE,
IN_CATEGORY_ID                    IN CODIFICATION_MASTER_STAGE.CATEGORY_ID%TYPE,
IN_DESCRIPTION                    IN CODIFICATION_MASTER_STAGE.DESCRIPTION%TYPE,
IN_CATEGORY_TYPE                  IN CODIFICATION_MASTER_STAGE.CATEGORY_TYPE%TYPE,
IN_ACTIVE_FOR_DOTCOM              IN CODIFICATION_MASTER_STAGE.ACTIVE_FOR_DOTCOM%TYPE,
OUT_STATUS                        OUT VARCHAR2,
OUT_MESSAGE                       OUT VARCHAR2
) AS

BEGIN

    INSERT INTO CODIFICATION_MASTER_STAGE (
        CODIFICATION_ID,
        CATEGORY_ID,
        DESCRIPTION,
        CATEGORY_TYPE,
        ACTIVE_FOR_DOTCOM
    ) VALUES (
        IN_CODIFICATION_ID,
        DECODE(IN_CATEGORY_ID, 'GENERIC', 'Other', INITCAP(IN_CATEGORY_ID)),
        IN_DESCRIPTION,
        IN_CATEGORY_TYPE,
        IN_ACTIVE_FOR_DOTCOM
    );

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CODIFICATION_MASTER_STG;

PROCEDURE INSERT_FLORIST_HOURS_STG
(
IN_FLORIST_ID                   IN FLORIST_HOURS_STAGE.FLORIST_ID%TYPE,
IN_DAY_OF_WEEK                  IN FLORIST_HOURS_STAGE.DAY_OF_WEEK%TYPE,
IN_OPEN_CLOSE_CODE              IN FLORIST_HOURS_STAGE.OPEN_CLOSE_CODE%TYPE,
IN_OPEN_TIME                    IN FLORIST_HOURS_STAGE.OPEN_TIME%TYPE,
IN_CLOSE_TIME                   IN FLORIST_HOURS_STAGE.CLOSE_TIME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

    INSERT INTO FLORIST_HOURS_STAGE (
        FLORIST_ID,
        DAY_OF_WEEK,
        OPEN_CLOSE_CODE,
        OPEN_TIME,
        CLOSE_TIME
    ) VALUES (
        IN_FLORIST_ID,
        IN_DAY_OF_WEEK,
        IN_OPEN_CLOSE_CODE,
        IN_OPEN_TIME,
        IN_CLOSE_TIME
    );
    
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_FLORIST_HOURS_STG;

PROCEDURE LOAD_FLORIST_HOURS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the data from the
        florist_hours_stage table to the florist_hours table.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- cursor for existing hours that have changed
CURSOR update_cur IS
    SELECT fhs.florist_id,
        fhs.day_of_week,
        fhs.open_close_code,
        fhs.open_time,
        fhs.close_time,
        fh.open_close_code old_open_close_code
    FROM florist_hours_stage fhs
    JOIN florist_hours fh
      ON fh.florist_id = fhs.florist_id
     AND fh.day_of_week = fhs.day_of_week
     AND nvl(fh.open_close_code, 'NA') <> nvl(fhs.open_close_code, 'NA');

-- cursor for new hours
CURSOR insert_cur IS
    SELECT fhs.florist_id,
        fhs.day_of_week,
        fhs.open_close_code,
        fhs.open_time,
        fhs.close_time
    FROM florist_hours_stage fhs
    WHERE not exists (select 'Y'
        FROM florist_hours fh
        WHERE fh.florist_id = fhs.florist_id
        AND fh.day_of_week = fhs.day_of_week);

BEGIN

    -- update existing hours
    dbms_application_info.set_client_info('Update Florist Hours');
    for rec in update_cur loop

        INSERT INTO florist_comments
        (
            florist_id,
            comment_date,
            comment_type,
            florist_comment,
            created_date,
            created_by
        ) values (
            rec.florist_id,
            sysdate,
            'Codification',
            initcap(rec.day_of_week) || ' Delivery was changed from ' || 
                decode(rec.old_open_close_code, 'F', 'Closed', 'A', 'Closed AM', 'P', 'Closed PM', 'Open') || ' to ' || 
                decode(rec.open_close_code, 'F', 'Closed', 'A', 'Closed AM', 'P', 'Closed PM', 'Open'),
            sysdate,
            user
        );

        update florist_hours
        set open_close_code = rec.open_close_code,
            open_time = rec.open_time,
            close_time = rec.close_time,
            updated_on = sysdate,
            updated_by = 'SYS'
        where florist_id = rec.florist_id
        and day_of_week = rec.day_of_week;

    end loop;

    -- insert new hours
    dbms_application_info.set_client_info('Insert Florist Hours');
    for rec in insert_cur loop

        IF rec.open_close_code is not null THEN
            INSERT INTO florist_comments
            (
                florist_id,
                comment_date,
                comment_type,
                florist_comment,
                created_date,
                created_by
            ) values (
                rec.florist_id,
                sysdate,
                'Codification',
                initcap(rec.day_of_week) || ' Delivery was added as ' || 
                    decode(rec.open_close_code, 'F', 'Closed', 'A', 'Closed AM', 'P', 'Closed PM', 'Open'),
                sysdate,
                user
            );
        END IF;

        insert into florist_hours (
            florist_id,
            day_of_week,
            open_close_code,
            open_time,
            close_time,
            created_on,
            created_by,
            updated_on,
            updated_by)
        values (
            rec.florist_id,
            rec.day_of_week,
            rec.open_close_code,
            rec.open_time,
            rec.close_time,
            sysdate,
            'SYS',
            sysdate,
            'SYS');

    end loop;
    
    -- delete inactive hours
    dbms_application_info.set_client_info('Delete Florist Hours');
    delete from florist_hours fh
    where not exists (select 'Y'
        from florist_hours_stage fhs
        where fhs.florist_id = fh.florist_id
        and fhs.day_of_week = fh.day_of_week);
    
    -- delete overrides older than 60 days
    dbms_application_info.set_client_info('Delete Florist Hours Overrides');
    delete from florist_hours_override
    where override_date < trunc(sysdate-60);

    out_status := 'Y';
    commit;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_FLORIST_HOURS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOAD_FLORIST_HOURS;

PROCEDURE VERIFY_MEMBER_LOAD
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

v_message               varchar2(4000);
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(4000);
crlf                    varchar2(2) := chr(13) || chr(10);

CURSOR verify_cur IS
    SELECT  distinct 'Florist Master updates' as message
        FROM    ftd_apps.florist_master_stage n
        JOIN    ftd_apps.florist_master o
        ON      n.florist_id = o.florist_id
        AND
        (       NVL (o.florist_name, 'null') <> NVL (n.florist_name, 'null')
        OR      NVL (o.address , 'null')<> NVL (n.address, 'null')
        OR      NVL (o.city, 'null') <> NVL (n.city, 'null')
        OR      NVL (o.state, 'null') <> NVL (n.state, 'null')
        OR      NVL (o.phone_number, 'null') <> NVL (n.phone_number, 'null')
        --OR      ftd_apps.OE_CLEANUP_ALPHANUM_STRING(NVL (o.zip_code, 'null value'), 'Y') <> ftd_apps.OE_CLEANUP_ALPHANUM_STRING(NVL (decode (ftd_apps.florist_query_pkg.get_country_from_zip (n.zip_code), 'CA', substr (n.zip_code, 1, 3) || substr (substr (n.zip_code, 4), -3), n.zip_code), 'null value'), 'Y')
        OR      NVL (o.city_state_number, 'null') <> NVL (n.city_state_number, 'null')
        OR      NVL (o.record_type, 'null') <> NVL (n.record_type, 'null')
        OR      NVL (o.mercury_flag, 'null') <> NVL (n.mercury_flag, 'null')
        OR      NVL (o.owners_name, 'null') <> NVL (n.owners_name, 'null')
        OR      NVL (o.minimum_order_amount, -1) <> NVL (n.minimum_order_amount, -1)
        OR      NVL (o.alt_phone_number, 'null') <> NVL (n.alt_phone_number, 'null')
        OR      NVL (o.fax_number, 'null') <> NVL (n.fax_number, 'null')
        OR      NVL (o.internal_link_number, 'null') <> NVL (n.internal_link_number, 'null')
        OR      NVL (o.territory, 'null') <> NVL (n.territory, 'null')
        OR      NVL (o.parent_florist_id, 'null') <> NVL (n.parent_florist_id, 'null')
        OR      NVL (o.top_level_florist_id, 'null') <> NVL (n.top_level_florist_id, 'null')
        OR      o.status = 'Inactive'
        )
        UNION
        -- Include florists where codifications will be removed (or codification price changes for florist)
        SELECT  distinct 'Florist Codification removals' as message
        FROM    ftd_apps.florist_codifications o
        JOIN    ftd_apps.codification_master c  ON c.codification_id = o.codification_id
        JOIN    ftd_apps.florist_master fm      ON fm.florist_id = o.florist_id
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    ftd_apps.florist_codifications_stage n
                WHERE   n.florist_id = o.florist_id
                AND     n.codification_id = o.codification_id
                AND     NVL(o.holiday_price_flag, 'null') = NVL (n.holiday_price_flag, 'null')
                AND     NVL(o.min_price, -1) = NVL (n.min_price, -1)
        )
        AND     fm.status <> 'Inactive'
        AND     c.codify_all_florist_flag <> 'Y'
        UNION
        -- Include florists that will have new codifications
        SELECT  distinct 'Florist Codification additions' as message
        FROM    ftd_apps.florist_codifications_stage n
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    ftd_apps.florist_codifications o
                WHERE   n.florist_id = o.florist_id
                AND     n.codification_id = o.codification_id
        )
        AND     EXISTS
        (
                SELECT  1
                FROM    ftd_apps.florist_master f
                WHERE   f.florist_id = n.florist_id
        )
        UNION
        -- Include florists where open/close data is changing
        SELECT  distinct 'Florist Hours updates' as message
        FROM    ftd_apps.florist_hours_stage fhs
        JOIN    ftd_apps.florist_hours fh
        ON      fh.florist_id = fhs.florist_id
        AND     fh.day_of_week = fhs.day_of_week
        AND     nvl(fh.open_close_code, 'null') <> nvl(fhs.open_close_code, 'null')
        UNION
        -- Include florists where open/close data is being added
        SELECT  distinct 'Florist Hours additions' as message
        FROM    ftd_apps.florist_hours_stage fhs
        WHERE   not exists (
                select 1
                FROM ftd_apps.florist_hours fh
                WHERE fh.florist_id = fhs.florist_id
                AND fh.day_of_week = fhs.day_of_week
        )
        UNION
        SELECT  distinct 'Florist Master additions' as message
        FROM    ftd_apps.florist_master_stage n
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    ftd_apps.florist_master o
                WHERE   n.florist_id = o.florist_id
        )
        UNION
        SELECT  distinct 'Florist Master removals' as message
        FROM    ftd_apps.florist_master o
        WHERE   NOT EXISTS
        (
                SELECT  1
                FROM    ftd_apps.florist_master_stage n
                WHERE   n.florist_id = o.florist_id
        )
        AND     o.status <> 'Inactive'
        AND     o.vendor_flag = 'N'
        AND     substr (florist_id, 1, 2) not in ('90', '91');
        
BEGIN

    v_message := null;
    for rec in verify_cur loop
        v_message := v_message || rec.message || crlf;
    end loop;
    
    if v_message is not null then
        -- insert a system message
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST DATA LOAD',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'The Member Load staging tables do not match the production tables ' ||
                    'for the following areas, please investigate: ' || crlf || crlf || v_message,
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );
    end if;

    out_status := 'Y';
    commit;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VERIFY_MEMBER_LOAD [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VERIFY_MEMBER_LOAD;

PROCEDURE UPDATE_CHILD_BLOCKS_SUSPENDS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

cursor get_parent_blocks is
    select fms.florist_id,
        fms.top_level_florist_id,
        fm.status,
        fb.block_type,
        fb.block_start_date,
        fb.block_end_date,
        fb.blocked_by_user_id,
        fb.block_reason
    from ftd_apps.florist_master_stage fms
    join ftd_apps.florist_blocks fb
    on fb.florist_id = fms.top_level_florist_id
    join ftd_apps.florist_master fm
    on fm.florist_id = fms.top_level_florist_id
    where not exists (select 'Y'
        from ftd_apps.florist_blocks fb1
        where fb1.florist_id = fms.florist_id
        and fb1.block_start_date = fb.block_start_date
        and nvl(fb1.block_end_date, trunc(sysdate-999)) = nvl(fb.block_end_date, trunc(sysdate-999)))
    order by 2,1;

cursor get_child_blocks is
    select fms.florist_id,
        fms.top_level_florist_id,
        fm.status,
        fb.block_type,
        fb.block_start_date,
        fb.block_end_date,
        fb.blocked_by_user_id,
        fb.block_reason
    from ftd_apps.florist_master_stage fms
    join ftd_apps.florist_blocks fb
    on fb.florist_id = fms.florist_id
    join ftd_apps.florist_master fm
    on fm.florist_id = fms.top_level_florist_id
    where not exists (select 'Y'
        from ftd_apps.florist_blocks fb1
        where fb1.florist_id = fms.top_level_florist_id
        and fb1.block_start_date = fb.block_start_date
        and nvl(fb1.block_end_date, trunc(sysdate-999)) = nvl(fb.block_end_date, trunc(sysdate-999)))
    order by 1;

cursor get_suspends is
    select fms.florist_id,
        fms.top_level_florist_id,
        fm.status,
        fs.suspend_type,
        fs.suspend_start_date,
        fs.suspend_end_date,
        fs.timezone_offset_in_minutes,
        fs.goto_florist_suspend_received,
        fs.goto_florist_suspend_merc_id,
        fs.goto_florist_resume_received,
        fs.goto_florist_resume_merc_id,
        fs.suspend_processed_flag,
        fs.block_suspend_override_flag,
        fsc.florist_id existing
    from ftd_apps.florist_master_stage fms
    join ftd_apps.florist_master fm
    on fm.florist_id = fms.top_level_florist_id
    left outer join ftd_apps.florist_suspends fs
    on fs.florist_id = fms.top_level_florist_id
    left outer join ftd_apps.florist_suspends fsc
    on fsc.florist_id = fms.florist_id
    where nvl(fs.suspend_type, 'X') <> nvl(fsc.suspend_type, 'X')
    or fs.suspend_start_date <> fsc.suspend_start_date
    or fs.suspend_end_date <> fsc.suspend_end_date
    order by 2,1;

cursor get_codification_blocks is
    select fm1.florist_id child_florist_id,
        fc1.codification_id,
        fc2.florist_id parent_florist_id,
        fc2.block_start_date parent_block_start_date,
	fc2.block_end_date parent_block_end_date
    from ftd_apps.florist_master fm1
    join ftd_apps.florist_master fm2
    on fm2.florist_id = fm1.top_level_florist_id
    join ftd_apps.florist_codifications fc1
    on fc1.florist_id = fm1.florist_id
    join ftd_apps.florist_codifications fc2
    on fc2.florist_id = fm2.florist_id
    and fc2.codification_id = fc1.codification_id
    where fm1.status <> 'Inactive'
    and (nvl(fc1.block_start_date, trunc(sysdate+99)) <> nvl(fc2.block_start_date, trunc(sysdate+99))
        or nvl(fc1.block_end_date, trunc(sysdate-99)) <> nvl(fc2.block_end_date, trunc(sysdate-99)))
    order by 1, 2;

v_status                  varchar2(1) := 'Y';
v_message                 varchar2(1000);
v_action_header_id        number;
    
begin

   dbms_application_info.set_client_info('UPDATE_CHILD_BLOCKS_SUSPENDS Start');
    
   for rec in get_child_blocks loop

       dbms_application_info.set_client_info('Child Block: ' || rec.florist_id);

       delete from ftd_apps.florist_blocks
       where florist_id = rec.florist_id
       and block_start_date = rec.block_start_date
       and nvl(block_end_date, trunc(sysdate-999)) = nvl(rec.block_end_date, trunc(sysdate-999));

       update ftd_apps.florist_master
       set status = rec.status
       where florist_id = rec.florist_id;

   end loop;

   for rec in get_parent_blocks loop

       dbms_application_info.set_client_info('Parent Block: ' || rec.florist_id);

       insert into ftd_apps.florist_blocks (
	   FLORIST_ID,
	   BLOCK_TYPE,
	   BLOCK_START_DATE,
	   BLOCK_END_DATE,
	   BLOCKED_BY_USER_ID,
           BLOCK_REASON
       ) values (
	   rec.florist_id,
	   rec.block_type,
	   rec.block_start_date,
	   rec.block_end_date,
	   rec.blocked_by_user_id,
	   rec.block_reason
       );

       update ftd_apps.florist_master
       set status = rec.status
       where florist_id = rec.florist_id;

   end loop;
    
   for rec in get_suspends loop

       dbms_application_info.set_client_info('Suspend: ' || rec.florist_id || ' Existing: ' || rec.existing);

       if rec.existing is null then

           insert into ftd_apps.florist_suspends (
               FLORIST_ID,
               SUSPEND_TYPE,
               SUSPEND_START_DATE,
               SUSPEND_END_DATE,
               TIMEZONE_OFFSET_IN_MINUTES,
               GOTO_FLORIST_SUSPEND_RECEIVED,
               GOTO_FLORIST_SUSPEND_MERC_ID,
               GOTO_FLORIST_RESUME_RECEIVED,
               GOTO_FLORIST_RESUME_MERC_ID,
               SUSPEND_PROCESSED_FLAG,
               BLOCK_SUSPEND_OVERRIDE_FLAG
           ) values (
               rec.florist_id,
               rec.suspend_type,
               rec.suspend_start_date,
               rec.suspend_end_date,
               rec.timezone_offset_in_minutes,
               rec.goto_florist_suspend_received,
               rec.goto_florist_suspend_merc_id,
               rec.goto_florist_resume_received,
               rec.goto_florist_resume_merc_id,
               'N',
               rec.block_suspend_override_flag
           );

       else

	       update ftd_apps.florist_suspends
	       set suspend_type = rec.suspend_type,
	       suspend_start_date = rec.suspend_start_date,
	       suspend_end_date = rec.suspend_end_date,
	       timezone_offset_in_minutes = rec.timezone_offset_in_minutes,
	       goto_florist_suspend_received = rec.goto_florist_suspend_received,
	       goto_florist_suspend_merc_id = rec.goto_florist_suspend_merc_id,
	       goto_florist_resume_received = rec.goto_florist_resume_received,
	       goto_florist_resume_merc_id = rec.goto_florist_resume_merc_id,
	       suspend_processed_flag = 'N',
	       block_suspend_override_flag = rec.block_suspend_override_flag
           where florist_id = rec.florist_id;

       end if;

       update ftd_apps.florist_master
       set status = rec.status
       where florist_id = rec.florist_id;

   end loop;
    
   for rec in get_codification_blocks loop

       dbms_application_info.set_client_info('Codification: ' || rec.child_florist_id || '|' || rec.codification_id);

       if rec.parent_block_start_date is null then

           update ftd_apps.florist_codifications
           set block_start_date = null,
               block_end_date = null
           where florist_id = rec.child_florist_id
           and codification_id = rec.codification_id;

       else

           update ftd_apps.florist_codifications
           set block_start_date = rec.parent_block_start_date,
               block_end_date = rec.parent_block_end_date
           where florist_id = rec.child_florist_id
           and codification_id = rec.codification_id;

       end if;

   end loop;
    
   dbms_application_info.set_client_info('UPDATE_CHILD_BLOCKS_SUSPENDS End');
    
   OUT_STATUS := 'Y';

   EXCEPTION WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CHILD_BLOCKS_SUSPENDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CHILD_BLOCKS_SUSPENDS;

END FLORIST_LOAD_AUDIT_PKG;
.
/
