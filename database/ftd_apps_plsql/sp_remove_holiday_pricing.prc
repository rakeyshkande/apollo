CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_HOLIDAY_PRICING (
 productID in varchar2 )

as
--==============================================================================
--
-- Name:    SP_REMOVE_HOLIDAY_PRICING
-- Type:    Procedure
-- Syntax:  SP_REMOVE_HOLIDAY_PRICING ( productId in varchar2 )
--
-- Description:   Updates PRODUCT_MASTER by PRODUCT_ID to set the holiday
--                pricing fields NULL.
--
--==============================================================================
begin
 update PRODUCT_MASTER
 set PRODUCT_MASTER.HOLIDAY_PRICE = NULL,
     PRODUCT_MASTER.HOLIDAY_SKU = NULL,
     PRODUCT_MASTER.HOLIDAY_START_DATE = NULL,
     PRODUCT_MASTER.HOLIDAY_END_DATE = NULL,
     PRODUCT_MASTER.HOLIDAY_DELUXE_PRICE = NULL,
     PRODUCT_MASTER.HOLIDAY_PREMIUM_PRICE = NULL
 where PRODUCT_MASTER.PRODUCT_ID = productID;
end
;
.
/
