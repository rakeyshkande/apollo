CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_DELIVERY_DATE_RANGES RETURN types.ref_cursor
--=============================================================
--
-- Name:    OE_GET_DELIVERY_DATE_RANGES
-- Type:    Function
-- Syntax:  OE_GET_DELIVERY_DATE_RANGES
-- Returns: ref_cursor for
--          START_DATE        VARCHAR2(10)
--          END_DATE          VARCHAR2(10)
--          INSTRUCTION_TEXT  VARCHAR2(100)
--
-- Description:   Queries the DELIVERY_DATE_RANGE table and
--                returns all active rows.
--
--=============================================================

AS
    cur_cursor   types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT TO_CHAR(START_DATE, 'mm/dd/yyyy') as "startDate",
              TO_CHAR(END_DATE, 'mm/dd/yyyy') as "endDate",
              INSTRUCTION_TEXT as "instructionText"
        FROM DELIVERY_DATE_RANGE
        WHERE ACTIVE_FLAG = 'Y'
		  AND END_DATE > SYSDATE
        ORDER BY START_DATE;

    RETURN cur_cursor;
END;
.
/
