CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_DELETE_CSZ_ZIP_CODE (
  zip in varchar2
)
as
--==============================================================================
--
-- Name:    OEL_DELETE_CSZ_ZIP_CODE
-- Type:    Procedure
-- Syntax:  OEL_DELETE_CSZ_ZIP_CODE ( zip in varchar2 )
--
-- Description:   Deletes a row from CSZ_AVAIL by CSZ_ZIP_CODE.
--
--==============================================================================
begin

 delete FROM CSZ_AVAIL
  where CSZ_ZIP_CODE = zip;

  COMMIT;

end
;
.
/
