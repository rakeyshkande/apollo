CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_DELETE_FLORIST_PRODUCT (
  floristId in varchar2,
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    OEL_DELETE_FLORIST_PRODUCT
-- Type:    Procedure
-- Syntax:  OEL_DELETE_FLORIST_PRODUCT ( floristId in varchar2,
--                                     productId in varchar2 )
--
-- Description:   Deletes a row from FLORIST_PRODUCTS by FLORIST_ID and PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM FLORIST_PRODUCTS
  where FLORIST_ID = floristId AND PRODUCT_ID = UPPER(productId);

  COMMIT;

end
;
.
/
