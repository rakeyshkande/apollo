CREATE OR REPLACE TRIGGER ftd_apps.upsell_detail_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.upsell_detail 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
   v_payload varchar2(2000);
   v_count   number;
   v_stat    varchar2(1) := 'Y';
   v_mess    varchar2(2000);
   v_trigger_enabled varchar2(10) := 'N';
   v_exception     EXCEPTION;
   v_exception_txt varchar2(200);
   v_novator_id varchar2(10);
   
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into ftd_apps.upsell_detail$
         (UPSELL_DETAIL_ID,
          UPSELL_MASTER_ID,
          UPSELL_DETAIL_SEQUENCE,
          UPSELL_DETAIL_NAME,
          GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          GBB_NAME_OVERRIDE_FLAG,
          GBB_NAME_OVERRIDE_TXT,
          GBB_PRICE_OVERRIDE_FLAG,
          GBB_PRICE_OVERRIDE_TXT,
          DEFAULT_SKU_FLAG,
          OPERATION$,
          TIMESTAMP$ 
         )
      values
         (:NEW.UPSELL_DETAIL_ID,
          :NEW.UPSELL_MASTER_ID,
          :NEW.UPSELL_DETAIL_SEQUENCE,
          :NEW.UPSELL_DETAIL_NAME,
          :NEW.GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          :NEW.GBB_NAME_OVERRIDE_FLAG,
          :NEW.GBB_NAME_OVERRIDE_TXT,
          :NEW.GBB_PRICE_OVERRIDE_FLAG,
          :NEW.GBB_PRICE_OVERRIDE_TXT,
          :NEW.DEFAULT_SKU_FLAG,
          'INS',
          v_current_timestamp          
         );
   
   ELSIF UPDATING  THEN

      insert into ftd_apps.upsell_detail$
         (UPSELL_DETAIL_ID,
          UPSELL_MASTER_ID,
          UPSELL_DETAIL_SEQUENCE,
          UPSELL_DETAIL_NAME,
          GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          GBB_NAME_OVERRIDE_FLAG,
          GBB_NAME_OVERRIDE_TXT,
          GBB_PRICE_OVERRIDE_FLAG,
          GBB_PRICE_OVERRIDE_TXT,
          DEFAULT_SKU_FLAG,
          OPERATION$,
          TIMESTAMP$ 
         )
      values
         (:OLD.UPSELL_DETAIL_ID,
          :OLD.UPSELL_MASTER_ID,
          :OLD.UPSELL_DETAIL_SEQUENCE,
          :OLD.UPSELL_DETAIL_NAME,
          :OLD.GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          :OLD.GBB_NAME_OVERRIDE_FLAG,
          :OLD.GBB_NAME_OVERRIDE_TXT,
          :OLD.GBB_PRICE_OVERRIDE_FLAG,
          :OLD.GBB_PRICE_OVERRIDE_TXT,
          :OLD.DEFAULT_SKU_FLAG,
          'UPD_OLD',
          v_current_timestamp          
         );


      insert into ftd_apps.upsell_detail$
         (UPSELL_DETAIL_ID,
          UPSELL_MASTER_ID,
          UPSELL_DETAIL_SEQUENCE,
          UPSELL_DETAIL_NAME,
          GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          GBB_NAME_OVERRIDE_FLAG,
          GBB_NAME_OVERRIDE_TXT,
          GBB_PRICE_OVERRIDE_FLAG,
          GBB_PRICE_OVERRIDE_TXT,
          DEFAULT_SKU_FLAG,
          OPERATION$,
          TIMESTAMP$ 
         )
      values
         (:NEW.UPSELL_DETAIL_ID,
          :NEW.UPSELL_MASTER_ID,
          :NEW.UPSELL_DETAIL_SEQUENCE,
          :NEW.UPSELL_DETAIL_NAME,
          :NEW.GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          :NEW.GBB_NAME_OVERRIDE_FLAG,
          :NEW.GBB_NAME_OVERRIDE_TXT,
          :NEW.GBB_PRICE_OVERRIDE_FLAG,
          :NEW.GBB_PRICE_OVERRIDE_TXT,
          :NEW.DEFAULT_SKU_FLAG,
          'UPD_NEW',
          v_current_timestamp          
         );


   ELSIF DELETING  THEN

      insert into ftd_apps.upsell_detail$
         (UPSELL_DETAIL_ID,
          UPSELL_MASTER_ID,
          UPSELL_DETAIL_SEQUENCE,
          UPSELL_DETAIL_NAME,
          GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          GBB_NAME_OVERRIDE_FLAG,
          GBB_NAME_OVERRIDE_TXT,
          GBB_PRICE_OVERRIDE_FLAG,
          GBB_PRICE_OVERRIDE_TXT,
          DEFAULT_SKU_FLAG,
          OPERATION$,
          TIMESTAMP$ 
         )
      values
         (:OLD.UPSELL_DETAIL_ID,  
          :OLD.UPSELL_MASTER_ID,
          :OLD.UPSELL_DETAIL_SEQUENCE,
          :OLD.UPSELL_DETAIL_NAME,
          :OLD.GBB_UPSELL_DETAIL_SEQUENCE_NUM,
          :OLD.GBB_NAME_OVERRIDE_FLAG,
          :OLD.GBB_NAME_OVERRIDE_TXT,
          :OLD.GBB_PRICE_OVERRIDE_FLAG,
          :OLD.GBB_PRICE_OVERRIDE_TXT,
          :OLD.DEFAULT_SKU_FLAG,
          'DEL',
          v_current_timestamp          
         );

   END IF;

   -- For Project Fresh, post messages for upsell detail and master with delay.
   -- Note this trigger will be called multiple times anytime an upsell is changed since all upsell_detail 
   -- records are removed and then re-added.
   --
   SELECT value INTO v_trigger_enabled FROM frp.global_parms WHERE context='SERVICE' AND name='FRESH_TRIGGERS_ENABLED';
   IF (v_trigger_enabled = 'Y') THEN 
      SELECT NVL(pm.novator_id, pm.product_id) 
             INTO  v_novator_id 
             FROM  ftd_apps.product_master pm 
             WHERE pm.product_id = NVL(:NEW.UPSELL_DETAIL_ID, :OLD.UPSELL_DETAIL_ID);
      v_payload := 'FRESH_MSG_PRODUCER|product|upsell_detail|' || v_novator_id;
      events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 15, v_stat, v_mess);      
      v_payload := 'FRESH_MSG_PRODUCER|product|upsell_master|' || NVL(:NEW.UPSELL_MASTER_ID, :OLD.UPSELL_MASTER_ID);
      events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 17, v_stat, v_mess);      
      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message in PDB (upsell_detail trigger) for Fresh product = ' || NVL(:NEW.UPSELL_DETAIL_ID, :OLD.UPSELL_DETAIL_ID) || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;
   END IF;

END;
/
