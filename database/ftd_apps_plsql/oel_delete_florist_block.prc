CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_DELETE_FLORIST_BLOCK (
  floristId in varchar2
)
as
--==============================================================================
--
-- Name:    OEL_DELETE_FLORIST_BLOCK
-- Type:    Procedure
-- Syntax:  OEL_DELETE_FLORIST_BLOCK ( floristId in varchar2,
--                                     blockType in varchar2 )
--
-- Description:   Deletes a row from FLORIST_BLOCKS by FLORIST_ID and BLOCK_TYPE.
--
--==============================================================================
   v_system_message_id number;
   v_out_status varchar2(1);
   v_message varchar2(500);

begin

   frp.misc_pkg.insert_system_messages(
      in_source             => 'oel_delete_florist_block',
      in_type               => 'INFO',
      in_message            => 'OE XML Listener is active',
      in_computer           => sys_context('USERENV','HOST'),
      out_system_message_id => v_system_message_id,
      out_status            => v_out_status,
      out_message           => v_message);

 delete FROM FLORIST_BLOCKS
  where FLORIST_ID = floristId;

  COMMIT;

end
;
.
/
