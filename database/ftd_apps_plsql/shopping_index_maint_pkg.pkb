create or replace package body FTD_APPS.SHOPPING_INDEX_MAINT_PKG as

PROCEDURE INSERT_SI_TPL_STAGE
(
IN_SI_TEMPLATE_TYPE_CODE	IN SI_TEMPLATE_TYPE.SI_TEMPLATE_TYPE_CODE%TYPE,
IN_SI_TEMPLATE_KEY		IN SI_TEMPLATE_STAGE.SI_TEMPLATE_KEY%TYPE,
IN_SI_TEMPLATE_ID		IN SI_TEMPLATE_STAGE.SI_TEMPLATE_ID%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a row in
        ftd_apps.SI_TEMPLATE_STAGE table.

Input:
	
        in_si_template_type_code         	varchar2
        in_si_template_key		        varchar2


Output:
        out_si_template_id			number

-----------------------------------------------------------------------------*/
v_si_template_id number;

BEGIN

    OUT_STATUS := 'Y';  
	
    INSERT INTO SI_TEMPLATE_STAGE
    (
	si_template_id,
	si_template_type_code,
	si_template_key,
	created_on,
	created_by
    )
    VALUES
    (
	IN_SI_TEMPLATE_ID,
	IN_SI_TEMPLATE_TYPE_CODE,
	IN_SI_TEMPLATE_KEY,
	sysdate,
	'SYS'
    );

    EXCEPTION WHEN OTHERS THEN
	    OUT_STATUS := 'N';
	OUT_MESSAGE := 'ERROR OCCURRED in INSERT_SI_TPL_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_SI_TPL_STAGE;

PROCEDURE INS_SI_TPL_STAGE
(
IN_SI_TEMPLATE_TYPE_CODE	IN SI_TEMPLATE_TYPE.SI_TEMPLATE_TYPE_DESC%TYPE,
IN_SI_TEMPLATE_KEY		IN SI_TEMPLATE_STAGE.SI_TEMPLATE_KEY%TYPE,
OUT_SI_TEMPLATE_ID		OUT SI_TEMPLATE_STAGE.SI_TEMPLATE_ID%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
) 
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns the si_template_id if found. Otherwise create new.
        Throws no_data_found exception if no valid type code match.
Input:
        in_source_code          varchar2
        in_index_name		varchar2

Output:
        si_template_id

-----------------------------------------------------------------------------*/
v_si_template_type_code SI_TEMPLATE_TYPE.SI_TEMPLATE_TYPE_CODE%TYPE;
v_status varchar2(1);
v_message varchar2(256);
v_exception EXCEPTION;
v_exception_msg varchar2(256);

BEGIN
    OUT_STATUS := 'Y';
    OUT_SI_TEMPLATE_ID := shopping_index_query_pkg.SI_TEMPLATE_ID_EXISTS(in_si_template_type_code, in_si_template_key);
    
    dbms_output.put_line('out_si_template_id:' || out_si_template_id);
    
    if OUT_SI_TEMPLATE_ID < 0 then
    	select si_template_type_code
	into v_si_template_type_code
	from si_template_type 
	where si_template_type_desc = in_si_template_type_code
        ;
        
        select si_template_id_sq.nextval
	into OUT_SI_TEMPLATE_ID
        from dual; 
        
    else
        -- template previously exists. Clear stage tables.
        DELETE_SHOPPING_INDEX_STAGE(OUT_SI_TEMPLATE_ID, v_status, v_message);
        if v_status = 'N' then
	   v_exception_msg := v_message;
	   raise v_exception;
        end if;
        
    end if;
    
    INSERT_SI_TPL_STAGE(v_si_template_type_code, in_si_template_key, OUT_SI_TEMPLATE_ID, v_status, v_message);
        
    if v_status = 'N' then
        v_exception_msg := v_message;
        raise v_exception;
    end if;    
	
    EXCEPTION WHEN v_exception THEN
    	out_status := 'N';
    	out_message := v_exception_msg;
    WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED in INS_SI_TPL_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,200);
	
END INS_SI_TPL_STAGE;

PROCEDURE INS_SI_TPL_INDEX_STAGE
(
IN_SI_TEMPLATE_ID		IN SI_TEMPLATE_INDEX_STAGE.SI_TEMPLATE_ID%TYPE,
IN_INDEX_NAME			IN SI_TEMPLATE_INDEX_STAGE.INDEX_NAME%TYPE,
IN_INDEX_DESC			IN SI_TEMPLATE_INDEX_STAGE.INDEX_DESC%TYPE,
IN_COUNTRY_ID			IN SI_TEMPLATE_INDEX_STAGE.COUNTRY_ID%TYPE,
IN_DISPLAY_SEQ_NUM		IN SI_TEMPLATE_INDEX_STAGE.DISPLAY_SEQ_NUM%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a row in
        ftd_apps.SI_TEMPLATE_INDEX_STAGE table.
Input:
	
        in_index_name         	varchar2
        in_si_template_id       number
        in_index_desc           varchar2
        in_country_id		varchar2
        in_display_seq_num	number

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
BEGIN

    INSERT INTO si_template_index_stage
    (
    	si_template_id,
    	index_name,
    	index_desc,
    	country_id,
    	display_seq_num,
    	created_on,
    	created_by
    )
    VALUES
    (
    	in_si_template_id,
    	in_index_name,
    	in_index_desc,
    	in_country_id,
    	in_display_seq_num,
    	sysdate,
    	'SYS'
    );
    
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
            OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in INS_SI_TPL_INDEX_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INS_SI_TPL_INDEX_STAGE;

PROCEDURE INS_SI_TPL_INDEX_PROD_STAGE
(
IN_SI_TEMPLATE_ID		IN SI_TEMPLATE_INDEX_PROD_STAGE.SI_TEMPLATE_ID%TYPE,
IN_INDEX_NAME			IN SI_TEMPLATE_INDEX_PROD_STAGE.INDEX_NAME%TYPE,
IN_PRODUCT_ID			IN SI_TEMPLATE_INDEX_PROD_STAGE.PRODUCT_ID%TYPE,
IN_DISPLAY_SEQ_NUM		IN SI_TEMPLATE_INDEX_PROD_STAGE.DISPLAY_SEQ_NUM%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a row in
        ftd_apps.SI_TEMPLATE_INDEX_PROD_STAGE table.
Input:
	in_si_template_id	number
        in_index_name         	varchar2
        in_prouduct_id           varchar2
        in_display_seq_num	number

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
BEGIN

    INSERT INTO si_template_index_prod_stage
    (
    	si_template_id,
    	index_name,
    	product_id,
    	display_seq_num,
    	created_on,
    	created_by
    )
    VALUES
    (
    	in_si_template_id,
    	in_index_name,
    	in_product_id,
    	in_display_seq_num,
    	sysdate,
    	'SYS'
    );
    
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
            OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in INS_SI_TPL_INDEX_PROD_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INS_SI_TPL_INDEX_PROD_STAGE;


PROCEDURE CONVERT_NOVATOR_ID
(
IN_SI_TEMPLATE_ID		IN SI_TEMPLATE_INDEX_PROD_STAGE.SI_TEMPLATE_ID%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the novator id to product id for
        ftd_apps.shopping_index_prod_stage table.
Input:
        IN_SI_TEMPLATE_ID          NUMBER

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
BEGIN

    delete from SI_TEMPLATE_INDEX_PROD_STAGE
    where product_id in
        (select product_id from SI_TEMPLATE_INDEX_PROD_STAGE
        minus
        (select s.product_id from SI_TEMPLATE_INDEX_PROD_STAGE s
        join ftd_apps.product_master pm
        on pm.novator_id=s.product_id
        union
        select s.product_id from SI_TEMPLATE_INDEX_PROD_STAGE s
        join ftd_apps.upsell_master um
        on um.upsell_master_id=s.product_id
        ))
    and si_template_id = in_si_template_id;

    UPDATE SI_TEMPLATE_INDEX_PROD_STAGE ss
       SET product_id = (select product_id from ftd_apps.product_master pm
                          where pm.novator_id = ss.product_id
                         union 
                         select upsell_master_id as product_id from ftd_apps.upsell_master um
                          where um.upsell_master_id = ss.product_id)
       WHERE si_template_id = IN_SI_TEMPLATE_ID;
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
            OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in CONVERT_NOVATOR_ID [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CONVERT_NOVATOR_ID;

PROCEDURE UPDATE_SHOPPING_INDEX_TRK
(
IN_FILE_NAME		IN SI_FILE_NAME_TRACKER.FILE_NAME%TYPE,
IN_UPDATED_BY		IN SI_FILE_NAME_TRACKER.UPDATED_BY%TYPE,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating source code last shopping index date.
Input:
        in_file_name          varchar2

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/

    v_date DATE := sysdate;
BEGIN
    OUT_STATUS := 'Y';
    
    INSERT INTO	
    SI_FILE_NAME_TRACKER
    (file_name,
     last_index_date,
     created_on,
     created_by,
     updated_on,
     updated_by
    ) values
    (
     in_file_name,
     v_date,
     v_date,
     in_updated_by,
     v_date,
     in_updated_by
    );
    
    EXCEPTION 
    WHEN DUP_VAL_ON_INDEX THEN
        UPDATE SI_FILE_NAME_TRACKER
        SET last_index_date = v_date,
            updated_on = v_date,
            updated_by = in_updated_by
        WHERE file_name = in_file_name;
    
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in UPDATE_SHOPPING_INDEX_TRK [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_SHOPPING_INDEX_TRK;

PROCEDURE DELETE_SHOPPING_INDEX
(
IN_HOUR_THRESHOLD	IN NUMBER,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing from the shopping_index_ 
        table rows that have not been updated for longer than the threshold hours.
Input:
        IN_HOUR_THRESHOLD          number

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/

v_date DATE;
    
CURSOR v_template_cur (in_date date) IS
    SELECT si_template_id
    FROM si_template tp, si_file_name_tracker ft
    WHERE tp.si_template_key = ft.file_name
    AND ft.last_index_date < in_date;
    
CURSOR v_ref_cur (in_date date) IS
    SELECT file_name
    FROM si_file_name_tracker ft
    WHERE ft.last_index_date < in_date;    

BEGIN
    v_date := sysdate - IN_HOUR_THRESHOLD/24;
    OUT_STATUS := 'Y';
    
    -- delete the template and their references that have outdated.
    FOR i in v_template_cur (v_date) LOOP

    	DELETE FROM si_template_index_prod
    	WHERE SI_TEMPLATE_ID = i.si_template_id;
    	
        DELETE FROM SI_TEMPLATE_INDEX_SUB
    	WHERE SI_TEMPLATE_ID = i.si_template_id;      	

        DELETE FROM SI_TEMPLATE_INDEX
    	WHERE SI_TEMPLATE_ID = i.si_template_id;
    	
        DELETE FROM SI_SOURCE_TEMPLATE_REF
    	WHERE SI_TEMPLATE_ID = i.si_template_id;    	
    	
    	DELETE FROM SI_TEMPLATE
    	WHERE SI_TEMPLATE_ID = i.si_template_id;    
    	
    END LOOP;
    
    -- delete the references that are outdated but the templates are still valid.
    FOR j in v_ref_cur (v_date) LOOP
    	DELETE FROM SI_SOURCE_TEMPLATE_REF
    	WHERE source_code = j.file_name;  
    END LOOP;
    
    	
    
    EXCEPTION WHEN OTHERS THEN
         OUT_STATUS := 'N';
         OUT_MESSAGE := 'ERROR OCCURRED in DELETE_SHOPPING_INDEX [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DELETE_SHOPPING_INDEX;

PROCEDURE MOVE_SHOPPING_INDEX_STAGE
(
IN_SI_TEMPLATE_ID	IN SI_TEMPLATE_STAGE.SI_TEMPLATE_ID%TYPE,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for moving the data from stage table to production tables.
Input:
        IN_SI_TEMPLATE_ID          NUMBER

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/


BEGIN

    
	-- INSERT THE STAGE RECORD THAT DO NOT EXIST IN SI_TEMPLATE
	INSERT INTO SI_TEMPLATE
	(SI_TEMPLATE_ID,
	 SI_TEMPLATE_TYPE_CODE,
	 SI_TEMPLATE_KEY,
	 CREATED_ON,
	 CREATED_BY,
	 UPDATED_ON,
	 UPDATED_BY
	)
	SELECT s1.si_template_id,s1.si_template_type_code,s1.si_template_key,s1.created_on,s1.created_by,s1.created_on,s1.created_by 
	FROM SI_TEMPLATE_STAGE s1
	WHERE si_template_id = in_si_template_id
	and not exists (
	select 'Y' from SI_TEMPLATE s2
	where s2.si_template_id = s1.si_template_id);


	-- update si_template does not apply

	-- INSERT THE STAGE RECORD THAT DO NOT EXIST IN SI_TEMPLATE_INDEX
	INSERT INTO SI_TEMPLATE_INDEX
	(
	 SI_TEMPLATE_ID,
	 INDEX_NAME,
	 INDEX_DESC,
	 COUNTRY_ID,
	 DISPLAY_SEQ_NUM,
	 created_on,
	 created_by,
	 updated_on,
	 updated_by
	)
	SELECT s1.si_template_id,s1.index_name,s1.index_desc, s1.country_id, s1.display_seq_num, s1.created_on,s1.created_by,s1.created_on, s1.created_by 
	FROM SI_TEMPLATE_INDEX_STAGE s1
	WHERE s1.si_template_id = in_si_template_id
	and not exists (
	select 'Y' from SI_TEMPLATE_INDEX s2
	where s2.si_template_id = s1.si_template_id
	and s2.index_name = s1.index_name)
	;
	
	-- INSERT THE STAGE RECORD THAT DO NOT EXIST IN SI_TEMPLATE_INDEX_SUB
	INSERT INTO SI_TEMPLATE_INDEX_SUB
	(
	 SI_TEMPLATE_ID,
	 INDEX_NAME,
	 SUB_INDEX_NAME,
	 DISPLAY_SEQ_NUM,
	 created_on,
	 created_by,
	 updated_on,
	 updated_by
	)
	SELECT s1.si_template_id,s1.index_name,s1.sub_index_name, s1.display_seq_num, s1.created_on,s1.created_by,s1.created_on, s1.created_by 
	FROM SI_TEMPLATE_INDEX_SUB_STAGE s1
	WHERE s1.si_template_id = in_si_template_id
	and not exists (
	select 'Y' from SI_TEMPLATE_INDEX_SUB s2
	where s2.si_template_id = s1.si_template_id
	and s2.index_name = s1.index_name
	and s2.sub_index_name = s1.sub_index_name)
	;     
		
	-- INSERT THE STAGE RECORD THAT DO NOT EXIST IN SI_TEMPLATE_INDEX_PROD
	INSERT INTO SI_TEMPLATE_INDEX_PROD
	(
	 SI_TEMPLATE_ID,
	 INDEX_NAME,
	 PRODUCT_ID,
	 DISPLAY_SEQ_NUM,
	 created_on,
	 created_by,
	 updated_on,
	 updated_by
	)
	SELECT s1.si_template_id,s1.index_name,s1.product_id, s1.display_seq_num, s1.created_on,s1.created_by,s1.created_on, s1.created_by 
	FROM SI_TEMPLATE_INDEX_PROD_STAGE s1
	WHERE s1.si_template_id = in_si_template_id
	and not exists (
	select 'Y' from SI_TEMPLATE_INDEX_PROD s2
	where s2.si_template_id = s1.si_template_id
	and s2.index_name = s1.index_name
	and s2.product_id = s1.product_id)	
	;      		
	
        update (
        select s1.index_desc,
        s1.country_id,
        s1.display_seq_num,
        s1.updated_on,
        s1.updated_by,
        s2.index_desc index_desc_stage,
        s2.country_id country_id_stage,
        s2.display_seq_num display_seq_num_stage,
        s2.created_on created_on_stage,
        s2.created_by created_by_stage
        from SI_TEMPLATE_INDEX s1
        join SI_TEMPLATE_INDEX_STAGE s2
        on (s2.index_name = s1.index_name
        and s2.si_template_id = s1.si_template_id)
        where s2.si_template_id = in_si_template_id
        and (s2.index_desc <> s1.index_desc
            or s2.country_id <> s1.country_id
            or s2.display_seq_num <> s1.display_seq_num)
        )
        set index_desc = index_desc_stage,
        country_id = country_id_stage,
        display_seq_num = display_seq_num_stage,
        updated_on = created_on_stage,
        updated_by = created_by_stage;	

        update (
        select s1.display_seq_num,
        s1.updated_on,
        s1.updated_by,
        s2.display_seq_num display_seq_num_stage,
        s2.created_on created_on_stage,
        s2.created_by created_by_stage
        from SI_TEMPLATE_INDEX_SUB s1
        join SI_TEMPLATE_INDEX_SUB_STAGE s2
        on (s2.index_name = s1.index_name
        and s2.si_template_id = s1.si_template_id
        and s2.sub_index_name = s1.sub_index_name)
        where s2.si_template_id = in_si_template_id
        and (s2.display_seq_num <> s1.display_seq_num)
        )
        set display_seq_num = display_seq_num_stage,
        updated_on = created_on_stage,
        updated_by = created_by_stage;
        

        update (
        select s1.display_seq_num,
        s1.updated_on,
        s1.updated_by,
        s2.display_seq_num display_seq_num_stage,
        s2.created_on created_on_stage,
        s2.created_by created_by_stage
        from SI_TEMPLATE_INDEX_PROD s1
        join SI_TEMPLATE_INDEX_PROD_STAGE s2
        on (s2.index_name = s1.index_name
        and s2.si_template_id = s1.si_template_id
        and s2.product_id = s1.product_id)
        where s2.si_template_id = in_si_template_Id
        and (s2.display_seq_num <> s1.display_seq_num)
        )
        set display_seq_num = display_seq_num_stage,
        updated_on = created_on_stage,
        updated_by = created_by_stage;

	
	-- delete from si_template_index_prod
	delete SI_TEMPLATE_INDEX_PROD s1
	where s1.si_template_id = in_si_template_id
	and not exists (
	select 'Y' from si_template_index_prod_stage s2
	where s2.index_name = s1.index_name
	and s2.si_template_id = s1.si_template_id
	and s2.product_id = s1.product_id);                

	-- delete from si_template_index_sub
	delete SI_TEMPLATE_INDEX_SUB s1
	where s1.si_template_id = in_si_template_id
	and not exists (
	select 'Y' from si_template_index_sub_stage s2
	where s2.index_name = s1.index_name
	and s2.si_template_id = s1.si_template_id
	and s2.index_name = s1.index_name
	and s2.sub_index_name = s1.sub_index_name
	); 	
	
	-- delete from si_template_index
	delete SI_TEMPLATE_INDEX s1
	where s1.si_template_id = in_si_template_id
	and not exists (
	select 'Y' from si_template_index_stage s2
	where s2.index_name = s1.index_name
	and s2.si_template_id = s1.si_template_id);   

        DELETE_SHOPPING_INDEX_STAGE(in_si_template_id, out_status, out_message); 

        OUT_STATUS := 'Y';
    
        EXCEPTION WHEN OTHERS THEN
            OUT_STATUS := 'N';
            OUT_MESSAGE := 'ERROR OCCURRED in MOVE_SHOPPING_INDEX_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END MOVE_SHOPPING_INDEX_STAGE;

PROCEDURE INS_SI_SRC_TPL_REF_STG_SINGLE
(
IN_SOURCE_CODE			IN SI_SOURCE_TEMPLATE_REF_STAGE.SOURCE_CODE%TYPE,
IN_SI_TEMPLATE_ID		IN VARCHAR2,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a row in
        ftd_apps.SI_SOURCE_TEMPLATE_REF_STAGE table.

Input:
	
        in_source_code         	varchar2
        in_si_template_id	varchar2


Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
BEGIN
    INSERT INTO si_source_template_ref_stage
    (
        source_code,
    	si_template_id,
    	created_on,
    	created_by
    )
    VALUES
    (
        in_source_code,
    	to_number(IN_SI_TEMPLATE_ID),
    	sysdate,
    	'SYS'
    );
    OUT_STATUS := 'Y';
        
    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in INS_SI_TPL_INDEX_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,100);


END INS_SI_SRC_TPL_REF_STG_SINGLE;


PROCEDURE INS_SI_SOURCE_TPL_REF_STAGE
(
IN_SOURCE_CODE			IN SI_SOURCE_TEMPLATE_REF_STAGE.SOURCE_CODE%TYPE,
IN_SI_TEMPLATE_IDS		IN VARCHAR2,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a row in
        ftd_apps.SI_SOURCE_TEMPLATE_REF_STAGE table for each si_template_id.

Input:
	
        in_source_code         	varchar2
        in_si_template_id	varchar2


Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
v_si_template_id_str varchar2(20);
v_delim_pos number := 0;
v_remain_str varchar2(200);
v_domain varchar2(20);
v_domain_template_id SI_SOURCE_TEMPLATE_REF_STAGE.si_template_id%TYPE;
v_error_msg varchar(400);
v_exception EXCEPTION;

CURSOR v_ref_cur (v_template_id si_template.si_template_id%type) 
is
select r.si_template_id
from si_source_template_ref r
where r.source_code = (select t.si_template_key from si_template t
		       where t.si_template_id = v_template_id
		      )
and r.si_template_id <> v_template_id
minus 
select r.si_template_id
from si_source_template_ref_stage r
where r.source_code = in_source_code
;

BEGIN
    OUT_STATUS := 'Y';
    
    v_delim_pos := INSTR(IN_SI_TEMPLATE_IDS,',');
    
    IF v_delim_pos > 0 THEN
        v_si_template_id_str := substr(IN_SI_TEMPLATE_IDS, 1, v_delim_pos-1);
    	v_remain_str := substr(IN_SI_TEMPLATE_IDS, v_delim_pos +1);
    ELSE
    	v_si_template_id_str := IN_SI_TEMPLATE_IDS;
    	v_remain_str := null;
    	
    END IF;
    
    -- insert a reference for the template itself.
    INS_SI_SRC_TPL_REF_STG_SINGLE(in_source_code, v_si_template_id_str, out_status, out_message);
    
    
    if out_status is null or out_status = 'N' then
    	v_error_msg := 'Error in INS_SI_SRC_TPL_REF_STG_SINGLE with source_code ' || in_source_code || '; si_template_id ' || v_si_template_id_str || out_message;
    	raise v_exception;
    end if;

    -- insert a row for each template that the template may reference.
    for v_ref_row in v_ref_cur (v_si_template_id_str) loop
    	INS_SI_SRC_TPL_REF_STG_SINGLE(in_source_code, v_ref_row.si_template_id, out_status, out_message);
    	if out_status is null or out_status = 'N' then
	    v_error_msg := 'Error in INS_SI_SRC_TPL_REF_STG_SINGLE with source_code ' || in_source_code || '; v_ref_row.si_template_id ' || v_si_template_id_str || out_message;
	    raise v_exception;
	end if;
    
    end loop;
    
    
    IF v_remain_str IS NOT NULL THEN
    	INS_SI_SOURCE_TPL_REF_STAGE(in_source_code, v_remain_str, out_status, out_message);
    END IF;


    EXCEPTION WHEN v_exception THEN
         OUT_STATUS := 'N';
         OUT_MESSAGE := 'ERROR OCCURRED in INS_SI_SOURCE_TPL_REF_STAGE. Unable to insert data into SI_SOURCE_TEMPLATE_REF_STAGE:' || v_error_msg;
    
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in INS_SI_SOURCE_TPL_REF_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,100);
    

END INS_SI_SOURCE_TPL_REF_STAGE;

PROCEDURE UPD_SI_SOURCE_TPL_REF
(
IN_SOURCE_CODE			IN SI_SOURCE_TEMPLATE_REF_STAGE.SOURCE_CODE%TYPE,
IN_SI_TEMPLATE_IDS		IN VARCHAR2,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating source template reference.
        Insert the list in stage table and then update.

Input:
	
        in_source_code         	varchar2
        in_si_template_id	varchar2


Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
v_si_template_id SI_SOURCE_TEMPLATE_REF_STAGE.SI_TEMPLATE_ID%TYPE;
v_exception EXCEPTION;
v_exception_msg varchar2(1000);

BEGIN

    OUT_STATUS := 'Y';
    
    -- insert template ids in stage table.
    delete from SI_SOURCE_TEMPLATE_REF_STAGE where source_code = in_source_code;
    INS_SI_SOURCE_TPL_REF_STAGE(in_source_code, in_si_template_ids, out_status, out_message);
    
    if out_status = 'N' then
        v_exception_msg := out_message;
        raise v_exception;
    END IF;
	
    -- add the template that's in stage and not in prod table
    INSERT INTO SI_SOURCE_TEMPLATE_REF
    (
    	 SOURCE_CODE,
    	 SI_TEMPLATE_ID,
    	 created_on,
    	 created_by,
    	 updated_on,
    	 updated_by
    )
    SELECT s1.source_code, s1.si_template_id,s1.created_on,s1.created_by,s1.created_on, s1.created_by 
    FROM SI_SOURCE_TEMPLATE_REF_STAGE s1
    WHERE s1.source_code = in_source_code
    and not exists (
    	select 'Y' from SI_SOURCE_TEMPLATE_REF s2
    	where 1 = 1
    	and s2.source_code = s1.source_code
    	and s2.si_template_id = s1.si_template_id
	);
    
    -- remove the template that's in prod and not in stage.
    DELETE SI_SOURCE_TEMPLATE_REF s1
    WHERE 1 = 1
    AND s1.source_code = in_source_code
    AND NOT EXISTS (
	select 'Y' from si_source_template_ref_stage s2
	where s2.source_code = s1.source_code
	and s2.si_template_id = s1.si_template_id
	);     

    delete from SI_SOURCE_TEMPLATE_REF_STAGE where source_code = in_source_code;

    EXCEPTION WHEN v_exception THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in UPD_SI_SOURCE_TPL_REF. Unable to insert data into SI_SOURCE_TEMPLATE_REF_STAGE:' || SUBSTR(v_exception_msg,1,256);
    
    WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in UPD_SI_SOURCE_TPL_REF [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPD_SI_SOURCE_TPL_REF;

PROCEDURE BUILD_INDEX_SUB_STAGE
(
IN_SI_TEMPLATE_ID	IN SI_TEMPLATE_INDEX_SUB_STAGE.SI_TEMPLATE_ID%TYPE,
IN_INDEX_NAME		IN SI_TEMPLATE_INDEX_SUB_STAGE.INDEX_NAME%TYPE,
IN_SUB_INDEX_NAME 	IN SI_TEMPLATE_INDEX_SUB_STAGE.SUB_INDEX_NAME%TYPE,
IN_DISPLAY_SEQ_NUM      IN SI_TEMPLATE_INDEX_SUB_STAGE.DISPLAY_SEQ_NUM%TYPE,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting sub-index for
        ftd_apps.si_template_index_sub_stage table.
Input:
        in_index_name         	varchar2
        in_si_template_id       varchar2
        in_sub_index_name       varchar2

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
BEGIN

    INSERT INTO SI_TEMPLATE_INDEX_SUB_STAGE (
        SI_TEMPLATE_ID,
        INDEX_NAME,
        SUB_INDEX_NAME,
        DISPLAY_SEQ_NUM,
        CREATED_ON,
        CREATED_BY)
    VALUES (
        IN_SI_TEMPLATE_ID,
        IN_INDEX_NAME,
        IN_SUB_INDEX_NAME,
        IN_DISPLAY_SEQ_NUM,
        sysdate,
        'SYS');   
    
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
            OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in BUILD_INDEX_SUB_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END BUILD_INDEX_SUB_STAGE;

PROCEDURE DELETE_SHOPPING_INDEX_STAGE
(
IN_SI_TEMPLATE_ID	IN NUMBER,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting shopping index stage data for
        the given si_template_id. If the give si_template_id is null, then
        all data in stage table will be deleted.
Input:
        in_si_template_id       number

Output:
        out_status		varchar2
        out_message		varchar2

-----------------------------------------------------------------------------*/
BEGIN

    IF in_si_template_id IS NULL THEN
        DELETE FROM si_template_index_prod_stage;
        DELETE FROM si_template_index_sub_stage;
        DELETE FROM si_template_index_stage;
        DELETE FROM si_template_stage;
    ELSE
        DELETE FROM si_template_index_prod_stage
        WHERE si_template_id = in_si_template_id;
        
        DELETE FROM si_template_index_sub_stage
        WHERE si_template_id = in_si_template_id;  
        
        DELETE FROM si_template_index_stage
        WHERE si_template_id = in_si_template_id;

        DELETE FROM si_template_stage
        WHERE si_template_id = in_si_template_id;
    END IF;
    
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
            OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED in DELETE_SHOPPING_INDEX_STAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_SHOPPING_INDEX_STAGE;

end SHOPPING_INDEX_MAINT_PKG;
/
