CREATE OR REPLACE
PROCEDURE ftd_apps.OE_UPDATE_SET_PRODUCT_AVAIL (
 productIdIn IN VARCHAR2
)
--=======================================================================
--
-- Name:    OE_UPDATE_SET_PRODUCT_AVAIL
-- Type:    Procedure
-- Syntax:  OE_UPDATE_SET_PRODUCT_AVAIL(productIdIn IN VARCHAR2)
--
-- Description:   Updates a product to status = available
--
--========================================================================

AS
BEGIN
        UPDATE PRODUCT_MASTER
        SET     STATUS = 'A',
                EXCEPTION_START_DATE = NULL,
                EXCEPTION_END_DATE = NULL
                WHERE PRODUCT_ID = productIdIn ;


END;
.
/
