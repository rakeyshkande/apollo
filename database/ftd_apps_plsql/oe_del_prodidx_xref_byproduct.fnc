CREATE OR REPLACE
FUNCTION ftd_apps.OE_DEL_PRODIDX_XREF_BYPRODUCT (productId IN varchar2)
    RETURN types.ref_cursor
--===================================================================
--
-- Name:    OE_DEL_PRODIDX_XREF_BYPRODUCT
-- Type:    Function
-- Syntax:  OE_DEL_PRODIDX_XREF_BYPRODUCT (productId IN varchar2)
-- Returns: ref_cursor for
--          countDeleted    NUMBER
--
-- Description:   Deletes rows from PRODUCT_INDEX_XREF by PRODUCT_ID
--                and returns the count of rows deleted.
--
--===================================================================
AS
    count_deleted     NUMBER := 0;
    count_cursor      types.ref_cursor;
BEGIN
    DELETE FROM PRODUCT_INDEX_XREF
    WHERE product_id = productId;

    count_deleted := SQL%ROWCOUNT;
    COMMIT;

    OPEN count_cursor FOR
      SELECT count_deleted as "countDeleted"
      FROM DUAL;

    RETURN count_cursor;
END
;
.
/
