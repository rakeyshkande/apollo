create or replace function ftd_apps.sp_get_vendor_active_products(IN_VENDOR_ID IN VARCHAR)
return types.ref_cursor 
--==============================================================================
--
-- Name:    SP_GET_VENDOR_ACTIVE_PRODUCTS
-- Type:    Function
-- Syntax:  SP_GET_VENDOR_ACTIVE_PRODUCTS
-- Returns: ref_cursor for
--          PRODUCT_SUBCODE_ID   VARCHAR2(10),
--          SHIPPING_SYSTEM
--
--
-- Description:   returns a ref cursor for active products 
--                for the passed vendor id.
--
--============================================================
is
  Result types.ref_cursor;
begin
  OPEN Result FOR
    SELECT vp."PRODUCT_SUBCODE_ID" FROM FTD_APPS."VENDOR_PRODUCT" vp
      JOIN FTD_APPS.PRODUCT_MASTER pm ON vp."PRODUCT_SUBCODE_ID" = pm."PRODUCT_ID"
     WHERE pm."STATUS"='A' AND vp."VENDOR_ID"=IN_VENDOR_ID
       AND vp."REMOVED"='N'
  UNION
    SELECT vp."PRODUCT_SUBCODE_ID" FROM FTD_APPS."VENDOR_PRODUCT" vp
      JOIN FTD_APPS."PRODUCT_SUBCODES" ps ON vp."PRODUCT_SUBCODE_ID" = ps.PRODUCT_SUBCODE_ID
      JOIN FTD_APPS."PRODUCT_MASTER" pm ON ps.PRODUCT_ID = pm.PRODUCT_ID
     WHERE pm."STATUS"='A' AND vp."VENDOR_ID"=IN_VENDOR_ID
       AND vp."REMOVED"='N';

  return(Result);
end sp_get_vendor_active_products;
.
/
/
