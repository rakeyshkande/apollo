CREATE OR REPLACE TRIGGER FTD_APPS.FLORIST_SUSPENDS_PAS_FEED_TRG
AFTER INSERT OR DELETE OR UPDATE OF SUSPEND_START_DATE,
                                    SUSPEND_END_DATE,
                                    SUSPEND_PROCESSED_FLAG 
                           ON FTD_APPS.FLORIST_SUSPENDS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE

   v_fm_record_type     florist_master.record_type%TYPE;
   v_buffer             integer := 0;

   procedure ins_pas(in_action      IN varchar2,
                     in_florist     IN varchar2,
                     in_start_date  IN date,
                     in_end_date    IN date) is

   v_start_date  date;
   v_priority    integer;
   v_max_days    integer;
   v_end_date    date;
   
   begin

      -- Don't send dates < current date to PAS
      if in_start_date IS NOT NULL and
         trunc(in_start_date) < trunc(sysdate) then
         v_start_date := sysdate;
      else
         v_start_date := in_start_date;
      end if;
             
      -- Don't even call PAS for old end dates
      if (in_end_date IS NOT NULL and 
            trunc(in_end_date) >= trunc(sysdate)) OR
         (in_end_date IS NULL)                    then   

         if in_start_date - (v_buffer/1440) <= sysdate then
            select value into v_max_days
            from frp.global_parms
            where context = 'PAS_CONFIG'
            and name = 'MAX_DAYS';
            v_end_date := trunc(sysdate) + v_max_days;
         else
            v_end_date := in_end_date;
         end if;

         v_priority := trunc(v_start_date) - trunc(sysdate);
         PAS.POST_PAS_COMMAND_PRIORITY(in_action,   
                              in_florist                       || ' ' || 
                              to_char(v_start_date,'MMDDYYYY') || ' ' || 
                              to_char(v_end_date,'MMDDYYYY'),
                              v_priority);

         -- dbms_output.put_line('PAYLOAD="' || in_action || ',' ||   
         --                      in_florist                       || ' ' || 
         --                      to_char(v_start_date,'MMDDYYYY') || ' ' || 
         --                      to_char(in_end_date,'MMDDYYYY')  || '"');
      end if;
   end ins_pas;

   procedure determine_dates(in_old_start_date   IN  date,
                             in_new_start_date   IN  date,
                             in_old_end_date     IN  date,
                             in_new_end_date     IN  date,
                             out_start_date      OUT date,
                             out_end_date        OUT date) is
      --
      --This routine works for both Block and Suspend start/end dates (identical logic).
      --
      --Return out_start_date as:
      --   1.  the non-null of the old START_DATE and new START_DATE 
      --         if one or the other is null
      --   2.  the lesser of the old and new START_DATE if they are both non-null
      --   3.  null if they are both null
      --Return out_end_date as: 
      --   1.  the new END_DATE if the old START_DATE and old END_DATE are null
      --   1a. the old END_DATE if the new START_DATE and new END_DATE are null (missing from rqmts doc)
      --   2.  or  null if the old or new END_DATE is null
      --   3.  or the greater of the old and the new END_DATE if they are both non-null

   begin
      if in_old_start_date IS NULL and in_new_start_date IS NULL THEN
         out_start_date := NULL;
      elsif in_old_start_date IS NULL THEN
         out_start_date := in_new_start_date;
      elsif in_new_start_date IS NULL THEN
         out_start_date := in_old_start_date;
      else  -- Old AND New have values
         if in_old_start_date < in_new_start_date THEN
            out_start_date := in_old_start_date;
         else
            out_start_date := in_new_start_date;
         end if;
      end if;
         
      if in_old_start_date IS NULL and in_old_end_date IS NULL THEN
         out_end_date := in_new_end_date;
      elsif in_new_start_date IS NULL and in_new_end_date IS NULL THEN
         out_end_date := in_old_end_date;
      elsif in_old_end_date IS NULL or in_new_end_date IS NULL THEN
         out_end_date := NULL;
      else  -- Old AND New have values
         if in_old_end_date > in_new_end_date THEN
            out_end_date := in_old_end_date;
         else
            out_end_date := in_new_end_date;
         end if;
      end if;
   end determine_dates;


   
BEGIN

   BEGIN
      -- Get the florist's record type.  Throw an 'X' in there if NULL to make
      -- subsequent conditions easier.
      select NVL(record_type,'X')
      into   v_fm_record_type
      from florist_master
      where florist_id = :NEW.florist_id;
      
   EXCEPTION
      when others then
         v_fm_record_type := 'X';
   END;

   IF v_fm_record_type = 'R' THEN 

      select value into v_buffer
      from frp.global_parms
      where context = 'UPDATE_FLORIST_SUSPEND'
      and name = 'BUFFER_MINUTES';

      IF INSERTING THEN

         IF :NEW.suspend_start_date is not null AND
            (:NEW.suspend_start_date <= sysdate - (v_buffer/1440) OR :NEW.suspend_processed_flag = 'Y') then
            ins_pas('MODIFIED_FLORIST', :new.florist_id, :new.suspend_start_date, :new.suspend_end_date);
         END IF;

      ELSIF UPDATING  THEN
         declare
            v_start_date  date;
            v_end_date    date;

         begin
         
            -- If Suspend start or end changed, need to send a PAS msg
            --
            -- Skip if suspend_start_date is past and suspend_processed_flag = N - 
            -- another job will run that will change the suspend_processed_flag to Y, 
            -- triggering another PAS update.

            if  (:NEW.suspend_processed_flag = 'N'   and                 
                 :NEW.suspend_start_date IS NOT NULL and 
                 :NEW.suspend_start_date - (v_buffer/1440) <= sysdate  and
                 :OLD.suspend_start_date IS NULL 
                ) THEN
               -- Do nothing - other job will get it
               null;
            else
               determine_dates(:OLD.suspend_start_date,
                               :NEW.suspend_start_date,
                               :OLD.suspend_end_date,
                               :NEW.suspend_end_date,
                               v_start_date,
                               v_end_date);
               ins_pas('MODIFIED_FLORIST', :NEW.florist_id, v_start_date, v_end_date);
            end if;
         end;
      
      ELSIF DELETING  THEN
      
         IF :OLD.suspend_start_date IS NOT NULL or :OLD.suspend_end_date IS NOT NULL then
            ins_pas('MODIFIED_FLORIST', :old.florist_id, :old.suspend_start_date, :old.suspend_end_date);
         END IF;
   
      END IF;
   END IF;

END;
/
