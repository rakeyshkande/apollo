CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIP_GNADD (inZipCode IN VARCHAR2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_ZIP_GNADD
-- Type:    Function
-- Syntax:  OE_GET_ZIP_GNADD (inZipCode IN VARCHAR2)
-- Returns: ref_cursor for
--          gnaddLevel          NUMBER
--          gnaddDate           VARCHAR2(10)
--          zipGnaddFlag        VARCHAR2(1)
--          zipGotoFloristFlag  VARCHAR2(1)
--
-- Description:   Queries the GLOBAL_PARMS, FLORIST_ZIPS, FLORIST_MASTER and
--                CSZ_AVAIL tables to collect information about the input zip
--                code related to GNADD.  Returns a single row in ref cursor.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
        tmpZipCode VARCHAR2(30) := NULL;
BEGIN

        IF ( inZipCode IS NOT NULL )
        THEN
                tmpZipCode := OE_CLEANUP_ALPHANUM_STRING(inZipCode, 'Y');
        END IF;

    OPEN cur_cursor FOR

        -- CR 11/2004 - for new florist data model
        -- retrieve florist that are not inactive
        -- check that the zip code is not blocked
        SELECT gp.GNADD_LEVEL as "gnaddLevel",
            to_char(gp.GNADD_DATE, 'mm/dd/yyyy') as "gnaddDate",
            csz.GNADD_FLAG as "zipGnaddFlag",
            DECODE(NVL(gc.GOTO_COUNT,0), 0, 'N', 'Y') as "zipGotoFloristFlag"
        FROM GLOBAL_PARMS gp, CSZ_AVAIL csz,
            ( SELECT COUNT(*) as goto_count
              FROM FLORIST_ZIPS fz, FLORIST_MASTER fm
              WHERE fz.ZIP_CODE = tmpZipCode
              AND fm.FLORIST_ID = fz.FLORIST_ID
              AND fm.SUPER_FLORIST_FLAG = 'Y'
              AND fm.status <> 'Inactive'
              AND fz.block_start_date is null) gc
        WHERE csz.CSZ_ZIP_CODE = tmpZipCode;

    RETURN cur_cursor;
END;
.
/
