CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_DNIS_BY_ID_SFMB (dnisId IN number)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_DNIS_BY_ID
-- Type:    Function
-- Syntax:  OE_GET_DNIS_BY_ID ( dnisId IN NUMBER )
-- Returns: ref_cursor for
--          DNIS_ID                 NUMBER(4)
--          DESCRIPTION             VARCHAR2(40)
--          ORIGIN                  VARCHAR2(10)
--          STATUS_FLAG             VARCHAR2(1)
--          OE_FLAG                 VARCHAR2(1)
--          YELLOW_PAGES_FLAG       VARCHAR2(1)
--          SCRIPT_CODE             VARCHAR2(2)
--          DEFAULT_SOURCE_CODE     VARCHAR2(10)
--			COMPANY_NAME			VARCHAR2(30)
--			SCRIPT_ID        		VARCHAR2(30)
--
-- Description:   Queries the DNIS table by DNIS ID and returns a ref cursor
--                to the resulting row.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT DNIS_ID             		 				    as "dnisId",
               DESCRIPTION         	  	 					as "description",
               ORIGIN              	  	 				  	as "origin",
               STATUS_FLAG         	  	 			 	    as "statusFlag",
               OE_FLAG             	  	 				    as "oeFlag",
               YELLOW_PAGES_FLAG   	  	 			 	    as "yellowPagesFlag",
               SCRIPT_CODE         	  	 					as "scriptCode",
               DNIS.DEFAULT_SOURCE_CODE  				    as "defaultSourceCode",
			   COMPANY.COMPANY_NAME		 					as "companyName",
			   SCRIPT_ID				 				  	as "scriptId",
			   SCRIPT_MASTER.SCRIPT_DESCRIPTION  			as "scriptDescription"
        FROM DNIS, COMPANY, SCRIPT_MASTER
        WHERE DNIS_ID = dnisId
		  AND DNIS.ORIGIN = COMPANY.COMPANY_ID
		  AND DNIS.SCRIPT_ID = SCRIPT_MASTER.SCRIPT_MASTER_ID;

    RETURN cur_cursor;
END
;
.
/
