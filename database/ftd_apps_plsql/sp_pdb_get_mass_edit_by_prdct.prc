create or replace 
PROCEDURE                          FTD_APPS.SP_PDB_GET_MASS_EDIT_BY_PRDCT (
  inProduct1    IN VARCHAR2,
  inProduct2    IN VARCHAR2,
  inProduct3    IN VARCHAR2,
  inProduct4    IN VARCHAR2,
  inProduct5    IN VARCHAR2,
  refCursor     OUT TYPES.REF_CURSOR
)

--=============================================================================
--
-- Name:    SP_PDB_GET_MASS_EDIT_BY_PRDCT
--
-- Description: Queries the PRODUCT_MASTER table by product idand returns a ref 
--              cursor to the resulting rows.  The input parameters determine
--              which filtering criteria should be applied to the query.
--            
--
--=============================================================================
AS
v_temp         VARCHAR(200);
v_sql          VARCHAR2(4000);
v_in_clause    VARCHAR2(1000);
v_in_count     NUMBER;

BEGIN

v_sql := 'SELECT null    	         as "MASTER_SKU",
               PRODUCT_ID            	 as "PRODUCT_ID",
               PRODUCT_NAME	     	 as "PRODUCT_NAME",
               NOVATOR_ID            	 as "NOVATOR_ID",
               NOVATOR_NAME            	 as "NOVATOR_NAME",
               FLORIST_REFERENCE_NUMBER  as "FLORIST_REFERENCE_NUMBER",
               CATEGORY			 as "CATEGORY",
               PRODUCT_TYPE	         as "PRODUCT_TYPE",
               PRODUCT_SUB_TYPE          as "PRODUCT_SUB_TYPE",
               LONG_DESCRIPTION          as "LONG_DESCRIPTION",
               STATUS                    as "STATUS",
               DELIVERY_TYPE             as "DELIVERY_TYPE",
               SHIP_METHOD_FLORIST       as "SHIP_METHOD_FLORIST",
               SHIP_METHOD_CARRIER       as "SHIP_METHOD_CARRIER",
               STANDARD_RECIPE           as "STANDARD_RECIPE",
               DELUXE_RECIPE             as "DELUXE_RECIPE",
               PREMIUM_RECIPE            as "PREMIUM_RECIPE",
               DIM_WEIGHT                as "DIM_WEIGHT",
               STANDARD_PRICE            as "STANDARD_PRICE",
               DELUXE_PRICE              as "DELUXE_PRICE",
               PREMIUM_PRICE             as "PREMIUM_PRICE",
               SENT_TO_NOVATOR_PROD      as "SENT_TO_NOVATOR_PROD",
               GENERAL_COMMENTS          as "GENERAL_COMMENTS",
               MERCURY_DESCRIPTION       as "MERCURY_DESCRIPTION",
               PQUAD_PRODUCT_ID          as "PQUAD_PRODUCT_ID"
   FROM FTD_APPS.PRODUCT_MASTER 
   WHERE PRODUCT_TYPE IS NOT NULL 
   AND PRODUCT_TYPE !=  ''NONE''
    ';
   
   v_in_clause := ' AND LOWER(PRODUCT_ID) IN (';
   
   
   IF inProduct1 IS NOT NULL THEN
       v_temp := ' LOWER(''' || inProduct1 || ''')';
       v_in_clause := v_in_clause || v_temp;
   END IF;
   
   IF inProduct2 IS NOT NULL THEN
       v_temp := ' ,LOWER(''' || inProduct2 || ''')';
       v_in_clause := v_in_clause || v_temp;
   END IF; 
   
   IF inProduct3 IS NOT NULL THEN
          v_temp := ' ,LOWER(''' || inProduct3 || ''')';
          v_in_clause := v_in_clause || v_temp;
   END IF; 
   
   IF inProduct4 IS NOT NULL THEN
          v_temp := ' ,LOWER(''' || inProduct4 || ''')';
          v_in_clause := v_in_clause || v_temp;
   END IF;  
   
   IF inProduct5 IS NOT NULL THEN
          v_temp := ' ,LOWER(''' || inProduct5 || ''')';
          v_in_clause := v_in_clause || v_temp;
   END IF;  
   
   v_in_clause := v_in_clause || ') order by 2';
   v_sql := v_sql || v_in_clause;
--   dbms_output.put_line('  sql: ' || v_sql);
   OPEN refCursor FOR v_sql;
END;
/