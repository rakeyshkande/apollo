CREATE OR REPLACE
TRIGGER ftd_apps.box_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.box 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.box$ (
         BOX_ID,         
         BOX_NAME,       
         BOX_DESC,       
         WIDTH_INCH_QTY, 
         HEIGHT_INCH_QTY,
         LENGTH_INCH_QTY,
         STANDARD_FLAG,  
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :NEW.BOX_ID,         
         :NEW.BOX_NAME,       
         :NEW.BOX_DESC,       
         :NEW.WIDTH_INCH_QTY, 
         :NEW.HEIGHT_INCH_QTY,
         :NEW.LENGTH_INCH_QTY,
         :NEW.STANDARD_FLAG,  
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.box$ (
         BOX_ID,         
         BOX_NAME,       
         BOX_DESC,       
         WIDTH_INCH_QTY, 
         HEIGHT_INCH_QTY,
         LENGTH_INCH_QTY,
         STANDARD_FLAG,  
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :OLD.BOX_ID,         
         :OLD.BOX_NAME,       
         :OLD.BOX_DESC,       
         :OLD.WIDTH_INCH_QTY, 
         :OLD.HEIGHT_INCH_QTY,
         :OLD.LENGTH_INCH_QTY,
         :OLD.STANDARD_FLAG,  
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.box$ (
         BOX_ID,         
         BOX_NAME,       
         BOX_DESC,       
         WIDTH_INCH_QTY, 
         HEIGHT_INCH_QTY,
         LENGTH_INCH_QTY,
         STANDARD_FLAG,  
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :NEW.BOX_ID,         
         :NEW.BOX_NAME,       
         :NEW.BOX_DESC,       
         :NEW.WIDTH_INCH_QTY, 
         :NEW.HEIGHT_INCH_QTY,
         :NEW.LENGTH_INCH_QTY,
         :NEW.STANDARD_FLAG,  
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.box$ (
         BOX_ID,         
         BOX_NAME,       
         BOX_DESC,       
         WIDTH_INCH_QTY, 
         HEIGHT_INCH_QTY,
         LENGTH_INCH_QTY,
         STANDARD_FLAG,  
         CREATED_BY,     
         CREATED_ON,     
         UPDATED_BY,     
         UPDATED_ON,     
         operation$, timestamp$
      ) VALUES (
         :OLD.BOX_ID,         
         :OLD.BOX_NAME,       
         :OLD.BOX_DESC,       
         :OLD.WIDTH_INCH_QTY, 
         :OLD.HEIGHT_INCH_QTY,
         :OLD.LENGTH_INCH_QTY,
         :OLD.STANDARD_FLAG,  
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',SYSDATE);

   END IF;

END;
/
