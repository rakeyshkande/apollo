CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCTS_UPDATED_SINCE
(IN_UPDATE_DATE IN DATE)
 RETURN types.ref_cursor
AUTHID CURRENT_USER
--=======================================================================
--
-- Name:    SP_GET_PRODUCTS_UPDATED_SINCE
-- Type:    Function
-- Returns: ref_cursor for
--          PRODUCT_ID          VARCHAR2(10)
--
-- Description:   Returns all products updated on or after the date/time passed in
--
--========================================================================

AS
    OUT_CUR types.ref_cursor;
BEGIN
    OPEN OUT_CUR FOR
        SELECT PRODUCT_ID
            FROM PRODUCT_MASTER
            WHERE LAST_UPDATE >= IN_UPDATE_DATE;
    RETURN OUT_CUR;
END;
.
/
