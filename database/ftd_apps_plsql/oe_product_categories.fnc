CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODUCT_CATEGORIES
    RETURN types.ref_cursor
--==================================================================
--
-- Name:    OE_PRODUCT_CATEGORIES
-- Type:    Function
-- Syntax:  OE_PRODUCT_CATEGORIES ( )
-- Returns: ref_cursor for
--          PRODUCT_CATEGORY_ID     VARCHAR2(10)
--          DESCRIPTION             VARCHAR2(40)
--
-- Description:   Queries the top level product indexes and returns
--                listing of their IDs and descriptions.
--
--==================================================================

AS
    category_cursor types.ref_cursor;
BEGIN
    OPEN category_cursor FOR
        SELECT INDEX_ID as "productCategoryId",
               INDEX_NAME as "description"
          FROM PRODUCT_INDEX
          WHERE ADV_SEARCH_PRODUCTS_FLAG = 'Y'
          order by DISPLAY_ORDER;

    RETURN category_cursor;
END
;
.
/
