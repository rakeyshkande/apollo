CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_COLORS (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_COLORS
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_COLORS ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--          PRODUCT_COLOR             VARCHAR2(25)
--
-- Description:   Queries the PRODUCT_COLORS table by PRODUCT ID and returns a ref cursor
--                to the resulting row.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT pc.PRODUCT_COLOR as "color",
               pc.ORDER_BY      as "orderBy",
               cm.DESCRIPTION as "colorName"
          FROM PRODUCT_COLORS pc, COLOR_MASTER cm
    WHERE pc.PRODUCT_ID = productId
    AND pc.PRODUCT_COLOR = cm.COLOR_MASTER_ID;

    RETURN cur_cursor;
END
;
.
/
