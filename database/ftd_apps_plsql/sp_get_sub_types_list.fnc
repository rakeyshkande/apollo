CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SUB_TYPES_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SUB_TYPES_LIST
-- Type:    Function
-- Syntax:  SP_GET_SUB_TYPES_LIST ()
-- Returns: ref_cursor for
--          SUB_TYPE_ID           VARCHAR2(10)
--          SUB_TYPE_DESCRIPTION  VARCHAR2(100)
--          TYPE_ID               NUMBER(10)
--
--
-- Description:   Queries the PRODUCT_SUB_TYPES table and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    product_sub_types_cursor types.ref_cursor;
BEGIN
    OPEN product_sub_types_cursor FOR
        SELECT SUB_TYPE_ID          as "subTypeId",
               SUB_TYPE_DESCRIPTION as "subTypeDescription",
               TYPE_ID              as "typeId"
        FROM PRODUCT_SUB_TYPES order by SUB_TYPE_DESCRIPTION;

    RETURN product_sub_types_cursor;
END
;
.
/
