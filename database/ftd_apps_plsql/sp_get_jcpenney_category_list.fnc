CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_JCPENNEY_CATEGORY_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_JCPENNEY_CATEGORY_LIST
-- Type:    Function
-- Syntax:  SP_GET_JCPENNEY_CATEGORY_LIST ( )
-- Returns: ref_cursor for
--          CATEGORY_ID     VARCHAR2(1)
--          CATEGORY_DESCRIPTION   VARCHAR2(25)
--
-- Description:   Queries the JCPenney_category table and returns a ref cursor for row.
--
--===========================================================

AS
    category_cursor types.ref_cursor;
BEGIN
    OPEN category_cursor FOR
        SELECT CATEGORY_ID as "categoryId",
               CATEGORY_DESCRIPTION as "categoryDescription"
          FROM JCPENNEY_CATEGORY order by CATEGORY_DESCRIPTION;

    RETURN category_cursor;
END;
.
/
