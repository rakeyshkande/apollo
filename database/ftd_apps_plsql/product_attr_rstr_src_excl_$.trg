CREATE OR REPLACE
TRIGGER ftd_apps.product_attr_rstr_src_excl_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.product_attr_restr_source_excl
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;


   IF INSERTING THEN

      INSERT INTO ftd_apps.product_attr_rstr_src_excl$ (
         source_code,
         product_attr_restr_id,
         created_on,
         created_by,
         OPERATION$,
         TIMESTAMP$
      ) VALUES (
         :NEW.source_code,
         :NEW.product_attr_restr_id,
         :NEW.created_on,
         :NEW.created_by,
         'INS',
          v_current_timestamp);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.product_attr_rstr_src_excl$ (
         source_code,
         product_attr_restr_id,
         created_on,
         created_by,
         OPERATION$,
         TIMESTAMP$
      ) VALUES (
         :OLD.source_code,
         :OLD.product_attr_restr_id,
         :OLD.created_on,
         :OLD.created_by,
         'UPD_OLD',
          v_current_timestamp);

      INSERT INTO ftd_apps.product_attr_rstr_src_excl$ (
         source_code,
         product_attr_restr_id,
         created_on,
         created_by,
         OPERATION$,
         TIMESTAMP$
      ) VALUES (
         :NEW.source_code,
         :NEW.product_attr_restr_id,
         :NEW.created_on,
         :NEW.created_by,
         'UPD_NEW',
          v_current_timestamp);


   ELSIF DELETING  THEN
      INSERT INTO ftd_apps.product_attr_rstr_src_excl$ (
         source_code,
         product_attr_restr_id,
         created_on,
         created_by,
         OPERATION$,
         TIMESTAMP$
      ) VALUES (
         :OLD.source_code,
         :OLD.product_attr_restr_id,
         :OLD.created_on,
         :OLD.created_by,
         'DEL',
          v_current_timestamp);

   END IF;

END;
/
