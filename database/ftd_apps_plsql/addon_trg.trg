CREATE OR REPLACE
TRIGGER ftd_apps.addon_trg
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.addon
REFERENCING new as newrow old as oldrow
FOR EACH ROW
DECLARE
   v_payload varchar2(2000);
   v_stat    varchar2(1) := 'Y';
   v_mess    varchar2(2000);
   v_exception     EXCEPTION;
   v_exception_txt varchar2(200);
   v_trigger_enabled varchar2(10) := 'N';

BEGIN

if INSERTING then
        insert into addon_history
        (
                addon_history_id,
                addon_id,
                addon_type,
                description,
                price,
                addon_text,
                unspsc,
                product_id,
                active_flag,
                default_per_type_flag,
                display_price_flag,
                addon_weight,
                created_on,
                created_by,
                updated_on,
                updated_by,
                status,
                florist_addon_flag,
                vendor_addon_flag,
                pquad_accessory_id,-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
                is_ftd_west_addon-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
        )
        values
        (
                addon_history_id_sq.nextval,
                :newrow.addon_id,
                :newrow.addon_type,
                :newrow.description,
                :newrow.price,
                :newrow.addon_text,
                :newrow.unspsc,
                :newrow.product_id,
                :newrow.active_flag,
                :newrow.default_per_type_flag,
                :newrow.display_price_flag,
                :newrow.addon_weight,
                :newrow.created_on,
                :newrow.created_by,
                :newrow.updated_on,
                :newrow.updated_by,
                'Active',
                :newrow.florist_addon_flag,
                :newrow.vendor_addon_flag,
                :newrow.pquad_accessory_id,
                :newrow.is_ftd_west_addon
        );

elsif UPDATING then
        update  addon_history
        set     status = 'Inactive',
                updated_on = sysdate,
                updated_by = :newrow.updated_by
        where   addon_id = :oldrow.addon_id
        and     status = 'Active';

        insert into addon_history
        (
                addon_history_id,
                addon_id,
                addon_type,
                description,
                price,
                addon_text,
                unspsc,
                product_id,
                active_flag,
                default_per_type_flag,
                display_price_flag,
                addon_weight,
                created_on,
                created_by,
                updated_on,
                updated_by,
                status,
                florist_addon_flag,
                vendor_addon_flag,
                pquad_accessory_id,-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
                is_ftd_west_addon-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
        )
        values
        (
                addon_history_id_sq.nextval,
                :newrow.addon_id,
                :newrow.addon_type,
                :newrow.description,
                :newrow.price,
                :newrow.addon_text,
                :newrow.unspsc,
                :newrow.product_id,
                :newrow.active_flag,
                :newrow.default_per_type_flag,
                :newrow.display_price_flag,
                :newrow.addon_weight,
                :newrow.created_on,
                :newrow.created_by,
                :newrow.updated_on,
                :newrow.updated_by,
                'Active',
                :newrow.florist_addon_flag,
                :newrow.vendor_addon_flag,
                :newrow.pquad_accessory_id,
                :newrow.is_ftd_west_addon
        );

         -- For Project Fresh, post a message with 15 sec delay.
         -- This will result in PDB determining all products associated with the addon and triggering updates.
         SELECT value INTO v_trigger_enabled FROM frp.global_parms WHERE context='SERVICE' AND name='FRESH_TRIGGERS_ENABLED';
         IF (v_trigger_enabled = 'Y') THEN          
            IF (:oldrow.addon_id IS NOT NULL) THEN
               v_payload := 'FRESH_MULTI_MSG_PRODUCER|addon|addon_all|' || :oldrow.addon_id;
               events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 15, v_stat, v_mess);      
               IF v_stat = 'N' THEN
                  v_exception_txt := 'ERROR posting a JMS message in PDB (addon trigger) for Fresh addon = ' || :oldrow.addon_id || SUBSTR(v_mess,1,200);
                  raise v_exception;
               END IF;
            END IF;
         END IF;

else
        update  addon_history
        set     status = 'Inactive',
                updated_on = sysdate,
                updated_by = :oldrow.updated_by
        where   addon_id = :oldrow.addon_id
        and     status = 'Active';

end if;

END ADDON_TRG;
/
