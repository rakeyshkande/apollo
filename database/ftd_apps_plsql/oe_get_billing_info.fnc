CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_BILLING_INFO ( sourceCode  IN VARCHAR2,
  prompt      IN VARCHAR2 )
 RETURN Types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_BILLING_INFO
-- Type:    Function
-- Syntax:  OE_GET_BILLING_INFO ( sourceCode  IN VARCHAR2,
--                                prompt      IN VARCHAR2 )
-- Returns: ref_cursor for
--          billingInfoSequence       NUMBER
--          billingInfoPrompt         VARCHAR2(25)
--          infoFormat                VARCHAR2(25)
--
-- Description:   Queries the BILLING_INFO table and returns all rows for the
--                given source code, or if a prompt is specified just the row
--                with that prompt.
--
--==============================================================================
AS
    cur_cursor Types.ref_cursor;

BEGIN
    OPEN cur_cursor FOR
        SELECT BILLING_INFO_SEQUENCE AS "billingInfoSequence",
              INFO_DESCRIPTION AS "billingInfoPrompt",
			  DECODE(INFO_DISPLAY, NULL, INFO_DESCRIPTION, INFO_DISPLAY) AS "billingInfoDisplay",
			  DECODE(INFO_FORMAT, NULL, '/.*/', Oe_Make_Regexp(INFO_FORMAT)) AS "infoFormat",
			  PROMPT_TYPE AS "promptType"
        FROM BILLING_INFO
        WHERE SOURCE_CODE_ID = UPPER(sourceCode)
        AND ( prompt IS NULL
          OR INFO_DESCRIPTION = UPPER(prompt) )
		AND ( prompt IS NOT NULL
		  OR INFO_DESCRIPTION != 'CERTIFICATE#' )
		ORDER BY BILLING_INFO_SEQUENCE;

    RETURN cur_cursor;
END;
.
/
