CREATE OR REPLACE TRIGGER FTD_APPS.PRODUCT_COMPONENT_SKU$
AFTER INSERT OR UPDATE OR DELETE ON FTD_APPS.PRODUCT_COMPONENT_SKU 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.product_component_sku$ (
         PRODUCT_ID,           
         COMPONENT_SKU_ID,           
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :NEW.PRODUCT_ID,           
         :NEW.COMPONENT_SKU_ID,           
         :NEW.created_by,                 
         :NEW.created_on,                 
         :NEW.updated_by,                  
         :NEW.updated_on,                 
          'INS',SYSTIMESTAMP);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.product_component_sku$ (
         PRODUCT_ID,           
         COMPONENT_SKU_ID,           
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :OLD.PRODUCT_ID,           
         :OLD.COMPONENT_SKU_ID,           
         :OLD.created_by,                 
         :OLD.created_on,                 
         :OLD.updated_by,                 
         :OLD.updated_on,                 
         'UPD_OLD',SYSTIMESTAMP);

      INSERT INTO ftd_apps.product_component_sku$ (
         PRODUCT_ID,           
         COMPONENT_SKU_ID,           
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :NEW.PRODUCT_ID,           
         :NEW.COMPONENT_SKU_ID,           
         :NEW.created_by,                 
         :NEW.created_on,                 
         :NEW.updated_by,                 
         :NEW.updated_on,                 
         'UPD_NEW',SYSTIMESTAMP);


   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.product_component_sku$ (
         PRODUCT_ID,           
         COMPONENT_SKU_ID,           
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :OLD.PRODUCT_ID,           
         :OLD.COMPONENT_SKU_ID,           
         :OLD.created_by,                 
         :OLD.created_on,                 
         :OLD.updated_by,                 
         :OLD.updated_on,                 
         'DEL',SYSTIMESTAMP);

   END IF;

END;
/
