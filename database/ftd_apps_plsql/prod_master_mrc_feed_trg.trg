create or replace
TRIGGER ftd_apps.prod_master_mrc_feed_trg
AFTER INSERT OR UPDATE OF 
NOVATOR_ID,
PRODUCT_NAME,
LONG_DESCRIPTION,
STANDARD_PRICE,
DELUXE_PRICE,
PREMIUM_PRICE,
NO_TAX_FLAG,
STATUS,
EXCEPTION_START_DATE,
EXCEPTION_END_DATE
 ON ftd_apps.product_master
REFERENCING new as new old as old
FOR EACH ROW

DECLARE

v_prod_src_cnt number := 0;
v_count    number := 0;

BEGIN
      
      BEGIN
             select count(*) into v_prod_src_cnt from ftd_apps.Product_company_xref pcx
              where pcx.product_id = :new.product_id and pcx.company_id = 'FTD';
              
              EXCEPTION WHEN NO_DATA_FOUND THEN
                      v_prod_src_cnt := 0;
      END;


	IF v_prod_src_cnt > 0 THEN
	
	    IF INSERTING or (:old.NOVATOR_ID <> :new.NOVATOR_ID)
		or (:old.PRODUCT_NAME <> :new.PRODUCT_NAME)
		or (:old.LONG_DESCRIPTION <> :new.LONG_DESCRIPTION)
		or (:old.STANDARD_PRICE <> :new.STANDARD_PRICE)
                or (:old.DELUXE_PRICE <> :new.DELUXE_PRICE) 
                or (:old.PREMIUM_PRICE <> :new.PREMIUM_PRICE)
		or (:old.NO_TAX_FLAG <> :new.NO_TAX_FLAG)
		or (:old.STATUS <> :new.STATUS)
		or (:old.EXCEPTION_START_DATE <> :new.EXCEPTION_START_DATE)
		or (:old.EXCEPTION_START_DATE is NULL and :new.EXCEPTION_START_DATE is not NULL)
                or (:old.EXCEPTION_START_DATE is not NULL and :new.EXCEPTION_START_DATE is NULL) 
		or (:old.EXCEPTION_END_DATE <> :new.EXCEPTION_END_DATE)
		or (:old.EXCEPTION_END_DATE is NULL and :new.EXCEPTION_END_DATE is not NULL)
                or (:old.EXCEPTION_END_DATE is not NULL and :new.EXCEPTION_END_DATE is NULL) 
		then       

            select count(*) into v_count from ptn_mercent.mrcnt_product_feed
            where product_id = :new.product_id and status = 'NEW';

            if v_count = 0 then

                insert into ptn_mercent.mrcnt_product_feed(
                    MRCNT_PRODUCT_FEED_ID,
                    PRODUCT_ID,
                    STATUS,
                    CREATED_ON,
                    CREATED_BY,
                    UPDATED_ON,
                    UPDATED_BY)
                values (
                    ptn_mercent.mrcnt_product_feed_id_sq.nextval,
                    :new.product_id,
                    'NEW',
                    sysdate,
                    'SYS',
                    sysdate,
                    'SYS');

            end if;

        END IF;

    END IF;

END prod_master_mrc_feed_trg;
/