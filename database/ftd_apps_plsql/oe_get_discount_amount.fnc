CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_DISCOUNT_AMOUNT (inPriceHeaderId  IN VARCHAR2,
 inItemPrice IN VARCHAR2)
 RETURN types.ref_cursor
--=======================================================================
--
-- Name:    OE_GET_DISCOUNT_AMOUNT
-- Type:    Function
-- Syntax:  OE_GET_DISCOUNT_AMOUNT ( inPriceHeaderId  IN VARCHAR2, inItemPrice IN VARCHAR2 )
-- Returns: ref_cursor for
--          discountAmount	NUMBER(7,2)
--
-- Description:   Queries the role and functions tables and returns
--                a ref cursor to all functions for the given row.
--
--========================================================================

AS
    role_cursor types.ref_cursor;
BEGIN
    OPEN role_cursor FOR
        SELECT phd.DISCOUNT_AMT as "discountAmount"
        FROM PRICE_HEADER_DETAILS phd
          WHERE phd.PRICE_HEADER_ID = inPriceHeaderId
            AND TO_NUMBER(inItemPrice) >= phd.MIN_DOLLAR_AMT
            AND TO_NUMBER(inItemPrice) <= phd.MAX_DOLLAR_AMT;

    RETURN role_cursor;
END;
.
/
