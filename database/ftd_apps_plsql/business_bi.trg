CREATE OR REPLACE
TRIGGER
 "FTD_APPS"."BUSINESS_BI" BEFORE INSERT ON "FTD_APPS"."BUSINESS" FOR EACH ROW

DECLARE
  temp integer;
BEGIN
  if (INSERTING) then
    select business_id.nextval into temp from dual;
    :new.business_id := temp;
  end if;
END business_bi;
.
/
