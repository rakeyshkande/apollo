create or replace procedure ftd_apps.SP_REMOVE_SHIPPING_KEY(
  IN_SHIPPING_KEY_ID IN VARCHAR2
) AS
/**********************************************************
--
-- Name:    SP_REMOVE_SHIPPING_KEY
-- Type:    Procedure
-- Syntax:  SP_REMOVE_SHIPPING_KEY ( shipping_key_id in varchar2 )
--
-- Description:   Deletes a row from SHIPPING_KEYS and 
--                child tables by SHIPPING_KEY_ID.
--
***********************************************************/
begin
  DELETE FROM FTD_APPS."SHIPPING_KEY_COSTS" WHERE "SHIPPING_KEY_DETAIL_ID" IN (SELECT "SHIPPING_KEY_DETAIL_ID" FROM FTD_APPS."SHIPPING_KEY_DETAILS" WHERE "SHIPPING_KEY_ID"=TO_CHAR(IN_SHIPPING_KEY_ID));
  DELETE FROM FTD_APPS."SHIPPING_KEY_DETAILS" WHERE "SHIPPING_KEY_ID" = TO_CHAR(IN_SHIPPING_KEY_ID);
  DELETE FROM FTD_APPS."SHIPPING_KEYS" WHERE "SHIPPING_KEY_ID" = TO_CHAR(IN_SHIPPING_KEY_ID);
end SP_REMOVE_SHIPPING_KEY;
/
