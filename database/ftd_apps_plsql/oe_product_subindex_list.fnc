CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODUCT_SUBINDEX_LIST (productIndexId IN NUMBER)
    RETURN VARCHAR2
--==========================================================================
--
-- Name:    OE_PRODUCT_SUBINDEX_LIST
-- Type:    Function
-- Syntax:  OE_PRODUCT_SUBINDEX_LIST ()
-- Returns: List of subindex filenames for specified index, space separated.
--
-- Description:   Queries the PRODUCT_INDEX table and concatenates a list
--                of the subindex names for the specified parent index.
--                Used by OE_PRODUCT_INDEX_LIST.
--
--==========================================================================
AS
    CURSOR curSubindexList (productIndexId IN NUMBER)
    IS SELECT si.index_file_name
    FROM product_index si
    WHERE si.parent_index_id = productIndexId
    ORDER BY si.display_order;

    subindexRow         curSubindexList%ROWTYPE;

    tmpSubindexList     VARCHAR2(32767) := NULL;
BEGIN
    FOR subindexRow IN curSubindexList(productIndexId)
    LOOP

        IF ( tmpSubindexList IS NOT NULL )
        THEN
            tmpSubindexList := tmpSubindexList || ' ';
        END IF;

        tmpSubindexList := tmpSubindexList || subindexRow.index_file_name;
    END LOOP;

    RETURN tmpSubindexList;
END
;
.
/
