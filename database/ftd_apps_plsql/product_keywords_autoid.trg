create or replace trigger ftd_apps.product_keywords_autoid
before insert on ftd_apps.product_keywords
for each row
begin
   select ftd_apps.product_keywords_sq.nextval into :new.product_keyword_id from dual;
end;
/
