CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODUCT_LIST_BY_INDEX_MD04 (
       IN_INDEX_ID               IN NUMBER,
       IN_PRICE_POINT_ID         IN NUMBER,
       IN_SOURCE_CODE            IN VARCHAR2,
       IN_DOMESTIC_INTL_FLAG     IN VARCHAR2, --NO LONGER USED
       IN_POSTAL_CODE            IN VARCHAR2,
       IN_DELIVERY_END_DATE      IN VARCHAR2,
       IN_COUNTRY_ID             IN VARCHAR2,
       IN_SCRIPT_CODE            IN VARCHAR2
)
  RETURN Types.ref_cursor
--==============================================================================
--
-- Name:    OE_PRODUCT_LIST_BY_INDEX_LITE
-- Type:    Function
-- Syntax:  OE_PRODUCT_LIST_BY_INDEX_LITE(IN_INDEX_ID IN NUMBER,
--                                    IN_PRICE_POINT_ID IN NUMBER,
--                                    IN_SOURCE_CODE IN VARCHAR2,
--                                    IN_DOMESTIC_INTL_FLAG IN VARCHAR2,
--                                    IN_POSTAL_CODE IN VARCHAR2
--                                    IN_SCRIPT_CODE)
-- Returns: ref_cursor for
--          PRODUCT_ID             VARCHAR2(10),
--          STANDARD_PRICE         NUMBER(8.2),
--
-- Description:   Searches PRODUCT_MASTER by the supplied index ID using PRODUCT_INDEX_XREF.
--                Price point ID specifies a desired price range within which returned
--                products should fall (by standard price).
--
--                Source code, domestic/international flag (D or I) and the recipient
--                zip code must be provided.  Some of the product information is source
--                code dependent or depends on domestic vs. international.  Zip code
--                is used to exclude products not sold in the recipient's state.
--
--===================================================
AS
    cur_cursor            Types.ref_cursor;
    v_pricingCode         SOURCE.PRICING_CODE%TYPE := NULL;
    v_zipGnaddFlag        CSZ_AVAIL.GNADD_FLAG%TYPE := NULL;
    v_zipFloralFlag       VARCHAR2(1) := 'Y';
    v_stateId             ZIP_CODE.STATE_ID%TYPE := NULL;
    v_deliveryEnd         DATE := TO_DATE(IN_DELIVERY_END_DATE, 'mm/dd/yyyy');
    v_currentDate         DATE := TRUNC(SYSDATE);
    v_alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    v_alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    v_numChars            VARCHAR2(10) := '0123456789';
    v_numSub              VARCHAR2(10) := '##########';
    v_cszZip              VARCHAR2(10);
    v_cleanZip            VARCHAR2(10) := Oe_Cleanup_Alphanum_String(UPPER(IN_POSTAL_CODE), 'Y');
    v_gnaddLevel          GLOBAL_PARMS.GNADD_LEVEL%TYPE := NULL;
BEGIN

    -- Select the pricing code for this source code, for the below query
    BEGIN
        SELECT PRICING_CODE
        INTO v_pricingCode
        FROM FTD_APPS.SOURCE
        WHERE SOURCE_CODE = UPPER(IN_SOURCE_CODE);
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- Get the global GNADD level
    BEGIN
        SELECT GNADD_LEVEL
        INTO v_gnaddLevel
        FROM FTD_APPS.GLOBAL_PARMS
        WHERE GLOBAL_PARMS_KEY = 1;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        v_gnaddLevel := 0;
    END;

    -- If no zip provided, assume zip GNADD is off and floral available wherever this is going
    IF ( v_cleanZip IS NULL )
    THEN
        v_zipGnaddFlag  := 'N';
        v_zipFloralFlag := 'Y';

    ELSE
        -- Determine if the input zip code is Canadian
        IF ( TRANSLATE(v_cleanZip, v_alphaChars || v_numChars, v_alphaSub || v_numSub) LIKE '@#@%' )
        THEN
            -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
            v_cszZip := SUBSTR(v_cleanZip,1,3);
        ELSE
            v_cszZip := v_cleanZip;
        END IF;

        -- Select the GNADD flag for this zip, if available
        BEGIN
            -- Assume NULL GNADD flag indicates GNADD is off
            SELECT NVL(GNADD_FLAG, 'N')
            INTO v_zipGnaddFlag
            FROM FTD_APPS.CSZ_AVAIL
            WHERE CSZ_ZIP_CODE = v_cszZip;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            -- If no row found on CSZ_AVAIL, then no floral for this zip
            v_zipFloralFlag := 'N';
        END;
    END IF;

    -- Select the time zone for this zip, if available
    BEGIN
        SELECT DISTINCT ZC.STATE_ID
        INTO v_stateId
        FROM ZIP_CODE zc, STATE_MASTER sm
        WHERE zc.ZIP_CODE_ID = v_cleanZip
          AND ZC.STATE_ID = SM.STATE_MASTER_ID;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- Open a query ref cursor to return product list results
    OPEN cur_cursor FOR
        SELECT DECODE(px.UPSELL_MASTER_ID, NULL, pm.PRODUCT_ID, px.UPSELL_MASTER_ID) AS "productID",
                       DECODE(px.UPSELL_MASTER_ID, NULL, pm.NOVATOR_ID, px.UPSELL_MASTER_ID) AS "novatorID",
               pm.STANDARD_PRICE AS "standardPrice",
               px.display_order AS "displayOrder"
        FROM
        	  FTD_APPS.PRODUCT_MASTER pm,
        	  FTD_APPS.PRODUCT_INDEX_XREF_VW px,
            FTD_APPS.SOURCE sc,
            FTD_APPS.PRODUCT_COMPANY_XREF pcx

        -- Find products for the given index
        WHERE px.product_index_id = IN_INDEX_ID
        AND px.product_id = pm.product_id

        -- Only available products should be returned
        AND pm.status = 'A'

        -- If input country is US or null, exclude international products
        AND ( IN_COUNTRY_ID IS NULL
         OR   IN_COUNTRY_ID != 'US'
         OR   pm.delivery_type != 'I' )

        -- Screen out products listed as excluded by state for the supplied zipcode
        AND NOT EXISTS (
            SELECT 1
            FROM FTD_APPS.ZIP_CODE zc, FTD_APPS.PRODUCT_EXCLUDED_STATES pes
            WHERE zc.zip_code_id = v_cleanZip
            AND pes.product_id = pm.product_id
            AND pes.excluded_state = zc.state_id
            AND pes.SUN = 'Y'
            AND pes.MON = 'Y'
            AND pes.TUE = 'Y'
            AND pes.WED = 'Y'
            AND pes.THU = 'Y'
            AND pes.FRI = 'Y'
            AND pes.SAT = 'Y'
        )

        -- If the recipient country is non-US, exclude all but floral products.
        AND ( IN_COUNTRY_ID IS NULL
         OR   IN_COUNTRY_ID = 'US'
         OR   pm.ship_method_florist = 'Y'
        )

        -- If the script code is JCPenny, exclude B and C JCP Categories.
        AND ( decode(IN_SCRIPT_CODE, null, 'X', in_script_code) != 'JP'
         OR   pm.JCP_CATEGORY = 'A'
        )

        -- If the product has exception start and end date, and delivery end date was specified,
        --   exclude if it cannot be delivered sometime prior to delivery end date
        AND (
              ( pm.exception_start_date IS NULL OR pm.exception_end_date IS NULL OR
                pm.exception_code IS NULL OR v_deliveryEnd IS NULL )
          OR
              ( pm.exception_code = 'U' AND ( v_currentDate < pm.exception_start_date
               OR v_deliveryEnd > pm.exception_end_date ) )
          OR
             ( pm.exception_code = 'A' AND
              ( ( pm.exception_start_date >= v_currentDate AND pm.exception_start_date <= v_deliveryEnd ) OR
              ( pm.exception_end_date >= v_currentDate AND pm.exception_end_date <= v_deliveryEnd ) ) )
           )

        -- Filter out products that do not match price range, if provided
        AND EXISTS
            (SELECT 1 FROM FTD_APPS.FILTER_PRICE_POINTS pp
              WHERE pm.standard_price >= pp.price_lower_limit
                AND pm.standard_price <= pp.price_upper_limit
                AND pp.price_point_id =
                   DECODE (IN_PRICE_POINT_ID, NULL, pp.price_point_id, IN_PRICE_POINT_ID)
            )

        -- Use product_compnay_xref mapping table to find all products for this company
        AND sc.source_code = UPPER(IN_SOURCE_CODE)
        AND sc.company_id = pcx.company_id
        AND pcx.product_id = pm.product_id

        -- Check for additional zip code and codification availability
        AND 1 =
            --SELECT 1 FROM GLOBAL_PARMS WHERE GLOBAL_PARMS_KEY=-1
          (SELECT FTD_APPS.OE_PRODUCT_LIST_INDEX_AVAIL(
                 v_cszZip,
                 DECODE(px.UPSELL_MASTER_ID, NULL, pm.PRODUCT_ID, px.UPSELL_MASTER_ID),
                 v_zipGnaddFlag,
                 v_gnaddLevel,
                 v_zipFloralFlag,
                 v_stateId,
                 pm.product_type,
                 pm.ship_method_carrier,
                 pm.ship_method_florist,
                 IN_COUNTRY_ID) FROM DUAL)

        -- Do not return personalized products
        AND (pm.personalization_template_id = 'NONE' OR pm.personalization_template_id IS NULL)

        -- Sort by index display order
        ORDER BY px.display_order;

    RETURN cur_cursor;
END;
.
/
