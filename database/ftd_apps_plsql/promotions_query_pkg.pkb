CREATE OR REPLACE
PACKAGE BODY ftd_apps.PROMOTIONS_QUERY_PKG AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all FTD_APPS.SOURCE_MASTER
        records that have an AUTO_PROMOTION_ENGINE value of 'Y'

Input:
        N/A
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/

PROCEDURE GET_APE_MASTER_SOURCE_CODES
(
  OUT_CUR                           OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select source_code, price_header_id
        from source_master
        where auto_promotion_engine = 'Y';

END GET_APE_MASTER_SOURCE_CODES;

PROCEDURE GET_APE_BASE_SOURCE_CODES
(
  IN_MASTER_SOURCE_CODE             IN  APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
  OUT_CUR                           OUT TYPES.REF_CURSOR
) AS

    BEGIN

    OPEN OUT_CUR FOR
        select base_source_code
        from ape_base_source_codes
        where master_source_code = in_master_source_code;

END GET_APE_BASE_SOURCE_CODES;

PROCEDURE GET_SOURCE_MASTER_BY_ID
(
  IN_SOURCE_CODE                   IN  FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
  OUT_CUR                          OUT TYPES.REF_CURSOR
)
AS

CURSOR ship_fee_cursor IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'ORDER_SERVICE_CONFIG'
             and name = 'DISPLAY_SHIP_FEE';

CURSOR service_fee_cursor IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'ORDER_SERVICE_CONFIG'
             and name = 'DISPLAY_SERVICE_FEE';

v_ship_fee_flag         VARCHAR2(1);
v_service_fee_flag      VARCHAR2(1);

BEGIN

    -- Get the frp.global_parms
    OPEN ship_fee_cursor;
    FETCH ship_fee_cursor into v_ship_fee_flag;
    CLOSE ship_fee_cursor;

    OPEN service_fee_cursor;
    FETCH service_fee_cursor into v_service_fee_flag;
    CLOSE service_fee_cursor;

    OPEN OUT_CUR FOR
        SELECT upper(sm.source_code) source_code,
            sm.snh_id,
            sm.price_header_id,
            sm.company_id,
            sm.order_source,
            sm.related_source_code,
            discount_allowed_flag,
            iotw_flag,
            pp.program_type,
            pr.calculation_basis,
            pr.points,
            pr.bonus_calculation_basis,
            pr.bonus_points,
            pr.maximum_points,
            pr.reward_type,
            pmmp.payment_method_id,
            pmmp.dollar_to_mp_operator,
            pmmp.rounding_method_id,
            pmmp.rounding_scale_qty,
            mrr.mp_redemption_rate_amt,
            sm.end_date,
            CASE sm.display_service_fee_code WHEN 'D' THEN v_service_fee_flag ELSE sm.display_service_fee_code END as client_disp_svc_fee_flag,
            CASE sm.display_shipping_fee_code WHEN 'D' THEN v_ship_fee_flag ELSE sm.display_shipping_fee_code END as client_disp_shp_fee_flag,
            sm.start_date,
            sm.price_header_id pricing_code,
            sm.snh_id shipping_code,
            spr.program_name partner_id,
            sm.payment_method_id valid_pay_method,
            sm.jcpenney_flag,
            sm.send_to_scrub,
            cm.company_name,
            cm.internet_origin,
            sm.fraud_flag,
            sm.billing_info_logic,
            sm.emergency_text_flag,
            sm.requires_delivery_confirmation,
            sm.webloyalty_flag,
            (select sfp.florist_id from ftd_apps.source_florist_priority sfp where sfp.source_code = sm.source_code and sfp.priority = 1) as primary_florist,
            global.cache_pkg.get_source_florist_backup(sm.source_code) as backup_florists,
            global.cache_pkg.get_source_index_found(sm.source_code, 'limit_products') as source_code_has_limit_index,
            sm.allow_free_shipping_flag,
            pp.partner_name,
            sm.same_day_upcharge,
            sm.display_same_day_upcharge,
            sm.auto_promotion_engine,
            sm.ape_product_catalog
        FROM ftd_apps.source_master sm
        JOIN ftd_apps.company_master cm ON sm.company_id = cm.company_id
        LEFT OUTER JOIN ftd_apps.source_program_ref spr ON sm.source_code  = spr.source_code
            AND spr.start_date  = ftd_apps.get_src_prg_ref_max_start_date(sm.source_code, sysdate)
        LEFT OUTER JOIN ftd_apps.partner_program pp ON spr.program_name = pp.program_name
        LEFT OUTER JOIN ftd_apps.program_reward pr ON pr.program_name  = pp.program_name
        LEFT OUTER JOIN ftd_apps.miles_points_redemption_rate mrr on mrr.mp_redemption_rate_id = sm.mp_redemption_rate_id
        LEFT OUTER JOIN ftd_apps.payment_method_miles_points pmmp ON pmmp.payment_method_id = mrr.payment_method_id
        WHERE SM.SOURCE_CODE = IN_SOURCE_CODE;

END GET_SOURCE_MASTER_BY_ID;

PROCEDURE GET_ACTIVE_CATALOG_PRODUCTS
(
  IN_PRODUCT_CATALOG               IN  FTD_APPS.SOURCE_MASTER.APE_PRODUCT_CATALOG%TYPE,
  OUT_CUR                          OUT TYPES.REF_CURSOR
)
AS

BEGIN

    IF UPPER(IN_PRODUCT_CATALOG) = 'AMAZON' THEN
        OPEN OUT_CUR FOR
            select apm.product_id,
                pm.standard_price
            from ptn_amazon.az_product_master apm, ftd_apps.product_master pm
            where (apm.catalog_status_standard = 'A' or apm.catalog_status_deluxe = 'A' or apm.catalog_status_premium = 'A')
            and pm.product_id = apm.product_id
            order by apm.product_id;
    ELSE
        OPEN OUT_CUR FOR
            select * from dual
            where 1=0;
    END IF;

END GET_ACTIVE_CATALOG_PRODUCTS;

PROCEDURE GET_APE_IOTW_SOURCE_CODE
(
  IN_MASTER_SOURCE_CODE            IN  FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
  IN_PRICE_HEADER_ID               IN  FTD_APPS.SOURCE_MASTER.PRICE_HEADER_ID%TYPE,
  OUT_CUR                          OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select source_code,
            description,
            iotw_flag,
            price_header_id,
            auto_promotion_engine,
            ape_product_catalog
        from ftd_apps.source_master
        where description like 'APE ' || in_master_source_code || '%'
        and price_header_id = in_price_header_id
        and iotw_flag = 'Y';

END GET_APE_IOTW_SOURCE_CODE;

PROCEDURE GET_IOTW_WITH_APE_DISABLED
(
  OUT_CUR                          OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select i.iotw_id
        from joe.iotw i, ftd_apps.source_master sm
        where i.source_code = sm.source_code
        and i.created_by = 'APE'
        and sm.auto_promotion_engine = 'N';

END GET_IOTW_WITH_APE_DISABLED;

PROCEDURE GET_EXPIRED_IOTW
(
  OUT_CUR                          OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select i.iotw_id
        from joe.iotw i
        where i.end_date is not null
        and i.end_date < trunc(sysdate);

END GET_EXPIRED_IOTW;

PROCEDURE GET_FUTURE_IOTW
(
  IN_SOURCE_CODE                   IN  JOE.IOTW.SOURCE_CODE%TYPE,
  IN_PRODUCT_ID                    IN  JOE.IOTW.PRODUCT_ID%TYPE,
  IN_END_DATE                      IN  JOE.IOTW.END_DATE%TYPE,
  OUT_CUR                          OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select iotw_id,
            iotw_source_code,
            start_date,
            end_date,
            created_by
        from joe.iotw
        where source_code = in_source_code
        and product_id = in_product_id
        and created_by <> 'APE'
        and ((in_end_date is null and start_date > sysdate)
        or (start_date <= in_end_date))
        order by start_date;

END GET_FUTURE_IOTW;

END PROMOTIONS_QUERY_PKG;
.
/
