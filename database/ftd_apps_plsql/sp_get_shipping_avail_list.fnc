CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SHIPPING_AVAIL_LIST (typeCode in varchar2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SHIPPING_AVAIL_LIST
-- Type:    Function
-- Syntax:  SP_GET_SHIPPING_AVAIL_LIST (typeCode IN varchar2)
-- Returns: ref_cursor for
--          COUNTRY_TYPE_ID           VARCHAR2(1)
--          COUNTRY_TYPE_DESCRIPTION  VARCHAR2(25)
--
-- Description:   Queries the COUNTRY_TYPE table by type_code (CURRENTLY HARDCODED TO 'FLORAL') and
--                returns a ref cursor for resulting row.
--
--======================================================================
AS
    avail_cursor types.ref_cursor;
BEGIN
    IF typeCode = 'FLORAL' THEN
      OPEN avail_cursor FOR
        SELECT COUNTRY_TYPE_ID   as "countryTypeId",
        COUNTRY_TYPE_DESCRIPTION as "countryTypeDescription"
        FROM COUNTRY_TYPE;
    END IF;

    RETURN avail_cursor;
END
;
.
/
