CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_HOLIDAY_SKU_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_HOLIDAY_SKU_LIST
-- Type:    Function
-- Syntax:  SP_GET_HOLIDAY_SKU_LIST ()
-- Returns: ref_cursor for
--          PRODUCT_ID  VARCHAR2(10)
--          HOLIDAY_SKU VARCHAR2(6)
--
--
-- Description:   Queries the PRODUCT_MASTER table and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    sku_cursor types.ref_cursor;
BEGIN
    OPEN sku_cursor FOR
        SELECT PRODUCT_ID        as "productId",
               HOLIDAY_SKU       as "holidaySKU"
          FROM PRODUCT_MASTER
          where holiday_sku is not null;
    RETURN sku_cursor;
END
;
.
/
