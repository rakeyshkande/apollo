CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_CUSTOMER (customerId IN VARCHAR2,
 institutionName IN VARCHAR2,
 address1 IN VARCHAR2,
 address2 IN VARCHAR2,
 inCity IN VARCHAR2,
 inState IN VARCHAR2,
 inZipCode IN VARCHAR2,
 inPhone IN VARCHAR2)
  RETURN Types.ref_cursor
--======================================================================
--
-- Name:    OE_GET_CUSTOMER
-- Type:    Function
-- Syntax:  OE_GET_CUSTOMER (customerId IN VARCHAR2)
-- Returns: ref_cursor for
--          customerId    VARCHAR2(8)
--          firstName     VARCHAR2(30)
--          lastName      VARCHAR2(30)
--          address1      VARCHAR2(30)
--          address2      VARCHAR2(30)
--          city          VARCHAR2(30)
--          state         VARCHAR2(2)
--          zipCode       VARCHAR2(10)
--          homePhone     VARCHAR2(20)
--          workPhone     VARCHAR2(20)
--          faxNumber     VARCHAR2(10)
--          email         VARCHAR2(40)
--          workPhoneExt  VARCHAR2(10)
--          countryId     VARCHAR2(2)
--          addressType   VARCHAR2(1)
--          bfhName       VARCHAR2(40)
--          bfhInfo       VARCHAR2(15)
--          county        VARCHAR2(30)
--          promoFlag     VARCHAR2(1)
--
-- Description:   Queries the CUSTOMER table by customer ID.
--
--=======================================================================

AS
    cur_cursor		Types.ref_cursor;
BEGIN
    -- Determine whether this is a phone-number based search

	IF(inPhone = '0000000000' OR inPhone = '9999999999')
	THEN
	        OPEN cur_cursor FOR
	              SELECT CUSTOMER_ID AS "customerId",
              NLS_INITCAP(FIRST_NAME) AS "firstName",
              NLS_INITCAP(LAST_NAME) AS "lastName",
              NLS_INITCAP(ADDRESS_1) AS "address1",
              NLS_INITCAP(ADDRESS_2) AS "address2",
              NLS_INITCAP(CITY) AS "city",
              STATE AS "state",
              ZIP_CODE AS "zipCode",
              HOME_PHONE AS "homePhone",
              WORK_PHONE AS "workPhone",
              FAX_NUMBER AS "faxNumber",
              EMAIL AS "email",
              WORK_PHONE_EXT AS "workPhoneExt",
              COUNTRY_ID AS "countryId",
              ADDRESS_TYPE AS "addressType",
              BFH_NAME AS "bfhName",
              BFH_INFO AS "bfhInfo",
              NLS_INITCAP(COUNTY) AS "county",
			  SPCL_PROMO_FLAG AS "promoFlag"
        FROM CUSTOMER
        WHERE customer_id = 'X';
	ELSIF ( inPhone IS NOT NULL )
	THEN

      OPEN cur_cursor FOR
        SELECT CUSTOMER_ID AS "customerId",
              NLS_INITCAP(FIRST_NAME) AS "firstName",
              NLS_INITCAP(LAST_NAME) AS "lastName",
              NLS_INITCAP(ADDRESS_1) AS "address1",
              NLS_INITCAP(ADDRESS_2) AS "address2",
              NLS_INITCAP(CITY) AS "city",
              STATE AS "state",
              ZIP_CODE AS "zipCode",
              HOME_PHONE AS "homePhone",
              WORK_PHONE AS "workPhone",
              FAX_NUMBER AS "faxNumber",
              EMAIL AS "email",
              WORK_PHONE_EXT AS "workPhoneExt",
              COUNTRY_ID AS "countryId",
              ADDRESS_TYPE AS "addressType",
              BFH_NAME AS "bfhName",
              BFH_INFO AS "bfhInfo",
              NLS_INITCAP(COUNTY) AS "county",
			  SPCL_PROMO_FLAG AS "promoFlag"
        FROM CUSTOMER
        WHERE (( HOME_PHONE = Oe_Cleanup_Alphanum_String(inPhone,'N') )
          OR ( WORK_PHONE = Oe_Cleanup_Alphanum_String(inPhone,'N') ))
          AND ROWNUM < 101
        ORDER BY CUSTOMER_ID;

  ELSIF ( customerId IS NOT NULL )
	THEN

      OPEN cur_cursor FOR
        SELECT CUSTOMER_ID AS "customerId",
              NLS_INITCAP(FIRST_NAME) AS "firstName",
              NLS_INITCAP(LAST_NAME) AS "lastName",
              NLS_INITCAP(ADDRESS_1) AS "address1",
              NLS_INITCAP(ADDRESS_2) AS "address2",
              NLS_INITCAP(CITY) AS "city",
              STATE AS "state",
              ZIP_CODE AS "zipCode",
              HOME_PHONE AS "homePhone",
              WORK_PHONE AS "workPhone",
              FAX_NUMBER AS "faxNumber",
              EMAIL AS "email",
              WORK_PHONE_EXT AS "workPhoneExt",
              COUNTRY_ID AS "countryId",
              ADDRESS_TYPE AS "addressType",
              BFH_NAME AS "bfhName",
              BFH_INFO AS "bfhInfo",
              NLS_INITCAP(COUNTY) AS "county",
			  SPCL_PROMO_FLAG AS "promoFlag"
        FROM CUSTOMER
        WHERE ( CUSTOMER_ID = customerId )
        ORDER BY CUSTOMER_ID;


	ELSE

      OPEN cur_cursor FOR
        SELECT CUSTOMER_ID AS "customerId",
              NLS_INITCAP(FIRST_NAME) AS "firstName",
              NLS_INITCAP(LAST_NAME) AS "lastName",
              NLS_INITCAP(ADDRESS_1) AS "address1",
              NLS_INITCAP(ADDRESS_2) AS "address2",
              NLS_INITCAP(CITY) AS "city",
              STATE AS "state",
              ZIP_CODE AS "zipCode",
              HOME_PHONE AS "homePhone",
              WORK_PHONE AS "workPhone",
              FAX_NUMBER AS "faxNumber",
              EMAIL AS "email",
              WORK_PHONE_EXT AS "workPhoneExt",
              COUNTRY_ID AS "countryId",
              ADDRESS_TYPE AS "addressType",
              BFH_NAME AS "bfhName",
              BFH_INFO AS "bfhInfo",
              NLS_INITCAP(COUNTY) AS "county",
			  SPCL_PROMO_FLAG AS "promoFlag"
        FROM CUSTOMER
        WHERE ((( customerId IS NULL )
          OR ( CUSTOMER_ID = customerId ))
        AND (( address1 IS NULL )
          OR ( ADDRESS_1 LIKE UPPER(address1) || '%' ))
        AND (( inCity IS NULL )
          OR ( CITY LIKE UPPER(inCity) || '%' ))
        AND (( inState IS NULL )
          OR ( STATE = inState ))
        AND (( inZipCode IS NULL )
          OR ( ZIP_CODE LIKE Oe_Cleanup_Alphanum_String(inZipCode,'Y') || '%' )))
        AND ROWNUM < 101
        ORDER BY CUSTOMER_ID;

	END IF;

    RETURN cur_cursor;
END;
.
/
