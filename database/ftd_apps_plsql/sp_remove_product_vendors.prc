create or replace procedure ftd_apps.sp_remove_product_vendors(
       IN_PRODUCT_ID in varchar2) is
--==============================================================================
--
-- Name:    sp_remove_product_vendors
-- Type:    Function
-- Syntax:  sp_remove_product_vendors(IN_PRODUCT_ID IN VARCHAR2)
--
--          Marks records vendor_product records removed where the passed in
--          product id and any of it's subcodes
--==============================================================================
begin
    UPDATE vendor_product vp
           SET vp.available='N', vp.removed='Y'
    WHERE vp.product_subcode_id = IN_PRODUCT_ID OR
          vp.product_subcode_id IN (SELECT ps.product_subcode_id FROM product_subcodes ps WHERE ps.product_id = IN_PRODUCT_ID);
end sp_remove_product_vendors;
.
/
/
