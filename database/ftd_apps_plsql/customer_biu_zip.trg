CREATE OR REPLACE TRIGGER FTD_APPS.CUSTOMER_BIU_ZIPl
BEFORE INSERT OR UPDATE OF HOME_PHONE, WORK_PHONE, ZIP_CODE ON ftd_apps.CUSTOMER
FOR EACH ROW
BEGIN
    :new.ZIP_CODE := OE_CLEANUP_ALPHANUM_STRING(:new.ZIP_CODE, 'Y');
    :new.HOME_PHONE := OE_CLEANUP_ALPHANUM_STRING(:new.HOME_PHONE, 'N');
    :new.WORK_PHONE := OE_CLEANUP_ALPHANUM_STRING(:new.WORK_PHONE, 'N');
EXCEPTION WHEN OTHERS THEN
    NULL;
END;
.
/
