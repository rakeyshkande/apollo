CREATE OR REPLACE PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_ADDON  (
 IN_PRODUCT_ID        in PRODUCT_ADDON.PRODUCT_ID%TYPE,
 IN_ADDON_ID          in PRODUCT_ADDON.ADDON_ID%TYPE,
 IN_ACTIVE_FLAG       in PRODUCT_ADDON.ACTIVE_FLAG%TYPE,
 IN_DISPLAY_SEQ_NUM   in PRODUCT_ADDON.DISPLAY_SEQ_NUM%TYPE,
 IN_MAX_QTY           in PRODUCT_ADDON.MAX_QTY%TYPE,
 IN_UPDATED_BY        in PRODUCT_ADDON.UPDATED_BY%TYPE
)
AS
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_ADDONS
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_ADDONS ( <see args above> )
--
-- Description:   Attempts to insert a row into PRODUCT_ADDON.  If the row
--                already exists (by product ID and addon Id), then and update
--                of the existing row is performed
--
--==============================================================================
BEGIN

    UPDATE PRODUCT_ADDON
      SET PRODUCT_ID = IN_PRODUCT_ID,
          ADDON_ID = IN_ADDON_ID,
          ACTIVE_FLAG = IN_ACTIVE_FLAG,
          DISPLAY_SEQ_NUM = IN_DISPLAY_SEQ_NUM,
          MAX_QTY = IN_MAX_QTY,
          UPDATED_ON = sysdate,
          UPDATED_BY = IN_UPDATED_BY
       WHERE PRODUCT_ID = IN_PRODUCT_ID
          AND ADDON_ID = IN_ADDON_ID;
     
     IF SQL%NOTFOUND THEN
       INSERT INTO PRODUCT_ADDON (
          PRODUCT_ID,
          ADDON_ID,
          ACTIVE_FLAG,
          DISPLAY_SEQ_NUM,
          MAX_QTY,
          CREATED_ON,
          CREATED_BY,
          UPDATED_ON,
          UPDATED_BY
          )
        VALUES (
          IN_PRODUCT_ID,
          IN_ADDON_ID,
          IN_ACTIVE_FLAG,
          IN_DISPLAY_SEQ_NUM,
          IN_MAX_QTY,
          sysdate,
          IN_UPDATED_BY,
          sysdate,
          IN_UPDATED_BY
        );
     END IF;

END;
.
/