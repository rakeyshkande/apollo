CREATE OR REPLACE
FUNCTION ftd_apps.UNIX2ORA ( pUnixDate in number,
    pTimeZone in varchar2 default 'GMT'
  ) return date
as
begin
	return new_time( to_date( '01-JAN-1970', 'DD-MON-YYYY' )
		+ ( pUnixDate / ( 1000*24*60*60 ) ), 'GMT', pTimeZone );
end unix2ora;
.
/
