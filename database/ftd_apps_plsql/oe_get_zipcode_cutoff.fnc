CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIPCODE_CUTOFF (
    inZip    IN VARCHAR2,
    inProdId IN VARCHAR2 )
  RETURN types.ref_cursor

AS
    cur_cursor          types.ref_cursor;
    --zipCutoff           CSZ_AVAIL.LATEST_CUTOFF%TYPE := NULL;
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
    cszZip              VARCHAR2(6);
    cleanZip            VARCHAR2(6) := Oe_Cleanup_Alphanum_String(UPPER(inZip), 'Y');

BEGIN
    -- Determine if the input zip code is Canadian
    IF ( TRANSLATE(cleanZip, alphaChars || numChars, alphaSub || numSub) LIKE '@#@%' )
    THEN
        -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
        cszZip := SUBSTR(cleanZip,1,3);
    ELSE
        cszZip := cleanZip;
    END IF;

    IF ( inProdId IS NULL )
    THEN
        -- No product ID, so get cutoff for zipcode.
        -- Could actually just use query below instead, but this saves us a join.
        BEGIN
            OPEN cur_cursor FOR
                SELECT CA.LATEST_CUTOFF AS ZIPCUTOFF
                FROM
                CSZ_AVAIL CA
                WHERE CA.CSZ_ZIP_CODE=cszZip;

            RETURN cur_cursor;
        END;
    ELSE
        -- Product ID was set, so join on csz_products in case codified product
        BEGIN
            -- Codified product cutoff takes precedence
            OPEN cur_cursor FOR
                SELECT NVL(CP.PRODUCT_CUTOFF, CA.LATEST_CUTOFF) AS ZIPCUTOFF
                FROM
                CSZ_AVAIL CA
                LEFT JOIN CSZ_PRODUCTS CP ON
                CA.CSZ_ZIP_CODE=CP.ZIP_CODE AND CP.PRODUCT_ID=inProdId
                WHERE CA.CSZ_ZIP_CODE=cszZip;

            RETURN cur_cursor;
        END;
    END IF;

END;
.
/
