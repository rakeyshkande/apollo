CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_OCCASION_LIST RETURN types.ref_cursor

 --==============================================================================
--
-- Name:    SP_GET_OCCASION_LIST
-- Type:    Function
-- Syntax:  SP_GET_OCCASION_LIST ( )
-- Returns: ref_cursor for
--          OCCASION_ID     NUMBER
--          DESCRIPTION     VARCHAR2(30)
--          DISPLAY_ORDER   NUMBER
--          LOCKED          CHAR(1)
--          ACTIVE          CHAR(1)
--			FTDWEST_ID		NUMBER
--
-- Description:   Queries the OCCASION table and returns a ref cursor for row.
--
--===========================================================

AS
    occasion_cursor types.ref_cursor;
BEGIN
    OPEN occasion_cursor FOR
        SELECT OCCASION_ID as "occasionId",
               DESCRIPTION as "description",
               DISPLAY_ORDER as "displayOrder",              
	       		ACTIVE as "active",
	       		USER_EDITABLE as "userEditable",
	       		FTDWEST_ID as "ftdWestOccasionId"
        FROM OCCASION
		ORDER BY DISPLAY_ORDER;

    RETURN occasion_cursor;
END
;
.
/
