CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_VENDOR_SHIPBLOCK_START (vendorId IN varchar2,
   shipBlockRank IN NUMBER)
    RETURN DATE
--=========================================================================
--
-- Name:    OE_GET_VENDOR_SHIPBLOCK_START
-- Type:    Function
-- Syntax:  OE_GET_VENDOR_SHIPBLOCK_START(vendorId IN VARCHAR2,
--                                      shipBlockRank IN NUMBER)
-- Returns: tmp_blockstart    DATE
--
-- Description:   Gets block start date for product shipment by vendor,
--                ordered by start date of the start / end date range.
--
--=========================================================================
AS
  tmp_blockstart    DATE;
BEGIN
    -- Vendor ship block list view lists start / end date ranges
    --   for blocked vendor shipment of products, ordered by start date
    SELECT start_date
    INTO tmp_blockstart
    FROM VENDOR_SHIP_BLOCK_LIST_VW
    WHERE vendor_id = vendorId
    AND ship_block_rank = shipBlockRank;

    RETURN tmp_blockstart;

EXCEPTION WHEN OTHERS THEN
    RETURN null;
END
;
.
/
