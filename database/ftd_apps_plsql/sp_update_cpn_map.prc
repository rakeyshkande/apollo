CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_CPN_MAP (
    sourceCodeIn in varchar2,
	productIDIn in varchar2,
    cpnIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_DELETE_CPN_MAP
-- Type:    Procedure
-- Syntax:  SP_DELETE_CPN_MAP ( asnNumberIn in varchar2,
--                                    clientNameIn in varchar2,
--                                      urlIn in varchar2)
--
-- Description:   Inserts a row to the CLIENT_URL table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 UPDATE CPN_MAP
   SET CPN = cpnIn
 WHERE SOURCE_CODE = sourceCodeIn
                AND PRODUCT_ID = productIDIn;

end
;
.
/
