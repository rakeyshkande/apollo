CREATE OR REPLACE
PROCEDURE ftd_apps.PRODUCT_HP_UPDATE AS

 CURSOR select_ids IS
   SELECT pm.product_id
     FROM product_master pm
     WHERE pm.company_id = 'SFMB'
       AND pm.status = 'A';

 BEGIN

  dbms_output.ENABLE(30000);
  dbms_output.put_line('-');

  FOR v_row IN select_ids LOOP

    insert into product_hp (product_id,ship_dates)
       values (v_row.product_id,'YYYYYNN');
    commit;

    dbms_output.put_line (v_row.product_id);

  END LOOP;

END
;
.
/
