create or replace
PACKAGE BODY          FTD_APPS.PRODUCT_MAPPING_PKG AS

PROCEDURE DELETE_PRODUCT_MAPPING
(
  IN_ORIGINAL_PRODUCT_ID    IN FTD_APPS.PHOENIX_PRODUCT_MAPPING.ORIGINAL_PRODUCT_ID%TYPE,
  OUT_STATUS                OUT VARCHAR2,
  OUT_MESSAGE               OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure deletes row in table PHOENIX_PRODUCT_MAPPING

Input:
        vendor_id        VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

  DELETE FTD_APPS.PHOENIX_PRODUCT_MAPPING
   WHERE ORIGINAL_PRODUCT_ID = UPPER(IN_ORIGINAL_PRODUCT_ID);

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N/A';  -- Using N/A so we can easily distinguish between this and actual error
    out_message := 'WARNING: No records found for this original product : ' || IN_ORIGINAL_PRODUCT_ID ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END DELETE_PRODUCT_MAPPING;



PROCEDURE GET_PRODUCT_MAPPING
(
  IN_ORIGINAL_PRODUCT_ID    IN FTD_APPS.PHOENIX_PRODUCT_MAPPING.ORIGINAL_PRODUCT_ID%TYPE,
  OUT_CUR                   OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Gets data from PHOENIX_PRODUCT_MAPPING for single product id
------------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
   SELECT pm.Product_Id, pm.Status, pm.Ship_Method_Carrier, pm.Ship_Method_Florist, 
       pm.Standard_Price As Good, pm.Deluxe_Price As Better, pm.Premium_Price As Best, pm.product_type, 
       NVL((SELECT 'Y' FROM ftd_apps.addon a WHERE pm.product_id = a.product_Id),'N') as addon_flag,
       (SELECT  
          LISTAGG (ppm.product_price_point_key || ' , ' || ppm.New_Product_Id, ' | ') 
          WITHIN GROUP (Order By decode(ppm.Product_Price_Point_key,'1','GOOD','2','BETTER','3','BEST')) as price_points 
          FROM FTD_APPS.phoenix_product_mapping ppm 
          WHERE ppm.original_product_id = UPPER(IN_ORIGINAL_PRODUCT_ID)
          GROUP BY ppm.original_product_id
       ) as price_point_mappings
       From Ftd_Apps.Product_Master pm
       Where pm.Product_Id = UPPER(IN_ORIGINAL_PRODUCT_ID);
   
END GET_PRODUCT_MAPPING;



PROCEDURE GET_PRODUCT_MAPPINGS
(
OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Gets all data from PHOENIX_PRODUCT_MAPPING
------------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
   SELECT ppm.original_product_id, 
          LISTAGG (ppm.PRODUCT_PRICE_POINT_KEY || ' , ' || ppm.New_Product_Id, ' | ') 
          WITHIN GROUP (Order By decode(ppm.Product_Price_Point_key,'1','GOOD','2','BETTER','3','BEST')) as price_points, 
          NVL((SELECT 'Y' FROM  ftd_apps.addon a WHERE a.product_id = ppm.original_product_id),'N') as addon_flag
   FROM   FTD_APPS.phoenix_product_mapping ppm
   GROUP BY ppm.original_product_id order by original_product_id;


END GET_PRODUCT_MAPPINGS;



PROCEDURE ADD_PRODUCT_MAPPING
(
IN_ORIGINAL_PRODUCT_ID      IN FTD_APPS.PHOENIX_PRODUCT_MAPPING.ORIGINAL_PRODUCT_ID%TYPE,
IN_NEW_PRODUCT_ID           IN FTD_APPS.PHOENIX_PRODUCT_MAPPING.NEW_PRODUCT_ID%TYPE,
IN_CSR_ID                   IN FTD_APPS.Phoenix_Product_Mapping.Created_By%Type,
IN_PRODUCT_PRICE_POINT_KEY  IN FTD_APPS.Phoenix_Product_Mapping.PRODUCT_PRICE_POINT_KEY%TYPE,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Used for adding or updating PHOENIX_PRODUCT_MAPPING
------------------------------------------------------------------------------*/
BEGIN
    INSERT INTO ftd_apps.phoenix_product_mapping 
                (original_product_id, new_product_id, created_by, created_on, updated_by, updated_on, product_price_point_key)
         VALUES
                (UPPER(in_original_product_id), UPPER(in_new_product_id), in_csr_id, sysdate, in_csr_id, sysdate, IN_PRODUCT_PRICE_POINT_KEY);
    OUT_STATUS := 'Y';

EXCEPTION 
WHEN DUP_VAL_ON_INDEX THEN
   BEGIN
      UPDATE ftd_apps.phoenix_product_mapping
         SET new_product_id = UPPER(IN_NEW_PRODUCT_ID),
             updated_by = IN_CSR_ID,
             updated_on = SYSDATE
       WHERE original_product_id = UPPER(IN_ORIGINAL_PRODUCT_ID)
         AND product_price_point_key = IN_PRODUCT_PRICE_POINT_KEY;
   END;
     
WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END ADD_PRODUCT_MAPPING;



PROCEDURE GET_SHIP_METHOD_BY_SKUS
(
IN_ORIGINAL_PRODUCT_ID          IN  VARCHAR2,
IN_NEW_PRODUCT_ID               IN  VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
v_Sql         VARCHAR2(4000);
BEGIN

v_sql := 'select product_id, ship_method_florist, ship_method_carrier from ftd_apps.product_master where product_id in ('''||in_original_product_id||''','''||in_new_product_id||''') and status = ''A''';
OPEN OUT_CUR FOR v_sql;
END GET_SHIP_METHOD_BY_SKUS;

PROCEDURE GET_PHOENIX_EMAIL_TYPES
(
OUT_CUR               OUT TYPES.REF_CURSOR
)
AS
v_sql         varchar2(4000);
BEGIN
v_sql := 'select email_type_key, email_type_name from ftd_apps.phoenix_email_types where display_status_flag=''Y'' order by display_order';
OPEN OUT_CUR FOR v_sql;
END GET_PHOENIX_EMAIL_TYPES;

FUNCTION IS_PRODUCT_VENDOR_MAPPABLE
(
 IN_PRODUCT_ID               IN VARCHAR2,
 IN_IS_ADDON                 IN VARCHAR2
)
 RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
   Determines if product can be used as Phoenix Mapping product.
   Also confirms if product is addon or not based on in_is_addon (Y or N)
   Returns Y, N (or N/A if product not found).
-----------------------------------------------------------------------------*/
v_flag VARCHAR2(5);

BEGIN

  BEGIN
    -- Ship_method_carrier column will reflect if vendor product or not
    SELECT ship_method_carrier INTO v_flag
      FROM ftd_apps.product_master
     WHERE product_id = UPPER(IN_PRODUCT_ID);

    EXCEPTION WHEN NO_DATA_FOUND THEN v_flag := 'N/A';
  END;

  BEGIN
    -- As long as vendor product exists, check if product is addon or not based on passed flag
    IF v_flag='Y' THEN
       SELECT DECODE(IN_IS_ADDON,
              (CASE (SELECT COUNT(1) FROM ftd_apps.addon WHERE product_id = UPPER(IN_PRODUCT_ID)) WHEN 0 THEN 'N' ELSE 'Y' END), 
              'Y', 'N')
         INTO v_flag
         FROM DUAL;
    END IF;
    
    EXCEPTION WHEN NO_DATA_FOUND THEN v_flag := 'N/A';
  END;

  RETURN v_flag;

END IS_PRODUCT_VENDOR_MAPPABLE;

END;
.
/
