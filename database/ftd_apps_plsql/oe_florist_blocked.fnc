CREATE OR REPLACE
FUNCTION ftd_apps.OE_FLORIST_BLOCKED (
 floristId IN VARCHAR2
 )
  RETURN VARCHAR2
as
    ret                 VARCHAR2(1);
    block_ts            DATE := SYSDATE;

begin

-- CR 11/2004 - changed for florist maintenance data model changes
-- use florist_master.status to check if the florist is currently blocked
/* old code
    SELECT DECODE(COUNT(*), 1, 'Y', 0, 'N')
    INTO ret
    FROM FLORIST_BLOCKS
    WHERE FLORIST_ID = floristId
    AND (block_ts > BLOCK_START_DATE AND block_ts < BLOCK_END_DATE);
*/

    SELECT DECODE(COUNT(*), 1, 'Y', 0, 'N')
    INTO ret
    FROM FLORIST_MASTER
    WHERE FLORIST_ID = floristId
    AND STATUS IN ('Blocked', 'Mercury Suspend', 'GoTo Suspend');

    return ret;
end;
.
/
