CREATE OR REPLACE
TRIGGER ftd_apps.product_master_upsell_trg
AFTER UPDATE ON ftd_apps.product_master
REFERENCING new as new old as old
FOR EACH ROW

--
-- This is needed for Project Fresh - when a product changes we want to trigger feeds 
-- for any upsell master that is associated with that product.  
-- Note the trigger for the product feed itself is in the product_master trigger.
--

DECLARE

v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_payload varchar2(2000);
v_exception     EXCEPTION;
v_exception_txt varchar2(200);
v_trigger_enabled varchar2(10) := 'N';

    CURSOR upsell_master_cur(in_product_id IN varchar2) IS
      select distinct um.upsell_master_id
      from   ftd_apps.upsell_master um
      join   ftd_apps.upsell_detail ud on ud.upsell_master_id = um.upsell_master_id
      where  um.upsell_status = 'Y'
      and    ud.upsell_detail_id = in_product_id;

BEGIN

     FOR product_rec in upsell_master_cur(:new.product_id) LOOP

      -- Post a message with 1 minute delay
      SELECT value INTO v_trigger_enabled FROM frp.global_parms WHERE context='SERVICE' AND name='FRESH_TRIGGERS_ENABLED';
      IF (v_trigger_enabled = 'Y') THEN 
         v_payload := 'FRESH_MSG_PRODUCER|product|product_master_upsell|' || product_rec.upsell_master_id;
         events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 60, v_stat, v_mess);      
         IF v_stat = 'N' THEN
            v_exception_txt := 'ERROR posting a JMS message in PDB for Fresh product upsell = ' || product_rec.upsell_master_id || SUBSTR(v_mess,1,200);
            raise v_exception;
         END IF;
      END IF;

     END LOOP;

END PRODUCT_MASTER_UPSELL_TRG;
/