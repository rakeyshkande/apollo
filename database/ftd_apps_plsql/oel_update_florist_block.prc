CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_UPDATE_FLORIST_BLOCK ( floristId in varchar2,
 blockType in varchar2,
 blockStartDate in date,
 blockEndDate in date)

as
--==============================================================================
--
-- Name:    OEL_UPDATE_FLORIST_BLOCK
-- Type:    Procedure
-- Syntax:  OEL_UPDATE_FLORIST_BLOCK ( floristId in varchar2,
--                                     blockType in varchar2,
--                                     blockStartDate in date,
--                                     blockEndDate in date)
--
-- Description:   Attempts to insert a row into FLORIST_BLOCKS.  If the row
--                already exists (by floristId), then updates the existing row.
--
--==============================================================================

   v_system_message_id number;
   v_out_status varchar2(1);
   v_message varchar2(500);

begin

   frp.misc_pkg.insert_system_messages(
      in_source             => 'oel_update_florist_block',
      in_type               => 'INFO',
      in_message            => 'OE XML Listener is active',
      in_computer           => sys_context('USERENV','HOST'),
      out_system_message_id => v_system_message_id,
      out_status            => v_out_status,
      out_message           => v_message);

  -- Attempt to insert into FLORIST_BLOCKS table
  INSERT INTO FLORIST_BLOCKS
    ( FLORIST_ID,
      BLOCK_TYPE,
      BLOCK_START_DATE,
      BLOCK_END_DATE
    )
  VALUES
    ( floristId,
      blockType,
      blockStartDate,
      blockEndDate
    );

  COMMIT;

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

  -- If a row already exists for this FLORIST_BLOCKS then update
  UPDATE FLORIST_BLOCKS
    SET BLOCK_START_DATE = blockStartDate,
      BLOCK_END_DATE = blockEndDate
    WHERE FLORIST_ID = floristId AND BLOCK_TYPE = blockType;

  COMMIT;
end
;
.
/
