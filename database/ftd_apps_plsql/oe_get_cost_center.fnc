CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_COST_CENTER (
  inCostCenterId IN VARCHAR2, inPartnerId IN VARCHAR2, inSourceCode IN VARCHAR2
)
RETURN types.ref_cursor
--=================================================================
--
-- Name:    OE_GET_COST_CENTER
-- Type:    Function
-- Syntax:  OE_GET_COST_CENTER (inCostCenterId IN VARCHAR2,
--                              inPartnerId IN VARCHAR2,
--                              inSourceCode IN VARCHAR2)
-- Returns: ref_cursor for
--          costCenterId       VARCHAR2
--
-- Description:  Used to validate Cost Center IDs (for ADVO, Target, etc.).
--               Note that COST_CENTERS rows with NULL source_code's represent
--               cost_center_id's that apply to all source_codes for that
--               partner (e.g., ADVO).
--
--==================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT COST_CENTER_ID AS "costCenterId"
        FROM COST_CENTERS
        WHERE COST_CENTER_ID = inCostCenterId
        AND   PARTNER_ID = inPartnerId
        AND   NVL(SOURCE_CODE_ID,'NVL') = NVL(inSourceCode,'NVL');

    RETURN cur_cursor;
END;
.
/
