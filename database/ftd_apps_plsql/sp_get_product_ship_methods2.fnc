CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_SHIP_METHODS2 (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_SHIP_METHODS2
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_SHIP_METHODS2 (productId IN varchar2 )
-- Returns: ref_cursor for
--          SHIP_METHOD_ID  VARCHAR2(10)
--
-- Description:   Queries the PRODUCT_SHIP_METHODS table by PRODUCT_ID
--                returning a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT p.SHIP_METHOD_ID as "shipMethodId",
                 p.DEFAULT_CARRIER as "defaultcarrier",
                 s.NOVATOR_TAG as "novatorTag"
            FROM FTD_APPS.PRODUCT_SHIP_METHODS p
        JOIN FTD_APPS.SHIP_METHODS s on s.SHIP_METHOD_ID=p.SHIP_METHOD_ID
            WHERE p.PRODUCT_ID = productId;
    RETURN cur_cursor;
END
;
.
/
