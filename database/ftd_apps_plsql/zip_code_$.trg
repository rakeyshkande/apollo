CREATE OR REPLACE TRIGGER ftd_apps.zip_code_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.zip_code
REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
   v_current_timestamp TIMESTAMP;
BEGIN
   v_current_timestamp := CURRENT_TIMESTAMP;

   IF INSERTING THEN
      INSERT INTO ftd_apps.zip_code$ (
         zip_code_id,
         city,
         state_id,
         oscar_zip_flag,
         oscar_city_state_flag,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) VALUES (
         :NEW.zip_code_id,
         :NEW.city,
         :NEW.state_id,
         :NEW.oscar_zip_flag,
         :NEW.oscar_city_state_flag,
         :NEW.created_on,
         :NEW.created_by,
         :NEW.updated_on,
         :NEW.updated_by,
         'INS', v_current_timestamp);

   ELSIF UPDATING THEN
      INSERT INTO ftd_apps.zip_code$ (
         zip_code_id,
         city,
         state_id,
         oscar_zip_flag,
         oscar_city_state_flag,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) VALUES (
         :OLD.zip_code_id,
         :OLD.city,
         :OLD.state_id,
         :OLD.oscar_zip_flag,
         :OLD.oscar_city_state_flag,
         :OLD.created_on,
         :OLD.created_by,
         :OLD.updated_on,
         :OLD.updated_by,
         'UPD_OLD', v_current_timestamp);

      INSERT INTO ftd_apps.zip_code$ (
         zip_code_id,
         city,
         state_id,
         oscar_zip_flag,
         oscar_city_state_flag,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) VALUES (
         :NEW.zip_code_id,
         :NEW.city,
         :NEW.state_id,
         :NEW.oscar_zip_flag,
         :NEW.oscar_city_state_flag,
         :NEW.created_on,
         :NEW.created_by,
         :NEW.updated_on,
         :NEW.updated_by,
         'UPD_NEW', v_current_timestamp);

   ELSIF DELETING THEN
      INSERT INTO ftd_apps.zip_code$ (
         zip_code_id,
         city,
         state_id,
         oscar_zip_flag,
         oscar_city_state_flag,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) VALUES (
         :OLD.zip_code_id,
         :OLD.city,
         :OLD.state_id,
         :OLD.oscar_zip_flag,
         :OLD.oscar_city_state_flag,
         :OLD.created_on,
         :OLD.created_by,
         :OLD.updated_on,
         :OLD.updated_by,
         'DEL', v_current_timestamp);

   END IF;
END;
/
