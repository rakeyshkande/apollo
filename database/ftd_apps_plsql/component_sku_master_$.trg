CREATE OR REPLACE TRIGGER FTD_APPS.COMPONENT_SKU_MASTER_$
AFTER INSERT OR UPDATE OR DELETE ON FTD_APPS.COMPONENT_SKU_MASTER 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.component_sku_master$ (
         COMPONENT_SKU_ID,           
         COMPONENT_SKU_NAME,         
         BASE_COST_DOLLAR_AMT,       
         BASE_COST_EFF_DATE,         
         QA_COST_DOLLAR_AMT,         
         QA_COST_EFF_DATE,           
         FREIGHT_IN_COST_DOLLAR_AMT, 
         FREIGHT_IN_COST_EFF_DATE,   
         AVAILABLE_FLAG,             
         COMMENT_TEXT,             
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :NEW.COMPONENT_SKU_ID,           
         :NEW.COMPONENT_SKU_NAME,         
         :NEW.BASE_COST_DOLLAR_AMT,       
         :NEW.BASE_COST_EFF_DATE,         
         :NEW.QA_COST_DOLLAR_AMT,         
         :NEW.QA_COST_EFF_DATE,           
         :NEW.FREIGHT_IN_COST_DOLLAR_AMT, 
         :NEW.FREIGHT_IN_COST_EFF_DATE,   
         :NEW.AVAILABLE_FLAG,             
         :NEW.COMMENT_TEXT,             
         :NEW.created_by,                 
         :NEW.created_on,                 
         :NEW.updated_by,                  
         :NEW.updated_on,                 
          'INS',SYSTIMESTAMP);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.component_sku_master$ (
         COMPONENT_SKU_ID,           
         COMPONENT_SKU_NAME,         
         BASE_COST_DOLLAR_AMT,       
         BASE_COST_EFF_DATE,         
         QA_COST_DOLLAR_AMT,         
         QA_COST_EFF_DATE,           
         FREIGHT_IN_COST_DOLLAR_AMT, 
         FREIGHT_IN_COST_EFF_DATE,   
         AVAILABLE_FLAG,             
         COMMENT_TEXT,             
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :OLD.COMPONENT_SKU_ID,           
         :OLD.COMPONENT_SKU_NAME,         
         :OLD.BASE_COST_DOLLAR_AMT,       
         :OLD.BASE_COST_EFF_DATE,         
         :OLD.QA_COST_DOLLAR_AMT,         
         :OLD.QA_COST_EFF_DATE,           
         :OLD.FREIGHT_IN_COST_DOLLAR_AMT, 
         :OLD.FREIGHT_IN_COST_EFF_DATE,   
         :OLD.AVAILABLE_FLAG,             
         :OLD.COMMENT_TEXT,                 
         :OLD.created_by,                 
         :OLD.created_on,                 
         :OLD.updated_by,                 
         :OLD.updated_on,                 
         'UPD_OLD',SYSTIMESTAMP);

      INSERT INTO ftd_apps.component_sku_master$ (
         COMPONENT_SKU_ID,           
         COMPONENT_SKU_NAME,         
         BASE_COST_DOLLAR_AMT,       
         BASE_COST_EFF_DATE,         
         QA_COST_DOLLAR_AMT,         
         QA_COST_EFF_DATE,           
         FREIGHT_IN_COST_DOLLAR_AMT, 
         FREIGHT_IN_COST_EFF_DATE,   
         AVAILABLE_FLAG,             
         COMMENT_TEXT,             
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :NEW.COMPONENT_SKU_ID,           
         :NEW.COMPONENT_SKU_NAME,         
         :NEW.BASE_COST_DOLLAR_AMT,       
         :NEW.BASE_COST_EFF_DATE,         
         :NEW.QA_COST_DOLLAR_AMT,         
         :NEW.QA_COST_EFF_DATE,           
         :NEW.FREIGHT_IN_COST_DOLLAR_AMT, 
         :NEW.FREIGHT_IN_COST_EFF_DATE,   
         :NEW.AVAILABLE_FLAG,             
         :NEW.COMMENT_TEXT,                 
         :NEW.created_by,                 
         :NEW.created_on,                 
         :NEW.updated_by,                 
         :NEW.updated_on,                 
         'UPD_NEW',SYSTIMESTAMP);


   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.component_sku_master$ (
         COMPONENT_SKU_ID,           
         COMPONENT_SKU_NAME,         
         BASE_COST_DOLLAR_AMT,       
         BASE_COST_EFF_DATE,         
         QA_COST_DOLLAR_AMT,         
         QA_COST_EFF_DATE,           
         FREIGHT_IN_COST_DOLLAR_AMT, 
         FREIGHT_IN_COST_EFF_DATE,   
         AVAILABLE_FLAG,             
         COMMENT_TEXT,             
         created_by,                 
         created_on,                 
         updated_by,                 
         updated_on,                 
         operation$, timestamp$
      ) VALUES (
         :OLD.COMPONENT_SKU_ID,           
         :OLD.COMPONENT_SKU_NAME,         
         :OLD.BASE_COST_DOLLAR_AMT,       
         :OLD.BASE_COST_EFF_DATE,         
         :OLD.QA_COST_DOLLAR_AMT,         
         :OLD.QA_COST_EFF_DATE,           
         :OLD.FREIGHT_IN_COST_DOLLAR_AMT, 
         :OLD.FREIGHT_IN_COST_EFF_DATE,   
         :OLD.AVAILABLE_FLAG,             
         :OLD.COMMENT_TEXT,                 
         :OLD.created_by,                 
         :OLD.created_on,                 
         :OLD.updated_by,                 
         :OLD.updated_on,                 
         'DEL',SYSTIMESTAMP);

   END IF;

END;
/
