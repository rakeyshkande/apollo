CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_UPSELL_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_UPSELL_LIST
-- Type:    Function
-- Syntax:  SP_GET_UPSELL_LIST ()
-- Returns: ref_cursor for
--
-- Description:   Queries the UPSELL_MASTER table
--
--=========================================================

AS
    list_cursor types.ref_cursor;
BEGIN
    OPEN list_cursor FOR
        SELECT UPSELL_MASTER_ID            as "masterID",
               UPSELL_NAME          as "masterName",
               UPSELL_DESCRIPTION                as "masterDescription",
               UPSELL_STATUS  as "masterStatus",
               GBB_POPOVER_FLAG as "gbbPopoverFlag",
               GBB_TITLE_TXT as "gbbTitle"
          FROM UPSELL_MASTER
          ORDER BY UPSELL_MASTER_ID;

    RETURN list_cursor;
END;
.
/
