CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIPCODE_INFO (inZipCode IN VARCHAR2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_ZIPCODE_INFO
-- Type:    Function
-- Syntax:  OE_GET_ZIPCODE_INFO (inZipCode IN VARCHAR2)
-- Returns: ref_cursor for
--          zipGnaddFlag        VARCHAR2(1)
--          zipGotoFloristFlag  VARCHAR2(1)
--          zipSundayFlag       VARCHAR2(1)
--          timeZone            VARCHAR2(2)
--          countryCode         VARCHAR2(3)
--
-- Description:   Queries the FLORIST_ZIPS, FLORIST_MASTER, STATE_MASTER
--                ZIP_CODE and CSZ_AVAIL tables to collect information
--                about the input zip code related to GNADD and cutoff.
--
--==============================================================================
AS
    cur_cursor          types.ref_cursor;
    zipGnaddFlag        CSZ_AVAIL.GNADD_FLAG%TYPE := NULL;
	zipSundayFlag       VARCHAR2(1) := NULL;
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
    cszZip              VARCHAR2(10);
    tmpZip              VARCHAR2(10) := UPPER(SUBSTR(inZipCode,1,5));
    block_ts            DATE := SYSDATE;
BEGIN
    -- if null is passed in then change it to 99999 so we don't return every row
    -- in the zip_code table
    tmpZip := NVL(tmpZip, '99999') ;

    -- Determine if the input zip code is Canadian
    IF ( TRANSLATE(tmpZip, alphaChars || numChars, alphaSub || numSub) LIKE '@#@%' )
    THEN
        -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
        cszZip := SUBSTR(tmpZip,1,3);
        -- Use the first three chars for all canadian lookups
        tmpZip := cszZip;
    ELSE
        cszZip := tmpZip;
    END IF;

    -- Select the GNADD flag for this zip, if available
    BEGIN
        -- Assume NULL GNADD flag indicates GNADD is off
        SELECT NVL(GNADD_FLAG, 'N')
        INTO zipGnaddFlag
        FROM CSZ_AVAIL
        WHERE CSZ_ZIP_CODE = cszZip;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        -- If no row found on CSZ_AVAIL, GNADD is on for the zip
        zipGnaddFlag := 'Y';
    END;

    zipSundayFlag := OE_GET_ZIP_SUNDAY_FLAG(cszZip);

    OPEN cur_cursor FOR
        SELECT zipGnaddFlag as "zipGnaddFlag",
            DECODE(NVL(gc.GOTO_COUNT,1), 0, 'N', 'Y') as "zipGotoFloristFlag",
            zipSundayFlag as "zipSundayFlag",
            sm.TIME_ZONE as "timeZone",
            sm.STATE_MASTER_ID as "stateId",
            NVL(sm.COUNTRY_CODE, 'US') as "countryCode"
        FROM ZIP_CODE zc, STATE_MASTER sm,

              -- Inline view computes count of goto florists for supplied zip
              ( SELECT

                -- Count GOTO florists, excluding blocked florists ONLY
                --   if GNADD level 2 is in effect
                COUNT(DECODE(gp.GNADD_LEVEL, 2,
                          DECODE(fb.BLOCK_START_DATE, NULL,
                              DECODE(fm.SUPER_FLORIST_FLAG,'Y',1)),
                          DECODE(fm.SUPER_FLORIST_FLAG,'Y',1))) as GOTO_COUNT

                FROM FLORIST_ZIPS fz, FLORIST_MASTER fm,
                      FLORIST_BLOCKS fb, GLOBAL_PARMS gp
                WHERE fz.ZIP_CODE = tmpZip
                AND fz.FLORIST_ID = fm.FLORIST_ID
                AND fz.FLORIST_ID = fb.FLORIST_ID (+)
                AND block_ts >= fb.BLOCK_START_DATE (+)
                AND block_ts <= fb.BLOCK_END_DATE (+)
              ) gc

        WHERE ZC.ZIP_CODE_ID LIKE CONCAT(tmpZip, '%')
          AND ZC.STATE_ID = SM.STATE_MASTER_ID;
    RETURN cur_cursor;
END;
.
/
