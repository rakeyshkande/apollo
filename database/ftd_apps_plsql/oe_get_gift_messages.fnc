CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_GIFT_MESSAGES RETURN types.ref_cursor
--==========================================================================
--
-- Name:    OE_GET_GIFT_MESSAGES
-- Type:    Function
-- Syntax:  OE_GET_GIFT_MESSAGES()
-- Returns: ref_cursor for
--          giftMessageId       NUMBER(9)
--          messageText         VARCHAR2(50)
--
-- Description:   Queries the GIFT_MESSAGES table and returns all rows.
--
--==========================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT GIFT_MESSAGE_ID as "giftMessageId",
            MESSAGE_TEXT as "messageText"
        FROM GIFT_MESSAGES
        ORDER BY GIFT_MESSAGE_ID;

    RETURN cur_cursor;
END
;
.
/
