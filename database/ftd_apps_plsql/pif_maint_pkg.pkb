create or replace package body ftd_apps.PIF_MAINT_PKG as

PROCEDURE POPULATE_STAGE_PROD_IDX_XREF
--=======================================================================
--
-- Name:    POPULATE_STAGE_PROD_IDX_XREF
-- Type:    Procedure
-- Syntax:  POPULATE_STAGE_PROD_IDX_XREF
--
-- Dexcription: loads POPULATE_STAGE_PRODUCT_INDEX_XREF using a cursor on
--    LOAD_PROD_INDEX_PRODUCT, marks duplicate rows that failed to load
--
--========================================================================

AS
  CURSOR load_cur IS
  SELECT ROWID AS row_id, lip.* FROM LOAD_PROD_INDEX_PRODUCT lip
  WHERE lip.LOOKUP_PRODUCT_ID IS NOT NULL
  ORDER BY lip.INDEX_ID, lip.LOOKUP_PRODUCT_ID;

BEGIN
  DELETE FROM STAGE_PRODUCT_INDEX_XREF;
  
  FOR load_row IN load_cur
  LOOP
	BEGIN

		INSERT INTO STAGE_PRODUCT_INDEX_XREF (
			PRODUCT_INDEX_ID,
			PRODUCT_ID,
			DISPLAY_ORDER
		)
		VALUES (
			load_row.INDEX_ID,
			load_row.LOOKUP_PRODUCT_ID,
			NVL(load_row.DISPLAY_ORDER, 0)
		);

		EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
		    UPDATE LOAD_PROD_INDEX_PRODUCT
			SET LOAD_FAILED = 'Y'
			WHERE ROWID = load_row.row_id;

	END;
   END LOOP;

END POPULATE_STAGE_PROD_IDX_XREF;

PROCEDURE POPULATE_STAGE_PRODUCT_INDEX
--=======================================================================
--
-- Name:    POPULATE_STAGE_PRODUCT_INDEX
-- Type:    Procedure
-- Syntax:  POPULATE_STAGE_PRODUCT_INDEX
--
-- Dexcription: populates the STAGE_PRODUCT_INDEX with the values in
--    LOAD_PRODUCT_INDEX
--
--========================================================================

AS
BEGIN
  DELETE FROM STAGE_PRODUCT_INDEX;
  INSERT INTO STAGE_PRODUCT_INDEX ( 
      INDEX_ID, 
      INDEX_FILE_NAME, 
      INDEX_NAME, 
      PARENT_INDEX_ID, 
      COUNTRY_ID, 
      DISPLAY_ORDER, 
      OCCASIONS_FLAG,  
      PRODUCTS_FLAG, 
      DROPSHIP_FLAG, 
      WEB_LIVE_FLAG, 
      CALL_CENTER_LIVE_FLAG, 
      WEB_TEST1_FLAG, 
      WEB_TEST2_FLAG,  
      WEB_TEST3_FLAG, 
      CALL_CENTER_TEST_FLAG, 
      SEARCH_INDEX_FLAG, 
      ADV_SEARCH_OCCASIONS_FLAG, 
      ADV_SEARCH_PRODUCTS_FLAG )  
    SELECT 
      INDEX_ID, 
      INDEX_FILE_NAME, 
      NVL(INDEX_NAME, INDEX_FILE_NAME), 
      PARENT_INDEX_ID, 
      COUNTRY_ID, 
      NVL(DISPLAY_ORDER, 0),  
      'N', 
      'N', 
      'N', 
      'Y', 
      'Y', 
      'N', 
      'N', 
      'N', 
      'N', 
      'N', 
      'N', 
      'N' 
      FROM LOAD_PRODUCT_INDEX;
END POPULATE_STAGE_PRODUCT_INDEX;

PROCEDURE POPULATE_STAGE_PROD_IDX_X_INTL
--=======================================================================
--
-- Name:    POPULATE_STAGE_PROD_IDX_X_INTL
-- Type:    Procedure
-- Syntax:  POPULATE_STAGE_PROD_IDX_X_INTL
--
-- Dexcription: copies all records from INTL_PRODUCT_INDEX_XREF to 
--              STAGE_PRODUCT_INDEX_XREF.
--
--========================================================================

AS
BEGIN
  INSERT INTO STAGE_PRODUCT_INDEX_XREF 
    SELECT * FROM INTL_PRODUCT_INDEX_XREF;

END POPULATE_STAGE_PROD_IDX_X_INTL;

PROCEDURE POPULATE_STAGE_PROD_IDX_INTL
--=======================================================================
--
-- Name:    POPULATE_STAGE_PROD_IDX_INTL
-- Type:    Procedure
-- Syntax:  POPULATE_STAGE_PROD_IDX_INTL
--
-- Dexcription: copies all records from INTL_PRODUCT_INDEX to 
--              STAGE_PRODUCT_INDEX.
--
--========================================================================

AS
BEGIN
  INSERT INTO STAGE_PRODUCT_INDEX SELECT * FROM INTL_PRODUCT_INDEX;

END POPULATE_STAGE_PROD_IDX_INTL;

PROCEDURE UPDATE_LOAD_PROD_IDX_PRODUCT
--=======================================================================
--
-- Name:    UPDATE_LOAD_PROD_IDX_PRODUCT
-- Type:    Procedure
-- Syntax:  UPDATE_LOAD_PROD_IDX_PRODUCT
--          Load staging table with production product information
--
--
--========================================================================
AS
BEGIN
  UPDATE LOAD_PROD_INDEX_PRODUCT lp 
    SET lp.INDEX_ID = ( SELECT pi.INDEX_ID FROM STAGE_PRODUCT_INDEX pi WHERE pi.INDEX_FILE_NAME = lp.INDEX_FILE_NAME ),
        lp.LOOKUP_PRODUCT_ID = ( SELECT pm.PRODUCT_ID FROM ftd_apps.PRODUCT_MASTER pm WHERE pm.PRODUCT_ID = lp.PRODUCT_ID );
        
  UPDATE LOAD_PROD_INDEX_PRODUCT lp 
    SET lp.LOOKUP_PRODUCT_ID = ( SELECT pm.PRODUCT_ID FROM ftd_apps.PRODUCT_MASTER pm WHERE pm.NOVATOR_ID = lp.PRODUCT_ID ) 
    WHERE lp.LOOKUP_PRODUCT_ID IS NULL;
    
  UPDATE LOAD_PROD_INDEX_PRODUCT lp 
    SET lp.LOOKUP_PRODUCT_ID = ( SELECT um.UPSELL_MASTER_ID FROM ftd_apps.UPSELL_MASTER um WHERE um.UPSELL_MASTER_ID = lp.PRODUCT_ID ) 
    WHERE lp.LOOKUP_PRODUCT_ID IS NULL;
    
  UPDATE LOAD_PROD_INDEX_PRODUCT lp 
    SET lp.LOOKUP_PRODUCT_ID = ( SELECT px.PRODUCT_ID FROM PRODUCT_XREF px WHERE px.PRODUCT_XREF_ID = lp.PRODUCT_ID ) 
    WHERE lp.LOOKUP_PRODUCT_ID IS NULL;
    
END UPDATE_LOAD_PROD_IDX_PRODUCT;

PROCEDURE UPDATE_LOAD_PRODUCT_INDEX
--=======================================================================
--
-- Name:    UPDATE_LOAD_PRODUCT_INDEX
-- Type:    Procedure
-- Syntax:  UPDATE_LOAD_PRODUCT_INDEX
--          Load staging table with production product information
--
--
--========================================================================
AS
BEGIN
  UPDATE LOAD_PRODUCT_INDEX l 
    SET l.index_id = ( SELECT pi.index_id FROM LOAD_PRODUCT_INDEX pi WHERE pi.index_file_name = l.index_file_name ) 
    WHERE l.index_id IS NULL;
    
  UPDATE LOAD_PRODUCT_INDEX 
    SET index_id = PRODUCT_INDEX_ID.NEXTVAL 
    WHERE index_id IS NULL;
    
  UPDATE LOAD_PRODUCT_INDEX l 
    SET l.parent_index_id = ( SELECT pi.index_id FROM LOAD_PRODUCT_INDEX pi WHERE pi.index_file_name = l.parent_index_file_name );  
END UPDATE_LOAD_PRODUCT_INDEX;

PROCEDURE UPDATE_STAGE_PRODUCT_INDEX
--=======================================================================
--
-- Name:    UPDATE_STAGE_PRODUCT_INDEX
-- Type:    Procedure
-- Syntax:  UPDATE_STAGE_PRODUCT_INDEX
--          Update STAGE_PRODUCT_INDEX with the index_id's for the 
--          parent indexes of "occasion" and "product".  Also updates 
--          advanced search properties.
--
--
--========================================================================
AS
BEGIN
  UPDATE STAGE_PRODUCT_INDEX 
    SET 
      OCCASIONS_FLAG = 'Y', 
      ADV_SEARCH_OCCASIONS_FLAG = 'Y', 
      PARENT_INDEX_ID = NULL  
    WHERE PARENT_INDEX_ID = ( SELECT INDEX_ID FROM STAGE_PRODUCT_INDEX WHERE INDEX_FILE_NAME = 'occasion' );
    
  UPDATE STAGE_PRODUCT_INDEX 
    SET 
      PRODUCTS_FLAG = 'Y', 
      PARENT_INDEX_ID = NULL  
    WHERE PARENT_INDEX_ID = ( SELECT INDEX_ID FROM STAGE_PRODUCT_INDEX WHERE INDEX_FILE_NAME = 'product' );
    
  UPDATE STAGE_PRODUCT_INDEX pi 
    SET 
      pi.ADV_SEARCH_PRODUCTS_FLAG = 'Y', 
      pi.DISPLAY_ORDER = ( SELECT l.DISPLAY_ORDER FROM LOAD_ADV_SEARCH_PRODUCT_INDEX l WHERE l.INDEX_FILE_NAME = pi.INDEX_FILE_NAME ) 
    WHERE EXISTS ( SELECT 1 FROM LOAD_ADV_SEARCH_PRODUCT_INDEX l WHERE l.INDEX_FILE_NAME = pi.INDEX_FILE_NAME );

END UPDATE_STAGE_PRODUCT_INDEX;

PROCEDURE UPDATE_STAGE_PROD_IDX_COMPANY (
  IN_COMPANY_ID       IN STAGE_PRODUCT_INDEX.COMPANY_ID%TYPE,
  IN_COMPANY_PREFIX   IN VARCHAR2
)
--=======================================================================
--
-- Name:    UPDATE_STAGE_PRODUCT_IDX_COMPANY
-- Type:    Procedure
-- Syntax:  UPDATE_STAGE_PRODUCT_IDX_COMPANY
--          Update company info on STAGE_PRODUCT_INDEX table
--
--
--========================================================================
AS
BEGIN
  UPDATE STAGE_PRODUCT_INDEX 
    SET COMPANY_ID = IN_COMPANY_ID 
    WHERE INDEX_FILE_NAME LIKE IN_COMPANY_PREFIX || '%';

END UPDATE_STAGE_PROD_IDX_COMPANY;

PROCEDURE BACKUP_PRODUCT_INDEX_TABLES
AS
--=======================================================================
--
-- Name:    BACKUP_PRODUCT_INDEX_TABLES
-- Type:    PROCEDURE
-- Syntax:  BACKUP_PRODUCT_INDEX_TABLES
--          copy all records from the product index tables to a save location
--
--
--========================================================================
BEGIN
  delete from product_index_bak;
  delete from product_index_xref_bak;
  insert into product_index_bak select * from product_index;
  insert into product_index_xref_bak select * from product_index_xref;
END BACKUP_PRODUCT_INDEX_TABLES;

PROCEDURE COPY_TO_PRODUCT_INDEX_TABLES
AS
--=======================================================================
--
-- Name:    COPY_TO_PRODUCT_INDEX_TABLES
-- Type:    PROCEDURE
-- Syntax:  COPY_TO_PRODUCT_INDEX_TABLES
--          move product indexes to the product index tables
--
--
--========================================================================
BEGIN
  delete from product_index;
  delete from product_index_xref;
  insert into product_index select * from stage_product_index;
  insert into product_index_xref select * from stage_product_index_xref;

  -- This was commented out (Jun 2018) since it's long running and results in
  -- table locks that prevent product updates via PDB.  Instead, the PAS command
  -- will be scheduled to run once per day after midnight (when noone would be using PDB).
  -- Insert a command to update PAS
  -- PAS.POST_PAS_COMMAND('MODIFIED_COUNTRY_PRODUCT','ALL ALL');  

END COPY_TO_PRODUCT_INDEX_TABLES;

PROCEDURE INSERT_PRODUCT_INDEX (
  IN_INDEX_FILE_NAME         IN LOAD_PRODUCT_INDEX.INDEX_FILE_NAME%TYPE, 
  IN_INDEX_NAME              IN LOAD_PRODUCT_INDEX.INDEX_NAME%TYPE, 
  IN_PARENT_INDEX_FILE_NAME  IN LOAD_PRODUCT_INDEX.PARENT_INDEX_FILE_NAME%TYPE, 
  IN_COUNTRY_ID              IN LOAD_PRODUCT_INDEX.COUNTRY_ID%TYPE, 
  IN_DISPLAY_ORDER           IN LOAD_PRODUCT_INDEX.DISPLAY_ORDER%TYPE
)
--=======================================================================
--
-- Name:    INSERT_PRODUCT_INDEX
-- Type:    Procedure
-- Syntax:  INSERT_PRODUCT_INDEX
-- DESCRIPTION:  insert a record into the LOAD_PROD_INDEX table
--
--
--========================================================================
AS
  v_display_order LOAD_PRODUCT_INDEX.DISPLAY_ORDER%TYPE;
BEGIN
  IF IN_DISPLAY_ORDER = -1 THEN
    v_display_order := null;
  ELSE
    v_display_order := IN_DISPLAY_ORDER;
  END IF;
  
  INSERT INTO LOAD_PRODUCT_INDEX(
    INDEX_FILE_NAME, 
    INDEX_NAME, 
    PARENT_INDEX_FILE_NAME, 
    COUNTRY_ID, 
    DISPLAY_ORDER
  ) VALUES (
    IN_INDEX_FILE_NAME,
    regexp_replace(asciistr(IN_INDEX_NAME), '\\([0-9]+)', '\u\1'),
    IN_PARENT_INDEX_FILE_NAME,
    IN_COUNTRY_ID,
    v_display_order
  );
END INSERT_PRODUCT_INDEX;

PROCEDURE INSERT_PRODUCT_INDEX_PRODUCT (
  IN_INDEX_FILE_NAME  IN LOAD_PROD_INDEX_PRODUCT.INDEX_FILE_NAME%TYPE, 
  IN_PRODUCT_ID       IN LOAD_PROD_INDEX_PRODUCT.PRODUCT_ID%TYPE, 
  IN_DISPLAY_ORDER    IN LOAD_PROD_INDEX_PRODUCT.DISPLAY_ORDER%TYPE,
  IN_LOAD_FAILED      IN LOAD_PROD_INDEX_PRODUCT.LOAD_FAILED%TYPE
)
--=======================================================================
--
-- Name:    INSERT_PRODUCT_INDEX_PRODUCT
-- Type:    Procedure
-- Syntax:  INSERT_PRODUCT_INDEX_PRODUCT
-- DESCRIPTION:  insert a record into the LOAD_PROD_INDEX_PRODUCT table
--
--
--========================================================================
AS
BEGIN
  INSERT INTO LOAD_PROD_INDEX_PRODUCT (
    INDEX_FILE_NAME, 
    PRODUCT_ID, 
    DISPLAY_ORDER, 
    LOAD_FAILED
  ) VALUES (
    IN_INDEX_FILE_NAME, 
    IN_PRODUCT_ID, 
    IN_DISPLAY_ORDER, 
    IN_LOAD_FAILED
  );
END INSERT_PRODUCT_INDEX_PRODUCT;

PROCEDURE EMPTY_LOAD_TABLES
AS
BEGIN
  DELETE FROM LOAD_PRODUCT_INDEX;
  DELETE FROM LOAD_PROD_INDEX_PRODUCT;
END EMPTY_LOAD_TABLES;

FUNCTION GET_LOAD_FAILED_PRODUCTS
 RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_LOAD_FAILED_PRODUCTS
-- Type:    Function
-- Syntax:  GET_LOAD_FAILED_PRODUCTS
-- Returns: ref_cursor for all columns in STAGE_LOAD_PROD_INDEX_PRODUCT 
--          where load_failed = 'Y'
--
--
--========================================================================

AS
    product_cursor types.ref_cursor;
BEGIN

    OPEN product_cursor FOR
  	SELECT *
	    FROM LOAD_PROD_INDEX_PRODUCT
	    WHERE LOOKUP_PRODUCT_ID IS NOT NULL
	      and load_failed = 'Y'
	    ORDER BY INDEX_ID, LOOKUP_PRODUCT_ID;
      
    RETURN product_cursor;

END GET_LOAD_FAILED_PRODUCTS;

FUNCTION GET_PK_VIOL_FOR_XREF_LOAD
 RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_PK_VIOL_FOR_XREF_LOAD
-- Type:    Function
-- Syntax:  GET_PK_VIOL_FOR_XREF_LOAD
--
-- Description: Returns rows in STAGE_LOAD_PROD_INDEX_PRODUCT and 
--              intl_product_index_xref that happen to have identical 
--              PK values.
--
--========================================================================

AS
  product_cursor types.ref_cursor;
BEGIN

  OPEN product_cursor FOR
	  SELECT 
      lp.INDEX_FILE_NAME as COUNTRY, 
      lp.INDEX_ID as INDEX_ID,
		  lp.LOOKUP_PRODUCT_ID as PRODUCT_ID
	  FROM LOAD_PROD_INDEX_PRODUCT lp,
	       intl_product_index_xref ip
	  WHERE ip.product_index_id = lp.index_id
	    and ip.product_id = lp.lookup_product_id
	  ORDER BY lp.INDEX_ID, lp.LOOKUP_PRODUCT_ID;
    
   RETURN product_cursor;

END GET_PK_VIOL_FOR_XREF_LOAD;


PROCEDURE GET_PIF_COMPANY_DATA (
  OUT_COMPANIES OUT types.ref_cursor,
  OUT_OCCASIONS OUT types.ref_cursor,
  OUT_PRODUCTS OUT types.ref_cursor
)
--=======================================================================
--
-- Name:    GET_PIF_COMPANY_DATA
-- Type:    Function
-- Syntax:  GET_PIF_COMPANY_DATA
--
-- Description: all rows in the following tables for active companies
--              PIF_COMPANIES
--              PIF_COMPANY_OCCASIONS
--              PIF_COMPANY_PRODUCTS
--
--========================================================================

AS
BEGIN

  OPEN OUT_COMPANIES FOR
	  SELECT 
        c.PIF_COMPANY_ID, 
        c.FILE_NAME_TXT, 
        c.FIELD_COUNT, 
        c.PREFIX_USED_FLAG, 
        c.PREFIX_TXT, 
        c.ADV_SEARCH_OCCASION_FLAG, 
        c.ACTIVE_FLAG 
      FROM PIF_COMPANIES c 
      WHERE c.ACTIVE_FLAG = 'Y' 
      ORDER BY c.PIF_COMPANY_ID;
      
  OPEN OUT_OCCASIONS FOR
    SELECT 
        o.PIF_COMPANY_ID, 
        o.OCCASION_IDX_FILE_NAME 
      FROM FTD_APPS.PIF_COMPANY_OCCASIONS o
      JOIN FTD_APPS.PIF_COMPANIES c ON o.PIF_COMPANY_ID = c.PIF_COMPANY_ID
      WHERE c.ACTIVE_FLAG = 'Y';
      
  OPEN OUT_PRODUCTS FOR
    SELECT 
        p.PIF_COMPANY_ID, 
        p.PRODUCT_IDX_FILE_NAME 
      FROM FTD_APPS.PIF_COMPANY_PRODUCTS p
      JOIN FTD_APPS.PIF_COMPANIES c ON p.PIF_COMPANY_ID = c.PIF_COMPANY_ID
      WHERE c.ACTIVE_FLAG = 'Y';

END GET_PIF_COMPANY_DATA;

end PIF_MAINT_PKG;
/
