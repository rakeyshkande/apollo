CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_COST_CENTER_ID (inCostCenterId IN VARCHAR2)
RETURN types.ref_cursor
--=================================================================
--
-- Name:    OE_GET_COST_CENTER_ID
-- Type:    Function
-- Syntax:  OE_GET_COST_CENTER_ID (inCostCenterId IN VARCHAR2)
-- Returns: ref_cursor for
--          costCenterId       VARCHAR2
--
-- Description:   Used to validate ADVO Cost Center IDs.
--
--==================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT COST_CENTER_ID AS "costCenterId"
        FROM COST_CENTER_IDS
        WHERE COST_CENTER_ID = inCostCenterId;

    RETURN cur_cursor;
END;
.
/
