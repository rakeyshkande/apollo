CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIP_SUNDAY_FLAG (
 zipCode IN VARCHAR2
 )
  RETURN VARCHAR2
as
    ret    CSZ_AVAIL.SUNDAY_DELIVERY_FLAG%TYPE := null;

begin

    -- Select the SUNDAY_DELIVERY_FLAG for this zip, if available
    begin
      -- Assume NULL SUNDAY_DELIVERY_FLAG flag indicates SUNDAY_DELIVERY_FLAG is off
      SELECT NVL(SUNDAY_DELIVERY_FLAG, 'N')
      INTO ret
      FROM CSZ_AVAIL
      WHERE CSZ_ZIP_CODE = zipCode;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- If no row found on CSZ_AVAIL, SUNDAY_DELIVERY_FLAG is on for the zip
      ret := 'Y';
    end;


    -- WebOE is looking for a 'Y' in this field so translate an 'S' to 'Y'.
    IF(ret = 'S')
        THEN
	ret := 'Y';
    END IF;

    return ret;
end;
.
/
