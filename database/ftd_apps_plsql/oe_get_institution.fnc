CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_INSTITUTION (institutionNameIn IN VARCHAR2,
  phoneNumberIn IN VARCHAR2 default null,
  addressIn IN VARCHAR2 default null,
  cityIn IN VARCHAR2 default null,
  stateIn IN VARCHAR2 default null,
  zipIn IN VARCHAR2 default null,
  institutionType IN VARCHAR2 default null)
  RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_INSTITUTION
-- Type:    Function
-- Syntax:  OE_GET_INSTITUTION(institutionNameIn IN VARCHAR2)
-- Returns: ref_cursor for
--          BUSINESS_ID   VARCHAR2(10)
--          BFH_NAME      VARCHAR2(30)
--          ADDRESS       VARCHAR2(30)
--          CITY          VARCHAR2(30)
--          STATE         VARCHAR2(2)
--          ZIP_CODE      VARCHAR2(10)
--
-- Description:   Queries the BUSINESS table by institutionNameIn and returns a ref cursor for
--                resulting row.
--
--==================================================\

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT B.BUSINESS_ID                 as "businessId",
               nls_initcap(B.BUSINESS_NAME)  as "bfhName",
               nls_initcap(B.ADDRESS)        as "address1",
               nls_initcap(B.CITY)           as "city",
               B.STATE                       as "state",

               -- Take 9-digit zipcodes down to 5 in the return
               decode(length(B.ZIP_CODE), 9,
                    substr(B.ZIP_CODE,1,5), B.ZIP_CODE) as "zipCode",

               B.PHONE_NUMBER                as "homePhone"
        FROM BUSINESS B
        WHERE ( institutionNameIn IS NULL
            OR B.BUSINESS_NAME like UPPER(institutionNameIn) || '%' )
        AND ( phoneNumberIn IS NULL
            OR B.PHONE_NUMBER = OE_CLEANUP_ALPHANUM_STRING(phoneNumberIn,'N') )
        AND ( addressIn IS NULL
            OR B.ADDRESS LIKE '%' || UPPER(addressIn) || '%' )
        AND ( cityIn IS NULL
            OR B.CITY LIKE UPPER(cityIn) || '%' )
        AND ( stateIn IS null OR B.STATE = stateIn )
        AND ( zipIn IS NULL OR B.ZIP_CODE LIKE OE_CLEANUP_ALPHANUM_STRING(zipIn,'Y') || '%')
        AND ( institutionType IS NULL
          OR EXISTS (
              SELECT 1 FROM INSTITUTION_TYPE IT
              WHERE IT.DESCRIPTION = B.BUSINESS_TYPE
              AND DECODE(IT.TYPE, 'N', 'H', IT.TYPE) =
                  DECODE(institutionType, 'N', 'H', institutionType) ));

    RETURN cur_cursor;
END;
.
/
