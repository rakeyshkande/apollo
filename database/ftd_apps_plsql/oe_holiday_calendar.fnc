CREATE OR REPLACE
FUNCTION ftd_apps.OE_HOLIDAY_CALENDAR RETURN types.ref_cursor
 --==============================================================
--
-- Name:    OE_HOLIDAY_CALENDAR
-- Type:    Function
-- Syntax:  OE_HOLIDAY_CALENDAR
-- Returns: ref_cursor for
--          holidayDate           VARCHAR2(10)
--          holidayDescription    VARCHAR2(50)
--
-- Description:   Queries HOLIDAY_CALENDAR and returns all rows.
--===============================================================

AS
   cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT TO_CHAR(HOLIDAY_DATE, 'mm/dd/yyyy') as "holidayDate",
                HOLIDAY_DESCRIPTION as "holidayDescription"
        FROM HOLIDAY_CALENDAR
        ORDER BY HOLIDAY_DATE;

    RETURN cur_cursor;
END
;
.
/
