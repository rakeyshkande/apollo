CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SHIPPING_KEYS RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SHIPPING_KEYS
-- Type:    Function
-- Syntax:  SP_GET_SHIPPING_KEYS ()
-- Returns: ref_cursor for
--          SHIPPING_KEY_ID           VARCHAR2(10)
--          SHIPPING_KEY_DESCRIPTION  VARCHAR2(25)
--
--
-- Description:   Queries the SHIPPING_KEYS table and
--                returns a ref cursor for resulting row.
--
--======================================================================

AS
    shipping_key_cursor types.ref_cursor;
BEGIN
    OPEN shipping_key_cursor FOR
        SELECT SHIPPING_KEY_ID          as "shippingKeyId",
               SHIPPING_KEY_DESCRIPTION as "shippingKeyDescription"
        FROM SHIPPING_KEYS
        ORDER BY SHIPPING_KEY_ID;

    RETURN shipping_key_cursor;
END
;
.
/
