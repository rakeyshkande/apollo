CREATE OR REPLACE
PROCEDURE ftd_apps.SP_INSERT_UPSELL_DETAIL (
  detailIdIn in varchar2,
  masterIdIn in varchar2,
  detailSeqIn in number,
  detailNameIn in varchar2,
  gbbNameOverrideFlag in varchar2,
  gbbNameOverrideText in varchar2,
  gbbPriceOverrideFlag in varchar2,
  gbbPriceOverrideText in varchar2,
  gbbSequence in number,
  defaultSkuFlag in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSERT_UPSELL_DETAIL
-- Type:    Procedure
-- Syntax:  SP_INSERT_UPSELL_DETAIL (detailIdIn, masterIdIn,detailSeqIn,detailNameIn)
--
-- Description:   INSERTS A ROW INTO UPSELL_DETAIL
--
--==============================================================================
begin

insert into UPSELL_DETAIL(UPSELL_DETAIL_ID,UPSELL_MASTER_ID,UPSELL_DETAIL_SEQUENCE,UPSELL_DETAIL_NAME,DEFAULT_SKU_FLAG,
        GBB_NAME_OVERRIDE_FLAG,GBB_NAME_OVERRIDE_TXT,GBB_PRICE_OVERRIDE_FLAG,GBB_PRICE_OVERRIDE_TXT, GBB_UPSELL_DETAIL_SEQUENCE_NUM)
VALUES(detailIdIn,masterIdIn,detailSeqIn,detailNameIn,defaultSkuFlag,
        gbbNameOverrideFlag,gbbNameOverrideText,gbbPriceOverrideFlag,gbbPriceOverrideText, gbbSequence);

end
;
.
/
