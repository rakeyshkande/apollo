-- FTD_APPS.SERVICE_FEE_OVERRIDE_HIST_TRG trigger to populate FTD_APPS.SERVICE_FEE_OVERRIDE_HIST history table

CREATE OR REPLACE TRIGGER FTD_APPS.SERVICE_FEE_OVERRIDE_HIST_TRG
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.service_fee_override REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW

BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.service_fee_override_hist (
        service_fee_override_hist_id,
      	snh_id,
        override_date,
      	domestic_charge,
      	international_charge,
      	updated_on,      	
      	VENDOR_CHARGE,          
		VENDOR_SAT_UPCHARGE,
        expired_on,
        SAME_DAY_UPCHARGE,
		vendor_sun_upcharge,
		vendor_mon_upcharge,
		SAME_DAY_UPCHARGE_FS
      ) VALUES (
        service_fee_override_hist_sq.nextval,
      	:NEW.snh_id,
        :NEW.override_date,
        :NEW.domestic_charge,
        :NEW.international_charge,
        :NEW.updated_on,      	
        :NEW.VENDOR_CHARGE,          
        :NEW.VENDOR_SAT_UPCHARGE,
        null,
        :NEW.SAME_DAY_UPCHARGE,
		:NEW.vendor_sun_upcharge,
		:NEW.vendor_mon_upcharge,
		:NEW.SAME_DAY_UPCHARGE_FS
         );

   ELSIF UPDATING  THEN

      update ftd_apps.service_fee_override_hist
      set    expired_on = sysdate
      where  snh_id = :old.snh_id
      and    override_date = :old.override_date
      and    expired_on is null;

      INSERT INTO ftd_apps.service_fee_override_hist (
        service_fee_override_hist_id,
      	snh_id,
        override_date,
      	domestic_charge,
      	international_charge,
      	updated_on,      	
      	VENDOR_CHARGE,          
		VENDOR_SAT_UPCHARGE,
        expired_on,
        SAME_DAY_UPCHARGE,
		vendor_sun_upcharge,
		vendor_mon_upcharge,
		SAME_DAY_UPCHARGE_FS
      ) VALUES (
        service_fee_override_hist_sq.nextval,
      	:NEW.snh_id,
        :NEW.override_date,
        :NEW.domestic_charge,
        :NEW.international_charge,
        :NEW.updated_on,      	
        :NEW.VENDOR_CHARGE,          
        :NEW.VENDOR_SAT_UPCHARGE,
        null,
        :NEW.SAME_DAY_UPCHARGE,
		:NEW.vendor_sun_upcharge,
		:NEW.vendor_mon_upcharge,
		:NEW.SAME_DAY_UPCHARGE_FS
         );

   ELSIF DELETING  THEN

      update ftd_apps.service_fee_override_hist
      set    expired_on = sysdate
      where  snh_id = :old.snh_id
      and    override_date = :old.override_date
      and    expired_on is null;

   END IF;

END;
/
