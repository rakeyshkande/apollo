CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_FLORIST_LIST_BULK (
   inCity IN VARCHAR2,
   inState IN VARCHAR2,
   inZip IN VARCHAR2,
   floristName IN VARCHAR2,
   phoneNumber IN VARCHAR2,
   inAddress IN VARCHAR2,
   productId IN VARCHAR2, 
   inSourceCode  IN  FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
   inDeliveryDate IN DATE,
   inDeliveryDateEnd IN DATE)
  RETURN types.ref_cursor
--=========================================================================
--
-- Name:    OE_GET_FLORIST_LIST_BULK
-- Type:    Function
-- Syntax:  OE_GET_FLORIST_LIST_BULK (inCity IN VARCHAR2,
--                               inState IN VARCHAR2,
--                               inZip IN VARCHAR2,
--                               floristName IN VARCHAR2,
--                               phoneNumber IN VARCHAR2,
--                               inAddress IN VARCHAR2,
--                               productId IN VARCHAR2,
--                               inSourceCode IN FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
--				 inDeliveryDate IN DATE, --will only be passed in from Scrub Florist Lookup
--				 in DeliveryDateEnd IN DATE) --will only be passed in from Scrub Florist Lookup
-- Returns: ref_cursor for
--          floristId         VARCHAR2(9)
--          floristName       VARCHAR2(40)
--          address           VARCHAR2(30)
--          city              VARCHAR2(30)
--          state             VARCHAR2(2)
--          floristZipCode    VARCHAR2(10)
--          phoneNumber       VARCHAR2(12)
--          floristWeight     NUMBER(3)
--          goToFlag          VARCHAR2(1)
--          sundayFlag        VARCHAR2(1)
--          mercuryFlag       VARCHAR2(1)
--          floristBlockType  VARCHAR2(1)  if NULL, florist is not blocked
--          priority          NUMBER
--
-- Description:  Queries the FLORIST_MASTER and FLORIST_ZIPS tables
--               for florists who cover the supplied zip or deliver to
--               the supplied city / state (if zip gets no hits).
--               Returns ref cursor to row set with basic florist info,
--               also a florist blocked flag that indicates if florist is
--               currently blocked.
--
-- NOTE:  THIS PROCEDURE IS A TEMPORARY SOLUTION FOR SCRUB BULK ORDERS
--        FOR THE HP REPLACEMENT PHASE II PROJECT.  THE ONLY DIFFERENCE
--        BETWEEN THIS PROC AND OE_GET_FLORIST_LIST IS THE CALL TO
--        OE_GET_FLORIST_PRODUCTS_BULK AND OE_GET_FLORIST_PRODUCTS
--        RESPECTIVELY.  WHEN OE_GET_FLORIST_PRODUCTS IS MODIFIED TO
--        HANDLE A VARCHAR2 GREATER THAN 255, THIS PROCEDURE SHOULD
--        BE DROPPED AND THE ORIGINAL OE_GET_FLORIST_LIST PROC SHOULD
--        BE USED.
--
--=========================================================================

AS
    cur_cursor          types.ref_cursor;
    tmp_florist_id      FLORIST_ZIPS.FLORIST_ID%TYPE;
    zip_search          BOOLEAN := TRUE;
    codified_flag       VARCHAR2(1);
BEGIN
    -- Determine whether the zip code has any florists
    -- CR 11/2004 - for new florist data model - check that the zip record is not blocked
    -- for florists that are not inactive
    BEGIN
        SELECT z.FLORIST_ID
        INTO tmp_florist_id
        FROM FLORIST_ZIPS z
        JOIN florist_master m
        on z.florist_id = m.florist_id
        and m.status <> 'Inactive'
        WHERE z.ZIP_CODE = inZip
        AND ROWNUM < 2
        AND z.block_start_date is null;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        zip_search := FALSE;
    END;

    -- Determine whether input product is codified
    BEGIN
        SELECT NVL(CHECK_OE_FLAG, 'N')
        INTO codified_flag
        FROM CODIFIED_PRODUCTS
        WHERE PRODUCT_ID = productId;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        codified_flag := 'N';
    END;

    -- If the zip got any hits then search by florist zip coverage
    -- CR 11/2004 - for new florist data model - use the function decode_florist_status_values
    -- to get the block type or M for suspend based off of the florist status
    IF ( zip_search )
    THEN
	IF inDeliveryDate IS NOT NULL THEN
		OPEN cur_cursor FOR
	            SELECT m.FLORIST_ID as "floristId",
	                nls_initcap(m.FLORIST_NAME) as "floristName",
	                m.ADDRESS as "address",
	                nls_initcap(m.DELIVERY_CITY) as "city",
	                m.DELIVERY_STATE as "state",
	                m.ZIP_CODE as "floristZipCode",
	                m.PHONE_NUMBER as "phoneNumber",
	                m.FLORIST_WEIGHT as "floristWeight",
	                m.SUPER_FLORIST_FLAG as "goToFlag",
	                DECODE(ftd_apps.florist_query_pkg.is_florist_open_sunday(m.florist_id), 'N', '', 'Y', 'Y') as "sundayFlag",
	                m.MERCURY_FLAG as "mercuryFlag",
	
	                -- Use florist block type to report florist blocks
	                --   AND unavailable codified product (report as 'P')
	                DECODE(codified_flag, 'N', null,
	                    DECODE(fc.codification_ID, null, 'P',
	                        DECODE(fc.block_start_date, null, null, 'P'))) as "floristBlockType",
	
	                decode( nvl(cp.codification_id, 'N/A'), nvl(fc.codification_id, 'X'), cp.product_id, null) as "floristProducts",
	                m.status as "status",
                    florist_query_pkg.is_florist_blocked(m.florist_id, inDeliveryDate, inDeliveryDateEnd) "florist_blocked",
                    florist_query_pkg.is_florist_suspended(m.florist_id, inDeliveryDate, inDeliveryDateEnd) "florist_suspended",
	                --If a source code is passed in, retrieve the florist priority; else return 0
	                decode(inSourceCode, null, 0, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID),0 )) as "priority"
	
	            FROM FLORIST_MASTER m
	            JOIN FLORIST_ZIPS z
	            ON z.ZIP_CODE = inZip
	            AND z.FLORIST_ID = m.FLORIST_ID
	            LEFT OUTER JOIN codified_products cp
	            ON cp.product_id = productId
	            LEFT OUTER JOIN florist_codifications fc
	            ON fc.codification_id = cp.codification_id
	            AND fc.florist_id = m.florist_id
	
	            WHERE 1=1
	
	            -- Addresses are loaded from HP mixed case so compare as is
	            AND (( inAddress IS NULL )
	              OR ( m.ADDRESS LIKE inAddress || '%' ))
	
	            AND (( floristName IS NULL )
	              OR ( m.FLORIST_NAME LIKE UPPER(floristName) || '%' ))
	            AND (( phoneNumber IS NULL )
	              OR ( m.PHONE_NUMBER = OE_CLEANUP_ALPHANUM_STRING(phoneNumber,'N') ))
	
	            -- Ignore vendors, florist ID starts with 91
	            AND m.FLORIST_ID NOT LIKE '91%'
	            AND florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
	            AND florist_query_pkg.is_florist_open_date_range(m.florist_id, inDeliveryDate, inDeliveryDateEnd) = 'Y'
	            ORDER BY 
	                --priority 1 = primary, priority 2 onwards = backup... screen needs to display primary followed by backup followed by other florists by weight
	                decode(inSourceCode, null, 999999, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID), 999999)),
	                DECODE(m.SUPER_FLORIST_FLAG, 'Y', 1, 2) ASC,
	                m.FLORIST_WEIGHT DESC;
        ELSE
       		OPEN cur_cursor FOR
            	SELECT m.FLORIST_ID as "floristId",
	                nls_initcap(m.FLORIST_NAME) as "floristName",
	                m.ADDRESS as "address",
	                nls_initcap(m.DELIVERY_CITY) as "city",
	                m.DELIVERY_STATE as "state",
	                m.ZIP_CODE as "floristZipCode",
	                m.PHONE_NUMBER as "phoneNumber",
	                m.FLORIST_WEIGHT as "floristWeight",
	                m.SUPER_FLORIST_FLAG as "goToFlag",
	                DECODE(ftd_apps.florist_query_pkg.is_florist_open_sunday(m.florist_id), 'N', '', 'Y', 'Y') as "sundayFlag",
	                m.MERCURY_FLAG as "mercuryFlag",
	
	                -- Use florist block type to report florist blocks
	                --   AND unavailable codified product (report as 'P')
	                DECODE(codified_flag, 'N', null,
	                    DECODE(fc.codification_ID, null, 'P',
	                        DECODE(fc.block_start_date, null, null, 'P'))) as "floristBlockType",
	
	                decode( nvl(cp.codification_id, 'N/A'), nvl(fc.codification_id, 'X'), cp.product_id, null) as "floristProducts",
	                m.status as "status",
                    florist_query_pkg.is_florist_suspended(m.florist_id, sysdate, null) "florist_suspended",
	                --If a source code is passed in, retrieve the florist priority; else return 0
	                decode(inSourceCode, null, 0, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID),0 )) as "priority"
	
	            FROM FLORIST_MASTER m
	            JOIN FLORIST_ZIPS z
	            ON z.ZIP_CODE = inZip
	            AND z.FLORIST_ID = m.FLORIST_ID
	            LEFT OUTER JOIN codified_products cp
	            ON cp.product_id = productId
	            LEFT OUTER JOIN florist_codifications fc
	            ON fc.codification_id = cp.codification_id
	            AND fc.florist_id = m.florist_id
	
	            WHERE 1=1
	
	            -- Addresses are loaded from HP mixed case so compare as is
	            AND (( inAddress IS NULL )
	              OR ( m.ADDRESS LIKE inAddress || '%' ))
	
	            AND (( floristName IS NULL )
	              OR ( m.FLORIST_NAME LIKE UPPER(floristName) || '%' ))
	            AND (( phoneNumber IS NULL )
	              OR ( m.PHONE_NUMBER = OE_CLEANUP_ALPHANUM_STRING(phoneNumber,'N') ))
	
	            -- Ignore vendors, florist ID starts with 91
	            AND m.FLORIST_ID NOT LIKE '91%'
	            ORDER BY 
	                --priority 1 = primary, priority 2 onwards = backup... screen needs to display primary followed by backup followed by other florists by weight
	                decode(inSourceCode, null, 999999, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID), 999999)),
	                DECODE(m.SUPER_FLORIST_FLAG, 'Y', 1, 2) ASC,
	                m.FLORIST_WEIGHT DESC;
       
       END IF;

    -- Otherwise search for florists delivering to the given city/state
    -- CR 11/2004 - for new florist data model - use the function decode_florist_status_values
    -- to get the block type or M for suspend based off of the florist status
    ELSE
        IF inDeliveryDate IS NOT NULL THEN
        	OPEN cur_cursor FOR
	            SELECT m.FLORIST_ID as "floristId",
	                nls_initcap(m.FLORIST_NAME) as "floristName",
	                m.ADDRESS as "address",
	                nls_initcap(m.DELIVERY_CITY) as "city",
	                m.DELIVERY_STATE as "state",
	                m.ZIP_CODE as "floristZipCode",
	                m.PHONE_NUMBER as "phoneNumber",
	                m.FLORIST_WEIGHT as "floristWeight",
	                m.SUPER_FLORIST_FLAG as "goToFlag",
			DECODE(ftd_apps.florist_query_pkg.is_florist_open_sunday(m.florist_id), 'N', '', 'Y', 'Y') as "sundayFlag",
	                m.MERCURY_FLAG as "mercuryFlag",
	
	                -- Use florist block type to report florist blocks
	                --   AND unavailable codified product (report as 'P')
	                DECODE(codified_flag, 'N', null,
	                    DECODE(fc.codification_ID, null, 'P',
	                        DECODE(fc.block_start_date, null, null, 'P'))) as "floristBlockType",
	
	                decode( nvl(cp.codification_id, 'N/A'), nvl(fc.codification_id, 'X'), cp.product_id, null) as "floristProducts",
	                m.status as "status",
                    florist_query_pkg.is_florist_blocked(m.florist_id, inDeliveryDate, inDeliveryDateEnd) "florist_blocked",
                    florist_query_pkg.is_florist_suspended(m.florist_id, inDeliveryDate, inDeliveryDateEnd) "florist_suspended",
	                --If a source code is passed in, retrieve the florist priority; else return 0
	                decode(inSourceCode, null, 0, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID),0 )) as "priority"
	
	            FROM FLORIST_MASTER m
	            LEFT OUTER JOIN codified_products cp
	            ON cp.product_id = productId
	            LEFT OUTER JOIN florist_codifications fc
	            ON fc.codification_id = cp.codification_id
	            AND fc.florist_id = m.florist_id
	
	            WHERE upper(m.DELIVERY_CITY) LIKE UPPER(inCity) || '%'
	            AND m.DELIVERY_STATE = UPPER(inState)
	
	            -- Addresses are loaded from HP mixed case so compare as is
	            AND (( inAddress IS NULL )
	              OR ( m.ADDRESS LIKE inAddress || '%' ))
	
	            AND (( floristName IS NULL )
	              OR ( m.FLORIST_NAME LIKE UPPER(floristName) || '%' ))
	            AND (( phoneNumber IS NULL )
	              OR ( m.PHONE_NUMBER = OE_CLEANUP_ALPHANUM_STRING(phoneNumber,'N') ))
	
	            -- Ignore vendors, florist ID starts with 91
	            AND m.FLORIST_ID NOT LIKE '91%'
	            AND m.status <> 'Inactive'
	            AND florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
	            AND florist_query_pkg.is_florist_open_date_range(m.florist_id, inDeliveryDate, inDeliveryDateEnd) = 'Y'
	            	
	            ORDER BY 
	                --priority 1 = primary, priority 2 onwards = backup... screen needs to display primary followed by backup followed by other florists by weight
	                decode(inSourceCode, null, 999999, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID), 999999)),
	                DECODE(m.SUPER_FLORIST_FLAG, 'Y', 1, 2) ASC,
	                m.FLORIST_WEIGHT DESC;
	ELSE
	       	OPEN cur_cursor FOR
	            SELECT m.FLORIST_ID as "floristId",
	                nls_initcap(m.FLORIST_NAME) as "floristName",
	                m.ADDRESS as "address",
	                nls_initcap(m.DELIVERY_CITY) as "city",
	                m.DELIVERY_STATE as "state",
	                m.ZIP_CODE as "floristZipCode",
	                m.PHONE_NUMBER as "phoneNumber",
	                m.FLORIST_WEIGHT as "floristWeight",
	                m.SUPER_FLORIST_FLAG as "goToFlag",
			DECODE(ftd_apps.florist_query_pkg.is_florist_open_sunday(m.florist_id), 'N', '', 'Y', 'Y') as "sundayFlag",
	                m.MERCURY_FLAG as "mercuryFlag",
	
	                -- Use florist block type to report florist blocks
	                --   AND unavailable codified product (report as 'P')
	                DECODE(codified_flag, 'N', null,
	                    DECODE(fc.codification_ID, null, 'P',
	                        DECODE(fc.block_start_date, null, null, 'P'))) as "floristBlockType",
	
	                decode( nvl(cp.codification_id, 'N/A'), nvl(fc.codification_id, 'X'), cp.product_id, null) as "floristProducts",
	                m.status as "status",
                    florist_query_pkg.is_florist_suspended(m.florist_id, sysdate, null) "florist_suspended",
	                --If a source code is passed in, retrieve the florist priority; else return 0
	                decode(inSourceCode, null, 0, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID),0 )) as "priority"
	
	            FROM FLORIST_MASTER m
	            LEFT OUTER JOIN codified_products cp
	            ON cp.product_id = productId
	            LEFT OUTER JOIN florist_codifications fc
	            ON fc.codification_id = cp.codification_id
	            AND fc.florist_id = m.florist_id
	
	            WHERE upper(m.DELIVERY_CITY) LIKE UPPER(inCity) || '%'
	            AND m.DELIVERY_STATE = UPPER(inState)
	
	            -- Addresses are loaded from HP mixed case so compare as is
	            AND (( inAddress IS NULL )
	              OR ( m.ADDRESS LIKE inAddress || '%' ))
	
	            AND (( floristName IS NULL )
	              OR ( m.FLORIST_NAME LIKE UPPER(floristName) || '%' ))
	            AND (( phoneNumber IS NULL )
	              OR ( m.PHONE_NUMBER = OE_CLEANUP_ALPHANUM_STRING(phoneNumber,'N') ))
	
	            -- Ignore vendors, florist ID starts with 91
	            AND m.FLORIST_ID NOT LIKE '91%'
	            AND m.status <> 'Inactive'
	            AND florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
	            	            	
	            ORDER BY 
	                --priority 1 = primary, priority 2 onwards = backup... screen needs to display primary followed by backup followed by other florists by weight
	                decode(inSourceCode, null, 999999, nvl((select priority from FTD_APPS.SOURCE_FLORIST_PRIORITY sfp where sfp.SOURCE_CODE = inSourceCode and sfp.FLORIST_ID = m.FLORIST_ID), 999999)),
	                DECODE(m.SUPER_FLORIST_FLAG, 'Y', 1, 2) ASC,
	                m.FLORIST_WEIGHT DESC;
	END IF;      

    END IF;

    RETURN cur_cursor;
END;
.
/
