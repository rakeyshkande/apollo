CREATE OR REPLACE TRIGGER FTD_APPS.HOLIDAY_COUNTRY_AIUD
AFTER INSERT OR UPDATE OR DELETE ON FTD_APPS.HOLIDAY_COUNTRY
FOR EACH ROW

DECLARE

v_date          date;
v_end_date      date;
v_add_days      number;
v_country_id    varchar2(100);

procedure post_pas(
   in_start_date in date,
   in_end_date in date,
   in_country_id in varchar2
   ) as

   v_temp_date date;
   v_priority integer;
   v_max_days integer;
   v_max_date date;

   begin

      select value into v_max_days
      from frp.global_parms
      where context = 'PAS_CONFIG'
      and name = 'MAX_DAYS';
      v_max_date := trunc(sysdate) + v_max_days;
      
      v_temp_date := in_start_date;
      while v_temp_date <= in_end_date loop
         if v_temp_date <= v_max_date then
            v_priority := trunc(v_temp_date) - trunc(sysdate);
            PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_COUNTRY', in_country_id || ' ' || to_char(v_temp_date, 'mmddyyyy'), v_priority);
         end if;
         v_temp_date := v_temp_date + 1;
      end loop;

   end;

BEGIN

--
-- Insert JMS message to eventually update the PAS framework tables
--

SELECT FGP.VALUE INTO V_ADD_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'ADD_DAYS';

if INSERTING then
   v_date := :new.holiday_date;
   v_country_id := :new.country_id;
   v_end_date := v_date + v_add_days;
   post_pas(v_date, v_end_date, v_country_id);
elsif UPDATING then
   v_date := :old.holiday_date;
   v_country_id := :old.country_id;
   v_end_date := v_date + v_add_days;
   post_pas(v_date, v_end_date, v_country_id);
   v_date := :new.holiday_date;
   v_country_id := :new.country_id;
   v_end_date := v_date + v_add_days;
   post_pas(v_date, v_end_date, v_country_id);
elsif DELETING then
   v_date := :old.holiday_date;
   v_country_id := :old.country_id;
   v_end_date := v_date + v_add_days;
   post_pas(v_date, v_end_date, v_country_id);
end if;

EXCEPTION WHEN OTHERS THEN
    NULL;
END;
/