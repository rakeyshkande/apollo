CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SHIPPING_METHODS RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SHIPPING_METHODS
-- Type:    Function
-- Syntax:  SP_GET_SHIPPING_METHODS ()
-- Returns: ref_cursor for
--          SHIP_METHOD_ID  VARCHAR2(10)
--          DESCRIPTION     VARCHAR2(25)
--
--
-- Description:   Queries the SHIP_METHODS table and
--                returns a ref cursor for resulting row.
--
--======================================================================

AS
    methods_cursor types.ref_cursor;
BEGIN
    OPEN methods_cursor FOR
        SELECT SHIP_METHOD_ID as "shipMethodId",
               DESCRIPTION    as "description"
         FROM SHIP_METHODS order by SDS_SHIP_VIA ;

    RETURN methods_cursor;
END
;
.
/
