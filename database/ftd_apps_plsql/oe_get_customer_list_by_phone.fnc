CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_CUSTOMER_LIST_BY_PHONE (phoneIn varchar2)
  RETURN types.ref_cursor
--======================================================================
--
-- Name:    OE_GET_CUSTOMER_LIST_BY_PHONE
-- Type:    Function
-- Syntax:  OE_GET_CUSTOMER_LIST_BY_PHONE (phoneIn VARCHAR2)
-- Returns: ref_cursor for
--          CUSTOMER_ID   VARCHAR2(8)
--          LAST_NAME     VARCHAR2(30)
--          FIRST_NAME    VARCHAR2(30)
--          ADDRESS_1     VARCHAR2(30)
--          ADDRESS_2     VARCHAR2(30)
--          CITY          VARCHAR2(30)
--          STATE         VARCHAR2(2)
--          ZIP_CODE      VARCHAR2(10)

--
-- Description:   Queries the CUSTOMER table by HOME_PHONE or WORK_PHONE
--                and returns a ref cursor for all resulting rows.
--
--=======================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT CUSTOMER_ID as "customerID",
              LAST_NAME as "lastName",
              FIRST_NAME as "firstName",
              ADDRESS_1 as "address1",
              ADDRESS_2 as "address2",
              CITY as "city",
              STATE as "state",
              ZIP_CODE as "zipCode"
        FROM CUSTOMER
        WHERE HOME_PHONE = phoneIn or WORK_PHONE = phoneIn
        ORDER BY LAST_NAME, FIRST_NAME;

    RETURN cur_cursor;
END;
.
/
