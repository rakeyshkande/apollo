CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PROD_EXCLUDED_STATES_2   (productId IN varchar2)
 RETURN types.ref_cursor
AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    SP_GET_PROD_EXCLUDED_STATES_2
-- Type:    Function
-- Syntax:  SP_GET_PROD_EXCLUDED_STATES_2 ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--          PRODUCT_ID              VARCHAR2(10)
--          EXCLUDED_STATE          VARCHAR2(2)
--          STATE_NAME              VARCHAR2(30)
--          SUN                     CHAR(1)
--          MON                     CHAR(1)
--          TUE                     CHAR(1)
--          WED                     CHAR(1)
--          THU                     CHAR(1)
--          FRI                     CHAR(1)
--          SAT                     CHAR(1)
--
-- Description:   Queries the PRODUCT_EXCLUDED_STATES table by PRODUCT ID and returns a ref cursor
--                to the resulting row.
--
--==================================================================
AS
    states_cursor types.ref_cursor;
BEGIN
    OPEN states_cursor FOR

         SELECT p.product_id        AS "productId",
                m.state_master_id   AS "excludedState",
                m.state_name        AS "stateName",
                p.sun               AS "sun",
                p.mon               AS "mon",
                p.tue               AS "tue",
                p.wed               AS "wed",
                p.thu               AS "thu",
                p.fri               AS "fri",
                p.sat               AS "sat"
        FROM ftd_apps.product_excluded_states p
        JOIN ftd_apps.state_master m ON m.state_master_id = p.excluded_state
        WHERE m.delivery_exclusion='Y'  and p.product_id=productId;

    RETURN states_cursor;
END;
.
/
