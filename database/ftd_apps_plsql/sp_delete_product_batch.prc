CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_PRODUCT_BATCH (inUserId in VARCHAR2, inProductId in VARCHAR2)
authid current_user
as
--==============================================================================
--
-- Name:    SP_DELETE_PRODUCT_BATCH
--
-- Description:   Deletes a row from PRODUCT_MASTER_BATCH by PRODUCT_ID and
--                LAST_UPDATE_USER_ID.
--
--==============================================================================
begin

  DELETE FROM PRODUCT_MASTER_BATCH
  WHERE LAST_UPDATE_USER_ID = inUserId
  AND PRODUCT_ID = inProductId;

end
;
.
/
