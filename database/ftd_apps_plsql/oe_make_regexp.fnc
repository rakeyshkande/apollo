CREATE OR REPLACE
FUNCTION ftd_apps.OE_MAKE_REGEXP ( formatStr IN VARCHAR2 )
RETURN VARCHAR2
IS
    latestNumber      NUMBER;
	literalMode		  BOOLEAN;
	resultStr		  VARCHAR2(100) := '/^';
	currentPos		  NUMBER := 1;
	currentChar		  CHAR(1);
BEGIN
    WHILE ( currentPos <= LENGTH(formatStr) )
	LOOP
		currentChar := SUBSTR(formatStr, currentPos, 1);

		IF ( currentChar = '''' )
		THEN
			IF ( literalMode )
			THEN
				literalMode := FALSE;
			ELSE
				literalMode := TRUE;
			END IF;

		ELSIF ( literalMode )
		THEN
			latestNumber := null;
			resultStr := resultStr || currentChar;

		ELSIF ( TRANSLATE(currentChar,'0123456789','@@@@@@@@@@') = '@' )
		THEN
			latestNumber := latestNumber || TO_NUMBER(currentChar);

		ELSIF ( currentChar = 'N' OR currentChar = 'n' )
		THEN
			resultStr := resultStr || '\d';
			IF ( latestNumber = 0 )
			THEN
				resultStr := resultStr || '*';
			ELSE
				resultStr := resultStr || '{' || latestNumber || '}';
			END IF;

		ELSIF ( currentChar = 'U' OR currentChar = 'u' )
		THEN
			resultStr := resultStr || '\d';
			IF ( latestNumber = 0 )
			THEN
				resultStr := resultStr || '*';
			ELSE
				resultStr := resultStr || '{1,' || latestNumber || '}';
			END IF;

		ELSIF ( currentChar = 'X' OR currentChar = 'x' )
		THEN
			resultStr := resultStr || '[0-9a-zA-Z]';
			IF ( latestNumber = 0 )
			THEN
				resultStr := resultStr || '*';
			ELSE
				resultStr := resultStr || '{' || latestNumber || '}';
			END IF;

		ELSIF ( currentChar = 'A' OR currentChar = 'a' )
		THEN
			resultStr := resultStr || '[a-zA-Z]';
			IF ( latestNumber = 0 )
			THEN
				resultStr := resultStr || '*';
			ELSE
				resultStr := resultStr || '{' || latestNumber || '}';
			END IF;
		ELSIF ( currentChar = 'M' OR currentChar = 'm' )
		THEN
			resultStr := resultStr || '[0-' || latestNumber || ']';
		END IF;
		currentPos := currentPos + 1;
	END LOOP;

  IF ( formatStr = 'Z' OR formatStr = 'z' )
		THEN
      RETURN '/[0-9a-zA-Z]/';
    ELSE
      resultStr := resultStr || '$/';
 	  IF ( currentChar = 'I' OR currentChar = 'i' )
		THEN
			resultStr := resultStr || 'i';
	  END IF;
  END IF;

	RETURN resultStr;
END;
.
/
