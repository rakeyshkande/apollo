create or replace procedure ftd_apps.SP_VALIDATE_PRODUCT_ID(
  IN_PRODUCT_ID IN VARCHAR2, 
  OUT_RESULT OUT VARCHAR2, 
  OUT_STATUS OUT VARCHAR2, 
  OUT_MESSAGE OUT VARCHAR2) is
--==============================================================================
--
-- Name:    SP_VALIDATE_PRODUCT_ID
-- Type:    Procedure
-- Syntax:  SP_VALIDATE_PRODUCT_ID ( <see args above> )
--
-- Description:   Determine if the passed id is a valid sku, product id, or novator id.
--
--==============================================================================
  v_product_id  PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_ID);
begin
OUT_RESULT := 'Y';
OUT_STATUS := 'Y';
BEGIN
-- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_product_id
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_product_id;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_product_id
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_product_id;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_product_id
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_product_id;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            OUT_RESULT := 'N';
          END;
        END;
    END; 
EXCEPTION WHEN OTHERS THEN
  OUT_RESULT := 'N';
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED ['||sqlcode||'] '||SUBSTR(sqlerrm,1,256);
END;  
end SP_VALIDATE_PRODUCT_ID;
/
