CREATE OR REPLACE TRIGGER ftd_apps.florist_blocks_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_blocks
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_blocks$ (
         FLORIST_ID,
         BLOCK_TYPE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         BLOCK_REASON,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.BLOCK_TYPE,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.BLOCKED_BY_USER_ID,
         :new.BLOCK_REASON,
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_blocks$ (
         FLORIST_ID,
         BLOCK_TYPE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         BLOCK_REASON,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.BLOCK_TYPE,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.BLOCKED_BY_USER_ID,
         :old.BLOCK_REASON,
         'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.florist_blocks$ (
         FLORIST_ID,
         BLOCK_TYPE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         BLOCK_REASON,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.BLOCK_TYPE,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.BLOCKED_BY_USER_ID,
         :new.BLOCK_REASON,
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_blocks$ (
         FLORIST_ID,
         BLOCK_TYPE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         BLOCK_REASON,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.BLOCK_TYPE,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.BLOCKED_BY_USER_ID,
         :old.BLOCK_REASON,
         'DEL',SYSDATE);

   END IF;

END;
/
