CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PRODUCTS_UPDATED_SINCE
(lastUpdateDateIn IN DATE)
 RETURN types.ref_cursor
AUTHID CURRENT_USER
--=======================================================================
--
-- Name:    OE_GET_PRODUCTS_UPDATED_SINCE
-- Type:    Function
-- Syntax:  OE_GET_PRODUCTS_UPDATED_SINCE(TO_DATE('31122010','DDMMYYYY'))
-- Returns: ref_cursor for
--          PRODUCT_ID          VARCHAR2(10)
--
-- Description:   Returns all products updated since the date/time passed in
--
--========================================================================

AS
    product_cursor types.ref_cursor;
BEGIN
    OPEN product_cursor FOR
        SELECT PRODUCT_ID
            FROM PRODUCT_MASTER
            WHERE LAST_UPDATE > lastUpdateDateIn;
    RETURN product_cursor;
END;
.
/
