CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PRODLIST_SHIP_COST (productId IN varchar2,
   shippingKeyId IN NUMBER,
   standardPrice IN NUMBER,
   shipMethodRank IN NUMBER)
    RETURN NUMBER
--=========================================================================
--
-- Name:    OE_GET_PRODLIST_SHIP_COST
-- Type:    Function
-- Syntax:  OE_GET_PRODLIST_SHIP_COST (productId IN VARCHAR,
--                                      shippingKeyId IN NUMBER,
--                                      standardPrice IN NUMBER,
--                                      shipMethodRank IN NUMBER)
-- Returns: tmp_shipcost  NUMBER
--
-- Description:   Gets shipping costs by product, shipper (shippingKeyId)
--                and price.  Shipping methods/costs are narrowed to those
--                available for the product and ordered by cost.
--
--=========================================================================
AS
  tmp_shipcost      NUMBER;
BEGIN
    -- Product delivery list view ranks delivery methods available
    --   for a product by cost from least cost to greatest
    SELECT pld.shipping_cost
    INTO tmp_shipcost
    FROM PRODUCT_DELIVERY_LIST_VW pld
    WHERE pld.product_id = productId
    AND pld.shipping_key_id = shippingKeyId
    AND pld.min_price <= standardPrice
    AND pld.max_price >= standardPrice
    AND pld.ship_method_rank = shipMethodRank;

    RETURN tmp_shipcost;

EXCEPTION WHEN OTHERS THEN
    RETURN null;
END
;
.
/
