CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_USER (
 userID   		    IN VARCHAR2,
 roleID           IN VARCHAR2,
 callCenterID     IN VARCHAR2,
 firstName        IN VARCHAR2,
 lastName 		    IN VARCHAR2,
 homePhone        IN VARCHAR2,
 address1 		    IN VARCHAR2,
 address2 		    IN VARCHAR2,
 userCity 		    IN VARCHAR2,
 stateCode        IN VARCHAR2,
 postalCode       IN VARCHAR2,
 currPassword     IN RAW,
 activeFlag       IN VARCHAR2,
 updateUser       IN VARCHAR2,
 lastUpdateDate   IN DATE
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_USER
-- Type:    Procedure
-- Syntax:  SP_UPDATE_USER ( <see args above> )
--
-- Description:   Performs an UPDATE/INSERT attempt on USER_PROFILE.
--
--==============================================================================

    tmp_date    DATE;

    -- Cursor handles row locking for updating of USER_PROFILE table
    CURSOR user_lock_cur (p_user_id  IN user_profile.user_id%TYPE) IS
        SELECT * FROM user_profile
        WHERE USER_ID = p_user_id
        FOR UPDATE;
    user_update_row    user_lock_cur%ROWTYPE;

begin

    -- Attempt to insert into USER_PROFILE
    INSERT INTO USER_PROFILE
      ( USER_ID,
        FIRST_NAME,
        LAST_NAME,
        CURRENT_PASSWORD,
        CURRENT_PASSWORD_DATE,
        ADDRESS_LINE_1,
        ADDRESS_LINE_2,
        CITY,
        STATE_CODE,
        POSTAL_CODE,
        HOME_PHONE,
        CALL_CENTER_ID,
        ROLE_ID,
        ACTIVE_FLAG,
        LOGIN_ATTEMPTS,
        DELETED_FLAG,
        CREATED_BY,
        CREATED_DATE,
        LAST_UPDATED_BY,
        LAST_UPDATED_DATE
      )
    VALUES
      ( userID,
        firstName,
        lastName,
        currPassword,
        SYSDATE,
        address1,
        address2,
        userCity,
        stateCode,
        postalCode,
        homePhone,
        callCenterID,
        roleID,
        activeFlag,
        0,
        'N',
        updateUser,
        SYSDATE,
        updateUser,
        to_date(to_char(SYSDATE,'MMDDYYYYHH24MISS'),'MMDDYYYYHH24MISS')
      );

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

    -- Lock the existing row on USER_PROFILE for update
    OPEN user_lock_cur(userID);
    FETCH user_lock_cur INTO user_update_row;

    -- Error if the last update date on existing row differs from the input parameter and
	-- and the row was not previously deleted
    IF ( (user_update_row.last_updated_date <> lastUpdateDate) and (user_update_row.deleted_flag = 'N') )
    THEN
        CLOSE user_lock_cur;
        COMMIT;
        RAISE_APPLICATION_ERROR(-20001, 'User update for ID ' || userID || ' failed due to last_update date mismatch. - TABLE: ' || to_char(user_update_row.last_updated_date,'MMDDYYYYHH24MISS') || ' PARM: ' || to_char(lastUpdateDate,'MMDDYYYYHH24MISS') || '*');
    END IF;

    UPDATE USER_PROFILE
    SET FIRST_NAME = firstName,
        LAST_NAME = lastName,
        CURRENT_PASSWORD = currPassword,
        CURRENT_PASSWORD_DATE = DECODE(current_password, currPassword, current_password_date, SYSDATE),
        ADDRESS_LINE_1 = address1,
        ADDRESS_LINE_2 = address2,
        CITY = userCity,
        STATE_CODE = stateCode,
        POSTAL_CODE = postalCode,
        HOME_PHONE = homePhone,
        CALL_CENTER_ID = callCenterID,
        ROLE_ID = roleID,
        ACTIVE_FLAG = activeFlag,
        DELETED_FLAG = 'N',
        LAST_UPDATED_BY = updateUser,
        LAST_UPDATED_DATE = to_date(to_char(SYSDATE,'MMDDYYYYHH24MISS'),'MMDDYYYYHH24MISS')
    WHERE CURRENT OF user_lock_cur;

    -- Release the row lock on USER_PROFILE with a commit
    CLOSE user_lock_cur;
    COMMIT;

end
;
.
/
