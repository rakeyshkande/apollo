CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_EXCLUDED_STATES (productId IN varchar2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_EXCLUDED_STATES
-- Type:    Function
-- Syntax:  SP_GET__PRODUCT_EXCLUDED_STATES ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--          PRODUCT_ID              VARCHAR2(10)
--          EXCLUDED_STATE          VARCHAR2(2)
--
-- Description:   Queries the PRODUCT_EXCLUDED_STATES table by PRODUCT ID and returns a ref cursor
--                to the resulting row.
--
--==================================================================
AS
    states_cursor types.ref_cursor;
BEGIN
    OPEN states_cursor FOR
        SELECT PRODUCT_ID     as "productId",
               EXCLUDED_STATE as "excludedState"
          FROM PRODUCT_EXCLUDED_STATES
         WHERE PRODUCT_ID = productId;

    RETURN states_cursor;
END
;
.
/
