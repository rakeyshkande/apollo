CREATE OR REPLACE PROCEDURE FTD_APPS.SP_UPDATE_PRODUCT_POPULARITY 
   (IN_NUM_DAYS_PAST       IN    NUMBER      DEFAULT 7,
    OUT_STATUS             OUT   VARCHAR2,
    OUT_MESSAGE            OUT   VARCHAR2) 
AS
/*-----------------------------------------------------------------------------

 Name:    SP_UPDATE_PRODUCT_POPULARITY
 Type:    Procedure
 Syntax:  SP_UPDATE_PRODUCT_POPULARITY(num_days, status, err_msg)

Description:
        This stored procedure will reset product_master popularity_order_cnt
        to zero, then recalculate for orders since a number of days past.
        
Input:
        in_num_days_past    number
        
Output:
        out_status          varchar2    Y if Successful, N if Error
        out_message         varchar2    loaded with oracle error if status=N

-----------------------------------------------------------------------------*/

-- Calculate Popularity (avoid using "delivery_date >=" - results in FTS)
cursor pop_cursor is
   select product_id,
          count(*) order_cnt
   from clean.order_details
   where delivery_date between sysdate - in_num_days_past and sysdate
   group by product_id;


BEGIN

  OUT_STATUS := 'Y';

   -- Reset Popularity
   begin
      update product_master
      set popularity_order_cnt = 0
      where popularity_order_cnt != 0
         or popularity_order_cnt is null;
   
   exception
      when others then
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED RESETTING POPULARITY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   
        RETURN;
   end;

   for pop_rec in pop_cursor LOOP
      begin
        update product_master
        set popularity_order_cnt = pop_rec.order_cnt,
            updated_by           = 'sp_update_product_popularity proc',
            updated_on           = sysdate
        where product_id = pop_rec.product_id;
        
        --if sql%rowcount > 0 then
        --   dbms_output.put_line('Update Product ' || pop_rec.product_id ||
        --       ' popularity to ' || pop_rec.order_cnt);
        --else
        --   dbms_output.put_line('Product ' || pop_rec.product_id ||
        --       ' has ' || pop_rec.order_cnt || ' orders, but NO product_master entry!!');
        --end if;

      exception
         when others then
           ROLLBACK;
           OUT_STATUS := 'N';
           OUT_MESSAGE := 'ERROR OCCURRED UPDATING POPULARITY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   
           RETURN;
      end;
   END LOOP;

   -- everything went good!
   commit;   


/* Trouble? */
EXCEPTION
   when others then
      ROLLBACK;
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'UNEXPECTED ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   

END SP_UPDATE_PRODUCT_POPULARITY;
/