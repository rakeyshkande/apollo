CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_EXCLUDE_STATE_ANF (
 productId in varchar2,
 excludeState in varchar2,
 sun in varchar2,
 mon in varchar2,
 tue in varchar2,
 wed in varchar2,
 thu in varchar2,
 fri in varchar2,
 sat in varchar2
)
authid current_user
as
--==============================================================================
--
-- Name:    SP_UPDATE_EXCLUDE_STATE_ANF
-- Type:    Procedure
-- Syntax:  SP_UPDATE_EXCLUDE_STATE_ANF ( productId in varchar2,
--                                      excludeState in varchar2,
--                                      sun in varchar2,
--                                      mon in varchar2,
--                                      tue in varchar2,
--                                      wed in varchar2,
--                                      thu in varchar2,
--                                      fri in varchar2,
--                                      sat in varchar2)
--
-- Description:   Attempts to insert a row into PRODUCT_EXCLUDED_STATES.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_EXCLUDED_STATES
  INSERT INTO PRODUCT_EXCLUDED_STATES
      ( PRODUCT_ID, EXCLUDED_STATE, SUN, MON, TUE, WED, THU, FRI, SAT )
  VALUES
      ( productId,
        excludeState,
        sun,mon,tue,wed,thu,fri,sat
      );

end;
.
/
