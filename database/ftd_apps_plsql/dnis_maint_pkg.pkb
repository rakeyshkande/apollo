CREATE OR REPLACE
PACKAGE BODY ftd_apps.DNIS_MAINT_PKG
AS

PROCEDURE INSERT_DNIS
(
IN_DNIS_ID                      IN DNIS.DNIS_ID%TYPE,
IN_DESCRIPTION                  IN DNIS.DESCRIPTION%TYPE,
IN_ORIGIN                       IN DNIS.ORIGIN%TYPE,
IN_STATUS_FLAG                  IN DNIS.STATUS_FLAG%TYPE,
IN_OE_FLAG                      IN DNIS.OE_FLAG%TYPE,
IN_YELLOW_PAGES_FLAG            IN DNIS.YELLOW_PAGES_FLAG%TYPE,
IN_SCRIPT_CODE                  IN DNIS.SCRIPT_CODE%TYPE,
IN_DEFAULT_SOURCE_CODE          IN DNIS.DEFAULT_SOURCE_CODE%TYPE,
IN_SCRIPT_ID                    IN DNIS.SCRIPT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting new dnis information

Input:
        dnis_id                         number
        description                     varchar2
        origin                          varchar2
        status_flag                     varchar2
        oe_flag                         varchar2
        yellow_pages_flag               varchar2
        script_code                     varchar2
        default_source_code             varchar2
        script_id                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO dnis
(
        dnis_id,
        description,
        origin,
        status_flag,
        oe_flag,
        yellow_pages_flag,
        script_code,
        default_source_code,
        script_id,
        company_id,
        script_type_code
)
VALUES
(
        in_dnis_id,
        in_description,
        in_origin,
        in_status_flag,
        in_oe_flag,
        in_yellow_pages_flag,
        in_script_code,
        in_default_source_code,
        in_script_id,
        in_script_id,
        'Default'
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_DNIS;


PROCEDURE UPDATE_DNIS
(
IN_DNIS_ID                      IN DNIS.DNIS_ID%TYPE,
IN_DESCRIPTION                  IN DNIS.DESCRIPTION%TYPE,
IN_ORIGIN                       IN DNIS.ORIGIN%TYPE,
IN_STATUS_FLAG                  IN DNIS.STATUS_FLAG%TYPE,
IN_OE_FLAG                      IN DNIS.OE_FLAG%TYPE,
IN_YELLOW_PAGES_FLAG            IN DNIS.YELLOW_PAGES_FLAG%TYPE,
IN_SCRIPT_CODE                  IN DNIS.SCRIPT_CODE%TYPE,
IN_DEFAULT_SOURCE_CODE          IN DNIS.DEFAULT_SOURCE_CODE%TYPE,
IN_SCRIPT_ID                    IN DNIS.SCRIPT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating dnis information

Input:
        dnis_id                         number
        description                     varchar2
        origin                          varchar2
        status_flag                     varchar2
        oe_flag                         varchar2
        yellow_pages_flag               varchar2
        script_code                     varchar2
        default_source_code             varchar2
        script_id                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  dnis
SET
        description = NVL(in_description, description),
        origin = NVL(in_origin, origin),
        status_flag = NVL(in_status_flag, status_flag),
        oe_flag = NVL(in_oe_flag, oe_flag),
        yellow_pages_flag = NVL(in_yellow_pages_flag, yellow_pages_flag),
        script_code = NVL(in_script_code, script_code),
        default_source_code = NVL(in_default_source_code, default_source_code),
        script_id = NVL(in_script_id, script_id),
        company_id = NVL(in_script_id, company_id)
WHERE   dnis_id = in_dnis_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_DNIS;


END;
.
/
