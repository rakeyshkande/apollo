CREATE OR REPLACE
PROCEDURE ftd_apps.SP_INSERT_CPN_MAP (
    sourceCodeIn in varchar2,
	productIDIn in varchar2,
    CPNIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSERT_CPN_MAP
-- Type:    Procedure
-- Syntax:  SP_INSERT_CPN_MAP ( asnNumberIn in varchar2,
--                                    clientNameIn in varchar2,
--                                      urlIn in varchar2)
--
-- Description:   Inserts a row to the CLIENT_URL table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 insert into CPN_MAP
 (SOURCE_CODE,PRODUCT_ID,CPN)
 VALUES(sourceCodeIn,productIDIn,CPNIn);

end
;
.
/
