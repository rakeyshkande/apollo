CREATE OR REPLACE TRIGGER "FTD_APPS"."DELIVERY_FEE_$" 
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.DELIVERY_FEE REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW

DECLARE

   c utl_smtp.connection; 
   v_location VARCHAR2(10);

   CURSOR pagers_c (p_project IN VARCHAR2) IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = p_project;

   PROCEDURE send_email(v_action IN VARCHAR2, v_delivery_fee_id IN VARCHAR2,
      v_description IN VARCHAR2, v_morning_delivery_fee IN NUMBER) AS
   BEGIN
      c := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server','NAME'));
      utl_smtp.helo(c, 'ftdi.com');
      utl_smtp.mail(c, 'oracle@ftdi.com');
      FOR pagers_r IN pagers_c ('SERVICE_FEE') LOOP
         utl_smtp.rcpt(c, pagers_r.pager_number);
      END LOOP;
      utl_smtp.open_data(c);
      utl_smtp.write_data(c, 'From: ' || v_location || utl_tcp.CRLF);
      FOR pagers_r IN pagers_c ('SERVICE_FEE') LOOP
         utl_smtp.write_data(c, 'To: ' || pagers_r.pager_number || utl_tcp.CRLF);
      END LOOP;
      utl_smtp.write_data(c, 'Subject: Morning Delivery Fee ' || v_action || utl_tcp.CRLF);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'ID:           ' || v_delivery_fee_id);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Action:                   ' || v_action);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Morning Delivery Fee:        ' || TO_CHAR(v_morning_delivery_fee,'999.99'));
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Description:              ' || v_description);
      utl_smtp.close_data(c);
      utl_smtp.quit(c);
   END;

BEGIN

   SELECT sys_context('USERENV','DB_NAME')
   INTO   v_location
   FROM   dual;

   IF INSERTING THEN

      INSERT INTO ftd_apps.delivery_fee$ (
      	delivery_fee_id,
        description,        
        requested_by,
        morning_delivery_fee,
        created_by,
        created_on,
        updated_by,
        updated_on,
        operation$,
        timestamp$
)
      VALUES (
        :NEW.delivery_fee_id,
        :NEW.description,        
        :NEW.requested_by,
        :NEW.morning_delivery_fee,
        :NEW.created_by,
        :NEW.created_on,
        :NEW.updated_by,
        :NEW.updated_on,
        'INS',
        sysdate
      );
      
      
	send_email('Insert',:NEW.delivery_fee_id,:NEW.description,:NEW.morning_delivery_fee);
      
   ELSIF UPDATING  THEN

      -- setting created_by = 'DELETION' means we are about to physically delete the row, but
      -- we first want to get the row into the delivery_fee_hist table. This logic avoids 
      -- cluttering this shadow table with updates when the delete is about to happen.
      IF :NEW.created_by <> 'DELETION' THEN

         INSERT INTO ftd_apps.delivery_fee$ (
         	delivery_fee_id,
			description,        
			requested_by,
			morning_delivery_fee,
			created_by,
			created_on,
			updated_by,
			updated_on,
			operation$,
			timestamp$
         ) VALUES (
        	:OLD.delivery_fee_id,
        	:OLD.description,
        	:OLD.requested_by,
			:OLD.morning_delivery_fee,
        	:OLD.created_by,
        	:OLD.created_on,
        	:OLD.updated_by,
        	:OLD.updated_on,
        	'UPD_OLD',
        	SYSDATE
         );

        INSERT INTO ftd_apps.delivery_fee$ (
        	delivery_fee_id,
			description,        
			requested_by,
			morning_delivery_fee,
			created_by,
			created_on,
			updated_by,
			updated_on,
			operation$,
			timestamp$
		) VALUES (
        	:NEW.delivery_fee_id,
        	:NEW.description,
        	:NEW.requested_by,
			:NEW.morning_delivery_fee,
        	:NEW.created_by,
        	:NEW.created_on,
        	:NEW.updated_by,
        	:NEW.updated_on,
        	'UPD_NEW',
        	SYSDATE
        );
        
         -- if the only thing that changed was the description then do not send the email.
         if(:NEW.delivery_fee_id            <> :OLD.delivery_fee_id            OR
            :NEW.morning_delivery_fee       <> :OLD.morning_delivery_fee) THEN
             send_email('Update',:NEW.delivery_fee_id,:NEW.description,:NEW.morning_delivery_fee);
          end if;

		END IF;

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.delivery_fee$ (
		delivery_fee_id,
		description,        
		requested_by,
		morning_delivery_fee,
		created_by,
		created_on,
		updated_by,
		updated_on,
		operation$,
		timestamp$
      ) VALUES (
      	:OLD.delivery_fee_id,
		:OLD.description,
		:OLD.requested_by,
		:OLD.morning_delivery_fee,
		:OLD.created_by,
		:OLD.created_on,
		:OLD.updated_by,
		:OLD.updated_on,
		'DEL',
		SYSDATE
     );
     
      send_email('Delete',:OLD.delivery_fee_id,:OLD.description,:OLD.morning_delivery_fee);

    END IF;

END;

/
ALTER TRIGGER "FTD_APPS"."DELIVERY_FEE_$" ENABLE;
