CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_CARRIER_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_CARRIER_LIST
-- Type:    Function
-- Syntax:  SP_GET_CARRIER_LIST ()
-- Returns: ref_cursor
--
-- Description:   Queries the carrier_info table for a list of all carriers
--
--==================================================\

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
    select carrier_id,carrier_name from venus.carriers order by carrier_name;
    RETURN cur_cursor;
END
;
.
/
