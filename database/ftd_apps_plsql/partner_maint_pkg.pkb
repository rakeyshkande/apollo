CREATE OR REPLACE PACKAGE BODY FTD_APPS.PARTNER_MAINT_PKG AS

PROCEDURE UPDATE_PARTNER_PRODUCT (
    IN_PRODUCT_SUBCODE_ID IN  PARTNER_PRODUCTS.PRODUCT_SUBCODE_ID%TYPE,
    IN_PARTNER_ID         IN  PARTNER_PRODUCTS.PARTNER_ID%TYPE,
    IN_WHOLESALE_PRICE    IN  PARTNER_PRODUCTS.WHOLESALE_PRICE_AMT%TYPE,
    IN_AVAILABLE_FLAG     IN  PARTNER_PRODUCTS.AVAILABLE_FLAG%TYPE,
    OUT_STATUS            OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts or updates record in the PARTNER_PRODUCTS table for the passed in
        parameters.  Return 'Y' on success or 'N' on failure.

Input:
        product_subcode_id        varchar2
        partner_id                varchar2
        wholesale_price           number
        available_flag            varchar2

Output:
        status            varchar2
        error message     varchar2

-----------------------------------------------------------------------------*/
BEGIN
  BEGIN
    INSERT INTO PARTNER_PRODUCTS (
      PRODUCT_SUBCODE_ID, 
      PARTNER_ID, 
      WHOLESALE_PRICE_AMT, 
      AVAILABLE_FLAG
    ) VALUES (
       IN_PRODUCT_SUBCODE_ID,
       IN_PARTNER_ID,
       IN_WHOLESALE_PRICE,
       IN_AVAILABLE_FLAG
    );
   EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
     UPDATE PARTNER_PRODUCTS
       SET 
         WHOLESALE_PRICE_AMT = IN_WHOLESALE_PRICE,
         AVAILABLE_FLAG = IN_AVAILABLE_FLAG
       WHERE 
         PRODUCT_SUBCODE_ID = IN_PRODUCT_SUBCODE_ID AND 
         PARTNER_ID = IN_PARTNER_ID;
   END;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END UPDATE_PARTNER_PRODUCT;

END;
.
/
