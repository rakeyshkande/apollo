CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_INSTITUTION_TYPES RETURN types.ref_cursor
--==========================================================================
--
-- Name:    OE_GET_INSTITUTION_TYPES
-- Type:    Function
-- Syntax:  OE_GET_INSTITUTION_TYPES()
-- Returns: ref_cursor for
--          institutionType     VARCHAR2(1)
--          description         VARCHAR2(12)
--
-- Description:   Queries the INSTITUTION_TYPES table and returns all rows.
--
--==========================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT TYPE as "institutionType",
            DESCRIPTION as "description"
        FROM INSTITUTION_TYPE
        ORDER BY TYPE;

    RETURN cur_cursor;
END
;
.
/
