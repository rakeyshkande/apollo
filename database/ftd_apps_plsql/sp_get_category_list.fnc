CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_CATEGORY_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_CATEGORY_LIST
-- Type:    Function
-- Syntax:  SP_GET_CATEGORY_LIST ( )
-- Returns: ref_cursor for
--          PRODUCT_CATEGORY_ID     VARCHAR2(10)
--          DESCRIPTION             VARCHAR2(40)
--
-- Description:   Queries the product_category table and returns a ref cursor for row.
--
--===========================================================

AS
    category_cursor types.ref_cursor;
BEGIN
    OPEN category_cursor FOR
        SELECT PRODUCT_CATEGORY_ID as "productCategoryId",
               DESCRIPTION as "description"
          FROM PRODUCT_CATEGORY order by DESCRIPTION;

    RETURN category_cursor;
END
;
.
/
