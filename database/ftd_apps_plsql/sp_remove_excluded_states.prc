CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_EXCLUDED_STATES (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_EXCLUDED_STATES
-- Type:    Procedure
-- Syntax:  SP_REMOVE_EXCLUDED_STATES ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_EXCLUDED_STATES by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_EXCLUDED_STATES
  where PRODUCT_ID = productId;

end
;
.
/
