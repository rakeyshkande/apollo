CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_STATE_DETAILS (inStateCode IN VARCHAR2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_STATE_DETAILS
-- Type:    Function
-- Syntax:  OE_GET_STATE_DETAILS (inStateCode IN VARCHAR2 )
-- Returns: stateName            VARCHAR2(30)
--          countryCode          VARCHAR2(3)
--          timeZone             VARCHAR2(2)
--          taxRate              NUMBER(5)
--
-- Description:   Queries the STATE_MASTER table by STATE_MASTER_ID, returns the results
--
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN

  OPEN cur_cursor FOR
      SELECT s.STATE_NAME as "stateName",
            s.COUNTRY_CODE as "countryCode",
            s.TIME_ZONE as "timeZone"
      FROM STATE_MASTER s
      WHERE s.STATE_MASTER_ID = inStateCode;

  RETURN cur_cursor;
END
;
.
/
