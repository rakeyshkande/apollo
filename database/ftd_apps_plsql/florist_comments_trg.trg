CREATE OR REPLACE TRIGGER ftd_apps.florist_comments_trg
BEFORE INSERT  ON ftd_apps.florist_comments
REFERENCING OLD AS old NEW AS new   
FOR EACH ROW
   
DECLARE
   
  v_florist_comment_id number;
   
BEGIN

   SELECT ftd_apps.florist_comments_id_seq.nextval INTO v_florist_comment_id FROM dual;
  
   
   -- Update create_date field to current system date
   :new.comment_id := v_florist_comment_id ;
   
END;
/
