CREATE OR REPLACE
FUNCTION ftd_apps.oe_vldate_srce_code_md04 (
    inSourceCode          IN VARCHAR2
 )
 RETURN types.ref_cursor
 AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    oe_vldate_srce_code_md04
-- Type:    Function
-- Syntax:  oe_vldate_srce_code_md04 ( inSourceCode IN VARCHAR2, inDnisScriptCode IN VARCHAR2  )
-- Returns: ref_cursor for
--          SOURCE_CODE                 VARCHAR2(10)
--          jcPenneyFlag                VARCHAR2(1)
--          EXPIRED_FLAG                VARCHAR2(1)
--
-- Description:   Queries the SOURCE table for the given source code and returns
--                the source code if found.  Used for validation.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN

    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            NVL(JCPENNEY_FLAG, 'N') as "jcPenneyFlag",
                              DECODE(COMPANY_ID, 'SFMB', 'Y', 'N') as "sfmbFlag",
                              DECODE(COMPANY_ID, 'GIFT', 'Y', 'N') as "giftFlag",
                              DECODE(COMPANY_ID, 'HIGH', 'Y', 'N') as "highFlag",
                              DECODE(COMPANY_ID, 'GSC', 'Y', 'N')  as "gscFlag",
                              DECODE(COMPANY_ID, 'FUSA', 'Y', 'N')  as "fusaFlag",
                              DECODE(COMPANY_ID, 'FLORIST', 'Y', 'N')  as "floristcomFlag",
                              DECODE(COMPANY_ID, 'FDIRECT', 'Y', 'N')  as "fdirectFlag",
            DECODE(END_DATE, NULL, 'N',
            DECODE(SIGN(END_DATE - TRUNC(SYSDATE)), 0, 'N', 1, 'N', 'Y')) as "expiredFlag"
      FROM SOURCE
      WHERE SOURCE_CODE = inSourceCode;

    RETURN cur_cursor;
END;
.
/
