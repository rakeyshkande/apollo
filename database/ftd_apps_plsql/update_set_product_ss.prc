CREATE OR REPLACE PROCEDURE FTD_APPS.UPDATE_SET_PRODUCT_SS (
   IN_VENDOR_ID 			IN VARCHAR2,
   OUT_STATUS   			OUT VARCHAR2,
   OUT_MESSAGE				OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the shipping system for all
        the products associated with a given vendor, in product master to the 
        value passed in, from PDB UI.
        It is only a one time thing. To switch the vendors from Escalate 
        shipping system to SDS/Argo. Once all the vendors using Escalate shipping 
        system have been moved to SDS/Argo, this procedure is not needed any longer.

Input:
        in_vendor_id                    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

	CURSOR cur_mult_vendor IS
	SELECT a.product_subcode_id 
	  FROM vendor_product a 
	 WHERE a.vendor_id = in_vendor_id
	   AND a.removed = 'N'
		AND EXISTS (SELECT 1 
		              FROM vendor_product b 
		             WHERE b.vendor_id != in_vendor_id 
		               AND b.product_subcode_id = a.product_subcode_id
		               AND b.removed = 'N');
		               
   v_product_subcode_id			vendor_product.product_subcode_id%TYPE := NULL;	 
   
BEGIN

   OPEN cur_mult_vendor;
   FETCH cur_mult_vendor INTO v_product_subcode_id;
   CLOSE cur_mult_vendor;
   
   IF v_product_subcode_id IS NOT NULL THEN
   
      out_status := 'N';
      out_message := 'WARNING: Product '|| v_product_subcode_id || ' is NOT carried just by vendor ' || in_vendor_id ||'.';
      
   ELSE

		UPDATE product_master a
			SET shipping_system = 'SDS'
		 WHERE product_id IN (
				SELECT product_subcode_id 
				  FROM vendor_product b
				 WHERE vendor_id = in_vendor_id
					AND b.product_subcode_id = a.product_id);
					
      IF SQL%FOUND THEN
	      out_status := 'Y';
	   ELSE
			out_status := 'Y';
			out_message := 'No products found in Product Master for Vendor ' || in_vendor_id ||'.';
	   END IF;					
					
      UPDATE product_master a
			SET shipping_system = 'SDS'
		 WHERE product_id IN (
			SELECT c.product_id 
			  FROM vendor_product b, product_subcodes c
			 WHERE b.vendor_id = in_vendor_id
				AND b.product_subcode_id = c.product_subcode_id
				AND c.product_id = a.product_id);		   					
		 
	   IF SQL%FOUND THEN
	      out_status := 'Y';
	   ELSE
			out_status := 'Y';
			out_message := 'No products found in Product Subcodes for Vendor ' || in_vendor_id ||'.';
	   END IF;
	   
	END IF;

EXCEPTION
   WHEN OTHERS THEN
	   out_status := 'N';
	   out_message := 'ERROR OCCURRED IN FTD_APPS.UPDATE_SET_PRODUCT_SS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END UPDATE_SET_PRODUCT_SS;
.
/