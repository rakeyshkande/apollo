-- FTD_APPS.SHIPPING_KEYS_$ trigger to populate FTD_APPS.SHIPPING_KEYS$ shadow table

CREATE OR REPLACE TRIGGER FTD_APPS.shipping_keys_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.shipping_keys REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.shipping_keys$ (
      	shipping_key_id,
      	shipping_key_description,
      	shipper,
      	tracking_url,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.shipping_key_id,
      	:NEW.shipping_key_description,
      	:NEW.shipper,
      	:NEW.tracking_url,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.shipping_keys$ (
      	shipping_key_id,
      	shipping_key_description,
      	shipper,
      	tracking_url,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.shipping_key_id,
      	:OLD.shipping_key_description,
      	:OLD.shipper,
      	:OLD.tracking_url,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO ftd_apps.shipping_keys$ (
      	shipping_key_id,
      	shipping_key_description,
      	shipper,
      	tracking_url,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.shipping_key_id,
      	:NEW.shipping_key_description,
      	:NEW.shipper,
      	:NEW.tracking_url,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.shipping_keys$ (
      	shipping_key_id,
      	shipping_key_description,
      	shipper,
      	tracking_url,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.shipping_key_id,
      	:OLD.shipping_key_description,
      	:OLD.shipper,
      	:OLD.tracking_url,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
