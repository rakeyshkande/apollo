CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_GIFT_CERTIFICATE_MD04 (inGiftCertificate IN VARCHAR2,
                                   inCompanyId IN VARCHAR2
    )
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_GIFT_CERTIFICATE_MD04
-- Type:    Function
-- Syntax:  OE_GET_GIFT_CERTIFICATE_MD04 ( inGiftCertificate IN VARCHAR2,
--                                    inCompanyId IN VARCHAR2)
-- Returns: ref_cursor for
--          giftCertificateId       VARCHAR2(10)
--          certificateAmount       NUMBER(7.2)
--          expirationDate          VARCHAR2(10)
--          issueDate               VARCHAR2(10)
--          redemptionFlag          VARCHAR2(1)
--          companyId               VARCHAR2(12)
--
-- Description:   Queries the GIFT_CERTIFICATE_MASTER table by GIFT_CERTIFICATE_ID
--                and company ID.  Normal gift certificates will have null
--                company ID.  Gift certificates for specific purposes (e.g.
--                Gift Store Card) will have company ID defined.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
/*
    OPEN cur_cursor FOR
        SELECT GIFT_CERTIFICATE_ID as "giftCertificateId",
            CERTIFICATE_AMOUNT as "certificateAmount",
            TO_CHAR(EXPIRATION_DATE, 'mm/dd/yyyy') as "expirationDate",
            TO_CHAR(ISSUE_DATE, 'mm/dd/yyyy') as "issueDate",
            REDEMPTION_FLAG as "redemptionFlag",
            COMPANY_ID as "companyId"
        FROM GIFT_CERTIFICATE_MASTER
        WHERE GIFT_CERTIFICATE_ID = UPPER(inGiftCertificate)
        AND   NVL(COMPANY_ID,'NVL') = NVL(inCompanyId,'NVL');
*/

        OPEN cur_cursor FOR
                SELECT  gcc.gc_coupon_number "giftCertificateId",
                        gcc.issue_amount "certificateAmount",
                        to_char (gcc.expiration_date, 'mm/dd/yyyy') "expirationDate",
                        to_char (gcc.issue_date, 'mm/dd/yyyy') "issueDate",
                        decode (gcc.gc_coupon_status, 'Redeemed', 'Y', 'N') "redemptionFlag",
                        null "companyId" -- gcr.company_id "companyId"
                FROM    clean.gc_coupons gcc
                JOIN    clean.gc_coupon_request gcr
                ON      gcc.request_number = gcr.request_number
                WHERE   upper (gcc.gc_coupon_number) = upper (inGiftCertificate)
                AND     gcc.gc_coupon_status IN ('Reinstate', 'Redeemed', 'Issued Active');
                --AND     nvl (gcr.company_id, 'NVL') = nvl (inCompanyId, 'NVL');


    RETURN cur_cursor;
END;
.
/
