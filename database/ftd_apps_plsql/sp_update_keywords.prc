CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_KEYWORDS (
 productId in varchar2,
 productKeyword in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_KEYWORDS
-- Type:    Procedure
-- Syntax:  SP_UPDATE_KEYWORDS ( productId in varchar2,
--                               productKeyword in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_KEYWORDS.
--                If the product/keyword combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_KEYWORDS
  INSERT INTO PRODUCT_KEYWORDS
      ( PRODUCT_ID,
				KEYWORD
		  )
  VALUES
		  ( productId,
				productKeyword
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		  rollback;
end
;
.
/
