CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_HOLIDAYS_BY_COUNTRY (countryId IN VARCHAR2)
 RETURN types.ref_cursor
 --==============================================================
--
-- Name:    OE_GET_HOLIDAYS_BY_COUNTRY
-- Type:    Function
-- Syntax:  OE_GET_HOLIDAYS_BY_COUNTRY
-- Returns: ref_cursor for
--          holidayDate           VARCHAR2(10)
--          holidayDescription    VARCHAR2(50)
--          deliverableFlag       VARCHAR2(1)
--
-- Description:   Queries HOLIDAY_COUNTRY by supplied country ID.
--
--===============================================================

AS
   cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT TO_CHAR(c.HOLIDAY_DATE, 'mm/dd/yyyy') as "holidayDate",
                h.HOLIDAY_DESCRIPTION as "holidayDescription",
                c.DELIVERABLE_FLAG as "deliverableFlag",
                c.SHIPPING_ALLOWED as "shippingAllowed"
        FROM HOLIDAY_COUNTRY c, HOLIDAY_CALENDAR h
        WHERE c.COUNTRY_ID = countryId
        AND h.HOLIDAY_DATE = c.HOLIDAY_DATE
        ORDER BY c.HOLIDAY_DATE;

    RETURN cur_cursor;
END
;
.
/
