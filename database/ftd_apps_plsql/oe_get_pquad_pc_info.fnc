CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PQUAD_PC_INFO (IN_PRODUCT_ID IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_PQUAD_PC_INFO
-- Type:    Function
--
-- Description:   Queries the PRODUCT_MASTER table by PRODUCT_ID and
--                returns a ref cursor for resulting row 
--                which contains PQuad (FTD West) Personal Creations data.
--
--===================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT PQUAD_PC_ID,
               PQUAD_PC_DISPLAY_NAMES,
               PERSONALIZATION_TEMPLATE_ORDER,
               PERSONALIZATION_CASE_FLAG
          FROM ftd_apps.PRODUCT_MASTER
         WHERE PRODUCT_ID = IN_PRODUCT_ID;

    RETURN cur_cursor;
END;
.
/
