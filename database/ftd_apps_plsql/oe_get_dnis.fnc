CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_DNIS RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_DNIS
-- Type:    Function
-- Syntax:  OE_GET_DNIS ( )
-- Returns: ref_cursor for
--          DNIS_ID                 NUMBER(4)
--          DESCRIPTION             VARCHAR2(40)
--          ORIGIN                  VARCHAR2(10)
--          STATUS_FLAG             VARCHAR2(1)
--          OE_FLAG                 VARCHAR2(1)
--          YELLOW_PAGES_FLAG       VARCHAR2(1)
--          SCRIPT_CODE             VARCHAR2(2)
--          DEFAULT_SOURCE_CODE     VARCHAR2(10)
--
-- Description:   Queries the DNIS table and returns a ref cursor for all rows.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT DNIS_ID             as "dnisId",
               DESCRIPTION         as "description",
               ORIGIN              as "origin",
               STATUS_FLAG         as "statusFlag",
               OE_FLAG             as "oeFlag",
               YELLOW_PAGES_FLAG   as "yellowPagesFlag",
               SCRIPT_CODE         as "scriptCode",
               DEFAULT_SOURCE_CODE as "defaultSourceCode"
        FROM DNIS;

    RETURN cur_cursor;
END
;
.
/
