CREATE OR REPLACE TRIGGER ftd_apps.florist_zips_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_zips
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_zips$ (
         FLORIST_ID,      
         ZIP_CODE,        
         LAST_UPDATE_DATE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         CUTOFF_TIME,     
         ASSIGNMENT_TYPE ,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.ZIP_CODE,
         :new.LAST_UPDATE_DATE,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.CUTOFF_TIME,
         :new.ASSIGNMENT_TYPE,
         'INS',SYSTIMESTAMP);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_zips$ (
         FLORIST_ID,      
         ZIP_CODE,        
         LAST_UPDATE_DATE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         CUTOFF_TIME,     
         ASSIGNMENT_TYPE ,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.ZIP_CODE,
         :old.LAST_UPDATE_DATE,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.CUTOFF_TIME,
         :old.ASSIGNMENT_TYPE,
         'UPD_OLD',SYSTIMESTAMP);

      INSERT INTO ftd_apps.florist_zips$ (
         FLORIST_ID,      
         ZIP_CODE,        
         LAST_UPDATE_DATE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         CUTOFF_TIME,     
         ASSIGNMENT_TYPE ,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.ZIP_CODE,
         :new.LAST_UPDATE_DATE,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.CUTOFF_TIME,
         :new.ASSIGNMENT_TYPE,
         'UPD_NEW',SYSTIMESTAMP);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_zips$ (
         FLORIST_ID,      
         ZIP_CODE,        
         LAST_UPDATE_DATE,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         CUTOFF_TIME,     
         ASSIGNMENT_TYPE ,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.ZIP_CODE,
         :old.LAST_UPDATE_DATE,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.CUTOFF_TIME,
         :old.ASSIGNMENT_TYPE,
         'DEL',SYSTIMESTAMP);

   END IF;

END;
/
