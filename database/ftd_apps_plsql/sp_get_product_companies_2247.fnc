CREATE OR REPLACE
FUNCTION ftd_apps.sp_get_product_companies_2247 (productID IN VARCHAR2)
  RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    sp_get_product_companies_2247
-- Type:    Function
-- Syntax:  sp_get_product_companies_2247 (productID )
-- Returns: ref_cursor for
--          COMPANY_ID        VARCHAR2(12)
--          COMANY_NAME       VAECHAR2(60)
--
-- Description:   Queries the PRODUCT_COMPANY_XREF able and returns companies for a product
--
--===========================================================


AS
    company_cursor types.ref_cursor;
BEGIN

    OPEN company_cursor FOR
        select
                cm.company_id, cm.company_name
        from
                product_company_xref pcx,
                company_master cm
        where
                pcx.company_id = cm.company_id
                and pcx.product_id = productID
                and cm.order_flag = 'Y'
        order by
                cm.company_name;

    RETURN company_cursor;
END;
.
/
