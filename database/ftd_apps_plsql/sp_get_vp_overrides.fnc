create or replace function ftd_apps.sp_get_vp_overrides
(
  IN_VENDOR_ID IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.VENDOR_ID%TYPE,
  IN_PRODUCT_SUBCODE_ID IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.PRODUCT_SUBCODE_ID%TYPE,
  IN_DELIVERY_OVERRIDE_DATE IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.DELIVERY_OVERRIDE_DATE%TYPE,
  IN_CARRIER_ID IN FTD_APPS.VENDOR_PRODUCT_OVERRIDE.CARRIER_ID%TYPE
)
return types.ref_cursor 
--==============================================================================
--
-- Name:    SP_GET_VP_OVERRIDES
-- Type:    Function
-- Syntax:  SP_GET_VP_OVERRIDES 
--          VENDOR_ID IN VARCHAR2
--          PRODUCT_SUBCODE_ID VARCHAR2
--          OVERRIDE_DATE IN DATE
--          CARRIER_ID IN VARCHAR2
-- Returns: ref_cursor for
--          VENDOR_ID          VARCHAR2(5)
--          PRODUCT_SUBCODE_ID VARCHAR2(10)
--          OVERRIDE_DATE      DATE
--          CARRIER_ID         VARCHAR2(10)
--
-- Description:   Queries the VENDOR_PRODUCT_OVERRIDE table by 
--                filtering by the passed in parameters and
--                returns a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor types.ref_cursor;
begin
  OPEN cur_cursor FOR
  SELECT vo.VENDOR_ID,  
         vo.DELIVERY_OVERRIDE_DATE,
         vo.CARRIER_ID,
         vo.SDS_SHIP_METHOD,
         vm.VENDOR_NAME,
         vo.PRODUCT_SUBCODE_ID
  FROM FTD_APPS.VENDOR_PRODUCT_OVERRIDE vo
  JOIN FTD_APPS.VENDOR_MASTER vm ON vo.vendor_id = vm.vendor_id
  WHERE 
        (vo.vendor_id = IN_VENDOR_ID OR IN_VENDOR_ID IS NULL) AND
        (vo.product_subcode_id = IN_PRODUCT_SUBCODE_ID OR IN_PRODUCT_SUBCODE_ID IS NULL) AND
        ((vo.delivery_override_date BETWEEN IN_DELIVERY_OVERRIDE_DATE AND IN_DELIVERY_OVERRIDE_DATE + 0.99999) OR IN_DELIVERY_OVERRIDE_DATE IS NULL) AND
        (vo.carrier_id = IN_CARRIER_ID OR IN_CARRIER_ID IS NULL) 
        
  ORDER BY vo.DELIVERY_OVERRIDE_DATE DESC, 
           vo.CARRIER_ID ASC,
           vo.SDS_SHIP_METHOD ASC,
           vm.VENDOR_NAME ASC,
           vo.PRODUCT_SUBCODE_ID ASC;
  return(cur_cursor);
end sp_get_vp_overrides;
.
/
