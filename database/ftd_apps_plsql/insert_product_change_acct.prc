CREATE OR REPLACE PROCEDURE FTD_APPS.INSERT_PRODUCT_CHANGE_ACCT (
 IN_TABLE_NAME in sys.USER_TABLES.TABLE_NAME%TYPE,
 IN_CHANGE_ID in ftd_apps.product_change$_acct.change_id%type,
 IN_PRODUCT_OR_SUBCODE_ID in ftd_apps.VENDOR_PRODUCT$.PRODUCT_SUBCODE_ID%TYPE,
 IN_UPDATED_BY in ftd_apps.VENDOR_PRODUCT$.UPDATED_BY%TYPE,
 IN_OPERATION in ftd_apps.VENDOR_PRODUCT$.OPERATION$%TYPE,
 IN_UPDATED_ON in ftd_apps.VENDOR_PRODUCT$.TIMESTAMP$%TYPE,
 IN_PRODUCT_HAS_SUBCODE_FLAG in VARCHAR2
 )
AS

--==============================================================================
--
-- Name:    INSERT_PRODUCT_CHANGE_ACCT
-- Type:    Procedure
-- Syntax:  INSERT_PRODUCT_CHANGE_ACCT ( <see args above> )
--
-- Description:   INSERTs modified records into the product change reporting tables.
--
--==============================================================================

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

-- First, insert a record into the parent metadata.
insert into product_change$_acct
(change_id, updated_by, operation$, timestamp$)
values (in_change_id, in_updated_by, in_operation, in_updated_on);

IF (in_table_name = 'PRODUCT_MASTER') THEN

IF (in_product_has_subcode_flag = 'Y') THEN

-- The product has subcodes.
-- Populate the vendor reporting table with these subcodes.
insert into vendor_product$_acct
(CHANGE_ID,
VENDOR_ID,
PRODUCT_ID,
VENDOR_COST,
AVAILABLE)
select
in_change_id,
vp.vendor_id,
in_product_or_subcode_id,
vp.vendor_cost,
vp.available
from
vendor_product vp, product_master pm, product_subcodes ps
where pm.product_id = in_product_or_subcode_id
and pm.product_id = ps.product_id
and ps.product_subcode_id = vp.product_subcode_id;

ELSE

-- Populate the vendor reporting table.
insert into vendor_product$_acct
(CHANGE_ID,
VENDOR_ID,
PRODUCT_ID,
VENDOR_COST,
AVAILABLE)
select
in_change_id,
vp.vendor_id,
in_product_or_subcode_id,
vp.vendor_cost,
vp.available
from
vendor_product vp, product_master pm
where pm.product_id = in_product_or_subcode_id
and pm.product_id = vp.product_subcode_id;

END IF;

ELSIF (in_table_name = 'VENDOR_PRODUCT') THEN

-- Determine if input is a subcode by attempting to insert subcode record into master.
insert into product_master$_acct
(CHANGE_ID,
PRODUCT_ID,
NOVATOR_ID,
STATUS,
PRODUCT_TYPE,
STANDARD_PRICE,
SUBCODE_PRICE,
DELUXE_PRICE,
PREMIUM_PRICE,
VENDOR_COST,
SUBCODE_VENDOR_COST,
DIM_WEIGHT,
SUBCODE_DIM_WEIGHT,
SHIP_METHOD_CARRIER,
SHIP_METHOD_FLORIST,
NO_TAX_FLAG)
select
in_change_id,
in_product_or_subcode_id,
pm.novator_id,
pm.status,
pm.product_type,
pm.standard_price,
ps.subcode_price,
pm.deluxe_price,
pm.premium_price,
pm.vendor_cost,
ps.vendor_price,
pm.dim_weight,
ps.dim_weight,
pm.ship_method_carrier,
pm.ship_method_florist,
pm.no_tax_flag
from product_master pm, product_subcodes ps
where pm.product_id = ps.product_id
and ps.product_subcode_id = in_product_or_subcode_id;

IF SQL%NOTFOUND THEN
-- The input was a product code.
-- Insert into the singular product record into the product master reporting table.
insert into product_master$_acct
(CHANGE_ID,
PRODUCT_ID,
NOVATOR_ID,
STATUS,
PRODUCT_TYPE,
STANDARD_PRICE,
DELUXE_PRICE,
PREMIUM_PRICE,
VENDOR_COST,
DIM_WEIGHT,
SHIP_METHOD_CARRIER,
SHIP_METHOD_FLORIST,
NO_TAX_FLAG)
select
in_change_id,
in_product_or_subcode_id,
pm.novator_id,
pm.status,
pm.product_type,
pm.standard_price,
pm.deluxe_price,
pm.premium_price,
pm.vendor_cost,
pm.dim_weight,
pm.ship_method_carrier,
pm.ship_method_florist,
pm.no_tax_flag
from product_master pm
where pm.product_id = in_product_or_subcode_id;

END IF;

ELSIF (in_table_name = 'PRODUCT_SUBCODES') THEN

-- Insert subcode vendor into vendor report table.
insert into vendor_product$_acct
(CHANGE_ID,
VENDOR_ID,
PRODUCT_ID,
VENDOR_COST,
AVAILABLE)
select
in_change_id,
vp.vendor_id,
in_product_or_subcode_id,
vp.vendor_cost,
vp.available
from
vendor_product vp
where vp.product_subcode_id = in_product_or_subcode_id;

END IF;

COMMIT;

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      raise_application_error(-20000, 'Fatal Error In FTD_APPS.INSERT_PRODUCT_CHANGE_ACCT: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END;
/