CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_AZ_PRODUCT_MASTER
    (IN_PRODUCT_ID IN varchar2)
    RETURN types.ref_cursor

--
-- Name:    SP_GET_AZ_PRODUCT_MASTER
-- Type:    Function
-- Returns: ref_cursor for
--          PRODUCT_ID                  VARCHAR2(10)
--
--
-- Description:   Queries the PTN_AMAZON.AZ_PRODUCT_MASTER table by PRODUCT_ID and returns a ref cursor
--                to the resulting row.
--
--=========================================================

AS
    OUT_CUR types.ref_cursor;
BEGIN

   OPEN OUT_CUR FOR
      SELECT product_id,
          catalog_status_standard,
          product_name_standard,
          product_description_standard,
          catalog_status_deluxe,
          product_name_deluxe,
          product_description_deluxe,
          catalog_status_premium,
          product_name_premium,
          product_description_premium,
          item_type
      FROM PTN_AMAZON.AZ_PRODUCT_MASTER
      WHERE PRODUCT_ID = IN_PRODUCT_ID;

   RETURN OUT_CUR;

END;
/
