CREATE OR REPLACE
PACKAGE BODY ftd_apps.SERVER_STATS_DML
    AS

--==============================================================================
PROCEDURE insert_row(
                     p_rowid              IN OUT VARCHAR2,
                     p_server_stats_id    IN OUT server_stats.server_stats_id%type,
                     p_server_name               server_stats.server_name%type,
                     p_timestamp                 server_stats.timestamp%type,
                     p_min1                      server_stats.min1%type,
                     p_min5                      server_stats.min5%type,
                     p_min10                     server_stats.min10%type) is

  cursor check_cur(pkey  server_stats.server_stats_id%TYPE) is
    select rowid
      from server_stats
     where server_stats_id = pkey;

  cursor next_id_cur is
    select server_stats_id.nextval
      from dual;

BEGIN

  open next_id_cur;
  fetch next_id_cur into p_server_stats_id;
  close next_id_cur;

  insert into server_stats (
                      server_stats_id,
                      server_name,
                      timestamp,
                      min1,
                      min5,
                      min10)
              values (
                      p_server_stats_id,
                      p_server_name,
                      sysdate,
                      p_min1,
                      p_min5,
                      p_min10);

  open check_cur(p_server_stats_id);
  fetch check_cur into p_rowid;
  if check_cur%NOTFOUND THEN

      close check_cur;
      raise NO_DATA_FOUND;

  end if;

  close check_cur;

END insert_row;

END server_stats_dml;
.
/
