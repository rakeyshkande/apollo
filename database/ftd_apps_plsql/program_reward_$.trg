CREATE OR REPLACE
TRIGGER ftd_apps.program_reward_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.program_reward REFERENCING OLD AS old NEW AS new FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.program_reward$ (
      program_name,
      calculation_basis,
      points,
      reward_type,
      created_on,
      created_by,
      updated_on,
      updated_by,
      customer_info,
      reward_name,
      participant_id_length_check,
      participant_email_check,
      maximum_points,
      bonus_calculation_basis,
      bonus_points,
      bonus_separate_data,
      operation$, timestamp$
      ) VALUES (
      :NEW.program_name,
      :NEW.calculation_basis,
      :NEW.points,
      :NEW.reward_type,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      :NEW.customer_info,
      :NEW.reward_name,
      :NEW.participant_id_length_check,
      :NEW.participant_email_check,
      :NEW.maximum_points,
      :NEW.bonus_calculation_basis,
      :NEW.bonus_points,
      :NEW.bonus_separate_data,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.program_reward$ (
      program_name,
      calculation_basis,
      points,
      reward_type,
      created_on,
      created_by,
      updated_on,
      updated_by,
      customer_info,
      reward_name,
      participant_id_length_check,
      participant_email_check,
      maximum_points,
      bonus_calculation_basis,
      bonus_points,
      bonus_separate_data,
      operation$, timestamp$
      ) VALUES (
      :OLD.program_name,
      :OLD.calculation_basis,
      :OLD.points,
      :OLD.reward_type,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      :OLD.customer_info,
      :OLD.reward_name,
      :OLD.participant_id_length_check,
      :OLD.participant_email_check,
      :OLD.maximum_points,
      :OLD.bonus_calculation_basis,
      :OLD.bonus_points,
      :OLD.bonus_separate_data,
      'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.program_reward$ (
      program_name,
      calculation_basis,
      points,
      reward_type,
      created_on,
      created_by,
      updated_on,
      updated_by,
      customer_info,
      reward_name,
      participant_id_length_check,
      participant_email_check,
      maximum_points,
      bonus_calculation_basis,
      bonus_points,
      bonus_separate_data,
      operation$, timestamp$
      ) VALUES (
      :NEW.program_name,
      :NEW.calculation_basis,
      :NEW.points,
      :NEW.reward_type,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      :NEW.customer_info,
      :NEW.reward_name,
      :NEW.participant_id_length_check,
      :NEW.participant_email_check,
      :NEW.maximum_points,
      :NEW.bonus_calculation_basis,
      :NEW.bonus_points,
      :NEW.bonus_separate_data,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.program_reward$ (
      program_name,
      calculation_basis,
      points,
      reward_type,
      created_on,
      created_by,
      updated_on,
      updated_by,
      customer_info,
      reward_name,
      participant_id_length_check,
      participant_email_check,
      maximum_points,
      bonus_calculation_basis,
      bonus_points,
      bonus_separate_data,
      operation$, timestamp$
      ) VALUES (
      :OLD.program_name,
      :OLD.calculation_basis,
      :OLD.points,
      :OLD.reward_type,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      :OLD.customer_info,
      :OLD.reward_name,
      :OLD.participant_id_length_check,
      :OLD.participant_email_check,
      :OLD.maximum_points,
      :OLD.bonus_calculation_basis,
      :OLD.bonus_points,
      :OLD.bonus_separate_data,
      'DEL',SYSDATE);

   END IF;

END;
/
