CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_SHIP_METHODS (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_SHIP_METHODS
-- Type:    Procedure
-- Syntax:  SP_REMOVE_SHIP_METHODS ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_SHIP_METHODS by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_SHIP_METHODS
  where PRODUCT_ID = productId;

end
;
.
/
