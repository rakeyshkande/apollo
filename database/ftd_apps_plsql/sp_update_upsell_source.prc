CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_UPSELL_SOURCE (
 upsellMasterId in varchar2,
 sourceCode in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_UPSELL_SOURCE
-- Type:    Procedure
-- Syntax:.  SP_UPDATE_UPSELL_SOURCE ( upsellMasterId in varchar2,
--                               sourceCode in varchar2 )
--
-- Description:   Attempts to insert a row into UPSELL_SOURCE.
--                If the upsellMasterId/sourceCode combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into UPSELL_SOURCE
  INSERT INTO UPSELL_SOURCE
      ( UPSELL_MASTER_ID,
        SOURCE_CODE,
        LAST_MODIFIED_DATE)
  VALUES
      ( upsellMasterId,
        upper(sourceCode),
        SYSDATE);

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    rollback;
end
;
.
/
