CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PRODDETAIL_COLOR (productId IN varchar2,
   colorRank IN NUMBER)
    RETURN VARCHAR2
--===============================================================
--
-- Name:    OE_GET_PRODDETAIL_COLOR
-- Type:    Function
-- Syntax:  OE_GET_PRODDETAIL_COLOR (productId IN VARCHAR,
--                                    colorRank IN NUMBER)
-- Returns: tmp_color     VARCHAR2(15)
--
-- Description:   Gets colors for a product in order of color ID.
--                Used by OE_PRODUCT_DETAILS.
--
--===============================================================
AS
  tmp_color     VARCHAR2(15);
BEGIN
    -- Product color list view ranks colors available
    --   for a product by color id
    SELECT NLS_INITCAP(pcl.color_description)
    INTO tmp_color
    FROM PRODUCT_COLORS_LIST_VW pcl
    WHERE pcl.product_id = productId
    AND pcl.color_rank = colorRank;

    RETURN tmp_color;

EXCEPTION WHEN OTHERS THEN
    RETURN null;
END
;
.
/
