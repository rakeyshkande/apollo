create or replace function       ftd_apps.double_formatter(num in number) return varchar2
as

v_result varchar2(40);
begin
    select trim(to_char(num, '90.99')) into v_result from dual;
    dbms_output.put_line  ('Number: ' || num || ' and Result: ' || v_result);
    return v_result;
end;
/
