CREATE OR REPLACE
function ftd_apps.get_gnadd_level return varchar2
is
    cursor gcur
        is
    select substr(to_char(gnadd_level),1,2) gnadd_level
      from global_parms;

    gp_gnadd_level varchar2(2);
begin
    for grec in gcur
    loop
       gp_gnadd_level := grec.gnadd_level;
    end loop;

    return gp_gnadd_level;

exception
     when others
     then
        null;
end;
.
/
