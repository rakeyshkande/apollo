CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_VENDOR_DELIVBLOCK_END (vendorId IN varchar2,
   deliveryBlockRank IN NUMBER)
    RETURN DATE
--=========================================================================
--
-- Name:    OE_GET_VENDOR_DELIVBLOCK_END
-- Type:    Function
-- Syntax:  OE_GET_VENDOR_DELIVBLOCK_END(vendorId IN VARCHAR2,
--                                      deliveryBlockRank IN NUMBER)
-- Returns: tmp_blockend    DATE
--
-- Description:   Gets block end date for product delivery by vendor,
--                ordered by start date of the start / end date range.
--
--=========================================================================
AS
  tmp_blockend    DATE;
BEGIN
    -- Vendor delivery block list view lists start / end date ranges
    --   for blocked vendor delivery of products, ordered by start date
    SELECT end_date
    INTO tmp_blockend
    FROM VENDOR_DELIV_BLOCK_LIST_VW
    WHERE vendor_id = vendorId
    AND delivery_block_rank = deliveryBlockRank;

    RETURN tmp_blockend;

EXCEPTION WHEN OTHERS THEN
    RETURN null;
END
;
.
/
