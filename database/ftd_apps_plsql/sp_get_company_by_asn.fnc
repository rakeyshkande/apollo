CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_COMPANY_BY_ASN (asnNumberIn varchar2)
  RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_COMPANY_BY_ASN
-- Type:    Function
-- Syntax:  SP_GET_COMPANY_BY_ASN (phoneIn VARCHAR2 )
-- Returns: ref_cursor for
--          asnNumberIn     VARCHAR2
--
-- Description:   Queries the Buyer Source Code and Client Url for the
--                passed in asn number.  An outer join is done on the url
--                table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==================================================\

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT BUYER_SOURCE_CODE.ASN_NUMBER,BUYER_SOURCE_CODE.CLIENT_NAME,BUYER_SOURCE_CODE.SOURCE_CODE,URL
        FROM BUYER_SOURCE_CODE,CLIENT_URL
        where  BUYER_SOURCE_CODE.ASN_NUMBER = CLIENT_URL.ASN_NUMBER(+)
          and  BUYER_SOURCE_CODE.ASN_NUMBER = asnNumberIn
        ORDER BY BUYER_SOURCE_CODE.CLIENT_NAME;

    RETURN cur_cursor;
END
;
.
/
