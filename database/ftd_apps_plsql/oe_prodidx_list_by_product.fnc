CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODIDX_LIST_BY_PRODUCT (productId IN VARCHAR2)
 RETURN types.ref_cursor
--==========================================================================
--
-- Name:    OE_PRODUCT_INDEX_LIST_BY_PRODUCT
-- Type:    Function
-- Syntax:  OE_PRODUCT_INDEX_LIST_BY_PRODUCT (productId IN VARCHAR2)
-- Returns: ref_cursor for
--          indexId               NUMBER
--          name                  VARCHAR2(100)
--          productCount          NUMBER
--          subIndices            VARCHAR2
--          liveIndex             VARCHAR2(1)
--          searchIndex           VARCHAR2(1)
--          webLiveIndex          VARCHAR2(1)
--          callCenterLiveIndex   VARCHAR2(1)
--          webTest1Index         VARCHAR2(1)
--          webTest2Index         VARCHAR2(1)
--          webTest3Index         VARCHAR2(1)
--          callCenterTestIndex   VARCHAR2(1)
--
-- Description:   Queries the PRODUCT_INDEX and PRODUCT_INDEX_XREF tables
--                by productId and returns a ref cursor to all product indexes
--                containing the specified product.  Products count and
--                subindices are left NULL intentionally.
--
--==========================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    -- Ignore search indexes for purposes of this query
    OPEN cur_cursor FOR
        SELECT pi.index_id as "indexId",
              pi.index_file_name as "name",
              NULL as "productCount",
              NULL as "subIndices",
              decode(pi.web_live_flag, 'Y', 'Y',
                    decode(pi.call_center_live_flag, 'Y', 'Y', 'N')) as "liveIndex",
              pi.search_index_flag as "searchIndex",
              pi.web_live_flag as "webLiveIndex",
              pi.call_center_live_flag as "callCenterLiveIndex",
              pi.web_test1_flag as "webTest1Index",
              pi.web_test2_flag as "webTest2Index",
              pi.web_test3_flag as "webTest3Index",
              pi.call_center_test_flag as "callCenterTestIndex"
         FROM PRODUCT_INDEX pi, PRODUCT_INDEX_XREF pix
         WHERE pix.product_id = productId
         AND pi.index_id = pix.product_index_id
         AND pi.search_index_flag = 'N'
         ORDER BY pi.index_file_name;

    RETURN cur_cursor;
END
;
.
/
