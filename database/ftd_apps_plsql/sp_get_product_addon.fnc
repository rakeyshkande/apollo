CREATE OR REPLACE
FUNCTION FTD_APPS.SP_GET_PRODUCT_ADDON (productId IN varchar2)
RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_ADDON
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_ADDON ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--            FAA.ADDON_ID
--	      FAA.DESCRIPTION
--	      FAA.PRICE
--            FAA.ACTIVE_FLAG
--            FAA.PQUAD_ACCESSORY_ID 
--            FAAT.ADDON_TYPE_ID
--	      FAAT.PRODUCT_FEED_FLAG
--     	      FAPA.ACTIVE_FLAG
--	      FAPA.DISPLAY_SEQ_NUM
--	      FAPA.MAX_QTY
--
-- Description:   Queries the PRODUCT_ADDON table by PRODUCT ID and returns a ref cursor
--                containing the resulting rows.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT         
      	FAA.ADDON_ID,
	FAA.DESCRIPTION,
	FAA.PRICE,
	FAA.ACTIVE_FLAG AS ADDON_ACTIVE_FLAG,
  FAA.PQUAD_ACCESSORY_ID ,
      	FAAT.ADDON_TYPE_ID,
	FAAT.PRODUCT_FEED_FLAG,
     	FAPA.ACTIVE_FLAG AS PRODUCT_ADDON_ACTIVE_FLAG,
	FAPA.DISPLAY_SEQ_NUM,
	FAPA.MAX_QTY
        
          FROM FTD_APPS.ADDON FAA, FTD_APPS.ADDON_TYPE FAAT, FTD_APPS.PRODUCT_ADDON FAPA
          WHERE FAA.ADDON_TYPE = FAAT.ADDON_TYPE_ID
          --don't include cards
          AND FAAT.ADDON_TYPE_ID <> 4
          --don't include vases
          AND FAAT.ADDON_TYPE_ID <> 7
          --don't include banners
          AND FAAT.ADDON_TYPE_ID <> 3
          AND FAA.ADDON_ID = FAPA.ADDON_ID (+)
          AND productId = FAPA.PRODUCT_ID (+)        
          
          ORDER by DECODE(FAPA.DISPLAY_SEQ_NUM, NULL, 999999, FAPA.DISPLAY_SEQ_NUM), FAA.DESCRIPTION ;

    RETURN cur_cursor;
END
;
.
/
