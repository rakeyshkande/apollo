CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_BY_CATALOG_FLAG (catalogOnly IN
    varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_BY_CATALOG_FLAG
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_BY_CATALOG_FLAG (catalogOnly IN varchar2)
-- Returns: ref_cursor for
--          PRODUCT_ID                  VARCHAR2(10)
--          NOVATOR_ID                  VARCHAR2(10)
--          PRODUCT_NAME                VARCHAR2(25)
--          NOVATOR_NAME                VARCHAR2(100)
--          STATUS                      VARCHAR2(1)
--          DELIVERY_TYPE               VARCHAR2(1)
--          CATEGORY                    VARCHAR2(5)
--          PRODUCT_TYPE                VARCHAR2(25)
--          PRODUCT_SUB_TYPE            VARCHAR2(25)
--          COLOR_SIZE_FLAG             VARCHAR2(1)
--          STANDARD_PRICE              NUMBER(8,2)
--          DELUXE_PRICE                NUMBER(8,2)
--          PREMIUM_PRICE               NUMBER(8,2)
--          PREFERRED_PRICE_POINT       NUMBER(2)
--          VARIABLE_PRICE_MAX          NUMBER(8,2)
--          SHORT_DESCRIPTION           VARCHAR2(80)
--          LONG_DESCRIPTION            VARCHAR2(540)
--          FLORIST_REFERENCE_NUMBER    VARCHAR2(62)
--          MERCURY_DESCRIPTION         VARCHAR2(62)
--          ITEM_COMMENTS               VARCHAR2(62)
--          ADD_ON_BALLOONS_FLAG        VARCHAR2(1)
--          ADD_ON_BEARS_FLAG           VARCHAR2(1)
--          ADD_ON_CARDS_FLAG           VARCHAR2(1)
--          ADD_ON_FUNERAL_FLAG         VARCHAR2(1)
--          CODIFIED_FLAG               VARCHAR2(5)
--          EXCEPTION_CODE              VARCHAR2(1)
--          EXCEPTION_START_DATE        DATE(7)
--          EXCEPTION_END_DATE          DATE(7)
--          EXCEPTION_MESSAGE           VARCHAR2(280)
--          VENDOR_ID                   VARCHAR2(5)
--          VENDOR_COST                 NUMBER(8,2)
--          VENDOR_SKU                  VARCHAR2(10)
--          SECOND_CHOICE_CODE          VARCHAR2(2)
--          HOLIDAY_SECOND_CHOICE_CODE  VARCHAR2(2)
--          DROPSHIP_CODE               VARCHAR2(2)
--          DISCOUNT_ALLOWED_FLAG       VARCHAR2(1)
--          DELIVERY_INCLUDED_FLAG      VARCHAR2(1)
--          TAX_FLAG                    VARCHAR2(1)
--          SERVICE_FEE_FLAG            VARCHAR2(1)
--          EXOTIC_FLAG                 VARCHAR2(1)
--          EGIFT_FLAG                  VARCHAR2(1)
--          COUNTRY_ID                  VARCHAR2(2)
--          ARRANGEMENT_SIZE            VARCHAR2(25)
--          ARRANGEMENT_COLORS          VARCHAR2(255)
--          DOMINANT_FLOWERS            VARCHAR2(255)
--          SEARCH_PRIORITY             VARCHAR2(5)
--          RECIPE                      VARCHAR2(255)
--          SUBCODE_FLAG                VARCHAR2(1)
--          DIM_WEIGHT                  VARCHAR2(10)
--          NEXT_DAY_UPGRADE_FLAG       VARCHAR2(1)
--          CORPORATE_SITE              VARCHAR2(1)
--          UNSPSC_CODE                 VARCHAR2(40)
--          PRICE_RANK_1                VARCHAR2(1)
--          PRICE_RANK_2                VARCHAR2(1)
--          PRICE_RANK_3                VARCHAR2(1)
--          SHIP_METHOD_CARRIER         VARCHAR2(1)
--          SHIP_METHOD_FLORIST         VARCHAR2(1)
--          SHIPPING_KEY                VARCHAR2(3)
--          LAST_UPDATE                 DATE(7)
--          VARIABLE_PRICE_FLAG         VARCHAR2(1)
--          HOLIDAY_SKU                 VARCHAR2(6)
--          HOLIDAY_PRICE               NUMBER(8,2)
--          CATALOG_FLAG                VARCHAR2(1)
--
--
-- Description:   Queries the PRODUCT_MASTER table by CATALOG_FLAG(catalogOnly) and returns a ref cursor
--                to the resulting row.
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
            SELECT product_id       as "productId",
               novator_id       as "novatorId",
               product_name     as "productName",
               novator_name     as "novatorName",
               status           as "status",
               delivery_type    as "deliveryType",
               category         as "category",
               product_type     as "productType",
               product_sub_type as "productSubType",
               color_size_flag  as "colorSizeFlag",
               standard_price   as "standardPrice",
               deluxe_price     as "deluxePrice",
               premium_price    as "premiumPrice",
               preferred_price_point    as "preferredPricePoint",
               variable_price_max       as "variablePriceMax",
               short_description        as "shortDescription",
               long_description         as "longDescription",
               florist_reference_number as "floristReferenceNumber",
               mercury_description      as "mercuryDescription",
               item_comments            as "itemsComments",
               add_on_balloons_flag     as "addOnBallonsFlag",
               add_on_bears_flag        as "addOnBearsFlag",
               add_on_cards_flag        as "addOnCardsFlag",
               add_on_funeral_flag      as "addOnFuneralFlag",
               codified_flag            as "codifiedFlag",
               exception_code           as "exceptionCode",
               exception_start_date     as "exceptionStartDate",
               exception_end_date       as "exceptionEndDate",
               exception_message        as "exceptionMessage",
               vendor_id                as "vendorId",
               vendor_cost              as "vendorCost" ,
               vendor_sku               as "vendorSku",
               second_choice_code       as "secondChoiceCode",
               holiday_second_choice_code as "holidaySecondChoiceCode",
               dropship_code              as "dropshipCode",
               discount_allowed_flag      as "discountAllowedFlag",
               delivery_included_flag     as "deliveryIncludedFlag",
               no_tax_flag                   as "taxFlag",
               service_fee_flag           as "serviceFeeFlag",
               exotic_flag                as "exoticFlag",
               egift_flag                 as "egiftFlag",
               country_id                 as "countryId",
               arrangement_size           as "arrangementSize",
               arrangement_colors         as "arrangementColors",
               dominant_flowers           as "dominantFlowers",
               search_priority            as "searchPriority",
               standard_recipe            as "recipe",
               subcode_flag               as "subcodeFlag",
               dim_weight                 as "dimWeight",
               next_day_upgrade_flag      as "nextDayUpgradeFlag",
               corporate_site             as "corporateSite",
               unspsc_code                as "unspscCode",
               price_rank_1               as "priceRank1",
               price_rank_2               as "priceRank2",
               price_rank_3               as "priceRank3",
               ship_method_carrier        as "shipMethodCarrier",
               ship_method_florist        as "shipMethodFlorist",
               shipping_key               as "shippingKey",
               last_update                as "lastUpdate",
               variable_price_flag        as "variablePriceFlag",
               holiday_sku                as "holidaySku",
               holiday_price              as "holidayPrice",
               catalog_flag               as "catalogFlag",
               holiday_deluxe_price       as "holidayDeluxePrice",
               holiday_premium_price      as "holidayPremiumPrice",
               holiday_start_date         as "holidayStartDate",
               holiday_end_date           as "holidayEndDate",
               mercury_second_choice      as "mercurySecondChoice",
               mercury_holiday_second_choice as "mercuryHolidaySecondChoice",
               add_on_chocolate_flag      as "addOnChocolateFlag"
        FROM PRODUCT_MASTER
    WHERE ( PRODUCT_MASTER.CATALOG_FLAG = catalogOnly );

    RETURN cur_cursor;
END
;
.
/
