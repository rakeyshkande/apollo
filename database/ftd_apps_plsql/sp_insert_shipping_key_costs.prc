create or replace procedure ftd_apps.SP_INSERT_SHIPPING_KEY_COSTS(
  IN_SHIPPING_KEY_DETAIL_ID IN NUMBER,
  IN_SHIP_METHOD IN VARCHAR2,
  IN_CREATED_BY IN VARCHAR2,
  IN_UPDATED_BY IN VARCHAR2,
  IN_COST IN NUMBER
) AS

/**********************************************************
--
-- Name:    SP_INSERT_SHIPPING_KEY_COSTS
-- Type:    Procedure
-- Syntax:  SP_INSERT_SHIPPING_KEY_COSTS ( 
--                                        SHIPPING_KEY_DETAIL_ID NUMBER, 
--                                        SHIPPING_METHOD VARCHAR2, 
--                                        CREATED_BY VARCHAR2,
--                                        UPDATED_BY VARCHAR2,
--                                        COST NUMBER )
--
-- Description:   INSERTS a row into SHIPPING_KEY_COSTS
--
***********************************************************/
begin
  INSERT INTO FTD_APPS.SHIPPING_KEY_COSTS (SHIPPING_KEY_DETAIL_ID, SHIPPING_METHOD_ID, SHIPPING_COST, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) 
    VALUES(TO_CHAR(IN_SHIPPING_KEY_DETAIL_ID),IN_SHIP_METHOD,IN_COST,IN_CREATED_BY,SYSDATE, IN_UPDATED_BY, SYSDATE );
end SP_INSERT_SHIPPING_KEY_COSTS;
.
/
