CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_UPSELL_SOURCE (upsellMasterId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_UPSELL_SOURCE
-- Type:    Function
-- Syntax:  SP_GET_UPSELL_SOURCE ( upsellMasterId IN VARCHAR2)
-- Returns: ref_cursor for
--          SOURCE_CODE      VARCHAR2(25)
--
-- Description:   Queries the UPSELL_SOURCE table by UPSELL MASTER ID and returns
--                a ref cursor to the resulting row.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT US.SOURCE_CODE as "sourceCode",
            SM.ORDER_SOURCE as "orderSource"
        FROM FTD_APPS.UPSELL_SOURCE us, FTD_APPS.SOURCE_MASTER SM
        WHERE US.UPSELL_MASTER_ID = upsellMasterId
        AND SM.SOURCE_CODE = US.SOURCE_CODE;

    RETURN cur_cursor;
END
;
.
/
