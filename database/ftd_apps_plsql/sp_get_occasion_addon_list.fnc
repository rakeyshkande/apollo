create or replace FUNCTION ftd_apps.SP_GET_OCCASION_ADDON_LIST RETURN types.ref_cursor

 --==============================================================================
--
-- Name:    SP_GET_OCCASION_ADDON_LIST
-- Type:    Function
-- Syntax:  SP_GET_OCCASION_ADDON_LIST ( )
-- Returns: ref_cursor for
--          OCCASION_ID     NUMBER
--          ADDON_ID     VARCHAR2(10)
--
-- Description:   Queries the OCCASION_ADDON table and returns a ref cursor for row.
--
--===========================================================

AS
    occasion_addon_cursor types.ref_cursor;
BEGIN
    OPEN occasion_addon_cursor FOR
        SELECT OCCASION_ID as "occasionId",
                  ADDON_ID as "addonId"
           FROM OCCASION_ADDON
                ORDER BY ADDON_ID;


    RETURN occasion_addon_cursor;
END;
/
