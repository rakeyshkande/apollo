CREATE OR REPLACE
FUNCTION ftd_apps.oe_get_src_lstbyval_md04 (
    searchValue IN varchar2,
    displayExpired IN varchar2,
    dnisType IN varchar2
 )
 RETURN types.ref_cursor
 AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    oe_get_src_lstbyval_md04
-- Type:    Function
-- Syntax:  oe_get_src_lstbyval_md04 ( searchValue IN VARCHAR2,
--                                           displayExpired IN VARCHAR2,
--                                           dnisType IN VARCHAR2 )
-- Returns: ref_cursor for
--          sourceDescription           VARCHAR2(25)
--          offerDescription            VARCHAR2(40)
--          serviceCharge               VARCHAR2(15)
--          expirationDate              VARCHAR2(10)
--          orderSource                 VARCHAR2(1)
--          sourceCode                  VARCHAR2(10)
--          expiredFlag                 VARCHAR2(1)
--
-- Description:   Queries the SOURCE table for all rows whose source code or
--                description starts with the input search value.  If the input
--                display expired flag is Y then returns all matching rows,
--                otherwise returns all rows that do not yet have an end date set.
--                If DNIS type is 'JP' for JCPenney then only search their source
--                codes.  Otherwise exclude JCPenney source codes from the search.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN

    OPEN cur_cursor FOR
                SELECT SUBSTR(SC.DESCRIPTION,1,1) as "anchor",
                      SC.DESCRIPTION as "sourceDescription",
                           PH.DESCRIPTION as "offerDescription",
                           '$' || SNH.FIRST_ORDER_DOMESTIC ||
                               ' / ' ||
                               '$' || SNH.FIRST_ORDER_INTERNATIONAL as "serviceCharge",
                           to_char(SC.END_DATE,'mm/dd/yyyy') as "expirationDate",
                           SC.ORDER_SOURCE as "orderSource",
                           SC.SOURCE_CODE as "sourceCode",
                           DECODE(SC.END_DATE, NULL, 'N',
                             DECODE(SIGN(SC.END_DATE - TRUNC(SYSDATE)), 0, 'N', 1, 'N', 'Y')) as "expiredFlag"
                FROM source SC, price_header PH, snh
                WHERE ( SC.SOURCE_CODE like (searchValue || '%')
                     OR SC.DESCRIPTION like (searchValue || '%') )
                AND SC.PRICING_CODE = ph.PRICE_HEADER_ID
                AND SC.SHIPPING_CODE = snh.SNH_ID
                AND ( displayExpired = 'Y'
                     OR SC.END_DATE IS NOT NULL )
                AND NVL(SC.JCPENNEY_FLAG, 'N') = DECODE(dnisType, 'JP', 'Y', 'N')
                AND DECODE(dnisType, 'SF', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'SFMB', 'Y', 'N')
                AND DECODE(dnisType, 'GS', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'GIFT', 'Y', 'N')
                AND DECODE(dnisType, 'HI', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'HIGH', 'Y', 'N')
                AND DECODE(dnisType, 'SC', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'GSC', 'Y', 'N')
                AND DECODE(dnisType, 'FS', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'FUSA', 'Y', 'N')
                AND DECODE(dnisType, 'FD', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'FDIRECT', 'Y', 'N')
                AND DECODE(dnisType, 'FL', 'Y', 'N') = DECODE(SC.COMPANY_ID, 'FLORIST', 'Y', 'N')

                ORDER BY SC.DESCRIPTION ASC, SC.ORDER_SOURCE DESC;

    RETURN cur_cursor;
END;
.
/
