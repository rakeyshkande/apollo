CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_PRODUCT_SUBCODES (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_PRODUCT_SUBCODES
-- Type:    Procedure
-- Syntax:  SP_REMOVE_PRODUCT_SUBCODES ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_SUBCODES by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_SUBCODES
  where PRODUCT_ID = productId;

end
;
.
/
