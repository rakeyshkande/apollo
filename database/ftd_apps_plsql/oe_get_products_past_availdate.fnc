CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PRODUCTS_PAST_AVAILDATE
 RETURN types.ref_cursor
--=======================================================================
--
-- Name:    OE_GET_PRODUCTS_PAST_AVAILDATE
-- Type:    Function
-- Syntax:  OE_GET_PRODUCTS_PAST_AVAILDATE
-- Returns: ref_cursor for
--          PRODUCT_ID          VARCHAR2(10)
--
-- Description:   Returns all products that have availability start date in the past
--
--========================================================================

AS
    product_cursor types.ref_cursor;
BEGIN
    OPEN product_cursor FOR
        SELECT PRODUCT_ID
            FROM PRODUCT_MASTER
            WHERE EXCEPTION_END_DATE < sysdate
            AND EXCEPTION_START_DATE IS NOT NULL
            AND EXCEPTION_CODE = 'A';
    RETURN product_cursor;
END;
.
/
