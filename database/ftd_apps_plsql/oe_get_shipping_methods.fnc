CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_SHIPPING_METHODS (
productId          IN VARCHAR2,
shippingKeyId      IN NUMBER,
standardPrice      IN NUMBER
)
  RETURN types.ref_cursor

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT psm.SHIP_METHOD_ID as "shipMethodCode",
               plvw.SHIPPING_COST as "shipCost",
               plvw.SHIPPING_METHOD as "shipMethodDesc"
        FROM PRODUCT_SHIP_METHODS psm, PRODUCT_DELIVERY_LIST_VW plvw
        WHERE psm.PRODUCT_ID = productId
            AND plvw.PRODUCT_ID = psm.PRODUCT_ID
            AND plvw.SHIPPING_KEY_ID = shippingKeyId
            AND standardPrice >= plvw.MIN_PRICE
            AND standardPrice <= plvw.MAX_PRICE
            AND psm.SHIP_METHOD_ID = plvw.SHIPPING_METHOD_ID;

    RETURN cur_cursor;
END
;
.
/
