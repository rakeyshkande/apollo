CREATE OR REPLACE
TRIGGER ftd_apps.program_reward_range_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.program_reward_range REFERENCING OLD AS old NEW AS new FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.program_reward_range$ (
      program_name,
      min_dollar,
      max_dollar,
      calculation_basis,
      points,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.program_name,
      :NEW.min_dollar,
      :NEW.max_dollar,
      :NEW.calculation_basis,
      :NEW.points,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.program_reward_range$ (
      program_name,
      min_dollar,
      max_dollar,
      calculation_basis,
      points,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.program_name,
      :OLD.min_dollar,
      :OLD.max_dollar,
      :OLD.calculation_basis,
      :OLD.points,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.program_reward_range$ (
      program_name,
      min_dollar,
      max_dollar,
      calculation_basis,
      points,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.program_name,
      :NEW.min_dollar,
      :NEW.max_dollar,
      :NEW.calculation_basis,
      :NEW.points,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.program_reward_range$ (
      program_name,
      min_dollar,
      max_dollar,
      calculation_basis,
      points,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.program_name,
      :OLD.min_dollar,
      :OLD.max_dollar,
      :OLD.calculation_basis,
      :OLD.points,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
