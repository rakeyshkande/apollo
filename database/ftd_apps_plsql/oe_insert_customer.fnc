CREATE OR REPLACE
FUNCTION ftd_apps.OE_INSERT_CUSTOMER (
    customerIn        in varchar2,
    firstNameIn       in varchar2,
    lastNameIn        in varchar2,
    addressTypeIn     in varchar2,
    address1In        in varchar2,
    address2In        in varchar2,
    cityIn            in varchar2,
    stateIn           in varchar2,
    zipcodeIn         in varchar2,
    countyIn          in varchar2,
    countryIdIn       in varchar2,
    homePhoneIn       in varchar2,
    workPhoneIn       in varchar2,
    workPhoneExtIn    in varchar2,
    faxNumberIn       in varchar2,
    emailIn           in varchar2,
    bfhNameIn         in varchar2,
    bfhInfoIn         in varchar2,
    lastUpdateUserIn  in varchar2,
    promoFlag         in varchar2
)
RETURN types.ref_cursor
AS
--==============================================================================
--
-- Name:    OE_INSERT_CUSTOMER
-- Type:    Procedure
-- Syntax:  OE_INSERT_CUSTOMER(  see args above  )
--
-- Description:   Performs an INSERT / UPDATE on the CUSTOMER table.
--
--==============================================================================

 CURSOR customer_update_cur ( p_first_name VARCHAR2, p_last_name VARCHAR2,
                              p_zip_code VARCHAR2, p_address1 VARCHAR2 )
 IS
 SELECT * FROM CUSTOMER
 WHERE FIRST_NAME = UPPER(p_first_name)
 AND LAST_NAME = UPPER(p_last_name)
 AND ADDRESS_1 = UPPER(p_address1)
 AND (( ZIP_CODE IS NULL AND p_zip_code IS NULL )
   OR  ZIP_CODE = p_zip_code )
 FOR UPDATE;

 customer_row   customer_update_cur%ROWTYPE;
 cleanZip       VARCHAR2(10) := OE_CLEANUP_ALPHANUM_STRING(UPPER(zipCodeIn),'Y');

 result_cur     types.ref_cursor;
 myerrmsg       VARCHAR2(3000);

BEGIN

    -- Try to perform INSERT/UPDATE within separate block
    --   to catch DUP_VAL_ON_INDEX
    BEGIN
         INSERT INTO CUSTOMER (
            CUSTOMER_ID,
            FIRST_NAME,
            LAST_NAME,
            ADDRESS_TYPE,
            ADDRESS_1,
            ADDRESS_2,
            CITY,
            STATE,
            ZIP_CODE,
            COUNTY,
            COUNTRY_ID,
            HOME_PHONE,
            WORK_PHONE,
            WORK_PHONE_EXT,
            FAX_NUMBER,
            EMAIL,
            BFH_NAME,
            BFH_INFO,
            SPCL_PROMO_FLAG,
            LAST_UPDATE_USER,
            LAST_UPDATE_DATE
         )
         VALUES (
            customerIn,
            UPPER(firstNameIn),
            UPPER(lastNameIn),
            addressTypeIn,
            UPPER(address1In),
            UPPER(address2In),
            UPPER(cityIn),
            stateIn,
            cleanZip,
            countyIn,
            countryIdIn,
            OE_CLEANUP_ALPHANUM_STRING(homePhoneIn,'N'),
            OE_CLEANUP_ALPHANUM_STRING(workPhoneIn,'N'),
            workPhoneExtIn,
            faxNumberIn,
            emailIn,
            UPPER(bfhNameIn),
            UPPER(bfhInfoIn),
            promoFlag,
            lastUpdateUserIn,
            SYSDATE
         );

         COMMIT;

    EXCEPTION WHEN DUP_VAL_ON_INDEX THEN

         OPEN customer_update_cur(firstNameIn, lastNameIn, cleanZip, address1In);
         FETCH customer_update_cur INTO customer_row;

         UPDATE CUSTOMER
         SET
            ADDRESS_TYPE = addressTypeIn,
            ADDRESS_2 = UPPER(address2In),
            CITY = UPPER(cityIn),
            STATE = stateIn,
            COUNTY = countyIn,
            COUNTRY_ID = countryIdIn,
            WORK_PHONE = OE_CLEANUP_ALPHANUM_STRING(workPhoneIn,'N'),
            WORK_PHONE_EXT = workPhoneExtIn,
            FAX_NUMBER = faxNumberIn,
            EMAIL = emailIn,
            BFH_NAME = UPPER(bfhNameIn),
            BFH_INFO = UPPER(bfhInfoIn),
            LAST_UPDATE_USER = lastUpdateUserIn,
            LAST_UPDATE_DATE = SYSDATE
         WHERE CURRENT OF customer_update_cur;

         IF NOT (( homePhoneIn IS NULL ) AND ( homePhoneIn = '' ))
         THEN
            UPDATE CUSTOMER
            SET HOME_PHONE = OE_CLEANUP_ALPHANUM_STRING(homePhoneIn,'N')
            WHERE CURRENT OF customer_update_cur;
         END IF;

         IF NOT (( promoFlag IS NULL ) AND ( promoFlag = '' ))
         THEN
            UPDATE CUSTOMER
            SET SPCL_PROMO_FLAG = promoFlag
            WHERE CURRENT OF customer_update_cur;
         END IF;

         COMMIT;
         CLOSE customer_update_cur;

    END;

    -- Return a dummy cursor so generic DAO can call the proc
    OPEN result_cur FOR
      SELECT 'OK' as "status"
      FROM DUAL;

    RETURN result_cur;

END;
.
/
