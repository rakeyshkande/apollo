CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_SHIP_DATES (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_SHIP_DATES
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_SHIP_DATES (productId IN varchar2 )
-- Returns: ref_cursor for
--          PRODUCT_ID      VARCHAR2(10)
--          MONDAY_FLAG     VARCHAR2(1)
--          TUESDAY_FLAG    VARCHAR2(1)
--          WEDNESDAY_FLAG  VARCHAR2(1)
--          THURSDAY_FLAG   VARCHAR2(1)
--          FRIDAY_FLAG     VARCHAR2(1)
--          SATURDAY_FLAG   VARCHAR2(1)
--          SUNDAY_FLAG     VARCHAR2(1)
--
-- Description:   Queries the PRODUCT_SHIP_DATES table by PRODUCT_ID and
--                returns a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT PRODUCT_ID     as "productId",
               MONDAY_FLAG    as "mondayFlag",
               TUESDAY_FLAG   as "tuesdayFlag",
               WEDNESDAY_FLAG as "wednesdayFlag",
               THURSDAY_FLAG  as "thursdayFlag",
               FRIDAY_FLAG    as "fridayFlag",
               SATURDAY_FLAG  as "saturdayFlag",
               SUNDAY_FLAG    as "sundayFlag"
             FROM PRODUCT_SHIP_DATES
    WHERE PRODUCT_ID = productId;

    RETURN cur_cursor;
END
;
.
/
