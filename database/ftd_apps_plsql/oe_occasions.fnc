CREATE OR REPLACE
FUNCTION ftd_apps.OE_OCCASIONS  RETURN
    types.ref_cursor
--=====================================================================
--
-- Name:    OE_OCCASIONS
-- Type:    Function
-- Syntax:  OE_OCCASIONS ( )
-- Returns: ref_cursor for
--          OCCASION_ID     NUMBER
--          DESCRIPTION     VARCHAR2(30)
--          DISPLAY_ORDER   NUMBER
--
-- Description:   Queries the product indexes to list those flagged for
--                occasion search.
--
--======================================================================

AS
    occasion_cursor types.ref_cursor;
BEGIN
    OPEN occasion_cursor FOR
        SELECT INDEX_ID as "occasionId",
               INDEX_NAME as "description",
               DISPLAY_ORDER as "displayOrder"
        FROM PRODUCT_INDEX
        WHERE ADV_SEARCH_OCCASIONS_FLAG = 'Y'
        ORDER BY DISPLAY_ORDER;

    RETURN occasion_cursor;
END
;
.
/
