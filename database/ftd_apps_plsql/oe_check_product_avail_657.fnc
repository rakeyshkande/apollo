CREATE OR REPLACE
FUNCTION ftd_apps.OE_CHECK_PRODUCT_AVAIL_657 (inZip IN VARCHAR2,
   productId IN VARCHAR2 )
  RETURN varchar2

AS
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
    availableFlag       VARCHAR2(1);
    zipGNADDFlag        VARCHAR2(1);
    zipDownFlag         VARCHAR2(6);
    retFlag             VARCHAR2(1);
    tmpZip              VARCHAR2(10) := UPPER(SUBSTR(inZip,1,5));
    cszZip              VARCHAR2(10);
    tmpProductId        VARCHAR2(100);

BEGIN

    -- Retruns the following values:
    -- D = Product not available and zip code is shut down
    -- N = Product not available and zip code is in GNADD
    -- G = Product not available and zip code is not in GNADD
    -- Y = Product available

  -- If the product ID is novator ID and they are different, make sure we use product id
  --   because some underlying tables only have product ID data
  BEGIN
    SELECT product_id INTO tmpProductId
    FROM product_master
    WHERE novator_id = UPPER(productId);
    EXCEPTION WHEN NO_DATA_FOUND THEN
        tmpProductId := productId;
  END;


  IF(LENGTH(tmpZip) >= 5)
    THEN
    -- Determine if the input zip code is Canadian
    IF ( TRANSLATE(tmpZip, alphaChars || numChars, alphaSub || numSub) LIKE '@#@%' )
    THEN
        -- Use the first three chars for all canadian lookups
        cszZip := SUBSTR(tmpZip,1,3);
    ELSE
        cszZip := tmpZip;
    END IF;

    BEGIN
    SELECT cszp.available_flag INTO availableFlag
    FROM CSZ_PRODUCTS cszp
    WHERE cszp.ZIP_CODE LIKE CONCAT(cszZip, '%') AND cszp.PRODUCT_ID = UPPER(tmpProductId);
    EXCEPTION WHEN NO_DATA_FOUND THEN
        availableFlag := 'N';
    END;

    BEGIN
    SELECT csz.CSZ_ZIP_CODE INTO zipDownFlag
    FROM CSZ_AVAIL csz
    WHERE csz.CSZ_ZIP_CODE LIKE CONCAT(cszZip, '%');
    EXCEPTION WHEN NO_DATA_FOUND THEN
        zipDownFlag := 'Y';
    END;

    BEGIN
    SELECT NVL(csz.GNADD_FLAG, 'N') INTO zipGNADDFlag
    FROM CSZ_AVAIL csz
    WHERE csz.CSZ_ZIP_CODE LIKE CONCAT(cszZip, '%');
    EXCEPTION WHEN NO_DATA_FOUND THEN
        zipGNADDFlag := 'N';
    END;
    ELSE
      availableFlag := 'Y';
    END IF;

    IF (availableFlag = 'N' AND zipDownFlag = 'Y')
      THEN retFlag := 'D';
    ELSIF (availableFlag = 'N' AND zipDownFlag != 'Y' AND zipGNADDFlag != 'Y')
      THEN retFlag := 'N';
    ELSIF (availableFlag = 'N' AND zipGNADDFlag != 'N')
      THEN retFlag := 'G';
    ELSIF (availableFlag != 'N')
      THEN retFlag := availableFlag;
    END IF;

    RETURN retFlag;
END;
.
/
