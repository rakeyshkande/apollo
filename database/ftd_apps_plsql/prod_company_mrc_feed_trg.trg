create or replace
TRIGGER ftd_apps.prod_company_mrc_feed_trg
AFTER INSERT OR DELETE
 ON ftd_apps.product_company_xref
REFERENCING new as new old as old
FOR EACH ROW

DECLARE

v_count    number := 0;
v_availability_flag varchar2(1);

BEGIN
	
	    IF DELETING then       

            select count(*) into v_count from ptn_mercent.mrcnt_product_feed
            where product_id = :old.product_id and status = 'NEW';
            
            select status into v_availability_flag from ftd_apps.product_master
            where product_id = :old.product_id;

            if v_count = 0 and :old.company_id='FTD' and v_availability_flag='A' then

                insert into ptn_mercent.mrcnt_product_feed(
                    MRCNT_PRODUCT_FEED_ID,
                    PRODUCT_ID,
                    STATUS,
                    CREATED_ON,
                    CREATED_BY,
                    UPDATED_ON,
                    UPDATED_BY)
                values (
                    ptn_mercent.mrcnt_product_feed_id_sq.nextval,
                    :OLD.product_id,
                    'NEW',
                    sysdate,
                    'SYS',
                    sysdate,
                    'SYS');

            end if;
		ELSIF INSERTING then
			select count(*) into v_count from ptn_mercent.mrcnt_product_feed
            where product_id = :new.product_id and status = 'NEW';
            
            select status into v_availability_flag from ftd_apps.product_master
            where product_id = :new.product_id;

            if v_count = 0 and :new.company_id='FTD' and v_availability_flag='A' then

                insert into ptn_mercent.mrcnt_product_feed(
                    MRCNT_PRODUCT_FEED_ID,
                    PRODUCT_ID,
                    STATUS,
                    CREATED_ON,
                    CREATED_BY,
                    UPDATED_ON,
                    UPDATED_BY)
                values (
                    ptn_mercent.mrcnt_product_feed_id_sq.nextval,
                    :NEW.product_id,
                    'NEW',
                    sysdate,
                    'SYS',
                    sysdate,
                    'SYS');

            end if;
        END IF;

END prod_company_mrc_feed_trg;
/
