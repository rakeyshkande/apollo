CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_FLOWER_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_FLOWERS_LIST
-- Type:    Function
-- Syntax:  SP_GET_FLOWERS_LIST ()
-- Returns: ref_cursor for
--          FLOWER_ID                   NUMBER
--          FLOWER_NAME                 VARCHAR2(35)
--
-- Description:   Queries the FLOWERS table and returns a ref cursor
--                to the resulting row.
--
--=========================================================

AS
    flower_list_cursor types.ref_cursor;
BEGIN
    OPEN flower_list_cursor FOR
        SELECT FLOWER_ID            as "flowerId",
               FLOWER_NAME          as "flowerName"
        FROM FLOWERS;

    RETURN flower_list_cursor;
END
;
.
/
