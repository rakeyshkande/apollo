CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_VENDOR_CARRIER_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_VENDOR_CARRIER_LIST
-- Type:    Function
-- Syntax:  SP_GET_VENDOR_CARRIER_LIST ()
-- Returns: ref_cursor for
--          DEFAULT_CARRIER   VARCHAR2
--          VENDOR_ID VARCHAR2
--
--
-- Description:   Queries the VENDOR_MASTER table and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
BEGIN
    OPEN vendor_cursor FOR
        SELECT DEFAULT_CARRIER as "defaultcarrier",
            VENDOR_NAME    as "vendorName"
          FROM VENDOR_MASTER
          ORDER BY VENDOR_NAME;

    RETURN vendor_cursor;
END
;
.
/
