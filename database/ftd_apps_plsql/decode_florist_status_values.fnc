CREATE OR REPLACE
FUNCTION ftd_apps.DECODE_FLORIST_STATUS_VALUES
(
IN_STATUS               IN VARCHAR2,
IN_BLOCK_TYPE           IN VARCHAR2
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for decoding the given florist master status
        checking if the florist is blocked.  If it is blocked then, return
        the given block type.  If it is suspended, return 'M'.  This function
        exists to regess to the old florist data model where florist blocks
        contained the blocks and suspends in the block type field with values
        of O, H, S, M.

Input:
        status                          varchar2 (florist master status)
        block type                      varchar2 (florist blocks block type)

Output:
        block or suspend value (O, H, S, M)

-----------------------------------------------------------------------------*/
v_return        varchar2(10);

BEGIN

CASE in_status
WHEN 'Blocked' THEN
        v_return := in_block_type;
WHEN 'Mercury Suspend' THEN
        v_return := 'M';
WHEN 'GoTo Suspemd' THEN
        v_return := 'M';
ELSE
        v_return := null;
END CASE;

RETURN v_return;

END;
.
/
