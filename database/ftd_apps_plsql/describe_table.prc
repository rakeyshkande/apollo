CREATE OR REPLACE
PROCEDURE ftd_apps.DESCRIBE_TABLE -- To call:
-- set serverout on
-- exec describe_table('<OWNER CAPS>', '<TABLE CAPS>')
  ( Desc_Table_Owner IN VARCHAR2, Desc_Table_Name IN VARCHAR2 ) IS

	Table_Exists		CHAR(1);
	Fill_Buffer		VARCHAR2(100);
	Line_Buffer		VARCHAR2(255);
	Comma_List_Start	CHAR(1) := 'Y';

	CURSOR Tab_Column_Attributes ( Tab_Owner VARCHAR2, Tab_Name VARCHAR2 ) IS
		SELECT ATC.Column_Name, ATC.Nullable, ATC.Data_Type, ATC.Data_Length, ATC.Data_Precision,
			ATC.Data_Scale, ATC.Data_Default, NVL(KC.Position, 9999) Is_Prim_Key
		  FROM All_Tab_Columns ATC, Key_Columns KC
		  WHERE ATC.Owner = Tab_Owner
		  AND ATC.Table_Name = Tab_Name
		  AND ATC.Owner = KC.Table_Owner (+)
		  AND ATC.Table_Name = KC.Table_Name (+)
		  AND ATC.Column_Name = KC.Column_Name (+)
		  AND 'P' = KC.Constraint_Type (+)
		  ORDER BY 8, 1;
	Tab_Col_Attrs		Tab_Column_Attributes%ROWTYPE;

	CURSOR Tab_Indexes ( Tab_Owner VARCHAR2, Tab_Name VARCHAR2 ) IS
		SELECT AI.Index_Name, AI.Uniqueness
		  FROM All_Indexes AI
		  WHERE Table_Owner = Tab_Owner
		  AND Table_Name = Tab_Name
		  ORDER BY Uniqueness DESC;
	Tab_Inds		Tab_Indexes%ROWTYPE;

	CURSOR Tab_Index_Columns ( Tab_Owner VARCHAR2, Tab_Name VARCHAR2, Ind_Name VARCHAR2 ) IS
		SELECT AIC.Column_Name
		  FROM All_Ind_Columns AIC
		  WHERE Table_Owner = Tab_Owner
		  AND Table_Name = Tab_Name
		  AND Index_Name = Ind_Name
		  ORDER BY Column_Position ASC;
	Ind_Cols		Tab_Index_Columns%ROWTYPE;

BEGIN
	DBMS_OUTPUT.ENABLE(300000);

	BEGIN
		SELECT 'X' INTO Table_Exists
		  FROM All_Tables
		  WHERE Owner = Desc_Table_Owner
		  AND Table_Name = Desc_Table_Name;
	  EXCEPTION
		WHEN NO_DATA_FOUND THEN
			DBMS_OUTPUT.PUT_LINE('Table ' || Desc_Table_Name || ' owned by ' || Desc_Table_Owner || ' does not exist.');
			RETURN;
	END;

	DBMS_OUTPUT.PUT_LINE('	');
	DBMS_OUTPUT.PUT_LINE('Name                            Null?    Type');
	DBMS_OUTPUT.PUT_LINE('------------------------------- -------- ----');

	FOR Tab_Col_Attrs IN Tab_Column_Attributes(Desc_Table_Owner, Desc_Table_Name)
	  LOOP
		Fill_Buffer := '                                                              ';
		Fill_Buffer := SUBSTR(Fill_Buffer, 1, 32 - LENGTH(Tab_Col_Attrs.Column_Name));
		DBMS_OUTPUT.PUT(Tab_Col_Attrs.Column_Name || Fill_Buffer);

		IF (Tab_Col_Attrs.Nullable = 'N')
		  THEN
			DBMS_OUTPUT.PUT('NOT NULL ');
		  ELSE
			DBMS_OUTPUT.PUT('         ');
		END IF;

		DBMS_OUTPUT.PUT(Tab_Col_Attrs.Data_Type);
		IF (Tab_Col_Attrs.Data_Type IN ('CHAR', 'RAW', 'VARCHAR', 'VARCHAR2'))
		  THEN
			IF (NVL(Tab_Col_Attrs.Data_Length, 0) != 0)
			  THEN
				DBMS_OUTPUT.PUT('(' || Tab_Col_Attrs.Data_Length || ')');
			END IF;

		ELSIF (Tab_Col_Attrs.Data_Type IN ('NUMBER', 'FLOAT', 'INTEGER', 'SMALLINT'))
		  THEN
			IF (NVL(Tab_Col_Attrs.Data_Precision, 0) != 0)
			  THEN
				DBMS_OUTPUT.PUT('(' || Tab_Col_Attrs.Data_Precision);

				IF (NVL(Tab_Col_Attrs.Data_Scale, 0) != 0)
				  THEN
					DBMS_OUTPUT.PUT(',' || Tab_Col_Attrs.Data_Scale);
				END IF;

				DBMS_OUTPUT.PUT(')');
			END IF;
		END IF;

		IF (Tab_Col_Attrs.Data_Default IS NOT NULL)
		  THEN
		  	DBMS_OUTPUT.PUT('  default ' || Tab_Col_Attrs.Data_Default);
		END IF;

		DBMS_OUTPUT.NEW_LINE;
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('	');
	DBMS_OUTPUT.PUT_LINE('Indexes');
	DBMS_OUTPUT.PUT_LINE('-------------------------------------------------------------------------------');

	FOR Tab_Inds IN Tab_Indexes(Desc_Table_Owner, Desc_Table_Name)
	  LOOP
		Fill_Buffer := '                                                              ';
		Fill_Buffer := SUBSTR(Fill_Buffer, 1, 32 - LENGTH(Tab_Inds.Index_Name));
		Line_Buffer := Tab_Inds.Index_Name || Fill_Buffer || Tab_Inds.Uniqueness || '(';

		Comma_List_Start := 'Y';
		FOR Ind_Cols IN Tab_Index_Columns(Desc_Table_Owner, Desc_Table_Name, Tab_Inds.Index_Name)
		  LOOP
			IF (Comma_List_Start = 'Y')
			  THEN
				Comma_List_Start := 'N';
			ELSE
				Line_Buffer := Line_Buffer || ', ';
			END IF;

			IF (LENGTH(Line_Buffer || Ind_Cols.Column_Name || ', ') > 255)
			  THEN
				DBMS_OUTPUT.PUT_LINE(Line_Buffer);
				Line_Buffer := Ind_Cols.Column_Name;
			ELSE
				Line_Buffer := Line_Buffer || Ind_Cols.Column_Name;
			END IF;
		END LOOP;

		DBMS_OUTPUT.PUT(Line_Buffer);
		IF (LENGTH(Line_Buffer || ')') > 255)
		  THEN
			DBMS_OUTPUT.PUT_LINE('');
		END IF;
		DBMS_OUTPUT.PUT_LINE(')');
	END LOOP;

END
;
.
/
