CREATE OR REPLACE
PROCEDURE ftd_apps.SP_MAINTAIN_PRODIDX_DROPSHIP as
  CURSOR all_prod_indexes IS
  SELECT * FROM product_index
  WHERE dropship_flag = 'Z'
  ORDER BY index_id;

  top_level             all_prod_indexes%ROWTYPE;

  nondropship_found     BOOLEAN := FALSE;
  dropship_value        product_index.dropship_flag%TYPE;

  FUNCTION SP_FIND_NONDROPSHIPS ( indexId IN NUMBER )
  RETURN BOOLEAN
  IS

    CURSOR index_products ( indexId IN NUMBER ) IS
    SELECT p.product_id, p.delivery_type
    FROM product_master p, product_index_xref i
    WHERE i.product_index_id = indexId
    AND i.product_id = p.product_id;

    CURSOR index_subindexes ( indexId IN NUMBER ) IS
    SELECT * FROM product_index
    WHERE parent_index_id = indexId;

    products_listing    index_products%ROWTYPE;
    sub_indexes         index_subindexes%ROWTYPE;

    nondropship_found   BOOLEAN := FALSE;

  BEGIN

    FOR products_listing IN index_products(indexId)
    LOOP

      nondropship_found := nondropship_found OR
                          ( products_listing.delivery_type != 'D' );
    END LOOP;

    FOR sub_indexes IN index_subindexes(indexId)
    LOOP

      nondropship_found := nondropship_found OR
                          ( SP_FIND_NONDROPSHIPS(sub_indexes.index_id) );
    END LOOP;

    RETURN nondropship_found;
  END;

begin
  UPDATE product_index
  SET dropship_flag = 'Z';
  COMMIT;

  WHILE (TRUE)
  LOOP

    OPEN all_prod_indexes;
    FETCH all_prod_indexes INTO top_level;
    IF (all_prod_indexes%NOTFOUND)
    THEN
      CLOSE all_prod_indexes;
      RETURN;
    END IF;
    CLOSE all_prod_indexes;

    nondropship_found := SP_FIND_NONDROPSHIPS(top_level.index_id);

    IF ( nondropship_found )
    THEN
      dropship_value := 'N';
    ELSE
      dropship_value := 'Y';
    END IF;

    UPDATE product_index
    SET dropship_flag = dropship_value
    WHERE index_id = top_level.index_id;

    COMMIT;
  END LOOP;

end
;
.
/
