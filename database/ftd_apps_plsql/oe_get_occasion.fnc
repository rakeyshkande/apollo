CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_OCCASION (occasionId IN VARCHAR2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_OCCASION
-- Type:    Function
-- Syntax:  OE_GET_OCCASION ( occasionId IN VARCHAR2 )

--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT OCCASION_ID as "occasionId",
               DESCRIPTION as "description",
               INDEX_ID as "indexId",
	       DISPLAY_ORDER as "displayOrder",
	       ACTIVE as "active",
	       USER_EDITABLE as "userEditable"
        FROM OCCASION
        WHERE OCCASION_ID = occasionId;

    RETURN cur_cursor;
END;
.
/
