CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_UPSELL_BY_SKU (skuIn IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_UPSELL_BY_SKU
-- Type:    Function
-- Syntax:  SP_GET_UPSELL_BY_SKU (skuId in VARCHAR2 )
-- Returns: ref_cursor
--
-- Description:   Queries the UPSELL_MASTER table by UPSELL_MASTER_ID
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT UPSELL_MASTER_ID            as "masterID",
               UPSELL_NAME          as "masterName",
               UPSELL_DESCRIPTION                as "masterDescription",
               UPSELL_STATUS  as "masterStatus",
               SEARCH_PRIORITY as "searchPriority",
               CORPORATE_SITE as "corporateSite",
               GBB_POPOVER_FLAG as "gbbPopoverFlag",
               GBB_TITLE_TXT as "gbbTitle"
          FROM UPSELL_MASTER
WHERE UPSELL_MASTER_ID = skuIn
          ORDER BY UPSELL_NAME;

    RETURN cur_cursor;
END;
.
/
