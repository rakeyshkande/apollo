CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_COMPONENTS (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_COMPONENTS
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_COMPONENTS ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--          PRODUCT_COMPONENT_ID      VARCHAR2(25)
--
-- Description:   Queries the PRODUCT_COMPONENT_SKU table by PRODUCT ID and returns
--                a ref cursor to the resulting row.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT pcs.COMPONENT_SKU_ID    as "componentSkuId",
               csm.COMPONENT_SKU_NAME  as "componentSkuName"
          FROM PRODUCT_COMPONENT_SKU pcs, COMPONENT_SKU_MASTER csm
    WHERE pcs.PRODUCT_ID = productId
    AND pcs.COMPONENT_SKU_ID = csm.COMPONENT_SKU_ID;

    RETURN cur_cursor;
END
;
.
/
