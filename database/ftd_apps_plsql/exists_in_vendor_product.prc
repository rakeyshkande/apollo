CREATE OR REPLACE PROCEDURE FTD_APPS.EXISTS_IN_VENDOR_PRODUCT (
  IN_PRODUCT_SUBCODE_ID	  IN 		VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
  OUT_EXISTS				  OUT    VARCHAR2,
  OUT_STATUS				  OUT		VARCHAR2,
  OUT_MESSAGE				  OUT	 	VARCHAR2)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure tells if a given product_id or product_subcode_id
   exists in ftd_apps.vendor_product table

Input:
        product_subcode_id   VARCHAR2


Output:
		  exists				 Y/N
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/

	CURSOR cur_exists IS
	SELECT COUNT(1)
	  FROM ftd_apps.vendor_product
	 WHERE product_subcode_id = in_product_subcode_id;
	 
	v_cnt			NUMBER(4) := 0;
 
BEGIN

   OPEN cur_exists;
   FETCH cur_exists INTO v_cnt;
   CLOSE cur_exists;
   
   IF v_cnt = 0 THEN
      out_exists := 'N';
   ELSE
      out_exists := 'Y';
   END IF;
   
   out_status := 'Y';

EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED IN FTD_APPS.PRODUCT_EXISTS_IN_VENDOR_PRODUCT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END EXISTS_IN_VENDOR_PRODUCT;
.
/