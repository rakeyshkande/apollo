CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_WEST_PRODUCT_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_WEST_PRODUCT_LIST
-- Type:    Function
-- Syntax:  SP_GET_WEST_PRODUCT_LIST ()
-- Returns: ref_cursor for
--          PQUAD_PRODUCT_ID            VARCHAR2(10)
--     --
-- Description:   Queries the PRODUCT_MASTER table and returns a ref cursor 
--                to the resulting row.
--
--=========================================================

AS
    west_product_list_cursor types.ref_cursor;
BEGIN
    OPEN west_product_list_cursor FOR
        select distinct(pm.pquad_product_id)
		from ftd_apps.product_master pm
		where pm.status = 'A'
		and pm.shipping_system = 'FTD WEST'
		and pm.PQUAD_PRODUCT_ID is not null
		and not exists(
		    select 1
		    from FTD_APPS.ADDON a
		    where a.PRODUCT_ID = pm.PRODUCT_ID
		)
		order by pquad_product_id asc;

    RETURN west_product_list_cursor;
END;
.
/
