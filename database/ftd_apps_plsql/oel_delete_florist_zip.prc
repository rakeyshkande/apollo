CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_DELETE_FLORIST_ZIP (
  floristId in varchar2,
  zip in varchar2
)
as
--==============================================================================
--
-- Name:    OEL_DELETE_FLORIST_ZIP
-- Type:    Procedure
-- Syntax:  OEL_DELETE_FLORIST_ZIP ( floristId in varchar2,
--                                     zip in varchar2 )
--
-- Description:   Deletes a row from FLORIST_ZIPS by FLORIST_ID and ZIP_CODE.
--
--==============================================================================

   v_system_message_id number;
   v_out_status varchar2(1);
   v_message varchar2(500);

begin

   frp.misc_pkg.insert_system_messages(
      in_source             => 'oel_delete_florist_zip',
      in_type               => 'INFO',
      in_message            => 'OE XML Listener is active',
      in_computer           => sys_context('USERENV','HOST'),
      out_system_message_id => v_system_message_id,
      out_status            => v_out_status,
      out_message           => v_message);

 delete FROM FLORIST_ZIPS
  where FLORIST_ID = floristId AND ZIP_CODE = zip;

  COMMIT;

end
;
.
/
