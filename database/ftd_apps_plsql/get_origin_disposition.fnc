CREATE OR REPLACE
FUNCTION ftd_apps.GET_ORIGIN_DISPOSITION
(
IN_ORIGIN_ID                    IN ORIGIN_DISPOSITION.ORIGIN_ID%TYPE,
IN_FTD_DISPOSITION_ID           IN ORIGIN_DISPOSITION.FTD_DISPOSITION_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning the origin disposition
        for the given origin and FTD disposition.

Input:
        origin_id               varchar2
        ftd_disposition_id      char

Output:
        origin_disposition_id   varchar2

-----------------------------------------------------------------------------*/
v_origin_disposition_id         ORIGIN_DISPOSITION.ORIGIN_DISPOSITION_ID%TYPE;

CURSOR c IS
        SELECT  origin_disposition_id
        FROM    origin_disposition
        WHERE   origin_id = in_origin_id
        AND     ftd_disposition_id = in_ftd_disposition_id;

BEGIN

OPEN c;
FETCH c INTO v_origin_disposition_id;
CLOSE c;

RETURN v_origin_disposition_id;

END;
.
/
