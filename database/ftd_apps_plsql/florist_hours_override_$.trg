CREATE OR REPLACE TRIGGER ftd_apps.florist_hours_override_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_hours_override
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_hours_override$ (
         FLORIST_ID, 
         DAY_OF_WEEK,
         OVERRIDE_DATE,
         CREATED_ON, 
         CREATED_BY, 
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.DAY_OF_WEEK,
         :new.OVERRIDE_DATE,
         :new.CREATED_ON,
         :new.CREATED_BY,
         :new.UPDATED_ON,
         :new.UPDATED_BY,
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_hours_override$ (
         FLORIST_ID, 
         DAY_OF_WEEK,
         OVERRIDE_DATE,
         CREATED_ON, 
         CREATED_BY, 
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.DAY_OF_WEEK,
         :old.OVERRIDE_DATE,
         :old.CREATED_ON,
         :old.CREATED_BY,
         :old.UPDATED_ON,
         :old.UPDATED_BY,
         'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.florist_hours_override$ (
         FLORIST_ID, 
         DAY_OF_WEEK,
         OVERRIDE_DATE,
         CREATED_ON, 
         CREATED_BY, 
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.DAY_OF_WEEK,
         :new.OVERRIDE_DATE,
         :new.CREATED_ON,
         :new.CREATED_BY,
         :new.UPDATED_ON,
         :new.UPDATED_BY,
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_hours_override$ (
         FLORIST_ID, 
         DAY_OF_WEEK,
         OVERRIDE_DATE,
         CREATED_ON, 
         CREATED_BY, 
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.DAY_OF_WEEK,
         :old.OVERRIDE_DATE,
         :old.CREATED_ON,
         :old.CREATED_BY,
         :old.UPDATED_ON,
         :old.UPDATED_BY,
         'DEL',SYSDATE);

   END IF;

END;
/
