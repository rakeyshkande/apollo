CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_COLORS_ANF (
 productId in varchar2,
 productColor in varchar2,
 orderBy in number
)
authid current_user
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_COLORS_ANF
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_COLORS_ANF ( productId in varchar2,
--                                     productColor in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_COLORS.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_COLORS
  INSERT INTO PRODUCT_COLORS
      ( PRODUCT_ID,
				PRODUCT_COLOR,
				ORDER_BY
		  )
  VALUES
		  ( productId,
				productColor,
				orderBy
		  );

end;
.
/
