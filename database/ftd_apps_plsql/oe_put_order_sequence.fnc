CREATE OR REPLACE
FUNCTION ftd_apps.OE_PUT_ORDER_SEQUENCE (inOrderSequence IN NUMBER)
--==============================================================================
--
-- Name:    OE_PUT_ORDER_SEQUENCE
-- Type:    Function
-- Syntax:  OE_PUT_ORDER_SEQUENCE ( inOrderSequence IN NUMBER )
-- Returns: ref_cursor for next sequence val
--
-- Description:   Places a discarded sequence value into ORDER_SEQUENCE_DISCARD
--
--==============================================================================
return types.ref_cursor
is
  status_cursor   types.ref_cursor;
begin
  begin
    INSERT INTO ORDER_SEQUENCE_DISCARD VALUES (inOrderSequence, 'A');
    COMMIT;

  exception when DUP_VAL_ON_INDEX THEN
    UPDATE ORDER_SEQUENCE_DISCARD SET STATUS = 'A'
    WHERE ORDER_SEQUENCE = inOrderSequence;
  end;

  OPEN status_cursor FOR
  SELECT 'OK' as "status" FROM DUAL;
  RETURN status_cursor;
end;
.
/
