CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_USER_LIST (inUserId     IN varchar2,
  inRoleId       IN VARCHAR2,
  inCallCenterID IN VARCHAR2,
  inStatus       IN VARCHAR2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_USER_LIST
-- Type:    Function
-- Syntax:  OE_GET_USER_LIST (inUserId        IN VARCHAR2,
--                            inRoleId        IN VARCHAR2,
--                            inCallCenterId  IN VARCHAR2,
--                            inStatus        IN VARCHAR2)
-- Returns: ref_cursor for
--         USER_ID                VARCHAR2(8)
--         ROLE_ID                varchar2(10)
--         CALL_CENTER_ID         VARCHAR2(2)
--         FIRST_NAME             VARCHAR2(25)
--         LAST_NAME              VARCHAR2(25)
--         ACTIVE_FLAG            VARCHAR2(1)
--
-- Description:   Queries the USER_PROFILE table by any combination of the
--                supplied parameters and returns a ref cursor for resulting rows.
--
--================================================================================
AS
    user_cursor types.ref_cursor;
BEGIN
    OPEN user_cursor FOR
        SELECT USER_ID    	   as "userId",
			         ROLE_ID			   as "roleID",
               CALL_CENTER_ID  as "callCenterId",
               FIRST_NAME      as "firstName",
               LAST_NAME       as "lastName",
               ACTIVE_FLAG     as "activeFLAG"
        FROM USER_PROFILE up
       WHERE ( inCallCenterId IS NULL
           OR EXISTS
         (Select 1 from call_center cc
            where up.call_center_id = cc.call_center_id
              and cc.call_center_id = inCallCenterId ))
         AND ( inRoleId IS NULL
           OR EXISTS
          (Select 1 from role ro
             where up.role_id = ro.role_id
               and ro.role_id = inRoleId ))
         AND up.active_flag =
             DECODE (inStatus, NULL, up.active_flag, inStatus)
         AND up.user_id =
             DECODE (inUserId, NULL, up.user_id, InUserId)
         AND up.deleted_flag = 'N';

    RETURN user_cursor;
END
;
.
/
