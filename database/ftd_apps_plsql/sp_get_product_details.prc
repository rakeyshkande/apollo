create or replace procedure          ftd_apps.SP_GET_PRODUCT_DETAILS(
IN_PRODUCT_ID            IN  VARCHAR2,
OUT_PRODUCT_CUR          OUT types.ref_cursor,
OUT_KEYWORD_CUR          OUT types.ref_cursor,
OUT_COLOR_CUR            OUT types.ref_cursor,
OUT_RECIPIENT_SEARCH_CUR OUT types.ref_cursor,
OUT_COMPANY_CUR          OUT types.ref_cursor,
OUT_EXCL_STATES_CUR      OUT types.ref_cursor,
OUT_SHIP_METHODS_CUR     OUT types.ref_cursor,
OUT_SUB_CODES_CUR        OUT types.ref_cursor,
OUT_SHIP_DATES_CUR       OUT types.ref_cursor,
OUT_VENDORS_CUR          OUT types.ref_cursor,
OUT_COMPONENT_CUR        OUT types.ref_cursor,
OUT_PRODUCT_ADDON_CUR    OUT types.ref_cursor,
OUT_PRODUCT_ADDON_VASE_CUR OUT types.ref_cursor,
OUT_PRODUCT_SOURCE_CUR   OUT types.ref_cursor
)
AS
  v_product_id  PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_ID);
begin

-- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_product_id
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_product_id;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_product_id
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_product_id;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_product_id
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_product_id;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

    --Get the product master information
    OUT_PRODUCT_CUR := FTD_APPS.SP_GET_PRODUCT_BY_ID_2247(v_product_id, 'PDB');

    --Get the keywords
    OUT_KEYWORD_CUR := FTD_APPS.SP_GET_PRODUCT_KEYWORDS(v_product_id);

    --Get the colors
    OUT_COLOR_CUR := FTD_APPS.SP_GET_PRODUCT_COLORS(v_product_id);

    --Get the recipient search
    OUT_RECIPIENT_SEARCH_CUR := FTD_APPS.SP_GET_PRODUCT_RECIPIENT_SRCH(v_product_id);

    --Get the companies
    OUT_COMPANY_CUR := FTD_APPS.SP_GET_PRODUCT_COMPANIES_2247(v_product_id);

    --Get the excluded states
    OUT_EXCL_STATES_CUR := FTD_APPS.SP_GET_PROD_EXCLUDED_STATES_2(v_product_id);

    --Get the ship methods
    OUT_SHIP_METHODS_CUR := FTD_APPS.SP_GET_PRODUCT_SHIP_METHODS2(v_product_id);

    --Get the subcodes
    OUT_SUB_CODES_CUR := FTD_APPS.SP_GET_PRODUCT_SUBCODES2(v_product_id);

    --Get the ship dates
    OUT_SHIP_DATES_CUR := FTD_APPS.SP_GET_PRODUCT_SHIP_DATES(v_product_id);

    --Get the product vendors
    OUT_VENDORS_CUR := FTD_APPS.VENDOR_PKG.GET_VENDORS_FOR_PRODUCT(v_product_id);
    
    --Get the product component skus
    OUT_COMPONENT_CUR := FTD_APPS.SP_GET_PRODUCT_COMPONENTS(v_product_id);

    --Get the addons
    OUT_PRODUCT_ADDON_CUR := FTD_APPS.SP_GET_PRODUCT_ADDON(v_product_id);
    
    --Get the addons of vase type
    OUT_PRODUCT_ADDON_VASE_CUR := FTD_APPS.SP_GET_PRODUCT_ADDON_BY_TYPE(v_product_id, 7);

    --Get the product source xrefs
    OUT_PRODUCT_SOURCE_CUR := FTD_APPS.SP_GET_PRODUCT_SOURCE(v_product_id);
end;
.
/
