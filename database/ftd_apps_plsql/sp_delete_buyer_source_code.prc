CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_BUYER_SOURCE_CODE (
  asnNumberIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSERT_BUYER_SOURCE_CODE
-- Type:    Procedure
-- Syntax:  SP_INSERT_BUYER_SOURCE_CODE ( asnNumberIn in varchar2)
--
-- Description:   Deletes a row in the BUYER_SOURCE_CODE table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 delete BUYER_SOURCE_CODE
 where ASN_NUMBER = asnNumberIn;

end
;
.
/
