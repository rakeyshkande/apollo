
CREATE OR REPLACE TRIGGER FTD_APPS.STATE_MASTER_AIUD
AFTER INSERT OR UPDATE OR DELETE ON FTD_APPS.STATE_MASTER
FOR EACH ROW
BEGIN

--
-- Insert JMS message to eventually update the PAS framework tables
--
if INSERTING  or 
   (UPDATING AND :new.drop_ship_available_flag != :old.drop_ship_available_flag) then   
       PAS.POST_PAS_COMMAND('MODIFIED_STATE',:new.state_master_id);
elsif DELETING then
       PAS.POST_PAS_COMMAND('MODIFIED_STATE',:old.state_master_ID);
end if;

EXCEPTION WHEN OTHERS THEN
    NULL;
END;
/