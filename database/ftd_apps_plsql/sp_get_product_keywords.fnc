CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_KEYWORDS (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_KEYWORDS
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_KEYWORDS ( productId IN VARCHAR2)
-- Returns: ref_cursor for
--          KEYWORD             VARCHAR2(25)
--
-- Description:   Queries the PRODUCT_KEYWORD table by PRODUCT ID and returns a ref cursor
--                to the resulting row.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT KEYWORD as "keyword"
          FROM PRODUCT_KEYWORDS
    WHERE PRODUCT_ID = productId;

    RETURN cur_cursor;
END
;
.
/
