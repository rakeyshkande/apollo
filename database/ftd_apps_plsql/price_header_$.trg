-- FTD_APPS.PRICE_HEADER_$ trigger to populate FTD_APPS.PRICE_HEADER$ shadow table

CREATE OR REPLACE TRIGGER ftd_apps.price_header_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.price_header REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.price_header$ (
      	price_header_id,
      	description,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.price_header_id,
      	:NEW.description,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.price_header$ (
      	price_header_id,
      	description,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.price_header_id,
      	:OLD.description,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,      
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO ftd_apps.price_header$ (
      	price_header_id,
      	description,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.price_header_id,
      	:NEW.description,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,      	
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.price_header$ (
      	price_header_id,
      	description,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.price_header_id,
      	:OLD.description,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,      	
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
