CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_PRODUCT_SOURCE (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_PRODUCT_SOURCE
-- Type:    Procedure
-- Syntax:  SP_REMOVE_PRODUCT_SOURCE ( productId in varchar2 )
--
-- Description:   Deletes all rows from PRODUCT_SOURCE by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_SOURCE
  where PRODUCT_ID = productId;

end
;
.
/
