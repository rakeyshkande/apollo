CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_UPDATE_FLORIST_ZIP ( floristId in varchar2,
 zip in varchar2,
 lastUpdateDate in date)

as
--==============================================================================
--
-- Name:    OEL_UPDATE_FLORIST_ZIP
-- Type:    Procedure
-- Syntax:  OEL_UPDATE_FLORIST_ZIP ( floristId in varchar2,
--                                     zip in varchar2,
--                                     lastUpdateDate in date)
--
-- Description:   Attempts to insert a row into FLORIST_ZIPS.  If the row
--                already exists (by floristId), then updates the existing row.
--
-- This procedure should NOT BE USED as is does not correctly update PAS
--==============================================================================

   v_system_message_id number;
   v_out_status varchar2(1);
   v_message varchar2(500);

begin

   frp.misc_pkg.insert_system_messages(
      in_source             => 'oel_update_florist_zip',
      in_type               => 'INFO',
      in_message            => 'OE XML Listener is active',
      in_computer           => sys_context('USERENV','HOST'),
      out_system_message_id => v_system_message_id,
      out_status            => v_out_status,
      out_message           => v_message);

  -- Attempt to insert into FLORIST_ZIPS table
  INSERT INTO FLORIST_ZIPS
    ( FLORIST_ID,
      ZIP_CODE,
      LAST_UPDATE_DATE
    )
  VALUES
    ( floristId,
      UPPER(zip),
      lastUpdateDate
    );

  COMMIT;

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

  -- If a row already exists for this FLORIST_ZIPS then update
  UPDATE FLORIST_ZIPS
    SET ZIP_CODE = UPPER(zip),
      LAST_UPDATE_DATE = lastUpdateDate
    WHERE FLORIST_ID = floristId;

  COMMIT;
end
;
.
/
