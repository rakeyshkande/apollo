CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_VALID_ZIPCODE
    (zipCodeIn IN VARCHAR2,
  cityIn IN VARCHAR2,
  stateIn IN VARCHAR2)
  RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_VALID_ZIPCODE
-- Type:    Function
-- Syntax:  OE_GET_VALID_ZIPCODE(zipcodeIn IN VARCHAR2,
--                              cityIn IN VARCHAR2,
--                              stateIn IN VARCHAR2)
-- Returns: ref_cursor for
--          zipCode         VARCHAR2(10)
--          city            VARCHAR2(50)
--          state           VARCHAR2(2)
--          gnaddLevel      NUMBER
--
-- Description:   Queries the ZIP_CODE table to validate the provided zip code.
--                If city and state are provided, orders the result set to
--                present the row matching input city and state first.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT z.ZIP_CODE_ID as "zipCode",
            z.CITY as "city",
            z.STATE_ID as "state",
            gp.GNADD_LEVEL as "gnaddLevel",

            -- Dummied out gnadd flag because join to CSZ
            --   was causing validations to fail
            null as "zipGnaddFlag"

        FROM ZIP_CODE z, GLOBAL_PARMS gp
        WHERE z.ZIP_CODE_ID = UPPER(zipCodeIn)
        ORDER BY DECODE(z.CITY, upper(cityIn), 0, 1) + DECODE(z.STATE_ID, upper(stateIn), 0, 1) ASC;

    RETURN cur_cursor;
END
;
.
/
