CREATE OR REPLACE
TRIGGER FTD_APPS.INV_TRK_PAS_UPDATE_$ 
	AFTER UPDATE OR DELETE ON FTD_APPS.INV_TRK
	FOR EACH ROW

/*****************************************************************************************
* THIS TRIGGER IS TO IDENTIFY IF THERE IS ANY DATA CHANGE THAT SHOULD BE TRIGGERED TO PAS
*****************************************************************************************/

DECLARE

	V_STAT								            VARCHAR2(10);
	V_MESS								            VARCHAR2(2000);
	V_PAYLOAD							            VARCHAR2(2000);
	V_EXCEPTION							          EXCEPTION;
	V_EXCEPTION_TXT						        VARCHAR2(200);
	V_SYSTEM_MESSAGE_ID 			        NUMBER;

	V_TEMP_DATE               				DATE; 
	V_END_DATE                				DATE;
        V_ADD_DAYS                                              NUMBER;
	V_MAX_END_DATE						DATE;
	V_MAX_DAYS						NUMBER;
	V_TRIGGER_PAS             				CHAR;
	V_PRIORITY                                              INTEGER;

BEGIN

	-- DEFAULT VALUES
	V_TEMP_DATE := TRUNC(SYSDATE);
	V_TRIGGER_PAS:= 'N';

	IF UPDATING  THEN	

		IF :OLD.START_DATE <> :NEW.START_DATE OR
		:OLD.END_DATE <> :NEW.END_DATE OR
		:OLD.REVISED_FORECAST_QTY <> :NEW.REVISED_FORECAST_QTY OR
		:OLD.SHUTDOWN_THRESHOLD_QTY <> :NEW.SHUTDOWN_THRESHOLD_QTY OR
		:OLD.STATUS <> :NEW.STATUS THEN	

			V_TRIGGER_PAS := 'Y';

			IF :OLD.START_DATE <= :NEW.START_DATE AND :OLD.START_DATE > SYSDATE THEN
				V_TEMP_DATE := :OLD.START_DATE;
			ELSE 
				IF :OLD.START_DATE >= :NEW.START_DATE AND :NEW.START_DATE > SYSDATE THEN
					V_TEMP_DATE := :NEW.START_DATE;
				END IF;
			END IF;

			IF :OLD.END_DATE >= :NEW.END_DATE THEN
				V_END_DATE := :OLD.END_DATE;
			ELSE 
				V_END_DATE := :NEW.END_DATE;
			END IF;
		END IF;

	ELSE
		V_TRIGGER_PAS := 'Y';
		V_END_DATE := :OLD.END_DATE;
		IF :OLD.START_DATE > SYSDATE THEN
			V_TEMP_DATE := :OLD.START_DATE;
		END IF;
	END IF;

        SELECT FGP.VALUE INTO V_MAX_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'MAX_DAYS';
        SELECT FGP.VALUE INTO V_ADD_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'ADD_DAYS';

	IF (V_TRIGGER_PAS = 'Y') THEN
		v_max_end_date := trunc(sysdate) + v_max_days - v_add_days;

		IF (v_end_date > v_max_end_date) THEN
			v_end_date := v_max_end_date;
		END IF;

		v_end_date := v_end_date + v_add_days;

		WHILE V_TEMP_DATE <= (V_END_DATE) LOOP
			v_priority := v_temp_date - trunc(sysdate);
			v_mess := TO_CHAR(V_TEMP_DATE,'MMDDYYYY') || ' ' || :OLD.VENDOR_ID || ' ' || :OLD.PRODUCT_ID || ' ' || 'ALL PROCESS_INVALIDATION';
			PAS.POST_PAS_COMMAND_PRIORITY('ADD_VENDOR_PRODUCT_STATE_DATE', v_mess, v_priority);
			V_TEMP_DATE := V_TEMP_DATE + 1; 
		END LOOP;
	END IF;

	EXCEPTION    
		WHEN OTHERS THEN  
			V_EXCEPTION_TXT := 'ERROR OCCURRED IN INV_TRK_PAS_UPDATE_$ TRIGGER: INVENTORY TRACKING ID = ' || :NEW.INV_TRK_ID || ' [SQLCODE = ' || SQLCODE || '] ' ||SUBSTR (SQLERRM,1,200);          

		FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES (
		IN_SOURCE                => 'INV_TRK_PAS_UPDATE TRIGGER',
		IN_TYPE                  => 'SYSTEM EXCEPTION',
		IN_MESSAGE               => V_EXCEPTION_TXT,
		IN_COMPUTER              => SYS_CONTEXT('USERENV','HOST'),
		IN_EMAIL_SUBJECT         => 'PAS UPDATE FAILURE',
		OUT_SYSTEM_MESSAGE_ID    => V_SYSTEM_MESSAGE_ID,
		OUT_STATUS               => V_STAT,
		OUT_MESSAGE              => V_MESS
		);		

END;

/
