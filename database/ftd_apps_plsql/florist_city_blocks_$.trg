CREATE OR REPLACE TRIGGER ftd_apps.florist_city_blocks_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_city_blocks
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_city_blocks$ (
         FLORIST_ID,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.BLOCKED_BY_USER_ID,
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_city_blocks$ (
         FLORIST_ID,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.BLOCKED_BY_USER_ID,
         'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.florist_city_blocks$ (
         FLORIST_ID,
          BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.BLOCKED_BY_USER_ID,
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_city_blocks$ (
         FLORIST_ID,
         BLOCK_START_DATE,
         BLOCK_END_DATE,
         BLOCKED_BY_USER_ID,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.BLOCKED_BY_USER_ID,
         'DEL',SYSDATE);

   END IF;

END;
/
