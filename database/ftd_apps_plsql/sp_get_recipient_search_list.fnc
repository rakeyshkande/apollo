CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_RECIPIENT_SEARCH_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_RECIPIENT_SEARCH_LIST
-- Type:    Function
-- Syntax:  SP_GET_RECIPIENT_SEARCH_LIST ()
-- Returns: ref_cursor for
--          RECIPIENT_SEARCH_ID   VARCHAR2(2)
--          DESCRIPTION           VARCHAR2(40)
--
-- Description:   Queries the RECIPIENT_SEARCH_MASTER table and
--                returns a ref cursor for resulting row.
--
--======================================================================

AS
    rec_cursor types.ref_cursor;
BEGIN
    OPEN rec_cursor FOR
        SELECT RECIPIENT_SEARCH_ID as "receipientSearchId",
               DESCRIPTION         as "description"
        FROM RECIPIENT_SEARCH_MASTER;

    RETURN rec_cursor;
END
;
.
/
