CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_ROLE_FUNCTIONS (
    roleId IN VARCHAR2,
    functionId IN VARCHAR2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_ROLE_FUNCTIONS
-- Type:    Procedure
-- Syntax:  SP_UPDATE_ROLE_FUNCTIONS ( <see args above> )
--
-- Description:   Performs an INSERT/UPDATE attempt on ROLE_FUNCTIONS.
--
--==============================================================================
begin

  -- Attempt to insert into ROLE_FUNCTIONS
  INSERT INTO ROLE_FUNCTIONS
      (
        ROLE_ID,
        FUNCTION_ID
		  )
  VALUES
		  (
        roleId,
				functionId
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN

    -- Update an existing row for this role id if found
    UPDATE ROLE_FUNCTIONS
      SET FUNCTION_ID = functionId
      WHERE ROLE_ID = roleId;
end
;
.
/
