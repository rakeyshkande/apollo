CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_TYPES_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_TYPES_LIST
-- Type:    Function
-- Syntax:  SP_GET_TYPES_LIST ()
-- Returns: ref_cursor for
--          TYPE_ID           VARCHAR2(10)
--          TYPE_DESCRIPTION  VARCHAR2(25)
--
--
-- Description:   Queries the PRODUCT_TYPES table and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    product_types_cursor types.ref_cursor;
BEGIN
    OPEN product_types_cursor FOR
        SELECT TYPE_ID          as "typeId",
               TYPE_DESCRIPTION as "typeDescription"
          FROM PRODUCT_TYPES order by TYPE_DESCRIPTION;

    RETURN product_types_cursor;
END
;
.
/
