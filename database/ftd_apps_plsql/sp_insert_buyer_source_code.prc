CREATE OR REPLACE
PROCEDURE ftd_apps.SP_INSERT_BUYER_SOURCE_CODE (
  asnNumberIn in varchar2,
	clientNameIn in varchar2,
    sourceCodeIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSERT_BUYER_SOURCE_CODE
-- Type:    Procedure
-- Syntax:  SP_INSERT_BUYER_SOURCE_CODE ( asnNumberIn in varchar2,
--                                    clientNameIn in varchar2,
--                                      sourceCodeIn in varchar2)
--
-- Description:   Inserts a row to the BUYER_SOURCE_CODE table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 insert into BUYER_SOURCE_CODE
 (ASN_NUMBER,CLIENT_NAME,SOURCE_CODE)
 VALUES(asnNumberIn,clientNameIn,sourceCodeIn);

end
;
.
/
