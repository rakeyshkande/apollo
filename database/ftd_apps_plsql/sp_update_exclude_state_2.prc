CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_EXCLUDE_STATE_2
(productId in varchar2,
 excludeState in varchar2,
 sun in varchar2,
 mon in varchar2,
 tue in varchar2,
 wed in varchar2,
 thu in varchar2,
 fri in varchar2,
 sat in varchar2 )
AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    SP_UPDATE_EXCLUDE_STATE_2
-- Type:    Procedure
-- Syntax:  SP_UPDATE_EXCLUDE_STATE_2 ( productId in varchar2,
--                                      excludeState in varchar2
--                                      sun in varchar2,
--                                      mon in varchar2,
--                                      tue in varchar2,
--                                      wed in varchar2,
--                                      thu in varchar2,
--                                      fri in varchar2,
--                                      sat in varchar2)
--
-- Description:   Attempts to insert a row into PRODUCT_EXCLUDED_STATES.
--                If the product/state combination already exists, updates.
--
--==============================================================================
AS
begin
  -- Attempt to insert into PRODUCT_EXCLUDED_STATES
  INSERT INTO PRODUCT_EXCLUDED_STATES
      ( PRODUCT_ID, EXCLUDED_STATE, SUN, MON, TUE, WED, THU, FRI, SAT )
  VALUES( productId,excludeState,sun,mon,tue,wed,thu,fri,sat );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    -- If a row already exists for this PRODUCT_ID/state then update
  UPDATE PRODUCT_EXCLUDED_STATES
    SET SUN = sun,
        MON = mon,
        TUE = tue,
        WED = wed,
        THU = thu,
        FRI = fri,
        SAT = sat
    WHERE PRODUCT_ID = productId AND EXCLUDED_STATE = excludeState;
end;
.
/
