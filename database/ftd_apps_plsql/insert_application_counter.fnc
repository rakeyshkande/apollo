CREATE OR REPLACE
FUNCTION ftd_apps.INSERT_APPLICATION_COUNTER (
    applicationNameIn        in varchar2
)
RETURN types.ref_cursor
AS
--==============================================================================
--
-- Name:    INSERT_APPLICATION_COUNTER
-- Type:    Procedure
-- Syntax:  INSERT_APPLICATION_COUNTER(  applicationNameIn  )
--
-- Description:   Performs an INSERT into FRP.APPLICATION_COUNTER table.
--
--==============================================================================

   result_cur     types.ref_cursor;
   v_system_message_id number;
   v_out_status varchar2(1);
   v_message varchar2(500);

begin

   frp.misc_pkg.insert_application_counter(
      in_application_name   => applicationNameIn,
      out_status            => v_out_status,
      out_message           => v_message);

    -- Return a dummy cursor so generic DAO can call the proc
    OPEN result_cur FOR
      SELECT v_out_status as "status"
      FROM DUAL;

    RETURN result_cur;

END;
.
/
