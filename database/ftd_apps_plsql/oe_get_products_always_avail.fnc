CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PRODUCTS_ALWAYS_AVAIL
 RETURN types.ref_cursor
--=======================================================================
--
-- Name:    OE_GET_PRODUCTS_ALWAYS_AVAIL
-- Type:    Function
-- Syntax:  OE_GET_PRODUCTS_ALWAYS_AVAIL
-- Returns: ref_cursor for
--          PRODUCT_ID          VARCHAR2(10)
--
-- Description:   Returns all products with availability dates of 12/31/2010
--
--========================================================================
AS
    product_cursor types.ref_cursor;
BEGIN
    OPEN product_cursor FOR
        SELECT PRODUCT_ID
            FROM PRODUCT_MASTER
            WHERE EXCEPTION_END_DATE = TO_DATE('31122010','DDMMYYYY')
            AND EXCEPTION_START_DATE <= sysdate
            AND EXCEPTION_CODE = 'A';
    RETURN product_cursor;
END;
.
/
