create or replace PACKAGE BODY  FTD_APPS.SOURCE_QUERY_PKG
AS


PROCEDURE GET_CALCULATION_BASIS_VAL_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table calculation_basis_val.

Input:
        N/A

Output:
        cursor containing calculation_basis_val info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT calculation_basis,
            status,
            description
       FROM calculation_basis_val
      ORDER BY calculation_basis;

END GET_CALCULATION_BASIS_VAL_LIST;


PROCEDURE GET_REWARD_TYPE_VAL_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table reward_type_val.

Input:
        N/A

Output:
        cursor containing reward_type_val info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT reward_type,
            status,
            description
       FROM reward_type_val
      ORDER BY reward_type;

END GET_REWARD_TYPE_VAL_LIST;


PROCEDURE GET_PROGRAM_TYPE_VAL_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table program_type_val.

Input:
        N/A

Output:
        cursor containing program_type_val info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT program_type,
            status,
            description
       FROM program_type_val
      ORDER BY program_type;

END GET_PROGRAM_TYPE_VAL_LIST;


PROCEDURE GET_PARTNER_MASTER_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table partner_master.

Input:
        N/A

Output:
        cursor containing partner_master info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT partner_name
       FROM partner_master
      ORDER BY partner_name;

END GET_PARTNER_MASTER_LIST;


PROCEDURE GET_PARTNER_PROGRAM_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table partner_program.

Input:
        N/A

Output:
        cursor containing partner_program info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT program_name,
            partner_name,
            program_type,
            email_exclude_flag,
            last_post_date
       FROM partner_program
      ORDER BY partner_name,
               program_name;

END GET_PARTNER_PROGRAM_LIST;



PROCEDURE GET_PROGRAM_REWARD_RANGE
(
 IN_PROGRAM_NAME  IN PROGRAM_REWARD_RANGE.PROGRAM_NAME%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table program_reward_range for a given
   program_name.

Input:
        program_name   VARCHAR2

Output:
        cursor containing program_reward_range info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT program_name,
            min_dollar,
            max_dollar,
            calculation_basis,
            points
       FROM program_reward_range
      WHERE program_name = in_program_name
      ORDER BY min_dollar;

END GET_PROGRAM_REWARD_RANGE;


PROCEDURE GET_PARTNER_PROG_N_PROG_REWARD
(
 IN_PROGRAM_NAME      IN PARTNER_PROGRAM.PROGRAM_NAME%TYPE,
 IN_PROGRAM_TYPE      IN PARTNER_PROGRAM.PROGRAM_TYPE%TYPE,
 IN_CALCULATION_BASIS IN PROGRAM_REWARD.CALCULATION_BASIS%TYPE,
 IN_POINTS            IN PROGRAM_REWARD.POINTS%TYPE,
 IN_REWARD_TYPE       IN PROGRAM_REWARD.REWARD_TYPE%TYPE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table program_reward and partner_program for a
   given program_name, program_type, calculation_basis, points, or reward_type.

Input:
        program_name      VARCHAR2
        program_type      VARCHAR2
        calculation_basis VARCHAR2
        points            NUMBER
        reward_type       VARCHAR2

Output:
        cursor containing program_reward and partner_program info
------------------------------------------------------------------------------*/

v_main_sql VARCHAR2(3000);
v_sql VARCHAR2(1000);
v_params_exist BOOLEAN := FALSE;


BEGIN

   v_main_sql := 'SELECT pp.program_name,
                         pp.partner_name,
                         pp.program_type,
                         pp.email_exclude_flag,
                         pp.last_post_date,
                         pp.updated_by,
                         pp.updated_on,
                         pr.program_name,
                         pr.calculation_basis,
                         pr.points,
                         pr.maximum_points,
                         pr.reward_type,
                         pr.customer_info,
                         pr.reward_name,
                         pr.participant_id_length_check,
                         pr.participant_email_check,
                         pr.bonus_calculation_basis,
                         pr.bonus_points,
                         pr.bonus_separate_data,
                         pp.program_long_name,
                         pp.membership_data_required
                    FROM partner_program pp,
                         program_reward pr
                   WHERE pp.program_name = pr.program_name (+)';

   IF in_program_name IS NOT NULL THEN
      v_params_exist := TRUE;
      v_sql := 'pp.program_name = ''' || in_program_name || '''';
   END IF;

   IF in_program_type IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' OR pp.program_type = ''' || in_program_type || '''';
      ELSE
        v_sql := 'pp.program_type = ''' || in_program_type || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_calculation_basis IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' OR pr.calculation_basis = ''' || in_calculation_basis || '''';
      ELSE
        v_sql := 'pr.calculation_basis = ''' || in_calculation_basis || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_points IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' OR pr.points = ''' || in_points || '''';
      ELSE
        v_sql := 'pr.points = ''' || in_points || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_reward_type IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' OR pr.reward_type = ''' || in_reward_type || '''';
      ELSE
        v_sql := 'pr.reward_type = ''' || in_reward_type || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF v_params_exist = TRUE THEN
      v_main_sql := v_main_sql || ' AND (' || v_sql || ')';
   END IF;

   OPEN out_cursor FOR v_main_sql;

END GET_PARTNER_PROG_N_PROG_REWARD;


PROCEDURE GET_SOURCE_BY_PARTNER_PROGRAM
(
 IN_SOURCE_CODE              IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_UPDATED_BY               IN SOURCE_MASTER.UPDATED_BY%TYPE,
 IN_REQUESTED_BY             IN SOURCE_MASTER.REQUESTED_BY%TYPE,
 IN_COMPANY_ID               IN SOURCE_MASTER.COMPANY_ID%TYPE,
 IN_SOURCE_TYPE              IN SOURCE_MASTER.SOURCE_TYPE%TYPE,
 IN_PRICE_HEADER_ID          IN SOURCE_MASTER.PRICE_HEADER_ID%TYPE,
 IN_SNH_ID                   IN SOURCE_MASTER.SNH_ID%TYPE,
 IN_END_DATE                 IN SOURCE_MASTER.END_DATE%TYPE,
 IN_PROGRAM_NAME             IN SOURCE_PROGRAM_REF.PROGRAM_NAME%TYPE,
 IN_PROGRAM_WEBSITE_URL      IN SOURCE_MASTER.PROGRAM_WEBSITE_URL%TYPE,
 IN_PROMOTION_CODE           IN SOURCE_MASTER.PROMOTION_CODE%TYPE,
 IN_DEFAULT_SOURCE_CODE_FLAG IN SOURCE_MASTER.DEFAULT_SOURCE_CODE_FLAG%TYPE,
 IN_JCPENNEY_FLAG            IN SOURCE_MASTER.JCPENNEY_FLAG%TYPE,
 IN_OVER_21_FLAG             IN PRODUCT_ATTR_RESTR.PRODUCT_ATTR_RESTR_NAME%TYPE,
 IN_UPDATED_ON_START         IN SOURCE_MASTER.UPDATED_ON%TYPE,
 IN_UPDATED_ON_END           IN SOURCE_MASTER.UPDATED_ON%TYPE,
 IN_SORT_BY                  IN VARCHAR2,
 IN_START_POSITION           IN NUMBER,
 IN_END_POSITION             IN NUMBER,
 IN_IOTW_FLAG                IN SOURCE_MASTER.IOTW_FLAG%TYPE,
 OUT_MAX_ROWS               OUT NUMBER,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the distinct records in table source_master for the
   optional input input parameters.

Input:
        source_code               VARCHAR2
        updated_by                VARCHAR2
        requested_by              VARCHAR2
        company_id                VARCHAR2
        source_type               VARCHAR2
        price_header_id           VARCHAR2
        snh_id                    VARCHAR2
        end_date                  DATE
        program_name              VARCHAR2
        program_website_url       VARCHAR2
        promotion_code            VARCHAR2
        default_source_code_flag  CHAR
        jcpenney_flag             CHAR
        over_21_flag              CHAR
        updated_on_start          DATE
        updated_on_end            DATE
        in_sort_by                VARCHAR2
        in_start_position         NUMBER
        in_end_position           NUMBER

Output:
        out_max_rows              NUMBER
        cursor containing source_master info
------------------------------------------------------------------------------*/

v_main_sql CLOB;
v_rowcount_sql VARCHAR2(3000);
v_sql VARCHAR2(1000);
v_params_exist BOOLEAN := FALSE;


BEGIN

   v_main_sql := 'SELECT DISTINCT s.source_code,
                     s.description,
                     s.snh_id,
                     s.price_header_id,
                     s.send_to_scrub,
                     s.fraud_flag,
                     s.enable_lp_processing,
                     s.emergency_text_flag,
                     s.requires_delivery_confirmation,
                     s.created_on,
                     s.created_by,
                     s.updated_on,
                     s.updated_by,
                     s.source_type,
                     s.department_code,
                     TO_CHAR(s.start_date, ''MM/DD/YYYY'') start_date,
                     TO_CHAR(s.end_date, ''MM/DD/YYYY'') end_date,
                     s.payment_method_id,
                     s.list_code_flag,
                     s.default_source_code_flag,
                     s.billing_info_prompt,
                     s.billing_info_logic,
                     s.marketing_group,
                     s.external_call_center_flag,
                     s.highlight_description_flag,
                     s.discount_allowed_flag,
                     s.bin_number_check_flag,
                     s.order_source,
                     s.yellow_pages_code,
                     s.jcpenney_flag,
                     s.company_id,
                     s.promotion_code,
                     s.bonus_promotion_code,
                     s.requested_by,
                     s.program_website_url,
                     s.comment_text,
                     s.related_source_code,
                     pb.partner_bank_id,
                     pb.pst_code partner_bank_pst_code,
                     s.webloyalty_flag,
                     s.iotw_flag,
                     s.invoice_password,
                     decode(bi.info_description, ''CERTIFICATE#'', ''N'', null, ''N'', ''Y'') billing_info_flag,
                     decode(bi.info_description, ''CERTIFICATE#'', ''Y'', ''N'') gift_certificate_flag,
                     s.mp_redemption_rate_id,
                     s.add_on_free_id,
                     s.display_service_fee_code,
                     s.display_shipping_fee_code,
                     s.primary_backup_rwd_flag,
          		     s.apply_surcharge_code,
                     s.surcharge_description,
                     s.display_surcharge_flag,
                     s.surcharge_amount ,
                     s.allow_free_shipping_flag,
					 s.same_day_upcharge,
					 s.display_same_day_upcharge,
					 s.morning_delivery_flag,
					 s.morning_delivery_free_shipping,
					 s.delivery_fee_id,
					 s.auto_promotion_engine,
					 s.ape_product_catalog,
                     s.oscar_selection_enabled_flag,
                     s.oscar_scenario_group_id,
			         s.funeral_cemetery_loc_chk,
			         s.hospital_loc_chck,
			         s.funeral_cemetery_lead_time_chk,
			         s.funeral_cemetery_lead_time,
			         s.bo_hrs_mon_fri_start,
			         s.bo_hrs_mon_fri_end,
			         s.bo_hrs_sat_start,
			         s.bo_hrs_sat_end,
			         s.bo_hrs_sun_start,
			         s.bo_hrs_sun_end,
              		 slm.legacy_id,
               		 s.calculate_tax_flag,
               		s.custom_shipping_carrier,
					s.MERCH_AMT_FULL_REFUND_FLAG,
					s.same_day_upcharge_fs
					
                    FROM source_master s,
                         source_legacy_id_mapping slm,
                         source_program_ref spr,
                         partner_bank pb,
                         billing_info bi
                   WHERE s.source_code = spr.source_code (+)
                   AND   s.partner_bank_id = pb.partner_bank_id (+)
                   AND s.source_code = slm.source_code (+)
                   and s.source_code = bi.source_code_id (+)
                   and bi.billing_info_sequence (+) = ''1'' ';

   v_rowcount_sql := 'SELECT COUNT(DISTINCT s.source_code)
                        FROM source_master s,
                             source_program_ref spr
                       WHERE s.source_code = spr.source_code (+)';


   IF in_source_code IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND LOWER(s.source_code) = LOWER(''' || in_source_code || ''')';
      ELSE
        v_sql := 'LOWER(s.source_code) = LOWER(''' || in_source_code || ''') ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_updated_by IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.updated_by = ''' || in_updated_by || '''';
      ELSE
        v_sql := 's.updated_by = ''' || in_updated_by || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_requested_by IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.requested_by = ''' || in_requested_by || '''';
      ELSE
        v_sql := 's.requested_by = ''' || in_requested_by || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_company_id IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.company_id = ''' || in_company_id || '''';
      ELSE
        v_sql := 's.company_id = ''' || in_company_id || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_source_type IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND LOWER(s.source_type) LIKE LOWER(''%' || in_source_type || '%'')';
      ELSE
        v_sql := 'LOWER(s.source_type) LIKE LOWER(''%' || in_source_type || '%'')';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_price_header_id IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.price_header_id = ''' || in_price_header_id || '''';
      ELSE
        v_sql := 's.price_header_id = ''' || in_price_header_id || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_snh_id IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.snh_id = ''' || in_snh_id || '''';
      ELSE
        v_sql := 's.snh_id = ''' || in_snh_id || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_end_date IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND TRUNC(s.end_date) = TRUNC(TO_DATE(''' || in_end_date || '''))';
      ELSE
        v_sql := 'TRUNC(s.end_date) = TRUNC(TO_DATE(''' || in_end_date || '''))';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_program_name IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND LOWER(spr.program_name) LIKE LOWER(''%' || in_program_name || '%'')';
      ELSE
        v_sql := 'LOWER(spr.program_name) LIKE LOWER(''%' || in_program_name || '%'')';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_program_website_url IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND LOWER(s.program_website_url) LIKE LOWER(''%' || in_program_website_url || '%'')';
      ELSE
        v_sql := 'LOWER(s.program_website_url) LIKE LOWER(''%' || in_program_website_url || '%'')';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_promotion_code IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND LOWER(s.promotion_code) LIKE LOWER(''%' || in_promotion_code || '%'')';
      ELSE
        v_sql := 'LOWER(s.promotion_code) LIKE LOWER(''%' || in_promotion_code || '%'')';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_default_source_code_flag IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.default_source_code_flag = ''' || in_default_source_code_flag || '''';
      ELSE
        v_sql := 's.default_source_code_flag = ''' || in_default_source_code_flag || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_jcpenney_flag IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.jcpenney_flag = ''' || in_jcpenney_flag || '''';
      ELSE
        v_sql := 's.jcpenney_flag = ''' || in_jcpenney_flag || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_over_21_flag IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.source_code NOT IN (SELECT parse.source_code
                            FROM ftd_apps.product_attr_restr_source_excl parse
                            JOIN ftd_apps.product_attr_restr par
                            ON par.product_attr_restr_id = parse.product_attr_restr_id
        		    AND par.PRODUCT_ATTR_RESTR_NAME = ''OVER_21''
        		    AND par.PRODUCT_ATTR_RESTR_VALUE = ''' || in_over_21_flag || ''')';
      ELSE
        v_sql := ' s.source_code NOT IN (SELECT parse.source_code
                            FROM ftd_apps.product_attr_restr_source_excl parse
                            JOIN ftd_apps.product_attr_restr par
                            ON par.product_attr_restr_id = parse.product_attr_restr_id
        		    AND par.PRODUCT_ATTR_RESTR_NAME = ''OVER_21''
        		    AND par.PRODUCT_ATTR_RESTR_VALUE = ''' || in_over_21_flag || ''')';
      END IF;

      v_params_exist := TRUE;

   END IF;

   IF in_iotw_flag IS NOT NULL THEN

      IF v_params_exist = TRUE THEN
        v_sql := v_sql || ' AND s.iotw_flag = ''' || in_iotw_flag || '''';
      ELSE
        v_sql := 's.iotw_flag = ''' || in_iotw_flag || ''' ';
      END IF;

      v_params_exist := TRUE;

   END IF;
   IF (in_updated_on_start IS NOT NULL) OR (in_updated_on_end IS NOT NULL) THEN

      IF (in_updated_on_start IS NOT NULL) AND (in_updated_on_end IS NULL) THEN

        IF v_params_exist = TRUE THEN
          v_sql := v_sql || ' AND (TRUNC(s.updated_on) >= TRUNC(TO_DATE(''' || in_updated_on_start || ''')))';
        ELSE
          v_sql := '(TRUNC(s.updated_on) >= TRUNC(TO_DATE(''' || in_updated_on_start || ''')))';
        END IF;

      ELSIF (in_updated_on_start IS NULL) AND (in_updated_on_end IS NOT NULL) THEN

        IF v_params_exist = TRUE THEN
          v_sql := v_sql || ' AND (TRUNC(s.updated_on) <= TRUNC(TO_DATE(''' || in_updated_on_end || ''')))';
        ELSE
          v_sql := '(TRUNC(s.updated_on) <= TRUNC(TO_DATE(''' || in_updated_on_end || ''')))';
        END IF;

      ELSE --both are not null

        IF v_params_exist = TRUE THEN
          v_sql := v_sql || ' AND (TRUNC(s.updated_on) BETWEEN TRUNC(TO_DATE(''' || in_updated_on_start || ''')) AND TRUNC(TO_DATE(''' || in_updated_on_end || ''')))';
        ELSE
          v_sql := '(TRUNC(s.updated_on) BETWEEN TRUNC(TO_DATE(''' || in_updated_on_start || ''')) AND TRUNC(TO_DATE(''' || in_updated_on_end || ''')))';
        END IF;

      END IF;

      v_params_exist := TRUE;

   END IF;

   --concatinate the sql string with any parameters that may have been passed in
   IF v_params_exist = TRUE THEN
      v_main_sql := v_main_sql || ' AND ' || v_sql;
      v_rowcount_sql := v_rowcount_sql || ' AND ' || v_sql;
   END IF;

   IF in_sort_by IS NOT NULL THEN
     v_main_sql := v_main_sql || ' ORDER BY s.' || in_sort_by;
   END IF;

   --the follwing code will only return the rows asked for
   IF in_start_position IS NOT NULL AND in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT *
                     FROM (SELECT rslt.*,
                                  ROWNUM rnum
                             FROM (' || v_main_sql || ') rslt
                            WHERE ROWNUM <= ' || in_end_position || ')
                    WHERE rnum >= ' || in_start_position;
   ELSIF in_start_position IS NOT NULL THEN
     v_main_sql := 'SELECT *
                     FROM (SELECT rslt.*,
                                 ROWNUM rnum
                             FROM (' || v_main_sql || ') rslt)
                    WHERE rnum >= ' || in_start_position;
   ELSIF in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ') rslt
                            WHERE ROWNUM <= ' || in_end_position;
   END IF;
   EXECUTE IMMEDIATE v_rowcount_sql INTO out_max_rows;

   OPEN out_cursor FOR v_main_sql;
--open out_cursor for select v_main_sql from dual;

END GET_SOURCE_BY_PARTNER_PROGRAM;


PROCEDURE GET_SOURCE_TYPE_VAL_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table source_type_val.

Input:
        N/A

Output:
        cursor containing source_type_val info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT source_type,
            status
       FROM source_type_val
       where source_type not in ('BATESVILLE',
                                  'BV FRIENDS AND FAMILY',
                                  'BV GIFTS','LEGACY',
                                  'LEGACY',
                                  'LEGACY.COM',
                                  'SCI',
                                  'SCI CEMETERY',
                                  'SCI DROP SHIP ONLY',
                                  'SCI FRIENDS & FAMILY',
                                  'SCI JEWISH GIFTS',
                                  'SCI NO DIG MEM BRANDING')
      ORDER BY source_type;

END GET_SOURCE_TYPE_VAL_LIST;


PROCEDURE GET_SRC_MAST_N_SRC_MAST_RES
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in tables source_master_reserve excluding
   source codes in the source master.

Input:
        N/A

Output:
        cursor containing source_master and source_master_reserve info
------------------------------------------------------------------------------*/
BEGIN
OPEN out_cursor FOR
        SELECT  smr.source_code,
                null,
                null,
                smr.description,
                smr.source_type,
                ' ',
                null
        FROM    ftd_apps.source_master_reserve smr
        WHERE   smr.status = 'Inactive'
        AND     length(smr.source_code) >= 5
        ORDER BY smr.source_code;

END GET_SRC_MAST_N_SRC_MAST_RES;


FUNCTION ISNUMBER
(
  IN_NUMBER IN VARCHAR2
)
RETURN NUMBER
IS
/*------------------------------------------------------------------------------
Description:
   This function determines if the string passed in is a number.

Input:
        N/A

Output:
        returns the number value
------------------------------------------------------------------------------*/
v_number NUMBER;

BEGIN

        v_number := TO_NUMBER(in_number);
        RETURN in_number;

        EXCEPTION WHEN OTHERS THEN RETURN null;

END ISNUMBER;


PROCEDURE GET_SRC_MAST_RES_SOURCE_CODES
(
 IN_SOURCE_CODE_BEGIN   IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_SOURCE_CODE_END     IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 OUT_CURSOR            OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in tables source_master_reserve whose source code
    is between the two source codes passed in or for just one source code
    passed in.

Input:
        source_code_begin   VARCHAR2
        source_code_end     VARCHAR2

Output:
        cursor containing source_master and source_master_reserve info
------------------------------------------------------------------------------*/
v_sql VARCHAR2(1000);

BEGIN

   IF in_source_code_end IS NULL THEN
      OPEN out_cursor FOR
         SELECT source_code,
                description,
                source_type
           FROM source_master_reserve
          WHERE status = 'Active'
            AND source_code = in_source_code_begin
          ORDER BY source_code;
   ELSE
      v_sql := 'SELECT source_code,
                    description,
                    source_type
               FROM source_master_reserve
              WHERE status = ''Active''
                AND SOURCE_QUERY_PKG.ISNUMBER(source_code) BETWEEN TO_NUMBER(''' || in_source_code_begin || ''')
                                              AND TO_NUMBER(''' || in_source_code_end || ''')
              ORDER BY source_code';

     OPEN out_cursor FOR v_sql;
   END IF;

END GET_SRC_MAST_RES_SOURCE_CODES;



PROCEDURE GET_SRC_MAST_RES_SRC_MAST
(
 IN_SOURCE_CODE_BEGIN   IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_SOURCE_CODE_END     IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in tables source_master and source_master_reserve
    whose source code is between the two source codes passed in or for just
    one source code passed in.

Input:
        source_code_begin   VARCHAR2
        source_code_end     VARCHAR2

Output:
        cursor containing source_master and source_master_reserve info
------------------------------------------------------------------------------*/
v_sql VARCHAR2(1000);

BEGIN

  IF in_source_code_end IS NULL THEN
    OPEN out_cursor FOR
      SELECT smr.source_code,
             smr.description,
             smr.source_type,
             sm.description,
             sm.source_type,
             sm.order_source,
             sm.program_website_url
        FROM source_master_reserve smr,
             source_master sm
       WHERE smr.source_code = in_source_code_begin
         AND sm.source_code = smr.source_code
       ORDER BY smr.source_code;
  ELSE
    v_sql := 'SELECT smr.source_code,
                     smr.description,
                     smr.source_type,
                     sm.description,
                     sm.source_type,
                     sm.order_source,
                     sm.program_website_url
                FROM source_master_reserve smr,
                     source_master sm
               WHERE smr.source_code = sm.source_code
                 AND SOURCE_QUERY_PKG.ISNUMBER(smr.source_code) BETWEEN TO_NUMBER(''' || in_source_code_begin || ''')
                                                   AND TO_NUMBER(''' || in_source_code_end || ''')
                ORDER BY smr.source_code';

    OPEN out_cursor FOR v_sql;
  END IF;

END GET_SRC_MAST_RES_SRC_MAST;


PROCEDURE GET_SRC_PROG_REF_N_PART_PROG
(
 IN_SOURCE_CODE   IN SOURCE_PROGRAM_REF.SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table source_program_ref and its related
   records from table partner program for a given source_code.

Input:
        source_code  VARCHAR2

Output:
        cursor containing source_program_ref info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT spr.source_program_ref_id,
            spr.source_code,
            spr.program_name,
            TO_CHAR(spr.start_date,'MM/DD/YYYY') start_date,
            spr.created_on,
            spr.created_by,
            spr.updated_on,
            spr.updated_by,
            pp.partner_name,
            pp.program_type,
            pp.email_exclude_flag,
            pp.last_post_date,
            pm.preferred_partner_flag,
            pm.source_primary_florist_flag
       FROM source_program_ref spr,
            partner_program pp,
            partner_master pm
      WHERE spr.source_code = in_source_code
        AND spr.program_name = pp.program_name
        AND pp.partner_name = pm.partner_name
      ORDER BY spr.start_date;

END GET_SRC_PROG_REF_N_PART_PROG;


PROCEDURE GET_SOURCE_PROGRAM_REF
(
 IN_SOURCE_PROGRAM_REF_ID  IN SOURCE_PROGRAM_REF.SOURCE_PROGRAM_REF_ID%TYPE,
 OUT_CURSOR               OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table source_program_ref for a given source_code.

Input:
        source_code  VARCHAR2

Output:
        cursor containing source_program_ref info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT source_program_ref_id,
            source_code,
            program_name,
            TO_CHAR(start_date,'MM/DD/YYYY') start_date,
            created_on,
            created_by,
            updated_on,
            updated_by
       FROM source_program_ref
      WHERE source_program_ref_id = in_source_program_ref_id
      ORDER BY start_date;

END GET_SOURCE_PROGRAM_REF;


PROCEDURE GET_PARTNER_PROGRAM
(
 IN_PARTNER_NAME   IN PARTNER_PROGRAM.PARTNER_NAME%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table partner_program for a given partner_name.

Input:
        partner_name  VARCHAR2

Output:
        cursor containing partner_program info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT program_name,
            partner_name,
            program_long_name,
            program_type,
            email_exclude_flag,
            last_post_date
       FROM partner_program
      WHERE partner_name = in_partner_name
      ORDER BY program_name;

END GET_PARTNER_PROGRAM;


PROCEDURE GET_SOURCE_BY_UPDATING_USER
(
 IN_UPDATED_BY               IN SOURCE_MASTER.UPDATED_BY%TYPE,
 IN_UPDATED_ON_START         IN SOURCE_MASTER.UPDATED_ON%TYPE,
 IN_UPDATED_ON_END           IN SOURCE_MASTER.UPDATED_ON%TYPE,
 IN_SORT_BY                  IN VARCHAR2,
 IN_START_POSITION           IN NUMBER,
 IN_END_POSITION             IN NUMBER,
 OUT_MAX_ROWS               OUT NUMBER,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the distinct records in table source_master for the
   optional input input parameters.

Input:
        updated_by                VARCHAR2
        updated_on_start          DATE
        updated_on_end            DATE
        in_sort_by                VARCHAR2
        in_start_position         NUMBER
        in_end_position           NUMBER

Output:
        out_max_rows              NUMBER
        cursor containing source_master info
------------------------------------------------------------------------------*/

v_main_sql VARCHAR2(3000);
v_rowcount_sql VARCHAR2(3000);
v_sql VARCHAR2(1000);
v_params_exist BOOLEAN := FALSE;


BEGIN

   v_main_sql := 'SELECT DISTINCT source_code,
                         description,
                         snh_id,
                         price_header_id,
                         send_to_scrub,
                         fraud_flag,
                         enable_lp_processing,
                         emergency_text_flag,
                         requires_delivery_confirmation,
                         created_on,
                         created_by,
                         updated_on,
                         updated_by,
                         source_type,
                         department_code,
                         TO_CHAR(start_date, ''MM/DD/YYYY'') start_date,
                         TO_CHAR(end_date, ''MM/DD/YYYY'') end_date,
                         payment_method_id,
                         list_code_flag,
                         default_source_code_flag,
                         billing_info_prompt,
                         billing_info_logic,
                         marketing_group,
                         external_call_center_flag,
                         highlight_description_flag,
                         discount_allowed_flag,
                         bin_number_check_flag,
                         order_source,
                         yellow_pages_code,
                         jcpenney_flag,
                         company_id,
                         promotion_code,
                         bonus_promotion_code,
                         requested_by,
                         program_website_url,
                         comment_text,
                         related_source_code,
                         webloyalty_flag,
                         iotw_flag,
                         invoice_password,
                         add_on_free_id,
                         primary_backup_rwd_flag

                    FROM source_master
                   WHERE updated_by = ''' || in_updated_by || '''';

   v_rowcount_sql := 'SELECT COUNT(DISTINCT source_code)
                        FROM source_master
                       WHERE updated_by = ''' || in_updated_by || '''';


   IF (in_updated_on_start IS NOT NULL) OR (in_updated_on_end IS NOT NULL) THEN

      IF (in_updated_on_start IS NOT NULL) AND (in_updated_on_end IS NULL) THEN

        v_sql := v_sql || '(TRUNC(updated_on) >= TRUNC(TO_DATE(''' || in_updated_on_start || ''')))';

      ELSIF (in_updated_on_start IS NULL) AND (in_updated_on_end IS NOT NULL) THEN

        v_sql := v_sql || '(TRUNC(updated_on) <= TRUNC(TO_DATE(''' || in_updated_on_end || ''')))';

      ELSE --both are not null

        v_sql := v_sql || '(TRUNC(updated_on) BETWEEN TRUNC(TO_DATE(''' || in_updated_on_start || ''')) AND TRUNC(TO_DATE(''' || in_updated_on_end || ''')))';

      END IF;

      v_params_exist := TRUE;

   END IF;

   --the follwing code will only return the rows asked for
   IF in_start_position IS NOT NULL AND in_end_position IS NOT NULL THEN

    v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ' ORDER BY updated_on DESC ) rslt
                     WHERE ROWNUM <= ' || in_end_position;

     IF v_params_exist = TRUE THEN
        v_main_sql := v_main_sql || ' AND ' || v_sql;
     END IF;

     IF in_sort_by IS NOT NULL THEN
       v_main_sql := v_main_sql || ' ORDER BY ' || in_sort_by;
     END IF;

     v_main_sql := 'SELECT *
                      FROM (' || v_main_sql || ')
                     WHERE rnum >= ' || in_start_position;

   ELSIF in_start_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ' ORDER BY updated_on DESC ) rslt';

     IF v_params_exist = TRUE THEN
        v_main_sql := v_main_sql || ' AND ' || v_sql;
     END IF;

     IF in_sort_by IS NOT NULL THEN
       v_main_sql := v_main_sql || ' ORDER BY ' || in_sort_by;
     END IF;

     v_main_sql := 'SELECT *
                      FROM (' || v_main_sql || ')
                     WHERE rnum >= ' || in_start_position;


   ELSIF in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ' ORDER BY updated_on DESC ) rslt
                     WHERE ROWNUM <= ' || in_end_position;

     IF v_params_exist = TRUE THEN
        v_main_sql := v_main_sql || ' AND ' || v_sql;
     END IF;

     IF in_sort_by IS NOT NULL THEN
       v_main_sql := v_main_sql || ' ORDER BY ' || in_sort_by;
     END IF;

     v_main_sql := 'SELECT *
                      FROM (' || v_main_sql || ')';


   ELSE
     --concatinate the sql string with any parameters that may have been passed in
     IF v_params_exist = TRUE THEN
        v_main_sql := v_main_sql || ' AND ' || v_sql;
     END IF;

     IF in_sort_by IS NOT NULL THEN
       v_main_sql := v_main_sql || ' ORDER BY updated_on DESC, ' || in_sort_by;
     END IF;

   END IF;

   --concatinate the sql string with any parameters that may have been passed in
   IF v_params_exist = TRUE THEN
      v_rowcount_sql := v_rowcount_sql || ' AND ' || v_sql;
   END IF;

   EXECUTE IMMEDIATE v_rowcount_sql INTO out_max_rows;

   OPEN out_cursor FOR v_main_sql;
--open out_cursor for select v_main_sql from dual;

END GET_SOURCE_BY_UPDATING_USER;


FUNCTION GET_SRC_PRG_REF_MAX_START_DATE
(
 IN_SOURCE_CODE  IN SOURCE_PROGRAM_REF.SOURCE_CODE%TYPE,
 IN_DATE         IN DATE
)
RETURN DATE
IS
/*------------------------------------------------------------------------------
Description:
   Returns the max start date less than the date passed in for the
   source code passed in.

Input:
        source_code  VARCHAR2
        in_date      DATE

Output:
        start_date      DATE
------------------------------------------------------------------------------*/
v_start_date source_program_ref.start_date%TYPE;

BEGIN

   BEGIN
     SELECT max(start_date)
       INTO v_start_date
       FROM source_program_ref
      WHERE source_code = in_source_code
        AND start_date <= in_date;

     EXCEPTION WHEN NO_DATA_FOUND THEN v_start_date := NULL;
   END;

   RETURN v_start_date;

END GET_SRC_PRG_REF_MAX_START_DATE;


PROCEDURE GET_SRC_MST_SRC_PRGREF_PRG_RWD
(
 IN_SOURCE_CODE   IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_DATE          IN DATE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records from tables source_master, source_program_ref,
   and partner_reward for the given source_code and date.

Input:
        source_code  VARCHAR2
        in_date      DATE

Output:
        cursor containing partner_program info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT sm.promotion_code,
            sm.bonus_promotion_code,
            sm.partner_bank_id,
            pp.program_name,
            pr.calculation_basis,
            pr.points,
            pr.reward_type,
            pr.reward_name,
            pr.bonus_calculation_basis,
            pr.bonus_points,
            pr.bonus_separate_data,
            pr.maximum_points,
            pb.pst_code,
            pb.balance,
            pp.partner_name,
            pp.program_type
       FROM source_master sm,
            source_program_ref spr,
            program_reward pr,
            partner_bank pb,
            partner_program pp
      WHERE sm.source_code = in_source_code
        AND spr.source_code = sm.source_code
        AND spr.start_date  = get_src_prg_ref_max_start_date(in_source_code, in_date)
        AND spr.program_name = pp.program_name
        AND pb.partner_bank_id (+) = sm.partner_bank_id
        AND pr.program_name (+) = pp.program_name;

END GET_SRC_MST_SRC_PRGREF_PRG_RWD;


FUNCTION GET_SOURCE_REWARD_TYPE
(
IN_SOURCE_CODE                  IN SOURCE_MASTER.SOURCE_CODE%TYPE
)
RETURN VARCHAR2
AS
/*------------------------------------------------------------------------------
Description:
        Returns M - miles if the source code is associated with a program
        reward or the pricing discount type (D - dollar or P - percent)

Input:
        source_code  VARCHAR2

Output:
        M/D/Points/Miles/Cash Back/TotSavings
------------------------------------------------------------------------------*/
CURSOR out_cur IS
        SELECT  decode (pr.program_name, null, phd.discount_type, pr.reward_type)
        FROM    source_master sm
        JOIN    price_header_details phd
        ON      sm.price_header_id = phd.price_header_id
        LEFT OUTER JOIN
        (
                select  spr.source_code,
                        spr.program_name
                from    ftd_apps.source_program_ref spr
                where   spr.source_code = in_source_code
                and     spr.start_date =
                        (
                                select  max(b.start_date)
                                from    source_program_ref b
                                where   b.source_code = spr.source_code
                                and     b.start_date <= sysdate
                        )
        ) spr
        ON     sm.source_code = spr.source_code
        LEFT OUTER JOIN program_reward pr
        ON     pr.program_name = spr.program_name
        WHERE  sm.source_code = in_source_code;

v_return        varchar2(100);

BEGIN

OPEN out_cur;
FETCH out_cur INTO v_return;
CLOSE out_cur;

RETURN v_return;

END GET_SOURCE_REWARD_TYPE;


PROCEDURE GET_PARTNER_PROG_BY_PROG_TYPE
(
 IN_PROGRAM_TYPE   IN PARTNER_PROGRAM.PROGRAM_TYPE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table partner_program for a given program_type.

Input:
        program_type  VARCHAR2

Output:
        cursor containing partner_program info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT program_name,
            program_long_name,
            program_type,
            partner_name,
            email_exclude_flag,
            last_post_date
       FROM partner_program
      WHERE program_type = in_program_type
      ORDER BY program_name;

END GET_PARTNER_PROG_BY_PROG_TYPE;


PROCEDURE GET_PARTNER_BANK_BY_SOURCE
(
IN_SOURCE_CODE          IN SOURCE_MASTER.SOURCE_CODE%TYPE,
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        Returns the partner bank information for the partner associated
        to the given source code

Input:
        source_code             varchar2

Output:
        cursor containing partner_bank info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
        SELECT  pb.partner_bank_id,
                pb.pst_code
        FROM    partner_bank pb
        WHERE   EXISTS
        (
                SELECT  1
                FROM    source_program_ref s
                JOIN    partner_program p
                ON      s.program_name=p.program_name
                WHERE   pb.partner_name = p.partner_name
                AND     s.source_code = in_source_code
        )
        ORDER BY pb.pst_code;

END GET_PARTNER_BANK_BY_SOURCE;

PROCEDURE GET_RDMPTN_RATE_BY_SOURCE
(
IN_SOURCE_CODE   	IN SOURCE_MASTER.SOURCE_CODE%TYPE,
OUT_CURSOR      	OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        Returns the redemption rate associated with the source code.

Input:
        source_code             varchar2

Output:
        cursor containing redemption rate info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
        SELECT  mp_redemption_rate_amt
        FROM    miles_points_redemption_rate rr
        JOIN	source_master sm
        ON	sm.mp_redemption_rate_id = rr.mp_redemption_rate_id
        WHERE   sm.source_code = IN_SOURCE_CODE;

END GET_RDMPTN_RATE_BY_SOURCE;

PROCEDURE GET_PREFERRED_PARTNER
(
OUT_PARTNER_CURSOR      OUT TYPES.REF_CURSOR,
OUT_SOURCE_CURSOR      	OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        Returns the partners with preferred_partner_flag of 'Y'.

Input:
        source_code             varchar2

Output:
        cursor containing redemption rate info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_partner_cursor FOR
        SELECT  partner_name,
                preferred_processing_resource,
                reply_email_address,
                default_phone_source_code,
                default_web_source_code,
                preferred_partner_flag,
                florist_resend_allowed_flag,
                display_name
        FROM    partner_master
        WHERE   preferred_partner_flag = 'Y';

OPEN out_source_cursor FOR
        SELECT  distinct spr.source_code, pm.partner_name, pm.preferred_processing_resource, reply_email_address, pm.preferred_partner_flag, pm.florist_resend_allowed_flag
        FROM    source_program_ref spr
        JOIN	partner_program pp
        ON	pp.program_name = spr.program_name
        JOIN	partner_master pm
        ON	pm.partner_name = pp.partner_name
        WHERE   preferred_partner_flag = 'Y';

END GET_PREFERRED_PARTNER;

PROCEDURE GET_PARTNER_BIN_MAP_BY_SOURCE
(
 IN_SOURCE_CODE IN VARCHAR2,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active records in table ftd_apps.source_partner_bin_mapping for a
   specific source code.

Input:
        N/A

Output:
        cursor containing the active partners info
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CURSOR FOR
     SELECT UNIQUE PARTNER_NAME
       FROM SOURCE_PARTNER_BIN_MAPPING
        WHERE SOURCE_CODE = IN_SOURCE_CODE
        AND BIN_PROCESSING_ACTIVE_FLAG = 'Y';

END GET_PARTNER_BIN_MAP_BY_SOURCE;

PROCEDURE GET_PARTNER_MASTER_BY_NAME
(
 IN_PARTNER_NAME        IN  PARTNER_MASTER.PARTNER_NAME%TYPE,
 OUT_cURSOR             OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        select partner_name,
            file_sequence_prefix,
            preferred_processing_resource,
            preferred_partner_flag,
            reply_email_address,
            default_phone_source_code,
            default_web_source_code,
            bin_processing_flag,
            sm1.description default_phone_sc_description,
            sm2.description default_web_sc_description
        from partner_master pm
        left outer join source_master sm1
        on sm1.source_code = pm.default_phone_source_code
        left outer join source_master sm2
        on sm2.source_code = pm.default_web_source_code
        where pm.partner_name = in_partner_name
;

END GET_PARTNER_MASTER_BY_NAME;

PROCEDURE GET_ALL_PRODUCT_ATTRIBUTES
(
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        select product_attr_restr_id,
            product_attr_restr_name,
            product_attr_restr_oper,
            product_attr_restr_value,
            product_attr_restr_desc
        from product_attr_restr
        order by display_order_seq_num;

END GET_ALL_PRODUCT_ATTRIBUTES;

PROCEDURE GET_PRODUCT_ATTR_SOURCE_EXCL
(
 IN_SOURCE_CODE         IN  SOURCE_MASTER.SOURCE_CODE%TYPE,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        select pse.PRODUCT_ATTR_RESTR_ID,
               p.PRODUCT_ATTR_RESTR_NAME,
               p.PRODUCT_ATTR_RESTR_OPER,
               p.PRODUCT_ATTR_RESTR_VALUE
        from product_attr_restr_source_excl pse
        join product_attr_restr p
           on pse.product_attr_restr_id = p.product_attr_restr_id
        where pse.source_code = in_source_code
        order by display_order_seq_num;

END GET_PRODUCT_ATTR_SOURCE_EXCL;

FUNCTION SOURCE_CODE_EXISTS
(
IN_SOURCE_CODE                  IN SOURCE_MASTER.SOURCE_CODE%TYPE
)
RETURN VARCHAR2
AS
/*------------------------------------------------------------------------------
Description:
        Returns Y if source code exists.

Input:
        source_code  VARCHAR2

Output:
        Y or null
------------------------------------------------------------------------------*/
CURSOR out_cur IS
        SELECT  'Y'
          FROM source_master
         WHERE upper(source_code) = upper(IN_SOURCE_CODE);

v_return        varchar2(100);

BEGIN

OPEN out_cur;
FETCH out_cur INTO v_return;
CLOSE out_cur;

RETURN v_return;

END SOURCE_CODE_EXISTS;


PROCEDURE GET_SOURCE_FLORIST_PRIORITY
(
 IN_SOURCE_CODE  IN  SOURCE_MASTER.SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all priority florists (primary, secondary, etc.) for the given
   source code.  Highest priority (i.e. lowest value) will be returned first
   and should be considered the Primary florist.

------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT florist_id,
             priority
       FROM source_florist_priority
       WHERE source_code = in_source_code
      ORDER BY priority;

END GET_SOURCE_FLORIST_PRIORITY;

PROCEDURE GET_SRC_DFLT_ORDER_INFO
(
 IN_SOURCE_CODE                    IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 OUT_CURSOR                        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the default order information in the Source
		Master Table.

-----------------------------------------------------------------------------*/

BEGIN
  OPEN OUT_CURSOR FOR
    SELECT
        recpt_location_type,
        recpt_business_name,
        recpt_location_detail,
        recpt_address,
        recpt_zip_code,
        recpt_city,
        recpt_state_id,
        recpt_country_id,
        recpt_phone,
        recpt_phone_ext,
        cust_first_name,
        cust_last_name,
        cust_daytime_phone,
        cust_daytime_phone_ext,
        cust_evening_phone,
        cust_evening_phone_ext,
        cust_address,
        cust_zip_code,
        cust_city,
        cust_state_id,
        cust_country_id,
        cust_email_address,
        requested_by
  FROM source_master
  WHERE source_code = in_source_code;

END GET_SRC_DFLT_ORDER_INFO;

PROCEDURE GET_SRC_SURCH_VAL_BY_ORD_TIME
(
 IN_SOURCE_CODE                    IN SOURCE_MASTER$.SOURCE_CODE%TYPE,
 IN_ORDER_TIME                     IN SOURCE_MASTER$.TIMESTAMP$%TYPE,
 OUT_CUR                           OUT TYPES.REF_CURSOR
 )
 AS

/*------------------------------------------------------------------------------
Description:
   Retrieves one source code record from SOURCE_MASTER$ based on source_code and order timestamp.
------------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
   SELECT
        SURCHARGE_AMOUNT,
        APPLY_SURCHARGE_CODE,
        SURCHARGE_DESCRIPTION,
        DISPLAY_SURCHARGE_FLAG
   FROM   FTD_APPS.SOURCE_MASTER$ sm
   WHERE  sm.SOURCE_CODE = in_source_code
   AND sm. operation$ in ('UPD_NEW','INS')
   AND sm.TIMESTAMP$ =
        ( Select max(timestamp$)
            from ftd_apps.SOURCE_MASTER$ sm2
            WHERE  sm2.SOURCE_CODE = sm.source_code
            and sm2.timestamp$ <= in_order_time
            and sm2.operation$ in ('UPD_NEW','INS')
         );
END GET_SRC_SURCH_VAL_BY_ORD_TIME;


PROCEDURE GET_SRC_SURCH_VAL_FRM_SRC_MSTR
(
 IN_SOURCE_CODE                    IN SOURCE_MASTER$.SOURCE_CODE%TYPE,
 OUT_CUR                           OUT TYPES.REF_CURSOR
 )
 AS

/*------------------------------------------------------------------------------
Description:
   Retrieves one source code record from SOURCE_MASTER based on source_code .
------------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
   SELECT
        SURCHARGE_AMOUNT,
        APPLY_SURCHARGE_CODE,
        SURCHARGE_DESCRIPTION,
        DISPLAY_SURCHARGE_FLAG
   FROM   FTD_APPS.SOURCE_MASTER
   WHERE  SOURCE_CODE = IN_SOURCE_CODE ;
END GET_SRC_SURCH_VAL_FRM_SRC_MSTR;


PROCEDURE GET_ALL_SRC_MP_MEMBER_LVL_INFO
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS

/*------------------------------------------------------------------------------
Description:
   Retrieves all the member levels,their descriptions and assigned source code
------------------------------------------------------------------------------*/

BEGIN
  OPEN OUT_CURSOR FOR
    SELECT MP_MEMBER_LEVEL_ID, DESCRIPTION, SOURCE_CODE
    FROM FTD_APPS.SOURCE_MP_MEMBER_LEVEL;
END GET_ALL_SRC_MP_MEMBER_LVL_INFO;


PROCEDURE GET_SOURCE_CODE_HIST_RECORD (
   IN_SOURCE_CODE    	IN  	FTD_APPS.SOURCE_MASTER$.SOURCE_CODE%TYPE,
   IN_ORDER_TIME        IN 		FTD_APPS.SOURCE_MASTER$.TIMESTAMP$%TYPE,
   OUT_CUR              OUT 	TYPES.REF_CURSOR
)  AS
/*------------------------------------------------------------------------------
Description:
   Retrieves source code record from history table.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR

   SELECT DISTINCT
        SM.MORNING_DELIVERY_FLAG,
        SM.DELIVERY_FEE_ID,
        SM.MORNING_DELIVERY_FREE_SHIPPING,
        SM.SOURCE_CODE
   FROM FTD_APPS.SOURCE_MASTER$ SM
   WHERE  SM.SOURCE_CODE = IN_SOURCE_CODE
   AND SM.OPERATION$ IN ('UPD_NEW','INS')
   AND SM.TIMESTAMP$ =
        (SELECT MAX(TIMESTAMP$)
            FROM FTD_APPS.SOURCE_MASTER$ SM2
            WHERE  SM2.SOURCE_CODE = SM.SOURCE_CODE
            and SM2.TIMESTAMP$ <= IN_ORDER_TIME
            and SM2.OPERATION$ in ('UPD_NEW','INS')
         );

END GET_SOURCE_CODE_HIST_RECORD;

PROCEDURE GET_SOURCE_CODE_DETAIL
(
 IN_SOURCE_CODE				IN 		SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_ORDER_SOURCE            IN 		SOURCE_MASTER.ORDER_SOURCE%TYPE,
 OUT_SRC_MASTER_CURSOR      OUT 	TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns the source code details from different tables.

Input:
        source_code               	VARCHAR2
        order_source                VARCHAR2

Output:
        cursors containing source code detail
------------------------------------------------------------------------------*/
v_in_src_code      varchar2(20);

BEGIN

v_in_src_code := IN_SOURCE_CODE;

IF IN_ORDER_SOURCE = 'P' THEN
    v_in_src_code := IN_SOURCE_CODE || 'P';
END IF;

	OPEN OUT_SRC_MASTER_CURSOR FOR
		SELECT DISTINCT SM.*,
        SPB.CREATED_BY,
	      SPB.PARTNER_NAME,
	      SPB.BIN_PROCESSING_ACTIVE_FLAG,
	      SPR.PROGRAM_NAME,
	      SPR.START_DATE,
	      SPR.SOURCE_PROGRAM_REF_ID,
	      PSM.PROGRAM_ID AS EMAIL_PROGRAM_ID,
	      DECODE(BI.INFO_DESCRIPTION, 'CERTIFICATE#', 'Y', 'N') GIFT_CERTIFICATE_FLAG,
	      DECODE(BI.INFO_DESCRIPTION, 'CERTIFICATE#', 'N', null, 'N', 'Y') BILLING_INFO_FLAG
    	FROM SOURCE_MASTER SM
    	LEFT JOIN SOURCE_PARTNER_BIN_MAPPING SPB
        	ON SM.SOURCE_CODE=SPB.SOURCE_CODE
    	LEFT JOIN SOURCE_PROGRAM_REF SPR
        	ON SM.SOURCE_CODE=SPR.SOURCE_CODE
    	LEFT JOIN FRP.program_to_source_mapping PSM
        	ON  PSM.SOURCE_CODE=SM.SOURCE_CODE
        LEFT JOIN FTD_APPS.BILLING_INFO BI
        	ON SM.SOURCE_CODE = BI.SOURCE_CODE_ID
    	WHERE SM.SOURCE_CODE = v_in_src_code;

END GET_SOURCE_CODE_DETAIL;

PROCEDURE GET_APE_BASE_SOURCE_CODES
(
 IN_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records from FTD_APPS.APE_BASE_SOURCE_CODES for a given source_code.

Input:
        source_code  VARCHAR2

Output:
        cursor containing base source codes info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT BASE_SOURCE_CODE
       FROM FTD_APPS.APE_BASE_SOURCE_CODES
      WHERE MASTER_SOURCE_CODE = IN_SOURCE_CODE;

END GET_APE_BASE_SOURCE_CODES;

PROCEDURE IS_IOTW_SOURCE_CODE
(
 IN_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Checks if the given source_code is IOTW source code.

Input:
        source_code  VARCHAR2

Output:
        cursor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT SOURCE_CODE
       FROM FTD_APPS.SOURCE_MASTER
      WHERE SOURCE_CODE = IN_SOURCE_CODE
	  and IOTW_FLAG = 'Y';

END IS_IOTW_SOURCE_CODE;

PROCEDURE IS_INVALID_SOURCE_CODE
(
 IN_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Checks if the given source_code exist in source master table.

Input:
        source_code  VARCHAR2

Output:
        cursor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT SOURCE_CODE
       FROM FTD_APPS.SOURCE_MASTER
      WHERE SOURCE_CODE = IN_SOURCE_CODE;

END IS_INVALID_SOURCE_CODE;

PROCEDURE IS_APE_ENABLED_SOURCE_CODE
(
 IN_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Checks if the given source_code exist in source master table.

Input:
        source_code  VARCHAR2

Output:
        cursor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT SOURCE_CODE
       FROM FTD_APPS.SOURCE_MASTER
      WHERE SOURCE_CODE = IN_SOURCE_CODE
	  and AUTO_PROMOTION_ENGINE = 'Y';

END IS_APE_ENABLED_SOURCE_CODE;

PROCEDURE IS_MILES_POINTS_SOURCE_CODE
(
 IN_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Checks if the given source_code has miles/points promotions.

Input:
        source_code  VARCHAR2

Output:
        cursor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT SOURCE_CODE
       FROM FTD_APPS.SOURCE_MASTER
      WHERE SOURCE_CODE = IN_SOURCE_CODE
	  and PRICE_HEADER_ID = 'ZZ';

END IS_MILES_POINTS_SOURCE_CODE;

PROCEDURE CHECK_BASE_SOURCE_CODE
(
 IN_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   checks if the given source_code is a base source code.

Input:
        source_code  VARCHAR2

Output:
        cursor containing base source codes info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT absc.MASTER_SOURCE_CODE
       FROM FTD_APPS.APE_BASE_SOURCE_CODES absc
      WHERE absc.BASE_SOURCE_CODE = IN_SOURCE_CODE
      and exists(select 1 from ftd_apps.source_master where source_code=absc.MASTER_SOURCE_CODE and auto_promotion_engine='Y');

END CHECK_BASE_SOURCE_CODE;

PROCEDURE GET_SOURCE_CODE_FLORISTS
(
IN_SOURCE_CODE  IN    SOURCE_MASTER.SOURCE_CODE%TYPE,
OUT_CUR         OUT   TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves florists associated with a Source code.
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR

  select distinct sm.source_code, fm.florist_id, fm.florist_name, fm.zip_code, fm.state,fm.city,
  	fm.address,fm.phone_number, fm.status, fm.longitude, fm.latitude, fm.last_updated_date, sf.priority
  from ftd_apps.source_master sm
  	join ftd_apps.source_florist_priority sf on sf.source_code = sm.source_code
  	join ftd_apps.florist_master fm on sf.florist_id = fm.florist_id
  where sm.source_code = in_source_code order by fm.florist_name;

END GET_SOURCE_CODE_FLORISTS;

PROCEDURE GET_SRC_MP_MEMBER_LVL_INFO
(
 IN_SOURCE_CODE    	IN  	FTD_APPS.SOURCE_MP_MEMBER_LEVEL.SOURCE_CODE%TYPE,
 OUT_CURSOR 		OUT 	TYPES.REF_CURSOR
)
AS

/*------------------------------------------------------------------------------
Description:
   Retrieves the member levels,their descriptions and assigned source code
   for a given source code.
------------------------------------------------------------------------------*/
BEGIN
  OPEN OUT_CURSOR FOR
    SELECT MP_MEMBER_LEVEL_ID, DESCRIPTION, SOURCE_CODE
    FROM FTD_APPS.SOURCE_MP_MEMBER_LEVEL WHERE SOURCE_CODE = IN_SOURCE_CODE;

END GET_SRC_MP_MEMBER_LVL_INFO;

PROCEDURE GET_SOURCE_CODE_FOR_LEGACY_ID
(
IN_LEGACY_ID       IN FTD_APPS.Source_Legacy_Id_Mapping.Legacy_Id%TYPE,
OUT_SOURCE_CODE    OUT VARCHAR2
)

AS

BEGIN

  select  SOURCE_CODE
  into    OUT_SOURCE_CODE
  From    Ftd_Apps.Source_Legacy_Id_Mapping Slm
  where   slm.LEGACY_ID = IN_LEGACY_ID 
  AND rownum = 1;
  
  EXCEPTION WHEN NO_DATA_FOUND THEN
        OUT_SOURCE_CODE:= null;

END GET_SOURCE_CODE_FOR_LEGACY_ID;

PROCEDURE GET_LEGACY_ID_FOR_SOURCE_CODE
(
IN_SOURCE_CODE       IN FTD_APPS.Source_Legacy_Id_Mapping.SOURCE_CODE%TYPE,
OUT_LEGACY_ID    OUT VARCHAR2
)

AS

BEGIN

  select  LEGACY_ID
  into    OUT_LEGACY_ID
  From    Ftd_Apps.Source_Legacy_Id_Mapping Slm
  where   slm.SOURCE_CODE = IN_SOURCE_CODE;
  
  EXCEPTION WHEN NO_DATA_FOUND THEN
        OUT_LEGACY_ID:= null;
  
END GET_LEGACY_ID_FOR_SOURCE_CODE;



PROCEDURE GET_SOURCE_CODE_TAX_FLAG
(
IN_SOURCE_CODE                 IN FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
OUT_SOURCE_CODE_TAX_FLAG            OUT FTD_APPS.SOURCE_MASTER.CALCULATE_TAX_FLAG%TYPE
)
AS

BEGIN

      SELECT CALCULATE_TAX_FLAG into OUT_SOURCE_CODE_TAX_FLAG FROM FTD_APPS.SOURCE_MASTER WHERE SOURCE_CODE=IN_SOURCE_CODE;

END GET_SOURCE_CODE_TAX_FLAG;

PROCEDURE GET_AFFILIATE_CODE
(
IN_SOURCE_CODE   IN FTD_APPS.Source_Legacy_Id_Mapping.SOURCE_CODE%TYPE,
OUT_AFFILIATE_CODE    OUT VARCHAR2
)
AS

BEGIN
  select  custom_shipping_carrier   into    OUT_AFFILIATE_CODE  From    Ftd_Apps.Source_master  where   SOURCE_CODE = IN_SOURCE_CODE;

EXCEPTION WHEN NO_DATA_FOUND THEN
    OUT_AFFILIATE_CODE:= null;
      
END GET_AFFILIATE_CODE;



PROCEDURE GET_REFUND_MERCH_AMT_FLAG
(
IN_SOURCE_CODE                 IN FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
OUT_FLAG                       OUT VARCHAR2
)
AS

BEGIN

      SELECT Merch_Amt_Full_Refund_Flag into OUT_FLAG FROM FTD_APPS.SOURCE_MASTER WHERE SOURCE_CODE=IN_SOURCE_CODE;

EXCEPTION WHEN NO_DATA_FOUND THEN
    OUT_FLAG:= null;

END GET_REFUND_MERCH_AMT_FLAG;


END;

.
/