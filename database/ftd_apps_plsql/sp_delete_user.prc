CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_USER (
 userID   		    IN VARCHAR2,
 updateUser       IN VARCHAR2,
 lastUpdateDate   IN DATE
)
as
--==============================================================================
--
-- Name:    SP_DELETE_USER
-- Type:    Procedure
-- Syntax:  SP_DELETE_USER ( <see args above> )
--
-- Description:   Marks the input user on USER_PROFILE as deleted.
--
--==============================================================================

    tmp_date    DATE;

    -- Cursor handles row locking for updating of USER_PROFILE table
    CURSOR user_lock_cur (p_user_id  IN user_profile.user_id%TYPE) IS
        SELECT * FROM user_profile
        WHERE USER_ID = p_user_id
        FOR UPDATE;
    user_update_row    user_lock_cur%ROWTYPE;

begin

    -- Lock the existing row on USER_PROFILE for update
    OPEN user_lock_cur(userID);
    FETCH user_lock_cur INTO user_update_row;

    -- Error if the last update date on existing row differs from the input parameter
    IF ( user_update_row.last_updated_date <> lastUpdateDate )
    THEN
        CLOSE user_lock_cur;
        COMMIT;
        RAISE_APPLICATION_ERROR(-20001, 'User delete for ID ' || userID || ' failed due to last_update date mismatch. - TABLE: ' || to_char(user_update_row.last_updated_date,'MMDDYYYYHH24MISS') || ' PARM: ' || to_char(lastUpdateDate,'MMDDYYYYHH24MISS') || '*');
    END IF;

    UPDATE USER_PROFILE
    SET DELETED_FLAG = 'Y',
        LAST_UPDATED_BY = updateUser,
        LAST_UPDATED_DATE = to_date(to_char(SYSDATE,'MMDDYYYYHH24MISS'),'MMDDYYYYHH24MISS')
    WHERE CURRENT OF user_lock_cur;

    -- Release the row lock on USER_PROFILE with a commit
    CLOSE user_lock_cur;
    COMMIT;

end
;
.
/
