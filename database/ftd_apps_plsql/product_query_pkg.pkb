CREATE OR REPLACE
PACKAGE BODY ftd_apps.PRODUCT_QUERY_PKG AS


FUNCTION GET_SHIP_METHOD_BY_ID (
	IN_SHIP_ID    IN ship_methods.ship_method_id%TYPE )
RETURN ship_methods.description%TYPE
IS

v_desc ship_methods.description%TYPE;

BEGIN


BEGIN
SELECT description
INTO v_desc
FROM ship_methods
WHERE ship_method_id = in_ship_id;

EXCEPTION WHEN NO_DATA_FOUND THEN v_desc := NULL;
END;

RETURN v_desc;

END GET_SHIP_METHOD_BY_ID;



FUNCTION GET_SUBCODE_REFERENCE_NUMBER (
	IN_ID    IN product_subcodes.product_id%TYPE )
RETURN product_subcodes.subcode_reference_number%TYPE
IS

v_data product_subcodes.subcode_reference_number%TYPE;

BEGIN

BEGIN
SELECT subcode_reference_number
INTO v_data
FROM product_subcodes
WHERE product_subcode_id = in_id;

EXCEPTION WHEN NO_DATA_FOUND THEN v_data := NULL;
END;

RETURN v_data;

END GET_SUBCODE_REFERENCE_NUMBER;




FUNCTION GET_PRODUCT_NAME_BY_ID
	( IN_PRODUCT_ID         IN  PRODUCT_MASTER.PRODUCT_ID%TYPE )

	 RETURN PRODUCT_MASTER.PRODUCT_NAME%TYPE
IS

v_data PRODUCT_MASTER.PRODUCT_NAME%TYPE;
BEGIN

	BEGIN
	SELECT product_name
	INTO v_data
	FROM product_master
	WHERE product_id = in_product_id;

	EXCEPTION WHEN NO_DATA_FOUND THEN v_data := NULL;
	END;
RETURN v_data;
END GET_PRODUCT_NAME_BY_ID;





PROCEDURE GET_PRODUCT_FEED_BY_SEARCH
(
 IN_PRODUCT_ID              IN PRODUCT_FEED_EXTERNAL.PRODUCT_ID%TYPE,
 IN_NOVATOR_ID              IN PRODUCT_MASTER.NOVATOR_ID%TYPE,
 IN_SORT_BY                  IN VARCHAR2,
 IN_START_POSITION           IN NUMBER,
 IN_END_POSITION             IN NUMBER,
 OUT_MAX_ROWS               OUT NUMBER,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the distinct records in table source_master for the
   optional input input parameters.

Input:
        in_product_id             VARCHAR2
        in_novator_id             VARCHAR2
        in_sort_by                VARCHAR2
        in_start_position         NUMBER
        in_end_position           NUMBER

Output:
        out_max_rows              NUMBER
        cursor containing source_master info
------------------------------------------------------------------------------*/

v_main_sql VARCHAR2(4000);
v_rowcount_sql VARCHAR2(4000);
v_sql VARCHAR2(1000);
v_params_exist BOOLEAN := FALSE;
v_sfmb varchar2(100) := 'SFMB';


BEGIN

   v_main_sql := 'SELECT pm.PRODUCT_ID,
pm.NOVATOR_ID,
pm.PRODUCT_NAME,
pm.NOVATOR_NAME,
pm.STATUS,
pm.DELIVERY_TYPE,
pm.CATEGORY,
pm.PRODUCT_TYPE,
pm.PRODUCT_SUB_TYPE,
pm.COLOR_SIZE_FLAG,
pm.STANDARD_PRICE,
pm.DELUXE_PRICE,
pm.PREMIUM_PRICE,
pm.PREFERRED_PRICE_POINT,
pm.VARIABLE_PRICE_MAX,
pm.SHORT_DESCRIPTION,
pm.LONG_DESCRIPTION,
pm.FLORIST_REFERENCE_NUMBER,
pm.MERCURY_DESCRIPTION,
pm.ITEM_COMMENTS,
pm.ADD_ON_BALLOONS_FLAG,
pm.ADD_ON_BEARS_FLAG,
pm.ADD_ON_CARDS_FLAG,
pm.ADD_ON_FUNERAL_FLAG,
pm.CODIFIED_FLAG,
pm.EXCEPTION_CODE,
pm.EXCEPTION_START_DATE,
pm.EXCEPTION_END_DATE,
pm.EXCEPTION_MESSAGE,
pm.VENDOR_ID,
pm.VENDOR_COST,
pm.VENDOR_SKU,
pm.SECOND_CHOICE_CODE,
pm.DROPSHIP_CODE,
pm.DISCOUNT_ALLOWED_FLAG,
pm.DELIVERY_INCLUDED_FLAG,
pm.SERVICE_FEE_FLAG,
pm.EXOTIC_FLAG,
pm.EGIFT_FLAG,
pm.COUNTRY_ID,
pm.ARRANGEMENT_SIZE,
pm.ARRANGEMENT_COLORS,
pm.DOMINANT_FLOWERS,
pm.SEARCH_PRIORITY,
pm.standard_RECIPE recipe,
pm.SUBCODE_FLAG,
pm.DIM_WEIGHT,
pm.NEXT_DAY_UPGRADE_FLAG,
pm.CORPORATE_SITE,
pm.UNSPSC_CODE,
pm.PRICE_RANK_1,
pm.PRICE_RANK_2,
pm.PRICE_RANK_3,
pm.SHIP_METHOD_CARRIER,
pm.SHIP_METHOD_FLORIST,
pm.SHIPPING_KEY,
pm.LAST_UPDATE,
pm.VARIABLE_PRICE_FLAG,
pm.HOLIDAY_SKU,
pm.HOLIDAY_PRICE,
pm.CATALOG_FLAG,
pm.HOLIDAY_SECOND_CHOICE_CODE,
pm.HOLIDAY_DELUXE_PRICE,
pm.HOLIDAY_PREMIUM_PRICE,
pm.HOLIDAY_START_DATE,
pm.HOLIDAY_END_DATE,
pm.MERCURY_SECOND_CHOICE,
pm.MERCURY_HOLIDAY_SECOND_CHOICE,
pm.ADD_ON_CHOCOLATE_FLAG,
pm.NO_TAX_FLAG,
pm.JCP_CATEGORY,
pm.CROSS_REF_NOVATOR_ID,
pm.GENERAL_COMMENTS,
pm.SENT_TO_NOVATOR_PROD,
pm.SENT_TO_NOVATOR_UAT,
pm.SENT_TO_NOVATOR_TEST,
pm.SENT_TO_NOVATOR_CONTENT,
pm.COMPANY_ID,
pm.DEFAULT_CARRIER,
pm.HOLD_UNTIL_AVAILABLE,
pm.LAST_UPDATE_USER_ID,
pm.LAST_UPDATE_SYSTEM,
pm.MONDAY_DELIVERY_FRESHCUT,
pm.TWO_DAY_SAT_FRESHCUT,
pm.WEBOE_BLOCKED,
pm.EXPRESS_SHIPPING_ONLY,
pm.OVER_21,
pm.SHIPPING_SYSTEM,
                         pfe.buy_url,
                         pfe.image_url,
                         pfe.important_product_flag,
                         pfe.promotional_text,
                         pfe.shipping_promotional_text,
                         pfe.created_on,
                         pfe.created_by,
                         pfe.updated_on,
                         pfe.updated_by,
                         cp.codification_id,
                         pc.description category_description
                    FROM product_master pm,
                         product_feed_external pfe,
                         codified_products cp,
                         product_category pc,
                         product_company_xref pcx
                   WHERE pm.product_id = pcx.product_id
                         and pcx.company_id = ''FTD''
                         and pm.product_id = pfe.product_id (+)
                         and pm.product_id = cp.product_id (+)
                         and pm.category = pc.product_category_id (+)';

   v_rowcount_sql := 'SELECT COUNT(DISTINCT pm.product_id)
                    FROM product_master pm,
                         product_feed_external pfe,
                         product_company_xref pcx
                   WHERE pm.product_id = pcx.product_id
                         and pcx.company_id = ''FTD''
                         and pm.product_id = pfe.product_id (+)';


   IF in_product_id IS NOT NULL THEN

        v_sql := ' AND LOWER(pm.product_id) = LOWER(''' || in_product_id || ''')';
        v_main_sql := v_main_sql || v_sql;
        v_rowcount_sql := v_rowcount_sql || v_sql;

   END IF;

   IF in_novator_id IS NOT NULL THEN

        v_sql := ' AND LOWER(pm.novator_id) = LOWER(''' || in_novator_id || ''')';
        v_main_sql := v_main_sql || v_sql;
        v_rowcount_sql := v_rowcount_sql || v_sql;

   END IF;

   IF in_sort_by IS NOT NULL THEN
     IF (lower(in_sort_by) = 'product_id') THEN
       v_main_sql := v_main_sql || ' ORDER BY pm.product_id';
     ELSE
       v_main_sql := v_main_sql || ' ORDER BY ' || in_sort_by || ', pm.product_id';
     END IF;
   END IF;

   --the following code will only return the rows asked for
   IF in_start_position IS NOT NULL AND in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT *
                     FROM (SELECT rslt.*,
                                  ROWNUM rnum
                             FROM (' || v_main_sql || ') rslt
                            WHERE ROWNUM <= ' || in_end_position || ')
                    WHERE rnum >= ' || in_start_position;
   ELSIF in_start_position IS NOT NULL THEN
     v_main_sql := 'SELECT *
                     FROM (SELECT rslt.*,
                                 ROWNUM rnum
                             FROM (' || v_main_sql || ') rslt)
                    WHERE rnum >= ' || in_start_position;
   ELSIF in_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ') rslt
                            WHERE ROWNUM <= ' || in_end_position;
   END IF;
   EXECUTE IMMEDIATE v_rowcount_sql INTO out_max_rows;

   OPEN out_cursor FOR v_main_sql;
--open out_cursor for select v_main_sql from dual;

END GET_PRODUCT_FEED_BY_SEARCH;


PROCEDURE UPDATE_INSERT_PRODUCT_FEED_EXT
(
 IN_PRODUCT_ID                 IN PRODUCT_FEED_EXTERNAL.PRODUCT_ID%TYPE,
 IN_BUY_URL                    IN PRODUCT_FEED_EXTERNAL.BUY_URL%TYPE,
 IN_IMAGE_URL                  IN PRODUCT_FEED_EXTERNAL.IMAGE_URL%TYPE,
 IN_IMPORTANT_PRODUCT_FLAG     IN PRODUCT_FEED_EXTERNAL.IMPORTANT_PRODUCT_FLAG%TYPE,
 IN_PROMOTIONAL_TEXT           IN PRODUCT_FEED_EXTERNAL.PROMOTIONAL_TEXT%TYPE,
 IN_SHIPPING_PROMOTIONAL_TEXT  IN PRODUCT_FEED_EXTERNAL.SHIPPING_PROMOTIONAL_TEXT%TYPE,
 IN_UPDATED_BY                 IN PRODUCT_FEED_EXTERNAL.UPDATED_BY%TYPE,
 OUT_STATUS                   OUT VARCHAR2,
 OUT_MESSAGE                  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the product_external_feed table.

Input:
        PRODUCT_ID                  VARCHAR2
        BUY_URL                     VARCHAR2
        IMAGE_URL                   VARCHAR2
        IMPORTANT_PRODUCT_FLAG      VARCHAR2
        PROMOTIONAL_TEXT            VARCHAR2
        SHIPPING_PROMOTIONAL_TEXT   VARCHAR2
        UPDATED_BY                  VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE product_feed_external
    SET buy_url = in_buy_url,
        image_url = in_image_url,
        important_product_flag = in_important_product_flag,
        promotional_text = in_promotional_text,
        shipping_promotional_text = in_shipping_promotional_text,
        updated_on = SYSDATE,
        updated_by = in_updated_by
   WHERE product_id = in_product_id;


  IF SQL%NOTFOUND THEN
    INSERT INTO product_feed_external
       (product_id,
        buy_url,
        image_url,
        important_product_flag,
        promotional_text,
        shipping_promotional_text,
        created_on,
        created_by,
        updated_on,
        updated_by)
      VALUES
       (in_product_id,
        in_buy_url,
        in_image_url,
        in_important_product_flag,
        in_promotional_text,
        in_shipping_promotional_text,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'UPDATE_INSERT_PRODUCT_FEED_EXTERNAL: ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_PRODUCT_FEED_EXT;

PROCEDURE GET_SUBCODE_DETAILS
(
 IN_PRODUCT_ID               IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in product_subcodes for a given product_id.

Input:
        in_product_id             VARCHAR2

Output:
        cursor containing product_subcode info
------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cursor FOR

      SELECT  product_subcode_id,
              subcode_description,
              subcode_price,
              subcode_reference_number,
              dim_weight,
              active_flag
      FROM    product_subcodes
      WHERE   product_id = in_product_id;

END GET_SUBCODE_DETAILS;

PROCEDURE GET_ADDITIONAL_PRODUCT_INFO
(
 IN_PRODUCT_ID               IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns additional product information for a given product_id.

Input:
        in_product_id             VARCHAR2

Output:
        cursor containing product info
------------------------------------------------------------------------------*/

CURSOR product_colors(in_product varchar) IS
    SELECT   pc.product_color,
             cm.description
    FROM     ftd_apps.product_colors pc,
             ftd_apps.color_master cm
    WHERE    pc.product_id = in_product
             and cm.color_master_id = pc.product_color
    ORDER BY pc.order_by, pc.product_color;

CURSOR product_excluded_states(in_product varchar) IS
    SELECT   pes.excluded_state
    FROM     ftd_apps.product_excluded_states pes
    WHERE    pes.product_id = in_product
    ORDER BY pes.excluded_state;

CURSOR product_keywords(in_product varchar) IS
    SELECT   DISTINCT pk.keyword
    FROM     ftd_apps.product_keywords pk
    WHERE    pk.product_id = in_product
    ORDER BY pk.keyword;

CURSOR product_ship_dates(in_product varchar) IS
    SELECT   psd.monday_flag,
             psd.tuesday_flag,
             psd.wednesday_flag,
             psd.thursday_flag,
             psd.friday_flag,
             psd.saturday_flag,
             psd.sunday_flag
    FROM     ftd_apps.product_ship_dates psd
    WHERE    psd.product_id = in_product;

CURSOR product_ship_methods(in_product varchar) IS
    SELECT   psm.ship_method_id
    FROM     ftd_apps.product_ship_methods psm
    WHERE    psm.product_id = in_product
    ORDER BY psm.ship_method_id;

CURSOR shipping_costs(in_product varchar, in_method varchar) IS
    SELECT   skc.shipping_cost
    FROM     product_master pm,
             shipping_key_details skd,
             shipping_key_costs skc
    WHERE    pm.product_id = in_product
             and skd.shipping_key_id = pm.shipping_key
             and skd.min_price <= pm.standard_price
             and skd.max_price >= pm.standard_price
             and skc.shipping_key_detail_id = skd.shipping_detail_id
             and skc.shipping_method_id = in_method;

CURSOR recipient_search(in_product varchar) IS
    SELECT   rsm.description
    FROM     product_recipient_search prs,
             recipient_search_master rsm
    WHERE    prs.product_id = in_product
             and rsm.recipient_search_id = prs.recipient_search_id
    ORDER BY rsm.description;

out_colors           varchar2(4000) := '';
out_excluded_states  varchar2(4000) := '';
out_keywords         varchar2(4000) := '';
out_ship_dates       varchar2(4000) := '';
out_ship_methods     varchar2(4000) := '';
gr_cost              varchar2(100) := '';
two_cost             varchar2(100) := '';
nd_cost              varchar2(100) := '';
temp_cost            varchar2(100) := '';
out_recipient_search varchar2(4000) := '';

begin

    for msg in product_colors(in_product_id) loop
        if length(out_colors) IS NULL then
            out_colors := msg.description;
        else
            out_colors := out_colors || ',' || msg.description;
        end if;
    end loop;

    for msg in product_excluded_states(in_product_id) loop
        if length(out_excluded_states) IS NULL then
            out_excluded_states := msg.excluded_state;
        else
            out_excluded_states := out_excluded_states || ',' || msg.excluded_state;
        end if;
    end loop;

    for msg in product_keywords(in_product_id) loop
        if length(out_keywords) IS NULL then
            out_keywords := msg.keyword;
        else
            out_keywords := out_keywords || ',' || msg.keyword;
        end if;
    end loop;

    for msg in product_ship_dates(in_product_id) loop
        if msg.monday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Monday';
            else
                out_ship_dates := out_ship_dates || ',Monday';
            end if;
        end if;
        if msg.tuesday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Tuesday';
            else
                out_ship_dates := out_ship_dates || ',Tuesday';
            end if;
        end if;
        if msg.wednesday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Wednesday';
            else
                out_ship_dates := out_ship_dates || ',Wednesday';
            end if;
        end if;
        if msg.thursday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Thursday';
            else
                out_ship_dates := out_ship_dates || ',Thursday';
            end if;
        end if;
        if msg.friday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Friday';
            else
                out_ship_dates := out_ship_dates || ',Friday';
            end if;
        end if;
        if msg.saturday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Saturday';
            else
                out_ship_dates := out_ship_dates || ',Saturday';
            end if;
        end if;
        if msg.sunday_flag = 'N' then
            if length(out_ship_dates) IS NULL then
                out_ship_dates := 'Sunday';
            else
                out_ship_dates := out_ship_dates || ',Sunday';
            end if;
        end if;
    end loop;

    for msg in product_ship_methods(in_product_id) loop
        if length(out_ship_methods) IS NULL then
            out_ship_methods := msg.ship_method_id;
        else
            out_ship_methods := out_ship_methods || ',' || msg.ship_method_id;
        end if;

        open shipping_costs(in_product_id, msg.ship_method_id);
        fetch shipping_costs into temp_cost;
        close shipping_costs;

        if msg.ship_method_id = 'GR' then
            gr_cost := temp_cost;
        elsif msg.ship_method_id = '2F' then
            two_cost := temp_cost;
        elsif msg.ship_method_id = 'ND' then
            nd_cost := temp_cost;
        end if;

    end loop;

    for msg in recipient_search(in_product_id) loop
        if length(out_recipient_search) IS NULL then
            out_recipient_search := msg.description;
        else
            out_recipient_search := out_recipient_search || ',' || msg.description;
        end if;
    end loop;

    OPEN out_cursor FOR
        SELECT  out_colors product_colors,
                out_excluded_states excluded_states,
                out_keywords product_keywords,
                out_ship_dates excluded_ship_days,
                out_ship_methods ship_methods,
                gr_cost ground_cost,
                two_cost two_day_cost,
                nd_cost next_day_cost,
                out_recipient_search recipient_search
        FROM    dual;

END GET_ADDITIONAL_PRODUCT_INFO;


PROCEDURE GET_PROD_FEED_CI_PRODUCTS
(
 IN_PRODUCT_ID               IN VARCHAR2,
 IN_UPSELL_ID					  IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the regular and upsell child products for product feed to CI.
   
Input:
       Optional in_product_id
	   
Output:
        cursor containing source_master info
------------------------------------------------------------------------------*/
v_sql VARCHAR2(1000);
v_main_sql VARCHAR2(4000);
V_DISCOUNT_SOURCE_CODE VARCHAR2(10);

BEGIN
  BEGIN
     SELECT VALUE INTO V_DISCOUNT_SOURCE_CODE from FRP.GLOBAL_PARMS WHERE CONTEXT='PARTNER_REWARD' AND NAME='SITEWIDE_DISCOUNT_SOURCE_CODE';
  EXCEPTION 
  WHEN OTHERS THEN
	 V_DISCOUNT_SOURCE_CODE := '33952';
  END;
	
   v_main_sql := 'SELECT 
	distinct pm.PRODUCT_ID,
    pm.NOVATOR_ID,
    ud.UPSELL_MASTER_ID,
    pm.NOVATOR_NAME,
    pm.LONG_DESCRIPTION,
    pm.STANDARD_PRICE, 
    i.IOTW_SOURCE_CODE,
    phd.DISCOUNT_TYPE,
    pm.NOVATOR_NAME,
    pc.DESCRIPTION,
    pt.TYPE_DESCRIPTION,
    pst.SUB_TYPE_DESCRIPTION,
	pm.STATUS,
	pt.TYPE_ID,
	pm.DISCOUNT_ALLOWED_FLAG,
	pm.EXCEPTION_START_DATE,
    pm.EXCEPTION_END_DATE,
	pm.ARRANGEMENT_SIZE, 
	pm.DOMINANT_FLOWERS,
	ud.UPSELL_DETAIL_SEQUENCE,
	pm.PQUAD_PRODUCT_ID
FROM 
    FTD_APPS.PRODUCT_MASTER pm, 
    FTD_APPS.PRODUCT_COMPANY_XREF pcx,
    JOE.IOTW i,
    FTD_APPS.SOURCE_MASTER sm,
    FTD_APPS.UPSELL_DETAIL ud,
    FTD_APPS.PRICE_HEADER_DETAILS phd,
    FTD_APPS.PRODUCT_CATEGORY pc,
    FTD_APPS.PRODUCT_TYPES pt,
    FTD_APPS.PRODUCT_SUB_TYPES pst
WHERE 1=1
    and pm.product_id = pcx.product_id
    and pc.PRODUCT_CATEGORY_ID = pm.CATEGORY
    and pm.PRODUCT_TYPE = pt.TYPE_ID(+)
    and (pm.PRODUCT_SUB_TYPE = pst.SUB_TYPE_ID(+) and pm.product_type = pst.TYPE_ID(+))
    and pm.PRODUCT_ID = i.PRODUCT_ID(+)
    and i.IOTW_SOURCE_CODE = sm.SOURCE_CODE(+)
    and pm.product_id = ud.upsell_detail_id(+)
    and sm.PRICE_HEADER_ID = phd.PRICE_HEADER_ID(+)
    and pcx.company_id = ''FTD''
    and pm.NOVATOR_ID is not null
    and i.SOURCE_CODE(+) = '''|| V_DISCOUNT_SOURCE_CODE||'''
    and (i.START_DATE(+) <= trunc(sysdate))
    and (i.END_DATE(+) is null or i.END_DATE(+) >= trunc(sysdate))
    and pm.delivery_type <> ''I''';

	IF in_product_id IS NOT NULL THEN
		v_sql := ' AND LOWER(pm.product_id) = LOWER(''' || in_product_id || ''')';
		v_main_sql := v_main_sql || v_sql;
	END IF;
	
	IF in_upsell_id IS NOT NULL THEN
		v_sql := ' AND LOWER(ud.upsell_master_id) = LOWER(''' || in_upsell_id || ''')';
		v_main_sql := v_main_sql || v_sql;
	END IF;
	
	v_main_sql := v_main_sql || 'order by ud.UPSELL_MASTER_ID, pm.NOVATOR_ID';
	
   OPEN out_cursor FOR v_main_sql;

END GET_PROD_FEED_CI_PRODUCTS;



PROCEDURE GET_PROD_FEED_CI_UPSELLS
(
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the  records in table source_master for the
   optional input input parameters.

Input:
       none
Output:
        cursor containing source_master info
------------------------------------------------------------------------------*/

v_main_sql VARCHAR2(4000);

BEGIN

   v_main_sql := 'SELECT 
    distinct um.UPSELL_MASTER_ID,
    um.UPSELL_DESCRIPTION,
    um.UPSELL_STATUS,
    um.UPSELL_NAME
FROM
    FTD_APPS.UPSELL_MASTER um,
    FTD_APPS.UPSELL_DETAIL ud,
    FTD_APPS.UPSELL_COMPANY_XREF ucx,
    FTD_APPS.PRODUCT_COMPANY_XREF pcx
WHERE
    1=1
    and ud.UPSELL_MASTER_ID = um.UPSELL_MASTER_ID
    and um.UPSELL_MASTER_ID = ucx.PRODUCT_ID
    and ud.UPSELL_DETAIL_ID = pcx.PRODUCT_ID
    and ucx.COMPANY_ID = ''FTD''
    and pcx.COMPANY_ID = ''FTD''
ORDER BY
    um.UPSELL_MASTER_ID';

		
   OPEN out_cursor FOR v_main_sql;

END GET_PROD_FEED_CI_UPSELLS;




PROCEDURE GET_DOMESTIC_SF_BY_SOURCE_CODE
(
 IN_SOURCE_CODE               IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns the domestic service fee charge for a given source code.

Input:
        in_source_code             VARCHAR2

Output:
        cursor containing first_order_domestic  info
------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cursor FOR
	  
	  	SELECT first_order_domestic 
		FROM FTD_APPS.SOURCE_MASTER sm, FTD_APPS.SNH snh
		WHERE  sm.SNH_ID = snh.SNH_ID
		AND sm.SOURCE_CODE = in_source_code;
	  
END GET_DOMESTIC_SF_BY_SOURCE_CODE;


PROCEDURE GET_IOTWS_BY_SC_AND_PROD_ID
(
 IN_SOURCE_CODE               IN VARCHAR2,
 IN_PRODUCT_ID					 IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all IOTWs for a given source code.

Input:
        in_source_code             VARCHAR2

Output:
        cursor containing IOTW  info
------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cursor FOR
	  		
		SELECT 
			i.PRODUCT_ID,
			phd.DISCOUNT_AMT,
			phd.DISCOUNT_TYPE
		FROM 
			FTD_APPS.PRODUCT_MASTER pm, 
			FTD_APPS.PRODUCT_COMPANY_XREF pcx,
			JOE.IOTW i,
			FTD_APPS.SOURCE_MASTER sm,
			FTD_APPS.PRICE_HEADER_DETAILS phd
		WHERE 
			pm.product_id = pcx.product_id
			and pm.PRODUCT_ID = i.PRODUCT_ID
			and i.IOTW_SOURCE_CODE = sm.SOURCE_CODE
			and sm.PRICE_HEADER_ID = phd.PRICE_HEADER_ID
			and pcx.company_id = 'FTD'
			and pm.NOVATOR_ID is not null
			and i.SOURCE_CODE = in_source_code
			and (i.START_DATE <= trunc(sysdate))
			and (i.END_DATE is null or i.END_DATE>= trunc(sysdate))
            and (pm.STANDARD_PRICE between phd.MIN_DOLLAR_AMT and phd.MAX_DOLLAR_AMT)
			and (pm.PRODUCT_ID = in_product_id)
		order by i.PRODUCT_ID;
		
END GET_IOTWS_BY_SC_AND_PROD_ID;

PROCEDURE GET_SKU_ID_FROM_NOVATOR_ID
(
 IN_NOVATOR_ID              IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns SKU Id for a given Novator Id.

Input:
      IN_NOVATOR_ID             VARCHAR2

Output:
      cursor containing SKU ID
------------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
	  		
		SELECT PRODUCT_ID FROM FTD_APPS.PRODUCT_MASTER WHERE NOVATOR_ID = IN_NOVATOR_ID; 
		
END GET_SKU_ID_FROM_NOVATOR_ID;

PROCEDURE GET_DISCOUNTS
(
 IN_SOURCE_CODE          IN VARCHAR2,
 IN_PRODUCT_ID              IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)

AS
/*------------------------------------------------------------------------------
Description:
   Returns discounts type and amt for a given source code and product id.

Input:
        in_source_code             VARCHAR2

Output:
        cursor containing discount  info
------------------------------------------------------------------------------*/

BEGIN

   OPEN out_cursor FOR
	  		
		SELECT 
			pm.PRODUCT_ID,
			phd.DISCOUNT_AMT,
			phd.DISCOUNT_TYPE
		FROM 
			FTD_APPS.PRODUCT_MASTER pm,
			FTD_APPS.PRODUCT_COMPANY_XREF pcx,
			FTD_APPS.SOURCE_MASTER sm,
			FTD_APPS.PRICE_HEADER_DETAILS phd
		WHERE
			pm.product_id = pcx.product_id
			and sm.PRICE_HEADER_ID = phd.PRICE_HEADER_ID
			and pcx.company_id = 'FTD'
			and pm.NOVATOR_ID is not null
			and sm.SOURCE_CODE = IN_SOURCE_CODE
            and (pm.STANDARD_PRICE between phd.MIN_DOLLAR_AMT and phd.MAX_DOLLAR_AMT)
			and (pm.PRODUCT_ID = IN_PRODUCT_ID);
		
END GET_DISCOUNTS;


PROCEDURE PROJECT_FRESH_BULK_UPDATE
(
IN_UPDATED_AFTER             IN VARCHAR2,
IN_SOURCE                    IN VARCHAR2,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
) AS

v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_exception     EXCEPTION;
v_exception_txt varchar2(200);
v_payload varchar2(2000);

CURSOR updated_prod_cur IS
    select pm.novator_id 
    from   ftd_apps.product_master pm 
    where  pm.updated_on > TO_DATE(IN_UPDATED_AFTER,'YYYY-MM-DD HH24:MI')
    union
    select nvl(a.product_id, a.addon_id) as novator_id
    from   ftd_apps.addon a
    where  a.updated_on > TO_DATE(IN_UPDATED_AFTER,'YYYY-MM-DD HH24:MI');

CURSOR updated_source_cur IS
    select sm.source_code 
    from   ftd_apps.source_master sm 
    where  sm.updated_on > TO_DATE(IN_UPDATED_AFTER,'YYYY-MM-DD HH24:MI');

CURSOR all_upsell_cur IS
    select distinct upsell_master_id 
    from   ftd_apps.upsell_master;
     
BEGIN
    IF (in_source = 'sourcecode') THEN
       -- Trigger sourcecodes that have changed
       FOR source_rec IN updated_source_cur LOOP
         v_payload := 'FRESH_MSG_PRODUCER|' || IN_SOURCE || '|source_master_all|' || source_rec.source_code;
         events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 15, v_stat, v_mess);      
         IF v_stat = 'N' THEN
            v_exception_txt := 'ERROR posting a JMS message for Fresh sourcecode = ' || source_rec.source_code || SUBSTR(v_mess,1,200);
            raise v_exception;
         END IF;        
       END LOOP;
    ELSE 
       -- Trigger products that have changed
       FOR prod_rec IN updated_prod_cur LOOP
         v_payload := 'FRESH_MSG_PRODUCER|' || IN_SOURCE || '|product_master_all|' || prod_rec.novator_id;
         events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 15, v_stat, v_mess);      
         IF v_stat = 'N' THEN
            v_exception_txt := 'ERROR posting a JMS message for Fresh product = ' || prod_rec.novator_id || SUBSTR(v_mess,1,200);
            raise v_exception;
         END IF;        
       END LOOP;
       -- Trigger all upsell masters (we trigger all to be safe since no timestamp)
       FOR upsell_rec IN all_upsell_cur LOOP
         v_payload := 'FRESH_MSG_PRODUCER|' || IN_SOURCE || '|upsell_master_all|' || upsell_rec.upsell_master_id;
         events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 15, v_stat, v_mess);      
         IF v_stat = 'N' THEN
            v_exception_txt := 'ERROR posting a JMS message for Fresh upsell master = ' || upsell_rec.upsell_master_id || SUBSTR(v_mess,1,200);
            raise v_exception;
         END IF;        
       END LOOP;
    END IF;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END PROJECT_FRESH_BULK_UPDATE;



PROCEDURE PROJECT_FRESH_ADDON_UPDATE
(
IN_ADDON_ID                  IN VARCHAR2,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
) AS

v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_exception     EXCEPTION;
v_exception_txt varchar2(200);
v_payload varchar2(2000);

CURSOR prod_addon_cur IS
    select NVL(pm.novator_id, pm.product_id) as novator_id
    from   ftd_apps.product_addon pa, ftd_apps.product_master pm
    where  pa.product_id = pm.product_id
    and    pa.active_flag = 'Y'
    and    pm.status='A'    
    and    pa.addon_id = IN_ADDON_ID;
    
     
BEGIN
    FOR prod_rec IN prod_addon_cur LOOP
      v_payload := 'FRESH_MSG_PRODUCER|product|product_master_addon_all|' || prod_rec.novator_id;
      events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 20, v_stat, v_mess);      
      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message for Fresh addon product = ' || prod_rec.novator_id || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;        
    END LOOP;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END PROJECT_FRESH_ADDON_UPDATE;


PROCEDURE PROJECT_FRESH_MISC_TRIGGERS
(
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
) AS
/*------------------------------------------------------------------------------
Description:
   Miscellaneous triggers needed for Project Fresh.
   This includes triggering sourcecodes and IOTW when they reach their start/end dates.
   Also for triggering products that have had (PIF) category changes.
------------------------------------------------------------------------------*/

v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_exception     EXCEPTION;
v_exception_txt varchar2(200);
v_payload varchar2(2000);

-- Sourcecodes and IOTW when they reach their start/end dates
--
CURSOR sourcecode_cur IS
   select source_code
   from   joe.iotw iw
   where  iw.start_date = trunc(sysdate) or iw.end_date = trunc(sysdate)
   UNION
   select source_code
   from   ftd_apps.source_master sm
   where  sm.start_date = trunc(sysdate) or sm.end_date = trunc(sysdate);

-- Catalog changes.
-- This assumes original index information is in bak tables 
-- and new info was already copied from staging to prod tables.
--
CURSOR catalog_cur IS
   -- Products that have been removed from indexes
   --
   SELECT distinct pix.product_id 
   FROM   ftd_apps.product_index_xref pix, ftd_apps.product_index pi 
   WHERE  pix.product_index_id = pi.index_id
   AND    (
           SELECT count(pixb.product_id) 
           FROM ftd_apps.product_index_bak pib,
                ftd_apps.product_index_xref_bak pixb
           WHERE  pixb.product_index_id = pib.index_id 
           AND    pixb.product_id = pix.product_id
           AND    pib.index_file_name = pi.index_file_name
          ) <> 1
   UNION
   --
   -- Products that have been added to indexes 
   --
   SELECT distinct pixb.product_id  
   FROM   ftd_apps.product_index_xref_bak pixb, ftd_apps.product_index_bak pib 
   WHERE  pixb.product_index_id = pib.index_id
   AND    (
           SELECT count(pix.product_id) 
           FROM ftd_apps.product_index pi,
                ftd_apps.product_index_xref pix
           WHERE  pix.product_index_id = pi.index_id 
           AND    pix.product_id = pixb.product_id
           AND    pi.index_file_name = pib.index_file_name
          ) <> 1;

BEGIN
    FOR sc_rec IN sourcecode_cur LOOP
      v_payload := 'FRESH_MSG_PRODUCER|promo|source_or_iotw_start_end|' || sc_rec.source_code;
      events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 20, v_stat, v_mess);      
      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message for Fresh source_code = ' || sc_rec.source_code || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;        
    END LOOP;

    FOR cat_rec IN catalog_cur LOOP
      v_payload := 'FRESH_MSG_PRODUCER|product|catalog_change|' || cat_rec.product_id;
      events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 20, v_stat, v_mess);      
      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message for Fresh catalog = ' || cat_rec.product_id || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;        
    END LOOP;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END PROJECT_FRESH_MISC_TRIGGERS;


END PRODUCT_QUERY_PKG;
.
/