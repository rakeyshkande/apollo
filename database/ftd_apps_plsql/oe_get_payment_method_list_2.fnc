CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PAYMENT_METHOD_LIST_2 ( paymentType IN VARCHAR2,
  jcPenneyFlag IN VARCHAR2,
  discoverFlag IN VARCHAR2,
  aafesFlag IN VARCHAR2)
 RETURN types.ref_cursor
--=================================================================
--
-- Name:    OE_GET_PAYMENT_METHOD_LIST
-- Type:    Function
-- Syntax:  OE_GET_PAYMENT_METHOD_LIST (paymentType IN VARCHAR2,
--                                      jcPenneyFlag IN VARCHAR2,
--                                      discoverFlag IN VARCHAR2,
--                                      aafesFlag IN VARCHAR2)
-- Returns: ref_cursor for
--          paymentMethodId   VARCHAR2(2)
--          description       VARCHAR2(25)
--          paymentType       VARCHAR2(1)
--
-- Description:   Queries the PAYMENT_METHODS table, if paymentType
--                is provided returns only pay methods of appropriate type.
--                If jcPenneyFlag is passed in 'Y', then includes
--                JC Penney card and excludes Diners Club / Carte Blanche
--                as payment types, otherwise it does not.
--
--==================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT PAYMENT_METHOD_ID as "paymentMethodId",
            DESCRIPTION as "description",
            PAYMENT_TYPE as "paymentType"
        FROM PAYMENT_METHODS
        WHERE PAYMENT_TYPE = NVL(paymentType, PAYMENT_TYPE)
        AND ( NVL(jcPenneyFlag, 'N') = 'Y'
              OR  PAYMENT_METHOD_ID != 'JP' )
        AND ( NVL(jcPenneyFlag, 'N') = 'N'
              OR  PAYMENT_METHOD_ID NOT IN ('DC', 'CB'))
        AND ( NVL(discoverFlag, 'N') = 'N'
              OR  PAYMENT_METHOD_ID = 'DI')
        AND ( PAYMENT_METHOD_ID != 'MS' OR NVL(aafesFlag, 'N') = 'Y')

        ORDER BY PAYMENT_METHOD_ID;

    RETURN cur_cursor;
END;
.
/
