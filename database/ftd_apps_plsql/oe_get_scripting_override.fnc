CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_SCRIPTING_OVERRIDE (inTriggerId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_SCRIPTING_OVERRIDE
-- Type:    Function
-- Syntax:  OE_GET_SCRIPTING_OVERRIDE (inTriggerId IN varchar2)
-- Returns: ref_cursor for
--          TRIGGER_ID        VARCHAR2(12)
--          TRIGGETR_TYPE     VARCHAR2(12)
--          PAGE_NAME         VARCHAR2(20)
--          FIELD_NAME        VARCHAR2(50)
--          SCRIPT_TEXT       VARCHAR2(500)
--          INSTRUCTION_TEXT  VARCHAR2(500)
--
--======================================================================
AS
    user_cursor types.ref_cursor;
BEGIN
    OPEN user_cursor FOR
        SELECT TRIGGER_ID       as "triggerId",
               TRIGGER_TYPE     as "triggerType",
               PAGE_NAME        as "pageName",
               FIELD_NAME       as "fieldName",
               SCRIPT_TEXT      as "scriptText",
               INSTRUCTION_TEXT as "instructionText"
          FROM SCRIPTING_OVERRIDE
		  WHERE TRIGGER_ID = UPPER(inTriggerId);

    RETURN user_cursor;
END;
.
/
