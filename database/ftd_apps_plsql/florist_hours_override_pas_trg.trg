CREATE OR REPLACE TRIGGER ftd_apps.florist_hours_override_pas_trg
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_hours_override
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE

   procedure ins_pas(in_florist       IN varchar2,
                     in_override_date IN date) is
   begin
      
      IF in_override_date >= trunc(sysdate) THEN
        IF in_override_date = trunc(sysdate) THEN
           PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', in_florist);
        ELSE
           PAS.POST_PAS_COMMAND('MODIFIED_FLORIST', in_florist || ' ' ||
               to_char(in_override_date, 'MMddyyyy') || ' ' || to_char(in_override_date, 'MMddyyyy'));
        END IF;
      END IF;
     
   end ins_pas;
   
BEGIN

   IF INSERTING THEN
      ins_pas(:new.florist_id, :new.override_date);
   ELSIF UPDATING  THEN
      -- there is no current way to update florist id or day of week but I will account for it anyway
      IF :old.florist_id <> :new.florist_id THEN
         ins_pas(:old.florist_id, :old.override_date);
         ins_pas(:new.florist_id, :new.override_date);
      ELSIF :old.day_of_week <> :new.day_of_week THEN
         ins_pas(:old.florist_id, :old.override_date);
         ins_pas(:new.florist_id, :new.override_date);
      ELSIF :old.override_date <> :new.override_date THEN
         ins_pas(:new.florist_id, :new.override_date);
      END IF;
   ELSIF DELETING  THEN
      ins_pas(:old.florist_id, :old.override_date);
   END IF;
   
END;
/
