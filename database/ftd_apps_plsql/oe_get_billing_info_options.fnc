CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_BILLING_INFO_OPTIONS ( sourceCode  IN VARCHAR2 )
RETURN Types.ref_cursor

--==============================================================================
--
-- Name:    OE_GET_BILLING_INFO_OPTIONS
-- Type:    Function
-- Syntax:  OE_GET_BILLING_INFO_OPTIONS ( sourceCode  IN VARCHAR2 )
--
-- Description:   Queries the OE_GET_BILLING_INFO_OPTIONS table and returns all
--				  rows for the given source code.
--
--==============================================================================
AS
    cur_cursor Types.ref_cursor;

BEGIN
    OPEN cur_cursor FOR
        SELECT BILLING_INFO_DESC AS "billingInfoDesc",
              OPTION_VALUE AS "optionValue",
			  OPTION_NAME AS "optionDisplay",
			  OPTION_SEQUENCE AS "optionSequence"
        FROM BILLING_INFO_OPTIONS
        WHERE SOURCE_CODE_ID = UPPER(sourceCode)
        ORDER BY BILLING_INFO_DESC, OPTION_SEQUENCE;
    RETURN cur_cursor;
END;
.
/
