CREATE OR REPLACE
PACKAGE BODY ftd_apps.MISC_PKG AS


PROCEDURE INSERT_CSR_VIEWED_IDS
(
IN_CSR_ID                       IN CSR_VIEWED_IDS.CSR_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_VIEWED_IDS.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_VIEWED_IDS.ENTITY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the history of viewed
        record entities

Input:
        csr_id                          varchar2
        entity_type                     varchar2 (FLORIST_MASTER, MERCURY)
        entity_id                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR  count_cur IS
        SELECT  count (1), min(created_date), max (decode (entity_id, in_entity_id, 'Y', 'N')) exists_flag
        FROM    csr_viewed_ids
        WHERE   csr_id = in_csr_id
        AND     entity_type = in_entity_type;

v_count         number := 0;
v_created_date  date;
v_exists        varchar2(1);

BEGIN

OPEN count_cur;
FETCH count_cur INTO v_count, v_created_date, v_exists;
CLOSE count_cur;

CASE
WHEN v_exists = 'Y' THEN
        -- updated the history record with the current date if the csr previously viewed the record
        UPDATE  csr_viewed_ids
        SET     created_date = sysdate
        WHERE   csr_id = in_csr_id
        AND     entity_type = in_entity_type
        AND     entity_id = in_entity_id;

WHEN (in_entity_type = 'FLORIST_MASTER' AND v_count < 50) OR
     (in_entity_type = 'MERCURY') THEN
        -- insert up to 50 history records for florist records
        -- but no limit for mercury records
        INSERT INTO csr_viewed_ids
        (
                csr_id,
                created_date,
                entity_type,
                entity_id
        )
        VALUES
        (
                in_csr_id,
                sysdate,
                in_entity_type,
                in_entity_id
        );

ELSE
        -- if the number of history records has reached 50 and the entity is new
        -- update the oldest record with the new history
        UPDATE  csr_viewed_ids
        SET     created_date = sysdate,
                entity_id = in_entity_id
        WHERE   csr_id = in_csr_id
        AND     entity_type = in_entity_type
        AND     created_date = v_created_date;
END CASE;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CSR_VIEWED_IDS;


PROCEDURE DELETE_CSR_VIEWED_IDS
(
IN_CSR_ID                       IN CSR_VIEWED_IDS.CSR_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_VIEWED_IDS.ENTITY_TYPE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the history of viewed
        record entities for the given csr and entity type

Input:
        csr_id                          varchar2
        entity_type                     varchar2 (FLORIST_MASTER, MERCURY)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM csr_viewed_ids
WHERE   csr_id = in_csr_id
AND     entity_type = in_entity_type;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_CSR_VIEWED_IDS;


PROCEDURE VIEW_CSR_VIEWED_IDS
(
IN_CSR_ID                       IN CSR_VIEWED_IDS.CSR_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_VIEWED_IDS.ENTITY_TYPE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for

Input:
        csr_id                          varchar2
        entity_type                     varchar2

Output:
        cursor containing all records from csr_viewed_ids

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                csr_id,
                created_date,
                entity_type,
                entity_id
        FROM    csr_viewed_ids
        ORDER BY created_date desc;

END VIEW_CSR_VIEWED_IDS;


PROCEDURE INSERT_HP_ACTIONS
(
IN_TABLE_NAME                   IN HP_ACTIONS.TABLE_NAME%TYPE,
IN_TABLE_KEY                    IN HP_ACTIONS.TABLE_KEY%TYPE,
IN_SUBSET                       IN HP_ACTIONS.SUBSET%TYPE,
IN_ACTION                       IN HP_ACTIONS.ACTION%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for insering an update action that
        the HP will pick up to update the HP files.

Input:
        table_name                      varchar2
        table_key                       varchar2
        subset                          varchar2
        action                          varchar2
        created_date                    date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_insert_record boolean := true;
v_active        char (1);

cursor inactive_cur (p_florist_id varchar2) is
        select  'Y'
        from    florist_master
        where   florist_id = p_florist_id
        and     status = 'Inactive';

BEGIN

-- CR 1/2005 - check if the given florist is currently inactive
-- if so do not insert the record.
IF in_table_name in ('FLORIST_BLOCKS', 'FLORIST_ZIPS', 'FLORIST_CODIFICATIONS') OR
   (in_table_name = 'FLORIST_MASTER' and in_subset in ('LOAD_WEIGHT', 'BRANCH_LINKING')) THEN

        open inactive_cur (substr (in_table_key, 1, 9));
        fetch inactive_cur into v_active;
        IF inactive_cur%found THEN
                v_insert_record := false;
        END IF;
        close inactive_cur;

END IF;

IF v_insert_record THEN
        INSERT INTO hp_actions
        (
                hp_update_id,
                table_name,
                table_key,
                subset,
                action,
                created_date
        )
        VALUES
        (
                hp_actions_id_sq.nextval,
                in_table_name,
                in_table_key,
                in_subset,
                in_action,
                sysdate
        );
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_HP_ACTIONS;


PROCEDURE UPDATE_GNADD_LEVEL
(
IN_GNADD_LEVEL                  IN GLOBAL_PARMS.GNADD_LEVEL%TYPE,
IN_UPDATED_BY						  IN FRP.GLOBAL_PARMS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the gnadd level to the
        given value and starting the available zip/product process for
        all covered zip codes.

Input:
        gnadd_level                     number
        updated_by							 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

FRP.MISC_PKG.UPDATE_GLOBAL_PARMS('FTDAPPS_PARMS','GNADD_LEVEL',in_gnadd_level, UPPER(IN_UPDATED_BY), OUT_STATUS, OUT_MESSAGE);

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_GNADD_LEVEL;


PROCEDURE UPDATE_GNADD_DATE
(
IN_GNADD_DATE                   IN GLOBAL_PARMS.GNADD_DATE%TYPE,
IN_UPDATED_BY						  IN FRP.GLOBAL_PARMS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the gnadd date to the
        given value.

Input:
        gnadd_level                     number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

FRP.MISC_PKG.UPDATE_GLOBAL_PARMS('FTDAPPS_PARMS','GNADD_DATE',to_char(in_gnadd_date, 'Month DD, YYYY'), UPPER(IN_UPDATED_BY), OUT_STATUS, OUT_MESSAGE);

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_GNADD_DATE;


FUNCTION GET_EFOS_CITY_STATE_CODE
(
 IN_DELIVERY_CITY  IN VARCHAR2,
 IN_DELIVERY_STATE IN VARCHAR2
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from view efos_city_state_vw
        for a particular city and state.

Input:
        city name
        state id

Output:
        city_state_code

-----------------------------------------------------------------------------*/
v_city_state_code VARCHAR2(6);

cursor ecs_cur is
    SELECT code
      FROM efos_city_state_vw
     WHERE TRIM(state) = UPPER(TRIM(in_delivery_state))
       AND TRIM(city)  = UPPER(TRIM(in_delivery_city))
     ORDER BY code desc;
BEGIN

  open ecs_cur;
  fetch ecs_cur into v_city_state_code;
  close ecs_cur;

  RETURN v_city_state_code;

END GET_EFOS_CITY_STATE_CODE;


FUNCTION CHANGE_TIMES_TIMEZONE
(
 IN_TIME      IN VARCHAR2,
 IN_TIME_ZONE IN VARCHAR2,
 IN_STATE_ID  IN VARCHAR2
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure converts a time to the central timezone of the
        bassed on the state passed in.

Input:
        time, state id, range begin date, range end date

Output:
        adjusted time - VARCHAR2

-----------------------------------------------------------------------------*/

v_adjusted_time VARCHAR2(4);

BEGIN

  IF in_time IS NULL THEN

     v_adjusted_time := NULL;

  ELSE

     CASE TRIM(in_time_zone)
       WHEN '1' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) - 100),'0000'));
       WHEN '2' THEN
            v_adjusted_time := in_time;
       WHEN '3' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) + 100),'0000'));
       WHEN '4' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) + 200),'0000'));
       WHEN '5' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) + 300),'0000'));
       WHEN '6' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) + 400),'0000'));
       ELSE
            v_adjusted_time := NULL;
     END CASE;

  END IF;

  RETURN v_adjusted_time;

END CHANGE_TIMES_TIMEZONE;

FUNCTION CHANGE_TIMES_TO_REC_TIMEZONE
(
 IN_TIME      IN VARCHAR2,
 IN_TIME_ZONE IN VARCHAR2,
 IN_STATE_ID  IN VARCHAR2
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure converts a time to the timezone of the
        recipient state passed in.

Input:
        time, state id, range begin date, range end date

Output:
        adjusted time - VARCHAR2

-----------------------------------------------------------------------------*/

v_adjusted_time VARCHAR2(4);

BEGIN

  IF in_time IS NULL THEN

     v_adjusted_time := NULL;

  ELSE

     CASE TRIM(in_time_zone)
       WHEN '1' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) + 100),'0000'));
       WHEN '2' THEN
            v_adjusted_time := in_time;
       WHEN '3' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) - 100),'0000'));
       WHEN '4' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) - 200),'0000'));
       WHEN '5' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) - 300),'0000'));
       WHEN '6' THEN
            v_adjusted_time := TRIM(TO_CHAR((TO_NUMBER(in_time) - 400),'0000'));
       ELSE
            v_adjusted_time := NULL;
     END CASE;

  END IF;

  RETURN v_adjusted_time;

END CHANGE_TIMES_TO_REC_TIMEZONE;

FUNCTION IS_IT_WITHIN_BLOCKED_DATES
(
 IN_BLOCK_START_DATE  IN DATE,
 IN_BLOCK_END_DATE    IN DATE,
 IN_ADJUST_END_DAYS   IN NUMBER,
 IN_RANGE_BEGIN_DATE  IN DATE,
 IN_RANGE_END_DATE    IN DATE
)
RETURN CHAR
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure determines if range date(s) are within the block
        dates. However if the end days is to be adjusted adjust it by the
        number of days passed in.

        Note, two assumptions are being made 1) in_range_begin_date is not
        NULL and 2) in_range_begin_date is less than in_range_end_date.

Input:
        block start date   DATE
        block end date     DATE
        adjust_end_days    NUMBER
        range begin date   DATE
        range end date     DATE

Output:
        v_blocked - CHAR

-----------------------------------------------------------------------------*/

v_blocked        CHAR(1);
v_block_end_date DATE;

BEGIN

  IF (in_block_start_date IS NULL) AND (in_block_end_date IS NULL) THEN
     v_blocked := 'N';
  ELSE

    IF (in_adjust_end_days <> 0) AND (in_block_end_date IS NOT NULL) THEN
       v_block_end_date := in_block_end_date + in_adjust_end_days;
    ELSE
       v_block_end_date := in_block_end_date;
    END IF;

    IF in_range_end_date IS NULL THEN
      IF    (in_block_start_date IS NULL) AND (TRUNC(in_range_begin_date) <= TRUNC(v_block_end_date)) THEN
               v_blocked := 'Y';
      ELSIF (TRUNC(in_block_start_date) <= TRUNC(in_range_begin_date)) AND (v_block_end_date IS NULL) THEN
               v_blocked := 'Y';
      ELSIF (TRUNC(in_range_begin_date) BETWEEN TRUNC(in_block_start_date) AND TRUNC(v_block_end_date)) THEN
               v_blocked := 'Y';
      ELSE
               v_blocked := 'N';
      END IF;

    ELSE
      IF (TRUNC(in_range_begin_date) BETWEEN TRUNC(in_block_start_date) AND TRUNC(v_block_end_date))
               AND
              (TRUNC(in_range_end_date) BETWEEN TRUNC(in_block_start_date) AND TRUNC(v_block_end_date)) THEN

               --the range dates fall on or within the blocked dates
               v_blocked := 'Y';

      ELSIF (in_block_start_date IS NULL)
               AND
              (TRUNC(in_range_end_date) <= TRUNC(v_block_end_date)) THEN

               --the range dates fall on or within the blocked dates
               v_blocked := 'Y';

      ELSIF (TRUNC(in_range_begin_date) >= TRUNC(in_block_start_date))
               AND
              (v_block_end_date IS NULL) THEN

               --the range dates fall on or within the blocked dates
               v_blocked := 'Y';
      ELSE
               v_blocked := 'N';
      END IF;

    END IF;
  END IF;

  RETURN v_blocked;

END IS_IT_WITHIN_BLOCKED_DATES;


PROCEDURE GET_PRODUCT_SECOND_CHOICE
(
 IN_PRODUCT_SECOND_CHOICE_ID   IN PRODUCT_SECOND_CHOICE.PRODUCT_SECOND_CHOICE_ID%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a record from table product_second_choice
         for the given product_second_choice_id.

Input:
        product_second_choice_id

Output:
        cursor containing product_second_choice info
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
     SELECT product_second_choice_id,
            oe_description1,
            oe_description2,
            mercury_description1,
            mercury_description2,
            oe_holiday_description,
            oe_mercury_description
       FROM product_second_choice
      WHERE product_second_choice_id = in_product_second_choice_id;

END GET_PRODUCT_SECOND_CHOICE;


PROCEDURE GET_PARTNER_CONFIG_FIELDS
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns pcard configuration and partner pcard
        configuration reference records related to each other..

Input:
        N/A

Output:
        out_cursor      REF_CURSOR

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT program_name,
           pcard_config_id,
           field_name,
           sort_order,
           source_table,
           source_field,
           field_prefix,
           field_suffix,
           currency_decimal_ind
      FROM pcard_config pc
     ORDER BY program_name,
              sort_order;


END GET_PARTNER_CONFIG_FIELDS;


FUNCTION RETRIEVE_ACTIVE_COUNTRY_NAME
(
 IN_COUNTRY_ID    IN COUNTRY_MASTER.COUNTRY_ID%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the active country name for the
        country id passed in, howeve NULL is returned if one is not
        found or if more than one name is active.

Input:
        country_id              VARCHAR2

Output:
        varchar2

-----------------------------------------------------------------------------*/
v_name VARCHAR2(50);

BEGIN

  BEGIN
    SELECT name
      INTO v_name
      FROM country_master
     WHERE country_id = in_country_id
       AND status = 'Active';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN v_name := NULL;
      WHEN TOO_MANY_ROWS THEN v_name := NULL;
  END;

  RETURN v_name;

END RETRIEVE_ACTIVE_COUNTRY_NAME;


PROCEDURE INSERT_PRODUCT_SECOND_CHOICE
(
 IN_PRODUCT_SECOND_CHOICE_ID  IN PRODUCT_SECOND_CHOICE.PRODUCT_SECOND_CHOICE_ID%TYPE,
 IN_OE_DESCRIPTION            IN VARCHAR2,
 IN_MERCURY_DESCRIPTION       IN VARCHAR2,
 IN_OE_HOLIDAY_DESCRIPTION    IN PRODUCT_SECOND_CHOICE.OE_HOLIDAY_DESCRIPTION%TYPE,
 IN_OE_MERCURY_DESCRIPTION    IN PRODUCT_SECOND_CHOICE.OE_MERCURY_DESCRIPTION%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table product_second_choice.

Input:
        product_second_choice_id  VARCHAR2
        oe_description            VARCHAR2
        mercury_description       VARCHAR2
        oe_holiday_description    VARCHAR2
        oe_mercury_desription     VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

v_oe_description1      product_second_choice.oe_description1%TYPE;
v_oe_description2      product_second_choice.oe_description2%TYPE;
v_mercury_description1 product_second_choice.mercury_description1%TYPE;
v_mercury_description2 product_second_choice.mercury_description2%TYPE;

BEGIN

  v_oe_description1      := SUBSTR(in_oe_description,1,60);
  v_oe_description2      := SUBSTR(in_oe_description,61);
  v_mercury_description1 := SUBSTR(in_mercury_description,1,60);
  v_mercury_description2 := SUBSTR(in_mercury_description,61);


  INSERT INTO product_second_choice
     (product_second_choice_id,
      oe_description1,
      oe_description2,
      mercury_description1,
      mercury_description2,
      oe_holiday_description,
      oe_mercury_description)
    VALUES
      (in_product_second_choice_id,
       v_oe_description1,
       v_oe_description2,
       v_mercury_description1,
       v_mercury_description2,
       in_oe_holiday_description,
       in_oe_mercury_description);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_PRODUCT_SECOND_CHOICE;


PROCEDURE UPDATE_PRODUCT_SECOND_CHOICE
(
 IN_PRODUCT_SECOND_CHOICE_ID  IN PRODUCT_SECOND_CHOICE.PRODUCT_SECOND_CHOICE_ID%TYPE,
 IN_OE_DESCRIPTION            IN VARCHAR2,
 IN_MERCURY_DESCRIPTION       IN VARCHAR2,
 IN_OE_HOLIDAY_DESCRIPTION    IN PRODUCT_SECOND_CHOICE.OE_HOLIDAY_DESCRIPTION%TYPE,
 IN_OE_MERCURY_DESCRIPTION    IN PRODUCT_SECOND_CHOICE.OE_MERCURY_DESCRIPTION%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record into table product_second_choice.

Input:
        product_second_choice_id  VARCHAR2
        oe_description            VARCHAR2
        mercury_description       VARCHAR2
        oe_holiday_description    VARCHAR2
        oe_mercury_desription     VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

v_oe_description1      product_second_choice.oe_description1%TYPE;
v_oe_description2      product_second_choice.oe_description2%TYPE;
v_mercury_description1 product_second_choice.mercury_description1%TYPE;
v_mercury_description2 product_second_choice.mercury_description2%TYPE;

BEGIN

  v_oe_description1      := SUBSTR(in_oe_description,1,60);
  v_oe_description2      := SUBSTR(in_oe_description,61);
  v_mercury_description1 := SUBSTR(in_mercury_description,1,60);
  v_mercury_description2 := SUBSTR(in_mercury_description,61);


  UPDATE product_second_choice
     SET oe_description1        = v_oe_description1,
         oe_description2        = v_oe_description2,
         mercury_description1   = v_mercury_description1,
         mercury_description2   = v_mercury_description2,
         oe_holiday_description = in_oe_holiday_description,
         oe_mercury_description = in_oe_mercury_description
   WHERE product_second_choice_id = in_product_second_choice_id;

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_error_message := 'WARNING: No product_second_choice updated for product_second_choice_id ' || in_product_second_choice_id ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PRODUCT_SECOND_CHOICE;


PROCEDURE GET_ALL_PROD_SECOND_CHOICES
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the rows from the second_choice table.

Input:
        N/A

Output:
        cursor containing second_choice info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT product_second_choice_id,
            oe_description1 || oe_description2 "oe_description",
            mercury_description1 || mercury_description2 "mercury_description",
            oe_holiday_description,
            oe_mercury_description
       FROM product_second_choice;

END GET_ALL_PROD_SECOND_CHOICES;


PROCEDURE INSERT_OCCASION
(
 IN_OCCASION_ID     IN OCCASION.OCCASION_ID%TYPE,
 IN_DESCRIPTION     IN OCCASION.DESCRIPTION%TYPE,
 IN_DISPLAY_ORDER   IN OCCASION.DISPLAY_ORDER%TYPE,
 IN_ACTIVE          IN OCCASION.ACTIVE%TYPE,
 IN_USER_EDITABLE   IN OCCASION.USER_EDITABLE%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table occasion.

Input:
        occasion_id      VARCHAR2
        description      VARCHAR2
        display_order    VARCHAR2
        active           CHAR
        user_editable    CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO occasion
     (occasion_id,
      description,
      index_id,
      display_order,
      active,
      user_editable)
    VALUES
      (in_occasion_id,
       in_description,
       NULL,
       in_display_order,
       in_active,
       in_user_editable);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_OCCASION;


PROCEDURE UPDATE_OCCASION
(
 IN_OCCASION_ID     IN OCCASION.OCCASION_ID%TYPE,
 IN_DESCRIPTION     IN OCCASION.DESCRIPTION%TYPE,
 IN_DISPLAY_ORDER   IN OCCASION.DISPLAY_ORDER%TYPE,
 IN_ACTIVE          IN OCCASION.ACTIVE%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record in table occasion.

Input:
        occasion_id      VARCHAR2
        description      VARCHAR2
        display_order    VARCHAR2
        active           CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  UPDATE occasion
     SET description = in_description,
         display_order = in_display_order,
         active = in_active
   WHERE occasion_id = in_occasion_id;

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_error_message := 'WARNING: No occasion updated for occasion_id ' || in_occasion_id ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_OCCASION;


PROCEDURE UPDATE_INSERT_CPC_INFO
(
 IN_SOURCE_CODE_ID              IN CPC_INFO.SOURCE_CODE_ID%TYPE,
 IN_CREDIT_CARD_TYPE            IN CPC_INFO.CREDIT_CARD_TYPE%TYPE,
 IN_CREDIT_CARD_NUMBER          IN CPC_INFO.CREDIT_CARD_NUMBER%TYPE,
 IN_CREDIT_CARD_EXPIRATION_DATE IN CPC_INFO.CREDIT_CARD_EXPIRATION_DATE%TYPE,
 OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE                   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the cpc_info table.

Input:
        source_code_id              VARCHAR2
        credit_card_type            VARCHAR2
        credit_card_number          VARCHAR2
        credit_card_expiration_date VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

  v_key_name cpc_info.key_name%type;

BEGIN

  v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

  UPDATE cpc_info
     SET credit_card_type            = in_credit_card_type,
         credit_card_number          = global.encryption.encrypt_it(in_credit_card_number,v_key_name),
         key_name                    = v_key_name,
         credit_card_expiration_date = in_credit_card_expiration_date
   WHERE source_code_id = in_source_code_id;

  IF SQL%NOTFOUND THEN
    INSERT INTO cpc_info
       (source_code_id,
        credit_card_type,
        credit_card_number,
        key_name,
        credit_card_expiration_date)
      VALUES
       (in_source_code_id,
        in_credit_card_type,
        global.encryption.encrypt_it(in_credit_card_number,v_key_name),
        v_key_name,
        in_credit_card_expiration_date);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_CPC_INFO;


PROCEDURE GET_CPC_INFO
(
 IN_SOURCE_CODE_ID      IN CPC_INFO.SOURCE_CODE_ID%TYPE,
 OUT_CURSOR            OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        The procedure retrives all records from table cpc_info for the
        source_code_id passed in.

Input:
        source_code_id VARCHAR2

Output:
        cursor containing cpc_info records

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
       SELECT source_code_id,
              credit_card_type,
              global.encryption.decrypt_it(credit_card_number,key_name) credit_card_number,
              credit_card_expiration_date
         FROM cpc_info
        WHERE source_code_id = in_source_code_id;



END GET_CPC_INFO;


PROCEDURE UPDATE_INSERT_AAA_MEMBERS
(
 IN_AAA_MEMBER_ID   IN AAA_MEMBERS.AAA_MEMBER_ID%TYPE,
 IN_MEMBER_NAME     IN AAA_MEMBERS.MEMBER_NAME%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts/updates a record in table aaa_members.

Input:
        aaa_member_id  NUMBER
        member_name    VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE aaa_members
     SET member_name = in_member_name
   WHERE aaa_member_id = in_aaa_member_id;

  IF SQL%NOTFOUND THEN
    INSERT INTO aaa_members
             (aaa_member_id,
              member_name)
           VALUES
             (in_aaa_member_id,
              in_member_name);
  END IF;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_INSERT_AAA_MEMBERS;


PROCEDURE INSERT_COST_CENTERS
(
 IN_COST_CENTER_ID  IN COST_CENTERS.COST_CENTER_ID%TYPE,
 IN_FILLER          IN COST_CENTERS.FILLER%TYPE,
 IN_PARTNER_ID      IN COST_CENTERS.PARTNER_ID%TYPE,
 IN_SOURCE_CODE_ID  IN COST_CENTERS.SOURCE_CODE_ID%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table cost_centers.

Input:
        cost_center_id VARCHAR2
        filler         VARCHAR2
        partner_id     VARCHAR2
        source_code_id VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO cost_centers
     (cost_center_id,
      filler,
      partner_id,
      source_code_id)
    VALUES
      (in_cost_center_id,
       in_filler,
       in_partner_id,
       in_source_code_id);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_COST_CENTERS;


PROCEDURE GET_GIFT_MESSAGES (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all gift message codes and descriptions
------------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
        SELECT GIFT_MESSAGE_ID,
            MESSAGE_TEXT
        FROM FTD_APPS.GIFT_MESSAGES
        ORDER BY GIFT_MESSAGE_ID;
END GET_GIFT_MESSAGES;


PROCEDURE TRUNCATE_BUSINESS
(
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure truncates the business table.

Input:
        N/A

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  execute immediate('TRUNCATE TABLE business');

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END TRUNCATE_BUSINESS;


PROCEDURE UPDATE_PARTNER_BANK
(
 IN_PARTNER_BANK_ID IN PARTNER_BANK.PARTNER_BANK_ID%TYPE,
 IN_AMOUNT          IN NUMBER,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record in table partner_bank.

Input:
        occasion_id      VARCHAR2
        description      VARCHAR2
        display_order    VARCHAR2
        active           CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE partner_bank
     SET balance = balance - in_amount
   WHERE partner_bank_id = in_partner_bank_id;

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_error_message := 'WARNING: No partner_bank updated for partner_bank_id ' || in_partner_bank_id ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PARTNER_BANK;


PROCEDURE INSERT_INV_CTRL_NOTIFICATIONS
(
 IN_PRODUCT_ID      IN INV_CTRL_NOTIFICATIONS.PRODUCT_ID%TYPE,
 IN_VENDOR_ID       IN INV_CTRL_NOTIFICATIONS.PRODUCT_ID%TYPE,
 IN_EMAIL_ADDRESS   IN INV_CTRL_NOTIFICATIONS.EMAIL_ADDRESS%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table inv_ctrl_notifications.

Input:
        product_id    VARCHAR2
        email_address VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO inv_ctrl_notifications
     (product_id,
      VENDOR_ID,
      email_address)
    VALUES
      (in_product_id,
       in_vendor_id,
       in_email_address);

  out_status := 'Y';

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
       out_status := 'Y';
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_INV_CTRL_NOTIFICATIONS;


PROCEDURE DELETE_ALL_INV_CTRL_NOTIFIS
(
 IN_PRODUCT_ID   IN INV_CTRL_NOTIFICATIONS.PRODUCT_ID%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure deletes every row in table inv_ctrl_notifications
   for the product_id passed in.

Input:
        product_id    VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

  DELETE inv_ctrl_notifications
   WHERE product_id = in_product_id;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ALL_INV_CTRL_NOTIFIS;


PROCEDURE INSERT_COUNTRY_MASTER
(
 IN_COUNTRY_ID          IN COUNTRY_MASTER.COUNTRY_ID%TYPE,
 IN_NAME                IN COUNTRY_MASTER.NAME%TYPE,
 IN_OE_COUNTRY_TYPE     IN COUNTRY_MASTER.OE_COUNTRY_TYPE%TYPE,
 IN_OE_DISPLAY_ORDER    IN COUNTRY_MASTER.OE_DISPLAY_ORDER%TYPE,
 IN_ADD_ON_DAYS         IN COUNTRY_MASTER.ADD_ON_DAYS%TYPE,
 IN_CUTOFF_TIME         IN COUNTRY_MASTER.CUTOFF_TIME%TYPE,
 IN_MONDAY_CLOSED       IN COUNTRY_MASTER.MONDAY_CLOSED%TYPE,
 IN_TUESDAY_CLOSED      IN COUNTRY_MASTER.TUESDAY_CLOSED%TYPE,
 IN_WEDNESDAY_CLOSED    IN COUNTRY_MASTER.WEDNESDAY_CLOSED%TYPE,
 IN_THURSDAY_CLOSED     IN COUNTRY_MASTER.THURSDAY_CLOSED%TYPE,
 IN_FRIDAY_CLOSED       IN COUNTRY_MASTER.FRIDAY_CLOSED%TYPE,
 IN_SATURDAY_CLOSED     IN COUNTRY_MASTER.SATURDAY_CLOSED%TYPE,
 IN_SUNDAY_CLOSED       IN COUNTRY_MASTER.SUNDAY_CLOSED%TYPE,
 IN_THREE_CHARACTER_ID  IN COUNTRY_MASTER.THREE_CHARACTER_ID%TYPE,
 IN_STATUS              IN COUNTRY_MASTER.STATUS%TYPE,
 IN_MONDAY_TRANSIT_FLAG    IN COUNTRY_MASTER.MONDAY_TRANSIT_FLAG%TYPE,
 IN_TUESDAY_TRANSIT_FLAG   IN COUNTRY_MASTER.TUESDAY_TRANSIT_FLAG%TYPE,
 IN_WEDNESDAY_TRANSIT_FLAG IN COUNTRY_MASTER.WEDNESDAY_TRANSIT_FLAG%TYPE,
 IN_THURSDAY_TRANSIT_FLAG  IN COUNTRY_MASTER.THURSDAY_TRANSIT_FLAG%TYPE,
 IN_FRIDAY_TRANSIT_FLAG    IN COUNTRY_MASTER.FRIDAY_TRANSIT_FLAG%TYPE,
 IN_SATURDAY_TRANSIT_FLAG  IN COUNTRY_MASTER.SATURDAY_TRANSIT_FLAG%TYPE,
 IN_SUNDAY_TRANSIT_FLAG    IN COUNTRY_MASTER.SUNDAY_TRANSIT_FLAG%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_ERROR_MESSAGE     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table country_master.

Input:
        product_id         VARCHAR2
        name               VARCHAR2
        oe_country_type    VARCHAR2
        oe_display_order   NUMBER
        add_on_days        NUMBER
        cutoff_time        NUMBER
        monday_closed      CHAR
        tuesday_closed     CHAR
        wednesday_closed   CHAR
        thursday_closed    CHAR
        friday_closed      CHAR
        saturday_closed    CHAR
        sunday_closed      CHAR
        three_character_id VARCHAR2
        status             VARCHAR2
        monday_transit_flag      CHAR
        tuesday_transit_flag     CHAR
        wednesday_transit_flag   CHAR
        thursday_transit_flag    CHAR
        friday_transit_flag      CHAR
        saturday_transit_flag    CHAR
        sunday_transit_flag      CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO country_master
     (country_id,
      name,
      oe_country_type,
      oe_display_order,
      add_on_days,
      cutoff_time,
      monday_closed,
      tuesday_closed,
      wednesday_closed,
      thursday_closed,
      friday_closed,
      saturday_closed,
      sunday_closed,
      three_character_id,
      status,
      monday_transit_flag,
      tuesday_transit_flag,
      wednesday_transit_flag,
      thursday_transit_flag,
      friday_transit_flag,
      saturday_transit_flag,
      sunday_transit_flag)
    VALUES
      (in_country_id,
       in_name,
       in_oe_country_type,
       in_oe_display_order,
       in_add_on_days,
       in_cutoff_time,
       in_monday_closed,
       in_tuesday_closed,
       in_wednesday_closed,
       in_thursday_closed,
       in_friday_closed,
       in_saturday_closed,
       in_sunday_closed,
       in_three_character_id,
       in_status,
       in_monday_transit_flag,
       in_tuesday_transit_flag,
       in_wednesday_transit_flag,
       in_thursday_transit_flag,
       in_friday_transit_flag,
       in_saturday_transit_flag,
       in_sunday_transit_flag);

  PAS.POST_PAS_COMMAND('MODIFIED_COUNTRY',in_country_id);  

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_COUNTRY_MASTER;


PROCEDURE UPDATE_COUNTRY_MASTER
(
 IN_COUNTRY_ID          IN COUNTRY_MASTER.COUNTRY_ID%TYPE,
 IN_NAME                IN COUNTRY_MASTER.NAME%TYPE,
 IN_OE_COUNTRY_TYPE     IN COUNTRY_MASTER.OE_COUNTRY_TYPE%TYPE,
 IN_OE_DISPLAY_ORDER    IN COUNTRY_MASTER.OE_DISPLAY_ORDER%TYPE,
 IN_ADD_ON_DAYS         IN COUNTRY_MASTER.ADD_ON_DAYS%TYPE,
 IN_CUTOFF_TIME         IN COUNTRY_MASTER.CUTOFF_TIME%TYPE,
 IN_MONDAY_CLOSED       IN COUNTRY_MASTER.MONDAY_CLOSED%TYPE,
 IN_TUESDAY_CLOSED      IN COUNTRY_MASTER.TUESDAY_CLOSED%TYPE,
 IN_WEDNESDAY_CLOSED    IN COUNTRY_MASTER.WEDNESDAY_CLOSED%TYPE,
 IN_THURSDAY_CLOSED     IN COUNTRY_MASTER.THURSDAY_CLOSED%TYPE,
 IN_FRIDAY_CLOSED       IN COUNTRY_MASTER.FRIDAY_CLOSED%TYPE,
 IN_SATURDAY_CLOSED     IN COUNTRY_MASTER.SATURDAY_CLOSED%TYPE,
 IN_SUNDAY_CLOSED       IN COUNTRY_MASTER.SUNDAY_CLOSED%TYPE,
 IN_THREE_CHARACTER_ID  IN COUNTRY_MASTER.THREE_CHARACTER_ID%TYPE,
 IN_STATUS              IN COUNTRY_MASTER.STATUS%TYPE,
 IN_MONDAY_TRANSIT_FLAG    IN COUNTRY_MASTER.MONDAY_TRANSIT_FLAG%TYPE,
 IN_TUESDAY_TRANSIT_FLAG   IN COUNTRY_MASTER.TUESDAY_TRANSIT_FLAG%TYPE,
 IN_WEDNESDAY_TRANSIT_FLAG IN COUNTRY_MASTER.WEDNESDAY_TRANSIT_FLAG%TYPE,
 IN_THURSDAY_TRANSIT_FLAG  IN COUNTRY_MASTER.THURSDAY_TRANSIT_FLAG%TYPE,
 IN_FRIDAY_TRANSIT_FLAG    IN COUNTRY_MASTER.FRIDAY_TRANSIT_FLAG%TYPE,
 IN_SATURDAY_TRANSIT_FLAG  IN COUNTRY_MASTER.SATURDAY_TRANSIT_FLAG%TYPE,
 IN_SUNDAY_TRANSIT_FLAG    IN COUNTRY_MASTER.SUNDAY_TRANSIT_FLAG%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_ERROR_MESSAGE     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record in table country_master
        for the name passed in.

Input:
        product_id         VARCHAR2
        name               VARCHAR2
        oe_country_type    VARCHAR2
        oe_display_order   NUMBER
        add_on_days        NUMBER
        cutoff_time        NUMBER
        monday_closed      CHAR
        tuesday_closed     CHAR
        wednesday_closed   CHAR
        thursday_closed    CHAR
        friday_closed      CHAR
        saturday_closed    CHAR
        sunday_closed      CHAR
        monday_transit_flag    CHAR
        tuesday_transit_flag   CHAR
        wednesday_transit_flag CHAR
        thursday_transit_flag  CHAR
        friday_transit_flag    CHAR
        saturday_transit_flag  CHAR
        sunday_transit_flag    CHAR
        three_character_id VARCHAR2
        status             VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  UPDATE country_master
     SET country_id = in_country_id,
         oe_country_type    = in_oe_country_type,
         oe_display_order   = in_oe_display_order,
         add_on_days        = in_add_on_days,
         cutoff_time        = in_cutoff_time,
         monday_closed      = in_monday_closed,
         tuesday_closed     = in_tuesday_closed,
         wednesday_closed   = in_wednesday_closed,
         thursday_closed    = in_thursday_closed,
         friday_closed      = in_friday_closed,
         saturday_closed    = in_saturday_closed,
         sunday_closed      = in_sunday_closed,
         three_character_id = in_three_character_id,
         status             = in_status,
         monday_transit_flag      = in_monday_transit_flag,
         tuesday_transit_flag     = in_tuesday_transit_flag,
         wednesday_transit_flag   = in_wednesday_transit_flag,
         thursday_transit_flag    = in_thursday_transit_flag,
         friday_transit_flag      = in_friday_transit_flag,
         saturday_transit_flag    = in_saturday_transit_flag,
         sunday_transit_flag      = in_sunday_transit_flag
   WHERE name               = in_name;


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_error_message := 'WARNING: No country_master updated for country_id ' || in_country_id ||'.';
  END IF;

  PAS.POST_PAS_COMMAND('MODIFIED_COUNTRY',in_country_id);  

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_COUNTRY_MASTER;


PROCEDURE INSERT_HOLIDAY_CALENDAR
(
 IN_HOLIDAY_DATE         IN HOLIDAY_CALENDAR.HOLIDAY_DATE%TYPE,
 IN_HOLIDAY_DESCRIPTION  IN HOLIDAY_CALENDAR.HOLIDAY_DESCRIPTION%TYPE,
 IN_GLOBAL_CODE          IN HOLIDAY_CALENDAR.GLOBAL_CODE%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_ERROR_MESSAGE      OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table holiday_calendar.

Input:
        holiday_date          DATE
        holiday_description   VARCHAR2
        global_code           CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO holiday_calendar
     (holiday_date,
      holiday_description,
      global_code)
    VALUES
      (in_holiday_date,
       in_holiday_description,
       in_global_code);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_HOLIDAY_CALENDAR;


PROCEDURE INSERT_HOLIDAY_COUNTRY
(
 IN_COUNTRY_ID           IN HOLIDAY_COUNTRY.COUNTRY_ID%TYPE,
 IN_HOLIDAY_DATE         IN HOLIDAY_COUNTRY.HOLIDAY_DATE%TYPE,
 IN_DELIVERABLE_FLAG     IN HOLIDAY_COUNTRY.DELIVERABLE_FLAG%TYPE,
 IN_SHIPPING_ALLOWED     IN HOLIDAY_COUNTRY.SHIPPING_ALLOWED%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_ERROR_MESSAGE      OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table holiday_country.

Input:
        country_id        VARCHAR2
        holiday_date      DATE
        deliverable_flag  CHAR
        shipping_allowed  CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

v_date          date;
v_end_date      date;
v_add_days      number;
v_priority      integer;

BEGIN


  INSERT INTO holiday_country
     (country_id,
      holiday_date,
      deliverable_flag,
      shipping_allowed)
    VALUES
      (in_country_id,
       in_holiday_date,
       in_deliverable_flag,
       in_shipping_allowed);

  out_status := 'Y';

  v_date := in_holiday_date;
  SELECT FGP.VALUE INTO V_ADD_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'ADD_DAYS';
  v_end_date := v_date + v_add_days;

  while v_date <= v_end_date loop
    v_priority := v_date - trunc(sysdate);
    PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_COUNTRY', in_country_id || ' ' || to_char(v_date, 'mmddyyyy'), v_priority);
    v_date := v_date + 1;
  end loop;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_HOLIDAY_COUNTRY;


PROCEDURE DELETE_HOLIDAY_COUNTRY
(
 IN_COUNTRY_ID      IN HOLIDAY_COUNTRY.COUNTRY_ID%TYPE,
 IN_HOLIDAY_DATE    IN HOLIDAY_COUNTRY.HOLIDAY_DATE%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_MESSAGE       OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure deletes every row in table holiday_country
   for the country_id and holiday_date passed in.

Input:
        country_id        VARCHAR2
        holiday_date      DATE


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/

v_date          date;
v_end_date      date;
v_add_days      number;
v_priority      integer;

BEGIN

  DELETE holiday_country
   WHERE country_id = in_country_id
     AND holiday_date = in_holiday_date;

  IF SQL%FOUND THEN
     out_status := 'Y';

     v_date := in_holiday_date;
     SELECT FGP.VALUE INTO V_ADD_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'ADD_DAYS';
     v_end_date := v_date + v_add_days;

     while v_date <= v_end_date loop
       v_priority := v_date - trunc(sysdate);
       PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_COUNTRY', in_country_id || ' ' || to_char(v_date, 'mmddyyyy'), v_priority);
       v_date := v_date + 1;
     end loop;
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No holiday_country updated for country_id ' || in_country_id ||' and holiday_date ' || in_holiday_date ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_HOLIDAY_COUNTRY;

PROCEDURE GET_SERVICE_FEE_OVERRIDE_LIST
(
  IN_SERVICE_FEE_ID    IN SERVICE_FEE_OVERRIDE.SNH_ID%TYPE,
  OUT_CUR             OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_CUR FOR
    SELECT   SNH_ID,
             to_char(OVERRIDE_DATE,'MM/DD/YYYY') override_date,
             DOMESTIC_CHARGE,
             INTERNATIONAL_CHARGE,
             VENDOR_CHARGE,
             VENDOR_SAT_UPCHARGE,
             REQUESTED_BY,
             SAME_DAY_UPCHARGE,
             VENDOR_SUN_UPCHARGE,
             VENDOR_MON_UPCHARGE,
             SAME_DAY_UPCHARGE_FS
    FROM     SERVICE_FEE_OVERRIDE
    WHERE    SNH_ID = IN_SERVICE_FEE_ID
    ORDER BY OVERRIDE_DATE;

END GET_SERVICE_FEE_OVERRIDE_LIST;

PROCEDURE GET_SOURCE_BY_SERVICE_FEE
(
  IN_SERVICE_FEE_ID    IN SOURCE_MASTER.SNH_ID%TYPE,
  OUT_CUR             OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_CUR FOR
    SELECT   SOURCE_CODE,
             DESCRIPTION
    FROM     SOURCE_MASTER
    WHERE    SNH_ID = IN_SERVICE_FEE_ID
    ORDER BY SOURCE_CODE;

END GET_SOURCE_BY_SERVICE_FEE;

PROCEDURE UPDATE_INSERT_SERVICE_FEE
(
  IN_SNH_ID                     IN SNH.SNH_ID%TYPE,
  IN_DESCRIPTION                IN SNH.DESCRIPTION%TYPE,
  IN_FIRST_ORDER_DOMESTIC       IN SNH.FIRST_ORDER_DOMESTIC%TYPE,
  IN_FIRST_ORDER_INTERNATIONAL  IN SNH.FIRST_ORDER_INTERNATIONAL%TYPE,
  IN_VENDOR_CHARGE	        	IN SNH.VENDOR_CHARGE%TYPE,
  IN_VENDOR_SAT_UPCHARGE		IN SNH.VENDOR_SAT_UPCHARGE%TYPE,  
  IN_UPDATED_BY                 IN SNH.UPDATED_BY%TYPE,
  IN_REQUESTED_BY               IN SNH.REQUESTED_BY%TYPE,
  IN_SAME_DAY_UPCHARGE  		IN SNH.SAME_DAY_UPCHARGE%TYPE,
  IN_VENDOR_SUN_UPCHARGE		IN SNH.VENDOR_SUN_UPCHARGE%TYPE,  
  IN_VENDOR_MON_UPCHARGE		IN SNH.VENDOR_MON_UPCHARGE%TYPE, 
  IN_SAME_DAY_UPCHARGE_FS  		IN SNH.SAME_DAY_UPCHARGE_FS%TYPE, 
  OUT_STATUS                   	OUT VARCHAR2,
  OUT_MESSAGE                 	OUT VARCHAR2
)
AS

BEGIN

  UPDATE snh
    SET  description = in_description,
         first_order_domestic = in_first_order_domestic,
         first_order_international = in_first_order_international,
         vendor_charge = in_vendor_charge,
         vendor_sat_upcharge = in_vendor_sat_upcharge,
         vendor_sun_upcharge = in_vendor_sun_upcharge,
         vendor_mon_upcharge = in_vendor_mon_upcharge,
         requested_by = in_requested_by,
         updated_by = in_updated_by,
         updated_on = sysdate,
         same_day_upcharge = in_same_day_upcharge,
         same_day_upcharge_fs = in_same_day_upcharge_fs
   WHERE snh_id = in_snh_id;


  IF SQL%NOTFOUND THEN
    INSERT INTO snh
       (snh_id,
        description,
        first_order_domestic,
        second_order_domestic,
        third_order_domestic,
        first_order_international,
        second_order_international,
        third_order_international,
        vendor_charge,
        vendor_sat_upcharge,
        vendor_sun_upcharge,
        vendor_mon_upcharge,
        requested_by,
        created_on,
        created_by,
        updated_on,
        updated_by,
        same_day_upcharge,
        same_day_upcharge_fs)
      VALUES
       (in_snh_id,
        in_description,
        in_first_order_domestic,
        0.00,
        0.00,
        in_first_order_international,
        0.00,
        0.00,
        in_vendor_charge,
        in_vendor_sat_upcharge,
        in_vendor_sun_upcharge,
        in_vendor_mon_upcharge,
        in_requested_by,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by,
        in_same_day_upcharge,
        in_same_day_upcharge_fs);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_SERVICE_FEE;

PROCEDURE DELETE_SERVICE_FEE
(
  IN_SNH_ID                     IN SNH.SNH_ID%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS

BEGIN

  -- First, delete any rows in SERVICE_FEE_OVERRIDE
  DELETE FROM SERVICE_FEE_OVERRIDE
        WHERE SNH_ID = IN_SNH_ID;

  DELETE FROM SNH
        WHERE SNH_ID = IN_SNH_ID;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_SERVICE_FEE;

PROCEDURE UPDATE_SERVICE_FEE_OVERRIDE
(
  IN_SNH_ID                     IN SERVICE_FEE_OVERRIDE.SNH_ID%TYPE,
  IN_OVERRIDE_DATE              IN SERVICE_FEE_OVERRIDE.OVERRIDE_DATE%TYPE,
  IN_DOMESTIC_CHARGE            IN SERVICE_FEE_OVERRIDE.DOMESTIC_CHARGE%TYPE,
  IN_INTERNATIONAL_CHARGE       IN SERVICE_FEE_OVERRIDE.INTERNATIONAL_CHARGE%TYPE,
  IN_VENDOR_CHARGE	        	IN SERVICE_FEE_OVERRIDE.VENDOR_CHARGE%TYPE,
  IN_VENDOR_SAT_UPCHARGE		IN SERVICE_FEE_OVERRIDE.VENDOR_SAT_UPCHARGE%TYPE,   
  IN_REQUESTED_BY               IN SERVICE_FEE_OVERRIDE.REQUESTED_BY%TYPE,
  IN_UPDATED_BY                 IN SERVICE_FEE_OVERRIDE.UPDATED_BY%TYPE,
  IN_SAME_DAY_UPCHARGE   		IN SERVICE_FEE_OVERRIDE.SAME_DAY_UPCHARGE%TYPE,
  IN_VENDOR_SUN_UPCHARGE		IN SERVICE_FEE_OVERRIDE.VENDOR_SUN_UPCHARGE%TYPE,   
  IN_VENDOR_MON_UPCHARGE		IN SERVICE_FEE_OVERRIDE.VENDOR_MON_UPCHARGE%TYPE,
  IN_SAME_DAY_UPCHARGE_FS   	IN SERVICE_FEE_OVERRIDE.SAME_DAY_UPCHARGE_FS%TYPE,   
  OUT_STATUS                   	OUT VARCHAR2,
  OUT_MESSAGE                  	OUT VARCHAR2
)
AS

dateRangeFlag varchar(100);

CURSOR getDeliveryDateRange IS
  select 'Y'
    from delivery_date_range
   where start_date <= in_override_date
     and end_date >= in_override_date
     and active_flag = 'Y'
     and ROWNUM = 1;

BEGIN

  UPDATE service_fee_override
    SET  domestic_charge = in_domestic_charge,
         international_charge = in_international_charge,
         vendor_charge = in_vendor_charge,
         vendor_sat_upcharge = in_vendor_sat_upcharge,
         vendor_sun_upcharge = in_vendor_sun_upcharge,
         vendor_mon_upcharge = in_vendor_mon_upcharge,
         requested_by = in_requested_by,
         updated_by = in_updated_by,
         updated_on = sysdate,
         same_day_upcharge = in_same_day_upcharge,
         same_day_upcharge_fs = in_same_day_upcharge_fs
   WHERE snh_id = in_snh_id
         and override_date = in_override_date;


  IF SQL%NOTFOUND THEN
    INSERT INTO service_fee_override
       (snh_id,
        override_date,
        domestic_charge,
        international_charge,
        vendor_charge,
        vendor_sat_upcharge,  
        vendor_sun_upcharge,
        vendor_mon_upcharge,      
        requested_by,
        created_on,
        created_by,
        updated_on,
        updated_by,
        same_day_upcharge,
        same_day_upcharge_fs)
      VALUES
       (in_snh_id,
        in_override_date,
        in_domestic_charge,
        in_international_charge,
        in_vendor_charge,
        in_vendor_sat_upcharge,
        in_vendor_sun_upcharge,
        in_vendor_mon_upcharge,
        in_requested_by,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by,
        in_same_day_upcharge,
        in_same_day_upcharge_fs);
  END IF;

  open getDeliveryDateRange;
  fetch getDeliveryDateRange into dateRangeFlag;
  close getDeliveryDateRange;

  if dateRangeFlag = 'Y' then
    out_status := 'DATE_RANGE';
  else
    out_status := 'Y';
  end if;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_SERVICE_FEE_OVERRIDE;

PROCEDURE DELETE_SERVICE_FEE_OVERRIDE
(
  IN_SNH_ID                     IN SERVICE_FEE_OVERRIDE.SNH_ID%TYPE,
  IN_OVERRIDE_DATE              IN SERVICE_FEE_OVERRIDE.OVERRIDE_DATE%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS

BEGIN

  DELETE FROM SERVICE_FEE_OVERRIDE
        WHERE SNH_ID = IN_SNH_ID
        AND   OVERRIDE_DATE = IN_OVERRIDE_DATE;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_SERVICE_FEE_OVERRIDE;

PROCEDURE GET_SERVICE_FEE_HISTORY
(
  IN_SERVICE_FEE_ID    IN SNH$.SNH_ID%TYPE,
  OUT_CUR             OUT TYPES.REF_CURSOR
)
AS

BEGIN

OPEN OUT_CUR FOR
select null override_date,
       timestamp$ timestamp,
       operation$ operation,
       updated_by,
       first_order_domestic domestic_charge,
       first_order_international international_charge,
       vendor_charge,
       vendor_sat_upcharge,
       vendor_sun_upcharge,
       vendor_mon_upcharge,
       requested_by,
       'SNH' whereFrom,
       same_day_upcharge,
       same_day_upcharge_fs
  from ftd_apps.snh$
  where snh_id = in_service_fee_id
  and operation$ != 'UPD_OLD'
union all
  (select to_char(override_date,'MM/DD/YYYY') override_date,
         timestamp$ timestamp,
         operation$ operation,
         updated_by,
         domestic_charge,
         international_charge,
         vendor_charge,
         vendor_sat_upcharge,
         vendor_sun_upcharge,
       	 vendor_mon_upcharge,
         requested_by,
         'SFO' whereFrom,
         same_day_upcharge,
         same_day_upcharge_fs
    from ftd_apps.service_fee_override$
    where snh_id = in_service_fee_id
    and operation$ != 'UPD_OLD')
order by timestamp desc;


END GET_SERVICE_FEE_HISTORY;

FUNCTION GET_EX_PRODUCTS_TO_PROCESS
RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_EX_PRODUCTS_TO_PROCESS
-- Type:    Function
-- Syntax:  GET_EX_PRODUCTS_TO_PROCESS ()
--
--
-- Description:   Queries the PRODUCT_MASTER table
--                returns a ref cursor for all products with availability excptions
--                which need to be processed.
--
--==============================================================================
AS
    product_cursor types.ref_cursor;
BEGIN
    OPEN product_cursor FOR
      SELECT PRODUCT_ID
            FROM PRODUCT_MASTER
            WHERE EXCEPTION_END_DATE < trunc(sysdate)
            AND EXCEPTION_START_DATE IS NOT NULL
            AND EXCEPTION_CODE = 'A'
        ORDER BY "PRODUCT_ID";

    RETURN product_cursor;
END GET_EX_PRODUCTS_TO_PROCESS;




FUNCTION HAS_SERVICE_FEE_BEEN_OVERRIDEN
(
  IN_SOURCE_CODE      IN SOURCE_MASTER.SOURCE_CODE%TYPE,
  IN_DELIVERY_DATE    IN VARCHAR2,
  IN_SERVICE_FEE   IN SERVICE_FEE_OVERRIDE.DOMESTIC_CHARGE%TYPE

)
RETURN VARCHAR2
AS

 CURSOR snh_cur IS
   SELECT 'Y'
   FROM FTD_APPS.SOURCE_MASTER sm, FTD_APPS.SERVICE_FEE_OVERRIDE sfo
   WHERE to_char(sfo.override_date,'MM/DD/YYYY') = IN_DELIVERY_DATE
    AND sfo.DOMESTIC_CHARGE != IN_SERVICE_FEE
    AND sm.SNH_ID = sfo.SNH_ID
    AND sm.SOURCE_CODE  =  IN_SOURCE_CODE;

v_return VARCHAR2(1);

BEGIN

 OPEN snh_cur;
    FETCH snh_cur INTO v_return;
 close snh_cur;


   IF v_return IS NULL THEN
     v_return := 'N';
   END IF;

RETURN v_return;

END HAS_SERVICE_FEE_BEEN_OVERRIDEN;


PROCEDURE UPDATE_BEFORE_DELETE_SNH
(
  IN_SNH_ID                     IN SNH.SNH_ID%TYPE,
  IN_REQUESTED_BY               IN SNH.REQUESTED_BY%TYPE,
  IN_UPDATED_BY                 IN SNH.UPDATED_BY%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS

BEGIN

  UPDATE snh
    SET  requested_by = in_requested_by,
         updated_by = in_updated_by,
         created_by = 'DELETION'
   WHERE snh_id = in_snh_id;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_BEFORE_DELETE_SNH;

PROCEDURE UPDATE_BEFORE_DELETE_SFO
(
  IN_SNH_ID                     IN SERVICE_FEE_OVERRIDE.SNH_ID%TYPE,
  IN_OVERRIDE_DATE              IN SERVICE_FEE_OVERRIDE.OVERRIDE_DATE%TYPE,
  IN_REQUESTED_BY               IN SERVICE_FEE_OVERRIDE.REQUESTED_BY%TYPE,
  IN_UPDATED_BY                 IN SERVICE_FEE_OVERRIDE.UPDATED_BY%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS

BEGIN

  UPDATE service_fee_override
    SET  requested_by = in_requested_by,
         updated_by = in_updated_by,
         created_by = 'DELETION'
   WHERE snh_id = in_snh_id
         and override_date = in_override_date;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_BEFORE_DELETE_SFO;

/******************************************************************************
                          GET_BOX_INFO

Description: This procedure is responsible for retrieving box info

******************************************************************************/
PROCEDURE GET_BOX_INFO
(
  IN_BOX_ID                     IN FTD_APPS.BOX.BOX_ID%TYPE,
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS 
BEGIN

OPEN OUT_CUR FOR
        SELECT b.BOX_ID,
               b.BOX_NAME,
               b.BOX_DESC,
               b.WIDTH_INCH_QTY,
               b.HEIGHT_INCH_QTY,
               b.LENGTH_INCH_QTY,
               b.STANDARD_FLAG,
               b.CREATED_BY,
               b.CREATED_ON,
               b.UPDATED_BY,
               b.UPDATED_ON
         FROM  FTD_APPS.BOX b
        WHERE  ( (IN_BOX_ID IS NULL) OR (b.BOX_ID = IN_BOX_ID) );

END GET_BOX_INFO;


/******************************************************************************
                          GET_STANDARD_BOX_INFO

Description: This procedure is responsible for retrieving standard box info

******************************************************************************/
PROCEDURE GET_STANDARD_BOX_INFO
(
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS 
BEGIN

OPEN OUT_CUR FOR
        SELECT b.BOX_ID,
               b.BOX_NAME,
               b.BOX_DESC,
               b.WIDTH_INCH_QTY,
               b.HEIGHT_INCH_QTY,
               b.LENGTH_INCH_QTY,
               b.STANDARD_FLAG,
               b.CREATED_BY,
               b.CREATED_ON,
               b.UPDATED_BY,
               b.UPDATED_ON
         FROM  FTD_APPS.BOX b
        WHERE  b.STANDARD_FLAG = 'Y';

END GET_STANDARD_BOX_INFO;


/******************************************************************************
                          INSERT_BOX_INFO

Description: This procedure is responsible for inserting box info

******************************************************************************/
PROCEDURE INSERT_BOX_INFO
(
  IN_BOX_NAME                    IN FTD_APPS.BOX.BOX_NAME%TYPE,
  IN_BOX_DESC                    IN FTD_APPS.BOX.BOX_DESC%TYPE,
  IN_WIDTH_INCH_QTY              IN FTD_APPS.BOX.WIDTH_INCH_QTY%TYPE,
  IN_HEIGHT_INCH_QTY             IN FTD_APPS.BOX.HEIGHT_INCH_QTY%TYPE,
  IN_LENGTH_INCH_QTY             IN FTD_APPS.BOX.LENGTH_INCH_QTY%TYPE,
  IN_STANDARD_FLAG               IN FTD_APPS.BOX.STANDARD_FLAG%TYPE,
  IN_CREATED_BY                  IN FTD_APPS.BOX.CREATED_BY%TYPE,
  IN_UPDATED_BY                  IN FTD_APPS.BOX.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS 
BEGIN

      INSERT INTO FTD_APPS.BOX
      (
          BOX_ID,
          BOX_NAME,
          BOX_DESC,
          WIDTH_INCH_QTY,
          HEIGHT_INCH_QTY,
          LENGTH_INCH_QTY,
          STANDARD_FLAG,
          CREATED_BY,
          CREATED_ON,
          UPDATED_BY,
          UPDATED_ON
      )
      VALUES
      (
          key.keygen ('BOX'),
          IN_BOX_NAME,
          IN_BOX_DESC,
          IN_WIDTH_INCH_QTY,
          IN_HEIGHT_INCH_QTY,
          IN_LENGTH_INCH_QTY,
          IN_STANDARD_FLAG,
          IN_CREATED_BY,
          SYSDATE,
          IN_UPDATED_BY,
          SYSDATE
      );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_BOX_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_BOX_INFO;


/******************************************************************************
                          UPDATE_BOX_INFO

Description: This procedure is responsible for updating box info

******************************************************************************/
PROCEDURE UPDATE_BOX_INFO
(
  IN_BOX_ID                      IN FTD_APPS.BOX.BOX_ID%TYPE,
  IN_BOX_NAME                    IN FTD_APPS.BOX.BOX_NAME%TYPE,
  IN_BOX_DESC                    IN FTD_APPS.BOX.BOX_DESC%TYPE,
  IN_WIDTH_INCH_QTY              IN FTD_APPS.BOX.WIDTH_INCH_QTY%TYPE,
  IN_HEIGHT_INCH_QTY             IN FTD_APPS.BOX.HEIGHT_INCH_QTY%TYPE,
  IN_LENGTH_INCH_QTY             IN FTD_APPS.BOX.LENGTH_INCH_QTY%TYPE,
  IN_UPDATED_BY                  IN FTD_APPS.BOX.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS 
BEGIN

      UPDATE FTD_APPS.BOX
         SET BOX_NAME            = IN_BOX_NAME,
             BOX_DESC            = IN_BOX_DESC,
             WIDTH_INCH_QTY      = IN_WIDTH_INCH_QTY,
             HEIGHT_INCH_QTY     = IN_HEIGHT_INCH_QTY,
             LENGTH_INCH_QTY     = IN_LENGTH_INCH_QTY,
             UPDATED_BY          = IN_UPDATED_BY,
             UPDATED_ON          = SYSDATE
       WHERE BOX_ID              = IN_BOX_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_BOX_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPDATE_BOX_INFO;


/******************************************************************************
                          DELETE_BOX_INFO

Description: This procedure is responsible for deleting box info

******************************************************************************/
PROCEDURE DELETE_BOX_INFO 
(
  IN_BOX_ID                     IN FTD_APPS.BOX.BOX_ID%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS 
BEGIN

      DELETE 
        FROM FTD_APPS.BOX
       WHERE BOX_ID = IN_BOX_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DELETE_BOX_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DELETE_BOX_INFO;


/******************************************************************************
                          ASSIGN_BOX_TO_PRODUCT

Description: This procedure is responsible for assigning a box to a product

******************************************************************************/
PROCEDURE ASSIGN_BOX_TO_PRODUCT 
(
  IN_BOX_ID                     IN FTD_APPS.PRODUCT_MASTER.BOX_ID%TYPE,
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_UPDATED_BY                 IN FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS 
BEGIN

      UPDATE FTD_APPS.PRODUCT_MASTER
         SET BOX_ID = IN_BOX_ID,
             UPDATED_BY = IN_UPDATED_BY,
             UPDATED_ON = SYSDATE
       WHERE ((IN_PRODUCT_ID IS NULL) OR (PRODUCT_ID = IN_PRODUCT_ID));

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN ASSIGN_BOX_TO_PRODUCT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END ASSIGN_BOX_TO_PRODUCT;


/******************************************************************************
                          REASSIGN_BOX_TO_PRODUCT

Description: This procedure is responsible for updating box info on a product

******************************************************************************/
PROCEDURE REASSIGN_BOX_TO_PRODUCT 
(
  IN_ORIGINAL_BOX_ID            IN FTD_APPS.PRODUCT_MASTER.BOX_ID%TYPE,
  IN_NEW_BOX_ID                 IN FTD_APPS.PRODUCT_MASTER.BOX_ID%TYPE,
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_UPDATED_BY                 IN FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS 
BEGIN

      UPDATE FTD_APPS.PRODUCT_MASTER
         SET BOX_ID = IN_NEW_BOX_ID,
             UPDATED_BY = IN_UPDATED_BY,
             UPDATED_ON = SYSDATE
       WHERE ((IN_PRODUCT_ID IS NULL) OR (PRODUCT_ID = IN_PRODUCT_ID))
         AND ((IN_ORIGINAL_BOX_ID IS NULL) OR (BOX_ID = IN_ORIGINAL_BOX_ID));

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN REASSIGN_BOX_TO_PRODUCT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END REASSIGN_BOX_TO_PRODUCT;

PROCEDURE GET_MP_REDEMPTION_RATE_LIST
(
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving active given miles_points_redemption_rate records

Input:
        NA

Output:
        our_cur				refcursor

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CUR FOR
    SELECT   MP_REDEMPTION_RATE_ID,
             DESCRIPTION,
             PAYMENT_METHOD_ID,
             MP_REDEMPTION_RATE_AMT,
             REQUESTED_BY,
             UPDATED_BY,
             (select 'Y' from ftd_apps.source_master sm
                where sm.mp_redemption_rate_id = miles_points_redemption_rate.mp_redemption_rate_id and ROWNUM = 1) source_codes_exist
    FROM     MILES_POINTS_REDEMPTION_RATE
    WHERE    ACTIVE_FLAG = 'Y'
    ORDER BY MP_REDEMPTION_RATE_ID;
    
END GET_MP_REDEMPTION_RATE_LIST;

PROCEDURE GET_MP_REDEMPTION_RATE
(
  IN_MP_REDEMPTION_RATE_ID	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_ID%TYPE,
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving data for the given miles_points_redemption_rate record

Input:
        in_mp_redemption_rate_id        varchar2

Output:
        our_cur				refcursor

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
select mp_redemption_rate_id,
       payment_method_id,
       description,
       mp_redemption_rate_amt,
       updated_by,
       requested_by
  from ftd_apps.miles_points_redemption_rate
  where mp_redemption_rate_id = in_mp_redemption_rate_id
  ;

END GET_MP_REDEMPTION_RATE;

PROCEDURE GET_MP_REDEMPTION_RATE_HISTORY
(
  IN_MP_REDEMPTION_RATE_ID	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_ID%TYPE,
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving updating history
        for the given miles_points_redemption_rate record

Input:
        in_mp_redemption_rate_id        varchar2

Output:
        our_cur				refcursor

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
select timestamp$ timestamp,
       operation$ operation,
       payment_method_id,
       description,
       mp_redemption_rate_amt,
       updated_by,
       requested_by
  from ftd_apps.miles_points_redemption_rate$
  where mp_redemption_rate_id = in_mp_redemption_rate_id
  and operation$ != 'UPD_OLD'
order by timestamp desc;

END GET_MP_REDEMPTION_RATE_HISTORY;

PROCEDURE GET_SOURCE_BY_MP_RT
(
  IN_MP_REDEMPTION_RATE_ID	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_ID%TYPE,
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving source codes
        for the given miles_points_redemption_rate record

Input:
        in_mp_redemption_rate_id        varchar2

Output:
        our_cur				refcursor

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CUR FOR
    SELECT   SOURCE_CODE,
             DESCRIPTION
    FROM     SOURCE_MASTER
    WHERE    MP_REDEMPTION_RATE_ID = in_mp_redemption_rate_id
    ORDER BY SOURCE_CODE;
END GET_SOURCE_BY_MP_RT;

PROCEDURE UPDATE_MP_REDEMPTION_RATE
(
  IN_MP_REDEMPTION_RATE_ID	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_ID%TYPE,
  IN_DESCRIPTION		IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.DESCRIPTION%TYPE,
  IN_PAYMENT_METHOD_ID		IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.PAYMENT_METHOD_ID%TYPE,
  IN_REDEMPTION_RATE_AMT	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_AMT%TYPE,
  IN_UPDATED_BY			IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.UPDATED_BY%TYPE,
  IN_REQUESTED_BY		IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.REQUESTED_BY%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS 
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the miles_points_redemption_rate record

Input:
        in_mp_redemption_rate_id        varchar2
        in_description			varchar2
        in_payment_method_id		varchar2
        in_redemption_rate_amt		number
        in_updated_by			varchar2
        in_requested_by			varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE miles_points_redemption_rate
    SET  description = in_description,
         payment_method_id = in_payment_method_id,
         mp_redemption_rate_amt = in_redemption_rate_amt,
         requested_by = in_requested_by,
         updated_by = in_updated_by,
         updated_on = sysdate
   WHERE mp_redemption_rate_id = in_mp_redemption_rate_id;


  IF SQL%NOTFOUND THEN
    INSERT INTO miles_points_redemption_rate
       ( mp_redemption_rate_id,
	 payment_method_id,
	 description,
 	 mp_redemption_rate_amt,
	 active_flag,
	 created_on,
	 created_by,
	 updated_on,
	 updated_by,
	 requested_by
	) VALUES
       (
         in_mp_redemption_rate_id,
       	 in_payment_method_id,
       	 in_description,
         in_redemption_rate_amt,
       	 'Y',
       	 SYSDATE,
       	 in_updated_by,
       	 SYSDATE,
       	 in_updated_by,
	 in_requested_by
       );
  END IF;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN REASSIGN_BOX_TO_PRODUCT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_MP_REDEMPTION_RATE;

PROCEDURE DELETE_MP_REDEMPTION_RATE
(
  IN_MP_REDEMPTION_RATE_ID	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_ID%TYPE,
  OUT_STATUS                   OUT VARCHAR2,
  OUT_MESSAGE                  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the miles_points_redemption_rate record

Input:
        in_mp_redemption_rate_id        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM miles_points_redemption_rate
WHERE   mp_redemption_rate_id = in_mp_redemption_rate_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_MP_REDEMPTION_RATE;

PROCEDURE GET_MP_PAYMENT_METHOD
(
  OUT_CUR                      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving miles points payment method ids.

Input:
	NA
Output:
        our_cur				refcursor

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CUR FOR
    SELECT   PAYMENT_METHOD_ID
    FROM     PAYMENT_METHOD_MILES_POINTS
    ORDER BY PAYMENT_METHOD_ID;
    
END GET_MP_PAYMENT_METHOD;

PROCEDURE UPDATE_INSERT_COMPONENT_SKU
(
 IN_COMPONENT_SKU_ID            IN COMPONENT_SKU_MASTER.COMPONENT_SKU_ID%TYPE,
 IN_COMPONENT_SKU_NAME          IN COMPONENT_SKU_MASTER.COMPONENT_SKU_NAME%TYPE,
 IN_BASE_COST_DOLLAR_AMT        IN COMPONENT_SKU_MASTER.BASE_COST_DOLLAR_AMT%TYPE,
 IN_BASE_COST_EFF_DATE          IN COMPONENT_SKU_MASTER.BASE_COST_EFF_DATE%TYPE,
 IN_QA_COST_DOLLAR_AMT          IN COMPONENT_SKU_MASTER.QA_COST_DOLLAR_AMT%TYPE,
 IN_QA_COST_EFF_DATE            IN COMPONENT_SKU_MASTER.QA_COST_EFF_DATE%TYPE,
 IN_FREIGHT_IN_COST_DOLLAR_AMT  IN COMPONENT_SKU_MASTER.FREIGHT_IN_COST_DOLLAR_AMT%TYPE,
 IN_FREIGHT_IN_COST_EFF_DATE    IN COMPONENT_SKU_MASTER.FREIGHT_IN_COST_EFF_DATE%TYPE,
 IN_AVAILABLE_FLAG              IN COMPONENT_SKU_MASTER.AVAILABLE_FLAG%TYPE,
 IN_COMMENT_TEXT                IN COMPONENT_SKU_MASTER.COMMENT_TEXT%TYPE,
 IN_UPDATED_BY                  IN COMPONENT_SKU_MASTER.UPDATED_BY%TYPE,
 OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE             OUT VARCHAR2
)
AS

BEGIN

  UPDATE component_sku_master
    SET  component_sku_name = in_component_sku_name,
         base_cost_dollar_amt = in_base_cost_dollar_amt,
         base_cost_eff_date = in_base_cost_eff_date,
         qa_cost_dollar_amt = in_qa_cost_dollar_amt,
         qa_cost_eff_date = in_qa_cost_eff_date,
         freight_in_cost_dollar_amt = in_freight_in_cost_dollar_amt,
         freight_in_cost_eff_date = in_freight_in_cost_eff_date,
         available_flag = in_available_flag,
         comment_text = in_comment_text,
         updated_by = in_updated_by,
         updated_on = sysdate
   WHERE component_sku_id = in_component_sku_id;


  IF SQL%NOTFOUND THEN
    INSERT INTO component_sku_master
       (component_sku_id,
        component_sku_name,
        base_cost_dollar_amt,
        base_cost_eff_date,
        qa_cost_dollar_amt,
        qa_cost_eff_date,
        freight_in_cost_dollar_amt,
        freight_in_cost_eff_date,
        available_flag,
        comment_text,
        created_on,
        created_by,
        updated_on,
        updated_by)
      VALUES
       (in_component_sku_id,
        in_component_sku_name,
        in_base_cost_dollar_amt,
        in_base_cost_eff_date,
        in_qa_cost_dollar_amt,
        in_qa_cost_eff_date,
        in_freight_in_cost_dollar_amt,
        in_freight_in_cost_eff_date,
        in_available_flag,
        in_comment_text,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_COMPONENT_SKU;

PROCEDURE GET_COMPONENT_SKU_LIST
(
 IN_COMPONENT_SKU_ID    IN VARCHAR2,
 IN_SHOW_AVAILABLE      IN VARCHAR2,
 OUT_CURSOR            OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns Component SKU information.
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT component_sku_id,
            component_sku_name,
            base_cost_dollar_amt,
            to_char(base_cost_eff_date, 'MM/DD/YYYY') base_cost_eff_date,
            qa_cost_dollar_amt,
            to_char(qa_cost_eff_date, 'MM/DD/YYYY') qa_cost_eff_date,
            freight_in_cost_dollar_amt,
            to_char(freight_in_cost_eff_date, 'MM/DD/YYYY') freight_in_cost_eff_date,
            available_flag,
            comment_text
       FROM ftd_apps.component_sku_master
      WHERE ( (in_component_sku_id is null) or (component_sku_id = in_component_sku_id) )
        AND ( (in_show_available is null) or (available_flag = 'Y') )
      ORDER BY component_sku_id;

END GET_COMPONENT_SKU_LIST;

FUNCTION GET_ALT_PAY_INGRIAN_KEY_NAME
(
IN_PAYMENT_METHOD_ID            IN PAYMENT_METHODS.PAYMENT_METHOD_ID%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the alternate payment ingrian key.

Input:
	NA
Output:
        our_cur				refcursor

-----------------------------------------------------------------------------*/
v_key_name PAYMENT_METHODS.ALTERNATE_KEY_NAME%TYPE;

BEGIN

	select ALTERNATE_KEY_NAME
	into v_key_name
	from ftd_apps.payment_methods
	where payment_method_id = IN_PAYMENT_METHOD_ID;
	
	if v_key_name IS NULL or v_key_name = '' then
		v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');
	end if;
	
	RETURN v_key_name;
	
END GET_ALT_PAY_INGRIAN_KEY_NAME;

PROCEDURE CLEAR_EXCEPTION_DATES
(
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_UPDATED_BY                 IN FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
--=======================================================================
--
-- Name:    CLEAR_EXCEPTION_DATES
-- Type:    Procedure
-- Syntax:  CLEAR_EXCEPTION_DATES
-- INPUT:	
--		IN_PRODUCT_ID       
--		IN_UPDATED_BY
-- OUTPUT:	
--		OUT_STATUS
--		OUT_MESSAGE
--
-- Description:   Updates a product exception code and dates to null
--
--========================================================================

AS
BEGIN
        UPDATE PRODUCT_MASTER
        SET     EXCEPTION_CODE = NULL,
                EXCEPTION_START_DATE = NULL,
                EXCEPTION_END_DATE = NULL,
                UPDATED_ON = SYSDATE,
                UPDATED_BY = IN_UPDATED_BY
                WHERE PRODUCT_ID = IN_PRODUCT_ID;
  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END CLEAR_EXCEPTION_DATES;

PROCEDURE UPDATE_AVAILABILITY (
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_VENDOR_ID                  IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
  IN_UPDATE_PRODUCT		IN VARCHAR2,
  IN_UPDATE_VENDOR		IN VARCHAR2,
  IN_UPDATE_DROPSHIP_DELIVERY	IN VARCHAR2,
  IN_UPDATE_STATUS		IN VARCHAR2,
  IN_UPDATED_BY                 IN FTD_APPS.VENDOR_PRODUCT.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
--=======================================================================
--
-- Name:    UPDATE_AVAILABILITY
-- Type:    Procedure
-- Syntax:  UPDATE_AVAILABILITY
-- INPUT:	
--		IN_PRODUCT_ID                 
--		IN_VENDOR_ID                  
--		IN_UPDATE_PRODUCT		
--		IN_UPDATE_VENDOR		
--		IN_UPDATE_DROPSHIP_DELIVERY	
--		IN_UPDATE_STATUS		
--		IN_UPDATED_BY                 
-- OUTPUT:	
--		OUT_STATUS
--		OUT_MESSAGE
--
-- Description:   Updates product/vendor/dropship Delivery availability
--
--========================================================================
AS

v_vendor_dropship_flag 	varchar2(100);

BEGIN
     --set vendor / dropship flag based on status passed in
     IF IN_UPDATE_STATUS = 'A' THEN
        v_vendor_dropship_flag := 'Y';
     ELSE
     	v_vendor_dropship_flag := 'N';
     END IF;
     
     -- check to see if vendor status needs to be updated  
     IF IN_UPDATE_VENDOR = 'Y' THEN
             
     UPDATE FTD_APPS.VENDOR_PRODUCT vp
     SET vp.available = v_vendor_dropship_flag,
         vp.updated_by = IN_UPDATED_BY,
         vp.updated_on = sysdate
     WHERE vp.vendor_id = IN_VENDOR_ID
     AND   vp.product_subcode_id = IN_PRODUCT_ID;
     END IF;
    
     IF SQL%ROWCOUNT = 0 THEN
     ROLLBACK;
            out_status := 'N';
      out_message := 'FAILED TO UPDATE VENDOR PRODUCT INFO FOR VENDOR ' || IN_VENDOR_ID;
     END IF;
     
     -- check to see if dropship delivery and product status needs to be updated  
     IF IN_UPDATE_DROPSHIP_DELIVERY = 'Y' AND IN_UPDATE_PRODUCT = 'Y' THEN
             
     UPDATE ftd_apps.product_master pm
     SET pm.ship_method_carrier = v_vendor_dropship_flag,
         pm.status = IN_UPDATE_STATUS,
         pm.updated_by = IN_UPDATED_BY,
         pm.updated_on = sysdate
     WHERE pm.product_id = IN_PRODUCT_ID;
     END IF;
     IF SQL%ROWCOUNT = 0 THEN
     ROLLBACK;
            out_status := 'N';
      out_message := 'FAILED TO UPDATE STATUS AND DROPSHIP INFO FOR PRODUCT ' || IN_PRODUCT_ID;
     END IF;
     
     -- check to see if only dropship delivery needs to be updated  
     IF IN_UPDATE_DROPSHIP_DELIVERY = 'Y' AND IN_UPDATE_PRODUCT = 'N' THEN
             
     UPDATE ftd_apps.product_master pm
     SET pm.ship_method_carrier = v_vendor_dropship_flag,
         pm.updated_by = IN_UPDATED_BY,
         pm.updated_on = sysdate
     WHERE pm.product_id = IN_PRODUCT_ID;
     END IF;
     IF SQL%ROWCOUNT = 0 THEN
     ROLLBACK;
            out_status := 'N';
      out_message := 'FAILED TO UPDATE DROPSHIP INFO FOR PRODUCT ' || IN_PRODUCT_ID;
     END IF;
     
     -- check to see if only product status needs to be updated  
     IF IN_UPDATE_PRODUCT = 'Y' AND IN_UPDATE_DROPSHIP_DELIVERY = 'N' THEN
             
     UPDATE ftd_apps.product_master pm
     SET pm.status = IN_UPDATE_STATUS,
         pm.updated_by = IN_UPDATED_BY,
         pm.updated_on = sysdate
     WHERE pm.product_id = IN_PRODUCT_ID;
     END IF;
     IF SQL%ROWCOUNT = 0 THEN
     ROLLBACK;
            out_status := 'N';
      out_message := 'FAILED TO UPDATE STATUS FOR PRODUCT ' || IN_PRODUCT_ID;
     END IF;
     
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_AVAILABILITY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END UPDATE_AVAILABILITY;

PROCEDURE UPDATE_PRODUCT_EXCEPTION_DATES (
  IN_PRODUCT_ID			IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_EXCEPTION_CODE		IN FTD_APPS.PRODUCT_MASTER.EXCEPTION_CODE%TYPE,
  IN_EXCEPTION_START_DATE	IN FTD_APPS.PRODUCT_MASTER.EXCEPTION_START_DATE%TYPE,
  IN_EXCEPTION_END_DATE		IN FTD_APPS.PRODUCT_MASTER.EXCEPTION_END_DATE%TYPE,
  IN_UPDATED_BY                 IN FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
--=======================================================================
--
-- Name:    UPDATE_PRODUCT_EXCEPTION_DATES
-- Type:    Procedure
-- Syntax:  UPDATE_PRODUCT_EXCEPTION_DATES
-- INPUT:	
--		IN_PRODUCT_ID                 
--		IN_EXCEPTION_CODE
--		IN_EXCEPTION_START_DATE                 
--		IN_EXCEPTION_END_DATE		
--		IN_UPDATED_BY                 
-- OUTPUT:	
--		OUT_STATUS
--		OUT_MESSAGE
--
-- Description:   Updates exception dates for product
--
--========================================================================


AS
BEGIN
        UPDATE PRODUCT_MASTER
        SET     EXCEPTION_CODE = 'A',
                EXCEPTION_START_DATE = IN_EXCEPTION_START_DATE,
                EXCEPTION_END_DATE = IN_EXCEPTION_END_DATE,
                UPDATED_ON = SYSDATE,
                UPDATED_BY = IN_UPDATED_BY
                WHERE PRODUCT_ID = IN_PRODUCT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PRODUCT_EXCEPTION_DATES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END UPDATE_PRODUCT_EXCEPTION_DATES;

FUNCTION MULTI_VALUE (V_VAL IN VARCHAR2)
RETURN VARCHAR2
--=======================================================================
--
-- Name:    MULTI_VALUE
-- Type:    Function
-- Syntax:  
-- INPUT:	
--		v_val                
-- OUTPUT:	
--		varchar2
--
-- Description:   return parameter string in statement to be used in a dynamic SQL.
--		  The code exists in schema rpt. In the future we may want to refactor the code in rpt
--		  schema to use this function instead.
--
--========================================================================
IS
	p_val varchar2(1000);
	p_val2 varchar2(1000);
	v_new_parm  varchar2(1000);
	v_comma_pos number(3);
	v_numb_commas number(3) := 0;
	v_start number(3) := 0;
BEGIN
	p_val := v_val;
	p_val2 := v_val;
	while instr(p_val2,',') <> 0 loop
		v_comma_pos := instr(p_val2,',');
		p_val := ''''||rtrim(ltrim(substr(p_val2,1,v_comma_pos-1),' '),' ')||''',';
		v_new_parm := v_new_parm || p_val;
		p_val2 := substr(p_val2, v_comma_pos+1);
	end loop;
	v_new_parm:= v_new_parm||''''||rtrim(ltrim(p_val2,' '),' ')||'''';
	
	RETURN(v_new_parm);
END MULTI_VALUE;

PROCEDURE GET_PRICE_PAY_VENDOR_BATCH_ID (
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2,
  OUT_BATCH_ID                  OUT NUMBER
)
AS

BEGIN

    SELECT price_pay_vendor_batch_sq.nextval INTO out_batch_id from dual;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PRODUCT_EXCEPTION_DATES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END GET_PRICE_PAY_VENDOR_BATCH_ID;

PROCEDURE INSERT_PRICE_PAY_VENDOR_UPDATE (
  IN_BATCH_ID                   IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PRICE_PAY_VENDOR_BATCH_ID%TYPE,
  IN_SPREADSHEET_ROW_NUM        IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.SPREADSHEET_ROW_NUM%TYPE,
  IN_PRODUCT_ID                 IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PRODUCT_ID%TYPE,
  IN_VENDOR_ID                  IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.VENDOR_ID%TYPE,
  IN_VENDOR_COST                IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.VENDOR_COST%TYPE,
  IN_STANDARD_PRICE             IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.STANDARD_PRICE%TYPE,
  IN_DELUXE_PRICE               IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.DELUXE_PRICE%TYPE,
  IN_PREMIUM_PRICE              IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PREMIUM_PRICE%TYPE,
  IN_PROCESSED_FLAG             IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PROCESSED_FLAG%TYPE,
  IN_ERROR_MSG_TXT              IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.ERROR_MSG_TXT%TYPE,
  IN_VENDOR_COST_VARIANCE       IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.VENDOR_COST_VARIANCE_PCT%TYPE,
  IN_STANDARD_PRICE_VARIANCE    IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.STANDARD_PRICE_VARIANCE_PCT%TYPE,
  IN_DELUXE_PRICE_VARIANCE      IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.DELUXE_PRICE_VARIANCE_PCT%TYPE,
  IN_PREMIUM_PRICE_VARIANCE     IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PREMIUM_PRICE_VARIANCE_PCT%TYPE,
  IN_USER_ID                    IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.CREATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO PRICE_PAY_VENDOR_UPDATE (
        PRICE_PAY_VENDOR_BATCH_ID,
        SPREADSHEET_ROW_NUM,
        PRODUCT_ID,
        VENDOR_ID,
        VENDOR_COST,
        VENDOR_COST_VARIANCE_PCT,
        STANDARD_PRICE,
        STANDARD_PRICE_VARIANCE_PCT,
        DELUXE_PRICE,
        DELUXE_PRICE_VARIANCE_PCT,
        PREMIUM_PRICE,
        PREMIUM_PRICE_VARIANCE_PCT,
        PROCESSED_FLAG,
        ERROR_MSG_TXT,
        CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON)
    VALUES (
        IN_BATCH_ID,
        IN_SPREADSHEET_ROW_NUM,
        IN_PRODUCT_ID,
        IN_VENDOR_ID,
        IN_VENDOR_COST,
        IN_VENDOR_COST_VARIANCE,
        IN_STANDARD_PRICE,
        IN_STANDARD_PRICE_VARIANCE,
        IN_DELUXE_PRICE,
        IN_DELUXE_PRICE_VARIANCE,
        IN_PREMIUM_PRICE,
        IN_PREMIUM_PRICE_VARIANCE,
        IN_PROCESSED_FLAG,
        IN_ERROR_MSG_TXT,
        IN_USER_ID,
        SYSDATE,
        IN_USER_ID,
        SYSDATE);

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PRODUCT_EXCEPTION_DATES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_PRICE_PAY_VENDOR_UPDATE;

PROCEDURE UPDATE_PRICE_PAY_VENDOR_UPDATE (
  IN_BATCH_ID                   IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PRICE_PAY_VENDOR_BATCH_ID%TYPE,
  IN_SPREADSHEET_ROW_NUM        IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.SPREADSHEET_ROW_NUM%TYPE,
  IN_PROCESSED_FLAG             IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PROCESSED_FLAG%TYPE,
  IN_ERROR_MSG_TXT              IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.ERROR_MSG_TXT%TYPE,
  IN_USER_ID                    IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.CREATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS

BEGIN

    UPDATE PRICE_PAY_VENDOR_UPDATE
    SET PROCESSED_FLAG = IN_PROCESSED_FLAG,
        ERROR_MSG_TXT = IN_ERROR_MSG_TXT,
        UPDATED_BY = IN_USER_ID,
        UPDATED_ON = SYSDATE
    WHERE PRICE_PAY_VENDOR_BATCH_ID = IN_BATCH_ID
    AND SPREADSHEET_ROW_NUM = IN_SPREADSHEET_ROW_NUM;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PRODUCT_EXCEPTION_DATES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PRICE_PAY_VENDOR_UPDATE;

PROCEDURE GET_PRICE_PAY_VENDOR_LIST (
  IN_BATCH_ID                   IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PRICE_PAY_VENDOR_BATCH_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT * FROM PRICE_PAY_VENDOR_UPDATE
        WHERE PRICE_PAY_VENDOR_BATCH_ID = IN_BATCH_ID
        ORDER BY SPREADSHEET_ROW_NUM;

END GET_PRICE_PAY_VENDOR_LIST;

PROCEDURE GET_PPV_APPROVAL_LIST (
  IN_BATCH_ID                   IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PRICE_PAY_VENDOR_BATCH_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT P.PRODUCT_ID,
            PM.PRODUCT_NAME,
            P.VENDOR_ID,
            P.VENDOR_COST,
            P.STANDARD_PRICE,
            P.DELUXE_PRICE,
            P.PREMIUM_PRICE,
            P.ERROR_MSG_TXT,
            P.VENDOR_COST_VARIANCE_PCT VC_VARIANCE,
            P.STANDARD_PRICE_VARIANCE_PCT STANDARD_VARIANCE,
            P.DELUXE_PRICE_VARIANCE_PCT DELUXE_VARIANCE,
            P.PREMIUM_PRICE_VARIANCE_PCT PREMIUM_VARIANCE,
            case
                when (p.vendor_cost_variance_pct > p.standard_price_variance_pct
                    and p.vendor_cost_variance_pct > p.deluxe_price_variance_pct
                    and p.vendor_cost_variance_pct > p.premium_price_variance_pct) then p.vendor_cost_variance_pct
                when (p.standard_price_variance_pct > p.deluxe_price_variance_pct
                    and p.standard_price_variance_pct > p.premium_price_variance_pct) then p.standard_price_variance_pct
                when (p.deluxe_price_variance_pct > p.premium_price_variance_pct) then p.deluxe_price_variance_pct
                else p.premium_price_variance_pct
            END AS SORT_VARIANCE
        FROM PRICE_PAY_VENDOR_UPDATE P, PRODUCT_MASTER PM
        WHERE P.PRICE_PAY_VENDOR_BATCH_ID = IN_BATCH_ID
        AND PM.PRODUCT_ID (+) = P.PRODUCT_ID
        ORDER BY sort_variance desc, p.error_msg_txt desc;

END GET_PPV_APPROVAL_LIST;

PROCEDURE GET_PPV_APPROVAL_COUNTS (
  IN_BATCH_ID                   IN FTD_APPS.PRICE_PAY_VENDOR_UPDATE.PRICE_PAY_VENDOR_BATCH_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

v_no_product_master    number := 0;
v_no_vendor_master     number := 0;
v_no_vendor_product    number := 0;

BEGIN

    begin
        select count(*) into v_no_product_master
        from price_pay_vendor_update p
        where p.price_pay_vendor_batch_id = in_batch_id
        and not exists (
            select 'Y' from product_master pm
            where pm.product_id = p.product_id);

        exception when no_data_found then null;
    end;

    begin
        select count(*) into v_no_vendor_master
        from price_pay_vendor_update p
        where p.price_pay_vendor_batch_id = in_batch_id
        and p.vendor_id is not null
        and not exists (
            select 'Y' from vendor_master vm
            where vm.vendor_id = p.vendor_id);

        exception when no_data_found then null;
    end;

    begin
        select count(*) into v_no_vendor_product
        from price_pay_vendor_update p
        where p.price_pay_vendor_batch_id = in_batch_id
        and p.vendor_id is not null
        and not exists (
            select 'Y' from vendor_product vp
            where vp.vendor_id = p.vendor_id
            and vp.product_subcode_id = p.product_id);

        exception when no_data_found then null;
    end;

    OPEN OUT_CUR FOR
        SELECT V_NO_PRODUCT_MASTER "NO_PRODUCT_MASTER",
            V_NO_VENDOR_MASTER "NO_VENDOR_MASTER",
            V_NO_VENDOR_PRODUCT "NO_VENDOR_PRODUCT"
        FROM DUAL;

END GET_PPV_APPROVAL_COUNTS;

PROCEDURE GET_VENDOR_PRODUCT (
  IN_VENDOR_ID                  IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
  IN_PRODUCT_ID                 IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT VENDOR_SKU,
            VENDOR_COST,
            AVAILABLE,
            REMOVED,
            VENDOR_COST_EFF_DATE
        FROM FTD_APPS.VENDOR_PRODUCT
        WHERE VENDOR_ID = IN_VENDOR_ID
        AND PRODUCT_SUBCODE_ID = IN_PRODUCT_ID;

END GET_VENDOR_PRODUCT;

PROCEDURE UPDATE_PRODUCT_MASTER_PRICES (
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  IN_STANDARD_PRICE             IN FTD_APPS.PRODUCT_MASTER.STANDARD_PRICE%TYPE,
  IN_DELUXE_PRICE               IN FTD_APPS.PRODUCT_MASTER.DELUXE_PRICE%TYPE,
  IN_PREMIUM_PRICE              IN FTD_APPS.PRODUCT_MASTER.PREMIUM_PRICE%TYPE,
  IN_USER_ID                    IN FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS

BEGIN

    UPDATE PRODUCT_MASTER
    SET STANDARD_PRICE = IN_STANDARD_PRICE,
        DELUXE_PRICE = IN_DELUXE_PRICE,
        PREMIUM_PRICE = IN_PREMIUM_PRICE,
        UPDATED_BY = IN_USER_ID,
        UPDATED_ON = SYSDATE
    WHERE PRODUCT_ID = IN_PRODUCT_ID;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PRODUCT_EXCEPTION_DATES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PRODUCT_MASTER_PRICES;

PROCEDURE UPDATE_VENDOR_COST (
  IN_VENDOR_ID                  IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
  IN_PRODUCT_ID                 IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
  IN_VENDOR_COST                IN FTD_APPS.VENDOR_PRODUCT.VENDOR_COST%TYPE,
  IN_USER_ID                    IN FTD_APPS.VENDOR_PRODUCT.UPDATED_BY%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS

BEGIN

    UPDATE VENDOR_PRODUCT
    SET VENDOR_COST = IN_VENDOR_COST,
        UPDATED_BY = IN_USER_ID,
        UPDATED_ON = SYSDATE
    WHERE VENDOR_ID = IN_VENDOR_ID
    AND PRODUCT_SUBCODE_ID = IN_PRODUCT_ID;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PRODUCT_EXCEPTION_DATES [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_COST;

PROCEDURE GET_COMP_ADD_ON_INFO (
  IN_SOURCE_CODE                IN FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
  IN_PRODUCT_ID                 IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT  a.addon_text,
		at.DESCRIPTION  
	FROM 
		ADDON a,
		ADDON_TYPE at,
		PRODUCT_MASTER pm,
		SOURCE_MASTER sm
	WHERE   sm.SOURCE_CODE = IN_SOURCE_CODE
	AND	pm.PRODUCT_ID = IN_PRODUCT_ID
	AND 	sm.ADD_ON_FREE_ID = pm.ADD_ON_FREE_ID
	AND 	a.ADDON_ID = sm.ADD_ON_FREE_ID
	AND	a.ADDON_TYPE = at.ADDON_TYPE_ID;

END GET_COMP_ADD_ON_INFO;

PROCEDURE GET_UPSELL_MASTER_FOR_BASE_ID (
  IN_PRODUCT_ID    IN  FTD_APPS.UPSELL_DETAIL.UPSELL_DETAIL_ID%TYPE,
  OUT_CUR          OUT TYPES.REF_CURSOR
)
AS

BEGIN
    OPEN OUT_CUR FOR
        SELECT ud.upsell_master_id
        FROM   ftd_apps.upsell_detail ud, ftd_apps.upsell_master um
        WHERE  ud.upsell_detail_id = IN_PRODUCT_ID
        AND    ud.upsell_detail_sequence = 1   -- Making sure it's base product
        AND    ud.upsell_master_id = um.upsell_master_id;

END GET_UPSELL_MASTER_FOR_BASE_ID;

PROCEDURE GET_STATE_BY_ZIP_AND_CITY(
  IN_ZIP_CODE_ID                IN FTD_APPS.ZIP_CODE.ZIP_CODE_ID%TYPE,
  IN_CITY                       IN FTD_APPS.ZIP_CODE.CITY%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR,
  OUT_STATUS                    OUT VARCHAR2, 
  OUT_MESSAGE                   OUT VARCHAR2 
)
AS

v_state_id      varchar(2)   := '';
v_city_cond     varchar(200) := '';

BEGIN

IF IN_CITY IS NOT NULL AND IN_CITY  != '' THEN
  v_city_cond := ' AND CITY = ''' || IN_CITY || '''';
END IF;

open OUT_CUR for 
  select unique state_id, zip_code_id from  ftd_apps.zip_code where 
  zip_code_id = IN_ZIP_CODE_ID 
  || v_city_cond;
  

out_status := 'Y';
out_message := '';

EXCEPTION WHEN OTHERS THEN
BEGIN
  out_status := 'N';
  out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;
END GET_STATE_BY_ZIP_AND_CITY;

PROCEDURE GET_COMPANY_SENDING_FLORISTS
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves sending florist numbers from ftd_apps.company_master table

Output:
        cursor containing sending florist numbers
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
     SELECT distinct sending_florist_number, 
            frp.misc_pkg.get_global_parm_value ('MERCURY_INTERFACE_CONFIG', 'DELIVERY_CONFIRMATION_SUFFIX_LIST_' || sending_florist_number) AVAILABLE_SUFFIXES
     FROM   ftd_apps.company_master;

END GET_COMPANY_SENDING_FLORISTS;

PROCEDURE GET_SENDING_FLORIST_BY_ID
(
 IN_ORDER_DETAIL_ID   		IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR     		OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves sending florist details
        from ftd_apps.company_master table based on order detail id

Input:
        order_detail_id

Output:
        cursor containing sending florist info
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
      SELECT sending_florist_number, company_name
      FROM ftd_apps.company_master cm, clean.orders o, clean.order_details od
      WHERE od.order_detail_id = IN_ORDER_DETAIL_ID
      AND   od.order_guid = o.order_guid
      AND   o.company_id = cm.company_id;

END GET_SENDING_FLORIST_BY_ID;

PROCEDURE GET_SRC_MEMBER_LEVEL_BY_MP_RT
(
IN_MP_REDEMPTION_RATE_ID	IN FTD_APPS.MILES_POINTS_REDEMPTION_RATE.MP_REDEMPTION_RATE_ID%TYPE,
OUT_CUR OUT TYPES.REF_CURSOR
)
AS
 
BEGIN
   OPEN OUT_CUR FOR
      SELECT sm.source_code, ml.mp_member_level_id 
      FROM ftd_apps.source_master sm LEFT OUTER JOIN ftd_apps.source_mp_member_level ml
      ON trim(sm.source_code) = trim(ml.source_code)
      WHERE sm.mp_redemption_rate_id = in_mp_redemption_rate_id
      ORDER BY sm.source_code,ml.display_order;
    
END GET_SRC_MEMBER_LEVEL_BY_MP_RT;

PROCEDURE UPDATE_INSERT_MORNING_DELIVERY
(
	IN_ID                     	IN DELIVERY_FEE.DELIVERY_FEE_ID%TYPE,
	IN_DESCRIPTION            	IN DELIVERY_FEE.DESCRIPTION%TYPE,
	IN_DELIVERY_FEE	          	IN DELIVERY_FEE.MORNING_DELIVERY_FEE%TYPE,
	IN_UPDATED_BY             	IN DELIVERY_FEE.UPDATED_BY%TYPE,
	IN_REQUESTED_BY           	IN DELIVERY_FEE.REQUESTED_BY%TYPE,
	OUT_STATUS                	OUT VARCHAR2,
	OUT_MESSAGE               	OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates/inserts a record into table delivery_fee.

Input:
        delivery_fee_id        	VARCHAR2
        description      		VARCHAR2
        morning_delivery_fee	NUMBER
        updated_by				VARCHAR2
		requested_by			VARCHAR2
		
Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN
    UPDATE  delivery_fee SET
        description = in_description, 
        morning_delivery_fee = IN_DELIVERY_FEE,         
        requested_by = in_requested_by,
        updated_by = in_updated_by,
        updated_on = sysdate
        WHERE delivery_fee_id = in_id;

  
	IF SQL%NOTFOUND THEN
    INSERT INTO delivery_fee
		(delivery_fee_id,
		description,
        morning_delivery_fee,
        requested_by,
        created_on,
        created_by,
        updated_on,
        updated_by)
    VALUES
		(in_id,
		in_description,
        IN_DELIVERY_FEE,       
        in_requested_by,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by);
	END IF;

	out_status := 'Y';

	EXCEPTION WHEN OTHERS THEN
	BEGIN
		out_status := 'N';
		out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;

END UPDATE_INSERT_MORNING_DELIVERY;

Procedure UPDATE_DELETE_DELIVERY_FEE
(
	IN_DELIVERY_FEE_ID            IN DELIVERY_FEE.DELIVERY_FEE_ID%TYPE,
	IN_REQUESTED_BY               IN DELIVERY_FEE.REQUESTED_BY%TYPE,
	IN_UPDATED_BY                 IN DELIVERY_FEE.UPDATED_BY%TYPE,
	OUT_STATUS                    OUT VARCHAR2,
	OUT_MESSAGE                   OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates and deletes a record of table delivery_fee.

Input:
        delivery_fee_id        	 VARCHAR2
		requested_by			 VARCHAR2
        updated_by				 VARCHAR2		
		
Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN
	UPDATE DELIVERY_FEE SET  
		requested_by = in_requested_by,
        updated_by = in_updated_by,
        created_by = 'DELETION'
        WHERE DELIVERY_FEE_ID = IN_DELIVERY_FEE_ID;

	DELETE FROM DELIVERY_FEE  WHERE DELIVERY_FEE_ID = IN_DELIVERY_FEE_ID; 
   
	OUT_STATUS := 'Y';
   
	EXCEPTION WHEN OTHERS THEN
	BEGIN
		out_status := 'N';
		out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;
	
END UPDATE_DELETE_DELIVERY_FEE;

PROCEDURE GET_SOURCE_BY_DELIVERY_FEE_ID
(
	IN_DELIEVRY_FEE_ID    		IN SOURCE_MASTER.DELIVERY_FEE_ID%TYPE,
	OUT_CUR             		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves all the source codes associated with 
		a given delivery fee id.

Input:
        delivery_fee_id        	VARCHAR2		
		
Output:
        cursor

-----------------------------------------------------------------------------*/
BEGIN
	OPEN OUT_CUR FOR
		SELECT   SOURCE_CODE, DESCRIPTION
		FROM     SOURCE_MASTER
		WHERE    DELIVERY_FEE_ID = IN_DELIEVRY_FEE_ID
		ORDER BY SOURCE_CODE;

END GET_SOURCE_BY_DELIVERY_FEE_ID;

PROCEDURE GET_MORNING_DELIVERY_FEE_HIST
(
	IN_DELIVERY_FEE_ID    		IN DELIVERY_FEE$.DELIVERY_FEE_ID%TYPE,
	OUT_CUR               		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves the history of a given delivery fee id.

Input:
        delivery_fee_id        	VARCHAR2		
		
Output:
        cursor

-----------------------------------------------------------------------------*/
BEGIN
	OPEN OUT_CUR FOR
		select timestamp$ timestamp, operation$ operation, updated_by, TO_CHAR(morning_delivery_fee, '999.99') as delivery_fee, requested_by
		from ftd_apps.DELIVERY_FEE$ where delivery_fee_id = IN_DELIVERY_FEE_ID and operation$ != 'UPD_OLD'
		order by timestamp desc;

END GET_MORNING_DELIVERY_FEE_HIST;

PROCEDURE INSERT_CARRIER_ZIP_CODE
(
IN_ZIP_CODE                   	IN CARRIER_ZIP_CODE.ZIP_CODE%TYPE,
IN_CARRIER_ID                   IN CARRIER_ZIP_CODE.CARRIER%TYPE,
IN_WEEKDAY_BY_NOON_DEL_FLAG     IN CARRIER_ZIP_CODE.WEEKDAY_BY_NOON_DEL_FLAG%TYPE,
IN_SATURDAY_BY_NOON_DEL_FLAG    IN CARRIER_ZIP_CODE.SATURDAY_BY_NOON_DEL_FLAG%TYPE,
IN_SATURDAY_DEL_AVAILABLE_FLAG  IN CARRIER_ZIP_CODE.SATURDAY_DEL_AVAILABLE%TYPE,
IN_USER_ID                   	IN CARRIER_ZIP_CODE.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting carrier zip information.

Input:                              
        zip_code                        varchar2
        carrier_id                      varchar2
        weekday_by_noon_del_flag        varchar2
        weekday_by_noon_del_flag        varchar2
        SATURDAY_DEL_AVAILABLE			    varchar2
        user_id                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR check_zip_exist(in_zip_code varchar2) IS
        select 'Y' from FTD_APPS.CARRIER_ZIP_CODE
        where zip_code = in_zip_code
        and carrier = IN_CARRIER_ID;
        
CURSOR check_flags_match(weekday_flag varchar2, saturday_flag varchar2, saturday_del_avail varchar2) IS
        select 'Y' from FTD_APPS.CARRIER_ZIP_CODE
        where zip_code = IN_ZIP_CODE
        and carrier = IN_CARRIER_ID
        and WEEKDAY_BY_NOON_DEL_FLAG = weekday_flag
        and SATURDAY_BY_NOON_DEL_FLAG = saturday_flag
        and SATURDAY_DEL_AVAILABLE = saturday_del_avail;
        
is_zip_exist CHAR(1) := 'N';
is_flags_match CHAR(1) := 'N';

BEGIN

OPEN check_zip_exist (IN_ZIP_CODE);
			FETCH check_zip_exist INTO is_zip_exist;
      IF check_zip_exist%found THEN
      is_zip_exist := 'Y';
      END IF;

OPEN check_flags_match (IN_WEEKDAY_BY_NOON_DEL_FLAG, IN_SATURDAY_BY_NOON_DEL_FLAG, IN_SATURDAY_DEL_AVAILABLE_FLAG);
			FETCH check_flags_match INTO is_flags_match;
      IF check_flags_match%found THEN
      is_flags_match := 'Y';
      END IF;
      
-- if zip does exist then update it
IF is_zip_exist = 'Y' THEN

    IF is_flags_match = 'Y' THEN
      update ftd_apps.carrier_zip_code set processed_flag='Y',
      CREATED_ON=to_char(sysdate,'dd-MON-yy'),
      CREATED_BY=IN_USER_ID,
      UPDATED_ON=to_char(sysdate,'dd-MON-yy'),
      UPDATED_BY=IN_USER_ID
      where zip_code = IN_ZIP_CODE and carrier = IN_CARRIER_ID;
    END IF;
    
    IF is_flags_match = 'N' THEN
      update ftd_apps.carrier_zip_code set WEEKDAY_BY_NOON_DEL_FLAG=IN_WEEKDAY_BY_NOON_DEL_FLAG,
      SATURDAY_BY_NOON_DEL_FLAG=IN_SATURDAY_BY_NOON_DEL_FLAG,
      SATURDAY_DEL_AVAILABLE=IN_SATURDAY_DEL_AVAILABLE_FLAG,
      processed_flag='Y',
      CREATED_ON=to_char(sysdate,'dd-MON-yy'),
      CREATED_BY=IN_USER_ID,
      UPDATED_ON=to_char(sysdate,'dd-MON-yy'),
      UPDATED_BY=IN_USER_ID
      where zip_code = IN_ZIP_CODE and carrier = IN_CARRIER_ID;      
    END IF;
    
END IF;

-- if zip doesn't exist then insert it
IF is_zip_exist = 'N' THEN
INSERT INTO FTD_APPS.CARRIER_ZIP_CODE
(
		ZIP_CODE,
		CARRIER,
		WEEKDAY_BY_NOON_DEL_FLAG,
		SATURDAY_BY_NOON_DEL_FLAG,
		SATURDAY_DEL_AVAILABLE,
		PROCESSED_FLAG,
		CREATED_ON,
		CREATED_BY,
		UPDATED_ON,
		UPDATED_BY
)
VALUES
(
        IN_ZIP_CODE,
        IN_CARRIER_ID,
        IN_WEEKDAY_BY_NOON_DEL_FLAG,
        IN_SATURDAY_BY_NOON_DEL_FLAG,
        IN_SATURDAY_DEL_AVAILABLE_FLAG,
        'Y',
        to_char(sysdate,'dd-MON-yy'),
        IN_USER_ID,
        to_char(sysdate,'dd-MON-yy'),
        IN_USER_ID
);
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_CARRIER_ZIP_CODE;

PROCEDURE DELETE_CARRIER_ZIP_CODE
(
IN_CARRIER_ID                   IN CARRIER_ZIP_CODE.CARRIER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the carrier zip information.

Input:
        carrier_id                        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

      delete from ftd_apps.carrier_zip_code where carrier = IN_CARRIER_ID and processed_flag='N';
      update ftd_apps.carrier_zip_code set processed_flag='N' where carrier = IN_CARRIER_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_CARRIER_ZIP_CODE;

PROCEDURE GET_ZIP_CODE_REPORT_EMAIL_LIST
(
  OUT_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves ZIP_CODE_REPORT email ids
Input:
        NA
Output:
        out_cur error information

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
  SELECT pager_number email_id
  FROM SITESCOPE.pagers
  where project = 'ZIP_CODE_REPORT';

END GET_ZIP_CODE_REPORT_EMAIL_LIST;

PROCEDURE GET_LAST_ZIP_FILE_RECVD_DATE
(
  IN_CARRIER_ID                   IN CARRIER_ZIP_CODE.CARRIER%TYPE,
  OUT_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves date
Input:
        NA
Output:
        out_cur error information

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
  select distinct created_on from FTD_APPS.carrier_zip_code
  where created_by='CARRIER_ZIPS' and carrier = IN_CARRIER_ID;

END GET_LAST_ZIP_FILE_RECVD_DATE;


PROCEDURE INSERT_FS_PROCESSING_EMAIL
(
IN_EMAIL_ADDRESS		IN VARCHAR2,
OUT_STATUS      OUT VARCHAR2
)
AS
  
BEGIN
  
	INSERT INTO FS_PROCESSING_EMAILS(EMAIL_ADDRESS, CREATED_ON)
	VALUES(IN_EMAIL_ADDRESS, TRUNC(SYSDATE));
	
	OUT_STATUS 	:= 'Y';
	
  EXCEPTION WHEN OTHERS THEN        
	OUT_STATUS := 'N';        
			
END INSERT_FS_PROCESSING_EMAIL;


PROCEDURE CHECK_FS_PROCESSING_EMAILS
(
IN_EMAIL_ADDRESS		IN VARCHAR2,
OUT_STATUS      OUT VARCHAR2
)
AS
  v_count number;
BEGIN
  
	SELECT COUNT(*) into v_count from FS_PROCESSING_EMAILS
	WHERE EMAIL_ADDRESS = IN_EMAIL_ADDRESS AND trunc(CREATED_ON)=trunc(SYSDATE);
	
	IF v_count > 0 THEN 
		OUT_STATUS 	:= 'Y';
	ELSE
		OUT_STATUS := 'N';
	END IF;
	
  EXCEPTION WHEN OTHERS THEN        
		OUT_STATUS := 'N';        
			
END CHECK_FS_PROCESSING_EMAILS;

PROCEDURE  GET_BBN_AVAILABILITY (
    IN_ZIP_CODE		 	      	IN	FTD_APPS.CARRIER_ZIP_CODE.ZIP_CODE%TYPE,
    WEEKDAY_BY_NOON 	  		IN 	VARCHAR2,
    SATURDAY_BY_NOON 	  		IN 	VARCHAR2,    
    OUT_STATUS 			    	OUT VARCHAR2,
    OUT_MESSAGE			    	OUT VARCHAR2
)
as
/*------------------------------------------------------------------------------
 Description:  Gets the BBN order availability (WEEDDAY BY NOON or SATURDAY NOON)
 			   for a given zip code.
--------------------------------------------------------------------------------*/
BEGIN
	IF WEEKDAY_BY_NOON = 'Y' THEN    
        select distinct 'Y' into OUT_STATUS from FTD_APPS.CARRIER_ZIP_CODE 
        where zip_code = IN_ZIP_CODE and WEEKDAY_BY_NOON_DEL_FLAG = 'Y';       
   	ELSE     
        IF SATURDAY_BY_NOON = 'Y' THEN     
        	select distinct 'Y' into OUT_STATUS from FTD_APPS.CARRIER_ZIP_CODE  
        	where zip_code = IN_ZIP_CODE and SATURDAY_BY_NOON_DEL_FLAG = 'Y';        
        END IF;      
    END IF;
    
    EXCEPTION WHEN OTHERS THEN
    BEGIN
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;
	
END GET_BBN_AVAILABILITY;

PROCEDURE GET_OSCAR_SCENARIO_GROUPS
(
  OUT_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves OSCAR_SCENARTIO_GROUP LIST
Input:
        NA
Output:
        out_cur error information

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
  SELECT oscar_scenario_group_id, oscar_scenario_group_name, active_flag
  FROM OSCAR_SCENARIO_GROUPS where active_flag='Y';

END GET_OSCAR_SCENARIO_GROUPS;

PROCEDURE UPDATE_VENDOR_PROD_AVAILABLE (
IN_PRODUCT_ID                   IN FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
IN_VENDOR_ID                    IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
IN_AVAILBLE_STATUS              IN FTD_APPS.VENDOR_PRODUCT.AVAILABLE%TYPE,
IN_UPDATED_BY                   IN FTD_APPS.VENDOR_PRODUCT.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: UPDATES THE VENDOR_PRODUCT.AVAILABLE FLAG TO THE ONE PASSED IN.
------------------------------------------------------------------------------------------------------------------*/
BEGIN

	UPDATE FTD_APPS.VENDOR_PRODUCT 
		SET AVAILABLE = IN_AVAILBLE_STATUS, 
			UPDATED_BY = IN_UPDATED_BY, 
			UPDATED_ON = SYSDATE
		WHERE VENDOR_ID = IN_VENDOR_ID
			AND PRODUCT_SUBCODE_ID = IN_PRODUCT_ID;
	
	OUT_STATUS	:= 'Y';

EXCEPTION
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_VENDOR_PROD_AVAILABLE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_PROD_AVAILABLE;

PROCEDURE UPDATE_PROD_DROPSHIP_DELIVERY (
IN_PRODUCT_ID                   IN FTD_APPS.PRODUCT_MASTER.VENDOR_ID%TYPE,
IN_DROPSHIP_AVAILBLE            IN FTD_APPS.PRODUCT_MASTER.SHIP_METHOD_CARRIER%TYPE,
IN_UPDATED_BY                   IN FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: UPDATES THE PRODUCT_MASTER.SHIP_METHOD_CARRIER FLAG TO THE ONE PASSED IN.
------------------------------------------------------------------------------------------------------------------*/
BEGIN

	UPDATE FTD_APPS.PRODUCT_MASTER 
		SET SHIP_METHOD_CARRIER = IN_DROPSHIP_AVAILBLE, 
			UPDATED_BY = IN_UPDATED_BY, 
			UPDATED_ON = SYSDATE
		WHERE PRODUCT_ID = IN_PRODUCT_ID;
	
	OUT_STATUS	:= 'Y';

EXCEPTION
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PROD_DROPSHIP_DELIVERY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PROD_DROPSHIP_DELIVERY;

PROCEDURE UPDATE_PROD_STATUS (
IN_PRODUCT_ID                   IN    FTD_APPS.PRODUCT_MASTER.VENDOR_ID%TYPE,
IN_STATUS            			      IN    FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_UPDATED_BY                   IN    FTD_APPS.PRODUCT_MASTER.UPDATED_BY%TYPE,
OUT_STATUS                      OUT   VARCHAR2,
OUT_MESSAGE                     OUT   VARCHAR2
)
AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: UPDATES THE PRODUCT_MASTER.STATUS FLAG TO THE ONE PASSED IN.
------------------------------------------------------------------------------------------------------------------*/
BEGIN

	UPDATE FTD_APPS.PRODUCT_MASTER 
		SET STATUS = IN_STATUS, 
			UPDATED_BY = IN_UPDATED_BY, 
			UPDATED_ON = SYSDATE
		WHERE PRODUCT_ID = IN_PRODUCT_ID;
	
	OUT_STATUS	:= 'Y';

EXCEPTION
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PROD_STATUS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PROD_STATUS;

PROCEDURE INSERT_STORE_FEED (
IN_FEED_XML                   	IN    FTD_APPS.STORE_FEEDS.STORE_FEED_XML %TYPE,
IN_FEED_STATUS            	    IN    FTD_APPS.STORE_FEEDS.STATUS%TYPE,
IN_FEED_TYPE                    IN    FTD_APPS.STORE_FEEDS.STORE_FEED_TYPE%TYPE,
IN_USER_ID						IN	  FTD_APPS.STORE_FEEDS.CREATED_BY%TYPE,
OUT_FEED_ID                     OUT   FTD_APPS.STORE_FEEDS.STORE_FEED_ID%type,
OUT_STATUS                      OUT   VARCHAR2,
OUT_MESSAGE                     OUT   VARCHAR2
)
AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: INSERTS FEEDS INTO FTD_APPS.STORE_FEEDS TABLE.
------------------------------------------------------------------------------------------------------------------*/
BEGIN
	
	INSERT INTO FTD_APPS.STORE_FEEDS 
		(
                STORE_FEED_ID,
                STORE_FEED_XML,
                STORE_FEED_TYPE,
                STATUS,
                Created_on,
                created_by,
                updated_on,
                updated_by
        )
        VALUES
        (
                STORE_FEEDS_ID_SQ.NEXTVAL,
                IN_FEED_XML,
                IN_FEED_TYPE,
                IN_FEED_STATUS,
                SYSDATE,
                IN_USER_ID,
                SYSDATE,
                IN_USER_ID
        );
        
       	OUT_FEED_ID := STORE_FEEDS_ID_SQ.CURRVAl;
		OUT_STATUS	:= 'Y';

EXCEPTION
  WHEN OTHERS THEN
  BEGIN
    OUT_STATUS	:= 'N';
    OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_STORE_FEED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_STORE_FEED;

PROCEDURE GET_STORE_FEEDS
(
IN_FEED_TYPE IN FTD_APPS.STORE_FEEDS.STORE_FEED_TYPE%TYPE, 
IN_FEED_STATUS IN FTD_APPS.STORE_FEEDS.STATUS%TYPE, 
IN_BATCH_SIZE IN NUMBER,
OUT_STORE_FEEDS OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves store feeds for the given store_feed_type and status from table STORE_FEEDS.
-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_STORE_FEEDS FOR
       SELECT * 
       FROM FTD_APPS.STORE_FEEDS
       WHERE STORE_FEED_TYPE = IN_FEED_TYPE AND STATUS = IN_FEED_STATUS AND ROWNUM<=IN_BATCH_SIZE;

END GET_STORE_FEEDS;

PROCEDURE UPDATE_FEED_STATUS
(
IN_FEED_ID IN FTD_APPS.STORE_FEEDS.STORE_FEED_ID%TYPE, 
IN_FEED_STATUS IN FTD_APPS.STORE_FEEDS.STATUS%TYPE, 
IN_USER_ID IN FTD_APPS.STORE_FEEDS.UPDATED_BY%TYPE, 
OUT_STATUS OUT VARCHAR2,
OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a feed record into table STORE_FEEDS.
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE FTD_APPS.STORE_FEEDS
     SET STATUS = IN_FEED_STATUS,
         UPDATED_BY = IN_USER_ID,
         UPDATED_ON = SYSDATE
   WHERE STORE_FEED_ID = IN_FEED_ID;

  IF SQL%FOUND THEN
     OUT_STATUS := 'Y';
  ELSE
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'WARNING: No store_feed updated for store_feed_id ' || IN_FEED_ID ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       OUT_STATUS := 'N';
       OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FEED_STATUS;

PROCEDURE GET_UNPROCESSED_STORE_FEEDS
(
IN_FEED_TYPE 		IN 		FTD_APPS.STORE_FEEDS.STORE_FEED_TYPE%TYPE, 
IN_RETRY_COUNT 		IN 		FTD_APPS.STORE_FEEDS.RETRY_COUNT%TYPE, 
IN_BATCH_SIZE 		IN 		NUMBER,
OUT_STORE_FEEDS 	OUT 	TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves store feeds for the given store_feed_type and status from table STORE_FEEDS.
-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_STORE_FEEDS FOR
       SELECT * 
       FROM FTD_APPS.STORE_FEEDS
       WHERE STORE_FEED_TYPE = IN_FEED_TYPE 
       AND(STATUS = 'NEW' OR ( STATUS = 'FAILED' AND RETRY_COUNT <= IN_RETRY_COUNT)) 
       AND ROWNUM <= IN_BATCH_SIZE
       order by STORE_FEED_ID;

END GET_UNPROCESSED_STORE_FEEDS;

PROCEDURE UPDATE_FEED_STATUS
(
IN_FEED_ID 			IN 		FTD_APPS.STORE_FEEDS.STORE_FEED_ID%TYPE, 
IN_FEED_STATUS 		IN 		FTD_APPS.STORE_FEEDS.STATUS%TYPE,
IN_STATUS_INFO 		IN 		FTD_APPS.STORE_FEEDS.STATUS_INFO%TYPE,
IN_USER_ID 			IN 		FTD_APPS.STORE_FEEDS.UPDATED_BY%TYPE, 
IN_INC_COUNT  		IN 		CHAR,
OUT_STATUS 			OUT 	VARCHAR2,
OUT_MESSAGE 		OUT 	VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a feed record into table STORE_FEEDS.
-----------------------------------------------------------------------------*/

V_RETRY_COUNT NUMBER;

BEGIN

  SELECT RETRY_COUNT INTO V_RETRY_COUNT FROM FTD_APPS.STORE_FEEDS WHERE STORE_FEED_ID = IN_FEED_ID;

  IF IN_INC_COUNT = 'Y' THEN
    V_RETRY_COUNT := V_RETRY_COUNT + 1;
  END IF;

  UPDATE FTD_APPS.STORE_FEEDS
     SET STATUS = IN_FEED_STATUS,
     STATUS_INFO = IN_STATUS_INFO,
     UPDATED_BY = IN_USER_ID,
     UPDATED_ON = SYSDATE,
     RETRY_COUNT = V_RETRY_COUNT
   WHERE STORE_FEED_ID = IN_FEED_ID;

  IF SQL%FOUND THEN
     OUT_STATUS := 'Y';
  ELSE
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'WARNING: No store_feed updated for store_feed_id ' || IN_FEED_ID ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       OUT_STATUS := 'N';
       OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FEED_STATUS;

PROCEDURE CARRIER_HAS_BBN
(
IN_CARRIER     IN FTD_APPS.CARRIER_ZIP_CODE.CARRIER%TYPE,
IN_ZIP_CODE    IN FTD_APPS.CARRIER_ZIP_CODE.ZIP_CODE%TYPE,
IN_DELIVERY_TYPE IN VARCHAR2,
OUT_CARRIER_BBN_FLAG           OUT varchar2
)
AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        carrier id allows morning delivery for the passed in zip code/delivery day combination.  
        Delivery Type will contain either WEEKDAY or SATURDAY, depending on what the ship method is on the venus record.
        Y will be returned if the carrier allows morning delivery, otherwise N will be returned.


Input:
        IN_CARRIER_ID       VARCHAR2
        IN_ZIP_CODE         VARCHAR2
        IN_DELIVERY_DAY     VARCHAR2

Output:
        OUT_CARRIER_BBN_FLAG                 VARCHAR2

-----------------------------------------------------------------------------*/
v_count     number(4);
BEGIN
     IF IN_DELIVERY_TYPE = 'WEEKDAY' THEN      
        SELECT count(*)
          INTO v_count
        FROM  ftd_apps.carrier_zip_code czc
        WHERE czc.carrier = IN_CARRIER
        AND   czc.zip_code = IN_ZIP_CODE
        AND   czc.weekday_by_noon_del_flag = 'Y';
        
    ELSE 
        SELECT count(*)
          INTO v_count
        FROM  ftd_apps.carrier_zip_code czc
        WHERE czc.carrier = IN_CARRIER
        AND   czc.zip_code = IN_ZIP_CODE
        AND   czc.saturday_by_noon_del_flag = 'Y';
    END IF;
    
    IF v_count > 0 THEN
      OUT_CARRIER_BBN_FLAG := 'Y';
    ELSE
      OUT_CARRIER_BBN_FLAG := 'N';
    END IF;     

END CARRIER_HAS_BBN;

PROCEDURE CONVERT_TIME_ZONE_BY_STATE
(
  IN_STATE_ID IN FTD_APPS.STATE_MASTER.STATE_MASTER_ID%TYPE,
  IN_TIME      IN VARCHAR2,
  OUT_TIME     OUT VARCHAR2
)
AS
  v_time_zone NUMBER;
BEGIN
  select time_zone into v_time_zone from ftd_apps.state_master where STATE_MASTER_ID = IN_STATE_ID;
  
  OUT_TIME := FTD_APPS.MISC_PKG.CHANGE_TIMES_TO_REC_TIMEZONE(IN_TIME,v_time_zone,IN_STATE_ID);
END CONVERT_TIME_ZONE_BY_STATE;

PROCEDURE DELETE_CSR_VIEWED_ID
(
IN_CSR_ID                       IN CSR_VIEWED_IDS.CSR_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_VIEWED_IDS.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_VIEWED_IDS.ENTITY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the history of viewed
        record entities for the given csr and entity type

Input:
        csr_id                          varchar2
        entity_type                     varchar2 (FLORIST_MASTER, MERCURY)
        entity_id                     varchar2 

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM csr_viewed_ids
WHERE   csr_id = in_csr_id
AND     entity_type = in_entity_type
AND     entity_id = in_entity_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_CSR_VIEWED_ID;

PROCEDURE DELETE_FS_PROCESSING_EMAIL
(
IN_EMAIL_ADDRESS		IN VARCHAR2,
OUT_STATUS      		OUT VARCHAR2
)
AS
BEGIN
  
	DELETE from FS_PROCESSING_EMAILS
	WHERE EMAIL_ADDRESS = IN_EMAIL_ADDRESS AND trunc(CREATED_ON)=trunc(SYSDATE);
	
	OUT_STATUS 	:= 'Y';
	
	COMMIT;
	
  EXCEPTION WHEN OTHERS THEN        
		OUT_STATUS := 'N';        
			
END DELETE_FS_PROCESSING_EMAIL;

PROCEDURE DELETE_PRODUCT
(
IN_PRODUCT_ID              	IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a product

Input:
        order_detail_id                 number
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_product_order_count	int := 0;
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

--check to see if any orders have been placed against this product
--if there are any DO NOT delete product 
select count(*) into v_product_order_count
from scrub.order_details od
where od.product_id = IN_PRODUCT_ID;

IF v_product_order_count = 0 THEN
	select count(*) into v_product_order_count
	from clean.order_details od
	where od.product_id = IN_PRODUCT_ID; 
END IF;

IF v_product_order_count = 0 THEN

	delete from ftd_apps.product_addon
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_colors
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_company_xref
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_component_sku$
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_component_sku
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_hp
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_keywords
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_ship_dates
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_ship_methods
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_snapshot
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_snapshot_1
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_snapshot_2
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.csz_products_stage
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.csz_products
	where PRODUCT_ID = IN_PRODUCT_ID;

	delete from ftd_apps.product_recipient_search
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_source
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_master$
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_master$_acct
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	delete from ftd_apps.product_master
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	--this is in here again to ensure the table is cleaned out because when you delete from the product_master table it inserts a delete record
	delete from ftd_apps.product_master$
	where PRODUCT_ID = IN_PRODUCT_ID;
	
	--this is in here again to ensure the table is cleaned out because when you delete from the product_master table it inserts a delete record
	delete from ftd_apps.product_master$_acct
	where PRODUCT_ID = IN_PRODUCT_ID;

END IF;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED DELETE_PRODUCT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_PRODUCT;

PROCEDURE INSERT_WEST_PRODUCT_UPDATE
(
IN_NOVATOR_ID  					IN FTD_APPS.WEST_PRODUCT_UPDATE.NOVATOR_ID%TYPE,
IN_PQUAD_PRODUCT_ID             IN FTD_APPS.WEST_PRODUCT_UPDATE.PQUAD_PRODUCT_ID%TYPE,
IN_PROCESSED				    IN FTD_APPS.WEST_PRODUCT_UPDATE.PROCESSED%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a record into the ftd_apps.west_product_update table.

Input:
        novator_id						varchar2
        pquad_product_id				number
        processed	                    char
     
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT 
INTO 
    FTD_APPS.WEST_PRODUCT_UPDATE 
    (
        WEST_PRODUCT_UPDATE_ID, 
        NOVATOR_ID, 
        PQUAD_PRODUCT_ID, 
        PROCESSED, 
        CREATED_ON, 
        CREATED_BY, 
        UPDATED_ON, 
        UPDATED_BY
    ) 
    VALUES 
    (
        west_product_update_id_sq.nextval, 
        IN_NOVATOR_ID, 
        IN_PQUAD_PRODUCT_ID, 
        IN_PROCESSED, 
        SYSDATE, 
        'SHIP_WEST_PRODUCT_UPDATE', 
        SYSDATE, 
        'SHIP_WEST_PRODUCT_UPDATE'
    );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_WEST_PRODUCT_UPDATE;

PROCEDURE UPDATE_WEST_PRODUCT_STATUS
(
  IN_WEST_PRODUCT_UPDATE_ID     IN FTD_APPS.WEST_PRODUCT_UPDATE.WEST_PRODUCT_UPDATE_ID%TYPE,
  IN_PROCESSED				    IN FTD_APPS.WEST_PRODUCT_UPDATE.PROCESSED%TYPE,  
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
)
AS 
BEGIN

      UPDATE FTD_APPS.WEST_PRODUCT_UPDATE
         SET PROCESSED           		= IN_PROCESSED,
             UPDATED_BY          		= 'WEST_PRODUCT_UPDATED_MONITOR_JOB',
             UPDATED_ON          		= SYSDATE
       WHERE WEST_PRODUCT_UPDATE_ID     = IN_WEST_PRODUCT_UPDATE_ID;

IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: West Product Status not updated for West Product Update Id ' || IN_WEST_PRODUCT_UPDATE_ID ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPDATE_WEST_PRODUCT_STATUS;

PROCEDURE GET_PQUAD_PRODUCT_DETAILS
(
IN_PQUAD_PRODUCT_ID             IN FTD_APPS.PRODUCT_MASTER.PQUAD_PRODUCT_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning pertinent product details

Input:
        pquad_product_id                varchar2
  
Output:
        cursor containing pertinent product details for pquad id passed in

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
    SELECT pm.novator_id, pm.pquad_product_id, pm.status, pm.product_type, pm.ship_method_florist, pm.ship_method_carrier, pm.product_id
	 FROM ftd_apps.product_master pm
	 WHERE pm.PQUAD_PRODUCT_ID = IN_PQUAD_PRODUCT_ID;

END GET_PQUAD_PRODUCT_DETAILS;

PROCEDURE SEND_WEST_PRODUCT_UPDATES
(
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS

   mon_cursor types.ref_cursor;
   type mon_type_rec is record
     (novator_id		ftd_apps.west_product_update.novator_id%Type,
      pquad_product_id		ftd_apps.west_product_update.pquad_product_id%Type,
      west_product_update_id	ftd_apps.west_product_update.west_product_update_id%Type
     );
   type mon_type_tab is table of mon_type_rec;
   mon_table mon_type_tab;

   c utl_smtp.connection;
   v_status varchar2(1000);
   v_message varchar2(1000);

begin

   open mon_cursor for
     select novator_id, pquad_product_id, west_product_update_id
       from ftd_apps.west_product_update
       where processed = 'N'
       and trunc(created_on) = trunc(sysdate)
       order by novator_id asc;


  fetch mon_cursor bulk collect into mon_table;
   close mon_cursor;

   if mon_table.count > 0 then

      /* Set up email */
      ops$oracle.mail_pkg.init_email(c,'DSI_VENDOR_PRICE_ALERTS','FTDW Product(s) Made Unavailable or Dropship Delivery Removed',v_status, v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;

       -- This call will allow us to format the body in html for a pretty email
      utl_smtp.write_raw_data(c, utl_raw.cast_to_raw('Content-Type' || ': ' || 
                                               'text/html; charset=iso-8859-1' || utl_tcp.CRLF));

      
      utl_smtp.write_data(c, utl_tcp.crlf || 'The following FTD West products have been made unavailable or dropship delivery removed.<br><br>');
      
      -- Put headings as the top row of a table - size/align accordingly
      utl_smtp.write_data(c, '<table border=1>'    ||
                                     '<tr valign=top><td align=center width=120><b>Novator Id</b></td>' ||
                                         '<td align=center width=120><b>PQuad Product Id</b></td>'    ||
                                         '<td align=left   width=0></td>'        ||
                                      '</tr>');
      
      FOR i IN 1..mon_table.COUNT LOOP
          -- Add rows from query into the table
          utl_smtp.write_data(c, '<tr valign=top>' ||
                                    '<td align=center>' || mon_table(i).novator_id || '</td>' ||
                                    '<td align=center>' || mon_table(i).pquad_product_id || '</td>' ||
                                    '<td></td>' ||                                                           
                                 '</tr>');   

      END LOOP;
      
      -- Close the table
      utl_smtp.write_data(c, '</table><br><br>'); 
      
      ops$oracle.mail_pkg.close_email(c,v_status,v_message);
      if v_status <> 'Y' then
         raise_application_error(-20000,v_message,true);
      end if;
      
      if v_status = 'Y' then
        FOR i IN 1..mon_table.COUNT LOOP
          update ftd_apps.west_product_update
	    set processed = 'Y',
	    updated_on = SYSDATE,
	    updated_by = 'SHIP_WEST_PRODUCT_UPDATE'
	    where west_product_update_id = mon_table(i).west_product_update_id;
	END LOOP;
      end if;
   END IF;
   
   IF SQL%FOUND THEN
     out_status := 'Y';
   ELSE
     out_status := 'N';
     out_message := 'WARNING: SEND_WEST_PRODUCT_UPDATES failed.';
   END IF;

   EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END SEND_WEST_PRODUCT_UPDATES;

PROCEDURE IS_SINGLE_FLORIST_ZIPCODE
(
 IN_ZIP_CODE    IN florist_zips.ZIP_CODE%TYPE,
 OUT_STATUS     OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the whether a zipcode is single florist or not.

Input: 
        zipcode              VARCHAR2

Output:
        varchar2

-----------------------------------------------------------------------------*/

v_status Number;

BEGIN


   select count(*) into v_status
   from 
      ftd_apps.florist_zips 
   where zip_code = IN_ZIP_CODE and zip_code in (
    select 
      fz.zip_code
    from
      ftd_apps.florist_zips fz,
      ftd_apps.florist_master fm 
    where 
      fm.florist_id = fz.florist_id
    and
      fm.status = 'Active'
    and
      fm.record_type='R'
    group by fz.zip_code having count(*)='1');

   
   IF v_status = 0 THEN
     OUT_STATUS := 'N';
   ELSE 
      OUT_STATUS := 'Y';
   END IF;   


END IS_SINGLE_FLORIST_ZIPCODE;


FUNCTION GET_PDP_PERSONALIZATION_STR
(
  IN_ORDER_DETAIL_ID        IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE 
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
     Returns the PDP (West) personalization fields as a single string suitable 
     for display in front-end mouseovers.  Note that "&" is replaced with "&amp;"
     and single quotes are changed to back-ticks (since mouseovers can't deal with them).  
     A PDP order contains the 'PersonalizationPayload' tag in the 
     personalization_data column, so data is only returned if this condition is met.
     If it's not PDP, or the data is not properly formed, an appropriate 'Unable to display...'
     message is returned.

-----------------------------------------------------------------------------*/
v_return_str VARCHAR2(4000);
v_is_pdp_order NUMBER;

CURSOR pz_cur IS
   select name_str, value_str, order_str 
   from 
   (
   SELECT XMLTYPE(substr( pData, 10, length(pData)-12)) persData 
   from (select 
      XMLQUERY('/personalizations/personalization[1]/data/text()' passing XMLTYPE(personalization_data) returning content).getClobVal() as pData
     FROM clean.order_details
     WHERE order_detail_id = IN_ORDER_DETAIL_ID)
     ),
     XMLTable('/Accessories/Personalization/PersonalizationItems'
           PASSING persData
           COLUMNS
                   name_str  varchar2(100)  PATH 'DisplayName',
                   value_str varchar2(2000) PATH 'Value',
                   order_str varchar2(10)   PATH 'SortOrder'
     ) 
   order by to_number(order_str);
   
BEGIN
   BEGIN
      v_is_pdp_order := 0;
      v_return_str := '';
      
      -- Confirm it's a PDP personalization
      --
      SELECT count(*) 
      INTO v_is_pdp_order
      FROM clean.order_details  
      WHERE instr(personalization_data, 'PersonalizationPayload') > 0 
      AND order_detail_id = IN_ORDER_DETAIL_ID;
      
      IF v_is_pdp_order > 0 THEN
      
        -- Use cursor to build single string containing all PDP personalization data
        --
        FOR rec IN pz_cur LOOP
           BEGIN
              v_return_str := CONCAT(v_return_str, rec.name_str || ': ' || rec.value_str || '<br/>');
           END;
        END LOOP;
      ELSE 
        v_return_str := 'Unable to display personalization';
      END IF;
   EXCEPTION WHEN OTHERS THEN
      v_return_str := 'Unable to display personalization - Contact Production Support';
   END;
      
   RETURN v_return_str;   
END GET_PDP_PERSONALIZATION_STR;

PROCEDURE GET_GIFT_CODE_COMPANIES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for retrieving all of the companies
             gift codes can be created for

Input:  None

Output: cursor containing company info
        Out status
        Out message if status is not 'Y'

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
        SELECT  COMPANY_ID,
                COMPANY_NAME,
                INTERNET_ORIGIN
        FROM    FTD_APPS.COMPANY_MASTER
        WHERE   gift_code_display_flag = 'Y';
   
END GET_GIFT_CODE_COMPANIES;



END MISC_PKG;
.
/
