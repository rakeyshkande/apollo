create or replace trigger ftd_apps.phoenix_product_mapping_$
after insert or update or delete on ftd_apps.phoenix_product_mapping
referencing old as old new as new
for each row
begin

   if inserting then

      insert into ftd_apps.phoenix_product_mapping$ (
	ORIGINAL_PRODUCT_ID,
	NEW_PRODUCT_ID,
	CREATED_ON,
	CREATED_BY,
	UPDATED_ON,
	UPDATED_BY,
        PRODUCT_PRICE_POINT_KEY,
	operation$, timestamp$
      ) values (
         :NEW.ORIGINAL_PRODUCT_ID,
         :NEW.NEW_PRODUCT_ID,
         :NEW.CREATED_ON,
         :NEW.CREATED_BY,
         :NEW.UPDATED_ON,
         :NEW.UPDATED_BY,
         :NEW.PRODUCT_PRICE_POINT_KEY,
         'INS',systimestamp);

   elsif updating then

      insert into ftd_apps.phoenix_product_mapping$ (
	ORIGINAL_PRODUCT_ID,
	NEW_PRODUCT_ID,
	CREATED_ON,
	CREATED_BY,
	UPDATED_ON,
	UPDATED_BY,
        PRODUCT_PRICE_POINT_KEY,
	operation$, timestamp$
      ) values (
         :OLD.ORIGINAL_PRODUCT_ID,
         :OLD.NEW_PRODUCT_ID,
         :OLD.CREATED_ON,
         :OLD.CREATED_BY,
         :OLD.UPDATED_ON,
         :OLD.UPDATED_BY,
         :OLD.PRODUCT_PRICE_POINT_KEY,
         'UPD_OLD',systimestamp);

      insert into ftd_apps.phoenix_product_mapping$ (
	ORIGINAL_PRODUCT_ID,
	NEW_PRODUCT_ID,
	CREATED_ON,
	CREATED_BY,
	UPDATED_ON,
	UPDATED_BY,
        PRODUCT_PRICE_POINT_KEY,
	operation$, timestamp$
      ) values (
         :NEW.ORIGINAL_PRODUCT_ID,
         :NEW.NEW_PRODUCT_ID,
         :NEW.CREATED_ON,
         :NEW.CREATED_BY,
         :NEW.UPDATED_ON,
         :NEW.UPDATED_BY,
         :NEW.PRODUCT_PRICE_POINT_KEY,
         'UPD_NEW',systimestamp);

   elsif deleting then

      insert into ftd_apps.phoenix_product_mapping$ (
	ORIGINAL_PRODUCT_ID,
	NEW_PRODUCT_ID,
	CREATED_ON,
	CREATED_BY,
	UPDATED_ON,
	UPDATED_BY,
        PRODUCT_PRICE_POINT_KEY,
	operation$, timestamp$
      ) values (
         :OLD.ORIGINAL_PRODUCT_ID,
         :OLD.NEW_PRODUCT_ID,
         :OLD.CREATED_ON, 
         :OLD.CREATED_BY,
         :OLD.UPDATED_ON,
         :OLD.UPDATED_BY,
         :OLD.PRODUCT_PRICE_POINT_KEY,
         'DEL',systimestamp);

   end if;

end;
/
