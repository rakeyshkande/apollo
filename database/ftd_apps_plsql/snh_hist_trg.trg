-- FTD_APPS.SNH_HIST_TRG trigger to populate FTD_APPS.SNH_HIST history table

CREATE OR REPLACE TRIGGER FTD_APPS.snh_hist_trg
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.snh REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW

BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.snh_hist (
      	snh_hist_id,
      	snh_id,
      	first_order_domestic,
      	first_order_international,
      	updated_on,      	
        VENDOR_CHARGE,          
        VENDOR_SAT_UPCHARGE,
        VENDOR_SUN_UPCHARGE,
        VENDOR_MON_UPCHARGE,
        expired_on,
        SAME_DAY_UPCHARGE,
        SAME_DAY_UPCHARGE_FS
      ) VALUES (
        snh_hist_sq.nextval,
      	:NEW.snh_id,
		:NEW.first_order_domestic,
        :NEW.first_order_international,
      	:NEW.updated_on,      	
        :NEW.VENDOR_CHARGE,          
        :NEW.VENDOR_SAT_UPCHARGE,
        :NEW.VENDOR_SUN_UPCHARGE,
        :NEW.VENDOR_MON_UPCHARGE,
         null,
        :NEW.SAME_DAY_UPCHARGE,
        :NEW.SAME_DAY_UPCHARGE_FS
         );

   ELSIF UPDATING  THEN

      update snh_hist set expired_on = sysdate where snh_id = :new.snh_id and expired_on is null;

      INSERT INTO ftd_apps.snh_hist (
      	snh_hist_id,
      	snh_id,
      	first_order_domestic,
      	first_order_international,
      	updated_on,      	
        VENDOR_CHARGE,          
        VENDOR_SAT_UPCHARGE,
        VENDOR_SUN_UPCHARGE,
        VENDOR_MON_UPCHARGE,
        expired_on,
        SAME_DAY_UPCHARGE,
        SAME_DAY_UPCHARGE_FS
      ) VALUES (
        snh_hist_sq.nextval,
      	:NEW.snh_id,
	    :NEW.first_order_domestic,
        :NEW.first_order_international,
      	:NEW.updated_on,      	
        :NEW.VENDOR_CHARGE,          
        :NEW.VENDOR_SAT_UPCHARGE,
        :NEW.VENDOR_SUN_UPCHARGE,
        :NEW.VENDOR_MON_UPCHARGE,
         null,
        :NEW.SAME_DAY_UPCHARGE,
        :NEW.SAME_DAY_UPCHARGE_FS
         );

   ELSIF DELETING  THEN

      update snh_hist set expired_on = sysdate where snh_id = :old.snh_id and expired_on is null;

   END IF;

END;
/
