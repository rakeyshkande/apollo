create or replace 
FUNCTION FTD_APPS.SP_GET_ACTIVE_OCCASIONS RETURN types.ref_cursor

 --==============================================================================
--
-- Name:    SP_GET_ACTIVE_OCCASIONS
-- Type:    Function
-- Syntax:  SP_GET_ACTIVE_OCCASIONS ( )
-- Returns: ref_cursor for
--          OCCASION_ID     NUMBER
--          OCCASION_DESCRIPTION    VARCHAR2(10)
--
-- Description:   Queries the OCCASION table and returns a ref cursor for row.
--
--===========================================================

AS
    occasions_cursor types.ref_cursor;
BEGIN
    OPEN OCCASIONS_CURSOR FOR
      SELECT
        OCCASION_ID,
        DESCRIPTION,
        INDEX_ID,
        DISPLAY_ORDER
      FROM FTD_APPS.OCCASION
      WHERE ACTIVE = 'Y' 
      AND BOX_X_DESCRIPTION IS NOT NULL
      ORDER BY OCCASION_ID;

    RETURN occasions_cursor;
END;
.
/
