CREATE OR REPLACE
FUNCTION ftd_apps.OE_SUB_INDEX_LIST_SFMB (
liveFlag                IN VARCHAR2,
dropshipFlag            IN VARCHAR2,
countryID               IN VARCHAR2,
sourceCodeID			IN VARCHAR2
)
 RETURN types.ref_cursor
--==========================================================================
--
-- Name:    OE_SUB_INDEX_LIST
-- Type:    Function
-- Syntax:  OE_SUB_INDEX_LIST (liveFlag IN VARCHAR2,
--                              dropshipFlag IN VARCHAR2,
--                              countryID IN VARCHAR2)
--								sourceCodeID IN VARCHAR2
-- Returns: ref_cursor for
--          parentIndexId     NUMBER
--          indexId           NUMBER
--          indexName         VARCHAR2(100)
--          indexFileName     VARCHAR2(300)
--          displayOrder      NUMBER
--
-- Description:   Gets all subindexes directly under an Occasion or Category
--                top level index and returns a ref cursor to the rows.
--                Search indexes are always ignored.  If dropshipFlag is
--                provided as 'Y', indexes that contain ONLY dropship products
--                are ignored.  Live flag 'Y' or 'N' indicates that only indexes
--                for the live or the test call center app respectively should
--                be returned.  Country ID if provided narrows down to just
--                those indexes with the appropriate COUNTRY_ID field value.
--
--==========================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT pi.parent_index_id as "parentIndexId",
              pi.index_id as "indexId",
              pi.index_name as "name",
              pi.index_file_name as "indexFileName",
              pi.display_order as "displayOrder"
         FROM PRODUCT_INDEX pi, SOURCE sc
         WHERE pi.parent_index_id IN
              ( SELECT pt.index_id FROM PRODUCT_INDEX pt
                WHERE pt.parent_index_id IS NULL
                AND ( pt.occasions_flag = 'Y' OR pt.products_flag = 'Y' )
                AND pt.search_index_flag = 'N'
                AND pt.call_center_live_flag = DECODE(liveFlag, 'Y', 'Y', pt.call_center_live_flag)
                AND pt.call_center_test_flag = DECODE(liveFlag, 'N', 'Y', pt.call_center_test_flag)
                AND pt.dropship_flag = DECODE(dropshipFlag, 'Y', 'N', pt.dropship_flag)
                AND NVL(pt.country_id, '@@') = DECODE(countryID, NULL, NVL(pt.country_id, '@@'), countryID)
              )

         -- Ignore search indexes for purposes of this query
         AND pi.search_index_flag = 'N'

         -- liveFlag 'Y' indicates CALL_CENTER_LIVE_FLAG should be 'Y'
         AND pi.call_center_live_flag = DECODE(liveFlag, 'Y', 'Y', pi.call_center_live_flag)

         -- liveFlag 'N' indicates CALL_CENTER_TEST_FLAG should be 'Y'
         AND pi.call_center_test_flag = DECODE(liveFlag, 'N', 'Y', pi.call_center_test_flag)

         -- dropshipFlag 'Y' indicates DROPSHIP_FLAG should be 'N' (not dropship only)
         AND pi.dropship_flag = DECODE(dropshipFlag, 'Y', 'N', pi.dropship_flag)

         -- narrow by countryID if provided
         AND NVL(pi.country_id, '@@') = DECODE(countryID, NULL, NVL(pi.country_id, '@@'), countryID)

		 AND sc.SOURCE_CODE = sourceCodeID

		 -- only pull indexes for the company in the source code
		 AND DECODE(pi.company_id, null, 'FTD', pi.company_id) = DECODE(sc.company_id, null, 'FTD', sc.company_id)

         ORDER BY pi.parent_index_id, pi.display_order;

    RETURN cur_cursor;
END
;
.
/
