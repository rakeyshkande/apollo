CREATE OR REPLACE
PACKAGE BODY ftd_apps.PROMOTIONS_MAINT_PKG AS

PROCEDURE UPDATE_SOURCE_PRICE_HEADER
(
  IN_SOURCE_CODE                    IN  SOURCE_MASTER.SOURCE_CODE%TYPE,
  IN_PRICE_HEADER_ID                IN  SOURCE_MASTER.PRICE_HEADER_ID%TYPE,
  OUT_STATUS                        OUT VARCHAR2,
  OUT_MESSAGE                       OUT VARCHAR2
) AS

BEGIN

    UPDATE source_master
    set price_header_id = in_price_header_id,
        updated_on = sysdate,
        updated_by = 'APE'
    where source_code = in_source_code;

    out_status := 'Y';
    out_message := NULL;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED: [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END UPDATE_SOURCE_PRICE_HEADER;

PROCEDURE CREATE_SOURCE_CODE
(
  IN_MASTER_SOURCE_CODE             IN  SOURCE_MASTER.SOURCE_CODE%TYPE,
  IN_PRICE_HEADER_ID                IN  SOURCE_MASTER.PRICE_HEADER_ID%TYPE,
  OUT_SOURCE_CODE                   OUT VARCHAR2,
  OUT_STATUS                        OUT VARCHAR2,
  OUT_MESSAGE                       OUT VARCHAR2
) AS

CURSOR get_reserved_source_code is
    select min(source_code)
    from ftd_apps.source_master_reserve
    where source_type = 'APE'
    and status = 'Inactive';

CURSOR price_header_cur is
    select description
    from ftd_apps.price_header
    where price_header_id = in_price_header_id;

CURSOR source_master_cur is
    select *
    from ftd_apps.source_master
    where source_code = in_master_source_code;

CURSOR parse_cur is
    select product_attr_restr_id
    from ftd_apps.product_attr_restr_source_excl
    where source_code = in_master_source_code;

v_description varchar2(80);

BEGIN

    open get_reserved_source_code;
    fetch get_reserved_source_code into out_source_code;
    close get_reserved_source_code;

    open price_header_cur;
    fetch price_header_cur into v_description;
    close price_header_cur;

    v_description := 'APE ' || in_master_source_code || ' ' || v_description;

    FOR rec in SOURCE_MASTER_CUR loop
        FTD_APPS.SOURCE_MAINT_PKG.UPDATE_INSERT_SOURCE_MASTER (
            IN_SOURCE_CODE => out_source_code,
            IN_DESCRIPTION => v_description,
            IN_SNH_ID => rec.snh_id,
            IN_PRICE_HEADER_ID => in_price_header_id,
            IN_SEND_TO_SCRUB => rec.send_to_scrub,
            IN_FRAUD_FLAG => rec.fraud_flag,
            IN_ENABLE_LP_PROCESSING => rec.enable_lp_processing,
            IN_EMERGENCY_TEXT_FLAG => rec.emergency_text_flag,
            IN_REQUIRES_DELIVERY_CONFIRM => rec.requires_delivery_confirmation,
            IN_SOURCE_TYPE => rec.source_type,
            IN_DEPARTMENT_CODE => rec.department_code,
            IN_START_DATE => sysdate,
            IN_END_DATE => null,
            IN_PAYMENT_METHOD_ID => rec.payment_method_id,
            IN_LIST_CODE_FLAG => rec.list_code_flag,
            IN_DEFAULT_SOURCE_CODE_FLAG => rec.default_source_code_flag,
            IN_BILLING_INFO_PROMPT => rec.billing_info_prompt,
            IN_BILLING_INFO_LOGIC => rec.billing_info_logic,
            IN_MARKETING_GROUP => rec.marketing_group,
            IN_EXTERNAL_CALL_CENTER_FLAG => rec.external_call_center_flag,
            IN_HIGHLIGHT_DESCRIPTION_FLAG => rec.highlight_description_flag,
            IN_DISCOUNT_ALLOWED_FLAG => rec.discount_allowed_flag,
            IN_BIN_NUMBER_CHECK_FLAG => rec.bin_number_check_flag,
            IN_ORDER_SOURCE => rec.order_source,
            IN_YELLOW_PAGES_CODE => rec.yellow_pages_code,
            IN_JCPENNEY_FLAG => rec.jcpenney_flag,
            IN_COMPANY_ID => rec.company_id,
            IN_PROMOTION_CODE => rec.promotion_code,
            IN_BONUS_PROMOTION_CODE => rec.bonus_promotion_code,
            IN_UPDATED_BY => 'APE',
            IN_REQUESTED_BY => rec.requested_by,
            IN_PROGRAM_WEBSITE_URL => rec.program_website_url,
            IN_RELATED_SOURCE_CODE => rec.related_source_code,
            IN_COMMENT_TEXT => rec.comment_text,
            IN_PARTNER_BANK_ID => rec.partner_bank_id,
            IN_WEBLOYALTY_FLAG => rec.webloyalty_flag,
            IN_IOTW_FLAG => 'Y',
            IN_INVOICE_PASSWORD => rec.invoice_password,
            IN_BILLING_INFO_FLAG => 'N',
            IN_GIFT_CERTIFICATE_FLAG => 'N',
            IN_MP_REDEMPTION_RATE_ID => rec.mp_redemption_rate_id,
            IN_ADD_ON_FREE_ID => rec.add_on_free_id,
            IN_DISPLAY_SERVICE_FEE_CODE => rec.display_service_fee_code,
            IN_DISPLAY_SHIPPING_FEE_CODE => rec.display_shipping_fee_code,
            IN_PRIMARY_BACKUP_RWD_FLAG => rec.primary_backup_rwd_flag,
            IN_APPLY_SURCHARGE_CODE => rec.apply_surcharge_code,
            IN_SURCHARGE_AMOUNT => rec.surcharge_amount,
            IN_SURCHARGE_DESCRIPTION => rec.surcharge_description,
            IN_DISPLAY_SURCHARGE_FLAG => rec.display_surcharge_flag,
            IN_ALLOW_FREE_SHIPPING_FLAG => rec.allow_free_shipping_flag,
            IN_SAME_DAY_UPCHARGE => rec.same_day_upcharge,
            IN_DISPLAY_SAME_DAY_UPCHARGE => rec.display_same_day_upcharge,
            IN_MORNING_DELIVERY_FLAG => rec.morning_delivery_flag,
            IN_MORNING_DELIVERY_FS_MEMBERS => rec.morning_delivery_free_shipping,
            IN_DELIVERY_FEE_ID => rec.delivery_fee_id,
            IN_AUTO_PROMO_ENGINE_FLAG => 'N',
            IN_APE_PRODUCT_CATALOG => null,
            IN_OSCAR_SELECTION_FLAG => rec.oscar_selection_enabled_flag,
            IN_OSCAR_SCENARIO_GROUP_ID => rec.oscar_scenario_group_id,
            IN_FUNERAL_CEMETERY_LOC_CHK    => rec.funeral_cemetery_loc_chk,
		    IN_HOSPITAL_LOC_CHCK          => rec.hospital_loc_chck,
		    IN_SYMPATHY_LEAD_TIME_CHK      => rec.funeral_cemetery_lead_time_chk,
		    IN_SYMPATHY_LEAD_TIME           => rec.funeral_cemetery_lead_time,
		    IN_BO_HRS_MON_FRI_START        => rec.bo_hrs_mon_fri_start,
		    IN_BO_HRS_MON_FRI_END          => rec.bo_hrs_mon_fri_end,
		    IN_BO_HRS_SAT_START            => rec.bo_hrs_sat_start,
		    IN_BO_HRS_SAT_END             => rec.bo_hrs_sat_end,
		    IN_BO_HRS_SUN_START            => rec.bo_hrs_sun_start,
		    IN_BO_HRS_SUN_END              => rec.bo_hrs_sun_end,
		    IN_LEGACY_ID => null,
		    IN_CALCULATE_TAX_FLAG => rec.calculate_tax_flag,
		    IN_SHIPPING_CARRIER_FALG => rec.custom_shipping_carrier,
		    IN_MERCH_AMT_FULL_REFUND_FLAG  => rec.MERCH_AMT_FULL_REFUND_FLAG,
		    IN_SAME_DAY_UPCHARGE_FS => rec.SAME_DAY_UPCHARGE_FS,
			IN_RECPT_ADDRESS =>  rec.RECPT_ADDRESS,
        	IN_RECPT_CITY =>   rec.RECPT_CITY,
        	IN_RECPT_STATE =>  rec.RECPT_STATE_ID,
        	IN_RECPT_COUNTRY =>  rec.RECPT_COUNTRY_ID,
        	IN_RECPT_PHONE_NUMBER => rec.RECPT_PHONE,
        	IN_RECPT_ZIP_CODE =>  rec.RECPT_ZIP_CODE,
		    OUT_STATUS => out_status,
            OUT_MESSAGE => out_message
        );


        update ftd_apps.source_master_reserve
        set description = rec.description,
            source_type = rec.source_type,
            status = 'Active'
        where source_code = out_source_code;

        FOR rec1 in PARSE_CUR loop
            insert into ftd_apps.product_attr_restr_source_excl (
                source_code,
                product_attr_restr_id,
                created_on,
                created_by,
                updated_on,
                updated_by)
            values (
                out_source_code,
                rec1.product_attr_restr_id,
                sysdate,
                'APE',
                sysdate,
                'APE');
        END LOOP;

    END LOOP;

    --out_status := 'Y';
    --out_message := NULL;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED: [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END CREATE_SOURCE_CODE;

END PROMOTIONS_MAINT_PKG;
.
/
