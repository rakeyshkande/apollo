CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_UPSELL_DETAIL (
  masterId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_UPSELL_MASTER
-- Type:    Procedure
-- Syntax:  SP_REMOVE_UPSELL_MASTER ( productId in varchar2 )
--
-- Description:   Deletes a row from UPSELL_DETAIL and UPSELL_MASTER
--
--==============================================================================
begin

 delete FROM UPSELL_DETAIL
  where UPSELL_MASTER_ID = masterId;

end
;
.
/
