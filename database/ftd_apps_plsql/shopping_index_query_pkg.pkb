create or replace package body ftd_apps.SHOPPING_INDEX_QUERY_PKG as


FUNCTION SI_TEMPLATE_ID_EXISTS
(
IN_SI_TEMPLATE_TYPE_CODE	IN SI_TEMPLATE_TYPE.SI_TEMPLATE_TYPE_DESC%TYPE,
IN_SI_TEMPLATE_KEY		IN SI_TEMPLATE_STAGE.SI_TEMPLATE_KEY%TYPE
) RETURN NUMBER

AS
/*-----------------------------------------------------------------------------
Description:
        This function is to check if the index exists for the source code.
        Throws no_data_found exception if no valid type code match.
Input:
        in_source_code          varchar2
        in_index_name		varchar2

Output:
        si_template_id

-----------------------------------------------------------------------------*/
v_si_template_id SI_TEMPLATE.SI_TEMPLATE_ID%TYPE;
v_si_template_type_code  SI_TEMPLATE_STAGE.SI_TEMPLATE_TYPE_CODE%TYPE;

BEGIN

    select si_template_type_code
    into v_si_template_type_code
    from si_template_type 
    where si_template_type_desc = in_si_template_type_code
    ;
    
    BEGIN 
        select si_template_id
        into v_si_template_id
        from si_template 
        where si_template_type_code = v_si_template_type_code
        and si_template_key = in_si_template_key;
    
    EXCEPTION WHEN NO_DATA_FOUND THEN
        v_si_template_id := -1;
    END;
	
    return v_si_template_id;
	
END SI_TEMPLATE_ID_EXISTS;

--==========================================================================
--
-- Name:    GET_SOURCE_INDEX_FOUND
-- Type:    Function
-- Syntax:  GET_SOURCE_INDEX_FOUND ()
-- Returns: "Y" if the index and source is found
--          "N" if the index and source is NOT found
--
--==========================================================================
FUNCTION GET_SOURCE_INDEX_FOUND
(
IN_SOURCE_CODE      in FTD_APPS.SI_SOURCE_TEMPLATE_REF.SOURCE_CODE%TYPE,
IN_INDEX_NAME       in FTD_APPS.SI_TEMPLATE_INDEX.INDEX_NAME%TYPE 
)
RETURN VARCHAR2
AS

v_rec_found          char (1);

CURSOR  sis_cur IS
        select decode (count (1), 0, 'N', 'Y')
          from ftd_apps.SI_SOURCE_TEMPLATE_REF r, ftd_apps.SI_TEMPLATE_INDEX i
         where r.source_code = IN_SOURCE_CODE
           and r.si_template_id = i.si_template_id
           and i.index_name = IN_INDEX_NAME;

BEGIN

OPEN sis_cur;
FETCH sis_cur INTO v_rec_found;
CLOSE sis_cur;

RETURN v_rec_found;
 
END GET_SOURCE_INDEX_FOUND;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source template data.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_SOURCE_TEMPLATE_REF
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN OUT_CUR FOR
    SELECT sir.source_code, sir.si_template_id
    FROM ftd_apps.si_source_template_ref sir
    JOIN ftd_apps.si_template st
    ON st.si_template_id = sir.si_template_id
    JOIN ftd_apps.si_template_type stt
    ON stt.si_template_type_code = st.si_template_type_code
    ORDER BY sir.source_code, stt.sort_num
    ;

END GET_SOURCE_TEMPLATE_REF;


PROCEDURE GET_DOMAIN_TEMPLATE
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN OUT_CUR FOR
    SELECT st.si_template_key, st.si_template_id
    FROM si_template st
    WHERE st.si_template_type_code = 'DN'
    ;

END GET_DOMAIN_TEMPLATE;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning special filtering index product data.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_FILTER_INDEX
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN OUT_CUR FOR
    SELECT si.si_template_id, si.index_name, sip.product_id
    FROM si_template_index si
    JOIN si_template_index_prod sip
    ON si.si_template_id = sip.si_template_id
    AND si.index_name = sip.index_name
    WHERE si.index_name in ('limit_products','exclude_products','nonsearchitems')
    ORDER BY si.si_template_id,si.index_name
    ;
END GET_FILTER_INDEX;

end SHOPPING_INDEX_QUERY_PKG;
/
