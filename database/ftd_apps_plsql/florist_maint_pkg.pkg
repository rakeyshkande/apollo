CREATE OR REPLACE
PACKAGE ftd_apps.FLORIST_MAINT_PKG AS

/*-----------------------------------------------------------------------------
Description:
        This package contains the procedures required to update florist
        related data.

Procedures:
UPDATE_FLORIST_LOCK
UPDATE_FLORIST_PROFILE
UPDATE_FLORIST_CODIFIED_MISC
UPDATE_FLORIST_ZIPS
DELETE_FLORIST_ZIPS
UPDATE_FLORIST_BLOCKS
UPDATE_FLORIST_SUSPEND
UPDATE_FLORIST_CODIFICATIONS
INITIATE_FTDM_SHUTDOWN
UPDATE_FTDM_SHUTDOWN_RESTART
RESTART_FTDM
UPDATE_FLORIST_N_INSERT_CMMNTS
-----------------------------------------------------------------------------*/

PROCEDURE UPDATE_FLORIST_LOCK
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_LOCK_INDICATOR               IN VARCHAR2,
IN_LOCKED_BY                    IN FLORIST_MASTER.LOCKED_BY%TYPE,
OUT_LOCKED_BY                   OUT FLORIST_MASTER.LOCKED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_FLORIST_PROFILE
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_FLORIST_NAME                 IN FLORIST_MASTER.FLORIST_NAME%TYPE,
IN_ADDRESS                      IN FLORIST_MASTER.ADDRESS%TYPE,
IN_CITY                         IN FLORIST_MASTER.CITY%TYPE,
IN_STATE                        IN FLORIST_MASTER.STATE%TYPE,
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE,
IN_PHONE_NUMBER                 IN FLORIST_MASTER.PHONE_NUMBER%TYPE,
IN_FAX_NUMBER                   IN FLORIST_MASTER.FAX_NUMBER%TYPE,
IN_EMAIL_ADDRESS                IN FLORIST_MASTER.EMAIL_ADDRESS%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
IN_ALT_PHONE_NUMBER             IN FLORIST_MASTER.ALT_PHONE_NUMBER%TYPE,
IN_TERRITORY                    IN FLORIST_MASTER.TERRITORY%TYPE,
IN_OWNERS_NAME                  IN FLORIST_MASTER.OWNERS_NAME%TYPE,
IN_RECORD_TYPE                  IN FLORIST_MASTER.RECORD_TYPE%TYPE,
IN_STATUS                       IN FLORIST_MASTER.STATUS%TYPE,
IN_VENDOR_FLAG                  IN FLORIST_MASTER.VENDOR_FLAG%TYPE,
IN_MINIMUM_ORDER_AMOUNT         IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_VENDOR_WEIGHT
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_FLORIST_WEIGHT               IN FLORIST_MASTER.FLORIST_WEIGHT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_PROFILE
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_FLORIST_NAME                 IN FLORIST_MASTER.FLORIST_NAME%TYPE,
IN_ADDRESS                      IN FLORIST_MASTER.ADDRESS%TYPE,
IN_CITY                         IN FLORIST_MASTER.CITY%TYPE,
IN_STATE                        IN FLORIST_MASTER.STATE%TYPE,
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE,
IN_PHONE_NUMBER                 IN FLORIST_MASTER.PHONE_NUMBER%TYPE,
IN_FAX_NUMBER                   IN FLORIST_MASTER.FAX_NUMBER%TYPE,
IN_EMAIL_ADDRESS                IN FLORIST_MASTER.EMAIL_ADDRESS%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
IN_ALT_PHONE_NUMBER             IN FLORIST_MASTER.ALT_PHONE_NUMBER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_CODIFIED_MISC
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_MERCURY_FLAG                 IN FLORIST_MASTER.MERCURY_FLAG%TYPE,
IN_MINIMUM_ORDER_AMOUNT         IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_ZIPS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_ZIPS.BLOCK_END_DATE%TYPE,
IN_CUTOFF_TIME                  IN FLORIST_ZIPS.CUTOFF_TIME%TYPE,
IN_UPDATED_BY                   IN VARCHAR2,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ASSIGNMENT_TYPE              IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE DELETE_FLORIST_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE DELETE_NON_MEMBER_FLORIST_ZIPS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_WEIGHTS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_SUPER_FLORIST_FLAG           IN FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE,
IN_ADJUSTED_WEIGHT              IN FLORIST_MASTER.ADJUSTED_WEIGHT%TYPE,
IN_ADJUSTED_WEIGHT_START_DATE   IN FLORIST_MASTER.ADJUSTED_WEIGHT_START_DATE%TYPE,
IN_ADJUSTED_WEIGHT_END_DATE     IN FLORIST_MASTER.ADJUSTED_WEIGHT_END_DATE%TYPE,
IN_ADJUSTED_WEIGHT_PERM_FLAG    IN FLORIST_MASTER.ADJUSTED_WEIGHT_PERMENANT_FLAG%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
IN_SUSPEND_OVERRIDE_FLAG        IN FLORIST_MASTER.SUSPEND_OVERRIDE_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_FLORIST_WEIGHT_HISTORY
(
IN_FLORIST_ID                   IN FLORIST_WEIGHT_HISTORY.FLORIST_ID%TYPE,
IN_FLORIST_WEIGHT               IN FLORIST_WEIGHT_HISTORY.FLORIST_WEIGHT%TYPE,
IN_INITIAL_WEIGHT               IN FLORIST_WEIGHT_HISTORY.INITIAL_WEIGHT%TYPE,
IN_ADJUSTED_WEIGHT              IN FLORIST_WEIGHT_HISTORY.ADJUSTED_WEIGHT%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_WEIGHT_HISTORY.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_FLORIST_COMMENTS
(
IN_FLORIST_ID                   IN FLORIST_COMMENTS.FLORIST_ID%TYPE,
IN_COMMENT_TYPE                 IN FLORIST_COMMENTS.COMMENT_TYPE%TYPE,
IN_FLORIST_COMMENT              IN FLORIST_COMMENTS.FLORIST_COMMENT%TYPE,
IN_MANAGER_ONLY_FLAG            IN FLORIST_COMMENTS.MANAGER_ONLY_FLAG%TYPE,
IN_CREATED_BY                   IN FLORIST_COMMENTS.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ENQUEUE_FLAG                 IN VARCHAR2,
IN_BLOCK_REASON                 IN FLORIST_BLOCKS.BLOCK_REASON%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE REMOVE_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ENQUEUE_FLAG                 IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);


PROCEDURE FLORIST_SUSPEND_RESUME
(
IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_TYPE                 IN FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE,
IN_SUSPEND_INDICATOR            IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);


PROCEDURE UPDATE_FLORIST_SUSPEND
(
IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_TYPE                 IN FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE,
IN_SUSPEND_INDICATOR            IN VARCHAR2,
IN_SUSPEND_START_DATE           IN FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE,
IN_SUSPEND_END_DATE             IN FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE,
IN_GOTO_FLORIST_SUS_RECEIVED IN FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_RECEIVED%TYPE,
IN_GOTO_FLORIST_SUS_MERC_ID  IN FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_MERC_ID%TYPE,
IN_GOTO_FLORIST_RES_RECEIVED IN FLORIST_SUSPENDS.GOTO_FLORIST_RESUME_RECEIVED%TYPE,
IN_GOTO_FLORIST_RES_MERC_ID   IN FLORIST_SUSPENDS.GOTO_FLORIST_RESUME_MERC_ID%TYPE,
IN_TIMEZONE_OFFSET_IN_MINUTES      IN FLORIST_SUSPENDS.TIMEZONE_OFFSET_IN_MINUTES%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_MERCURY_SUSPEND
(
IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_START_DATE           IN FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE,
IN_SUSPEND_END_DATE             IN FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE,
IN_TIMEZONE_OFFSET_IN_MINUTES      IN FLORIST_SUSPENDS.TIMEZONE_OFFSET_IN_MINUTES%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_GOTO_SUSPEND
(

IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_INDICATOR            IN VARCHAR2,
IN_GOTO_FLORIST_SUS_MERC_ID  IN FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_MERC_ID%TYPE,
IN_GOTO_FLORIST_RES_MERC_ID   IN FLORIST_SUSPENDS. GOTO_FLORIST_RESUME_MERC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_PRODUCTS
(
IN_FLORIST_ID                   IN FLORIST_PRODUCTS.FLORIST_ID%TYPE,
IN_CODIFICATION_ID              IN FLORIST_CODIFICATIONS.CODIFICATION_ID%TYPE,
IN_BLOCK_INDICATOR              IN FLORIST_PRODUCTS.BLOCK_FLAG%TYPE,
IN_ACTION                       IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_NEW_FLORIST_CODIFIED
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_CODIFICATIONS
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
IN_CODIFICATION_ID              IN FLORIST_CODIFICATIONS.CODIFICATION_ID%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_END_DATE               IN FLORIST_CODIFICATIONS.BLOCK_END_DATE%TYPE,
IN_CSR_ID                       IN VARCHAR2,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INITIATE_FTDM_SHUTDOWN
(
IN_SHUTDOWN_BY                  IN FTDM_SHUTDOWN.SHUTDOWN_BY%TYPE,
IN_RESTART_DATE                 IN FTDM_SHUTDOWN.RESTART_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FTDM_SHUTDOWN_RESTART
(
IN_RESTART_DATE                 IN FTDM_SHUTDOWN.RESTART_DATE%TYPE,
IN_RESTARTED_BY                 IN FTDM_SHUTDOWN.RESTARTED_BY%TYPE,
IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FTDM_SHUTDOWN_RESTART
(
IN_RESTART_DATE                 IN FTDM_SHUTDOWN.RESTART_DATE%TYPE,
IN_RESTARTED_BY                 IN FTDM_SHUTDOWN.RESTARTED_BY%TYPE,
IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE,
IN_ENQUEUE                      IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE RESTART_FTDM
(
IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_N_INSERT_CMMNTS
(
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
);


PROCEDURE MOVE_FLORIST_ZIPS_TO_PARENT;


PROCEDURE UPDATE_ZIP_MASTER_FLORIST_ID	
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE FTDM_SHUTDOWN_FLORIST_BLOCK
(
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE FTDM_SHUTDOWN_FLORIST_UNBLOCK
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE FTDM_SHUTDOWN_UPDATE_BLOCKS
(
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_EROS_AVAIL_HISTORY
(
IN_FLORIST_ID                   IN EROS_AVAILABILITY_HISTORY.FLORIST_ID%TYPE,
IN_GENERATION_DATE              IN EROS_AVAILABILITY_HISTORY.GENERATION_DATE%TYPE,
IN_EXPIRATION_DATE              IN EROS_AVAILABILITY_HISTORY.EXPIRATION_DATE%TYPE,
IN_ONLINE_FLAG                  IN EROS_AVAILABILITY_HISTORY.ONLINE_FLAG%TYPE,
IN_CALL_FORWARDING_FLAG         IN EROS_AVAILABILITY_HISTORY.CALL_FORWARDING_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ENQUEUE_FLAG		        IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_LATLONG
(
 IN_FLORIST_ID         IN FLORIST_MASTER.FLORIST_ID%TYPE,
 IN_LATITUDE		   IN FLORIST_MASTER.LATITUDE%TYPE,
 IN_LONGITUDE          IN FLORIST_MASTER.LONGITUDE%TYPE,
 IN_UPDATED_BY		   IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
);

PROCEDURE INSERT_FLORIST_SUSPEND_HISTORY
(
 IN_FLORIST_ID                  IN FLORIST_MASTER.FLORIST_ID%TYPE,
 IN_SUSPEND_START_DATE          IN FLORIST_SUSPEND_HISTORY.SUSPEND_START_DATE%TYPE,
 IN_SUSPEND_END_DATE            IN FLORIST_SUSPEND_HISTORY.SUSPEND_END_DATE%TYPE,
 OUT_STATUS                     OUT VARCHAR2,
 OUT_MESSAGE                    OUT VARCHAR2
);

PROCEDURE UPDATE_FLORIST_ZIPS_MARS
(
 IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
 IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
 IN_BLOCK_INDICATOR              IN VARCHAR2,
 IN_BLOCK_END_DATE               IN FLORIST_ZIPS.BLOCK_END_DATE%TYPE,
 IN_CUTOFF_TIME                  IN FLORIST_ZIPS.CUTOFF_TIME%TYPE,
 IN_UPDATED_BY                   IN VARCHAR2,
 IN_ADDITIONAL_COMMENT           IN VARCHAR2,
 IN_ASSIGNMENT_TYPE              IN VARCHAR2,
 OUT_STATUS                      OUT VARCHAR2,
 OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_FLORIST_HOURS_OVERRIDE
(
IN_FLORIST_ID                   IN FLORIST_HOURS_OVERRIDE.FLORIST_ID%TYPE,
IN_OVERRIDE_DATE				IN FLORIST_HOURS_OVERRIDE.OVERRIDE_DATE%TYPE,
Out_Status                      Out Varchar2,
Out_Message                     Out Varchar2
);

PROCEDURE DELETE_FLORIST_HOURS_OVERRIDE
(
In_Florist_Id                   In Florist_Hours_Override.Florist_Id%Type,
In_Override_Date				        In Florist_Hours_Override.Override_Date%Type,
OUT_STATUS                      OUT VARCHAR2,
Out_Message                     Out Varchar2
);

PROCEDURE UPDATE_FLORIST_HOURS_OVERRIDE
(
In_Florist_Id                   In Florist_Hours_Override.Florist_Id%Type,
IN_OVERRIDE_DATE				        IN FLORIST_HOURS_OVERRIDE.OVERRIDE_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
Out_Message                     Out Varchar2
);

PROCEDURE UPDATE_FLORIST_CITY_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_CITY_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_START_DATE				IN FLORIST_CITY_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE				IN FLORIST_CITY_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCK_COMMENTS				IN FLORIST_COMMENTS.FLORIST_COMMENT%TYPE,
IN_UPDATED_BY					IN FLORIST_CITY_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE DELETE_FLORIST_CITY_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_CITY_BLOCKS.FLORIST_ID%TYPE,
IN_UPDATED_BY					IN FLORIST_CITY_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_HOLIDAY_PRODUCTS
(
IN_PRODUCT_ID					IN HOLIDAY_PRODUCTS.PRODUCT_ID%TYPE,
IN_DELIVERY_DATE                IN HOLIDAY_PRODUCTS.DELIVERY_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE DELETE_HOLIDAY_PRODUCTS
(
IN_PRODUCT_ID					IN HOLIDAY_PRODUCTS.PRODUCT_ID%TYPE,
IN_DELIVERY_DATE                IN HOLIDAY_PRODUCTS.DELIVERY_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_EXCLUSION_ZONE_HEADER
(
IN_DESCRIPTION			IN VARCHAR2,
IN_BLOCK_START_DATE             IN DATE,
IN_BLOCK_END_DATE		IN DATE,
IN_CREATED_BY			IN VARCHAR2,
OUT_HEADER_ID			OUT PLS_INTEGER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE INSERT_EXCLUSION_ZONE_DETAIL
(
IN_HEADER_ID                    IN VARCHAR2,
IN_BLOCK_SOURCE			IN VARCHAR2,
IN_BLOCK_SOURCE_KEY             IN VARCHAR2,
IN_CREATED_BY			IN VARCHAR2,
OUT_ADDED_FLAG			OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

PROCEDURE REMOVE_EXCLUSION_ZONE
(
IN_HEADER_ID                    IN VARCHAR2,
IN_USER_ID                      IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
);

END FLORIST_MAINT_PKG   ;

/
