-- FTD_APPS.SOURCE_MASTER_RESERVE_$ trigger to populate FTD_APPS.SOURCE_MASTER_RESERVE$ shadow table

CREATE OR REPLACE TRIGGER FTD_APPS.source_master_reserve_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.source_master_reserve REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.source_master_reserve$ (
      	source_code,
      	description,
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.source_code,
      	:NEW.description,
      	:NEW.source_type,
      	:NEW.status,
      	:NEW.created_on,
      	:NEW.created_by,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.source_master_reserve$ (
      	source_code,
      	description,
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.source_code,
      	:OLD.description,
      	:OLD.source_type,
      	:OLD.status,
      	:OLD.created_on,
      	:OLD.created_by,
      	:OLD.updated_by,
      	:OLD.updated_on,
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO ftd_apps.source_master_reserve$ (
      	source_code,
      	description,
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.source_code,
      	:NEW.description,
      	:NEW.source_type,
      	:NEW.status,
      	:NEW.created_on,
      	:NEW.created_by,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.source_master_reserve$ (
      	source_code,
      	description,
      	source_type,
      	status,
      	created_on,
      	created_by,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.source_code,
      	:OLD.description,
      	:OLD.source_type,
      	:OLD.status,
      	:OLD.created_on,
      	:OLD.created_by,
      	:OLD.updated_by,
      	:OLD.updated_on,
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
