CREATE OR REPLACE
PACKAGE ftd_apps.PRODUCT_QUERY_PKG AS

/*-----------------------------------------------------------------------------
Description:
        This package contains the procedures required to retrieve
        product related data.

Procedures:
GET_SHIP_METHOD_BY_ID - return ship_methods.description
GET_SUBCODE_REFERENCE_NUMBER - get product id subcode_reference_number
GET_PRODUCT_NAME_BY_ID - return product name
-----------------------------------------------------------------------------*/


FUNCTION GET_SHIP_METHOD_BY_ID (
	IN_SHIP_ID    IN ship_methods.ship_method_id%TYPE )
RETURN ship_methods.description%TYPE;


FUNCTION GET_SUBCODE_REFERENCE_NUMBER (
	IN_ID    IN product_subcodes.product_id%TYPE )
RETURN product_subcodes.subcode_reference_number%TYPE;

FUNCTION GET_PRODUCT_NAME_BY_ID
	( IN_PRODUCT_ID         IN  PRODUCT_MASTER.PRODUCT_ID%TYPE )
	  RETURN PRODUCT_MASTER.PRODUCT_NAME%TYPE ;

PROCEDURE GET_PRODUCT_FEED_BY_SEARCH
(
 IN_PRODUCT_ID              IN PRODUCT_FEED_EXTERNAL.PRODUCT_ID%TYPE,
 IN_NOVATOR_ID              IN PRODUCT_MASTER.NOVATOR_ID%TYPE,
 IN_SORT_BY                  IN VARCHAR2,
 IN_START_POSITION           IN NUMBER,
 IN_END_POSITION             IN NUMBER,
 OUT_MAX_ROWS               OUT NUMBER,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE UPDATE_INSERT_PRODUCT_FEED_EXT
(
 IN_PRODUCT_ID                 IN PRODUCT_FEED_EXTERNAL.PRODUCT_ID%TYPE,
 IN_BUY_URL                    IN PRODUCT_FEED_EXTERNAL.BUY_URL%TYPE,
 IN_IMAGE_URL                  IN PRODUCT_FEED_EXTERNAL.IMAGE_URL%TYPE,
 IN_IMPORTANT_PRODUCT_FLAG     IN PRODUCT_FEED_EXTERNAL.IMPORTANT_PRODUCT_FLAG%TYPE,
 IN_PROMOTIONAL_TEXT           IN PRODUCT_FEED_EXTERNAL.PROMOTIONAL_TEXT%TYPE,
 IN_SHIPPING_PROMOTIONAL_TEXT  IN PRODUCT_FEED_EXTERNAL.SHIPPING_PROMOTIONAL_TEXT%TYPE,
 IN_UPDATED_BY                 IN PRODUCT_FEED_EXTERNAL.UPDATED_BY%TYPE,
 OUT_STATUS                   OUT VARCHAR2,
 OUT_MESSAGE                  OUT VARCHAR2
);

PROCEDURE GET_SUBCODE_DETAILS
(
 IN_PRODUCT_ID              IN VARCHAR2,
 OUT_CURSOR                OUT TYPES.REF_CURSOR
);

PROCEDURE GET_ADDITIONAL_PRODUCT_INFO
(
 IN_PRODUCT_ID              IN VARCHAR2,
 OUT_CURSOR                OUT TYPES.REF_CURSOR
);

PROCEDURE GET_PROD_FEED_CI_PRODUCTS
(
 IN_PRODUCT_ID               IN VARCHAR2,
 IN_UPSELL_ID					  IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE GET_PROD_FEED_CI_UPSELLS
(
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE GET_DOMESTIC_SF_BY_SOURCE_CODE 
(
 IN_SOURCE_CODE          IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE GET_IOTWS_BY_SC_AND_PROD_ID
(
 IN_SOURCE_CODE          IN VARCHAR2,
 IN_PRODUCT_ID              IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE GET_SKU_ID_FROM_NOVATOR_ID
(
 IN_NOVATOR_ID              IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE GET_DISCOUNTS
(
 IN_SOURCE_CODE          IN VARCHAR2,
 IN_PRODUCT_ID              IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
);

PROCEDURE PROJECT_FRESH_BULK_UPDATE
(
IN_UPDATED_AFTER             IN VARCHAR2,
IN_SOURCE                    IN VARCHAR2,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
);

PROCEDURE PROJECT_FRESH_ADDON_UPDATE
(
IN_ADDON_ID                  IN VARCHAR2,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
);

PROCEDURE PROJECT_FRESH_MISC_TRIGGERS
(
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
);

END PRODUCT_QUERY_PKG;
.
/
