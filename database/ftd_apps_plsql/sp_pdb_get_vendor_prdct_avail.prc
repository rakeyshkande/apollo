create or replace 
PROCEDURE          FTD_APPS.SP_PDB_GET_VENDOR_PRDCT_AVAIL (
  inProduct             IN VARCHAR2,
  outVendorAvailCount   OUT NUMBER
) AS

--=============================================================================
--
-- Name:    SP_PDB_GET_VENDOR_PRDCT_AVAIL
--
-- Description: Counts the number of vendors available on a product.
--            
--
--============================================================================

BEGIN

   SELECT COUNT(*) INTO outVendorAvailCount
    FROM FTD_APPS.VENDOR_PRODUCT 
    WHERE LOWER(PRODUCT_SUBCODE_ID) = LOWER(inProduct)
    AND AVAILABLE = 'Y';

END;
/