CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ORDER_SEQUENCE return types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_ORDER_SEQUENCE2
-- Type:    Function
-- Syntax:  OE_GET_ORDER_SEQUENCE2 ( )
-- Returns: ref_cursor for next sequence val
--
-- Description:   Returns either a previously discarded or new sequence value.
--
--==============================================================================
as
  -- Under Oracle 8.1.7 you MUST use a column name with NOWAIT to avoid bug
  CURSOR discard_cur IS
  SELECT *
  FROM ORDER_SEQUENCE_DISCARD
  WHERE STATUS = 'A'
  AND ORDER_SEQUENCE = (
    SELECT MIN(ORDER_SEQUENCE) FROM ORDER_SEQUENCE_DISCARD WHERE STATUS = 'A' )
  FOR UPDATE OF STATUS NOWAIT;

  discard_row    discard_cur%ROWTYPE;
  seq_cursor     types.ref_cursor;

begin

  -- Attempt to lock a previously discarded sequence value from table
  OPEN discard_cur;
  FETCH discard_cur INTO discard_row;
  IF ( discard_cur%NOTFOUND )
  THEN
    RAISE NO_DATA_FOUND;
  END IF;

  -- Set the discarded rows status to 'R' and commit (releasing lock)
  UPDATE ORDER_SEQUENCE_DISCARD
  SET STATUS = 'R'
  WHERE CURRENT OF discard_cur;
  CLOSE discard_cur;
  COMMIT;

  -- Delete the discarded row
  DELETE FROM ORDER_SEQUENCE_DISCARD
  WHERE ORDER_SEQUENCE = discard_row.ORDER_SEQUENCE;
  COMMIT;

  OPEN seq_cursor FOR
  SELECT discard_row.ORDER_SEQUENCE as "orderSequence"
  FROM DUAL;

  RETURN seq_cursor;

-- If no discard rows are found, or any exception is raised (such as lock wait)
--   then simply grab the next value from the sequence and return it
EXCEPTION WHEN OTHERS THEN

  IF ( discard_cur%ISOPEN )
  THEN
      CLOSE discard_cur;
  END IF;

  ROLLBACK;

  OPEN seq_cursor FOR
  SELECT OE_ORDER_ID.NEXTVAL as "orderSequence"
  FROM DUAL;

  RETURN seq_cursor;
end;
.
/
