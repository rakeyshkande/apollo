CREATE OR REPLACE
PROCEDURE ftd_apps.zip_code_report AS

/* Send out periodic email for zip code availability
   Enable with: execute frp.jobs_pkg.submit('zip_code_report')
   Turn off with: execute frp.jobs_pkg.remove('zip_code_report')
   Check frp.global_parms.value where context = 'zip_code_report' and name = 'INTERVAL' for the calling interval --
   if you change this, make the changes take effect by removing and submitting the job (see above)
   Email list is in sitescope.pagers, project 'ZIP_CODE_REPORT'
*/

   c utl_smtp.connection;
   v_max_flag_sequence NUMBER;
   v_product_id VARCHAR2(10);

/* Produce a random list of 20 zip codes from the florist_zips table, that are also in csz_avail */
   CURSOR florist_zips_c (p_max_flag_sequence IN NUMBER) IS
      SELECT csz_zip_code, SUBSTR(product_flags, 1, p_max_flag_sequence) product_flags
      FROM  (SELECT csz_zip_code, product_flags, dbms_random.random
             FROM  (SELECT DISTINCT ca.csz_zip_code, ca.product_flags
                    FROM   ftd_apps.florist_zips fz, ftd_apps.csz_avail ca
                    WHERE  ca.csz_zip_code = fz.zip_code)
             ORDER BY 3)
      WHERE  ROWNUM <= 20;
   florist_zips_r florist_zips_c%ROWTYPE;

/* Produce a random list of 20 zip codes from the florist_zips table, that are NOT in csz_avail */
   CURSOR csz_zips_c IS
      SELECT zip_code
      FROM  (SELECT zip_code, dbms_random.random
             FROM  (SELECT DISTINCT zip_code
                    FROM   ftd_apps.florist_zips fz, ftd_apps.zip_code zc
                    WHERE  fz.zip_code = zc.zip_code_id
                    MINUS
                    SELECT DISTINCT csz_zip_code
                    FROM   ftd_apps.csz_avail)
             ORDER BY 2)
      WHERE  ROWNUM <= 20;
   csz_zips_r csz_zips_c%ROWTYPE;

   CURSOR pagers_c IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = 'ZIP_CODE_REPORT';
   pagers_r pagers_c%ROWTYPE;

   PROCEDURE send_header(name IN VARCHAR2, header IN VARCHAR2) AS
   BEGIN
      utl_smtp.write_data(c, name || ': ' || header || utl_tcp.CRLF);
   END;

BEGIN

   SELECT MAX(flag_sequence)
   INTO   v_max_flag_sequence
   FROM   ftd_apps.codified_products;

   dbms_random.initialize(TO_NUMBER(TO_CHAR(SYSDATE,'hh24ddmmyy')));
   c := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server','NAME'));
   utl_smtp.helo(c, 'ftdi.com');
   utl_smtp.mail(c, frp.use_this_from_address);
   FOR pagers_r IN pagers_c LOOP
      utl_smtp.rcpt(c, pagers_r.pager_number);
   END LOOP;
   utl_smtp.open_data(c);
   FOR pagers_r IN pagers_c LOOP
      send_header('To', '"'||pagers_r.pager_number||'" '||pagers_r.pager_number);
   END LOOP;
   send_header('Subject', 'Zip Code Report');

   utl_smtp.write_data(c, utl_tcp.CRLF||'Zip Codes and Product Flags'||utl_tcp.CRLF);
   FOR florist_zips_r IN florist_zips_c (v_max_flag_sequence) LOOP
      utl_smtp.write_data(c, utl_tcp.CRLF||florist_zips_r.csz_zip_code);
      FOR x IN 1..v_max_flag_sequence LOOP
         BEGIN
            SELECT product_id
            INTO   v_product_id
            FROM   ftd_apps.codified_products
            WHERE  flag_sequence = x
            AND    rownum <= 1;
            utl_smtp.write_data(c, utl_tcp.CRLF||RPAD(v_product_id,13)||SUBSTR(florist_zips_r.product_flags,x,1));
         EXCEPTION WHEN OTHERS THEN NULL;
         END;
      END LOOP;
      utl_smtp.write_data(c, utl_tcp.CRLF);
   END LOOP;

   utl_smtp.write_data(c, utl_tcp.CRLF||utl_tcp.CRLF||'Florist Zips that are not in CSZ_AVAIL'||utl_tcp.CRLF);
   FOR csz_zips_r IN csz_zips_c LOOP
      utl_smtp.write_data(c, utl_tcp.CRLF||csz_zips_r.zip_code);
   END LOOP;

   utl_smtp.close_data(c);
   utl_smtp.quit(c);
   dbms_random.terminate;

EXCEPTION

   WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

      BEGIN

         utl_smtp.quit(c);

      EXCEPTION

         WHEN utl_smtp.transient_error OR utl_smtp.permanent_error THEN

            NULL; -- When the SMTP server is down or unavailable, we don't have
            -- a connection to the server. The quit call will raise an
            -- exception that we can ignore.

      END;

      raise_application_error(-20000, 'Failed to send mail due to the following error: ' || sqlerrm);

END;
.
/
