CREATE OR REPLACE TRIGGER FTD_APPS.FLORIST_BLOCKS_PAS_FEED_TRG
AFTER INSERT OR DELETE OR UPDATE OF BLOCK_START_DATE,                                    
                                    BLOCK_END_DATE                                    
                           ON FTD_APPS.FLORIST_BLOCKS
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE

   v_fm_record_type     florist_master.record_type%TYPE;
   v_florist_id         florist_master.florist_id%TYPE;

   procedure ins_pas(in_action      IN varchar2,
                     in_florist     IN varchar2,
                     in_start_date  IN date,
                     in_end_date    IN date) is
      v_start_date  date;
      v_priority    integer;
   begin

      -- Don't send dates < current date to PAS
      if in_start_date IS NOT NULL and
         trunc(in_start_date) < trunc(sysdate) then
         v_start_date := sysdate;
      else
         v_start_date := in_start_date;
      end if;
             
      -- Don't even call PAS for old end dates
      if (in_end_date IS NOT NULL and 
            trunc(in_end_date) >= trunc(sysdate)) OR
         (in_end_date IS NULL)                    then   

         v_priority := trunc(v_start_date) - trunc(sysdate);
         PAS.POST_PAS_COMMAND_PRIORITY(in_action,   
                              in_florist                       || ' ' || 
                              to_char(v_start_date,'MMDDYYYY') || ' ' || 
                              to_char(in_end_date,'MMDDYYYY'),
                              v_priority);

         -- dbms_output.put_line('PAYLOAD="' || in_action || ',' ||   
         --                      in_florist                       || ' ' || 
         --                      to_char(v_start_date,'MMDDYYYY') || ' ' || 
         --                      to_char(in_end_date,'MMDDYYYY')  || '"');
      end if;
   end ins_pas;

   procedure determine_dates(in_old_start_date   IN  date,
                             in_new_start_date   IN  date,
                             in_old_end_date     IN  date,
                             in_new_end_date     IN  date,
                             out_start_date      OUT date,
                             out_end_date        OUT date) is
      --
      --This routine works for both Block and Suspend start/end dates (identical logic).
      --
      --Return out_start_date as:
      --   1.  the non-null of the old START_DATE and new START_DATE 
      --         if one or the other is null
      --   2.  the lesser of the old and new START_DATE if they are both non-null
      --   3.  null if they are both null
      --Return out_end_date as: 
      --   1.  the new END_DATE if the old START_DATE and old END_DATE are null
      --   1a. the old END_DATE if the new START_DATE and new END_DATE are null (missing from rqmts doc)
      --   2.  or  null if the old or new END_DATE is null
      --   3.  or the greater of the old and the new END_DATE if they are both non-null

   begin
      if in_old_start_date IS NULL and in_new_start_date IS NULL THEN
         out_start_date := NULL;
      elsif in_old_start_date IS NULL THEN
         out_start_date := in_new_start_date;
      elsif in_new_start_date IS NULL THEN
         out_start_date := in_old_start_date;
      else  -- Old AND New have values
         if in_old_start_date < in_new_start_date THEN
            out_start_date := in_old_start_date;
         else
            out_start_date := in_new_start_date;
         end if;
      end if;
         
      if in_old_start_date IS NULL and in_old_end_date IS NULL THEN
         out_end_date := in_new_end_date;
      elsif in_new_start_date IS NULL and in_new_end_date IS NULL THEN
         out_end_date := in_old_end_date;
      elsif in_old_end_date IS NULL or in_new_end_date IS NULL THEN
         out_end_date := NULL;
      else  -- Old AND New have values
         if in_old_end_date > in_new_end_date THEN
            out_end_date := in_old_end_date;
         else
            out_end_date := in_new_end_date;
         end if;
      end if;
   end determine_dates;


   
BEGIN
   --   dbms_output.put_line('florist_blocks_pas_feed_trg logic');
   --   dbms_output.put_line('old.block_start_date=' || to_char(:old.block_start_date) ||
   --                    '    new.block_start_date=' || to_char(:new.block_start_date));
   --   dbms_output.put_line('old.block_end_date=' || to_char(:old.block_end_date) ||
   --                    '    new.block_end_date=' || to_char(:new.block_end_date));



   BEGIN
      -- Get the florist's record type.  Throw an 'X' in there if NULL to make
      -- subsequent conditions easier.

      IF DELETING THEN
         v_florist_id := :OLD.florist_id;
      ELSE
         v_florist_id := :NEW.florist_id;
      END IF;

      select NVL(record_type,'X')
      into   v_fm_record_type
      from florist_master
      where florist_id = v_florist_id;
      
   EXCEPTION
      when others then
         v_fm_record_type := 'X';
   END;

   IF v_fm_record_type = 'R' THEN 
      IF INSERTING THEN

         IF :NEW.block_start_date IS NOT NULL then
            ins_pas('MODIFIED_FLORIST', :new.florist_id, :new.block_start_date, :new.block_end_date);
         END IF;
      
      ELSIF UPDATING  THEN
         declare
            v_start_date  date;
            v_end_date    date;

         begin
         
            -- If Block start or end changed, need to send a PAS msg
            if ((:OLD.block_start_date IS NULL     and :NEW.block_start_date IS NOT NULL) OR
                (:OLD.block_start_date IS NOT NULL and :NEW.block_start_date IS NULL)     OR
                (:OLD.block_start_date              != :NEW.block_start_date)             OR
                (:OLD.block_end_date   IS NULL     and :NEW.block_end_date IS NOT NULL)   OR
                (:OLD.block_end_date   IS NOT NULL and :NEW.block_end_date IS NULL)       OR
                (:OLD.block_end_date                != :NEW.block_end_date)
               ) THEN

               determine_dates(:OLD.block_start_date,
                               :NEW.block_start_date,
                               :OLD.block_end_date,
                               :NEW.block_end_date,
                               v_start_date,
                               v_end_date);
 
               ins_pas('MODIFIED_FLORIST', :NEW.florist_id, v_start_date, v_end_date);
            end if;

         end;
      
      ELSIF DELETING  THEN
      
         IF :OLD.block_start_date IS NOT NULL or :OLD.block_end_date IS NOT NULL then
            ins_pas('MODIFIED_FLORIST', :old.florist_id, :old.block_start_date, :old.block_end_date);
         END IF;

      END IF;
   END IF;

END;
/


select * from dba_errors where name = 'FLORIST_BLOCKS_PAS_FEED_TRG';
