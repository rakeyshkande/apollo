CREATE OR REPLACE
PROCEDURE ftd_apps.SP_INSERT_CLIENT_URL (
  asnNumberIn in varchar2,
	clientNameIn in varchar2,
    urlIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSERT_CLIENT_URL
-- Type:    Procedure
-- Syntax:  SP_INSERT_CLIENT_URL ( asnNumberIn in varchar2,
--                                    clientNameIn in varchar2,
--                                      urlIn in varchar2)
--
-- Description:   Inserts a row to the CLIENT_URL table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==============================================================================
begin

 insert into CLIENT_URL
 (ASN_NUMBER,CLIENT_NAME,URL)
 VALUES(asnNumberIn,clientNameIn,urlIn);

end
;
.
/
