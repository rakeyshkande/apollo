CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_COMPANY_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_COMPANY_LIST
-- Type:    Function
-- Syntax:  SP_GET_COMPANY_LIST ()
-- Returns: ref_cursor
--
-- Description:   Queries the Buyer Source Code and Client Url for all
--                companies.  An outer join is done on the url
--                table.
--
-- !! NOTE, THIS TABLE EXISTS WITH THE B2B SCHEMA AND IS A SYNONYM ON THIS
--          DATABASE.
--==================================================\

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT bsc.ASN_NUMBER,
			   bsc.CLIENT_NAME,
			   bsc.SOURCE_CODE,
			   cu.URL
        FROM BUYER_SOURCE_CODE bsc, CLIENT_URL cu
        where  bsc.ASN_NUMBER = cu.ASN_NUMBER(+)
        ORDER BY bsc.CLIENT_NAME;

    RETURN cur_cursor;
END
;
.
/
