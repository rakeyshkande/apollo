CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODUCT_LIST_INDEX_AVAIL  (
	  IN_POSTAL              IN VARCHAR2, --assumes upper case and 5 digits for US or 3 char for CA
    IN_PRODUCT_ID          IN VARCHAR2, --assumes upper case
    IN_ZIP_GNADD_FLAG      IN VARCHAR2,
    IN_GNADD_LEVEL         IN NUMBER,
    IN_ZIP_FLORAL_FLAG     IN VARCHAR2,
    IN_STATE_ID            IN VARCHAR2,
    IN_PRODUCT_TYPE        IN VARCHAR2,
    IN_SHIP_METHOD_CARRIER IN VARCHAR2,
    IN_SHIP_METHOD_FLORIST IN VARCHAR2,
    IN_COUNTRY_ID          IN VARCHAR2
     )
    RETURN NUMBER
--==============================================================================
--
-- Name:    OE_PRODUCT_LIST_INDEX_AVAIL
-- Type:    Function
-- Syntax:  OE_PRODUCT_LIST_INDEX_AVAIL(
--                                      IN_POSTAL              VARCHAR2,
--                                      IN_PRODUCT_ID          VARCHAR2,
--                                      IN_ZIP_GNADD_FLAG      VARCHAR2,
--                                      IN_GNADD_LEVEL         NUMBER,
--                                      IN_ZIP_FLORAL_FLAG     VARCHAR2,
--                                      IN_STATE_ID            VARCHAR2,
--                                      IN_PRODUCT_TYPE        VARCHAR2,
--                                      IN_SHIP_METHOD_CARRIER VARCHAR2,
--                                      IN_SHIP_METHOD_FLORIST VARCHAR2,
--                                      IN_COUNTRY_ID          VARCHAR2   )
--
-- Description:   Validates the availability of a product for display
--                in the product list when shopping by index.  This function
--                is only called by FTD_APPS.OE_PRODUCT_LIST_BY_INDEX_XXXX.
--                Returns 1 if the product is available or 0 if it is not
--                available
--==============================================================================
AS
    v_checkOeFlag           CODIFIED_PRODUCTS.CHECK_OE_FLAG%TYPE := NULL;
    v_codifiedSpecial       CODIFIED_PRODUCTS.CODIFIED_SPECIAL%TYPE := NULL;
    v_cszAvailableFlag      CSZ_PRODUCTS.AVAILABLE_FLAG%TYPE := NULL;
    v_twoDayShipAvailable   VARCHAR(1) := NULL;
    v_retval                NUMBER := NULL;

BEGIN
    -- Determine if this product is considered codified for Order Entry
    BEGIN
      SELECT cp.CHECK_OE_FLAG, cp.CODIFIED_SPECIAL
      INTO v_checkOeFlag, v_codifiedSpecial
      FROM CODIFIED_PRODUCTS cp
      WHERE cp.product_id = IN_PRODUCT_ID and (cp.check_oe_flag='Y' OR cp.check_oe_flag='Yes');
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_checkOeFlag := NULL;
      v_codifiedSpecial := 'N';
    END;

    -- Is the codified product available in the zip code
    BEGIN
      SELECT cszp.AVAILABLE_FLAG
      INTO v_cszAvailableFlag
      FROM CSZ_PRODUCTS cszp
      WHERE
        cszp.PRODUCT_ID = IN_PRODUCT_ID AND
        cszp.ZIP_CODE = IN_POSTAL;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        v_cszAvailableFlag := 'N';
    END;

    --Determine if two day ground shipping is available for non fresh-cut products
    BEGIN
      SELECT 'Y' INTO v_twoDayShipAvailable
      FROM FTD_APPS.PRODUCT_SHIP_METHODS psm
      WHERE psm.product_id = IN_PRODUCT_ID
        AND psm.ship_method_id = '2F'
        AND IN_PRODUCT_TYPE NOT IN ('FRECUT');
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_twoDayShipAvailable := 'N';
    END;

    IF
        -- If input zip code is blocked and GNADD level 0 in effect,
        -- or no floral deliveries to this zip code
        -- then list only Specialty Gift , Same Day Gift and Fresh Cuts products
        (( (IN_ZIP_GNADD_FLAG != 'Y' OR IN_GNADD_LEVEL != 0) AND IN_ZIP_FLORAL_FLAG = 'Y')
        OR  IN_PRODUCT_TYPE IN ('SPEGFT','FRECUT','SDG','SDFC') )

        -- GNADD level 3 test
        -- At level 3 floral products show GNADD delivery date
        -- Codified products are not available
        -- Special Codified product show GNADD delivery date
        AND ( IN_GNADD_LEVEL != 3 OR v_checkOeFlag IS NULL
              OR v_codifiedSpecial = 'Y'
            )

        -- If input zip code is blocked and GNADD level 1 in effect
        -- then exclude ALL codified products except codified special and
        -- products that have carrier ship methods also
        AND ( IN_ZIP_GNADD_FLAG != 'Y' OR IN_GNADD_LEVEL != 1
              OR v_checkOeFlag IS NULL
              OR v_codifiedSpecial = 'Y'
              OR IN_SHIP_METHOD_CARRIER = 'Y'
            )

        -- Check product availability for codified products by zip.  Available
        --  flag of 'N' indicates codified product not available for zip.
        -- if the product is codified special then only exclude it if GNADD is off.
        -- If the product is SDG or SDFC then it must be available by a florist
        -- if outside the US
        AND ( v_checkOeFlag IS NULL
              OR IN_POSTAL IS NULL
              OR (IN_PRODUCT_TYPE IN ('SDG','SDFC') AND (IN_COUNTRY_ID IS NULL OR  IN_COUNTRY_ID = 'US'))
              OR (v_codifiedSpecial = 'Y' AND v_cszAvailableFlag != 'N' OR IN_ZIP_GNADD_FLAG = 'Y')
              OR v_cszAvailableFlag != 'N'
            )

        -- If the recipient state is Alaska or Hawaii, exclude dropship products that
        --   do NOT have second day shipping.  DO NOT exclude floral.
        AND ( IN_STATE_ID IS NULL
              OR IN_STATE_ID NOT IN ('AK','HI')
              OR (IN_SHIP_METHOD_FLORIST = 'Y' AND IN_SHIP_METHOD_CARRIER != 'Y')
              -- Check for SDG with florist delivery.  If the SDG product is deliverable by a
              -- florist then we should not exclude it from the list
              OR (IN_PRODUCT_TYPE IN ('SDG','SDFC') AND v_cszAvailableFlag != 'N')
              OR v_twoDayShipAvailable = 'Y'
            )
    THEN
      v_retval := 1;
    ELSE
      v_retval := 0;
    END IF;

    RETURN v_retval;
END;
.
/
