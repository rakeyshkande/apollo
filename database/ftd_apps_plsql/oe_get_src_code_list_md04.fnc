CREATE OR REPLACE
FUNCTION ftd_apps.oe_get_src_code_list_md04 (
    displayExpired IN VARCHAR2,
    dnisId IN VARCHAR2
 )
 RETURN types.ref_cursor
 AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    oe_get_src_code_list_md04
-- Type:    Function
-- Syntax:  oe_get_src_code_list_md04 ( displayExpired IN VARCHAR2,
--                                    dnisId IN VARCHAR2 )
-- Returns: ref_cursor for
--          SOURCE_CODE                 VARCHAR2(10)
--          DESCRIPTION                 VARCHAR2(25)
--
-- Description:   Queries the SOURCE table for all default source code rows.
--                If the input display expired flag is Y then returns all matching rows,
--                otherwise returns all rows that do not yet have an end date set.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
    dfltSrcCode DNIS.DEFAULT_SOURCE_CODE%TYPE;
    scriptCode  DNIS.SCRIPT_CODE%TYPE;
BEGIN

  -- Fetch the default source code for the DNIS if possible
  BEGIN
    SELECT DEFAULT_SOURCE_CODE, SCRIPT_CODE
    INTO dfltSrcCode, scriptCode
    FROM DNIS
    WHERE DNIS_ID = dnisId;
  EXCEPTION WHEN OTHERS THEN
    NULL;
  END;

  -- Display the JC Penney source codes for JCP dnis
  IF ( scriptCode = 'JP' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND JCPENNEY_FLAG = 'Y'
      ORDER BY DESCRIPTION;

  -- Display the SFMB source codes for SFMB dnis
  ELSIF ( scriptCode = 'SF' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'SFMB'
      ORDER BY DESCRIPTION;

  -- Display the HIGH source codes for HIGH (ButterfieldBlooms) dnis
  ELSIF ( scriptCode = 'HI' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'HIGH'
      ORDER BY DESCRIPTION;

  -- Display the GIFT source codes for GIFT dnis
  ELSIF ( scriptCode = 'GS' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'GIFT'
      ORDER BY DESCRIPTION;

  -- Display the GIFT STORE CARD source codes for GSC dnis
  ELSIF ( scriptCode = 'SC' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'GSC'
      ORDER BY DESCRIPTION;

  -- Display the FUSA source codes for FUSA dnis
  ELSIF ( scriptCode = 'FS' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'FUSA'
      ORDER BY DESCRIPTION;

  -- Display the FDIRECT source codes for FDIRECT dnis
  ELSIF ( scriptCode = 'FD' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'FDIRECT'
      ORDER BY DESCRIPTION;

  -- Display the FLORIST.COM source codes for FLORIST.COM dnis
  ELSIF ( scriptCode = 'FL' )
  THEN
    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
      AND COMPANY_ID = 'FLORIST'
      ORDER BY DESCRIPTION;

  -- Otherwise display the top 50 source codes based on default flag
  ELSE

    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
             DESCRIPTION as "description"
      FROM SOURCE
      WHERE ( displayExpired = 'Y' OR END_DATE IS NULL )
          AND NVL(COMPANY_ID, 'FTD') != 'GIFT'
          AND NVL(COMPANY_ID, 'FTD') != 'SFMB'
          AND NVL(COMPANY_ID, 'FTD') != 'HIGH'
          AND NVL(COMPANY_ID, 'FTD') != 'GSC'
          AND NVL(COMPANY_ID, 'FTD') != 'FUSA'
          AND NVL(COMPANY_ID, 'FTD') != 'FDIRECT'
          AND NVL(COMPANY_ID, 'FTD') != 'FLORIST'
      AND ( OE_DEFAULT_FLAG = 'Y'
        OR SOURCE_CODE = dfltSrcCode )
          ORDER BY DESCRIPTION;

  END IF;

  RETURN cur_cursor;
END;
.
/
