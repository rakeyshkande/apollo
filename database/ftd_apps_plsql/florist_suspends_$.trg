CREATE OR REPLACE TRIGGER ftd_apps.florist_suspends_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_suspends
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_suspends$ (
         FLORIST_ID,
         suspend_type,
         suspend_start_date,
         suspend_end_date,
         timezone_offset_in_minutes,
         goto_florist_suspend_received,
         goto_florist_suspend_merc_id,
         goto_florist_resume_received,
         goto_florist_resume_merc_id,
         suspend_processed_flag,
         block_suspend_override_flag,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.suspend_type,
         :new.suspend_start_date,
         :new.suspend_end_date,
         :new.timezone_offset_in_minutes,
         :new.goto_florist_suspend_received,
         :new.goto_florist_suspend_merc_id,
         :new.goto_florist_resume_received,
         :new.goto_florist_resume_merc_id,
         :new.suspend_processed_flag,
         :new.block_suspend_override_flag,
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_suspends$ (
         FLORIST_ID,
         suspend_type,
         suspend_start_date,
         suspend_end_date,
         timezone_offset_in_minutes,
         goto_florist_suspend_received,
         goto_florist_suspend_merc_id,
         goto_florist_resume_received,
         goto_florist_resume_merc_id,
         suspend_processed_flag,
         block_suspend_override_flag,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.suspend_type,
         :old.suspend_start_date,
         :old.suspend_end_date,
         :old.timezone_offset_in_minutes,
         :old.goto_florist_suspend_received,
         :old.goto_florist_suspend_merc_id,
         :old.goto_florist_resume_received,
         :old.goto_florist_resume_merc_id,
         :old.suspend_processed_flag,
         :old.block_suspend_override_flag,
         'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.florist_suspends$ (
         FLORIST_ID,
         suspend_type,
         suspend_start_date,
         suspend_end_date,
         timezone_offset_in_minutes,
         goto_florist_suspend_received,
         goto_florist_suspend_merc_id,
         goto_florist_resume_received,
         goto_florist_resume_merc_id,
         suspend_processed_flag,
         block_suspend_override_flag,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.suspend_type,
         :new.suspend_start_date,
         :new.suspend_end_date,
         :new.timezone_offset_in_minutes,
         :new.goto_florist_suspend_received,
         :new.goto_florist_suspend_merc_id,
         :new.goto_florist_resume_received,
         :new.goto_florist_resume_merc_id,
         :new.suspend_processed_flag,
         :new.block_suspend_override_flag,
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_suspends$ (
         FLORIST_ID,
         suspend_type,
         suspend_start_date,
         suspend_end_date,
         timezone_offset_in_minutes,
         goto_florist_suspend_received,
         goto_florist_suspend_merc_id,
         goto_florist_resume_received,
         goto_florist_resume_merc_id,
         suspend_processed_flag,
         block_suspend_override_flag,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.suspend_type,
         :old.suspend_start_date,
         :old.suspend_end_date,
         :old.timezone_offset_in_minutes,
         :old.goto_florist_suspend_received,
         :old.goto_florist_suspend_merc_id,
         :old.goto_florist_resume_received,
         :old.goto_florist_resume_merc_id,
         :old.suspend_processed_flag,
         :old.block_suspend_override_flag,
         'DEL',SYSDATE);

   END IF;

END;
/
