CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_UPSELL_DETAIL_BY_ID (id IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_RECIPIENT_SRCH
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_RECIPIENT_SRCH (productId in VARCHAR2 )
-- Returns: ref_cursor for
--          RECIPIENT_SEARCH_ID    VARCHAR2(2)
--
-- Description:   Queries the PRODUCT_RECIPIENT_SEARCH table by PRODUCT_ID and
--                returns a ref cursor for resulting row.
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
SELECT U.UPSELL_MASTER_ID,
                U.UPSELL_DETAIL_ID,
                U.UPSELL_DETAIL_SEQUENCE,
                U.UPSELL_DETAIL_NAME,
                M.UPSELL_NAME,
                U.GBB_NAME_OVERRIDE_FLAG,
                U.GBB_NAME_OVERRIDE_TXT,
                U.GBB_PRICE_OVERRIDE_FLAG,
                U.GBB_PRICE_OVERRIDE_TXT,
                U.DEFAULT_SKU_FLAG
FROM UPSELL_DETAIL U, UPSELL_MASTER M
WHERE U.UPSELL_DETAIL_ID = id
  AND U.UPSELL_MASTER_ID = M.UPSELL_MASTER_ID;

    RETURN cur_cursor;
END;
.
/
