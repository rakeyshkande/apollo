CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_RECIPIENT_SEARCH (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_RECIPIENT_SEARCH
-- Type:    Procedure
-- Syntax:  SP_REMOVE_RECIPIENT_SEARCH ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_RECIPIENT_SEARCH by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_RECIPIENT_SEARCH
  where PRODUCT_ID = productId;

end
;
.
/
