CREATE OR REPLACE
PROCEDURE ftd_apps.sp_update_product_company_2247 (productID IN VARCHAR2, companyID VARCHAR2)
 --==============================================================================
--
-- Name:    sp_update_product_company_2247
-- Type:    PROCEDURE
-- Syntax:  sp_update_product_company_2247(productID, companyID )
--
-- Description:   Adds a record to PRODUCT_COMPANY_XREF
--
--===========================================================


AS
BEGIN
	INSERT INTO PRODUCT_COMPANY_XREF (PRODUCT_ID, COMPANY_ID)
	VALUES (productID, companyID);
END;
.
/
