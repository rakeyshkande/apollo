CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SUB_CODE_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SUB_CODE_LIST
-- Type:    Function
-- Syntax:  SP_GET_SUB_CODE_LIST ()
-- Returns: ref_cursor for
--          PRODUCT_SUBCODE_ID  VARCHAR2(10)
--          PRODUCT_ID          VARCHAR2(10)
--          SUBCODE_DESCRIPTION VARCHAR2(100)
--          SUBCODE_PRICE       NUMBER(8,2)
--
--
-- Description:   Queries the PRODUCT_SUBCODES table and
--                returns a ref cursor for resulting row.
--
-- 12/3/02 emueller, added vendorsku and dimWeight fields
--============================================================
AS
    subcode_cursor types.ref_cursor;
BEGIN
    OPEN subcode_cursor FOR
        SELECT PRODUCT_ID          as "productId",
               PRODUCT_SUBCODE_ID  as "productSubcodeId",
               SUBCODE_DESCRIPTION as "subcodeDescription",
               SUBCODE_PRICE       as "subcodePrice",
               HOLIDAY_SKU         as "subcodeHolidaySKU",
               HOLIDAY_PRICE       as "subcodeHolidayPrice",
               VENDOR_SKU  as "vendorSKU",
               DIM_WEIGHT as "dimWeight"
          FROM PRODUCT_SUBCODES;

    RETURN subcode_cursor;
END;
.
/
