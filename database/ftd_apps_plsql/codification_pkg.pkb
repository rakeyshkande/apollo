CREATE OR REPLACE
PACKAGE BODY ftd_apps.CODIFICATION_PKG AS

/*-----------------------------------------------------------------------------
Description:
        This package has all of the stored procedures for calling stored procs.

Procedures:



-----------------------------------------------------------------------------*/







PROCEDURE VIEW_CODIFIED_PRODUCTS
(

        OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the codification information for all codified products.


Input:
        None
Output:
        Ref cursor to all the product codification records.

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                 CP.PRODUCT_ID,
                 PM.NOVATOR_ID,
                 PM.PRODUCT_NAME ,
                 PM.STATUS ,
                 CP.CODIFICATION_ID ,
                 CM.DESCRIPTION ,
                 CP.CHECK_OE_FLAG,
                 CP.FLAG_SEQUENCE
                 FROM CODIFIED_PRODUCTS CP,
                   PRODUCT_MASTER PM,
                   CODIFICATION_MASTER CM
                WHERE CP.PRODUCT_ID = PM.PRODUCT_ID
         AND  CP.CODIFICATION_ID = CM.CODIFICATION_ID
         ORDER BY CP.PRODUCT_ID;






END VIEW_CODIFIED_PRODUCTS;


PROCEDURE VIEW_CODIFIED_MASTER
(

        OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the codification information for all codification master records


Input:
        None
Output:
        Ref cursor to all the product codification records.

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT
CM.CODIFICATION_ID,
CM.DESCRIPTION,
CM.CATEGORY_ID,
CM.CODIFY_ALL_FLORIST_FLAG ,
CM.LOAD_FROM_FILE,
CM.CODIFICATION_REQUIRED
  FROM CODIFICATION_MASTER CM
ORDER BY CM.CODIFICATION_ID;



END VIEW_CODIFIED_MASTER;


PROCEDURE VIEW_CODIFIED_INFORMATION
        (

                CODE_MASTER_CUR                         OUT TYPES.REF_CURSOR,
                CODE_PROD_CUR                           OUT TYPES.REF_CURSOR
        )
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns ref cursors for both types of codification master and product_codifications


Input:
        None - Returns all data from these.
Output:
        Ref cursors to codified products
        Ref Cursor to codified

-----------------------------------------------------------------------------*/



BEGIN

view_codified_master(CODE_MASTER_CUR);
view_codified_products(CODE_PROD_CUR);

END VIEW_CODIFIED_INFORMATION;





 PROCEDURE UPDATE_CODIFIED_PRODUCTS
        (

                IN_PRODUCT_ID                   IN CODIFIED_PRODUCTS.PRODUCT_ID%TYPE,
                IN_CHECK_OE_FLAG                IN CODIFIED_PRODUCTS.CHECK_OE_FLAG%TYPE,
                IN_FLAG_SEQUENCE                IN CODIFIED_PRODUCTS.FLAG_SEQUENCE%TYPE,
                IN_CODIFICATION_ID              IN CODIFIED_PRODUCTS.CODIFICATION_ID%TYPE,
                OUT_STATUS                      OUT VARCHAR2,
                OUT_MESSAGE                     OUT VARCHAR2


        )
        AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating all codification records


Input:
        Codification data.
Output:
        Status = Yes if succesfull N if not sucessfule
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/




CURSOR exists_cur IS
        SELECT  count (1)
        FROM    CODIFIED_PRODUCTS
        WHERE   product_id = in_product_id;


v_check                 number;
v_action                varchar2(10);

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_check;
CLOSE exists_cur;


IF v_check > 0 THEN

        UPDATE  codified_products
        SET

                check_oe_flag = in_check_oe_flag,
                flag_sequence = in_flag_sequence,
                codification_id = in_codification_id
         WHERE   product_id = in_product_id;

         v_action := 'Update';


ELSE


        INSERT INTO codified_products
        (
                product_id,
                check_oe_flag,
                flag_sequence,
                codification_id,
                codified_special,
                holiday_codified_flag
        )
        VALUES
        (
                in_product_id,
                in_check_oe_flag,
                in_flag_sequence,
                in_codification_id,
                'N',
                'N'

                         );

         v_action := 'Insert';

END IF;

OUT_STATUS := 'Y';

-- Insert a command to update PAS for the product, NO to not cascade updates
PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT',in_product_id || ' NO');  


EXCEPTION WHEN OTHERS THEN
BEGIN
        IF exists_cur%isopen THEN
                CLOSE exists_cur;
        END IF;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END UPDATE_CODIFIED_PRODUCTS;



PROCEDURE UPDATE_CODIFICATION_MASTER
        (

                IN_CODIFICATION_ID              IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
                IN_CATEGORY_ID                  IN CODIFICATION_MASTER.CATEGORY_ID%TYPE,
                IN_DESCRIPTION                  IN CODIFICATION_MASTER.DESCRIPTION%TYPE,
                IN_LOAD_FROM_FILE               IN CODIFICATION_MASTER.LOAD_FROM_FILE%TYPE,
                IN_CODIFY_ALL_FLORIST_FLAG      IN CODIFICATION_MASTER.CODIFY_ALL_FLORIST_FLAG%TYPE,
                IN_CODIFICATION_REQUIRED        IN CODIFICATION_MASTER.CODIFICATION_REQUIRED%TYPE,
                OUT_STATUS                      OUT VARCHAR2,
                OUT_MESSAGE                     OUT VARCHAR2


        )


AS
/*-----------------------------------------------------------------------------
Description:
        Updates the codification master table.


Input:
        None
Output:
        Status = Yes if succesfull N if not sucessfule
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/




CURSOR exists_cur IS
        SELECT  count (1)
        FROM    CODIFICATION_MASTER
        WHERE   CODIFICATION_ID = IN_CODIFICATION_ID;


v_check                 number;
v_action                varchar2(10);

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_check;
CLOSE exists_cur;


IF v_check > 0 THEN

        UPDATE  codification_master
        SET


         CATEGORY_ID=IN_CATEGORY_ID     ,
         DESCRIPTION=IN_DESCRIPTION,
         LOAD_FROM_FILE=IN_LOAD_FROM_FILE,
         CODIFY_ALL_FLORIST_FLAG = IN_CODIFY_ALL_FLORIST_FLAG,
         CODIFICATION_REQUIRED = IN_CODIFICATION_REQUIRED
       WHERE
         CODIFICATION_ID=IN_CODIFICATION_ID;
         v_action := 'Update';


ELSE


        INSERT INTO codification_master
        (
                CODIFICATION_ID,
                CATEGORY_ID ,
                DESCRIPTION,
                LOAD_FROM_FILE,
                CODIFY_ALL_FLORIST_FLAG,
                CODIFICATION_REQUIRED
        )
        VALUES
        (
             IN_CODIFICATION_ID,
                IN_CATEGORY_ID,
                IN_DESCRIPTION,
                IN_LOAD_FROM_FILE,
                IN_CODIFY_ALL_FLORIST_FLAG, 
                IN_CODIFICATION_REQUIRED

                         );
        v_action := 'Insert';

END IF;

OUT_STATUS := 'Y';


EXCEPTION WHEN OTHERS THEN
BEGIN
        IF exists_cur%isopen THEN
                CLOSE exists_cur;
        END IF;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_CODIFICATION_MASTER;



PROCEDURE CODIFY_ALL_FLORISTS
        (

                IN_CODIFICATION_ID                   IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
                IN_USER                              IN FLORIST_COMMENTS.CREATED_BY%TYPE,
                OUT_STATUS                      OUT VARCHAR2,
                OUT_MESSAGE                     OUT VARCHAR2


        )
                AS
/*-----------------------------------------------------------------------------
Description:
        Adds the codification for all the florists.


Input:
        Codification to apply to all florists
        User - The user who made the change.
Output:
        Status = Yes if succesfull N if not sucessful
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/

type varchar_tab_type is table of varchar2(9) index by pls_integer;
v_florist_id_tab        varchar_tab_type;

-- get all active and inactive florists exluding S and E type florists
-- that do not currently have the given codification
cursor ins_cur is
        select  m.florist_id
        from    florist_master m
        where   m.record_type = 'R'
        and     not exists
        (
                select  1
                from    florist_codifications c
                where   c.florist_id = m.florist_id
                and     c.codification_id = in_codification_id
        );

BEGIN

-- CR 1/2005
-- rewrote logic to insert missing codifications instead of replacing all florist codifications
-- for the specific codification id.  this will reduce the number of hp_actions inserts.

-- initialize the status parameter
out_status := 'Y';

open ins_cur;
fetch ins_cur bulk collect into v_florist_id_tab;
close ins_cur;

for x in 1..v_florist_id_tab.count loop

        -- insert missing florist codifications
        insert into florist_codifications
        (
                florist_id,
                codification_id,
                block_start_date,
                block_end_date,
                holiday_price_flag,
                min_price,
                codified_date
        )
        values
        (
                v_florist_id_tab (x),
                in_codification_id,
                null,
                null,
                null,
                null,
                sysdate
        );

        -- insert a comment in for each florist we added it to
        insert into ftd_apps.florist_comments
        (
                florist_id,
                comment_date,
                comment_type,
                florist_comment,
                manager_only_flag,
                created_date,
                created_by
        )
        values
        (
                v_florist_id_tab (x),
                sysdate,
                'Codification',
                'Codification: '|| in_codification_id || ' added.  This was done for all florists.',
                'N',
                sysdate,
                in_user
        );

        -- update florist_products with the changes
        florist_maint_pkg.update_florist_products (v_florist_id_tab (x), in_codification_id, 'N', 'Insert', out_status, out_message);

        if out_status <> 'Y' then
                exit;
        end if;
end loop;

EXCEPTION WHEN OTHERS THEN
BEGIN


        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END CODIFY_ALL_FLORISTS;

PROCEDURE UNCODIFY_ALL_FLORISTS
        (

                IN_CODIFICATION_ID                   IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
                IN_USER                              IN FLORIST_COMMENTS.CREATED_BY%TYPE,
                OUT_STATUS                      OUT VARCHAR2,
                OUT_MESSAGE                     OUT VARCHAR2


        )
                AS
/*-----------------------------------------------------------------------------
Description:
        Adds the codification for all the florists.


Input:
        Codification to apply to all florists
        User - The user who made the change.
Output:
        Status = Yes if succesfull N if not sucessful
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/

type varchar_tab_type is table of varchar2(9) index by pls_integer;
v_florist_id_tab        varchar_tab_type;

-- get all florists exluding S and E type florists with the given codification
cursor del_cur is
        select  c.florist_id
        from    florist_codifications c
        where   c.codification_id = in_codification_id;

BEGIN

-- CR 1/2005
-- rewrote logic to delete florist codifications.

open del_cur;
fetch del_cur bulk collect into v_florist_id_tab;
close del_cur;

for x in 1..v_florist_id_tab.count loop

        -- delete all florist codifications with the given codification
        delete  from florist_codifications
        where   codification_id = in_codification_id
        and     florist_id = v_florist_id_tab (x);

        -- insert a comment in for each florist effected
        insert into florist_comments
        (
                florist_id,
                comment_date,
                comment_type,
                florist_comment,
                manager_only_flag,
                created_date,
                created_by
        )
        values
        (
                v_florist_id_tab (x),
                SYSDATE,
                'Codification',
                'Codification: '|| in_codification_id || ' removed.  This was done for all florists.',
                'N',
                SYSDATE,
                in_user
        );

        -- update florist_products with the changes
        florist_maint_pkg.update_florist_products (v_florist_id_tab (x), in_codification_id, 'N', 'Delete', out_status, out_message);

        if out_status <> 'Y' then
                exit;
        end if;
end loop;

EXCEPTION WHEN OTHERS THEN
BEGIN


        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UNCODIFY_ALL_FLORISTS;




PROCEDURE DELETE_CODIFIED_PRODUCT
(

        IN_PRODUCT_ID                   IN CODIFIED_PRODUCTS.PRODUCT_ID%TYPE,
        OUT_STATUS                      OUT VARCHAR2,
        OUT_MESSAGE                     OUT VARCHAR2

)
AS

/*-----------------------------------------------------------------------------
Description:
        Deletes the codified product passed in.


Input:
        Product Id to delete.
Output:
        Status = Yes if succesfull N if not sucessful
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/

BEGIN

delete from
ftd_apps.codified_products
where product_id = in_product_id;

/*  Still need to determine if Novator needs to be updated with this info immediately or if we will do this later. */

-- Insert a command to update PAS
PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT',in_product_id || ' NO');  


END DELETE_CODIFIED_PRODUCT;


PROCEDURE DELETE_CODIFICATION_MASTER
(


                        IN_CODIFICATION_ID                   IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
                        IN_USER                              IN FLORIST_COMMENTS.CREATED_BY%TYPE,
                        OUT_STATUS                      OUT VARCHAR2,
                        OUT_MESSAGE                     OUT VARCHAR2

)
AS

/*-----------------------------------------------------------------------------
Description:
        Deletes the codificaiton master passed in.  Will not delete ifthere are existing products already for this codification.


Input:
        Codification Master to delete.
        User id of persone deleting. Puts this in the Florist comments.
Output:
        Status = Yes if succesfull N if not sucessful
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/


CURSOR exists_cur IS
        SELECT  count (1)
        FROM    CODIFIED_PRODUCTS
        WHERE   codification_id = in_codification_id;


v_check                 number;

BEGIN


/* Insure all codified products for this id are not there. */

OPEN exists_cur;
FETCH exists_cur INTO v_check;
CLOSE exists_cur;



IF v_check > 0 THEN

    OUT_STATUS := 'N';
    OUT_MESSAGE := 'Unable to delete due to existing products attached to: ' || in_codification_id;

ELSE

/* Remove all florist codifications for this particular codified id */
delete from
ftd_apps.florist_codifications
where codification_id = in_codification_id;


/* Remove codified master itself */
delete
from ftd_apps.codification_master
where codification_id = in_codification_id;

/* Make comments on florist records with user id*/
insert into ftd_apps.florist_comments
(  florist_id,
   comment_date,
  comment_type,
  florist_comment,
  manager_only_flag,
  created_date,
  created_by
)
select florist_id,
         SYSDATE,
          'Codification',
          'Codification: '|| in_codification_id || ' was removed.  This was done because this codification was deleted.',
           'N',
           SYSDATE,
           in_user
FROM
     ftd_apps.FLORIST_MASTER
WHERE RECORD_TYPE = 'R';

OUT_STATUS := 'Y';

END IF;


EXCEPTION WHEN OTHERS THEN
BEGIN


        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block





END DELETE_CODIFICATION_MASTER;

PROCEDURE GET_PRODUCT_BY_NOVATOR_ID
(

                IN_NOVATOR_ID                   IN PRODUCT_MASTER.NOVATOR_ID%TYPE,
                OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the product descriptions but id


Input:
        None
Output:
        Ref cursor to all the product codification records.

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                 PM.PRODUCT_ID,
                 PM.PRODUCT_NAME,
                 PM.STATUS
           FROM   PRODUCT_MASTER PM
           WHERE  PM.novator_id = in_novator_id;


END GET_PRODUCT_BY_NOVATOR_ID;

PROCEDURE GET_PRODUCT_BY_PRODUCT_ID
(

                IN_PRODUCT_ID                   IN PRODUCT_MASTER.PRODUCT_ID%TYPE,
                OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the product ID, Name and Status
        by Product ID


Input:
        Product ID
Output:
        Ref cursor to all the product codification records.

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                 PM.PRODUCT_ID,
                 PM.PRODUCT_NAME,
                 PM.STATUS
           FROM   PRODUCT_MASTER PM
           WHERE  PM.product_id = in_product_id;


END GET_PRODUCT_BY_PRODUCT_ID;


PROCEDURE CHANGE_CODIFICATION_ID
        (

                IN_PREVIOUS_CODIFICATION_ID         IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
                IN_CODIFICATION_ID                  IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
                IN_CATEGORY_ID                      IN CODIFICATION_MASTER.CATEGORY_ID%TYPE,
                IN_DESCRIPTION                      IN CODIFICATION_MASTER.DESCRIPTION%TYPE,
                IN_LOAD_FROM_FILE                   IN CODIFICATION_MASTER.LOAD_FROM_FILE%TYPE,
                IN_CODIFY_ALL_FLORIST_FLAG          IN CODIFICATION_MASTER.CODIFY_ALL_FLORIST_FLAG%TYPE,
                IN_CODIFICATION_REQUIRED            IN CODIFICATION_MASTER.CODIFICATION_REQUIRED%TYPE,
                OUT_STATUS                          OUT VARCHAR2,
                OUT_MESSAGE                         OUT VARCHAR2


        )
           AS

/*-----------------------------------------------------------------------------
Description:
        Creates a new Codification_Master row,
        changes the Codification_ID in the Related FLORIST_CODIFICATIONS and CODIFIED_PRODUCTS
        tables to the new Codification_ID
        and then deletes old Codification_Master row

Input:
        Previous_Codification_ID - Existing Codification ID
        Codification_ID - New Codification ID
        Category - Category ID
        Description - Description of Codification
        Load_from_file - Load from file name
Output:
        Status = Yes if succesfull N if not sucessful
        Message = null if no error,  error text if error.

-----------------------------------------------------------------------------*/

CURSOR update_cur(v_codification_id varchar2) IS
    select codification_id, product_id
    from codified_products
    where codification_id = v_codification_id;
    
BEGIN


/* Insert into codification_master table new Codification Code */

        INSERT INTO CODIFICATION_MASTER
        (
                CODIFICATION_ID,
                CATEGORY_ID ,
                DESCRIPTION,
                LOAD_FROM_FILE,
                CODIFY_ALL_FLORIST_FLAG,
                CODIFICATION_REQUIRED
        )
        VALUES
        (
                IN_CODIFICATION_ID,
                IN_CATEGORY_ID,
                IN_DESCRIPTION,
                IN_LOAD_FROM_FILE,
                IN_CODIFY_ALL_FLORIST_FLAG,
                NVL(IN_CODIFICATION_REQUIRED, 'N')
        );

/* Update florist_codifications table with new Codification Code where old Codification Code used */

        UPDATE FLORIST_CODIFICATIONS
               SET
                CODIFICATION_ID = IN_CODIFICATION_ID
               WHERE
                CODIFICATION_ID = IN_PREVIOUS_CODIFICATION_ID;

/* Update codified_products table with new Codification Code where old Codification Code used */

        FOR rec in update_cur(in_previous_codification_id) loop
            -- Insert a command to update PAS
            PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT', rec.product_id || ' N');
        END LOOP;
        
        UPDATE CODIFIED_PRODUCTS
               SET
                CODIFICATION_ID = IN_CODIFICATION_ID
               WHERE
                CODIFICATION_ID = IN_PREVIOUS_CODIFICATION_ID;


/* Delete old codification id from Codification_Master */

        DELETE FROM CODIFICATION_MASTER
               WHERE
                CODIFICATION_ID = IN_PREVIOUS_CODIFICATION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN


        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END CHANGE_CODIFICATION_ID;

PROCEDURE GET_NEXT_AVAILABLE_SEQUENCE
(

                OUT_SEQUENCE                         OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will return the next available sequence number from the CODIFIED_PRODUCTS
        table.
        It will attempt to use skipped numbers first


Input:
        None
Output:
        Sequence Number

-----------------------------------------------------------------------------*/

/*  This cursor will get the minimum sequence number */
CURSOR min_seq_cur IS
        select min(flag_sequence) from CODIFIED_PRODUCTS;

/*  This cursor will return the minimum sequence number skipped is one exist */
CURSOR missing_seq_cur IS
        select min(a.flag_sequence + 1) from CODIFIED_PRODUCTS a, CODIFIED_PRODUCTS b
        where a.flag_sequence + 1 = b.flag_sequence (+)
        AND b.flag_sequence is null
        and a.flag_sequence <> (SELECT max(z.flag_sequence) from CODIFIED_PRODUCTS z);

/*  This cursor will return the max sequence number */
CURSOR max_seq_cur IS
        select max(flag_sequence) from CODIFIED_PRODUCTS;


BEGIN


/* Check if 1 is being used as a sequence number */

OPEN min_seq_cur;
FETCH min_seq_cur INTO OUT_SEQUENCE;
CLOSE min_seq_cur;

IF OUT_SEQUENCE > 1 THEN

        OUT_SEQUENCE := 1;

ELSE
        /* Check if any sequence numbers have been skipped */

        OPEN missing_seq_cur;
        FETCH missing_seq_cur INTO OUT_SEQUENCE;
        CLOSE missing_seq_cur;

        IF OUT_SEQUENCE IS NULL THEN

                /* If no skipping of sequence number send back 1 greater than the max */
                OPEN max_seq_cur;
                FETCH max_seq_cur INTO OUT_SEQUENCE;
                CLOSE max_seq_cur;

                OUT_SEQUENCE := OUT_SEQUENCE + 1;
        END IF;


END IF;




END GET_NEXT_AVAILABLE_SEQUENCE;

END CODIFICATION_PKG;
.
/
