CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_EXCLUDE_STATE (
 productId in varchar2,
 excludeState in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_EXCLUDE_STATE
-- Type:    Procedure
-- Syntax:  SP_UPDATE_EXCLUDE_STATE ( productId in varchar2,
--                                    excludeState in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_EXCLUDED_STATES.
--                If the product/state combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_EXCLUDED_STATES
  INSERT INTO PRODUCT_EXCLUDED_STATES
      ( PRODUCT_ID,
				EXCLUDED_STATE
		  )
  VALUES
		  ( productId,
				excludeState
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		  rollback;
end
;
.
/
