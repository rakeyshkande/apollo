CREATE OR REPLACE TRIGGER ftd_apps.inv_trk_inv_ctrl_email_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.inv_trk_inv_ctrl_email 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
   
      insert into ftd_apps.inv_trk_inv_ctrl_email$ 
           (inv_trk_id,               
            inv_trk_email_id,               
            created_by,               
            created_on,               
            updated_by,               
            updated_on,               
            OPERATION$, TIMESTAMP$)               
       VALUES (
            :NEW.inv_trk_id,               
            :NEW.inv_trk_email_id,               
            :NEW.created_by,               
            :NEW.created_on,               
            :NEW.updated_by,               
            :NEW.updated_on,               
            'INS', v_current_timestamp);               

   ELSIF UPDATING  THEN
      insert into ftd_apps.inv_trk_inv_ctrl_email$ 
           (inv_trk_id,               
            inv_trk_email_id,               
            created_by,               
            created_on,               
            updated_by,               
            updated_on,               
            OPERATION$, TIMESTAMP$)               
       VALUES (
            :OLD.inv_trk_id,               
            :OLD.inv_trk_email_id,               
            :OLD.created_by,               
            :OLD.created_on,               
            :OLD.updated_by,               
            :OLD.updated_on,               
            'UPD_OLD', v_current_timestamp);               


      insert into ftd_apps.inv_trk_inv_ctrl_email$ 
           (inv_trk_id,               
            inv_trk_email_id,               
            created_by,               
            created_on,               
            updated_by,               
            updated_on,               
            OPERATION$, TIMESTAMP$)               
       VALUES (
            :NEW.inv_trk_id,               
            :NEW.inv_trk_email_id,               
            :NEW.created_by,               
            :NEW.created_on,               
            :NEW.updated_by,               
            :NEW.updated_on,               
            'UPD_NEW', v_current_timestamp);               



   ELSIF DELETING  THEN
      insert into ftd_apps.inv_trk_inv_ctrl_email$ 
           (inv_trk_id,               
            inv_trk_email_id,               
            created_by,               
            created_on,               
            updated_by,               
            updated_on,               
            OPERATION$, TIMESTAMP$)               
       VALUES (
            :OLD.inv_trk_id,               
            :OLD.inv_trk_email_id,               
            :OLD.created_by,               
            :OLD.created_on,               
            :OLD.updated_by,               
            :OLD.updated_on,               
            'DEL', v_current_timestamp);               
   END IF;

END;
/
