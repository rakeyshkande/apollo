CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ADDON_BY_OCCASION (occasionIn IN NUMBER)
RETURN types.ref_cursor

--==============================================================================
--
-- Name:    OE_GET_ADDON_BY_OCCASION
-- Type:    Function
-- Syntax:  OE_GET_ADDON_BY_OCCASION (occasionIn IN NUMBER)
-- Returns: ref_cursor for
--          ADDON_ID       NUMBER
--          ADDON_TYPE     VARCHAR2(2)
--          DESCRIPTION    VARCHAR2(50)
--          PRICE          NUMBER
--
-- Description:   Queries the ADDON and OCCASION_ADDON Tables by OccasionId
--                and returns a ref cursor to the resulting rows.
--
--=============================================================
AS
    addon_cursor types.ref_cursor;
BEGIN
    OPEN addon_cursor FOR
         SELECT A.ADDON_ID as "addonId",
                A.ADDON_TYPE as "addonType",
                A.DESCRIPTION as "description",
                A.PRICE as "price"
          FROM ADDON A, OCCASION_ADDON OA
          WHERE A.ADDON_ID = OA.ADDON_ID
            AND OA.OCCASION_ID = occasionIn
          ORDER BY A.ADDON_TYPE, A.ADDON_ID;


    RETURN addon_cursor;
END
;
.
/
