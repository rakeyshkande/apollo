CREATE OR REPLACE FUNCTION FTD_APPS.PAYMENT_METHOD_IS_CC (
      p_payment_method_id payment_methods.payment_method_id%TYPE) RETURN VARCHAR2 IS

/* Accept a string as input; return a 'Y' if it is a payment_method_id for a credit card, 'N' otherwise. */
/* The main criteria for this is the payment_type field in the payment_methods table.  In general, for a 
   payment_method_id, if the payment_type is 'C', it's a credit card.  However, payment_method_id NC (nocharge)
   has payment_type 'C', but is not a credit card, so we handle this. */
/* This method is mostly applying to logic of whether the data is in the credit card table. Add GD */  
   
   v_cc payment_methods.payment_type%TYPE;
   v_count NUMBER;
   v_return VARCHAR2(1);

BEGIN

   IF p_payment_method_id IS NULL THEN
      v_return := 'N';

   ELSE
      SELECT COUNT(*) INTO v_count FROM payment_methods WHERE payment_method_id = p_payment_method_id;
      IF v_count = 0 THEN
         v_return := 'N';

      ELSE

         SELECT DECODE(p_payment_method_id, 'NC', 'X', 'GD', 'C', payment_type)
         INTO   v_cc
         FROM   payment_methods
         WHERE  payment_method_id = p_payment_method_id;

         IF v_cc = 'C' THEN
            v_return := 'Y';
         ELSE
            v_return := 'N';
         END IF;

      END IF;

   END IF;

   RETURN v_return;

END;
.
/
