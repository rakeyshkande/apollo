create or replace PACKAGE BODY               FTD_APPS.FEES_FEEDS_PKG AS

PROCEDURE GET_FEES_FEEDS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2,
OUT_CURSOR                          OUT TYPES.REF_CURSOR
)
AS

v_batch_count NUMBER := 30;

cursor in_progress is
    SELECT FEED_ID
       FROM FTD_APPS.FEES_FEEDS
       WHERE status = 'NEW'
	   order by created_on, feed_id
       offset 0 rows fetch next (select value from frp.global_parms where context = 'FEES_FEEDS_CONFIG' and name = 'BATCH_COUNT') rows only;
BEGIN
 
  OPEN OUT_CURSOR FOR
     SELECT FEED_ID,
            FEED_XML,
            FEED_TYPE,
            STATUS,
            CREATED_ON,
            UPDATED_ON
       FROM FTD_APPS.FEES_FEEDS
       WHERE status = 'NEW'
	   order by created_on, feed_id
       offset 0 rows fetch next (select value from frp.global_parms where context = 'FEES_FEEDS_CONFIG' and name = 'BATCH_COUNT') rows only;
    
   FOR rc_feed in in_progress LOOP
    dbms_output.put_line  ('Feed id: ' || rc_feed.feed_id);
    update FTD_APPS.FEES_FEEDS
      SET STATUS = 'IN_PROGRESS', UPDATED_ON = sysdate
      WHERE FEED_ID = rc_feed.feed_id;
    commit;
   END LOOP;
OUT_STATUS := 'Y';

EXCEPTION

  WHEN others THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_FEES_FEEDS;

PROCEDURE UPDATE_FEED_STATUS
(
IN_FEED_ID						IN ftd_apps.FEES_FEEDS.FEED_ID%TYPE,
IN_STATUS   			       IN  ftd_apps.FEES_FEEDS.STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
BEGIN

  UPDATE FTD_APPS.FEES_FEEDS set STATUS = IN_STATUS, updated_on = sysdate where FEED_ID = IN_FEED_ID; commit;

OUT_STATUS := 'Y';

EXCEPTION

  WHEN others THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_FEED_STATUS;

PROCEDURE INSERT_MDELIVERY_FEE_FEED
(
IN_FEED_XML              IN  ftd_apps.FEES_FEEDS.FEED_XML%TYPE,
IN_FEED_TYPE             IN  ftd_apps.FEES_FEEDS.FEED_TYPE%TYPE,
IN_STATUS   			       IN  ftd_apps.FEES_FEEDS.STATUS%TYPE,

OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                    OUT VARCHAR2
)
AS
/************************************************************************
This procecure is responsible for inserting the the fuel surcharge feed.

INPUT:
	in_feed_xml              in  ftd_apps.FEES_FEEDS.feed_xml%type
	in_feed_type             in  ftd_apps.FEES_FEEDS.feed_type%type
	in_status   			       in  ftd_apps.FEES_FEEDS.status%type

OUTPUT:
	out_status                     out varchar2
	out_message                    out varchar2

*************************************************************************/

v_last_end_date		delivery_fee.created_on%TYPE;
v_start_date		delivery_fee.created_on%TYPE;


BEGIN
	out_status := 'N';

	v_last_end_date := sysdate;

	-- next second
	v_start_date := v_last_end_date + 1/86400;

	INSERT INTO ftd_apps.FEES_FEEDS
		    (feed_id,
		     feed_xml,
           	 feed_type,
		     status,
	         created_on
			 --updated_on,
		    )
	VALUES ('MFD'|| MOR_DEL_SEQ.nextval,
		     in_feed_xml,
           	 in_feed_type,
		     in_status,
		     v_start_date
		     --v_start_date
		     );

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_MDELIVERY_FEE_FEED;



PROCEDURE INSERT_FUEL_SURCHARGE_FEED
(
IN_FEED_XML              IN  ftd_apps.FEES_FEEDS.FEED_XML%TYPE,
IN_FEED_TYPE             IN  ftd_apps.FEES_FEEDS.FEED_TYPE%TYPE,
IN_STATUS   			       IN  ftd_apps.FEES_FEEDS.STATUS%TYPE,

OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                    OUT VARCHAR2
)
AS
/************************************************************************
This procecure is responsible for inserting the the fuel surcharge feed.

INPUT:
	in_feed_xml              in  ftd_apps.FEES_FEEDS.feed_xml%type
	in_feed_type             in  ftd_apps.FEES_FEEDS.feed_type%type
	in_status   			       in  ftd_apps.FEES_FEEDS.status%type

OUTPUT:
	out_status                     out varchar2
	out_message                    out varchar2

*************************************************************************/

v_last_end_date		FUEL_SURCHARGE.END_DATE%TYPE;
v_start_date		FUEL_SURCHARGE.START_DATE%TYPE;


BEGIN
	out_status := 'N';

	v_last_end_date := sysdate;

	-- next second
	v_start_date := v_last_end_date + 1/86400;

	INSERT INTO ftd_apps.FEES_FEEDS
		    (feed_id,
		     feed_xml,
           	 feed_type,
		     status,
	         created_on
			 --updated_on,
		    )
	VALUES ('FFD'|| FUEL_SUR_SEQ.nextval,
		     in_feed_xml,
           	 in_feed_type,
		     in_status,
		     v_start_date
		     --v_start_date
		     );

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_FUEL_SURCHARGE_FEED;

PROCEDURE GENERATE_BULK_FEE_FEEDS
(
IN_FEED_TYPE                      IN  VARCHAR2,
IN_ID_LIST                        IN  VARCHAR2,

OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                    OUT VARCHAR2
)
AS
/************************************************************************
This procecure is responsible for inserting the the bulk source feeds. 

INPUT:
  in_feed_type                      IN  VARCHAR2, (SOURCE_FEED, SHIPPING_KEY_FEED, FUEL)
	in_id_list              IN  VARCHAR2,
OUTPUT:
	out_status                     out varchar2
	out_message                    out varchar2

*************************************************************************/

v_id_list		VARCHAR2(4000) := IN_ID_LIST;
v_snh_id		VARCHAR2(40);
v_deli_fee_id		VARCHAR2(40);
v_feed_count  NUMBER;
v_feed_xml  CLOB;
v_last_end_date		delivery_fee.created_on%TYPE;
v_start_date		delivery_fee.created_on%TYPE;

cursor feeds_cur is
    WITH DATA AS
    ( SELECT v_id_list id FROM dual
    )
    SELECT trim(regexp_substr(id, '[^,]+', 1, LEVEL)) id
    FROM DATA
    CONNECT BY instr(id, ',', 1, LEVEL - 1) > 0;
cursor service_override_cur is
    select * from FTD_APPS.SERVICE_FEE_OVERRIDE where snh_id = v_snh_id and override_date > sysdate - 1;
    
BEGIN
	out_status := 'N';

	v_last_end_date := sysdate;

	-- next second
	v_start_date := v_last_end_date + 1/86400;
  
  FOR feed in feeds_cur LOOP
    dbms_output.put_line  (IN_FEED_TYPE || ': ' || feed.id);
    
    IF IN_FEED_TYPE = 'SOURCE_FEED' THEN
        
        -- check if the service is already sent.if not generate and send now.
        select snh_id into v_snh_id from ftd_apps.source_master where source_code = feed.id;
        
        select count(*) into v_feed_count from ftd_apps.fees_feeds where feed_type = 'serviceFee' and extractValue(xmlType(feed_xml),'/serviceFee/id/text()') = v_snh_id;
         
        IF v_feed_count = 0 and v_snh_id is not null THEN
          dbms_output.put_line  ('Inserting associated service Fee feed: ' || v_snh_id);
          SELECT xmlserialize(content XMLAGG( XMLELEMENT("serviceFee", XMLELEMENT("action", 'Add'),
                               XMLELEMENT ("id", snh_id),
                               XMLELEMENT ("domesticFee", double_formatter(FIRST_ORDER_DOMESTIC)),
                               XMLELEMENT ("floristSameDayUpcharge", double_formatter(same_day_upcharge)),
                               XMLELEMENT ("floristSameDayUpchargeFS", double_formatter(same_day_upcharge_fs)),
                               XMLELEMENT ("internationalFee", double_formatter(first_order_international)),
                               XMLELEMENT ("description", description),
                               XMLELEMENT ("vendorCharge", double_formatter(vendor_charge)),
                               XMLELEMENT ("vendorSatUpcharge", double_formatter(vendor_sat_upcharge)),
                               XMLELEMENT ("vendorSunUpcharge", double_formatter(vendor_sun_upcharge)),
                               XMLELEMENT ("vendorMonUpcharge", double_formatter(vendor_mon_upcharge))
                               )).extract('/*') indent) result into v_feed_xml
          FROM ftd_apps.snh where snh_id = v_snh_id;
          LOOP
              EXIT WHEN v_start_date + (1/86400) <= SYSDATE; -- sleep for 1 sec
          END LOOP;          
          INSERT INTO ftd_apps.FEES_FEEDS
            (feed_id,
             feed_xml,
             feed_type,
             status,
             created_on
            )
          VALUES ('SRV'|| FUEL_SUR_SEQ.nextval,
                 v_feed_xml,
                 'serviceFee',
                 'NEW',
                 v_start_date
                 );
        END IF;
        
         FOR svr_ovr_fee in service_override_cur LOOP
          select count(*) INTO v_feed_count
          from ftd_apps.fees_feeds 
          where feed_type = 'serviceFeeOverride' 
          and extractValue(xmlType(feed_xml),'/serviceFeeOverride/id/text()') = v_snh_id 
          and extractValue(xmlType(feed_xml),'/serviceFeeOverride/overrideDate/text()') = TO_CHAR(svr_ovr_fee.override_date, 'MM/dd/YYYY');      
         
          IF v_feed_count = 0 and v_snh_id is not null THEN
            dbms_output.put_line  ('Inserting associated Service Override Fee feed for service fee: ' || v_snh_id || 'and on date: ' || svr_ovr_fee.override_date);
            SELECT xmlserialize(content XMLAGG( XMLELEMENT("serviceFeeOverride", XMLELEMENT("action", 'Add'),
                           XMLELEMENT ("id", snh_id),
                           XMLELEMENT ("overrideDate", TO_CHAR(override_date, 'MM/dd/YYYY')),
                           XMLELEMENT ("domesticFee", double_formatter(DOMESTIC_CHARGE)),
                           XMLELEMENT ("floristSameDayUpcharge", double_formatter(same_day_upcharge)),
                           XMLELEMENT ("floristSameDayUpchargeFS", double_formatter(same_day_upcharge_fs)),
                           XMLELEMENT ("internationalFee", double_formatter(INTERNATIONAL_CHARGE)),
                           XMLELEMENT ("description", (select description as description from ftd_apps.snh where snh_id = '654')),
                           XMLELEMENT ("vendorCharge", double_formatter(vendor_charge)),
                           XMLELEMENT ("vendorSatUpcharge", double_formatter(vendor_sat_upcharge)),
                           XMLELEMENT ("vendorSunUpcharge", double_formatter(vendor_sun_upcharge)),
                           XMLELEMENT ("vendorMonUpcharge", double_formatter(vendor_mon_upcharge))
                           )).extract('/*') indent) result INTO v_feed_xml
            FROM ftd_apps.SERVICE_FEE_OVERRIDE where snh_id = v_snh_id and override_date = svr_ovr_fee.override_date;
          LOOP
              EXIT WHEN v_start_date + (1/86400) <= SYSDATE; -- sleep for 1 sec
          END LOOP;             
           INSERT INTO ftd_apps.FEES_FEEDS
              (feed_id, 
               feed_xml,
               feed_type,
               status,
               created_on
              )
            VALUES ('SRV_OVR'|| FUEL_SUR_SEQ.nextval,
                   v_feed_xml,
                   'serviceFeeOverride',
                   'NEW',
                   v_start_date
                   );
          END IF;
        END LOOP;
        select delivery_fee_id into v_deli_fee_id from ftd_apps.source_master where source_code = feed.id;
        select count(*) into v_feed_count from ftd_apps.fees_feeds where feed_type = 'MORNING_DELIVERY' and extractValue(xmlType(feed_xml),'/morningDelivery/id/text()') = v_deli_fee_id;
         
        IF v_feed_count = 0 and v_deli_fee_id is not null THEN
          dbms_output.put_line  ('Inserting associated Morning Delivery Fee feed: ' || v_deli_fee_id);
               SELECT xmlserialize(content XMLAGG( XMLELEMENT("morningDelivery", XMLELEMENT("action", 'Add'),
                               XMLELEMENT ("id", delivery_fee_id),
                               XMLELEMENT ("morningDeliveryFee", double_formatter(morning_delivery_fee)),
                               XMLELEMENT ("description", description)
                               )).extract('/*') indent) result into v_feed_xml
          FROM ftd_apps.delivery_fee where delivery_fee_id = v_deli_fee_id;
	      LOOP
              EXIT WHEN v_start_date + (1/86400) <= SYSDATE; -- sleep for 1 sec
          END LOOP;          
          INSERT INTO ftd_apps.FEES_FEEDS
            (feed_id,
             feed_xml,
             feed_type,
             status,
             created_on
            )
          VALUES ('MFD'|| MOR_DEL_SEQ.nextval,
                 v_feed_xml,
                 'MORNING_DELIVERY',
                 'NEW',
                 v_start_date
                 );
        END IF;

        LOOP
              EXIT WHEN v_start_date + (1/86400) <= SYSDATE; -- sleep for 1 sec
        END LOOP;  
        dbms_output.put_line  ('Inserting source code feed: ' || feed.id);
        FTD_APPS.POST_STORE_FEEDS_COMMAND(IN_FEED_TYPE || ' ' || feed.id); 
        commit;

    ELSIF IN_FEED_TYPE = 'SHIPPING_KEY_FEED' THEN
    
        dbms_output.put_line  ('Inserting shipping key feed: ' || feed.id);
        FTD_APPS.POST_STORE_FEEDS_COMMAND(IN_FEED_TYPE || ' ' || feed.id); 
        commit;

    ELSIF IN_FEED_TYPE = 'FUEL' THEN
    
         dbms_output.put_line  ('Inserting Fuel surcharge feed: ' || v_deli_fee_id);
         SELECT xmlserialize(content XMLAGG( XMLELEMENT("fuel_surcharge",
                         XMLELEMENT ("apply_surcharge_code", apply_surcharge_code),
                         XMLELEMENT ("fuel_surcharge_amt", double_formatter(fuel_surcharge_amt)),
                         XMLELEMENT ("surcharge_description", SURCHARGE_DESCRIPTION),
                         XMLELEMENT ("send_surcharge_to_florist", SEND_SURCHARGE_TO_FLORIST_FLAG),
                         XMLELEMENT ("display_surcharge", DISPLAY_SURCHARGE_FLAG)
                         )).extract('/*') indent) result into v_feed_xml
        from (
              select * from FTD_APPS.FUEL_SURCHARGE order by start_date desc offset 0 row fetch next 1 row only
             )
       where rownum = 1;
       INSERT_FUEL_SURCHARGE_FEED(v_feed_xml,IN_FEED_TYPE, 'NEW', OUT_STATUS, OUT_MESSAGE);
    
    END IF;

   END LOOP;

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
		OUT_STATUS := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END GENERATE_BULK_FEE_FEEDS;


END FEES_FEEDS_PKG;
/