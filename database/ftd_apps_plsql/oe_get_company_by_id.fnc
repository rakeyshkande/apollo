CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_COMPANY_BY_ID (inCompanyId IN VARCHAR2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_COMPANY_BY_ID
-- Type:    Function
-- Syntax:  OE_GET_COMPANY_BY_ID ( inCompanyId IN VARCHAR2 )
-- Returns: ref_cursor for
--          COMPANY_ID              VARCHAR2(10)
--          COMPANY_NAME            VARCHAR2(30)
--          DEFAULT_DNIS_ID         NUMBER
--          DEFAULT_SOURCE_CODE     VARCHAR2(10)
--
-- Description:   Queries the COMPANY table by company ID and returns a ref cursor
--                to the resulting row.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT COMPANY_ID as "companyId",
               COMPANY_NAME as "companyName",
               DEFAULT_DNIS_ID as "dnisId",
			         DEFAULT_SOURCE_CODE as "sourceCode"
        FROM COMPANY
        WHERE COMPANY_ID = inCompanyId;

    RETURN cur_cursor;
END;
.
/
