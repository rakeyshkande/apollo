CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_DEL_EXCL_STATES
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_DEL_EXCL_STATES
-- Type:    Function
-- Syntax:  SP_GET_DEL_EXCL_STATES ()
-- Returns: ref_cursor for
--          stateId          VARCHAR2(2)
--      StareName        varchar2(30)
--
-- Description:  Queries the STATE_MASTER table for all DELIVERY_EXCLUSION
--               values of 'Y' and returns a ref cursor to the resulting row.
--
--==============================================================================
AS
    states_cursor types.ref_cursor;
BEGIN
    OPEN states_cursor FOR
        SELECT STATE_MASTER_ID             as "stateId",
               STATE_NAME          as "stateName"
          FROM STATE_MASTER
         WHERE DELIVERY_EXCLUSION = 'Y'
         ORDER BY STATE_NAME ASC;

    RETURN states_cursor;
END;
.
/
