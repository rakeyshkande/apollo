CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_SUBCODES2
    (productId IN varchar2)
    RETURN types.ref_cursor
AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_SUBCODES
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_SUBCODES (productId IN varchar2 )
-- Returns: ref_cursor for
--          PRODUCT_ID            VARCHAR2(10)
--          PRODUCT_SUBCODE_ID    VARCHAR2(10)
--          SUBCODE_DESCRIPTION   VARCHAR2(100)
--          SUBCODE_PRICE         NUMBER(8,2)
--          DISPLAY_ORDER         NUMBER(3,0)
--
-- Description:   Queries the PRODUCT_SUBCODES table by PRODUCT_ID and
--                returns a ref cursor for resulting row.
--
-- 12/3/02 emueller, added vendor sku and dimWeight fields
--======================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT PRODUCT_ID          as "productId",
               PRODUCT_SUBCODE_ID  as "productSubcodeId",
               SUBCODE_DESCRIPTION as "subcodeDescription",
               SUBCODE_PRICE       as "subcodePrice",
               HOLIDAY_SKU         as "subcodeHolidaySKU",
               HOLIDAY_PRICE       as "subcodeHolidayPrice",
               ACTIVE_FLAG         as "subCodeAvailable",
               SUBCODE_REFERENCE_NUMBER as "subcodeRefNumber",
               VENDOR_PRICE        as "subcodeVendorPrice",
               VENDOR_SKU          as "vendorSKU",
               DIM_WEIGHT          as "dimWeight",
               DISPLAY_ORDER       AS "displayOrder"
             FROM PRODUCT_SUBCODES
    WHERE PRODUCT_ID = UPPER(productId)
    ORDER BY DISPLAY_ORDER;

    RETURN cur_cursor;
END;
.
/
