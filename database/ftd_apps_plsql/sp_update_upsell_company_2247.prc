CREATE OR REPLACE
PROCEDURE ftd_apps.sp_update_upsell_company_2247 (productID IN VARCHAR2, companyID VARCHAR2)
--============================================================================
--
-- Name:    sp_update_upsell_company_2247
-- Type:    PROCEDURE
-- Syntax:  sp_update_upsell_company_2247(productID, companyID )
--
-- Description:   Adds a record to UPSELL_COMPANY_XREF
--
--===========================================================
AS
BEGIN
 INSERT INTO UPSELL_COMPANY_XREF (PRODUCT_ID, COMPANY_ID)
 VALUES (productID, companyID);
END;
.
/
