CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PROMOTION_PROGRAM (promotionID IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_PROMOTION_PROGRAM
-- Type:    Function
-- Syntax:  OE_GET_PROMOTION_PROGRAM (promotionID IN varchar2)
-- Returns: ref_cursor for
--         PROMOTION_ID      as promotionId
--         BASE_POINTS       as basePoints
--         UNIT_POINTS       as unitPoints
--         UNIT              as unit
--         MIN_PRICE         as minPrice
--         MAX_PRICE         as maxPrice
--         REWARD_TYPE       as rewardType
--
-- Description:   Queries the PROMOTION_PROGRAMS table by promotionID = PROMOTION_ID and
--                returns a ref cursor for resulting row.
--
--                UNIT:
--                    DOL = points per dollar
--                    ITM = points per item
--                    PER = percent
--                REWARD_TYPE:
--                    PTS = points
--                    MLS = miles
--                    PER = percent
--
-- Changed: Defect 4906
--          The old function wasn't properly handling a bonus_calculation_basis of 'M'
--          This new version gets all of its data from ftd_apps.program_reward
--
--=============================================================
AS

cursor get_program_reward(programId in varchar2) is
    select program_name, calculation_basis, points, bonus_calculation_basis, bonus_points,
        reward_type
    from ftd_apps.program_reward
    where program_name = programId;

v_program_name varchar2(1000);
v_base_points number;
v_unit_points number;
v_unit varchar2(100);
v_min_price number := 0;
v_max_price number := 100000;
v_reward_type varchar2(1000);
promotion_cursor types.ref_cursor;

BEGIN

    FOR x IN get_program_reward(promotionID) LOOP

        v_base_points := 0;
        v_unit_points := 0;
        v_unit := 'DOL';

        if (x.calculation_basis = 'F') then
            v_base_points := v_base_points + nvl(x.points, 0);
            if (x.bonus_calculation_basis = 'F') then
                v_base_points := v_base_points + nvl(x.bonus_points, 0);
            else
                v_unit_points := v_unit_points + nvl(x.bonus_points, 0);
            end if;
        else
            v_unit_points := v_unit_points + nvl(x.points, 0);
            if (x.bonus_calculation_basis = 'F') then
                v_base_points := v_base_points + nvl(x.bonus_points, 0);
            else
                v_unit_points := v_unit_points + nvl(x.bonus_points, 0);
            end if;
        end if;

        v_program_name := x.program_name;
        v_reward_type := x.reward_type;

    END LOOP;

    OPEN promotion_cursor FOR
        SELECT v_program_name        as "promotionId",
               v_base_points         as "basePoints",
               v_unit_points         as "unitPoints",
               v_unit                as "unit",
               v_min_price           as "minPrice",
               v_max_price           as "maxPrice",
               v_reward_type         as "rewardType"
        FROM dual;

    RETURN promotion_cursor;
END
;
.
/
