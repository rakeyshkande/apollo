CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_GLOBAL_PARMS
    RETURN types.ref_cursor
--==============================================================================
--
-- Returns: ref_cursor for
--          gnaddLevel                   NUMBER,
--          gnaddDate                    VARCHAR2(10),
--          intlOverrideDays             NUMBER,
--          mondayCutoff                 VARCHAR2(5),
--          tuesdayCutoff                VARCHAR2(5),
--          wednesdayCutoff              VARCHAR2(5),
--          thursdayCutoff               VARCHAR2(5),
--          fridayCutoff                 VARCHAR2(5),
--          saturdayCutoff               VARCHAR2(5),
--          sundayCutoff                 VARCHAR2(5),
--          freshcutsCutoff              VARCHAR2(5),
--          specialtyGiftCutoff          VARCHAR2(5),
--          intlCutoff                   VARCHAR2(4),
--          deliveryDaysOut              NUMBER,
--          checkJcPenney                VARCHAR2(1),
--          freshCutsSvcCharge           NUMBER,
--          specialSvcCharge             NUMBER,
--          freshCutsSvcChargeTrigger    VARCHAR2(1),
--          canadianExchangeRate         NUMBER,
--          freshCutsSatCharge           NUMBER
--
-- Description:   Queries the GLOBAL_PARMS table, which has a single row, and
--                returns the row's contents in a ref cursor.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT GNADD_LEVEL as "gnaddLevel",
            to_char(GNADD_DATE, 'mm/dd/yyyy') as "gnaddDate",
            INTL_OVERRIDE_DAYS as "intlOverrideDays",
            MONDAY_CUTOFF as "mondayCutoff",
            TUESDAY_CUTOFF as "tuesdayCutoff",
            WEDNESDAY_CUTOFF as "wednesdayCutoff",
            THURSDAY_CUTOFF as "thursdayCutoff",
            FRIDAY_CUTOFF as "fridayCutoff",
            SATURDAY_CUTOFF as "saturdayCutoff",
            SUNDAY_CUTOFF as "sundayCutoff",
            EXOTIC_CUTOFF as "exoticCutoff",
            FRESHCUTS_CUTOFF as "freshcutsCutoff",
            SPECIALITYGIFT_CUTOFF as "specialtyGiftCutoff",
            INTL_CUTOFF as "internationalCutoff",
            DELIVERY_DAYS_OUT as "deliveryDaysOut",
            CHECK_JC_PENNEY as "checkJcPenney",
                              FRESHCUTS_SVC_CHARGE as "freshCutsSvcCharge",
                              SPECIAL_SVC_CHARGE as "specialSvcCharge",
                              FRESHCUTS_SVC_CHARGE_TRIGGER as "freshCutsSvcChargeTrigger",
                              CANADIAN_EXCHANGE_RATE as "canadianExchangeRate",
                              FRESHCUTS_SAT_CHARGE as "freshCutsSatCharge",
		DELIVERY_DAYS_OUT_MAX as "deliveryDaysOutMAX",
		DELIVERY_DAYS_OUT_MIN as "deliveryDaysOutMIN",
		ALLOW_SUBSTITUTION as "allowSubstitution",
            FLORAL_LABEL_STANDARD as "standardLabel",
            FLORAL_LABEL_DELUXE as "deluxeLabel",
            FLORAL_LABEL_PREMIUM as "premiumLabel"
        FROM GLOBAL_PARMS;

    RETURN cur_cursor;
END;
.
/
