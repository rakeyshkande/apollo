CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_STATE_BY_COUNTRY (countryCode varchar2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_STATE_BY_COUNTRY
-- Type:    Function
-- Syntax:  OE_GET_STATE_BY_COUNTRY(countryCode IN VARCHAR2)
-- Returns: ref_cursor for
--          STATE_NAME      VARCHAR2(30)
--
--
-- Description:   Queries the STATE_MASTER table by countryCode and
--                returns a ref cursor for resulting row.
--
--=============================================================
AS
    state_cursor types.ref_cursor;
BEGIN
    OPEN state_cursor FOR
        SELECT STATE_NAME as "stateName"
          FROM STATE_MASTER
         WHERE country_Code = countryCode ;

    RETURN state_cursor;
END
;
.
/
