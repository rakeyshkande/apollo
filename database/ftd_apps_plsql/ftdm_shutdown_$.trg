-- FTD_APPS.FTDM_SHUTDOWN_$ trigger to populate FTD_APPS.FTDM_SHUTDOWN$ shadow table

CREATE OR REPLACE TRIGGER FTD_APPS.ftdm_shutdown_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.ftdm_shutdown REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.ftdm_shutdown$ (
      	shutdown_flag,
      	shutdown_by,
      	shutdown_date,
      	restart_date,
      	restarted_by,
      	last_updated_date,
      	last_updated_by,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.shutdown_flag,
      	:NEW.shutdown_by,
      	:NEW.shutdown_date,
      	:NEW.restart_date,
      	:NEW.restarted_by,
      	:NEW.last_updated_date,
      	:NEW.last_updated_by,
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.ftdm_shutdown$ (
      	shutdown_flag,
      	shutdown_by,
      	shutdown_date,
      	restart_date,
      	restarted_by,
      	last_updated_date,
      	last_updated_by,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.shutdown_flag,
      	:OLD.shutdown_by,
      	:OLD.shutdown_date,
      	:OLD.restart_date,
      	:OLD.restarted_by,
      	:OLD.last_updated_date,
      	:OLD.last_updated_by,
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO ftd_apps.ftdm_shutdown$ (
      	shutdown_flag,
      	shutdown_by,
      	shutdown_date,
      	restart_date,
      	restarted_by,
      	last_updated_date,
      	last_updated_by,
			operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.shutdown_flag,
      	:NEW.shutdown_by,
      	:NEW.shutdown_date,
      	:NEW.restart_date,
      	:NEW.restarted_by,
      	:NEW.last_updated_date,
      	:NEW.last_updated_by,
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.ftdm_shutdown$ (
      	shutdown_flag,
      	shutdown_by,
      	shutdown_date,
      	restart_date,
      	restarted_by,
      	last_updated_date,
      	last_updated_by,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.shutdown_flag,
      	:OLD.shutdown_by,
      	:OLD.shutdown_date,
      	:OLD.restart_date,
      	:OLD.restarted_by,
      	:OLD.last_updated_date,
      	:OLD.last_updated_by,
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
