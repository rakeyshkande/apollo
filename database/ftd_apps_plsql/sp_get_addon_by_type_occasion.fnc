CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_ADDON_BY_TYPE_OCCASION (typeIn varchar2, occasionIn IN varchar2, addonId IN VARCHAR2)
RETURN types.ref_cursor

--==============================================================================
--
-- Name:    SP_GET_ADDON_BY_TYPE_OCCASION
-- Type:    Function
-- Syntax:  SP_GET_ADDON_BY_TYPE_OCCASION (typeIn VARCHAR2, occasionIn varchar2,
--                                         addonId VARCHAR2)
-- Returns: ref_cursor for
--          ADDON_ID       NUMBER
--          DESCRIPTION    VARCHAR2(50)
--          PRICE          NUMBER
--          TEXT           VARCHAR2(255)
--
-- Description:   Queries the ADDON and OCCASION_ADDON Tables by Addon Type,
--                Occasion ID and / or Addon ID and returns a ref cursor to the
--                resulting row.  If an occasion is not provided, gets all addons
--                of the specified type.
--
--=============================================================
AS
    addon_cursor types.ref_cursor;
BEGIN
    OPEN addon_cursor FOR
         SELECT A.ADDON_ID as "addonId",
                A.DESCRIPTION as "description",
                A.PRICE as "price",
                A.ADDON_TEXT as "inside"
          FROM ADDON A
          WHERE A.ADDON_TYPE = typeIn
            AND A.ACTIVE_FLAG = 'Y'
            AND (( occasionIn IS NULL )
              OR EXISTS (
                SELECT 1 FROM OCCASION_ADDON OA
                WHERE OA.OCCASION_ID = occasionIn
                AND OA.ADDON_ID = A.ADDON_ID
              ))
            AND A.ADDON_ID = NVL(addonId, A.ADDON_ID);

    RETURN addon_cursor;
END
;
.
/
