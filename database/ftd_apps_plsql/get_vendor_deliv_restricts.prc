CREATE OR REPLACE
PROCEDURE ftd_apps.get_vendor_deliv_restricts
(
  in_vendor_id           IN vendor_delivery_restrictions.vendor_id%TYPE,
  out_cur               OUT types.ref_cursor
) AS

/*-----------------------------------------------------------------------------
    This procedure will retrieve a row from vendor_delivery_restrictions, given
    a vendor ID.

Input:
        in_vendor_id                      varchar2

Output:
        out                               ref cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT vendor_id, start_date, end_date
FROM   vendor_delivery_restrictions
WHERE  vendor_id = in_vendor_id;

END get_vendor_deliv_restricts;
.
/
