create or replace
PACKAGE BODY FTD_APPS.OSCAR_QUERY_PKG AS

  PROCEDURE GET_OSCAR_ZIP_CODE 
(
  OUT_CUR OUT TYPES.REF_CURSOR
) AS
  BEGIN
OPEN out_cur FOR
        SELECT
                z.zip_code_id,
                z.city,
                z.state_id,
                z.oscar_zip_flag,                
                z.oscar_city_state_flag 
        FROM  ftd_apps.zip_code z;      

  END GET_OSCAR_ZIP_CODE;

END OSCAR_QUERY_PKG;
.
/