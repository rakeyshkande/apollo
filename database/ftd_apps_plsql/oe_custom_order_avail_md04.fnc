CREATE OR REPLACE
FUNCTION ftd_apps.OE_CUSTOM_ORDER_AVAIL_MD04 (inCustOrderPid IN VARCHAR2,
  inSourceCode IN VARCHAR2)
  RETURN types.ref_cursor
--=================================================================
--
-- Name:    OE_CUSTOM_ORDER_AVAIL_MD04
-- Type:    Function
-- Syntax:  OE_CUSTOM_ORDER_AVAIL_MD04
--          (inCustOrderPid IN VARCHAR2, inSourceCode IN VARCHAR2)
-- Returns: ref_cursor for
--          sourceCompanyId   VARCHAR2(12)
--
-- Description: Used to check if custom order is available
--              for specified source code.  If no records returned, custom
--              order is not available.  Custom order number should
--              be constant (6611), but we pass it as parameter.
--
--==================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT pcx.COMPANY_ID AS "sourceCompanyId"
        FROM PRODUCT_COMPANY_XREF pcx, SOURCE src
        WHERE pcx.PRODUCT_ID = inCustOrderPid
        AND src.SOURCE_CODE = inSourceCode
        AND src.COMPANY_ID = pcx.COMPANY_ID;
    RETURN cur_cursor;
END;
.
/
