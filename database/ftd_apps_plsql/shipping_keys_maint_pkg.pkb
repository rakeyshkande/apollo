CREATE OR REPLACE PACKAGE BODY FTD_APPS.SHIPPING_KEYS_MAINT_PKG AS

PROCEDURE DELETE_SHIPPING_KEYS
(
IN_SHIPPING_KEY_ID						IN	FTD_APPS.SHIPPING_KEYS.SHIPPING_KEY_ID%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a record from SHIPPING_KEYS

Input:
        in_shipping_key_id		             varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   DELETE FROM ftd_apps.shipping_keys WHERE shipping_key_id = in_shipping_key_id;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.DELETE_SHIPPING_KEYS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_SHIPPING_KEYS;

PROCEDURE UPDATE_SHIPPING_KEYS
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEYS.SHIPPING_KEY_ID%TYPE,
IN_UPDATED_BY								IN FTD_APPS.SHIPPING_KEYS.UPDATED_BY%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a record in SHIPPING_KEYS

Input:
        in_shipping_key_id						 varchar2
        in_updated_by							 varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   UPDATE ftd_apps.shipping_keys 
      SET updated_on = SYSDATE, 
          updated_by = in_updated_by 
    WHERE shipping_key_id = in_shipping_key_id;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.UPDATE_SHIPPING_KEYS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;

END UPDATE_SHIPPING_KEYS;

PROCEDURE UPDATE_SHIPPING_KEYS
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEYS.SHIPPING_KEY_ID%TYPE,
IN_SHIPPING_KEY_DESCRIPTION			IN	FTD_APPS.SHIPPING_KEYS.SHIPPING_KEY_DESCRIPTION%TYPE,
IN_SHIPPER									IN FTD_APPS.SHIPPING_KEYS.SHIPPER%TYPE,
IN_UPDATED_BY								IN FTD_APPS.SHIPPING_KEYS.UPDATED_BY%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a record in SHIPPING_KEYS

Input:
        in_shipping_key_id						 varchar2
        in_shipping_key_description        varchar2
        in_shipper								 varchar2
        in_updated_by							 varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   UPDATE ftd_apps.shipping_keys 
      SET shipping_key_description = in_shipping_key_description, 
          shipper = in_shipper, 
          updated_on = SYSDATE, 
          updated_by = in_updated_by 
    WHERE shipping_key_id = in_shipping_key_id;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.UPDATE_SHIPPING_KEYS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_SHIPPING_KEYS;

PROCEDURE DELETE_SHIPPING_KEY_COSTS
(
IN_SHIPPING_KEY_ID						IN	FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a record from SHIPPING_KEY_COSTS

Input:
        in_shipping_key_id		             varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   DELETE FROM ftd_apps.shipping_key_costs 
         WHERE shipping_key_detail_id IN (
        SELECT shipping_detail_id 
          FROM ftd_apps.shipping_key_details 
         WHERE shipping_key_id = in_shipping_key_id);

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.DELETE_SHIPPING_KEY_COSTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_SHIPPING_KEY_COSTS;

PROCEDURE UPDATE_SHIPPING_KEY_COSTS
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
IN_UPDATED_BY								IN FTD_APPS.SHIPPING_KEY_COSTS.UPDATED_BY%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a record in SHIPPING_KEY_COSTS

Input:
        in_shipping_key_id						 varchar2
        in_updated_by							 varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	UPDATE ftd_apps.shipping_key_costs
	   SET updated_on = SYSDATE, 
	   	 updated_by = in_updated_by
	 WHERE shipping_key_detail_id IN (
	SELECT shipping_key_detail_id 
	  FROM ftd_apps.shipping_key_details 
	 WHERE shipping_key_id = in_shipping_key_id);

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.UPDATE_SHIPPING_KEY_COSTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_SHIPPING_KEY_COSTS;

PROCEDURE DELETE_SHIPPING_KEY_DETAILS
(
IN_SHIPPING_KEY_ID						IN	FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a record from SHIPPING_KEY_DETAILS

Input:
        in_updated_on							 date
        in_updated_by							 varchar2
        in_shipping_key_id						 varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   DELETE FROM ftd_apps.shipping_key_details WHERE shipping_key_id = in_shipping_key_id;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.DELETE_SHIPPING_KEY_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_SHIPPING_KEY_DETAILS;

PROCEDURE UPDATE_SHIPPING_KEY_DETAILS
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
IN_UPDATED_BY								IN FTD_APPS.SHIPPING_KEY_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a record in SHIPPING_KEY_DETAILS

Input:
        in_shipping_key_id						 varchar2
        in_updated_by							 varchar2

Output:
        status                          	 varchar2 (Y or N)
        message                         	 varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	UPDATE ftd_apps.shipping_key_details
	   SET updated_on = SYSDATE, 
	   	 updated_by = in_updated_by
	 WHERE shipping_key_id = in_shipping_key_id;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_MAINT_PKG.UPDATE_SHIPPING_KEY_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_SHIPPING_KEY_DETAILS;

END SHIPPING_KEYS_MAINT_PKG;
.
/
