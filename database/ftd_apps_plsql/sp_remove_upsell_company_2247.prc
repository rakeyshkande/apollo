CREATE OR REPLACE
PROCEDURE ftd_apps.sp_remove_upsell_company_2247 (productID IN VARCHAR2)
 --===========================================================================
--
-- Name:    sp_remove_upsell_company_2247
-- Type:    PROCEDURE
-- Syntax:  sp_remove_upsell_company_2247(productID )
--
-- Description:   Removes all records for the product in UPSELL_COMPANY_XREF
--
--===========================================================
AS
BEGIN
 DELETE FROM upsell_company_xref
 WHERE product_id = productID;
END;
.
/
