CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_PAYMENT_METHOD  (
    inPaymentMethodId IN VARCHAR2 )
 RETURN types.ref_cursor
--=================================================================
--
-- Name:    OE_GET_PAYMENT_METHOD
-- Type:    Function
-- Syntax:  OE_GET_PAYMENT_METHOD (inPaymentMethodId IN VARCHAR2)
-- Returns: ref_cursor for
--          paymentMethodId VARCHAR2(2)
--          description VARCHAR2(25)
--          paymentType CHAR(1)
--          cardId   VARCHAR2(10)
--
--
--==================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT  PAYMENT_METHOD_ID as "paymentMethodId",
                DESCRIPTION as "description",
                PAYMENT_TYPE as "paymentType",
                CARD_ID as "cardId"
        FROM PAYMENT_METHODS
        WHERE PAYMENT_METHOD_ID = inPaymentMethodId;

    RETURN cur_cursor;
END;
.
/
