CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_SHIP_METHOD_ANF (
 productId in varchar2,
 shippingMethod in varchar2,
 defaultCarrier in varchar2
)
authid current_user
as
--==============================================================================
--
-- Name:    SP_UPDATE_SHIP_METHOD_ANF
-- Type:    Procedure
-- Syntax:  SP_UPDATE_SHIP_METHOD_ANF ( productId in varchar2,
--                                  shippingMethod in varchar2 )
--
-- Description:   Attempts to insert into PRODUCT_SHIP_METHODS.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_SHIP_METHODS
  INSERT INTO PRODUCT_SHIP_METHODS
      ( PRODUCT_ID,
				SHIP_METHOD_ID,
				DATE_LAST_MODIFIED,
                DEFAULT_CARRIER
		  )
  VALUES
		  ( productId,
				shippingMethod,
				SYSDATE,
                defaultCarrier
		  );

end;
.
/
