CREATE OR REPLACE
PACKAGE BODY ftd_apps.FLORIST_RESUME_JOB_PKG AS

TYPE pg_florist_id_tab_typ IS TABLE OF FLORIST_MASTER.FLORIST_ID%TYPE INDEX BY PLS_INTEGER;
TYPE pg_block_start_date_tab_typ IS TABLE OF FLORIST_BLOCKS.BLOCK_START_DATE%TYPE INDEX BY PLS_INTEGER;

pg_check_blocks_job             VARCHAR2(100) := 'FLORIST_RESUME_JOB_PKG.CHECK_BLOCKS;';
pg_check_ftdm_shutdown_job      VARCHAR2(100) := 'FLORIST_RESUME_JOB_PKG.CHECK_FTDM_SHUTDOWN(''SYS'');';


PROCEDURE START_FLORIST_RESUME_JOBS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for scheduling the florist resume jobs.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
AS

v_jobno         number;
v_runtime_date  date;
v_interval      varchar2(100) := 'INTERVAL';
v_interval_val  varchar2(100);

v_check         number;

CURSOR check_cur (p_what varchar2 )IS
        SELECT  1
        FROM    user_jobs
        WHERE   what = p_what;
BEGIN

-- get the runtime and interval parameters for the check blocks job
v_interval_val := frp.misc_pkg.get_global_parm_value (pg_check_blocks_job, v_interval);
EXECUTE IMMEDIATE 'select '||v_interval_val||' from dual' INTO v_runtime_date;

-- schedule the check blocks job if it isn't already scheduled
OPEN check_cur (pg_check_blocks_job);
FETCH check_cur INTO v_check;
IF check_cur%notfound THEN
        dbms_job.submit(job=>v_jobno, what=>pg_check_blocks_job, next_date=>v_runtime_date, interval=>v_interval_val);
        commit;
END IF;
CLOSE check_cur;

-- get the runtime and interval parameters for the check ftdm shutdown job
v_interval_val := frp.misc_pkg.get_global_parm_value (pg_check_ftdm_shutdown_job, v_interval);
EXECUTE IMMEDIATE 'select '||v_interval_val||' from dual' INTO v_runtime_date;

-- schedule the check ftdm shutdown job if it isn't already scheduled
OPEN check_cur (pg_check_ftdm_shutdown_job);
FETCH check_cur INTO v_check;
IF check_cur%notfound THEN
        dbms_job.submit(job=>v_jobno, what=>pg_check_ftdm_shutdown_job, next_date=>v_runtime_date, interval=>v_interval_val);
        commit;
END IF;
CLOSE check_cur;

END START_FLORIST_RESUME_JOBS;


PROCEDURE STOP_FLORIST_RESUME_JOBS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing the florist resume
        jobs from the schedule.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
AS

v_job           number;

CURSOR check_cur (p_what varchar2 )IS
        SELECT  job
        FROM    user_jobs
        WHERE   what = p_what;
BEGIN

-- remove the check blocks job from the schedule
OPEN check_cur (pg_check_blocks_job);
FETCH check_cur INTO v_job;
IF check_cur%found THEN
        dbms_job.remove (v_job);
        commit;
END IF;
CLOSE check_cur;

-- remove the check ftdm shutdown job from the schedule
OPEN check_cur (pg_check_ftdm_shutdown_job);
FETCH check_cur INTO v_job;
IF check_cur%found THEN
        dbms_job.remove (v_job);
        commit;
END IF;
CLOSE check_cur;

END STOP_FLORIST_RESUME_JOBS;


PROCEDURE CHECK_FLORIST_BLOCKS
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any blocks expire
        on the current date.  If so, update the block record with null
        values and start the available zip process for each florist
        found.

        Only top level florists are checked for blocks based on the rule that
        if any parent/child is blocked then all florists in the parent/child
        tree are blocked.  The update_florist_blocks will traverse the tree
        to unblock all of the florists owned by the top level florist.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_today                 date := trunc(sysdate);
v_florist_id_tab        pg_florist_id_tab_typ;
v_block_start_date_tab  pg_block_start_date_tab_typ;
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

CURSOR check_cur IS
        SELECT  b.florist_id, b.block_start_date
        FROM    florist_blocks b
        WHERE   trunc (b.block_end_date) < v_today
        AND     EXISTS
        (       select  1
                from    florist_master m
                where   m.top_level_florist_id = b.florist_id
                and     m.status <> 'Inactive'
                and     rownum = 1
        );

BEGIN

OPEN check_cur;
FETCH check_cur BULK COLLECT into v_florist_id_tab, v_block_start_date_tab;
CLOSE check_cur;

FOR x in 1..v_florist_id_tab.count LOOP
        FLORIST_MAINT_PKG.REMOVE_FLORIST_BLOCKS
        (
                IN_FLORIST_ID => v_florist_id_tab(x),
                IN_BLOCK_TYPE => null,
                IN_BLOCK_INDICATOR => 'N',
                IN_BLOCK_START_DATE => v_block_start_date_tab(x),
                IN_BLOCK_END_DATE => null,
                IN_BLOCKED_BY_USER_ID => user,
                IN_ADDITIONAL_COMMENT => null,
                IN_ENQUEUE_FLAG => 'Y',
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );
        
        IF v_out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_BLOCKS',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'Florist ID::' || v_florist_id_tab(x) || ' -- ' || v_out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );
        END IF;
END LOOP;

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_BLOCKS',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );


END CHECK_FLORIST_BLOCKS;


PROCEDURE CHECK_FLORIST_CITY_BLOCKS
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any city blocks expire
        on the current date.  If so, delete the block record.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_florist_id_tab        pg_florist_id_tab_typ;
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

CURSOR check_cur IS
        SELECT  b.florist_id
        FROM    florist_city_blocks b
        WHERE   b.block_end_date is not null
        AND     trunc (b.block_end_date) < trunc(sysdate);

BEGIN

OPEN check_cur;
FETCH check_cur BULK COLLECT into v_florist_id_tab;
CLOSE check_cur;

FOR x in 1..v_florist_id_tab.count LOOP
        FLORIST_MAINT_PKG.DELETE_FLORIST_CITY_BLOCKS
        (
                IN_FLORIST_ID => v_florist_id_tab(x),
                IN_UPDATED_BY => 'SYS',
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );
        
        IF v_out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_CITY_BLOCKS',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'Florist ID::' || v_florist_id_tab(x) || ' -- ' || v_out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );
        END IF;
END LOOP;

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_CITY_BLOCKS',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );


END CHECK_FLORIST_CITY_BLOCKS;


PROCEDURE CHECK_FLORIST_RESUMES(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any suspends expire
        on the current date.  If so, update the suspend record with null
        values and start the available zip process for each florist
        found.

        Only top level florists are checked for suspends based on the rule that
        if any parent/child is suspended then all florists in the parent/child
        tree are suspended.  The update_florist_suspend will traverse the tree
        to unsuspend all of the florists owned by the top level florist.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_today                 date := sysdate;
v_florist_id_tab        pg_florist_id_tab_typ;
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);
v_goto_await            varchar(2000);

/* Get all records where the block after now
   or we have received a Goto Suspend without
   a mercury suspend and it has exceeded its time to live */







CURSOR check_cur(in_goto_await varchar2) IS
        SELECT  b.florist_id
        FROM    florist_suspends b
         WHERE (  (b.suspend_end_date-(b.timezone_offset_in_minutes/1440)) <= SYSDATE
                       or (b.suspend_end_date is null AND b.suspend_type = 'G'
                AND b.goto_florist_suspend_received <= SYSDATE))
        AND     EXISTS
        (       select  1
                from    florist_master m
                where   m.top_level_florist_id = b.florist_id
                and     m.status <> 'Inactive'
                and     rownum = 1
        );

BEGIN

out_status := 'Y';
out_message := '';



v_goto_await := frp.misc_pkg.get_global_parm_value ('UPDATE_FLORIST_SUSPEND','GOTO_AWAIT_MERC_SUSPEND');
OPEN check_cur(v_goto_await);
FETCH check_cur BULK COLLECT into v_florist_id_tab;
CLOSE check_cur;

FOR x in 1..v_florist_id_tab.count LOOP
        FLORIST_MAINT_PKG.FLORIST_SUSPEND_RESUME
        (
                IN_FLORIST_ID => v_florist_id_tab (x),
                IN_SUSPEND_TYPE =>'M',
                IN_SUSPEND_INDICATOR => 'N',
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );







        IF v_out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_SUSPENDS',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'Florist ID::' || v_florist_id_tab (x) || ' -- ' || v_out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );
        END IF;
END LOOP;


EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_SUSPENDS',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );

END CHECK_FLORIST_RESUMES;


PROCEDURE CHECK_FLORIST_SUSPENDS(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any suspends that should
        start on the current date.  If so, update the suspend record with null
        values and start the available zip process for each florist
        found.

        Only top level florists are checked for suspends based on the rule that
        if any parent/child is suspended then all florists in the parent/child
        tree are suspended.  The update_florist_suspend will traverse the tree
        to unsuspend all of the florists owned by the top level florist.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_today                 date := sysdate;
v_florist_id_tab        pg_florist_id_tab_typ;
v_message_id            number;




CURSOR check_cur IS
        SELECT  b.florist_id
        FROM    florist_suspends b
        left outer join frp.global_parms gp
        on gp.context = 'UPDATE_FLORIST_SUSPEND'
        and gp.name = 'BUFFER_MINUTES'
         WHERE ( (b.suspend_start_date-(b.timezone_offset_in_minutes/1440) - (gp.value/1440)) <= SYSDATE
         AND b.suspend_processed_flag = 'N')
         AND     EXISTS
        (       select  1
                from    florist_master m
                where   m.top_level_florist_id = b.florist_id
                and     m.status <> 'Inactive'
                and     rownum = 1
        );

BEGIN


out_status := 'Y';
out_message := '';



OPEN check_cur;
FETCH check_cur BULK COLLECT into v_florist_id_tab;
CLOSE check_cur;

FOR x in 1..v_florist_id_tab.count LOOP
        FLORIST_MAINT_PKG.FLORIST_SUSPEND_RESUME
        (
                IN_FLORIST_ID => v_florist_id_tab (x),
                IN_SUSPEND_TYPE =>'M',
                IN_SUSPEND_INDICATOR=>'Y',
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );



        IF out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_SUSPENDS',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'Florist ID::' || v_florist_id_tab (x) || ' -- ' || out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => out_status,
                        OUT_MESSAGE => out_message
                );
        END IF;
END LOOP;


CHECK_FLORIST_RESUMES(out_status, out_message);

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_SUSPENDS',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );

END CHECK_FLORIST_SUSPENDS;


PROCEDURE CHECK_FLORIST_CODIFICATIONS
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any blocks expire
        on the current date.  If so, update the block record with null
        values and start the available zip process for each florist
        found.

        Only top level florists are checked for blocks based on the rule that
        if any parent/child is blocked then all florists in the parent/child
        tree are blocked.  The update_florist_blocks will traverse the tree
        to unblock all of the florists owned by the top level florist.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

TYPE codification_id_tab_typ IS TABLE OF FLORIST_CODIFICATIONS.CODIFICATION_ID%TYPE INDEX BY PLS_INTEGER;

v_today                 date := trunc(sysdate);
v_florist_id_tab        pg_florist_id_tab_typ;
v_codication_id_tab     codification_id_tab_typ;
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

CURSOR check_cur IS
        SELECT DISTINCT  m.TOP_LEVEL_FLORIST_ID,
                c.codification_id
        FROM    ftd_apps.florist_codifications c,  ftd_apps.florist_master m
        WHERE   trunc (c.block_end_date) < v_today
        AND m.FLORIST_ID=c.florist_id
        AND m.status <> 'Inactive';

BEGIN

OPEN check_cur;
FETCH check_cur BULK COLLECT into v_florist_id_tab, v_codication_id_tab;
CLOSE check_cur;

FOR x in 1..v_florist_id_tab.count LOOP
        FLORIST_MAINT_PKG.UPDATE_FLORIST_CODIFICATIONS
        (
                IN_FLORIST_ID => v_florist_id_tab (x),
                IN_CODIFICATION_ID => v_codication_id_tab (x),
                IN_BLOCK_INDICATOR => 'N',
                IN_BLOCK_END_DATE => null,
                IN_CSR_ID => user,
                IN_ADDITIONAL_COMMENT => null,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );

        IF v_out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_CODIFICATIONS',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'Florist ID::' || v_florist_id_tab (x) || ' -- ' || v_out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );
        END IF;
END LOOP;

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_CODIFICATIONS',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );

END CHECK_FLORIST_CODIFICATIONS;


PROCEDURE CHECK_FLORIST_ZIPS
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any blocks expire
        on the current date.  If so, update the block record with null
        values and start the available zip process for each zip code
        found.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

TYPE zip_code_tab_typ IS TABLE OF FLORIST_ZIPS.ZIP_CODE%TYPE INDEX BY PLS_INTEGER;
TYPE florist_tab_typ IS TABLE OF FLORIST_ZIPS.FLORIST_ID%TYPE INDEX BY PLS_INTEGER;

v_today                 date := trunc(sysdate);
v_zip_code_tab          zip_code_tab_typ;
v_florist_tab           florist_tab_typ;
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

CURSOR check_cur IS
        SELECT  DISTINCT
                z.zip_code
        FROM    florist_zips z
        WHERE   trunc (z.block_end_date) < v_today
        AND     EXISTS
        (       select  1
                from    florist_master m
                where   m.florist_id = z.florist_id
                and     m.status <> 'Inactive'
        );

BEGIN

OPEN check_cur;
FETCH check_cur BULK COLLECT into v_zip_code_tab;
CLOSE check_cur;

FOR x in 1..v_zip_code_tab.count LOOP
        UPDATE  florist_zips z
        SET
                z.last_update_date = sysdate,
                z.block_start_date = null,
                z.block_end_date = null,
		z.blocked_by_user_id = null
        WHERE   z.zip_code = v_zip_code_tab(x)
        AND     trunc (z.block_end_date) < v_today
        AND     EXISTS
        (       select  1
                from    florist_master m
                where   m.florist_id = z.florist_id
                and     m.status <> 'Inactive'
        ) RETURNING florist_id BULK COLLECT INTO v_florist_tab;

        FOR y IN 1..v_florist_tab.count LOOP
                -- insert the unblock comment for all affected florists
                FLORIST_MAINT_PKG.INSERT_FLORIST_COMMENTS
                (
                        IN_FLORIST_ID => v_florist_tab (y),
                        IN_COMMENT_TYPE => 'Zip Codes',
                        IN_FLORIST_COMMENT => 'Zip Code ' || v_zip_code_tab(x) || ' was changed from Blocked to Active',
                        IN_MANAGER_ONLY_FLAG => 'N',
                        IN_CREATED_BY => user,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );

                IF v_out_status = 'N' THEN
                        exit;
                END IF;
        END LOOP;

        IF v_out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_ZIPS',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'ZIP CODE::' || v_zip_code_tab (x) || ' -- ' || v_out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );
        END IF;
END LOOP;

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FLORIST_ZIPS',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );


END CHECK_FLORIST_ZIPS;



PROCEDURE CHECK_BLOCKS
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if any blocks expire
        on the current date by calling the respective procedures.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_out_status            varchar2(1);
v_out_message           varchar2(256);


BEGIN



CHECK_FLORIST_BLOCKS;
CHECK_FLORIST_SUSPENDS(v_out_status, v_out_message);  /* This also calls CHECK RESUMES */
CHECK_FLORIST_CODIFICATIONS;
CHECK_FLORIST_ZIPS;
CHECK_FLORIST_CITY_BLOCKS;

END CHECK_BLOCKS;


PROCEDURE CHECK_FTDM_SHUTDOWN (IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for checking if the manual florist
        shutdown is expired.

Input:
        in_last_updated_by 				 csr_id

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_today                 date := trunc(sysdate);
v_date                  date;
v_message_id            number;
v_out_status            varchar2(1);
v_out_message           varchar2(256);

CURSOR check_cur IS
        SELECT  restart_date
        FROM    ftdm_shutdown
        WHERE   trunc (restart_date) <= v_today;

BEGIN

OPEN check_cur;
FETCH check_cur into v_date;
CLOSE check_cur;

IF v_date IS NOT NULL THEN
        FLORIST_MAINT_PKG.RESTART_FTDM
        (
        			 IN_LAST_UPDATED_BY => in_last_updated_by,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );

        IF v_out_status = 'Y' THEN
                commit;
        ELSE
                rollback;

                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FTDM_SHUTDOWN',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => v_out_message,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => v_out_status,
                        OUT_MESSAGE => v_out_message
                );
        END IF;
END IF;

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'FLORIST_RESUME_JOB_PKG.CHECK_FTDM_SHUTDOWN',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );


END CHECK_FTDM_SHUTDOWN;


END FLORIST_RESUME_JOB_PKG;

/
