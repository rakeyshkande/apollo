CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_ROLE  ( roleID          IN VARCHAR2,
   description     IN VARCHAR2,
   updateUser      IN VARCHAR2,
   lastUpdatedDate IN DATE
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_ROLE
-- Type:    Procedure
-- Syntax:  SP_UPDATE_ROLE ( <see args above> )
--
-- Description:   Performs an UPDATE/INSERT attempt on ROLE.
--
--==============================================================================

    tmp_date    DATE;

    -- Cursor handles row locking for updating of ROLE table
    CURSOR role_lock_cur (p_role_id  IN role.role_id%TYPE) IS
        SELECT * FROM role
        WHERE ROLE_ID = p_role_id
        FOR UPDATE;
    role_update_row    role_lock_cur%ROWTYPE;

begin

    -- Attempt to insert into ROLE
    INSERT INTO ROLE
      ( ROLE_ID,
        DESCRIPTION,
        CREATED_BY,
        CREATED_DATE,
        LAST_UPDATED_BY,
        LAST_UPDATED_DATE
      )
    VALUES
      ( roleID,
        description,
        updateUser,
        to_date(to_char(SYSDATE,'MMDDYYYYHH24MISS'),'MMDDYYYYHH24MISS'),
        updateUser,
        to_date(to_char(SYSDATE,'MMDDYYYYHH24MISS'),'MMDDYYYYHH24MISS')
      );

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

    -- Lock the existing row on ROLE for update
    OPEN role_lock_cur(roleID);
    FETCH role_lock_cur INTO role_update_row;

    -- Error if the last update date on existing row differs from the input parameter and
	  -- and the row was not previously deleted
    IF (role_update_row.last_updated_date <> lastUpdatedDate)
    THEN
        CLOSE role_lock_cur;
        COMMIT;
        RAISE_APPLICATION_ERROR(-20001, 'Role update for ID ' || roleID || ' failed due to last_update date mismatch. - TABLE: ' || to_char(role_update_row.last_updated_date,'MMDDYYYYHH24MISS') || ' PARM: ' || to_char(lastUpdatedDate,'MMDDYYYYHH24MISS') || '*');
    END IF;

    UPDATE ROLE
    SET DESCRIPTION = description,
        LAST_UPDATED_BY = updateUser,
        LAST_UPDATED_DATE = to_date(to_char(SYSDATE,'MMDDYYYYHH24MISS'),'MMDDYYYYHH24MISS')
    WHERE CURRENT OF role_lock_cur;

    -- Release the row lock on ROLE with a commit
    CLOSE role_lock_cur;
    COMMIT;

end
;
.
/
