CREATE OR REPLACE FUNCTION FTD_APPS.SP_GET_VENDOR_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_VENDOR_LIST
-- Type:    Function
-- Syntax:  SP_GET_VENDOR_LIST ()
-- Returns: ref_cursor for
--          VENDOR_ID   VARCHAR2(5)
--          VENDOR_NAME VARCHAR2(40)
--
--
-- Description:   Queries the VENDOR_MASTER table and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
BEGIN
    OPEN vendor_cursor FOR
        SELECT VENDOR_ID    as "vendorId",
               VENDOR_NAME  as "vendorName",
               VENDOR_TYPE  as "vendorType"
          FROM VENDOR_MASTER
          WHERE ACTIVE = 'Y'
          ORDER BY VENDOR_NAME;

    RETURN vendor_cursor;
END
;
.
/
