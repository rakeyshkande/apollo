CREATE OR REPLACE
PROCEDURE ftd_apps.OE_UPDATE_CERTIFICATE ( ftdGuid in varchar2,
 firstName in varchar2,
 lastName in varchar2,
 callCenterId in varchar2,
 roleId in varchar2,
 userId in varchar2
)
as

begin

  -- Attempt to insert into FLORIST_MASTER table
  INSERT INTO SECURITY_CERT
    ( FTD_GUID,
      FIRST_NAME,
      LAST_NAME,
      CALL_CENTER_ID,
      ROLE_ID,
      USER_ID,
      LAST_UPDATE_DATE
    )
  VALUES
    ( ftdGuid,
      firstName,
      lastName,
      callCenterId,
      roleId,
      userId,
      SYSDATE
    );

  COMMIT;

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

  -- If a row already exists update the row
   UPDATE SECURITY_CERT
    SET FTD_GUID = ftdGuid,
        LAST_UPDATE_DATE = SYSDATE
    WHERE USER_ID = userId;

  COMMIT;
end;
.
/
