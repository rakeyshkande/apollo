CREATE OR REPLACE
PROCEDURE ftd_apps.OE_TIM AS
 cursor center_counts is
   select count(*) cnt, cc.DESCRIPTION call_center
     from user_profile up, security_cert sc, call_center cc
     where sc.user_id = up.user_id
       and sc.last_update_date > (sysdate - 1/48)
       and cc.call_center_id = up.call_center_id
     group by cc.description;
 cursor center_counts_five is
   select count(*) cnt, cc.DESCRIPTION call_center
     from user_profile up, security_cert sc, call_center cc
     where sc.user_id = up.user_id
       and sc.last_update_date > (sysdate - 1/288)
       and cc.call_center_id = up.call_center_id
     group by cc.description;
 cursor order_counts is
   SELECT cc.DESCRIPTION call_center, COUNT(*) Orders
     FROM SESSION_ORDER o, USER_PROFILE up, CALL_CENTER cc
     WHERE o.transaction_date > SYSDATE - 1
     AND o.order_status = 'HP_SUCCESS'
     AND o.csr_user_name = up.user_id
     AND cc.CALL_CENTER_ID = up.CALL_CENTER_ID
     GROUP BY cc.DESCRIPTION;
 cursor order_counts_hour is
   SELECT cc.DESCRIPTION call_center, COUNT(*) Orders
     FROM SESSION_ORDER o, USER_PROFILE up, CALL_CENTER cc
     WHERE o.transaction_date > SYSDATE - 1/24
     AND o.order_status = 'HP_SUCCESS'
     AND o.csr_user_name = up.user_id
     AND cc.CALL_CENTER_ID = up.CALL_CENTER_ID
     GROUP BY cc.DESCRIPTION;
 v_row_count NUMBER := 0;
 v_call_center varchar2(20);
 BEGIN
  dbms_output.enable(30000);
  dbms_output.put_line('-');
  select count(*) into v_row_count from security_cert
    where last_update_date > (sysdate - 1/48);
  dbms_output.put_line ('Current users (30 minutes) = ' || v_row_count);
  for v_row in center_counts loop
    v_call_center := v_row.call_center;
    v_row_count := v_row.cnt;
    dbms_output.put_line (substr(v_call_center || '             ',1,13) || ' - ' || v_row_count);
  end loop;
  dbms_output.put_line('-');
  select count(*) into v_row_count from security_cert
    where last_update_date > (sysdate - 1/288);
  dbms_output.put_line ('Current users (5 minutes) = ' || v_row_count);
  for v_row in center_counts_five loop
    v_call_center := v_row.call_center;
    v_row_count := v_row.cnt;
    dbms_output.put_line (substr(v_call_center || '             ',1,13) || ' - ' || v_row_count);
  end loop;
  dbms_output.put_line('-');
  select count(*) into v_row_count from session_order
    where transaction_date > (sysdate - 1) and order_status = 'HP_SUCCESS';
  dbms_output.put_line ('Orders in last 24 hours = ' || v_row_count);
  for v_row in order_counts loop
    v_call_center := v_row.call_center;
    v_row_count := v_row.orders;
    dbms_output.put_line (substr(v_call_center || '             ',1,13) || ' - ' || v_row_count);
  end loop;
  dbms_output.put_line('-');
  select count(*) into v_row_count from session_order
    where transaction_date > (sysdate - 1/24) and order_status = 'HP_SUCCESS';
  dbms_output.put_line ('Orders in last hour = ' || v_row_count);
  for v_row in order_counts_hour loop
    v_call_center := v_row.call_center;
    v_row_count := v_row.orders;
    dbms_output.put_line (substr(v_call_center || '             ',1,13) || ' - ' || v_row_count);
  end loop;
END
;
.
/
