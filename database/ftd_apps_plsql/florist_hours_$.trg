CREATE OR REPLACE TRIGGER ftd_apps.florist_hours_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_hours
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_hours$ (
         FLORIST_ID, 
         DAY_OF_WEEK, 
         OPEN_CLOSE_CODE, 
         OPEN_TIME, 
         CLOSE_TIME, 
         created_on, created_by, updated_on, updated_by,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.DAY_OF_WEEK,
         :new.OPEN_CLOSE_CODE,
         :new.OPEN_TIME,
         :new.CLOSE_TIME,
         :new.created_on, :new.created_by, :new.updated_on, :new.updated_by,
         'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_hours$ (
         FLORIST_ID, 
         DAY_OF_WEEK, 
         OPEN_CLOSE_CODE, 
         OPEN_TIME, 
         CLOSE_TIME, 
         created_on, created_by, updated_on, updated_by,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.DAY_OF_WEEK,
         :old.OPEN_CLOSE_CODE,
         :old.OPEN_TIME,
         :old.CLOSE_TIME,
         :old.created_on, :old.created_by, :old.updated_on, :old.updated_by,
         'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.florist_hours$ (
         FLORIST_ID, 
         DAY_OF_WEEK, 
         OPEN_CLOSE_CODE, 
         OPEN_TIME, 
         CLOSE_TIME, 
         created_on, created_by, updated_on, updated_by,
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.DAY_OF_WEEK,
         :new.OPEN_CLOSE_CODE,
         :new.OPEN_TIME,
         :new.CLOSE_TIME,
         :new.created_on, :new.created_by, :new.updated_on, :new.updated_by,
         'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_hours$ (
         FLORIST_ID, 
         DAY_OF_WEEK, 
         OPEN_CLOSE_CODE, 
         OPEN_TIME, 
         CLOSE_TIME, 
         created_on, created_by, updated_on, updated_by,
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.DAY_OF_WEEK,
         :old.OPEN_CLOSE_CODE,
         :old.OPEN_TIME,
         :old.CLOSE_TIME,
         :old.created_on, :old.created_by, :old.updated_on, :old.updated_by,
         'DEL',SYSDATE);

   END IF;

END;
/
