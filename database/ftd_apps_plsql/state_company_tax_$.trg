create or replace trigger ftd_apps.state_company_tax_$
after insert or update or delete on ftd_apps.state_company_tax
referencing old as old new as new
for each row
begin

   if inserting then

      insert into ftd_apps.state_company_tax$ (
         STATE_MASTER_ID,
         COMPANY_ID,
         TAX1_NAME,
         TAX1_RATE,
         TAX2_NAME,
         TAX2_RATE,
         TAX3_NAME,
         TAX3_RATE,
         TAX4_NAME,
         TAX4_RATE,
         TAX5_NAME,
         TAX5_RATE,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) values (
         :NEW.STATE_MASTER_ID,
         :NEW.COMPANY_ID,
         :NEW.TAX1_NAME,
         :NEW.TAX1_RATE,
         :NEW.TAX2_NAME,
         :NEW.TAX2_RATE,
         :NEW.TAX3_NAME,
         :NEW.TAX3_RATE,
         :NEW.TAX4_NAME,
         :NEW.TAX4_RATE,
         :NEW.TAX5_NAME,
         :NEW.TAX5_RATE,
         :NEW.CREATED_ON,
         :NEW.CREATED_BY,
         :NEW.UPDATED_ON,
         :NEW.UPDATED_BY,
         'INS',systimestamp);

   elsif updating then

      insert into ftd_apps.state_company_tax$ (
         STATE_MASTER_ID,
         COMPANY_ID,
         TAX1_NAME,
         TAX1_RATE,
         TAX2_NAME,
         TAX2_RATE,
         TAX3_NAME,
         TAX3_RATE,
         TAX4_NAME,
         TAX4_RATE,
         TAX5_NAME,
         TAX5_RATE,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) values (
         :OLD.STATE_MASTER_ID,
         :OLD.COMPANY_ID,
         :OLD.TAX1_NAME,
         :OLD.TAX1_RATE,
         :OLD.TAX2_NAME,
         :OLD.TAX2_RATE,
         :OLD.TAX3_NAME,
         :OLD.TAX3_RATE,
         :OLD.TAX4_NAME,
         :OLD.TAX4_RATE,
         :OLD.TAX5_NAME,
         :OLD.TAX5_RATE,
         :OLD.CREATED_ON,
         :OLD.CREATED_BY,
         :OLD.UPDATED_ON,
         :OLD.UPDATED_BY,
         'UPD_OLD',systimestamp);

      insert into ftd_apps.state_company_tax$ (
         STATE_MASTER_ID,
         COMPANY_ID,
         TAX1_NAME,
         TAX1_RATE,
         TAX2_NAME,
         TAX2_RATE,
         TAX3_NAME,
         TAX3_RATE,
         TAX4_NAME,
         TAX4_RATE,
         TAX5_NAME,
         TAX5_RATE,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) values (
         :NEW.STATE_MASTER_ID,
         :NEW.COMPANY_ID,
         :NEW.TAX1_NAME,
         :NEW.TAX1_RATE,
         :NEW.TAX2_NAME,
         :NEW.TAX2_RATE,
         :NEW.TAX3_NAME,
         :NEW.TAX3_RATE,
         :NEW.TAX4_NAME,
         :NEW.TAX4_RATE,
         :NEW.TAX5_NAME,
         :NEW.TAX5_RATE,
         :NEW.CREATED_ON,
         :NEW.CREATED_BY,
         :NEW.UPDATED_ON,
         :NEW.UPDATED_BY,
         'UPD_NEW',systimestamp);

   elsif deleting then

      insert into ftd_apps.state_company_tax$ (
         STATE_MASTER_ID,
         COMPANY_ID,
         TAX1_NAME,
         TAX1_RATE,
         TAX2_NAME,
         TAX2_RATE,
         TAX3_NAME,
         TAX3_RATE,
         TAX4_NAME,
         TAX4_RATE,
         TAX5_NAME,
         TAX5_RATE,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$
      ) values (
         :OLD.STATE_MASTER_ID,
         :OLD.COMPANY_ID,
         :OLD.TAX1_NAME,
         :OLD.TAX1_RATE,
         :OLD.TAX2_NAME,
         :OLD.TAX2_RATE,
         :OLD.TAX3_NAME,
         :OLD.TAX3_RATE,
         :OLD.TAX4_NAME,
         :OLD.TAX4_RATE,
         :OLD.TAX5_NAME,
         :OLD.TAX5_RATE,
         :OLD.CREATED_ON,
         :OLD.CREATED_BY,
         :OLD.UPDATED_ON,
         :OLD.UPDATED_BY,
         'DEL',systimestamp);

   end if;

end;
/
