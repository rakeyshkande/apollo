CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_KEYWORDS_ANF (
 productId in varchar2,
 productKeyword in varchar2
)
authid current_user
as
--==============================================================================
--
-- Name:    SP_UPDATE_KEYWORDS_ANF
-- Type:    Procedure
-- Syntax:  SP_UPDATE_KEYWORDS_ANF ( productId in varchar2,
--                               productKeyword in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_KEYWORDS.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_KEYWORDS
  INSERT INTO PRODUCT_KEYWORDS
      ( PRODUCT_ID,
				KEYWORD
		  )
  VALUES
		  ( productId,
				productKeyword
		  );

end;
.
/
