create or replace procedure ftd_apps.SP_INSERT_SHIPPING_KEY(
  IN_DESCRIPTION IN VARCHAR2,
  IN_SHIPPER IN VARCHAR2,
  IN_CREATED_BY IN VARCHAR2,
  IN_UPDATED_BY IN VARCHAR2,
  OUT_ID OUT VARCHAR2
) AS

/**********************************************************
--
-- Name:    SP_INSERT_SHIPPING_KEY
-- Type:    Procedure
-- Syntax:  SP_INSERT_SHIPPING_KEY ( DESCRIPTION in varchar2, 
--                                   SHIPPER in VARCHAR2, 
--                                   CREATED_BY VARCHAR2,
--                                   UPDATED_BY VARCHAR2,
--                                   ID VARCHAR2 )
--
-- Description:   INSERTS a row into SHIPPING_KEYS and
--                returns the new id.
--
***********************************************************/
begin
  SELECT MAX(TO_NUMBER(SHIPPING_KEY_ID))+1 INTO OUT_ID FROM FTD_APPS.SHIPPING_KEYS;
  INSERT INTO FTD_APPS.SHIPPING_KEYS (SHIPPING_KEY_ID, SHIPPING_KEY_DESCRIPTION, SHIPPER, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
    VALUES(TO_CHAR(OUT_ID),IN_DESCRIPTION,IN_SHIPPER,IN_CREATED_BY,SYSDATE,IN_UPDATED_BY, SYSDATE );
end SP_INSERT_SHIPPING_KEY;
.
/
