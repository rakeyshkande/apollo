CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_FUNCTIONS_LIST RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    SP_GET_FUNCTIONS_LIST
-- Type:    Function
-- Syntax:  SP_GET_FUNCTIONS_LIST( )
-- Returns: ref_cursor for
--          functionId         VARCHAR2(20)
--          description        VARCHAR2(50)
--          createdBy          VARCHAR2(10)
--          createdDate        DATE
--          lastUpdateUser     VARCHAR2(10)
--          lastUpdateDate     DATE
-- Description:   Queries the functions table and returns a ref cursor for row.
--
--===========================================================

AS
   function_cursor types.ref_cursor;
BEGIN
    OPEN function_cursor FOR
        SELECT FUNCTION_ID          as "functionId",
               FUNCTION_DESCRIPTION as "description",
               CREATED_BY           as "createdBy",
               CREATED_DATE         as "createdDate",
               LAST_UPDATE_USER     as "lastUpdateUser",
               LAST_UPDATE_DATE     as "lastUpdateDate"
          FROM functions;

    RETURN function_cursor;
END
;
.
/
