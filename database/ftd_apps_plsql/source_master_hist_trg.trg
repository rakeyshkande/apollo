-- FTD_APPS.SOURCE_MASTER_HIST_TRG trigger to populate FTD_APPS.SOURCE_MASTER_HIST history table

CREATE OR REPLACE TRIGGER FTD_APPS.SOURCE_MASTER_HIST_TRG
AFTER INSERT OR UPDATE of snh_id OR DELETE ON ftd_apps.source_master REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW

BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.source_master_hist (
        source_master_hist_id,
        source_code,
      	snh_id,
      	updated_on,      	
        expired_on,
	delivery_fee_id
      ) VALUES (
        source_master_hist_sq.nextval,
        :NEW.source_code,
      	:NEW.snh_id,
        :NEW.updated_on,      	
        null,
	:NEW.delivery_fee_id
         );

   ELSIF UPDATING  THEN

      update ftd_apps.source_master_hist
      set    expired_on = sysdate
      where  source_code = :new.source_code
      and    expired_on is null;

      INSERT INTO ftd_apps.source_master_hist (
        source_master_hist_id,
        source_code,
      	snh_id,
      	updated_on,      	
        expired_on,
	delivery_fee_id
      ) VALUES (
        source_master_hist_sq.nextval,
        :NEW.source_code,
      	:NEW.snh_id,
        :NEW.updated_on,      	
        null,
	:NEW.delivery_fee_id
         );

   ELSIF DELETING  THEN

      update ftd_apps.source_master_hist
      set    expired_on = sysdate
      where  source_code = :old.source_code
      and    expired_on is null;

   END IF;

END;
/
