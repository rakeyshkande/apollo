CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_AAA_MEMBER
    (aaaMemberId  IN NUMBER)
 RETURN types.ref_cursor
--=================================================================
--
-- Name:    OE_GET_AAA_MEMBER
-- Type:    Function
-- Syntax:  OE_GET_AAA_MEMBER (aaaMemberId  IN NUMBER)
-- Returns: ref_cursor for
--          aaaMemberId       NUMBER
--          memberName        VARCHAR2(50)
--
-- Description:   Used to validate AAA membership IDs.
--
--==================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT AAA_MEMBER_ID as "aaaMemberId",
            MEMBER_NAME as "memberName"
        FROM AAA_MEMBERS
        WHERE AAA_MEMBER_ID = aaaMemberId;

    RETURN cur_cursor;
END
;
.
/
