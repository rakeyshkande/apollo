CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_COUNTRY_LIST_2247 (dnisType IN VARCHAR2)
  RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    OE_GET_COUNTRY_LIST_2247
-- Type:    Function
-- Syntax:  OE_GET_COUNTRY_LIST_2247 ( )
-- Returns: ref_cursor for
--          COUNTRY_ID        VARCHAR2(2)
--          NAME              VAECHAR2(50)
--          OE_COUNTRY_TYPE   VARCHAR2(1)
--          OE_DISPLAY_ORDER  NUMBER(3)
--
-- Description:   Queries the COUNTRY_MASTER table and returns a ref cursor for row.
--
--===========================================================


AS
    country_cursor types.ref_cursor;
BEGIN

    OPEN country_cursor FOR
	SELECT  DISTINCT
		cm.country_id as "countryId",
		cm.name as "name",
		cm.oe_country_type as "oeCountryType",
		cm.oe_display_order as "oeDisplayOrder",
		cm.oe_country_type
	FROM
		scripting_country_xref scx,
		COUNTRY_MASTER cm
	WHERE
		cm.country_id not in ('00', '01')
		AND cm.country_id = scx.country_id (+)
		AND (scx.scripting_code = dnisType OR
			(NOT EXISTS (SELECT * FROM scripting_country_xref WHERE scripting_code = dnisType)))

	ORDER BY
		oe_display_order, name;

    RETURN country_cursor;
END;
.
/
