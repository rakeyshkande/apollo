-- FTD_APPS.SHIPPING_KEY_DETAILS_$ trigger to populate FTD_APPS.SHIPPING_KEY_DETAILS$ shadow table

CREATE OR REPLACE TRIGGER FTD_APPS.shipping_key_details_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.shipping_key_details REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.shipping_key_details$ (
      	shipping_detail_id,
      	shipping_key_id,
      	min_price,
      	max_price,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.shipping_detail_id,
      	:NEW.shipping_key_id,
      	:NEW.min_price,
      	:NEW.max_price,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.shipping_key_details$ (
      	shipping_detail_id,
      	shipping_key_id,
      	min_price,
      	max_price,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.shipping_detail_id,
      	:OLD.shipping_key_id,
      	:OLD.min_price,
      	:OLD.max_price,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO ftd_apps.shipping_key_details$ (
      	shipping_detail_id,
      	shipping_key_id,
      	min_price,
      	max_price,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.shipping_detail_id,
      	:NEW.shipping_key_id,
      	:NEW.min_price,
      	:NEW.max_price,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.shipping_key_details$ (
      	shipping_detail_id,
      	shipping_key_id,
      	min_price,
      	max_price,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.shipping_detail_id,
      	:OLD.shipping_key_id,
      	:OLD.min_price,
      	:OLD.max_price,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
