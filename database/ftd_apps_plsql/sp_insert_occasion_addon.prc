CREATE OR REPLACE
PROCEDURE ftd_apps.SP_INSERT_OCCASION_ADDON (
  occasionIn in number,
	addonIn in varchar2
)
as
--==============================================================================
--
-- Name:    SP_INSERT_INSTITUTION
-- Type:    Procedure
-- Syntax:  SP_INSERT_INSTITUTION ( occasionIn in number,
--                                    addonIn in number )
--
-- Description:   Inserts a row to the BUSINESS table.
--
--==============================================================================
begin

 insert into OCCASION_ADDON
 (OCCASION_ID,ADDON_ID)
 VALUES(occasionIn,addonIn);

end
;
.
/
