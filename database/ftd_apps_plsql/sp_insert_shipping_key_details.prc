create or replace procedure ftd_apps.SP_INSERT_SHIPPING_KEY_DETAILS(
  IN_SHIPPING_KEY IN NUMBER,
  IN_MIN_PRICE IN NUMBER,
  IN_MAX_PRICE IN NUMBER,
  IN_CREATED_BY IN VARCHAR2,
  IN_UPDATED_BY IN VARCHAR2,
  OUT_ID OUT VARCHAR2
) AS

/**********************************************************
--
-- Name:    SP_INSERT_SHIPPING_KEY_DETAILS
-- Type:    Procedure
-- Syntax:  SP_INSERT_SHIPPING_KEY_DETAILS ( 
--                                          SHIPPING_KEY_ID NUMBER, 
--                                          MIN_PRICE NUMBER, 
--                                          MAX_PRICE in NUMBER, 
--                                          CREATED_BY VARCHAR2,
--                                          UPDATED_BY VARCHAR2,
--                                          ID VARCHAR2 )
--
-- Description:   INSERTS a row into SHIPPING_KEY_DETAILS and
--                returns the new id.
--
***********************************************************/
begin
     SELECT MAX(TO_NUMBER(SHIPPING_DETAIL_ID))+1 INTO OUT_ID FROM FTD_APPS.SHIPPING_KEY_DETAILS;
     INSERT INTO FTD_APPS.SHIPPING_KEY_DETAILS (SHIPPING_DETAIL_ID, SHIPPING_KEY_ID, MIN_PRICE, MAX_PRICE, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON)
            VALUES(TO_CHAR(OUT_ID),TO_CHAR(IN_SHIPPING_KEY),IN_MIN_PRICE,IN_MAX_PRICE,IN_CREATED_BY,SYSDATE,IN_UPDATED_BY,SYSDATE);
end SP_INSERT_SHIPPING_KEY_DETAILS;
.
/
