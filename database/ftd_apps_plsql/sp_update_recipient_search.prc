CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_RECIPIENT_SEARCH (
 productId in varchar2,
 recipientId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_RECIPIENT_SEARCH
-- Type:    Procedure
-- Syntax:  SP_UPDATE_RECIPIENT_SEARCH ( <see args above> )
--
-- Description:   Performs an INSERT/UPDATE attempt on PRODUCT_RECIPIENT_SEARCH.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_KEYWORDS
  INSERT INTO PRODUCT_RECIPIENT_SEARCH
      ( PRODUCT_ID,
				RECIPIENT_SEARCH_ID
		  )
  VALUES
		  ( productId,
				recipientId
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN

    -- Update an existing row for this product id if found
    UPDATE PRODUCT_RECIPIENT_SEARCH
      SET RECIPIENT_SEARCH_ID = recipientId
      WHERE PRODUCT_ID = productId;
end
;
.
/
