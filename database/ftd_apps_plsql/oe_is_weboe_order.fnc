CREATE OR REPLACE
FUNCTION ftd_apps.OE_IS_WEBOE_ORDER 
  (IN_MASTER_ORDER_NUMBER IN FTD_APPS.SESSION_ORDER_MASTER.CONFIRMATION_NUMBER%TYPE)
  RETURN VARCHAR2
--====================================================================

/*-----------------------------------------------------------------------------
Description:
        This procedure will check if an order is in the webOE session_order_master table

Input:
        IN_MASTER_ORDER_NUMBER			 FTD_APPS.SESSION_ORDER_MASTER.CONFIRMATION_NUMBER%TYPE

Output:
        OUT_IS_WEBOE_ORDER                       varchar2 (Y or N)

-----------------------------------------------------------------------------*/
AS
OUT_IS_WEBOE_ORDER  VARCHAR2(1);
BEGIN
   SELECT 'Y'
   INTO OUT_IS_WEBOE_ORDER
   FROM FTD_APPS.SESSION_ORDER_MASTER
   WHERE CONFIRMATION_NUMBER = IN_MASTER_ORDER_NUMBER;
   
   RETURN OUT_IS_WEBOE_ORDER;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
     OUT_IS_WEBOE_ORDER := 'N';
     RETURN OUT_IS_WEBOE_ORDER;
END
;
.
/