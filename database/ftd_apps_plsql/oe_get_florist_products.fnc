CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_FLORIST_PRODUCTS (
 floristId in varchar2
)
return varchar2
as

productVal varchar2(32767);
cursor cur_cursor is
    select PRODUCT_ID from FLORIST_PRODUCTS
    where FLORIST_ID = floristId;
my_val cur_cursor%ROWTYPE;

begin

  for my_val in cur_cursor
    loop
--       productVal := floristId;
      productVal := CONCAT(productVal, my_val.PRODUCT_ID);
      productVal := CONCAT(productVal, ' ');
    end loop;

-- productVal := floristId;
 return productVal;
end
;
.
/
