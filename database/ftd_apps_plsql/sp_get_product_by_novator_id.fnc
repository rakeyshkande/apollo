CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_BY_NOVATOR_ID (novatorId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_BY_NOVATOR_ID
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_BY_NOVATOR_ID (novatorId )
-- Returns: ref_cursor for
--          PRODUCT_ID    VARCHAR2(10)
--
-- Description:   Queries the PRODUCT_MASTER table by NOVATOR_ID and returns a ref cursor for all rows.
--
--=========================================================


AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT product_id as "productId"
         FROM PRODUCT_MASTER
    WHERE NOVATOR_ID = novatorId;

    RETURN cur_cursor;
END
;
.
/
