CREATE OR REPLACE
PROCEDURE ftd_apps.SP_LOG_INVALID_LOGIN_ATTEMPT (
 userId in varchar2,
 messageId in number
)
as
--==============================================================================
--
-- Name:    SP_LOG_INVALID_LOGIN_ATTEMPT
-- Type:    Procedure
-- Syntax:  SP_LOG_INVALID_LOGIN_ATTEMPT( userId in varchar2,
--                                      messageId in number )
--
-- Description:   Logs an audit row for a user's invalid login
--                and increments the user's invalid login attempts on
--                their user profile.
--
--==============================================================================

v_loginAttempts NUMBER(10);

begin
  /* Log attempt */
  SP_LOG_AUDIT_MESSAGE(userId, messageId, 'Invalid Login Atmpt');


  /* Update the USER_PROFILE table */
  select USER_PROFILE.LOGIN_ATTEMPTS
    INTO v_loginAttempts
    FROM USER_PROFILE
    WHERE USER_PROFILE.USER_ID=userId;

  v_loginAttempts := v_loginAttempts + 1;

  update USER_PROFILE
    set LOGIN_ATTEMPTS=v_loginAttempts
    where USER_PROFILE.USER_ID=userId;

end
;
.
/
