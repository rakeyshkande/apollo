CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_BY_ID
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_BY_ID ()
-- Returns: ref_cursor for
--          PRODUCT_ID                  VARCHAR2(10)
--          NOVATOR_NAME                VARCHAR2(100)
--          STATUS                      VARCHAR2(1)
--          EXCEPTION_START_DATE        DATE(7)
--          EXCEPTION_END_DATE          DATE(7)
--          HOLD_UNTIL_AVAILABLE  	CHAR(1)
--
-- Description:   Queries the PRODUCT_MASTER table and returns a ref cursor
--                to the resulting row.
--
--=========================================================

AS
    product_list_cursor types.ref_cursor;
BEGIN
    OPEN product_list_cursor FOR
        SELECT PRODUCT_ID            as "productId",
               NOVATOR_NAME          as "novatorName",
               STATUS                as "status",
               EXCEPTION_START_DATE  as "exceptionStartDate",
               EXCEPTION_END_DATE    as "exceptionEndDate",
	       HOLD_UNTIL_AVAILABLE  as "holdUntilAvailable"
          FROM PRODUCT_MASTER
          ORDER BY STATUS, PRODUCT_ID;

    RETURN product_list_cursor;
END;
.
/
