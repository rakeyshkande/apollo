create or replace 
PROCEDURE                          FTD_APPS.SP_PDB_GET_MASS_EDIT_BY_SKU (
  inMasterSku1    IN VARCHAR2,
  inMasterSku2    IN VARCHAR2,
  inMasterSku3    IN VARCHAR2,
  inMasterSku4    IN VARCHAR2,
  inMasterSku5    IN VARCHAR2,
  refCursor     OUT TYPES.REF_CURSOR
)

--=============================================================================
--
-- Name:    SP_PDB_GET_MASS_EDIT_BY_SKU
--
-- Description: Queries the PRODUCT_MASTER table and returns a ref cursor
--              to the resulting rows.  The input parameters determine
--              which filtering criteria should be applied to the query.
--            
--
--=========================================================
AS
v_temp         VARCHAR(200);
v_sql          VARCHAR2(4000);
v_in_clause    VARCHAR2(1000);
v_in_count     NUMBER;

BEGIN

v_sql := 'SELECT U.UPSELL_MASTER_ID    	 as "MASTER_SKU",
               PRODUCT_ID            	 as "PRODUCT_ID",
               PRODUCT_NAME	     	 as "PRODUCT_NAME",
               NOVATOR_ID            	 as "NOVATOR_ID",
               NOVATOR_NAME            	 as "NOVATOR_NAME",
               FLORIST_REFERENCE_NUMBER  as "FLORIST_REFERENCE_NUMBER",
               CATEGORY			 as "CATEGORY",
               PRODUCT_TYPE	         as "PRODUCT_TYPE",
               PRODUCT_SUB_TYPE          as "PRODUCT_SUB_TYPE",
               PRODUCT_TYPE	         as "PRODUCT_TYPE",
               PRODUCT_SUB_TYPE          as "PRODUCT_SUB_TYPE",
               LONG_DESCRIPTION          as "LONG_DESCRIPTION",
               STATUS                    as "STATUS",
               DELIVERY_TYPE             as "DELIVERY_TYPE",
               SHIP_METHOD_FLORIST       as "SHIP_METHOD_FLORIST",
               SHIP_METHOD_CARRIER       as "SHIP_METHOD_CARRIER",
               STANDARD_RECIPE           as "STANDARD_RECIPE",
               DELUXE_RECIPE             as "DELUXE_RECIPE",
               PREMIUM_RECIPE            as "PREMIUM_RECIPE",
               DIM_WEIGHT                as "DIM_WEIGHT",
               STANDARD_PRICE            as "STANDARD_PRICE",
               DELUXE_PRICE              as "DELUXE_PRICE",
               PREMIUM_PRICE             as "PREMIUM_PRICE",
               SENT_TO_NOVATOR_PROD      as "SENT_TO_NOVATOR_PROD",
               GENERAL_COMMENTS          as "GENERAL_COMMENTS",
               MERCURY_DESCRIPTION       as "MERCURY_DESCRIPTION",
               PQUAD_PRODUCT_ID          as "PQUAD_PRODUCT_ID"
   FROM FTD_APPS.UPSELL_DETAIL U, FTD_APPS.PRODUCT_MASTER P
   WHERE U.UPSELL_DETAIL_ID = P.PRODUCT_ID 
   AND P.PRODUCT_TYPE IS NOT NULL 
   AND P.PRODUCT_TYPE !=  ''NONE''
   ';
    
   v_in_clause := ' AND LOWER(U.UPSELL_MASTER_ID) IN (';
   
   
   IF inMasterSku1 IS NOT NULL THEN
       v_temp := ' LOWER(''' || inMasterSku1 || ''')';
       v_in_clause := v_in_clause || v_temp;
   END IF;
   
   IF inMasterSku2 IS NOT NULL THEN
       v_temp := ' ,LOWER(''' || inMasterSku2 || ''')';
       v_in_clause := v_in_clause || v_temp;
   END IF; 
   
   IF inMasterSku3 IS NOT NULL THEN
          v_temp := ' ,LOWER(''' || inMasterSku3 || ''')';
          v_in_clause := v_in_clause || v_temp;
   END IF; 
   
   IF inMasterSku4 IS NOT NULL THEN
          v_temp := ' ,LOWER(''' || inMasterSku4 || ''')';
          v_in_clause := v_in_clause || v_temp;
   END IF;  
   
   IF inMasterSku5 IS NOT NULL THEN
          v_temp := ' ,LOWER(''' || inMasterSku5 || ''')';
          v_in_clause := v_in_clause || v_temp;
   END IF;  
   
   v_in_clause := v_in_clause || ') order by 1, 2';
   v_sql := v_sql || v_in_clause;
--   dbms_output.put_line('  sql: ' || v_sql);
   OPEN refCursor FOR v_sql;
END;
/