CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_CUSTOMER_TEST
    (customerId IN VARCHAR2,
 institutionName IN VARCHAR2,
 address1 IN VARCHAR2,
 address2 IN VARCHAR2,
 inCity IN VARCHAR2,
 inState IN VARCHAR2,
 inZipCode IN VARCHAR2,
 inPhone IN VARCHAR2)
  RETURN types.ref_cursor
--======================================================================
--
-- Name:    OE_GET_CUSTOMER
-- Type:    Function
-- Syntax:  OE_GET_CUSTOMER (customerId IN VARCHAR2)
-- Returns: ref_cursor for
--          customerId    VARCHAR2(8)
--          firstName     VARCHAR2(30)
--          lastName      VARCHAR2(30)
--          address1      VARCHAR2(30)
--          address2      VARCHAR2(30)
--          city          VARCHAR2(30)
--          state         VARCHAR2(2)
--          zipCode       VARCHAR2(10)
--          homePhone     VARCHAR2(20)
--          workPhone     VARCHAR2(20)
--          faxNumber     VARCHAR2(10)
--          email         VARCHAR2(40)
--          workPhoneExt  VARCHAR2(10)
--          countryId     VARCHAR2(2)
--          addressType   VARCHAR2(1)
--          bfhName       VARCHAR2(40)
--          bfhInfo       VARCHAR2(15)
--          county        VARCHAR2(30)
--
-- Description:   Queries the CUSTOMER table by customer ID.
--
--=======================================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT CUSTOMER_ID as "customerId",
              nls_initcap(FIRST_NAME) as "firstName",
              nls_initcap(LAST_NAME) as "lastName",
              nls_initcap(ADDRESS_1) as "address1",
              nls_initcap(ADDRESS_2) as "address2",
              nls_initcap(CITY) as "city",
              STATE as "state",
              ZIP_CODE as "zipCode",
              HOME_PHONE as "homePhone",
              WORK_PHONE as "workPhone",
              FAX_NUMBER as "faxNumber",
              EMAIL as "email",
              WORK_PHONE_EXT as "workPhoneExt",
              COUNTRY_ID as "countryId",
              ADDRESS_TYPE as "addressType",
              BFH_NAME as "bfhName",
              BFH_INFO as "bfhInfo",
              nls_initcap(COUNTY) as "county"
        FROM CUSTOMER
        WHERE (( customerId IS NULL )
          OR ( CUSTOMER_ID = customerId ))
        AND (( institutionName IS NULL )
          OR ( BFH_NAME LIKE UPPER(institutionName) || '%' ))
        AND (( address1 IS NULL )
          OR ( ADDRESS_1 LIKE UPPER(address1) || '%' ))
        AND (( address2 IS NULL )
          OR ( ADDRESS_2 LIKE UPPER(address2) || '%' ))
        AND (( inCity IS NULL )
          OR ( CITY LIKE UPPER(inCity) || '%' ))
        AND (( inState IS NULL )
          OR ( STATE = inState ))
        AND (( inZipCode IS NULL )
          OR ( ZIP_CODE LIKE OE_CLEANUP_ALPHANUM_STRING(inZipCode,'N') || '%' ))
        AND (( inPhone IS NULL )
          OR ( HOME_PHONE = OE_CLEANUP_ALPHANUM_STRING(inPhone,'Y') )
          OR ( WORK_PHONE = OE_CLEANUP_ALPHANUM_STRING(inPhone,'Y') ))
        ORDER BY CUSTOMER_ID;

    RETURN cur_cursor;
END
;
.
/
