CREATE OR REPLACE
TRIGGER ftd_apps.miles_points_redemption_rate_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.miles_points_redemption_rate
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN

      INSERT INTO ftd_apps.miles_points_redemption_rate$ (
         MP_REDEMPTION_RATE_ID,
         PAYMENT_METHOD_ID,
         DESCRIPTION,
         MP_REDEMPTION_RATE_AMT,
         ACTIVE_FLAG,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$,
         requested_by
      ) VALUES (
         :new.MP_REDEMPTION_RATE_ID,
         :new.PAYMENT_METHOD_ID,
         :new.DESCRIPTION,
         :new.MP_REDEMPTION_RATE_AMT,
         :new.ACTIVE_FLAG,
         :new.CREATED_ON,
         :new.CREATED_BY,
         :new.UPDATED_ON,
         :new.UPDATED_BY,
         'INS',v_current_timestamp,
         :new.requested_by);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.miles_points_redemption_rate$ (
         MP_REDEMPTION_RATE_ID,
         PAYMENT_METHOD_ID,
         DESCRIPTION,
         MP_REDEMPTION_RATE_AMT,
         ACTIVE_FLAG,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$,
         requested_by
      ) VALUES (
         :old.MP_REDEMPTION_RATE_ID,
         :old.PAYMENT_METHOD_ID,
         :old.DESCRIPTION,
         :old.MP_REDEMPTION_RATE_AMT,
         :old.ACTIVE_FLAG,
         :old.CREATED_ON,
         :old.CREATED_BY,
         :old.UPDATED_ON,
         :old.UPDATED_BY,
         'UPD_OLD',v_current_timestamp,
         :old.requested_by);

      INSERT INTO ftd_apps.miles_points_redemption_rate$ (
         MP_REDEMPTION_RATE_ID,
         PAYMENT_METHOD_ID,
         DESCRIPTION,
         MP_REDEMPTION_RATE_AMT,
         ACTIVE_FLAG,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$,
         requested_by
      ) VALUES (
         :new.MP_REDEMPTION_RATE_ID,
         :new.PAYMENT_METHOD_ID,
         :new.DESCRIPTION,
         :new.MP_REDEMPTION_RATE_AMT,
         :new.ACTIVE_FLAG,
         :new.CREATED_ON,
         :new.CREATED_BY,
         :new.UPDATED_ON,
         :new.UPDATED_BY,
         'UPD_NEW',v_current_timestamp,
         :new.requested_by);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.miles_points_redemption_rate$ (
         MP_REDEMPTION_RATE_ID,
         PAYMENT_METHOD_ID,
         DESCRIPTION,
         MP_REDEMPTION_RATE_AMT,
         ACTIVE_FLAG,
         CREATED_ON,
         CREATED_BY,
         UPDATED_ON,
         UPDATED_BY,
         operation$, timestamp$,
         requested_by
      ) VALUES (
         :old.MP_REDEMPTION_RATE_ID,
         :old.PAYMENT_METHOD_ID,
         :old.DESCRIPTION,
         :old.MP_REDEMPTION_RATE_AMT,
         :old.ACTIVE_FLAG,
         :old.CREATED_ON,
         :old.CREATED_BY,
         :old.UPDATED_ON,
         :old.UPDATED_BY,
         'DEL',v_current_timestamp,
         :old.requested_by);

   END IF;

END;
/
