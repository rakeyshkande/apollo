CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ROLE ( inRoleId  IN VARCHAR2 )
 RETURN types.ref_cursor
--=======================================================================
--
-- Name:    OE_GET_ROLE
-- Type:    Function
-- Syntax:  OE_GET_ROLE(inRoleId IN VARCHAR2)
-- Returns: ref_cursor for
--          roleId            VARCHAR2(10)
--          roleDescription   VARCHAR2(50)
--          functionId        VARCHAR2(20)
--          roleFunction      VARCHAR2(50)
--          updateUser        VARCHAR2(8)
--          updateDate        DATE
--
-- Description:   Queries the role and functions tables and returns
--                a ref cursor to all functions for the given row.
--
--========================================================================

AS
    role_cursor types.ref_cursor;
BEGIN
    OPEN role_cursor FOR
        SELECT r.ROLE_ID as "roleId",
               r.DESCRIPTION as "roleDescription",
               rf.FUNCTION_ID as "functionId",
               f.FUNCTION_DESCRIPTION as "roleFunction",
			   r.LAST_UPDATED_BY as "updateUser",
			   r.LAST_UPDATED_DATE as "updateDate"
        FROM ROLE r, ROLE_FUNCTIONS rf, FUNCTIONS f
        WHERE r.role_id = inRoleId
          AND rf.role_id = r.role_id
          AND f.function_id = rf.function_id;

    RETURN role_cursor;
END
;
.
/
