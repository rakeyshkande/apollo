CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_BY_ID_2247
    (productId IN varchar2,
    source IN varchar2 := 'NOT PDB')
    RETURN types.ref_cursor
    --==============================================================================
--
-- Name:    SP_GET_PRODUCT_BY_ID_2247
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_BY_ID_2247(productId IN varchar2)
-- Returns: ref_cursor for
--          PRODUCT_ID                  VARCHAR2(10)
--          NOVATOR_ID                  VARCHAR2(10)
--          PRODUCT_NAME                VARCHAR2(25)
--          NOVATOR_NAME                VARCHAR2(100)
--          STATUS                      VARCHAR2(1)
--          DELIVERY_TYPE               VARCHAR2(1)
--          CATEGORY                    VARCHAR2(5)
--          PRODUCT_TYPE                VARCHAR2(25)
--          PRODUCT_SUB_TYPE            VARCHAR2(25)
--          COLOR_SIZE_FLAG             VARCHAR2(1)
--          STANDARD_PRICE              NUMBER(8,2)
--          DELUXE_PRICE                NUMBER(8,2)
--          PREMIUM_PRICE               NUMBER(8,2)
--          PREFERRED_PRICE_POINT       NUMBER(2)
--          VARIABLE_PRICE_MAX          NUMBER(8,2)
--          SHORT_DESCRIPTION           VARCHAR2(80)
--          LONG_DESCRIPTION            VARCHAR2(540)
--          FLORIST_REFERENCE_NUMBER    VARCHAR2(62)
--          MERCURY_DESCRIPTION         VARCHAR2(62)
--          ITEM_COMMENTS               VARCHAR2(62)
--          ADD_ON_BALLOONS_FLAG        VARCHAR2(1)
--          ADD_ON_BEARS_FLAG           VARCHAR2(1)
--          ADD_ON_CARDS_FLAG           VARCHAR2(1)
--          ADD_ON_FUNERAL_FLAG         VARCHAR2(1)
--          CODIFIED_FLAG               VARCHAR2(5)
--          EXCEPTION_CODE              VARCHAR2(1)
--          EXCEPTION_START_DATE        DATE(7)
--          EXCEPTION_END_DATE          DATE(7)
--          EXCEPTION_MESSAGE           VARCHAR2(280)
--          VENDOR_ID                   VARCHAR2(5)
--          VENDOR_COST                 NUMBER(8,2)
--          VENDOR_SKU                  VARCHAR2(10)
--          SECOND_CHOICE_CODE          VARCHAR2(2)
--          HOLIDAY_SECOND_CHOICE_CODE  VARCHAR2(2)
--          DROPSHIP_CODE               VARCHAR2(2)
--          DISCOUNT_ALLOWED_FLAG       VARCHAR2(1)
--          DELIVERY_INCLUDED_FLAG      VARCHAR2(1)
--          TAX_FLAG                    VARCHAR2(1)
--          SERVICE_FEE_FLAG            VARCHAR2(1)
--          EXOTIC_FLAG                 VARCHAR2(1)
--          EGIFT_FLAG                  VARCHAR2(1)
--          COUNTRY_ID                  VARCHAR2(2)
--          ARRANGEMENT_SIZE            VARCHAR2(25)
--          ARRANGEMENT_COLORS          VARCHAR2(255)
--          DOMINANT_FLOWERS            VARCHAR2(255)
--          SEARCH_PRIORITY             VARCHAR2(5)
--          RECIPE                      VARCHAR2(255)
--          SUBCODE_FLAG                VARCHAR2(1)
--          DIM_WEIGHT                  VARCHAR2(10)
--          NEXT_DAY_UPGRADE_FLAG       VARCHAR2(1)
--          CORPORATE_SITE              VARCHAR2(1)
--          UNSPSC_CODE                 VARCHAR2(40)
--          PRICE_RANK_1                VARCHAR2(1)
--          PRICE_RANK_2                VARCHAR2(1)
--          PRICE_RANK_3                VARCHAR2(1)
--          SHIP_METHOD_CARRIER         VARCHAR2(1)
--          SHIP_METHOD_FLORIST         VARCHAR2(1)
--          SHIPPING_KEY                VARCHAR2(3)
--          LAST_UPDATE                 DATE(7)
--          VARIABLE_PRICE_FLAG         VARCHAR2(1)
--          HOLIDAY_SKU                 VARCHAR2(6)
--          HOLIDAY_PRICE               NUMBER(8,2)
--          CATALOG_FLAG                VARCHAR2(1)
--          HOLIDAY_DELUXE_PRICE        NUMBER(8,2)
--          HOLIDAY_PREMIUM_PRICE       NUMBER(8,2)
--          HOLIDAY_START_DATE          DATE(7)
--          HOLIDAY_END_DATE            DATE(7)
--          HOLD_UNTIL_AVAILABLE        CHAR(1)
--          HOLD_UNTIL_AVAILABLE        CHAR(1)
--          last_update_user_id         VARCHAR2(8)
--          last_update_system          VARCHAR2(40)
--          mondayDeliveryFreshcut      VARCHAR2(1)
--          twoDayShipSatFreshcut       VARCHAR2(1)
--          weboeBlocked                VARCHAR2(1)
--          expressShippingOnly         VARCHAR2(1)
--          over21                      VARCHAR2(1)
--          shippingSystem              VARCHAR2(10)
--          personalization_template_id VARCHAR2(50)
--          personalization_lead_days   NUMBER
--          department_code             VARCHAR2(4)
--          personalization_template_order VARCHAR2(50)
--          ZONE_JUMP_ELIGIBLE_FLAG     CHAR(1)
--          BOX_ID                      NUMBER(11)
--          custom_flag                 VARCHAR2(1)
--          keywords_txt                VARCHAR2(4000)
--          supplies_expense_dollar_amt NUMBER(8,2)
--          supplies_expense_eff_date   DATE
--          royalty_percent             NUMBER(8,2)
--          royalty_percent_eff_date    DATE
--          PERSONAL_GREETING_FLAG      CHAR(1)
--          PREMIER_COLLECTION_FLAG     VARCHAR2(1)
--          SERVICE_DURATION            NUMBER
--          ALLOW_FREE_SHIPPING_FLAG    CHAR(1)
--          MORNING_DELIVERY_FLAG       CHAR(1)
--          PQUAD_PRODUCT_ID             NUMBER(32)
--          BULLET_DESCRIPTION          VARCHAR2(1024)
--
-- Description:   Queries the PRODUCT_MASTER table by PRODUCT_ID and returns a ref cursor
--                to the resulting row.
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN

   OPEN cur_cursor FOR
      SELECT pm.product_id                    as "productId",
             novator_id                    as "novatorId",
             product_name                  as "productName",
             novator_name                  as "novatorName",
             status                        as "status",
             delivery_type                 as "deliveryType",
             category                      as "category",
             product_type                  as "productType",
             product_sub_type              as "productSubType",
             color_size_flag               as "colorSizeFlag",
             standard_price                as "standardPrice",
             deluxe_price                  as "deluxePrice",
             premium_price                 as "premiumPrice",
             preferred_price_point         as "preferredPricePoint",
             variable_price_max            as "variablePriceMax",
             short_description             as "shortDescription",
             long_description              as "longDescription",
             florist_reference_number      as "floristReferenceNumber",
             mercury_description           as "mercuryDescription",
             item_comments                 as "itemsComments",
             add_on_balloons_flag          as "addOnBallonsFlag",
             add_on_bears_flag             as "addOnBearsFlag",
             add_on_cards_flag             as "addOnCardsFlag",
             add_on_funeral_flag           as "addOnFuneralFlag",
             cp.codification_id            as "codifiedFlag",
             exception_code                as "exceptionCode",
             exception_start_date          as "exceptionStartDate",
             exception_end_date            as "exceptionEndDate",
             exception_message             as "exceptionMessage",
             null                          as "vendorId",
             null                          as "vendorCost" ,
             null                          as "vendorSku",
             second_choice_code            as "secondChoiceCode",
             holiday_second_choice_code    as "holidaySecondChoiceCode",
             dropship_code                 as "dropshipCode",
             discount_allowed_flag         as "discountAllowedFlag",
             delivery_included_flag        as "deliveryIncludedFlag",
             no_tax_flag                   as "taxFlag",
             service_fee_flag              as "serviceFeeFlag",
             exotic_flag                   as "exoticFlag",
             egift_flag                    as "egiftFlag",
             country_id                    as "countryId",
             arrangement_size              as "arrangementSize",
             arrangement_colors            as "arrangementColors",
             dominant_flowers              as "dominantFlowers",
             search_priority               as "searchPriority",
             standard_recipe               as "recipe",
             subcode_flag                  as "subcodeFlag",
             dim_weight                    as "dimWeight",
             next_day_upgrade_flag         as "nextDayUpgradeFlag",
             corporate_site                as "corporateSite",
             unspsc_code                   as "unspscCode",
             price_rank_1                  as "priceRank1",
             price_rank_2                  as "priceRank2",
             price_rank_3                  as "priceRank3",
             ship_method_carrier           as "shipMethodCarrier",
             ship_method_florist           as "shipMethodFlorist",
             shipping_key                  as "shippingKey",
             DECODE(source, 'PDB', cast(last_update as timestamp), last_update)    as "lastUpdate",
             variable_price_flag           as "variablePriceFlag",
             holiday_sku                   as "holidaySku",
             holiday_price                 as "holidayPrice",
             catalog_flag                  as "catalogFlag",
             holiday_deluxe_price          as "holidayDeluxePrice",
             holiday_premium_price         as "holidayPremiumPrice",
             holiday_start_date            as "holidayStartDate",
             holiday_end_date              as "holidayEndDate",
             mercury_second_choice         as "mercurySecondChoice",
             mercury_holiday_second_choice as "mercuryHolidaySecondChoice",
             add_on_chocolate_flag         as "addOnChocolateFlag",
             JCP_Category                  as "JCPenneyCategory",
             CROSS_REF_NOVATOR_ID          as "crossRefNovatorID",
             GENERAL_COMMENTS              as "generalComments",
             SENT_TO_NOVATOR_PROD          as "sentToNovatorProd",
             SENT_TO_NOVATOR_UAT           as "sentToNovatorUat",
             SENT_TO_NOVATOR_TEST          as "sentToNovatorTest",
             SENT_TO_NOVATOR_CONTENT       as "sentToNovatorContent",
             DEFAULT_CARRIER               as "defaultCarrier",
             HOLD_UNTIL_AVAILABLE          as "holdUntilAvailable",
             last_update_user_id           as "lastUpdateUserId",
             last_update_system            as "lastUpdateSystem",
             MONDAY_DELIVERY_FRESHCUT      as "mondayDeliveryFreshcut",
             TWO_DAY_SAT_FRESHCUT          as "twoDayShipSatFreshcut",
             WEBOE_BLOCKED                 as "weboeBlocked",
             EXPRESS_SHIPPING_ONLY         as "expressShippingOnly",
             OVER_21                       as "over21",
             SHIPPING_SYSTEM               as "shippingSystem",
             PERSONALIZATION_TEMPLATE_ID   as "personalizationTemplate",
             PERSONALIZATION_LEAD_DAYS     as "personalizationLeadDays",
             DEPARTMENT_CODE               as "departmentCode",
             PERSONALIZATION_TEMPLATE_ORDER as "personalizationTemplateOrder",
             ZONE_JUMP_ELIGIBLE_FLAG	   as "zoneJumpEligibleFlag",
             BOX_ID                        as "boxId",
             CUSTOM_FLAG                   as "customFlag",
             KEYWORDS_TXT                  as "keywordsTxt",
             SUPPLIES_EXPENSE_DOLLAR_AMT   as "suppliesExpenseDollarAmt",
             SUPPLIES_EXPENSE_EFF_DATE     as "suppliesExpenseEffDate",
             ROYALTY_PERCENT               as "royaltyPercent",
             ROYALTY_PERCENT_EFF_DATE      as "royaltyPercentEffDate",
             GBB_POPOVER_FLAG              as "gbbPopoverFlag",
             GBB_TITLE_TXT                 as "gbbTitle",
             GBB_NAME_OVERRIDE_FLAG_1      as "gbbNameOverrideFlag1",
             GBB_NAME_OVERRIDE_TXT_1       as "gbbNameOverrideText1",
             GBB_PRICE_OVERRIDE_FLAG_1     as "gbbPriceOverrideFlag1",
             GBB_PRICE_OVERRIDE_TXT_1      as "gbbPriceOverrideText1",
             GBB_NAME_OVERRIDE_FLAG_2      as "gbbNameOverrideFlag2",
             GBB_NAME_OVERRIDE_TXT_2       as "gbbNameOverrideText2",
             GBB_PRICE_OVERRIDE_FLAG_2     as "gbbPriceOverrideFlag2",
             GBB_PRICE_OVERRIDE_TXT_2      as "gbbPriceOverrideText2",
             GBB_NAME_OVERRIDE_FLAG_3      as "gbbNameOverrideFlag3",
             GBB_NAME_OVERRIDE_TXT_3       as "gbbNameOverrideText3",
             GBB_PRICE_OVERRIDE_FLAG_3     as "gbbPriceOverrideFlag3",
             GBB_PRICE_OVERRIDE_TXT_3      as "gbbPriceOverrideText3",
             UPDATED_BY                    as "updatedBy",
             send_standard_recipe_flag     as "sendStandardRecipeFlag",
             deluxe_recipe                 as "deluxeRecipe",
             send_deluxe_recipe_flag       as "sendDeluxeRecipeFlag",
             premium_recipe                as "premiumRecipe",
             send_premium_recipe_flag      as "sendPremiumRecipeFlag",
             PERSONAL_GREETING_FLAG 	   as "personalGreetingFlag",
             PREMIER_COLLECTION_FLAG       as "premierCollectionFlag",
             SERVICE_DURATION              as "serviceDuration",
             ALLOW_FREE_SHIPPING_FLAG      as "allowFreeShippingFlag",
             MORNING_DELIVERY_FLAG		   as "morningDeliveryFlag",
			 PQUAD_PRODUCT_ID              as "pquadProductID",
			 PQUAD_PC_ID                   as "pquadPcID",
             PQUAD_PC_DISPLAY_NAMES        as "pquadPcDisplayNames",
             PERSONALIZATION_CASE_FLAG     as "personalizationCaseFlag",
             ALL_ALPHA_FLAG				   as "allAlphaFlag",
             BULLET_DESCRIPTION            as "bulletDescription"   
      FROM PRODUCT_MASTER PM
      LEFT OUTER JOIN CODIFIED_PRODUCTS CP
      ON CP.PRODUCT_ID = PM.PRODUCT_ID
      WHERE PM.PRODUCT_ID = UPPER(productId);
   
    RETURN cur_cursor;
END;
/
