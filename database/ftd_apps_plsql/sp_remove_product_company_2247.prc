CREATE OR REPLACE
PROCEDURE ftd_apps.sp_remove_product_company_2247 (productID IN VARCHAR2)
 --==============================================================================
--
-- Name:    sp_remove_product_company_2247
-- Type:    PROCEDURE
-- Syntax:  sp_remove_product_company_2247(productID )
--
-- Description:   Removes all records for the product in PRODUCT_COMPANY_XREF
--
--===========================================================


AS
BEGIN
	DELETE FROM product_company_xref
	WHERE product_id = productID;
END;
.
/
