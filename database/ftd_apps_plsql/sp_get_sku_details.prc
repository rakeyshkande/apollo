create or replace procedure ftd_apps.SP_GET_SKU_DETAILS(
IN_SKU_ID                IN  VARCHAR2,
OUT_SKU_CUR              OUT types.ref_cursor,
OUT_COMPANY_CUR          OUT types.ref_cursor,
OUT_KEYWORD_CUR          OUT types.ref_cursor,
OUT_RECIPIENT_SEARCH_CUR OUT types.ref_cursor,
OUT_UPSELL_SOURCE_CUR    OUT types.ref_cursor
) 
AS
  v_sku_id  PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_SKU_ID);
begin
    
    --Get the sku master information
    OUT_SKU_CUR := FTD_APPS.SP_GET_UPSELL_BY_SKU(v_sku_id);
    
    --Get the companies
    OUT_COMPANY_CUR := FTD_APPS.SP_GET_UPSELL_COMPANIES_2247(v_sku_id);
    
    --Get the keywords 
    OUT_KEYWORD_CUR := FTD_APPS.SP_GET_PRODUCT_KEYWORDS(v_sku_id);
    
    --Get the recipient search
    OUT_RECIPIENT_SEARCH_CUR := FTD_APPS.SP_GET_PRODUCT_RECIPIENT_SRCH(v_sku_id);
    
    --Get the website source codes
    OUT_UPSELL_SOURCE_CUR := FTD_APPS.SP_GET_UPSELL_SOURCE(v_sku_id);
    
end SP_GET_SKU_DETAILS;
/
