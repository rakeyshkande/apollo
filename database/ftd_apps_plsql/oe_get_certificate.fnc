CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_CERTIFICATE (ftdGuid IN VARCHAR2
)
  RETURN types.ref_cursor

AS
    cur_cursor types.ref_cursor;
BEGIN

    UPDATE SECURITY_CERT SET LAST_UPDATE_DATE = SYSDATE WHERE FTD_GUID = ftdGuid;

	  commit;

    OPEN cur_cursor FOR
        SELECT FIRST_NAME as "firstName",
              LAST_NAME as "lastName",
              CALL_CENTER_ID as "callCenterId",
              ROLE_ID as "roleId",
              USER_ID as "userName"
        FROM SECURITY_CERT
        WHERE FTD_GUID = ftdGuid;

    RETURN cur_cursor;
END
;
.
/
