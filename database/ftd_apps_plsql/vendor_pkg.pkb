CREATE OR REPLACE PACKAGE BODY FTD_APPS.VENDOR_PKG
AS


FUNCTION GET_NEXT_VENDOR_ID
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------
Description:
   Returns the next vendor_id sequence number in a varchar format with
   padded leading zeros..
------------------------------------------------------------------------------*/
v_seq_number     NUMBER;
v_vendor_id      VENDOR_MASTER.VENDOR_ID%TYPE;
v_column_length  NUMBER;

BEGIN
   SELECT vendor_id_sequence.NEXTVAL
     INTO v_seq_number
     FROM DUAL;

   SELECT data_length
     INTO v_column_length
     FROM ALL_TAB_COLUMNS
    WHERE owner = 'FTD_APPS'
      AND table_name = 'VENDOR_MASTER'
      AND column_name = 'VENDOR_ID';

   v_vendor_id := LPAD(TO_CHAR(v_seq_number), v_column_length,'0');

   RETURN v_vendor_id;

END GET_NEXT_VENDOR_ID;


PROCEDURE GET_VENDOR_TYPES_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table vendor_types.

Input:
        N/A

Output:
        cursor containing vendor_types info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT vendor_code,
            display_name,
            description
       FROM vendor_types
       WHERE active = 'Y';

END GET_VENDOR_TYPES_LIST;


PROCEDURE INSERT_VENDOR_MASTER
(
 IN_VENDOR_ID        IN VENDOR_MASTER.VENDOR_ID%TYPE,
 IN_VENDOR_NAME      IN VENDOR_MASTER.VENDOR_NAME%TYPE,
 IN_DEFAULT_CARRIER  IN VENDOR_MASTER.DEFAULT_CARRIER%TYPE,
 IN_MEMBER_NUMBER    IN VENDOR_MASTER.MEMBER_NUMBER%TYPE,
 IN_VENDOR_TYPE      IN VENDOR_MASTER.VENDOR_TYPE%TYPE,
 IN_ACTIVE           IN VENDOR_MASTER.ACTIVE%TYPE,
 IN_ADDRESS1         IN VENDOR_MASTER.ADDRESS1%TYPE,
 IN_ADDRESS2         IN VENDOR_MASTER.ADDRESS2%TYPE,
 IN_CITY             IN VENDOR_MASTER.CITY%TYPE,
 IN_STATE            IN VENDOR_MASTER.STATE%TYPE,
 IN_ZIP_CODE         IN VENDOR_MASTER.ZIP_CODE%TYPE,
 IN_LEAD_DAYS        IN VENDOR_MASTER.LEAD_DAYS%TYPE,
 IN_COMPANY          IN VENDOR_MASTER.COMPANY%TYPE,
 IN_GENERAL_INFO     IN VENDOR_MASTER.GENERAL_INFO%TYPE,
 IN_CUTOFF_TIME      IN VENDOR_MASTER.CUTOFF_TIME%TYPE,
 IN_ACCOUNTS_PAYABLE_ID IN VENDOR_MASTER.ACCOUNTS_PAYABLE_ID%TYPE,
 IN_GL_ACCOUNT_NUMBER   IN VENDOR_MASTER.GL_ACCOUNT_NUMBER%TYPE,
 IN_ZONE_JUMP_ELIGIBLE_FLAG IN VENDOR_MASTER.ZONE_JUMP_ELIGIBLE_FLAG%TYPE,
 IN_NEW_SHIPPING_SYSTEM	    IN VENDOR_MASTER.NEW_SHIPPING_SYSTEM%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure inserts a record into the vendor_master and
   florist_master tables.

Input:
         vendor_id       VARCHAR2
	 vendor_name     VARCHAR2
	 default_carrier VARCHAR2
	 member_number   VARCHAR2
	 vendor_type     VARCHAR2
	 active          CHAR
	 address1        VARCHAR2
	 address2        VARCHAR2
	 city            VARCHAR2
	 state           VARCHAR2
	 zip_code        VARCHAR2
	 lead_days       NUMBER
	 company         VARCHAR2
	 general_info    VARCHAR2
	 cutoff_time     VARCHAR2
	 accounts_payable_id VARCHAR2
	 gl_account_number   VARCHAR2
         zone_jump_eligible_flag	CHAR
         new_shipping_system	CHAR

Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
   v_usage_percent NUMBER;
   v_count      number := 0;
BEGIN

   INSERT INTO florist_master
       (florist_id,
        florist_name,
        address,
        city,
        state,
        zip_code,
        vendor_flag,
        suspend_override_flag,
        allow_message_forwarding_flag,
        is_fto_flag,
        status,
        last_updated_date, last_updated_by)
     VALUES
       (in_member_number,
	in_vendor_name,
	in_address1 || ' ' || in_address2,
	in_city,
	in_state,
	in_zip_code,
	'Y',
	'N',
	'N',
	'N',
	'Active',
	SYSDATE, 'SYS_INSERT_VENDOR');

   INSERT INTO vendor_master
       (vendor_id,
	vendor_name,
	default_carrier,
	member_number,
	vendor_type,
	active,
	address1,
	address2,
	city,
	state,
	zip_code,
	lead_days,
	company,
	general_info,
	cutoff_time,
  	accounts_payable_id,
  	gl_account_number,
  	zone_jump_eligible_flag,
  	new_shipping_system)
     VALUES
       (in_vendor_id,
        in_vendor_name,
	in_default_carrier,
	in_member_number,
	in_vendor_type,
	in_active,
	in_address1,
	in_address2,
	in_city,
	in_state,
	in_zip_code,
	in_lead_days,
	in_company,
	in_general_info,
	in_cutoff_time,
  	in_accounts_payable_id,
  	in_gl_account_number,
  	in_zone_jump_eligible_flag,
  	in_new_shipping_system);

     FOR cid_rec IN (SELECT carrier_id FROM venus.carriers)
        LOOP
           IF UPPER(cid_rec.carrier_id) = 'FEDEX' THEN
              v_usage_percent := 100;
            ELSE
              v_usage_percent := 0;
           END IF;
           INSERT INTO venus.vendor_carrier_ref (vendor_id, carrier_id, usage_percent)
              VALUES (in_vendor_id, cid_rec.carrier_id, v_usage_percent);
        END LOOP;

  -- Insert a command to update PAS for non FTD WEST vendors only
   SELECT count(*)
        into v_count
        FROM  ftd_apps.vendor_master vm
        WHERE vm.vendor_id = in_vendor_id
        AND vm.vendor_type != 'FTD WEST';
        
     IF v_count > 0 THEN
        PAS.POST_PAS_COMMAND('MODIFIED_VENDOR',in_vendor_id);
     END IF;
  


  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_VENDOR_MASTER;


PROCEDURE GET_VENDOR_MASTER
(
 IN_VENDOR_ID                   IN VENDOR_MASTER.VENDOR_ID%TYPE,
 OUT_VENDOR_MASTER_CURSOR      OUT TYPES.REF_CURSOR,
 OUT_PRIMARY_CONTACT_CURSOR    OUT TYPES.REF_CURSOR,
 OUT_ALTERNATE_CONTACT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table vendor_master and vendor_contact for the
   vendor id passed in.

Input:
       vendor id    VARCHAR2

Output:
        cursor containing vendor_master info
        cursor containing primary vendor_contact info
        cursor containing alternate vendor_contact info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_vendor_master_cursor FOR
     SELECT vendor_id,
            vendor_name,
            default_carrier,
            member_number,
            vendor_type,
            active,
            address1,
            address2,
            city,
            state,
            zip_code,
            lead_days,
            company,
            general_info,
            cutoff_time,
            accounts_payable_id,
            gl_account_number,
            zone_jump_eligible_flag,
            new_shipping_system
       FROM vendor_master
      WHERE vendor_id = in_vendor_id;

   OPEN out_primary_contact_cursor FOR
     SELECT vendor_id,
            contact_name,
            primary,
            phone,
            phone_extension,
            fax,
            fax_extension,
            email
       FROM vendor_contact
      WHERE vendor_id = in_vendor_id
        AND primary = 'Y';

   OPEN out_alternate_contact_cursor FOR
     SELECT vendor_id,
            contact_name,
            primary,
            phone,
            phone_extension,
            fax,
            fax_extension,
            email
       FROM vendor_contact
      WHERE vendor_id = in_vendor_id
        AND primary = 'N';

END GET_VENDOR_MASTER;


PROCEDURE UPDATE_VENDOR_MASTER
(
 IN_VENDOR_ID        IN VENDOR_MASTER.VENDOR_ID%TYPE,
 IN_VENDOR_NAME      IN VENDOR_MASTER.VENDOR_NAME%TYPE,
 IN_DEFAULT_CARRIER  IN VENDOR_MASTER.DEFAULT_CARRIER%TYPE,
 IN_MEMBER_NUMBER    IN VENDOR_MASTER.MEMBER_NUMBER%TYPE,
 IN_VENDOR_TYPE      IN VENDOR_MASTER.VENDOR_TYPE%TYPE,
 IN_ACTIVE           IN VENDOR_MASTER.ACTIVE%TYPE,
 IN_ADDRESS1         IN VENDOR_MASTER.ADDRESS1%TYPE,
 IN_ADDRESS2         IN VENDOR_MASTER.ADDRESS2%TYPE,
 IN_CITY             IN VENDOR_MASTER.CITY%TYPE,
 IN_STATE            IN VENDOR_MASTER.STATE%TYPE,
 IN_ZIP_CODE         IN VENDOR_MASTER.ZIP_CODE%TYPE,
 IN_LEAD_DAYS        IN VENDOR_MASTER.LEAD_DAYS%TYPE,
 IN_COMPANY          IN VENDOR_MASTER.COMPANY%TYPE,
 IN_GENERAL_INFO     IN VENDOR_MASTER.GENERAL_INFO%TYPE,
 IN_CUTOFF_TIME      IN VENDOR_MASTER.CUTOFF_TIME%TYPE,
 IN_ACCOUNTS_PAYABLE_ID IN VENDOR_MASTER.ACCOUNTS_PAYABLE_ID%TYPE,
 IN_GL_ACCOUNT_NUMBER   IN VENDOR_MASTER.GL_ACCOUNT_NUMBER%TYPE,
 IN_ZONE_JUMP_ELIGIBLE_FLAG   IN VENDOR_MASTER.ZONE_JUMP_ELIGIBLE_FLAG%TYPE,
 IN_NEW_SHIPPING_SYSTEM	      IN VENDOR_MASTER.NEW_SHIPPING_SYSTEM%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure updates a record in the vendor_master and
   florist_master tables.

Input:
         vendor_id       VARCHAR2
	 vendor_name     VARCHAR2
	 default_carrier VARCHAR2
	 member_number   VARCHAR2
	 vendor_type     VARCHAR2
	 active          CHAR
	 address1        VARCHAR2
	 address2        VARCHAR2
	 city            VARCHAR2
	 state           VARCHAR2
	 zip_code        VARCHAR2
	 lead_days       NUMBER
	 company         VARCHAR2
	 general_info    VARCHAR2
	 cutoff_time     VARCHAR2
	 accounts_payable_id VARCHAR2
	 gl_account_number   VARCHAR2
         zone_jump_eligible_flag	CHAR
         new_shipping_system	CHAR

Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

   UPDATE vendor_master
      SET vendor_name = in_vendor_name,
	  default_carrier = in_default_carrier,
	  member_number = in_member_number,
	  vendor_type = in_vendor_type,
	  active = in_active,
	  address1 = in_address1,
	  address2 = in_address2,
	  city = in_city,
	  state = in_state,
	  zip_code = in_zip_code,
	  lead_days = in_lead_days,
	  company = in_company,
	  general_info = in_general_info,
	  cutoff_time = in_cutoff_time,
    	accounts_payable_id = in_accounts_payable_id,
    	gl_account_number = in_gl_account_number,
    	zone_jump_eligible_flag = in_zone_jump_eligible_flag,
    	new_shipping_system = in_new_shipping_system
    WHERE vendor_id = in_vendor_id;


  IF SQL%FOUND THEN
    out_status := 'Y';

    -- Insert a command to update PAS for ARGO Vendors only
    IF nvl(in_vendor_type, 'X') != 'FTD WEST' THEN
        PAS.POST_PAS_COMMAND('MODIFIED_VENDOR', in_vendor_id);
    END IF;

    UPDATE florist_master
       SET florist_name = in_vendor_name,
           address = in_address1 || ' ' || in_address2,
           city = in_city,
           state = in_state,
           zip_code = in_zip_code,
           last_updated_date = SYSDATE,
           last_updated_by = 'SYS_UPDATE_VENDOR'
     WHERE florist_id = in_member_number;

    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
      out_status := 'N';
      out_message := 'WARNING: No records found for florist_master ' || in_member_number ||'.';
    END IF;

  ELSE
    out_status := 'N';
    out_message := 'WARNING: No records found for vendor_master ' || in_vendor_id ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_MASTER;


PROCEDURE INACTIVATE_VENDOR
(
 IN_VENDOR_ID    IN VENDOR_MASTER.VENDOR_ID%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure updates the active column to N for the vendor_id
   passed in.

Input:
        vendor_id        VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
v_count      number := 0;
BEGIN

   UPDATE vendor_master
      SET active = 'N'
    WHERE vendor_id = in_vendor_id;

  IF SQL%FOUND THEN
    out_status := 'Y';
    -- Insert a command to update PAS for non FTD WEST vendors only
   SELECT count(*)
        into v_count
        FROM  ftd_apps.vendor_master vm
        WHERE vm.vendor_id = in_vendor_id
        AND vm.vendor_type != 'FTD WEST';
        
     IF v_count > 0 THEN
    	PAS.POST_PAS_COMMAND('MODIFIED_VENDOR',in_vendor_id);
    END IF;
  ELSE
    out_status := 'N';
    out_message := 'WARNING: No records found for vendor_master ' || in_vendor_id ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INACTIVATE_VENDOR;


PROCEDURE INSERT_VENDOR_CONTACT
(
 IN_VENDOR_ID       IN VENDOR_CONTACT.VENDOR_ID%TYPE,
 IN_CONTACT_NAME    IN VENDOR_CONTACT.CONTACT_NAME%TYPE,
 IN_PRIMARY         IN VENDOR_CONTACT.PRIMARY%TYPE,
 IN_PHONE           IN VENDOR_CONTACT.PHONE%TYPE,
 IN_PHONE_EXTENSION IN VENDOR_CONTACT.PHONE_EXTENSION%TYPE,
 IN_FAX             IN VENDOR_CONTACT.FAX%TYPE,
 IN_FAX_EXTENSION   IN VENDOR_CONTACT.FAX_EXTENSION%TYPE,
 IN_EMAIL           IN VENDOR_CONTACT.EMAIL%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure inserts a record into the vendor_contact table and
   updates its related record in table florist_master.

Input:
         vendor_id       VARCHAR2
	 contact_name    VARCHAR2
	 primary         CHAR
	 phone           VARCHAR2
	 phone_extension VARCHAR2
	 fax             VARCHAR2
	 fax_extension   VARCHAR2
	 email           VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
v_member_number vendor_master.member_number%TYPE;

BEGIN

   INSERT INTO vendor_contact
       (vendor_id,
	contact_name,
	primary,
	phone,
	phone_extension,
	fax,
	fax_extension,
	email)
     VALUES
       (in_vendor_id,
        in_contact_name,
	in_primary,
	in_phone,
	in_phone_extension,
	in_fax,
	in_fax_extension,
	in_email);

   SELECT member_number
     INTO v_member_number
     FROM vendor_master
    WHERE vendor_id = in_vendor_id;

   IF in_primary = 'Y' THEN
     UPDATE florist_master
        SET owners_name = in_contact_name,
            phone_number = in_phone,
            fax_number = in_fax,
            email_address = in_email
      WHERE florist_id = v_member_number;
   ELSE
     UPDATE florist_master
        SET alt_phone_number = in_phone
      WHERE florist_id = v_member_number;
   END IF;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_VENDOR_CONTACT;


PROCEDURE DELETE_ALL_VENDOR_CONTACTS
(
 IN_VENDOR_ID    IN VENDOR_CONTACT.VENDOR_ID%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure deletes every row in table vendor_contact that is
   related to the vendor_id passed in.

Input:
        vendor_id        VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

  DELETE vendor_contact
   WHERE vendor_id = in_vendor_id;

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    out_message := 'WARNING: No records found for vendor_contact ' || in_vendor_id ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ALL_VENDOR_CONTACTS;


PROCEDURE INSERT_VENDOR_RESTRICTIONS
(
 IN_BLOCK_TYPE   IN VARCHAR2,
 IN_VENDOR_ID    IN VARCHAR2,
 IN_START_DATE   IN DATE,
 IN_END_DATE     IN DATE,
 IN_GLOBAL_FLAG  IN CHAR,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the distinct records from the vendor delivery restrictions
   table and the vendor shipping restrictions that have a global flag set to 'Y'.

Input:
        block_type       VARCHAR2
        vendor_id        VARCHAR2
	start_date       DATE
	end_date         DATE
	global_flag      CHAR


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
v_add_days     number;
v_end_date     date;
v_date         date;
v_priority     integer;

BEGIN

   IF LOWER(in_block_type) = 'shipping' THEN
     INSERT INTO vendor_shipping_restrictions
       (vendor_id,
	start_date,
	end_date,
	global_flag)
     VALUES
       (in_vendor_id,
        in_start_date,
        in_end_date,
        in_global_flag);

     SELECT FGP.VALUE INTO V_ADD_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'ADD_DAYS';
     v_end_date := in_end_date + v_add_days;

   ELSE
     INSERT INTO vendor_delivery_restrictions
       (vendor_id,
	start_date,
	end_date,
	global_flag)
     VALUES
       (in_vendor_id,
        in_start_date,
        in_end_date,
        in_global_flag);

     v_end_date := in_end_date;

   END IF;

   v_date := in_start_date;
   while v_date <= v_end_date loop
     v_priority := v_date - trunc(sysdate);
     PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_VENDOR',in_vendor_id || ' ' || to_char(v_date, 'mmddyyyy'), v_priority);
     v_date := v_date + 1;
   end loop;

  out_status := 'Y';

  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
       out_status := 'Y';
       out_message := ' ';
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_VENDOR_RESTRICTIONS;


PROCEDURE DELETE_VENDOR_RESTRICTIONS
(
 IN_BLOCK_TYPE   IN VARCHAR2,
 IN_VENDOR_ID    IN VARCHAR2,
 IN_START_DATE   IN DATE,
 IN_GLOBAL_FLAG  IN VARCHAR2,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Deletes all the records from the vendor delivery restrictions table or the
   vendor shipping restrictions for the vendor id and start date passed for the
   appropriate global flag.

Input:
        block_type       VARCHAR2
        vendor_id        VARCHAR2
	start_date       DATE
        global_flag      VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
v_add_days     number;
v_end_date     date;
v_date         date;
v_priority     integer;

BEGIN

   IF LOWER(in_block_type) = 'shipping' THEN
     DELETE FROM vendor_shipping_restrictions
       WHERE vendor_id = in_vendor_id
	 AND TRUNC(start_date) = TRUNC(in_start_date)
	 AND global_flag = IN_GLOBAL_FLAG;

     SELECT FGP.VALUE INTO V_ADD_DAYS FROM FRP.GLOBAL_PARMS FGP WHERE CONTEXT = 'PAS_CONFIG' AND NAME = 'ADD_DAYS';
     v_end_date := in_start_date + v_add_days;

   ELSIF LOWER(in_block_type) = 'delivery' THEN
     DELETE FROM vendor_delivery_restrictions
       WHERE vendor_id = in_vendor_id
	 AND TRUNC(start_date) = TRUNC(in_start_date)
	 AND global_flag = IN_GLOBAL_FLAG;

     v_end_date := in_start_date;
     
   END IF;

  IF SQL%FOUND THEN
    out_status := 'Y';

    v_date := in_start_date;
    while v_date <= v_end_date loop
      v_priority := v_date - trunc(sysdate);
      PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_VENDOR',in_vendor_id || ' ' || to_char(v_date, 'mmddyyyy'), v_priority);
      v_date := v_date + 1;
    end loop;

  ELSE
    out_status := 'N';
    out_message := 'WARNING: No vendor restritions were deleted.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_VENDOR_RESTRICTIONS;


PROCEDURE INSERT_GLOBAL_DELIVERY_BLOCK
(
 IN_BLOCK_DATE   IN DATE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure calls stored procedure insert_vendor_restrictions
   to insert global delivery restriction dates for each vendor in the
   vendor_master table.


Input:
        block_date       DATE


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
  CURSOR vendor_master_cur IS
     SELECT vendor_id
       FROM vendor_master;

  v_vendor_id  vendor_master.vendor_id%TYPE;

BEGIN


  FOR vm_record IN vendor_master_cur LOOP
      insert_vendor_restrictions('delivery', vm_record.vendor_id, in_block_date, in_block_date, 'Y',out_status, out_message);
      IF out_status = 'N' THEN
        EXIT;
      END IF;
  END LOOP;


--  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_GLOBAL_DELIVERY_BLOCK;


PROCEDURE INSERT_GLOBAL_SHIPPING_BLOCK
(
 IN_BLOCK_DATE   IN DATE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure calls stored procedure insert_vendor_restrictions
   to insert global shipping restriction dates for each vendor in the
   vendor_master table.


Input:
        block_date       DATE


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
  CURSOR vendor_master_cur IS
     SELECT vendor_id
       FROM vendor_master;

  v_vendor_id  vendor_master.vendor_id%TYPE;

BEGIN


  FOR vm_record IN vendor_master_cur LOOP
      insert_vendor_restrictions('shipping', vm_record.vendor_id, in_block_date, in_block_date, 'Y',out_status, out_message);
      IF out_status = 'N' THEN
        EXIT;
      END IF;
  END LOOP;


--  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_GLOBAL_SHIPPING_BLOCK;


PROCEDURE GET_VENDOR_BLOCKS
(
 IN_VENDOR_ID    IN VARCHAR2,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the distinct records from the vendor delivery restrictions
   table and the vendor shipping restrictions that have a global flag set to 'Y'.

Input:
        vendor_id        VARCHAR2

Output:
        cursor containing vendor_delivery_restrictions and
           vendor_shipping_restrictions info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
      SELECT DISTINCT start_date,
             end_date,
             'Delivery' block_type
        FROM vendor_delivery_restrictions
       WHERE vendor_id = in_vendor_id
         AND global_flag = 'N'
         AND end_date >= TRUNC(SYSDATE)
     UNION
      SELECT DISTINCT start_date,
             end_date,
             'Shipping' block_type
        FROM vendor_shipping_restrictions
       WHERE vendor_id = in_vendor_id
         AND global_flag = 'N'
         AND end_date >= TRUNC(SYSDATE);


END GET_VENDOR_BLOCKS;


PROCEDURE GET_GLOBAL_VENDOR_BLOCKS
(
 IN_VENDOR_ID    IN VARCHAR2,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all global vendor blocks for a vendor.

Input:
        vendor_id        VARCHAR2

Output:
        cursor containing vendor_delivery_restrictions and
           vendor_shipping_restrictions info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
      SELECT DISTINCT start_date,
             end_date,
             'Delivery' block_type
        FROM vendor_delivery_restrictions
       WHERE vendor_id = in_vendor_id
         AND global_flag = 'Y'
         AND end_date >= TRUNC(SYSDATE)
     UNION
      SELECT DISTINCT start_date,
             end_date,
             'Shipping' block_type
        FROM vendor_shipping_restrictions
       WHERE vendor_id = in_vendor_id
         AND global_flag = 'Y'
         AND end_date >= TRUNC(SYSDATE);


END GET_GLOBAL_VENDOR_BLOCKS;


PROCEDURE GET_GLOBAL_BLOCKS
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the distinct records from the vendor delivery restrictions
   table and the vendor shipping restrictions that have a global flag set to 'Y'.

Input:
        N/A

Output:
        cursor containing vendor_delivery_restrictions and
           vendor_shipping_restrictions info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
      SELECT DISTINCT start_date,
             end_date,
             'Delivery' block_type
        FROM vendor_delivery_restrictions
       WHERE global_flag = 'Y'
         AND end_date >= TRUNC(SYSDATE)
     UNION
      SELECT DISTINCT start_date,
             end_date,
             'Shipping' block_type
        FROM vendor_shipping_restrictions
       WHERE global_flag = 'Y'
         AND end_date >= TRUNC(SYSDATE);


END GET_GLOBAL_BLOCKS;


PROCEDURE DELETE_GLOBAL_BLOCKS
(
 IN_BLOCK_TYPE   IN VARCHAR2,
 IN_BLOCK_DATE   IN DATE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure deletes every row in tables vendor delivery restrictions
   table or vendor shipping restrictions depending on the block type passed in
   and block date.

Input:
        block_type   VARCHAR2
        block_date   DATE


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

   IF LOWER(in_block_type) = 'shipping' THEN
     DELETE vendor_shipping_restrictions
      WHERE TRUNC(start_date) = TRUNC(in_block_date)
        AND global_flag = 'Y';
   ELSE
     DELETE vendor_delivery_restrictions
      WHERE TRUNC(start_date) = TRUNC(in_block_date)
        AND global_flag = 'Y';
   END IF;

   PAS.POST_PAS_COMMAND('MODIFIED_VENDOR','ALL');


  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    out_message := 'WARNING: No global ' || in_block_type || ' restrictions records found for start_date ' || in_block_date ||'.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_GLOBAL_BLOCKS;


PROCEDURE INSERT_MAINT_COMMENTS
(
 IN_COMMENT_TYPE         IN MAINT_COMMENTS.COMMENT_TYPE%TYPE,
 IN_COMMENT_TYPE_ID      IN MAINT_COMMENTS.COMMENT_TYPE_ID%TYPE,
 IN_COMMENT_DESCRIPTION  IN MAINT_COMMENTS.COMMENT_DESCRIPTION%TYPE,
 IN_CREATED_BY           IN MAINT_COMMENTS.CREATED_BY%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure inserts a record into the maint_comments table.

Input:
         comment_type        VARCHAR2
	 comment_type_id     VARCHAR2
	 comment_description VARCHAR2
         created_by          VARCHAR2

Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

   INSERT INTO maint_comments
       (comment_id,
	comment_type,
	comment_type_id,
	comment_description,
        created_by,
        created_on)
     VALUES
       (comment_id_sq.NEXTVAL,
	in_comment_type,
	in_comment_type_id,
	in_comment_description,
        in_created_by,
        SYSDATE);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_MAINT_COMMENTS;


PROCEDURE GET_VENDOR_MAINT_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   7	Returns all the records from the vendor_master table that are active.

Input:
        N/A

Output:
        cursor containing vendor_master and vendor_contact info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT vm.vendor_id,
            vm.vendor_name,
            vm.city,
            vm.state,
            vc.phone,
            vc.email,
            vm.zone_jump_eligible_flag,
            (SELECT COUNT(DISTINCT start_date)
               FROM vendor_delivery_restrictions
              WHERE vendor_id = vm.vendor_id
                AND end_date >= SYSDATE)
             +
            (SELECT COUNT(DISTINCT start_date)
               FROM vendor_shipping_restrictions
              WHERE vendor_id = vm.vendor_id
                AND end_date >= TRUNC(SYSDATE))          days_blocked,
            vm.new_shipping_system    
       FROM vendor_master vm,
            vendor_contact vc
      WHERE vm.active = 'Y'
        AND vc.vendor_id (+) = vm.vendor_id
        AND vc.primary (+) = 'Y'
   ORDER BY vm.vendor_name;

END GET_VENDOR_MAINT_LIST;


PROCEDURE GET_VENDOR_MAINT_LIST_SORTED
(
 IN_SORT_BY     IN VARCHAR2,
 OUT_CURSOR    OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   7	Returns all the records from the vendor_master table that are active.

Input:
        N/A

Output:
        cursor containing vendor_master and vendor_contact info
------------------------------------------------------------------------------*/
-- variables
v_order_by            varchar2 (4000) := IN_SORT_BY;

BEGIN
  
  IF IN_SORT_BY is null or IN_SORT_BY = '' THEN
    v_order_by := 'vm.vendor_id'  ; 
  END IF; 
  
  OPEN out_cursor FOR
    'SELECT vm.vendor_id,
            trim(vm.vendor_name) vendor_name,
            vm.city,
            vm.state,
            vc.phone,
            vc.email,
            vm.zone_jump_eligible_flag,
            (SELECT COUNT(DISTINCT start_date)
               FROM vendor_delivery_restrictions
              WHERE vendor_id = vm.vendor_id
                AND end_date >= SYSDATE)
             +
            (SELECT COUNT(DISTINCT start_date)
               FROM vendor_shipping_restrictions
              WHERE vendor_id = vm.vendor_id
                AND end_date >= TRUNC(SYSDATE))          days_blocked,
            vm.new_shipping_system               
     FROM vendor_master vm,
          vendor_contact vc
    WHERE vm.active = ''Y''
      AND vc.vendor_id (+) = vm.vendor_id
      AND vc.primary (+) = ''Y''
    ORDER BY trim(' || v_order_by || ')'; 

END GET_VENDOR_MAINT_LIST_SORTED;


PROCEDURE GET_MAINT_COMMENTS
(
 IN_COMMENT_TYPE       IN MAINT_COMMENTS.COMMENT_TYPE%TYPE,
 IN_COMMENT_TYPE_ID    IN MAINT_COMMENTS.COMMENT_TYPE_ID%TYPE,
 IN_STARTING_PAGE      IN NUMBER,
 IN_RETURN_MAX_NUMBER  IN NUMBER,
 OUT_TOTAL_PAGES      OUT NUMBER,
 OUT_CURSOR           OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   	Returns all the records from the maint_comments table
        for the comment_type and comment_type_id passed in.

Input:
        comment_type          VARCHAR2
        comment_type_id       NUMBER
        in_start_position     NUMBER
        in_end_position       NUMBER

Output:
        out_max_rows          NUMBER
        cursor containing maint_comments info
------------------------------------------------------------------------------*/
v_main_sql          VARCHAR2(3000);
v_rowcount_sql      VARCHAR2(3000);
v_max_rows          NUMBER;
v_start_position    NUMBER;
v_end_position      NUMBER;

BEGIN

   v_main_sql := 'SELECT comment_id,
	                 comment_type,
	                 comment_type_id,
	                 comment_description,
                         created_on,
                         created_by
                    FROM maint_comments
                   WHERE comment_type = ''' || in_comment_type || '''
                     AND comment_type_id = ''' || in_comment_type_id || '''
                   ORDER BY created_on DESC';

   SELECT COUNT(1)
     INTO v_max_rows
     FROM maint_comments
    WHERE comment_type = in_comment_type
      AND comment_type_id = in_comment_type_id
    ORDER BY created_on DESC;

   IF v_max_rows > 0 THEN
     IF in_return_max_number >= v_max_rows  THEN
       --more rows are asked to be returned than exist
       -- so return all the rows on one page
       v_start_position := 1;
       out_total_pages := 1;
     ELSE

       ---total number pf pages
       out_total_pages := CEIL(v_max_rows / in_return_max_number);

       --if the page asked to be returned is greater than the
       -- total number of pages, then return just the last page
       IF in_starting_page > out_total_pages THEN
         v_end_position := out_total_pages * in_return_max_number;
       ELSE
         v_end_position := in_starting_page * in_return_max_number;
       END IF;

       v_start_position := v_end_position - in_return_max_number + 1;
     END IF;
   END IF;


   --the follwing code will only return the rows asked for
   IF v_start_position IS NOT NULL AND v_end_position IS NOT NULL THEN

     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ') rslt
                     WHERE ROWNUM <= ' || v_end_position;

     v_main_sql := 'SELECT *
                      FROM (' || v_main_sql || ')
                     WHERE rnum >= ' || v_start_position;

   ELSIF v_start_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ') rslt';

     v_main_sql := 'SELECT *
                      FROM (' || v_main_sql || ')
                     WHERE rnum >= ' || v_start_position;


   ELSIF v_end_position IS NOT NULL THEN
     v_main_sql := 'SELECT rslt.*,
                           ROWNUM rnum
                      FROM (' || v_main_sql || ') rslt
                     WHERE ROWNUM <= ' || v_end_position;

     v_main_sql := 'SELECT *
                      FROM (' || v_main_sql || ')';


   END IF;


   OPEN out_cursor FOR v_main_sql;
--open out_cursor for select v_main_sql from dual;


END GET_MAINT_COMMENTS;


PROCEDURE GET_VENDOR
(
 IN_VENDOR_ID  IN  FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns vendor for the given vendor id.

Input:
        vendor id

Output:
        cursor containing vendor info
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CURSOR FOR
     SELECT vendor_name,
            vendor_type,
            member_number
     FROM  ftd_apps.vendor_master
     WHERE vendor_id = in_vendor_id;

END GET_VENDOR;

PROCEDURE UPDATE_VENDOR_CUTOFF_GLOBAL
(
 IN_CUTOFF_TIME          IN FTD_APPS.VENDOR_MASTER.CUTOFF_TIME%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure updates all vendor cutoff times to the passed value.

Input:
	      cutoff_time     VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
BEGIN

    UPDATE vendor_master set cutoff_time = IN_CUTOFF_TIME;
    OUT_STATUS := 'Y';

    -- Insert a command to update PAS
    PAS.POST_PAS_COMMAND('MODIFIED_VENDOR','ALL');

  EXCEPTION
    WHEN OTHERS THEN
       OUT_STATUS := 'N';
       OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_CUTOFF_GLOBAL;

PROCEDURE GET_ACTIVE_VENDOR_TYPES_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table vendor_types where the ACTIVE
   flag is set to ?Y?.

Input:
        N/A

Output:
        cursor containing vendor_types info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT vendor_code,
            display_name,
            description
       FROM vendor_types
       WHERE active = 'Y';

END GET_ACTIVE_VENDOR_TYPES_LIST;

FUNCTION GET_VENDORS_OF_TYPE
	(IN_VENDOR_TYPE VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDORS_OF_TYPE
-- Type:    Function
-- Syntax:  GET_VENDORS_OF_TYPE(IN_VENDOR_TYPE VARCHAR2)
-- Returns: ref_cursor for
--          VENDOR_ID   VARCHAR2(5)
--          VENDOR_NAME VARCHAR2(40)
--
--
-- Description:   Queries the VENDOR_MASTER table for vendors
--				  of a specific type and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
BEGIN
    OPEN vendor_cursor FOR
        SELECT vm.VENDOR_ID    as "vendorId",
               vm.VENDOR_NAME  as "vendorName"
          FROM VENDOR_MASTER vm
          WHERE vm.VENDOR_TYPE = IN_VENDOR_TYPE
          ORDER BY vm.VENDOR_NAME;

    RETURN vendor_cursor;
END GET_VENDORS_OF_TYPE;

FUNCTION GET_VENDORS_FOR_PRODUCT
	(IN_PRODUCT_SUBCODE_ID VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDORS_FOR_PRODUCT
-- Type:    Function
-- Syntax:  GET_VENDORS_FOR_PRODUCT(IN_PRODUCT_SKU_ID VARCHAR2)
-- Returns: ref_cursor for
--          VENDOR_ID      VARCHAR2(5)
--          PRODUCT_SUBCODE_ID VARCHAR2(10)
--          VENDOR_COST    NUMBER
--          VENDOR_SKU     VARCHAR(10)
--          AVAILABLE      CHAR(1)
--          REMOVED        CHAR(1)
--          VENDOR_NAME    VARCHAR
--
--
-- Description:   Queries the VENDOR_PRODUCT table for vendors
--				  that are listed for passed product/sku id
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
    v_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_SUBCODE_ID);
    v_hasSubcodes            CHAR(1);
BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

    BEGIN
      SELECT 'Y' INTO v_hasSubcodes FROM FTD_APPS.PRODUCT_SUBCODES ps1 WHERE ps1.product_id = v_productID AND ROWNUM =1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_hasSubcodes := 'N';
    END;

    IF v_hasSubcodes = 'Y' THEN
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED,
                   vm.VENDOR_NAME
              FROM VENDOR_PRODUCT vp
              JOIN VENDOR_MASTER vm ON vp.VENDOR_ID = vm.VENDOR_ID
              JOIN PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
              WHERE ps.PRODUCT_ID = v_productID
              AND vp.REMOVED = 'N'
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
    ELSE
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED,
                   vm.VENDOR_NAME
              FROM VENDOR_PRODUCT vp
              JOIN VENDOR_MASTER vm ON vp.VENDOR_ID = vm.VENDOR_ID
              WHERE vp.PRODUCT_SUBCODE_ID = v_productID
              AND vp.REMOVED = 'N'
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
      END IF;

    RETURN vendor_cursor;

END GET_VENDORS_FOR_PRODUCT;

FUNCTION GET_VENDOR_ID_FOR_PRODUCT
	(IN_PRODUCT_SUBCODE_ID VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDOR_ID_FOR_PRODUCT
-- Type:    Function
-- Syntax:  GET_VENDOR_ID_FOR_PRODUCT(IN_PRODUCT_SKU_ID VARCHAR2)
-- Returns: ref_cursor for
--          VENDOR_ID      VARCHAR2(5)
--          VENDOR_NAME    VARCHAR
--
--
-- Description:   Queries the VENDOR_PRODUCT table for vendors
--				  that are listed for passed product/sku id
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
    v_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_SUBCODE_ID);
    v_hasSubcodes            CHAR(1);
BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

    BEGIN
      SELECT 'Y' INTO v_hasSubcodes FROM FTD_APPS.PRODUCT_SUBCODES ps1 WHERE ps1.product_id = v_productID AND ROWNUM =1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_hasSubcodes := 'N';
    END;

    IF v_hasSubcodes = 'Y' THEN
        OPEN vendor_cursor FOR
            SELECT distinct vp.VENDOR_ID,
                   vm.VENDOR_NAME
              FROM VENDOR_PRODUCT vp
              JOIN VENDOR_MASTER vm ON vp.VENDOR_ID = vm.VENDOR_ID
              JOIN PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
              WHERE ps.PRODUCT_ID = v_productID
              AND vp.REMOVED = 'N'
              AND vm.ACTIVE = 'Y'
              ORDER BY vm.VENDOR_NAME;
    ELSE
        OPEN vendor_cursor FOR
            SELECT distinct vp.VENDOR_ID,
                   vm.VENDOR_NAME
              FROM VENDOR_PRODUCT vp
              JOIN VENDOR_MASTER vm ON vp.VENDOR_ID = vm.VENDOR_ID
              WHERE vp.PRODUCT_SUBCODE_ID = v_productID
              AND vp.REMOVED = 'N'
              AND vm.ACTIVE = 'Y'
              ORDER BY vm.VENDOR_NAME;
    END IF;

    RETURN vendor_cursor;

END GET_VENDOR_ID_FOR_PRODUCT;

FUNCTION GET_AVAILABLE_VENDORS
	(IN_PRODUCT_SUBCODE_ID VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_AVAILABLE_VENDORS
-- Type:    Function
-- Syntax:  GET_AVAILABLE_VENDORS(IN_PRODUCT_SKU_ID VARCHAR2)
-- Returns: ref_cursor for
--          VENDOR_ID      VARCHAR2(5)
--          PRODUCT_SUBCODE_ID VARCHAR2(10)
--          VENDOR_COST    NUMBER
--          VENDOR_SKU     VARCHAR(10)
--          AVAILABLE      CHAR(1)
--          REMOVED        CHAR(1)
--          VENDOR_NAME    VARCHAR
--
--
-- Description:   Queries the VENDOR_PRODUCT table for vendors
--				  that are listed for passed product/sku id
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
    v_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_SUBCODE_ID);
    v_hasSubcodes            CHAR(1);


BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

    BEGIN
      SELECT 'Y' INTO v_hasSubcodes FROM FTD_APPS.PRODUCT_SUBCODES ps1 WHERE ps1.product_id = v_productID AND ROWNUM =1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_hasSubcodes := 'N';
    END;

    IF v_hasSubcodes = 'Y' THEN
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
              JOIN PRODUCT_MASTER pm ON ps.PRODUCT_ID = pm.PRODUCT_ID
              WHERE ps.PRODUCT_ID = v_productID
              AND vp.PRODUCT_SUBCODE_ID = UPPER(IN_PRODUCT_SUBCODE_ID)
              AND vp.REMOVED = 'N'
              AND vp.AVAILABLE = 'Y'
              AND pm.PRODUCT_ID = v_productID
              AND pm.STATUS = 'A'
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
    ELSE
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_MASTER pm ON vp.PRODUCT_SUBCODE_ID = pm.PRODUCT_ID
              WHERE vp.PRODUCT_SUBCODE_ID = v_productID
              AND vp.REMOVED = 'N'
              AND vp.AVAILABLE = 'Y'
              AND pm.STATUS = 'A'
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
      END IF;

    RETURN vendor_cursor;

END GET_AVAILABLE_VENDORS;

FUNCTION GET_AVAILABLE_VENDORS
	(IN_PRODUCT_SUBCODE_ID VARCHAR2,
         IN_ORDER_DETAIL_ID NUMBER)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_AVAILABLE_VENDORS
-- Type:    Function
-- Syntax:  GET_AVAILABLE_VENDORS(IN_PRODUCT_SKU_ID VARCHAR2, IN_ORDER_DETAIL_ID NUMBER)
-- Returns: ref_cursor for
--          VENDOR_ID      VARCHAR2(5)
--          PRODUCT_SUBCODE_ID VARCHAR2(10)
--          VENDOR_COST    NUMBER
--          VENDOR_SKU     VARCHAR(10)
--          AVAILABLE      CHAR(1)
--          REMOVED        CHAR(1)
--          VENDOR_NAME    VARCHAR
--
--
-- Description:   Queries the VENDOR_PRODUCT table for vendors
--				  that are listed for passed product/sku id/add ons
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    vendor_cursor types.ref_cursor;
    v_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_SUBCODE_ID);
    v_hasSubcodes            CHAR(1);
    v_addon_cnt              number;

BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

    BEGIN
        SELECT COUNT(*)
        INTO V_ADDON_CNT
        FROM CLEAN.ORDER_ADD_ONS
        WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
        V_ADDON_CNT := 0;
    END;

    BEGIN
      SELECT 'Y' INTO v_hasSubcodes FROM FTD_APPS.PRODUCT_SUBCODES ps1 WHERE ps1.product_id = v_productID AND ROWNUM =1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_hasSubcodes := 'N';
    END;

    IF v_hasSubcodes = 'Y' THEN
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
              JOIN PRODUCT_MASTER pm ON ps.PRODUCT_ID = pm.PRODUCT_ID
              WHERE ps.PRODUCT_ID = v_productID
              AND vp.PRODUCT_SUBCODE_ID = UPPER(IN_PRODUCT_SUBCODE_ID)
              AND vp.REMOVED = 'N'
              AND vp.AVAILABLE = 'Y'
              AND pm.PRODUCT_ID = v_productID
              AND pm.STATUS = 'A'
    and (v_addon_cnt = 0 OR vp.vendor_id in (
        select va.vendor_id
        from ftd_apps.vendor_addon va
        where va.active_flag = 'Y'
        and va.addon_id in (
            select add_on_code
            from clean.order_add_ons
            where order_detail_id = IN_ORDER_DETAIL_ID)
        group by va.vendor_id
        having count(addon_id) = v_addon_cnt
    ))
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
    ELSE
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_MASTER pm ON vp.PRODUCT_SUBCODE_ID = pm.PRODUCT_ID
              WHERE vp.PRODUCT_SUBCODE_ID = v_productID
              AND vp.REMOVED = 'N'
              AND vp.AVAILABLE = 'Y'
              AND pm.STATUS = 'A'
    and (v_addon_cnt = 0 OR vp.vendor_id in (
        select va.vendor_id
        from ftd_apps.vendor_addon va
        where va.active_flag = 'Y'
        and va.addon_id in (
            select add_on_code
            from clean.order_add_ons
            where order_detail_id = IN_ORDER_DETAIL_ID)
        group by va.vendor_id
        having count(addon_id) = v_addon_cnt
    ))
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
      END IF;

    RETURN vendor_cursor;

END GET_AVAILABLE_VENDORS;

FUNCTION GET_VENDOR_PRODUCT
	(IN_VENDOR_ID VARCHAR2, IN_PRODUCT_SUBCODE_ID VARCHAR2)
	RETURN types.ref_cursor
--==============================================================================
--
-- Name:    GET_VENDOR_PRODUCT
-- Type:    Function
-- Syntax:  GET_VENDOR_PRODUCT(IN_VENDOR_ID VARCHAR2, IN_PRODUCT_SKU_ID VARCHAR2)
-- Returns: ref_cursor for
--          VENDOR_ID      VARCHAR2(5)
--          PRODUCT_SUBCODE_ID VARCHAR2(10)
--          VENDOR_COST    NUMBER
--          VENDOR_SKU     VARCHAR(10)
--          AVAILABLE      CHAR(1)
--          REMOVED        CHAR(1)
--          VENDOR_NAME    VARCHAR
--
--
-- Description:   Queries the VENDOR_PRODUCT table for the passed vendor_id,
--                product/sku id and marked available
--                returns a ref cursor for resulting row.
--
--==============================================================================
AS
    vendor_cursor types.ref_cursor;
    v_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_SUBCODE_ID);
    v_hasSubcodes            CHAR(1);
BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

    BEGIN
      SELECT 'Y' INTO v_hasSubcodes FROM FTD_APPS.PRODUCT_SUBCODES ps1 WHERE ps1.product_id = v_productID AND ROWNUM =1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_hasSubcodes := 'N';
    END;

    IF v_hasSubcodes = 'Y' THEN
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
              JOIN PRODUCT_MASTER pm ON ps.PRODUCT_ID = pm.PRODUCT_ID
              WHERE ps.PRODUCT_ID = v_productID
              AND vp.VENDOR_ID = IN_VENDOR_ID
              AND vp.REMOVED = 'N'
              AND vp.AVAILABLE = 'Y'
              AND pm.PRODUCT_ID = v_productID
              AND pm.STATUS = 'A'
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
    ELSE
        OPEN vendor_cursor FOR
            SELECT vp.VENDOR_ID,
                   vp.PRODUCT_SUBCODE_ID,
                   vp.VENDOR_COST,
                   vp.VENDOR_SKU,
                   vp.AVAILABLE,
                   vp.REMOVED
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_MASTER pm ON vp.PRODUCT_SUBCODE_ID = pm.PRODUCT_ID
              WHERE vp.PRODUCT_SUBCODE_ID = v_productID
              AND vp.VENDOR_ID = IN_VENDOR_ID
              AND vp.REMOVED = 'N'
              AND vp.AVAILABLE = 'Y'
              AND pm.STATUS = 'A'
              ORDER BY vp.VENDOR_ID, vp.PRODUCT_SUBCODE_ID;
      END IF;

    RETURN vendor_cursor;

END GET_VENDOR_PRODUCT;

PROCEDURE PAST_VENDOR_CUTOFF
(
IN_PRODUCT_ID         IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
PAST_CUTOFF          OUT VARCHAR2,
CUTOFF               OUT VARCHAR2)
--==============================================================================
--
-- Name:    PAST_VENDOR_CUTOFF
-- Type:    Function
-- Syntax:  PAST_VENDOR_CUTOFF(IN_PRODUCT_ID VARCHAR2
-- Returns:
--          PAST_CUTOFF    VARCHAR2  ('true' or 'false')
--          CUTOFF         VARCHAR2
--
--
-- Description:   DETERMINE IF WE ARE PAST THE CUTOFF TO SHIP A PRODUCT,
--                IDENTIFIED BY THE PASSED IN ID, DAY.  RETURN THE CUTOFF AND
--                TRUE IF WE ARE PAST THE CUTOFF ELSE FALSE.
--
--==============================================================================
AS
v_temp_string        VARCHAR2(100);
v_latest_cutoff      DATE;
v_backend_delay      NUMBER;
v_vendor_latest_cutoff DATE;
v_cutoff             VARCHAR2(100);

BEGIN
  -- obtain latest vendor cutoff time
  /*SELECT MAX(vm."CUTOFF_TIME") CUTOFF_TIME
    INTO v_cutoff
    FROM FTD_APPS."VENDOR_PRODUCT" vpv, FTD_APPS."VENDOR_MASTER" vm,
         (
           SELECT distinct vp2."AVAILABLE", vp2."PRODUCT_SUBCODE_ID"
             FROM FTD_APPS."PRODUCT_SUBCODES" ps2, FTD_APPS."VENDOR_PRODUCT" vp2, FTD_APPS."PRODUCT_MASTER" pm
           WHERE
           (
             (
               vp2."PRODUCT_SUBCODE_ID" = ps2."PRODUCT_SUBCODE_ID" OR
               vp2."PRODUCT_SUBCODE_ID" = pm."PRODUCT_ID"
             ) AND
             vp2."AVAILABLE" = 'Y'
           )
         ) yy
  WHERE vpv."VENDOR_ID" = vm."VENDOR_ID" AND vpv."PRODUCT_SUBCODE_ID" = IN_PRODUCT_ID
    AND yy.PRODUCT_SUBCODE_ID = IN_PRODUCT_ID;*/

  SELECT MAX(vm."CUTOFF_TIME") CUTOFF_TIME
    INTO v_cutoff
    FROM FTD_APPS."VENDOR_PRODUCT" vpv,
    		FTD_APPS."VENDOR_MASTER" vm
   WHERE vpv."PRODUCT_SUBCODE_ID" = IN_PRODUCT_ID
     AND vpv.available = 'Y'
	  AND vpv."VENDOR_ID" = vm."VENDOR_ID";

  -- get the latest cutoff time
  BEGIN
    select "VALUE" INTO v_temp_string from FRP."GLOBAL_PARMS" where "CONTEXT"='SHIPPING_PARMS' AND NAME = 'LASTEST_CUTOFF_VENDOR';
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_temp_string := '2330';
  END;

  -- compute the latest cutoff time
  v_latest_cutoff := TRUNC(SYSDATE)+(TO_NUMBER(SUBSTR(v_temp_string,1,2))*(1/24))+(TO_NUMBER(SUBSTR (v_temp_string,3,2))*(1/1440));

  if v_latest_cutoff - SYSDATE < 0 THEN
    PAST_CUTOFF := 'true';
    -- Set and format CUTOFF
    CUTOFF := TO_CHAR(v_latest_cutoff, 'HH24MI');
  ELSE
    -- get the back-end cutoff delay
    BEGIN
      select "VALUE" INTO v_temp_string from FRP."GLOBAL_PARMS" where "CONTEXT"='SHIPPING_PARMS' AND NAME = 'BACKEND_CUTOFF_DELAY';
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_temp_string := '30';
    END;
    v_backend_delay := TO_NUMBER(v_temp_string);

    -- default to true
    PAST_CUTOFF  := 'true';

    -- try and find a cutoff that has not passed
     v_temp_string := v_cutoff;
     v_vendor_latest_cutoff :=
       TRUNC(SYSDATE)+ -- 00:00 today
       (TO_NUMBER(SUBSTR(v_temp_string,1,2))*(1/24))+ -- hours from vendor cutoff
       (TO_NUMBER(SUBSTR (v_temp_string,4,2))*(1/1440)) + -- minutes from vendor cutoff
       (v_backend_delay*(1/1440)); -- back end delay

     -- is the computed vendor cutoff time after the latest cutoff time?
     IF v_latest_cutoff - v_vendor_latest_cutoff < 0 THEN
       v_vendor_latest_cutoff := v_latest_cutoff;
     END IF;

     -- If we are not past the latest cutoff, then set return value and exit
     IF v_vendor_latest_cutoff - SYSDATE > 0 THEN
       PAST_CUTOFF := 'false';
     END IF;

    -- Set and format CUTOFF
    CUTOFF := TO_CHAR(v_vendor_latest_cutoff, 'HH24MI');

  END IF;

END PAST_VENDOR_CUTOFF;

PROCEDURE INSERT_VENDOR_CUTOFF
(
 IN_VENDOR_ID           IN FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE,
 IN_CUTOFF_TIME         IN FTD_APPS.VENDOR_MASTER.CUTOFF_TIME%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure updates the cutoff time for a given vendor.

Input:
	      vendor_id       VARCHAR2
	      cutoff_time     VARCHAR2


Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/
v_count      number := 0;
BEGIN

    UPDATE vendor_master set cutoff_time = IN_CUTOFF_TIME where vendor_id = IN_VENDOR_ID;
    OUT_STATUS := 'Y';

    -- Insert a command to update PAS for non FTD WEST vendors only
      SELECT count(*)
        into v_count
        FROM  ftd_apps.vendor_master vm
        WHERE vm.vendor_id = in_vendor_id
        AND vm.vendor_type != 'FTD WEST';
        
     IF v_count > 0 THEN
    	PAS.POST_PAS_COMMAND('MODIFIED_VENDOR',in_vendor_id);
   	 END IF;

  EXCEPTION
    WHEN OTHERS THEN
       OUT_STATUS := 'N';
       OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_VENDOR_CUTOFF;

PROCEDURE GET_VENDOR_IDS
(
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves all vendor ids.

Input:
N/A

Output:
cursor containing vendor id info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT vendor_id
    FROM ftd_apps.vendor_master
    WHERE ACTIVE='Y';

END GET_VENDOR_IDS;

PROCEDURE GET_VENDOR_MASTER_ONLY
(
 IN_VENDOR_ID                   IN VENDOR_MASTER.VENDOR_ID%TYPE,
 OUT_VENDOR_MASTER_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table vendor_master for the
   vendor id passed in.

Input:
       vendor id    VARCHAR2

Output:
        cursor containing vendor_master info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_vendor_master_cursor FOR
     SELECT vendor_id,
            vendor_name,
            default_carrier,
            member_number,
            vendor_type,
            active,
            address1,
            address2,
            city,
            state,
            zip_code,
            lead_days,
            company,
            general_info,
            cutoff_time
       FROM vendor_master
      WHERE vendor_id = in_vendor_id;


END GET_VENDOR_MASTER_ONLY;

PROCEDURE GET_PARTNER_SHIPPING_ACCOUNTS
(
 IN_VENDOR_ID        IN VENDOR_MASTER.VENDOR_ID%TYPE,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table partner_shipping_accounts for the vendor

Input:
	vendor_id
Output:
        cursor containing partner_shipping_accounts info for the vendor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT vendor_id,
        carrier_id,
        partner_id,
        account_num,
        validation_regex
       FROM ftd_apps.partner_shipping_accounts
       WHERE vendor_id = in_vendor_id;

END GET_PARTNER_SHIPPING_ACCOUNTS;

PROCEDURE GET_PARTNER_SHIPPING_ACCOUNTS
(
 IN_VENDOR_ID        IN partner_shipping_accounts.VENDOR_ID%TYPE,
 IN_PARTNER_ID       IN partner_shipping_accounts.PARTNER_ID%TYPE,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table partner_shipping_accounts for the vendor
   and partner id's passed

Input:
	vendor_id
Output:
        cursor containing partner_shipping_accounts info for the vendor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT vendor_id,
        carrier_id,
        partner_id,
        account_num,
        validation_regex
       FROM ftd_apps.partner_shipping_accounts
       WHERE vendor_id = in_vendor_id AND
             partner_id = in_partner_id;

END GET_PARTNER_SHIPPING_ACCOUNTS;

PROCEDURE UPDATE_PARTNER_SHIPPING_ACCNTS
(
 IN_VENDOR_ID       IN VENDOR_MASTER.VENDOR_ID%TYPE,
 IN_CARRIER_ID      IN PARTNER_SHIPPING_ACCOUNTS.CARRIER_ID%TYPE,
 IN_PARTNER_ID      IN PARTNER_SHIPPING_ACCOUNTS.PARTNER_ID%TYPE,
 IN_ACCOUNT_NUM     IN PARTNER_SHIPPING_ACCOUNTS.ACCOUNT_NUM%TYPE,
 IN_VALIDATION_REGEX	IN PARTNER_SHIPPING_ACCOUNTS.VALIDATION_REGEX%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   This stored procedure updates a record in the ftd_apps.partner_shipping_accounts.
	Only the account number can be updated throught the UI.  However, if a
	new vendor is added, then an insert will be done using the default
	records found in FTD_APPS.DEFAULT_SHIPPING_ACCOUNTS

Input:
         vendor_id       VARCHAR2
         carrier_id      VARCHAR2
         partner_id      VARCHAR2
         account_num     VARCHAR2
         validation_regexVARCHAR2

Output:
        status           VARCHAR2
        message          VARCHAR2
------------------------------------------------------------------------------*/

v_check         number;

CURSOR check_cur IS
        SELECT  max(1)
        FROM    partner_shipping_accounts
    	WHERE vendor_id = in_vendor_id
      	    and carrier_id = in_carrier_id
      	    and partner_id = in_partner_id;


BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

IF v_check IS NOT NULL THEN

   UPDATE partner_shipping_accounts
      SET account_num = in_account_num
    WHERE vendor_id = in_vendor_id
      and carrier_id = in_carrier_id
      and partner_id = in_partner_id;

ELSE
        INSERT INTO partner_shipping_accounts
        (
                vendor_id,
		carrier_id,
		partner_id,
		account_num,
        	validation_regex
        )
        VALUES
        (
                in_vendor_id,
		in_carrier_id,
		in_partner_id,
		in_account_num,
        	in_validation_regex
        );
END IF;

OUT_STATUS := 'Y';


  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PARTNER_SHIPPING_ACCNTS;


PROCEDURE GET_DEFAULT_SHIPPING_ACCOUNTS
(
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table default_shipping_accounts
Input:
	vendor_id
Output:
        cursor containing default_shipping_accounts
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT carrier_id,
        partner_id,
        account_num,
        validation_regex
       FROM ftd_apps.default_shipping_accounts;

END GET_DEFAULT_SHIPPING_ACCOUNTS;

FUNCTION HAS_PARTNER_SHIP_ACCOUNT(
  IN_VENDOR_ID IN PARTNER_SHIPPING_ACCOUNTS.VENDOR_ID%TYPE,
  IN_PARTNER_ID IN PARTNER_SHIPPING_ACCOUNTS.PARTNER_ID%TYPE
) RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        Determine if a vendor has a shipping account for the passed in vendor

Input:
        vendor_id                 varchar2
        partner_id                varchar2

Output:
        "Y" if an account exists, otherwise "N"

-----------------------------------------------------------------------------*/
  v_count number;
  v_result varchar2(1);
BEGIN
  SELECT COUNT(*) INTO v_count
    FROM  FTD_APPS.PARTNER_SHIPPING_ACCOUNTS
    WHERE PARTNER_SHIPPING_ACCOUNTS.VENDOR_ID = IN_VENDOR_ID AND
          PARTNER_SHIPPING_ACCOUNTS.PARTNER_ID = IN_PARTNER_ID AND
          PARTNER_SHIPPING_ACCOUNTS.ACCOUNT_NUM IS NOT NULL;

  IF v_count > 0 THEN
    v_result := 'Y';
  ELSE
    v_result := 'N';
  END IF;

  RETURN v_result;
END HAS_PARTNER_SHIP_ACCOUNT;

PROCEDURE GET_PARTNER_SHIP_ACCT_FOR_ZIP
(
 IN_VENDOR_ID        IN partner_shipping_accounts.VENDOR_ID%TYPE,
 IN_PARTNER_ID       IN partner_shipping_accounts.PARTNER_ID%TYPE,
 IN_POSTAL_CODE      IN VENUS.CARRIER_EXCLUDED_ZIPS.ZIP_CODE%TYPE,

 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records in table partner_shipping_accounts for the vendor
   and partner id's passed

Input:
	vendor_id
Output:
        cursor containing partner_shipping_accounts info for the vendor
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT
        psa.vendor_id,
        psa.carrier_id,
        psa.partner_id,
        psa.account_num
       FROM ftd_apps.partner_shipping_accounts psa
       JOIN VENUS."CARRIERS" c ON psa."CARRIER_ID" = c."CARRIER_ID"
       WHERE psa.vendor_id = IN_VENDOR_ID AND
             psa.partner_id = IN_PARTNER_ID AND
             psa.account_num IS NOT NULL AND
             c."ACTIVE"='Y' AND
             c.carrier_id NOT IN (SELECT cez.carrier_id
               FROM venus.carrier_excluded_zips cez
               WHERE NVL(IN_POSTAL_CODE,'NULLVALUE') = cez.zip_code);

END GET_PARTNER_SHIP_ACCT_FOR_ZIP;

FUNCTION GET_PARTNER_SHIPPING_ACCOUNT
(
 IN_VENDOR_ID        IN partner_shipping_accounts.VENDOR_ID%TYPE,
 IN_PARTNER_ID       IN partner_shipping_accounts.PARTNER_ID%TYPE,
 IN_CARRIER_ID       IN partner_shipping_accounts.CARRIER_ID%TYPE
) RETURN partner_shipping_accounts.ACCOUNT_NUM%TYPE
AS
/*------------------------------------------------------------------------------
Description:
   Returns the account number in table partner_shipping_accounts for the
   parameters passed in

Input:
	vendor_id
Output:
  account_num
------------------------------------------------------------------------------*/
v_account_num partner_shipping_accounts.ACCOUNT_NUM%TYPE;
BEGIN
  BEGIN
     SELECT
        account_num
       INTO v_account_num
       FROM ftd_apps.partner_shipping_accounts
       WHERE vendor_id = in_vendor_id AND
             partner_id = in_partner_id AND
             carrier_id = in_carrier_id;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_account_num := '';
  END;
  RETURN v_account_num;
END GET_PARTNER_SHIPPING_ACCOUNT;


/******************************************************************************
                          GET_VENDOR_BLOCKS_BY_DATE_TYPE

Description: This procedure receives an optional input date and block type,
             and returns all the vendor characteristics for the input combo.

******************************************************************************/
PROCEDURE GET_VENDOR_BLOCK_INFO
(
 IN_BLOCK_DATE       IN VARCHAR2,
 IN_BLOCK_TYPE       IN VARCHAR2,
 IN_VENDOR_IDS       IN VARCHAR2,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS

-- variables
v_sql                    varchar2 (4000) := '';
v_select_delivery_from   varchar2 (4000) := ' select v.vendor_id, v.vendor_name, r.global_flag, r.start_date, r.end_date, ''Delivery'' block_type from ftd_apps.vendor_delivery_restrictions r, ftd_apps.vendor_master v';
v_select_shipping_from   varchar2 (4000) := ' select v.vendor_id, v.vendor_name, r.global_flag, r.start_date, r.end_date, ''Shipping'' block_type from ftd_apps.vendor_shipping_restrictions r, ftd_apps.vendor_master v';
v_where                  varchar2 (4000) := ' where start_date > sysdate ';
v_and                    varchar2 (4000) := ' and r.vendor_id = v.vendor_id ';
v_order_by               varchar2 (4000) := ' order by v.vendor_id ';

BEGIN

IF IN_BLOCK_DATE is not null THEN
  v_and   := v_and ||  ' and trunc(to_date(''' || IN_BLOCK_DATE || ''', ''yyyy/mm/dd'')) between trunc(start_date) and trunc(end_date) ';
END IF;

/*note that the calling code is responsible for putting Apostrophes in the in_vendor_id field*/
IF IN_VENDOR_IDS is not null THEN
  v_and   := v_and ||  ' and v.vendor_id in (' || IN_VENDOR_IDS || ')';
END IF;

CASE

  WHEN IN_BLOCK_TYPE is NULL or IN_BLOCK_TYPE = 'All' THEN
    v_sql           := v_select_delivery_from || v_where || v_and;
    v_sql  := v_sql || ' UNION  ';
    v_sql  := v_sql || v_select_delivery_from || v_where || v_and || v_order_by;

  WHEN IN_BLOCK_TYPE = 'Shipping' THEN
    v_sql           := v_select_shipping_from || v_where || v_and || v_order_by;

  WHEN IN_BLOCK_TYPE = 'Delivery' THEN
    v_sql           := v_select_delivery_from || v_where || v_and || v_order_by;

END CASE;

OPEN OUT_CURSOR FOR v_sql;

END GET_VENDOR_BLOCK_INFO;



/******************************************************************************
                          GET_PRODUCT_AND_VENDOR_INFO

Description: Queries the VENDOR_PRODUCT table for vendors that are listed
             for passed product/sku id returns a ref cursor for resulting row.

******************************************************************************/
PROCEDURE GET_PRODUCT_AND_VENDOR_INFO
(
IN_PRODUCT_ID   IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
OUT_CUR        OUT TYPES.REF_CURSOR
)
AS

BEGIN

OPEN OUT_CUR FOR 
  select pm.product_id, pm.product_name, pm.status product_status, pm.updated_by product_updated_by, to_char(pm.exception_start_date, 'mm-dd-yyyy') exception_start_date,  vp.vendor_id, vm.vendor_name, vp.available vendor_product_status
  from ftd_apps.vendor_master vm, ftd_apps.vendor_product vp, ftd_apps.product_master pm
  where pm.PRODUCT_ID = IN_PRODUCT_ID
  and vp.vendor_id = vm.vendor_id
  and vp.product_subcode_id = pm.product_id
  and vp.removed = 'N'
  and vm.active = 'Y'
  order by vm.vendor_name;

END GET_PRODUCT_AND_VENDOR_INFO;

PROCEDURE UPDATE_VENDOR_ADDON (
 IN_VENDOR_ID         in VENDOR_ADDON.VENDOR_ID%TYPE,
 IN_ADDON_ID          in VENDOR_ADDON.ADDON_ID%TYPE,
 IN_ACTIVE_FLAG       in VENDOR_ADDON.ACTIVE_FLAG%TYPE,
 IN_VENDOR_SKU        in VENDOR_ADDON.VENDOR_SKU%TYPE,
 IN_VENDOR_COST       in VENDOR_ADDON.VENDOR_COST%TYPE,
 IN_UPDATED_BY        in VENDOR_ADDON.UPDATED_BY%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
--==============================================================================
--
-- Name:    UPDATE_VENDOR_ADDON
-- Type:    Procedure
-- Syntax:  UPDATE_VENDOR_ADDON ( <see args above> )
--
-- Description:   Attempts to insert a row into VENDOR_ADDON.  If the row
--                already exists (by vendor ID and addon Id), then and update
--                of the existing row is performed
--
--==============================================================================
BEGIN

    UPDATE VENDOR_ADDON
      SET VENDOR_ID = IN_VENDOR_ID,
          ADDON_ID = IN_ADDON_ID,
          ACTIVE_FLAG = IN_ACTIVE_FLAG,
          VENDOR_SKU = IN_VENDOR_SKU,
          VENDOR_COST = IN_VENDOR_COST,
          UPDATED_ON = sysdate,
          UPDATED_BY = IN_UPDATED_BY
       WHERE VENDOR_ID = IN_VENDOR_ID
          AND ADDON_ID = IN_ADDON_ID;
     
     IF SQL%NOTFOUND THEN
       INSERT INTO VENDOR_ADDON (
          VENDOR_ID,
          ADDON_ID,
          ACTIVE_FLAG,
          VENDOR_SKU,
          VENDOR_COST,
          CREATED_ON,
          CREATED_BY,
          UPDATED_ON,
          UPDATED_BY
          )
        VALUES (
          IN_VENDOR_ID,
          IN_ADDON_ID,
          IN_ACTIVE_FLAG,
          IN_VENDOR_SKU,
          IN_VENDOR_COST,
          sysdate,
          IN_UPDATED_BY,
          sysdate,
          IN_UPDATED_BY
        );
     END IF;
     out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_ADDON;

/******************************************************************************
 SEND_TO_NEW_SHIP_SYSTEM 

Description:  Check to see if all vendors associated to product on the order are
              can use the new shipping system  

******************************************************************************/
FUNCTION SEND_TO_NEW_SHIP_SYSTEM 
(
IN_PRODUCT_ID       IN   FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_count_vendors_for_product		int := 0;
v_count_vendors_on_new_ship		int := 0;
v_SEND_TO_NEW_SHIP_SYSTEM  varchar2 (1) := 'N';

BEGIN

  select count(*) into v_count_vendors_for_product
  from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm
  where vp.PRODUCT_SUBCODE_ID = IN_PRODUCT_ID
  and vp.AVAILABLE = 'Y'
  and vp.removed = 'N'
  and vm.VENDOR_ID = vp.vendor_id;
  
  select count(*) into v_count_vendors_on_new_ship
  from ftd_apps.vendor_product vp, ftd_apps.vendor_master vm
  where vp.PRODUCT_SUBCODE_ID = IN_PRODUCT_ID
  and vp.AVAILABLE = 'Y'
  and vp.removed = 'N'
  and vm.NEW_SHIPPING_SYSTEM = 'Y'
  and vm.VENDOR_ID = vp.vendor_id;  

IF v_count_vendors_for_product > 0 AND  v_count_vendors_for_product = v_count_vendors_on_new_ship THEN
  	v_SEND_TO_NEW_SHIP_SYSTEM := 'Y';  
END IF;

RETURN v_SEND_TO_NEW_SHIP_SYSTEM;
END SEND_TO_NEW_SHIP_SYSTEM;

PROCEDURE GET_VENDOR_PRODUCT_FLAGS
(
 IN_PRODUCT_SUBCODE_ID VARCHAR2,
 IN_VENDOR_ID    IN VARCHAR2,
 OUT_REMOVED OUT VARCHAR2,
 OUT_VP_AVAILABILITY OUT VARCHAR2
)
AS

/*------------------------------------------------------------------------------
Description:
   Returns the Flag isRemoved  and availability for a given product and vendor

Input:
         in_product_subcode_id	 VARCHAR2
        vendor_id        VARCHAR2

Output:
        varchar containing is_removed_flag
        varchar containing availability Flag		
------------------------------------------------------------------------------*/
    v_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_SUBCODE_ID);
    v_hasSubcodes            CHAR(1);
BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_ID
        INTO v_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO v_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = v_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            OUT_REMOVED := 'Y';
          END;
        END;
    END;

    BEGIN
      SELECT 'Y' INTO v_hasSubcodes FROM FTD_APPS.PRODUCT_SUBCODES ps1 WHERE ps1.product_id = v_productID AND ROWNUM =1;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_hasSubcodes := 'N';
    END;

    IF v_hasSubcodes = 'Y' THEN
            SELECT vp.REMOVED,vp.AVAILABLE INTO OUT_REMOVED, OUT_VP_AVAILABILITY
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_SUBCODES ps ON vp.PRODUCT_SUBCODE_ID = ps.PRODUCT_SUBCODE_ID
              WHERE ps.PRODUCT_ID = v_productID
              AND vp.PRODUCT_SUBCODE_ID = UPPER(IN_PRODUCT_SUBCODE_ID)
              AND vp.VENDOR_ID = IN_VENDOR_ID;
    ELSE
            SELECT vp.REMOVED,vp.AVAILABLE INTO OUT_REMOVED, OUT_VP_AVAILABILITY
              FROM VENDOR_PRODUCT vp
              JOIN PRODUCT_MASTER pm ON vp.PRODUCT_SUBCODE_ID = pm.PRODUCT_ID
              WHERE vp.PRODUCT_SUBCODE_ID = v_productID
              AND vp.VENDOR_ID = IN_VENDOR_ID;
      END IF;

END GET_VENDOR_PRODUCT_FLAGS;

END;
.
/
