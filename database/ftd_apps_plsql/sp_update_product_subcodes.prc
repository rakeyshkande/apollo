CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_SUBCODES (
 productId in varchar2,
 subCodeId in varchar2,
 subCodeDesc in varchar2,
 subCodePrice in number,
 subCodeHolidaySKU in varchar2,
 subCodeHolidayPrice in number,
 subCodeAvailable in varchar2,
 subCodeRefNumber in varchar2,
 subCodeVendorCost in number,
 subCodeVendorSKU in varchar2,
 subCodeDimWeight in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_SUBCODES
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_SUBCODES ( <see args above> )
--
-- Description:   Performs an INSERT/UPDATE attempt on PRODUCT_SUBCODES.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_SUBCODES
  INSERT INTO PRODUCT_SUBCODES
      ( PRODUCT_ID,
				PRODUCT_SUBCODE_ID,
				SUBCODE_DESCRIPTION,
				SUBCODE_PRICE,
				HOLIDAY_SKU,
				HOLIDAY_PRICE,
				ACTIVE_FLAG,
				SUBCODE_REFERENCE_NUMBER,
				VENDOR_PRICE,
                VENDOR_SKU,
                DIM_WEIGHT
		  )
  VALUES
		  ( productId,
				 subCodeId,
         subCodeDesc,
         subCodePrice,
         subCodeHolidaySKU,
         subCodeHolidayPrice,
         subCodeAvailable,
         subCodeRefNumber,
         subCodeVendorCost,
         subCodeVendorSKU,
         subCodeDimWeight
		  );

  -- Insert a command to update PAS
  -- PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT',productId || ' YES');  
  -- Removed this because PDB already does a global recalc on the product


EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		     -- Update an existing row for this product id if found
    UPDATE PRODUCT_SUBCODES
      SET SUBCODE_DESCRIPTION = subCodeDesc,
					SUBCODE_PRICE = subCodePrice,
					HOLIDAY_SKU = subCodeHolidaySKU,
					HOLIDAY_PRICE = subCodeHolidayPrice,
					ACTIVE_FLAG = subCodeAvailable,
					SUBCODE_REFERENCE_NUMBER = subCodeRefNumber,
					VENDOR_PRICE = subCodeVendorCost,
                    VENDOR_SKU = subCodeVendorSKU,
                    DIM_WEIGHT = subCodeDimWeight
      WHERE PRODUCT_ID = productId AND PRODUCT_SUBCODE_ID = subCodeId;

    -- Insert a command to update PAS
    -- PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT',productId || ' YES');  
    -- Removed this because PDB already does a global recalc on the product

end;
.
/
