

CREATE OR REPLACE TRIGGER ftd_apps.fuel_surcharge_aiu
AFTER INSERT OR UPDATE OF END_DATE
ON ftd_apps.fuel_surcharge

/*****************************************************************************************   
***          Created By : Steve Yeazel
***          Created On : 08/17/2008
***              Reason : Ensure only 1 NULL end_date 
***
*** Special Instructions: 
***
*****************************************************************************************/   

DECLARE
   v_null_end_date_cnt number;    
  
BEGIN

   select count(*) 
   into v_null_end_date_cnt
   From ftd_apps.fuel_surcharge
   where end_date is null;
   
   if v_null_end_date_cnt > 1 then
      raise_application_error(-20001, 'Max of 1 NULL End_Date allowed in table.  ' ||
                                      'This would result in ' || v_null_end_date_cnt || '.');
   end if;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In ftd_apps.fuel_surcharge_trg: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END fuel_surcharge_aiu;
/
