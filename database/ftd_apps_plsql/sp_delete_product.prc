CREATE OR REPLACE
PROCEDURE ftd_apps.SP_DELETE_PRODUCT (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_DELETE_PRODUCT
-- Type:    Procedure
-- Syntax:  SP_DELETE_PRODUCT ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_MASTER by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_MASTER
  where PRODUCT_ID = productId;

  COMMIT;

end
;
.
/
