CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_BATCH (
 inProductId in varchar2,
 inNovatorId in varchar2,
 inUserId    in varchar2,
 inStatus    in varchar2,
 inXmlDoc    in clob,
 inXmlErrors in clob
)
authid current_user
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_BATCH
--
-- Description:   Attempts to insert a row into PRODUCT_MASTER_BATCH.  If the row
--                already exists (by product ID and User ID), then update instead.
--
--==============================================================================

begin

    -- Attempt to insert into PRODUCT_MASTER_BATCH
    INSERT INTO PRODUCT_MASTER_BATCH
      ( PRODUCT_ID,
        NOVATOR_ID,
        LAST_UPDATE_USER_ID,
        STATUS,
        XML_DOCUMENT,
        XML_ERRORS
      )
    VALUES
      ( inProductId,
        inNovatorId,
        inUserId,
        inStatus,
        inXmlDoc,
        inXmlErrors
      );

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

    -- Update the existing row for this product
    UPDATE PRODUCT_MASTER_BATCH
    SET NOVATOR_ID = inNovatorId,
        STATUS = inStatus,
        XML_DOCUMENT = inXmlDoc,
        XML_ERRORS = inXmlErrors
    WHERE PRODUCT_ID = inProductId
    AND LAST_UPDATE_USER_ID = inUserId;

end;
.
/
