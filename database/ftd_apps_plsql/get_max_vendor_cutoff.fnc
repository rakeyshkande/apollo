CREATE OR REPLACE FUNCTION FTD_APPS.GET_MAX_VENDOR_CUTOFF
(
IN_PRODUCT_ID         IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE
)
RETURN types.ref_cursor
IS
--==============================================================================
--
-- Name:    GET_MAX_VENDOR_CUTOFF
-- Type:    Function
-- Syntax:  GET_MAX_VENDOR_CUTOFF(IN_PRODUCT_ID VARCHAR2)
-- Returns: 
--          CUTOFF         VARCHAR2
--
--
-- Description:   DETERMINE IF WE ARE PAST THE CUTOFF TO SHIP A PRODUCT, 
--                IDENTIFIED BY THE PASSED IN ID, DAY.  RETURN THE CUTOFF 
--
--==============================================================================
v_temp_string        	VARCHAR2(100);
v_latest_cutoff      	DATE;
v_vendor_latest_cutoff 	DATE;
v_cutoff             	VARCHAR2(100);
cutoff        				VARCHAR2(100);
cur_cursor 					types.ref_cursor;

BEGIN
  -- obtain latest vendor cutoff time    
  SELECT MAX(vm."CUTOFF_TIME") CUTOFF_TIME
    INTO v_cutoff
    FROM FTD_APPS."VENDOR_PRODUCT" vpv, 
    		FTD_APPS."VENDOR_MASTER" vm
   WHERE vpv."PRODUCT_SUBCODE_ID" = IN_PRODUCT_ID
     AND vpv.available = 'Y'
	  AND vpv."VENDOR_ID" = vm."VENDOR_ID";

  -- get the latest cutoff time
  BEGIN
    select "VALUE" INTO v_temp_string from FRP."GLOBAL_PARMS" where "CONTEXT"='SHIPPING_PARMS' AND NAME = 'LASTEST_CUTOFF_VENDOR';
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_temp_string := '2330';
  END;
  
  -- compute the latest cutoff time
  v_latest_cutoff := TRUNC(SYSDATE)+(TO_NUMBER(SUBSTR(v_temp_string,1,2))*(1/24))+(TO_NUMBER(SUBSTR (v_temp_string,3,2))*(1/1440));
  
  -- Set and format CUTOFF
  cutoff := TO_CHAR(v_latest_cutoff, 'HH24MI');
    
  -- try and find a cutoff that has not passed
  v_temp_string := v_cutoff;
  v_vendor_latest_cutoff := 
	 TRUNC(SYSDATE)+ -- 00:00 today
	 (TO_NUMBER(SUBSTR(v_temp_string,1,2))*(1/24))+ -- hours from vendor cutoff
	 (TO_NUMBER(SUBSTR (v_temp_string,4,2))*(1/1440));  -- minutes from vendor cutoff

  -- is the computed vendor cutoff time after the latest cutoff time?
  IF v_latest_cutoff - v_vendor_latest_cutoff < 0 THEN
	 v_vendor_latest_cutoff := v_latest_cutoff;
  END IF;

  -- Set and format CUTOFF
  cutoff := TO_CHAR(v_vendor_latest_cutoff, 'HH24MI');
  
  OPEN cur_cursor FOR
  SELECT cutoff "cutoff" FROM DUAL;

  RETURN cur_cursor;

END GET_MAX_VENDOR_CUTOFF;
/