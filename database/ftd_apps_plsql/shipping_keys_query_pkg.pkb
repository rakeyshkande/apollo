CREATE OR REPLACE PACKAGE BODY FTD_APPS.SHIPPING_KEYS_QUERY_PKG AS

PROCEDURE GET_SHIPPING_KEYS
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEYS.SHIPPING_KEY_ID%TYPE,
OUT_CUR                         		OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all/one record from ftd_apps.shipping_keys.

Input:
        none

Output:
        cursor containing all records from florist_status

-----------------------------------------------------------------------------*/
BEGIN

   IF in_shipping_key_id IS NULL THEN

		OPEN OUT_CUR FOR
		SELECT shipping_key_id,
				 shipping_key_description, 
				 shipper 
		  FROM ftd_apps.shipping_keys 
		 ORDER BY TO_NUMBER(shipping_key_id);
		 
	ELSE
	
		OPEN OUT_CUR FOR
		SELECT shipping_key_id,
				 shipping_key_description, 
				 shipper 
		  FROM ftd_apps.shipping_keys 
		 WHERE shipping_key_id = in_shipping_key_id;
		 
   END IF;

	out_status := 'Y';
	out_message := NULL;
	
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_QUERY_PKG.GET_SHIPPING_KEYS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_SHIPPING_KEYS;

PROCEDURE GET_SHIPPING_KEY_DETAILS
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
OUT_CUR                         		OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving record(s) from ftd_apps.shipping_key_details.

Input:
        none

Output:
        cursor containing all records from florist_status

-----------------------------------------------------------------------------*/
BEGIN

	OPEN OUT_CUR FOR
	SELECT shipping_detail_id,
			 min_price, 
			 max_price 
	  FROM ftd_apps.shipping_key_details 
	 WHERE shipping_key_id = in_shipping_key_id 
	 ORDER BY shipping_detail_id;

	out_status := 'Y';
	out_message := NULL;
	
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_QUERY_PKG.GET_SHIPPING_KEY_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_SHIPPING_KEY_DETAILS;

PROCEDURE GET_SHIPPING_KEY_COSTS
(
IN_SHIPPING_KEY_DETAIL_ID				IN FTD_APPS.SHIPPING_KEY_COSTS.SHIPPING_KEY_DETAIL_ID%TYPE,
OUT_CUR                         		OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving record(s) from ftd_apps.shipping_key_costs.

Input:
        none

Output:
        cursor containing all records from florist_status

-----------------------------------------------------------------------------*/
BEGIN

	OPEN OUT_CUR FOR
	SELECT shipping_method_id,
	 		 shipping_cost 
	  FROM ftd_apps.shipping_key_costs 
	 WHERE shipping_key_detail_id = in_shipping_key_detail_id;
	 
	out_status := 'Y';
	out_message := NULL;	 

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_QUERY_PKG.GET_SHIPPING_KEY_COSTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_SHIPPING_KEY_COSTS;

PROCEDURE GET_PRODUCTS
(
IN_SHIPPING_KEY  							IN FTD_APPS.PRODUCT_MASTER.SHIPPING_KEY%TYPE,
OUT_CUR                         		OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving record(s) from ftd_apps.product_master.

Input:
        none

Output:
        cursor containing all records from florist_status

-----------------------------------------------------------------------------*/
BEGIN

	OPEN OUT_CUR FOR
	SELECT product_id 
	  FROM ftd_apps.product_master 
	 WHERE shipping_key = in_shipping_key 
	 ORDER BY product_id;

	out_status := 'Y';
	out_message := NULL;
	
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_QUERY_PKG.GET_PRODUCTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_PRODUCTS;

PROCEDURE GET_SHIPPING_KEY_DETAIL_COST
(
IN_SHIPPING_KEY_ID						IN FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
OUT_CUR                         		OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving record(s) 
        from ftd_apps.shipping_key_details and shipping_key_costs
-----------------------------------------------------------------------------*/
BEGIN

	OPEN OUT_CUR FOR 
	 	SELECT distinct sd.shipping_detail_id, sd.min_price, sd.max_price, sc.shipping_cost, sc.shipping_method_id 
 			FROM ftd_apps.shipping_key_details sd 
 				join ftd_apps.shipping_key_costs sc on sd.shipping_detail_id = sc.shipping_key_detail_id 
 			WHERE shipping_key_id = IN_SHIPPING_KEY_ID 
		ORDER BY sd.shipping_detail_id;

	out_status := 'Y';
	out_message := NULL;
	
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.SHIPPING_KEYS_QUERY_PKG.GET_SHIPPING_KEY_DETAIL_COST [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_SHIPPING_KEY_DETAIL_COST;

END SHIPPING_KEYS_QUERY_PKG;
.
/
