CREATE OR REPLACE
PACKAGE BODY                   FTD_APPS.INV_TRK_PKG
AS

/******************************************************************************
                             INSERT_INPUT_ID_TEMP

Description: This function will insert vendor ids and product ids in the tempt table
       for the given comma delimited strings.

******************************************************************************/
PROCEDURE INSERT_INPUT_ID_TEMP
(
IN_PRODUCT_IDS            IN   VARCHAR2,
IN_VENDOR_IDS             IN   VARCHAR2
)
AS

-- variables
v_ids varchar2(1000);
v_id  varchar2(1000);
v_comma_pos number(3);

BEGIN
    DELETE FROM ftd_apps.inv_trk_product_temp;
    DELETE FROM ftd_apps.inv_trk_vendor_temp;

    IF IN_PRODUCT_IDS IS NOT NULL THEN
      v_ids := REPLACE(IN_PRODUCT_IDS,'''','');
      WHILE INSTR(v_ids,',') <> 0 LOOP
        v_comma_pos := INSTR(v_ids,',');
        v_id := RTRIM(LTRIM(SUBSTR(v_ids,1,v_comma_pos-1),' '),' ');
        v_ids := SUBSTR(v_ids, v_comma_pos+1);

        INSERT INTO FTD_APPS.INV_TRK_PRODUCT_TEMP values (RTRIM(LTRIM(v_id)));
      END LOOP;
      IF v_ids IS NOT NULL THEN
        INSERT INTO FTD_APPS.INV_TRK_PRODUCT_TEMP values (RTRIM(LTRIM(v_ids)));
      END IF;
    END IF;

    IF IN_VENDOR_IDS IS NOT NULL THEN
  v_ids := REPLACE(IN_VENDOR_IDS,'''','');
  WHILE instr(v_ids,',') <> 0 LOOP
    v_comma_pos := INSTR(v_ids,',');
    v_id := RTRIM(LTRIM(SUBSTR(v_ids,1,v_comma_pos-1),' '),' ');
    v_ids := SUBSTR(v_ids, v_comma_pos+1);

    INSERT INTO FTD_APPS.INV_TRK_VENDOR_TEMP values (RTRIM(LTRIM(v_id)));
  END LOOP;
  IF v_ids IS NOT NULL THEN
    INSERT INTO FTD_APPS.INV_TRK_VENDOR_TEMP values (RTRIM(LTRIM(v_ids)));
  END IF;
    END IF;



END INSERT_INPUT_ID_TEMP;

/******************************************************************************
                        SEARCH_INV_TRK

Description:

******************************************************************************/
PROCEDURE SEARCH_INV_TRK
(
IN_INV_TRK_ID                     IN FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
OUT_INV_TRK_CUR                  OUT TYPES.REF_CURSOR,
OUT_VENDOR_CUR                   OUT TYPES.REF_CURSOR,
OUT_EMAIL_CTRL_CUR               OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_INV_TRK_CUR FOR
    select a.inv_trk_id,a.inv_status, a.product_id, pm.product_name, pm.status product_status, pm.updated_by product_updated_by, to_char(pm.exception_start_date, 'mm-dd-yyyy') exception_start_date, a.vendor_id, vm.vendor_name, a.vendor_product_status, a.vendor_product_updated_by, a.warning_threshold_qty, a.shutdown_threshold_qty, to_char(a.start_date, 'MM/dd/yyyy') start_date, to_char(a.end_date, 'MM/dd/yyyy') end_date, a.forecast_qty, a.revised_forecast_qty, a.comments, a.ftd_msg_count, a.can_msg_count, a.rej_msg_count, a.shipped_shipping, a.on_hand
    from ftd_apps.product_master pm, ftd_apps.vendor_master vm,
    (
    select it.inv_trk_id,it.status inv_status, it.product_id, vp.available vendor_product_status, vp.updated_by vendor_product_updated_by, it.vendor_id, it.warning_threshold_qty, it.shutdown_threshold_qty, it.start_date, it.end_date, it.forecast_qty, it.revised_forecast_qty, it.comments, sum(itac.ftd_msg_count) ftd_msg_count, sum(itac.can_msg_count) can_msg_count, sum(itac.rej_msg_count ) rej_msg_count, (sum(itac.ftd_msg_count) - sum(itac.can_msg_count) - sum(itac.rej_msg_count )) shipped_shipping, (it.revised_forecast_qty - sum(itac.ftd_msg_count) + sum(itac.can_msg_count) + sum(itac.rej_msg_count )) on_hand
    from ftd_apps.inv_trk it, venus.inv_trk_assigned_count itac, ftd_apps.vendor_product vp
    where it.inv_trk_id = IN_INV_TRK_ID
    and it.vendor_id = itac.vendor_id
    and it.product_id = itac.product_id
    and itac.ship_date between it.start_date and it.end_date
    and it.vendor_id = vp.vendor_id
    and it.product_id = vp.product_subcode_id
    and itac.vendor_id = vp.vendor_id
    and itac.product_id = vp.product_subcode_id
    group by it.inv_trk_id,it.status, it.product_id, vp.available, vp.updated_by, it.vendor_id, it.warning_threshold_qty, it.shutdown_threshold_qty, it.start_date, it.end_date, it.forecast_qty, it.revised_forecast_qty, it.comments
    ) a
    where a.vendor_id = vm.vendor_id
    and a.product_id = pm.product_id;

  OPEN OUT_VENDOR_CUR FOR
    select vp.vendor_id, vm.vendor_name
    from ftd_apps.vendor_master vm, ftd_apps.vendor_product vp
    where vp.vendor_id = vm.vendor_id
    and vp.removed = 'N'
    and vm.active = 'Y'
    and vp.product_subcode_id = (select product_id from ftd_apps.inv_trk it where it.inv_trk_id = IN_INV_TRK_ID)
    order by vm.vendor_name;

  OPEN OUT_EMAIL_CTRL_CUR FOR
    select e.inv_trk_email_id, e.email_address
    from ftd_apps.inv_trk_inv_ctrl_email ce, ftd_apps.inv_trk_email e
    where ce.inv_trk_email_id = e.inv_trk_email_id
    and e.active_flag = 'Y'
    and ce.inv_trk_id = IN_INV_TRK_ID;

END SEARCH_INV_TRK;



/******************************************************************************
                             CAN_INV_TRK_BE_CREATED

Description: This procedure will check if the product/date combination can be
             added by checking if there are other records with different date ranges.
             If there are, it will not allow it.
             If everything looks ok, it will allow it.

******************************************************************************/
PROCEDURE CAN_INV_TRK_BE_CREATED
(
IN_PRODUCT_ID                     IN FTD_APPS.INV_TRK.PRODUCT_ID%TYPE,
IN_VENDOR_ID                      IN FTD_APPS.INV_TRK.VENDOR_ID%TYPE,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
OUT_CAN_BE_CREATED               OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2
)
AS

-- variables
v_exact_count1              number;
v_exact_count2              number;
v_range_count1              number;

-- Exceptions
END_DATE_NULL_VIOLATION     EXCEPTION;
INDEX_VIOLATION             EXCEPTION;
INVALID_INVENTORY_PERIOD    EXCEPTION;
VENDOR_REMOVED		    EXCEPTION;

BEGIN

OUT_CAN_BE_CREATED := 'N';
OUT_MESSAGE        := '';

--check if IN_END_DATE = null.  If it's null, raise an exception
IF IN_END_DATE is null THEN
  RAISE END_DATE_NULL_VIOLATION;
  
ELSE
  --Check for product, vendor, and dates in the INV_TRK table
  select count(*)
  into   v_exact_count1
  from   ftd_apps.inv_trk
  where  product_id = IN_PRODUCT_ID
  and    vendor_id = IN_VENDOR_ID
  and    start_date = IN_START_DATE
  and    end_date = IN_END_DATE;

  --If the EXACT record already exists
  IF v_exact_count1 > 0 THEN
    RAISE INDEX_VIOLATION;
  ELSE
    --Check for product, and dates in the INV_TRK table
    select count(*)
    into   v_exact_count2
    from   ftd_apps.inv_trk
    where  product_id = IN_PRODUCT_ID
    and    start_date = IN_START_DATE
    and    end_date = IN_END_DATE;

    --If IN_START_DATE and IN_END_DATE and IN_PRODUCT does not yield a record, user is trying add a record for a different inventory period
    IF v_exact_count2 = 0 THEN
      select count(*)
      into v_range_count1
      from
      (
        select distinct it.START_DATE, it.END_DATE
        from   FTD_APPS.INV_TRK it
        where  it.product_id = IN_PRODUCT_ID
      ) a
      where (   (IN_START_DATE between a.start_date and a.end_date)
             or (IN_END_DATE between a.start_date and a.end_date)
             or (IN_START_DATE <= a.start_date and IN_END_DATE >= a.end_date)
            );

      --If count > 0, IN_START_DATE and IN_END_DATE fall within other inventory period
      IF v_range_count1 > 0 THEN
        RAISE INVALID_INVENTORY_PERIOD;
      END IF;
    END IF;
  END IF;
END IF;

OUT_CAN_BE_CREATED      := 'Y';

EXCEPTION
  WHEN END_DATE_NULL_VIOLATION THEN
    OUT_MESSAGE := 'End Date cannot be null';

  WHEN INDEX_VIOLATION THEN
    OUT_MESSAGE := 'Record Already Exists';

  WHEN INVALID_INVENTORY_PERIOD THEN
    OUT_MESSAGE := 'Start and/or End date fall within other inventory period(s)';

  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN CAN_INV_TRK_BE_CREATED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END CAN_INV_TRK_BE_CREATED;


/******************************************************************************
                             CAN_INV_TRK_BE_EDITED

Description:

******************************************************************************/
PROCEDURE CAN_INV_TRK_BE_EDITED
(
IN_INV_TRK_ID                     IN FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
IN_PRODUCT_ID                     IN FTD_APPS.INV_TRK.PRODUCT_ID%TYPE,
IN_VENDOR_ID                      IN FTD_APPS.INV_TRK.VENDOR_ID%TYPE,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
OUT_CAN_BE_EDITED                OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2
)
AS


-- variables
v_exact_count1    number;
v_exact_count2    number;
v_range_count1    number;
v_vendor_count    number;

v_record_found    varchar2(1);

v_inv_trk_id      FTD_APPS.INV_TRK.INV_TRK_ID%TYPE;
v_vendor_id       FTD_APPS.INV_TRK.VENDOR_ID%TYPE;
v_start_date      FTD_APPS.INV_TRK.START_DATE%TYPE;
v_end_date        FTD_APPS.INV_TRK.END_DATE%TYPE;

-- cursors
OUT_VENDOR_CUR1   types.ref_cursor;
OUT_VENDOR_CUR2   types.ref_cursor;
OUT_DATE_CUR      types.ref_cursor;

-- Exceptions
END_DATE_NULL_VIOLATION     EXCEPTION;
INVENTORY_RECORD_EXIST      EXCEPTION;
INVALID_INVENTORY_PERIOD    EXCEPTION;

BEGIN

OUT_CAN_BE_EDITED := 'N';
OUT_MESSAGE := '';

--check if IN_END_DATE = null.  If it's null, raise an exception
IF IN_END_DATE is null THEN
  RAISE END_DATE_NULL_VIOLATION;
ELSE

  select count(*)
  into   v_exact_count1
  from   ftd_apps.inv_trk it
  where  it.inv_trk_id = IN_INV_TRK_ID
  and    it.product_id = IN_PRODUCT_ID
  and    it.start_date = IN_START_DATE
  and    it.end_date = IN_END_DATE
  and    it.vendor_id = IN_VENDOR_ID;

  --if v_count = 1, user is trying to update revised etc... update allowed = Y.
  --if v_count != 1, user is updating dates
  IF v_exact_count1 != 1 THEN
    --check to see if there exists an inventory record for this product for this date.
    select count(it.vendor_id)
    into   v_exact_count2
    from   ftd_apps.inv_trk it
    where  it.product_id = IN_PRODUCT_ID
    and    it.start_date = IN_START_DATE
    and    it.end_date = IN_END_DATE;

    --if count > 0, an inventory record is found for this date for this product, in which case, check if the vendor
    --id exist for that date range.  If it does, stop the user from creating this record.
    IF v_exact_count2 > 0 THEN
      v_record_found := 'N';

      OPEN OUT_VENDOR_CUR1 FOR
        select it.vendor_id
        from   ftd_apps.inv_trk it
        where  it.product_id = IN_PRODUCT_ID
        and    it.start_date = IN_START_DATE
        and    it.end_date = IN_END_DATE;

      LOOP
        FETCH OUT_VENDOR_CUR1 INTO v_vendor_id;
        EXIT WHEN OUT_VENDOR_CUR1%NOTFOUND;
        IF v_vendor_id = IN_VENDOR_ID THEN
          v_record_found := 'Y';
          EXIT;
        END IF;
      END LOOP;
      CLOSE OUT_VENDOR_CUR1;

      --if vendor is found, stop the user from updating
      IF v_record_found = 'Y' THEN
        RAISE INVENTORY_RECORD_EXIST;
      END IF;
    --if count = 0, an inventory record is NOT found.  This means that the user has either entered a new inventory date period,
    --or the new dates entered fall WITHIN existing inventory period.  If former, allow.  If latter, prevent.
    ELSE
      --find a start date such that the user-entered start and end dates fall in that inventory period.
      select count(distinct a.start_date)
      into   v_range_count1
      from
            (
              select it.INV_TRK_ID, it.PRODUCT_ID, it.VENDOR_ID, it.START_DATE, it.END_DATE
              from   ftd_apps.inv_trk it
              where  it.product_id = IN_PRODUCT_ID
            ) a
      where (     (IN_START_DATE between a.start_date and a.end_date)
              or  (IN_END_DATE between a.start_date and a.end_date)
              or  (IN_START_DATE <= a.start_date and IN_END_DATE >= a.end_date)
            );

      --if user-entered start and end dates fall in multiple ranges, do not allow the record to be edited.
      IF v_range_count1 > 1 THEN
        RAISE INVENTORY_RECORD_EXIST;

      --if user-entered start and end dates fall in one range ONLY, check if that vendor already exists for this inventory period
      ELSIF v_range_count1 = 1 THEN
        select distinct a.start_date, a.end_date
        into   v_start_date, v_end_date
        from
              (
                select it.INV_TRK_ID, it.PRODUCT_ID, it.VENDOR_ID, it.START_DATE, it.END_DATE
                from   ftd_apps.inv_trk it
                where  it.product_id = IN_PRODUCT_ID
              ) a
        where (     (IN_START_DATE between a.start_date and a.end_date)
                or  (IN_END_DATE between a.start_date and a.end_date)
                or  (IN_START_DATE <= a.start_date and IN_END_DATE >= a.end_date)
              );

        select count(it.vendor_id)
        into   v_vendor_count
        from   ftd_apps.inv_trk it
        where  it.product_id = IN_PRODUCT_ID
        and    it.start_date = v_start_date
        and    it.end_date = v_end_date;

        --if user-entered start and end dates fall in one range ONLY, check # of vendors within that range.
        --if multiple vendors exist, do NOT allow the record to be edited
        IF v_vendor_count > 1 THEN
          RAISE INVENTORY_RECORD_EXIST;

        --if user-entered start and end dates fall in one range ONLY, and there is only vendor within that range,
        --check if the user is editing THAT record, or trying to make another record override that INV_TRK_ID.
        --if former, allow; else, prevent
        ELSE
          v_record_found := 'N';

          OPEN OUT_VENDOR_CUR2 FOR
            select it.inv_trk_id, it.vendor_id
            from   ftd_apps.inv_trk it
            where  it.product_id = IN_PRODUCT_ID
            and    it.start_date = v_start_date
            and    it.end_date = v_end_date;

          LOOP
            FETCH OUT_VENDOR_CUR2 INTO v_inv_trk_id, v_vendor_id;
            EXIT WHEN OUT_VENDOR_CUR2%NOTFOUND;
            IF v_vendor_id = IN_VENDOR_ID AND v_inv_trk_id = IN_INV_TRK_ID THEN
              v_record_found := 'Y';
            EXIT;
            END IF;
          END LOOP;
          CLOSE OUT_VENDOR_CUR2;

          IF v_record_found = 'N' THEN
            RAISE INVENTORY_RECORD_EXIST;
          END IF;
        END IF;
      END IF;
    END IF;
  END IF;
END IF;

OUT_CAN_BE_EDITED := 'Y';

EXCEPTION
  WHEN END_DATE_NULL_VIOLATION THEN
    OUT_MESSAGE := 'End date cannot be null';

  WHEN INVENTORY_RECORD_EXIST THEN
    OUT_MESSAGE := 'Another record with these characteristics already exist.  Please delete that record first before modifying this record.';

  WHEN INVALID_INVENTORY_PERIOD THEN
    OUT_MESSAGE := 'Start and/or End date fall within other inventory period(s)';

  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN CAN_INV_TRK_BE_EDITED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END CAN_INV_TRK_BE_EDITED;


/*----------------------------------------------------------------------
CREATE_INVENTORY

-----------------------------------------------------------------------*/
PROCEDURE CREATE_INVENTORY
(
IN_PRODUCT_ID                     IN FTD_APPS.INV_TRK.PRODUCT_ID%TYPE,
IN_VENDOR_ID                      IN FTD_APPS.INV_TRK.VENDOR_ID%TYPE,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_FORECAST_QTY                   IN FTD_APPS.INV_TRK.FORECAST_QTY%TYPE,
IN_WARNING_THRESHOLD_QTY          IN FTD_APPS.INV_TRK.WARNING_THRESHOLD_QTY%TYPE,
IN_SHUTDOWN_THRESHOLD_QTY         IN FTD_APPS.INV_TRK.SHUTDOWN_THRESHOLD_QTY%TYPE,
IN_COMMENTS                       IN FTD_APPS.INV_TRK.COMMENTS%TYPE,
IN_CREATED_BY                     IN FTD_APPS.INV_TRK.CREATED_BY%TYPE,
OUT_INV_TRK_ID                   OUT FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
OUT_STATUS                       OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2,
OUT_ERR_CODE					 OUT NUMBER
)
AS

-- variables
v_record_can_be_created          varchar2(1);
v_message                        varchar2(4000);
v_product_count                  number;
v_product_vendor_count           number;
v_removed_vendor_flag	         varchar2(10);

-- Exceptions
RECORD_CANNOT_BE_CREATED         EXCEPTION;
VENDOR_REMOVED		         EXCEPTION;

BEGIN

OUT_INV_TRK_ID              := 0;
OUT_STATUS                  := 'N';
OUT_MESSAGE                 := '';

  --check if the add is even allowed.
  CAN_INV_TRK_BE_CREATED(IN_PRODUCT_ID, IN_VENDOR_ID, IN_START_DATE, IN_END_DATE, v_record_can_be_created, v_message);

  --If the record can be created, create the reecord, else raise an exception
  IF v_record_can_be_created = 'N' THEN
    RAISE RECORD_CANNOT_BE_CREATED;
  ELSE
    
    select removed into v_removed_vendor_flag from ftd_apps.vendor_product where vendor_id=IN_VENDOR_ID and product_subcode_id=IN_PRODUCT_ID;
    IF v_removed_vendor_flag = 'Y' THEN
      --check if the vendor is in removed state.
        RAISE VENDOR_REMOVED;
    END IF;
    --get the next sequence value
    select ftd_apps.inv_trk_sq.nextval into OUT_INV_TRK_ID from dual;

    -- Create inv_trk record
    insert into ftd_apps.inv_trk
    (
       inv_trk_id
      ,product_id
      ,vendor_id
      ,start_date
      ,end_date
      ,forecast_qty
      ,revised_forecast_qty
      ,warning_threshold_qty
      ,shutdown_threshold_qty
      ,comments
      ,created_by
      ,created_on
      ,updated_by
      ,updated_on
    )
    values
    (
       OUT_INV_TRK_ID
      ,IN_PRODUCT_ID
      ,IN_VENDOR_ID
      ,IN_START_DATE
      ,IN_END_DATE
      ,IN_FORECAST_QTY
      ,IN_FORECAST_QTY
      ,IN_WARNING_THRESHOLD_QTY
      ,IN_SHUTDOWN_THRESHOLD_QTY
      ,IN_COMMENTS
      ,IN_CREATED_BY
      ,sysdate
      ,IN_CREATED_BY
      ,sysdate
    );

    -- Create inv_trk_inv_ctrl_email records
    insert into ftd_apps.inv_trk_inv_ctrl_email
    (
       inv_trk_id
      ,inv_trk_email_id
      ,created_by
      ,created_on
      ,updated_by
      ,updated_on
    )
    select OUT_INV_TRK_ID, ite.INV_TRK_EMAIL_ID, IN_CREATED_BY, sysdate, IN_CREATED_BY, sysdate
    from   ftd_apps.inv_trk_email ite, ftd_apps.inv_trk_vendor_email itve
    where  ite.inv_trk_email_id = itve.inv_trk_email_id
    and    ite.active_flag = 'Y'
    and    itve.vendor_id = IN_VENDOR_ID;

    -- create inv_trk_count record
    select count(*)
    into   v_product_count
    from   venus.inv_trk_count
    where  product_id = IN_PRODUCT_ID
    and    ship_date = IN_START_DATE;

    IF v_product_count = 0 THEN
      insert into venus.inv_trk_count
      (
         product_id
        ,ship_date
        ,ftd_msg_count
        ,can_msg_count
        ,rej_msg_count
      )
      values
      (
         IN_PRODUCT_ID
        ,IN_START_DATE
        ,0
        ,0
        ,0
      );
    END IF;

    -- create inv_trk_assigned_count record
    select count(*)
    into   v_product_vendor_count
    from   venus.inv_trk_assigned_count
    where  product_id = IN_PRODUCT_ID
    and    vendor_id = IN_VENDOR_ID
    and    ship_date = IN_START_DATE;

    IF v_product_vendor_count = 0 THEN
      insert into venus.inv_trk_assigned_count
      (
         product_id
        ,vendor_id
        ,ship_date
        ,ftd_msg_count
        ,can_msg_count
        ,rej_msg_count
      )
      values
      (
         IN_PRODUCT_ID
        ,IN_VENDOR_ID
        ,IN_START_DATE
        ,0
        ,0
        ,0
      );
    END IF;
  END IF;

OUT_STATUS := 'Y';

EXCEPTION
  WHEN RECORD_CANNOT_BE_CREATED THEN
    OUT_MESSAGE := v_message;
    OUT_ERR_CODE := 0;

  WHEN VENDOR_REMOVED THEN
	OUT_MESSAGE := 'Vendor is removed.';
        
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN CREATE_INVENTORY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    OUT_ERR_CODE := 1;


END CREATE_INVENTORY;



/*----------------------------------------------------------------------
UPDATE_INVENTORY

-----------------------------------------------------------------------*/
PROCEDURE UPDATE_INVENTORY
(
IN_INV_TRK_ID                     IN FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_REVISED_FORECAST               IN FTD_APPS.INV_TRK.REVISED_FORECAST_QTY%TYPE,
IN_WARNING_THRESHOLD              IN FTD_APPS.INV_TRK.WARNING_THRESHOLD_QTY%TYPE,
IN_SHUTDOWN_THRESHOLD             IN FTD_APPS.INV_TRK.SHUTDOWN_THRESHOLD_QTY%TYPE,
IN_COMMENTS                       IN FTD_APPS.INV_TRK.COMMENTS%TYPE,
IN_EMAIL_IDS_TO_DELETE            IN VARCHAR2,
IN_NEW_EMAIL_ADDRESS              IN FTD_APPS.INV_TRK_EMAIL.EMAIL_ADDRESS%TYPE,
IN_UPDATED_BY                     IN FTD_APPS.INV_TRK.UPDATED_BY%TYPE,
OUT_STATUS                       OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2,
OUT_ERR_CODE					 OUT NUMBER
)
AS

-- variables
v_product_id                     FTD_APPS.INV_TRK.PRODUCT_ID%TYPE;
v_vendor_id                      FTD_APPS.INV_TRK.VENDOR_ID%TYPE;
v_inv_trk_email_id               FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE;
v_record_can_be_edited           varchar2(1);
v_message                        varchar2(4000);
v_email_count                    number;
v_email_ctrl_count               number;
v_product_count                  number;
v_product_vendor_count           number;

-- Exceptions
RECORD_CANNOT_BE_EDITED          EXCEPTION;
MULTIPLE_ACTIVE_EMAILS           EXCEPTION;

--sqls
v_sql                            varchar2(4000);

BEGIN

OUT_STATUS                  := 'N';
OUT_MESSAGE                 := '';

  --select the product id and vendor id based on inv_trk_id.  Note that the calling method is NOT passing it because
  --we wanted to avoid someone calling this stored proc and updating product-id and vendor-id
  select product_id, vendor_id
  into   v_product_id, v_vendor_id
  from   ftd_apps.inv_trk
  where  inv_trk_id = IN_INV_TRK_ID;

  --check if the edit is even allowed.
  CAN_INV_TRK_BE_EDITED(IN_INV_TRK_ID, v_product_id, v_vendor_id, IN_START_DATE, IN_END_DATE, v_record_can_be_edited, v_message);

  --If the record can be created, create the reecord, else raise an exception
  IF v_record_can_be_edited = 'N' THEN
    RAISE RECORD_CANNOT_BE_EDITED;
  ELSE

    --**************************** INV_TRK *****************************

    -- Updated the inv_trk record
    update ftd_apps.inv_trk
    set    start_date               = IN_START_DATE
          ,end_date                 = IN_END_DATE
          ,revised_forecast_qty     = IN_REVISED_FORECAST
          ,warning_threshold_qty    = IN_WARNING_THRESHOLD
          ,shutdown_threshold_qty   = IN_SHUTDOWN_THRESHOLD
          ,comments                 = IN_COMMENTS
          ,updated_by               = IN_UPDATED_BY
          ,updated_on               = sysdate
    where  inv_trk_id               = IN_INV_TRK_ID;

    --********************** IN_EMAIL_IDS_TO_DELETE ********************
    --If IN_EMAIL_IDS_TO_DELETE not null, delete the inv_trk_inv_ctrl_email records
    IF IN_EMAIL_IDS_TO_DELETE is not null THEN
      v_sql := 'delete from ftd_apps.inv_trk_inv_ctrl_email where inv_trk_id = ' || IN_INV_TRK_ID || ' and to_char(inv_trk_email_id) in (' || IN_EMAIL_IDS_TO_DELETE || ')';
      execute immediate v_sql;
    END IF;


    --********************** IN_NEW_EMAIL_ADDRESS **********************
    -- If IN_NEW_EMAIL_ADDRESS is not null
    --  1) check inv_trk_email to see if an Active email address already exists
    --    a) if not, create one
    --    b) if yes, use it
    --    c) if multiple, raise an error
    --  2) create a inv_trk_inv_ctrl_email record
    IF IN_NEW_EMAIL_ADDRESS is not null THEN
      select count(*)
      into   v_email_count
      from   ftd_apps.inv_trk_email
      where  lower(email_address) = lower(IN_NEW_EMAIL_ADDRESS)
      and    active_flag = 'Y';

      IF v_email_count > 1 THEN
        RAISE MULTIPLE_ACTIVE_EMAILS;
      ELSIF v_email_count = 1 THEN
        select inv_trk_email_id
        into   v_inv_trk_email_id
        from   ftd_apps.inv_trk_email
        where  lower(email_address) = lower(IN_NEW_EMAIL_ADDRESS)
        and    active_flag = 'Y';
      ELSE
        select ftd_apps.inv_trk_email_sq.nextval into v_inv_trk_email_id from dual;

        insert into ftd_apps.inv_trk_email
        (
           inv_trk_email_id
          ,email_address
          ,active_flag
          ,created_by
          ,created_on
          ,updated_by
          ,updated_on
        )
        values
        (
           v_inv_trk_email_id
          ,lower(IN_NEW_EMAIL_ADDRESS)
          ,'Y'
          ,IN_UPDATED_BY
          ,sysdate
          ,IN_UPDATED_BY
          ,sysdate
        );
      END IF;

      select count(*)
      into   v_email_ctrl_count
      from   ftd_apps.inv_trk_inv_ctrl_email
      where  inv_trk_email_id = v_inv_trk_email_id
      and    inv_trk_id = IN_INV_TRK_ID;

      IF v_email_ctrl_count = 0 THEN
        insert into ftd_apps.inv_trk_inv_ctrl_email
        (
           inv_trk_id
          ,inv_trk_email_id
          ,created_by
          ,created_on
          ,updated_by
          ,updated_on
        )
        values
        (
           IN_INV_TRK_ID
          ,v_inv_trk_email_id
          ,IN_UPDATED_BY
          ,sysdate
          ,IN_UPDATED_BY
          ,sysdate
        );
      END IF;
    END IF;


    --************************* INV_TRK_COUNT **************************
    -- create inv_trk_count record
    select count(*)
    into   v_product_count
    from   venus.inv_trk_count
    where  product_id = v_product_id
    and    ship_date = IN_START_DATE;

    IF v_product_count = 0 THEN
      insert into venus.inv_trk_count
      (
         product_id
        ,ship_date
        ,ftd_msg_count
        ,can_msg_count
        ,rej_msg_count
      )
      values
      (
         v_product_id
        ,IN_START_DATE
        ,0
        ,0
        ,0
      );
    END IF;


    --********************* INV_TRK_ASSIGNED_COUNT *********************
    -- create inv_trk_assigned_count record
    select count(*)
    into   v_product_vendor_count
    from   venus.inv_trk_assigned_count
    where  product_id = v_product_id
    and    vendor_id = v_vendor_id
    and    ship_date = IN_START_DATE;

    IF v_product_vendor_count = 0 THEN
      insert into venus.inv_trk_assigned_count
      (
         product_id
        ,vendor_id
        ,ship_date
        ,ftd_msg_count
        ,can_msg_count
        ,rej_msg_count
      )
      values
      (
         v_product_id
        ,v_vendor_id
        ,IN_START_DATE
        ,0
        ,0
        ,0
      );
    END IF;
  END IF;

OUT_STATUS := 'Y';

EXCEPTION
  WHEN RECORD_CANNOT_BE_EDITED THEN
    OUT_MESSAGE := v_message;

  WHEN MULTIPLE_ACTIVE_EMAILS THEN
    OUT_MESSAGE := 'Multiple active emails found';
     OUT_ERR_CODE:= 0;

  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN CREATE_INVENTORY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    OUT_ERR_CODE:= 1;


END UPDATE_INVENTORY;


/*----------------------------------------------------------------------
DELETE_INVENTORY

-----------------------------------------------------------------------*/
PROCEDURE DELETE_INVENTORY
(
IN_INV_TRK_ID                     IN FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
OUT_STATUS                       OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2
)
AS

BEGIN

OUT_STATUS        := 'N';
OUT_MESSAGE       := '';

delete
from   ftd_apps.inv_trk_inv_ctrl_email
where  inv_trk_id = IN_INV_TRK_ID;

delete
from   ftd_apps.inv_trk
where  inv_trk_id = IN_INV_TRK_ID;

OUT_STATUS      := 'Y';

EXCEPTION
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN DELETE_INVENTORY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_INVENTORY;


/*----------------------------------------------------------------------
RETRIEVE_PRODUCT_IDS

-----------------------------------------------------------------------*/
PROCEDURE RETRIEVE_PRODUCT_IDS
(
IN_INV_TRK_IDS                    IN VARCHAR2,
OUT_CUR                          OUT TYPES.REF_CURSOR
)
AS

--sqls
v_sql                            varchar2(4000);

BEGIN

v_sql := 'select distinct product_id from ftd_apps.inv_trk where inv_trk_id in (' || IN_INV_TRK_IDS || ')';

OPEN OUT_CUR FOR v_sql;

END RETRIEVE_PRODUCT_IDS;


/*----------------------------------------------------------------------
RETRIEVE_PRODUCT_VENDOR

-----------------------------------------------------------------------*/
PROCEDURE RETRIEVE_PRODUCT_VENDOR
(
IN_INV_TRK_IDS                    IN VARCHAR2,
OUT_CUR                          OUT TYPES.REF_CURSOR
)
AS

--sqls
v_sql                            varchar2(4000);

BEGIN

v_sql := 'select distinct it.product_id, it.vendor_id, it.inv_trk_id, it.status inv_status, vp.available vp_available, 
			vp.removed, pm.status, pm.product_type, pm.ship_method_carrier, pm.ship_method_florist
		  	from ftd_apps.inv_trk it, ftd_apps.vendor_product vp, ftd_apps.product_master pm
		  	where pm.product_id = it.product_id and vp.product_subcode_id = pm.product_id
		  	and vp.vendor_id = it.vendor_id and it.inv_trk_id in (' || IN_INV_TRK_IDS || ')'; 

OPEN OUT_CUR FOR v_sql;

END RETRIEVE_PRODUCT_VENDOR;


/*----------------------------------------------------------------------
UPDATE_SHUTDOWN_THRESHOLD

-----------------------------------------------------------------------*/
PROCEDURE UPDATE_SHUTDOWN_THRESHOLD
(
IN_INV_TRK_IDS                    IN VARCHAR2,
IN_PRODUCT_ID                     IN FTD_APPS.INV_TRK.PRODUCT_ID%TYPE,
IN_VENDOR_ID                      IN FTD_APPS.INV_TRK.VENDOR_ID%TYPE,
IN_UPDATED_BY                     IN FTD_APPS.INV_TRK.UPDATED_BY%TYPE,
OUT_STATUS                       OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2
)
AS

--sqls
v_sql                            varchar2(4000);

BEGIN

OUT_STATUS                  := 'N';
OUT_MESSAGE                 := '';

v_sql := 'update ftd_apps.inv_trk set shutdown_threshold_qty = null, updated_by = ''' || IN_UPDATED_BY || ''', updated_on = sysdate where to_char(inv_trk_id) in (' || IN_INV_TRK_IDS || ') and product_id = ''' || IN_PRODUCT_ID || ''' and vendor_id = ''' || IN_VENDOR_ID || '''';
execute immediate v_sql;

OUT_STATUS                  := 'Y';


EXCEPTION
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_SHUTDOWN_THRESHOLD [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPDATE_SHUTDOWN_THRESHOLD;


/*----------------------------------------------------------------------
ORDERS_TAKEN_INFO

-----------------------------------------------------------------------*/
PROCEDURE ORDERS_TAKEN_INFO
(
IN_PRODUCT_ID           IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
OUT_WEEK_TOTAL         OUT NUMBER,
OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS

cursor c7_cur is
  select  count(1)
  from    venus.venus v
  where   v.msg_type = 'FTD'
  and     v.product_id = IN_PRODUCT_ID
  and     v.order_date >= trunc(sysdate - 6)
  and     not exists
          (
          select  1
          from    venus.venus v2
          where   v2.venus_order_number = v.venus_order_number
          and     v2.msg_type in ('CAN','REJ')
          );

cursor daily_cur (p_date date)is
  select  count(1)
  from    venus.venus v
  where   v.msg_type = 'FTD'
  and     v.product_id = IN_PRODUCT_ID
  and     trunc (v.order_date) = trunc(p_date)
  and     not exists
          (
          select  1
          from    venus.venus v2
          where   v2.venus_order_number = v.venus_order_number
          and     v2.msg_type in ('CAN', 'REJ')
          );


v_date          date;
v_day_name      varchar2(30);
v_count         integer := 0;


BEGIN

-- initialize the out parms
out_week_total := 0;

-- get the count of order delivered in the last 7 days
open c7_cur;
fetch c7_cur into out_week_total;
close c7_cur;

-- get the daily counts
delete from rep_tab;

FOR i IN 0..6
  LOOP
    v_date := sysdate - i;
    v_day_name:= to_char((sysdate - i), 'Day') ;

    OPEN daily_cur (v_date);
    FETCH daily_cur INTO v_count;
    CLOSE daily_cur;

    insert into rep_tab
    (
      col1,
      col2,
      col3,
      col4
    )
    values
    (
      to_char(v_date,'mm-dd-yyyy'),
      v_day_name,
      v_count,
      to_char(v_date,'D')
    );

  END LOOP;

OPEN out_cursor FOR
select  col1 "date", col2 "day", col3 "row_count"
from    rep_tab
order by col4;

END ORDERS_TAKEN_INFO;


PROCEDURE INSERT_INV_TRK_EVENT_HISTORY
(
  IN_INV_TRK_ID                  IN FTD_APPS.INV_TRK_EVENT_HISTORY.INV_TRK_ID%TYPE,
  IN_EVENT_TYPE                IN FTD_APPS.INV_TRK_EVENT_HISTORY.EVENT_TYPE%TYPE,
  IN_UPDATED_BY                  IN FTD_APPS.INV_TRK_EVENT_HISTORY.UPDATED_BY%TYPE,
  IN_PRODUCT_AVAIL_START_DATE    IN FTD_APPS.INV_TRK_EVENT_HISTORY.PRODUCT_AVAIL_START_DATE%TYPE,
  IN_PRODUCT_AVAIL_END_DATE      IN FTD_APPS.INV_TRK_EVENT_HISTORY.PRODUCT_AVAIL_END_DATE%TYPE,
  OUT_STATUS                     OUT VARCHAR2,
  OUT_MESSAGE                    OUT VARCHAR2
  )
AS
/*----------------------------------------------------------------------
Description:
        The proc will insert a record into the ftd_apps.inv_trk_event_history table.

Input:
  IN_INV_TRK_ID                  IN FTD_APPS.INV_TRK_EVENT_HISTORY.INV_TRK_ID%TYPE,
    IN_EVENT_TYPE                  IN FTD_APPS.INV_TRK_EVENT_HISTORY.EVENT_TYPE%TYPE,
    IN_UPDATED_BY                  IN FTD_APPS.INV_TRK_EVENT_HISTORY.UPDATED_BY%TYPE,
    IN_PRODUCT_AVAIL_START_DATE    IN FTD_APPS.INV_TRK_EVENT_HISTORY.PRODUCT_AVAIL_START_DATE%TYPE,
    IN_PRODUCT_AVAIL_END_DATE      IN FTD_APPS.INV_TRK_EVENT_HISTORY.PRODUCT_AVAIL_END_DATE%TYPE,

Output:
  OUT_INV_TRK_ID  OUT FTD_APPS.INV_TRK.INV_TRK_ID%TYPE
  OUT_STATUS      OUT VARCHAR2
  OUT_MESSAGE     OUT VARCHAR2

-----------------------------------------------------------------------*/
v_current_timestamp timestamp;
BEGIN

      v_current_timestamp := current_timestamp;

      INSERT INTO FTD_APPS.INV_TRK_EVENT_HISTORY
      (
          INV_TRK_ID,
          EVENT_TIMESTAMP,
          EVENT_TYPE,
          UPDATED_BY,
          PRODUCT_AVAIL_START_DATE,
          PRODUCT_AVAIL_END_DATE
      )
      VALUES
      (
          IN_INV_TRK_ID,
          v_current_timestamp,
          IN_EVENT_TYPE,
          IN_UPDATED_BY,
          IN_PRODUCT_AVAIL_START_DATE,
          IN_PRODUCT_AVAIL_END_DATE
      );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_INV_TRK_EVENT_HISTORY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_INV_TRK_EVENT_HISTORY;

PROCEDURE DELETE_INV_TRK_EMAIL
(
IN_EMAIL_ADDRESS                IN FTD_APPS.INV_TRK_EMAIL.EMAIL_ADDRESS%TYPE,
IN_UPDATED_BY                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for setting the email address
        to inactive.

Input:
        in_email_address
        in_updated_by                   varchar2 (FLORIST_MASTER, MERCURY)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_inv_trk_email_id               FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE;

BEGIN

  --Obtain the email id
  SELECT_INV_TRK_EMAIL(IN_EMAIL_ADDRESS, v_inv_trk_email_id);

  --If the email address doesn't exist return an error message
  IF(v_inv_trk_email_id IS NULL) THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'Email address does not exist.';
  --If the email address exists set it to inactive
  ELSE
    UPDATE ftd_apps.inv_trk_email ite
    SET active_flag = 'N',
    updated_on = SYSDATE,
    updated_by = IN_UPDATED_BY
    WHERE ite.inv_trk_email_id = v_inv_trk_email_id;
  END IF;

END DELETE_INV_TRK_EMAIL;


PROCEDURE GET_VENDOR_BY_INV_TRK_EMAIL
(
IN_EMAIL_ADDRESS                IN FTD_APPS.INV_TRK_EMAIL.EMAIL_ADDRESS%TYPE,
OUT_VENDOR_AVAIL_CUR            IN OUT TYPES.REF_CURSOR,
OUT_VENDOR_CHOSEN_CUR           IN OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for obtaining default vendors
        associated with an email address and all active vendors that are
        not associated with the email address.

Input:
        in_email_address

Output:
        out_vendor_avail_cur
        out_vendor_chosen_cur

-----------------------------------------------------------------------------*/

v_inv_trk_email_id               FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE;

BEGIN
  --Obtain the email id
  SELECT_INV_TRK_EMAIL(IN_EMAIL_ADDRESS, v_inv_trk_email_id);

  --Obtain vendors associated with the email address
  OPEN OUT_VENDOR_CHOSEN_CUR FOR
    SELECT vm.vendor_id, TRIM(vm.vendor_name) vendor_name
    FROM ftd_apps.vendor_master vm
    JOIN ftd_apps.inv_trk_vendor_email itve
    ON itve.vendor_id = vm.vendor_id
    AND itve.inv_trk_email_id = v_inv_trk_email_id
    ORDER BY TRIM(vm.vendor_name) asc;

  --Obtain all active vendors not associated with the email address

  OPEN OUT_VENDOR_AVAIL_CUR FOR
    SELECT vm.vendor_id, TRIM(vm.vendor_name) vendor_name
    FROM ftd_apps.vendor_master vm
    WHERE vm.active = 'Y'
    AND vm.vendor_id not in
      (
        SELECT itve.vendor_id
        FROM ftd_apps.inv_trk_vendor_email itve
        WHERE itve.inv_trk_email_id = v_inv_trk_email_id
      )
    ORDER BY TRIM(vm.vendor_name) asc;

END GET_VENDOR_BY_INV_TRK_EMAIL;


PROCEDURE INSERT_INV_TRK_EMAIL
(
IN_EMAIL_ADDRESS                IN FTD_APPS.INV_TRK_EMAIL.EMAIL_ADDRESS%TYPE,
IN_UPDATED_BY                   IN VARCHAR2,
OUT_INV_TRK_EMAIL_ID            OUT FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting an inventory tracking
        email address.  If the active email address already exists return
        the latest inv_trk_email_id.

Input:
        in_email_address
        in_updated_by

Output:
        out_inv_trk_email_id
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  SELECT MAX(ite.inv_trk_email_id)
  INTO OUT_INV_TRK_EMAIL_ID
  FROM ftd_apps.inv_trk_email ite
  WHERE LOWER(ite.email_address) = LOWER(IN_EMAIL_ADDRESS)
  AND ite.active_flag = 'Y';

  IF(OUT_INV_TRK_EMAIL_ID IS NULL) THEN
    SELECT ftd_apps.inv_trk_email_sq.nextval INTO OUT_INV_TRK_EMAIL_ID FROM dual;
    INSERT INTO ftd_apps.inv_trk_email
    (inv_trk_email_id, email_address, active_flag, created_on, created_by, updated_on, updated_by)
    VALUES(OUT_INV_TRK_EMAIL_ID, LOWER(IN_EMAIL_ADDRESS), 'Y', SYSDATE, IN_UPDATED_BY, SYSDATE, IN_UPDATED_BY);
  END IF;

  EXCEPTION
  WHEN OTHERS THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_INV_TRK_EMAIL;


PROCEDURE SAVE_INV_TRK_EMAIL
(
IN_EMAIL_ADDRESS                IN FTD_APPS.INV_TRK_EMAIL.EMAIL_ADDRESS%TYPE,
IN_VENDOR_ID_LIST               IN VARCHAR2,
IN_APPLY_TO_CURRENT_FLAG        IN CHAR,
IN_UPDATED_BY                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for modifying default vendors associated
        with an email address and modifying which inventory tracking records
        the email address is associated with.

Input:
        in_email_address
        in_vendor_id_list               varchar2
        in_apply_to_current_flag        char
        in_updated_by                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
--Obtain all inventory tracking records associated with a given vendor that are not already associated with a given email address
cursor inv_trk_id (p_vendor_id FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE, p_inv_trk_email_id FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE) is
  SELECT  it.inv_trk_id
  FROM    ftd_apps.inv_trk it
  WHERE   it.vendor_id = p_vendor_id
  AND     it.end_date >= TRUNC(SYSDATE)
  AND NOT EXISTS
    (
      SELECT 1
      FROM ftd_apps.inv_trk_inv_ctrl_email itice
      WHERE itice.inv_trk_id = it.inv_trk_id
      AND itice.inv_trk_email_id = p_inv_trk_email_id
    );

v_inv_trk_email_id               FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE;
v_vendor_id                      FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE;
v_vendor_list                    VARCHAR2(4000);

-- used for dynamic sql
v_insert_sql                     VARCHAR2(4000);
v_delete_sql                     VARCHAR2(4000);

-- cursors
insert_cur                       types.ref_cursor;
delete_cur                       types.ref_cursor;


BEGIN
  --Obtain the email id
  SELECT_INV_TRK_EMAIL(IN_EMAIL_ADDRESS, v_inv_trk_email_id);

  --If the email address doesn't exist create it
  IF(v_inv_trk_email_id IS NULL) THEN
    INSERT_INV_TRK_EMAIL(IN_EMAIL_ADDRESS, IN_UPDATED_BY, v_inv_trk_email_id, OUT_STATUS, OUT_MESSAGE);
  END IF;

  IF(OUT_STATUS IS NULL) THEN
    --If no vendor id data exists use a blank space so that the dynamic SQL doesn't error
    IF(LENGTH(IN_VENDOR_ID_LIST) > 0) THEN
      v_vendor_list := IN_VENDOR_ID_LIST;
    ELSE
      v_vendor_list := '-1';
    END IF;

    --Determine which vendors need to be associated with the email address
    v_insert_sql := 'SELECT vm.vendor_id FROM ftd_apps.vendor_master vm WHERE vm.vendor_id IN (' || v_vendor_list || ') AND vm.vendor_id NOT IN (SELECT ite.vendor_id FROM ftd_apps.inv_trk_vendor_email ite WHERE ite.inv_trk_email_id = ' || v_inv_trk_email_id || ')';
    OPEN insert_cur FOR v_insert_sql;
    LOOP
      FETCH insert_cur INTO v_vendor_id;
      EXIT WHEN insert_cur%NOTFOUND;
      INSERT INTO ftd_apps.inv_trk_vendor_email itve
      (vendor_id, inv_trk_email_id, created_on, created_by, updated_on, updated_by)
      VALUES(v_vendor_id, v_inv_trk_email_id, SYSDATE, IN_UPDATED_BY, SYSDATE, IN_UPDATED_BY);
    END LOOP;
    CLOSE insert_cur;

    --Determine which vendors need to be disassociated with the email address
    v_delete_sql := 'SELECT vm.vendor_id FROM ftd_apps.vendor_master vm WHERE vm.vendor_id NOT IN (' || v_vendor_list || ') AND vm.vendor_id IN (SELECT ite.vendor_id FROM ftd_apps.inv_trk_vendor_email ite WHERE ite.inv_trk_email_id = ' || v_inv_trk_email_id || ')';
    OPEN delete_cur FOR v_delete_sql;
    LOOP
      FETCH delete_cur INTO v_vendor_id;
      EXIT WHEN delete_cur%NOTFOUND;
      DELETE FROM ftd_apps.inv_trk_vendor_email itve
      WHERE itve.vendor_id = v_vendor_id
      AND itve.inv_trk_email_id = v_inv_trk_email_id;
    END LOOP;
    CLOSE delete_cur;

    IF (IN_APPLY_TO_CURRENT_FLAG = 'Y') THEN
      --Determine which inventory records need to be associated with the email address
      v_insert_sql := 'SELECT vm.vendor_id FROM ftd_apps.vendor_master vm WHERE vm.vendor_id IN (' || v_vendor_list || ') AND vm.vendor_id NOT IN (SELECT it.vendor_id FROM ftd_apps.inv_trk it JOIN ftd_apps.inv_trk_inv_ctrl_email itce ON itce.inv_trk_id = it.inv_trk_id AND itce.inv_trk_email_id = ' || v_inv_trk_email_id || ' WHERE it.end_date >= TRUNC(SYSDATE))';
      OPEN insert_cur FOR v_insert_sql;
      LOOP
        FETCH insert_cur INTO v_vendor_id;
        EXIT WHEN insert_cur%NOTFOUND;
        --If the vendor does not exist insert it
        FOR inv_rec in inv_trk_id(v_vendor_id, v_inv_trk_email_id) LOOP
          BEGIN
            INSERT into ftd_apps.inv_trk_inv_ctrl_email itice
            (inv_trk_id, inv_trk_email_id, created_on, created_by, updated_on, updated_by)
            VALUES(inv_rec.inv_trk_id, v_inv_trk_email_id, SYSDATE, IN_UPDATED_BY, SYSDATE, IN_UPDATED_BY);
          END;
        END LOOP;
      END LOOP;
      CLOSE insert_cur;

      --Determine which inventory records need to be disassociated with the email address
      v_delete_sql := 'SELECT vm.vendor_id FROM ftd_apps.vendor_master vm WHERE vm.vendor_id NOT IN (' || v_vendor_list || ') AND vm.vendor_id IN (SELECT it.vendor_id FROM ftd_apps.inv_trk it JOIN ftd_apps.inv_trk_inv_ctrl_email itce ON itce.inv_trk_id = it.inv_trk_id AND itce.inv_trk_email_id = ' || v_inv_trk_email_id || ' WHERE it.end_date >= TRUNC(SYSDATE))';
      OPEN delete_cur FOR v_delete_sql;
      LOOP
        FETCH delete_cur INTO v_vendor_id;
        EXIT WHEN delete_cur%NOTFOUND;
        --If the vendor does not exist delete it
        DELETE FROM ftd_apps.inv_trk_inv_ctrl_email itice
        WHERE itice.inv_trk_email_id = v_inv_trk_email_id
        AND itice.inv_trk_id in
          (
            SELECT  it.inv_trk_id
            FROM    ftd_apps.inv_trk it
            WHERE   it.vendor_id = v_vendor_id
            AND     it.end_date >= TRUNC(SYSDATE)
          );
      END LOOP;
      CLOSE delete_cur;

    END IF;

  END IF;

  EXCEPTION WHEN OTHERS THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END SAVE_INV_TRK_EMAIL;


PROCEDURE SELECT_INV_TRK_EMAIL
(
IN_EMAIL_ADDRESS                IN FTD_APPS.INV_TRK_EMAIL.EMAIL_ADDRESS%TYPE,
OUT_INV_TRK_EMAIL_ID            OUT FTD_APPS.INV_TRK_EMAIL.INV_TRK_EMAIL_ID%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the latest active
        inv_trk_email_id associated with a given email address.  If no active
        email address exists OUT_INV_TRK_EMAIL_ID will be NULL.

Input:
        in_email_address

Output:
        out_inv_trk_email_id

-----------------------------------------------------------------------------*/
BEGIN

  SELECT MAX(ite.inv_trk_email_id)
  INTO OUT_INV_TRK_EMAIL_ID
  FROM ftd_apps.inv_trk_email ite
  WHERE LOWER(ite.email_address) = LOWER(IN_EMAIL_ADDRESS)
  AND ite.active_flag = 'Y';

END SELECT_INV_TRK_EMAIL;

/*------------------------------------------------------------------------------
PROCEDURES FOR NEW SKU-IT
--------------------------------------------------------------------------------*/

PROCEDURE SEARCH_INVENTORY (
    IN_PRODUCT_IDS            IN  VARCHAR2,
    IN_VENDOR_IDS             IN  VARCHAR2,
    IN_START_DATE             IN  FTD_APPS.INV_TRK.START_DATE%TYPE,
    IN_END_DATE               IN  FTD_APPS.INV_TRK.END_DATE%TYPE,
    IN_PRODUCT_STATUS         IN  FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
    IN_PERIOD_STATUS          IN  VARCHAR2,
    IN_ON_HAND                IN  NUMBER,
    IN_START_REC              IN  NUMBER,
    IN_END_REC                IN  NUMBER,
    INOUT_TOT_REC_CNT         IN  OUT NUMBER,
    OUT_MESSAGE				        OUT VARCHAR2,
    OUT_VEN_INV_CUR           OUT TYPES.REF_CURSOR

) AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO RETRIEVE INVENTORY FOR A GIVEN SEARCH CRITERIA.
------------------------------------------------------------------------------------------------------------------*/
	-- INDEX BY TABLE TO STORE CURSOR RESULT SETS
	TYPE 	VARCHAR_TAB_TYP 		IS TABLE OF VARCHAR2(32767) INDEX BY PLS_INTEGER;
	TYPE 	NUMBER_TAB_TYP 			IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
	TYPE 	DATE_TAB_TYP 		  	IS TABLE OF DATE INDEX BY PLS_INTEGER;

	-- VARIABLES
	V_INV_TRK_ID 						NUMBER_TAB_TYP;
	V_PRODUCT_ID 						VARCHAR_TAB_TYP;
	V_PRODUCT_NAME 						VARCHAR_TAB_TYP;
	V_PRODUCT_STATUS 					VARCHAR_TAB_TYP;
	V_PRODUCT_UPDATED_BY 				VARCHAR_TAB_TYP;
	V_EXCEPTION_START_DATE 				DATE_TAB_TYP;
	V_VENDOR_ID 						VARCHAR_TAB_TYP;
	V_VENDOR_NAME 						VARCHAR_TAB_TYP;
	V_VENDOR_PRODUCT_STATUS 			VARCHAR_TAB_TYP;
	V_VENDOR_PRODUCT_UPDATED_BY 		VARCHAR_TAB_TYP;
	V_SHUTDOWN_THRESHOLD_QTY 			NUMBER_TAB_TYP;
	V_START_DATE 						DATE_TAB_TYP;
	V_END_DATE 							DATE_TAB_TYP;
	V_FORECAST_QTY 						NUMBER_TAB_TYP;
	V_REVISED_FORECAST_QTY 				NUMBER_TAB_TYP;
	V_FTD_MSG_COUNT 					NUMBER_TAB_TYP;
	V_CAN_MSG_COUNT 					NUMBER_TAB_TYP;
	V_REJ_MSG_COUNT 					NUMBER_TAB_TYP;
	V_ON_HAND 							NUMBER_TAB_TYP;
	
	V_VEN_PROD_INV_SQL 					VARCHAR2 (32767);
	
	V_TOTAL_RECORDS      				NUMBER 	:= INOUT_TOT_REC_CNT;
	V_START_REC          				NUMBER 	:= IN_START_REC;
	V_END_REC           	 			NUMBER 	:= IN_END_REC;
	V_PERFORM_SEARCH 					CHAR 	:= 'Y';
  	V_INV_START_DATES       			VARCHAR2(10000);
    
	-- CURSORS
	OUT_PRODUCT_VENDOR_REC_CUR TYPES.REF_CURSOR;
	
	
BEGIN
	OUT_MESSAGE := '';
  
  	  -- IF PERIOD_STATUS IS NOT NULL, FIRST CHECK IF THERE ARE ANY INVENTORIES FOR GIVEN PERIOD STATUS.
	  IF (IN_VENDOR_IDS IS NULL) AND (IN_PERIOD_STATUS IS NOT NULL OR IN_ON_HAND IS NOT NULL) THEN 		  
		  V_INV_START_DATES := GET_INV_PERIODS(IN_PERIOD_STATUS, IN_PRODUCT_IDS, IN_PRODUCT_STATUS, IN_START_DATE, IN_END_DATE, IN_ON_HAND);		  
     	  IF V_INV_START_DATES IS NULL OR V_INV_START_DATES = '' THEN
			V_PERFORM_SEARCH := 'N';
      		INOUT_TOT_REC_CNT := 0;
			V_VEN_PROD_INV_SQL := 'SELECT NULL FROM DUAL';
		  END IF;		  
	  END IF;
	
    -- IF PERFORM SEARCH IS Y AND TOTAL COUNT IS NOT PROVIDED, GET TOTAL COUNT.	
	IF V_PERFORM_SEARCH = 'Y' AND (V_TOTAL_RECORDS IS NULL OR V_TOTAL_RECORDS  = 0 OR V_TOTAL_RECORDS = '') THEN
    	V_VEN_PROD_INV_SQL := GET_SEARCH_INV_SQL(IN_PRODUCT_IDS, IN_VENDOR_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_PERIOD_STATUS, IN_ON_HAND, V_TOTAL_RECORDS, V_INV_START_DATES);
    	OPEN OUT_PRODUCT_VENDOR_REC_CUR FOR V_VEN_PROD_INV_SQL;
      	FETCH OUT_PRODUCT_VENDOR_REC_CUR INTO V_TOTAL_RECORDS;
        INOUT_TOT_REC_CNT := V_TOTAL_RECORDS;
		
		IF V_TOTAL_RECORDS <= 0 THEN
			V_PERFORM_SEARCH := 'N';
			V_VEN_PROD_INV_SQL := 'SELECT NULL FROM DUAL';	
		ELSE 
      		IF V_TOTAL_RECORDS  < V_START_REC THEN
        		V_PERFORM_SEARCH := 'N';
        		OUT_MESSAGE := 'START RECORD IS MORE THAN TOTAL RECORDS';
        		V_VEN_PROD_INV_SQL := 'SELECT NULL FROM DUAL';
      		END IF;
    	END IF;
    
  END IF;
	
	IF V_TOTAL_RECORDS > 0 AND V_PERFORM_SEARCH = 'Y' THEN
		V_VEN_PROD_INV_SQL := GET_SEARCH_INV_SQL(IN_PRODUCT_IDS, IN_VENDOR_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_PERIOD_STATUS, IN_ON_HAND, V_TOTAL_RECORDS, V_INV_START_DATES);
    		
		IF V_START_REC = 0 OR V_END_REC = 0 OR V_END_REC < V_START_REC THEN
			V_START_REC        := 1;
			V_END_REC          := 500;
		END IF;
		
		V_VEN_PROD_INV_SQL := ' SELECT * FROM (SELECT ROWNUM ROWCNT, COMP_SQL.* FROM (' || V_VEN_PROD_INV_SQL || ' ) COMP_SQL ) WHERE ROWCNT BETWEEN ' || V_START_REC || ' AND ' || V_END_REC;
		
		--dbms_output.put_line('vsql: ' || V_VEN_PROD_INV_SQL);
		
	END IF;
		
	OPEN OUT_VEN_INV_CUR FOR V_VEN_PROD_INV_SQL;
	
	EXCEPTION		
		WHEN OTHERS THEN
			OUT_MESSAGE := 'ERROR OCCURRED IN SEARCH_INVENTORY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
			RAISE_APPLICATION_ERROR(-20001, OUT_MESSAGE);
			
END SEARCH_INVENTORY;

FUNCTION GET_SEARCH_INV_SQL (
    IN_PRODUCT_IDS           	IN 	VARCHAR2,
    IN_VENDOR_IDS            	IN 	VARCHAR2,
    IN_START_DATE            	IN 	FTD_APPS.INV_TRK.START_DATE%TYPE,
    IN_END_DATE              	IN 	FTD_APPS.INV_TRK.END_DATE%TYPE,
    IN_PRODUCT_STATUS        	IN 	FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
    IN_PERIOD_STATUS			IN  VARCHAR2,
    IN_ON_HAND               	IN 	NUMBER,
    IN_TOTAL_REC             	IN 	NUMBER,
    IN_INV_START_DATES        IN VARCHAR2
) RETURN VARCHAR2 AS

/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO CONSTRUCT A SEARCH INV SQL. IF VENDOR ID IS NOT INCLUDED IN SEARCH CONDITIONS, 
ALL VENDORS, PENDING ORDERS ROWS WILL NOT BE CALCULATED.
------------------------------------------------------------------------------------------------------------------*/
	V_SQL          VARCHAR2 (32767) := ' ';
	V_SUB_SQL      VARCHAR2 (32767) := ' ';
	V_TOT_INV_SQL  VARCHAR2 (10000) := ' ';
	V_PEND_INV_SQL  VARCHAR2 (15000) := ' ';
	
	V_VAR_SQL      VARCHAR2 (4000) := ' SELECT A.INV_U_STATUS, A.INV_A_STATUS, A.INV_X_STATUS, A.INV_TRK_ID, A.INV_STATUS, A.PERIOD_SHUTDOWN, A.MOST_RECENT_STATUS, A.PRODUCT_ID, PM.PRODUCT_NAME, PM.STATUS PRODUCT_STATUS, PM.UPDATED_BY PRODUCT_UPDATED_BY, PM.EXCEPTION_START_DATE, A.VENDOR_ID, A.VENDOR_NAME, A.VP_STATUS VENDOR_PRODUCT_STATUS, A.VP_UPDT_BY VENDOR_PRODUCT_UPDATED_BY, A.SHUTDOWN_THRESHOLD_QTY, TO_CHAR(A.START_DATE, ''MM/dd/yyyy'') START_DATE, TO_CHAR(A.END_DATE, ''MM/dd/yyyy'') END_DATE, A.FORECAST_QTY, A.REVISED_FORECAST_QTY, A.FTD_MSG_COUNT, A.CAN_MSG_COUNT, A.REJ_MSG_COUNT, A.ON_HAND, (A.FTD_MSG_COUNT - A.CAN_MSG_COUNT - A.REJ_MSG_COUNT) SHIPPED_SHIPPING, CASE A.REVISED_FORECAST_QTY WHEN 0 THEN 0 ELSE (((A.FTD_MSG_COUNT - A.CAN_MSG_COUNT - A.REJ_MSG_COUNT) / A.REVISED_FORECAST_QTY ) * 100) END AS SELL_THRU_PERCENT ';
 
	V_COUNT_SQL    VARCHAR2(100)  := ' SELECT COUNT(*) ';  
	V_FROM1        VARCHAR2 (100) := ' FROM FTD_APPS.PRODUCT_MASTER PM, (';		
	V_FROM2        VARCHAR2 (10) := ' ) A ';
	V_WHERE        VARCHAR2 (100) := ' WHERE A.PRODUCT_ID = PM.PRODUCT_ID  ';	
	V_ORDER_BY     VARCHAR2 (100) := ' ORDER BY A.PRODUCT_ID, A.START_DATE DESC, A.VENDOR_ID ASC, A.VENDOR_NAME DESC '; 
  	
BEGIN

	V_SUB_SQL := GET_VEN_PROD_INV_SQL(IN_PRODUCT_IDS, IN_VENDOR_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_PERIOD_STATUS, IN_ON_HAND, IN_TOTAL_REC);
	
	--QUERY TO GET ALL THE INVENTORY FOR A PRODUCT
	IF IN_VENDOR_IDS IS NULL THEN   
		V_TOT_INV_SQL := GET_ALL_VENDORS_SQL(IN_PRODUCT_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_ON_HAND, IN_INV_START_DATES);
		V_PEND_INV_SQL := GET_PEND_ORDER_SQL(IN_PRODUCT_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_INV_START_DATES);
		V_SUB_SQL := V_SUB_SQL || ' UNION ALL ' || V_PEND_INV_SQL || ' UNION ALL ' || V_TOT_INV_SQL;     
	END IF;
	
	--IF TOTAL RECORD COUNT IS NULL OR 0 RETURN QUERY TO FETCH TOTAL RECORD COUNT
	IF IN_TOTAL_REC = 0  OR IN_TOTAL_REC IS NULL THEN
		V_SQL        := V_COUNT_SQL || V_FROM1 || V_SUB_SQL || V_FROM2 || V_WHERE ||  V_ORDER_BY;
	ELSE
		V_SQL := V_VAR_SQL || V_FROM1 || V_SUB_SQL || V_FROM2 || V_WHERE ||  V_ORDER_BY;
	END IF;
	
	RETURN V_SQL;
	
END GET_SEARCH_INV_SQL;

FUNCTION GET_VEN_PROD_INV_SQL (
    IN_PRODUCT_IDS           	IN 	VARCHAR2,
    IN_VENDOR_IDS            	IN 	VARCHAR2,
    IN_START_DATE            	IN 	FTD_APPS.INV_TRK.START_DATE%TYPE,
    IN_END_DATE              	IN 	FTD_APPS.INV_TRK.END_DATE%TYPE,
    IN_PRODUCT_STATUS        	IN 	FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
    IN_PERIOD_STATUS			    IN  VARCHAR2,
    IN_ON_HAND               	IN 	NUMBER,
    IN_TOTAL_REC             	IN 	NUMBER	
) RETURN VARCHAR2 AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO CONSTRUCT A QUERY TO RETRIEVE THE NON-PENDING FTD ORDER COUNT 
FOR A GIVEN SEARCH CRITERIA.
------------------------------------------------------------------------------------------------------------------*/
	-- VARIABLES
	V_SQL VARCHAR2 (32767) := ' ';
	V_SUB_SELECT   VARCHAR2 (4000) := ' SELECT IT.INV_TRK_ID, IT.STATUS INV_STATUS, IT.PRODUCT_ID, VP.AVAILABLE VP_STATUS, VP.UPDATED_BY VP_UPDT_BY, NULL PERIOD_SHUTDOWN, VM.VENDOR_NAME, IT.VENDOR_ID, IT.SHUTDOWN_THRESHOLD_QTY, IT.START_DATE, IT.END_DATE, IT.FORECAST_QTY, IT.REVISED_FORECAST_QTY, SUM(ITAC.FTD_MSG_COUNT) FTD_MSG_COUNT, SUM(ITAC.CAN_MSG_COUNT) CAN_MSG_COUNT, SUM(ITAC.REJ_MSG_COUNT ) REJ_MSG_COUNT, (IT.REVISED_FORECAST_QTY - SUM(ITAC.FTD_MSG_COUNT) + SUM(ITAC.CAN_MSG_COUNT) + SUM(ITAC.REJ_MSG_COUNT )) ON_HAND, NULL INV_U_STATUS, NULL INV_A_STATUS, NULL INV_X_STATUS, NULL MOST_RECENT_STATUS';
	
	V_SUB_FROM     VARCHAR2 (1000) := ' FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC, FTD_APPS.VENDOR_PRODUCT VP, FTD_APPS.VENDOR_MASTER VM, FTD_APPS.PRODUCT_MASTER PM ';
	V_SUB_WHERE    VARCHAR2 (100) := ' WHERE IT.VENDOR_ID = ITAC.VENDOR_ID ';
	
	V_SUB_AND      VARCHAR2 (4000) := ' AND VM.VENDOR_ID = ITAC.VENDOR_ID AND IT.PRODUCT_ID = ITAC.PRODUCT_ID AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE AND IT.VENDOR_ID = VP.VENDOR_ID AND IT.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID AND ITAC.VENDOR_ID = VP.VENDOR_ID AND ITAC.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID AND PM.PRODUCT_ID = IT.PRODUCT_ID ';
	
	V_SUB_GROUP_BY VARCHAR2 (4000) := ' GROUP BY IT.INV_TRK_ID, IT.STATUS, IT.PRODUCT_ID, VP.AVAILABLE, VP.UPDATED_BY, VM.VENDOR_NAME, IT.VENDOR_ID, IT.SHUTDOWN_THRESHOLD_QTY, IT.START_DATE, IT.END_DATE, IT.FORECAST_QTY, IT.REVISED_FORECAST_QTY ';
	
	V_SUB_HAVING          VARCHAR2 (4000) := ' ';

BEGIN
	
	IF IN_VENDOR_IDS IS NOT NULL THEN
		V_SUB_AND      := V_SUB_AND || ' AND IT.VENDOR_ID IN (' || IN_VENDOR_IDS ||') ';
	END IF;
	IF IN_PRODUCT_IDS IS NOT NULL THEN
		V_SUB_AND       := V_SUB_AND || ' AND IT.PRODUCT_ID IN (' || IN_PRODUCT_IDS ||') ';
	END IF;
	
	IF IN_START_DATE IS NOT NULL THEN
		V_SUB_AND      := V_SUB_AND || ' AND IT.START_DATE >= TO_DATE(''' || TO_CHAR(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;		
	END IF;
	
	IF IN_END_DATE IS NOT NULL THEN
		V_SUB_AND    := V_SUB_AND || ' AND IT.END_DATE <= TO_DATE(''' || TO_CHAR(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;
  
	CASE
	  WHEN IN_PRODUCT_STATUS = 'X' THEN
		V_SUB_AND         := V_SUB_AND || ' AND PM.STATUS = ''U'' AND PM.UPDATED_BY = ''SYSTEM_INV_TRK''';
	  WHEN IN_PRODUCT_STATUS = 'U' THEN
		V_SUB_AND         := V_SUB_AND || ' AND PM.STATUS = ''U'' AND PM.UPDATED_BY != ''SYSTEM_INV_TRK''';
	  WHEN IN_PRODUCT_STATUS = 'A' THEN
		V_SUB_AND         := V_SUB_AND || ' AND PM.STATUS = ''A'' AND PM.EXCEPTION_START_DATE IS NULL';
	  WHEN IN_PRODUCT_STATUS = 'R' THEN
		V_SUB_AND         := V_SUB_AND || ' AND PM.STATUS = ''A'' AND PM.EXCEPTION_START_DATE IS NOT NULL';
	  ELSE
		V_SUB_AND         := V_SUB_AND;
	END CASE;
  
	CASE
    
    WHEN IN_PERIOD_STATUS = 'X' THEN
		V_SUB_AND     := V_SUB_AND || ' AND IT.STATUS = ''X''';
		WHEN IN_PERIOD_STATUS = 'U' THEN
		V_SUB_AND     := V_SUB_AND || ' AND (IT.STATUS = ''U'' OR IT.STATUS = ''N'')';
		WHEN IN_PERIOD_STATUS = 'A' THEN
		V_SUB_AND     := V_SUB_AND || ' AND IT.STATUS = ''A''';
		ELSE
		V_SUB_AND     := V_SUB_AND;
    
	END CASE;
	
	IF IN_ON_HAND IS NOT NULL THEN
	V_SUB_HAVING  := ' HAVING (IT.REVISED_FORECAST_QTY - SUM(ITAC.FTD_MSG_COUNT) + SUM(ITAC.CAN_MSG_COUNT) + SUM(ITAC.REJ_MSG_COUNT )) < ' || IN_ON_HAND;
	END IF;

	V_SQL := V_SUB_SELECT || V_SUB_FROM || V_SUB_WHERE || V_SUB_AND || V_SUB_GROUP_BY || V_SUB_HAVING;
	
	RETURN V_SQL;
	
END GET_VEN_PROD_INV_SQL;

FUNCTION GET_ALL_VENDORS_SQL (
    IN_PRODUCT_IDS            IN VARCHAR2,
    IN_START_DATE             IN FTD_APPS.INV_TRK.START_DATE%TYPE,
    IN_END_DATE               IN FTD_APPS.INV_TRK.END_DATE%TYPE,
    IN_PRODUCT_STATUS         IN FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
    IN_ON_HAND                IN NUMBER,
    IN_INV_START_DATES        IN    VARCHAR2
) RETURN VARCHAR2 AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO CONSTRUCT A QUERY TO RETRIEVE THE PENDING + NON_PENDING FTD ORDER COUNT 
FOR A GIVEN SEARCH CRITERIA.
------------------------------------------------------------------------------------------------------------------*/
	V_SQL  VARCHAR2 (4000) := ' SELECT 0 INV_TRK_ID, NULL INV_STATUS, A.PRODUCT_ID, NULL VP_STATUS, NULL VP_UPDT_BY, INV_TRK_PKG.CHECK_PRODUCT_SHUTDOWN(A.PRODUCT_ID, A.START_DATE) PERIOD_SHUTDOWN, ''ALL VENDORS'' VENDOR_NAME, NULL VENDOR_ID, A.SHUTDOWN_THRESHOLD_QTY, A.START_DATE, A.END_DATE, A.FORECAST_QTY, A.REVISED_FORECAST_QTY, SUM(ITC.FTD_MSG_COUNT ) FTD_MSG_COUNT, SUM(ITC.CAN_MSG_COUNT) CAN_MSG_COUNT, SUM(ITC.REJ_MSG_COUNT) REJ_MSG_COUNT, (A.REVISED_FORECAST_QTY - SUM(ITC.FTD_MSG_COUNT ) + SUM(ITC.CAN_MSG_COUNT) + SUM(ITC.REJ_MSG_COUNT)) ON_HAND, (SELECT DISTINCT ''U'' FROM FTD_APPS.INV_TRK IT_U WHERE IT_U.STATUS IN (''U'',''N'') AND IT_U.PRODUCT_ID = A.PRODUCT_ID AND IT_U.START_DATE = A.START_DATE AND IT_U.END_DATE = A.END_DATE) INV_U_STATUS, (SELECT DISTINCT ''A'' FROM FTD_APPS.INV_TRK IT_A WHERE IT_A.STATUS = ''A'' AND IT_A.PRODUCT_ID = A.PRODUCT_ID AND IT_A.START_DATE = A.START_DATE AND IT_A.END_DATE = A.END_DATE) INV_A_STATUS, (SELECT DISTINCT ''X'' FROM FTD_APPS.INV_TRK IT_X WHERE IT_X.STATUS = ''X'' AND IT_X.PRODUCT_ID = A.PRODUCT_ID AND IT_X.START_DATE = A.START_DATE AND IT_X.END_DATE = A.END_DATE) INV_X_STATUS, (SELECT IT.STATUS FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = A.PRODUCT_ID AND IT.START_DATE = A.START_DATE AND IT.END_DATE = A.END_DATE AND IT.UPDATED_ON = (SELECT MAX(IT1.UPDATED_ON) FROM FTD_APPS.INV_TRK IT1 WHERE IT1.PRODUCT_ID = A.PRODUCT_ID AND IT1.START_DATE = A.START_DATE AND IT1.END_DATE = A.END_DATE) AND ROWNUM = 1) MOST_RECENT_STATUS ';
	V_FROM1           VARCHAR2 (4000) := ' FROM VENUS.INV_TRK_COUNT ITC, FTD_APPS.PRODUCT_MASTER PM, (';
	V_SUB_SQL         VARCHAR2 (4000) := ' ';
	V_SUB_SELECT      VARCHAR2 (4000) := ' SELECT IT.PRODUCT_ID,  IT.START_DATE, IT.END_DATE, SUM(SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, SUM(IT.FORECAST_QTY) FORECAST_QTY, SUM(IT.REVISED_FORECAST_QTY) REVISED_FORECAST_QTY ';
	V_SUB_FROM        VARCHAR2 (4000) := ' FROM FTD_APPS.INV_TRK IT ';
	V_SUB_WHERE       VARCHAR2 (4000) := ' WHERE 1=1 ';
	V_SUB_AND         VARCHAR2 (4000) := ' ';
	V_SUB_GROUP_BY    VARCHAR2 (4000) := ' GROUP BY IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE ';
	V_FROM2           VARCHAR2 (4000) := ' ) A ';
	V_WHERE           VARCHAR2 (4000) := ' WHERE A.PRODUCT_ID = ITC.PRODUCT_ID ';
	V_AND             VARCHAR2 (4000) := ' AND A.PRODUCT_ID = PM.PRODUCT_ID ';
	V_GROUP_BY        VARCHAR2 (4000) := ' GROUP BY A.PRODUCT_ID, PM.PRODUCT_NAME, PM.STATUS, PM.UPDATED_BY, PM.EXCEPTION_START_DATE, ''ALL VENDORS'', A.SHUTDOWN_THRESHOLD_QTY, A.START_DATE, A.END_DATE, A.FORECAST_QTY, A.REVISED_FORECAST_QTY, NULL, NULL, NULL ';


	BEGIN

	IF IN_PRODUCT_IDS IS NOT NULL THEN
	  V_SUB_AND       := V_SUB_AND || ' AND IT.PRODUCT_ID IN (' || IN_PRODUCT_IDS ||') ';
	END IF;
  
  IF IN_INV_START_DATES IS NOT NULL THEN
   V_SUB_AND       := V_SUB_AND || ' AND IT.START_DATE IN (' || IN_INV_START_DATES || ') ' ;
  END IF;

	IF IN_START_DATE IS NOT NULL THEN
	  V_SUB_AND       := V_SUB_AND || ' AND IT.START_DATE >= TO_DATE(''' || TO_CHAR(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;

	IF IN_END_DATE IS NOT NULL THEN
	  V_SUB_AND       := V_SUB_AND || ' AND IT.END_DATE <= TO_DATE(''' || TO_CHAR(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;

	V_SUB_SQL         := V_SUB_SELECT || V_SUB_FROM || V_SUB_WHERE || V_SUB_AND || V_SUB_GROUP_BY;

	V_AND             := V_AND || ' AND ITC.SHIP_DATE BETWEEN A.START_DATE AND A.END_DATE ';

	CASE
	  WHEN IN_PRODUCT_STATUS = 'X' THEN
		V_AND         := V_AND || ' AND PM.STATUS = ''U'' AND PM.UPDATED_BY = ''SYSTEM_INV_TRK''';
	  WHEN IN_PRODUCT_STATUS = 'U' THEN
		V_AND         := V_AND || ' AND PM.STATUS = ''U'' AND PM.UPDATED_BY != ''SYSTEM_INV_TRK''';
	  WHEN IN_PRODUCT_STATUS = 'A' THEN
		V_AND         := V_AND || ' AND PM.STATUS = ''A'' AND PM.EXCEPTION_START_DATE IS NULL';
	  WHEN IN_PRODUCT_STATUS = 'R' THEN
		V_AND         := V_AND || ' AND PM.STATUS = ''A'' AND PM.EXCEPTION_START_DATE IS NOT NULL';
	  ELSE
		V_AND         := V_AND;
	END CASE;

	V_SQL             := V_SQL || V_FROM1 || V_SUB_SQL || V_FROM2 || V_WHERE || V_AND || V_GROUP_BY;

	RETURN V_SQL;

END GET_ALL_VENDORS_SQL;

FUNCTION GET_PEND_ORDER_SQL(
    IN_PRODUCT_IDS            IN 	VARCHAR2,
    IN_START_DATE             IN 	FTD_APPS.INV_TRK.START_DATE%TYPE,
    IN_END_DATE               IN 	FTD_APPS.INV_TRK.END_DATE%TYPE,
    IN_PRODUCT_STATUS         IN 	FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
    IN_INV_START_DATES        IN  VARCHAR2
) RETURN VARCHAR2 AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO CONSTRUCT A QUERY TO RETRIEVE THE PENDING FTD ORDER COUNT FOR A GIVEN SEARCH CRITERIA.
------------------------------------------------------------------------------------------------------------------*/
	-- PENDING ORDERS QUERY ONLY WHEN VENDOR ID IS NULL, SO NO CONSTRAINTS ON VENDOR_MASTER AND VENDOR_PRODUCT.
		V_SQL  VARCHAR2 (32767) := ' SELECT  DISTINCT 0 INV_TRK_ID, NULL INV_STATUS, VEND_CNT.PRODUCT_ID, NULL VP_STATUS, NULL VP_UPDT_BY, NULL PERIOD_SHUTDOWN, ''PENDING'' VENDOR_NAME, NULL VENDOR_ID,	0 SHUTDOWN_THRESHOLD_QTY, VEND_CNT.START_DATE, VEND_CNT.END_DATE, 0 FORECAST_QTY, 0 REVISED_FORECAST_QTY, 
		(ALL_CNT.FTD_MSG_COUNT - VEND_CNT.FTD_MSG_COUNT) FTD_MSG_COUNT, (ALL_CNT.CAN_MSG_COUNT - VEND_CNT.CAN_MSG_COUNT) CAN_MSG_COUNT,
		(ALL_CNT.REJ_MSG_COUNT - VEND_CNT.REJ_MSG_COUNT) REJ_MSG_COUNT, (0 - (ALL_CNT.FTD_MSG_COUNT - VEND_CNT.FTD_MSG_COUNT) 
		+ (ALL_CNT.CAN_MSG_COUNT - VEND_CNT.CAN_MSG_COUNT) + (ALL_CNT.REJ_MSG_COUNT - VEND_CNT.REJ_MSG_COUNT)) ON_HAND,		
		NULL INV_U_STATUS, NULL INV_A_STATUS, NULL INV_X_STATUS, NULL MOST_RECENT_STATUS FROM ';
			
	V_FROM_ALL_CNT VARCHAR2 (4000) := ' (SELECT A.PRODUCT_ID, A.START_DATE, A.END_DATE, SUM(ITC.FTD_MSG_COUNT) FTD_MSG_COUNT, SUM(ITC.CAN_MSG_COUNT) CAN_MSG_COUNT,  SUM(ITC.REJ_MSG_COUNT)   REJ_MSG_COUNT FROM VENUS.INV_TRK_COUNT ITC, (SELECT DISTINCT IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE FROM FTD_APPS.INV_TRK IT, FTD_APPS.PRODUCT_MASTER PM WHERE PM.PRODUCT_ID = IT.PRODUCT_ID ';
	
	V_FROM_ALL_CNT_END VARCHAR2 (4000) := ' ) A WHERE ITC.SHIP_DATE BETWEEN A.START_DATE AND A.END_DATE AND ITC.PRODUCT_ID = A.PRODUCT_ID GROUP BY A.PRODUCT_ID, A.START_DATE, A.END_DATE) ALL_CNT LEFT OUTER JOIN ';
	
	V_FROM_VEND VARCHAR2 (4000) := ' (SELECT A1.PRODUCT_ID, A1.START_DATE, A1.END_DATE, SUM(A1.FORECAST_QTY) FORECAST_QTY, SUM(A1.REVISED_FORECAST_QTY) REVISED_FORECAST_QTY, SUM(A1.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, SUM(A1.FTD_MSG_COUNT) FTD_MSG_COUNT,SUM(A1.CAN_MSG_COUNT) CAN_MSG_COUNT, SUM(A1.REJ_MSG_COUNT) REJ_MSG_COUNT, SUM(A1.ON_HAND) ON_HAND FROM ( SELECT IT.PRODUCT_ID, IT.VENDOR_ID, IT.SHUTDOWN_THRESHOLD_QTY, IT.START_DATE, IT.END_DATE, IT.FORECAST_QTY, IT.REVISED_FORECAST_QTY, SUM(ITAC.FTD_MSG_COUNT) FTD_MSG_COUNT, SUM(ITAC.CAN_MSG_COUNT) CAN_MSG_COUNT, 
	SUM(ITAC.REJ_MSG_COUNT ) REJ_MSG_COUNT, (IT.REVISED_FORECAST_QTY - SUM(ITAC.FTD_MSG_COUNT) + SUM(ITAC.CAN_MSG_COUNT) + SUM(ITAC.REJ_MSG_COUNT )) ON_HAND 
	FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC, FTD_APPS.VENDOR_PRODUCT VP, FTD_APPS.VENDOR_MASTER VM, FTD_APPS.PRODUCT_MASTER PM  
	WHERE IT.VENDOR_ID = ITAC.VENDOR_ID  AND VM.VENDOR_ID = ITAC.VENDOR_ID AND IT.PRODUCT_ID = ITAC.PRODUCT_ID AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE AND IT.VENDOR_ID = VP.VENDOR_ID AND IT.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID AND ITAC.VENDOR_ID = VP.VENDOR_ID AND ITAC.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID AND PM.PRODUCT_ID = IT.PRODUCT_ID ';
	
	V_FROM_VEND_END VARCHAR2 (4000) := ' GROUP BY IT.PRODUCT_ID, IT.VENDOR_ID, IT.SHUTDOWN_THRESHOLD_QTY, IT.START_DATE, IT.END_DATE, IT.FORECAST_QTY, IT.REVISED_FORECAST_QTY) A1 GROUP BY A1.PRODUCT_ID, A1.START_DATE, A1.END_DATE) VEND_CNT ON VEND_CNT.PRODUCT_ID = ALL_CNT.PRODUCT_ID AND VEND_CNT.START_DATE = ALL_CNT.START_DATE AND VEND_CNT.END_DATE = ALL_CNT.END_DATE ';
		
	V_SQL_CONDITIONS  VARCHAR2 (4000) := '';

	
BEGIN

	IF IN_PRODUCT_IDS IS NOT NULL THEN
	  V_SQL_CONDITIONS := V_SQL_CONDITIONS || ' AND IT.PRODUCT_ID IN (' || IN_PRODUCT_IDS ||') ';
	END IF;
	
	CASE
	  WHEN IN_PRODUCT_STATUS = 'X' THEN
		V_SQL_CONDITIONS         := V_SQL_CONDITIONS || ' AND PM.STATUS = ''U'' AND PM.UPDATED_BY = ''SYSTEM_INV_TRK''';
	  WHEN IN_PRODUCT_STATUS = 'U' THEN
		V_SQL_CONDITIONS         := V_SQL_CONDITIONS || ' AND PM.STATUS = ''U'' AND PM.UPDATED_BY != ''SYSTEM_INV_TRK''';
	  WHEN IN_PRODUCT_STATUS = 'A' THEN
		V_SQL_CONDITIONS         := V_SQL_CONDITIONS || ' AND PM.STATUS = ''A'' AND PM.EXCEPTION_START_DATE IS NULL';
	  WHEN IN_PRODUCT_STATUS = 'R' THEN
		V_SQL_CONDITIONS         := V_SQL_CONDITIONS || ' AND PM.STATUS = ''A'' AND PM.EXCEPTION_START_DATE IS NOT NULL';
	  ELSE
		V_SQL_CONDITIONS         := V_SQL_CONDITIONS;
	END CASE;
  
  IF IN_INV_START_DATES IS NOT NULL THEN
   V_SQL_CONDITIONS       := V_SQL_CONDITIONS || ' AND IT.START_DATE IN (' || IN_INV_START_DATES || ') ' ;
  END IF;
		
	IF IN_START_DATE IS NOT NULL THEN
	  V_SQL_CONDITIONS       := V_SQL_CONDITIONS || ' AND IT.START_DATE >= TO_DATE(''' || TO_CHAR(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;

	IF IN_END_DATE IS NOT NULL THEN
	  V_SQL_CONDITIONS       := V_SQL_CONDITIONS || ' AND IT.END_DATE <= TO_DATE(''' || TO_CHAR(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;
	
	V_SQL  := V_SQL || V_FROM_ALL_CNT || V_SQL_CONDITIONS || V_FROM_ALL_CNT_END || V_FROM_VEND || V_SQL_CONDITIONS || V_FROM_VEND_END  ;
	
	RETURN V_SQL;
	
END GET_PEND_ORDER_SQL;

FUNCTION CALCULATE_ONHAND ( 
      IN_INV_TRK_ID  		    IN    FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
      IN_REVISED_COUNT 	    IN    FTD_APPS.INV_TRK.REVISED_FORECAST_QTY%TYPE
) RETURN NUMBER AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO CALCUALTE ON HAND
IF REVISED COUNT IS PASSED IN (UPDATED REVISED COUNT PASSED AS PARAM), CONSIDER THE SAME 
ELSE GET THE CURRENT REVISED COUNT 
------------------------------------------------------------------------------------------------------------------*/
	V_ON_HAND number;
	V_TOT_ORDERS number;
	V_REVISED_QTY number;
BEGIN
  V_ON_HAND := 0;
  V_TOT_ORDERS := 0;
  V_REVISED_QTY := 0;
  
  IF IN_REVISED_COUNT IS NULL THEN
      SELECT IT.REVISED_FORECAST_QTY INTO V_REVISED_QTY FROM FTD_APPS.INV_TRK IT WHERE IT.INV_TRK_ID = IN_INV_TRK_ID;
  ELSE
      V_REVISED_QTY := IN_REVISED_COUNT;
  END IF;
  
  SELECT  (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) INTO V_TOT_ORDERS
	FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC, FTD_APPS.VENDOR_PRODUCT VP, 
  FTD_APPS.VENDOR_MASTER VM, FTD_APPS.PRODUCT_MASTER PM 
		WHERE IT.VENDOR_ID = ITAC.VENDOR_ID  
			AND ITAC.VENDOR_ID = VM.VENDOR_ID 
			AND VM.VENDOR_ID = VP.VENDOR_ID 
			AND IT.PRODUCT_ID = ITAC.PRODUCT_ID 
			AND ITAC.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID  
			AND PM.PRODUCT_ID = IT.PRODUCT_ID
			AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE 
			AND IT.INV_TRK_ID = IN_INV_TRK_ID
			AND PM.STATUS = 'A' 
			AND PM.SHIP_METHOD_CARRIER = 'Y'			 
			AND VP.AVAILABLE = 'Y' 
			AND VP.REMOVED = 'N'
			AND VM.ACTIVE = 'Y';
      
      V_ON_HAND := 	V_REVISED_QTY - V_TOT_ORDERS;
      
      RETURN V_ON_HAND;
      
END CALCULATE_ONHAND;

PROCEDURE UPDATE_INVENTORY_STATUS (
IN_INV_TRK_ID                    IN FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
IN_STATUS                     	 IN FTD_APPS.INV_TRK.STATUS%TYPE,
IN_UPDATED_BY                    IN FTD_APPS.INV_TRK.UPDATED_BY%TYPE,
OUT_STATUS                       OUT VARCHAR2,
OUT_MESSAGE                      OUT VARCHAR2
) AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: UPDATES THE INVENTORY STATUS TO THE ONE PASS IN, IF NOT ALREADY CALCULATED (THAT IS IF STATUS IS NULL)
RECALCULATE ON HAND AND UPDATE THE STATUS
------------------------------------------------------------------------------------------------------------------*/
V_STATUS	CHAR(1);
V_ONHAND	NUMBER;
V_SHUTDOWN	NUMBER;

BEGIN

	OUT_STATUS	:= 'N';
	OUT_MESSAGE	:= '';
	V_STATUS := IN_STATUS;

	IF V_STATUS IS NULL THEN
		V_ONHAND := CALCULATE_ONHAND(IN_INV_TRK_ID, null);
		SELECT SHUTDOWN_THRESHOLD_QTY INTO V_SHUTDOWN FROM FTD_APPS.INV_TRK WHERE INV_TRK_ID = IN_INV_TRK_ID;
		IF V_SHUTDOWN >= V_ONHAND THEN 
			V_STATUS := 'X';
		ELSE 
			V_STATUS := 'A';
		END IF;
	END IF;	
	
	UPDATE FTD_APPS.INV_TRK 
    SET STATUS = V_STATUS, 
        UPDATED_BY = IN_UPDATED_BY, 
        UPDATED_ON = SYSDATE 
    WHERE INV_TRK_ID = IN_INV_TRK_ID;
	
	OUT_STATUS	:= 'Y';

EXCEPTION
  WHEN OTHERS THEN
    OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_INVENTORY_STATUS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_INVENTORY_STATUS;

PROCEDURE SEND_INVENTORY_ALERTS (
IN_INV_TRK_ID     	            IN    FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
IN_PRODUCT_ID     	            IN    FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_VENDOR_ID      	            IN    FTD_APPS.VENDOR_PRODUCT.VENDOR_ID%TYPE,
IN_PRODUCT_NAME   	            IN    FTD_APPS.PRODUCT_MASTER.PRODUCT_NAME%TYPE,
IN_INV_START_DATE               IN    FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_INV_END_DATE                 IN    FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_VENDOR_NAME                  IN    FTD_APPS.VENDOR_MASTER.VENDOR_NAME%TYPE,
IN_ON_HAND                      IN    NUMBER,
IN_TOT_INV                      IN    NUMBER,
IN_SHUTDOWN                     IN    NUMBER,
IN_FTD_ORDER_CNT                IN    NUMBER,
IN_PEND_ORDER_CNT               IN    NUMBER,
IN_ALERT_TYPE                   IN    VARCHAR2,
OUT_STATUS                      OUT   VARCHAR2,
OUT_MESSAGE                     OUT   VARCHAR2
)
AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: SENDS INVENTORY EMAILS AS PER THE ALERT TYPE. 
------------------------------------------------------------------------------------------------------------------*/
TYPE VARCHAR_TAB_TYP IS TABLE OF VARCHAR2(200) INDEX BY PLS_INTEGER;
C  						UTL_SMTP.CONNECTION;
EMAIL_ADDRESS_CUR 		TYPES.REF_CURSOR;
EMAIL_TAB      			VARCHAR_TAB_TYP;

V_SUB_SQL VARCHAR2(1000) := '';
V_EMAIL_ID_SQL VARCHAR2(4000) := 'SELECT DISTINCT ITE.EMAIL_ADDRESS FROM FTD_APPS.INV_TRK_EMAIL ITE, FTD_APPS.INV_TRK_INV_CTRL_EMAIL ITICE 
									WHERE  ITICE.INV_TRK_EMAIL_ID = ITE.INV_TRK_EMAIL_ID AND ITE.ACTIVE_FLAG = ''Y''';
V_SUBJECT VARCHAR(1000):= '';
V_CONTENT VARCHAR2(4000) := '';

PROCEDURE SEND_HEADER(NAME IN VARCHAR2, HEADER IN VARCHAR2) AS
BEGIN
  UTL_SMTP.WRITE_RAW_DATA(C, utl_raw.cast_to_raw(NAME || ': ' || HEADER || UTL_TCP.CRLF));
END;

BEGIN

	OUT_STATUS := 'N';
    
	-- FOR INVENTORY ALERT/ LOCATION SHUTDOWN ALERT, INV_TRK ID IS NOT NULL. 
    IF IN_ALERT_TYPE = 'Inventory Alert' OR IN_ALERT_TYPE = 'Location Shutdown Alert' THEN
      V_EMAIL_ID_SQL := V_EMAIL_ID_SQL || ' AND ITICE.INV_TRK_ID = ' || IN_INV_TRK_ID;    
    ELSE
        V_SUB_SQL := ' SELECT DISTINCT INV_TRK_ID FROM FTD_APPS.INV_TRK WHERE 1 = 1 ' ;
        
        -- FOR PRODUCT SHUTDOWN, VENDOR ID IS NOT PROVIDED, GET ALL EMAILDS FOR A PRODUCT FOR ALL INVENTORIES.
        V_SUB_SQL := V_SUB_SQL || ' AND PRODUCT_ID = ''' || IN_PRODUCT_ID || '''';
        
        -- SEND MAILS TO ALL THE EMAIL IDS ASSOCIATED FOR A PRODUCT FOR A GIVEN PERIOD.
         IF IN_ALERT_TYPE = 'Period Shutdown Alert' THEN
            V_SUB_SQL := V_SUB_SQL || ' AND START_DATE = ''' || IN_INV_START_DATE || ''' AND END_DATE = ''' || IN_INV_END_DATE || '''';	
         END IF;
                  
		IF IN_ALERT_TYPE = 'Product Shutdown Alert' OR  IN_ALERT_TYPE = 'Dropship Shutdown Alert' THEN
			-- PRODUCT SHUTDOWN FOR A VENDOR, GET THE EMAIL IDS FOR ALL INVENTORIES FOR A VENDOR IRRESPECTIVE OF PERIOD
			IF IN_VENDOR_ID IS NOT NULL OR IN_VENDOR_ID != '' THEN
				V_SUB_SQL := V_SUB_SQL || ' AND VENDOR_ID = ''' || IN_VENDOR_ID || '''';	
			END IF; 
		END IF;
          
     V_EMAIL_ID_SQL := V_EMAIL_ID_SQL || ' AND ITICE.INV_TRK_ID IN ( ' || V_SUB_SQL || ' )';
 	   
	END IF;
	
	OPEN EMAIL_ADDRESS_CUR FOR V_EMAIL_ID_SQL;
		FETCH EMAIL_ADDRESS_CUR BULK COLLECT INTO EMAIL_TAB;
	CLOSE EMAIL_ADDRESS_CUR;
  
	IF EMAIL_TAB.COUNT > 0 THEN   
      V_CONTENT := IN_PRODUCT_ID || ' - ' || IN_PRODUCT_NAME || UTL_TCP.CRLF;
      V_SUBJECT :=  IN_ALERT_TYPE || ' - ' || IN_PRODUCT_ID;  
	  
      IF IN_VENDOR_ID IS NOT NULL AND IN_ALERT_TYPE != 'Period Shutdown Alert' THEN  
         V_SUBJECT := V_SUBJECT || ' - ' || IN_VENDOR_NAME;
         V_CONTENT := V_CONTENT || IN_VENDOR_NAME || UTL_TCP.CRLF;
      END IF;
      
      V_CONTENT := V_CONTENT || TO_CHAR(IN_INV_START_DATE, 'MM/DD/YYYY') || ' ' || TO_CHAR(IN_INV_END_DATE, 'MM/DD/YYYY');
      
      IF IN_ALERT_TYPE = 'Period Shutdown Alert' THEN
        V_SUBJECT := V_SUBJECT || ' - ' || IN_INV_START_DATE || ' - ' || IN_INV_END_DATE;
        V_CONTENT :=  V_CONTENT || ' - ' || 'SHUTDOWN';				
      END IF;
      
      V_CONTENT := V_CONTENT || UTL_TCP.CRLF || UTL_TCP.CRLF;
      
		IF IN_ALERT_TYPE != 'Period Shutdown Alert' THEN
			V_CONTENT := V_CONTENT ||  'INVENTORY NOW: ' || IN_ON_HAND || '.  ' || 'SHUTDOWN AT ' || IN_SHUTDOWN || '.' || UTL_TCP.CRLF || UTL_TCP.CRLF;
		ELSE
			V_CONTENT := V_CONTENT || 'TOTAL INVENTORY: ' || IN_TOT_INV || '.  SHIPPED/SHIPPING: ' || IN_FTD_ORDER_CNT || '.  PENDING: ' || IN_PEND_ORDER_CNT || UTL_TCP.CRLF || UTL_TCP.CRLF;
		END IF;


		IF IN_ALERT_TYPE = 'Location Shutdown Alert' THEN
			V_CONTENT := V_CONTENT || IN_VENDOR_NAME || ' SHUTDOWN.';
		END IF;

		IF IN_ALERT_TYPE = 'Product Shutdown Alert' THEN
			V_CONTENT := V_CONTENT || 'PRODUCT WAS SHUTDOWN.';
			IF IN_VENDOR_ID IS NOT NULL THEN
				V_CONTENT := V_CONTENT ||' FOR ' || IN_VENDOR_NAME || '.';
			END IF;
		END IF;

		IF IN_ALERT_TYPE = 'Dropship Shutdown Alert' THEN
			V_CONTENT := V_CONTENT || 'VENDOR LOCATION AND DROP SHIP DELIVERY SHUTDOWN FOR PRODUCT. FLORIST DELIVERY IS STILL AVAILABLE.';
		END IF;
		
		C := UTL_SMTP.OPEN_CONNECTION(FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('Mail Server', 'NAME'));
		UTL_SMTP.HELO(C, 'ftdi.com');
		UTL_SMTP.MAIL(C, 'noreply-apolloinventory@ftdi.com');
  
		FOR X IN 1..EMAIL_TAB.COUNT LOOP
		--FOR EMAIL_ADDRESS_RECORD IN EMAIL_ADDRESS_CUR LOOP
		  UTL_SMTP.RCPT(C, EMAIL_TAB(X));
		END LOOP;

		UTL_SMTP.OPEN_DATA(C);

		FOR X IN 1..EMAIL_TAB.COUNT LOOP
		--FOR EMAIL_ADDRESS_RECORD IN EMAIL_ADDRESS_CUR LOOP
		  SEND_HEADER('TO', '"'||EMAIL_TAB(X)||'" '||EMAIL_TAB(X));
		END LOOP;

		SEND_HEADER('Subject', V_SUBJECT);

		send_header('Content-Type', 'text/plain; charset=iso-8859-1');
		send_header('X-Mailer', 'Mailer by Oracle UTL_SMTP');
		send_header('From', 'noreply-apolloinventory@ftdi.com');
		utl_smtp.write_data(c, utl_tcp.CRLF);

		UTL_SMTP.WRITE_RAW_DATA(C, utl_raw.cast_to_raw(V_CONTENT));    

		UTL_SMTP.CLOSE_DATA(C);
		UTL_SMTP.QUIT(C);
		
		OUT_STATUS := 'Y';
   END IF;
		    
	EXCEPTION
    WHEN UTL_SMTP.TRANSIENT_ERROR OR UTL_SMTP.PERMANENT_ERROR THEN
		BEGIN
			UTL_SMTP.QUIT(C);
			EXCEPTION
				WHEN UTL_SMTP.TRANSIENT_ERROR OR UTL_SMTP.PERMANENT_ERROR THEN
					NULL; 
				-- WHEN THE SMTP SERVER IS DOWN OR UNAVAILABLE, WE DON'T HAVE
                -- A CONNECTION TO THE SERVER. THE QUIT CALL WILL RAISE AN
                -- EXCEPTION THAT WE CAN IGNORE.
		END;
		RAISE_APPLICATION_ERROR(-20000, 'FAILED TO SEND MAIL DUE TO THE FOLLOWING ERROR: ' || SQLERRM);

END SEND_INVENTORY_ALERTS;

PROCEDURE GET_AVAIL_VENDOR_PROD_INV (
    IN_PRODUCT_ID               IN    VARCHAR2,
    IN_VENDOR_ID                IN    VARCHAR2, 
    OUT_CUR           		      OUT   TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
DESCRIPTION: GET THE CURSOR OF INVENTORY TRACKING IDS WHICH ARE AVAILABLE FOR 
GIVEN PRODUCT AND VENDOR
-----------------------------------------------------------------------------*/
BEGIN
	
	OPEN OUT_CUR FOR 
	
	SELECT INV_TRK_ID FROM FTD_APPS.INV_TRK IT 
    JOIN FTD_APPS.VENDOR_PRODUCT VP ON VP.VENDOR_ID = IT.VENDOR_ID 
      AND VP.PRODUCT_SUBCODE_ID = IT.PRODUCT_ID AND VP.AVAILABLE = 'Y' AND VP.REMOVED = 'N'
    JOIN FTD_APPS.PRODUCT_MASTER PM ON PM.PRODUCT_ID = IT.PRODUCT_ID 
      AND PM.SHIP_METHOD_CARRIER = 'Y' AND PM.STATUS = 'A'
	WHERE IT.STATUS = 'A' AND IT.VENDOR_ID  = IN_VENDOR_ID AND IT.PRODUCT_ID = IN_PRODUCT_ID
	AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)));

END GET_AVAIL_VENDOR_PROD_INV;

PROCEDURE GET_INVENTORY_DETAIL (
    IN_PRODUCT_ID               IN    VARCHAR2,
    IN_VENDOR_ID                IN    VARCHAR2,
    IN_SHIP_DATE                IN    FTD_APPS.INV_TRK.START_DATE%TYPE,    
    OUT_CUR           		      OUT   TYPES.REF_CURSOR
) AS
/*------------------------------------------------------------------------------------
DESCRIPTION: GET THE CURSOR OF INVENTORY DETAIL FOR A GIVEN PRODUCT, VENDOR, SHIP DATE
--------------------------------------------------------------------------------------*/
BEGIN
	
	OPEN OUT_CUR FOR 
	
	SELECT 	PM.PRODUCT_NAME PRODUCT_NAME, 
			PM.SHIP_METHOD_CARRIER IS_VENDOR_DELIVERABLE, 
			PM.SHIP_METHOD_FLORIST IS_FLORIST_DELIVERABLE, 
			PM.STATUS PROD_STATUS,
			PM.PRODUCT_TYPE,
			VM.VENDOR_NAME VENDOR_NAME, 
			VP.AVAILABLE VENDOR_PROD_STATUS,
			IT.INV_TRK_ID INV_TRK_ID, 
			IT.PRODUCT_ID PRODUCT_ID, 
			IT.VENDOR_ID VENDOR_ID,			
			IT.SHUTDOWN_THRESHOLD_QTY, 
			IT.WARNING_THRESHOLD_QTY, 
			IT.START_DATE, 
			IT.END_DATE, 
			IT.REVISED_FORECAST_QTY TOT_INVENTORY, 
			(SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) TOT_SHIPPED, 
			0 PEND_ORDER_COUNT,
      IT.STATUS INV_STATUS
	FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC, FTD_APPS.VENDOR_PRODUCT VP, FTD_APPS.VENDOR_MASTER VM, FTD_APPS.PRODUCT_MASTER PM 
		WHERE IT.VENDOR_ID = ITAC.VENDOR_ID AND ITAC.VENDOR_ID = VM.VENDOR_ID AND VM.VENDOR_ID = VP.VENDOR_ID AND IT.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID
          AND IT.PRODUCT_ID = ITAC.PRODUCT_ID AND ITAC.PRODUCT_ID = PM.PRODUCT_ID 
          AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE 
          AND IT.PRODUCT_ID = IN_PRODUCT_ID AND IT.VENDOR_ID = IN_VENDOR_ID AND IN_SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE 
	GROUP BY IT.INV_TRK_ID, IT.PRODUCT_ID, IT.VENDOR_ID, PM.PRODUCT_NAME, PM.SHIP_METHOD_CARRIER, PM.SHIP_METHOD_FLORIST,PM.STATUS,PM.PRODUCT_TYPE, VM.VENDOR_NAME,
	VP.AVAILABLE, IT.SHUTDOWN_THRESHOLD_QTY, IT.WARNING_THRESHOLD_QTY, IT.START_DATE, IT.END_DATE, IT.FORECAST_QTY, IT.REVISED_FORECAST_QTY, IT.STATUS;
		
	EXCEPTION WHEN OTHERS THEN		
			RAISE_APPLICATION_ERROR(-20001, 'ERROR OCCURRED IN GET_INVENTORY_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256));		
	
END GET_INVENTORY_DETAIL;

PROCEDURE GET_PROD_PERIOD_INV_DETAIL (
    IN_PRODUCT_ID               IN    VARCHAR2,
    IN_SHIP_DATE                IN    FTD_APPS.INV_TRK.START_DATE%TYPE, 
    IN_INV_TRK_ID               IN    FTD_APPS.INV_TRK.INV_TRK_ID%TYPE,
    OUT_CUR           		    OUT   TYPES.REF_CURSOR
) AS

V_SQL VARCHAR2(4000) := '';

V_INV_AVAILBLE CHAR(1) := 'U';
V_UNAVAIL_EXISTS CHAR(1) := 'N';

V_START_DATE DATE;
V_END_DATE DATE;

/*----------------------------------------------------------------------------------------------------
DESCRIPTION: GET THE CURSOR OF INVENTORY DETAIL FOR A GIVEN PRODUCT, SHIP DATE/ INVENTORY TRACKING ID
IRRESPECTIVE OF VENDOR
ALL ORDER CNT IS SAVED IN INV_TRK_COUNT (PENDING+VENDOR ASSIGNED). GET THE CURRENT ORDERS FOR A PRODUCT, 
PERIOD FROM VENUS.INV_TRK_COUNT
------------------------------------------------------------------------------------------------------*/
BEGIN
	
	BEGIN
	
		SELECT  DISTINCT START_DATE, END_DATE INTO V_START_DATE, V_END_DATE FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = IN_PRODUCT_ID AND
		((IN_SHIP_DATE IS NOT NULL AND IN_SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE) OR (IN_INV_TRK_ID IS NOT NULL AND IT.INV_TRK_ID = IN_INV_TRK_ID));
		   
		EXCEPTION WHEN NO_DATA_FOUND THEN  
			V_INV_AVAILBLE := 'N';
	END;
	
	BEGIN
	
		SELECT  DISTINCT 'A' INTO V_INV_AVAILBLE FROM FTD_APPS.INV_TRK IT 
			WHERE IT.PRODUCT_ID = IN_PRODUCT_ID AND IT.STATUS = 'A' AND IT.START_DATE = V_START_DATE  AND IT.END_DATE = V_END_DATE;		

		EXCEPTION WHEN NO_DATA_FOUND THEN  
			V_INV_AVAILBLE := 'U';
	END;

	BEGIN
	
		SELECT  DISTINCT 'Y' INTO V_UNAVAIL_EXISTS FROM FTD_APPS.INV_TRK IT 
			WHERE IT.PRODUCT_ID = IN_PRODUCT_ID AND IT.STATUS IN ('U','X') AND IT.START_DATE = V_START_DATE  AND IT.END_DATE = V_END_DATE;

		EXCEPTION WHEN NO_DATA_FOUND THEN  
			V_UNAVAIL_EXISTS := 'N';
	END;

      
IF V_INV_AVAILBLE = 'A' THEN
	
	V_SQL := 'SELECT PM.PRODUCT_ID PRODUCT_ID, PM.PRODUCT_NAME PRODUCT_NAME, PM.SHIP_METHOD_CARRIER IS_VENDOR_DELIVERABLE, 
			PM.SHIP_METHOD_FLORIST IS_FLORIST_DELIVERABLE, PM.STATUS PROD_STATUS, PM.PRODUCT_TYPE, 
			TOT_INV.START_DATE START_DATE, TOT_INV.END_DATE END_DATE, TOT_INV.TOT_INVENTORY TOT_INVENTORY, 
			TOT_AVAIL_INV.WARNING_THRESHOLD_QTY WARNING_THRESHOLD_QTY, TOT_AVAIL_INV.SHUTDOWN_THRESHOLD_QTY SHUTDOWN_THRESHOLD_QTY, 
			CASE ''' || V_UNAVAIL_EXISTS || ''' WHEN ''N'' THEN (TOT_AVAIL_INV.TOT_INVENTORY - TOT_ORDERS.ALL_ORDER_COUNT) ELSE (TOT_AVAIL_INV.TOT_INVENTORY - (TOT_ORDERS.ALL_ORDER_COUNT - TOT_UNAVAIL_ASSIGNED_ORDERS.FTD_ORDER_COUNT))  END AS ON_HAND,
			TOT_ORDERS.ALL_ORDER_COUNT TOT_ORDER_COUNT, TOT_ASSIGNED_ORDERS.FTD_ORDER_COUNT TOT_SHIPPED FROM FTD_APPS.PRODUCT_MASTER PM       
			     
			LEFT JOIN  (SELECT IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE, SUM(IT.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, 
			SUM(IT.WARNING_THRESHOLD_QTY) WARNING_THRESHOLD_QTY, SUM(IT.REVISED_FORECAST_QTY) TOT_INVENTORY 
			FROM FTD_APPS.INV_TRK IT WHERE IT.START_DATE = ''' || V_START_DATE || ''' AND IT.END_DATE = ''' || V_END_DATE || ''' 
			GROUP BY IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE) TOT_INV ON TOT_INV.PRODUCT_ID = PM.PRODUCT_ID 
			
			LEFT JOIN (SELECT IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE, SUM(IT.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, 
			SUM(IT.WARNING_THRESHOLD_QTY) WARNING_THRESHOLD_QTY, SUM(IT.REVISED_FORECAST_QTY) TOT_INVENTORY 
			FROM FTD_APPS.INV_TRK IT, FTD_APPS.VENDOR_PRODUCT VP, FTD_APPS.VENDOR_MASTER VM 
			WHERE IT.VENDOR_ID = VP.VENDOR_ID  AND VM.VENDOR_ID = VP.VENDOR_ID AND VP.REMOVED = ''N'' 
			AND VM.ACTIVE = ''Y'' AND IT.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID AND IT.STATUS = ''A'' 
			AND IT.START_DATE = ''' || V_START_DATE || ''' AND IT.END_DATE = ''' || V_END_DATE || '''
			GROUP BY IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE) TOT_AVAIL_INV ON TOT_AVAIL_INV.PRODUCT_ID = PM.PRODUCT_ID 
      
			LEFT JOIN (SELECT ITC.PRODUCT_ID, (SUM(ITC.FTD_MSG_COUNT) - SUM(ITC.CAN_MSG_COUNT) - SUM(ITC.REJ_MSG_COUNT)) 
			ALL_ORDER_COUNT FROM VENUS.INV_TRK_COUNT ITC WHERE ITC.SHIP_DATE BETWEEN ''' || V_START_DATE || ''' AND ''' || V_END_DATE || '''
			 GROUP BY ITC.PRODUCT_ID) TOT_ORDERS ON TOT_ORDERS.PRODUCT_ID = PM.PRODUCT_ID        
			
			LEFT JOIN (SELECT ITAC.PRODUCT_ID, (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) FTD_ORDER_COUNT 
			FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC WHERE IT.VENDOR_ID = ITAC.VENDOR_ID 
			AND IT.PRODUCT_ID = ITAC.PRODUCT_ID  AND ITAC.SHIP_DATE BETWEEN IT.START_DATE 
			AND IT.END_DATE AND IT.START_DATE = ''' || V_START_DATE || ''' AND IT.END_DATE = ''' || V_END_DATE || '''  			 
			GROUP BY ITAC.PRODUCT_ID) TOT_ASSIGNED_ORDERS ON TOT_ASSIGNED_ORDERS.PRODUCT_ID = PM.PRODUCT_ID
							
			LEFT JOIN (SELECT ITAC.PRODUCT_ID, (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) FTD_ORDER_COUNT 
			FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC WHERE IT.VENDOR_ID = ITAC.VENDOR_ID 
			AND IT.PRODUCT_ID = ITAC.PRODUCT_ID  AND ITAC.SHIP_DATE BETWEEN IT.START_DATE 
			AND IT.END_DATE AND IT.START_DATE = ''' || V_START_DATE || ''' AND IT.END_DATE = ''' || V_END_DATE || '''  AND IT.STATUS NOT IN (''A'')			 
			GROUP BY ITAC.PRODUCT_ID) TOT_UNAVAIL_ASSIGNED_ORDERS ON TOT_UNAVAIL_ASSIGNED_ORDERS.PRODUCT_ID = PM.PRODUCT_ID

			WHERE PM.PRODUCT_ID = ''' || IN_PRODUCT_ID || '''';
ELSE

	V_SQL := 'SELECT PM.PRODUCT_ID PRODUCT_ID, PM.PRODUCT_NAME PRODUCT_NAME, PM.SHIP_METHOD_CARRIER IS_VENDOR_DELIVERABLE, 
			PM.SHIP_METHOD_FLORIST IS_FLORIST_DELIVERABLE, PM.STATUS PROD_STATUS, PM.PRODUCT_TYPE, 
			TOT_INV.START_DATE START_DATE, TOT_INV.END_DATE END_DATE, TOT_INV.TOT_INVENTORY TOT_INVENTORY, 
			TOT_INV.WARNING_THRESHOLD_QTY WARNING_THRESHOLD_QTY, TOT_INV.SHUTDOWN_THRESHOLD_QTY SHUTDOWN_THRESHOLD_QTY, 
			(TOT_INV.SHUTDOWN_THRESHOLD_QTY) ON_HAND,
			TOT_ORDERS.ALL_ORDER_COUNT TOT_ORDER_COUNT, TOT_ASSIGNED_ORDERS.FTD_ORDER_COUNT TOT_SHIPPED FROM FTD_APPS.PRODUCT_MASTER PM       
			
			LEFT JOIN  (SELECT IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE, SUM(IT.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, 
			SUM(IT.WARNING_THRESHOLD_QTY) WARNING_THRESHOLD_QTY, SUM(IT.REVISED_FORECAST_QTY) TOT_INVENTORY 
			FROM FTD_APPS.INV_TRK IT WHERE IT.START_DATE = ''' || V_START_DATE || ''' AND IT.END_DATE = ''' || V_END_DATE || ''' 
			GROUP BY IT.PRODUCT_ID, IT.START_DATE, IT.END_DATE) TOT_INV ON TOT_INV.PRODUCT_ID = PM.PRODUCT_ID 
						
            LEFT JOIN (SELECT ITC.PRODUCT_ID, (SUM(ITC.FTD_MSG_COUNT) - SUM(ITC.CAN_MSG_COUNT) - SUM(ITC.REJ_MSG_COUNT)) 
			ALL_ORDER_COUNT FROM VENUS.INV_TRK_COUNT ITC WHERE ITC.SHIP_DATE BETWEEN ''' || V_START_DATE || ''' AND ''' || V_END_DATE || '''
			 GROUP BY ITC.PRODUCT_ID) TOT_ORDERS ON TOT_ORDERS.PRODUCT_ID = PM.PRODUCT_ID   
			
			LEFT JOIN (SELECT ITAC.PRODUCT_ID, (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) FTD_ORDER_COUNT 
			FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC WHERE IT.VENDOR_ID = ITAC.VENDOR_ID 
			AND IT.PRODUCT_ID = ITAC.PRODUCT_ID  AND ITAC.SHIP_DATE BETWEEN IT.START_DATE 
			AND IT.END_DATE AND IT.START_DATE = ''' || V_START_DATE || ''' AND IT.END_DATE = ''' || V_END_DATE || '''  			 
			GROUP BY ITAC.PRODUCT_ID) TOT_ASSIGNED_ORDERS ON TOT_ASSIGNED_ORDERS.PRODUCT_ID = PM.PRODUCT_ID

			WHERE PM.PRODUCT_ID = ''' || IN_PRODUCT_ID || '''';
      
    END IF; 

	--dbms_output.put_line('vsql: ' || v_sql);

OPEN OUT_CUR FOR V_SQL;
				
EXCEPTION WHEN OTHERS THEN		
			RAISE_APPLICATION_ERROR(-20001, 'ERROR OCCURRED IN GET_PROD_PERIOD_INV_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256));
	
END GET_PROD_PERIOD_INV_DETAIL;

PROCEDURE GET_PROD_INV_DETAIL (
    IN_PRODUCT_ID               IN    VARCHAR2,  
    OUT_CUR           		    OUT   TYPES.REF_CURSOR
) AS

V_SQL VARCHAR2(4000) := '';
V_INV_AVAILBLE CHAR(1) := 'U';
V_UNAVAIL_EXISTS CHAR(1) := 'N';
V_MIN_START_DATE DATE;
V_MAX_END_DATE DATE;
/*----------------------------------------------------------------------------------------------------
DESCRIPTION: GET THE CURSOR OF INVENTORY DETAIL FOR A GIVEN PRODUCT, 
IRRESPECTIVE OF VENDOR AND PERIOD
------------------------------------------------------------------------------------------------------*/
BEGIN
	
  BEGIN
  
  SELECT  DISTINCT 'A' INTO V_INV_AVAILBLE FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = IN_PRODUCT_ID AND IT.STATUS = 'A'
  AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE))); 
  
  EXCEPTION WHEN NO_DATA_FOUND THEN  
    V_INV_AVAILBLE := 'U';	
    
  END;
      
  BEGIN 
   
  SELECT  DISTINCT 'Y' INTO V_UNAVAIL_EXISTS FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = IN_PRODUCT_ID AND IT.STATUS IN ('U','X')
  AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)));
  
  EXCEPTION WHEN NO_DATA_FOUND THEN  
    V_UNAVAIL_EXISTS := 'N';
    	
  END;
  
  BEGIN
  
  	SELECT MAX(END_DATE) INTO  V_MAX_END_DATE 	FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = IN_PRODUCT_ID 
  	AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= SYSDATE));
    
    SELECT MIN(START_DATE) INTO V_MIN_START_DATE FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = IN_PRODUCT_ID 
    AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)));
    
    EXCEPTION WHEN NO_DATA_FOUND THEN 
      V_INV_AVAILBLE := 'No Inventory'; 
    
  END;
  
	IF V_INV_AVAILBLE = 'No Inventory' THEN
	  V_SQL := 'SELECT NULL FROM DUAL';
	END IF;
  
	 
IF V_INV_AVAILBLE = 'A' THEN
	
	V_SQL := 'SELECT PM.PRODUCT_ID PRODUCT_ID, PM.PRODUCT_NAME PRODUCT_NAME, PM.SHIP_METHOD_CARRIER IS_VENDOR_DELIVERABLE, 
			PM.SHIP_METHOD_FLORIST IS_FLORIST_DELIVERABLE, PM.STATUS PROD_STATUS, PM.PRODUCT_TYPE, TOT_INV.TOT_INVENTORY TOT_INVENTORY, 
			TOT_AVAIL_INV.WARNING_THRESHOLD_QTY WARNING_THRESHOLD_QTY, TOT_AVAIL_INV.SHUTDOWN_THRESHOLD_QTY SHUTDOWN_THRESHOLD_QTY, 
			CASE ''' || V_UNAVAIL_EXISTS || ''' WHEN ''N'' THEN (TOT_AVAIL_INV.TOT_INVENTORY - TOT_ORDERS.ALL_ORDER_COUNT) ELSE (TOT_AVAIL_INV.TOT_INVENTORY - (TOT_ORDERS.ALL_ORDER_COUNT - TOT_UNAVAIL_ASSIGNED_ORDERS.FTD_ORDER_COUNT))  END AS ON_HAND,
			TOT_ORDERS.ALL_ORDER_COUNT TOT_ORDER_COUNT, TOT_ASSIGNED_ORDERS.FTD_ORDER_COUNT TOT_SHIPPED FROM FTD_APPS.PRODUCT_MASTER PM 

			LEFT JOIN  (SELECT IT.PRODUCT_ID, SUM(IT.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, 
			SUM(IT.WARNING_THRESHOLD_QTY) WARNING_THRESHOLD_QTY, SUM(IT.REVISED_FORECAST_QTY) TOT_INVENTORY 
			FROM FTD_APPS.INV_TRK IT WHERE ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)))
			GROUP BY IT.PRODUCT_ID) TOT_INV ON TOT_INV.PRODUCT_ID = PM.PRODUCT_ID 
			
			LEFT JOIN (SELECT IT.PRODUCT_ID, SUM(IT.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, 
			SUM(IT.WARNING_THRESHOLD_QTY) WARNING_THRESHOLD_QTY, SUM(IT.REVISED_FORECAST_QTY) TOT_INVENTORY 
			FROM FTD_APPS.INV_TRK IT, FTD_APPS.VENDOR_PRODUCT VP, FTD_APPS.VENDOR_MASTER VM 
			WHERE IT.VENDOR_ID = VP.VENDOR_ID  AND VM.VENDOR_ID = VP.VENDOR_ID AND VP.REMOVED = ''N'' 
			AND VM.ACTIVE = ''Y'' AND IT.PRODUCT_ID = VP.PRODUCT_SUBCODE_ID AND IT.STATUS = ''A'' 
			AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)))			
			GROUP BY IT.PRODUCT_ID) TOT_AVAIL_INV ON TOT_AVAIL_INV.PRODUCT_ID = PM.PRODUCT_ID 
			
      		LEFT JOIN (SELECT ITC.PRODUCT_ID, (SUM(ITC.FTD_MSG_COUNT) - SUM(ITC.CAN_MSG_COUNT) - SUM(ITC.REJ_MSG_COUNT)) 
      		ALL_ORDER_COUNT FROM VENUS.INV_TRK_COUNT ITC WHERE ITC.SHIP_DATE BETWEEN ''' || V_MIN_START_DATE || ''' AND ''' || V_MAX_END_DATE || '''
      		GROUP BY ITC.PRODUCT_ID) TOT_ORDERS ON TOT_ORDERS.PRODUCT_ID = PM.PRODUCT_ID  
			
			LEFT JOIN (SELECT IT.PRODUCT_ID, (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) FTD_ORDER_COUNT 
      		FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC WHERE IT.VENDOR_ID = ITAC.VENDOR_ID 
      		AND IT.PRODUCT_ID = ITAC.PRODUCT_ID  AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE 
      		AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)))
      		GROUP BY IT.PRODUCT_ID) TOT_ASSIGNED_ORDERS ON TOT_ASSIGNED_ORDERS.PRODUCT_ID = PM.PRODUCT_ID
			
			LEFT JOIN (SELECT IT.PRODUCT_ID, (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) FTD_ORDER_COUNT 
      		FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC WHERE IT.VENDOR_ID = ITAC.VENDOR_ID 
      		AND IT.PRODUCT_ID = ITAC.PRODUCT_ID  AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE AND IT.STATUS NOT IN (''A'')
      		AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)))
      		GROUP BY IT.PRODUCT_ID) TOT_UNAVAIL_ASSIGNED_ORDERS ON TOT_UNAVAIL_ASSIGNED_ORDERS.PRODUCT_ID = PM.PRODUCT_ID

			WHERE PM.PRODUCT_ID = ''' || IN_PRODUCT_ID || '''';
END IF;

IF V_INV_AVAILBLE = 'U' THEN

	V_SQL := 'SELECT PM.PRODUCT_ID PRODUCT_ID, PM.PRODUCT_NAME PRODUCT_NAME, PM.SHIP_METHOD_CARRIER IS_VENDOR_DELIVERABLE, 
			PM.SHIP_METHOD_FLORIST IS_FLORIST_DELIVERABLE, PM.STATUS PROD_STATUS, PM.PRODUCT_TYPE,
			TOT_INV.TOT_INVENTORY TOT_INVENTORY, TOT_INV.WARNING_THRESHOLD_QTY WARNING_THRESHOLD_QTY, TOT_INV.SHUTDOWN_THRESHOLD_QTY SHUTDOWN_THRESHOLD_QTY, 
			(TOT_INV.SHUTDOWN_THRESHOLD_QTY) ON_HAND, TOT_ORDERS.ALL_ORDER_COUNT TOT_ORDER_COUNT, TOT_ASSIGNED_ORDERS.FTD_ORDER_COUNT TOT_SHIPPED FROM FTD_APPS.PRODUCT_MASTER PM       
			
			LEFT JOIN  (SELECT IT.PRODUCT_ID, SUM(IT.SHUTDOWN_THRESHOLD_QTY) SHUTDOWN_THRESHOLD_QTY, 
			SUM(IT.WARNING_THRESHOLD_QTY) WARNING_THRESHOLD_QTY, SUM(IT.REVISED_FORECAST_QTY) TOT_INVENTORY 
			FROM FTD_APPS.INV_TRK IT WHERE ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)))
			GROUP BY IT.PRODUCT_ID) TOT_INV ON TOT_INV.PRODUCT_ID = PM.PRODUCT_ID 
						
            LEFT JOIN (SELECT ITC.PRODUCT_ID, (SUM(ITC.FTD_MSG_COUNT) - SUM(ITC.CAN_MSG_COUNT) - SUM(ITC.REJ_MSG_COUNT)) 
            ALL_ORDER_COUNT FROM VENUS.INV_TRK_COUNT ITC WHERE 	ITC.SHIP_DATE BETWEEN ''' || V_MIN_START_DATE || ''' AND ''' || V_MAX_END_DATE || ''' 
            GROUP BY ITC.PRODUCT_ID) TOT_ORDERS ON TOT_ORDERS.PRODUCT_ID = PM.PRODUCT_ID  
      		
      		LEFT JOIN (SELECT IT.PRODUCT_ID, (SUM(ITAC.FTD_MSG_COUNT) - SUM(ITAC.CAN_MSG_COUNT) - SUM(ITAC.REJ_MSG_COUNT)) FTD_ORDER_COUNT 
      		FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC WHERE IT.VENDOR_ID = ITAC.VENDOR_ID 
      		AND IT.PRODUCT_ID = ITAC.PRODUCT_ID  AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE 
      		AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)))
      		GROUP BY IT.PRODUCT_ID) TOT_ASSIGNED_ORDERS ON TOT_ASSIGNED_ORDERS.PRODUCT_ID = PM.PRODUCT_ID

			WHERE PM.PRODUCT_ID = ''' || IN_PRODUCT_ID || '''';
      
    END IF; 

	--dbms_output.put_line('vsql: ' || v_sql);

OPEN OUT_CUR FOR V_SQL;
				
EXCEPTION WHEN OTHERS THEN		
			RAISE_APPLICATION_ERROR(-20001, 'ERROR OCCURRED IN GET_PROD_INV_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256));
	
END GET_PROD_INV_DETAIL;

FUNCTION IS_SHUTDOWN_REACHED ( 
      IN_INV_TRK_ID  		    IN    FTD_APPS.INV_TRK.INV_TRK_ID%TYPE
) RETURN VARCHAR2 AS
/*----------------------------------------------------------------------------------------------------------------
DESCRIPTION: FUNCTION USED TO CHECK IF ONHAND IS GREATER THAN SHUTDOWN THRESHOLD. RETURNS Y, IF IT IS
ELSE N, IF IT IS NOT REACHED
------------------------------------------------------------------------------------------------------------------*/
	V_ON_HAND number;
	V_SHUTDOWN number;
	"OUT_SHUTDOWN_REACHED" varchar2(1);
BEGIN
  V_ON_HAND := CALCULATE_ONHAND(IN_INV_TRK_ID,null);
  
  SELECT SHUTDOWN_THRESHOLD_QTY INTO V_SHUTDOWN FROM FTD_APPS.INV_TRK WHERE INV_TRK_ID = IN_INV_TRK_ID;    
      
		IF V_SHUTDOWN >= V_ON_HAND THEN 
			OUT_SHUTDOWN_REACHED := 'Y';
		ELSE 
			OUT_SHUTDOWN_REACHED := 'N';
		END IF;
	  
RETURN OUT_SHUTDOWN_REACHED;
      
END IS_SHUTDOWN_REACHED;

PROCEDURE GET_VENUS_INV_TRK (
  IN_PRODUCT_ID               IN    VENUS.INV_TRK_COUNT.PRODUCT_ID%TYPE,
  IN_SHIP_DATE                IN    VENUS.INV_TRK_COUNT.SHIP_DATE%TYPE,    
  OUT_CUR                     OUT   TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select ftd_msg_count, can_msg_count, rej_msg_count
        from venus.inv_trk_count
        where product_id = IN_PRODUCT_ID
        and ship_date = in_ship_date;

END GET_VENUS_INV_TRK;

PROCEDURE GET_VENUS_INV_TRK_ASSIGNED (
  IN_PRODUCT_ID               IN    VENUS.INV_TRK_ASSIGNED_COUNT.PRODUCT_ID%TYPE,
  IN_SHIP_DATE                IN    VENUS.INV_TRK_ASSIGNED_COUNT.SHIP_DATE%TYPE,    
  IN_VENDOR_ID                IN    VENUS.INV_TRK_ASSIGNED_COUNT.VENDOR_ID%TYPE,
  OUT_CUR                     OUT   TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select ftd_msg_count, can_msg_count, rej_msg_count
        from venus.inv_trk_assigned_count
        where product_id = in_product_id
        and vendor_id = in_vendor_id
        and ship_date = in_ship_date;

END GET_VENUS_INV_TRK_ASSIGNED;

PROCEDURE UPDATE_VENUS_INV_TRK (
  IN_PRODUCT_ID               IN    VENUS.INV_TRK_COUNT.PRODUCT_ID%TYPE,
  IN_SHIP_DATE                IN    VENUS.INV_TRK_COUNT.SHIP_DATE%TYPE,
  IN_FTD_QTY                  IN    VENUS.INV_TRK_COUNT.FTD_MSG_COUNT%TYPE,
  IN_REJ_QTY                  IN    VENUS.INV_TRK_COUNT.REJ_MSG_COUNT%TYPE,
  IN_CAN_QTY                  IN    VENUS.INV_TRK_COUNT.CAN_MSG_COUNT%TYPE,
  OUT_STATUS                  OUT   VARCHAR2,
  OUT_MESSAGE                 OUT   VARCHAR2
) AS

BEGIN

    update venus.inv_trk_count
    set ftd_msg_count = IN_FTD_QTY,
        rej_msg_count = IN_REJ_QTY,
        can_msg_count = IN_CAN_QTY
    where product_id = IN_PRODUCT_ID
    and ship_date = IN_SHIP_DATE;

    OUT_STATUS := 'Y';

    EXCEPTION
        WHEN OTHERS THEN
            OUT_STATUS := 'N';
            OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_VENUS_INV_TRK [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENUS_INV_TRK;

PROCEDURE UPDATE_VENUS_INV_TRK_ASSIGNED (
  IN_PRODUCT_ID               IN    VENUS.INV_TRK_ASSIGNED_COUNT.PRODUCT_ID%TYPE,
  IN_SHIP_DATE                IN    VENUS.INV_TRK_ASSIGNED_COUNT.SHIP_DATE%TYPE,    
  IN_VENDOR_ID                IN    VENUS.INV_TRK_ASSIGNED_COUNT.VENDOR_ID%TYPE,
  IN_FTD_QTY                  IN    VENUS.INV_TRK_ASSIGNED_COUNT.FTD_MSG_COUNT%TYPE,
  IN_REJ_QTY                  IN    VENUS.INV_TRK_ASSIGNED_COUNT.REJ_MSG_COUNT%TYPE,
  IN_CAN_QTY                  IN    VENUS.INV_TRK_ASSIGNED_COUNT.CAN_MSG_COUNT%TYPE,
  OUT_STATUS                  OUT   VARCHAR2,
  OUT_MESSAGE                 OUT   VARCHAR2
) AS

BEGIN

    update venus.inv_trk_assigned_count
    set ftd_msg_count = IN_FTD_QTY,
        rej_msg_count = IN_REJ_QTY,
        can_msg_count = IN_CAN_QTY
    where product_id = IN_PRODUCT_ID
    and vendor_id = IN_VENDOR_ID
    and ship_date = IN_SHIP_DATE;

    OUT_STATUS := 'Y';

    EXCEPTION
        WHEN OTHERS THEN
            OUT_STATUS := 'N';
            OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_VENUS_INV_TRK_ASSIGNED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENUS_INV_TRK_ASSIGNED;

FUNCTION GET_INV_PERIODS (
	IN_PERIOD_STATUS        IN 		FTD_APPS.INV_TRK.STATUS%TYPE,
	IN_PRODUCT_IDS          IN 		VARCHAR2,
	IN_PRODUCT_STATUS       IN 		FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
	IN_START_DATE           IN 		FTD_APPS.INV_TRK.START_DATE%TYPE,
	IN_END_DATE             IN 		FTD_APPS.INV_TRK.END_DATE%TYPE, 
	IN_ON_HAND              IN 		NUMBER
) RETURN VARCHAR2 AS

	-- VARIABLES
	V_SQL           VARCHAR2 (4000) := ' SELECT TO_CHAR(IT.START_DATE) START_DATE, (IT.REVISED_FORECAST_QTY - SUM(ITAC.FTD_MSG_COUNT) + SUM(ITAC.CAN_MSG_COUNT) + SUM(ITAC.REJ_MSG_COUNT )) ON_HAND FROM FTD_APPS.INV_TRK IT, VENUS.INV_TRK_ASSIGNED_COUNT ITAC, FTD_APPS.PRODUCT_MASTER PM  
	WHERE IT.VENDOR_ID = ITAC.VENDOR_ID  AND IT.PRODUCT_ID = ITAC.PRODUCT_ID AND ITAC.SHIP_DATE BETWEEN IT.START_DATE AND IT.END_DATE 
	AND PM.PRODUCT_ID = IT.PRODUCT_ID AND IT.PRODUCT_ID IN (' || IN_PRODUCT_IDS ||') ';
	
	V_CONDS 		VARCHAR2 (4000) := ' ';
	
	V_GROUP_BY      VARCHAR2 (1000) := ' GROUP BY  IT.SHUTDOWN_THRESHOLD_QTY, IT.START_DATE, IT.REVISED_FORECAST_QTY ';
	V_HAVING		    VARCHAR2 (1000) := ' ';
	V_ORDER_BY      VARCHAR2 (1000) := ' ORDER BY IT.START_DATE DESC '; 
	
	V_START_DATES_LIST 	VARCHAR2(32600);

	-- CURSORS
	OUT_INV_PERIODS_CUR TYPES.REF_CURSOR;

	TYPE DATE_TAB_TYP IS TABLE OF DATE INDEX BY PLS_INTEGER;
	TYPE NUMBER_TAB_TYP IS TABLE OF NUMBER INDEX BY PLS_INTEGER;
	
	ONHAND_TAB             NUMBER_TAB_TYP;
	DATE_TAB               DATE_TAB_TYP;
 

BEGIN

  IF IN_PERIOD_STATUS IS NOT NULL THEN 
    CASE WHEN IN_PERIOD_STATUS = 'U' THEN
      V_CONDS    := V_CONDS || ' AND IT.STATUS IN (''U'',''N'') ';
    ELSE
      V_CONDS    := V_CONDS || ' AND IT.STATUS IN ('''|| IN_PERIOD_STATUS ||''') ';
    END CASE;
  END IF;

	IF IN_PRODUCT_STATUS IS NOT NULL THEN
		V_CONDS    := V_CONDS || ' AND PM.STATUS IN (''' || IN_PRODUCT_STATUS ||''') ';
	END IF;

	IF IN_START_DATE IS NOT NULL THEN
		V_CONDS   := V_CONDS || ' AND IT.START_DATE >= TO_DATE(''' || TO_CHAR(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;

	IF IN_END_DATE IS NOT NULL THEN
		V_CONDS   := V_CONDS || ' AND IT.END_DATE <= TO_DATE(''' || TO_CHAR(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
	END IF;	
	
	IF IN_ON_HAND IS NOT NULL THEN
		V_HAVING := ' HAVING (IT.REVISED_FORECAST_QTY - SUM(ITAC.FTD_MSG_COUNT) + SUM(ITAC.CAN_MSG_COUNT) + SUM(ITAC.REJ_MSG_COUNT )) < ' || IN_ON_HAND;	
	END IF;

	V_SQL := V_SQL || V_CONDS || V_GROUP_BY || V_HAVING || V_ORDER_BY;
  
  	--dbms_output.put_line('vsql: ' || v_sql);

	OPEN OUT_INV_PERIODS_CUR FOR V_SQL;  
	FETCH OUT_INV_PERIODS_CUR BULK COLLECT INTO DATE_TAB, ONHAND_TAB;
	CLOSE OUT_INV_PERIODS_CUR;

	IF DATE_TAB.COUNT > 0 THEN
		FOR x IN 1..DATE_TAB.COUNT LOOP
			V_START_DATES_LIST := ''''|| DATE_TAB(x) || ''',' || V_START_DATES_LIST;
		END LOOP;
	END IF;

	IF V_START_DATES_LIST IS NOT NULL THEN
		V_START_DATES_LIST := SUBSTR(V_START_DATES_LIST, 1, length(V_START_DATES_LIST)-1);
	END IF;
  
  	RETURN V_START_DATES_LIST;

END GET_INV_PERIODS;


PROCEDURE GET_PRODUCT_PERIODS (
  IN_PRODUCT_ID               IN    FTD_APPS.INV_TRK.PRODUCT_ID%TYPE,
  OUT_CUR                     OUT   TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
DESCRIPTION: GET THE CURSOR OF INVENTORY TRACKING Periods That ARE defined FOR 
GIVEN PRODUCT
-----------------------------------------------------------------------------*/
BEGIN
	
	OPEN OUT_CUR FOR 
	
	SELECT DISTINCT START_DATE, END_DATE FROM FTD_APPS.INV_TRK IT WHERE IT.PRODUCT_ID = IN_PRODUCT_ID
	AND ((TRUNC(SYSDATE) BETWEEN IT.START_DATE AND IT. END_DATE) OR (IT.START_DATE >= TRUNC(SYSDATE)));

END GET_PRODUCT_PERIODS;

FUNCTION CHECK_PRODUCT_SHUTDOWN
(
  IN_PRODUCT_ID               IN    VENUS.INV_TRK_ASSIGNED_COUNT.PRODUCT_ID%TYPE,
  IN_SHIP_DATE                IN    VENUS.INV_TRK_ASSIGNED_COUNT.SHIP_DATE%TYPE  
) RETURN VARCHAR2 AS

v_shutdown_threshold_qty     FTD_APPS.INV_TRK.SHUTDOWN_THRESHOLD_QTY%TYPE;
v_revised_forecast_qty       FTD_APPS.INV_TRK.REVISED_FORECAST_QTY%TYPE;
v_start_date                 FTD_APPS.INV_TRK.START_DATE%TYPE;
v_end_date                   FTD_APPS.INV_TRK.END_DATE%TYPE;
v_ftd_msg_count              VENUS.INV_TRK_COUNT.FTD_MSG_COUNT%TYPE;
v_can_msg_count              VENUS.INV_TRK_COUNT.CAN_MSG_COUNT%TYPE;
v_rej_msg_count              VENUS.INV_TRK_COUNT.REJ_MSG_COUNT%TYPE;
v_inventory_count            number;
v_ftd_msg_count_na           VENUS.INV_TRK_COUNT.FTD_MSG_COUNT%TYPE;
v_can_msg_count_na           VENUS.INV_TRK_COUNT.CAN_MSG_COUNT%TYPE;
v_rej_msg_count_na           VENUS.INV_TRK_COUNT.REJ_MSG_COUNT%TYPE;
OUT_SHUTDOWN_LVL_REACHED     varchar2(10);

BEGIN

  OUT_SHUTDOWN_LVL_REACHED  := 'N';

  BEGIN

    SELECT nvl(sum(shutdown_threshold_qty), 0), nvl(sum(revised_forecast_qty), 0), min(start_date), max(end_date)
    INTO   v_shutdown_threshold_qty, v_revised_forecast_qty, v_start_date, v_end_date
    FROM   ftd_apps.inv_trk it,
           ftd_apps.product_master pm,
           ftd_apps.vendor_product vp
    WHERE  it.product_id = IN_PRODUCT_ID
    AND    IN_SHIP_DATE between it.start_date and it.end_date
    AND    it.status not in ('N', 'U', 'X')
    AND    pm.product_id = it.product_id
    AND    pm.status = 'A'
    AND    vp.vendor_id = it.vendor_id
    AND    vp.product_subcode_id = pm.product_id
    AND    vp.available = 'Y';

    EXCEPTION WHEN NO_DATA_FOUND THEN
      OUT_SHUTDOWN_LVL_REACHED := 'Y';

  END;

  IF v_revised_forecast_qty = 0 THEN
    OUT_SHUTDOWN_LVL_REACHED := 'Y';
  END IF;

  IF OUT_SHUTDOWN_LVL_REACHED = 'N' THEN

    BEGIN

      SELECT SUM(itc.ftd_msg_count), SUM(itc.can_msg_count), SUM(itc.rej_msg_count)
      INTO   v_ftd_msg_count, v_can_msg_count, v_rej_msg_count
      FROM   venus.inv_trk_count itc
      WHERE  itc.product_id = IN_PRODUCT_ID
      AND    itc.ship_date between v_start_date and v_end_date;

      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_ftd_msg_count := 0;
        v_can_msg_count := 0;
        v_rej_msg_count := 0;

    END;

    BEGIN

      SELECT NVL(SUM(itac.ftd_msg_count), 0), NVL(SUM(itac.can_msg_count), 0), NVL(SUM(itac.rej_msg_count), 0)
      INTO   v_ftd_msg_count_na, v_can_msg_count_na, v_rej_msg_count_na
      FROM   venus.inv_trk_assigned_count itac,
             ftd_apps.product_master pm,
             ftd_apps.vendor_master vm,
             ftd_apps.inv_trk it,
             ftd_apps.vendor_product vp
      WHERE  itac.product_id = IN_PRODUCT_ID
      AND    itac.ship_date between v_start_date and v_end_date
      AND    itac.vendor_id = vm.vendor_id
      AND    itac.product_id = pm.product_id
      AND    it.product_id = pm.product_id
      AND    it.vendor_id = vm.vendor_id
      AND    it.start_date = v_start_date
      AND    it.end_date = v_end_date
      AND    vp.vendor_id = it.vendor_id
      AND    vp.product_subcode_id = pm.product_id
      AND    (it.status in ('N', 'U', 'X')
             OR pm.status = 'U'
             OR vm.active = 'N'
             or vp.available = 'N');

      EXCEPTION WHEN NO_DATA_FOUND THEN
        v_ftd_msg_count_na := 0;
        v_can_msg_count_na := 0;
        v_rej_msg_count_na := 0;

    END;

    v_ftd_msg_count := v_ftd_msg_count - v_ftd_msg_count_na;
    v_can_msg_count := v_can_msg_count - v_can_msg_count_na;
    v_rej_msg_count := v_rej_msg_count - v_rej_msg_count_na;

    v_inventory_count := v_revised_forecast_qty - v_ftd_msg_count + v_can_msg_count + v_rej_msg_count;

    --dbms_output.put_line('v_inventory_count: ' || v_inventory_count || '  v_shutdown_threshold_qty: ' || v_shutdown_threshold_qty);

    IF (v_inventory_count <= v_shutdown_threshold_qty) THEN
      OUT_SHUTDOWN_LVL_REACHED  := 'Y';
    END IF;

  END IF;

  RETURN OUT_SHUTDOWN_LVL_REACHED;

END CHECK_PRODUCT_SHUTDOWN;

/*******************************************************************************
#18018 -- Putting back the old procs for inv search for reporting purpose
********************************************************************************/

/******************************************************************************
                        SEARCH_INV_TRK_PRODUCT_VENDOR

Description: This procedure is responsible for retrieving inventory records
             based on primary search of vendor ids and product ids. It first
             populate the ids in the temp table, then calls an overloaded
             procedure to retrieve data.

******************************************************************************/
PROCEDURE SEARCH_INV_TRK_PRODUCT_VENDOR
(
IN_PRODUCT_IDS                    IN VARCHAR2,
IN_VENDOR_IDS                     IN VARCHAR2,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_PRODUCT_STATUS                 IN FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_VENDOR_PRODUCT_STATUS          	IN FTD_APPS.VENDOR_PRODUCT.AVAILABLE%TYPE,
IN_ON_HAND                        	IN NUMBER,
OUT_RECORD_COUNT                 	OUT NUMBER,
OUT_CUR                          	OUT TYPES.REF_CURSOR
)
AS

v_max_records               number;

-- cursors
OUT_PRODUCT_VENDOR_CUR      types.ref_cursor;

BEGIN

  -- insert vendor ids in temp table ftd_apps.inv_trk_vendor_temp.
  INSERT_INPUT_ID_TEMP(IN_PRODUCT_IDS, IN_VENDOR_IDS);

  -- initialize the record count returned
  OUT_RECORD_COUNT := 0;

  BEGIN
    select to_number(value) into v_max_records from frp.global_parms where context = 'OPM_INV_CTXT' and name = 'REC_PAGE_SIZE' and value is not null;
    EXCEPTION WHEN OTHERS THEN v_max_records := 200;
  END;

  SEARCH_INV_TRK_PRODUCT_VENDOR
  (
  IN_PRODUCT_IDS                    => IN_PRODUCT_IDS,
  IN_VENDOR_IDS                     => IN_VENDOR_IDS,
  IN_START_DATE                     => IN_START_DATE,
  IN_END_DATE                       => IN_END_DATE,
  IN_PRODUCT_STATUS                 => IN_PRODUCT_STATUS,
  IN_VENDOR_PRODUCT_STATUS          => IN_VENDOR_PRODUCT_STATUS,
  IN_ON_HAND                        => IN_ON_HAND,
  IN_MAX_RECORD_COUNT       		=> v_max_records,
  OUT_RECORD_COUNT                  => OUT_RECORD_COUNT,
  OUT_CUR                           => OUT_CUR
  );

END SEARCH_INV_TRK_PRODUCT_VENDOR;


/******************************************************************************
                        SEARCH_INV_TRK_PRODUCT_VENDOR

Description: This procedure is responsible for retrieving inventory records
             based on primary search of vendor ids and product ids.

******************************************************************************/
PROCEDURE SEARCH_INV_TRK_PRODUCT_VENDOR
(
IN_PRODUCT_IDS                    IN VARCHAR2,
IN_VENDOR_IDS                     IN VARCHAR2,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_PRODUCT_STATUS                 IN FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_VENDOR_PRODUCT_STATUS          IN FTD_APPS.VENDOR_PRODUCT.AVAILABLE%TYPE,
IN_ON_HAND                        IN NUMBER,
IN_MAX_RECORD_COUNT               IN NUMBER,
OUT_RECORD_COUNT                  OUT NUMBER,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS

-- index by table to store cursor result sets
TYPE varchar_tab_typ IS TABLE OF VARCHAR2(4000) INDEX BY PLS_INTEGER;
TYPE number_tab_typ  IS TABLE OF NUMBER         INDEX BY PLS_INTEGER;
TYPE date_tab_typ    IS TABLE OF DATE           INDEX BY PLS_INTEGER;

-- variables
v_sql_product_vendor        varchar2 (4000);
v_inv_trk_id                number_tab_typ;
v_product_id                varchar_tab_typ;
v_product_name              varchar_tab_typ;
v_product_status            varchar_tab_typ;
v_product_updated_by        varchar_tab_typ;
v_exception_start_date      date_tab_typ;
v_vendor_id                 varchar_tab_typ;
v_vendor_name               varchar_tab_typ;
v_vendor_product_status     varchar_tab_typ;
v_vendor_product_updated_by varchar_tab_typ;
v_shutdown_threshold_qty    number_tab_typ;
v_start_date                date_tab_typ;
v_end_date                  date_tab_typ;
v_forecast_qty              number_tab_typ;
v_revised_forecast_qty      number_tab_typ;
v_ftd_msg_count             number_tab_typ;
v_can_msg_count             number_tab_typ;
v_rej_msg_count             number_tab_typ;
v_on_hand                   number_tab_typ;

v_max_records               number;

-- cursors
OUT_PRODUCT_VENDOR_CUR      types.ref_cursor;

BEGIN

  -- initialize the record count returned
  OUT_RECORD_COUNT := 0;

  v_max_records := IN_MAX_RECORD_COUNT;

  v_sql_product_vendor       := GET_PRODUCT_VENDOR_SQL(IN_PRODUCT_IDS, IN_VENDOR_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_VENDOR_PRODUCT_STATUS, IN_ON_HAND);

  DELETE FROM ftd_apps.inv_trk_search_temp;

  OPEN  OUT_PRODUCT_VENDOR_CUR FOR v_sql_product_vendor;
  FETCH OUT_PRODUCT_VENDOR_CUR    BULK COLLECT INTO
        v_inv_trk_id,
        v_product_id,
        v_product_name,
        v_product_status,
        v_product_updated_by,
        v_exception_start_date,
        v_vendor_id,
        v_vendor_name,
        v_vendor_product_status,
        v_vendor_product_updated_by,
        v_shutdown_threshold_qty,
        v_start_date,
        v_end_date,
        v_forecast_qty,
        v_revised_forecast_qty,
        v_ftd_msg_count,
        v_can_msg_count,
        v_rej_msg_count,
        v_on_hand;
  CLOSE OUT_PRODUCT_VENDOR_CUR;

  -- store product vendor data
  FORALL x IN 1..v_inv_trk_id.count
    INSERT INTO ftd_apps.inv_trk_search_temp
                (inv_trk_id,      product_id,      product_name,      product_status,      product_updated_by,      exception_start_date,      vendor_id,      vendor_name,      vendor_product_status,      vendor_product_updated_by,      shutdown_threshold_qty,      start_date,      end_date,      forecast_qty,      revised_forecast_qty,      ftd_msg_count,      can_msg_count,      rej_msg_count,      on_hand)
         VALUES (v_inv_trk_id(x), v_product_id(x), v_product_name(x), v_product_status(x), v_product_updated_by(x), v_exception_start_date(x), v_vendor_id(x), v_vendor_name(x), v_vendor_product_status(x), v_vendor_product_updated_by(x), v_shutdown_threshold_qty(x), v_start_date(x), v_end_date(x), v_forecast_qty(x), v_revised_forecast_qty(x), v_ftd_msg_count(x), v_can_msg_count(x), v_rej_msg_count(x), v_on_hand(x));


  -- OUTPUTS
  select count(*) into OUT_RECORD_COUNT from ftd_apps.inv_trk_search_temp where rownum <= v_max_records;

  OPEN OUT_CUR FOR
    select *
    from (
         select   inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, to_char(start_date, 'MM/dd/yyyy') start_date, to_char(end_date, 'MM/dd/yyyy') end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand, (ftd_msg_count - can_msg_count - rej_msg_count) shipped_shipping, CASE revised_forecast_qty WHEN 0 THEN 0 ELSE (((ftd_msg_count - can_msg_count - rej_msg_count) / revised_forecast_qty ) * 100) END as sell_thru_percent
         from     ftd_apps.inv_trk_search_temp a
         order by product_id, a.start_date desc, vendor_id
         )
    where rownum <= v_max_records;


END SEARCH_INV_TRK_PRODUCT_VENDOR;

/******************************************************************************
                        SEARCH_INV_TRK_PRODUCT

Description: This procedure is responsible for retrieving inventory records
             based on primary search of product ids. It first populates the
             product ids in the temp table. Then it calls an overloaded procedure
             to retrieve results.

******************************************************************************/
PROCEDURE SEARCH_INV_TRK_PRODUCT
(
IN_PRODUCT_IDS                    IN VARCHAR2,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_PRODUCT_STATUS                 IN FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_VENDOR_PRODUCT_STATUS          IN FTD_APPS.VENDOR_PRODUCT.AVAILABLE%TYPE,
IN_ON_HAND                        IN NUMBER,
OUT_RECORD_COUNT                  OUT NUMBER,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS

v_max_records               number;

BEGIN

  -- insert vendor ids in temp table ftd_apps.inv_trk_vendor_temp.
  INSERT_INPUT_ID_TEMP(IN_PRODUCT_IDS, null);

  -- initialize the record count returned
  OUT_RECORD_COUNT := 0;

  BEGIN
    select to_number(value) into v_max_records from frp.global_parms where context = 'OPM_INV_CTXT' and name = 'REC_PAGE_SIZE' and value is not null;
    EXCEPTION WHEN OTHERS THEN v_max_records := 200;
  END;

  SEARCH_INV_TRK_PRODUCT
  (
  IN_PRODUCT_IDS                    => IN_PRODUCT_IDS,
  IN_START_DATE                     => IN_START_DATE,
  IN_END_DATE                       => IN_END_DATE,
  IN_PRODUCT_STATUS                 => IN_PRODUCT_STATUS,
  IN_VENDOR_PRODUCT_STATUS          => IN_VENDOR_PRODUCT_STATUS,
  IN_ON_HAND                        => IN_ON_HAND,
  IN_MAX_RECORD_COUNT               => v_max_records,
  OUT_RECORD_COUNT                  => OUT_RECORD_COUNT,
  OUT_CUR                           => OUT_CUR
  );

END SEARCH_INV_TRK_PRODUCT;


/******************************************************************************
                        SEARCH_INV_TRK_PRODUCT

Description: This procedure is responsible for retrieving inventory records
             based on primary search of product ids.

******************************************************************************/
PROCEDURE SEARCH_INV_TRK_PRODUCT
(
IN_PRODUCT_IDS                    IN VARCHAR2,
IN_START_DATE                     IN FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE                       IN FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_PRODUCT_STATUS                 IN FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_VENDOR_PRODUCT_STATUS          IN FTD_APPS.VENDOR_PRODUCT.AVAILABLE%TYPE,
IN_ON_HAND                        IN NUMBER,
IN_MAX_RECORD_COUNT       IN NUMBER,
OUT_RECORD_COUNT                 OUT NUMBER,
OUT_CUR                          OUT TYPES.REF_CURSOR
)
AS

-- index by table to store cursor result sets
TYPE varchar_tab_typ IS TABLE OF VARCHAR2(4000) INDEX BY PLS_INTEGER;
TYPE number_tab_typ  IS TABLE OF NUMBER         INDEX BY PLS_INTEGER;
TYPE date_tab_typ    IS TABLE OF DATE           INDEX BY PLS_INTEGER;

-- variables
v_sql_product_vendor        varchar2 (4000);
v_sql_product_summary       varchar2 (4000);
v_inv_trk_id                number_tab_typ;
v_product_id                varchar_tab_typ;
v_product_name              varchar_tab_typ;
v_product_status            varchar_tab_typ;
v_product_updated_by        varchar_tab_typ;
v_exception_start_date      date_tab_typ;
v_vendor_id                 varchar_tab_typ;
v_vendor_name               varchar_tab_typ;
v_vendor_product_status     varchar_tab_typ;
v_vendor_product_updated_by varchar_tab_typ;
v_shutdown_threshold_qty    number_tab_typ;
v_start_date                date_tab_typ;
v_end_date                  date_tab_typ;
v_forecast_qty              number_tab_typ;
v_revised_forecast_qty      number_tab_typ;
v_ftd_msg_count             number_tab_typ;
v_can_msg_count             number_tab_typ;
v_rej_msg_count             number_tab_typ;
v_on_hand                   number_tab_typ;

v_max_records               number;

-- cursors
OUT_PRODUCT_VENDOR_CUR      types.ref_cursor;
OUT_SUMMARY_CUR             types.ref_cursor;

BEGIN

-- initialize the record count returned
OUT_RECORD_COUNT := 0;

  v_max_records := IN_MAX_RECORD_COUNT;

  v_sql_product_vendor   := GET_PRODUCT_VENDOR_SQL(IN_PRODUCT_IDS, null, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, IN_VENDOR_PRODUCT_STATUS, null);
  v_sql_product_summary  := GET_PRODUCT_SUMMARY_SQL(IN_PRODUCT_IDS, IN_START_DATE, IN_END_DATE, IN_PRODUCT_STATUS, null);

  DELETE FROM ftd_apps.inv_trk_search_prod_vndr_temp;
  DELETE FROM ftd_apps.inv_trk_search_product_temp;
  DELETE FROM ftd_apps.inv_trk_search_temp;

  OPEN  OUT_PRODUCT_VENDOR_CUR FOR v_sql_product_vendor;
  FETCH OUT_PRODUCT_VENDOR_CUR    BULK COLLECT INTO
        v_inv_trk_id,
        v_product_id,
        v_product_name,
        v_product_status,
        v_product_updated_by,
        v_exception_start_date,
        v_vendor_id,
        v_vendor_name,
        v_vendor_product_status,
        v_vendor_product_updated_by,
        v_shutdown_threshold_qty,
        v_start_date,
        v_end_date,
        v_forecast_qty,
        v_revised_forecast_qty,
        v_ftd_msg_count,
        v_can_msg_count,
        v_rej_msg_count,
        v_on_hand;
  CLOSE OUT_PRODUCT_VENDOR_CUR;

  -- store vendor data
  FORALL x IN 1..v_inv_trk_id.count
    INSERT INTO ftd_apps.inv_trk_search_prod_vndr_temp
                (inv_trk_id,      product_id,      product_name,      product_status,      product_updated_by,      exception_start_date,      vendor_id,      vendor_name,      vendor_product_status,      vendor_product_updated_by,      shutdown_threshold_qty,      start_date,      end_date,      forecast_qty,      revised_forecast_qty,      ftd_msg_count,      can_msg_count,      rej_msg_count,      on_hand)
         VALUES (v_inv_trk_id(x), v_product_id(x), v_product_name(x), v_product_status(x), v_product_updated_by(x), v_exception_start_date(x), v_vendor_id(x), v_vendor_name(x), v_vendor_product_status(x), v_vendor_product_updated_by(x), v_shutdown_threshold_qty(x), v_start_date(x), v_end_date(x), v_forecast_qty(x), v_revised_forecast_qty(x), v_ftd_msg_count(x), v_can_msg_count(x), v_rej_msg_count(x), v_on_hand(x));


  OPEN  OUT_SUMMARY_CUR FOR v_sql_product_summary;
  FETCH OUT_SUMMARY_CUR           BULK COLLECT INTO
        v_product_id,
        v_product_name,
        v_product_status,
        v_product_updated_by,
        v_exception_start_date,
        v_vendor_name,
        v_start_date,
        v_end_date,
        v_forecast_qty,
        v_revised_forecast_qty,
        v_ftd_msg_count,
        v_can_msg_count,
        v_rej_msg_count,
        v_on_hand;
  CLOSE OUT_SUMMARY_CUR;

  -- store summary data
  FORALL x IN 1..v_product_id.count
    INSERT INTO ftd_apps.inv_trk_search_product_temp
                (inv_trk_id, product_id,      product_name,      product_status,      product_updated_by,      exception_start_date,      vendor_id, vendor_name,      vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date,      end_date,      forecast_qty,      revised_forecast_qty,      ftd_msg_count,      can_msg_count,      rej_msg_count,      on_hand)
         VALUES (0,          v_product_id(x), v_product_name(x), v_product_status(x), v_product_updated_by(x), v_exception_start_date(x), null,      v_vendor_name(x), null,                  null,                      null,                   v_start_date(x), v_end_date(x), v_forecast_qty(x), v_revised_forecast_qty(x), v_ftd_msg_count(x), v_can_msg_count(x), v_rej_msg_count(x), v_on_hand(x));


  --INSERT PRODUCT VENDOR DATA IN THE FINAL TABLE
  insert into ftd_apps.inv_trk_search_temp
    (inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date, end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand)
  select
     inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date, end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand
  from ftd_apps.inv_trk_search_prod_vndr_temp;


  --INSERT PRODUCT SUMMARY DATA IN THE FINAL TABLE
  insert into ftd_apps.inv_trk_search_temp
    (inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date, end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand)
  select
     inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date, end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand
  from ftd_apps.inv_trk_search_product_temp;



  --INSERT PENDING DATA IN THE FINAL TABLE
  insert into ftd_apps.inv_trk_search_temp
    (inv_trk_id, product_id,   product_name,   product_status,   product_updated_by,   exception_start_date,   vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date,   end_date,                                          forecast_qty,                                                          revised_forecast_qty,                                             ftd_msg_count,                                            can_msg_count,                                            rej_msg_count,                                on_hand)
  select
     0,          p.product_id, p.product_name, p.product_status, p.product_updated_by, p.exception_start_date, null,      'PENDING',   null,                  null,                      0,                      p.start_date, p.end_date, (p.forecast_qty - sum(v.forecast_qty)) forecast_qty,   (p.revised_forecast_qty - sum(v.revised_forecast_qty)) revised_forecast_qty,   (p.ftd_msg_count - sum(v.ftd_msg_count) ) ftd_msg_count,   (p.can_msg_count - sum(v.can_msg_count)) can_msg_count,   (p.rej_msg_count - sum(v.rej_msg_count)) rej_msg_count,   (p.on_hand - sum(v.on_hand)) on_hand
  from
        (select inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date, end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand from ftd_apps.inv_trk_search_product_temp) p,
        (select inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, start_date, end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand from ftd_apps.inv_trk_search_prod_vndr_temp) v
  where p.product_id = v.product_id
  and p.start_date = v.start_date
  and p.end_date = v.end_date
  group by 0,    p.product_id, p.product_name, p.product_status, p.product_updated_by, p.exception_start_date, null,      'PENDING',   null,                  null,                      0,                      p.start_date, p.end_date,                                        p.forecast_qty,                                                        p.revised_forecast_qty,                                           p.ftd_msg_count,                                          p.can_msg_count,                                          p.rej_msg_count,                              p.on_hand;

  --If the onhand is provided, delete from the temp table anything which does not match the criteria
  IF IN_ON_HAND is not null THEN
    delete from ftd_apps.inv_trk_search_temp where on_hand >= IN_ON_HAND;
  END IF;

  -- OUTPUTS
  select count(*) into OUT_RECORD_COUNT from ftd_apps.inv_trk_search_temp where rownum <= v_max_records;

  OPEN OUT_CUR FOR
    select *
    from (
         select   inv_trk_id, product_id, product_name, product_status, product_updated_by, exception_start_date, vendor_id, vendor_name, vendor_product_status, vendor_product_updated_by, shutdown_threshold_qty, to_char(start_date, 'MM/dd/yyyy') start_date, to_char(end_date, 'MM/dd/yyyy') end_date, forecast_qty, revised_forecast_qty, ftd_msg_count, can_msg_count, rej_msg_count, on_hand, (ftd_msg_count - can_msg_count - rej_msg_count) shipped_shipping, CASE revised_forecast_qty WHEN 0 THEN 0 ELSE (((ftd_msg_count - can_msg_count - rej_msg_count) / revised_forecast_qty ) * 100) END as sell_thru_percent
         from     ftd_apps.inv_trk_search_temp a
         order by product_id asc, a.start_date desc, vendor_id asc, vendor_name desc
         )
    where rownum <= v_max_records;

END SEARCH_INV_TRK_PRODUCT;

/******************************************************************************
                             GET_PRODUCT_VENDOR_SQL

Description: This function will create a SQL statement which will retrieve
             inventory records based primarily on vendor and product ids, and
             secondarily on other search criteria.

******************************************************************************/
FUNCTION GET_PRODUCT_VENDOR_SQL
(
IN_PRODUCT_IDS           IN   VARCHAR2,
IN_VENDOR_IDS            IN   VARCHAR2,
IN_START_DATE            IN   FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE              IN   FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_PRODUCT_STATUS        IN   FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_VENDOR_PRODUCT_STATUS IN   FTD_APPS.VENDOR_PRODUCT.AVAILABLE%TYPE,
IN_ON_HAND               IN   NUMBER
)
RETURN VARCHAR2
AS

-- variables
v_sql             varchar2 (4000) := ' select a.inv_trk_id, a.product_id, pm.product_name, pm.status p_status, pm.updated_by p_updt_by, pm.exception_start_date, a.vendor_id, vm.vendor_name, a.vp_status, a.vp_updt_by, a.shutdown_threshold_qty, a.start_date, a.end_date, a.forecast_qty, a.revised_forecast_qty, a.ftd_msg_count, a.can_msg_count, a.rej_msg_count, a.on_hand ';
v_from1           varchar2 (4000) := ' from ftd_apps.product_master pm, ftd_apps.vendor_master vm, (';
v_sub_sql         varchar2 (4000) := ' ';
v_sub_select      varchar2 (4000) := ' select it.inv_trk_id, it.product_id, vp.available vp_status, vp.updated_by vp_updt_by, it.vendor_id, it.shutdown_threshold_qty, it.start_date, it.end_date, it.forecast_qty, it.revised_forecast_qty, sum(itac.ftd_msg_count) ftd_msg_count, sum(itac.can_msg_count) can_msg_count, sum(itac.rej_msg_count ) rej_msg_count, (it.revised_forecast_qty - sum(itac.ftd_msg_count) + sum(itac.can_msg_count) + sum(itac.rej_msg_count )) on_hand ';
v_sub_from        varchar2 (4000) := ' from ftd_apps.inv_trk it, venus.inv_trk_assigned_count itac, ftd_apps.vendor_product vp';
v_sub_where       varchar2 (4000) := ' where it.vendor_id = itac.vendor_id ';
v_sub_and         varchar2 (4000) := ' and it.product_id = itac.product_id and itac.ship_date between it.start_date and it.end_date and it.vendor_id = vp.vendor_id and it.product_id = vp.product_subcode_id and itac.vendor_id = vp.vendor_id and itac.product_id = vp.product_subcode_id ';
v_sub_group_by    varchar2 (4000) := ' group by it.inv_trk_id, it.product_id, vp.available, vp.updated_by, it.vendor_id, it.shutdown_threshold_qty, it.start_date, it.end_date, it.forecast_qty, it.revised_forecast_qty ';
v_from2           varchar2 (4000) := ' ) a ';
v_where           varchar2 (4000) := ' where a.vendor_id = vm.vendor_id  ';
v_and             varchar2 (4000) := ' and a.product_id = pm.product_id ';

BEGIN

--start working on the sub-select
IF IN_VENDOR_IDS is not null THEN
  v_sub_and       := v_sub_and || ' and it.vendor_id in (SELECT vendor_id FROM FTD_APPS.INV_TRK_VENDOR_TEMP) ';
END IF;

IF IN_PRODUCT_IDS is not null THEN
  v_sub_and       := v_sub_and || ' and it.product_id in (SELECT product_id FROM FTD_APPS.INV_TRK_PRODUCT_TEMP) ';
END IF;

IF IN_START_DATE is not null THEN
  v_sub_and       := v_sub_and || ' and it.start_date >= to_date(''' || to_char(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
END IF;

IF IN_END_DATE is not null THEN
  v_sub_and       := v_sub_and || ' and it.end_date <= to_date(''' || to_char(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
END IF;

CASE
  WHEN IN_VENDOR_PRODUCT_STATUS = 'X' THEN
    v_sub_and     := v_sub_and || ' and vp.available = ''N'' and vp.updated_by = ''SYSTEM_INV_TRK''';
  WHEN IN_VENDOR_PRODUCT_STATUS = 'U' THEN
    v_sub_and     := v_sub_and || ' and vp.available = ''N'' and vp.updated_by != ''SYSTEM_INV_TRK''';
  WHEN IN_VENDOR_PRODUCT_STATUS = 'A' THEN
    v_sub_and     := v_sub_and || ' and vp.available = ''Y''';
  ELSE
    v_sub_and     := v_sub_and;
END CASE;

--create the final sql string for the sub-select
v_sub_sql         := v_sub_select || v_sub_from || v_sub_where || v_sub_and || v_sub_group_by;


--start working on the outer select
IF IN_ON_HAND is not null THEN
  v_and           := v_and || ' and a.on_hand < ' || IN_ON_HAND;
END IF;

CASE
  WHEN IN_PRODUCT_STATUS = 'X' THEN
    v_and         := v_and || ' and pm.status = ''U'' and pm.updated_by = ''SYSTEM_INV_TRK''';
  WHEN IN_PRODUCT_STATUS = 'U' THEN
    v_and         := v_and || ' and pm.status = ''U'' and pm.updated_by != ''SYSTEM_INV_TRK''';
  WHEN IN_PRODUCT_STATUS = 'A' THEN
    v_and         := v_and || ' and pm.status = ''A'' and pm.exception_start_date is null';
  WHEN IN_PRODUCT_STATUS = 'R' THEN
    v_and         := v_and || ' and pm.status = ''A'' and pm.exception_start_date is not null';
  ELSE
    v_and         := v_and;
END CASE;

v_sql             := v_sql || v_from1 || v_sub_sql || v_from2 || v_where || v_and;

RETURN v_sql;

END GET_PRODUCT_VENDOR_SQL;


/******************************************************************************
                          GET_PRODUCT_SUMMARY_SQL

Description: This function will create a SQL statement which will retrieve
             inventory record totals based on product and other search criteria
             (excluding vendor).

******************************************************************************/
FUNCTION GET_PRODUCT_SUMMARY_SQL
(
IN_PRODUCT_IDS       IN   VARCHAR2,
IN_START_DATE        IN   FTD_APPS.INV_TRK.START_DATE%TYPE,
IN_END_DATE          IN   FTD_APPS.INV_TRK.END_DATE%TYPE,
IN_PRODUCT_STATUS    IN   FTD_APPS.PRODUCT_MASTER.STATUS%TYPE,
IN_ON_HAND           IN   NUMBER
)
RETURN VARCHAR2
AS

-- variables
v_sql             varchar2 (4000) := ' select a.product_id, pm.product_name, pm.status p_status, pm.updated_by p_updt_by, pm.exception_start_date, ''ALL VENDORS'' vendor_name, a.start_date, a.end_date, a.forecast_qty, a.revised_forecast_qty, sum(itc.ftd_msg_count ) ftd_msg_count, sum(itc.can_msg_count) can_msg_count, sum(itc.rej_msg_count) rej_msg_count, (a.revised_forecast_qty - sum(itc.ftd_msg_count ) + sum(itc.can_msg_count) + sum(itc.rej_msg_count)) on_hand ';
v_from1           varchar2 (4000) := ' from venus.inv_trk_count itc, ftd_apps.product_master pm, (';
v_sub_sql         varchar2 (4000) := ' ';
v_sub_select      varchar2 (4000) := ' select it.product_id,  it.start_date, it.end_date, sum(it.forecast_qty) forecast_qty, sum(it.revised_forecast_qty) revised_forecast_qty ';
v_sub_from        varchar2 (4000) := ' from ftd_apps.inv_trk it ';
v_sub_where       varchar2 (4000) := ' where 1=1 ';
v_sub_and         varchar2 (4000) := ' ';
v_sub_group_by    varchar2 (4000) := ' group by it.product_id, it.start_date, it.end_date ';
v_from2           varchar2 (4000) := ' ) a ';
v_where           varchar2 (4000) := ' where a.product_id = itc.product_id ';
v_and             varchar2 (4000) := ' and a.product_id = pm.product_id ';
v_group_by        varchar2 (4000) := ' group by a.product_id, pm.product_name, pm.status, pm.updated_by, pm.exception_start_date, ''ALL VENDORS'', a.start_date, a.end_date, a.forecast_qty, a.revised_forecast_qty, null, null, null ';
v_having          varchar2 (4000) := ' ';


BEGIN

--start working on the sub-select
IF IN_PRODUCT_IDS is not null THEN
  v_sub_and       := v_sub_and || ' and it.product_id in (SELECT product_id FROM FTD_APPS.INV_TRK_PRODUCT_TEMP) ';
END IF;

IF IN_START_DATE is not null THEN
  v_sub_and       := v_sub_and || ' and it.start_date >= to_date(''' || to_char(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
END IF;

IF IN_END_DATE is not null THEN
  v_sub_and       := v_sub_and || ' and it.end_date <= to_date(''' || to_char(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') ' ;
END IF;

--create the final sql string for the sub-select
v_sub_sql         := v_sub_select || v_sub_from || v_sub_where || v_sub_and || v_sub_group_by;


--start working on the outer select
v_and             := v_and || ' and itc.ship_date between a.start_date and a.end_date ';

CASE
  WHEN IN_PRODUCT_STATUS = 'X' THEN
    v_and         := v_and || ' and pm.status = ''U'' and pm.updated_by = ''SYSTEM_INV_TRK''';
  WHEN IN_PRODUCT_STATUS = 'U' THEN
    v_and         := v_and || ' and pm.status = ''U'' and pm.updated_by != ''SYSTEM_INV_TRK''';
  WHEN IN_PRODUCT_STATUS = 'A' THEN
    v_and         := v_and || ' and pm.status = ''A'' and pm.exception_start_date is null';
  WHEN IN_PRODUCT_STATUS = 'R' THEN
    v_and         := v_and || ' and pm.status = ''A'' and pm.exception_start_date is not null';
  ELSE
    v_and         := v_and;
END CASE;


IF IN_ON_HAND is not null THEN
  v_having        := ' having (a.revised_forecast_qty - sum(itc.ftd_msg_count ) + sum(itc.can_msg_count) + sum(itc.rej_msg_count)) < ' || IN_ON_HAND;
END IF;


v_sql             := v_sql || v_from1 || v_sub_sql || v_from2 || v_where || v_and || v_group_by || v_having;

RETURN v_sql;

END GET_PRODUCT_SUMMARY_SQL;

END;

.
/