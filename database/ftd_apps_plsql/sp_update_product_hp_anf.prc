CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_HP_ANF ( productId in varchar2,
 shipDates in varchar2,
 productColors in varchar2,
 productSubCodes in varchar2,
 excludedStates in varchar2 )
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_HP_ANF
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_HP ( productId in varchar2,
--                                delRestrictionStartDate in date,
--                                shipRestrictionStartDate in date,
--                                shipDates in varchar2,
--                                productColors in varchar2,
--                                productSubCodes in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_HP.  If the row
--                already exists (by product ID), then updates the existing row.
--
--==============================================================================

begin

  -- Attempt to insert into PRODUCT_HP table
  INSERT INTO PRODUCT_HP
    ( PRODUCT_ID,
      SHIP_DATES,
      PRODUCT_COLORS,
      PRODUCT_SUBCODES,
      EXCLUDED_STATES
    )
  VALUES
    ( productId,
      shipDates,
      productColors,
      productSubCodes,
      excludedStates
    );



EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

  -- If a row already exists for this PRODUCT_ID then update
  UPDATE PRODUCT_HP
    SET SHIP_DATES = shipDates,
        PRODUCT_COLORS = productColors,
        PRODUCT_SUBCODES = productSubCodes,
        EXCLUDED_STATES = excludedStates
    WHERE PRODUCT_ID = productId;

end;
.
/
