CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_SCRIPTING_BY_COMP_PAGE
(
 	   CompanyId IN VARCHAR2,
	   PageId 	 IN VARCHAR2
)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_SCRIPTING_BY_COMP_PAGE
-- Type:    Function
-- Syntax:  OE_GET_SCRIPTING_BY_COMP_PAGE (CompanyId IN NUMBER, PageID IN varchar2)
-- Returns: ref_cursor for
--          PAGE_NAME         VARCHAR2(20)
--          COMPANY_ID        NUMBER(9)
--          FIELD_NAME        VARCHAR2(50)
--          SCRIPT_TEXT       VARCHAR2(500)
--          INSTRUCTION_TEXT  VARCHAR2(500)
--
-- Description:   Queries the SCRIPTING table by CompanyId, PageID and
--                returns a ref cursor for resulting row.
--
--======================================================================
AS
    user_cursor types.ref_cursor;
BEGIN
    OPEN user_cursor FOR
        SELECT PAGE_NAME        as "pageName",
               COMPANY_ID       as "companyId",
               FIELD_NAME       as "fieldName",
               SCRIPT_TEXT      as "scriptText",
               INSTRUCTION_TEXT as "instructionText"
          FROM SCRIPTING
		  WHERE COMPANY_ID = UPPER(CompanyId)
            AND PAGE_NAME = UPPER(pageId);
    RETURN user_cursor;
END;
.
/
