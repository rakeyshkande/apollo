CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_ZIPCODE_BY_CITYSTATE (cityIn varchar2, stateNameIn varchar2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_ZIPCODE_BY_CITYSTATE
-- Type:    Function
-- Syntax:  OE_GET_ZIPCODE_BY_CITYSTATE()
-- Returns: ref_cursor for
--          ZIP_CODE_ID   VARCHAR2(10)
--          CITY          VARCHAR2(40)
--          STATE_NAME    VARCHAR2(30)
--
--
-- Description:   Queries STATE_MASTER to return row for input of stateNameIn and
--                queries ZIP_CODE for input of cityIn and state_master_id returning
--                a ref cursor for resulting rows.
--
--=============================================================
AS
    zipcode_cursor types.ref_cursor;
BEGIN
    OPEN zipcode_cursor FOR
        SELECT Z.ZIP_CODE_ID as "zipCode",
               Z.CITY        as "city",
               S.STATE_MASTER_ID  as "state"
          FROM STATE_MASTER S, ZIP_CODE Z
         WHERE --S.STATE_NAME = stateNameIn AND
         S.STATE_MASTER_ID = stateNameIn AND
         Z.CITY like UPPER(cityIn) || '%'
           AND Z.STATE_ID = S.STATE_MASTER_ID
      ORDER BY Z.CITY;

    RETURN zipcode_cursor;
END;
.
/
