CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_NOVATOR (
 IN_PRODUCT_ID                               FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
 IN_SENT_TO_NOVATOR_PROD                     FTD_APPS.PRODUCT_MASTER.SENT_TO_NOVATOR_PROD%TYPE,
 IN_SENT_TO_NOVATOR_UAT                      FTD_APPS.PRODUCT_MASTER.SENT_TO_NOVATOR_UAT%TYPE,
 IN_SENT_TO_NOVATOR_TEST                     FTD_APPS.PRODUCT_MASTER.SENT_TO_NOVATOR_TEST%TYPE,
 IN_SENT_TO_NOVATOR_CONTENT                  FTD_APPS.PRODUCT_MASTER.SENT_TO_NOVATOR_CONTENT%TYPE,
 OUT_STATUS                                  OUT VARCHAR2,
 OUT_MESSAGE                                 OUT VARCHAR2

)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_NOVATOR
-- Type:    Procedure
--
-- Description:   Updates sent to Novator values for a given product id.  
--                Pass in null for environments that should not be udpated.
--
--==============================================================================
begin

  UPDATE ftd_apps.product_master pm
  SET pm.sent_to_novator_prod = nvl(IN_SENT_TO_NOVATOR_PROD, pm.sent_to_novator_prod),
      pm.sent_to_novator_uat = nvl(IN_SENT_TO_NOVATOR_UAT, pm.sent_to_novator_uat),
      pm.sent_to_novator_test = nvl(IN_SENT_TO_NOVATOR_TEST, pm.sent_to_novator_test),
      pm.sent_to_novator_content = nvl(IN_SENT_TO_NOVATOR_CONTENT, pm.sent_to_novator_content)
  WHERE pm.product_id = IN_PRODUCT_ID;
  EXCEPTION WHEN OTHERS THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END SP_UPDATE_PRODUCT_NOVATOR;
/
