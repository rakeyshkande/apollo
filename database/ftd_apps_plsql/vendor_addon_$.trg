CREATE OR REPLACE TRIGGER FTD_APPS.vendor_addon_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.vendor_addon 
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN

      INSERT INTO ftd_apps.vendor_addon$ (
         vendor_id,         
         addon_id,          
         active_Flag,       
         vendor_sku,        
         vendor_cost,       
         created_on,        
         created_by,        
         updated_on,        
         updated_by,        
         OPERATION$,        
         TIMESTAMP$        
      ) VALUES (
         :NEW.vendor_id,         
         :NEW.addon_id,          
         :NEW.active_Flag,       
         :NEW.vendor_sku,        
         :NEW.vendor_cost,       
         :NEW.created_on,        
         :NEW.created_by,        
         :NEW.updated_on,        
         :NEW.updated_by,        
         'INS',
         v_current_timestamp);

   ELSIF UPDATING THEN

      INSERT INTO ftd_apps.vendor_addon$ (
         vendor_id,         
         addon_id,          
         active_Flag,       
         vendor_sku,        
         vendor_cost,       
         created_on,        
         created_by,        
         updated_on,        
         updated_by,        
         OPERATION$,        
         TIMESTAMP$        
      ) VALUES (
         :OLD.vendor_id,         
         :OLD.addon_id,          
         :OLD.active_Flag,       
         :OLD.vendor_sku,        
         :OLD.vendor_cost,       
         :OLD.created_on,        
         :OLD.created_by,        
         :OLD.updated_on,        
         :OLD.updated_by,        
         'UPD_OLD',
         v_current_timestamp);

      INSERT INTO ftd_apps.vendor_addon$ (
         vendor_id,         
         addon_id,          
         active_Flag,       
         vendor_sku,        
         vendor_cost,       
         created_on,        
         created_by,        
         updated_on,        
         updated_by,        
         OPERATION$,        
         TIMESTAMP$        
      ) VALUES (
         :NEW.vendor_id,         
         :NEW.addon_id,          
         :NEW.active_Flag,       
         :NEW.vendor_sku,        
         :NEW.vendor_cost,       
         :NEW.created_on,        
         :NEW.created_by,        
         :NEW.updated_on,        
         :NEW.updated_by,        
         'UPD_NEW',
         v_current_timestamp);

   ELSIF DELETING THEN
      INSERT INTO ftd_apps.vendor_addon$ (
         vendor_id,         
         addon_id,          
         active_Flag,       
         vendor_sku,        
         vendor_cost,       
         created_on,        
         created_by,        
         updated_on,        
         updated_by,        
         OPERATION$,        
         TIMESTAMP$        
      ) VALUES (
         :OLD.vendor_id,         
         :OLD.addon_id,          
         :OLD.active_Flag,       
         :OLD.vendor_sku,        
         :OLD.vendor_cost,       
         :OLD.created_on,        
         :OLD.created_by,        
         :OLD.updated_on,        
         :OLD.updated_by,        
         'DEL',
         v_current_timestamp);

   END IF;
END;
/
