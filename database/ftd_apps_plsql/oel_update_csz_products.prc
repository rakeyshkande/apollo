CREATE OR REPLACE
PROCEDURE ftd_apps.OEL_UPDATE_CSZ_PRODUCTS  ( zipCode IN VARCHAR2,
   productId IN VARCHAR2,
   availableFlag IN VARCHAR2,
   productCutoff IN VARCHAR2 )
as
--==============================================================================
--
-- Name:    OEL_UPDATE_CSZ_PRODUCTS
-- Type:    Procedure
-- Syntax:  OEL_UPDATE_CSZ_PRODUCTS ( zipCode IN VARCHAR2,
--                                    productId IN VARCHAR2,
--                                    availableFlag IN VARCHAR2,
--                                    productCutoff IN VARCHAR2 )
--
-- Description:   Attempts to insert a row into CSZ_PRODUCTS.  If the row
--                already exists (by zipCode and productId), then updates
--                the existing row.
--
--==============================================================================

begin

  -- Attempt to insert into CSZ_PRODUCTS table
  INSERT INTO CSZ_PRODUCTS
    ( ZIP_CODE,
      PRODUCT_ID,
      AVAILABLE_FLAG,
      PRODUCT_CUTOFF
    )
  VALUES
    ( UPPER(zipCode),
      UPPER(productId),
      UPPER(availableFlag),
      productCutoff
    );

  COMMIT;

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN

  -- If a row already exists for this CSZ_PRODUCTS then update
  UPDATE CSZ_PRODUCTS
    SET AVAILABLE_FLAG = UPPER(availableFlag),
        PRODUCT_CUTOFF = productCutoff
    WHERE ZIP_CODE = UPPER(zipCode)
    AND PRODUCT_ID = UPPER(productId);

  COMMIT;
end;
.
/
