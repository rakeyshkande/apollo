CREATE OR REPLACE
FUNCTION ftd_apps.sp_get_upsell_companies_2247 (productID IN VARCHAR2)
  RETURN types.ref_cursor
 --===========================================================================
--
-- Name:    sp_get_upsell_companies_2247
-- Type:    Function
-- Syntax:  sp_get_upsell_companies_2247 (productID )
-- Returns: ref_cursor for
--          COMPANY_ID        VARCHAR2(12)
--          COMANY_NAME       VAECHAR2(60)
--
-- Description:   Queries the UPSELL_COMPANY_XREF able and returns companies for an upsell product
--
--===========================================================
AS
    company_cursor types.ref_cursor;
BEGIN
    OPEN company_cursor FOR
 select
  cm.company_id, cm.company_name
 from
  upsell_company_xref ucx,
  company_master cm
 where
  ucx.company_id = cm.company_id
  and ucx.product_id = productID
  and cm.order_flag = 'Y'
 order by
  cm.company_name;
    RETURN company_cursor;
END;
.
/
