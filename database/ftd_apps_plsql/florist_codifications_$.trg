CREATE OR REPLACE TRIGGER ftd_apps.florist_codifications_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_codifications
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.florist_codifications$ (
         FLORIST_ID,        
         CODIFICATION_ID,   
         BLOCK_START_DATE,  
         BLOCK_END_DATE,    
         HOLIDAY_PRICE_FLAG,
         MIN_PRICE,         
         CODIFIED_DATE,     
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.CODIFICATION_ID,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.HOLIDAY_PRICE_FLAG,
         :new.MIN_PRICE,
         :new.CODIFIED_DATE,
         'INS',SYSTIMESTAMP);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.florist_codifications$ (
         FLORIST_ID,        
         CODIFICATION_ID,   
         BLOCK_START_DATE,  
         BLOCK_END_DATE,    
         HOLIDAY_PRICE_FLAG,
         MIN_PRICE,         
         CODIFIED_DATE,     
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.CODIFICATION_ID,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.HOLIDAY_PRICE_FLAG,
         :old.MIN_PRICE,
         :old.CODIFIED_DATE,
         'UPD_OLD',SYSTIMESTAMP);

      INSERT INTO ftd_apps.florist_codifications$ (
         FLORIST_ID,        
         CODIFICATION_ID,   
         BLOCK_START_DATE,  
         BLOCK_END_DATE,    
         HOLIDAY_PRICE_FLAG,
         MIN_PRICE,         
         CODIFIED_DATE,     
         operation$, timestamp$
      ) VALUES (
         :new.FLORIST_ID,
         :new.CODIFICATION_ID,
         :new.BLOCK_START_DATE,
         :new.BLOCK_END_DATE,
         :new.HOLIDAY_PRICE_FLAG,
         :new.MIN_PRICE,
         :new.CODIFIED_DATE,
         'UPD_NEW',SYSTIMESTAMP);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.florist_codifications$ (
         FLORIST_ID,        
         CODIFICATION_ID,   
         BLOCK_START_DATE,  
         BLOCK_END_DATE,    
         HOLIDAY_PRICE_FLAG,
         MIN_PRICE,         
         CODIFIED_DATE,     
         operation$, timestamp$
      ) VALUES (
         :old.FLORIST_ID,
         :old.CODIFICATION_ID,
         :old.BLOCK_START_DATE,
         :old.BLOCK_END_DATE,
         :old.HOLIDAY_PRICE_FLAG,
         :old.MIN_PRICE,
         :old.CODIFIED_DATE,
         'DEL',SYSTIMESTAMP);


   END IF;

END;
/
