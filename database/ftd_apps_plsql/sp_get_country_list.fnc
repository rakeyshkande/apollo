CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_COUNTRY_LIST (intl_dom varchar2 DEFAULT 'A')
  RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    SP_GET_COUNTRY_LIST
-- Type:    Function
-- Syntax:  SP_GET_COUNTRY_LIST ( )
-- Returns: ref_cursor for
--          COUNTRY_ID        VARCHAR2(2)
--          NAME              VAECHAR2(50)
--          OE_COUNTRY_TYPE   VARCHAR2(1)
--          OE_DISPLAY_ORDER  NUMBER(3)
--
-- Description:   Queries the COUNTRY_MASTER table and returns a ref cursor for row.
--
--===========================================================


AS
    country_cursor types.ref_cursor;
BEGIN

    IF (UPPER(intl_dom) = 'D') THEN
        OPEN country_cursor FOR
        SELECT country_id as "countryId",
           name as "name",
           oe_country_type as "oeCountryType",
           oe_display_order as "oeDisplayOrder"
        FROM COUNTRY_MASTER
        WHERE OE_COUNTRY_TYPE = UPPER(intl_dom)
		    AND   country_id not in ('00', '01')
        ORDER BY oe_display_order, name;
    ELSIF (UPPER(intl_dom) = 'I') THEN
        OPEN country_cursor FOR
        SELECT country_id as "countryId",
           name as "name",
           oe_country_type as "oeCountryType",
           oe_display_order as "oeDisplayOrder"
        FROM COUNTRY_MASTER
        WHERE OE_COUNTRY_TYPE = UPPER(intl_dom)
		    --AND country_id not in ('00', '01')
        ORDER BY oe_display_order, name;
    ELSE
        OPEN country_cursor FOR
        SELECT country_id as "countryId",
             name as "name",
             oe_country_type as "oeCountryType",
             oe_display_order as "oeDisplayOrder"
        FROM COUNTRY_MASTER
		    WHERE country_id not in ('00', '01')
        ORDER BY oe_display_order, name;
    END IF;
    RETURN country_cursor;
END;
.
/
