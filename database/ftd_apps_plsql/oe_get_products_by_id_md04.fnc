CREATE OR REPLACE FUNCTION FTD_APPS.OE_GET_PRODUCTS_BY_ID_MD04  (
productIdList         IN VARCHAR2,
inPricePointId        IN NUMBER,
sourceCode            IN VARCHAR2,
domesticIntlFlag      IN VARCHAR2,
inZipCode             IN VARCHAR2,
deliveryEnd           IN VARCHAR2,
inCountryId           IN VARCHAR2,
scriptCode            IN VARCHAR2,
deliveryDate          IN VARCHAR2
)
  RETURN Types.ref_cursor

--==============================================================================
--
-- Name:    OE_GET_PRODUCTS_BY_ID_MD04
-- Type:    Function
--
-- Returns: ref_cursor for
--          PRODUCT_ID             VARCHAR2(10)
--          PRODUCT_NAME           VARCHAR2(25)
--          NOVATOR_NAME           VARCHAR2(100)
--          DELIVERY_TYPE          VARCHAR2(1)
--          CATEGORY               VARCHAR2(5)
--          productType            VARCHAR2(25)
--          productSubType         VARCHAR2(25)
--          zipGnaddFlag           VARCHAR2(1)
--          zipFloralFlag          VARCHAR2(1)
--          zipGotoFloristFlag     VARCHAR2(1)
--          zipSundayFlag          VARCHAR2(1)
--          timeZone               VARCHAR2(1)
--          stateId                VARCHAR(2),
--          addonDays              NUMBER,
--          addonBalloonsFlag      VARCHAR2(1)
--          addonBearsFlag         VARCHAR2(1)
--          addonCardsFlag         VARCHAR2(1)
--          addonChocolateFlag     VARCHAR2(1)
--          addonFuneralFlag       VARCHAR2(1)
--          discountType           VARCHAR2(1)
--          STANDARD_PRICE         NUMBER(8.2)
--          standardDiscountAmt    NUMBER(7.2)
--          DELUXE_PRICE           NUMBER(8.2)
--          deluxeDiscountAmt      NUMBER(7.2)
--          PREMIUM_PRICE          NUMBER(8.2)
--          premiumDiscountAmt     NUMBER(7.2)
--          VARIABLE_PRICE_MAX     NUMBER(8.2)
--          LONG_DESCRIPTION       VARCHAR2(540)
--          DISCOUNT_ALLOWED_FLAG  VARCHAR2(1)
--          DELIVERY_INCLUDED_FLAG VARCHAR2(1)
--          SERVICE_FEE_FLAG       VARCHAR2(1)
--          ARRANGEMENT_SIZE       VARCHAR2(25)
--          PRICE_RANK_1           VARCHAR2(1)
--          PRICE_RANK_2           VARCHAR2(1)
--          PRICE_RANK_3           VARCHAR2(1)
--          exceptionCode          VARCHAR2(1)
--          exceptionStartDate     VARCHAR2(10)
--          exceptionEndDate       VARCHAR2(10)
--          mondayFlag             VARCHAR2(1)
--          tuesdayFlag            VARCHAR2(1)
--          wednesdayFlag          VARCHAR2(1)
--          thursdayFlag           VARCHAR2(1)
--          fridayFlag             VARCHAR2(1)
--          saturdayFlag           VARCHAR2(1)
--          sundayFlag             VARCHAR2(1)
--          exSundayFlag           VARCHAR2(1)
--          exMondayFlag           VARCHAR2(1)
--          exTuesdayFlag          VARCHAR2(1)
--          exWednesdayFlag        VARCHAR2(1)
--          exThursdayFlag         VARCHAR2(1)
--          exFridayFlag           VARCHAR2(1)
--          exSaturdayFlag         VARCHAR2(1)
--          mondayDelFCFlag        VARCHAR2(1)
--          twoDaySatFCFlag        VARCHAR2(1)
--          vendorID               VARCHAR2(2)
--          SHIP_METHOD_CARRIER    VARCHAR2(1)
--          SHIP_METHOD_FLORIST    VARCHAR2(1)
--          VARIABLE_PRICE_FLAG    VARCHAR2(1)
--          SERVICE_CHARGE         NUMBER(8.2)
--          CARRIER                VARCHAR2(25)
--          color1                 VARCHAR2(15)
--          color2                 VARCHAR2(15)
--          color3                 VARCHAR2(15)
--          color4                 VARCHAR2(15)
--          color5                 VARCHAR2(15)
--          color6                 VARCHAR2(15)
--          color7                 VARCHAR2(15)
--          color8                 VARCHAR2(15)
--          vendorShipBlockStart1  VARCHAR2(10)
--          vendorShipBlockEnd1    VARCHAR2(10)
--          vendorShipBlockStart2  VARCHAR2(10)
--          vendorShipBlockEnd2    VARCHAR2(10)
--          vendorShipBlockStart3  VARCHAR2(10)
--          vendorShipBlockEnd3    VARCHAR2(10)
--          vendorShipBlockStart4  VARCHAR2(10)
--          vendorShipBlockEnd4    VARCHAR2(10)
--          vendorShipBlockStart5  VARCHAR2(10)
--          vendorShipBlockEnd5    VARCHAR2(10)
--          vendorShipBlockStart6  VARCHAR2(10)
--          vendorShipBlockEnd6    VARCHAR2(10)
--          vendorDelivBlockStart1 VARCHAR2(10)
--          vendorDelivBlockEnd1   VARCHAR2(10)
--          vendorDelivBlockStart2 VARCHAR2(10)
--          vendorDelivBlockEnd2   VARCHAR2(10)
--          vendorDelivBlockStart3 VARCHAR2(10)
--          vendorDelivBlockEnd3   VARCHAR2(10)
--          vendorDelivBlockStart4 VARCHAR2(10)
--          vendorDelivBlockEnd4   VARCHAR2(10)
--          vendorDelivBlockStart5 VARCHAR2(10)
--          vendorDelivBlockEnd5   VARCHAR2(10)
--          vendorDelivBlockStart6 VARCHAR2(10)
--          vendorDelivBlockEnd6   VARCHAR2(10)
--          largeImage             VARCHAR2(1)
--          floralDeliveryAvail    VARCHAR2(1)
--          dropShipDeliveryAvail  VARCHAR2(1)
--
-- Description:   Queries PRODUCT_MASTER by PR.PRODUCT_ID
--                returning a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor Types.ref_cursor;

    pricingCode         SOURCE.PRICING_CODE%TYPE := NULL;
    zipGnaddFlag        CSZ_AVAIL.GNADD_FLAG%TYPE := NULL;
    zipFloralFlag       VARCHAR2(1) := 'Y';
    zipSundayFlag       VARCHAR2(1) := NULL;
      timeZone          STATE_MASTER.TIME_ZONE%TYPE := NULL;
    stateId             ZIP_CODE.STATE_ID%TYPE := NULL;
    currentDate         VARCHAR2(10) := TO_CHAR(SYSDATE, 'mm/dd/yyyy');
    addonDays           NUMBER;
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
    cszZip              VARCHAR2(6);
    cleanZip            VARCHAR2(6) := Oe_Cleanup_Alphanum_String(UPPER(inZipCode), 'Y');
    mySelect            VARCHAR2(16000);
    block_ts            VARCHAR2(16) := TO_CHAR(SYSDATE, 'mm/dd/yyyy HH24:MI');
    gnaddLevel          VARCHAR2(2);
BEGIN

    -- Select the pricing code for this source code, for the below query
    BEGIN
        SELECT PRICING_CODE
        INTO pricingCode
        FROM SOURCE
        WHERE SOURCE_CODE = UPPER(sourceCode);
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- If no zip provided, assume zip GNADD is off and floral available wherever this is going
    IF ( cleanZip IS NULL )
    THEN
        zipGnaddFlag  := 'N';
        zipFloralFlag := 'Y';
    ELSE
        -- Determine if the input zip code is Canadian
        IF ( TRANSLATE(cleanZip, alphaChars || numChars, alphaSub || numSub) LIKE '@#@%' )
        THEN
            -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
            cszZip := SUBSTR(cleanZip,1,3);
        ELSE
            cszZip := cleanZip;
        END IF;

        -- Select the GNADD flag for this zip, if available
        BEGIN
            -- Assume NULL GNADD flag indicates GNADD is off
            SELECT NVL(GNADD_FLAG, 'N')
            INTO zipGnaddFlag
            FROM CSZ_AVAIL
            WHERE CSZ_ZIP_CODE = cszZip;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            -- If no row found on CSZ_AVAIL and GNADD level is > 0, GNADD is on for the zip
            -- If GNADD level is zero then the zip is blocked
            zipGnaddFlag := 'Y';
        END;
    END IF;

    -- Select the time zone for this zip, if available
    BEGIN
        SELECT DISTINCT SM.TIME_ZONE, ZC.STATE_ID
        INTO timeZone, stateId
        FROM ZIP_CODE zc, STATE_MASTER sm
        WHERE zc.ZIP_CODE_ID = UPPER(cleanZip)
          AND ZC.STATE_ID = SM.STATE_MASTER_ID;
    EXCEPTION WHEN OTHERS THEN
        timeZone := '5';
    END;

    -- Select add on days for this country from country master
    BEGIN
        SELECT UNIQUE NVL(ADD_ON_DAYS, 1)
        INTO addonDays
        FROM COUNTRY_MASTER
        WHERE COUNTRY_ID = inCountryId;
    EXCEPTION WHEN OTHERS THEN
        -- Default to 1 day if not known
        addonDays := 1;
    END;

    zipSundayFlag := OE_GET_ZIP_SUNDAY_FLAG(cleanZip);
    gnaddLevel := get_gnadd_level();

    -- Open a query ref cursor to return product list results
    mySelect :=
        'SELECT DECODE(pmv.UPSELL_PRODUCT_FLAG, ''Y'', pmv.PRODUCT_ID, pm.PRODUCT_ID) as "productID", ' ||
               'DECODE(pmv.UPSELL_PRODUCT_FLAG, ''Y'', pmv.PRODUCT_ID, pm.NOVATOR_ID) as "novatorID", ' ||
               'DECODE(pmv.UPSELL_PRODUCT_FLAG, ''Y'', pmv.UPSELL_NAME, pm.PRODUCT_NAME) as "productName", ' ||
               'pm.NOVATOR_NAME as "novatorName", ' ||
               'pm.DELIVERY_TYPE as "deliveryType", ' ||
               'pm.CATEGORY as "productCategory", ' ||
                     'pm.PRODUCT_TYPE as "productType", ' ||
                     'pm.PRODUCT_SUB_TYPE as "productSubType", ' ||
               '''' || zipGnaddFlag || ''' as "zipGnaddFlag", ' ||
               '''' || zipFloralFlag || ''' as "zipFloralFlag", ' ||

               -- If the input zip is null, assume goto and sunday delivery available
               'DECODE(''' || cleanZip || ''', NULL, ''Y'', ' ||
                  'DECODE(gc.GOTO_COUNT, 0, ''N'', ''Y'')) as "zipGotoFloristFlag", ' ||
               '''' || zipSundayFlag || ''' as "zipSundayFlag", ' ||

               timeZone || ' as "timeZone", ' ||
               '''' || stateId || ''' as "stateId", ' ||
               addonDays || ' as "addonDays", ' ||
               'pm.ADD_ON_BALLOONS_FLAG as "addonBalloonsFlag", ' ||
               'pm.ADD_ON_BEARS_FLAG as "addonBearsFlag", ' ||
               'pm.ADD_ON_CARDS_FLAG as "addonCardsFlag", ' ||
               'pm.ADD_ON_CHOCOLATE_FLAG as "addonChocolateFlag", ' ||
               'pm.ADD_ON_FUNERAL_FLAG as "addonFuneralFlag", ' ||
               'phd_std.DISCOUNT_TYPE as "discountType", ' ||
               'pm.STANDARD_PRICE as "standardPrice", ' ||
               'phd_std.DISCOUNT_AMT as "standardDiscountAmt", ' ||
               'pm.DELUXE_PRICE as "deluxePrice", ' ||
               'phd_dlx.DISCOUNT_AMT as "deluxeDiscountAmt", ' ||
               'pm.PREMIUM_PRICE as "premiumPrice", ' ||
               'phd_prm.DISCOUNT_AMT as "premiumDiscountAmt", ' ||
               'pm.VARIABLE_PRICE_MAX as "variablePriceMax", ' ||
               'DECODE(pmv.UPSELL_PRODUCT_FLAG, ''Y'', pmv.UPSELL_DESC, pm.LONG_DESCRIPTION) as "longDescription", ' ||
               'pm.DISCOUNT_ALLOWED_FLAG as "discountFlag", ' ||
               'pm.DELIVERY_INCLUDED_FLAG as "deliveryFlag", ' ||
               'pm.SERVICE_FEE_FLAG as "serviceFeeFlag", ' ||
               'pm.ARRANGEMENT_SIZE as "arrangementSize", ' ||
               'pm.PRICE_RANK_1 as "priceRank1", ' ||
               'pm.PRICE_RANK_2 as "priceRank2", ' ||
               'pm.PRICE_RANK_3 as "priceRank3", ' ||
               'pm.EXCEPTION_CODE as "exceptionCode", ' ||
               'to_char(pm.EXCEPTION_START_DATE, ''mm/dd/yyyy'') as "exceptionStartDate", ' ||
               'to_char(pm.EXCEPTION_END_DATE, ''mm/dd/yyyy'') as "exceptionEndDate", ' ||
               'psd.MONDAY_FLAG as "mondayFlag", ' ||
               'psd.TUESDAY_FLAG as "tuesdayFlag", ' ||
               'psd.WEDNESDAY_FLAG as "wednesdayFlag", ' ||
               'psd.THURSDAY_FLAG as "thursdayFlag", ' ||
               'psd.FRIDAY_FLAG as "fridayFlag", ' ||
               'psd.SATURDAY_FLAG as "saturdayFlag", ' ||
               'psd.SUNDAY_FLAG as "sundayFlag", ' ||
               'pes2.SUN as "exSundayFlag", ' ||
               'pes2.MON as "exMondayFlag", ' ||
               'pes2.TUE as "exTuesdayFlag", ' ||
               'pes2.WED as "exWednesdayFlag", ' ||
               'pes2.THU as "exThursdayFlag", ' ||
               'pes2.FRI as "exFridayFlag", ' ||
               'pes2.SAT as "exSaturdayFlag", ' ||
               'pm.MONDAY_DELIVERY_FRESHCUT AS "mondayDelFCFlag", ' ||
               'pm.TWO_DAY_SAT_FRESHCUT AS "twoDaySatFCFlag", ' ||
               'null as "vendorID", ' ||
               'pm.SHIP_METHOD_CARRIER as "shipMethodCarrier", ' ||
               'pm.SHIP_METHOD_FLORIST as "shipMethodFlorist", ' ||
               'pm.VARIABLE_PRICE_FLAG as "variablePriceFlag", ' ||
               'pm.STATUS as "status", ' ||
               'pm.SHIPPING_KEY as "shippingKey", ' ||

               -- Flag whether product is codified and, if codified,
               --   whether product is available.  Non-codified products
               --   return value of 'NA' for availability.
               'NVL(cp.check_oe_flag, ''N'') as "codifiedProduct", ' ||
               'DECODE(cp.check_oe_flag, NULL, ''NA'', ' ||
                      'DECODE(''' || cleanZip || ''', NULL, ''S'', ' ||
                          'NVL(cszp.available_flag, ''N''))) as "codifiedAvailable", ' ||

               'NVL(cp.codified_special, ''N'') as "codifiedSpecial", ' ||

               -- Populate service charge
               'OE_GET_FLORAL_SERVICE_CHARGE(pm.PRODUCT_TYPE, ''' || domesticIntlFlag || ''',''' || sourceCode || ''', pm.STANDARD_PRICE, pm.shipping_key, ''' || deliveryDate || ''') as "serviceCharge",' ||

               'sk.SHIPPER as "carrier", ' ||

               -- Get the colors available for the product, if any
               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 1), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 1)) as "color1", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 2), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 2)) as "color2", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 3), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 3)) as "color3", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 4), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 4)) as "color4", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 5), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 5)) as "color5", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 6), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 6)) as "color6", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 7), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 7)) as "color7", ' ||

               'DECODE(SIGN(nvl(pcc.colors_count, 0) - 8), -1, NULL, ' ||
                  'OE_GET_PRODDETAIL_COLOR(pm.product_id, 8)) as "color8", ' ||

               -- If a product is carrier delivered, get the vendor shipping
               --   block start and end date ranges, if any
               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 1), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_start(pm.vendor_id, 1),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockStart1", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 1), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_end(pm.vendor_id, 1),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockEnd1", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 2), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_start(pm.vendor_id, 2),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockStart2", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 2), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_end(pm.vendor_id, 2),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockEnd2", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 3), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_start(pm.vendor_id, 3),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockStart3", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 3), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_end(pm.vendor_id, 3),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockEnd3", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 4), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_start(pm.vendor_id, 4),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockStart4", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 4), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_end(pm.vendor_id, 4),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockEnd4", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 5), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_start(pm.vendor_id, 5),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockStart5", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 5), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_end(pm.vendor_id, 5),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockEnd5", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 6), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_start(pm.vendor_id, 6),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockStart6", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(psbc.count, 0) - 6), -1, NULL, ' ||
                  'to_char(oe_get_prod_shipblock_end(pm.vendor_id, 6),''mm/dd/yyyy'')) ' ||
               ') as "vendorShipBlockEnd6", ' ||

               -- If a product is carrier delivered, get the vendor delivery
               --   block start and end date ranges, if any
               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 1), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_start(pm.vendor_id, 1),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockStart1", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 1), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_end(pm.vendor_id, 1),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockEnd1", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 2), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_start(pm.vendor_id, 2),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockStart2", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 2), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_end(pm.vendor_id, 2),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockEnd2", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 3), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_start(pm.vendor_id, 3),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockStart3", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 3), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_end(pm.vendor_id, 3),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockEnd3", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 4), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_start(pm.vendor_id, 4),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockStart4", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 4), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_end(pm.vendor_id, 4),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockEnd4", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 5), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_start(pm.vendor_id, 5),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockStart5", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 5), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_end(pm.vendor_id, 5),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockEnd5", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 6), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_start(pm.vendor_id, 6),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockStart6", ' ||

               'DECODE(pm.ship_method_carrier, ''Y'', ' ||
                  'DECODE(SIGN(nvl(pdbc.count, 0) - 6), -1, NULL, ' ||
                  'to_char(oe_get_prod_delivblock_end(pm.vendor_id, 6),''mm/dd/yyyy'')) ' ||
               ') as "vendorDelivBlockEnd6", ' ||

               -- Always indicate that a large image should be available
               '''Y'' as "largeImage"' ||

        'FROM PRODUCT_MASTER pm, PRODUCT_MASTER_VW pmv, SHIPPING_KEYS sk, ' ||
              'SOURCE sc, SNH snh, ' ||
              'PRICE_HEADER_DETAILS phd_std, ' ||
              'PRICE_HEADER_DETAILS phd_dlx, ' ||
              'PRICE_HEADER_DETAILS phd_prm, ' ||
              'PRODUCT_SHIP_DATES psd, ' ||
              'PRODUCT_SHIPMETHODS_CNT_VW psc, ' ||
              'PROD_SHIP_BLOCK_CNT_VW psbc, ' ||
              'PROD_DELIV_BLOCK_CNT_VW pdbc, ' ||
              'PRODUCT_COLORS_CNT_VW pcc, ' ||
              'CODIFIED_PRODUCTS cp, CSZ_PRODUCTS cszp, ' ||
              'PRODUCT_EXCLUDED_STATES pes2, ' ||

              -- Inline view computes count of goto florists for supplied zip
              '( SELECT ' ||

                -- Count florists with Sunday delivery from florist_hours table
               'COUNT(DECODE(fb.BLOCK_START_DATE, NULL, ' ||
                          'DECODE(florist_query_pkg.is_florist_open_sunday(fz.FLORIST_ID),''Y'',1))) as SUNDAY_COUNT, ' ||
                -- Count GOTO florists, excluding blocked florists ONLY
                --   if GNADD level 2 is in effect
                'COUNT(DECODE('''||gnaddLevel||''', 2, ' ||
                          'DECODE(fb.BLOCK_START_DATE, NULL, ' ||
                              'DECODE(fm.SUPER_FLORIST_FLAG,''Y'',1)), ' ||
                          'DECODE(fm.SUPER_FLORIST_FLAG,''Y'',1))) as GOTO_COUNT ' ||

                'FROM FLORIST_ZIPS fz, FLORIST_MASTER fm, ' ||
                      'FLORIST_BLOCKS fb ' ||
                'WHERE fz.ZIP_CODE = ''' || cleanZip || ''' ' ||
                'AND fz.FLORIST_ID = fm.FLORIST_ID ' ||
                'AND fz.FLORIST_ID = fb.FLORIST_ID (+) ' ||
                'AND TO_DATE(''' || block_ts || ''', ''mm/dd/yyyy HH24:MI'') >= fb.BLOCK_START_DATE (+) ' ||
                'AND TO_DATE(''' || block_ts || ''', ''mm/dd/yyyy HH24:MI'') <= fb.BLOCK_END_DATE (+) ' ||
                -- cr 11/2004 - changes for florist data model
                'and fm.status <> ''Inactive'' ' ||
                'and fz.block_start_date is null ' ||
              ') gc ' ||

        -- For product detail, match on product ID or Novator ID
        --   also match subcodes for a master product if present
        'WHERE pmv.product_id IN (' || productIdList || ') ' ||

        'AND pmv.product_detail_id = pm.product_id ' ||

        -- Only available products should be returned
        'AND pm.status = ''A'' ' ||

        -- If input country is US or null, exclude international products
        'AND (''' || inCountryId || ''' IS NULL OR ''' || inCountryId || ''' != ''US'' ' ||
          'OR  pm.delivery_type != ''I'' ) ' ||

        -- If input zip code is blocked and GNADD level 0 in effect,
        -- or no floral deliveries to this zip code
        -- then list only Specialty Gift , Same Day Gift and Fresh Cuts products
        'AND (( (''' || zipGnaddFlag || ''' != ''Y'' OR '''||gnaddLevel||''' != 0) ' ||
          'AND ''' || zipFloralFlag || ''' = ''Y'') ' ||
          'OR  pm.PRODUCT_TYPE IN (''SPEGFT'',''FRECUT'',''SDG'',''SDFC'') ) ' ||

        -- Determine if this product is considered codified for Order Entry
        'AND pm.product_id = cp.product_id (+) ' ||
        'AND ''Y'' = cp.check_oe_flag (+) ' ||

        -- GNADD level 3 test
        -- At level 3 floral products show GNADD delivery date
        -- Codified products are not available
        -- Special Codified product show GNADD delivery date
        'AND ('''||gnaddLevel||''' != 3 OR cp.check_oe_flag IS NULL OR NVL(cp.codified_special, ''N'') = ''Y'')' ||

        -- If input zip code is blocked and GNADD level 1 in effect
        --   then exclude ALL codified products
        'AND (''' || zipGnaddFlag || ''' != ''Y'' OR '''||gnaddLevel||''' != 1 ' ||
          'OR  cp.check_oe_flag IS NULL OR NVL(cp.codified_special, ''N'') = ''Y'' OR pm.ship_method_carrier = ''Y'')' ||

        -- Check product availability for codified products by zip.  Available
        --   flag of 'N' indicates codified product not available for zip.
        'AND pm.product_id = cszp.product_id (+) ' ||
        'AND ''' || cszZip || ''' = cszp.zip_code (+) ' ||
        'AND ( cp.check_oe_flag IS NULL OR ''' || cleanZip || ''' IS NULL OR pm.PRODUCT_TYPE IN (''SDG'',''SDFC'') ' ||
          ' OR (NVL(cp.codified_special, ''N'') = ''Y'' AND (NVL(cszp.available_flag, ''N'') != ''N'' OR ''' ||
            zipGnaddFlag || ''' = ''Y'')) OR  NVL(cszp.available_flag, ''N'') != ''N'' ) ' ||


        -- If the recipient country is non-US, exclude all but floral products.
        'AND ( ''' || inCountryId || ''' IS NULL ' ||
          'OR  ''' || inCountryId || ''' = ''US'' ' ||
          'OR pm.ship_method_florist = ''Y'' ) ' ||

        -- Screen out products listed as excluded by state for the supplied zipcode
        'AND NOT EXISTS ( ' ||
            'SELECT 1 ' ||
            'FROM ZIP_CODE zc, PRODUCT_EXCLUDED_STATES pes ' ||
            'WHERE zc.zip_code_id = UPPER(''' || cleanZip || ''') ' ||
            'AND pes.product_id = pm.product_id ' ||
            'AND pes.excluded_state = zc.state_id ' ||
            'AND pes.SUN = ''Y'' ' ||
            'AND pes.MON = ''Y'' ' ||
            'AND pes.TUE = ''Y'' ' ||
            'AND pes.WED = ''Y'' ' ||
            'AND pes.THU = ''Y'' ' ||
            'AND pes.FRI = ''Y'' ' ||
            'AND pes.SAT = ''Y'' ' ||
        ') ' ||

        -- If the script code is JCPenny, exclude B and C JCP Categories.
        'AND ( decode(''' || scriptCode || ''', null, ''X'', ''' || scriptCode || ''') != ''JP'' ' ||
          'OR pm.JCP_CATEGORY = ''A'' ) ' ||

        -- If the recipient state is Alaska or Hawaii, exclude dropship products that
        --   do NOT have second day shipping.  DO NOT exclude floral.
        'AND  ( ''' || stateId || ''' IS NULL ' ||
        'OR ''' || stateId || ''' NOT IN (''AK'',''HI'') ' ||
        'OR    (pm.ship_method_florist = ''Y'' AND pm.ship_method_carrier != ''Y'')  ' ||
                -- Check for SDG with florist delivery.  If the SDG product has florist delivery then
                -- we should not exclude it from the list
        'OR    (pm.product_type IN (''SDG'',''SDFC'') AND NVL(cszp.available_flag, ''N'') != ''N'')' ||
        'OR     EXISTS ( ' ||
            'SELECT 1 FROM PRODUCT_SHIP_METHODS psm ' ||
            'WHERE psm.product_id = pm.product_id ' ||
            'AND psm.ship_method_id = ''2F'' ' ||
                        'AND pm.PRODUCT_TYPE NOT IN (''FRECUT'') ' ||
                  ') ' ||
                ') ' ||

                -- If the product has exception start and end date, and delivery end date was specified,
                --   exclude if it cannot be delivered sometime prior to delivery end date
                'AND ( ( pm.exception_start_date IS NULL OR pm.exception_end_date IS NULL OR ' ||
                        'pm.exception_code IS NULL OR ''' || deliveryEnd || ''' IS NULL ) ' ||
                  'OR  ( pm.exception_code = ''U'' AND ( TO_DATE(''' || currentDate || ''', ''mm/dd/yyyy'') < pm.exception_start_date ' ||
                    'OR TO_DATE(''' || deliveryEnd || ''', ''mm/dd/yyyy'') > pm.exception_end_date ) ) ' ||
                  'OR  ( pm.exception_code = ''A'' AND ' ||
                      '( ( pm.exception_start_date >= TO_DATE(''' || currentDate || ''', ''mm/dd/yyyy'') ' ||
                      ' AND pm.exception_start_date <= TO_DATE(''' || deliveryEnd || ''', ''mm/dd/yyyy'') ) OR ' ||
                      '( pm.exception_end_date >= TO_DATE(''' || currentDate || ''', ''mm/dd/yyyy'') ' ||
                      ' AND pm.exception_end_date <= TO_DATE(''' || deliveryEnd || ''', ''mm/dd/yyyy'') ) ' ||
                ') ) ) ' ||

        -- If the product is dropshipped, determine the shipper on SHIPPING_KEYS
        'AND DECODE(pm.ship_method_carrier, ''Y'', pm.shipping_key, NULL) = sk.shipping_key_id (+) ' ||
        'AND pm.product_id = psc.product_id (+) ' ||
        'AND pm.vendor_id = psbc.product_id (+) ' ||
        'AND pm.vendor_id = pdbc.product_id (+) ' ||
        'AND pm.product_id = pcc.product_id (+) ' ||

        -- Use the source code to get shipping and handling for florist delivered
        'AND sc.source_code = UPPER(''' || sourceCode || ''') ' ||
        'AND sc.shipping_code = snh.snh_id (+) ' ||

        -- Find out product ship days if specified
        'AND pm.product_id = psd.product_id (+) ' ||

        -- Excluded shipping days
        'AND pm.product_id = pes2.product_id (+) ' ||
        'AND '''|| stateId ||''' = pes2.excluded_state (+) ' ||

        -- For standard, deluxe and premium prices (if given),
        --   determine the discount type and amount by pricing code
        'AND ''' || pricingCode || ''' = phd_std.price_header_id (+) ' ||
        'AND pm.standard_price >= phd_std.min_dollar_amt (+) ' ||
        'AND pm.standard_price <= phd_std.max_dollar_amt (+) ' ||
        'AND ''' || pricingCode || ''' = phd_dlx.price_header_id (+) ' ||
        'AND pm.deluxe_price >= phd_dlx.min_dollar_amt (+) ' ||
        'AND pm.deluxe_price <= phd_dlx.max_dollar_amt (+) ' ||
        'AND ''' || pricingCode || ''' = phd_prm.price_header_id (+) ' ||
        'AND pm.premium_price >= phd_prm.min_dollar_amt (+) ' ||
        'AND pm.premium_price <= phd_prm.max_dollar_amt (+) ' ||

        -- Filter out products that do not match price range, if provided
        'AND EXISTS ' ||
            '(Select 1 from filter_price_points pp ' ||
              'where pm.standard_price >= pp.price_lower_limit ' ||
                'and pm.standard_price <= pp.price_upper_limit ' ||
                'and pp.price_point_id = ' ||
                   'DECODE (''' || inPricePointId || ''', NULL, pp.price_point_id, ''' || inPricePointId || ''')) ' ||

        'ORDER BY DECODE(pmv.UPSELL_PRODUCT_FLAG, ''Y'', pmv.UPSELL_SEARCH_PRIORITY, pm.search_priority)';

    OPEN cur_cursor FOR mySelect;
    RETURN cur_cursor;

END;
.
/
/
