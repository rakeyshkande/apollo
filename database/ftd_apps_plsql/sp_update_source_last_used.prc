CREATE OR REPLACE PROCEDURE FTD_APPS.SP_UPDATE_SOURCE_LAST_USED 
   (IN_NUM_DAYS_PAST       IN    NUMBER      DEFAULT 7,
    OUT_STATUS             OUT   VARCHAR2,
    OUT_MESSAGE            OUT   VARCHAR2) 
AS
/*-----------------------------------------------------------------------------

 Name:    SP_UPDATE_SOURCE_LAST_USED
 Type:    Procedure
 Syntax:  SP_UPDATE_SOURCE_LAST_USED(num_days, status, err_msg)

Description:
        This stored procedure will update the source_master last_used_on_order_date
        for any source codes used in recent orders.
        
Input:
        in_num_days_past    number
        
Output:
        out_status          varchar2    Y if Successful, N if Error
        out_message         varchar2    loaded with oracle error if status=N

-----------------------------------------------------------------------------*/

--
-- Get the latest date the source_code was used on either orders
-- or order_details (avoid using "updated_on >=" - results in FTS)
--
cursor last_cursor is
select source_Code, max(updated_on) last_used_on_date
from (select source_Code,max(updated_on) updated_on
      from clean.orders
      where updated_on between sysdate - in_num_days_past and sysdate
      group by source_code
      UNION
      select source_Code,max(updated_on)
      from clean.order_details
      where updated_on between sysdate - in_num_days_past and sysdate
      group by source_code)
group by source_code;



BEGIN

  OUT_STATUS := 'Y';

   for last_rec in last_cursor LOOP
      begin
        update source_master
        set last_used_on_order_date = last_rec.last_used_on_date,
            updated_by              = 'sp_update_source_last_used proc',
            updated_on              = sysdate
        where source_code = last_rec.source_code
        and last_used_on_order_date <> last_rec.last_used_on_date
        ;
        
        --if sql%rowcount > 0 then
        --   dbms_output.put_line('Update Source ' || last_rec.source_code ||
        --       ' last_used to ' || last_rec.last_used_on_date);
        --else
        --   dbms_output.put_line('Source ' || last_rec.source_code ||
        --       ' last used on ' || last_rec.last_rec.last_used_on_date || 
        --         ', but has NO source_master entry!!');
        --end if;

      exception
         when others then
           ROLLBACK;
           OUT_STATUS := 'N';
           OUT_MESSAGE := 'ERROR OCCURRED UPDATING LAST_USED_DATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   
           RETURN;
      end;
   END LOOP;

   -- everything went good!
   commit;   


/* Trouble? */
EXCEPTION
   when others then
      ROLLBACK;
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'UNEXPECTED ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   

END SP_UPDATE_SOURCE_LAST_USED;
/