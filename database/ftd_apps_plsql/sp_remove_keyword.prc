CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_KEYWORD (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_KEYWORD
-- Type:    Procedure
-- Syntax:  SP_REMOVE_KEYWORD ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_KEYWORDS by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_KEYWORDS
  where PRODUCT_ID = productId;

end
;
.
/
