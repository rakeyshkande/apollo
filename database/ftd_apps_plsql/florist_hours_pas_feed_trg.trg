CREATE OR REPLACE TRIGGER ftd_apps.florist_hours_pas_feed_trg
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.florist_hours
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE

   procedure ins_pas(in_florist       IN varchar2,
                     in_day_of_week   IN varchar2) is

   v_zip_count number;
   type array_t is varray(7) of varchar2(100);
   days_array array_t := array_t('SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY');
   v_today varchar2(100);
   v_today_num int := 0;
   v_date_num int := 0;
   v_diff int;
   
   begin
   
      select count(*)
      into v_zip_count
      from florist_zips
      where florist_id = in_florist;
      
      IF v_zip_count > 0 THEN   
         IF trim(to_char(sysdate, 'DAY')) = in_day_of_week THEN
            PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_FLORIST', in_florist, 0);
         ELSE
            -- assign a priority of 1 to 6 based on the number of days in the future from today
            v_today := trim(to_char(sysdate, 'DAY'));
    
            FOR x in days_array.first..days_array.last LOOP
               if days_array(x) = v_today then
                  v_today_num := x;
               end if;
               if days_array(x) = in_day_of_week then
                  v_date_num := x;
               end if;
            END LOOP;
    
            v_diff := v_date_num - v_today_num;
            if v_diff < 0 then
               v_diff := v_diff + 7;
            end if;
            PAS.POST_PAS_COMMAND_PRIORITY('MODIFIED_FLORIST', in_florist || ' ' || in_day_of_week, v_diff);
         END IF;
      END IF;
     
   end ins_pas;
   
BEGIN

   IF INSERTING THEN
      -- only recalculate PAS if the new open_close_code is not null
      if :new.open_close_code is not null then
         ins_pas(:new.florist_id, :new.day_of_week);
      end if;
   ELSIF UPDATING  THEN
      -- there is no current way to update florist id or day of week but I will account for it anyway
      IF :old.florist_id <> :new.florist_id THEN
         ins_pas(:old.florist_id, :old.day_of_week);
         ins_pas(:new.florist_id, :new.day_of_week);
      ELSIF :old.day_of_week <> :new.day_of_week THEN
         ins_pas(:old.florist_id, :old.day_of_week);
         ins_pas(:new.florist_id, :new.day_of_week);
      ELSIF nvl(:old.open_close_code, 'X') <> nvl(:new.open_close_code, 'X') THEN
         ins_pas(:new.florist_id, :new.day_of_week);
      END IF;
   ELSIF DELETING  THEN
      ins_pas(:old.florist_id, :old.day_of_week);
   END IF;
   
END;
/
