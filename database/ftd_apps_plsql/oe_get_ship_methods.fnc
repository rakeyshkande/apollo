CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_SHIP_METHODS RETURN types.ref_cursor
--====================================================================
--
-- Name:    OE_GET_SHIP_METHODS
-- Type:    Function
-- Syntax:  OE_GET_SHIP_METHODS ()
-- Returns: ref_cursor for
--          shipMethodId  VARCHAR2(2)
--			description VARCHAR2(25)
--
-- Description:   Returns ship method ID and Description
--
--====================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT SHIP_METHOD_ID as "shipMethodId",
			   DESCRIPTION as "description"
        FROM SHIP_METHODS
        ORDER BY ship_method_id;

    RETURN cur_cursor;
END
;
.
/
