CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_SOURCECODE_BY_YPCODE (
    yellowPagesCode IN VARCHAR2
 )
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_SOURCECODE_BY_YPCODE
-- Type:    Function
-- Syntax:  OE_GET_SOURCECODE_BY_YPCODE ( yellowPagesCode IN VARCHAR2 )
-- Returns: ref_cursor for
--          SOURCE_CODE                 VARCHAR2(10)
--          DESCRIPTION                 VARCHAR2(25)
--
-- Description:   Queries the SOURCE table by YELLOW_PAGES_CODE, returns the
--                resulting row's source code and description.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN

  OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
             DESCRIPTION as "description"
      FROM SOURCE
      WHERE YELLOW_PAGES_CODE = yellowPagesCode
      ORDER BY DESCRIPTION;

  RETURN cur_cursor;
END
;
.
/
