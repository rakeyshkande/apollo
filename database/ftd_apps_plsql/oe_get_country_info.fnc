CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_COUNTRY_INFO (countryId  IN VARCHAR2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_COUNTRY_INFo
-- Type:    Function
-- Syntax:  OE_GET_COUNTRY_INFo ( countryId IN NUMBER )
-- Returns: ref_cursor for
--          countryName             VARCHAR2
--          oeCountryType           VARCHAR2
--          addOnDays               NUMBER
--          cableFee                NUMBER
--
-- Description:   Queries the COUNTRY_MASTER table by country ID.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
  IF LENGTH(countryId)=3 THEN
    OPEN cur_cursor FOR
        SELECT NAME           as "countryName",
              OE_COUNTRY_TYPE as "oeCountryType",
              ADD_ON_DAYS     as "addOnDays",
              '12.99'         as "cableFee"
        FROM COUNTRY_MASTER
        WHERE THREE_CHARACTER_ID = countryId;
  ELSE
    OPEN cur_cursor FOR
        SELECT NAME           as "countryName",
              OE_COUNTRY_TYPE as "oeCountryType",
              ADD_ON_DAYS     as "addOnDays",
              '12.99'         as "cableFee"
        FROM COUNTRY_MASTER
        WHERE COUNTRY_ID = countryId;
  END IF;

    RETURN cur_cursor;
END;
.
/
