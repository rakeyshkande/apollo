CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_AZ_PRODUCT_ATTRIBUTES
    (productId IN varchar2,
    attributeType IN varchar2)
    RETURN types.ref_cursor

--
-- Name:    SP_GET_AZ_PRODUCT_ATTRIBUTES
-- Type:    Function
-- Returns: ref_cursor for
--
--
-- Description:   Queries the PTN_AMAZON.AZ_PRODUCT_ATTRIBUTES table by PRODUCT_ID and 
--                PRODUCT_ATTRIBUTE_TYPE and returns a ref cursor to the resulting row.
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN

   OPEN cur_cursor FOR
      SELECT product_id,
         product_attribute_type,
         product_attribute_value,
         product_attribute_sequence
      FROM PTN_AMAZON.AZ_PRODUCT_ATTRIBUTES
      WHERE PRODUCT_ID = productId
      AND PRODUCT_ATTRIBUTE_TYPE = attributeType
      ORDER BY product_attribute_sequence;
   
    RETURN cur_cursor;
END;
/
