CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_RECIPIENT_LIST (phoneIn varchar2)
  RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_FLORIST_LIST
-- Type:    Function
-- Syntax:  OE_GET_FLORIST_LIST (phoneIn VARCHAR2 )
-- Returns: ref_cursor for
--          BFH_NAME      VARCHAR2(20)
--          LAST_NAME     VARCHAR2(30)
--          FIRST_NAME    VARCHAR2(30)
--          ADDRESS_1     VARCHAR2(30)
--          ADDRESS_2     VARCHAR2(30)
--          CITY          VARCHAR2(30)
--          STATE         VARCHAR2(2)
--          ZIP_CODE      VARCHAR2(10)
--
-- Description:   Queries the CUSTOMER table by PHONE NUMBER and returns a ref cursor for all
--                resulting rows.
--
--==================================================\

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT BFH_NAME || ' - ' || LAST_NAME || ' '||
               FIRST_NAME || ' - ' || ADDRESS_1 || ' - ' ||
               ADDRESS_2 || ' - ' || CITY || ' - ' ||
               STATE || ' - ' || ZIP_CODE as "result"
        FROM CUSTOMER
        WHERE HOME_PHONE = OE_CLEANUP_ALPHANUM_STRING(phoneIn, 'N')
		or WORK_PHONE = OE_CLEANUP_ALPHANUM_STRING(phoneIn, 'N')
        ORDER BY LAST_NAME, FIRST_NAME;

    RETURN cur_cursor;
END;
.
/
