CREATE OR REPLACE
TRIGGER ftd_apps.source_program_ref_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.source_program_ref REFERENCING OLD AS old NEW AS new FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.source_program_ref$ (
      source_program_ref_id,
      source_code,
      program_name,
      start_date,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.source_program_ref_id,
      :NEW.source_code,
      :NEW.program_name,
      :NEW.start_date,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.source_program_ref$ (
      source_program_ref_id,
      source_code,
      program_name,
      start_date,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.source_program_ref_id,
      :OLD.source_code,
      :OLD.program_name,
      :OLD.start_date,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.source_program_ref$ (
      source_program_ref_id,
      source_code,
      program_name,
      start_date,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.source_program_ref_id,
      :NEW.source_code,
      :NEW.program_name,
      :NEW.start_date,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.source_program_ref$ (
      source_program_ref_id,
      source_code,
      program_name,
      start_date,
      created_on,
      created_by,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.source_program_ref_id,
      :OLD.source_code,
      :OLD.program_name,
      :OLD.start_date,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
