CREATE OR REPLACE
FUNCTION ftd_apps.GET_TIME_IN_MILLIS ( str in varchar2 )
 return varchar2
 as LANGUAGE JAVA
 NAME 'MyTimestamp.getTimeInMillis(java.lang.String) return java.lang.String';
.
/
