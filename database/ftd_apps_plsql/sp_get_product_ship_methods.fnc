CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_SHIP_METHODS (productId IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_SHIP_METHODS
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_SHIP_METHODS (productId IN varchar2 )
-- Returns: ref_cursor for
--          SHIP_METHOD_ID  VARCHAR2(10)
--
-- Description:   Queries the PRODUCT_SHIP_METHODS table by PRODUCT_ID
--                returning a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT SHIP_METHOD_ID as "shipMethodId",DEFAULT_CARRIER as "defaultcarrier"
        FROM PRODUCT_SHIP_METHODS
    WHERE PRODUCT_ID = productId;


    RETURN cur_cursor;
END
;
.
/
