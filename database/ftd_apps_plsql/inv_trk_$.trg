CREATE OR REPLACE TRIGGER ftd_apps.inv_trk_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.inv_trk 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN

      insert into ftd_apps.inv_trk$
           (inv_trk_id,
            product_id,
            vendor_id,
            start_date,
            end_date,
            forecast_qty,
            revised_forecast_qty,
            warning_threshold_qty,
            shutdown_threshold_qty,
            comments,
            status,
            created_by,
            created_on,
            updated_by,
            updated_on,
            OPERATION$, TIMESTAMP$)
       VALUES (
            :NEW.inv_trk_id,
            :NEW.product_id,
            :NEW.vendor_id,
            :NEW.start_date,
            :NEW.end_date,
            :NEW.forecast_qty,
            :NEW.revised_forecast_qty,
            :NEW.warning_threshold_qty,
            :NEW.shutdown_threshold_qty,
            :NEW.comments,
            :NEW.status,
            :NEW.created_by,
            :NEW.created_on,
            :NEW.updated_by,
            :NEW.updated_on,
            'INS', v_current_timestamp);

   ELSIF UPDATING  THEN
      insert into ftd_apps.inv_trk$
           (inv_trk_id,
            product_id,
            vendor_id,
            start_date,
            end_date,
            forecast_qty,
            revised_forecast_qty,
            warning_threshold_qty,
            shutdown_threshold_qty,
            comments,
            status,
            created_by,
            created_on,
            updated_by,
            updated_on,
            OPERATION$, TIMESTAMP$)
       VALUES (
            :OLD.inv_trk_id,
            :OLD.product_id,
            :OLD.vendor_id,
            :OLD.start_date,
            :OLD.end_date,
            :OLD.forecast_qty,
            :OLD.revised_forecast_qty,
            :OLD.warning_threshold_qty,
            :OLD.shutdown_threshold_qty,
            :OLD.comments,
            :OLD.status,
            :OLD.created_by,
            :OLD.created_on,
            :OLD.updated_by,
            :OLD.updated_on,
            'UPD_OLD', v_current_timestamp);

      insert into ftd_apps.inv_trk$
           (inv_trk_id,
            product_id,
            vendor_id,
            start_date,
            end_date,
            forecast_qty,
            revised_forecast_qty,
            warning_threshold_qty,
            shutdown_threshold_qty,
            comments,
            status,
            created_by,
            created_on,
            updated_by,
            updated_on,
            OPERATION$, TIMESTAMP$)
       VALUES (
            :NEW.inv_trk_id,
            :NEW.product_id,
            :NEW.vendor_id,
            :NEW.start_date,
            :NEW.end_date,
            :NEW.forecast_qty,
            :NEW.revised_forecast_qty,
            :NEW.warning_threshold_qty,
            :NEW.shutdown_threshold_qty,
            :NEW.comments,
            :NEW.status,
            :NEW.created_by,
            :NEW.created_on,
            :NEW.updated_by,
            :NEW.updated_on,
            'UPD_NEW', v_current_timestamp);


   ELSIF DELETING  THEN

      insert into ftd_apps.inv_trk$
           (inv_trk_id,
            product_id,
            vendor_id,
            start_date,
            end_date,
            forecast_qty,
            revised_forecast_qty,
            warning_threshold_qty,
            shutdown_threshold_qty,
            comments,
            status,
            created_by,
            created_on,
            updated_by,
            updated_on,
            OPERATION$, TIMESTAMP$)
       VALUES (
            :OLD.inv_trk_id,
            :OLD.product_id,
            :OLD.vendor_id,
            :OLD.start_date,
            :OLD.end_date,
            :OLD.forecast_qty,
            :OLD.revised_forecast_qty,
            :OLD.warning_threshold_qty,
            :OLD.shutdown_threshold_qty,
            :OLD.comments,
            :OLD.status,
            :OLD.created_by,
            :OLD.created_on,
            :OLD.updated_by,
            :OLD.updated_on,
            'DEL',v_current_timestamp);

   END IF;

END;
/
