CREATE OR REPLACE 
PACKAGE BODY FTD_APPS.CONTENT_MAINT_PKG AS

PROCEDURE INSERT_CONTENT_MASTER
(
    IN_CONTENT_MASTER_ID        IN CONTENT_MASTER.CONTENT_MASTER_ID%TYPE,
    IN_CONTENT_CONTEXT          IN CONTENT_MASTER.CONTENT_CONTEXT%TYPE,
    IN_CONTENT_NAME             IN CONTENT_MASTER.CONTENT_NAME%TYPE,
    IN_CONTENT_DESCRIPTION      IN CONTENT_MASTER.CONTENT_DESCRIPTION%TYPE,
    IN_CONTENT_FILTER_ID1       IN CONTENT_MASTER.CONTENT_FILTER_ID1%TYPE,
    IN_CONTENT_FILTER_ID2       IN CONTENT_MASTER.CONTENT_FILTER_ID2%TYPE,
    IN_UPDATED_BY               IN CONTENT_MASTER.UPDATED_BY%TYPE,

    OUT_STATUS                  OUT VARCHAR2,
    OUT_CONTENT_MASTER_ID       OUT CONTENT_MASTER.CONTENT_MASTER_ID%TYPE,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the CONTENT_MASTER table

Input:
    IN_CONTENT_MASTER_ID        Master ID, if zero then insert, otherwise update
    IN_CONTENT_CONTEXT          Conext text
    IN_CONTENT_NAME             Name
    IN_CONTENT_DESCRIPTION      Description
    IN_CONTENT_FILTER_ID1       FK to the content_filter table
    IN_CONTENT_FILTER_ID2       FK to the content_filter table
    IN_UPDATED_BY               Who updated

Output:
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR check_for_duplicate IS
        select  'Y'
        from    content_master
        where   content_context = in_content_context
        and     content_name = in_content_name;

v_duplicate char(1) := 'N';
v_content_master_id NUMBER;

BEGIN

    OUT_STATUS := 'Y';

    IF IN_CONTENT_MASTER_ID > 0 THEN

      UPDATE CONTENT_MASTER 
      SET 
        CONTENT_CONTEXT = IN_CONTENT_CONTEXT,
        CONTENT_NAME = IN_CONTENT_NAME,
        CONTENT_DESCRIPTION = IN_CONTENT_DESCRIPTION,
        CONTENT_FILTER_ID1 = IN_CONTENT_FILTER_ID1,
        CONTENT_FILTER_ID2 = IN_CONTENT_FILTER_ID2,
        UPDATED_BY = IN_UPDATED_BY,
        UPDATED_ON = sysdate
      WHERE  content_master_id = IN_CONTENT_MASTER_ID;

    ELSE

      OPEN check_for_duplicate;
      FETCH check_for_duplicate INTO v_duplicate;
      CLOSE check_for_duplicate;

      IF (v_duplicate = 'Y') THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Duplicate: A record already exists with these values';
      ELSE

        SELECT content_master_sq.nextval into v_content_master_id from dual;
        OUT_CONTENT_MASTER_ID := v_content_master_id;

        INSERT INTO CONTENT_MASTER (
          CONTENT_MASTER_ID,
          CONTENT_CONTEXT,
          CONTENT_NAME,
          CONTENT_DESCRIPTION,
          CONTENT_FILTER_ID1,
          CONTENT_FILTER_ID2,
          UPDATED_BY,
          UPDATED_ON,
          CREATED_BY,
          CREATED_ON
        ) VALUES (
          v_content_master_id,
          IN_CONTENT_CONTEXT,
          IN_CONTENT_NAME,
          IN_CONTENT_DESCRIPTION,
          IN_CONTENT_FILTER_ID1,
          IN_CONTENT_FILTER_ID2,
          IN_UPDATED_BY,
          sysdate,
          IN_UPDATED_BY,
          sysdate
        );

      END IF;

  END IF;


EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CONTENT_MASTER;

PROCEDURE INSERT_CONTENT_DETAIL
(
    IN_CONTENT_DETAIL_ID        IN CONTENT_DETAIL.CONTENT_DETAIL_ID%TYPE,
    IN_CONTENT_MASTER_ID        IN CONTENT_DETAIL.CONTENT_MASTER_ID%TYPE,
    IN_FILTER_1_VALUE           IN CONTENT_DETAIL.FILTER_1_VALUE%TYPE,
    IN_FILTER_2_VALUE           IN CONTENT_DETAIL.FILTER_2_VALUE%TYPE,
    IN_CONTENT_TXT              IN CONTENT_DETAIL.CONTENT_TXT%TYPE,
    IN_UPDATED_BY               IN CONTENT_DETAIL.UPDATED_BY%TYPE,

    OUT_CONTENT_DETAIL_ID       OUT CONTENT_DETAIL.CONTENT_DETAIL_ID%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
Description:
        Inserts a row in to the CONTENT_DETAIL table

Input:
    IN_CONTENT_DETAIL_ID        Detail Id being updated
    IN_CONTENT_MASTER_ID        Master Id being updated
    IN_FILTER_1_VALUE           Filter 1 value
    IN_FILTER_2_VALUE           Filter 2 value
    IN_CONTENT_TXT              Content Text Value
    IN_UPDATED_BY               Who updated

Output:
    OUT_CONTENT_DETAIL_ID       New content_detail_id
    OUT_STATUS                  Status of the insert
    OUT_MESSAGE                 Error message, if any

-----------------------------------------------------------------------------*/

CURSOR check_for_duplicate IS
        select  'Y'
        from    content_detail
        where   content_master_id = in_content_master_id
        and     content_detail_id <> in_content_detail_id
        and     nvl(filter_1_value, '0') = nvl(in_filter_1_value, '0')
        and     nvl(filter_2_value, '0') = nvl(in_filter_2_value, '0');

v_duplicate char(1) := 'N';
v_content_detail_id NUMBER := in_content_detail_id;

BEGIN

    OPEN check_for_duplicate;
    FETCH check_for_duplicate INTO v_duplicate;
    CLOSE check_for_duplicate;

    IF (v_duplicate = 'Y') THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Duplicate: A record already exists with these filter values';
    ELSE
        IF (in_content_detail_id = 0) THEN
            SELECT content_detail_sq.nextval into v_content_detail_id from dual;
            out_content_detail_id := v_content_detail_id;

            INSERT INTO CONTENT_DETAIL (
              CONTENT_DETAIL_ID,
              CONTENT_MASTER_ID,
              FILTER_1_VALUE,
              FILTER_2_VALUE,
              CONTENT_TXT,
              UPDATED_BY,
              UPDATED_ON,
              CREATED_BY,
              CREATED_ON
            ) VALUES (
              v_content_detail_id,
              IN_CONTENT_MASTER_ID,
              IN_FILTER_1_VALUE,
              IN_FILTER_2_VALUE,
              IN_CONTENT_TXT,
              IN_UPDATED_BY,
              sysdate,
              IN_UPDATED_BY,
              sysdate
            );
        ELSE
            update content_detail
            set filter_1_value = in_filter_1_value,
                filter_2_value = in_filter_2_value,
                content_txt = in_content_txt,
                updated_by = in_updated_by,
                updated_on = sysdate
            where content_detail_id = in_content_detail_id;
        END IF;

        OUT_STATUS := 'Y';
    END IF;

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CONTENT_DETAIL;

END CONTENT_MAINT_PKG;
/
