-- FTD_APPS.VENDOR_PRODUCT_$ to populate FTD_APPS.VENDOR_PRODUCT$ shadow table
CREATE OR REPLACE TRIGGER ftd_apps.vendor_product_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.vendor_product 
REFERENCING OLD AS old NEW AS new FOR EACH ROW
DECLARE

v_change_id ftd_apps.product_change$_acct.change_id%type;
v_operation ftd_apps.vendor_product$.operation$%TYPE;
v_now ftd_apps.vendor_product$.timestamp$%TYPE := systimestamp;
v_change_unit_id ftd_apps.product_acctg_change_unit.acctg_change_unit_id%TYPE;
v_change_code ftd_apps.product_acctg_change_class.acctg_change_class_code%TYPE;

BEGIN
   -- check if accounting-pertinent fields changed
   IF ( INSERTING OR DELETING OR 
        (:OLD.VENDOR_ID != :NEW.VENDOR_ID) OR
        (:OLD.PRODUCT_SUBCODE_ID != :NEW.PRODUCT_SUBCODE_ID) OR
        (:OLD.VENDOR_COST != :NEW.VENDOR_COST)) THEN  
        
      -- create change unit record
      select acctg_change_unit_id_sq.NEXTVAL into v_change_unit_id from dual;  
      
      -- determine change code:
      IF UPDATING THEN
	      -- check if the change is availability only, delivery method only, or neither.
	      IF ( (:OLD.VENDOR_ID = :NEW.VENDOR_ID) AND
                   (:OLD.PRODUCT_SUBCODE_ID = :NEW.PRODUCT_SUBCODE_ID) AND
                   (:OLD.VENDOR_COST = :NEW.VENDOR_COST) AND
                   (:OLD.AVAILABLE != :NEW.AVAILABLE)    
		 ) THEN
		 -- availability change only
		 v_change_code := 'A';
	      ELSE
		 -- not a change that needs to be  filtered
		 v_change_code := 'O';
	      END IF;
      ELSE
      	  -- deleting or inserting
      	  v_change_code := 'O';
      END IF;
	  
      INSERT INTO ftd_apps.product_acctg_change_unit
      (
      	acctg_change_unit_id,
      	acctg_change_class_code,
      	created_on
      )
      VALUES
      (
      	v_change_unit_id,
      	v_change_code,
      	sysdate
      );
   
   END IF;

   IF INSERTING THEN
      v_operation := 'INS';
      INSERT INTO ftd_apps.vendor_product$ (
        VENDOR_ID, 
	PRODUCT_SUBCODE_ID, 
	VENDOR_SKU, 
	VENDOR_COST, 
	AVAILABLE, 
	REMOVED,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id,
        vendor_cost_eff_date
      ) VALUES (
        :NEW.VENDOR_ID, 
	:NEW.PRODUCT_SUBCODE_ID, 
	:NEW.VENDOR_SKU, 
	:NEW.VENDOR_COST, 
	:NEW.AVAILABLE, 
	:NEW.REMOVED,
        :NEW.CREATED_ON, 
	:NEW.CREATED_BY, 
	:NEW.UPDATED_ON, 
	:NEW.UPDATED_BY,
        v_operation,
        v_now,
        v_change_unit_id,
        :NEW.vendor_cost_eff_date);
   ELSIF UPDATING  THEN
      v_operation := 'UPD';
      INSERT INTO ftd_apps.vendor_product$ (
        VENDOR_ID, 
	PRODUCT_SUBCODE_ID, 
	VENDOR_SKU, 
	VENDOR_COST, 
	AVAILABLE, 
	REMOVED,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id,
        vendor_cost_eff_date
      ) VALUES (
        :OLD.VENDOR_ID, 
	:OLD.PRODUCT_SUBCODE_ID, 
	:OLD.VENDOR_SKU, 
	:OLD.VENDOR_COST, 
	:OLD.AVAILABLE, 
	:OLD.REMOVED,
        :OLD.CREATED_ON, 
	:OLD.CREATED_BY, 
	:OLD.UPDATED_ON, 
	:OLD.UPDATED_BY,
        'UPD_OLD',
        v_now,
        v_change_unit_id,
        :OLD.vendor_cost_eff_date);
      INSERT INTO ftd_apps.vendor_product$ (
        VENDOR_ID, 
	PRODUCT_SUBCODE_ID, 
	VENDOR_SKU, 
	VENDOR_COST, 
	AVAILABLE, 
	REMOVED,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id,
        vendor_cost_eff_date
      ) VALUES (
        :NEW.VENDOR_ID, 
	:NEW.PRODUCT_SUBCODE_ID, 
	:NEW.VENDOR_SKU, 
	:NEW.VENDOR_COST, 
	:NEW.AVAILABLE, 
	:NEW.REMOVED,
        :NEW.CREATED_ON, 
	:NEW.CREATED_BY, 
	:NEW.UPDATED_ON, 
	:NEW.UPDATED_BY,
        'UPD_NEW',
        v_now,
        v_change_unit_id,
        :NEW.vendor_cost_eff_date);
   ELSIF DELETING  THEN
      v_operation := 'DEL';
      INSERT INTO ftd_apps.vendor_product$ (
        VENDOR_ID, 
	PRODUCT_SUBCODE_ID, 
	VENDOR_SKU, 
	VENDOR_COST, 
	AVAILABLE, 
	REMOVED,
        CREATED_ON, 
	CREATED_BY, 
	UPDATED_ON, 
	UPDATED_BY,
        operation$, 
        timestamp$,
        acctg_change_unit_id,
        vendor_cost_eff_date
      ) VALUES (
        :OLD.VENDOR_ID, 
	:OLD.PRODUCT_SUBCODE_ID, 
	:OLD.VENDOR_SKU, 
	:OLD.VENDOR_COST, 
	:OLD.AVAILABLE, 
	:OLD.REMOVED,
        :OLD.CREATED_ON, 
	:OLD.CREATED_BY, 
	:OLD.UPDATED_ON, 
	:OLD.UPDATED_BY,
        v_operation,
        v_now,
        v_change_unit_id,
        :OLD.vendor_cost_eff_date);
   END IF;

   -- Verify that an accounting-pertinent metric has changed.
   IF ((v_operation = 'DEL') OR
     (v_operation = 'INS') OR
     (:OLD.VENDOR_ID != :NEW.VENDOR_ID) OR
     (:OLD.PRODUCT_SUBCODE_ID != :NEW.PRODUCT_SUBCODE_ID) OR
     (:OLD.VENDOR_COST != :NEW.VENDOR_COST) OR
     (:OLD.AVAILABLE != :NEW.AVAILABLE)) THEN

   -- Populate the accounting shadow table.
   -- Obtain a sequence-based surrogate key.
   select product_change$_acct_sq.NEXTVAL into v_change_id from dual;

   IF ((v_operation = 'DEL') OR
       (v_operation = 'UPD')) THEN

   IF (v_operation = 'UPD') THEN
      v_operation := 'UPD_OLD';
   END IF;

   INSERT INTO FTD_APPS.VENDOR_PRODUCT$_ACCT 
   (	CHANGE_ID, 
        VENDOR_ID, 
	PRODUCT_ID,  
	VENDOR_COST, 
	AVAILABLE
   ) VALUES (
      v_change_id,
      :OLD.VENDOR_ID,
      :OLD.PRODUCT_SUBCODE_ID,
      :OLD.VENDOR_COST,
      :OLD.AVAILABLE
   );

   -- Populate the remaining accounting audit tables.
   FTD_APPS.INSERT_PRODUCT_CHANGE_ACCT (
      'VENDOR_PRODUCT',
      v_change_id,
      :OLD.PRODUCT_SUBCODE_ID,
      :OLD.UPDATED_BY,
      v_operation,
      v_now,
      null);

   IF (v_operation = 'UPD_OLD') THEN
   -- Increment the sequence for the UPD new record.
   select product_change$_acct_sq.NEXTVAL into v_change_id from dual;

   v_operation := 'UPD_NEW';
   END IF;

   END IF;

   IF (v_operation != 'DEL') THEN
   
   INSERT INTO FTD_APPS.VENDOR_PRODUCT$_ACCT 
   (	CHANGE_ID, 
        VENDOR_ID, 
	PRODUCT_ID,  
	VENDOR_COST, 
	AVAILABLE
   ) VALUES (
      v_change_id,
      :NEW.VENDOR_ID,
      :NEW.PRODUCT_SUBCODE_ID,
      :NEW.VENDOR_COST,
      :NEW.AVAILABLE
   );

   -- Populate the remaining accounting audit tables.
   FTD_APPS.INSERT_PRODUCT_CHANGE_ACCT (
      'VENDOR_PRODUCT',
      v_change_id,
      :NEW.PRODUCT_SUBCODE_ID,
      :NEW.UPDATED_BY,
      v_operation,
      v_now,
      null);

   END IF;

   END IF;

END;
/
