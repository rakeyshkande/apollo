CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_PRODUCT_COMPONENTS (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_PRODUCT_COMPONENTS
-- Type:    Procedure
-- Syntax:  SP_REMOVE_PRODUCT_COMPONENTS ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_COMPONENT_SKU by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_COMPONENT_SKU
  where PRODUCT_ID = productId;

end
;
.
/
