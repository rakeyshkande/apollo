CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_FLORAL_SERVICE_CHARGE (productType IN varchar2,
   domesticIntlFlag IN varchar2,
   sourceCode IN varchar2,
   price IN NUMBER,
   shippingKey IN NUMBER,
   deliveryDate IN varchar2)
    RETURN VARCHAR2
--===============================================================
--
-- Name:    OE_GET_FLORAL_SERVICE_CHARGE
-- Type:    Function
--
--
-- Description:   Gets the floral service charge
--
--===============================================================
AS
  charge     VARCHAR2(16);
BEGIN

  -- If this is a same day gift then get the charge from
  -- the shipping key
  IF productType = 'SDG'
  THEN
    select skc.SHIPPING_COST
    into charge
    from SHIPPING_KEY_COSTS skc, SHIPPING_KEY_DETAILS skd
    WHERE skd.SHIPPING_KEY_ID = shippingKey
      AND skd.SHIPPING_DETAIL_ID = skc.SHIPPING_KEY_DETAIL_ID
      AND skd.MIN_PRICE <= price
      AND skd.MAX_PRICE >= price
      AND skc.SHIPPING_METHOD_ID = 'SD';
  ELSE
    select DECODE(UPPER(domesticIntlFlag),
        'D',
        nvl(
            (select sfo.domestic_charge from ftd_apps.service_fee_override sfo
                where sfo.snh_id = snh.snh_id
                and sfo.override_date = to_date(deliveryDate, 'MM/DD/YYYY')
        ), snh.first_order_domestic),
        'I',
        nvl(
            (select sfo.international_charge from ftd_apps.service_fee_override sfo
                where sfo.snh_id = snh.snh_id
                and sfo.override_date = to_date(deliveryDate, 'MM/DD/YYYY')
        ), snh.first_order_international),
        NULL)
    into charge
    from SNH snh, SOURCE sc
    -- Use the source code to get shipping and handling for florist delivered
    WHERE sc.source_code = UPPER(sourceCode)
      AND sc.shipping_code = snh.snh_id;
  END IF;

  RETURN charge;

EXCEPTION WHEN OTHERS THEN
    RETURN null;
END
;
.
/
