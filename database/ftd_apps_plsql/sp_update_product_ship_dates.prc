CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_SHIP_DATES (
 productId in varchar2,
 mondayFlag in varchar2,
 tuesdayFlag in varchar2,
 wednesdayFlag in varchar2,
 thursdayFlag in varchar2,
 fridayFlag in varchar2,
 saturdayFlag in varchar2,
 sundayFlag in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_SHIP_DATES
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_SHIP_DATES ( <see args above> )
--
-- Description:   Performs an INSERT/UPDATE attempt on PRODUCT_SHIP_DATES.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_SHIP_DATES
  INSERT INTO PRODUCT_SHIP_DATES
      ( PRODUCT_ID,
				MONDAY_FLAG,
				TUESDAY_FLAG,
				WEDNESDAY_FLAG,
				THURSDAY_FLAG,
				FRIDAY_FLAG,
				SATURDAY_FLAG,
				SUNDAY_FLAG
		  )
  VALUES
		  ( productId,
				 mondayFlag,
         tuesdayFlag,
         wednesdayFlag,
         thursdayFlag,
         fridayFlag,
         saturdayFlag,
         sundayFlag
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		     -- Update an existing row for this product id if found
    UPDATE PRODUCT_SHIP_DATES
      SET MONDAY_FLAG = mondayFlag,
					TUESDAY_FLAG = tuesdayFlag,
					WEDNESDAY_FLAG = wednesdayFlag,
					THURSDAY_FLAG = thursdayFlag,
					FRIDAY_FLAG = fridayFlag,
					SATURDAY_FLAG = saturdayFlag,
					SUNDAY_FLAG = sundayFlag
      WHERE PRODUCT_ID = productId;
end
;
.
/
