CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_SOURCE (
 productId in varchar2,
 sourceCode in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_SOURCE
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_SOURCE ( productId in varchar2,
--                               sourceCode in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_SOURCE.
--                If the product/sourceCode combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_SOURCE
  INSERT INTO PRODUCT_SOURCE
      ( PRODUCT_ID,
        SOURCE_CODE,
        LAST_MODIFIED_DATE)
  VALUES
      ( productId,
        upper(sourceCode),
        SYSDATE);

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    rollback;
end
;
.
/
