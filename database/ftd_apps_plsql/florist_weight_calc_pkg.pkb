CREATE OR REPLACE
PACKAGE BODY ftd_apps.FLORIST_WEIGHT_CALC_PKG AS

FUNCTION GET_TOTAL_SENT_FROM_FTD
(
IN_PARENT_FLORIST_ID            MERCURY.MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
IN_SUMMARY_DATE                 MERCURY.MERCURY_FLORIST_SUMMARY.SUMMARY_DATE%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:

Input:
        florist_id                      varchar2 (parent florist id)
        summary_date                    date (first of the summary month)

Output:
        sent_count                      number (sum of the florist under and including the parent)

-----------------------------------------------------------------------------*/

v_sent_count                  MERCURY.MERCURY_FLORIST_SUMMARY.SENT_COUNT%TYPE;

-- Cursor gets total sent count of the florists for the given parent florist
CURSOR count_cur IS
        SELECT  sum (sent_count)
        FROM    mercury.mercury_florist_summary
        WHERE   florist_id IN
        (
                select  m.florist_id
                from    florist_master m
                where   m.parent_florist_id = in_parent_florist_id
                or      m.florist_id = in_parent_florist_id
        )
        AND     summary_date = in_summary_date;

BEGIN

OPEN count_cur;
FETCH count_cur INTO v_sent_count;
CLOSE count_cur;

RETURN v_sent_count;

END GET_TOTAL_SENT_FROM_FTD;


PROCEDURE UPDATE_FLORIST_WEIGHT_CALC
(
IN_PARENT_FLORIST_ID            IN FLORIST_WEIGHT_CALC_HISTORY.PARENT_FLORIST_ID%TYPE,
IN_CALC_DATE                    IN FLORIST_WEIGHT_CALC_HISTORY.CALC_DATE%TYPE,
IN_BASE_WEIGHT                  IN FLORIST_WEIGHT_CALC_HISTORY.BASE_WEIGHT%TYPE,
IN_LOW_SENDING_THRESHOLD_WGHT   IN FLORIST_WEIGHT_CALC_HISTORY.LOW_SENDING_THRESHOLD_WEIGHT%TYPE,
IN_RECIPROCITY_WEIGHT           IN FLORIST_WEIGHT_CALC_HISTORY.RECIPROCITY_WEIGHT%TYPE,
IN_FINAL_WEIGHT                 IN FLORIST_WEIGHT_CALC_HISTORY.FINAL_WEIGHT%TYPE,
IN_TOTAL_REVENUE                IN FLORIST_WEIGHT_CALC_HISTORY.TOTAL_REVENUE%TYPE,
IN_REBATE_AMOUNT                IN FLORIST_WEIGHT_CALC_HISTORY.REBATE_AMOUNT%TYPE,
IN_TOTAL_CLEARED_ORDERS         IN FLORIST_WEIGHT_CALC_HISTORY.TOTAL_CLEARED_ORDERS%TYPE,
IN_ORDERS_SENT_FROM_FTD         IN FLORIST_WEIGHT_CALC_HISTORY.ORDERS_SENT_FROM_FTD%TYPE,
IN_LOW_SENDING_THRESHOLD_VALUE  IN FLORIST_WEIGHT_CALC_HISTORY.LOW_SENDING_THRESHOLD_VALUE%TYPE,
IN_RECIPROCITY_RATIO            IN FLORIST_WEIGHT_CALC_HISTORY.RECIPROCITY_RATIO%TYPE,
IN_GROCERY_STORE_FLAG           IN FLORIST_WEIGHT_CALC_HISTORY.GROCERY_STORE_FLAG%TYPE,
IN_ORDER_GATHERER_FLAG          IN FLORIST_WEIGHT_CALC_HISTORY.ORDER_GATHERER_FLAG%TYPE,
IN_SEND_ONLY_FLAG               IN FLORIST_WEIGHT_CALC_HISTORY.SEND_ONLY_FLAG%TYPE,
IN_GROCERY_STORE_WEIGHT         IN FLORIST_WEIGHT_CALC_HISTORY.GROCERY_STORE_WEIGHT%TYPE,
IN_REVENUE_LINKING_KEY          IN FLORIST_WEIGHT_CALC_HISTORY.REVENUE_LINKING_KEY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating the florist
        weight calculation history for the given month.

Input:
        parent_florist_id               varchar2
        calc_date                       date
        base_weight                     number
        low_sending_threshold_weight    number
        reciprocity_weight              number
        final_weight                    number
        total_revenue                   number
        rebate_amount                   number
        total_cleared_orders            number
        orders_sent_from_ftd            number
        low_sending_threshold_value     number
        reciprocity_ratio               number
        grocery_store_flag              char
        order_gatherer_flag             char
        send_only_flag                  char
        grocery_store_weight            number
        revenue_linking_key             char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_calc_date             DATE := to_date (to_char (in_calc_date, 'MM/YYYY'), 'MM/YYYY');
v_exists                NUMBER;
v_order_sent_from_ftd   NUMBER;

CURSOR exists_cur IS
        SELECT  count (1)
        FROM    florist_weight_calc_history
        WHERE   parent_florist_id = in_parent_florist_id
        AND     calc_date = v_calc_date;

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

IF v_exists > 0 THEN

        UPDATE  florist_weight_calc_history
        SET
                base_weight = NVL(in_base_weight, base_weight),
                low_sending_threshold_weight = NVL(in_low_sending_threshold_wght, low_sending_threshold_weight),
                reciprocity_weight = NVL(in_reciprocity_weight, reciprocity_weight),
                final_weight = NVL(in_final_weight, final_weight),
                total_revenue = NVL(in_total_revenue, total_revenue),
                rebate_amount = NVL(in_rebate_amount, rebate_amount),
                total_cleared_orders = NVL(in_total_cleared_orders, total_cleared_orders),
                orders_sent_from_ftd = NVL(in_orders_sent_from_ftd, orders_sent_from_ftd),
                low_sending_threshold_value = NVL(in_low_sending_threshold_value, low_sending_threshold_value),
                reciprocity_ratio = NVL(in_reciprocity_ratio, reciprocity_ratio),
                grocery_store_flag = NVL(in_grocery_store_flag, grocery_store_flag),
                order_gatherer_flag = NVL(in_order_gatherer_flag, order_gatherer_flag),
                send_only_flag = NVL(in_send_only_flag, send_only_flag),
                GROCERY_STORE_WEIGHT = NVL (in_grocery_store_weight, GROCERY_STORE_WEIGHT),
                REVENUE_LINKING_KEY = NVL(in_revenue_linking_key, revenue_linking_key)
        WHERE   parent_florist_id = in_parent_florist_id
        AND     calc_date = v_calc_date;

ELSE
        INSERT INTO florist_weight_calc_history
        (
                parent_florist_id,
                calc_date,
                base_weight,
                low_sending_threshold_weight,
                reciprocity_weight,
                final_weight,
                total_revenue,
                rebate_amount,
                total_cleared_orders,
                orders_sent_from_ftd,
                low_sending_threshold_value,
                reciprocity_ratio,
                grocery_store_flag,
                order_gatherer_flag,
                send_only_flag,
                GROCERY_STORE_WEIGHT,
                revenue_linking_key
        )
        VALUES
        (
                in_parent_florist_id,
                v_calc_date,
                in_base_weight,
                in_low_sending_threshold_wght,
                in_reciprocity_weight,
                in_final_weight,
                in_total_revenue,
                in_rebate_amount,
                in_total_cleared_orders,
                in_orders_sent_from_ftd,
                in_low_sending_threshold_value,
                in_reciprocity_ratio,
                in_grocery_store_flag,
                in_order_gatherer_flag,
                in_send_only_flag,
                in_grocery_store_weight,
                in_revenue_linking_key
        );

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_WEIGHT_CALC;


PROCEDURE GET_FLORIST_REVENUE_REBATE
(
IN_CALC_DATE                    IN FLORIST_WEIGHT_CALC_HISTORY.CALC_DATE%TYPE,
IN_DATE_RANGE_START             IN DATE,
IN_DATE_RANGE_END               IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the revenue and rebate
        rolling average in the given date range for all florists that
        are currently having the weight calculations.

Input:
        calc_date               date
        date range start        date
        date range end          date

Output:
        result set containing the parent florist id, average total revenue,
        and average rebate amount

-----------------------------------------------------------------------------*/

v_calc_date             DATE := to_date (to_char (in_calc_date, 'MM/YYYY'), 'MM/YYYY');
v_date_range_start      DATE := to_date (to_char (in_date_range_start, 'MM/YYYY'), 'MM/YYYY');
v_date_range_end        DATE := to_date (to_char (in_date_range_end, 'MM/YYYY'), 'MM/YYYY');

BEGIN

OPEN OUT_CUR FOR
        SELECT  a.parent_florist_id,
                avg (a.total_revenue) avg_total_revenue,
                avg (a.rebate_amount) avg_rebate_amount
        FROM    florist_weight_calc_history a
        JOIN    florist_weight_calc_history b
        ON      a.parent_florist_id = b.parent_florist_id
        AND     a.calc_date BETWEEN v_date_range_start AND v_date_range_end
        AND     b.calc_date = v_calc_date
        GROUP BY a.parent_florist_id
        ORDER BY a.parent_florist_id;

END GET_FLORIST_REVENUE_REBATE;


PROCEDURE GET_FLORIST_LOW_SENDING
(
IN_CALC_DATE                    IN FLORIST_WEIGHT_CALC_HISTORY.CALC_DATE%TYPE,
IN_DATE_RANGE_START             IN DATE,
IN_DATE_RANGE_END               IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the low sending values
        in the given date range for all florists that are currently having
        the weight calculations.

Input:
        calc_date               date
        date range start        date
        date range end          date

Output:
        result set containing parent florist id, calc date, total cleared orders,
        low sending threshold value, low sending threshold weight
-----------------------------------------------------------------------------*/

v_calc_date             DATE := to_date (to_char (in_calc_date, 'MM/YYYY'), 'MM/YYYY');
v_date_range_start      DATE := to_date (to_char (in_date_range_start, 'MM/YYYY'), 'MM/YYYY');
v_date_range_end        DATE := to_date (to_char (in_date_range_end, 'MM/YYYY'), 'MM/YYYY');

BEGIN

OPEN OUT_CUR FOR
        SELECT  a.parent_florist_id,
                a.calc_date,
                a.total_cleared_orders,
                a.low_sending_threshold_value,
                a.low_sending_threshold_weight
        FROM    florist_weight_calc_history a
        JOIN    florist_weight_calc_history b
        ON      a.parent_florist_id = b.parent_florist_id
        AND     a.calc_date BETWEEN v_date_range_start AND v_date_range_end
        AND     b.calc_date = v_calc_date
        ORDER BY a.parent_florist_id, a.calc_date;

END GET_FLORIST_LOW_SENDING;


PROCEDURE GET_FLORIST_RECIPROCITY
(
IN_CALC_DATE                    IN FLORIST_WEIGHT_CALC_HISTORY.CALC_DATE%TYPE,
IN_DATE_RANGE_START             IN DATE,
IN_DATE_RANGE_END               IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the reciprocity values
        in the given date range for all florists that are currently having
        the weight calculations.

Input:
        calc_date               date
        date range start        date
        date range end          date

Output:
        result set containing parent florist id, calc date, total cleared orders,
        reciprocity ratio, reciprocity weight
-----------------------------------------------------------------------------*/

v_calc_date             DATE := to_date (to_char (in_calc_date, 'MM/YYYY'), 'MM/YYYY');
v_date_range_start      DATE := to_date (to_char (in_date_range_start, 'MM/YYYY'), 'MM/YYYY');
v_date_range_end        DATE := to_date (to_char (in_date_range_end, 'MM/YYYY'), 'MM/YYYY');

BEGIN

OPEN OUT_CUR FOR
        SELECT  a.parent_florist_id,
                a.calc_date,
                a.total_cleared_orders,
                a.reciprocity_ratio,
                a.reciprocity_weight,
                a.orders_sent_from_ftd
        FROM    florist_weight_calc_history a
        JOIN    florist_weight_calc_history b
        ON      a.parent_florist_id = b.parent_florist_id
        AND     a.calc_date BETWEEN v_date_range_start AND v_date_range_end
        AND     b.calc_date = v_calc_date
        ORDER BY a.parent_florist_id, a.calc_date;

END GET_FLORIST_RECIPROCITY;


PROCEDURE BUILD_FLORIST_WEIGHT_AUDIT
(
IN_CALC_DATE                    IN FLORIST_WEIGHT_CALC_HISTORY.CALC_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for building the weight audit report and
        storing the data into the audit report tables.

Input:
         calc_date               date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

01/05 creyes - removed the linking check because the florist are linked by the
first 6 digits of the florist id (florist id minus the suffix).
-----------------------------------------------------------------------------*/
TYPE varchar_tab_typ IS TABLE OF varchar2(9) INDEX BY PLS_INTEGER;

v_report_threshold    varchar2(2000) := nvl(frp.misc_pkg.get_global_parm_value ('WEIGHT AUDIT REPORT', 'THRESHOLD'),'4');
v_report_threshold_num NUMBER;

v_calc_date             DATE := to_date (to_char (in_calc_date, 'MM/YYYY'), 'MM/YYYY');

v_report_id             AUDIT_HOME.AUDIT_REPORT_HEADER.REPORT_ID%TYPE;
v_row_sequence          AUDIT_HOME.AUDIT_REPORT_DETAIL.ROW_SEQUENCE%TYPE := 1;
v_florist_tab           varchar_tab_typ;
v_value_1               varchar2(100);
v_value_2               varchar2(100);
v_value_3               varchar2(100);
v_value_4               varchar2(100);
v_value_5               varchar2(100);
v_value_6               varchar2(100);
v_value_7               varchar2(100);
v_value_8               varchar2(100);
v_value_9               varchar2(100);
v_value_10              varchar2(100);

CURSOR florist_cur(threshold NUMBER) IS
        SELECT  f.florist_id field_name,
                f.florist_name value_1,
                prev.base_weight value_2,
                prev.low_sending_threshold_weight value_3,
                prev.reciprocity_weight value_4,
                prev.final_weight value_5,
                curr.base_weight value_6,
                curr.low_sending_threshold_weight value_7,
                curr.reciprocity_weight value_8,
                curr.final_weight value_9,
                curr.grocery_store_flag value_10
        FROM    florist_master f
        JOIN    florist_weight_calc_history prev
        ON      f.florist_id = prev.parent_florist_id
        AND     prev.calc_date =
                (
                        SELECT  MAX (calc_date)
                        FROM    florist_weight_calc_history
                        WHERE   calc_date < v_calc_date
                )
        JOIN    florist_weight_calc_history curr
        ON      f.florist_id = curr.parent_florist_id
        AND     curr.calc_date = v_calc_date
        WHERE   abs(prev.final_weight - curr.final_weight) >= threshold
        ORDER BY f.florist_id;

v_florist_row                   florist_cur%rowtype;

CURSOR active_cur IS
        SELECT  f.florist_id value_1
        FROM    florist_master f
        WHERE   status <> 'Inactive'
        AND     florist_id NOT IN
              (
                SELECT  f.florist_id
                FROM    florist_master f
                JOIN    florist_weight_calc_history c
                ON      SUBSTR(f.florist_id, 1, 7) = substr(c.parent_florist_id, 1, 7)
                AND     c.calc_date = v_calc_date
        )
        ORDER BY f.florist_id;

BEGIN


v_report_threshold_num := to_number(v_report_threshold);

AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_HEADER
(
        'Weight Audit',
        'Weight Audit Report',
        'New',
        user,
        v_report_id,
        out_status,
        out_message
);

-- insert the audit report weight differences
IF out_status = 'Y' THEN
        FOR v_florist_row in florist_cur(v_report_threshold_num) LOOP

                AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_DETAIL
                (
                        v_report_id,
                        v_florist_row.field_name,
                        v_row_sequence,
                        null,
                        to_char(v_florist_row.value_1),
                        to_char(v_florist_row.value_2),
                        to_char(v_florist_row.value_3),
                        to_char(v_florist_row.value_4),
                        to_char(v_florist_row.value_5),
                        to_char(v_florist_row.value_6),
                        to_char(v_florist_row.value_7),
                        to_char(v_florist_row.value_8),
                        to_char(v_florist_row.value_9),
                        to_char(v_florist_row.value_10),
                        out_status,
                        out_message
                );

                IF out_status = 'N' THEN
                        exit;
                ELSE
                        v_row_sequence := v_row_sequence + 1;
                END IF;
        END LOOP;
END IF;

-- report active florists with no revenue
OPEN active_cur;
FETCH active_cur BULK COLLECT INTO v_florist_tab;
CLOSE active_cur;

IF v_florist_tab.count > 0 THEN
        -- insert the error description record
        AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_DETAIL
        (
                v_report_id,
                null,
                v_row_sequence,
                null,
                'Active Florists With No Revenue:',
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                out_status,
                out_message
        );

        v_row_sequence := v_row_sequence + 1;

        IF out_status = 'Y' THEN
                FOR x IN 1..v_florist_tab.count LOOP
                        CASE mod(x, 10)
                        WHEN 1 THEN
                                v_value_1 := v_florist_tab(x);
                        WHEN 2 THEN
                                v_value_2 := v_florist_tab(x);
                        WHEN 3 THEN
                                v_value_3 := v_florist_tab(x);
                        WHEN 4 THEN
                                v_value_4 := v_florist_tab(x);
                        WHEN 5 THEN
                                v_value_5 := v_florist_tab(x);
                        WHEN 6 THEN
                                v_value_6 := v_florist_tab(x);
                        WHEN 7 THEN
                                v_value_7 := v_florist_tab(x);
                        WHEN 8 THEN
                                v_value_8 := v_florist_tab(x);
                        WHEN 9 THEN
                                v_value_9 := v_florist_tab(x);
                        WHEN 0 THEN
                                v_value_10 := v_florist_tab(x);
                        END CASE;

                        IF mod (x, 10) = 0 OR
                           x = v_florist_tab.count THEN
                                -- insert the florists with linking errors
                                AUDIT_HOME.AUDIT_PKG.INSERT_AUDIT_REPORT_DETAIL
                                (
                                        v_report_id,
                                        null,
                                        v_row_sequence,
                                        null,
                                        v_value_1,
                                        v_value_2,
                                        v_value_3,
                                        v_value_4,
                                        v_value_5,
                                        v_value_6,
                                        v_value_7,
                                        v_value_8,
                                        v_value_9,
                                        v_value_10,
                                        out_status,
                                        out_message
                                );

                                IF out_status = 'N' THEN
                                        exit;
                                ELSE
                                        v_row_sequence := v_row_sequence + 1;
                                        v_value_1 := null;
                                        v_value_2 := null;
                                        v_value_3 := null;
                                        v_value_4 := null;
                                        v_value_5 := null;
                                        v_value_6 := null;
                                        v_value_7 := null;
                                        v_value_8 := null;
                                        v_value_9 := null;
                                        v_value_10 := null;
                                END IF;
                        END IF;
                END LOOP;
        END IF;
END IF;

END BUILD_FLORIST_WEIGHT_AUDIT;

PROCEDURE VIEW_FLORIST_WEIGHT_RPT_NAMES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the names of the florist
        weight audit reports that can be viewed by the user

Input:
        none

Output:
        cursor containing weight audit report names

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                to_char (creation_date, 'mm/dd/yyyy') || ' ' ||
                name || ' - ' ||
                status ||
                decode (status, 'New', null, ' - ' || to_char (updated_date, 'mm/dd/yyyy')) as audit_report,
                report_id
        FROM    audit_home.audit_report_header
        WHERE   type = 'Weight Audit'
        AND     to_char (creation_date, 'yyyymm') >= to_char (add_months (sysdate, -3), 'yyyymm')
        ORDER BY decode (status, 'New', 'A', 'B'), creation_date DESC;

END VIEW_FLORIST_WEIGHT_RPT_NAMES;

PROCEDURE LOAD_FLORIST_WEIGHT_DATA
(
IN_CALC_DATE                    IN FLORIST_WEIGHT_CALC_HISTORY.CALC_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the florist weight
        data from the staging table to the production table.

Input:
        calc_date               date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

TYPE varchar_tab_typ IS TABLE OF varchar2(100) INDEX BY PLS_INTEGER;
TYPE number_tab_typ IS TABLE OF number INDEX BY PLS_INTEGER;

v_calc_date                     DATE := to_date (to_char (in_calc_date, 'MM/YYYY'), 'MM/YYYY');

CURSOR weight_cur IS
        SELECT  distinct
                f.florist_id,
                f.florist_weight old_final_weight,
                f.initial_weight old_initial_weight,
                n.final_weight new_final_weight,
                n.base_weight new_initial_weight,
                o.final_weight override_final_weight,
                o.base_weight override_initial_weight,
                to_char (o.override_start_date, 'mm/dd/yyyy'),
                to_char (o.override_end_date, 'mm/dd/yyyy')
        FROM    florist_master f
        JOIN    florist_weight_calc_history n
        ON      SUBSTR(f.florist_id, 1, 7) = substr(n.parent_florist_id, 1, 7)
        AND     n.calc_date = v_calc_date
        LEFT OUTER JOIN florist_weight_override o
        ON      n.parent_florist_id = o.parent_florist_id
        AND     o.override_start_date <= v_calc_date
        AND     o.override_end_date >= v_calc_date;

v_florist_id_tab                varchar_tab_typ;
v_old_final_weight_tab          number_tab_typ;
v_old_initial_weight_tab        number_tab_typ;
v_new_final_weight_tab          number_tab_typ;
v_new_initial_weight_tab        number_tab_typ;
v_override_final_weight_tab     number_tab_typ;
v_override_initial_weight_tab   number_tab_typ;
v_override_start_tab            varchar_tab_typ;
v_override_end_tab              varchar_tab_typ;
v_comment                       varchar2(4000);

BEGIN

OPEN weight_cur;
FETCH weight_cur BULK COLLECT INTO
        v_florist_id_tab,
        v_old_final_weight_tab,
        v_old_initial_weight_tab,
        v_new_final_weight_tab,
        v_new_initial_weight_tab,
        v_override_final_weight_tab,
        v_override_initial_weight_tab,
        v_override_start_tab,
        v_override_end_tab;
CLOSE weight_cur;

FOR x in 1..v_florist_id_tab.count LOOP
-- update florist_master with the new computed weights
-- if an override weight exists, update the record using the override
        UPDATE  florist_master
        SET     florist_weight = coalesce (v_override_final_weight_tab (x), v_new_final_weight_tab (x)),
                initial_weight = v_new_initial_weight_tab(x)
        WHERE   florist_id = v_florist_id_tab (x);

-- update the florist weight history with the old values
        INSERT INTO florist_weight_history
        (
                florist_id,
                history_date,
                florist_weight,
                initial_weight
        )
        VALUES
        (
                v_florist_id_tab (x),
                sysdate,
                coalesce (v_override_final_weight_tab (x), v_new_final_weight_tab (x)),
                v_new_initial_weight_tab (x)
        );


-- create the comments for each florist noting if the override weight was used

        IF v_override_final_weight_tab (x) IS NULL THEN
                v_comment := 'Florist weight ' || to_char (v_old_final_weight_tab (x)) || ' was changed to ' || to_char (v_new_final_weight_tab (x));
        ELSE
                v_comment := 'Florist weight ' || to_char (v_old_final_weight_tab (x)) || ' was changed to ' || to_char (v_override_final_weight_tab (x)) || ' from period ' || v_override_start_tab (x) || ' to ' || v_override_end_tab (x) || ' due to RWD model change ';
        END IF;

        -- update the florist comments with the weight changes
        INSERT INTO florist_comments
        (
                florist_id,
                comment_date,
                comment_type,
                florist_comment,
                created_date,
                created_by
        )
        VALUES
        (
                v_florist_id_tab (x),
                sysdate,
                'Weight',
                v_comment,
                sysdate,
                user
        );

END LOOP;

OUT_STATUS := 'Y';
EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN LOAD_FLORIST_WEIGHT_DATA [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOAD_FLORIST_WEIGHT_DATA;


PROCEDURE WEIGHT_CALC_CONVERSION
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is the final step in the data conversion for the weight
        calc history and the weight override tables.  These tables
        should be first loaded using the data in the Access database which
        was created from importing the Excel files containing the revenue
        history and the list of florists from the HP that should have the
        weight override.
-----------------------------------------------------------------------------*/

v_rowcount              number;
BEGIN

update ftd_apps.florist_weight_calc_history
set orders_sent_from_ftd = ftd_apps.florist_weight_calc_pkg.GET_TOTAL_SENT_FROM_FTD(parent_florist_id, calc_date)
where calc_date >= to_date ('07/04', 'mm/yy');

v_rowcount := sql%rowcount;
dbms_output.put_line ('update orders sent from ftd::' || to_char (v_rowcount));
commit;


delete from ftd_apps.florist_weight_override o
where not exists
(
        select  1
        from    ftd_apps.florist_master m
        where   m.florist_id = o.parent_florist_id
        and     m.record_type = 'R'
);

v_rowcount := sql%rowcount;
dbms_output.put_line ('delete non R florists from override::' || to_char (v_rowcount));
commit;

update ftd_apps.florist_weight_override o
set     override_start_date = to_date ('01/05', 'mm/yy'),
        override_end_date = to_date ('03/05', 'mm/yy'),
        final_weight = 12;

v_rowcount := sql%rowcount;
dbms_output.put_line ('update override dates and weights::' || to_char (v_rowcount));
commit;

END WEIGHT_CALC_CONVERSION;

FUNCTION CALC_FLORIST_SCORE
(
 IN_FLORIST_ID        IN florist_master.florist_id%TYPE,
 IN_ZIP_CODE          IN florist_master.zip_code%TYPE,
 IN_SOURCE_CODE       IN preferred_florist.source_code%TYPE,
 IN_CODIFICATION_ID   IN florist_codifications.codification_id%TYPE,
 IN_DELIVERY_DATE     IN DATE,  
 IN_DELIVERY_DATE_END IN DATE
)
 RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure will calculate a score for florist that has been pre-
        determined to support a product and diverty city/zip code.

Input:
        florist id, zip code, source code, codification id, Y/N flag (all florist)

Output:
        score - NUMBER

-----------------------------------------------------------------------------*/

v_status             florist_master.status%TYPE;
v_super_florist_flag florist_master.super_florist_flag%TYPE;
v_mercury_flag       florist_master.mercury_flag%TYPE;
v_source_code        preferred_florist.source_code%TYPE;
v_codification_id    florist_codifications.codification_id%TYPE;
v_block_type         florist_blocks.block_type%TYPE;
v_suspend_type       florist_suspends.suspend_type%TYPE;
v_priority           source_florist_priority.priority%TYPE;

v_score NUMBER := 0;

v_preferred_bit    NUMBER := 32;
v_zip_bit          NUMBER := 16;
v_mercury_bit      NUMBER := 8;
v_codification_bit NUMBER := 4;

--status_offsets
v_active_status NUMBER := 3;
v_suspended     NUMBER := 2;
v_block         NUMBER := -1000;

v_primary_backup_offset     NUMBER := 64;
v_primary_backup_multiplier NUMBER := 16;

-- Vars to keep track of the day range status
v_active_day     BOOLEAN DEFAULT FALSE;
v_block_day      BOOLEAN DEFAULT FALSE;
v_current_day    Date;
v_num_days       NUMBER;


BEGIN

  SELECT fm.mercury_flag,
         fm.super_florist_flag,
         fm.status,
         pf.source_code,
         fc.codification_id,
         fs.suspend_type,
         sfp.priority
    INTO v_mercury_flag,
         v_super_florist_flag,
         v_status,
         v_source_code,
         v_codification_id,
         v_suspend_type,
         v_priority
    FROM florist_master fm
         LEFT OUTER JOIN florist_suspends fs
                      ON fs.florist_id = fm.florist_id
         LEFT OUTER JOIN florist_codifications fc
                      ON fc.florist_id = fm.florist_id
                     AND fc.codification_id = in_codification_id
         LEFT OUTER JOIN preferred_florist pf
                      ON pf.florist_id  = fm.florist_id
                     AND pf.source_code = in_source_code
         LEFT OUTER JOIN source_florist_priority sfp
                      ON sfp.florist_id  = fm.florist_id
                     AND sfp.source_code = in_source_code
   WHERE fm.florist_id = in_florist_id;




  IF v_mercury_flag = 'M' THEN
     v_score := v_score + v_mercury_bit;
  END IF;

  --if a matching codification id was not returned then it
  -- means that the florist is not codified for this product
  IF v_codification_id IS NOT NULL THEN
     v_score := v_score + v_codification_bit;
  END IF;


  IF v_status = 'Active' THEN
     v_score := v_score + v_active_status;
     v_active_day := true;
  END IF;

  IF v_super_florist_flag = 'Y' THEN
    IF v_status = 'GoTo Suspend' THEN
       v_score := v_score + v_suspended;
    END IF;
  ELSE
    IF v_status = 'Mercury Suspend' THEN
       v_score := v_score + v_suspended;
    END IF;
  END IF;

  IF v_status = 'Blocked' THEN
    -- Apply the settings from the best day in the range.  Active, Soft, Hard, nothing in that order.

    IF in_delivery_date_end is null THEN
      v_num_days := 0;
    ELSE
      v_num_days := trunc(in_delivery_date_end) - trunc(in_delivery_date);
    END IF;

    FOR i IN 0..v_num_days LOOP
      v_current_day := in_delivery_date + i;
  
      BEGIN 
        SELECT block_type 
          into v_block_type
          from ftd_apps.florist_blocks
         where 1=1
           and florist_id = in_florist_id
           and block_start_date <= v_current_day
           and (block_end_date >= v_current_day OR block_end_date is null);
        EXCEPTION WHEN NO_DATA_FOUND THEN
          v_active_day := true;
      END;    -- end exception block
      
      IF v_block_type is not null and v_block_type in ('S','H','O') THEN
        v_block_day := true;
      END IF;

    END LOOP;
  
    IF v_active_day THEN
       v_score := v_score + v_active_status;
    ELSE
      IF v_block_day THEN
        v_score := v_score + v_block;
      END IF;
    END IF;
  END IF;    

  --if a null zip code passed in means that the florist is being
  -- by the delivery city.
  IF in_zip_code IS NOT NULL THEN
     v_score := v_score + v_zip_bit;

     --if a matching source code was not returned then it
     -- means that the florist is not a preferred florist
     IF v_source_code IS NOT NULL THEN
        -- Only mark preferred if they are active for that day
        IF v_active_day AND (v_suspend_type is null OR (v_super_florist_flag = 'Y' and v_status = 'Mercury Suspend')) THEN
           v_score := v_score + v_preferred_bit;
        END IF;
     END IF;
  END IF;

  IF v_priority > 0 THEN
    IF v_active_day AND (v_suspend_type is null OR (v_super_florist_flag = 'Y' and v_status = 'Mercury Suspend')) THEN
      v_score := v_score + (v_primary_backup_offset + ( (20 - v_priority) * v_primary_backup_multiplier) );
    END IF;
  END IF;

  RETURN v_score;


END CALC_FLORIST_SCORE;


END;

/
