CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_EXCEPTIONS_LIST RETURN types.ref_cursor
 --==============================================================================
--
-- Name:    SP_GET_EXCEPTIONS_LIST
-- Type:    Function
-- Syntax:  SP_GET_EXCEPTIONS_LIST ( )
-- Returns: ref_cursor for
--          EXCEPTION_ID           VARCHAR2(1)
--          EXCEPTION_DESCRIPTION  VARCHAR2(25)
--
-- Description:   Queries the EXCEPTIONS table and returns a ref cursor for row.
--
--===========================================================

AS
    exceptions_cursor types.ref_cursor;
BEGIN
    OPEN exceptions_cursor FOR
        SELECT exception_id          as "exceptionId",
               exception_description as "exceptionDescription"
          FROM "EXCEPTIONS" ORDER BY exception_description;

    RETURN exceptions_cursor;
END
;
.
/
