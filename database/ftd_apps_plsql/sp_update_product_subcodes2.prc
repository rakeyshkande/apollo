CREATE OR REPLACE PROCEDURE FTD_APPS.SP_UPDATE_PRODUCT_SUBCODES2  (
 productId in varchar2,
 subCodeId in varchar2,
 subCodeDesc in varchar2,
 subCodePrice in number,
 subCodeHolidaySKU in varchar2,
 subCodeHolidayPrice in number,
 subCodeAvailable in varchar2,
 subCodeRefNumber in varchar2,
 subCodeDimWeight in VARCHAR2,
 subCodeDisplayOrder IN NUMBER,
 updatedBy in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_SUBCODES2
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_SUBCODES2 ( <see args above> )
--
-- Description:   Performs an INSERT/UPDATE attempt on PRODUCT_SUBCODES.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_SUBCODES
  INSERT INTO PRODUCT_SUBCODES
      ( PRODUCT_ID,
                PRODUCT_SUBCODE_ID,
                SUBCODE_DESCRIPTION,
                SUBCODE_PRICE,
                HOLIDAY_SKU,
                HOLIDAY_PRICE,
                ACTIVE_FLAG,
                SUBCODE_REFERENCE_NUMBER,
                DIM_WEIGHT,
                DISPLAY_ORDER,
                CREATED_ON, 
                CREATED_BY, 
                UPDATED_ON, 
                UPDATED_BY
          )
  VALUES
          ( productId,
           subCodeId,
         subCodeDesc,
         subCodePrice,
         subCodeHolidaySKU,
         subCodeHolidayPrice,
         subCodeAvailable,
         subCodeRefNumber,
         subCodeDimWeight,
         subCodeDisplayOrder,
         SYSDATE,
         updatedBy,
         SYSDATE,
         updatedBy
          );

  -- Insert a command to update PAS
  -- PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT',productId || ' YES');  
  -- Removed this because PDB does a global one for the product

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
             -- Update an existing row for this product id if found
    UPDATE PRODUCT_SUBCODES
      SET SUBCODE_DESCRIPTION = subCodeDesc,
                    SUBCODE_PRICE = subCodePrice,
                    HOLIDAY_SKU = subCodeHolidaySKU,
                    HOLIDAY_PRICE = subCodeHolidayPrice,
                    ACTIVE_FLAG = subCodeAvailable,
                    SUBCODE_REFERENCE_NUMBER = subCodeRefNumber,
                    DIM_WEIGHT = subCodeDimWeight,
                    DISPLAY_ORDER = subCodeDisplayOrder,
                    UPDATED_ON = SYSDATE, 
	            UPDATED_BY = updatedBy
      WHERE PRODUCT_ID = productId AND PRODUCT_SUBCODE_ID = subCodeId;

    -- Insert a command to update PAS
    -- PAS.POST_PAS_COMMAND('MODIFIED_PRODUCT',productId || ' YES');  
    -- Removed this because PDB does a global one for the product
end;
.
/
