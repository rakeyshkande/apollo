CREATE OR REPLACE
FUNCTION ftd_apps.OE_VALIDATE_SOURCE_CODE_SFMB (
    inSourceCode          IN VARCHAR2
 )
 RETURN types.ref_cursor
 AUTHID CURRENT_USER
--==============================================================================
--
-- Name:    OE_VALIDATE_SOURCE_CODE
-- Type:    Function
-- Syntax:  OE_VALIDATE_SOURCE_CODE ( inSourceCode IN VARCHAR2, inDnisScriptCode IN VARCHAR2  )
-- Returns: ref_cursor for
--          SOURCE_CODE                 VARCHAR2(10)
--          jcPenneyFlag                VARCHAR2(1)
--          EXPIRED_FLAG                VARCHAR2(1)
--
-- Description:   Queries the SOURCE table for the given source code and returns
--                the source code if found.  Used for validation.
--
--==============================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN

    OPEN cur_cursor FOR
      SELECT SOURCE_CODE as "sourceCode",
            NVL(JCPENNEY_FLAG, 'N') as "jcPenneyFlag",
                              DECODE(COMPANY_ID, 'SFMB', 'Y', 'N') as "sfmbFlag",
                              DECODE(COMPANY_ID, 'GIFT', 'Y', 'N') as "giftFlag",
            DECODE(END_DATE, NULL, 'N',
            DECODE(SIGN(END_DATE - TRUNC(SYSDATE)), 0, 'N', 1, 'N', 'Y')) as "expiredFlag"
      FROM SOURCE
      WHERE SOURCE_CODE = inSourceCode;

    RETURN cur_cursor;
END;
.
/
