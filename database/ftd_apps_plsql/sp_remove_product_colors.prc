CREATE OR REPLACE
PROCEDURE ftd_apps.SP_REMOVE_PRODUCT_COLORS (
  productId in varchar2
)
as
--==============================================================================
--
-- Name:    SP_REMOVE_PRODUCT_COLORS
-- Type:    Procedure
-- Syntax:  SP_REMOVE_PRODUCT_COLORS ( productId in varchar2 )
--
-- Description:   Deletes a row from PRODUCT_COLORS by PRODUCT_ID.
--
--==============================================================================
begin

 delete FROM PRODUCT_COLORS
  where PRODUCT_ID = productId;

end
;
.
/
