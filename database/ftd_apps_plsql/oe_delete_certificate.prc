CREATE OR REPLACE
PROCEDURE ftd_apps.OE_DELETE_CERTIFICATE (
  ftdGuid in varchar2
)
as

begin

 delete FROM SECURITY_CERT
  where FTD_GUID = ftdGuid;

  COMMIT;

end
;
.
/
