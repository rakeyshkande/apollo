CREATE OR REPLACE FUNCTION FTD_APPS.OE_PRODUCT_DETAILS_657  (
IN_PRODUCT_ID             IN VARCHAR2,
IN_SOURCE_CODE            IN VARCHAR2,
IN_DOMESTIC_INTL_FLAG     IN VARCHAR2,
IN_POSTAL_CODE            IN VARCHAR2,
IN_COUNTRY_ID             IN VARCHAR2,
IN_DELIVERY_DATE          IN VARCHAR2
)
  RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_PRODUCT_DETAILS_657
-- Type:    Function
-- Syntax:  OE_PRODUCT_DETAILS_657(IN_PRODUCT_ID IN VARCHAR2,
--                             IN_SOURCE_CODE IN VARCHAR2,
--                             IN_DOMESTIC_INTL_FLAG IN VARCHAR2,
--                             IN_COUNTRY_ID IN VARCHAR2,
--                             IN_DELIVERY_DATE)
-- Returns: ref_cursor for
--          PRODUCT_ID             VARCHAR2(10)
--          PRODUCT_NAME           VARCHAR2(25)
--          NOVATOR_NAME           VARCHAR2(100)
--          DELIVERY_TYPE          VARCHAR2(1)
--          CATEGORY               VARCHAR2(5)
--          productType            VARCHAR2(25)
--          productSubType         VARCHAR2(25)
--          zipGnaddFlag           VARCHAR2(1)
--          zipFloralFlag          VARCHAR2(1)
--          zipGotoFloristFlag     VARCHAR2(1)
--          zipSundayFlag          VARCHAR2(1)
--          timeZone               VARCHAR2(1)
--          stateId                VARCHAR(2)
--          addonDays              NUMBER,
--          egiftFlag              VARCHAR2(1)
--          addonBalloonsFlag      VARCHAR2(1)
--          addonBearsFlag         VARCHAR2(1)
--          addonCardsFlag         VARCHAR2(1)
--          addonChocolateFlag     VARCHAR2(1)
--          addonFuneralFlag       VARCHAR2(1)
--          discountType           VARCHAR2(1)
--          STANDARD_PRICE         NUMBER(8.2)
--          standardDiscountAmt    NUMBER(7.2)
--          DELUXE_PRICE           NUMBER(8.2)
--          deluxeDiscountAmt      NUMBER(7.2)
--          PREMIUM_PRICE          NUMBER(8.2)
--          premiumDiscountAmt     NUMBER(7.2)
--          VARIABLE_PRICE_MAX     NUMBER(8.2)
--          LONG_DESCRIPTION       VARCHAR2(540)
--          DISCOUNT_ALLOWED_FLAG  VARCHAR2(1)
--          DELIVERY_INCLUDED_FLAG VARCHAR2(1)
--          SERVICE_FEE_FLAG       VARCHAR2(1)
--          ARRANGEMENT_SIZE       VARCHAR2(25)
--          PRICE_RANK_1           VARCHAR2(1)
--          PRICE_RANK_2           VARCHAR2(1)
--          PRICE_RANK_3           VARCHAR2(1)
--          exceptionCode          VARCHAR2(1)
--          exceptionStartDate     VARCHAR2(10)
--          exceptionEndDate       VARCHAR2(10)
--          exceptionMessage       VARCHAR2(280)
--          jcpCategory            VARCHAR2(1)
--          mondayFlag             VARCHAR2(1)
--          tuesdayFlag            VARCHAR2(1)
--          wednesdayFlag          VARCHAR2(1)
--          thursdayFlag           VARCHAR2(1)
--          fridayFlag             VARCHAR2(1)
--          saturdayFlag           VARCHAR2(1)
--          sundayFlag             VARCHAR2(1)
--          exSundayFlag           VARCHAR2(1)
--          exMondayFlag           VARCHAR2(1)
--          exTuesdayFlag          VARCHAR2(1)
--          exWednesdayFlag        VARCHAR2(1)
--          exThursdayFlag         VARCHAR2(1)
--          exFridayFlag           VARCHAR2(1)
--          exSaturdayFlag         VARCHAR2(1)
--          mondayDelFCFlag        VARCHAR2(1)
--          twoDaySatFCFlag        VARCHAR2(1)
--          vendorID               VARCHAR2(2)
--          SHIP_METHOD_CARRIER    VARCHAR2(1)
--          SHIP_METHOD_FLORIST    VARCHAR2(1)
--          VARIABLE_PRICE_FLAG    VARCHAR2(1)
--          SERVICE_CHARGE         NUMBER(8.2)
--          CARRIER                VARCHAR2(25)
--          color1                 VARCHAR2(15)
--          color2                 VARCHAR2(15)
--          color3                 VARCHAR2(15)
--          color4                 VARCHAR2(15)
--          color5                 VARCHAR2(15)
--          color6                 VARCHAR2(15)
--          color7                 VARCHAR2(15)
--          color8                 VARCHAR2(15)
--          vendorShipBlockStart1  VARCHAR2(10)
--          vendorShipBlockEnd1    VARCHAR2(10)
--          vendorShipBlockStart2  VARCHAR2(10)
--          vendorShipBlockEnd2    VARCHAR2(10)
--          vendorShipBlockStart3  VARCHAR2(10)
--          vendorShipBlockEnd3    VARCHAR2(10)
--          vendorShipBlockStart4  VARCHAR2(10)
--          vendorShipBlockEnd4    VARCHAR2(10)
--          vendorShipBlockStart5  VARCHAR2(10)
--          vendorShipBlockEnd5    VARCHAR2(10)
--          vendorShipBlockStart6  VARCHAR2(10)
--          vendorShipBlockEnd6    VARCHAR2(10)
--          vendorDelivBlockStart1 VARCHAR2(10)
--          vendorDelivBlockEnd1   VARCHAR2(10)
--          vendorDelivBlockStart2 VARCHAR2(10)
--          vendorDelivBlockEnd2   VARCHAR2(10)
--          vendorDelivBlockStart3 VARCHAR2(10)
--          vendorDelivBlockEnd3   VARCHAR2(10)
--          vendorDelivBlockStart4 VARCHAR2(10)
--          vendorDelivBlockEnd4   VARCHAR2(10)
--          vendorDelivBlockStart5 VARCHAR2(10)
--          vendorDelivBlockEnd5   VARCHAR2(10)
--          vendorDelivBlockStart6 VARCHAR2(10)
--          vendorDelivBlockEnd6   VARCHAR2(10)
--          largeImage             VARCHAR2(1)
--          weboeBlocked           VARCHAR2(1)
--          expressShippingOnly    VARCHAR2(1)
--          over21                 VARCHAR2(1)
--          shippingSystem        VARCHAR2(10)
--          personalization       VARCHAR2(10)
--          personalGreetingFlag   VARCHAR2(1)
--          allowFreeShippingFlag  VARCHAR2(1)
--
-- Description:   Queries PRODUCT_MASTER by PR.PRODUCT_ID
--                returning a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor types.ref_cursor;

    v_sourceCode               SOURCE.SOURCE_CODE%TYPE := UPPER(IN_SOURCE_CODE);
    v_pricingCode              SOURCE.PRICING_CODE%TYPE := NULL;
    v_subCode                  PRODUCT_SUBCODES.PRODUCT_SUBCODE_ID%TYPE := NULL;
    v_alphaChars               VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    v_alphaSub                 VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    v_numChars                 VARCHAR2(10) := '0123456789';
    v_numSub                   VARCHAR2(10) := '##########';
    v_cszZip                   VARCHAR2(6);
    v_cleanZip                 VARCHAR2(6) := OE_CLEANUP_ALPHANUM_STRING(UPPER(IN_POSTAL_CODE), 'Y');
    v_counter                  INTEGER;
    v_colorCount               INTEGER;
    v_zipDownFlag              VARCHAR2(6);
    v_codifiedAvailable        VARCHAR2(1);

    out_productID              PRODUCT_MASTER.PRODUCT_ID%TYPE := UPPER(IN_PRODUCT_ID);
    out_novatorID              PRODUCT_MASTER.NOVATOR_ID%TYPE := NULL;
    out_productName            PRODUCT_MASTER.PRODUCT_NAME%TYPE := NULL;
    out_novatorName            PRODUCT_MASTER.NOVATOR_NAME%TYPE := NULL;
    out_deliveryType           PRODUCT_MASTER.DELIVERY_TYPE%TYPE := NULL;
    out_productCategory        PRODUCT_MASTER.CATEGORY%TYPE := NULL;
    out_productType            PRODUCT_MASTER.PRODUCT_TYPE%TYPE := NULL;
    out_productSubType         PRODUCT_MASTER.PRODUCT_SUB_TYPE%TYPE := NULL;
    out_zipGnaddFlag           CSZ_AVAIL.GNADD_FLAG%TYPE := NULL;
    out_zipFloralFlag          VARCHAR2(1) := 'Y';
    out_zipGotoFloristFlag     VARCHAR2(1) := 'Y';
    out_zipSundayFlag          VARCHAR2(1) := NULL;
    out_timeZone               STATE_MASTER.TIME_ZONE%TYPE := NULL;
    out_stateId                ZIP_CODE.STATE_ID%TYPE := NULL;
    out_addonDays              NUMBER;
    out_egiftFlag              PRODUCT_MASTER.EGIFT_FLAG%TYPE := NULL;
    out_addonBalloonsFlag      PRODUCT_MASTER.ADD_ON_BALLOONS_FLAG%TYPE := NULL;
    out_addonBearsFlag         PRODUCT_MASTER.ADD_ON_BEARS_FLAG%TYPE := NULL;
    out_addonCardsFlag         PRODUCT_MASTER.ADD_ON_CARDS_FLAG%TYPE := NULL;
    out_addonChocolateFlag     PRODUCT_MASTER.ADD_ON_CHOCOLATE_FLAG%TYPE := NULL;
    out_addonFuneralFlag       PRODUCT_MASTER.ADD_ON_FUNERAL_FLAG%TYPE := NULL;
    out_discountType           PRICE_HEADER_DETAILS.DISCOUNT_TYPE%TYPE := NULL;
    out_standardPrice          PRODUCT_MASTER.STANDARD_PRICE%TYPE := NULL;
    out_standardDiscountAmt    PRICE_HEADER_DETAILS.DISCOUNT_AMT%TYPE := NULL;
    out_deluxePrice            PRODUCT_MASTER.DELUXE_PRICE%TYPE := NULL;
    out_deluxeDiscountAmt      PRICE_HEADER_DETAILS.DISCOUNT_AMT%TYPE := NULL;
    out_premiumPrice           PRODUCT_MASTER.PREMIUM_PRICE%TYPE := NULL;
    out_premiumDiscountAmt     PRICE_HEADER_DETAILS.DISCOUNT_AMT%TYPE := NULL;
    out_variablePriceMax       PRODUCT_MASTER.VARIABLE_PRICE_MAX%TYPE := NULL;
    out_longDescription        PRODUCT_MASTER.LONG_DESCRIPTION%TYPE := NULL;
    out_discountFlag           PRODUCT_MASTER.DISCOUNT_ALLOWED_FLAG%TYPE := NULL;
    out_deliveryFlag           PRODUCT_MASTER.DELIVERY_INCLUDED_FLAG%TYPE := NULL;
    out_serviceFeeFlag         PRODUCT_MASTER.SERVICE_FEE_FLAG%TYPE := NULL;
    out_arrangementSize        PRODUCT_MASTER.ARRANGEMENT_SIZE%TYPE := NULL;
    out_priceRank1             PRODUCT_MASTER.PRICE_RANK_1%TYPE := NULL;
    out_priceRank2             PRODUCT_MASTER.PRICE_RANK_2%TYPE := NULL;
    out_priceRank3             PRODUCT_MASTER.PRICE_RANK_3%TYPE := NULL;
    out_exceptionCode          PRODUCT_MASTER.EXCEPTION_CODE%TYPE := NULL;
    out_exceptionStartDate     VARCHAR2(10); --MM/DD/YYYY
    out_exceptionEndDate       VARCHAR2(10); --MM/DD/YYYY
    out_jcpCategory            PRODUCT_MASTER.JCP_CATEGORY%TYPE := NULL;
    out_mondayFlag             PRODUCT_SHIP_DATES.MONDAY_FLAG%TYPE := NULL;
    out_tuesdayFlag            PRODUCT_SHIP_DATES.TUESDAY_FLAG%TYPE := NULL;
    out_wednesdayFlag          PRODUCT_SHIP_DATES.WEDNESDAY_FLAG%TYPE := NULL;
    out_thursdayFlag           PRODUCT_SHIP_DATES.THURSDAY_FLAG%TYPE := NULL;
    out_fridayFlag             PRODUCT_SHIP_DATES.FRIDAY_FLAG%TYPE := NULL;
    out_saturdayFlag           PRODUCT_SHIP_DATES.SATURDAY_FLAG%TYPE := NULL;
    out_sundayFlag             PRODUCT_SHIP_DATES.SUNDAY_FLAG%TYPE := NULL;
    out_exSundayFlag           VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.SUN%TYPE
    out_exMondayFlag           VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.MON%TYPE
    out_exTuesdayFlag          VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.TUE%TYPE
    out_exWednesdayFlag        VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.WED%TYPE
    out_exThursdayFlag         VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.THU%TYPE
    out_exFridayFlag           VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.FRI%TYPE
    out_exSaturdayFlag         VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_EXCLUDED_STATES.SAT%TYPE
    out_mondayDelFCFlag        VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_MASTER.MONDAY_DELIVERY_FRESHCUT%TYPE
    out_twoDaySatFCFlag        VARCHAR(1) := NULL; --jdbc driver returns char(32) if you use PRODUCT_MASTER.TWO_DAY_SAT_FRESHCUT%TYPE
    out_vendorID               PRODUCT_MASTER.VENDOR_ID%TYPE := NULL;
    out_exceptionMessage       PRODUCT_MASTER.EXCEPTION_MESSAGE%TYPE := NULL;
    out_shipMethodCarrier      PRODUCT_MASTER.SHIP_METHOD_CARRIER%TYPE := NULL;
    out_shipMethodFlorist      PRODUCT_MASTER.SHIP_METHOD_FLORIST%TYPE := NULL;
    out_variablePriceFlag      PRODUCT_MASTER.VARIABLE_PRICE_FLAG%TYPE := NULL;
    out_shippingKey            PRODUCT_MASTER.SHIPPING_KEY%TYPE := NULL;
    out_secondChoice           PRODUCT_SECOND_CHOICE.OE_DESCRIPTION1%TYPE := NULL;
    out_secondChoiceCode       PRODUCT_MASTER.SECOND_CHOICE_CODE%TYPE := NULL;
    out_status                 PRODUCT_MASTER.STATUS%TYPE := NULL;
    out_egift                  PRODUCT_MASTER.EGIFT_FLAG%TYPE := NULL;
    out_codifiedProduct        CODIFIED_PRODUCTS.CHECK_OE_FLAG%TYPE := NULL;
    out_codifiedAvailable      VARCHAR2(2) := NULL;
    out_codifiedSpecial        CODIFIED_PRODUCTS.CODIFIED_SPECIAL%TYPE := NULL;
    out_codifiedDeliverable    VARCHAR2(1);
    out_serviceCharge          VARCHAR2(16);
    out_carrier                SHIPPING_KEYS.SHIPPER%TYPE := NULL;
    out_color1                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color2                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color3                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color4                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color5                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color6                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color7                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_color8                 COLOR_MASTER.DESCRIPTION%TYPE := NULL;
    out_vendorShipBlockStart1  VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockEnd1    VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockStart2  VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockEnd2    VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockStart3  VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockEnd3    VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockStart4  VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockEnd4    VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockStart5  VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockEnd5    VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockStart6  VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorShipBlockEnd6    VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockStart1 VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockEnd1   VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockStart2 VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockEnd2   VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockStart3 VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockEnd3   VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockStart4 VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockEnd4   VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockStart5 VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockEnd5   VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockStart6 VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_vendorDelivBlockEnd6   VARCHAR2(10) := NULL; --MM/DD/YYYY
    out_largeImage             VARCHAR2(1) := 'Y'; --Always return Y
    out_weboeBlocked           PRODUCT_MASTER.WEBOE_BLOCKED%TYPE := NULL;
    out_expressShippingOnly    PRODUCT_MASTER.EXPRESS_SHIPPING_ONLY%TYPE := NULL;
    out_over21                 PRODUCT_MASTER.OVER_21%TYPE := NULL;
    out_shippingSystem         PRODUCT_MASTER.SHIPPING_SYSTEM%TYPE := NULL;
    out_personalization_template   PRODUCT_MASTER.PERSONALIZATION_TEMPLATE_ID%TYPE := NULL;
    out_personal_greeting_flag PRODUCT_MASTER.PERSONAL_GREETING_FLAG%TYPE := NULL;
    out_allow_free_shipping_flag PRODUCT_MASTER.ALLOW_FREE_SHIPPING_FLAG%TYPE := NULL;
BEGIN
    -- Determine if the product ID given is actually a subcode
    BEGIN
        SELECT PRODUCT_SUBCODE_ID, PRODUCT_ID
        INTO v_subCode, out_productID
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = out_productId;
    EXCEPTION WHEN OTHERS THEN
        BEGIN
          SELECT PRODUCT_ID
          INTO out_productID
          FROM PRODUCT_MASTER
          WHERE NOVATOR_ID = out_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT PRODUCT_ID
            INTO out_productID
            FROM PRODUCT_MASTER
            WHERE PRODUCT_ID = out_productID;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            NULL;
          END;
        END;
    END;

     -- Select the pricing code for this source code, for the below query
    BEGIN
        SELECT PRICING_CODE
        INTO v_pricingCode
        FROM SOURCE
        WHERE SOURCE_CODE = v_sourceCode;
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- If no zip provided, assume zip GNADD is off and floral available wherever this is going
    IF ( v_cleanZip IS NULL )
    THEN
        out_zipGnaddFlag := 'N';
        out_zipFloralFlag := 'Y';

    ELSE
        -- Determine if the input zip code is Canadian
        IF ( TRANSLATE(v_cleanZip, v_alphaChars || v_numChars, v_alphaSub || v_numSub) LIKE '@#@%' )
        THEN
            -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
            v_cszZip := SUBSTR(v_cleanZip,1,3);
        ELSE
            v_cszZip := v_cleanZip;
        END IF;

        -- Select the GNADD flag for this zip, if available
        BEGIN
            -- Assume NULL GNADD flag indicates GNADD is off
            SELECT NVL(GNADD_FLAG, 'N')
            INTO out_zipGnaddFlag
            FROM FTD_APPS.CSZ_AVAIL
            WHERE CSZ_ZIP_CODE = v_cszZip;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            -- If no row found on CSZ_AVAIL, then no floral for this zip
            out_zipFloralFlag := 'N';
            out_zipGnaddFlag := 'N';
        END;
    END IF;

    -- Select the time zone for this zip, if available
    BEGIN
       SELECT DISTINCT SM.TIME_ZONE, ZC.STATE_ID
        INTO out_timeZone, out_stateId
        FROM ZIP_CODE zc, STATE_MASTER sm
        WHERE zc.ZIP_CODE_ID = v_cleanZip
          AND ZC.STATE_ID = SM.STATE_MASTER_ID;
    EXCEPTION WHEN OTHERS THEN
        out_timeZone := '5';
    END;

    -- Select add on days for this country from country master
    BEGIN
        SELECT UNIQUE NVL(ADD_ON_DAYS, 1)
        INTO out_addonDays
        FROM COUNTRY_MASTER
        WHERE COUNTRY_ID = IN_COUNTRY_ID;
    EXCEPTION WHEN OTHERS THEN
        -- Default to 1 day if not known
        out_addonDays := 1;
    END;

    --Get basic product information
    BEGIN
      SELECT
      	pm.PRODUCT_ID,
      	pm.NOVATOR_ID,
      	pm.PRODUCT_NAME,
      	pm.NOVATOR_NAME,
      	pm.DELIVERY_TYPE,
      	pm.CATEGORY,
      	pm.PRODUCT_TYPE,
      	pm.PRODUCT_SUB_TYPE,
      	pm.EGIFT_FLAG,
      	pm.ADD_ON_BALLOONS_FLAG,
      	pm.ADD_ON_BEARS_FLAG,
      	pm.ADD_ON_CARDS_FLAG,
      	pm.ADD_ON_CHOCOLATE_FLAG,
      	pm.ADD_ON_FUNERAL_FLAG,
      	pm.STANDARD_PRICE,
      	pm.DELUXE_PRICE,
      	pm.PREMIUM_PRICE,
      	pm.VARIABLE_PRICE_MAX,
      	pm.LONG_DESCRIPTION,
      	pm.DISCOUNT_ALLOWED_FLAG,
      	pm.DELIVERY_INCLUDED_FLAG,
      	pm.SERVICE_FEE_FLAG,
      	pm.ARRANGEMENT_SIZE,
      	pm.PRICE_RANK_1,
      	pm.PRICE_RANK_2,
      	pm.PRICE_RANK_3,
      	pm.EXCEPTION_CODE,
      	to_char(pm.EXCEPTION_START_DATE, 'mm/dd/yyyy'),
        to_char(pm.EXCEPTION_END_DATE, 'mm/dd/yyyy'),
        pm.JCP_CATEGORY,
        pm.MONDAY_DELIVERY_FRESHCUT,
        pm.TWO_DAY_SAT_FRESHCUT,
        null,
        pm.EXCEPTION_MESSAGE,
        pm.SHIP_METHOD_CARRIER,
        pm.SHIP_METHOD_FLORIST,
        pm.VARIABLE_PRICE_FLAG,
        pm.SHIPPING_KEY,
        pm.SECOND_CHOICE_CODE,
        pm.EGIFT_FLAG,
        pm.STATUS,
        pm.WEBOE_BLOCKED,
        pm.EXPRESS_SHIPPING_ONLY,
        pm.OVER_21,
        PM.SHIPPING_SYSTEM,
        PM.PERSONALIZATION_TEMPLATE_ID,
        PM.PERSONAL_GREETING_FLAG,
        PM.ALLOW_FREE_SHIPPING_FLAG

      INTO

    	  out_productID,
        out_novatorID,
        out_productName,
        out_novatorName,
        out_deliveryType,
        out_productCategory,
        out_productType,
        out_productSubType,
        out_egiftFlag,
        out_addonBalloonsFlag,
        out_addonBearsFlag,
        out_addonCardsFlag,
        out_addonChocolateFlag,
        out_addonFuneralFlag,
        out_standardPrice,
        out_deluxePrice,
        out_premiumPrice,
        out_variablePriceMax,
        out_longDescription,
        out_discountFlag,
        out_deliveryFlag,
        out_serviceFeeFlag,
        out_arrangementSize,
        out_priceRank1,
        out_priceRank2,
        out_priceRank3,
        out_exceptionCode,
        out_exceptionStartDate,
        out_exceptionEndDate,
        out_jcpCategory,
        out_mondayDelFCFlag,
        out_twoDaySatFCFlag,
        out_vendorID,
        out_exceptionMessage,
        out_shipMethodCarrier,
        out_shipMethodFlorist,
        out_variablePriceFlag,
        out_shippingKey,
        out_secondChoiceCode,
        out_egift,
        out_status,
        out_weboeBlocked,
        out_expressShippingOnly,
        out_over21,
        out_shippingSystem,
        out_personalization_template,
        out_personal_greeting_flag,
        out_allow_free_shipping_flag

      FROM PRODUCT_MASTER pm,
           SOURCE sc,
           PRODUCT_COMPANY_XREF pcx
      WHERE pm.product_id = out_productId
        -- Use product_compnay_xref mapping table to find all products for this company
        and sc.source_code = v_sourceCode
        and sc.company_id = pcx.company_id
        and pcx.product_id = pm.product_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- create an empty cursor
      OPEN cur_cursor for select * from dual where dummy = 'Y';
      return cur_cursor;
    END;

    -- Determine if this product is excluded for delivery by state
    BEGIN
      SELECT pes.SUN, pes.MON, pes.TUE, pes.WED, pes.THU, pes.FRI, pes.SAT
      INTO
          out_exSundayFlag,
          out_exMondayFlag,
          out_exTuesdayFlag,
          out_exWednesdayFlag,
          out_exThursdayFlag,
          out_exFridayFlag,
          out_exSaturdayFlag
       FROM PRODUCT_EXCLUDED_STATES pes, ZIP_CODE z
       WHERE z.ZIP_CODE_ID = v_cleanZip
          AND pes.EXCLUDED_STATE = z.STATE_ID
          AND pes.PRODUCT_ID = UPPER(IN_PRODUCT_ID)
          AND ROWNUM()=1;

       IF
          out_exSundayFlag = 'Y' AND
          out_exMondayFlag = 'Y' AND
          out_exTuesdayFlag = 'Y' AND
          out_exWednesdayFlag = 'Y' AND
          out_exThursdayFlag = 'Y' AND
          out_exFridayFlag = 'Y' AND
          out_exSaturdayFlag = 'Y'
       THEN
          out_status := 'U';
       END IF;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      out_exSundayFlag := NULL;
      out_exMondayFlag := NULL;
      out_exTuesdayFlag := NULL;
      out_exWednesdayFlag := NULL;
      out_exThursdayFlag := NULL;
      out_exFridayFlag := NULL;
      out_exSaturdayFlag := NULL;
    END;

    out_zipSundayFlag := OE_GET_ZIP_SUNDAY_FLAG(v_cleanZip);

    -- Determine if this product is considered codified for Order Entry
    -- Flag whether product is codified and, if codified,
    --   whether product is available.  Non-codified products
    --   return value of 'NA' for availability.
    BEGIN
         SELECT NVL(check_oe_flag, 'N'), NVL(codified_special, 'N')
         INTO out_codifiedProduct, out_codifiedSpecial
         FROM CODIFIED_PRODUCTS
         WHERE PRODUCT_ID = out_productID AND check_oe_flag='Y';
    EXCEPTION WHEN NO_DATA_FOUND THEN
        out_codifiedProduct := 'N';
        out_codifiedSpecial := 'N';
    END;

    IF out_codifiedProduct = 'Y' THEN
       IF ( v_cleanZip IS NULL ) THEN
          BEGIN
            --If no zip was passed in, then assume the best case
            out_codifiedAvailable := 'S';
            v_codifiedAvailable := 'Y';
            v_zipDownFlag := 'N';
          END;

         ELSE
          BEGIN
               SELECT DECODE(cp.check_oe_flag, NULL, 'NA',
                DECODE(v_cleanZip, NULL, 'S', NVL(csz.available_flag, 'N')))
               INTO out_codifiedAvailable
               FROM CSZ_PRODUCTS csz, CODIFIED_PRODUCTS cp
               WHERE csz.PRODUCT_ID=out_productID
                     AND cp.PRODUCT_ID=out_productID
                     AND v_cszZip = csz.zip_code;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            IF out_codifiedProduct = 'Y' THEN
              out_codifiedAvailable := 'N';
            ELSE
              out_codifiedAvailable := 'NA';
            END IF;
          END;

          --Determine if the codified product is deliverable
          BEGIN
            SELECT cszp.available_flag INTO v_codifiedAvailable
            FROM CSZ_PRODUCTS cszp
            WHERE cszp.ZIP_CODE = v_cszZip AND cszp.PRODUCT_ID = UPPER(IN_PRODUCT_ID);
          EXCEPTION WHEN NO_DATA_FOUND THEN
            v_codifiedAvailable := 'N';
          END;

          BEGIN
            SELECT csz.CSZ_ZIP_CODE INTO v_zipDownFlag
            FROM CSZ_AVAIL csz
            WHERE csz.CSZ_ZIP_CODE = v_cszZip;
          EXCEPTION WHEN NO_DATA_FOUND THEN
            v_zipDownFlag := 'Y';
          END;

       END IF;
    ELSE
      out_codifiedAvailable := 'NA';
    END IF;

    IF (v_codifiedAvailable = 'N' AND v_zipDownFlag = 'Y')
      THEN out_codifiedDeliverable := 'D';
    ELSIF (v_codifiedAvailable = 'N' AND v_zipDownFlag != 'Y' AND out_zipGnaddFlag != 'Y')
      THEN out_codifiedDeliverable := 'N';
    ELSIF (v_codifiedAvailable = 'N' AND out_zipGnaddFlag != 'N')
      THEN out_codifiedDeliverable := 'G';
    ELSIF (v_codifiedAvailable != 'N')
      THEN out_codifiedDeliverable := v_codifiedAvailable;
    END IF;

    --Second choice description
--    SELECT OE_GET_SECOND_CHOICE_DESC(out_secondChoiceCode)
--           INTO out_secondChoice from dual;
    BEGIN
      SELECT OE_DESCRIPTION1 INTO out_secondChoice
      FROM PRODUCT_SECOND_CHOICE
      WHERE PRODUCT_SECOND_CHOICE_ID = out_secondChoiceCode;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      out_secondChoice := NULL;
    END;

    --Standard discounts
    BEGIN
      SELECT DISCOUNT_TYPE, DISCOUNT_AMT
      INTO out_discountType, out_standardDiscountAmt
      FROM PRICE_HEADER_DETAILS
      WHERE v_pricingCode = PRICE_HEADER_ID AND out_standardPrice >= MIN_DOLLAR_AMT
          AND out_standardPrice <= MAX_DOLLAR_AMT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      out_discountType :=NULL;
      out_standardDiscountAmt :=NULL;
    END;

    --Deluxe discounts
    BEGIN
      SELECT DECODE(out_discountType,NULL,DISCOUNT_TYPE,out_discountType), DISCOUNT_AMT
      INTO out_discountType, out_deluxeDiscountAmt
      FROM PRICE_HEADER_DETAILS
      WHERE v_pricingCode = PRICE_HEADER_ID AND out_deluxePrice >= MIN_DOLLAR_AMT
          AND out_deluxePrice <= MAX_DOLLAR_AMT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      out_deluxeDiscountAmt := NULL;
    END;

    --Premium discounts
    BEGIN
      SELECT DECODE(out_discountType,NULL,DISCOUNT_TYPE,out_discountType), DISCOUNT_AMT
      INTO out_discountType, out_premiumDiscountAmt
      FROM PRICE_HEADER_DETAILS
      WHERE v_pricingCode = PRICE_HEADER_ID AND out_premiumPrice >= MIN_DOLLAR_AMT
          AND out_premiumPrice <= MAX_DOLLAR_AMT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      out_premiumDiscountAmt := NULL;
    END;

    --Product Colors
    BEGIN
      SELECT COLORS_COUNT
      INTO v_colorCount
      FROM PRODUCT_COLORS_CNT_VW
      WHERE PRODUCT_ID = out_productID;

      v_counter := 0;
      DECLARE CURSOR color_cur IS
      SELECT NLS_INITCAP(COLOR_DESCRIPTION) AS "DESCRIPTION", COLOR_RANK
      FROM PRODUCT_COLORS_LIST_VW
      WHERE PRODUCT_ID = out_productId
      ORDER BY COLOR_RANK;
      BEGIN
        FOR color_rec IN color_cur
          LOOP
              v_counter := v_counter+1;
              EXIT WHEN v_counter > 8 OR v_counter > v_colorCount;
              CASE color_rec.COLOR_RANK
                   WHEN 1 THEN
                        out_color1 := color_rec.DESCRIPTION;
                   WHEN 2 THEN
                        out_color2 := color_rec.DESCRIPTION;
                   WHEN 3 THEN
                        out_color3 := color_rec.DESCRIPTION;
                   WHEN 4 THEN
                        out_color4 := color_rec.DESCRIPTION;
                   WHEN 5 THEN
                        out_color5 := color_rec.DESCRIPTION;
                   WHEN 6 THEN
                        out_color6 := color_rec.DESCRIPTION;
                   WHEN 7 THEN
                        out_color7 := color_rec.DESCRIPTION;
                   WHEN 8 THEN
                        out_color8 := color_rec.DESCRIPTION;
               END CASE;
          END LOOP;
      END;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      NULL;
    END;

    --Service charge
    BEGIN
    -- If this is a same day gift then get the charge from
    -- the shipping key
    IF out_productType = 'SDG'
    THEN
      select skc.SHIPPING_COST
      into out_serviceCharge
      from SHIPPING_KEY_COSTS skc, SHIPPING_KEY_DETAILS skd
      WHERE skd.SHIPPING_KEY_ID = out_shippingKey
        AND skd.SHIPPING_DETAIL_ID = skc.SHIPPING_KEY_DETAIL_ID
        AND skd.MIN_PRICE <= out_standardPrice
        AND skd.MAX_PRICE >= out_standardPrice
        AND skc.SHIPPING_METHOD_ID = 'SD';
    ELSE
      select DECODE(UPPER(IN_DOMESTIC_INTL_FLAG),
          'D',
          nvl(
              (select sfo.domestic_charge from ftd_apps.service_fee_override sfo
                  where sfo.snh_id = snh.snh_id
                  and sfo.override_date = to_date(in_delivery_date, 'MM/DD/YYYY')
          ), snh.first_order_domestic),
          'I',
          nvl(
              (select sfo.international_charge from ftd_apps.service_fee_override sfo
                  where sfo.snh_id = snh.snh_id
                  and sfo.override_date = to_date(in_delivery_date, 'MM/DD/YYYY')
          ), snh.first_order_international),
          NULL)
      into out_serviceCharge
      from SNH snh, SOURCE sc
      -- Use the source code to get shipping and handling for florist delivered
      WHERE sc.source_code = v_sourceCode
        AND sc.shipping_code = snh.snh_id;
    END IF;
  EXCEPTION WHEN OTHERS THEN
      out_serviceCharge := NULL;
  END;

  --Ship days (old proc returns regardless of product type or ship method)
  BEGIN
    SELECT
         MONDAY_FLAG,
         TUESDAY_FLAG,
         WEDNESDAY_FLAG,
         THURSDAY_FLAG,
         FRIDAY_FLAG,
         SATURDAY_FLAG,
         SUNDAY_FLAG
    INTO
         out_mondayFlag,
         out_tuesdayFlag,
         out_wednesdayFlag,
         out_thursdayFlag,
         out_fridayFlag,
         out_saturdayFlag,
         out_sundayFlag
    FROM PRODUCT_SHIP_DATES
    WHERE PRODUCT_ID = out_productID;
  EXCEPTION WHEN NO_DATA_FOUND THEN
         out_mondayFlag := NULL;
         out_tuesdayFlag := NULL;
         out_wednesdayFlag := NULL;
         out_thursdayFlag := NULL;
         out_fridayFlag := NULL;
         out_saturdayFlag := NULL;
         out_sundayFlag := NULL;
   END;

   --Drop ship only
   IF out_shipMethodCarrier='Y' THEN
     --Carrier
     BEGIN
       SELECT SHIPPER
       INTO out_carrier
       FROM SHIPPING_KEYS
       WHERE SHIPPING_KEY_ID = out_shippingKey;
     EXCEPTION WHEN NO_DATA_FOUND THEN
       out_carrier := NULL;
     END;

     --Vendor block delivery dates
     BEGIN
         v_counter := 0;
         DECLARE CURSOR delivery_cur IS
         SELECT START_DATE, END_DATE, COUNT
         FROM PROD_DELIV_BLOCK_LIST_VW
         WHERE PRODUCT_ID = out_productId
               --AND END_DATE >= TRUNC(SYSDATE)
         ORDER BY COUNT;
         BEGIN
           FOR delivery_rec IN delivery_cur
              LOOP
                  v_counter := v_counter+1;
                  EXIT WHEN v_counter > 6;
                  CASE delivery_rec.COUNT
                       WHEN 1 THEN
                            out_vendorDelivBlockStart1 := to_char(delivery_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorDelivBlockEnd1 := to_char(delivery_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 2 THEN
                            out_vendorDelivBlockStart2 := to_char(delivery_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorDelivBlockEnd2 := to_char(delivery_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 3 THEN
                            out_vendorDelivBlockStart3 := to_char(delivery_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorDelivBlockEnd3 := to_char(delivery_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 4 THEN
                            out_vendorDelivBlockStart4 := to_char(delivery_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorDelivBlockEnd4 := to_char(delivery_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 5 THEN
                            out_vendorDelivBlockStart5 := to_char(delivery_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorDelivBlockEnd5 := to_char(delivery_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 6 THEN
                            out_vendorDelivBlockStart6 := to_char(delivery_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorDelivBlockEnd6 := to_char(delivery_rec.END_DATE,'mm/dd/yyyy');
                       ELSE
                         NULL;
                   END CASE;
              END LOOP;
         END;
     EXCEPTION WHEN NO_DATA_FOUND THEN
       NULL;
     END;

     --Vendor block ship dates
     BEGIN
         v_counter := 0;
         DECLARE CURSOR ship_cur IS
         SELECT START_DATE, END_DATE, COUNT
         FROM PROD_SHIP_BLOCK_LIST_VW
         WHERE PRODUCT_ID = out_productId
               --AND END_DATE >= TRUNC(SYSDATE)
         ORDER BY COUNT;
         BEGIN
           FOR ship_rec IN ship_cur
              LOOP
                  v_counter := v_counter+1;
                  EXIT WHEN v_counter > 6;
                  CASE ship_rec.COUNT
                       WHEN 1 THEN
                            out_vendorShipBlockStart1 := to_char(ship_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorShipBlockEnd1 := to_char(ship_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 2 THEN
                            out_vendorShipBlockStart2 := to_char(ship_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorShipBlockEnd2 := to_char(ship_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 3 THEN
                            out_vendorShipBlockStart3 := to_char(ship_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorShipBlockEnd3 := to_char(ship_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 4 THEN
                            out_vendorShipBlockStart4 := to_char(ship_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorShipBlockEnd4 := to_char(ship_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 5 THEN
                            out_vendorShipBlockStart5 := to_char(ship_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorShipBlockEnd5 := to_char(ship_rec.END_DATE,'mm/dd/yyyy');
                       WHEN 6 THEN
                            out_vendorShipBlockStart6 := to_char(ship_rec.START_DATE,'mm/dd/yyyy');
                            out_vendorShipBlockEnd6 := to_char(ship_rec.END_DATE,'mm/dd/yyyy');
                       ELSE
                         NULL;
                   END CASE;
              END LOOP;
         END;
     EXCEPTION WHEN NO_DATA_FOUND THEN
       RETURN cur_cursor;
     END;
  END IF;

  -- Open a query ref cursor to return product list results
  OPEN cur_cursor FOR
      SELECT
              out_productId              as "productID",
              out_novatorId              as "novatorID",
              out_productName            as "productName",
              out_novatorName            as "novatorName",
              out_deliveryType           as "deliveryType",
              out_productCategory        as "productCategory",
              out_productType            as "productType",
              out_productSubType         as "productSubType",
              out_zipGnaddFlag           as "zipGnaddFlag",
              out_zipFloralFlag          as "zipFloralFlag",
              out_zipGotoFloristFlag     as "zipGotoFloristFlag",
              out_zipSundayFlag          as "zipSundayFlag",
              out_timeZone               as "timeZone",
              out_stateId                as "stateId",
              out_addonDays              as "addonDays",
              out_egiftFlag              as "egiftFlag",
              out_addonBalloonsFlag      as "addonBalloonsFlag",
              out_addonBearsFlag         as "addonBearsFlag",
              out_addonCardsFlag         as "addonCardsFlag",
              out_addonChocolateFlag     as "addonChocolateFlag",
              out_addonFuneralFlag       as "addonFuneralFlag",
              out_discountType           as "discountType",
              out_standardPrice          as "standardPrice",
              out_standardDiscountAmt    as "standardDiscountAmt",
              out_deluxePrice            as "deluxePrice",
              out_deluxeDiscountAmt      as "deluxeDiscountAmt",
              out_premiumPrice           as "premiumPrice",
              out_premiumDiscountAmt     as "premiumDiscountAmt",
              out_variablePriceMax       as "variablePriceMax",
              out_longDescription        as "longDescription",
              out_discountFlag           as "discountFlag",
              out_deliveryFlag           as "deliveryFlag",
              out_serviceFeeFlag         as "serviceFeeFlag",
              out_arrangementSize        as "arrangementSize",
              out_priceRank1             as "priceRank1",
              out_priceRank2             as "priceRank2",
              out_priceRank3             as "priceRank3",
              out_exceptionCode          as "exceptionCode",
              out_exceptionStartDate     as "exceptionStartDate",
              out_exceptionEndDate       as "exceptionEndDate",
              out_jcpCategory            as "jcpCategory",
              out_mondayFlag             as "mondayFlag",
              out_tuesdayFlag            as "tuesdayFlag",
              out_wednesdayFlag          as "wednesdayFlag",
              out_thursdayFlag           as "thursdayFlag",
              out_fridayFlag             as "fridayFlag",
              out_saturdayFlag           as "saturdayFlag",
              out_sundayFlag             as "sundayFlag",
              out_exSundayFlag           as "exSundayFlag",
              out_exMondayFlag           as "exMondayFlag",
              out_exTuesdayFlag          as "exTuesdayFlag",
              out_exWednesdayFlag        as "exWednesdayFlag",
              out_exThursdayFlag         as "exThursdayFlag",
              out_exFridayFlag           as "exFridayFlag",
              out_exSaturdayFlag         as "exSaturdayFlag",
              out_mondayDelFCFlag        as "mondayDelFCFlag",
              out_twoDaySatFCFlag        as "twoDaySatFCFlag",
              out_vendorID               as "vendorID",
              out_exceptionMessage       as "exceptionMessage",
              out_shipMethodCarrier      as "shipMethodCarrier",
              out_shipMethodFlorist      as "shipMethodFlorist",
              out_variablePriceFlag      as "variablePriceFlag",
              out_shippingKey            as "shippingKey",
              out_secondChoice           as "secondChoice",
              out_secondChoiceCode       as "secondChoiceCode",
              out_status                 as "status",
              out_egift                  as "egift",
              out_codifiedProduct        as "codifiedProduct",
              out_codifiedAvailable      as "codifiedAvailable",
              out_codifiedSpecial        as "codifiedSpecial",
              out_codifiedDeliverable    as "codifiedDeliverable",
              out_serviceCharge          as "serviceCharge",
              out_carrier                as "carrier",
              out_color1                 as "color1",
              out_color2                 as "color2",
              out_color3                 as "color3",
              out_color4                 as "color4",
              out_color5                 as "color5",
              out_color6                 as "color6",
              out_color7                 as "color7",
              out_color8                 as "color8",
              out_vendorShipBlockStart1  as "vendorShipBlockStart1",
              out_vendorShipBlockEnd1    as "vendorShipBlockEnd1",
              out_vendorShipBlockStart2  as "vendorShipBlockStart2",
              out_vendorShipBlockEnd2    as "vendorShipBlockEnd2",
              out_vendorShipBlockStart3  as "vendorShipBlockStart3",
              out_vendorShipBlockEnd3    as "vendorShipBlockEnd3",
              out_vendorShipBlockStart4  as "vendorShipBlockStart4",
              out_vendorShipBlockEnd4    as "vendorShipBlockEnd4",
              out_vendorShipBlockStart5  as "vendorShipBlockStart5",
              out_vendorShipBlockEnd5    as "vendorShipBlockEnd5",
              out_vendorShipBlockStart6  as "vendorShipBlockStart6",
              out_vendorShipBlockEnd6    as "vendorShipBlockEnd6",
              out_vendorDelivBlockStart1 as "vendorDelivBlockStart1",
              out_vendorDelivBlockEnd1   as "vendorDelivBlockEnd1",
              out_vendorDelivBlockStart2 as "vendorDelivBlockStart2",
              out_vendorDelivBlockEnd2   as "vendorDelivBlockEnd2",
              out_vendorDelivBlockStart3 as "vendorDelivBlockStart3",
              out_vendorDelivBlockEnd3   as "vendorDelivBlockEnd3",
              out_vendorDelivBlockStart4 as "vendorDelivBlockStart4",
              out_vendorDelivBlockEnd4   as "vendorDelivBlockEnd4",
              out_vendorDelivBlockStart5 as "vendorDelivBlockStart5",
              out_vendorDelivBlockEnd5   as "vendorDelivBlockEnd5",
              out_vendorDelivBlockStart6 as "vendorDelivBlockStart6",
              out_vendorDelivBlockEnd6   as "vendorDelivBlockEnd6",
              out_largeImage             as "largeImage",
              out_weboeBlocked           as "weboeBlocked",
              out_personalization_template as "personalizationTemplateId",
              out_personal_greeting_flag as "personalGreetingFlag",
              out_allow_free_shipping_flag as "allowFreeShippingFlag"

      FROM dual;

  RETURN cur_cursor;
END;
.
/
/
