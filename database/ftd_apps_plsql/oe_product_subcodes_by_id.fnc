CREATE OR REPLACE
FUNCTION ftd_apps.OE_PRODUCT_SUBCODES_BY_ID  (inProductSubCodeId IN VARCHAR2)
 RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_PRODUCT_SUBCODES
-- Type:    Function
-- Syntax:  OE_PRODUCT_SUBCODES(inProductSubCodeId IN VARCHAR2)
-- Returns: ref_cursor for
--          productSubcodeId            VARCHAR2(10)
--          subcodeDescription        VARCHAR2(100)
--          subcodePrice                NUMBER(8,2)
--          subcodeReferenceNumber  VARCHAR2(40)
--          holidaySku                  VARCHAR2(6)
--          holidayPrice                NUMBER(8,2)
--
-- Description:   Queries the PRODUCT_SUBCODES table by PRODUCT_SUBCODE_ID.  Returns any
--                active subcodes for the product with detail information.
--
--===============================================================================
AS
    subcode_cursor types.ref_cursor;
BEGIN
    OPEN subcode_cursor FOR
        SELECT PRODUCT_ID as "productId",
               PRODUCT_SUBCODE_ID as "productSubcodeId",
               SUBCODE_DESCRIPTION as "subcodeDesc",
               TO_CHAR(SUBCODE_PRICE, '9999999.00') as "subcodePrice",
               SUBCODE_REFERENCE_NUMBER as "subcodeRefNumber",
               HOLIDAY_SKU as "holidaySku",
               HOLIDAY_PRICE as "holidayPrice"
        FROM PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = UPPER(inProductSubCodeId)
          AND NVL(ACTIVE_FLAG, 'N') = 'Y';

    RETURN subcode_cursor;
END
;
.
/
