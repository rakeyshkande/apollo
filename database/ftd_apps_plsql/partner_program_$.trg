CREATE OR REPLACE
TRIGGER ftd_apps.partner_program_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.partner_program REFERENCING OLD AS old NEW AS new FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO ftd_apps.partner_program$ (
      program_name,
      partner_name,
      program_type,
      email_exclude_flag,
      created_on,
      created_by,
      updated_on,
      updated_by,
      last_post_date,
      operation$, timestamp$
      ) VALUES (
      :NEW.program_name,
      :NEW.partner_name,
      :NEW.program_type,
      :NEW.email_exclude_flag,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      :NEW.last_post_date,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.partner_program$ (
      program_name,
      partner_name,
      program_type,
      email_exclude_flag,
      created_on,
      created_by,
      updated_on,
      updated_by,
      last_post_date,
      operation$, timestamp$
      ) VALUES (
      :OLD.program_name,
      :OLD.partner_name,
      :OLD.program_type,
      :OLD.email_exclude_flag,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      :OLD.last_post_date,
      'UPD_OLD',SYSDATE);

      INSERT INTO ftd_apps.partner_program$ (
      program_name,
      partner_name,
      program_type,
      email_exclude_flag,
      created_on,
      created_by,
      updated_on,
      updated_by,
      last_post_date,
      operation$, timestamp$
      ) VALUES (
      :NEW.program_name,
      :NEW.partner_name,
      :NEW.program_type,
      :NEW.email_exclude_flag,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      :NEW.last_post_date,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.partner_program$ (
      program_name,
      partner_name,
      program_type,
      email_exclude_flag,
      created_on,
      created_by,
      updated_on,
      updated_by,
      last_post_date,
      operation$, timestamp$
      ) VALUES (
      :OLD.program_name,
      :OLD.partner_name,
      :OLD.program_type,
      :OLD.email_exclude_flag,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      :OLD.last_post_date,
      'DEL',SYSDATE);

   END IF;

END;
/
