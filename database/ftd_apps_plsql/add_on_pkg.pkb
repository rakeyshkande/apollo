CREATE OR REPLACE
PACKAGE BODY ftd_apps.ADD_ON_PKG AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the addons that are
        associated to a specific product id

Input:
        IN_PRODUCT_ID : The product Id to search for
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/

PROCEDURE GET_ADDON_CUR_BY_PRODUCT_ID
(
IN_PRODUCT_ID                     IN PRODUCT_ADDON.PRODUCT_ID%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for        
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG
        from PRODUCT_ADDON fpa, ADDON faa, ADDON_TYPE faat
        where fpa.PRODUCT_ID = IN_PRODUCT_ID
        and fpa.ADDON_ID = faa.ADDON_ID
        and faat.ADDON_TYPE_ID = faa.ADDON_TYPE
        and fpa.ACTIVE_FLAG = 'Y'
        and faa.ACTIVE_FLAG = 'Y'
        order by fpa.DISPLAY_SEQ_NUM;
        
END GET_ADDON_CUR_BY_PRODUCT_ID;



/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the addon specific information  
        based on an inputted product id, occasion id, and source code

Input:
        IN_PRODUCT_ID :                   The product id to filter the results on
        IN_OCCASION_ID :                  The occasion id to filter the results on
        IN_SOURCE_CODE :                  The source code to filter the results on
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_ADDON_CUR_BY_PROD_OCCA
(
IN_PRODUCT_ID                     IN PRODUCT_ADDON.PRODUCT_ID%TYPE,
IN_OCCASION_ID                    IN OCCASION_ADDON.OCCASION_ID%TYPE,
IN_SOURCE_CODE                    IN SOURCE_MASTER.SOURCE_CODE%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS

v_partner_name                    PARTNER_PROGRAM.PARTNER_NAME%TYPE;
v_global_chocolate_flag           FRP.GLOBAL_PARMS.VALUE%TYPE;
v_add_on_free_id                  SOURCE_MASTER.ADD_ON_FREE_ID%TYPE;
v_ship_method_florist             PRODUCT_MASTER.SHIP_METHOD_FLORIST%TYPE;
v_ship_method_carrier             PRODUCT_MASTER.SHIP_METHOD_CARRIER%TYPE;


BEGIN
    
    select distinct pp.PARTNER_NAME 
    INTO v_partner_name
    from PARTNER_PROGRAM PP, SOURCE_PROGRAM_REF SPR, SOURCE_MASTER SM
    where SM.SOURCE_CODE = IN_SOURCE_CODE
      and SM.SOURCE_CODE = SPR.SOURCE_CODE (+)
      and SPR.PROGRAM_NAME = PP.PROGRAM_NAME (+);
     
    v_global_chocolate_flag := frp.misc_pkg.get_global_parm_value('FTDAPPS_PARMS', 'MOD_ORDER_CHOCOLATES_AVAILABLE');

    IF IN_SOURCE_CODE is null THEN
       v_add_on_free_id := 'N/A';
    ELSE
       select sm.ADD_ON_FREE_ID
       INTO v_add_on_free_id
       from SOURCE_MASTER sm
       where sm.SOURCE_CODE = IN_SOURCE_CODE;
    END IF;
    
    select pm.SHIP_METHOD_FLORIST
    INTO v_ship_method_florist
    from PRODUCT_MASTER pm
    where pm.PRODUCT_ID = IN_PRODUCT_ID;
    
    select pm.SHIP_METHOD_CARRIER
    INTO v_ship_method_carrier
    from PRODUCT_MASTER pm
    where pm.PRODUCT_ID = IN_PRODUCT_ID;
   
    OPEN out_cur for
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, fapa.DISPLAY_SEQ_NUM, fapa.MAX_QTY ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'N' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'dropship'
        ELSE 'florist'
        END
        ) as ADDON_DELIVERY_TYPE 
		
        from ADDON faa, ADDON_TYPE faat, OCCASION_ADDON faoa, PRODUCT_ADDON fapa
        where faa.addon_type = faat.addon_type_id
        and fapa.product_id = IN_PRODUCT_ID
        and (IN_OCCASION_ID is null or faoa.occasion_id = IN_OCCASION_ID)
        and faa.addon_id = faoa.addon_id
        and fapa.addon_id = faa.addon_id
        and faa.active_flag = 'Y'
        and fapa.ACTIVE_FLAG = 'Y'
	and ((v_ship_method_florist = 'Y') or 
                (v_ship_method_carrier = 'Y' and exists ( 
                select 'Y' 
                from ftd_apps.VENDOR_ADDON  fava, ftd_apps.VENDOR_PRODUCT favp
                where fava.ADDON_ID = faa.addon_id
                and fava.ACTIVE_FLAG = 'Y'
                and favp.PRODUCT_SUBCODE_ID = IN_PRODUCT_ID 
                and favp.VENDOR_ID = fava.VENDOR_ID
                and favp.AVAILABLE = 'Y')
                )
        )
        and faa.addon_type not in ('4','3',decode(v_global_chocolate_flag, 'Y', decode(v_partner_name, 'USAA', decode(v_add_on_free_id, 'FC', '5', 'N/A'), 'N/A'), '5'), decode(v_add_on_free_id, faa.addon_id, 'N/A', '6'))
      UNION ALL
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, 1 AS DISPLAY_SEQ_NUM, 1 as MAX_QTY ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'N' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'dropship'
        ELSE 'florist'
        END
        ) as ADDON_DELIVERY_TYPE 
		
        from ADDON faa, ADDON_TYPE faat, PRODUCT_MASTER fapm, OCCASION_ADDON faoa
        where faa.addon_type = faat.addon_type_id
        and fapm.product_id = IN_PRODUCT_ID
        and faa.addon_id = faoa.addon_id
        and (IN_OCCASION_ID is null or faoa.occasion_id = IN_OCCASION_ID)
        and faa.active_flag = 'Y'
        and fapm.ADD_ON_CARDS_FLAG = 'Y' 
        and faa.ADDON_TYPE = '4'
      UNION ALL
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, 1 AS DISPLAY_SEQ_NUM, 1 as MAX_QTY ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'N' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'dropship'
        ELSE 'florist'
        END
        ) as ADDON_DELIVERY_TYPE 
		
        from ADDON faa, ADDON_TYPE faat, PRODUCT_MASTER fapm, OCCASION_ADDON faoa
        where faa.addon_type = faat.addon_type_id
        and fapm.product_id = IN_PRODUCT_ID
        and faa.addon_id = faoa.addon_id
        and (IN_OCCASION_ID is null or faoa.occasion_id = IN_OCCASION_ID)
        and faa.active_flag = 'Y'
        and fapm.ADD_ON_FUNERAL_FLAG = 'Y' 
        and faa.ADDON_TYPE = '3'
        order by DISPLAY_SEQ_NUM, ADD_ON_DESCRIPTION, ADDON_ID;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
        -- Just bail out if any of the select into's above fail
        OPEN out_cur FOR select null from dual;

END GET_ADDON_CUR_BY_PROD_OCCA;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the addon specific information
        based on an inputted product id, occasion id, florist available (delivery type) , dropship available (delivery type)  and source code

Input:
        IN_PRODUCT_ID :                   The product id to filter the results on
        IN_OCCASION_ID :                  The occasion id to filter the results on
        IN_SOURCE_CODE :                  The source code to filter the results on
        IN_FLORIST_AVAILABLE              "Y" if the product Delivered by florist
        IN_DROPSHIP_AVAILABLE             "Y" if the product Delivered by dropship/carrier
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/

PROCEDURE GET_ADDON_CUR_BY_PROD_OCCA_DTP
(
IN_PRODUCT_ID                     IN PRODUCT_ADDON.PRODUCT_ID%TYPE,
IN_OCCASION_ID                    IN OCCASION_ADDON.OCCASION_ID%TYPE,
IN_SOURCE_CODE                    IN SOURCE_MASTER.SOURCE_CODE%TYPE,
IN_FLORIST_AVAILABLE              IN VARCHAR2,
IN_DROPSHIP_AVAILABLE             IN VARCHAR2,

OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS

v_partner_name                    PARTNER_PROGRAM.PARTNER_NAME%TYPE;
v_global_chocolate_flag           FRP.GLOBAL_PARMS.VALUE%TYPE;
v_add_on_free_id                  SOURCE_MASTER.ADD_ON_FREE_ID%TYPE;
v_ship_method_florist             PRODUCT_MASTER.SHIP_METHOD_FLORIST%TYPE;
v_ship_method_carrier             PRODUCT_MASTER.SHIP_METHOD_CARRIER%TYPE;


BEGIN

    select distinct pp.PARTNER_NAME
    INTO v_partner_name
    from PARTNER_PROGRAM PP, SOURCE_PROGRAM_REF SPR, SOURCE_MASTER SM
    where SM.SOURCE_CODE = IN_SOURCE_CODE
      and SM.SOURCE_CODE = SPR.SOURCE_CODE (+)
      and SPR.PROGRAM_NAME = PP.PROGRAM_NAME (+);

    v_global_chocolate_flag := frp.misc_pkg.get_global_parm_value('FTDAPPS_PARMS', 'MOD_ORDER_CHOCOLATES_AVAILABLE');

    IF IN_SOURCE_CODE is null THEN
       v_add_on_free_id := 'N/A';
    ELSE
       select sm.ADD_ON_FREE_ID
       INTO v_add_on_free_id
       from SOURCE_MASTER sm
       where sm.SOURCE_CODE = IN_SOURCE_CODE;
    END IF;

    select pm.SHIP_METHOD_FLORIST
    INTO v_ship_method_florist
    from PRODUCT_MASTER pm
    where pm.PRODUCT_ID = IN_PRODUCT_ID;

    select pm.SHIP_METHOD_CARRIER
    INTO v_ship_method_carrier
    from PRODUCT_MASTER pm
    where pm.PRODUCT_ID = IN_PRODUCT_ID;


	
    OPEN out_cur for
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, fapa.DISPLAY_SEQ_NUM, fapa.MAX_QTY ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'N')
        THEN 'florist'
        ELSE 'dropship' 
        END
        ) as ADDON_DELIVERY_TYPE 
		
        from ADDON faa, ADDON_TYPE faat, OCCASION_ADDON faoa, PRODUCT_ADDON fapa
        where faa.addon_type = faat.addon_type_id
        and fapa.product_id = IN_PRODUCT_ID
        and (IN_OCCASION_ID is null or faoa.occasion_id = IN_OCCASION_ID)
        and faa.addon_id = faoa.addon_id
        and fapa.addon_id = faa.addon_id
        and faa.active_flag = 'Y'
        and fapa.ACTIVE_FLAG = 'Y'
		and (faa.FLORIST_ADDON_FLAG = IN_FLORIST_AVAILABLE or  faa.VENDOR_ADDON_FLAG = IN_DROPSHIP_AVAILABLE)

	and ((v_ship_method_florist = 'Y' ) or
                (v_ship_method_carrier = 'Y' and exists (
                select 'Y'
                from ftd_apps.VENDOR_ADDON  fava, ftd_apps.VENDOR_PRODUCT favp
                where fava.ADDON_ID = faa.addon_id
                and fava.ACTIVE_FLAG = 'Y'
                and favp.PRODUCT_SUBCODE_ID = IN_PRODUCT_ID
                and favp.VENDOR_ID = fava.VENDOR_ID
                and favp.AVAILABLE = 'Y')
                )
        )
        and faa.addon_type not in ('4','3',decode(v_global_chocolate_flag, 'Y', decode(v_partner_name, 'USAA', decode(v_add_on_free_id, 'FC', '5', 'N/A'), 'N/A'), '5'), decode(v_add_on_free_id, faa.addon_id, 'N/A', '6'))
		
		
		
         UNION ALL
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, 1 AS DISPLAY_SEQ_NUM, 1 as MAX_QTY ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'N')
        THEN 'florist'
        ELSE 'dropship' 
        END
        ) as ADDON_DELIVERY_TYPE 
		
        from ADDON faa, ADDON_TYPE faat, PRODUCT_MASTER fapm, OCCASION_ADDON faoa
        where faa.addon_type = faat.addon_type_id
        and fapm.product_id = IN_PRODUCT_ID
        and faa.addon_id = faoa.addon_id
        and (IN_OCCASION_ID is null or faoa.occasion_id = IN_OCCASION_ID)
        and faa.active_flag = 'Y'
        and fapm.ADD_ON_CARDS_FLAG = 'Y'
        and faa.ADDON_TYPE = '4'
		and (faa.FLORIST_ADDON_FLAG = IN_FLORIST_AVAILABLE or  faa.VENDOR_ADDON_FLAG = IN_DROPSHIP_AVAILABLE)
				
      UNION ALL
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, 1 AS DISPLAY_SEQ_NUM, 1 as MAX_QTY ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'N')
        THEN 'florist'
        ELSE 'dropship' 
        END
        ) as ADDON_DELIVERY_TYPE 
		
        from ADDON faa, ADDON_TYPE faat, PRODUCT_MASTER fapm, OCCASION_ADDON faoa
        where faa.addon_type = faat.addon_type_id
        and fapm.product_id = IN_PRODUCT_ID
        and faa.addon_id = faoa.addon_id
        and (IN_OCCASION_ID is null or faoa.occasion_id = IN_OCCASION_ID)
        and faa.active_flag = 'Y'
        and fapm.ADD_ON_FUNERAL_FLAG = 'Y'
        and faa.ADDON_TYPE = '3'
		and (faa.FLORIST_ADDON_FLAG = IN_FLORIST_AVAILABLE or  faa.VENDOR_ADDON_FLAG = IN_DROPSHIP_AVAILABLE)
		
        order by DISPLAY_SEQ_NUM, ADD_ON_DESCRIPTION, ADDON_ID;
		

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
        -- Just bail out if any of the select into's above fail
        OPEN out_cur FOR select null from dual;

END GET_ADDON_CUR_BY_PROD_OCCA_DTP;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the addons that are
        associated to a specific vendor id

Input:
        IN_VENDOR_ID : The vendor Id to search for
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/

PROCEDURE GET_ALL_ADDON_CUR_BY_VENDOR_ID
(
IN_VENDOR_ID                      IN VENDOR_ADDON.VENDOR_ID%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for      
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, fva.ACTIVE_FLAG as VENDOR_ACTIVE_FLAG, fva.VENDOR_SKU, fva.VENDOR_COST
        from ADDON faa, ADDON_TYPE faat, VENDOR_ADDON fva
        where faa.ADDON_ID = fva.ADDON_ID (+)
        and IN_VENDOR_ID = fva.VENDOR_ID (+)
        and faat.ADDON_TYPE_ID = faa.ADDON_TYPE
        ORDER BY ADD_ON_DESCRIPTION;
        
END GET_ALL_ADDON_CUR_BY_VENDOR_ID;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the active addons that are
        associated to a specific vendor id

Input:
        IN_VENDOR_ID : The vendor Id to search for
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/

PROCEDURE GET_ATV_ADDON_CUR_BY_VENDOR_ID
(
IN_VENDOR_ID                      IN VENDOR_ADDON.VENDOR_ID%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, fva.ACTIVE_FLAG as VENDOR_ACTIVE_FLAG, fva.VENDOR_SKU, fva.VENDOR_COST,
        faa.PQUAD_ACCESSORY_ID   from ADDON faa, ADDON_TYPE faat, VENDOR_ADDON fva
        where fva.VENDOR_ID = IN_VENDOR_ID
        and fva.ADDON_ID = faa.ADDON_ID
        and faat.ADDON_TYPE_ID = faa.ADDON_TYPE
        and fva.ACTIVE_FLAG = 'Y'
        ORDER BY ADD_ON_DESCRIPTION;
        
END GET_ATV_ADDON_CUR_BY_VENDOR_ID;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the addons that are
        associated to a specific order

Input:
        IN_ORDER_DETAIL_ID                   the order detail id to search for
        IN_VENDOR_INFO_FLAG                  Whether or not to return the vendor info
Output:
        OUT_CUR                              cursor with add-on information
        OUT_VENDOR_CUR                       cursor with vendor information

-----------------------------------------------------------------------------*/


PROCEDURE GET_ADDON_CUR_BY_DETAIL_ID
(
IN_ORDER_DETAIL_ID                   IN PRODUCT_ADDON.PRODUCT_ID%TYPE,
IN_VENDOR_INFO_FLAG                  IN  VARCHAR2,
OUT_CUR                              OUT TYPES.REF_CURSOR,
OUT_VENDOR_CUR                       OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select faah.ADDON_ID, faah.ADDON_TYPE, faah.DESCRIPTION AS ADD_ON_DESCRIPTION, faah.PRICE, faah.ADDON_WEIGHT, faah.ADDON_TEXT, faah.UNSPSC, faah.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faah.DEFAULT_PER_TYPE_FLAG, faah.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, coao.ADD_ON_QUANTITY
        from ADDON_HISTORY faah, ADDON_TYPE faat, ADDON faa, CLEAN.ORDER_ADD_ONS coao
        where faah.ADDON_ID = coao.add_on_code
        and faah.ADDON_TYPE = faat.ADDON_TYPE_ID
        and faah.ADDON_ID = faa.ADDON_ID
        and faah.ADDON_HISTORY_ID = coao.ADD_ON_HISTORY_ID
        and coao.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;
        
    IF IN_VENDOR_INFO_FLAG = 'true' THEN
        OPEN out_vendor_cur for
            select fava.VENDOR_ID, fava.ADDON_ID, fava.VENDOR_SKU, fava.VENDOR_COST
            from FTD_APPS.VENDOR_ADDON fava, CLEAN.ORDER_ADD_ONS coao
             where fava.ACTIVE_FLAG = 'Y'
             and fava.ADDON_ID = coao.add_on_code
             and coao.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID
             order by fava.ADDON_ID;
    ELSE
      OPEN out_vendor_cur for
            select null from dual; 
    END IF;
        
END GET_ADDON_CUR_BY_DETAIL_ID;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the addons that are
        associated to a venus id

Input:
        IN_VENUS_ID                          The venus id to search for
        IN_VENDOR_INFO_FLAG                  Whether or not to return the vendor info

Output:
        OUT_CUR                              cursor with add-on information
        OUT_VENDOR_CUR                       cursor with the vendor information
-----------------------------------------------------------------------------*/
PROCEDURE GET_ADDON_CUR_BY_VENUS_ID
(
IN_VENUS_ID                          IN  VENUS.VENUS_ADD_ONS.VENUS_ID%TYPE,
IN_VENDOR_INFO_FLAG                  IN  VARCHAR2,
OUT_CUR                              OUT TYPES.REF_CURSOR,
OUT_VENDOR_CUR                       OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select  faah.ADDON_ID, faah.ADDON_TYPE, faah.DESCRIPTION AS ADD_ON_DESCRIPTION, faah.PRICE, faah.ADDON_WEIGHT, faah.ADDON_TEXT, faah.UNSPSC, faah.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faah.DEFAULT_PER_TYPE_FLAG, faah.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG, vva.ADDON_QTY AS ADD_ON_QUANTITY
        from ADDON_HISTORY faah, ADDON_TYPE faat, ADDON faa, venus.venus_add_ons vva
        where vva.ADDON_ID = faah.ADDON_ID
        and faah.ADDON_TYPE = faat.ADDON_TYPE_ID
        and faah.ADDON_ID = faa.ADDON_ID
        and faah.ADDON_HISTORY_ID = vva.ADD_ON_HISTORY_ID
        and vva.VENUS_ID = IN_VENUS_ID;
        
    IF IN_VENDOR_INFO_FLAG = 'true' THEN
        OPEN out_vendor_cur for
            select fava.VENDOR_ID, fava.ADDON_ID, fava.VENDOR_SKU, fava.VENDOR_COST
            from FTD_APPS.VENDOR_ADDON fava, venus.venus_add_ons vva
             where fava.ACTIVE_FLAG = 'Y'
             and fava.ADDON_ID = vva.ADDON_ID
             and vva.VENUS_ID = IN_VENUS_ID
             order by fava.ADDON_ID;
    ELSE
      OPEN out_vendor_cur for
            select null from dual; 
    END IF;      
        
END GET_ADDON_CUR_BY_VENUS_ID;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the list of vendors that can fulfill
        the inputted product and addons contained in the 

Input:
        IN_PRODUCT_ID : A string containing the product id
        IN_ADD_ON_LIST : A string containing the list of addons to be placed in a in() function
        IN_ADD_ON_COUNT : The number of add-ons in the IN_ADD_ON_LIST
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_VENDOR_BY_ADDON_PRODUCT
(
IN_PRODUCT_ID                         IN VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
IN_ADD_ON_ID_STRING                   IN VARCHAR2,
IN_ADD_ON_COUNT                       IN NUMBER,
OUT_CUR                               OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
       'select favp.VENDOR_ID
          from VENDOR_PRODUCT favp
          where favp.PRODUCT_SUBCODE_ID = ''' || IN_PRODUCT_ID || '''
          and favp.AVAILABLE = ''Y''
          and favp.REMOVED = ''N''
          INTERSECT
          select fava.VENDOR_ID 
          from VENDOR_ADDON fava, ADDON faa
          where fava.ACTIVE_FLAG = ''Y''
          and fava.ADDON_ID in (' || IN_ADD_ON_ID_STRING || ')
          and faa.ADDON_ID = fava.ADDON_ID
          and faa.ACTIVE_FLAG = ''Y''
          Group by fava.VENDOR_ID
          having count(fava.VENDOR_ID) = ' || IN_ADD_ON_COUNT;
        
END GET_VENDOR_BY_ADDON_PRODUCT;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the addon information  
        for an inputted addon id

Input:
        IN_ADD_ON_ID : The addon Id to search for
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/

PROCEDURE GET_ADDON_BY_ADDON_ID
(
IN_ADD_ON_ID                          IN ADDON.ADDON_ID%TYPE,
IN_VENDOR_INFO_FLAG                   IN  VARCHAR2,
IN_PRODUCT_INFO_FLAG                  IN  VARCHAR2,
OUT_CUR                               OUT TYPES.REF_CURSOR,
OUT_VENDOR_CUR                        OUT TYPES.REF_CURSOR,
OUT_PRODUCT_CUR                       OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAG ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'N' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'dropship'
        ELSE 'florist'
        END
        ) as ADDON_DELIVERY_TYPE, 
        (
			SELECT 	LISTAGG(occasion_id, ',') WITHIN GROUP (ORDER BY occasion_id)
			FROM 	ftd_apps.occasion_addon
			WHERE 	addon_id = IN_ADD_ON_ID
		) LINKED_OCCASIONS, -- Changes for Apollo.Q3.2015 - Addon-Occasion association
		faa.PQUAD_ACCESSORY_ID, faa.IS_FTD_WEST_ADDON -- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
        from FTD_APPS.ADDON faa, FTD_APPS.ADDON_TYPE faat
        where faa.ADDON_ID = IN_ADD_ON_ID
        and faat.ADDON_TYPE_ID = faa.ADDON_TYPE;
        
    IF IN_VENDOR_INFO_FLAG = 'true' THEN
       OPEN out_vendor_cur for
            select fava.VENDOR_ID, fava.ADDON_ID, fava.VENDOR_SKU, fava.VENDOR_COST, fava.ACTIVE_FLAG as VENDOR_ACTIVE_FLAG
            from FTD_APPS.VENDOR_ADDON fava
             where fava.ADDON_ID = IN_ADD_ON_ID
             order by fava.VENDOR_ID;
    ELSE
      OPEN out_vendor_cur for
            select null from dual; 
    END IF;
    
    IF IN_PRODUCT_INFO_FLAG = 'true' THEN
       OPEN out_product_cur for
            select fapa.PRODUCT_ID, fapa.ADDON_ID, fapa.DISPLAY_SEQ_NUM, fapa.ACTIVE_FLAG, fapa.MAX_QTY
            from PRODUCT_ADDON fapa
             where fapa.ADDON_ID = IN_ADD_ON_ID
             and fapa.ACTIVE_FLAG = 'Y';
    ELSE
      OPEN out_product_cur for
            select null from dual; 
    END IF;
    
END GET_ADDON_BY_ADDON_ID;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all of the addon information  
        for an inputted addon id

Input:
        IN_ADD_ON_ID : The addon Id to search for
Output:
        OUT_CUR                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_ALL_ADDON_CUR
(
IN_VENDOR_INFO_FLAG               IN  VARCHAR2,
OUT_CUR                           OUT TYPES.REF_CURSOR,
OUT_VENDOR_CUR                    OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select faa.ADDON_ID, faa.ADDON_TYPE, faa.DESCRIPTION AS ADD_ON_DESCRIPTION, faa.PRICE, faa.ADDON_WEIGHT, faa.ADDON_TEXT, faa.UNSPSC,faa.PRODUCT_ID, faa.ACTIVE_FLAG AS ADD_ON_ACTIVE_FLAG, faa.DEFAULT_PER_TYPE_FLAG, faa.DISPLAY_PRICE_FLAG, faat.ADDON_TYPE_ID, faat.DESCRIPTION AS ADD_ON_TYPE_DESCRIPTION, faat.PRODUCT_FEED_FLAg ,
		
		(  CASE 
        WHEN (faa.FLORIST_ADDON_FLAG = 'Y' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'both'   
        WHEN (faa.FLORIST_ADDON_FLAG = 'N' AND faa.VENDOR_ADDON_FLAG = 'Y')
        THEN 'dropship'
        ELSE 'florist'
        END
        ) as ADDON_DELIVERY_TYPE, 
		faa.PQUAD_ACCESSORY_ID, faa.IS_FTD_WEST_ADDON -- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on and are displayed on Addon Dashboard page.
        from ADDON faa, ADDON_TYPE faat
        where faat.ADDON_TYPE_ID = faa.ADDON_TYPE
        order by faat.DESCRIPTION, faa.ACTIVE_FLAG desc, faa.DESCRIPTION;
        
    IF IN_VENDOR_INFO_FLAG = 'true' THEN
       OPEN out_vendor_cur for
            select fava.VENDOR_ID, fava.ADDON_ID, fava.VENDOR_SKU, fava.VENDOR_COST
            from FTD_APPS.VENDOR_ADDON fava
             where fava.ACTIVE_FLAG = 'Y'
             order by fava.VENDOR_ID;
    ELSE
      OPEN out_vendor_cur for
            select null from dual; 
    END IF;
        
END GET_ALL_ADDON_CUR;

FUNCTION ORDER_ADD_ON_AVAILABILITY
(
IN_ORDER_DETAIL_ID   IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
This function returns whether or not the add ons associated to an order are still 
available

Input:
	order_detail_id

Output:
	Y/N add ons associated to an order are still available

-----------------------------------------------------------------------------*/
--variables
v_order_add_on_count            number(4);
v_total_order_add_on_count      number(4);
v_available_add_on_count        number(4);
v_available_banner_count        number(4);
v_available_card_count          number(4);
v_add_ons_available             varchar2(1) := 'Y';

BEGIN
   
	--get add on count from order, if 0, return true
	select count(*) into v_order_add_on_count
	from clean.order_add_ons oao
	where oao.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;
	
	IF v_order_add_on_count > 0 THEN
		--get count of available add ons excluding funeral banner and greeting cards
		select count(*) into v_available_add_on_count
		from ftd_apps.ADDON faa, ftd_apps.PRODUCT_ADDON fapa, FTD_APPS.PRODUCT_MASTER pm, clean.order_details od, CLEAN.ORDER_ADD_ONS oao
		where od.order_detail_id = IN_ORDER_DETAIL_ID
		and od.ORDER_DETAIL_ID = oao.ORDER_DETAIL_ID
		and faa.addon_type not in ('3','4')
		and faa.addon_id = oao.ADD_ON_CODE
		and faa.addon_id = fapa.ADDON_ID
		and fapa.product_id = od.PRODUCT_ID
		and pm.product_id = fapa.product_id
		and faa.active_flag = 'Y'
		and fapa.ACTIVE_FLAG = 'Y'
		and (pm.SHIP_METHOD_FLORIST = 'Y' or exists ( 
		    select 1 
		    from ftd_apps.VENDOR_ADDON fava 
		    where fava.ADDON_ID = faa.addon_id
		    and fava.ACTIVE_FLAG = 'Y'  
		    )
		);
		
		--get count for available funeral banner
		select count(*) into v_available_banner_count
		from ftd_apps.ADDON faa, FTD_APPS.PRODUCT_MASTER pm, clean.order_details od, CLEAN.ORDER_ADD_ONS oao
		where od.order_detail_id = IN_ORDER_DETAIL_ID
		and od.ORDER_DETAIL_ID = oao.ORDER_DETAIL_ID
		and faa.addon_id = oao.ADD_ON_CODE
                and faa.addon_type = '3'
		and pm.product_id = od.product_id
		and faa.active_flag = 'Y'
		and pm.ADD_ON_FUNERAL_FLAG = 'Y'
		and pm.SHIP_METHOD_FLORIST = 'Y';
		
		--get count for available card add ons
		select count(*) into v_available_card_count
		from ftd_apps.ADDON faa, FTD_APPS.PRODUCT_MASTER pm, clean.order_details od, CLEAN.ORDER_ADD_ONS oao
		where od.order_detail_id = IN_ORDER_DETAIL_ID
		and od.ORDER_DETAIL_ID = oao.ORDER_DETAIL_ID
		and faa.addon_id = oao.ADD_ON_CODE
                and faa.addon_type = '4'
		and pm.product_id = od.product_id
		and faa.active_flag = 'Y'
		and pm.ADD_ON_CARDS_FLAG = 'Y'
		and pm.SHIP_METHOD_FLORIST = 'Y';
		
		--add all add on counts together
		v_total_order_add_on_count := v_available_add_on_count + v_available_banner_count + v_available_card_count;
	
		--compare add ons associated to order against current add on availability
		--return 'Y' if all add ons are still available, return 'N' if any of the add ons
		--are unavailable
		IF v_total_order_add_on_count < v_order_add_on_count THEN
			v_add_ons_available := 'N';
		END IF;
	END IF;
	
  RETURN v_add_ons_available;

END ORDER_ADD_ON_AVAILABILITY;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all of the add-on types

Output:
        OUT_CUR                  Cursor containing all add-on types

-----------------------------------------------------------------------------*/
PROCEDURE GET_ALL_ADD_ON_TYPE_CUR
(
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN

    OPEN out_cur for      
        select faat.ADDON_TYPE_ID, faat.DESCRIPTION, faat.PRODUCT_FEED_FLAG
          from addon_type faat
          order by faat.DESCRIPTION;
          
END GET_ALL_ADD_ON_TYPE_CUR;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating an add-on

Output:
        OUT_STATUS                  Error status
        OUT_MESSAGE                 Error message (if applicable)

-----------------------------------------------------------------------------*/
PROCEDURE UPDATE_ADDON (
IN_ADDON_ID               in ADDON.ADDON_ID%TYPE,
IN_ADDON_TYPE             in ADDON.ADDON_TYPE%TYPE,
IN_DESCRIPTION            in ADDON.DESCRIPTION%TYPE,
IN_PRICE                  in ADDON.PRICE%TYPE,
IN_ADDON_TEXT             in ADDON.ADDON_TEXT%TYPE,
IN_UNSPSC                 in ADDON.UNSPSC%TYPE,
IN_PRODUCT_ID             in ADDON.PRODUCT_ID%TYPE,
IN_ACTIVE_FLAG            in ADDON.ACTIVE_FLAG%TYPE,
IN_DEFAULT_PER_TYPE_FLAG  in ADDON.DEFAULT_PER_TYPE_FLAG%TYPE,
IN_DISPLAY_PRICE_FLAG     in ADDON.DISPLAY_PRICE_FLAG%TYPE,
IN_ADDON_WEIGHT           in ADDON.ADDON_WEIGHT%TYPE,
IN_DELIVERY_TYPE		      in VARCHAR2,
IN_CSR_ID                 in ADDON.UPDATED_BY%TYPE,
IN_OCCASIONS              in VARCHAR2,-- Changes for Apollo.Q3.2015 - Addon-Occasion association
IN_PQUAD_ACCESSORY_ID	    in ADDON.PQUAD_ACCESSORY_ID%TYPE, -- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
IN_IS_FTD_WEST_ADDON	    in ADDON.IS_FTD_WEST_ADDON%TYPE,  -- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
OUT_STATUS                OUT VARCHAR2,
OUT_MESSAGE               OUT VARCHAR2
)
AS
Florist_addon             varchar2(1) := 'Y';
vendor_addon			  varchar2(1) := 'Y';
v_occasions        		  dbms_sql.varchar2s;
BEGIN
    IF IN_DEFAULT_PER_TYPE_FLAG = 'Y' THEN
      UPDATE ADDON
       SET DEFAULT_PER_TYPE_FLAG = 'N',
       UPDATED_ON = sysdate,
       UPDATED_BY = IN_CSR_ID
       WHERE 
           DEFAULT_PER_TYPE_FLAG = 'Y'
           AND ADDON_TYPE = IN_ADDON_TYPE;
    END IF;
       

	IF IN_DELIVERY_TYPE = 'florist' THEN 
	Florist_addon := 'Y';
	vendor_addon := 'N' ;
	
	ELSIF IN_DELIVERY_TYPE = 'dropship' THEN 
	Florist_addon := 'N';
	vendor_addon := 'Y' ;
	
	ELSIF IN_DELIVERY_TYPE = 'both' THEN 
 	Florist_addon := 'Y';
	vendor_addon := 'Y' ;
  
  END IF;

    UPDATE ADDON
      SET ADDON_TYPE              = IN_ADDON_TYPE,
          DESCRIPTION             = IN_DESCRIPTION,
          PRICE                   = IN_PRICE,
          ADDON_TEXT              = IN_ADDON_TEXT,
          UNSPSC                  = IN_UNSPSC,
          PRODUCT_ID              = IN_PRODUCT_ID,
          ACTIVE_FLAG             = IN_ACTIVE_FLAG,
          DEFAULT_PER_TYPE_FLAG   = IN_DEFAULT_PER_TYPE_FLAG,
          DISPLAY_PRICE_FLAG      = IN_DISPLAY_PRICE_FLAG,
          ADDON_WEIGHT            = IN_ADDON_WEIGHT,
          FLORIST_ADDON_FLAG      = Florist_addon,
          VENDOR_ADDON_FLAG       = vendor_addon ,
		  PQUAD_ACCESSORY_ID      = IN_PQUAD_ACCESSORY_ID,-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
          IS_FTD_WEST_ADDON       = IN_IS_FTD_WEST_ADDON,-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
          UPDATED_ON = sysdate,
          UPDATED_BY = IN_CSR_ID
       WHERE ADDON_ID = IN_ADDON_ID;
     
     IF SQL%NOTFOUND THEN
       INSERT INTO ADDON (
          ADDON_ID,
          ADDON_TYPE,
          DESCRIPTION,
          PRICE,
          ADDON_TEXT,
          UNSPSC,
          PRODUCT_ID,
          ACTIVE_FLAG,
          DEFAULT_PER_TYPE_FLAG,
          DISPLAY_PRICE_FLAG,
          ADDON_WEIGHT,
          CREATED_ON,
          CREATED_BY,
          FLORIST_ADDON_FLAG,      
          VENDOR_ADDON_FLAG ,       
		  PQUAD_ACCESSORY_ID,-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
          IS_FTD_WEST_ADDON,-- DI-4: Add-on Dashboard Enhancements: Added 2 new attributes to add-on : PQUAD_ACCESSORY_ID, IS_FTD_WEST_ADDON.
          UPDATED_ON,
          UPDATED_BY
          )
        VALUES (
          IN_ADDON_ID,
          IN_ADDON_TYPE,
          IN_DESCRIPTION,
          IN_PRICE,
          IN_ADDON_TEXT,
          IN_UNSPSC,
          IN_PRODUCT_ID,
          IN_ACTIVE_FLAG,
          IN_DEFAULT_PER_TYPE_FLAG,
          IN_DISPLAY_PRICE_FLAG,
          IN_ADDON_WEIGHT,
          sysdate,
          IN_CSR_ID,
          Florist_addon,
          vendor_addon,
		  IN_PQUAD_ACCESSORY_ID,
          IN_IS_FTD_WEST_ADDON,
          sysdate,
          IN_CSR_ID
        );
     END IF;  
	 
	-- Changes for Apollo.Q3.2015 - Addon-Occasion association
	DELETE FROM ftd_apps.occasion_addon WHERE addon_id = IN_ADDON_ID;
  
	IF(IN_OCCASIONS IS NOT NULL) THEN
		v_occasions   := ftd_apps.dcsn_fx_maint_pkg.tokenizer(IN_OCCASIONS, ',');
    
		FOR i IN v_occasions.FIRST..v_occasions.LAST
		LOOP
			INSERT INTO occasion_addon (OCCASION_ID,ADDON_ID)VALUES(to_number(v_occasions(i)), IN_ADDON_ID);
		END LOOP;
	END IF;  
     
     out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ADDON;

FUNCTION VENDOR_ADD_ON_AVAILABILITY
(
IN_VENUS_ID   IN VARCHAR2,
IN_VENDOR_ID  IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
This function returns whether or not the vendor passed in has all of the add ons associated to the venus order available

Input:
	venus_id
	vendor_id

Output:
	Y/N this vendor has all add ons associated to venus order available

-----------------------------------------------------------------------------*/
--variables
v_venus_order_add_on_count      number(4);
v_available_add_on_count        number(4);
v_vendor_add_ons_available      varchar2(1) := 'Y';

BEGIN
   
	--get add on count from venus order, if 0, return true
	select count(*) into v_venus_order_add_on_count
	from venus.venus v, venus.venus_add_ons vao
	where v.VENUS_ID = IN_VENUS_ID
	and v.VENUS_ID = vao.VENUS_ID; 
	
	IF v_venus_order_add_on_count > 0 THEN
		--get count of available add ons for the vendor passed in
		select count(*) into v_available_add_on_count
		from venus.venus v, venus.venus_add_ons vao, ftd_apps.vendor_addon va
		where v.VENUS_ID = IN_VENUS_ID
		and v.VENUS_ID = vao.VENUS_ID
		and va.vendor_id = IN_VENDOR_ID
		and vao.ADDON_ID = va.ADDON_ID
		and va.ACTIVE_FLAG = 'Y';
	
		--compare add ons associated to venus order against current vendor add on availability
		--return 'Y' if all add ons are still available, return 'N' if any of the add ons
		--are unavailable
		IF v_available_add_on_count < v_venus_order_add_on_count THEN
			v_vendor_add_ons_available := 'N';
		END IF;
	END IF;
	
  RETURN v_vendor_add_ons_available;

END VENDOR_ADD_ON_AVAILABILITY;

/*-----------------------------------------------------------------------------
Description:
        This procedure is will return the product ids where the addon inputted
        is the only one of that type that is assigned

Output:
        IN_ADD_ON_ID             The add-on id to search for

Output:
        OUT_CUR                  Cursor containing the product ids

-----------------------------------------------------------------------------*/
PROCEDURE GET_IS_ONLY_OF_TYPE_ASSIGNED
(
IN_ADD_ON_ID                          IN ADDON.ADDON_ID%TYPE,
OUT_CUR                               OUT TYPES.REF_CURSOR
)
AS
BEGIN
  OPEN out_cur for
   select fapa.PRODUCT_ID
    from PRODUCT_ADDON fapa, ADDON faa
     where fapa.ADDON_ID = IN_ADD_ON_ID
     and fapa.ACTIVE_FLAG = 'Y'
     and faa.ACTIVE_FLAG = 'Y'
     and faa.ADDON_ID = fapa.ADDON_ID
     and (select count (1)  
           from ADDON faa2, PRODUCT_ADDON fapa2
           where faa2.ADDON_TYPE = faa.ADDON_TYPE
           and fapa2.PRODUCT_ID = fapa.PRODUCT_ID
           and fapa2.ADDON_ID = faa2.ADDON_ID
           and fapa2.ACTIVE_FLAG = 'Y'
           and faa2.ACTIVE_FLAG = 'Y'
            ) = 1 ;

END GET_IS_ONLY_OF_TYPE_ASSIGNED;

/*-----------------------------------------------------------------------------
Description:
        This procedure is will return the vendor ids where the addon given is not available at other vendors
        and assigned to a product that is drop-ship

Output:
        IN_ADD_ON_ID             The add-on id to search for
        IN_VENDOR_ID             The vendor id to search for

Output:
        OUT_CUR                  Cursor containing the product ids

-----------------------------------------------------------------------------*/
PROCEDURE GET_IS_LAST_VENDOR_ASSIGNED
(
IN_VENDOR_ID                          IN VENDOR_ADDON.VENDOR_ID%TYPE,
IN_ADD_ON_ID                          IN ADDON.ADDON_ID%TYPE,
OUT_CUR                               OUT TYPES.REF_CURSOR
)
AS
BEGIN
  OPEN out_cur for
    select va.VENDOR_ID, pm.PRODUCT_ID, pa.ADDON_ID
      from ftd_apps.vendor_addon va, ftd_apps.product_master pm, ftd_apps.product_addon pa
     where va.vendor_id = IN_VENDOR_ID
       and va.addon_id = pa.addon_id
       and pa.addon_id = IN_ADD_ON_ID
       and va.active_flag = 'Y'
       and pa.active_flag = 'Y'
       and pa.product_id = pm.product_id
       and pm.ship_method_carrier = 'Y'
       and 
           (
            select count (1)
              from ftd_apps.vendor_addon va2, ftd_apps.product_master pm2, ftd_apps.product_addon pa2
             where va2.vendor_id != IN_VENDOR_ID
               and va2.addon_id = pa2.addon_id
               and pa2.addon_id = IN_ADD_ON_ID
               and va2.active_flag = 'Y'
               and pa.active_flag = 'Y'
               and pa2.product_id = pm2.product_id
               and pm2.ship_method_carrier = 'Y'
           ) = 0;
END GET_IS_LAST_VENDOR_ASSIGNED;

/*-----------------------------------------------------------------------------
Apollo.Q3.2015 changes.
Description:
This procedure retrieves all the active occasions.
Output:
OUT_CUR                  Cursor containing all active occasions.
-----------------------------------------------------------------------------*/
PROCEDURE GET_ALL_OCCASIONS_CUR
(
    OUT_CUR OUT TYPES.REF_CURSOR 
)
AS
BEGIN
  OPEN out_cur FOR 
  SELECT	'OCC-'||occasion_id occasion_id, description
  FROM 		ftd_apps.OCCASION 
  --WHERE 	active = 'Y' 
  order by description;
END GET_ALL_OCCASIONS_CUR;

END ADD_ON_PKG;
.
/
