CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_SEARCH_PRIORITY_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_SEARCH_PRIORITY_LIST
-- Type:    Function
-- Syntax:  SP_GET_SEARCH_PRIORITY_LIST ()
-- Returns: ref_cursor for
--          PRIORITY_ID        VARCHAR2(5)
--          PRIORITY_DESCRIPTION  VARCHAR(25 )
--
-- Description:   Queries the PRODUCT_SEARCH_PRIORITIES table and
--                returns a ref cursor for resulting row.
--
--======================================================================

AS
    priority_cursor types.ref_cursor;
BEGIN
    OPEN priority_cursor FOR
        SELECT PRIORITY_ID          as "priorityId",
               PRIORITY_DESCRIPTION as "priorityDescription"
        FROM PRODUCT_SEARCH_PRIORITIES ORDER BY PRIORITY_ID;

    RETURN priority_cursor;
END
;
.
/
