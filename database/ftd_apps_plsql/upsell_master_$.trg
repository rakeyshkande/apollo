CREATE OR REPLACE TRIGGER ftd_apps.upsell_master_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.upsell_master 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into ftd_apps.upsell_master$
         (UPSELL_MASTER_ID,    
          UPSELL_NAME,         
          UPSELL_DESCRIPTION,  
          UPSELL_STATUS,       
          SEARCH_PRIORITY,     
          CORPORATE_SITE,      
          GBB_POPOVER_FLAG,    
          GBB_TITLE_TXT,       
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:NEW.UPSELL_MASTER_ID,    
          :NEW.UPSELL_NAME,         
          :NEW.UPSELL_DESCRIPTION,  
          :NEW.UPSELL_STATUS,       
          :NEW.SEARCH_PRIORITY,     
          :NEW.CORPORATE_SITE,      
          :NEW.GBB_POPOVER_FLAG,    
          :NEW.GBB_TITLE_TXT,       
          'INS',          
          v_current_timestamp          
         );
   
   ELSIF UPDATING  THEN

      insert into ftd_apps.upsell_master$
         (UPSELL_MASTER_ID,    
          UPSELL_NAME,         
          UPSELL_DESCRIPTION,  
          UPSELL_STATUS,       
          SEARCH_PRIORITY,     
          CORPORATE_SITE,      
          GBB_POPOVER_FLAG,    
          GBB_TITLE_TXT,       
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:OLD.UPSELL_MASTER_ID,    
          :OLD.UPSELL_NAME,         
          :OLD.UPSELL_DESCRIPTION,  
          :OLD.UPSELL_STATUS,       
          :OLD.SEARCH_PRIORITY,     
          :OLD.CORPORATE_SITE,      
          :OLD.GBB_POPOVER_FLAG,    
          :OLD.GBB_TITLE_TXT,       
          'UPD_OLD',          
          v_current_timestamp          
         );

      insert into ftd_apps.upsell_master$
         (UPSELL_MASTER_ID,    
          UPSELL_NAME,         
          UPSELL_DESCRIPTION,  
          UPSELL_STATUS,       
          SEARCH_PRIORITY,     
          CORPORATE_SITE,      
          GBB_POPOVER_FLAG,    
          GBB_TITLE_TXT,       
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:NEW.UPSELL_MASTER_ID,    
          :NEW.UPSELL_NAME,         
          :NEW.UPSELL_DESCRIPTION,  
          :NEW.UPSELL_STATUS,       
          :NEW.SEARCH_PRIORITY,     
          :NEW.CORPORATE_SITE,      
          :NEW.GBB_POPOVER_FLAG,    
          :NEW.GBB_TITLE_TXT,       
          'UPD_NEW',
          v_current_timestamp          
         );

   ELSIF DELETING  THEN

      insert into ftd_apps.upsell_master$
         (UPSELL_MASTER_ID,    
          UPSELL_NAME,         
          UPSELL_DESCRIPTION,  
          UPSELL_STATUS,       
          SEARCH_PRIORITY,     
          CORPORATE_SITE,      
          GBB_POPOVER_FLAG,    
          GBB_TITLE_TXT,       
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:OLD.UPSELL_MASTER_ID,    
          :OLD.UPSELL_NAME,         
          :OLD.UPSELL_DESCRIPTION,  
          :OLD.UPSELL_STATUS,       
          :OLD.SEARCH_PRIORITY,     
          :OLD.CORPORATE_SITE,      
          :OLD.GBB_POPOVER_FLAG,    
          :OLD.GBB_TITLE_TXT,       
          'DEL',          
          v_current_timestamp          
         );

   END IF;

END;
/
