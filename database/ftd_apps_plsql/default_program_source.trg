CREATE OR REPLACE
TRIGGER ftd_apps.default_program_source
AFTER INSERT ON ftd_apps.source_master
REFERENCING NEW AS newRow
FOR EACH ROW
BEGIN
   INSERT INTO frp.program_to_source_mapping (program_id, source_code)
   SELECT default_program_id,:newRow.source_code from ftd_apps.company_master
   WHERE  :newRow.company_id = company_id;
END default_program_source;
.
/
