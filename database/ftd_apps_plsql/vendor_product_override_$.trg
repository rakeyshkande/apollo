
CREATE OR REPLACE TRIGGER ftd_apps.vendor_product_override_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.vendor_product_override 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into ftd_apps.vendor_product_override$
         (VENDOR_ID,    
          PRODUCT_SUBCODE_ID,         
          DELIVERY_OVERRIDE_DATE,  
          CARRIER_ID,       
          SDS_SHIP_METHOD,     
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:NEW.VENDOR_ID,    
          :NEW.PRODUCT_SUBCODE_ID,         
          :NEW.DELIVERY_OVERRIDE_DATE,  
          :NEW.CARRIER_ID,       
          :NEW.SDS_SHIP_METHOD,     
          'INS',          
          v_current_timestamp          
         );
   
   ELSIF UPDATING  THEN

      insert into ftd_apps.vendor_product_override$
         (VENDOR_ID,    
          PRODUCT_SUBCODE_ID,         
          DELIVERY_OVERRIDE_DATE,  
          CARRIER_ID,       
          SDS_SHIP_METHOD,     
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:OLD.VENDOR_ID,    
          :OLD.PRODUCT_SUBCODE_ID,         
          :OLD.DELIVERY_OVERRIDE_DATE,  
          :OLD.CARRIER_ID,       
          :OLD.SDS_SHIP_METHOD,     
          'UPD_OLD',          
          v_current_timestamp          
         );

      insert into ftd_apps.vendor_product_override$
         (VENDOR_ID,    
          PRODUCT_SUBCODE_ID,         
          DELIVERY_OVERRIDE_DATE,  
          CARRIER_ID,       
          SDS_SHIP_METHOD,     
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:NEW.VENDOR_ID,    
          :NEW.PRODUCT_SUBCODE_ID,         
          :NEW.DELIVERY_OVERRIDE_DATE,  
          :NEW.CARRIER_ID,       
          :NEW.SDS_SHIP_METHOD,     
          'UPD_NEW',          
          v_current_timestamp          
         );

   ELSIF DELETING  THEN

      insert into ftd_apps.vendor_product_override$
         (VENDOR_ID,    
          PRODUCT_SUBCODE_ID,         
          DELIVERY_OVERRIDE_DATE,  
          CARRIER_ID,       
          SDS_SHIP_METHOD,     
          OPERATION$,          
          TIMESTAMP$          
         )
      values
         (:OLD.VENDOR_ID,    
          :OLD.PRODUCT_SUBCODE_ID,         
          :OLD.DELIVERY_OVERRIDE_DATE,  
          :OLD.CARRIER_ID,       
          :OLD.SDS_SHIP_METHOD,     
          'DEL',          
          v_current_timestamp          
         );

   END IF;

END;
/
