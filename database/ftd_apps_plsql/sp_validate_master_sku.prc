create or replace procedure ftd_apps.SP_VALIDATE_MASTER_SKU(
  IN_SKU IN VARCHAR2, 
  OUT_RESULT OUT VARCHAR2,
  OUT_STATUS OUT VARCHAR2, 
  OUT_MESSAGE OUT VARCHAR2
) 
is
--==============================================================================
--
-- Name:    SP_VALIDATE_MASTER_SKU
-- Type:    Procedure
-- Syntax:  SP_VALIDATE_MASTER_SKU ( <see args above> )
--
-- Description:   Determine if the sku is a valid master sku
--
--==============================================================================
  v_sku UPSELL_MASTER.UPSELL_MASTER_ID%type := UPPER(IN_SKU);
begin
  OUT_STATUS := 'Y';
  BEGIN
     SELECT 'Y' INTO OUT_RESULT
          FROM UPSELL_MASTER
          WHERE UPSELL_MASTER_ID = v_sku;

  EXCEPTION WHEN NO_DATA_FOUND THEN
     OUT_RESULT := 'N';
  WHEN OTHERS THEN
    OUT_RESULT := 'N';
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED ['||sqlcode||'] '||SUBSTR(sqlerrm,1,256);
  END;
end SP_VALIDATE_MASTER_SKU;
/
