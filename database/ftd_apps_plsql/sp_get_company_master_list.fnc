CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_COMPANY_MASTER_LIST RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_COMPANY_MASTER_LIST
-- Type:    Function
-- Syntax:  SP_GET_COMPANY_MASTER_LIST ()
-- Returns: ref_cursor for
--          Company_ID           VARCHAR2
--          Company_name  VARCHAR2
--
--
-- Description:   Queries the COMPANY_MASTER table and
--                returns a ref cursor for resulting row.
--
--============================================================
AS
    company_types_cursor types.ref_cursor;
BEGIN
    OPEN company_types_cursor FOR
        SELECT COMPANY_ID          as "companyId",
               COMPANY_NAME as "companyName"
          FROM COMPANY_MASTER
          WHERE (email_direct_mail_flag = 'Y' or order_flag = 'Y' or report_flag = 'Y')
          order by SORT_ORDER;

    RETURN company_types_cursor;
END
;
.
/
