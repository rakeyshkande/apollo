CREATE OR REPLACE 
PACKAGE BODY FTD_APPS.TEXT_SEARCH_QUERY_PKG AS

FUNCTION GET_PRODUCT_INDEX_NAMES
(
   IN_PRODUCT_ID          IN  PRODUCT_INDEX_XREF.PRODUCT_ID%TYPE
)
RETURN VARCHAR2
AS
--==========================================================================
--
-- Name:    GET_PRODUCT_INDEX_NAMES
-- Type:    Function
-- Syntax:  GET_PRODUCT_INDEX_NAMES ()
-- Returns: List of index names for the specified product
--
-- Description:   Queries the SHOPPING_INDEX_SRC_PROD table
--                and concatenates a list of the index names for the specified
--                product.
--                Used by OE_PRODUCT_INDEX_LIST.
--
--==========================================================================

    tmpindexList     VARCHAR2(32767) := NULL;
BEGIN

   -- The following will return multiple rows as a single string.
   --    Notes:  1)  The distinct removes repeat index names
   --            2)  The "sys_connect_by_path" does not like a delimiter that is
   --                   present in a value, so we could not use a space.  The
   --                   tilde (~) is used then later removed with the "replace".
   with data
      as
      (
         select rowvalues, row_number() over (order by rowvalues) rn, count(*) over () cnt
            from
            (
              select distinct sip.index_name rowvalues     --< This is the primary query!!
                from si_template_index_prod sip
               where sip.product_id = IN_PRODUCT_ID
            )
      )
     select ltrim(replace(sys_connect_by_path(rowvalues, '~'),'~',' ')) catvalues
      into tmpindexList
      from data
      where rn = cnt
      start with rn = 1
      connect by prior rn = rn-1;

    RETURN tmpindexList;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN tmpindexList;

END GET_PRODUCT_INDEX_NAMES;

FUNCTION GET_PRODUCT_INDEX_COUNTRIES
(
   IN_PRODUCT_ID          IN  PRODUCT_INDEX_XREF.PRODUCT_ID%TYPE
)
RETURN VARCHAR2
AS
--==========================================================================
--
-- Name:    GET_PRODUCT_INDEX_COUNTRIES
-- Type:    Function
-- Syntax:  GET_PRODUCT_INDEX_COUNTRIES ()
-- Returns: List of countries for the specified product
--
-- Description:   Queries the SHOPPING_INDEX_SRC and SHOPPING_INDEX_SRC_PROD tables
--                and concatenates a list of the country_id values for the specified
--                product.
--                Used by OE_PRODUCT_INDEX_LIST.
--
--==========================================================================

    tmpindexList     VARCHAR2(32767) := NULL;
BEGIN

   -- The following will return multiple rows as a single string.
   --    Notes:  1)  The distinct removes repeat index names
   --            2)  The "sys_connect_by_path" does not like a delimiter that is
   --                   present in a value, so we could not use a space.  The
   --                   tilde (~) is used then later removed with the "replace".
   with data
      as
      (
         select rowvalues, row_number() over (order by rowvalues) rn, count(*) over () cnt
            from
            (
              select distinct si.country_id  rowvalues     --< This is the primary query!!
                from si_template_index si, si_template_index_prod sip
               where sip.index_name = si.index_name
                 and sip.product_id = IN_PRODUCT_ID
                 and sip.index_name <> 'nonsearchitems'
            )
      )
     select ltrim(replace(sys_connect_by_path(rowvalues, '~'),'~',' ')) catvalues
      into tmpindexList
      from data
      where rn = cnt
      start with rn = 1
      connect by prior rn = rn-1;

    RETURN tmpindexList;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN tmpindexList;

END GET_PRODUCT_INDEX_COUNTRIES;


--==========================================================================
--
-- Name:    GET_PRODUCT_SOURCE_CODES
-- Type:    Function
-- Syntax:  GET_PRODUCT_SOURCE_CODES ()
-- Returns: List of source codes for the specified product as a CLOB
--
-- Description:   Queries the SHOPPING_INDEX_SRC_PROD table and concatenates 
--                a list of the source_codes values for the specified product.
--                Used by GET_ACTIVE_PRODUCTS.
--
--==========================================================================
FUNCTION GET_PRODUCT_SOURCE_CODES
(
   IN_PRODUCT_ID          IN  PRODUCT_MASTER.PRODUCT_ID%TYPE
) 
RETURN clob
AS

sourceCodesClob    CLOB;
len                BINARY_INTEGER;
v_source_code      si_source_template_ref.source_code%type;


cursor source_cur is 
       select distinct str.source_code 
         from si_source_template_ref str, si_template_index_prod stip 
        where stip.product_id = IN_PRODUCT_ID
          and str.si_template_id = stip.si_template_id;


BEGIN
  dbms_lob.createtemporary(sourceCodesClob, TRUE);
  dbms_lob.open(sourceCodesClob, dbms_lob.lob_readwrite);

  FOR source_rec IN source_cur
     LOOP 
        v_source_code := source_rec.source_code || ' ';
        len := length(v_source_code);

        dbms_lob.writeappend(sourceCodesClob, len, v_source_code);
        
    END LOOP;

    RETURN sourceCodesClob;

  EXCEPTION
     WHEN NO_DATA_FOUND THEN
        RETURN sourceCodesClob;

END GET_PRODUCT_SOURCE_CODES;


PROCEDURE GET_ACTIVE_SOURCE_CODES
(
   IN_LAST_USED_IN_DAYS  IN  NUMBER,
   SRC_CURSOR            IN OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the active source codes that have been used
        in the specified number of days.

Input:
        in_last_used_in_days  number

Output:
        src_cursor            cursor

-----------------------------------------------------------------------------*/

BEGIN

   OPEN src_cursor FOR
     SELECT sm.source_code,
            sm.description,
            sm.source_type,
            sm.company_id
       FROM source_master sm
      WHERE start_date <= sysdate
        AND (end_date >= sysdate or end_date is null)
        AND last_used_on_order_date >= (sysdate - IN_LAST_USED_IN_DAYS)
        AND iotw_flag = 'N';

END GET_ACTIVE_SOURCE_CODES;


PROCEDURE GET_ACTIVE_PRODUCT
(
   IN_PRODUCT_ID          IN VARCHAR2,
   PROD_CURSOR            IN OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves a single product and the details

Input:

Output:
        prod_cursor        cursor

-----------------------------------------------------------------------------*/
BEGIN

   OPEN prod_cursor FOR
     SELECT pm.product_id,
            pm.novator_id,
            pm.product_name,
            pm.novator_name,
            pm.long_description,
            pm.dominant_flowers,
            pm.keywords_txt as order_entry_keyword,
            pcx.company_id,
            pm.standard_price,
            pm.deluxe_price,
            pm.premium_price,
            pm.short_description,
            pm.ship_method_carrier,
            pm.ship_method_florist,
            pm.discount_allowed_flag,
            GET_PRODUCT_INDEX_NAMES(pm.product_id) as index_name,
            DECODE(pm.delivery_type,
                    'I',TEXT_SEARCH_QUERY_PKG.GET_PRODUCT_INDEX_COUNTRIES(pm.product_id),
                    TEXT_SEARCH_QUERY_PKG.GET_DOMESTIC_VENDOR_COUNTRIES(pm.ship_method_florist)
                  ) AS country_id,
            pm.popularity_order_cnt,
            cp.codification_id,
            pm.over_21,
            TEXT_SEARCH_QUERY_PKG.GET_PRODUCT_SOURCE_CODES(pm.product_id) as source_codes,
            pm.allow_free_shipping_flag
       FROM product_master pm,
            product_company_xref pcx,
            codified_products cp
      WHERE pm.status = 'A'
        AND pm.weboe_blocked = 'N'
        AND pm.product_id = pcx.product_id
        AND cp.product_id (+)= pm.product_id
        AND pm.product_id = IN_PRODUCT_ID;

END GET_ACTIVE_PRODUCT;

PROCEDURE GET_ACTIVE_PRODUCT_LIST
(
   PROD_CURSOR            IN OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the active products

Input:

Output:
        prod_cursor        cursor

-----------------------------------------------------------------------------*/
BEGIN

   OPEN prod_cursor FOR
     SELECT distinct pm.product_id
       FROM product_master pm,
            product_company_xref pcx
      WHERE pm.status = 'A'
        AND pm.weboe_blocked = 'N'
        AND pm.product_id = pcx.product_id;

END GET_ACTIVE_PRODUCT_LIST;


PROCEDURE GET_ACTIVE_IOTW
(
   IN_LAST_USED_IN_DAYS   IN     NUMBER,
   IOTW_CURSOR            IN OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves all of the active item of the week source codes and product
        information where the source code has been used
        in the specified number of days.

Input:
        in_last_used_in_days  number

Output:
        iotw_cursor        cursor

-----------------------------------------------------------------------------*/
BEGIN

   OPEN iotw_cursor FOR
     SELECT iotw.product_id,
            iotw.iotw_id,
            iotw.product_page_messaging_txt,
            pm.novator_id,
            pm.product_name,
            pm.long_description,
            pm.dominant_flowers,
            pm.standard_price,
            pcx.company_id,
            sm.source_code,
            sm.description as source_code_description,
            sm.source_type,
            sm.price_header_id as source_code_price_header_id,
            ism.description as iotw_source_code_description,
            ism.source_type as iotw_source_type,
            ism.source_code as iotw_source_code,
            ism.price_header_id as iotw_sc_price_header_id
     FROM joe.iotw iotw,
          ftd_apps.product_master pm,
          ftd_apps.source_master ism,
          ftd_apps.source_master sm,
          ftd_apps.product_company_xref pcx
     WHERE 1=1
     -- Joins
       AND iotw.product_id = pm.product_id
       AND iotw.source_code = sm.source_code
       AND iotw.iotw_source_code = ism.source_code
       AND pm.product_id = pcx.product_id
     -- Filters
       AND iotw.start_date <= SYSDATE
       AND ((iotw.end_date IS NULL) OR (iotw.end_date >= TRUNC(SYSDATE)))
       AND sm.start_date <= SYSDATE
       AND (sm.end_date >= TRUNC(SYSDATE) OR sm.end_date IS NULL)
       AND sm.last_used_on_order_date >= (SYSDATE - IN_LAST_USED_IN_DAYS)
       AND ism.start_date <= SYSDATE
       AND (ism.end_date >= TRUNC(SYSDATE) OR ism.end_date IS NULL)
       AND ism.last_used_on_order_date >= (SYSDATE - IN_LAST_USED_IN_DAYS)
       AND ism.iotw_flag = 'Y'
       AND pm.status = 'A'
       AND pm.weboe_blocked = 'N';

END GET_ACTIVE_IOTW;

FUNCTION GET_DOMESTIC_VENDOR_COUNTRIES
(
   IN_SHIP_METHOD_FLORIST  IN  PRODUCT_MASTER.SHIP_METHOD_FLORIST%TYPE
)
RETURN VARCHAR2
AS
--==========================================================================
--
-- Name:    GET_DOMESTIC_VENDOR_COUNTRIES
-- Type:    Function
-- Syntax:  GET_DOMESTIC_VENDOR_COUNTRIES ()
-- Returns: "US" if drop ship is set to N for all Canadian state_master entries
--          "US CA" if drop ship to Canada is allowed or product is florist delivered
--
--==========================================================================

   OUT_COUNTRIES           VARCHAR2(4000) := NULL;
   V_TEMP_COUNT            VARCHAR2(4000);

BEGIN

   IF (IN_SHIP_METHOD_FLORIST = 'N') THEN

      SELECT count(1) INTO V_TEMP_COUNT
         FROM FTD_APPS.STATE_MASTER
         WHERE COUNTRY_CODE = 'CAN' AND DROP_SHIP_AVAILABLE_FLAG = 'N';

      IF (V_TEMP_COUNT > 0) THEN
         OUT_COUNTRIES := 'US';
      ELSE
         OUT_COUNTRIES := 'US CA';
      END IF;

   ELSE
      OUT_COUNTRIES := 'US CA';
   END IF;

   RETURN OUT_COUNTRIES;

END GET_DOMESTIC_VENDOR_COUNTRIES;

END TEXT_SEARCH_QUERY_PKG;
/
