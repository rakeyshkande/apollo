CREATE OR REPLACE TRIGGER ftd_apps.order_service_xsl_$
   AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.order_service_xsl
   REFERENCING OLD AS old NEW AS new FOR EACH ROW
DECLARE

v_timestamp      timestamp := systimestamp;

BEGIN
   
   IF INSERTING THEN
      INSERT INTO ftd_apps.order_service_xsl$ 
      (XSL_CODE,       
       XSL_DESC,       
       XSL_TXT,        
       CREATED_ON,     
       CREATED_BY,     
       UPDATED_ON,     
       UPDATED_BY,     
       OPERATION$,     
       TIMESTAMP$     
      ) 
      VALUES 
      (:NEW.XSL_CODE,       
       :NEW.XSL_DESC,       
       :NEW.XSL_TXT,        
       :NEW.CREATED_ON,     
       :NEW.CREATED_BY,     
       :NEW.UPDATED_ON,     
       :NEW.UPDATED_BY,     
       'INS',     
       v_timestamp
      );
         
   ELSIF UPDATING  THEN
   
      INSERT INTO ftd_apps.order_service_xsl$ 
      (XSL_CODE,       
       XSL_DESC,       
       XSL_TXT,        
       CREATED_ON,     
       CREATED_BY,     
       UPDATED_ON,     
       UPDATED_BY,     
       OPERATION$,     
       TIMESTAMP$     
      ) 
      VALUES 
      (:OLD.XSL_CODE,       
       :OLD.XSL_DESC,       
       :OLD.XSL_TXT,        
       :OLD.CREATED_ON,     
       :OLD.CREATED_BY,     
       :OLD.UPDATED_ON,     
       :OLD.UPDATED_BY,     
       'UPD_OLD',     
       v_timestamp
      );

      INSERT INTO ftd_apps.order_service_xsl$ 
      (XSL_CODE,       
       XSL_DESC,       
       XSL_TXT,        
       CREATED_ON,     
       CREATED_BY,     
       UPDATED_ON,     
       UPDATED_BY,     
       OPERATION$,     
       TIMESTAMP$     
      ) 
      VALUES 
      (:NEW.XSL_CODE,       
       :NEW.XSL_DESC,       
       :NEW.XSL_TXT,        
       :NEW.CREATED_ON,     
       :NEW.CREATED_BY,     
       :NEW.UPDATED_ON,     
       :NEW.UPDATED_BY,     
       'UPD_NEW',     
       v_timestamp
      );

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.order_service_xsl$ 
      (XSL_CODE,       
       XSL_DESC,       
       XSL_TXT,        
       CREATED_ON,     
       CREATED_BY,     
       UPDATED_ON,     
       UPDATED_BY,     
       OPERATION$,     
       TIMESTAMP$     
      ) 
      VALUES 
      (:OLD.XSL_CODE,       
       :OLD.XSL_DESC,       
       :OLD.XSL_TXT,        
       :OLD.CREATED_ON,     
       :OLD.CREATED_BY,     
       :OLD.UPDATED_ON,     
       :OLD.UPDATED_BY,     
       'DEL',     
       v_timestamp
      );

   END IF;

END;
/
