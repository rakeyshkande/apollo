create or replace trigger "FTD_APPS"."store_feeds_ai"
AFTER INSERT ON FTD_APPS.STORE_FEEDS
FOR EACH ROW
DECLARE
    c utl_smtp.connection;
    v_status    VARCHAR2(1000);
    v_message   VARCHAR2(1000);
    err_num NUMBER;
    err_msg VARCHAR2(100);
    v_error_status VARCHAR2(5) := 'N';
    
    v_source_code_eligible VARCHAR2(1); 
    v_feed_type_list VARCHAR2(1000);--'serviceFeeOverride, serviceFee, SHIPPING_KEY_FEED, SOURCE_FEED'
    v_id VARCHAR2(10);
    v_do VARCHAR2(5) := 'N';

    PROCEDURE send_email(v_feed_id IN VARCHAR2, v_feed_type IN VARCHAR2, v_msg IN VARCHAR2) AS
      BEGIN
        ops$oracle.mail_pkg.init_email(c, 'FEES_FEED_ALERTS',
                    'Pageable-Fees feeds insertion error' , v_status, v_message);

        utl_smtp.write_raw_data(c, utl_raw.cast_to_raw('Content-Type' || ': ' ||
                               'text/html; charset=iso-8859-1' || utl_tcp.CRLF));

        utl_smtp.write_data(c, '<body> <br> Error inserting the fee feeds into :: <b>FTD_APPS.FTD_FEES_FEEDS </b> ' );
        utl_smtp.write_data(c, '<br> Oracle Error code  :: '||v_msg||' <br> <br>' );

        utl_smtp.write_data(c, '<table border=1>'||
                      '<tr valign=top>'||
                      '<td align=center width=300><b> Feed Type </b></td>' ||
                      '<td align=center width=300><b> Feed ID</b></td>' ||
                      '</tr>');
        utl_smtp.write_data(c,'<tr valign=top>'||
                       '<td align=center width=300 style="color:blue;"> '|| v_feed_type  ||' </b> </td>' ||
                       '<td align=center width=300 style="color:blue;"> '|| v_feed_id    ||' </b> </td>' ||
                      '</tr> ');

        utl_smtp.write_data(c, '</table> </body>');

        ops$oracle.mail_pkg.close_email(c, v_status, v_message);
      END;
      
      PROCEDURE check_list(v_feed_type IN VARCHAR2, v_feed_xml IN CLOB, v_created_on IN DATE) AS
      BEGIN
      
        select value into v_feed_type_list from frp.global_parms where context = 'FEES_FEEDS_CONFIG' and name = 'FEED_TYPE_LIST';
        
        IF v_feed_type_list like '%'||:new.STORE_FEED_TYPE||'%' THEN
          v_do := 'Y';
          
--          IF v_feed_type = 'SOURCE_FEED' THEN
--            select extractValue(xmlType(v_feed_xml),'/sourceFeed/sourceCode/text()') into v_id from dual;
--            select case 
--                  when exists (select 1 
--                               from ftd_apps.fees_feeds_sources 
--                               where source_code = v_id
--                               and ACTIVE_STATUS = 'Y') 
--                  then 'Y' 
--                  else 'N' 
--              end as rec_exists into v_source_code_eligible
--            from dual;
--            IF  v_source_code_eligible is null OR v_source_code_eligible <> 'Y' THEN
--              v_do := 'N';
--            END IF;
--          END IF;
       END IF;

       IF '00:05' between to_char(v_created_on-(1/1440),'HH24:MI') and to_char(v_created_on+(1/1440),'HH24:MI') THEN
            v_do := 'N';
       END IF;

      END;
BEGIN

  BEGIN
  --Types of feeds: 'serviceFeeOverride', 'MORNING_DELIVERY', 'SHIPPING_KEY_FEED', 'SOURCE_FEED', 'serviceFee', 'FUEL'
      check_list(:new.STORE_FEED_TYPE, :new.STORE_FEED_XML, :new.CREATED_ON);  
    
      IF v_do ='Y' THEN
        INSERT INTO FTD_APPS.FEES_FEEDS(FEED_ID, FEED_XML, FEED_TYPE, CREATED_ON, UPDATED_ON)
        VALUES (:new.STORE_FEED_ID, :new.STORE_FEED_XML, :new.STORE_FEED_TYPE, :new.CREATED_ON, :new.UPDATED_ON);
      END IF; 
  EXCEPTION
     WHEN OTHERS THEN
      err_num := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 100);
      v_error_status := 'Y';
  END;
  IF v_error_status ='Y' THEN
      send_email(:new.STORE_FEED_ID, :new.STORE_FEED_TYPE, err_num||err_msg);
  END IF;

END;
/