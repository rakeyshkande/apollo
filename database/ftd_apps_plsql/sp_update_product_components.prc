CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_COMPONENTS (
 in_product_id in varchar2,
 in_component_sku_id in varchar2
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_COMPONENTS
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_COMPONENTS ( product_id in varchar2,
--                                         component_sku_id in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_COMPONENT_SKU.
--                If the product/sku combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_COMPONENT_SKU
  INSERT INTO PRODUCT_COMPONENT_SKU
      ( PRODUCT_ID,
	COMPONENT_SKU_ID,
	CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON)
  VALUES
	(in_product_id,
	in_component_sku_id,
	'SYS',
        sysdate,
        'SYS',
        sysdate);

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		  rollback;
end
;
.
/
