CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_SOURCECODE_DETAILS (inSourceCode IN VARCHAR2, inDeliveryDate IN VARCHAR2)
 RETURN Types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_SOURCECODE_DETAILS
-- Type:    Function
-- Syntax:  OE_GET_SOURCECODE_DETAILS (inSourceCode IN VARCHAR2, inDeliveryDate IN VARCHAR2)
-- Returns: ref_cursor for
--          marketingGroup              VARCHAR2(25)
--          validPayMethod              VARCHAR2(2)
--          paymentType                 VARCHAR2(1)
--          billingInfoPrompt           VARCHAR2(75)
--          billingInfoLogic            VARCHAR2(75)
--          partnerId                   VARCHAR2(5)
--          discountType                VARCHAR2(1)
--          domesticServiceCharge       NUMBER(5.2)
--          intlServiceCharge           NUMBER(5.2)
--
-- Description:   Queries the SOURCE table by SOURCE_CODE, returns the resulting
--                row's marketing group, discount type and service fees.
--
--==============================================================================
AS
    cur_cursor Types.ref_cursor;
BEGIN

  OPEN cur_cursor FOR
      SELECT s.MARKETING_GROUP AS "marketingGroup",
            s.VALID_PAY_METHOD AS "validPayMethod",
            pm.PAYMENT_TYPE AS "paymentType",
            s.BILLING_INFO_PROMPT AS "billingInfoPrompt",
			s.BILLING_INFO_LOGIC AS "billingInfoLogic",
            s.PARTNER_ID AS "partnerId",
			s.PRICING_CODE AS "pricingCode",
			s.DESCRIPTION AS "description",
			s.YELLOW_PAGES_CODE AS "yellowPagesCode",
			s.JCPENNEY_FLAG AS "jcPenneyFlag",
            p.DISCOUNT_TYPE AS "discountType",
            nvl(
                (select sfo.domestic_charge from ftd_apps.service_fee_override sfo
                    where sfo.snh_id = snh.snh_id
                    and sfo.override_date = to_date(inDeliveryDate, 'MM/DD/YYYY')
            ), snh.first_order_domestic) AS "domesticServiceCharge",
            nvl(
                (select sfo.international_charge from ftd_apps.service_fee_override sfo
                    where sfo.snh_id = snh.snh_id
                    and sfo.override_date = to_date(inDeliveryDate, 'MM/DD/YYYY')
            ), snh.first_order_international) AS "intlServiceCharge",
            s.SOURCE_TYPE AS "sourceType",
            pp.membership_data_required AS "membershipDataRequired"
      FROM SOURCE s, PRICE_HEADER_DETAILS p, SNH, PAYMENT_METHODS pm, PARTNER_PROGRAM pp
      WHERE s.SOURCE_CODE = inSourceCode
      AND s.SHIPPING_CODE = SNH.SNH_ID
      AND p.PRICE_HEADER_ID = s.PRICING_CODE
      AND s.VALID_PAY_METHOD = pm.PAYMENT_METHOD_ID (+)
      AND s.PARTNER_ID = pp.PROGRAM_NAME (+)
      AND p.MIN_DOLLAR_AMT = (
           SELECT MIN(pi.MIN_DOLLAR_AMT)
           FROM PRICE_HEADER_DETAILS pi
           WHERE pi.PRICE_HEADER_ID = p.PRICE_HEADER_ID );

  RETURN cur_cursor;
END;
.
/
