CREATE OR REPLACE
PACKAGE BODY ftd_apps.FLORIST_QUERY_PKG AS

FUNCTION FORMAT_FLORIST_ID
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for formatting the florist id for the
        search procudures.  Valid florist id formats are 999999, 99-9999,
        999999AA, or 99-9999AA.  If no suffix is given, the the wild card will
        be added.  If the suffix is given, then the suffix will be changed
        to upper case.

Input:
        florist_id                      varchar2

Output:
        formatted florist id ready for the search query.
-----------------------------------------------------------------------------*/

v_florist_id florist_master.florist_id%type;
BEGIN

case length (in_florist_id)
        when 6 then v_florist_id := substr (in_florist_id, 1, 2) || '-' || substr (in_florist_id, 3) || '%';
        when 7 then v_florist_id := in_florist_id || '%';
        when 8 then v_florist_id := substr (in_florist_id, 1, 2) || '-' || upper (substr (in_florist_id, 3));
        when 9 then v_florist_id := upper (in_florist_id);
        else null;
end case;

return v_florist_id;

END FORMAT_FLORIST_ID;


FUNCTION GET_COUNTRY_FROM_ZIP
(
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning the coutry code based on the
        format of the given zip.

Input:
        zip_code                        varchar2

Output:
        country code                    varchar2

-----------------------------------------------------------------------------*/
v_zip_code      florist_master.zip_code%type;
v_alpha_chars   varchar2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
v_alpha_sub     varchar2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
v_num_chars     varchar2(10) := '0123456789';
v_num_sub       varchar2(10) := '##########';
v_country_code  varchar2(2) := 'US';

BEGIN

v_zip_code := translate (in_zip_code, v_alpha_chars || v_num_chars, v_alpha_sub || v_num_sub);

if substr (v_zip_code, 1, 3) = '@#@' then
        v_country_code := 'CA';
end if;

RETURN v_country_code;

END GET_COUNTRY_FROM_ZIP;


FUNCTION FORMAT_ZIP_CODE
(
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for formatting the zip code for the
        search procudures.  Valid florist id formats are 99999, A9A 9A9,
        A9A-9A9, A9A9A9, or A9A.  If the zip code is a US zip code, nothing
        will be done.  If a Canadian zip is given, any space or dash will be
        removed and the zip will be changed to upper case.  If the Canadian
        zip is only 3 digits, the the wild card will be added.

        11/2004 - the requirement was changed to only store 3 character
        Canadian zips.  So if a 6 character zip code is passed in, the first
        3 characters will be used.

Input:
        zip_code                        varchar2

Output:
        formatted zip_code ready for the search query.
-----------------------------------------------------------------------------*/

v_zip_code      florist_master.zip_code%type;
v_number        number;

BEGIN

if get_country_from_zip (in_zip_code) = 'CA' then
        -- only use the first 3 characters of the Canadian zip
        v_zip_code := substr (UPPER (in_zip_code), 1, 3);

        -- PREVIOUS CODE to handle 3 or 6 character CA zips --
        --v_zip_code := replace (in_zip_code, ' ', '');
        --v_zip_code := replace (v_zip_code, '-', '');

        --if length(v_zip_code) = 3 then
        --        v_zip_code := upper (v_zip_code) || '%';
        --end if;

else
        v_zip_code := in_zip_code;
end if;

return v_zip_code;

END FORMAT_ZIP_CODE;


FUNCTION FORMAT_ZIP_CODE_II
(
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for formatting the zip code for the
        search procudures. 
        If the Zip code is Canadian, it returns only first three characters 
        if the zip code is non candian it returns first five digits.
        
Input:
        zip_code                        varchar2

Output:
        formatted zip_code ready for the search query.
-----------------------------------------------------------------------------*/

v_zip_code          florist_zips.zip_code%type;


BEGIN

-- Determine if the input zip code is Canadian
IF  get_country_from_zip (in_zip_code) = 'CA' then
   -- If Canadian, use only first three characters
   v_zip_code := SUBSTR(IN_ZIP_CODE, 1, 3);
ELSE
   -- Use only first five characters for U. S. zip codes
   v_zip_code := SUBSTR(IN_ZIP_CODE, 1, 5);
END IF;

return v_zip_code;

END FORMAT_ZIP_CODE_II;



FUNCTION COUNT_FLORISTS_IN_ZIP
(
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the number of all active
        florists that cover the given zip code.

Input:
        zip_code                varchar2

Output:
        count                   number

-----------------------------------------------------------------------------*/
v_zip_code      varchar2(10) := format_zip_code (in_zip_code);
v_count         number := 0;

CURSOR count_cur IS
        SELECT
                count (1)
        FROM    florist_master m
        JOIN    florist_zips z
        ON      m.florist_id = z.florist_id
        AND     m.status <> 'Inactive'
        WHERE   z.zip_code = v_zip_code     -- PREVIOUSLY like operator was used to handle 3 or 6 character CA zips
        AND     z.block_start_date is null
        AND     florist_query_pkg.is_florist_open_for_delivery(m.florist_id, trunc(sysdate)) = 'Y'
        AND     florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
        AND     florist_query_pkg.is_florist_blocked(m.florist_id, trunc(sysdate), null) = 'N'
        AND     florist_query_pkg.is_florist_suspended(m.florist_id, trunc(sysdate), null) = 'N'
        AND     m.record_type = 'R';

BEGIN

OPEN count_cur;
FETCH count_cur INTO v_count;
CLOSE count_cur;

RETURN v_count;

END COUNT_FLORISTS_IN_ZIP;


FUNCTION GET_FLORIST_WEIGHT
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning the florist's current
        weight.

Input:
        florist_id                      varchar2

Output:
        florist's current weight
-----------------------------------------------------------------------------*/

v_florist_weight                NUMBER;
v_adjusted_weight               NUMBER;
v_adjusted_weight_start_date    DATE;
v_adjusted_weight_end_date      DATE;

CURSOR weight_cur IS
        SELECT  florist_weight,
                adjusted_weight,
                adjusted_weight_start_date,
                adjusted_weight_end_date
        FROM    florist_master
        WHERE   florist_id = in_florist_id;
BEGIN

OPEN weight_cur;
FETCH weight_cur INTO v_florist_weight, v_adjusted_weight, v_adjusted_weight_start_date, v_adjusted_weight_end_date;
CLOSE weight_cur;

IF v_adjusted_weight_start_date <= sysdate and (v_adjusted_weight_end_date > sysdate or v_adjusted_weight_end_date is null) THEN
        v_florist_weight := v_adjusted_weight;
END IF;

IF v_florist_weight IS NULL THEN
        v_florist_weight := -1;
END IF;

return v_florist_weight;

END GET_FLORIST_WEIGHT;


PROCEDURE SEARCH_FLORIST_MASTER
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_CITY                         IN FLORIST_MASTER.CITY%TYPE,
IN_STATE                        IN FLORIST_MASTER.STATE%TYPE,
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE,
IN_PHONE_NUMBER                 IN FLORIST_MASTER.PHONE_NUMBER%TYPE,
IN_FLORIST_NAME                 IN FLORIST_MASTER.FLORIST_NAME%TYPE,
IN_STATUS                       IN FLORIST_MASTER.STATUS%TYPE,
IN_PRODUCT                      IN VARCHAR2,
IN_PRODUCT_SEARCH_TYPE          IN VARCHAR2,
IN_VENDOR_FLAG                  IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving florist based on the
        given search parameters.

        11/2004 - requirements were changed for Canadian zips (store 3 characters)
        If 6 characters are passed into the search, only the first 3 characters
        will be used for the search.

Input:
        florist_id                      varchar2 (formats: 999999, 99-9999, 999999AA, 99-9999AA)
        city                            varchar2 (city and state coverage)
        state                           varchar2
        zip_code                        varchar2 (coverage zips)
        phone_number                    varchar2
        florist_name                    varchar2

        status                          varchar2
        product                         varchar2 (novator_id, product_name, novator_name, codification_id)
        product_search_type             varchar2 (sku, product name, codification)

        vendor_flag                     varchar2 (Y - return florists and vendors, N - return only florists)
Output:
        cursor containing records from florist_master that match the
        given search parameters

-----------------------------------------------------------------------------*/
v_debug                 boolean := false; -- debug mode will display the sql string
v_sql                   varchar2 (16000);

-- select clause variables
v_base                  varchar2 (500) := 'select /*+ rule */ distinct ';
v_fields                varchar2 (1000) := 'm.florist_id, m.florist_name, m.zip_code, m.state, m.phone_number, florist_query_pkg.is_florist_open_sunday(m.florist_id) sunday_delivery_flag, decode (m.mercury_flag, ''M'', ''Y'', ''N'') mercury_flag, m.status, decode (m.super_florist_flag, ''Y'', ''Y'', ''N'') super_florist_flag, m.florist_weight, m.adjusted_weight, m.city, florist_query_pkg.get_florist_weight (m.florist_id) current_weight ';
v_fields_zip_order      varchar2 (500) := ', decode (florist_query_pkg.format_zip_code(m.zip_code), fz.zip_code, ''a'', ''z'') zip_order ';
v_from                  varchar2 (500) := 'from florist_master m ';

-- join clause variables
v_join                  varchar2 (4000);

v_florist_zip_join      varchar2 (500) := 'join florist_zips fz on m.florist_id = fz.florist_id ';
v_zip_code              varchar2 (500) := 'and upper(fz.zip_code) = ''' || format_zip_code (in_zip_code) || ''' ';  -- PREVIOUSLY like operator was used to handle 3 or 6 character CA zips

v_zip_code_join         varchar2 (500) := 'join zip_code z on fz.zip_code = z.zip_code_id ';
v_city                  varchar2 (500) := 'and upper(z.city) = ''' || upper (replace (in_city, chr(39), chr(39)||chr(39))) || ''' ';
v_state                 varchar2 (500) := 'and z.state_id = ''' || in_state || ''' ';

v_codification_join     varchar2 (500) := 'join florist_codifications c on m.florist_id = c.florist_id ';
v_codification_id       varchar2 (500) := 'and upper(c.codification_id) = ''' || upper(replace (in_product, chr(39), chr(39)||chr(39))) || ''' ';

v_codified_prod_join    varchar2 (500) := 'join codified_products cp on c.codification_id = cp.codification_id ';
v_products_join         varchar2 (500) := 'join product_master p on cp.product_id = p.product_id ';
v_product_name          varchar2 (500) := 'and (upper(p.product_name) like ''%' || upper(replace (in_product, chr(39), chr(39)||chr(39))) || '%'' or upper(p.novator_name) like ''%' || upper(replace (in_product, chr(39), chr(39)||chr(39))) || '%'') ';
v_product_sku           varchar2 (500) := 'and upper(p.novator_id) = ''' || upper (replace (in_product, chr(39), chr(39)||chr(39))) || ''' ';

-- where clause variables
v_where                 varchar2 (4000) := 'where m.record_type = ''R'' ';
v_florist_id            varchar2 (500) := 'and upper(m.florist_id) like ''' || format_florist_id (in_florist_id) || ''' ';
v_phone                 varchar2 (500) := 'and m.phone_number = ''' || replace (replace (replace (in_phone_number, '(', ''), ')', ''), '-', '') || ''' ';
v_florist_name          varchar2 (500) := 'and upper(m.florist_name) like ''%' || upper (replace (in_florist_name, chr(39), chr(39)||chr(39))) || '%'' ';
v_status                varchar2 (500) := 'and m.status = ''' || in_status || ''' ';
v_vendor                varchar2 (500) := 'and m.vendor_flag = ''N'' ';

-- order by variables
v_order_by              varchar2 (4000) := 'order by ';
v_order_florist_id      varchar2 (500) := 'm.florist_id ';
v_order_only_weight     varchar2 (500) := 'current_weight desc, m.florist_id ';
v_order_zip_weight      varchar2 (500) := 'zip_order, current_weight desc, m.florist_id ';


begin

-- build the join clause if the product criteria is given
if coalesce (in_zip_code, in_city, in_state) is not null then
        v_join := v_join || v_florist_zip_join;

        if in_zip_code is not null then
                v_join := v_join || v_zip_code;
        end if;

        if coalesce (in_city, in_state) is not null then
                v_join := v_join || v_zip_code_join;

                if in_city is not null then
                        v_join := v_join || v_city;
                end if;

                if in_state is not null then
                        v_join := v_join || v_state;
                end if;
        end if;
end if;

if in_product_search_type is not null then
        v_join := v_join || v_codification_join;

        if in_product_search_type = 'codification' then
                v_join := v_join || v_codification_id;
        else
                v_join := v_join || v_codified_prod_join || v_products_join;

                if in_product_search_type = 'sku' then
                        v_join := v_join || v_product_sku;
                else
                        v_join := v_join || v_product_name;
                end if;
        end if;
end if;

-- build where clause using the florist master fields
if in_florist_id is not null then
        v_where := v_where || v_florist_id;
end if;

if in_phone_number is not null then
        v_where := v_where || v_phone;
end if;

if in_florist_name is not null then
        v_where := v_where || v_florist_name;
end if;

if in_status is not null then
        v_where := v_where || v_status;
else
        v_where := v_where || 'and m.status <> ''Inactive'' ';
end if;

if in_vendor_flag = 'N' then
        v_where := v_where || v_vendor;
end if;

-- build order by clause
case
        when (in_zip_code is not null and coalesce (in_florist_id, in_city, in_state, in_phone_number, in_florist_name, in_status, in_product) is null)
          or (in_zip_code is not null and in_state is not null and coalesce (in_florist_id, in_city, in_phone_number, in_florist_name, in_status, in_product) is null)
          or (in_zip_code is not null and in_florist_name is not null and coalesce (in_florist_id, in_city, in_state, in_phone_number, in_status, in_product) is null)
                then
                        v_fields := v_fields || v_fields_zip_order;
                        v_order_by := v_order_by || v_order_zip_weight;
        when in_city is not null and in_state is not null and coalesce (in_florist_id, in_zip_code, in_phone_number, in_florist_name, in_status, in_product) is null
                then v_order_by := v_order_by || v_order_only_weight;
        else    v_order_by := v_order_by || v_order_florist_id;
end case;

-- build the entire sql statement
v_sql := v_base || v_fields || v_from || v_join || v_where || v_order_by;

-- execute the sql statement
if v_debug then
        open out_cur for select v_sql as debug_mode from dual;
else
        open out_cur for v_sql;
end if;


END SEARCH_FLORIST_MASTER;


PROCEDURE VIEW_CSR_VIEWED_FLORISTS
(
IN_CSR_ID                       IN CSR_VIEWED_IDS.CSR_ID%TYPE,
IN_COUNT                        IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the previously viewed
        florists returning the most recent number based on the given count.

Input:
        csr_id                          varchar2
        count                           number

Output:
        cursor containing records from florist_master that were previously
        viewed by the given csr

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  *
        FROM
        (
                select
                        m.florist_id,
                        m.florist_name,
                        m.zip_code,
                        m.state,
                        m.phone_number,
                        florist_query_pkg.is_florist_open_sunday(m.florist_id) sunday_delivery_flag,
                        decode (m.mercury_flag, 'M', 'Y', 'N') mercury_flag,
                        m.status,
                        decode (m.super_florist_flag, 'Y', 'Y', 'N') super_florist_flag,
                        m.florist_weight,
                        m.adjusted_weight,
                        m.city,
                        v.created_date
                from    florist_master m
                join    csr_viewed_ids v
                on      m.florist_id = v.entity_id
                where   v.csr_id = in_csr_id
                and     v.entity_type = 'FLORIST_MASTER'
                order by v.created_date desc
        ) m
        WHERE rownum <= in_count;

END VIEW_CSR_VIEWED_FLORISTS;


PROCEDURE VIEW_FLORIST_STATUS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all florist status codes.

Input:
        none

Output:
        cursor containing all records from florist_status

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                status
        FROM    florist_status;

END VIEW_FLORIST_STATUS;


PROCEDURE VIEW_FLORIST_MASTER
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given florist
        information.

Input:
        florist_id                      varchar2

Output:
        cursor containing the record from florist_master for the given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.florist_id,
                m.florist_name,
                m.address,
                m.city,
                m.state,
                m.zip_code,
                m.phone_number,
                m.florist_weight,
                decode (m.super_florist_flag, 'Y', 'Y', 'N') super_florist_flag,
                m.owners_name, -- #10
                m.status,
                m.fax_number,
                m.initial_weight,
                m.adjusted_weight,
                m.adjusted_weight_start_date,
                m.adjusted_weight_end_date,
                m.last_updated_by,
                m.last_updated_date,
                m.alt_phone_number,
                m.sending_rank,  -- #20
                m.orders_sent,
                (select count(1) from florist_zips z where z.florist_id = m.florist_id and z.block_start_date is null) zip_count,
                m.city_state_number,
                m.record_type,
                decode (m.mercury_flag, 'M', 'Y', 'N') mercury_flag,
                florist_query_pkg.is_florist_open_sunday(m.florist_id) sunday_delivery_flag,
                m.delivery_city,
                m.delivery_state,
                m.minimum_order_amount,
                m.territory,  -- #30
                m.email_address,
                decode (m.adjusted_weight_permenant_flag, 'Y', 'Y', 'N') adjusted_weight_permenant_flag,
                m.sunday_delivery_block_st_dt,
                m.sunday_delivery_block_end_dt,
                case
                  when m.sunday_delivery_block_st_dt is not null then 'Y'
                  else 'N'
                end sunday_delivery_block_flag,
                m.vendor_flag,
                m.suspend_override_flag,
                m.allow_message_forwarding_flag,
                m.is_fto_flag
        FROM    florist_master m
        WHERE   m.florist_id = in_florist_id;

END VIEW_FLORIST_MASTER;


PROCEDURE VIEW_ASSOCIATED_FLORISTS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the top level florist and
        parent florist information for the given florist.

Input:
        florist_id                      varchar2

Output:
        cursor containing the records from florist_master for the associated
        forists for given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.florist_id,
                m.florist_name,
                m.state,
                m.zip_code,
                m.phone_number,
                decode (m.mercury_flag, 'M', 'Y', 'N') mercury_flag,
                is_florist_open_sunday(m.florist_id) sunday_delivery_flag,
                m.status,
                (select count(1) from florist_zips z where z.florist_id = m.florist_id and z.block_start_date is null) zip_count
        FROM    florist_master m
        WHERE   m.top_level_florist_id =
        (       SELECT  top_level_florist_id
                FROM    florist_master
                WHERE   florist_id = in_florist_id
        )
        AND     m.florist_id <> in_florist_id
        AND     m.status <> 'Inactive'
        AND     m.record_type = 'R';

END VIEW_ASSOCIATED_FLORISTS;


PROCEDURE VIEW_FLORIST_CODIFICATIONS
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all codifications for the
        given florist.

Input:
        florist_id                      varchar2

Output:
        cursor containing the records from florist_codifications for the
        given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                c.florist_id,
                c.codification_id,
                c.block_start_date,
                c.block_end_date,
                case
                  when c.block_start_date is not null then 'Y'
                  else 'N'
                end blocked_flag,
                cm.description,
                cm.category_id
        FROM    florist_codifications c
        JOIN    codification_master cm
        ON      c.codification_id = cm.codification_id
        WHERE   c.florist_id = in_florist_id;

END VIEW_FLORIST_CODIFICATIONS;


PROCEDURE VIEW_FLORIST_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the zip code coverage
        of the given florist.

Input:
        florist_id                      varchar2

Output:
        cursor containing all records from florist_zips for the given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                z.florist_id,
                z.zip_code,
                z.block_start_date,
                z.block_end_date,
                case
                  when z.block_start_date is not null then 'Y'
                  else 'N'
                end blocked_flag,
                z.cutoff_time,
                count_florists_in_zip(z.zip_code) florist_count,
                (select city from zip_code zc where zc.zip_code_id = z.zip_code and rownum = 1) city,
                z.assignment_type
        FROM    florist_zips z
        WHERE   z.florist_id = in_florist_id;

END VIEW_FLORIST_ZIPS;


PROCEDURE VIEW_FLORIST_ZIPS_AVAIL
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Queries all florists associated with specified florist (determined 
        via top florist) and joins with EFOS city/state and zip info to determine 
        potentially available zips for specified florist.  
        Note that inactive florists are NOT included in query 
        (since NOT considered members).

        Also note that zip_master_florist_id is currently being used instead
        of top_level_florist_id (since it has been deemed unreliable).

Input:
        florist_id                      varchar2

Output:
        Cursor containing all potentially available zips for the given florist.

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT DISTINCT
                zip.zip_code_id, zip.city
        FROM    ftd_apps.florist_master fm0, ftd_apps.florist_master fm, 
                ftd_apps.efos_city_state ecs,
                ftd_apps.efos_state_codes es,
                ftd_apps.zip_code zip
        WHERE   fm0.florist_id = in_florist_id
        AND     fm0.zip_master_florist_id = fm.zip_master_florist_id
        AND     fm.status <> 'Inactive'
        AND     fm.city_state_number = ecs.city_state_code
        AND     substr(fm.city_state_number, 0, 2) = es.state_code
        AND     zip.city = rtrim(ecs.city_name)
        AND     zip.state_id = es.state_id;

END VIEW_FLORIST_ZIPS_AVAIL;


PROCEDURE VIEW_FLORIST_ZIPS_ASSIGNED
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Gets list of all zips currently assigned to other florists associated
        with specified florist (via top florist).
        In other words, this is list of zips that could potentially be assigned to 
        specified florist but cannot since currently assigned to other florist in group.

        Note that zip_master_florist_id is currently being used instead
        of top_level_florist_id (since it has been deemed unreliable).

Input:
        florist_id                      varchar2

Output:
        Cursor containing zips associated with florist but assigned to other florists

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT DISTINCT
                fz.zip_code, fz.assignment_type
        FROM    ftd_apps.florist_master fm0, ftd_apps.florist_master fm,  
                ftd_apps.florist_zips fz
        WHERE   fm0.florist_id = in_florist_id
        AND     fm0.zip_master_florist_id = fm.zip_master_florist_id
        AND     fm.status <> 'Inactive'
        AND     fm.florist_id = fz.florist_id
        AND     fm.florist_id != fm0.florist_id;

END VIEW_FLORIST_ZIPS_ASSIGNED;


PROCEDURE VIEW_FLORIST_ZIPS_DEAD
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Gets list of zips that just dropped to zero coverage after member load.

Input:

Output:
        Cursor containing zips and their associated top-level florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT DISTINCT
               fzr.zip_code, 
               fzr.top_level_florist_id
        FROM   ftd_apps.florist_zips_removal fzr
        WHERE  fzr.latest_flag = 'Y'
        AND    fzr.zip_code not in 
               (
                    SELECT fz.zip_code
                    FROM ftd_apps.florist_zips fz
                    WHERE fz.zip_code = fzr.zip_code
               );
               
END VIEW_FLORIST_ZIPS_DEAD;


PROCEDURE VIEW_CITY_ZIPS
(
IN_CITY                   IN VARCHAR2,
IN_STATE                  IN VARCHAR2,
OUT_CUR                   OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Gets list of all zips for specified city.

Input:
        city                      varchar2
        state                     varchar2

Output:
        Cursor containing zips

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT DISTINCT 
                zip_code_id
        FROM    ftd_apps.zip_code 
        WHERE   city = upper(in_city) 
        AND     state_id = in_state;

END VIEW_CITY_ZIPS;


PROCEDURE VIEW_QUEUE_FLORIST_ZIP
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
IN_ZIP                          IN FLORIST_ZIPS.ZIP_CODE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the zip code coverage
        of the given florist.

Input:
        florist_id                      varchar2

Output:
        cursor containing all records from florist_zips for the given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                z.florist_id,
                z.zip_code,
                z.block_start_date,
                z.block_end_date,
                case
                  when z.block_start_date is not null then 'Y'
                  else 'N'
                end blocked_flag,
                z.cutoff_time,
                count_florists_in_zip(z.zip_code) florist_count,
                (select city from zip_code zc where zc.zip_code_id = z.zip_code and rownum = 1) city
        FROM    florist_zips z
        WHERE   z.florist_id = in_florist_id
        AND     z.zip_code = in_zip;

END VIEW_QUEUE_FLORIST_ZIP;


PROCEDURE VIEW_FLORIST_COMMENTS
(
IN_FLORIST_ID                   IN FLORIST_COMMENTS.FLORIST_ID%TYPE,
IN_COMMENT_COUNT                IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the comments for the
        given florist.

Input:
        florist_id                      varchar2

Output:
        cursor containing all records from florist_comments for the given
        florist

-----------------------------------------------------------------------------*/
BEGIN

IF in_comment_count IS NULL THEN
        -- get all comments
        OPEN OUT_CUR FOR
                SELECT
                        florist_id,
                        to_char (comment_date, 'mm/dd/yyyy hh:mi:ssAM') comment_date_char,
                        comment_type,
                        florist_comment,
                        manager_only_flag,
                        created_by
                FROM    florist_comments
                WHERE   florist_id = in_florist_id
                ORDER BY comment_date desc, comment_type;
ELSE
        -- get the specified number of records
        OPEN OUT_CUR FOR
                SELECT *
                FROM
                (
                        SELECT
                                florist_id,
                                to_char (comment_date, 'mm/dd/yyyy hh:mi:ssAM') comment_date_char,
                                comment_type,
                                florist_comment,
                                manager_only_flag,
                                created_by
                        FROM    florist_comments
                        WHERE   florist_id = in_florist_id
                        ORDER BY comment_date desc, comment_type
                )
                WHERE rownum <= in_comment_count;
END IF;

END VIEW_FLORIST_COMMENTS;


PROCEDURE VIEW_FLORIST_WEIGHT_HISTORY
(
IN_FLORIST_ID                   IN FLORIST_WEIGHT_HISTORY.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retuning the florist weight history

Input:
        florist_id                      varchar2

Output:
        cursor containing all columns from florist_weight_history for the
        given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                florist_id,
                history_date,
                florist_weight,
                initial_weight,
                adjusted_weight,
                last_updated_by
        FROM    florist_weight_history
        WHERE   florist_id = in_florist_id
        ORDER BY history_date DESC;

END VIEW_FLORIST_WEIGHT_HISTORY;


PROCEDURE VIEW_FLORIST_INFO
(
IN_FLORIST_ID                   IN FLORIST_COMMENTS.FLORIST_ID%TYPE,
IN_CSR_ID                       IN CSR_VIEWED_IDS.CSR_ID%TYPE,
IN_COMMENT_COUNT                IN NUMBER,
OUT_FLORIST_MASTER              OUT TYPES.REF_CURSOR,
OUT_ASSOCIATED_FLORISTS         OUT TYPES.REF_CURSOR,
OUT_FLORIST_CODIFICATIONS       OUT TYPES.REF_CURSOR,
OUT_FLORIST_ZIPS                OUT TYPES.REF_CURSOR,
OUT_FLORIST_COMMENTS            OUT TYPES.REF_CURSOR,
OUT_FLORIST_ZIPS_AVAIL          OUT TYPES.REF_CURSOR,
OUT_FLORIST_ZIPS_ASSIGNED       OUT TYPES.REF_CURSOR,
OUT_SOURCE_INFO                 OUT TYPES.REF_CURSOR,
OUT_FLORIST_BLOCKS              OUT TYPES.REF_CURSOR,
OUT_FLORIST_SUSPENDS            OUT TYPES.REF_CURSOR,
OUT_FLORIST_CITIES              OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all information for the
        given florist.

Input:
        florist_id                      varchar2

Output:
        refcursors with from the following procedures:
        view_florist_master
        view_associated_florists
        view_florist_codifications
        view_florist_zips
        view_florist_comments
        view_florist_zips_avail
        view_florist_zips_assigned
	VIEW_FLORIST_SOURCE_INFO
-----------------------------------------------------------------------------*/

v_status        varchar2(1);
v_message       varchar2(1000);

BEGIN

-- mark the florist as viewed by the given csr
misc_pkg.insert_csr_viewed_ids (in_csr_id, 'FLORIST_MASTER', in_florist_id, v_status, v_message);

view_florist_master (in_florist_id, out_florist_master);
view_associated_florists (in_florist_id, out_associated_florists);
view_florist_codifications (in_florist_id, out_florist_codifications);
view_florist_comments (in_florist_id, in_comment_count, out_florist_comments);
view_florist_zips (in_florist_id, out_florist_zips);
view_florist_zips_avail (in_florist_id, out_florist_zips_avail);
view_florist_zips_assigned (in_florist_id, out_florist_zips_assigned);
view_florist_source_info (in_florist_id, out_source_info);
view_florist_blocks (in_florist_id, out_florist_blocks);
view_florist_suspends (in_florist_id, out_florist_suspends);
view_florist_cities (in_florist_id, out_florist_cities);

END VIEW_FLORIST_INFO;


PROCEDURE VIEW_FLORISTS_BY_ZIPS
(
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
IN_DELIVERY_DATE                IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the all active florists
        that cover the given zip code.

Input:
        zip_code                varchar2

Output:
        cursor containing florist_id from florist_zips for the given zip code

-----------------------------------------------------------------------------*/
v_zip_code      varchar2(10) := format_zip_code (in_zip_code);

BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.florist_id,
                m.florist_name,
                m.zip_code,
                m.state,
                m.phone_number,
                is_florist_open_sunday(m.florist_id) sunday_delivery_flag,
                decode (m.mercury_flag, 'M', 'Y', 'N') mercury_flag,
                m.status,
                decode (m.super_florist_flag, 'Y', 'Y', 'N') super_florist_flag,
                get_florist_weight (m.florist_id) florist_weight,
                decode (m.zip_code, z.zip_code, 'a', 'z') zip_order
        FROM    florist_master m
        JOIN    florist_zips z
        ON      m.florist_id = z.florist_id
        AND     m.status <> 'Inactive'
        WHERE   upper(z.zip_code) = v_zip_code   -- PREVIOUSLY like operator was used to handle 3 or 6 character CA zips
        AND     florist_query_pkg.is_florist_open_for_delivery(m.florist_id, in_delivery_date) = 'Y'
        AND     florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
        AND     florist_query_pkg.is_florist_blocked(m.florist_id, in_delivery_date, null) = 'N'
        AND     florist_query_pkg.is_florist_suspended(m.florist_id, in_delivery_date, null) = 'N'
        AND     m.record_type = 'R'
        AND     (z.block_start_date is null
                OR z.block_end_date < in_delivery_date)
        ORDER BY zip_order, florist_weight desc, m.florist_id;

END VIEW_FLORISTS_BY_ZIPS;


PROCEDURE VIEW_COVERED_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the all zip codes
        covered by the given florist and any of its associated florists.

Input:
        florist_id              varchar2

Output:
        cursor containing zip codes from florist_zips for the given zip code

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                z.zip_code
        FROM    florist_zips z
        JOIN    florist_master m
        ON      z.florist_id = m.florist_id
        WHERE   m.top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        AND     m.record_type = 'R'
        AND     m.status <> 'Inactive'
        ORDER BY z.zip_code;

END VIEW_COVERED_ZIPS;


PROCEDURE VIEW_FLORIST_COMMENT_TYPES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the list of comment types.

Input:
        none

Output:
        cursor containing all records from florist_comment_types

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                comment_type,
                description
        FROM    florist_comment_types;

END VIEW_FLORIST_COMMENT_TYPES;

PROCEDURE VIEW_CODIFICATION_CAT
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all codification
        categories

Input:
        codification_id                 varchar2
        category_id                     varchar2
        description                     varchar2

Output:
        cursor containing all records from codification_categories

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                category_id,
                description
        FROM    codification_categories;

END VIEW_CODIFICATION_CAT;


PROCEDURE VIEW_CODIFICATIONS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all codifications types.

Input:
        codification_id                 varchar2
        category_id                     varchar2
        description                     varchar2

Output:
        cursor containing all records from codification_master

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                codification_id,
                category_id,
                description
        FROM    codification_master;

END VIEW_CODIFICATIONS;


PROCEDURE VIEW_CODIFICATIONS_BY_ID
(
IN_CODIFICATION_ID              IN CODIFICATION_MASTER.CODIFICATION_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all codifications types.

Input:
        codification_id                 varchar2
        category_id                     varchar2
        description                     varchar2

Output:
        cursor containing all records from codification_master

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                codification_id,
                category_id,
                description
        FROM    codification_master
        WHERE   codification_id = in_codification_id;

END VIEW_CODIFICATIONS_BY_ID;


PROCEDURE VIEW_ZIP_CITY_FLORIST_COUNT
(
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the zip code city and
        the number of florists covering the zip code.

Input:
        zip code                        varchar2

Output:
        cursor containing the number of florists covering the zip
        code and the city name for the zip code

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  count_florists_in_zip(in_zip_code) florist_count,
                city
        FROM    zip_code
        WHERE   upper (zip_code_id) = upper (in_zip_code)
        AND     rownum = 1;

END VIEW_ZIP_CITY_FLORIST_COUNT;


PROCEDURE VIEW_FTDM_SHUTDOWN
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the FTMD shutdown record.

Input:
        none

Output:
        cursor containing the record from ftdm_shutdown

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                shutdown_flag,
                shutdown_by,
                shutdown_date,
                restart_date,
                restarted_by,
                last_updated_date
        FROM    ftdm_shutdown;

END VIEW_FTDM_SHUTDOWN;


PROCEDURE VIEW_VIEW_QUEUE
(
IN_MERCURY_ID                   IN  MERCURY.MERCURY.MERCURY_ID%TYPE,
OUT_FLORIST                     OUT TYPES.REF_CURSOR,
OUT_FLORIST_CODIFICATIONS       OUT TYPES.REF_CURSOR,
OUT_FLORIST_ZIPS                OUT TYPES.REF_CURSOR,
OUT_FLORIST_BLOCKS              OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Return the cursors needed for the "View Queue" page.

Input:
        codification_id                 varchar2
        category_id                     varchar2
        description                     varchar2

Output:
        cursor containing fields from various tables with florist information
        cursor from view_florist_codifications
        cursor from view_florist_zips

-----------------------------------------------------------------------------*/

v_codified_florist_count number;
v_codification_id        codified_products.codification_id%TYPE;
v_active_florists        number;
v_zip_block_flag         varchar2(10) := 'N';
v_total_messages         number;
v_product_count          number;
v_product_name           VARCHAR2(100);
v_zip            mercury.mercury.zip_code%TYPE;
v_msg_type       mercury.mercury.msg_type%TYPE;
v_product_id     mercury.mercury.product_id%TYPE;
v_order_number   varchar2(1000);
v_block_end_date date;
v_delivery_date  date;
v_florist_id     mercury.mercury.responsible_florist%type;
alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
numChars            VARCHAR2(10) := '0123456789';
numSub              VARCHAR2(10) := '##########';


BEGIN

/* Get some preliminary data for this message */
SELECT responsible_florist, msg_type
INTO   v_florist_id, v_msg_type
FROM   mercury.mercury m
WHERE  m.mercury_id = in_mercury_id;

/* Get the zip code and product code.  If the message is an FTD, use the
   data in the message.  If the message is a REJ or FOR, need to
   find the original FTD message and use THAT data.
   The original FTD message has a mercury_message_number that matches
   the mercury_order_number on the REJ or FOR message.  However, more than
   one FTD message can match this criterion, so to reduce the matches to
   one, make sure the mercury_id and the mercury_message_number in
   the FTD message are NOT the same. */
IF v_msg_type = 'FTD' THEN
   SELECT zip_code, product_id, delivery_date, substr(reference_number, 1, instr(reference_number || '*', '*') - 1) order_number
   INTO   v_zip, v_product_id, v_delivery_date, v_order_number
   FROM   mercury.mercury
   WHERE  mercury_id = in_mercury_id;
ELSIF v_msg_type IN ('FOR','REJ') THEN
   SELECT /*+ INDEX (M1 MERCURY_N3) */ m1.zip_code, m1.product_id, m1.delivery_date, substr(m1.reference_number, 1, instr(m1.reference_number || '*', '*') - 1) order_number
   INTO   v_zip, v_product_id, v_delivery_date, v_order_number
   FROM   mercury.mercury m1, mercury.mercury m2
   WHERE  m2.mercury_id = in_mercury_id
   AND    m2.mercury_order_number = m1.mercury_message_number
   AND    m1.mercury_id <> m1.mercury_message_number
   AND m1.transmission_time > (sysdate - 60)
   AND    m1.msg_type = 'FTD';
END IF;

-- Determine if the input zip code is Canadian
IF (TRANSLATE(v_zip, alphaChars||numChars, alphaSub||numSub) LIKE '@#@%') THEN
   -- If Canadian, use only first three characters
   v_zip := SUBSTR(v_zip, 1, 3);
ELSE
   -- Use only first five characters for U. S. zip codes
   v_zip := SUBSTR(v_zip, 1, 5);
END IF;

/* Now figure out if the product_id is a product or a subcode; or, indeed, if it
   exists at all. */
SELECT COUNT(*)
INTO   v_product_count
FROM   product_subcodes
WHERE  product_subcode_id = v_product_id;
BEGIN

   IF v_product_count > 0 THEN

      SELECT ps.subcode_description
      INTO   v_product_name
      FROM   product_subcodes ps
      WHERE  ps.product_subcode_id = v_product_id;

   ELSE

      SELECT product_name
      INTO   v_product_name
      FROM   product_master
      WHERE  product_id = v_product_id;
      v_product_count := 1;

   END IF;

EXCEPTION
   WHEN OTHERS THEN
      v_product_name := NULL;
      v_product_count := 0;
END;

/* Count the number of codified products for the product ID in the message */
BEGIN
   SELECT codification_id
   INTO   v_codification_id
   FROM   codified_products cp
   WHERE  cp.product_id = v_product_id;
EXCEPTION
   WHEN OTHERS THEN
      v_codification_id := NULL;
END;

/* Count the entries in the florist_zips table, for this zip code and responsible florist  */
BEGIN
   SELECT 'Y', fz.block_end_date
   INTO   v_zip_block_flag, v_block_end_date
   FROM   florist_zips fz, florist_master fm, mercury.mercury m
   WHERE  fz.zip_code = v_zip
   AND    fz.block_start_date IS NOT NULL
   AND    fz.florist_id = fm.florist_id
   AND   (fm.status IS NULL OR fm.status <> 'Inactive')
   AND    m.mercury_id = in_mercury_id
   AND    m.responsible_florist = fz.florist_id;
EXCEPTION
   WHEN OTHERS THEN
      v_zip_block_flag := 'N';
END;

/* Find the number of active florists in this zip code. */
SELECT COUNT(*)
INTO   v_active_florists
FROM   florist_zips fz, florist_master fm
WHERE  fz.zip_code = v_zip
AND    fz.florist_id = fm.florist_id
AND    fm.status = 'Active';

/* Find the number of entries in the view queue. */
SELECT COUNT(*)
INTO   v_total_messages
FROM   mercury.mercury m
WHERE  view_queue = 'Y';

/* Does this florist have any codification entries?
   To qualify, the responsible_florist has to be in the florist_codifications table,
   and the mercury product_id has to be in the codified_products table, and
   the codification_ids in florist_codifications and codified_products have to match. */
SELECT COUNT(*)
INTO   v_codified_florist_count
FROM   florist_codifications fc, codified_products cp
WHERE  v_florist_id = fc.florist_id
AND    v_product_id = cp.product_id
AND    cp.codification_id = fc.codification_id;

IF v_codified_florist_count > 0 THEN
   OPEN OUT_FLORIST FOR
      SELECT v_delivery_date delivery_date,
             m.mercury_message_number,
             v_order_number order_number,
             to_char (m.created_on, 'mm/dd/yyyy hh:mi:ssAM') created_on,
             m.transmission_time,
             m.comments,
             m.city_state_zip,
             v_zip mercury_zip,
             fm.florist_name,
             fm.city,
             fm.state,
             decode (fm.super_florist_flag, 'Y', 'Y', 'N') super_florist_flag,
             fm.status,
             fm.florist_id,
             fs.suspend_type,
             fs.suspend_start_date,
             fs.suspend_end_date,
             fs.timezone_offset_in_minutes,
             v_product_name product_name,
             v_product_id product_id,
             decode(fc.block_start_date, null, 'N', 'Y') codify_blocked,
             fc.block_start_date codify_block_start_date,
             fc.block_end_date codify_block_end_date,
             v_codification_id codification_id,
             v_zip_block_flag zip_blocked,
             v_block_end_date zip_block_end_date,
             v_active_florists active_florists_count,
             v_total_messages total_view_queue_messages,
             case
                when fs.suspend_start_date is not null then 'Y'
                else 'N'
             end suspend_flag,
             gp.value default_unblock_date
      FROM   mercury.mercury m,
             florist_master fm,
             florist_codifications fc
      LEFT OUTER JOIN florist_suspends fs
      ON     fc.florist_id = fs.florist_id
      JOIN   frp.global_parms gp
      ON     context = 'LOAD_MEMBER_DATA' and name = 'VIEW_QUEUE_DEFAULT_END_DATE'
      WHERE  m.mercury_id = in_mercury_id
      AND    m.responsible_florist = fm.florist_id
      AND    m.responsible_florist = fc.florist_id
      AND    fc.codification_id = v_codification_id;
ELSE
   OPEN OUT_FLORIST FOR
      SELECT v_delivery_date delivery_date,
             m.mercury_message_number,
             v_order_number order_number,
             to_char (m.created_on, 'mm/dd/yyyy hh:mi:ssAM') created_on,
             m.transmission_time,
             m.comments,
             m.city_state_zip,
             v_zip mercury_zip,
             fm.florist_name,
             fm.city,
             fm.state,
             decode (fm.super_florist_flag, 'Y', 'Y', 'N') super_florist_flag,
             fm.status,
             fm.florist_id,
             fs.suspend_type,
             fs.suspend_start_date,
             fs.suspend_end_date,
             fs.timezone_offset_in_minutes,
             v_product_name product_name,
             v_product_id product_id,
             'N' codify_blocked,
             NULL codify_block_start_date,
             NULL codify_block_end_date,
             v_codification_id codification_id,
             v_zip_block_flag zip_blocked,
             v_block_end_date zip_block_end_date,
             v_active_florists active_florists_count,
             v_total_messages total_view_queue_messages,
             case
                when fs.suspend_start_date is not null then 'Y'
                else 'N'
             end suspend_flag,
             gp.value default_unblock_date
      FROM   mercury.mercury m,
             florist_master fm
      LEFT OUTER JOIN florist_suspends fs
      ON     fm.florist_id = fs.florist_id
      JOIN   frp.global_parms gp
      ON     context = 'LOAD_MEMBER_DATA' and name = 'VIEW_QUEUE_DEFAULT_END_DATE'
      WHERE  m.mercury_id = in_mercury_id
      AND    m.responsible_florist = fm.florist_id;
END IF;

view_florist_codifications (v_florist_id, out_florist_codifications);
--view_florist_zips (v_florist_id, out_florist_zips);
view_queue_florist_zip (v_florist_id, v_zip, out_florist_zips);
view_florist_blocks (v_florist_id, out_florist_blocks);

END VIEW_VIEW_QUEUE;




FUNCTION GET_VIEW_QUEUE_MERCURY_ID
(
IN_CSR_ID                       IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning the first mercury message
        that the given csr has not yet viewed in the order of
        messages sent by go to florist with the oldest message first, messages
        with an order for a codified product with the oldest message first,
        all other messages with the oldest message first

Input:
        csr_id                  varchar2

Output:
        mercury_id              varchar2 (null if an error occurred updating the csr_viewed_id table)
-----------------------------------------------------------------------------*/

CURSOR mesg_cur IS
        SELECT  m.mercury_id
        FROM    mercury.mercury m
        WHERE   m.view_queue = 'Y'
        AND     NOT EXISTS
        (
                SELECT  1
                FROM    csr_viewed_ids
                WHERE   csr_id = in_csr_id
                AND     entity_type = 'MERCURY'
                AND     entity_id = m.mercury_id
        )
        AND     ftd_apps.florist_query_pkg.is_florist_locked(m.responsible_florist, in_csr_id) is null
        ORDER BY coalesce (
                        decode ((select distinct 1 from florist_master f where f.florist_id = m.responsible_florist and f.super_florist_flag = 'Y'), 1, 'A', null),
                        decode ((select distinct 1
                                 from   mercury.mercury f
                                 join   ftd_apps.codified_products c
                                 on     f.product_id = c.product_id
                                 and    c.codification_id is not null
                                 where  f.mercury_message_number = m.mercury_order_number
                                 and    f.msg_type = 'FTD'),
                                1, 'B', null),
                        'C'
                ), m.created_on;

v_mercury_id    mercury.mercury.mercury_id%type;
v_status        varchar2(1);
v_message       varchar2(1000);
v_lock_success  varchar2(1);


BEGIN

OPEN mesg_cur;
LOOP
-- get the first mercury message from the cursor

  FETCH mesg_cur INTO v_mercury_id;
      EXIT WHEN mesg_cur%NOTFOUND;
  mercury.mercury_pkg.update_mercury_lock(v_mercury_id,in_csr_id,v_lock_success,v_status,v_message);
  IF v_lock_success = 'Y' THEN
      EXIT;
  END IF;
END LOOP;

CLOSE mesg_cur;

IF v_mercury_id IS NOT NULL THEN
        -- mark the message as viewed by the given csr
        misc_pkg.insert_csr_viewed_ids (in_csr_id, 'MERCURY', v_mercury_id, v_status, v_message);
END IF;

IF v_status = 'Y' THEN
        RETURN v_mercury_id;
ELSE
        RETURN null;
END IF;

END GET_VIEW_QUEUE_MERCURY_ID;

FUNCTION IS_FTDM_SHUTDOWN RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning an indicator if FTDM
        Shutdown is in effect or not.

Input:
        none

Output:
        'Y' - FTDM Shutdown is in effect
        'N' - FTDM Shutdown not in effect
-----------------------------------------------------------------------------*/

CURSOR C IS
        select decode (count (1), 0, 'N', 'Y') from ftdm_shutdown;

v_return        varchar2(1) := 'N';


BEGIN

open c;
fetch c into v_return;
close c;

RETURN v_return;

END IS_FTDM_SHUTDOWN;


FUNCTION IS_FLORIST_LOCKED
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_LOCKED_BY                    IN FLORIST_MASTER.LOCKED_BY%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning an indicator if the florist
        is locked or not.  The florist is considered locked and not updatable
        if another user has it locked within the amount of time allowed.

Input:
        florist_id                      varchar2

Output:
        csr id who has the florist locked
        null - florist is not locked
-----------------------------------------------------------------------------*/

v_timeout       number := to_number (frp.misc_pkg.get_global_parm_value ('FLORIST_LOCKING', 'LOCK_TIMEOUT'))/1440;
v_locked_by     varchar2(100) := 'FLORIST LOAD PROCESS';

CURSOR C IS
        select  locked_by
        FROM    florist_master
        WHERE   florist_id = in_florist_id
        AND     ((locked_by <> in_locked_by
        AND       locked_date >= sysdate - v_timeout)
        OR       locked_by = v_locked_by);

v_return        varchar2(100);

BEGIN

open c;
fetch c into v_return;
close c;

RETURN v_return;

END IS_FLORIST_LOCKED;

FUNCTION IS_FLORIST_ID_IN_USE
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning an indicator if the florist
        id is in use or not.
Input:
        florist_id                      varchar2

Output:
        'Y' - Florist ID in use
        'N' - Florist ID NOT in use
-----------------------------------------------------------------------------*/

v_florist_count          number;
v_return        varchar2(1) := 'N';

BEGIN


SELECT COUNT(*)
INTO   v_florist_count
FROM   florist_master
WHERE  upper (florist_id) = upper (in_florist_id);

IF v_florist_count > 0 THEN
        v_return :='Y';

END IF;

RETURN v_return;

END IS_FLORIST_ID_IN_USE;


PROCEDURE VIEW_FLORIST_DASHBOARD
(
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
IN_PRODUCT_ID                   IN CODIFIED_PRODUCTS.PRODUCT_ID%TYPE,
IN_DELIVERY_DATE     		IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for all florists covering a zip code
        and whether they are codified for the given product

Input:
        zip code                varchar2
        product id              varchar2

Output:
        cursor containing florist dashboard information

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  count (florist_id) total_covering,
                count (decode (status, 'Blocked', 1, 'Mercury Suspend', 1, 'GoTo Suspend', 1, null)) total_blocked,
                count (decode (status, 'Active', decode (codified, 'Y', 1, null), null)) total_active_codified,
                count (decode (status, 'Active', decode (codified, null, 1, null), null)) total_active_not_codified,
                count (decode (ftd_apps.florist_query_pkg.is_florist_open_in_eros(florist_id), 'N', 1)) total_currently_closed,
                count (decode (ftd_apps.florist_query_pkg.is_florist_open_for_delivery(florist_id, trunc(IN_DELIVERY_DATE)), 'N', 1)) total_delivery_date_closed
        FROM
        (
                select  z.florist_id,
                        coalesce (
                                case when z.block_start_date < sysdate AND z.block_end_date > sysdate then 'Blocked' else null end,
                                m.status
                        ) status,
                        (
                                select 'Y'
                                from    florist_codifications fc
                                join    codified_products cp
                                on      fc.codification_id = cp.codification_id
                                where   fc.florist_id = m.florist_id
                                and     cp.product_id = in_product_id
                                and     (fc.block_start_date >= sysdate
                                or      fc.block_start_date is null
                                or      fc.block_end_date <= sysdate)
                        ) codified
                from    florist_zips z
                join    florist_master m
                on      z.florist_id = m.florist_id
                and     m.status <> 'Inactive'
                where   z.zip_code = in_zip_code
                and     not exists
                        (select 1
                         from   ftd_apps.florist_blocks b
                         where   m.florist_id = b.florist_id
                         and     b.block_type = 'O')
        );

END VIEW_FLORIST_DASHBOARD;



PROCEDURE GET_ROUTABLE_FLORISTS
(
 IN_ZIP_CODE          IN FLORIST_ZIPS.ZIP_CODE%TYPE,
 IN_PRODUCT_ID        IN PRODUCT_MASTER.PRODUCT_ID%TYPE,
 IN_DELIVERY_DATE     IN DATE,
 IN_DELIVERY_DATE_END IN DATE,
 IN_SOURCE_CODE       IN SOURCE.SOURCE_CODE%TYPE,
 IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_FLORIST_USED.ORDER_DETAIL_ID%TYPE,
 IN_DELIVERY_CITY     IN EFOS_CITY_STATE.CITY_NAME%TYPE,
 IN_DELIVERY_STATE    IN EFOS_STATE_CODES.STATE_ID%TYPE,
 IN_ORDER_VALUE       IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
 IN_RETURN_ALL        IN CHAR,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
  Overloaded call of GET_ROUTABLE_FLORISTS with IN_RWD_FLAG_OVERRIDE set 
  to null (i.e., do not override).  This is the more common call.
  
-----------------------------------------------------------------------------*/
BEGIN
  get_routable_florists(in_zip_code,
                                     in_product_id,
                                     in_delivery_date,
                                     in_delivery_date_end,
                                     in_source_code,
                                     in_order_detail_id,
                                     in_delivery_city,
                                     in_delivery_state,
                                     in_order_value ,
                                     in_return_all,
                                     null,
                                     out_cursor);

END GET_ROUTABLE_FLORISTS;


PROCEDURE GET_ROUTABLE_FLORISTS
(
 IN_ZIP_CODE          IN FLORIST_ZIPS.ZIP_CODE%TYPE,
 IN_PRODUCT_ID        IN PRODUCT_MASTER.PRODUCT_ID%TYPE,
 IN_DELIVERY_DATE     IN DATE,
 IN_DELIVERY_DATE_END IN DATE,
 IN_SOURCE_CODE       IN SOURCE.SOURCE_CODE%TYPE,
 IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_FLORIST_USED.ORDER_DETAIL_ID%TYPE,
 IN_DELIVERY_CITY     IN EFOS_CITY_STATE.CITY_NAME%TYPE,
 IN_DELIVERY_STATE    IN EFOS_STATE_CODES.STATE_ID%TYPE,
 IN_ORDER_VALUE       IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
 IN_RETURN_ALL        IN CHAR,
 IN_RWD_FLAG_OVERRIDE IN CHAR DEFAULT NULL, 
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will return a list of routable florists in the highest
        score tier.

Input:
        zip code, product_id, delivery date, end delivery date, source code,
        order detail id, delivery city, delivery state, order value (amount),
        return all florist (Y/N) flag

Output:
        score - NUMBER

-----------------------------------------------------------------------------*/

v_flag CHAR(1) := 'N';
v_max_score NUMBER;
v_max_primary_backup_score NUMBER;
v_primary_backup_exists char(1) := 'N';

v_city_state_code efos_city_state.city_state_code%TYPE;
v_codification_id codified_products.codification_id%TYPE;
v_codification_required codification_master.codification_required%TYPE;
v_product_type product_master.product_type%TYPE;

v_mon_cutoff_time global_parms.monday_cutoff%TYPE;
v_tue_cutoff_time global_parms.tuesday_cutoff%TYPE;
v_wed_cutoff_time global_parms.wednesday_cutoff%TYPE;
v_thu_cutoff_time global_parms.thursday_cutoff%TYPE;
v_fri_cutoff_time global_parms.friday_cutoff%TYPE;
v_sat_cutoff_time global_parms.saturday_cutoff%TYPE;
v_sun_cutoff_time global_parms.sunday_cutoff%TYPE;

v_default_cutoff_time VARCHAR(4);
v_delivery_day CHAR(3);
v_current_time VARCHAR(4);
v_rwd_flag CHAR(1) := 'Y';
v_zip_city VARCHAR(100);

BEGIN

  DELETE FROM temp_florist;

  IF in_delivery_date_end IS NULL THEN
  -- if an end delivery date was not passed in get the default the
  --  delivery cut off time for that day of the week just in case
  --  it may be used.

     v_delivery_day := TO_CHAR(in_delivery_date,'DY');

     SELECT monday_cutoff,
            tuesday_cutoff,
            wednesday_cutoff,
            thursday_cutoff,
            friday_cutoff,
            saturday_cutoff,
            sunday_cutoff
       INTO v_mon_cutoff_time,
            v_tue_cutoff_time,
            v_wed_cutoff_time,
            v_thu_cutoff_time,
            v_fri_cutoff_time,
            v_sat_cutoff_time,
            v_sun_cutoff_time
       FROM global_parms
      WHERE global_parms_key = 1;

     CASE v_delivery_day
       WHEN 'SUN' THEN
            v_default_cutoff_time := v_sun_cutoff_time;
       WHEN 'MON' THEN
            v_default_cutoff_time := v_mon_cutoff_time;
       WHEN 'TUE' THEN
            v_default_cutoff_time := v_tue_cutoff_time;
       WHEN 'WED' THEN
            v_default_cutoff_time := v_wed_cutoff_time;
       WHEN 'THU' THEN
            v_default_cutoff_time := v_thu_cutoff_time;
       WHEN 'FRI' THEN
            v_default_cutoff_time := v_fri_cutoff_time;
       WHEN 'SAT' THEN
            v_default_cutoff_time := v_sat_cutoff_time;
       ELSE
            v_default_cutoff_time := NULL;
     END CASE;

  ELSE
     v_delivery_day := NULL;
     v_default_cutoff_time := NULL;
  END IF;

  v_city_state_code := MISC_PKG.get_efos_city_state_code(in_delivery_city, in_delivery_state);

  IF in_return_all = 'Y' THEN
    BEGIN
      -- if the product is codified get its codification id
      SELECT codification_id
        INTO v_codification_id
        FROM codified_products
       WHERE product_id = in_product_id;

      EXCEPTION WHEN NO_DATA_FOUND THEN v_codification_id := NULL;
    END;
    
   OPEN out_cursor FOR
    
     SELECT * FROM (
     -- Added the above outer select since we must filter out negative "score" values returned from calc_florist_score (per defect #12219).
     -- Negative scores represent florist_blocks (soft, hard, optout)
     --
     SELECT fm.florist_id,
            FLORIST_WEIGHT_CALC_PKG.calc_florist_score(fm.florist_id, fz.zip_code, in_source_code, v_codification_id, in_delivery_date, in_delivery_date_end) "score",
            ftd_apps.florist_query_pkg.get_florist_weight(fm.florist_id) "weight",
            fm.florist_name,
            fm.phone_number,
            fm.mercury_flag,
            fm.super_florist_flag,
            florist_query_pkg.is_florist_open_sunday(fm.florist_id) sunday_delivery_flag,
            NVL(MISC_PKG.CHANGE_TIMES_TIMEZONE(fz.cutoff_time,sm.time_zone,fm.state),MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state)) "cutoff_time",
            fm.status,
            fc.codification_id,
            MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fc.block_start_date,fc.block_end_date,0,in_delivery_date,in_delivery_date_end) "florist_blocked",
            v_codification_id "product_codification_id",
            clean.order_query_pkg.get_ordr_florist_used_seq_list(fm.florist_id,in_order_detail_id) "sequences",
            sfp.priority "priority",
            'Zip' "zip_city_flag",
            florist_query_pkg.is_florist_suspended(fm.florist_id, in_delivery_date, in_delivery_date_end) "florist_suspended",
            fm.minimum_order_amount
       FROM state_master sm,
            florist_zips fz,
            florist_master fm
            LEFT OUTER JOIN florist_codifications fc
                         ON fc.florist_id = fm.florist_id
                        AND fc.codification_id = v_codification_id
            LEFT OUTER JOIN source_florist_priority sfp
                         ON sfp.florist_id = fm.florist_id
                        AND sfp.source_code = in_source_code
      WHERE (
             fm.florist_id = fz.florist_id
             AND
             fm.status <> 'Inactive'
             AND
             fz.zip_code = in_zip_code
            )
        AND NOT EXISTS (SELECT 1
                          FROM florist_blocks fb
                         WHERE fb.florist_id = fm.florist_id
                           AND fb.block_type = 'O'
                           AND 'Y' = (MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fb.block_start_date,fb.block_end_date,0,in_delivery_date,in_delivery_date_end)))
        AND fm.state = sm.state_master_id
        AND florist_query_pkg.is_blocked_suspended_closed(fm.florist_id, in_delivery_date, in_delivery_date_end,
                fz.block_start_date, fz.block_end_date, null, null, 'N') = 'N'
    UNION
     SELECT fm.florist_id,
            FLORIST_WEIGHT_CALC_PKG.calc_florist_score(fm.florist_id, NULL, in_source_code, v_codification_id, in_delivery_date, in_delivery_date_end) "score",
            ftd_apps.florist_query_pkg.get_florist_weight(fm.florist_id) "weight",
            fm.florist_name,
            fm.phone_number,
            fm.mercury_flag,
            fm.super_florist_flag,
            florist_query_pkg.is_florist_open_sunday(fm.florist_id) sunday_delivery_flag,
            MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state) "cutoff_time",
            fm.status,
            fc.codification_id,
            MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fc.block_start_date,fc.block_end_date,0,in_delivery_date,in_delivery_date_end) "florist_blocked",
            v_codification_id "product_codification_id",
            clean.order_query_pkg.get_ordr_florist_used_seq_list(fm.florist_id,in_order_detail_id) "sequences",
            sfp.priority "priority",
            'City' "zip_city_flag",
            florist_query_pkg.is_florist_suspended(fm.florist_id, in_delivery_date, in_delivery_date_end) "florist_suspended",
            fm.minimum_order_amount
       FROM state_master sm,
            florist_master fm
            LEFT OUTER JOIN florist_codifications fc
                         ON fc.florist_id = fm.florist_id
                        AND fc.codification_id = v_codification_id
            LEFT OUTER JOIN source_florist_priority sfp
                         ON sfp.florist_id = fm.florist_id
                        AND sfp.source_code = in_source_code
      WHERE fm.city_state_number = v_city_state_code
        AND fm.status <> 'Inactive'
        AND NOT EXISTS (SELECT 1
                          FROM florist_zips fz
                         WHERE fz.florist_id = fm.florist_id
                           AND fz.zip_code = in_zip_code)
        AND NOT EXISTS (SELECT 1
                          FROM florist_blocks fb
                         WHERE fb.florist_id = fm.florist_id
                           AND fb.block_type = 'O'
                           AND 'Y' = (MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fb.block_start_date,fb.block_end_date,0,in_delivery_date,in_delivery_date_end)))
        AND sm.state_master_id = fm.state
        AND florist_query_pkg.is_blocked_suspended_closed(fm.florist_id, in_delivery_date, in_delivery_date_end,
                null, null, null, null, 'N') = 'N'
        AND florist_query_pkg.is_florist_city_blocked(fm.florist_id, in_delivery_date, in_delivery_date_end) = 'N'
      --
      -- Remainder of outer select described above
      --
      ) 
      WHERE "score" >= 0
      ORDER BY "florist_suspended", "priority", "score" DESC,
               "weight" DESC;


  ELSIF in_return_all = 'N' THEN

    SELECT TO_CHAR(SYSDATE, 'HH24MI')
      INTO v_current_time
      FROM DUAL;

    SELECT pm.product_type,
           cp.codification_id,
           cm.codification_required
      INTO v_product_type,
           v_codification_id,
           v_codification_required
      FROM product_master pm,
           codified_products cp,
           codification_master cm
     WHERE pm.product_id = in_product_id
       AND pm.product_id = cp.product_id (+)
       AND cm.codification_id (+) = cp.codification_id;

    -- search for florist for the zip code passed in

    IF v_delivery_day IS NULL THEN
    -- a date range was passed in

      INSERT INTO temp_florist
      SELECT fm.florist_id,
             FLORIST_WEIGHT_CALC_PKG.calc_florist_score(fm.florist_id, fz.zip_code, in_source_code, v_codification_id, in_delivery_date, in_delivery_date_end),
             TO_NUMBER(NVL(MISC_PKG.CHANGE_TIMES_TIMEZONE(fz.cutoff_time,sm.time_zone,fm.state),MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state))),
             TO_NUMBER(v_current_time),
             sfp.priority
        FROM florist_zips fz, state_master sm, florist_master fm
             LEFT OUTER JOIN florist_codifications fc
                          ON fc.florist_id = fm.florist_id
                         AND fc.codification_id = v_codification_id
             LEFT OUTER JOIN source_florist_priority sfp
                          ON sfp.florist_id = fm.florist_id
                         AND sfp.source_code = in_source_code
       WHERE fz.zip_code = in_zip_code
         AND fz.florist_id = fm.florist_id
         -- exclude already used florists
         AND SUBSTR(fm.florist_id,1,7) NOT IN (select SUBSTR(florist_id,1,7) from clean.order_florist_used where order_detail_id = in_order_detail_id)
         AND fm.status <> 'Inactive'
         AND sm.state_master_id = fm.state
         -- required product codification check
         AND (
              v_codification_required IS NULL
              OR
              v_codification_required = 'N'
              OR
              (
               v_codification_required = 'Y'
               AND
               fc.codification_id IS NOT NULL
              )
             )
         -- minimum value check (ignore for goto florists and for codified products)
         AND (
              fm.super_florist_flag = 'Y'
              OR
              NVL(fm.minimum_order_amount, 0) <= in_order_value
              OR
              fc.codification_id IS NOT NULL
             )
         -- if product is a same day gift, florist must be codified for it
         AND (
              TRIM(v_product_type) != 'SDG'
              OR
              fc.codification_id IS NOT NULL
             )
         AND florist_query_pkg.is_blocked_suspended_closed(fm.florist_id, in_delivery_date, in_delivery_date_end,
                 fz.block_start_date, fz.block_end_date, fc.block_start_date, fc.block_end_date, 'Y') = 'N';
     ELSE
     -- a single date was passed in
      INSERT INTO temp_florist
        SELECT fm.florist_id,
               FLORIST_WEIGHT_CALC_PKG.calc_florist_score(fm.florist_id, fz.zip_code, in_source_code, v_codification_id, in_delivery_date, in_delivery_date_end),
               TO_NUMBER(NVL(MISC_PKG.CHANGE_TIMES_TIMEZONE(fz.cutoff_time,sm.time_zone,fm.state),MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state))),
               TO_NUMBER(v_current_time),
               sfp.priority
          FROM florist_zips fz, state_master sm, florist_master fm
               LEFT OUTER JOIN florist_codifications fc
                            ON fc.florist_id = fm.florist_id
                           AND fc.codification_id = v_codification_id
               LEFT OUTER JOIN source_florist_priority sfp
                            ON sfp.florist_id = fm.florist_id
                           AND sfp.source_code = in_source_code
         WHERE fz.zip_code = in_zip_code
           AND fz.florist_id = fm.florist_id
           -- exclude already used florists
           AND SUBSTR(fm.florist_id,1,7) NOT IN (select SUBSTR(florist_id,1,7) from clean.order_florist_used where order_detail_id = in_order_detail_id)
           -- zip code block check
           AND v_flag = (MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fz.block_start_date,fz.block_end_date,0,in_delivery_date,in_delivery_date_end))
           AND fm.status <> 'Inactive'
           AND not exists (select 'Y' from ftd_apps.florist_blocks fb
               where fb.florist_id = fz.florist_id
               and in_delivery_date between trunc(fb.block_start_date) and nvl(fb.block_end_date, in_delivery_date))
           AND sm.state_master_id = fm.state
           AND florist_query_pkg.is_florist_suspended(fm.florist_id, in_delivery_date, null) = 'N'
           AND florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, in_delivery_date) = 'Y'
           AND florist_query_pkg.is_florist_open_in_eros(fm.florist_id) = 'Y'
           -- Check for same day cutoff if the delivery is for today
           AND (
                in_delivery_date > SYSDATE
                OR
                NVL(MISC_PKG.CHANGE_TIMES_TIMEZONE(fz.cutoff_time,sm.time_zone,fm.state),MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state))
                       > TO_NUMBER(v_current_time)
               )
           -- required product codification check
           AND (
                v_codification_required IS NULL
                OR
                v_codification_required = 'N'
                OR
                (
                 v_codification_required = 'Y'
                 AND
                 fc.codification_id IS NOT NULL
                )
               )
           -- codification block check
           AND v_flag = (MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fc.block_start_date,fc.block_end_date,0,in_delivery_date,in_delivery_date_end))
           -- minimum value check (ignore for goto florists and for codified products)
           AND (
                fm.super_florist_flag = 'Y'
                OR
                NVL(fm.minimum_order_amount, 0) <= in_order_value
                OR
                fc.codification_id IS NOT NULL
               )
           -- if product is a same day gift, florist must be codified for it
           AND (
                TRIM(v_product_type) != 'SDG'
                OR
                fc.codification_id IS NOT NULL
               );
     END IF;

    SELECT MAX(score)
      INTO v_max_score
      FROM temp_florist
     WHERE score >= 0;

    v_zip_city := 'Zip';
    
    IF v_max_score IS NULL THEN

    -- no florist were found for the zip code passed in therefor search by city_state code

       DELETE FROM temp_florist;
       
       v_zip_city := 'City';

       IF v_delivery_day IS NULL THEN
         INSERT INTO temp_florist
         SELECT fm.florist_id,
                FLORIST_WEIGHT_CALC_PKG.calc_florist_score(fm.florist_id, NULL, in_source_code, v_codification_id, in_delivery_date, in_delivery_date_end),
                TO_NUMBER(MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state)),
                TO_NUMBER(v_current_time),
                sfp.priority
           FROM state_master sm, florist_master fm
                LEFT OUTER JOIN florist_codifications fc
                             ON fc.florist_id = fm.florist_id
                            AND fc.codification_id = v_codification_id
                LEFT OUTER JOIN source_florist_priority sfp
                             ON sfp.florist_id = fm.florist_id
                            AND sfp.source_code = in_source_code
          WHERE fm.city_state_number = v_city_state_code
            -- exclude already used florists
            AND SUBSTR(fm.florist_id,1,7) NOT IN (select SUBSTR(florist_id,1,7) from clean.order_florist_used where order_detail_id = in_order_detail_id)
            AND fm.status <> 'Inactive'
            AND sm.state_master_id = fm.state
            -- required product codification check
            AND (
                 v_codification_required IS NULL
                OR
                v_codification_required = 'N'
                OR
                (
                 v_codification_required = 'Y'
                 AND
                 fc.codification_id IS NOT NULL
                )
               )
            -- minimum value check (ignore for goto florists and for codified products)
            AND (
                 fm.super_florist_flag = 'Y'
                 OR
                 NVL(fm.minimum_order_amount, 0) <= in_order_value
                 OR
                 fc.codification_id IS NOT NULL
                )
            -- if product is a same day gift, florist must be codified for it
            AND (
                 TRIM(v_product_type) != 'SDG'
                 OR
                 fc.codification_id IS NOT NULL
                )
            AND florist_query_pkg.is_blocked_suspended_closed(fm.florist_id, in_delivery_date, in_delivery_date_end,
                    null, null, fc.block_start_date, fc.block_end_date, 'Y') = 'N'
            AND florist_query_pkg.is_florist_city_blocked(fm.florist_id, in_delivery_date, in_delivery_date_end) = 'N';

       ELSE
         INSERT INTO temp_florist
           SELECT fm.florist_id,
                  FLORIST_WEIGHT_CALC_PKG.calc_florist_score(fm.florist_id, NULL, in_source_code, v_codification_id, in_delivery_date, in_delivery_date_end),
                  TO_NUMBER(MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,fm.state)),
                  TO_NUMBER(MISC_PKG.CHANGE_TIMES_TIMEZONE(v_current_time,sm.time_zone,fm.state)),
                  sfp.priority
             FROM state_master sm, florist_master fm
                  LEFT OUTER JOIN florist_codifications fc
                               ON fc.florist_id = fm.florist_id
                              AND fc.codification_id = v_codification_id
                  LEFT OUTER JOIN source_florist_priority sfp
                               ON sfp.florist_id = fm.florist_id
                              AND sfp.source_code = in_source_code
            WHERE fm.city_state_number = v_city_state_code
              -- exclude already used florists
              AND SUBSTR(fm.florist_id,1,7) NOT IN (select SUBSTR(florist_id,1,7) from clean.order_florist_used where order_detail_id = in_order_detail_id)
              AND fm.status <> 'Inactive'
              AND sm.state_master_id = fm.state
              AND florist_query_pkg.is_florist_suspended(fm.florist_id, in_delivery_date, null) = 'N'
              AND florist_query_pkg.is_florist_blocked(fm.florist_id, in_delivery_date, null) = 'N'
              AND florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, in_delivery_date) = 'Y'
              AND florist_query_pkg.is_florist_open_in_eros(fm.florist_id) = 'Y'
              AND florist_query_pkg.is_florist_city_blocked(fm.florist_id, in_delivery_date, null) = 'N'
              -- Check for same day cutoff if the delivery is for today
              AND (
                   in_delivery_date > SYSDATE
                   OR
                   MISC_PKG.CHANGE_TIMES_TIMEZONE(v_default_cutoff_time,sm.time_zone,sm.state_master_id)
                       > TO_NUMBER(v_current_time)
                   )
              -- required product codification check
              AND (
                   v_codification_required IS NULL
                   OR
                   v_codification_required = 'N'
                   OR
                   (
                    v_codification_required = 'Y'
                    AND
                    fc.codification_id IS NOT NULL
                   )
                  )
              -- codification block check
              AND v_flag = (MISC_PKG.IS_IT_WITHIN_BLOCKED_DATES(fc.block_start_date,fc.block_end_date,0,in_delivery_date,in_delivery_date_end))
              -- minimum value check (ignore for goto florists and for codified products)
              AND (
                   fm.super_florist_flag = 'Y'
                   OR
                   NVL(fm.minimum_order_amount, 0) <= in_order_value
                   OR
                   fc.codification_id IS NOT NULL
                  )
              -- if product is a same day gift, florist must be codified for it
              AND (
                   TRIM(v_product_type) != 'SDG'
                   OR
                   fc.codification_id IS NOT NULL
                  );
       END IF;

       SELECT MAX(score)
         INTO v_max_score
         FROM temp_florist
        WHERE score >= 0;         
    END IF;

    SELECT MAX(score)
      INTO v_max_primary_backup_score
      FROM temp_florist tf
      WHERE tf.priority > 0
      AND tf.score >= 0;

    BEGIN
      SELECT 'Y'
        INTO v_primary_backup_exists
        FROM SOURCE_FLORIST_PRIORITY
        WHERE source_code = in_source_code
        AND rownum = 1;

      EXCEPTION WHEN NO_DATA_FOUND THEN v_primary_backup_exists := 'N';
    END;
    
    -- Calculate/Setup the RWD Processing Flag
    IF (v_primary_backup_exists = 'N') THEN
            v_rwd_flag := 'Y';
    ELSIF (in_rwd_flag_override IS NOT NULL) THEN
            v_rwd_flag := in_rwd_flag_override;
    ELSE
            -- Use the Value in the SOURCE_MASTER table
            BEGIN
                    SELECT primary_backup_rwd_flag
                    INTO v_rwd_flag
                    FROM source_master
                    WHERE source_code = in_source_code;
                    EXCEPTION WHEN NO_DATA_FOUND THEN v_rwd_flag := 'Y';
            END;
    END IF;

    -- if v_max_primary_backup_score is not null but less than v_max_score then
    -- all Primary/Backup florists are not active. 
    -- if v_rwd_flag = 'Y', ok to return highest scoring items - return items with score = v_max_score.
    -- if v_rwd_flag = 'N', only return primary or backup (v_max_primary_backup_score = v_max_score)
    -- Else, return nothing, so order gets placed into a Queue
    IF (v_rwd_flag = 'Y') OR (v_max_primary_backup_score = v_max_score) THEN
       OPEN OUT_CURSOR FOR
          SELECT fm.florist_id,
              tf.score,
              ftd_apps.florist_query_pkg.get_florist_weight(fm.florist_id) "weight",
              fm.florist_name,
              fm.phone_number,
              fm.mercury_flag,
              fm.super_florist_flag,
              florist_query_pkg.is_florist_open_sunday(fm.florist_id) sunday_delivery_flag,
              tf.cutoff_time,
              fm.status,
              NULL "codification_id",
              NULL "florist_blocked",
              v_codification_id "product_codification_id",
              NULL "sequences",
              tf.priority,
              v_zip_city "zip_city_flag"
          FROM florist_master fm
              INNER JOIN temp_florist tf
                      ON tf.florist_id = fm.florist_id
                     AND tf.score = v_max_score;
    ELSE
       OPEN OUT_CURSOR FOR
          select * from dual where 1=0;
    END IF;

  END IF;

END GET_ROUTABLE_FLORISTS;


PROCEDURE GET_FLORIST_STATUS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_STATUS                      OUT FLORIST_MASTER.STATUS%TYPE,
OUT_MERCURY_FLAG                OUT FLORIST_MASTER.MERCURY_FLAG%TYPE,
OUT_OPT_OUT_FLAG                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the status of the
        florist, the mercury flag and if the florist is opt out blocked

Input:
        florist_id                      varchar2

Output:
        florist_status                  varchar2
        mercury_flag                    varchar2
        opt_out_flag                    varchar2

-----------------------------------------------------------------------------*/

CURSOR florist_cur IS
        SELECT  f.status,
                f.mercury_flag,
                decode ((select b.block_type from florist_blocks b where b.florist_id = f.florist_id and block_type = 'O'), 'O', 'Y', 'N') opt_out_flag
        FROM    florist_master f
        WHERE   f.florist_id = in_florist_id
        AND     f.record_type = 'R';

BEGIN

OPEN florist_cur;
FETCH florist_cur INTO out_status, out_mercury_flag, out_opt_out_flag;
CLOSE florist_cur;

END GET_FLORIST_STATUS;


PROCEDURE GET_FLORIST_STATUS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_DELIVERY_DATE		IN DATE,
IN_DELIVERY_DATE_END		IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the status of the
        florist or vendor
Input:
        florist_id                      varchar2
        delivery_date			date
        deliver_date_end		date

Output:
        out_cur		                ref_cursor
      
-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  decode (f.status, 'Mercury Suspend', decode (f.super_florist_flag, 'Y', 'Active', 'Suspend'), 'GoTo Suspend', 'Suspend', f.status) status,
                f.mercury_flag,
                decode ((select b.block_type from florist_blocks b where b.florist_id = f.florist_id and b.block_type = 'O'), 'O', 'Y', 'N') opt_out_flag,
                'N' vendor_flag,
                florist_query_pkg.is_florist_open_sunday(f.florist_id) sunday_delivery_flag,
                florist_query_pkg.is_florist_open_in_eros(f.florist_id) as florist_open_in_eros,
                florist_query_pkg.is_florist_suspended(f.florist_id, in_delivery_date, in_delivery_date_end) florist_suspended,
                ( (select listagg('Florist is currently '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') || '.  Florist schedule is ') 
	               within group (order by fh1.florist_id) 
	               from ftd_apps.florist_hours fh1
	               where fh1.florist_id = f.florist_id
	               and UPPER(trim(to_char(SYSDATE, 'Day'))) = UPPER(fh1.day_of_week)
	            ) 
	            || 
	            (select listagg(INITCAP(fh1.day_of_week) || ' - '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM'), ', ') 
	              within group (ORDER BY 
	              CASE
	                  WHEN fh1.day_of_week = 'SUNDAY' THEN 1
	                  WHEN fh1.day_of_week = 'MONDAY' THEN 2
	                  WHEN fh1.day_of_week = 'TUESDAY' THEN 3
	                  WHEN fh1.day_of_week = 'WEDNESDAY' THEN 4
	                  WHEN fh1.day_of_week = 'THURSDAY' THEN 5
	                  WHEN fh1.day_of_week = 'FRIDAY' THEN 6
	                  WHEN fh1.day_of_week = 'SATURDAY' THEN 7
	              END ASC) || '.' as florist_schedule
	             from ftd_apps.florist_hours fh1
	             WHERE fh1.florist_id = f.florist_id) ) as florist_currently_closed_msg,
	        florist_query_pkg.is_florist_open_date_range(f.florist_id, in_delivery_date, in_delivery_date_end) as open_on_delivery,
	        ( (select listagg('Florist is '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') || ' on selected delivery date.  Florist schedule is ') 
	               within group (order by fh1.florist_id) 
	               from ftd_apps.florist_hours fh1
	               where fh1.florist_id = f.florist_id
	               and UPPER(trim(to_char(in_delivery_date, 'Day'))) = UPPER(fh1.day_of_week)
	            ) 
	            || 
	            (select listagg(INITCAP(fh1.day_of_week) || ' - '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM'), ', ') 
	              within group (ORDER BY 
	              CASE
	                  WHEN fh1.day_of_week = 'SUNDAY' THEN 1
	                  WHEN fh1.day_of_week = 'MONDAY' THEN 2
	                  WHEN fh1.day_of_week = 'TUESDAY' THEN 3
	                  WHEN fh1.day_of_week = 'WEDNESDAY' THEN 4
	                  WHEN fh1.day_of_week = 'THURSDAY' THEN 5
	                  WHEN fh1.day_of_week = 'FRIDAY' THEN 6
	                  WHEN fh1.day_of_week = 'SATURDAY' THEN 7
	              END ASC) || '.' as florist_schedule
	             from ftd_apps.florist_hours fh1
	             WHERE fh1.florist_id = f.florist_id) ) as florist_closed_on_dd_msg
        FROM    florist_master f
        WHERE   f.florist_id = in_florist_id
        AND     f.record_type = 'R'
        AND     (f.vendor_flag = 'N' or f.vendor_flag is null)
        AND     not exists
        (       select  1
                from    vendor_master v
                where   v.member_number = f.florist_id
        )
        UNION
        SELECT  'Active' status,
                null mercury_flag,
                null opt_out_flag,
                'Y' vendor_flag,
                null,
                null,
                null,
                null,
                null,
                null
        FROM    vendor_master v
        WHERE   v.member_number = in_florist_id;
END GET_FLORIST_STATUS;

PROCEDURE GET_UDD_FLORIST_STATUS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_DELIVERY_DATE		IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the status of the
        florist or vendor for Update Delivery Date since it allows non-R type Florists
Input:
        florist_id                      varchar2

Output:
        florist_status                  varchar2
        mercury_flag                    varchar2
        opt_out_flag                    varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  decode (f.status, 'Mercury Suspend', decode (f.super_florist_flag, 'Y', 'Active', 'Suspend'), 'GoTo Suspend', 'Suspend', f.status) status,
                f.mercury_flag,
                decode ((select b.block_type from florist_blocks b where b.florist_id = f.florist_id and b.block_type = 'O'), 'O', 'Y', 'N') opt_out_flag,
                'N' vendor_flag,
                florist_query_pkg.is_florist_open_sunday(f.florist_id) sunday_delivery_flag,
                florist_query_pkg.is_florist_open_in_eros(f.florist_id) as florist_open_in_eros,
                ( (select listagg('Florist is currently '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') || '.  Florist schedule is ') 
	               within group (order by fh1.florist_id) 
	               from ftd_apps.florist_hours fh1
	               where fh1.florist_id = f.florist_id
	               and UPPER(trim(to_char(SYSDATE, 'Day'))) = UPPER(fh1.day_of_week)
	            ) 
	            || 
	            (select listagg(INITCAP(fh1.day_of_week) || ' - '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM'), ', ') 
	              within group (ORDER BY 
	              CASE
	                  WHEN fh1.day_of_week = 'SUNDAY' THEN 1
	                  WHEN fh1.day_of_week = 'MONDAY' THEN 2
	                  WHEN fh1.day_of_week = 'TUESDAY' THEN 3
	                  WHEN fh1.day_of_week = 'WEDNESDAY' THEN 4
	                  WHEN fh1.day_of_week = 'THURSDAY' THEN 5
	                  WHEN fh1.day_of_week = 'FRIDAY' THEN 6
	                  WHEN fh1.day_of_week = 'SATURDAY' THEN 7
	              END ASC) || '.' as florist_schedule
	             from ftd_apps.florist_hours fh1
	             WHERE fh1.florist_id = f.florist_id) ) as florist_currently_closed_msg,
	        florist_query_pkg.is_florist_open_for_delivery(f.florist_id, in_delivery_date) as open_on_delivery,
	        ( (select listagg('Florist is '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') || ' on selected delivery date.  Florist schedule is ') 
	               within group (order by fh1.florist_id) 
	               from ftd_apps.florist_hours fh1
	               where fh1.florist_id = f.florist_id
	               and UPPER(trim(to_char(in_delivery_date, 'Day'))) = UPPER(fh1.day_of_week)
	            ) 
	            || 
	            (select listagg(INITCAP(fh1.day_of_week) || ' - '|| decode(fh1.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM'), ', ') 
	              within group (ORDER BY 
	              CASE
	                  WHEN fh1.day_of_week = 'SUNDAY' THEN 1
	                  WHEN fh1.day_of_week = 'MONDAY' THEN 2
	                  WHEN fh1.day_of_week = 'TUESDAY' THEN 3
	                  WHEN fh1.day_of_week = 'WEDNESDAY' THEN 4
	                  WHEN fh1.day_of_week = 'THURSDAY' THEN 5
	                  WHEN fh1.day_of_week = 'FRIDAY' THEN 6
	                  WHEN fh1.day_of_week = 'SATURDAY' THEN 7
	              END ASC) || '.' as florist_schedule
	             from ftd_apps.florist_hours fh1
	             WHERE fh1.florist_id = f.florist_id) ) as florist_closed_on_dd_msg,
                florist_query_pkg.is_florist_suspended(f.florist_id, in_delivery_date, null) florist_suspended,
                florist_query_pkg.is_florist_blocked(f.florist_id, in_delivery_date, null) florist_blocked
        FROM    florist_master f
        WHERE   f.florist_id = in_florist_id
        AND     (f.vendor_flag = 'N' or f.vendor_flag is null)
        AND     not exists
        (       select  1
                from    vendor_master v
                where   v.member_number = f.florist_id
        )
        UNION
        SELECT  'Active' status,
                null mercury_flag,
                null opt_out_flag,
                'Y' vendor_flag,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        FROM    vendor_master v
        WHERE   v.member_number = in_florist_id;


END GET_UDD_FLORIST_STATUS;


PROCEDURE GET_FLORIST_NAME_PHONE
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the name and
        phone number of the given florist
Input:
        florist_id                      varchar2

Output:
        cursor containing florist information

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT
                m.florist_id,
                m.florist_name,
                m.phone_number
        FROM    florist_master M
        WHERE   m.florist_id = in_florist_id;

END GET_FLORIST_NAME_PHONE;


PROCEDURE GET_GOTO_FLORIST_SHUTDOWN
(
 IN_FLORIST_ID  IN GOTO_FLORIST_SHUTDOWN.FLORIST_ID%TYPE,
 OUT_CURSOR    OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the records from table goto_florist_shutdown.

Input:
        florist_id VARCHAR2

Output:
        cursor containing goto_florist_shutdown info
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT florist_id,
            p_status,
            f_status,
            q_status,
            x_status,
            merc_info
       FROM goto_florist_shutdown
      WHERE florist_id = in_florist_id;

END GET_GOTO_FLORIST_SHUTDOWN;



PROCEDURE VIEW_FLORIST_ZIPCODE_SUMMARY
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a summary of florists to be
        sent to AS/400 for reporting.

Input:
        none

Output:
        cursor containing the florist number, number of zips the florist
        covers, and the goto flag of the florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.florist_id,
                (select count(1) from florist_zips z where z.florist_id = m.florist_id) zip_count,
                decode (m.super_florist_flag, 'Y', 'Y', 'N') goto_flag
        FROM    florist_master m
        WHERE   m.status <> 'Inactive'
        AND     m.record_type = 'R'
        AND     m.vendor_flag = 'N'
        ORDER BY m.florist_id;

END VIEW_FLORIST_ZIPCODE_SUMMARY;



PROCEDURE VIEW_FLORIST_SOURCE_INFO
(
IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE,
OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is quering all Source Codes and their Priorities assigned to the given Florist and 
returns the results ordered by ascending priority from the FTD_APPS.SOURCE_FLORIST_PRIORITY table.

Input:
        IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE

Output:
        cursor containing source codes, descriptions and priority

-----------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
    SELECT
        sfp.source_code,
        sm.description,
        sfp.priority
    FROM source_florist_priority sfp
    INNER JOIN source_master sm
    ON sfp.source_code = sm.source_code
    WHERE  sfp.florist_id = in_florist_id
    ORDER BY sfp.priority ASC;
END VIEW_FLORIST_SOURCE_INFO;

PROCEDURE VIEW_FLORIST_BLOCKS
(
IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE,
OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is retrieving all the blocks for the current day and the future for the florist

Input:
        IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE

Output:
        cursor containing blocks

-----------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
    SELECT
        fb.block_type,
        fb.block_start_date,
        fb.block_end_date,
        fb.blocked_by_user_id,
        case
          when fb.block_start_date is not null then 'Y'
          else 'N'
        end blocked_flag,
        fb.block_reason
    FROM florist_blocks fb
    WHERE  fb.florist_id = in_florist_id
    ORDER BY fb.block_start_date;

END VIEW_FLORIST_BLOCKS;

PROCEDURE VIEW_FLORIST_SUSPENDS
(
IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE,
OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is retrieving all the suspends for the current day and the future for the florist

Input:
        IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE

Output:
        cursor containing blocks

-----------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
    SELECT
        fs.suspend_type,
        fs.suspend_start_date,
        fs.suspend_end_date,
        case
          when fs.suspend_start_date is not null then 'Y'
          else 'N'
        end suspend_flag
    FROM florist_suspends fs
    WHERE  fs.florist_id = in_florist_id
    ORDER BY fs.suspend_start_date;

END VIEW_FLORIST_SUSPENDS;

FUNCTION IS_FLORIST_BLOCKED
(
IN_FLORIST_ID                   IN VARCHAR2,
IN_DELIVERY_DATE                IN DATE
)
RETURN VARCHAR2
AS

v_block_flag  varchar2(2) := 'N';

BEGIN

    select 'Y' into v_block_flag
    from ftd_apps.florist_blocks
    where florist_id = in_florist_id
    and block_start_date <= in_delivery_date
    and (block_type = 'O' or
        block_end_date >= in_delivery_date);

    RETURN v_block_flag;

    EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN 'N';

END IS_FLORIST_BLOCKED;

PROCEDURE VIEW_FLORIST_DAY_CLOSURES
(
IN_FLORIST_ID                   IN FLORIST_HOURS.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given florist
        information.

Input:
        florist_id                      varchar2

Output:
        cursor containing the record from florist_hours for the given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
    SELECT
       INITCAP(m.day_of_week) day_of_week,
       decode(m.open_close_code, null, 'Open','F','Closed','A','Closed AM','P','Closed PM') jde_status,
       (select count(*) from ftd_apps.florist_hours_override fho
       where fho.florist_id = m.florist_id) as "override_count"
    FROM  florist_hours m
    WHERE m.florist_id=in_florist_id
    ORDER BY 
      CASE
          WHEN m.day_of_week = 'SUNDAY' THEN 1
          WHEN m.day_of_week = 'MONDAY' THEN 2
          WHEN m.day_of_week = 'TUESDAY' THEN 3
          WHEN m.day_of_week = 'WEDNESDAY' THEN 4
          WHEN m.day_of_week = 'THURSDAY' THEN 5
          WHEN m.day_of_week = 'FRIDAY' THEN 6
          WHEN m.day_of_week = 'SATURDAY' THEN 7
     END ASC;

END VIEW_FLORIST_DAY_CLOSURES;

PROCEDURE VIEW_CLOSURE_OVERRIDE 
(
IN_FLORIST_ID                   IN FLORIST_HOURS_OVERRIDE.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given florist
        information.

Input:
        florist_id                      varchar2

Output:
        cursor containing the record from florist_hours_override for the given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
    SELECT
        INITCAP(m.day_of_week) day_of_week,
        to_char(m.override_date,'MM/DD/YYYY') override_date,
        to_char(m.created_on ,'MM/DD/YYYY HH24:MI') created_on
    from florist_hours_override m
    WHERE m.florist_id=in_florist_id
    order by m.override_date;

END VIEW_CLOSURE_OVERRIDE ;

FUNCTION IS_FLORIST_OPEN_FOR_DELIVERY
(
IN_FLORIST_ID      IN VARCHAR2,
IN_DELIVERY_DATE   IN DATE
)
RETURN VARCHAR2
AS

v_flag CHAR(1);
v_open_close_code varchar2(10);
v_override_date date;
v_pm_cutoff_time varchar2(100);
v_time_zone varchar2(100);
v_state_code varchar2(100);

BEGIN

    select fh.open_close_code,
        (select fho.override_date
        from ftd_apps.florist_hours_override fho
        where fh.florist_id = fho.florist_id
        and fh.day_of_week = fho.day_of_week
        and fho.override_date = trunc(in_delivery_date))
    into v_open_close_code, v_override_date
    from ftd_apps.florist_hours fh
    where fh.florist_id = in_florist_id
    and fh.day_of_week = trim(to_char(in_delivery_date, 'DAY'));

    select fm.state, sm.time_zone
    into v_state_code, v_time_zone
    from ftd_apps.florist_master fm
    join ftd_apps.state_master sm
    on sm.state_master_id = fm.state
    where fm.florist_id = in_florist_id;

    select value
    into v_pm_cutoff_time
    from frp.global_parms
    where context = 'FTDAPPS_PARMS'
    and name = 'PM_CLOSED_DELIVERY_CUTOFF';

    v_pm_cutoff_time := ftd_apps.misc_pkg.change_times_timezone(v_pm_cutoff_time, v_time_zone, v_state_code);

    v_flag := 'N';

    if v_open_close_code is null then
        if v_override_date is null then
            -- no open/close code and no override
            v_flag := 'Y';
        else
            -- no open/close code but with an override is closed
            --v_flag := 'N';
            -- EROS does not have the concept of overriding an open date so they are still open
            v_flag := 'Y';
        end if;
    else
        if v_override_date is not null then
            -- any open/close code with an override is open
            v_flag := 'Y';
        else
            if v_open_close_code in ('F', 'A') then
                -- Full day and AM closed are always closed
                v_flag := 'N';
            else
                -- PM closed, check cutoff time
                if in_delivery_date > sysdate then
                    v_flag := 'Y';
                else
                    if to_number(to_char(sysdate, 'hh24mi')) < to_number(v_pm_cutoff_time) then
                        -- before cutoff
                        v_flag := 'Y';
                    else
                        -- after cutoff
                        v_flag := 'N';
                    end if;
                end if;
            end if;
        end if;
    end if;

    return v_flag;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN return 'Y';
        -- there should always be a record in florist_hours
        -- but if not, assume Open

END IS_FLORIST_OPEN_FOR_DELIVERY;

FUNCTION IS_FLORIST_OPEN_IN_EROS
(
IN_FLORIST_ID      IN VARCHAR2
)
RETURN VARCHAR2
AS

v_flag CHAR(1) := 'Y';
v_open_close_code varchar2(10);
v_override_date date;
v_pm_cutoff_time varchar2(100);
v_eod_cutoff_time varchar2(100);
v_midnight_cutoff_time varchar2(100) := '2400';
v_noon_cutoff_time varchar2(100) := '1200';
v_local_time varchar2(100);
v_time_zone varchar2(100);
v_state_code varchar2(100);
v_open_close_code_temp varchar2(10);
v_override_date_temp date;
v_date date;

BEGIN

    select fm.state, sm.time_zone
    into v_state_code, v_time_zone
    from ftd_apps.florist_master fm
    join ftd_apps.state_master sm
    on sm.state_master_id = fm.state
    where fm.florist_id = in_florist_id;

    select value
    into v_pm_cutoff_time
    from frp.global_parms
    where context = 'FTDAPPS_PARMS'
    and name = 'PM_CLOSED_ORDER_CUTOFF';

    select value
    into v_eod_cutoff_time
    from frp.global_parms
    where context = 'FTDAPPS_PARMS'
    and name = 'EOD_CLOSED_ORDER_CUTOFF';
 
    v_date := trunc(sysdate);
    v_local_time := to_char(sysdate, 'hh24mi');
    if to_char(sysdate, 'hh24mi') < '0600' then
        v_local_time := '2400' + v_local_time;
        v_date := v_date - 1;
    end if;
    v_local_time := ftd_apps.misc_pkg.CHANGE_TIMES_TO_REC_TIMEZONE(v_local_time, v_time_zone, v_state_code);
    if to_number(v_local_time) >= to_number(v_midnight_cutoff_time) then
        v_date := v_date + 1;
    end if;
    
    select fh.open_close_code,
        (select fho.override_date
        from ftd_apps.florist_hours_override fho
        where fh.florist_id = fho.florist_id
        and fh.day_of_week = fho.day_of_week
        and fho.override_date = trunc(v_date))
    into v_open_close_code, v_override_date
    from ftd_apps.florist_hours fh
    where fh.florist_id = in_florist_id
    and fh.day_of_week = trim(to_char(v_date, 'DAY'));

    if to_number(v_local_time) >= to_number(v_eod_cutoff_time) and to_number(v_local_time) < to_number(v_midnight_cutoff_time)
            and (nvl(v_open_close_code, 'X') not in ('F', 'P') or (nvl(v_open_close_code, 'X') in ('F', 'P') and trim(to_char(v_date, 'DAY')) = 'SUNDAY')) then

        v_date := v_date + 1;
        select fh.open_close_code,
            (select fho.override_date
            from ftd_apps.florist_hours_override fho
            where fh.florist_id = fho.florist_id
            and fh.day_of_week = fho.day_of_week
            and fho.override_date = trunc(v_date))
        into v_open_close_code_temp, v_override_date_temp
        from ftd_apps.florist_hours fh
        where fh.florist_id = in_florist_id
        and fh.day_of_week = trim(to_char(v_date, 'DAY'));
        
        if trim(to_char(v_date, 'DAY')) not in ('MONDAY') or nvl(v_open_close_code_temp, 'Open') in ('F', 'A') then
            v_open_close_code := v_open_close_code_temp;
            v_override_date := v_override_date_temp;
            v_local_time := '0000';
        else
            v_date := v_date - 1;
        end if;

    end if;
    
    if to_number(v_local_time) >= to_number(v_midnight_cutoff_time) then
        v_local_time := v_local_time - '2400';
    end if;

    if trim(to_char(v_date, 'DAY')) = 'SUNDAY' then
        --everyone is open on Sunday
        v_flag := 'Y';
    else
    
        v_flag := 'N';

        if v_open_close_code is null then
            if v_override_date is null then
                -- no open/close code and no override
                v_flag := 'Y';
            else
                -- no open/close code but with an override is closed
                --v_flag := 'N';
                -- EROS does not have the concept of overriding an open date so they are still open
                v_flag := 'Y';
            end if;
        else
            if v_override_date is not null then
                -- any open/close code with an override is open
                v_flag := 'Y';
            else
                if v_open_close_code in ('F') then
                    -- Full day closed are always closed
                    v_flag := 'N';
                else
                    if v_open_close_code in ('A') then
                        if v_date > sysdate then
                            v_flag := 'N';
                        else
                            -- AM closed is open after 12:00
                            if to_number(v_local_time) < to_number(v_noon_cutoff_time) then
                                v_flag := 'N';
                            else
                                v_flag := 'Y';
                            end if;
                        end if;
                    else
                        -- PM closed, check cutoff time
                        if to_number(v_local_time) < to_number(v_pm_cutoff_time) then
                            -- before cutoff
                            v_flag := 'Y';
                        else
                            -- after cutoff
                            v_flag := 'N';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    
    end if;

    return v_flag;

    EXCEPTION
        WHEN OTHERS THEN return 'Y';
        -- there should always be a record in florist_hours
        -- but if not, assume Open
    
END IS_FLORIST_OPEN_IN_EROS;

FUNCTION IS_FLORIST_OPEN_SUNDAY
(
IN_FLORIST_ID      IN VARCHAR2
)
RETURN VARCHAR2
AS

v_flag CHAR(1);

BEGIN

    select decode(nvl(open_close_code, 'Y'), 'F', 'N', 'A', 'N', 'Y')
    into v_flag
    from FTD_APPS.FLORIST_HOURS
    where florist_id = in_florist_id
    and DAY_OF_WEEK = 'SUNDAY';
    
    return v_flag;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN return 'N';
        -- there should always be a record in florist_hours

END IS_FLORIST_OPEN_SUNDAY;

FUNCTION IS_FLORIST_OPEN_DATE_RANGE
(
IN_FLORIST_ID      IN VARCHAR2,
IN_START_DATE      IN DATE,
IN_END_DATE        IN DATE
)
RETURN VARCHAR2
AS

v_flag       CHAR(1);
v_temp_date  DATE;
v_temp_end_date  DATE;

BEGIN

    v_temp_date := in_start_date;
    IF in_end_date is null or in_end_date = '' then
    	v_temp_end_date := in_start_date;
    ELSE
    	v_temp_end_date := in_end_date;
    END IF;
    
    while v_temp_date <= v_temp_end_date loop
        v_flag := florist_query_pkg.is_florist_open_for_delivery(in_florist_id, v_temp_date);
        if v_flag = 'Y' then
            v_temp_date := v_temp_end_date + 1;
        else
            v_temp_date := v_temp_date + 1;
        end if;
        
    end loop;

    return v_flag;
    
END IS_FLORIST_OPEN_DATE_RANGE;

PROCEDURE VIEW_FLORIST_CODIFICATIONS
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
IN_CODIFICATION_ID              IN FLORIST_CODIFICATIONS.CODIFICATION_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the codification details for the
        given florist and codification id.

Input:
        florist_id                      varchar2
        codification_id					varchar2

Output:
        cursor containing the records from florist_codifications for the
        given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                c.florist_id,
                c.codification_id,
                c.block_start_date,
                c.block_end_date,
                case
                  when c.block_start_date is not null then 'Y'
                  else 'N'
                end blocked_flag,
                cm.description,
                cm.category_id
        FROM    florist_codifications c
        JOIN    codification_master cm
        ON      c.codification_id = cm.codification_id
        WHERE   c.florist_id = in_florist_id
        AND		c.codification_id = in_codification_id;

END VIEW_FLORIST_CODIFICATIONS;

PROCEDURE VIEW_FLORIST_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
IN_ZIP_CODE	                    IN FLORIST_ZIPS.ZIP_CODE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the zip code coverage
        of the given florist.

Input:
        florist_id                      varchar2
        zip_code                        varchar2

Output:
        cursor containing all records from florist_zips for the given florist

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                z.florist_id,
                z.zip_code,
                z.block_start_date,
                z.block_end_date,
                case
                  when z.block_start_date is not null then 'Y'
                  else 'N'
                end blocked_flag,
                z.cutoff_time,
                count_florists_in_zip(z.zip_code) florist_count,
                (select city from zip_code zc where zc.zip_code_id = z.zip_code and rownum = 1) city,
                z.assignment_type
        FROM    florist_zips z
        WHERE   z.florist_id = in_florist_id 
        AND		z.zip_code = in_zip_code;

END VIEW_FLORIST_ZIPS;

procedure get_florist_zip
(
in_florist_id                   in florist_zips.florist_id%type,
in_zip_code                     in florist_zips.zip_code%type,
out_cursor     					out types.ref_cursor
)
as

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a record from table florist_zips
         for the given florist_id and zip_code

Input:
        florist_id
		zip_code

Output:
        cursor containing florist_zip info
-----------------------------------------------------------------------------*/

begin
	
	open out_cursor for
	select	florist_id,
			zip_code,
			block_end_date,
			cutoff_time,
			assignment_type,
			decode (block_start_date, null, 'N', 'Y') block_indicator
	from	florist_zips
	where	florist_id = in_florist_id
			and zip_code = in_zip_code;

end get_florist_zip;

PROCEDURE VIEW_CODIFIED_PRODUCTS
(
IN_PRODUCT_ID                   IN CODIFIED_PRODUCTS.PRODUCT_ID%TYPE,
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
OUT_CURSOR     					OUT TYPES.REF_CURSOR
)

AS

BEGIN

OPEN OUT_CURSOR FOR
  SELECT (SELECT DISTINCT CP.CODIFICATION_ID FROM FTD_APPS.CODIFIED_PRODUCTS CP, FTD_APPS.FLORIST_CODIFICATIONS FC
    WHERE  CP.PRODUCT_ID = IN_PRODUCT_ID AND CP.CODIFICATION_ID = FC.CODIFICATION_ID) AS CODIFICATION_ID, 
      (SELECT DISTINCT FM.MINIMUM_ORDER_AMOUNT FROM  FTD_APPS.FLORIST_MASTER FM
    WHERE FM.FLORIST_ID = IN_FLORIST_ID) AS CODIFIED_MINIMUM FROM DUAL;
  
END VIEW_CODIFIED_PRODUCTS;


PROCEDURE GET_FLORIST_OVERRIDE
(
In_Florist_Id                   In Florist_Hours_Override.Florist_Id%Type,
In_Override_Date				        In Florist_Hours_Override.Override_Date%Type,
OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

BEGIN

    Open OUT_CUR For 
    SELECT FLORIST_ID,DAY_OF_WEEK,OVERRIDE_DATE
    from  FTD_APPS.Florist_Hours_Override
       	 Where Florist_Id = In_Florist_Id And To_Char(Override_Date,'DD-MON-YYYY') = To_Char(In_Override_Date,'DD-MON-YYYY');
    
END GET_FLORIST_OVERRIDE;

FUNCTION IS_FLORIST_SUSPENDED
(
IN_FLORIST_ID      IN VARCHAR2,
IN_START_DATE      IN DATE,
IN_END_DATE        IN DATE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:

-----------------------------------------------------------------------------*/

v_flag                  CHAR(1) := 'N';
v_suspend_start_date    ftd_apps.florist_suspends.suspend_start_date%type;
v_suspend_end_date      ftd_apps.florist_suspends.suspend_end_date%type;
v_suspend_type          ftd_apps.florist_suspends.suspend_type%type;
v_goto_flag             ftd_apps.florist_master.super_florist_flag%type;
v_buffer                integer;

BEGIN

    select fs.suspend_start_date,
        fs.suspend_end_date,
        fs.suspend_type,
        fm.super_florist_flag
    into v_suspend_start_date,
        v_suspend_end_date,
        v_suspend_type,
        v_goto_flag
    from florist_suspends fs
    join florist_master fm
    on fm.florist_id = fs.florist_id
    where fs.florist_id = in_florist_id;
    
    if v_suspend_start_date is null then
        v_flag := 'N';
    elsif (v_suspend_type <> 'G' and v_goto_flag = 'Y') then
        v_flag := 'N';
    else
    
        select value into v_buffer
        from frp.global_parms
        where context = 'UPDATE_FLORIST_SUSPEND'
        and name = 'BUFFER_MINUTES';
        
        if (sysdate between v_suspend_start_date - (v_buffer/1440) and v_suspend_end_date) then
            -- currently suspended
            v_flag := 'Y';
        else
            if in_end_date is null then
                -- no date range
                if in_start_date BETWEEN v_suspend_start_date - (v_buffer/1440) AND v_suspend_end_date then
                    v_flag := 'Y';
                end if;
            else
                -- date range
                if in_start_date between v_suspend_start_date - (v_buffer/1440) AND v_suspend_end_date and
                        in_end_date between v_suspend_start_date - (v_buffer/1440) AND v_suspend_end_date then
                    v_flag := 'Y';
                end if;
            end if;
        end if;
    end if;
    
    return v_flag;

EXCEPTION
   WHEN NO_DATA_FOUND THEN
      return 'N';
   
END IS_FLORIST_SUSPENDED;

FUNCTION IS_FLORIST_BLOCKED
(
IN_FLORIST_ID      IN VARCHAR2,
IN_START_DATE      IN DATE,
IN_END_DATE        IN DATE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:

-----------------------------------------------------------------------------*/

v_block_type          varchar2(100);
v_active_day     BOOLEAN DEFAULT FALSE;
v_block_day      BOOLEAN DEFAULT FALSE;
v_current_day    Date;
v_num_days       NUMBER;

begin

   IF IN_END_DATE is null THEN
      v_num_days := 0;
    ELSE
      v_num_days := trunc(IN_END_DATE) - trunc(IN_START_DATE);
    END IF;

    FOR i IN 0..v_num_days LOOP
      v_current_day := IN_START_DATE + i;
      v_block_type := null;
  
      BEGIN 
        SELECT block_type 
          into v_block_type
          from ftd_apps.florist_blocks
         where 1=1
           and florist_id = IN_FLORIST_ID
           and block_start_date <= v_current_day
           and (block_end_date >= v_current_day OR block_end_date is null);
        EXCEPTION WHEN NO_DATA_FOUND THEN
          v_active_day := true;
      END;    -- end exception block
      
      IF v_block_type is not null and v_block_type in ('S','H','O') THEN
        v_block_day := true;
      END IF;
      
    END LOOP;
  
    IF v_block_day and not v_active_day then
        return 'Y';
    ELSE
        return 'N';
    END IF;
    
END IS_FLORIST_BLOCKED;

PROCEDURE VIEW_FLORIST_CITIES
(
IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE,
OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is retrieving all the related cities for the florist id

Input:
        IN_FLORIST_ID       IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE

Output:
        cursor containing florist cities

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
    select fm.florist_id,
        fm.florist_name,
        fm.city,
        fm.state,
        fm.record_type,
        count_florists_in_city(fm.city_state_number) florist_count,
        fcb.block_start_date,
        fcb.block_end_date,
        decode(fm.record_type, 'R', fm.florist_id, fm.parent_florist_id) sort1,
        decode(fm.record_type, 'R', 1, 2) sort2
    from florist_master fm
    left outer join florist_city_blocks fcb
    on fcb.florist_id = fm.florist_id
    WHERE fm.top_level_florist_id = (
        select fm1.top_level_florist_id
        from florist_master fm1
        where fm1.florist_id = in_florist_id)
    and fm.status <> 'Inactive'
    order by decode(fm.record_type, 'R', fm.florist_id, fm.parent_florist_id),
        decode(fm.record_type, 'R', 1, 2),
        fm.florist_id;
    
END VIEW_FLORIST_CITIES;

FUNCTION COUNT_FLORISTS_IN_CITY
(
IN_CITY_STATE_NUMBER        IN FLORIST_MASTER.CITY_STATE_NUMBER%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the number of all active
        florists that cover the given city.

Input:
        city_state_number                varchar2

Output:
        count                   number

-----------------------------------------------------------------------------*/
v_count         number := 0;

CURSOR count_cur IS
        SELECT
                count (1)
        FROM    florist_master m
        WHERE   m.city_state_number = in_city_state_number
        AND     m.status <> 'Inactive'
        AND     florist_query_pkg.is_florist_open_for_delivery(m.florist_id, trunc(sysdate)) = 'Y'
        AND     florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
        AND     florist_query_pkg.is_florist_blocked(m.florist_id, trunc(sysdate), null) = 'N'
        AND     florist_query_pkg.is_florist_city_blocked(m.florist_id, trunc(sysdate), null) = 'N'
        AND     florist_query_pkg.is_florist_suspended(m.florist_id, trunc(sysdate), null) = 'N';

BEGIN

OPEN count_cur;
FETCH count_cur INTO v_count;
CLOSE count_cur;

RETURN v_count;

END COUNT_FLORISTS_IN_CITY;

PROCEDURE VIEW_FLORISTS_BY_CITY
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_DELIVERY_DATE                IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the all active florists
        that cover the same city_state_number for the given florist id.

Input:
        florist_id                varchar2

Output:
        cursor containing florist_id from florist_zips for the given zip code

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.florist_id,
                m.florist_name,
                m.record_type,
                m.status,
                is_florist_open_sunday(m.florist_id) sunday_delivery_flag,
                decode (m.mercury_flag, 'M', 'Y', 'N') mercury_flag,
                decode (m.super_florist_flag, 'Y', 'Y', 'N') super_florist_flag,
                get_florist_weight (m.florist_id) florist_weight
        FROM    florist_master m
        WHERE   m.city_state_number = (
                    select fm.city_state_number
                    from florist_master fm
                    where fm.florist_id = in_florist_id)
        AND     m.status <> 'Inactive'
        AND     florist_query_pkg.is_florist_open_for_delivery(m.florist_id, in_delivery_date) = 'Y'
        AND     florist_query_pkg.is_florist_open_in_eros(m.florist_id) = 'Y'
        AND     florist_query_pkg.is_florist_blocked(m.florist_id, in_delivery_date, null) = 'N'
        AND     florist_query_pkg.is_florist_city_blocked(m.florist_id, in_delivery_date, null) = 'N'
        AND     florist_query_pkg.is_florist_suspended(m.florist_id, in_delivery_date, null) = 'N'
        ORDER BY florist_weight desc, m.florist_id;

END VIEW_FLORISTS_BY_CITY;

FUNCTION IS_FLORIST_CITY_BLOCKED (
IN_FLORIST_ID      IN VARCHAR2,
IN_START_DATE      IN DATE,
IN_END_DATE        IN DATE
)
RETURN VARCHAR2
AS

v_florist_id     varchar2(100);
v_active_day     BOOLEAN DEFAULT FALSE;
v_block_day      BOOLEAN DEFAULT FALSE;
v_current_day    Date;
v_num_days       NUMBER;

begin

   IF IN_END_DATE is null THEN
      v_num_days := 0;
    ELSE
      v_num_days := trunc(IN_END_DATE) - trunc(IN_START_DATE);
    END IF;

    FOR i IN 0..v_num_days LOOP
      v_current_day := IN_START_DATE + i;
      v_florist_id := null;

      BEGIN
        SELECT florist_id
          into v_florist_id
          from florist_city_blocks
         where 1=1
           and florist_id = IN_FLORIST_ID
           and block_start_date <= v_current_day
           and (block_end_date >= v_current_day OR block_end_date is null);
        EXCEPTION WHEN NO_DATA_FOUND THEN
          v_active_day := true;
      END;    -- end exception block

      IF v_florist_id is not null THEN
        v_block_day := true;
      END IF;

    END LOOP;

    IF v_block_day and not v_active_day then
        return 'Y';
    ELSE
        return 'N';
    END IF;

END IS_FLORIST_CITY_BLOCKED;

PROCEDURE GET_GOTO_STATUS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning GoTo status for the given florist id.

Input:
        florist_id                varchar2

Output:
        cursor containing GoTo Status from florist_master for the given florist id

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                FLORIST_ID
        FROM    florist_master 
        WHERE   florist_id = IN_FLORIST_ID
        and SUPER_FLORIST_FLAG = 'Y';

END GET_GOTO_STATUS;

PROCEDURE IS_CITY_SEARCHED_ORDER
(
IN_MERCURY_ID                   IN MERCURY.MERCURY.MERCURY_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning GoTo status for the given florist id.

Input:
        florist_id                varchar2

Output:
        cursor containing GoTo Status from florist_master for the given florist id

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select m.mercury_id
from mercury.mercury m
left outer join clean.order_florist_used ofu
on ofu.order_detail_id = to_number(m.reference_number)
and ofu.florist_id = m.filling_florist
where 1=1
and m.mercury_order_number = IN_MERCURY_ID
and m.MSG_TYPE = 'FTD'
and ofu.created_on = (select max(ofu1.created_on)
    from clean.order_florist_used ofu1
    where ofu1.order_detail_id = to_number(m.reference_number)
    and ofu1.florist_id = m.filling_florist
    and ofu1.selection_data like 'City');


END IS_CITY_SEARCHED_ORDER;

PROCEDURE VIEW_HOLIDAY_PRODUCTS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select to_char(delivery_date, 'mm/dd/yyyy') as "delivery_date",
        count(*) as "product_count"
        from holiday_products
        group by delivery_date
        order by delivery_date;
        
END VIEW_HOLIDAY_PRODUCTS;

PROCEDURE VIEW_HOLIDAY_PRODUCTS_DETAIL
(
IN_DELIVERY_DATE                IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select hp.product_id, pm.status
        from holiday_products hp
        join product_master pm
        on pm.product_id = hp.product_id
        where delivery_date = in_delivery_date
        order by product_id;

END VIEW_HOLIDAY_PRODUCTS_DETAIL;

PROCEDURE GET_PAS_STATES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT STATE_MASTER_ID as "stateMasterId",
	    STATE_NAME      as "stateName",
	    COUNTRY_CODE    as "countryCode",
	    TIME_ZONE       as "timeZone"
	FROM STATE_MASTER
	WHERE nvl(country_code, 'US') in ('US', 'CAN')
	AND state_name not like 'Armed Forces%'
	ORDER BY nvl(country_code, 'AA'), STATE_NAME;

END GET_PAS_STATES;

PROCEDURE GET_EROS_CITIES
(
IN_CITY_NAME                    IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select trim(ecs.city_name) as "city_name",
            esc.state_id
        from efos_city_state_stage ecs
        join efos_state_codes esc
        on esc.state_code = substr(ecs.city_state_code, 1, 2)
        join state_master sm
        on sm.state_master_id = esc.state_id
        where trim(ecs.city_name) like upper(IN_CITY_NAME) || '%'
        order by trim(ecs.city_name), esc.state_id;

END GET_EROS_CITIES;

PROCEDURE VERIFY_EROS_CITY
(
IN_CITY_NAME                    IN VARCHAR2,
IN_STATE_CODE                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR exists_cur IS
    SELECT   city_state_code
    FROM     efos_city_state ecs
    JOIN     efos_state_codes esc
      ON     esc.state_code = substr(ecs.city_state_code, 1, 2)
    JOIN     state_master sm
      ON     sm.state_master_id = esc.state_id
    WHERE    trim(ecs.city_name) = IN_CITY_NAME
      AND    esc.state_id = IN_STATE_CODE;

v_city_state_code varchar2(1000);

BEGIN

    OPEN exists_cur;
    FETCH exists_cur INTO v_city_state_code;
    CLOSE exists_cur;
    
    if v_city_state_code is not null then
        OUT_STATUS := 'Y';
    else
        OUT_STATUS := 'N';
    end if;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VERIFY_EROS_CITY;

PROCEDURE GET_EXCLUSION_ZONE_DETAIL_LIST
(
OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select h.exclusion_zone_header_id,
            exclusion_description,
            to_char(block_start_date, 'mm/dd/yyyy') block_start_date,
            to_char(block_end_date, 'mm/dd/yyyy') block_end_date,
            (select count(*)
                from exclusion_zone_detail d
                where d.exclusion_zone_header_id = h.exclusion_zone_header_id
                and d.block_source = 'CITY') city_cnt,
            (select count(*)
                from exclusion_zone_detail d
                where d.exclusion_zone_header_id = h.exclusion_zone_header_id
                and d.block_source = 'STATE') state_cnt,
            (select count(*)
                from exclusion_zone_detail d
                where d.exclusion_zone_header_id = h.exclusion_zone_header_id
                and d.block_source = 'ZIP') zip_cnt,
            case
                when block_end_date >= trunc(sysdate) then 'Y'
                else 'N'
            end remove_flag
        from exclusion_zone_header h
	where block_end_date > sysdate - 30
        order by block_end_date desc, h.exclusion_zone_header_id desc;

END GET_EXCLUSION_ZONE_DETAIL_LIST;

PROCEDURE GET_EXCLUSION_ZONE_BY_ID
(
IN_HEADER_ID                    IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select h.exclusion_zone_header_id,
            exclusion_description,
            to_char(block_start_date, 'mm/dd/yyyy') block_start_date,
            to_char(block_end_date, 'mm/dd/yyyy') block_end_date,
            (select count(*)
                from exclusion_zone_detail d
                where d.exclusion_zone_header_id = h.exclusion_zone_header_id
                and d.block_source = 'CITY') city_cnt,
            (select count(*)
                from exclusion_zone_detail d
                where d.exclusion_zone_header_id = h.exclusion_zone_header_id
                and d.block_source = 'STATE') state_cnt,
            (select count(*)
                from exclusion_zone_detail d
                where d.exclusion_zone_header_id = h.exclusion_zone_header_id
                and d.block_source = 'ZIP') zip_cnt,
            case
                when block_end_date >= trunc(sysdate) then 'Y'
                else 'N'
            end remove_flag
        from exclusion_zone_header h
	where exclusion_zone_header_id = in_header_id;

END GET_EXCLUSION_ZONE_BY_ID;

PROCEDURE GET_EXCLUSION_ZONE_DETAILS
(
IN_HEADER_ID                    IN VARCHAR2,
IN_BLOCK_SOURCE                 IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select block_source_key
	from exclusion_zone_detail
	where exclusion_zone_header_id = IN_HEADER_ID
	and block_source = IN_BLOCK_SOURCE
	order by block_source_key;

END GET_EXCLUSION_ZONE_DETAILS;

FUNCTION IS_BLOCKED_SUSPENDED_CLOSED
(
IN_FLORIST_ID            IN VARCHAR2,
IN_START_DATE            IN DATE,
IN_END_DATE              IN DATE,
IN_ZIP_BLOCK_START_DATE  IN DATE,
IN_ZIP_BLOCK_END_DATE    IN DATE,
IN_COD_BLOCK_START_DATE  IN DATE,
IN_COD_BLOCK_END_DATE    IN DATE,
IN_CHECK_SUSPEND         IN VARCHAR2
)
RETURN VARCHAR2
AS

v_block_suspend_closed_flag     char(1);
v_temp_flag                     char(1);
v_end_date                      date;

BEGIN

    v_end_date := in_end_date;
    if v_end_date is null then
        v_end_date := in_start_date;
    end if;
    
    FOR loop_date IN
    (
        SELECT in_start_date + ROWNUM - 1 delivery_date
        FROM dual
        CONNECT BY LEVEL <= v_end_date - in_start_date + 1
    )
    LOOP

        v_block_suspend_closed_flag := 'N';
    
        v_temp_flag := florist_query_pkg.is_florist_blocked(in_florist_id, loop_date.delivery_date, null);
        if v_temp_flag = 'Y' then
            v_block_suspend_closed_flag := 'Y';
        else
            v_temp_flag := florist_query_pkg.is_florist_suspended(in_florist_id, loop_date.delivery_date, null);
            if v_temp_flag = 'Y' and in_check_suspend = 'Y' then
                v_block_suspend_closed_flag := 'Y';
            else
                v_temp_flag := florist_query_pkg.is_florist_open_for_delivery(in_florist_id, loop_date.delivery_date);
                if v_temp_flag = 'N' then
                    v_block_suspend_closed_flag := 'Y';
                else
                    v_temp_flag := florist_query_pkg.is_florist_open_in_eros(in_florist_id);
                    if v_temp_flag = 'N' then
                        v_block_suspend_closed_flag := 'Y';
                    else
                        if in_zip_block_start_date is not null then
                            v_temp_flag := misc_pkg.is_it_within_blocked_dates(in_zip_block_start_date, in_zip_block_end_date, 0, loop_date.delivery_date, null);
                            if v_temp_flag = 'Y' then
                                v_block_suspend_closed_flag := 'Y';
                            end if;
                        end if;
                        if in_cod_block_start_date is not null then
                            v_temp_flag := misc_pkg.is_it_within_blocked_dates(in_cod_block_start_date, in_cod_block_end_date, 0, loop_date.delivery_date, null);
                            if v_temp_flag = 'Y' then
                                v_block_suspend_closed_flag := 'Y';
                            end if;
                        end if;
                    end if;
                end if;
            end if;
        end if;
          
        if v_block_suspend_closed_flag = 'N' then
            exit;
        end if;
    
    END LOOP loop_date;

    return v_block_suspend_closed_flag;
    
END IS_BLOCKED_SUSPENDED_CLOSED;

END FLORIST_QUERY_PKG;
/
