CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_FLORIST_PRODUCTS_BULK (
 floristId in varchar2
)
return varchar2
as

--=========================================================================
-- NOTE:  THIS PROCEDURE IS A TEMPORARY SOLUTION FOR SCRUB BULK ORDERS
--        FOR THE HP REPLACEMENT PHASE II PROJECT.  THE ONLY DIFFERENCE
--        BETWEEN THIS PROC AND OE_GET_FLORIST_PRODUCTS IS THE
--        LENGTH OF THE VARIABLE PRODUCTVAL.
--        WHEN OE_GET_FLORIST_PRODUCTS IS MODIFIED TO
--        HANDLE A VARCHAR2 GREATER THAN 255, THIS PROCEDURE SHOULD
--        BE DROPPED AND THE ORIGINAL PROC SHOULD BE USED.
--=========================================================================

productVal varchar2(4000);
cursor cur_cursor is
    select PRODUCT_ID from FLORIST_PRODUCTS
    where FLORIST_ID = floristId;
my_val cur_cursor%ROWTYPE;

begin

  for my_val in cur_cursor
    loop
      productVal := CONCAT(productVal, my_val.PRODUCT_ID);
      productVal := CONCAT(productVal, ' ');
    end loop;

 return productVal;
end
;
.
/
