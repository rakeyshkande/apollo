CREATE OR REPLACE
TRIGGER ftd_apps.product_master_aiu_status_trg
AFTER INSERT OR UPDATE OR DELETE OF STATUS ON ftd_apps.product_master
REFERENCING new as new old as old
FOR EACH ROW

DECLARE

    CURSOR amazon_product_cur(in_product_id IN varchar2) IS
        select *
        from ptn_amazon.az_product_master
        where product_id = in_product_id;

    v_count         number := 0;

BEGIN

    IF INSERTING or (:old.STATUS <> :new.STATUS) THEN

        FOR product_rec in amazon_product_cur(:new.product_id) LOOP

            select count(*) into v_count
            from ptn_amazon.az_inventory_feed
            where product_id = :new.product_id
            and feed_status = 'NEW';

            if v_count = 0 then

                insert into ptn_amazon.az_inventory_feed (
                    AZ_INVENTORY_FEED_ID,
                    PRODUCT_ID,
                    FEED_STATUS,
                    CREATED_ON,
                    CREATED_BY,
                    UPDATED_ON,
                    UPDATED_BY)
                values (
                    ptn_amazon.az_inventory_feed_id_sq.nextval,
                    :new.product_id,
                    'NEW',
                    sysdate,
                    'SYS',
                    sysdate,
                    'SYS');
            end if;

        END LOOP;

    END IF;

END PRODUCT_MASTER_AIU_STATUS_TRG;
/
