CREATE OR REPLACE
PACKAGE BODY ftd_apps.FLORIST_MAINT_PKG AS

TYPE varchar_tab_typ IS TABLE OF VARCHAR2(100) INDEX BY PLS_INTEGER;

PROCEDURE UPDATE_FLORIST_LOCK
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_LOCK_INDICATOR               IN VARCHAR2,
IN_LOCKED_BY                    IN FLORIST_MASTER.LOCKED_BY%TYPE,
OUT_LOCKED_BY                   OUT FLORIST_MASTER.LOCKED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for locking or unlocking the given
        florist.

Input:
        florist_id                      varchar2
        lock indicator                  varchar2 (Y or N)
        locked by                       varchar2

Output:
        locked by                       varchar2 (csr id if the florist is already locked)
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF in_lock_indicator = 'Y' THEN
        out_locked_by := florist_query_pkg.is_florist_locked (in_florist_id, in_locked_by);

        IF out_locked_by IS NOT NULL THEN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'Florist is locked';
        ELSE
                UPDATE  florist_master
                SET
                        locked_by = in_locked_by,
                        locked_date = sysdate
                WHERE   florist_id = in_florist_id;

                OUT_STATUS := 'Y';
        END IF;

ELSE
        UPDATE  florist_master
        SET
                locked_by = null,
                locked_date = null
        WHERE   florist_id = in_florist_id
        AND     locked_by = in_locked_by;

        OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_LOCK;


PROCEDURE INSERT_FLORIST_PROFILE
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_FLORIST_NAME                 IN FLORIST_MASTER.FLORIST_NAME%TYPE,
IN_ADDRESS                      IN FLORIST_MASTER.ADDRESS%TYPE,
IN_CITY                         IN FLORIST_MASTER.CITY%TYPE,
IN_STATE                        IN FLORIST_MASTER.STATE%TYPE,
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE,
IN_PHONE_NUMBER                 IN FLORIST_MASTER.PHONE_NUMBER%TYPE,
IN_FAX_NUMBER                   IN FLORIST_MASTER.FAX_NUMBER%TYPE,
IN_EMAIL_ADDRESS                IN FLORIST_MASTER.EMAIL_ADDRESS%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
IN_ALT_PHONE_NUMBER             IN FLORIST_MASTER.ALT_PHONE_NUMBER%TYPE,
IN_TERRITORY                    IN FLORIST_MASTER.TERRITORY%TYPE,
IN_OWNERS_NAME                  IN FLORIST_MASTER.OWNERS_NAME%TYPE,
IN_RECORD_TYPE                  IN FLORIST_MASTER.RECORD_TYPE%TYPE,
IN_STATUS                       IN FLORIST_MASTER.STATUS%TYPE,
IN_VENDOR_FLAG                  IN FLORIST_MASTER.VENDOR_FLAG%TYPE,
IN_MINIMUM_ORDER_AMOUNT         IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating florist master for the
        given florist with the data from the florist profile page.

Input:
        florist_id                      varchar2
        florist_name                    varchar2
        address                         varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        phone_number                    varchar2
        fax_number                      varchar2
        email_address                   varchar2
        last_updated_by                 varchar2
        alt_phone_number                varchar2
        record_type                     varchar2
        status                          varchar2
        vendor_flag                     varchar2
        minimum_order_amount            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_action                varchar2(10);
v_florist_id            FLORIST_MASTER.FLORIST_ID%TYPE;
BEGIN

v_florist_id := UPPER(in_florist_id);
INSERT INTO florist_master
(
        florist_id,
        florist_name,
        address,
        city,
        state,
        zip_code,
        phone_number,
        fax_number,
        email_address,
        last_updated_by,
        last_updated_date,
        alt_phone_number,
        territory,
        owners_name,
        record_type,
        status,
        top_level_florist_id,
        vendor_flag,
        minimum_order_amount,
        suspend_override_flag
)
VALUES
(
        v_florist_id,
        in_florist_name,
        in_address,
        in_city,
        in_state,
        in_zip_code,
        in_phone_number,
        in_fax_number,
        in_email_address,
        in_last_updated_by,
        to_char(sysdate,'dd-MON-yy'),
        in_alt_phone_number,
        in_territory,
        in_owners_name,
        in_record_type,
        in_status,
        v_florist_id,
        in_vendor_flag,
        in_minimum_order_amount,
        'N'
);

OUT_STATUS := 'Y';

IF in_vendor_flag = 'N' THEN
        -- insert codify all codifications for the new florist
        insert_new_florist_codified (v_florist_id, in_last_updated_by, out_status, out_message);
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_FLORIST_PROFILE;


PROCEDURE UPDATE_VENDOR_WEIGHT
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_FLORIST_WEIGHT               IN FLORIST_MASTER.FLORIST_WEIGHT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating florist master weight for
        vendors.

Input:
        florist_id                      varchar2
        florist_weight                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_action                varchar2(10);

BEGIN

UPDATE  florist_master
SET
        florist_weight = NVL(in_florist_weight, florist_weight)
WHERE   florist_id = in_florist_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_VENDOR_WEIGHT;


PROCEDURE UPDATE_FLORIST_PROFILE
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_FLORIST_NAME                 IN FLORIST_MASTER.FLORIST_NAME%TYPE,
IN_ADDRESS                      IN FLORIST_MASTER.ADDRESS%TYPE,
IN_CITY                         IN FLORIST_MASTER.CITY%TYPE,
IN_STATE                        IN FLORIST_MASTER.STATE%TYPE,
IN_ZIP_CODE                     IN FLORIST_MASTER.ZIP_CODE%TYPE,
IN_PHONE_NUMBER                 IN FLORIST_MASTER.PHONE_NUMBER%TYPE,
IN_FAX_NUMBER                   IN FLORIST_MASTER.FAX_NUMBER%TYPE,
IN_EMAIL_ADDRESS                IN FLORIST_MASTER.EMAIL_ADDRESS%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
IN_ALT_PHONE_NUMBER             IN FLORIST_MASTER.ALT_PHONE_NUMBER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating florist master for the
        given florist with the data from the florist profile page.

Input:
        florist_id                      varchar2
        florist_name                    varchar2
        address                         varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        phone_number                    varchar2
        fax_number                      varchar2
        email_address                   varchar2
        last_updated_by                 varchar2
        alt_phone_number                varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_action                varchar2(10);
BEGIN

UPDATE  florist_master
SET
        florist_name = NVL(in_florist_name, florist_name),
        address = NVL(in_address, address),
        city = NVL(in_city, city),
        state = NVL(in_state, state),
        zip_code = NVL(in_zip_code, zip_code),
        phone_number = NVL(in_phone_number, phone_number),
        fax_number = NVL(in_fax_number, fax_number),
        email_address = NVL(in_email_address, email_address),
        last_updated_by = NVL(in_last_updated_by, last_updated_by),
        last_updated_date = to_char(sysdate,'dd-MON-yy'),
        alt_phone_number = NVL(in_alt_phone_number, alt_phone_number)
WHERE   florist_id = in_florist_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_PROFILE;


PROCEDURE UPDATE_FLORIST_CODIFIED_MISC
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_MERCURY_FLAG                 IN FLORIST_MASTER.MERCURY_FLAG%TYPE,
IN_MINIMUM_ORDER_AMOUNT         IN FLORIST_MASTER.MINIMUM_ORDER_AMOUNT%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating florist master for the
        given florist with data from the florist codification page

Input:
        florist_id                      varchar2
        mercury_flag                    varchar2
        minimum_order_amount            number
        last_updated_by                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := 'Y';

UPDATE  florist_master
SET
        mercury_flag = decode (in_mercury_flag, 'Y', 'M', null),
        minimum_order_amount = NVL(in_minimum_order_amount, minimum_order_amount),
        last_updated_by = in_last_updated_by,
        last_updated_date = to_char(sysdate,'dd-MON-yy')
WHERE   florist_id = in_florist_id;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_CODIFIED_MISC;


PROCEDURE UPDATE_FLORIST_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_ZIPS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_ZIPS.BLOCK_END_DATE%TYPE,
IN_CUTOFF_TIME                  IN FLORIST_ZIPS.CUTOFF_TIME%TYPE,
IN_UPDATED_BY                   IN VARCHAR2,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ASSIGNMENT_TYPE              IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting the florist
        zip information.

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        zip_code                        varchar2
        last_update_date                date
        block_indicator                 varchar2 (Y or N)
        block_start_date                date
        block_end_date                  date
        cutoff_time                     varchar2
        updated by                      varchar2
        additional comment              varchar2 (addition to the comment for a particular app)
        assignment_type                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
        SELECT  count (1),
                max (decode (block_start_date, null, 'N', 'Y')) current_block_indicator,
                max (block_start_date) block_start_date
        FROM    florist_zips
        WHERE   florist_id = in_florist_id
        AND     zip_code = in_zip_code;

v_check                 number;
v_current_block_indicator varchar2(1);
v_comment_block         varchar2 (100);
v_block_start_date      FLORIST_ZIPS.BLOCK_START_DATE%TYPE;
v_action                varchar2(10);
v_order_detail_id       varchar2(10);
v_start_index           number;
v_end_index             number;

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_check, v_current_block_indicator, v_block_start_date;
CLOSE exists_cur;

IF in_block_indicator = 'N' THEN
        v_block_start_date := null;
ELSE
    IF in_block_start_date IS NULL THEN
        IF v_block_start_date IS NULL THEN
            v_block_start_date := trunc(sysdate);
        END IF;
    ELSE
        v_block_start_date := in_block_start_date;
    END IF;
END IF;

out_status := 'Y';

IF v_check > 0 THEN

        UPDATE  florist_zips
        SET
                last_update_date = sysdate,
                block_start_date = v_block_start_date,
                block_end_date = in_block_end_date,
                cutoff_time = NVL(in_cutoff_time, cutoff_time),
                assignment_type = NVL(in_assignment_type, assignment_type)
        WHERE   florist_id = in_florist_id
        AND     zip_code = in_zip_code;

        IF v_current_block_indicator <> in_block_indicator THEN
                IF in_block_indicator = 'N' THEN
                        v_comment_block := ' was changed from Blocked to Active';
                ELSE
                        v_comment_block := ' was Blocked';
                END IF;
				
				IF in_updated_by = 'MARS' THEN
				  v_order_detail_id := '';
				  IF in_additional_comment IS NOT NULL THEN
					v_start_index := INSTR(in_additional_comment,'(');
					v_end_index := INSTR(in_additional_comment,')');
					v_order_detail_id := substr(in_additional_comment, v_start_index,(v_end_index - v_start_index + 1));
				  END IF;
				  
					-- insert the block comment on the update if the zip is block changed
					INSERT_FLORIST_COMMENTS
					(
							IN_FLORIST_ID => in_florist_id,
							IN_COMMENT_TYPE => 'Zip Codes',
							IN_FLORIST_COMMENT => 'Zipcode was blocked for ' || to_char(v_block_start_date,'MM/dd/yyyy') || '-' || to_char(in_block_end_date, 'MM/dd/yyyy') || ' by MARS auto View Queue handling ' || v_order_detail_id,
							IN_MANAGER_ONLY_FLAG => 'N',
							IN_CREATED_BY => in_updated_by,
							OUT_STATUS => out_status,
							OUT_MESSAGE => out_message
					);
				END IF;

                -- insert the block comment on the update if the zip is block changed
                INSERT_FLORIST_COMMENTS
                (
                        IN_FLORIST_ID => in_florist_id,
                        IN_COMMENT_TYPE => 'Zip Codes',
                        IN_FLORIST_COMMENT => 'Zip Code ' || in_zip_code || v_comment_block || ' ' || in_additional_comment,
                        IN_MANAGER_ONLY_FLAG => 'N',
                        IN_CREATED_BY => in_updated_by,
                        OUT_STATUS => out_status,
                        OUT_MESSAGE => out_message
                );
        END IF;

        v_action := 'Update';

ELSE

        INSERT INTO florist_zips
        (
                florist_id,
                zip_code,
                last_update_date,
                block_start_date,
                block_end_date,
                cutoff_time,
                assignment_type
        )
        VALUES
        (
                UPPER(in_florist_id),
                UPPER(in_zip_code),
                sysdate,
                v_block_start_date,
                in_block_end_date,
                in_cutoff_time,
                in_assignment_type
        );

        v_action := 'Insert';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_ZIPS;


PROCEDURE DELETE_FLORIST_ZIPS
(
IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the florist zip information.

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        zip_code                        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

OUT_STATUS := 'Y';

DELETE FROM florist_zips
WHERE   florist_id = in_florist_id
AND     zip_code = in_zip_code;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_FLORIST_ZIPS;


PROCEDURE DELETE_NON_MEMBER_FLORIST_ZIPS
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting any florist zip information
        for florists that are no longer "signed up" for particular zips.  
        This is run after a member load and linking.
        
        The purpose of the florist_zips_removal table is to keep a (brief) history 
        of the zips that were removed (so they can be reviewed after-the-fact).

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

type varchar_tab_type is table of varchar2(100) index by pls_integer;
v_top_florist_id_tab        varchar_tab_type;
v_zip_code_tab              varchar_tab_type;
v_do_not_erase              number;

-- Cursor containing list of zips (for associated top-level florist) that 
-- should be removed from florist_zips.
--
CURSOR delete_cur IS
    SELECT DISTINCT
           top_level_florist_id, zip_code
    FROM   ftd_apps.florist_zips_removal
    WHERE  latest_flag = 'Y';

-- Cursor to determine if specified zip should NOT be removed from florist_zips.
-- If assignment_type is S (for Save), as opposed to T (for Temporary) or null,
-- for any florist under top-level florist, then zip should NOT be removed.
--
CURSOR do_not_erase_cur (p_top_florist_id varchar2, p_zip_code varchar2) IS
    SELECT count(1)
    FROM   ftd_apps.florist_zips fz,
           ftd_apps.florist_master fm
    WHERE  fm.zip_master_florist_id = p_top_florist_id
    AND    fm.florist_id = fz.florist_id
    AND    fm.status <> 'Inactive'
    AND    fz.zip_code = p_zip_code
    AND    fz.assignment_type = 'S';
    
BEGIN

-- Reset flag (which represents most recent set of zips that were removed)
-- from last run.
--
UPDATE ftd_apps.florist_zips_removal
SET    latest_flag = null
WHERE  latest_flag = 'Y';


-- Purge old records from florist_zip_removals
--
DELETE FROM  
       ftd_apps.florist_zips_removal
WHERE  created_on < sysdate - 90;  -- Records older than 90 days


-- Get (and save) zips that are in florist_zips but are no longer available to 
-- associated florist.  We accomplish this by simply doing difference between
-- florist_zips and florist_master (since florist_master should have recently 
-- been updated by member load and linking). This list represents the 
-- florist_zip entries we need to remove (so we save to florist_zips_removal).
--
INSERT INTO 
       ftd_apps.florist_zips_removal
       (top_level_florist_id, zip_code, created_on, latest_flag)
       
        -- Current list of assigned zipcodes for each top-level florist
        --
        SELECT DISTINCT
               fm.zip_master_florist_id, 
               fz.zip_code,
               TRUNC(SYSDATE), 
               'Y'
        FROM   ftd_apps.florist_zips fz,
               ftd_apps.florist_master fm
        WHERE  fz.florist_id = fm.florist_id
        AND    fm.status <> 'Inactive'
        AND    fm.vendor_flag = 'N'
        --     Don't include any non-numeric zips (e.g., Canadian)
        AND    LENGTH(TRIM(TRANSLATE(fz.zip_code, ' 0123456789',' '))) IS NULL
        
    MINUS
    
        -- New list of available zipcodes for each top-level florist
        --
        SELECT DISTINCT
               fm.zip_master_florist_id, 
               zip.zip_code_id,
               TRUNC(SYSDATE),
               'Y'
        FROM   ftd_apps.florist_master fm,
               ftd_apps.florist_master cfm,
               ftd_apps.efos_city_state ecs,
               ftd_apps.efos_state_codes es,
               ftd_apps.zip_code zip
        WHERE
               cfm.zip_master_florist_id = fm.zip_master_florist_id
        AND    cfm.status <> 'Inactive'
        AND    fm.status <> 'Inactive'
        AND    cfm.city_state_number = ecs.city_state_code
        AND    substr(cfm.city_state_number,0,2) = es.state_code
        AND    zip.city = rtrim(ecs.city_name)
        AND    zip.state_id = es.state_id;


-- Loop over zips to be removed and delete from florist_zips (as long as
-- zip override was not set).
--
OPEN delete_cur;
FETCH delete_cur BULK COLLECT INTO v_top_florist_id_tab, v_zip_code_tab;
CLOSE delete_cur;

FOR x in 1..v_top_florist_id_tab.count LOOP
        v_do_not_erase := -1;
        OPEN do_not_erase_cur (v_top_florist_id_tab(x), v_zip_code_tab(x));
        FETCH do_not_erase_cur INTO v_do_not_erase;
        CLOSE do_not_erase_cur;

        IF v_do_not_erase = 1 THEN
        
            -- Zip override was set, so just save that fact in florist_zips_removal
            --
            UPDATE ftd_apps.florist_zips_removal
            SET    do_not_erase_flag = 'Y'
            WHERE  top_level_florist_id = v_top_florist_id_tab(x)
            AND    zip_code = v_zip_code_tab(x);

        ELSE
        
            -- Delete it
            --
            DELETE FROM 
                   ftd_apps.florist_zips 
            WHERE  zip_code = v_zip_code_tab(x)
            AND    florist_id IN
            (
                   SELECT florist_id 
                   FROM   ftd_apps.florist_master
                   WHERE  zip_master_florist_id = v_top_florist_id_tab(x)
            );

            out_status := 'Y';

        END IF;
END LOOP;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_NON_MEMBER_FLORIST_ZIPS;


PROCEDURE UPDATE_FLORIST_WEIGHTS
(
IN_FLORIST_ID                   IN FLORIST_MASTER.FLORIST_ID%TYPE,
IN_SUPER_FLORIST_FLAG           IN FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE,
IN_ADJUSTED_WEIGHT              IN FLORIST_MASTER.ADJUSTED_WEIGHT%TYPE,
IN_ADJUSTED_WEIGHT_START_DATE   IN FLORIST_MASTER.ADJUSTED_WEIGHT_START_DATE%TYPE,
IN_ADJUSTED_WEIGHT_END_DATE     IN FLORIST_MASTER.ADJUSTED_WEIGHT_END_DATE%TYPE,
IN_ADJUSTED_WEIGHT_PERM_FLAG    IN FLORIST_MASTER.ADJUSTED_WEIGHT_PERMENANT_FLAG%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
IN_SUSPEND_OVERRIDE_FLAG        IN FLORIST_MASTER.SUSPEND_OVERRIDE_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating florist master for the
        given florist with data from the florist codification page

Input:
        florist_id                      varchar2
        super_florist_flag              varchar2
        adjusted_weight                 number
        adjusted_weight_start_date      date
        adjusted_weight_end_date        date
        adjusted_weight_permenant_flag  varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR branch_cur IS
        SELECT  florist_id,
                super_florist_flag,
                florist_weight,
                initial_weight,
                adjusted_weight,
                adjusted_weight_start_date,
                adjusted_weight_end_date,
                adjusted_weight_permenant_flag
        FROM    florist_master
        WHERE   top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        AND     status <> 'Inactive';


CURSOR history_cur IS
        SELECT  florist_weight,
                initial_weight,
                adjusted_weight
        FROM    florist_master
        WHERE   florist_id = in_florist_id
        AND     NVL (adjusted_weight, -1) <> NVL (in_adjusted_weight, -1);

v_florist_id                    florist_master.florist_id%type;
v_super_florist_flag            florist_master.super_florist_flag%type;
v_florist_weight                florist_master.florist_weight%type;
v_initial_weight                florist_master.initial_weight%type;
v_adjusted_weight               florist_master.adjusted_weight%type;
v_adjusted_weight_start_date    florist_master.adjusted_weight_start_date%type;
v_adjusted_weight_end_date      florist_master.adjusted_weight_end_date%type;
v_adjusted_weight_perm_flag     florist_master.adjusted_weight_permenant_flag%type;

v_comment                       varchar2(1000);
BEGIN

out_status := 'Y';

OPEN branch_cur;
FETCH branch_cur INTO
        v_florist_id,
        v_super_florist_flag,
        v_florist_weight,
        v_initial_weight,
        v_adjusted_weight,
        v_adjusted_weight_start_date,
        v_adjusted_weight_end_date,
        v_adjusted_weight_perm_flag;

WHILE branch_cur%found LOOP

        IF NVL (in_adjusted_weight, -1) <> NVL (v_adjusted_weight, -1) THEN
                insert_florist_weight_history
                (
                        v_florist_id,
                        v_florist_weight,
                        v_initial_weight,
                        in_adjusted_weight,
                        in_last_updated_by,
                        out_status,
                        out_message
                );
        END IF;

        IF out_status = 'Y' THEN
                UPDATE  florist_master
                SET
                        super_florist_flag = in_super_florist_flag,
                        adjusted_weight = in_adjusted_weight,
                        adjusted_weight_start_date = in_adjusted_weight_start_date,
                        adjusted_weight_end_date = decode (in_adjusted_weight_perm_flag, 'Y', null, in_adjusted_weight_end_date),
                        adjusted_weight_permenant_flag = in_adjusted_weight_perm_flag,
                        suspend_override_flag = in_suspend_override_flag,
                        last_updated_by = in_last_updated_by,
                        last_updated_date = to_char(sysdate,'dd-MON-yy')
                WHERE   florist_id = v_florist_id;

                -- insert goto florist change comments
                IF out_status = 'Y' THEN
                        IF NVL(in_super_florist_flag, 'null check') <> NVL(v_super_florist_flag, 'null check') THEN
                                v_comment := 'Goto Florist was changed from ' || v_super_florist_flag || ' to ' || in_super_florist_flag;

                                IF in_florist_id <> v_florist_id THEN
                                        v_comment := v_comment || ' on florist ' || in_florist_id;
                                END IF;

                                INSERT_FLORIST_COMMENTS
                                (
                                        IN_FLORIST_ID => v_florist_id,
                                        IN_COMMENT_TYPE => 'Weight',
                                        IN_FLORIST_COMMENT => v_comment,
                                        IN_MANAGER_ONLY_FLAG => 'N',
                                        IN_CREATED_BY => in_last_updated_by,
                                        OUT_STATUS => out_status,
                                        OUT_MESSAGE => out_message
                                );
                        END IF;
                END IF;

                -- insert goto florist change comments
                IF out_status = 'Y' THEN
                        IF NVL(in_adjusted_weight, -1) <> NVL(v_adjusted_weight, -1) OR
                           NVL(in_adjusted_weight_start_date, to_date ('01/01/1001', 'mm/dd/yyyy')) <> NVL(v_adjusted_weight_start_date, to_date ('01/01/1001', 'mm/dd/yyyy')) OR
                           NVL(in_adjusted_weight_end_date, to_date ('01/01/1001', 'mm/dd/yyyy')) <> NVL(v_adjusted_weight_end_date, to_date ('01/01/1001', 'mm/dd/yyyy')) OR
                           NVL(in_adjusted_weight_perm_flag, 'N') <> NVL(v_adjusted_weight_perm_flag, 'N') THEN

                                IF in_adjusted_weight_perm_flag = 'N' THEN
                                        v_comment := 'Adjusted Weight ' || to_char (in_adjusted_weight) || ' from ' || to_char (in_adjusted_weight_start_date, 'mm/dd/yyyy') || ' to ' || to_char (in_adjusted_weight_end_date, 'mm/dd/yyyy');
                                ELSE
                                        v_comment := 'Adjusted Weight ' || to_char (in_adjusted_weight) || ' from ' || to_char (in_adjusted_weight_start_date, 'mm/dd/yyyy') || ' to no end date';
                                END IF;

                                IF in_florist_id <> v_florist_id THEN
                                        v_comment := v_comment || ' on florist ' || in_florist_id;
                                END IF;

                                INSERT_FLORIST_COMMENTS
                                (
                                        IN_FLORIST_ID => v_florist_id,
                                        IN_COMMENT_TYPE => 'Weight',
                                        IN_FLORIST_COMMENT => v_comment,
                                        IN_MANAGER_ONLY_FLAG => 'N',
                                        IN_CREATED_BY => in_last_updated_by,
                                        OUT_STATUS => out_status,
                                        OUT_MESSAGE => out_message
                                );
                        END IF;
                END IF;

        END IF;

        IF out_status = 'N' THEN
                exit;
        END IF;

        FETCH branch_cur INTO
                v_florist_id,
                v_super_florist_flag,
                v_florist_weight,
                v_initial_weight,
                v_adjusted_weight,
                v_adjusted_weight_start_date,
                v_adjusted_weight_end_date,
                v_adjusted_weight_perm_flag;

END LOOP;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLORIST_WEIGHTS;


PROCEDURE INSERT_FLORIST_WEIGHT_HISTORY
(
IN_FLORIST_ID                   IN FLORIST_WEIGHT_HISTORY.FLORIST_ID%TYPE,
IN_FLORIST_WEIGHT               IN FLORIST_WEIGHT_HISTORY.FLORIST_WEIGHT%TYPE,
IN_INITIAL_WEIGHT               IN FLORIST_WEIGHT_HISTORY.INITIAL_WEIGHT%TYPE,
IN_ADJUSTED_WEIGHT              IN FLORIST_WEIGHT_HISTORY.ADJUSTED_WEIGHT%TYPE,
IN_LAST_UPDATED_BY              IN FLORIST_WEIGHT_HISTORY.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the weight history for
        the florist

Input:
        florist_id                      varchar2
        florist_weight                  number
        initial_weight                  number
        adjusted_weight                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO florist_weight_history
(
        florist_id,
        history_date,
        florist_weight,
        initial_weight,
        adjusted_weight,
        last_updated_by
)
VALUES
(
        UPPER(in_florist_id),
        sysdate,
        in_florist_weight,
        in_initial_weight,
        in_adjusted_weight,
        in_last_updated_by
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_FLORIST_WEIGHT_HISTORY;


PROCEDURE INSERT_FLORIST_COMMENTS
(
IN_FLORIST_ID                   IN FLORIST_COMMENTS.FLORIST_ID%TYPE,
IN_COMMENT_TYPE                 IN FLORIST_COMMENTS.COMMENT_TYPE%TYPE,
IN_FLORIST_COMMENT              IN FLORIST_COMMENTS.FLORIST_COMMENT%TYPE,
IN_MANAGER_ONLY_FLAG            IN FLORIST_COMMENTS.MANAGER_ONLY_FLAG%TYPE,
IN_CREATED_BY                   IN FLORIST_COMMENTS.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting florist comments.

Input:
        florist_id                      varchar2
        comment_type                    varchar2
        florist_comment                 varchar2
        manager_only_flag               varchar2
        created_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO florist_comments
(
        florist_id,
        comment_date,
        comment_type,
        florist_comment,
        manager_only_flag,
        created_date,
        created_by
)
VALUES
(
        UPPER(in_florist_id),
        sysdate,
        in_comment_type,
        in_florist_comment,
        in_manager_only_flag,
        sysdate,
        in_created_by
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_FLORIST_COMMENTS;



PROCEDURE UPDATE_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting florist
        blocks.

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        block_type                      varchar2 (H, S, O, null - if no block)
        block_indicator                 varchar2 (Y or N)
        block_end_date                  date
        blocked_by_user_id              varchar2
        additional comment              varchar2 (addition to the comment for a particular app)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	UPDATE_FLORIST_BLOCKS(IN_FLORIST_ID, IN_BLOCK_TYPE, IN_BLOCK_INDICATOR, IN_BLOCK_END_DATE, IN_BLOCKED_BY_USER_ID, IN_ADDITIONAL_COMMENT, 'Y', OUT_STATUS, OUT_MESSAGE);

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.UPDATE_FLORIST_BLOCK - [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_BLOCKS;


PROCEDURE INSERT_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ENQUEUE_FLAG                 IN VARCHAR2,
IN_BLOCK_REASON                 IN FLORIST_BLOCKS.BLOCK_REASON%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting florist blocks.

        The available zips process for the given zip code is started if the enqueue flag is 'Y'.

Input:
        florist_id                      varchar2
        block_type                      varchar2 (H, S, O, null - if no block)
        block_indicator                 varchar2 (Y or N)
        block_start_date                date
        block_end_date                  date
        blocked_by_user_id              varchar2
        additional comment              varchar2 (addition to the comment for a particular app)
        enqueue_flag 			varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_florist_id            FLORIST_BLOCKS.FLORIST_ID%TYPE;
v_goto_florist          FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE;
v_exists                varchar2(1);
v_status                varchar2(100);
v_suspend_type          FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE;
v_action                varchar2(10);
v_comment_disposition   varchar2(20) := 'Blocks';
v_comment               varchar2(1000);
v_block_end_date        FLORIST_BLOCKS.BLOCk_END_DATE%TYPE;
v_suspend_start_date    FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE;
v_suspend_end_date      FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE;
v_remain_suspended      varchar2(10);
v_comment_action        varchar2(500);
v_record_type           varchar2(1);
branch_cur              sys_refcursor;



BEGIN

out_status := 'N';

out_message := 'Florist record not found or is inactive';

v_remain_suspended := 'N'; 

      OPEN branch_cur for 
        SELECT  f.florist_id,
                f.super_florist_flag,
                s.suspend_type,
                decode (s.florist_id, null, 'N', 'Y') exists_ind,
                decode (s.suspend_type, 'M', 'Mercury Suspend', 'G', 'GoTo Suspend') suspend_status,               
                nvl(suspend_start_date-(timezone_offset_in_minutes/1440),to_date('01/01/0001','MM/DD/YYYY')) suspend_start,
                nvl(suspend_end_date-(timezone_offset_in_minutes/1440),to_date('01/01/0001','MM/DD/YYYY')) suspend_end,
                record_type
                FROM    ftd_apps.florist_master f
        LEFT OUTER JOIN ftd_apps.florist_suspends s
        ON      f.florist_id = s.florist_id
        WHERE   f.top_level_florist_id = (select top_level_florist_id from ftd_apps.florist_master where florist_id = IN_FLORIST_ID)
        AND     f.status <> 'Inactive';

FETCH branch_cur INTO v_florist_id, v_goto_florist,v_suspend_type, v_exists, v_status,v_suspend_start_date, v_suspend_end_date, v_record_type;
  out_status := 'Y';

WHILE branch_cur%found LOOP

        /* Upate block data */
 
                INSERT INTO florist_blocks
                (
                        florist_id,
                        block_type,
                        block_start_date,
                        block_end_date,
                        blocked_by_user_id,
                        block_reason
                )
                VALUES
                (
                        UPPER(v_florist_id),
                        in_block_type,
                        in_block_start_date,
                        in_block_end_date,
                        in_blocked_by_user_id,
                        in_block_reason
                );

                v_action := 'Insert';


        IF in_block_indicator = 'Y' THEN
                -- set the status
                UPDATE  florist_master
                SET     status = 'Blocked'
                WHERE   florist_id = v_florist_id;
         END IF;


        -- build and insert the comments for the florist blocks
        IF v_florist_id = in_florist_id THEN
                -- create the comment for the updated florist
                v_comment := 'Florist was ';
        ELSE
                -- create the comment for the associated florist
                v_comment := 'Florist ' || in_florist_id || ': was ';
        END IF;

        CASE in_block_type
                WHEN 'S' THEN v_comment_action := 'Soft blocked for ' || to_char(in_block_start_date, 'mm/dd/yy') || '-' || to_char(in_block_end_date, 'mm/dd/yy');
                WHEN 'H' THEN v_comment_action :=  'Hard blocked for ' || to_char(in_block_start_date, 'mm/dd/yy') || '-' || to_char(in_block_end_date, 'mm/dd/yy');
                WHEN 'O' THEN v_comment_action := 'Opt Out blocked for ' || to_char(in_block_start_date, 'mm/dd/yy');
                ELSE  v_comment_action := v_comment_action || ' ';
         END CASE;

        v_comment := v_comment || v_comment_action || ' ' || in_additional_comment;

        -- insert the comment
        INSERT_FLORIST_COMMENTS
        (
                IN_FLORIST_ID => v_florist_id,
                IN_COMMENT_TYPE => v_comment_disposition,
                IN_FLORIST_COMMENT => v_comment,
                IN_MANAGER_ONLY_FLAG => 'N',
                IN_CREATED_BY => in_blocked_by_user_id,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );


        IF out_status = 'N' THEN
                exit;
        END IF;

        FETCH branch_cur INTO v_florist_id, v_goto_florist,v_suspend_type, v_exists, v_status, v_suspend_start_date, v_suspend_end_date, v_record_type;
END LOOP;

CLOSE branch_cur;




EXCEPTION WHEN OTHERS THEN
BEGIN
        IF branch_cur%isopen THEN
                CLOSE branch_cur;
        END IF;

         OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_FLORIST_BLOCKS;

PROCEDURE REMOVE_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_START_DATE             IN FLORIST_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ENQUEUE_FLAG                 IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing florist blocks.

        The available zips process for the given zip code is started if the enqueue flag is 'Y'.

Input:
        florist_id                      varchar2
        block_type                      varchar2 (H, S, O, null - if no block)
        block_indicator                 varchar2 (Y or N)
        block_start_date                date
        block_end_date                  date
        blocked_by_user_id              varchar2
        additional comment              varchar2 (addition to the comment for a particular app)
        enqueue_flag 			varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR blocked_cur IS
        SELECT  block_start_date
        FROM    florist_blocks
        WHERE   florist_id = in_florist_id;

CURSOR branch_cur IS
        SELECT  f.florist_id,
                nvl(f.super_florist_flag, 'N') super_florist_flag,
                s.suspend_type,
                decode (fb.florist_id, null, 'N', 'Y') exists_ind,
                decode (s.suspend_type, 'M', 'Mercury Suspend', 'G', 'GoTo Suspend') suspend_status,
                nvl(suspend_start_date-(timezone_offset_in_minutes/1440),to_date('01/01/0001','MM/DD/YYYY')) suspend_start,
                nvl(suspend_end_date-(timezone_offset_in_minutes/1440),to_date('01/01/0001','MM/DD/YYYY')) suspend_end,
                record_type,
                fb.block_type,
                fb.block_end_date,
                f.status previous_status
                FROM    florist_master f
                JOIN    florist_blocks fb
                  ON    fb.florist_id = f.florist_id
                  AND   (in_blocked_by_user_id <> 'FIT' or fb.blocked_by_user_id = in_blocked_by_user_id)
        LEFT OUTER JOIN florist_suspends s
        ON      f.florist_id = s.florist_id
        WHERE   f.top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        --AND     f.record_type = 'R'
        AND fb.florist_id = f.florist_id
        AND fb.block_start_date = IN_BLOCK_START_DATE
        --AND fb.block_end_date = IN_BLOCK_END_DATE
        --AND fb.block_type = IN_BLOCK_TYPE
        ;
        
CURSOR branch_future_block_cur (branch_florist_id varchar2) IS
        SELECT count(*)
        FROM florist_blocks fb
        WHERE 1=1
          AND fb.florist_id = branch_florist_id
          AND (fb.block_type = 'O' OR fb.block_end_date >= trunc(sysdate));

v_florist_id            FLORIST_BLOCKS.FLORIST_ID%TYPE;
v_goto_florist          FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE;
v_exists                varchar2(1);
v_status                varchar2(100);
v_block_start_date      FLORIST_BLOCKS.BLOCK_START_DATE%TYPE;
v_suspend_type          FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE;
v_action                varchar2(10);
v_comment_disposition   varchar2(20) := 'Blocks';
v_comment               varchar2(1000);
v_block_end_date        FLORIST_BLOCKS.BLOCk_END_DATE%TYPE;
v_suspend_start_date    FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE;
v_suspend_end_date      FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE;
v_remain_suspended      varchar2(10);
v_remain_blocked        varchar2(10);
v_comment_action        varchar2(500);
v_record_type           varchar2(1);
v_future_block_count    number;
v_block_type            varchar2(10);
v_previous_status       varchar2(100);

BEGIN

out_status := 'N';

out_message := 'Florist record not found or is inactive';

OPEN branch_cur;

FETCH branch_cur INTO v_florist_id, v_goto_florist,v_suspend_type, v_exists, v_status, v_suspend_start_date, v_suspend_end_date, v_record_type, v_block_type, v_block_end_date, v_previous_status;
out_status := 'Y';

WHILE branch_cur%found LOOP

        v_remain_suspended := 'N';
        v_remain_blocked := 'N';

        /* remove block data */
        IF v_exists = 'Y' THEN

                DELETE FROM  florist_blocks
                WHERE florist_id = v_florist_id
                  AND block_start_date = IN_BLOCK_START_DATE;

                v_action := 'Delete';
        END IF;

        /* If we are in a suspend time, then should remain suspend */
        /* Unless we are a goto florist then it has to be a goto suspend */
        IF (v_suspend_start_date < SYSDATE and v_suspend_end_date > SYSDATE) THEN
            IF (v_goto_florist <> 'Y') THEN
                v_remain_suspended := 'Y';
                v_comment_action := ' Unblocked but will remain suspended';
            ELSE
                IF v_suspend_type = 'G' THEN
                    v_remain_suspended := 'Y';
                    v_comment_action := ' Unblocked but will remain suspended';
                ELSE
                    v_remain_suspended := 'N';
                    v_comment_action := ' Unblocked';
                END IF;
            END IF;
        ELSE
               v_remain_suspended := 'N';
               v_comment_action := ' Unblocked';
        END IF;

        /* Check for any blocks in the future to see if we remain blocked */
        OPEN branch_future_block_cur (v_florist_id);
        FETCH branch_future_block_cur into v_future_block_count;
        CLOSE branch_future_block_cur;
        
        IF ( v_future_block_count > 0) THEN
            v_remain_blocked := 'Y';
        END IF;

        IF v_previous_status <> 'Inactive' THEN
            IF (v_remain_suspended = 'Y') THEN
                UPDATE  florist_master
                SET     status = decode(v_suspend_type, 'G', 'GoTo Suspend', 'Mercury Suspend')
                WHERE   florist_id = v_florist_id;
            ELSIF (v_remain_blocked = 'Y') THEN
                UPDATE  florist_master
                SET     status = 'Blocked'
                WHERE   florist_id = v_florist_id;
            ELSIF (v_remain_blocked = 'N') THEN
                UPDATE  florist_master
                SET     status = 'Active'
                WHERE   florist_id = v_florist_id;
            END IF;
        END IF;


        -- build and insert the comments for the florist blocks
        IF v_florist_id = in_florist_id THEN
                -- create the comment for the updated florist
                v_comment := 'Florist ';
        ELSE
                -- create the comment for the associated florist
                v_comment := 'Florist ' || in_florist_id || ' ';
        END IF;

        CASE v_block_type
                WHEN 'S' THEN v_comment_action := 'Soft block for ' || to_char(in_block_start_date, 'mm/dd/yy') || '-' || to_char(v_block_end_date, 'mm/dd/yy');
                WHEN 'H' THEN v_comment_action :=  'Hard block for ' || to_char(in_block_start_date, 'mm/dd/yy') || '-' || to_char(v_block_end_date, 'mm/dd/yy');
                WHEN 'O' THEN v_comment_action := 'Opt Out block for ' || to_char(in_block_start_date, 'mm/dd/yy');
                ELSE  v_comment_action := v_comment_action || ' ';
        END CASE;

        v_comment := v_comment || v_comment_action || ' was removed ' || in_additional_comment;

        -- insert the comment
        INSERT_FLORIST_COMMENTS
        (
                IN_FLORIST_ID => v_florist_id,
                IN_COMMENT_TYPE => v_comment_disposition,
                IN_FLORIST_COMMENT => v_comment,
                IN_MANAGER_ONLY_FLAG => 'N',
                IN_CREATED_BY => in_blocked_by_user_id,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );

        IF out_status = 'N' THEN
                exit;
        END IF;

        FETCH branch_cur INTO v_florist_id, v_goto_florist,v_suspend_type, v_exists, v_status, v_suspend_start_date, v_suspend_end_date, v_record_type, v_block_type, v_block_end_date, v_previous_status;
END LOOP;

CLOSE branch_cur;




EXCEPTION WHEN OTHERS THEN
BEGIN
        IF branch_cur%isopen THEN
                CLOSE branch_cur;
        END IF;

         OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_FLORIST_BLOCKS;


PROCEDURE UPDATE_FLORIST_MERCURY_SUSPEND
(

IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_START_DATE           IN FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE,
IN_SUSPEND_END_DATE             IN FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE,
IN_TIMEZONE_OFFSET_IN_MINUTES   IN FLORIST_SUSPENDS.TIMEZONE_OFFSET_IN_MINUTES%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2

)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting florist
        suspends

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        suspend_type                    varchar2 (M, G, null - if no suspend)
        suspend_indicator               varchar2 (Y or N)
        suspend_end_date                date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE_FLORIST_SUSPEND(in_florist_id,
                       'M',
                       null,                    -- suspend indicator doesn't mean anything with a mercury suspend
                       in_suspend_start_date,   -- suspend date
                       in_suspend_end_date,     -- resume date
                       null,
                       null,
                       null,
                       null,
                       in_timezone_offset_in_minutes ,
                       out_status,
                       out_message);

END UPDATE_FLORIST_MERCURY_SUSPEND;



PROCEDURE UPDATE_FLORIST_GOTO_SUSPEND
(
IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_INDICATOR            IN VARCHAR2,
IN_GOTO_FLORIST_SUS_MERC_ID     IN FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_MERC_ID%TYPE,
IN_GOTO_FLORIST_RES_MERC_ID     IN FLORIST_SUSPENDS.GOTO_FLORIST_RESUME_MERC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting florist
        suspends

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        suspend_indicator               varchar2 (Y or N)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE_FLORIST_SUSPEND(in_florist_id,
                       'G',
                        in_suspend_indicator,
                        null,
                        null,
                        SYSDATE,
                        in_goto_florist_sus_merc_id,
                        SYSDATE,
                        in_goto_florist_res_merc_id,
                        null,
                        out_status,
                        out_message);




END UPDATE_FLORIST_GOTO_SUSPEND;



PROCEDURE UPDATE_FLORIST_SUSPEND
(
IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_TYPE                 IN FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE,
IN_SUSPEND_INDICATOR            IN VARCHAR2,
IN_SUSPEND_START_DATE           IN FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE,
IN_SUSPEND_END_DATE             IN FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE,
IN_GOTO_FLORIST_SUS_RECEIVED    IN FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_RECEIVED%TYPE,
IN_GOTO_FLORIST_SUS_MERC_ID     IN FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_MERC_ID%TYPE,
IN_GOTO_FLORIST_RES_RECEIVED    IN FLORIST_SUSPENDS.GOTO_FLORIST_RESUME_RECEIVED%TYPE,
IN_GOTO_FLORIST_RES_MERC_ID     IN FLORIST_SUSPENDS.GOTO_FLORIST_RESUME_MERC_ID%TYPE,
IN_TIMEZONE_OFFSET_IN_MINUTES    IN FLORIST_SUSPENDS.TIMEZONE_OFFSET_IN_MINUTES%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting florist
        suspends.  This generic process takes a suspend type and determines
        which fields to update base on this.
        There is a lot of business logic in this stored procedure to determine
        based on a goto suspend vs a mercury suspend.
        NOTE: THIS ONLY SAVES THE DATA and enters comments to florist it
        does NOT suspend or resume any florists or call updates to other systems.
        The florist_suspend_resume does this.

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        suspend_type                    varchar2 (M, G, null - if no suspend)
        suspend_indicator               varchar2 (Y or N) Y suspend, N Resume
        suspend_start_date              date
        suspend_end_date                date
        in_goto_florist_sus_received    date
        in_goto_florist_sus_merc_id     varchar2  Mercury Id of the Go To Suspend
        in_goto_florist_res_received    date
        in_goto_florist_res_merc_id     varchar2  Mercury Id of the Go To Resume
        in_timezone_offset_in_minutes   number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR branch_cur IS
        SELECT  f.florist_id,
                decode (s.florist_id, null, 'N', 'Y') exists_ind,
                s.suspend_type,
                nvl (f.super_florist_flag, 'N') goto_florist,
                NVL (s.suspend_processed_flag, 'N') currently_suspended,
                s.suspend_start_date,
                s.suspend_end_date
                FROM    florist_master f
        LEFT OUTER JOIN florist_suspends s
        ON      f.florist_id = s.florist_id
        WHERE   f.top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        --AND     f.record_type = 'R'
        AND     f.status <> 'Inactive';

v_florist_id            FLORIST_BLOCKS.FLORIST_ID%TYPE;
v_exists                varchar2(1);
v_current_suspend_type  FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE;
v_goto_florist          FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE;

v_suspend_type          FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE;
v_currently_suspended   FLORIST_SUSPENDS.SUSPEND_PROCESSED_FLAG%TYPE;
v_suspend_start_date    FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE;
v_suspend_end_date      FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE;
v_action                varchar2(10);
v_comment_disposition   varchar2(20) := 'Profile';
v_comment               varchar2(4000);
v_comment_action        varchar2(4000);
v_await_merc_suspend    varchar2(2000) := frp.misc_pkg.get_global_parm_value ('UPDATE_FLORIST_SUSPEND','GOTO_AWAIT_MERC_SUSPEND');
v_goto_sus_res          varchar2(1);
v_force_resume          varchar2(1);
v_merc_date_overlap     varchar2(1);
v_buffer                integer;

BEGIN

/* If we do not get inside the loop then there is no active florist for
this and the status will come back as an error */
out_status := 'N';
out_message := 'Florist record not found or is inactive: ' || in_florist_id;

OPEN branch_cur;
FETCH branch_cur INTO v_florist_id, v_exists, v_current_suspend_type, v_goto_florist,
                      v_currently_suspended, v_suspend_start_date, v_suspend_end_date;

WHILE branch_cur%found LOOP

        out_status := 'Y';
        out_message := 'N';

        /*  Already Exists Just Update
         */
        IF v_exists = 'Y' THEN

                /* Update Mercury Type Suspend
                 */
                IF in_suspend_type = 'M'  THEN
                        v_suspend_type := in_suspend_type;

                        -- Determine if new Merc Suspend date range overlaps with current one
                        IF (v_suspend_start_date is not null) AND
                           (v_suspend_end_date > in_suspend_start_date) AND
                           (v_suspend_start_date < in_suspend_end_date)
                        THEN
                            v_merc_date_overlap := 'Y';
                        ELSE
                            v_merc_date_overlap := 'N';
                        END IF;

                        IF v_current_suspend_type = 'G' THEN
                                v_comment_action := 'Mercury Suspend Received while florist in Go To Suspend.  Suspend Date:' ||
                                    NVL(to_char(in_suspend_start_date-in_timezone_offset_in_minutes/1440,'MM/DD/YYYY HH:MI AM'), 'None') ||
                                    ' Resume Date:' || NVL(to_char(in_suspend_end_date-in_timezone_offset_in_minutes/1440,'MM/DD/YYYY HH:MI AM'), 'None') ||
                                    '. (Times in Central Time)';

                                -- Already in Go To Suspend, so preserve it if either:
                                --   1. This new Merc date range overlaps the current one.
                                --   2. There is no current Merc.  This means Go To Susp
                                --      was received before Merc Susp.
                                -- Otherwise, existing Merc Susp is being canceled
                                -- and replaced with this one, hence Go To Susp
                                -- should be dropped.
                                --
                                IF v_suspend_start_date is not null THEN
                                    IF v_merc_date_overlap = 'Y' THEN
                                        v_suspend_type := 'G';
                                    END IF;
                                ELSE
                                    -- No current Merc Susp
                                    v_suspend_type := 'G';
                                END IF;

                        ELSE /* We are not in goto suspend */
                             v_comment_action := 'Mercury Suspend/Resume Received.  Suspend Date:' || NVL(to_char(in_suspend_start_date-in_timezone_offset_in_minutes/1440,'MM/DD/YYYY HH:MI AM'), 'None')
                             || ' Resume Date:' || NVL(to_char(in_suspend_end_date-in_timezone_offset_in_minutes/1440,'MM/DD/YYYY HH:MI AM'), 'None') ||'. (Times in Central Time)';
                        END IF;

                        -- If current Merc Suspend is active and new Merc Suspend date range
                        -- does not overlap, we need to resume.  If Merc Susp date range overlaps
                        -- we still need to resume if start date is in future or end date in past.
                        -- We set flag so florist_suspend_resume gets invoked. Since we're currently
                        -- suspended and new start date is in future it will treat as resume.  It will
                        -- also reset florist_blocks.suspend_processed_flag back to N.
                        --
                        IF v_currently_suspended = 'Y' AND
                           (v_merc_date_overlap = 'N'  OR
                            in_suspend_start_date > sysdate OR in_suspend_end_date <= sysdate)
                        THEN
                             v_comment_action := v_comment_action || '  Original Merc Suspend was resumed.';
                             v_force_resume := 'Y';
                        END IF;

                        UPDATE  florist_suspends
                        SET
                                suspend_type = v_suspend_type,
                                suspend_start_date = nvl (in_suspend_start_date, suspend_start_date),
                                suspend_end_date = in_suspend_end_date,
                                timezone_offset_in_minutes = in_timezone_offset_in_minutes
                        WHERE   florist_id = v_florist_id;


                /* Update Goto Type Suspend or Resume only for goto florists
                 */
                ELSIF v_goto_florist = 'Y' THEN
                        IF in_suspend_indicator = 'Y'  THEN /* This is goto suspend */
                                CASE v_current_suspend_type
                                WHEN 'G' THEN
                                        v_comment_action := 'Go To Suspend Received. Already have an active Go To Suspend. ';
                                WHEN 'M' THEN
                                        v_comment_action := 'Go To Suspend Received as Message #: ' || in_goto_florist_sus_merc_id;
                                        UPDATE  florist_suspends
                                        SET
                                                suspend_type = in_suspend_type,
                                                goto_florist_suspend_received = in_goto_florist_sus_received,
                                                goto_florist_suspend_merc_id = in_goto_florist_sus_merc_id
                                        WHERE   florist_id = v_florist_id;
                                        v_goto_sus_res := 'Y';

                                ELSE
                                        v_comment_action := 'Go To Suspend Received. No Mercury Suspend Received Yet.  Will Await ' || v_await_merc_suspend  || ' minutes for a mercury suspend.';
                                        UPDATE  florist_suspends
                                        SET
                                                suspend_type = in_suspend_type,
                                                goto_florist_suspend_received = in_goto_florist_sus_received,
                                                goto_florist_suspend_merc_id = in_goto_florist_sus_merc_id
                                        WHERE   florist_id = v_florist_id;
                                END CASE;
                        ELSE  /* This is a goto resume */
                                IF v_current_suspend_type = 'G' THEN  /* If this is in Goto Suspend Mode turn it back to Mercury Suspend */
                                        v_comment_action := 'Go To Resume Received. Message #:' || in_goto_florist_res_merc_id;

                                        UPDATE  florist_suspends
                                        SET
                                                suspend_type = 'M',
                                                goto_florist_resume_received =  in_goto_florist_res_received,
                                                goto_florist_resume_merc_id  = in_goto_florist_res_merc_id
                                        WHERE   florist_id = v_florist_id;
                                        v_goto_sus_res := 'Y';
                                ELSE
                                        v_comment_action := 'Go To Resume Received but no Mercury Suspend was in effect';
                                        UPDATE  florist_suspends
                                        SET
                                                goto_florist_resume_received =  in_goto_florist_res_received,
                                                goto_florist_resume_merc_id  = in_goto_florist_res_merc_id
                                        WHERE   florist_id = v_florist_id;
                                END IF;
                        END IF;
                END IF;
                v_action := 'Update';


        /* Record does not already exist we must insert it
         */
        ELSE
                IF in_suspend_type='M'  THEN /* Update Mercury Type Suspend */

                     v_comment_action := 'Mercury Suspend/Resume Received.  Suspend Date:' || NVL(to_char(in_suspend_start_date-in_timezone_offset_in_minutes/1440,'MM/DD/YYYY HH:MI AM'), 'None')
                                     || ' Resume Date:' || NVL(to_char(in_suspend_end_date-in_timezone_offset_in_minutes/1440,'MM/DD/YYYY HH:MI AM'), 'None') ||'. (Times in Central Time)';

                        INSERT INTO florist_suspends
                        (
                                florist_id,
                                suspend_type,
                                suspend_start_date,
                                suspend_end_date,
                                timezone_offset_in_minutes,
                                suspend_processed_flag

                        )
                        VALUES
                        (
                                UPPER(v_florist_id),
                                   in_suspend_type,
                                in_suspend_start_date,
                                in_suspend_end_date,
                                in_timezone_offset_in_minutes,
                                'N'
                        );

                ELSE /* Insert Goto Type Suspend */
                        IF in_suspend_indicator = 'Y'  THEN /* This is goto suspend */
                                v_comment_action := 'Go To Suspend Received. No Mercury Suspend Received Yet.  Will Await ' || v_await_merc_suspend  || ' minutes for a mercury suspend.';
                                INSERT INTO florist_suspends
                                (
                                        florist_id,
                                        suspend_type,
                                        goto_florist_suspend_received ,
                                        goto_florist_suspend_merc_id,
                                        suspend_processed_flag
                                )
                                VALUES
                                (
                                        UPPER(v_florist_id),
                                        in_suspend_type,
                                        in_goto_florist_sus_received ,
                                        in_goto_florist_sus_merc_id,
                                        'N'
                                );
                        ELSE /* This is a resume since there is no Goto Suspend we store the data but do not update */
                                v_comment_action := 'Go To Resume Received but no Mercury Suspend was in effect';
                                INSERT INTO florist_suspends
                                (
                                        florist_id,
                                        suspend_type,
                                        goto_florist_resume_received ,
                                        goto_florist_resume_merc_id,
                                        suspend_processed_flag

                                )
                                VALUES
                                (
                                        v_florist_id,
                                        null,
                                        in_goto_florist_res_received ,
                                        in_goto_florist_res_merc_id,
                                        'N'
                                );

                        END IF;

             END IF;
             v_action := 'Insert';
        END IF;

        IF v_comment_action is not null THEN
                -- build and insert the comments for the florist blocks
                IF v_florist_id = in_florist_id THEN
                        -- create the comment for the updated florist
                        v_comment := 'Florist update due to ';
                ELSE
                        -- create the comment for the associated florist
                        v_comment := 'Florist ' || in_florist_id || ': caused an update for this florist due to  ';
                END IF;

                v_comment := v_comment || v_comment_action      ;

                INSERT_FLORIST_COMMENTS
                        (
                                IN_FLORIST_ID => v_florist_id,
                                IN_COMMENT_TYPE => v_comment_disposition,
                                IN_FLORIST_COMMENT => v_comment,
                                IN_MANAGER_ONLY_FLAG => 'N',
                                IN_CREATED_BY => user,
                                OUT_STATUS => out_status,
                                OUT_MESSAGE => out_message
                        );

                IF out_status = 'N' THEN
                        exit;
                END IF;

        END IF;

        FETCH branch_cur INTO v_florist_id, v_exists, v_current_suspend_type, v_goto_florist,
                              v_currently_suspended, v_suspend_start_date, v_suspend_end_date;
END LOOP;

CLOSE branch_cur;

select value into v_buffer
from frp.global_parms
where context = 'UPDATE_FLORIST_SUSPEND'
and name = 'BUFFER_MINUTES';

IF (v_force_resume = 'Y') THEN
      FLORIST_SUSPEND_RESUME
        (
                IN_FLORIST_ID => in_florist_id,
                IN_SUSPEND_TYPE =>'M',
                IN_SUSPEND_INDICATOR => 'N',
                OUT_STATUS => out_status,
                OUT_MESSAGE =>out_message
        );
ELSIF (v_goto_sus_res = 'Y') THEN
      FLORIST_SUSPEND_RESUME
        (
                IN_FLORIST_ID => in_florist_id,
                IN_SUSPEND_TYPE =>'G',
                IN_SUSPEND_INDICATOR => in_suspend_indicator,
                OUT_STATUS => out_status,
                OUT_MESSAGE =>out_message
        );
ELSIF (IN_SUSPEND_START_DATE - (v_buffer/1440) < sysdate) THEN
      FLORIST_SUSPEND_RESUME
        (
                IN_FLORIST_ID => in_florist_id,
                IN_SUSPEND_TYPE =>'M',
                IN_SUSPEND_INDICATOR => 'Y',
                OUT_STATUS => out_status,
                OUT_MESSAGE =>out_message
        );
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF branch_cur%isopen THEN
                CLOSE branch_cur;
        END IF;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_SUSPEND;


PROCEDURE FLORIST_SUSPEND_RESUME
(
IN_FLORIST_ID                   IN FLORIST_SUSPENDS.FLORIST_ID%TYPE,
IN_SUSPEND_TYPE                 IN FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE,
IN_SUSPEND_INDICATOR            IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting florist
        suspends.  This generic process takes a suspend type and determines
        which fields to update base on this.
        There is a lot of business logic in this stored procedure to determine
        based on a goto suspend vs a mercury suspend.

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        suspend_type                    varchar2 - not used (these values are retrieved from the suspend record)
        suspend_indicator               varchar2 - not used

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR branch_cur IS
        SELECT  f.florist_id,
                s.suspend_type,
                NVL (suspend_start_date, to_date('01/02/0001','MM/DD/YYYY'))-NVL(timezone_offset_in_minutes/1440,0) suspend_start_date,
                NVL (suspend_end_date, to_date('01/02/0001','MM/DD/YYYY'))-NVL(timezone_offset_in_minutes/1440,0) suspend_end_date,
                NVL (s.suspend_processed_flag, 'N') currently_suspended,
                NVL(f.super_florist_flag,'N') goto_florist,
                s.goto_florist_suspend_received
                FROM    florist_master f
        LEFT OUTER JOIN florist_suspends s
        ON      f.florist_id = s.florist_id
        WHERE   f.top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        --AND     f.record_type = 'R'
        AND     f.status <> 'Inactive';


--                NVL (block_start_date, to_date('01/02/0001','MM/DD/YYYY')) block_start,
--                NVL (block_end_date, to_date('01/02/0001','MM/DD/YYYY')) block_end,
--                block_type,
CURSOR florist_block_cur (w_cur_florist_id varchar2) IS
    select distinct 'B'
    from ftd_apps.florist_blocks fb
    where 1=1
      and fb.florist_id = w_cur_florist_id
      and (fb.block_type = 'O'
           or fb.block_end_date >= trunc(sysdate)
           );



CURSOR goto_comment_cur (p_florist_id varchar2, p_suspend_date date) IS
        SELECT  count (1)
        FROM    florist_comments
        left outer join frp.global_parms gp
        on gp.context = 'UPDATE_FLORIST_SUSPEND'
        and gp.name = 'BUFFER_MINUTES'
        WHERE   florist_id = p_florist_id
        AND     comment_date >= p_suspend_date - (gp.value/1440)
        AND     florist_comment like '%Go To Florist Mercury Suspend Start Time has passed.  Orders will continue to go to this florist unless a Go To Suspend is received.%';


v_goto_florist_suspend_recvd     FLORIST_SUSPENDS.GOTO_FLORIST_SUSPEND_RECEIVED%TYPE;

v_florist_id                    FLORIST_SUSPENDS.FLORIST_ID%TYPE;
v_suspend_type                  FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE;
v_suspend_start_date            FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE;
v_suspend_end_date              FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE;
v_currently_suspended           FLORIST_SUSPENDS.SUSPEND_PROCESSED_FLAG%TYPE;
v_goto_florist                  FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE;
v_florist_still_blocked         varchar2(1);

v_action                        varchar2(10);
v_comment_disposition           varchar2(20) := 'Profile';
v_comment                       varchar2(4000);
v_comment_action                varchar2(4000);
v_florist_suspend_status        varchar2(100);
v_today                         date := sysdate;
v_buffer                        integer := 0;

v_goto_comment_check            number := 0;
v_await_merc_suspend    varchar2(2000) := frp.misc_pkg.get_global_parm_value ('UPDATE_FLORIST_SUSPEND','GOTO_AWAIT_MERC_SUSPEND');


BEGIN

/* If we do not get inside the loop then there is no active florist for
this and the status will come back as an error */
out_status := 'N';
out_message := 'Florist record not found or is inactive';

select value into v_buffer
from frp.global_parms
where context = 'UPDATE_FLORIST_SUSPEND'
and name = 'BUFFER_MINUTES';

OPEN branch_cur;
FETCH branch_cur INTO v_florist_id, v_suspend_type, v_suspend_start_date, v_suspend_end_date,
                                     v_currently_suspended, v_goto_florist, v_goto_florist_suspend_recvd;
--v_block_start_date, v_block_end_date, v_block_type, 

WHILE branch_cur%found LOOP

        out_status := 'Y';
        out_message := null;

        IF v_currently_suspended = 'N' and
           v_suspend_start_date - (v_buffer/1440) <= v_today and
           v_suspend_end_date > v_today THEN /* This is a Suspend */

                IF v_goto_florist <> 'Y' THEN  /* This is not a goto florist*/
                                               /* Go ahead and suspend */

                        /* Set suspend processed flag to Y so we do not process again */
                        UPDATE  florist_suspends
                        SET     suspend_processed_flag = 'Y'
                        WHERE   florist_id = v_florist_id;

                        v_action := 'Update';
                        v_comment_action := 'Mercury Suspend has taken effect.';

                ELSE /* This is a goto florist */
                        IF v_suspend_type  = 'G' THEN /* If the suspend typs is G then suspend */
                                UPDATE  florist_suspends
                                SET     suspend_processed_flag = 'Y'
                                WHERE   florist_id = v_florist_id;

                                v_action := 'Update';
                                v_comment_action := 'Go To Suspend is now in effect. ';

                        ELSE /* Suspend type is not a Goto - Do NOT suspend */
                                v_suspend_type := NULL; /* do not update the florist master status */

                                OPEN goto_comment_cur (v_florist_id, v_suspend_start_date);
                                FETCH goto_comment_cur INTO v_goto_comment_check;
                                CLOSE goto_comment_cur;

                                IF v_goto_comment_check = 0 THEN
                                        v_action := 'Update';
                                        v_comment_action := 'Go To Florist Mercury Suspend Start Time has passed.  Orders will continue to go to this florist unless a Go To Suspend is received. ';
                                ELSE
                                        v_comment_action := null;
                                END IF;
                        END IF;

                END IF;

        ELSIF v_currently_suspended = 'Y' and
              (v_suspend_start_date - (v_buffer/1440) > v_today OR
               v_suspend_end_date <= v_today) THEN /* This is a resume */

                IF v_suspend_end_date <= v_today THEN /* No pending suspends left so clear all suspend fields */
                        UPDATE  florist_suspends
                        SET
                                suspend_type = NULL,
                                suspend_start_date = NULL,
                                suspend_end_date = NULL,
                                timezone_offset_in_minutes = NULL,
                                goto_florist_suspend_received = NULL,
                                goto_florist_suspend_merc_id = NULL,
                                goto_florist_resume_received = NULL,
                                goto_florist_resume_merc_id = NULL,
                                suspend_processed_flag = 'N'
                        WHERE   florist_id = v_florist_id;
                ELSE    /* there is a pending suspend so just update the suspend processed flag to N */
                        UPDATE  florist_suspends
                        SET
                                goto_florist_suspend_received = NULL,
                                goto_florist_suspend_merc_id = NULL,
                                goto_florist_resume_received = NULL,
                                goto_florist_resume_merc_id = NULL,
                                suspend_processed_flag = 'N'
                        WHERE   florist_id = v_florist_id;
                END IF;

                OPEN florist_block_cur (v_florist_id);
                FETCH florist_block_cur into v_florist_still_blocked;
                CLOSE florist_block_cur;
                
                IF v_florist_still_blocked = 'B' THEN
--                IF v_block_start_date < SYSDATE AND (v_block_end_date > SYSDATE OR v_block_type = 'O') THEN
                        /*  Still blocked */
                        v_suspend_type := 'B';
                        IF v_currently_suspended = 'N' THEN  /* This means a Goto was received but we are waiting for a Merc Suspend */
                                v_comment_action := 'Time expired on Goto Wait for Mercury Suspend. No change to status.';
                        ELSE
                                v_comment_action := 'Mercury Resume processed. Block is still in effect.';
                        END IF;

                ELSE /* Go ahead and unsuspend */

                        IF v_currently_suspended = 'N' THEN  /* This means a Goto was received but we are waiting for a Merc Suspend */
                                v_suspend_type := NULL;         /* do not update the florist master status */
                                v_action := 'Delete';
                                v_comment_action := 'Time expired on Goto Wait for Mercury Suspend. No change to status.';
                        ELSE
                                v_action := 'Delete';
                                v_comment_action := 'Mercury Resume processed.';
                                v_suspend_type := 'N';          /* update the florist master status to active */
                        END IF;
                END IF;

        ELSIF v_currently_suspended = 'Y' and
              v_suspend_type = 'M' and
              v_goto_florist = 'Y' THEN /* this is goto florist with a mercury suspend it should be set to active */

                UPDATE  florist_suspends
                SET
                        goto_florist_suspend_received = NULL,
                        goto_florist_suspend_merc_id = NULL,
                        goto_florist_resume_received = NULL,
                        goto_florist_resume_merc_id = NULL,
                        suspend_processed_flag = 'N'
                WHERE   florist_id = v_florist_id;

                v_action := 'Delete';
                v_comment_action := 'Go To Resume processed. Goto florist is active. ';

        ELSIF v_suspend_end_date <= v_today THEN        /* if it gets in here then we have suspend */



		            IF v_suspend_type = 'G' THEN
		                   IF  v_goto_florist_suspend_recvd + (v_await_merc_suspend/1440) <= SYSDATE  THEN  /* undo Goto Suspend */
		                     UPDATE  florist_suspends       /* that was received after it expired */
				         SET                     /*  we can simply clear it at this point */
				       	                      suspend_type = NULL,
				       	                      suspend_start_date = NULL,
				       	                      suspend_end_date = NULL,
				       	                      timezone_offset_in_minutes = NULL,
				       	                      goto_florist_suspend_received = NULL,
				       	                      goto_florist_suspend_merc_id = NULL,
				       	                      goto_florist_resume_received = NULL,
				      	                      goto_florist_resume_merc_id = NULL,
				     	                      suspend_processed_flag = 'N'
				         WHERE   florist_id = v_florist_id;


		                       v_suspend_type := NULL;
			               v_comment_action := 'No Mercury Suspend Received within ' || v_await_merc_suspend  || ' minutes. Go To Suspend Cleared.';

                                    ELSE  /*  Do Nothing */
                                       v_suspend_type := NULL;
				       v_comment_action := NULL;

                                    END IF;

		            ELSE
		               UPDATE  florist_suspends       /* that was received after it expired */
		                    SET                     /*  we can simply clear it at this point */
		                       suspend_type = NULL,
		                       suspend_start_date = NULL,
		                       suspend_end_date = NULL,
		                       timezone_offset_in_minutes = NULL,
		                       goto_florist_suspend_received = NULL,
		                       goto_florist_suspend_merc_id = NULL,
		                       goto_florist_resume_received = NULL,
		                       goto_florist_resume_merc_id = NULL,
		                       suspend_processed_flag = 'N'
		                   WHERE   florist_id = v_florist_id;


		                   v_suspend_type := NULL;
				   v_comment_action := 'Suspend/ Resume cleared.  It was never active.  No change to florist status. ';
                            END IF;

        ELSE
                -- do nothing
	        v_suspend_type := NULL;
        END IF;

        IF v_suspend_type is not NULL THEN
                UPDATE  florist_master
                SET     status = decode (v_suspend_type, 'M', decode (v_goto_florist, 'Y', 'Active', 'Mercury Suspend'), 'G', 'GoTo Suspend', 'B','Blocked','Active')
                WHERE   florist_id = v_florist_id
                AND     status <> 'Inactive';
        END IF;

        -- build and insert the comments for the florist blocks
        IF v_comment_action is not null THEN
                IF v_florist_id = in_florist_id THEN
                        -- create the comment for the updated florist
                        v_comment := 'Florist update due to ';
                ELSE
                        -- create the comment for the associated florist
                        v_comment := 'Florist ' || in_florist_id || ': caused an update for this florist due to  ';
                END IF;

                v_comment := v_comment || v_comment_action;

                -- insert the comment
                INSERT_FLORIST_COMMENTS
                (
                        IN_FLORIST_ID => v_florist_id,
                        IN_COMMENT_TYPE => v_comment_disposition,
                        IN_FLORIST_COMMENT => v_comment,
                        IN_MANAGER_ONLY_FLAG => 'N',
                        IN_CREATED_BY => user,
                        OUT_STATUS => out_status,
                        OUT_MESSAGE => out_message
                );

        END IF;

        IF out_status = 'N' THEN
                exit;
        END IF;

        FETCH branch_cur INTO v_florist_id, v_suspend_type, v_suspend_start_date, v_suspend_end_date,
	                                     v_currently_suspended, v_goto_florist, v_goto_florist_suspend_recvd;

END LOOP;

CLOSE branch_cur;

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF branch_cur%isopen THEN
                CLOSE branch_cur;
        END IF;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FLORIST_SUSPEND_RESUME;




PROCEDURE UPDATE_FLORIST_PRODUCTS
(
IN_FLORIST_ID                   IN FLORIST_PRODUCTS.FLORIST_ID%TYPE,
IN_CODIFICATION_ID              IN FLORIST_CODIFICATIONS.CODIFICATION_ID%TYPE,
IN_BLOCK_INDICATOR              IN FLORIST_PRODUCTS.BLOCK_FLAG%TYPE,
IN_ACTION                       IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or deleting the
        florist product information.  Keep florist_codifications
        and florist_products in sync.

Input:
        florist_id                      varchar2
        codification_id                 varchar2
        block_indicator                 varchar2 (Y or N)
        action                          varchar2 (Update, Insert or Delete)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_product_id_tab        varchar_tab_typ;

CURSOR product_cur IS
        SELECT  c.product_id
        FROM    codified_products c
        WHERE   c.codification_id = in_codification_id
        AND     NOT EXISTS
        (
                SELECT  1
                FROM    florist_products p
                WHERE   p.florist_id = in_florist_id
                AND     p.product_id = c.product_id
        );

BEGIN

CASE in_action
WHEN 'Update' THEN
        UPDATE  florist_products
        SET     block_flag = in_block_indicator
        WHERE   florist_id = in_florist_id
        AND     product_id IN
        (
                SELECT  product_id
                FROM    codified_products
                WHERE   codification_id = in_codification_id
        ) RETURNING product_id BULK COLLECT INTO v_product_id_tab;

WHEN 'Insert' THEN
        OPEN product_cur;
        FETCH product_cur BULK COLLECT INTO v_product_id_tab;
        CLOSE product_cur;

        FORALL x IN 1..v_product_id_tab.count
                INSERT INTO florist_products
                (
                        florist_id,
                        product_id,
                        block_flag
                )
                VALUES
                (
                        UPPER(in_florist_id),
                        v_product_id_tab (x),
                        in_block_indicator
                );

ELSE
        DELETE FROM florist_products
        WHERE   florist_id = in_florist_id
        AND     product_id IN
        (
                SELECT  product_id
                FROM    codified_products
                WHERE   codification_id = in_codification_id
        ) RETURNING product_id BULK COLLECT INTO v_product_id_tab;
END CASE;

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_PRODUCTS;


PROCEDURE INSERT_NEW_FLORIST_CODIFIED
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting codify all products
        for a new florist

Input:
        florist_id                      varchar2
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- Defect #5542 - Revise the current codification process so that the codifications 
-- VPR, VPF and .VPW (MIN_AMT_CODIFY_ALL_CODIFICATIONS) are only added to new members 
-- with a Minimum Order value of $30 (MIN_AMT_FOR_CODIFY_ALL) or less.
--
CURSOR check_minimum_order_amount(p_florist_id varchar2) IS
        SELECT  count (1)
        FROM    florist_master
        WHERE   florist_id = p_florist_id
        AND     NVL(minimum_order_amount,0) <= frp.misc_pkg.get_global_parm_value('LOAD_MEMBER_DATA_CONFIG','MIN_AMT_FOR_CODIFY_ALL');

-- All codify-all codifications
CURSOR all_codified_cur IS
        SELECT  codification_id
        FROM    codification_master
        WHERE   codify_all_florist_flag = 'Y';

-- Codify-all codifications that do not have minimum order amount requirement
CURSOR non_min_codified_cur IS
        SELECT  codification_id
        FROM    codification_master
        WHERE   codify_all_florist_flag = 'Y'
        AND     INSTR(frp.misc_pkg.get_global_parm_value('LOAD_MEMBER_DATA_CONFIG','MIN_AMT_CODIFY_ALL_CODIFICATIONS'), '''' || codification_id || '''') = 0;

v_action                varchar2(10) := 'Insert';
v_comment_disposition   varchar2(20) := 'Codification';
v_codification_id_tab   varchar_tab_typ;
v_less_than_min_amt     number := 0;


BEGIN

OPEN check_minimum_order_amount(in_florist_id);
FETCH check_minimum_order_amount INTO v_less_than_min_amt;
CLOSE check_minimum_order_amount;

-- insert the florist codification records for codify all codifications
--
IF (v_less_than_min_amt = 1) THEN
        -- Add all codifications
        OPEN  all_codified_cur;
        FETCH all_codified_cur BULK COLLECT INTO v_codification_id_tab;
        CLOSE all_codified_cur;
ELSE
        -- Only add codifications that do not have minimum order amount requirement (since this florist exceeds it)
        OPEN  non_min_codified_cur;
        FETCH non_min_codified_cur BULK COLLECT INTO v_codification_id_tab;
        CLOSE non_min_codified_cur;
END IF;
FORALL x IN 1..v_codification_id_tab.count
        INSERT INTO florist_codifications
        (
                florist_id,
                codification_id
        )
        VALUES
        (
                in_florist_id,
                v_codification_id_tab(x)
        );

out_status := 'Y';

-- insert the comments for the florist codifications
FOR x IN 1..v_codification_id_tab.count LOOP
        insert_florist_comments
        (
                in_florist_id,
                v_comment_disposition,
                v_codification_id_tab (x) || ' was added',
                'N',
                in_csr_id,
                out_status,
                out_message
        );

        IF out_status = 'N' THEN
                exit;
        END IF;

        -- update florist_products with the changes
        update_florist_products
        (       in_florist_id,
                v_codification_id_tab (x),
                'N',
                v_action,
                out_status,
                out_message
        );

        IF out_status = 'N' THEN
                exit;
        END IF;

END LOOP;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_NEW_FLORIST_CODIFIED;


PROCEDURE UPDATE_FLORIST_CODIFICATIONS
(
IN_FLORIST_ID                   IN FLORIST_CODIFICATIONS.FLORIST_ID%TYPE,
IN_CODIFICATION_ID              IN FLORIST_CODIFICATIONS.CODIFICATION_ID%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_END_DATE               IN FLORIST_CODIFICATIONS.BLOCK_END_DATE%TYPE,
IN_CSR_ID                       IN VARCHAR2,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting the
        florist codification information

Input:
        florist_id                      varchar2
        codification_id                 varchar2
        block_indicator                 varchar2 (Y or N)
        block_end_date                  date
        csr_id                          varchar2
        additional comment              varchar2 (addition to the comment for a particular app)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR blocked_cur IS
        SELECT  block_start_date
        FROM    florist_codifications
        WHERE   florist_id = in_florist_id
        AND     codification_id = in_codification_id;

CURSOR branch_cur IS
        SELECT  f.florist_id,
                (select 'Y' from florist_codifications b where b.florist_id = f.florist_id and b.codification_id = in_codification_id)
        FROM    florist_master f
        WHERE   f.top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        --AND     f.record_type = 'R'
        AND     f.status <> 'Inactive';

v_florist_id            FLORIST_CODIFICATIONS.FLORIST_ID%TYPE;
v_exists                varchar2(1);
v_block_start_date      FLORIST_CODIFICATIONS.BLOCK_START_DATE%TYPE;
v_action                varchar2(10);
v_comment_disposition   varchar2(20) := 'Codification';
v_comment               varchar2(4000);
v_comment_block         varchar2(100);
v_order_detail_id		varchar2(100);
v_start_index           number;
v_end_index             number;

BEGIN

out_status := 'Y';
out_message := 'Florist record not found or is inactive';

IF in_block_indicator = 'N' THEN
        v_block_start_date := null;
        v_comment_block := 'unblocked';
ELSE
        OPEN blocked_cur;
        FETCH blocked_cur INTO v_block_start_date;
        CLOSE blocked_cur;

        IF v_block_start_date is null THEN
                v_block_start_date := trunc(sysdate);
        END IF;
        v_comment_block := 'blocked';
END IF;

OPEN branch_cur;
FETCH branch_cur INTO v_florist_id, v_exists;

WHILE branch_cur%found LOOP

        IF v_exists = 'Y' THEN

                UPDATE  florist_codifications
                SET
                        block_start_date = v_block_start_date,
                        block_end_date = in_block_end_date
                WHERE   florist_id = v_florist_id
                AND     codification_id = in_codification_id;

                v_action := 'Update';
        ELSE

                INSERT INTO florist_codifications
                (
                        florist_id,
                        codification_id,
                        block_start_date,
                        block_end_date,
                        codified_date
                )
                VALUES
                (
                        UPPER(v_florist_id),
                        in_codification_id,
                        v_block_start_date,
                        in_block_end_date,
                        sysdate
                );

                v_action := 'Insert';

        END IF;

        -- build and insert the comments for the florist codifications
        IF v_action = 'Insert' THEN
                IF v_florist_id = in_florist_id THEN
                        -- create the comment for the updated florist
                        v_comment := in_codification_id || ' was added';
                ELSE
                        -- create the comment for the associated florist
                        v_comment := in_codification_id || ' was added to ' || in_florist_id;
                END IF;
        ELSE
                IF v_florist_id = in_florist_id THEN
                        -- create the comment for the updated florist
                        v_comment := in_codification_id || ' was ' || v_comment_block || ' from the florist';
                ELSE
                        -- create the comment for the associated florist
                        v_comment := in_codification_id || ' was ' || v_comment_block || ' for florist ' || in_florist_id;
                END IF;
        END IF;
		
		IF IN_CSR_ID = 'MARS' THEN
          v_order_detail_id := '';
          IF in_additional_comment IS NOT NULL THEN
            v_start_index := INSTR(in_additional_comment,'(');
            v_end_index := INSTR(in_additional_comment,')');
            v_order_detail_id := substr(in_additional_comment, v_start_index,(v_end_index - v_start_index + 1));
          END IF;
          
          -- insert this comment only for the messages handled by MARS
          insert_florist_comments
          (
              in_florist_id => v_florist_id,
              in_comment_type => v_comment_disposition,
              in_florist_comment => 'Block End Date ' || to_char(in_block_end_date, 'MM/dd/yyyy') || ' was added by MARS auto View Queue handling '  || v_order_detail_id,
              in_manager_only_flag => 'N',
              in_created_by => in_csr_id,
              out_status => out_status,
              out_message => out_message
          );
        END IF;
	
        -- insert the comment
        insert_florist_comments
        (
                in_florist_id => v_florist_id,
                in_comment_type => v_comment_disposition,
                in_florist_comment => v_comment || ' ' || in_additional_comment,
                in_manager_only_flag => 'N',
                in_created_by => in_csr_id,
                out_status => out_status,
                out_message => out_message
        );

        IF out_status = 'Y' THEN
                -- update florist_products with the changes
                update_florist_products (v_florist_id, in_codification_id, in_block_indicator, v_action, out_status, out_message);
        END IF;

        FETCH branch_cur INTO v_florist_id, v_exists;
END LOOP;

CLOSE branch_cur;

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF branch_cur%isopen THEN
                CLOSE branch_cur;
        END IF;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_CODIFICATIONS;


PROCEDURE INITIATE_FTDM_SHUTDOWN
(
IN_SHUTDOWN_BY                  IN FTDM_SHUTDOWN.SHUTDOWN_BY%TYPE,
IN_RESTART_DATE                 IN FTDM_SHUTDOWN.RESTART_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for initating the FTDM shutdown by
        inserting a record in FTDM_SHUTDOWN and calling the procedure
        to rebuild the available zips and products. It also calls a proc
        to put soft blocks on all FTDM florists

Input:
        shutdown_by                     varchar2
        restart_date							 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO ftdm_shutdown
(
        shutdown_flag,
        shutdown_by,
        shutdown_date,
        restart_date,
        restarted_by,
        last_updated_date,
        last_updated_by
)
VALUES
(
        'Y',
        UPPER(in_shutdown_by),
        sysdate,
        in_restart_date,
        UPPER(in_shutdown_by),
        sysdate,
        UPPER(in_shutdown_by)
);

-- call FTDM_SHUTDOWN_FLORIST_BLOCK. Put soft block on all FTDM florists
FTDM_SHUTDOWN_FLORIST_BLOCK(in_restart_date, out_status, out_message);

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INITIATE_FTDM_SHUTDOWN;

PROCEDURE UPDATE_FTDM_SHUTDOWN_RESTART
(
IN_RESTART_DATE                 IN FTDM_SHUTDOWN.RESTART_DATE%TYPE,
IN_RESTARTED_BY                 IN FTDM_SHUTDOWN.RESTARTED_BY%TYPE,
IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the FTDM shutdown record
        with the schedule restart date. Call FTDM_SHUTDOWN_UPDATE_BLOCKS proc
        to update the end date on the soft blocks

Input:
        restart_date                    date
        restarted_by                    varchar2
        last_updated_by						 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN
             OUT_STATUS := 'Y';

             UPDATE_FTDM_SHUTDOWN_RESTART(IN_RESTART_DATE, IN_RESTARTED_BY, IN_LAST_UPDATED_BY, 'N', OUT_STATUS, OUT_MESSAGE);


EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FTDM_SHUTDOWN_RESTART;




PROCEDURE UPDATE_FTDM_SHUTDOWN_RESTART
(
IN_RESTART_DATE                 IN FTDM_SHUTDOWN.RESTART_DATE%TYPE,
IN_RESTARTED_BY                 IN FTDM_SHUTDOWN.RESTARTED_BY%TYPE,
IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE,
IN_ENQUEUE                      IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the FTDM shutdown record
        with the schedule restart date. Call FTDM_SHUTDOWN_UPDATE_BLOCKS proc
        to update the end date on the soft blocks

Input:
        restart_date                    date
        restarted_by                    varchar2
        last_updated_by						 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := 'Y';

UPDATE  ftdm_shutdown
SET
        restart_date = in_restart_date,
        restarted_by = UPPER(in_restarted_by),
        last_updated_date = sysdate,
        last_updated_by = UPPER(in_last_updated_by);
        
-- call FTDM_SHUTDOWN_UPDATE_BLOCKS to update the end date on the soft blocks
FTDM_SHUTDOWN_UPDATE_BLOCKS(in_restart_date, out_status, out_message);        

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FTDM_SHUTDOWN_RESTART;


PROCEDURE RESTART_FTDM
(
IN_LAST_UPDATED_BY				  IN FTDM_SHUTDOWN.LAST_UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for restarting manual florists by
        deleting the record in FTDM_SHUTDOWN and calling the procedure to
        rebuild the available zips and products. It also calls the proc 
        FTDM_SHUTDOWN_FLORIST_UNBLOCK to unblock the soft blocks

        This procedure will first check that the restart date on the shutdown
        record matches the date the procedure is executed in the case the
        restart date was updated and the original restart message remained in
        the queue.  If the dates do not match, the restart message is ignored.

Input:
        last_updated_by						 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_restart_date          FTDM_SHUTDOWN.RESTART_DATE%TYPE;

CURSOR restart_cur IS
        SELECT  restart_date
        FROM    ftdm_shutdown;

BEGIN

open restart_cur;
fetch restart_cur into v_restart_date;

if restart_cur%found and trunc (v_restart_date) = trunc (sysdate) then

        UPDATE  ftdm_shutdown
		  SET		 last_updated_by = UPPER(in_last_updated_by) || ' - DELETE',
		  			 last_updated_date = sysdate;
        
        delete from ftdm_shutdown;

	-- call FTDM_SHUTDOWN_FLORIST_UNBLOCK to unblock the soft blocks
	FTDM_SHUTDOWN_FLORIST_UNBLOCK(out_status, out_message);
		  
end if;

CLOSE restart_cur;

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF restart_cur%isopen THEN
                CLOSE restart_cur;
        END IF;

        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END RESTART_FTDM;


PROCEDURE MOVE_FLORIST_ZIPS_TO_PARENT
AS

/* Select all Florist_zips for florsits that are not an 'R' record type and have a parent florist */
CURSOR zip_cur IS
        SELECT  coalesce (p.florist_id, t.florist_id, f.florist_id),
                f.florist_id,
                z.zip_code
        FROM    florist_master f
        JOIN    florist_zips z
        ON      f.florist_id = z.florist_id
        LEFT OUTER JOIN florist_master p
        ON      f.parent_florist_id = p.florist_id
        AND     p.record_type = 'R'
        LEFT OUTER JOIN florist_master t
        ON      f.top_level_florist_id = t.florist_id
        AND     t.record_type = 'R'
        WHERE   f.florist_id = z.florist_id
        AND     f.record_type <> 'R'
        AND     f.parent_florist_id is not null;

CURSOR exists_cur (p_florist_id varchar2, p_zip_code varchar2) IS
        SELECT  count (1)
        FROM    florist_zips
        WHERE   florist_id = p_florist_id
        AND     zip_code = p_zip_code;

type varchar_tab_type is table of varchar2(100) index by pls_integer;
v_new_florist_id_tab    varchar_tab_type;
v_florist_id_tab        varchar_tab_type;
v_zip_code_tab          varchar_tab_type;

v_exists                number;
v_rowcount              number;

BEGIN

v_rowcount := 0;

OPEN zip_cur;
FETCH zip_cur BULK COLLECT INTO v_new_florist_id_tab, v_florist_id_tab, v_zip_code_tab;
CLOSE zip_cur;

FOR x in 1..v_florist_id_tab.count LOOP
        v_exists := -1;
        OPEN exists_cur (v_new_florist_id_tab(x), v_zip_code_tab(x));
        FETCH exists_cur INTO v_exists;
        CLOSE exists_cur;

        IF v_exists = 0 THEN
                INSERT INTO florist_zips
                (
                        florist_id,
                        zip_code,
                        last_update_date,
                        cutoff_time,
                        block_start_date,
                        block_end_date
                )
                SELECT  v_new_florist_id_tab (x),
                        UPPER(zip_code),
                        last_update_date,
                        cutoff_time,
                        block_start_date,
                        block_end_date
                FROM    florist_zips
                WHERE   florist_id = v_florist_id_tab(x)
                AND     zip_code = v_zip_code_tab(x);

                v_rowcount := v_rowcount + 1;
        END IF;
END LOOP;

dbms_output.put_line ('MOVE_FLORIST_ZIPS_TO_PARENT (MOVE ZIPS)::' || to_char (v_rowcount));

FORALL x in 1..v_florist_id_tab.count
        DELETE FROM florist_zips
        WHERE   florist_id = v_florist_id_tab(x)
        AND     zip_code = v_zip_code_tab(x);

v_rowcount := sql%rowcount;
dbms_output.put_line ('MOVE_FLORIST_ZIPS_TO_PARENT (DELETE NON R ZIPS)::' || to_char (v_rowcount));
END MOVE_FLORIST_ZIPS_TO_PARENT;


PROCEDURE UPDATE_FLORIST_N_INSERT_CMMNTS
(
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates every florist_master found
        allow_message_forwarding_flag and inserts a record for it into the
        florist_comments table.

Input:
        N/A

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR florist_cur IS
  SELECT florist_id
    FROM florist_master
   WHERE (
          SUBSTR(florist_id, 1, 2) = '90'
          OR
          SUBSTR(florist_id, 1,2) = '91'
         )
     AND allow_message_forwarding_flag = 'N'
 UNION
  SELECT florist_id
    FROM florist_master fm1
   WHERE fm1.allow_message_forwarding_flag = 'N'
     AND fm1.record_type = 'R'
     AND fm1.internal_link_number IN (SELECT fm2.internal_link_number
                                        FROM florist_master fm2
                                       WHERE fm2.record_type = 'R'
                                         AND fm2.status <> 'Inactive'
                                       GROUP BY fm2.internal_link_number
                                       HAVING COUNT(fm2.internal_link_number) > 1)
 UNION
  SELECT florist_id
    FROM florist_master fm1
   WHERE fm1.allow_message_forwarding_flag = 'N'
     AND fm1.record_type = 'R'
     AND fm1.top_level_florist_id IN (SELECT fm2.top_level_florist_id
                                        FROM florist_master fm2
                                       WHERE fm2.record_type = 'R'
                                         AND fm2.status <> 'Inactive'
                                       GROUP BY fm2.top_level_florist_id
                                       HAVING COUNT(fm2.top_level_florist_id) > 1);


CURSOR florist_cur2 IS
  SELECT florist_id
    FROM florist_master
   WHERE allow_message_forwarding_flag = 'Y'
 MINUS
  (
    SELECT florist_id
      FROM florist_master
     WHERE SUBSTR(florist_id, 1, 2) = '90'
        OR SUBSTR(florist_id, 1,2) = '91'
   UNION
    SELECT florist_id
      FROM florist_master fm1
     WHERE fm1.record_type = 'R'
       AND fm1.internal_link_number IN (SELECT fm2.internal_link_number
                                          FROM florist_master fm2
                                         WHERE fm2.record_type = 'R'
                                           AND fm2.status <> 'Inactive'
                                         GROUP BY fm2.internal_link_number
                                         HAVING COUNT(fm2.internal_link_number) > 1)
   UNION
    SELECT florist_id
      FROM florist_master fm1
     WHERE fm1.record_type = 'R'
       AND fm1.top_level_florist_id IN (SELECT fm2.top_level_florist_id
                                          FROM florist_master fm2
                                         WHERE fm2.record_type = 'R'
                                           AND fm2.status <> 'Inactive'
                                         GROUP BY fm2.top_level_florist_id
                                        HAVING COUNT(fm2.top_level_florist_id) > 1)
  );


BEGIN

  FOR f_rec IN florist_cur LOOP
    UPDATE florist_master
       SET allow_message_forwarding_flag = 'Y'
     WHERE florist_id = f_rec.florist_id;

    INSERT INTO florist_comments
         (florist_id,
          comment_date,
          comment_type,
          florist_comment,
          manager_only_flag,
          created_date,
          created_by)
      VALUES
         (f_rec.florist_id,
          SYSDATE,
          'Forward Flag',
          'Message forwarding flag set to ALLOWED',
          'N',
          SYSDATE,
          'FLORIST_FORWARD_UPDATE');
  END LOOP;

  FOR f_rec2 IN florist_cur2 LOOP
    UPDATE florist_master
       SET allow_message_forwarding_flag = 'N'
     WHERE florist_id = f_rec2.florist_id;

    INSERT INTO florist_comments
        (florist_id,
         comment_date,
         comment_type,
         florist_comment,
         manager_only_flag,
         created_date,
         created_by)
      VALUES
        (f_rec2.florist_id,
         SYSDATE,
         'Forward Flag',
         'Message forwarding flag set to NOT ALLOWED',
         'N',
         SYSDATE,
         'FLORIST_FORWARD_UPDATE');
  END LOOP;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;


END UPDATE_FLORIST_N_INSERT_CMMNTS;



PROCEDURE UPDATE_ZIP_MASTER_FLORIST_ID
(
   OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
  UPdates column zip_,master_florist_id using the following
  logic:
      If the first seven characters are different than the top_level_florist_id
         then use the top_level_florist_id
         else
            Usint the minimum florist_id where matching the first 7 charcters.
  
Input:

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR master_cur IS
		select 
		   florist_id, 
		   case when substr(top_level_florist_id,1,7) <> substr(florist_id,1,7)
		   then top_level_florist_id
		  else
			  (select  min(florist_id)  from ftd_apps.florist_master fm2
				  where  fm2.florist_id like substr(fm1.florist_id,1,7) || '%'
				  and fm2.status <> 'Inactive'
				   group by substr(fm2.florist_id,1,7)
			 ) 
	 end new_zip_master_florist_id, zip_master_florist_id
	 from ftd_apps.florist_master fm1
	 where fm1.status <> 'Inactive';

BEGIN

FOR m_rec IN master_cur LOOP

  -- only perform the update if the current value is null or different from the new value
  IF m_rec.zip_master_florist_id is null or m_rec.new_zip_master_florist_id <> m_rec.zip_master_florist_id THEN
  
    dbms_application_info.set_client_info('Update Zip Master Florist ID: ' || m_rec.florist_id);
    UPDATE ftd_apps.florist_master
       SET florist_master.zip_master_florist_id = m_rec.new_zip_master_florist_id
     WHERE florist_id = m_rec.florist_id;

    INSERT INTO ftd_apps.florist_comments
         (florist_id,
          comment_date,
          comment_type,
          florist_comment,
          manager_only_flag,
          created_date,
          created_by)
      VALUES
         (m_rec.florist_id,
          SYSDATE,
          'Master Florist',
          'Master florist set to ' || m_rec.new_zip_master_florist_id,
          'N',
          SYSDATE,
          'SYS');
  END IF;
	
END LOOP;
  
  
  out_status := 'Y';
  
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_ZIP_MASTER_FLORIST_ID;

PROCEDURE FTDM_SHUTDOWN_FLORIST_BLOCK
(
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is called by INITIATE_FTDM_SHUTDOWN and 
        puts a soft block on all FTDM florists until the given date.

Input:
        block_end_date                  date
        
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- get a cursor for all active FTDM florists who don't have blocks or have a block end date less that the in_block_end_date
CURSOR ftdm_florists_cur IS
select distinct(f.florist_id)  
  from ftd_apps.florist_master f
  where f.status <> 'Inactive' 
  and f.vendor_flag = 'N'
  and  (f.mercury_flag <> 'M' or f.mercury_flag is null) 
  and not exists (select 'Y'
    from ftd_apps.florist_blocks fb
    where fb.florist_id = f.florist_id
    and fb.block_type = 'O');  -- Dont want OPTED out florists either

	v_florist_id             FLORIST_BLOCKS.FLORIST_ID%TYPE;
	v_message_id            number;

BEGIN

OPEN ftdm_florists_cur;
FETCH ftdm_florists_cur INTO v_florist_id;

-- put a hard block on each date in the ftdm shutdown range where a block doesn't already exist for all FTDM florists
WHILE ftdm_florists_cur%found LOOP
  UPDATE_FLORIST_BLOCKS(v_florist_id, 'H', 'Y', IN_BLOCK_END_DATE, 'FTDMSHUTDOWN-ON', 'BLOCKED BY FTDM SHUTDOWN', 'N', OUT_STATUS, OUT_MESSAGE);
  IF out_status = 'N' THEN
                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_MAINT_PKG.FTDM_SHUTDOWN_FLORIST_BLOCK',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'ERROR UPDATING BLOCK FOR FLORIST ' || v_florist_id || ' : ' || OUT_MESSAGE,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => OUT_STATUS,
                        OUT_MESSAGE => OUT_MESSAGE
                );
  END IF;
  out_status := 'Y';
  out_message := ' ';
	FETCH ftdm_florists_cur INTO v_florist_id;
END LOOP;

out_status := 'Y';
out_message := ' ';

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF ftdm_florists_cur%isopen THEN
                CLOSE ftdm_florists_cur;
        END IF;

         OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN FTD_APPS.FTDM_SHUTDOWN_FLORIST_BLOCK - [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FTDM_SHUTDOWN_FLORIST_BLOCK;

PROCEDURE FTDM_SHUTDOWN_FLORIST_UNBLOCK
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is called by RESTART_FTDM 
        and unblocks all FTDM florists that have been  
        blocked by the FTDM SHUTDOWN process.

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
	-- get a cursor for all FTDM florists that have blocks caused by FTDM
	CURSOR ftdm_florists_cur IS
	select f.florist_id, block_start_date  from ftd_apps.florist_master f
	LEFT OUTER JOIN ftd_apps.florist_blocks b
	ON      f.florist_id = b.florist_id
	where (b.BLOCKED_BY_USER_ID = 'FTDMSHUTDOWN-ON')
	and f.florist_id != (select gp.value from frp.global_parms gp where gp.context = 'SERVICE' and gp.name = 'EFA_FTDM_FLORIST');

	v_florist_id            FLORIST_BLOCKS.FLORIST_ID%TYPE;
	v_block_start_date      FLORIST_BLOCKS.BLOCK_START_DATE%TYPE;
	v_message_id            number;
	

BEGIN

	OPEN ftdm_florists_cur;
	FETCH ftdm_florists_cur INTO v_florist_id, v_block_start_date;

	WHILE ftdm_florists_cur%found LOOP
        REMOVE_FLORIST_BLOCKS(v_florist_id, null, 'N', v_block_start_date, null, 'FTDMSHUTDOWN-ON', 'BLOCKED BY FTDM SHUTDOWN', 'N', OUT_STATUS, OUT_MESSAGE);
        IF out_status = 'N' THEN
                -- insert system message error
                FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
                (
                        IN_SOURCE => 'FLORIST_MAINT_PKG.FTDM_SHUTDOWN_FLORIST_UNBLOCK',
                        IN_TYPE => 'EXCEPTION',
                        IN_MESSAGE => 'ERROR UPDATING BLOCK FOR FLORIST ' || v_florist_id || ' : ' || OUT_MESSAGE,
                        IN_COMPUTER => null,
                        OUT_SYSTEM_MESSAGE_ID => v_message_id,
                        OUT_STATUS => OUT_STATUS,
                        OUT_MESSAGE => OUT_MESSAGE
                );
        END IF;
        out_status := 'Y';
        out_message := ' ';
         FETCH ftdm_florists_cur INTO v_florist_id, v_block_start_date;
END LOOP;

	out_status := 'Y';
	out_message := ' ';

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF ftdm_florists_cur%isopen THEN
                CLOSE ftdm_florists_cur;
        END IF;

         OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FTDM_SHUTDOWN_FLORIST_UNBLOCK;




PROCEDURE FTDM_SHUTDOWN_UPDATE_BLOCKS
(
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
)
/*-----------------------------------------------------------------------------
Description:
        This procedure is called by UPDATE_FTDM_SHUTDOWN_RESTART 
        and updates the restart date on the soft block 
        on all FTDM florists to the given date.

Input:
        block_end_date                  date
        
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
AS
	-- get a cursor for all active FTDM florists who don't have blocks or have a block end date less that the in_block_end_date
	CURSOR ftdm_florists_cur IS
	select f.florist_id  from ftd_apps.florist_master f
	LEFT OUTER JOIN ftd_apps.florist_blocks b
	ON      f.florist_id = b.florist_id
	where ((f.status <> 'Inactive' 
		and f.vendor_flag = 'N'
		and  (f.mercury_flag <> 'M' or f.mercury_flag is null) 
		and (b.block_end_date is null or in_block_end_date > b.block_end_date)
		and (b.block_type is null or b.block_type <> 'O'))  -- Dont want OPTED out florists either
		or  
		(b.BLOCKED_BY_USER_ID = 'FTDMSHUTDOWN-ON'));

	v_florist_id             FLORIST_BLOCKS.FLORIST_ID%TYPE;

BEGIN

	OPEN ftdm_florists_cur;
	FETCH ftdm_florists_cur INTO v_florist_id;

	-- put a soft block on all FTDM florists
	WHILE ftdm_florists_cur%found LOOP
			 UPDATE_FLORIST_BLOCKS(v_florist_id, 'S', 'Y', IN_BLOCK_END_DATE, 'FTDMSHUTDOWN-ON', 'BLOCKED BY FTDM SHUTDOWN', 'N', OUT_STATUS, OUT_MESSAGE);
			  IF out_status = 'N' THEN
						 exit;
			  END IF;

				FETCH ftdm_florists_cur INTO v_florist_id;
	END LOOP;

	out_status := 'Y';
	out_message := ' ';

EXCEPTION WHEN OTHERS THEN
BEGIN
        IF ftdm_florists_cur%isopen THEN
                CLOSE ftdm_florists_cur;
        END IF;

         OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FTDM_SHUTDOWN_UPDATE_BLOCKS;

PROCEDURE INSERT_EROS_AVAIL_HISTORY
(
IN_FLORIST_ID                   IN EROS_AVAILABILITY_HISTORY.FLORIST_ID%TYPE,
IN_GENERATION_DATE              IN EROS_AVAILABILITY_HISTORY.GENERATION_DATE%TYPE,
IN_EXPIRATION_DATE              IN EROS_AVAILABILITY_HISTORY.EXPIRATION_DATE%TYPE,
IN_ONLINE_FLAG                  IN EROS_AVAILABILITY_HISTORY.ONLINE_FLAG%TYPE,
IN_CALL_FORWARDING_FLAG         IN EROS_AVAILABILITY_HISTORY.CALL_FORWARDING_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO EROS_AVAILABILITY_HISTORY (
        EROS_AVAIL_HISTORY_ID,
        FLORIST_ID,
        GENERATION_DATE,
        EXPIRATION_DATE,
        ONLINE_FLAG,
        CALL_FORWARDING_FLAG,
        CREATED_ON
    ) VALUES (
        EROS_AVAIL_HISTORY_ID_SQ.NEXTVAL,
        IN_FLORIST_ID,
        IN_GENERATION_DATE,
        IN_EXPIRATION_DATE,
        IN_ONLINE_FLAG,
        IN_CALL_FORWARDING_FLAG,
        SYSDATE
    );

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_EROS_AVAIL_HISTORY;

PROCEDURE UPDATE_FLORIST_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_TYPE                   IN FLORIST_BLOCKS.BLOCK_TYPE%TYPE,
IN_BLOCK_INDICATOR              IN VARCHAR2,
IN_BLOCK_END_DATE               IN FLORIST_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCKED_BY_USER_ID           IN FLORIST_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
IN_ADDITIONAL_COMMENT           IN VARCHAR2,
IN_ENQUEUE_FLAG		        IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting florist
        blocks.

        The available zips process for the given zip code is started if the enqueue flag is 'Y'.

Input:
        florist_id                      varchar2
        block_type                      varchar2 (H, S, O, null - if no block)
        block_indicator                 varchar2 (Y or N)
        block_end_date                  date
        blocked_by_user_id              varchar2
        additional comment              varchar2 (addition to the comment for a particular app)
        enqueue_flag 			varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR blocked_cur(v_florist_id varchar2, v_start_date date, v_end_date date) is
        SELECT block_start_date
        FROM florist_blocks
        WHERE florist_id = v_florist_id
        AND block_start_date <= trunc(v_end_date)
        AND (block_end_date is null or block_end_date >= trunc(v_start_date));

CURSOR branch_cur IS
        SELECT  f.florist_id,
                f.super_florist_flag,
                s.suspend_type,
                decode (s.florist_id, null, 'N', 'Y') exists_ind,
                decode (s.suspend_type, 'M', 'Mercury Suspend', 'G', 'GoTo Suspend') suspend_status,               
                b.block_end_date,
                nvl(suspend_start_date-(timezone_offset_in_minutes/1440),to_date('01/01/0001','MM/DD/YYYY')) suspend_start,
                nvl(suspend_end_date-(timezone_offset_in_minutes/1440),to_date('01/01/0001','MM/DD/YYYY')) suspend_end,
                record_type
                FROM    florist_master f
        LEFT OUTER JOIN florist_suspends s
        ON      f.florist_id = s.florist_id
        LEFT OUTER JOIN ftd_apps.florist_blocks b
        ON      f.florist_id = b.florist_id
        WHERE   f.top_level_florist_id = (select top_level_florist_id from florist_master where florist_id = in_florist_id)
        AND     f.status <> 'Inactive';

v_florist_id            FLORIST_BLOCKS.FLORIST_ID%TYPE;
v_goto_florist          FLORIST_MASTER.SUPER_FLORIST_FLAG%TYPE;
v_exists                varchar2(1);
v_status                varchar2(100);
v_block_start_date      FLORIST_BLOCKS.BLOCK_START_DATE%TYPE;
v_suspend_type          FLORIST_SUSPENDS.SUSPEND_TYPE%TYPE;
v_action                varchar2(10);
v_comment_disposition   varchar2(20) := 'Codification';
v_comment               varchar2(1000);
v_block_end_date      FLORIST_BLOCKS.BLOCk_END_DATE%TYPE;
v_suspend_start_date  FLORIST_SUSPENDS.SUSPEND_START_DATE%TYPE;
v_suspend_end_date      FLORIST_SUSPENDS.SUSPEND_END_DATE%TYPE;
v_remain_suspended      varchar2(10);
v_comment_action     varchar2(500);
v_record_type           varchar2(1);
v_days_to_add	int := 0;
v_counter	int := 0;
v_new_block_start_date      date;
v_new_block_end_date        date;

BEGIN

out_status := 'N';

out_message := 'Florist record not found or is inactive';

v_remain_suspended := 'N';

OPEN branch_cur;

FETCH branch_cur INTO v_florist_id, v_goto_florist,v_suspend_type, v_exists, v_status, v_block_end_date,v_suspend_start_date, v_suspend_end_date, v_record_type;
out_status := 'Y';

WHILE branch_cur%found LOOP
       
    IF in_block_indicator = 'N' THEN
        v_block_start_date := null;
    ELSE
        v_block_start_date := null;
        OPEN blocked_cur(v_florist_id, sysdate, in_block_end_date);
        FETCH blocked_cur INTO v_block_start_date;
        CLOSE blocked_cur;
    END IF;

    IF v_block_start_date is null THEN
        -- no block exists for the ftdm shutdown period, apply entire ftdm shutdown block
        --INSERT_FLORIST_BLOCKS(IN_FLORIST_ID, IN_BLOCK_TYPE, IN_BLOCK_INDICATOR, v_block_start_date, IN_BLOCK_END_DATE, IN_BLOCKED_BY_USER_ID, IN_ADDITIONAL_COMMENT, 'Y', null, OUT_STATUS, OUT_MESSAGE);
        INSERT INTO florist_blocks
                (
                        florist_id,
                        block_type,
                        block_start_date,
                        block_end_date,
                        blocked_by_user_id,
                        block_reason
                )
                VALUES
                (
                        UPPER(v_florist_id),
                        in_block_type,
                        trunc (sysdate),
                        in_block_end_date,
                        in_blocked_by_user_id,
                        null
                );

                v_action := 'Insert';

        IF in_block_indicator = 'Y' THEN
                -- set the status
                UPDATE  florist_master
                SET     status = 'Blocked'
                WHERE   florist_id = v_florist_id;
         END IF;


        -- build and insert the comments for the florist blocks
        IF v_florist_id = in_florist_id THEN
                -- create the comment for the updated florist
                v_comment := 'Florist was ';
        ELSE
                -- create the comment for the associated florist
                v_comment := 'Florist ' || in_florist_id || ': was ';
        END IF;

        CASE in_block_type
                WHEN 'S' THEN v_comment_action := 'Soft blocked for ' || to_char(v_block_start_date, 'mm/dd/yy') || '-' || to_char(in_block_end_date, 'mm/dd/yy');
                WHEN 'H' THEN v_comment_action :=  'Hard blocked for ' || to_char(v_block_start_date, 'mm/dd/yy') || '-' || to_char(in_block_end_date, 'mm/dd/yy');
                WHEN 'O' THEN v_comment_action := 'Opt Out blocked for ' || to_char(v_block_start_date, 'mm/dd/yy');
                ELSE  v_comment_action := v_comment_action || ' ';
         END CASE;

        v_comment := v_comment || v_comment_action || ' ' || in_additional_comment;

        -- insert the comment
        INSERT_FLORIST_COMMENTS
        (
                IN_FLORIST_ID => v_florist_id,
                IN_COMMENT_TYPE => v_comment_disposition,
                IN_FLORIST_COMMENT => v_comment,
                IN_MANAGER_ONLY_FLAG => 'N',
                IN_CREATED_BY => in_blocked_by_user_id,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );

        IF out_status = 'N' THEN
                exit;
        END IF;  
    ELSE
         -- block exists so need to add individual block dates if necessary
         SELECT (in_block_end_date - trunc(sysdate)) into v_days_to_add FROM dual; 
              
         /* Loop to determine how many days to check */
         while v_counter <= v_days_to_add
         loop
           /* Add a day to block start date*/
           v_new_block_start_date := trunc(sysdate) + v_counter;
           v_new_block_end_date := v_new_block_start_date;

           v_block_start_date := null;
           OPEN blocked_cur(v_florist_id, v_new_block_start_date, v_new_block_start_date);
           FETCH blocked_cur INTO v_block_start_date;
           CLOSE blocked_cur;

           IF v_block_start_date is null THEN
             --INSERT_FLORIST_BLOCKS(IN_FLORIST_ID, IN_BLOCK_TYPE, IN_BLOCK_INDICATOR, v_new_date, v_new_date, IN_BLOCKED_BY_USER_ID, IN_ADDITIONAL_COMMENT, 'Y', null, OUT_STATUS, OUT_MESSAGE);  
                      INSERT INTO florist_blocks
                (
                        florist_id,
                        block_type,
                        block_start_date,
                        block_end_date,
                        blocked_by_user_id,
                        block_reason
                )
                VALUES
                (
                        UPPER(v_florist_id),
                        in_block_type,
                        v_new_block_start_date,
                        v_new_block_end_date,
                        in_blocked_by_user_id,
                        null
                );

                v_action := 'Insert';


              IF in_block_indicator = 'Y' THEN
                     -- set the status
                     UPDATE  florist_master
                     SET     status = 'Blocked'
                     WHERE   florist_id = v_florist_id;
              END IF;


             -- build and insert the comments for the florist blocks
             IF v_florist_id = in_florist_id THEN
                     -- create the comment for the updated florist
                     v_comment := 'Florist was ';
             ELSE
                     -- create the comment for the associated florist
                     v_comment := 'Florist ' || in_florist_id || ': was ';
             END IF;

             CASE in_block_type
                     WHEN 'S' THEN v_comment_action := 'Soft blocked for ' || to_char(v_new_block_start_date, 'mm/dd/yy') || '-' || to_char(v_new_block_end_date, 'mm/dd/yy');
                     WHEN 'H' THEN v_comment_action :=  'Hard blocked for ' || to_char(v_new_block_start_date, 'mm/dd/yy') || '-' || to_char(v_new_block_end_date, 'mm/dd/yy');
                     WHEN 'O' THEN v_comment_action := 'Opt Out blocked for ' || to_char(v_new_block_start_date, 'mm/dd/yy');
                     ELSE  v_comment_action := v_comment_action || ' ';
              END CASE;

             v_comment := v_comment || v_comment_action || ' ' || in_additional_comment;

             -- insert the comment
             INSERT_FLORIST_COMMENTS
             (
                     IN_FLORIST_ID => v_florist_id,
                     IN_COMMENT_TYPE => v_comment_disposition,
                     IN_FLORIST_COMMENT => v_comment,
                     IN_MANAGER_ONLY_FLAG => 'N',
                     IN_CREATED_BY => in_blocked_by_user_id,
                     OUT_STATUS => out_status,
                     OUT_MESSAGE => out_message
             );

             IF out_status = 'N' THEN
                exit;
             END IF;    
           END IF;  

           /* Increment counter */
           v_counter := v_counter + 1;
         
         end loop;
       
         v_counter := 0;
       END IF;
 
       FETCH branch_cur INTO v_florist_id, v_goto_florist,v_suspend_type, v_exists, v_status, v_block_end_date,v_suspend_start_date, v_suspend_end_date, v_record_type;
END LOOP;

CLOSE branch_cur;


EXCEPTION WHEN OTHERS THEN
BEGIN
        IF branch_cur%isopen THEN
                CLOSE branch_cur;
        END IF;

         OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_BLOCKS;

PROCEDURE UPDATE_FLORIST_LATLONG
(
 IN_FLORIST_ID         IN FLORIST_MASTER.FLORIST_ID%TYPE,
 IN_LATITUDE		   IN FLORIST_MASTER.LATITUDE%TYPE,
 IN_LONGITUDE          IN FLORIST_MASTER.LONGITUDE%TYPE,
 IN_UPDATED_BY		   IN FLORIST_MASTER.LAST_UPDATED_BY%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the latitude and longitude information of a florist

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE FLORIST_MASTER SET 
	LATITUDE = IN_LATITUDE,
	LONGITUDE = IN_LONGITUDE,
	LAST_UPDATED_DATE = TO_CHAR(SYSDATE,'dd-MON-yy'),
	LAST_UPDATED_BY = IN_UPDATED_BY
  WHERE FLORIST_ID = IN_FLORIST_ID;

  out_status := 'Y';
  
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_FLORIST_LATLONG;

PROCEDURE INSERT_FLORIST_SUSPEND_HISTORY
(
 IN_FLORIST_ID                  IN FLORIST_MASTER.FLORIST_ID%TYPE,
 IN_SUSPEND_START_DATE          IN FLORIST_SUSPEND_HISTORY.SUSPEND_START_DATE%TYPE,
 IN_SUSPEND_END_DATE            IN FLORIST_SUSPEND_HISTORY.SUSPEND_END_DATE%TYPE,
 OUT_STATUS                     OUT VARCHAR2,
 OUT_MESSAGE                    OUT VARCHAR2
) AS

cursor top_level_cur is
  select top_level_florist_id
  from florist_master
  where florist_id = in_florist_id
  and status <> 'Inactive';

cursor exists_cur(p_top_level_florist_id varchar2) is
  select (sysdate - updated_on) * 1440 as time_difference
  from florist_suspend_history
  where top_level_florist_id = p_top_level_florist_id
  and suspend_start_date = in_suspend_start_date
  and suspend_end_date = in_suspend_end_date;

v_top_level_florist_id          varchar2(9);
v_time_difference               number;
v_time_threshold                number;

BEGIN

  OPEN top_level_cur;
  FETCH top_level_cur INTO v_top_level_florist_id;
  CLOSE top_level_cur;

  open exists_cur(v_top_level_florist_id);
  fetch exists_cur into v_time_difference;

  if exists_cur%notfound then

      insert into florist_suspend_history (
          top_level_florist_id,
          suspend_start_date,
          suspend_end_date,
          created_on,
          created_by,
          updated_on,
          updated_by)
      values (
          v_top_level_florist_id,
          in_suspend_start_date,
          in_suspend_end_date,
          sysdate,
          'SYS',
          sysdate,
          'SYS');

      out_status := 'Y';

  else

      select value into v_time_threshold from frp.global_parms where context = 'LOAD_MEMBER_DATA_CONFIG' and name = 'SUSPEND_HISTORY_THRESHOLD';
      if v_time_threshold is null then
          v_time_threshold := 5;
      end if;

      if v_time_difference > v_time_threshold then

          update florist_suspend_history
          set updated_on = sysdate,
          updated_by = 'Retry'
          where top_level_florist_id = v_top_level_florist_id
          and suspend_start_date = in_suspend_start_date
          and suspend_end_date = in_suspend_end_date;

          out_status := 'Y';

      else
          out_status := 'N';
          out_message := 'Record already exists';
      end if;

  end if;

  close exists_cur;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_FLORIST_SUSPEND_HISTORY;
/*
procedure update_florist_zip_cutoff
(
in_florist_id                   in florist_zips.florist_id%type,
in_zip_code                     in florist_zips.zip_code%type,
in_cutoff_time                  in florist_zips.cutoff_time%type,
in_updated_by                   in varchar2,
in_comments						in varchar2,
out_status                      out varchar2,
out_message                     out varchar2
)
as
	v_current_cutoff	ftd_apps.florist_zips.cutoff_time%type;
	v_florist_comments	varchar2(4000);
begin
	out_status :='Y';
	out_message := 'Success';
	
	begin
		select	max(cutoff_time)
		into	v_current_cutoff
		from	florist_zips
		where	florist_id = in_florist_id
		and 	zip_code = in_zip_code;
	exception
	when no_data_found then
		out_status :='N';
		out_message := 'Zipcode '||in_zip_code|| 'is not associated with florist '||in_florist_id||'.';
		return;
	when others then
		out_status := 'N';
		out_message := 'Exception occured while fetching cutoff-time for florist '||in_florist_id||' and zipcode '||in_zip_code||'. Exception message: '||sqlerrm;
		return;
	end;
	
	if(v_current_cutoff <> in_cutoff_time) then
	
		begin
			update  florist_zips
			set		last_update_date = sysdate,
					cutoff_time = nvl(in_cutoff_time, cutoff_time)
			where   florist_id = in_florist_id
			and     zip_code = in_zip_code;
		exception
		when others then
			out_status := 'N';
			out_message := 'Exception occured while updating florist '||in_florist_id||' zipcode '||in_zip_code||' cutoff-time to '||in_cutoff_time||'. Exception message : '||sqlerrm;
			return;
		end;
	
		v_florist_comments := 	'Cutoff was changed from'||v_current_cutoff||
								'to '||in_cutoff_time||
								'for Postal Code '||in_zip_code||
								'by MARS auto View Queue handling '||
								in_comments;
	
		insert_florist_comments
		(
			in_florist_id => in_florist_id,
			in_comment_type => 'Zip Codes',
			in_florist_comment => v_florist_comments,
			in_manager_only_flag => 'N',
			in_created_by => in_updated_by,
			out_status => out_status,
			out_message => out_message
		);
	
	end if;
		
exception
when others then
	out_status := 'N';
	out_message := 'Exception occured in main block of florist_maint_pkg.update_florist_zip_cutoff. Exception message: '||sqlerrm;
	return;
end update_florist_zip_cutoff;
*/


PROCEDURE UPDATE_FLORIST_ZIPS_MARS
(
 IN_FLORIST_ID                   IN FLORIST_ZIPS.FLORIST_ID%TYPE,
 IN_ZIP_CODE                     IN FLORIST_ZIPS.ZIP_CODE%TYPE,
 IN_BLOCK_INDICATOR              IN VARCHAR2,
 IN_BLOCK_END_DATE               IN FLORIST_ZIPS.BLOCK_END_DATE%TYPE,
 IN_CUTOFF_TIME                  IN FLORIST_ZIPS.CUTOFF_TIME%TYPE,
 IN_UPDATED_BY                   IN VARCHAR2,
 IN_ADDITIONAL_COMMENT           IN VARCHAR2,
 IN_ASSIGNMENT_TYPE              IN VARCHAR2,
 OUT_STATUS                      OUT VARCHAR2,
 OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting the florist
        zip information received from MARS.

        The available zips process for the given zip code is started.

Input:
        florist_id                      varchar2
        zip_code                        varchar2
        last_update_date                date
        block_indicator                 varchar2 (Y or N)
        block_end_date                  date
        cutoff_time                     varchar2
        updated by                      varchar2
        additional comment              varchar2 (addition to the comment for a particular app)
        assignment_type                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
        SELECT  count (1),
                max (decode (block_start_date, null, 'N', 'Y')) current_block_indicator,
                max (block_start_date) block_start_date
        FROM    florist_zips
        WHERE   florist_id = in_florist_id
        AND     zip_code = in_zip_code;

v_check                 number;
v_current_block_indicator varchar2(1);
v_comment_block         varchar2 (100);
v_block_start_date      FLORIST_ZIPS.BLOCK_START_DATE%TYPE;
v_action                varchar2(10);

BEGIN

	OPEN exists_cur;
	FETCH exists_cur INTO v_check, v_current_block_indicator, v_block_start_date;
	CLOSE exists_cur;

	IF in_block_indicator = 'N' THEN
			v_block_start_date := null;
	ELSE
			IF v_block_start_date IS NULL THEN
					v_block_start_date := sysdate;
			END IF;
	END IF;

	out_status := 'Y';
	
	UPDATE  florist_zips
	SET
			last_update_date = sysdate,
			block_start_date = v_block_start_date,
			block_end_date = in_block_end_date,
			cutoff_time = NVL(in_cutoff_time, cutoff_time),
			assignment_type = NVL(in_assignment_type, assignment_type)
	WHERE   florist_id = in_florist_id
	AND     zip_code = in_zip_code;

	-- insert the block comment on the update if the zip is block changed
	INSERT_FLORIST_COMMENTS
	(
			IN_FLORIST_ID => in_florist_id,
			IN_COMMENT_TYPE => 'Zip Codes',
			IN_FLORIST_COMMENT => in_additional_comment,
			IN_MANAGER_ONLY_FLAG => 'N',
			IN_CREATED_BY => in_updated_by,
			OUT_STATUS => out_status,
			OUT_MESSAGE => out_message
	);

	v_action := 'Update';
	
EXCEPTION WHEN OTHERS THEN
	BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;    -- end exception block

END UPDATE_FLORIST_ZIPS_MARS;

PROCEDURE INSERT_FLORIST_HOURS_OVERRIDE
(
In_Florist_Id                   In Florist_Hours_Override.Florist_Id%Type,
IN_OVERRIDE_DATE				        IN FLORIST_HOURS_OVERRIDE.OVERRIDE_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

    INSERT INTO FLORIST_HOURS_OVERRIDE (
        FLORIST_ID,
        DAY_OF_WEEK,
        OVERRIDE_DATE,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
		UPDATED_BY
    ) VALUES (
        In_Florist_Id,
        TRIM(TO_CHAR(IN_OVERRIDE_DATE,'DAY')),
        IN_OVERRIDE_DATE,
       	Sysdate,
       	'EROS',
       	Sysdate,
       	'EROS'
    );
    
    -- insert the Flroist Overrides comment on the insert if the Override is changed
        	ftd_apps.FLORIST_MAINT_PKG.INSERT_FLORIST_COMMENTS
					(
							IN_FLORIST_ID => In_Florist_Id,
							In_Comment_Type => 'Profile',
							IN_FLORIST_COMMENT => 'Florist Hours Changed - ' || TRIM(TO_CHAR(IN_OVERRIDE_DATE,'DAY')) || ' Closure Override - Shop open for ' || To_Char(IN_OVERRIDE_DATE,'mm/dd/yyyy'),
							IN_MANAGER_ONLY_FLAG => 'N',
							In_Created_By => 'EROS',
							OUT_STATUS => out_status,
							Out_Message => Out_Message
					);
          
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);
    
END INSERT_FLORIST_HOURS_OVERRIDE;

PROCEDURE DELETE_FLORIST_HOURS_OVERRIDE
(
In_Florist_Id                   In Florist_Hours_Override.Florist_Id%Type,
IN_OVERRIDE_DATE				        IN FLORIST_HOURS_OVERRIDE.OVERRIDE_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

v_open_close_code                Varchar2(10);
v_open_close_desc                 varchar2(100);

BEGIN

    Delete From Florist_Hours_Override 
    WHERE FLORIST_ID = In_Florist_Id AND TO_CHAR(OVERRIDE_DATE,'DD-MON-YYYY') = TO_CHAR(IN_OVERRIDE_DATE,'DD-MON-YYYY');
     
	 Select Open_Close_Code 
     INTO v_open_close_code
     From Ftd_Apps.Florist_Hours  
     Where Florist_Id= In_Florist_Id And Day_Of_Week = Trim(To_Char(In_Override_Date,'DAY'));
    
      If v_open_close_code  = 'F' Then
     V_Open_Close_Desc := 'closed';
     ElsIf v_open_close_code = 'A' Then
     V_Open_Close_Desc := 'AM closed';
     ElsIf V_Open_Close_Code= 'P' Then
     v_open_close_desc := 'PM closed';
      Else
     v_open_close_desc := 'Open';
     END IF; 
	 
      -- insert the Flroist Overrides comment on the Delete if the Override is changed
        	ftd_apps.FLORIST_MAINT_PKG.INSERT_FLORIST_COMMENTS
					(
							IN_FLORIST_ID => In_Florist_Id,
							In_Comment_Type => 'Profile',
							IN_FLORIST_COMMENT => 'Florist Hours Changed - ' || TRIM(TO_CHAR(IN_OVERRIDE_DATE,'DAY')) || ' Closure Override Deleted - Shop  ' || V_Open_Close_Desc ||' '||  To_Char(IN_OVERRIDE_DATE,'mm/dd/yyyy'),
							IN_MANAGER_ONLY_FLAG => 'N',
							In_Created_By => 'EROS',
							OUT_STATUS => out_status,
							Out_Message => Out_Message
					);
     
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_FLORIST_HOURS_OVERRIDE;

PROCEDURE UPDATE_FLORIST_HOURS_OVERRIDE
(
In_Florist_Id                   In Florist_Hours_Override.Florist_Id%Type,
IN_OVERRIDE_DATE				        IN FLORIST_HOURS_OVERRIDE.OVERRIDE_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

    Update Florist_Hours_Override 
    Set Updated_On= Sysdate,UPDATED_BY='EROS'
       	 WHERE FLORIST_ID = In_Florist_Id AND TO_CHAR(OVERRIDE_DATE,'DD-MON-YYYY') = TO_CHAR(IN_OVERRIDE_DATE,'DD-MON-YYYY');
     
      -- insert the Flroist Overrides comment on the Update if the Override is changed
        	ftd_apps.FLORIST_MAINT_PKG.INSERT_FLORIST_COMMENTS
					(
							IN_FLORIST_ID => In_Florist_Id,
							In_Comment_Type => 'Profile',
							IN_FLORIST_COMMENT => 'Florist Hours Changed - ' || TO_CHAR(IN_OVERRIDE_DATE,'DAY') || ' Closure Override Updated - Shop open for ' || To_Char(IN_OVERRIDE_DATE,'mm/dd/yyyy'),
							IN_MANAGER_ONLY_FLAG => 'N',
							In_Created_By => 'EROS',
							OUT_STATUS => out_status,
							Out_Message => Out_Message
					);
     
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLORIST_HOURS_OVERRIDE;

PROCEDURE UPDATE_FLORIST_CITY_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_CITY_BLOCKS.FLORIST_ID%TYPE,
IN_BLOCK_START_DATE				IN FLORIST_CITY_BLOCKS.BLOCK_START_DATE%TYPE,
IN_BLOCK_END_DATE				IN FLORIST_CITY_BLOCKS.BLOCK_END_DATE%TYPE,
IN_BLOCK_COMMENTS				IN FLORIST_COMMENTS.FLORIST_COMMENT%TYPE,
IN_UPDATED_BY					IN FLORIST_CITY_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR city_cur IS
        SELECT  city
        FROM    florist_master
        WHERE   florist_id = in_florist_id;

CURSOR exists_cur IS
        SELECT  block_end_date
        FROM    florist_city_blocks
        WHERE   florist_id = in_florist_id;

CURSOR branch_cur IS
    SELECT  florist_id
    FROM    florist_master
    WHERE   top_level_florist_id = (
            select top_level_florist_id
            from florist_master
            where florist_id = in_florist_id)
    AND     status <> 'Inactive'
    AND     record_type = 'R';

v_block_end_date        date;
v_comment_text          varchar2(4000);
v_florist_id            varchar2(100);
v_city_name             varchar2(4000);

BEGIN

    OPEN city_cur;
    FETCH city_cur INTO v_city_name;
    CLOSE city_cur;

    OPEN exists_cur;
    FETCH exists_cur INTO v_block_end_date;

    if exists_cur%notfound then
        insert into florist_city_blocks (
            florist_id,
            block_start_date,
            block_end_date,
            blocked_by_user_id
        ) values (
            in_florist_id,
            in_block_start_date,
            in_block_end_date,
            in_updated_by
        );
        if IN_BLOCK_COMMENTS is null then
          v_comment_text := in_florist_id || ' (' || v_city_name || ') was ';
          if in_block_end_date is null then
              v_comment_text := v_comment_text || 'permanently blocked';
          else
              v_comment_text := v_comment_text || 'blocked until ' || to_char(in_block_end_date, 'mm/dd/yyyy');
          end if;
        else
          v_comment_text := IN_BLOCK_COMMENTS;
        end if;

    else
        update florist_city_blocks
        set block_start_date = in_block_start_date,
            block_end_date = in_block_end_date,
            blocked_by_user_id = in_updated_by
        where florist_id = in_florist_id;

        if IN_BLOCK_COMMENTS is null then
          v_comment_text := in_florist_id || ' (' || v_city_name || ') block was changed from ';
          if v_block_end_date is null then
              v_comment_text := v_comment_text || 'permanent to ';
          else
              v_comment_text := v_comment_text || to_char(v_block_end_date, 'mm/dd/yyyy') || ' to ';
          end if;
          if in_block_end_date is null then
              v_comment_text := v_comment_text || 'permanent';
          else
              v_comment_text := v_comment_text || to_char(in_block_end_date, 'mm/dd/yyyy');
          end if;
        else
          v_comment_text := IN_BLOCK_COMMENTS;
        end if;
    end if;

    CLOSE exists_cur;

    OPEN branch_cur;
    FETCH branch_cur INTO v_florist_id;
    WHILE branch_cur%found LOOP

        -- insert the block comment
        INSERT_FLORIST_COMMENTS
        (
            IN_FLORIST_ID => v_florist_id,
            IN_COMMENT_TYPE => 'Cities',
            IN_FLORIST_COMMENT => v_comment_text,
            IN_MANAGER_ONLY_FLAG => 'N',
            IN_CREATED_BY => in_updated_by,
            OUT_STATUS => out_status,
            OUT_MESSAGE => out_message
        );

        FETCH branch_cur INTO v_florist_id;
    END LOOP;

    CLOSE branch_cur;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLORIST_CITY_BLOCKS;

PROCEDURE DELETE_FLORIST_CITY_BLOCKS
(
IN_FLORIST_ID                   IN FLORIST_CITY_BLOCKS.FLORIST_ID%TYPE,
IN_UPDATED_BY					IN FLORIST_CITY_BLOCKS.BLOCKED_BY_USER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR city_cur IS
        SELECT  city
        FROM    florist_master
        WHERE   florist_id = in_florist_id;

CURSOR branch_cur IS
    SELECT  florist_id
    FROM    florist_master
    WHERE   top_level_florist_id = (
            select top_level_florist_id
            from florist_master
            where florist_id = in_florist_id)
    AND     status <> 'Inactive'
    AND     record_type = 'R';

v_comment_text          varchar2(4000);
v_florist_id            varchar2(100);
v_city_name             varchar2(4000);

BEGIN

    OPEN city_cur;
    FETCH city_cur INTO v_city_name;
    CLOSE city_cur;

    delete from florist_city_blocks
    where florist_id = in_florist_id;
    
    v_comment_text := in_florist_id || ' (' || v_city_name || ') block was removed';

    OPEN branch_cur;
    FETCH branch_cur INTO v_florist_id;
    WHILE branch_cur%found LOOP

        -- insert the block comment
        INSERT_FLORIST_COMMENTS
        (
            IN_FLORIST_ID => v_florist_id,
            IN_COMMENT_TYPE => 'Cities',
            IN_FLORIST_COMMENT => v_comment_text,
            IN_MANAGER_ONLY_FLAG => 'N',
            IN_CREATED_BY => in_updated_by,
            OUT_STATUS => out_status,
            OUT_MESSAGE => out_message
        );
 
        FETCH branch_cur INTO v_florist_id;
    END LOOP;
 
    CLOSE branch_cur;
    
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_FLORIST_CITY_BLOCKS;

PROCEDURE INSERT_HOLIDAY_PRODUCTS
(
IN_PRODUCT_ID                   IN HOLIDAY_PRODUCTS.PRODUCT_ID%TYPE,
IN_DELIVERY_DATE                IN HOLIDAY_PRODUCTS.DELIVERY_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

CURSOR exists_cur IS
        SELECT  product_id
        FROM    holiday_products hp
        WHERE   hp.product_id = upper(in_product_id)
        AND     hp.delivery_date = in_delivery_date;

CURSOR product_cur IS
        SELECT  pm.product_id
        FROM    product_master pm
        WHERE   pm.product_id = upper(in_product_id)
        AND NOT EXISTS (select 'Y'
             FROM holiday_products hp
             WHERE hp.product_id = upper(in_product_id)
             and hp.delivery_date = in_delivery_date);

CURSOR upsell_cur IS
        SELECT  upsell_master_id
        FROM    upsell_master
        WHERE   upsell_master_id = upper(in_product_id);

v_product_id varchar2(1000);

BEGIN

    OUT_STATUS := 'Y';

    OPEN product_cur;
    FETCH product_cur INTO v_product_id;
    CLOSE product_cur;
    
    if v_product_id is null then
        OPEN exists_cur;
        FETCH exists_cur INTO v_product_id;
        CLOSE exists_cur;
	if v_product_id is not null then
            out_status := 'N';
	    out_message := 'Product ' || upper(in_product_id) || ' already exists';
	else
            OPEN upsell_cur;
            FETCH upsell_cur INTO v_product_id;
            CLOSE upsell_cur;
	    if v_product_id is not null then
                out_status := 'N';
	        out_message := 'Product ' || upper(in_product_id) || ' is an Upsell Master Id';
	    else
                out_status := 'N';
	        out_message := 'Product ' || upper(in_product_id) || ' not on file';
	    end if;
	end if;
    else
        insert into holiday_products (product_id, delivery_date)
        values
        (v_product_id, in_delivery_date);
    end if;

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_HOLIDAY_PRODUCTS;

PROCEDURE DELETE_HOLIDAY_PRODUCTS
(
IN_PRODUCT_ID					IN HOLIDAY_PRODUCTS.PRODUCT_ID%TYPE,
IN_DELIVERY_DATE                IN HOLIDAY_PRODUCTS.DELIVERY_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    delete from holiday_products
    where product_id = in_product_id
    and delivery_date = in_delivery_date;
    
    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_HOLIDAY_PRODUCTS;

PROCEDURE INSERT_EXCLUSION_ZONE_HEADER
(
IN_DESCRIPTION			IN VARCHAR2,
IN_BLOCK_START_DATE             IN DATE,
IN_BLOCK_END_DATE		IN DATE,
IN_CREATED_BY			IN VARCHAR2,
OUT_HEADER_ID			OUT PLS_INTEGER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

    select exclusion_zone_header_id_seq.nextval
    into out_header_id
    from dual;

    insert into exclusion_zone_header (
        exclusion_zone_header_id,
	exclusion_description,
	block_start_date,
	block_end_date,
	created_on,
	created_by,
	updated_on,
	updated_by
    ) values (
        out_header_id,
	in_description,
	in_block_start_date,
	in_block_end_date,
	sysdate,
	in_created_by,
	sysdate,
	in_created_by
    );

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_EXCLUSION_ZONE_HEADER;

PROCEDURE INSERT_EXCLUSION_ZONE_DETAIL
(
IN_HEADER_ID                    IN VARCHAR2,
IN_BLOCK_SOURCE			IN VARCHAR2,
IN_BLOCK_SOURCE_KEY             IN VARCHAR2,
IN_CREATED_BY			IN VARCHAR2,
OUT_ADDED_FLAG			OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

v_city varchar2(1000);
v_state varchar2(100);
v_block_start_date date;
v_block_end_date date;

cursor get_florist_zips_by_city(v_city varchar2, v_state varchar) is
    select fz.florist_id, fz.zip_code
    from zip_code zc
    join florist_zips fz
    on fz.zip_code = zc.zip_code_id
    where zc.city = v_city
    and zc.state_id = v_state
    and fz.block_start_date is null
    order by fz.zip_code, fz.florist_id;

cursor get_florists_by_city(v_city varchar2, v_state varchar) is
    select fm.florist_id, fm.record_type, status, ec.city_state_code
    from ftd_apps.efos_city_state_stage ec
    join ftd_apps.efos_state_codes es
    on es.state_id = v_state
    and trim(ec.city_name) = v_city
    and es.state_code = substr(ec.city_state_code, 1, 2)
    join ftd_apps.florist_master fm
    on fm.city_state_number = ec.city_state_code
    and fm.status <> 'Inactive'
    where not exists (select 'Y'
        from ftd_apps.florist_city_blocks fcb
	where fcb.florist_id = fm.florist_id)
    order by 1;

cursor get_cities_by_state(v_state_name varchar2) is
    select distinct zc.city, sm.state_master_id
    from zip_code zc
    join state_master sm
    on sm.state_name = v_state_name
    where zc.state_id = sm.state_master_id
    order by 1;

cursor get_florists_by_zip(v_zip_code varchar2) is
    select fz.florist_id, fz.zip_code
    from florist_zips fz
    where fz.zip_code = v_zip_code
    and fz.block_start_date is null
    order by fz.zip_code;

CURSOR branch_cur(v_florist_id varchar2) IS
    SELECT  florist_id
    FROM    florist_master
    WHERE   top_level_florist_id = (
            select top_level_florist_id
            from florist_master
            where florist_id = v_florist_id)
    AND     status <> 'Inactive'
    AND     record_type = 'R';

BEGIN

    OUT_ADDED_FLAG := 'N';

    select block_start_date, block_end_date
    into v_block_start_date, v_block_end_date
    from exclusion_zone_header
    where exclusion_zone_header_id = in_header_id;

    if in_block_source = 'CITY' then

        v_city := substr(in_block_source_key, 0, length(in_block_source_key) - 4);
        v_state := substr(in_block_source_key, -2);

        for rec in get_florist_zips_by_city(v_city, v_state) loop

            update ftd_apps.florist_zips
            set block_start_date = v_block_start_date,
                block_end_date = v_block_end_date,
                blocked_by_user_id = 'PEZ-' || in_header_id,
                last_update_date = sysdate
            where florist_id = rec.florist_id
            and zip_code = rec.zip_code;

            -- insert the block comment
            INSERT_FLORIST_COMMENTS (
                IN_FLORIST_ID => rec.florist_id,
                IN_COMMENT_TYPE => 'Zip Codes',
                IN_FLORIST_COMMENT => 'Zip Code ' || rec.zip_code || ' was Blocked',
                IN_MANAGER_ONLY_FLAG => 'N',
                IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_created_by,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
            );
 
	    OUT_ADDED_FLAG := 'Y';

	    end loop;

        for rec in get_florists_by_city(v_city, v_state) loop

            insert into ftd_apps.florist_city_blocks (
                florist_id,
	        block_start_date,
	        block_end_date,
	        blocked_by_user_id
            ) values (
                rec.florist_id,
	        v_block_start_date,
	        v_block_end_date,
                'PEZ-' || in_header_id
            );

            for cur_rec in branch_cur(rec.florist_id) loop
                -- insert the block comment
                INSERT_FLORIST_COMMENTS (
                    IN_FLORIST_ID => cur_rec.florist_id,
                    IN_COMMENT_TYPE => 'Cities',
                    IN_FLORIST_COMMENT => rec.florist_id || ' (' || v_city || ') was blocked until ' || to_char(v_block_end_date, 'mm/dd/yyyy'),
                    IN_MANAGER_ONLY_FLAG => 'N',
                    IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_created_by,
                    OUT_STATUS => out_status,
                    OUT_MESSAGE => out_message
                );
            end loop;
 
	        OUT_ADDED_FLAG := 'Y';

        end loop;

	    dbms_application_info.set_client_info('<' || v_city || '> <' || v_state || '> ' || out_added_flag);

    else

        if in_block_source = 'STATE' then

	    for city_rec in get_cities_by_state(in_block_source_key) loop

	        v_state := city_rec.state_master_id;

                for rec in get_florist_zips_by_city(city_rec.city, city_rec.state_master_id) loop

                    update ftd_apps.florist_zips
                    set block_start_date = v_block_start_date,
                        block_end_date = v_block_end_date,
                        blocked_by_user_id = 'PEZ-' || in_header_id,
		                last_update_date = sysdate
                    where florist_id = rec.florist_id
                    and zip_code = rec.zip_code;

                    -- insert the block comment
                    INSERT_FLORIST_COMMENTS (
                        IN_FLORIST_ID => rec.florist_id,
                        IN_COMMENT_TYPE => 'Zip Codes',
                        IN_FLORIST_COMMENT => 'Zip Code ' || rec.zip_code || ' was Blocked',
                        IN_MANAGER_ONLY_FLAG => 'N',
                        IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_created_by,
                        OUT_STATUS => out_status,
                        OUT_MESSAGE => out_message
                    );
 
	            OUT_ADDED_FLAG := 'Y';

        	end loop;

                for rec in get_florists_by_city(city_rec.city, city_rec.state_master_id) loop

                    insert into ftd_apps.florist_city_blocks (
                        florist_id,
	                block_start_date,
	                block_end_date,
	                blocked_by_user_id
                    ) values (
                        rec.florist_id,
	                v_block_start_date,
	                v_block_end_date,
                        'PEZ-' || in_header_id
                    );

                    for cur_rec in branch_cur(rec.florist_id) loop
                        -- insert the block comment
                        INSERT_FLORIST_COMMENTS (
                            IN_FLORIST_ID => cur_rec.florist_id,
                            IN_COMMENT_TYPE => 'Cities',
                            IN_FLORIST_COMMENT => rec.florist_id || ' (' || city_rec.city || ') was blocked until ' || to_char(v_block_end_date, 'mm/dd/yyyy'),
                            IN_MANAGER_ONLY_FLAG => 'N',
                            IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_created_by,
                            OUT_STATUS => out_status,
                            OUT_MESSAGE => out_message
                        );
                    end loop;
 
        	        OUT_ADDED_FLAG := 'Y';

                end loop;

	    end loop;
	
	    dbms_application_info.set_client_info(in_block_source_key || ' ' || v_state || ' ' || out_added_flag);

	else

	    if in_block_source= 'ZIP' then
	    
	        for rec in get_florists_by_zip(in_block_source_key) loop

                    update ftd_apps.florist_zips
                    set block_start_date = v_block_start_date,
                        block_end_date = v_block_end_date,
                        blocked_by_user_id = 'PEZ-' || in_header_id,
                        last_update_date = sysdate
                    where florist_id = rec.florist_id
                    and zip_code = rec.zip_code;

                    -- insert the block comment
                    INSERT_FLORIST_COMMENTS (
                        IN_FLORIST_ID => rec.florist_id,
                        IN_COMMENT_TYPE => 'Zip Codes',
                        IN_FLORIST_COMMENT => 'Zip Code ' || rec.zip_code || ' was Blocked',
                        IN_MANAGER_ONLY_FLAG => 'N',
                        IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_created_by,
                        OUT_STATUS => out_status,
                        OUT_MESSAGE => out_message
                    );
 
		    OUT_ADDED_FLAG := 'Y';

		    end loop;

	        dbms_application_info.set_client_info(in_block_source_key || ' ' || out_added_flag);

	    end if;

	end if;

    end if;

    if out_added_flag = 'Y' then

        insert into exclusion_zone_detail (
	    exclusion_zone_detail_id,
	    exclusion_zone_header_id,
	    block_source,
	    block_source_key,
	    created_on,
	    created_by,
	    updated_on,
	    updated_by
	)values (
	    exclusion_zone_detail_id_seq.nextval,
	    in_header_id,
	    in_block_source,
	    in_block_source_key,
	    sysdate,
	    in_created_by,
	    sysdate,
	    in_created_by
	);

    end if;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_EXCLUSION_ZONE_DETAIL;

PROCEDURE REMOVE_EXCLUSION_ZONE
(
IN_HEADER_ID                    IN VARCHAR2,
IN_USER_ID                      IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

cursor get_florists_zips(v_header_id varchar2) is
    select florist_id, zip_code
    from florist_zips
    where blocked_by_user_id = 'PEZ-' || v_header_id
    order by zip_code, florist_id;

cursor get_florist_city_blocks(v_header_id varchar2) is
    select fcb.florist_id, fm.city
    from florist_city_blocks fcb
    join florist_master fm
    on fm.florist_id = fcb.florist_id
    where fcb.blocked_by_user_id = 'PEZ-' || v_header_id
    order by fcb.florist_id;

CURSOR branch_cur(v_florist_id varchar2) IS
    SELECT  florist_id
    FROM    florist_master
    WHERE   top_level_florist_id = (
            select top_level_florist_id
            from florist_master
            where florist_id = v_florist_id)
    AND     status <> 'Inactive'
    AND     record_type = 'R';

BEGIN

    for rec in get_florists_zips(in_header_id) loop

        update ftd_apps.florist_zips
        set block_start_date = null,
            block_end_date = null,
            blocked_by_user_id = null,
            last_update_date = sysdate
        where florist_id = rec.florist_id
        and zip_code = rec.zip_code;

        -- insert the block comment
        INSERT_FLORIST_COMMENTS (
            IN_FLORIST_ID => rec.florist_id,
            IN_COMMENT_TYPE => 'Zip Codes',
            IN_FLORIST_COMMENT => 'Zip Code ' || rec.zip_code || ' was changed from Blocked to Active',
            IN_MANAGER_ONLY_FLAG => 'N',
            IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_user_id,
            OUT_STATUS => out_status,
            OUT_MESSAGE => out_message
        );
 
    end loop;

    for rec in get_florist_city_blocks(in_header_id) loop

        delete from ftd_apps.florist_city_blocks
        where florist_id = rec.florist_id;

        for cur_rec in branch_cur(rec.florist_id) loop
            -- insert the block comment
            INSERT_FLORIST_COMMENTS (
                IN_FLORIST_ID => cur_rec.florist_id,
                IN_COMMENT_TYPE => 'Cities',
                IN_FLORIST_COMMENT => rec.florist_id || ' (' || rec.city || ') block was removed',
                IN_MANAGER_ONLY_FLAG => 'N',
                IN_CREATED_BY => 'PEZ-' || in_header_id || ' / ' || in_user_id,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
            );
        end loop;
 
    end loop;
    
    delete from exclusion_zone_detail
    where exclusion_zone_header_id = in_header_id;
    
    delete from exclusion_zone_header
    where exclusion_zone_header_id = in_header_id;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END REMOVE_EXCLUSION_ZONE;

END FLORIST_MAINT_PKG;

/
