CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRICE_POINTS_LIST RETURN types.ref_cursor

 --==============================================================================
--
-- Name:    SP_GET_PRICE_POINTS_LIST
-- Type:    Function
-- Syntax:  SP_GET_PRICE_POINTS_LIST ( )
-- Returns: ref_cursor for
--          PRICE_POINT_ID           NUMBER(3)
--          PRICE_POINT_DESCRIPTION  VARCHAR2(25)
--
-- Description:   Queries the FILTER_PRICE_POINTS table and returns a ref cursor for row.
--
--===========================================================

AS
    price_points_cursor types.ref_cursor;
BEGIN
    OPEN price_points_cursor FOR
        SELECT price_point_id as "pricePointsId",
               price_point_description as "pricePointDescription"
          FROM FILTER_PRICE_POINTS;

    RETURN price_points_cursor;
END
;
.
/
