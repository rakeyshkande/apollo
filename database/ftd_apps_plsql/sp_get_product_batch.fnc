CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_PRODUCT_BATCH (
  inUserId       IN VARCHAR2,
  inProductId    IN VARCHAR2,
  inNovatorId    IN VARCHAR2
)
RETURN types.ref_cursor

--=============================================================================
--
-- Name:    SP_GET_PRODUCT_BATCH
--
-- Description: Queries the PRODUCT_MASTER_BATCH table and returns a ref cursor
--              to the resulting rows.  This table contains product information
--              that is to be "batched" to Novator.  The (required) inUserId input
--              parameter indicates the user to retrieve the batch changes for.
--              Either inProductId or inNovatorId (or neither) may be specified to
--              retrieve only for that product.
--
--=========================================================
AS
    product_batch_cursor types.ref_cursor;
BEGIN
    OPEN product_batch_cursor FOR
        SELECT PRODUCT_ID            as "productId",
               NOVATOR_ID            as "novatorId",
               LAST_UPDATE_USER_ID   as "lastUpdateUserId",
               STATUS                as "status",
               XML_DOCUMENT          as "xmlDocument",
               XML_ERRORS            as "xmlErrors"
          FROM PRODUCT_MASTER_BATCH
          WHERE
          -- Handle case were Product ID is specified
          (NVL(inProductId,'N/A') = 'N/A' OR PRODUCT_ID = inProductId)
          AND
          -- Handle case were Novator ID is specified
          (NVL(inNovatorId,'N/A') = 'N/A' OR NOVATOR_ID = inNovatorId)
          AND
          LAST_UPDATE_USER_ID = inUserId;

    RETURN product_batch_cursor;
END;
.
/
