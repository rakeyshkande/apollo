CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_COMPANY_BY_CPN RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_COMPANY_BY_CPN
-- Type:    Function
-- Syntax:  SP_GET_COMPANY_BY_ASN (phoneIn VARCHAR2 )
-- Returns: ref_cursor
--
--==================================================\

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT BUYER_SOURCE_CODE.ASN_NUMBER,BUYER_SOURCE_CODE.CLIENT_NAME,BUYER_SOURCE_CODE.SOURCE_CODE,URL
        FROM BUYER_SOURCE_CODE,CLIENT_URL
        where  BUYER_SOURCE_CODE.ASN_NUMBER = CLIENT_URL.ASN_NUMBER(+)
          and  exists ( select * from CPN_MAP WHERE CPN_MAP.SOURCE_CODE = BUYER_SOURCE_CODE.SOURCE_CODE)
        ORDER BY BUYER_SOURCE_CODE.CLIENT_NAME;

    RETURN cur_cursor;
END;
.
/
