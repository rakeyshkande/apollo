-- FTD_APPS.SNH_$ trigger to populate FTD_APPS.SNH$ shadow table

CREATE OR REPLACE TRIGGER FTD_APPS.snh_$
AFTER INSERT OR UPDATE OR DELETE ON ftd_apps.snh REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW

DECLARE

   c utl_smtp.connection;
   v_location VARCHAR2(10);

   CURSOR pagers_c (p_project IN VARCHAR2) IS
      SELECT pager_number
      FROM   sitescope.pagers
      WHERE  project = p_project;

   PROCEDURE send_email(v_action IN VARCHAR2, v_snh_id IN VARCHAR2,
      v_domestic_fee IN NUMBER, v_international_fee IN NUMBER, v_description IN VARCHAR2, v_vendor_charge IN NUMBER,
      v_sat_upcharge IN NUMBER, v_same_day_upcharge IN NUMBER, v_sun_upcharge IN NUMBER, v_mon_upcharge IN NUMBER, v_same_day_upcharge_fs IN NUMBER) AS
   BEGIN
      c := utl_smtp.open_connection(frp.misc_pkg.get_global_parm_value('Mail Server','NAME'));
      utl_smtp.helo(c, 'ftdi.com');
      utl_smtp.mail(c, 'oracle@ftdi.com');
      FOR pagers_r IN pagers_c ('SERVICE_FEE') LOOP
         utl_smtp.rcpt(c, pagers_r.pager_number);
      END LOOP;
      utl_smtp.open_data(c);
      utl_smtp.write_data(c, 'From: ' || v_location || utl_tcp.CRLF);
      FOR pagers_r IN pagers_c ('SERVICE_FEE') LOOP
         utl_smtp.write_data(c, 'To: ' || pagers_r.pager_number || utl_tcp.CRLF);
      END LOOP;
      utl_smtp.write_data(c, 'Subject: Service Charge Update' || utl_tcp.CRLF);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Service Charge ID:           ' || v_snh_id);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Action:                   ' || v_action);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Domestic (aka Florist) Charge:             ' || v_domestic_fee);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Same Day Upcharge:        ' || v_same_day_upcharge);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Same Day Upcharge-FS Members:        ' || v_same_day_upcharge_fs);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'International Charge:        ' || v_international_fee);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Description:              ' || v_description);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Vendor Charge:            ' || v_vendor_charge);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Vendor Saturday Upcharge: ' || v_sat_upcharge);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Vendor Sunday Upcharge: ' || v_sun_upcharge);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Vendor Monday Upcharge: ' || v_mon_upcharge);
      utl_smtp.close_data(c);
      utl_smtp.quit(c);
   END;

BEGIN

   SELECT sys_context('USERENV','DB_NAME')
   INTO   v_location
   FROM   dual;

   IF INSERTING THEN

      INSERT INTO ftd_apps.snh$ (
      	snh_id,
        description,
        first_order_domestic, 
        second_order_domestic, 
        third_order_domestic,
        first_order_international,
      	second_order_international, 
        third_order_international, 
        requested_by, 
        vendor_charge, 
        vendor_sat_upcharge, 
        vendor_sun_upcharge,
        vendor_mon_upcharge,
        created_by,
        created_on, 
        updated_by, 
        updated_on, 
        operation$, 
        timestamp$,
        same_day_upcharge,
        same_day_upcharge_fs
)
      VALUES (
        :NEW.snh_id, 
        :NEW.description, 
        :NEW.first_order_domestic, 
        :NEW.second_order_domestic, 
        :NEW.third_order_domestic,
        :NEW.first_order_international, 
        :NEW.second_order_international, 
        :NEW.third_order_international, 
        :NEW.requested_by,
        :NEW.vendor_charge, 
        :NEW.vendor_sat_upcharge, 
        :NEW.vendor_sun_upcharge,
        :NEW.vendor_mon_upcharge,
        :NEW.created_by, 
        :NEW.created_on, 
        :NEW.updated_by, 
        :NEW.updated_on,
        'INS', 
        sysdate,
        :NEW.same_day_upcharge,
        :NEW.same_day_upcharge_fs
      );
 
      send_email('Insert', :NEW.snh_id, :NEW.first_order_domestic, :NEW.first_order_international, :NEW.description,
      :NEW.vendor_charge, :NEW.vendor_sat_upcharge, :NEW.same_day_upcharge, :NEW.vendor_sun_upcharge, :NEW.vendor_mon_upcharge, :NEW.same_day_upcharge_fs);

   ELSIF UPDATING  THEN

      IF :NEW.created_by <> 'DELETION' THEN

         INSERT INTO ftd_apps.snh$ (
         	snh_id,
         	description,
         	first_order_domestic,
         	second_order_domestic,
         	third_order_domestic,
         	first_order_international,
         	second_order_international,
        	third_order_international,
            requested_by,
         	created_by,
        	created_on,
        	updated_by,
        	updated_on,      	
        	operation$, 
        	timestamp$,
            VENDOR_CHARGE,          
            VENDOR_SAT_UPCHARGE,
            VENDOR_SUN_UPCHARGE,
            VENDOR_MON_UPCHARGE,
            same_day_upcharge,
            same_day_upcharge_fs
         ) VALUES (
        	:OLD.snh_id,
        	:OLD.description,
        	:OLD.first_order_domestic,
        	:OLD.second_order_domestic,
        	:OLD.third_order_domestic,
        	:OLD.first_order_international,
        	:OLD.second_order_international,
        	:OLD.third_order_international,
            :OLD.requested_by,
        	:OLD.created_by,
        	:OLD.created_on,
        	:OLD.updated_by,
        	:OLD.updated_on,      	
        	'UPD_OLD',
        	SYSDATE,
            :OLD.VENDOR_CHARGE,          
            :OLD.VENDOR_SAT_UPCHARGE,
            :OLD.VENDOR_SUN_UPCHARGE,
            :OLD.VENDOR_MON_UPCHARGE,
            :OLD.same_day_upcharge,
            :OLD.same_day_upcharge_fs
         );

        INSERT INTO ftd_apps.snh$ (
        	snh_id,
        	description,
        	first_order_domestic,
        	second_order_domestic,
        	third_order_domestic,
        	first_order_international,
        	second_order_international,
        	third_order_international,
            requested_by,
        	created_by,
        	created_on,
        	updated_by,
        	updated_on,      	
        	operation$, 
        	timestamp$,
            VENDOR_CHARGE,          
            VENDOR_SAT_UPCHARGE,
            VENDOR_SUN_UPCHARGE,
            VENDOR_MON_UPCHARGE,
            same_day_upcharge,
            same_day_upcharge_fs
        ) VALUES (
        	:NEW.snh_id,
        	:NEW.description,
        	:NEW.first_order_domestic,
        	:NEW.second_order_domestic,
        	:NEW.third_order_domestic,
        	:NEW.first_order_international,
        	:NEW.second_order_international,
        	:NEW.third_order_international,
            :NEW.requested_by,
        	:NEW.created_by,
        	:NEW.created_on,
        	:NEW.updated_by,
        	:NEW.updated_on,      	
        	'UPD_NEW',
        	SYSDATE,
            :NEW.VENDOR_CHARGE,          
            :NEW.VENDOR_SAT_UPCHARGE,
            :NEW.VENDOR_SUN_UPCHARGE,
            :NEW.VENDOR_MON_UPCHARGE,
            :NEW.same_day_upcharge,
            :NEW.same_day_upcharge_fs
        );

         -- if the only thing that changed was the description then do not send the email.
         if (:NEW.snh_id                     <> :OLD.snh_id                     OR
             :NEW.first_order_domestic       <> :OLD.first_order_domestic       OR
             :NEW.second_order_domestic      <> :OLD.second_order_domestic      OR
             :NEW.third_order_domestic       <> :OLD.third_order_domestic       OR
             :NEW.first_order_international  <> :OLD.first_order_international  OR
             :NEW.second_order_international <> :OLD.second_order_international OR
             :NEW.third_order_international  <> :OLD.third_order_international  OR
             :NEW.vendor_charge              <> :OLD.vendor_charge              OR
             :NEW.vendor_sat_upcharge        <> :OLD.vendor_sat_upcharge        OR
             :NEW.vendor_sun_upcharge        <> :OLD.vendor_sun_upcharge        OR
             :NEW.vendor_mon_upcharge        <> :OLD.vendor_mon_upcharge        OR
             :NEW.same_day_upcharge          <> :OLD.same_day_upcharge			OR
             :NEW.same_day_upcharge_fs          <> :OLD.same_day_upcharge_fs          ) THEN
            send_email('Update', :NEW.snh_id, :NEW.first_order_domestic, :NEW.first_order_international, :NEW.description,
            :NEW.vendor_charge, :NEW.vendor_sat_upcharge, :NEW.same_day_upcharge, :NEW.vendor_sun_upcharge, :NEW.vendor_mon_upcharge, :NEW.same_day_upcharge_fs);
         end if;

      END IF;

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.snh$ (
      	snh_id,
      	description,
      	first_order_domestic,
      	second_order_domestic,
      	third_order_domestic,
      	first_order_international,
      	second_order_international,
      	third_order_international,
        requested_by,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,      	
      	operation$, 
      	timestamp$,
      	VENDOR_CHARGE,          
		VENDOR_SAT_UPCHARGE,
		VENDOR_SUN_UPCHARGE,
		VENDOR_MON_UPCHARGE,
        same_day_upcharge,
        same_day_upcharge_fs
      ) VALUES (
      	:OLD.snh_id,
      	:OLD.description,
      	:OLD.first_order_domestic,
      	:OLD.second_order_domestic,
      	:OLD.third_order_domestic,
      	:OLD.first_order_international,
      	:OLD.second_order_international,
      	:OLD.third_order_international,
        :OLD.requested_by,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,      	
      	'DEL',
      	SYSDATE,
        :OLD.VENDOR_CHARGE,          
        :OLD.VENDOR_SAT_UPCHARGE,
        :OLD.VENDOR_SUN_UPCHARGE,
        :OLD.VENDOR_MON_UPCHARGE,
        :OLD.same_day_upcharge,
        :OLD.same_day_upcharge_fs
      	);

      send_email('Delete', :OLD.snh_id, :OLD.first_order_domestic, :OLD.first_order_international, :OLD.description,
      :OLD.vendor_charge, :OLD.vendor_sat_upcharge, :OLD.same_day_upcharge, :OLD.vendor_sun_upcharge, :OLD.vendor_mon_upcharge, :OLD.same_day_upcharge_fs);

   END IF;

END;
/
