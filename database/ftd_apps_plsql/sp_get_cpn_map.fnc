CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_CPN_MAP (sourceCodeIn varchar2)
RETURN types.ref_cursor

 --==============================================================================
--
-- Name:    SP_GET_CPN_MAP
-- Type:    Function
-- Syntax:  SP_GET_CPN_MAP ( )
-- Returns: ref_cursor
--
-- Description:   Queries the CPN_MAP table and returns a ref cursor for row.
--
--===========================================================

AS
    ret_cursor types.ref_cursor;
BEGIN
    OPEN ret_cursor FOR
        SELECT
               C.PRODUCT_ID as "productID",
               C.CPN as "CPN",
               P.SENT_TO_NOVATOR_PROD,
               P.SENT_TO_NOVATOR_CONTENT,
               P.SENT_TO_NOVATOR_UAT,
               P.SENT_TO_NOVATOR_TEST
    FROM CPN_MAP C, PRODUCT_MASTER P
    WHERE C.SOURCE_CODE = sourceCodeIn AND
                    C.PRODUCT_ID = P.NOVATOR_ID
	ORDER BY C.PRODUCT_ID;

    RETURN ret_cursor;
END;
.
/
