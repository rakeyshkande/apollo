CREATE OR REPLACE
FUNCTION FTD_APPS.GET_SRC_PRG_REF_MAX_START_DATE
(
IN_SOURCE_CODE            IN VARCHAR2,
IN_DATE                   IN DATE
)
RETURN DATE

AS
/*-----------------------------------------------------------------------------
Description:
   Returns the max start date less than the date passed in for the
   source code passed in.

Input:
        source_code  VARCHAR2
        in_date      DATE

Output:
        start_date      DATE
-----------------------------------------------------------------------------*/
v_start_date        date;

BEGIN

     SELECT max(start_date)
       INTO v_start_date
       FROM ftd_apps.source_program_ref
      WHERE source_code = in_source_code
        AND start_date <= in_date;

     RETURN v_start_date;

END;
.
/
