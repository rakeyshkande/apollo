create or replace PACKAGE BODY FTD_APPS.SOURCE_MAINT_PKG
AS


PROCEDURE VIEW_MARKETING_GROUP_VAL
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the list of marketing
        groups

Input:
        marketing_group                 varchar2
        status                          varchar2

Output:
        cursor containing all records from marketing_group_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                marketing_group
        FROM    marketing_group_val
        WHERE   status = 'Active'
        ORDER BY marketing_group;

END VIEW_MARKETING_GROUP_VAL;



PROCEDURE INSERT_MARKETING_GROUP_VAL
(
IN_MARKETING_GROUP              IN MARKETING_GROUP_VAL.MARKETING_GROUP%TYPE,
IN_CSR_ID                       IN MARKETING_GROUP_VAL.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting new marketing groups

Input:
        marketing_group                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO marketing_group_val
(
        marketing_group,
        created_on,
        created_by
)
VALUES
(
        UPPER(in_marketing_group),
        sysdate,
        in_csr_id
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_MARKETING_GROUP_VAL;


PROCEDURE UPDATE_INSERT_PARTNER_PROGRAM
(
 IN_PROGRAM_NAME              IN PARTNER_PROGRAM.PROGRAM_NAME%TYPE,
 IN_PARTNER_NAME              IN PARTNER_PROGRAM.PARTNER_NAME%TYPE,
 IN_PROGRAM_TYPE              IN PARTNER_PROGRAM.PROGRAM_TYPE%TYPE,
 IN_EMAIL_EXCLUDE_FLAG        IN PARTNER_PROGRAM.EMAIL_EXCLUDE_FLAG%TYPE,
 IN_UPDATED_BY                IN PARTNER_PROGRAM.UPDATED_BY%TYPE,
 IN_PROGRAM_LONG_NAME         IN PARTNER_PROGRAM.PROGRAM_LONG_NAME%TYPE,
 IN_MEMBERSHIP_DATA_REQUIRED  IN PARTNER_PROGRAM.MEMBERSHIP_DATA_REQUIRED%TYPE,
 OUT_STATUS                  OUT VARCHAR2,
 OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the partner_program table.

Input:
        program_name              VARCHAR2
        partner_name              VARCHAR2
        program_type              VARCHAR2
        email_exclude_flag        CHAR
        updated_by                VARCHAR2
        program_long_name         VARCHAR2
        membership_data_required  VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE partner_program
    SET partner_name = in_partner_name,
        program_type = in_program_type,
        email_exclude_flag = in_email_exclude_flag,
        updated_on = SYSDATE,
        updated_by = in_updated_by,
        program_long_name = in_program_long_name,
        membership_data_required = in_membership_data_required
   WHERE program_name = in_program_name;


  IF SQL%NOTFOUND THEN
    INSERT INTO partner_program
       (program_name,
        partner_name,
        program_type,
        email_exclude_flag,
        created_on,
        created_by,
        updated_on,
        updated_by,
        program_long_name,
        membership_data_required)
      VALUES
       (in_program_name,
        in_partner_name,
        in_program_type,
        in_email_exclude_flag,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by,
        in_program_long_name,
        in_membership_data_required);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_PARTNER_PROGRAM;


PROCEDURE UPDATE_INSERT_PROG_RWRD_RANGE
(
 IN_PROGRAM_NAME        IN PROGRAM_REWARD_RANGE.PROGRAM_NAME%TYPE,
 IN_MIN_DOLLAR          IN PROGRAM_REWARD_RANGE.MIN_DOLLAR%TYPE,
 IN_MAX_DOLLAR          IN PROGRAM_REWARD_RANGE.MAX_DOLLAR%TYPE,
 IN_CALCULATION_BASIS   IN PROGRAM_REWARD_RANGE.CALCULATION_BASIS%TYPE,
 IN_POINTS              IN PROGRAM_REWARD.POINTS%TYPE,
 IN_UPDATED_BY          IN PROGRAM_REWARD.UPDATED_BY%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the program_reward_range table.

Input:
        program_name         VARCHAR2
        min_dollar           NUMBER
        max_dollar           NUMBER
        calculation_basis    VARCHAR2
        points               NUMBER
        updated_by           VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE program_reward_range
    SET min_dollar        = in_min_dollar,
        max_dollar        = in_max_dollar,
        calculation_basis = in_calculation_basis,
        points            = in_points,
        updated_on        = SYSDATE,
        updated_by        = in_updated_by
   WHERE program_name = in_program_name;

  IF SQL%NOTFOUND THEN
    INSERT INTO program_reward_range
       (program_name,
        min_dollar,
        max_dollar,
        calculation_basis,
        points,
        created_on,
        created_by,
        updated_on,
        updated_by)
      VALUES
       (in_program_name,
        in_min_dollar,
        in_max_dollar,
        in_calculation_basis,
        in_points,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_PROG_RWRD_RANGE;


PROCEDURE UPDATE_INSERT_PROGRAM_REWARD
(
 IN_PROGRAM_NAME                IN PROGRAM_REWARD.PROGRAM_NAME%TYPE,
 IN_CALCULATION_BASIS           IN PROGRAM_REWARD.CALCULATION_BASIS%TYPE,
 IN_POINTS                      IN PROGRAM_REWARD.POINTS%TYPE,
 IN_REWARD_TYPE                 IN PROGRAM_REWARD.REWARD_TYPE%TYPE,
 IN_CUSTOMER_INFO               IN PROGRAM_REWARD.CUSTOMER_INFO%TYPE,
 IN_REWARD_NAME                 IN PROGRAM_REWARD.REWARD_NAME%TYPE,
 IN_PARTICIPANT_ID_LENGTH_CHECK IN PROGRAM_REWARD.PARTICIPANT_ID_LENGTH_CHECK%TYPE,
 IN_PARTICIPANT_EMAIL_CHECK     IN PROGRAM_REWARD.PARTICIPANT_EMAIL_CHECK%TYPE,
 IN_MAXIMUM_POINTS              IN PROGRAM_REWARD.MAXIMUM_POINTS%TYPE,
 IN_BONUS_CALCULATION_BASIS     IN PROGRAM_REWARD.BONUS_CALCULATION_BASIS%TYPE,
 IN_BONUS_POINTS                IN PROGRAM_REWARD.BONUS_POINTS%TYPE,
 IN_BONUS_SEPARATE_DATA         IN PROGRAM_REWARD.BONUS_SEPARATE_DATA%TYPE,
 IN_UPDATED_BY                  IN PROGRAM_REWARD.UPDATED_BY%TYPE,
 OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE                   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the program_reward table.

Input:
        program_name                VARCHAR2
        calculation_basis           VARCHAR2
        points                      NUMBER
        reward_type                 VARCHAR2
        customer_info               VARCHAR2
        reward_name                 VARCHAR2
        participant_id_length_check VARCHAR2
        participant_email_check     VARCHAR2
        maximum_points              NUMBER
        updated_by                  VARCHAR2
        bonus_calculation_basis     VARCHAR2
        bonus_points                NUMBER
        bonus_separate_data         VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE program_reward
    SET calculation_basis           = in_calculation_basis,
        points                      = in_points,
        reward_type                 = in_reward_type,
        customer_info               = in_customer_info,
        reward_name                 = in_reward_name,
        participant_id_length_check = in_participant_id_length_check,
        participant_email_check     = in_participant_email_check,
        maximum_points              = in_maximum_points,
        bonus_calculation_basis     = in_bonus_calculation_basis,
        bonus_points                = in_bonus_points,
        bonus_separate_data         = in_bonus_separate_data,
        updated_on                  = SYSDATE,
        updated_by                  = in_updated_by
   WHERE program_name = in_program_name;

  IF SQL%NOTFOUND THEN
    INSERT INTO program_reward
       (program_name,
        calculation_basis,
        points,
        reward_type,
        customer_info,
        reward_name,
        participant_id_length_check,
        participant_email_check,
        maximum_points,
        bonus_calculation_basis,
        bonus_points,
        bonus_separate_data,
        created_on,
        created_by,
        updated_on,
        updated_by)
      VALUES
       (in_program_name,
        in_calculation_basis,
        in_points,
        in_reward_type,
        in_customer_info,
        in_reward_name,
        in_participant_id_length_check,
        in_participant_email_check,
        in_maximum_points,
        in_bonus_calculation_basis,
        in_bonus_points,
        in_bonus_separate_data,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_PROGRAM_REWARD;


PROCEDURE UPDATE_INSERT_SOURCE_MASTER
(
 IN_SOURCE_CODE                 IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_DESCRIPTION                 IN SOURCE_MASTER.DESCRIPTION%TYPE,
 IN_SNH_ID                      IN SOURCE_MASTER.SNH_ID%TYPE,
 IN_PRICE_HEADER_ID             IN SOURCE_MASTER.PRICE_HEADER_ID%TYPE,
 IN_SEND_TO_SCRUB               IN SOURCE_MASTER.SEND_TO_SCRUB%TYPE,
 IN_FRAUD_FLAG                  IN SOURCE_MASTER.FRAUD_FLAG%TYPE,
 IN_ENABLE_LP_PROCESSING        IN SOURCE_MASTER.ENABLE_LP_PROCESSING%TYPE,
 IN_EMERGENCY_TEXT_FLAG         IN SOURCE_MASTER.EMERGENCY_TEXT_FLAG%TYPE,
 IN_REQUIRES_DELIVERY_CONFIRM   IN SOURCE_MASTER.REQUIRES_DELIVERY_CONFIRMATION%TYPE,
 IN_SOURCE_TYPE                 IN SOURCE_MASTER.SOURCE_TYPE%TYPE,
 IN_DEPARTMENT_CODE             IN SOURCE_MASTER.DEPARTMENT_CODE%TYPE,
 IN_START_DATE                  IN SOURCE_MASTER.START_DATE%TYPE,
 IN_END_DATE                    IN SOURCE_MASTER.END_DATE%TYPE,
 IN_PAYMENT_METHOD_ID           IN SOURCE_MASTER.PAYMENT_METHOD_ID%TYPE,
 IN_LIST_CODE_FLAG              IN SOURCE_MASTER.LIST_CODE_FLAG%TYPE,
 IN_DEFAULT_SOURCE_CODE_FLAG    IN SOURCE_MASTER.DEFAULT_SOURCE_CODE_FLAG%TYPE,
 IN_BILLING_INFO_PROMPT         IN SOURCE_MASTER.BILLING_INFO_PROMPT%TYPE,
 IN_BILLING_INFO_LOGIC          IN SOURCE_MASTER.BILLING_INFO_LOGIC%TYPE,
 IN_MARKETING_GROUP             IN SOURCE_MASTER.MARKETING_GROUP%TYPE,
 IN_EXTERNAL_CALL_CENTER_FLAG   IN SOURCE_MASTER.EXTERNAL_CALL_CENTER_FLAG%TYPE,
 IN_HIGHLIGHT_DESCRIPTION_FLAG  IN SOURCE_MASTER.HIGHLIGHT_DESCRIPTION_FLAG%TYPE,
 IN_DISCOUNT_ALLOWED_FLAG       IN SOURCE_MASTER.DISCOUNT_ALLOWED_FLAG%TYPE,
 IN_BIN_NUMBER_CHECK_FLAG       IN SOURCE_MASTER.BIN_NUMBER_CHECK_FLAG%TYPE,
 IN_ORDER_SOURCE                IN SOURCE_MASTER.ORDER_SOURCE%TYPE,
 IN_YELLOW_PAGES_CODE           IN SOURCE_MASTER.YELLOW_PAGES_CODE%TYPE,
 IN_JCPENNEY_FLAG               IN SOURCE_MASTER.JCPENNEY_FLAG%TYPE,
 IN_COMPANY_ID                  IN SOURCE_MASTER.COMPANY_ID%TYPE,
 IN_PROMOTION_CODE              IN SOURCE_MASTER.PROMOTION_CODE%TYPE,
 IN_BONUS_PROMOTION_CODE        IN SOURCE_MASTER.BONUS_PROMOTION_CODE%TYPE,
 IN_UPDATED_BY                  IN SOURCE_MASTER.UPDATED_BY%TYPE,
 IN_REQUESTED_BY                IN SOURCE_MASTER.REQUESTED_BY%TYPE,
 IN_PROGRAM_WEBSITE_URL         IN SOURCE_MASTER.PROGRAM_WEBSITE_URL%TYPE,
 IN_RELATED_SOURCE_CODE         IN SOURCE_MASTER.RELATED_SOURCE_CODE %TYPE,
 IN_COMMENT_TEXT                IN SOURCE_MASTER.COMMENT_TEXT%TYPE,
 IN_PARTNER_BANK_ID				IN SOURCE_MASTER.PARTNER_BANK_ID%TYPE,
 IN_WEBLOYALTY_FLAG             IN SOURCE_MASTER.WEBLOYALTY_FLAG%TYPE,
 IN_IOTW_FLAG                   IN SOURCE_MASTER.IOTW_FLAG%TYPE,
 IN_INVOICE_PASSWORD            IN SOURCE_MASTER.INVOICE_PASSWORD%TYPE,
 IN_BILLING_INFO_FLAG           IN SOURCE_MASTER.IOTW_FLAG%TYPE,
 IN_GIFT_CERTIFICATE_FLAG       IN SOURCE_MASTER.IOTW_FLAG%TYPE,
 IN_MP_REDEMPTION_RATE_ID	IN SOURCE_MASTER.MP_REDEMPTION_RATE_ID%TYPE,
 IN_ADD_ON_FREE_ID              IN SOURCE_MASTER.ADD_ON_FREE_ID%TYPE,
 IN_DISPLAY_SERVICE_FEE_CODE    IN SOURCE_MASTER.DISPLAY_SERVICE_FEE_CODE%TYPE,
 IN_DISPLAY_SHIPPING_FEE_CODE   IN SOURCE_MASTER.DISPLAY_SHIPPING_FEE_CODE%TYPE,
 IN_PRIMARY_BACKUP_RWD_FLAG	IN SOURCE_MASTER.PRIMARY_BACKUP_RWD_FLAG%TYPE,
 IN_APPLY_SURCHARGE_CODE        IN SOURCE_MASTER.APPLY_SURCHARGE_CODE%TYPE,
 IN_SURCHARGE_AMOUNT            IN SOURCE_MASTER.SURCHARGE_AMOUNT%TYPE,
 IN_SURCHARGE_DESCRIPTION       IN SOURCE_MASTER.SURCHARGE_DESCRIPTION%TYPE,
 IN_DISPLAY_SURCHARGE_FLAG      IN SOURCE_MASTER.DISPLAY_SURCHARGE_FLAG%TYPE,
 IN_ALLOW_FREE_SHIPPING_FLAG    IN SOURCE_MASTER.ALLOW_FREE_SHIPPING_FLAG%TYPE,
 IN_SAME_DAY_UPCHARGE			IN SOURCE_MASTER.SAME_DAY_UPCHARGE%TYPE,
 IN_DISPLAY_SAME_DAY_UPCHARGE	IN SOURCE_MASTER.DISPLAY_SAME_DAY_UPCHARGE%TYPE,
 IN_MORNING_DELIVERY_FLAG       IN SOURCE_MASTER.MORNING_DELIVERY_FLAG%TYPE,
 IN_MORNING_DELIVERY_FS_MEMBERS IN SOURCE_MASTER.MORNING_DELIVERY_FREE_SHIPPING%TYPE,
 IN_DELIVERY_FEE_ID             IN SOURCE_MASTER.DELIVERY_FEE_ID%TYPE,
 IN_AUTO_PROMO_ENGINE_FLAG      IN SOURCE_MASTER.AUTO_PROMOTION_ENGINE%TYPE,
 IN_APE_PRODUCT_CATALOG         IN SOURCE_MASTER.APE_PRODUCT_CATALOG%TYPE,
 IN_OSCAR_SELECTION_FLAG	IN SOURCE_MASTER.OSCAR_SELECTION_ENABLED_FLAG%TYPE,
 IN_OSCAR_SCENARIO_GROUP_ID	        IN SOURCE_MASTER.OSCAR_SCENARIO_GROUP_ID%TYPE,
 IN_FUNERAL_CEMETERY_LOC_CHK    IN SOURCE_MASTER.FUNERAL_CEMETERY_LOC_CHK%TYPE,
 IN_HOSPITAL_LOC_CHCK           IN SOURCE_MASTER.HOSPITAL_LOC_CHCK%TYPE,
 IN_SYMPATHY_LEAD_TIME_CHK      IN SOURCE_MASTER.FUNERAL_CEMETERY_LEAD_TIME_CHK%TYPE,
 IN_SYMPATHY_LEAD_TIME           IN SOURCE_MASTER.FUNERAL_CEMETERY_LEAD_TIME%TYPE,
 IN_BO_HRS_MON_FRI_START        IN SOURCE_MASTER.BO_HRS_MON_FRI_START%TYPE,
 IN_BO_HRS_MON_FRI_END          IN SOURCE_MASTER.BO_HRS_MON_FRI_END%TYPE,
 IN_BO_HRS_SAT_START            IN SOURCE_MASTER.BO_HRS_SAT_START%TYPE,
 IN_BO_HRS_SAT_END              IN SOURCE_MASTER.BO_HRS_SAT_END%TYPE,
 IN_BO_HRS_SUN_START            IN SOURCE_MASTER.BO_HRS_SUN_START%TYPE,
 IN_BO_HRS_SUN_END              IN SOURCE_MASTER.BO_HRS_SUN_END%TYPE,
 IN_LEGACY_ID                   IN SOURCE_LEGACY_ID_MAPPING.LEGACY_ID%TYPE,
 IN_CALCULATE_TAX_FLAG          IN SOURCE_MASTER.CALCULATE_TAX_FLAG%TYPE,
 IN_SHIPPING_CARRIER_FALG       IN SOURCE_MASTER.CUSTOM_SHIPPING_CARRIER%TYPE,
 IN_MERCH_AMT_FULL_REFUND_FLAG   IN SOURCE_MASTER.MERCH_AMT_FULL_REFUND_FLAG%TYPE,
 IN_SAME_DAY_UPCHARGE_FS			IN SOURCE_MASTER.SAME_DAY_UPCHARGE_FS%TYPE,
 In_Recpt_Address               In Source_Master.Recpt_Address%Type,
 In_Recpt_City                  In Source_Master.Recpt_City%Type,
 In_Recpt_State                 In Source_Master.Recpt_State_Id%Type,
 In_Recpt_Country               In Source_Master.Recpt_Country_Id%Type,
 In_Recpt_Phone_Number          In Source_Master.Recpt_Phone%Type,
 IN_RECPT_ZIP_CODE              IN source_master.RECPT_ZIP_CODE%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE                    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the source_master table.

Input:
        source_code                    VARCHAR2
        description                    VARCHAR2
        snh_id                         VARCHAR2
        price_header_id                VARCHAR2
        send_to_scrub                  CHAR
        fraud_flag                     CHAR
        enable_lp_processing           CHAR
        emergency_text_flag            CHAR
        requires_delivery_confirmation CHAR
        source_type                    VARCHAR2
        department_code                VARCHAR2
        start_date                     DATE
        end_date                       DATE
        payment_method_id              VARCHAR2
        list_code_flag                 CHAR
        default_source_code_flag       CHAR
        billing_info_prompt            VARCHAR2
        billing_info_logic             VARCHAR2
        marketing_group                VARCHAR2
        external_call_center_flag      CHAR
        highlight_description_flag     CHAR
        discount_allowed_flag          CHAR
        bin_number_check_flag          CHAR
        order_source                   CHAR
        yellow_pages_code              VARCHAR2
        jcpenney_flag                  CHAR
        company_id                     VARCHAR2
        promotion_code                 VARCHAR2
        bonus_promotion_code           VARCHAR2
        updated_by                     VARCHAR2
        requested_by                   VARCHAR2
        program_website_url            VARCHAR2
        related_source_code            VARCHAR2
        comment_text                   VARCHAR2
        partner_bank_id		             INTEGER
        webloyalty_flag                CHAR
        iotw_flag                      CHAR
        invoice_password               VARCHAR2
      	mp_redemption_rate_id	         VARCHAR2
        display_service_fee_code       VARCHAR2
        display_shipping_fee_code      VARCHAR2
      	primary_backup_rwd_flag        CHAR
        apply_surcharge_code           VARCHAR2
        surcharge_amount               NUMBER
        surcharge_description          VARCHAR2
        display_surcharge_flag         VARCHAR2
        allow_free_shipping_flag       VARCHAR2
		same_day_upcharge			   VARCHAR2
		display_same_day_upcharge      VARCHAR2
		morning_delivery_flag		   CHAR
		morning_delivery_free_shipping CHAR
		delivery_fee_id                VARCHAR2
		AUTO_PROMOTION_ENGINE			varchar2
		APE_PRODUCT_CATALOG				varchar2
        oscar_selection_enabled_flag   CHAR
        oscar_scenario_group_id	       INTEGER
        calculate_tax_flag             CHAR
        custom_shipping_carrier        varchar2(3)
        MERCH_AMT_FULL_REFUND_FLAG     CHAR
        same_day_upcharge_fs		   VARCHAR2
        RECPT_ADDRESS                  VARCHAR2(90)  
		RECPT_ZIP_CODE                 VARCHAR2(12)  
		RECPT_CITY                     VARCHAR2(30)  
		RECPT_STATE_ID                 VARCHAR2(2)   
		RECPT_COUNTRY_ID               VARCHAR2(2)   
		RECPT_PHONE                    VARCHAR2(20)  
Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE source_master
    SET description                    = in_description,
        snh_id                         = in_snh_id,
        price_header_id                = in_price_header_id,
        send_to_scrub                  = in_send_to_scrub,
        fraud_flag                     = in_fraud_flag,
        enable_lp_processing           = in_enable_lp_processing,
        emergency_text_flag            = in_emergency_text_flag,
        requires_delivery_confirmation = in_requires_delivery_confirm,
        source_type                    = in_source_type,
        department_code                = in_department_code,
        start_date                     = in_start_date,
        end_date                       = in_end_date,
        payment_method_id              = in_payment_method_id,
        list_code_flag                 = in_list_code_flag,
        default_source_code_flag       = in_default_source_code_flag,
        billing_info_prompt            = in_billing_info_prompt,
        billing_info_logic             = in_billing_info_logic,
        marketing_group                = in_marketing_group,
        external_call_center_flag      = in_external_call_center_flag,
        highlight_description_flag     = in_highlight_description_flag,
        discount_allowed_flag          = in_discount_allowed_flag,
        bin_number_check_flag          = in_bin_number_check_flag,
        order_source                   = in_order_source,
        yellow_pages_code              = in_yellow_pages_code,
        jcpenney_flag                  = in_jcpenney_flag,
        company_id                     = in_company_id,
        promotion_code                 = in_promotion_code,
        bonus_promotion_code           = in_bonus_promotion_code,
        requested_by                   = in_requested_by,
        program_website_url            = in_program_website_url,
        related_source_code            = in_related_source_code,
        comment_text                   = in_comment_text,
        updated_on                     = SYSDATE,
        updated_by                     = in_updated_by,
        last_used_on_order_date        = SYSDATE,
        partner_bank_id	      	       = in_partner_bank_id,
        webloyalty_flag                = in_webloyalty_flag,
        iotw_flag                      = in_iotw_flag,
        invoice_password               = in_invoice_password,
        mp_redemption_rate_id	         = in_mp_redemption_rate_id,
        add_on_free_id                 = in_add_on_free_id,
        display_service_fee_code       = in_display_service_fee_code,
        display_shipping_fee_code      = in_display_shipping_fee_code,
	      primary_backup_rwd_flag        = in_primary_backup_rwd_flag,
        apply_surcharge_code           = in_apply_surcharge_code,
        surcharge_amount               = in_surcharge_amount,
        surcharge_description          = in_surcharge_description,
        display_surcharge_flag         = in_display_surcharge_flag,
        allow_free_shipping_flag       = in_allow_free_shipping_flag,
		same_day_upcharge			   = in_same_day_upcharge,
		display_same_day_upcharge	   = in_display_same_day_upcharge,
                morning_delivery_flag   = in_morning_delivery_flag,
				morning_delivery_free_shipping = in_morning_delivery_fs_members,
				delivery_fee_id  = in_delivery_fee_id,
				AUTO_PROMOTION_ENGINE = IN_AUTO_PROMO_ENGINE_FLAG,
				APE_PRODUCT_CATALOG = IN_APE_PRODUCT_CATALOG,
        oscar_selection_enabled_flag = in_oscar_selection_flag,
        oscar_scenario_group_id	      = in_oscar_scenario_group_id,
        funeral_cemetery_loc_chk	      = IN_FUNERAL_CEMETERY_LOC_CHK,
        hospital_loc_chck	      = IN_HOSPITAL_LOC_CHCK,
        funeral_cemetery_lead_time_chk	      = IN_SYMPATHY_LEAD_TIME_CHK,
        funeral_cemetery_lead_time	      = IN_SYMPATHY_LEAD_TIME,
        bo_hrs_mon_fri_start	      = IN_BO_HRS_MON_FRI_START,
        bo_hrs_mon_fri_end	      = IN_BO_HRS_MON_FRI_END,
        bo_hrs_sat_start	      = IN_BO_HRS_SAT_START,
        bo_hrs_sat_end	      = IN_BO_HRS_SAT_END,
        bo_hrs_sun_start	      = IN_BO_HRS_SUN_START,
        bo_hrs_sun_end	      = IN_BO_HRS_SUN_END,
        calculate_tax_flag    = IN_CALCULATE_TAX_FLAG,
        custom_shipping_carrier = IN_SHIPPING_CARRIER_FALG,
        MERCH_AMT_FULL_REFUND_FLAG = IN_MERCH_AMT_FULL_REFUND_FLAG,
        same_day_upcharge_fs		   = in_same_day_upcharge_fs,
        Recpt_Address = In_Recpt_Address,
        Recpt_City = In_Recpt_City,
        Recpt_State_Id = In_Recpt_State,
        Recpt_Country_Id = In_Recpt_Country,
        Recpt_Phone = In_Recpt_Phone_Number,
        RECPT_ZIP_CODE = IN_RECPT_ZIP_CODE

   WHERE source_code = in_source_code;
   
  IF SQL%NOTFOUND THEN
    INSERT INTO source_master
       (source_code,
        description,
        snh_id,
        price_header_id,
        send_to_scrub,
        fraud_flag,
        enable_lp_processing,
        emergency_text_flag,
        requires_delivery_confirmation,
        source_type,
        department_code,
        start_date,
        end_date,
        payment_method_id,
        list_code_flag,
        default_source_code_flag,
        billing_info_prompt,
        billing_info_logic,
        marketing_group,
        external_call_center_flag,
        highlight_description_flag,
        discount_allowed_flag,
        bin_number_check_flag,
        order_source,
        yellow_pages_code,
        jcpenney_flag,
        company_id,
        promotion_code,
        bonus_promotion_code,
        requested_by,
        program_website_url,
        related_source_code,
        comment_text,
        iotw_flag,
        invoice_password,
        created_on,
        created_by,
        updated_on,
        updated_by,
        last_used_on_order_date,
        partner_bank_id,
        mp_redemption_rate_id,
        add_on_free_id,
        display_service_fee_code,
        display_shipping_fee_code,
	      primary_backup_rwd_flag,
        apply_surcharge_code,
        surcharge_amount,
        surcharge_description,
        display_surcharge_flag,
        allow_free_shipping_flag,
		same_day_upcharge,
		display_same_day_upcharge,
                morning_delivery_flag,
                morning_delivery_free_shipping,
                delivery_fee_id,
		AUTO_PROMOTION_ENGINE,
                APE_PRODUCT_CATALOG,
                oscar_selection_enabled_flag,
                oscar_scenario_group_id,
                funeral_cemetery_loc_chk,
                hospital_loc_chck,
                funeral_cemetery_lead_time_chk,
                funeral_cemetery_lead_time,
                bo_hrs_mon_fri_start,
                bo_hrs_mon_fri_end,
                bo_hrs_sat_start,
                bo_hrs_sat_end,
                bo_hrs_sun_start,
                bo_hrs_sun_end,
                calculate_tax_flag,
                custom_shipping_carrier,
                MERCH_AMT_FULL_REFUND_FLAG,
                same_day_upcharge_fs,
                Recpt_Address,
                Recpt_Zip_Code,
                Recpt_City,
                Recpt_State_Id,
                Recpt_Country_Id,
                RECPT_PHONE
                )
      VALUES
       (in_source_code,
        in_description,
        in_snh_id,
        in_price_header_id,
        in_send_to_scrub,
        in_fraud_flag,
        in_enable_lp_processing,
        in_emergency_text_flag,
        in_requires_delivery_confirm,
        in_source_type,
        in_department_code,
        in_start_date,
        in_end_date,
        in_payment_method_id,
        in_list_code_flag,
        in_default_source_code_flag,
        in_billing_info_prompt,
        in_billing_info_logic,
        in_marketing_group,
        in_external_call_center_flag,
        in_highlight_description_flag,
        in_discount_allowed_flag,
        in_bin_number_check_flag,
        in_order_source,
        in_yellow_pages_code,
        in_jcpenney_flag,
        in_company_id,
        in_promotion_code,
        in_bonus_promotion_code,
        in_requested_by,
        in_program_website_url,
        in_related_source_code,
        in_comment_text,
        in_iotw_flag,
        in_invoice_password,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_partner_bank_id,
        in_mp_redemption_rate_id,
        in_add_on_free_id,
        in_display_service_fee_code,
        in_display_shipping_fee_code,
	      in_primary_backup_rwd_flag,
        in_apply_surcharge_code,
        in_surcharge_amount,
        in_surcharge_description,
        in_display_surcharge_flag,
        in_allow_free_shipping_flag,
		in_same_day_upcharge,
		in_display_same_day_upcharge,
                in_morning_delivery_flag,
                in_morning_delivery_fs_members,
                in_delivery_fee_id,
		IN_AUTO_PROMO_ENGINE_FLAG,
                IN_APE_PRODUCT_CATALOG,
                in_oscar_selection_flag,
                in_oscar_scenario_group_id,
    			in_funeral_cemetery_loc_chk,
    			in_hospital_loc_chck,
			    in_sympathy_lead_time_chk,
			    in_sympathy_lead_time,
			    in_bo_hrs_mon_fri_start,
				in_bo_hrs_mon_fri_end,
			    in_bo_hrs_sat_start,
			    in_bo_hrs_sat_end,
			    in_bo_hrs_sun_start,
			    in_bo_hrs_sun_end,
			    IN_CALCULATE_TAX_FLAG,
			    IN_SHIPPING_CARRIER_FALG,
			    IN_MERCH_AMT_FULL_REFUND_FLAG,
			    in_same_day_upcharge_fs,
			    In_Recpt_Address,
		        In_Recpt_Zip_Code,
		        In_Recpt_City,
		        In_Recpt_State,
		        In_Recpt_Country,
		        IN_RECPT_PHONE_NUMBER
			    );
          
          
  END IF;

  IF IN_BILLING_INFO_FLAG <> 'Y' THEN

      DELETE FROM FTD_APPS.BILLING_INFO
          WHERE SOURCE_CODE_ID = IN_SOURCE_CODE
          AND INFO_DESCRIPTION = 'CERTIFICATE#';

      IF IN_GIFT_CERTIFICATE_FLAG = 'Y' THEN

          INSERT INTO FTD_APPS.BILLING_INFO
              (SOURCE_CODE_ID, BILLING_INFO_SEQUENCE, INFO_DESCRIPTION)
              VALUES
              (IN_SOURCE_CODE, '1', 'CERTIFICATE#');

      END IF;

  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_SOURCE_MASTER;


PROCEDURE UPDATE_INSERT_SOURCE_MAST_RES
(
 IN_SOURCE_CODE_BEGIN   IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_SOURCE_CODE_END     IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_DESCRIPTION         IN SOURCE_MASTER_RESERVE.DESCRIPTION%TYPE,
 IN_SOURCE_TYPE         IN SOURCE_MASTER_RESERVE.SOURCE_TYPE%TYPE,
 IN_STATUS              IN SOURCE_MASTER_RESERVE.STATUS%TYPE,
 IN_UPDATED_BY				IN SOURCE_MASTER_RESERVE.UPDATED_BY%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/inserts records into the table
        source_master_reserve for the source code range passed in
        or for just one source code passed in.

Input:
        in_source_code_begin VARCHAR2
        in_source_code_end   VARCHAR2
        description          VARCHAR2
        source_type          VARCHAR2
        status               VARCHAR2
        updated_by			  VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  IF in_source_code_end IS NULL THEN

    UPDATE source_master_reserve
       SET description = in_description,
           source_type = in_source_type,
           status      = in_status,
           updated_by  = UPPER(in_updated_by),
           updated_on  = SYSDATE
     WHERE source_code = in_source_code_begin;

    IF SQL%NOTFOUND THEN
      INSERT INTO source_master_reserve
        (source_code,
         description,
         source_type,
         status,
         created_on,
         created_by,
         updated_on,
         updated_by)
        VALUES
        (in_source_code_begin,
         in_description,
         in_source_type,
         in_status,
         SYSDATE,
         UPPER(in_updated_by),
         SYSDATE,
         UPPER(in_updated_by));
    END IF;

  ELSE

    FOR i IN in_source_code_begin .. in_source_code_end
    LOOP

      UPDATE source_master_reserve
        SET description    = in_description,
            source_type    = in_source_type,
            status         = in_status,
            updated_by     = UPPER(in_updated_by),
            updated_on		= SYSDATE
      WHERE SOURCE_QUERY_PKG.ISNUMBER(source_code) = i;

      IF SQL%NOTFOUND THEN

        INSERT INTO source_master_reserve
           (source_code,
            description,
            source_type,
            status,
            created_on,
            created_by,
            updated_on,
            updated_by)
          VALUES
           (i,
            in_description,
            in_source_type,
            in_status,
            SYSDATE,
            UPPER(in_updated_by),
            SYSDATE,
            UPPER(in_updated_by));

      END IF;

    END LOOP;

  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    rollback;
  END;

END UPDATE_INSERT_SOURCE_MAST_RES;


PROCEDURE INSERT_SOURCE_MASTER_RESERVE
(
 IN_SOURCE_CODE_BEGIN   IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_SOURCE_CODE_END     IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_DESCRIPTION         IN SOURCE_MASTER_RESERVE.DESCRIPTION%TYPE,
 IN_SOURCE_TYPE         IN SOURCE_MASTER_RESERVE.SOURCE_TYPE%TYPE,
 IN_STATUS              IN SOURCE_MASTER_RESERVE.STATUS%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts records into the table
        source_master_reserve for the source code range passed in
        or for just one source code passed in.

Input:
        in_source_code_begin VARCHAR2
        in_source_code_end   VARCHAR2
        description          VARCHAR2
        source_type          VARCHAR2
        status               VARCHAR2


Output:
        status
        message

Special Instructions:

  		  This procedure is not being used currently.
-----------------------------------------------------------------------------*/

BEGIN

  IF in_source_code_end IS NULL THEN

    INSERT INTO source_master_reserve
        (source_code,
         description,
         source_type,
         status,
         created_on,
         created_by)
       VALUES
        (in_source_code_begin,
         in_description,
         in_source_type,
         in_status,
         SYSDATE,
         USER);

  ELSE

    FOR i IN in_source_code_begin .. in_source_code_end
    LOOP

      INSERT INTO source_master_reserve
           (source_code,
            description,
            source_type,
            status,
            created_on,
            created_by)
          VALUES
           (i,
            in_description,
            in_source_type,
            in_status,
            SYSDATE,
            USER);

    END LOOP;

  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    rollback;
  END;

END INSERT_SOURCE_MASTER_RESERVE;


PROCEDURE UPDATE_SOURCE_MASTER_RESERVE
(
 IN_SOURCE_CODE_BEGIN   IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_SOURCE_CODE_END     IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_DESCRIPTION         IN SOURCE_MASTER_RESERVE.DESCRIPTION%TYPE,
 IN_SOURCE_TYPE         IN SOURCE_MASTER_RESERVE.SOURCE_TYPE%TYPE,
 IN_STATUS              IN SOURCE_MASTER_RESERVE.STATUS%TYPE,
 IN_UPDATED_BY				IN SOURCE_MASTER_RESERVE.UPDATED_BY%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates records into the table
        source_master_reserve for the source code range passed in
        or for just one source code passed in.

Input:
        in_source_code_begin VARCHAR2
        in_source_code_end   VARCHAR2
        description          VARCHAR2
        source_type          VARCHAR2
        status               VARCHAR2
        updated_by			  VARCHAR2


Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  IF in_source_code_end IS NULL THEN

    UPDATE source_master_reserve
       SET description = in_description,
           source_type = in_source_type,
           status      = in_status,
           updated_by  = UPPER(in_updated_by),
           updated_on  = SYSDATE
     WHERE source_code = in_source_code_begin;

    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_message := 'WARNING: No source_master_reserve records were updated for source_code ' || in_source_code_begin || '.';
    END IF;

  ELSE

    FOR i IN in_source_code_begin .. in_source_code_end
    LOOP

      UPDATE source_master_reserve
         SET description    = in_description,
             source_type    = in_source_type,
             status         = in_status,
           	 updated_by     = UPPER(in_updated_by),
          	 updated_on     = SYSDATE
       WHERE SOURCE_QUERY_PKG.ISNUMBER(source_code) = i;

      IF SQL%FOUND THEN
         out_status := 'Y';
      ELSE
         out_status := 'N';
         out_message := 'WARNING: No source_master_reserve records were updated for source_code ' || i || '.';
         EXIT;
      END IF;

    END LOOP;

  END IF;



  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    rollback;
  END;

END UPDATE_SOURCE_MASTER_RESERVE;


PROCEDURE INACTIVE_SOURCE_MASTER_RESERVE
(
 IN_SOURCE_CODE_BEGIN   IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 IN_SOURCE_CODE_END     IN SOURCE_MASTER_RESERVE.SOURCE_CODE%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates each record in the source_master_reserve table
        to Inactive for source with in the range passed in.

Input:
        in_source_code_begin VARCHAR2
        in_source_code_end   VARCHAR2


Output:
        status
        message

Special Instructions:

  		  This procedure is not being used currently.
-----------------------------------------------------------------------------*/

BEGIN

  FOR i IN in_source_code_begin .. in_source_code_end
  LOOP

    UPDATE source_master_reserve
       SET status = 'Inactive'
     WHERE SOURCE_QUERY_PKG.ISNUMBER(source_code) = i;

  END LOOP;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    rollback;
  END;

END INACTIVE_SOURCE_MASTER_RESERVE;


PROCEDURE INSERT_SOURCE_TYPE_VAL
(
 IN_SOURCE_TYPE  IN SOURCE_TYPE_VAL.SOURCE_TYPE%TYPE,
 IN_CSR_ID       IN SOURCE_TYPE_VAL.CREATED_BY%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts a record into the source_type_val table.

Input:
        source_type  VARCHAR2
        csr_id       VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO source_type_val
       (source_type,
        status,
        created_on,
        created_by,
        updated_on,
        updated_by)
    VALUES
       (in_source_type,
        'Active',
        SYSDATE,
        UPPER(in_csr_id),
        SYSDATE,
        UPPER(in_csr_id));

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_SOURCE_TYPE_VAL;



PROCEDURE LOAD_PARTNER_SOURCE_MASTER
(
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will be a one time only run in production.  It populates
        the new partner/source schema from existing data in source, partners
        and promotion programs.  After the data is populated into the
        new schema, the source, partners and promotion programs tables will be
        renamed and views will be created in their place.  The view will be
        based off the the new schema.

Output:
        status
        message

Special Instructions:

  		  This procedure is not being used currently.
-----------------------------------------------------------------------------*/

BEGIN

-- fix some source data
update ftd_apps.source_old set points_miles_per_dollar = 10 where partner_id = 'UNIT3';
update ftd_apps.promotion_programs_old set base_points = 500, unit_points = 0 where promotion_id = 'UNIT5' and min_price = 0;

-- delete any existing data from the new source schema (for development and QA reruns)
delete from ftd_apps.source_program_ref;
execute immediate('alter table cost_centers disable constraint cost_centers_fk2');
delete from ftd_apps.source_master;
delete from ftd_apps.source_master_reserve;
delete from ftd_apps.source_type_val;
delete from ftd_apps.program_reward_range;
delete from ftd_apps.program_reward;
execute immediate('alter table PARTNER_PCARD_CONFIG_REF disable constraint PARTNER_PCARD_CONFIG_REF_F1');
execute immediate('alter table cost_centers disable constraint cost_centers_fk1');
delete from ftd_apps.partner_program;
delete from ftd_apps.partner_master;

-- populate the new schema from the existing source, partner, and promotion_programs tables
insert into ftd_apps.partner_master
(
        partner_name,
        created_on,
        created_by,
        updated_on,
        updated_by
)
select  distinct
        partner_name,
        sysdate,
        'SYS',
        sysdate,
        'SYS'
from    ftd_apps.partners_old p
where not exists
(
        select  1
        from    ftd_apps.partner_master m1
        where m1.partner_name = p.partner_name
);


insert into ftd_apps.partner_program
(
        program_name,
        partner_name,
        program_type,
        email_exclude_flag,
        created_on,
        created_by,
        updated_on,
        updated_by
)
select  distinct
        partner_id,
        partner_name,
        'Default',
        email_exclude_flag,
        sysdate,
        'SYS',
        sysdate,
        'SYS'
from    ftd_apps.partners_old p
where not exists
(
        select  1
        from    ftd_apps.partner_program m1
        where m1.program_name = p.partner_id
);

insert into ftd_apps.program_reward
(
        program_name,
        calculation_basis,
        points,
        reward_type,
        created_on,
        created_by,
        updated_on,
        updated_by,
        customer_info,
        reward_name,
        participant_id_length_check,
        participant_email_check,
        maximum_points,
        bonus_calculation_basis,
        bonus_points,
        bonus_separate_data
)
select  distinct
        s.partner_id,
        decode (s.mileage_calculation_source, 'MALL1', 'M', null, 'M', s.mileage_calculation_source),
        s.points_miles_per_dollar,
        p.reward_type,
        sysdate,
        'SYS',
        sysdate,
        'SYS',
        r.customer_info,
        r.reward_name,
        r.id_length,
        r.email_exclude_flag,
        (select distinct s1.maximum_points_miles from ftd_apps.source_old s1 where s1.partner_id = s.partner_id and s1.maximum_points_miles is not null and s1.start_date <= sysdate and (s1.end_date is null or s1.end_date >= sysdate)) maximum_points_miles,
        s1.bonus_type,
        s1.bonus_points,
        s1.separate_data
from    ftd_apps.source_old s
join    ftd_apps.partners_old r
on      s.partner_id = r.partner_id
join
(
        select  distinct
                promotion_id,
                reward_type
        from    ftd_apps.promotion_programs_old
) p
on      s.partner_id = p.promotion_id
left outer join
(
        select  distinct
                partner_id,
                decode (bonus_type, 'D', 'M', bonus_type) bonus_type,
                bonus_points,
                separate_data
        from    ftd_apps.source_old
        where   bonus_type is not null
        and     points_miles_per_dollar is not null
        and     start_date <= sysdate
        and     (end_date is null or end_date >= sysdate)
) s1
on      s1.partner_id = s.partner_id
where   s.points_miles_per_dollar is not null
and     s.start_date <= sysdate
and     (s.end_date is null or s.end_date >= sysdate);


insert into ftd_apps.program_reward_range
(
        program_name,
        min_dollar,
        max_dollar,
        calculation_basis,
        points,
        created_on,
        created_by,
        updated_on,
        updated_by
)
select  distinct
        pp.promotion_id,
        pp.min_price,
        pp.max_price,
        decode (pp.unit, 'DOL', 'F', 'ITM', 'L'),
        pp.base_points,
        sysdate,
        'SYS',
        sysdate,
        'SYS'
from    ftd_apps.promotion_programs_old pp
where   pp.reward_type in ('Miles', 'Points')
and     ((pp.min_price = 0 and pp.max_price <> 100000)
or       (pp.min_price <> 0 and pp.max_price = 100000))
and     exists
(
        select  1
        from    ftd_apps.program_reward pr
        where   pr.program_name = pp.promotion_id
);


insert into ftd_apps.source_type_val
(
        source_type,
        status,
        created_on,
        created_by
)
select  distinct
        s.marketing_group,
        'Active',
        sysdate,
        'SYS'
from    ftd_apps.source_old s
where   not exists
(
        select  1
        from    ftd_apps.source_type_val t
        where   t.source_type = s.marketing_group
);

insert into ftd_apps.source_type_val (source_type, status,created_on,created_by)
values ('NONE', 'Active', SYSDATE, 'SYS');

INSERT INTO ftd_apps.source_master_reserve
(
        source_code,
        description,
        source_type,
        status,
        created_on,
        created_by
)
SELECT
        source_code,
        description,
        source_type,
        'Active',
        sysdate,
        'SYS'
FROM    ftd_apps.source_old;

insert into ftd_apps.source_master
(
        source_code,
        description,
        snh_id,
        price_header_id,
        send_to_scrub,
        fraud_flag,
        enable_lp_processing,
        emergency_text_flag,
        requires_delivery_confirmation,
        created_on,
        created_by,
        updated_on,
        updated_by,
        source_type,
        department_code,
        start_date,
        end_date,
        payment_method_id,
        list_code_flag,
        default_source_code_flag,
        billing_info_prompt,
        billing_info_logic,
        marketing_group,
        external_call_center_flag,
        highlight_description_flag,
        discount_allowed_flag,
        bin_number_check_flag,
        order_source,
        yellow_pages_code,
        jcpenney_flag,
        company_id,
        promotion_code,
        bonus_promotion_code,
        requested_by,
        related_source_code,
        program_website_url
)
SELECT
        source_code,
        description,
        shipping_code,
        pricing_code,
        nvl (send_to_scrub, 'N'),
        fraud_flag,
        nvl (enable_lp_processing, 'N'),
        nvl (emergency_text_flag, 'N'),
        NVL (requires_delivery_confirmation, 'N'),
        sysdate,
        'SYS',
        sysdate,
        'SYS',
        marketing_group,
        department_code,
        start_date,
        end_date,
        valid_pay_method,
        list_code_flag,
        nvl (default_source_code_flag, 'N'),
        billing_info_prompt,
        billing_info_logic,
        source_type,
        external_call_center_flag,
        nvl (highlight_description_flag, 'N'),
        nvl (discount_allowed_flag, 'N'),
        bin_number_check_flag,
        order_source,
        yellow_pages_code,
        jcpenney_flag,
        company_id,
        promotion_code,
        null,
        'SYS',
        null,
        null
FROM    ftd_apps.source_old;

INSERT INTO ftd_apps.source_program_ref
(
        source_program_ref_id,
        source_code,
        program_name,
        start_date,
        created_on,
        created_by,
        updated_on,
        updated_by
)
SELECT
        ftd_apps.source_program_ref_id_sq.nextval,
        s.source_code,
        s.partner_id,
        trunc(s.start_date),
        sysdate,
        'SYS',
        sysdate,
        'SYS'
FROM    ftd_apps.source_old s
JOIN    ftd_apps.partner_program pp
ON      s.partner_id = pp.program_name;

execute immediate('alter table cost_centers enable constraint cost_centers_fk2');
execute immediate('alter table cost_centers enable constraint cost_centers_fk1');
execute immediate('alter table PARTNER_PCARD_CONFIG_REF enable constraint PARTNER_PCARD_CONFIG_REF_F1');

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
out_status := 'N';
out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
execute immediate('alter table cost_centers enable constraint cost_centers_fk1');
execute immediate('alter table cost_centers enable constraint cost_centers_fk2');
execute immediate('alter table PARTNER_PCARD_CONFIG_REF enable constraint PARTNER_PCARD_CONFIG_REF_F1');
RAISE;

END LOAD_PARTNER_SOURCE_MASTER;


PROCEDURE INSERT_SOURCE_PROGRAM_REF
(
 IN_SOURCE_CODE   IN SOURCE_PROGRAM_REF.SOURCE_CODE%TYPE,
 IN_PROGRAM_NAME  IN SOURCE_PROGRAM_REF.PROGRAM_NAME%TYPE,
 IN_START_DATE    IN SOURCE_PROGRAM_REF.START_DATE%TYPE,
 IN_CREATED_BY    IN SOURCE_PROGRAM_REF.CREATED_BY%TYPE,
 OUT_STATUS      OUT VARCHAR2,
 OUT_MESSAGE     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts a record into the source_program_ref table.

Input:
        source_code  VARCHAR2
        program_name VARCHAR2
        start_date   DATE
        created_by   VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO source_program_ref
       (source_program_ref_id,
        source_code,
        program_name,
        start_date,
        created_on,
        created_by,
        updated_on,
        updated_by)
      VALUES
       (source_program_ref_id_sq.NEXTVAL,
        in_source_code,
        in_program_name,
        in_start_date,
        SYSDATE,
        in_created_by,
        SYSDATE,
        in_created_by);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_SOURCE_PROGRAM_REF;


PROCEDURE UPDATE_SOURCE_PROGRAM_REF
(
 IN_SOURCE_PROGRAM_REF_ID  IN SOURCE_PROGRAM_REF.SOURCE_PROGRAM_REF_ID%TYPE,
 IN_SOURCE_CODE            IN SOURCE_PROGRAM_REF.SOURCE_CODE%TYPE,
 IN_PROGRAM_NAME           IN SOURCE_PROGRAM_REF.PROGRAM_NAME%TYPE,
 IN_START_DATE             IN SOURCE_PROGRAM_REF.START_DATE%TYPE,
 IN_UPDATED_BY             IN SOURCE_PROGRAM_REF.UPDATED_BY%TYPE,
 OUT_STATUS               OUT VARCHAR2,
 OUT_MESSAGE              OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates a record in the source_program_ref table
        for the source_program_ref_id passed in.

Input:
        source_program_ref_id NUMBER
        source_code           VARCHAR2
        program_name          VARCHAR2
        start_date            DATE
        updated_by            VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE source_program_ref
    SET source_code  = in_source_code,
        program_name = in_program_name,
        start_date   = in_start_date,
        updated_on   = SYSDATE,
        updated_by   = in_updated_by
  WHERE source_program_ref_id = in_source_program_ref_id;

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No source_program_ref records updated for source_program_ref_id ' || in_source_program_ref_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_SOURCE_PROGRAM_REF;


PROCEDURE UPDATE_PARTNER_PROG_LAST_POST
(
 IN_PROGRAM_NAME    IN PARTNER_PROGRAM.PROGRAM_NAME%TYPE,
 IN_LAST_POST_DATE  IN PARTNER_PROGRAM.LAST_POST_DATE%TYPE,
 IN_UPDATED_BY      IN PARTNER_PROGRAM.UPDATED_BY%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_MESSAGE       OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the last_post_date field of records
        in the partner_program table for the program_name passed in.

Input:
        program_name    VARCHAR2
        last_posted_on  DATE
        updated_by      VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE partner_program
    SET last_post_date  = in_last_post_date,
        updated_on      = SYSDATE,
        updated_by      = in_updated_by
   WHERE program_name = in_program_name;


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No partner_program updated for program_name ' || in_program_name ||'.';
  END IF;


  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_PARTNER_PROG_LAST_POST;



PROCEDURE LOAD_SOURCE_MASTER_RESERVE
(
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts source codes into the source master reserve
        from the load table containing data from the excel master spreadsheet.

Output:
        status
        message

Special Instructions:

  		  This procedure is not being used currently.
-----------------------------------------------------------------------------*/

BEGIN

-- insert any new source codes as inactive
INSERT INTO source_master_reserve
(
        source_code,
        description,
        source_type,
        status,
        created_on,
        created_by
)
SELECT  l.source_code,
        l.description,
        l.source_type,
        'Inactive',
        sysdate,
        'SYS'
FROM    source_master_reserve_stage l
WHERE   not exists
(
        select  1
        from    source_master_reserve s
        where   s.source_code = l.source_code
);

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
out_status := 'N';
out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
RAISE;

END LOAD_SOURCE_MASTER_RESERVE;



PROCEDURE UPDATE_INSERT_SRC_PART_BIN_MAP
(
 IN_SOURCE_CODE                IN SOURCE_PARTNER_BIN_MAPPING.SOURCE_CODE%TYPE,
 IN_PARTNER_NAME               IN SOURCE_PARTNER_BIN_MAPPING.PARTNER_NAME%TYPE,
 IN_BIN_PROC_ACTIVE_FLAG IN SOURCE_PARTNER_BIN_MAPPING.BIN_PROCESSING_ACTIVE_FLAG%TYPE,
 IN_UPDATED_BY                 IN SOURCE_PARTNER_BIN_MAPPING.UPDATED_BY%TYPE,
 OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE                   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts source bin mappings into the source_partner_bin_mapping table

Input:
        source_code                     VARCHAR2
        partner_name                    VARCHAR2
        bin_processing_active_flag      VARCHAR2
        in_updated_by                   VARCHAR2

Output:
        status
        message

Special Instructions:

  		  This procedure is not being used currently.
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE SOURCE_PARTNER_BIN_MAPPING
    SET SOURCE_CODE = IN_SOURCE_CODE,
        PARTNER_NAME = IN_PARTNER_NAME,
        BIN_PROCESSING_ACTIVE_FLAG = IN_BIN_PROC_ACTIVE_FLAG,
        UPDATED_BY = IN_UPDATED_BY,
        UPDATED_ON = SYSDATE

   WHERE SOURCE_CODE = IN_SOURCE_CODE
     AND PARTNER_NAME = IN_PARTNER_NAME;


  IF SQL%NOTFOUND THEN
    INSERT INTO SOURCE_PARTNER_BIN_MAPPING
       (SOURCE_CODE,
        PARTNER_NAME,
        BIN_PROCESSING_ACTIVE_FLAG,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY)
      VALUES
       (IN_SOURCE_CODE,
        IN_PARTNER_NAME,
        IN_BIN_PROC_ACTIVE_FLAG,
        SYSDATE,
        IN_UPDATED_BY,
        SYSDATE,
        IN_UPDATED_BY);
  END IF;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
OUT_STATUS := 'N';
OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
RAISE;

END UPDATE_INSERT_SRC_PART_BIN_MAP;

PROCEDURE DELETE_PROD_ATTR_SOURCE_EXCL
(
 IN_SOURCE_CODE                IN  PRODUCT_ATTR_RESTR_SOURCE_EXCL.SOURCE_CODE%TYPE,
 IN_PRODUCT_ATTR_RESTR_ID      IN  PRODUCT_ATTR_RESTR_SOURCE_EXCL.PRODUCT_ATTR_RESTR_ID%TYPE,
 OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE                   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure deletes all records in the product_attribute_source_excl table
        for the source_code passed in.

Input:
        source_code           VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  delete from product_attr_restr_source_excl
  where source_code = in_source_code
  and product_attr_restr_id = in_product_attr_restr_id;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_PROD_ATTR_SOURCE_EXCL;

PROCEDURE INSERT_PROD_ATTR_SOURCE_EXCL
(
 IN_SOURCE_CODE                IN  PRODUCT_ATTR_RESTR_SOURCE_EXCL.SOURCE_CODE%TYPE,
 IN_PRODUCT_ATTR_RESTR_ID      IN  PRODUCT_ATTR_RESTR_SOURCE_EXCL.PRODUCT_ATTR_RESTR_ID%TYPE,
 IN_UPDATED_BY                 IN  PRODUCT_ATTR_RESTR_SOURCE_EXCL.CREATED_BY%TYPE,
 OUT_STATUS                    OUT VARCHAR2,
 OUT_MESSAGE                   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts a record in the product_attribute_source_excl table
        for the source_code and product_attribute_id passed in.

Input:
        source_code           VARCHAR2
        product_attribute_id  VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  insert into product_attr_restr_source_excl (
      source_code,
      product_attr_restr_id,
      updated_on,
      updated_by,
      created_on,
      created_by)
  values (
      in_source_code,
      in_product_attr_restr_id,
      sysdate,
      in_updated_by,
      sysdate,
      in_updated_by);

  out_status := 'Y';

  EXCEPTION
    -- OK if exists.
    WHEN DUP_VAL_ON_INDEX THEN
      out_status := 'Y';
    WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_PROD_ATTR_SOURCE_EXCL;

PROCEDURE INSERT_SOURCE_FLORIST_PRIORITY
(
 IN_SOURCE_CODE         IN SOURCE_FLORIST_PRIORITY.SOURCE_CODE%TYPE,
 IN_FLORIST_ID          IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE,
 IN_PRIORITY            IN SOURCE_FLORIST_PRIORITY.PRIORITY%TYPE,
 IN_UPDATED_BY          IN SOURCE_FLORIST_PRIORITY.UPDATED_BY%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates a record in the source_florist_priority table.

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE source_florist_priority
    SET florist_id       = in_florist_id,
        priority          = in_priority,
        updated_on        = SYSDATE,
        updated_by        = in_updated_by
   WHERE source_code = in_source_code
     AND florist_id  = in_florist_id;

  IF SQL%NOTFOUND THEN
    INSERT INTO source_florist_priority
       (source_code,
        florist_id,
        priority,
        created_on,
        created_by,
        updated_on,
        updated_by)
      VALUES
       (in_source_code,
        in_florist_id,
        in_priority,
        SYSDATE,
        in_updated_by,
        SYSDATE,
        in_updated_by);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_SOURCE_FLORIST_PRIORITY;


PROCEDURE DELETE_SOURCE_FLORIST_PRIORITY
(
 IN_SOURCE_CODE         IN SOURCE_FLORIST_PRIORITY.SOURCE_CODE%TYPE,
 IN_FLORIST_ID          IN SOURCE_FLORIST_PRIORITY.FLORIST_ID%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates/creates deletes a record from the source_florist_priority table.

-----------------------------------------------------------------------------*/

BEGIN

  DELETE FROM source_florist_priority
   WHERE source_code = in_source_code
     AND florist_id = in_florist_id;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_SOURCE_FLORIST_PRIORITY;



PROCEDURE UPDATE_SRC_DFLT_ORDER_INFO
(
 IN_SOURCE_CODE                    IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_RECPT_LOCATION_TYPE            IN SOURCE_MASTER.RECPT_LOCATION_TYPE%TYPE,
 IN_RECPT_BUSINESS_NAME            IN SOURCE_MASTER.RECPT_BUSINESS_NAME%TYPE,
 IN_RECPT_LOCATION_DETAIL          IN SOURCE_MASTER.RECPT_LOCATION_DETAIL%TYPE,
 IN_RECPT_ADDRESS                  IN SOURCE_MASTER.RECPT_ADDRESS%TYPE,
 IN_RECPT_ZIP_CODE                 IN SOURCE_MASTER.RECPT_ZIP_CODE%TYPE,
 IN_RECPT_CITY                     IN SOURCE_MASTER.RECPT_CITY%TYPE,
 IN_RECPT_STATE_ID                 IN SOURCE_MASTER.RECPT_STATE_ID%TYPE,
 IN_RECPT_COUNTRY_ID               IN SOURCE_MASTER.RECPT_COUNTRY_ID%TYPE,
 IN_RECPT_PHONE                    IN SOURCE_MASTER.RECPT_PHONE%TYPE,
 IN_RECPT_PHONE_EXT                IN SOURCE_MASTER.RECPT_PHONE_EXT%TYPE,
 IN_CUST_FIRST_NAME                IN SOURCE_MASTER.CUST_FIRST_NAME%TYPE,
 IN_CUST_LAST_NAME                 IN SOURCE_MASTER.CUST_LAST_NAME%TYPE,
 IN_CUST_DAYTIME_PHONE             IN SOURCE_MASTER.CUST_DAYTIME_PHONE%TYPE,
 IN_CUST_DAYTIME_PHONE_EXT         IN SOURCE_MASTER.CUST_DAYTIME_PHONE_EXT%TYPE,
 IN_CUST_EVENING_PHONE             IN SOURCE_MASTER.CUST_EVENING_PHONE%TYPE,
 IN_CUST_EVENING_PHONE_EXT         IN SOURCE_MASTER.CUST_EVENING_PHONE_EXT%TYPE,
 IN_CUST_ADDRESS                   IN SOURCE_MASTER.CUST_ADDRESS%TYPE,
 IN_CUST_ZIP_CODE                  IN SOURCE_MASTER.CUST_ZIP_CODE%TYPE,
 IN_CUST_CITY                      IN SOURCE_MASTER.CUST_CITY%TYPE,
 IN_CUST_STATE_ID                  IN SOURCE_MASTER.CUST_STATE_ID%TYPE,
 IN_CUST_COUNTRY_ID                IN SOURCE_MASTER.CUST_COUNTRY_ID%TYPE,
 IN_CUST_EMAIL_ADDRESS             IN SOURCE_MASTER.CUST_EMAIL_ADDRESS%TYPE,
 IN_UPDATED_BY                     IN SOURCE_MASTER.UPDATED_BY%TYPE,
 OUT_STATUS                        OUT VARCHAR2,
 OUT_MESSAGE                       OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the default order information in the Source
		Master Table.

-----------------------------------------------------------------------------*/

BEGIN
  UPDATE source_master
    SET
        recpt_location_type        = in_recpt_location_type,
        recpt_business_name        = in_recpt_business_name,
        recpt_location_detail      = in_recpt_location_detail,
        recpt_address              = in_recpt_address,
        recpt_zip_code             = in_recpt_zip_code,
        recpt_city                 = in_recpt_city,
        recpt_state_id             = in_recpt_state_id,
        recpt_country_id           = in_recpt_country_id,
        recpt_phone                = in_recpt_phone,
        recpt_phone_ext            = in_recpt_phone_ext,
        cust_first_name            = in_cust_first_name,
        cust_last_name             = in_cust_last_name,
        cust_daytime_phone         = in_cust_daytime_phone,
        cust_daytime_phone_ext     = in_cust_daytime_phone_ext,
        cust_evening_phone         = in_cust_evening_phone,
        cust_evening_phone_ext     = in_cust_evening_phone_ext,
        cust_address               = in_cust_address,
        cust_zip_code              = in_cust_zip_code,
        cust_city                  = in_cust_city,
        cust_state_id              = in_cust_state_id,
        cust_country_id            = in_cust_country_id,
        cust_email_address         = in_cust_email_address,
        updated_on                 = SYSDATE,
        updated_by                 = in_updated_by
  WHERE source_code = in_source_code;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_SRC_DFLT_ORDER_INFO;

PROCEDURE UPDATE_SRC_MP_MEMBER_LVL_INFO
(
 IN_SOURCE_CODE         IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_MP_MEMBER_LEVEL_ID  IN SOURCE_MP_MEMBER_LEVEL.MP_MEMBER_LEVEL_ID%TYPE,
 IN_UPDATED_BY          IN SOURCE_MP_MEMBER_LEVEL.UPDATED_BY%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the source_code field of records
        in the source_mp_member_level table for the source_code passed in.

Input:
        SOURCE_CODE         VARCHAR2
        MP_MEMBER_LEVEL_ID  VARCHAR2
        UPDATED_BY          VARCHAR2

Output:
        status
        message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE FTD_APPS.SOURCE_MP_MEMBER_LEVEL
    SET SOURCE_CODE  = IN_SOURCE_CODE,
        UPDATED_ON      = SYSDATE,
        UPDATED_BY      = IN_UPDATED_BY
   WHERE MP_MEMBER_LEVEL_ID = IN_MP_MEMBER_LEVEL_ID;


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No SOURCE_CODE updated for source_code ' || in_source_code ||'.';
  END IF;


  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_SRC_MP_MEMBER_LVL_INFO;

PROCEDURE INSERT_APE_BASE_SOURCE_CODES
(
 IN_SOURCE_CODE         IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 IN_BASE_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.BASE_SOURCE_CODE%TYPE,
 IN_UPDATED_BY			IN APE_BASE_SOURCE_CODES.UPDATED_BY%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Inserts all base source codes for a given source_code.

Input:
        source_code  VARCHAR2
		BASE_SOURCE_CODE  VARCHAR2

Output:

------------------------------------------------------------------------------*/
BEGIN

	INSERT INTO FTD_APPS.APE_BASE_SOURCE_CODES
       (MASTER_SOURCE_CODE,
        BASE_SOURCE_CODE,
        CREATED_ON,
        CREATED_BY,
        UPDATED_ON,
        UPDATED_BY)
      VALUES
       (IN_SOURCE_CODE,
        IN_BASE_SOURCE_CODE,
        SYSDATE,
        IN_UPDATED_BY,
        SYSDATE,
        IN_UPDATED_BY);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_APE_BASE_SOURCE_CODES;

PROCEDURE DELETE_APE_BASE_SOURCE_CODES
(
 IN_SOURCE_CODE         IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 IN_BASE_SOURCE_CODE   IN APE_BASE_SOURCE_CODES.BASE_SOURCE_CODE%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Deletes all base source codes for a given source_code.

Input:
        source_code  VARCHAR2
		BASE_SOURCE_CODE  VARCHAR2

Output:

------------------------------------------------------------------------------*/
BEGIN

	DELETE FROM FTD_APPS.APE_BASE_SOURCE_CODES
	WHERE MASTER_SOURCE_CODE = IN_SOURCE_CODE
	and BASE_SOURCE_CODE = IN_BASE_SOURCE_CODE;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_APE_BASE_SOURCE_CODES;

PROCEDURE UPDATE_APE_FLAG
(
 IN_SOURCE_CODE         IN APE_BASE_SOURCE_CODES.MASTER_SOURCE_CODE%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   updates AUTO_PROMOTION_ENGINE to 'N' for a given source_code.

Input:
        source_code  VARCHAR2

Output:

------------------------------------------------------------------------------*/
BEGIN

	UPDATE FTD_APPS.SOURCE_MASTER
    SET AUTO_PROMOTION_ENGINE = 'N'
   WHERE SOURCE_CODE  = IN_SOURCE_CODE;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_APE_FLAG;


PROCEDURE DELETE_SC_DEPENDENCY
(
 IN_SOURCE_CODE         IN Source_legacy_Id_Mapping.SOURCE_CODE%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Delete the dependencies for a given source_code.

Input:
        source_code  VARCHAR2

Output:

------------------------------------------------------------------------------*/
BEGIN

	DELETE FROM  FTD_APPS.Source_legacy_Id_Mapping
    WHERE SOURCE_CODE  = IN_SOURCE_CODE;
  
 
  out_status := 'Y';
  

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_SC_DEPENDENCY;

PROCEDURE UPDATE_INSERT_SC_DEPENDENCY
(
 IN_SOURCE_CODE         IN Source_legacy_Id_Mapping.SOURCE_CODE%TYPE,
 IN_LEGACY_ID           IN Source_legacy_Id_Mapping.LEGACY_ID%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Delete the dependencies for a given source_code.

Input:
        source_code  VARCHAR2

Output:

------------------------------------------------------------------------------*/
BEGIN

    UPDATE ftd_apps.source_legacy_id_mapping
   set legacy_id = in_legacy_id
   where source_code = in_source_code;

  IF SQL%NOTFOUND THEN

      INSERT INTO FTD_APPS.SOURCE_LEGACY_ID_MAPPING
          (SOURCE_CODE,LEGACY_ID,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
          values
          (in_source_code,in_legacy_id,sysdate,'SYS',sysdate,'SYS');
          
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_SC_DEPENDENCY;

PROCEDURE DELETE_SOURCE_CODE_FLORISTS
(
 IN_SOURCE_CODE        IN SOURCE_FLORIST_PRIORITY.SOURCE_CODE%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure deletes florists associated with a Source code.

-----------------------------------------------------------------------------*/

BEGIN

  DELETE FROM source_florist_priority WHERE source_code = in_source_code;

  out_status := 'Y';

  IF SQL%FOUND THEN
     out_status := 'Y';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_SOURCE_CODE_FLORISTS;

PROCEDURE UPDATE_OSCAR_FLORIST_SELECTION(
  IN_SOURCE_TYPES                   IN  VARCHAR2,
  IN_OSCAR_FLORIST_FLAG             IN  VARCHAR2,
  IN_OSCAR_GROUP_ID                 IN  VARCHAR2,
  OUT_STATUS                        OUT VARCHAR2,
  OUT_MESSAGE                       OUT VARCHAR2
)
AS
v_result varchar2(1000);
v_sql1 varchar2(2000);
v_sql2 varchar2(2000);
BEGIN
    
    v_result := UPPER(IN_SOURCE_TYPES);
    select  regexp_replace(v_result,',' , ''',''') into v_result from dual;
    v_result := ''''||v_result||'''';
    if(UPPER(IN_OSCAR_FLORIST_FLAG)='Y' OR UPPER(IN_OSCAR_FLORIST_FLAG)='N')THEN
        v_sql1 := 'UPDATE FTD_APPS.SOURCE_MASTER SET OSCAR_SELECTION_ENABLED_FLAG='''||upper(IN_OSCAR_FLORIST_FLAG)||''',updated_by = ''Q1SP17-21'',updated_on =trunc(sysdate) WHERE UPPER(SOURCE_TYPE) IN ('||v_result||') AND OSCAR_SELECTION_ENABLED_FLAG <> '''||upper(IN_OSCAR_FLORIST_FLAG)||'''' ;
        execute IMMEDIATE v_sql1;
    END IF;
    IF(IN_OSCAR_GROUP_ID='1' OR IN_OSCAR_GROUP_ID='2')THEN
        v_sql2 := 'UPDATE FTD_APPS.SOURCE_MASTER SET OSCAR_SCENARIO_GROUP_ID='''||IN_OSCAR_GROUP_ID||''',updated_by = ''Q1SP17-21'',updated_on =trunc(sysdate) WHERE UPPER(SOURCE_TYPE) IN ('||v_result||') AND OSCAR_SCENARIO_GROUP_ID <> '''||IN_OSCAR_GROUP_ID||'''' ;
        execute IMMEDIATE v_sql2;
    END IF;

   IF SQL%ROWCOUNT = 0 THEN
     out_status := 'N';
     out_message := 'No record found/updated for source types ' || UPPER(v_result) || '.';
   ELSE
     out_status := 'Y';
     out_message := 'SUCCESS';
   END IF;
   COMMIT;
EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
END UPDATE_OSCAR_FLORIST_SELECTION;

PROCEDURE UPDATE_SC_ATTRIBUTES
(
 IN_SOURCE_CODE         IN SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_LOCATION            IN SOURCE_MASTER.DESCRIPTION%TYPE,
 IN_ADDRESS             IN Source_Master.Recpt_Address%Type,
 IN_CITY                IN Source_Master.Recpt_City%Type,
 IN_STATE               IN Source_Master.Recpt_State_Id%Type,
 IN_ZIPCODE             IN Source_Master.Recpt_Country_Id%Type,
 IN_COUNTRY             IN Source_Master.Recpt_Phone%Type,
 IN_PHONE_NUMBER        IN source_master.RECPT_ZIP_CODE%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*------------------------------------------------------------------------------
Description:
   Update attributes for a given source_code.

Input:
        source_code  VARCHAR2

Output:

------------------------------------------------------------------------------*/
BEGIN

    UPDATE FTD_APPS.SOURCE_MASTER
    SET 
     SOURCE_CODE = IN_SOURCE_CODE,
     DESCRIPTION = IN_LOCATION,
     RECPT_ADDRESS = IN_ADDRESS,
     RECPT_CITY = IN_CITY,
     RECPT_STATE_ID = IN_STATE,
     RECPT_COUNTRY_ID = IN_COUNTRY,
     RECPT_PHONE = IN_PHONE_NUMBER,
     RECPT_ZIP_CODE = IN_ZIPCODE,
     UPDATED_ON = sysdate,
     UPDATED_BY = 'Web Portal'
   WHERE SOURCE_CODE = IN_SOURCE_CODE;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_SC_ATTRIBUTES;

END;
.
/