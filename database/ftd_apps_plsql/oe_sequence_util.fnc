CREATE OR REPLACE
FUNCTION ftd_apps.oe_sequence_util RETURN types.ref_cursor AS
   v_return types.ref_cursor;
BEGIN
   OPEN v_return FOR
   SELECT big_sequence.nextval FROM dual;
   RETURN v_return;
END;
.
/
