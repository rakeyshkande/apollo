CREATE OR REPLACE
PACKAGE BODY                   FTD_APPS.CONTENT_QUERY_PKG
AS


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the values for the content.
        This procedure assumes the values can be retrieved based on the order
        detail id and the content_master entry.
 
        If the exact value cannot be found, then the default value is checked.
        If no default value can be found, a blank cursor is returned.
        

Input:
        in_content_context              content context for content_master
        in_content_name                 content name for content_master
        in_order_detail_id              order detail id for filter values

Output:
        cur                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_TEXT_WITH_FILTER
(
IN_CONTENT_CONTEXT                IN FTD_APPS.CONTENT_MASTER.CONTENT_CONTEXT%TYPE,
IN_CONTENT_NAME                   IN FTD_APPS.CONTENT_MASTER.CONTENT_NAME%TYPE,
IN_CONTENT_FILTER_1_VALUE         IN FTD_APPS.CONTENT_DETAIL.FILTER_1_VALUE%TYPE,
IN_CONTENT_FILTER_2_VALUE         IN FTD_APPS.CONTENT_DETAIL.FILTER_2_VALUE%TYPE,
OUT_CONTENT_TXT                   OUT FTD_APPS.CONTENT_DETAIL.CONTENT_TXT%TYPE
)
AS

    cursor content_with_1_2_cur is
        SELECT cd.content_txt
	FROM CONTENT_DETAIL cd, 
	     CONTENT_MASTER cm
	WHERE 1=1
	AND cm.content_master_id = cd.content_master_id
	AND cm.content_context = IN_CONTENT_CONTEXT
	AND cm.content_name = IN_CONTENT_NAME
	AND cd.filter_1_value = IN_CONTENT_FILTER_1_VALUE
	AND cd.filter_2_value = IN_CONTENT_FILTER_2_VALUE;

    cursor content_with_1_d_cur is
        SELECT cd.content_txt
	FROM CONTENT_DETAIL cd, 
	     CONTENT_MASTER cm
	WHERE 1=1
	AND cm.content_master_id = cd.content_master_id
	AND cm.content_context = IN_CONTENT_CONTEXT
	AND cm.content_name = IN_CONTENT_NAME
	AND cd.filter_1_value = IN_CONTENT_FILTER_1_VALUE
	AND cd.filter_2_value is null;

    cursor content_with_d_d_cur is
        SELECT cd.content_txt
	FROM CONTENT_DETAIL cd, 
	     CONTENT_MASTER cm
	WHERE 1=1
	AND cm.content_master_id = cd.content_master_id
	AND cm.content_context = IN_CONTENT_CONTEXT
	AND cm.content_name = IN_CONTENT_NAME
	AND cd.filter_1_value is null
	AND cd.filter_2_value is null;

    --v_content_detail content_with_1_2_cur%ROWTYPE;
    v_content_detail VARCHAR2(4000);

BEGIN

    OUT_CONTENT_TXT := null; 

    -- First get the row that matches both passed in filter values
    OPEN content_with_1_2_cur;
    FETCH content_with_1_2_cur into v_content_detail;
    OUT_CONTENT_TXT := v_content_detail; 
    CLOSE content_with_1_2_cur;

    IF(v_content_detail IS NULL) THEN
        -- Try getting a row with the filter 1 and default filter 2
        OPEN content_with_1_d_cur;
        FETCH content_with_1_d_cur into v_content_detail;
        OUT_CONTENT_TXT := v_content_detail; 
        CLOSE content_with_1_d_cur;
    END IF;
    IF(v_content_detail IS NULL) THEN
        -- Try getting a row with the default filter 1 and default filter 2
        OPEN content_with_d_d_cur;
        FETCH content_with_d_d_cur into v_content_detail;
        OUT_CONTENT_TXT := v_content_detail; 
        CLOSE content_with_d_d_cur;
    END IF;


END GET_CONTENT_TEXT_WITH_FILTER;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the rows in the 
        content_filter table

Input:

Output:
        cur                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_FILTER
(
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select 
            CONTENT_FILTER_ID,
            CONTENT_FILTER_NAME
        from content_filter;


END GET_CONTENT_FILTER;
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the rows in the 
        content_master table

Input:

Output:
        cur                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_MASTER
(
IN_CONTEXT_FILTER                 IN FTD_APPS.CONTENT_MASTER.CONTENT_CONTEXT%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select 
            CONTENT_MASTER_ID,
            CONTENT_CONTEXT,
            CONTENT_NAME,
            CONTENT_DESCRIPTION,
            CONTENT_FILTER_ID1,
            CONTENT_FILTER_ID2
        from content_master
        where (in_context_filter is NULL or content_context = in_context_filter)
        order by content_context, content_name;


END GET_CONTENT_MASTER;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the rows in the 
        content_detail table

Input:

Output:
        cur                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_DETAIL
(
IN_CONTEXT_FILTER                 IN FTD_APPS.CONTENT_MASTER.CONTENT_CONTEXT%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select 
            cd.CONTENT_DETAIL_ID,
            cd.CONTENT_MASTER_ID,
            cd.FILTER_1_VALUE,
            cd.FILTER_2_VALUE,
            cd.CONTENT_TXT,
            cm.content_context,
            cm.content_name,
            cm.content_filter_id1,
            cm.content_filter_id2
        from content_detail cd, content_master cm
        where 1=1
          and cm.content_master_id = cd.content_master_id
        and (in_context_filter is NULL or cm.content_context = in_context_filter)
        order by cm.content_context, cm.content_name;


END GET_CONTENT_DETAIL;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the rows in the 
        content_detail table that have the corresponding content_detail_id

Input:

Output:
        cur                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_DETAIL_BY_ID
(
IN_CONTENT_DETAIL_ID              IN FTD_APPS.CONTENT_DETAIL.CONTENT_DETAIL_ID%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
BEGIN
    OPEN out_cur for
        select 
            cd.CONTENT_DETAIL_ID,
            cd.CONTENT_MASTER_ID,
            cd.FILTER_1_VALUE,
            cd.FILTER_2_VALUE,
            cd.CONTENT_TXT,
            cm.content_context,
            cm.content_name,
            cm.content_filter_id1,
            cm.content_filter_id2,
            cf1.content_filter_name filter1_name,
            cf2.content_filter_name filter2_name
        from content_detail cd, content_master cm,
            content_filter cf1, content_filter cf2
        where 1=1
            and cd.content_detail_id = IN_CONTENT_DETAIL_ID
            and cm.content_master_id = cd.content_master_id
            and cf1.content_filter_id (+) = cm.content_filter_id1
            and cf2.content_filter_id (+) = cm.content_filter_id2;


END GET_CONTENT_DETAIL_BY_ID;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all the rows in the 
        content_master table that have the corresponding content_master_id

Input:

Output:
        cur                             Cursor

-----------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_MASTER_BY_ID
(
IN_CONTENT_MASTER_ID              IN FTD_APPS.CONTENT_MASTER.CONTENT_MASTER_ID%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN out_cur for
        select
            cm.content_master_id,
            cm.content_context,
            cm.content_name,
            cm.content_filter_id1,
            cm.content_filter_id2,
            cf1.content_filter_name filter1_name,
            cf2.content_filter_name filter2_name
        from content_master cm,
            content_filter cf1, content_filter cf2
        where 1=1
            and cm.content_master_id = in_content_master_id
            and cf1.content_filter_id (+) = cm.content_filter_id1
            and cf2.content_filter_id (+) = cm.content_filter_id2;

END GET_CONTENT_MASTER_BY_ID;

PROCEDURE GET_CONTEXT_LIST
(
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT DISTINCT CONTENT_CONTEXT
        FROM FTD_APPS.CONTENT_MASTER;

END GET_CONTEXT_LIST;


/*-----------------------------------------------------------------------------
Description:  This procedure retrieves the Preferred Partner's Extra Information to be displayed in JOE.

Input: 
	in_filter_1_value		varchar2
	in_content_context		varchar2
	in_content_name			varchar2

Output:
        cur                             Cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_PREFERRED_PINFO
(
IN_FILTER_1_VALUE                   IN VARCHAR2,
IN_CONTENT_CONTEXT                  IN VARCHAR2,
IN_CONTENT_NAME                     IN VARCHAR2,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN out_cur for
   SELECT    cd.filter_2_value filter_2_value, cd.content_txt content_txt
   FROM      ftd_apps.content_detail cd
   WHERE     cd.filter_1_value = IN_FILTER_1_VALUE
             AND cd.content_master_id = (
             SELECT cm.content_master_id content_master_id
             FROM ftd_apps.content_master cm
             WHERE cm.content_context = IN_CONTENT_CONTEXT
             AND cm.content_name = IN_CONTENT_NAME);

END GET_PREFERRED_PINFO;



END;
.
/
/
