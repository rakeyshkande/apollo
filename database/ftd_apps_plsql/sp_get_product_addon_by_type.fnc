CREATE OR REPLACE
FUNCTION FTD_APPS.SP_GET_PRODUCT_ADDON_BY_TYPE (productId IN varchar2, typeId IN number)
RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_PRODUCT_ADDON_BY_TYPE
-- Type:    Function
-- Syntax:  SP_GET_PRODUCT_ADDON_BY_TYPE ( productId IN VARCHAR2, typeId IN numbe)
-- Returns: ref_cursor for
--            FAA.ADDON_ID
--	      FAA.DESCRIPTION
--	      FAA.PRICE
--            FAA.ACTIVE_FLAG
--            FAA.PQUAD_ACCESSORY_ID
--            FAAT.ADDON_TYPE_ID
--	      FAAT.PRODUCT_FEED_FLAG
--     	      FAPA.ACTIVE_FLAG
--	      FAPA.DISPLAY_SEQ_NUM
--	      FAPA.MAX_QTY
--
-- Description:   Queries the PRODUCT_ADDON table by PRODUCT ID and ADDON_TYPE and returns a ref cursor
--                containing the resulting rows.
--
--========================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT         
      	FAA.ADDON_ID,
	FAA.DESCRIPTION,
	FAA.PRICE,
	FAA.ACTIVE_FLAG AS ADDON_ACTIVE_FLAG,
        FAA.PQUAD_ACCESSORY_ID,
      	FAAT.ADDON_TYPE_ID,
	FAAT.PRODUCT_FEED_FLAG,
     	FAPA.ACTIVE_FLAG AS PRODUCT_ADDON_ACTIVE_FLAG,
	FAPA.DISPLAY_SEQ_NUM,
	FAPA.MAX_QTY
        
          FROM FTD_APPS.ADDON FAA, FTD_APPS.ADDON_TYPE FAAT, FTD_APPS.PRODUCT_ADDON FAPA
          WHERE FAA.ADDON_TYPE = FAAT.ADDON_TYPE_ID
          AND FAAT.ADDON_TYPE_ID = typeId
          AND FAA.ADDON_ID = FAPA.ADDON_ID (+)
          AND productId = FAPA.PRODUCT_ID (+)

          ORDER by DECODE(FAPA.DISPLAY_SEQ_NUM, NULL, 999999, FAPA.DISPLAY_SEQ_NUM), FAA.DESCRIPTION ;
          
    RETURN cur_cursor;
END
;
.
/
