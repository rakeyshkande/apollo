create or replace procedure ftd_apps.SP_DELETE_PRODUCT_COMPLETE(
IN_PRODUCT_ID IN VARCHAR2
)
as
--==============================================================================
--
-- Name:    SP_DELETE_PRODUCT_COMPLETE
-- Type:    Procedure
-- Syntax:  SP_DELETE_PRODUCT_COMPLETE ( productId in varchar2 )
--
-- Description: Wrapper to delete a product and all associated relationships
--
--==============================================================================
v_product_id FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%type;
begin
     v_product_id := UPPER(IN_PRODUCT_ID);
     FTD_APPS.SP_REMOVE_KEYWORD(v_product_id);
     FTD_APPS.SP_REMOVE_EXCLUDED_STATES(v_product_id);
     FTD_APPS.SP_REMOVE_PRODUCT_COMPANY_2247(v_product_id);
     FTD_APPS.SP_REMOVE_RECIPIENT_SEARCH(v_product_id);
     FTD_APPS.SP_REMOVE_SHIP_METHODS(v_product_id);
     FTD_APPS.SP_REMOVE_PRODUCT_SHIP_DATES(v_product_id);
     FTD_APPS.SP_REMOVE_PRODUCT_SUBCODES(v_product_id);
     FTD_APPS.SP_REMOVE_PRODUCT_COLORS(v_product_id);
     FTD_APPS.SP_REMOVE_PRODUCT_HP(v_product_id);
     FTD_APPS.SP_DELETE_PRODUCT(v_product_id);
end SP_DELETE_PRODUCT_COMPLETE;
/
