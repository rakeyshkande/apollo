create or replace trigger ftd_apps.source_master_$
AFTER INSERT OR DELETE OR UPDATE ON ftd_apps.source_master
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE

v_exception     EXCEPTION;
v_exception_txt varchar2(200);
v_payload varchar2(2000);
v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_trigger_enabled varchar2(10) := 'N';


BEGIN

   IF :NEW.updated_by <> 'sp_update_source_last_used proc' THEN

   IF INSERTING THEN
      INSERT INTO ftd_apps.source_master$ (
      source_master$_id,
      source_code,
      description,
      snh_id,
      price_header_id,
      send_to_scrub,
      fraud_flag,
      enable_lp_processing,
      emergency_text_flag,
      requires_delivery_confirmation,
      created_on,
      created_by,
      updated_on,
      updated_by,
      source_type,
      department_code,
      start_date,
      end_date,
      payment_method_id,
      list_code_flag,
      default_source_code_flag,
      billing_info_prompt,
      billing_info_logic,
      marketing_group,
      external_call_center_flag,
      highlight_description_flag,
      discount_allowed_flag,
      bin_number_check_flag,
      order_source,
      yellow_pages_code,
      jcpenney_flag,
      company_id,
      promotion_code,
      bonus_promotion_code,
      requested_by,
      related_source_code,
      program_website_url,
      comment_text,
      partner_bank_id,
      operation$, timestamp$,
      last_used_on_order_date,
      mp_redemption_rate_id,
      webloyalty_flag,
      iotw_flag,
      invoice_password,
      add_on_free_id,
      DISPLAY_SERVICE_FEE_CODE,
      DISPLAY_SHIPPING_FEE_CODE,
      PRIMARY_BACKUP_RWD_FLAG,
      RECPT_LOCATION_TYPE,
      RECPT_BUSINESS_NAME,
      RECPT_LOCATION_DETAIL,
      RECPT_ADDRESS,
      RECPT_ZIP_CODE,
      RECPT_CITY,
      RECPT_STATE_ID,
      RECPT_COUNTRY_ID,
      RECPT_PHONE,
      RECPT_PHONE_EXT,
      CUST_FIRST_NAME,
      CUST_LAST_NAME,
      CUST_DAYTIME_PHONE,
      CUST_DAYTIME_PHONE_EXT,
      CUST_EVENING_PHONE,
      CUST_EVENING_PHONE_EXT,
      CUST_ADDRESS,
      CUST_ZIP_CODE,
      CUST_CITY,
      CUST_STATE_ID,
      CUST_COUNTRY_ID,
      CUST_EMAIL_ADDRESS,
      apply_surcharge_code,
      surcharge_description,
      display_surcharge_flag,
      surcharge_amount,
      allow_free_shipping_flag,
      same_day_upcharge,
      display_same_day_upcharge,
      morning_delivery_flag,
      MORNING_DELIVERY_FREE_SHIPPING,
      DELIVERY_FEE_ID,
      auto_promotion_engine,
      ape_product_catalog,
      OSCAR_SELECTION_ENABLED_FLAG,
      OSCAR_SCENARIO_GROUP_ID,
      calculate_tax_flag,
      custom_shipping_carrier,
      same_day_upcharge_fs
      ) VALUES (
      source_master$_sq.nextval,
      :NEW.source_code,
      :NEW.description,
      :NEW.snh_id,
      :NEW.price_header_id,
      :NEW.send_to_scrub,
      :NEW.fraud_flag,
      :NEW.enable_lp_processing,
      :NEW.emergency_text_flag,
      :NEW.requires_delivery_confirmation,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      :NEW.source_type,
      :NEW.department_code,
      :NEW.start_date,
      :NEW.end_date,
      :NEW.payment_method_id,
      :NEW.list_code_flag,
      :NEW.default_source_code_flag,
      :NEW.billing_info_prompt,
      :NEW.billing_info_logic,
      :NEW.marketing_group,
      :NEW.external_call_center_flag,
      :NEW.highlight_description_flag,
      :NEW.discount_allowed_flag,
      :NEW.bin_number_check_flag,
      :NEW.order_source,
      :NEW.yellow_pages_code,
      :NEW.jcpenney_flag,
      :NEW.company_id,
      :NEW.promotion_code,
      :NEW.bonus_promotion_code,
      :NEW.requested_by,
      :NEW.related_source_code,
      :NEW.program_website_url,
      :NEW.comment_text,
      :NEW.partner_bank_id,
      'INS',SYSDATE,
      :NEW.last_used_on_order_date,
      :NEW.mp_redemption_rate_id,
      :NEW.webloyalty_flag,
      :NEW.iotw_flag,
      :NEW.invoice_password,
      :NEW.add_on_free_id,
      :NEW.DISPLAY_SERVICE_FEE_CODE,
      :NEW.DISPLAY_SHIPPING_FEE_CODE,
      :NEW.PRIMARY_BACKUP_RWD_FLAG,
      :NEW.RECPT_LOCATION_TYPE,
      :NEW.RECPT_BUSINESS_NAME,
      :NEW.RECPT_LOCATION_DETAIL,
      :NEW.RECPT_ADDRESS,
      :NEW.RECPT_ZIP_CODE,
      :NEW.RECPT_CITY,
      :NEW.RECPT_STATE_ID,
      :NEW.RECPT_COUNTRY_ID,
      :NEW.RECPT_PHONE,
      :NEW.RECPT_PHONE_EXT,
      :NEW.CUST_FIRST_NAME,
      :NEW.CUST_LAST_NAME,
      :NEW.CUST_DAYTIME_PHONE,
      :NEW.CUST_DAYTIME_PHONE_EXT,
      :NEW.CUST_EVENING_PHONE,
      :NEW.CUST_EVENING_PHONE_EXT,
      :NEW.CUST_ADDRESS,
      :NEW.CUST_ZIP_CODE,
      :NEW.CUST_CITY,
      :NEW.CUST_STATE_ID,
      :NEW.CUST_COUNTRY_ID,
      :NEW.CUST_EMAIL_ADDRESS,
      :NEW.apply_surcharge_code,
      :NEW.surcharge_description,
      :NEW.display_surcharge_flag,
      :NEW.surcharge_amount,
      :NEW.allow_free_shipping_flag,
      :NEW.same_day_upcharge,
      :NEW.display_same_day_upcharge,
      :NEW.morning_delivery_flag,
      :NEW.MORNING_DELIVERY_FREE_SHIPPING,
      :NEW.DELIVERY_FEE_ID,
      :NEW.auto_promotion_engine,
      :NEW.ape_product_catalog,
      :NEW.OSCAR_SELECTION_ENABLED_FLAG,
      :NEW.OSCAR_SCENARIO_GROUP_ID,
      :NEW.calculate_tax_flag,
      :NEW.custom_shipping_carrier,
      :NEW.same_day_upcharge_fs
);
       -- For Project Fresh enqueue JMS events
       --
       SELECT value INTO v_trigger_enabled FROM frp.global_parms WHERE context='SERVICE' AND name='FRESH_TRIGGERS_ENABLED';
       IF (v_trigger_enabled = 'Y') THEN 
         -- Post a message with 1 minute delay
         v_payload := 'FRESH_MSG_PRODUCER|promo|source_master_new|' || :NEW.SOURCE_CODE;
         events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 60, v_stat, v_mess);      
         IF v_stat = 'N' THEN
            v_exception_txt := 'ERROR posting a JMS Fresh message during sourcecode insert:  ' || :NEW.SOURCE_CODE || SUBSTR(v_mess,1,200);
            raise v_exception;
         END IF;
       END IF;

   ELSIF UPDATING  THEN

      INSERT INTO ftd_apps.source_master$ (
      source_master$_id,
      source_code,
      description,
      snh_id,
      price_header_id,
      send_to_scrub,
      fraud_flag,
      enable_lp_processing,
      emergency_text_flag,
      requires_delivery_confirmation,
      created_on,
      created_by,
      updated_on,
      updated_by,
      source_type,
      department_code,
      start_date,
      end_date,
      payment_method_id,
      list_code_flag,
      default_source_code_flag,
      billing_info_prompt,
      billing_info_logic,
      marketing_group,
      external_call_center_flag,
      highlight_description_flag,
      discount_allowed_flag,
      bin_number_check_flag,
      order_source,
      yellow_pages_code,
      jcpenney_flag,
      company_id,
      promotion_code,
      bonus_promotion_code,
      requested_by,
      related_source_code,
      program_website_url,
      comment_text,
      partner_bank_id,
      operation$, timestamp$,
      last_used_on_order_date,
      mp_redemption_rate_id,
      webloyalty_flag,
      iotw_flag,
      invoice_password,
      add_on_free_id,
      DISPLAY_SERVICE_FEE_CODE,
      DISPLAY_SHIPPING_FEE_CODE,
      PRIMARY_BACKUP_RWD_FLAG,
      RECPT_LOCATION_TYPE,
      RECPT_BUSINESS_NAME,
      RECPT_LOCATION_DETAIL,
      RECPT_ADDRESS,
      RECPT_ZIP_CODE,
      RECPT_CITY,
      RECPT_STATE_ID,
      RECPT_COUNTRY_ID,
      RECPT_PHONE,
      RECPT_PHONE_EXT,
      CUST_FIRST_NAME,
      CUST_LAST_NAME,
      CUST_DAYTIME_PHONE,
      CUST_DAYTIME_PHONE_EXT,
      CUST_EVENING_PHONE,
      CUST_EVENING_PHONE_EXT,
      CUST_ADDRESS,
      CUST_ZIP_CODE,
      CUST_CITY,
      CUST_STATE_ID,
      CUST_COUNTRY_ID,
      CUST_EMAIL_ADDRESS,
      apply_surcharge_code,
      surcharge_description,
      display_surcharge_flag,
      surcharge_amount,
      allow_free_shipping_flag,
      same_day_upcharge,
      display_same_day_upcharge,
      morning_delivery_flag,
      MORNING_DELIVERY_FREE_SHIPPING,
      DELIVERY_FEE_ID,
      auto_promotion_engine,
      ape_product_catalog,
      OSCAR_SELECTION_ENABLED_FLAG,
      OSCAR_SCENARIO_GROUP_ID,
      calculate_tax_flag,
      custom_shipping_carrier,
      same_day_upcharge_fs
      ) VALUES (
      source_master$_sq.nextval,
      :OLD.source_code,
      :OLD.description,
      :OLD.snh_id,
      :OLD.price_header_id,
      :OLD.send_to_scrub,
      :OLD.fraud_flag,
      :OLD.enable_lp_processing,
      :OLD.emergency_text_flag,
      :OLD.requires_delivery_confirmation,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      :OLD.source_type,
      :OLD.department_code,
      :OLD.start_date,
      :OLD.end_date,
      :OLD.payment_method_id,
      :OLD.list_code_flag,
      :OLD.default_source_code_flag,
      :OLD.billing_info_prompt,
      :OLD.billing_info_logic,
      :OLD.marketing_group,
      :OLD.external_call_center_flag,
      :OLD.highlight_description_flag,
      :OLD.discount_allowed_flag,
      :OLD.bin_number_check_flag,
      :OLD.order_source,
      :OLD.yellow_pages_code,
      :OLD.jcpenney_flag,
      :OLD.company_id,
      :OLD.promotion_code,
      :OLD.bonus_promotion_code,
      :OLD.requested_by,
      :OLD.related_source_code,
      :OLD.program_website_url,
      :OLD.comment_text,
      :OLD.partner_bank_id,
      'UPD_OLD',SYSDATE,
      :OLD.last_used_on_order_date,
      :OLD.mp_redemption_rate_id,
      :OLD.webloyalty_flag,
      :OLD.iotw_flag,
      :OLD.invoice_password,
      :OLD.add_on_free_id,
      :OLD.DISPLAY_SERVICE_FEE_CODE,
      :OLD.DISPLAY_SHIPPING_FEE_CODE,
      :OLD.PRIMARY_BACKUP_RWD_FLAG,
      :OLD.RECPT_LOCATION_TYPE,
      :OLD.RECPT_BUSINESS_NAME,
      :OLD.RECPT_LOCATION_DETAIL,
      :OLD.RECPT_ADDRESS,
      :OLD.RECPT_ZIP_CODE,
      :OLD.RECPT_CITY,
      :OLD.RECPT_STATE_ID,
      :OLD.RECPT_COUNTRY_ID,
      :OLD.RECPT_PHONE,
      :OLD.RECPT_PHONE_EXT,
      :OLD.CUST_FIRST_NAME,
      :OLD.CUST_LAST_NAME,
      :OLD.CUST_DAYTIME_PHONE,
      :OLD.CUST_DAYTIME_PHONE_EXT,
      :OLD.CUST_EVENING_PHONE,
      :OLD.CUST_EVENING_PHONE_EXT,
      :OLD.CUST_ADDRESS,
      :OLD.CUST_ZIP_CODE,
      :OLD.CUST_CITY,
      :OLD.CUST_STATE_ID,
      :OLD.CUST_COUNTRY_ID,
      :OLD.CUST_EMAIL_ADDRESS,
      :OLD.apply_surcharge_code,
      :OLD.surcharge_description,
      :OLD.display_surcharge_flag,
      :OLD.surcharge_amount,
      :OLD.allow_free_shipping_flag,
      :OLD.same_day_upcharge,
      :OLD.display_same_day_upcharge,
      :OLD.morning_delivery_flag,
      :OLD.MORNING_DELIVERY_FREE_SHIPPING,
      :OLD.DELIVERY_FEE_ID,
      :OLD.auto_promotion_engine,
      :OLD.ape_product_catalog,
      :OLD.OSCAR_SELECTION_ENABLED_FLAG,
      :OLD.OSCAR_SCENARIO_GROUP_ID,
      :OLD.calculate_tax_flag,
      :OLD.custom_shipping_carrier,
      :OLD.same_day_upcharge_fs
);

      INSERT INTO ftd_apps.source_master$ (
      source_master$_id,
      source_code,
      description,
      snh_id,
      price_header_id,
      send_to_scrub,
      fraud_flag,
      enable_lp_processing,
      emergency_text_flag,
      requires_delivery_confirmation,
      created_on,
      created_by,
      updated_on,
      updated_by,
      source_type,
      department_code,
      start_date,
      end_date,
      payment_method_id,
      list_code_flag,
      default_source_code_flag,
      billing_info_prompt,
      billing_info_logic,
      marketing_group,
      external_call_center_flag,
      highlight_description_flag,
      discount_allowed_flag,
      bin_number_check_flag,
      order_source,
      yellow_pages_code,
      jcpenney_flag,
      company_id,
      promotion_code,
      bonus_promotion_code,
      requested_by,
      related_source_code,
      program_website_url,
      comment_text,
      partner_bank_id,
      operation$, timestamp$,
      last_used_on_order_date,
      mp_redemption_rate_id,
      webloyalty_flag,
      iotw_flag,
      invoice_password,
      add_on_free_id,
      DISPLAY_SERVICE_FEE_CODE,
      DISPLAY_SHIPPING_FEE_CODE,
      PRIMARY_BACKUP_RWD_FLAG,
      RECPT_LOCATION_TYPE,
      RECPT_BUSINESS_NAME,
      RECPT_LOCATION_DETAIL,
      RECPT_ADDRESS,
      RECPT_ZIP_CODE,
      RECPT_CITY,
      RECPT_STATE_ID,
      RECPT_COUNTRY_ID,
      RECPT_PHONE,
      RECPT_PHONE_EXT,
      CUST_FIRST_NAME,
      CUST_LAST_NAME,
      CUST_DAYTIME_PHONE,
      CUST_DAYTIME_PHONE_EXT,
      CUST_EVENING_PHONE,
      CUST_EVENING_PHONE_EXT,
      CUST_ADDRESS,
      CUST_ZIP_CODE,
      CUST_CITY,
      CUST_STATE_ID,
      CUST_COUNTRY_ID,
      CUST_EMAIL_ADDRESS,
      apply_surcharge_code,
      surcharge_description,
      display_surcharge_flag,
      surcharge_amount,
      allow_free_shipping_flag,
      same_day_upcharge,
      display_same_day_upcharge,
      morning_delivery_flag,
      MORNING_DELIVERY_FREE_SHIPPING,
      DELIVERY_FEE_ID,
      auto_promotion_engine,
      ape_product_catalog,
      OSCAR_SELECTION_ENABLED_FLAG,
      OSCAR_SCENARIO_GROUP_ID,
      calculate_tax_flag,
      custom_shipping_carrier,
      same_day_upcharge_fs
      ) VALUES (
      source_master$_sq.nextval,
      :NEW.source_code,
      :NEW.description,
      :NEW.snh_id,
      :NEW.price_header_id,
      :NEW.send_to_scrub,
      :NEW.fraud_flag,
      :NEW.enable_lp_processing,
      :NEW.emergency_text_flag,
      :NEW.requires_delivery_confirmation,
      :NEW.created_on,
      :NEW.created_by,
      :NEW.updated_on,
      :NEW.updated_by,
      :NEW.source_type,
      :NEW.department_code,
      :NEW.start_date,
      :NEW.end_date,
      :NEW.payment_method_id,
      :NEW.list_code_flag,
      :NEW.default_source_code_flag,
      :NEW.billing_info_prompt,
      :NEW.billing_info_logic,
      :NEW.marketing_group,
      :NEW.external_call_center_flag,
      :NEW.highlight_description_flag,
      :NEW.discount_allowed_flag,
      :NEW.bin_number_check_flag,
      :NEW.order_source,
      :NEW.yellow_pages_code,
      :NEW.jcpenney_flag,
      :NEW.company_id,
      :NEW.promotion_code,
      :NEW.bonus_promotion_code,
      :NEW.requested_by,
      :NEW.related_source_code,
      :NEW.program_website_url,
      :NEW.comment_text,
      :NEW.partner_bank_id,
      'UPD_NEW',SYSDATE,
      :NEW.last_used_on_order_date,
      :NEW.mp_redemption_rate_id,
      :NEW.webloyalty_flag,
      :NEW.iotw_flag,
      :NEW.invoice_password,
      :NEW.add_on_free_id,
      :NEW.DISPLAY_SERVICE_FEE_CODE,
      :NEW.DISPLAY_SHIPPING_FEE_CODE,
      :NEW.PRIMARY_BACKUP_RWD_FLAG,
      :NEW.RECPT_LOCATION_TYPE,
      :NEW.RECPT_BUSINESS_NAME,
      :NEW.RECPT_LOCATION_DETAIL,
      :NEW.RECPT_ADDRESS,
      :NEW.RECPT_ZIP_CODE,
      :NEW.RECPT_CITY,
      :NEW.RECPT_STATE_ID,
      :NEW.RECPT_COUNTRY_ID,
      :NEW.RECPT_PHONE,
      :NEW.RECPT_PHONE_EXT,
      :NEW.CUST_FIRST_NAME,
      :NEW.CUST_LAST_NAME,
      :NEW.CUST_DAYTIME_PHONE,
      :NEW.CUST_DAYTIME_PHONE_EXT,
      :NEW.CUST_EVENING_PHONE,
      :NEW.CUST_EVENING_PHONE_EXT,
      :NEW.CUST_ADDRESS,
      :NEW.CUST_ZIP_CODE,
      :NEW.CUST_CITY,
      :NEW.CUST_STATE_ID,
      :NEW.CUST_COUNTRY_ID,
      :NEW.CUST_EMAIL_ADDRESS,
      :NEW.apply_surcharge_code,
      :NEW.surcharge_description,
      :NEW.display_surcharge_flag,
      :NEW.surcharge_amount,
      :NEW.allow_free_shipping_flag,
      :NEW.same_day_upcharge,
      :NEW.display_same_day_upcharge,
      :NEW.morning_delivery_flag,
      :NEW.MORNING_DELIVERY_FREE_SHIPPING,
      :NEW.DELIVERY_FEE_ID,
      :NEW.auto_promotion_engine,
      :NEW.ape_product_catalog,
      :NEW.OSCAR_SELECTION_ENABLED_FLAG,
      :NEW.OSCAR_SCENARIO_GROUP_ID,
      :NEW.calculate_tax_flag,
      :NEW.custom_shipping_carrier,
      :NEW.same_day_upcharge_fs
);

       -- For Project Fresh enqueue JMS events for pricing or start/end date changes
       --
       SELECT value INTO v_trigger_enabled FROM frp.global_parms WHERE context='SERVICE' AND name='FRESH_TRIGGERS_ENABLED';
       IF (v_trigger_enabled = 'Y') THEN 
          IF (:NEW.PRICE_HEADER_ID <> :OLD.PRICE_HEADER_ID OR 
              :NEW.START_DATE <> :OLD.START_DATE OR
              (to_char(:NEW.END_DATE, 'YYYY-MM-DD HH24:MI:SS') || 'x') <> (to_char(:OLD.END_DATE, 'YYYY-MM-DD HH24:MI:SS') || 'x')
              ) 
          THEN
            -- Post a message with 1 minute delay
            v_payload := 'FRESH_MSG_PRODUCER|promo|source_master|' || :OLD.SOURCE_CODE;
            events.post_a_message_flex('OJMS.PDB', NULL, v_payload, 60, v_stat, v_mess);      
            IF v_stat = 'N' THEN
               v_exception_txt := 'ERROR posting a JMS Fresh message during sourcecode update:  ' || :OLD.SOURCE_CODE || SUBSTR(v_mess,1,200);
               raise v_exception;
            END IF;
          END IF; 
       END IF;

   ELSIF DELETING  THEN

      INSERT INTO ftd_apps.source_master$ (
      source_master$_id,
      source_code,
      description,
      snh_id,
      price_header_id,
      send_to_scrub,
      fraud_flag,
      enable_lp_processing,
      emergency_text_flag,
      requires_delivery_confirmation,
      created_on,
      created_by,
      updated_on,
      updated_by,
      source_type,
      department_code,
      start_date,
      end_date,
      payment_method_id,
      list_code_flag,
      default_source_code_flag,
      billing_info_prompt,
      billing_info_logic,
      marketing_group,
      external_call_center_flag,
      highlight_description_flag,
      discount_allowed_flag,
      bin_number_check_flag,
      order_source,
      yellow_pages_code,
      jcpenney_flag,
      company_id,
      promotion_code,
      bonus_promotion_code,
      requested_by,
      related_source_code,
      program_website_url,
      comment_text,
      partner_bank_id,
      operation$, timestamp$,
      last_used_on_order_date,
      mp_redemption_rate_id,
      webloyalty_flag,
      iotw_flag,
      invoice_password,
      add_on_free_id,
      DISPLAY_SERVICE_FEE_CODE,
      DISPLAY_SHIPPING_FEE_CODE,
      PRIMARY_BACKUP_RWD_FLAG,
      RECPT_LOCATION_TYPE,
      RECPT_BUSINESS_NAME,
      RECPT_LOCATION_DETAIL,
      RECPT_ADDRESS,
      RECPT_ZIP_CODE,
      RECPT_CITY,
      RECPT_STATE_ID,
      RECPT_COUNTRY_ID,
      RECPT_PHONE,
      RECPT_PHONE_EXT,
      CUST_FIRST_NAME,
      CUST_LAST_NAME,
      CUST_DAYTIME_PHONE,
      CUST_DAYTIME_PHONE_EXT,
      CUST_EVENING_PHONE,
      CUST_EVENING_PHONE_EXT,
      CUST_ADDRESS,
      CUST_ZIP_CODE,
      CUST_CITY,
      CUST_STATE_ID,
      CUST_COUNTRY_ID,
      CUST_EMAIL_ADDRESS,
      apply_surcharge_code,
      surcharge_description,
      display_surcharge_flag,
      surcharge_amount,
      allow_free_shipping_flag,
      same_day_upcharge,
      display_same_day_upcharge,
      morning_delivery_flag,
      MORNING_DELIVERY_FREE_SHIPPING,
      DELIVERY_FEE_ID,
      auto_promotion_engine,
      ape_product_catalog,
      OSCAR_SELECTION_ENABLED_FLAG,
      OSCAR_SCENARIO_GROUP_ID,
      calculate_tax_flag,
      custom_shipping_carrier,
      same_day_upcharge_fs      
      ) VALUES (
      source_master$_sq.nextval,
      :OLD.source_code,
      :OLD.description,
      :OLD.snh_id,
      :OLD.price_header_id,
      :OLD.send_to_scrub,
      :OLD.fraud_flag,
      :OLD.enable_lp_processing,
      :OLD.emergency_text_flag,
      :OLD.requires_delivery_confirmation,
      :OLD.created_on,
      :OLD.created_by,
      :OLD.updated_on,
      :OLD.updated_by,
      :OLD.source_type,
      :OLD.department_code,
      :OLD.start_date,
      :OLD.end_date,
      :OLD.payment_method_id,
      :OLD.list_code_flag,
      :OLD.default_source_code_flag,
      :OLD.billing_info_prompt,
      :OLD.billing_info_logic,
      :OLD.marketing_group,
      :OLD.external_call_center_flag,
      :OLD.highlight_description_flag,
      :OLD.discount_allowed_flag,
      :OLD.bin_number_check_flag,
      :OLD.order_source,
      :OLD.yellow_pages_code,
      :OLD.jcpenney_flag,
      :OLD.company_id,
      :OLD.promotion_code,
      :OLD.bonus_promotion_code,
      :OLD.requested_by,
      :OLD.related_source_code,
      :OLD.program_website_url,
      :OLD.comment_text,
      :OLD.partner_bank_id,
      'DEL',SYSDATE,
      :OLD.last_used_on_order_date,
      :OLD.mp_redemption_rate_id,
      :OLD.webloyalty_flag,
      :OLD.iotw_flag,
      :OLD.invoice_password,
      :OLD.add_on_free_id,
      :OLD.DISPLAY_SERVICE_FEE_CODE,
      :OLD.DISPLAY_SHIPPING_FEE_CODE,
      :OLD.PRIMARY_BACKUP_RWD_FLAG,
      :OLD.RECPT_LOCATION_TYPE,
      :OLD.RECPT_BUSINESS_NAME,
      :OLD.RECPT_LOCATION_DETAIL,
      :OLD.RECPT_ADDRESS,
      :OLD.RECPT_ZIP_CODE,
      :OLD.RECPT_CITY,
      :OLD.RECPT_STATE_ID,
      :OLD.RECPT_COUNTRY_ID,
      :OLD.RECPT_PHONE,
      :OLD.RECPT_PHONE_EXT,
      :OLD.CUST_FIRST_NAME,
      :OLD.CUST_LAST_NAME,
      :OLD.CUST_DAYTIME_PHONE,
      :OLD.CUST_DAYTIME_PHONE_EXT,
      :OLD.CUST_EVENING_PHONE,
      :OLD.CUST_EVENING_PHONE_EXT,
      :OLD.CUST_ADDRESS,
      :OLD.CUST_ZIP_CODE,
      :OLD.CUST_CITY,
      :OLD.CUST_STATE_ID,
      :OLD.CUST_COUNTRY_ID,
      :OLD.CUST_EMAIL_ADDRESS,
      :OLD.apply_surcharge_code,
      :OLD.surcharge_description,
      :OLD.display_surcharge_flag,
      :OLD.surcharge_amount,
      :OLD.allow_free_shipping_flag,
      :OLD.same_day_upcharge,
      :OLD.display_same_day_upcharge,
      :OLD.morning_delivery_flag,
      :OLD.MORNING_DELIVERY_FREE_SHIPPING,
      :OLD.DELIVERY_FEE_ID,
      :OLD.auto_promotion_engine,
      :OLD.ape_product_catalog,
      :OLD.OSCAR_SELECTION_ENABLED_FLAG,
      :OLD.OSCAR_SCENARIO_GROUP_ID,
      :OLD.calculate_tax_flag,
      :OLD.custom_shipping_carrier,
      :OLD.same_day_upcharge_fs      
);

   END IF;
  END IF;
END;
/
