CREATE OR REPLACE
FUNCTION ftd_apps.OE_GET_FUNCTIONS_BY_ROLE (inRoleId IN VARCHAR2)
  RETURN types.ref_cursor
--==============================================================================
--
-- Name:    OE_GET_FUNCTIONS_BY_ROLE
-- Type:    Function
-- Syntax:  OE_GET_FUNCTIONS_BY_ROLE(inRoleId IN VARCHAR2)
-- Returns: ref_cursor for
--          functionId            VARCHAR2(20)
--          description           VARCHAR2(50)
--          createdBy             VARCHAR2(10)
--          createdDate           DATE
--          lastUpdateUser        VARCHAR2(10)
--          lastUpdateDate        DATE
--          roleId                VARCHAR2(10)
--
-- Description:   Queries the functions table and returns a ref cursor for row.
--
--===========================================================

AS
   roleFunction_cursor types.ref_cursor;
BEGIN
    OPEN roleFunction_cursor FOR
        SELECT fu.FUNCTION_ID       as "functionId",
               FUNCTION_DESCRIPTION as "description",
               CREATED_BY           as "createdBy",
               CREATED_DATE         as "createdDate",
               LAST_UPDATE_USER     as "lastUpdateUser",
               LAST_UPDATE_DATE     as "lastUpdateDate",
               rf.ROLE_ID           as "roleId"
           FROM functions fu, role_functions rf
          WHERE rf.role_id = UPPER(inRoleId)
            AND rf.function_id = fu.function_id;

    RETURN roleFunction_cursor;
END
;
.
/
