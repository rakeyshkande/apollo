CREATE OR REPLACE
FUNCTION ftd_apps.SP_GET_UPSELL_DETAIL_LIST (skuIn IN varchar2)
    RETURN types.ref_cursor
--==============================================================================
--
-- Name:    SP_GET_UPSELL_DETAIL_LIST
-- Type:    Function
-- Syntax:  SP_GET_UPSELL_DETAIL_LIST (skuIn in VARCHAR2 )
-- Returns: ref_cursor for--
-- Description:   Queries the UPSELL_DETAIL PRODUCT_MASTER table by SKU AND PRODUCT_ID
--
--=========================================================

AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
SELECT U.UPSELL_DETAIL_ID,
       U.UPSELL_DETAIL_SEQUENCE,
       U.UPSELL_DETAIL_NAME,
       P.PRODUCT_TYPE,
       P.STANDARD_PRICE,
       P.STATUS,
       P.NOVATOR_ID,
       P.SENT_TO_NOVATOR_PROD,
       P.SENT_TO_NOVATOR_CONTENT,
       P.SENT_TO_NOVATOR_UAT,
       P.SENT_TO_NOVATOR_TEST,
       U.GBB_NAME_OVERRIDE_FLAG,
       U.GBB_NAME_OVERRIDE_TXT,
       U.GBB_PRICE_OVERRIDE_FLAG,
       U.GBB_PRICE_OVERRIDE_TXT,
       U.GBB_UPSELL_DETAIL_SEQUENCE_NUM,
       U.DEFAULT_SKU_FLAG
FROM UPSELL_DETAIL U, PRODUCT_MASTER P
WHERE U.UPSELL_MASTER_ID = skuIn AND
U.UPSELL_DETAIL_ID = P.PRODUCT_ID
ORDER BY U.UPSELL_DETAIL_SEQUENCE;

    RETURN cur_cursor;
END;
.
/
