CREATE OR REPLACE
PROCEDURE ftd_apps.SP_UPDATE_PRODUCT_COLORS (
 productId in varchar2,
 productColor in varchar2,
 orderBy in number
)
as
--==============================================================================
--
-- Name:    SP_UPDATE_PRODUCT_COLORS
-- Type:    Procedure
-- Syntax:  SP_UPDATE_PRODUCT_COLORS ( productId in varchar2,
--                                     productColor in varchar2 )
--
-- Description:   Attempts to insert a row into PRODUCT_COLORS.
--                If the product/color combination already exists, ignores.
--
--==============================================================================
begin

  -- Attempt to insert into PRODUCT_COLORS
  INSERT INTO PRODUCT_COLORS
      ( PRODUCT_ID,
				PRODUCT_COLOR,
				ORDER_BY
		  )
  VALUES
		  ( productId,
				productColor,
				orderBy
		  );

EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
		  rollback;
end
;
.
/
