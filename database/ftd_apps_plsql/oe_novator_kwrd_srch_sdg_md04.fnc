CREATE OR REPLACE FUNCTION FTD_APPS.oe_novator_kwrd_srch_sdg_md04 (
novatorIdList         IN VARCHAR2,
inPricePointId        IN NUMBER,
sourceCode            IN VARCHAR2,
domesticIntlFlag      IN VARCHAR2,
inZipCode             IN VARCHAR2,
deliveryEnd           IN VARCHAR2,
inCountryId           IN VARCHAR2,
scriptCode            IN VARCHAR2
)
  RETURN Types.ref_cursor
--==============================================================================
--
-- Name:    oe_novator_kwrd_srch_sdg_md04
-- Type:    Function
-- Syntax:  oe_novator_kwrd_srch_sdg_md04(novatorIdList IN VARCHAR2,
--                             sourceCode IN VARCHAR2,
--                             domesticIntlFlag IN VARCHAR2,
--                             inZipCode IN VARCHAR2)
-- Returns: ref_cursor for
--          PRODUCT_ID             VARCHAR2(10)
--          STANDARD_PRICE         NUMBER(8.2)
--
-- Description:   Queries PRODUCT_MASTER by PR.PRODUCT_ID
--                returning a ref cursor for resulting row.
--
--===================================================
AS
    cur_cursor Types.ref_cursor;

    pricingCode         SOURCE.PRICING_CODE%TYPE := NULL;
    zipGnaddFlag        CSZ_AVAIL.GNADD_FLAG%TYPE := NULL;
    zipFloralFlag       VARCHAR2(1) := 'Y';
      timeZone            STATE_MASTER.TIME_ZONE%TYPE := NULL;
    stateId             ZIP_CODE.STATE_ID%TYPE := NULL;
    currentDate         VARCHAR2(10) := TO_CHAR(SYSDATE, 'mm/dd/yyyy');
    addonDays           NUMBER;
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
    cszZip              VARCHAR2(6);
    cleanZip            VARCHAR2(6) := OE_CLEANUP_ALPHANUM_STRING(UPPER(inZipCode), 'Y');
    mySelect            VARCHAR2(16000);
    block_ts            VARCHAR2(16) := TO_CHAR(SYSDATE, 'mm/dd/yyyy HH24:MI');
    gnaddLevel          VARCHAR2(2);
BEGIN

    -- Select the pricing code for this source code, for the below query
    BEGIN
        SELECT PRICING_CODE
        INTO pricingCode
        FROM SOURCE
        WHERE SOURCE_CODE = UPPER(sourceCode);
    EXCEPTION WHEN OTHERS THEN
        NULL;
    END;

    -- If no zip provided, assume zip GNADD is off and floral available wherever this is going
    IF ( cleanZip IS NULL )
    THEN
        zipGnaddFlag := 'N';
        zipFloralFlag := 'Y';

    ELSE
        -- Determine if the input zip code is Canadian
        IF ( TRANSLATE(cleanZip, alphaChars || numChars, alphaSub || numSub) LIKE '@#@%' )
        THEN
            -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
            cszZip := SUBSTR(cleanZip,1,3);
        ELSE
            cszZip := cleanZip;
        END IF;

        -- Select the GNADD flag for this zip, if available
        BEGIN
            -- Assume NULL GNADD flag indicates GNADD is off
            SELECT NVL(GNADD_FLAG, 'N')
            INTO zipGnaddFlag
            FROM CSZ_AVAIL
            WHERE CSZ_ZIP_CODE = cszZip;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            -- If no row found on CSZ_AVAIL, then no floral for this zip
            zipFloralFlag := 'N';
        END;
    END IF;

    -- Select the time zone for this zip, if available
    BEGIN
        SELECT DISTINCT SM.TIME_ZONE, ZC.STATE_ID
        INTO timeZone, stateId
        FROM ZIP_CODE zc, STATE_MASTER sm
        WHERE zc.ZIP_CODE_ID = UPPER(cleanZip)
          AND ZC.STATE_ID = SM.STATE_MASTER_ID;
    EXCEPTION WHEN OTHERS THEN
        timeZone := '5';
    END;

    -- Select add on days for this country from country master
    BEGIN
        SELECT NVL(ADD_ON_DAYS, 1)
        INTO addonDays
        FROM COUNTRY_MASTER
        WHERE COUNTRY_ID = inCountryId;
    EXCEPTION WHEN OTHERS THEN
        -- Default to 1 day if not known
        addonDays := 1;
    END;
    gnaddLevel := get_gnadd_level();

    -- Open a query ref cursor to return product list results
    mySelect :=
        'SELECT pm.PRODUCT_ID as "productID", ' ||
               'pm.NOVATOR_ID as "novatorID", ' ||
               'pm.STANDARD_PRICE as "standardPrice" ' ||

        'FROM PRODUCT_MASTER pm, PRODUCT_MASTER_VW pmv, SHIPPING_KEYS sk, ' ||
              'SOURCE sc, SNH snh, ' ||
              'PRICE_HEADER_DETAILS phd_std, ' ||
              'PRICE_HEADER_DETAILS phd_dlx, ' ||
              'PRICE_HEADER_DETAILS phd_prm, ' ||
              'PRODUCT_SHIP_DATES psd, ' ||
              'PRODUCT_SHIPMETHODS_CNT_VW psc, ' ||
              'PROD_SHIP_BLOCK_CNT_VW psbc, ' ||
              'PROD_DELIV_BLOCK_CNT_VW pdbc, ' ||
              'PRODUCT_COLORS_CNT_VW pcc, ' ||
              'CODIFIED_PRODUCTS cp, CSZ_PRODUCTS cszp, ' ||
              'PRODUCT_COMPANY_XREF pcx, ' ||
              -- Inline view computes count of goto florists for supplied zip
              '( SELECT ' ||

                -- Count florists with Sunday delivery from florist_hours table
                'COUNT(DECODE(fb.BLOCK_START_DATE, NULL, ' ||
                          'DECODE(florist_query_pkg.is_florist_open_sunday(fz.FLORIST_ID),''Y'',1))) as SUNDAY_COUNT, ' ||
                

                -- Count GOTO florists, excluding blocked florists ONLY
                --   if GNADD level 2 is in effect
                'COUNT(DECODE('''||gnaddLevel||''', 2, ' ||
                          'DECODE(fb.BLOCK_START_DATE, NULL, ' ||
                              'DECODE(fm.SUPER_FLORIST_FLAG,''Y'',1)), ' ||
                          'DECODE(fm.SUPER_FLORIST_FLAG,''Y'',1))) as GOTO_COUNT ' ||

                'FROM FLORIST_ZIPS fz, FLORIST_MASTER fm, ' ||
                      'FLORIST_BLOCKS fb ' ||
                'WHERE fz.ZIP_CODE = ''' || cleanZip || ''' ' ||
                'AND fz.FLORIST_ID = fm.FLORIST_ID ' ||
                'AND fz.FLORIST_ID = fb.FLORIST_ID (+) ' ||
                'AND TO_DATE(''' || block_ts || ''', ''mm/dd/yyyy HH24:MI'') >= fb.BLOCK_START_DATE (+) ' ||
                'AND TO_DATE(''' || block_ts || ''', ''mm/dd/yyyy HH24:MI'') <= fb.BLOCK_END_DATE (+) ' ||
                -- cr 11/2004 - changes for florist data model
                'and fm.status <> ''Inactive'' ' ||
                'and fz.block_start_date is null ' ||
              ') gc ' ||

        -- For product detail, match on product ID or Novator ID
        --   also match subcodes for a master product if present
        'WHERE pmv.product_id IN (' || novatorIdList || ') ' ||

                'AND pmv.product_detail_id = pm.product_id ' ||

        -- Only available products should be returned
        'AND pm.status = ''A'' ' ||

        -- If input country is US or null, exclude international products
        'AND (''' || inCountryId || ''' IS NULL OR ''' || inCountryId || ''' != ''US'' ' ||
          'OR  pm.delivery_type != ''I'' ) ' ||

        -- If input zip code is blocked and GNADD level 0 in effect,
        -- or no floral deliveries to this zip code
        -- then list only Specialty Gift , Same Day Gift and Fresh Cuts products
        'AND (( (''' || zipGnaddFlag || ''' != ''Y'' OR '''||gnaddLevel||''' != 0) ' ||
          'AND ''' || zipFloralFlag || ''' = ''Y'') ' ||
          'OR  pm.PRODUCT_TYPE IN (''SPEGFT'',''FRECUT'',''SDG'',''SDFC'') ) ' ||

        -- Determine if this product is considered codified for Order Entry
        'AND pm.product_id = cp.product_id (+) ' ||
        'AND ''Y'' = cp.check_oe_flag (+) ' ||

        -- GNADD level 3 test
        -- At level 3 floral products show GNADD delivery date
        -- Codified products are not available
        -- Special Codified product show GNADD delivery date
        'AND ('''||gnaddLevel||''' != 3 OR cp.check_oe_flag IS NULL OR NVL(cp.codified_special, ''N'') = ''Y'')' ||

        -- If input zip code is blocked and GNADD level 1 in effect
        --   then exclude ALL codified products
        'AND (''' || zipGnaddFlag || ''' != ''Y'' OR '''||gnaddLevel||''' != 1 ' ||
          'OR  cp.check_oe_flag IS NULL OR NVL(cp.codified_special, ''N'') = ''Y'' OR pm.ship_method_carrier = ''Y'')' ||


        -- Check product availability for codified products by zip.  Available
        --   flag of 'N' indicates codified product not available for zip.
        -- if the product is codified special then only exclude it if GNADD is off.
        -- If the product is SDG or SDFC then it must be available by a florist
        -- if outside the US
        --'AND pm.product_id = cszp.product_id (+) ' ||
        --'AND ''' || cszZip || ''' = cszp.ZIP_CODE (+) ' ||
        --'AND ( cp.check_oe_flag IS NULL OR ''' || cleanZip || ''' IS NULL ' ||
        --      'OR (pm.PRODUCT_TYPE IN (''SDG'',''SDFC'') AND ( ''' || inCountryId || ''' IS NULL OR ''' ||
        --           inCountryId || ''' = ''US'') AND (NVL(cszp.available_flag, ''N'') != ''N'')) ' ||
        --      'OR (NVL(cp.codified_special, ''N'') = ''Y'' AND (NVL(cszp.available_flag, ''N'') != ''N'') ' ||
        --          'OR ''' || zipGnaddFlag || ''' = ''Y'') ' ||
        --      'OR NVL(cszp.available_flag, ''N'') != ''N'') ' ||

        -- Check product availability for codified products by zip.  Available
        --   flag of 'N' indicates codified product not available for zip.
        'AND pm.product_id = cszp.product_id (+) ' ||
        'AND ''' || cszZip || ''' = cszp.zip_code (+) ' ||
        'AND ( cp.check_oe_flag IS NULL OR ''' || cleanZip || ''' IS NULL OR pm.PRODUCT_TYPE IN (''SDG'',''SDFC'') ' ||
          ' OR (NVL(cp.codified_special, ''N'') = ''Y'' AND (NVL(cszp.available_flag, ''N'') != ''N'' OR ''' || zipGnaddFlag || ''' = ''Y'')) OR  NVL(cszp.available_flag, ''N'') != ''N'' ) ' ||


        -- If the recipient country is non-US, exclude all but floral products.
        'AND ( ''' || inCountryId || ''' IS NULL ' ||
          'OR  ''' || inCountryId || ''' = ''US'' ' ||
          'OR pm.ship_method_florist = ''Y'' ) ' ||

        -- Screen out products listed as excluded by state for the supplied zipcode
        'AND NOT EXISTS ( ' ||
            'SELECT 1 ' ||
            'FROM ZIP_CODE zc, PRODUCT_EXCLUDED_STATES pes ' ||
            'WHERE zc.zip_code_id = UPPER(''' || cleanZip || ''') ' ||
            'AND pes.product_id = pm.product_id ' ||
            'AND pes.excluded_state = zc.state_id ' ||
        ') ' ||

        -- If the script code is JCPenny, exclude B and C JCP Categories.
        'AND ( ''' || scriptCode || ''' != ''JP'' ' ||
          'OR pm.JCP_CATEGORY = ''A'' ) ' ||

        -- If the recipient state is Alaska or Hawaii, exclude dropship products that
        --   do NOT have second day shipping.  DO NOT exclude floral.
        'AND  ( ''' || stateId || ''' IS NULL ' ||
        'OR ''' || stateId || ''' NOT IN (''AK'',''HI'') ' ||
                -- Check for SDG with florist delivery.  If the SDG product has florist delivery then
                -- we should not exclude it from the list
        'OR    (pm.product_type IN (''SDG'',''SDFC'') AND NVL(cszp.available_flag, ''N'') != ''N'')' ||
        'OR    (pm.ship_method_florist = ''Y'' AND pm.ship_method_carrier != ''Y'')  ' ||
        'OR     EXISTS ( ' ||
            'SELECT 1 FROM PRODUCT_SHIP_METHODS psm ' ||
            'WHERE psm.product_id = pm.product_id ' ||
            'AND psm.ship_method_id = ''2F'' ' ||
                        'AND pm.PRODUCT_TYPE NOT IN (''FRECUT'') ' ||
                  ') ' ||
                ') ' ||

                -- If the product has exception start and end date, and delivery end date was specified,
                --   exclude if it cannot be delivered sometime prior to delivery end date
                'AND ( ( pm.exception_start_date IS NULL OR pm.exception_end_date IS NULL OR ' ||
                        'pm.exception_code IS NULL OR ''' || deliveryEnd || ''' IS NULL ) ' ||
                  'OR  ( pm.exception_code = ''U'' AND ( TO_DATE(''' || currentDate || ''', ''mm/dd/yyyy'') < pm.exception_start_date ' ||
                    'OR TO_DATE(''' || deliveryEnd || ''', ''mm/dd/yyyy'') > pm.exception_end_date ) ) ' ||
                  'OR  ( pm.exception_code = ''A'' AND ' ||
                      '( ( pm.exception_start_date >= TO_DATE(''' || currentDate || ''', ''mm/dd/yyyy'') ' ||
                      ' AND pm.exception_start_date <= TO_DATE(''' || deliveryEnd || ''', ''mm/dd/yyyy'') ) OR ' ||
                      '( pm.exception_end_date >= TO_DATE(''' || currentDate || ''', ''mm/dd/yyyy'') ' ||
                      ' AND pm.exception_end_date <= TO_DATE(''' || deliveryEnd || ''', ''mm/dd/yyyy'') ) ' ||
                ') ) ) ' ||

        -- If the product is dropshipped, determine the shipper on SHIPPING_KEYS
        'AND DECODE(pm.ship_method_carrier, ''Y'', pm.shipping_key, NULL) = sk.shipping_key_id (+) ' ||
        'AND pm.product_id = psc.product_id (+) ' ||
        'AND pm.product_id = psbc.product_id (+) ' ||
        'AND pm.product_id = pdbc.product_id (+) ' ||
        'AND pm.product_id = pcc.product_id (+) ' ||

        -- Use the source code to get shipping and handling for florist delivered
        'AND sc.source_code = UPPER(''' || sourceCode || ''') ' ||
        'AND sc.shipping_code = snh.snh_id (+) ' ||

        -- Find out product ship days if specified
        'AND pm.product_id = psd.product_id (+) ' ||

        -- For standard, deluxe and premium prices (if given),
        --   determine the discount type and amount by pricing code
        'AND ''' || pricingCode || ''' = phd_std.price_header_id (+) ' ||
        'AND pm.standard_price >= phd_std.min_dollar_amt (+) ' ||
        'AND pm.standard_price <= phd_std.max_dollar_amt (+) ' ||
        'AND ''' || pricingCode || ''' = phd_dlx.price_header_id (+) ' ||
        'AND pm.deluxe_price >= phd_dlx.min_dollar_amt (+) ' ||
        'AND pm.deluxe_price <= phd_dlx.max_dollar_amt (+) ' ||
        'AND ''' || pricingCode || ''' = phd_prm.price_header_id (+) ' ||
        'AND pm.premium_price >= phd_prm.min_dollar_amt (+) ' ||
        'AND pm.premium_price <= phd_prm.max_dollar_amt (+) ' ||

        -- Filter out products that do not match price range, if provided
        'AND EXISTS ' ||
            '(Select 1 from filter_price_points pp ' ||
              'where pm.standard_price >= pp.price_lower_limit ' ||
                'and pm.standard_price <= pp.price_upper_limit ' ||
                'and pp.price_point_id = ' ||
                   'DECODE (''' || inPricePointId || ''', NULL, pp.price_point_id, ''' || inPricePointId || ''')) ' ||

        -- Use product_compnay_xref mapping table to find all products for this company
        'and sc.company_id = pcx.company_id ' ||
        'and pcx.product_id = pm.product_id ' ||


        'ORDER BY DECODE(pmv.UPSELL_PRODUCT_FLAG, ''Y'', pmv.UPSELL_SEARCH_PRIORITY, pm.search_priority)';

    OPEN cur_cursor FOR mySelect;
    RETURN cur_cursor;

END;
.
/
/
