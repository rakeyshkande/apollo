CREATE OR REPLACE
PROCEDURE ftd_apps.OE_UPDATE_SET_PRODUCT_UNAVAIL (
 productIdIn IN VARCHAR2
)
--=======================================================================
--
-- Name:    OE_UPDATE_SET_PRODUCT_UNAVAIL
-- Type:    Procedure
-- Syntax:  OE_UPDATE_SET_PRODUCT_UNAVAIL(productIdIn IN VARCHAR2)
--
-- Description:   Updates a products setting status = unavailable
--
--========================================================================

AS
BEGIN
        UPDATE PRODUCT_MASTER
        SET     STATUS = 'U',
                EXCEPTION_START_DATE = NULL,
                EXCEPTION_END_DATE = NULL
                WHERE PRODUCT_ID = productIdIn ;

END;
.
/
