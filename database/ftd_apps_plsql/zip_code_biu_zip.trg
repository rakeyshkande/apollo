CREATE OR REPLACE TRIGGER FTD_APPS.ZIP_CODE_BIU_ZIP
BEFORE INSERT OR UPDATE OF ZIP_CODE_ID ON ftd_apps.ZIP_CODE
FOR EACH ROW
BEGIN
    :new.ZIP_CODE_ID := OE_CLEANUP_ALPHANUM_STRING(:new.ZIP_CODE_ID, 'Y');
EXCEPTION WHEN OTHERS THEN
    NULL;
END;
.
/
