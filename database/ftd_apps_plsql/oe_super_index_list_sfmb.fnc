CREATE OR REPLACE
FUNCTION ftd_apps.OE_SUPER_INDEX_LIST_SFMB (
liveFlag                IN VARCHAR2,
occasionsFlag           IN VARCHAR2,
productsFlag            IN VARCHAR2,
dropshipFlag            IN VARCHAR2,
countryID               IN VARCHAR2,
sourceCodeID			IN VARCHAR2
)
 RETURN types.ref_cursor
--==========================================================================
--
-- Name:    OE_SUPER_INDEX_LIST
-- Type:    Function
-- Syntax:  OE_SUPER_INDEX_LIST (<see args above>)
-- Returns: ref_cursor for:
--          indexId           NUMBER
--          name              VARCHAR2(100)
--          indexFileName     VARCHAR2(300)
--          displayOrder      NUMBER,
-- 			sourceCodeID	  VARCHAR2
--
-- Description:   Gets the super index page columns, either those for the Occasions
--                column or those for the Products column, as specified.
--                Search indexes are always ignored.  If dropshipFlag is
--                provided as 'Y', indexes that contain ONLY dropship products
--                are ignored.  Live flag 'Y' or 'N' indicates that only indexes
--                for the live or the test call center app respectively should
--                be returned.  Country ID if provided narrows down to just
--                those indexes with the appropriate COUNTRY_ID field value.
--
--==========================================================================
AS
    cur_cursor types.ref_cursor;
BEGIN
    OPEN cur_cursor FOR
        SELECT pi.index_id as "indexId",
              pi.index_name as "name",
              pi.index_file_name as "indexFileName",
              pi.display_order as "displayOrder",
              DECODE(NVL(sc.subindex_count, 0), 0, 'N', 'Y') as "subIndexesExist"
         FROM PRODUCT_INDEX pi, INDEX_SUBINDEX_CNT_VW sc, SOURCE sc

         -- Ignore search indexes for purposes of this query
         WHERE pi.search_index_flag = 'N'
         -- Defect 2177. For international orders, do not check for parent_index_id
         AND ((NVL(countryID, '@@') IN ('@@', 'US', 'CA') AND pi.parent_index_id IS NULL)
           OR (NVL(countryID, '@@') NOT IN ('US', 'CA', '@@')))
         AND pi.index_id = sc.index_id (+)

         -- liveFlag 'Y' indicates CALL_CENTER_LIVE_FLAG should be 'Y'
         AND pi.call_center_live_flag = DECODE(liveFlag, 'Y', 'Y', pi.call_center_live_flag)

         -- liveFlag 'N' indicates CALL_CENTER_TEST_FLAG should be 'Y'
         AND pi.call_center_test_flag = DECODE(liveFlag, 'N', 'Y', pi.call_center_test_flag)

         -- narrow to indexes with OCCASIONS_FLAG 'Y' if occasionsFlag was supplied
         AND pi.occasions_flag = DECODE(occasionsFlag, 'N', pi.occasions_flag, 'Y')

         -- narrow to indexes with PRODUCTS_FLAG 'Y' if productsFlag was supplied
         AND pi.products_flag = DECODE(productsFlag, 'N', pi.products_flag, 'Y')

         -- dropshipFlag 'Y' indicates DROPSHIP_FLAG should be 'N' (not dropship only)
         AND pi.dropship_flag = DECODE(dropshipFlag, 'Y', 'N', pi.dropship_flag)

         -- narrow by countryID if provided
         AND NVL(pi.country_id, '@@') = DECODE(countryID, NULL, NVL(pi.country_id, '@@'), countryID)

		 AND sc.SOURCE_CODE = sourceCodeID

		 -- only pull indexes for the company in the source code
		 AND DECODE(pi.company_id, null, 'FTD', pi.company_id) = DECODE(sc.company_id, null, 'FTD', sc.company_id)

         ORDER BY display_order;

    RETURN cur_cursor;
END
;
.
/
