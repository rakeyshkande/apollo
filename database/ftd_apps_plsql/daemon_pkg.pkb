CREATE OR REPLACE
PACKAGE BODY ftd_apps.DAEMON_PKG AS

   pg_this_package VARCHAR2(61) := 'ftd_apps.daemon_pkg';

FUNCTION CURRENTLY_RUNNING (p_job_name IN VARCHAR2) RETURN BOOLEAN IS

/*-----------------------------------------------------------------------------
Description:
        Function looks for the daemon in the DBMS_JOB queue.  Returns
        a TRUE if it is found, or a FALSE if it is not.

Parameter:
        the name of the job, as found in user_jobs.what, including any parameters.

Returns:
        BOOLEAN value indicating if job is already in the queue.
-----------------------------------------------------------------------------*/

   v_check    NUMBER;
   v_return   BOOLEAN;

BEGIN

   SELECT COUNT(*)
   INTO   v_check
   FROM   user_jobs
   WHERE  what = p_job_name;

   IF v_check > 0 THEN
      v_return := TRUE;
   ELSE
      v_return := FALSE;
   END IF;

   RETURN V_RETURN;

END CURRENTLY_RUNNING;



PROCEDURE submit_hp_actions_monitor_1 IS

/*-----------------------------------------------------------------------------
Description:
        submit submit_hp_actions_monitor_1
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit_hp_actions_monitor_1';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;
   v_context               frp.global_parms.context%TYPE := 'hp_actions_monitor_1';

BEGIN

   v_submit_string := v_context||'('||frp.misc_pkg.get_global_parm_value(v_context, 'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(v_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit_hp_actions_monitor_1;



PROCEDURE remove_hp_actions_monitor_1 IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove_hp_actions_monitor_1';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_context               frp.global_parms.context%TYPE := 'hp_actions_monitor_1';

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = v_context||'('||frp.misc_pkg.get_global_parm_value(v_context, 'THRESHHOLD')||');';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove_hp_actions_monitor_1;


PROCEDURE submit_hp_actions_monitor_2 IS

/*-----------------------------------------------------------------------------
Description:
        submit submit_hp_actions_monitor_2
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit_hp_actions_monitor_2';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;
   v_context               frp.global_parms.context%TYPE := 'hp_actions_monitor_2';

BEGIN

   v_submit_string := v_context||'('||frp.misc_pkg.get_global_parm_value(v_context, 'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(v_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit_hp_actions_monitor_2;



PROCEDURE remove_hp_actions_monitor_2 IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove_hp_actions_monitor_2';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_context               frp.global_parms.context%TYPE := 'hp_actions_monitor_2';

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = v_context||'('||frp.misc_pkg.get_global_parm_value(v_context, 'THRESHHOLD')||');';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove_hp_actions_monitor_2;

FUNCTION zip_code_report_interval RETURN date IS

/*-----------------------------------------------------------------------------
   Given the current time, return the next time the zip code report is to run.
   The code's a bit ugly, feel free to rewrite.
   It represents a schedule of 7:00, 10:00, 12:00, 13:00, 19:00.

   --2006.05.08 eso schedule of 7:00, 10:00, 13:00, 16:00, 19:00
-----------------------------------------------------------------------------*/

   v_date DATE;
   v_hour NUMBER;

BEGIN
   SELECT to_number(to_char(sysdate,'hh24')) INTO v_hour FROM dual;
   CASE v_hour
      WHEN  0 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  1 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  2 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  3 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  4 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  5 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  6 THEN v_date := TRUNC(SYSDATE)+(7/24);
      WHEN  7 THEN v_date := TRUNC(SYSDATE)+(10/24);
      WHEN  8 THEN v_date := TRUNC(SYSDATE)+(10/24);
      WHEN  9 THEN v_date := TRUNC(SYSDATE)+(10/24);
      WHEN 10 THEN v_date := TRUNC(SYSDATE)+(13/24);
      WHEN 11 THEN v_date := TRUNC(SYSDATE)+(13/24);
      WHEN 12 THEN v_date := TRUNC(SYSDATE)+(13/24);
      WHEN 13 THEN v_date := TRUNC(SYSDATE)+(16/24);
      WHEN 14 THEN v_date := TRUNC(SYSDATE)+(16/24);
      WHEN 15 THEN v_date := TRUNC(SYSDATE)+(16/24);
      WHEN 16 THEN v_date := TRUNC(SYSDATE)+(19/24);
      WHEN 17 THEN v_date := TRUNC(SYSDATE)+(19/24);
      WHEN 18 THEN v_date := TRUNC(SYSDATE)+(19/24);
      WHEN 19 THEN v_date := TRUNC(SYSDATE+1)+(7/24);
      WHEN 20 THEN v_date := TRUNC(SYSDATE+1)+(7/24);
      WHEN 21 THEN v_date := TRUNC(SYSDATE+1)+(7/24);
      WHEN 22 THEN v_date := TRUNC(SYSDATE+1)+(7/24);
      WHEN 23 THEN v_date := TRUNC(SYSDATE+1)+(7/24);
   END CASE;
   RETURN v_date;
END zip_code_report_interval;


PROCEDURE submit_zip_code_report IS

/*-----------------------------------------------------------------------------
Description:
        submit zip_code_report
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit_zip_code_report';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;
   v_context               frp.global_parms.context%TYPE := 'zip_code_report';

BEGIN

   v_submit_string := v_context||';';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := 'ftd_apps.daemon_pkg.zip_code_report_interval';
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit_zip_code_report;



PROCEDURE remove_zip_code_report IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove_zip_code_report';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_context               frp.global_parms.context%TYPE := 'zip_code_report';

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what = v_context||';';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove_zip_code_report;


PROCEDURE submit_florist_monitor_3 IS

/*-----------------------------------------------------------------------------
Description:
        submit florist_monitor_3
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit_florist_monitor_3';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;
   v_context               frp.global_parms.context%TYPE := 'florist_monitor_3';

BEGIN

   v_submit_string := v_context||'('||frp.misc_pkg.get_global_parm_value(v_context, 'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(v_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit_florist_monitor_3;


PROCEDURE remove_florist_monitor_3 IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove_florist_monitor_3';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_context               frp.global_parms.context%TYPE := 'florist_monitor_3';

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what like v_context||'%';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove_florist_monitor_3;


PROCEDURE sb_weboe_nodispatch_monitor IS

/*-----------------------------------------------------------------------------
Description:
        submit weboe_nodispatch_monitor
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'sb_weboe_nodispatch_monitor';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;
   v_context               frp.global_parms.context%TYPE := 'weboe_nodispatch_monitor';

BEGIN

   v_submit_string := v_context||'('||frp.misc_pkg.get_global_parm_value(v_context, 'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(v_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END sb_weboe_nodispatch_monitor;


PROCEDURE rm_weboe_nodispatch_monitor IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'rm_weboe_nodispatch_monitor';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_context               frp.global_parms.context%TYPE := 'weboe_nodispatch_monitor';

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what like v_context||'%';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END rm_weboe_nodispatch_monitor;


PROCEDURE submit (p_context VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        submit a daemon
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;

BEGIN

   v_submit_string := p_context||'('||frp.misc_pkg.get_global_parm_value(p_context, 'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_submit_string) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(p_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit;



PROCEDURE remove (p_context VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what like p_context||'%';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove;


END daemon_pkg;
.
/
