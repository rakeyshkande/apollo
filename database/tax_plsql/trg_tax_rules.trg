create or replace trigger tax.trg_tax_rules
AFTER INSERT OR UPDATE
ON tax.tax_rules
REFERENCING OLD AS OLD NEW AS NEW 
FOR EACH ROW

DECLARE
    c utl_smtp.connection;
    v_status    VARCHAR2(1000);
    v_message   VARCHAR2(1000);
    err_num NUMBER;
    err_msg VARCHAR2(100);
    v_error_status VARCHAR2(5) := 'N';

    PROCEDURE send_email(v_description IN VARCHAR2, v_updated_by IN VARCHAR2, v_updated_on IN VARCHAR2) AS
      BEGIN
        ops$oracle.mail_pkg.init_email(c, 'TAX_MS_ALERTS',
                    'ATTENTION:Tax Rules updated *Action Required*' , v_status, v_message);

        utl_smtp.write_raw_data(c, utl_raw.cast_to_raw('Content-Type' || ': ' ||
                               'text/html; charset=iso-8859-1' || utl_tcp.CRLF));

        utl_smtp.write_data(c, '<body>' );
        utl_smtp.write_data(c, ' <br> Tax Rules have been modified. Please reflect the same on GCP Tax Micro Service end. <br>' );
        utl_smtp.write_data(c, ' <br> Description  :: '||v_description||' <br>' );       
        utl_smtp.write_data(c, ' <br> Updated By  :: '||v_updated_by||' <br>' );
        utl_smtp.write_data(c, ' <br> Updated On  :: '||v_updated_on||' <br>' );
        utl_smtp.write_data(c, '</body>');
        

        ops$oracle.mail_pkg.close_email(c, v_status, v_message);
      END;

BEGIN
   IF INSERTING THEN
     -- initialize the status
      INSERT INTO TAX.TAX_RULES$
      (RULE_HIST_ID,RULE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
    VALUES (TAX_RULE_ID_SQ.nextval,:NEW.RULE,:NEW.DESCRIPTION,:NEW.CREATED_ON,:NEW.CREATED_BY,:NEW.UPDATED_ON,:NEW.UPDATED_BY);

    ELSIF UPDATING THEN
      INSERT INTO TAX.TAX_RULES$
      (RULE_HIST_ID,RULE,DESCRIPTION,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
    VALUES (TAX_RULE_ARCHIVE_ID_SQ.nextval,:OLD.RULE,:OLD.DESCRIPTION,:OLD.CREATED_ON,:OLD.CREATED_BY,:OLD.UPDATED_ON,:OLD.UPDATED_BY);
   END IF;
   
   BEGIN
    send_email(:new.DESCRIPTION, :new.UPDATED_BY, :new.UPDATED_ON);
   EXCEPTION
    WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In TAX.TRG_TAX_RULES while Sending Notification mail when Tax Rules are updated: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
   END;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In TAX.TRG_TAX_RULES: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);

END trg_tax_rules;
.
/
