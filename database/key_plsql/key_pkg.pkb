CREATE OR REPLACE PACKAGE BODY KEY.KEY_PKG
AS
PROCEDURE GET_NEXT_ORDER_ID 
(
OUT_ORDER_ID                             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will return the next Order Id.

Input:
        none
Output:
        OUT_ORDER_ID -- next order id
-----------------------------------------------------------------------------*/
BEGIN
	SELECT 'ORD'||to_char(KEY.KEYGEN('ORDERS')) 
	INTO OUT_ORDER_ID
	FROM dual;

END GET_NEXT_ORDER_ID;

END;
/
