CREATE OR REPLACE
function key.keygen(p_table in varchar2) return number as
/* This function returns key values for the FRP or SCRUB schema.
   It assumes that each table needing keys has a corresponding sequence,
   that has the same name as the table, followed by _SQL.  If this is
   true, it will return the NEXTVAL of that sequence. */
   v_key number;
   v_string varchar2(400);
begin
   v_string := 'select '||upper(p_table)||'_sq.nextval from dual';
   execute immediate v_string into v_key;
   return v_key;
end;
.
/
