CREATE OR REPLACE PACKAGE BODY "ACCOUNT"."ACCOUNT_MAINT_PKG"
AS

PROCEDURE CREATE_ACTIVE_ACCOUNT
(
    IN_EMAIL_ADDRESS            IN ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE,
    IN_UPDATED_BY               IN ACCOUNT_MASTER.UPDATED_BY%TYPE,
    OUT_ACCOUNT_MASTER_ID       OUT ACCOUNT_MASTER.ACCOUNT_MASTER_ID%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for creating an Active Account for the
        email address.
        If an active account already exists, it returns the existing account.
        Else, it creates a new Account Master and Account Email Address.
        
Input:
        IN_EMAIL_ADDRESS              The Email address of the account.
        IN_UPDATED_BY                 The id of the user creating the account.
        
Output:
        OUT_ACCOUNT_MASTER_ID         The Id of the new/existing account.
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/

existing_account_master_id    account_master.account_master_id%type;
CURSOR current_account_cur IS 
    select am.account_master_id from account_master am
    inner join account_email ae
    on am.account_master_id = ae.account_master_id
    where
        ae.email_address = lower(in_email_address)
        and ae.active_flag = 'Y';

BEGIN
    
    OPEN current_account_cur;
    FETCH current_account_cur into existing_account_master_id;
    
    IF current_account_cur%found THEN
        CLOSE current_account_cur;
        out_account_master_id := existing_account_master_id;
    ELSE
        CLOSE current_account_cur;
        -- create a new account
        select account_master_id_sq.nextval into out_account_master_id from dual;
        INSERT INTO account.account_master
        (
            account_master_id,
            created_on,
            created_by,
            updated_on,
            updated_by
        )
        values
        (
            out_account_master_id,
            sysdate,
            in_updated_by,
            sysdate,
            in_updated_by
        );
        
        -- insert account email
        INSERT INTO account.account_email
        (
            account_email_id,
            account_master_id,
            email_address,
            active_flag,
            created_on,
            created_by,
            updated_on,
            updated_by
        )
        values
        (
            account_email_id_sq.nextval,
            out_account_master_id,
            lower(in_email_address),
            'Y',
            sysdate,
            in_updated_by,
            sysdate,
            in_updated_by
        );    
    END IF;

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_ACTIVE_ACCOUNT;


PROCEDURE ADD_PROGRAM_TO_ACCOUNT
(
    IN_ACCOUNT_MASTER_ID        IN ACCOUNT_PROGRAM.ACCOUNT_MASTER_ID%TYPE,
    IN_PROGRAM_NAME             IN PROGRAM_MASTER.PROGRAM_NAME%TYPE,
    IN_PROGRAM_STATUS           IN ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_STATUS%TYPE,
    IN_AUTO_RENEW_FLAG          IN ACCOUNT_PROGRAM.AUTO_RENEW_FLAG%TYPE,
    IN_EXPIRATION_DATE          IN ACCOUNT_PROGRAM.EXPIRATION_DATE%TYPE,
    IN_START_DATE               IN ACCOUNT_PROGRAM.START_DATE%TYPE,
    IN_EXTERNAL_ORDER_NUMBER    IN ACCOUNT_PROGRAM.EXTERNAL_ORDER_NUMBER%TYPE,    
    IN_UPDATED_BY               IN ACCOUNT_PROGRAM.UPDATED_BY%TYPE,
    OUT_ACCOUNT_PROGRAM_ID      OUT ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_ID%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for adding the program to the account 
        if the program is not already on the account. Else, does nothing.
        Returns the account program id of the new or existing program record 
        for the Program Record on the account master.
        When checking for an existing program, it checks for the same Program
        Name and External Order Number.

        
Input:
        IN_ACCOUNT_MASTER_ID         The Account Master to add the Program
        IN_PROGRAM_NAME              The name of the Program to add
        IN_PROGRAM_STATUS            The status of the program to add
        IN_AUTO_RENEW_FLAG           The value for the auto-renew flag
        IN_EXPIRATION_DATE           Expiration date of the program
        IN_START_DATE                Start date of the program
        IN_EXTERNAL_ORDER_NUMBER     The order number that purchased the program.        
        IN_UPDATED_BY                The id of the user adding the program.
        
Output:
        OUT_ACCOUNT_PROGRAM_ID       The Id of the new/existing account program.
        OUT_STATUS                   'Y' on success. Else 'N'
        OUT_MESSAGE                  Error message if the status is 'N'
-----------------------------------------------------------------------------*/

existing_account_program_id    account_program.account_program_id%type;
CURSOR current_account_program_cur IS 
    select ap.account_program_id from account_program ap
    inner join program_master pm
    on ap.program_master_id = pm.program_master_id
    where
        ap.account_master_id = in_account_master_id
        and pm.program_name = in_program_name
        and ap.external_order_number = in_external_order_number;
        
BEGIN
    
    OPEN current_account_program_cur;
    FETCH current_account_program_cur into existing_account_program_id;
    
    IF current_account_program_cur%found THEN
        CLOSE current_account_program_cur;
        out_account_program_id := existing_account_program_id;
    ELSE
        CLOSE current_account_program_cur;
        -- create a new program account
        select account_program_id_sq.nextval into out_account_program_id from dual;
        INSERT INTO account_program
        (
            account_program_id,
            account_master_id,
            program_master_id,
            account_program_status,
            auto_renew_flag,
            expiration_date,
            start_date,
            external_order_number,
            created_on,
            created_by,
            updated_on,
            updated_by
        )
        VALUES
        (
            out_account_program_id,
            in_account_master_id,
            (select program_master_id from program_master where program_name=in_program_name),
            in_program_status,
            in_auto_renew_flag,
            trunc(in_expiration_date),
            trunc(in_start_date),
            in_external_order_number,
            sysdate,
            in_updated_by,
            sysdate,
            in_updated_by
        );
        
    END IF;

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END ADD_PROGRAM_TO_ACCOUNT;

PROCEDURE SET_ACCOUNT_EMAIL_INACTIVE
(
    IN_EMAIL_ADDRESS            IN ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE,
    IN_UPDATED_BY               IN ACCOUNT_EMAIL.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS
/*-----------------------------------------------------------------------------
Description:
        This procedure sets the ACTIVE_FLAG in ACCOUNT_EMAIL to N.
        
Input:
        IN_EMAIL_ADDRESS              The Email address of the account.
        IN_UPDATED_BY                 The User making the change.
Output:
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/

BEGIN

    update account_email
    set active_flag = 'N',
        updated_by = in_updated_by,
        updated_on = sysdate
    where account_email_id = (
        select ae.account_email_id
        from account_email ae, account_master am
        where ae.email_address = lower(in_email_address)
        and ae.active_flag = 'Y'
        and ae.account_master_id = am.account_master_id);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END SET_ACCOUNT_EMAIL_INACTIVE;

PROCEDURE INSERT_ACCOUNT_EMAIL
(
    IN_ACCOUNT_MASTER_ID        IN ACCOUNT_EMAIL.ACCOUNT_MASTER_ID%TYPE,
    IN_EMAIL_ADDRESS            IN ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE,
    IN_ACTIVE_FLAG              IN ACCOUNT_EMAIL.ACTIVE_FLAG%TYPE,
    IN_UPDATED_BY               IN ACCOUNT_EMAIL.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS

BEGIN

    insert into account.account_email (
        account_email_id,
        account_master_id,
        email_address,
        active_flag,
        created_on,
        created_by,
        updated_on,
        updated_by)
    values (
        account_email_id_sq.nextval,
        in_account_master_id,
        lower(in_email_address),
        in_active_flag,
        sysdate,
        in_updated_by,
        sysdate,
        in_updated_by);    

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ACCOUNT_EMAIL;

PROCEDURE UPDATE_ACCOUNT_PROGRAM
(
    IN_ACCOUNT_PROGRAM_ID       IN ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_ID%TYPE,
    IN_PROGRAM_STATUS            IN ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_STATUS%TYPE,
    IN_AUTO_RENEW_FLAG          IN ACCOUNT_PROGRAM.AUTO_RENEW_FLAG%TYPE,
    IN_EXPIRATION_DATE          IN ACCOUNT_PROGRAM.EXPIRATION_DATE%TYPE,
    IN_START_DATE               IN ACCOUNT_PROGRAM.START_DATE%TYPE,
    IN_EXTERNAL_ORDER_NUMBER    IN ACCOUNT_PROGRAM.EXTERNAL_ORDER_NUMBER%TYPE,    
    IN_UPDATED_BY               IN ACCOUNT_PROGRAM.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS

BEGIN

    update account_program
    set account_program_status = in_program_status,
        auto_renew_flag = in_auto_renew_flag,
        expiration_date = in_expiration_date,
        start_date = in_start_date,
        external_order_number=in_external_order_number,
        updated_by = in_updated_by,
        updated_on = sysdate
    where account_program_id = in_account_program_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ACCOUNT_PROGRAM;


PROCEDURE UPDATE_PROGRAM_STATUS_BY_ORDER
(
    IN_EXTERNAL_ORDER_NUMBER    IN ACCOUNT_PROGRAM.EXTERNAL_ORDER_NUMBER%TYPE,    
    IN_PROGRAM_STATUS           IN ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_STATUS%TYPE,
    IN_UPDATED_BY               IN ACCOUNT_PROGRAM.UPDATED_BY%TYPE,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the status on a Program by Order Number. If the
        status on the record changes, it queues a message to the JMS queue to 
        feed the update to the Website.
        If no programs are found for the order number, the method does nothing.
        Errors enquing the JMS message are returned in the return values.
        
Input:
        IN_EXTERNAL_ORDER_NUMBER    The order number of the program.    
        IN_PROGRAM_STATUS           The new status value
        IN_UPDATED_BY               The User making the change. 
    
Output:
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/

v_feed_jms_task             varchar2(4000);
v_jmsstatus                 varchar2(10);
v_jmsmessage                varchar2(4000);
   
CURSOR update_account_program_cur IS 
    select ap.account_program_id from account_program ap
    where
        ap.external_order_number = in_external_order_number
        and ap.account_program_status != in_program_status;

BEGIN
    
    OUT_STATUS := 'Y';
    
    FOR account_program_rec IN update_account_program_cur
    LOOP
        -- Update the Program Status
        update account_program
        set account_program_status = in_program_status,
            updated_on = sysdate,
            updated_by = in_updated_by
        where account_program_id = account_program_rec.account_program_id;

        -- Send the JMS Message to feed the update to the website
        v_feed_jms_task := '<accountTask type="feedProgramInfoToWeb"><programs><program><accountProgramId>'
                           || account_program_rec.account_program_id
                           || '</accountProgramId></program></programs></accountTask>';
        
        EVENTS.POST_A_MESSAGE_FLEX('OJMS.ACCOUNT', 'feedProgramInfoToWeb:' || account_program_rec.account_program_id, v_feed_jms_task,0,v_jmsstatus,v_jmsmessage);
        
        IF v_jmsstatus = 'N' THEN
            OUT_STATUS := 'N';
            OUT_MESSAGE := 'ERROR QUEUEING JMS MESSAGE: ' || v_jmsmessage;
        END IF;
    
    END LOOP;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PROGRAM_STATUS_BY_ORDER;

PROCEDURE FS_AUTO_RENEW(
IN_EMAIL_ADDRESS        IN ACCOUNT.ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE
)
AS
  v_prod_id                 clean.order_details.product_id%type;
  v_default_prod_id         clean.order_details.product_id%type;
  v_src_cd                  clean.order_details.source_code%type;
  v_ext_ord                 varchar2(100);
  v_mas_ord                 varchar2(100);
  v_cc_id                   clean.credit_cards.cc_id%type;
  v_cc_type                 clean.credit_cards.cc_type%type;
  v_cc_number               clean.credit_cards.cc_number%type;
  v_cc_expiration           clean.credit_cards.cc_expiration%type;
  v_prod_price              clean.payments.credit_amount%type;
  v_default_prod_price      clean.payments.credit_amount%type;
  v_active_prod_cnt         number;
  v_active_default_prod_cnt number;
  v_active_src_cd_cnt       number;
  v_src_res_cnt             number;
  v_recipient_id            clean.order_details.recipient_id%type;
  v_buyer_id                clean.orders.customer_id%type;
  cust_rec                  clean.customer%ROWTYPE;
  pay_cur                   TYPES.REF_CURSOR;
  ext_ord_cur               TYPES.REF_CURSOR;
  mas_ord_cur               TYPES.REF_CURSOR;
  ord_det_rec               clean.order_details%ROWTYPE;
  ord_rec                   clean.orders%ROWTYPE;
  v_key_name                clean.credit_cards.key_name%type;
  v_pay_id                  clean.payments.payment_id%type;
  cc_number                 varchar2(20);
  --message                   sys.aq$_jms_text_message;
  v_system_message_id       number;
  v_sql_condition           varchar2(200);
  v_stat                    VARCHAR2(1);
  v_mess                    VARCHAR2(256);
  v_user_id                 varchar2(1000);


  CURSOR ap_cur IS
  select cod.external_order_number, cod.occasion, cod.product_id, concat('SOURCE_CODE_',co.company_id) as source_code, cod.order_guid,
  aap.account_program_id, aap.expiration_date, aae.email_address, co.master_order_number, cod.order_detail_id, co.origin_id, co.language_id,
  cod.recipient_id, fac.company_id,
  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(cod.recipient_id,'Day') as recipient_day_phone,
  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(cod.recipient_id,'Evening') as recipient_evening_phone,
  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(cod.recipient_id,'Day') as recipient_day_ext,
  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(cod.recipient_id,'Evening') as recipient_evening_ext,
  co.customer_id,
  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(co.customer_id,'Day') as customer_day_phone,
  clean.customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type(co.customer_id,'Evening') as customer_evening_phone,
  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(co.customer_id,'Day') as customer_day_ext,
  clean.customer_query_pkg.GET_CUSTOMER_ext_by_id_type(co.customer_id,'Evening') as customer_evening_ext,
  (select email_address from clean.email where email_id = co.email_id) as customer_email,
  fac.default_dnis_id as dnis_id
  from clean.order_details cod, ACCOUNT.account_program aap,
  ACCOUNT.ACCOUNT_EMAIL aae, clean.orders co, FTD_APPS.company fac
  where
  cod.external_order_number = aap.external_order_number
  and aap.account_master_id = aae.account_master_id
  and cod.order_guid = co.order_guid
  and aae.active_flag = 'Y'
  and (IN_EMAIL_ADDRESS is null or lower(aae.email_address) = lower(IN_EMAIL_ADDRESS))
  and aap.auto_renew_flag = 'Y'
  and aap.account_program_status = 'A'
  and fac.company_id = co.company_id||'P'
  and cod.product_id is not null
  and (aap.processed_flag is null or aap.processed_flag = 'N')
  and trunc(aap.expiration_date)=trunc(sysdate);

  ap_rec ap_cur%ROWTYPE;

  BEGIN

select count(*) into v_active_default_prod_cnt from FRP.global_parms fgp, FTD_APPS.product_master fpm where fpm.product_id = fgp.VALUE and
          fgp.context = 'SERVICES_FREESHIPPING' and fgp.name = 'DEFAULT' AND FPM.STATUS='A' AND fpm.product_type='SERVICES' and fpm.product_sub_type='FREESHIP';
if v_active_default_prod_cnt > 0 then
    select NVL(fgp.value,''), NVL(fpm.standard_price,0) into v_default_prod_id, v_default_prod_price from FRP.global_parms fgp, FTD_APPS.product_master fpm where fpm.product_id = fgp.VALUE and
          fgp.context = 'SERVICES_FREESHIPPING' and fgp.name = 'DEFAULT' AND fpm.product_type='SERVICES' and fpm.product_sub_type='FREESHIP';
end if;
  
  select value into v_user_id from frp.global_parms where context = 'SERVICES_FREESHIPPING' and name = 'UPDATED_BY';
  if v_user_id is null then
      v_user_id := 'FTDGoldAutoRenew';
  end if;

  --select value into v_key_name from frp.global_parms where context='Ingrian' and name='Current Key';
  /*
  * Loop through the cursor and auto renew the free shipping memberships that are due to expire.
  */
  FOR ap_rec IN ap_cur loop
      select count(*) into v_active_prod_cnt from FRP.global_parms fgp, ftd_apps.product_master fpm where fpm.product_id = fgp.VALUE and
          fgp.context = 'SERVICES_FREESHIPPING' and upper(fgp.name) = upper(ap_rec.product_id) AND FPM.STATUS='A' AND fpm.product_type='SERVICES' and fpm.product_sub_type='FREESHIP';
      /*
      *Check if the auto-renewal sku or default sku is valid. if both are invalid send system message and quit the program
      */
      if v_active_prod_cnt = 0 then
          if v_active_default_prod_cnt = 0 then
            --DBMS_OUTPUT.PUT_LINE('Send System Message');
            frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'FS AUTO-RENEWAL FAILED DUE TO INVALID SKU, CHECK THE SETUP FOR THE SKU IN GLOBAL_PARMS -> '||'EXTERNAL_ORDR_NUMBER : '||ap_rec.external_order_number||' SKU : '||ap_rec.product_id,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
            continue;
          else
            v_prod_id := v_default_prod_id;
            v_prod_price := v_default_prod_price;
          end if;
      else
        
        select value into v_prod_id from frp.global_parms where context = 'SERVICES_FREESHIPPING' and name = ap_rec.product_id;
        select standard_price into v_prod_price from FTD_APPS.product_master where product_id = v_prod_id;
      end if;
      -- Check if the auto renew source code is valid otherwise send system message ans quit the program
      select count(*) INTO  v_src_res_cnt from  FTD_APPS.PRODUCT_ATTR_RESTR_SOURCE_EXCL fpa, frp.global_parms fgp where fpa.source_code = FGP.VALUE AND fgp.context='SERVICES_FREESHIPPING'
      AND FGP.NAME=ap_rec.source_code and fpa.product_attr_restr_id = 10;
      select COUNT(*) INTO  v_active_src_cd_cnt from ftd_apps.source_master fsm, frp.global_parms fgp where FSM.SOURCE_CODE=FGP.VALUE AND fgp.context='SERVICES_FREESHIPPING'
      AND FGP.NAME=ap_rec.source_code AND (FSM.END_DATE IS NULL OR FSM.END_DATE >= SYSDATE) ;
      
      if v_active_src_cd_cnt = 0  or v_src_res_cnt > 0 then
         frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'FS AUTO-RENEWAL FAILED DUE TO INVALID SOURCE CODE, CHECK THE SETUP FOR THE SOURCE CODE IN GLOBAL_PARMS -> '||'EXTERNAL_ORDR_NUMBER : '||ap_rec.external_order_number||' SOURCE CODE : '||ap_rec.source_code,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
            continue;
      end if;
      select value into v_src_cd from frp.global_parms where name=ap_rec.source_code and context = 'SERVICES_FREESHIPPING';
      --Get last used credit card information
      clean.order_query_pkg.GET_LAST_CC_USED_FS(IN_ORDER_DETAIL_ID=>ap_rec.order_detail_id, OUT_CUR=>pay_cur);
      v_cc_id :=null;
      FETCH pay_cur INTO v_cc_id, v_cc_type, v_cc_number, v_cc_expiration, v_key_name;
      close pay_cur;
      
	  IF v_cc_id is not null then
        --Create recipient
        select customer_id, first_name, last_name, address_1, city, state, zip_code, country, address_type, business_name
        into cust_rec.customer_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
        cust_rec.zip_code, cust_rec.country, cust_rec.address_type, cust_rec.business_name
        from clean.customer cc where customer_id = ap_rec.recipient_id;

        v_recipient_id := joe.customer_id_sq.nextval;
        
        INSERT INTO JOE.ORDER_CUSTOMER
        (
            customer_id, first_name, last_name, address, city, state, zip_code, country, buyer_recipient_ind,
            daytime_phone, daytime_phone_ext, evening_phone, evening_phone_ext, email_address, newsletter_flag,
            qms_response_code, address_type, business_name, business_info, membership_id, membership_first_name,
            membership_last_name, program_name, created_by, created_on, updated_by, updated_on
        )
        VALUES
        (
            v_recipient_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
            cust_rec.zip_code, cust_rec.country, 'R', ap_rec.recipient_day_phone, ap_rec.recipient_day_ext, ap_rec.recipient_evening_phone,
            ap_rec.recipient_evening_ext, null, 'N', null, cust_rec.address_type, cust_rec.business_name, null, null, null, null, null,
            v_user_id, sysdate, v_user_id, sysdate
        );

        --Create buyer
        select customer_id, first_name, last_name, address_1, city, state, zip_code, country, address_type, business_name
        into cust_rec.customer_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
        cust_rec.zip_code, cust_rec.country, cust_rec.address_type, cust_rec.business_name
        from clean.customer where customer_id = ap_rec.customer_id;

        v_buyer_id := joe.customer_id_sq.nextval;
        INSERT INTO JOE.ORDER_CUSTOMER
        (
            customer_id, first_name, last_name, address, city, state, zip_code, country, buyer_recipient_ind,
            daytime_phone, daytime_phone_ext, evening_phone, evening_phone_ext, email_address, newsletter_flag,
            qms_response_code, address_type, business_name, business_info, membership_id, membership_first_name,
            membership_last_name, program_name, created_by, created_on, updated_by, updated_on
        )
        VALUES
        (
            v_buyer_id, cust_rec.first_name, cust_rec.last_name, cust_rec.address_1, cust_rec.city, cust_rec.state,
            cust_rec.zip_code, cust_rec.country, 'B', ap_rec.customer_day_phone, ap_rec.customer_day_ext, ap_rec.customer_evening_phone,
            ap_rec.customer_evening_ext, ap_rec.customer_email, 'N', null, cust_rec.address_type, cust_rec.business_name, null, null, null, null, null,
            v_user_id, sysdate, v_user_id, sysdate
        );
        
        JOE.OE_MAINT_PKG.GET_ORDER_DETAIL_SEQUENCE(v_ext_ord);
        JOE.OE_MAINT_PKG.GET_MASTER_SEQUENCE(v_mas_ord);
        
        v_ext_ord := 'C'|| v_ext_ord;
        v_mas_ord := 'M'|| v_mas_ord;
        --DBMS_OUTPUT.PUT_LINE('Got new Master_Order_Number : '||v_mas_ord);
        --DBMS_OUTPUT.PUT_LINE('Got new External_Order_Number : '||v_ext_ord);
        INSERT INTO JOE.ORDERS
        (
            master_order_number, buyer_id, dnis_id, source_code, origin_id, order_date,
            order_taken_by_identity_id, item_cnt, order_amt, fraud_id, fraud_cmnt, sent_to_apollo_date,
            sent_to_apollo_retry_cnt, created_by, created_on, updated_by, updated_on, language_id
        )
        VALUES
        (
            v_mas_ord, v_buyer_id, ap_rec.dnis_id, v_src_cd, ap_rec.company_id,
            sysdate, v_user_id, 1, v_prod_price, null, null, null, 0,
            v_user_id, sysdate, v_user_id, sysdate, ap_rec.language_id
        );
        INSERT INTO JOE.ORDER_DETAILS
        (
            external_order_number, master_order_number, source_code, delivery_date, recipient_id,
            product_id, product_subcode_id, first_color_choice, second_color_choice, iotw_flag, occasion_id,
            card_message_signature, order_cmnts, florist_cmnts, florist_id, ship_method_id, delivery_date_range_end,
            size_ind, product_amt, add_on_amt, shipping_fee_amt, service_fee_amt, discount_amt, tax_amt, item_total_amt,
            miles_points_qty, created_by, created_on, updated_by, updated_on, personal_greeting_id, bin_source_changed_flag,
            tax1_name, tax1_description, tax1_rate, tax1_amount, tax2_name, tax2_description, tax2_rate, tax2_amount,
            tax3_name, tax3_description, tax3_rate, tax3_amount, tax4_name, tax4_description, tax4_rate, tax4_amount,
            tax5_name, tax5_description, tax5_rate, tax5_amount
        )
        VALUES
        (
            v_ext_ord, v_mas_ord, v_src_cd, sysdate, v_buyer_id, v_prod_id,
            null, null, null, 'N', ap_rec.occasion, null, null, null, null, null,
            null, null, v_prod_price, 0, 0, 0, 0, 0, v_prod_price,
            null, v_user_id, sysdate, v_user_id, sysdate, null,'N',
            null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null
        );
      INSERT INTO JOE.ORDER_PAYMENTS
      (
          payment_id, master_order_number, payment_method_id, cc_number, key_name,
          cc_expiration, auth_amt, approval_code, approval_verbiage, approval_action_code,
          acq_reference_number, avs_result_code, gc_coupon_number, gc_amt,  aafes_ticket_number,
          nc_approval_identity_id, nc_type_code, nc_reason_code, nc_order_detail_id, nc_amt,
          csc_response_code, csc_validated_flag, csc_failure_cnt, created_by, created_on,
          updated_by, updated_on
      )
      VALUES
      (
          joe.payment_id_sq.nextval, v_mas_ord, v_cc_type,
          v_cc_number,v_key_name,  v_cc_expiration, v_prod_price,
          null, null, null, null, null, null, null, null, null,  null, null, null, null,
          null, 'N', null, v_user_id, sysdate, v_user_id, sysdate
      );

        --Send message to queue
        EVENTS.post_a_message_flex('ojms.JOE_ORDER_DISPATCHER',NULL,v_mas_ord,3,v_stat,v_mess);
		 
        --update processed Flag
        update account.account_program
        set processed_flag = 'Y',
            updated_on = sysdate,
            updated_by = v_user_id
        where account_program_id = ap_rec.account_program_id;

        IF (v_stat = 'N') THEN
        ROLLBACK;
        frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'FS AUTO-RENEWAL FAILED DUE UNEXPECTED ERROR IN SENDING MESSAGE TO JOE_ORDER_DISPATCHER -> '||'EXTERNAL_ORDR_NUMBER : '||ap_rec.external_order_number,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
        END IF;

      END IF;


  end loop;
EXCEPTION WHEN OTHERS THEN
        rollback;
        frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256)||'EXTERNAL_ORDR_NUMBER : '||ap_rec.external_order_number,
            sys_context('USERENV','HOST'), v_system_message_id, v_stat, v_mess);
        
END FS_AUTO_RENEW;

END;
.
/
