CREATE OR REPLACE
PACKAGE BODY          ACCOUNT.ACCOUNT_QUERY_PKG
AS

PROCEDURE VIEW_ACCOUNT_ACTIVE_PROGRAMS
(
    IN_EMAIL_ADDRESS            IN ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE,
    IN_ACTIVE_DATE              IN DATE,
    OUT_CUR                     OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns all the active Programs for an Account.
        If the Account does not exist, returns an empty list.
        The program is active if its 
            PROGRAM_MASTER.ACTIVE_FLAG = 'Y'
            START_DATE <= IN_ACTIVE_DATE <= EXPIRATION_DATE (Date checks ignore time)
            ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_STATUS = 'A' 
        
Input:
        IN_EMAIL_ADDRESS              The Email address of the account.
        IN_ACTIVE_DATE                The date to check the account start and expirations with
Output:
        OUT_CUR                       The List of Active Programs for the Account.
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/
BEGIN
     open out_cur for 
        select 
            ap.ACCOUNT_MASTER_ID,        
            ap.ACCOUNT_PROGRAM_ID,
            ap.PROGRAM_MASTER_ID,
            pm.PROGRAM_NAME,
            pm.PROGRAM_DESCRIPTION
        from ACCOUNT.ACCOUNT_PROGRAM ap
        inner join ACCOUNT.PROGRAM_MASTER pm
        on ap.PROGRAM_MASTER_ID = pm.PROGRAM_MASTER_ID
        inner join ACCOUNT.ACCOUNT_MASTER am
        on ap.ACCOUNT_MASTER_ID = am.ACCOUNT_MASTER_ID
        inner join ACCOUNT.ACCOUNT_EMAIL em
        on am.ACCOUNT_MASTER_ID = em.ACCOUNT_MASTER_ID
        where
			ap.ACCOUNT_PROGRAM_STATUS = 'A'
            and (ap.START_DATE is null or trunc(ap.START_DATE) <= trunc(IN_ACTIVE_DATE))
            and (ap.EXPIRATION_DATE is null or trunc(IN_ACTIVE_DATE) <= trunc(ap.EXPIRATION_DATE))
            and (em.EMAIL_ADDRESS = lower(IN_EMAIL_ADDRESS) and em.ACTIVE_FLAG='Y')
            and pm.ACTIVE_FLAG = 'Y';

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VIEW_ACCOUNT_ACTIVE_PROGRAMS;

PROCEDURE VIEW_ACTIVE_ACCOUNT
(
    IN_EMAIL_ADDRESS            IN ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE,
    OUT_CUR_ACCT_MASTER         OUT TYPES.REF_CURSOR,
    OUT_CUR_ACCT_EMAIL          OUT TYPES.REF_CURSOR,
    OUT_CUR_ACCT_PROGRAM        OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns all the active Account Master,
            Account Email, and Account Program records for an Account.
        If the Account does not exist, returns an empty list.
        
Input:
        IN_EMAIL_ADDRESS              The Email address of the account.
Output:
        OUT_CUR_ACCT_MASTER           The List of Active Account Master records for the Account.
        OUT_CUR_ACCT_EMAIL            The List of Active Account Email records for the Account.
        OUT_CUR_ACCT_PROGRAM          The List of Active Account Program records for the Account.
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/

v_account_master_id         number;

CURSOR get_account_cur(p_email_address varchar2) IS
    SELECT am.account_master_id
    FROM account.account_master am, account.account_email ae
    WHERE ae.email_address = lower(p_email_address)
    AND ae.active_flag = 'Y'
    AND am.account_master_id = ae.account_master_id;
 
BEGIN

OPEN get_account_cur(in_email_address);
FETCH get_account_cur INTO v_account_master_id;
CLOSE get_account_cur;

IF v_account_master_id IS NOT NULL THEN

    OPEN OUT_CUR_ACCT_MASTER FOR
        select account_master_id
        from account_master
        where account_master_id = v_account_master_id;

    OPEN OUT_CUR_ACCT_EMAIL FOR
        select account_email_id,
            email_address,
            active_flag
        from account_email
        where account_master_id = v_account_master_id;

    OPEN OUT_CUR_ACCT_PROGRAM FOR
        select ap.account_program_id,
            ap.program_master_id,
            pm.program_name,
            ap.account_program_status,
            ap.auto_renew_flag,
            cast(ap.start_date as timestamp) start_date,
            cast(ap.expiration_date as timestamp) expiration_date,
            ap.external_order_number,
            cast(ap.updated_on as timestamp) updated_on
        from account_program ap, program_master pm
        where ap.account_master_id = v_account_master_id
        and pm.program_master_id = ap.program_master_id;

ELSE

    OPEN OUT_CUR_ACCT_MASTER FOR
        select * from dual
        where dummy = 'nothing';

    OPEN OUT_CUR_ACCT_EMAIL FOR
        select * from dual
        where dummy = 'nothing';

    OPEN OUT_CUR_ACCT_PROGRAM FOR
        select * from dual
        where dummy = 'nothing';

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VIEW_ACTIVE_ACCOUNT;


PROCEDURE VIEW_ACCOUNT_BY_PROGRAM_ID
(
    IN_ACCOUNT_PROGRAM_ID       IN ACCOUNT_PROGRAM.ACCOUNT_PROGRAM_ID%TYPE,
    OUT_CUR_ACCT_MASTER         OUT TYPES.REF_CURSOR,
    OUT_CUR_ACCT_EMAIL          OUT TYPES.REF_CURSOR,
    OUT_CUR_ACCT_PROGRAM        OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)

 AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns all the active Account Master,
        Account Email, records for an Account. It also returns
        the specific program information for the passed in program.
        
        If the Account or Program does not exist, returns an empty list.
        
Input:
        IN_ACCOUNT_PROGRAM_ID         The program id identifying the program and account.

Output:
        OUT_CUR_ACCT_MASTER           The List of Active Account Master records for the Program.
        OUT_CUR_ACCT_EMAIL            The List of Active Account Email records for the Program.
        OUT_CUR_ACCT_PROGRAM          The List of Program records for the Program.
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/

v_account_master_id         number;
v_account_program_id         number;

CURSOR get_account_cur(p_account_program_id account_program.account_program_id%type) IS
    SELECT ap.account_master_id
    FROM account.account_program ap
    WHERE ap.account_program_id = p_account_program_id;

CURSOR get_active_account_cur(p_account_master_id account_program.account_master_id%type) IS
    SELECT ap.account_program_id
    FROM account.account_program ap
    WHERE ap.account_master_id = p_account_master_id
    AND ap.account_program_status = 'A'
    AND ap.expiration_date = (select max(ap1.expiration_date)
        FROM account.account_program ap1
        WHERE ap1.account_master_id = ap.account_master_id
        AND ap1.account_program_status = 'A'
        AND ap1.start_date <= trunc(sysdate)
        AND ap1.expiration_date >= trunc(sysdate))
    AND rownum=1;

BEGIN

OPEN get_account_cur(in_account_program_id);
FETCH get_account_cur INTO v_account_master_id;
CLOSE get_account_cur;

IF v_account_master_id IS NOT NULL THEN

    OPEN get_active_account_cur(v_account_master_id);
    FETCH get_active_account_cur INTO v_account_program_id;
    CLOSE get_active_account_cur;

    IF v_account_program_id is null THEN
        v_account_program_id := in_account_program_id;
    END IF;

    OPEN OUT_CUR_ACCT_MASTER FOR
        select account_master_id
        from account_master
        where account_master_id = v_account_master_id;

    OPEN OUT_CUR_ACCT_EMAIL FOR
        select account_email_id,
            email_address,
            active_flag
        from account_email
        where account_master_id = v_account_master_id;

    OPEN OUT_CUR_ACCT_PROGRAM FOR
        select ap.account_program_id,
            ap.program_master_id,
            pm.program_name,
            ap.account_program_status,
            ap.auto_renew_flag,
            cast(ap.start_date as timestamp) start_date,
            cast(ap.expiration_date as timestamp) expiration_date,
            ap.external_order_number,
            cast(ap.updated_on as timestamp) updated_on
        from account_program ap, program_master pm
        where ap.account_master_id = v_account_master_id
        and pm.program_master_id = ap.program_master_id
        and ap.account_program_id = v_account_program_id;

ELSE

    OPEN OUT_CUR_ACCT_MASTER FOR
        select * from dual
        where dummy = 'nothing';

    OPEN OUT_CUR_ACCT_EMAIL FOR
        select * from dual
        where dummy = 'nothing';

    OPEN OUT_CUR_ACCT_PROGRAM FOR
        select * from dual
        where dummy = 'nothing';

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VIEW_ACCOUNT_BY_PROGRAM_ID;
        
PROCEDURE GET_PROGRAM_MASTER_BY_NAME
(
    IN_PROGRAM_NAME             IN PROGRAM_MASTER.PROGRAM_NAME%TYPE,
    OUT_CUR                     OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS

BEGIN

    OPEN OUT_CUR FOR
        select program_master_id,
            program_name,
            program_description,
            active_flag,
            program_url,
            display_name
        from program_master
        where program_name = in_program_name;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END GET_PROGRAM_MASTER_BY_NAME;

PROCEDURE GET_PROGRAMS_BY_EMAIL
(
    IN_EMAIL_ADDRESS            IN ACCOUNT_EMAIL.EMAIL_ADDRESS%TYPE,
    OUT_CUR                     OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select pm.program_name,
            pm.display_name,
            ae.email_address,
            ap.account_program_status,
            ap.start_date,
            ap.expiration_date,
            pm.program_url,
            ap.external_order_number
        from account.account_email ae,
            account.account_program ap,
            account.program_master pm,
            account.account_master am
        where ae.email_address = lower(IN_EMAIL_ADDRESS)
        and ae.active_flag = 'Y'
        and ap.account_master_id = ae.account_master_id
        and pm.program_master_id = ap.program_master_id
        and am.account_master_id = ae.account_master_id
        order by ap.expiration_date desc;

END GET_PROGRAMS_BY_EMAIL;
        
PROCEDURE VIEW_PROGRAMS_BY_ORDER
(
    IN_EXTERNAL_ORDER_NUMBER    IN ACCOUNT_PROGRAM.EXTERNAL_ORDER_NUMBER%TYPE,
    OUT_CUR_ACCT_PROGRAM        OUT TYPES.REF_CURSOR,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns all the Program Records linked to the passed in 
        External Order Number        
        If the Programs do not exist, returns an empty list.
        
Input:
        IN_ACCOUNT_PROGRAM_ID         The program id identifying the program and account.

Output:
        OUT_CUR_ACCT_PROGRAM          The List of Program records for the order.
        OUT_STATUS                    'Y' on success. Else 'N'
        OUT_MESSAGE                   Error message if the status is 'N'
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR_ACCT_PROGRAM FOR
    select ap.account_program_id,
        ap.program_master_id,
        pm.program_name,
        ap.account_program_status,
        ap.auto_renew_flag,
        cast(ap.start_date as timestamp) start_date,
        cast(ap.expiration_date as timestamp) expiration_date,
        ap.external_order_number,
        cast(ap.updated_on as timestamp) updated_on,
		pm.display_name
    from account_program ap, program_master pm
    where
    pm.program_master_id = ap.program_master_id
    and ap.external_order_number = in_external_order_number;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END VIEW_PROGRAMS_BY_ORDER;        
        
PROCEDURE GET_PROGRAM_MASTER_LIST
(
    OUT_CUR                     OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select program_master_id,
            program_name,
            program_description,
            active_flag,
            program_url,
            display_name
        from program_master;

END GET_PROGRAM_MASTER_LIST;

PROCEDURE IS_ACCOUNT_EXISTS
(
    IN_EXTERNAL_ORDER_NUMBER    IN ACCOUNT_PROGRAM.EXTERNAL_ORDER_NUMBER%TYPE,
    OUT_RESULT_FLAG             OUT VARCHAR2,
    OUT_STATUS                  OUT VARCHAR2,
    OUT_MESSAGE                 OUT VARCHAR2
) AS

v_count       number;

BEGIN

OUT_RESULT_FLAG := 'N';
SELECT COUNT(*) INTO v_count FROM ACCOUNT.ACCOUNT_PROGRAM WHERE EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;
IF v_count > 0 THEN
  OUT_RESULT_FLAG := 'Y';
END IF;
OUT_STATUS := 'Y';
EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END IS_ACCOUNT_EXISTS;

PROCEDURE GET_PROGRAM_BY_ORDER_DETAIL_ID
(
    IN_ORDER_DETAIL_ID          IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
    OUT_CUR                     OUT TYPES.REF_CURSOR
   
) AS

BEGIN

    OPEN OUT_CUR FOR
        select ae.email_address,
            am.account_master_id,
            co.order_taken_by
        from account.account_email ae,
            account.account_master am,
            clean.order_details cod, 
            clean.orders co, 
            clean.email ce
        where ae.email_address = lower(ce.email_address)
        and ae.active_flag = 'Y'
        and am.account_master_id = ae.account_master_id
        and co.order_guid = cod.order_guid 
        and ce.email_id = co.email_id 
        and cod.order_detail_id = IN_ORDER_DETAIL_ID;
 
END GET_PROGRAM_BY_ORDER_DETAIL_ID;
END;
.
/
