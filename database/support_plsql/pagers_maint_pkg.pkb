CREATE OR REPLACE
PACKAGE BODY support.PAGER_MAINT_PKG AS


PROCEDURE INSERT_PAGER
(
IN_NAME                         IN PAGER.NAME%TYPE,
IN_EMAIL                        IN PAGER.EMAIL%TYPE,

OUT_PAGER_ID                    OUT PAGER.PAGER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting pager information.

Input:
        name                            varchar2
        email                           varchar2


Output:
        pager_id                        number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT PAGER_ID_SQ.NEXTVAL 
  INTO OUT_PAGER_ID 
  FROM dual;

INSERT INTO PAGER
(
        PAGER_ID,
        NAME,
        EMAIL

)
VALUES
(
        OUT_PAGER_ID,
        IN_NAME,
        IN_EMAIL
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE PAGER NUMBER';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_PAGER;

PROCEDURE INSERT_PAGER_GROUP
(
IN_NAME                         IN PAGER_GROUP.NAME%TYPE,

OUT_PAGER_GROUP_ID              OUT PAGER_GROUP.PAGER_GROUP_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting pager group information.

Input:
        name                            varchar2


Output:
        pager_group_id                  number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT PAGER_GROUP_ID_SQ.NEXTVAL 
  INTO OUT_PAGER_GROUP_ID
  FROM DUAL;

INSERT INTO PAGER_GROUP
(
        PAGER_GROUP_ID,
        NAME

)
VALUES
(
        OUT_PAGER_GROUP_ID,
        IN_NAME
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE PAGER GROUP NUMBER';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_PAGER_GROUP;

PROCEDURE INSERT_PAGER_GROUP_ENTRY
(
IN_PAGER_GROUP_ID               IN PAGER_GROUP_ENTRY.PAGER_GROUP_ID%TYPE,
IN_PAGER_ID                     IN PAGER_GROUP_ENTRY.PAGER_ID%TYPE,

OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting pager group entry information.

Input:
        pager_group_id                  number
        pager_id                        number


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN


INSERT INTO PAGER_GROUP_ENTRY
(
        PAGER_GROUP_ID,
        PAGER_ID
)
VALUES
(
        IN_PAGER_GROUP_ID,
        IN_PAGER_ID
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE PAGER GROUP ENTRY';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_PAGER_GROUP_ENTRY;

PROCEDURE INSERT_CONTACT
(
IN_RULE_ID                      IN CONTACT.RULE_ID%TYPE,
IN_PAGER_GROUP_ID               IN CONTACT.PAGER_GROUP_ID%TYPE,
IN_PAGER_ID                     IN CONTACT.PAGER_ID%TYPE,
IN_PRIORITY                     IN CONTACT.PRIORITY%TYPE,
IN_ESCALATION_MINUTES           IN CONTACT.ESCALATION_MINUTES%TYPE,

OUT_CONTACT_ID                  OUT CONTACT.CONTACT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting contact information.

Input:
        rule_id                         number  
        pager_group_id                  number
        pager_id                        number
        priority                        number
        escalation_minues               number


Output:
        contact_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT CONTACT_ID_SQ.NEXTVAL
  INTO OUT_CONTACT_ID
  FROM dual;

INSERT INTO CONTACT
(
        CONTACT_ID,
        RULE_ID,
        PAGER_GROUP_ID,
        PAGER_ID,
        PRIORITY,
        ESCALATION_MINUTES

)
VALUES
(
        OUT_CONTACT_ID,
        IN_RULE_ID,
        IN_PAGER_GROUP_ID,
        IN_PAGER_ID,
        IN_PRIORITY,
        IN_ESCALATION_MINUTES
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE CONTACT NUMBER';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_CONTACT;

PROCEDURE INSERT_RULE
(
IN_NAME                         IN RULE.NAME%TYPE,
IN_CONDITION                    IN RULE.CONDITION%TYPE,
IN_CONSEQUENCE                  IN RULE.CONSEQUENCE%TYPE,
IN_SALIENCE                     IN RULE.SALIENCE%TYPE,
IN_ACTIVE                       IN RULE.ACTIVE%TYPE,
IN_ON_HOLD                      IN RULE.ON_HOLD%TYPE,
IN_HOLD_UNTIL                   IN RULE.HOLD_UNTIL%TYPE,

OUT_RULE_ID                     OUT RULE.RULE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting rule information.

Input:
        name                            varchar2
        condition                       varchar2
        consequence                     varchar2
        salience                        number
        active                          char
        on_hold                         char
        hold_until                      date


Output:
        rule_id                         number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT RULE_ID_SQ.NEXTVAL
  INTO OUT_RULE_ID
  FROM dual;

INSERT INTO RULE
(
        RULE_ID,
        NAME,
        CONDITION,
        CONSEQUENCE,
        SALIENCE,
        ACTIVE,
        ON_HOLD,
        HOLD_UNTIL

)
VALUES
(
        OUT_RULE_ID,
        IN_NAME,
        IN_CONDITION,
        IN_CONSEQUENCE,
        IN_SALIENCE,
        IN_ACTIVE,
        IN_ON_HOLD,
        IN_HOLD_UNTIL
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE RULE NUMBER';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_RULE;


PROCEDURE UPDATE_PAGER
(
IN_PAGER_ID                     IN PAGER.PAGER_ID%TYPE,
IN_NAME                         IN PAGER.NAME%TYPE,
IN_EMAIL                        IN PAGER.EMAIL%TYPE,

OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for update pager information for the
        given id.

Input:
        pager_id                        number  
        name                            varchar2
        email                           varchar2


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  PAGER
SET     NAME = IN_NAME,
        EMAIL = IN_EMAIL
WHERE   PAGER_ID = IN_PAGER_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_PAGER;

PROCEDURE UPDATE_PAGER_GROUP
(
IN_PAGER_GROUP_ID               IN PAGER_GROUP.PAGER_GROUP_ID%TYPE,
IN_NAME                         IN PAGER_GROUP.NAME%TYPE,

OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for update pager group information for the
        given id.

Input:
        pager_group_id                  number  
        name                            varchar2


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  PAGER_GROUP
SET     NAME = IN_NAME
WHERE   PAGER_GROUP_ID = IN_PAGER_GROUP_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_PAGER_GROUP;

PROCEDURE UPDATE_CONTACT
(
IN_CONTACT_ID                   IN CONTACT.CONTACT_ID%TYPE,
IN_RULE_ID                      IN CONTACT.RULE_ID%TYPE,
IN_PAGER_GROUP_ID               IN CONTACT.PAGER_GROUP_ID%TYPE,
IN_PAGER_ID                     IN CONTACT.PAGER_ID%TYPE,
IN_PRIORITY                     IN CONTACT.PRIORITY%TYPE,
IN_ESCALATION_MINUTES           IN CONTACT.ESCALATION_MINUTES%TYPE,

OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for update contact information for the
        given id.

Input:
        contact_id                      number  
        rule_id                         number  
        pager_group_id                  number  
        pager_id                        number  
        priority                        number  
        escalation_minutes              number  


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  CONTACT
SET     RULE_ID = IN_RULE_ID,
        PAGER_GROUP_ID = IN_PAGER_GROUP_ID,
        PAGER_ID = IN_PAGER_ID,
        PRIORITY = IN_PRIORITY,
        ESCALATION_MINUTES = IN_ESCALATION_MINUTES
WHERE   CONTACT_ID = IN_CONTACT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_CONTACT;

PROCEDURE UPDATE_RULE
(
IN_RULE_ID                      IN RULE.RULE_ID%TYPE,
IN_NAME                         IN RULE.NAME%TYPE,
IN_CONDITION                    IN RULE.CONDITION%TYPE,
IN_CONSEQUENCE                  IN RULE.CONSEQUENCE%TYPE,
IN_SALIENCE                     IN RULE.SALIENCE%TYPE,
IN_ACTIVE                       IN RULE.ACTIVE%TYPE,
IN_ON_HOLD                      IN RULE.ON_HOLD%TYPE,
IN_HOLD_UNTIL                   IN RULE.HOLD_UNTIL%TYPE,

OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for update rule information for the
        given id.

Input:
        rule_id                         number  
        name                            varchar2
        condition                       varchar2
        consequence                     varchar2
        salience                        number  
        active                          char    
        on_hold                         char    
        hold_until                      date    


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  RULE 
SET     NAME = IN_NAME,
        CONDITION = IN_CONDITION,
        CONSEQUENCE = IN_CONSEQUENCE,
        SALIENCE = IN_SALIENCE,
        ACTIVE = IN_ACTIVE,
        ON_HOLD = IN_ON_HOLD,
        HOLD_UNTIL = IN_HOLD_UNTIL
WHERE   RULE_ID = IN_RULE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_RULE;


PROCEDURE DELETE_PAGER
(
IN_PAGER_ID                     IN PAGER.PAGER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Delete from the pager table based on pager ID.

Input:
        pager_id                        number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

DELETE FROM PAGER WHERE PAGER_ID = IN_PAGER_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_PAGER;

PROCEDURE DELETE_PAGER_GROUP
(
IN_PAGER_GROUP_ID               IN PAGER_GROUP.PAGER_GROUP_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Delete from the pager group table based on pager group ID.

Input:
        pager_group_id                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

DELETE FROM PAGER_GROUP WHERE PAGER_GROUP_ID = IN_PAGER_GROUP_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_PAGER_GROUP;

PROCEDURE DELETE_PAGER_GROUP_ENTRY
(
IN_PAGER_GROUP_ID               IN PAGER_GROUP_ENTRY.PAGER_GROUP_ID%TYPE,
IN_PAGER_ID                     IN PAGER_GROUP_ENTRY.PAGER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Delete from the pager group entry table based on pager ID and
	pager group ID.

Input:
        pager_id                        number
        pager_group_id                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

DELETE FROM PAGER_GROUP_ENTRY WHERE PAGER_ID = IN_PAGER_ID AND
        PAGER_GROUP_ID = IN_PAGER_GROUP_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_PAGER_GROUP_ENTRY;

PROCEDURE DELETE_CONTACT
(
IN_CONTACT_ID                   IN CONTACT.CONTACT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Delete from the contact table based on contact ID.

Input:
        contact_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

DELETE FROM CONTACT WHERE CONTACT_ID = IN_CONTACT_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_CONTACT;

PROCEDURE DELETE_RULE
(
IN_RULE_ID                      IN RULE.RULE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Delete from the rule table based on rule ID.

Input:
        rule_id                         number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

DELETE FROM RULE WHERE RULE_ID = IN_RULE_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_RULE;

PROCEDURE FIND_RULE_BY_ID
(
IN_RULE_ID                          IN RULE.RULE_ID%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the rule identified by the unique rule id

Input:
        rule_id                          rule id of rule to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT RULE_ID, NAME, CONDITION, CONSEQUENCE, 
         SALIENCE, ACTIVE, ON_HOLD, HOLD_UNTIL 
  FROM RULE 
  WHERE RULE_ID = IN_RULE_ID;

END FIND_RULE_BY_ID;

PROCEDURE FIND_RULE_ACTIVE
(
IN_ACTIVE                           IN RULE.ACTIVE%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find all the rules identified by the active flag.

Input:
        active                          active status of rules to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT RULE_ID, NAME, CONDITION, CONSEQUENCE, 
         SALIENCE, ACTIVE, ON_HOLD, HOLD_UNTIL 
  FROM RULE 
  WHERE ACTIVE = IN_ACTIVE;

END FIND_RULE_ACTIVE;

PROCEDURE FIND_RULE_EXPIRE_HOLD
(
IN_ON_HOLD                          IN RULE.ON_HOLD%TYPE,
IN_HOLD_UNTIL                       IN RULE.HOLD_UNTIL%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find all the rules that are on hold and the hold time has expired.

Input:
        on_hold                          on_hold status of the row to look for
        hold_until                       time to check to being past

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT RULE_ID, NAME, CONDITION, CONSEQUENCE, 
         SALIENCE, ACTIVE, ON_HOLD, HOLD_UNTIL 
  FROM RULE 
  WHERE ON_HOLD = IN_ON_HOLD
  AND HOLD_UNTIL < IN_HOLD_UNTIL;

END FIND_RULE_EXPIRE_HOLD;

PROCEDURE FIND_RULE_COUNT
(
OUT_COUNT                       OUT NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        Count all the rules.

Input:

Output:
        count                          count of all the rule rows
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

SELECT COUNT(*)
INTO OUT_COUNT
FROM RULE; 

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FIND_RULE_COUNT;

PROCEDURE FIND_PAGER_COUNT
(
OUT_COUNT                       OUT NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        Count all the pagers.

Input:

Output:
        count                          count of all the pager rows
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

SELECT COUNT(*)
INTO OUT_COUNT
FROM PAGER; 

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FIND_PAGER_COUNT;

PROCEDURE FIND_CONTACT_COUNT
(
OUT_COUNT                       OUT NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        Count all the contacts.

Input:

Output:
        count                          count of all the contacts rows
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

SELECT COUNT(*)
INTO OUT_COUNT
FROM CONTACT; 

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FIND_CONTACT_COUNT;

PROCEDURE FIND_PAGER_GROUP_COUNT
(
OUT_COUNT                       OUT NUMBER,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        Count all the pager group.

Input:

Output:
        count                          count of all the pager group rows
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


BEGIN

SELECT COUNT(*)
INTO OUT_COUNT
FROM PAGER_GROUP; 

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END FIND_PAGER_GROUP_COUNT;

PROCEDURE FIND_CONTACT_BY_ID
(
IN_CONTACT_ID                       IN CONTACT.CONTACT_ID%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the contact identified by the unique rule id

Input:
        contact_id                      contact id of contact to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT CONTACT_ID, RULE_ID, PAGER_GROUP_ID, PAGER_ID, PRIORITY,
         ESCALATION_MINUTES 
  FROM CONTACT 
  WHERE CONTACT_ID = IN_CONTACT_ID;

END FIND_CONTACT_BY_ID;

PROCEDURE FIND_CONTACT_BY_RULE_ID
(
IN_RULE_ID                       IN CONTACT.RULE_ID%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find all the contacts associated with a rule.

Input:
        rule_id                         rule id of contacts to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT CONTACT_ID, RULE_ID, PAGER_GROUP_ID, PAGER_ID, PRIORITY,
         ESCALATION_MINUTES 
  FROM CONTACT 
  WHERE RULE_ID = IN_RULE_ID;

END FIND_CONTACT_BY_RULE_ID;

PROCEDURE FIND_PAGER_BY_ID
(
IN_PAGER_ID                         IN PAGER.PAGER_ID%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the pager identified by the unique pager id

Input:
        pager_id                          pager id of pager to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT PAGER_ID, NAME, EMAIL
  FROM PAGER 
  WHERE PAGER_ID = IN_PAGER_ID;

END FIND_PAGER_BY_ID;

PROCEDURE FIND_PAGER_ALL
(
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find all the pagers.

Input:

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT PAGER_ID, NAME, EMAIL 
  FROM PAGER; 

END FIND_PAGER_ALL;

PROCEDURE FIND_PAGER_GROUP_BY_ID
(
IN_PAGER_GROUP_ID                   IN PAGER_GROUP.PAGER_GROUP_ID%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the pager group identified by the unique pager group id

Input:
        pager_group_id                  pager group id of pager group to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT PAGER_GROUP_ID, NAME
  FROM PAGER_GROUP 
  WHERE PAGER_GROUP_ID = IN_PAGER_GROUP_ID;

END FIND_PAGER_GROUP_BY_ID;

PROCEDURE FIND_PAGER_GROUP_ALL
(
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find all the pagers groups.

Input:

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT PAGER_GROUP_ID, NAME 
  FROM PAGER_GROUP; 

END FIND_PAGER_GROUP_ALL;


PROCEDURE FIND_PAGER_GROUP_BY_PAGE
(
IN_START_ROW              IN NUMERIC,
IN_END_ROW                IN NUMERIC,
OUT_CUR                   OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the list of pagers that correspond to the first result
		and size parameters.  The count of contacts is returned.

Input:
        first_result              row number of the first result
        result_size               max number of rows to return

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
    SELECT * FROM (
        SELECT ROWNUM RN, pager_Group_id, NAME, 
            (SELECT COUNT(*) 
             FROM contact 
             WHERE contact.pager_group_id = pg.pager_group_id) COUNT
        FROM pager_group pg
        WHERE ROWNUM <= IN_END_ROW) r
    WHERE r.rn >= IN_START_ROW;

	
END FIND_PAGER_GROUP_BY_PAGE;

PROCEDURE FIND_PAGER_BY_PAGE
(
IN_START_ROW              IN NUMERIC,
IN_END_ROW                IN NUMERIC,
OUT_CUR                   OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the list of pagers that correspond to the first result
		and size parameters.  The count of contacts and groups is returned.

Input:
        first_result              row number of the first result
        result_size               max number of rows to return

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
    SELECT * FROM (
        SELECT ROWNUM RN, pager_id, NAME, EMAIL, 
            (SELECT COUNT(*) 
             FROM contact 
             WHERE contact.pager_id = pg.pager_id) CONTACT_COUNT,
            (SELECT COUNT(*) 
             FROM pager_group_entry pge 
             WHERE pge.pager_id = pg.pager_id) GROUP_COUNT
        FROM pager pg
        WHERE ROWNUM <= IN_END_ROW) r
    WHERE r.rn >= IN_START_ROW;
	
END FIND_PAGER_BY_PAGE;

PROCEDURE FIND_RULE_BY_PAGE
(
IN_START_ROW              IN NUMERIC,
IN_END_ROW                IN NUMERIC,
OUT_CUR                   OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the list of rules that correspond to the first result
		and size parameters.  

Input:
        first_result              row number of the first result
        result_size               max number of rows to return

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
    SELECT * FROM (
        SELECT ROWNUM RN, RULE_ID, NAME, CONDITION, CONSEQUENCE,
		       SALIENCE, ACTIVE, ON_HOLD, HOLD_UNTIL
        FROM RULE 
        WHERE ROWNUM <= IN_END_ROW) r
    WHERE r.rn >= IN_START_ROW;
	
END FIND_RULE_BY_PAGE;

PROCEDURE FIND_PAGER_BY_GROUP
(
IN_PAGER_GROUP_ID                   IN PAGER_GROUP.PAGER_GROUP_ID%TYPE,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Find the pagers identified by the pager group id

Input:
        pager_group_id                  pager group id of pagers to get

Output:
        cur                             cursor containg the rows
-----------------------------------------------------------------------------*/


BEGIN

OPEN OUT_CUR FOR
  SELECT P.PAGER_ID, NAME, EMAIL
  FROM PAGER P, PAGER_GROUP_ENTRY PGE  
  WHERE P.PAGER_ID = PGE.PAGER_ID
  AND PGE.pager_group_id = IN_PAGER_GROUP_ID;
  
END FIND_PAGER_BY_GROUP;

PROCEDURE ADD_PAGER_TO_GROUP
(
IN_PAGER_GROUP_ID               IN PAGER_GROUP.PAGER_GROUP_ID%TYPE,
IN_PAGER_ID                     IN PAGER.PAGER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure adds a pager to a pager group.

Input:
        pager_id                        number
        pager_group_id                  number


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO PAGER_GROUP_ENTRY
(
        PAGER_ID,
        PAGER_GROUP_ID
)
VALUES
(
        IN_PAGER_ID,
        IN_PAGER_GROUP_ID
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE PAGER IN GROUP';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END ADD_PAGER_TO_GROUP;

PROCEDURE REMOVE_PAGER_FROM_GROUP
(
IN_PAGER_GROUP_ID               IN PAGER_GROUP.PAGER_GROUP_ID%TYPE,
IN_PAGER_ID                     IN PAGER.PAGER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure removes a pager from a pager group.

Input:
        pager_id                        number
        pager_group_id                  number


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM PAGER_GROUP_ENTRY
WHERE PAGER_ID = IN_PAGER_ID
AND PAGER_GROUP_ID = IN_PAGER_GROUP_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE PAGER IN GROUP';
END;    -- end duplicate error

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_PAGER_FROM_GROUP;

END PAGER_MAINT_PKG;
/
