create or replace 
PACKAGE BODY  TRACKING.PDB_MASS_EDIT AS                     

 PROCEDURE INSERT_MASS_EDIT_HEADER
 (
  IN_FILE_NAME                  IN TRACKING.MASS_EDIT_HEADER.FILE_NAME%TYPE,
  IN_FILE_STATUS                IN TRACKING.MASS_EDIT_HEADER.FILE_STATUS%TYPE,
  IN_TOTAL_RECORDS              IN TRACKING.MASS_EDIT_HEADER.TOTAL_RECORDS%TYPE,
  IN_CREATED_BY                 IN TRACKING.MASS_EDIT_HEADER.CREATED_BY%TYPE,
  IN_UPDATED_BY                 IN TRACKING.MASS_EDIT_HEADER.UPDATED_BY%TYPE,
--IN_ERROR_FILE                 IN TRACKING.MASS_EDIT_HEADER.ERROR_FILE%TYPE,
  OUT_MASS_EDIT_HEADER_ID       OUT TRACKING.MASS_EDIT_HEADER.MASS_EDIT_HEADER_ID%TYPE, 
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
 ) AS

 /*-----------------------------------------------------------------------------
  This stored procedure will insert a new record into the MASS_EDIT_HEADER
  table.

  Input:
        IN_FILE_NAME                    varchar2
        IN_FILE_STATUS                  varchar2
        IN_TOTAL_RECORDS                number
        IN_CREATED_BY                   varchar2
        IN_UPDATED_BY                   varchar2
        IN_ERROR_FILE                   clob
         
  Output:
        mass_edit_header_id             id of newly inserted row
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
  
 BEGIN
    
   BEGIN
    INSERT INTO TRACKING.MASS_EDIT_HEADER (
      MASS_EDIT_HEADER_ID, 
      FILE_NAME, 
      FILE_UPLOAD_DATE, 
      FILE_STATUS, 
      TOTAL_RECORDS, 
      CREATED_ON, 
      CREATED_BY, 
      UPDATED_BY, 
      UPDATED_ON 
    --  ERROR_FILE
    ) VALUES (
      TRACKING.MASS_EDIT_HEADER_ID_SQ.nextval,
      IN_FILE_NAME,
      sysdate,
      IN_FILE_STATUS,
      IN_TOTAL_RECORDS,
       sysdate,
      IN_CREATED_BY,
      IN_UPDATED_BY,
       sysdate
    );
    OUT_MASS_EDIT_HEADER_ID := TRACKING.MASS_EDIT_HEADER_ID_SQ.CURRVAl;
    OUT_STATUS := 'Y';
   END; 
   
   EXCEPTION WHEN OTHERS THEN
     BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;    -- end exception block

 
 END INSERT_MASS_EDIT_HEADER;
 
 PROCEDURE INSERT_MASS_EDIT_DETAIL
 (
  IN_MASS_EDIT_HEADER_ID        IN TRACKING.MASS_EDIT_DETAIL.MASS_EDIT_HEADER_ID%TYPE,
  IN_UPSELL_MASTER_ID           IN TRACKING.MASS_EDIT_DETAIL.UPSELL_MASTER_ID%TYPE,
  IN_UPSELL_DETAIL_ID           IN TRACKING.MASS_EDIT_DETAIL.UPSELL_DETAIL_ID%TYPE,
  IN_STATUS	                    IN TRACKING.MASS_EDIT_DETAIL.STATUS%TYPE,
  IN_ERROR_MSG                  IN TRACKING.MASS_EDIT_DETAIL.ERROR_MSG%TYPE,
  IN_CREATED_BY                 IN TRACKING.MASS_EDIT_DETAIL.CREATED_BY%TYPE,
  IN_UPDATED_BY                 IN TRACKING.MASS_EDIT_DETAIL.UPDATED_BY%TYPE,
  IN_PRODUCT_ID                 IN TRACKING.MASS_EDIT_DETAIL.PRODUCT_ID%TYPE,
  OUT_MASS_EDIT_DETAIL_ID       OUT TRACKING.MASS_EDIT_DETAIL.MASS_EDIT_DETAIL_ID%TYPE,
  OUT_STATUS                    OUT VARCHAR,
  OUT_MESSAGE                   OUT VARCHAR
 ) AS

 /*-----------------------------------------------------------------------------
  This stored procedure will insert a new record into the MASS_EDIT_DETAIL
  table.

  Input:
        MASS_EDIT_HEADER_ID           number 
	MASS_EDIT_DETAIL_ID           number 
	UPSELL_MASTER_ID              varchar2 
	UPSELL_DETAIL_ID              varchar2
	STATUS                        varchar2
	ERROR_MSG                     varchar2
	CREATED_BY                    varchar2
	UPDATED_BY                    varchar2
	PRODUCT_ID                    varchar2
         
  Output:
        mass edit detail id     id of newly inserted row
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)
-----------------------------------------------------------------------------*/
  
 BEGIN
    
   BEGIN
    INSERT INTO TRACKING.MASS_EDIT_DETAIL (
      MASS_EDIT_HEADER_ID,
      MASS_EDIT_DETAIL_ID, 
      UPSELL_MASTER_ID,
      UPSELL_DETAIL_ID,
      STATUS,
      ERROR_MSG,
      CREATED_ON,
      CREATED_BY,
      UPDATED_BY,
      UPDATED_ON,
      PRODUCT_ID
    ) VALUES (
      IN_MASS_EDIT_HEADER_ID,
      TRACKING.MASS_EDIT_DETAIL_ID_SQ.nextval,
      IN_UPSELL_MASTER_ID,
      IN_UPSELL_DETAIL_ID,
      IN_STATUS,
      IN_ERROR_MSG,
      sysdate,
      IN_CREATED_BY,
      IN_UPDATED_BY,
      sysdate,
      IN_PRODUCT_ID     
    );
    OUT_MASS_EDIT_DETAIL_ID := TRACKING.MASS_EDIT_DETAIL_ID_SQ.CURRVAl;
    OUT_STATUS := 'Y';
   END; 
   
   EXCEPTION WHEN OTHERS THEN
     BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;    -- end exception block

 
 END INSERT_MASS_EDIT_DETAIL;	
 
 PROCEDURE UPDATE_MASS_EDIT_HEADER
  (
   IN_FILE_STATUS                IN TRACKING.MASS_EDIT_HEADER.FILE_STATUS%TYPE,
   IN_TOTAL_RECORDS              IN TRACKING.MASS_EDIT_HEADER.TOTAL_RECORDS%TYPE,   
   IN_UPDATED_BY                 IN TRACKING.MASS_EDIT_HEADER.UPDATED_BY%TYPE,
   IN_MASS_EDIT_HEADER_ID        IN TRACKING.MASS_EDIT_HEADER.MASS_EDIT_HEADER_ID%TYPE, 
   OUT_STATUS                    OUT VARCHAR,
   OUT_MESSAGE                   OUT VARCHAR
  ) AS
 
  /*-----------------------------------------------------------------------------
   This stored procedure will insert a new record into the MASS_EDIT_HEADER
   table.
 
   Input:
         IN_FILE_STATUS                  varchar2
         IN_TOTAL_RECORDS                number
         IN_UPDATED_BY                   varchar2
         IN_ERROR_FILE                   clob
         IN_MASS_EDIT_HEADER             number
          
   Output:
         status                          varchar2 (Y or N)
         message                         varchar2 (error message)
 -----------------------------------------------------------------------------*/
   
  BEGIN
     
    BEGIN
     UPDATE TRACKING.MASS_EDIT_HEADER 
       SET  
           FILE_STATUS       = IN_FILE_STATUS, 
           TOTAL_RECORDS     = IN_TOTAL_RECORDS,          
           UPDATED_BY        = IN_UPDATED_BY, 
           UPDATED_ON        = sysdate 
       WHERE MASS_EDIT_HEADER_ID = IN_MASS_EDIT_HEADER_ID;
       
     OUT_STATUS := 'Y';
    END; 
    
    EXCEPTION WHEN OTHERS THEN
      BEGIN
         OUT_STATUS := 'N';
         OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      END;    -- end exception block
 
  
 END UPDATE_MASS_EDIT_HEADER;
 
 
 PROCEDURE GET_MASS_EDIT_FILE_STATUS
  (
    OUT_CURSOR           OUT TYPES.REF_CURSOR
  )
  AS
  BEGIN
    OPEN OUT_CURSOR FOR
SELECT EH.MASS_EDIT_HEADER_ID,
  EH.CREATED_BY,
  TO_CHAR(EH.FILE_UPLOAD_DATE,'mm/dd/yyyy HH:MI:SS') FILE_UPLOAD_DATE ,
  EH.FILE_NAME,
  EH.FILE_STATUS,
  EH.TOTAL_RECORDS, 
  (SELECT COUNT(*)
  FROM TRACKING.MASS_EDIT_DETAIL ED
  WHERE ED.STATUS           = 'Failure'
  AND ED.MASS_EDIT_HEADER_ID=EH.MASS_EDIT_HEADER_ID
  ) RECORDSFAILED,  
  (SELECT COUNT(*)
  FROM TRACKING.MASS_EDIT_DETAIL ED
  WHERE ED.STATUS           = 'Success'
  AND ED.MASS_EDIT_HEADER_ID=EH.MASS_EDIT_HEADER_ID
  ) RECORDSSUCCESS,
   (SELECT (SUBSTR(FILE_NAME,0,25)
    || '_'
    || MASS_EDIT_HEADER_ID
    || '.xlsx')
  FROM TRACKING.MASS_EDIT_HEADER
  WHERE file_STATUS      = 'Complete'
  AND MASS_EDIT_HEADER_ID=EH.MASS_EDIT_HEADER_ID
  ) ERROR_FILE_NAME
FROM TRACKING.MASS_EDIT_HEADER EH
ORDER BY EH.FILE_UPLOAD_DATE DESC;

END GET_MASS_EDIT_FILE_STATUS;

END PDB_MASS_EDIT;
.
/