CREATE OR REPLACE
PACKAGE BODY TRACKING.PAYMENT_TRACKING_PKG AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a new payment tracking 
        record.

Output:
        payment_tracking_id             varchar2
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PROCEDURE INSERT_PAYMENT_TRACKING
(
IN_CLIENT_NAME                          IN PAYMENT_TRACKING.CLIENT_NAME%TYPE,
IN_CREATED_BY                           IN PAYMENT_TRACKING.CREATED_BY%TYPE,
IN_ERROR_CODE                           IN PAYMENT_TRACKING.ERROR_CODE%TYPE,
IN_ERROR_MESSAGE                        IN PAYMENT_TRACKING.ERROR_MESSAGE%TYPE,
IN_PAYMENT_TYPE                         IN PAYMENT_TRACKING.PAYMENT_TYPE%TYPE,
IN_REQUEST                              IN PAYMENT_TRACKING.REQUEST%TYPE,
IN_RESPONSE                             IN PAYMENT_TRACKING.RESPONSE%TYPE,
IN_RESPONSE_CODE                        IN PAYMENT_TRACKING.RESPONSE_CODE%TYPE,
IN_RESPONSE_MESSAGE                     IN PAYMENT_TRACKING.RESPONSE_MESSAGE%TYPE,
IN_SOURCE_CODE                          IN PAYMENT_TRACKING.SOURCE_CODE%TYPE,
IN_TRANSACTION_ID                       IN PAYMENT_TRACKING.TRANSACTION_ID%TYPE,
IN_TRANSACTION_TYPE                     IN PAYMENT_TRACKING.TRANSACTION_TYPE%TYPE,
IN_UPDATED_BY                           IN PAYMENT_TRACKING.UPDATED_BY%TYPE,
OUT_PAYMENT_TRACKING_ID                OUT VARCHAR2,
OUT_STATUS                             OUT VARCHAR2,
OUT_MESSAGE                            OUT VARCHAR2
)
AS

BEGIN

  SELECT TRACKING.PAYMENT_TRACKING_SEQ.NEXTVAL 
    INTO OUT_PAYMENT_TRACKING_ID
    FROM DUAL;


  INSERT INTO TRACKING.PAYMENT_TRACKING
  (
    CLIENT_NAME,
    CREATED_BY,
    CREATED_ON,
    ERROR_CODE,
    ERROR_MESSAGE,
    PAYMENT_TRACKING_ID,
    PAYMENT_TYPE,
    REQUEST,
    RESPONSE,
    RESPONSE_CODE,
    RESPONSE_MESSAGE,
    SOURCE_CODE,
    TRANSACTION_ID,
    TRANSACTION_TYPE,
    UPDATED_BY,
    UPDATED_ON
  )
  VALUES
  (
    IN_CLIENT_NAME,
    IN_CREATED_BY,
    sysdate,
    IN_ERROR_CODE,
    IN_ERROR_MESSAGE,
    OUT_PAYMENT_TRACKING_ID,
    IN_PAYMENT_TYPE,
    IN_REQUEST,
    IN_RESPONSE,
    IN_RESPONSE_CODE,
    IN_RESPONSE_MESSAGE,
    IN_SOURCE_CODE,
    IN_TRANSACTION_ID,
    IN_TRANSACTION_TYPE,
    IN_UPDATED_BY,
    sysdate
  );

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PAYMENT_TRACKING;



/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updated an existing payment tracking 
        record.  Note that it will only update the columns for which a non-NULL
        value was passsed.  

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PROCEDURE UPDATE_PAYMENT_TRACKING
(
IN_CLIENT_NAME                          IN PAYMENT_TRACKING.CLIENT_NAME%TYPE,
IN_ERROR_CODE                           IN PAYMENT_TRACKING.ERROR_CODE%TYPE,
IN_ERROR_MESSAGE                        IN PAYMENT_TRACKING.ERROR_MESSAGE%TYPE,
IN_PAYMENT_TRACKING_ID                  IN PAYMENT_TRACKING.PAYMENT_TRACKING_ID%TYPE,
IN_PAYMENT_TYPE                         IN PAYMENT_TRACKING.PAYMENT_TYPE%TYPE,
IN_REQUEST                              IN PAYMENT_TRACKING.REQUEST%TYPE,
IN_RESPONSE                             IN PAYMENT_TRACKING.RESPONSE%TYPE,
IN_RESPONSE_CODE                        IN PAYMENT_TRACKING.RESPONSE_CODE%TYPE,
IN_RESPONSE_MESSAGE                     IN PAYMENT_TRACKING.RESPONSE_MESSAGE%TYPE,
IN_SOURCE_CODE                          IN PAYMENT_TRACKING.SOURCE_CODE%TYPE,
IN_TRANSACTION_ID                       IN PAYMENT_TRACKING.TRANSACTION_ID%TYPE,
IN_TRANSACTION_TYPE                     IN PAYMENT_TRACKING.TRANSACTION_TYPE%TYPE,
IN_UPDATED_BY                           IN PAYMENT_TRACKING.UPDATED_BY%TYPE,
OUT_STATUS                             OUT VARCHAR2,
OUT_MESSAGE                            OUT VARCHAR2
)
AS 
BEGIN

  UPDATE TRACKING.PAYMENT_TRACKING
     SET CLIENT_NAME         = nvl (IN_CLIENT_NAME, CLIENT_NAME), 
         ERROR_CODE          = nvl (IN_ERROR_CODE, ERROR_CODE), 
         ERROR_MESSAGE       = nvl (IN_ERROR_MESSAGE, ERROR_MESSAGE), 
         PAYMENT_TYPE        = nvl (IN_PAYMENT_TYPE, PAYMENT_TYPE), 
         REQUEST             = nvl (IN_REQUEST, REQUEST), 
         RESPONSE            = nvl (IN_RESPONSE, RESPONSE), 
         RESPONSE_CODE       = nvl (IN_RESPONSE_CODE, RESPONSE_CODE), 
         RESPONSE_MESSAGE    = nvl (IN_RESPONSE_MESSAGE, RESPONSE_MESSAGE), 
         SOURCE_CODE         = nvl (IN_SOURCE_CODE, SOURCE_CODE), 
         TRANSACTION_ID      = nvl (IN_TRANSACTION_ID, TRANSACTION_ID), 
         TRANSACTION_TYPE    = nvl (IN_TRANSACTION_TYPE, TRANSACTION_TYPE), 
         UPDATED_BY          = nvl (IN_UPDATED_BY, UPDATED_BY), 
         UPDATED_ON          = sysdate
   WHERE PAYMENT_TRACKING_ID = IN_PAYMENT_TRACKING_ID;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPDATE_PAYMENT_TRACKING;


END;
.
/
