CREATE OR REPLACE
PACKAGE BODY tracking.ACTION_MAINT_PKG
AS

PROCEDURE INSERT_ACTION_HEADER
(
IN_DEFECT                       IN ACTION_HEADER.DEFECT%TYPE,
IN_DESCRIPTION                  IN ACTION_HEADER.DESCRIPTION%TYPE,
IN_REQUESTED_BY                 IN ACTION_HEADER.REQUESTED_BY%TYPE,
IN_CREATED_BY                   IN ACTION_HEADER.CREATED_BY%TYPE,
OUT_ACTION_HEADER_ID            OUT ACTION_HEADER.ACTION_HEADER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting an action header and
        returning an action header id.

Input:
        defect                          number
        description                     varchar2
        requested_by                    varchar2
        created_by                      varchar2

Output:
        action header id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

  SELECT ACTION_HEADER_SQ.NEXTVAL INTO out_action_header_id FROM DUAL;

  INSERT INTO action_header (
         action_header_id,
         defect,
         description,
         requested_by,
         created_on,
         created_by)
  VALUES (
  	out_action_header_id,
  	in_defect,
  	in_description,
  	in_requested_by,
  	SYSDATE,
  	in_created_by);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED IN INSERT_ACTION_HEADER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      ROLLBACK;
    END;

END INSERT_ACTION_HEADER;


PROCEDURE INSERT_ACTION_HEADER
(
IN_DEFECT                       IN ACTION_HEADER.DEFECT%TYPE,
IN_DESCRIPTION                  IN ACTION_HEADER.DESCRIPTION%TYPE,
IN_REQUESTED_BY                 IN ACTION_HEADER.REQUESTED_BY%TYPE,
OUT_ACTION_HEADER_ID            OUT ACTION_HEADER.ACTION_HEADER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting an action header,
        setting the created_by field to 'SYS', and returning an action
        header id.

Input:
        defect                          number
        description                     varchar2
        requested_by                    varchar2

Output:
        action header id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

  SELECT ACTION_HEADER_SQ.NEXTVAL INTO out_action_header_id FROM DUAL;

  INSERT INTO action_header (
         action_header_id,
         defect,
         description,
         requested_by,
         created_on,
         created_by)
  VALUES (
  	out_action_header_id,
  	in_defect,
  	in_description,
  	in_requested_by,
  	SYSDATE,
  	'SYS');

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED IN INSERT_ACTION_HEADER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      ROLLBACK;
    END;

END INSERT_ACTION_HEADER;


PROCEDURE INSERT_ACTION_DETAIL
(
IN_ACTION_HEADER_ID             IN ACTION_DETAIL.ACTION_HEADER_ID%TYPE,
IN_IDENTIFIER                   IN ACTION_DETAIL.IDENTIFIER%TYPE,
IN_IDENTIFIER_COLUMN_NAME       IN ACTION_DETAIL.IDENTIFIER_COLUMN_NAME%TYPE,
IN_CREATED_BY                   IN ACTION_DETAIL.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting an action detail.

Input:
        action header id                number
        identifier                      varchar2
        identifier_column_name          varchar2
        created_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_action_detail_id NUMBER;

BEGIN

  SELECT ACTION_DETAIL_SQ.NEXTVAL INTO v_action_detail_id FROM DUAL;

  INSERT INTO action_detail (
         action_detail_id,
         action_header_id,
         identifier,
         identifier_column_name,
         created_on,
         created_by)
  VALUES (
  	v_action_detail_id,
        in_action_header_id,
        in_identifier,
        in_identifier_column_name,
        SYSDATE,
        in_created_by);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED IN INSERT_ACTION_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      ROLLBACK;
    END;

END INSERT_ACTION_DETAIL;


PROCEDURE INSERT_ACTION_DETAIL
(
IN_ACTION_HEADER_ID             IN ACTION_DETAIL.ACTION_HEADER_ID%TYPE,
IN_IDENTIFIER                   IN ACTION_DETAIL.IDENTIFIER%TYPE,
IN_IDENTIFIER_COLUMN_NAME       IN ACTION_DETAIL.IDENTIFIER_COLUMN_NAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting an action detail and 
        setting the created_by field to 'SYS'.

Input:
        action header id                number
        identifier                      varchar2
        identifier_column_name          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_action_detail_id NUMBER;

BEGIN

  SELECT ACTION_DETAIL_SQ.NEXTVAL INTO v_action_detail_id FROM DUAL;

  INSERT INTO action_detail (
         action_detail_id,
         action_header_id,
         identifier,
         identifier_column_name,
         created_on,
         created_by)
  VALUES (
  	v_action_detail_id,
        in_action_header_id,
        in_identifier,
        in_identifier_column_name,
        SYSDATE,
        'SYS');

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED IN INSERT_ACTION_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      ROLLBACK;
    END;

END INSERT_ACTION_DETAIL;

END;
.
/
