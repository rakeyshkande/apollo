CREATE OR REPLACE VIEW PAS.PAS_FLORIST_PRODUCT_ZIP_DT_VW (PRODUCT_ID, DELIVERY_DATE, ZIP_CODE, CODIFICATION_ID,
CUTOFF_TIME, ADDON_DAYS_QTY) AS
SELECT pp.product_id, pz.delivery_date, pz.zip_code_id, pp.codification_id,
          (pz.cutoff_time + pt.delta_to_cst_hhmm) AS cutoff_time, pp.addon_days_qty
    FROM pas.pas_florist_zipcode_dt pz, pas.pas_product_dt pp, ftd_apps.zip_code zc, ftd_apps.state_master sm,
          pas.pas_timezone_dt pt, pas.pas_country_dt pc
    WHERE 1 = 1
          AND pt.time_zone_code = sm.TIME_ZONE
          AND pt.delivery_date = pz.delivery_date
          AND sm.state_master_id = zc.state_id
          AND zc.zip_code_id = pz.zip_code_id
          AND pt.delivery_date = pp.delivery_date
          AND pc.delivery_date = pz.delivery_date
          AND pc.country_id = DECODE(sm.country_code, NULL, 'US', 'CAN', 'CA', sm.country_code)
          AND pc.delivery_available_flag = 'Y'
          AND pp.delivery_available_flag = 'Y'
          AND pp.ship_method_florist = 'Y'
          AND pz.status_code = 'Y'
          AND pp.product_type IN ('FLORAL', 'SDFC', 'SDG')
          AND (pp.codification_id is null OR pp.codification_id = '')
  UNION
    SELECT pp.product_id, pz.delivery_date, pz.zip_code_id, pp.codification_id,
          (pz.cutoff_time + pt.delta_to_cst_hhmm) AS cutoff_time, pp.addon_days_qty
    FROM pas.pas_florist_zipcode_dt pz, pas.pas_product_dt pp, ftd_apps.zip_code zc, ftd_apps.state_master sm,
          pas.pas_timezone_dt pt, pas.pas_country_dt pc
    WHERE 1 = 1
          AND pt.time_zone_code = sm.TIME_ZONE
          AND pt.delivery_date = pz.delivery_date
          AND sm.state_master_id = zc.state_id
          AND zc.zip_code_id = pz.zip_code_id
          AND pt.delivery_date = pp.delivery_date
          AND pc.delivery_date = pz.delivery_date
          AND pc.country_id = DECODE(sm.country_code, NULL, 'US', 'CAN', 'CA', sm.country_code)
          AND pc.delivery_available_flag = 'Y'
          AND pp.delivery_available_flag = 'Y'
          AND pp.ship_method_florist = 'Y'
          AND pz.status_code = 'Y'
          AND (select 'Y' from ftd_apps.florist_codifications fc, ftd_apps.florist_zips fz,ftd_apps.florist_suspends fs,
                   ftd_apps.florist_master fm, frp.global_parms gp
               where fc.codification_id = pp.codification_id
               and fz.florist_id = fc.florist_id
               and fz.zip_code = pz.zip_code_id
               and fs.florist_id (+) = fc.florist_id
               and fm.florist_id = fc.florist_id
               and fm.status <> 'Inactive'
               and gp.context = 'UPDATE_FLORIST_SUSPEND'
               and gp.name = 'BUFFER_MINUTES'
               and ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fz.florist_id, pz.delivery_date) = 'Y'
               and ftd_apps.florist_query_pkg.is_florist_open_in_eros(fz.florist_id) = 'Y'
               AND (   (pz.delivery_date NOT BETWEEN TRUNC(fc.block_start_date) AND fc.block_end_date)
                    OR (fc.block_start_date IS NULL)  )
               AND (   (pz.delivery_date NOT BETWEEN TRUNC(fz.block_start_date) AND fz.block_end_date)
                    OR (fz.block_start_date IS NULL)  )
               AND not exists (select 'Y' from ftd_apps.florist_blocks fb
                   where fb.florist_id = fz.florist_id
                   and pz.delivery_date between trunc(fb.block_start_date) and nvl(fb.block_end_date, pz.delivery_date))
               AND (   (
                   (   (pz.delivery_date = trunc(sysdate) and sysdate not between fs.suspend_start_date - (gp.value/1440) and fs.suspend_end_date)
                  OR (pz.delivery_date > sysdate and pz.delivery_date NOT BETWEEN fs.suspend_start_date AND fs.suspend_end_date
                          and sysdate not between fs.suspend_start_date - (gp.value/1440) and fs.suspend_end_date))
                      OR (fs.suspend_type <> 'G' and fm.super_florist_flag ='Y'))
                   OR (fs.suspend_start_date IS NULL)  )
               AND (   ((pz.delivery_date - trunc(sysdate)) > pp.addon_days_qty)
                    OR (((pz.delivery_date - trunc(sysdate)) = pp.addon_days_qty)
                       AND (fz.cutoff_time + pt.delta_to_cst_hhmm) > to_char(sysdate,'HH24mm')))
               and rownum=1) = 'Y';
