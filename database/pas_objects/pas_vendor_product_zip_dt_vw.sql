CREATE OR REPLACE VIEW PAS.PAS_VENDOR_PRODUCT_ZIP_DT_VW (
  VENDOR_ID,
  PRODUCT_ID,
  ZIP_CODE_ID,
  DELIVERY_DATE,
  SHIP_ND_DATE,
  SHIP_2D_DATE,
  SHIP_GR_DATE,
  SHIP_SAT_DATE,
  SHIP_ND_CUTOFF_TIME,
  SHIP_2D_CUTOFF_TIME,
  SHIP_GR_CUTOFF_TIME,
  SHIP_SAT_CUTOFF_TIME
) AS 
SELECT pvpsd.vendor_id,
    pvpsd.product_id,
    zip.zip_code_id,
    pvpsd.delivery_date,
    pvpsd.ship_nd_date,
    pvpsd.ship_2d_date,
    pvpsd.ship_gr_date,
    cast(decode(ship_sat_date,null,null, decode((select count(*)
        from ftd_apps.carrier_zip_code czc,ftd_apps.vendor_master vm
        where czc.zip_code = zip.zip_code_id
        and czc.saturday_del_available = 'Y'
        and vm.vendor_id = pvpsd.vendor_id
        and (vm.vendor_type = 'FTP'
            or exists (select 'Y'
                from venus.vendor_carrier_ref vcr
                where vcr.vendor_id = vm.vendor_id
                and vcr.usage_percent = -1
                and vcr.carrier_id = czc.carrier))), 0, null, ship_sat_date)) as date),
    pvpsd.ship_nd_cutoff_time,
    pvpsd.ship_2d_cutoff_time,
    pvpsd.ship_gr_cutoff_time,
    pvpsd.ship_sat_cutoff_time
FROM pas.PAS_VENDOR_PRODUCT_STATE_DT pvpsd,
    ftd_apps.zip_code zip
WHERE pvpsd.state_code = zip.state_id;
