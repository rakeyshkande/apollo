CREATE OR REPLACE FUNCTION frp.use_this_from_address RETURN VARCHAR2 AS

   v_location VARCHAR2(30);

BEGIN

   SELECT SYS_CONTEXT('USERENV','DB_NAME')
   INTO   v_location
   FROM   DUAL;

   RETURN v_location||'_database@ftdi.com';

END use_this_from_address;
/
grant execute on frp.use_this_from_address to amazon;
grant execute on frp.use_this_from_address to clean;
grant execute on frp.use_this_from_address to ops$oracle;
grant execute on frp.use_this_from_address to events;
grant execute on frp.use_this_from_address to ftd_apps;
grant execute on frp.use_this_from_address to mercury;
grant execute on frp.use_this_from_address to ojms;
grant execute on frp.use_this_from_address to pop;
grant execute on frp.use_this_from_address to scrub;
grant execute on frp.use_this_from_address to sitescope;
grant execute on frp.use_this_from_address to venus;
