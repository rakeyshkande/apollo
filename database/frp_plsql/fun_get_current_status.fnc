CREATE OR REPLACE
FUNCTION frp.FUN_GET_CURRENT_STATUS (
   PNUM_ORDER_DETAIL_ID   FRP.ORDER_STATE.ORDER_DETAIL_ID%TYPE)
RETURN VARCHAR2
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***             Purpose : Get the current status from frp.order_state table
***
***    Input Parameters : pnum_order_detail_id - order_detail_id
***
***             Returns : current status from frp.order_state table
***
*** Special Instructions:
***
*****************************************************************************************/

   lchr_current_status   frp.order_state.status%TYPE := NULL;

BEGIN

   -- fetch the current status
   BEGIN
      SELECT
         status
      INTO
         lchr_current_status
      FROM
         frp.order_state
      WHERE
         order_detail_id = pnum_order_detail_id;
   EXCEPTION
      WHEN OTHERS THEN
         lchr_current_status := NULL;
   END;

   RETURN lchr_current_status;


EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In FRP.FUN_GET_CURRENT_STATUS: SQLCODE: ' || sqlcode ||
         ' SQL ERROR MESSAGE: ' || sqlerrm);
END FUN_GET_CURRENT_STATUS;
.
/
