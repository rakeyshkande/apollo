create or replace trigger frp.secure_config_$
after insert or update or delete on frp.secure_config referencing old as old new as new for each row
begin

   if inserting then

      insert into frp.secure_config$ (
         context,
         name,
         value,
         description,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) values (
         :new.context,
         :new.name,
         :new.value,
         :new.description,
         :new.created_on,
         :new.created_by,
         :new.updated_on,
         :new.updated_by,
         'INS',
         sysdate);

   elsif updating then

      insert into frp.secure_config$ (
         context,
         name,
         value,
         description,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) values (
         :old.context,
         :old.name,
         :old.value,
         :old.description,
         :old.created_on,
         :old.created_by,
         :old.updated_on,
         :old.updated_by,
         'UPD_OLD',
         sysdate);

      insert into frp.secure_config$ (
         context,
         name,
         value,
         description,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) values (
         :new.context,
         :new.name,
         :new.value,
         :new.description,
         :new.created_on,
         :new.created_by,
         :new.updated_on,
         :new.updated_by,
         'UPD_NEW',
         sysdate);

   elsif deleting then

      insert into frp.secure_config$ (
         context,
         name,
         value,
         description,
         created_on,
         created_by,
         updated_on,
         updated_by,
         operation$,
         timestamp$
      ) values (
         :old.context,
         :old.name,
         :old.value,
         :old.description,
         :old.created_on,
         :old.created_by,
         :old.updated_on,
         :old.updated_by,
         'DEL',
         sysdate);

   end if;

end;
/
