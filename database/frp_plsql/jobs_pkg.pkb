CREATE OR REPLACE
PACKAGE BODY frp.JOBS_PKG AS

   PROCEDURE submit(p_context IN global_parms.context%TYPE) IS
      v_submitter_name global_parms.value%TYPE;
      v_system_message_id NUMBER;
      v_status VARCHAR2(4000);
      v_message VARCHAR2(4000);
   BEGIN
      v_submitter_name := misc_pkg.get_global_parm_value(p_context, 'SUBMITTER');
      EXECUTE IMMEDIATE 'begin '||v_submitter_name||'; end;';
   EXCEPTION
      WHEN OTHERS THEN
         misc_pkg.insert_system_messages(
            'frp.jobs_pkg',
            'INFO',
            'scheduling job "'||p_context||'", '||sqlcode||' '||substr(sqlerrm, 1, 256),
            sys_context('USERENV','HOST'),
            v_system_message_id, v_status, v_message);
   END submit;


   PROCEDURE remove(p_context IN global_parms.context%TYPE) IS
      v_remover_name global_parms.value%TYPE;
      v_system_message_id NUMBER;
      v_status VARCHAR2(4000);
      v_message VARCHAR2(4000);
   BEGIN
      v_remover_name := misc_pkg.get_global_parm_value(p_context, 'REMOVER');
      EXECUTE IMMEDIATE 'begin '||v_remover_name||'; end;';
   EXCEPTION
      WHEN OTHERS THEN
         misc_pkg.insert_system_messages(
            'frp.jobs_pkg',
            'INFO',
            'scheduling job "'||p_context||'", '||sqlcode||' '||substr(sqlerrm, 1, 256),
            sys_context('USERENV','HOST'),
            v_system_message_id, v_status, v_message);
   END remove;


   PROCEDURE submit_all IS
   BEGIN
      NULL;
   END submit_all;


   PROCEDURE remove_all IS
   BEGIN
      NULL;
   END remove_all;


   PROCEDURE monitor IS

      c utl_smtp.connection;
      v_location VARCHAR2(10);

      v_system_message_id     NUMBER;
      v_status                VARCHAR2(1000);
      v_message               VARCHAR2(1000);

      CURSOR broken_c IS
         SELECT job, schema_user, what, broken, failures, next_date, sysdate current_date
         FROM   dba_jobs
         WHERE  failures > 0
         OR    (next_date < sysdate-(5/1440) AND this_date IS NULL);
         --WHERE  broken = 'Y'
         --OR     failures > 0
         --OR    (next_date < sysdate-(5/1440) AND this_date IS NULL);
      broken_r broken_c%ROWTYPE;


   BEGIN
      FOR broken_r IN broken_c LOOP

         ops$oracle.mail_pkg.init_email(c,'DBA_ALERTS','Scheduled Job Problem',v_status, v_message);
         if v_status <> 'Y' then
            raise_application_error(-20000,v_message,true);
         end if;

         utl_smtp.write_data(c, utl_tcp.CRLF||'Database '||v_location||
         ' Problem with job: '||broken_r.job||' '||broken_r.what||' BROKEN = '||broken_r.broken||
         ' failures = '||nvl(broken_r.failures, 0)||' next_date = '||to_char(broken_r.next_date, 'yyyy/mm/dd hh24:mi')||
         ' sysdate = '||to_char(broken_r.current_date, 'yyyy/mm/dd hh24:mi'));

         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         if v_status <> 'Y' then
            raise_application_error(-20000,v_message,true);
         end if;

      END LOOP;
   END monitor;

END jobs_pkg;
/
