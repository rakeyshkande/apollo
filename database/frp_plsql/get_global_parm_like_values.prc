create or replace PROCEDURE frp.GET_GLOBAL_PARM_LIKE_VALUES (
                IN_CONTEXT      IN FRP.GLOBAL_PARMS.CONTEXT%TYPE,
                IN_NAME_LIKE    IN FRP.GLOBAL_PARMS.NAME%TYPE,
                OUT_CUR         OUT TYPES.REF_CURSOR) IS
  
/*-----------------------------------------------------------------------------
Description:
       get All the global params matching given name

Input:
        IN_CONTEXT                       varchar2
        IN_NAME_LIKE                     varchar2

Output:
        cursor result set containing all global param name value pairs

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
                SELECT  NAME, VALUE 
                FROM    FRP.GLOBAL_PARMS 
    WHERE CONTEXT = IN_CONTEXT AND NAME LIKE IN_NAME_LIKE;

END GET_GLOBAL_PARM_LIKE_VALUES;
/
grant execute on frp.GET_GLOBAL_PARM_LIKE_VALUES to osp;

