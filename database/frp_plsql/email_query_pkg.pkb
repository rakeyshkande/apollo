CREATE OR REPLACE
PACKAGE BODY frp.EMAIL_QUERY_PKG AS

FUNCTION GET_PROGRAM_BY_SOURCE
(
IN_SOURCE_CODE                  IN PROGRAM_TO_SOURCE_MAPPING.SOURCE_CODE%TYPE,
IN_DATE                         IN PROGRAM_MAPPING.START_DATE%TYPE
)
RETURN NUMBER

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the current program mapping
        information for the given date.  If a source code is valid but
        no default program or custom program has been assigned to it,
        the system default program will be returned.

Input:
        source_code                     varchar2
        date                            date

Output:
        program_id                      number

-----------------------------------------------------------------------------*/

V_PROGRAM_ID            PROGRAM_MAPPING.PROGRAM_ID%TYPE;

BEGIN

SELECT  COALESCE (      C.CUSTOM_PROGRAM_ID,
                        D.DEFAULT_PROGRAM_ID,
                        TO_NUMBER (MISC_PKG.GET_GLOBAL_PARM_VALUE ('EMAIL_CONFIRMATION', 'SYSTEM_DEFAULT_PROGRAM'))
                )
INTO    V_PROGRAM_ID
FROM
(
        SELECT  S.SOURCE_CODE,
                (
                        SELECT PM.PROGRAM_ID
                        FROM    PROGRAM_MAPPING PM
                        JOIN    PROGRAM_TO_SOURCE_MAPPING PSM
                        ON      PM.PROGRAM_ID = PSM.PROGRAM_ID
                        AND     PM.DEFAULT_PROGRAM = 'Y'
                        WHERE   PSM.SOURCE_CODE = S.SOURCE_CODE
                ) AS DEFAULT_PROGRAM_ID
        FROM    FTD_APPS.SOURCE S
        WHERE   S.SOURCE_CODE = IN_SOURCE_CODE
) D                                             -- default program for the source code
LEFT OUTER JOIN
(
        SELECT  PSM.SOURCE_CODE,
                PSM.PROGRAM_ID AS CUSTOM_PROGRAM_ID
        FROM    PROGRAM_TO_SOURCE_MAPPING PSM
        JOIN    PROGRAM_MAPPING PM
        ON      PM.PROGRAM_ID = PSM.PROGRAM_ID
        WHERE   PSM.SOURCE_CODE = IN_SOURCE_CODE
        AND     PM.START_DATE <= IN_DATE
        AND     PM.END_DATE >= IN_DATE
) C                                             -- custom program for the source code
ON      C.SOURCE_CODE = D.SOURCE_CODE;

RETURN V_PROGRAM_ID;

EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN NULL;

END GET_PROGRAM_BY_SOURCE;


PROCEDURE VIEW_EMAIL_CONFIRMATION
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given email confirmation
        information.

Input:
        email_confirmation_guid         varchar2

Output:
        cursor result set containing email confirmation and style sheet information
        email_confirmation_guid         varchar2
        email_style_sheet_id            number
        to_address                      varchar2
        from_address                    varchar2
        subject                         varchar2
        filename                        varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  C.EMAIL_CONFIRMATION_GUID,
                C.EMAIL_STYLE_SHEET_ID,
                C.TO_ADDRESS,
                C.FROM_ADDRESS,
                C.SUBJECT,
                SS.FILENAME
        FROM    EMAIL_CONFIRMATION C
        JOIN    EMAIL_STYLE_SHEET SS
        ON      C.EMAIL_STYLE_SHEET_ID = SS.EMAIL_STYLE_SHEET_ID
        WHERE   C.EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID;

END VIEW_EMAIL_CONFIRMATION;


PROCEDURE VIEW_EMAIL_CONTENT
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given email confirmation
        content information.

Input:
        email_confirmation_guid         varchar2

Output:
        cursor result set containing email confirmation content information
        email_confirmation_guid         varchar2
        content_type                    varchar2
        section                         number
        content                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  EMAIL_CONFIRMATION_GUID,
                CONTENT_TYPE,
                SECTION,
                CONTENT
        FROM    EMAIL_CONTENT
        WHERE   EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID
        ORDER BY CONTENT_TYPE, SECTION;

END VIEW_EMAIL_CONTENT;


PROCEDURE VIEW_EMAIL_CONFIRMATION_INFO
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
OUT_CONFIRMATION_CUR            OUT TYPES.REF_CURSOR,
OUT_CONTENT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all information
        for the given email confirmation guid.

Input:
        email_confirmation_guid         varchar2

Output:
        confirmation cursor
        content cursor
-----------------------------------------------------------------------------*/

BEGIN

VIEW_EMAIL_CONFIRMATION (IN_EMAIL_CONFIRMATION_GUID, OUT_CONFIRMATION_CUR);
VIEW_EMAIL_CONTENT (IN_EMAIL_CONFIRMATION_GUID, OUT_CONTENT_CUR);

END VIEW_EMAIL_CONFIRMATION_INFO;


PROCEDURE VIEW_PROGRAM_BY_ID
(
IN_PROGRAM_ID                   IN PROGRAM_MAPPING.PROGRAM_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning program mapping
        information for the given id

Input:
        program_id                      number

Output:
        cursor result set containing program information
        program_id                      number
        description                     varchar2
        start_date                      varchar2
        end_date                        varchar2
        default_program                 varchar2
        created_on                      varchar2
        updated_on                      varchar2
        updated_by                      varchar2
        program_name                    varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  PROGRAM_ID,
                DESCRIPTION,
                TO_CHAR (START_DATE, 'MM/DD/YYYY') AS START_DATE,
                TO_CHAR (END_DATE, 'MM/DD/YYYY') AS END_DATE,
                DEFAULT_PROGRAM,
                TO_CHAR (CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                UPDATED_BY,
                PROGRAM_NAME
        FROM    PROGRAM_MAPPING
        WHERE   PROGRAM_ID = IN_PROGRAM_ID;

END VIEW_PROGRAM_BY_ID;


PROCEDURE VIEW_SECTION
(
IN_SECTION_ID                   IN EMAIL_SECTIONS.SECTION_ID%TYPE,
OUT_SECTIONS                    OUT TYPES.REF_CURSOR,
OUT_CONTENT                     OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning information for the
        given email section id and associated section content

Input:
        section_id                      number

Output:
        cursor result set containing email section information
        section_id                      number
        content_type                    varchar2
        start_date                      varchar2
        end_date                        varchar2
        default_section                 varchar2
        section_type                    varchar2
        description                     varchar2
        created_on                      varchar2
        updated_on                      varchar2
        updated_by                      varchar2
        section_name                    varchar2

        cursor result set containing content information
        section_id                      number
        content_section_order_id        number
        content                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_SECTIONS FOR
        SELECT  SECTION_ID,
                CONTENT_TYPE,
                TO_CHAR (START_DATE, 'MM/DD/YYYY') AS START_DATE,
                TO_CHAR (END_DATE, 'MM/DD/YYYY') AS END_DATE,
                DEFAULT_SECTION,
                SECTION_TYPE,
                DESCRIPTION,
                TO_CHAR (CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                UPDATED_BY,
                SECTION_NAME
        FROM    EMAIL_SECTIONS
        WHERE   SECTION_ID = IN_SECTION_ID;

OPEN OUT_CONTENT FOR
        SELECT  SECTION_ID,
                CONTENT_SECTION_ORDER_ID,
                CONTENT
        FROM    EMAIL_SECTION_CONTENT
        WHERE   SECTION_ID = IN_SECTION_ID
        ORDER BY SECTION_ID, CONTENT_SECTION_ORDER_ID;

END VIEW_SECTION;


PROCEDURE VIEW_SECTION_BY_NAME
(
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
IN_SECTION_NAME                 IN EMAIL_SECTIONS.SECTION_NAME%TYPE,
OUT_SECTIONS                    OUT TYPES.REF_CURSOR,
OUT_CONTENT                     OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning information for the
        given email section type and name and associated section content

Input:
        section_type                    varchar2
        section_name                    varchar2

Output:
        cursor result set containing email section information
        section_id                      number
        content_type                    varchar2
        start_date                      varchar2
        end_date                        varchar2
        default_section                 varchar2
        section_type                    varchar2
        description                     varchar2
        created_on                      varchar2
        updated_on                      varchar2
        updated_by                      varchar2
        section_name                    varchar2

        cursor result set containing content information
        section_id                      number
        content_section_order_id        number
        content                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_SECTIONS FOR
        SELECT  SECTION_ID,
                CONTENT_TYPE,
                TO_CHAR (START_DATE, 'MM/DD/YYYY') AS START_DATE,
                TO_CHAR (END_DATE, 'MM/DD/YYYY') AS END_DATE,
                DEFAULT_SECTION,
                SECTION_TYPE,
                DESCRIPTION,
                TO_CHAR (CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                UPDATED_BY,
                SECTION_NAME
        FROM    EMAIL_SECTIONS
        WHERE   SECTION_TYPE = IN_SECTION_TYPE
        AND     SECTION_NAME = IN_SECTION_NAME
        ORDER BY SECTION_ID;

VIEW_CONTENT_SECTION_BY_NAME (IN_SECTION_TYPE, IN_SECTION_NAME, OUT_CONTENT);

END VIEW_SECTION_BY_NAME;

PROCEDURE VIEW_SECTION_BY_TYPE
(
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
OUT_SECTIONS                    OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the email sections names.

Input:
        section_type                    varchar2

Output:
        cursor result set containing email section information
        section_name                    varchar2

-----------------------------------------------------------------------------*/

BEGIN


OPEN OUT_SECTIONS FOR
        SELECT  DISTINCT
                SECTION_NAME
        FROM    EMAIL_SECTIONS
        WHERE   SECTION_TYPE = IN_SECTION_TYPE
        ORDER BY UPPER (SECTION_NAME);

END VIEW_SECTION_BY_TYPE;


PROCEDURE VIEW_CONTENT_SECTION_BY_NAME
(
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
IN_SECTION_NAME                 IN EMAIL_SECTIONS.SECTION_NAME%TYPE,
OUT_CONTENT                     OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning content information for the
        given email section type and name.

Input:
        section_type                    varchar2
        section_name                    varchar2

Output:
        cursor result set containing content information
        section_id                      number
        content_section_order_id        number
        content                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CONTENT FOR
        SELECT  ESC.SECTION_ID,
                ESC.CONTENT_SECTION_ORDER_ID,
                ESC.CONTENT
        FROM    EMAIL_SECTION_CONTENT ESC
        JOIN    EMAIL_SECTIONS ES
        ON      ESC.SECTION_ID = ES.SECTION_ID
        WHERE   ES.SECTION_TYPE = IN_SECTION_TYPE
        AND     ES.SECTION_NAME = IN_SECTION_NAME
        ORDER BY ESC.SECTION_ID, ESC.CONTENT_SECTION_ORDER_ID;

END VIEW_CONTENT_SECTION_BY_NAME;


PROCEDURE VIEW_SECTIONS
(
IN_PROGRAM_ID                   IN PROGRAM_MAPPING.PROGRAM_ID%TYPE,
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
IN_DATE                         IN EMAIL_SECTIONS.START_DATE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning section
        information for the given program id.  If the section type is specified
        only sections for that type is returned.  If the date is specified,
        only records the most current records will be returned (the records are
        effective for the given and the start date is the closest to the given
        date).

Input:
        program_id                      number
        section_type                    varchar2 (optional)
        date                            date (optional)

Output:
        cursor result set containing section information
        section_id                      number
        content_type                    varchar2
        start_date                      varchar2
        end_date                        varchar2
        default_section                 varchar2
        section_type                    varchar2
        description                     varchar2
        created_on                      varchar2
        updated_on                      varchar2
        updated_by                      varchar2
        section_name                    varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  S.SECTION_ID,
                S.CONTENT_TYPE,
                TO_CHAR (S.START_DATE, 'MM/DD/YYYY') AS START_DATE,
                TO_CHAR (S.END_DATE, 'MM/DD/YYYY') AS END_DATE,
                S.DEFAULT_SECTION,
                S.SECTION_TYPE,
                S.DESCRIPTION,
                TO_CHAR (S.CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (S.UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                S.UPDATED_BY,
                S.SECTION_NAME
        FROM    EMAIL_SECTIONS S
        JOIN    PROGRAM_SECTION_MAPPING PSM
        ON      S.SECTION_ID = PSM.SECTION_ID
        WHERE   PSM.PROGRAM_ID = IN_PROGRAM_ID
        AND     (S.SECTION_TYPE = IN_SECTION_TYPE OR IN_SECTION_TYPE IS NULL)
        AND     (S.START_DATE =
                        (       SELECT  MAX (S2.START_DATE)
                                FROM    EMAIL_SECTIONS S2
                                JOIN    PROGRAM_SECTION_MAPPING PSM
                                ON      S2.SECTION_ID = PSM.SECTION_ID
                                WHERE   PSM.PROGRAM_ID = IN_PROGRAM_ID
                                AND     S2.SECTION_TYPE = S.SECTION_TYPE
                                AND     S2.CONTENT_TYPE = S.CONTENT_TYPE
                                AND     S2.START_DATE <= IN_DATE
                                AND     S2.END_DATE >= IN_DATE OR S2.END_DATE IS NULL
                        )
        OR      IN_DATE IS NULL);

END VIEW_SECTIONS;


PROCEDURE VIEW_SOURCES_BY_PROGRAM_ID
(
IN_PROGRAM_ID                   IN PROGRAM_MAPPING.PROGRAM_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the sources that
        are associated to the given program.

Input:
        program_id                      number

Output:
        cursor result set containing source codes
        source code                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  SOURCE_CODE
        FROM    PROGRAM_TO_SOURCE_MAPPING PSM
        JOIN    PROGRAM_MAPPING PM
        ON      PM.PROGRAM_ID = PSM.PROGRAM_ID
        WHERE   PM.PROGRAM_ID = IN_PROGRAM_ID;

END VIEW_SOURCES_BY_PROGRAM_ID;


PROCEDURE VIEW_SOURCES
(
IN_ALL_FLAG                     IN  VARCHAR2,
IN_PROGRAM_ID                   IN  PROGRAM_MAPPING.PROGRAM_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source codes
        available to be assigned to programs.  The valid search flag
        are 'Y' - everything, 'N' - if no program id is specified,
        sources that are assigned to a default are not returned, if a
        program id is specified, sources that are assigned to the
        program are not returned.

Input:
        all flag                varchar2 ('Y' or 'N')
        program id              number (optional)

Output:
        cursor result set containing source codes
        source code                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ALL_FLAG = 'Y' THEN

        OPEN OUT_CUR FOR
                SELECT  SOURCE_CODE
                FROM    FTD_APPS.SOURCE
                ORDER BY SOURCE_CODE;

ELSIF IN_ALL_FLAG = 'N' AND IN_PROGRAM_ID IS NULL THEN

        OPEN OUT_CUR FOR
                SELECT  SOURCE_CODE
                FROM    FTD_APPS.SOURCE
                WHERE   SOURCE_CODE NOT IN
                        (
                                SELECT  DISTINCT PSM.SOURCE_CODE
                                FROM    PROGRAM_TO_SOURCE_MAPPING PSM
                                JOIN    PROGRAM_MAPPING PM
                                ON      PSM.PROGRAM_ID = PM.PROGRAM_ID
                                WHERE   PM.DEFAULT_PROGRAM = 'Y'
                        )
                ORDER BY SOURCE_CODE;

ELSE

        OPEN OUT_CUR FOR
                SELECT  SOURCE_CODE
                FROM    FTD_APPS.SOURCE
                WHERE   SOURCE_CODE NOT IN
                        (
                                SELECT  DISTINCT SOURCE_CODE
                                FROM    PROGRAM_TO_SOURCE_MAPPING
                                WHERE   PROGRAM_ID = IN_PROGRAM_ID
                        )
                ORDER BY SOURCE_CODE;
END IF;

END VIEW_SOURCES;



PROCEDURE SEARCH_PROGRAM_BY_NAME
(
IN_PROGRAM_NAME                 IN PROGRAM_MAPPING.PROGRAM_NAME%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the programs with similar
        to the given name.

Input:
        program_name                    varchar2

Output:
        cursor result set containing program information
        program_id                      number
        program_name                    varchar2
        description                     varchar2
        created on                      varchar2
        updated on                      varchar2
        updated by                      varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  PROGRAM_ID,
                PROGRAM_NAME,
                DESCRIPTION,
                TO_CHAR (CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                UPDATED_BY
        FROM    PROGRAM_MAPPING
        WHERE   UPPER (PROGRAM_NAME) LIKE '%' || UPPER (IN_PROGRAM_NAME) || '%'
        ORDER BY UPPER (PROGRAM_NAME);

END SEARCH_PROGRAM_BY_NAME;


PROCEDURE SEARCH_PROGRAM_BY_SOURCE
(
IN_SOURCE_CODE                  IN PROGRAM_TO_SOURCE_MAPPING.SOURCE_CODE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the programs with for the
        the given source code.

Input:
        source_code                     varchar2

Output:
        cursor result set containing program information
        program_id                      number
        program_name                    varchar2
        description                     varchar2
        created on                      varchar2
        updated on                      varchar2
        updated by                      varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  PM.PROGRAM_ID,
                PM.PROGRAM_NAME,
                PM.DESCRIPTION,
                TO_CHAR (PM.CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (PM.UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                PM.UPDATED_BY
        FROM    PROGRAM_MAPPING PM
        JOIN    PROGRAM_TO_SOURCE_MAPPING PSM
        ON      PM.PROGRAM_ID = PSM.PROGRAM_ID
        WHERE   PSM.SOURCE_CODE = IN_SOURCE_CODE
        ORDER BY UPPER (PM.PROGRAM_NAME);

END SEARCH_PROGRAM_BY_SOURCE;


PROCEDURE SEARCH_SECTIONS_BY_TYPE
(
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
IN_SECTION_NAME                 IN EMAIL_SECTIONS.SECTION_NAME%TYPE,
OUT_SECTIONS                    OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the email sections.
        If name is specified, return only the sections with a similar name.

Input:
        section_type                    varchar2
        section_name                    varchar2 (optional)

Output:
        cursor result set containing email section information
        section_id                      number
        content_type                    varchar2
        start_date                      varchar2
        end_date                        varchar2
        default_section                 varchar2
        section_type                    varchar2
        description                     varchar2
        created_on                      varchar2
        updated_on                      varchar2
        updated_by                      varchar2
        section_name                    varchar2

        cursor result set containing content information
        section_id                      number
        content_section_order_id        number
        content                         varchar2
-----------------------------------------------------------------------------*/

V_SECTION_NAME                  EMAIL_SECTIONS.SECTION_NAME%TYPE;

BEGIN

V_SECTION_NAME := '%' || UPPER (IN_SECTION_NAME) || '%';

OPEN OUT_SECTIONS FOR
        SELECT  SECTION_ID,
                CONTENT_TYPE,
                TO_CHAR (START_DATE, 'MM/DD/YYYY') AS START_DATE,
                TO_CHAR (END_DATE, 'MM/DD/YYYY') AS END_DATE,
                DEFAULT_SECTION,
                SECTION_TYPE,
                DESCRIPTION,
                TO_CHAR (CREATED_ON, 'MM/DD/YYYY') AS CREATED_ON,
                TO_CHAR (UPDATED_ON, 'MM/DD/YYYY') AS UPDATED_ON,
                UPDATED_BY,
                SECTION_NAME
        FROM    EMAIL_SECTIONS
        WHERE   SECTION_TYPE = IN_SECTION_TYPE
        AND     (UPPER (SECTION_NAME) LIKE V_SECTION_NAME OR V_SECTION_NAME IS NULL)
        AND     CONTENT_TYPE = 'text'
        ORDER BY UPPER (SECTION_NAME);

END SEARCH_SECTIONS_BY_TYPE;


PROCEDURE VIEW_CONTENT_BY_PROGRAM
(
IN_PROGRAM_ID                   PROGRAM_SECTION_MAPPING.PROGRAM_ID%TYPE,
IN_DATE                         EMAIL_SECTIONS.START_DATE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the current email section
        content for the given program id current on the given date.
Input:
        program_id                      number
        date                            date

Output:
        cursor result set containing email section content
        section_type                    varchar2
        content_type                    varchar2
        content                         varchar2
        section_id                      number
        content_section_order_id        number
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  S.SECTION_TYPE,
                S.CONTENT_TYPE,
                C.CONTENT,
                C.SECTION_ID,
                C.CONTENT_SECTION_ORDER_ID
        FROM    EMAIL_SECTIONS S
        JOIN    EMAIL_SECTION_CONTENT C
        ON      S.SECTION_ID = C.SECTION_ID
        JOIN    PROGRAM_SECTION_MAPPING PSM
        ON      S.SECTION_ID = PSM.SECTION_ID
        WHERE   PSM.PROGRAM_ID = IN_PROGRAM_ID
        AND     S.START_DATE =
                        (       SELECT  MAX (S2.START_DATE)
                                FROM    EMAIL_SECTIONS S2
                                JOIN    PROGRAM_SECTION_MAPPING PSM
                                ON      S2.SECTION_ID = PSM.SECTION_ID
                                WHERE   PSM.PROGRAM_ID = IN_PROGRAM_ID
                                AND     S2.SECTION_TYPE = S.SECTION_TYPE
                                AND     S2.CONTENT_TYPE = S.CONTENT_TYPE
                                AND     S2.START_DATE <= IN_DATE
                                AND     (S2.END_DATE >= IN_DATE OR S2.END_DATE IS NULL)
                        )
        ORDER BY S.SECTION_TYPE, C.SECTION_ID, C.CONTENT_SECTION_ORDER_ID;

END VIEW_CONTENT_BY_PROGRAM;


PROCEDURE VIEW_CURRENT_EMAIL_SECTIONS
(
IN_SOURCE_CODE                  PROGRAM_TO_SOURCE_MAPPING.SOURCE_CODE%TYPE,
CONTENT_CUR                     OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the current email section
        content for the given source code.

Input:
        source_code                     varchar2

Output:
        cursor result set containing email section content
        section_type                    varchar2
        content_type                    varchar2
        content                         varchar2
        section_id                      number
        content_section_order_id        number
-----------------------------------------------------------------------------*/

V_PROGRAM_ID            PROGRAM_MAPPING.PROGRAM_ID%TYPE;
V_TODAY                 DATE := TRUNC (SYSDATE);

BEGIN

V_PROGRAM_ID := GET_PROGRAM_BY_SOURCE (IN_SOURCE_CODE, V_TODAY);
VIEW_CONTENT_BY_PROGRAM (V_PROGRAM_ID, V_TODAY, CONTENT_CUR);

IF V_PROGRAM_ID IS NOT NULL THEN
        OUT_STATUS := 'Y';
ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'SOURCE CODE NOT FOUND';
END IF;

END VIEW_CURRENT_EMAIL_SECTIONS;


PROCEDURE VIEW_CURRENT_SECTION_MAPPING
(
IN_PROGRAM_ID                   PROGRAM_SECTION_MAPPING.PROGRAM_ID%TYPE,
IN_DATE                         EMAIL_SECTIONS.START_DATE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the current sections
        for the given program id current on the given date.

Input:
        program_id                      number
        date                            date

Output:
        cursor result set containing email section content
        section_type                    varchar2
        content_type                    varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT
                S.SECTION_TYPE,
                S.SECTION_NAME
        FROM    EMAIL_SECTIONS S
        JOIN    PROGRAM_SECTION_MAPPING PSM
        ON      S.SECTION_ID = PSM.SECTION_ID
        WHERE   PSM.PROGRAM_ID = IN_PROGRAM_ID
        AND     S.START_DATE =
                        (       SELECT  MAX (S2.START_DATE)
                                FROM    EMAIL_SECTIONS S2
                                JOIN    PROGRAM_SECTION_MAPPING PSM
                                ON      S2.SECTION_ID = PSM.SECTION_ID
                                WHERE   PSM.PROGRAM_ID = IN_PROGRAM_ID
                                AND     S2.SECTION_TYPE = S.SECTION_TYPE
                                AND     S2.CONTENT_TYPE = S.CONTENT_TYPE
                                AND     S2.START_DATE <= IN_DATE
                                AND     (S2.END_DATE >= IN_DATE OR S2.END_DATE IS NULL)
                        )
        ORDER BY S.SECTION_TYPE;

END VIEW_CURRENT_SECTION_MAPPING;


FUNCTION PROGRAM_NAME_EXISTS
(
IN_PROGRAM_NAME         IN PROGRAM_MAPPING.PROGRAM_NAME%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an program name exists.

Input:
        program_name                    varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    PROGRAM_MAPPING
WHERE   PROGRAM_NAME = IN_PROGRAM_NAME;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END PROGRAM_NAME_EXISTS;


FUNCTION SECTION_NAME_EXISTS
(
IN_SECTION_NAME         IN EMAIL_SECTIONS.SECTION_NAME%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an section name exists.

Input:
        section_name                    varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    EMAIL_SECTIONS
WHERE   SECTION_NAME = IN_SECTION_NAME;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END SECTION_NAME_EXISTS;


FUNCTION CHECK_CUSTOM_PROGRAM_OVERLAP
(
IN_SOURCE_CODE                  IN PROGRAM_TO_SOURCE_MAPPING.SOURCE_CODE%TYPE,
IN_PROGRAM_ID                   IN PROGRAM_TO_SOURCE_MAPPING.PROGRAM_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if a program overlaps
        and existing programs for the given source.

Input:
        source code             varchar2
        program id              number

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT         NUMBER;
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    PROGRAM_MAPPING OLD
JOIN    PROGRAM_TO_SOURCE_MAPPING PSM
ON      OLD.PROGRAM_ID = PSM.PROGRAM_ID
AND     PSM.SOURCE_CODE = IN_SOURCE_CODE
AND     PSM.PROGRAM_ID <> IN_PROGRAM_ID
AND     OLD.DEFAULT_PROGRAM = 'N'
CROSS JOIN
(       SELECT  START_DATE,
                END_DATE
        FROM    PROGRAM_MAPPING
        WHERE   PROGRAM_ID = IN_PROGRAM_ID
) NEW
WHERE   NEW.START_DATE BETWEEN OLD.START_DATE AND OLD.END_DATE
OR      NEW.END_DATE BETWEEN OLD.START_DATE AND OLD.END_DATE;

IF V_COUNT > 0 THEN
        RETURN 'Y';
ELSE
        RETURN 'N';
END IF;

END CHECK_CUSTOM_PROGRAM_OVERLAP;


PROCEDURE VIEW_ADDITIONAL_ORDER_FIELDS
(
IN_SOURCE_CODE                  IN VARCHAR2,
OUT_JCPENNEY_FLAG               OUT VARCHAR2,
OUT_PARTNER_CUR                 OUT TYPES.REF_CURSOR,
OUT_SHIP_METHOD_CUR             OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the additional fields
        needed in the confirmation email for the given order

Input:
        order_guid                      varchar2

Output:
        jcpenny_flag                    varchar2
        cursor result set containing partner information
        cursor result set containing ship method descriptions

-----------------------------------------------------------------------------*/

V_PARTNER_ID    VARCHAR2 (10);

BEGIN

SELECT  S.JCPENNEY_FLAG, S.PARTNER_ID
INTO    OUT_JCPENNEY_FLAG, V_PARTNER_ID
FROM    FTD_APPS.SOURCE S
WHERE   S.SOURCE_CODE = IN_SOURCE_CODE;

GLOBAL.GLOBAL_PKG.GET_PARTNERS (V_PARTNER_ID, OUT_PARTNER_CUR);
GLOBAL.GLOBAL_PKG.GET_SHIP_METHODS (OUT_SHIP_METHOD_CUR);

END VIEW_ADDITIONAL_ORDER_FIELDS;


END EMAIL_QUERY_PKG;
.
/
