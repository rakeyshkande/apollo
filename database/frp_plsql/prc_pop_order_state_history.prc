CREATE OR REPLACE
PROCEDURE frp.PRC_POP_ORDER_STATE_HISTORY (
   PNUM_ORDER_DETAIL_ID   FRP.ORDER_STATE_HISTORY.ORDER_DETAIL_ID%TYPE,
   PCHR_NEW_STATUS   FRP.ORDER_STATE_HISTORY.STATUS%TYPE,
   PCHR_CURRENT_STATUS   FRP.ORDER_STATE_HISTORY.OLD_STATUS%TYPE,
   PCHR_TRIGGER_NAME   FRP.ORDER_STATE_HISTORY.TRIGGER_NAME%TYPE)
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***             Purpose : Inserts a record into frp.order_state_history table
***
***    Input Parameters : pnum_order_detail_id - order_detail_id
***                       pchr_new_status - new status
***                       pchr_current_status - previous status
***                       pchr_trigger_name - DB trigger that invoked this procedure
***
*** Special Instructions:
***
*****************************************************************************************/

BEGIN

   INSERT INTO frp.order_state_history (
      order_detail_id,
      status,
      old_status,
      created_on,
      trigger_name)
	VALUES (
		pnum_order_detail_id,
		pchr_new_status,
		pchr_current_status,
		SYSDATE,
		pchr_trigger_name);

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In FRP.PRC_POP_ORDER_STATE_HISTORY: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END PRC_POP_ORDER_STATE_HISTORY;
.
/
