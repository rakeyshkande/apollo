CREATE OR REPLACE
PACKAGE BODY frp.EMAIL_MAINT_PKG AS

PROCEDURE INSERT_EMAIL_STYLE_SHEET
(
IN_FILENAME                     IN EMAIL_STYLE_SHEET.FILENAME%TYPE,
OUT_EMAIL_STYLE_SHEET_ID        OUT EMAIL_STYLE_SHEET.EMAIL_STYLE_SHEET_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email style sheet information.
        ** needs to be updated with table changes or deleted if not used

Input:
        filename                        varchar2

Output:
        email_style_sheet_id            number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OUT_EMAIL_STYLE_SHEET_ID := KEY.KEYGEN('EMAIL_STYLE_SHEET');

INSERT INTO EMAIL_STYLE_SHEET
(
        EMAIL_STYLE_SHEET_ID,
        FILENAME
)
VALUES
(
        OUT_EMAIL_STYLE_SHEET_ID,
        IN_FILENAME
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_STYLE_SHEET;


PROCEDURE INSERT_EMAIL_CONFIRMATION
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
IN_EMAIL_STYLE_SHEET_ID         IN EMAIL_CONFIRMATION.EMAIL_STYLE_SHEET_ID%TYPE,
IN_TO_ADDRESS                   IN EMAIL_CONFIRMATION.TO_ADDRESS%TYPE,
IN_FROM_ADDRESS                 IN EMAIL_CONFIRMATION.FROM_ADDRESS%TYPE,
IN_SUBJECT                      IN EMAIL_CONFIRMATION.SUBJECT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email confirmation information.

Input:
        email_confirmation_guid         varchar2
        email_style_sheet_id            number
        to_address                      varchar2
        from_address                    varchar2
        subject                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO EMAIL_CONFIRMATION
(
        EMAIL_CONFIRMATION_GUID,
        EMAIL_STYLE_SHEET_ID,
        TO_ADDRESS,
        FROM_ADDRESS,
        SUBJECT
)
VALUES
(
        IN_EMAIL_CONFIRMATION_GUID,
        IN_EMAIL_STYLE_SHEET_ID,
        IN_TO_ADDRESS,
        IN_FROM_ADDRESS,
        IN_SUBJECT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_CONFIRMATION;


PROCEDURE INSERT_EMAIL_CONTENT
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONTENT.EMAIL_CONFIRMATION_GUID%TYPE,
IN_CONTENT_TYPE                 IN EMAIL_CONTENT.CONTENT_TYPE%TYPE,
IN_SECTION                      IN EMAIL_CONTENT.SECTION%TYPE,
IN_CONTENT                      IN EMAIL_CONTENT.CONTENT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email content information.

Input:
        email confirmation guid         varchar2
        content_type                    varchar2
        section                         number
        content                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO EMAIL_CONTENT
(
        EMAIL_CONFIRMATION_GUID,
        CONTENT_TYPE,
        SECTION,
        CONTENT
)
VALUES
(
        IN_EMAIL_CONFIRMATION_GUID,
        IN_CONTENT_TYPE,
        IN_SECTION,
        IN_CONTENT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_CONTENT;


PROCEDURE UPDATE_EMAIL_STYLE_SHEET
(
IN_EMAIL_STYLE_SHEET_ID         IN EMAIL_STYLE_SHEET.EMAIL_STYLE_SHEET_ID%TYPE,
IN_FILENAME                     IN EMAIL_STYLE_SHEET.FILENAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email style sheet information.
        ** needs to be updated with table changes or deleted if not used

Input:
        email_style_sheet_id            number
        filename                        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_STYLE_SHEET
SET     FILENAME = IN_FILENAME
WHERE   EMAIL_STYLE_SHEET_ID = IN_EMAIL_STYLE_SHEET_ID;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_STYLE_SHEET;


PROCEDURE UPDATE_EMAIL_CONFIRMATION
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
IN_EMAIL_STYLE_SHEET_ID         IN EMAIL_CONFIRMATION.EMAIL_STYLE_SHEET_ID%TYPE,
IN_TO_ADDRESS                   IN EMAIL_CONFIRMATION.TO_ADDRESS%TYPE,
IN_FROM_ADDRESS                 IN EMAIL_CONFIRMATION.FROM_ADDRESS%TYPE,
IN_SUBJECT                      IN EMAIL_CONFIRMATION.SUBJECT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email confirmation information.

Input:
        email_confirmation_guid         varchar2
        email_style_sheet_id            number
        to_address                      varchar2
        from_address                    varchar2
        subject                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_CONFIRMATION
SET     EMAIL_STYLE_SHEET_ID = IN_EMAIL_STYLE_SHEET_ID,
        TO_ADDRESS = IN_TO_ADDRESS,
        FROM_ADDRESS = IN_FROM_ADDRESS,
        SUBJECT = IN_SUBJECT
WHERE   EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_CONFIRMATION;


PROCEDURE UPDATE_EMAIL_CONTENT
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONTENT.EMAIL_CONFIRMATION_GUID%TYPE,
IN_CONTENT_TYPE                 IN EMAIL_CONTENT.CONTENT_TYPE%TYPE,
IN_SECTION                      IN EMAIL_CONTENT.SECTION%TYPE,
IN_CONTENT                      IN EMAIL_CONTENT.CONTENT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email content information.

Input:
        email confirmation guid         varchar2
        content_type                    varchar2
        section                         number
        content                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_CONTENT
SET     CONTENT = IN_CONTENT
WHERE   EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID
AND     CONTENT_TYPE = IN_CONTENT_TYPE
AND     SECTION = IN_SECTION;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_CONTENT;


PROCEDURE INSERT_PROGRAM_MAPPING
(
IN_UPDATED_BY                   IN PROGRAM_MAPPING.UPDATED_BY%TYPE,
IN_PROGRAM_NAME                 IN PROGRAM_MAPPING.PROGRAM_NAME%TYPE,
IN_DESCRIPTION                  IN PROGRAM_MAPPING.DESCRIPTION%TYPE,
IN_START_DATE                   IN PROGRAM_MAPPING.START_DATE%TYPE,
IN_END_DATE                     IN PROGRAM_MAPPING.END_DATE%TYPE,
IN_DEFAULT_PROGRAM              IN PROGRAM_MAPPING.DEFAULT_PROGRAM%TYPE,
OUT_PROGRAM_ID                  OUT PROGRAM_MAPPING.PROGRAM_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting program mapping information.

Input:
        updated_by                      varchar2
        program_name                    varchar2
        description                     varchar2
        start_date                      date
        end_date                        date
        default_program                 varchar2

Output:
        program_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OUT_PROGRAM_ID := KEY.KEYGEN('PROGRAM_MAPPING');

INSERT INTO PROGRAM_MAPPING
(
        PROGRAM_ID,
        DESCRIPTION,
        START_DATE,
        END_DATE,
        DEFAULT_PROGRAM,
        CREATED_ON,
        UPDATED_ON,
        UPDATED_BY,
        PROGRAM_NAME
)
VALUES
(
        OUT_PROGRAM_ID,
        IN_DESCRIPTION,
        IN_START_DATE,
        IN_END_DATE,
        IN_DEFAULT_PROGRAM,
        SYSDATE,                                -- CREATED_ON,
        SYSDATE,                                -- UPDATED_ON
        IN_UPDATED_BY,
        IN_PROGRAM_NAME
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_PROGRAM_MAPPING;


PROCEDURE INSERT_EMAIL_SECTIONS
(
IN_UPDATED_BY                   IN EMAIL_SECTIONS.UPDATED_BY%TYPE,
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
IN_SECTION_NAME                 IN EMAIL_SECTIONS.SECTION_NAME%TYPE,
IN_DESCRIPTION                  IN EMAIL_SECTIONS.DESCRIPTION%TYPE,
IN_START_DATE                   IN EMAIL_SECTIONS.START_DATE%TYPE,
IN_END_DATE                     IN EMAIL_SECTIONS.END_DATE%TYPE,
IN_CONTENT_TYPE                 IN EMAIL_SECTIONS.CONTENT_TYPE%TYPE,
IN_DEFAULT_SECTION              IN EMAIL_SECTIONS.DEFAULT_SECTION%TYPE,
OUT_SECTION_ID                  OUT EMAIL_SECTIONS.SECTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email section information.

Input:
        updated_by                      varchar2
        section_type                    varchar2
        section_name                    varchar2
        description                     varchar2
        start_date                      date
        end_date                        date
        content_type                    varchar2
        default_section                 varchar2

Output:
        section_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OUT_SECTION_ID := KEY.KEYGEN('EMAIL_SECTIONS');

INSERT INTO EMAIL_SECTIONS
(
        SECTION_ID,
        CONTENT_TYPE,
        START_DATE,
        END_DATE,
        DEFAULT_SECTION,
        SECTION_TYPE,
        DESCRIPTION,
        CREATED_ON,
        UPDATED_ON,
        UPDATED_BY,
        SECTION_NAME
)
VALUES
(
        OUT_SECTION_ID,
        IN_CONTENT_TYPE,
        IN_START_DATE,
        IN_END_DATE,
        IN_DEFAULT_SECTION,
        IN_SECTION_TYPE,
        IN_DESCRIPTION,
        SYSDATE,                        -- CREATED_ON,
        SYSDATE,                        -- UPDATED_ON,
        IN_UPDATED_BY,
        IN_SECTION_NAME
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_SECTIONS;


PROCEDURE INSERT_EMAIL_SECTION_CONTENT
(
IN_SECTION_ID                   IN EMAIL_SECTION_CONTENT.SECTION_ID%TYPE,
IN_CONTENT_SECTION_ORDER_ID     IN EMAIL_SECTION_CONTENT.CONTENT_SECTION_ORDER_ID%TYPE,
IN_CONTENT                      IN EMAIL_SECTION_CONTENT.CONTENT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email section content
        information.

Input:
        section_id                      number
        content_section_order_id        number
        content                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO EMAIL_SECTION_CONTENT
(
        SECTION_ID,
        CONTENT_SECTION_ORDER_ID,
        CONTENT
)
VALUES
(
        IN_SECTION_ID,
        IN_CONTENT_SECTION_ORDER_ID,
        IN_CONTENT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_SECTION_CONTENT;


PROCEDURE INSERT_PROGRAM_SECTION_MAPPING
(
IN_PROGRAM_ID                   IN PROGRAM_SECTION_MAPPING.PROGRAM_ID%TYPE,
IN_SECTION_ID                   IN PROGRAM_SECTION_MAPPING.SECTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting program to section mapping
        information.

Input:
        program_id                      number
        section_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO PROGRAM_SECTION_MAPPING
(
        PROGRAM_ID,
        SECTION_ID
)
VALUES
(
        IN_PROGRAM_ID,
        IN_SECTION_ID
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_PROGRAM_SECTION_MAPPING;


PROCEDURE UPDATE_PROGRAM_MAPPING
(
IN_UPDATED_BY                   IN PROGRAM_MAPPING.UPDATED_BY%TYPE,
IN_PROGRAM_ID                   IN PROGRAM_MAPPING.PROGRAM_ID%TYPE,
IN_PROGRAM_NAME                 IN PROGRAM_MAPPING.PROGRAM_NAME%TYPE,
IN_DESCRIPTION                  IN PROGRAM_MAPPING.DESCRIPTION%TYPE,
IN_START_DATE                   IN PROGRAM_MAPPING.START_DATE%TYPE,
IN_END_DATE                     IN PROGRAM_MAPPING.END_DATE%TYPE,
IN_DEFAULT_PROGRAM              IN PROGRAM_MAPPING.DEFAULT_PROGRAM%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating program mapping information.

Input:
        program_id                      number
        updated_by                      varchar2
        program_name                    varchar2
        description                     varchar2
        start_date                      date
        end_date                        date
        default_program                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE PROGRAM_MAPPING
SET     DESCRIPTION = IN_DESCRIPTION,
        START_DATE = IN_START_DATE,
        END_DATE = IN_END_DATE,
        DEFAULT_PROGRAM = IN_DEFAULT_PROGRAM,
        UPDATED_ON = SYSDATE,
        UPDATED_BY = IN_UPDATED_BY,
        PROGRAM_NAME = IN_PROGRAM_NAME
WHERE   PROGRAM_ID = IN_PROGRAM_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_PROGRAM_MAPPING;


PROCEDURE UPDATE_EMAIL_SECTIONS
(
IN_UPDATED_BY                   IN EMAIL_SECTIONS.UPDATED_BY%TYPE,
IN_SECTION_ID                   IN EMAIL_SECTIONS.SECTION_ID%TYPE,
IN_SECTION_TYPE                 IN EMAIL_SECTIONS.SECTION_TYPE%TYPE,
IN_SECTION_NAME                 IN EMAIL_SECTIONS.SECTION_NAME%TYPE,
IN_DESCRIPTION                  IN EMAIL_SECTIONS.DESCRIPTION%TYPE,
IN_START_DATE                   IN EMAIL_SECTIONS.START_DATE%TYPE,
IN_END_DATE                     IN EMAIL_SECTIONS.END_DATE%TYPE,
IN_CONTENT_TYPE                 IN EMAIL_SECTIONS.CONTENT_TYPE%TYPE,
IN_DEFAULT_SECTION              IN EMAIL_SECTIONS.DEFAULT_SECTION%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email section information.

Input:
        updated_by                      varchar2
        section_id                      number
        section_type                    varchar2
        section_name                    varchar2
        description                     varchar2
        start_date                      date
        end_date                        date
        content_type                    varchar2
        default_section                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_SECTIONS
SET     CONTENT_TYPE = IN_CONTENT_TYPE,
        START_DATE = IN_START_DATE,
        END_DATE = IN_END_DATE,
        DEFAULT_SECTION = IN_DEFAULT_SECTION,
        SECTION_TYPE = IN_SECTION_TYPE,
        DESCRIPTION = IN_DESCRIPTION,
        UPDATED_ON = SYSDATE,
        UPDATED_BY = IN_UPDATED_BY,
        SECTION_NAME = IN_SECTION_NAME
WHERE   SECTION_ID = IN_SECTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_SECTIONS;


PROCEDURE UPDATE_EMAIL_SECTION_CONTENT
(
IN_SECTION_ID                   IN EMAIL_SECTION_CONTENT.SECTION_ID%TYPE,
IN_CONTENT_SECTION_ORDER_ID     IN EMAIL_SECTION_CONTENT.CONTENT_SECTION_ORDER_ID%TYPE,
IN_CONTENT                      IN EMAIL_SECTION_CONTENT.CONTENT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email section content
        information.

Input:
        section_id                      number
        content_section_order_id        number
        content                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_SECTION_CONTENT
SET     CONTENT= IN_CONTENT
WHERE   SECTION_ID = IN_SECTION_ID
AND     CONTENT_SECTION_ORDER_ID = IN_CONTENT_SECTION_ORDER_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_SECTION_CONTENT;


PROCEDURE DELETE_PROGRAM_MAPPING
(
IN_PROGRAM_ID                   IN PROGRAM_MAPPING.PROGRAM_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting programs and the
        it's associated section mappings for the given program id.  First
        check if the program is associated to a source. If so, return
        an error message.

Input:
        section_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (PROGRAM ASSOCIATED TO A SOURCE,
                                                  error message)
-----------------------------------------------------------------------------*/

V_COUNT         NUMBER;
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    PROGRAM_TO_SOURCE_MAPPING
WHERE   PROGRAM_ID = IN_PROGRAM_ID;

IF V_COUNT > 0 THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'PROGRAM ASSOCIATED TO A SOURCE';

ELSE
        DELETE FROM PROGRAM_SECTION_MAPPING
        WHERE   PROGRAM_ID = IN_PROGRAM_ID;

        DELETE FROM PROGRAM_MAPPING
        WHERE   PROGRAM_ID = IN_PROGRAM_ID;

        OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_PROGRAM_MAPPING;



PROCEDURE DELETE_EMAIL_SECTIONS
(
IN_SECTION_ID                   IN EMAIL_SECTION_CONTENT.SECTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting email sections and the
        it's associated section content for the given section id.  A
        section can only be deleted if it is not associated to any programs or
        if it and the associated programs are both expired.  If not, return
        an error message.

Input:
        section_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (CANNOT DELETE THE SPECIFIED SECTION,
                                                  error message)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_DEFAULT_SECTION       EMAIL_SECTIONS.DEFAULT_SECTION%TYPE;
V_SECTION_END_DATE      EMAIL_SECTIONS.END_DATE%TYPE;
V_DEFAULT_PROGRAM       PROGRAM_MAPPING.DEFAULT_PROGRAM%TYPE;
V_PROGRAM_END_DATE      PROGRAM_MAPPING.END_DATE%TYPE;
V_HTML_SECTION_ID       EMAIL_SECTIONS.SECTION_ID%TYPE;

CURSOR HTML_CUR IS
        SELECT  SECTION_ID
        FROM    EMAIL_SECTIONS A
        WHERE   EXISTS
        (       SELECT  1
                FROM    EMAIL_SECTIONS B
                WHERE   B.SECTION_ID = IN_SECTION_ID
                AND     B.SECTION_ID <> A.SECTION_ID
                AND     B.SECTION_TYPE = A.SECTION_TYPE
                AND     B.SECTION_NAME = A.SECTION_NAME
                AND     B.START_DATE = A.START_DATE
                AND     B.DEFAULT_SECTION = A.DEFAULT_SECTION
        );

BEGIN

-- find if the section is associated to any programs.  if it is then
-- check if both the section or any associated program are active.
SELECT  COUNT (X)
INTO    V_COUNT
FROM
(
        SELECT  1 AS X
        FROM    PROGRAM_SECTION_MAPPING
        WHERE   SECTION_ID = IN_SECTION_ID
        UNION ALL
        SELECT  1
        FROM    EMAIL_SECTIONS ES
        LEFT OUTER JOIN PROGRAM_SECTION_MAPPING PSM
        ON      ES.SECTION_ID = PSM.SECTION_ID
        LEFT OUTER JOIN PROGRAM_MAPPING PM
        ON      PSM.PROGRAM_ID = PM.PROGRAM_ID
        WHERE   ES.SECTION_ID = IN_SECTION_ID
        AND     ES.END_DATE > SYSDATE
        AND     PM.END_DATE > SYSDATE
) A
;

-- a section can only be deleted if it is not associated a program
-- or if both it and the associated programs are expired.
IF V_COUNT > 0 THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'CANNOT DELETE THE SPECIFIED SECTION';

ELSE
        -- find the corresponding html record if the given id
        -- is the text record of a section.  delete both records.
        OPEN HTML_CUR;
        FETCH HTML_CUR INTO V_HTML_SECTION_ID;
        CLOSE HTML_CUR;

        DELETE FROM EMAIL_SECTION_CONTENT
        WHERE   SECTION_ID IN (IN_SECTION_ID, V_HTML_SECTION_ID);

        DELETE FROM EMAIL_SECTIONS
        WHERE   SECTION_ID IN (IN_SECTION_ID, V_HTML_SECTION_ID);

        OUT_STATUS := 'Y';

END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_EMAIL_SECTIONS;


PROCEDURE DELETE_EMAIL_SECTION_CONTENT
(
IN_SECTION_ID                   IN EMAIL_SECTION_CONTENT.SECTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting email section content
        information for the given section id

Input:
        section_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM EMAIL_SECTION_CONTENT
WHERE   SECTION_ID = IN_SECTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_EMAIL_SECTION_CONTENT;


PROCEDURE DELETE_PROGRAM_SECTION_MAPPING
(
IN_PROGRAM_ID                   IN PROGRAM_SECTION_MAPPING.PROGRAM_ID%TYPE,
IN_SECTION_ID                   IN PROGRAM_SECTION_MAPPING.SECTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting program to section mapping
        information.

Input:
        program_id                      number
        section_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM PROGRAM_SECTION_MAPPING
WHERE   PROGRAM_ID = IN_PROGRAM_ID
AND     SECTION_ID = IN_SECTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_PROGRAM_SECTION_MAPPING;


PROCEDURE UPDATE_DEFAULT_PROGRAM_SOURCE
(
IN_SOURCE_CODE                  IN PROGRAM_TO_SOURCE_MAPPING.SOURCE_CODE%TYPE,
IN_PROGRAM_ID                   IN PROGRAM_TO_SOURCE_MAPPING.PROGRAM_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a program to source mapping
        for the default program.

Input:
        source_code                     varchar2
        program_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM PROGRAM_TO_SOURCE_MAPPING PSM
WHERE   PSM.SOURCE_CODE = IN_SOURCE_CODE
AND     EXISTS
        (       SELECT  1
                FROM    PROGRAM_MAPPING PM
                WHERE   PROGRAM_ID = PSM.PROGRAM_ID
                AND     PM.DEFAULT_PROGRAM = 'Y'
        );

INSERT INTO PROGRAM_TO_SOURCE_MAPPING
(
        PROGRAM_ID,
        SOURCE_CODE
)
VALUES
(
        IN_PROGRAM_ID,
        IN_SOURCE_CODE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END UPDATE_DEFAULT_PROGRAM_SOURCE;


PROCEDURE UPDATE_CUSTOM_PROGRAM_SOURCE
(
IN_SOURCE_CODE                  IN PROGRAM_TO_SOURCE_MAPPING.SOURCE_CODE%TYPE,
IN_PROGRAM_ID                   IN PROGRAM_TO_SOURCE_MAPPING.PROGRAM_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a program to source mapping
        for a custom program.

Input:
        source_code                     varchar2
        program_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (ERROR OCCURRED)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO PROGRAM_TO_SOURCE_MAPPING
(
        PROGRAM_ID,
        SOURCE_CODE
)
VALUES
(
        IN_PROGRAM_ID,
        IN_SOURCE_CODE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END UPDATE_CUSTOM_PROGRAM_SOURCE;


PROCEDURE RESET_CUSTOM_PROGRAM_SOURCE
(
IN_PROGRAM_ID                   IN PROGRAM_TO_SOURCE_MAPPING.PROGRAM_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a program to source mapping
        for a custom program.

Input:
        program_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (CANNOT RESET DEFAULT PROGRAMS)
-----------------------------------------------------------------------------*/

V_TEST  CHAR (1);

BEGIN

SELECT  'X'
INTO    V_TEST
FROM    PROGRAM_MAPPING
WHERE   PROGRAM_ID = IN_PROGRAM_ID
AND     DEFAULT_PROGRAM = 'N';

DELETE FROM PROGRAM_TO_SOURCE_MAPPING
WHERE   PROGRAM_ID = IN_PROGRAM_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'CANNOT RESET DEFAULT PROGRAMS';
END;    -- end no data found exception

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END RESET_CUSTOM_PROGRAM_SOURCE;


END EMAIL_MAINT_PKG;
.
/
