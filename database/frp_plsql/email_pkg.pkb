CREATE OR REPLACE
PACKAGE BODY frp.EMAIL_PKG AS

PROCEDURE INSERT_EMAIL_STYLE_SHEET
(
IN_FILENAME                     IN EMAIL_STYLE_SHEET.FILENAME%TYPE,
OUT_EMAIL_STYLE_SHEET_ID        OUT EMAIL_STYLE_SHEET.EMAIL_STYLE_SHEET_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email style sheet information.
        ** needs to be updated with table changes or deleted if not used

Input:
        filename                        varchar2

Output:
        email_style_sheet_id            number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OUT_EMAIL_STYLE_SHEET_ID := KEY.KEYGEN('EMAIL_STYLE_SHEET');

INSERT INTO EMAIL_STYLE_SHEET
(
        EMAIL_STYLE_SHEET_ID,
        FILENAME
)
VALUES
(
        OUT_EMAIL_STYLE_SHEET_ID,
        IN_FILENAME
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_STYLE_SHEET;


PROCEDURE INSERT_EMAIL_CONFIRMATION
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
IN_EMAIL_STYLE_SHEET_ID         IN EMAIL_CONFIRMATION.EMAIL_STYLE_SHEET_ID%TYPE,
IN_TO_ADDRESS                   IN EMAIL_CONFIRMATION.TO_ADDRESS%TYPE,
IN_FROM_ADDRESS                 IN EMAIL_CONFIRMATION.FROM_ADDRESS%TYPE,
IN_SUBJECT                      IN EMAIL_CONFIRMATION.SUBJECT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email confirmation information.

Input:
        email_confirmation_guid         varchar2
        email_style_sheet_id            number
        to_address                      varchar2
        from_address                    varchar2
        subject                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO EMAIL_CONFIRMATION
(
        EMAIL_CONFIRMATION_GUID,
        EMAIL_STYLE_SHEET_ID,
        TO_ADDRESS,
        FROM_ADDRESS,
        SUBJECT
)
VALUES
(
        IN_EMAIL_CONFIRMATION_GUID,
        IN_EMAIL_STYLE_SHEET_ID,
        IN_TO_ADDRESS,
        IN_FROM_ADDRESS,
        IN_SUBJECT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_CONFIRMATION;


PROCEDURE INSERT_EMAIL_CONTENT
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONTENT.EMAIL_CONFIRMATION_GUID%TYPE,
IN_SECTION                      IN EMAIL_CONTENT.SECTION%TYPE,
IN_CONTENT                      IN EMAIL_CONTENT.CONTENT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email content information.

Input:
        email confirmation guid         varchar2
        section                         number
        content                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO EMAIL_CONTENT
(
        EMAIL_CONFIRMATION_GUID,
        SECTION,
        CONTENT
)
VALUES
(
        IN_EMAIL_CONFIRMATION_GUID,
        IN_SECTION,
        IN_CONTENT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_EMAIL_CONTENT;


PROCEDURE UPDATE_EMAIL_STYLE_SHEET
(
IN_EMAIL_STYLE_SHEET_ID         IN EMAIL_STYLE_SHEET.EMAIL_STYLE_SHEET_ID%TYPE,
IN_FILENAME                     IN EMAIL_STYLE_SHEET.FILENAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email style sheet information.
        ** needs to be updated with table changes or deleted if not used

Input:
        email_style_sheet_id            number
        filename                        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_STYLE_SHEET
SET     FILENAME = IN_FILENAME
WHERE   EMAIL_STYLE_SHEET_ID = IN_EMAIL_STYLE_SHEET_ID;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_STYLE_SHEET;


PROCEDURE UPDATE_EMAIL_CONFIRMATION
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
IN_EMAIL_STYLE_SHEET_ID         IN EMAIL_CONFIRMATION.EMAIL_STYLE_SHEET_ID%TYPE,
IN_TO_ADDRESS                   IN EMAIL_CONFIRMATION.TO_ADDRESS%TYPE,
IN_FROM_ADDRESS                 IN EMAIL_CONFIRMATION.FROM_ADDRESS%TYPE,
IN_SUBJECT                      IN EMAIL_CONFIRMATION.SUBJECT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email confirmation information.

Input:
        email_confirmation_guid         varchar2
        email_style_sheet_id            number
        to_address                      varchar2
        from_address                    varchar2
        subject                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_CONFIRMATION
SET     EMAIL_STYLE_SHEET_ID = IN_EMAIL_STYLE_SHEET_ID,
        TO_ADDRESS = IN_TO_ADDRESS,
        FROM_ADDRESS = IN_FROM_ADDRESS,
        SUBJECT = IN_SUBJECT
WHERE   EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_CONFIRMATION;


PROCEDURE UPDATE_EMAIL_CONTENT
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONTENT.EMAIL_CONFIRMATION_GUID%TYPE,
IN_SECTION                      IN EMAIL_CONTENT.SECTION%TYPE,
IN_CONTENT                      IN EMAIL_CONTENT.CONTENT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email content information.

Input:
        email confirmation guid         varchar2
        section                         number
        content                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE EMAIL_CONTENT
SET     CONTENT = IN_CONTENT
WHERE   EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID
AND     SECTION = IN_SECTION;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_EMAIL_CONTENT;


PROCEDURE DELETE_EMAIL_SECTIONS
(
IN_SECTION_ID                   IN EMAIL_SECTION_CONTENT.SECTION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting email sections and the
        it's associated section content for the given section id.

Input:
        section_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM EMAIL_SECTION_CONTENT
WHERE   SECTION_ID = IN_SECTION_ID;

DELETE FROM EMAIL_SECTIONS
WHERE   SECTION_ID = IN_SECTION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_EMAIL_SECTIONS;




PROCEDURE VIEW_EMAIL_CONFIRMATION
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given email confirmation
        information.

Input:
        email_confirmation_guid         varchar2

Output:
        cursor result set containing email confirmation and style sheet information
        email_confirmation_guid         varchar2
        email_style_sheet_id            number
        to_address                      varchar2
        from_address                    varchar2
        subject                         varchar2
        filename                        varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  C.EMAIL_CONFIRMATION_GUID,
                C.EMAIL_STYLE_SHEET_ID,
                C.TO_ADDRESS,
                C.FROM_ADDRESS,
                C.SUBJECT,
                SS.FILENAME
        FROM    EMAIL_CONFIRMATION C
        JOIN    EMAIL_STYLE_SHEET SS
        ON      C.EMAIL_STYLE_SHEET_ID = SS.EMAIL_STYLE_SHEET_ID
        WHERE   C.EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID;

END VIEW_EMAIL_CONFIRMATION;


PROCEDURE VIEW_EMAIL_CONTENT
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given email confirmation
        content information.

Input:
        email_confirmation_guid         varchar2

Output:
        cursor result set containing email confirmation content information
        email_confirmation_guid         varchar2
        section                         number
        content                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  EMAIL_CONFIRMATION_GUID,
                SECTION,
                CONTENT
        FROM    EMAIL_CONTENT
        WHERE   EMAIL_CONFIRMATION_GUID = IN_EMAIL_CONFIRMATION_GUID
        ORDER BY CONTENT;

END VIEW_EMAIL_CONTENT;


PROCEDURE VIEW_EMAIL_CONFIRMATION_INFO
(
IN_EMAIL_CONFIRMATION_GUID      IN EMAIL_CONFIRMATION.EMAIL_CONFIRMATION_GUID%TYPE,
OUT_CONFIRMATION_CUR            OUT TYPES.REF_CURSOR,
OUT_CONTENT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all information
        for the given email confirmation guid.

Input:
        email_confirmation_guid         varchar2

Output:
        confirmation cursor
        content cursor
-----------------------------------------------------------------------------*/

BEGIN

VIEW_EMAIL_CONFIRMATION (IN_EMAIL_CONFIRMATION_GUID, OUT_CONFIRMATION_CUR);
VIEW_EMAIL_CONTENT (IN_EMAIL_CONFIRMATION_GUID, OUT_CONTENT_CUR);

END VIEW_EMAIL_CONFIRMATION_INFO;

END EMAIL_PKG;
.
/
