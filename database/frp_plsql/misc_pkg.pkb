CREATE OR REPLACE PACKAGE BODY  frp.MISC_PKG AS

FUNCTION GET_GLOBAL_PARM_VALUE
(
IN_CONTEXT      IN GLOBAL_PARMS.CONTEXT%TYPE,
IN_NAME         IN GLOBAL_PARMS.NAME%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the value for the given
        global parm.

Input:
        name                            varchar2

Output:
        value                           varchar2

-----------------------------------------------------------------------------*/

V_VALUE         GLOBAL_PARMS.VALUE%TYPE;

BEGIN

SELECT  VALUE
INTO    V_VALUE
FROM    GLOBAL_PARMS
WHERE   CONTEXT = IN_CONTEXT
AND     NAME = IN_NAME;

RETURN V_VALUE;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_GLOBAL_PARM_VALUE;


PROCEDURE VIEW_GLOBAL_PARMS
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all global parms.

Input:
        none

Output:
        cursor result set containing all fields global parms
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  CONTEXT,
                NAME,
                VALUE,
                DESCRIPTION
        FROM    GLOBAL_PARMS
        ORDER BY CONTEXT, NAME;

END VIEW_GLOBAL_PARMS;


PROCEDURE VIEW_PARMS_BY_CONTEXT
(
IN_CONTEXT      IN  GLOBAL_PARMS.CONTEXT%TYPE,
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all global parms
        for the given context.

Input:
        none

Output:
        cursor result set containing all fields global parms
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  CONTEXT,
                NAME,
                VALUE,
                DESCRIPTION
        FROM    GLOBAL_PARMS
        WHERE   CONTEXT = IN_CONTEXT
        ORDER BY CONTEXT, NAME;

END VIEW_PARMS_BY_CONTEXT;


PROCEDURE VIEW_UNIQUE_CONTEXT
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning unique context from global parms.

Input:
        none

Output:
        cursor result set containing distinct context
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT CONTEXT
        FROM    GLOBAL_PARMS
        ORDER BY CONTEXT;

END VIEW_UNIQUE_CONTEXT;

PROCEDURE PAS_CHECK_GLOBAL_PARMS
(
IN_CONTEXT    IN GLOBAL_PARMS.CONTEXT%TYPE,
IN_NAME       IN GLOBAL_PARMS.NAME%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure checks the global parm to see if it should trigger a
        product avaialability process to kick off.
-----------------------------------------------------------------------------*/
BEGIN

  IF (IN_CONTEXT = 'FTDAPPS_PARMS') THEN
    IF (IN_NAME = 'EXOTIC_CUTOFF') THEN
      PAS.POST_PAS_COMMAND('MODIFIED_CUTOFF_EXOTIC','');  
    ELSIF (IN_NAME = 'SATURDAY_CUTOFF') THEN
      PAS.POST_PAS_COMMAND('MODIFIED_CUTOFF_SATURDAY','');  
    ELSIF (IN_NAME = 'SUNDAY_CUTOFF') THEN
      PAS.POST_PAS_COMMAND('MODIFIED_CUTOFF_SUNDAY','');  
    ELSIF (IN_NAME = 'LATEST_CUTOFF') THEN
      PAS.POST_PAS_COMMAND('MODIFIED_CUTOFF_LATEST','');  
    END IF;
  END IF;
  IF (IN_CONTEXT = 'SHIPPING_PARMS') THEN
    IF (IN_NAME = 'LASTEST_CUTOFF_VENDOR') THEN
      PAS.POST_PAS_COMMAND('MODIFIED_CUTOFF_LASTEST_VENDOR','');  
    END IF;
  END IF;

END PAS_CHECK_GLOBAL_PARMS;


PROCEDURE UPDATE_GLOBAL_PARMS
(
IN_CONTEXT                      IN GLOBAL_PARMS.CONTEXT%TYPE,
IN_NAME                         IN GLOBAL_PARMS.NAME%TYPE,
IN_VALUE                        IN GLOBAL_PARMS.VALUE%TYPE,
IN_UPDATED_BY						  IN GLOBAL_PARMS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting global parameters.
        This is an overloaded procedure that does NOT affect the description column.

Input:
        context                         varchar2
        name                            varchar2
        value                           varchar2
        updated_by							 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT  INTO GLOBAL_PARMS
(
        CONTEXT,
        NAME,
        VALUE,
        CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON
)
VALUES
(
        IN_CONTEXT,
        IN_NAME,
        IN_VALUE,
        UPPER(IN_UPDATED_BY),
        SYSDATE,
        UPPER(IN_UPDATED_BY),
        SYSDATE
);

OUT_STATUS := 'Y';
PAS_CHECK_GLOBAL_PARMS(IN_CONTEXT, IN_NAME);

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        UPDATE  GLOBAL_PARMS
        SET     VALUE = IN_VALUE,
        			 UPDATED_BY = UPPER(IN_UPDATED_BY),
        			 UPDATED_ON = SYSDATE
        WHERE   CONTEXT = IN_CONTEXT
        AND     NAME = IN_NAME;

        PAS_CHECK_GLOBAL_PARMS(IN_CONTEXT, IN_NAME);
        OUT_STATUS := 'Y';
END;    -- end duplicate exception

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END UPDATE_GLOBAL_PARMS;



PROCEDURE UPDATE_GLOBAL_PARMS
(
IN_CONTEXT                      IN GLOBAL_PARMS.CONTEXT%TYPE,
IN_NAME                         IN GLOBAL_PARMS.NAME%TYPE,
IN_VALUE                        IN GLOBAL_PARMS.VALUE%TYPE,
IN_DESCRIPTION                  IN GLOBAL_PARMS.DESCRIPTION%TYPE,
IN_UPDATED_BY			IN GLOBAL_PARMS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting global parameters.

Input:
        context                         varchar2
        name                            varchar2
        value                           varchar2
        description                     varchar2
        updated_by			varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT  INTO GLOBAL_PARMS
(
        CONTEXT,
        NAME,
        VALUE,
        DESCRIPTION,
        CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON
)
VALUES
(
        IN_CONTEXT,
        IN_NAME,
        IN_VALUE,
        IN_DESCRIPTION,
        UPPER(IN_UPDATED_BY),
        SYSDATE,
        UPPER(IN_UPDATED_BY),
        SYSDATE
);

PAS_CHECK_GLOBAL_PARMS(IN_CONTEXT, IN_NAME);
OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        UPDATE  GLOBAL_PARMS
        SET     VALUE = IN_VALUE,
                DESCRIPTION = IN_DESCRIPTION,
                UPDATED_BY = UPPER(IN_UPDATED_BY),
                UPDATED_ON = SYSDATE
        WHERE   CONTEXT = IN_CONTEXT
        AND     NAME = IN_NAME;

        PAS_CHECK_GLOBAL_PARMS(IN_CONTEXT, IN_NAME);
        OUT_STATUS := 'Y';

END;    -- end duplicate exception

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_GLOBAL_PARMS;


PROCEDURE DELETE_GLOBAL_PARMS
(
IN_CONTEXT                      IN GLOBAL_PARMS.CONTEXT%TYPE,
IN_NAME                         IN GLOBAL_PARMS.NAME%TYPE,
IN_UPDATED_BY						  IN GLOBAL_PARMS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting global parameters.

Input:
        context                         varchar2
        name                            varchar2
        updated_by							 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  GLOBAL_PARMS
SET     UPDATED_BY = UPPER(IN_UPDATED_BY) || ' - DELETE',
		  UPDATED_ON = SYSDATE
WHERE   CONTEXT = IN_CONTEXT
AND     NAME = IN_NAME;

OUT_STATUS := 'Y';

DELETE FROM GLOBAL_PARMS
WHERE   CONTEXT = IN_CONTEXT
AND     NAME = IN_NAME;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_GLOBAL_PARMS;


PROCEDURE INSERT_SYSTEM_MESSAGES
(
IN_SOURCE                       IN SYSTEM_MESSAGES.SOURCE%TYPE,
IN_TYPE                         IN SYSTEM_MESSAGES.TYPE%TYPE,
IN_MESSAGE                      IN SYSTEM_MESSAGES.MESSAGE%TYPE,
IN_COMPUTER                     IN SYSTEM_MESSAGES.COMPUTER%TYPE,
IN_EMAIL_SUBJECT                IN SYSTEM_MESSAGES.EMAIL_SUBJECT%TYPE,
OUT_SYSTEM_MESSAGE_ID           OUT SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting system messages.

Input:
        source                          varchar2
        type                            varchar2
        message                         varchar2
        email_subject                   varchar2

Output:
        system_message_id               number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

OUT_SYSTEM_MESSAGE_ID := KEY.KEYGEN('SYSTEM_MESSAGES');

INSERT  INTO SYSTEM_MESSAGES
(
        SOURCE,
        TYPE,
        MESSAGE,
        TIMESTAMP,
        SYSTEM_MESSAGE_ID,
        COMPUTER,
        EMAIL_SUBJECT
)
VALUES
(
        NVL(IN_SOURCE,'unknown'),
        IN_TYPE,
        IN_MESSAGE,
        SYSDATE,
        OUT_SYSTEM_MESSAGE_ID,
        IN_COMPUTER,
        IN_EMAIL_SUBJECT
);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_SYSTEM_MESSAGES;

PROCEDURE INSERT_SYSTEM_MESSAGES
(
IN_SOURCE                       IN SYSTEM_MESSAGES.SOURCE%TYPE,
IN_TYPE                         IN SYSTEM_MESSAGES.TYPE%TYPE,
IN_MESSAGE                      IN SYSTEM_MESSAGES.MESSAGE%TYPE,
IN_COMPUTER                     IN SYSTEM_MESSAGES.COMPUTER%TYPE,
OUT_SYSTEM_MESSAGE_ID           OUT SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting system messages
        overloading the procedure above defaulting the email subject
        to null.

Input:
        source                          varchar2
        type                            varchar2
        message                         varchar2

Output:
        system_message_id               number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT_SYSTEM_MESSAGES
(
        NVL(IN_SOURCE,'unknown'),
        IN_TYPE,
        IN_MESSAGE,
        IN_COMPUTER,
        NULL,
        OUT_SYSTEM_MESSAGE_ID,
        OUT_STATUS,
        OUT_MESSAGE
);

END INSERT_SYSTEM_MESSAGES;


PROCEDURE UPDATE_SYSTEM_MESSAGES_READ
(
IN_SOURCE                       IN SYSTEM_MESSAGES.SOURCE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating all messages to read.

Input:
        source                          varchar2 (optional)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE SYSTEM_MESSAGES
SET     READ = 'Y'
WHERE   (SOURCE = IN_SOURCE OR IN_SOURCE IS NULL)
AND     READ = 'N';

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_SYSTEM_MESSAGES_READ;

PROCEDURE UPDATE_SYSTEM_MESSAGES_READ_ID
(
IN_SYSTEM_MESSAGE_ID            IN SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE,
IN_READ_FLAG                    IN SYSTEM_MESSAGES.READ%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the read status of a single
	message.

Input:
        system_message_id               number
        read                            char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE SYSTEM_MESSAGES
SET     READ = IN_READ_FLAG
WHERE   SYSTEM_MESSAGE_ID = IN_SYSTEM_MESSAGE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_SYSTEM_MESSAGES_READ_ID;

PROCEDURE UPDATE_SYSTEM_MESSAGES_ESCL_ID
(
IN_SYSTEM_MESSAGE_ID            IN SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE,
IN_ESCALATION_LEVEL             IN SYSTEM_MESSAGES.ESCALATION_LEVEL%TYPE,
IN_ESCALATION_TIME              IN SYSTEM_MESSAGES.ESCALATION_TIME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the escalation levels of
        a single message

Input:
        system_message_id               number
        escalation_level                number
        escalation_time                 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE SYSTEM_MESSAGES
SET     ESCALATION_LEVEL = IN_ESCALATION_LEVEL,
        ESCALATION_TIME  = IN_ESCALATION_TIME
WHERE   SYSTEM_MESSAGE_ID = IN_SYSTEM_MESSAGE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_SYSTEM_MESSAGES_ESCL_ID;



PROCEDURE DELETE_SYSTEM_MESSAGES
(
IN_SYSTEM_MESSAGE_ID            IN SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting system messages.

Input:
        system_message_id               number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

DELETE FROM SYSTEM_MESSAGES
WHERE   SYSTEM_MESSAGE_ID = IN_SYSTEM_MESSAGE_ID;

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END DELETE_SYSTEM_MESSAGES;


PROCEDURE VIEW_SYSTEM_MESSAGES
(
IN_READ_FLAG    IN  SYSTEM_MESSAGES.READ%TYPE,
IN_SOURCE       IN  SYSTEM_MESSAGES.SOURCE%TYPE,
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning system messages.  If the
        read flag is 'NEW' return only records that have not been read.  Otherwise,
        return all.  In both cases, for the records returned that have not
        been read, update them to indicate that they have been read.

Input:
        read_flag                       varchar2

Output:
        cursor result set containing system message information


-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT  SOURCE,
        TYPE,
        MESSAGE,
        TIMESTAMP,
        SYSTEM_MESSAGE_ID,
        COMPUTER
FROM    SYSTEM_MESSAGES
WHERE   ((READ = 'N' AND IN_READ_FLAG = 'NEW') OR IN_READ_FLAG = 'ALL')
AND     (SOURCE = IN_SOURCE OR IN_SOURCE IS NULL)
ORDER BY TIMESTAMP DESC;

END VIEW_SYSTEM_MESSAGES;


PROCEDURE VIEW_SYSTEM_MESSAGES_SOURCES
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the sources that exist
        in system messages

Input:
        none

Output:
        cursor result set containing system message sources


-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT
                SOURCE
        FROM    SYSTEM_MESSAGES;

END VIEW_SYSTEM_MESSAGES_SOURCES;

PROCEDURE VIEW_SYSTEM_MESSAGES_BY_ID
(
IN_SYSTEM_MESSAGE_ID    IN  SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a single system messages.

Input:
        system_message_id               number

Output:
        cursor result set containing system message information


-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT  SYSTEM_MESSAGE_ID,
        SOURCE,
        TYPE,
        MESSAGE,
        READ,
        TO_TIMESTAMP(TO_CHAR(TIMESTAMP, 'DD-MON-YYYY HH:MI:SS')) AS TIMESTAMP,
        COMPUTER,
        EMAIL_SUBJECT,
        TO_TIMESTAMP(TO_CHAR(ESCALATION_TIME, 'DD-MON-YYYY HH:MI:SS')) AS ESCALATION_TIME,
        ESCALATION_LEVEL
FROM    SYSTEM_MESSAGES
WHERE   SYSTEM_MESSAGE_ID = IN_SYSTEM_MESSAGE_ID;

END VIEW_SYSTEM_MESSAGES_BY_ID;

PROCEDURE VIEW_SYSTEM_MSGS_UNREAD_ESCL
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning system messages.  Only unread
        messages where the escalation time is in the past, or no escalation level
	are returned.

Input:

Output:
        cursor result set containing system message information


-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT  SYSTEM_MESSAGE_ID,
        SOURCE,
        TYPE,
        MESSAGE,
        READ,
        TO_TIMESTAMP(TO_CHAR(TIMESTAMP, 'DD-MON-YYYY HH:MI:SS')) AS TIMESTAMP,
        COMPUTER,
        EMAIL_SUBJECT,
        TO_TIMESTAMP(TO_CHAR(ESCALATION_TIME, 'DD-MON-YYYY HH:MI:SS')) AS ESCALATION_TIME,
        ESCALATION_LEVEL
FROM    SYSTEM_MESSAGES
WHERE   (READ = 'N')
AND     ((ESCALATION_TIME < SYSDATE) OR ESCALATION_TIME IS NULL);

END VIEW_SYSTEM_MSGS_UNREAD_ESCL;

PROCEDURE VIEW_ADDRESS_TYPES
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all address types.

Input:
        none

Output:
        cursor result set containing all fields in forbidden words
        word                            varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ADDRESS_TYPE,
                CODE,
                DESCRIPTION,
		PROMPT_FOR_BUSINESS_FLAG,
                PROMPT_FOR_ROOM_FLAG,
                ROOM_LABEL_TXT,	
                PROMPT_FOR_HOURS_FLAG,	
                HOURS_LABEL_TXT,
                BUSINESS_LABEL_TXT
        FROM    ADDRESS_TYPES
        ORDER BY SORT_ORDER;
END VIEW_ADDRESS_TYPES;



PROCEDURE VIEW_STATUS_MAPPING
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all statuses.

Input:
        none

Output:
        cursor result set containing all fields in status mapping

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  STATUS,
                DESCRIPTION,
                DISPLAY_ORDER
        FROM    STATUS_MAPPING;

END VIEW_STATUS_MAPPING;



PROCEDURE INSERT_STATS_OSP
(
IN_MASTER_ORDER_NUMBER          IN  STATS_OSP.MASTER_ORDER_NUMBER%TYPE,
IN_EXTERNAL_ORDER_NUMBER        IN  STATS_OSP.EXTERNAL_ORDER_NUMBER%TYPE,
IN_STATUS                       IN  STATS_OSP.STATUS%TYPE,
IN_STATE                        IN  STATS_OSP.ITEM_STATE%TYPE,
IN_TIME                         IN  STATS_OSP.TIME_FIELD%TYPE,
IN_CSR_ID                       IN  STATS_OSP.CSR_ID%TYPE,
IN_ORDER_ORIGIN                 IN  STATS_OSP.ORDER_ORIGIN%TYPE,
IN_DATE                         IN  STATS_OSP.TIMESTAMP%TYPE,
IN_RELATOR                      IN  STATS_OSP.RELATOR%TYPE,
IN_TEST_ORDER_FLAG              IN  STATS_OSP.TEST_ORDER_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting stats_osp.

Input:
        master_order_number             varchar2
        external_order_number           varchar2
        status                          varchar2
        state                           varchar2
        time                            number
        csr_id                          varchar2
        order_origin                    varchar2
        timestamp                       date
        relator                         varchar2
        test_order_flag                 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT  INTO STATS_OSP
(
        MASTER_ORDER_NUMBER,
        EXTERNAL_ORDER_NUMBER,
        STATUS,
        ITEM_STATE,
        TIME_FIELD,
        CSR_ID,
        ORDER_ORIGIN,
        TIMESTAMP,
        RELATOR,
        TEST_ORDER_FLAG
)
VALUES
(
        IN_MASTER_ORDER_NUMBER,
        IN_EXTERNAL_ORDER_NUMBER,
        IN_STATUS,
        IN_STATE,
        IN_TIME,
        IN_CSR_ID,
        IN_ORDER_ORIGIN,
        IN_DATE,
        IN_RELATOR,
        IN_TEST_ORDER_FLAG
);

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_STATS_OSP;


PROCEDURE UPDATE_STATS_VALIDATION
(
IN_EXTERNAL_ORDER_NUMBER        IN STATS_VALIDATION.EXTERNAL_ORDER_NUMBER%TYPE,
IN_RELATOR                      IN STATS_VALIDATION.RELATOR%TYPE,
OUT_STATS_VALIDATION_ID         OUT STATS_VALIDATION.STATS_VALIDATION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating stats validation.
        If the external order number already exists, update the record.  Otherwise
        insert a new record.

Input:
        external_order_number           varchar2
        relator                         varchar2

Output:
        stats_validation_id             number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

SELECT  STATS_VALIDATION_ID
INTO    OUT_STATS_VALIDATION_ID
FROM    STATS_VALIDATION
WHERE   EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

UPDATE STATS_VALIDATION
SET     RELATOR = IN_RELATOR,
        TIMESTAMP = SYSDATE
WHERE   EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        OUT_STATS_VALIDATION_ID := KEY.KEYGEN('STATS_VALIDATION');

        INSERT  INTO STATS_VALIDATION
        (
                STATS_VALIDATION_ID,
                EXTERNAL_ORDER_NUMBER,
                RELATOR,
                TIMESTAMP
        )
        VALUES
        (
                OUT_STATS_VALIDATION_ID,
                IN_EXTERNAL_ORDER_NUMBER,
                IN_RELATOR,
                SYSDATE
        );

        OUT_STATUS := 'Y';
        COMMIT;
END;    -- end no data found exception

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_STATS_VALIDATION;


PROCEDURE INSERT_STATS_VALIDATE_REASONS
(
IN_STATS_VALIDATION_ID          IN STATS_VALIDATION_REASONS.STATS_VALIDATION_ID%TYPE,
IN_REASON                       IN STATS_VALIDATION_REASONS.REASON%TYPE,
IN_ERROR_VALUE                  IN STATS_VALIDATION_REASONS.ERROR_VALUE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting stats validation reasons.

Input:
        stats_validation_id             number
        reason                          varchar2
        error_value                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;
V_STATS_VALIDATION_REASONS_ID     STATS_VALIDATION_REASONS.STATS_VALIDATION_REASONS_ID%TYPE;

BEGIN

V_STATS_VALIDATION_REASONS_ID := KEY.KEYGEN('STATS_VALIDATION_REASONS');

INSERT  INTO STATS_VALIDATION_REASONS
(
        STATS_VALIDATION_REASONS_ID,
        STATS_VALIDATION_ID,
        REASON,
        ERROR_VALUE
)
VALUES
(
        V_STATS_VALIDATION_REASONS_ID,
        IN_STATS_VALIDATION_ID,
        IN_REASON,
        IN_ERROR_VALUE
);

OUT_STATUS := 'Y';
COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_STATS_VALIDATE_REASONS;


PROCEDURE VIEW_DELETE_ORDER_SEQUENCE
(
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

V_PREFIX_IGNORE VARCHAR2(100);

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving all available order
        sequence numbers and deleting the records returned.

Input:
        none

Output:
        cursor result set containing all fields in order_sequence
        external_order_number           varchar2
        timestamp                       date

        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

-- We don't want to perform sequence check for Ariba order prefixes, so remove them from order_sequence table.
-- Note that PREFIX_IGNORE will contain Ariba prefix, but may contain other prefixes in future.
--
SELECT VALUE INTO V_PREFIX_IGNORE FROM frp.global_parms WHERE context='SEQUENCE_CHECKER' AND name='PREFIX_IGNORE'; 
DELETE FROM ORDER_SEQUENCE
WHERE  REGEXP_LIKE(external_order_number, V_PREFIX_IGNORE);

EXECUTE IMMEDIATE 'TRUNCATE TABLE ORDER_SEQUENCE_PURGE';

INSERT INTO ORDER_SEQUENCE_PURGE
(
        EXTERNAL_ORDER_NUMBER,
        TIMESTAMP
)
SELECT  EXTERNAL_ORDER_NUMBER,
        TIMESTAMP
FROM    ORDER_SEQUENCE
join    global_parms gp
on      gp.context = 'SEQUENCE_CHECKER'
and     gp.name = 'PROCESSING_DELAY_MINUTES'
WHERE   TIMESTAMP < sysdate - (gp.value/1440);

DELETE FROM ORDER_SEQUENCE
WHERE   EXTERNAL_ORDER_NUMBER IN
        (SELECT EXTERNAL_ORDER_NUMBER FROM ORDER_SEQUENCE_PURGE);

COMMIT;

OPEN OUT_CUR FOR
        SELECT  EXTERNAL_ORDER_NUMBER,
                TIMESTAMP
        FROM    ORDER_SEQUENCE_PURGE
        ORDER BY EXTERNAL_ORDER_NUMBER;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END VIEW_DELETE_ORDER_SEQUENCE;


PROCEDURE GET_GLOBAL_PARAMETER
(
 IN_NAME    IN VARCHAR2,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will retrieve fields name and value from table
        global_parms for the given name.

Input:
        name

Output:
        cursor containing global parameters

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CURSOR FOR
   SELECT name,
          value
     FROM frp.global_parms
     WHERE name = in_name;

END GET_GLOBAL_PARAMETER;

FUNCTION IS_CSR_LAST_IN_SCRUB_ORDER
(
IN_MASTER_ORDER_NUMBER          IN STATS_OSP.MASTER_ORDER_NUMBER%TYPE,
IN_CSR_ID                       IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function will check if the given csr was the last scrub in csr
        for the given order (item state of 3005).

Input:
        order guid              varchar
        csr_id                  varchar

Output:
        Y/N

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
        SELECT  s.csr_id
        FROM    stats_osp s
        WHERE   s.master_order_number = in_master_order_number
        AND     s.item_state = '3005'
        ORDER BY s.TIMESTAMP DESC;

v_csr_id        stats_osp.csr_id%TYPE;
v_return        CHAR(1) := 'N';

BEGIN

-- get the first record in the cursor which should be the last csr
-- who scrubbed the given order
OPEN exists_cur;
FETCH exists_cur INTO v_csr_id;
CLOSE exists_cur;

IF v_csr_id IS NOT NULL AND v_csr_id = in_csr_id THEN
        v_return := 'Y';
END IF;

RETURN v_return;

END IS_CSR_LAST_IN_SCRUB_ORDER;

PROCEDURE INSERT_APPLICATION_COUNTER
(
  IN_APPLICATION_NAME          IN APPLICATION_COUNTER.APPLICATION_NAME%TYPE,
  OUT_STATUS                  OUT VARCHAR2,
  OUT_MESSAGE                 OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO FRP.APPLICATION_COUNTER (
        APPLICATION_NAME,
        TIMESTAMP_DATETIME
    )
    VALUES (
        IN_APPLICATION_NAME,
        SYSDATE
    );

    COMMIT;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END INSERT_APPLICATION_COUNTER;

PROCEDURE INSERT_FILE_ARCHIVE (
	IN_CONTEXT        IN   FILE_ARCHIVE.CONTEXT%TYPE,      
	IN_NAME           IN   FILE_ARCHIVE.NAME%TYPE,         
	IN_FILE_NAME      IN   FILE_ARCHIVE.FILE_NAME%TYPE,    
	IN_FILE_DATA_TXT  IN   FILE_ARCHIVE.FILE_DATA_TXT%TYPE,
	IN_CREATED_ON     IN   FILE_ARCHIVE.CREATED_ON%TYPE,   
	IN_CREATED_BY     IN   FILE_ARCHIVE.CREATED_BY%TYPE,
  OUT_ID            OUT  FILE_ARCHIVE.FILE_ARCHIVE_ID%TYPE,
	OUT_STATUS        OUT  VARCHAR2,
	OUT_MESSAGE       OUT  VARCHAR2   
)
--=======================================================================
--
-- Name:    INSERT_FILE_ARCHIVE
-- Type:    Procedure
-- Syntax:  INSERT_FILE_ARCHIVE
--
-- Description: Inserts a record into the FRP.FILE_ARCHIVE TABLE
--
--========================================================================
AS
  v_created_on FILE_ARCHIVE.CREATED_ON%TYPE;
  v_create_parms NUMBER;
BEGIN
  IF IN_CREATED_ON IS NULL THEN
    v_created_on := SYSDATE;
  ELSE
    v_created_on := IN_CREATED_ON;
  END IF;
  
  --Insert a default record into the parameters table
  BEGIN
    SELECT 1 INTO v_create_parms 
      FROM FILE_ARCHIVE_PARAMETERS 
      WHERE CONTEXT = IN_CONTEXT AND NAME = IN_NAME;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    INSERT INTO FILE_ARCHIVE_PARAMETERS p (p.CONTEXT,p.NAME,p.DAYS_TO_HOLD_QTY,p.LAST_PURGE_DATE)
      VALUES(IN_CONTEXT,IN_NAME,30,SYSDATE);
  END;
  
  SELECT FILE_ARCHIVE_SQ.NEXTVAL INTO OUT_ID FROM DUAL;
  
  INSERT INTO FILE_ARCHIVE (
	  FILE_ARCHIVE_ID, 
	  CONTEXT, 
	  NAME, 
	  FILE_NAME, 
	  FILE_DATA_TXT, 
	  CREATED_ON, 
	  CREATED_BY
	) VALUES (
	  OUT_ID,
	  IN_CONTEXT,      
	  IN_NAME,         
	  IN_FILE_NAME,    
	  IN_FILE_DATA_TXT,
	  v_created_on,   
	  IN_CREATED_BY   
	);

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  BEGIN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    -- end exception block
END INSERT_FILE_ARCHIVE;

FUNCTION GET_FILE_ARCHIVE (
  IN_FILE_ARCHIVE_ID IN FILE_ARCHIVE.FILE_ARCHIVE_ID%TYPE
) RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_FILE_ARCHIVE
-- Type:    Procedure
-- Syntax:  GET_FILE_ARCHIVE
--
-- Description: Retrieves the FRP.FILE_ARCHIVE record for the passed in
--              primary key
--
--========================================================================
AS
  file_cursor types.ref_cursor;
BEGIN

  OPEN file_cursor FOR
  	SELECT 
	    f.FILE_ARCHIVE_ID, 
	    f.CONTEXT, 
	    f.NAME, 
	    f.FILE_NAME, 
	    f.FILE_DATA_TXT, 
	    f.CREATED_ON, 
	    f.CREATED_BY 
	  FROM FRP.FILE_ARCHIVE f 
	  WHERE f.FILE_ARCHIVE_ID = IN_FILE_ARCHIVE_ID
	  ORDER BY f.FILE_ARCHIVE_ID;
    
    RETURN file_cursor;
END GET_FILE_ARCHIVE;

FUNCTION GET_LATEST_FILE_ARCHIVES (
  IN_CONTEXT IN FILE_ARCHIVE.CONTEXT%TYPE,
  IN_NAME IN FILE_ARCHIVE.NAME%TYPE
) RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_LATEST_FILE_ARCHIVES
-- Type:    Procedure
-- Syntax:  GET_LATEST_FILE_ARCHIVES
--
-- Description: Retrieves the newest FRP.FILE_ARCHIVE records 
--              for the passed in context and name
--
--========================================================================
AS
  v_file_archive_id  FILE_ARCHIVE.FILE_ARCHIVE_ID%TYPE;
BEGIN
  	SELECT MAX(f.FILE_ARCHIVE_ID) INTO v_file_archive_id 
	  FROM FRP.FILE_ARCHIVE f 
	  WHERE 
      f.CONTEXT = IN_CONTEXT AND
      f.NAME = IN_NAME;
    
    RETURN GET_RELATED_FILE_ARCHIVES(v_file_archive_id);
END GET_LATEST_FILE_ARCHIVES;

FUNCTION GET_RELATED_FILE_ARCHIVES (
  IN_FILE_ARCHIVE_ID IN FILE_ARCHIVE.FILE_ARCHIVE_ID%TYPE
) RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_RELATED_FILE_ARCHIVES
-- Type:    Procedure
-- Syntax:  GET_RELATED_FILE_ARCHIVES
--
-- Description: Retrieves the FRP.FILE_ARCHIVES which have the same, 
--              context, name, and timestamp as the record for the 
--              passed in primary key.
--
--========================================================================
AS
  file_cursor   types.ref_cursor;
  v_context     FILE_ARCHIVE.CONTEXT%TYPE;
  v_name        FILE_ARCHIVE.NAME%TYPE;
  v_created_on  FILE_ARCHIVE.CREATED_ON%TYPE;
BEGIN

  SELECT 
      f.CONTEXT, 
      f.NAME, 
      f.CREATED_ON 
    INTO
      v_context,
      v_name,
      v_created_on
    FROM FILE_ARCHIVE f 
    WHERE f.FILE_ARCHIVE_ID = IN_FILE_ARCHIVE_ID;

  OPEN file_cursor FOR
  	SELECT 
	    f.FILE_ARCHIVE_ID, 
	    f.CONTEXT, 
	    f.NAME, 
	    f.FILE_NAME, 
	    f.FILE_DATA_TXT, 
	    f.CREATED_ON, 
	    f.CREATED_BY 
	  FROM FRP.FILE_ARCHIVE f 
	  WHERE 
      f.CONTEXT = v_context AND
      f.NAME = v_name AND
      f.CREATED_ON = v_created_on 
	  ORDER BY f.FILE_ARCHIVE_ID;
    
    RETURN file_cursor;
END GET_RELATED_FILE_ARCHIVES;

PROCEDURE PURGE_FILE_ARCHIVE(
	OUT_STATUS        OUT  VARCHAR2,
	OUT_MESSAGE       OUT  VARCHAR2
)
AS
  CURSOR parm_cur IS 
    SELECT p.CONTEXT, 
           p.NAME, 
           p.DAYS_TO_HOLD_QTY 
      FROM FILE_ARCHIVE_PARAMETERS p 
      ORDER BY p.CONTEXT, p.NAME; 
BEGIN
  FOR parm_rec IN parm_cur
    LOOP
      DELETE 
        FROM FILE_ARCHIVE f 
        WHERE 
          f.CONTEXT = parm_rec.CONTEXT AND
          f.NAME = parm_rec.NAME AND
          TRUNC(f.CREATED_ON)+parm_rec.DAYS_TO_HOLD_QTY < TRUNC(SYSDATE);
          
      IF SQL%ROWCOUNT > 0 THEN
        UPDATE FILE_ARCHIVE_PARAMETERS p 
          SET p.LAST_PURGE_DATE = SYSDATE 
          WHERE p.CONTEXT='' and p.NAME='';
      END IF;
      
    END LOOP;

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  BEGIN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    -- end exception block
END PURGE_FILE_ARCHIVE;

FUNCTION GET_FILE_ARCHIVE_PARMS
  RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_FILE_ARCHIVE_PARMS
-- Type:    Procedure
-- Syntax:  GET_FILE_ARCHIVE_PARMS
--
-- Description: Retrieves all the FRP.FILE_ARCHIVE_PARAMETERS records
--
--========================================================================
AS
  parm_cursor types.ref_cursor;
BEGIN
  OPEN parm_cursor FOR
    SELECT 
      p.CONTEXT, 
      p.NAME, 
      p.DAYS_TO_HOLD_QTY, 
      p.LAST_PURGE_DATE 
    FROM FRP.FILE_ARCHIVE_PARAMETERS p 
    ORDER BY p.CONTEXT, p.NAME;
    
    RETURN parm_cursor;
END GET_FILE_ARCHIVE_PARMS;

FUNCTION GET_FILE_ARCHIVE_CATEGORIES
  RETURN types.ref_cursor
--=======================================================================
--
-- Name:    GET_FILE_ARCHIVE_CATEGORIES
-- Type:    Procedure
-- Syntax:  GET_FILE_ARCHIVE_CATEGORIES
--
-- Description: Retrieves all distinct combinations (primary keys) of 
--              context/name in the FILE_ARCHIVE table
--
--========================================================================
AS
  cat_cursor types.ref_cursor;
BEGIN
  OPEN cat_cursor FOR
    SELECT DISTINCT f.CONTEXT, f.NAME 
      FROM FILE_ARCHIVE f 
    ORDER by f.CONTEXT, f.NAME;
    
    RETURN cat_cursor;
END GET_FILE_ARCHIVE_CATEGORIES;

PROCEDURE UPDATE_FILE_ARCHIVE_PARAMETER (
  IN_CONTEXT         IN  FILE_ARCHIVE_PARAMETERS.CONTEXT%TYPE,
  IN_NAME            IN  FILE_ARCHIVE_PARAMETERS.NAME%TYPE,
  IN_DAYS_TO_HOLD    IN  FILE_ARCHIVE_PARAMETERS.DAYS_TO_HOLD_QTY%TYPE,
  OUT_STATUS         OUT VARCHAR2,
  OUT_MESSAGE        OUT VARCHAR2
)
AS
--=======================================================================
--
-- Name:    UPDATE_FILE_ARCHIVE_PARAMETER
-- Type:    Procedure
-- Syntax:  UPDATE_FILE_ARCHIVE_PARAMETER
--
-- Description: Updates teh FRP.FILE_ARCHIVE_PARAMETERS record with the 
--              passed in values.  If the record does not exist, it will
--              be added.
--
--========================================================================
BEGIN
  BEGIN
    INSERT INTO FRP.FILE_ARCHIVE_PARAMETERS f ( 
      f.CONTEXT, 
      f.NAME, 
      f.DAYS_TO_HOLD_QTY, 
      f.LAST_PURGE_DATE
    ) VALUES (
      IN_CONTEXT,
      IN_NAME,
      IN_DAYS_TO_HOLD,
      SYSDATE
    );
  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    UPDATE FRP.FILE_ARCHIVE_PARAMETERS f
      SET 
        f.DAYS_TO_HOLD_QTY = IN_DAYS_TO_HOLD
      WHERE f.CONTEXT = IN_CONTEXT AND f.NAME = IN_NAME;
  END;
  
  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  BEGIN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    -- end exception block
END UPDATE_FILE_ARCHIVE_PARAMETER;

PROCEDURE VIEW_EXTERNAL_ORDER_PREFIX (
  IN_PREFIX          IN  EXTERNAL_ORDER_PREFIX.ORDER_PREFIX%TYPE,
  OUT_CUR            OUT TYPES.REF_CURSOR) AS

BEGIN

   IF IN_PREFIX IS NOT NULL THEN
     OPEN OUT_CUR FOR
     SELECT ORDER_PREFIX,
            LAST_ORDER_NUMBER,
            CREATED_ON
     FROM   EXTERNAL_ORDER_PREFIX
     WHERE  ORDER_PREFIX = IN_PREFIX
     ORDER BY ORDER_PREFIX;
   ELSE
     OPEN OUT_CUR FOR
     SELECT ORDER_PREFIX,
            LAST_ORDER_NUMBER,
            CREATED_ON
     FROM   EXTERNAL_ORDER_PREFIX
     ORDER BY ORDER_PREFIX;
   END IF;

END VIEW_EXTERNAL_ORDER_PREFIX;

PROCEDURE UPDATE_EXTERNAL_ORDER_PREFIX (
  IN_PREFIX             IN  EXTERNAL_ORDER_PREFIX.ORDER_PREFIX%TYPE,
  IN_LAST_ORDER_NUMBER  IN  EXTERNAL_ORDER_PREFIX.LAST_ORDER_NUMBER%TYPE,
  OUT_STATUS            OUT VARCHAR2,
  OUT_MESSAGE           OUT VARCHAR2) AS

BEGIN

    UPDATE EXTERNAL_ORDER_PREFIX
        SET LAST_ORDER_NUMBER = IN_LAST_ORDER_NUMBER,
          UPDATED_ON = SYSDATE,
          UPDATED_BY = 'SYS'
        WHERE ORDER_PREFIX = IN_PREFIX;

    IF SQL%NOTFOUND THEN
        INSERT INTO EXTERNAL_ORDER_PREFIX
            (ORDER_PREFIX, LAST_ORDER_NUMBER, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
            VALUES
            (IN_PREFIX, IN_LAST_ORDER_NUMBER, SYSDATE, 'SYS', SYSDATE, 'SYS');
    END IF;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END UPDATE_EXTERNAL_ORDER_PREFIX;

PROCEDURE DELETE_EXTERNAL_ORDER_PREFIX (
  IN_PREFIX             IN  EXTERNAL_ORDER_PREFIX.ORDER_PREFIX%TYPE,
  OUT_STATUS            OUT VARCHAR2,
  OUT_MESSAGE           OUT VARCHAR2) AS

BEGIN

    DELETE FROM EXTERNAL_ORDER_PREFIX
        WHERE ORDER_PREFIX = IN_PREFIX;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END DELETE_EXTERNAL_ORDER_PREFIX;

PROCEDURE INSERT_MISSING_ORDER_SEQUENCE (
  IN_ORDER_NUMBER       IN  MISSING_ORDER_SEQUENCE.EXTERNAL_ORDER_NUMBER%TYPE,
  IN_SYSTEM_MESSAGE_ID  IN  MISSING_ORDER_SEQUENCE.SYSTEM_MESSAGE_ID%TYPE,
  OUT_STATUS            OUT VARCHAR2,
  OUT_MESSAGE           OUT VARCHAR2) AS

BEGIN

    INSERT INTO MISSING_ORDER_SEQUENCE
        (EXTERNAL_ORDER_NUMBER, SYSTEM_MESSAGE_ID, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY)
        VALUES
        (IN_ORDER_NUMBER, IN_SYSTEM_MESSAGE_ID, SYSDATE, 'SYS', SYSDATE, 'SYS');

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END INSERT_MISSING_ORDER_SEQUENCE;

PROCEDURE GET_MISSING_ORDER_SEQUENCE (
  OUT_CUR                         OUT TYPES.REF_CURSOR) AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT EXTERNAL_ORDER_NUMBER, SYSTEM_MESSAGE_ID, CREATED_ON
        FROM MISSING_ORDER_SEQUENCE
        ORDER BY EXTERNAL_ORDER_NUMBER;

END GET_MISSING_ORDER_SEQUENCE;

PROCEDURE DELETE_MISSING_ORDER_SEQUENCE (
  IN_ORDER_NUMBER       IN  MISSING_ORDER_SEQUENCE.EXTERNAL_ORDER_NUMBER%TYPE,
  OUT_STATUS            OUT VARCHAR2,
  OUT_MESSAGE           OUT VARCHAR2) AS

BEGIN

    DELETE FROM MISSING_ORDER_SEQUENCE
        WHERE EXTERNAL_ORDER_NUMBER = IN_ORDER_NUMBER;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END DELETE_MISSING_ORDER_SEQUENCE;


PROCEDURE SAVE_CACHE_STATS
(
    IN_NODE_NAME         IN CACHE_STATISTICS.NODE_NAME%TYPE,
    IN_APP_NAME          IN CACHE_STATISTICS.APP_NAME%TYPE,         
    IN_CACHE_NAME        IN CACHE_STATISTICS.CACHE_NAME%TYPE,   
    IN_CACHE_TYPE        IN CACHE_STATISTICS.CACHE_TYPE%TYPE,
    IN_PRELOAD_FLAG      IN CACHE_STATISTICS.PRELOAD_FLAG%TYPE,  
    IN_ACCESS_COUNT      IN CACHE_STATISTICS.ACCESS_COUNT%TYPE, 
    IN_REFRESH_COUNT     IN CACHE_STATISTICS.REFRESH_COUNT%TYPE, 
    IN_REFRESH_RATE      IN CACHE_STATISTICS.REFRESH_RATE%TYPE, 
    IN_EXPIRE_RATE       IN CACHE_STATISTICS.EXPIRE_RATE%TYPE,  
    IN_EXPIRE_CHECK_RATE IN CACHE_STATISTICS.EXPIRE_CHECK_RATE%TYPE, 
    IN_FIRST_LOAD_TIME   IN CACHE_STATISTICS.FIRST_LOAD_TIME%TYPE,
    IN_LAST_REFRESH_TIME IN CACHE_STATISTICS.LAST_REFRESH_TIME%TYPE, 
    OUT_STATUS           OUT VARCHAR2,
    OUT_MESSAGE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting application cache use statistics
        into cache_statistics table

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
        UPDATE  CACHE_STATISTICS
        SET     NODE_NAME = IN_NODE_NAME,
                APP_NAME = IN_APP_NAME,
                CACHE_NAME = IN_CACHE_NAME, 
                CACHE_TYPE = IN_CACHE_TYPE,
                PRELOAD_FLAG = IN_PRELOAD_FLAG,
                ACCESS_COUNT = IN_ACCESS_COUNT,
                REFRESH_COUNT = IN_REFRESH_COUNT,
                REFRESH_RATE = IN_REFRESH_RATE,
                EXPIRE_RATE = IN_EXPIRE_RATE,
                EXPIRE_CHECK_RATE = IN_EXPIRE_CHECK_RATE,
                FIRST_LOAD_TIME = IN_FIRST_LOAD_TIME,
                LAST_REFRESH_TIME = IN_LAST_REFRESH_TIME,
                UPDATED_ON = SYSDATE
        WHERE  NODE_NAME = IN_NODE_NAME 
        AND    APP_NAME = IN_APP_NAME
        AND    CACHE_NAME = IN_CACHE_NAME;
        
        IF (SQL%ROWCOUNT = 0) THEN
            INSERT  INTO CACHE_STATISTICS
            (
                    NODE_NAME,
                    APP_NAME,
                    CACHE_NAME, 
                    CACHE_TYPE,
                    PRELOAD_FLAG,
                    ACCESS_COUNT,
                    REFRESH_COUNT,
                    REFRESH_RATE,
                    EXPIRE_RATE,
                    EXPIRE_CHECK_RATE,
                    FIRST_LOAD_TIME,
                    LAST_REFRESH_TIME,
                    UPDATED_ON
            )
            VALUES
            (
                    IN_NODE_NAME,
                    IN_APP_NAME,
                    IN_CACHE_NAME, 
                    IN_CACHE_TYPE,
                    IN_PRELOAD_FLAG,
                    IN_ACCESS_COUNT,
                    IN_REFRESH_COUNT,
                    IN_REFRESH_RATE,
                    IN_EXPIRE_RATE,
                    IN_EXPIRE_CHECK_RATE,
                    IN_FIRST_LOAD_TIME,
                    IN_LAST_REFRESH_TIME,
                    SYSDATE
            );
        END IF;
        
        OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END SAVE_CACHE_STATS;

FUNCTION IS_BAMS_CC(
IN_PAYMENT_TYPE VARCHAR2
)RETURN VARCHAR2
AS

index_pos number := 1;
current_pos number;
param_bams_cc_list global_parms.value%type;

BEGIN

  IF IN_PAYMENT_TYPE IS NOT NULL THEN
    param_bams_cc_list := get_global_parm_value('AUTH_CONFIG','bams.cc.list') || ',';
  
    LOOP
      current_pos := INSTR(param_bams_cc_list,',',index_pos);
      EXIT WHEN (NVL(current_pos,0) = 0);
      IF IN_PAYMENT_TYPE = SUBSTR(param_bams_cc_list,index_pos,current_pos-index_pos) THEN
         RETURN 'Y';
         EXIT;
      END IF;
      index_pos := current_pos + 1;
    END LOOP;    
  END IF;
  
  RETURN 'N';
EXCEPTION WHEN OTHERS THEN
    RETURN 'N';  
  
END IS_BAMS_CC;

PROCEDURE GET_PARTNER_PREFIXES (
  OUT_CUR                OUT TYPES.REF_CURSOR
) AS

BEGIN

  open out_cur for
    select partner_id
    from ptn_pi.partner_mapping;
--    select 'PRO' from dual;
  
END GET_PARTNER_PREFIXES;

PROCEDURE UPDATE_GLOBAL_SHIPPING_PARMS
(
IN_CONTEXT                      IN GLOBAL_PARMS.CONTEXT%TYPE,
IN_NAME                         IN GLOBAL_PARMS.NAME%TYPE,
IN_VALUE                        IN GLOBAL_PARMS.VALUE%TYPE,
IN_UPDATED_BY					IN GLOBAL_PARMS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting global shipping parameters.
        

Input:
        context                         varchar2
        name                            varchar2
        value                           varchar2
        updated_by						varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT  INTO GLOBAL_PARMS
(
        CONTEXT,
        NAME,
        VALUE,
        CREATED_BY,
        CREATED_ON,
        UPDATED_BY,
        UPDATED_ON
)
VALUES
(
        IN_CONTEXT,
        IN_NAME,
        IN_VALUE,
        UPPER(IN_UPDATED_BY),
        SYSDATE,
        UPPER(IN_UPDATED_BY),
        SYSDATE
);

OUT_STATUS := 'Y';
PAS_CHECK_GLOBAL_PARMS(IN_CONTEXT, IN_NAME);
   commit;  

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        UPDATE  GLOBAL_PARMS
        SET     VALUE = IN_VALUE,
        			 UPDATED_BY = UPPER(IN_UPDATED_BY),
        			 UPDATED_ON = SYSDATE
        WHERE   CONTEXT = IN_CONTEXT
        AND     NAME = IN_NAME;

        PAS_CHECK_GLOBAL_PARMS(IN_CONTEXT, IN_NAME);
        OUT_STATUS := 'Y';
END;    -- end duplicate exception

   commit;  

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

 

END UPDATE_GLOBAL_SHIPPING_PARMS;

END MISC_PKG;
/
