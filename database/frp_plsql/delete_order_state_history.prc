CREATE OR REPLACE
PROCEDURE frp.DELETE_ORDER_STATE_HISTORY (
   PNUM_DAYS IN NUMBER DEFAULT 90,
   PNUM_COMMIT_PT IN NUMBER DEFAULT 10000)
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/14/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***             Purpose : Deletes records from frp.order_state_history table that are older than
***                       90 days.
***
***    Input Parameters : pnum_days - determines the age of data that needs to be purged
***                       pnum_commit_pt - determines when to commit the purge.
***
*** Special Instructions: It is run daily by the DBMS_SCHEDULER scheduler
***
*****************************************************************************************/

   CURSOR cur_data IS
   SELECT
      ROWID rid
   FROM
      frp.order_state_history
   WHERE
      TRUNC(created_on) < (TRUNC(SYSDATE) - pnum_days);

BEGIN

   -- fetch the data to be purged that is older than 90 days
   FOR rec IN cur_data
   LOOP

      DELETE FROM
         frp.order_state_history
      WHERE
         ROWID = rec.rid;

      IF  MOD(cur_data%ROWCOUNT, pnum_commit_pt) = 0 THEN
          COMMIT;
      END IF;

   END LOOP;

   COMMIT;

EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;

END DELETE_ORDER_STATE_HISTORY;
.
/
