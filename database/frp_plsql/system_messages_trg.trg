CREATE OR REPLACE TRIGGER   frp.system_messages_trg
AFTER INSERT ON frp.system_messages
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE
/* Send out pages for entries in the system_messages table.
/*   Build a dynamic query to exclude certain messages that we don't want pages on.  */
/* To add, delete or modify recipients, look at the table sitescope.pagers. */

   v_location VARCHAR2(10);
   v_email_subject varchar2(100);
   v_page_distro VARCHAR2(100);
   v_nopage_distro VARCHAR2(100);
   v_action_suppress system_msg_filter.action_type%type;
   v_action_nopage system_msg_filter.action_type%type;
   v_action_mod_subj system_msg_filter.action_type%type;
   v_action_overwrite_distro system_msg_filter.action_type%type;
   v_suppress_flag varchar2(1);
   v_nopage_flag varchar2(1);
   v_nopage_start_hour varchar(2);
   v_nopage_end_hour varchar(2);
   v_send_distro system_msg_filter.OVERWRITE_DISTRO_TXT%type;
   v_send_msg system_messages.message%type;
   v_start_hr number;
   v_end_hr number;
   v_cur_hr number;

   CURSOR filter_cur(in_msg_source VARCHAR2, in_msg_subject VARCHAR2, in_msg_body VARCHAR2) IS
      SELECT    f.FILTER_NAME,
		f.ACTIVE_FLAG,
		f.MSG_SOURCE,
		f.MSG_SUBJECT,
		f.MSG_BODY,
		f.ACTION_TYPE,
		f.NOPAGE_START_HOUR,
		f.NOPAGE_END_HOUR,
		f.SUBJECT_PREFIX_TXT,
		f.OVERWRITE_DISTRO_TXT
	FROM    SYSTEM_MSG_FILTER f
	JOIN    SYSTEM_MSG_ACTION a
	  ON	f.action_type = a.action_type
	 AND	f.ACTION_TYPE in ('SUPPRESS','NOPAGE','OVERWRITE_DISTRO','MODIFY_SUBJECT')
       WHERE    f.active_flag = 'Y'
         AND    (
         	  --match on msg_source,msg_subject,msg_body
         	  ((msg_source is not null and msg_subject is not null and msg_body is not null)
         	  AND upper(in_msg_source) like upper('%' || msg_source || '%')
         	  AND upper(in_msg_subject) like upper('%' || msg_subject || '%')
         	  AND upper(in_msg_body) like upper('%' || msg_body || '%')
         	  )
         	  OR
         	  --match on msg_source,msg_subject
         	  ((msg_source is not null and msg_subject is not null and msg_body is null)
         	  AND upper(in_msg_source) like upper('%' || msg_source || '%')
         	  AND upper(in_msg_subject) like upper('%' || msg_subject || '%')
         	  )   
         	  OR
         	  --match on msg_source,msg_body
         	  ((msg_source is not null and msg_subject is null and msg_body is not null)
         	  AND upper(in_msg_source) like upper('%' || msg_source || '%')
         	  AND upper(in_msg_body) like upper('%' || msg_body || '%')
         	  )     
         	  OR
         	  --match on msg_subject,msg_body
         	  ((msg_source is null and msg_subject is not null and msg_body is not null)
         	  AND upper(in_msg_subject) like upper('%' || msg_subject || '%')
         	  AND upper(in_msg_body) like upper('%' || msg_body || '%')
         	  )   
         	  OR
         	  --match on msg_source
         	  ((msg_source is not null and msg_subject is null and msg_body is null)
         	  AND upper(in_msg_source) like upper('%' || msg_source || '%')
         	  )
         	  OR
         	  --match msg_subject
         	  ((msg_source is null and msg_subject is not null and msg_body is null)
         	  AND upper(in_msg_subject) like upper('%' || msg_subject || '%')
         	  )
         	  OR
         	  --match on msg_body
         	  ((msg_source is null and msg_subject is null and msg_body is not null)
         	  AND upper(in_msg_body) like upper('%' || msg_body || '%')
         	  )         	  
         	)
    ORDER BY    a.priority
       ;
       
   --filter_rec filter_cur%ROWTYPE;

   --v_status varchar2(1000);   -- REMOVE ME AFTER DEBUGGING!
   --v_message varchar2(1000);  -- REMOVE ME AFTER DEBUGGING!

   PROCEDURE send_email (in_project varchar2, in_email_subject varchar2, in_body varchar2) AS
       ---------------------------------------------------------------------
       -- send_email - Proc to send email given the project and body, which
       --              are the two things that change below.
       ---------------------------------------------------------------------

      v_status varchar2(1000);
      v_message varchar2(1000);
      c utl_smtp.connection;

   BEGIN
      IF frp.misc_pkg.get_global_parm_value('Mail Server','System Messages Enabled') = 'Y' THEN

         ops$oracle.mail_pkg.init_email(c,in_project,in_email_subject,v_status,v_message);
         if v_status = 'N' THEN
            raise_application_error(-20000, 'Failed to send mail due to the following error: ' || v_message);
         end if;

         utl_smtp.write_data(c, in_body);
         if v_status = 'N' THEN
            raise_application_error(-20000, 'Failed to send mail due to the following error: ' || v_message);
         end if;

         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         if v_status = 'N' THEN
            raise_application_error(-20000, 'Failure closing email connection with the following error: ' || v_message);
         end if;

      end if;
   END send_email;

BEGIN
   v_page_distro := 'SCRUB_ALERTS';
   v_nopage_distro := 'SCRUB_NOPAGE_ALERTS';
   v_action_suppress := 'SUPPRESS';
   v_action_nopage := 'NOPAGE';
   v_action_overwrite_distro := 'OVERWRITE_DISTRO';
   v_action_mod_subj := 'MODIFY_SUBJECT';   
   v_suppress_flag := 'N';
   v_nopage_flag := 'N';
   v_nopage_start_hour := null;
   v_nopage_end_hour := null;
   v_send_distro := v_page_distro;   
   v_send_msg := :new.message;

   if :new.email_subject is null then
      v_email_subject := 'System Message';
   else
      v_email_subject := :new.email_subject;
   end if;

   SELECT sys_context('USERENV','DB_NAME')
   INTO   v_location
   FROM   dual;

--
-- Following part takes care of the suppressions required
--

   FOR filter_rec in filter_cur (:new.source, v_email_subject, :new.message) LOOP
   	IF filter_rec.action_type = v_action_suppress THEN
   		v_suppress_flag := 'Y';
   		EXIT;  -- no need to go on
   	ELSIF filter_rec.action_type = v_action_nopage THEN
   		IF filter_rec.nopage_start_hour IS NOT NULL AND filter_rec.nopage_start_hour IS NOT NULL THEN
   			v_nopage_start_hour := filter_rec.nopage_start_hour;
   			v_nopage_end_hour := filter_rec.nopage_end_hour;
   			v_start_hr := to_number(v_nopage_start_hour);
   			v_end_hr := to_number(v_nopage_end_hour);
   			v_cur_hr := to_number(to_char(sysdate,'HH24'));
   			
   			IF v_end_hr >= v_start_hr THEN
   			    IF v_cur_hr >= v_start_hr AND v_cur_hr < v_end_hr THEN
   			    	v_nopage_flag := 'Y';
   			    END IF;
   			ELSE 
   			    IF (v_cur_hr >= v_start_hr and v_cur_hr <= 24)
   			    OR (v_cur_hr >= 0 and v_cur_hr < v_end_hr) THEN
   			        v_nopage_flag := 'Y';
   			    END IF;
   			END IF;
   		ELSE
   		 	v_nopage_flag := 'Y';
   		END IF;
   	ELSIF filter_rec.action_type = v_action_overwrite_distro THEN
   		IF filter_rec.OVERWRITE_DISTRO_TXT = '##TOKEN_SOURCE##' THEN
   		    v_send_distro := :new.source;
   		ELSE 
   		    v_send_distro := filter_rec.OVERWRITE_DISTRO_TXT;
   		END IF;
   	ELSIF filter_rec.action_type = v_action_mod_subj THEN
   		v_email_subject := filter_rec.SUBJECT_PREFIX_TXT || ' ' || v_email_subject;
   	END IF;
   END LOOP;
   
   IF v_nopage_flag = 'N' THEN
   	v_send_msg := utl_tcp.CRLF||'Database '||v_location||' '||:new.message;
   ELSE
        v_send_distro := v_nopage_distro;
        v_email_subject := 'NOPAGE ' || v_email_subject; 
   END IF;
   
   IF v_suppress_flag = 'Y' THEN
      null;
   ELSE
      send_email(v_send_distro, v_email_subject, v_send_msg);
   END IF;

END;
/
