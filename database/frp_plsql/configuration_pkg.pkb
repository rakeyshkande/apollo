CREATE OR REPLACE PACKAGE BODY FRP.CONFIGURATION_PKG
AS


PROCEDURE GET_SECURE_CONTEXT
(
IN_CONTEXT                          IN VARCHAR2,
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

BEGIN

  IF IN_CONTEXT IS NULL OR IN_CONTEXT = '' THEN
    OPEN OUT_CUR FOR
    SELECT c.CONTEXT, c.NAME, global.encryption.decrypt_it(c.VALUE,c.key_name) VALUE, c.DESCRIPTION
    FROM   FRP.SECURE_CONFIG c
    ORDER BY c.CONTEXT, c.NAME;
  ELSE
    OPEN OUT_CUR FOR
    SELECT c.CONTEXT, c.NAME, global.encryption.decrypt_it(c.VALUE,c.key_name) VALUE, c.DESCRIPTION
    FROM   FRP.SECURE_CONFIG c
    WHERE  c.CONTEXT = IN_CONTEXT
    ORDER BY c.CONTEXT, c.NAME;
  END IF;

END GET_SECURE_CONTEXT;

PROCEDURE GET_SECURE_CONTEXTS
(
OUT_CUR                             OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_CUR FOR
  SELECT DISTINCT c.CONTEXT FROM FRP.SECURE_CONFIG c ORDER BY c.context;

END GET_SECURE_CONTEXTS;

PROCEDURE UPDATE_SECURE_PROPERTY
(
IN_CONTEXT                          IN VARCHAR2,
IN_NAME                             IN VARCHAR2,
IN_VALUE                            IN VARCHAR2,
IN_DESCRIPTION                      IN VARCHAR2,
IN_UPDATED_BY                       IN VARCHAR2,
OUT_STATUS                          OUT VARCHAR2,
OUT_MESSAGE                         OUT VARCHAR2
)
AS

CURSOR exists_cur IS
        SELECT  c.NAME
        FROM    FRP.SECURE_CONFIG c
        WHERE   c.CONTEXT = IN_CONTEXT
        AND     c.NAME = IN_NAME;

V_PROP_NAME         FRP.SECURE_CONFIG.NAME%type;

v_key_name frp.secure_config.key_name%type;

BEGIN

  OPEN exists_cur;
  FETCH exists_cur INTO V_PROP_NAME;
  CLOSE exists_cur;

  v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

  IF V_PROP_NAME IS NOT NULL THEN

    UPDATE FRP.SECURE_CONFIG C
    SET c.CONTEXT = IN_CONTEXT,
    c.NAME = IN_NAME,
    c.VALUE = global.encryption.encrypt_it(IN_VALUE,v_key_name),
    c.key_name = v_key_name,
    c.DESCRIPTION = IN_DESCRIPTION,
    c.UPDATED_BY = IN_UPDATED_BY,
    c.UPDATED_ON = SYSDATE
    WHERE   c.CONTEXT = IN_CONTEXT
    AND     c.NAME = IN_NAME;

  ELSE

    INSERT INTO FRP.SECURE_CONFIG
    (
      CONTEXT,
      NAME,
      VALUE,
      KEY_NAME,
      DESCRIPTION,
      CREATED_BY,
      CREATED_ON,
      UPDATED_BY,
      UPDATED_ON
    )
    VALUES
    (
      IN_CONTEXT,
      IN_NAME,
      global.encryption.encrypt_it(IN_VALUE,v_key_name),
      v_key_name,
      IN_DESCRIPTION,
      IN_UPDATED_BY,
      SYSDATE,
      IN_UPDATED_BY,
      SYSDATE
    );

  END IF;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
          OUT_STATUS := 'N';
          OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_SECURE_PROPERTY;

PROCEDURE DELETE_SECURE_PROPERTY
(
IN_CONTEXT                          IN VARCHAR2,
IN_NAME                             IN VARCHAR2,
IN_UPDATED_BY                       IN VARCHAR2,
OUT_STATUS                          OUT VARCHAR2,
OUT_MESSAGE                         OUT VARCHAR2
)
AS

BEGIN

  UPDATE FRP.SECURE_CONFIG C
  SET c.UPDATED_BY = IN_UPDATED_BY || '-DELETE',
  c.UPDATED_ON = SYSDATE
  WHERE   c.CONTEXT = IN_CONTEXT
  AND     c.NAME = IN_NAME;

  DELETE FROM FRP.SECURE_CONFIG c
  WHERE c.CONTEXT = IN_CONTEXT
  AND c.NAME = IN_NAME;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
          OUT_STATUS := 'N';
          OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_SECURE_PROPERTY;

END;
/
