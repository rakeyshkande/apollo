create or replace procedure frp.system_messages_monitor(p_threshold in integer) as

/* Check the number of system_messages over the past minute.  If it exceeds the threshold, disable the trigger.
   If the number of messages is below the threshold, and the trigger is disabled, re-enable it. */

   c utl_smtp.connection;
   v_status varchar2(1000);
   v_message varchar2(1000);

   v_count integer;
   v_trigger_status varchar2(30);
   v_sql varchar2(32000);
   
   CURSOR msg_filter_cur IS
   SELECT f.msg_body
     FROM frp.system_msg_filter f
    WHERE f.action_type = 'EXCL_FROM_ON_OFF_SWITCH'
      AND f.msg_body is not null
      ;

begin

   -- Specify messages that cannot be specified from the screen and messages excluded from source filter.
   v_sql :=  'select count(*)
	   from   frp.system_messages sm
	   where  sm.timestamp >= trunc(sysdate,''MI'')-(1/1440)
	   and    sm.timestamp < trunc(sysdate,''MI'')
	   AND sm.type <> ''INFO''
	   AND NOT (sm.source = ''FLORIST SUSPEND SERVICE''
	       AND (sm.message LIKE ''%ERROR OCCURRED [-1] ORA-00001: unique constraint (FTD_APPS.FLORIST_BLOCKS_NEW_PK) violated%''
	      OR   sm.message like ''%Loading Suspend and Resume data for the following florist failed%''))
	   AND NOT (sm.source = ''Email Queue Automated Process'' AND message like ''java.lang.Exception: Parse error in file:%'')
	   AND NOT (sm.source = ''ORDER DISPATCHER''
	       AND sm.message like ''java.lang.NullPointerException%'')
	   AND sm.source not in (select f.msg_source from frp.system_msg_filter f
	   			 where f.action_type = ''EXCL_FROM_ON_OFF_SWITCH''
	   			   and f.msg_source is not null
	   			)
	   '
	   ;
  
   -- Add message exclusions.
   FOR c in msg_filter_cur LOOP
   	v_sql := v_sql || ' AND sm.message not like ''%' || replace(c.msg_body,'''','''''') || '%'' ' || chr(10);
   END LOOP;
   
   EXECUTE IMMEDIATE v_sql
   INTO v_count;


   select status
   into   v_trigger_status
   from   user_triggers
   where  trigger_name = 'SYSTEM_MESSAGES_TRG';

   IF v_count > p_threshold THEN

      if v_trigger_status = 'ENABLED' then

         execute immediate 'alter trigger frp.system_messages_trg disable';

         ops$oracle.mail_pkg.init_email(c,'SCRUB_ALERTS','911 - SYSTEM_MESSAGES Trigger Disabled',v_status, v_message);
         if v_status <> 'Y' then
            raise_application_error(-20000,v_message,true);
         end if;

         utl_smtp.write_data(c, utl_tcp.crlf||'zeus system_messages trigger auto-disabled due to message count.');

         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         if v_status <> 'Y' then
            raise_application_error(-20000,v_message,true);
         end if;

      end if;

   else

      if v_trigger_status = 'DISABLED' then
         execute immediate 'alter trigger frp.system_messages_trg enable';

         ops$oracle.mail_pkg.init_email(c,'SCRUB_ALERTS','SYSTEM_MESSAGES Trigger Enabled',v_status, v_message);
         if v_status <> 'Y' then
            raise_application_error(-20000,v_message,true);
         end if;

         utl_smtp.write_data(c, utl_tcp.crlf||'zeus system_messages trigger re-enabled because message count went back down.');

         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         if v_status <> 'Y' then
            raise_application_error(-20000,v_message,true);
         end if;

      end if;

   end if;

end;
/
