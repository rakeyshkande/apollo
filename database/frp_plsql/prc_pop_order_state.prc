CREATE OR REPLACE
PROCEDURE frp.PRC_POP_ORDER_STATE (
   PNUM_ORDER_DETAIL_ID   FRP.ORDER_STATE.ORDER_DETAIL_ID%TYPE,
   PCHR_STATUS   FRP.ORDER_STATE.STATUS%TYPE)
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***             Purpose : Inserts/Updates a record into frp.order_state table
***
***    Input Parameters : pnum_order_detail_id - order_detail_id
***                       pchr_status - new status
***
*** Special Instructions:
***
*****************************************************************************************/

   lchr_order_exists   VARCHAR2(1) := 'N';

BEGIN

   -- check to see if the order_detail_id already exists
   BEGIN
      SELECT
         DECODE(COUNT(1), 0, 'N', 'Y')
      INTO
         lchr_order_exists
      FROM
         frp.order_state
      WHERE
         order_detail_id = pnum_order_detail_id;
   EXCEPTION
      WHEN OTHERS THEN
         lchr_order_exists := 'N';
   END;

   -- insert/update into the order_state table depending on if the order_detail_id already exists
   IF lchr_order_exists = 'Y' THEN

      UPDATE
         frp.order_state
      SET
         status = pchr_status,
         updated_on = SYSDATE
      WHERE
         order_detail_id = pnum_order_detail_id;

   ELSE

      INSERT INTO frp.order_state (
	 order_detail_id,
	 status,
	 created_on,
	 updated_on)
      VALUES (
	 pnum_order_detail_id,
	 pchr_status,
	 SYSDATE,
	 SYSDATE);

   END IF;


EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In FRP.PRC_POP_ORDER_STATE: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END PRC_POP_ORDER_STATE;
.
/
