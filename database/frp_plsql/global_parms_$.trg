-- FRP.GLOBAL_PARMS_$ trigger to populate FRP.GLOBAL_PARMS$ shadow table

CREATE OR REPLACE TRIGGER FRP.global_parms_$
AFTER INSERT OR UPDATE OR DELETE ON frp.global_parms REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO frp.global_parms$ (
      	context,
      	name,
      	value,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,
      	operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.context,
      	:NEW.name,
      	:NEW.value,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,      	
      	'INS',
      	SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO frp.global_parms$ (
      	context,
			name,
			value,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,			
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.context,
      	:OLD.name,
      	:OLD.value,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,      	
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO frp.global_parms$ (
      	context,
			name,
			value,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,			
			operation$, 
      	timestamp$
      ) VALUES (
      	:NEW.context,
			:NEW.name,
      	:NEW.value,
      	:NEW.created_by,
      	:NEW.created_on,
      	:NEW.updated_by,
      	:NEW.updated_on,      	
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO frp.global_parms$ (
      	context,
			name,
			value,
      	created_by,
      	created_on,
      	updated_by,
      	updated_on,			
			operation$, 
      	timestamp$
      ) VALUES (
      	:OLD.context,
		   :OLD.name,
      	:OLD.value,
      	:OLD.created_by,
      	:OLD.created_on,
      	:OLD.updated_by,
      	:OLD.updated_on,      	
      	'DEL',
      	SYSDATE);

   END IF;

END;
/
