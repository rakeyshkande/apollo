CREATE OR REPLACE
PACKAGE BODY                   FRP.SYSTEM_MSG_PKG
AS


PROCEDURE GET_PAGE_FILTER_LIST
(
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure retrieves page filters.

Input: 
	

Output:
        cur                             Cursor
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT  SYSTEM_MSG_FILTER_ID,
		FILTER_NAME,
		ACTIVE_FLAG,
		MSG_SOURCE,
		MSG_SUBJECT,
		MSG_BODY,
		ACTION_TYPE,
		NOPAGE_START_HOUR,
		NOPAGE_END_HOUR,
		SUBJECT_PREFIX_TXT,
		OVERWRITE_DISTRO_TXT
        FROM frp.system_msg_filter
        ORDER BY ACTION_TYPE, upper(MSG_SOURCE), upper(MSG_SUBJECT), upper(MSG_BODY)	
        ;

END GET_PAGE_FILTER_LIST;

PROCEDURE GET_ACTION_TYPE_LIST
(
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure retrieves page action types.

Input: 
	

Output:
        cur                             Cursor
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT  ACTION_TYPE,
        	ACTION_DESC,
        	PRIORITY
        FROM frp.system_msg_action
        ;

END GET_ACTION_TYPE_LIST;

PROCEDURE GET_FILTER_BY_ACTION
(
IN_ACTION_TYPE			  IN SYSTEM_MSG_FILTER.ACTION_TYPE%TYPE,
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure retrieves page filter by action type.

Input: 
	

Output:
        cur                             Cursor
-----------------------------------------------------------------------------*/

BEGIN

  IF in_action_type = '' or in_action_type is null then
  	GET_PAGE_FILTER_LIST
  	(
  	    out_cur => out_cur
  	)
  	;
  ELSE

    OPEN OUT_CUR FOR
        SELECT  SYSTEM_MSG_FILTER_ID,
		FILTER_NAME,
		ACTIVE_FLAG,
		MSG_SOURCE,
		MSG_SUBJECT,
		MSG_BODY,
		ACTION_TYPE,
		NOPAGE_START_HOUR,
		NOPAGE_END_HOUR,
		SUBJECT_PREFIX_TXT,
		OVERWRITE_DISTRO_TXT
        FROM frp.system_msg_filter
        WHERE action_type = IN_ACTION_TYPE
        ORDER BY upper(MSG_SOURCE), upper(MSG_SUBJECT), upper(MSG_BODY)	
        ;
  END IF;

END GET_FILTER_BY_ACTION;

PROCEDURE GET_FILTER_BY_ID
(
IN_SYSTEM_MSG_FILTER_ID		IN SYSTEM_MSG_FILTER.SYSTEM_MSG_FILTER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure retrieves page filter by id.

Input: 
	

Output:
        cur                             Cursor
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT  SYSTEM_MSG_FILTER_ID,
		FILTER_NAME,
		ACTIVE_FLAG,
		MSG_SOURCE,
		MSG_SUBJECT,
		MSG_BODY,
		ACTION_TYPE,
		NOPAGE_START_HOUR,
		NOPAGE_END_HOUR,
		SUBJECT_PREFIX_TXT,
		OVERWRITE_DISTRO_TXT
        FROM frp.system_msg_filter
        WHERE system_msg_filter_id = IN_SYSTEM_MSG_FILTER_ID
        ;

END GET_FILTER_BY_ID;



PROCEDURE GET_PAGE_DISTRO_LIST
(
OUT_CUR                           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure retrieves distro list.

Input: 
	

Output:
        cur                             Cursor
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT  project
          FROM  sitescope.projects
        ;
        

END GET_PAGE_DISTRO_LIST;

PROCEDURE INS_UPD_SYSTEM_MSG_FILTER
(
IN_SYSTEM_MSG_FILTER_ID		IN SYSTEM_MSG_FILTER.SYSTEM_MSG_FILTER_ID%TYPE,
IN_FILTER_NAME			IN SYSTEM_MSG_FILTER.FILTER_NAME%TYPE,
IN_ACTION_TYPE			IN SYSTEM_MSG_FILTER.ACTION_TYPE%TYPE,
IN_MSG_SOURCE			IN SYSTEM_MSG_FILTER.MSG_SOURCE%TYPE,
IN_MSG_SUBJECT			IN SYSTEM_MSG_FILTER.MSG_SUBJECT%TYPE,
IN_MSG_BODY			IN SYSTEM_MSG_FILTER.MSG_BODY%TYPE,		
IN_NOPAGE_START_HOUR		IN SYSTEM_MSG_FILTER.NOPAGE_START_HOUR%TYPE,
IN_NOPAGE_END_HOUR		IN SYSTEM_MSG_FILTER.NOPAGE_END_HOUR%TYPE,
IN_SUBJECT_PREFIX_TXT		IN SYSTEM_MSG_FILTER.SUBJECT_PREFIX_TXT%TYPE,
IN_OVERWRITE_DISTRO_TXT		IN SYSTEM_MSG_FILTER.OVERWRITE_DISTRO_TXT%TYPE,
IN_UPDATED_BY			IN SYSTEM_MSG_FILTER.UPDATED_BY%TYPE,
OUT_SYSTEM_MSG_FILTER_ID	OUT SYSTEM_MSG_FILTER.SYSTEM_MSG_FILTER_ID%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure inserts/updates a system msg filter.

Input: 
	filter_name
	action_type
	msg_source
	msg_subject
	msg_body
	nopage_start_hour
	nopage_end_hour
	subject_prefix_txt
	overwrite_distro_txt
	update_by

Output:
        OUT_STATUS
        OUT_MESSAGE
-----------------------------------------------------------------------------*/


CURSOR check_for_duplicate IS
select  'Y'
from system_msg_filter
where 1 =1
and (((msg_source is null and in_msg_source is null) or (msg_source is not null and in_msg_source is not null and upper(msg_source) = upper(in_msg_source) ))
    and ((msg_body is null and in_msg_body is null) or (msg_body is not null and in_msg_body is not null and upper(msg_body) = upper(in_msg_body) ))
    and ((msg_subject is null and in_msg_subject is null) or (msg_subject is not null and in_msg_subject is not null and upper(msg_subject) = upper(in_msg_subject) ))
    and action_type = in_action_type 
    )
or filter_name = in_filter_name
;

v_duplicate char(1) := 'N';

BEGIN

OUT_STATUS := 'Y';

    IF IN_SYSTEM_MSG_FILTER_ID > 0 THEN

      UPDATE system_msg_filter 
      SET 
        FILTER_NAME = IN_FILTER_NAME,
        ACTION_TYPE = IN_ACTION_TYPE,
        MSG_SOURCE = IN_MSG_SOURCE,
        MSG_SUBJECT = IN_MSG_SUBJECT,
        MSG_BODY = IN_MSG_BODY,
        NOPAGE_START_HOUR = IN_NOPAGE_START_HOUR,
        NOPAGE_END_HOUR = IN_NOPAGE_END_HOUR,
        SUBJECT_PREFIX_TXT = IN_SUBJECT_PREFIX_TXT,
        OVERWRITE_DISTRO_TXT = IN_OVERWRITE_DISTRO_TXT,
        UPDATED_BY = IN_UPDATED_BY,
        UPDATED_ON = sysdate
      WHERE  SYSTEM_MSG_FILTER_ID = IN_SYSTEM_MSG_FILTER_ID;

      OUT_SYSTEM_MSG_FILTER_ID := IN_SYSTEM_MSG_FILTER_ID;
    ELSE

      OPEN check_for_duplicate;
      FETCH check_for_duplicate INTO v_duplicate;
      CLOSE check_for_duplicate;

      IF (v_duplicate = 'Y') THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Duplicate: A record already exists with these values';
      ELSE

        SELECT frp.system_msg_filter_id_seq.nextval INTO OUT_SYSTEM_MSG_FILTER_ID FROM DUAL;

        INSERT INTO system_msg_filter
        (
		SYSTEM_MSG_FILTER_ID,
		FILTER_NAME,
		ACTIVE_FLAG,
		MSG_SOURCE,
		MSG_SUBJECT,
		MSG_BODY,
		ACTION_TYPE,
		NOPAGE_START_HOUR,
		NOPAGE_END_HOUR,
		SUBJECT_PREFIX_TXT,
		OVERWRITE_DISTRO_TXT,
		CREATED_ON,
		CREATED_BY,
		UPDATED_ON,
		UPDATED_BY
        )
        VALUES
        (
		OUT_SYSTEM_MSG_FILTER_ID,
		IN_FILTER_NAME,
		'Y',
		IN_MSG_SOURCE,
		IN_MSG_SUBJECT,
		IN_MSG_BODY,
		IN_ACTION_TYPE,
		IN_NOPAGE_START_HOUR,
		IN_NOPAGE_END_HOUR,
		IN_SUBJECT_PREFIX_TXT,
		IN_OVERWRITE_DISTRO_TXT,
		SYSDATE,
		IN_UPDATED_BY,
		SYSDATE,
		IN_UPDATED_BY
        );

      END IF;

  END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        
END INS_UPD_SYSTEM_MSG_FILTER;

PROCEDURE DELETE_PAGE_FILTER
(
IN_SYSTEM_MSG_FILTER_ID		IN SYSTEM_MSG_FILTER.SYSTEM_MSG_FILTER_ID%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure delete a system msg filter.

Input: 
	IN_SYSTEM_MSG_FILTER_ID

Output:
        OUT_STATUS
        OUT_MESSAGE
-----------------------------------------------------------------------------*/



BEGIN

OUT_STATUS := 'Y';

    DELETE FROM system_msg_filter
    WHERE system_msg_filter_id = IN_SYSTEM_MSG_FILTER_ID;
    

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        
END DELETE_PAGE_FILTER;

PROCEDURE UPD_PAGE_FILTER_STATUS
(
IN_SYSTEM_MSG_FILTER_ID		IN SYSTEM_MSG_FILTER.SYSTEM_MSG_FILTER_ID%TYPE,
IN_ACTIVE_FLAG			IN SYSTEM_MSG_FILTER.ACTIVE_FLAG%TYPE,
IN_UPDATED_BY			IN SYSTEM_MSG_FILTER.UPDATED_BY%TYPE,
OUT_STATUS			OUT VARCHAR2,
OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:  This procedure updates system msg filter status by id.

Input: 
	IN_SYSTEM_MSG_FILTER_ID
	IN_ACTIVE_FLAG

Output:
        OUT_STATUS
        OUT_MESSAGE
-----------------------------------------------------------------------------*/



BEGIN

OUT_STATUS := 'Y';

    UPDATE system_msg_filter
    SET active_flag = IN_ACTIVE_FLAG,
        updated_on = sysdate
    WHERE system_msg_filter_id = IN_SYSTEM_MSG_FILTER_ID;
    

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        
END UPD_PAGE_FILTER_STATUS;


END;
.
/
