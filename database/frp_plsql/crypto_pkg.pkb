CREATE OR REPLACE PACKAGE BODY FRP.CRYPTO_PKG AS

PROCEDURE GET_PGP_PRIVATE_KEY_INFO
(
IN_PGP_KEY_CODE                       IN  PGP_CRYPTO_INFO.PGP_KEY_CODE%TYPE,
OUT_KEY_FILE_PATH                     OUT VARCHAR2,
OUT_PASS_PHRASE                       OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
      Returns the Private Key File Path and the Pass Phrase for accessing the 
      file given the Key Code.

Input:
      The key code to retrieve.

Output:
      The key file path and the key code or null values if they are not found.
-----------------------------------------------------------------------------*/

BEGIN
  SELECT 
    pg.private_key_file_path, 
    global.encryption.decrypt_it(pg.pass_phrase,pg.key_name) decrypted_pass_phrase
  INTO
    out_key_file_path,
    out_pass_phrase
  FROM PGP_CRYPTO_INFO pg
  WHERE pg.pgp_key_code = in_pgp_key_code;
	
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_key_file_path := null;
    out_pass_phrase := null;
  END;    -- end exception block	
	
END GET_PGP_PRIVATE_KEY_INFO;

END CRYPTO_PKG;
/
