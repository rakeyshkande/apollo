DROP MATERIALIZED VIEW STATS.LY_MONTH_ORDERS_BY_DAY_MV;
CREATE MATERIALIZED VIEW STATS.LY_MONTH_ORDERS_BY_DAY_MV
  --
  -- Orders for last year.  Beg date = 1st of month.  End Date
  -- = a few days into the next month, to accommodate the 
  -- dateshift for YOY comparison.
  --
  TABLESPACE STATS_DATA 
  BUILD IMMEDIATE USING NO INDEX 
  REFRESH FORCE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  DISABLE QUERY REWRITE
  AS select trunc(transaction_date) transaction_date,
          count(*) order_count,
          sum(cob.PRODUCT_AMOUNT +
	      cob.ADD_ON_AMOUNT  +
	      cob.SERVICE_FEE    +
	      cob.SHIPPING_FEE   -
              cob.DISCOUNT_AMOUNT) order_amt
   from   clean.order_details cod
   join   clean.orders co on (co.order_guid=cod.order_guid)
   join   ftd_apps.origins fao on (co.origin_id = fao.origin_id)
   join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
   join   clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id
                           and cob.additional_bill_indicator = 'N')
   join   ftd_apps.product_master pm on (cod.product_id = pm.product_id)                           
   where  fao.origin_type in ('phone','bulk','ariba','internet')
     and    trunc(cat.transaction_date) >= trunc(add_months(sysdate-(1/24),-12),'MM')         -- beg_date
     and    trunc(cat.transaction_date) <= trunc(last_day(add_months(sysdate-(1/24),-12))+4)  -- end_date
     and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
     and    cat.transaction_type = 'Order'
     and    cat.payment_type <> 'NC'
     and    co.origin_id not in ('TEST', 'test')
     and    co.source_code not in ('12743')
     and    co.company_id not in ('ProFlowers')
     and    pm.category <> 'SRVCS'
   group by trunc(transaction_date);
 