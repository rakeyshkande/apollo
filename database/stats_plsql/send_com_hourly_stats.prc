create or replace procedure stats.send_com_hourly_stats is

   r_customStyles   ops$oracle.xml_spreadsheet.t_rec_customStyles;
   r_background     ops$oracle.xml_spreadsheet.t_rec_background;
   r_font           ops$oracle.xml_spreadsheet.t_rec_font;
   r_alignment      ops$oracle.xml_spreadsheet.t_rec_alignment;
   tab_customStyles ops$oracle.xml_spreadsheet.t_tab_customStyles;
   tab_columns      ops$oracle.xml_spreadsheet.t_tab_columns;

   v_file_name VARCHAR2(100); /* ascii file attachment */
   v_file_handle utl_file.file_type;
   v_file_handle_2 utl_file.file_type;
   v_line VARCHAR2(5000);
   v_multiplier NUMBER;  /* from marketing factors, turns an incomplete cumulative order number into a full-day projection */
   conn UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   conn_exec UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   mesg VARCHAR2(32767);
   crlf VARCHAR2(2) := chr(13) || chr(10);
   v_date date; /* Static date/time */
   v_start_hour varchar2(5); /* character representation of starting time */
   v_end_hour varchar2(5); /* character representation of ending time */
   v_retry_count number;
   v_success_indicator number;
   v_recipient_list varchar2(200);
   v_recipient_list_execs varchar2(200);
   v_body           varchar2(4000) := NULL;
   v_exec_body      varchar2(4000) := NULL;
   v_holiday_block  varchar2(4000) := NULL;

   cursor c_recipient_list(p_hour in number, in_holiday_active_flag varchar2) is
      select project
      from   com_hourly_stats_schedule
      where  hour = p_hour
        and  project NOT LIKE '%EXECUTIVE%'
        and  (in_holiday_active_flag = 'YES' or holiday_only_flag = 'N');
   r_recipient_list c_recipient_list%rowtype;


   cursor c_recipient_list_execs(p_hour in number) is
      select project
      from   com_hourly_stats_schedule
      where  hour = p_hour
        and project LIKE '%EXECUTIVE%';
   r_recipient_list_execs c_recipient_list_execs%rowtype;


   /* Cursor for producing the origin-type spreadsheet. */
   cursor com_cursor (p_date in date) is
      select txn_date, hour, phone_internet, order_count, merch_value, add_ons, shipping_fee,
             service_fee, discount_amount, order_value, tax, total_order_amount from (
      select to_char(cat.transaction_date, 'mm-dd-yyyy') txn_date,
             to_char(cat.transaction_date, 'hh24') hour,
             case fao.origin_type
                when 'phone' then 'Phone'
                when 'bulk'  then 'Phone'
                when 'ariba' then 'Internet'
                when 'internet' then 'Internet'
             end phone_internet,
             sum(decode(cat.transaction_type,'Order',1,0)) order_count,
             sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))
                discount_amount,
             (sum(cat.Product_amount)+sum(cat.add_on_amount) +sum(cat.shipping_fee)
                + sum(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee)))
                -sum(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))
                Order_value,
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) tax,
             (sum(cat.Product_amount)+sum(cat.add_on_amount) +sum(cat.shipping_fee)
                +sum(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee)))
                -sum(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))
                +sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) total_order_amount
      from   clean.order_details cod
      join   clean.orders co on (co.order_guid=cod.order_guid)
      join   ftd_apps.origins fao on (co.origin_id = fao.origin_id)
      join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
      join   ftd_apps.product_master pm on (cod.product_id = pm.product_id)
      where  fao.origin_type in ('internet','phone','bulk','ariba')
      and    cat.transaction_date >= decode(to_char(p_date,'hh24'),'00',trunc(p_date-1),TRUNC(P_DATE))
      and    cat.transaction_date < trunc(p_date,'hh24')
      and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
      and    cat.transaction_type = 'Order'
      and    cat.payment_type <> 'NC'
      and    co.origin_id not in ('TEST', 'test')
      and    co.source_code not in ('12743')
      and    co.company_id not in ('ProFlowers')
      and    pm.category <> 'SRVCS'
      group by to_char(cat.transaction_date, 'mm-dd-yyyy'),to_char(cat.transaction_date, 'hh24'),
      case fao.origin_type
         when 'phone' then 'Phone'
         when 'bulk'  then 'Phone'
         when 'ariba' then 'Internet'
         when 'internet' then 'Internet'
      end)
      order by hour, phone_internet;
   com_row com_cursor%rowtype;

   /* Cursor for producing the source-type spreadsheet. */
   cursor source_cursor (p_date in date) is
      select to_char(cat.transaction_date,'mm-dd-yyyy') txn_date, sm.source_type,
       sum(decode(cat.transaction_type,'Order',1,0)) order_count,
       sum(cat.Product_amount) Merch_value,
       sum(cat.add_on_amount) add_ons,
       sum(cat.shipping_fee) shipping_fee,
       sum(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee))  service_fee,
       sum(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))
                discount_amount,
       (sum(cat.Product_amount)+sum(cat.add_on_amount) +sum(cat.shipping_fee)
                + sum(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee)))
                -sum(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))
                Order_value,
       sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) tax,
       (sum(cat.Product_amount)+sum(cat.add_on_amount) +sum(cat.shipping_fee)
                +sum(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee)))
                -sum(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))
                +sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) total_order_amount
      from   clean.order_details cod
      join   clean.orders co on (co.order_guid=cod.order_guid)
      join   ftd_apps.origins fao on (co.origin_id = fao.origin_id)
      join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
      join   ftd_apps.product_master pm on (cod.product_id = pm.product_id) 
      join ftd_apps.source_master sm on (sm.source_code = co.source_code)                          
      where  fao.origin_type in ('internet','phone','bulk','ariba')
      and    cat.transaction_date >= decode(to_char(sysdate,'hh24'),'00',trunc(sysdate-1),TRUNC(sysdate))
      and    cat.transaction_date < trunc(sysdate,'hh24')
      and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
      and    cat.transaction_type = 'Order'
      and    cat.payment_type <> 'NC'
      and    co.origin_id not in ('TEST', 'test')
      and    co.source_code not in ('12743')
      and    co.company_id not in ('ProFlowers')
      and    pm.category <> 'SRVCS'
      group by to_char(cat.transaction_date,'mm-dd-yyyy') , sm.source_type
      order by 1, 2;
   source_row source_cursor%rowtype;

   v_last_hour      number;     /* Orders for the last hour */
   v_last_hour_amt  number;     /* Last hour's total dollar amount of orders */
   v_last_hour_aov  number;     /* Last hour's average order avg order value */

   v_provided_date   date;          /* This identifies a forecast, for the case where we have multiple forecasts. */
   v_ly_mtd_beg_date date;          /* For prior year MTD values, this is the start date of last  year  */
   v_ly_mtd_end_date date;          /* For prior year MTD values, this is the end date of last  year  */
   v_ly_full_month_end_date date;   /* For prior year full month values, this is the end date of last  year  */
   v_ly_month_first_day date;       /* LY MView min date value.  Used to determine if we need to refresh */


   v_day_forecast   NUMBER;     /* forecast total orders for today */
   v_day_total      number;     /* Today's cumulative orders.  NOT PROJECTED */
   v_day_amt  number;           /* Today's cumulative orders dollar amount.  NOT PROJECTED */
   v_day_aov  number;           /* Today's cumulative orders average order value.  NOT PROJECTED */
   v_day_pct number;            /* Percentage of projected orders for today to forecast */

   v_mtd_forecast number;       /* forecast total orders for the month to date (includes today) */
   v_mtd_total number;          /* Month's cumulative orders, up to end-of-day yesterday. */
   v_mtd_amt   number;          /* Month's cumulative orders (to eod yesterday) dollar amount. */
   v_mtd_aov   number;          /* Month's cumulative orders (to eod yesterday) average order value. */
   v_mtd_pct number;            /* Percentage of projected MTD orders to forecast */
   v_day_projected_pace number; /* Projected orders for today */
   v_mtd_projected_pace number; /* Projected orders for MTD */
   v_ly_mtd number;             /* Last year's MTD orders (to end-of-day) */
   v_yoy_mtd_pct number;        /* Growth in this year's MTD orders over last year */
   v_day_pct_sunk number;       /* Cumulative orders divided by projected pace */
   v_day_pct_sunk_to_fcst number;  /* Cumulative orders divided by forecast */
   v_mtd_end_date date;         /* date extracted from MTD materialized view */

   v_full_month_projected_pace          number := 0; /* Full Month projected pace (to end of today) + remaining forecast */
   v_full_month_projected_rev           number := 0; /* Full Month projected revenue (month pace to eod * mtd_aov) */
   v_full_month_forecast_rev            number := 0; /* Full Month Forecasted revenue (v_full_month_forecast * ??? DO NOT USE ACTUAL AOV) */
   v_full_month_rev_pct                 number := 0; /* Full Month Projected rev / Full Month Forecast Rev */
   v_py_full_month_rev                  number := 0; /* Prior Year Actual Rev for the Full Month (query from database) */
   v_yoy_full_month_rev_pct             number := 0; /* v_full_month_projected_rev / v_py_full_month_rev */

   v_mtd_amt_rnd                        number := 0; /*Rounded equivalent of MTD Actual Revenue (for the rpt)  */
   v_full_month_forecast_rev_rnd        number := 0; /*Rounded equivalent of Full Month Forecast Revenue (for the rpt) */
   v_full_month_projected_rev_rnd       number := 0; /*Rounded equivalent of Full Month Projected (for the rpt) */
   v_py_full_month_rev_rnd              number := 0; /*Rounded equivalent of Prior Year Actual Rev for the Full Month  */

   v_full_month_forecast                number := 0; /* Full Month orders forecasted */
   v_full_month_remaining_fcst          number := 0; /* Full Month orders forecasted from tomorrow to EOM */
   v_full_month_pct                     number := 0; /* full month projected pace / holiday forecast */
   v_full_month_pct_sunk                number := 0; /* Full Month MTD projected pace (to end of day) / holiday projected pace */
   v_full_month_pct_sunk_to_fcst        number := 0; /* Full Month MTD projected pace (to end of day) / full month forecast */

   v_holiday_errors_flag             varchar2(3) := 'NO';
   v_holiday_end_day                 varchar2(2); /* "Holiday Periods" start on day 1 and end on this day */
   v_holiday_projected_pace          number := 0; /* MTD projected pace (to end of today) + remaining forecast */
   v_holiday_remaining_forecast      number := 0; /* Orders forecasted from tomorrow to end of holiday period */
   v_holiday_forecast                number := 0; /* Forecasted for 1st to last day of holiday period */
   v_holiday_pct                     number := 0; /* percentage of projected full month orders to forecast */
   v_holiday_pct_sunk                number := 0; /* Cumulative orders dividided by projectd pace for holiday period */
   v_holiday_active_flag             varchar2(100) := 'NO';

BEGIN
   dbms_output.put_line('Starting send_com_hourly_stats proc at ' || to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));


   v_date := sysdate;
   v_start_hour := to_char(trunc(v_date-(1/24),'hh24'),'hh24:mi');
   v_end_hour := to_char(trunc(v_date,'hh24'),'hh24:mi');
   v_file_name := 'FTDCOM_Hourly_Orders_'||to_char(v_date-(1/24),'yyyymmdd_hh24')||'.xls';

   ----------------------------------------------------------------
   -- see if its time to refresh the MTD materialized view
   -- Do this the first run for the new day = 1AM (12AM is still for prior day)
   ----------------------------------------------------------------
   select trunc(mtd_end_date) into v_mtd_end_date
   from mtd_orders_to_yesterday_mv
   where rownum < 2;

   if v_mtd_end_date != trunc(sysdate-(1/24)) then
      dbms_output.put_line ('Mtd_end_date:' || v_mtd_end_date ||
         ', sysdate-(1/24):' || trunc(sysdate-(1/24)) ||
         ' - ### REFRESH MVIEW!! ###');

      dbms_output.put_line('Start Refresh MTD Materialized View at ' ||
                  to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));

      dbms_mview.refresh('mtd_orders_to_yesterday_mv');

      dbms_output.put_line('Finished Refresh MTD Materialized View at ' ||
                  to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));
   else
      dbms_output.put_line ('Mtd_end_date:' || v_mtd_end_date ||
         ', sysdate-(1/24):' || trunc(sysdate-(1/24)) ||
         ' -  Do Not Refresh MTD MVIEW');
   end if;


   ----------------------------------------------------------------
   -- See if we need to refresh LY Month Daily Totals
   -- This only needs done once a month, with the 1AM run on the first
   ----------------------------------------------------------------
   select trunc(min(transaction_date))
   into v_ly_month_first_day
   from ly_month_orders_by_day_mv;

   if to_char(v_ly_month_first_day,'MM') != to_char(sysdate-(1/24),'MM') then
      dbms_output.put_line ('LY_Month_First_Day:' || v_ly_month_first_day ||
         ', sysdate-(1/24):' || trunc(sysdate-(1/24)) ||
         ' - ### REFRESH LAST YEAR MONTH MVIEW!! ###');

      dbms_output.put_line('Start Refresh of LY Month Materialized View at ' ||
                  to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));

      execute immediate('alter session set optimizer_features_enable=''10.2.0.4''');
      dbms_mview.refresh('ly_month_orders_by_day_mv');

      dbms_output.put_line('Finished Refresh of LY Month Materialized View at ' ||
                  to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));

   else
      dbms_output.put_line ('LY_Month_First_Day:' || v_ly_month_first_day ||
         ', sysdate-(1/24):' || trunc(sysdate-(1/24)) ||
         ' - Do Not Refresh LY Month MVIEW');
   end if;



   ----------------------------------------------------------------
   -- Find Beg/End of Days last year we want to compare against
   ----------------------------------------------------------------
   declare

      c_days_in_year number;
      c_yoy_offset number;
      v_last_hour_date   date  := sysdate - (1/24);

   begin
      dbms_output.put_line(chr(10) || 'Calculate date ranges...');

--    Calculate the date range (begin date and end date) for a year-over-year comparison of month-to-date data
--    The Rules:
--      Want to compare the same days of the week.  This means the start of the date range is always the second day of the month.
--      EXCEPT for leap years.  From 3/1 of a leap year until 2/28 of the following year, the start of the date range is the
--      third day of the month.
--      Normally the number of days in the date range equals today's date.  For example, if today is 2/1/2007, the date range to
--      compare with is 2/2/2006 - 2/3/2006 (1 day).  If today is 9/10/2007, the date range to compare with is
--      9/2/2006 - 9/12/2006 (10 days).  If today is 9/10/2008, leap year rules apply, but the number of days in the range is
--      still the same -- 9/3/2008 - 9/13/2008.  If today is 10/31/2009, the date range is 10/2/2008 - 11/2/2008.
--      EXCEPT for 2/29.  When this happens, we want 28 days in the range.  The range for 2/29 will be the same as the range for
--      2/28.  So on 2/28/2008, the date range to compare with is 2/2/2007 - 3/1/2007.  On 2/29/2008, the date range to compare
--      with is the same -- 2/2/2007 - 3/1/2007.

--    58 is the number of days in January + February, minus 1.
      if ops$oracle.is_leap_year(sysdate-58) = 'Y' then
         c_days_in_year := 366;

--       special logic to change the begin date for 2/28 and 2/29 of a leap year
         if to_char(sysdate,'mmdd') in ('0228','0229') then
            c_yoy_offset := 1;
         else
            c_yoy_offset := 2;
         end if;

      else

         c_days_in_year := 365;
         c_yoy_offset := 1;

--       special logic to change the begin date for 2/28 the year after a leap year
         if ops$oracle.is_leap_year(sysdate-59) = 'Y' then
            c_days_in_year := 366;
            c_yoy_offset := 2;
         end if;
      end if;

      dbms_output.put_line('c_days_in_year=' || c_days_in_year || '      c_yoy_offset=' || c_yoy_offset);

--    Note how we change the end date for 2/29 -- so it's the same as on 2/28 of that year


      select decode(trunc(v_last_hour_date),
                            last_day(trunc(v_last_hour_date)),   trunc((v_last_hour_date)-c_days_in_year, 'month')+c_yoy_offset,
                            trunc(v_last_hour_date, 'month'),    trunc((v_last_hour_date)-c_days_in_year, 'month')+c_yoy_offset,
                            trunc((v_last_hour_date)-(c_days_in_year-1),'month')+c_yoy_offset),
             decode(to_char(v_last_hour_date,'mmdd'),'0229',trunc(v_last_hour_date)-364,trunc(v_last_hour_date)-363),
             decode(to_char(last_day(v_last_hour_date),'mmdd'),
                            '0229',   trunc(last_day(v_last_hour_date))-364,
                            trunc(last_day(v_last_hour_date))-363)
      into v_ly_mtd_beg_date, v_ly_mtd_end_date, v_ly_full_month_end_date
      from   dual;


      dbms_output.put_line('Last Year MTD Comparison:  >= ' || v_ly_mtd_beg_date ||
                           '    and < ' || v_ly_mtd_end_date);

      dbms_output.put_line('Last Year Full Month Comparison:  >= ' || v_ly_mtd_beg_date ||
                           '    and < ' || v_ly_full_month_end_date);
      dbms_output.put_line(chr(10));
   end;

   -------------------------------------------------------------------------
   -- last hours' phone orders.  Get order dollar total/average via order_bills
   -------------------------------------------------------------------------
   select count(*) order_count,
          sum(cob.PRODUCT_AMOUNT +
              cob.ADD_ON_AMOUNT  +
              cob.SERVICE_FEE    +
              cob.SHIPPING_FEE   -
              cob.DISCOUNT_AMOUNT) order_total_amt,
          round(sum(cob.PRODUCT_AMOUNT +
              cob.ADD_ON_AMOUNT  +
              cob.SERVICE_FEE    +
              cob.SHIPPING_FEE   -
              cob.DISCOUNT_AMOUNT) / count(*),2)  order_aov
     into v_last_hour,
          v_last_hour_amt,
          v_last_hour_aov
      from   clean.order_details cod
      join   clean.orders co on (co.order_guid=cod.order_guid)
      join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id
                                                   and cat.transaction_type = 'Order')
      join   clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id
                                                   and additional_bill_indicator = 'N')
      join   ftd_apps.product_master pm on (cod.product_id = pm.product_id)
      where  cat.transaction_date >= TRUNC(v_date,'hh24')-(1/24)
      and    cat.transaction_date < trunc(v_date,'hh24')
      and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
      and    cat.transaction_type = 'Order'
      and    cat.payment_type <> 'NC'
      and    co.origin_id not in ('TEST', 'test')
      and    co.source_code not in ('12743')
      and    co.company_id not in ('ProFlowers')
      and    pm.category <> 'SRVCS';


   -------------------------------------------------------------------------
   -- todays' phone orders from midnight to the hour just ended
   -------------------------------------------------------------------------
   select count(*) order_count,
       sum(cob.PRODUCT_AMOUNT +
           cob.ADD_ON_AMOUNT  +
           cob.SERVICE_FEE    +
           cob.SHIPPING_FEE   -
           cob.DISCOUNT_AMOUNT) order_amt,
       round(sum(cob.PRODUCT_AMOUNT +
           cob.ADD_ON_AMOUNT  +
           cob.SERVICE_FEE    +
           cob.SHIPPING_FEE   -
           cob.DISCOUNT_AMOUNT) / count(*),2)  order_aov
     into v_day_total,
          v_day_amt,
          v_day_aov
   from   clean.order_details cod
   join   clean.orders co on (co.order_guid=cod.order_guid)
   join   clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id
                                                and cat.transaction_type = 'Order')
   join   clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id
                                                and additional_bill_indicator = 'N')
   join   ftd_apps.product_master pm on (cod.product_id = pm.product_id)
   where  cat.transaction_date >= trunc(v_date-(1/24))
   and    cat.transaction_date < trunc(v_date,'hh24')
   and    cod.order_disp_code not in ('In-Scrub','Pending','Removed')
   and    cat.transaction_type = 'Order'
   and    cat.payment_type <> 'NC'
   and    co.origin_id not in ('TEST', 'test')
   and    co.source_code not in ('12743')
   and    co.company_id not in ('ProFlowers')
   and    pm.category <> 'SRVCS';



   -------------------------------------------------------------------------
   -- the multiplier is supplied by Marketing and is used to project orders to the end of the day
   -------------------------------------------------------------------------
   select multiplier
   into   v_multiplier
   from   com_hourly_stats_factors
   where  day = trim(to_char(v_date-(1/24),'Day'))
   and    hour = to_number(to_char(v_date-(1/24),'hh24'));

   -------------------------------------------------------------------------
   -- Forecasts are provided by Marketing.  The table can store multiple forecasts.  A global parm indicates
   -- which forecast to use.  If the value is NULL, it returns the max(forecast_provided_date) for the
   -- current date.  The global parm only needs set if there are multiple forecasts within a month.
   -------------------------------------------------------------------------
   select to_date(value,'yyyy/mm/dd')
   into   v_provided_date
   from   frp.global_parms
   where  context = 'COM Hourly Stats'
   and    name = 'Forecast Provided Date';

   if v_provided_date is null then
      select max(forecast_provided_date)
      into v_provided_date
      from marketing_forecast
      where forecast_date = trunc(v_date-(1/24));
   end if;


   -------------------------------------------------------------------------
   -- If no forecast found, could mean we've moved into a new month or just that the provided date is wrong.
   -- Either way, return a zero so the report can continue.
   -------------------------------------------------------------------------
   begin
      select forecast
      into   v_day_forecast
      from   marketing_forecast
      where  forecast_date = trunc(v_date-(1/24))
      and    forecast_provided_date = v_provided_date;
   exception when others then
      v_day_forecast := 0;
   end;

   -------------------------------------------------------------------------
   -- Get the forecast for the full month
   -------------------------------------------------------------------------
   begin
      select sum(forecast)
      into   v_full_month_forecast
      from   marketing_forecast
      where  to_char(forecast_date,'YYYYMM') = to_char(trunc(v_date-(1/24)),'YYYYMM')
      and    forecast_provided_date = v_provided_date;
   exception when others then
      v_full_month_forecast := 0;
   end;


   -------------------------------------------------------------------------
   -- this is the forecast, from tomorrow to the end of the month
   -------------------------------------------------------------------------
   BEGIN
      select sum(forecast)
      into   v_full_month_remaining_fcst
      from   marketing_forecast
      where  forecast_date > trunc(v_date-(1/24))
        and  forecast_date <= last_day(v_date-(1/24))
        and    forecast_provided_date = v_provided_date;
   EXCEPTION
      when others then
         v_full_month_remaining_fcst := 0;
   END;
   if v_full_month_remaining_fcst is null then
      v_full_month_remaining_fcst := 0;
   end if;


   dbms_output.put_line('monthly forecast after today=' || v_full_month_remaining_fcst);


   -- This is how many orders we expect to have today, based on Marketing multipliers
   v_day_projected_pace := round(v_day_total * v_multiplier);

   select order_count, round(order_amt,2), round(order_aov,2)
   into v_mtd_total, v_mtd_amt, v_mtd_aov
   from mtd_orders_to_yesterday_mv
   where rownum < 2;

   v_mtd_projected_pace := v_mtd_total + v_day_projected_pace;

   dbms_output.put_line('retrieved mtd orders from mview = ' || v_mtd_total);

   -- Last year's MTD orders
   select nvl(sum(order_count),0)
   into v_ly_mtd
   from ly_month_orders_by_day_mv
   where transaction_date >= v_ly_mtd_beg_date
     and transaction_date <  v_ly_mtd_end_date;


   -- Get last year's full month revenue
   select nvl(sum(order_amt),0)
   into v_py_full_month_rev
   from ly_month_orders_by_day_mv
   where transaction_date >= v_ly_mtd_beg_date
     and transaction_date <  v_ly_full_month_end_date;

   if v_ly_mtd <> 0 then
      v_yoy_mtd_pct := v_mtd_projected_pace / v_ly_mtd;
   end if;

   -------------------------------------------------------------------------
   -- this is the forecast for the month, from the 1st to the end of day today
   -------------------------------------------------------------------------
   begin
      select sum(forecast)
      into   v_mtd_forecast
      from   marketing_forecast
      where  forecast_date >= trunc(v_date-(1/24),'month')
      and    forecast_date <= trunc(v_date-(1/24))
      and    forecast_provided_date = v_provided_date;
   exception when others then
      v_mtd_forecast := 0;
   end;

   v_full_month_projected_pace := v_mtd_projected_pace + v_full_month_remaining_fcst;

   if (v_day_projected_pace <> 0) then
      v_day_pct_sunk := v_day_total / v_day_projected_pace;
   end if;

   if (v_day_forecast <> 0) then
      v_day_pct_sunk_to_fcst := v_day_total / v_day_forecast;
   end if;

   if (v_full_month_projected_pace <> 0) then
      v_full_month_pct_sunk := v_mtd_projected_pace / v_full_month_projected_pace;
   end if;

   if (v_full_month_forecast <> 0) then
      v_full_month_pct_sunk_to_fcst := v_mtd_projected_pace / v_full_month_forecast;
   end if;


   dbms_output.put_line('full_month_proj_rev=' || v_mtd_aov || ' * ' || v_full_month_projected_pace);
   v_full_month_projected_rev := v_mtd_aov * v_full_month_projected_pace;

  --DO NOT USE A FORECAST_REV GENERATED FROM ACTUAL AOV - WAITING FOR KURT DIRECTION
  --v_full_month_forecast_rev  := v_mtd_aov * v_full_month_forecast;

   dbms_output.put_line('v_mtd_aov = ' || v_mtd_aov || '        v_mtd_total=' || v_mtd_total);


   if (v_day_forecast <> 0) then
      v_day_pct := v_day_projected_pace / v_day_forecast;
   end if;

   if (v_mtd_forecast <> 0) then
      v_mtd_pct := v_mtd_projected_pace / v_mtd_forecast;
   end if;

   if (v_full_month_forecast <> 0) then
      v_full_month_pct := v_full_month_projected_pace / v_full_month_forecast;
   end if;

   if (v_py_full_month_rev <> 0) then
      v_yoy_full_month_rev_pct   := v_full_month_projected_rev / v_py_full_month_rev;
   end if;


   dbms_output.put_line('mtd_proj_pace=' || v_mtd_projected_pace ||
                        '    v_full_month_remaining=' || v_full_month_remaining_fcst);


   /* The executives want the revenue numbers rounded to the millions on the report */
   v_mtd_amt_rnd                   := round(v_mtd_amt                  /1000000,1);
  -- v_full_month_forecast_rev_rnd   := round(v_full_month_forecast_rev  /1000000,1);
   v_full_month_projected_rev_rnd  := round(v_full_month_projected_rev /1000000,1);
   v_py_full_month_rev_rnd         := round(v_py_full_month_rev        /1000000,1);



   --#####################################################
   -- See if we need to create a HOLIDAY STATS Block
   --#####################################################
   dbms_output.put_line('Holiday month?  v_date-(1/24)=' || to_char(v_date-(1/24),'MMDD'));
   if to_char(v_date-(1/24),'MM') = '02' OR
      to_char(v_date-(1/24),'MM') = '05' OR
      to_char(v_date-(1/24),'MM') = '12' THEN

      v_holiday_errors_flag := 'NO';
      v_holiday_active_flag := 'YES';

      BEGIN
         -- It's a holiday month - find the last day of the "holiday period" (first day is 01):
         select value
         into   v_holiday_end_day
         from   frp.global_parms
         where  context = 'COM Hourly Stats'
         and    name = decode(to_char(v_date-(1/24),'MM'),
                                 '02','Valentines End of Holiday Day',
                                 '05','Mothers Day End of Holiday Day',
                                 '12','Christmas End of Holiday Day',
                                 'What the Heck - this should not happen!');

      EXCEPTION
         when others then
            v_holiday_block := crlf || 'BYPASSING - NO PARM FOR LAST DAY OF HOLIDAY PERIOD';
            v_holiday_errors_flag := 'YES';
      END;


     if v_holiday_errors_flag != 'YES' then

         BEGIN
            -- this is the holiday forecast, from the 1st to the end of the holiday period
            select sum(forecast)
            into   v_holiday_forecast
            from   marketing_forecast
            where  forecast_date >= trunc(v_date-(1/24),'month')
              and  forecast_date <= to_date(to_char(trunc(v_date-(1/24)),'YYYYMM') || v_holiday_end_day,'YYYYMMDD')
              and    forecast_provided_date = v_provided_date;
         EXCEPTION
            when others then
               v_holiday_block := crlf || 'ERROR COMPUTING HOLIDAY FORECAST' || crlf;
               v_holiday_forecast := 0;
               v_holiday_errors_flag := 'YES';
         END;


         BEGIN
            -- this is the forecast for the remainder of the holiday period (after today)
            select nvl(sum(forecast),0)
            into   v_holiday_remaining_forecast
            from   marketing_forecast
            where  forecast_date > trunc(v_date-(1/24))
            and    forecast_date <= to_date(to_char(trunc(v_date-(1/24)),'YYYYMM') || v_holiday_end_day,'YYYYMMDD')
            and    forecast_provided_date = v_provided_date;

         EXCEPTION
            when others then
               v_holiday_block := 'ERROR COMPUTING REMAINING HOLIDAY FORECAST' || crlf;
               v_holiday_remaining_forecast := 0;
               v_holiday_errors_flag := 'YES';
         END;

         dbms_output.put_line('Remaining Holiday Forecast=' || v_holiday_remaining_forecast);
         dbms_output.put_line('Forecast to end of day today=' || v_mtd_forecast);
         dbms_output.put_line('Projected MTD to end of day today=' || v_mtd_projected_pace);

         v_holiday_projected_pace := v_mtd_projected_pace + v_holiday_remaining_forecast;

         dbms_output.put_line('v_holiday_projected_pace=' || v_holiday_projected_pace);


         if (v_holiday_forecast <> 0) then
            v_holiday_pct := v_holiday_projected_pace / v_holiday_forecast;
         end if;

         if (v_holiday_projected_pace <> 0) then
            v_holiday_pct_sunk := v_mtd_projected_pace / v_holiday_projected_pace;
         end if;


         -- Only print if still in the holiday period (we didn't know the last date before coming
         -- into this block)

         if v_holiday_end_day >= to_char(v_date-(1/24),'DD') THEN
            dbms_output.put_line('INCLUDE THE HOLIDAY SECTION');
            -- ADD HOLIDAY SECTION FIELDS
            v_holiday_block :=
                    crlf || crlf || 'HOLIDAY STATS' || '  (Days 01-' || v_holiday_end_day || ')' || crlf ||
                   'projected pace:      '||lpad(to_char(v_holiday_projected_pace,'9,999,999'),12)||crlf||
                   'holiday forecast:    '||lpad(to_char(v_holiday_forecast,'9,999,999'),12)||crlf||
                   'pace v. forecast:    '||lpad(to_char(v_holiday_pct*100,'999.0'),12)||'%'||crlf||
                   '% of holiday sunk:   '||lpad(to_char(v_holiday_pct_sunk*100,'999.0'),12)||'%';
         end if;

       end if;      /* Condition after finding holiday end day parm */
   end if;         /* ############# HOLIDAY SECTION ###################### */





   /**********************************************/
   /* Format the body of the NON-Executive Email */
   /**********************************************/
   v_body := 'DAY STATS (Gross Taken)' || crlf ||
             'last hour:           '||lpad(to_char(v_last_hour,'999,999'),12)||crlf||
             'cumulative:          '||lpad(to_char(v_day_total  ,'999,999'),12)||crlf||
             'projected pace:      '||lpad(to_char(v_day_projected_pace,'999,999'),12)||crlf||
             'forecast as of '||to_char(v_provided_date,'mm-dd')||':  '||lpad(to_char(v_day_forecast,'999,999'),10)||crlf||
             'proj. pace v. forecast: '||lpad(to_char(v_day_pct*100,'999.0'),9)||'%'||crlf||
       --      '% of fcst sunk:    '||lpad(to_char(v_day_pct_sunk*100,'999.0'),12)||'%'||
             '% taken v. forecast:      '||lpad(to_char(v_day_pct_sunk_to_fcst*100,'999.0'),7)||'%'||
             crlf || crlf || 'MTD STATS (Gross Taken)' || crlf ||
             'mtd projected:       '||lpad(to_char(v_mtd_projected_pace,'9,999,999'),12)||crlf||
             'mtd forecast:        '||lpad(to_char(v_mtd_forecast,'9,999,999'),12)||crlf||
             'pace v. forecast:    '||lpad(to_char(v_mtd_pct*100,'999.0'),12)||'%' ||crlf||
        --     crlf || crlf || 'FULL MONTH STATS' || crlf ||
        --     'proj full month:     '||lpad(to_char(v_full_month_projected_pace,'9,999,999'),12)||crlf||
        --     'forecast full month: '||lpad(to_char(v_full_month_forecast,'9,999,999'),12)||crlf||
        --     'pace v. forecast:    '||lpad(to_char(v_full_month_pct*100,'999.0'),12)||'%'||crlf||
        --     '% of month sunk:     '||lpad(to_char(v_full_month_pct_sunk*100,'999.0'),12)||'%';
             '% taken v. full month forecast:'||lpad(to_char(v_full_month_pct_sunk_to_fcst*100,'999.0'),6)||'%';
   v_body := v_body || v_holiday_block;
   v_body := v_body ||
             crlf || crlf || 'YOY STATS (Day to Day)' || crlf ||
             'Last year mtd total: '||lpad(to_char(v_ly_mtd,'9,999,999'),12)||crlf||
             'yoy growth:          '||lpad(to_char(v_yoy_mtd_pct*100,'999.0'),12)||'%';



   /**********************************************/
   /* Format the body of the EXECUTIVE Email     */
   /**********************************************/

   v_exec_body :=
                '---- EOD ----   ' ||crlf||
                'Fcst Orders: ' ||lpad(to_char(v_day_forecast,'999,999'),10)||crlf||
                'Proj Orders: ' ||lpad(to_char(v_day_projected_pace,'999,999'),10)||crlf||
                '% Variance:     ' ||lpad(to_char(v_day_pct*100,'999.0'),7)||'%'||crlf||
                           crlf ||
                '---- MTD ----   ' ||crlf||
          --      'MTD Fcst AOV:              ' || '     (TBD)'||crlf||
                'Actual AOV:     ' ||lpad(to_char(v_mtd_aov  ,'$999.99'),7)||crlf||
          --      '% Variance:                ' || '     (TBD)'||crlf||
          --                 crlf ||
          --      'MTD Fcst Rev:            ' || '     (TBD)'||crlf||
                'Actual Rev:    ' || lpad(to_char(v_mtd_amt_rnd,'$999.9')||'M',8)||crlf||
          --      '% Variance:                ' || '     (TBD)'||crlf||
                          crlf ||
                '---- Full Month ----   ' ||crlf||
                'Fcst orders: ' ||lpad(to_char(v_full_month_forecast,'9,999,999'),10)||crlf||
                'Proj orders: ' ||lpad(to_char(v_full_month_projected_pace,'9,999,999'),10)||crlf||
                '% Variance:     ' ||lpad(to_char(v_full_month_pct*100,'999.0'),7)||'%'||crlf||
                          crlf ||
          --      'Fcst Full Month Rev:       ' ||lpad(to_char(v_full_month_forecast_rev_rnd,'$999.9')||'M',10)||crlf||
                'Proj Rev:      ' ||lpad(to_char(v_full_month_projected_rev_rnd,'$999.9')||'M',8)||crlf||
           --     '% Variance:                ' ||lpad(to_char(v_full_month_rev_pct*100,'999.0'),10)||'%'||crlf||
                          crlf ||
          --      'Prior Year Full Month Rev: ' ||lpad(to_char(v_py_full_month_rev,'$999,999,999'),13)||crlf||
                '---- Prior Year ----' ||crlf||
                'Full Month Rev:' ||lpad(to_char(v_py_full_month_rev_rnd,'$999.9')||'M',8)||crlf||
                '% Variance:     ' ||lpad(to_char(v_yoy_full_month_rev_pct*100,'999.0'),7)||'%';



   /*****************************************************************************************/
   /* Build the attachment in a file. - The attachment only goes to the Non-Executive Email */
   /*****************************************************************************************/
   r_font.fName              := 'Calibri';
   r_font.fSize              := 11;
   r_customStyles.font       := r_font;

   r_customStyles.id         := 'PlainText';
   r_customStyles.background := r_background;
   tab_customStyles(1)       := r_customStyles;

   r_customStyles.id         := 'PlainInteger';
   r_customStyles.format     := '##,###,##0';
   r_customStyles.background := r_background;
   tab_customStyles(2)       := r_customStyles;

   r_customStyles.id         := 'IntegerNoCommas';
   r_customStyles.format     := '#######0';
   r_customStyles.background := r_background;
   tab_customStyles(3)       := r_customStyles;

   r_customStyles.id         := 'Money';
   r_customStyles.format     := '#,###,##0.00';
   r_customStyles.background := r_background;
   tab_customStyles(4)       := r_customStyles;

   r_customStyles.id         := 'PlainTextRight';
   r_customStyles.format     := 'General';
   r_customStyles.background := r_background;
   r_alignment.horizontal    := 'Right';
   r_customStyles.alignment  := r_alignment;
   tab_customStyles(5)       := r_customStyles;

      -- new workbook
   v_file_handle := ops$oracle.xml_spreadsheet.createNewFile(p_path     => 'LOG_FILE_OUTPUT',
                                                             p_filename => v_file_name,
                                                             p_tab_customstyles => tab_customStyles);

   -- column widths
   tab_columns(1).cWidth := 55; --txn_date
   tab_columns(2).cWidth := 26; --hour
   tab_columns(3).cWidth := 81; --phone_internet
   tab_columns(4).cWidth := 63; --order_count
   tab_columns(5).cWidth := 65; --merch_value
   tab_columns(6).cWidth := 65; --add_ons
   tab_columns(7).cWidth := 67; --shipping_fee
   tab_columns(8).cWidth := 65; --service_fee
   tab_columns(9).cWidth := 89; --discount_amount
   tab_columns(10).cWidth := 65; --order_value
   tab_columns(11).cWidth := 65; --tax
   tab_columns(12).cWidth := 103; --total_order_amount

   -- new tab
   ops$oracle.xml_spreadsheet.newWorksheet(v_file_handle,'Orders by Origin Type',ops$oracle.xml_spreadsheet.g_tab_caption,tab_columns);

   -- write column headers
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'TXN Date', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Hour', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Phone/Internet', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Order Count', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Merch Value', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Add Ons', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Shipping Fee', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Service Fee', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Discount Amount', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Order Value', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Tax', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Total Order Amount', 'PlainTextRight');

   open com_cursor (v_date);
   loop
      fetch com_cursor into com_row;
      exit when com_cursor%notfound;

      -- new row
      ops$oracle.xml_spreadsheet.newDataRow(v_file_handle);
      -- write cells
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.txn_date          , 'PlainText');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.hour              , 'PlainText');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.phone_internet    , 'PlainText');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.order_count       , 'PlainInteger');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.merch_value       , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.add_ons           , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.shipping_fee      , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.service_fee       , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.discount_amount   , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.order_value       , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.tax               , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, com_row.total_order_amount, 'Money');

   end loop;

   -- Close the spreadsheet
   ops$oracle.xml_spreadsheet.closeWorksheet(v_file_handle,1);

   -- column widths
   tab_columns(1).cWidth := 55; --txn_date
   tab_columns(2).cWidth := 146; --source_type
   tab_columns(3).cWidth := 63; --order_count
   tab_columns(4).cWidth := 65; --merch_value
   tab_columns(5).cWidth := 65; --add_ons
   tab_columns(6).cWidth := 67; --shipping_fee
   tab_columns(7).cWidth := 65; --service_fee
   tab_columns(8).cWidth := 89; --discount_amount
   tab_columns(9).cWidth := 65; --order_value
   tab_columns(10).cWidth := 65; --tax
   tab_columns(11).cWidth := 103; --total_order_amount
   tab_columns(12).cWidth := 60;

   -- new tab
   ops$oracle.xml_spreadsheet.newWorksheet(v_file_handle,'Orders by Sub-Channel',ops$oracle.xml_spreadsheet.g_tab_caption,tab_columns);

   -- write column headers
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'TXN Date', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Source Type', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Order Count', 'PlainText');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Merch Value', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Add Ons', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Shipping Fee', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Service Fee', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Discount Amount', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Order Value', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Tax', 'PlainTextRight');
   ops$oracle.xml_spreadsheet.writeData(v_file_handle, 'Total Order Amount', 'PlainTextRight');

   open source_cursor (v_date);
   loop
      fetch source_cursor into source_row;
      exit when source_cursor%notfound;

      -- new row
      ops$oracle.xml_spreadsheet.newDataRow(v_file_handle);
      -- write cells
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.txn_date          , 'PlainText');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.source_type       , 'PlainText');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.order_count       , 'PlainInteger');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.merch_value       , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.add_ons           , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.shipping_fee      , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.service_fee       , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.discount_amount   , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.order_value       , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.tax               , 'Money');
      ops$oracle.xml_spreadsheet.writeData (v_file_handle, source_row.total_order_amount, 'Money');

   end loop;

   -- Close the spreadsheet
   ops$oracle.xml_spreadsheet.closeWorksheet(v_file_handle,1);

   -- Close the file
   ops$oracle.xml_spreadsheet.closeFile(v_file_handle);



   /***************************************/
   /* Send out the NON-Executive Email    */
   /***************************************/
   BEGIN
      --
      -- Generate recipient list and open email connection
      --
      v_recipient_list := '';
      dbms_output.put_line('Retrieving recipient list for hour: ' || to_char(v_date,'hh24') || ' holiday: ' || v_holiday_active_flag);
      open c_recipient_list(to_number(to_char(v_date,'hh24')), v_holiday_active_flag);
      loop
         fetch c_recipient_list into r_recipient_list;
         exit when c_recipient_list%notfound;
         v_recipient_list := v_recipient_list || '''' || r_recipient_list.project ||''',';
      end loop;
      close c_recipient_list;
      if length(v_recipient_list) > 0 then
         v_recipient_list := substr(v_recipient_list,1,length(v_recipient_list)-1);
      else
         dbms_output.put_line('No recipients, exiting');
         return;
      end if;

      --  ############### REMOVE THE FOLLOWING LINE AFTER TESTING ################
      -- v_recipient_list := '''TEST COM HOURLY STATS''';

      dbms_output.put_line('Non exec recipient list=' || v_recipient_list);

      conn := ops$oracle.demo_mail.begin_mail_multilist(
            recipient_projects => v_recipient_list,
            subject => 'FTD.COM Hourly Orders Taken '||v_start_hour||' - '||v_end_hour,
            mime_type => ops$oracle.demo_mail.MULTIPART_MIME_TYPE);
   END;


   BEGIN
      --
      -- Put in the body of the email.
      --
      -- The first call to attach_text will be the body of the mess*/
      -- You can format the message with HTML formatting codes */

      ops$oracle.demo_mail.attach_mb_text(
           conn => conn,
           data => v_body,
           mime_type => 'text/plain; charset=iso-8859-1' );

   END attach_text;

   BEGIN
      /* You must call begin_attachment and end_attachment */
      /* for any files you wish to attach to the message */
      /* The first example here is for a plain text attachment */
      /* It makes repeated calls to demo_mail.write_text */
      ops$oracle.demo_mail.begin_attachment(conn => conn,
                                 mime_type => 'text/plain; charset=iso-8859-1',
                                 inline => TRUE,
                                 filename => v_file_name,
                                 transfer_enc => '8 bit');
      v_file_handle_2 := utl_file.fopen(location => 'LOG_FILE_OUTPUT',
                                      filename => v_file_name,
                                      open_mode => 'r',
                                      max_linesize => 5000);
      begin
         loop
            utl_file.get_line(v_file_handle_2, v_line, 5000);
            mesg := v_line || crlf;
            ops$oracle.demo_mail.write_text(conn => conn, message => mesg);
         end loop;
      exception when others then null;
      end;
      utl_file.fclose(v_file_handle_2);
      ops$oracle.demo_mail.end_attachment (conn => conn);
   END begin_attachment;

   v_success_indicator := 0;
   v_retry_count := 0;
   while v_success_indicator = 0 loop
      begin
         /* Must call this for anything to work */
         ops$oracle.demo_mail.end_mail(conn => conn);
         v_success_indicator := 1;
         dbms_output.put_line('I completed the end_mail call');
      exception when others then
         v_retry_count := v_retry_count + 1;
         dbms_output.put_line('EXCEPTION: the end_mail call failed');
         if v_retry_count > 10 then
            dbms_output.put_line('Giving up');
            raise;
         end if;
      end;
   end loop;



   /******************************************************************/
   /*  Now Send out the Executive Email - This one has no attachment */
   /******************************************************************/

-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--
--  ***********************************
--  *  *  * W  A  R  N  I  N  G *  *  *
--  ***********************************
--
-- 7/1/2008  UNDER NO CIRCUMSTANCES SHOULD THE "EXECUTIVE" EMAIL BE SENT OUT WITHOUT A WRITTEN
-- REQUEST/APPROVAL FROM EVP OF CONSUMER (MICHAEL BURGESS AT THE TIME OF THIS DIRECTION).
--
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--
--   BEGIN
--      --
--      -- Generate recipient list and open email connection
--      --
--      v_recipient_list_execs := '';
--      open c_recipient_list_execs(to_number(to_char(v_date,'hh24')));
--      loop
--         fetch c_recipient_list_execs into r_recipient_list_execs;
--         exit when c_recipient_list_execs%notfound;
--         v_recipient_list_execs := v_recipient_list_execs || '''' || r_recipient_list_execs.project ||''',';
--      end loop;
--      close c_recipient_list_execs;
--      v_recipient_list_execs := substr(v_recipient_list_execs,1,length(v_recipient_list_execs)-1);
--      dbms_output.put_line('extracted EXECUTIVE recipient list=' || v_recipient_list_execs);
--
--      --  ############### REMOVE THE FOLLOWING LINE AFTER TESTING ################
--      -- v_recipient_list_execs := '''TEST COM HOURLY STATS''';
--
--      dbms_output.put_line('using recipient list=' || v_recipient_list_execs
--                           || ',   Strlen=' || length(v_recipient_list_execs));
--   END;
--
--   if length(v_recipient_list_execs) > 0 THEN
--      BEGIN
--         conn_exec := ops$oracle.demo_mail.begin_mail_multilist(
--            recipient_projects => v_recipient_list_execs,
--            subject => 'FTD.COM Executive Hourly Summary '||v_start_hour||' - '||v_end_hour,
--            mime_type => ops$oracle.demo_mail.MULTIPART_MIME_TYPE);
--      END;
--
--      BEGIN
--         ops$oracle.demo_mail.attach_mb_text(
--              conn => conn_exec,
--              data => v_exec_body,
--              mime_type => 'text/plain; charset=iso-8859-1' );
--      END attach_text;
--
--      v_success_indicator := 0;
--      v_retry_count := 0;
--      while v_success_indicator = 0 loop
--         begin
--            /* Must call this for anything to work */
--            ops$oracle.demo_mail.end_mail(conn => conn_exec);
--            v_success_indicator := 1;
--            dbms_output.put_line('I completed the EXEC end_mail call');
--         exception when others then
--            v_retry_count := v_retry_count + 1;
--            dbms_output.put_line('EXCEPTION: the EXEC end_mail call failed');
--            if v_retry_count > 10 then
--               dbms_output.put_line('Giving up');
--               raise;
--            end if;
--         end;
--      end loop;
--   else
--      dbms_output.put_line('No report for execs this hour');
--   end if;



   dbms_output.put_line('Ending send_com_hourly_stats proc at ' || to_char(sysdate,'MM/DD/YYYY HH24:MI:SS'));
END send_com_hourly_stats;
/
