CREATE OR REPLACE PACKAGE BODY STATS.DASHBOARD_STATS_PKG
AS

FUNCTION GET_PROJECTED_ORDERS
(
   IN_START_TIMESTAMP          IN  TIMESTAMP,
   IN_STOP_TIMESTAMP           IN  TIMESTAMP
) 
RETURN NUMBER
AS
--==========================================================================
--
-- Name:    GET_PRODJECTED_ORDERS
-- Type:    Function
-- Syntax:  GET_PRODUCT_INDEX_NAMES ()
-- Syntax:  GET_PRODJECTED_ORDERS ()
-- Returns: The number of projected orders for the time period
--
-- Description:   Queries the MARKETING_FORECAST and COM_HOURLY_STATS_FACTORS tables 
--                and calculates the order count for the time period.
--                Used by CREATE_ORDER_STATS.
--
--==========================================================================

    v_forecast     NUMBER := 0;

    v_hour_multiplier        NUMBER;
    v_prev_hour_multiplier   NUMBER;
    v_hour                   NUMBER;
    v_day_forecast           NUMBER;
    v_hour_forecast          NUMBER;
    v_seconds_time           NUMBER;
    v_provided_date   date;      /* This identifies a forecast, for the case where we have multiple forecasts. */


BEGIN

    select to_date(value,'yyyy/mm/dd')
    into   v_provided_date
    from   frp.global_parms
    where  context = 'COM Hourly Stats'
      and  name = 'Forecast Provided Date';

    if v_provided_date is null then
        select max(forecast_provided_date)
        into v_provided_date
        from marketing_forecast
        where forecast_date = trunc(IN_START_TIMESTAMP-(1/24));
    end if;


    select hf.multiplier, hf.hour, fo.forecast
    into v_hour_multiplier, v_hour, v_day_forecast
    from stats.com_hourly_stats_factors hf,
         stats.marketing_forecast fo
    where 1=1
      and to_char(IN_START_TIMESTAMP,'hh24') = hf.hour
      and fo.forecast_date = trunc(IN_START_TIMESTAMP)
      and trim(to_char(IN_START_TIMESTAMP,'Day')) = hf.day
      and fo.forecast_provided_date = v_provided_date;

    IF v_hour = 0 THEN
        v_prev_hour_multiplier := 100;
        v_hour_forecast := ( 100 / v_hour_multiplier) * (v_day_forecast) / 100;
    ELSE
        select hf.multiplier
        into v_prev_hour_multiplier
        from stats.com_hourly_stats_factors hf,
             stats.marketing_forecast fo
        where 1=1
          and to_char(IN_START_TIMESTAMP,'hh24') = (hf.hour + 1)
          and fo.forecast_date = trunc(IN_START_TIMESTAMP)
          and trim(to_char(IN_START_TIMESTAMP,'Day')) = hf.day
          and fo.forecast_provided_date = v_provided_date;

        v_hour_forecast := (( 100 / v_hour_multiplier) - ( 100 / v_prev_hour_multiplier)) * (v_day_forecast) / 100;
    END IF;


    -- Change the hour to the time difference
    select extract (MINUTE from (IN_STOP_TIMESTAMP - IN_START_TIMESTAMP)) * 60  + 
           extract (SECOND from (IN_STOP_TIMESTAMP - IN_START_TIMESTAMP))
    into v_seconds_time
    from dual;

    v_forecast := v_hour_forecast * v_seconds_time / 3600;

    RETURN v_forecast;
    
EXCEPTION
   WHEN NO_DATA_FOUND THEN
      RETURN 0;

END GET_PROJECTED_ORDERS;

PROCEDURE CREATE_FLORIST_STATS
(
  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the florist statistics

Input:

Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_stat_timestamp    TIMESTAMP;

v_covered_zip_count  NUMBER;
v_gnadd_zip_count    NUMBER;

v_zip_stats_seq      NUMBER;

BEGIN

  SELECT sysdate
  INTO v_stat_timestamp
  FROM dual;

  BEGIN
      INSERT INTO FLORIST_GOTO (stat_timestamp, territory, florist_goto_count, florist_goto_id)
          select v_stat_timestamp,territory, florist_goto_count, florist_goto_seq.nextval
          from
            (SELECT territory,COUNT(florist_id) florist_goto_count
                 FROM ftd_apps.florist_master
                WHERE super_florist_flag = 'Y'
                      AND record_type = 'R'
                      AND status <> 'Inactive'
                      AND territory IS NOT NULL
                GROUP BY territory) terr_counts;

      EXCEPTION WHEN OTHERS THEN NULL;
  END;

  BEGIN
      INSERT INTO FLORIST_GOTO_SUSPEND (stat_timestamp, territory, 
                                        florist_goto_suspend_count, florist_goto_suspend_id)
      SELECT v_stat_timestamp, territory, florist_count, florist_goto_suspend_seq.nextval FROM
      (SELECT territory,COUNT(florist_id) florist_count
        FROM ftd_apps.florist_master
      WHERE super_florist_flag = 'Y'
        AND record_type = 'R'
        AND status in ('GoTo Suspend', 'Blocked')
        AND territory IS NOT NULL
      GROUP BY territory) terr_counts;

      EXCEPTION WHEN OTHERS THEN NULL;
  END;

  BEGIN
      INSERT INTO FLORIST_GOTO_SHUTDOWN (stat_timestamp, territory, 
                                        florist_goto_shutdown_count, florist_goto_shutdown_id)
      SELECT v_stat_timestamp, territory, florist_count, florist_goto_shutdown_seq.nextval FROM
      (SELECT territory, COUNT(fg.florist_id) florist_count
        FROM ftd_apps.goto_florist_shutdown fg, ftd_apps.florist_master fm
        WHERE fg.florist_id = fm.florist_id
        AND record_type = 'R'
        AND territory IS NOT NULL
      GROUP BY territory) terr_counts;

      EXCEPTION WHEN OTHERS THEN NULL;
  END;

  BEGIN
      INSERT INTO MERCURY_SHOP (stat_timestamp, territory, mercury_shop_count,mercury_shop_id)
      SELECT v_stat_timestamp, territory, florist_count, mercury_shop_seq.nextval FROM
      (SELECT territory, COUNT(florist_id) florist_count
        FROM ftd_apps.florist_master
       WHERE mercury_flag = 'M'
                AND status <> 'Inactive'
         AND record_type = 'R'
         AND territory IS NOT NULL
      GROUP BY territory) terr_counts;
      EXCEPTION WHEN OTHERS THEN NULL;
  END;

  BEGIN
      INSERT INTO MERCURY_SHOP_SHUTDOWN  (stat_timestamp, territory, 
                                          mercury_shop_shutdown_count,mercury_shop_shutdown_id)
      SELECT v_stat_timestamp, territory, florist_count, mercury_shop_shutdown_seq.nextval FROM
      (SELECT territory, COUNT(florist_id) florist_count
        FROM ftd_apps.florist_master
               WHERE mercury_flag = 'M'
        AND record_type = 'R'
        AND status NOT IN ('Active', 'Inactive')
        AND territory IS NOT NULL
      GROUP BY territory) terr_counts;
      EXCEPTION WHEN OTHERS THEN NULL;
  END;

  BEGIN
      SELECT COUNT(DISTINCT zip_code)
        INTO v_covered_zip_count
               FROM ftd_apps.florist_zips, ftd_apps.csz_avail
        where zip_code = csz_zip_code;

      EXCEPTION WHEN NO_DATA_FOUND THEN v_covered_zip_count := 0;
  END;

  BEGIN
      SELECT COUNT(csz_zip_code)
        INTO v_gnadd_zip_count
        FROM ftd_apps.csz_avail
       WHERE gnadd_flag = 'Y';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_gnadd_zip_count := 0;
  END;

  select zip_stats_seq.nextval into v_zip_stats_seq from dual;

  insert into zip_stats
    (zip_stats_id,
     stat_timestamp,
     covered_zip_count,
     gnadd_zip_count)
    values
    (v_zip_stats_seq,
     v_stat_timestamp,
     v_covered_zip_count,
     v_gnadd_zip_count);



  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
        
END CREATE_FLORIST_STATS;

PROCEDURE CREATE_QUEUE_STATS
(
  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the queue statistics

Input:

Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/

CURSOR queue_cur is 
      select queue_type, count(*) queue_count
      from clean.queue
      group by queue_type;

CURSOR other_cur is 
      select 'OTHER' queue_type, count(o.order_guid)  queue_count
      from scrub.order_details od,
           scrub.orders o
      where 1=1
        and od.status in ('2002','2003','2005')
        and o.order_origin not in ('TEST','test')
        and o.order_guid = od.order_guid
	and od.updated_on > (sysdate - 180);

v_stat_timestamp    TIMESTAMP;

v_queue_id NUMBER;

BEGIN

  SELECT sysdate
  INTO v_stat_timestamp
  FROM dual;


  BEGIN
    FOR queue_rec in queue_cur LOOP
      select queue_stats_seq.nextval into v_queue_id from dual;

      insert into queue_stats (queue_stats_id, stat_timestamp, queue_type, queue_count)
          values (v_queue_id, v_stat_timestamp, queue_rec.queue_type, queue_rec.queue_count);
    END LOOP;

  END;
  BEGIN

    FOR other_rec in other_cur LOOP
      select queue_stats_seq.nextval into v_queue_id from dual;

      insert into queue_stats (queue_stats_id, stat_timestamp, queue_type, queue_count)
          values (v_queue_id, v_stat_timestamp, other_rec.queue_type, other_rec.queue_count);
    END LOOP;
	

  END;
 
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
        
END CREATE_QUEUE_STATS;

PROCEDURE CREATE_ORDER_STATS
(
  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the order statistics

Input:

Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_stat_timestamp        TIMESTAMP;
v_old_stat_timestamp    TIMESTAMP;
v_internet_count NUMBER;
v_phone_count NUMBER;
v_vendor_count NUMBER;
v_florist_count NUMBER;
v_total_count NUMBER;
v_forecast_count NUMBER;
v_order_stats_id NUMBER;

CURSOR order_cur (start_timestamp TIMESTAMP, stop_timestamp TIMESTAMP) is
        SELECT origin_type,od.ship_method,COUNT(od.order_detail_id) order_count
          FROM clean.order_details od,
               clean.orders o, 
               clean.accounting_transactions cat,
               ftd_apps.origins fao
         WHERE cat.transaction_date  >= start_timestamp
           AND cat.transaction_date  < stop_timestamp
           AND od.order_disp_code NOT IN ('In-Scrub', 'Removed', 'Pending')
           AND o.order_guid = od.order_guid
           AND o.origin_id NOT IN ('test','TEST')
           and cat.transaction_type = 'Order'
           and cat.payment_type <> 'NC'
           AND cat.order_detail_id = od.order_detail_id
           AND o.origin_id = fao.origin_id
         group by origin_type, od.ship_method;

BEGIN

    v_internet_count := 0;
    v_phone_count := 0;
    v_vendor_count := 0;
    v_florist_count := 0;
    v_total_count := 0;

  SELECT sysdate
  INTO v_stat_timestamp
  FROM dual;

  SELECT nvl(max(stat_timestamp), sysdate - (300 / 86400))
  INTO v_old_stat_timestamp
  FROM order_stats;

  BEGIN
      v_forecast_count := GET_PROJECTED_ORDERS(v_old_stat_timestamp, v_stat_timestamp);

      FOR order_rec in order_cur(v_old_stat_timestamp, v_stat_timestamp) LOOP
        IF order_rec.origin_type = 'internet' THEN
            v_internet_count := v_internet_count + order_rec.order_count;
        ELSE
            v_phone_count := v_phone_count + order_rec.order_count;
        END IF;

	IF order_rec.ship_method = 'SD' OR order_rec.ship_method is null THEN
            v_florist_count := v_florist_count + order_rec.order_count;
        ELSE
            v_vendor_count := v_vendor_count + order_rec.order_count;
        END IF;

	v_total_count := v_total_count + order_rec.order_count;

      END LOOP;

      select order_stats_seq.nextval into v_order_stats_id from dual;

      insert into order_stats (stat_timestamp, total_order_count, internet_order_count, 
                               phone_order_count, florist_order_count, vendor_order_count, forecast_order_count,
                               order_stats_id)
          values (v_stat_timestamp, v_total_count, v_internet_count,
                  v_phone_count, v_florist_count, v_vendor_count, v_forecast_count,
                  v_order_stats_id);
  END;
 
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
        
END CREATE_ORDER_STATS;

PROCEDURE CREATE_PRODUCT_STATS
(
  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the product statistics

Input:

Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_stat_timestamp        TIMESTAMP;
v_old_stat_timestamp    TIMESTAMP;
v_internet_count NUMBER;
v_phone_count NUMBER;
v_vendor_count NUMBER;
v_florist_count NUMBER;
v_total_count NUMBER;
v_product_stats_id NUMBER;


CURSOR order_cur (start_timestamp TIMESTAMP, stop_timestamp TIMESTAMP) is
    SELECT 
           decode(od.ship_method,null,'Florist','SD','Florist','Vendor') delivery_type,
           od.product_id,
           COUNT(od.order_detail_id) order_count
      FROM clean.order_details od,
           clean.orders o, 
           clean.accounting_transactions cat,
           ftd_apps.origins fao
     WHERE 1=1 
       AND cat.transaction_date  >= start_timestamp
       AND cat.transaction_date  < stop_timestamp
       AND od.order_disp_code NOT IN ('In-Scrub', 'Removed', 'Pending')
       AND o.order_guid = od.order_guid
       AND o.origin_id NOT IN ('test','TEST')
       and cat.transaction_type = 'Order'
       and cat.payment_type <> 'NC'
       AND cat.order_detail_id = od.order_detail_id
       AND o.origin_id = fao.origin_id
     group by decode(od.ship_method,null,'Florist','SD','Florist','Vendor'), od.product_id;

BEGIN

    v_internet_count := 0;
    v_phone_count := 0;
    v_vendor_count := 0;
    v_florist_count := 0;
    v_total_count := 0;

  SELECT sysdate
  INTO v_stat_timestamp
  FROM dual;

  SELECT nvl(max(stat_timestamp), sysdate - (300 / 86400))
  INTO v_old_stat_timestamp
  FROM product_stats;

  BEGIN
    FOR order_rec in order_cur(v_old_stat_timestamp, v_stat_timestamp) LOOP
        SELECT product_stats_seq.nextval into v_product_stats_id from dual;

        insert into product_stats (product_stats_id, stat_timestamp, product_id, delivery_type, order_count)
            values (v_product_stats_id, v_stat_timestamp, order_rec.product_id, order_rec.delivery_type, order_rec.order_count);
    END LOOP;

  END;
 
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
        
END CREATE_PRODUCT_STATS;

PROCEDURE CREATE_PAYMENT_STATS
(
  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the payment statistics

Input:

Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_stat_timestamp        TIMESTAMP;
v_old_stat_timestamp    TIMESTAMP;
v_payment_stats_id NUMBER;

CURSOR order_cur (start_timestamp TIMESTAMP, stop_timestamp TIMESTAMP) is
        SELECT p.payment_type,COUNT(p.payment_id) payment_count
          FROM clean.order_details od,
               clean.orders o, 
               ftd_apps.origins fao,
               clean.payments p
         WHERE p.created_on  >= start_timestamp
           AND p.created_on  < stop_timestamp
           AND od.order_disp_code NOT IN ('In-Scrub', 'Removed', 'Pending')
           AND o.order_guid = od.order_guid
           AND o.origin_id NOT IN ('test','TEST')
           and p.payment_type <> 'NC'
           and p.payment_indicator = 'P'
           and p.additional_bill_id is null
           AND o.origin_id = fao.origin_id
           and p.order_guid = o.order_guid
         group by p.payment_type;


BEGIN

  SELECT sysdate
  INTO v_stat_timestamp
  FROM dual;

  SELECT nvl(max(stat_timestamp), sysdate - (300 / 86400))
  INTO v_old_stat_timestamp
  FROM payment_stats;

  BEGIN

      FOR payment_rec in order_cur(v_old_stat_timestamp, v_stat_timestamp) LOOP

          select payment_stats_sq.nextval into v_payment_stats_id from dual;
    
          insert into payment_stats (stat_timestamp, payment_type, payment_count,  payment_stats_id)
              values (v_stat_timestamp, payment_rec.payment_type, payment_rec.payment_count,
                      v_payment_stats_id);

      END LOOP;
  END;
 
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
        
END CREATE_PAYMENT_STATS;

PROCEDURE CREATE_AUTHORIZATION_STATS
(
  IN_STAT_TIMESTAMP           IN AUTHORIZATION_STATS.STAT_TIMESTAMP%TYPE,
  IN_CREDIT_CARD_TYPE         IN AUTHORIZATION_STATS.CREDIT_CARD_TYPE%TYPE,
  IN_APPROVAL_COUNT           IN AUTHORIZATION_STATS.APPROVAL_COUNT%TYPE,
  IN_DECLINE_COUNT            IN AUTHORIZATION_STATS.DECLINE_COUNT%TYPE,
  IN_SERVER_NAME              IN AUTHORIZATION_STATS.SERVER_NAME%TYPE,
  IN_AUTH_CLIENT              IN AUTHORIZATION_STATS.AUTH_CLIENT%TYPE,


  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the authorization statistics

Input:

Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_auth_stats_id NUMBER;

BEGIN

  BEGIN

      select authorization_stats_sq.nextval into v_auth_stats_id from dual;

      insert into authorization_stats (authorization_stats_id, stat_timestamp, CREDIT_CARD_TYPE, APPROVAL_COUNT,
                                       DECLINE_COUNT, SERVER_NAME, AUTH_CLIENT)
          values (v_auth_stats_id, IN_STAT_TIMESTAMP, IN_CREDIT_CARD_TYPE, IN_APPROVAL_COUNT,
                  IN_DECLINE_COUNT, IN_SERVER_NAME, IN_AUTH_CLIENT);
  END;
 
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
        
END CREATE_AUTHORIZATION_STATS;

PROCEDURE CREATE_ORDER_SERVICE_STATS
(
  IN_NODE               IN ORDER_SERVICE_STATS.NODE%TYPE,
  IN_CLIENT_ID          IN ORDER_SERVICE_STATS.CLIENT_ID %TYPE,
  IN_ACTION_TYPE        IN ORDER_SERVICE_STATS.ACTION_TYPE %TYPE,
  IN_STAT_COUNT         IN ORDER_SERVICE_STATS.STAT_COUNT%TYPE,
  IN_STATUS_CODE        IN ORDER_SERVICE_STATS.STATUS_CODE %TYPE,
  IN_STAT_TIMESTAMP     IN ORDER_SERVICE_STATS.STAT_TIMESTAMP %TYPE,

  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the order statistics

Input:
  IN_NODE
  IN_CLIENT_ID
  IN_ACTION_TYPE
  IN_STAT_COUNT
  IN_STATUS_CODE
  IN_STAT_TIMESTAMP
Output:
        OUT_STATUS
        OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_order_stats_id NUMBER;

BEGIN 
 BEGIN 
  select order_service_stats_sq.nextval into v_order_stats_id from dual;

  INSERT INTO ORDER_SERVICE_STATS
  (
   ORDER_SERVICE_STATS_ID,
   NODE,
   CLIENT_ID,
   ACTION_TYPE,
   STAT_COUNT,
   STATUS_CODE,
   STAT_TIMESTAMP
  )
  VALUES
  (
   v_order_stats_id,
   IN_NODE,
   IN_CLIENT_ID,
   IN_ACTION_TYPE,
   IN_STAT_COUNT,
   IN_STATUS_CODE,
   IN_STAT_TIMESTAMP
  );


  OUT_STATUS := 'Y';
 END;
 
  EXCEPTION WHEN OTHERS THEN
  BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);  END;
        
END CREATE_ORDER_SERVICE_STATS;

PROCEDURE INSERT_AUTH_RESPONSE_STATS
(
  IN_REFERENCE_NUMBER   		IN AUTHORIZATION_RESPONSE_STATS.REFERENCE_NUMBER%TYPE,
  IN_CREDIT_CARD_TYPE           IN AUTHORIZATION_RESPONSE_STATS.CREDIT_CARD_TYPE%TYPE,
  IN_VALIDATION_STATUS_CODE     IN AUTHORIZATION_RESPONSE_STATS.VALIDATION_STATUS_CODE%TYPE,
  IN_REQUEST_XML         		IN AUTHORIZATION_RESPONSE_STATS.REQUEST_XML%TYPE,
  IN_RESPONSE_XML        		IN AUTHORIZATION_RESPONSE_STATS.RESPONSE_XML%TYPE,
  IN_TIMESTAMP     				IN AUTHORIZATION_RESPONSE_STATS.TIMESTAMP%TYPE,

  OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the authorisation response statistics

Input:
  IN_REFERENCE_NUMBER
  IN_CREDIT_CARD_TYPE
  IN_VALIDATION_STATUS_CODE
  IN_REQUEST_XML
  IN_RESPONSE_XML
  IN_TIMESTAMP
Output:
   OUT_STATUS
   OUT_MESSAGE

-----------------------------------------------------------------------------*/
v_auth_response_id NUMBER;

BEGIN 
 BEGIN 
  select AUTHORIZATION_RESPOSE_ID_SEQ.nextval into v_auth_response_id from dual;

  INSERT INTO AUTHORIZATION_RESPONSE_STATS 
  (
   AUTHORIZATION_RESPONSE_ID,
   REFERENCE_NUMBER,
   CREDIT_CARD_TYPE,
   VALIDATION_STATUS_CODE,
   REQUEST_XML,
   RESPONSE_XML,
   TIMESTAMP

  )
  VALUES
  (
   v_auth_response_id,
   IN_REFERENCE_NUMBER,
   IN_CREDIT_CARD_TYPE,
   IN_VALIDATION_STATUS_CODE,
   IN_REQUEST_XML,
   IN_RESPONSE_XML,
   IN_TIMESTAMP
  );
  
  OUT_STATUS := 'Y';
  END;
  EXCEPTION WHEN OTHERS THEN
  BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256); 
  END;
        
END INSERT_AUTH_RESPONSE_STATS;


END DASHBOARD_STATS_PKG;
/



