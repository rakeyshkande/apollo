CREATE OR REPLACE PACKAGE BODY stats.oracle_dashboard_query_pkg AS

   FUNCTION get_stat_id (in_stat_name IN database_stats_val.stat_name%TYPE) RETURN NUMBER IS

      v_stat_id  NUMBER;

   BEGIN

      -- get the primary key for our stat
      SELECT stat_id INTO v_stat_id FROM database_stats_val WHERE stat_name = in_stat_name;

      RETURN v_stat_id;

   END get_stat_id;

   FUNCTION get_most_recent_frame (in_stat_id   IN NUMBER,
                                   in_inst_id   IN NUMBER DEFAULT NULL,
                                   in_end_id    IN NUMBER DEFAULT NULL) RETURN NUMBER IS
      v_frame_id NUMBER;

   BEGIN

      -- get the most recent frame for the specified stat.
      -- If in_inst_id is specified, get the most recent frame for the specified stat and instance.
      -- If in_end_id is specified, get the most recent frame for the specified stat (and instance, if appropriate)
      -- that is older than in_end_id.  This frame is intended to be a begin_id.

      IF in_inst_id IS NULL THEN

         IF in_end_id IS NULL THEN

            SELECT MAX(database_stats_master_id)
            INTO   v_frame_id
            FROM   database_stats 
            WHERE  stat_id = in_stat_id;

         ELSE

            SELECT MAX(database_stats_master_id)
            INTO   v_frame_id
            FROM   database_stats
            WHERE  stat_id = in_stat_id
            AND    database_stats_master_id < in_end_id;
      
         END IF;
   
      ELSE
 
         IF in_end_id IS NULL THEN

            SELECT MAX(database_stats_master_id)
            INTO   v_frame_id
            FROM   instance_stats 
            WHERE  stat_id = in_stat_id
            AND    instance_id = in_inst_id;

         ELSE

            SELECT MAX(database_stats_master_id)
            INTO   v_frame_id
            FROM   instance_stats
            WHERE  stat_id = in_stat_id
            AND    instance_id = in_inst_id
            AND    database_stats_master_id < in_end_id;

         END IF;

      END IF;

      RETURN v_frame_id;

   END get_most_recent_frame;


   FUNCTION get_time (in_frame_id IN NUMBER) RETURN DATE IS

      v_end_time DATE;

   BEGIN

      SELECT stat_timestamp INTO v_end_time FROM database_stats_master WHERE database_stats_master_id = in_frame_id;
      RETURN v_end_time;

   END get_time;


   FUNCTION get_stat_value (in_stat_id  IN NUMBER,
                            in_frame_id IN NUMBER,
                            in_inst_id  IN NUMBER DEFAULT NULL) RETURN NUMBER IS

      v_stat NUMBER;

   BEGIN

      -- Get the statistic value, for a particular instance if specified.
      IF in_inst_id IS NULL THEN

         SELECT stat_value
         INTO   v_stat
         FROM   database_stats
         WHERE  database_stats_master_id = in_frame_id
         AND    stat_id = in_stat_id;

      ELSE

         SELECT stat_value
         INTO   v_stat
         FROM   instance_stats
         WHERE  database_stats_master_id = in_frame_id
         AND    stat_id = in_stat_id
         AND    instance_id = in_inst_id;

      END IF;

      RETURN v_stat;

   END get_stat_value;

   FUNCTION get_raw_database_stat (in_stat_name IN database_stats_val.stat_name%TYPE) RETURN NUMBER IS

      v_stat_id  NUMBER;
      v_begin_id NUMBER;

   BEGIN

      -- get the primary key for our stat
      v_stat_id := get_stat_id(in_stat_name);

      -- get the most recent statistics frame for this stat
      v_begin_id := get_most_recent_frame (in_stat_id => v_stat_id);

      -- get the value for this frame
      RETURN get_stat_value (in_stat_id => v_stat_id, in_frame_id => v_begin_id);

   END get_raw_database_stat;

   FUNCTION get_raw_instance_stat (in_stat_name IN database_stats_val.stat_name%TYPE,
                                   in_inst_id IN NUMBER) RETURN NUMBER IS
      v_stat_id  NUMBER;
      v_begin_id NUMBER;

   BEGIN

      -- get the primary key for our stat
      v_stat_id := get_stat_id(in_stat_name);

      -- get the most recent statistics frame for this stat and instance
      v_begin_id := get_most_recent_frame (in_stat_id => v_stat_id, in_inst_id => in_inst_id);

      -- get the stat value for this instance and frame
      RETURN get_stat_value (in_stat_id => v_stat_id, in_frame_id => v_begin_id, in_inst_id => in_inst_id);

   END get_raw_instance_stat;

   FUNCTION get_raw_instance_stat_delta (in_stat_name IN database_stats_val.stat_name%TYPE,
                                         in_inst_id IN NUMBER) RETURN NUMBER IS
      v_stat_id    NUMBER;
      v_begin_id   NUMBER;
      v_begin_stat NUMBER;
      v_end_id     NUMBER;
      v_end_stat   NUMBER;

   BEGIN

      -- get the primary key for our stat
      v_stat_id := get_stat_id(in_stat_name);

      -- get the most recent statistics frame for this stat and instance
      v_end_id := get_most_recent_frame (in_stat_id => v_stat_id, in_inst_id => in_inst_id);

      -- get the next most recent frame
      v_begin_id := get_most_recent_frame (in_stat_id => v_stat_id, in_inst_id => in_inst_id, in_end_id => v_end_id);

      -- get the stat value for end frame
      v_end_stat := get_stat_value (in_stat_id => v_stat_id, in_frame_id => v_end_id, in_inst_id => in_inst_id);

      -- get stat value for begin frame
      v_begin_stat := get_stat_value (in_stat_id => v_stat_id, in_frame_id => v_begin_id, in_inst_id => in_inst_id);

      RETURN v_end_stat - v_begin_stat;

   END get_raw_instance_stat_delta;

   FUNCTION get_instance_stat_rate (in_stat_name IN database_stats_val.stat_name%TYPE,
                                    in_inst_id IN NUMBER) RETURN NUMBER IS

      v_stat_id    NUMBER;
      v_begin_id   NUMBER;
      v_end_id     NUMBER;
      v_begin_stat NUMBER;
      v_end_stat   NUMBER;
      v_begin_time DATE;
      v_end_time   DATE;

   BEGIN

      -- get the primary key for our stat
      v_stat_id := get_stat_id(in_stat_name);

      -- get the most recent statistics frame for this stat and instance
      v_end_id := get_most_recent_frame (in_stat_id => v_stat_id, in_inst_id => in_inst_id);

      -- get the exact time of that frame
      v_end_time := get_time(v_end_id);

      -- get the next most recent frame
      v_begin_id := get_most_recent_frame (in_stat_id => v_stat_id, in_inst_id => in_inst_id, in_end_id => v_end_id);

      -- get the exact time of that frame
      v_begin_time := get_time(v_begin_id);

      -- get the stat value for begin frame
      v_begin_stat := get_stat_value (in_stat_id => v_stat_id, in_frame_id => v_begin_id, in_inst_id => in_inst_id);

      -- get the stat value for end frame
      v_end_stat := get_stat_value (in_stat_id => v_stat_id, in_frame_id => v_end_id, in_inst_id => in_inst_id);

      -- The difference in the date values is a real number of days.  Multiply by 86,400 to convert to seconds
      RETURN (v_end_stat-v_begin_stat)/((v_end_time-v_begin_time)*86400);

   END get_instance_stat_rate;

   FUNCTION get_cache_efficiencies (in_inst_id IN NUMBER) RETURN cache_eff_rowset PIPELINED IS

      cache_eff_rec cache_eff_typ;

      v_physical_reads             NUMBER;
      v_consistent_gets            NUMBER;
      v_db_block_gets              NUMBER;
      v_gc_cr_blocks_received      NUMBER;
      v_gc_curr_blocks_received    NUMBER;
      v_global_cache_reads         NUMBER;
      v_logical_reads              NUMBER;

   BEGIN

      -- Deltas are the actual stat values over the interval
      v_physical_reads          := get_raw_instance_stat_delta('physical reads',            in_inst_id);
      v_consistent_gets         := get_raw_instance_stat_delta('consistent gets',           in_inst_id);
      v_db_block_gets           := get_raw_instance_stat_delta('db block gets',             in_inst_id);
      v_gc_cr_blocks_received   := get_raw_instance_stat_delta('gc cr blocks received',     in_inst_id);
      v_gc_curr_blocks_received := get_raw_instance_stat_delta('gc current blocks received',in_inst_id);

      v_global_cache_reads := v_gc_cr_blocks_received + v_gc_curr_blocks_received;

      v_logical_reads := v_consistent_gets + v_db_block_gets;

      -- global cache reads + logical reads + physical reads should total 100%
      cache_eff_rec.inst_id := in_inst_id;
      cache_eff_rec.global_cache_pct := (v_global_cache_reads/v_logical_reads)*100;
      cache_eff_rec.local_cache_pct := ((v_logical_reads - v_global_cache_reads - v_physical_reads)/v_logical_reads)*100;
      cache_eff_rec.noncache_pct := (v_physical_reads/v_logical_reads)*100;
      cache_eff_rec.phys_reads_per_sec := get_instance_stat_rate('physical reads',in_inst_id);
      cache_eff_rec.logi_reads_per_sec := get_instance_stat_rate('consistent gets',in_inst_id)
                                        + get_instance_stat_rate('db block gets',in_inst_id);

      PIPE ROW (cache_eff_rec);

      RETURN;

   END get_cache_efficiencies;

   FUNCTION get_transaction_rate (in_inst_id IN NUMBER) RETURN NUMBER IS
   BEGIN

      RETURN get_instance_stat_rate('user commits',in_inst_id)+get_instance_stat_rate('user rollbacks',in_inst_id);

   END get_transaction_rate;

   PROCEDURE get_all_stats (out_database_cur     OUT types.ref_cursor,
                            out_sessions_cur     OUT types.ref_cursor,
                            out_gcremaster_cur   OUT types.ref_cursor,
                            out_logfilesync_cur  OUT types.ref_cursor,
                            out_cache_eff_cur    OUT types.ref_cursor,
                            out_transactions_cur OUT types.ref_cursor,
                            out_usercalls_cur    OUT types.ref_cursor,
                            out_memory_cur       OUT types.ref_cursor,
                            out_loadavg_cur      OUT types.ref_cursor) IS


   BEGIN

      OPEN out_database_cur FOR
      SELECT get_raw_database_stat('lock count') lock_count,
             get_raw_database_stat('old lock count') old_lock_count,
             'ZEUS_DATA_VK' data_diskgroup_name,
             get_raw_database_stat('free megabytes in ZEUS_DATA_VK') data_diskgroup_free_mbytes,
             'ZEUS_FLASH' flash_diskgroup_name,
             get_raw_database_stat('free megabytes in ZEUS_FLASH') flash_diskgroup_free_mbytes,
             get_raw_database_stat('tablespace percent free') smallest_tablespace_pct_free
      FROM   dual;

      OPEN out_sessions_cur FOR
      SELECT host_name, get_raw_instance_stat('sessions per instance',inst_id) sessions
      FROM   gv$instance
      ORDER BY inst_id;

      OPEN out_gcremaster_cur FOR
      SELECT host_name, get_instance_stat_rate('gc remaster wait events',inst_id) gc_remasters_per_sec
      FROM   gv$instance
      ORDER BY inst_id;

      OPEN out_logfilesync_cur FOR
      SELECT host_name,
            (decode(get_raw_instance_stat_delta('log file sync wait events',inst_id),0,0,
            (get_raw_instance_stat_delta('log file sync time waited',inst_id)/get_raw_instance_stat_delta('log file sync wait events',inst_id))))*10 log_file_sync_avg_wait_msec
      FROM   gv$instance
      ORDER BY inst_id;

      OPEN out_cache_eff_cur FOR
      SELECT a.host_name, b.global_cache_pct, b.local_cache_pct, b.noncache_pct, b.phys_reads_per_sec, b.logi_reads_per_sec
      FROM   gv$instance a, table(get_cache_efficiencies(a.inst_id)) b
      ORDER BY a.inst_id;

      OPEN out_transactions_cur FOR
      SELECT host_name, get_transaction_rate(inst_id) transactions_per_sec FROM gv$instance ORDER BY inst_id;

      OPEN out_usercalls_cur FOR
      SELECT host_name, get_instance_stat_rate('user calls',inst_id) user_calls_per_sec
      FROM   gv$instance
      ORDER BY inst_id;

      OPEN out_memory_cur FOR
      SELECT host_name, get_raw_instance_stat('total memory used',inst_id)/1024/1024 memory_mbytes
      FROM   gv$instance
      ORDER BY inst_id;

      OPEN out_loadavg_cur FOR
      SELECT host_name, get_raw_instance_stat('load average',inst_id) load_factor
      FROM   gv$instance
      ORDER BY inst_id;

   END get_all_stats;


   PROCEDURE get_all_time_series (in_begin_time       IN  DATE,
                                  in_end_time         IN  DATE,
                                  out_locks_cur       OUT types.ref_cursor,
                                  out_aged_locks_cur  OUT types.ref_cursor,
                                  out_sessions_cur    OUT types.ref_cursor) IS

      v_stat_id NUMBER;

   BEGIN

      v_stat_id := get_stat_id('lock count');
      OPEN out_locks_cur FOR
      SELECT dsm.stat_timestamp, ds.stat_value lock_count
      FROM   database_stats_master dsm
      JOIN   database_stats ds ON dsm.database_stats_master_id = ds.database_stats_master_id
      WHERE  ds.stat_id = v_stat_id
      AND    dsm.stat_timestamp BETWEEN in_begin_time AND in_end_time
      ORDER BY 1;

      v_stat_id := get_stat_id('old lock count');
      OPEN out_aged_locks_cur FOR
      SELECT dsm.stat_timestamp, ds.stat_value old_lock_count
      FROM   database_stats_master dsm
      JOIN   database_stats ds ON dsm.database_stats_master_id = ds.database_stats_master_id
      WHERE  ds.stat_id = v_stat_id
      AND    dsm.stat_timestamp BETWEEN in_begin_time AND in_end_time
      ORDER BY 1;

      v_stat_id := get_stat_id('sessions per instance');
      OPEN out_sessions_cur FOR
      SELECT dsm.stat_timestamp, a.host_name, ds.stat_value sessions
      FROM   database_stats_master dsm
      JOIN   instance_stats ds ON dsm.database_stats_master_id = ds.database_stats_master_id
      JOIN   gv$instance a on ds.instance_id = a.inst_id
      WHERE  ds.stat_id = v_stat_id
      AND    dsm.stat_timestamp BETWEEN in_begin_time AND in_end_time
      ORDER BY dsm.stat_timestamp, ds.instance_id;

   END get_all_time_series;

END oracle_dashboard_query_pkg;
/
