CREATE OR REPLACE PACKAGE BODY STATS.DASHBOARD_QUERY_PKG
AS

PROCEDURE GET_CLEAN_HOURLY_STATS
(
IN_MAX_SECONDS_BACK             IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the rows from 
	clean_hourly_stats that match the time criteria.

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
    SELECT TO_TIMESTAMP(TO_CHAR(clean_date, 'DD-MON-YYYY HH24:MI:SS'),'DD-MON-YYYY HH24:MI:SS') AS clean_date,
           orders_taken,
           goto_florist_count,  
           goto_florist_suspend,  
           goto_florist_shutdown,  
           mercury_shop_count,  
           mercury_shop_shutdown,  
           covered_zip_count,  
           gnadd_zip_count,
	   reject_queue_end,
	   ask_queue_end,
	   zip_queue_end
    FROM rpt.clean_hourly_stats chs
    WHERE 1=1
      AND chs.clean_date > (sysdate - IN_MAX_SECONDS_BACK / 86400);
        
END GET_CLEAN_HOURLY_STATS;

PROCEDURE GET_FLORIST_STATS_SUMMARY
(
IN_MAX_SECONDS_BACK             IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        florist stats 

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
    SELECT stat_timestamp,
           sum(goto_florist_count) as goto_florist_count,  
           sum(goto_florist_suspend_count) as goto_florist_suspend_count,  
           sum(goto_florist_shutdown_count) as goto_florist_shutdown_count,  
           sum(mercury_shop_count) as mercury_shop_count,  
           sum(mercury_shop_shutdown_count) as mercury_shop_shutdown_count
    FROM florist_stats_vw fs
    WHERE 1=1
      AND fs.stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
    group by stat_timestamp;
        
END GET_FLORIST_STATS_SUMMARY;

PROCEDURE GET_FLORIST_STATS_DETAIL
(
IN_HOURS_BACK         IN NUMBER,
OUT_CUR               OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        florist stats 

Input:
        IN_HOURS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
v_timestamp zip_stats.stat_timestamp%type;

BEGIN
	BEGIN
	IF IN_HOURS_BACK = 0 THEN
		 select max(stat_timestamp) into v_timestamp from florist_stats_vw where STAT_TIMESTAMP  between (sysdate-1/24) and sysdate;
  	 
  	ELSIF IN_HOURS_BACK = 1 THEN 
  		select max(stat_timestamp) into v_timestamp from florist_stats_vw where STAT_TIMESTAMP between (sysdate-2/24) and (sysdate-1/24);
      
  	ELSE
  		select max(stat_timestamp) into v_timestamp from florist_stats_vw where STAT_TIMESTAMP between (sysdate - (IN_HOURS_BACK+1)/24) AND (sysdate - (IN_HOURS_BACK)/24) ;
      
  	END IF;
  	
	  	EXCEPTION WHEN OTHERS THEN v_timestamp := SYSDATE+1;
  	 END;

  		
  OPEN out_cur FOR
    SELECT fs.stat_timestamp, 
           trx.region, 
           sum(fs.goto_florist_count) as goto_florist_count,  
           sum(fs.goto_florist_suspend_count) as goto_florist_suspend_count,  
           sum(fs.goto_florist_shutdown_count) as goto_florist_shutdown_count,  
           sum(fs.mercury_shop_count) as mercury_shop_count,  
           sum(fs.mercury_shop_shutdown_count) as mercury_shop_shutdown_count
    FROM florist_stats_vw fs,
         territory_region_xref trx
    WHERE 1=1
      and fs.territory = trx.territory
      and fs.stat_timestamp = v_timestamp
    group by fs.stat_timestamp, trx.region;
        
END GET_FLORIST_STATS_DETAIL;

PROCEDURE GET_QUEUE_STATS_SUMMARY
(
IN_MAX_SECONDS_BACK             IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the queue counts data

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
BEGIN


  OPEN out_cur FOR
    select qse.stat_timestamp, qsw.work_queue_count, qse.email_queue_count, qso.other_queue_count, qst.total_queue_count
    from  (SELECT stat_timestamp, sum(queue_count) work_queue_count
           FROM   queue_stats
           WHERE  stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
           and    queue_type in ('ADJ', 'REJ', 'FTD', 'ASK', 'ANS', 'CON', 'DEN', 'CAN', 'GEN', 'ZIP', 'CREDIT', 'ORDER',
                                 'LP')
           group by stat_timestamp) qsw,
          (SELECT stat_timestamp, sum(queue_count) email_queue_count
           FROM   queue_stats
           WHERE  stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
           and    queue_type in ('OA', 'CX', 'MO','ND','QI','PD','BD','DI','QC','OT','SE')
           group by stat_timestamp) qse,
          (SELECT stat_timestamp, sum(queue_count) other_queue_count
           FROM   queue_stats
           WHERE  stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
           and    queue_type in ('OTHER')
           group by stat_timestamp) qso,
          (SELECT stat_timestamp, sum(queue_count) total_queue_count
           FROM   queue_stats
           WHERE  stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
           group by stat_timestamp) qst
    WHERE  1=1
    and    qse.stat_timestamp = qsw.stat_timestamp
    and    qse.stat_timestamp = qso.stat_timestamp
    and    qse.stat_timestamp = qst.stat_timestamp;

        
END GET_QUEUE_STATS_SUMMARY;

PROCEDURE GET_QUEUE_STATS_DETAIL
(
IN_MAX_SECONDS_BACK             IN NUMBER,
IN_QUEUE_LIST                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the queue counts data

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);

BEGIN

    v_main_sql :=
        'SELECT stat_timestamp, queue_type, sum(queue_count) queue_count
           FROM   queue_stats
           WHERE  stat_timestamp > (sysdate - ';
    v_main_sql := v_main_sql || IN_MAX_SECONDS_BACK  || ' / 86400) AND  queue_type in ( ';

    v_main_sql := v_main_sql || IN_QUEUE_LIST;

    v_main_sql := v_main_sql || ' )';

    v_main_sql := v_main_sql || ' group by stat_timestamp, queue_type';

  OPEN out_cur FOR v_main_sql;
        
END GET_QUEUE_STATS_DETAIL;

PROCEDURE GET_ZIP_STATS
(
IN_MAX_SECONDS_BACK             IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the zip code data

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
    SELECT stat_timestamp,
           covered_zip_count,
           gnadd_zip_count  
    FROM zip_stats zs
    WHERE 1=1
      AND zs.stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400);
        
END GET_ZIP_STATS;

PROCEDURE GET_ORDER_STATS_SUMMARY
(
IN_MAX_SECONDS_BACK             IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        florist stats 

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
    SELECT stat_timestamp,
           total_order_count,
	   florist_order_count,
	   vendor_order_count, 
	   internet_order_count,
	   phone_order_count,
           forecast_order_count
    FROM order_stats os
    WHERE 1=1
      AND os.stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400);


END GET_ORDER_STATS_SUMMARY;


PROCEDURE GET_ORDER_STATS_CUMULATIVE
(
IN_MAX_SECONDS_BACK             IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        order stats.  The stats are represented as the counts for a rolling
        hour time period.

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of rows 

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
    SELECT os_hdr.stat_timestamp, 
           sum(os_dtl.total_order_count) as total_order_count,
	   sum(os_dtl.florist_order_count) as florist_order_count,
	   sum(os_dtl.vendor_order_count) as vendor_order_count, 
	   sum(os_dtl.internet_order_count) as internet_order_count,
	   sum(os_dtl.phone_order_count) as phone_order_count,
           sum(os_dtl.forecast_order_count) as forecast_order_count
    FROM stats.order_stats os_hdr,
         stats.order_stats os_dtl
    WHERE 1=1
      AND os_hdr.stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
      AND os_dtl.stat_timestamp >= os_hdr.stat_timestamp  - 59/1440
      AND os_dtl.stat_timestamp <= os_hdr.stat_timestamp
      group by os_hdr.stat_timestamp;
      
        
END GET_ORDER_STATS_CUMULATIVE;


PROCEDURE GET_PRODUCT_STATS_DETAIL
(
IN_MAX_SECONDS_BACK             IN NUMBER,
IN_MAX_PRODUCTS                 IN NUMBER,
OUT_FLORIST_CUR                 OUT TYPES.REF_CURSOR,
OUT_VENDOR_CUR                  OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        product stats.  Only the stats for the top ten products for the time
        period are returned.

Input:
        IN_MAX_SECONDS_BACK  Max seconds back to retrieve data

Output:
        List of florist rows 
        List of vendor rows 

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_florist_cur FOR
      select stat_timestamp, product_id, order_count
      from product_stats
      where delivery_type  = 'Florist' 
        AND stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
        AND product_id in 
            (select product_id from (
            select delivery_type,product_id, sum(order_count) total_order_count
            from product_stats ps
            where 1=1
            --      AND ps.stat_timestamp > (sysdate - 86400 / 86400)
                  AND ps.stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
                  AND ps.delivery_type = 'Florist'
            group by delivery_type,product_id
            order by total_order_count desc) top_products
            where rownum <= IN_MAX_PRODUCTS )
      order by stat_timestamp;

  OPEN out_vendor_cur FOR
      select stat_timestamp, product_id, order_count
      from product_stats
      where delivery_type  = 'Vendor' 
        AND stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
        AND product_id in 
            (select product_id from (
            select delivery_type,product_id, sum(order_count) total_order_count
            from product_stats ps
            where 1=1
            --      AND ps.stat_timestamp > (sysdate - 86400 / 86400)
                  AND ps.stat_timestamp > (sysdate - IN_MAX_SECONDS_BACK / 86400)
                  AND ps.delivery_type = 'Vendor'
            group by delivery_type,product_id
            order by total_order_count desc) top_products
            where rownum <= IN_MAX_PRODUCTS )
      order by stat_timestamp;
        
END GET_PRODUCT_STATS_DETAIL;

PROCEDURE GET_PAYMENT_STATS_SUMMARY
(
IN_START_TIMESTAMP              IN TIMESTAMP,
IN_STOP_TIMESTAMP               IN TIMESTAMP,
OUT_PAYMENT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        payment stats.  All of the payment stats for the given period of time 
        are summarized by payment type.

Input:
        IN_START_TIMESTAMP  Time to start with
        IN_STOP_TIMESTAMP   Time to stop at

Output:
        List of payment rows

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_payment_cur FOR
      select payment_type, sum(payment_count) payment_count
      from stats.payment_stats
      where 1=1
        and stat_timestamp > IN_START_TIMESTAMP
        and stat_timestamp <= IN_STOP_TIMESTAMP
      group by payment_type;
  
END GET_PAYMENT_STATS_SUMMARY;

PROCEDURE GET_AUTH_STATS_SUMMARY
(
IN_START_TIMESTAMP              IN TIMESTAMP,
IN_STOP_TIMESTAMP               IN TIMESTAMP,
OUT_AUTH_CUR                    OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        authorization stats.  All of the authorization stats for the given period 
        are summarized by card type.

Input:
        IN_START_TIMESTAMP  Time to start with
        IN_STOP_TIMESTAMP   Time to stop at

Output:
        List of authorization rows

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_auth_cur FOR
      select credit_card_type, sum(approval_count) approval_count, sum(decline_count) decline_count
      from stats.authorization_stats
      where 1=1
        and stat_timestamp > IN_START_TIMESTAMP
        and stat_timestamp <= IN_STOP_TIMESTAMP
      group by credit_card_type;
  
END GET_AUTH_STATS_SUMMARY;

PROCEDURE GET_OS_STATS_SUMMARY
(
IN_START_TIMESTAMP              IN TIMESTAMP,
IN_STOP_TIMESTAMP               IN TIMESTAMP,
IN_GROUP_BY                     IN VARCHAR2,
IN_FILTER_ON                    IN VARCHAR2,
OUT_OS_CUR                      OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all the data that contains
        order service stats.

Input:
        IN_START_TIMESTAMP  Time to start with
        IN_STOP_TIMESTAMP   Time to stop at

Output:
        List of orderservice rows

-----------------------------------------------------------------------------*/
v_main_sql VARCHAR2(3000);
BEGIN
    
    v_main_sql :=
      'select 
             sum(stat_count) stat_count, 
             stat_timestamp, ';
    v_main_sql := v_main_sql || IN_GROUP_BY;

    v_main_sql := v_main_sql || 

      ' from STATS.order_service_stats
      where 1=1
        and stat_timestamp > ''' || IN_START_TIMESTAMP || ''' ' ||
        'and stat_timestamp <= ''' || IN_STOP_TIMESTAMP || ''' ';

    v_main_sql := v_main_sql || IN_FILTER_ON;

    v_main_sql := v_main_sql || ' group by stat_timestamp, ' || IN_GROUP_BY;

  OPEN out_os_cur FOR v_main_sql;
  
END GET_OS_STATS_SUMMARY;

END DASHBOARD_QUERY_PKG;
/
