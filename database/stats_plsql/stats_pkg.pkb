CREATE OR REPLACE PACKAGE BODY STATS.STATS_PKG AS

PROCEDURE INSERT_SERVICE_RESPONSE
(
   IN_SERVICE_NAME     IN  SERVICE_RESPONSE_TRACKING.SERVICE_NAME%TYPE,
   IN_SERVICE_METHOD   IN  SERVICE_RESPONSE_TRACKING.SERVICE_METHOD%TYPE,
   IN_TRANSACTION_ID   IN  SERVICE_RESPONSE_TRACKING.TRANSACTION_ID%TYPE,
   IN_RESPONSE_TIME    IN  SERVICE_RESPONSE_TRACKING.RESPONSE_TIME%TYPE,
   IN_CREATED_ON       IN  SERVICE_RESPONSE_TRACKING.CREATED_ON%TYPE,
   OUT_STATUS          OUT VARCHAR2,
   OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will insert records into the service_response_tracking table,
        which is used to track response times for web service calls.

-----------------------------------------------------------------------------*/

v_service_response_id SERVICE_RESPONSE_TRACKING.SERVICE_RESPONSE_TRACKING_ID%type;

BEGIN

   SELECT STATS.SERVICE_RESPONSE_TRACKING_SQ.NEXTVAL INTO v_service_response_id from dual;

   INSERT INTO STATS.SERVICE_RESPONSE_TRACKING
   (
        SERVICE_RESPONSE_TRACKING_ID,
        SERVICE_NAME,
        SERVICE_METHOD,
        TRANSACTION_ID,
        RESPONSE_TIME,
        CREATED_ON
   )
   VALUES
   (
        v_service_response_id,
        IN_SERVICE_NAME,
        IN_SERVICE_METHOD,
        IN_TRANSACTION_ID,
        IN_RESPONSE_TIME,
        IN_CREATED_ON
   );

   OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
   BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;    -- end exception block

END INSERT_SERVICE_RESPONSE;


PROCEDURE INSERT_SERVICE_RESPONSE_REQ
(
   IN_SERVICE_NAME     IN  SERVICE_RESPONSE_TRACKING.SERVICE_NAME%TYPE,
   IN_SERVICE_METHOD   IN  SERVICE_RESPONSE_TRACKING.SERVICE_METHOD%TYPE,
   IN_TRANSACTION_ID   IN  SERVICE_RESPONSE_TRACKING.TRANSACTION_ID%TYPE,
   IN_RESPONSE_TIME    IN  SERVICE_RESPONSE_TRACKING.RESPONSE_TIME%TYPE,
   IN_CREATED_ON       IN  SERVICE_RESPONSE_TRACKING.CREATED_ON%TYPE,
   IN_REQUEST		   IN  SERVICE_RESPONSE_TRACKING.REQUEST%TYPE,
   IN_RESPONSE	       IN  SERVICE_RESPONSE_TRACKING.RESPONSE%TYPE,
   OUT_STATUS          OUT VARCHAR2,
   OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will insert records into the service_response_tracking table,
        which is used to track response times for web service calls.  It will
        also save the request and response to/from the web service call.

-----------------------------------------------------------------------------*/

v_service_response_id SERVICE_RESPONSE_TRACKING.SERVICE_RESPONSE_TRACKING_ID%type;

BEGIN

   SELECT STATS.SERVICE_RESPONSE_TRACKING_SQ.NEXTVAL INTO v_service_response_id from dual;

   INSERT INTO STATS.SERVICE_RESPONSE_TRACKING
   (
        SERVICE_RESPONSE_TRACKING_ID,
        SERVICE_NAME,
        SERVICE_METHOD,
        TRANSACTION_ID,
        RESPONSE_TIME,
        CREATED_ON,
        REQUEST,
        RESPONSE
   )
   VALUES
   (
        v_service_response_id,
        IN_SERVICE_NAME,
        IN_SERVICE_METHOD,
        IN_TRANSACTION_ID,
        IN_RESPONSE_TIME,
        IN_CREATED_ON,
        IN_REQUEST,
        IN_RESPONSE
   );

   OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
   BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;    -- end exception block

END INSERT_SERVICE_RESPONSE_REQ;

END STATS_PKG;
/
