CREATE OR REPLACE PACKAGE BODY stats.oracle_dashboard_stats_pkg AS

   PROCEDURE populate_stats (in_terval_sec IN NUMBER) IS

      v_master_id      NUMBER;
      v_stats_result   NUMBER;
      v_stat_timestamp DATE;

      -- structure for storing stats for an arbitrary number of instances in a cluster
      TYPE inst_stat_typ IS RECORD (inst_id NUMBER, stats_result NUMBER);
      TYPE inst_stat IS TABLE OF inst_stat_typ;
      inst_stat_tab inst_stat;

      CURSOR stats_c IS
         SELECT stat_id, gathering_command, presentation_level
         FROM   database_stats_val
         WHERE  gather_interval = in_terval_sec
         AND    manual_flag = 'N';
      stats_r stats_c%ROWTYPE;

   BEGIN

      -- database_stats_master stores the timestamp for this stats interval
      -- common between database stats and instance stats
      SELECT database_stats_master_sq.NEXTVAL, SYSDATE INTO v_master_id, v_stat_timestamp FROM DUAL;
      INSERT INTO database_stats_master(database_stats_master_id, stat_timestamp) VALUES (v_master_id, v_stat_timestamp);

      -- Look for all automatic stats in the specified interval
      -- Each automatic stat should have its own gathering command
      FOR stats_r IN stats_c LOOP

         IF stats_r.presentation_level = 'Database' THEN
            EXECUTE IMMEDIATE stats_r.gathering_command INTO v_stats_result;
            INSERT INTO database_stats (database_stats_master_id, stat_id, stat_value)
            VALUES (v_master_id, stats_r.stat_id, v_stats_result);
         END IF;

         IF stats_r.presentation_level = 'Instance' THEN
            EXECUTE IMMEDIATE stats_r.gathering_command BULK COLLECT INTO inst_stat_tab;
            FOR i IN inst_stat_tab.first..inst_stat_tab.last LOOP
               INSERT INTO instance_stats (database_stats_master_id, instance_id, stat_id, stat_value)
               VALUES (v_master_id, inst_stat_tab(i).inst_id, stats_r.stat_id, inst_stat_tab(i).stats_result);
            END LOOP;
         END IF;

      END LOOP;

      COMMIT;

   END populate_stats;

   PROCEDURE populate_manual_stat (in_hostname IN VARCHAR2,
                                   in_stat_name IN database_stats_val.stat_name%TYPE,
                                   in_stat_value IN NUMBER) IS

      v_stat_id            number;
      v_presentation_level database_stats_val.presentation_level%TYPE;
      v_master_id          NUMBER;
      v_inst_id            NUMBER;
      v_stat_timestamp     DATE;

   BEGIN

      -- database_stats_master stores the timestamp for this stats interval
      -- common between database stats and instance stats
      SELECT database_stats_master_sq.NEXTVAL, SYSDATE INTO v_master_id, v_stat_timestamp FROM DUAL;
      INSERT INTO database_stats_master(database_stats_master_id, stat_timestamp) VALUES (v_master_id, v_stat_timestamp);

      SELECT stat_id, presentation_level
      INTO   v_stat_id, v_presentation_level
      FROM   database_stats_val
      WHERE  stat_name = in_stat_name;

      IF v_presentation_level = 'Instance' then
         SELECT inst_id INTO v_inst_id FROM gv$instance where host_name = in_hostname;
         INSERT INTO instance_stats (database_stats_master_id, instance_id, stat_id, stat_value)
         VALUES (v_master_id, v_inst_id, v_stat_id, in_stat_value);
      END IF;

   END populate_manual_stat;

END oracle_dashboard_stats_pkg;
/
