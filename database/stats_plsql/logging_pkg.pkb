CREATE OR REPLACE PACKAGE BODY STATS.LOGGING_PKG AS

PROCEDURE LOG_FLORIST_SELECTION
(
   IN_ORDER_DETAIL_ID  IN  FLORIST_SELECTION_LOG.ORDER_DETAIL_ID%TYPE,
   IN_FLORIST_LIST     IN  FLORIST_SELECTION_LOG.FLORIST_LIST%TYPE,
   IN_SELECTED_FLORIST IN  FLORIST_SELECTION_LOG.SELECTED_FLORIST%TYPE,
   IN_SELECTOR         IN  FLORIST_SELECTION_LOG.SELECTOR%TYPE,
   IN_OSCAR_REQ_ID     IN  FLORIST_SELECTION_LOG.OSCAR_REQUEST_ID%TYPE,
   IN_OSCAR_REASON     IN  FLORIST_SELECTION_LOG.OSCAR_REASON%TYPE,
   OUT_LOG_ID          OUT NUMBER,
   OUT_STATUS          OUT VARCHAR2,
   OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will insert records into the florist_selection_log table,
        which is used to track each florist selection request and who selected
        it (either Apollo lottery or Oscar)

-----------------------------------------------------------------------------*/

v_florist_sel_log_id STATS.FLORIST_SELECTION_LOG.FLORIST_SELECTION_LOG_ID%type;

BEGIN

   SELECT STATS.FLORIST_SELECTION_LOG_SQ.NEXTVAL INTO v_florist_sel_log_id from dual;

   INSERT INTO STATS.FLORIST_SELECTION_LOG
   (
        FLORIST_SELECTION_LOG_ID,
        ORDER_DETAIL_ID,
        FLORIST_LIST,
        SELECTED_FLORIST,
        SELECTOR,
        OSCAR_REQUEST_ID,
        OSCAR_REASON,
        CREATED_ON
   )
   VALUES
   (
        v_florist_sel_log_id,
        IN_ORDER_DETAIL_ID,
        IN_FLORIST_LIST,
        IN_SELECTED_FLORIST,
        IN_SELECTOR,
        IN_OSCAR_REQ_ID,
        IN_OSCAR_REASON,
        SYSDATE
   );

   OUT_STATUS := 'Y';
   OUT_LOG_ID := v_florist_sel_log_id;

EXCEPTION WHEN OTHERS THEN
   BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;    -- end exception block

END LOG_FLORIST_SELECTION;


END LOGGING_PKG;
/
