create or replace PACKAGE BODY STATS.DASHBOARD_TABLE_STATS_PKG
AS

PROCEDURE GET_FLORIST_TABLE_STATS
(
OUT_CURRENT_HOUR_STATS          OUT TYPES.REF_CURSOR,
OUT_PREVIOUS_HOUR_STATS         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for current and previous hour florist stats

Output:
        The current and previous hour stats in the form of cursors are returned 

-----------------------------------------------------------------------------*/

BEGIN
 
     OPEN OUT_CURRENT_HOUR_STATS FOR
      SELECT T1.*, 'CURRENT' v_hour, COVERED_ZIP_COUNT, GNADD_ZIP_COUNT
      from (
          SELECT stat_timestamp,
             sum(goto_florist_count) as goto_florist_count,  
             sum(goto_florist_shutdown_count) as goto_florist_shutdown_count,
             sum(mercury_shop_count) as mercury_shop_count,  
             sum(mercury_shop_shutdown_count) as mercury_shop_shutdown_count
      FROM FLORIST_STATS_VW
      WHERE STAT_TIMESTAMP BETWEEN (sysdate-1/24) AND SYSDATE
      GROUP BY stat_timestamp ORDER BY stat_timestamp DESC ) T1
      JOIN ZIP_STATS ZS ON 
      T1.stat_timestamp = ZS.stat_timestamp
      where ROWNUM = 1;
  
     OPEN OUT_PREVIOUS_HOUR_STATS FOR
     SELECT T2.*, 'PREVIOUS' v_hour, COVERED_ZIP_COUNT, GNADD_ZIP_COUNT
      from (
          SELECT stat_timestamp,
             sum(goto_florist_count) as goto_florist_count,
             sum(goto_florist_shutdown_count) as goto_florist_shutdown_count,  
             sum(mercury_shop_count) as mercury_shop_count,  
             sum(mercury_shop_shutdown_count) as mercury_shop_shutdown_count
      FROM FLORIST_STATS_VW
      WHERE  STAT_TIMESTAMP BETWEEN (sysdate-2/24) AND (SYSDATE -1/24)
      GROUP BY stat_timestamp ORDER BY stat_timestamp DESC ) T2
      LEFT JOIN ZIP_STATS ZS ON 
      T2.stat_timestamp = ZS.stat_timestamp
      where ROWNUM = 1;
 
END GET_FLORIST_TABLE_STATS;

PROCEDURE GET_ORDER_FLOW_TABLE_STATS
(
OUT_CURRENT_HOUR_STATS          OUT TYPES.REF_CURSOR,
OUT_PREVIOUS_HOUR_STATS         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for current and previous hour order stats

Output:
        The current and previous hour stats in the form of cursors are returned

-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURRENT_HOUR_STATS FOR
  	  SELECT T1.*,'CURRENT' AS v_hour 
  	  FROM (
			 SELECT os_hdr.stat_timestamp,
					SUM(os_dtl.internet_order_count) AS internet_order_count,
					SUM(os_dtl.phone_order_count) AS phone_order_count
			 FROM stats.order_stats os_hdr,
				  stats.order_stats os_dtl
			 WHERE 1=1
				AND os_hdr.stat_timestamp >= (sysdate - 1/24)
        		AND os_dtl.stat_timestamp >= os_hdr.stat_timestamp  - 59/1440
        		AND os_dtl.stat_timestamp <= os_hdr.stat_timestamp
			 GROUP BY os_hdr.stat_timestamp
			 ORDER BY os_hdr.stat_timestamp DESC
		   )T1
	  WHERE ROWNUM = 1;
              		  

   OPEN OUT_PREVIOUS_HOUR_STATS FOR
  	  SELECT T2.*, 'PREVIOUS' AS v_hour 
      FROM (
	         SELECT os_hdr.stat_timestamp,
					SUM(os_dtl.internet_order_count) AS internet_order_count,
					SUM(os_dtl.phone_order_count) AS phone_order_count
			 FROM stats.order_stats os_hdr,
				  stats.order_stats os_dtl
			 WHERE 1=1
				AND os_hdr.stat_timestamp >= (sysdate - 2/24)
       			AND os_hdr.stat_timestamp <= (sysdate - 1/24)
        		AND os_dtl.stat_timestamp >= os_hdr.stat_timestamp  - 59/1440
        		AND os_dtl.stat_timestamp <= os_hdr.stat_timestamp
        	 GROUP BY os_hdr.stat_timestamp
			 ORDER BY os_hdr.stat_timestamp DESC
		  )T2
	  WHERE ROWNUM =1;

END GET_ORDER_FLOW_TABLE_STATS;

PROCEDURE GET_QUEUE_TABLE_STATS
(
OUT_CURRENT_HOUR_STATS          OUT TYPES.REF_CURSOR,
OUT_PREVIOUS_HOUR_STATS         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for current and previous hour queue stats

Output:
        The current and previous hour stats in the form of cursors are returned

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CURRENT_HOUR_STATS FOR
SELECT * FROM 
 (SELECT qse.stat_timestamp,'CURRENT' AS v_hour, qsw.work_queue_count, qse.email_queue_count, qso.other_queue_count
      FROM 
            (SELECT stat_timestamp, SUM(queue_count) work_queue_count
             FROM   stats.queue_stats
             WHERE  stat_timestamp BETWEEN (sysdate - 1/24) AND sysdate
             AND    queue_type IN ('ADJ', 'REJ', 'FTD', 'ASK', 'ANS', 'CON', 'DEN', 'CAN', 'GEN', 'ZIP', 'CREDIT', 'ORDER', 'LP')
             GROUP BY stat_timestamp) qsw,
            
            (SELECT stat_timestamp, SUM(queue_count) email_queue_count
             FROM   stats.queue_stats
             WHERE  stat_timestamp  BETWEEN (sysdate - 1/24) AND sysdate
             AND    queue_type IN ('OA', 'CX', 'MO','ND','QI','PD','BD','DI','QC','OT','SE')
             GROUP BY stat_timestamp) qse,
            
            (SELECT stat_timestamp, SUM(queue_count) other_queue_count
             FROM   stats.queue_stats
             WHERE  stat_timestamp  BETWEEN (sysdate - 1/24) AND sysdate
             AND    queue_type IN ('OTHER')
             GROUP BY stat_timestamp) qso
      WHERE  1=1
      AND    qse.stat_timestamp = qsw.stat_timestamp
      AND    qse.stat_timestamp = qso.stat_timestamp
      ORDER BY qse.stat_timestamp desc
    )
    WHERE ROWNUM = 1;
    
      
OPEN OUT_PREVIOUS_HOUR_STATS FOR
  
 SELECT * FROM 
 (SELECT qse.stat_timestamp,'PREVIOUS' AS v_hour, qsw.work_queue_count, qse.email_queue_count, qso.other_queue_count
      FROM 
            (SELECT stat_timestamp, SUM(queue_count) work_queue_count
             FROM   stats.queue_stats
             WHERE  stat_timestamp BETWEEN (sysdate - 2/24) AND (sysdate - 1/24)
             AND    queue_type IN ('ADJ', 'REJ', 'FTD', 'ASK', 'ANS', 'CON', 'DEN', 'CAN', 'GEN', 'ZIP', 'CREDIT', 'ORDER', 'LP')
             GROUP BY stat_timestamp) qsw,
            
            (SELECT stat_timestamp, SUM(queue_count) email_queue_count
             FROM   stats.queue_stats
             WHERE  stat_timestamp  BETWEEN (sysdate - 2/24) AND (sysdate - 1/24)
             AND    queue_type IN ('OA', 'CX', 'MO','ND','QI','PD','BD','DI','QC','OT','SE')
             GROUP BY stat_timestamp) qse,
            
            (SELECT stat_timestamp, SUM(queue_count) other_queue_count
             FROM   stats.queue_stats
             WHERE  stat_timestamp  BETWEEN (sysdate - 2/24) AND (sysdate - 1/24)
             AND    queue_type IN ('OTHER')
             GROUP BY stat_timestamp) qso
      WHERE  1=1
      AND    qse.stat_timestamp = qsw.stat_timestamp
      AND    qse.stat_timestamp = qso.stat_timestamp
      ORDER BY qse.stat_timestamp desc
    )
    WHERE ROWNUM = 1;
      
END GET_QUEUE_TABLE_STATS;


PROCEDURE GET_INVENTORY_TABLE_STATS
(
OUT_CURRENT_HOUR_STATS          OUT TYPES.REF_CURSOR,
OUT_PREVIOUS_HOUR_STATS         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for current and previous hour inventory stats

Output:
        The current and previous hour stats in the form of cursors are returned

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CURRENT_HOUR_STATS FOR
	SELECT T1.*, 'CURRENT' AS v_hour
      FROM (
	         SELECT os_hdr.stat_timestamp,
					SUM(os_dtl.florist_order_count) AS florist_order_count,
					SUM(os_dtl.vendor_order_count) AS vendor_order_count
			 FROM stats.order_stats os_hdr,
				  stats.order_stats os_dtl
			 WHERE 1=1
				AND os_hdr.stat_timestamp >= (sysdate - 1/24)
       			AND os_hdr.stat_timestamp <= sysdate
        		AND os_dtl.stat_timestamp >= os_hdr.stat_timestamp  - 59/1440
        		AND os_dtl.stat_timestamp <= os_hdr.stat_timestamp
        	 GROUP BY os_hdr.stat_timestamp
			 ORDER BY os_hdr.stat_timestamp DESC
		  )T1
	  WHERE ROWNUM =1;
	
    
      
OPEN OUT_PREVIOUS_HOUR_STATS FOR
  
SELECT T2.*, 'PREVIOUS' AS v_hour
      FROM (
	         SELECT os_hdr.stat_timestamp,
					SUM(os_dtl.florist_order_count) AS florist_order_count,
					SUM(os_dtl.vendor_order_count) AS vendor_order_count
			 FROM stats.order_stats os_hdr,
				  stats.order_stats os_dtl
			 WHERE 1=1
				AND os_hdr.stat_timestamp >= (sysdate - 2/24)
       			AND os_hdr.stat_timestamp <= (sysdate - 1/24)
        		AND os_dtl.stat_timestamp >= os_hdr.stat_timestamp  - 59/1440
        		AND os_dtl.stat_timestamp <= os_hdr.stat_timestamp
        	 GROUP BY os_hdr.stat_timestamp
			 ORDER BY os_hdr.stat_timestamp DESC
		  )T2
	  WHERE ROWNUM =1;
 
END GET_INVENTORY_TABLE_STATS;


END DASHBOARD_TABLE_STATS_PKG;

/