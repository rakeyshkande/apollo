CREATE OR REPLACE
PROCEDURE rpt.acc05_orders_dlvy_date_pop_tab
	(p_company_code		in varchar2,
	 p_origin		in varchar2,
	 p_delivery_date	in varchar2,
	 p_month		in varchar2,
	 p_monthly_daily_ind	in varchar2
	)
IS
	v_company_predicate	varchar2(1000);
	v_origin_predicate	varchar2(1000);
	v_sql			varchar2(5000);
	v_start_date		date;
	v_end_date		date;
	v_delivery_date		date;
	v_delivery_plus		date;
	x_delivery_date		date;
BEGIN
--
--******************************************************************************
--
if p_delivery_date is null then
   x_delivery_date := sysdate;
else
   x_delivery_date := to_date(p_delivery_date,'mm/dd/yyyy');
end if;

if p_monthly_daily_ind = 'M' then
   v_start_date		:= add_months(trunc(x_delivery_date,'month'),-1);	    -- 1st Day of Prev Month
   v_end_date		:= last_day(x_delivery_date);				    -- Last Day of Curr Month
   v_delivery_date	:= last_day(add_months(trunc(x_delivery_date,'month'),-1)); -- Last Day of Prev Month
   v_delivery_plus	:= last_day(add_months(trunc(x_delivery_date,'month'),-1)) +1; -- next day for delivery date
else
   if p_month = '1' then
	v_start_date	:= trunc(x_delivery_date,'month');			    -- 1st Day of Curr Month
	v_end_date	:= last_day(x_delivery_date);				    -- Last Day of Curr Month
	v_delivery_date	:= x_delivery_date;					    -- Sysdate - 1
	v_delivery_plus	:= x_delivery_date+1;				    -- Sysdate
   elsif p_month = '2' then
	v_start_date	:= trunc(x_delivery_date,'month');			    -- 1st Day of Curr Month
	v_end_date	:= last_day(add_months(x_delivery_date,1));		    -- Last Day of Next Month
	v_delivery_date	:= x_delivery_date;					    -- Sysdate - 1
	v_delivery_plus	:= x_delivery_date+1;				    -- Sysdate
   elsif p_month = '3' then
	v_start_date	:= trunc(x_delivery_date,'month');			    -- 1st Day of Curr Month
	v_end_date	:= last_day(add_months(x_delivery_date,2));		    -- Last Day of Next Next Month
	v_delivery_date	:= x_delivery_date;					    -- Sysdate - 1
	v_delivery_plus	:= x_delivery_date+1;				    -- Sysdate
   else
	v_start_date	:= trunc(x_delivery_date,'month');			    -- 1st Day of Curr Month
	v_end_date	:= last_day(add_months(x_delivery_date,1));		    -- Last Day of Next Month
	v_delivery_date	:= x_delivery_date;					    -- Sysdate - 1
	v_delivery_plus	:= x_delivery_date+1;				    -- Sysdate
   end if;
end if;

if p_company_code <> 'ALL' then
	v_company_predicate:= ' AND co.company_id in ('||rpt.multi_value(p_company_code)||') ';
end if;

if p_origin <> 'ALL' then
	v_origin_predicate:= ' AND nvl(co.order_taken_call_center,co.origin_id) in ('||rpt.multi_value(p_origin)||') ';
end if;

v_sql := 'insert into rpt.rep_tab
		(col1,
		col2,
		col3,
		col4,
		col5,
		col6,
		col7,
		col8
		)
	  select delivery_date,
             to_date(to_char(delivery_date,''Month''),''Month'') delivery_month,
             sum(phone_count) phone_count,
             sum(internet_count) internet_count,
             sum(phone_count+internet_count) phone_internet_total,
             sum(florist_delivered_count) florist_delivered_count,
             sum(ds_delivered_count) ds_delivered_count,
             sum(florist_delivered_count+ds_delivered_count) florist_ds_delivered_total
	  from
		(select case when (nvl(trunc(cod.eod_delivery_date),''01-JAN-2005'') between '''|| v_start_date ||''' and '''|| v_delivery_date ||''')
                        then trunc(cod.eod_delivery_date)
                        else trunc(cod.delivery_date)
			end delivery_date,
			sum(decode(fao.origin_type,''phone'',1,0)) phone_count,
			sum(decode(fao.origin_type,''internet'',1,0)) internet_count,
			sum(case when (nvl(cat.ship_method,''SD'')=''SD'')
                	    then 1
	                    else 0
        	            end) florist_delivered_count,
			sum(case when (nvl(cat.ship_method,''SD'')<>''SD'')
	                    then 1
			    else 0
                	    end) ds_delivered_count
		from clean.order_details cod
		join clean.orders co
			on (co.order_guid=cod.order_guid)
		join ftd_apps.origins fao
			on (co.origin_id = fao.origin_id)
		join clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
                join ftd_apps.product_master pm
                    on pm.product_id = cod.product_id
                    and pm.product_type <> ''SERVICES''
		where co.company_id is not null
		and co.origin_id is not null '
		|| v_company_predicate
		|| v_origin_predicate
		|| ' and co.origin_id <>''TEST''
		and cat.transaction_type = ''Order''
		and cat.payment_type <> ''NC''
		and trunc(co.order_date) <= '''|| v_delivery_date ||'''
		and (trunc(cod.eod_delivery_date) between '''|| v_start_date ||''' and '''|| v_delivery_date ||'''
		  or trunc(cod.delivery_date) between '''|| v_delivery_plus ||''' and '''|| v_end_date ||''')
	group by case when (nvl(trunc(cod.eod_delivery_date),''01-JAN-2005'') between '''|| v_start_date ||''' and '''|| v_delivery_date ||''')
                         then trunc(cod.eod_delivery_date)
                         else trunc(cod.delivery_date)
               		 end
		union all
		select case when (nvl(trunc(cod.eod_delivery_date),''01-JAN-2005'') between '''|| v_start_date ||''' and '''|| v_delivery_date ||''')
                       then trunc(cod.eod_delivery_date)
                       else case when (nvl(trunc(cod.delivery_date),''01-JAN-2005'') between '''|| v_delivery_plus ||''' and '''|| v_end_date ||''')
                            then trunc(cod.delivery_date)
                            else trunc(cat.transaction_date)
                            end
		       end delivery_date,
			sum(decode(fao.origin_type,''phone'',-1,0)) phone_count,
		       sum(decode(fao.origin_type,''internet'',-1,0)) internet_count,
		       sum(case when (nvl(cat.ship_method,''SD'')=''SD'')
                           then -1
                           else 0
			   end) florist_delivered_count,
		       sum(case when (nvl(cat.ship_method,''SD'')<>''SD'')
                           then -1
	                   else 0
			   end) ds_delivered_count
		from clean.order_details cod
		join clean.orders co
			on (co.order_guid=cod.order_guid)
		join ftd_apps.origins fao
			on (co.origin_id = fao.origin_id)
		join clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
                join ftd_apps.product_master pm
                    on pm.product_id = cod.product_id
                    and pm.product_type <> ''SERVICES''
		where co.company_id is not null
		and co.origin_id is not null '
		||v_company_predicate
		||v_origin_predicate
		|| ' and co.origin_id <>''TEST''
		and nvl(cat.refund_disp_code,''X'') like ''A%''
		and cat.transaction_type=''Refund''
		and cat.payment_type<>''NC''
		and trunc(co.order_date) <= '''|| v_delivery_date ||'''
		and (trunc(cod.eod_delivery_date) between '''|| v_start_date ||''' and '''|| v_delivery_date ||'''
		  or trunc(cod.delivery_date) between '''|| v_delivery_plus ||''' and '''|| v_end_date ||'''
		  or (  trunc(cod.eod_delivery_date) < '''|| v_delivery_date ||'''
		      and trunc(cat.transaction_date) between '''|| v_start_date ||''' and '''|| v_delivery_date ||'''))
	group by case when (nvl(trunc(cod.eod_delivery_date),''01-JAN-2005'') between '''|| v_start_date ||''' and '''|| v_delivery_date ||''')
                         then trunc(cod.eod_delivery_date)
                         else case when (nvl(trunc(cod.delivery_date),''01-JAN-2005'') between '''|| v_delivery_plus ||''' and '''|| v_end_date ||''')
                            then trunc(cod.delivery_date)
                            else trunc(cat.transaction_date)
                            end
                         end
		union all
		select calendar_date,
		       0,
		       0,
		       0,
		       0
		from rpt.dates
		where calendar_date between '''|| v_start_date ||''' and '''|| v_end_date ||''' )
		group by delivery_date,
			to_date(to_char(delivery_date,''Month''),''Month'') '
	;
execute immediate v_sql
;
end
;
.
/
