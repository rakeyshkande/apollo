create or replace PROCEDURE        rpt.CSR05_Florist_Avail_POP_TAB  
    (p_source_code in varchar2,
                                p_source_type in varchar2,
                                p_florist_status in varchar2
                                )

    IS
    v_source_temp_table varchar2(100):= NULL;
    v_stype_temp_table varchar2(100):= NULL;
    v_fstatus_temp_table varchar2(200):= NULL;
    v_source_code_predicate  varchar2(1000) := NULL;
    v_source_type_predicate  varchar2(1000) := NULL;
    v_florist_status_predicate  varchar2(1000) := NULL;
    v_sql                                 varchar2(5000) := NULL;
    v_date varchar2(20) := to_char(sysdate,'dd-mm-yy');

BEGIN

  if  p_source_code <> 'ALL'
  and p_source_code IS NOT NULL then
        v_source_temp_table := 'JOIN FTD_APPS.fl_avl_src_temp FST ON SM.source_code = FST.source_code';
  end if;

  if  p_source_type <> 'ALL'
  and p_source_type IS NOT NULL then
         v_stype_temp_table := 'JOIN FTD_APPS.fl_avl_srtp_temp FSTT ON SM.source_type = FSTT.source_type';
  end if;

  if  p_florist_status <> 'ALL'
  and p_florist_status IS NOT NULL then
        v_fstatus_temp_table := 'JOIN FTD_APPS.fl_avl_fl_sts_temp FFT ON Decode(Fb.Block_Type,''O'',''Opt Out'',Fm.Status) = FFT.florist_status';
  end if;


  v_sql :=
     'INSERT into rpt.rep_tab
                (col1,
                col2,
                col3,
                col4,
                col5,
                col6,
                col7,
                col8
                ) Select FM.Florist_Id,
                   FM.Florist_Name,
                   SM.Source_Code,
                   SM.Source_Type,
                   Decode(Fb.Block_Type,''O'',''Opt Out'',Fm.Status) As Status,
                   decode(SFP.Priority,''1'',''Yes'','' '') Primary,
                   decode(SFP.Priority,''2'',''Yes'','' '') Backup,
                   to_char(to_date(Fm.Last_Updated_Date),''mm/dd/yyyy'') As Updated_On
                From  Ftd_Apps.Florist_Master Fm
                JOIN  Ftd_Apps.Source_Florist_Priority Sfp ON Fm.Florist_Id=Sfp.Florist_Id
                JOIN  Ftd_Apps.Source_Master Sm ON Sm.Source_Code=Sfp.Source_Code
  LEFT OUTER JOIN Ftd_Apps.Florist_Blocks Fb on Fm.florist_id = Fb.florist_id and Fb.block_start_date <= to_date('''||v_date||''',''dd-mm-yy'') and Fm.status = ''Blocked''
      '||v_source_temp_table||'
      '||v_stype_temp_table||'
      '||v_fstatus_temp_table||'';

  EXECUTE IMMEDIATE v_sql;
END CSR05_Florist_Avail_POP_TAB;
.
/
