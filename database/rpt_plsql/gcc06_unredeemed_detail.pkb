CREATE OR REPLACE
PACKAGE BODY rpt.GCC06_UnRedeemed_Detail IS

  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure print_line( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
	rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     --	when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end;
--****************************************************************************************************
--
--****************************************************************************************************
procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  print_line( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;

--****************************************************************************************************
--****************************************************************************************************
procedure print_heading( y in number )
is
begin

  print_comment( 'Print Headings' );

  print_line( 'F;R'||to_char(y)||';FG0L;SM1' );
  print_line( 'F;Y'||to_char(y)||';X8;FG0R;SM1' );
  print_line( 'F;Y'||to_char(y)||';X9;FG0R;SM1' );
  print_line( 'F;Y'||to_char(y)||';X10;FG0R;SM1' );
  print_line( 'F;Y'||to_char(y)||';X11;FG0R;SM1' );

  print_line( 'C;Y'||y||';X2;K"Request #"' );
  print_line( 'C;Y'||y||';X3;K"Issue Date"' );
  print_line( 'C;Y'||y||';X4;K"Certificate"' );
  print_line( 'C;Y'||y||';X5;K"Program Name"' );
  print_line( 'C;Y'||y||';X6;K"Company"' );
  print_line( 'C;Y'||y||';X7;K"Issue Amount"' );
  print_line( 'C;Y'||y||';X8;K"Paid Amount"' );
  print_line( 'C;Y'||y||';X9;K"Total Liability"' );
  print_line( 'C;Y'||y||';X10;K"Count"' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_heading;

--****************************************************************************************************
--****************************************************************************************************

procedure print_title(p_start_date in date, p_end_date in date)
is

begin

  print_comment( 'Print Title' );

  print_line( 'ID;ORACLE' );

  print_line( 'P;Ph:mm:ss');                 -- SM0
  print_line( 'P;FCourier;M200' );           -- SM1
  print_line( 'P;FCourier;M200;SB' );        -- SM2
  print_line( 'P;FCourier;M200;SUB' );       -- SM3
  print_line( 'P;FCourier;M160' );           -- SM4

  print_line( 'F;R1;FG0L;SM2' );
  if p_start_date is null then
     print_line( 'C;Y1;X2;K"DETAIL OF UNREDEEMED GIFT CERTIFICATES REPORT THROUGH '||to_char(p_end_date,'mm/dd/yyyy')||'"' );
  else
     print_line( 'C;Y1;X2;K"Detail of Unredeemed Gift Certificates Report for '||to_char(p_start_date,'mm/dd/yyyy')||' Thru '||to_char(p_end_date,'mm/dd/yyyy')||'"' );
  end if;

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_title;

procedure print_totals(p_issue_amount in number, p_paid_amount in number,
                       p_count in number, p_heading in varchar2, p_y in number)
is
  line varchar2(300);

begin

  print_line( 'F;R'||to_char(p_y)||';FG0L;SM1' );
  print_line( 'F;Y'||to_char(p_y)||';X7;F$2R;SM1' );
  print_line( 'F;Y'||to_char(p_y)||';X8;F$2R;SM1' );
  print_line( 'F;Y'||to_char(p_y)||';X10;FG0R;SM1' );

  line := 'C;Y'|| to_char(p_y) || ';X2';
  line := line || ';K"'||p_heading||'"';
  print_line( line );

  line := 'C;Y'|| to_char(p_y) || ';X7';
  line := line || ';K'||p_issue_amount||'';
  print_line( line );

  line := 'C;Y'|| to_char(p_y) || ';X8';
  line := line || ';K'||p_paid_amount||'';
  print_line( line );

  line := 'C;Y'|| to_char(p_y) || ';X10';
  line := line || ';K'||p_count||'';
  print_line( line );

  if g_CLOB = 'N' then
      utl_file.fflush( g_file );
  end if;

end print_totals;

--****************************************************************************************************
--  The print_rows function adds the data records to the file.  In this example there are actually
--  two cursors.  The cursor is just your query from you data model.  If you have a complex data
--  model you will have to some additional PL/SQL to get all of your data.
--****************************************************************************************************
function print_rows(p_start_date  in date,
                    p_end_date    in date ) return number
is
  line       varchar2(32767) := null;  -- print line
  y          number          := 4;    -- first detail line number (row)

  v_old_request   varchar2(15)   := 'XXX';   -- holds previous request number

  v_request_issue_amount     number := 0;
  v_request_paid_amount      number := 0;
  v_request_count            number := 0;
  v_report_issue_amount      number := 0;
  v_report_paid_amount       number := 0;
  v_report_count             number := 0;

  type           my_curs_type is REF CURSOR;
  curs           my_curs_type;

  type       ret_rec is record
                   (
                   request_number  varchar2(80),
                   issue_date date,
                   certificate_number varchar2(80),
                   program_name varchar2(80),
                   company varchar2(80),
                   issue_amt number,
                   paid_amt number,
                   total_liability number,
                   coupon_count number
                   );

  ret           ret_rec;

begin

  OPEN curs FOR 
  select cgcr.request_number,
         cgcr.issue_date,
         cgc.gc_coupon_number certificate_number,
         cgcr.program_name,
         facm.company_name company,
         cgcr.issue_amount issue_amt,
         cgcr.paid_amount paid_amt,
         (cgcr.paid_amount * cgcr.quantity) total_liability,
         cgcr.quantity coupon_count
  from   clean.gc_coupons cgc, clean.gc_coupon_request cgcr, ftd_apps.company_master facm
  where  cgc.gc_coupon_status in ('Issued Active','Reinstate')
  and    cgcr.request_number = cgc.request_number
  and    cgcr.expiration_date is null
  and    cgcr.gc_coupon_type='GC'
  and    cgcr.issue_date between p_start_date and p_end_date
  and    cgcr.company_id=facm.company_id(+)
  order by cgcr.issue_date, cgcr.request_number, cgc.gc_coupon_number;

       LOOP
       	  FETCH curs INTO ret;
     	    EXIT WHEN curs%NOTFOUND;

				--**************************************************************************
				--REQUEST Break  --  Print sub totals for REQUEST
				--**************************************************************************

				if v_old_request = 'XXX' then
					v_old_request := ret.request_number;
				end if;
				if v_old_request <> ret.request_number then
					print_totals(v_request_issue_amount, v_request_paid_amount, v_request_count,
						'Totals for Request ' || v_old_request, y);
					v_request_issue_amount := 0;
					v_request_paid_amount := 0;
					v_request_count := 0;
					y:=y+1;
					print_line( 'C;Y'||to_char(y)||';X2;K" "' );
					y:=y+1;
				end if;

				--**************************************************************************
				--Print DETAIL Line
				--**************************************************************************

  				line := 'C;Y'|| to_char(y) || ';X2';
				line := line || ';K"'||ret.request_number||'"';
				print_line( line );

  				line := 'C;Y'|| to_char(y) || ';X3';
				line := line || ';K"'||ret.issue_date||'"';
				print_line( line );

  				line := 'C;Y'|| to_char(y) || ';X4';
				line := line || ';K"'||ret.certificate_number||'"';
				print_line( line );

  				line := 'C;Y'|| to_char(y) || ';X5';
				line := line || ';K"'||ret.program_name||'"';
				print_line( line );

  				line := 'C;Y'|| to_char(y) || ';X6';
				line := line || ';K"'||ret.company||'"';
				print_line( line );

				print_line( 'F;F$2G;X7' );--to add a dollar sign and 2 decimal places
  				line := 'C;Y'|| to_char(y) || ';X7';
				line := line || ';K'||ret.issue_amt||'';
				print_line( line );

				print_line( 'F;F$2G;X8' );--to add a dollar sign and 2 decimal places
  				line := 'C;Y'|| to_char(y) || ';X8';
				line := line || ';K'||ret.paid_amt||'';
				print_line( line );

				print_line( 'F;F$2G;X9' );--to add a dollar sign and 2 decimal places
  				line := 'C;Y'|| to_char(y) || ';X9';
				line := line || ';K'||ret.total_liability||'';
				print_line( line );

 				line := 'C;Y'|| to_char(y) || ';X10';
				line := line || ';K'||ret.coupon_count||'';
				print_line( line );

				y := y + 1;
				print_line( 'C;Y'||to_char(y) );

				if g_CLOB = 'N' then
					utl_file.fflush( g_file );
				end if;

				v_old_request := ret.request_number;

				v_request_issue_amount := v_request_issue_amount + ret.issue_amt;
				v_request_paid_amount := v_request_paid_amount + ret.paid_amt;
				v_request_count := v_request_count + 1;
				v_report_issue_amount := v_report_issue_amount + ret.issue_amt;
				v_report_paid_amount := v_report_paid_amount + ret.paid_amt;
				v_report_count := v_report_count + 1;

      END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 4 then
 		 print_comment( 'No Rows Returned' );
     line := 'C;Y'|| to_char(y) || ';X2';
     line := line || ';K"No Data to Found"';
		 print_line( line );

     y := y + 1;
     print_line( 'C;Y'||to_char(y) );

     if g_CLOB = 'N' then
	utl_file.fflush( g_file );
     end if;

  else

    print_totals(v_request_issue_amount, v_request_paid_amount, v_request_count,
        'Totals for Request ' || v_old_request, y);
    y:=y+1;
    print_line( 'C;Y'||to_char(y)||';X2;K" "' );
    y:=y+1;
    print_totals(v_report_issue_amount, v_report_paid_amount, v_report_count,
        'Grand Totals', y);

  end if;

  --**************************************************************************
  --Ceanup Stuff - Close Cursor and Return
  --**************************************************************************
  CLOSE curs;

  RETURN (y);

end print_rows;

--****************************************************************************************************
-- This section is used to set the column widths.  You will want to change the last number of the
-- line to match the width of the field in the database.
--****************************************************************************************************
procedure print_widths
is
begin

  print_comment( 'Format Column Widths' );

  print_line( 'F;W01 01 05' );
  print_line( 'F;W02 02 15' );
  print_line( 'F;W03 03 15' );
  print_line( 'F;W04 08 15' );
  print_line( 'F;W09 09 20' );
  print_line( 'F;W10 10 15' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_widths;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure run_report(p_start_date in varchar2,
		p_end_date in varchar2,
		p_submission_id in number,
		p_CLOB in varchar2
		)
is
  l_row_cnt number;
  l_col_cnt number;
  l_status  number;
  v_start_date date;
  v_end_date date;
   v_OUT_STATUS_det	varchar2(30);
   v_OUT_MESSAGE_det	varchar2(1000);
   v_OUT_STATUS_sub 	varchar2(30);
   v_OUT_MESSAGE_sub	varchar2(1000);

begin

   if p_start_date is not null then
      v_start_date := to_date(p_start_date,'mm/dd/yyyy');
   else
      v_start_date := add_months(trunc(sysdate,'MONTH'), -1);
   end if;
   if p_end_date is not null then
      v_end_date := to_date(p_end_date,'mm/dd/yyyy');
   else
      v_end_date := last_day(add_months(sysdate,-1));
   end if;

--
-- Prepare for either CLOB output or REPORT_DIR output
--

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
			P_SUBMISSION_ID,
			'SYS',
			'S' ,
			null ,
			v_OUT_STATUS_DET,
			v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
       			(
			P_SUBMISSION_ID,
			empty_clob(),
			v_out_status_det,
			v_out_message_det
		        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','GCC06_Gift_Cert_UnRedeemed_Detail.slk', 'W' );
   end if;

    print_title( v_start_date,
               v_end_date );

    print_heading(3);

    l_row_cnt := print_rows( v_start_date,
		             v_end_date );

    print_widths;

    print_line( 'E' );

   --
   -- Update Database with CLOB and set report submission status (if CLOB'ing)
   -- Close report file (if not CLOB'ing
   --

      if p_CLOB = 'Y' then
         begin
   	-- update the report_detail_char table with the package global variable g_clob
   	rpt.report_clob_pkg.update_report_detail_char
   			(
   			 p_submission_id,
   			 v_out_status_det,
   			 v_out_message_det
   			);
   	if v_out_status_det = 'N' then
   	-- if db update failure, update report_submission_log with ERROR status
   	rpt.report_pkg.update_report_submission_log
   			(
   			 p_submission_id,
   			 'REPORT',
   			 'E',
   			 v_out_message_det,
   			 v_out_status_sub,
   			 v_out_message_sub
   			);
   	else
   	-- if db update successful, update report_submission_log with COMPLETE status
   	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
   			(
   			 P_SUBMISSION_ID,
   			 'SYS',
   			 'C' ,
   			 null ,
   			 v_out_status_det ,
   			 v_out_message_det
   			);
   	end if;
   	--exception
   	--	when others then srw.message(32321,'Error in p');
        end;
        commit;
      else
        utl_file.fflush( g_file );
        utl_file.fclose( g_file );
   end if;

  end run_report;
end;
.
/
