create or replace PROCEDURE rpt.acc08_ords_dlvd_shp_st_pop_tab
			(p_start_date in varchar2,
			 p_end_date in varchar2,
			 p_origin in varchar2,
			 p_company in varchar2
			)
IS
  v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
  v_end_date date := to_date(p_end_date||' 23:59:59','mm/dd/yyyy hh24:mi:ss');
  v_origin_predicate VARCHAR2(500);
  v_company_predicate VARCHAR2(500);
  v_sql			VARCHAR2(30000);

BEGIN

execute immediate 'alter session set "_disable_function_based_index"=TRUE';
IF p_origin = 'ALL' OR INSTR(p_origin, 'ALL') > 0 THEN
   v_origin_predicate := ' ';
ELSE
	v_origin_predicate:= ' AND co.origin_id in ('||rpt.multi_value(p_origin)||') ';
END IF;

IF p_company = 'ALL' OR INSTR(p_company, 'ALL') > 0 THEN
   v_company_predicate := ' AND co.company_id is not null ';
ELSE
	v_company_predicate:= ' AND co.company_id in ('||rpt.multi_value(p_company)||') ';
END IF;
  
v_sql := '  
insert into rpt.rep_tab (col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15,col16)
select ''1'',a.country,
            a.state,
             sum(a.Merch_value+nvl(b.Merch_value,0)),
             sum(a.add_ons+nvl(b.add_ons,0)),
             sum(a.shipping_fee+nvl(b.shipping_fee,0)),
             sum(a.service_fee+nvl(b.service_fee,0)),
             sum(a.discount_amount+nvl(b.discount_amount,0)),
             sum(a.order_value+nvl(b.order_value,0)),
			 sum(a.TAX1_AMOUNT+nvl(b.TAX1_AMOUNT,0)),
			 sum(a.TAX2_AMOUNT+nvl(b.TAX2_AMOUNT,0)),
			 sum(a.TAX3_AMOUNT+nvl(b.TAX3_AMOUNT,0)),
			 sum(a.TAX4_AMOUNT+nvl(b.TAX4_AMOUNT,0)),
              sum(a.tax+nvl(b.tax,0)),
             sum(a.total_order_amount+nvl(b.total_order_amount,0)),
             sum(a.order_count-nvl(b.order_count,0))
from (select
       Case cc.country when ''US'' then ''US''
                       when ''USA'' then ''US''
                       when ''CA'' then ''CA''
                       when ''CAN'' then ''CA''
                       else ''INTL''
       end country,
            cc.state,
            sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (   sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
            
				 sum(nvl(cob.TAX1_AMOUNT,0))             TAX1_AMOUNT,
				 sum(nvl(cob.TAX2_AMOUNT,0))             TAX2_AMOUNT,
				 sum(nvl(cob.TAX3_AMOUNT,0))             TAX3_AMOUNT,
				 sum(nvl(cob.TAX4_AMOUNT,0))             TAX4_AMOUNT,
				 
				  sum(nvl(cat.tax,0)+
                 nvl(cat.shipping_tax,0)+
                 nvl(cat.service_fee_tax,0))             tax,
             (   sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
                 sum(nvl(cat.tax,0)+
                     nvl(cat.shipping_tax,0)+
                     nvl(cat.service_fee_tax,0))      total_order_amount,
             SUM(CASE upper(cat.transaction_type) WHEN ''ORDER'' THEN 1 ELSE null END) order_count
from clean.order_details cod
join clean.orders co
on (co.order_guid=cod.order_guid)
right outer join ftd_apps.origins fao
on (fao.origin_id = co.origin_id)
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
join clean.order_bills cob
on (cat.order_bill_id = cob.order_bill_id)
join clean.customer cc
on(cc.customer_id = cod.recipient_id)
where ((cod.eod_delivery_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)<= cod.eod_delivery_date)
   or (cat.transaction_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)>= cod.eod_delivery_date))
and cat.transaction_type <> ''Refund''
and cat.payment_type <> ''NC''
and co.origin_id is not null '
|| v_company_predicate ||
' and co.origin_id <>''TEST''
and co.origin_id <>''test''
and cc.country in (''US'',''USA'',''CA'',''CAN'') ' || v_origin_predicate ||
' group by cc.country,
            cc.state 
UNION
select ''INTL''country,
       ''NA'' state,
            sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (   sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             
			     sum(nvl(cob.TAX1_AMOUNT,0))             TAX1_AMOUNT,
				 sum(nvl(cob.TAX2_AMOUNT,0))             TAX2_AMOUNT,
				 sum(nvl(cob.TAX3_AMOUNT,0))             TAX3_AMOUNT,
				 sum(nvl(cob.TAX4_AMOUNT,0))             TAX4_AMOUNT,
			 
			 
			 sum(nvl(cat.tax,0)+
                 nvl(cat.shipping_tax,0)+
                 nvl(cat.service_fee_tax,0))             tax,
				 
             (   sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
                 sum(nvl(cat.tax,0)+
                     nvl(cat.shipping_tax,0)+
                     nvl(cat.service_fee_tax,0))      total_order_amount,
             SUM(CASE upper(cat.transaction_type) WHEN ''ORDER'' THEN 1 ELSE null END) order_count
from clean.order_details cod
join clean.orders co
on (co.order_guid=cod.order_guid)
right outer join ftd_apps.origins fao
on (fao.origin_id = co.origin_id)
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
join clean.order_bills cob
on (cat.order_bill_id = cob.order_bill_id)
join clean.customer cc
on(cc.customer_id = cod.recipient_id)
where ((cod.eod_delivery_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)<= cod.eod_delivery_date)
   or (cat.transaction_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)>= cod.eod_delivery_date))
and cat.transaction_type <> ''Refund''
and cat.payment_type <> ''NC''
and co.origin_id is not null '
|| v_company_predicate ||
' and co.origin_id <>''TEST''
and co.origin_id <>''test''
and (cc.country not in (''US'',''USA'',''CA'',''CAN'') or cc.country is null) ' || v_origin_predicate ||
' ) a,
(select Case cc.country when ''US'' then ''US''
                       when ''USA'' then ''US''
                       when ''CA'' then ''CA''
	         when ''CAN'' then ''CA''
                       else ''INTL''
       end country,
            cc.state state,
            sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (sum    (cat.Product_amount)+
                  sum(cat.add_on_amount) +
                  sum(cat.shipping_fee) +
                  sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                  sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             
			
			     sum(nvl(cr.TAX1_AMOUNT,0))*-1            TAX1_AMOUNT,
				 sum(nvl(cr.TAX2_AMOUNT,0))*-1            TAX2_AMOUNT,
				 sum(nvl(cr.TAX3_AMOUNT,0))*-1             TAX3_AMOUNT,
				 sum(nvl(cr.TAX4_AMOUNT,0))*-1            TAX4_AMOUNT,
				 
				sum(nvl(cat.tax,0)+
                 nvl(cat.shipping_tax,0)+
                 nvl(cat.service_fee_tax,0))             tax,
				
             (   sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
                 sum(nvl(cat.tax,0)+
                     nvl(cat.shipping_tax,0)+
                     nvl(cat.service_fee_tax,0))         total_order_amount,
            sum(decode(substr(cat.refund_disp_code,1,1),''A'',1))  order_count
from clean.order_details cod
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)


join clean.refund cr
on (cr.refund_id = cat.refund_id)
join  clean.refund_disposition_val crdv
on (crdv.refund_disp_code = cat.refund_disp_code)
join clean.orders co
on (cod.order_guid = co.order_guid)
join clean.customer cc
on(cc.customer_id = cod.recipient_id)
where ((cod.eod_delivery_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)<= cod.eod_delivery_date)
   or (cat.transaction_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)>= cod.eod_delivery_date))
and cat.payment_type <> ''NC''
and crdv.refund_accounting_type = ''CR''
and co.origin_id is not null '
|| v_company_predicate ||
' and co.origin_id <>''TEST''
and co.origin_id <>''test''
and cc.country in (''US'',''USA'',''CA'',''CAN'') ' || v_origin_predicate ||
' group by cc.country,
            cc.state
UNION
select ''INTL''country,
       ''NA'' state,
            sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (sum    (cat.Product_amount)+
                  sum(cat.add_on_amount) +
                  sum(cat.shipping_fee) +
                  sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                  sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             
				 sum(nvl(cr.TAX1_AMOUNT,0))*-1             TAX1_AMOUNT,
				 sum(nvl(cr.TAX2_AMOUNT,0))*-1             TAX2_AMOUNT,
				 sum(nvl(cr.TAX3_AMOUNT,0))*-1             TAX3_AMOUNT,
				 sum(nvl(cr.TAX4_AMOUNT,0))*-1             TAX4_AMOUNT,
				 
				 sum(nvl(cat.tax,0)+
                 nvl(cat.shipping_tax,0)+
                 nvl(cat.service_fee_tax,0))             tax,
				 
             (   sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
                 sum(nvl(cat.tax,0)+
                     nvl(cat.shipping_tax,0)+
                     nvl(cat.service_fee_tax,0))         total_order_amount,
            sum(decode(substr(cat.refund_disp_code,1,1),''A'',1))  order_count
from clean.order_details cod
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)


join clean.refund cr
on (cr.refund_id = cat.refund_id)
join  clean.refund_disposition_val crdv
on (crdv.refund_disp_code = cat.refund_disp_code)
join clean.orders co
on (cod.order_guid = co.order_guid)
join clean.customer cc
on(cc.customer_id = cod.recipient_id)
where ((cod.eod_delivery_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)<= cod.eod_delivery_date)
   or (cat.transaction_date between '''|| v_start_date||''' AND '''||v_end_date||''''||
   ' and trunc(cat.transaction_date)>= cod.eod_delivery_date))
and cat.payment_type <> ''NC''
and crdv.refund_accounting_type = ''CR''
and co.origin_id is not null '
|| v_company_predicate ||
' and co.origin_id <>''TEST''
and co.origin_id <>''test''
and (cc.country not in (''US'',''USA'',''CA'',''CAN'') or cc.country is null) ' || v_origin_predicate ||
' ) b
where a.country= b.country(+)
and            a.state = b.state(+)
group by ''1'',a.country,
            a.state ';
            
EXECUTE IMMEDIATE v_sql;
execute immediate 'alter session set "_disable_function_based_index"=FALSE';

END;
/
