CREATE OR REPLACE
PROCEDURE rpt.mkt01_pop_tab (p_start_date in varchar2,
			 p_end_date in varchar2,
			 p_company_code in varchar2,
			 p_gross_net_ind in varchar2,
			 p_sum_dtl_ind in varchar2,
			 p_sort_ind in varchar2)
IS
--
---------------------------------------------------------
-- Work Variables
---------------------------------------------------------
--
v_sql			varchar2(30000);
--
v_company_id_predicate	varchar2(100);
--
v_start_date	date := to_date(p_start_date,'mm/dd/yyyy');
v_start_date_py	date := add_months(trunc(to_date(p_start_date,'mm/dd/yyyy'),'Month'),-12);
--
v_end_date	date := to_date(p_end_date,'mm/dd/yyyy');
v_end_date_py	date := add_months(trunc(to_date(p_end_date,'mm/dd/yyyy'),'Month'),-12);
--
BEGIN
--
if  p_company_code IS NOT NULL
and p_company_code <> 'ALL' then
   v_company_id_predicate :=
		' AND co.company_id in (' || rpt.multi_value(p_company_code) || ')';
end if;
--
--**************************************************************
-- Orders By Source - GROSS DETAIL
--**************************************************************
--
If p_gross_net_ind = 'Gross' and p_sum_dtl_ind = 'Detail' Then
   v_sql := 'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15,
	 col16,
	 col17
	)
	SELECT   ''1O'',
	 --
	 CASE fasm.order_source
         WHEN ''P'' THEN ''Phone''
         WHEN ''I'' THEN ''Internet''
         END origin_type,
	 --
         fasm.source_type source_type,
         fasm.source_code source_code,
         co.company_id company_id,
	 --
	 count(pyo.order_count) order_count_py,
         sum(decode(cat.transaction_type,''Order'',1,0)) order_count,
	 --
         sum(cat.product_amount) product_amount,
         sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_on_amount,
         sum(cat.shipping_fee) shipping_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
	 --
         sum(cat.tax) tax,
         sum(cat.shipping_tax) shipping_tax,
         sum(cat.service_fee_tax) service_fee_tax,
	 --
         (sum(cat.product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) +
          sum(cat.tax) +
          sum(cat.shipping_tax) +
          sum(cat.service_fee_tax)
         ) total_order,
	 --
         (sum(cat.Product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) / count(distinct cod.order_detail_id)
	 AOV
	--
        FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	ftd_apps.source_master fasm
			on (fasm.source_code = cod.source_code)
        LEFT OUTER JOIN
		(
		 SELECT fasm.order_source,
			fasm.source_type,
			fasm.source_code,
			co.company_id,
			sum(decode(cat.transaction_type,''Order'',1,0)) order_count
		 --
		 FROM	clean.orders co
		 JOIN	clean.order_details cod
				on (cod.order_guid = co.order_guid)
		 JOIN	clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
		 JOIN	ftd_apps.source_master fasm
				on (fasm.source_code = cod.source_code)
		 --
		 WHERE	fasm.order_source in (''I'',''P'')
		 AND	cat.payment_type <> ''NC''
		 AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
		 AND	(trunc(cat.transaction_date) between '''||v_start_date_py||
					''' and '''||v_end_date_py||''') '
		 ||v_company_id_predicate|| '
		 --
		 GROUP BY fasm.order_source,
			  fasm.source_type,
			  fasm.source_code,
			  co.company_id
		) pyo
			on (pyo.order_source = fasm.order_source  and
			    pyo.source_type = fasm.source_type and
			    pyo.source_code = fasm.source_code and
			    pyo.company_id  = co.company_id)
	--
	WHERE	fasm.order_source in (''I'',''P'')
	AND	cat.payment_type <> ''NC''
        AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
	AND	(trunc(cat.transaction_date) between '''||v_start_date||
				''' and '''||v_end_date||''') '
	||v_company_id_predicate|| '
	--
	GROUP BY CASE fasm.order_source
         WHEN ''P'' THEN ''Phone''
         WHEN ''I'' THEN ''Internet''
         END,
         fasm.source_type,
         fasm.source_code,
         co.company_id '
	;
END IF
;
--
--**************************************************************
-- Orders By Source - GROSS SUMMARY
--**************************************************************
--
If p_gross_net_ind = 'Gross' and p_sum_dtl_ind = 'Summary' Then
   v_sql := 'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15
	)
	SELECT   ''2O'',
	 --
         fasm.source_type source_type,
         co.company_id company_id,
	 --
	 count(pyo.order_count) order_count_py,
         sum(decode(cat.transaction_type,''Order'',1,0)) order_count,
	 --
         sum(cat.product_amount) product_amount,
         sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_on_amount,
         sum(cat.shipping_fee) shipping_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
	 --
         sum(cat.tax) tax,
         sum(cat.shipping_tax) shipping_tax,
         sum(cat.service_fee_tax) service_fee_tax,
	 --
         (sum(cat.product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) +
          sum(cat.tax) +
          sum(cat.shipping_tax) +
          sum(cat.service_fee_tax)
         ) total_order,
	 --
         (sum(cat.Product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) / count(distinct cod.order_detail_id)
	  AOV
	 --
        FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	ftd_apps.source_master fasm
			on (fasm.source_code = cod.source_code)
        LEFT OUTER JOIN
		(
		 SELECT fasm.source_type,
			co.company_id,
			sum(decode(cat.transaction_type,''Order'',1,0)) order_count
		 --
		 FROM	clean.orders co
		 JOIN	clean.order_details cod
				on (cod.order_guid = co.order_guid)
		 JOIN	clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
		 JOIN	ftd_apps.source_master fasm
				on (fasm.source_code = cod.source_code)
		 --
		 WHERE	fasm.order_source in (''I'',''P'')
		 AND	cat.payment_type <> ''NC''
		 AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
		 AND	(trunc(cat.transaction_date) between '''||v_start_date_py||
					''' and '''||v_end_date_py||''') '
		 ||v_company_id_predicate|| '
		 --
		 GROUP BY fasm.source_type,
			  co.company_id
		) pyo
			on (pyo.source_type = fasm.source_type and
			    pyo.company_id  = co.company_id)
	--
	WHERE	fasm.order_source in (''I'',''P'')
	AND	cat.payment_type <> ''NC''
        AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
	AND	(trunc(cat.transaction_date) between '''||v_start_date||
				''' and '''||v_end_date||''') '
	||v_company_id_predicate|| '
	--
	GROUP BY fasm.source_type,
		 co.company_id '
	;
END IF
;
--
--**************************************************************
-- Orders By Source - NET DETAIL
--**************************************************************
--
If p_gross_net_ind = 'Net' and p_sum_dtl_ind = 'Detail' Then
   v_sql := 'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15,
	 col16,
	 col17
	)
	 --
	SELECT   ''3O'',
	 --
	 CASE fasm.order_source
         WHEN ''P'' THEN ''Phone''
         WHEN ''I'' THEN ''Internet''
         END origin_type,
	 --
         fasm.source_type source_type,
         fasm.source_code source_code,
         co.company_id company_id,
	 --
	 count(pyo.order_count) order_count_py,
         sum(decode(cat.transaction_type,''Order'',1,0)) order_count,
	 --
         sum(cat.product_amount) product_amount,
         sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_on_amount,
         sum(cat.shipping_fee) shipping_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
	 --
         sum(cat.tax) tax,
         sum(cat.shipping_tax) shipping_tax,
         sum(cat.service_fee_tax) service_fee_tax,
	 --
         (sum(cat.product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) +
          sum(cat.tax) +
          sum(cat.shipping_tax) +
          sum(cat.service_fee_tax)
         ) total_order,
	 --
         (sum(cat.Product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)))/ count(distinct cod.order_detail_id)
	  AOV
	--
        FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	ftd_apps.source_master fasm
			on (fasm.source_code = cod.source_code)
        LEFT OUTER JOIN
		(
		 SELECT fasm.order_source,
			fasm.source_type,
			fasm.source_code,
			co.company_id,
			sum(decode(cat.transaction_type,''Order'',1,0))order_count
		 --
		 FROM	clean.orders co
		 JOIN	clean.order_details cod
				on (cod.order_guid = co.order_guid)
		 JOIN	clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
		 JOIN	ftd_apps.source_master fasm
				on (fasm.source_code = cod.source_code)
		 --
		 WHERE	fasm.order_source in (''I'',''P'')
		 AND	cat.payment_type <> ''NC''
                 AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
		 AND	(trunc(cat.transaction_date) between '''||v_start_date_py||
					''' and '''||v_end_date_py||''') '
		 ||v_company_id_predicate|| '
		 --
		 GROUP BY fasm.order_source,
			  fasm.source_type,
			  fasm.source_code,
			  co.company_id
		) pyo
			on (pyo.order_source = fasm.order_source  and
			    pyo.source_type = fasm.source_type and
			    pyo.source_code = fasm.source_code and
			    pyo.company_id  = co.company_id)
	--
	WHERE	fasm.order_source in (''I'',''P'')
	AND	cat.payment_type <> ''NC''
        AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
	AND	(trunc(cat.transaction_date) between '''||v_start_date||
				''' and '''||v_end_date||''') '
	||v_company_id_predicate|| '
	--
	GROUP BY CASE fasm.order_source
         WHEN ''P'' THEN ''Phone''
         WHEN ''I'' THEN ''Internet''
         END,
         fasm.source_type,
         fasm.source_code,
         co.company_id
	--
	--
	UNION ALL
	--
	--
	SELECT   ''3R'',
	 --
	 CASE fasm.order_source
         WHEN ''P'' THEN ''Phone''
         WHEN ''I'' THEN ''Internet''
         END origin_type,
	 --
         fasm.source_type source_type,
         fasm.source_code source_code,
         co.company_id company_id,
	 --
	 count(pyr.order_count) order_count_py,
         sum(decode(substr(crdv.refund_disp_code,1,1),''A'',-1,0)) order_count,
	 --
         sum(cat.product_amount) product_amount,
         sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_on_amount,
         sum(cat.shipping_fee) shipping_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
	 --
         sum(cat.tax) tax,
         sum(cat.shipping_tax) shipping_tax,
         sum(cat.service_fee_tax) service_fee_tax,
	 --
         (sum(cat.product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) +
          sum(cat.tax) +
          sum(cat.shipping_tax) +
          sum(cat.service_fee_tax)
         ) total_order,
	 --
         (sum(cat.Product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) / count(distinct cod.order_detail_id)
	  AOV
	--
        FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	clean.refund_disposition_val crdv
			on (crdv.refund_disp_code = cat.refund_disp_code)
	JOIN	ftd_apps.source_master fasm
			on (fasm.source_code = cod.source_code)
        LEFT OUTER JOIN
		(
		 SELECT fasm.order_source,
			fasm.source_type,
			fasm.source_code,
			co.company_id,
			sum(decode(substr(crdv.refund_disp_code,1,1),''A'',-1,0)) order_count
		 FROM	clean.orders co
		 JOIN	clean.order_details cod
				on (cod.order_guid = co.order_guid)
		 JOIN	clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
		 JOIN	clean.refund_disposition_val crdv
				on (crdv.refund_disp_code = cat.refund_disp_code)
		 JOIN	ftd_apps.source_master fasm
				on (fasm.source_code = cod.source_code)
		 WHERE	fasm.order_source in (''I'',''P'')
	         AND     co.origin_id <>''TEST''
        	 AND	cat.payment_type <> ''NC''
		 AND	cat.transaction_type = ''Refund''
		 AND	crdv.refund_accounting_type = ''CR''
		 AND	(trunc(cat.transaction_date) between '''||v_start_date_py||
					''' and '''||v_end_date_py||''') '
		 ||v_company_id_predicate|| '
		 --
		 GROUP BY fasm.order_source,
			  fasm.source_type,
			  fasm.source_code,
			  co.company_id
		) pyr
			on (pyr.order_source	= fasm.order_source  and
			    pyr.source_type	= fasm.source_type and
			    pyr.source_code	= fasm.source_code and
			    pyr.company_id	= co.company_id)
	--
	WHERE	fasm.order_source in (''I'',''P'')
	AND	cat.payment_type <> ''NC''
	AND	cat.transaction_type = ''Refund''
        AND     co.origin_id <>''TEST''
	AND	crdv.refund_accounting_type = ''CR''
	AND	(trunc(cat.transaction_date) between '''||v_start_date||
				''' and '''||v_end_date||''') '
	||v_company_id_predicate|| '
	--
	GROUP BY CASE fasm.order_source
         WHEN ''P'' THEN ''Phone''
         WHEN ''I'' THEN ''Internet''
         END,
         fasm.source_type,
         fasm.source_code,
         co.company_id '
	;
END IF
;
--
--**************************************************************
-- Orders By Source - NET SUMMARY
--**************************************************************
--
If p_gross_net_ind = 'Net' and p_sum_dtl_ind = 'Summary' Then
   v_sql := 'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15
	)
	SELECT   ''4O'',
	 --
         fasm.source_type source_type,
         co.company_id company_id,
	 --
	 count(pyo.order_count) order_count_py,
         sum(decode(cat.transaction_type,''Order'',1,0)) order_count,
	 --
         sum(cat.product_amount) product_amount,
         sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_on_amount,
         sum(cat.shipping_fee) shipping_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
	 --
         sum(cat.tax) tax,
         sum(cat.shipping_tax) shipping_tax,
         sum(cat.service_fee_tax) service_fee_tax,
	 --
         (sum(cat.product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) +
          sum(cat.tax) +
          sum(cat.shipping_tax) +
          sum(cat.service_fee_tax)
         ) total_order,
	 --
         (sum(cat.Product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) / count(distinct cod.order_detail_id)
	  AOV
	--
        FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	ftd_apps.source_master fasm
			on (fasm.source_code = cod.source_code)
        LEFT OUTER JOIN
		(
		 SELECT fasm.source_type source_type,
			co.company_id company_id,
			sum(decode(cat.transaction_type,''Order'',1,0)) order_count
		 --
		 FROM	clean.orders co
		 JOIN	clean.order_details cod
				on (cod.order_guid = co.order_guid)
		 JOIN	clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
		 JOIN	ftd_apps.source_master fasm
				on (fasm.source_code = cod.source_code)
		 --
		 WHERE	fasm.order_source in (''I'',''P'')
		 AND	cat.payment_type <> ''NC''
		 AND     co.origin_id <>''TEST''
                 AND     cat.transaction_type in (''Order'',''Add Bill'')
		 AND	(trunc(cat.transaction_date) between '''||v_start_date_py||
					''' and '''||v_end_date_py||''') '
		 ||v_company_id_predicate|| '
		 --
		 GROUP BY fasm.source_type,
			  co.company_id
		) pyo
			on (pyo.source_type = fasm.source_type and
			    pyo.company_id  = co.company_id)
	--
	WHERE	fasm.order_source in (''I'',''P'')
	AND	cat.payment_type <> ''NC''
	AND	cat.transaction_type <> ''Refund''
        AND     co.origin_id <>''TEST''
        AND     cat.transaction_type in (''Order'',''Add Bill'')
	AND	(trunc(cat.transaction_date) between '''||v_start_date||
				''' and '''||v_end_date||''') '
	||v_company_id_predicate|| '
	--
	GROUP BY fasm.source_type,
	         co.company_id
	--
	--
	UNION ALL
	--
	--
	SELECT   ''4R'',
	 --
         fasm.source_type source_type,
         co.company_id company_id,
	 --
	 count(pyr.order_count) order_count_py,
         sum(decode(substr(crdv.refund_disp_code,1,1),''A'',-1,0)) order_count,
	 --
         sum(cat.product_amount) product_amount,
         sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_on_amount,
         sum(cat.shipping_fee) shipping_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
         sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
	 --
         sum(cat.tax) tax,
         sum(cat.shipping_tax) shipping_tax,
         sum(cat.service_fee_tax) service_fee_tax,
	 --
         (sum(cat.product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) +
          sum(cat.tax) +
          sum(cat.shipping_tax) +
          sum(cat.service_fee_tax)
         ) total_order,
	 --
         (sum(cat.Product_amount) +
          sum(cat.add_on_amount) +
          sum(cat.shipping_fee) +
          sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
          sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) / count(distinct cod.order_detail_id)
	  AOV
	--
        FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	clean.refund_disposition_val crdv
			on (crdv.refund_disp_code = cat.refund_disp_code)
	JOIN	ftd_apps.source_master fasm
			on (fasm.source_code = cod.source_code)
        LEFT OUTER JOIN
		(
		 SELECT fasm.source_type source_type,
			co.company_id company_id,
			sum(decode(substr(crdv.refund_disp_code,1,1),''A'',-1,0)) order_count
		 --
		 FROM	clean.orders co
		 JOIN	clean.order_details cod
				on (cod.order_guid = co.order_guid)
		 JOIN	clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
		 JOIN	clean.refund_disposition_val crdv
				on (crdv.refund_disp_code = cat.refund_disp_code)
		 JOIN	ftd_apps.source_master fasm
				on (fasm.source_code = cod.source_code)
		 --
		 WHERE	fasm.order_source in (''I'',''P'')
                 AND     co.origin_id <>''TEST''
		 AND	cat.payment_type <> ''NC''
		 AND	cat.transaction_type = ''Refund''
		 AND	crdv.refund_accounting_type = ''CR''
		 AND	(trunc(cat.transaction_date) between '''||v_start_date_py||
					''' and '''||v_end_date_py||''') '
		 ||v_company_id_predicate|| '
		 --
		 GROUP BY fasm.source_type,
			  co.company_id
		) pyr
			on (pyr.source_type = fasm.source_type and
			    pyr.company_id  = co.company_id)
	--
	WHERE	fasm.order_source in (''I'',''P'')
	AND	cat.payment_type <> ''NC''
        AND     co.origin_id <>''TEST''
	AND	cat.transaction_type = ''Refund''
	AND	crdv.refund_accounting_type = ''CR''
	AND	(trunc(cat.transaction_date) between '''||v_start_date||
				''' and '''||v_end_date||''') '
	||v_company_id_predicate|| '
	--
	GROUP BY fasm.source_type,
	         co.company_id '
	;
END IF
;
--DELETE rpt.rep_tab;
--commit;
EXECUTE IMMEDIATE v_sql;
--commit;
--
END
;
.
/
