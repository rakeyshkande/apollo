create or replace PROCEDURE RPT.CLEAN_COUNTS_FUTURE 
AS

CURSOR shutdown_details(in_date date) is
    SELECT fm.florist_id,
        fm.super_florist_flag,
        (select fb1.blocked_by_user_id
            from ftd_apps.florist_blocks fb1
            where fb1.florist_id (+) = fm.florist_id
            AND trunc(fb1.block_start_date) <= trunc(in_date)
            AND (fb1.block_end_date is null OR trunc(fb1.block_end_date) >= trunc(in_date))
            AND rownum = 1) blocked_by,
        (case ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, trunc(in_date), null)
            when 'Y' then (select suspend_type
                from ftd_apps.florist_suspends fs
                where fs.florist_id = fm.florist_id)
            else null
        end) suspend_type,
        to_char(in_date, 'DY') day_of_week,
        ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(in_date)) is_florist_open_for_delivery,
        ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) is_florist_open_in_eros
    FROM ftd_apps.florist_master fm
    WHERE fm.record_type = 'R'
    AND fm.status <> 'Inactive'
    AND fm.mercury_flag = 'M'
    AND (
        ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, trunc(in_date), null) = 'Y'
        OR ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, trunc(in_date), null) = 'Y'
        OR ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(in_date)) <> 'Y'
        OR ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) <> 'Y'
    );


v_days_in_future        PLS_INTEGER;
v_result_date           DATE;
v_goto_florist_count    PLS_INTEGER;
v_goto_florist_suspend  PLS_INTEGER;
v_goto_florist_shutdown PLS_INTEGER;
v_mercury_shop_count    PLS_INTEGER;
v_mercury_shop_shutdown PLS_INTEGER;
v_covered_zip_count     PLS_INTEGER;
v_no_coverage_zip_cnt   PLS_INTEGER;

v_suspend_cnt                   PLS_INTEGER;
v_eros_block_cnt                PLS_INTEGER;
v_fit_block_cnt                 PLS_INTEGER;
v_user_block_cnt                PLS_INTEGER;
v_sunday_not_codified_cnt       PLS_INTEGER;
v_goto_suspend_cnt              PLS_INTEGER;
v_goto_eros_block_cnt           PLS_INTEGER;
v_goto_fit_block_cnt            PLS_INTEGER;
v_goto_user_block_cnt           PLS_INTEGER;
v_goto_sunday_not_codified_cnt  PLS_INTEGER;

BEGIN
  
  select value into v_days_in_future from frp.global_parms where context = 'RPT' and name = 'HOLIDAY_MONITORING_FUTURE_DAYS';

  BEGIN
      SELECT COUNT(DISTINCT fz.zip_code)
          INTO v_covered_zip_count
      FROM ftd_apps.florist_zips fz
      JOIN ftd_apps.florist_master fm
      ON fz.florist_id = fm.florist_id
      AND fm.record_type = 'R'
      AND fm.status <> 'Inactive';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_covered_zip_count := 0;
  END;

  for i in 1..v_days_in_future loop
    select trunc(sysdate + i) into v_result_date from dual;
    --dbms_output.put_line('Hai :' || trunc(v_result_date));
 
    BEGIN
      SELECT COUNT(florist_id)
      INTO v_goto_florist_count
      FROM ftd_apps.florist_master
      WHERE super_florist_flag = 'Y'
      AND record_type          = 'R'
      AND status              <> 'Inactive';
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_goto_florist_count := 0;
    END;
    
    BEGIN
      SELECT COUNT(fm.florist_id)
      INTO v_goto_florist_suspend
      FROM ftd_apps.florist_master fm
      WHERE fm.record_type         = 'R'
      AND fm.status               <> 'Inactive'
      AND fm.super_florist_flag    = 'Y'
      AND (
          ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, trunc(v_result_date), null) = 'Y'
          OR ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, trunc(v_result_date), null) = 'Y'
          OR ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(v_result_date)) <> 'Y'
          OR ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) <> 'Y'
      );

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      v_goto_florist_suspend := 0;
    END;


  BEGIN
      SELECT COUNT(florist_id)
        INTO v_mercury_shop_count
        FROM ftd_apps.florist_master
       WHERE mercury_flag = 'M'
                AND status <> 'Inactive'
         AND record_type = 'R';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_mercury_shop_count := 0;
  END;

  BEGIN
      SELECT COUNT(fm.florist_id)
        INTO v_mercury_shop_shutdown
        FROM ftd_apps.florist_master fm
        WHERE fm.record_type = 'R'
        AND fm.status <> 'Inactive'
        AND fm.mercury_flag = 'M'
        AND (
            ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, trunc(v_result_date), null) = 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, trunc(v_result_date), null) = 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(v_result_date)) <> 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) <> 'Y'
        );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_mercury_shop_shutdown := 0;
  END;

  v_suspend_cnt := 0;
  v_eros_block_cnt := 0;
  v_fit_block_cnt := 0;
  v_user_block_cnt := 0;
  v_sunday_not_codified_cnt := 0;
  v_goto_suspend_cnt := 0;
  v_goto_eros_block_cnt := 0;
  v_goto_fit_block_cnt := 0;
  v_goto_user_block_cnt := 0;
  v_goto_sunday_not_codified_cnt := 0;

  FOR rec in shutdown_details(v_result_date) loop

      IF rec.suspend_type is not null and (rec.super_florist_flag is null or rec.super_florist_flag = 'N'
        OR (rec.super_florist_flag = 'Y' and rec.suspend_type = 'G')) THEN
          IF rec.super_florist_flag = 'Y' THEN
              v_goto_suspend_cnt := v_goto_suspend_cnt + 1;
          END IF;
          v_suspend_cnt := v_suspend_cnt + 1;
      ELSIF ((rec. is_florist_open_for_delivery <> 'Y') or (rec.is_florist_open_in_eros <> 'Y')) THEN
          IF rec.super_florist_flag = 'Y' THEN
              v_goto_sunday_not_codified_cnt := v_goto_sunday_not_codified_cnt + 1;
          END IF;
          v_sunday_not_codified_cnt := v_sunday_not_codified_cnt + 1;
      ELSIF rec.blocked_by is not null THEN
          IF rec.blocked_by = 'EROS' THEN
              IF rec.super_florist_flag = 'Y' THEN
                  v_goto_eros_block_cnt := v_goto_eros_block_cnt + 1;
              END IF;
              v_eros_block_cnt := v_eros_block_cnt + 1;
          ELSIF rec.blocked_by = 'FIT' THEN
              IF rec.super_florist_flag = 'Y' THEN
                  v_goto_fit_block_cnt := v_goto_fit_block_cnt + 1;
              END IF;
              v_fit_block_cnt := v_fit_block_cnt + 1;
          ELSE
              IF rec.super_florist_flag = 'Y' THEN
                  v_goto_user_block_cnt := v_goto_user_block_cnt + 1;
              END IF;
              v_user_block_cnt := v_user_block_cnt + 1;
          END IF;
      END IF;
      
  END LOOP;
  
  BEGIN
    
      SELECT count(p.zip_code_id)
          INTO v_no_coverage_zip_cnt
      FROM pas.pas_florist_zipcode_dt p
      WHERE p.delivery_date = trunc(v_result_date)
      AND p.status_code = 'N'
      AND exists (select distinct 'Y' 
          FROM ftd_apps.florist_zips fz
          JOIN ftd_apps.florist_master fm
          ON fz.florist_id = fm.florist_id
          AND fm.status <> 'Inactive'
          WHERE fz.zip_code = p.zip_code_id);
  
      EXCEPTION WHEN NO_DATA_FOUND THEN v_no_coverage_zip_cnt := 0;
  
  END;
  
  
  delete from RPT.clean_hourly_stats where trunc(clean_date) = trunc(v_result_date);

  
  insert into RPT.clean_hourly_stats (
      clean_date,
      GOTO_FLORIST_COUNT,
      GOTO_FLORIST_SUSPEND,
      MERCURY_SHOP_COUNT,
      MERCURY_SHOP_SHUTDOWN,
      COVERED_ZIP_COUNT,
      EMAIL_AUTO_HANDLE,
      suspend_cnt,
      eros_block_cnt,
      fit_block_cnt,
      user_block_cnt,
      sunday_not_codified_cnt,
      goto_suspend_cnt,
      goto_eros_block_cnt,
      goto_fit_block_cnt,
      goto_user_block_cnt,
      goto_sunday_not_codified_cnt,
	  no_coverage_zip_cnt
	  )
  values (
      v_result_date,
      v_goto_florist_count,
      v_goto_florist_suspend,
      v_mercury_shop_count,
      v_mercury_shop_shutdown,
      v_covered_zip_count,
      0,
      v_suspend_cnt,
      v_eros_block_cnt,
      v_fit_block_cnt,
      v_user_block_cnt,
      v_sunday_not_codified_cnt,
      v_goto_suspend_cnt,
      v_goto_eros_block_cnt,
      v_goto_fit_block_cnt,
      v_goto_user_block_cnt,
      v_goto_sunday_not_codified_cnt,
	  v_no_coverage_zip_cnt);

    
  end loop;

END CLEAN_COUNTS_FUTURE;

.
/