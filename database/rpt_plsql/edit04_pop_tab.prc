CREATE OR REPLACE
PROCEDURE rpt.EDIT04_POP_TAB
		(p_start_date in varchar2,
		 p_end_date in varchar2
		)
IS

v_sql		varchar2(30000);
v_start_date	date	:= to_date(p_start_date,'mm/dd/yyyy');
v_end_date 	date	:= to_date(p_end_date,'mm/dd/yyyy');

BEGIN

--
--**************************************************************
--
-- ED04 Accounting Report - Mercury Reoncilation
--
--**************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15,
	col16
	)
	WITH mOrder as	(SELECT mm.reference_number,
				mm.mercury_order_number,
				mm.filling_florist,
				mm.recipient,
				mm.delivery_date,
				mm.msg_type
			 FROM	mercury.mercury mm
			 WHERE	mm.msg_type = ''FTD''
			)
	SELECT	mm.mercury_order_number,
		cod.external_order_number,
		crm.filler_id,
		fafm.florist_name,
		cc.last_name,
		crm.amount_reconciled,
		crm.amount_to_reconcile,
		crm.recon_disp_code,
		crm.recon_message_id,
		crdcv.description,
		crm.system,
		crm.created_on,
		crm.updated_on,
		mm.recipient,
		mm.delivery_date,
		mm.filling_florist
	--
	FROM	clean.recon_messages crm
	JOIN	clean.recon_disp_code_val crdcv
		  on crdcv.recon_disp_code = crm.recon_disp_code
	LEFT
	OUTER
	JOIN	mOrder mm
		  on (replace(mm.mercury_order_number, ''-'', NULL)) = crm.message_id
	JOIN	clean.order_details cod
		  on to_char(cod.order_detail_id) = mm.reference_number
	JOIN	clean.customer cc
		  on cc.customer_id = cod.recipient_id
	JOIN	ftd_apps.florist_master fafm
		  on fafm.florist_id = crm.filler_id
	--
	WHERE	crm.system = ''Merc''
	AND	mm.msg_type = ''FTD''
	AND	crm.recon_disp_code <> ''RECONCILED''
	AND	crm.archive_indicator <> ''Y''
	--
	ORDER BY crm.message_id '
;
EXECUTE IMMEDIATE v_sql
;
END
;
.
/
