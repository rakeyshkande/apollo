CREATE OR REPLACE
PROCEDURE rpt.get_add_info (p_start_date in varchar2,
			                      p_end_date in varchar2)
IS
  --
  --------------------------------------------------------------------------
  -- This procedure selects additional billing information from the CO_BRAND
  -- table into a temporary global table BRAND_TAB.
  --
  -- One row per GUID with up to 4 additional billing information fields is
  -- created.
  --
  -- The CO_BRAND table houses one additional billing information field and
  -- value per row for a given GUID.  An order (i.e. GUID), depending on the
  -- customer source code, can require any number of additional billing
  -- information fields.  When an order is captured, a CO_BRAND row is
  -- created for each field and it's value.
  --
  -- ***********************************************************************
  --
  -- This is another attempt at documenting the get_add_info procedure and
  -- the CO_BRAND and BRAND_TAB tables:
  --
  -- The get_add_info procedure selects CO_BRAND rows within a given date
  -- range. The results are loaded into a temporaray global table BRAND_TAB;
  -- one row per corporate customer (i.e. source code) with a maximum of 4
  -- pieces of additional billing information.
  --
  -- The CO_BRAND table houses additional billing information for an order
  -- depending on the corporate customer.  A CO_BRAND row contains a single
  -- piece of additional billing information such as Purchase Order Number.
  -- A corporate customer may require several pieces of additonal billing
  -- information.
  --
  -- As a result, get_add_info re-structures the additional billing
  -- information into a horizontal format.
  -------------------------------------------------------------------------

  --------------------------------------------------
  -- Define Cursor
  --------------------------------------------------

  v_start_date 		date		:= to_date(p_start_date,'mm/dd/yyyy');
--
  v_end_date   		date 		:= to_date(p_end_date,'mm/dd/yyyy');
  CURSOR co_brand_curs  is
     SELECT DISTINCT cod.order_guid,
                     'ariba_unspsc_code' info_name,
                     cod.ariba_unspsc_code info_data
     FROM clean.order_details cod
     JOIN clean.payments cp
     ON cod.order_guid=cp.order_guid
     join clean.orders co ON cod.order_guid = co.order_guid
     WHERE cp.payment_type='IN'
     AND cod.ariba_unspsc_code is not null
     AND cod.ariba_unspsc_code <> 'empty'
     AND	(trunc(co.order_date) between v_start_date and v_end_date)
     UNION ALL
     SELECT DISTINCT cod.order_guid,
                     'ariba_po_number' info_name,
                     cod.ariba_po_number info_data
     FROM clean.order_details cod
     JOIN clean.payments cp
     ON cod.order_guid=cp.order_guid
     join clean.orders co ON cod.order_guid = co.order_guid
    WHERE cp.payment_type='IN'
     AND cod.ariba_po_number is not null
     AND cod.ariba_po_number <> 'empty'
     AND	(trunc(co.order_date) between v_start_date and v_end_date)
     UNION ALL
     SELECT DISTINCT cod.order_guid,
                     'ariba_ams_project_code' info_name,
                     cod.ariba_ams_project_code info_data
     FROM clean.order_details cod
     JOIN clean.payments cp
     ON cod.order_guid=cp.order_guid
     join clean.orders co ON cod.order_guid = co.order_guid
     WHERE cp.payment_type='IN'
     AND cod.ariba_ams_project_code is not null
     AND	(trunc(co.order_date) between v_start_date and v_end_date)
     AND cod.ariba_ams_project_code <> 'empty'
     UNION ALL
     SELECT DISTINCT cod.order_guid,
                     'ariba_cost_center' info_name,
                     cod.ariba_cost_center info_date
     FROM clean.order_details cod
     JOIN clean.payments cp
     ON cod.order_guid=cp.order_guid
     join clean.orders co ON cod.order_guid = co.order_guid
     WHERE cp.payment_type='IN'
     AND cod.ariba_cost_center is not null
     AND	(trunc(co.order_date) between v_start_date and v_end_date)
     AND cod.ariba_cost_center <> 'empty'
     UNION
     SELECT distinct ccb.order_guid,
	    ccb.info_name,
	    ccb.info_data
     --
     FROM   clean.co_brand ccb
     JOIN   clean.orders co
     		on (co.order_guid = ccb.order_guid)
     JOIN   clean.payments cp
		on (cp.order_guid = co.order_guid)
     --
     WHERE  cp.payment_type = 'IN'
      and (ccb.info_name <>'Customer Birthdate'
      and ccb.info_name<>'SCRUBBED_ON'
      and ccb.info_name<>'SCRUBBED_BY')
     AND	(trunc(co.order_date) between v_start_date and v_end_date)
      UNION
      select distinct cod.order_guid,
               code.info_name,
               code.info_data
      from clean.order_details cod
      join clean.order_detail_extensions code on cod.order_detail_id=code.order_detail_id
      join clean.orders co  ON cod.order_guid = co.order_guid
      where (code.info_name <>'Customer Birthdate'
      and code.info_name<>'SCRUBBED_ON'
      and code.info_name<>'SCRUBBED_BY')
      AND (trunc(co.order_date) between v_start_date and v_end_date)
    order by order_guid
    ;

  --------------------------------------------------
  -- Define Table Row Layout
  --------------------------------------------------
  TYPE co_brand_rec is record
    (cbr_guid   clean.orders.order_guid%type,
     cbr_field1 clean.co_brand.info_name%type,
     cbr_value1 clean.co_brand.info_data%type,
     cbr_field2 clean.co_brand.info_name%type,
     cbr_value2 clean.co_brand.info_data%type,
     cbr_field3 clean.co_brand.info_name%type,
     cbr_value3 clean.co_brand.info_data%type,
     cbr_field4 clean.co_brand.info_name%type,
     cbr_value4 clean.co_brand.info_data%type
    );

  --------------------------------------------------
  -- Define Type using Table Row Layout
  --------------------------------------------------
  Type co_brand_tab_type is table of co_brand_rec
     index by binary_integer;

  --------------------------------------------------
  -- Define Table using Type
  --------------------------------------------------
  co_brand_tab co_brand_tab_type;

  --------------------------------------------------
  -- Define Variables
  --------------------------------------------------
  v_idx number(5) := 1;
  v_val number(5) := 1;
  s_guid clean.orders.order_guid%type := 'NONE';

BEGIN
 --
 --------------------------------------------------------------------------
 -- Loop thru the "co_brand_curs" Cursor and load the "co_brand_tab" Array
 --------------------------------------------------------------------------
 --
 FOR co_brand_curs_rec in co_brand_curs LOOP
     if s_guid = 'NONE' then
        s_guid := co_brand_curs_rec.order_guid;
     end if;

     if s_guid <> co_brand_curs_rec.order_guid then
        s_guid := co_brand_curs_rec.order_guid;
        v_idx := v_idx + 1;
        v_val := 1;
     end if;

     if v_val = 1 then
        co_brand_tab(v_idx).cbr_guid   :=  co_brand_curs_rec.order_guid;
        co_brand_tab(v_idx).cbr_field1 :=  co_brand_curs_rec.info_name;
        co_brand_tab(v_idx).cbr_value1 :=  co_brand_curs_rec.info_data;
     elsif v_val = 2 then
        co_brand_tab(v_idx).cbr_field2 :=  co_brand_curs_rec.info_name;
        co_brand_tab(v_idx).cbr_value2 :=  co_brand_curs_rec.info_data;
     elsif v_val = 3 then
        co_brand_tab(v_idx).cbr_field3 :=  co_brand_curs_rec.info_name;
        co_brand_tab(v_idx).cbr_value3 :=  co_brand_curs_rec.info_data;
     elsif v_val = 4 then
        co_brand_tab(v_idx).cbr_field4 :=  co_brand_curs_rec.info_name;
        co_brand_tab(v_idx).cbr_value4 :=  co_brand_curs_rec.info_data;
     end if;

     v_val := v_val + 1;
 END LOOP;
 --
 --------------------------------------------------------------------------
 -- Load the "brand_tab" Table with the "co_brand_tab" Array
 --------------------------------------------------------------------------
 --
 --DELETE dkloster.brand_tab;
 --COMMIT;

 IF co_brand_tab.COUNT > 0 then
    FOR i in 1..v_idx LOOP
      INSERT INTO RPT.brand_tab
	 (order_guid,
	  info_name1,
	  info_data1,
	  info_name2,
	  info_data2,
	  info_name3,
	  info_data3,
	  info_name4,
	  info_data4
	 )
      VALUES
	 (co_brand_tab(i).cbr_guid,
	  co_brand_tab(i).cbr_field1,
	  co_brand_tab(i).cbr_value1,
	  co_brand_tab(i).cbr_field2,
	  co_brand_tab(i).cbr_value2,
	  co_brand_tab(i).cbr_field3,
	  co_brand_tab(i).cbr_value3,
	  co_brand_tab(i).cbr_field4,
	  co_brand_tab(i).cbr_value4
	 )
      ;
      COMMIT;
    END LOOP;

 END IF;

END;
.
/
