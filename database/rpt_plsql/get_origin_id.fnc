CREATE OR REPLACE
FUNCTION rpt.GET_ORIGIN_ID(
   pnum_order_detail_id   NUMBER)
RETURN VARCHAR2
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 05/10/2006
***              Reason : Enhancement1080: To improve performance of the report
***             Purpose : Get the origin_id for a given order_detail_id
CREATE OR REPLACE FUNCTION RPT.GET_ORIGIN_ID(
   pnum_order_detail_id   NUMBER)
RETURN VARCHAR2
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 05/10/2006
***              Reason : Enhancement1080: To improve performance of the report
***             Purpose : Get the origin_id for a given order_detail_id
***
***    Input Parameters : pnum_order_detail_id - order_detail_id
***
***             Returns : Origin_id for a given order_detail_id
***
*** Special Instructions:
***
*****************************************************************************************/

   v_origin_id   varchar2(200) := NULL;

BEGIN

   SELECT
      co.origin_id
   INTO
      v_origin_id
   FROM
      clean.order_details cod,
      clean.orders co
   WHERE
      cod.order_detail_id = pnum_order_detail_id
      AND co.order_guid = cod.order_guid;

   RETURN v_origin_id;

EXCEPTION
   WHEN OTHERS THEN
      RETURN NULL;
END;
.
/
