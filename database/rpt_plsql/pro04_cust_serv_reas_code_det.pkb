CREATE OR REPLACE
PACKAGE BODY rpt.PRO04_Cust_Serv_Reas_Code_Det IS
--
  count_v       	number(9)  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure p( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
	rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     --	when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end p;

--****************************************************************************************************
-- This is the column headings for the Report
--****************************************************************************************************
  procedure print_headings(y in number)
  is
  begin
    p( 'F;R'||y||';FG0C;SM2' );
    p( 'C;Y'||y||';X2;K" USER"' );
    p( 'C;Y'||y||';X4;K" DATE"' );
    p( 'C;Y'||y||';X5;K" REASON CODE"' );
    p( 'C;Y'||y||';X6;K" # OF CALLS"' );
    p( 'F;R'||to_char(y+1)||';FG0C;SM2' );
    p( 'C;Y'||to_char(y+1)||';X6;K" TAKEN"' );
    p( 'C;Y'||y||';X7;K" AVG HANDLE"' );
    p( 'C;Y'||to_char(y+1)||';X7;K" TIME"' );
  end print_headings;
--*******************************************************************************************
-- This sets up the Formats that will be used in the SLK file.  It also prints a title
-- for our report.
--*******************************************************************************************
  procedure     print_title(  p_start_date in date,
                              p_end_date   in date)
  is
    l_title varchar2(2000);
  begin
    p( 'ID;ORACLE' );
    p( 'P;Ph:mm:ss');
    p( 'P;FCourier;M200' );
    p( 'P;FCourier;M200;SB' );
    p( 'P;FCourier;M200;SUB' );
    p( 'P;FCourier;M160' );
    p( 'F;C1;FG0R;SM1');
    p( 'F;R1;FG0C;SM2' );
    p( 'C;Y1;X5;K" CUSTOMER SERVICE CALL REASON DETAIL REPORT FOR ' || to_char(p_start_date,'mm/dd/yyyy') || ' THRU ' || to_char(p_end_date,'mm/dd/yyyy') || '"' );
    if g_CLOB = 'N' then
	utl_file.fflush( g_file );
    end if;
  end print_title;

--**********************************************************************************************************
-- This will Define the Column Widths for the SLK File
--**********************************************************************************************************
  procedure print_widths is
  begin
    p( 'F;W1 1 2' );     -- margin
    p( 'F;W2 2 20' );    -- Center, team, csr last
    p( 'F;W3 3 20' );    -- first name
    p( 'F;W4 4 10' );		 -- order date
    p( 'F;W5 5 20' );    -- reason code
    p( 'F;W6 6 10' );     -- number calls
    p( 'F;W7 7 15' );    -- average handle time
    if g_CLOB = 'N' then
	utl_file.fflush( g_file );
    end if;
  end print_widths;
--**********************************************************************************************************
-- This will Print the subtotals for a date change.  This will accept the starting and ending point of
-- the range of cells that will be summed.
--**********************************************************************************************************
  procedure sub_totals_for_dates (p_y in number,
                                  p_date_y in number) is
  line varchar2(300);
  BEGIN
           line := 'C;Y'|| to_char(p_y) || ';X6';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'F;P0;FG0R'||to_char(p_y)||';X7';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X7';
           line := line || ';EAVERAGE(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
   END sub_totals_for_dates;
--*************************************************************************************************************
-- This will print a subtotal when the team or center is changed. It accepts the row this subtotal is printed
-- as well as a string of all the cells that will be summed.
--*************************************************************************************************************
procedure sub_totals_for_team_or_center (p_y in number,
                                         p_cell_totals in varchar2)
            is
             line varchar2(300);
            BEGIN
             line := 'C;Y'|| to_char(p_y) || ';X6';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'F;P0;FG0R'||to_char(p_y)||';X7';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X7';
             line := line || ';EAVERAGE('||p_cell_totals||')';
             p( line );
end sub_totals_for_team_or_center;


--********************************************************************************
--********************************************************************************
--*
--* This is the MAIN routine PRINT_ROWS
--*
--********************************************************************************
--********************************************************************************

function print_rows(p_start_date in date,
                    p_end_date   in date,
                    p_center_name in varchar2,
                    p_team_name   in varchar2,
                    p_reason_code  in varchar2,
                    p_user_id in varchar2 ) return number is
  type    my_curs_type is REF CURSOR;-- This is must be a weak REF CURSOR (No Returning clause)
  curs    my_curs_type;-- This allows us to define the SQL Statement for the CURSOR Dynamically
                          -- which was required because the users wanted to be able to pass multiple
                          -- parameter values.  It's also more efficient if they pass a null value
  TYPE    date_total_tab_type is table of varchar2(10)
        index by pls_integer;
  date_total_tab date_total_tab_type; -- Keeps track of the cells that contain totals for each date changes
                                    -- Will be used for total of each team
  team_total_tab date_total_tab_type; -- Keeps track of the cells that contain totals for each Team changes
                                    -- Will be used for total of each team
  null_tab       date_total_tab_type;  -- This is used to clear the tab
  i       number(4) := 1;             -- Index for date_total_tab
  t       number(4) := 1;             -- Index for team_total_tab
  date_y number := 4;                 -- Determines the start range for totaling Date totals
  center_y number := 4;
  v_old_team   ftd_apps.cs_groups.description%type  := 'XXXX';-- Used to determine when a team changes
  v_old_date   varchar2(15) := '01-jan-1900';-- Used to determine when a Date changes
  v_old_center ftd_apps.call_center.description%type:= 'XXXX';-- Used to determine when a Date changes
  str     varchar2(5000); -- This holds the Dynamic Query that will be executed.
  type ret_rec is record -- When we fetch we will fetch into a record of this structure
              (center_name  varchar2(80),
              team_name varchar2(80),
       	      csr_first_name varchar2(80),
          	  csr_last_name varchar2(80),
             	order_date varchar2(80),
      	      reason_code varchar2(80),
        	    number_calls number,
              avg_handle_time number);
  ret     ret_rec; -- Based on the record type defined above
  v_user_id_pred varchar2(300);-- Used to create a where clause condition for user id.
  v_center_name_pred varchar2(300);-- Used to create a where clause condition for center.
  v_team_name_pred varchar2(300);-- Used to create a where clause condition for team.
  v_reason_code_pred varchar2(300); -- Used to create a where clause condition for reason.
  row_cnt number          := 0;
  line    varchar2(32767) := null;
  n       number;
  y       number;

--**************************************************************************************
--*
--* Begin the processing
--*
--**************************************************************************************

begin

--*****************************************************************
--* Build the WHERE clause predicates for our select statements.
--* It allows the user to pass in multiple values for Team, Center,
--* User_id, Reason Code.  If the parameter value is null it will
--* not include the predicate.
--*****************************************************************
if p_center_name is null
or p_center_name = 'ALL' then
   v_center_name_pred := null;
else
   --v_center_name_pred:= ' and upper(facc.description) in ('||upper(multi_value(p_team_name))||') ';
   v_center_name_pred:= ' and upper(facc.call_center_id) in ('||upper(multi_value(p_team_name))||') ';
end if;

if p_team_name is null
or p_team_name = 'ALL' then
   v_team_name_pred := null;
else
   --v_team_name_pred:= ' and upper(facg.description) in ('||upper(multi_value(p_team_name))||') ';
   v_team_name_pred:= ' and upper(facg.cs_group_id) in ('||upper(multi_value(p_team_name))||') ';
end if;

if p_user_id is null then
   v_user_id_pred := null;
else
   v_user_id_pred:= ' and upper(ccl.csr_id) in ('||upper(multi_value(p_user_id))||') ';
end if;

if p_reason_code is null
or p_reason_code = 'ALL' then
   v_reason_code_pred := null;
else
   --v_reason_code_pred := ' and upper(ccrv.call_reason_code) in  ('||upper(multi_value(p_reason_code))||') ';
   v_reason_code_pred := ' and upper(ccrv.description) in  ('||upper(multi_value(p_reason_code))||') ';
end if;

--*****************************************************************
--* Build the SELECT statement dynamically.
--*****************************************************************
str := 'select nvl(facc.description, ''No Center'')  center_name,
               nvl(facg.description, ''No Team'')  team_name,
               nvl(au.first_name, '' '')     csr_first_name,
               nvl(au.last_name, '' '')      csr_last_name,
               nvl(to_char(ccl.start_time, ''mm/dd/yyyy''), '' '') order_date,
               nvl(to_char(ccrv.description), '' '')               reason_code,
               count(ccl.call_log_id)        number_calls,
               avg(ccl.end_time-ccl.start_time)                    avg_handle_time
from      clean.call_log ccl
left  outer join         clean.call_reason_val ccrv
on (ccl.call_reason_code=ccrv.call_reason_code)
left outer join   aas.identity ai
on (ccl.csr_id          =ai.identity_id)
left outer JOIN aas.user_history auh
on (ai.user_id = auh.user_id)
left outer join   aas.users au
on (auh.user_id          =au.user_id)
left outer join   ftd_apps.call_center facc
on (au.call_center_id   =facc.call_center_id)
left outer join          ftd_apps.cs_groups facg
on (au.cs_group_id      =facg.cs_group_id)
WHERE trunc(ccl.start_time) between to_date('''||p_start_date||''',''dd-MON-rr'') and
                                    to_date('''||p_end_date||''',''dd-MON-rr'')
and   trunc(ccl.start_time) between auh.start_date and auh.end_date '
|| v_user_id_pred
|| v_center_name_pred
|| v_team_name_pred
|| v_reason_code_pred
||' group by  nvl(facc.description, ''No Center''),
          nvl(facg.description, ''No Team''),
          nvl(au.first_name, '' ''),
          nvl(au.last_name, '' ''),
          nvl(to_char(ccl.start_time, ''mm/dd/yyyy''), '' ''),
         nvl(to_char(ccrv.description), '' '')
order by nvl(facc.description, ''No Center''),
          nvl(facg.description, ''No Team''),
          nvl(to_char(ccl.start_time, ''mm/dd/yyyy''), '' '')';


--*****************************************************************
--* Begin the loop processing
--*****************************************************************
--dbms_output.put_line(substr(str,-765,255));
    y := 4;

--*************************************************************************
-- Use a REFERENCE CURSOR because the SELECT statement has dynamic
-- variables which can not be done with a STATIC CURSOR.
--*************************************************************************

      OPEN curs for str;
      LOOP
      	FETCH curs  into ret;
      	exit when curs%notfound;
        --*****************************************************************
        --* This will break if the Date Changes after the 1st row.  It will
	--* print the subtotals and enter the y coordinate where this total
	--* was calculated.  This will be used when the team changes.
        --*****************************************************************

	if ((v_old_date <> ret.order_date) or (v_old_team <> ret.team_name) or (v_old_center<>ret.center_name))
		and y > 4 then
	   p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
           p( line );
           sub_totals_for_dates (y,date_y);
           date_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
      	   y := y+1;
      	   date_y := y;

	   if (v_old_team <> ret.team_name or v_old_center <> ret.center_name) then
		DECLARE
      		 	v_cell_totals  varchar2(3000);
      	   	BEGIN
      	     		FOR j in date_total_tab.first..date_total_tab.last loop
			-- Builds a list of all the rows that has a subtotal for all
			-- the breaks on Date
			v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	     		END LOOP;
      	     		v_cell_totals := rtrim(v_cell_totals,',');
	     		p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
             		line := 'C;Y'|| to_char(y) || ';X2';
             		line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
             		p( line );
             		sub_totals_for_team_or_center (y,v_cell_totals);

	     		--if v_old_center = ret.center_name then
             		date_total_tab := null_tab;
	     		--end if;

             		team_total_tab(t) := 'R'||to_char(y)||'C';
             		t := t+1;
             		i := i+1;
      	     		y:=y+1;
      	     		date_y := y;
      	   	END;
	   end if;

	   if v_old_center <> ret.center_name then
	   	--  Print sub totals for center
      	   	DECLARE
      		 	v_cell_totals  varchar2(300);
      	   	BEGIN
      	     		FOR j in team_total_tab.first..team_total_tab.last loop
      	   	  		v_cell_totals :=  v_cell_totals||team_total_tab(j)||',';
      	     		END LOOP;
           		v_cell_totals := rtrim(v_cell_totals,',');
        	 	p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
           		line := 'C;Y'|| to_char(y) || ';X2';
           		line := line || ';K"Totals for Center '||v_old_center||' on '||p_start_date||' and '||p_end_date||'"';
           		p( line );
           		sub_totals_for_team_or_center (y,v_cell_totals);
           		team_total_tab := null_tab;
      	   		y:=y+1;
      	   		p( 'F;R'||to_char(y)||';FG0L;SM2' );
           		line := 'C;Y'|| to_char(y) || ';X2';
           		line := line || ';K"'||ret.center_name||'"';
           		p( line );
      	   		y:=y+1;
      	   		p( 'F;R'||to_char(y)||';FG0C;SM2' );
          		line := 'C;Y'|| to_char(y) || ';X2';
            		line := line || ';K"'||ret.team_name||'"';
            		p( line );
      	   		y:=y+1;
      	   		center_y := y;
      	   		date_y := y;
      	   		p( 'F;R'||to_char(y)||';FG0L;SM3' );
      	   	end;
	   end if;

      	end if;

        --*****************************************************************
        --* This will break if the Team Changes after the 1st row but the
	--* date doesn't. It will print the subtotals and enter the y
	--* coordinate where this total was calculated.  This will be used
	--* when the Center changes.
        --*****************************************************************
/**
      	if ((v_old_team <> ret.team_name and v_old_date = ret.order_date) or v_old_center<>ret.center_name)
		and y > 4 then
	   --  Print sub totals for team when just team changed
           p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
           p( line );
           sub_totals_for_dates (y,date_y);
           date_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
      	   y:=y+1;
	   -- print total for team
      	   DECLARE
      		 v_cell_totals  varchar2(3000);
      	   BEGIN
      	     FOR j in date_total_tab.first..date_total_tab.last loop
		-- Builds a list of all the rows that has a subtotal for all
		-- the breaks on Date
		v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	     END LOOP;
      	     v_cell_totals := rtrim(v_cell_totals,',');
	     p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
             p( line );
             sub_totals_for_team_or_center (y,v_cell_totals);

	     --if v_old_center = ret.center_name then
             	date_total_tab := null_tab;
	     --end if;

             team_total_tab(t) := 'R'||to_char(y)||'C';
             t := t+1;
             i := i+1;
      	     y:=y+1;
      	     date_y := y;
      	   END;
	   --*****************************************************************
	   --*
	   --*****************************************************************
	   if v_old_center = ret.center_name then
		p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
		line := 'C;Y'|| to_char(y) || ';X2';
		line := line || ';K"'||ret.team_name||'"';
		p( line );
		y:=y+1;
	   end if;
      	end if;

        --*****************************************************************
        --*
        --*****************************************************************
      	if ((v_old_team <> ret.team_name and v_old_date <> ret.order_date)or v_old_center<>ret.center_name)
		and y > 4 then
	   -- print total for team when team and date changed
      	   DECLARE
      		 v_cell_totals  varchar2(3000);
      	   BEGIN
      	     FOR j in date_total_tab.first..date_total_tab.last loop

      	   	  v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	     END LOOP;
      	     v_cell_totals := rtrim(v_cell_totals,',');
          	 p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
             p( line );
             sub_totals_for_team_or_center (y,v_cell_totals);
             date_total_tab := null_tab;
             team_total_tab(t) := 'R'||to_char(y)||'C';
             t := t+1;
             i := i+1;
      	     y:=y+1;
      	     date_y := y;
      	   END;
	   --*****************************************************************
	   --*
	   --*****************************************************************
	   if v_old_center = ret.center_name then
		p( 'F;R'||to_char(y)||';FG0C;SM2' );
		line := 'C;Y'|| to_char(y) || ';X2';
		line := line || ';K"'||ret.team_name||'"';
		p( line );
		y:=y+1;
	   end if;
      	end if;

        --*****************************************************************
        --*
        --*****************************************************************
       	if v_old_center <> ret.center_name
		and y = 4 then
      	   p( 'F;R'||to_char(y)||';FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"'||ret.center_name||'"';
           p( line );
      	   y:=y+1;
      	   p( 'F;R'||to_char(y)||';FG0C;SM2' );
          	line := 'C;Y'|| to_char(y) || ';X2';
            line := line || ';K"'||ret.team_name||'"';
            p( line );
      	   y:=y+1;
      	   center_y := y;
      	elsif v_old_center <> ret.center_name and y > 4 then
	   --  Print sub totals for center
      	   DECLARE
      		 v_cell_totals  varchar2(300);
      	   BEGIN
      	     FOR j in team_total_tab.first..team_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||team_total_tab(j)||',';
      	     END LOOP;
           v_cell_totals := rtrim(v_cell_totals,',');
        	 p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"Totals for Center '||v_old_center||' on '||p_start_date||' and '||p_end_date||'"';
           p( line );
           sub_totals_for_team_or_center (y,v_cell_totals);
           team_total_tab := null_tab;
      	   y:=y+1;
      	   p( 'F;R'||to_char(y)||';FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"'||ret.center_name||'"';
           p( line );
      	   y:=y+1;
      	   p( 'F;R'||to_char(y)||';FG0C;SM2' );
          	line := 'C;Y'|| to_char(y) || ';X2';
            line := line || ';K"'||ret.team_name||'"';
            p( line );
      	   y:=y+1;
      	   center_y := y;
      	   date_y := y;
      	   p( 'F;R'||to_char(y)||';FG0L;SM3' );
      	   end;
      	end if;

**/

      	p( 'F;R'||y||';FG0L;SM3' );

        line := 'C;Y'|| to_char(y) || ';X2';
        line := line || ';K"'||ret.csr_last_name||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X3';
        line := line || ';K"'||ret.csr_first_name||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X4';
        line := line || ';K"'||ret.order_date||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X5';
        line := line || ';K"'||ret.reason_code||'"';
        p( line );

      	p( 'F;Y'||y||';X6;FG0R;SM3' );
        line := 'C;Y'|| to_char(y) || ';X6';
        line := line || ';K'||ret.number_calls;
        p( line );

      	p( 'F;Y'||y||';X7;FG0R;SM3' );
         line := 'F;P0;FG0G'||to_char(y)||';X7';
        p( line );

        line := 'C;K'||ret.avg_handle_time;
        p( line );

        y := y + 1;
        p( 'C;Y'||to_char(y) );
	if g_CLOB = 'N' then
	   	utl_file.fflush( g_file );
	end if;

        v_old_team := ret.team_name;
        v_old_center := ret.center_name;
        v_old_date  := ret.order_date;

      end loop;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************


if y = 4 then
      line := 'C;Y'|| to_char(y) || ';X2';
      line := line || ';K"No Data Found"';
      p( line );

      y := y + 1;
      p( 'C;Y'||to_char(y) );

  else

	--  Print sub totals for last date
	p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
        line := 'C;Y'|| to_char(y) || ';X2';
        line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
        p( line );
        sub_totals_for_dates (y,date_y);
        date_total_tab(i) := 'R'||to_char(y)||'C';
        i := i+1;
      	y:=y+1;
      	date_y := y;

	-- Print totals for last Team
      	DECLARE
      		 v_cell_totals  varchar2(300);
	BEGIN
      	   	 --dbms_output.put_line(date_total_tab.count);
      	     FOR j in date_total_tab.first..date_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	   	 --dbms_output.put_line( v_cell_totals);
      	     END LOOP;
      	     v_cell_totals := rtrim(v_cell_totals,',');
      	     --dbms_output.put_line( v_cell_totals);
           	 p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
             p( line );
             sub_totals_for_team_or_center (y,v_cell_totals);
             date_total_tab := null_tab;
             team_total_tab(t) := 'R'||to_char(y)||'C';
             t := t+1;
             i := i+1;
      	     y:=y+1;
      	     date_y := y;
	END;

	-- Print total for last Center
	DECLARE
      		 v_cell_totals  varchar2(300);
	BEGIN
      	   	 --dbms_output.put_line(date_total_tab.count);
      	     FOR j in team_total_tab.first..team_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||team_total_tab(j)||',';
      	   	 --dbms_output.put_line( v_cell_totals);
      	     END LOOP;
           v_cell_totals := rtrim(v_cell_totals,',');
        	 p( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"Totals for Center '||ret.center_name||' on '||p_start_date||' and '||p_end_date||'"';
           p( line );
           sub_totals_for_team_or_center (y,v_cell_totals);
           team_total_tab := null_tab;
      	   y:=y+1;
      	   center_y := y;
      	   date_y := y;
      	   p( 'F;R'||to_char(y)||';FG0L;SM3' );
	END;

  end if;

close curs;

return(y);

end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
  procedure show(p_start_date in varchar2,
                    p_end_date in varchar2,
                    p_center_name in varchar2,
                    p_team_name in varchar2,
                    p_reason_code in varchar2,
                    p_user_id in varchar2,
                    p_submission_id in number,
		    p_CLOB in varchar2
		)
  is
   v_start_date date;
   v_end_date date;
   l_row_cnt number(9);
   v_OUT_STATUS_det	varchar2(30);
   v_OUT_MESSAGE_det	varchar2(1000);
   v_OUT_STATUS_sub 	varchar2(30);
   v_OUT_MESSAGE_sub	varchar2(1000);

begin

--
-- Prepare for either CLOB output or REPORT_DIR output
--
   v_start_date := to_date(p_start_date, 'mm/dd/yyyy');
   v_end_date := to_date(p_end_date, 'mm/dd/yyyy');

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
			P_SUBMISSION_ID,
			'SYS',
			'S' ,
			null ,
			v_OUT_STATUS_DET,
			v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
       			(
			P_SUBMISSION_ID,
			empty_clob(),
			v_out_status_det,
			v_out_message_det
		        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','PRO04_Cust_Serv_Reas_Code_Det.slk', 'W' );
   end if;

    print_title(  v_start_date,
                  v_end_date);

    print_headings(2);

    l_row_cnt := print_rows(v_start_date,
                    v_end_date,
                    p_center_name,
                    p_team_name,
                    p_reason_code,
                    p_user_id);
    print_widths;
    p( 'E' );

   --
   -- Update Database with CLOB and set report submission status (if CLOB'ing)
   -- Close report file (if not CLOB'ing
   --

      if p_CLOB = 'Y' then
         begin
   	-- update the report_detail_char table with the package global variable g_clob
   	rpt.report_clob_pkg.update_report_detail_char
   			(
   			 p_submission_id,
   			 v_out_status_det,
   			 v_out_message_det
   			);
   	if v_out_status_det = 'N' then
   	-- if db update failure, update report_submission_log with ERROR status
   	rpt.report_pkg.update_report_submission_log
   			(
   			 p_submission_id,
   			 'REPORT',
   			 'E',
   			 v_out_message_det,
   			 v_out_status_sub,
   			 v_out_message_sub
   			);
   	else
   	-- if db update successful, update report_submission_log with COMPLETE status
   	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
   			(
   			 P_SUBMISSION_ID,
   			 'SYS',
   			 'C' ,
   			 null ,
   			 v_out_status_det ,
   			 v_out_message_det
   			);
   	end if;
   	--exception
   	--	when others then srw.message(32321,'Error in p');
        end;
        commit;
      else
        utl_file.fflush( g_file );
        utl_file.fclose( g_file );
   end if;

  end show;

end;
.
/
