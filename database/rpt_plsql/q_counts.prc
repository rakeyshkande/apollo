CREATE OR REPLACE
PROCEDURE rpt.q_counts
(
 IN_DATE      IN DATE,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table clean_hourly_stats.

Input:
        in_date         DATE
        in_hour         INTEGER

Output:
        status          VARCHAR2
        message         VARCHAR2

-----------------------------------------------------------------------------*/

v_orders_taken          INTEGER;
v_goto_florist_count    INTEGER;
v_goto_florist_suspend  INTEGER;
v_goto_florist_shutdown INTEGER;
v_mercury_shop_count    INTEGER;
v_mercury_shop_shutdown INTEGER;
v_covered_zip_count     INTEGER;
v_gnadd_zip_count       INTEGER;
v_ftd_queue_end         INTEGER;
v_ftd_queue_incoming    INTEGER;
v_ftd_queue_tagged      INTEGER;
v_ftdm_queue_end        INTEGER;
v_ftdm_queue_incoming   INTEGER;
v_ftdm_queue_tagged     INTEGER;
v_ask_queue_end         INTEGER;
v_ask_queue_incoming    INTEGER;
v_ask_queue_tagged      INTEGER;
v_reject_queue_end      INTEGER;
v_reject_queue_incoming INTEGER;
v_reject_queue_tagged   INTEGER;
v_reject_queue_auto     INTEGER;
v_email_queue_end       INTEGER;
v_email_queue_incoming  INTEGER;
v_email_queue_tagged    INTEGER;
v_email_worked          INTEGER;
v_merc_queue_end         INTEGER;
v_merc_queue_incoming    INTEGER;
v_merc_queue_tagged      INTEGER;


BEGIN

  BEGIN
      SELECT COUNT(od.order_detail_id)
        INTO v_orders_taken
        FROM clean.order_details od,
             clean.orders o
       WHERE od.created_on >= TRUNC(in_date)
         AND od.created_on < TRUNC(in_date+1)
         AND od.order_disp_code NOT IN ('In-Scrub', 'Removed', 'Pending')
         AND o.order_guid = od.order_guid
         AND o.origin_id <> 'test';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_orders_taken := 0;
  END;

/* Mercury Queue Counts */

BEGIN
      SELECT COUNT(message_id)
       INTO v_merc_queue_end
               FROM clean.queue_delete_history
                      WHERE  queue_created_on < TRUNC(in_date)
                      AND       queue_deleted_on >= TRUNC(in_date+1)
          AND mercury_number  is not null;
      EXCEPTION WHEN NO_DATA_FOUND THEN v_merc_queue_end := 0;
  END;


BEGIN
      SELECT COUNT(message_id)
        INTO v_merc_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= TRUNC(in_date)
                 AND created_on < TRUNC(in_date+1)
                 AND mercury_id is not null
           UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= TRUNC(in_date)
                 AND queue_created_on < TRUNC(in_date + 1)
                 AND mercury_number is not null
              	);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_merc_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_merc_queue_tagged
        FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
                  AND mercury_number is not null;

      EXCEPTION WHEN NO_DATA_FOUND THEN v_merc_queue_tagged := 0;
  END;


/* FTD Queue Counts */

BEGIN
      SELECT COUNT(message_id)
       INTO v_ftd_queue_end
               FROM clean.queue_delete_history
                      WHERE  queue_created_on < TRUNC(in_date)
                      AND       queue_deleted_on >= TRUNC(in_date+1)
                    AND message_type = 'FTD';
      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftd_queue_end := 0;
  END;


BEGIN
      SELECT COUNT(message_id)
        INTO v_ftd_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= TRUNC(in_date)
                 AND created_on < TRUNC(in_date+1)
          AND message_type = 'FTD'
           UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= TRUNC(in_date)
                 AND queue_created_on < TRUNC(in_date + 1)
          AND message_type = 'FTD'
              	);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftd_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ftd_queue_tagged
        FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
                                            AND message_type = 'FTD';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftd_queue_tagged := 0;
  END;

  /*  FTDM Queue Counts */


BEGIN
      SELECT COUNT(message_id)
       INTO v_ftdm_queue_end
               FROM clean.queue_delete_history
                      WHERE  queue_created_on < TRUNC(in_date)
                      AND       queue_deleted_on >= TRUNC(in_date+1)
                    AND message_type = 'FTDM';
      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftdm_queue_end := 0;
  END;


BEGIN
      SELECT COUNT(message_id)
        INTO v_ftdm_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= TRUNC(in_date)
                 AND created_on < TRUNC(in_date+1)
          AND message_type = 'FTDM'
           UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= TRUNC(in_date)
                 AND queue_created_on < TRUNC(in_date + 1)
          AND message_type = 'FTDM'
              	);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftdm_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ftdm_queue_tagged
        FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
          AND message_type = 'FTDM';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftdm_queue_tagged := 0;
  END;
/* ASK */
BEGIN
      SELECT COUNT(message_id)
       INTO v_ask_queue_end
               FROM clean.queue_delete_history
                      WHERE  queue_created_on < TRUNC(in_date)
                      AND       queue_deleted_on >= TRUNC(in_date+1)
                    AND message_type = 'ASK';
      EXCEPTION WHEN NO_DATA_FOUND THEN v_ask_queue_end := 0;
  END;


BEGIN
      SELECT COUNT(message_id)
        INTO v_ask_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= TRUNC(in_date)
                 AND created_on < TRUNC(in_date+1)
          AND message_type = 'FTDM'
           UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= TRUNC(in_date)
                 AND queue_created_on < TRUNC(in_date + 1)
          AND message_type = 'ASK'
              	);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ask_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ask_queue_tagged
        FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
          AND message_type = 'ASK';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ask_queue_tagged := 0;
  END;

/* Rejext */

BEGIN
      SELECT COUNT(message_id)
       INTO v_reject_queue_end
               FROM clean.queue_delete_history
                      WHERE  queue_created_on < TRUNC(in_date)
                      AND       queue_deleted_on >= TRUNC(in_date+1)
                    AND message_type = 'REJ';
      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_end := 0;
  END;


BEGIN
      SELECT COUNT(message_id)
        INTO v_reject_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= TRUNC(in_date)
                 AND created_on < TRUNC(in_date+1)
          AND message_type = 'REJ'
           UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= TRUNC(in_date)
                 AND queue_created_on < TRUNC(in_date + 1)
          AND message_type = 'REJ'
              	);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_reject_queue_tagged
        FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
          AND message_type = 'REJ';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_tagged := 0;
  END;

/* Email */
  BEGIN
      SELECT COUNT(message_id)
        INTO v_email_queue_end
        FROM clean.queue_delete_history
	                      WHERE  queue_created_on < TRUNC(in_date)
	                      AND       queue_deleted_on >= TRUNC(in_date+1)
              AND message_type IN (SELECT queue_type
                                FROM clean.queue_type_val
                               WHERE queue_indicator = 'Email');

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(point_of_contact_id)
        INTO v_email_queue_incoming
        FROM clean.point_of_contact
       WHERE point_of_contact_type = 'Email'
         AND sent_received_indicator = 'I'
         AND created_on >= (TRUNC(in_date))
         AND created_on < (TRUNC(in_date+1));

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_email_queue_tagged
        FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
         AND queue_type IN (SELECT queue_type
                                    FROM clean.queue_type_val
                                   WHERE queue_indicator = 'Email');

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_queue_tagged := 0;
  END;




  BEGIN
      SELECT COUNT(*)
        INTO v_email_worked
            FROM clean.queue_delete_history qdh
                  WHERE  tagged_on >= TRUNC(in_date)
                      AND       tagged_on < TRUNC(in_date+1)
                   AND queue_type IN (SELECT queue_type
                              FROM clean.queue_type_val
                             WHERE queue_indicator = 'Email');

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_worked := 0;
  END;



  INSERT INTO rpt.q_stats
       (clean_date,
        orders_taken,
        goto_florist_count,
        goto_florist_suspend,
        goto_florist_shutdown,
        mercury_shop_count,
        mercury_shop_shutdown,
        covered_zip_count,
        gnadd_zip_count,
        ftd_queue_end,
        ftd_queue_incoming,
        ftd_queue_tagged,
        ftdm_queue_end,
        ftdm_queue_incoming,
        ftdm_queue_tagged,
        ask_queue_end,
        ask_queue_incoming,
        ask_queue_tagged,
        reject_queue_end,
        reject_queue_incoming,
        reject_queue_tagged,
        reject_queue_auto,
        email_queue_end,
        email_queue_incoming,
        email_queue_tagged,
        email_worked,
        merc_queue_end,
	merc_queue_incoming,
	merc_queue_tagged
       )
      VALUES
       (     TRUNC(in_date) ,
        v_orders_taken,
        v_goto_florist_count,
        v_goto_florist_suspend,
        v_goto_florist_shutdown,
        v_mercury_shop_count,
        v_mercury_shop_shutdown,
        v_covered_zip_count,
        v_gnadd_zip_count,
        v_ftd_queue_end,
        v_ftd_queue_incoming,
        v_ftd_queue_tagged,
        v_ftdm_queue_end,
        v_ftdm_queue_incoming,
        v_ftdm_queue_tagged,
        v_ask_queue_end,
        v_ask_queue_incoming,
        v_ask_queue_tagged,
        v_reject_queue_end,
        v_reject_queue_incoming,
        v_reject_queue_tagged,
        v_reject_queue_auto,
        v_email_queue_end,
        v_email_queue_incoming,
        v_email_queue_tagged,
        v_email_worked,
        v_merc_queue_end,
	v_merc_queue_incoming,
	v_merc_queue_tagged
         );

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;


END q_counts;
.
/
