CREATE OR REPLACE FUNCTION rpt.get_pay_vendor_amount (
	in_order_detail_id			venus.venus.reference_number%TYPE)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure gets the amount paid to the vendor. ADJ's have not
        been considered. If we have ADJ's then we might have to subtract the 
        price on the ADJ from the price on the FTD record.

Input:
        in_date         DATE
        in_hour         PLS_INTEGER

Output:
        status          VARCHAR2
        message         VARCHAR2

-----------------------------------------------------------------------------*/
	v_price			venus.venus.price%TYPE := 0;
	
BEGIN

	SELECT vv.price
	  INTO v_price
	  FROM VENUS.VENUS vv
	 WHERE vv.reference_number = in_order_detail_id 
	   AND (reference_number, created_on) in (
	       (SELECT reference_number,MAX(created_on) created_on
			    FROM VENUS.VENUS vv3
				WHERE vv3.venus_order_number IS NOT NULL
				  AND vv3.reference_number IS NOT NULL
				  AND vv3.price IS NOT NULL
				  AND vv3.price <> 0
				  AND vv3.msg_type ='FTD'
				  AND vv3.reference_number = vv.reference_number
			   GROUP BY reference_number));
			   
	RETURN v_price;			   
			   
EXCEPTION
   WHEN OTHERS THEN
      RETURN 0;
END;
.
/