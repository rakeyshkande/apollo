CREATE OR REPLACE
PROCEDURE rpt.MER03_Drop_Ship_Track_Pop_Tab
		(p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_date_ind in varchar2,
		 p_tracking_ind in varchar2
		)
IS
  v_start_date		 date		:= to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		 date		:= to_date(p_end_date,'mm/dd/yyyy');
  v_sql			 varchar2(5000)	:= NULL;
  v_tracking_predicate   varchar2(3000) := null;
  v_date_predicate       varchar2(3000) := null;
  v_date_order           varchar2(35)   := Null;
BEGIN
  --
  --**********************************************************************
  -- Drop Ship Tracking Number Query
  --**********************************************************************
  --
  if p_tracking_ind = 'Y' then
     v_tracking_predicate := ' and cot.tracking_number is not null ';
  elsif p_tracking_ind = 'N' then
     v_tracking_predicate := ' and cot.tracking_number is null ';
  else
     v_tracking_predicate := null;
  end if;
  if p_date_ind = 'D' then
      v_date_predicate := 	' AND	trunc(cod.delivery_date) between '''
		|| v_start_date || ''' and ''' || v_end_date || '''';
      v_date_order := ' cod.delivery_date ';
  ELSE
      v_date_predicate := 	' AND	decode(vv.zone_jump_label_date, NULL, trunc(cod.ship_date), trunc(vv.zone_jump_label_date)) between '''
		|| v_start_date || ''' and ''' || v_end_date || '''';
      v_date_order := ' cod.ship_date ';
  END if;
     v_sql :=
     'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14
	)
	SELECT	''1'',
		to_char(co.order_date, ''mm/dd/yyyy'')		order_date,
		to_char(cod.ship_date, ''mm/dd/yyyy'')		ship_date,
		to_char(cod.delivery_date, ''mm/dd/yyyy'')	delivery_date,
		to_char(cod.eod_delivery_date, ''mm/dd/yyyy'')	eod_delivery_date,
		cod.external_order_number			order_number,
		vv.product_id                   		product_id,
		--vcd.ship_method					ship_method,
		cod.ship_method					ship_method,
		cot.tracking_number				tracking_number,
		to_char(vv.updated_on, ''Fmhh:Fmmi:ss'')	updated_on,
		vv.venus_order_number				mercury_number,
		favm.vendor_name				vendor_name,
		NVL(vcs.carrier_name, DECODE(vv.shipping_system, ''SDS'', ''None-Selected'', ''Unknown''))	carrier_name,
		to_char(vv.zone_jump_label_date, ''mm/dd/yyyy'') zone_jump_label_date
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod
			on (cod.order_guid = co.order_guid)
	LEFT OUTER JOIN	clean.order_tracking cot
			on (cot.order_detail_id = cod.order_detail_id)
	JOIN	venus.venus vv
			on (vv.reference_number = to_char(cod.order_detail_id))
	JOIN	ftd_apps.vendor_master favm
			on (favm.vendor_id = cod.vendor_id)
	LEFT OUTER JOIN	venus.carrier_delivery vcd
			on (vcd.carrier_delivery = cod.carrier_delivery)
	LEFT OUTER JOIN	venus.carriers vcs
			on (vcs.carrier_id = vcd.carrier_id)
	--
	WHERE	cod.order_disp_code NOT IN (''Held'',
					    ''In-Scrub'',
					    ''Removed'',
					    ''Validated'',
					    ''Pending'')
	AND NOT EXISTS	(select ''x''
			    from clean.refund cr
			    where substr(cr.refund_disp_code,1,1) = ''A''
                            and cod.order_detail_id = cr.order_detail_id)
	AND NOT EXISTS	(select 1
			 from	venus.venus v2
			 where	v2.venus_order_number = vv.venus_order_number
			 and	v2.msg_type in (''REJ'',''CAN''))
         AND vv.venus_id = (select v3.venus_id
                             from venus.venus v3
                             where V3.MSG_TYPE = ''FTD''
                             AND v3.reference_number = vv.reference_number
                             and v3.transmission_time = (select max(vv4.transmission_time)
                                                         from venus.venus vv4
                                                         where vv4.reference_number = v3.reference_number
                                                         AND vv4.MSG_TYPE = ''FTD'')
			     and  v3.created_on = (select max(vv5.created_on)
                                                         from venus.venus vv5
                                                         where vv5.reference_number = v3.reference_number
                                                         AND vv5.MSG_TYPE = ''FTD'')
                             and rownum=1
			   )
        AND vv.msg_type = ''FTD'' '
        ||v_tracking_predicate||
	 v_date_predicate||
	 ' ORDER BY '||
		v_date_order||',
		vv.transmission_time,
		favm.vendor_name,
		cod.product_id ';
  EXECUTE IMMEDIATE v_sql;
END;
.
/
