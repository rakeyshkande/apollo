create or replace PROCEDURE rpt.acc09_refunds_ship_st_pop_tab
			(p_start_date in varchar2,
			 p_end_date in varchar2,
			 p_origin in varchar2,
			 p_company_code in varchar2
			)
IS
  v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
  v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
  v_origin_predicate VARCHAR2(500);
  v_company_predicate varchar2(4000);
  v_sql			VARCHAR2(30000);

BEGIN

IF p_origin = 'ALL' OR INSTR(p_origin, 'ALL') > 0 THEN
   v_origin_predicate := ' ';
ELSE
	v_origin_predicate:= ' AND co.origin_id in ('||rpt.multi_value(p_origin)||') ';
END IF;

IF p_company_code = 'ALL' OR INSTR(p_company_code, 'ALL') > 0 THEN
    v_company_predicate := ' AND co.company_id is not null ';
ELSE
    v_company_predicate:= ' AND co.company_id in (select * from table(string_convert(''' || p_company_code || '''))) ';
END IF;

v_sql := '
insert into rpt.rep_tab (col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15)
select ''1'' walmart,
       Case cc.country when ''US'' then ''US''
                       when ''USA'' then ''US''
                       when ''CA'' then ''CA''
                       else ''INTL''
       end country,
            cc.state state,
             -1*(sum(cat.Product_amount)-sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) Merch_value,
             -1*sum(cat.add_on_amount) add_ons,
             -1*sum(cat.shipping_fee) shipping_fee,
             -1*sum(cat.service_fee)  service_fee,
             -1*(sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))) Order_value,
       
			 sum(nvl(cr.TAX1_AMOUNT,0)) TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0))  TAX2_AMOUNT,
			sum(nvl(cr.TAX3_AMOUNT,0))  TAX3_AMOUNT,
	sum(nvl(cr.TAX4_AMOUNT,0))  TAX4_AMOUNT,
       -1*sum(nvl(cat.tax,0)+
                    nvl(cat.shipping_tax,0)+
                    nvl(cat.service_fee_tax,0))             tax,

             -1*(sum(cat.Product_amount)+
                 sum(cat.add_on_amount) +
                 sum(cat.shipping_fee) +
                 sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) -
                 sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
             sum(nvl(cat.tax,0)+
                 nvl(cat.shipping_tax,0)+
                 nvl(cat.service_fee_tax,0)))             total_order_amount,
             count(cat.order_detail_id) order_count
from clean.order_details cod
join clean.customer cc
on(cc.customer_id = cod.recipient_id)
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)

JOIN	clean.refund cr on (cr.refund_id=cat.refund_id)
join  clean.refund_disposition_val crdv
on (crdv.refund_disp_code = cat.refund_disp_code)
join clean.orders co
on (co.order_guid=cod.order_guid)
where trunc(cat.transaction_date)
   between '''|| v_start_date||''' AND '''||v_end_date||''''|| v_origin_predicate || v_company_predicate ||
	' and cat.payment_type <> ''NC''
	and crdv.refund_accounting_type = ''PG''
	and co.origin_id <>''TEST''
	and co.origin_id <>''test''
group by ''1'',
   cc.country,
   cc.state ';


EXECUTE IMMEDIATE v_sql;

END;
.
/