CREATE OR REPLACE
PROCEDURE rpt.mer12_ppv_success_pop_tab
	(p_batch_id in varchar2)
IS


BEGIN
--
--**************************************************************
--
-- MER04 Price/Pay Vendor Success Report
--
--**************************************************************
--


INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
         col9
	)
SELECT
	ppvu.spreadsheet_row_num,
	ppvu.product_id,
        pm.product_name,
        ppvu.vendor_id,
        vm.vendor_name,
        ppvu.vendor_cost,
        ppvu.standard_price,
        ppvu.deluxe_price,
        ppvu.premium_price
from    ftd_apps.price_pay_vendor_update ppvu,
        ftd_apps.product_master pm,
        ftd_apps.vendor_master vm
--
WHERE	ppvu.price_pay_vendor_batch_id = p_batch_id
AND	ppvu.error_msg_txt is null
AND     pm.product_id (+) = ppvu.product_id
AND     vm.vendor_id (+) = ppvu.vendor_id
--
ORDER BY ppvu.spreadsheet_row_num;

END;
.
/
