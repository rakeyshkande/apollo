create or replace package body rpt.mkt03_customer_recap_pkg
as

/*-----------------------------------------------------------------------------
PROCEDURE line3_active_start (date)

Executes the query to get the active start.
-----------------------------------------------------------------------------*/
procedure line3_active_start
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 3: Active Start - count of all customers with orders 12 through 1 month prior to the
-- report execution month
-- EXAMPLE: Report execution month is August 2005 (8/1/05 - 8/31/05): A customer would be counted
-- as part of the Start if they had placed an order from 8/1/2004 through 7/31/2005 (12 months prior to 8/1/2005)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'3' line_number,
	'Active' group_header,
	'Start' row_header,
	count (distinct concat_id) all_count,
	count (decode(company_id, 'FTD', 1, null)) ftd_count,
	count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
	count (decode(company_id, 'FLORIST', 1, null)) florist_count,
	count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
	count (decode(company_id, 'FUSA', 1, null)) fusa_count,
	count (decode(company_id, 'ROSES', 1, null)) roses_count
from
(
	select 	o.concat_id,
		o.company_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= v_last_year
	and 	o.order_date < v_rpt_date
	group by o.concat_id,
		o.company_id
);
commit;
end line3_active_start;

/*-----------------------------------------------------------------------------
PROCEDURE line4_5_new_and_conv (date)

Executes the query to get the new to file and the conversion to brand.
-----------------------------------------------------------------------------*/
procedure line4_5_new_and_conv
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

-- cursor to get all customers new in the report execution month which will be used to get the counts
-- for line 4 - new to file and also part 2 of line 5 - conversion to brand.
cursor cust_cur (p_rpt_date date, p_next_month date)is
	select 	o.concat_id,
		o.company_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= p_rpt_date
	and 	o.order_date < p_next_month
	and	not exists
	(
		select	1
		from	clean.cust_recap_order_vw o2
		where	o2.order_date < p_rpt_date
		and	o2.concat_id = o.concat_id
	)
	order by o.concat_id, o.order_date;

-- counts for new to file
v_ftd_new	number := 0;
v_ftdca_new	number := 0;
v_florist_new	number := 0;
v_fdirect_new	number := 0;
v_fusa_new	number := 0;
v_roses_new number := 0;


-- counts for part 2 of conversion to brand
v_ftd_conv	number := 0;
v_ftdca_conv	number := 0;
v_florist_conv	number := 0;
v_fdirect_conv	number := 0;
v_fusa_conv	number := 0;
v_roses_conv number := 0;

-- flags for processing new customers
v_ftd_found	boolean := FALSE;
v_ftdca_found	boolean := FALSE;
v_florist_found	boolean := FALSE;
v_fdirect_found	boolean := FALSE;
v_fusa_found	boolean := FALSE;
v_roses_found	boolean := FALSE;

-- hold variables for processing new customers
v_hold_concat_id varchar2(82) := 'no customer';
v_hold_co_id	varchar2(10);

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- process the new customers
for cust_row in cust_cur (v_rpt_date, v_next_month)loop
	if cust_row.concat_id <> v_hold_concat_id then
		-- save the customer and the first company id found for the customer
		v_hold_concat_id := cust_row.concat_id;
		v_hold_co_id 	:= cust_row.company_id;
		v_ftd_found	:= FALSE;
		v_ftdca_found	:= FALSE;
		v_florist_found	:= FALSE;
		v_fdirect_found	:= FALSE;
		v_fusa_found	:= FALSE;
		v_roses_found	:= FALSE;

		-- accumulate counts for new to file customers
		case cust_row.company_id
		when 'FTD' then
			v_ftd_new := v_ftd_new + 1;
		when 'FTDCA' then
			v_ftdca_new := v_ftdca_new + 1;
		when 'FLORIST' then
			v_florist_new := v_florist_new + 1;
		when 'FDIRECT' then
			v_fdirect_new := v_fdirect_new + 1;
		when 'FUSA' then
			v_fusa_new := v_fusa_new + 1;
		when 'ROSES' then
			v_roses_new := v_roses_new + 1;
		else
			null;
		end case;
	else
		-- accumulate counts for conversion to brand for new customers
		if cust_row.company_id <> v_hold_co_id then
			case cust_row.company_id
			when 'FTD' then
				if not v_ftd_found then
					v_ftd_conv := v_ftd_conv + 1;
					v_ftd_found := TRUE;
				end if;
			when 'FTDCA' then
				if not v_ftdca_found then
					v_ftdca_conv := v_ftdca_conv + 1;
					v_ftdca_found := TRUE;
				end if;
			when 'FLORIST' then
				if not v_florist_found then
					v_florist_conv := v_florist_conv + 1;
					v_florist_found := TRUE;
				end if;
			when 'FDIRECT' then
				if not v_fdirect_found then
					v_fdirect_conv := v_fdirect_conv + 1;
					v_fdirect_found := TRUE;
				end if;
			when 'FUSA' then
				if not v_fusa_found then
					v_fusa_conv := v_fusa_conv + 1;
					v_fusa_found := TRUE;
				end if;
			when 'ROSES' then
				if not v_roses_found then
					v_roses_conv := v_roses_conv + 1;
					v_roses_found := TRUE;
				end if;
			else
				null;
			end case;
		end if;
	end if;
end loop;


-- line 4: New to File - count of all customer with the first order in the report execution month
-- EXAMPLE: Report execution month is August 2005 (8/1/2005 - 8/31/2005) any customer who had never
-- placed an order before and has placed their first for any brand between 8/1/2005 and 8/31/2005
-- inclusive of those dates.
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
values
( 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'4',
	'Active',
	'New to File',
	v_ftd_new + v_ftdca_new + v_florist_new + v_fdirect_new + v_fusa_new + v_roses_new,
	v_ftd_new,
	v_ftdca_new,
	v_florist_new,
	v_fdirect_new,
	v_fusa_new,
	v_roses_new
	
);
commit;

-- line 5: Conversion to Brand - part 1 gets customers new to a brand where the customer placed an
-- order in a previous month and an order for a different brand in the report execution month
-- EXAMPLE: Report execution month is August 2005 (8/1/2005 - 8/31/2005) any customer has previously
-- purchased on the FTD but has not purchased on any other brand, places a new order between
-- 8/1/2005 and 8/31/2005 for Gift Sense would be considered a conversion to the Gift Sense brand.
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
	

)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'5' line_number,
	'Active' group_header,
	'Conversion to Brand' row_header,
	null all_count,
	count (decode(company_id, 'FTD', 1, null)) ftd_count,
	count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
	count (decode(company_id, 'FLORIST', 1, null)) florist_count,
	count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
	count (decode(company_id, 'FUSA', 1, null)) fusa_count,
	count (decode(company_id, 'ROSES', 1, null)) roses_count
from
(
	select	*
	from
	(
		select 	o.concat_id,
			o.company_id
		from 	clean.cust_recap_order_vw o
		where 	o.order_date >= v_rpt_date
		and 	o.order_date < v_next_month
		minus
		select 	o.concat_id,
			o.company_id
		from 	clean.cust_recap_order_vw o
		where 	o.order_date < v_rpt_date
		group by o.concat_id,
			 o.company_id
	) o
	where 	exists
	(
		select 	1
		from 	clean.cust_recap_order_vw o2
		where 	o2.order_date < v_rpt_date
		and	o2.concat_id = o.concat_id
		and	o2.company_id <> o.company_id
	)
);
commit;

-- line 5: Conversion to Brand - part 2 gets customers new to a brand where the customer placed the first
-- order in the report execution month and an order for a different brand in the same month
update rpt.rep_cust_recap rt
set 	rt.col6 = nvl (to_number (rt.col6), 0) + v_ftd_conv,
	rt.col7 = nvl (to_number (rt.col7), 0) + v_ftdca_conv,
	rt.col8 = nvl (to_number (rt.col8), 0) + v_florist_conv,
	rt.col9 = nvl (to_number (rt.col9), 0) + v_fdirect_conv,
	rt.col10 = nvl (to_number (rt.col10), 0) + v_fusa_conv,
	rt.col11 = nvl (to_number (rt.col11), 0) + v_roses_conv
where	rt.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	rt.col2 = '5';
commit;
end line4_5_new_and_conv;

/*-----------------------------------------------------------------------------
PROCEDURE line6_reactivate_cust (date)

Executes the query to get reactivated customers.
-----------------------------------------------------------------------------*/
procedure line6_reactivate_cust
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 6: Active Reactivated Customer - part 1 gets customers with the last order placed prior to 12 months
-- before the report execution month without orders in the previous 12 through 1 months of the report
-- execution month for all brands
-- EXAMPLE: Report is for August 2005 (8/1/05 - 8/31/05); Customer A places order in any month prior
-- to August 2004(up to July 31,2004) (Inactive) and no orders since, until recent order placed in
-- August 2005 (August 1, 2005-August 31,2005).  A count of 1 would be in Reactivated Customer
-- (should be equal to Reactivated Customer field in inactive section except positive)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'6' line_number,
	'Active' group_header,
	'Reactivated Customer' row_header,
	count (distinct concat_id) all_count,
	null ftd_count,
	null ftdca_count,
	null florist_count,
	null fdirect_count,
	null fusa_count,
	null roses_count
from
(
	select 	o.concat_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= v_rpt_date
	and 	o.order_date < v_next_month
	and	exists
	(
		select 	1
		from 	clean.cust_recap_order_vw o2
		where 	o2.order_date < v_last_year
		and	o2.concat_id = o.concat_id
	)
	and	not exists
	(
		select 	1
		from 	clean.cust_recap_order_vw o3
		where 	o3.order_date >= v_last_year
		and	o3.order_date < v_rpt_date
		and	o3.concat_id = o.concat_id
	)
	group by o.concat_id
);
commit;

-- line 6: Active Reactivated Customer - part 2 gets customers with the last order placed prior to 12 months
-- before the report execution month without orders in the previous 12 through 1 months of the report
-- execution month for specific brands
-- EXAMPLE: Report is for August 2005; Customer A places order for the FTD brand in September of 2003
-- (Inactive for the brand) and an order for the Giftsense brand in December 2003 (Inactive for the brand),
-- and recently order for the Giftsense brand in August 2005.  A count of 1 would be in the
-- Reactivated Customer under the Giftsense column only.
update rpt.rep_cust_recap rt
set (rt.col6, rt.col7, rt.col8, rt.col9, rt.col10, rt.col11) =
(
	select 	count (decode(company_id, 'FTD', 1, null)) ftd_count,
		count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
		count (decode(company_id, 'FLORIST', 1, null)) florist_count,
		count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
		count (decode(company_id, 'FUSA', 1, null)) fusa_count,
		count (decode(company_id, 'ROSES', 1, null)) roses_count
	from
	(
		select 	o.concat_id,
			o.company_id
		from 	clean.cust_recap_order_vw o
		where 	o.order_date >= v_rpt_date
		and 	o.order_date < v_next_month
		and	exists
		(
			select 	1
			from 	clean.cust_recap_order_vw o2
			where 	o2.order_date < v_last_year
			and	o2.concat_id = o.concat_id
			and 	o2.company_id = o.company_id
		)
		and	not exists
		(
			select 	1
			from 	clean.cust_recap_order_vw o3
			where 	o3.order_date >= v_last_year
			and	o3.order_date < v_rpt_date
			and	o3.concat_id = o.concat_id
			and 	o3.company_id = o.company_id
		)
		group by o.concat_id,
			o.company_id
	)
)
where	rt.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	rt.col2 = '6';
commit;
end line6_reactivate_cust;


/*-----------------------------------------------------------------------------
PROCEDURE line7_attrition_inactive (date)

Executes the query to get attrition to inactive.
-----------------------------------------------------------------------------*/
procedure line7_attrition_inactive
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 7: Active Attrition to Inactive (-) - part 1 gets customers that fell to inactive status in the report
-- execution month for all brands
-- EXAMPLE: Report is for August 2005 (8/1/05 - 8/31/05). Customer A has not placed an order since
-- August of 2004, thus putting them in the > 12 months category (Inactive) (Placed their last order
-- in August 2004)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
	
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'7' line_number,
	'Active' group_header,
	'Attrition to Inactive (-)' row_header,
	count (distinct concat_id) * -1 all_count,
	null ftd_count,
	null ftdca_count,
	null florist_count,
	null fdirect_count,
	null fusa_count,
	null roses_count
from
(
	select 	o.concat_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= v_last_year
	and 	o.order_date < v_11mos_ago
	and	not exists
	(
		select 	1
		from 	clean.cust_recap_order_vw o2
		where 	o2.order_date >= v_11mos_ago
                and     o2.order_date < v_next_month
		and	o2.concat_id = o.concat_id
	)
	group by o.concat_id
);
commit;

-- line 7: Active Attrition to Inactive (-) - part 2 gets customers that fell to inactive status in the report
-- execution month for specific brands
update rpt.rep_cust_recap rt
set (rt.col6, rt.col7, rt.col8, rt.col9, rt.col10, rt.col11) =
(
	select 	count (decode(company_id, 'FTD', 1, null)) * -1 ftd_count,
		count (decode(company_id, 'FTDCA', 1, null)) * -1 ftdca_count,
		count (decode(company_id, 'FLORIST', 1, null)) * -1 florist_count,
		count (decode(company_id, 'FDIRECT', 1, null)) * -1 fdirect_count,
		count (decode(company_id, 'FUSA', 1, null)) * -1 fusa_count,
		count (decode(company_id, 'ROSES', 1, null)) * -1 roses_count
	from
	(
		select 	o.concat_id,
			o.company_id
		from 	clean.cust_recap_order_vw o
		where 	o.order_date >= v_last_year
		and 	o.order_date < v_11mos_ago
		and	not exists
		(
			select 	1
			from 	clean.cust_recap_order_vw o2
			where 	o2.order_date >= v_11mos_ago
                        and     o2.order_date < v_next_month
			and	o2.concat_id = o.concat_id
			and	o2.company_id = o.company_id
		)
		group by o.concat_id,
			o.company_id
	)
)
where 	rt.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	rt.col2 = '7';
commit;
end line7_attrition_inactive;

/*-----------------------------------------------------------------------------
PROCEDURE line9_inactive_start (date)

Executes the query to get the inactive start.
-----------------------------------------------------------------------------*/
procedure line9_inactive_start
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 9: Inactive Start - part 1 gets customers with the last order prior to 12 months of the report
-- execution month for all brands
-- EXAMPLE: Report execution month is August 2005 (8/1/05 - 8/31/05): A customer would be counted as
-- part of the Start if they had placed their last order prior to 8/1/04 or a purchaser flag on the
-- customer record without any orders in Apollo (HP converted customers).
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'9' line_number,
	'Inactive' group_header,
	'Start' row_header,
	count (distinct concat_id) all_count,
	null ftd_count,
	null ftdca_count,
	null florist_count,
	null fdirect_count,
	null fusa_count,
	null roses_count
from
(
	select 	o.concat_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date < v_last_year
	and 	not exists
	(
		select 	1
		from 	clean.cust_recap_order_vw o2
		where 	o2.order_date >= v_last_year
		and 	o2.order_date < v_rpt_date
		and	o2.concat_id = o.concat_id
	)
	group by o.concat_id
);
commit;

-- line 9: Inactive Start - part 2 gets customers with the last order prior to 12 months of the report
-- execution month for specific brands
-- EXAMPLE: Report is for August 2005; A customer would be counted as part of FTD Start if they had
-- placed there last FTD order prior to 8/1/04 but placed an Giftsense order between 8/01/04 and 8/31/05
-- in this case the customer would not be in the inactive start for the ALL column in the report.
update rpt.rep_cust_recap rt
set (rt.col6, rt.col7, rt.col8, rt.col9, rt.col10, rt.col11) =
(
	select 	count (decode(company_id, 'FTD', 1, null)) ftd_count,
		count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
		count (decode(company_id, 'FLORIST', 1, null)) florist_count,
		count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
		count (decode(company_id, 'FUSA', 1, null)) fusa_count,
		count (decode(company_id, 'ROSES', 1, null)) roses_count
	from
	(
		select 	o.concat_id,
			o.company_id
		from 	clean.cust_recap_order_vw o
		where 	o.order_date < v_last_year
		and 	not exists
		(
			select 	1
			from 	clean.cust_recap_order_vw o2
			where 	o2.order_date >= v_last_year
			and 	o2.order_date < v_rpt_date
			and	o2.concat_id = o.concat_id
			and 	o2.company_id = o.company_id
		)
		group by o.concat_id,
			o.company_id
	)
)
where	rt.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	rt.col2 = '9';
commit;

-- line 9: Inactive Start - part 3 gets customers with a buyer indicator = 'Y' with no associated orders
-- which takes into account HP converted customers with order previous to April 2004 that were not converted
-- into Apollo
update rpt.rep_cust_recap rt
set (rt.col5, rt.col6) =
(
	select 	nvl (to_number (rt.col5), 0) + count (distinct c.concat_id),
		nvl (to_number (rt.col6), 0) + count (distinct c.concat_id)
	from	clean.customer c
	where	c.buyer_indicator = 'Y'
	and	not exists
	(
		select 	1
		from 	clean.orders o
		where	o.customer_id = c.customer_id
	)
)
where	rt.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	rt.col2 = '9';
commit;
end line9_inactive_start;


/*-----------------------------------------------------------------------------
PROCEDURE line17_ltm_net_orders (date)

Executes the query to get the last 12 months net orders.
-----------------------------------------------------------------------------*/
procedure line17_ltm_net_orders
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 17 - Last 12 Months Net Orders Taken: last twelve month order count
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'17' line_number,
	'Other' group_header,
	'Last 12 Months Net Orders Taken' row_header,
	count (1) all_count,
	count (decode(company_id, 'FTD', 1, null)) ftd_count,
	count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
	count (decode(company_id, 'FLORIST', 1, null)) florist_count,
	count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
	count (decode(company_id, 'FUSA', 1, null)) fusa_count,
	count (decode(company_id, 'ROSES', 1, null)) roses_count
from
(
	select 	distinct o.order_detail_id, o.company_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= v_11mos_ago
	and 	o.order_date < v_next_month
);
commit;
end line17_ltm_net_orders;

/*-----------------------------------------------------------------------------
PROCEDURE line18_cur_mo_net_orders (date)

Executes the query to get the current months net orders.
-----------------------------------------------------------------------------*/
procedure line18_cur_mo_net_orders
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 18 - Current Month Net Orders Taken: execution month order count
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'18' line_number,
	'Other' group_header,
	'Current Month Net Orders Taken' row_header,
	count (1) all_count,
	count (decode(company_id, 'FTD', 1, null)) ftd_count,
	count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
	count (decode(company_id, 'FLORIST', 1, null)) florist_count,
	count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
	count (decode(company_id, 'FUSA', 1, null)) fusa_count,
	count (decode(company_id, 'ROSES', 1, null)) roses_count
from
(
	select 	distinct o.order_detail_id, o.company_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= v_rpt_date
	and 	o.order_date < v_next_month
);
commit;
end line18_cur_mo_net_orders;

/*-----------------------------------------------------------------------------
PROCEDURE line20_cur_mo_customers (date)

Executes the query to get the current months customers.
-----------------------------------------------------------------------------*/
procedure line20_cur_mo_customers
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_last_month    date;
v_last_year     date;
v_11mos_ago     date;
v_next_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);
v_last_year := add_months (v_rpt_date, -12);
v_11mos_ago := add_months(v_rpt_date, -11);
v_next_month := add_months(v_rpt_date, 1);

-- line 20 - Current Month Customers: count of customer base that had an order in the report
-- execution month
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'20' line_number,
	'Other' group_header,
	'Current Month Customers' row_header,
	count (distinct concat_id) all_count,
	count (decode(company_id, 'FTD', 1, null)) ftd_count,
	count (decode(company_id, 'FTDCA', 1, null)) ftdca_count,
	count (decode(company_id, 'FLORIST', 1, null)) florist_count,
	count (decode(company_id, 'FDIRECT', 1, null)) fdirect_count,
	count (decode(company_id, 'FUSA', 1, null)) fusa_count,
	count (decode(company_id, 'ROSES', 1, null)) roses_count
from
(
	select 	o.concat_id,
		o.company_id
	from 	clean.cust_recap_order_vw o
	where 	o.order_date >= v_rpt_date
	and 	o.order_date < v_next_month
	group by o.concat_id,
		o.company_id
);
commit;
end line20_cur_mo_customers;


/*-----------------------------------------------------------------------------
PROCEDURE computed_totals (date)

Inserts the computed report records.
-----------------------------------------------------------------------------*/
procedure computed_totals
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_check		integer := 0;
v_rep_row	rpt.rep_cust_recap%rowtype;

v_last_month    date;

begin

v_last_month := add_months(v_rpt_date, -1);

-- check if the prior month ending balance exists, if not values of the current active start will be
-- in the prior month endind balance
select 	count (1)
into	v_check
from 	rpt.rep_cust_recap
where	col1 = to_char (v_last_month, 'mm/dd/yyyy')
and	col2 = '8';

if v_check > 0 then
	-- line 1: Prior Month Ending Balance - value reported as Month Ending Balance in the prior month
	insert into rpt.rep_cust_recap
	(	col1,
		col2,
		col3,
		col4,
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11
	)
	select	to_char (v_rpt_date, 'mm/dd/yyyy'),
		'1',
		'Active',
		'Prior Month Ending Balance',
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11
	from	rpt.rep_cust_recap
	where	col1 = to_char (v_last_month, 'mm/dd/yyyy')
	and	col2 = '8';
else
	-- line 1: Prior Month Ending Balance - 0 values
	insert into rpt.rep_cust_recap
	(	col1,
		col2,
		col3,
		col4,
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11
	)
	select	to_char (v_rpt_date, 'mm/dd/yyyy'),
		'1',
		'Active',
		'Prior Month Ending Balance',
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11
	from	rpt.rep_cust_recap
	where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
	and	col2 = '3';
end if;
commit;

-- line 2: Discrepancy Count (-) - current month active start minus prior month ending balance (line 3 - line 1)
-- This line is computed after line 1 and 3 are retrieved from the database
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'2' line_number,
	'Active' group_header,
	'Discrepancy Count (-)' row_header,
	nvl (to_number (r3.col5), 0) - nvl (to_number (r1.col5), 0) all_count,
	nvl (to_number (r3.col6), 0) - nvl (to_number (r1.col6), 0) ftd_count,
	nvl (to_number (r3.col7), 0) - nvl (to_number (r1.col7), 0) ftdca_count,
	nvl (to_number (r3.col8), 0) - nvl (to_number (r1.col8), 0) florist_count,
	nvl (to_number (r3.col9), 0) - nvl (to_number (r1.col9), 0) fdirect_count,
	nvl (to_number (r3.col10), 0) - nvl (to_number (r1.col10), 0) fusa_count,
	nvl (to_number (r3.col11), 0) - nvl (to_number (r1.col11), 0) roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r3
on 	r1.col1 = r3.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	r1.col2 = '1'
and	r3.col2 = '3';
commit;

-- line 8: Active Ending Balance - active start + new to file + conversion to brand + reactivated customer +
-- (attrition to inactive)  (sum of lines 3 through 7)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
	
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'8' line_number,
	'Active' group_header,
	'Ending Balance' row_header,
	sum (nvl (to_number (col5), 0)) all_count,
	sum (nvl (to_number (col6), 0)) ftd_count,
	sum (nvl (to_number (col7), 0)) ftdca_count,
	sum (nvl (to_number (col8), 0)) florist_count,
	sum (nvl (to_number (col9), 0)) fdirect_count,
	sum (nvl (to_number (col10), 0)) fusa_count,
	sum (nvl (to_number (col11), 0)) roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (col2) between 3 and 7;
commit;


-- line 10: Inactive Attrition to Inactive (-) - gets customers that fell to inactive status in the
-- report execution month (negative value of line 7)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'10' line_number,
	'Inactive' group_header,
	'Attrition to Inactive' row_header,
	nvl (to_number (col5), 0) * -1 all_count,
	nvl (to_number (col6), 0) * -1 ftd_count,
	nvl (to_number (col7), 0) * -1 ftdca_count,
	nvl (to_number (col8), 0) * -1 florist_count,
	nvl (to_number (col9), 0) * -1 fdirect_count,
	nvl (to_number (col10), 0) * -1 fusa_count,
	nvl (to_number (col11), 0) * -1 roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (col2) = 7;
commit;

-- line 11: Inactive Reactivated Customer (-) - part 1 gets customers with the last order placed prior to 12 months
-- before the report execution month without orders in the previous 12 through 1 months of the report
-- execution month (negative value of line 6)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'11' line_number,
	'Inactive' group_header,
	'Reactivated Customer (-)' row_header,
	nvl (to_number (col5), 0) * -1 all_count,
	nvl (to_number (col6), 0) * -1 ftd_count,
	nvl (to_number (col7), 0) * -1 ftdca_count,
	nvl (to_number (col8), 0) * -1 florist_count,
	nvl (to_number (col9), 0) * -1 fdirect_count,
	nvl (to_number (col10), 0) * -1 fusa_count,
	nvl (to_number (col11), 0) * -1 roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (col2) = 6;
commit;

-- line 12: Inactive Ending Balance - inactive start + attrition to inactive + (reactivated customer)
-- (sum of lines 9 through 11)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'12' line_number,
	'Inactive' group_header,
	'Ending Balance' row_header,
	sum (nvl (to_number (col5), 0)) all_count,
	sum (nvl (to_number (col6), 0)) ftd_count,
	sum (nvl (to_number (col7), 0)) ftdca_count,
	sum (nvl (to_number (col8), 0)) florist_count,
	sum (nvl (to_number (col9), 0)) fdirect_count,
	sum (nvl (to_number (col10), 0)) fusa_count,
	sum (nvl (to_number (col11), 0)) roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (col2) between 9 and 11;
commit;

-- line 13 - Total Start: active start + inactive start  (line 3 + line 9)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'13' line_number,
	'Totals' group_header,
	'Total Start' row_header,
	sum (nvl (to_number (col5), 0)) all_count,
	sum (nvl (to_number (col6), 0))  ftd_count,
	sum (nvl (to_number (col7), 0))  ftdca_count,
	sum (nvl (to_number (col8), 0))  florist_count,
	sum (nvl (to_number (col9), 0))  fdirect_count,
	sum (nvl (to_number (col10), 0))  fusa_count,
	sum (nvl (to_number (col11), 0))  roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (col2) in (3, 9);
commit;

-- line 14 - Total Customer Base: active ending balance + inactive ending balance (line 8 + line 12)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'14' line_number,
	'Totals' group_header,
	'Total Customer Base' row_header,
	sum (nvl (to_number (col5), 0)) all_count,
	sum (nvl (to_number (col6), 0))  ftd_count,
	sum (nvl (to_number (col7), 0))  ftdca_count,
	sum (nvl (to_number (col8), 0))  florist_count,
	sum (nvl (to_number (col9), 0))  fdirect_count,
	sum (nvl (to_number (col10), 0))  fusa_count,
	sum (nvl (to_number (col11), 0))  roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	to_number (col2) in (8, 12);
commit;


-- line 15 - Total Change: total customer base - total start  (line 14 - line 13)
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'15' line_number,
	'Totals' group_header,
	'Total Change' row_header,
	nvl (to_number (r1.col5), 0) - nvl (to_number (r2.col5), 0)  all_count,
	nvl (to_number (r1.col6), 0) - nvl (to_number (r2.col6), 0)  ftd_count,
	nvl (to_number (r1.col7), 0)  - nvl (to_number (r2.col7), 0)  ftdca_count,
	nvl (to_number (r1.col8), 0)  - nvl (to_number (r2.col8), 0)  florist_count,
	nvl (to_number (r1.col9), 0)  - nvl (to_number (r2.col9), 0)  fdirect_count,
	nvl (to_number (r1.col10), 0)  - nvl (to_number (r2.col10), 0)  fusa_count,
	nvl (to_number (r1.col11), 0)  - nvl (to_number (r2.col11), 0)  roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r2
on 	r1.col1 = r2.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (r1.col2)= 14
and	to_number (r2.col2) = 13;
commit;


-- line 16 - % Change: total change / total start  (line 15 / line 13)
-- The total start values are checked for null or 0 to prevent invalid calculations
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'16' line_number,
	'Totals' group_header,
	'% Change' row_header,
	decode (to_number (r2.col5), null, 0, 0, 0, round (nvl (to_number (r1.col5), 0) / to_number (r2.col5), 4)) all_count,
	decode (to_number (r2.col6), null, 0, 0, 0, round (nvl (to_number (r1.col6), 0) / to_number (r2.col6), 4)) ftd_count,
	decode (to_number (r2.col7), null, 0, 0, 0, round (nvl (to_number (r1.col7), 0) / to_number (r2.col7), 4)) ftdca_count,
	decode (to_number (r2.col8), null, 0, 0, 0, round (nvl (to_number (r1.col8), 0) / to_number (r2.col8), 4)) florist_count,
	decode (to_number (r2.col9), null, 0, 0, 0, round (nvl (to_number (r1.col9), 0) / to_number (r2.col9), 4)) fdirect_count,
	decode (to_number (r2.col10), null, 0, 0, 0, round (nvl (to_number (r1.col10), 0) / to_number (r2.col10), 4)) fusa_count,
	decode (to_number (r2.col11), null, 0, 0, 0, round (nvl (to_number (r1.col11), 0) / to_number (r2.col11), 4)) roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r2
on 	r1.col1 = r2.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	to_number (r1.col2) = 15
and	to_number (r2.col2) = 13;
commit;


-- line 19 - Average Order per Active Customer: last 12 months net orders taken / active ending balance
-- (line 18 / line 8). The active ending balance values are checked for null or 0 to prevent invalid calculations.
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'19' line_number,
	'Other' group_header,
	'Average Order per Active Customer' row_header,
	decode (to_number (r2.col5), null, 0, 0, 0, round (nvl (to_number (r1.col5), 0) / to_number (r2.col5), 2)) all_count,
	decode (to_number (r2.col6), null, 0, 0, 0, round (nvl (to_number (r1.col6), 0) / to_number (r2.col6), 2)) ftd_count,
	decode (to_number (r2.col7), null, 0, 0, 0, round (nvl (to_number (r1.col7), 0) / to_number (r2.col7), 2)) ftdca_count,
	decode (to_number (r2.col8), null, 0, 0, 0, round (nvl (to_number (r1.col8), 0) / to_number (r2.col8), 2)) florist_count,
	decode (to_number (r2.col9), null, 0, 0, 0, round (nvl (to_number (r1.col9), 0) / to_number (r2.col9), 2)) fdirect_count,
	decode (to_number (r2.col10), null, 0, 0, 0, round (nvl (to_number (r1.col10), 0) / to_number (r2.col10), 2)) fusa_count,
	decode (to_number (r2.col11), null, 0, 0, 0, round (nvl (to_number (r1.col11), 0) / to_number (r2.col11), 2)) roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r2
on 	r1.col1 = r2.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	to_number (r1.col2) = 17
and	to_number (r2.col2) = 8;
commit;


-- line 21 - % New Customer: new to file / current month customers  (line 4 / line 20)  The
-- current month customer values are checked for null or 0 to prevent invalid calculations.
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'21' line_number,
	'Other' group_header,
	'% New Customer' row_header,
	decode (to_number (r2.col5), null, 0, 0, 0, round (nvl (to_number (r1.col5), 0) / to_number (r2.col5), 4)) all_count,
	decode (to_number (r2.col6), null, 0, 0, 0, round (nvl (to_number (r1.col6), 0) / to_number (r2.col6), 4)) ftd_count,
	decode (to_number (r2.col7), null, 0, 0, 0, round (nvl (to_number (r1.col7), 0) / to_number (r2.col7), 4)) ftdca_count,
	decode (to_number (r2.col8), null, 0, 0, 0, round (nvl (to_number (r1.col8), 0) / to_number (r2.col8), 4)) florist_count,
	decode (to_number (r2.col9), null, 0, 0, 0, round (nvl (to_number (r1.col9), 0) / to_number (r2.col9), 4)) fdirect_count,
	decode (to_number (r2.col10), null, 0, 0, 0, round (nvl (to_number (r1.col10), 0) / to_number (r2.col10), 4)) fusa_count,
	decode (to_number (r2.col11), null, 0, 0, 0, round (nvl (to_number (r1.col11), 0) / to_number (r2.col11), 4)) roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r2
on 	r1.col1 = r2.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and	to_number (r1.col2) = 4
and	to_number (r2.col2) = 20;
commit;


-- line 22 - % Active Repeat Customer: (current month customers - new file - reactivated customer) / current month customers
-- ((line 20 -  line 4 - line 6) / line 20)  The current month customer values are checked for null or 0
-- to prevent invalid calculations.
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'22' line_number,
	'Other' group_header,
	'% Active Repeat Customer' row_header,
	decode (to_number (r3.col5), null, 0, 0, 0, round ((nvl (to_number (r3.col5), 0)-nvl (to_number (r2.col5), 0)-nvl (to_number (r1.col5), 0)) / to_number (r3.col5), 4)) all_count,
	decode (to_number (r3.col6), null, 0, 0, 0, round ((nvl (to_number (r3.col6), 0)-nvl (to_number (r2.col6), 0)-nvl (to_number (r1.col6), 0)) / to_number (r3.col6), 4)) ftd_count,
	decode (to_number (r3.col7), null, 0, 0, 0, round ((nvl (to_number (r3.col7), 0)-nvl (to_number (r2.col7), 0)-nvl (to_number (r1.col7), 0)) / to_number (r3.col7), 4)) ftdca_count,
	decode (to_number (r3.col8), null, 0, 0, 0, round ((nvl (to_number (r3.col8), 0)-nvl (to_number (r2.col8), 0)-nvl (to_number (r1.col8), 0)) / to_number (r3.col8), 4)) florist_count,
	decode (to_number (r3.col9), null, 0, 0, 0, round ((nvl (to_number (r3.col9), 0)-nvl (to_number (r2.col9), 0)-nvl (to_number (r1.col9), 0)) / to_number (r3.col9), 4)) fdirect_count,
	decode (to_number (r3.col10), null, 0, 0, 0, round ((nvl (to_number (r3.col10), 0)-nvl (to_number (r2.col10), 0)-nvl (to_number (r1.col10), 0)) / to_number (r3.col10), 4)) fusa_count,
	decode (to_number (r3.col11), null, 0, 0, 0, round ((nvl (to_number (r3.col11), 0)-nvl (to_number (r2.col11), 0)-nvl (to_number (r1.col11), 0)) / to_number (r3.col11), 4)) roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r2
on 	r1.col1 = r2.col1
join 	rpt.rep_cust_recap r3
on 	r1.col1 = r3.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (r1.col2) = 6
and	to_number (r2.col2) = 4
and	to_number (r3.col2) = 20;
commit;


-- 23 - % Reactivated Customer: reactivated customer / current month customers  (line 6 / line 20)
-- The current month customer values are checked for null or 0 to prevent invalid calculations.
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'23' line_number,
	'Other' group_header,
	'% Reactivated Customer' row_header,
	decode (to_number (r2.col5), null, 0, 0, 0, round (nvl (to_number (r1.col5), 0) / to_number (r2.col5), 4)) all_count,
	decode (to_number (r2.col6), null, 0, 0, 0, round (nvl (to_number (r1.col6), 0) / to_number (r2.col6), 4)) ftd_count,
	decode (to_number (r2.col7), null, 0, 0, 0, round (nvl (to_number (r1.col7), 0) / to_number (r2.col7), 4)) ftdca_count,
	decode (to_number (r2.col8), null, 0, 0, 0, round (nvl (to_number (r1.col8), 0) / to_number (r2.col8), 4)) florist_count,
	decode (to_number (r2.col9), null, 0, 0, 0, round (nvl (to_number (r1.col9), 0) / to_number (r2.col9), 4)) fdirect_count,
	decode (to_number (r2.col10), null, 0, 0, 0, round (nvl (to_number (r1.col10), 0) / to_number (r2.col10), 4)) fusa_count,
	decode (to_number (r2.col11), null, 0, 0, 0, round (nvl (to_number (r1.col11), 0) / to_number (r2.col11), 4)) roses_count
from	rpt.rep_cust_recap r1
join 	rpt.rep_cust_recap r2
on 	r1.col1 = r2.col1
where	r1.col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (r1.col2) = 6
and	to_number (r2.col2) = 20;
commit;


-- line 24 - % Total Repeat Customer: (current month customers - new to file) / active start
insert into rpt.rep_cust_recap
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
)
select 	to_char (v_rpt_date, 'mm/dd/yyyy'),
	'24' line_number,
	'Other' group_header,
	'% Total Repeat Customer' row_header,
	sum (nvl (to_number (col5), 0)) all_count,
	sum (nvl (to_number (col6), 0))  ftd_count,
	sum (nvl (to_number (col7), 0))  ftdca_count,
	sum (nvl (to_number (col8), 0))  florist_count,
	sum (nvl (to_number (col9), 0))  fdirect_count,
	sum (nvl (to_number (col10), 0))  fusa_count,
	sum (nvl (to_number (col11), 0))  roses_count
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
and 	to_number (col2) in (22, 23);
commit;
end computed_totals;

/*-----------------------------------------------------------------------------
PROCEDURE delete_cust_recap (date)

Delete the existing report records for the given date
-----------------------------------------------------------------------------*/
procedure delete_cust_recap
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is
pragma autonomous_transaction;

begin

delete from rpt.rep_cust_recap
where	col1 = to_char (in_rpt_date, 'mm/dd/yyyy');
commit;

end delete_cust_recap;


/*-----------------------------------------------------------------------------
PROCEDURE build_cust_recap (date)

Main calling procedure that controls the flow of the calls to the query
procedures.

THIS PROCEDURE IS CALLED IF THE REPORT IS BUILT VIA THE REPORT SUBMITTER IN THE
REPORT APPLICATION AND THE REPORT RECORDS DO NOT ALREADY EXIST IN THE
REP_CUST_RECAP TABLE.  THIS CAN ALSO BE MANUALLY CALLED TO BUILD THE REPORT
AS A NON PARALLEL PROCESS.
-----------------------------------------------------------------------------*/
procedure build_cust_recap
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));

begin

-- delete the existing report records in the cust recap table
delete_cust_recap (v_rpt_date);

-- build the report lines from queries
line3_active_start (v_rpt_date);
line4_5_new_and_conv (v_rpt_date);
line6_reactivate_cust (v_rpt_date);
line7_attrition_inactive (v_rpt_date);
line9_inactive_start (v_rpt_date);
line17_ltm_net_orders (v_rpt_date);
line18_cur_mo_net_orders (v_rpt_date);
line20_cur_mo_customers (v_rpt_date);

-- build the computed totals
computed_totals (v_rpt_date);

end build_cust_recap;


/******************************************************************************
THE FOLLOWING PROCEDURES ARE USED TO SUBMIT THE REPORT CREATION AS THREE
PARALLEL PROCESSES.  THE JOBS WILL BE SUBMITTED ON THE FIRST OF THE MONTH AND
THE EXCEL REPORT WILL BE SCHEDULED IN THE REPORT APPLICATION TO RUN ON THE
FIFTH OF THE MONTH.
******************************************************************************/

/*-----------------------------------------------------------------------------
function submit_jobs_complete (date)

Functions that checks if the submitted jobs have inserted the report records
from the queries.
-----------------------------------------------------------------------------*/
FUNCTION submit_jobs_complete
(
in_rpt_date		date
)
return boolean
as

v_check		number := 0;
v_return	boolean := false;

begin

-- get the number of submitted jobs for the given month
select 	count (1)
into	v_check
from	all_jobs
where 	what like 'rpt.mkt03_customer_recap_pkg.submit_cust_recap%' || to_char (in_rpt_date, 'mm/yyyy') || '%';

-- if the job count is 1, then the queries are done and the submitted jobs are complete.
if v_check = 1 then
	v_return := true;
end if;

return v_return;

end submit_jobs_complete;

/*-----------------------------------------------------------------------------
PROCEDURE submit_cust_recap1 (date)

Procedure #1 that controls the flow of the calls to the query procedures.
Used in the parallel processing of building the report.
-----------------------------------------------------------------------------*/
procedure submit_cust_recap1
(
in_rpt_date		date
)
is

begin

-- build the report lines from queries
line3_active_start (in_rpt_date);
line4_5_new_and_conv (in_rpt_date);
line6_reactivate_cust (in_rpt_date);

if submit_jobs_complete (in_rpt_date) then
	-- build the computed totals
	computed_totals (in_rpt_date);
end if;

end submit_cust_recap1;


/*-----------------------------------------------------------------------------
PROCEDURE submit_cust_recap2 (date)

Procedure #2 that controls the flow of the calls to the query procedures.
Used in the parallel processing of building the report.
-----------------------------------------------------------------------------*/
procedure submit_cust_recap2
(
in_rpt_date		date
)
is

begin

-- build the report lines from queries
line9_inactive_start (in_rpt_date);

if submit_jobs_complete (in_rpt_date)then
	-- build the computed totals
	computed_totals (in_rpt_date);
end if;

end submit_cust_recap2;

/*-----------------------------------------------------------------------------
PROCEDURE submit_cust_recap3 (date)

Procedure #3 that controls the flow of the calls to the query procedures.
Used in the parallel processing of building the report.
-----------------------------------------------------------------------------*/
procedure submit_cust_recap3
(
in_rpt_date		date
)
is

begin

-- build the report lines from queries
line7_attrition_inactive (in_rpt_date);
line17_ltm_net_orders (in_rpt_date);
line18_cur_mo_net_orders (in_rpt_date);
line20_cur_mo_customers (in_rpt_date);

if submit_jobs_complete (in_rpt_date) then
	-- build the computed totals
	computed_totals (in_rpt_date);
end if;

end submit_cust_recap3;


/*-----------------------------------------------------------------------------
PROCEDURE submit_cust_recap (date)

Main calling procedure that controls the flow of the parallel processing of
building the report.
-----------------------------------------------------------------------------*/
procedure submit_cust_recap
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
is

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_submit_string	varchar2 (1000);
v_job           binary_integer;

begin

-- delete the existing report records in the cust recap table
delete_cust_recap (v_rpt_date);

-- submit report in 3 jobs;
v_submit_string := 'rpt.mkt03_customer_recap_pkg.submit_cust_recap1(to_date(''' || to_char (v_rpt_date, 'mm/yyyy') || ''', ''mm/yyyy''));';
dbms_job.submit(v_job, v_submit_string);

v_submit_string := 'rpt.mkt03_customer_recap_pkg.submit_cust_recap2(to_date(''' || to_char (v_rpt_date, 'mm/yyyy') || ''', ''mm/yyyy''));';
dbms_job.submit(v_job, v_submit_string);

v_submit_string := 'rpt.mkt03_customer_recap_pkg.submit_cust_recap3(to_date(''' || to_char (v_rpt_date, 'mm/yyyy') || ''', ''mm/yyyy''));';
dbms_job.submit(v_job, v_submit_string);

commit;

end submit_cust_recap;

end mkt03_customer_recap_pkg;
/
