CREATE OR REPLACE
function rpt.multi_value (v_val in varchar2)
return varchar2
is
P_val varchar2(1000);
P_val2 varchar2(1000);
v_new_parm  varchar2(1000);
v_comma_pos number(3);
v_numb_commas number(3) := 0;
v_start number(3) := 0;
BEGIN
p_val := v_val;
p_val2 := v_val;
while instr(p_val2,',') <> 0 loop
v_comma_pos := instr(p_val2,',');
P_val := ''''||rtrim(ltrim(substr(p_val2,1,v_comma_pos-1),' '),' ')||''',';
v_new_parm := v_new_parm || p_val;
P_val2 := substr(p_val2, v_comma_pos+1);
end loop;
v_new_parm:= v_new_parm||''''||rtrim(ltrim(p_val2,' '),' ')||'''';
return(v_new_parm);
end;
.
/
