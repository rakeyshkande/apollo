CREATE OR REPLACE
PACKAGE BODY rpt.PRO03_Cust_serv_queue_detail IS

  count_v		number(9)  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure p( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
		rpt.report_pkg.UPDATE_REPORT_DETAIL_CHAR
			(
			g_SUBMISSION_ID,
			p_str||chr(10),
			v_OUT_STATUS_det,
			v_OUT_MESSAGE_det
			);
		if v_out_status_det = 'N' then
			rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
			(
			g_SUBMISSION_ID,
			'REPORT',
			'E',
			v_OUT_MESSAGE_det,
			v_OUT_STATUS_sub,
			v_OUT_MESSAGE_sub
			);
		end if;
  end if;

end;

--
--****************************************************************************************************
-- print a comment line
--****************************************************************************************************
procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  p( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;
--
--****************************************************************************************************
-- This is the column headings for the Report
--****************************************************************************************************
procedure print_headings(y in number)
is
begin
    p( 'F;R'||y||';FG0L;SM2' );
    p( 'C;Y'||y||';X8;K"# "' );
    p( 'C;Y'||y||';X9;K"# "' );
    p( 'C;Y'||y||';X10;K"# OF"' );
    p( 'C;Y'||y||';X11;K"AVG "' );

    p( 'F;R'||7||';FG0L;SM2' );
    p( 'C;Y'||7||';X4;K"AGENT"' );
    p( 'C;Y'||7||';X5;K"AGENT"' );
    p( 'C;Y'||7||';X8;K"OF ITEMS"' );
    p( 'C;Y'||7||';X9;K"OF ITEMS"' );
    p( 'C;Y'||7||';X10;K"OTHER ITEMS"' );
    p( 'C;Y'||7||';X11;K"HANDLE"' );

    p( 'F;R'||8||';FG0L;SM2' );
    p( 'C;Y'||8||';X2;K"TEAM NAME"' );
    p( 'C;Y'||8||';X3;K"AGENT ID"' );
    p( 'C;Y'||8||';X4;K"LAST NAME"' );
    p( 'C;Y'||8||';X5;K"FIRST NAME"' );
    p( 'C;Y'||8||';X6;K"DATE"' );
    p( 'C;Y'||8||';X7;K"QUEUE"' );
    p( 'C;Y'||8||';X8;K"TAGGED"' );
    p( 'C;Y'||8||';X9;K"DELETED"' );
    p( 'C;Y'||8||';X10;K"DELETED"' );
    p( 'C;Y'||8||';X11;K"TIME"' );

end print_headings;
--
--*******************************************************************************************
-- This sets up the Formats that will be used in the SLK file.  It also prints a title
-- for our report.
--*******************************************************************************************
procedure print_title(
	p_start_date in date, 
	p_end_date in date,
	p_center_name in varchar2,
	p_team_name in varchar2,
	p_user_id   in varchar2,
	p_queue_type in varchar2)
is

   type    my_curs_type is REF CURSOR;
   curs    my_curs_type;
   lchr_center_name   varchar2(3000);
   lchr_team_name   varchar2(3000);
   lchr_queue_name   varchar2(3000);
   lchr_user_id   varchar2(3000);
   str   varchar2(5000);
   v_center_name_pred varchar2(300);
   v_team_name_pred varchar2(300);
   v_queue_name_pred varchar2(300);
   v_temp   varchar2(100);
   
 begin

    if p_center_name is null
   	 or p_center_name = 'ALL' then
       v_center_name_pred := null;
       lchr_center_name := 'ALL';
    else
       v_center_name_pred:= ' where upper(facc.call_center_id) in ('||upper(multi_value(p_center_name))||') ';
		 str :=  'select facc.description center_name
		 				from   ftd_apps.call_center facc '
						||v_center_name_pred;

		 OPEN curs for str;
		 LOOP
			 EXIT WHEN curs%NOTFOUND;
			 FETCH curs into v_temp;
			 lchr_center_name := lchr_center_name || ', ' || v_temp;
			 v_temp := NULL;
		 END LOOP;
		 CLOSE curs;
   
		 SELECT
		    SUBSTR(lchr_center_name, 3, LENGTH(lchr_center_name) - 4)
		 INTO
		    lchr_center_name
		 FROM
		    DUAL; 
    end if;

    v_temp := NULL;
    str := NULL;
     
    if p_team_name is null
       or p_team_name = 'ALL' then
       v_team_name_pred := null;
       lchr_team_name := 'ALL';
    else
       v_team_name_pred:= ' where upper(facg.cs_group_id) in ('||upper(multi_value(p_team_name))||') ';
	    str :=  'select facg.description team_name
	 				 from   ftd_apps.cs_groups facg '
					 ||v_team_name_pred; 

	    OPEN curs for str;
	    LOOP
	   	  EXIT WHEN curs%NOTFOUND;
			  FETCH curs into v_temp;
			  lchr_team_name := lchr_team_name || ', ' || v_temp;
			  v_temp := NULL;
	    END LOOP;
	    CLOSE curs;
   
	    SELECT
		    SUBSTR(SUBSTR(lchr_team_name, 3, LENGTH(lchr_team_name)-4),1,243)
		 INTO
		    lchr_team_name
		 FROM
		    DUAL; 						 
    end if;

     v_temp := NULL;
     str := NULL;
     
     if p_queue_type is null 
     	  or p_queue_type = 'ALL' then
        v_queue_name_pred := null;
        lchr_queue_name := 'ALL';
     else
        v_queue_name_pred := ' where upper(cqv.queue_type) in ('||upper(multi_value(p_queue_type))||') ';
		  str :=  'select cqv.description queue_name
		 				 from   clean.queue_type_val cqv '
						 ||v_queue_name_pred; 

		  OPEN curs for str;
		  LOOP
		 	  EXIT WHEN curs%NOTFOUND;
			  FETCH curs into v_temp;
			  lchr_queue_name := lchr_queue_name || ', ' || v_temp;
			  v_temp := NULL;
		  END LOOP;
		  CLOSE curs;
   
		  SELECT
		     SUBSTR(SUBSTR(lchr_queue_name, 3, LENGTH(lchr_queue_name) - 4),1,243)
		  INTO
		     lchr_queue_name
		  FROM
		     DUAL; 						        
     end if;
     
     if p_user_id is null 
     	  or p_user_id = 'ALL' then
        lchr_user_id := 'ALL';
     else
          
		  SELECT
		     SUBSTR(p_user_id,1,243)
		  INTO
		     lchr_user_id
		  FROM
		     DUAL; 						        
     end if;

  p( 'ID;ORACLE' );

  p( 'P;Ph:mm:ss');                 -- SM0
  p( 'P;FCourier;M200' );           -- SM1
  p( 'P;FCourier;M200;SB' );        -- SM2
  p( 'P;FCourier;M200;SUB' );       -- SM3
  p( 'P;FCourier;M160' );           -- SM4

  p( 'F;C1;FG0L;SM2' );
  p( 'C;Y1;X2;K"CUSTOMER SERVICE QUEUE DETAIL REPORT ' ||
		p_start_date || ' Thru ' || p_end_date || '"' );
  p( 'C;Y2;X2;K"Center Name: ' || lchr_center_name || '"' );
  p( 'C;Y3;X2;K"Team Name: ' || lchr_team_name || '"' );
  p( 'C;Y4;X2;K"User ID: ' || lchr_user_id || '"' );
  p( 'C;Y5;X2;K"Queue Type: ' || lchr_queue_name || '"' );
  p( 'C;Y1;X11;K"Date: ' || to_char(SYSDATE, 'mm/dd/yyyy') || '"' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_title;
--
--**********************************************************************************************************
-- This will Define the Column Widths for the SLK File
--**********************************************************************************************************
procedure print_widths
is
begin
    p( 'F;W01 01 5' );   -- margin
    p( 'F;W02 02 15' );  -- Center, Team
    p( 'F;W03 04 15' );  -- Last, First
    p( 'F;W05 05 10' );	 -- Date
    p( 'F;W06 06 10' );	 -- Queue
    p( 'F;W07 09 15' );	 -- #of items tagged,# of Items Deleted, #of other Items
    p( 'F;W10 10 15' );	 -- average handle time
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_widths;
--
--**********************************************************************************************************
function format_time (p_time in number)
return varchar2
is

	CURSOR fmt_time IS
   SELECT TO_CHAR(TRUNC( MOD( (p_time)*24, 24 ) ), 'FM00')||':'||
	       TO_CHAR(TRUNC( MOD( (p_time)*24*60, 60 ) ), 'FM00')||':'|| 
 			 TO_CHAR( MOD( (p_time)*24*60*60, 60 ), 'FM00') avg_hdl_time FROM dual;
	 			 
 	v_avg_hdl_time   varchar2(20) := NULL;
 	
begin

   -- format the time
	OPEN fmt_time;
	FETCH fmt_time INTO v_avg_hdl_time;
   CLOSE fmt_time;
   
   return(v_avg_hdl_time);
   
end format_time;

--**************************************************************************************************************
-- This will dynamically build the select statement. Loop thru the REF CURSOR and determine where to print
-- the lines for the SLK file
--**************************************************************************************************************
function print_rows(p_start_date in varchar2,
                    p_end_date   in varchar2,
                    p_center_name in varchar2,
                    p_team_name   in varchar2,
                    p_user_id  in varchar2,
                    p_queue_type  in varchar2
		   ) return number
is
    type    my_curs_type is REF CURSOR; -- This must be a weak REF CURSOR (No Returning clause)
    curs    my_curs_type;  -- This allows us to define the SQL Statement for the CURSOR Dynamically
                          -- which was required because the users wanted to be able to pass multiple
                          -- parameter values.  It's also more efficient if they pass a null value
    str     varchar2(20000);            -- This holds the Dynamic Query that will be executed.
    type ret_rec is record              -- When we fetch we will fetch into a record of this structure
              (order_date varchar2(80),
       	      first_name varchar2(80),
       	      last_name varchar2(80),
              queue_type  varchar2(80),
              center_name  varchar2(80),
              team_name varchar2(80),
              identity_id varchar2(80),
      	      Number_tagged number,
        	    Same_day_Del number,
        	    not_Same_day_Del number,
              avg_handle_time number);
    ret     ret_rec;-- Based on the record type defined above
    v_center_name_pred varchar2(300);
    v_team_name_pred varchar2(300);
    v_queue_name_pred varchar2(300);
    v_queue_name_tag_pred varchar2(300);
    v_user_id_pred varchar2(300);
    row_cnt number          := 0;
    line    varchar2(32767) := null;
    n       number;
    y       number; -- Line number of the text that's written
    date_y number := 8;
    center_y number := 8;
    v_max_count   number(10) := 0; -- max count of the queues to calculate the avg handle time
    v_old_team   varchar2(80) := 'XXXX';-- Used to determine when a team changes
    v_old_date   varchar2(15) := '01-jan-1900';-- Used to determine when a Date changes
    v_old_center ftd_apps.call_center.description%type:= 'XXXX'; -- Used to determine when a center changes
    v_date_tot_tagged	number(10) := 0; -- tot for each date
    v_date_tot_deleted	number(10) := 0; -- tot for each date
    v_date_tot_other_del	number(10) := 0; -- tot for each date
    v_date_tot_avg_hdl_tm   number := 0; -- tot for each date
    v_team_tot_tagged	number(10) := 0; -- tot for each team for the date range
    v_team_tot_deleted	number(10) := 0; -- tot for each team for the date range
    v_team_tot_other_del	number(10) := 0; -- tot for each team for the date range
    v_team_tot_avg_hdl_tm   number := 0; -- tot for each team for the date range
    v_ctr_tot_tagged	number(10) := 0; -- tot for each center for the date range
    v_ctr_tot_deleted	number(10) := 0; -- tot for each center for the date range
    v_ctr_tot_other_del	number(10) := 0; -- tot for each center for the date range
    v_ctr_tot_avg_hdl_tm   number := 0; -- tot for each center for the date range
--
begin
   
   -----------------------------------------------------
   --  Building the predicates (Where clause conditions)
   -----------------------------------------------------
   if p_center_name is null 
   or p_center_name = 'ALL' then
      v_center_name_pred := null;
   else
      v_center_name_pred:= ' and upper(facc.call_center_id) in ('||upper(multi_value(p_center_name))||') ';
   end if;

   if p_team_name is null 
   or p_team_name = 'ALL' then
      v_team_name_pred := null;
   else
      v_team_name_pred:= ' and upper(facg.cs_group_id) in ('||upper(multi_value(p_team_name))||') ';
   end if;

   if p_user_id is null then
      v_user_id_pred := null;
   else
      v_user_id_pred := ' and upper(ai.identity_id) in  ('||upper(multi_value(p_user_id))||') ';
   end if;

   if p_queue_type is null 
   or p_queue_type = 'ALL' then
      v_queue_name_pred := null;
      v_queue_name_tag_pred := null;
   else
      v_queue_name_pred := ' and upper(cqh.queue_type) in  ('||upper(multi_value(p_queue_type))||') ';
      v_queue_name_tag_pred := ' and upper(cq.queue_type) in  ('||upper(multi_value(p_queue_type))||') ';
   end if;

   -----------------------------------------------------
   --  Define the SELECT
   -----------------------------------------------------
   str := 'select order_date,
               first_name,
              last_name,
              queue_type,
             center_name,
             team_name,
             identity_id,
   nvl(sum(number_tagged),0) number_tagged,
   nvl(sum(Same_day_del),0) Same_day_del,
   nvl(sum(not_same_day_del),0) not_same_day_del,
   nvl((sum(avg_handle_time) ),0) avg_handle_time
   from(
   select to_char(trunc(cqt.tagged_on),''mm/dd/yyyy'') order_date,
              au.first_name first_name,
              au.last_name last_name,
              cq.queue_type queue_type,
             facc.description center_name,
             facg.description team_name,
             ai.identity_id identity_id,
              nvl(count(distinct cqt.message_id),0) Number_tagged,
   0 Same_day_del,
   0 not_same_day_del,
   NVL((sum(ceh.end_time - ceh.start_time)),0) avg_handle_time
   from       clean.queue_tag cqt
   join           clean.queue cq
   on             (cq.message_id=cqt.message_id)
   join           aas.identity ai
   on             (cqt.csr_id = ai.identity_id)
   join           aas.user_history auh
   on             (ai.user_id = auh.user_id)
   join           aas.users au
   on             (ai.user_id = au.user_id)
   join			   clean.entity_history ceh
	on			      ((ceh.entity_id  = cq.order_guid
	               or ceh.entity_id = to_char(cq.order_detail_id))
                  and ceh.csr_id = ai.identity_id
                  and trunc(ceh.start_time) = trunc(cqt.tagged_on)
                  and trunc(ceh.end_time) = trunc(cqt.tagged_on))
   left outer join ftd_apps.call_center facc
   on             (facc.call_center_id = auh.call_center_id)
   left outer join ftd_apps.cs_groups facg
   on             (facg.cs_group_id = auh.cs_group_id)
   where       trunc(cqt.tagged_on) between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999 ' ||
   ' and       trunc(cqt.tagged_on) between trunc(auh.start_date) and trunc(auh.end_date) '
   ||v_center_name_pred
   ||v_team_name_pred
   ||v_queue_name_tag_pred
   ||v_user_id_pred
   ||' group by  to_char(trunc(cqt.tagged_on),''mm/dd/yyyy''),
              au.first_name ,
              au.last_name ,
              cq.queue_type ,
             facc.description ,
             facg.description ,
             ai.identity_id
   union all
   select to_char(trunc(cqh.tagged_on),''mm/dd/yyyy'') order_date,
              au.first_name first_name,
              au.last_name last_name,
              cqh.queue_type queue_type,
              facc.description center_name,
              facg.description team_name,
              ai.identity_id identity_id,
              nvl(count(distinct cqh.message_id),0) Number_tagged,
   			  nvl(count(distinct cqh.message_id),0) Same_day_del,
   			  0 not_same_day_del,
   			  NVL((sum(ceh.end_time - ceh.start_time)),0) avg_handle_time
   from       clean.queue_delete_history cqh
   join           aas.identity ai
   on             (cqh.tagged_by = ai.identity_id)
   join           aas.user_history auh
   on             (ai.user_id = auh.user_id)
   join           aas.users au
   on             (ai.user_id = au.user_id)
   join			   clean.entity_history ceh
	on			      ((ceh.entity_id  = cqh.order_guid
	               or ceh.entity_id = to_char(cqh.order_detail_id))
                  and ceh.csr_id = ai.identity_id
                  and trunc(ceh.start_time) = trunc(cqh.tagged_on)
                  and trunc(ceh.end_time) = trunc(cqh.tagged_on))
   left outer join           ftd_apps.call_center facc
   on             (facc.call_center_id = auh.call_center_id)
   left outer join           ftd_apps.cs_groups facg
   on             (facg.cs_group_id = auh.cs_group_id)
   where       trunc(cqh.tagged_on) = trunc(cqh.queue_deleted_on)
     and      cqh.queue_deleted_by =cqh.tagged_by
     and 		trunc(cqh.tagged_on) between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999 ' ||
   ' and       trunc(cqh.tagged_on) between trunc(auh.start_date) and trunc(auh.end_date) '
   ||v_center_name_pred
   ||v_team_name_pred
   ||v_queue_name_pred
   ||v_user_id_pred
   ||' group by  to_char(trunc(cqh.tagged_on),''mm/dd/yyyy''),
               au.first_name ,
               au.last_name ,
               cqh.queue_type ,
              facc.description ,
              facg.description ,
              ai.identity_id
   union all
   select to_char(trunc(cqh.tagged_on),''mm/dd/yyyy'') order_date,
              au.first_name first_name,
              au.last_name last_name,
              cqh.queue_type queue_type,
             facc.description center_name,
             facg.description team_name,
             ai.identity_id identity_id,
   0,
   0,
              nvl(count(distinct cqh.message_id),0),
   NVL((sum(ceh.end_time - ceh.start_time)),0) avg_handle_time
   from       clean.queue_delete_history cqh
   join       aas.identity ai
   on         (cqh.queue_deleted_by = ai.identity_id)
   join           aas.user_history auh
   on             (ai.user_id = auh.user_id)
   join       aas.users au
   on         (ai.user_id = au.user_id)
  join			  clean.entity_history ceh
   on			  ((ceh.entity_id  = cqh.order_guid
          or ceh.entity_id = to_char(cqh.order_detail_id))
           and ceh.csr_id = ai.identity_id
           and cqh.queue_deleted_on between ceh.start_time and ceh.end_time)	          
   left outer join ftd_apps.call_center facc
   on         (facc.call_center_id = au.call_center_id)
   left outer join ftd_apps.cs_groups facg
   on         (facg.cs_group_id = au.cs_group_id)
   where  (trunc(cqh.tagged_on)<> trunc(cqh.queue_deleted_on)
                      or cqh.queue_deleted_by <>cqh.tagged_by)
   and       trunc(cqh.queue_deleted_on) between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999 ' ||
   ' and       trunc(cqh.queue_deleted_on) between trunc(auh.start_date) and trunc(auh.end_date) '
   ||v_center_name_pred
   ||v_team_name_pred
   ||v_queue_name_pred
   ||v_user_id_pred
   ||' group by   to_char(trunc(cqh.tagged_on),''mm/dd/yyyy''),
              au.first_name ,
              au.last_name ,
              cqh.queue_type ,
             facc.description ,
             facg.description ,
             ai.identity_id)
   group by  center_name,
             team_name,
             order_date,
             last_name,
             first_name,
              queue_type,
              identity_id
   order by  center_name,
             team_name,
             order_date,
             identity_id,
             last_name,
             first_name,
              queue_type
   ';
--
--************************************************************************
-- Open cursor and begin loop
--************************************************************************
--
      y := 9;
--
      OPEN curs for str;
      LOOP
     	FETCH curs  into ret;
      	exit when curs%notfound;
       	if v_old_date <> ret.order_date and y > 9 then
	   		--  Print sub totals for date
	   		p( 'F;R'||to_char(y)||';FG0L;SM2' );
            line := 'C;Y'|| to_char(y) || ';X3';
            line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
            p( line );
      	    
    			line := 'C;Y'|| to_char(y) || ';X8';
	         line := line || ';K'||v_date_tot_tagged;
            p( line );
            
            line := 'C;Y'|| to_char(y) || ';X9';
				line := line || ';K'||v_date_tot_deleted;
            p( line );
            
            line := 'C;Y'|| to_char(y) || ';X10';
				line := line || ';K'||v_date_tot_other_del;
            p( line );

            SELECT
		         DECODE(greatest(greatest(v_date_tot_tagged, v_date_tot_deleted), v_date_tot_other_del), 0, 1,
		            greatest(greatest(v_date_tot_tagged, v_date_tot_deleted), v_date_tot_other_del))
		      INTO
		         v_max_count
		      FROM
		         DUAL;
	         
            line := 'F;P0;FG0G'||to_char(y)||';X11';
            p( line );
            line := 'C;Y'|| to_char(y) || ';X11';
            line := line || ';K'||(v_date_tot_avg_hdl_tm/v_max_count);
            p( line );
            
            v_max_count := 1;
            v_date_tot_tagged := 0;
				v_date_tot_deleted := 0;
				v_date_tot_other_del	:= 0;
				v_date_tot_avg_hdl_tm := 0;

            y:=y+1;
      	   date_y := y;
      	end if;

      	if (NVL(v_old_team, 'X') <> NVL(ret.team_name, 'X') and v_old_date = ret.order_date) and y > 9 then
	   		--  Print sub totals for team
	   	   p( 'F;R'||to_char(y)||';FG0L;SM2' );
            line := 'C;Y'|| to_char(y) || ';X3';
            line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
            p( line );
            
      	   line := 'C;Y'|| to_char(y) || ';X8';
	         line := line || ';K'||v_date_tot_tagged;
            p( line );
            
            line := 'C;Y'|| to_char(y) || ';X9';
				line := line || ';K'||v_date_tot_deleted;
            p( line );
            
            line := 'C;Y'|| to_char(y) || ';X10';
				line := line || ';K'||v_date_tot_other_del;
            p( line );

            SELECT
		         DECODE(greatest(greatest(v_date_tot_tagged, v_date_tot_deleted), v_date_tot_other_del), 0, 1,
		            greatest(greatest(v_date_tot_tagged, v_date_tot_deleted), v_date_tot_other_del))
		      INTO
		         v_max_count
		      FROM
		         DUAL;			    				
    			
      	   line := 'F;P0;FG0G'||to_char(y)||';X11';
            p( line );
            line := 'C;Y'|| to_char(y) || ';X11';
            line := line || ';K'||(v_date_tot_avg_hdl_tm/v_max_count);
            p( line );
            
      	   v_max_count := 1;
      	   v_date_tot_tagged := 0;
      	   v_date_tot_deleted := 0;
    			v_date_tot_other_del	:= 0;
    			v_date_tot_avg_hdl_tm := 0;
    			
            y:=y+1;

	   		-- print total for team
      	   BEGIN
      	     p( 'F;R'||to_char(y)||';FG0L;SM2' );
              line := 'C;Y'|| to_char(y) || ';X3';
              line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
              p( line );
             
    			  line := 'C;Y'|| to_char(y) || ';X8';
	           line := line || ';K'||v_team_tot_tagged;
              p( line );
            
              line := 'C;Y'|| to_char(y) || ';X9';
				  line := line || ';K'||v_team_tot_deleted;
              p( line );
            
              line := 'C;Y'|| to_char(y) || ';X10';
				  line := line || ';K'||v_team_tot_other_del;
              p( line );

            SELECT
		         DECODE(greatest(greatest(v_team_tot_tagged, v_team_tot_deleted), v_team_tot_other_del), 0, 1,
		            greatest(greatest(v_team_tot_tagged, v_team_tot_deleted), v_team_tot_other_del))
		      INTO
		         v_max_count
		      FROM
		         DUAL; 			  	  
    			  
      	     line := 'F;P0;FG0G'||to_char(y)||';X11';
              p( line );
              line := 'C;Y'|| to_char(y) || ';X11';
              line := line || ';K'||(v_team_tot_avg_hdl_tm/v_max_count);
              p( line );
            
      	     v_max_count := 1;
      	     v_team_tot_tagged := 0;
      	     v_team_tot_deleted := 0;
    			  v_team_tot_other_del	:= 0;
    			  v_team_tot_avg_hdl_tm := 0;
    			  v_date_tot_tagged := 0;
				  v_date_tot_deleted := 0;
    			  v_date_tot_other_del	:= 0;
    			  v_date_tot_avg_hdl_tm := 0;
    			
              y:=y+1;
      	     date_y := y;
      	   END;
      	end if;


      	if (NVL(v_old_team, 'X') <> NVL(ret.team_name, 'X') and v_old_date <> ret.order_date) and y > 9 then
	   		-- print total for team
      	   BEGIN
      	     p( 'F;R'||to_char(y)||';FG0L;SM2' );
             line := 'C;Y'|| to_char(y) || ';X3';
             line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
             p( line );
             
    			 line := 'C;Y'|| to_char(y) || ';X8';
	          line := line || ';K'||v_team_tot_tagged;
             p( line );
            
             line := 'C;Y'|| to_char(y) || ';X9';
				 line := line || ';K'||v_team_tot_deleted;
             p( line );
            
             line := 'C;Y'|| to_char(y) || ';X10';
				 line := line || ';K'||v_team_tot_other_del;
             p( line );
    			
            SELECT
		         DECODE(greatest(greatest(v_team_tot_tagged, v_team_tot_deleted), v_team_tot_other_del), 0, 1,
		            greatest(greatest(v_team_tot_tagged, v_team_tot_deleted), v_team_tot_other_del))
		      INTO
		         v_max_count
		      FROM
		         DUAL; 			 	 
    			 
      	    line := 'F;P0;FG0G'||to_char(y)||';X11';
             p( line );
             line := 'C;Y'|| to_char(y) || ';X11';
             line := line || ';K'||(v_team_tot_avg_hdl_tm/v_max_count);
             p( line );
            
      	    v_max_count := 1;
      	    v_team_tot_tagged := 0;
      	    v_team_tot_deleted := 0;
    			 v_team_tot_other_del	:= 0;
    			 v_team_tot_avg_hdl_tm := 0;
    			 v_date_tot_tagged := 0;
				 v_date_tot_deleted := 0;
    			 v_date_tot_other_del	:= 0;    			 
    			 v_date_tot_avg_hdl_tm := 0;
    			
             y:=y+1;
      	     date_y := y;
      	   END;
      	end if;

      	if NVL(v_old_center, 'X') <> NVL(ret.center_name, 'X') and y = 9 then
      	   p( 'F;R'||to_char(y)||';FG0L;SM2' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"'||ret.center_name||'"';
           p( line );
      	   y:=y+1;
      	   center_y := y;
      	elsif NVL(v_old_center, 'X') <> NVL(ret.center_name, 'X') and y > 9 then
	   --  Print sub totals for center
      	   BEGIN
      	     p( 'F;R'||to_char(y)||';FG0L;SM2' );
             line := 'C;Y'|| to_char(y) || ';X3';
             line := line || ';K"Totals for Center '||v_old_center||' on '||p_start_date||' and '||p_end_date||'"';
             p( line );
             
    			line := 'C;Y'|| to_char(y) || ';X8';
	         line := line || ';K'||v_ctr_tot_tagged;
            p( line );
            
            line := 'C;Y'|| to_char(y) || ';X9';
				line := line || ';K'||v_ctr_tot_deleted;
            p( line );
            
            line := 'C;Y'|| to_char(y) || ';X10';
				line := line || ';K'||v_ctr_tot_other_del;
            p( line );
    			
            SELECT
		         DECODE(greatest(greatest(v_ctr_tot_tagged, v_ctr_tot_deleted), v_ctr_tot_other_del), 0, 1,
		            greatest(greatest(v_ctr_tot_tagged, v_ctr_tot_deleted), v_ctr_tot_other_del))
		      INTO
		         v_max_count
		      FROM
		         DUAL;   				
    			
      	     line := 'F;P0;FG0G'||to_char(y)||';X11';
             p( line );
             line := 'C;Y'|| to_char(y) || ';X11';
             line := line || ';K'||(v_ctr_tot_avg_hdl_tm/v_max_count);
             p( line );
            
      	   v_max_count := 1;
      	   v_ctr_tot_tagged := 0;
      	   v_ctr_tot_deleted := 0;
    			v_ctr_tot_other_del	:= 0;
    			v_ctr_tot_avg_hdl_tm := 0;
    			v_team_tot_tagged := 0;
				v_team_tot_deleted := 0;
    			v_team_tot_other_del	:= 0;    			
    			v_team_tot_avg_hdl_tm := 0;
    			v_date_tot_tagged := 0;
				v_date_tot_deleted := 0;
    			v_date_tot_other_del	:= 0;    			
    			v_date_tot_avg_hdl_tm := 0;
    			
      	     y:=y+1;
      	     p( 'F;R'||to_char(y)||';FG0L;SM2' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"'||ret.center_name||'"';
             p( line );
              y:=y+1;
      	     center_y := y;
      	     date_y := y;
      	     p( 'F;R'||to_char(y)||';FG0L;SM3' );
      	   END;
      	end if;

      	p( 'F;R'||to_char(y)||';FG0L;SM3' );

        line := 'C;Y'|| to_char(y) || ';X2';
        line := line || ';K"'||ret.team_name||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X3';
        line := line || ';K"'||ret.identity_id||'"';
        p( line );
        
        line := 'C;Y'|| to_char(y) || ';X4';
        line := line || ';K"'||ret.last_name||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X5';
        line := line || ';K"'||ret.first_name||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X6';
        line := line || ';K"'||ret.order_date||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X7';
        line := line || ';K"'||ret.queue_type||'"';
        p( line );

        line := 'C;Y'|| to_char(y) || ';X8';
        line := line || ';K'||ret.Number_tagged;
        p( line );

        line := 'C;Y'|| to_char(y) || ';X9';
        line := line || ';K'||ret.same_day_del;
        p( line );

        line := 'C;Y'|| to_char(y) || ';X10';
        line := line || ';K'||ret.not_same_day_del;
        p( line );
        
			v_date_tot_tagged := v_date_tot_tagged + ret.number_tagged;
			v_date_tot_deleted := v_date_tot_deleted + ret.Same_day_del;
			v_date_tot_other_del	:= v_date_tot_other_del + ret.not_same_day_del;
			v_date_tot_avg_hdl_tm := v_date_tot_avg_hdl_tm + ret.avg_handle_time;

			v_team_tot_tagged := v_team_tot_tagged + ret.number_tagged;
			v_team_tot_deleted := v_team_tot_deleted + ret.Same_day_del;
			v_team_tot_other_del := v_team_tot_other_del + ret.not_same_day_del;
			v_team_tot_avg_hdl_tm := v_team_tot_avg_hdl_tm + ret.avg_handle_time;
			
			v_ctr_tot_tagged := v_ctr_tot_tagged + ret.number_tagged;
			v_ctr_tot_deleted := v_ctr_tot_deleted + ret.Same_day_del;
			v_ctr_tot_other_del := v_ctr_tot_other_del + ret.not_same_day_del;
			v_ctr_tot_avg_hdl_tm := v_ctr_tot_avg_hdl_tm + ret.avg_handle_time;
			
        line := 'F;P0;FG0G'||to_char(y)||';X11';
        p( line );
            
		  SELECT
		     DECODE(greatest(greatest(ret.number_tagged, ret.same_day_del),ret.not_same_day_del), 0, 1,
		     	  greatest(greatest(ret.number_tagged, ret.same_day_del),ret.not_same_day_del))
        INTO
           v_max_count
        FROM
           DUAL;
           
  		  line := 'C;Y'|| to_char(y) || ';X11';
        line := line || ';K"'||format_time(ret.avg_handle_time/v_max_count) || '"';
        p( line );

        y := y + 1;
        p( 'C;Y'||to_char(y) );

			if g_CLOB = 'N' then
				utl_file.fflush( g_file );
			end if;

		  v_max_count := 1;
		  v_old_team := ret.team_name;
        v_old_center := ret.center_name;
        v_old_date  := ret.order_date;

      END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 9 then
     print_comment( 'No Rows Returned' );

     line := 'C;Y'|| to_char(y) || ';X2';
     line := line || ';K"No Data Found"';
     p( line );

     y := y + 1;
     p( 'C;Y'||to_char(y) );

     if g_CLOB = 'N' then
        utl_file.fflush( g_file );
     end if;

  else

     --  Print sub totals for last date
     p( 'F;R'||to_char(y)||';FG0L;SM2' );
     line := 'C;Y'|| to_char(y) || ';X3';
     line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
     p( line );
     
		line := 'C;Y'|| to_char(y) || ';X8';
		line := line || ';K'||v_date_tot_tagged;
		p( line );

		line := 'C;Y'|| to_char(y) || ';X9';
		line := line || ';K'||v_date_tot_deleted;
		p( line );

		line := 'C;Y'|| to_char(y) || ';X10';
		line := line || ';K'||v_date_tot_other_del;
		p( line );


		  SELECT
		     DECODE(greatest(greatest(v_date_tot_tagged, v_date_tot_deleted), v_date_tot_other_del), 0, 1,
		     	  greatest(greatest(v_date_tot_tagged, v_date_tot_deleted), v_date_tot_other_del))
        INTO
           v_max_count
        FROM
           DUAL;   	  
     
     line := 'F;P0;FG0G'||to_char(y)||';X11';
     p( line );
     line := 'C;Y'|| to_char(y) || ';X11';
     line := line || ';K'||(v_date_tot_avg_hdl_tm/v_max_count);
     p( line );
            
     	v_max_count := 1;
     	v_date_tot_tagged := 0;
		v_date_tot_deleted := 0;
		v_date_tot_other_del	:= 0;
		v_date_tot_avg_hdl_tm := 0;
		
     y:=y+1;
     date_y := y;

     -- Print totals for last team
     BEGIN
			p( 'F;R'||to_char(y)||';FG0L;SM2' );
			line := 'C;Y'|| to_char(y) || ';X3';
			line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' THRU '||p_end_date||'"';
			p( line );

			line := 'C;Y'|| to_char(y) || ';X8';
			line := line || ';K'||v_team_tot_tagged;
			p( line );

			line := 'C;Y'|| to_char(y) || ';X9';
			line := line || ';K'||v_team_tot_deleted;
			p( line );

			line := 'C;Y'|| to_char(y) || ';X10';
			line := line || ';K'||v_team_tot_other_del;
			p( line );

		  SELECT
		     DECODE(greatest(greatest(v_team_tot_tagged, v_team_tot_deleted), v_team_tot_other_del), 0, 1,
		     	  greatest(greatest(v_team_tot_tagged, v_team_tot_deleted), v_team_tot_other_del))
        INTO
           v_max_count
        FROM
           DUAL;
	
			line := 'F;P0;FG0G'||to_char(y)||';X11';
			p( line );
			line := 'C;Y'|| to_char(y) || ';X11';
			line := line || ';K'||(v_team_tot_avg_hdl_tm/v_max_count);
			p( line );

			v_max_count := 1;
			v_team_tot_tagged := 0;
			v_team_tot_deleted := 0;
			v_team_tot_other_del	:= 0;
			v_team_tot_avg_hdl_tm := 0;

			y:=y+1;
			date_y := y;
     END;

     -- Print total for last center
     BEGIN
			p( 'F;R'||to_char(y)||';FG0L;SM2' );
			line := 'C;Y'|| to_char(y) || ';X3';
			line := line || ';K"Totals for Center '||ret.center_name||' on '||p_start_date||' and '||p_end_date||'"';
			p( line );

			line := 'C;Y'|| to_char(y) || ';X8';
			line := line || ';K'||v_ctr_tot_tagged;
			p( line );

			line := 'C;Y'|| to_char(y) || ';X9';
			line := line || ';K'||v_ctr_tot_deleted;
			p( line );

			line := 'C;Y'|| to_char(y) || ';X10';
			line := line || ';K'||v_ctr_tot_other_del;
			p( line );
	
		  SELECT
		     DECODE(greatest(greatest(v_ctr_tot_tagged, v_ctr_tot_deleted), v_ctr_tot_other_del), 0, 1,
		     	  greatest(greatest(v_ctr_tot_tagged, v_ctr_tot_deleted), v_ctr_tot_other_del))
        INTO
           v_max_count
        FROM
           DUAL;		
	
			line := 'F;P0;FG0G'||to_char(y)||';X11';
			p( line );
			line := 'C;Y'|| to_char(y) || ';X11';
			line := line || ';K'||(v_ctr_tot_avg_hdl_tm/v_max_count);
			p( line );

			v_max_count := 1;
			v_ctr_tot_tagged := 0;
			v_ctr_tot_deleted := 0;
			v_ctr_tot_other_del	:= 0;	
			v_ctr_tot_avg_hdl_tm := 0;

			y:=y+1;
			center_y := y;
			date_y := y;
			p( 'F;R'||to_char(y)||';FG0L;SM3' );	
     END;

  end if;

  close curs;

  return(y);

end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure show	(p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_center_name in varchar2,
		 p_team_name in varchar2,
		 p_user_id   in varchar2,
		 p_queue_type in varchar2,
		 p_submission_id in number,
		 p_CLOB in varchar2
		)
is
  v_start_date date;
  v_end_date date;
  l_row_cnt number(9);
  v_OUT_STATUS varchar2(30);
  v_OUT_MESSAGE varchar2(1000);

begin
    v_start_date := to_date(p_start_date,'mm/dd/yyyy');
    v_end_date   := to_date(p_end_date,'mm/dd/yyyy');

    if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
                                P_SUBMISSION_ID,
                                'SYS',
                                'S' ,
                                null ,
                                v_OUT_STATUS ,
                                v_OUT_MESSAGE
                        );
       commit;
       rpt.report_pkg.INSERT_REPORT_DETAIL_CHAR
       (
          P_SUBMISSION_ID,
          empty_clob(),--IN_REPORT_DETAIL
          v_out_status,
          v_out_message
        );
    else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','PRO03_Cust_Serv_Queue_Detail.slk', 'W' );
    end if;

	  if p_start_date is NULL then
		  v_start_date := sysdate;
	  else
		  v_start_date := to_date(p_start_date, 'mm/dd/yyyy');
	  end if;

	  if p_end_date is NULL then
		  v_end_date := sysdate;
	  else
		  v_end_date := to_date(p_end_date, 'mm/dd/yyyy');
	  end if;

	  print_title(	v_start_date,
			v_end_date,
			p_center_name,
			p_team_name,
			p_user_id,
			p_queue_type);

	  print_headings(6);

	  l_row_cnt := print_rows
				(to_char(v_start_date, 'mm/dd/yyyy'),
				 to_char(v_end_date, 'mm/dd/yyyy'),
				 p_center_name,
				 p_team_name,
				 p_user_id,
				 p_queue_type
				);

	  print_widths;

	  p( 'E' );

    if p_CLOB = 'Y' then
		rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
			(
			P_SUBMISSION_ID,
			'SYS',
			'C' ,
			null ,
			v_OUT_STATUS ,
			v_OUT_MESSAGE
			);
		commit;
    else
		utl_file.fflush( g_file );
		utl_file.fclose( g_file );
    end if;

end show;

end;
.
/
