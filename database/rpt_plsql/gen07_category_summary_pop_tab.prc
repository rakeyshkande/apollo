CREATE OR REPLACE
procedure rpt.gen07_category_summary_pop_tab

(p_start_date in varchar2,
 p_end_date in varchar2,
 p_origin_code in varchar2)

is
	v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
	v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
   v_origin_predicate	VARCHAR2(1000);
   v_origin_predicate1	VARCHAR2(1000);
   v_sql			VARCHAR2(10000);

begin

  IF p_origin_code <>'ALL' THEN
	v_origin_predicate := '	AND fao.origin_id in ('|| rpt.multi_value(p_origin_code)||') ';
	v_origin_predicate1 := '	AND co.origin_id in ('|| rpt.multi_value(p_origin_code)||') ';
  END IF;

v_sql :=
'insert into rpt.category_tab 	(col1,
			col2,
			col3,
			col4,
			col5)
select	to_char(co.order_date,''dd'') order_date_char,
	cod.order_detail_id order_detail_id,
	fao.origin_type origin_type,
	co.company_id,
	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category
from 	clean.orders co,
          	clean.order_details cod,
          	ftd_apps.product_master fapm,
          	ftd_apps.vendor_master favm,
          	ftd_apps.origins fao,
	ftd_apps.codified_products facp,
	ftd_apps.codification_master facm
where co.order_guid=cod.order_guid
and cod.product_id=fapm.product_id
and fapm.product_id=facp.product_id(+)
and facp.codification_id=facm.codification_id(+)
and co.origin_id=fao.origin_id
and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate
||
' and not exists	(select ''x''
              	from clean.order_details cod1
              	where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
              	and co.order_guid=cod1.order_guid)            	
group by to_char(co.order_date,''dd''),
	cod.order_detail_id,
	fao.origin_type,
	co.company_id,
	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end';
EXECUTE IMMEDIATE v_sql;	

v_sql :=
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''99'',
	category,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
                      	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'              and (case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end) in (''DS'',''DSF'',''DSP'')
           	and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
                           	fapm.standard_price)
group by ''99'',
	category';
EXECUTE IMMEDIATE v_sql;	

v_sql :=
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''98'',
	company_id company,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
		co.company_id,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'             and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            fapm.standard_price,
		co.company_id)
group by ''98'',
	company_id';
EXECUTE IMMEDIATE v_sql;	

v_sql :=
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''97'',
	category,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
                      	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and co.company_id=''GIFT''
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'              and (case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end) in (''DS'',''DSF'',''DSP'')
           	and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
                           	fapm.standard_price)
group by ''97'',
	category';
EXECUTE IMMEDIATE v_sql;	

v_sql := 
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''96'',
	category,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
                      	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and co.company_id=''HIGH''
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'             and (case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end) in (''DS'',''DSF'',''DSP'')
           	and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
                           	fapm.standard_price)
group by ''96'',
	category';
EXECUTE IMMEDIATE v_sql;

v_sql :=
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''95'',
	category,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
                      	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and co.company_id=''FLORIST''
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'             and (case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end) in (''DS'',''DSF'',''DSP'')
           	and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
                           	fapm.standard_price)
group by ''95'',
	category';
EXECUTE IMMEDIATE v_sql;	

v_sql :=
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''94'',
	category,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
                      	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and co.company_id=''FUSA''
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'              and (case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end) in (''DS'',''DSF'',''DSP'')
           	and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
                           	fapm.standard_price)
group by ''94'',
	category';
EXECUTE IMMEDIATE v_sql;	

v_sql :=
'insert into rpt.category_tab	(col1,
			col2,
			col3)
select 	''93'',
	category,
             	sum(total_revenue) total_revenue
from	(select 	fapm.standard_price,
       		fapm.department_code,
                      	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
        		(count(co.order_guid) * fapm.standard_price) total_revenue
           	from 	clean.orders co,
          	      	clean.order_details cod,
          	      	ftd_apps.product_master fapm,
           		ftd_apps.codified_products facp,
		ftd_apps.codification_master facm
	where co.order_guid=cod.order_guid
	and cod.product_id=fapm.product_id
	and fapm.product_id=facp.product_id(+)
	and facp.codification_id=facm.codification_id(+)
	and co.company_id=''FDIRECT''
	and trunc(co.order_date) between  '''|| v_start_date||''' AND '''||
                                            v_end_date||''''
||v_origin_predicate1
||
'              and (case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end) in (''DS'',''DSF'',''DSP'')
           	and not exists	(select ''x''
                                          from clean.order_details cod1
                                          where cod.order_disp_code in (''In-Scrub'',''Pending'',''Removed'')
                                           and co.order_guid=cod1.order_guid)
              group by 	fapm.department_code,
                            case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
                           	fapm.standard_price)
group by ''93'',
	category';
EXECUTE IMMEDIATE v_sql;	

end;
.
/
