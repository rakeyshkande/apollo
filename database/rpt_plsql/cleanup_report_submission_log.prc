CREATE OR REPLACE
PROCEDURE rpt.cleanup_report_submission_log IS
BEGIN
   delete from rpt.report_submission_log
   where completed_on is null
   and report_status = 'O'
   and submitted_on < SYSDATE - 5;
   COMMIT;
END cleanup_report_submission_log;
.
/
