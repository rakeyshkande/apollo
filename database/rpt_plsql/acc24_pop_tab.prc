CREATE OR REPLACE
PROCEDURE rpt.ACC24_POP_TAB
	(
	 p_start_date in varchar2,
	 p_end_date in varchar2
	)
IS
  v_start_date	date := to_date(p_start_date,'mm/dd/yyyy');
  v_end_date	date := to_date(p_end_date,'mm/dd/yyyy');


BEGIN
--
--**************************************************************
--
-- ACC24 Accounting Report - Price Variance Report Amazon
--
--**************************************************************
--
INSERT into RPT.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13
	)
SELECT	 azpv.confirmation_number,
	 to_char(azpv.order_timestamp, 'dd-mon-yyyy hh24:mi:ss') order_timestamp,
	 azpv.product_id,
	 azpv.amazon_price,
	 azpv.amazon_shipping,
	 azpv.amazon_service_fee,
	 azpv.amazon_taxes,
	 azpv.amazon_shipping_taxes,
	 azpv.ftd_price,
	 azpv.ftd_shipping,
	 azpv.ftd_service_fee,
	 azpv.ftd_taxes,
	 azpv.ftd_shipping_taxes
--
FROM	 amazon.az_price_variance azpv
--
WHERE	 trunc(azpv.order_timestamp) between v_start_date and v_end_date
--
ORDER BY azpv.order_item_number
;
END
;
.
/
