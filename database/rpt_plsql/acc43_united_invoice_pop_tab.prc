CREATE OR REPLACE
PROCEDURE rpt.acc43_united_invoice_pop_tab
	(p_start_date in varchar2,
	 p_end_date in varchar2
	)
IS
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');


BEGIN
--
--**************************************************************
--
-- ACC43 Accounting Report - United Mileage Plus Invoice Report
--
--**************************************************************
--
--
--**************************************************************
--  Orders
--**************************************************************
--


INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15,
	 col16
	)
SELECT	
	p.payment_id,
	od.external_order_number,
	o.order_date,
	global.encryption.decrypt_it(aa.ap_account_txt,aa.key_name) ap_account_txt,
	c.first_name || ' ' || c.last_name,
	od.product_id,
	pm.product_name,
	ob.product_amount,
	ob.add_on_amount,
	ob.shipping_fee,
	ob.service_fee,
	ob.discount_amount,
	ob.tax,
	ob.miles_points_amt,
	pmmp.commission_pct,
	1 sort_order
from clean.payments p
join clean.orders o
	on p.order_guid=o.order_guid
join clean.order_details od
	on o.order_guid=od.order_guid
join clean.ap_accounts aa
	on p.ap_account_id = aa.ap_account_id
join clean.customer c
	on o.customer_id = c.customer_id
join ftd_apps.product_master pm
	on od.product_id = pm.product_id
join ftd_apps.payment_method_miles_points pmmp
	on p.payment_type = pmmp.payment_method_id
JOIN clean.order_bills ob
	ON      od.order_detail_id = ob.order_detail_id
        AND     ob.bill_status in ('Billed', 'Settled')
        and trunc(ob.bill_date) between v_start_date and v_end_date
        AND     ((p.additional_bill_id is null and ob.additional_bill_indicator = 'N')
        OR       (p.additional_bill_id = ob.order_bill_id and ob.additional_bill_indicator = 'Y'))
--
WHERE	p.payment_type = 'UA'
AND	p.payment_indicator='P'
--
ORDER BY o.order_date, od.external_order_number;
--
--
--**************************************************************
--  Refunds
--**************************************************************
--
INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15,
	 col16
	)
SELECT	
	cp.payment_id,
	cod.external_order_number,
	co.order_date,
	global.encryption.decrypt_it(aa.ap_account_txt,aa.key_name) ap_account_txt,
	c.first_name || ' ' || c.last_name,
	cod.product_id,
	pm.product_name,
	-cr.refund_product_amount,
	-cr.refund_addon_amount,
	-cr.refund_shipping_fee,
	-cr.refund_service_fee,
	-cr.refund_discount_amount,
	-cr.refund_tax,
	-cpr.miles_points_debit_amt miles_points_amt,
	pmmp.commission_pct,
	2 sort_order
--
	FROM		clean.orders co
	JOIN		clean.payments cp
				on (cp.order_guid = co.order_guid)
	JOIN		clean.order_details cod
				on (cod.order_guid = co.order_guid)
	JOIN		clean.refund cr
				on cr.order_detail_id = cod.order_detail_id
	JOIN		clean.payments cpr
				on cr.refund_id = cpr.refund_id
join clean.ap_accounts aa
	on cp.ap_account_id = aa.ap_account_id
join clean.customer c
	on co.customer_id = c.customer_id
join ftd_apps.product_master pm
	on cod.product_id = pm.product_id
join ftd_apps.payment_method_miles_points pmmp
	on cp.payment_type = pmmp.payment_method_id
	--
	WHERE		cr.refund_status = 'Billed'
	AND		cr.refund_disp_code <> 'E10'
	AND		cp.payment_indicator = 'P'
	AND		cp.additional_bill_id IS NULL
	AND		cp.payment_type = 'UA'
	AND		trunc(cr.refund_date) between v_start_date and v_end_date
ORDER BY co.order_date, cod.external_order_number;

END;
.
/
