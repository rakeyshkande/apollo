CREATE OR REPLACE
PACKAGE BODY rpt.REPORT_CLOB_PKG
AS

PROCEDURE CLOB_VARIABLE_UPDATE
(
IN_STR                          IN VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will append p_str to the global clob variable
-----------------------------------------------------------------------------*/
BEGIN

        g_clob := g_clob || in_str || chr(10);

END CLOB_VARIABLE_UPDATE;

PROCEDURE CLOB_VARIABLE_RESET
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will append p_str to the global clob variable
-----------------------------------------------------------------------------*/
BEGIN

        g_clob := NULL;

END CLOB_VARIABLE_RESET;


PROCEDURE INSERT_REPORT_DETAIL_CHAR
(
IN_REPORT_SUBMISSION_ID         IN REPORT_DETAIL_CHAR.REPORT_SUBMISSION_ID%TYPE,
IN_REPORT_DETAIL                IN REPORT_DETAIL_CHAR.REPORT_DETAIL%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the report detail clob

Input:
        report_submission_id            number
        report_detail                   clob
        expiration_date                 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR report_cur IS
        SELECT  sysdate + nvl (r.retention_days, 0) expiration_date
        FROM    report r
        WHERE   exists
        (       select  1
                from    report_submission_log l
                where   l.report_submission_id = in_report_submission_id
                and     l.report_id = r.report_id
        );

v_expiration_date       date;

BEGIN

OPEN report_cur;
FETCH report_cur INTO v_expiration_date;
CLOSE report_cur;

INSERT INTO report_detail_char
(
        report_submission_id,
        report_detail,
        expiration_date
)
VALUES
(
        in_report_submission_id,
        in_report_detail,
        v_expiration_date
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REPORT_DETAIL_CHAR;


PROCEDURE UPDATE_REPORT_DETAIL_CHAR
(
IN_REPORT_SUBMISSION_ID         IN REPORT_DETAIL_CHAR.REPORT_SUBMISSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for adding to the report detail

Input:
        report_submission_id            number
        report_detail                   clob

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  report_detail_char
SET
        report_detail = g_clob
WHERE   report_submission_id = in_report_submission_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REPORT_DETAIL_CHAR;


END;
.
/
