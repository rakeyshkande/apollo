CREATE OR REPLACE
PROCEDURE rpt.acc22_remit_amazon_pop_tab
(p_start_date in varchar2,
p_end_date in varchar2)
IS
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
BEGIN
insert into rpt.rep_tab (col1,col2,col3,col4,col5,col6,col7)
select      cod.external_order_number  Order_number,
            co.order_date              order_date,
            asor.az_order_id           Amazon_order_id,
            aaor.az_order_related_id   ftd_adj_id,
            asd.processed_date         settlement_date,
            (   nvl(asoi.item_price,0) +
                nvl(asoi.tax,0) +
                nvl(asoi.shipping_tax,0) +
                nvl(asoi.commission,0)) settlement_amount,
            asd.az_settlement_data_id settlement_number
from amazon.settlement_order_item asoi
right outer join amazon.settlement_order asor
    on (asoi.settlement_detail_id = asor.settlement_detail_id)
join amazon.az_order_related aaor
    on (asoi.confirmation_number = aaor.confirmation_number)
left outer join clean.order_details cod
    on ( asoi.confirmation_number = cod.external_order_number)
left outer join     clean.orders co
    on (co.order_guid = cod.order_guid)
join amazon.settlement_detail asdl
    on (asdl.settlement_detail_id = asor.settlement_detail_id)
join amazon.settlement_data asd
    on (asd.AZ_SETTLEMENT_DATA_ID=asdl.AZ_SETTLEMENT_DATA_ID)
where aaor.type_feed='OrderAcknowledgement'
and trunc(asd.processed_date) between v_start_date and v_end_date
UNION
select      cod.external_order_number  Order_number,
            co.order_date              order_date,
            asor.az_order_id           Amazon_order_id,
            aaor.az_order_related_id   ftd_adj_id,
            asd.processed_date         settlement_date,
            (   nvl(asai.item_price,0) +
                nvl(asai.tax,0) +
                nvl(asai.shipping_tax,0) +
                nvl(asai.commission,0)) settlement_amount,
            asd.az_settlement_data_id settlement_number
from amazon.settlement_adjustment_item asai
join AMAZON.AZ_ORDER_DETAIL aaod
	on (aaod.AZ_ORDER_ITEM_NUMBER = asai.AMAZON_ORDER_ITEM_CODE)
join clean.order_details cod
	on (aaod.confirmation_number  = cod.external_order_number)
left outer join     clean.orders co
    on (co.order_guid = cod.order_guid)
join clean.accounting_transactions cat
	on (cat.order_detail_id = cod.order_detail_id)
right outer join      amazon.settlement_order asor
	on (asai.settlement_detail_id = asor.settlement_detail_id)
join     amazon.az_order_related aaor
	on (asai.confirmation_number = aaor.confirmation_number)
join amazon.settlement_detail asdl
	on (asdl.settlement_detail_id = asor.settlement_detail_id)
join amazon.settlement_data asd
	on (asd.AZ_SETTLEMENT_DATA_ID=asdl.AZ_SETTLEMENT_DATA_ID)
where aaor.type_feed='OrderAcknowledgement'
and trunc(asd.processed_date) between v_start_date and v_end_date
order by
     case co.master_order_number
          when null then 'Z'
          else co.master_order_number
     end
;
END;
.
/
