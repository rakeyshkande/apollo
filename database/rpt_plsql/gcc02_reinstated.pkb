CREATE OR REPLACE
PACKAGE BODY rpt.GCC02_Reinstated IS

  count_v		number(9)  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure p( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
	rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     --	when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end p;

/*********************************************************************/
/* The next set of lines are the column headers for the excel file.  */
/* All you need to change in this section is the values used for the */
/* column headings.                                                  */
/*                                                                   */
/*********************************************************************/
  procedure print_headings(y in number)
  is
  BEGIN
		p( 'F;R2;FG0C;SM2' );
    p( 'C;Y2;X2;K"REQUEST #"' );
    p( 'C;X3;K"GIFT CERT #"' );
    p( 'C;X4;K"REINSTATE DATE"' );
    p( 'C;X5;K"PROGRAM NAME"' );
    p( 'C;X6;K"COMPANY"' );
    p( 'C;X7;K"ISSUE AMOUNT"' );
    p( 'C;X8;K"PAID AMOUNT"' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_headings;

  procedure     print_title(  p_start_date in date,
                              p_end_date   in date)
  is
    l_title varchar2(2000);
    v_month varchar2(10);

  begin

  	v_month := to_char(add_months(sysdate,-1),'mm/yyyy');
    p( 'ID;ORACLE' );
    p( 'P;Ph:mm:ss');
    p( 'P;FCourier;M200' );
    p( 'P;FCourier;M200;SB' );
    p( 'P;FCourier;M200;SUB' );
    p( 'P;FCourier;M160' );
    p( 'F;C1;FG0R;SM1');
    p( 'F;R1;FG0C;SM2' );
if p_start_date is null then
	  p( 'C;Y1;X4;K" GIFT CERTIFICATE REINSTATED REPORT FOR '||v_month||'"' );
else
    p( 'C;Y1;X4;K"GIFT CERTIFICATES REINSTATED REPORT FOR '||to_char(p_start_date,'mm/dd/yyyy')||' THRU '||to_char(p_end_date,'mm/dd/yyyy')||'"' );
end if;
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_title;

--**********************************************************************************************************
  procedure print_widths is
  begin
    p( 'F;W1 1 5' );
    p( 'F;W2 2 15' );
    p( 'F;W3 3 15' );
    p( 'F;W4 20 15' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_widths;
--**************************************************************************************************************
function print_rows(p_start_date in date,
                    p_end_date   in date) return number is
type    my_curs_type is REF CURSOR;
curs    my_curs_type;
TYPE    type_total_tab_type is table of varchar2(10)
        index by pls_integer;
type_total_tab type_total_tab_type; -- Keeps track of the cells that contain totals for each date changes
                                    -- Will be used for total of each team
--team_total_tab type_total_tab_type; -- Keeps track of the cells that contain totals for each Team changes
                                    -- Will be used for total of each team
null_tab       type_total_tab_type;  -- This is used to clear the tab
i       number(4) := 1;             -- Index for type_total_tab
t       number(4) := 1;             -- Index for team_total_tab
type_y number := 3;
v_old_type   varchar2(80) := 'XXXX';
str     varchar2(5000);
type ret_rec is record
              (request_number  varchar2(80),
              gc_number varchar2(80),
       	      reinstate_date varchar2(15),
      	      program_name varchar2(80),
      	      company varchar2(80),
              issue_amt  number,
              paid_amt  number);
v_start_date_pred varchar2(300);
ret     ret_rec;
row_cnt number          := 0;
line    varchar2(32767) := null;
n       number;
y       number;


--*****************************************************************************
--*****************************************************************************
--*
--*****************************************************************************
--*****************************************************************************

begin

-----------------------------------------------------
--  Building the predicates (Where clause conditions)
-----------------------------------------------------


if p_start_date is null then
   v_start_date_pred := ' WHERE trunc(cgch.reinstate_date) between add_months(trunc(sysdate,''MONTH''),-1)
                                              and last_day(add_months(sysdate,-1)) ';
else
	  v_start_date_pred := ' WHERE trunc(cgch.reinstate_date) between trunc(to_date('''||p_start_date||'''))
                               and trunc(to_date('''||p_end_date||''')) ';
end if;

-----------------------------------------------------
--  Define the SELECT
-----------------------------------------------------

str := 'select cgcr.request_number request_number,
             cgch.gc_coupon_number gc_number,
             to_char(cgch.reinstate_date,''mm/dd/yyyy'') reinstate_date,
           cgcr.program_name program_name,
           facm.company_name company,
           cgcr.issue_amount issue_amt,
           cgcr.paid_amount paid_amt
from     clean.gc_coupons cgc
join     clean.gc_coupon_request cgcr
on(cgc.request_number=cgcr.request_number)
join     clean.gc_coupon_history cgch
on(cgc.gc_coupon_number=cgch.gc_coupon_number)
join     ftd_apps.company_master facm
on(cgcr.company_id=facm.company_id) '||
v_start_date_pred||
' order by cgcr.request_number,
cgch.reinstate_date';



--dbms_output.put_line(substr(str,-240,240));


p( 'C;Y3' );
y := 3;


-----------------------------------------------------
--  Process the Cursor
-----------------------------------------------------

      OPEN curs for str;
      LOOP
      	FETCH curs  into ret;
      	exit when curs%notfound;

/*********************************************************************/
/* Two notes here.  First you want the number next to the "X" to     */
/* match the number next to the "C" in your column heading part.     */
/* Second you do not want to repeat a number next to the "X".  If    */
/* do it will not show up correctly in the excel file.               */
/*********************************************************************/

        line := 'C;X2'; -- || to_char(count_v);
        line := line || ';K"'||ret.request_number||'"';
        p( line );

        line := 'C;X3'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.gc_number||'"';
        p( line );

        line := 'C;X4'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.reinstate_date||'"';
        p( line );

        line := 'C;X5'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.program_name||'"';
        p( line );

        line := 'C;X6'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.company||'"';
        p( line );

        p( 'F;F$2G;X7' );--to add a dollar sign and 2 decimal places
        line := 'C;X7'; -- || to_char(count_v+1);
        line := line || ';K'||ret.issue_amt||'';
        p( line );

        p( 'F;F$2G;X8' );--to add a dollar sign and 2 decimal places
        line := 'C;X8'; -- || to_char(count_v+1);
        line := line || ';K'||ret.paid_amt||'';
        p( line );

/*********************************************************************/
/* The next two lines are for increase the line number and create a  */
/* new record line.                                                  */
/*                                                                   */
/*********************************************************************/

        y := y + 1;
        p( 'C;Y'||to_char(y) );

	  if g_CLOB = 'N' then
	     utl_file.fflush( g_file );
	  end if;

      END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 3 then

      line := 'C;Y'|| to_char(y) || ';X2';
      line := line || ';K"No Data Found"';
      p( line );

      y := y + 1;
      p( 'C;Y'||to_char(y) );

      if g_CLOB = 'N' then
	 utl_file.fflush( g_file );
      end if;

  else

    p( 'C;Y' || to_char(y + 3) );
    p( 'F;X3;FG0R;SM1' );
    p( 'C;X3;K"Totals:"' );

    p( 'F;X7;FG0R;SM1' );
    p( 'C;X7;ESUM(R3C:R' ||
                to_char(y+2) || 'C)' );

    p( 'F;X8;FG0R;SM1' );
    p( 'C;X8;ESUM(R3C:R' ||
                to_char(y+2) || 'C)' );

  end if;

  return row_cnt;

  end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure show(p_start_date in varchar2,
                 p_end_date in varchar2,
		 p_submission_id in number,
		 p_CLOB in varchar2
		)
is
   v_start_date date;
   v_end_date date;
   l_row_cnt number(9);
   v_OUT_STATUS_det	varchar2(30);
   v_OUT_MESSAGE_det	varchar2(1000);
   v_OUT_STATUS_sub 	varchar2(30);
   v_OUT_MESSAGE_sub	varchar2(1000);

begin

--
-- Prepare for either CLOB output or REPORT_DIR output
--

   if p_start_date is not null then
	v_start_date := to_date(p_start_date, 'mm/dd/yyyy');
   end if;

   if p_end_date is not null then
	v_end_date := to_date(p_end_date, 'mm/dd/yyyy');
   end if;

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
			P_SUBMISSION_ID,
			'SYS',
			'S' ,
			null ,
			v_OUT_STATUS_DET,
			v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
       			(
			P_SUBMISSION_ID,
			empty_clob(),
			v_out_status_det,
			v_out_message_det
		        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','GCC02_Gift_Cert_Reinstated.slk', 'W' );
   end if;


    print_title(  v_start_date,
                  v_end_date);

    print_headings(2);

    l_row_cnt := print_rows(v_start_date,
                    v_end_date);

    print_widths;

    p( 'E' );

  --
  -- Update Database with CLOB and set report submission status (if CLOB'ing)
  -- Close report file (if not CLOB'ing
  --

     if p_CLOB = 'Y' then
        begin
  	-- update the report_detail_char table with the package global variable g_clob
  	rpt.report_clob_pkg.update_report_detail_char
  			(
  			 p_submission_id,
  			 v_out_status_det,
  			 v_out_message_det
  			);
  	if v_out_status_det = 'N' then
  	-- if db update failure, update report_submission_log with ERROR status
  	rpt.report_pkg.update_report_submission_log
  			(
  			 p_submission_id,
  			 'REPORT',
  			 'E',
  			 v_out_message_det,
  			 v_out_status_sub,
  			 v_out_message_sub
  			);
  	else
  	-- if db update successful, update report_submission_log with COMPLETE status
  	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
  			(
  			 P_SUBMISSION_ID,
  			 'SYS',
  			 'C' ,
  			 null ,
  			 v_out_status_det ,
  			 v_out_message_det
  			);
  	end if;
  	--exception
  	--	when others then srw.message(32321,'Error in p');
       end;
       commit;
     else
       utl_file.fflush( g_file );
       utl_file.fclose( g_file );
   end if;
  end show;
end;
.
/
