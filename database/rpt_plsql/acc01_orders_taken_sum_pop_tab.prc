CREATE OR REPLACE
PROCEDURE rpt.acc01_orders_taken_sum_pop_tab
			(p_start_date in varchar2,
			 p_end_date in varchar2,
			 p_company_code in varchar2,
			 p_origin in varchar2
			)
IS
  v_company_predicate	varchar2(100);
  v_origin_predicate	varchar2(100);
  v_sql			varchar2(10000);
  v_start_date		date := to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		date := to_date(p_end_date,'mm/dd/yyyy');

BEGIN

--
--**********************************************************************
-- Initalize rep_tab table
--**********************************************************************
--
  insert into rpt.rep_tab
	(col1,
	 col2)
  select 1,
	 'Phone'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 1,
	 'Internet'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 1,
	 'Florist Delivered'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 1,
	 'Drop Ship Delivered'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 2,
	 'Phone'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 2,
	 'Internet'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 2,
	 'Florist Delivered'
  from Dual;

  insert into rpt.rep_tab
	(col1,
	col2)
  select 2,
	 'Drop Ship Delivered'
  from Dual;
--
--**********************************************************************
-- Set the company code and origin id WHERE clause predicates
--**********************************************************************
--
  if p_company_code<>'ALL' then
	v_company_predicate:= ' AND co.company_id in ('||rpt.multi_value(p_company_code)||') ';
  end if;

  if p_origin <>'ALL' then
	v_origin_predicate:= ' AND nvl(co.order_taken_call_center,co.origin_id) in ('||rpt.multi_value(p_origin)||') ';
  end if;
--
--**********************************************************************
-- Gross Orders Taken Phone or Internet
--**********************************************************************
--
  v_sql :=
	'insert into rpt.rep_tab
		(col1,
		col2,
		col3,
		col4,
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11,
		col12)
	select 1,
		case fao.origin_type
               when ''phone'' then ''Phone''
               when ''bulk''  then ''Phone''
               when ''ariba'' then ''Internet''
               when ''internet'' then ''Internet''
       end ,
             sum(decode(cat.transaction_type,''Order'',1,0)) order_count,
             sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) tax,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax)             total_order_amount,
             ((sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))/sum(decode(cat.transaction_type,''Order'',1,0))) AVO
from clean.order_details cod
join clean.orders co
on (co.order_guid=cod.order_guid)
join ftd_apps.origins fao
on (co.origin_id = fao.origin_id)
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
join ftd_apps.product_master pm
    on pm.product_id = cod.product_id
    and pm.product_type <> ''SERVICES''
where fao.origin_type in (''internet'',''phone'',''bulk'',''ariba'')
and trunc(cat.transaction_date) between '''|| v_start_date||''' and '''||
                                            v_end_date||''''
||v_company_predicate
||v_origin_predicate
||
' and cod.order_disp_code not in (''In-Scrub'',''Pending'',''Removed'')
and cat.transaction_type <> ''Refund''
and cat.payment_type <> ''NC''
and co.origin_id <>''TEST''
group by  case fao.origin_type
               when ''phone'' then ''Phone''
               when ''bulk''  then ''Phone''
               when ''ariba'' then ''Internet''
               when ''internet'' then ''Internet''
	end'
;
execute immediate v_sql
;
--
--**********************************************************************
-- Net Orders Taken Florists Dropship-Delivered
--**********************************************************************
--
  v_sql :=
	'insert into rpt.rep_tab
	(col1,
col2,
col3,
col4,
col5,
col6,
col7,
col8,
col9,
col10,
col11,
col12)
select 1,case nvl(cat.ship_method,''SD'')
              when ''SD'' then ''Florist Delivered''
              Else ''Drop Ship Delivered''
             end ,
             sum(decode(cat.transaction_type,''Order'',1,0)) order_count,
             sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) tax,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax)             total_order_amount,
             decode(count(cat.accounting_transaction_id),0,0,(sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)))/sum(decode(cat.transaction_type,''Order'',1,0))) AVO
from clean.order_details cod
join clean.orders co
on (co.order_guid = cod.order_guid)
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
join ftd_apps.product_master pm
    on pm.product_id = cod.product_id
    and pm.product_type <> ''SERVICES''
WHERE trunc(cat.transaction_date) between '''|| v_start_date||''' and '''||
                                            v_end_date||''''
||v_company_predicate
||v_origin_predicate
||' and cod.order_disp_code not in (''In-Scrub'',''Pending'',''Removed'')
and cat.transaction_type <> ''Refund''
and cat.payment_type <> ''NC''
and co.origin_id <>''TEST''
group by case nvl(cat.ship_method,''SD'')
              when ''SD'' then ''Florist Delivered''
              Else ''Drop Ship Delivered''
	end'
;
execute immediate v_sql
;
--
--**********************************************************************
-- Net Contra Rev Orders Taken Phone or Internet
--**********************************************************************
--
v_sql :=
'insert into rpt.rep_tab
(col1,
col2,
col3,
col4,
col5,
col6,
col7,
col8,
col9,
col10,
col11,
col12)
select 2,case fao.origin_type
              when ''phone'' then ''Phone''
              when ''bulk''  then ''Phone''
               when ''ariba'' then ''Internet''
               when ''internet'' then ''Internet''
       end ,
              sum(decode(substr(cat.refund_disp_code,1,1),''A'',1,0))order_count,
             sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) tax,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax)             total_order_amount,
             decode(sum(decode(substr(cat.refund_disp_code,1,1),''A'',1,0)),0,0,(sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)))/sum(decode(substr(cat.refund_disp_code,1,1),''A'',1,0))) AVO
from clean.order_details cod
join clean.orders co
on  (co.order_guid=cod.order_guid)
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
join  clean.refund_disposition_val crdv
on (crdv.refund_disp_code = cat.refund_disp_code)
join ftd_apps.origins fao
on (co.origin_id = fao.origin_id)
join ftd_apps.product_master pm
    on pm.product_id = cod.product_id
    and pm.product_type <> ''SERVICES''

where fao.origin_type in (''internet'',''phone'',''ariba'',''bulk'')
and trunc(cat.transaction_date) between '''|| v_start_date||''' and '''||
                                            v_end_date||''''
||v_company_predicate
||v_origin_predicate
||' and crdv.refund_accounting_type = ''CR''
and cod.order_disp_code not in (''In-Scrub'',''Pending'',''Removed'')
and cat.payment_type <> ''NC''
and co.origin_id <>''TEST''
group by  case fao.origin_type
               when ''phone'' then ''Phone''
               when ''bulk''  then ''Phone''
               when ''ariba'' then ''Internet''
               when ''internet'' then ''Internet''
	end'
;
execute immediate v_sql
;
--
--**********************************************************************
-- Contra Florists Dropship-Delivered
--**********************************************************************
--
v_sql :=
'insert into rpt.rep_tab
(col1,
col2,
col3,
col4,
col5,
col6,
col7,
col8,
col9,
col10,
col11,
col12)
select 2,case nvl(cat.ship_method,''SD'')
              when ''SD'' then ''Florist Delivered''
              Else ''Drop Ship Delivered''
             end,
              sum(decode(substr(cat.refund_disp_code,1,1),''A'',1,0))order_count,
             sum(cat.Product_amount) Merch_value,
             sum(cat.add_on_amount+nvl(cat.add_on_discount_amount,0)) add_ons,
             sum(cat.shipping_fee) shipping_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  service_fee,
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount+nvl(cat.add_on_discount_amount,0))) discount_amount,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Order_value,
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax) tax,
             (sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount))+
             sum(cat.tax+cat.shipping_tax+cat.service_fee_tax)             total_order_amount,
             decode(sum(decode(substr(cat.refund_disp_code,1,1),''A'',1,0)),0,0,(sum(cat.Product_amount)+
             sum(cat.add_on_amount) +
             sum(cat.shipping_fee) +
             sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee))  -
             sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)))/sum(decode(substr(cat.refund_disp_code,1,1),''A'',1,0))) AVO
from clean.order_details cod
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
join  clean.refund_disposition_val crdv
on (crdv.refund_disp_code = cat.refund_disp_code)
join clean.orders co
on (cod.order_guid = co.order_guid)
join ftd_apps.product_master pm
    on pm.product_id = cod.product_id
    and pm.product_type <> ''SERVICES''
where crdv.refund_accounting_type = ''CR''
and trunc(cat.transaction_date) between '''|| v_start_date||''' and '''||
                                            v_end_date||''''
||v_company_predicate
||v_origin_predicate
||' and cod.order_disp_code not in (''In-Scrub'',''Pending'',''Removed'')
and cat.payment_type <> ''NC''
and co.origin_id <>''TEST''
group by case nvl(cat.ship_method,''SD'')
              when ''SD'' then ''Florist Delivered''
              Else ''Drop Ship Delivered''
	end'
;
execute immediate v_sql
;
END
;
.
/
