CREATE OR REPLACE
PROCEDURE rpt.ACC21_POP_TAB
	(
	 p_thru_date in varchar2
	)
IS
v_date date := to_date(p_thru_date,'mm/dd/yyyy');

BEGIN
--
--**************************************************************
--
-- ACC21 Accounting Report - Aging Report Amazon
--
--**************************************************************
--
INSERT into RPT.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11
	)
	SELECT	CASE	WHEN trunc(to_number(sysdate - cod.eod_delivery_date)) between 00 and 30 then 30
			WHEN trunc(to_number(sysdate - cod.eod_delivery_date)) between 31 and 60 then 60
			WHEN trunc(to_number(sysdate - cod.eod_delivery_date)) between 61 and 100 then 90
			ELSE 91
		END age_group,
		trunc(to_number(sysdate - cod.eod_delivery_date)) days_aged,
		azor.az_order_related_id az_order_related_id,
		asor.az_order_id az_order_id,
		trim(co.order_date) order_date,
		trim(cod.eod_delivery_date) delivery_date,
		co.master_order_number master_order_number,
		cod.external_order_number external_order_number,
		nvl((cat.product_amount +
			cat.add_on_amount +
			cat.shipping_fee +
			cat.service_fee +
			cat.discount_amount +
			cat.shipping_tax +
			cat.service_fee_tax +
			cat.tax),0) transaction_amount,
		nvl(cat.wholesale_amount,0) wholesale_amount,
		nvl((asoi.item_price +
			asoi.tax +
			asoi.shipping +
			asoi.shipping_tax +
			asoi.commission),0) remittance
	--
	FROM		clean.orders co
	JOIN		clean.order_details cod
				on (cod.order_guid = co.order_guid)
	JOIN		clean.accounting_transactions cat
				on (cat.order_detail_id = cod.order_detail_id)
	LEFT OUTER JOIN	amazon.az_order_detail azod
				on (azod.master_order_number = co.master_order_number)
	LEFT OUTER JOIN	amazon.settlement_order_item asoi
				on (asoi.amazon_order_item_code = azod.az_order_item_number)
	LEFT OUTER JOIN	amazon.settlement_order asor
				on (asor.settlement_detail_id = asoi.settlement_detail_id)
	LEFT OUTER JOIN	amazon.az_order_related azor
				on (azor.az_order_number = azod.az_order_number)
	--
        WHERE	co.origin_id = 'AMZNI'
	AND	cod.eod_delivery_indicator = 'Y'
	and azor.type_feed='OrderAcknowledgement'
	AND	nvl(cod.eod_delivery_date, sysdate)
			<=v_date
	--		<= nvl(p_thru_date,last_day(add_months(sysdate,-1)))
        --
        UNION ALL
        --
	SELECT
		30,
		0,
		0,
		'',
		to_char(sysdate),
		to_char(sysdate),
		' ',
		'3030303030',
		0,
		0,
		0
		FROM dual
        --
	UNION ALL
	--
	SELECT
		60,
		0,
		0,
		'',
		to_char(sysdate),
		to_char(sysdate),
		' ',
		'6060606060',
		0,
		0,
		0
		FROM dual
	--
	UNION ALL
	--
	SELECT
		90,
		0,
		0,
		'',
		to_char(sysdate),
		to_char(sysdate),
		' ',
		'9090909090',
		0,
		0,
		0
		FROM dual
	--
	UNION ALL
	--
	SELECT
		91,
		0,
		0,
		'',
		to_char(sysdate),
		to_char(sysdate),
		' ',
		'9191919191',
		0,
		0,
		0
		FROM dual
;
END
;
.
/
