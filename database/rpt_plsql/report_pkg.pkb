CREATE OR REPLACE PACKAGE BODY RPT.REPORT_PKG
AS

PROCEDURE INSERT_REPORT
(
IN_NAME                         IN REPORT.NAME%TYPE,
IN_DESCRIPTION                  IN REPORT.DESCRIPTION%TYPE,
IN_REPORT_FILE_PREFIX           IN REPORT.REPORT_FILE_PREFIX%TYPE,
IN_REPORT_TYPE                  IN REPORT.REPORT_TYPE%TYPE,
IN_REPORT_OUTPUT_TYPE           IN REPORT.REPORT_OUTPUT_TYPE%TYPE,
IN_REPORT_CATEGORY              IN REPORT.REPORT_CATEGORY%TYPE,
IN_SERVER_NAME                  IN REPORT.SERVER_NAME%TYPE,
IN_HOLIDAY_INDICATOR            IN REPORT.HOLIDAY_INDICATOR%TYPE,
IN_STATUS                       IN REPORT.STATUS%TYPE,
IN_CREATED_BY                   IN REPORT.CREATED_BY%TYPE,
IN_ORACLE_RDF                   IN REPORT.ORACLE_RDF%TYPE,
IN_REPORT_DAYS_SPAN             IN REPORT.REPORT_DAYS_SPAN%TYPE,
IN_NOTES                        IN REPORT.NOTES%TYPE,
IN_ACL_NAME                     IN REPORT.ACL_NAME%TYPE,
IN_RETENTION_DAYS               IN REPORT.RETENTION_DAYS%TYPE,
IN_RUN_TIME_OFFSET              IN REPORT.RUN_TIME_OFFSET%TYPE,
IN_END_OF_DAY_REQUIRED          IN REPORT.END_OF_DAY_REQUIRED%TYPE,
IN_SCHEDULE_DAY                 IN REPORT.SCHEDULE_DAY%TYPE,
IN_SCHEDULE_TYPE                IN REPORT.SCHEDULE_TYPE%TYPE,
IO_REPORT_ID                    IN OUT REPORT.REPORT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the report header information.

Input:
        name                            varchar2
        description                     varchar2
        report_file_prefix              varchar2
        report_type                     varchar2
        report_output_type              varchar2
        report_category                 varchar2
        server_name                     varchar2
        holiday_indicator               char
        status                          varchar2
        created_by                      varchar2
        oracle_rdf                      varchar2
        report_days_span                number
        notes                           varchar2
        acl_name                        varchar2
        retention_days                  number
        run_time_offset                 number
        end_of_day_required             varchar2
        schedule_day                    varchar2
        schedule_type                   varchar2

Output:
        report_id                       number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF io_report_id IS NULL THEN
        select report_id_sq.nextval into io_report_id from dual;
END IF;

INSERT INTO report
(
        report_id,
        name,
        description,
        report_file_prefix,
        report_type,
        report_output_type,
        report_category,
        server_name,
        holiday_indicator,
        status,
        created_on,
        created_by,
        updated_on,
        updated_by,
        oracle_rdf,
        report_days_span,
        notes,
        acl_name,
        retention_days,
        run_time_offset,
        end_of_day_required,
        schedule_day,
        schedule_type
)
VALUES
(
        io_report_id,
        in_name,
        in_description,
        in_report_file_prefix,
        in_report_type,
        in_report_output_type,
        in_report_category,
        in_server_name,
        in_holiday_indicator,
        in_status,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_oracle_rdf,
        in_report_days_span,
        in_notes,
        in_acl_name,
        in_retention_days,
        in_run_time_offset,
        in_end_of_day_required,
        in_schedule_day,
        in_schedule_type
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REPORT;


PROCEDURE UPDATE_REPORT
(
IN_REPORT_ID                    IN REPORT.REPORT_ID%TYPE,
IN_NAME                         IN REPORT.NAME%TYPE,
IN_DESCRIPTION                  IN REPORT.DESCRIPTION%TYPE,
IN_REPORT_FILE_PREFIX           IN REPORT.REPORT_FILE_PREFIX%TYPE,
IN_REPORT_TYPE                  IN REPORT.REPORT_TYPE%TYPE,
IN_REPORT_OUTPUT_TYPE           IN REPORT.REPORT_OUTPUT_TYPE%TYPE,
IN_REPORT_CATEGORY              IN REPORT.REPORT_CATEGORY%TYPE,
IN_SERVER_NAME                  IN REPORT.SERVER_NAME%TYPE,
IN_HOLIDAY_INDICATOR            IN REPORT.HOLIDAY_INDICATOR%TYPE,
IN_STATUS                       IN REPORT.STATUS%TYPE,
IN_UPDATED_BY                   IN REPORT.UPDATED_BY%TYPE,
IN_ORACLE_RDF                   IN REPORT.ORACLE_RDF%TYPE,
IN_REPORT_DAYS_SPAN             IN REPORT.REPORT_DAYS_SPAN%TYPE,
IN_NOTES                        IN REPORT.NOTES%TYPE,
IN_ACL_NAME                     IN REPORT.ACL_NAME%TYPE,
IN_RETENTION_DAYS               IN REPORT.RETENTION_DAYS%TYPE,
IN_RUN_TIME_OFFSET              IN REPORT.RUN_TIME_OFFSET%TYPE,
IN_END_OF_DAY_REQUIRED          IN REPORT.END_OF_DAY_REQUIRED%TYPE,
IN_SCHEDULE_DAY                 IN REPORT.SCHEDULE_DAY%TYPE,
IN_SCHEDULE_TYPE                IN REPORT.SCHEDULE_TYPE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating report header
        information.

Input:
        report_id                       number
        name                            varchar2
        description                     varchar2
        report_file_prefix              varchar2
        report_type                     varchar2
        report_output_type              varchar2
        report_category                 varchar2
        server_name                     varchar2
        holiday_indicator               char
        status                          varchar2
        updated_by                      varchar2
        oracle_rdf                      varchar2
        report_days_span                number
        notes                           varchar2
        acl_name                        varchar2
        retention_days                  number
        run_time_offset                 number
        end_of_day_required             varchar2
        schedule_day                    varchar2
        schedule_type                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  report
SET
        name = NVL(in_name, name),
        description = NVL(in_description, description),
        report_file_prefix = NVL(in_report_file_prefix, report_file_prefix),
        report_type = NVL(in_report_type, report_type),
        report_output_type = NVL(in_report_output_type, report_output_type),
        report_category = NVL(in_report_category, report_category),
        server_name = NVL(in_server_name, server_name),
        holiday_indicator = NVL(in_holiday_indicator, holiday_indicator),
        status = NVL(in_status, status),
        updated_on = sysdate,
        updated_by = in_updated_by,
        oracle_rdf = NVL(in_oracle_rdf, oracle_rdf),
        report_days_span = NVL(in_report_days_span, report_days_span),
        notes = NVL(in_notes, notes),
        acl_name = NVL(in_acl_name, acl_name),
        retention_days = NVL(in_retention_days, retention_days),
        run_time_offset = NVL (in_run_time_offset, run_time_offset),
        end_of_day_required = NVL (in_end_of_day_required, end_of_day_required),
        schedule_day = NVL (in_schedule_day, schedule_day),
        schedule_type = NVL (in_schedule_type, schedule_type)
WHERE   report_id = in_report_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REPORT;


PROCEDURE INSERT_REPORT_PARM
(
IN_DISPLAY_NAME                 IN REPORT_PARM.DISPLAY_NAME%TYPE,
IN_REPORT_PARM_TYPE             IN REPORT_PARM.REPORT_PARM_TYPE%TYPE,
IN_ORACLE_PARM_NAME             IN REPORT_PARM.ORACLE_PARM_NAME%TYPE,
IN_CREATED_BY                   IN REPORT_PARM.CREATED_BY%TYPE,
IN_VALIDATION                   IN REPORT_PARM.VALIDATION%TYPE,
OUT_REPORT_PARM_ID              OUT REPORT_PARM.REPORT_PARM_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting report parameter
        information.

Input:
        display_name                    varchar2
        report_parm_type                varchar2
        oracle_parm_name                varchar2
        created_by                      varchar2
        validation                      varchar2

Output:
        report_parm_id                  number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO report_parm
(
        report_parm_id,
        display_name,
        report_parm_type,
        oracle_parm_name,
        created_on,
        created_by,
        updated_on,
        updated_by,
        validation
)
VALUES
(
        report_parm_id_sq.nextval,
        in_display_name,
        in_report_parm_type,
        in_oracle_parm_name,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_validation
) RETURNING report_parm_id INTO out_report_parm_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REPORT_PARM;


PROCEDURE UPDATE_REPORT_PARM
(
IN_REPORT_PARM_ID               IN REPORT_PARM.REPORT_PARM_ID%TYPE,
IN_DISPLAY_NAME                 IN REPORT_PARM.DISPLAY_NAME%TYPE,
IN_REPORT_PARM_TYPE             IN REPORT_PARM.REPORT_PARM_TYPE%TYPE,
IN_ORACLE_PARM_NAME             IN REPORT_PARM.ORACLE_PARM_NAME%TYPE,
IN_UPDATED_BY                   IN REPORT_PARM.UPDATED_BY%TYPE,
IN_VALIDATION                   IN REPORT_PARM.VALIDATION%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the report parameter
        information.

Input:
        report_parm_id                  number
        display_name                    varchar2
        report_parm_type                varchar2
        oracle_parm_name                varchar2
        updated_by                      varchar2
        validation                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  report_parm
SET
        display_name = NVL(in_display_name, display_name),
        report_parm_type = NVL(in_report_parm_type, report_parm_type),
        oracle_parm_name = NVL(in_oracle_parm_name, oracle_parm_name),
        updated_on = sysdate,
        updated_by = in_updated_by,
        validation = NVL(in_validation, validation)
WHERE   report_parm_id = in_report_parm_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REPORT_PARM;


PROCEDURE INSERT_REPORT_PARM_VALUE
(
IN_CREATED_BY                   IN REPORT_PARM_VALUE.CREATED_BY%TYPE,
IN_DISPLAY_TEXT                 IN REPORT_PARM_VALUE.DISPLAY_TEXT%TYPE,
IN_DB_STATEMENT_ID              IN REPORT_PARM_VALUE.DB_STATEMENT_ID%TYPE,
IN_DEFAULT_FORM_VALUE           IN REPORT_PARM_VALUE.DEFAULT_FORM_VALUE%TYPE,
OUT_REPORT_PARM_VALUE_ID        OUT REPORT_PARM_VALUE.REPORT_PARM_VALUE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting parameter values.

Input:
        created_by                      varchar2
        display_text                    varchar2
        db_statement_id                 varchar2
        default_form_value              varchar2

Output:
        report_parm_value_id            number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO report_parm_value
(
        report_parm_value_id,
        created_on,
        created_by,
        updated_on,
        updated_by,
        display_text,
        db_statement_id,
        default_form_value
)
VALUES
(
        report_parm_value_id_sq.nextval,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_display_text,
        in_db_statement_id,
        in_default_form_value
) RETURNING report_parm_value_id INTO out_report_parm_value_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REPORT_PARM_VALUE;


PROCEDURE UPDATE_REPORT_PARM_VALUE
(
IN_REPORT_PARM_VALUE_ID         IN REPORT_PARM_VALUE.REPORT_PARM_VALUE_ID%TYPE,
IN_UPDATED_BY                   IN REPORT_PARM_VALUE.UPDATED_BY%TYPE,
IN_DISPLAY_TEXT                 IN REPORT_PARM_VALUE.DISPLAY_TEXT%TYPE,
IN_DB_STATEMENT_ID              IN REPORT_PARM_VALUE.DB_STATEMENT_ID%TYPE,
IN_DEFAULT_FORM_VALUE           IN REPORT_PARM_VALUE.DEFAULT_FORM_VALUE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating parameter value
        information.

Input:
        report_parm_value_id            number
        updated_by                      varchar2
        display_text                    varchar2
        db_statement_id                 varchar2
        default_form_value                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  report_parm_value
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        display_text = NVL(in_display_text, display_text),
        db_statement_id = NVL(in_db_statement_id, db_statement_id),
        default_form_value = NVL(in_default_form_value, default_form_value)
WHERE   report_parm_value_id = in_report_parm_value_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REPORT_PARM_VALUE;


PROCEDURE INSERT_REPORT_SUBMISSION_LOG
(
IN_REPORT_ID                    IN REPORT_SUBMISSION_LOG.REPORT_ID%TYPE,
IN_REPORT_FILE_NAME             IN REPORT_SUBMISSION_LOG.REPORT_FILE_NAME%TYPE,
IN_SUBMISSION_TYPE              IN REPORT_SUBMISSION_LOG.SUBMISSION_TYPE%TYPE,
IN_SUBMITTED_BY                 IN REPORT_SUBMISSION_LOG.SUBMITTED_BY%TYPE,
IN_REPORT_STATUS                IN REPORT_SUBMISSION_LOG.REPORT_STATUS%TYPE,
IN_ORACLE_REPORT_URL            IN REPORT_SUBMISSION_LOG.ORACLE_REPORT_URL%TYPE,
OUT_REPORT_SUBMISSION_ID        OUT REPORT_SUBMISSION_LOG.REPORT_SUBMISSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a new submission log
        record when a report gets created.

Input:
        report_id                       number
        report_file_name                varchar2
        submission_type                 char
        submitted_by                    varchar2
        report_status                   varchar2
        oracle_report_url               varchar2

Output:
        report_submission_id            number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

CURSOR report_cur IS
        SELECT  report_output_type
        FROM    report
        WHERE   report_id = in_report_id;

v_report_output_type    report.report_output_type%type;

BEGIN

OPEN report_cur;
FETCH report_cur INTO v_report_output_type;
CLOSE report_cur;

INSERT INTO report_submission_log
(
        report_submission_id,
        report_id,
        report_file_name,
        submission_type,
        submitted_on,
        submitted_by,
        report_status,
        oracle_report_url,
        submitted_output_type
)
VALUES
(
        report_submission_id_sq.nextval,
        in_report_id,
        in_report_file_name,
        in_submission_type,
        sysdate,
        in_submitted_by,
        in_report_status,
        in_oracle_report_url,
        v_report_output_type
) RETURNING report_submission_id INTO out_report_submission_id;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END INSERT_REPORT_SUBMISSION_LOG;


PROCEDURE UPDATE_REPORT_SUBMISSION_LOG
(
IN_REPORT_SUBMISSION_ID         IN REPORT_SUBMISSION_LOG.REPORT_SUBMISSION_ID%TYPE,
IN_COMPLETED_BY                 IN REPORT_SUBMISSION_LOG.COMPLETED_BY%TYPE,
IN_REPORT_STATUS                IN REPORT_SUBMISSION_LOG.REPORT_STATUS%TYPE,
IN_ERROR_MESSAGE                IN REPORT_SUBMISSION_LOG.ERROR_MESSAGE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the status and error
        message for the given submitted report execution.

Input:
        report_submission_id            number
        completed_by                    varchar2
        report_status                   varchar2
        error_message                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  report_submission_log
SET
        completed_on = sysdate,
        completed_by = in_completed_by,
        report_status = in_report_status,
        error_message = in_error_message
WHERE   report_submission_id = in_report_submission_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REPORT_SUBMISSION_LOG;


PROCEDURE UPDATE_REPORT_SUB_ACCESSED
(
IN_REPORT_SUBMISSION_ID         IN REPORT_SUBMISSION_LOG.REPORT_SUBMISSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the last accessed date of
        the given submitted report execution.

Input:
        report_submission_id            number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE  report_submission_log
SET
        last_accessed_date = sysdate
WHERE   report_submission_id = in_report_submission_id;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END UPDATE_REPORT_SUB_ACCESSED;


PROCEDURE INSERT_REPORT_DETAIL_CHAR
(
IN_REPORT_SUBMISSION_ID         IN REPORT_DETAIL_CHAR.REPORT_SUBMISSION_ID%TYPE,
IN_REPORT_DETAIL                IN REPORT_DETAIL_CHAR.REPORT_DETAIL%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the report detail clob

Input:
        report_submission_id            number
        report_detail                   clob
        expiration_date                 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR report_cur IS
        SELECT  sysdate + nvl (r.retention_days, 0) expiration_date
        FROM    report r
        WHERE   exists
        (       select  1
                from    report_submission_log l
                where   l.report_submission_id = in_report_submission_id
                and     l.report_id = r.report_id
        );

v_expiration_date       date;

BEGIN

OPEN report_cur;
FETCH report_cur INTO v_expiration_date;
CLOSE report_cur;

INSERT INTO report_detail_char
(
        report_submission_id,
        report_detail,
        expiration_date
)
VALUES
(
        in_report_submission_id,
        in_report_detail,
        v_expiration_date
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REPORT_DETAIL_CHAR;


PROCEDURE UPDATE_REPORT_DETAIL_CHAR
(
IN_REPORT_SUBMISSION_ID         IN REPORT_DETAIL_CHAR.REPORT_SUBMISSION_ID%TYPE,
IN_REPORT_DETAIL                IN VARCHAR2,        ----   REPORT_DETAIL_CHAR.REPORT_DETAIL%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for adding to the report detail

Input:
        report_submission_id            number
        report_detail                   clob

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  report_detail_char
SET
        report_detail = report_detail || in_report_detail
WHERE   report_submission_id = in_report_submission_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REPORT_DETAIL_CHAR;


PROCEDURE INSERT_REPORT_DETAIL_BIN
(
IN_REPORT_SUBMISSION_ID         IN REPORT_DETAIL_CHAR.REPORT_SUBMISSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the report detail blob

Input:
        report_submission_id            number
        expiration_date                 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR report_cur IS
        SELECT  sysdate + nvl (r.retention_days, 0) expiration_date
        FROM    report r
        WHERE   exists
        (       select  1
                from    report_submission_log l
                where   l.report_submission_id = in_report_submission_id
                and     l.report_id = r.report_id
        );

v_expiration_date       date;

BEGIN

OPEN report_cur;
FETCH report_cur INTO v_expiration_date;
CLOSE report_cur;

INSERT INTO report_detail_bin
(
        report_submission_id,
        report_detail,
        expiration_date
)
VALUES
(
        in_report_submission_id,
        EMPTY_BLOB(),
        v_expiration_date
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REPORT_DETAIL_BIN;



PROCEDURE GET_REPORT_LIST
(
IN_CSR_ID                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_CATEGORY_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning user executable report
        list

Input:
        csr_id                          varchar2

Output:
        cursor containing all columns from report
        cursor containing report category information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CATEGORY_CUR FOR
        SELECT  c.report_category,
                c.description report_category_desc
        FROM    report_category_val c
        WHERE   status = 'Active'
        ORDER BY c.sort_order;

OPEN OUT_CUR FOR
        SELECT
                r.report_id,
                r.name,
                r.description,
                r.report_category,
                c.description category_description
        FROM    report r
        JOIN    report_category_val c
        ON      r.report_category = c.report_category
        AND     c.status = 'Active'
        WHERE   exists
                (       select  1
                        from    aas.rel_identity_role ir
                        join    aas.rel_role_acl ra
                        on      ir.role_id = ra.role_id
                        where   ra.acl_name = r.acl_name
                        and     ir.identity_id = in_csr_id
                )
        AND     r.status = 'Active'
        AND     r.report_type in ('U', 'B')
        ORDER by c.sort_order, r.name;

END GET_REPORT_LIST;

PROCEDURE GET_REPORT_PARM
(
IN_REPORT_ID                    IN REPORT_PARM_REF.REPORT_ID%TYPE,
IN_PARM_VALUE_INDICATOR         IN VARCHAR2,
OUT_RERORT_CUR                  OUT TYPES.REF_CURSOR,
OUT_PARM_CUR                    OUT TYPES.REF_CURSOR,
OUT_VALUE_CUR                   OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the report parameters
        information.

Input:
        report_id                       number
        parm_value_indicator            varchar2 (Y/N if parm values should be returned)

Output:
        cursor containing the report information

        cursor containing all columns from report_parm and report_parm_ref
        for the given report id

        cursor containing the parameter values

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_RERORT_CUR FOR
        SELECT
                r.report_id,
                r.name,
                r.description,
                r.report_file_prefix,
                r.report_type,
                r.report_output_type,
                r.report_category,
                r.acl_name,
                r.server_name,
                r.holiday_indicator,
                r.oracle_rdf,
                r.report_days_span,
                r.notes,
                c.description report_category_desc,
                r.end_of_day_required
        FROM    report r
        JOIN    report_category_val c
        ON      r.report_category = c.report_category
        WHERE   r.report_id = in_report_id;


OPEN OUT_PARM_CUR FOR
        SELECT
                rp.report_parm_id,
                rp.display_name,
                rp.report_parm_type,
                rp.oracle_parm_name,
                rp.validation,
                rpr.report_id,
                rpr.sort_order,
                rpr.required_indicator,
                rpr.allow_future_date_ind allow_future_date
        FROM    report_parm rp
        JOIN    report_parm_ref rpr
        ON      rp.report_parm_id = rpr.report_parm_id
        WHERE   rpr.report_id = in_report_id
        ORDER BY rpr.sort_order;

IF in_parm_value_indicator = 'Y' THEN
        OPEN OUT_VALUE_CUR FOR
                SELECT
                        rpv.report_parm_value_id,
                        rpv.display_text,
                        rpv.db_statement_id,
                        rpv.default_form_value,
                        rpvr.report_parm_id,
                        rpvr.sort_order
                FROM    report_parm_value rpv
                JOIN    report_parm_value_ref rpvr
                ON      rpv.report_parm_value_id = rpvr.report_parm_value_id
                JOIN    report_parm_ref rpr
                ON      rpvr.report_parm_id = rpr.report_parm_id
                WHERE   rpr.report_id = in_report_id
                ORDER BY rpr.sort_order, rpvr.sort_order;
ELSE
        OPEN OUT_VALUE_CUR FOR
                SELECT  null
                FROM    dual;
END IF;

END GET_REPORT_PARM;


PROCEDURE GET_SUBMITTED_REPORTS
(
IN_CSR_ID                       IN VARCHAR2,
IN_START_DATE                   IN DATE,
IN_END_DATE                     IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning user accessible reports
        that were submitted in the given time frame.

Input:
        csr_id                          varchar2
        start_date                      date
        end_date                        date

Output:
        cursor containing all columns from report_submission_log including
        the output type from report

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                l.report_submission_id,
                l.report_id,
                l.report_file_name,
                l.submission_type,
                to_char (l.submitted_on, 'mm/dd/yyyy hh:miAM') submitted_on,
                l.submitted_by,
                to_char (l.completed_on, 'mm/dd/yyyy hh:miAM') completed_on,
                l.completed_by,
                l.report_status,
                l.error_message,
                l.submitted_output_type,
                l.oracle_report_url,
                to_char (l.last_accessed_date, 'mm/dd/yyyy hh:miAM') last_accessed_date,
                s.description status_description,
                r.name report_name,
                c.description category_description,
                l.submitted_on submitted_on_sort
        FROM    report_submission_log l
        JOIN    report r
        ON      l.report_id = r.report_id
        JOIN    report_category_val c
        ON      r.report_category = c.report_category
        JOIN    report_status_val s
        ON      l.report_status = s.report_status
        AND     s.status = 'Active'
        WHERE   l.submitted_by = in_csr_id
        AND     l.submitted_on between in_start_date and in_end_date + 1
        UNION
        SELECT
                l.report_submission_id,
                l.report_id,
                l.report_file_name,
                l.submission_type,
                to_char (l.submitted_on, 'mm/dd/yyyy hh:miAM') submitted_on,
                l.submitted_by,
                to_char (l.completed_on, 'mm/dd/yyyy hh:miAM') completed_on,
                l.completed_by,
                l.report_status,
                l.error_message,
                l.submitted_output_type,
                l.oracle_report_url,
                to_char (l.last_accessed_date, 'mm/dd/yyyy hh:miAM') last_accessed_date,
                s.description status_description,
                r.name report_name,
                c.description category_description,
                l.submitted_on submitted_on_sort
        FROM    report_submission_log l
        JOIN    report r
        ON      l.report_id = r.report_id
        JOIN    report_category_val c
        ON      r.report_category = c.report_category
        JOIN    report_status_val s
        ON      l.report_status = s.report_status
        AND     s.status = 'Active'
        WHERE   l.submitted_by = 'SYS'
        AND     l.submitted_on between in_start_date and in_end_date + 1
        AND     exists
                (       select  1
                        from    aas.rel_identity_role ir
                        join    aas.rel_role_acl ra
                        on      ir.role_id = ra.role_id
                        where   ra.acl_name = r.acl_name
                        and     ir.identity_id = in_csr_id
                )
        ORDER BY submitted_on_sort desc;

END GET_SUBMITTED_REPORTS;


PROCEDURE VIEW_REPORT_SUBMISSION_LOG
(
IN_REPORT_SUBMISSION_ID         IN REPORT_SUBMISSION_LOG.REPORT_SUBMISSION_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the submitted report info

Input:
        report_submission_id            number

Output:
        cursor containing all records from report_submission_log

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                report_submission_id,
                report_id,
                report_file_name,
                submission_type,
                submitted_on,
                submitted_by,
                completed_on,
                completed_by,
                report_status,
                error_message,
                oracle_report_url
        FROM    report_submission_log
        WHERE   report_submission_id = in_report_submission_id;

END VIEW_REPORT_SUBMISSION_LOG;


PROCEDURE GET_GENERATED_REPORT
(
IN_REPORT_SUBMISSION_ID         IN REPORT_DETAIL_BIN.REPORT_SUBMISSION_ID%TYPE,
IN_REPORT_OUTPUT_TYPE           IN REPORT_OUTPUT_TYPE.REPORT_OUTPUT_TYPE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the report details.

Input:
        report_submission_id            number
        report_detail                   blob
        expiration_date                 date

Output:
        cursor containing all columns from report_detail_bin or report_detail_char

-----------------------------------------------------------------------------*/

v_status        char(1);
v_message       varchar2(1000);

BEGIN

UPDATE_REPORT_SUB_ACCESSED (in_report_submission_id, v_status, v_message);

IF in_report_output_type = 'Bin' THEN
        OPEN OUT_CUR FOR
                SELECT
                        report_submission_id,
                        report_detail,
                        expiration_date
                FROM    report_detail_bin
                WHERE   report_submission_id = in_report_submission_id;
ELSE
        OPEN OUT_CUR FOR
                SELECT
                        report_submission_id,
                        report_detail,
                        expiration_date
                FROM    report_detail_char
                WHERE   report_submission_id = in_report_submission_id;
END IF;

END GET_GENERATED_REPORT;


PROCEDURE LOOKUP_SOURCE_CODE
(
IN_SOURCE_CODE                  IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ROW_COUNT                   OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of source codes.

Input:
        source_code                     varchar2

Output:
        cursor containing source codes

-----------------------------------------------------------------------------*/

CURSOR count_cur IS
        SELECT  count (1)
        FROM    ftd_apps.source_master
        WHERE   source_code like '%' || in_source_code || '%'
        AND     start_date <= sysdate
        AND     (end_date >= sysdate OR
                 end_date IS NULL);

BEGIN

OPEN count_cur;
FETCH count_cur INTO out_row_count;
CLOSE count_cur;

OPEN OUT_CUR FOR
        SELECT  a.source_code,
                a.description
        FROM
        (
                SELECT  b.source_code,
                        b.description,
                        rownum row_number
                FROM
                (
                        SELECT  source_code,
                                description
                        FROM    ftd_apps.source_master
                        WHERE   source_code like '%' || in_source_code || '%'
                        AND     start_date <= sysdate
                        AND     (end_date >= sysdate OR
                                 end_date IS NULL)
                        ORDER BY source_code
                ) b
                WHERE   rownum <= (in_start_position + in_max_number_returned - 1)
        ) a
        WHERE   a.row_number >= in_start_position;

END LOOKUP_SOURCE_CODE;


FUNCTION VALIDATE_SOURCE_CODE
(
IN_SOURCE_CODE                  IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for validating the existance of the
        given source code

Input:
        source_code                     varchar2

Output:
        Y/N if the source code exists

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        SELECT  'Y'
        FROM    ftd_apps.source_master
        WHERE   source_code = in_source_code
        AND     start_date <= sysdate
        AND     (end_date >= sysdate OR
                 end_date IS NULL);

v_exists        char(1) := 'N';

BEGIN
OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END VALIDATE_SOURCE_CODE;


PROCEDURE LOOKUP_SOURCE_TYPE
(
IN_SOURCE_TYPE                  IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ROW_COUNT                   OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of source types

Input:
        source_type                     varchar2

Output:
        cursor containing source types

-----------------------------------------------------------------------------*/

CURSOR count_cur IS
        SELECT  count (1)
        FROM    ftd_apps.source_type_val
        WHERE   UPPER (source_type) like '%' || UPPER (in_source_type) || '%'
        AND     status = 'Active';
BEGIN

OPEN count_cur;
FETCH count_cur INTO out_row_count;
CLOSE count_cur;

OPEN OUT_CUR FOR
        SELECT  a.source_type
        FROM
        (
                SELECT  b.source_type,
                        rownum row_number
                FROM
                (
                        SELECT  source_type
                        FROM    ftd_apps.source_type_val
                        WHERE   source_type like '%' || in_source_type || '%'
                        AND     status = 'Active'
                        ORDER BY source_type
                ) b
                WHERE   rownum <= (in_start_position + in_max_number_returned - 1)
        ) a
        WHERE   a.row_number >= in_start_position;

END LOOKUP_SOURCE_TYPE;


FUNCTION VALIDATE_SOURCE_TYPE
(
IN_SOURCE_TYPE                  IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for validating the existance of the
        given source type

Input:
        source_type                     varchar2

Output:
        Y/N if the source type exists

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        SELECT  'Y'
        FROM    ftd_apps.source_type_val
        WHERE   UPPER (source_type) = UPPER (in_source_type)
        AND     status = 'Active';

v_exists        char(1) := 'N';

BEGIN
OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END VALIDATE_SOURCE_TYPE;


PROCEDURE LOOKUP_VENDOR_ID
(
IN_VENDOR_ID                    IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ROW_COUNT                   OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of vendor ids

Input:
        vendor_id                       varchar2

Output:
        cursor containing vendor ids

-----------------------------------------------------------------------------*/

CURSOR count_cur IS
        SELECT  count (1)
        FROM    ftd_apps.vendor_master
        WHERE   vendor_id like '%' || in_vendor_id || '%';
BEGIN

OPEN count_cur;
FETCH count_cur INTO out_row_count;
CLOSE count_cur;

OPEN OUT_CUR FOR
        SELECT  a.vendor_id,
                a.vendor_name
        FROM
        (
                SELECT  b.vendor_id,
                        b.vendor_name,
                        rownum row_number
                FROM
                (
                        SELECT  vendor_id,
                                vendor_name
                        FROM    ftd_apps.vendor_master
                        WHERE   vendor_id like '%' || in_vendor_id || '%'
                        AND	active = 'Y'
                        ORDER BY vendor_name
                ) b
                WHERE   rownum <= (in_start_position + in_max_number_returned - 1)
        ) a
        WHERE   a.row_number >= in_start_position;

END LOOKUP_VENDOR_ID;


FUNCTION VALIDATE_VENDOR_ID
(
IN_VENDOR_ID                    IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for validating the existance of the
        given vendor id

Input:
        vendor_id                       varchar2

Output:
        Y/N if the vendor_id exists

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        SELECT  'Y'
        FROM    ftd_apps.vendor_master
        WHERE   UPPER (vendor_id) = UPPER (in_vendor_id);

v_exists        char(1) := 'N';

BEGIN
OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END VALIDATE_VENDOR_ID;


FUNCTION VALIDATE_VENDOR_PRODUCT_ID
(
IN_PRODUCT_ID                   IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for validating the existance of the
        given vendor product id

Input:
        product_id                      varchar2

Output:
        Y/N if the vendor product_id exists, NV if the product exists but is
        not vendor delivered.

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        SELECT  decode (p.ship_method_carrier, 'Y', 'Y', 'NV')
        FROM    ftd_apps.product_master p
        WHERE   UPPER (p.product_id) = UPPER (in_product_id)
        OR      exists (
                        select  1
                        from    ftd_apps.product_subcodes ps
                        where   ps.product_id = p.product_id
                        and     ps.product_subcode_id = UPPER (in_product_id)
                        );



v_exists        varchar2(2) := 'N';

BEGIN
OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END VALIDATE_VENDOR_PRODUCT_ID;


PROCEDURE LOOKUP_DNIS_ID
(
IN_DNIS_ID                      IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ROW_COUNT                   OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of dnis ids

Input:
        dnis_id                         varchar2

Output:
        cursor containing dnis ids

-----------------------------------------------------------------------------*/

CURSOR count_cur IS
        SELECT  count (1)
        FROM    ftd_apps.dnis
        WHERE   dnis_id like '%' || in_dnis_id || '%';
BEGIN

OPEN count_cur;
FETCH count_cur INTO out_row_count;
CLOSE count_cur;

OPEN OUT_CUR FOR
        SELECT  a.dnis_id,
                a.description
        FROM
        (
                SELECT  b.dnis_id,
                        b.description,
                        rownum row_number
                FROM
                (
                        SELECT  dnis_id,
                                description
                        FROM    ftd_apps.dnis
                        WHERE   dnis_id like '%' || in_dnis_id || '%'
                        ORDER BY dnis_id
                ) b
                WHERE   rownum <= (in_start_position + in_max_number_returned - 1)
        ) a
        WHERE   a.row_number >= in_start_position;

END LOOKUP_DNIS_ID;


FUNCTION VALIDATE_DNIS_ID
(
IN_DNIS_ID                      IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for validating the existance of the
        given dnis id

Input:
        dnis_id                         varchar2

Output:
        Y/N if the dnis_id exists

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        SELECT  'Y'
        FROM    ftd_apps.dnis
        WHERE   dnis_id = in_dnis_id;

v_exists        char(1) := 'N';

BEGIN
OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END VALIDATE_DNIS_ID;


PROCEDURE LOOKUP_USER_ID
(
IN_IDENTITY_ID                  IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ROW_COUNT                   OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of identity ids

Input:
        identity_id                     varchar2

Output:
        cursor containing identity ids

-----------------------------------------------------------------------------*/

CURSOR count_cur IS
        SELECT  count (1)
        FROM    aas.identity
        WHERE   identity_id like '%' || in_identity_id || '%';
BEGIN

OPEN count_cur;
FETCH count_cur INTO out_row_count;
CLOSE count_cur;

OPEN OUT_CUR FOR
        SELECT  a.identity_id,
                a.user_name
        FROM
        (
                SELECT  b.identity_id,
                        b.user_name,
                        rownum row_number
                FROM
                (
                        SELECT  i.identity_id,
                                u.first_name || ' ' || u.last_name user_name
                        FROM    aas.identity i
                        JOIN    aas.users u
                        ON      i.user_id = u.user_id
                        WHERE   i.identity_id like '%' || in_identity_id || '%'
                        ORDER BY i.identity_id
                ) b
                WHERE   rownum <= (in_start_position + in_max_number_returned - 1)
        ) a
        WHERE   a.row_number >= in_start_position;

END LOOKUP_USER_ID;


FUNCTION VALIDATE_USER_ID
(
IN_IDENTITY_ID                  IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for validating the existance of the
        given identity id

Input:
        identity_id                     varchar2

Output:
        Y/N if the identity_id exists

-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        SELECT  'Y'
        FROM    aas.identity
        WHERE   UPPER (identity_id) = UPPER (in_identity_id);

v_exists        char(1) := 'N';

BEGIN
OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END VALIDATE_USER_ID;


PROCEDURE GET_CALL_CENTER_LIST
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of call centers.

Input:
        none

Output:
        list of call centers

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  call_center_id,
                description
        FROM    ftd_apps.call_center
        ORDER BY description;

END GET_CALL_CENTER_LIST;


PROCEDURE GET_CS_GROUPS_LIST
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of cs groups.

Input:
        none

Output:
        list of cs groups

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  cs_group_id,
                description
        FROM    ftd_apps.cs_groups
        ORDER BY description;

END GET_CS_GROUPS_LIST;


PROCEDURE GET_QUEUE_TYPE_LIST
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of queue types.

Input:
        none

Output:
        list of queue types

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  queue_type,
                description
        FROM    clean.queue_type_val
        ORDER BY queue_indicator, sort_order;

END GET_QUEUE_TYPE_LIST;


PROCEDURE GET_CALL_REASON_LIST
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of call reasons.

Input:
        none

Output:
        list of queue types

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  call_reason_code,
                description
        FROM    clean.call_reason_val
        ORDER BY description;

END GET_CALL_REASON_LIST;


PROCEDURE GET_ORIGIN_LIST
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of internet
        origins and phone call centers.

Input:
        none

Output:
        list of origins/call centers

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  origin_id,
                description
        FROM    ftd_apps.origins
        WHERE   (origin_type = 'internet'
        OR      origin_id = 'BULK')
        AND     active = 'Y'
        UNION
        SELECT  call_center_id origin_id,
                description
        FROM    ftd_apps.call_center
        ORDER BY description;

END GET_ORIGIN_LIST;



PROCEDURE GET_REPORT_LIST_TO_RUN
(
IN_RUN_DATE             IN DATE,
OUT_CUR                 OUT TYPES.REF_CURSOR,
OUT_END_OF_DAY_DONE     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of reports
        to run for daily or monthly schedule.

Input:
        run_date                date

Output:
        list of reports

-----------------------------------------------------------------------------*/

CURSOR eod_done (p_run_date date) IS
        SELECT  'Y'
        FROM    dual
        WHERE  ( FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_JDE_EOD_FLAG') = 'N'
                OR
                (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_JDE_EOD_FLAG') = 'Y'
        	 AND EXISTS          -- check the regular eod process
		(
			SELECT  1
			FROM    clean.billing_header
			WHERE   processed_indicator = 'Y'
			AND     TRUNC (created_on) = TRUNC (p_run_date+1)
		)))
        AND     (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_AAFES_EOD_FLAG') = 'N'
                OR
                (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_AAFES_EOD_FLAG') = 'Y'
        	 AND EXISTS          -- check AAFES eod process
		(
			SELECT  1
			FROM    clean.aafes_billing_header
			WHERE   processed_indicator = 'Y'
			AND     TRUNC (created_on) = TRUNC (p_run_date+1)
		)))
        AND (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_PAYPAL_EOD_FLAG') = 'N'
                    OR
                (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_PAYPAL_EOD_FLAG') = 'Y'
        	 AND EXISTS          -- check PayPal eod process
		(
			SELECT  1
			FROM    clean.alt_pay_billing_header
			WHERE   processed_flag = 'Y'
			AND	payment_method_id = 'PP'
			AND     TRUNC (created_on) = TRUNC (p_run_date+1)
		)))
        AND     (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_BILLMELATER_EOD_FLAG') = 'N'
                      OR
                (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_BILLMELATER_EOD_FLAG') = 'Y'
        	 AND EXISTS          -- check BillMeLater eod process
	        (
			SELECT  1
			FROM    clean.alt_pay_billing_header
			WHERE   processed_flag = 'Y'
			AND	payment_method_id = 'BM'
			AND     TRUNC (created_on) = TRUNC (p_run_date+1)
        	)))
        AND     (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_GIFT_CARD_EOD_FLAG') = 'N'
                      OR
                (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_GIFT_CARD_EOD_FLAG') = 'Y'
             AND EXISTS          -- check Gift Card EOD process
            (
            SELECT  1
            FROM    clean.alt_pay_billing_header
            WHERE   processed_flag = 'Y'
            AND payment_method_id = 'GD'
            AND     TRUNC (created_on) = TRUNC (p_run_date+1)
            )))
       	AND (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_UA_EOD_FLAG') = 'N'
                      OR
                (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('ACCOUNTING_CONFIG','DISPATCH_UA_EOD_FLAG') = 'Y'
        AND     EXISTS              -- check United Mileage Plus eod process
	        (
			SELECT  1
			FROM    clean.alt_pay_billing_header
			WHERE   processed_flag = 'Y'
			AND	payment_method_id = 'UA'
			AND     TRUNC (billing_batch_date) = TRUNC (p_run_date)
        	)));
        
BEGIN

OPEN eod_done (in_run_date);
FETCH eod_done INTO out_end_of_day_done;
CLOSE eod_done;

OPEN OUT_CUR FOR
        SELECT  report_id,
                server_name,
                oracle_rdf,
                report_type,
                report_output_type,
                run_time_offset,
                end_of_day_required,
                report_file_prefix,
                name,
                schedule_parm_value
        FROM    report
        WHERE   status = 'Active'
        AND     report_type IN ('S', 'B')
        AND     (schedule_type = 'D'
        OR       (schedule_type = 'W' AND schedule_day = to_char (in_run_date + 1, 'd'))
        OR       (schedule_type = 'M' AND schedule_day = to_char (in_run_date + 1, 'dd'))
        OR       (schedule_type = 'B' AND 
                     (to_char (in_run_date + 1, 'dd') = '01'
                     or
                     to_char (in_run_date + 1, 'dd') = schedule_day) ))
        ORDER BY run_time_offset, report_id;

END GET_REPORT_LIST_TO_RUN;

FUNCTION IS_VALID_FLORIST (
   PCHR_OPTION   VARCHAR2,
   PCHR_DATA   VARCHAR2)
RETURN VARCHAR2
AS

/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 05/15/2006
***              Reason : Enhancement825: Create new Member Order Distribution Report
***             Purpose : Validates the Florist Id entered on the Report Submission Form
***
***    Input Parameters : pchr_option - By Florist Number 'N'
***                                     By Florist Prefix Number 'P'
***                                     By Central Account Number 'A'
***                       pchr_data - Value for the option picked
***                       
***             Returns : TRUE OR FALSE
***
*** Special Instructions: 
***
*****************************************************************************************/

   
   CURSOR cur_florist_id
   IS
   SELECT 
      'Y' 
   FROM 
      ftd_apps.florist_master
	WHERE 
   	florist_id = SUBSTR(REPLACE(pchr_data, '-'), 1, 2) || '-' || SUBSTR(REPLACE(pchr_data, '-'), 3); 
      

   CURSOR cur_florist_prefix
   IS
   SELECT 
      'Y' 
   FROM 
      ftd_apps.florist_master
	WHERE 
   	SUBSTR(florist_id, 1, 7) = SUBSTR(REPLACE(pchr_data, '-'), 1, 2) || '-' || SUBSTR(REPLACE(pchr_data, '-'), 3); 
   
   CURSOR cur_link_no
   IS
   SELECT 
      'Y' 
   FROM 
      ftd_apps.florist_master
	WHERE 
   	TO_NUMBER(internal_link_number) = TO_NUMBER(pchr_data); 
	
	lchr_valid   VARCHAR2(10) := 'N';
   
BEGIN

   IF pchr_option = 'N' THEN
      
      OPEN cur_florist_id;
      FETCH cur_florist_id INTO lchr_valid;
      CLOSE cur_florist_id;
      
   ELSIF pchr_option = 'P' THEN
     
      OPEN cur_florist_prefix;
      FETCH cur_florist_prefix INTO lchr_valid;
      CLOSE cur_florist_prefix;
    
   ELSIF pchr_option = 'A' THEN
    
      OPEN cur_link_no;
      FETCH cur_link_no INTO lchr_valid;
      CLOSE cur_link_no;
      
   END IF;
   
   RETURN lchr_valid;
      

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In RPT.IS_VALID_FLORIST: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END IS_VALID_FLORIST;

PROCEDURE GET_STATE_LIST
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of states.

Input:
        none

Output:
        list of states

-----------------------------------------------------------------------------*/
BEGIN

	OPEN out_cur FOR
	     SELECT state_master_id, 
	     			state_name 
	     	 FROM ftd_apps.state_master 
	     	WHERE NVL(country_code, 'USA') <> 'INT'
	     	ORDER BY state_name;
        
END GET_STATE_LIST;

PROCEDURE GET_WINE_VENDORS
(
OUT_CUR						OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of wine vendors.

Input:
        none

Output:
        list of wine vendors

-----------------------------------------------------------------------------*/
BEGIN

	OPEN out_cur FOR
	SELECT value vendor_id,
			 INITCAP(name) vendor_name
	  FROM frp.global_parms
	 WHERE context = 'WINE VENDORS'
	 ORDER BY name;
        
END GET_WINE_VENDORS;

PROCEDURE GET_SOURCE_CODES
(
OUT_CUR						OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of ALL source codes.

Input:
        none

Output:
        list of ALL source codes and their names

-----------------------------------------------------------------------------*/
BEGIN

	OPEN out_cur FOR
	SELECT source_code source_code,
		    source_code || ' - ' || INITCAP(description) description
	  FROM ftd_apps.source_master
	 ORDER BY source_code;
        
END GET_SOURCE_CODES;

PROCEDURE GET_PRIOR_MONTHS
(
OUT_CUR						OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of ALL prior months.

Input:
        none

Output:
        list of ALL prior months

-----------------------------------------------------------------------------*/
BEGIN

	OPEN out_cur FOR
	SELECT TO_CHAR(calendar_month) || '/' || TO_CHAR(calendar_year) value,
			 TO_CHAR(calendar_month, '00') || '/' || TO_CHAR(calendar_year, '0000') prior_months
 	  FROM rpt.months 
    WHERE TO_DATE(TO_CHAR(calendar_year) || '/' || TO_CHAR(calendar_month), 'yyyy/mm') < ADD_MONTHS(SYSDATE, -1)
    ORDER BY calendar_year DESC , calendar_month DESC;
        
END GET_PRIOR_MONTHS;

PROCEDURE GET_PARTNER_ORIGIN_ID
(
OUT_CUR						OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of partner origins.

Input:
        none

Output:
        list of partner origins

-----------------------------------------------------------------------------*/
BEGIN

	OPEN out_cur FOR
	SELECT origin_id origin_id,
			 INITCAP(description) name
	  FROM ftd_apps.origins
	 WHERE active = 'Y'
	   AND partner_report_flag = 'Y'
	 ORDER BY name;
        
END GET_PARTNER_ORIGIN_ID;


PROCEDURE GET_ACTIVE_VENDORS
(
OUT_CUR						OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning a list of active vendor ids

Input:

Output:
        cursor containing vendor ids

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  vm.vendor_id,
                trim(vm.vendor_name) vendor_name
        FROM    ftd_apps.vendor_master vm
        WHERE	vm.active = 'Y'
        ORDER BY trim(vm.vendor_name);

END GET_ACTIVE_VENDORS;

PROCEDURE GET_REPORT_BY_ID
(
IN_REPORT_ID                    IN REPORT.REPORT_ID%TYPE,
OUT_REPORT_CUR                  OUT TYPES.REF_CURSOR
)
AS

BEGIN

OPEN OUT_REPORT_CUR FOR
    SELECT
        r.report_id,
        r.name,
        r.description,
        r.report_file_prefix,
        r.report_type,
        r.report_output_type,
        r.report_category,
        r.server_name,
        r.holiday_indicator,
        r.status,
        r.oracle_rdf,
        r.report_days_span,
        r.notes,
        r.acl_name,
        r.retention_days,
        r.run_time_offset,
        r.end_of_day_required,
        r.schedule_day,
        r.schedule_type,
        r.schedule_parm_value
    FROM    report r
    WHERE   r.report_id = in_report_id;

END GET_REPORT_BY_ID;

PROCEDURE GET_MODIFY_ORDER_PARTNERS
(
OUT_CUR                     OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select partner_id,
        partner_name
        from ptn_pi.partner_mapping
        order by partner_id;

END GET_MODIFY_ORDER_PARTNERS;

/*
* This procedure gets invoked by a scheduled DBMS job: events.trigger_florist_reports_email (scheduled to run @ 6.00 AM CST every tuesday)
* Accepts report submitted date as input.
* Retrieves the below scheduled reports generated on in_report_date:
* 			1. Primary/Backup Florist Availability (SCI, Weekly)
*			2. Primary/Backup Florist Availability (Batesville , Weekly)
*			3. Primary/Backup Florist Availability (Legacy, Weekly)
* If report status is Completed (C), it'll be added to the email as an attachment.
* If report status is other than C i.e., 'E' (Error) or 'O' (Open), a message "<report-name> report was unsuccessfully generated." will be added
* to the email body.
* Once looped through all the report submissions, an email will be sent to configured list of recipients.
*/
procedure send_florist_reports_email
(
	in_report_date  date
)
as
	v_smtp_server		varchar2(100);
	v_email_from		varchar2(100);
	v_email_to			varchar2(1000);
	v_email_subject		varchar2(100);			
	v_email_body		varchar2(4000);
	v_arr_attachments	rpt.report_pkg.array_attachments;
	v_msg_type			varchar2(100);
	v_count				number;
begin
	v_smtp_server		:= frp.misc_pkg.get_global_parm_value('MESSAGE_GENERATOR_CONFIG','SMTP_HOST_NAME');
	v_email_from 		:= 'Florist_Reports@ftdi.com';
	v_email_to 			:= frp.misc_pkg.get_global_parm_value('FLORIST_MAINTENANCE','PRIM_BKP_FLORIST_REPORT_RECIPIENTS');
	v_email_subject 	:= 'Weekly Primary/Backup Florist Availability Report';
	v_email_body		:= '';
	v_arr_attachments 	:= rpt.report_pkg.array_attachments();
	v_msg_type			:= 'text/plain';
	v_count 			:= 0;
	
	for v_reports_cur in
	(
		select	rsl.report_submission_id,
				r.name,
				rsl.report_status,
				rdc.report_detail
		from	rpt.report_submission_log rsl,
				rpt.report r,
				rpt.report_detail_char rdc
		where	trunc(rsl.submitted_on) = trunc(in_report_date)
				and rsl.submission_type = 'S'
				and rsl.report_id = r.report_id
				and upper(r.name) in
									(
										'PRIMARY/BACKUP FLORIST AVAILABILITY (SCI, WEEKLY)',
										'PRIMARY/BACKUP FLORIST AVAILABILITY (BATESVILLE , WEEKLY)',
										'PRIMARY/BACKUP FLORIST AVAILABILITY (LEGACY, WEEKLY)'
									)
				and rsl.report_submission_id = rdc.report_submission_id(+)
	)
	loop
		if(v_reports_cur.report_status = 'C') then
			v_count := v_count+1;
			v_arr_attachments.extend(1);
			v_arr_attachments(v_count).attach_name := case 	when upper(v_reports_cur.name) like '%LEGACY%' then 'PrimaryORBackup_Florist_Availability_LEGACY_WEEKLY.slk'
															when upper(v_reports_cur.name) like '%BATESVILLE%' then 'PrimaryORBackup_Florist_Availability_BATESVILLE_WEEKLY.slk'
															when upper(v_reports_cur.name) like '%SCI%' then 'PrimaryORBackup_Florist_Availability_SCI_WEEKLY.slk'
														end;	
			v_arr_attachments(v_count).content_type := 'text/plain';
			v_arr_attachments(v_count).attach_content := v_reports_cur.report_detail;
		else
			v_email_body := v_email_body||case 	when upper(v_reports_cur.name) like '%LEGACY%' then 'PrimaryORBackup_Florist_Availability_LEGACY_WEEKLY.slk'
															when upper(v_reports_cur.name) like '%BATESVILLE%' then 'PrimaryORBackup_Florist_Availability_BATESVILLE_WEEKLY.slk'
															when upper(v_reports_cur.name) like '%SCI%' then 'PrimaryORBackup_Florist_Availability_SCI_WEEKLY.slk'
														end||' report was unsuccessfully generated.'||chr(10);
		end if;
	end loop;
	
	-- Invoke send_mail procedure to trigger the email (with attachments, if any)
	rpt.report_pkg.send_mail
	(
		in_smtp_server	=> v_smtp_server,
		in_from 		=> v_email_from,
		in_to 			=> v_email_to,
		in_subject 		=> v_email_subject,
		in_message_body => v_email_body,
		in_attachments 	=> v_arr_attachments,
		in_message_type => v_msg_type
	);

end send_florist_reports_email;

/*
* This procedure accepts all the parameters required to send an email, along with attachmens and utilizes oracle mail package: UTL_SMTP
* to send email.
*/
PROCEDURE SEND_MAIL 
(
	IN_SMTP_SERVER	VARCHAR2,		
	IN_FROM    		VARCHAR2,
	IN_TO      		VARCHAR2,
	IN_SUBJECT      VARCHAR2,
	IN_MESSAGE_BODY VARCHAR2,
	IN_ATTACHMENTS  ARRAY_ATTACHMENTS DEFAULT NULL,
	IN_MESSAGE_TYPE VARCHAR2 DEFAULT 'TEXT/PLAIN'
)
AS	
	conn		utl_smtp.connection;
	n_offset	NUMBER;
	n_amount	NUMBER        := 1900;
	v_crlf		VARCHAR2(5)   := CHR(13) || CHR(10);	
BEGIN

-- Open the SMTP connection ...
	conn := utl_smtp.open_connection(in_smtp_server);
    utl_smtp.helo(conn, in_smtp_server);
    utl_smtp.mail(conn, in_from);
    utl_smtp.rcpt(conn, in_to);
    
-- Open data
    utl_smtp.open_data(conn);
    
-- Message info
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('To: '|| in_to || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('Date: ' || to_char(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('From: '|| in_from || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('Subject: '|| in_subject || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('MIME-Version: 1.0' || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('Content-Type: multipart/mixed; boundary="SECBOUND"' || v_crlf || v_crlf));    
    
    
-- Message body
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('--SECBOUND' || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('Content-Type: text/plain' || v_crlf || v_crlf));
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw(in_message_body || v_crlf));
     
	if(in_attachments.COUNT > 0) then
		FOR i IN in_attachments.FIRST .. in_attachments.LAST
		LOOP
			-- Attach info
			utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('--SECBOUND' || v_crlf));
			utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('Content-Type: '||in_attachments(i).content_type||' name="'||in_attachments(i).attach_name|| '"' || v_crlf));
			utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('Content-Disposition: attachment; filename="'||in_attachments(i).attach_name|| '"' || v_crlf || v_crlf));
			
			 -- Attach body
			n_offset := 1;
			WHILE n_offset < dbms_lob.getlength(in_attachments(i).attach_content)
			LOOP
				utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw(dbms_lob.substr(in_attachments(i).attach_content, n_amount, n_offset)));
				n_offset := n_offset + n_amount;
			END LOOP;
			utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('' || v_crlf));
		
		END LOOP;
	end if;	
     -- Last boundry
    utl_smtp.write_raw_data(conn, utl_raw.cast_to_raw('--SECBOUND--' || v_crlf));
 
  -- Close data
    utl_smtp.close_data(conn);
    utl_smtp.quit(conn);
    
    dbms_output.put_line('Email sent...');

END SEND_MAIL;

PROCEDURE GET_FLORIST_STATUSES
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of Florist Statuses.

Input:
        none

Output:
        list of Florist Statuses

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  Status,
        Status As Description
        FROM    FTD_APPS.Prim_Bkp_Florist_Status
        ORDER BY status;

END GET_FLORIST_STATUSES;

PROCEDURE GET_SOURCE_TYPES
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of Source Types.

Input:
        none

Output:
        list of Source Types

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  source_type description,
        source_type
        FROM    FTD_APPS.Source_Type_Val
        WHERE status = 'Active'
        order by source_type;

END GET_SOURCE_TYPES;
        
END REPORT_PKG;
.
/