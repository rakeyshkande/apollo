CREATE OR REPLACE
PROCEDURE rpt.acc07_orders_orgn_dnis_pop_tab
			(p_start_date date,
			 p_end_date date
			)
IS
  cursor get_data_cur is
  select origin,
             origin_description,
             phone_or_internet,
             dnis,
             sum(merch_value) merch_value,
             sum(add_ons) add_ons,
             sum(shipping) shipping,
             sum(service_fee) service_fee,
             sum(discount) discount,
             sum(tax) tax,
             sum(total) total,
             sum(order_count) order_count
  from
  (select nvl(co.order_taken_call_center,co.origin_id) origin,
             fao.description origin_description,
             decode(fao.origin_type,'phone','P','internet','I') phone_or_internet,
             co.weboe_dnis_id dnis,
             sum(nvl(cat.product_amount,0)) merch_value,
             sum(nvl(cat.add_on_amount,0)) add_ons,
             sum(nvl(cat.shipping_fee,0)) shipping,
             sum(nvl(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee),0)) service_fee,
             sum(nvl(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount),0)*-1) discount,
             sum(nvl(cat.shipping_tax,0)+nvl(cat.service_fee_tax,0)+nvl(cat.tax,0)) tax,
             sum((nvl(cat.product_amount,0)+nvl(cat.add_on_amount,0)+nvl(cat.shipping_fee,0)+nvl(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee),0)-nvl(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount),0) +nvl(cat.shipping_tax,0)+nvl(cat.service_fee_tax,0)+nvl(cat.tax,0))) total,
             count(distinct cat.order_detail_id) order_count
	from clean.order_details cod,
          clean.orders co,
          ftd_apps.origins fao,
          clean.accounting_transactions cat
	where cat.order_detail_id=cod.order_detail_id
	and cod.order_guid=co.order_guid
	and nvl(co.order_taken_call_center,co.origin_id)=fao.origin_id
	and cat.payment_type<>'NC'
	and cat.transaction_type<>'Refund'
	and fao.origin_id<>'TEST'
and co.origin_id<>'TEST'
and co.origin_id is not null
and co.company_id is not null
and trunc(cat.transaction_date) between trunc(p_start_date) and trunc(p_end_date)
group by nvl(co.order_taken_call_center,co.origin_id),
                 fao.description,
                 decode(fao.origin_type,'phone','P','internet','I'),
             co.weboe_dnis_id
union all
select origin_id,
             description,
             decode(origin_type,'phone','P','internet','I'),
             null,
             0,
             0,
             0,
             0,
             0,
             0,
             0,
             0
from ftd_apps.origins
where origin_id<>'TEST'
)
group by origin,
                 origin_description,
                 phone_or_internet,
                 dnis;

cursor get_origin_count_cur is
select col1 origin,
          count(col1) origin_count
from rpt.rep_tab
group by col1;

begin



for get_data_rec in get_data_cur loop
insert into rpt.rep_tab (col1,
		col2,
		col3,
		col4,
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11,
		col12)
values (get_data_rec.origin,
           get_data_rec.origin_description,
           get_data_rec.phone_or_internet,
           get_data_rec.dnis,
           get_data_rec.merch_value,
          get_data_rec.add_ons,
           get_data_rec.shipping,
           get_data_rec.service_fee,
            get_data_rec.discount,
          get_data_rec.tax,
           get_data_rec.total,
           get_data_rec.order_count);
end loop;

for get_origin_count_rec in get_origin_count_cur loop
	if get_origin_count_rec.origin_count>1 then
		delete from rpt.rep_tab
		where col1=get_origin_count_rec.origin
		and col4 is null;
	end if;
end loop;

end;
.
/
