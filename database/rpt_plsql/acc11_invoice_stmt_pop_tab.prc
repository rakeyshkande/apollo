create or replace PROCEDURE rpt.acc11_invoice_stmt_pop_tab
    (p_start_date IN varchar2,
     p_end_date IN varchar2,
     p_source_code IN varchar2,
     p_company_code IN varchar2 )
IS
---------------------------------------------------------
-- This procedure is used by and extracts data for both
-- Invoice Statement (ACC11 and ACC12) reports.
--
-- These two reports are essentially the same.  The type
-- of invoice source code determines which report
-- generates the corporate customer's invoice statement.
--
-- The ACC11 report generates invoice statements for
-- source codes that do NOT require additional billing
-- information.
--
-- The ACC12 report generates invoice statements for
-- source codes that DO require additional billing
-- information.
---------------------------------------------------------
--
---------------------------------------------------------
-- Work Variables
---------------------------------------------------------
--
v_sql varchar2(30000);

 --
v_source_code_predicate varchar2(2000);
v_company_predicate varchar2(2000);

 --
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');

 --
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');

 --
---------------------------------------------------------
-- The GET_ADD_INFO procedure loads the temporary global
-- table BRAND_TAB.
--
-- The REP_TAB table is loaded in four phases:
-- 1. Current Orders
-- 2. Current Refunds
-- 3. Prior Orders
-- 4. Prior Refunds
---------------------------------------------------------
--
BEGIN
DELETE
FROM rpt.brand_tab;


COMMIT;

 rpt.get_add_info(p_start_date,p_end_date);

 IF p_source_code IS NOT NULL THEN
     v_source_code_predicate:= ' AND fasm.source_code in (' || rpt.multi_value(p_source_code) || ')';
 END IF;

 IF p_company_code = 'ALL' OR INSTR(p_company_code, 'ALL') > 0 THEN
     v_company_predicate := ' AND co.company_id is not null ';
 ELSE
     v_company_predicate:= ' AND co.company_id in (select * from table(string_convert(''' || p_company_code || '''))) ';
 END IF;

 --
v_sql := 'INSERT into RPT.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15,
	 col16,
	 col17,
	 col18,
	 col19,
	 col20,
	 col21,
	 col22,
	 col23,
	 col24,
	 col25,
	 col26,
	 col27,
	 col28,
	 col29,
	 col30,
	 col31,
	 col32,
	 col33,
	 col34,
	 col35,
	 col36,
	 col37,
	 col38,
	 col39
	)
	--------------------------------------
	-- Current Orders
	--------------------------------------
	SELECT	fasm.description corporate_customer,
		''Current'' timeframe,
		''Order'' tran_type,
		fasm.source_code source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id sender_id,
	        cc1.first_name sender_first,
        	cc1.last_name sender_last,
	        cc2.customer_id recipient_id,
        	cc2.first_name recipient_first,
	        cc2.last_name recipient_last,
	        cc2.state delivery_state,
	        cob.bill_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4,
		sum(nvl(cat.product_amount,0)),
		sum(nvl(cat.add_on_amount,0) + nvl(cat.add_on_discount_amount,0)),
		sum(nvl(cat.service_fee,0)),
		sum(nvl(cat.shipping_fee,0)),
		sum(nvl(cob.TAX1_AMOUNT,0)) TAX1_AMOUNT,
			 sum(nvl(cob.TAX2_AMOUNT,0)) TAX2_AMOUNT,
			 sum(nvl(cob.TAX3_AMOUNT,0)) TAX3_AMOUNT,
			 sum(nvl(cob.TAX4_AMOUNT,0)) TAX4_AMOUNT,
		sum(nvl(cat.shipping_tax,0) +
		    nvl(cat.service_fee_tax,0) +
		    nvl(cat.tax,0)),
		sum(nvl(cat.tax,0)),
		-1*sum(nvl(cat.discount_amount,0) + nvl(cat.add_on_discount_amount,0)),
		count(distinct cat.order_detail_id),
		AVG(nvl(cob.TAX1_RATE,0) + nvl(cob.TAX2_RATE,0)+ nvl(cob.TAX3_RATE,0) + nvl(cob.TAX4_RATE,0)) * 100
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	LEFT OUTER JOIN	RPT.brand_tab bt on (bt.order_guid = co.order_guid)
	JOIN	clean.customer cc1 on (cc1.customer_id = co.customer_id)
	JOIN	clean.customer cc2 on (cc2.customer_id = cod.recipient_id)

	JOIN	clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid and cob.additional_bill_indicator = ''N''
                                                             and cp.additional_bill_id is null)
                                                         or (cp.order_guid = co.order_guid and cob.order_bill_id = cp.additional_bill_id
				     and cob.additional_bill_indicator = ''Y'')
	JOIN	ftd_apps.source_master fasm on (fasm.source_code = cod.source_code)
	JOIN	ftd_apps.product_master fapm on (fapm.product_id = cod.product_id)
   JOIN    clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cob.order_bill_id=cat.order_bill_id)

	--
	WHERE	cp.payment_type = ''IN''
        AND     cat.transaction_type <> ''Refund''
	AND	cob.bill_status in (''Billed'',''Settled'')
	AND	cp.payment_indicator = ''P''
	AND	co.origin_id NOT IN (''TARGI'', ''WLMTI'', ''AMZNI'')

	AND  cob.bill_date between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'')  + 0.99999
	 ' || v_source_code_predicate || ' ' || v_company_predicate || '
	--
	GROUP BY
		fasm.description,
		''Current'',
		''Order'',
		fasm.source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id,
	        cc1.first_name,
        	cc1.last_name,
	        cc2.customer_id,
        	cc2.first_name,
	        cc2.last_name,
	        cc2.state,
	        cob.bill_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4
	--
	UNION ALL
	--
	--------------------------------------
	-- Current Refunds
	--------------------------------------
	SELECT	fasm.description corporate_customer,
		''Current'' timeframe,
		''Refund'' tran_type,
		fasm.source_code source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id sender_id,
	        cc1.first_name sender_first,
        	cc1.last_name sender_last,
	        cc2.customer_id recipient_id,
        	cc2.first_name recipient_first,
	        cc2.last_name recipient_last,
	        cc2.state delivery_state,
	        cr.refund_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4,
		sum(nvl(cat.product_amount,0)),
		sum(nvl(cat.add_on_amount,0) + nvl(cat.add_on_discount_amount,0)),
		sum(nvl(cat.service_fee,0)),
		sum(nvl(cat.shipping_fee,0)),
		 sum(nvl(cr.TAX1_AMOUNT,0))*-1            TAX1_AMOUNT,
				 sum(nvl(cr.TAX2_AMOUNT,0))*-1            TAX2_AMOUNT,
				 sum(nvl(cr.TAX3_AMOUNT,0))*-1             TAX3_AMOUNT,
				 sum(nvl(cr.TAX4_AMOUNT,0))*-1            TAX4_AMOUNT,
		sum(nvl(cat.shipping_tax,0) +
		    nvl(cat.service_fee_tax,0) +
		    nvl(cat.tax,0)),
		sum(nvl(cat.tax,0)),


		-1*sum(nvl(cat.discount_amount,0) + nvl(cat.add_on_discount_amount,0)),
		count(distinct cod.order_detail_id),
		AVG(nvl(cr.TAX1_RATE,0) + nvl(cr.TAX2_RATE,0)+ nvl(cr.TAX3_RATE,0) + nvl(cr.TAX4_RATE,0)) * 100
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	LEFT OUTER JOIN	RPT.brand_tab bt on (bt.order_guid = co.order_guid)
	JOIN    clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)
	JOIN	clean.customer cc1 on (cc1.customer_id = co.customer_id)
	JOIN	clean.customer cc2 on (cc2.customer_id = cod.recipient_id)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid)

	JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cp.refund_id)
	JOIN	ftd_apps.source_master fasm on (fasm.source_code = cod.source_code)
	JOIN	ftd_apps.product_master fapm on (fapm.product_id = cod.product_id)
   JOIN    clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cat.refund_id = cr.refund_id)

	--
	WHERE	cp.payment_type = ''IN''
	AND	cr.refund_status = ''Billed''
	AND	cp.payment_indicator = ''R''
        AND     cat.transaction_type = ''Refund''
	AND	co.origin_id NOT IN (''TARGI'', ''WLMTI'', ''AMZNI'')
 	AND    co.order_date between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999
	AND cr.refund_date between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999
	 ' || v_source_code_predicate || ' ' || v_company_predicate || '
	--
	GROUP BY
		fasm.description,
		''Current'',
		''Refund'',
		fasm.source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id,
	        cc1.first_name,
        	cc1.last_name,
	        cc2.customer_id,
        	cc2.first_name,
	        cc2.last_name,
	        cc2.state,
	        cr.refund_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4
	--
	UNION ALL
	--
	--------------------------------------
	-- Prior Orders
	--------------------------------------
	SELECT	fasm.description corporate_customer,
		''Prior'' timeframe,
		''Order'' tran_type,
		fasm.source_code source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id sender_id,
	        cc1.first_name sender_first,
        	cc1.last_name sender_last,
	        cc2.customer_id recipient_id,
        	cc2.first_name recipient_first,
	        cc2.last_name recipient_last,
	        cc2.state delivery_state,
	        cob.bill_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4,
		sum(nvl(cat.product_amount,0)),
		sum(nvl(cat.add_on_amount,0) + nvl(cat.add_on_discount_amount,0)),
		sum(nvl(cat.service_fee,0)),
		sum(nvl(cat.shipping_fee,0)),
		sum(nvl(cob.TAX1_AMOUNT,0)) TAX1_AMOUNT,
			 sum(nvl(cob.TAX2_AMOUNT,0)) TAX2_AMOUNT,
			 sum(nvl(cob.TAX3_AMOUNT,0)) TAX3_AMOUNT,
			 sum(nvl(cob.TAX4_AMOUNT,0)) TAX4_AMOUNT,
		sum(nvl(cat.shipping_tax,0) +
		    nvl(cat.service_fee_tax,0) +
		    nvl(cat.tax,0)),
		sum(nvl(cat.tax,0)),


		-1*sum(nvl(cat.discount_amount,0) + nvl(cat.add_on_discount_amount,0)),
		count(distinct cod.order_detail_id),
		AVG(nvl(cob.TAX1_RATE,0) + nvl(cob.TAX2_RATE,0)+ nvl(cob.TAX3_RATE,0) + nvl(cob.TAX4_RATE,0)) * 100
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	LEFT OUTER JOIN	RPT.brand_tab bt on (bt.order_guid = co.order_guid)
	JOIN	clean.customer cc1 on (cc1.customer_id = co.customer_id)
	JOIN	clean.customer cc2 on (cc2.customer_id = cod.recipient_id)
	JOIN	clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)

	JOIN	clean.payments cp on (cp.order_guid = co.order_guid and cob.additional_bill_indicator = ''N''
                                                             and cp.additional_bill_id is null)
                                                         or (cp.order_guid = co.order_guid and cob.order_bill_id = cp.additional_bill_id
				     and cob.additional_bill_indicator = ''Y'')
	JOIN	ftd_apps.source_master fasm on (fasm.source_code = cod.source_code)
	JOIN	ftd_apps.product_master fapm on (fapm.product_id = cod.product_id)
   JOIN    clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cob.order_bill_id=cat.order_bill_id)

	--
	WHERE	cp.payment_type = ''IN''
	AND	cat.transaction_type = ''Add Bill''
	AND	cob.bill_status in (''Billed'',''Settled'')
	AND	cp.payment_indicator = ''P''
	AND	co.origin_id NOT IN (''TARGI'', ''WLMTI'', ''AMZNI'')
	AND    cob.bill_date between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999
	AND    co.order_date < to_date('''||p_start_date||''',''mm/dd/yyyy'')
	 ' || v_source_code_predicate || ' ' || v_company_predicate || '
	--
	GROUP BY
		fasm.description,
		''Prior'',
		''Order'',
		fasm.source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id,
	        cc1.first_name,
        	cc1.last_name,
	        cc2.customer_id,
        	cc2.first_name,
	        cc2.last_name,
	        cc2.state,
	        cob.bill_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4
	--
	UNION ALL
	--
	--------------------------------------
	-- Prior Refunds
	--------------------------------------
	SELECT	fasm.description corporate_customer,
		''Prior'' timeframe,
		''Refund'' tran_type,
		fasm.source_code source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id sender_id,
	        cc1.first_name sender_first,
        	cc1.last_name sender_last,
	        cc2.customer_id recipient_id,
        	cc2.first_name recipient_first,
	        cc2.last_name recipient_last,
	        cc2.state delivery_state,
	        cr.refund_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4,
		sum(nvl(cat.product_amount,0)),
		sum(nvl(cat.add_on_amount,0) + nvl(cat.add_on_discount_amount,0)),
		sum(nvl(cat.service_fee,0)),
		sum(nvl(cat.shipping_fee,0)),
 sum(nvl(cr.TAX1_AMOUNT,0))*-1            TAX1_AMOUNT,
				 sum(nvl(cr.TAX2_AMOUNT,0))*-1            TAX2_AMOUNT,
				 sum(nvl(cr.TAX3_AMOUNT,0))*-1             TAX3_AMOUNT,
				 sum(nvl(cr.TAX4_AMOUNT,0))*-1            TAX4_AMOUNT,
		sum(nvl(cat.shipping_tax,0) +
		    nvl(cat.service_fee_tax,0) +
		    nvl(cat.tax,0)),
		sum(nvl(cat.tax,0)),


		-1*sum(nvl(cat.discount_amount,0) + nvl(cat.add_on_discount_amount,0)),
		count(distinct cod.order_detail_id),
		AVG(nvl(cr.TAX1_RATE,0) + nvl(cr.TAX2_RATE,0)+ nvl(cr.TAX3_RATE,0) + nvl(cr.TAX4_RATE,0)) * 100
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	LEFT OUTER JOIN	RPT.brand_tab bt on (bt.order_guid = co.order_guid)
	JOIN	clean.customer cc1 on (cc1.customer_id = co.customer_id)
	JOIN	clean.customer cc2 on (cc2.customer_id = cod.recipient_id)


	JOIN	clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cp.refund_id)
	JOIN	ftd_apps.source_master fasm on (fasm.source_code = cod.source_code)
	JOIN	ftd_apps.product_master fapm on (fapm.product_id = cod.product_id)
   JOIN  clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cat.refund_id = cr.refund_id)

	--
	WHERE	cp.payment_type = ''IN''
	AND	cr.refund_status = ''Billed''
        AND     cat.transaction_type = ''Refund''
	AND	cp.payment_indicator = ''R''
	AND	co.origin_id NOT IN (''TARGI'', ''WLMTI'', ''AMZNI'')
	AND   cr.refund_date between to_date('''||p_start_date||''',''mm/dd/yyyy'') and to_date('''||p_end_date||''',''mm/dd/yyyy'') + 0.99999
	AND	co.order_date < to_date('''||p_start_date||''',''mm/dd/yyyy'')
	 ' || v_source_code_predicate || ' ' || v_company_predicate || '
	--
	GROUP BY
		fasm.description,
		''Prior'',
		''Refund'',
		fasm.source_code,
	        cod.external_order_number,
		cod.order_detail_id,
	        co.order_date,
		cod.recipient_id,
	        co.customer_id,
        	cc1.customer_id,
	        cc1.first_name,
        	cc1.last_name,
	        cc2.customer_id,
        	cc2.first_name,
	        cc2.last_name,
	        cc2.state,
	        cr.refund_status,
        	cod.product_id,
		bt.info_name1,
		bt.info_data1,
		bt.info_name2,
		bt.info_data2,
		bt.info_name3,
		bt.info_data3,
		bt.info_name4,
		bt.info_data4 ' ;

DELETE
FROM rpt.rep_tab;


COMMIT;

 --
EXECUTE IMMEDIATE v_sql;

 --
END;
.
/