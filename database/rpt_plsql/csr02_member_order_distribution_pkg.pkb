CREATE OR REPLACE PACKAGE BODY rpt.member_order_distribution_pkg IS

/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 05/12/2006
***              Reason : Enhancement825: Create new Member Order Distribution Report
***             Purpose : Populate rpt.rep_tab with report data
***
*** Special Instructions: Called from BeforeReport trigger
***
*****************************************************************************************/

   v_start_date date;
   v_end_date 	date;
   v_date	date;
   i   pls_integer := 1;


	TYPE  type_head_tab_type is table of varchar2(10) index by pls_integer;

	date_head_tab type_head_tab_type; -- Keeps track of the column headers

	empty_date_head_tab type_head_tab_type; -- empty pl/sql table

	-- valid options entered 
	--		'A'=central account number
	--		'P'=prefix number
	--		'N'=florist number


	-- Populates the pl/sql table with the months in the date range
	PROCEDURE load_columns IS
	BEGIN
		v_date := v_start_date;
		WHILE v_date <= LAST_DAY(v_end_date)
		LOOP
			date_head_tab(i) := TO_CHAR(v_date,'mm/yy');
			v_date := ADD_MONTHS(v_date, 1);
			i := i+ 1;
		END LOOP;
	END load_columns;


	-- format the data passed in to the report
	FUNCTION FORMAT_DATA
	(
	IN_DATA	IN VARCHAR2
	)
	RETURN VARCHAR2
	AS
	/*-----------------------------------------------------------------------------
	Description:
			  This function is responsible for formatting the data entered (florist_id,
			  florist prefix id, central account number) in the report.
			  Valid Florist Id formats are 999999AA, or 99-9999AA.
			  Valid Florist Prefix Id formats are 999999, or 99-9999.
			  Valid Central Account Number formats are 9999. 
			  If the suffix is given, then the suffix will be changed to upper case.
			  If the format is 999999, then change it to 99-9999

	Input:
			  Data	  varchar2

	Output:
			  formatted florist id ready for the search query.
	-----------------------------------------------------------------------------*/

	v_data varchar2(10);

	BEGIN

		case length (in_data)
  		   when 4 then v_data := in_data;
		   when 6 then v_data := substr (in_data, 1, 2) || '-' || substr (in_data, 3);
		   when 7 then v_data := in_data;
		   when 8 then v_data := substr (in_data, 1, 2) || '-' || upper (substr (in_data, 3));
		   when 9 then v_data := upper (in_data);
		   else v_data := in_data;
		end case;

		return v_data;

	END FORMAT_DATA;


	-- populates the report output data in the rpt.rep_tab temp table
	PROCEDURE florist_data (
		p_option in varchar2, 
		p_data in varchar2,
		p_zip_code in varchar2,
		p_date_type in varchar2,
		p_start_date in date,
		p_end_date in date) 
	IS

		--lineout	varchar2(2000) := '';
		v_cancel_sent   varchar2(1) := 'N';
		v_cnt	number(10) := 0;
		v_den_sent   varchar2(1) := 'N';
		v_DS_cnt   number(10) := 0;
		v_filled_cnt   number(10) := 0;
		v_floral_cnt   number(10) := 0;
		v_for_sent   varchar2(1) := 'N';
		v_ftd_sent   varchar2(1) := 'N';
		v_has_live_ftd   varchar2(1) := 'N';
		v_reject_sent   varchar2(1) := 'N';
		v_sent_cnt   number(10) := 0;
		v_out_cur TYPES.REF_CURSOR;

		CURSOR florist_cur (p_data   in varchar2) IS
		SELECT florist_id
		  FROM ftd_apps.florist_master
		 WHERE (p_option = 'N' AND florist_id = p_data)
		UNION
		SELECT florist_id
		  FROM ftd_apps.florist_master
		 WHERE (p_option = 'P' AND SUBSTR(florist_id, 1, 7) = p_data)
		UNION
		SELECT florist_id
		  FROM ftd_apps.florist_master
		 WHERE (p_option = 'A' AND TO_NUMBER(NVL(internal_link_number, '0000')) = TO_NUMBER(p_data));
			 
		CURSOR mercnt_dd_cur (v_str in varchar2, v_zip_code in varchar2, p_florist in varchar2) is 
		SELECT DISTINCT mercury_order_number, TRUNC(created_on) created_on
		  FROM mercury.mercury m
		 WHERE m.delivery_date BETWEEN DECODE(v_str, TO_CHAR(p_start_date, 'MM/YY'), p_start_date, TO_DATE('01/' || v_str, 'DD/MM/YY')) 
				 AND DECODE(v_str, TO_CHAR(p_end_date, 'MM/YY'), p_end_date, LAST_DAY(TO_DATE('01/' || v_str, 'DD/MM/YY'))) + 0.99999
			--AND SUBSTR(m.zip_code, 1, 5) = v_zip_code
			AND DECODE(ftd_apps.florist_query_pkg.get_country_from_zip(m.zip_code), 'CA', 
						SUBSTR(m.zip_code, 1, 3),SUBSTR(m.zip_code, 1, 5)) = v_zip_code
			AND m.filling_florist = p_florist
			AND m.msg_type = 'FTD'
			AND m.mercury_order_number IS NOT NULL;  

		CURSOR mercnt_od_cur (v_str in varchar2, v_zip_code in varchar2, p_florist in varchar2) is 
		SELECT DISTINCT mercury_order_number, TRUNC(created_on) created_on
		  FROM mercury.mercury m
		 WHERE m.order_date BETWEEN DECODE(v_str, TO_CHAR(p_start_date, 'MM/YY'), p_start_date, TO_DATE('01/' || v_str, 'DD/MM/YY')) 
				 AND DECODE(v_str, TO_CHAR(p_end_date, 'MM/YY'), p_end_date, LAST_DAY(TO_DATE('01/' || v_str, 'DD/MM/YY'))) + 0.99999
			--AND SUBSTR(m.zip_code, 1, 5) = v_zip_code
			AND DECODE(ftd_apps.florist_query_pkg.get_country_from_zip(m.zip_code), 'CA', 
						SUBSTR(m.zip_code, 1, 3),SUBSTR(m.zip_code, 1, 5)) = v_zip_code
			AND m.filling_florist = p_florist
			AND m.msg_type = 'FTD'
			AND m.mercury_order_number IS NOT NULL; 

	BEGIN

		FOR j in date_head_tab.first..date_head_tab.last LOOP

			-- get the count of order, DS and floral for the date range and zip_code
			zip_data (
				date_head_tab(j), 
				p_date_type,
				p_zip_code,
				p_start_date,
				p_end_date,
				v_cnt,
				v_DS_cnt,
				v_floral_cnt);

			--dbms_output.put_line('Before : ' || v_cnt || ' **tab** ' || v_DS_cnt || ' **tab** ' || v_floral_cnt);

			-- get the count of orders sent and filled for florist, date range and zip_code
			FOR florist_rec in florist_cur (p_data) LOOP

				--initialize variables
				v_sent_cnt := 0;
				v_filled_cnt := 0;

				-- get the counts from the mercury table
				IF p_date_type = 'D' THEN

					FOR merc_rec IN mercnt_dd_cur(date_head_tab(j), p_zip_code, florist_rec.florist_id)
					LOOP

						-- initialize variables
						v_has_live_ftd := 'N';
						v_cancel_sent := 'N';
						v_reject_sent := 'N';
						v_for_sent := 'N';
						v_ftd_sent := 'N';

						mercury.get_mercury_order_status(
							merc_rec.mercury_order_number,
							merc_rec.created_on,
							v_out_cur);

						FETCH v_out_cur INTO
							v_has_live_ftd,
							v_cancel_sent,
							v_reject_sent,
							v_ftd_sent,
							v_for_sent,
							v_den_sent;

						-- increment the sent counter variable
						IF v_ftd_sent = 'Y' THEN
							v_sent_cnt := v_sent_cnt + 1;
						END IF;

						-- increment the filled counter variable
						IF v_has_live_ftd = 'Y'  OR v_for_sent = 'Y' THEN
							v_filled_cnt := v_filled_cnt + 1;
						END IF;


					END LOOP;

				ELSIF p_date_type = 'O' THEN

					FOR merc_rec IN mercnt_od_cur(date_head_tab(j), p_zip_code, florist_rec.florist_id)
					LOOP

						-- initialize variables
						v_has_live_ftd := 'N';
						v_cancel_sent := 'N';
						v_reject_sent := 'N';
						v_for_sent := 'N';
						v_ftd_sent := 'N';

						mercury.get_mercury_order_status(
							merc_rec.mercury_order_number,
							merc_rec.created_on,
							v_out_cur);

						FETCH v_out_cur INTO
							v_has_live_ftd,
							v_cancel_sent,
							v_reject_sent,
							v_ftd_sent,
							v_for_sent,
							v_den_sent;

						-- increment the sent counter variable
						IF v_ftd_sent = 'Y' THEN
							v_sent_cnt := v_sent_cnt + 1;
						END IF;

						-- increment the filled counter variable
						IF v_has_live_ftd = 'Y' THEN
							v_filled_cnt := v_filled_cnt + 1;
						END IF;


					END LOOP;

				END IF;

				-- get the total count for all florists in that zip_code
				--v_sent_cnt := v_sent_cnt + 1;
				--v_filled_cnt := v_filled_cnt + 1;

			END LOOP; -- florist_cur 

			INSERT INTO rpt.rep_tab	(
				col1,
				col2,
				col3,
				col4,
				col5,
				col6,
				col7,
				col8)
			SELECT
				p_data,
				p_zip_code,
				date_head_tab(j),
				v_cnt,
				v_DS_cnt,
				v_floral_cnt,
				v_sent_cnt,
				v_filled_cnt
			FROM
				DUAL;

			--dbms_output.put_line('After: ' || v_cnt || ' **tab** ' || v_DS_cnt || ' **tab** ' || v_floral_cnt || ' **tab** ' || v_sent_cnt || ' **tab** ' || v_filled_cnt);

			-- initialize variables
			v_cnt := 0;
			v_DS_cnt := 0;
			v_floral_cnt := 0;
			v_sent_cnt := 0;
			v_filled_cnt := 0;

		END LOOP;

	END florist_data;


	-- populates the report output data in the rpt.rep_tab temp table
	PROCEDURE zip_data (
		p_str in varchar2, 
		p_date_type in varchar2,
		p_zip_code in varchar2,
		p_start_date in date,
		p_end_date in date,
		p_cnt out number,
		p_DS_cnt out number,
		p_floral_cnt out number) 
	IS

		CURSOR zipcnt_dd_cur (v_str in varchar2, v_zip_code in varchar2) is
		select count(cod.ROWID) orders,
				 NVL(SUM(DECODE (cod.ship_method, NULL, 0, 'SD', 0, 1)), 0) DS_orders,
				 NVL(SUM(DECODE (cod.ship_method, NULL, 1, 'SD', 1, 0)), 0) floral_orders
		  from clean.order_details cod, clean.customer cc
		 where cod.delivery_date BETWEEN DECODE(v_str, TO_CHAR(p_start_date, 'MM/YY'), p_start_date, TO_DATE('01/' || v_str, 'DD/MM/YY')) 
				 AND DECODE(v_str, TO_CHAR(p_end_date, 'MM/YY'), p_end_date, LAST_DAY(TO_DATE('01/' || v_str, 'DD/MM/YY'))) + 0.99999
			--and SUBSTR(cc.zip_code, 1, 5) = v_zip_code
			AND DECODE(ftd_apps.florist_query_pkg.get_country_from_zip(substr(cc.zip_code,1,10)), 'CA', 
		 			SUBSTR(cc.zip_code, 1, 3),SUBSTR(cc.zip_code, 1, 5)) = v_zip_code
			and cod.recipient_id = cc.customer_id;

		CURSOR zipcnt_od_cur (v_str in varchar2, v_zip_code in varchar2) is
		select count(cod.ROWID) orders,
				 NVL(SUM(DECODE (cod.ship_method, NULL, 0, 'SD', 0, 1)), 0) DS_orders,
				 NVL(SUM(DECODE (cod.ship_method, NULL, 1, 'SD', 1, 0)), 0) floral_orders
		  from clean.order_details cod, clean.customer cc, clean.orders co
		 where co.order_date BETWEEN DECODE(v_str, TO_CHAR(p_start_date, 'MM/YY'), p_start_date, TO_DATE('01/' || v_str, 'DD/MM/YY')) 
				 AND DECODE(v_str, TO_CHAR(p_end_date, 'MM/YY'), p_end_date, LAST_DAY(TO_DATE('01/' || v_str, 'DD/MM/YY'))) + 0.99999
			--and SUBSTR(cc.zip_code, 1, 5) = v_zip_code
			AND DECODE(ftd_apps.florist_query_pkg.get_country_from_zip(substr(cc.zip_code,1,10)), 'CA', 
		 			SUBSTR(cc.zip_code, 1, 3),SUBSTR(cc.zip_code, 1, 5)) = v_zip_code
			and cod.recipient_id = cc.customer_id
			and cod.order_guid = co.order_guid;

	BEGIN

		-- get the counts from the ftd_apps tables
		IF p_date_type = 'D' THEN
			OPEN zipcnt_dd_cur (p_str, p_zip_code);
			FETCH zipcnt_dd_cur INTO p_cnt, p_DS_cnt, p_floral_cnt;
			CLOSE zipcnt_dd_cur;
		ELSIF p_date_type = 'O' THEN
			OPEN zipcnt_od_cur (p_str, p_zip_code);
			FETCH zipcnt_od_cur INTO p_cnt, p_DS_cnt, p_floral_cnt;
			CLOSE zipcnt_od_cur;
		END IF;

		--dbms_output.put_line('Before : ' || p_cnt || ' **tab** ' || p_DS_cnt || ' **tab** ' || p_floral_cnt);	

	END zip_data;


	--***************************************************************************************
	-- The Main Section of the Report
	--***************************************************************************************
	PROCEDURE main( 
		p_start_date 	in varchar2,
		p_end_date 	in varchar2,
		p_date_type   in varchar2,
		p_option 	in varchar2,
		p_data		in varchar2
	)
	IS

		CURSOR florzip_cur (p_data in varchar2) IS
		SELECT  DISTINCT 
			z.zip_code zip_code
		FROM  
			ftd_apps.florist_zips z, 
			ftd_apps.florist_master m
		WHERE  
			(p_option = 'N' AND m.florist_id = p_data)
			AND z.florist_id = m.florist_id			
		UNION
		SELECT  DISTINCT 
			z.zip_code zip_code
		FROM  
			ftd_apps.florist_zips z, 
			ftd_apps.florist_master m
		WHERE  
			(p_option = 'P' AND SUBSTR(m.florist_id, 1, 7) = p_data)
			AND z.florist_id = m.florist_id			
		UNION
		SELECT  DISTINCT 
			z.zip_code zip_code
		FROM  
			ftd_apps.florist_zips z, 
			ftd_apps.florist_master m
		WHERE  
			(p_option = 'A' AND TO_NUMBER(NVL(m.internal_link_number, '0000')) = TO_NUMBER(p_data))
			AND z.florist_id = m.florist_id
		ORDER BY 1;

		v_data   varchar2(10);
		v_zip_code   varchar2(20) := NULL;

	BEGIN

		v_start_date := to_date(p_start_date,'mm/dd/yyyy');
		v_end_date 	:= to_date(p_end_date,'mm/dd/yyyy');

		load_columns;

		v_data := format_data(p_data);

		FOR rec IN florzip_cur(v_data)
		LOOP

			-- format the zip_code based on the country code
			BEGIN
			   SELECT
					DECODE(ftd_apps.florist_query_pkg.get_country_from_zip(rec.zip_code), 'CA', 
						SUBSTR(rec.zip_code, 1, 3),SUBSTR(rec.zip_code, 1, 5)) zip_code
					--SUBSTR(rec.zip_code, 1, 5) zip_code
				INTO
				   v_zip_code
				FROM
				   DUAL;
			EXCEPTION
			   WHEN OTHERS THEN
			      v_zip_code := rec.zip_code;
			END;

			florist_data(p_option, v_data, v_zip_code, p_date_type, v_start_date, v_end_date);

			v_zip_code := NULL;

		END LOOP;

		-- initialize the pl/sql table
		date_head_tab := empty_date_head_tab;

	END main;

END member_order_distribution_pkg;
/
