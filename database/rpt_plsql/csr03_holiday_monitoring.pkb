CREATE OR REPLACE
PACKAGE BODY rpt.csr03_holiday_monitoring IS

  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure print_line( p_str in varchar2 )
is

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
	rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     --	when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end;
--****************************************************************************************************
--
--****************************************************************************************************
procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  print_line( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;

--****************************************************************************************************
--****************************************************************************************************
procedure print_heading( y in number )
is
begin

  print_comment( 'Print Headings' );

  print_line( 'C;Y6;X1;K"# Go To Florists Not Available"' );
  print_line( 'C;Y7;X1;K"% Go To Florists Not Available"' );
  print_line( 'C;Y8;X1;K"   % of Go To EROS Block"' );
  print_line( 'C;Y9;X1;K"   % of Go To GOTO Suspend"' );
  print_line( 'C;Y10;X1;K"   % of Go To FIT Block"' );
  print_line( 'C;Y11;X1;K"   % of Go To Apollo User Block"' );
  print_line( 'C;Y12;X1;K"   % of Go To Closed on Delivery Date"' );
  
  print_line( 'C;Y14;X1;K"# of FTD Mercury Shops Not Available"' );
  print_line( 'C;Y15;X1;K"% of FTD Mercury Shops Not Available"' );
  print_line( 'C;Y16;X1;K"   % of Mercury Shops EROS Block"' );
  print_line( 'C;Y17;X1;K"   % of Mercury Shops Mercury Suspend"' );
  print_line( 'C;Y18;X1;K"   % of Mercury Shops FIT Block"' );
  print_line( 'C;Y19;X1;K"   % of Mercury Shops Apollo User Block"' );
  print_line( 'C;Y20;X1;K"   % of Mercury Shops Closed on Delivery Date "' );
  
  print_line( 'C;Y22;X1;K"Total Zip Codes Covered"' );
  print_line( 'C;Y23;X1;K"# of Zip Codes with No Coverage"' );
  print_line( 'C;Y24;X1;K"% of Zip Codes with No Coverage"' );
  print_line( 'C;Y25;X1;K"    # of GNADD Zip Codes"' );
  print_line( 'C;Y26;X1;K"    % of GNADD Zip Codes"' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_heading;

--****************************************************************************************************
--****************************************************************************************************

procedure print_title(p_run_date in date)
is

begin

  print_comment( 'Print Title' );

  print_line( 'ID;ORACLE' );

  print_line( 'P;Ph:mm:ss');                 -- SM0
  print_line( 'P;FCourier;M200' );           -- SM1
  print_line( 'P;FCourier;M200;SB' );        -- SM2
  print_line( 'P;FCourier;M200;SUB' );       -- SM3
  print_line( 'P;FCourier;M160' );           -- SM4

  print_line( 'F;R5;FG0R' );
  print_line( 'F;w1 1 36' );

  print_line( 'F;R1;FG0L;SD' );
  print_line( 'C;Y1;X1;K"Holiday Monitoring Report"' );
  print_line( 'F;R2;FG0L;SD' );
  print_line( 'C;Y2;X1;K"Delivery Date: '||to_char(p_run_date,'Day mm/dd/yyyy')||'"' );
  print_line( 'F;R3;FG0L;SD' );
  print_line( 'C;Y3;X1;K"Date Time Report Ran: '||to_char(sysdate,'mm/dd/yyyy HH:MI AM')||'"' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_title;

--****************************************************************************************************
--  The print_rows function adds the data records to the file.  In this example there are actually
--  two cursors.  The cursor is just your query from you data model.  If you have a complex data
--  model you will have to some additional PL/SQL to get all of your data.
--****************************************************************************************************
function print_rows(p_run_date    in date ) return number
is
  line       varchar2(32767) := null;  -- print line
  y          number          := 2;    -- first detail line number (column)

  v_goto_shutdown_percent             number := 0;
  v_mercury_shutdown_percent          number := 0;
  v_gnadd_zip_percent                 number := 0;
  v_suspend_percent                   number := 0;
  v_eros_block_percent                number := 0;
  v_fit_block_percent                 number := 0;
  v_user_block_percent                number := 0;
  v_sunday_not_codified_percent       number := 0;
  v_goto_suspend_percent              number := 0;
  v_goto_eros_block_percent           number := 0;
  v_goto_fit_block_percent            number := 0;
  v_goto_user_block_percent           number := 0;
  v_goto_sunday_not_codified_per      number := 0;
  v_no_coverage_zip_percent           number := 0;

  type           my_curs_type is REF CURSOR;
  curs           my_curs_type;

  type ret_rec is record (
      now_hour                 varchar2(80),
      goto_florist_count       number,
      goto_shutdown_count      number,
      mercury_shop_count       number,
      mercury_shutdown_count   number,
      covered_zip_count        number,
      gnadd_zip_count          number,
      suspend_cnt              number,
      eros_block_cnt           number,
      fit_block_cnt            number,
      user_block_cnt           number,
      sunday_not_codified_cnt  number,
      goto_suspend_cnt         number,
      goto_eros_block_cnt      number,
      goto_fit_block_cnt       number,
      goto_user_block_cnt      number,
      goto_sunday_not_codified_cnt number,
      no_coverage_zip_cnt      number
      );

  ret           ret_rec;

begin

  OPEN curs FOR
    SELECT
    ltrim(to_char(clean_date,'HH:mi AM'),0) as now_hour,
    goto_florist_count,
    goto_florist_suspend as "goto_shutdown_count",
    mercury_shop_count,
    mercury_shop_shutdown as "mercury_shutdown_count",
    covered_zip_count,
    gnadd_zip_count,
    suspend_cnt,
    eros_block_cnt,
    fit_block_cnt,
    user_block_cnt,
    sunday_not_codified_cnt,
    goto_suspend_cnt,
    goto_eros_block_cnt,
    goto_fit_block_cnt,
    goto_user_block_cnt,
    goto_sunday_not_codified_cnt,
    no_coverage_zip_cnt
    from rpt.Hourly_Stats_VW
    where trunc(clean_date) = p_run_date
    order by clean_date;

      LOOP
       	 FETCH curs INTO ret;
     	    EXIT WHEN curs%NOTFOUND;

            --**************************************************************************
            --Print DETAIL Line
            --**************************************************************************

            line := 'C;Y5;X'|| to_char(y);
            line := line || ';K"'||ret.now_hour||'"';
            print_line( line );

            line := 'C;Y6;X'|| to_char(y);
            line := line || ';K'||ret.goto_shutdown_count||'';
            print_line( line );

            if ret.goto_florist_count > 0 then
                v_goto_shutdown_percent := ret.goto_shutdown_count / ret.goto_florist_count;
            end if;
            if ret.goto_shutdown_count > 0 then
                v_goto_eros_block_percent := ret.goto_eros_block_cnt / ret.goto_shutdown_count;
                v_goto_suspend_percent := ret.goto_suspend_cnt / ret.goto_shutdown_count;
                v_goto_fit_block_percent := ret.goto_fit_block_cnt / ret.goto_shutdown_count;
                v_goto_user_block_percent := ret.goto_user_block_cnt / ret.goto_shutdown_count;
                v_goto_sunday_not_codified_per := ret.goto_sunday_not_codified_cnt / ret.goto_shutdown_count;
            else
                v_goto_eros_block_percent := 0;
                v_goto_suspend_percent := 0;
                v_goto_fit_block_percent := 0;
                v_goto_user_block_percent := 0;
                v_goto_sunday_not_codified_per := 0;
            end if;
            if ret.mercury_shop_count > 0 then
                v_mercury_shutdown_percent := ret.mercury_shutdown_count / ret.mercury_shop_count;
            end if;
            if ret.mercury_shutdown_count > 0 then
                v_eros_block_percent := ret.eros_block_cnt / ret.mercury_shutdown_count;
                v_suspend_percent := ret.suspend_cnt / ret.mercury_shutdown_count;
                v_fit_block_percent := ret.fit_block_cnt / ret.mercury_shutdown_count;
                v_user_block_percent := ret.user_block_cnt / ret.mercury_shutdown_count;
                v_sunday_not_codified_percent := ret.sunday_not_codified_cnt / ret.mercury_shutdown_count;
            end if;
            if ret.covered_zip_count > 0 then
                v_no_coverage_zip_percent := ret.no_coverage_zip_cnt / ret.covered_zip_count;
                v_gnadd_zip_percent := ret.gnadd_zip_count / ret.covered_zip_count;
            end if;

            print_line( 'F;F%1R;Y7' );--to add a percent sign and 1 decimal place
            line := 'C;Y7;X'|| to_char(y);
            line := line || ';K'||v_goto_shutdown_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y8' );--to add a percent sign and 1 decimal place
            line := 'C;Y8;X'|| to_char(y);
            line := line || ';K'||v_goto_eros_block_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y9' );--to add a percent sign and 1 decimal place
            line := 'C;Y9;X'|| to_char(y);
            line := line || ';K'||v_goto_suspend_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y10' );--to add a percent sign and 1 decimal place
            line := 'C;Y10;X'|| to_char(y);
            line := line || ';K'||v_goto_fit_block_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y11' );--to add a percent sign and 1 decimal place
            line := 'C;Y11;X'|| to_char(y);
            line := line || ';K'||v_goto_user_block_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y12' );--to add a percent sign and 1 decimal place
            line := 'C;Y12;X'|| to_char(y);
            line := line || ';K'||v_goto_sunday_not_codified_per||'';
            print_line( line );

            line := 'C;Y14;X'|| to_char(y);
            line := line || ';K'||ret.mercury_shutdown_count||'';
            print_line( line );

            print_line( 'F;F%1R;Y15' );--to add a percent sign and 1 decimal place
            line := 'C;Y15;X'|| to_char(y);
            line := line || ';K'||v_mercury_shutdown_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y16' );--to add a percent sign and 1 decimal place
            line := 'C;Y16;X'|| to_char(y);
            line := line || ';K'||v_eros_block_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y17' );--to add a percent sign and 1 decimal place
            line := 'C;Y17;X'|| to_char(y);
            line := line || ';K'||v_suspend_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y18' );--to add a percent sign and 1 decimal place
            line := 'C;Y18;X'|| to_char(y);
            line := line || ';K'||v_fit_block_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y19' );--to add a percent sign and 1 decimal place
            line := 'C;Y19;X'|| to_char(y);
            line := line || ';K'||v_user_block_percent||'';
            print_line( line );

            print_line( 'F;F%1R;Y20' );--to add a percent sign and 1 decimal place
            line := 'C;Y20;X'|| to_char(y);
            line := line || ';K'||v_sunday_not_codified_percent||'';
            print_line( line );

            line := 'C;Y22;X'|| to_char(y);
            line := line || ';K'||ret.covered_zip_count||'';
            print_line( line );

            line := 'C;Y23;X'|| to_char(y);
            line := line || ';K'||ret.no_coverage_zip_cnt||'';
            print_line( line );

            print_line( 'F;F%1R;Y24' );--to add a percent sign and 1 decimal place
            line := 'C;Y24;X'|| to_char(y);
            line := line || ';K'||v_no_coverage_zip_percent||'';
            print_line( line );

            line := 'C;Y25;X'|| to_char(y);
            line := line || ';K'||ret.gnadd_zip_count||'';
            print_line( line );

            print_line( 'F;F%1R;Y26' );--to add a percent sign and 1 decimal place
            line := 'C;Y26;X'|| to_char(y);
            line := line || ';K'||v_gnadd_zip_percent||'';
            print_line( line );

            y := y + 1;
            --print_line( 'C;Y'||to_char(y) );

            if g_CLOB = 'N' then
               utl_file.fflush( g_file );
            end if;

      END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 2 then
     print_comment( 'No Rows Returned' );
     line := 'C;Y6;X2';
     line := line || ';K"No Data Found"';
     print_line( line );

     if g_CLOB = 'N' then
	utl_file.fflush( g_file );
     end if;

  end if;

  --**************************************************************************
  --Cleanup Stuff - Close Cursor and Return
  --**************************************************************************
  CLOSE curs;

  RETURN (y);

end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure run_report(p_run_date in varchar2,
		p_submission_id in number,
		p_CLOB in varchar2
		)
is
  l_row_cnt number;
  l_col_cnt number;
  l_status  number;
  v_run_date date;
  v_OUT_STATUS_det	varchar2(30);
  v_OUT_MESSAGE_det	varchar2(1000);
  v_OUT_STATUS_sub 	varchar2(30);
  v_OUT_MESSAGE_sub	varchar2(1000);

begin

   if p_run_date is not null then
      v_run_date := to_date(p_run_date,'mm/dd/yyyy');
   else
      v_run_date := add_months(trunc(sysdate,'MONTH'), -1);
   end if;

--
-- Prepare for either CLOB output or REPORT_DIR output
--

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
			P_SUBMISSION_ID,
			'SYS',
			'S' ,
			null ,
			v_OUT_STATUS_DET,
			v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
       			(
			P_SUBMISSION_ID,
			empty_clob(),
			v_out_status_det,
			v_out_message_det
		        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','CSR03_Holiday_Monitoring.slk', 'W' );
   end if;

    print_title( v_run_date);

    print_heading(3);

    l_row_cnt := print_rows( v_run_date );

    print_line( 'E' );

   --
   -- Update Database with CLOB and set report submission status (if CLOB'ing)
   -- Close report file (if not CLOB'ing
   --

      if p_CLOB = 'Y' then
         begin
   	-- update the report_detail_char table with the package global variable g_clob
   	rpt.report_clob_pkg.update_report_detail_char
   			(
   			 p_submission_id,
   			 v_out_status_det,
   			 v_out_message_det
   			);
   	if v_out_status_det = 'N' then
   	-- if db update failure, update report_submission_log with ERROR status
   	rpt.report_pkg.update_report_submission_log
   			(
   			 p_submission_id,
   			 'REPORT',
   			 'E',
   			 v_out_message_det,
   			 v_out_status_sub,
   			 v_out_message_sub
   			);
   	else
   	-- if db update successful, update report_submission_log with COMPLETE status
   	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
   			(
   			 P_SUBMISSION_ID,
   			 'SYS',
   			 'C' ,
   			 null ,
   			 v_out_status_det ,
   			 v_out_message_det
   			);
   	end if;
   	--exception
   	--	when others then srw.message(32321,'Error in p');
        end;
        commit;
      else
        utl_file.fflush( g_file );
        utl_file.fclose( g_file );
   end if;

  end run_report;
end;
.
/
