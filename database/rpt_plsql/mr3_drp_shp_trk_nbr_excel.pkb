CREATE OR REPLACE
PACKAGE BODY rpt.mr3_drp_shp_trk_nbr_EXCEL IS

--  g_file text_io.file_type;
  g_first_sw varchar2(1) := 'Y';

--*********************************************************************************
--
-- Procedure Name: print_line
--
-- Description:    Controls the Excel (SLYK) output.  The p_clob variable
--                 determines if the output is directed to a file or the
--                 database.
--
--                 If file output, the filename and location is set in the
--                 BEFOREREPORT program module.  The "put_line" function
--                 within "text_io" package is used to write output to the
--                 file.
--
--                 If database output, the "clob_variable_update" procedure
--                 within the "report_clob_pkg" package is used to update the
--                 G_CLOB global package variable.  The AFTERREPORT program
--                 module calls the "update_report_detail_char" procedure
--                 within the "report_clob_pkg" package to update the
--                 REPORT_DETAIL_CHAR table entry for this report execution.
--
--*********************************************************************************

procedure print_line( p_str in varchar2 )
is
	v_out_status_det   varchar2(30);
	v_out_message_det  varchar2(1000);
	v_out_status_sub   varchar2(30);
	v_out_message_sub  varchar2(1000);

begin
/*
	if :p_clob = 'N' then
		begin
			text_io.put_line( g_file, p_str );
			exception
				when others then null;
		end;
	else
*/
		begin
			rpt.report_clob_pkg.clob_variable_update(p_str);
		exception
			when others then
                        null;
                        --srw.message(32321,'Error in p');
		end;

--	end if;

end print_line;

--*********************************************************************
--
--*********************************************************************
procedure print_comment( p_comment varchar2)
is
begin
    return;
    print_line( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );
end print_comment;

--***************************************************************************************

procedure print_headings(y in number)
is
begin

  --print_line( 'F;R'||y||';FG0L;SM2' );
	print_line( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
  print_line( 'C;Y'||y||';X2;K"Order #"' );
	print_line( 'F;Y'||to_char(y)||';X3;FG0L;SM2' );
  print_line( 'C;Y'||y||';X3;K"Product Id"' );
	print_line( 'F;Y'||to_char(y)||';X4;FG0L;SM2' );
  print_line( 'C;Y'||y||';X4;K"Carrier"' );
	print_line( 'F;Y'||to_char(y)||';X5;FG0L;SM2' );
  print_line( 'C;Y'||y||';X5;K"Ship Method"' );
	print_line( 'F;Y'||to_char(y)||';X6;FG0L;SM2' );
  print_line( 'C;Y'||y||';X6;K"Order Date"' );
	print_line( 'F;Y'||to_char(y)||';X7;FG0L;SM2' );
  print_line( 'C;Y'||y||';X7;K"Delivery Date"' );
	print_line( 'F;Y'||to_char(y)||';X8;FG0L;SM2' );
  print_line( 'C;Y'||y||';X8;K"Ship Date"' );
	print_line( 'F;Y'||to_char(y)||';X9;FG0L;SM2' );
	print_line( 'C;Y'||y||';X9;K"Zone Jump Label Date"' );
	print_line( 'F;Y'||to_char(y)||';X10;FG0L;SM2' );
  print_line( 'C;Y'||y||';X10;K"Time Sent"' );
	print_line( 'F;Y'||to_char(y)||';X11;FG0L;SM2' );    
  print_line( 'C;Y'||y||';X11;K"Vendor"' );
	print_line( 'F;Y'||to_char(y)||';X12;FG0L;SM2' );    
  print_line( 'C;Y'||y||';X12;K"Tracking Number"' );
	print_line( 'F;Y'||to_char(y)||';X13;FG0L;SM2' );    
  print_line( 'C;Y'||y||';X13;K"Mercury #"' );

end print_headings;

--***************************************************************************************

procedure print_title (p_start_date   in varchar2,
                       p_end_date     in varchar2,
                       p_date_ind     in varchar2,
                       p_tracking_ind in varchar2)
is
	date_hdg				varchar2(50)		:= 'Date Range: 01/01/0001 thru 12/31/9999';
	parm_hdg				varchar2(100)		:= NULL;

begin

  print_line( 'ID;ORACLE' );

  print_line( 'P;Ph:mm:ss');

  print_comment( 'Fonts' );
  print_line( 'P;FCourier New;M200' );
  print_line( 'P;FCourier New;M200;SB' );
  print_line( 'P;FCourier New;M200;SUB' );
  print_line( 'P;FCourier New;M200' );
  print_line( 'P;FCourier New;M160' );

  print_comment( 'Title Row' );

	date_hdg	:= 'Date Range: ' || p_start_date || ' thru ' || p_end_date;

	parm_hdg	:= ' Run Parm: Select by ';
	if p_date_ind = 'S' then
		 parm_hdg := parm_hdg || 'Ship Date (or Zone Jump date where exists)  ';
	else
		 parm_hdg := parm_hdg || 'Delivery Date ';
	end if;

	if nvl(p_tracking_ind,'B') = 'Y' then
		 parm_hdg := parm_hdg || 'With Tracking Numbers';
	elsif nvl(p_tracking_ind,'B') = 'N' then
		 parm_hdg := parm_hdg || 'Without Tracking Numbers';
	else
		 parm_hdg := parm_hdg || 'Both With and Without Tracking Numbers';
	end if;

  print_line( 'F;R1;FG0L;SM2' );
  print_line( 'C;Y1;X2;K"Drop Ship Tracking Number Report - ' || date_hdg || '  ' || parm_hdg || '"' );

end print_title;

--***************************************************************************************

function print_rows( p_start_date in varchar2,
		     p_end_date in varchar2,
                     p_date_ind in varchar2,
		     p_tracking_ind in varchar2
		                               ) return number is

  row_cnt  number          := 0;
  line     varchar2(32767) := null;
  n        number;
  y        number:= 5;
  sv_bill_group	varchar2(20)			:= 'XXX';
  sv_bill_desc	varchar2(30)		:= 'Unknown Bill Type';

  type         		tot_tbl_type is table of varchar2(10)
                	index by binary_integer;

  null_tot_tbl		tot_tbl_type;         		-- This is used to clear a table
  grp_tot_tbl			tot_tbl_type;         		-- Keeps track of the cells that contain totals for each GROUP change
  idx_g						number(4) 			:= 1;			-- Index for grp_tot_tbl
  grp_y						number    			:= 3;     -- Holds the starting Y coordinate for a GROUP

--
-- DEFINE Ship Cursor (Perform a FIND on ==> 'BEGIN')
--
  cursor get_rec_ship_cur
  is
		select col1 record_type,
           col2 order_date,
           col3 ship_date,
           col4 delivery_date,
           col5 eod_delivery_date,
           col6 order_number,
           col7 product_id,
           col8 ship_method,
           col9 tracking_number,
           col10 time_sent,
           col11 mercury_number,
           col12 vendor_name,
           col13 carrier_name,
           col14 zone_jump_label_date
		from	 rpt.rep_tab
		order by
					 col3,
					 col10,
					 col12,
					 col7,
					 col11,
					 col13
		;

--
-- DEFINE Delivery Cursor (Perform a FIND on ==> 'BEGIN')
--
  cursor get_rec_delivery_cur
  is
		select col1 record_type,
           col2 order_date,
           col3 ship_date,
           col4 delivery_date,
           col5 eod_delivery_date,
           col6 order_number,
           col7 product_id,
           col8 ship_method,
           col9 tracking_number,
           col10 time_sent,
           col11 mercury_number,
           col12 vendor_name,
           col13 carrier_name,
           col14 zone_jump_label_date
		from	 rpt.rep_tab
		order by
					 col4,
					 col10,
					 col12,
					 col7,
					 col11,
					 col13
		;
--
--------------------------------------------------------------------
-- BEGIN Cursor Processing
--------------------------------------------------------------------
--
begin
          g_first_sw	:= 'Y';
	   y     := 4;
	   idx_g := 1;
	   grp_y := y;

      if p_date_ind = 'S' then
      for get_rec_ship_rec IN get_rec_ship_cur loop

				print_line( 'F;Y'||to_char(y)||';X2;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X2';
        line := line || ';K"'||get_rec_ship_rec.order_number||'"';
        print_line( line );

      	print_line( 'F;Y'|| to_char(y) || ';X3;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X3';
        line := line || ';K"'||get_rec_ship_rec.product_id||'"';
        print_line( line );

		  print_line( 'F;Y'|| to_char(y) || ';X4;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X4';
        line := line || ';K"'||get_rec_ship_rec.carrier_name||'"';
        print_line( line );

      	print_line( 'F;Y'|| to_char(y) || ';X5;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X5';
        line := line || ';K"'||get_rec_ship_rec.ship_method||'"';
        print_line( line );

      	print_line( 'F;Y'|| to_char(y) || ';X6;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X6';
        line := line || ';K"'||get_rec_ship_rec.order_date||'"';
        print_line( line );

      	print_line( 'F;Y'|| to_char(y) || ';X7;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X7';
        line := line || ';K"'||get_rec_ship_rec.delivery_date||'"';
        print_line( line );

      	print_line( 'F;Y'|| to_char(y) || ';X8;FG0L;SM3' );
        line := 'C;Y'|| to_char(y) || ';X8';
        line := line || ';K"'||get_rec_ship_rec.ship_date||'"';
        print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X9;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X9';
		  line := line || ';K"'||get_rec_ship_rec.zone_jump_label_date||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X10;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X10';
		  line := line || ';K"'||get_rec_ship_rec.time_sent||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X11;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X11';
		  line := line || ';K"'||get_rec_ship_rec.vendor_name||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X12;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X12';
		  line := line || ';K"'||get_rec_ship_rec.tracking_number||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X13;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X13';
		  line := line || ';K"'||get_rec_ship_rec.mercury_number||'"';
        print_line( line );

        y := y + 1;
      end loop;
      else
      for get_rec_delivery_rec IN get_rec_delivery_cur loop

			print_line( 'F;Y'||to_char(y)||';X2;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X2';
		  line := line || ';K"'||get_rec_delivery_rec.order_number||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X3;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X3';
		  line := line || ';K"'||get_rec_delivery_rec.product_id||'"';
		  print_line( line );

		  print_line( 'F;Y'|| to_char(y) || ';X4;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X4';
		  line := line || ';K"'||get_rec_delivery_rec.carrier_name||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X5;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X5';
		  line := line || ';K"'||get_rec_delivery_rec.ship_method||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X6;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X6';
		  line := line || ';K"'||get_rec_delivery_rec.order_date||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X7;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X7';
		  line := line || ';K"'||get_rec_delivery_rec.delivery_date||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X8;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X8';
		  line := line || ';K"'||get_rec_delivery_rec.ship_date||'"';
		  print_line( line );

		  print_line( 'F;Y'|| to_char(y) || ';X9;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X9';
		  line := line || ';K"'||get_rec_delivery_rec.zone_jump_label_date||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X10;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X10';
		  line := line || ';K"'||get_rec_delivery_rec.time_sent||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X11;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X11';
		  line := line || ';K"'||get_rec_delivery_rec.vendor_name||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X12;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X12';
		  line := line || ';K"'||get_rec_delivery_rec.tracking_number||'"';
		  print_line( line );

			print_line( 'F;Y'|| to_char(y) || ';X13;FG0L;SM3' );
		  line := 'C;Y'|| to_char(y) || ';X13';
		  line := line || ';K"'||get_rec_delivery_rec.mercury_number||'"';
        print_line( line );

        y := y + 1;
      end loop;
      end if;

    	-------------------------------------------------
    	--Print "No Rows Returned" line if no data exists
  	  -------------------------------------------------
  	  if y = 4 then
  	     print_comment( 'No Rows Returned' );
	     line := 'C;Y'|| to_char(y) || ';X2';
  	     line := line || ';K"No Data Found"';
    	     print_line( line );
	     y := y + 1;
  	     print_line( 'C;Y'||to_char(y) );
	     y := y + 1;
             line := 'C;Y'|| to_char(y) || ';X2';
  	     line := line || ';K"Total Orders = '||to_char(y-6)||'"';
    	   print_line( line );
          ELSE
	     y := y + 1;
             line := 'C;Y'|| to_char(y) || ';X2';
  	     line := line || ';K"Total Orders = '||to_char(y-5)||'"';
    	   print_line( line );
  	  end if;

    	return row_cnt;

end print_rows;

--***************************************************************************************

procedure print_widths
is
begin

  print_comment( 'Format Column Widths' );

  print_line( 'F;W1 1 5' );
  print_line( 'F;W2 2 20' );       -- Order #
  print_line( 'F;W3 3 15' );       -- Product Id
  print_line( 'F;W4 4 15' );       -- Carrier Name
  print_line( 'F;W5 5 15' );       -- Ship Method
  print_line( 'F;W6 6 15' );       -- Order Date
  print_line( 'F;W7 7 15' );       -- Delivery Date
  print_line( 'F;W8 8 15' );       -- Ship Date
  print_line( 'F;W9 9 20' );       -- Zone Jump Label Date
  print_line( 'F;W10 10 15' );       -- Time Sent
  print_line( 'F;W11 11 50' );       -- Vendor
  print_line( 'F;W12 12 20' );     -- Tracking Number
  print_line( 'F;W13 13 15' );     -- Mercury #

  print_line( 'E' );

end print_widths;

--*********************************************************************************
--
-- Procedure Name: show
--
-- Description:    This is the main procedure.  It is called by the BEFOREREPORT
--                 program module to generate the Excel (SYLK) file.
--
--                 All routines above are subordinate to this procedure.
--
--*********************************************************************************

procedure show(
--							p_file					in text_io.file_type,
							p_start_date		in varchar2 default sysdate,
							p_end_date			in varchar2 default sysdate,
							p_date_ind			in varchar2,
							p_tracking_ind	in varchar2
							)
is

  l_row_cnt number;
  l_col_cnt number;
  l_status  number;

begin

--  g_file := p_file;

  print_title(p_start_date,
                       p_end_date,
                       p_date_ind,
                       p_tracking_ind);

  print_headings(3);

  l_row_cnt := print_rows(p_start_date,
                          p_end_date,
  			  p_date_ind,
  			  p_tracking_ind
  			);

  print_widths;

end show;

end;
.
/
