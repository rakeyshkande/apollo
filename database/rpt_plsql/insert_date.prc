CREATE OR REPLACE
procedure rpt.insert_date
is
v_date date;
i number(10);
begin
i:=0;
v_date:=to_date('01-JUL-05');
loop
exit when v_date=to_date('31-DEC-10','DD-MON-RR');

insert into rpt.dates(calendar_date)
values(v_date);

v_date:=v_date+1;
end loop;

commit;
end;
.
/
