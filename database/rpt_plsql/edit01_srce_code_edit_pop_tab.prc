CREATE OR REPLACE
procedure rpt.EDIT01_Srce_Code_Edit_Pop_Tab
		(p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_company_code in varchar2,
		 p_change_type in varchar2
		)
IS
  v_start_date 		date		:= to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		date		:= to_date(p_end_date,'mm/dd/yyyy');
  v_company_predicate	varchar2(1000)	:= NULL;
  v_change_predicate	varchar2(1000)	:= NULL;
  v_sql			varchar2(3000)	:= NULL;

BEGIN
  --
  --**********************************************************************
  -- Set the company code and change type WHERE clause predicates
  --**********************************************************************
  --
  if p_company_code <> 'ALL' then
	v_company_predicate :=
		' AND FASM.company_id in ('||rpt.multi_value(p_company_code)||') ';
  end if;
  --
  if p_change_type <> 'ALL' then
	v_change_predicate :=
		' AND FASM .operation$ in ('||rpt.multi_value(p_change_type)||') ';
  end if;
  --
  --**********************************************************************
  -- Source Code Edit Query
  --**********************************************************************
  --
  v_sql :=
  'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14,
	 col15,
	 col16,
	 col17,
	 col18,
	 col19,
         col20
	)
	SELECT	decode(FASM.operation$, ''DEL'', ''3'', ''INS'', ''2'', ''1'') sort1,
		decode(FASM.operation$, ''DEL'', ''4'', ''INS'', ''3'', ''UPD_OLD'', ''2'', ''UPD_NEW'', ''1'', ''0'') sort2,
	        decode(FASM.operation$, ''DEL'', ''Expired'', ''INS'', ''New'', ''UPD_OLD'', ''Updated'', ''UPD_NEW'', ''Updated'', ''Updated'') change_category,
	        decode(FASM.operation$, ''DEL'', ''Expired'', ''INS'', ''New'', ''UPD_OLD'', ''Old'', ''UPD_NEW'', ''Updated'', ''Change?'') change_type,
		FASM.source_code	  source_code,
		FASM.description	  description,
		FASM.source_type	  source_type,
		FASM.company_id		  company_id,
		FASM.start_date		  start_date,
		FASM.end_date		  end_date,
		FASM.updated_by		  updated_by,
		concat(substr(au.first_name,1,1),au.last_name) requested_by,
		FASM.price_header_id	  price_header_id,
		faph.description	  price_code_description,
		FASM.snh_id		  snh_id,
		fasnh.description	  service_fee_code_description,
		FASM.promotion_code	  pst_code,
		FASM.bonus_promotion_code promotion_code,
		to_char(FASM.timestamp$, ''mm/dd/yyyy hh:mi:ss'') updated_on,
                mprr.description          redemption_rate_description
	--
	FROM 	ftd_apps.source_master$ FASM
	JOIN	ftd_apps.price_header faph
		   on (faph.price_header_id = FASM.price_header_id)
	JOIN	ftd_apps.snh fasnh
		   on (fasnh.snh_id = FASM.snh_id)
	LEFT
	OUTER
	JOIN	aas.users au
		   on (to_char(au.user_id) = FASM.requested_by)
	LEFT
	OUTER
	JOIN	ftd_apps.miles_points_redemption_rate mprr
		   on (mprr.mp_redemption_rate_id = FASM.mp_redemption_rate_id)
	--
	WHERE	trunc(FASM.timestamp$) between '''
		|| v_start_date || ''' and ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_change_predicate
   ;

  EXECUTE IMMEDIATE v_sql
  ;

END;
.
/
