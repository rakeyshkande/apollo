create or replace PROCEDURE RPT.ACC03_POP_TAB
	(p_start_date in varchar2,
	 p_end_date in varchar2,
	 p_company_code in varchar2
	)
IS
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
BEGIN
--
--**************************************************************
--
-- ACC03 Accounting Report - Daily Billing Log
--
--**************************************************************
--
--
--**************************************************************
--  Orders
--**************************************************************
--
INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10
	)
SELECT  bill_group,
	bill_type,
	sum(nvl(ftd_amt, 0)),
	sum(nvl(ftdca_amt, 0)),
	sum(nvl(gift_amt, 0)),
	sum(nvl(fusa_amt, 0)),
	sum(nvl(flor_amt, 0)),
	sum(nvl(fdir_amt, 0)),
	sum(nvl(roses_amt, 0)),
	sum(nvl(pro_amt, 0))
FROM (	SELECT
	CASE when (cp.payment_type = 'AX' or (cp.payment_type = 'PC' and cc_type = 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'DC'
	     then 'JDE'
	     when (cp.payment_type in ('DI', 'MC', 'VI', 'CB') or (cp.payment_type = 'PC' and cc_type <> 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'GD'
	     then 'OTH'
	     when cp.payment_type = 'GC'
	     Then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id = 'AMZNI')
	     Then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id = 'WLMTI')
	     then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id in (mcp.ftd_origin_mapping))
	     then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id in (pm.ftd_origin_mapping))
	     then 'OTH'
	     when cp.payment_type = 'IN'
	     Then 'OTH'
	     when cp.payment_type = 'MS'
	     then 'OTH'
	     when (cp.payment_type = 'PP')
	     then 'OTH'
	     when (cp.payment_type = 'BM')
	     then 'OTH'
	     when (cp.payment_type = 'UA')
	     then 'OTH'
	END  bill_group,
	CASE when (cp.payment_type = 'AX' or (cp.payment_type = 'PC' and cc_type = 'AX'))
	     then 'American Express'
	     when cp.payment_type = 'DC'
	     then 'Diners Club'
	     when (cp.payment_type in ('DI', 'MC', 'VI', 'CB') or (cp.payment_type = 'PC' and cc_type <> 'AX'))
	     then 'Bank Cards'
	     when cp.payment_type = 'GD'
	     then 'Gift Card'
	     when cp.payment_type = 'GC'
	     Then 'Gift Certificates'
	     when cp.payment_type = 'IN' and  cp.origin_id = 'AMZNI'
	     then 'Amazon'
	     when cp.payment_type = 'IN' and  cp.origin_id = 'WLMTI'
	     then 'Walmart'
	     When (cp.Payment_Type = 'IN' And  Cp.Origin_Id In (mcp.Ftd_Origin_Mapping))
	     then mcp.mailbox_name
	     When (cp.Payment_Type = 'IN' And  Cp.Origin_Id In (pm.Ftd_Origin_Mapping))
	     then pm.partner_name
	     when cp.payment_type = 'IN'
	     Then 'Invoiced Orders'
	     when cp.payment_type = 'MS'
	     then 'Military Star (AAFES)'
	     when cp.payment_type = 'PP'
	     then 'PayPal'
	     when cp.payment_type = 'BM'
	     then 'Bill Me Later'
	     when cp.payment_type = 'UA'
	     Then 'United Mileage Plus'
	     else 'Unknown Bill Type'
	END  bill_type,
	sum(CASE when cp.company_id='FTD'
		 then cp.credit_amount
	    	else 0
	    	END) ftd_amt,
	sum(CASE when cp.company_id='GIFT'
		 then cp.credit_amount
		 else 0
	    	END) gift_amt,
	sum(CASE when cp.company_id='FTDCA'
		 then cp.credit_amount
		else 0
	    	END) ftdca_amt,
	sum(CASE when cp.company_id='FUSA'
		 then cp.credit_amount
		 else 0
	    	END) fusa_amt,
	sum(CASE when cp.company_id='FLORIST'
		 then cp.credit_amount
		 else 0
	    	END) flor_amt,
	sum(CASE when cp.company_id='FDIRECT'
		 then cp.credit_amount
		else 0
	    	END) fdir_amt,
  sum(CASE when cp.company_id='ROSES'
		 then cp.credit_amount
		else 0
	    	END) roses_amt,
	sum(CASE when cp.company_id='ProFlowers'
		 then cp.credit_amount
		else 0
	    	END) pro_amt
--
FROM		(select distinct 	p.payment_id,
				p.credit_amount,
				p.payment_type,
				p.payment_indicator,
				p.cc_id,
				o.origin_id,
				O.Company_Id
            		from clean.payments p
           			join clean.orders o
           			on p.order_guid=o.order_guid
            		join clean.order_details od
            		On O.Order_Guid=Od.Order_Guid
        		AND P.BILL_STATUS IN ('Billed', 'Settled')
        		and trunc(p.bill_date) between v_start_date and v_end_date
        	) cp
LEFT OUTER JOIN	clean.credit_cards ccc
			On (Ccc.Cc_Id = Cp.Cc_Id)
Left outer Join Ptn_Mercent.Mrcnt_Channel_Mapping Mcp
on  MCP.FTD_ORIGIN_MAPPING = cp.origin_id
-- Left outer Join ptn_pi.partner_mapping pm
-- CR 16030 : Exclude individual Ariba partners records from the report and add their billing amount into 'Invoiced Orders' section.
Left outer Join (select * from ptn_pi.partner_mapping where ftd_origin_mapping <> 'ARI') pm
on pm.ftd_origin_mapping = cp.origin_id
--
WHERE		cp.payment_type <> 'NC'
AND		cp.payment_indicator='P'
AND		(p_company_code = 'ALL' or cp.company_id in (SELECT * FROM TABLE(rpt.string_convert(p_company_code))))
--
GROUP BY
	CASE when (cp.payment_type = 'AX' or (cp.payment_type = 'PC' and cc_type = 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'DC'
	     then 'JDE'
	     when (cp.payment_type in ('DI', 'MC', 'VI', 'CB') or (cp.payment_type = 'PC' and cc_type <> 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'GD'
	     then 'OTH'
	     when cp.payment_type = 'GC'
	     THEN 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id = 'AMZNI')
	     Then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id = 'WLMTI')
	     then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id in (mcp.ftd_origin_mapping))
	     then 'OTH'
	     when (cp.payment_type = 'IN' and cp.origin_id in (pm.ftd_origin_mapping))
	     then 'OTH'
	     when cp.payment_type = 'IN'
	     Then 'OTH'
	     when cp.payment_type = 'MS'
	     then 'OTH'
	     when (cp.payment_type = 'PP')
	     then 'OTH'
	     when (cp.payment_type = 'BM')
	     then 'OTH'
	     when (cp.payment_type = 'UA')
	     then 'OTH'
	END,
	CASE when (cp.payment_type = 'AX' or (cp.payment_type = 'PC' and cc_type = 'AX'))
	     then 'American Express'
	     when cp.payment_type = 'DC'
	     then 'Diners Club'
	     when (cp.payment_type in ('DI', 'MC', 'VI', 'CB') or (cp.payment_type = 'PC' and cc_type <> 'AX'))
	     then 'Bank Cards'
	     when cp.payment_type = 'GD'
	     then 'Gift Card'
	     when cp.payment_type = 'GC'
	     THEN 'Gift Certificates'
	     when cp.payment_type = 'IN' and  cp.origin_id = 'AMZNI'
	     then 'Amazon'
	     when cp.payment_type = 'IN' and  cp.origin_id = 'WLMTI'
	     then 'Walmart'
	     When (cp.Payment_Type = 'IN' And  Cp.Origin_Id In (mcp.Ftd_Origin_Mapping))
	     then mcp.mailbox_name
	     When (cp.Payment_Type = 'IN' And  Cp.Origin_Id In (pm.Ftd_Origin_Mapping))
	     then pm.partner_name
	     when  cp.payment_type = 'IN'
	     then 'Invoiced Orders'
	     when cp.payment_type = 'MS'
	     then 'Military Star (AAFES)'
	     when cp.payment_type = 'PP'
	     then 'PayPal'
	     when cp.payment_type = 'BM'
	     then 'Bill Me Later'
	     when cp.payment_type = 'UA'
	     Then 'United Mileage Plus'
	     else 'Unknown Bill Type'
	END
        UNION ALL
	SELECT	'JDE',
		'American Express',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'JDE',
		'Diners Club',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'JDE',
		'Bank Cards',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Amazon',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Gift Card',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Gift Certificates',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Invoiced Orders',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Military Star (AAFES)',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Walmart',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'PayPal',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Bill Me Later',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'United Mileage Plus',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	Select	'OTH',
		m.mailbox_name,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM ptn_mercent.mrcnt_channel_mapping m
        UNION ALL
	Select	'OTH',
		pm.partner_name,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM ptn_pi.partner_mapping pm
	-- CR 16030 : Exclude individual Ariba partners records from the report and add their billing amount into 'Invoiced Orders' section.
	where pm.ftd_origin_mapping <> 'ARI'
)
GROUP BY
	bill_group,
	bill_type
ORDER BY
	BILL_TYPE
;
--
--
--**************************************************************
--  Refunds
--**************************************************************
--
INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10
	)
SELECT  bill_group,
	bill_type,
	sum(nvl(ftd_amt, 0)),
	sum(nvl(ftdca_amt, 0)),
	sum(nvl(gift_amt, 0)),
	sum(nvl(fusa_amt, 0)),
	sum(nvl(flor_amt, 0)),
	sum(nvl(fdir_amt, 0)),
	sum(nvl(roses_amt, 0)),
	sum(nvl(pro_amt, 0))
FROM (	SELECT
	CASE when (cp.payment_type = 'AX' or (cp.payment_type = 'PC' and cc_type = 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'DC'
	     then 'JDE'
	     when (cp.payment_type in ('DI', 'MC', 'VI', 'CB') or (cp.payment_type = 'PC' and cc_type <> 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'GD'
	     then 'OTH'
	     when cp.payment_type = 'GC'
	     THEN 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id = 'AMZNI')
	     then 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id = 'WLMTI')
	     then 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id in (mcp.ftd_origin_mapping))
	     then 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id in (pm.ftd_origin_mapping))
	     then 'OTH'
	     when cp.payment_type = 'IN'
	     then 'OTH'
	     when cp.payment_type = 'MS'
	     then 'OTH'
	     when (cp.payment_type = 'PP')
	     then 'OTH'
	     when (cp.payment_type = 'BM')
	     then 'OTH'
	     when (cp.payment_type = 'UA')
	     then 'OTH'
	END  bill_group,
	Case When (Cp.Payment_Type = 'AX' Or (Cp.Payment_Type = 'PC' And Cc_Type = 'AX'))
	     then 'American Express'
	     When Cp.Payment_Type = 'DC'
	     then 'Diners Club'
	     When (Cp.Payment_Type In ('DI', 'MC', 'VI', 'CB') Or (Cp.Payment_Type = 'PC' And Cc_Type <> 'AX'))
	     then 'Bank Cards'
	     When Cp.Payment_Type = 'GD'
	     then 'Gift Card'
	     When Cp.Payment_Type = 'GC'
	     THEN 'Gift Certificates'
	     when cp.payment_type = 'IN' and  co.origin_id = 'AMZNI'
	     then 'Amazon'
	     when cp.payment_type = 'IN' and  co.origin_id = 'WLMTI'
	     then 'Walmart'
	     when cp.payment_type = 'IN' and  co.origin_id in (mcp.ftd_origin_mapping)
	     then mcp.mailbox_name
	     when cp.payment_type = 'IN' and  co.origin_id in (pm.ftd_origin_mapping)
	     then pm.partner_name
	     When Cp.Payment_Type = 'IN'
	     then 'Invoiced Orders'
	     when cp.payment_type = 'MS'
	     then 'Military Star (AAFES)'
	     When Cp.Payment_Type = 'PP'
	     then 'PayPal'
	     When Cp.Payment_Type = 'BM'
	     then 'Bill Me Later'
	     when cp.payment_type = 'UA'
	     then 'United Mileage Plus'
	     else 'Unknown Bill Type'
	END  bill_type,
	sum(CASE when co.company_id='FTD'
		 then cp.debit_amount*-1
		 else 0
	    	END) ftd_amt,
	sum(CASE when co.company_id='GIFT'
		 then cp.debit_amount*-1
		 else 0
	    	END) gift_amt,
	sum(CASE when co.company_id='FTDCA'
		 then cp.debit_amount*-1
		 else 0
	    	END) ftdca_amt,
	sum(CASE when co.company_id='FUSA'
		 then cp.debit_amount*-1
		 else 0
	    	END) fusa_amt,
	sum(CASE when co.company_id='FLORIST'
		 then cp.debit_amount*-1
		 else 0
	    	END) flor_amt,
	sum(CASE when co.company_id='FDIRECT'
		 then cp.debit_amount*-1
		else 0
	    	END) fdir_amt,
    sum(CASE when co.company_id='ROSES'
		 then cp.debit_amount*-1
		else 0
	    	END) roses_amt,
    sum(CASE when co.company_id='ProFlowers'
		 then cp.debit_amount*-1
		else 0
	    	END) pro_amt
	--
	FROM		clean.orders co
	JOIN		clean.payments cp
				on (cp.order_guid = co.order_guid)
	JOIN		clean.order_details cod
				on (cod.order_guid = co.order_guid)
	JOIN		clean.refund cr
				on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cp.refund_id)
	LEFT OUTER JOIN	clean.credit_cards ccc
				on (ccc.cc_id = cp.cc_id)
	Left Outer Join Ptn_Mercent.Mrcnt_Channel_Mapping Mcp
	on  MCP.FTD_ORIGIN_MAPPING = co.origin_id
	-- Left outer Join ptn_pi.partner_mapping pm
	-- CR 16030 : Exclude individual Ariba partners records from the report and add their billing amount into 'Invoiced Orders' section.
	Left outer Join (select * from ptn_pi.partner_mapping where ftd_origin_mapping <> 'ARI') pm
	on pm.ftd_origin_mapping = co.origin_id
	--
	WHERE		cr.refund_status = 'Billed'
	AND		cr.refund_disp_code <> 'E10'
	AND		cp.payment_indicator='R'
	AND		CP.PAYMENT_TYPE <> 'NC'
	AND		trunc(cr.refund_date) between v_start_date and v_end_date
	AND		(p_company_code = 'ALL' or co.company_id in (SELECT * FROM TABLE(rpt.string_convert(p_company_code))))
	GROUP BY
		CASE when (cp.payment_type = 'AX' or (cp.payment_type = 'PC' and cc_type = 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'DC'
	     then 'JDE'
	     when (cp.payment_type in ('DI', 'MC', 'VI', 'CB') or (cp.payment_type = 'PC' and cc_type <> 'AX'))
	     then 'JDE'
	     when cp.payment_type = 'GD'
	     then 'OTH'
	     when cp.payment_type = 'GC'
	     THEN 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id = 'AMZNI')
	     then 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id = 'WLMTI')
	     then 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id in (mcp.ftd_origin_mapping))
	     then 'OTH'
	     when (cp.payment_type = 'IN' and co.origin_id in (pm.ftd_origin_mapping))
	     then 'OTH'
	     when cp.payment_type = 'IN'
	     then 'OTH'
	     when cp.payment_type = 'MS'
	     then 'OTH'
	     when (cp.payment_type = 'PP')
	     then 'OTH'
	     when (cp.payment_type = 'BM')
	     then 'OTH'
	     when (cp.payment_type = 'UA')
	     then 'OTH'
	END ,
	Case When (Cp.Payment_Type = 'AX' Or (Cp.Payment_Type = 'PC' And Cc_Type = 'AX'))
	     then 'American Express'
	     When Cp.Payment_Type = 'DC'
	     then 'Diners Club'
	     When (Cp.Payment_Type In ('DI', 'MC', 'VI', 'CB') Or (Cp.Payment_Type = 'PC' And Cc_Type <> 'AX'))
	     then 'Bank Cards'
	     When Cp.Payment_Type = 'GD'
	     then 'Gift Card'
	     When Cp.Payment_Type = 'GC'
	     THEN 'Gift Certificates'
	     when cp.payment_type = 'IN' and  co.origin_id = 'AMZNI'
	     then 'Amazon'
	     when cp.payment_type = 'IN' and  co.origin_id = 'WLMTI'
	     then 'Walmart'
	     when cp.payment_type = 'IN' and  co.origin_id in (mcp.ftd_origin_mapping)
	     then mcp.mailbox_name
	     when cp.payment_type = 'IN' and  co.origin_id in (pm.ftd_origin_mapping)
	     then pm.partner_name
	     When Cp.Payment_Type = 'IN'
	     then 'Invoiced Orders'
	     when cp.payment_type = 'MS'
	     then 'Military Star (AAFES)'
	     When Cp.Payment_Type = 'PP'
	     then 'PayPal'
	     When Cp.Payment_Type = 'BM'
	     then 'Bill Me Later'
	     when cp.payment_type = 'UA'
	     then 'United Mileage Plus'
	     else 'Unknown Bill Type'
	END
      UNION ALL
	SELECT	'JDE',
		'American Express',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'JDE',
		'Diners Club',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'JDE',
		'Bank Cards',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Amazon',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Gift Card',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Gift Certificates',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Invoiced Orders',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Military Star (AAFES)',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Walmart',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'PayPal',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'Bill Me Later',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	SELECT	'OTH',
		'United Mileage Plus',
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM DUAL
        UNION ALL
	Select	'OTH',
		m.mailbox_name,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM ptn_mercent.mrcnt_channel_mapping m
        UNION ALL
	Select	'OTH',
		pm.partner_name,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
    0
	FROM ptn_pi.partner_mapping pm
	-- CR 16030 : Exclude individual Ariba partners records from the report and add their billing amount into 'Invoiced Orders' section.
	where pm.ftd_origin_mapping <> 'ARI'
)
GROUP BY
	bill_group,
	bill_type
ORDER BY
	BILL_TYPE
;
END
;
.
/
