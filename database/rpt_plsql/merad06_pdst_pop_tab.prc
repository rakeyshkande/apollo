CREATE OR REPLACE
PROCEDURE rpt.merad06_pdst_pop_tab

(p_start_date in varchar2,
 p_end_date in varchar2,
 p_origin_code in varchar2 DEFAULT NULL,
 p_target_only in varchar2)

IS
	v_total_revenue number;
	v_gift_revenue number;
	v_high_revenue number;
	v_florist_revenue number;
	v_fusa_revenue number;
	v_fdirect_revenue number;
	v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
	v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
   v_origin_predicate	VARCHAR2(1000);
   v_origin_predicate1	VARCHAR2(1000);
   v_sql			VARCHAR2(32000);

begin

  IF p_origin_code <>'ALL' THEN
  	v_origin_predicate := '	AND (( ''' || p_origin_code || ''' IS NOT NULL AND fao.origin_id in ('|| rpt.multi_value(p_origin_code)||') ) ' ||
  	 ' OR ( ''' || p_origin_code || ''' IS NULL AND ''' || p_target_only || ''' = ''Y'' AND fao.origin_id = ''TARGI'') ' || 
   	 ' OR ( ''' || p_origin_code || ''' IS NULL AND ''' || p_target_only || ''' = ''N'')) ';   
  	v_origin_predicate1 := '	AND (( ''' || p_origin_code || ''' IS NOT NULL AND co.origin_id in ('|| rpt.multi_value(p_origin_code)||') ) ' ||
  	 ' OR ( ''' || p_origin_code || ''' IS NULL AND ''' || p_target_only || ''' = ''Y'' AND co.origin_id = ''TARGI'') ' || 
   	 ' OR ( ''' || p_origin_code || ''' IS NULL AND ''' || p_target_only || ''' = ''N'')) '; 
  END IF;

rpt.gen07_category_pop_tab(p_start_date,p_end_date, p_origin_code, p_target_only);

v_sql :=
'insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25,
			col26,
			col27,
			col28,
			col29,
			col30,
			col31,
			col32,
			col33,
			col34,
			col35,
			col36,
			col37,
			col38,
			col39,
			col40,
			col41,
			col42,
			col43,
			col44,
			col45,
			col46,
			col47,
			col48,
			col49,
			col50,
			col51,
			col52,
			col53,
			col54,
			col55,
			col56,
			col57,
			col58,
			col59,
			col60,
			col61,
			col62)
select 	''1'',
	product_id,
          	product_description,
          	vendor_name,
	sum(one) one,
	sum(two) two,
	sum(three) three,
	sum(four) four,
	sum(five) five,
	sum(six) six,
	sum(seven) seven,
	sum(eight) eight,
	sum(nine) nine,
	sum(ten) ten,
	sum(eleven) eleven,
	sum(twelve) twelve,
	sum(thirteen) thirteen,
	sum(fourteen) fourteen,
	sum(fifteen) fifteen,
	sum(sixteen) sixteen,
	sum(seventeen) seventeen,
	sum(eighteen) eighteen,
	sum(nineteen) nineteen,
	sum(twenty) twenty,
	sum(twentyone) twentyone,
	sum(twentytwo) twentytwo,
	sum(twentythree) twentythree,
	sum(twentyfour) twentyfour,
	sum(twentyfive) twentyfive,
	sum(twentysix) twentysix,
	sum(twentyseven) twentyseven,
	sum(twentyeight) twentyeight,
	sum(twentynine) twentynine,
	sum(thirty) thirty,
	sum(thirtyone) thirtyone,
        sum(order_total) order_total,
        sum(phone) phone,
        sum(internet) internet,
        category,
        retail_unit_price,
        sum(total_revenue) total_revenue,
        sum(giftsense_total) giftsense_total,
        sum(giftsense_phone) giftsense_phone,
        sum(giftsense_internet) giftsense_internet,
        sum(giftsense_revenue) giftsense_revenue,
        sum(butterfield_total) butterfield_total,
        sum(butterfield_phone) butterfield_phone,
        sum(butterfield_internet) butterfield_internet,
        sum(butterfield_revenue) butterfield_revenue,
        sum(floristcom_total) floristcom_total,
        sum(floristcom_phone) floristcom_phone,
        sum(floristcom_internet) floristcom_internet,
        sum(floristcom_revenue) floristcom_revenue,
        sum(flowersusa_total) floristusa_total,
        sum(flowersusa_phone) floristusa_phone,
        sum(flowersusa_internet) floristusa_internet,
        sum(flowersusa_revenue) floristusa_revenue,
        sum(flowersdirect_total) flowersdirect_total,
        sum(flowersdirect_phone) flowersdirect_phone,
        sum(flowersdirect_internet) flowersdirect_internet,
        sum(flowersdirect_revenue) flowersdirect_revenue,
        department_code
from
(select 	cod.product_id product_id,
             	fapm.product_name product_description,
             	decode(nvl(cod.ship_method,''SD''),''SD'','' '',favm.vendor_name) vendor_name,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''01'',1,null)
                                          ELSE NULL
                  END)  one,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''02'',1,null)
                                          ELSE NULL
                  END)  two,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''03'',1,null)
                                          ELSE NULL
                  END)  three,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''04'',1,null)
                                          ELSE NULL
                  END) four,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''05'',1,null)
                                          ELSE NULL
                  END) five,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''06'',1,null)
                                          ELSE NULL
                  END) six,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''07'',1,null)
                                          ELSE NULL
                  END)  seven,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''08'',1,null)
                                          ELSE NULL
                  END) eight,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''09'',1,null)
                                          ELSE NULL
                  END) nine,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''10'',1,null)
                                          ELSE NULL
                  END)  ten,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''11'',1,null)
                                          ELSE NULL
                  END)  eleven,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''12'',1,null)
                                          ELSE NULL
                  END) twelve,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''13'',1,null)
                                          ELSE NULL
                  END) thirteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''14'',1,null)
                                          ELSE NULL
                  END) fourteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''15'',1,null)
                                          ELSE NULL
                  END) fifteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''16'',1,null)
                                          ELSE NULL
                  END) sixteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''17'',1,null)
                                          ELSE NULL
                  END) seventeen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''18'',1,null)
                                          ELSE NULL
                  END) eighteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''19'',1,null)
                                          ELSE NULL
                  END) nineteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''20'',1,null)
                                          ELSE NULL
                  END) twenty,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''21'',1,null)
                                          ELSE NULL
                  END) twentyone,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''22'',1,null)
                                          ELSE NULL
                  END) twentytwo,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''23'',1,null)
                                          ELSE NULL
                  END) twentythree,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''24'',1,null)
                                          ELSE NULL
                  END) twentyfour,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''25'',1,null)
                                          ELSE NULL
                  END) twentyfive,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''26'',1,null)
                                          ELSE NULL
                  END) twentysix,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''27'',1,null)
                                          ELSE NULL
                  END) twentyseven,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''28'',1,null)
                                          ELSE NULL
                  END) twentyeight,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''29'',1,null)
                                          ELSE NULL
                  END) twentynine,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''30'',1,null)
                                          ELSE NULL
                  END) thirty,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''31'',1,null)
                                          ELSE NULL
                  END) thirtyone,
	count(CASE WHEN cat.transaction_type = ''Order''
                   THEN 1
                   ELSE NULL
              END) order_total,
	sum(CASE WHEN cat.transaction_type = ''Order''
                   THEN decode(fao.origin_type,''phone'',1,0)
                   ELSE NULL
              END) phone,
	sum(CASE WHEN cat.transaction_type = ''Order''
                   THEN decode(fao.origin_type,''internet'',1,0)
                   ELSE NULL
              END) internet,
	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	      when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                    when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
	fapm.standard_price retail_unit_price,
	count(co.order_guid) * fapm.standard_price total_revenue,
	sum(decode(co.company_id,''GIFT'',1,0)) giftsense_total,
	sum(decode(co.company_id,''GIFT'',decode(fao.origin_type,''phone'',1,0),0)) giftsense_phone,
   sum(decode(co.company_id,''GIFT'',decode(fao.origin_type,''internet'',1,0),0)) giftsense_internet,
	(sum(decode(co.company_id,''GIFT'',1,0)) *  fapm.standard_price) giftsense_revenue,
	sum(decode(co.company_id,''HIGH'',1,0)) butterfield_total,
	sum(decode(co.company_id,''HIGH'',decode(fao.origin_type,''phone'',1,0),0)) butterfield_phone,
	sum(decode(co.company_id,''HIGH'',decode(fao.origin_type,''internet'',1,0),0)) butterfield_internet,
	(sum(decode(co.company_id,''HIGH'',1,0)) *  fapm.standard_price) butterfield_revenue,
	sum(decode(co.company_id,''FLORIST'',1,0)) floristcom_total,
	sum(decode(co.company_id,''FLORIST'',decode(fao.origin_type,''phone'',1,0),0)) floristcom_phone,
	sum(decode(co.company_id,''FLORIST'',decode(fao.origin_type,''internet'',1,0),0)) floristcom_internet,
   (sum(decode(co.company_id,''FLORIST'',1,0)) *  fapm.standard_price) floristcom_revenue,
   sum(decode(co.company_id,''FUSA'',1,0)) flowersusa_total,
   sum(decode(co.company_id,''FUSA'',decode(fao.origin_type,''phone'',1,0),0)) flowersusa_phone,
   sum(decode(co.company_id,''FUSA'',decode(fao.origin_type,''internet'',1,0),0)) flowersusa_internet,
	(sum(decode(co.company_id,''FUSA'',1,0)) *  fapm.standard_price) flowersusa_revenue,
	sum(decode(co.company_id,''FDIRECT'',1,0)) flowersdirect_total,
   sum(decode(co.company_id,''FDIRECT'',decode(fao.origin_type,''phone'',1,0),0)) flowersdirect_phone,
   sum(decode(co.company_id,''FDIRECT'',decode(fao.origin_type,''internet'',1,0),0)) flowersdirect_internet,
 	(sum(decode(co.company_id,''FDIRECT'',1,0)) *  fapm.standard_price) flowersdirect_revenue,
   fapm.department_code department_code
FROM clean.order_details cod
JOIN clean.orders co                   ON (co.order_guid=cod.order_guid)
JOIN ftd_apps.origins fao              ON (co.origin_id = fao.origin_id)
JOIN clean.accounting_transactions cat ON (cat.order_detail_id = cod.order_detail_id)
JOIN ftd_apps.product_master fapm      ON (cod.product_id=fapm.product_id)
LEFT OUTER JOIN ftd_apps.codified_products facp   ON (fapm.product_id=facp.product_id)
LEFT OUTER JOIN ftd_apps.codification_master facm ON (facp.codification_id=facm.codification_id)
LEFT OUTER JOIN ftd_apps.vendor_master favm       ON (cod.vendor_id=favm.vendor_id)
WHERE cod.subcode is null
AND trunc(cat.transaction_date) between '''|| v_start_date||''' AND '''|| v_end_date||''''
||v_origin_predicate
||
' AND cod.order_disp_code not in (''Held'',''In-Scrub'',''Pending'',''Removed'')
AND cod.eod_delivery_indicator <> ''X''
AND cat.transaction_type <> ''Refund''
AND co.origin_id <>''TEST''
group by fapm.department_code,
        	cod.product_id,
             	fapm.product_name,
	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	      when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                    when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
	decode(nvl(cod.ship_method,''SD''),''SD'','' '',favm.vendor_name),
            	fapm.standard_price
union all
select 	cod.subcode product_id,
             	fapm.product_name||'' - ''||faps.subcode_description product_description,
             	decode(nvl(cod.ship_method,''SD''),''SD'','' '',favm.vendor_name) vendor_name,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''01'',1,null)
                                          ELSE NULL
                  END)  one,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''02'',1,null)
                                          ELSE NULL
                  END)  two,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''03'',1,null)
                                          ELSE NULL
                  END)  three,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''04'',1,null)
                                          ELSE NULL
                  END) four,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''05'',1,null)
                                          ELSE NULL
                  END) five,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''06'',1,null)
                                          ELSE NULL
                  END) six,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''07'',1,null)
                                          ELSE NULL
                  END)  seven,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''08'',1,null)
                                          ELSE NULL
                  END) eight,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''09'',1,null)
                                          ELSE NULL
                  END) nine,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''10'',1,null)
                                          ELSE NULL
                  END)  ten,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''11'',1,null)
                                          ELSE NULL
                  END)  eleven,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''12'',1,null)
                                          ELSE NULL
                  END) twelve,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''13'',1,null)
                                          ELSE NULL
                  END) thirteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''14'',1,null)
                                          ELSE NULL
                  END) fourteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''15'',1,null)
                                          ELSE NULL
                  END) fifteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''16'',1,null)
                                          ELSE NULL
                  END) sixteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''17'',1,null)
                                          ELSE NULL
                  END) seventeen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''18'',1,null)
                                          ELSE NULL
                  END) eighteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''19'',1,null)
                                          ELSE NULL
                  END) nineteen,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''20'',1,null)
                                          ELSE NULL
                  END) twenty,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''21'',1,null)
                                          ELSE NULL
                  END) twentyone,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''22'',1,null)
                                          ELSE NULL
                  END) twentytwo,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''23'',1,null)
                                          ELSE NULL
                  END) twentythree,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''24'',1,null)
                                          ELSE NULL
                  END) twentyfour,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''25'',1,null)
                                          ELSE NULL
                  END) twentyfive,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''26'',1,null)
                                          ELSE NULL
                  END) twentysix,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''27'',1,null)
                                          ELSE NULL
                  END) twentyseven,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''28'',1,null)
                                          ELSE NULL
                  END) twentyeight,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''29'',1,null)
                                          ELSE NULL
                  END) twentynine,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''30'',1,null)
                                          ELSE NULL
                  END) thirty,
	count(CASE WHEN cat.transaction_type = ''Order''
                                          THEN decode(to_char(trunc(co.order_date),''dd''),''31'',1,null)
                                          ELSE NULL
                  END) thirtyone,
	count(CASE WHEN cat.transaction_type = ''Order''
                   THEN 1
                   ELSE NULL
              END) order_total,
	sum(CASE WHEN cat.transaction_type = ''Order''
                   THEN decode(fao.origin_type,''phone'',1,0)
                   ELSE NULL
              END) phone,
	sum(CASE WHEN cat.transaction_type = ''Order''
                   THEN decode(fao.origin_type,''internet'',1,0)
                   ELSE NULL
              END) internet,
	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                    when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end category,
	fapm.standard_price retail_unit_price,
	count(co.order_guid) * fapm.standard_price total_revenue,
	sum(decode(co.company_id,''GIFT'',1,0)) giftsense_total,
	sum(decode(co.company_id,''GIFT'',decode(fao.origin_type,''phone'',1,0),0)) giftsense_phone,
            	sum(decode(co.company_id,''GIFT'',decode(fao.origin_type,''internet'',1,0),0)) giftsense_internet,
	(sum(decode(co.company_id,''GIFT'',1,0)) *  fapm.standard_price) giftsense_revenue,
	sum(decode(co.company_id,''HIGH'',1,0)) butterfield_total,
	sum(decode(co.company_id,''HIGH'',decode(fao.origin_type,''phone'',1,0),0)) butterfield_phone,
	sum(decode(co.company_id,''HIGH'',decode(fao.origin_type,''internet'',1,0),0)) butterfield_internet,
	(sum(decode(co.company_id,''HIGH'',1,0)) *  fapm.standard_price) butterfield_revenue,
	sum(decode(co.company_id,''FLORIST'',1,0)) floristcom_total,
	sum(decode(co.company_id,''FLORIST'',decode(fao.origin_type,''phone'',1,0),0)) floristcom_phone,
	sum(decode(co.company_id,''FLORIST'',decode(fao.origin_type,''internet'',1,0),0)) floristcom_internet,
            	(sum(decode(co.company_id,''FLORIST'',1,0)) *  fapm.standard_price) floristcom_revenue,
            	sum(decode(co.company_id,''FUSA'',1,0)) flowersusa_total,
            	sum(decode(co.company_id,''FUSA'',decode(fao.origin_type,''phone'',1,0),0)) flowersusa_phone,
            	sum(decode(co.company_id,''FUSA'',decode(fao.origin_type,''internet'',1,0),0)) flowersusa_internet,
	(sum(decode(co.company_id,''FUSA'',1,0)) *  fapm.standard_price) flowersusa_revenue,
	sum(decode(co.company_id,''FDIRECT'',1,0)) flowersdirect_total,
            	sum(decode(co.company_id,''FDIRECT'',decode(fao.origin_type,''phone'',1,0),0)) flowersdirect_phone,
            	sum(decode(co.company_id,''FDIRECT'',decode(fao.origin_type,''internet'',1,0),0)) flowersdirect_internet,
 	(sum(decode(co.company_id,''FDIRECT'',1,0)) *  fapm.standard_price) flowersdirect_revenue,
   fapm.department_code department_code
FROM clean.order_details cod
JOIN clean.orders co                   ON (co.order_guid=cod.order_guid)
JOIN ftd_apps.origins fao              ON (co.origin_id = fao.origin_id)
JOIN clean.accounting_transactions cat ON (cat.order_detail_id = cod.order_detail_id)
JOIN ftd_apps.product_master fapm      ON (cod.product_id=fapm.product_id)
LEFT OUTER JOIN ftd_apps.codified_products facp   ON (fapm.product_id=facp.product_id)
LEFT OUTER JOIN ftd_apps.codification_master facm ON (facp.codification_id=facm.codification_id)
LEFT OUTER JOIN ftd_apps.vendor_master favm       ON (cod.vendor_id=favm.vendor_id)
JOIN ftd_apps.product_subcodes faps    ON (cod.subcode=faps.product_subcode_id)
WHERE cod.subcode is not null
AND trunc(cat.transaction_date) between '''|| v_start_date||''' AND '''|| v_end_date||''''
||v_origin_predicate
||
' AND cod.order_disp_code not in (''Held'',''In-Scrub'',''Pending'',''Removed'')
AND cod.eod_delivery_indicator <> ''X''
AND cat.transaction_type <> ''Refund''
AND co.origin_id <>''TEST''
group by fapm.department_code,
			cod.subcode,
             	fapm.product_name||'' - ''||faps.subcode_description,
	case when (facp.codification_id in (''VPR'',''VRB''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''ROS''
		else	fapm.category
	end
	        when   (facp.codification_id in (''VPT''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
		then	''GEN''
		else	fapm.category
	end
	      when   (facp.codification_id in (''VPF''))
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''GEN''
							else	fapm.category
			end
	       when (facm.category_id=''Holiday'')
		then 	case nvl(cod.ship_method,''SD'') when ''SD''
							then 	''CDH''
							else	fapm.category
			end
                     when (fapm.product_type in (''SDG'',''SDFC''))
		then	case nvl(cod.ship_method,''SD'') when ''SD''
							then	''CSD''
							else	fapm.category
			end
		else	fapm.category
	end,
	decode(nvl(cod.ship_method,''SD''),''SD'','' '',favm.vendor_name),
	fapm.standard_price)
group by 	''1'',
   department_code,
	product_id,
   product_description,
   category,
   vendor_name,
   retail_unit_price';
EXECUTE IMMEDIATE v_sql;   

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25,
			col26,
			col27,
			col28,
			col29,
			col30,
			col31,
			col32,
			col33,
			col34,
			col35,
			col36)
select	row_name,
	product_id,
   product_description,
   vendor_name,
	sum(one) one,
	sum(two) two,
	sum(three) three,
	sum(four) four,
	sum(five) five,
	sum(six) six,
	sum(seven) seven,
	sum(eight) eight,
	sum(nine) nine,
	sum(ten) ten,
	sum(eleven) eleven,
	sum(twelve) twelve,
	sum(thirteen) thirteen,
	sum(fourteen) fourteen,
	sum(fifteen) fifteen,
	sum(sixteen) sixteen,
	sum(seventeen) seventeen,
	sum(eighteen) eighteen,
	sum(nineteen) nineteen,
	sum(twenty) twenty,
	sum(twentyone) twentyone,
	sum(twentytwo) twentytwo,
	sum(twentythree) twentythree,
	sum(twentyfour) twentyfour,
	sum(twentyfive) twentyfive,
	sum(twentysix) twentysix,
	sum(twentyseven) twentyseven,
	sum(twentyeight) twentyeight,
	sum(twentynine) twentynine,
	sum(thirty) thirty,
	sum(thirtyone) thirtyone,
   department_code
from
(select 	'2' row_name,
	' ' product_id,
	' ' product_description,
	' ' vendor_name,
	sum(decode(rct.col1,'01',decode(rct.col5,'DS',1,0))) one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DS',1,0))) two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DS',1,0))) three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DS',1,0))) four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DS',1,0))) five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DS',1,0))) six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DS',1,0))) seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DS',1,0))) eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DS',1,0))) nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DS',1,0))) ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DS',1,0))) eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DS',1,0))) twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DS',1,0))) thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DS',1,0))) fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DS',1,0))) fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DS',1,0))) sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DS',1,0))) seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DS',1,0))) eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DS',1,0))) nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DS',1,0))) twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DS',1,0))) twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DS',1,0))) twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DS',1,0))) twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DS',1,0))) twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DS',1,0))) twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DS',1,0))) twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DS',1,0))) twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DS',1,0))) twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DS',1,0))) twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DS',1,0))) thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DS',1,0))) thirtyone,
	' ' department_code
from 	rpt.category_tab rct
group by	'2',
	' ',
	' ',
	' ',
	' '
union all
select 	'3',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'01',1))*100 one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'02',1))*100 two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'03',1))*100 three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'04',1))*100 four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'05',1))*100 five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'06',1))*100 six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'07',1))*100 seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'08',1))*100 eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'09',1))*100 nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'10',1))*100 ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'11',1))*100 eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'12',1))*100 twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'13',1))*100 thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'14',1))*100 fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'15',1))*100 fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'16',1))*100 sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'17',1))*100 seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'18',1))*100 eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'19',1))*100 nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'20',1))*100 twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'21',1))*100 twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'22',1))*100 twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'23',1))*100 twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'24',1))*100 twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'25',1))*100 twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'26',1))*100 twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'27',1))*100 twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'28',1))*100 twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'29',1))*100 twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'30',1))*100 thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DS',1,0)))/sum(decode(rct.col1,'31',1))*100 thirtyone,
	' '	
from 	rpt.category_tab rct
group by	'3',
	' ',
	' ',
	' ',
	' '
union all
select 	'4',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DSP',1,0))) one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DSP',1,0))) two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DSP',1,0))) three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DSP',1,0))) four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DSP',1,0))) five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DSP',1,0))) six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DSP',1,0))) seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DSP',1,0))) eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DSP',1,0))) nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DSP',1,0))) ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DSP',1,0))) eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DSP',1,0))) twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DSP',1,0))) thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DSP',1,0))) fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DSP',1,0))) fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DSP',1,0))) sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DSP',1,0))) seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DSP',1,0))) eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DSP',1,0))) nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DSP',1,0))) twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DSP',1,0))) twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DSP',1,0))) twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DSP',1,0))) twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DSP',1,0))) twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DSP',1,0))) twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DSP',1,0))) twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DSP',1,0))) twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DSP',1,0))) twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DSP',1,0))) twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DSP',1,0))) thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DSP',1,0))) thirtyone,
	' '
from 	rpt.category_tab rct
group by	'4',
	' ',
	' ',
	' ',
	' '
union all
select 	'5',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'01',1))*100 one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'02',1))*100 two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'03',1))*100 three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'04',1))*100 four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'05',1))*100 five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'06',1))*100 six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'07',1))*100 seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'08',1))*100 eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'09',1))*100 nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'10',1))*100 ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'11',1))*100 eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'12',1))*100 twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'13',1))*100 thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'14',1))*100 fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'15',1))*100 fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'16',1))*100 sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'17',1))*100 seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'18',1))*100 eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'19',1))*100 nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'20',1))*100 twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'21',1))*100 twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'22',1))*100 twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'23',1))*100 twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'24',1))*100 twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'25',1))*100 twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'26',1))*100 twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'27',1))*100 twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'28',1))*100 twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'29',1))*100 twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'30',1))*100 thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DSP',1,0)))/sum(decode(rct.col1,'31',1))*100 thirtyone,
	' '
from 	rpt.category_tab rct
group by	'5',
	' ',
	' ',
	' ',
	' '
union all
select 	'6',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DSF',1,0))) one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DSF',1,0))) two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DSF',1,0))) three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DSF',1,0))) four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DSF',1,0))) five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DSF',1,0))) six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DSF',1,0))) seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DSF',1,0))) eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DSF',1,0))) nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DSF',1,0))) ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DSF',1,0))) eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DSF',1,0))) twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DSF',1,0))) thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DSF',1,0))) fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DSF',1,0))) fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DSF',1,0))) sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DSF',1,0))) seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DSF',1,0))) eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DSF',1,0))) nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DSF',1,0))) twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DSF',1,0))) twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DSF',1,0))) twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DSF',1,0))) twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DSF',1,0))) twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DSF',1,0))) twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DSF',1,0))) twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DSF',1,0))) twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DSF',1,0))) twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DSF',1,0))) twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DSF',1,0))) thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DSF',1,0))) thirtyone,
	' '
from 	rpt.category_tab rct
group by	'6',
	' ',
	' ',
	' ',
	' '
union all
select 	'7',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'01',1))*100 one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'02',1))*100 two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'03',1))*100 three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'04',1))*100 four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'05',1))*100 five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'06',1))*100 six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'07',1))*100 seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'08',1))*100 eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'09',1))*100 nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'10',1))*100 ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'11',1))*100 eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'12',1))*100 twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'13',1))*100 thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'14',1))*100 fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'15',1))*100 fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'16',1))*100 sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'17',1))*100 seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'18',1))*100 eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'19',1))*100 nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'20',1))*100 twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'21',1))*100 twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'22',1))*100 twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'23',1))*100 twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'24',1))*100 twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'25',1))*100 twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'26',1))*100 twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'27',1))*100 twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'28',1))*100 twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'29',1))*100 twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'30',1))*100 thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DSF',1,0)))/sum(decode(rct.col1,'31',1))*100 thirtyone,
	' '
from 	rpt.category_tab rct
group by	'7',
	' ',
	' ',
	' ',
	' '
union all
select 	'8',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0))) thirtyone,
	' '
from 	rpt.category_tab rct
group by	'8',
	' ',
	' ',
	' ',
	' '
union all
select 	'9',
	' ',
	' ',
	' ',
	sum(decode(rct.col1,'01',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'01',1))*100 one,
	sum(decode(rct.col1,'02',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'02',1))*100 two,
	sum(decode(rct.col1,'03',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'03',1))*100 three,
	sum(decode(rct.col1,'04',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'04',1))*100 four,
	sum(decode(rct.col1,'05',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'05',1))*100 five,
	sum(decode(rct.col1,'06',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'06',1))*100 six,
	sum(decode(rct.col1,'07',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'07',1))*100 seven,
	sum(decode(rct.col1,'08',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'08',1))*100 eight,
	sum(decode(rct.col1,'09',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'09',1))*100 nine,
	sum(decode(rct.col1,'10',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'10',1))*100 ten,
	sum(decode(rct.col1,'11',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'11',1))*100 eleven,
	sum(decode(rct.col1,'12',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'12',1))*100 twelve,
	sum(decode(rct.col1,'13',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'13',1))*100 thirteen,
	sum(decode(rct.col1,'14',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'14',1))*100 fourteen,
	sum(decode(rct.col1,'15',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'15',1))*100 fifteen,
	sum(decode(rct.col1,'16',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'16',1))*100 sixteen,
	sum(decode(rct.col1,'17',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'17',1))*100 seventeen,
	sum(decode(rct.col1,'18',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'18',1))*100 eighteen,
	sum(decode(rct.col1,'19',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'19',1))*100 nineteen,
	sum(decode(rct.col1,'20',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'20',1))*100 twenty,
	sum(decode(rct.col1,'21',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'21',1))*100 twentyone,
	sum(decode(rct.col1,'22',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'22',1))*100 twentytwo,
	sum(decode(rct.col1,'23',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'23',1))*100 twentythree,
	sum(decode(rct.col1,'24',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'24',1))*100 twentyfour,
	sum(decode(rct.col1,'25',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'25',1))*100 twentyfive,
	sum(decode(rct.col1,'26',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'26',1))*100 twentysix,
	sum(decode(rct.col1,'27',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'27',1))*100 twentyseven,
	sum(decode(rct.col1,'28',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'28',1))*100 twentyeight,
	sum(decode(rct.col1,'29',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'29',1))*100 twentynine,
	sum(decode(rct.col1,'30',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'30',1))*100 thirty,
	sum(decode(rct.col1,'31',decode(rct.col5,'DS',1,'DSP',1,'DSF',1,0)))/sum(decode(rct.col1,'31',1))*100 thirtyone,
	' '
from 	rpt.category_tab rct
group by	'9',
	' ',
	' ',
	' ',
	' ')
group by row_name,
			department_code,
			product_id,
			product_description,
			vendor_name;			

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25)
select 	'11',
	nvl(sum(decode(rct.col5,'DS',1,0)),0) gift,
            	nvl((sum(decode(rct.col5,'DS',1))/count(rct.col2))*100,0) gift_pct,
            	nvl(sum(decode(rct.col5,'DSP',1,0)),0) plants,
            	nvl((sum(decode(rct.col5,'DSP',1))/count(rct.col2))*100,0) plt_pct,
            	nvl(sum(decode(rct.col5,'DSF',1,0)),0) dsf,
	nvl((sum(decode(rct.col5,'DSF',1))/count(rct.col2))*100,0) dsf_pct,
            	nvl(sum(decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0)),0) ttl_gift_plt_dsf,
 	nvl((sum(decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0))/count(rct.col2)) *100,0) ttl_gift_plt_dsf_pct,
	--
	nvl(sum(decode(rct.col3,'phone', decode(rct.col5,'DS',1,0),0)),0) gift_ph,
            	nvl((sum(decode(rct.col3,'phone', decode(rct.col5,'DS',1)))/sum(decode(rct.col3,'phone',1)))*100,0) gift_ph_pct,
            	nvl(sum(decode(rct.col3,'phone', decode(rct.col5,'DSP',1,0),0)),0) plt_ph,
           	nvl((sum(decode(rct.col3,'phone', decode(rct.col5,'DSP',1)))/sum(decode(rct.col3,'phone',1)))*100,0) plt_ph_pct,
            	nvl(sum(decode(rct.col3,'phone', decode(rct.col5,'DSF',1,0),0)),0) dsf_ph,
            	nvl((sum(decode(rct.col3,'phone', decode(rct.col5,'DSF',1)))/sum(decode(rct.col3,'phone',1)))*100,0) dsf_ph_pct,
            	nvl(sum(decode(rct.col3,'phone', decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) tot_gift_plt_dsf_ph,
            	nvl((sum(decode(rct.col3,'phone', decode(rct.col5,'DS',1, 'DSP',1,'DSF',1)))/sum(decode(rct.col3,'phone',1)))*100,0) ttl_gift_plt_dsf_ph_pct,
	--
	nvl(sum(decode(rct.col3,'internet', decode(rct.col5,'DS',1,0),0)),0) gift_inet,
            	nvl((sum(decode(rct.col3,'internet', decode(rct.col5,'DS',1)))/sum(decode(rct.col3,'internet',1)))*100,0) gift_inet_pct,
            	nvl(sum(decode(rct.col3,'internet', decode(rct.col5,'DSP',1,0),0)),0) plt_inet,
            	nvl((sum(decode(rct.col3,'internet', decode(rct.col5,'DSP',1)))/sum(decode(rct.col3,'internet',1)))*100,0) plt_inet_pct,
            	nvl(sum(decode(rct.col3,'internet', decode(rct.col5,'DSF',1,0),0)),0) dsf_inet,
            	nvl((sum(decode(rct.col3,'internet', decode(rct.col5,'DSF',1)))/sum(decode(rct.col3,'internet',1)))*100,0) dsf_inet_pct,
            	nvl(sum(decode(rct.col3,'internet', decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) ttl_gift_plt_dsf_inet,
            	nvl((sum(decode(rct.col3,'internet',decode(rct.col5,'DS',1,'DSP',1,'DSF',1)))/sum(decode(rct.col3,'internet',1)))*100,0)  ttl_gift_plt_dsf_inet_pct
from 	rpt.category_tab rct;

insert into rpt.rep_tab	(col1,
			col26,
			col28,
			col30,
			col32)
select 	'11',
	decode(rct.col2,'DS',rct.col3) gift_ttl_rev,
      	decode(rct.col2,'DSP',rct.col3) plt_ttl_rev,
      	decode(rct.col2,'DSF',rct.col3) dsf_ttl_rev,
      	sum(rct.col3)  gift_plt_dsf_ttl_rev
from 	rpt.category_tab rct
where rct.col1='99'
group by decode(rct.col2,'DS',rct.col3),
      	decode(rct.col2,'DSP',rct.col3),
      	decode(rct.col2,'DSF',rct.col3);

select 	sum(rct.col3) total_revenue
into 	v_total_revenue
from	rpt.category_tab rct
where rct.col1='98';

select 	sum(rct.col3) total_revenue
into 	v_gift_revenue
from	rpt.category_tab rct
where rct.col2='GIFT'
and rct.col1='98';

select 	sum(rct.col3) total_revenue
into 	v_high_revenue
from	rpt.category_tab rct
where rct.col2='HIGH'
and rct.col1='98';

select 	sum(rct.col3) total_revenue
into 	v_florist_revenue
from	rpt.category_tab rct
where rct.col2='FLORIST'
and rct.col1='98';

select 	sum(rct.col3) total_revenue
into 	v_fusa_revenue
from	rpt.category_tab rct
where rct.col2='FUSA'
and rct.col1='98';

select 	sum(rct.col3) total_revenue
into 	v_fdirect_revenue
from	rpt.category_tab rct
where rct.col2='FDIRECT'
and rct.col1='98';

insert into rpt.rep_tab	(col1,
			col27,
			col29,
			col31,
			col33)
select 	'11',
	(decode(rct.col2,'DS',rct.col3)/v_total_revenue)*100 gift_pct_ttl_rev,
      	(decode(rct.col2,'DSP',rct.col3)/v_total_revenue)*100 plt_pct_ttl_rev,
            	(decode(rct.col2,'DSF',rct.col3)/v_total_revenue)*100 dsf_pct_ttl_rev,
            	(sum(rct.col8)/v_total_revenue)*100 gift_plt_dsf_pct_ttl_rev
from 	rpt.category_tab rct
where rct.col1='99'
group by '11',
	(decode(rct.col2,'DS',rct.col3)/v_total_revenue)*100,
      	(decode(rct.col2,'DSP',rct.col3)/v_total_revenue)*100,
            	(decode(rct.col2,'DSF',rct.col3)/v_total_revenue)*100;

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25)
select 	'12',
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col5,'DS',1,0),0)),0) gift,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col5,'DS',1)))/sum(decode(rct.col4,'GIFT',1,0)))*100,0) gift_pct,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col5,'DSP',1,0),0)),0) plants,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col5,'DSP',1)))/sum(decode(rct.col4,'GIFT',1,0)))*100,0) plt_pct,
            	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col5,'DSF',1,0),0)),0) dsf,
	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col5,'DSF',1)))/sum(decode(rct.col4,'GIFT',1,0)))*100,0) dsf_pct,
            	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) ttl_gift_plt_dsf,
 	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0)))/sum(decode(rct.col4,'GIFT',1,0)))*100,0) ttl_gift_plt_dsf_pct,
     	 --
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_ph,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone',1))))*100,0) gift_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone',1))))*100,0) plt_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_ph,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone',1))))*100,0) dsf_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'phone',1))))*100,0) gift_plt_dsf_giftp_pct_ph,
     	--
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_inet,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet',1))))*100,0) gift_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet',1))))*100,0) plt_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_inet,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet',1))))*100,0) dsf_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'GIFT',decode(rct.col3,'internet',1))))*100,0) gift_plt_dsf_giftp_pct_inet
     	--
from 	rpt.category_tab rct;

insert into rpt.rep_tab	(col1,
			col26,
			col28,
			col30,
			col32)
select 	'12',
	decode(rct.col2,'DS',rct.col3) gift_ttl_rev,
      	decode(rct.col2,'DSP',rct.col3) plt_ttl_rev,
      	decode(rct.col2,'DSF',rct.col3) dsf_ttl_rev,
      	sum(rct.col3)  gift_plt_dsf_ttl_rev
from 	rpt.category_tab rct
where rct.col1='97'
group by decode(rct.col2,'DS',rct.col3),
      	decode(rct.col2,'DSP',rct.col3),
      	decode(rct.col2,'DSF',rct.col3);

insert into rpt.rep_tab	(col1,
			col27,
			col29,
			col31,
			col33)
select 	'12',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100 gift_pct_ttl_rev,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100 plt_pct_ttl_rev,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100 dsf_pct_ttl_rev,
            	(sum(rct.col3)/v_gift_revenue)*100 gift_plt_dsf_pct_ttl_rev
from 	rpt.category_tab rct
where rct.col1='97'
group by '12',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100;

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25)
select 	'13',
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col5,'DS',1,0),0)),0) gift,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col5,'DS',1)))/sum(decode(rct.col4,'HIGH',1,0)))*100,0) gift_pct,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col5,'DSP',1,0),0)),0) plants,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col5,'DSP',1)))/sum(decode(rct.col4,'HIGH',1,0)))*100,0) plt_pct,
            	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col5,'DSF',1,0),0)),0) dsf,
	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col5,'DSF',1)))/sum(decode(rct.col4,'HIGH',1,0)))*100,0) dsf_pct,
            	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) ttl_gift_plt_dsf,
 	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0)))/sum(decode(rct.col4,'HIGH',1,0)))*100,0) ttl_gift_plt_dsf_pct,
     	 --
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_ph,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone',1))))*100,0) gift_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone',1))))*100,0) plt_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_ph,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone',1))))*100,0) dsf_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'phone',1))))*100,0) gift_plt_dsf_giftp_pct_ph,
     	--
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_inet,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet',1))))*100,0) gift_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet',1))))*100,0) plt_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_inet,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet',1))))*100,0) dsf_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'HIGH',decode(rct.col3,'internet',1))))*100,0) gift_plt_dsf_giftp_pct_inet
     	--
from 	rpt.category_tab rct;

insert into rpt.rep_tab	(col1,
			col26,
			col28,
			col30,
			col32)
select 	'13',
	decode(rct.col2,'DS',rct.col3) gift_ttl_rev,
      	decode(rct.col2,'DSP',rct.col3) plt_ttl_rev,
      	decode(rct.col2,'DSF',rct.col3) dsf_ttl_rev,
      	sum(rct.col3)  gift_plt_dsf_ttl_rev
from 	rpt.category_tab rct
where rct.col1='96'
group by decode(rct.col2,'DS',rct.col3),
      	decode(rct.col2,'DSP',rct.col3),
      	decode(rct.col2,'DSF',rct.col3);

insert into rpt.rep_tab	(col1,
			col27,
			col29,
			col31,
			col33)
select 	'13',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100 gift_pct_ttl_rev,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100 plt_pct_ttl_rev,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100 dsf_pct_ttl_rev,
            	(sum(rct.col3)/v_gift_revenue)*100 gift_plt_dsf_pct_ttl_rev
from 	rpt.category_tab rct
where rct.col1='96'
group by '13',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100;

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25)
select 	'14',
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DS',1,0),0)),0) gift,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DS',1)))/sum(decode(rct.col4,'FLORIST',1,0)))*100,0) gift_pct,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DSP',1,0),0)),0) plants,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DSP',1)))/sum(decode(rct.col4,'FLORIST',1,0)))*100,0) plt_pct,
            	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DSF',1,0),0)),0) dsf,
	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DSF',1)))/sum(decode(rct.col4,'FLORIST',1,0)))*100,0) dsf_pct,
            	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) ttl_gift_plt_dsf,
 	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0)))/sum(decode(rct.col4,'FLORIST',1,0)))*100,0) ttl_gift_plt_dsf_pct,
     	 --
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_ph,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone',1))))*100,0) gift_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone',1))))*100,0) plt_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_ph,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone',1))))*100,0) dsf_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'phone',1))))*100,0) gift_plt_dsf_giftp_pct_ph,
     	--
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_inet,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet',1))))*100,0) gift_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet',1))))*100,0) plt_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_inet,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet',1))))*100,0) dsf_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'FLORIST',decode(rct.col3,'internet',1))))*100,0) gift_plt_dsf_giftp_pct_inet
     	--
from 	rpt.category_tab rct;

insert into rpt.rep_tab	(col1,
			col26,
			col28,
			col30,
			col32)
select 	'14',
	decode(rct.col2,'DS',rct.col3) gift_ttl_rev,
      	decode(rct.col2,'DSP',rct.col3) plt_ttl_rev,
      	decode(rct.col2,'DSF',rct.col3) dsf_ttl_rev,
      	sum(rct.col3)  gift_plt_dsf_ttl_rev
from 	rpt.category_tab rct
where rct.col1='95'
group by decode(rct.col2,'DS',rct.col3),
      	decode(rct.col2,'DSP',rct.col3),
      	decode(rct.col2,'DSF',rct.col3);

insert into rpt.rep_tab	(col1,
			col27,
			col29,
			col31,
			col33)
select 	'14',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100 gift_pct_ttl_rev,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100 plt_pct_ttl_rev,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100 dsf_pct_ttl_rev,
            	(sum(rct.col3)/v_gift_revenue)*100 gift_plt_dsf_pct_ttl_rev
from 	rpt.category_tab rct
where rct.col1='95'
group by '14',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100;

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25)
select 	'15',
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col5,'DS',1,0),0)),0) gift,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col5,'DS',1)))/sum(decode(rct.col4,'FUSA',1,0)))*100,0) gift_pct,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col5,'DSP',1,0),0)),0) plants,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col5,'DSP',1)))/sum(decode(rct.col4,'FUSA',1,0)))*100,0) plt_pct,
            	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col5,'DSF',1,0),0)),0) dsf,
	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col5,'DSF',1)))/sum(decode(rct.col4,'FUSA',1,0)))*100,0) dsf_pct,
            	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) ttl_gift_plt_dsf,
 	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0)))/sum(decode(rct.col4,'FUSA',1,0)))*100,0) ttl_gift_plt_dsf_pct,
     	 --
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_ph,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone',1))))*100,0) gift_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone',1))))*100,0) plt_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_ph,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone',1))))*100,0) dsf_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'phone',1))))*100,0) gift_plt_dsf_giftp_pct_ph,
     	--
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_inet,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet',1))))*100,0) gift_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet',1))))*100,0) plt_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_inet,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet',1))))*100,0) dsf_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'FUSA',decode(rct.col3,'internet',1))))*100,0) gift_plt_dsf_giftp_pct_inet
     	--
from 	rpt.category_tab rct;

insert into rpt.rep_tab	(col1,
			col26,
			col28,
			col30,
			col32)
select 	'15',
	decode(rct.col2,'DS',rct.col3) gift_ttl_rev,
      	decode(rct.col2,'DSP',rct.col3) plt_ttl_rev,
      	decode(rct.col2,'DSF',rct.col3) dsf_ttl_rev,
      	sum(rct.col3)  gift_plt_dsf_ttl_rev
from 	rpt.category_tab rct
where rct.col1='94'
group by decode(rct.col2,'DS',rct.col3),
      	decode(rct.col2,'DSP',rct.col3),
      	decode(rct.col2,'DSF',rct.col3);

insert into rpt.rep_tab	(col1,
			col27,
			col29,
			col31,
			col33)
select 	'15',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100 gift_pct_ttl_rev,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100 plt_pct_ttl_rev,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100 dsf_pct_ttl_rev,
            	(sum(rct.col3)/v_gift_revenue)*100 gift_plt_dsf_pct_ttl_rev
from 	rpt.category_tab rct
where rct.col1='94'
group by '15',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100;

insert into rpt.rep_tab	(col1,
			col2,
			col3,
			col4,
			col5,
			col6,
			col7,
			col8,
			col9,
			col10,
			col11,
			col12,
			col13,
			col14,
			col15,
			col16,
			col17,
			col18,
			col19,
			col20,
			col21,
			col22,
			col23,
			col24,
			col25)
select 	'16',
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DS',1,0),0)),0) gift,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DS',1)))/sum(decode(rct.col4,'FDIRECT',1,0)))*100,0) gift_pct,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DSP',1,0),0)),0) plants,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DSP',1)))/sum(decode(rct.col4,'FDIRECT',1,0)))*100,0) plt_pct,
            	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DSF',1,0),0)),0) dsf,
	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DSF',1)))/sum(decode(rct.col4,'FDIRECT',1,0)))*100,0) dsf_pct,
            	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0),0)),0) ttl_gift_plt_dsf,
 	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col5,'DS',1, 'DSP',1,'DSF',1,0)))/sum(decode(rct.col4,'FDIRECT',1,0)))*100,0) ttl_gift_plt_dsf_pct,
     	 --
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_ph,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone',1))))*100,0) gift_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone',1))))*100,0) plt_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_ph,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone',1))))*100,0) dsf_giftp_pct_ph,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_ph,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'phone',1))))*100,0) gift_plt_dsf_giftp_pct_ph,
     	--
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DS',1,0),0),0)),0) gift_giftp_inet,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DS',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet',1))))*100,0) gift_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DSP',1,0),0),0)),0) plt_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DSP',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet',1))))*100,0) plt_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DSF',1,0),0),0)),0) dsf_giftp_ttll_inet,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DSF',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet',1))))*100,0) dsf_giftp_pct_inet,
	nvl(sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5, 'DS',1,'DSP',1,'DSF',1,0),0),0)),0) gift_plt_dsf_giftp_ttl_inet,
            	nvl((sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet', decode(rct.col5,'DS',1,'DSP',1,'DSF',1))))/sum(decode(rct.col4,'FDIRECT',decode(rct.col3,'internet',1))))*100,0) gift_plt_dsf_giftp_pct_inet
     	--
from 	rpt.category_tab rct;

insert into rpt.rep_tab	(col1,
			col26,
			col28,
			col30,
			col32)
select 	'16',
	decode(rct.col2,'DS',rct.col3) gift_ttl_rev,
      	decode(rct.col2,'DSP',rct.col3) plt_ttl_rev,
      	decode(rct.col2,'DSF',rct.col3) dsf_ttl_rev,
      	sum(rct.col3)  gift_plt_dsf_ttl_rev
from 	rpt.category_tab rct
where rct.col1='93'
group by decode(rct.col2,'DS',rct.col3),
      	decode(rct.col2,'DSP',rct.col3),
      	decode(rct.col2,'DSF',rct.col3);

insert into rpt.rep_tab	(col1,
			col27,
			col29,
			col31,
			col33)
select 	'16',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100 gift_pct_ttl_rev,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100 plt_pct_ttl_rev,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100 dsf_pct_ttl_rev,
            	(sum(rct.col3)/v_gift_revenue)*100 gift_plt_dsf_pct_ttl_rev
from 	rpt.category_tab rct
where rct.col1='93'
group by '16',
	(decode(rct.col2,'DS',rct.col3)/v_gift_revenue)*100,
      	(decode(rct.col2,'DSP',rct.col3)/v_gift_revenue)*100,
            	(decode(rct.col2,'DSF',rct.col3)/v_gift_revenue)*100;         	

end;
.
/
