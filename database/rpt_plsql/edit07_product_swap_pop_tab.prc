CREATE OR REPLACE procedure rpt.EDIT07_Product_Swap_Pop_Tab
		(p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_partner_id in varchar2
		)
IS

  v_start_date 		date		:= to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		date		:= to_date(p_end_date,'mm/dd/yyyy');
  v_sql			varchar2(3000)	:= NULL;
  v_partner_predicate varchar2(1000);

BEGIN
  
  --**********************************************************************
  -- Product Swap Query
  --**********************************************************************
  --
  if p_partner_id<>'ALL' then
	v_partner_predicate:= ' AND pod.partner_id in ('||rpt.multi_value(p_partner_id)||') ';
  end if;

  v_sql :=
  'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11
	)
	SELECT to_char(o.order_date, ''mm/dd/yyyy''),
	    to_char(ppu.delivery_date, ''mm/dd/yyyy''),
	    pod.partner_order_item_number,
	    od.external_order_number,
	    ppu.original_product_id,
	    to_char(ppu.original_merch_amount, ''99990.00''),
	    ppu.new_product_id,
	    to_char(ppu.new_merch_amount, ''99990.00''),
	    ppu.created_by,
	    pod.partner_id,
	    to_char(ppu.new_merch_amount - ppu.original_merch_amount, ''99990.00'')
	FROM joe.partner_product_update ppu
	JOIN clean.order_details od
	ON od.order_detail_id = ppu.order_detail_id
	JOIN clean.orders o
	ON o.order_guid = od.order_guid
	JOIN ptn_pi.partner_order_detail pod
	ON pod.confirmation_number = od.external_order_number
	WHERE TRUNC(ppu.created_on) BETWEEN '''
	    || v_start_date || ''' and ''' || v_end_date || ''''
	||v_partner_predicate;

  EXECUTE IMMEDIATE v_sql;

END;
.
/
