CREATE OR REPLACE FUNCTION rpt.GET_FTDW_VENDOR_ID
RETURN VARCHAR2 AS 
/*-----------------------------------------------------------------------------
Description:
        This function gets the vendor_id associated with the FTD West shipping system.

Output:
        vendor_Id          VARCHAR2
        
-----------------------------------------------------------------------------*/
ftdw_vendor_id			Varchar2(5) := null;

BEGIN
  
  SELECT vendor_id
    INTO ftdw_vendor_id
	  FROM FTD_APPS.VENDOR_MASTER
	 WHERE VENDOR_TYPE = 'FTD WEST';
  
RETURN ftdw_vendor_id;

EXCEPTION
   WHEN OTHERS THEN
      RETURN null;
END;
.
/
