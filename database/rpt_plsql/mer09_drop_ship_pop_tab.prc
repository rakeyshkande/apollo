CREATE OR REPLACE
PROCEDURE rpt.MER09_Drop_Ship_Pop_Tab
                (p_product_id in varchar2,
                 p_vendor_id in varchar2,
                 p_available_ind in varchar2
                )
IS
  v_start_week                   date;
  v_end_week                     date;
  v_start_month                  date;
  v_end_month                    date;
  v_product_id_predicate_1         varchar2(1000) := NULL;
  v_product_id_predicate_2         varchar2(1000) := NULL;
  v_product_id_predicate_3         varchar2(1000) := NULL;
  v_vendor_id_predicate_1          varchar2(1000) := NULL;
  v_vendor_id_predicate_2          varchar2(1000) := NULL;
  v_vendor_id_predicate_3          varchar2(1000) := NULL;
  v_available_ind_predicate      varchar2(1000) := NULL;
  v_sql                          varchar2(10000)        := NULL;
  v_sql_pre                      varchar2(4000)        := NULL;

BEGIN
--**********************************************************************
  -- Set the product id and vendor id WHERE clause predicates
  --**********************************************************************
  --
  if  p_product_id <> 'ALL'
  and p_product_id IS NOT NULL then
        v_product_id_predicate_1 := ' AND vv.product_id in (SELECT product_id FROM FTD_APPS.INV_TRK_PRODUCT_TEMP)';
        v_product_id_predicate_2 := ' AND fpm.product_id in (SELECT product_id FROM FTD_APPS.INV_TRK_PRODUCT_TEMP)';
        v_product_id_predicate_3 := ' AND fait.product_id in (SELECT product_id FROM FTD_APPS.INV_TRK_PRODUCT_TEMP)';
        
  end if;
  --
  if  p_vendor_id <> 'ALL'
  and p_vendor_id IS NOT NULL then
        v_vendor_id_predicate_1 := ' AND vv.vendor_id in (SELECT vendor_id FROM FTD_APPS.INV_TRK_VENDOR_TEMP)';
        v_vendor_id_predicate_2 := ' AND fvm.vendor_id in (SELECT vendor_id FROM FTD_APPS.INV_TRK_VENDOR_TEMP)';
        v_vendor_id_predicate_3 := ' AND fait.vendor_id in (SELECT vendor_id FROM FTD_APPS.INV_TRK_VENDOR_TEMP)';
        
  end if;
  --
  if p_available_ind IS NOT NULL then
        v_available_ind_predicate :=
                ' AND fpm.status = ''' || p_available_ind || '''';
  end if;
  --
  --**********************************************************************
  -- Vendor Drop Ship Control Query
  --**********************************************************************
  --
  
  -- Step 1: Insert inventory data into temporary table. This is to get around the problem with inconsistent results
  -- caused by multiple group by statements.
  v_sql_pre := 'INSERT into ftd_apps.inv_trk_search_temp
  		(inv_trk_id,
  		 ftd_msg_count,
  		 can_msg_count,
  		 rej_msg_count
  		)
  		select fait.inv_trk_id,
		       sum(itac.ftd_msg_count) ftd_msg_count,
		       sum(itac.can_msg_count) can_msg_count,
		       sum(itac.rej_msg_count) rej_msg_count
		       from ftd_apps.inv_trk fait
		       join venus.inv_trk_assigned_count itac
		       on fait.product_id=itac.product_id
		       and fait.vendor_id=itac.vendor_id
		       where itac.ship_date between fait.start_date and fait.end_date '
		       || v_product_id_predicate_3
        	       || v_vendor_id_predicate_3 
          	       || ' group by fait.inv_trk_id'
          	;
  
  --delete from ftd_apps.inv_trk_search_temp;
  --INSERT into ftd_apps.inv_trk_search_temp
  --  		(inv_trk_id,
  --  		 ftd_msg_count,
  --  		 can_msg_count,
  --  		 rej_msg_count
  --		)
  --select fait.inv_trk_id,
--	       sum(itac.ftd_msg_count) ftd_msg_count,
--	       sum(itac.can_msg_count) can_msg_count,
--	       sum(itac.rej_msg_count) rej_msg_count
--	       from ftd_apps.inv_trk fait
--	       join venus.inv_trk_assigned_count itac
	--       on fait.product_id=itac.product_id
	--       and fait.vendor_id=itac.vendor_id
	--       where itac.ship_date between fait.start_date and fait.end_date 
--group by fait.inv_trk_id;
  
  v_sql :=
  'INSERT into rpt.rep_tab
        (col1,
         col2,
         col3,
         col4,
         col5,
         col6,
         col7,
         col8,
         col9,
         col10,
         col11,
         col12,
         col13,
         col14,
         col15,
         col16,
         col17,
         col18,
         col19,
         col20,
         col21,
         col22,
         col23,
         col24
        )
select nvl(inv_counts.vendor_id,''Unknown'') vendor_id,
       nvl(inv_counts.product_id,''Unknown'') product_id,
       0,
       nvl(fvm.vendor_name,''Unknown Vendor'')           vendor_name,
       nvl(fvm.member_number,''00-0000AA'')              vendor_member_number,
       inv_counts.inventory_level,
       inv_counts.inventory_updated_on,
       inv_counts.start_date inventory_start_date,
       inv_counts.end_date inventory_end_date,
       inv_counts.product_id it_product_id,
       nvl(fpm.product_name,''N/A'')            	product_name,
       fpm.status                               	product_status,
       inv_counts.prev_30_day_order_count,
       inv_counts.sun_count,
       inv_counts.mon_count,
       inv_counts.tue_count,
       inv_counts.wed_count,
       inv_counts.thu_count,
       inv_counts.fri_count,
       inv_counts.sat_count,
       inv_counts.all_count,
       inv_counts.warning_threshold,
       inv_counts.shutdown_threshold,
       ''1'' vendor_sort_order
from 
   (
	select a.vendor_id,
           a.product_id,     
       (a.revised_forecast_qty - a.ftd_msg_count + a.can_msg_count + a.rej_msg_count) inventory_level,
       trunc(a.updated_on)                             inventory_updated_on,
       a.start_date,
       a.end_date,
       count(1) prev_30_day_order_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Sun'',1,0)
                         ELSE null
                    END)     sun_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Mon'',1,0)
                         ELSE null
                    END)     mon_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Tue'',1,0)
                         ELSE null
                    END)     tue_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Wed'',1,0)
                         ELSE null
                    END)     wed_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Thu'',1,0)
                         ELSE null
                    END)     thu_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Fri'',1,0)
                         ELSE null
                    END)     fri_count,
       sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Sat'',1,0)
                         ELSE null
                    END)     sat_count,
       nvl(sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN 1
                         ELSE null
                    END),0)                                all_count,
       a.warning_threshold_qty warning_threshold,
       a.shutdown_threshold_qty shutdown_threshold
    from venus.venus vv
    JOIN (select fait.product_id,
                 fait.vendor_id,
                 fait.start_date,
                 fait.end_date,
                 fait.revised_forecast_qty,
                 fait.warning_threshold_qty,
                 fait.shutdown_threshold_qty,
                 fait.updated_on,
                 st.ftd_msg_count,
                 st.can_msg_count,
                 st.rej_msg_count
          from ftd_apps.inv_trk fait
          join ftd_apps.inv_trk_search_temp st
            on fait.inv_trk_id=st.inv_trk_id
         ) a
      on vv.product_id=a.product_id
      and vv.vendor_id=a.vendor_id
    where a.start_date <= trunc(vv.order_date)
      AND a.end_date >= trunc(vv.order_date)
      AND NOT EXISTS (SELECT  1
                      FROM    venus.venus v2
                      WHERE   v2.venus_order_number = vv.venus_order_number
                        AND     v2.msg_type = ''CAN'')
      AND vv.order_date >= TRUNC(SYSDATE - 29)
      AND vv.msg_type = ''FTD''
      '
        || v_product_id_predicate_1
        || v_vendor_id_predicate_1  
        || '
    group by a.vendor_id,
             a.product_id,
             (a.revised_forecast_qty - a.ftd_msg_count + a.can_msg_count + a.rej_msg_count),
             trunc(a.updated_on),
             a.start_date,
             a.end_date,
             a.warning_threshold_qty,
             a.shutdown_threshold_qty
   ) inv_counts
JOIN         ftd_apps.vendor_product fvp
   ON           inv_counts.product_id = fvp.product_subcode_id
   AND         inv_counts.vendor_id=fvp.vendor_id
JOIN         ftd_apps.product_master fpm
   ON           fpm.product_id = inv_counts.product_id
JOIN         ftd_apps.vendor_master fvm
   ON           fvm.vendor_id = inv_counts.vendor_id
WHERE fvp.removed = ''N''
      AND fvm.active = ''Y''
'
      || v_product_id_predicate_2
      || v_vendor_id_predicate_2
      || v_available_ind_predicate
|| '
UNION                  
        SELECT  null                 	vendor_id,
                fpm.product_id         product_id,
                0,-- nvl(vv.reference_number, ''999999999'')   	reference_number,
                null  	vendor_name,
                null    	vendor_member_number,
                null inventory_level,
                null                          	inventory_updated_on,
                null                          	inventory_start_date,
                null inventory_end_date,
                fpm.product_id                           		it_product_id,
                nvl(fpm.product_name,''N/A'')            	product_name,
                fpm.status                               	product_status,
                count(1)                                  	prev_30_day_order_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Sun'',1,0)
                         ELSE null
                    END)     sun_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Mon'',1,0)
                         ELSE null
                    END)     mon_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Tue'',1,0)
                         ELSE null
                    END)     tue_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Wed'',1,0)
                         ELSE null
                    END)     wed_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Thu'',1,0)
                         ELSE null
                    END)     thu_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Fri'',1,0)
                         ELSE null
                    END)     fri_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Sat'',1,0)
                         ELSE null
                    END)     sat_count,
                nvl(sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN 1
                         ELSE null
                    END),0)                                all_count,
                null warning_threshold,
                null shutdown_threshold,
                ''2'' vendor_sort_order
        FROM    venus.venus vv
        JOIN 	ftd_apps.product_master fpm
        ON	vv.product_id = fpm.product_id
         WHERE
         NOT EXISTS (SELECT  1
                                FROM    venus.venus v2
                                WHERE   v2.venus_order_number = vv.venus_order_number
                                AND     v2.msg_type = ''CAN'')
         AND vv.order_date >= TRUNC(SYSDATE - 29)
        '
        || v_product_id_predicate_1
        || v_vendor_id_predicate_1
        || v_available_ind_predicate || '         
         AND    vv.msg_type = ''FTD''
         AND vv.vendor_id is null
         GROUP BY null,
                fpm.product_id,
                0,-- nvl(vv.reference_number, ''999999999'')   	reference_number,
                fpm.product_id,
                nvl(fpm.product_name,''N/A''),
                fpm.status,
                ''2''';

  delete from ftd_apps.inv_trk_search_temp;
  EXECUTE IMMEDIATE v_sql_pre;
  EXECUTE IMMEDIATE v_sql;
  
END
;
.
/
