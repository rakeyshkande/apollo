create or replace PROCEDURE rpt.mkt03_customer_recap_pop_tab
(
in_rpt_date		date := add_months (trunc (sysdate, 'mm'), -1)
)
IS

v_rpt_date	date := nvl (trunc (in_rpt_date, 'mm'), add_months (trunc (sysdate, 'mm'), -1));
v_check		number := 0;

BEGIN

/*------------------------------------------------------------------------------------------------------
-- This report's sylk file can be recreated for previously built reports without having to rebuild the
-- report data. The default behavior of the report is to build the report via the scheduled job,
-- mkt03_customer_recap_pkg.submit_cust_recap on the 1st of the month and schedule the creation of the
-- excel file on the 5th of the month.
--
-- The logic is as follows:
-- Check to see if the report data exists in the rep_cust_recap table.   If it exists then, retrieve
-- the report data without rebuilding it.  If it doens't exist, then build the report data.
-------------------------------------------------------------------------------------------------------*/

-- Check if the report data exists.  If not, build the report.
select	count (1)
into	v_check
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy');

if v_check = 0 then
	rpt.mkt03_customer_recap_pkg.build_cust_recap (v_rpt_date);
end if;

-- retrieve the report records for the excel file
insert into rpt.rep_tab
(	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
  col14
)
select	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
  col14
from	rpt.rep_cust_recap
where	col1 = to_char (v_rpt_date, 'mm/dd/yyyy')
order by to_number (col2);

end;
/
