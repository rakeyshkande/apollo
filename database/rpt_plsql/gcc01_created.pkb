CREATE OR REPLACE
PACKAGE BODY rpt.GCC01_Created IS

  count_v		number(9)  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure p( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
	rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     --	when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end p;

--****************************************************************************************************
/*********************************************************************/
/* The print_heading procedure is used to write the heading of the   */
/* file.  The first four lines are standard in all files.  The ones  */
/* with "'F;C2;FG0R;SM0'" in them need to be edited based on how many*/
/* columns are rturned in your query.  See the comments bellow for   */
/* information.                                                      */
/*********************************************************************/

  procedure print_headings(y in number)
  is
  BEGIN
		p( 'F;R2;FG0C;SM2' );
    p( 'C;Y2;X2;K"REQUEST #"' );
    p( 'C;X3;K"ISSUE  :DATE"' );
    p( 'C;X4;K"TYPE"' );
    p( 'C;X5;K"PROGRAM  :NAME"' );
    p( 'C;X6;K"COMPANY"' );
    p( 'C;X7;K"ISSUE  :AMT"' );
    p( 'C;X8;K"PAID  :AMT"' );
    p( 'C;X9;K"QUANTITY"' );
    p( 'C;X10;K"TOTAL AMT  :ISSUED"' );
    p( 'C;X11;K"TOTAL AMT  :PAID"' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_headings;
--
--*******************
  procedure     print_title(  p_start_date in date,
                              p_end_date   in date)
  is
    l_title varchar2(2000);
    v_month varchar2(10);

  begin

    v_month := nvl(to_char(p_start_date,'mm/yyyy'),to_char(add_months(sysdate,-1),'mm/yyyy'));
    p( 'ID;ORACLE' );
    p( 'P;Ph:mm:ss');
    p( 'P;FCourier;M200' );
    p( 'P;FCourier;M200;SB' );
    p( 'P;FCourier;M200;SUB' );
    p( 'P;FCourier;M160' );
    p( 'F;C1;FG0R;SM1');
    p( 'F;R1;FG0C;SM2' );
    if p_start_date is null then
	      p( 'C;Y1;X4;K" GIFT CERTIFICATE AND COUPONS ACTIVATED SUMMARY REPORT FOR '||v_month||'"' );
    else
        p( 'C;Y1;X4;K" GIFT CERTIFICATE AND COUPONS ACTIVATED SUMMARY REPORT FOR '||to_char(p_start_date,'mm/dd/yyyy')||' THRU '||to_char(p_end_date,'mm/dd/yyyy')||'"' );
    end if;
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_title;
--**********************************************************************************************************
  procedure print_widths is
  begin
    p( 'F;W1 1 5' );
    p( 'F;W2 2 15' );
    p( 'F;W3 3 15' );
    p( 'F;W4 20 15' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_widths;
--**********************************************************************************************************
  procedure sub_totals_for_dates (p_y in number,
                                  p_date_y in number) is
  line varchar2(300);
  BEGIN
           line := 'C;Y'|| to_char(p_y) || ';X7';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X8';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X9';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X10';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X11';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
   END sub_totals_for_dates;

procedure sub_totals_for_team_or_center (p_y in number,
                                         p_cell_totals in varchar2)
            is
             line varchar2(300);
            BEGIN
             line := 'C;Y'|| to_char(p_y) || ';X7';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X8';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X9';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X10';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X11';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
end sub_totals_for_team_or_center;

/*********************************************************************/
/* The print_rows function adds the data records to the file.  In    */
/* this example there are actually two cursors.  The cursor is just  */
/* your query from you data model.  If you have a complex data model */
/* you will have to some additional PL/SQL to get all of your data.  */
/*********************************************************************/

function print_rows(p_start_date in date,
                    p_end_date   in date) return number is
type    my_curs_type is REF CURSOR;
curs    my_curs_type;
TYPE    type_total_tab_type is table of varchar2(10)
        index by pls_integer;
date_total_tab type_total_tab_type; -- Keeps track of the cells that contain totals for each date changes
                                    -- Will be used for total of each team
--team_total_tab type_total_tab_type; -- Keeps track of the cells that contain totals for each Team changes
                                    -- Will be used for total of each team
null_tab       type_total_tab_type;  -- This is used to clear the tab
i       number(4) := 1;             -- Index for type_total_tab
t       number(4) := 1;             -- Index for team_total_tab
date_y number := 3;
v_old_type   varchar2(80) := 'XXXX';
str     varchar2(5000);
type ret_rec is record
              (request_number  varchar2(80),
               issue_date varchar2(20),
               coupon_type varchar2(80),
               gc_type varchar2(80),
      	       program_name varchar2(80),
      	       company varchar2(80),
               issue_amt  number,
               paid_amt  number,
               quantity number,
               total_amt_issued number,
               total_amt_paid number);
v_request_number_pred varchar2(300);
v_start_date_pred1 varchar2(300);
v_start_date_pred2 varchar2(300);
ret     ret_rec;
row_cnt number          := 0;
line    varchar2(32767) := null;
n       number;
y       number;


--*****************************************************************************
--*****************************************************************************
--*
--*****************************************************************************
--*****************************************************************************

begin

-----------------------------------------------------
--  Building the predicates (Where clause conditions)
-----------------------------------------------------

if p_start_date is null then
   v_start_date_pred2 := ' where trunc(cgprp.process_date) between add_months(trunc(sysdate,''MONTH''),-1)
                                              and last_day(add_months(sysdate,-1)) ';
   v_start_date_pred1 := ' where trunc(cgcr.issue_date) between add_months(trunc(sysdate,''MONTH''),-1)
                                              and last_day(add_months(sysdate,-1)) ';

else
	  v_start_date_pred1 := ' where trunc(cgcr.issue_date) between '''||p_start_date||'''
                               and '''||p_end_date||''' ';
	  v_start_date_pred2 := ' where trunc(cgprp.process_date) between '''||p_start_date||'''
                               and '''||p_end_date||''' ';

end if;

-----------------------------------------------------
--  Define the SELECT
-----------------------------------------------------

str := '  select cgcr.request_number request_number,
            to_char(cgcr.issue_date,''MM/DD/YYYY'') issue_date,
  nvl(decode(cgcr.gc_coupon_type,''GC'',''GC'',''CP'',''CP\CS'',''CS'',''CP\CS''),''UNK'') coupon_type,
            nvl(cgcr.gc_coupon_type,''UNK'') gc_type,
            cgcr.program_name program_name,
            facm.company_name company,
            cgcr.issue_amount issue_amt,
            cgcr.paid_amount paid_amt,
            cgcr.quantity quantity,
            cgcr.issue_amount *  cgcr.quantity total_amt_issued,
            cgcr.paid_amount *  cgcr.quantity total_amt_paid
 from   clean.gc_coupon_request cgcr,
             clean.gc_coupons cgc,
             ftd_apps.company_master facm          '
||v_start_date_pred1
||
'      and  cgcr.request_number = cgc.request_number
       and  cgcr.company_id        = facm.company_id
      and cgcr.gc_partner_program_name is null
 group by cgcr.request_number ,
            to_char(cgcr.issue_date,''MM/DD/YYYY''),
  nvl(decode(cgcr.gc_coupon_type,''GC'',''GC'',''CP'',''CP\CS'',''CS'',''CP\CS''),''UNK''),
            nvl(cgcr.gc_coupon_type,''UNK''),
            cgcr.program_name,
            facm.company_name,
            cgcr.issue_amount,
            cgcr.paid_amount,
            cgcr.quantity
union all
select cgcr.request_number request_number,
            to_char(cgcr.issue_date,''MM/DD/YYYY'') issue_date,
  nvl(decode(cgcr.gc_coupon_type,''GC'',''GC'',''CP'',''CP\CS'',''CS'',''CP\CS''),''UNK'') coupon_type,
            nvl(cgcr.gc_coupon_type,''UNK'') gc_type,
            cgcr.program_name program_name,
            facm.company_name company,
            cgcr.issue_amount issue_amt,
            cgcr.paid_amount paid_amt,
            count(distinct(cgprp.gc_coupon_number)) quantity,
            cgcr.issue_amount *  count(cgprp.gc_coupon_number) total_amt_issued,
            cgcr.paid_amount *  count(cgprp.gc_coupon_number) total_amt_paid
 from   clean.gc_coupon_request cgcr
 join   clean.gc_coupons cgc                   on cgcr.request_number = cgc.request_number
 join   ftd_apps.company_master facm           on cgcr.company_id        = facm.company_id
 join   clean.gc_program_request_process cgprp on cgprp.gc_coupon_number = cgc.gc_coupon_number '
||v_start_date_pred2
||
' and  cgprp.gc_program_config_id in (5,10,2)
 group by cgcr.request_number ,
            to_char(cgcr.issue_date,''MM/DD/YYYY''),
  nvl(decode(cgcr.gc_coupon_type,''GC'',''GC'',''CP'',''CP\CS'',''CS'',''CP\CS''),''UNK''),
            nvl(cgcr.gc_coupon_type,''UNK''),
            cgcr.program_name,
            facm.company_name,
            cgcr.issue_amount,
            cgcr.paid_amount,
            cgcr.quantity
 order by   3 desc,
 1';



/*dbms_output.put_line(substr(str,1,200));
dbms_output.put_line(substr(str,201,200));
dbms_output.put_line(substr(str,401,200));
dbms_output.put_line(substr(str,601,200));
dbms_output.put_line(substr(str,801,200));
dbms_output.put_line(substr(str,1001,200));
dbms_output.put_line(substr(str,1201,200));
dbms_output.put_line(substr(str,1401,200));
dbms_output.put_line(substr(str,1601,200));
dbms_output.put_line(substr(str,1801,200));
dbms_output.put_line(substr(str,2001,200));
dbms_output.put_line(substr(str,2201,200));
dbms_output.put_line(substr(str,2401,200));
dbms_output.put_line(substr(str,2601,200));
dbms_output.put_line(substr(str,2801,200));
dbms_output.put_line(substr(str,3001,200));
dbms_output.put_line(substr(str,3201,200));
dbms_output.put_line(substr(str,3401,200));
dbms_output.put_line(substr(str,3601,200));
dbms_output.put_line(substr(str,3801,200));
*/

p( 'C;Y3' );
y := 3;

-----------------------------------------------------
--  Process the Cursor
-----------------------------------------------------

      OPEN curs for str;
      LOOP
      	FETCH curs  into ret;
      	exit when curs%notfound;

/*********************************************************************/
/* Two notes here.  First you want the number next to the "X" to     */
/* match the number next to the "C" in your column heading part.     */
/* Second you do not want to repeat a number next to the "X".  If    */
/* do it will not show up correctly in the excel file.               */
/*********************************************************************/

		if v_old_type<>ret.coupon_type and y>3
			then
			     p( 'F;R'||to_char(y)||';FG0R;SM1' );
           line := 'C;Y'|| to_char(y) || ';X3';
           line := line || ';K"Totals for Type '||v_old_type||'"';
           p( line );
           sub_totals_for_dates (y,date_y);
           date_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
      	   y:=y+1;
      	   date_y := y;
           p( 'C;Y'||to_char(y) );
			end if;

        line := 'C;X2'; -- || to_char(count_v);
        line := line || ';K"'||ret.request_number||'"';
        p( line );

        line := 'C;X3'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.issue_date||'"';
        p( line );

        line := 'C;X4'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.gc_type||'"';
        p( line );

        line := 'C;X5'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.program_name||'"';
        p( line );

        line := 'C;X6'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.company||'"';
        p( line );

        p( 'F;F$2G;X7' );--to add a dollar sign and 2 decimal places
        line := 'C;X7'; -- || to_char(count_v+1);
        line := line || ';K'||ret.issue_amt||'';
        p( line );

        p( 'F;F$2G;X8' );--to add a dollar sign and 2 decimal places
        line := 'C;X8'; -- || to_char(count_v+1);
        line := line || ';K'||ret.paid_amt||'';
        p( line );

        line := 'C;X9'; -- || to_char(count_v+1);
        line := line || ';K'||ret.quantity||'';
        p( line );

        p( 'F;F$2G;X10' );--to add a dollar sign and 2 decimal places
        line := 'C;X10'; -- || to_char(count_v+1);
        line := line || ';K'||ret.total_amt_issued||'';
        p( line );

        p( 'F;F$2G;X11' );--to add a dollar sign and 2 decimal places
        line := 'C;X11'; -- || to_char(count_v+1);
        line := line || ';K'||ret.total_amt_paid||'';
        p( line );

/*********************************************************************/
/* The next two lines are for increase the line number and create a  */
/* new record line.                                                  */
/*                                                                   */
/*********************************************************************/

        y := y + 1;
        p( 'C;Y'||to_char(y) );

	  if g_CLOB = 'N' then
	     utl_file.fflush( g_file );
	  end if;

        v_old_type:=ret.coupon_type;

      END LOOP;


  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 3 then

      line := 'C;Y'|| to_char(y) || ';X2';
      line := line || ';K"No Data Found"';
      p( line );

      y := y + 1;
      p( 'C;Y'||to_char(y) );

      if g_CLOB = 'N' then
	 utl_file.fflush( g_file );
      end if;

  else
	--  Print sub totals for last date
      	   p( 'F;R'||to_char(y)||';FG0R;SM1' );
           line := 'C;Y'|| to_char(y) || ';X3';
           line := line || ';K"Totals for Type '||v_old_type||'"';
           p( line );
           sub_totals_for_dates (y,date_y);
           date_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
      	   y:=y+1;
      	   date_y := y;
      --
      	   DECLARE
      		 v_cell_totals  varchar2(300);
      	   BEGIN
      	     FOR j in date_total_tab.first..date_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	     END LOOP;
      	     v_cell_totals := rtrim(v_cell_totals,',');
      	     p( 'F;R'||to_char(y)||';FG0R;SM1' );
             line := 'C;Y'|| to_char(y) || ';X3';
             line := line || ';K"Grand Totals "';
             p( line );
             sub_totals_for_team_or_center (y,v_cell_totals);
      	   END;
  end if;

  return row_cnt;

  end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure show(p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_submission_id in number,
		 p_CLOB in varchar2
		)
is
   v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
   v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
   l_row_cnt number(9);
   v_OUT_STATUS_det	varchar2(30);
   v_OUT_MESSAGE_det	varchar2(1000);
   v_OUT_STATUS_sub 	varchar2(30);
   v_OUT_MESSAGE_sub	varchar2(1000);

begin

--
-- Prepare for either CLOB output or REPORT_DIR output
--

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
			P_SUBMISSION_ID,
			'SYS',
			'S' ,
			null ,
			v_OUT_STATUS_DET,
			v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
       			(
			P_SUBMISSION_ID,
			empty_clob(),
			v_out_status_det,
			v_out_message_det
		        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','GCC01_Gift_Cert_Created.slk', 'W' );
   end if;


    print_title(  v_start_date,
                  v_end_date);

    print_headings(2);

    l_row_cnt := print_rows(v_start_date,
                    v_end_date);

    print_widths;

    p( 'E' );

  --
  -- Update Database with CLOB and set report submission status (if CLOB'ing)
  -- Close report file (if not CLOB'ing
  --

     if p_CLOB = 'Y' then
        begin
  	-- update the report_detail_char table with the package global variable g_clob
  	rpt.report_clob_pkg.update_report_detail_char
  			(
  			 p_submission_id,
  			 v_out_status_det,
  			 v_out_message_det
  			);
  	if v_out_status_det = 'N' then
  	-- if db update failure, update report_submission_log with ERROR status
  	rpt.report_pkg.update_report_submission_log
  			(
  			 p_submission_id,
  			 'REPORT',
  			 'E',
  			 v_out_message_det,
  			 v_out_status_sub,
  			 v_out_message_sub
  			);
  	else
  	-- if db update successful, update report_submission_log with COMPLETE status
  	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
  			(
  			 P_SUBMISSION_ID,
  			 'SYS',
  			 'C' ,
  			 null ,
  			 v_out_status_det ,
  			 v_out_message_det
  			);
  	end if;
  	--exception
  	--	when others then srw.message(32321,'Error in p');
       end;
       commit;
     else
       utl_file.fflush( g_file );
       utl_file.fclose( g_file );
   end if;

  end show;
end;
.
/
