CREATE OR REPLACE
PACKAGE BODY rpt.GCC03_Coupons_Redeemed IS

  count_v               number(9)  := 1;
  g_SUBMISSION_ID       number(22);
  g_CLOB                varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure p( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
        rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     -- when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end p;


--****************************************************************************************************
  procedure print_headings(y in number)
  is
  BEGIN
    p( 'F;R'||y||';FG0C;SM2' );
    p( 'C;Y2;X2;K"REQUEST #"' );
    p( 'C;X3;K"CERTIFICATE #"' );
    p( 'C;X4;K"ISSUE  :DATE"' );
    p( 'C;X5;K"TYPE"' );
    p( 'C;X6;K"PROGRAM  :NAME"' );
    p( 'C;X7;K"COMPANY"' );
    p( 'C;X8;K"REDEMPTION  :DATE"' );
    p( 'C;X9;K"REDEMPTION  :AMT"' );
    p( 'C;X10;K"ISSUE  :AMT"' );
    p( 'C;X11;K"PAID  :AMT"' );
    p( 'C;X12;K"COUNT"' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_headings;
--*******************************************************************************************
  procedure     print_title(  p_start_date in date,
                              p_end_date   in date)
  is
    l_title varchar2(2000);
    v_month varchar2(10);

  begin

        v_month := to_char(add_months(sysdate,-1),'mm/yyyy');
    p( 'ID;ORACLE' );
    p( 'P;Ph:mm:ss');
    p( 'P;FCourier;M200' );
    p( 'P;FCourier;M200;SB' );
    p( 'P;FCourier;M200;SUB' );
    p( 'P;FCourier;M160' );
    p( 'F;C1;FG0R;SM1');
    p( 'F;R1;FG0C;SM2' );
    if p_start_date is null then
              p( 'C;Y1;X4;K" GIFT CERTIFICATE AND COUPONS REDEMPTION REPORT FOR '||v_month||'"' );
    else
        p( 'C;Y1;X4;K" GIFT CERTIFICATE AND COUPONS REDEMPTION REPORT FOR '||to_char(p_start_date,'mm/dd/yyyy')||' THRU '||to_char(p_end_date,'mm/dd/yyyy')||'"' );
    end if;

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_title;

--**********************************************************************************************************
  procedure print_widths is
  begin
    p( 'F;W1 1 5' );
    p( 'F;W2 2 15' );
    p( 'F;W3 3 15' );
    p( 'F;W4 20 15' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

  end print_widths;
--**********************************************************************************************************
  procedure sub_totals_for_coupon_type (p_y in number,
                                  p_type_y in number) is
  line varchar2(300);
  BEGIN
           line := 'C;Y'|| to_char(p_y) || ';X9';
           line := line || ';ESUM(R'||to_char(p_type_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X10';
           line := line || ';ESUM(R'||to_char(p_type_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X11';
           line := line || ';ESUM(R'||to_char(p_type_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X12';
           line := line || ';ECOUNTA(R'||to_char(p_type_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
   END sub_totals_for_coupon_type;
--*************************************************************************************************************
procedure sub_totals_for_team_or_center (p_y in number,
                                         p_cell_totals in varchar2)
            is
             line varchar2(300);
            BEGIN
             line := 'C;Y'|| to_char(p_y) || ';X9';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
                                                        line := 'C;Y'|| to_char(p_y) || ';X10';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X11';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X12';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
end sub_totals_for_team_or_center;
--**************************************************************************************************************
function print_rows(p_start_date in date,
                    p_end_date   in date,
                    p_request_number in number ) return number is
type    my_curs_type is REF CURSOR;
curs    my_curs_type;
TYPE    type_total_tab_type is table of varchar2(10)
        index by pls_integer;
type_total_tab type_total_tab_type; -- Keeps track of the cells that contain totals for each date changes
                                    -- Will be used for total of each team
--team_total_tab type_total_tab_type; -- Keeps track of the cells that contain totals for each Team changes
                                    -- Will be used for total of each team
null_tab       type_total_tab_type;  -- This is used to clear the tab
i       number(4) := 1;             -- Index for type_total_tab
t       number(4) := 1;             -- Index for team_total_tab
type_y number := 3;
v_old_type   varchar2(80) := 'XXXX';
str     varchar2(5000);
type ret_rec is record
              (request_number  varchar2(80),
              gc_number varchar2(80),
              issue_date date,
                  coupon_type varchar2(80),
                gc_coupon_type varchar2(80),
              program_name varchar2(80),
              company varchar2(80),
                    redemption_date varchar2(15),
                    redemption_amt  number,
                    issue_amt  number,
                    paid_amt  number,
              count_gc number);
v_request_number_pred varchar2(300);
v_start_date_pred varchar2(300);
ret     ret_rec;
row_cnt number          := 0;
line    varchar2(32767) := null;
n       number;
y       number;


--*****************************************************************************
--*****************************************************************************
--*
--*****************************************************************************
--*****************************************************************************

begin

-----------------------------------------------------
--  Building the predicates (Where clause conditions)
-----------------------------------------------------
if p_request_number is null then
   v_request_number_pred := null;
else
   v_request_number_pred := 'and cgcr.request_number= '||p_request_number||' ';
end if;

--dbms_output.put_line(P_START_DATE);

if p_start_date is null then
--dbms_output.put_line('Thinks p_start not null');
   v_start_date_pred := ' and trunc(ch.redemption_date) between add_months(trunc(sysdate,''MONTH''),-1)
                                              and last_day(add_months(sysdate,-1)) ';
else
--dbms_output.put_line('Thinks p_start IS null');
          v_start_date_pred := ' and trunc(ch.redemption_date) between trunc(to_date('''||p_start_date||'''))
                               and trunc(to_date('''||p_end_date||''')) ';
end if;


-----------------------------------------------------
--  Define the SELECT
-----------------------------------------------------

str := 'with   count_gc as  (select count(cgc.gc_coupon_number) count_gc_number,
                                                             cgcr.request_number
                                       from clean.gc_coupons cgc,
                                        clean.gc_coupon_request cgcr
                                    where cgc.request_number=cgcr.request_number
                                    group by cgcr.request_number)
select cgcr.request_number request_number,
             cgc.gc_coupon_number gc_number,
             cgcr.issue_date issue_date,
             nvl(decode(cgcr.gc_coupon_type,''GC'',''GC'',''CP'',''CP\CS'',''CS'',''CP\CS''),''UNK'') coupon_type,
            nvl(cgcr.gc_coupon_type,''UNK'') gc_coupon_type,
           cgcr.program_name program_name,
             facm.company_name company,
            to_char(ch.redemption_date,''mm/dd/yyyy'') redemption_date,
             ch.redemption_amount redemption_amt,
           cgcr.issue_amount issue_amt,
           cgcr.paid_amount paid_amt,
             1 count_gc
from clean.gc_coupons cgc
join         clean.gc_coupon_request cgcr
on (cgc.request_number=cgcr.request_number)
join          count_gc  count_gc
on (cgcr.request_number= count_gc.request_number)
join          clean.gc_coupon_history ch
on ( cgc.gc_coupon_number=ch.gc_coupon_number)
left outer join ftd_apps.company_master facm
on (cgcr.company_id=facm.company_id)
where ch.status = ''Active''
and ch.redemption_date is not null '
||v_start_date_pred||
v_request_number_pred||
' order by cgcr.gc_coupon_type desc,
cgcr.request_number,
cgc.gc_coupon_number';


--dbms_output.put_line(substr(str,-255,255));
p( 'C;Y3' );
y := 3;

-----------------------------------------------------
--  Process the Cursor
-----------------------------------------------------

      OPEN curs for str;
      LOOP
        FETCH curs  into ret;
        exit when curs%notfound;

/*********************************************************************/
/* Two notes here.  First you want the number next to the "X" to     */
/* match the number next to the "C" in your column heading part.     */
/* Second you do not want to repeat a number next to the "X".  If    */
/* do it will not show up correctly in the excel file.               */
/*********************************************************************/

                if v_old_type<>ret.coupon_type and y>3
                        then
                             p( 'F;R'||to_char(y)||';FG0R;SM1' );
           line := 'C;Y'|| to_char(y) || ';X3';
           line := line || ';K"Totals for Type '||v_old_type||'"';
           p( line );
           sub_totals_for_coupon_type (y,type_y);
           type_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
           y:=y+1;
           type_y := y;
           p( 'C;Y'||to_char(y) );
                        end if;

        line := 'C;X2'; -- || to_char(count_v);
        line := line || ';K"'||ret.request_number||'"';
        p( line );

        line := 'C;X3'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.gc_number||'"';
        p( line );

        line := 'C;X4'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.issue_date||'"';
        p( line );

        line := 'C;X5'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.gc_coupon_type||'"';
        p( line );

        line := 'C;X6'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.program_name||'"';
        p( line );

        line := 'C;X7'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.company||'"';
        p( line );

        line := 'C;X8'; -- || to_char(count_v+1);
        line := line || ';K"'||ret.redemption_date||'"';
        p( line );

        p( 'F;F$2G;X9' );--to add a dollar sign and 2 decimal places
        line := 'C;X9'; -- || to_char(count_v+1);
        line := line || ';K'||ret.redemption_amt||'';
        p( line );

        p( 'F;F$2G;X10' );--to add a dollar sign and 2 decimal places
        line := 'C;X10'; -- || to_char(count_v+1);
        line := line || ';K'||ret.issue_amt||'';
        p( line );

        p( 'F;F$2G;X11' );--to add a dollar sign and 2 decimal places
        line := 'C;X11'; -- || to_char(count_v+1);
        line := line || ';K'||ret.paid_amt||'';
        p( line );

        line := 'C;X12'; -- || to_char(count_v+1);
        line := line || ';K'||ret.count_gc||'';
        p( line );

/*********************************************************************/
/* The next two lines are for increase the line number and create a  */
/* new record line.                                                  */
/*                                                                   */
/*********************************************************************/

        y := y + 1;
        p( 'C;Y'||to_char(y) );

          if g_CLOB = 'N' then
             utl_file.fflush( g_file );
          end if;

        v_old_type:=ret.coupon_type;

      END LOOP;


  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 3 then

      line := 'C;Y'|| to_char(y) || ';X2';
      line := line || ';K"No Data Found"';
      p( line );

      y := y + 1;
      p( 'C;Y'||to_char(y) );

      if g_CLOB = 'N' then
         utl_file.fflush( g_file );
      end if;

  else

--  Print sub totals for last date
           p( 'F;R'||to_char(y)||';FG0R;SM1' );
           line := 'C;Y'|| to_char(y) || ';X3';
           line := line || ';K"Totals for Type '||v_old_type||'"';
           p( line );
           sub_totals_for_coupon_type (y,type_y);
           type_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
           y:=y+1;
           type_y := y;
      --
           DECLARE
                 v_cell_totals  varchar2(300);
           BEGIN
             FOR j in type_total_tab.first..type_total_tab.last loop
                  v_cell_totals :=  v_cell_totals||type_total_tab(j)||',';
             END LOOP;
             v_cell_totals := rtrim(v_cell_totals,',');
             p( 'F;R'||to_char(y)||';FG0R;SM1' );
             line := 'C;Y'|| to_char(y) || ';X3';
             line := line || ';K"Grand Totals "';
             p( line );
             sub_totals_for_team_or_center (y,v_cell_totals);
           END;

  end if;

  return row_cnt;

  end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure show(p_start_date in varchar2,
                 p_end_date in varchar2,
                 p_request_number in number := null,
                 p_submission_id in number,
                 p_CLOB in varchar2
                )
is
   v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
   v_end_date date   := to_date(p_end_date,'mm/dd/yyyy');
   l_row_cnt number(9);
   v_OUT_STATUS_det     varchar2(30);
   v_OUT_MESSAGE_det    varchar2(1000);
   v_OUT_STATUS_sub     varchar2(30);
   v_OUT_MESSAGE_sub    varchar2(1000);

begin
--
-- Prepare for either CLOB output or REPORT_DIR output
--

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
                        P_SUBMISSION_ID,
                        'SYS',
                        'S' ,
                        null ,
                        v_OUT_STATUS_DET,
                        v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
                        (
                        P_SUBMISSION_ID,
                        empty_clob(),
                        v_out_status_det,
                        v_out_message_det
                        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','GCC03_Gift_Cert_Coupons_Redeemed.slk', 'W' );
   end if;

   --
   -- Build Report
   --

    print_title(  v_start_date,
                  v_end_date);

    print_headings(2);

    l_row_cnt := print_rows(v_start_date,
                    v_end_date,
                    p_request_number);

    print_widths;

    p( 'E' );

   --
   -- Update Database with CLOB and set report submission status (if CLOB'ing)
   -- Close report file (if not CLOB'ing
   --

      if p_CLOB = 'Y' then
         begin
        -- update the report_detail_char table with the package global variable g_clob
        rpt.report_clob_pkg.update_report_detail_char
                        (
                         p_submission_id,
                         v_out_status_det,
                         v_out_message_det
                        );
        if v_out_status_det = 'N' then
        -- if db update failure, update report_submission_log with ERROR status
        rpt.report_pkg.update_report_submission_log
                        (
                         p_submission_id,
                         'REPORT',
                         'E',
                         v_out_message_det,
                         v_out_status_sub,
                         v_out_message_sub
                        );
        else
        -- if db update successful, update report_submission_log with COMPLETE status
        rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
                         P_SUBMISSION_ID,
                         'SYS',
                         'C' ,
                         null ,
                         v_out_status_det ,
                         v_out_message_det
                        );
        end if;
        --exception
        --      when others then srw.message(32321,'Error in p');
        end;
        commit;
      else
        utl_file.fflush( g_file );
        utl_file.fclose( g_file );
   end if;
  end show;
end;
.
/
