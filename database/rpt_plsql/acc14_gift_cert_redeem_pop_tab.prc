CREATE OR REPLACE
PROCEDURE rpt.acc14_gift_cert_redeem_pop_tab
			(p_start_date in varchar2,
			 p_end_date in varchar2
			)
IS
  v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
  v_end_date date := to_date(p_end_date,'mm/dd/yyyy');

BEGIN
  insert into rpt.rep_tab
	 (col1,
	  col2,
	  col3,
	  col4,
	  col5,
	  col6,
	  col7
	  )
  select cgch.gc_coupon_number			certificate_number,
	 cgcrq.issue_date			issue_date,
	 cgcrq.issue_amount			gc_value,
	 cc.first_name||' '||cc.last_name	customer_name,
	 cc.zip_code				zip_code,
	 cgch.redemption_date			redemption_date,
	 (cgcrq.issue_amount/2)			due_ftd_com
  --
  from	 clean.gc_coupon_request cgcrq
  join	 clean.gc_coupons cgc
		on (cgcrq.request_number=cgc.request_number)
  join	clean.gc_coupon_history cgch
		on (cgc.gc_coupon_number=cgch.gc_coupon_number)
  join	clean.order_details cod
		on (cgch.order_detail_id=cod.order_detail_id)
  join	clean.orders co
		on (cod.order_guid=co.order_guid)
  join	clean.customer cc
		on (co.customer_id=cc.customer_id)
  --
  where trunc(cgch.redemption_date) between v_start_date and v_end_date
  and substr(cgc.gc_coupon_number,0,2)='DC'
  and cgch.status = 'Active'
  ;

END;
.
/
