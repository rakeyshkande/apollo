CREATE OR REPLACE procedure rpt.EDIT05_Srvc_Fee_Edit_Pop_Tab
		(p_start_date in varchar2,
		 p_end_date in varchar2
		)
IS

  v_start_date 		date		:= to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		date		:= to_date(p_end_date,'mm/dd/yyyy');
  v_sql			varchar2(3000)	:= NULL;

BEGIN
  
  --**********************************************************************
  -- Service Fee Edit Query
  --**********************************************************************
  --
  v_sql :=
  'INSERT into rpt.rep_tab
	(col1,
	 col2,
	 col3,
	 col4,
	 col5,
	 col6,
	 col7,
	 col8,
	 col9,
	 col10,
	 col11,
	 col12,
	 col13,
	 col14
	)
	SELECT	DECODE(FASO.operation$, ''DEL'', ''3'', ''INS'', ''2'', ''1'') sort1,
			   DECODE(FASO.operation$, ''DEL'', ''4'', ''INS'', ''3'', ''UPD_NEW'', ''2'', ''UPD_OLD'', ''1'', ''0'') sort2,
		      DECODE(FASO.operation$, ''DEL'', ''Deletions'', ''INS'', ''New'', ''UPD_OLD'', ''Updates'', ''UPD_NEW'', ''Updates'', ''Updated'') change_category,
		      DECODE(FASO.operation$, ''DEL'', ''Deletion'', ''INS'', ''New'', ''UPD_OLD'', ''Old'', ''UPD_NEW'', ''Updated'', ''CHANGE?'') change_type,
				FASO.snh_id			  snh_id,
				TO_CHAR(FASO.TIMESTAMP$, ''mm/dd/yyyy hh:mi:ss am'') timestamp,
				DECODE(FASO.operation$, ''UPD_OLD'', TO_CHAR(FASO.UPDATED_ON, ''mm/dd/yyyy hh:mi:ss am''), ''UPD_NEW'', TO_CHAR(FASO.UPDATED_ON, ''mm/dd/yyyy hh:mi:ss am''),TO_CHAR(FASO.TIMESTAMP$, ''mm/dd/yyyy hh:mi:ss am'')) updated_on,
				TO_CHAR(FASO.override_date, ''mm/dd/yyyy'') override_date,
				FASO.updated_by		  updated_by,
				FASO.requested_by requested_by,
				to_char(FASO.domestic_charge,''99990.00'') domestic_charge,
				to_char(FASO.international_charge, ''99990.00'') international_charge,
				to_char(FASO.vendor_charge,''99990.00'') vendor_charge,
				to_char(FASO.vendor_sat_upcharge,''99990.00'') vendor_sat_upcharge
	  --
	  FROM 	ftd_apps.service_fee_override$ FASO
	  --
	 WHERE	TRUNC(FASO.TIMESTAMP$) BETWEEN '''
				|| v_start_date || ''' and ''' || v_end_date || '''';

  EXECUTE IMMEDIATE v_sql;

END;
.
/
