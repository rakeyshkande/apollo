CREATE OR REPLACE
PROCEDURE rpt.MER01_Vendor_Drop_Ship_Pop_Tab
                (p_product_id in varchar2,
                 p_vendor_id in varchar2,
                 p_available_ind in varchar2
                )
IS
  v_start_week                   date;
  v_end_week                     date;
  v_start_month                  date;
  v_end_month                    date;
  v_product_id_predicate         varchar2(1000) := NULL;
  v_vendor_id_predicate          varchar2(1000) := NULL;
  v_available_ind_predicate      varchar2(1000) := NULL;
  v_sql                          varchar2(10000)        := NULL;

BEGIN
--**********************************************************************
  -- Set the product id and vendor id WHERE clause predicates
  --**********************************************************************
  --
  if  p_product_id <> 'ALL'
  and p_product_id IS NOT NULL then
        v_product_id_predicate :=
                ' AND fapm.product_id in ('||rpt.multi_value(p_product_id)||') ';
  end if;
  --
  if  p_vendor_id <> 'ALL'
  and p_vendor_id IS NOT NULL then
        v_vendor_id_predicate :=
                ' AND fapm.vendor_id in ('||rpt.multi_value(p_vendor_id)||') ';
  end if;
  --
  if p_available_ind IS NOT NULL then
        v_available_ind_predicate :=
                ' AND fapm.status = ''' || p_available_ind || '''';
  end if;
  --
  --**********************************************************************
  -- Vendor Drop Ship Control Query
  --**********************************************************************
  --
  v_sql :=
  'INSERT into rpt.rep_tab
        (col1,
         col2,
         col3,
         col4,
         col5,
         col6,
         col7,
         col8,
         col9,
         col10,
         col11,
         col12,
         col13,
         col14,
         col15,
         col16,
         col17,
         col18,
         col19,
         col20,
         col21
        )
        SELECT  nvl(fapm.vendor_id,''00000'')               vendor_id,
                nvl(fapm.product_id,''X999'')               product_id,
                0,-- nvl(vv.reference_number, ''999999999'')   reference_number,
                nvl(fapm.vendor_name,''Unknown Vendor'')  vendor_name,
                nvl(fapm.member_number,''00-0000AA'')     vendor_member_number,
                faic.inventory_level                      inventory_level,
                trunc(faic.updated_on)                           inventory_updated_on,
                trunc(faic.start_date)                           inventory_start_date,
                faic.end_date                             inventory_end_date,
                fapm.product_id                           product_id,
                nvl(fapm.product_name,''N/A'')            product_name,
                fapm.status                               product_status,
                count(1)                                  prev_30_day_order_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Sun'',1,0)
                         ELSE null
                    END)     sun_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Mon'',1,0)
                         ELSE null
                    END)     mon_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Tue'',1,0)
                         ELSE null
                    END)     tue_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Wed'',1,0)
                         ELSE null
                    END)     wed_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Thu'',1,0)
                         ELSE null
                    END)     thu_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Fri'',1,0)
                         ELSE null
                    END)     fri_count,
                sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN decode(to_char(vv.order_date,''Dy''),''Sat'',1,0)
                         ELSE null
                    END)     sat_count,
                nvl(sum(CASE WHEN vv.order_date between trunc(sysdate) - 6 and trunc(sysdate)
                         THEN 1
                         ELSE null
                    END),0)                                all_count
        FROM    venus.venus vv
        JOIN    (select fpm.product_id product_id,
                        fpm.product_name,
								fvm.vendor_id,
				 				fvm.vendor_name,
				 				fvm.member_number,
                        fpm.status
                 from   ftd_apps.product_master fpm,
				 				ftd_apps.vendor_master fvm,
				 				ftd_apps.vendor_product fvp
				 		where fpm.product_id = fvp.product_subcode_id
						  and fvm.vendor_id = fvp.vendor_id
						  and fvp.removed = ''N''
						  and fvm.active = ''Y''
                 UNION
                 select fps.product_subcode_id,
                        fpm2.product_name || ''-''|| fps.subcode_description product_name,
				 				fvm2.vendor_id,
				 				fvm2.vendor_name,
				 				fvm2.member_number,
                        fpm2.status
                 from   ftd_apps.product_subcodes fps,
                        ftd_apps.product_master fpm2,
				 				ftd_apps.vendor_master fvm2,
				 				ftd_apps.vendor_product fvp2
                 where  fps.product_id = fpm2.product_id
                   and  fps.product_subcode_id = fvp2.product_subcode_id
						 and  fvm2.vendor_id = fvp2.vendor_id
						 and  fvp2.removed = ''N''
						 and  fvm2.active = ''Y'') fapm
        on (fapm.product_id = vv.product_id)
        left outer JOIN ftd_apps.inventory_control faic
                        on (faic.product_id = fapm.product_id
                        AND faic.vendor_id = fapm.vendor_id)
         WHERE
         NOT EXISTS (    SELECT  1
                                FROM    venus.venus v2
                                WHERE   v2.venus_order_number = vv.venus_order_number
                                AND     v2.msg_type = ''CAN'')
         AND vv.order_date >= TRUNC(SYSDATE - 29)
        '
        || v_product_id_predicate
        || v_vendor_id_predicate
        || v_available_ind_predicate || '
         AND    vv.msg_type = ''FTD''
         GROUP BY fapm.vendor_id,
                  fapm.product_id,
                  0, --vv.reference_number,
                  fapm.vendor_name,
                  fapm.member_number,
                  faic.inventory_level,
                  trunc(faic.updated_on),
                  trunc(faic.start_date),
                  faic.end_date,
                  fapm.product_id,
                  fapm.product_name,
                  fapm.status';
/*************************************************************************************************************
        SELECT  nvl(favm.vendor_id,''00000'')                           vendor_id,
                nvl(cod.product_id,''X999'')                            product_id,
                nvl(vv.reference_number, ''999999999'')                 reference_number,
                nvl(favm.vendor_name,''Unknown Vendor'')                vendor_name,
                nvl(favm.member_number,''00-0000AA'')           vendor_member_number,
                faic.inventory_level                                    inventory_level,
                faic.updated_on                                 inventory_updated_on,
                faic.start_date                                 inventory_start_date,
                faic.end_date                                   inventory_end_date,
                fapm.product_id                                 product_id,
                nvl(fapm.product_name,''N/A'')                  product_name,
                fapm.status                                     product_status,
                count(co.order_guid)                            prev_30_day_order_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Sun'',1,0)) sun_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Mon'',1,0)) mon_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Tue'',1,0)) tue_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Wed'',1,0)) wed_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Thu'',1,0)) thu_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Fri'',1,0)) fri_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Sat'',1,0)) sat_count,
                nvl(count(vvm.order_date),0)                            all_count
        --
        FROM    clean.order_details cod
        join    clean.orders co
                        on (cod.order_guid=co.order_guid)
        left outer JOIN venus.venus vv
                        on (vv.reference_number = to_char(cod.order_detail_id) and vv.msg_type = ''FTD''
                        AND NOT EXISTS  (select 1
                         from   venus.venus v2
                         where  v2.venus_order_number = vv.venus_order_number
                         and    v2.msg_type = ''CAN'') )
        left outer JOIN (select          cod2.product_id,
                                trunc(co2.order_date) order_date
                        from    clean.order_details cod2,
                                clean.orders co2
                        where cod2.order_guid=co2.order_guid
                        and trunc(co2.order_date) between sysdate-7 and sysdate
                        and NOT EXISTS  (select 1
                                        from    venus.venus v2
                                        where   v2.reference_number  = cod2.order_detail_id
                                        and     v2.msg_type = ''CAN'')
                        and exists      (select 1
                                        from    venus.venus v3
                                        where   v3.reference_number  = cod2.order_detail_id
                                        and     v3.msg_type = ''FTD'')) vvm
                        on (cod.product_id=vvm.product_id)
        JOIN    ftd_apps.vendor_master favm
                        on (favm.vendor_id = cod.vendor_id)
        JOIN    ftd_apps.product_master fapm
                        on (fapm.product_id = cod.product_id)
        left outer JOIN ftd_apps.inventory_control faic
                        on (faic.product_id = cod.product_id)
        --
        WHERE   cod.subcode IS NULL
        AND     trunc(co.order_date) between sysdate-30 and sysdate
        '
        || v_product_id_predicate
        || v_vendor_id_predicate
        || v_available_ind_predicate || '
        --
        GROUP BY favm.vendor_id,
                 cod.product_id,
                 vv.reference_number,
                 favm.vendor_name,
                 favm.member_number,
                 faic.inventory_level,
                 faic.updated_on,
                 faic.start_date,
                 faic.end_date,
                 fapm.product_id,
                 fapm.product_name,
                 fapm.status
        --
        UNION
        --
        SELECT  nvl(favm.vendor_id,''00000'')                           vendor_id,
                nvl(cod.subcode,''X999'')                               product_id,
                nvl(vv.reference_number, ''999999999'')                 reference_number,
                nvl(favm.vendor_name,''Unknown Vendor'')                vendor_name,
                nvl(favm.member_number,''00-0000AA'')                   vendor_member_number,
                faic.inventory_level                                    inventory_level,
                faic.updated_on                                         inventory_updated_on,
                faic.start_date                                         inventory_start_date,
                faic.end_date                                           inventory_end_date,
                fapm.product_id                                         product_id,
                nvl(fapm.product_name||'' - ''||faps.subcode_description,''N/A'')       product_name,
                fapm.status                                             product_status,
                count(co.order_guid)                            prev_30_day_order_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Sun'',1,0)) sun_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Mon'',1,0)) mon_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Tue'',1,0)) tue_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Wed'',1,0)) wed_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Thu'',1,0)) thu_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Fri'',1,0)) fri_count,
                sum(decode(to_char(vvm.order_date,''Dy''),''Sat'',1,0)) sat_count,
                nvl(count(vvm.order_date),0)                            all_count
        --
        FROM    clean.order_details cod
        join    clean.orders co
                        on (cod.order_guid=co.order_guid)
        JOIN    ftd_apps.product_subcodes faps
                        on (faps.product_subcode_id = cod.subcode)
        left outer JOIN venus.venus vv
                        on (vv.reference_number = to_char(cod.order_detail_id) and vv.msg_type = ''FTD''
                        AND NOT EXISTS  (select 1
                         from   venus.venus v2
                         where  v2.venus_order_number = vv.venus_order_number
                         and    v2.msg_type = ''CAN'') )
        left outer JOIN (select          cod2.subcode product_id,
                                trunc(co2.order_date) order_date
                        from    clean.order_details cod2,
                                clean.orders co2
                        where cod2.order_guid=co2.order_guid
                        and trunc(co2.order_date) between sysdate-7 and sysdate
                        and NOT EXISTS  (select 1
                                        from    venus.venus v2
                                        where   v2.reference_number  = cod2.order_detail_id
                                        and     v2.msg_type = ''CAN'')
                        and exists      (select 1
                                        from    venus.venus v3
                                        where   v3.reference_number  = cod2.order_detail_id
                                        and     v3.msg_type = ''FTD'')) vvm
                        on (cod.subcode=vvm.product_id)
        JOIN    ftd_apps.vendor_master favm
                        on (favm.vendor_id = cod.vendor_id)
        JOIN    ftd_apps.product_master fapm
                        on (fapm.product_id = faps.product_id)
        left outer JOIN ftd_apps.inventory_control faic
                        on (faic.product_id = cod.subcode)
        --
        WHERE   trunc(co.order_date) between sysdate-30 and sysdate
        '
        || v_product_id_predicate
        || v_vendor_id_predicate
        || v_available_ind_predicate || '
        --
        GROUP BY favm.vendor_id,
                 cod.subcode,
                 vv.reference_number,
                 favm.vendor_name,
                 favm.member_number,
                 faic.inventory_level,
                 faic.updated_on,
                 faic.start_date,
                 faic.end_date,
                 fapm.product_id,
                 nvl(fapm.product_name||'' - ''||faps.subcode_description,''N/A''),
                 fapm.status'
***************************************************************************************************************/

   EXECUTE IMMEDIATE v_sql
   ;
END
;
.
/
