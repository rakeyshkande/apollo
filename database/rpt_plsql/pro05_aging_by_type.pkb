CREATE OR REPLACE
PACKAGE BODY rpt.PRO05_Aging_By_Type IS

  count_v		number(9)  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure p( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
	rpt.report_pkg.UPDATE_REPORT_DETAIL_CHAR
		(
		g_SUBMISSION_ID,
		p_str||chr(10),
		v_OUT_STATUS_det,
		v_OUT_MESSAGE_det
		);
	if v_out_status_det = 'N' then
		rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
		(
		g_SUBMISSION_ID,
		'REPORT',
		'E',
		v_OUT_MESSAGE_det,
		v_OUT_STATUS_sub,
		v_OUT_MESSAGE_sub
		);
	end if;
  end if;

end;

--****************************************************************************************************
--****************************************************************************************************
procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  p( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;
--
--****************************************************************************************************
-- This is the column headings for the Report
--****************************************************************************************************
procedure print_headings(y in number)
is
begin

    p( 'F;R'||y||';FG0L;SM2' );
    p( 'C;Y'||y||';X4;K"# OF"' );
    p( 'C;Y'||y||';X5;K"# OF"' );
    p( 'C;Y'||y||';X6;K"AVG"' );

    p( 'F;R'||3||';FG0L;SM2' );
    p( 'C;Y'||3||';X2;K"QUEUE"' );
    p( 'C;Y'||3||';X3;K"DATE"' );
    p( 'C;Y'||3||';X4;K"ITEMS TAGGED"' );
    p( 'C;Y'||3||';X5;K"TOUCHES"' );
    p( 'C;Y'||3||';X6;K"HANDLE TIME"' );
    if g_CLOB = 'N' then
       utl_file.fflush( g_file );
    end if;

end print_headings;
--
--*******************************************************************************************
-- This sets up the Formats that will be used in the SLK file.  It also prints a title
-- for our report.
--*******************************************************************************************
procedure print_title(p_start_date in date, p_end_date in date)
is
    l_title varchar2(2000);
begin

    p( 'ID;ORACLE' );
    p( 'P;Ph:mm:ss');
    p( 'P;FCourier;M200' );
    p( 'P;FCourier;M150;SB' );
    p( 'P;FCourier;M200;SUB' );

    p( 'F;C1;FG0L;SM1');
    p( 'C;Y1;X2;K"AGING REPORT BY TYPE REPORT FOR ' ||
		p_start_date || ' Thru ' || p_end_date || '"' );
    if g_CLOB = 'N' then
       utl_file.fflush( g_file );
    end if;

end print_title;
--
--**********************************************************************************************************
-- This will Define the Column Widths for the SLK File
--**********************************************************************************************************
procedure print_widths is
begin

    p( 'F;W1 1 5' );     -- margin
    p( 'F;W2 2 30' );    -- Center, team, Queue
    p( 'F;W3 3 10' );	 -- order date
    p( 'F;W4 5 15' );    -- Number of Tagged,Number of Touches
    p( 'F;W6 6 15' );    -- Averag Handle Time
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_widths;
/*
--
--**********************************************************************************************************
-- This will Print the subtotals for a date change.  This will accept the starting and ending point of
-- the range of cells that will be summed.
--**********************************************************************************************************
procedure sub_totals_for_dates (p_y in number,
                                  p_date_y in number) is
  line varchar2(300);
  BEGIN
           p( 'F;Y'||to_char(p_y)||';X5;FG0C' );
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X5';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'F;P0;FG0R'||to_char(p_y)||';X6';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X6';
           line := line || ';EAVERAGE(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
end sub_totals_for_dates;
*/
--
--*************************************************************************************************************
-- This will print a subtotal when the team or center is changed. It accepts the row this subtotal is printed
-- as well as a string of all the cells that will be summed.
--*************************************************************************************************************
procedure sub_totals_for_team_center (p_y in number,
                                         p_cell_totals in varchar2)
            is
             line varchar2(300);
            BEGIN
             p( 'F;Y'||to_char(p_y)||';X4;FG0C' );
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X4';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             p( 'F;Y'||to_char(p_y)||';X5;FG0C' );
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X5';
             line := line || ';ESUM('||p_cell_totals||')';
             p( line );
             line := 'F;P0;FG0R'||to_char(p_y)||';X6';
             p( line );
             line := 'C;Y'|| to_char(p_y) || ';X6';
             line := line || ';EAVERAGE('||p_cell_totals||')';
             p( line );
end sub_totals_for_team_center;
--**********************************************************************************************************
-- This will Print the subtotals for a date change.  This will accept the starting and ending point of
-- the range of cells that will be summed.
--**********************************************************************************************************
procedure sub_totals_for_date (p_y in number,
                               p_date_y in varchar2)
                                          is
           line varchar2(300);
           BEGIN
           p( 'F;Y'||p_y||';X4;FG0C' );
           line := 'C;Y'|| to_char(p_y) || ';X4';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           p( 'F;Y'||p_y||';X5;FG0C' );
           line := 'C;Y'|| to_char(p_y) || ';X5';
           line := line || ';ESUM(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
           line := 'F;P0;FG0R'||to_char(p_y)||';X6';
           p( line );
           line := 'C;Y'|| to_char(p_y) || ';X6';
           line := line || ';EAVERAGE(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
           p( line );
end sub_totals_for_date;
--
--**************************************************************************************************************
-- This will dynamically build the select statement. Loop thru the REF CURSOR and determine where to print
-- the lines for the SLK file
--**************************************************************************************************************
function print_rows(p_start_date in date,
                    p_end_date   in date,
                    p_center_name in varchar2,
                    p_team_name   in varchar2,
                    p_queue_type  in varchar2) return number is
   type    my_curs_type is REF CURSOR; -- This must be a weak REF CURSOR (No Returning clause)
   curs    my_curs_type;  -- This allows us to define the SQL Statement for the CURSOR Dynamically
                          -- which was required because the users wanted to be able to pass multiple
                          -- parameter values.  It's also more efficient if they pass a null value
   TYPE    date_total_tab_type is table of varchar2(10)
           index by pls_integer;
   date_total_tab date_total_tab_type; -- Keeps track of the cells that contain totals for each date changes
                                       -- Will be used for total of each team
   team_total_tab date_total_tab_type; -- Keeps track of the cells that contain totals for each Team changes
                                       -- Will be used for total of each team
   null_tab       date_total_tab_type;  -- This is used to clear the tab
   i       number(4) := 1;             -- Index for date_total_tab
   t       number(4) := 1;             -- Index for team_total_tab
   date_y number := 4;                 -- Determines the start range for totaling Date totals
   center_y number := 4;                 -- Determines the start range for totaling Center totals
   str     varchar2(5000);             -- This holds the Dynamic Query that will be executed.
   type ret_rec is record              -- When we fetch we will fetch into a record of this structure
                 (tagged_on varchar2(80),
       	         queue_type varchar2(80),
                 center_name  varchar2(80),
                 team_name varchar2(80),
      	         Number_tagged varchar2(80),
        	       Number_touched number,
                 avg_handle_time number);
   ret     ret_rec;                    -- Based on the record type defined above
   v_old_team   ftd_apps.cs_groups.description%type  := 'XXXX';  -- Used to determine when a team changes
   v_old_center ftd_apps.call_center.description%type:= 'XXXX';	-- Used to determine when a center changes
   v_old_date   varchar2(15) := '01-jan-1900';                   -- Used to determine when a Date changes
   v_center_name_pred varchar2(300);                             -- Used to create a where clause condition for center.
   v_team_name_pred varchar2(300);                               -- Used to create a where clause condition for team.
   v_queue_type_pred varchar2(300);                              -- Used to create a where clause condition for Queue.
   row_cnt number          := 0;
   line    varchar2(32767) := null; -- Each line of  text that's written to the file.
   n       number;
   y       number;                  -- Line number of the text that's written


begin
   -----------------------------------------------------
   --  Building the predicates (Where clause conditions)
   -----------------------------------------------------


   if p_center_name is null
   or p_center_name = 'ALL' then
      v_center_name_pred := null;
   else
      --v_center_name_pred:= ' and upper(facc.description) in ('||upper(multi_value(p_center_name))||') ';
      v_center_name_pred:= ' and upper(facc.call_center_id) in ('||upper(multi_value(p_center_name))||') ';
   end if;

   if p_team_name is null
   or p_team_name = 'ALL' then
      v_team_name_pred := null;
   else
      --v_team_name_pred:= ' and upper(facg.description) in ('||upper(multi_value(p_team_name))||') ';
      v_team_name_pred:= ' and upper(facg.cs_group_id) in ('||upper(multi_value(p_team_name))||') ';
   end if;

   if p_queue_type is null
   or p_queue_type = 'ALL' then
      v_queue_type_pred := null;
   else
      v_queue_type_pred := ' and upper(cq.queue_type) in  ('||upper(multi_value(p_queue_type))||') ';
   end if;

   -----------------------------------------------------
   --  Define the SELECT
   -----------------------------------------------------
   str := 'select nvl(to_char(trunc(cqt.tagged_on),''mm/dd/yyyy''),'' '') tagged_on,
           nvl(cq.queue_type,'' '') queue_type,
             nvl(facc.description,'' '') center_name,
             nvl(facg.description,'' '') team_name,
              count(cqt.message_id) Number_tagged,
             count(ceh.entity_id) Number_touched,
             nvl(avg(ceh.end_time-ceh.start_time),0) avg_handle_time
   from clean.queue_tag cqt
   join clean.queue cq
   on (cqt.message_id = cq.message_id)
   left outer join clean.orders co
   on      (cq.order_guid = co.order_guid)
   join    aas.identity ai
   on      (cqt.csr_id = ai.identity_id)
   left outer join    aas.user_history auh
   on      (ai.user_id = auh.user_id)
   join    ftd_apps.call_center facc
   on      (facc.call_center_id = auh.call_center_id)
   left outer join clean.entity_history ceh
   on      (ceh.entity_id = co.order_guid)
   join           ftd_apps.cs_groups facg
   on      (facg.cs_group_id = auh.cs_group_id)
   where       trunc(cqt.tagged_on) between ''' || p_start_date || '''
				    and     ''' || p_end_date || '''
   and   trunc(cqt.tagged_on) between nvl(auh.start_date,trunc(co.order_date))
					  and nvl(auh.end_date,trunc(co.order_date)) '
   || v_center_name_pred
   || v_team_name_pred
   || v_queue_type_pred
   ||
   ' group by nvl(to_char(trunc(cqt.tagged_on),''mm/dd/yyyy''),'' '') ,
              nvl(cq.queue_type,'' '') ,
             nvl(facc.description,'' '') ,
             nvl(facg.description,'' '')
   order by nvl(facc.description,'' ''),
             nvl(facg.description,'' ''),
           nvl(to_char(trunc(cqt.tagged_on),''mm/dd/yyyy''),'' '')';
--
--****************************************************************************
-- Open the cursor and start the loop
--****************************************************************************
--
         y := 5;
--
        OPEN curs for str;
        LOOP
      	   FETCH curs  into ret;
         	     exit when curs%notfound;
       	if v_old_date <> ret.tagged_on and y > 5 then
	   --  Print sub totals for date
      	   p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
           p( line );
      	   sub_totals_for_date(y,date_y);
           date_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
      	   y:=y+1;
      	   date_y := y;
      	end if;

      	if (v_old_team <> ret.team_name and v_old_date = ret.tagged_on) and y > 5 then
	   --  Print sub totals for team
      	   p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
           p( line );
      	   sub_totals_for_date(y,date_y);
           date_total_tab(i) := 'R'||to_char(y)||'C';
           i := i+1;
      	   y:=y+1;

	   -- print total for team
      	   DECLARE
      		 v_cell_totals  varchar2(300);
      	   BEGIN
      	     FOR j in date_total_tab.first..date_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	     END LOOP;
      	     v_cell_totals := rtrim(v_cell_totals,',');
      	     p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' for '||to_char(p_start_date,'yyyy/mm/dd')||' THRU '||to_char(p_end_date,'yyyy/mm/dd')||'"';
             p( line );
             sub_totals_for_team_center (y , v_cell_totals);
             date_total_tab := null_tab;
             team_total_tab(t) := 'R'||to_char(y)||'C';
             t := t+1;
             i := i+1;
      	     y:=y+1;
      	     date_y := y;
      	   END;
      	end if;

      	if (v_old_team <> ret.team_name and v_old_date <> ret.tagged_on) and y > 5 then
	   -- print total for team
      	   DECLARE
      		 v_cell_totals  varchar2(300);
      	   BEGIN
      	     FOR j in date_total_tab.first..date_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	     END LOOP;
      	     v_cell_totals := rtrim(v_cell_totals,',');
	     p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' for '||to_char(p_start_date,'yyyy/mm/dd')||' THRU '||to_char(p_end_date,'yyyy/mm/dd')||'"';
             p( line );
             sub_totals_for_team_center (y , v_cell_totals);
             date_total_tab := null_tab;
             team_total_tab(t) := 'R'||to_char(y)||'C';
             t := t+1;
             i := i+1;
      	     y:=y+1;
      	     date_y := y;
      	   END;
      	   if v_old_center = ret.center_name then
      	      p( 'F;R'||to_char(y)||';FG0C;SM1' );
      	      line := 'C;Y'|| to_char(y) || ';X2';
              line := line || ';K"'||ret.team_name||'"';
              p( line );
              y:=y+1;
           end if;
      	end if;

      	if v_old_center <> ret.center_name and y = 5 then
      	   p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
           line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"'||ret.center_name||'"';
           p( line );
      	   y:=y+1;
      	   p( 'F;R'||to_char(y)||';FG0C;SM1' );
      	   line := 'C;Y'|| to_char(y) || ';X2';
           line := line || ';K"'||ret.team_name||'"';
           p( line );
           y:=y+1;
      	   center_y := y;
      	elsif v_old_center <> ret.center_name and y > 5 then
	   --  Print sub totals for center
      	   DECLARE
      		 v_cell_totals  varchar2(300);
      	   BEGIN
      	     FOR j in team_total_tab.first..team_total_tab.last loop
      	   	  v_cell_totals :=  v_cell_totals||team_total_tab(j)||',';
      	     END LOOP;
             v_cell_totals := rtrim(v_cell_totals,',');
             p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Center '||v_old_center||' on '||to_char(p_start_date,'yyyy/mm/dd')||' THRU '||to_char(p_end_date,'yyyy/mm/dd')||'"';
             p( line );
             sub_totals_for_team_center (y , v_cell_totals);
       	     y:=y+1;
      	     p( 'F;R'||to_char(y)||';FG0L;SM1' );
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"'||ret.center_name||'"';
             p( line );
             team_total_tab := null_tab;
      	     y:=y+1;
      	     center_y := y;
      	     date_y := y;
      	   END;
      	end if;

        p( 'F;R'||to_char(y)||';FG0C;SM0' );
        line := 'C;Y'|| to_char(y) || ';X2';
        line := line || ';K"'||ret.queue_type||'"';
        p( line );
        line := 'C;Y'|| to_char(y) || ';X3';
        line := line || ';K"'||ret.tagged_on||'"';
        p( line );
        p( 'F;Y'||y||';X4;FG0C' );
        line := 'C;Y'|| to_char(y) || ';X4';
        line := line || ';K'||ret.Number_tagged;
        p( line );
        p( 'F;Y'||y||';X5;FG0C' );
        line := 'C;Y'|| to_char(y) || ';X5';
        line := line || ';K'||ret.Number_touched;
        p( line );
	p( 'F;Y'||y||';X6;FG0R' );
	line := 'F;P0;FG0G;Y'||to_char(y)||';X6';
	p( line );
	line := 'C;Y'|| to_char(y) || ';X6';
	line := line || ';K'||ret.avg_handle_time;
	p( line );
	y := y + 1;
	p( 'C;Y'||to_char(y) );

	if g_CLOB = 'N' then
	   utl_file.fflush( g_file );
	end if;

	v_old_team := ret.team_name;
	v_old_center := ret.center_name;
	v_old_date  := ret.tagged_on;

	END LOOP;


  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 5 then
	print_comment( 'No Rows Returned' );

	line := 'C;Y'|| to_char(y) || ';X2';
	line := line || ';K"No Data Found"';
	p( line );

	y := y + 1;
	p( 'C;Y'||to_char(y) );

	if g_CLOB = 'N' then
	   utl_file.fflush( g_file );
	end if;

  else
      --  Print sub totals for last date
           	 p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
              line := 'C;Y'|| to_char(y) || ';X2';
              line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
              p( line );
              sub_totals_for_date (y,date_y);
              date_total_tab(i) := 'R'||to_char(y)||'C';
              i := i+1;
      	      y:=y+1;
      	      date_y := y;
     -- Print totals for last Team
         	   DECLARE
      		    v_cell_totals  varchar2(300);
      	      BEGIN
      	        FOR j in date_total_tab.first..date_total_tab.last loop
      	   	     v_cell_totals :=  v_cell_totals||date_total_tab(j)||',';
      	        END LOOP;
      	        v_cell_totals := rtrim(v_cell_totals,',');
           	    p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
                line := 'C;Y'|| to_char(y) || ';X2';
                line := line || ';K"Totals for Team '||v_old_team||' for '||to_char(p_start_date,'yyyy/mm/dd')||' THRU '||to_char(p_end_date,'yyyy/mm/dd')||'"';
                p( line );
                sub_totals_for_team_center (y,v_cell_totals);
                date_total_tab := null_tab;
                team_total_tab(t) := 'R'||to_char(y)||'C';
                t := t+1;
                i := i+1;
      	        y:=y+1;
      	        date_y := y;
      	      end;
     -- Print total for last Center
         	   DECLARE
      		    v_cell_totals  varchar2(300);
      	      BEGIN
      	        FOR j in team_total_tab.first..team_total_tab.last loop
      	   	     v_cell_totals :=  v_cell_totals||team_total_tab(j)||',';
      	        END LOOP;
              v_cell_totals := rtrim(v_cell_totals,',');
        	    p( 'F;Y'||to_char(y)||';X2;FG0L;SM1' );
              line := 'C;Y'|| to_char(y) || ';X2';
              line := line || ';K"Totals for Center '||ret.center_name||' on '||to_char(p_start_date,'yyyy/mm/dd')||' and '||to_char(p_end_date,'yyyy/mm/dd')||'"';
              p( line );
              sub_totals_for_team_center (y,v_cell_totals);
              team_total_tab := null_tab;
      	      y:=y+1;
      	      date_y := y;
      	      p( 'F;R'||to_char(y)||';FG0L' );
      	      end;

  end if;

  close curs;
  return(y);

end print_rows;
--
--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure show	(p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_center_name in varchar2,
		 p_team_name in varchar2,
		 p_queue_type in varchar2,
		 p_submission_id in number,
		 p_CLOB in varchar2
		)
is
  v_start_date date;
  v_end_date date;
  l_row_cnt number(9);
  v_OUT_STATUS varchar2(30);
  v_OUT_MESSAGE varchar2(1000);

begin
    v_start_date := to_date(p_start_date,'mm/dd/yyyy');
    v_end_date   := to_date(p_end_date,'mm/dd/yyyy');

    if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
                                P_SUBMISSION_ID,
                                'SYS',
                                'S' ,
                                null ,
                                v_OUT_STATUS ,
                                v_OUT_MESSAGE
                        );
       commit;
       rpt.report_pkg.INSERT_REPORT_DETAIL_CHAR
       (
          P_SUBMISSION_ID,
          empty_clob(),--IN_REPORT_DETAIL
          v_out_status,
          v_out_message
        );
    else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','PRO05_Aging_By_Type.slk', 'W' );
    end if;

  if p_start_date is NULL then
     v_start_date := sysdate;
  else
     v_start_date := to_date(p_start_date, 'mm/dd/yyyy');
  end if;

  if p_end_date is NULL then
     v_end_date := sysdate;
  else
     v_end_date := to_date(p_end_date, 'mm/dd/yyyy');
  end if;

  print_title(	v_start_date,
		v_end_date);

  print_headings(2);

  l_row_cnt := print_rows
			(v_start_date,
			 v_end_date,
			 p_center_name,
			 p_team_name,
			 p_queue_type
			);

  print_widths;

  p( 'E' );

    if p_CLOB = 'Y' then
	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
		(
		P_SUBMISSION_ID,
		'SYS',
		'C' ,
		null ,
		v_OUT_STATUS ,
		v_OUT_MESSAGE
		);
	commit;
    else
	utl_file.fflush( g_file );
	utl_file.fclose( g_file );
    end if;

end show;

END;
.
/
