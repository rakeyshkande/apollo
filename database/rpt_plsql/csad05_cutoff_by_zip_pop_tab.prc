CREATE OR REPLACE
procedure rpt.csad05_cutoff_by_zip_pop_tab

(p_cutoff in varchar2:=null,
 p_state  in varchar2)

is

begin

	insert into rpt.rep_tab (col1,
			     col2,
			     col3,
			     col4,
			     col5,
			     col6)
	select fazc.zip_code_id zip_code,
             fazc.city city,
             fazc.state_id state,
             fafz.cutoff_time,
             count(fafz.florist_id) florist_cutoff,
             sub.florist_zip
from ftd_apps.florist_zips fafz,
          ftd_apps.zip_code fazc,
  (select fazc.zip_code_id zip_code,
             fazc.city city,
             fazc.state_id state,
             count(fafz.florist_id) florist_zip
            from ftd_apps.florist_zips fafz,
                        ftd_apps.zip_code fazc
            where fafz.zip_code=fazc.zip_code_id
              and fazc.state_id = p_state
                      group by fazc.zip_code_id,
                         fazc.city,
                        fazc.state_id) sub,
	ftd_apps.state_master fasm,
	ftd_apps.florist_master fafm
where fafz.zip_code = fazc.zip_code_id
and fazc.state_id=fasm.state_master_id
and fafz.florist_id=fafm.florist_id
and fazc.state_id = p_state
and fafm.status='Active'
and fafz.cutoff_time=nvl(p_cutoff,fafz.cutoff_time)
and fazc.city = sub.city
and  fazc.state_id = sub.state
and sub.zip_code = fazc.zip_code_id
group by fafz.cutoff_time,
              fazc.zip_code_id,
             fazc.city,
             fazc.state_id,
             sub.florist_zip,
	fasm.country_code
order by fafz.cutoff_time,
	fasm.country_code desc,
	fazc.zip_code_id;
end;
.
/
