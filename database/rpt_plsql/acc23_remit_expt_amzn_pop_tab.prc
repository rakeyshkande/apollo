CREATE OR REPLACE
PROCEDURE rpt.acc23_remit_expt_amzn_pop_tab
      (p_tolerance in number,
p_start_date in varchar2,
p_end_date in varchar2)
IS
--
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
BEGIN
insert into rpt.rep_tab (col1,col2,col3,col4,col5,col6,col7,col8,col9)
select clean.order_number,
             clean.transaction_amount,
             clean.wholesale_transaction_amount,
             asor.az_order_id                  amazon_order_id,
             aaor.az_order_related_id          ftd_adj_id,
             asd.processed_date                Settlement_date,
            (    nvl(asai.item_price,0) +
                 nvl(asai.tax,0) +
                 nvl(asai.shipping,0) +
                 nvl(asai.shipping_tax,0) +
                 nvl(asai.commission,0))       Settlement_amount,
            asd.az_settlement_data_id       settlement_number ,
             (    nvl(asai.item_price,0) +
                  nvl(asai.tax,0) +
                  nvl(asai.shipping,0) +
                  nvl(asai.shipping_tax,0))-
                  nvl(clean.wholesale_transaction_amount,0)   diff
from amazon.settlement_adjustment_item asai
join AMAZON.AZ_ORDER_DETAIL aaod
	on (aaod.AZ_ORDER_ITEM_NUMBER = asai.AMAZON_ORDER_ITEM_CODE)
join (select order_number,
             sum(transaction_amount) transaction_amount,
             sum(wholesale_transaction_amount) wholesale_transaction_amount
        from
                (select cod.external_order_number                     order_number,
                (nvl(cat.product_amount,0) +
                 nvl(cat.tax,0) +
                 nvl(cat.shipping_fee,0) +
                 nvl(cat.shipping_tax,0) +
                 nvl(cat.service_fee,0) +
                 nvl(cat.service_fee_tax,0) +
                 nvl(cat.admin_fee,0) +
                 nvl(cat.add_on_amount,0) -
                 nvl(cat.discount_amount,0))   Transaction_amount,
                 cat.wholesale_amount              wholesale_transaction_amount
                  from clean.order_details cod
                  join clean.accounting_transactions cat
	             on (cat.order_detail_id = cod.order_detail_id))
                 group by order_number) clean
	on (aaod.confirmation_number  = clean.order_number)
right outer join      amazon.settlement_order asor
	on (asai.settlement_detail_id = asor.settlement_detail_id)
join     amazon.az_order_related aaor
	on (asai.confirmation_number = aaor.confirmation_number)
join amazon.settlement_detail asdl
	on (asdl.settlement_detail_id = asor.settlement_detail_id)
join amazon.settlement_data asd
	on (asd.AZ_SETTLEMENT_DATA_ID=asdl.AZ_SETTLEMENT_DATA_ID)
where abs((nvl(asai.item_price,0) +
             nvl(asai.tax,0) +
             nvl(asai.shipping,0) +
             nvl(asai.shipping_tax,0) +
             nvl(asai.commission,0))-nvl(clean.wholesale_transaction_amount,0))  > p_tolerance
and aaor.type_feed='OrderAcknowledgement'
and trunc(asd.processed_date) between v_start_date and v_end_date
--and (trunc(asd.processed_date) between v_start_date and v_end_date
--or asd.az_settlement_data_id in 	(select asdl1.az_settlement_data_id
--					from amazon.settlement_detail asdl1,
--					     amazon.settlement_data asd1
--					where asd1.AZ_SETTLEMENT_DATA_ID=asdl1.AZ_SETTLEMENT_DATA_ID
--					and asd.processed_date is null))
union
select clean.order_number,
             clean.transaction_amount,
             clean.wholesale_transaction_amount,
             asor.az_order_id                  amazon_order_id,
             aaor.az_order_related_id          ftd_adj_id,
             asd.processed_date                Settlement_date,
            (    nvl(asoi.item_price,0) +
                 nvl(asoi.tax,0) +
                 nvl(asoi.shipping,0) +
                 nvl(asoi.shipping_tax,0) +
                 nvl(asoi.commission,0))       Settlement_amount,
            asd.az_settlement_data_id        settlement_number ,
             (   nvl(asoi.item_price,0) +
                 nvl(asoi.tax,0) +
                 nvl(asoi.shipping,0) +
                 nvl(asoi.shipping_tax,0) +
                 nvl(asoi.commission,0))-
                 nvl(clean.wholesale_transaction_amount,0)   diff
from amazon.settlement_order_item asoi
join AMAZON.AZ_ORDER_DETAIL aaod
	on (aaod.AZ_ORDER_ITEM_NUMBER = asoi.AMAZON_ORDER_ITEM_CODE)
left join (select order_number,
             sum(transaction_amount) transaction_amount,
             sum(wholesale_transaction_amount) wholesale_transaction_amount
        from
                (select cod.external_order_number                     order_number,
                (nvl(cat.product_amount,0) +
                 nvl(cat.tax,0) +
                 nvl(cat.shipping_fee,0) +
                 nvl(cat.shipping_tax,0) +
                 nvl(cat.service_fee,0) +
                 nvl(cat.service_fee_tax,0) +
                 nvl(cat.admin_fee,0) +
                 nvl(cat.add_on_amount,0) -
                 nvl(cat.discount_amount,0))   Transaction_amount,
                 cat.wholesale_amount              wholesale_transaction_amount
                  from clean.order_details cod
                  join clean.accounting_transactions cat
	             on (cat.order_detail_id = cod.order_detail_id))
                 group by order_number) clean
	on (aaod.confirmation_number  = clean.order_number)
right outer join      amazon.settlement_order asor
	on (asoi.settlement_detail_id = asor.settlement_detail_id)
join     amazon.az_order_related aaor
	on (asoi.confirmation_number = aaor.confirmation_number)
join amazon.settlement_detail asdl
	on (asdl.settlement_detail_id = asor.settlement_detail_id)
join amazon.settlement_data asd
	on (asd.AZ_SETTLEMENT_DATA_ID=asdl.AZ_SETTLEMENT_DATA_ID)
where abs((nvl(asoi.item_price,0) +
             nvl(asoi.tax,0) +
             nvl(asoi.shipping,0) +
             nvl(asoi.shipping_tax,0) +
             nvl(asoi.commission,0))-
             nvl(clean.wholesale_transaction_amount,0))  > p_tolerance
and aaor.type_feed='OrderAcknowledgement'
and trunc(asd.processed_date) between v_start_date and v_end_date
--and (trunc(asd.processed_date) between v_start_date and v_end_date
--or asd.az_settlement_data_id in 	(select asdl1.az_settlement_data_id
--					from amazon.settlement_detail asdl1,
--					     amazon.settlement_data asd1
--					where asd1.AZ_SETTLEMENT_DATA_ID=asdl1.AZ_SETTLEMENT_DATA_ID
--					and asd.processed_date is null))
;
END;
.
/
