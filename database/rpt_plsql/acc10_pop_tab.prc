CREATE OR REPLACE
PROCEDURE rpt.acc10_pop_tab (p_run_date in varchar2)
IS
BEGIN
--
--**********************************************************************
-- Orders and Refunds
--**********************************************************************
--
INSERT into RPT.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5
	)
SELECT	source_code source_code,
	description description,
	sum(nvl(prev_month_invoice_amt,0)) prev_month_invoice_amt,
	sum(nvl(curr_month_invoice_amt,0)) curr_month_invoice_amt,
	sum(nvl(curr_month_invoice_amt,0)) + sum(nvl(prev_month_invoice_amt,0))  net_month_invoice_amt
--
FROM	(
	SELECT	'CURRENT ORDERS' month_type,
		fas.source_code source_code,
		fas.description description,
		0 prev_month_invoice_amt,
		sum(nvl((cat.product_amount +
			 cat.add_on_amount +
			 decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee) +
			 cat.shipping_fee +
			 cat.shipping_tax +
			 cat.tax -
			 cat.discount_amount), 0)) curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN	clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cob.order_bill_id=cat.order_bill_id)
	JOIN	ftd_apps.source fas on (fas.source_code = cod.source_code)
	--
	WHERE	cp.payment_type = 'IN'
	AND	cob.bill_status in ('Billed','Settled')
	AND 	cp.payment_indicator='P'
	AND	cat.transaction_type <> 'Refund'
	AND	trunc(cob.bill_date) between trunc(to_date(p_run_date),'month')
					 and last_day(to_date(p_run_date))
	AND 	trunc(co.order_date) between trunc(to_date(p_run_date),'month')
					 and last_day(to_date(p_run_date))
	--
	GROUP BY
		fas.source_code,
		fas.description
	--
	UNION ALL
	--
	SELECT	'CURRENT REFUNDS' month_type,
		fas.source_code source_code,
		fas.description description,
		0 prev_month_invoice_amt,
		sum(nvl((cat.product_amount +
			 cat.add_on_amount +
			 decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee) +
			 cat.shipping_fee +
			 cat.shipping_tax +
			 cat.tax -
			 cat.discount_amount), 0)) curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN    clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN    clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN    clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
	JOIN    ftd_apps.source fas on (fas.source_code = cod.source_code)
	JOIN    clean.refund cr on (cr.order_detail_id = cat.order_detail_id and cr.refund_id=cat.refund_id and cr.refund_id=cp.refund_id)
	--
	WHERE	cp.payment_type = 'IN'
	AND	cr.refund_status = 'Billed'
	AND 	cp.payment_indicator='R'
	AND	cat.transaction_type in ('Refund')
	AND	trunc(cr.refund_date) between trunc(to_date(p_run_date),'month')
					  and last_day(to_date(p_run_date))
	AND 	trunc(co.order_date) between trunc(to_date(p_run_date),'month')
					 and last_day(to_date(p_run_date))
	--
	GROUP BY
		fas.source_code,
		fas.description
	--
	UNION ALL
	--
	SELECT	'PRIOR ORDERS' month_type,
		fas.source_code source_code,
		fas.description description,
		sum(nvl((cat.product_amount +
			 cat.add_on_amount +
			 decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee) +
			 cat.shipping_fee +
			 cat.shipping_tax +
			 cat.tax -
			 cat.discount_amount), 0)) prev_month_invoice_amt,
			 0 curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN	clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cob.order_bill_id=cat.order_bill_id)
	JOIN	ftd_apps.source fas on (fas.source_code = co.source_code)
	--
	WHERE	cp.payment_type = 'IN'
	AND	cob.bill_status in ('Billed','Settled')
	AND 	cp.payment_indicator='P'
	AND	cat.transaction_type = 'Add Bill'
	AND	trunc(cob.bill_date) between trunc(to_date(p_run_date),'month')
					 and last_day(to_date(p_run_date))
	AND 	trunc(co.order_date) between add_months(trunc(to_date(p_run_date),'MONTH'),-1)
					 and last_day(add_months(to_date(p_run_date),-1))
	--
	GROUP BY
		fas.source_code,
		fas.description
	--
	UNION ALL
	--
	SELECT	'PRIOR REFUNDS' month_type,
		fas.source_code source_code,
		fas.description description,
		sum(nvl((cat.product_amount +
			 cat.add_on_amount +
			 cat.service_fee +
			 decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee) +
			 cat.shipping_tax +
			 cat.tax -
			 cat.discount_amount), 0)) prev_month_invoice_amt,
			 0 curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
	JOIN	ftd_apps.source fas on (fas.source_code = co.source_code)
	JOIN	clean.refund cr on (cr.order_detail_id = cat.order_detail_id and cr.refund_id=cat.refund_id and cr.refund_id=cp.refund_id)
	--
	WHERE	cp.payment_type = 'IN'
	AND 	cp.payment_indicator='R'
	AND	cr.refund_status = 'Billed'
	AND	cat.transaction_type in ('Refund')
	AND	trunc(cr.refund_date) between trunc(to_date(p_run_date),'month')
					 and last_day(to_date(p_run_date))
	AND 	trunc(co.order_date) between add_months(trunc(to_date(p_run_date),'MONTH'),-1)
					  and last_day(add_months(to_date(p_run_date),-1))
	--
	GROUP BY
		fas.source_code,
		fas.description
	)
GROUP BY source_code,
         description
;
END
;
.
/
