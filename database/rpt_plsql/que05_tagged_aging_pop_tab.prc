CREATE OR REPLACE
PROCEDURE rpt.que05_tagged_aging_pop_tab
	(p_user_id		in varchar2,
	 p_center_name		in varchar2,
	 p_queue_type	in varchar2,
	 p_team_name		in varchar2,
	p_start_date		in varchar2,
	p_end_date		in varchar2
	)
IS
	v_user_id_predicate	varchar2(100);
	v_center_predicate	varchar2(100);
	v_queue_type_predicate	varchar2(100);
	v_team_predicate	varchar2(100);
	v_sql			varchar2(5000);
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');

BEGIN
--
--******************************************************************************
--

if p_user_id <> 'ALL' then
	v_user_id_predicate:= ' and cqt.csr_id in ('||rpt.multi_value(p_user_id)||') ';
end if;

if p_center_name <> 'ALL' then
	v_center_predicate:= ' AND facc.call_center_id in ('||rpt.multi_value(p_center_name)||') ';
end if;

if p_queue_type <> 'ALL' then
	v_queue_type_predicate:= ' AND cq.queue_type in ('||rpt.multi_value(p_queue_type)||') ';
end if;

if p_team_name <> 'ALL' then
	v_team_predicate:= ' AND facg.cs_group_id in ('||rpt.multi_value(p_team_name)||') ';
end if;

v_sql := 'insert into rpt.rep_tab
		(col1,
		col2,
		col3,
		col4,
		col5,
		col6,
		col7,
		col8,
		col9,
		col10,
		col11,
		col12
		)
	  select nvl(facc.description,''Unknown'') center_name,
            cqt.csr_id user_id,
            au.first_name||'' ''||au.last_name user_name,
            au.first_name||'' ''||au.last_name name,
            cq.external_order_number order_number,
            cod.delivery_date,
            cq.queue_type,
            cqt.tag_disposition disposition,
            cqt.tag_priority priority,
            cqt.tagged_on date_tagged,
            max(ccve1.updated_on) last_touched,
            round(max(ccve1.updated_on)-cqt.tagged_on) elapsed_days
           -- case when (ccve2.updated_on>ccve1.updated_on)
	--	then ccve2.updated_on
	--	else ccve1.updated_on
           -- end last_touched,
           -- case when (ccve2.updated_on>ccve1.updated_on)
	--	then round(ccve2.updated_on-cqt.tagged_on,2)
	--	else round(ccve1.updated_on-cqt.tagged_on,2)
           -- end elapsed_days
from clean.queue cq
join   clean.queue_tag cqt
	on (cq.message_id=cqt.message_id)
left outer join   clean.order_details cod
	on (cq.order_detail_id=cod.order_detail_id)
join   aas.identity ai
	on (cqt.csr_id=ai.identity_id)
join   aas.users au
	on (ai.user_id=au.user_id)
left outer join ftd_apps.call_center facc
	on (au.call_center_id=facc.call_center_id)
left outer join ftd_apps.cs_groups facg
	on (au.cs_group_id=facg.cs_group_id)
left outer join  clean.csr_viewed_entities ccve1
	on (ccve1.entity_type=''ORDER_DETAILS''
	     and cq.order_detail_id=ccve1.entity_id and cqt.tagged_on<ccve1.updated_on)
--left outer join  clean.csr_viewed_entities ccve2
--	on (ccve1.entity_type=''ORDERS''
--	     and cq.order_guid=ccve2.entity_id and cqt.tagged_on<ccve2.updated_on)
where trunc(cqt.tagged_on) between '''|| v_start_date ||''' and '''|| v_end_date ||''''
|| v_user_id_predicate
|| v_center_predicate
|| v_queue_type_predicate
|| v_team_predicate
||' group by nvl(facc.description,''Unknown''),
            cqt.csr_id,
            au.first_name||'' ''||au.last_name,
            au.first_name||'' ''||au.last_name,
            cq.external_order_number,
            cod.delivery_date,
            cq.queue_type,
            cqt.tag_disposition,
            cqt.tag_priority,
            cqt.tagged_on'
;
execute immediate v_sql
;
end
;
.
/
