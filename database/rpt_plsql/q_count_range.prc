CREATE OR REPLACE
PROCEDURE rpt.q_count_range
(
 START_DATE      IN DATE,
 END_DATE        IN DATE  ,
 OUT_STATUS  OUT VARCHAR2,
  OUT_MESSAGE OUT VARCHAR2
)


AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table clean_hourly_stats.

Input:
   start_date
   end_date


Output:
        status          VARCHAR2
        message         VARCHAR2

-----------------------------------------------------------------------------*/

cur_date DATE;





BEGIN
  out_status :='Y';
  cur_date := start_date;

  WHILE cur_date <= end_date LOOP
      q_counts(cur_date,out_status,out_message);
      cur_date := cur_date + 1;
  END LOOP;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;


END q_count_range;
.
/
