CREATE OR REPLACE
PACKAGE BODY rpt.PRO06_QUEUE_DELETE_DETAIL IS

  g_data_sw		varchar2(1) := 'Y';
  g_count		number  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure print_line( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
	rpt.report_pkg.UPDATE_REPORT_DETAIL_CHAR
		(
		g_SUBMISSION_ID,
		p_str||chr(10),
		v_OUT_STATUS_det,
		v_OUT_MESSAGE_det
		);
	if v_out_status_det = 'N' then
		rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
		(
		g_SUBMISSION_ID,
		'REPORT',
		'E',
		v_OUT_MESSAGE_det,
		v_OUT_STATUS_sub,
		v_OUT_MESSAGE_sub
		);
	end if;
  end if;

end;

--****************************************************************************************************
--****************************************************************************************************

procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  print_line( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;

--****************************************************************************************************
--****************************************************************************************************

procedure print_headings(y in number)
is
begin

  print_comment( 'Print Headings' );

  print_line( 'F;R'||y||';FG0L;SM1' );
  print_line( 'F;Y'||to_char(y)||';X7;FG0R;SM1' );
  print_line( 'F;Y'||to_char(y)||';X8;FG0R;SM1' );

  print_line( 'C;Y'||y||';X2;K"Queue Type"' );
  print_line( 'C;Y'||y||';X3;K"Order #"' );
  print_line( 'C;Y'||y||';X4;K"Received Date"' );
  print_line( 'C;Y'||y||';X5;K"Tagged Date"' );
  print_line( 'C;Y'||y||';X6;K"Deleted Date"' );
  print_line( 'C;Y'||y||';X7;K"Tagged Time"' );
  print_line( 'C;Y'||y||';X8;K"# of Touches"' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_headings;

--****************************************************************************************************
--****************************************************************************************************

procedure print_title(p_deleted_date in date)
is
begin

  print_comment( 'Print Title' );

  print_line( 'ID;ORACLE' );

  print_line( 'P;Ph:mm:ss');                 -- SM0
  print_line( 'P;FCourier;M200' );           -- SM1
  print_line( 'P;FCourier;M200;SB' );        -- SM2
  print_line( 'P;FCourier;M200;SUB' );       -- SM3
  print_line( 'P;FCourier;M160' );           -- SM4

  print_line( 'F;R1;FG0L;SM2' );
  print_line( 'C;Y1;X2;K"Queue Delete Detail Report For ' || p_deleted_date || '"' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_title;

--****************************************************************************************************
--****************************************************************************************************

function print_rows(p_deleted_date in date, p_queue_type in varchar2 ) return number
is

  line    varchar2(32767) := null;
  y       number          := 4;

  type    my_curs_type is REF CURSOR;
  curs    my_curs_type;
  str     varchar2(5000);

  type    ret_rec is record
               (queue_type varchar2(80),
		order_number varchar2(80),
		received_date varchar2(80),
		tagged_date varchar2(80),
		deleted_date varchar2(80),
		tagged_time number,
		number_of_touches varchar2(80)
		);
  ret     ret_rec;

  v_queue_type_pred varchar2(300);

begin

  -----------------------------------------------------
  --  Building the predicates (Where clause conditions)
  -----------------------------------------------------

  print_comment( 'Build Parameter Where Clause Predicates' );

  if p_queue_type is null
  or p_queue_type = 'ALL' then
     v_queue_type_pred := null;
  else
     v_queue_type_pred:= ' and upper(cqdh.queue_type) in ('||rpt.multi_value(p_queue_type)||') ';
  end if;

  --DBMS_OUTPUT.PUT_LINE( 'Queue:   ' || v_queue_type_pred);

  -----------------------------------------------------
  --  Define the SELECT
  -----------------------------------------------------
  str := 'select   nvl(cqdh.queue_type, '' '') queue_type,
		   nvl(cqdh.external_order_number, '' '') order_number,
		   to_char(cqdh.queue_created_on, ''MM/DD/RRRR HH:MI AM'') received_date,
		   to_char(cqdh.tagged_on, ''MM/DD/RRRR HH:MI AM'') tagged_date,
		   to_char(cqdh.queue_deleted_on, ''MM/DD/RRRR HH:MI AM'') deleted_date,
		   cqdh.queue_deleted_on-cqdh.tagged_on tagged_time,
		   count(ceh.entity_id) number_of_touches
	  --
	  from		  clean.queue_delete_history cqdh
	  join		  clean.orders co
				on (cqdh.order_guid = co.order_guid)
	  left outer join clean.entity_history ceh
				on (cqdh.order_guid = ceh.entity_id)
	  --
	  where	   trunc(cqdh.queue_deleted_on) = ''' || p_deleted_date || ''' '
			|| v_queue_type_pred ||
	        ' group by cqdh.queue_type,
	                   cqdh.external_order_number,
	                   cqdh.queue_created_on,
	                   cqdh.tagged_on,
	                   cqdh.queue_deleted_on '
          ;

  -----------------------------------------------------
  --  Process the Cursor
  -----------------------------------------------------
  OPEN curs FOR str;
       LOOP
       	  FETCH curs INTO ret;
     	    EXIT WHEN curs%NOTFOUND;

          line := 'C;Y'|| to_char(y) || ';X2';
          line := line || ';K"'||ret.queue_type||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X3';
          line := line || ';K"'||ret.order_number||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X4';
          line := line || ';K"'||ret.received_date||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X5';
          line := line || ';K"'||ret.tagged_date||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X6';
          line := line || ';K"'||ret.deleted_date||'"';
          print_line( line );

          print_line( 'F;P0;FG0G'||to_char(y)||';X7' );
          line := 'C;Y'|| to_char(y) || ';X7';
          line := line || ';K'||ret.tagged_time;
          print_line( line );

	  print_line( 'F;Y'||to_char(y)||';X8;FG0R;SM0' );
          line := 'C;Y'|| to_char(y) || ';X8';
          line := line || ';K"'||ret.number_of_touches||'"';
          print_line( line );

          y := y + 1;
          print_line( 'C;Y'||to_char(y) );

	  if g_CLOB = 'N' then
	     utl_file.fflush( g_file );
	  end if;
       END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 4 then
     g_data_sw := 'N';

     print_comment( 'No Rows Returned' );

     line := 'C;Y'|| to_char(y) || ';X2';
     line := line || ';K"No Data Found"';
     print_line( line );

     y := y + 1;
     print_line( 'C;Y'||to_char(y) );

     if g_CLOB = 'N' then
        utl_file.fflush( g_file );
     end if;

  end if;

  --**************************************************************************
  --Print report totals
  --**************************************************************************
  if g_data_sw = 'Y' then
     print_line( 'F;Y'||to_char(y)||';X7;FG0R;SM1' );
     line := 'C;Y'|| to_char(y) || ';X7';
     line := line || ';K"Total Number of Items in Queue"';
     print_line( line );

     print_line( 'F;Y'||to_char(y)||';X8;FG0R;SM1' );
     line := 'C;Y'|| to_char(y) || ';X8';
     line := line || ';ECOUNTA(R4C7:R' ||to_char(y-1) || 'C7)';
     print_line( line );

     y := y + 1;

     print_line( 'F;Y'||to_char(y)||';X7;FG0R;SM1' );
     line := 'C;Y'|| to_char(y) || ';X7';
     line := line || ';K"Total Tagged Time"';
     print_line( line );

     print_line( 'F;Y'||to_char(y)||';X8;FG0R;SM1' );
     line := 'C;Y'|| to_char(y) || ';X8';
     line := line || ';ESUM(R4C7:R' ||to_char(y-2) || 'C7)';
     print_line( line );

     y := y + 1;

     print_line( 'F;Y'||to_char(y)||';X7;FG0R;SM1' );
     line := 'C;Y'|| to_char(y) || ';X7';
     line := line || ';K"Average Tagged Time"';
     print_line( line );

     print_line( 'F;Y'||to_char(y)||';X8;FG0R;SM1' );
     line := 'C;Y'|| to_char(y) || ';X8';
     line := line || ';EAVERAGE(R4C7:R' ||to_char(y-3) || 'C7)';
     print_line( line );

     y := y + 1;

  end if;

  --**************************************************************************
  --Ceanup Stuff - Close Cursor and Return
  --**************************************************************************
  CLOSE curs;

  RETURN (y);

end print_rows;

--****************************************************************************************************
--****************************************************************************************************

procedure print_widths
is
begin

  print_comment( 'Format Column Widths' );

  print_line( 'F;W1 1 5' );       -- margin
  print_line( 'F;W2 2 15' );      -- queue type
  print_line( 'F;W3 3 15' );      -- order number
  print_line( 'F;W4 4 25' );      -- received date
  print_line( 'F;W5 5 25' );		  -- tagged date
  print_line( 'F;W6 6 25' );		  -- deleted date
  print_line( 'F;W7 7 15' );	    -- taggged time
  print_line( 'F;W8 8 15' ); 		  -- number of touches
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_widths;

--****************************************************************************************************
--****************************************************************************************************

procedure run_report(p_deleted_date in varchar2,
		     p_queue_type in varchar2,
		     p_submission_id in number,
		     p_CLOB in varchar2
		    )
is
  v_deleted_date date;
  l_row_cnt number;
  v_OUT_STATUS varchar2(30);
  v_OUT_MESSAGE varchar2(1000);

begin
    v_deleted_date := to_date(p_deleted_date,'mm/dd/yyyy');

    if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
                                P_SUBMISSION_ID,
                                'SYS',
                                'S' ,
                                null ,
                                v_OUT_STATUS ,
                                v_OUT_MESSAGE
                        );
       commit;
       rpt.report_pkg.INSERT_REPORT_DETAIL_CHAR
       (
          P_SUBMISSION_ID,
          empty_clob(),--IN_REPORT_DETAIL
          v_out_status,
          v_out_message
        );
    else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','PRO06_Queue_Delete_Detail.slk', 'W' );
    end if;

  if p_deleted_date is NULL then
     v_deleted_date := sysdate;
  else
     v_deleted_date := to_date(p_deleted_date, 'mm/dd/yyyy');
  end if;

  print_title(v_deleted_date);

  print_headings(3);

  l_row_cnt := print_rows
			(v_deleted_date,
			 p_queue_type
			);

  print_widths;

  print_line( 'E' );

    if p_CLOB = 'Y' then
	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
		(
		P_SUBMISSION_ID,
		'SYS',
		'C' ,
		null ,
		v_OUT_STATUS ,
		v_OUT_MESSAGE
		);
	commit;
    else
	utl_file.fflush( g_file );
	utl_file.fclose( g_file );
    end if;

end run_report;

end;
.
/
