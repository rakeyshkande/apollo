CREATE OR REPLACE
PROCEDURE rpt.acc13_comp_orders_pop_tab
		(p_start_date in varchar2,
		 p_end_date in varchar2)

IS
  v_start_date date	:= to_date(p_start_date,'mm/dd/yyyy');
  v_end_date date	:= to_date(p_end_date,'mm/dd/yyyy');

BEGIN
  insert into rpt.rep_tab (col1,col2,col3,col4,col5,col6,col7, col8,col9)

  select case cp.payment_type
		when 'NC' then 'NC'
		else null end NC,
         cod.source_code source_code,
         cod.external_order_number order_number,
         co.order_date order_date,
         cc.first_name||' '||last_name sender,
         mm.product_id,
         cp.payment_type no_charge_payment,
         mm.price Mercury_value,
         0 Pay_vendor_amount
  from clean.payments cp
  join clean.orders co
	on (co.order_guid = cp.order_guid)
	and cp.payment_type<>'GC'
  join clean.order_details  cod
	on (cod.order_guid = co.order_guid)
  join mercury.mercury mm
	on(mm.reference_number = TO_char(cod.order_detail_id))
join
(select reference_number,max(transmission_time) transmission_time
        from
        (select mercury_order_number,reference_number, mercury_id, transmission_time
         from mercury.mercury mm3
         where mm3.message_direction='OUTBOUND'
         and mm3.mercury_order_number is not null
         and mm3.reference_number is not null
         and mm3.price is not null
         and mm3.price <> 0
         and mm3.msg_type ='FTD'
         and NVL(mm3.comp_order, 'X') = 'C'
         and not exists
            (select 1
            from mercury.mercury mm2
            where mm2.msg_type in ('CAN','REJ')
            and mm2.mercury_order_number=mm3.mercury_order_number)
            )
        group by reference_number) mm2
  on mm.reference_number=mm2.reference_number
  and mm.transmission_time=mm2.transmission_time
  join clean.customer cc
	on(cc.customer_id = co.customer_id)
  where order_disp_code not  in ('In-Scrub','Removed','Pending')
  and cp.payment_indicator='P'
  and cod.eod_delivery_indicator = 'Y'
  and trunc(cod.eod_delivery_date) between v_start_date and v_end_date
  --
union
  --
  select cp.payment_type NC,
         cod.source_code source_code,
         cod.external_order_number order_number,
         co.order_date order_date,
         cc.first_name||' '||last_name sender,
         mm.product_id,
         cp.payment_type no_charge_payment,
         mm.price Mercury_value,
         0 Pay_vendor_amount
  from clean.payments cp
  join clean.orders co
	on (co.order_guid = cp.order_guid)
	and cp.payment_type='NC'
  join clean.order_details  cod
	on (cod.order_guid = co.order_guid)
  join mercury.mercury mm
	on(mm.reference_number = TO_char(cod.order_detail_id))
join
(select reference_number,max(transmission_time) transmission_time
        from
        (select mercury_order_number,reference_number, mercury_id, transmission_time
         from mercury.mercury mm3
         where mm3.message_direction='OUTBOUND'
         and mm3.mercury_order_number is not null
         and mm3.reference_number is not null
         and mm3.price is not null
         and mm3.price <> 0
         and mm3.msg_type ='FTD'
	 and NVL(mm3.comp_order, 'X') <> 'C'
         and not exists
            (select 1
            from mercury.mercury mm2
            where mm2.msg_type in ('CAN','REJ')
            and mm2.mercury_order_number=mm3.mercury_order_number)
            )
        group by reference_number) mm2
  on mm.reference_number=mm2.reference_number
  and mm.transmission_time=mm2.transmission_time
  join clean.customer cc
	on(cc.customer_id = co.customer_id)
  where order_disp_code not  in ('In-Scrub','Removed','Pending')
  and cp.payment_indicator='P'
  and cod.eod_delivery_indicator = 'Y'
  and trunc(cod.eod_delivery_date) between v_start_date and v_end_date
  UNION ALL
  --
  select case cp.payment_type when 'NC' then 'NC'
                            else null
                            end NC,
           cod.source_code source_code,
            cod.external_order_number order_number,
           co.order_date order_date,
           cc.first_name||' '||last_name sender,
            vv.product_id,
            cp.payment_type no_charge_payment,
          0,
           vv.price Pay_vendor_amount
  from clean.payments cp
  join clean.orders co
	on (co.order_guid = cp.order_guid)
	and cp.payment_type<>'GC'
  join clean.order_details  cod
	on (cod.order_guid = co.order_guid)
  join venus.venus vv
	on(vv.reference_number = TO_char(cod.order_detail_id))
  join
  (select reference_number,max(transmission_time) transmission_time
        from
        (select venus_order_number,reference_number, venus_id, transmission_time
         from venus.venus vv3
         where vv3.message_direction='OUTBOUND'
         and vv3.venus_order_number is not null
         and vv3.reference_number is not null
         and vv3.price is not null
         and vv3.price <> 0
         and vv3.msg_type ='FTD'
         and NVL(vv3.comp_order, 'X') = 'C'
         and not exists
            (select 1
            from venus.venus vv2
            where vv2.msg_type in ('CAN','REJ')
            and vv2.venus_order_number=vv3.venus_order_number)
            )
        group by reference_number) vv2
  on vv.reference_number=vv2.reference_number
  and vv.transmission_time=vv2.transmission_time
  join clean.customer cc
	on(cc.customer_id = co.customer_id)
  where order_disp_code not in ('In-Scrub','Removed','Pending')
  and cp.payment_indicator='P'
  and cod.eod_delivery_indicator = 'Y'
  and trunc(cod.eod_delivery_date) between v_start_date and v_end_date
--
union
--
  select cp.payment_type NC,
           cod.source_code source_code,
            cod.external_order_number order_number,
           co.order_date order_date,
           cc.first_name||' '||last_name sender,
            vv.product_id,
            cp.payment_type no_charge_payment,
          0,
           vv.price Pay_vendor_amount
  from clean.payments cp
  join clean.orders co
	on (co.order_guid = cp.order_guid)
	and cp.payment_type='NC'
  join clean.order_details  cod
	on (cod.order_guid = co.order_guid)
  join venus.venus vv
	on(vv.reference_number = TO_char(cod.order_detail_id))
  join
  (select reference_number,max(transmission_time) transmission_time
        from
        (select venus_order_number,reference_number, venus_id, transmission_time
         from venus.venus vv3
         where vv3.message_direction='OUTBOUND'
         and vv3.venus_order_number is not null
         and vv3.reference_number is not null
         and vv3.price is not null
         and vv3.price <> 0
         and vv3.msg_type ='FTD'
	 and NVL(vv3.comp_order, 'X') <> 'C'
         and not exists
            (select 1
            from venus.venus vv2
            where vv2.msg_type in ('CAN','REJ')
            and vv2.venus_order_number=vv3.venus_order_number)
            )
        group by reference_number) vv2
  on vv.reference_number=vv2.reference_number
  and vv.transmission_time=vv2.transmission_time
  join clean.customer cc
	on(cc.customer_id = co.customer_id)
  where order_disp_code not in ('In-Scrub','Removed','Pending')
  and cp.payment_indicator='P'
  and cod.eod_delivery_indicator = 'Y'
  and trunc(cod.eod_delivery_date) between v_start_date and v_end_date
  ;
END
;
.
/
