create or replace PROCEDURE rpt.acc04_refund_summary_pop_tab
			(p_start_date in varchar2,
			 p_end_date in varchar2,
			 p_company_code in varchar2,
			 p_origin_id in varchar2
			)
IS
  v_start_date		date := to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		date := to_date(p_end_date,'mm/dd/yyyy');
  v_company_predicate	varchar2(100);
  v_origin_predicate	varchar2(100);
  v_sql			varchar2(30000);

BEGIN

--
--**********************************************************************
-- Set the company code and origin id WHERE clause predicates
--**********************************************************************
--
if p_company_code<>'ALL' then
   v_company_predicate :=
		' AND co.company_id in ('||rpt.multi_value(p_company_code)||') ';
end if;

if p_origin_id <>'ALL' then
   v_origin_predicate :=
		' AND nvl(co.order_taken_call_center,co.origin_id) in ('||rpt.multi_value(p_origin_id)||') ';
end if;

--
--**********************************************************************
-- Florist Refunds
--**********************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15,
	col16,
	col17,
	col18,
	col19
	)
SELECT	''1'' Query_Id,
	FTD_ship_vendor_type,
	FTD_refund_group,
        FTD_refund_code,
        FTD_refund_description,
        FTD_refund_accounting_type,
        sum(FTD_refund_product_amount) FTD_refund_product_amount,
        sum(FTD_refund_discount_amount) FTD_refund_discount_amount,
        sum(FTD_refund_addon_amount) FTD_refund_addon_amount,
        sum(FTD_refund_shipping_fee) FTD_refund_shipping_fee,
        sum(FTD_refund_service_fee) FTD_refund_service_fee,
        sum(FTD_refund_shipping_tax) FTD_refund_shipping_tax,
        sum(FTD_refund_service_tax) FTD_refund_service_tax,
		sum(FTD_refund_TAX1_AMOUNT) FTD_refund_TAX1_AMOUNT,
		sum(FTD_refund_TAX2_AMOUNT) FTD_refund_TAX2_AMOUNT,
		sum(FTD_refund_TAX3_AMOUNT) FTD_refund_TAX3_AMOUNT,
		sum(FTD_refund_TAX4_AMOUNT) FTD_refund_TAX4_AMOUNT,
        sum(FTD_refund_product_tax) FTD_refund_product_tax,
        sum(FTD_refund_count) FTD_refund_count
FROM 	(
	SELECT	''FLORIST'' FTD_ship_vendor_type,
		substr(cat.refund_disp_code, 1, 1)  FTD_refund_group,
		cat.refund_disp_code FTD_refund_code,
		crdv.description FTD_refund_description,
		crdv.refund_accounting_type FTD_refund_accounting_type,
		-1*sum(cat.product_amount) FTD_refund_product_amount,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) FTD_refund_discount_amount,
		-1*sum(cat.add_on_amount) FTD_refund_addon_amount,
		-1*sum(cat.shipping_fee) FTD_refund_shipping_fee,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) FTD_refund_service_fee,
		-1*sum(cat.shipping_tax) FTD_refund_shipping_tax,
		-1*sum(cat.service_fee_tax) FTD_refund_service_tax,
		sum(nvl(cr.TAX1_AMOUNT,0)) FTD_refund_TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0)) FTD_refund_TAX2_AMOUNT,
		sum(nvl(cr.TAX3_AMOUNT,0)) FTD_refund_TAX3_AMOUNT,
		sum(nvl(cr.TAX4_AMOUNT,0)) FTD_refund_TAX4_AMOUNT,
		-1*sum(cat.tax) FTD_refund_product_tax,
		count(cat.transaction_type) FTD_refund_count
	--
	FROM	clean.order_details cod
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
    JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cat.refund_id )	
	JOIN	clean.orders co
			on cod.order_guid = co.order_guid
	JOIN	clean.refund_disposition_val crdv
			on cat.refund_disp_code = crdv.refund_disp_code
        JOIN    ftd_apps.product_master pm
                        on pm.product_id = cod.product_id
                        and pm.product_type <> ''SERVICES''
	--
	WHERE	cat.transaction_type = ''Refund''
	AND	cat.payment_type <> ''NC''
	AND co.origin_id <>''TEST''
	AND	(cat.ship_method IS NULL  OR  cat.ship_method = ''SD'')
	AND	trunc(cat.transaction_date) between ''' || v_start_date || '''
					    and     ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_origin_predicate
	|| '
	--AND	trunc(cat.transaction_date) between v_start_date and v_end_date
	--AND	co.company_id in (nvl(p_company_code,co.company_id))
	--AND	co.origin_id in (nvl(p_origin_id,co.origin_id))
	--AND	co.company_id = DECODE(p_company_code,''ALL'',co.company_id,p_company_code)
	--AND	co.origin_id = DECODE(p_origin_id,''ALL'',co.origin_id,p_origin_id)
	--
	GROUP BY
		''FLORIST'',
		substr(cat.refund_disp_code, 1, 1),
		cat.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type
	--
	UNION ALL
	--
	SELECT	''FLORIST'',
		substr(crdv.refund_disp_code, 1, 1),
		crdv.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
		
	FROM	clean.refund_disposition_val crdv
	)
--
GROUP BY
	''1'',
	FTD_ship_vendor_type,
        FTD_refund_group,
        FTD_refund_code,
        FTD_refund_description,
        FTD_refund_accounting_type
--
ORDER BY
	FTD_ship_vendor_type,
        FTD_refund_group,
        FTD_refund_code '
;
EXECUTE IMMEDIATE v_sql
;
--
--**********************************************************************
-- Dropship Refunds
--**********************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15,
	col16,
	col17,
	col18,
	col19
	)
SELECT	''2'' Query_Id,
	FedEx_Ship_Vendor_Type,
	FedEx_refund_group,
	FedEx_refund_code,
	FedEx_refund_description,
	FedEx_refund_accounting_type,
        sum(FedEx_refund_product_amount) FedEx_refund_product_amount,
        sum(FedEx_refund_discount_amount) FedEx_refund_discount_amount,
        sum(FedEx_refund_addon_amount) FedEx_refund_addon_amount,
        sum(FedEx_refund_shipping_fee) FedEx_refund_shipping_fee,
        sum(FedEx_refund_service_fee) FedEx_refund_service_fee,
        sum(FedEx_refund_shipping_tax) FedEx_refund_shipping_tax,
        sum(FedEx_refund_service_tax) FedEx_refund_service_tax,
		sum(FedEx_refund_TAX1_AMOUNT) FedEx_refund_TAX1_AMOUNT,
		sum(FedEx_refund_TAX2_AMOUNT) FedEx_refund_TAX2_AMOUNT,
		sum(FedEx_refund_TAX3_AMOUNT) FedEx_refund_TAX3_AMOUNT,
		sum(FedEx_refund_TAX4_AMOUNT) FedEx_refund_TAX4_AMOUNT,
        sum(FedEx_refund_product_tax) FedEx_refund_product_tax,
        sum(FedEx_refund_count) FedEx_refund_count
FROM	(
	SELECT ''DROPSHIP'' FedEx_ship_vendor_type,
		substr(cat.refund_disp_code, 1, 1) FedEx_refund_group,
		cat.refund_disp_code FedEx_refund_code,
		crdv.description FedEx_refund_description,
		crdv.refund_accounting_type FedEx_refund_accounting_type,
		-1*sum(cat.product_amount) FedEx_refund_product_amount,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) FedEx_refund_discount_amount,
		-1*sum(cat.add_on_amount) FedEx_refund_addon_amount,
		-1*sum(cat.shipping_fee) FedEx_refund_shipping_fee,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) FedEx_refund_service_fee,
		-1*sum(cat.shipping_tax) FedEx_refund_shipping_tax,
		-1*sum(cat.service_fee_tax) FedEx_refund_service_tax,
		sum(nvl(cr.TAX1_AMOUNT,0)) FedEx_refund_TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0)) FedEx_refund_TAX2_AMOUNT,
		sum(nvl(cr.TAX3_AMOUNT,0)) FedEx_refund_TAX3_AMOUNT,
		sum(nvl(cr.TAX4_AMOUNT,0)) FedEx_refund_TAX4_AMOUNT,
		-1*sum(cat.tax) FedEx_refund_product_tax,
		count(cat.transaction_type) FedEx_refund_count
	--
	FROM	clean.order_details cod
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cat.refund_id )		
	JOIN	clean.orders co
			on cod.order_guid = co.order_guid
	JOIN	clean.refund_disposition_val crdv
			on cat.refund_disp_code = crdv.refund_disp_code
	--
	WHERE	cat.transaction_type = ''Refund''
	AND	cat.payment_type <> ''NC''
	AND co.origin_id <>''TEST''
	AND	(cat.ship_method IS NOT NULL AND cat.ship_method <> ''SD'')
	AND	trunc(cat.transaction_date) between ''' || v_start_date || '''
					    and     ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_origin_predicate
	|| '
	--AND	trunc(cat.transaction_date) between v_start_date and v_end_date
	--AND	co.company_id in (nvl(p_company_code,co.company_id))
	--AND	co.origin_id in (nvl(p_origin_id,co.origin_id))
	--AND	co.company_id = DECODE(p_company_code,''ALL'',co.company_id,p_company_code)
	--AND	co.origin_id = DECODE(p_origin_id,''ALL'',co.origin_id,p_origin_id)
	--
	GROUP BY
		''DROPSHIP'',
		substr(cat.refund_disp_code, 1, 1),
		cat.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type
	--
	UNION ALL
	--
	SELECT	''DROPSHIP'',
		substr(crdv.refund_disp_code, 1, 1),
		crdv.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	--
	FROM	clean.refund_disposition_val crdv
	)
--
GROUP BY
	''2'',
	FedEx_ship_vendor_type,
	FedEx_refund_group,
	FedEx_refund_code,
	FedEx_refund_description,
	FedEx_refund_accounting_type
--
ORDER BY
	FedEx_ship_vendor_type,
	FedEx_refund_group,
	FedEx_refund_code '
;
EXECUTE IMMEDIATE v_sql
;
--
--**********************************************************************
-- Services Refunds
--**********************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15,
	col16,
	col17,
	col18,
	col19
	)
SELECT	''6'' Query_Id,
	FTD_ship_vendor_type,
	FTD_refund_group,
        FTD_refund_code,
        FTD_refund_description,
        FTD_refund_accounting_type,
        sum(FTD_refund_product_amount) FTD_refund_product_amount,
        sum(FTD_refund_discount_amount) FTD_refund_discount_amount,
        sum(FTD_refund_addon_amount) FTD_refund_addon_amount,
        sum(FTD_refund_shipping_fee) FTD_refund_shipping_fee,
        sum(FTD_refund_service_fee) FTD_refund_service_fee,
        sum(FTD_refund_shipping_tax) FTD_refund_shipping_tax,
		sum(FTD_refund_service_tax) FTD_refund_service_tax,
        sum(FTD_refund_TAX1_AMOUNT) FTD_refund_TAX1_AMOUNT,
		sum(FTD_refund_TAX2_AMOUNT) FTD_refund_TAX2_AMOUNT,
		sum(FTD_refund_TAX3_AMOUNT) FTD_refund_TAX3_AMOUNT,
		sum(FTD_refund_TAX4_AMOUNT) FTD_refund_TAX4_AMOUNT,
        sum(FTD_refund_product_tax) FTD_refund_product_tax,
        sum(FTD_refund_count) FTD_refund_count
FROM 	(
	SELECT	''SERVICES'' FTD_ship_vendor_type,
		substr(cat.refund_disp_code, 1, 1)  FTD_refund_group,
		cat.refund_disp_code FTD_refund_code,
		crdv.description FTD_refund_description,
		crdv.refund_accounting_type FTD_refund_accounting_type,
		-1*sum(cat.product_amount) FTD_refund_product_amount,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) FTD_refund_discount_amount,
		-1*sum(cat.add_on_amount) FTD_refund_addon_amount,
		-1*sum(cat.shipping_fee) FTD_refund_shipping_fee,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) FTD_refund_service_fee,
		-1*sum(cat.shipping_tax) FTD_refund_shipping_tax,
		-1*sum(cat.service_fee_tax) FTD_refund_service_tax,
		sum(nvl(cr.TAX1_AMOUNT,0)) FTD_refund_TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0)) FTD_refund_TAX2_AMOUNT,
		sum(nvl(cr.TAX3_AMOUNT,0)) FTD_refund_TAX3_AMOUNT,
		sum(nvl(cr.TAX4_AMOUNT,0)) FTD_refund_TAX4_AMOUNT,
		-1*sum(cat.tax) FTD_refund_product_tax,
		count(cat.transaction_type) FTD_refund_count
	--
	FROM	clean.order_details cod
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cat.refund_id )
	JOIN	clean.orders co
			on cod.order_guid = co.order_guid
	JOIN	clean.refund_disposition_val crdv
			on cat.refund_disp_code = crdv.refund_disp_code
        JOIN    ftd_apps.product_master pm
                        on pm.product_id = cod.product_id
	--
	WHERE	cat.transaction_type = ''Refund''
	AND	cat.payment_type <> ''NC''
	AND co.origin_id <>''TEST''
        AND     pm.product_type = ''SERVICES''
	AND	(cat.ship_method IS NULL  OR  cat.ship_method = ''SD'')
	AND	trunc(cat.transaction_date) between ''' || v_start_date || '''
					    and     ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_origin_predicate
	|| '
	--AND	trunc(cat.transaction_date) between v_start_date and v_end_date
	--AND	co.company_id in (nvl(p_company_code,co.company_id))
	--AND	co.origin_id in (nvl(p_origin_id,co.origin_id))
	--AND	co.company_id = DECODE(p_company_code,''ALL'',co.company_id,p_company_code)
	--AND	co.origin_id = DECODE(p_origin_id,''ALL'',co.origin_id,p_origin_id)
	--
	GROUP BY
		''SERVICES'',
		substr(cat.refund_disp_code, 1, 1),
		cat.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type
	--
	UNION ALL
	--
	SELECT	''SERVICES'',
		substr(crdv.refund_disp_code, 1, 1),
		crdv.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	FROM	clean.refund_disposition_val crdv
	)
--
GROUP BY
	''6'',
	FTD_ship_vendor_type,
        FTD_refund_group,
        FTD_refund_code,
        FTD_refund_description,
        FTD_refund_accounting_type
--
ORDER BY
	FTD_ship_vendor_type,
        FTD_refund_group,
        FTD_refund_code '
;
EXECUTE IMMEDIATE v_sql
;
--
--**********************************************************************
-- Total Refunds
--**********************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15,
	col16,
	col17,
	col18,
	col19
	)
SELECT	''3'' Query_Id,
	Total_ship_vendor_type,
	Total_refund_group,
	Total_refund_code,
	Total_refund_description,
	Total_refund_accounting_type,
        sum(Total_refund_product_amount) Total_refund_product_amount,
        sum(Total_refund_discount_amount) Total_refund_discount_amount,
        sum(Total_refund_addon_amount) Total_refund_addon_amount,
        sum(Total_refund_shipping_fee) Total_refund_shipping_fee,
        sum(Total_refund_service_fee) Total_refund_service_fee,
        sum(Total_refund_shipping_tax) Total_refund_shipping_tax,
        sum(Total_refund_service_tax) Total_refund_service_tax,
		sum(Total_refund_TAX1_AMOUNT) Total_refund_TAX1_AMOUNT,
		sum(Total_refund_TAX2_AMOUNT) Total_refund_TAX2_AMOUNT,
		sum(Total_refund_TAX3_AMOUNT) Total_refund_TAX3_AMOUNT,
		sum(Total_refund_TAX4_AMOUNT) Total_refund_TAX4_AMOUNT,
        sum(Total_refund_product_tax) Total_refund_product_tax,
        sum(Total_refund_count) Total_refund_count
FROM	(
	SELECT ''TOTAL'' Total_ship_vendor_type,
		substr(cat.refund_disp_code, 1, 1)  Total_refund_group,
		cat.refund_disp_code Total_refund_code,
		crdv.description Total_refund_description,
		crdv.refund_accounting_type Total_refund_accounting_type,
		-1*sum(cat.product_amount) Total_refund_product_amount,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) Total_refund_discount_amount,
		-1*sum(cat.add_on_amount) Total_refund_addon_amount,
		-1*sum(cat.shipping_fee) Total_refund_shipping_fee,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) Total_refund_service_fee,
		-1*sum(cat.shipping_tax) Total_refund_shipping_tax,
		-1*sum(cat.service_fee_tax) Total_refund_service_tax,
		sum(nvl(cr.TAX1_AMOUNT,0)) Total_refund_TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0)) Total_refund_TAX2_AMOUNT,
		sum(nvl(cr.TAX3_AMOUNT,0)) Total_refund_TAX3_AMOUNT,
		sum(nvl(cr.TAX4_AMOUNT,0)) Total_refund_TAX4_AMOUNT,
		-1*sum(cat.tax) Total_refund_product_tax,
		count(cat.transaction_type) Total_refund_count
	--
	FROM	clean.order_details cod
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
	JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cat.refund_id )
	JOIN	clean.orders co
			on cod.order_guid = co.order_guid
	JOIN	clean.refund_disposition_val crdv
			on cat.refund_disp_code = crdv.refund_disp_code
	--
	WHERE	cat.transaction_type = ''Refund''
	AND	cat.payment_type <> ''NC''
	AND co.origin_id <>''TEST''
	AND	trunc(cat.transaction_date) between ''' || v_start_date || '''
					    and     ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_origin_predicate
	|| '
	--AND	trunc(cat.transaction_date) between v_start_date and v_end_date
	--AND	co.company_id in (nvl(p_company_code,co.company_id))
	--AND	co.origin_id in (nvl(p_origin_id,co.origin_id))
	--AND	co.company_id = DECODE(p_company_code,''ALL'',co.company_id,p_company_code)
	--AND	co.origin_id = DECODE(p_origin_id,''ALL'',co.origin_id,p_origin_id)
	--
	GROUP BY
		''TOTAL'',
		substr(cat.refund_disp_code, 1, 1),
		cat.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type
	--
	UNION ALL
	--
	SELECT	''TOTAL'',
		substr(crdv.refund_disp_code, 1, 1),
		crdv.refund_disp_code,
		crdv.description,
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	--
	FROM	clean.refund_disposition_val crdv
	)
--
GROUP BY
	''3'',
	Total_ship_vendor_type,
	Total_refund_group,
	Total_refund_code,
	Total_refund_description,
	Total_refund_accounting_type
--
ORDER BY
	Total_ship_vendor_type,
	Total_refund_group,
	Total_refund_code '
;
EXECUTE IMMEDIATE v_sql
;
--
--**********************************************************************
-- Accounting Type (CR vs. PG) by Ship Method (Florist vs. Dropship)
--**********************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15,
	col16
	
	)
SELECT	''4'',
	RefType_ship_vendor_type,
	RefType_refund_accounting_type,
        sum(RefType_refund_product_amount) RefType_refund_product_amount,
        sum(RefType_refund_discount_amount) RefType_refund_discount_amount,
        sum(RefType_refund_addon_amount) RefType_refund_addon_amount,
        sum(RefType_refund_shipping_fee) RefType_refund_shipping_fee,
        sum(RefType_refund_service_fee) RefType_refund_service_fee,
        sum(RefType_refund_shipping_tax) RefType_refund_shipping_tax,
        sum(RefType_refund_service_tax) RefType_refund_service_tax,
		sum(RefType_refund_TAX1_AMOUNT) RefType_refund_TAX1_AMOUNT,
		sum(RefType_refund_TAX2_AMOUNT) RefType_refund_TAX2_AMOUNT,
		sum(RefType_refund_TAX3_AMOUNT) RefType_refund_TAX3_AMOUNT,
		sum(RefType_refund_TAX4_AMOUNT) RefType_refund_TAX4_AMOUNT,
        sum(RefType_refund_product_tax) RefType_refund_product_tax,
        sum(RefType_refund_count) RefType_refund_count
FROM	(
	SELECT	CASE
                when pm.product_type = ''SERVICES''
                    THEN ''SERVICES''
                when cat.ship_method IS NULL OR cat.ship_method = ''SD''
		    THEN ''FLORIST''
		ELSE ''DROPSHIP''
		END RefType_ship_vendor_type,
		crdv.refund_accounting_type RefType_refund_accounting_type,
		-1*sum(cat.product_amount) RefType_refund_product_amount,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) RefType_refund_discount_amount,
		-1*sum(cat.add_on_amount) RefType_refund_addon_amount,
		-1*sum(cat.shipping_fee) RefType_refund_shipping_fee,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) RefType_refund_service_fee,
		-1*sum(cat.shipping_tax) RefType_refund_shipping_tax,
		-1*sum(cat.service_fee_tax) RefType_refund_service_tax,
		sum(nvl(cr.TAX1_AMOUNT,0)) RefType_refund_TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0)) RefType_refund_TAX2_AMOUNT,
		sum(nvl(cr.TAX3_AMOUNT,0)) RefType_refund_TAX3_AMOUNT,
		sum(nvl(cr.TAX4_AMOUNT,0)) RefType_refund_TAX4_AMOUNT,
		-1*sum(cat.tax) RefType_refund_product_tax,
		count(cat.transaction_type) RefType_refund_count
	--
	FROM	clean.order_details cod
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)

	JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cat.refund_id )
	JOIN	clean.orders co
			on cod.order_guid = co.order_guid
	JOIN	clean.refund_disposition_val crdv
			on cat.refund_disp_code = crdv.refund_disp_code
        JOIN    ftd_apps.product_master pm
                        on pm.product_id = cod.product_id
	--
	WHERE	cat.transaction_type = ''Refund''
	AND	cat.payment_type <> ''NC''
	AND co.origin_id <>''TEST''
	AND	trunc(cat.transaction_date) between ''' || v_start_date || '''
					    and     ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_origin_predicate
	|| '
	--AND	trunc(cat.transaction_date) between v_start_date and v_end_date
	--AND	co.company_id in (nvl(p_company_code,co.company_id))
	--AND	co.origin_id in (nvl(p_origin_id,co.origin_id))
	--AND	co.company_id = DECODE(p_company_code,''ALL'',co.company_id,p_company_code)
	--AND	co.origin_id = DECODE(p_origin_id,''ALL'',co.origin_id,p_origin_id)
	--
	GROUP BY
		CASE
                when pm.product_type = ''SERVICES''
                    THEN ''SERVICES''
                when cat.ship_method IS NULL OR cat.ship_method = ''SD''
		    THEN ''FLORIST''
		ELSE ''DROPSHIP''
		END,
		crdv.refund_accounting_type
	--
	UNION ALL
	--
	SELECT	''FLORIST'',
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	FROM	clean.refund_disposition_val crdv
	--
	UNION ALL
	--
	SELECT	''DROPSHIP'',
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	FROM	clean.refund_disposition_val crdv
	--
	UNION ALL
	--
	SELECT	''SERVICES'',
		crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	FROM	clean.refund_disposition_val crdv
	)
--
GROUP BY
	''4'',
	RefType_ship_vendor_type,
	RefType_refund_accounting_type '
;
EXECUTE IMMEDIATE v_sql
;
--
--**********************************************************************
-- Accounting Type (CR vs. PG) Total
--**********************************************************************
--
v_sql := 'INSERT into rpt.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5,
	col6,
	col7,
	col8,
	col9,
	col10,
	col11,
	col12,
	col13,
	col14,
	col15
	)
SELECT	''5'',
	RefType_refund_accounting_type,
        sum(RefType_refund_product_amount) RefType_refund_product_amount,
        sum(RefType_refund_discount_amount) RefType_refund_discount_amount,
        sum(RefType_refund_addon_amount) RefType_refund_addon_amount,
        sum(RefType_refund_shipping_fee) RefType_refund_shipping_fee,
        sum(RefType_refund_service_fee) RefType_refund_service_fee,
        sum(RefType_refund_shipping_tax) RefType_refund_shipping_tax,
        sum(RefType_refund_service_tax) RefType_refund_service_tax,
		sum(RefType_refund_TAX1_AMOUNT) RefType_refund_TAX1_AMOUNT,
		sum(RefType_refund_TAX2_AMOUNT) RefType_refund_TAX2_AMOUNT,
		sum(RefType_refund_TAX3_AMOUNT) RefType_refund_TAX3_AMOUNT,
		sum(RefType_refund_TAX4_AMOUNT) RefType_refund_TAX4_AMOUNT,
        sum(RefType_refund_product_tax) RefType_refund_product_tax,
        sum(RefType_refund_count) RefType_refund_count
FROM	(
	SELECT	crdv.refund_accounting_type RefType_refund_accounting_type,
		-1*sum(cat.product_amount) RefType_refund_product_amount,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount)) RefType_refund_discount_amount,
		-1*sum(cat.add_on_amount) RefType_refund_addon_amount,
		-1*sum(cat.shipping_fee) RefType_refund_shipping_fee,
		-1*sum(decode(co.origin_id,''WLMTI'',cat.wholesale_service_fee,cat.service_fee)) RefType_refund_service_fee,
		-1*sum(cat.shipping_tax) RefType_refund_shipping_tax,
		-1*sum(cat.service_fee_tax) RefType_refund_service_tax,
		sum(nvl(cr.TAX1_AMOUNT,0)) RefType_refund_TAX1_AMOUNT,
		sum(nvl(cr.TAX2_AMOUNT,0)) RefType_refund_TAX2_AMOUNT,
		sum(nvl(cr.TAX3_AMOUNT,0)) RefType_refund_TAX3_AMOUNT,
		sum(nvl(cr.TAX4_AMOUNT,0)) RefType_refund_TAX4_AMOUNT,
		-1*sum(cat.tax) RefType_refund_product_tax,
		count(cat.transaction_type) RefType_refund_count
	--
	FROM	clean.order_details cod
	JOIN	clean.accounting_transactions cat
			on (cat.order_detail_id = cod.order_detail_id)
    JOIN	clean.refund cr on (cr.order_detail_id = cod.order_detail_id and cr.refund_id=cat.refund_id )
	
	JOIN	clean.orders co
			on cod.order_guid = co.order_guid
	JOIN	clean.refund_disposition_val crdv
			on cat.refund_disp_code = crdv.refund_disp_code
	--
	WHERE	cat.transaction_type = ''Refund''
	AND	cat.payment_type <> ''NC''
	AND co.origin_id <>''TEST''
	AND	trunc(cat.transaction_date) between ''' || v_start_date || '''
					    and     ''' || v_end_date || ''''
	|| v_company_predicate
	|| v_origin_predicate
	|| '
	--AND	trunc(cat.transaction_date) between v_start_date and v_end_date
	--AND	co.company_id in (nvl(p_company_code,co.company_id))
	--AND	co.origin_id in (nvl(p_origin_id,co.origin_id))
	--AND	co.company_id = DECODE(p_company_code,''ALL'',co.company_id,p_company_code)
	--AND	co.origin_id = DECODE(p_origin_id,''ALL'',co.origin_id,p_origin_id)
	--
	GROUP BY
		crdv.refund_accounting_type
	--
	UNION ALL
	--
	SELECT	crdv.refund_accounting_type,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	--
	FROM	clean.refund_disposition_val crdv
	)
--
GROUP BY
	''5'',
	RefType_refund_accounting_type '
;
EXECUTE IMMEDIATE v_sql
;

END
;
.
/