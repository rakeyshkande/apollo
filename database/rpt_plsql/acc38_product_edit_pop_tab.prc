CREATE OR REPLACE
PROCEDURE rpt.ACC38_Product_Edit_Pop_Tab
                (
                 p_start_date in varchar2,
		 p_end_date in varchar2,
		 p_change_scope in varchar2,
		 p_exclude_avail_only_flag in varchar2,
		 p_exclude_del_method_only_flag in varchar2
                )
IS
  v_start_date 		date		:= to_date(p_start_date,'mm/dd/yyyy');
  v_end_date		date		:= to_date(p_end_date,'mm/dd/yyyy');
  v_change_class_predicate	varchar2(1000)	:= NULL;
  v_change_scope_predicate_1	varchar2(1000)	:= NULL;
  v_change_scope_predicate_2	varchar2(1000)	:= NULL;
  v_change_scope_predicate_3	varchar2(1000)	:= NULL;
  
  v_sql			varchar2(7000)	:= NULL;
  

BEGIN
  --
  --**********************************************************************
  -- Set the filters and change type WHERE clause predicates
  --**********************************************************************
  --
  if p_exclude_avail_only_flag = 'Y' then
	v_change_class_predicate := ' AND pacu.acctg_change_class_code <> ''A'' ';
  end if;
  
  if p_exclude_del_method_only_flag = 'Y' then
  	v_change_class_predicate := v_change_class_predicate || ' AND pacu.acctg_change_class_code <> ''D'' ';
  end if;
  --
  if upper(p_change_scope) = 'NEW' then
	v_change_scope_predicate_1 := ' AND decode(pms.operation$, ''INS'',2,1) = 2 ';
	v_change_scope_predicate_2 := ' AND decode(pss.operation$, ''INS'',2,1) = 2 ';
	v_change_scope_predicate_3 := ' AND decode(vps.operation$, ''INS'',2,1) = 2 ';
  elsif upper(p_change_scope) = 'UPDATED' then
  	v_change_scope_predicate_1 := ' AND decode(pms.operation$, ''INS'',2,1) = 1 ';
  	v_change_scope_predicate_2 := ' AND decode(pss.operation$, ''INS'',2,1) = 1 ';
	v_change_scope_predicate_3 := ' AND decode(vps.operation$, ''INS'',2,1) = 1 ';
  end if;
  --
  --**********************************************************************
  -- Product Edit Query
  --**********************************************************************
  --
  v_sql :=
	'INSERT into rpt.rep_tab
	        (col1,
         	 col2,
         	 col3,
         	 col4,
         	 col5,
         	 col6,
         	 col7,
         	 col8,
         	 col9,
         	 col10,
         	 col11,
		 col12,
		 col13,
		 col14,
		 col15,
		 col16,
		 col17,
		 col18,
         	 col19,
                 col20,
                 col21,
                 col22,
                 col23,
                 col24,
                 col25
         	 )
         -- product
	 select    
	     decode(pms.operation$, ''INS'',2,1) sort1,
	     pacu.acctg_change_unit_id sort2,
	     decode(pms.operation$, ''UPD_OLD'',''Old'',''UPD_NEW'',''New'',''INS'',''New'',pms.operation$) change_type,
	     pms.product_id product_id,
	     NULL product_subcode_id,
	     to_char(pms.updated_on,''mm/dd/yyyy'') update_date,
	     to_char(pms.updated_on,''hh:mi:ss AM'') update_time,
	     pms.updated_by user_id,
	     pms.status availability,
	     pms.novator_id novator_id,
	     pms.dim_weight dim_weight,
	     ''NA'' vendor_cost,
	     ''NA'' vendor_code,
	     pms.ship_method_florist dm_florist,
	     pms.ship_method_carrier dm_carrier,
	     pms.standard_price standard_price,
	     pms.deluxe_price deluxe_price,
	     pms.premium_price premium_price,
	     pms.no_tax_flag no_tax,
             ''NA'' vendor_cost_eff_date,
             to_char(pms.supplies_expense_dollar_amt),
             to_char(pms.supplies_expense_eff_date, ''mm/dd/yyyy''),
             to_char(pms.royalty_percent),
             to_char(pms.royalty_percent_eff_date, ''mm/dd/yyyy''),
             decode(pms.product_type, ''SERVICES'', ''Y'', ''N'')
	 from ftd_apps.product_master$ pms
	 join ftd_apps.product_acctg_change_unit pacu
	 on pms.acctg_change_unit_id=pacu.acctg_change_unit_id
	 where trunc(pms.timestamp$) between '''
		|| v_start_date || ''' and ''' || v_end_date || ''''
	|| v_change_class_predicate
	|| v_change_scope_predicate_1
	|| ' union
	     -- product subcodes
	     select
	     decode(pss.operation$, ''INS'',2,1) sort1,
	     pacu.acctg_change_unit_id sort2,
	     decode(pss.operation$, ''UPD_OLD'',''Old'',''UPD_NEW'',''New'',''INS'',''New'',pss.operation$) change_type,
	     pm.product_id product_id,
	     pss.product_subcode_id product_subcode_id,
	     to_char(pss.updated_on,''mm/dd/yyyy'') update_date,
	     to_char(pss.updated_on,''hh:mi:ss AM'') update_time,
	     pss.updated_by user_id,
	     pm.status availability,
	     pm.novator_id novator_id,
	     pss.dim_weight dim_weight,
	     ''NA'' vendor_cost,
	     ''NA'' vendor_code,
	     pm.ship_method_florist dm_florist,
	     pm.ship_method_carrier dm_carrier,
	     pss.subcode_price standard_price,
	     NULL deluxe_price,
	     NULL premium_price,
	     pm.no_tax_flag no_tax,
             ''NA'' vendor_cost_eff_date,
             to_char(pm.supplies_expense_dollar_amt),
             to_char(pm.supplies_expense_eff_date, ''mm/dd/yyyy''),
             to_char(pm.royalty_percent),
             to_char(pm.royalty_percent_eff_date, ''mm/dd/yyyy''),
             decode(pm.product_type, ''SERVICES'', ''Y'', ''N'')
	 from ftd_apps.product_master pm
	 join ftd_apps.product_subcodes$ pss
	 on pm.product_id=pss.product_id
	 join ftd_apps.product_acctg_change_unit pacu
	 on pss.acctg_change_unit_id=pacu.acctg_change_unit_id
	 where trunc(pss.timestamp$) between '''
		|| v_start_date || ''' and ''' || v_end_date || ''''
	|| v_change_class_predicate
	|| v_change_scope_predicate_2
	|| ' union
	     -- vendor product for product master
	     select
	     decode(vps.operation$, ''INS'',2,1) sort1,
	     pacu.acctg_change_unit_id sort2,
	     decode(vps.operation$, ''UPD_OLD'',''Old'',''UPD_NEW'',''New'',''INS'',''New'',vps.operation$) change_type,
	     pm.product_id product_id,
	     NULL product_subcode_id,
	     to_char(vps.updated_on,''mm/dd/yyyy'') update_date,
	     to_char(vps.updated_on,''hh:mi:ss AM'') update_time,
	     vps.updated_by user_id,
	     pm.status availability,
	     pm.novator_id novator_id,
	     pm.dim_weight dim_weight,
	     to_char(vps.vendor_cost),
	     vps.vendor_id vendor_code,
	     pm.ship_method_florist dm_florist,
	     pm.ship_method_carrier dm_carrier,
	     pm.standard_price standard_price,
	     pm.deluxe_price deluxe_price,
	     pm.premium_price premium_price,
	     pm.no_tax_flag no_tax,
             to_char(vps.vendor_cost_eff_date, ''mm/dd/yyyy''),
             to_char(pm.supplies_expense_dollar_amt),
             to_char(pm.supplies_expense_eff_date, ''mm/dd/yyyy''),
             to_char(pm.royalty_percent),
             to_char(pm.royalty_percent_eff_date, ''mm/dd/yyyy''),
             decode(pm.product_type, ''SERVICES'', ''Y'', ''N'')
	 from ftd_apps.product_master pm
	 join ftd_apps.vendor_product$ vps
	 on pm.product_id=vps.product_subcode_id
	 join ftd_apps.product_acctg_change_unit pacu
	 on vps.acctg_change_unit_id=pacu.acctg_change_unit_id
	 where trunc(vps.timestamp$) between '''
		|| v_start_date || ''' and ''' || v_end_date || ''''
	|| v_change_class_predicate
	|| v_change_scope_predicate_3
	|| ' union
	     -- vendor product for product subcode
	     select
	     decode(vps.operation$, ''INS'',2,1) sort1,
	     pacu.acctg_change_unit_id sort2,
	     decode(vps.operation$, ''UPD_OLD'',''Old'',''UPD_NEW'',''New'',''INS'',''New'',vps.operation$) change_type,
	     pm.product_id product_id,
	     ps.product_subcode_id,
	     to_char(vps.updated_on,''mm/dd/yyyy'') update_date,
	     to_char(vps.updated_on,''hh:mi:ss AM'') update_time,
	     vps.updated_by user_id,
	     pm.status availability,
	     pm.novator_id novator_id,
	     ps.dim_weight dim_weight,
	     to_char(vps.vendor_cost),
	     vps.vendor_id vendor_code,
	     pm.ship_method_florist dm_florist,
	     pm.ship_method_carrier dm_carrier,
	     ps.subcode_price standard_price,
	     NULL deluxe_price,
	     NULL premium_price,
	     pm.no_tax_flag no_tax,
             ''NA'' vendor_cost_eff_date,
             to_char(pm.supplies_expense_dollar_amt),
             to_char(pm.supplies_expense_eff_date, ''mm/dd/yyyy''),
             to_char(pm.royalty_percent),
             to_char(pm.royalty_percent_eff_date, ''mm/dd/yyyy''),
             decode(pm.product_type, ''SERVICES'', ''Y'', ''N'')
	 from ftd_apps.vendor_product$ vps
	 join ftd_apps.product_subcodes ps
	 on ps.product_subcode_id=vps.product_subcode_id
	 join ftd_apps.product_master pm
	 on pm.product_id=ps.product_id
	 join ftd_apps.product_acctg_change_unit pacu
	 on vps.acctg_change_unit_id=pacu.acctg_change_unit_id
	 where trunc(vps.timestamp$) between '''
		|| v_start_date || ''' and ''' || v_end_date || ''''
	|| v_change_class_predicate
	|| v_change_scope_predicate_3;
           
          
           
           EXECUTE IMMEDIATE v_sql
           ;
   
END
;
.
/
