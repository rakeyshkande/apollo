CREATE OR REPLACE
PROCEDURE rpt.acc10_invoice_summary_pop_tab
			(
			 p_run_date in varchar2
			)
IS
BEGIN
--
--**********************************************************************
-- Orders and Refunds
--**********************************************************************
--
INSERT into RPT.rep_tab
	(
	col1,
	col2,
	col3,
	col4,
	col5
	)
SELECT	source_code source_code,
	description description,
	sum(nvl(prev_month_invoice_amt,0)) prev_month_invoice_amt,
	sum(nvl(curr_month_invoice_amt,0)) curr_month_invoice_amt,
	sum(nvl(curr_month_invoice_amt,0)) + sum(nvl(prev_month_invoice_amt,0))  net_month_invoice_amt
--
FROM	(
	SELECT	'CURRENT ORDERS' month_type,
		fas.source_code source_code,
		fas.description description,
		0 prev_month_invoice_amt,
		sum(nvl((nvl(cat.product_amount, 0) +
			 nvl(cat.add_on_amount, 0) +
			 nvl(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee), 0) +
			 nvl(cat.shipping_fee, 0) +
			 nvl(cat.shipping_tax, 0) +
			 nvl(cat.service_fee_tax, 0) +
			 nvl(cat.tax, 0) -
			 nvl(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount), 0)), 0)) curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN	clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid and cob.additional_bill_indicator = 'N' and cp.additional_bill_id is null)
                           or (cp.order_guid = co.order_guid and cob.order_bill_id = cp.additional_bill_id and cob.additional_bill_indicator = 'Y')
	JOIN	clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cob.order_bill_id=cat.order_bill_id)
	JOIN	ftd_apps.source fas on (fas.source_code = cod.source_code)
	--
	WHERE	cp.payment_type in ('IN', 'UA')
	AND	cob.bill_status in ('Billed','Settled')
	AND 	cp.payment_indicator='P'
	AND	cat.transaction_type <> 'Refund'
	AND	cob.bill_date between trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
					 and last_day(to_date(p_run_date,'mm/dd/yyyy')) + 0.99999
	AND 	co.order_date between trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
					 and last_day(to_date(p_run_date,'mm/dd/yyyy')) + 0.99999
	--
	GROUP BY
		fas.source_code,
		fas.description
	--
	UNION ALL
	--
	SELECT	'CURRENT REFUNDS' month_type,
		fas.source_code source_code,
		fas.description description,
		0 prev_month_invoice_amt,
		sum(nvl((nvl(cat.product_amount, 0) +
			 nvl(cat.add_on_amount, 0) +
			 nvl(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee), 0) +
			 nvl(cat.shipping_fee, 0) +
			 nvl(cat.shipping_tax, 0) +
			 nvl(cat.service_fee_tax, 0) +
			 nvl(cat.tax, 0) -
			 nvl(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount), 0)), 0)) curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN    clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN    clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN    clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
	JOIN    ftd_apps.source fas on (fas.source_code = cod.source_code)
	JOIN    clean.refund cr on (cr.order_detail_id = cat.order_detail_id and cr.refund_id=cat.refund_id and cr.refund_id=cp.refund_id)
	--
	WHERE	cp.payment_type in ('IN', 'UA')
	AND	cr.refund_status = 'Billed'
	AND 	cp.payment_indicator='R'
	AND	cat.transaction_type in ('Refund')
	AND	cr.refund_date between trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
					  and last_day(to_date(p_run_date,'mm/dd/yyyy')) + 0.99999
	AND 	co.order_date between trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
					 and last_day(to_date(p_run_date,'mm/dd/yyyy')) + 0.99999
	--
	GROUP BY
		fas.source_code,
		fas.description
	--
	UNION ALL
	--
	SELECT	'PRIOR ORDERS' month_type,
		fas.source_code source_code,
		fas.description description,
		sum(nvl((nvl(cat.product_amount, 0) +
			 nvl(cat.add_on_amount, 0) +
			 nvl(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee), 0) +
			 nvl(cat.shipping_fee, 0) +
			 nvl(cat.shipping_tax, 0) +
			 nvl(cat.service_fee_tax, 0) +
			 nvl(cat.tax, 0) -
			 nvl(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount), 0)), 0)) prev_month_invoice_amt,
			 0 curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN	clean.order_bills cob on (cob.order_detail_id = cod.order_detail_id)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid and cob.additional_bill_indicator = 'N' and cp.additional_bill_id is null)
                           or (cp.order_guid = co.order_guid and cob.order_bill_id = cp.additional_bill_id and cob.additional_bill_indicator = 'Y')
	JOIN	clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id and cob.order_bill_id=cat.order_bill_id)
	JOIN	ftd_apps.source fas on (fas.source_code = co.source_code)
	--
	WHERE	cp.payment_type in ('IN', 'UA')
	AND	cob.bill_status in ('Billed','Settled')
	AND 	cp.payment_indicator='P'
	AND	cat.transaction_type = 'Add Bill'
	AND	cob.bill_date between trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
					 and last_day(to_date(p_run_date,'mm/dd/yyyy')) + 0.99999
	AND 	co.order_date < trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
	--
	GROUP BY
		fas.source_code,
		fas.description
	--
	UNION ALL
	--
	SELECT	'PRIOR REFUNDS' month_type,
		fas.source_code source_code,
		fas.description description,
		sum(nvl((nvl(cat.product_amount, 0) +
			 nvl(cat.add_on_amount, 0) +
			 nvl(decode(co.origin_id,'WLMTI',cat.wholesale_service_fee,cat.service_fee), 0) +
			 nvl(cat.shipping_fee, 0) +
			 nvl(cat.shipping_tax, 0) +
			 nvl(cat.service_fee_tax, 0) +
			 nvl(cat.tax, 0) -
			 nvl(decode(co.origin_id,'WLMTI',cat.product_amount+cat.add_on_amount-cat.wholesale_amount,cat.discount_amount), 0)), 0)) prev_month_invoice_amt,
			 0 curr_month_invoice_amt
	--
	FROM	clean.orders co
	JOIN	clean.order_details cod on (cod.order_guid = co.order_guid)
	JOIN	clean.payments cp on (cp.order_guid = co.order_guid)
	JOIN	clean.accounting_transactions cat on (cat.order_detail_id = cod.order_detail_id)
	JOIN	ftd_apps.source fas on (fas.source_code = co.source_code)
	JOIN	clean.refund cr on (cr.order_detail_id = cat.order_detail_id and cr.refund_id=cat.refund_id and cr.refund_id=cp.refund_id)
	--
	WHERE	cp.payment_type in ('IN', 'UA')
	AND 	cp.payment_indicator='R'
	AND	cr.refund_status = 'Billed'
	AND	cat.transaction_type in ('Refund')
	AND	cr.refund_date between trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
					 and last_day(to_date(p_run_date,'mm/dd/yyyy')) + 0.99999
	AND 	co.order_date < trunc(to_date(p_run_date,'mm/dd/yyyy'),'month')
	--
	GROUP BY
		fas.source_code,
		fas.description
	)
GROUP BY source_code,
         description
;
END
;
.
/
