CREATE OR REPLACE
PROCEDURE rpt.MER10_Inv_Trk_Shutdown_Pop_Tab
                (p_product_id in varchar2,
                 p_vendor_id in varchar2,
                 p_start_date in varchar2,
                 p_end_date in varchar2
                )
IS
  v_product_id_predicate         varchar2(1000) := NULL;
  v_vendor_id_predicate          varchar2(1000) := NULL;
  v_start_date_predicate      	 varchar2(1000) := NULL;
  v_end_date_predicate      	 varchar2(1000) := NULL;
  v_sql                          varchar2(10000)        := NULL;

BEGIN
--**********************************************************************
  -- Set the product id and vendor id WHERE clause predicates
  --**********************************************************************
  --
  if  p_product_id <> 'ALL'
  and p_product_id IS NOT NULL then
        --v_product_id_predicate :=
        --        ' AND fit.product_id in ('||rpt.multi_value(p_product_id)||') ';
        v_product_id_predicate := ' AND fit.product_id in (SELECT product_id FROM FTD_APPS.INV_TRK_PRODUCT_TEMP)';
  end if;
  --
  if  p_vendor_id <> 'ALL'
  and p_vendor_id IS NOT NULL then
        --v_vendor_id_predicate :=
        --        ' AND fit.vendor_id in ('||rpt.multi_value(p_vendor_id)||') ';
        v_vendor_id_predicate := ' AND fit.vendor_id in (SELECT vendor_id FROM FTD_APPS.INV_TRK_VENDOR_TEMP)';
  end if;
  --
  if p_start_date IS NOT NULL then
        v_start_date_predicate :=
                ' AND feh.event_timestamp >= to_date(''' || p_start_date || ''', ''mm/dd/yyyy'')';
  end if;
  
  --
  if p_end_date IS NOT NULL then
          v_end_date_predicate :=
                  ' AND feh.event_timestamp < to_date(''' || p_end_date || ''', ''mm/dd/yyyy'') + 1';
  end if;
  --
  --**********************************************************************
  -- Inventory Tracking Shutdown Query
  --**********************************************************************
  --
  v_sql :=
  'INSERT into rpt.rep_tab
        (col1,
         col2,
         col3,
         col4,
         col5,
         col6,
         col7,
         col8,
         col9,
         col10,
         col11
        )
        SELECT  (CASE feh.event_type
        		when ''PRODUCT_SHUTDOWN'' then ''PRODUCT SHUTDOWN''
        		when ''LOCATION_SHUTDOWN'' then ''LOCATION SHUTDOWN''
        		when ''PRODUCT_AVAILABILITY_DATES'' then ''PRODUCT AVAILABILITY DATES''
        		else ''UNKNOWN''
        	end) event_type,
                nvl(fit.product_id,''Unknown'')         product_id,
                nvl(fpm.product_name,''Unknown'')   	product_name,
                to_char(fit.start_date,''mm/dd/yyyy'')	inventory_start_date,
                DECODE(to_char(fit.end_date,''mm/dd/yyyy''),''12/31/9999'', null, to_char(fit.end_date,''mm/dd/yyyy'')) inventory_end_date,
                fit.vendor_id				vendor_id,
                nvl(fvm.vendor_name,''Unknown Vendor'') vendor_location,
                fit.shutdown_threshold_qty              shutdown_threshold,
                fpm.exception_start_date		availability_start_date,
                fpm.exception_end_date                  availability_end_date,
                to_char(feh.event_timestamp, ''mm/dd/yyyy HH24:MI:SS'')		        event_timestamp
        FROM    ftd_apps.inv_trk_event_history feh
        JOIN	ftd_apps.inv_trk fit
        ON	fit.inv_trk_id = feh.inv_trk_id
        JOIN 	ftd_apps.product_master fpm
        ON	fpm.product_id = fit.product_id
        JOIN	ftd_apps.vendor_master fvm
        ON	fvm.vendor_id = fit.vendor_id
         WHERE 1=1'
        || v_product_id_predicate
        || v_vendor_id_predicate
        || v_start_date_predicate 
        || v_end_date_predicate
        ;

   EXECUTE IMMEDIATE v_sql
   ;
END
;
.
/
