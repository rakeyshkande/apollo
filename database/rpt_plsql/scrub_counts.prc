CREATE OR REPLACE
PROCEDURE rpt.SCRUB_COUNTS as

-- TAS 02/04/2004
-- Procedure will run hourly from a cron job to capture scrub counts
-- for the HP Holiday Monitoring Report
-- JRT 1/28/2005
-- Modified to exclude test orders from counts

CURSOR inbound_counts ( start_time date, end_time date )IS
   SELECT COUNT(*) cnt, test_order_flag
     FROM frp.stats_osp
     WHERE timestamp >= start_time
       and timestamp < end_time
       and item_state = '3004'
       and status in ('2002','2003')
     GROUP BY test_order_flag;

CURSOR scrub_counts ( start_time date, end_time date )IS
   SELECT COUNT(*) cnt, test_order_flag
     FROM frp.stats_osp
     WHERE timestamp >= start_time
       and timestamp < end_time
       and status in ('2004','2005','2007')
       and item_state = '3006'
     GROUP BY test_order_flag;

v_inbound_count        number := 0;
v_scrubbed_count       number := 0;
v_inbound_test_count   number := 0;
v_scrubbed_test_count  number := 0;
v_scrub_total          number := 0;
start_time           date;
end_time             date;
stamp_time           date;
v_flag              varchar2(8);
v_cnt               number;
v_stat               varchar2(1);
v_mess               varchar2(3000);
V_current_minutes        number;
V_start_minutes        number;

BEGIN
dbms_output.put_line('scrub counts 0 '||to_char(sysdate,'hh24:mi:ss'));
CASE WHEN to_number(to_char(sysdate,'MI')) BETWEEN 0 AND 29
     THEN  v_current_minutes := 0;
           start_time := trunc(sysdate - (1/24), 'hh24') + 30/1440;
           end_time := trunc(sysdate, 'hh24');
     ELSE  v_current_minutes := 30/1440;
           start_time := trunc(sysdate, 'hh24');
           end_time := trunc(sysdate, 'hh24') + 30/1440;
END CASE;

  stamp_time := trunc(sysdate, 'hh24');


dbms_output.put_line('scrub counts 1 '||to_char(sysdate,'hh24:mi:ss'));
  FOR v_row IN inbound_counts (start_time, end_time) LOOP
    if (trim(v_row.test_order_flag) = 'N') then
      v_inbound_count := v_row.cnt;
    else
      v_inbound_test_count := v_row.cnt;
    end if;
  END LOOP;
dbms_output.put_line('scrub counts 2 '||to_char(sysdate,'hh24:mi:ss'));
  FOR v_row IN scrub_counts (start_time, end_time) LOOP
    if (trim(v_row.test_order_flag) = 'N') then
      v_scrubbed_count := v_row.cnt;
    else
      v_scrubbed_test_count := v_row.cnt;
    end if;
  END LOOP;
dbms_output.put_line('scrub counts 3 '||to_char(sysdate,'hh24:mi:ss'));
  SELECT count(*) into v_scrub_total
    FROM scrub.order_details sod, scrub.orders so
	WHERE sod.status in ('2002','2003')
	AND so.order_guid = sod.order_guid
	AND UPPER(so.order_origin) not like '%TEST%';
dbms_output.put_line('scrub counts 4 '||to_char(sysdate,'hh24:mi:ss'));
  insert into scrub_hourly_stats (
    scrub_date,
    inbound_count,
    scrubbed_count,
    inbound_test_count,
    scrubbed_test_count,
    scrub_total)
  values (
    stamp_time+v_current_minutes,
    v_inbound_count,
    v_scrubbed_count,
    v_inbound_test_count,
    v_scrubbed_test_count,
    v_scrub_total);

  commit;
dbms_output.put_line('scrub counts 5 '||to_char(sysdate,'hh24:mi:ss'));
  clean_counts(trunc(sysdate), to_number(to_char(sysdate,'HH24')) ,v_stat,v_mess);
  commit;
dbms_output.put_line('scrub counts done '||to_char(sysdate,'hh24:mi:ss'));
end;
.
/
