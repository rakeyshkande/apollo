CREATE OR REPLACE
PACKAGE BODY rpt.PRO01_CSR_ORDER_DETAIL IS

  g_break_sw		varchar2(1) := 'N';
  g_count		number  := 1;
  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure print_line( p_str in varchar2 )
is
  v_out_status_det   varchar2(30);
  v_out_message_det  varchar2(1000);
  v_out_status_sub   varchar2(30);
  v_out_message_sub  varchar2(1000);

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
	rpt.report_pkg.UPDATE_REPORT_DETAIL_CHAR
		(
		g_SUBMISSION_ID,
		p_str||chr(10),
		v_OUT_STATUS_det,
		v_OUT_MESSAGE_det
		);
	if v_out_status_det = 'N' then
		rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
		(
		g_SUBMISSION_ID,
		'REPORT',
		'E',
		v_OUT_MESSAGE_det,
		v_OUT_STATUS_sub,
		v_OUT_MESSAGE_sub
		);
	end if;
  end if;

end;

--****************************************************************************************************
--****************************************************************************************************

procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  print_line( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;

--****************************************************************************************************
--****************************************************************************************************

procedure print_headings(y in number)
is
begin

  print_comment( 'Print Headings' );

  print_line( 'F;R'||to_char(y)||';FG0L;SM1' );

  print_line( 'C;Y'||y||';X2;K"Center"' );
  print_line( 'C;Y'||y||';X3;K"Team"' );
  print_line( 'C;Y'||y||';X4;K"Order Date"' );

  print_line( 'C;Y'||y||';X5;K"Userid"' );
  print_line( 'C;Y'||y||';X6;K"CSR Name"' );
  print_line( 'C;Y'||y||';X8;K"Order Date"' );
  print_line( 'C;Y'||y||';X9;K"DNIS"' );
  print_line( 'C;Y'||y||';X10;K"Customer Name"' );
  print_line( 'C;Y'||y||';X12;K"Order Time"' );
  print_line( 'C;Y'||y||';X13;K"Order Number"' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_headings;

--****************************************************************************************************
--****************************************************************************************************

procedure print_title(p_start_date in date, p_end_date in date)
is
begin

  print_comment( 'Print Title' );

  print_line( 'ID;ORACLE' );

  print_line( 'P;Ph:mm:ss');                 -- SM0
  print_line( 'P;FCourier;M200' );           -- SM1
  print_line( 'P;FCourier;M200;SB' );        -- SM2
  print_line( 'P;FCourier;M200;SUB' );       -- SM3
  print_line( 'P;FCourier;M160' );           -- SM4

  print_line( 'F;R1;FG0L;SM2' );
  print_line( 'C;Y1;X2;K"Customer Sevice Order Detail Report For ' ||
			p_start_date || ' Thru ' || p_end_date || '"' );

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_title;

--****************************************************************************************************
--****************************************************************************************************

procedure sub_totals_for_dates (p_y in number, p_date_y in number)
is
  line varchar2(300);

begin
  g_break_sw := 'Y';

  print_line( 'F;R'||to_char(p_y)||';FG0L;SM1' );
  print_line( 'F;Y'||to_char(p_y)||';X6;FG0R;SM1' );

  line := 'C;Y'|| to_char(p_y) || ';X6';
  line := line || ';ECOUNTA(R'||to_char(p_date_y)||'C:R' ||to_char(p_y-1) || 'C)';
  print_line( line );

end sub_totals_for_dates;

--****************************************************************************************************
--****************************************************************************************************

procedure sub_totals_for_team_or_center (p_y in number, p_cell_totals in varchar2)
is
  line varchar2(300);

begin
  g_break_sw := 'Y';

  print_line( 'F;Y'||to_char(p_y)||';FG0G;SM1' );
  print_line( 'F;Y'||to_char(p_y)||';X6;FG0R;SM1' );

  line := 'C;Y'|| to_char(p_y) || ';X6';
  line := line || ';ESUM('||p_cell_totals||')';
  print_line( line );

end sub_totals_for_team_or_center;

--****************************************************************************************************
--****************************************************************************************************

function print_rows(p_start_date  in date,
                    p_end_date    in date,
                    p_center_name in varchar2,
                    p_team_name   in varchar2,
                    p_user_id     in varchar2,
                    p_dnis_id     in varchar2 ) return number
is

  line    			 varchar2(32767) := null;
  y     			   number          := 4;

  type    			 my_curs_type is REF CURSOR;
  curs    			 my_curs_type;
  str     			 varchar2(5000);

  type           tot_tbl_type is table of varchar2(10)
                 index by pls_integer;

  date_tot_tbl   tot_tbl_type;         -- Keeps track of the cells that contain totals for each DATE changes
                                       -- Will be used for total of each team
  team_tot_tbl   tot_tbl_type;         -- Keeps track of the cells that contain totals for each TEAM changes
                                       -- Will be used for total of each team
  null_tot_tbl   tot_tbl_type;         -- This is used to clear a table

  idx_i          number(4) := 1;       -- Index for date_tot_tbl
  idx_t          number(4) := 1;       -- Index for team_tot_tbl
  date_y         number    := 4;
  center_y       number    := 4;

  v_old_date     varchar2(15) := '01-jan-1900';
  v_old_team     varchar2(80) := 'XXXX';
  v_old_center   ftd_apps.call_center.description%type := 'XXXX';

  type		 ret_rec is record
                 (
                 center_name  varchar2(80),
                 team_name varchar2(80),
                 user_id varchar2(80),
      	         csr_first_name varchar2(80),
      	         csr_last_name varchar2(80),
       	         order_date varchar2(80),
		 dnis_number varchar2(80),
      	         customer_first_name varchar2(80),
      	         customer_last_name varchar2(80),
      	         order_time varchar2(80),
		 order_number varchar2(80)
          	 );

  ret    	 ret_rec;

  v_center_pred	 varchar2(300);
  v_team_pred	 varchar2(300);
  v_user_pred	 varchar2(300);
  v_dnis_pred	 varchar2(300);

begin

  -----------------------------------------------------
  --  Building the predicates (Where clause conditions)
  -----------------------------------------------------

  print_comment( 'Build Parameter Where Clause Predicates' );

  if p_center_name is null
  or p_center_name = 'ALL' then
     v_center_pred := null;
  else
     --v_center_pred:= ' and upper(facc.description)in ('||rpt.multi_value(p_center_name)||') ';
     v_center_pred:= ' and upper(facc.call_center_id)in ('||rpt.multi_value(p_center_name)||') ';
  end if;

  if p_team_name is null
  or p_team_name = 'ALL' then
     v_team_pred := null;
  else
     --v_team_pred:= ' and upper(facg.description) in ('||rpt.multi_value(p_team_name)||') ';
     v_team_pred:= ' and upper(facg.cs_group_id) in ('||rpt.multi_value(p_team_name)||') ';
  end if;

  if p_user_id is null then
     v_user_pred := null;
  else
     v_user_pred:= ' and upper(co.order_taken_by) in ('||rpt.multi_value(p_user_id)||') ';
  end if;

  if p_dnis_id is null then
     v_dnis_pred := null;
  else
     v_dnis_pred := ' and upper(ccl.dnis_id) in ('||rpt.multi_value(p_dnis_id)||') ';
  end if;

  --DBMS_OUTPUT.PUT_LINE( 'Center:     ' || v_center_pred);
  --DBMS_OUTPUT.PUT_LINE( 'Team:       ' || v_team_pred);
  --DBMS_OUTPUT.PUT_LINE( 'User Id:    ' || v_user_pred);
  --DBMS_OUTPUT.PUT_LINE( 'DNIS:       ' || v_dnis_pred);

  --DBMS_OUTPUT.PUT_LINE( 'Start Date: ' || p_start_date);
  --DBMS_OUTPUT.PUT_LINE( 'End Date:   ' || p_end_date);

  -----------------------------------------------------
  --  Define the SELECT
  -----------------------------------------------------
  str := 'select distinct nvl(facc.description, ''Not Available'') center_name,
                 nvl(facg.description, ''Not Available'')  team_name,
                 nvl(co.order_taken_by, '' '')  user_id,
		 nvl(au.first_name, '' '')  csr_first_name,
		 nvl(au.last_name, '' '')  csr_last_name,
                 nvl(to_char(co.order_date, ''mm/dd/yyyy''), '' '')  order_date,
                 nvl(to_char(ccl.dnis_id), '' '')  dnis_number,
                 nvl(cc.first_name, '' '')  customer_first_name,
                 nvl(cc.last_name, '' '')  customer_last_name,
                 nvl(to_char(co.order_date, ''hh:mi:ss''), '' '')  order_time,
                 nvl(to_char(co.master_order_number), '' '')  order_number
	  from   clean.orders co
	  --
          join clean.entity_history ceh
			on (co.order_guid=ceh.entity_id)
	  join clean.customer cc
			on (co.customer_id=cc.customer_id)
	  left outer join clean.call_log ccl
			on (ceh.call_log_id=ccl.call_log_id)
	  left outer join aas.identity ai
			on (co.order_taken_by=ai.identity_id)
	  left outer join aas.user_history auh
			on (ai.user_id = auh.user_id)
	  left outer join aas.users au
			on (auh.user_id = au.user_id)
	  left outer join ftd_apps.call_center facc
			on (auh.call_center_id=facc.call_center_id)
	  left outer join ftd_apps.cs_groups facg
			on (auh.cs_group_id=facg.cs_group_id)
	  --
          where  trunc(co.order_date) between ''' || p_start_date || '''
					  and ''' || p_end_date ||'''
	  and    trunc(co.order_date) between nvl(auh.start_date,trunc(co.order_date))
					  and nvl(auh.end_date,trunc(co.order_date)) '
          || v_user_pred
          || v_center_pred
          || v_team_pred
          || v_dnis_pred
;

  -----------------------------------------------------
  --  Process the Cursor
  -----------------------------------------------------
  OPEN curs FOR str;
       LOOP
       	  FETCH curs INTO ret;
     	    EXIT WHEN curs%NOTFOUND;

					--**************************************************************************
					--CENTER Break  --  Print sub totals for CENTER
					--**************************************************************************
      	  if v_old_center <> ret.center_name and y > 4 then
						 -----------------
						 -- date subtotals
						 -----------------
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
             print_line( line );
             sub_totals_for_dates (y,date_y);
             date_tot_tbl(idx_i) := 'R'||to_char(y)||'C';
             idx_i := idx_i+1;
        	   y:=y+1;
						 -----------------
      	  	 -- team subtotals
						 -----------------
        	   DECLARE
      		     v_cell_totals  varchar2(300);
      	     BEGIN
      	       FOR j in date_tot_tbl.first..date_tot_tbl.last loop
      	   	       v_cell_totals :=  v_cell_totals||date_tot_tbl(j)||',';
      	       END LOOP;
      	       v_cell_totals := rtrim(v_cell_totals,',');
               line := 'C;Y'|| to_char(y) || ';X2';
               line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' thru '||p_end_date||'"';
               print_line( line );
               sub_totals_for_team_or_center (y,v_cell_totals);
               date_tot_tbl := null_tot_tbl;
               team_tot_tbl(idx_t) := 'R'||to_char(y)||'C';
               idx_t := idx_t+1;
               idx_i := idx_i+1;
      	       y:=y+1;
      	     END;
						 ------------------
						 --center subtotals
						 ------------------
	     	     DECLARE
      		     v_cell_totals  varchar2(300);
      	     BEGIN
      	       FOR j in team_tot_tbl.first..team_tot_tbl.last loop
      	   	       v_cell_totals :=  v_cell_totals||team_tot_tbl(j)||',';
      	       END LOOP;
               v_cell_totals := rtrim(v_cell_totals,',');
               line := 'C;Y'|| to_char(y) || ';X2';
               line := line || ';K"Totals for Center '||v_old_center||' for '||p_start_date||' thru '||p_end_date||'"';
               print_line( line );
               sub_totals_for_team_or_center (y,v_cell_totals);
               team_tot_tbl := null_tot_tbl;
      	       y:=y+1;
      	       date_y := y;
      	       center_y := y;
      	     END;
					--**************************************************************************
					--TEAM Break  --  Print sub totals for TEAM
					--**************************************************************************
	     	  elsif v_old_team <> ret.team_name and y > 4 then
	     	     -----------------
						 -- date subtotals
						 -----------------
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
             print_line( line );
             sub_totals_for_dates (y,date_y);
             date_tot_tbl(idx_i) := 'R'||to_char(y)||'C';
             idx_i := idx_i+1;
        	   y:=y+1;
						 -----------------
						 -- team subtotals
						 -----------------
        	   DECLARE
      		     v_cell_totals  varchar2(300);
      	     BEGIN
      	       FOR j in date_tot_tbl.first..date_tot_tbl.last loop
      	   	       v_cell_totals :=  v_cell_totals||date_tot_tbl(j)||',';
      	       END LOOP;
      	       v_cell_totals := rtrim(v_cell_totals,',');
               line := 'C;Y'|| to_char(y) || ';X2';
               line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' thru '||p_end_date||'"';
               print_line( line );
               sub_totals_for_team_or_center (y,v_cell_totals);
               date_tot_tbl := null_tot_tbl;
               team_tot_tbl(idx_t) := 'R'||to_char(y)||'C';
               idx_t := idx_t+1;
               idx_i := idx_i+1;
      	       y:=y+1;
      	       date_y := y;
      	     END;
					--**************************************************************************
					--DATE Break  --  Print sub totals for DATE
					--**************************************************************************
          elsif v_old_date <> ret.order_date and y > 4 then
						 -----------------
						 -- date subtotals
						 -----------------
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
             print_line( line );
             sub_totals_for_dates (y,date_y);
             date_tot_tbl(idx_i) := 'R'||to_char(y)||'C';
             idx_i := idx_i+1;
        	   y:=y+1;
        	   date_y := y;
          end if;

					--**************************************************************************
					--Print BLANK Line
					--**************************************************************************

          if g_break_sw = 'Y' then
             line := 'C;Y'|| to_char(y) || ';X2';
             line := line || ';K" "';
             print_line( line );
             y := y + 1;
             print_line( 'C;Y'||to_char(y) );
             g_break_sw := 'N';
          end if;

					--**************************************************************************
					--Print DETAIL Line
					--**************************************************************************

          line := 'C;Y'|| to_char(y) || ';X2';
          line := line || ';K"'||ret.center_name||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X3';
          line := line || ';K"'||ret.team_name||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X4';
          line := line || ';K"'||ret.order_date||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X5';
          line := line || ';K"'||ret.user_id||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X6';
          line := line || ';K"'||ret.csr_first_name||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X7';
          line := line || ';K"'||ret.csr_last_name||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X8';
          line := line || ';K"'||ret.order_date||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X9';
          line := line || ';K"'||ret.dnis_number||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X10';
          line := line || ';K"'||ret.customer_first_name||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X11';
          line := line || ';K"'||ret.customer_last_name||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X12';
          line := line || ';K"'||ret.order_time||'"';
          print_line( line );

          line := 'C;Y'|| to_char(y) || ';X13';
          line := line || ';K"'||ret.order_number||'"';
          print_line( line );

          y := y + 1;
          print_line( 'C;Y'||to_char(y) );

	  if g_CLOB = 'N' then
	     utl_file.fflush( g_file );
	  end if;

	  v_old_center := ret.center_name;
	  v_old_team   := ret.team_name;
	  v_old_date   := ret.order_date;

       END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 4 then
  		print_comment( 'No Rows Returned' );

      line := 'C;Y'|| to_char(y) || ';X2';
      line := line || ';K"No Data Found"';
      print_line( line );

      y := y + 1;
      print_line( 'C;Y'||to_char(y) );

      if g_CLOB = 'N' then
	 utl_file.fflush( g_file );
      end if;

  else

  --**************************************************************************
  --Print sub totals for last DATE
  --**************************************************************************
  line := 'C;Y'|| to_char(y) || ';X2';
  line := line || ';K"Totals for Team '||v_old_team||' on '||v_old_date||'"';
  print_line( line );
  sub_totals_for_dates (y,date_y);
  date_tot_tbl(idx_i) := 'R'||to_char(y)||'C';
  idx_i := idx_i+1;
  y:=y+1;
  date_y := y;

  --**************************************************************************
  --Print sub totals for last TEAM
  --**************************************************************************
  DECLARE
    v_cell_totals  varchar2(300);
  BEGIN
    dbms_output.put_line(date_tot_tbl.count);
    FOR j in date_tot_tbl.first..date_tot_tbl.last loop
     	  v_cell_totals :=  v_cell_totals||date_tot_tbl(j)||',';
				dbms_output.put_line( v_cell_totals);
		END LOOP;
		v_cell_totals := rtrim(v_cell_totals,',');
		dbms_output.put_line( v_cell_totals);
    --print_line( 'F;Y'||to_char(y)||';X2;FG0L;SM2' );
    line := 'C;Y'|| to_char(y) || ';X2';
    line := line || ';K"Totals for Team '||v_old_team||' for '||p_start_date||' thru '||p_end_date||'"';
    print_line( line );
		sub_totals_for_team_or_center (y,v_cell_totals);
		date_tot_tbl := null_tot_tbl;
		team_tot_tbl(idx_t) := 'R'||to_char(y)||'C';
		idx_t := idx_t+1;
    idx_i := idx_i+1;
    y:=y+1;
    date_y := y;
	END;

  --**************************************************************************
  --Print totals for last CENTER
  --**************************************************************************
	DECLARE
	  v_cell_totals  varchar2(300);
	BEGIN
	  dbms_output.put_line(date_tot_tbl.count);
    FOR j in team_tot_tbl.first..team_tot_tbl.last loop
     	  v_cell_totals :=  v_cell_totals||team_tot_tbl(j)||',';
				dbms_output.put_line( v_cell_totals);
		END LOOP;
		v_cell_totals := rtrim(v_cell_totals,',');
		line := 'C;Y'|| to_char(y) || ';X2';
		line := line || ';K"Totals for Center '||ret.center_name||' for '||p_start_date||' thru '||p_end_date||'"';
		print_line( line );
		sub_totals_for_team_or_center (y,v_cell_totals);
		team_tot_tbl := null_tot_tbl;
		y:=y+1;
		center_y := y;
		date_y := y;
	END;

  end if;

  --**************************************************************************
  --Ceanup Stuff - Close Cursor and Return
  --**************************************************************************
  CLOSE curs;

  RETURN (y);

end print_rows;

--****************************************************************************************************
--****************************************************************************************************

procedure print_widths
is
begin

  print_comment( 'Format Column Widths' );

  print_line( 'F;W1 1 5' );       -- margin
  print_line( 'F;W2 2 20' );      -- center
  print_line( 'F;W3 3 20' );      -- team
  print_line( 'F;W4 4 15' );      -- order date
  print_line( 'F;W5 5 15' );		  -- user id
  print_line( 'F;W6 6 15' );		  -- csr first name
  print_line( 'F;W7 7 20' );	  	-- csr last name
  print_line( 'F;W8 8 15' ); 		  -- order date
  print_line( 'F;W9 9 15' ); 		  -- dnis number
  print_line( 'F;W10 10 15' );	  -- customer first name
  print_line( 'F;W11 11 20' );    -- customer last name
  print_line( 'F;W12 12 15' );    -- order time
  print_line( 'F;W13 13 15' );    -- order number

  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_widths;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure run_report	(p_start_date in varchar2,
			 p_end_date in varchar2,
			 p_center_name in varchar2,
			 p_team_name in varchar2,
			 p_user_id in varchar2,
			 p_dnis_id in varchar2,
			 p_submission_id in number,
			 p_CLOB in varchar2
			)
is
  v_start_date date;
  v_end_date date;
  l_row_cnt number(9);
  v_OUT_STATUS varchar2(30);
  v_OUT_MESSAGE varchar2(1000);

begin
    v_start_date := to_date(p_start_date,'mm/dd/yyyy');
    v_end_date   := to_date(p_end_date,'mm/dd/yyyy');

    if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
                                P_SUBMISSION_ID,
                                'SYS',
                                'S' ,
                                null ,
                                v_OUT_STATUS ,
                                v_OUT_MESSAGE
                        );
       commit;
       rpt.report_pkg.INSERT_REPORT_DETAIL_CHAR
       (
          P_SUBMISSION_ID,
          empty_clob(),--IN_REPORT_DETAIL
          v_out_status,
          v_out_message
        );
    else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','PRO01_CSR_Order_Detail.slk', 'W' );
    end if;

  if p_start_date is NULL then
     v_start_date := sysdate;
  else
     v_start_date := to_date(p_start_date, 'mm/dd/yyyy');
  end if;

  if p_end_date is NULL then
     v_end_date := sysdate;
  else
     v_end_date := to_date(p_end_date, 'mm/dd/yyyy');
  end if;

  print_title(	v_start_date,
		v_end_date );

  print_headings(3);

  l_row_cnt := print_rows
			(v_start_date,
			 v_end_date,
			 p_center_name,
			 p_team_name,
			 p_user_id,
			 p_dnis_id
			);

  print_widths;

  print_line( 'E' );

    if p_CLOB = 'Y' then
	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
		(
		P_SUBMISSION_ID,
		'SYS',
		'C' ,
		null ,
		v_OUT_STATUS ,
		v_OUT_MESSAGE
		);
	commit;
    else
	utl_file.fflush( g_file );
	utl_file.fclose( g_file );
    end if;

end run_report;

end;
.
/
