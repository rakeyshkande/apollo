CREATE OR REPLACE
procedure rpt.ED03_Mod_Del_Dat_Pop_Tab
(p_start_date in varchar2,
 p_end_date in varchar2 )
is
v_start_date date := to_date(p_start_date,'mm/dd/yyyy');
v_end_date date := to_date(p_end_date,'mm/dd/yyyy');
BEGIN
insert into rpt.rep_tab
(col1,
col2,
col3,
col4,
col5,
col6,
col7,
col8,
col9,
col10,
col11,
col12)
select       external_order_number Order_number,
	cod.eod_delivery_date delivery_date,
             cod.delivery_date Modified_delivery_date,
             cat.Product_amount Merch_value,
             cat.add_on_amount add_ons,
             cat.shipping_fee shipping_fee,
             cat.service_fee  service_fee,
             cat.discount_amount discount_amount,
             cat.Product_amount+
               cat.add_on_amount +
               cat.shipping_fee +
               cat.service_fee  -
               cat.discount_amount Order_value,
             cat.tax+cat.shipping_tax+cat.service_fee_tax tax,
             cat.Product_amount+
              cat.add_on_amount +
              cat.shipping_fee +
              cat.service_fee  -
              cat.discount_amount+
              cat.tax+cat.shipping_tax+cat.service_fee_tax             total_order_amount,
	cat.transaction_date transaction_date
from clean.order_details cod
join clean.accounting_transactions cat
on (cat.order_detail_id = cod.order_detail_id)
Where (trunc(cod.eod_delivery_date) < trunc(cod.delivery_date)
      and (trunc(cat.transaction_date) between v_start_date
                                        and v_end_date))
order by cod.eod_delivery_date;

end;
.
/
