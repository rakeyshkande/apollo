SET DEFINE OFF;

CREATE OR REPLACE
PACKAGE BODY rpt.csr04_holiday_queue IS

  g_SUBMISSION_ID	number(22);
  g_CLOB		varchar2(1);
  g_file utl_file.file_type;

--******************************************************************
-- This Proc will print a line to the SLK file.
--******************************************************************

procedure print_line( p_str in varchar2 )
is

begin

  if g_CLOB = 'N' then
     utl_file.put_line( g_file, p_str );
  else
     begin
	rpt.report_clob_pkg.clob_variable_update(p_str);
     --exception
     --	when others then srw.message(32321,'Error in print_line');
    end;
  end if;

end;
--****************************************************************************************************
--
--****************************************************************************************************
procedure print_comment( p_comment in varchar2 )
is
begin

  return;
  print_line( ';' || chr(10) || '; ' || p_comment || chr(10) || ';' );

end print_comment;

--****************************************************************************************************
--****************************************************************************************************
procedure print_heading( y in number )
is
begin

  print_comment( 'Print Headings' );

  print_line( 'C;Y4;X1;K"Scrub Start"' );
  print_line( 'C;Y5;X1;K"Scrub Incoming"' );
  print_line( 'C;Y6;X1;K"Scrub Worked"' );
  print_line( 'C;Y7;X1;K"Scrub End"' );
  print_line( 'C;Y8;X1;K"Merc Queue Start"' );
  print_line( 'C;Y9;X1;K"Merc Queue Incoming (excl.auto)"' );
  print_line( 'C;Y10;X1;K"Merc Queue Worked (excl.auto)"' );
  print_line( 'C;Y11;X1;K"Merc Queue End"' );
  print_line( 'C;Y12;X1;K"Merc Queue Tagged"' );
  print_line( 'C;Y13;X1;K"Merc Queue UnTagged"' );
  print_line( 'C;Y14;X1;K"FTD Start"' );
  print_line( 'C;Y15;X1;K"FTD Incoming"' );
  print_line( 'C;Y16;X1;K"FTD Worked"' );
  print_line( 'C;Y17;X1;K"FTD End"' );
  print_line( 'C;Y18;X1;K"FTD Tagged"' );
  print_line( 'C;Y19;X1;K"FTD Untagged"' );
  print_line( 'C;Y20;X1;K"FTDM Start"' );
  print_line( 'C;Y21;X1;K"FTDM Incoming"' );
  print_line( 'C;Y22;X1;K"FTDM Worked"' );
  print_line( 'C;Y23;X1;K"FTDM End"' );
  print_line( 'C;Y24;X1;K"FTDM Tagged"' );
  print_line( 'C;Y25;X1;K"FTDM Untagged"' );
  print_line( 'C;Y26;X1;K"ASK Start"' );
  print_line( 'C;Y27;X1;K"ASK Incoming"' );
  print_line( 'C;Y28;X1;K"ASK Worked"' );
  print_line( 'C;Y29;X1;K"ASK End"' );
  print_line( 'C;Y30;X1;K"ASK Tagged"' );
  print_line( 'C;Y31;X1;K"ASK Untagged"' );
  print_line( 'C;Y32;X1;K"REJ Start"' );
  print_line( 'C;Y33;X1;K"REJ Incoming"' );
  print_line( 'C;Y34;X1;K"REJ Worked"' );
  print_line( 'C;Y35;X1;K"REJ Auto-Resent"' );
  print_line( 'C;Y36;X1;K"REJ End"' );
  print_line( 'C;Y37;X1;K"REJ Tagged"' );
  print_line( 'C;Y38;X1;K"REJ Untagged"' );
  print_line( 'C;Y39;X1;K"ANS Start"' );
  print_line( 'C;Y40;X1;K"ANS Incoming"' );
  print_line( 'C;Y41;X1;K"ANS Worked"' );
  print_line( 'C;Y42;X1;K"ANS End"' );
  print_line( 'C;Y43;X1;K"ANS Tagged"' );
  print_line( 'C;Y44;X1;K"ANS Untagged"' );
  print_line( 'C;Y45;X1;K"DEN Start"' );
  print_line( 'C;Y46;X1;K"DEN Incoming"' );
  print_line( 'C;Y47;X1;K"DEN Worked"' );
  print_line( 'C;Y48;X1;K"DEN End"' );
  print_line( 'C;Y49;X1;K"DEN Tagged"' );
  print_line( 'C;Y50;X1;K"DEN Untagged"' );
  print_line( 'C;Y51;X1;K"CON Start"' );
  print_line( 'C;Y52;X1;K"CON Incoming"' );
  print_line( 'C;Y53;X1;K"CON Worked"' );
  print_line( 'C;Y54;X1;K"CON End"' );
  print_line( 'C;Y55;X1;K"CON Tagged"' );
  print_line( 'C;Y56;X1;K"CON Untagged"' );
  print_line( 'C;Y57;X1;K"CAN Start"' );
  print_line( 'C;Y58;X1;K"CAN Incoming"' );
  print_line( 'C;Y59;X1;K"CAN Worked"' );
  print_line( 'C;Y60;X1;K"CAN End"' );
  print_line( 'C;Y61;X1;K"CAN Tagged"' );
  print_line( 'C;Y62;X1;K"CAN Untagged"' );
  print_line( 'C;Y63;X1;K"GEN Start"' );
  print_line( 'C;Y64;X1;K"GEN Incoming"' );
  print_line( 'C;Y65;X1;K"GEN Worked"' );
  print_line( 'C;Y66;X1;K"GEN End"' );
  print_line( 'C;Y67;X1;K"GEN Tagged"' );
  print_line( 'C;Y68;X1;K"GEN Untagged"' );
  print_line( 'C;Y69;X1;K"ZIP Start"' );
  print_line( 'C;Y70;X1;K"ZIP Incoming"' );
  print_line( 'C;Y71;X1;K"ZIP Worked"' );
  print_line( 'C;Y72;X1;K"ZIP End"' );
  print_line( 'C;Y73;X1;K"ZIP Tagged"' );
  print_line( 'C;Y74;X1;K"ZIP Untagged"' );
  print_line( 'C;Y75;X1;K"CRD Start"' );
  print_line( 'C;Y76;X1;K"CRD Incoming"' );
  print_line( 'C;Y77;X1;K"CRD Worked"' );
  print_line( 'C;Y78;X1;K"CRD End"' );
  print_line( 'C;Y79;X1;K"CRD Tagged"' );
  print_line( 'C;Y80;X1;K"CRD Untagged"' );
  print_line( 'C;Y81;X1;K"LP Start"' );
  print_line( 'C;Y82;X1;K"LP Incoming"' );
  print_line( 'C;Y83;X1;K"LP Worked"' );
  print_line( 'C;Y84;X1;K"LP End"' );
  print_line( 'C;Y85;X1;K"LP Tagged"' );
  print_line( 'C;Y86;X1;K"LP Untagged"' );
  print_line( 'C;Y87;X1;K"ADJ Start"' );
  print_line( 'C;Y88;X1;K"ADJ Incoming"' );
  print_line( 'C;Y89;X1;K"ADJ Worked"' );
  print_line( 'C;Y90;X1;K"ADJ End"' );
  print_line( 'C;Y91;X1;K"ADJ Tagged"' );
  print_line( 'C;Y92;X1;K"ADJ Untagged"' );
  print_line( 'C;Y93;X1;K"EMAIL Start"' );
  print_line( 'C;Y94;X1;K"EMAIL Incoming"' );
  print_line( 'C;Y95;X1;K"EMAIL Worked"' );
  print_line( 'C;Y96;X1;K"EMAIL Auto Handled"' );
  print_line( 'C;Y97;X1;K"EMAIL End"' );
  print_line( 'C;Y98;X1;K"EMAIL Tagged"' );
  print_line( 'C;Y99;X1;K"EMAIL Untagged"' );
  print_line( 'C;Y100;X1;K"CX Start"' );
  print_line( 'C;Y101;X1;K"CX Incoming"' );
  print_line( 'C;Y102;X1;K"CX Worked"' );
  print_line( 'C;Y103;X1;K"CX End"' );
  print_line( 'C;Y104;X1;K"CX Tagged"' );
  print_line( 'C;Y105;X1;K"CX Untagged"' );
  print_line( 'C;Y106;X1;K"MO Start"' );
  print_line( 'C;Y107;X1;K"MO Incoming"' );
  print_line( 'C;Y108;X1;K"MO Worked"' );
  print_line( 'C;Y109;X1;K"MO End"' );
  print_line( 'C;Y110;X1;K"MO Tagged"' );
  print_line( 'C;Y111;X1;K"MO Untagged"' );
  print_line( 'C;Y112;X1;K"ND Start"' );
  print_line( 'C;Y113;X1;K"ND Incoming"' );
  print_line( 'C;Y114;X1;K"ND Worked"' );
  print_line( 'C;Y115;X1;K"ND End"' );
  print_line( 'C;Y116;X1;K"ND Tagged"' );
  print_line( 'C;Y117;X1;K"ND Untagged"' );
  print_line( 'C;Y118;X1;K"QI Start"' );
  print_line( 'C;Y119;X1;K"QI Incoming"' );
  print_line( 'C;Y120;X1;K"QI Worked"' );
  print_line( 'C;Y121;X1;K"QI End"' );
  print_line( 'C;Y122;X1;K"QI Tagged"' );
  print_line( 'C;Y123;X1;K"QI Untagged"' );
  print_line( 'C;Y124;X1;K"PD Start"' );
  print_line( 'C;Y125;X1;K"PD Incoming"' );
  print_line( 'C;Y126;X1;K"PD Worked"' );
  print_line( 'C;Y127;X1;K"PD End"' );
  print_line( 'C;Y128;X1;K"PD Tagged"' );
  print_line( 'C;Y129;X1;K"PD Untagged"' );
  print_line( 'C;Y130;X1;K"BD Start"' );
  print_line( 'C;Y131;X1;K"BD Incoming"' );
  print_line( 'C;Y132;X1;K"BD Worked"' );
  print_line( 'C;Y133;X1;K"BD End"' );
  print_line( 'C;Y134;X1;K"BD Tagged"' );
  print_line( 'C;Y135;X1;K"BD Untagged"' );
  print_line( 'C;Y136;X1;K"SE Start"' );
  print_line( 'C;Y137;X1;K"SE Incoming"' );
  print_line( 'C;Y138;X1;K"SE Worked"' );
  print_line( 'C;Y139;X1;K"SE End"' );
  print_line( 'C;Y140;X1;K"SE Tagged"' );
  print_line( 'C;Y141;X1;K"SE Untagged"' );
  print_line( 'C;Y142;X1;K"DI Start"' );
  print_line( 'C;Y143;X1;K"DI Incoming"' );
  print_line( 'C;Y144;X1;K"DI Worked"' );
  print_line( 'C;Y145;X1;K"DI End"' );
  print_line( 'C;Y146;X1;K"DI Tagged"' );
  print_line( 'C;Y147;X1;K"DI Untagged"' );
  print_line( 'C;Y148;X1;K"QC Start"' );
  print_line( 'C;Y149;X1;K"QC Incoming"' );
  print_line( 'C;Y150;X1;K"QC Worked"' );
  print_line( 'C;Y151;X1;K"QC End"' );
  print_line( 'C;Y152;X1;K"QC Tagged"' );
  print_line( 'C;Y153;X1;K"QC Untagged"' );
  print_line( 'C;Y154;X1;K"OA Start"' );
  print_line( 'C;Y155;X1;K"OA Incoming"' );
  print_line( 'C;Y156;X1;K"OA Worked"' );
  print_line( 'C;Y157;X1;K"OA End"' );
  print_line( 'C;Y158;X1;K"OA Tagged"' );
  print_line( 'C;Y159;X1;K"OA Untagged"' );
  print_line( 'C;Y160;X1;K"OT Start"' );
  print_line( 'C;Y161;X1;K"OT Incoming"' );
  print_line( 'C;Y162;X1;K"OT Worked"' );
  print_line( 'C;Y163;X1;K"OT End"' );
  print_line( 'C;Y164;X1;K"OT Tagged"' );
  print_line( 'C;Y165;X1;K"OT Untagged"' );
  print_line( 'C;Y166;X1;K"Total Tagged (Merc & Email)"' );
  print_line( 'C;Y167;X1;K"Total Untagged (Merc & Email)"' );
  print_line( 'C;Y168;X1;K"View Queue Count"' );
  
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_heading;

--****************************************************************************************************
--****************************************************************************************************

procedure print_title(p_run_date in date)
is

begin

  print_comment( 'Print Title' );

  print_line( 'ID;ORACLE' );

  print_line( 'P;Ph:mm:ss');                 -- SM0
  print_line( 'P;FCourier;M200' );           -- SM1
  print_line( 'P;FCourier;M200;SB' );        -- SM2
  print_line( 'P;FCourier;M200;SUB' );       -- SM3
  print_line( 'P;FCourier;M160' );           -- SM4

  print_line( 'F;R1;FG0L;SM2' );
  print_line( 'C;Y1;X1;K"Holiday Queue Report For '||to_char(p_run_date,'mm/dd/yyyy')||'"' );
  if g_CLOB = 'N' then
     utl_file.fflush( g_file );
  end if;

end print_title;

--****************************************************************************************************
--  The print_rows function adds the data records to the file.  In this example there are actually
--  two cursors.  The cursor is just your query from you data model.  If you have a complex data
--  model you will have to some additional PL/SQL to get all of your data.
--****************************************************************************************************
function print_rows(p_run_date    in date ) return number
is
  line       varchar2(32767) := null;  -- print line
  y          number          := 2;    -- first detail line number (row)

  type           my_curs_type is REF CURSOR;
  curs           my_curs_type;

  type ret_rec is record (
      now_hour                 varchar2(80),
      ampm                     varchar2(10),
      scrub_start number,
      scrub_first_hour number,
      scrub_end number,
      scrub_incoming number,
      scrub_worked number,
      merc_start number,
      merc_first_hour number,
      merc_queue_incoming number,
      merc_queue_end number,
      mercury_worked number,
      merc_queue_tagged number,
      mercury_untagged number,      
    ftd_start number,
	  ftd_first_hour number,
	  ftd_queue_incoming number,
	  ftd_queue_end number,
	  ftd_worked number,
	  ftd_queue_tagged number,
	  ftd_untagged number,    
    ftdm_start number,
	  ftdm_first_hour number,
	  ftdm_queue_incoming number,
	  ftdm_queue_end number,
	  ftdm_worked number,
	  ftdm_queue_tagged number,
	  ftdm_untagged number,
    ask_start number,
	  ask_first_hour number,
	  ask_queue_incoming number,
	  ask_queue_end number,
	  ask_worked number,
	  ask_queue_tagged number,
	  ask_untagged number,	  
	  ans_start number,
	  ans_first_hour number,
	  ans_queue_incoming number,
	  ans_queue_end number,
	  ans_worked number,
	  ans_queue_tagged number,
	  ans_untagged number,
    reject_start number,
	  reject_first_hour number,
	  reject_queue_incoming number,
	  reject_worked number,
    reject_queue_auto number,
    reject_queue_end number,
    reject_queue_tagged number,
	  reject_untagged number,
    con_start number,
	  con_first_hour number,
	  con_queue_incoming number,
	  con_queue_end number,
	  con_worked number,
	  con_queue_tagged number,
	  con_untagged number,
    can_start number,
	  can_first_hour number,
	  can_queue_incoming number,
	  can_queue_end number,
	  can_worked number,
	  can_queue_tagged number,
	  can_untagged number,
    gen_start number,
	  gen_first_hour number,
	  gen_queue_incoming number,
	  gen_queue_end number,
	  gen_worked number,
	  gen_queue_tagged number,
	  gen_untagged number,
    zip_start number,
	  zip_first_hour number,
	  zip_queue_incoming number,
	  zip_queue_end number,
	  zip_worked number,
	  zip_queue_tagged number,
	  zip_untagged number,
    crd_start number,
	  crd_first_hour number,
	  crd_queue_incoming number,
	  crd_queue_end number,
	  crd_worked number,
	  crd_queue_tagged number,
	  crd_untagged number,
    lp_start number,
	  lp_first_hour number,
	  lp_queue_incoming number,
	  lp_queue_end number,
	  lp_worked number,
	  lp_queue_tagged number,
	  lp_untagged number,
    adj_start number,
	  adj_first_hour number,
	  adj_queue_incoming number,
	  adj_queue_end number,
	  adj_worked number,
	  adj_queue_tagged number,
	  adj_untagged number,
    den_start number,
	  den_first_hour number,
	  den_queue_incoming number,
	  den_queue_end number,
	  den_worked number,
	  den_queue_tagged number,
	  den_untagged number,
    cx_start number,
	  cx_first_hour number,
	  cx_queue_incoming number,
	  cx_queue_end number,
	  cx_worked number,
	  cx_queue_tagged number,
	  cx_untagged number,
    mo_start number,
	  mo_first_hour number,
	  mo_queue_incoming number,
	  mo_queue_end number,
	  mo_worked number,
	  mo_queue_tagged number,
	  mo_untagged number,
    nd_start number,
	  nd_first_hour number,
	  nd_queue_incoming number,
	  nd_queue_end number,
	  nd_worked number,
	  nd_queue_tagged number,
	  nd_untagged number,
    qi_start number,
	  qi_first_hour number,
	  qi_queue_incoming number,
	  qi_queue_end number,
	  qi_worked number,
	  qi_queue_tagged number,
	  qi_untagged number,
    pd_start number,
	  pd_first_hour number,
	  pd_queue_incoming number,
	  pd_queue_end number,
	  pd_worked number,
	  pd_queue_tagged number,
	  pd_untagged number,
    bd_start number,
	  bd_first_hour number,
	  bd_queue_incoming number,
	  bd_queue_end number,
	  bd_worked number,
	  bd_queue_tagged number,
	  bd_untagged number,
    se_start number,
	  se_first_hour number,
	  se_queue_incoming number,
	  se_queue_end number,
	  se_worked number,
	  se_queue_tagged number,
	  se_untagged number,
    di_start number,
	  di_first_hour number,
	  di_queue_incoming number,
	  di_queue_end number,
	  di_worked number,
	  di_queue_tagged number,
	  di_untagged number,
    qc_start number,
	  qc_first_hour number,
	  qc_queue_incoming number,
	  qc_queue_end number,
	  qc_worked number,
	  qc_queue_tagged number,
	  qc_untagged number,
    oa_start number,
	  oa_first_hour number,
	  oa_queue_incoming number,
	  oa_queue_end number,
	  oa_worked number,
	  oa_queue_tagged number,
	  oa_untagged number,
    ot_start number,
	  ot_first_hour number,
	  ot_queue_incoming number,
	  ot_queue_end number,
	  ot_worked number,
	  ot_queue_tagged number,
	  ot_untagged number,
    email_start number,
	  email_first_hour number,
	  email_queue_incoming number,
    email_worked number,
    email_auto_handle number,
	  email_queue_end number,
	  email_queue_tagged number,
	  email_untagged number,
    view_queue_cnt number,
    email_merc_tagged number,
    email_merc_untagged number);

  ret           ret_rec;

begin

  OPEN curs FOR 
    SELECT 
      ltrim(to_char(clean_date,'HH:mi AM'),0) as now_hour,
      to_char(clean_date,'A.M.' ) as ampm,
      sdv.scrub_start,
      scrub_first_hour,
      scrub_total as scrub_end,
      inbound_count as scrub_incoming,
      scrubbed_count as scrub_worked,
      sdv.merc_start,
      merc_first_hour,
      merc_queue_incoming,
      merc_queue_end,
      ((sdv.merc_start + merc_queue_incoming ) - merc_queue_end) as mercury_worked,
      merc_queue_tagged,
      (merc_queue_end - merc_queue_tagged) as mercury_untagged,
      --
      sdv.ftd_start,
      ftd_first_hour, 
      ftd_queue_incoming,
      ftd_queue_end,
      ((sdv.ftd_start + ftd_queue_incoming) - ftd_queue_end) as ftd_worked,
      ftd_queue_tagged,
      (ftd_queue_end - ftd_queue_tagged) as ftd_untagged,
      --
      sdv.ftdm_start,  
      ftdm_first_hour, 
      ftdm_queue_incoming,
      ftdm_queue_end,
      ((sdv.ftdm_start + ftdm_queue_incoming) - ftdm_queue_end) as ftdm_worked, 
      ftdm_queue_tagged,
      (ftdm_queue_end - ftdm_queue_tagged) as ftdm_untagged,
      -- 
      sdv.ask_start,  
      ask_first_hour, 
      ask_queue_incoming,
      ask_queue_end,
      ((sdv.ask_start + ask_queue_incoming) - ask_queue_end) as ask_worked,
      ask_queue_tagged,
      (ask_queue_end - ask_queue_tagged) as ask_untagged, 
      --
      sdv.ans_start,  
      ans_first_hour, 
      ans_queue_incoming,
      ans_queue_end,
      ((sdv.ans_start + ans_queue_incoming) - ans_queue_end) as ans_worked,
      ans_queue_tagged,
      (ans_queue_end - ans_queue_tagged) as ans_untagged,
      --
      sdv.reject_start,  
      reject_first_hour,  
      reject_queue_incoming,
      ((sdv.reject_start + reject_queue_incoming) - reject_queue_end) as reject_worked,  
      reject_queue_auto,
      reject_queue_end,
      reject_queue_tagged, 
      (reject_queue_end - reject_queue_tagged) as reject_untagged,
      --
      sdv.con_start,  
      con_first_hour, 
      con_queue_incoming,
      con_queue_end,
      ((sdv.con_start + con_queue_incoming) - con_queue_end) as con_worked,
      con_queue_tagged,
      (con_queue_end - con_queue_tagged) as con_untagged,
      --
      sdv.can_start,  
      can_first_hour, 
      can_queue_incoming,
      can_queue_end,
      ((sdv.can_start + can_queue_incoming) - can_queue_end) as can_worked,
      can_queue_tagged,
      (can_queue_end - can_queue_tagged) as can_untagged,
      --
      sdv.gen_start,  
      gen_first_hour, 
      gen_queue_incoming,
      gen_queue_end,
      ((sdv.gen_start + gen_queue_incoming) - gen_queue_end) as gen_worked,
      gen_queue_tagged,
      (gen_queue_end - gen_queue_tagged) as gen_untagged,
      --
      sdv.zip_start,  
      zip_first_hour, 
      zip_queue_incoming,
      zip_queue_end,
      ((sdv.zip_start + zip_queue_incoming) - zip_queue_end) as zip_worked,
      zip_queue_tagged,
      (zip_queue_end - zip_queue_tagged) as zip_untagged, 
      --
      sdv.crd_start,  
      crd_first_hour, 
      crd_queue_incoming,
      crd_queue_end,
      ((sdv.crd_start + crd_queue_incoming) - crd_queue_end) as crd_worked,
      crd_queue_tagged,
      (crd_queue_end - crd_queue_tagged) as crd_untagged,
      --
      sdv.lp_start,  
      lp_first_hour, 
      lp_queue_incoming,
      lp_queue_end,
      ((sdv.lp_start + lp_queue_incoming) - lp_queue_end) as lp_worked,
      lp_queue_tagged,
      (lp_queue_end - lp_queue_tagged) as lp_untagged, 
      --
      sdv.adj_start,  
      adj_first_hour, 
      adj_queue_incoming,
      adj_queue_end,
      ((sdv.adj_start + adj_queue_incoming) - adj_queue_end) as adj_worked,
      adj_queue_tagged,
      (adj_queue_end - adj_queue_tagged) as adj_untagged,
      --
      sdv.den_start,  
      den_first_hour, 
      den_queue_incoming,
      den_queue_end,
      ((sdv.den_start + den_queue_incoming) - den_queue_end) as den_worked,
      den_queue_tagged,
      (den_queue_end - den_queue_tagged) as den_untagged, 
      --
      sdv.cx_start,  
      cx_first_hour, 
      cx_queue_incoming,
      cx_queue_end,
      ((sdv.cx_start + cx_queue_incoming) - cx_queue_end) as cx_worked,
      cx_queue_tagged,
      (cx_queue_end - cx_queue_tagged) as cx_untagged,
      --
      sdv.mo_start,  
      mo_first_hour, 
      mo_queue_incoming,
      mo_queue_end,
      ((sdv.mo_start + mo_queue_incoming) - mo_queue_end) as mo_worked,
      mo_queue_tagged,
      (mo_queue_end - mo_queue_tagged) as mo_untagged, 
      --
      sdv.nd_start,  
      nd_first_hour, 
      nd_queue_incoming,
      nd_queue_end,
      ((sdv.nd_start + nd_queue_incoming) - nd_queue_end) as nd_worked,
      nd_queue_tagged,
      (nd_queue_end - nd_queue_tagged) as nd_untagged,
      --
      sdv.qi_start,  
      qi_first_hour, 
      qi_queue_incoming,
      qi_queue_end,
      ((sdv.qi_start + qi_queue_incoming) - qi_queue_end) as qi_worked,
      qi_queue_tagged,
      (qi_queue_end - qi_queue_tagged) as qi_untagged, 
      --
      sdv.pd_start,  
      pd_first_hour, 
      pd_queue_incoming,
      pd_queue_end,
      ((sdv.pd_start + pd_queue_incoming) - pd_queue_end) as pd_worked,
      pd_queue_tagged,
      (pd_queue_end - pd_queue_tagged) as pd_untagged,
      --
      sdv.bd_start,  
      bd_first_hour, 
      bd_queue_incoming,
      bd_queue_end,
      ((sdv.bd_start + bd_queue_incoming) - bd_queue_end) as bd_worked,
      bd_queue_tagged,
      (bd_queue_end - bd_queue_tagged) as bd_untagged, 
      --
      sdv.se_start,  
      se_first_hour, 
      se_queue_incoming,
      se_queue_end,
      ((sdv.se_start + se_queue_incoming) - se_queue_end) as se_worked,
      se_queue_tagged,
      (se_queue_end - se_queue_tagged) as se_untagged,
      --
      sdv.di_start,  
      di_first_hour, 
      di_queue_incoming,
      di_queue_end,
      ((sdv.di_start + di_queue_incoming) - di_queue_end) as di_worked,
      di_queue_tagged,
      (di_queue_end - di_queue_tagged) as di_untagged, 
      --
      sdv.qc_start,  
      qc_first_hour, 
      qc_queue_incoming,
      qc_queue_end,
      ((sdv.qc_start + qc_queue_incoming) - qc_queue_end) as qc_worked,
      qc_queue_tagged,
      (qc_queue_end - qc_queue_tagged) as qc_untagged,
      --
      sdv.oa_start,  
      oa_first_hour, 
      oa_queue_incoming,
      oa_queue_end,
      ((sdv.oa_start + oa_queue_incoming) - oa_queue_end) as oa_worked,
      oa_queue_tagged,
      (oa_queue_end - oa_queue_tagged) as oa_untagged, 
      --
      sdv.ot_start,  
      ot_first_hour, 
      ot_queue_incoming,
      ot_queue_end,
      ((sdv.ot_start + ot_queue_incoming) - ot_queue_end) as ot_worked,
      ot_queue_tagged,
      (ot_queue_end - ot_queue_tagged) as ot_untagged, 
      --
      sdv.email_start,    
      email_first_hour,
      email_queue_incoming,
      email_worked,
      email_auto_handle,
      email_queue_end,
      email_queue_tagged,
      (email_queue_end - email_queue_tagged) as email_untagged,
      view_queue_cnt,
      ( merc_queue_tagged + email_queue_tagged) as email_merc_tagged,
      (merc_queue_end - merc_queue_tagged)  + (email_queue_end - email_queue_tagged) as email_merc_untagged
    from rpt.Hourly_Stats_VW,
    ( select
      to_char(sd.clean_date, 'mm/dd/yyyy hh24:mi' ) one_hour_ago,
      sd.scrub_total as scrub_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.scrub_total,'0') as scrub_first_hour,
      sd.ftd_queue_end as ftd_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.merc_queue_end,'0') as merc_first_hour,
      sd.merc_queue_end as merc_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.ftd_queue_end,'0') as ftd_first_hour,
      sd.ftdm_queue_end as ftdm_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.ftdm_queue_end,'0') as ftdm_first_hour,
      sd.ask_queue_end as ask_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.ask_queue_end,'0') as ask_first_hour,
      sd.reject_queue_end as reject_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.reject_queue_end,'0') as reject_first_hour, 
      --
      sd.ans_queue_end as ans_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.ans_queue_end,'0') as ans_first_hour, 
      sd.con_queue_end as con_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.con_queue_end,'0') as con_first_hour, 
      sd.can_queue_end as can_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.can_queue_end,'0') as can_first_hour, 
      sd.gen_queue_end as gen_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.gen_queue_end,'0') as gen_first_hour, 
      sd.zip_queue_end as zip_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.zip_queue_end,'0') as zip_first_hour, 
      sd.crd_queue_end as crd_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.crd_queue_end,'0') as crd_first_hour, 
      sd.lp_queue_end as lp_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.lp_queue_end,'0') as lp_first_hour, 
      sd.adj_queue_end as adj_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.adj_queue_end,'0') as adj_first_hour, 
      sd.den_queue_end as den_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.den_queue_end,'0') as den_first_hour, 
      --
      sd.email_queue_end as email_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.email_queue_end,'0') as email_first_hour, 
      sd.cx_queue_end as cx_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.cx_queue_end,'0') as cx_first_hour,
      sd.mo_queue_end as mo_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.mo_queue_end,'0') as mo_first_hour, 
      sd.nd_queue_end as nd_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.nd_queue_end,'0') as nd_first_hour, 
      sd.qi_queue_end as qi_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.qi_queue_end,'0') as qi_first_hour, 
      sd.pd_queue_end as pd_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.pd_queue_end,'0') as pd_first_hour, 
      sd.bd_queue_end as bd_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.bd_queue_end,'0') as bd_first_hour, 
      sd.se_queue_end as se_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.se_queue_end,'0') as se_first_hour, 
      sd.di_queue_end as di_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.di_queue_end,'0') as di_first_hour, 
      sd.qc_queue_end as qc_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.qc_queue_end,'0') as qc_first_hour, 
      sd.oa_queue_end as oa_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.oa_queue_end,'0') as oa_first_hour, 
      sd.ot_queue_end as ot_start,
      decode ( to_char(clean_date,'HH24:mi'), '00:00', sd.ot_queue_end,'0') as ot_first_hour 
      --
      from rpt.Hourly_Stats_VW sd
      where sd.clean_date 
      between trunc(p_run_date - 1) + 23/24 and trunc(p_run_date) + 23/24 ) sdv
    where clean_date >= trunc(p_run_date)
      and  clean_date <  trunc(p_run_date + 1)
      and   to_char(clean_date - 1/48, 'mm/dd/yyyy hh24:mi' ) = sdv.one_hour_ago  
    order by clean_date;

    LOOP
      FETCH curs INTO ret;
        EXIT WHEN curs%NOTFOUND;

      --**************************************************************************
      --Print DETAIL Line
      --**************************************************************************

      line := 'C;Y3;X'|| to_char(y);
      line := line || ';K"'||ret.now_hour||'"';
      print_line( line );

      line := 'C;Y4;X'|| to_char(y);
      line := line || ';K'||ret.scrub_start||'';
      print_line( line );

      line := 'C;Y5;X'|| to_char(y);
      line := line || ';K'||ret.scrub_incoming||'';
      print_line( line );

      line := 'C;Y6;X'|| to_char(y);
      line := line || ';K'||ret.scrub_worked||'';
      print_line( line );

      line := 'C;Y7;X'|| to_char(y);
      line := line || ';K'||ret.scrub_end||'';
      print_line( line );

      line := 'C;Y8;X'|| to_char(y);
      line := line || ';K'||ret.merc_start||'';
      print_line( line );

      line := 'C;Y9;X'|| to_char(y);
      line := line || ';K'||ret.merc_queue_incoming||'';
      print_line( line );

      line := 'C;Y10;X'|| to_char(y);
      line := line || ';K'||ret.mercury_worked||'';
      print_line( line );

      line := 'C;Y11;X'|| to_char(y);
      line := line || ';K'||ret.merc_queue_end||'';
      print_line( line );
      
	  line := 'C;Y12;X'|| to_char(y);
      line := line || ';K'||ret.merc_queue_tagged||'';
      print_line( line );
      
      line := 'C;Y13;X'|| to_char(y);
      line := line || ';K'||ret.mercury_untagged||'';
      print_line( line );
--
      line := 'C;Y14;X'|| to_char(y);
      line := line || ';K'||ret.ftd_start||'';
      print_line( line );
	  
      line := 'C;Y15;X'|| to_char(y);
      line := line || ';K'||ret.ftd_queue_incoming||'';
      print_line( line );

      line := 'C;Y16;X'|| to_char(y);
      line := line || ';K'||ret.ftd_worked||'';
      print_line( line );

      line := 'C;Y17;X'|| to_char(y);
      line := line || ';K'||ret.ftd_queue_end||'';
      print_line( line );

      line := 'C;Y18;X'|| to_char(y);
      line := line || ';K'||ret.ftd_queue_tagged||'';
      print_line( line );

      line := 'C;Y19;X'|| to_char(y);
      line := line || ';K'||ret.ftd_untagged||'';
      print_line( line );
--
      line := 'C;Y20;X'|| to_char(y);
      line := line || ';K'||ret.ftdm_start||'';
      print_line( line );
	  
      line := 'C;Y21;X'|| to_char(y);
      line := line || ';K'||ret.ftdm_queue_incoming||'';
      print_line( line );

      line := 'C;Y22;X'|| to_char(y);
      line := line || ';K'||ret.ftdm_worked||'';
      print_line( line );

      line := 'C;Y23;X'|| to_char(y);
      line := line || ';K'||ret.ftdm_queue_end||'';
      print_line( line );

      line := 'C;Y24;X'|| to_char(y);
      line := line || ';K'||ret.ftdm_queue_tagged||'';
      print_line( line );

      line := 'C;Y25;X'|| to_char(y);
      line := line || ';K'||ret.ftdm_untagged||'';
      print_line( line );
--
      line := 'C;Y26;X'|| to_char(y);
      line := line || ';K'||ret.ask_start||'';
      print_line( line );
	  
      line := 'C;Y27;X'|| to_char(y);
      line := line || ';K'||ret.ask_queue_incoming||'';
      print_line( line );

      line := 'C;Y28;X'|| to_char(y);
      line := line || ';K'||ret.ask_worked||'';
      print_line( line );

      line := 'C;Y29;X'|| to_char(y);
      line := line || ';K'||ret.ask_queue_end||'';
      print_line( line );

      line := 'C;Y30;X'|| to_char(y);
      line := line || ';K'||ret.ask_queue_tagged||'';
      print_line( line );

      line := 'C;Y31;X'|| to_char(y);
      line := line || ';K'||ret.ask_untagged||'';
      print_line( line );
--
      line := 'C;Y32;X'|| to_char(y);
      line := line || ';K'||ret.reject_start||'';
      print_line( line );
	  
      line := 'C;Y33;X'|| to_char(y);
      line := line || ';K'||ret.reject_queue_incoming||'';
      print_line( line );

      line := 'C;Y34;X'|| to_char(y);
      line := line || ';K'||ret.reject_worked||'';
      print_line( line );

      line := 'C;Y35;X'|| to_char(y);
      line := line || ';K'||ret.reject_queue_auto||'';
      print_line( line );
      
      line := 'C;Y36;X'|| to_char(y);
      line := line || ';K'||ret.reject_queue_end||'';
      print_line( line );
      
      line := 'C;Y37;X'|| to_char(y);
      line := line || ';K'||ret.reject_queue_tagged||'';
      print_line( line );

      line := 'C;Y38;X'|| to_char(y);
      line := line || ';K'||ret.reject_untagged||'';
      print_line( line );
--
      line := 'C;Y39;X'|| to_char(y);
      line := line || ';K'||ret.ans_start||'';
      print_line( line );
	  
      line := 'C;Y40;X'|| to_char(y);
      line := line || ';K'||ret.ans_queue_incoming||'';
      print_line( line );

      line := 'C;Y41;X'|| to_char(y);
      line := line || ';K'||ret.ans_worked||'';
      print_line( line );

      line := 'C;Y42;X'|| to_char(y);
      line := line || ';K'||ret.ans_queue_end||'';
      print_line( line );

      line := 'C;Y43;X'|| to_char(y);
      line := line || ';K'||ret.ans_queue_tagged||'';
      print_line( line );

      line := 'C;Y44;X'|| to_char(y);
      line := line || ';K'||ret.ans_untagged||'';
      print_line( line );
--
      line := 'C;Y45;X'|| to_char(y);
      line := line || ';K'||ret.den_start||'';
      print_line( line );
	  
      line := 'C;Y46;X'|| to_char(y);
      line := line || ';K'||ret.den_queue_incoming||'';
      print_line( line );

      line := 'C;Y47;X'|| to_char(y);
      line := line || ';K'||ret.den_worked||'';
      print_line( line );

      line := 'C;Y48;X'|| to_char(y);
      line := line || ';K'||ret.den_queue_end||'';
      print_line( line );

      line := 'C;Y49;X'|| to_char(y);
      line := line || ';K'||ret.den_queue_tagged||'';
      print_line( line );

      line := 'C;Y50;X'|| to_char(y);
      line := line || ';K'||ret.den_untagged||'';
      print_line( line );
--
      line := 'C;Y51;X'|| to_char(y);
      line := line || ';K'||ret.con_start||'';
      print_line( line );
	  
      line := 'C;Y52;X'|| to_char(y);
      line := line || ';K'||ret.con_queue_incoming||'';
      print_line( line );

      line := 'C;Y53;X'|| to_char(y);
      line := line || ';K'||ret.con_worked||'';
      print_line( line );

      line := 'C;Y54;X'|| to_char(y);
      line := line || ';K'||ret.con_queue_end||'';
      print_line( line );

      line := 'C;Y55;X'|| to_char(y);
      line := line || ';K'||ret.con_queue_tagged||'';
      print_line( line );

      line := 'C;Y56;X'|| to_char(y);
      line := line || ';K'||ret.con_untagged||'';
      print_line( line );
--
      line := 'C;Y57;X'|| to_char(y);
      line := line || ';K'||ret.can_start||'';
      print_line( line );
	  
      line := 'C;Y58;X'|| to_char(y);
      line := line || ';K'||ret.can_queue_incoming||'';
      print_line( line );

      line := 'C;Y59;X'|| to_char(y);
      line := line || ';K'||ret.can_worked||'';
      print_line( line );

      line := 'C;Y60;X'|| to_char(y);
      line := line || ';K'||ret.can_queue_end||'';
      print_line( line );

      line := 'C;Y61;X'|| to_char(y);
      line := line || ';K'||ret.can_queue_tagged||'';
      print_line( line );

      line := 'C;Y62;X'|| to_char(y);
      line := line || ';K'||ret.can_untagged||'';
      print_line( line );
--
      line := 'C;Y63;X'|| to_char(y);
      line := line || ';K'||ret.gen_start||'';
      print_line( line );
	  
      line := 'C;Y64;X'|| to_char(y);
      line := line || ';K'||ret.gen_queue_incoming||'';
      print_line( line );

      line := 'C;Y65;X'|| to_char(y);
      line := line || ';K'||ret.gen_worked||'';
      print_line( line );

      line := 'C;Y66;X'|| to_char(y);
      line := line || ';K'||ret.gen_queue_end||'';
      print_line( line );

      line := 'C;Y67;X'|| to_char(y);
      line := line || ';K'||ret.gen_queue_tagged||'';
      print_line( line );

      line := 'C;Y68;X'|| to_char(y);
      line := line || ';K'||ret.gen_untagged||'';
      print_line( line );
--
      line := 'C;Y69;X'|| to_char(y);
      line := line || ';K'||ret.zip_start||'';
      print_line( line );
	  
      line := 'C;Y70;X'|| to_char(y);
      line := line || ';K'||ret.zip_queue_incoming||'';
      print_line( line );

      line := 'C;Y71;X'|| to_char(y);
      line := line || ';K'||ret.zip_worked||'';
      print_line( line );

      line := 'C;Y72;X'|| to_char(y);
      line := line || ';K'||ret.zip_queue_end||'';
      print_line( line );

      line := 'C;Y73;X'|| to_char(y);
      line := line || ';K'||ret.zip_queue_tagged||'';
      print_line( line );

      line := 'C;Y74;X'|| to_char(y);
      line := line || ';K'||ret.zip_untagged||'';
      print_line( line );
--
      line := 'C;Y75;X'|| to_char(y);
      line := line || ';K'||ret.crd_start||'';
      print_line( line );
	  
      line := 'C;Y76;X'|| to_char(y);
      line := line || ';K'||ret.crd_queue_incoming||'';
      print_line( line );

      line := 'C;Y77;X'|| to_char(y);
      line := line || ';K'||ret.crd_worked||'';
      print_line( line );

      line := 'C;Y78;X'|| to_char(y);
      line := line || ';K'||ret.crd_queue_end||'';
      print_line( line );

      line := 'C;Y79;X'|| to_char(y);
      line := line || ';K'||ret.crd_queue_tagged||'';
      print_line( line );

      line := 'C;Y80;X'|| to_char(y);
      line := line || ';K'||ret.crd_untagged||'';
      print_line( line );
--
      line := 'C;Y81;X'|| to_char(y);
      line := line || ';K'||ret.lp_start||'';
      print_line( line );
	  
      line := 'C;Y82;X'|| to_char(y);
      line := line || ';K'||ret.lp_queue_incoming||'';
      print_line( line );

      line := 'C;Y83;X'|| to_char(y);
      line := line || ';K'||ret.lp_worked||'';
      print_line( line );

      line := 'C;Y84;X'|| to_char(y);
      line := line || ';K'||ret.lp_queue_end||'';
      print_line( line );

      line := 'C;Y85;X'|| to_char(y);
      line := line || ';K'||ret.lp_queue_tagged||'';
      print_line( line );

      line := 'C;Y86;X'|| to_char(y);
      line := line || ';K'||ret.lp_untagged||'';
      print_line( line );
--
      line := 'C;Y87;X'|| to_char(y);
      line := line || ';K'||ret.adj_start||'';
      print_line( line );
	  
      line := 'C;Y88;X'|| to_char(y);
      line := line || ';K'||ret.adj_queue_incoming||'';
      print_line( line );

      line := 'C;Y89;X'|| to_char(y);
      line := line || ';K'||ret.adj_worked||'';
      print_line( line );

      line := 'C;Y90;X'|| to_char(y);
      line := line || ';K'||ret.adj_queue_end||'';
      print_line( line );

      line := 'C;Y91;X'|| to_char(y);
      line := line || ';K'||ret.adj_queue_tagged||'';
      print_line( line );

      line := 'C;Y92;X'|| to_char(y);
      line := line || ';K'||ret.adj_untagged||'';
      print_line( line );
--
      line := 'C;Y93;X'|| to_char(y);
      line := line || ';K'||ret.email_start||'';
      print_line( line );
	  
      line := 'C;Y94;X'|| to_char(y);
      line := line || ';K'||ret.email_queue_incoming||'';
      print_line( line );

      line := 'C;Y95;X'|| to_char(y);
      line := line || ';K'||ret.email_worked||'';
      print_line( line );

      line := 'C;Y96;X'|| to_char(y);
      line := line || ';K'||ret.email_auto_handle||'';
      print_line( line );
      
      line := 'C;Y97;X'|| to_char(y);
      line := line || ';K'||ret.email_queue_end||'';
      print_line( line );
      
      line := 'C;Y98;X'|| to_char(y);
      line := line || ';K'||ret.email_queue_tagged||'';
      print_line( line );

      line := 'C;Y99;X'|| to_char(y);
      line := line || ';K'||ret.email_untagged||'';
      print_line( line );
--
      line := 'C;Y100;X'|| to_char(y);
      line := line || ';K'||ret.cx_start||'';
      print_line( line );
	  
      line := 'C;Y101;X'|| to_char(y);
      line := line || ';K'||ret.cx_queue_incoming||'';
      print_line( line );

      line := 'C;Y102;X'|| to_char(y);
      line := line || ';K'||ret.cx_worked||'';
      print_line( line );

      line := 'C;Y103;X'|| to_char(y);
      line := line || ';K'||ret.cx_queue_end||'';
      print_line( line );

      line := 'C;Y104;X'|| to_char(y);
      line := line || ';K'||ret.cx_queue_tagged||'';
      print_line( line );

      line := 'C;Y105;X'|| to_char(y);
      line := line || ';K'||ret.cx_untagged||'';
      print_line( line );
--
      line := 'C;Y106;X'|| to_char(y);
      line := line || ';K'||ret.mo_start||'';
      print_line( line );
	  
      line := 'C;Y107;X'|| to_char(y);
      line := line || ';K'||ret.mo_queue_incoming||'';
      print_line( line );

      line := 'C;Y108;X'|| to_char(y);
      line := line || ';K'||ret.mo_worked||'';
      print_line( line );

      line := 'C;Y109;X'|| to_char(y);
      line := line || ';K'||ret.mo_queue_end||'';
      print_line( line );

      line := 'C;Y110;X'|| to_char(y);
      line := line || ';K'||ret.mo_queue_tagged||'';
      print_line( line );

      line := 'C;Y111;X'|| to_char(y);
      line := line || ';K'||ret.mo_untagged||'';
      print_line( line );
--
      line := 'C;Y112;X'|| to_char(y);
      line := line || ';K'||ret.nd_start||'';
      print_line( line );
	  
      line := 'C;Y113;X'|| to_char(y);
      line := line || ';K'||ret.nd_queue_incoming||'';
      print_line( line );

      line := 'C;Y114;X'|| to_char(y);
      line := line || ';K'||ret.nd_worked||'';
      print_line( line );

      line := 'C;Y115;X'|| to_char(y);
      line := line || ';K'||ret.nd_queue_end||'';
      print_line( line );

      line := 'C;Y116;X'|| to_char(y);
      line := line || ';K'||ret.nd_queue_tagged||'';
      print_line( line );

      line := 'C;Y117;X'|| to_char(y);
      line := line || ';K'||ret.nd_untagged||'';
      print_line( line );
--
      line := 'C;Y118;X'|| to_char(y);
      line := line || ';K'||ret.qi_start||'';
      print_line( line );
	  
      line := 'C;Y119;X'|| to_char(y);
      line := line || ';K'||ret.qi_queue_incoming||'';
      print_line( line );

      line := 'C;Y120;X'|| to_char(y);
      line := line || ';K'||ret.qi_worked||'';
      print_line( line );

      line := 'C;Y121;X'|| to_char(y);
      line := line || ';K'||ret.qi_queue_end||'';
      print_line( line );

      line := 'C;Y122;X'|| to_char(y);
      line := line || ';K'||ret.qi_queue_tagged||'';
      print_line( line );

      line := 'C;Y123;X'|| to_char(y);
      line := line || ';K'||ret.qi_untagged||'';
      print_line( line );
--
      line := 'C;Y124;X'|| to_char(y);
      line := line || ';K'||ret.pd_start||'';
      print_line( line );
	  
      line := 'C;Y125;X'|| to_char(y);
      line := line || ';K'||ret.pd_queue_incoming||'';
      print_line( line );

      line := 'C;Y126;X'|| to_char(y);
      line := line || ';K'||ret.pd_worked||'';
      print_line( line );

      line := 'C;Y127;X'|| to_char(y);
      line := line || ';K'||ret.pd_queue_end||'';
      print_line( line );

      line := 'C;Y128;X'|| to_char(y);
      line := line || ';K'||ret.pd_queue_tagged||'';
      print_line( line );

      line := 'C;Y129;X'|| to_char(y);
      line := line || ';K'||ret.pd_untagged||'';
      print_line( line );
--
      line := 'C;Y130;X'|| to_char(y);
      line := line || ';K'||ret.bd_start||'';
      print_line( line );
	  
      line := 'C;Y131;X'|| to_char(y);
      line := line || ';K'||ret.bd_queue_incoming||'';
      print_line( line );

      line := 'C;Y132;X'|| to_char(y);
      line := line || ';K'||ret.bd_worked||'';
      print_line( line );

      line := 'C;Y133;X'|| to_char(y);
      line := line || ';K'||ret.bd_queue_end||'';
      print_line( line );

      line := 'C;Y134;X'|| to_char(y);
      line := line || ';K'||ret.bd_queue_tagged||'';
      print_line( line );

      line := 'C;Y135;X'|| to_char(y);
      line := line || ';K'||ret.bd_untagged||'';
      print_line( line );
--
      line := 'C;Y136;X'|| to_char(y);
      line := line || ';K'||ret.se_start||'';
      print_line( line );
	  
      line := 'C;Y137;X'|| to_char(y);
      line := line || ';K'||ret.se_queue_incoming||'';
      print_line( line );

      line := 'C;Y138;X'|| to_char(y);
      line := line || ';K'||ret.se_worked||'';
      print_line( line );

      line := 'C;Y139;X'|| to_char(y);
      line := line || ';K'||ret.se_queue_end||'';
      print_line( line );

      line := 'C;Y140;X'|| to_char(y);
      line := line || ';K'||ret.se_queue_tagged||'';
      print_line( line );

      line := 'C;Y141;X'|| to_char(y);
      line := line || ';K'||ret.se_untagged||'';
      print_line( line );
--
      line := 'C;Y142;X'|| to_char(y);
      line := line || ';K'||ret.di_start||'';
      print_line( line );
	  
      line := 'C;Y143;X'|| to_char(y);
      line := line || ';K'||ret.di_queue_incoming||'';
      print_line( line );

      line := 'C;Y144;X'|| to_char(y);
      line := line || ';K'||ret.di_worked||'';
      print_line( line );

      line := 'C;Y145;X'|| to_char(y);
      line := line || ';K'||ret.di_queue_end||'';
      print_line( line );

      line := 'C;Y146;X'|| to_char(y);
      line := line || ';K'||ret.di_queue_tagged||'';
      print_line( line );

      line := 'C;Y147;X'|| to_char(y);
      line := line || ';K'||ret.di_untagged||'';
      print_line( line );
--
      line := 'C;Y148;X'|| to_char(y);
      line := line || ';K'||ret.qc_start||'';
      print_line( line );
	  
      line := 'C;Y149;X'|| to_char(y);
      line := line || ';K'||ret.qc_queue_incoming||'';
      print_line( line );

      line := 'C;Y150;X'|| to_char(y);
      line := line || ';K'||ret.qc_worked||'';
      print_line( line );

      line := 'C;Y151;X'|| to_char(y);
      line := line || ';K'||ret.qc_queue_end||'';
      print_line( line );

      line := 'C;Y152;X'|| to_char(y);
      line := line || ';K'||ret.qc_queue_tagged||'';
      print_line( line );

      line := 'C;Y153;X'|| to_char(y);
      line := line || ';K'||ret.qc_untagged||'';
      print_line( line );
--
      line := 'C;Y154;X'|| to_char(y);
      line := line || ';K'||ret.oa_start||'';
      print_line( line );
	  
      line := 'C;Y155;X'|| to_char(y);
      line := line || ';K'||ret.oa_queue_incoming||'';
      print_line( line );

      line := 'C;Y156;X'|| to_char(y);
      line := line || ';K'||ret.oa_worked||'';
      print_line( line );

      line := 'C;Y157;X'|| to_char(y);
      line := line || ';K'||ret.oa_queue_end||'';
      print_line( line );

      line := 'C;Y158;X'|| to_char(y);
      line := line || ';K'||ret.oa_queue_tagged||'';
      print_line( line );

      line := 'C;Y159;X'|| to_char(y);
      line := line || ';K'||ret.oa_untagged||'';
      print_line( line );
--
      line := 'C;Y160;X'|| to_char(y);
      line := line || ';K'||ret.ot_start||'';
      print_line( line );
	  
      line := 'C;Y161;X'|| to_char(y);
      line := line || ';K'||ret.ot_queue_incoming||'';
      print_line( line );

      line := 'C;Y162;X'|| to_char(y);
      line := line || ';K'||ret.ot_worked||'';
      print_line( line );

      line := 'C;Y163;X'|| to_char(y);
      line := line || ';K'||ret.ot_queue_end||'';
      print_line( line );

      line := 'C;Y164;X'|| to_char(y);
      line := line || ';K'||ret.ot_queue_tagged||'';
      print_line( line );

      line := 'C;Y165;X'|| to_char(y);
      line := line || ';K'||ret.ot_untagged||'';
      print_line( line );
--
      line := 'C;Y166;X'|| to_char(y);
      line := line || ';K'||ret.email_merc_tagged||'';
      print_line( line );

      line := 'C;Y167;X'|| to_char(y);
      line := line || ';K'||ret.email_merc_untagged||'';
      print_line( line );
      
      line := 'C;Y168;X'|| to_char(y);
      line := line || ';K'||ret.view_queue_cnt||'';
      print_line( line );
      
      y := y + 1;

      if g_CLOB = 'N' then
        utl_file.fflush( g_file );
      end if;

    END LOOP;

  --**************************************************************************
  --Print "No Rows Returned" line if no data exists
  --**************************************************************************
  if y = 2 then
     print_comment( 'No Rows Returned' );
     line := 'C;Y4;X2';
     line := line || ';K"No Data Found"';
     print_line( line );

     if g_CLOB = 'N' then
	utl_file.fflush( g_file );
     end if;

  end if;

  --**************************************************************************
  --Cleanup Stuff - Close Cursor and Return
  --**************************************************************************
  CLOSE curs;

  RETURN (y);

end print_rows;

--***************************************************************************************
-- The Main Section of the Report
--***************************************************************************************
procedure run_report(p_run_date in varchar2,
		p_submission_id in number,
		p_CLOB in varchar2
		)
is
  l_row_cnt number;
  l_col_cnt number;
  l_status  number;
  v_run_date date;
  v_OUT_STATUS_det	varchar2(30);
  v_OUT_MESSAGE_det	varchar2(1000);
  v_OUT_STATUS_sub 	varchar2(30);
  v_OUT_MESSAGE_sub	varchar2(1000);

begin

   if p_run_date is not null then
      v_run_date := to_date(p_run_date,'mm/dd/yyyy');
   else
      v_run_date := add_months(trunc(sysdate,'MONTH'), -1);
   end if;

--
-- Prepare for either CLOB output or REPORT_DIR output
--

   if p_CLOB = 'Y' then
       g_SUBMISSION_ID := P_SUBMISSION_ID;
       -- set the report_submission_log status to STARTED
       rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
                        (
			P_SUBMISSION_ID,
			'SYS',
			'S' ,
			null ,
			v_OUT_STATUS_DET,
			v_OUT_MESSAGE_DET
                        );
       commit;
       -- insert empty clob into report_detail_char
       rpt.report_clob_pkg.INSERT_REPORT_DETAIL_CHAR
       			(
			P_SUBMISSION_ID,
			empty_clob(),
			v_out_status_det,
			v_out_message_det
		        );
       -- reset the global clob variable
       rpt.report_clob_pkg.clob_variable_reset;

   else
       g_CLOB := P_CLOB;
       g_file := utl_file.fopen( 'REPORT_DIR','CSR03_Holiday_Monitoring.slk', 'W' );
   end if;

    print_title( v_run_date);

    print_heading(3);

    l_row_cnt := print_rows( v_run_date );

    print_line( 'E' );

   --
   -- Update Database with CLOB and set report submission status (if CLOB'ing)
   -- Close report file (if not CLOB'ing
   --

      if p_CLOB = 'Y' then
         begin
   	-- update the report_detail_char table with the package global variable g_clob
   	rpt.report_clob_pkg.update_report_detail_char
   			(
   			 p_submission_id,
   			 v_out_status_det,
   			 v_out_message_det
   			);
   	if v_out_status_det = 'N' then
   	-- if db update failure, update report_submission_log with ERROR status
   	rpt.report_pkg.update_report_submission_log
   			(
   			 p_submission_id,
   			 'REPORT',
   			 'E',
   			 v_out_message_det,
   			 v_out_status_sub,
   			 v_out_message_sub
   			);
   	else
   	-- if db update successful, update report_submission_log with COMPLETE status
   	rpt.report_pkg.UPDATE_REPORT_SUBMISSION_LOG
   			(
   			 P_SUBMISSION_ID,
   			 'SYS',
   			 'C' ,
   			 null ,
   			 v_out_status_det ,
   			 v_out_message_det
   			);
   	end if;
   	--exception
   	--	when others then srw.message(32321,'Error in p');
        end;
        commit;
      else
        utl_file.fflush( g_file );
        utl_file.fclose( g_file );
   end if;

  end run_report;
end;
.
/
