CREATE OR REPLACE
PROCEDURE rpt.clean_counts
(
 IN_DATE      IN DATE,
 IN_HOUR      IN PLS_INTEGER,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table clean_hourly_stats.

Input:
        in_date         DATE
        in_hour         PLS_INTEGER

Output:
        status          VARCHAR2
        message         VARCHAR2

-----------------------------------------------------------------------------*/

CURSOR shutdown_details(v_run_date date) is
    SELECT fm.florist_id,
        fm.super_florist_flag,
        (select fb1.blocked_by_user_id
            from ftd_apps.florist_blocks fb1
            where fb1.florist_id (+) = fm.florist_id
            AND fb1.block_start_date <= trunc(v_run_date)
            AND (fb1.block_end_date is null OR fb1.block_end_date >= trunc(v_run_date))
            AND rownum = 1) blocked_by,
        (case ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, v_run_date, null)
            when 'Y' then (select suspend_type
                from ftd_apps.florist_suspends fs
                where fs.florist_id = fm.florist_id)
            else null
        end) suspend_type,
        to_char(v_run_date, 'DY') day_of_week,
        ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(v_run_date)) is_florist_open_for_delivery,
        ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) is_florist_open_in_eros
    FROM ftd_apps.florist_master fm
    WHERE fm.record_type = 'R'
    AND fm.status <> 'Inactive'
    AND fm.mercury_flag = 'M'
    AND (
        ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, v_run_date, null) = 'Y'
        OR ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, trunc(v_run_date), null) = 'Y'
        OR ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(v_run_date)) <> 'Y'
        OR ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) <> 'Y'
    );

v_goto_florist_count    PLS_INTEGER;
v_goto_florist_suspend  PLS_INTEGER;
v_mercury_shop_count    PLS_INTEGER;
v_mercury_shop_shutdown PLS_INTEGER;
v_covered_zip_count     PLS_INTEGER;
v_gnadd_zip_count       PLS_INTEGER;
v_ftd_queue_end         PLS_INTEGER;
v_ftd_queue_incoming    PLS_INTEGER;
v_ftd_queue_tagged      PLS_INTEGER;
v_ftdm_queue_end        PLS_INTEGER;
v_ftdm_queue_incoming   PLS_INTEGER;
v_ftdm_queue_tagged     PLS_INTEGER;
v_ask_queue_end         PLS_INTEGER;
v_ask_queue_incoming    PLS_INTEGER;
v_ask_queue_tagged      PLS_INTEGER;
v_reject_queue_end      PLS_INTEGER;
v_reject_queue_incoming PLS_INTEGER;
v_reject_queue_tagged   PLS_INTEGER;
v_reject_queue_auto     PLS_INTEGER;

v_ans_queue_end         PLS_INTEGER;
v_ans_queue_incoming    PLS_INTEGER;
v_ans_queue_tagged      PLS_INTEGER;
v_den_queue_end         PLS_INTEGER;
v_den_queue_incoming    PLS_INTEGER;
v_den_queue_tagged      PLS_INTEGER;
v_con_queue_end         PLS_INTEGER;
v_con_queue_incoming    PLS_INTEGER;
v_con_queue_tagged      PLS_INTEGER;
v_can_queue_end         PLS_INTEGER;
v_can_queue_incoming    PLS_INTEGER;
v_can_queue_tagged      PLS_INTEGER;
v_gen_queue_end         PLS_INTEGER;
v_gen_queue_incoming    PLS_INTEGER;
v_gen_queue_tagged      PLS_INTEGER;
v_zip_queue_end         PLS_INTEGER;
v_zip_queue_incoming    PLS_INTEGER;
v_zip_queue_tagged      PLS_INTEGER;
v_crd_queue_end         PLS_INTEGER;
v_crd_queue_incoming    PLS_INTEGER;
v_crd_queue_tagged      PLS_INTEGER;
v_lp_queue_end         PLS_INTEGER;
v_lp_queue_incoming    PLS_INTEGER;
v_lp_queue_tagged      PLS_INTEGER;
v_adj_queue_end         PLS_INTEGER;
v_adj_queue_incoming    PLS_INTEGER;
v_adj_queue_tagged      PLS_INTEGER;

v_email_queue_end       PLS_INTEGER;
v_email_queue_incoming  PLS_INTEGER;
v_email_queue_tagged    PLS_INTEGER;
v_email_worked          PLS_INTEGER;
v_email_auto_handle     PLS_INTEGER;

v_cx_queue_end         PLS_INTEGER;
v_cx_queue_incoming    PLS_INTEGER;
v_cx_queue_tagged      PLS_INTEGER;
v_mo_queue_end         PLS_INTEGER;
v_mo_queue_incoming    PLS_INTEGER;
v_mo_queue_tagged      PLS_INTEGER;
v_nd_queue_end         PLS_INTEGER;
v_nd_queue_incoming    PLS_INTEGER;
v_nd_queue_tagged      PLS_INTEGER;
v_qi_queue_end         PLS_INTEGER;
v_qi_queue_incoming    PLS_INTEGER;
v_qi_queue_tagged      PLS_INTEGER;
v_pd_queue_end         PLS_INTEGER;
v_pd_queue_incoming    PLS_INTEGER;
v_pd_queue_tagged      PLS_INTEGER;
v_bd_queue_end         PLS_INTEGER;
v_bd_queue_incoming    PLS_INTEGER;
v_bd_queue_tagged      PLS_INTEGER;
v_se_queue_end         PLS_INTEGER;
v_se_queue_incoming    PLS_INTEGER;
v_se_queue_tagged      PLS_INTEGER;
v_di_queue_end         PLS_INTEGER;
v_di_queue_incoming    PLS_INTEGER;
v_di_queue_tagged      PLS_INTEGER;
v_qc_queue_end         PLS_INTEGER;
v_qc_queue_incoming    PLS_INTEGER;
v_qc_queue_tagged      PLS_INTEGER;
v_OA_queue_end         PLS_INTEGER;
v_OA_queue_incoming    PLS_INTEGER;
v_OA_queue_tagged      PLS_INTEGER;
v_ot_queue_end         PLS_INTEGER;
v_ot_queue_incoming    PLS_INTEGER;
v_ot_queue_tagged      PLS_INTEGER;

v_merc_queue_end         PLS_INTEGER;
v_merc_queue_incoming    PLS_INTEGER;
v_merc_queue_tagged      PLS_INTEGER;
v_no_coverage_zip_cnt	 PLS_INTEGER := 0;
V_current_minutes        number;
c_half_hour    CONSTANT number := 30/1440;

v_suspend_cnt                   PLS_INTEGER := 0;
v_eros_block_cnt                PLS_INTEGER := 0;
v_fit_block_cnt                 PLS_INTEGER := 0;
v_user_block_cnt                PLS_INTEGER := 0;
v_sunday_not_codified_cnt       PLS_INTEGER := 0;
v_goto_suspend_cnt              PLS_INTEGER := 0;
v_goto_eros_block_cnt           PLS_INTEGER := 0;
v_goto_fit_block_cnt            PLS_INTEGER := 0;
v_goto_user_block_cnt           PLS_INTEGER := 0;
v_goto_sunday_not_codified_cnt  PLS_INTEGER := 0;
v_view_queue_cnt                PLS_INTEGER;

BEGIN

CASE WHEN to_number(to_char(sysdate,'MI')) BETWEEN 0 AND 29
     THEN  v_current_minutes := 0;
     ELSE  v_current_minutes := 30/1440;
END CASE;

  BEGIN
      SELECT COUNT(florist_id)
        INTO v_goto_florist_count
        FROM ftd_apps.florist_master
       WHERE super_florist_flag = 'Y'
    	 AND record_type = 'R'
	 AND status <> 'Inactive';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_goto_florist_count := 0;
  END;

  BEGIN
      SELECT COUNT(fm.florist_id)
        INTO v_goto_florist_suspend
        FROM ftd_apps.florist_master fm
        WHERE fm.record_type = 'R'
        AND fm.status <> 'Inactive'
        AND fm.super_florist_flag = 'Y'
        AND (
            ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, trunc(in_date) + (in_hour / 24) + v_current_minutes, null) = 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, trunc(in_date), null) = 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(in_date)) <> 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) <> 'Y'
        );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_goto_florist_suspend := 0;
  END;

  BEGIN
      SELECT COUNT(florist_id)
        INTO v_mercury_shop_count
        FROM ftd_apps.florist_master
       WHERE mercury_flag = 'M'
                AND status <> 'Inactive'
         AND record_type = 'R';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_mercury_shop_count := 0;
  END;

  BEGIN
      SELECT COUNT(fm.florist_id)
        INTO v_mercury_shop_shutdown
        FROM ftd_apps.florist_master fm
        WHERE fm.record_type = 'R'
        AND fm.status <> 'Inactive'
        AND fm.mercury_flag = 'M'
        AND (
            ftd_apps.florist_query_pkg.is_florist_suspended(fm.florist_id, trunc(in_date) + (in_hour / 24) + v_current_minutes, null) = 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_blocked(fm.florist_id, trunc(in_date), null) = 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_open_for_delivery(fm.florist_id, trunc(in_date)) <> 'Y'
            OR ftd_apps.florist_query_pkg.is_florist_open_in_eros(fm.florist_id) <> 'Y'
        );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_mercury_shop_shutdown := 0;
  END;

  BEGIN
      SELECT count(p.zip_code_id), sum(decode(p.status_code,'N', 1, 0))
          INTO v_covered_zip_count, v_no_coverage_zip_cnt
      FROM pas.pas_florist_zipcode_dt p
      WHERE p.delivery_date = trunc(sysdate)
      AND exists (select distinct 'Y' 
          FROM ftd_apps.florist_zips fz
          JOIN ftd_apps.florist_master fm
          ON fz.florist_id = fm.florist_id
          AND fm.status <> 'Inactive'
          WHERE fz.zip_code = p.zip_code_id);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_covered_zip_count := 0;
  END;

  BEGIN
      SELECT COUNT(csz_zip_code)
        INTO v_gnadd_zip_count
        FROM ftd_apps.csz_avail
       WHERE gnadd_flag = 'Y';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_gnadd_zip_count := 0;
  END;

  FOR rec in shutdown_details(trunc(in_date) + (in_hour / 24) + v_current_minutes) loop

      IF rec.suspend_type is not null and (rec.super_florist_flag is null or rec.super_florist_flag = 'N'
        OR (rec.super_florist_flag = 'Y' and rec.suspend_type = 'G')) THEN
          IF rec.super_florist_flag = 'Y' THEN
              v_goto_suspend_cnt := v_goto_suspend_cnt + 1;
          END IF;
          v_suspend_cnt := v_suspend_cnt + 1;
      ELSIF ((rec. is_florist_open_for_delivery <> 'Y') or (rec.is_florist_open_in_eros <> 'Y')) THEN
          IF rec.super_florist_flag = 'Y' THEN
              v_goto_sunday_not_codified_cnt := v_goto_sunday_not_codified_cnt + 1;
          END IF;
          v_sunday_not_codified_cnt := v_sunday_not_codified_cnt + 1;
      ELSIF rec.blocked_by is not null THEN
          IF rec.blocked_by = 'EROS' THEN
              IF rec.super_florist_flag = 'Y' THEN
                  v_goto_eros_block_cnt := v_goto_eros_block_cnt + 1;
              END IF;
              v_eros_block_cnt := v_eros_block_cnt + 1;
          ELSIF rec.blocked_by = 'FIT' THEN
              IF rec.super_florist_flag = 'Y' THEN
                  v_goto_fit_block_cnt := v_goto_fit_block_cnt + 1;
              END IF;
              v_fit_block_cnt := v_fit_block_cnt + 1;
          ELSE
              IF rec.super_florist_flag = 'Y' THEN
                  v_goto_user_block_cnt := v_goto_user_block_cnt + 1;
              END IF;
              v_user_block_cnt := v_user_block_cnt + 1;
          END IF;
      END IF;
      
  END LOOP;

/* Mercury Queue Counts */

BEGIN
      SELECT COUNT(message_id)
        INTO v_merc_queue_end
          FROM (
	              SELECT message_id
	       	       FROM clean.queue
	              WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
	                   AND mercury_id is not null
	               	     UNION
	               	      SELECT message_id
	             	        FROM clean.queue_delete_history
	               WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
	                   AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
	               AND mercury_number is not null

	                             );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_merc_queue_end := 0;
  END;


BEGIN
      SELECT COUNT(message_id)
        INTO v_merc_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND mercury_id is not null
           UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND mercury_number is not null
              	);

      EXCEPTION WHEN NO_DATA_FOUND THEN v_merc_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_merc_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.mercury_id is not null;

      EXCEPTION WHEN NO_DATA_FOUND THEN v_merc_queue_tagged := 0;
  END;


/* FTD Queue Counts */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ftd_queue_end
            FROM (
                            SELECT message_id
                     	        FROM clean.queue
                             WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                           AND message_type = 'FTD'
              	     UNION
              	      SELECT message_id
              	        FROM clean.queue_delete_history
                             WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                               AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                AND message_type = 'FTD'

                           );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftd_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ftd_queue_incoming
	        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND message_type = 'FTD'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND message_type = 'FTD'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftd_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ftd_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.message_type = 'FTD';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftd_queue_tagged := 0;
  END;

  /*  FTDM Queue Counts */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ftdm_queue_end
         FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND message_type = 'FTDM'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND message_type = 'FTDM'
                              order by 1
                    );




      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftdm_queue_end := 0;
  END;




  BEGIN
      SELECT COUNT(message_id)
        INTO v_ftdm_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND message_type = 'FTDM'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND message_type = 'FTDM'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftdm_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ftdm_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.message_type = 'FTDM';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ftdm_queue_tagged := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ask_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'ASK'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'ASK'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_ask_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ask_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'ASK'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'ASK'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ask_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ask_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'ASK';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ask_queue_tagged := 0;
  END;

/*             REJ QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_reject_queue_end
        FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'REJ'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'REJ'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_reject_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'REJ'
	     UNION
       	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'REJ'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_reject_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'REJ';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_tagged := 0;
  END;

  BEGIN
      SELECT COUNT(mercury_message_number)
        INTO v_reject_queue_auto
        FROM (
              SELECT mercury_message_number
                FROM mercury.mercury
               WHERE msg_type = 'REJ'
	         AND created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
             MINUS
              (
               SELECT mercury_number
                 FROM clean.queue
                WHERE queue_type = 'REJ'
              UNION
               SELECT mercury_number
                 FROM clean.queue_delete_history
                WHERE queue_type = 'REJ'
              )
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_reject_queue_auto := 0;
  END;


/*             ANS QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ANS_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'ANS'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'ANS'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_ANS_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ANS_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'ANS'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'ANS'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ANS_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ans_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'ANS';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ANS_queue_tagged := 0;
  END;


/*             DEN QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_DEN_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'DEN'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'DEN'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_DEN_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_DEN_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'DEN'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'DEN'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_DEN_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_DEN_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'DEN';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_DEN_queue_tagged := 0;
  END;


/*             CON QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CON_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'CON'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'CON'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_CON_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CON_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'CON'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'CON'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CON_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_CON_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'CON';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CON_queue_tagged := 0;
  END;

/*             CAN QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CAN_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'CAN'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'CAN'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_CAN_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CAN_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'CAN'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'CAN'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CAN_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_CAN_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'CAN';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CAN_queue_tagged := 0;
  END;


/*             GEN QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_GEN_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'GEN'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'GEN'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_GEN_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_GEN_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'GEN'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'GEN'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_GEN_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_GEN_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'GEN';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_GEN_queue_tagged := 0;
  END;


/*             ZIP QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ZIP_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'ZIP'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'ZIP'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_ZIP_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ZIP_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'ZIP'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'ZIP'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ZIP_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ZIP_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'ZIP';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ZIP_queue_tagged := 0;
  END;


/*             CRD QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CRD_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'CREDIT'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'CREDIT'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_CRD_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CRD_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'CREDIT'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'CREDIT'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CRD_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_CRD_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'CREDIT';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CRD_queue_tagged := 0;
  END;


/*             LP QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_LP_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'LP'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'LP'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_LP_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_LP_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'LP'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'LP'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_LP_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_LP_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'LP';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_LP_queue_tagged := 0;
  END;

/*             ADJ QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ADJ_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'ADJ'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'ADJ'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_ADJ_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ADJ_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'ADJ'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'ADJ'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ADJ_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ADJ_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'ADJ';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ADJ_queue_tagged := 0;
  END;


/*             EMAIL QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_email_queue_end
         FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND message_type IN (SELECT queue_type
                                FROM clean.queue_type_val
                               WHERE queue_indicator = 'Email')
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND message_type IN (SELECT queue_type
			         FROM clean.queue_type_val
                               WHERE queue_indicator = 'Email')

                    );



      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_queue_end := 0;

  END;




 BEGIN
      SELECT COUNT(point_of_contact_id)
        INTO v_email_auto_handle
        FROM clean.point_of_contact
       WHERE point_of_contact_type = 'Email'
         AND sent_received_indicator = 'O'
         AND LETTER_TITLE IN (
         'cx.autocancel',
         'di.before',
         'mo.changed',
         'oa.found',
         'di.track')
         AND created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
         AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_auto_handle  := 0;
  END;



   BEGIN
      SELECT COUNT(message_id)
        INTO v_email_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND message_type IN (SELECT queue_type
                                FROM clean.queue_type_val
                               WHERE queue_indicator = 'Email')
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND message_type IN (SELECT queue_type
		                                  FROM clean.queue_type_val
                               WHERE queue_indicator = 'Email')
             );
             EXCEPTION WHEN NO_DATA_FOUND THEN v_email_queue_incoming := 0;
	 END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_email_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type IN (SELECT queue_type
                                    FROM clean.queue_type_val
                                   WHERE queue_indicator = 'Email');

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_queue_tagged := 0;
  END;

  BEGIN
      SELECT COUNT(*)
        INTO v_email_worked
        FROM clean.queue_delete_history
       WHERE queue_deleted_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
         AND queue_deleted_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
         AND queue_type IN (SELECT queue_type
                              FROM clean.queue_type_val
                             WHERE queue_indicator = 'Email');

      EXCEPTION WHEN NO_DATA_FOUND THEN v_email_worked := 0;
  END;

/*             CX QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CX_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'CX'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'CX'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_CX_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_CX_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'CX'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'CX'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CX_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_CX_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'CX';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_CX_queue_tagged := 0;
  END;

/*             MO QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_MO_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'MO'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'MO'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_MO_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_MO_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'MO'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'MO'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_MO_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_MO_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'MO';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_MO_queue_tagged := 0;
  END;

/*             ND QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ND_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'ND'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'ND'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_ND_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_ND_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'ND'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'ND'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ND_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_ND_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'ND';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_ND_queue_tagged := 0;
  END;

/*             QI QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_QI_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'QI'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'QI'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_QI_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_QI_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'QI'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'QI'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_QI_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_QI_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'QI';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_QI_queue_tagged := 0;
  END;

/*             PD QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_PD_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'PD'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'PD'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_PD_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_PD_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'PD'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'PD'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_PD_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_PD_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'PD';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_PD_queue_tagged := 0;
  END;

/*             BD QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_BD_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'BD'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'BD'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_BD_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_BD_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'BD'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'BD'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_BD_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_BD_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'BD';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_BD_queue_tagged := 0;
  END;

/*             SE QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_SE_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'SE'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'SE'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_SE_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_SE_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'SE'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'SE'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_SE_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_SE_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'SE';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_SE_queue_tagged := 0;
  END;


/*             DI QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_DI_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'DI'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'DI'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_DI_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_DI_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'DI'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'DI'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_DI_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_DI_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'DI';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_DI_queue_tagged := 0;
  END;

/*             QC QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_QC_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'QC'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'QC'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_QC_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_QC_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'QC'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'QC'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_QC_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_QC_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'QC';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_QC_queue_tagged := 0;
  END;

/*             OA QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_OA_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'OA'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'OA'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_OA_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_OA_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'OA'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'OA'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_OA_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_OA_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'OA';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_OA_queue_tagged := 0;
  END;

/*             OT QUEUES                        */

  BEGIN
      SELECT COUNT(message_id)
        INTO v_OT_queue_end
          FROM (
                     SELECT message_id
              	        FROM clean.queue
                      WHERE created_on <= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                                    AND queue_type = 'OT'
       	     UNION
       	      SELECT message_id
       	        FROM clean.queue_delete_history
                      WHERE queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                        AND queue_deleted_on >=  (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                         AND queue_type = 'OT'

                    );


      EXCEPTION WHEN NO_DATA_FOUND THEN v_OT_queue_end := 0;
  END;

  BEGIN
      SELECT COUNT(message_id)
        INTO v_OT_queue_incoming
        FROM (
              SELECT message_id
       	        FROM clean.queue
               WHERE created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                 AND queue_type = 'OT'
	     UNION
	      SELECT message_id
	        FROM clean.queue_delete_history
               WHERE queue_created_on >= (TRUNC(in_date) + (in_hour / 24) + v_current_minutes - c_half_hour)
                 AND queue_created_on < (TRUNC(in_date) + (in_hour / 24) + v_current_minutes )
                  AND queue_type = 'OT'
             );

      EXCEPTION WHEN NO_DATA_FOUND THEN v_OT_queue_incoming := 0;
  END;

  BEGIN
      SELECT COUNT(qt.message_id)
        INTO v_OT_queue_tagged
        FROM clean.queue_tag qt,
             clean.queue q
       WHERE qt.message_id = q.message_id
         AND q.queue_type = 'OT';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_OT_queue_tagged := 0;
  END;
  
  BEGIN
      SELECT COUNT(view_queue)
        INTO v_view_queue_cnt
        FROM MERCURY.mercury
       WHERE view_queue = 'Y';

      EXCEPTION WHEN NO_DATA_FOUND THEN v_view_queue_cnt := 0;
  END;
  
  dbms_output.put_line(TRUNC(in_date) + (in_hour / 24) + v_current_minutes);

  delete from RPT.clean_hourly_stats
  where clean_date = TRUNC(in_date) + (in_hour / 24) + v_current_minutes;

  INSERT INTO rpt.clean_hourly_stats
       (clean_date,
        goto_florist_count,
        goto_florist_suspend,
        mercury_shop_count,
        mercury_shop_shutdown,
        covered_zip_count,
        gnadd_zip_count,
        ftd_queue_end,
        ftd_queue_incoming,
        ftd_queue_tagged,
        ftdm_queue_end,
        ftdm_queue_incoming,
        ftdm_queue_tagged,
        ask_queue_end,
        ask_queue_incoming,
        ask_queue_tagged,
        reject_queue_end,
        reject_queue_incoming,
        reject_queue_tagged,
        reject_queue_auto,
        ans_queue_end,
        ans_queue_incoming,
        ans_queue_tagged,
        den_queue_end,
        den_queue_incoming,
        den_queue_tagged,
        con_queue_end,
        con_queue_incoming,
        con_queue_tagged,
        can_queue_end,
        can_queue_incoming,
        can_queue_tagged,
        gen_queue_end,
        gen_queue_incoming,
        gen_queue_tagged,
        zip_queue_end,
        zip_queue_incoming,
        zip_queue_tagged,
        crd_queue_end,
        crd_queue_incoming,
        crd_queue_tagged,
        lp_queue_end,
        lp_queue_incoming,
        lp_queue_tagged,
        adj_queue_end,
        adj_queue_incoming,
        adj_queue_tagged,
        email_queue_end,
        email_queue_incoming,
        email_queue_tagged,
        email_auto_handle ,
        email_worked,
        cx_queue_end,
        cx_queue_incoming,
        cx_queue_tagged,
        mo_queue_end,
        mo_queue_incoming,
        mo_queue_tagged,
        nd_queue_end,
        nd_queue_incoming,
        nd_queue_tagged,
        qi_queue_end,
        qi_queue_incoming,
        qi_queue_tagged,
        pd_queue_end,
        pd_queue_incoming,
        pd_queue_tagged,
        bd_queue_end,
        bd_queue_incoming,
        bd_queue_tagged,
        se_queue_end,
        se_queue_incoming,
        se_queue_tagged,
        di_queue_end,
        di_queue_incoming,
        di_queue_tagged,
        qc_queue_end,
        qc_queue_incoming,
        qc_queue_tagged,
        oa_queue_end,
        oa_queue_incoming,
        oa_queue_tagged,
        ot_queue_end,
        ot_queue_incoming,
        ot_queue_tagged,
        merc_queue_end,
	merc_queue_incoming,
	merc_queue_tagged,
        suspend_cnt,
        eros_block_cnt,
        fit_block_cnt,
        user_block_cnt,
        sunday_not_codified_cnt,
        goto_suspend_cnt,
        goto_eros_block_cnt,
        goto_fit_block_cnt,
        goto_user_block_cnt,
        no_coverage_zip_cnt,
        goto_sunday_not_codified_cnt,
        view_queue_cnt
       )
      VALUES
       (TRUNC(in_date) + (in_hour / 24) + v_current_minutes,
        v_goto_florist_count,
        v_goto_florist_suspend,
        v_mercury_shop_count,
        v_mercury_shop_shutdown,
        v_covered_zip_count,
        v_gnadd_zip_count,
        v_ftd_queue_end,
        v_ftd_queue_incoming,
        v_ftd_queue_tagged,
        v_ftdm_queue_end,
        v_ftdm_queue_incoming,
        v_ftdm_queue_tagged,
        v_ask_queue_end,
        v_ask_queue_incoming,
        v_ask_queue_tagged,
        v_reject_queue_end,
        v_reject_queue_incoming,
        v_reject_queue_tagged,
        v_reject_queue_auto,
        v_ans_queue_end,
        v_ans_queue_incoming,
        v_ans_queue_tagged,
        v_den_queue_end,
        v_den_queue_incoming,
        v_den_queue_tagged,
        v_con_queue_end,
        v_con_queue_incoming,
        v_con_queue_tagged,
        v_can_queue_end,
        v_can_queue_incoming,
        v_can_queue_tagged,
        v_gen_queue_end,
        v_gen_queue_incoming,
        v_gen_queue_tagged,
        v_zip_queue_end,
        v_zip_queue_incoming,
        v_zip_queue_tagged,
        v_crd_queue_end,
        v_crd_queue_incoming,
        v_crd_queue_tagged,
        v_lp_queue_end,
        v_lp_queue_incoming,
        v_lp_queue_tagged,
        v_adj_queue_end,
        v_adj_queue_incoming,
        v_adj_queue_tagged,
        v_email_queue_end,
        v_email_queue_incoming,
        v_email_queue_tagged,
        v_email_auto_handle ,
        v_email_worked,
        v_cx_queue_end,
        v_cx_queue_incoming,
        v_cx_queue_tagged,
        v_mo_queue_end,
        v_mo_queue_incoming,
        v_mo_queue_tagged,
        v_nd_queue_end,
        v_nd_queue_incoming,
        v_nd_queue_tagged,
        v_qi_queue_end,
        v_qi_queue_incoming,
        v_qi_queue_tagged,
        v_pd_queue_end,
        v_pd_queue_incoming,
        v_pd_queue_tagged,
        v_bd_queue_end,
        v_bd_queue_incoming,
        v_bd_queue_tagged,
        v_se_queue_end,
        v_se_queue_incoming,
        v_se_queue_tagged,
        v_di_queue_end,
        v_di_queue_incoming,
        v_di_queue_tagged,
        v_qc_queue_end,
        v_qc_queue_incoming,
        v_qc_queue_tagged,
        v_oa_queue_end,
        v_oa_queue_incoming,
        v_oa_queue_tagged,
        v_ot_queue_end,
        v_ot_queue_incoming,
        v_ot_queue_tagged,
        v_merc_queue_end,
	v_merc_queue_incoming,
	v_merc_queue_tagged,
        v_suspend_cnt,
        v_eros_block_cnt,
        v_fit_block_cnt,
        v_user_block_cnt,
        v_sunday_not_codified_cnt,
        v_goto_suspend_cnt,
        v_goto_eros_block_cnt,
        v_goto_fit_block_cnt,
        v_goto_user_block_cnt,
        v_no_coverage_zip_cnt,
        v_goto_sunday_not_codified_cnt,
        v_view_queue_cnt
         );

  out_status := 'Y';

/*
  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;
*/

END clean_counts;

.
/