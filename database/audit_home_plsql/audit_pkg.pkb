CREATE OR REPLACE
PACKAGE BODY audit_home.AUDIT_PKG AS

PROCEDURE INSERT_AUDIT_REPORT_HEADER
(
IN_TYPE                         IN AUDIT_REPORT_HEADER.TYPE%TYPE,
IN_NAME                         IN AUDIT_REPORT_HEADER.NAME%TYPE,
IN_STATUS                       IN AUDIT_REPORT_HEADER.STATUS%TYPE,
IN_UPDATED_BY                   IN AUDIT_REPORT_HEADER.UPDATED_BY%TYPE,
OUT_REPORT_ID                   OUT AUDIT_REPORT_HEADER.REPORT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting all fields into the
        AUDIT_REPORT_HEADER table.

Input:
        report_id                       number
        type                            varchar2
        name                            varchar2
        report_comment                  varchar2
        status                          varchar2
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO audit_report_header
(
        report_id,
        type,
        name,
        status,
        creation_date,
        updated_date,
        updated_by
)
VALUES
(
        report_id_sq.nextval,
        in_type,
        in_name,
        in_status,
        sysdate,
        sysdate,
        in_updated_by
) returning report_id into out_report_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AUDIT_REPORT_HEADER;

PROCEDURE INSERT_AUDIT_REPORT_DETAIL
(
IN_REPORT_ID                    IN AUDIT_REPORT_DETAIL.REPORT_ID%TYPE,
IN_FIELD_NAME                   IN AUDIT_REPORT_DETAIL.FIELD_NAME%TYPE,
IN_ROW_SEQUENCE                 IN AUDIT_REPORT_DETAIL.ROW_SEQUENCE%TYPE,
IN_FILTER_VALUE                 IN AUDIT_REPORT_DETAIL.FILTER_VALUE%TYPE,
IN_VALUE_1                      IN AUDIT_REPORT_DETAIL.VALUE_1%TYPE,
IN_VALUE_2                      IN AUDIT_REPORT_DETAIL.VALUE_2%TYPE,
IN_VALUE_3                      IN AUDIT_REPORT_DETAIL.VALUE_3%TYPE,
IN_VALUE_4                      IN AUDIT_REPORT_DETAIL.VALUE_4%TYPE,
IN_VALUE_5                      IN AUDIT_REPORT_DETAIL.VALUE_5%TYPE,
IN_VALUE_6                      IN AUDIT_REPORT_DETAIL.VALUE_6%TYPE,
IN_VALUE_7                      IN AUDIT_REPORT_DETAIL.VALUE_7%TYPE,
IN_VALUE_8                      IN AUDIT_REPORT_DETAIL.VALUE_8%TYPE,
IN_VALUE_9                      IN AUDIT_REPORT_DETAIL.VALUE_9%TYPE,
IN_VALUE_10                     IN AUDIT_REPORT_DETAIL.VALUE_10%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting all fields into the
        AUDIT_REPORT_DETAIL table.

Input:
        report_id                       number
        field_name                      varchar2
        row_sequence                    number
        filter_value                    varchar2
        value_1                         varchar2
        value_2                         varchar2
        value_3                         varchar2
        value_4                         varchar2
        value_5                         varchar2
        value_6                         varchar2
        value_7                         varchar2
        value_8                         varchar2
        value_9                         varchar2
        value_10                        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO audit_report_detail
(
        report_id,
        field_name,
        row_sequence,
        filter_value,
        value_1,
        value_2,
        value_3,
        value_4,
        value_5,
        value_6,
        value_7,
        value_8,
        value_9,
        value_10
)
VALUES
(
        in_report_id,
        in_field_name,
        in_row_sequence,
        in_filter_value,
        in_value_1,
        in_value_2,
        in_value_3,
        in_value_4,
        in_value_5,
        in_value_6,
        in_value_7,
        in_value_8,
        in_value_9,
        in_value_10
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AUDIT_REPORT_DETAIL;


PROCEDURE UPDATE_AUDIT_REPORT_STATUS
(
IN_REPORT_ID                    IN AUDIT_REPORT_HEADER.REPORT_ID%TYPE,
IN_OLD_STATUS                   IN AUDIT_REPORT_HEADER.STATUS%TYPE,
IN_NEW_STATUS                   IN AUDIT_REPORT_HEADER.STATUS%TYPE,
IN_UPDATED_BY                   IN AUDIT_REPORT_HEADER.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the status of the audit
        report.

Input:
        report_id                       number
        old status                      varchar2
        new status                      varchar2
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message or RSNU code - old status changed not updated)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  audit_report_header
SET
        status = in_new_status,
        updated_date = sysdate,
        updated_by = in_updated_by
WHERE   report_id = in_report_id
AND     status = in_old_status;

IF sql%rowcount > 0 THEN
        out_status := 'Y';
ELSE
        out_status := 'N';
        out_message := 'RSNU::Report status not updated - old status was previously updated or the report does not exist';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_AUDIT_REPORT_STATUS;


PROCEDURE UPDATE_AUDIT_REPORT_COMMENT
(
IN_REPORT_ID                    IN AUDIT_REPORT_HEADER.REPORT_ID%TYPE,
IN_REPORT_COMMENT               IN AUDIT_REPORT_HEADER.REPORT_COMMENT%TYPE,
IN_UPDATED_BY                   IN AUDIT_REPORT_HEADER.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the comments for
        report.

Input:
        report_id                       number
        report_comment                  varchar2
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  audit_report_header
SET
        report_comment = in_report_comment,
        updated_date = sysdate,
        updated_by = in_updated_by
WHERE   report_id = in_report_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_AUDIT_REPORT_COMMENT;



PROCEDURE VIEW_AUDIT_REPORT_STATUS
(
IN_REPORT_ID                    IN AUDIT_REPORT_HEADER.REPORT_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the status and report
        dates for the given report.

Input:
        report_id                       number

Output:
        cursor containing status, creation_date, updated_date, updated_by
        from audit_report_header

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                status,
                creation_date,
                updated_date,
                updated_by
        FROM    audit_report_header
        WHERE   report_id = in_report_id;

END VIEW_AUDIT_REPORT_STATUS;


PROCEDURE VIEW_AUDIT_REPORT
(
IN_REPORT_ID                    IN AUDIT_REPORT_HEADER.REPORT_ID%TYPE,
OUT_HEADER_CUR                  OUT TYPES.REF_CURSOR,
OUT_DETAIL_CUR                  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the header and detail
        audit report records for the given report id.

Input:
        report_id                       number

Output:
        cursor containing all records from audit_report_header
        cursor containing all records from audit_report_detail

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_HEADER_CUR FOR
        SELECT
                report_id,
                type,
                name,
                report_comment,
                status,
                creation_date,
                updated_date,
                updated_by
        FROM    audit_report_header
        WHERE   report_id = in_report_id;

OPEN OUT_DETAIL_CUR FOR
        SELECT
                report_id,
                field_name,
                row_sequence,
                filter_value,
                value_1,
                value_2,
                value_3,
                value_4,
                value_5,
                value_6,
                value_7,
                value_8,
                value_9,
                value_10
        FROM    audit_report_detail
        WHERE   report_id = in_report_id
        ORDER BY row_sequence;

END VIEW_AUDIT_REPORT;


FUNCTION IS_AUDIT_APPROVED RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning an indicator if latest
        audit report in the current month is approved.

Input:
        none

Output:
        'Y' - approved
        'N' - not yet approved
-----------------------------------------------------------------------------*/

CURSOR C IS
        SELECT  status
        FROM    audit_report_header
        WHERE   report_id =
        (
                SELECT  max(report_id)
                FROM    audit_report_header
                WHERE   updated_date > to_date (to_char (sysdate, 'mm') || '01', 'mmdd')
        );

v_status        AUDIT_REPORT_HEADER.STATUS%TYPE;
v_return        varchar2(1) := 'N';

BEGIN

open c;
fetch c into v_status;
close c;

IF v_status = 'Approved' THEN
        v_return := 'Y';
END IF;

RETURN v_return;

END IS_AUDIT_APPROVED;


END AUDIT_PKG;
.
/
