CREATE OR REPLACE TRIGGER mercury.auto_response_key_phrase_$
AFTER INSERT OR UPDATE OR DELETE ON mercury.auto_response_key_phrase 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into mercury.auto_response_key_phrase$ (
          key_phrase_id, 
          msg_type,
          key_phrase_txt, 
          respond_with_can_flag, 
          sort_order_seq, 
          active_flag, 
          retry_ftd_flag,
	  florist_soft_block_flag,
          view_queue_flag,
          created_by, 
          created_on, 
          updated_by,
          updated_on,
          OPERATION$, TIMESTAMP$)
       VALUES (
         :NEW.key_phrase_id,                        
         :NEW.msg_type,          
         :NEW.key_phrase_txt,              
         :NEW.respond_with_can_flag,                  
         :NEW.sort_order_seq,                 
         :NEW.active_flag,               
         :NEW.retry_ftd_flag,
	 :NEW.florist_soft_block_flag,
         :NEW.view_queue_flag,
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'INS',v_current_timestamp);

   ELSIF UPDATING  THEN
      insert into mercury.auto_response_key_phrase$ (
          key_phrase_id, 
          msg_type,
          key_phrase_txt, 
          respond_with_can_flag, 
          sort_order_seq, 
          active_flag, 
          retry_ftd_flag,
	  florist_soft_block_flag,
          view_queue_flag,
          created_by, 
          created_on, 
          updated_by,
          updated_on,
          OPERATION$, TIMESTAMP$)
       VALUES (
         :OLD.key_phrase_id,                        
         :OLD.msg_type,          
         :OLD.key_phrase_txt,              
         :OLD.respond_with_can_flag,                  
         :OLD.sort_order_seq,                 
         :OLD.active_flag,               
         :OLD.retry_ftd_flag,
	 :OLD.florist_soft_block_flag,
         :OLD.view_queue_flag,
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'UPD_OLD',v_current_timestamp);

      insert into mercury.auto_response_key_phrase$ (
          key_phrase_id, 
          msg_type,
          key_phrase_txt, 
          respond_with_can_flag, 
          sort_order_seq, 
          active_flag, 
          retry_ftd_flag,
	  florist_soft_block_flag,
          view_queue_flag,
          created_by, 
          created_on, 
          updated_by,
          updated_on,
          OPERATION$, TIMESTAMP$)
       VALUES (
         :NEW.key_phrase_id,                        
         :NEW.msg_type,          
         :NEW.key_phrase_txt,              
         :NEW.respond_with_can_flag,                  
         :NEW.sort_order_seq,                 
         :NEW.active_flag,               
         :NEW.retry_ftd_flag,
	 :NEW.florist_soft_block_flag,
         :NEW.view_queue_flag,
         :NEW.CREATED_BY,
         :NEW.CREATED_ON,
         :NEW.UPDATED_BY,
         :NEW.UPDATED_ON,      
         'UPD_NEW',v_current_timestamp);


   ELSIF DELETING  THEN

      insert into mercury.auto_response_key_phrase$ (
          key_phrase_id, 
          msg_type,
          key_phrase_txt, 
          respond_with_can_flag, 
          sort_order_seq, 
          active_flag, 
          retry_ftd_flag,
	  florist_soft_block_flag,
          view_queue_flag,
          created_by, 
          created_on, 
          updated_by,
          updated_on,
          OPERATION$, TIMESTAMP$)
       VALUES (
         :OLD.key_phrase_id,                        
         :OLD.msg_type,          
         :OLD.key_phrase_txt,              
         :OLD.respond_with_can_flag,                  
         :OLD.sort_order_seq,                 
         :OLD.active_flag,               
         :OLD.retry_ftd_flag,
	 :OLD.florist_soft_block_flag,
         :OLD.view_queue_flag,
         :OLD.CREATED_BY,
         :OLD.CREATED_ON,
         :OLD.UPDATED_BY,
         :OLD.UPDATED_ON,      
         'DEL',v_current_timestamp);

   END IF;

END;
/
