CREATE OR REPLACE
TRIGGER mercury.trg_mercury_aiou
AFTER INSERT OR UPDATE OF MERCURY_STATUS
ON mercury.mercury
FOR EACH ROW

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***
***         Modified By : Divya Desai
***         Modified On : 05/01/2006
***              Reason : Code to ignore FOL messages
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE

   lchr_current_status   frp.order_state.status%TYPE := NULL;
   lchr_new_status   frp.order_state.status%TYPE := NULL;
   lchr_trigger_name   frp.order_state_history.trigger_name%TYPE := 'TRG_MERCURY_AIOU';
   lnum_order_detail_id   frp.order_state.order_detail_id%TYPE := TO_NUMBER(:NEW.reference_number);

   v_message_id NUMBER;
   v_status VARCHAR2(4000);
   v_message VARCHAR2(4000);

BEGIN

   -- fetch the current status
   lchr_current_status := frp.fun_get_current_status(lnum_order_detail_id);

   -- fetch the order_detail_id if null
   IF :NEW.reference_number IS NULL THEN
      lnum_order_detail_id := TO_NUMBER(mercury_get_order_detail_id(:NEW.mercury_order_number));
   END IF;

   IF INSERTING THEN

      -- initialize the status
      IF (:NEW.msg_type = 'FTD' AND :NEW.mercury_status = 'MO'
         AND :NEW.message_direction = 'OUTBOUND') THEN
         lchr_new_status := 'FTD_CREATED';
      ELSIF (:NEW.msg_type = 'FTD' AND :NEW.mercury_status IN ('MM', 'MN')
         AND :NEW.message_direction = 'OUTBOUND') THEN
         lchr_new_status := 'FTD_VERIFIED_MANUAL';
      ELSIF :NEW.msg_type = 'REJ' THEN
   	 lchr_new_status := 'FTD_REJECTED';
      END IF;

      -- ignore the FOL messages
      IF lnum_order_detail_id IS NULL THEN
         IF (:NEW.message_direction = 'OUTBOUND' AND :NEW.sending_florist = '90-8418') THEN
            lchr_new_status := NULL;
         ELSIF (:NEW.message_direction = 'INBOUND' AND :NEW.filling_florist LIKE '90-8418%') THEN
            lchr_new_status := NULL;
         END IF;
      END IF;

      -- populate the order state tables
      IF lchr_new_status IS NOT NULL THEN
         IF lnum_order_detail_id IS NULL THEN
            -- insert a system message
            FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
            (
               IN_SOURCE => 'MERCURY INTERFACE',
               IN_TYPE => 'ERROR',
               IN_MESSAGE => 'Order Detail Id is null for MERCURY_ID = ' || :NEW.mercury_id,
               IN_COMPUTER => sys_context('USERENV','HOST'),
               OUT_SYSTEM_MESSAGE_ID => v_message_id,
               OUT_STATUS => v_status,
               OUT_MESSAGE => v_message
            );
         ELSE
            -- insert or update into frp.order_state table
            frp.prc_pop_order_state (
               lnum_order_detail_id,
               lchr_new_status);

            -- insert into frp.order_state_history table
            frp.prc_pop_order_state_history (
               lnum_order_detail_id,
               lchr_new_status,
               lchr_current_status,
               lchr_trigger_name);
         END IF;
      END IF;

   END IF;

   IF UPDATING THEN

      -- initialize the status
      IF (:NEW.msg_type = 'FTD' AND :NEW.mercury_status = 'MC'
         AND :NEW.message_direction = 'OUTBOUND') THEN
   	 lchr_new_status := 'FTD_VERIFIED';
      END IF;

      -- ignore the FOL messages
      IF lnum_order_detail_id IS NULL THEN
         IF (:NEW.message_direction = 'OUTBOUND' AND :NEW.sending_florist = '90-8418') THEN
            lchr_new_status := NULL;
         ELSIF (:NEW.message_direction = 'INBOUND' AND :NEW.filling_florist LIKE '90-8418%') THEN
            lchr_new_status := NULL;
         END IF;
      END IF;

      -- Populate the order state tables
      IF lchr_new_status IS NOT NULL THEN
         IF lnum_order_detail_id IS NULL THEN
            -- insert a system message
            FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
            (
               IN_SOURCE => 'MERCURY INTERFACE',
               IN_TYPE => 'ERROR',
               IN_MESSAGE => 'Order Detail Id is null for MERCURY_ID = ' || :NEW.mercury_id,
               IN_COMPUTER => sys_context('USERENV','HOST'),
               OUT_SYSTEM_MESSAGE_ID => v_message_id,
               OUT_STATUS => v_status,
               OUT_MESSAGE => v_message
            );
         ELSE
            -- insert or update into frp.order_state table
            frp.prc_pop_order_state (
               lnum_order_detail_id,
               lchr_new_status);

            -- insert into frp.order_state_history table
            frp.prc_pop_order_state_history (
               lnum_order_detail_id,
               lchr_new_status,
               lchr_current_status,
               lchr_trigger_name);
         END IF;
      END IF;

   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In MERCURY.TRG_MERCURY_AIOU: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_mercury_aiou;
.
/
