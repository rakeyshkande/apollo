CREATE OR REPLACE PROCEDURE MERCURY.GET_MERCURY_ORDER_STATUS
(
IN_MERCURY_ORDER_NUMBER              IN VARCHAR2,
IN_CREATED_ON								 IN DATE,
OUT_CUR                              OUT TYPES.REF_CURSOR
)
AS

/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 05/12/2006
***              Reason : Enhancement825: Create new Member Order Distribution Report
***             Purpose : Returns mercury messages for the given order number
***
***    Input Parameters : in_mercury_order_number - message order number - varchar2
***
***   Output Parameters : has_live_ftd - varchar2
***                       cancel_sent - varchar2
***                       reject_sent - varchar2
***                       ftd_sent - varchar2
***                       for_sent - varchar2
***
*** Special Instructions: 
***
*****************************************************************************************/

	-- cursor to get all of the mercury messages for the order
	CURSOR merc_cur IS
	SELECT  
		m.mercury_id,
		m.msg_type,
		m.filling_florist,
		m.sak_text,
		m.reference_number,
		m.delivery_date,
		m.mercury_status,
		m.ask_answer_code,
		m.message_direction,
		m.mercury_order_number,
		(SELECT 'Y' FROM mercury.call_out_log co WHERE co.mercury_id = m.mercury_id) message_called_out
	FROM  
		mercury.mercury m
	WHERE  
		m.mercury_order_number = in_mercury_order_number
		AND m.created_on BETWEEN in_created_on AND in_created_on + 90
	ORDER BY 
		DECODE (m.msg_type, 'FTD', 9, 1),
		m.created_on DESC;


	v_has_live_ftd          varchar2(1) := 'N';
	v_den_sent					varchar2(1) := 'N';
	v_cancel_sent           varchar2(1) := 'N';
	v_ftd_sent              varchar2(1) := 'N';
	v_reject_sent           varchar2(1) := 'N';
	v_for_sent              varchar2(1) := 'N';

BEGIN



	-- process the messages in the reverse order from when they were sent
	-- stop at the first FTD message found

	-- get the message status from the last mercury order number
	FOR msg IN merc_cur LOOP

		CASE msg.msg_type

		WHEN 'FTD' THEN

			-- if the florist has mercury, check that the message is verified
			-- if the florist is a manual florist, do not need to check for a verified message
			IF msg.mercury_status = 'MC' THEN
				v_ftd_sent := 'Y';
				-- if no CAN or REJ message was previously found, the order has a live FTD message
				IF (v_cancel_sent = 'N' AND v_reject_sent = 'N') THEN
					v_has_live_ftd := 'Y';
				END IF;
			ELSIF (msg.mercury_status = 'MM' OR msg.mercury_status = 'MN') AND msg.message_called_out = 'Y' THEN
				v_ftd_sent := 'Y';
				-- MM = manual from order processing (queue created)
				-- MN = manual from communcation screen (no queue)
				-- manual message is verified if there is a record in the call out log
				-- if no CAN or REJ message was previous found, the order has a live FTD message
				IF (v_cancel_sent = 'N' AND v_reject_sent = 'N') THEN
					v_has_live_ftd := 'Y';
				END IF;
			END IF;

			EXIT;

		WHEN 'CAN' THEN
			-- check for a verified cancel message
			-- manual message is verified if there is a record in the call out log
			IF (msg.mercury_status = 'MC' OR
				((msg.mercury_status = 'MM' OR
				  msg.mercury_status = 'MN') AND
				  msg.message_called_out = 'Y')) AND
				v_den_sent = 'N' THEN
				v_cancel_sent := 'Y';
				v_has_live_ftd := 'N';
			END IF;

		WHEN 'REJ' THEN
			-- set that the florist rejected the order
			v_reject_sent := 'Y';
			v_has_live_ftd := 'N';

			WHEN 'DEN' THEN
				-- set that the florist rejected cancelling the order
				v_den_sent := 'Y';

			WHEN 'FOR' THEN
				-- check for cancel or reject message
				IF v_reject_sent = 'N' AND v_cancel_sent = 'N' THEN
				   v_for_sent := 'Y';
				END IF;

		ELSE
			NULL;

		END CASE;

	END LOOP;

	OPEN out_cur FOR
	SELECT  
	   v_has_live_ftd has_live_ftd,
		v_cancel_sent cancel_sent,
		v_reject_sent reject_sent,
		v_ftd_sent ftd_sent,
		v_for_sent for_sent,
		v_den_sent den_sent
	FROM    
	   dual;

END GET_MERCURY_ORDER_STATUS;
/