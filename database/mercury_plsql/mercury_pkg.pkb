CREATE OR REPLACE
PACKAGE BODY mercury.MERCURY_PKG AS

PROCEDURE INSERT_MERCURY
(
IN_MERCURY_ID                   IN VARCHAR2,
IN_MERCURY_MESSAGE_NUMBER       IN VARCHAR2,
IN_MERCURY_ORDER_NUMBER         IN VARCHAR2,
IN_MERCURY_STATUS               IN VARCHAR2,
IN_MSG_TYPE                     IN VARCHAR2,
IN_OUTBOUND_ID                  IN VARCHAR2,
IN_SENDING_FLORIST              IN VARCHAR2,
IN_FILLING_FLORIST              IN VARCHAR2,
IN_ORDER_DATE                   IN DATE,
IN_RECIPIENT                    IN VARCHAR2,
IN_ADDRESS                      IN VARCHAR2,
IN_CITY_STATE_ZIP               IN VARCHAR2,
IN_PHONE_NUMBER                 IN VARCHAR2,
IN_DELIVERY_DATE                IN DATE,
IN_DELIVERY_DATE_TEXT           IN VARCHAR2,
IN_FIRST_CHOICE                 IN VARCHAR2,
IN_SECOND_CHOICE                IN VARCHAR2,
IN_PRICE                        IN NUMBER,
IN_CARD_MESSAGE                 IN VARCHAR2,
IN_OCCASION                     IN VARCHAR2,
IN_SPECIAL_INSTRUCTIONS         IN VARCHAR2,
IN_PRIORITY                     IN VARCHAR2,
IN_OPERATOR                     IN VARCHAR2,
IN_COMMENTS                     IN VARCHAR2,
IN_SAK_TEXT                     IN VARCHAR2,
IN_CTSEQ                        IN NUMBER,
IN_CRSEQ                        IN NUMBER,
IN_TRANSMISSION_TIME            IN DATE,
IN_REFERENCE_NUMBER             IN VARCHAR2,
IN_PRODUCT_ID                   IN VARCHAR2,
IN_ZIP_CODE                     IN VARCHAR2,
IN_ASK_ANSWER_CODE              IN CHAR,
IN_SORT_VALUE                   IN VARCHAR2,
IN_RETRIEVAL_FLAG               IN CHAR,
IN_COMBINED_REPORT_NUMBER       IN VARCHAR2,
IN_ROF_NUMBER                   IN VARCHAR2,
IN_ADJ_REASON_CODE              IN VARCHAR2,
IN_OVER_UNDER_CHARGE            IN NUMBER,
IN_FROM_MESSAGE_NUMBER          IN VARCHAR2,
IN_FROM_MESSAGE_DATE            IN VARCHAR2,
IN_TO_MESSAGE_NUMBER            IN VARCHAR2,
IN_TO_MESSAGE_DATE              IN VARCHAR2,
IN_VIEW_QUEUE                   IN CHAR,
IN_MESSAGE_DIRECTION            IN VARCHAR2,
IN_COMP_ORDER                   IN CHAR,
IN_REQUIRE_CONFIRMATION         IN CHAR,
IN_SUFFIX                       IN VARCHAR2,
IN_ORDER_SEQ                    IN VARCHAR2,
IN_ADMIN_SEQ                    IN VARCHAR2,
IN_OLD_PRICE                    IN NUMBER,
IN_ORIG_COMPLAINT_COMM_TYPE_ID  IN VARCHAR2,
IN_NOTI_COMPLAINT_COMM_TYPE_ID  IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting mercury information.

Input:
        mercury_id                      varchar2
        mercury_message_number          varchar2
        mercury_order_number            varchar2
        mercury_status                  varchar2
        msg_type                        varchar2
        outbound_id                     varchar2
        sending_florist                 varchar2
        filling_florist                 varchar2
        order_date                      date
        recipient                       varchar2
        address                         varchar2
        city_state_zip                  varchar2
        phone_number                    varchar2
        delivery_date                   date
        delivery_date_text              varchar2
        first_choice                    varchar2
        second_choice                   varchar2
        price                           number
        card_message                    varchar2
        occasion                        varchar2
        special_instructions            varchar2
        priority                        varchar2
        operator                        varchar2
        comments                        varchar2
        sak_text                        varchar2
        ctseq                           number
        crseq                           number
        transmission_time               date
        reference_number                varchar2
        product_id                      varchar2
        zip_code                        varchar2
        ask_answer_code                 char
        sort_value                      varchar2
        retrieval_flag                  char
        combined_report_number          varchar2
        rof_number                      varchar2
        adj_reason_code                 varchar2
        over_under_charge               number
        from_message_number             varchar2
        from_message_date               varchar2
        to_message_number               varchar2
        to_message_date                 varchar2
        view_queue                      char
        message_direction               varchar2
        comp_order                      char
        require_confirmation            char
        suffix                          varchar2
        order_seq                       varchar2
        admin_seq                       varchar2
        old_price                       number,
        origin_complaint_comm_type_id   varchar2,
        notif_complaint_comm_type_id    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


v_resp_florist varchar2(20);
v_product_id  MERCURY.PRODUCT_ID%TYPE;

CURSOR merc_cur IS
        SELECT  filling_florist
        FROM    mercury.mercury
        WHERE   MERCURY_ORDER_NUMBER = in_mercury_order_number
        AND     created_on = (
                        select  max(created_on)
                        from    mercury.mercury
                        where   MERCURY_ORDER_NUMBER = in_mercury_order_number
                        and     (msg_type = 'FOR' or msg_type = 'FTD'));

BEGIN


v_product_id := in_product_id;

IF in_view_queue = 'Y' THEN
        IF in_message_direction = 'INBOUND' THEN
           IF  (in_msg_type = 'REJ') or (in_msg_type = 'FOR')  THEN
                  IF in_msg_type = 'REJ' THEN
                    IF SUBSTR(in_sending_florist,3,1) = '-' THEN
                         v_resp_florist := in_sending_florist;
                     ELSE

                        OPEN merc_cur;
                        FETCH merc_cur INTO v_resp_florist;
                        CLOSE merc_cur;

                     END IF;
                 ELSE

                        OPEN merc_cur;
                        FETCH merc_cur INTO v_resp_florist;
                        CLOSE merc_cur;

                 END IF;

           ELSE

               v_resp_florist := in_sending_florist;
           END IF;
        ELSE

                v_resp_florist := in_filling_florist;

        END IF;
END IF;

INSERT INTO mercury
(
        mercury_id,
        mercury_message_number,
        mercury_order_number,
        mercury_status,
        msg_type,
        outbound_id,
        sending_florist,
        filling_florist,
        order_date,
        recipient,
        address,
        city_state_zip,
        phone_number,
        delivery_date,
        delivery_date_text,
        first_choice,
        second_choice,
        price,
        card_message,
        occasion,
        special_instructions,
        priority,
        operator,
        comments,
        sak_text,
        ctseq,
        crseq,
        transmission_time,
        reference_number,
        product_id,
        zip_code,
        ask_answer_code,
        sort_value,
        retrieval_flag,
        combined_report_number,
        rof_number,
        adj_reason_code,
        over_under_charge,
        from_message_number,
        from_message_date,
        to_message_number,
        to_message_date,
        view_queue,
        responsible_florist,
        message_direction,
        comp_order,
        require_confirmation,
        suffix,
        order_seq,
        admin_seq,
        old_price,
        origin_complaint_comm_type_id,
        notif_complaint_comm_type_id
)
VALUES
(
        in_mercury_id,
        in_mercury_message_number,
        in_mercury_order_number,
        in_mercury_status,
        in_msg_type,
        in_outbound_id,
        in_sending_florist,
        in_filling_florist,
        in_order_date,
        in_recipient,
        in_address,
        in_city_state_zip,
        in_phone_number,
        in_delivery_date,
        in_delivery_date_text,
        in_first_choice,
        in_second_choice,
        in_price,
        in_card_message,
        in_occasion,
        in_special_instructions,
        in_priority,
        in_operator,
        in_comments,
        in_sak_text,
        in_ctseq,
        in_crseq,
        in_transmission_time,
        in_reference_number,
        v_product_id,
        in_zip_code,
        in_ask_answer_code,
        in_sort_value,
        in_retrieval_flag,
        in_combined_report_number,
        in_rof_number,
        in_adj_reason_code,
        in_over_under_charge,
        in_from_message_number,
        in_from_message_date,
        in_to_message_number,
        in_to_message_date,
        in_view_queue,
        v_resp_florist,
        in_message_direction,
        in_comp_order,
        in_require_confirmation,
        in_suffix,
        in_order_seq,
        in_admin_seq,
        in_old_price,
        in_orig_complaint_comm_type_id,
        in_noti_complaint_comm_type_id
);

OUT_STATUS := 'Y';



EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MERCURY;


PROCEDURE INSERT_MERCURY_WITH_APPROVAL
(
IN_MERCURY_ID                   IN VARCHAR2,
IN_MERCURY_MESSAGE_NUMBER       IN VARCHAR2,
IN_MERCURY_ORDER_NUMBER         IN VARCHAR2,
IN_MERCURY_STATUS               IN VARCHAR2,
IN_MSG_TYPE                     IN VARCHAR2,
IN_OUTBOUND_ID                  IN VARCHAR2,
IN_SENDING_FLORIST              IN VARCHAR2,
IN_FILLING_FLORIST              IN VARCHAR2,
IN_ORDER_DATE                   IN DATE,
IN_RECIPIENT                    IN VARCHAR2,
IN_ADDRESS                      IN VARCHAR2,
IN_CITY_STATE_ZIP               IN VARCHAR2,
IN_PHONE_NUMBER                 IN VARCHAR2,
IN_DELIVERY_DATE                IN DATE,
IN_DELIVERY_DATE_TEXT           IN VARCHAR2,
IN_FIRST_CHOICE                 IN VARCHAR2,
IN_SECOND_CHOICE                IN VARCHAR2,
IN_PRICE                        IN NUMBER,
IN_CARD_MESSAGE                 IN VARCHAR2,
IN_OCCASION                     IN VARCHAR2,
IN_SPECIAL_INSTRUCTIONS         IN VARCHAR2,
IN_PRIORITY                     IN VARCHAR2,
IN_OPERATOR                     IN VARCHAR2,
IN_COMMENTS                     IN VARCHAR2,
IN_SAK_TEXT                     IN VARCHAR2,
IN_CTSEQ                        IN NUMBER,
IN_CRSEQ                        IN NUMBER,
IN_TRANSMISSION_TIME            IN DATE,
IN_REFERENCE_NUMBER             IN VARCHAR2,
IN_PRODUCT_ID                   IN VARCHAR2,
IN_ZIP_CODE                     IN VARCHAR2,
IN_ASK_ANSWER_CODE              IN CHAR,
IN_SORT_VALUE                   IN VARCHAR2,
IN_RETRIEVAL_FLAG               IN CHAR,
IN_COMBINED_REPORT_NUMBER       IN VARCHAR2,
IN_ROF_NUMBER                   IN VARCHAR2,
IN_ADJ_REASON_CODE              IN VARCHAR2,
IN_OVER_UNDER_CHARGE            IN NUMBER,
IN_FROM_MESSAGE_NUMBER          IN VARCHAR2,
IN_FROM_MESSAGE_DATE            IN VARCHAR2,
IN_TO_MESSAGE_NUMBER            IN VARCHAR2,
IN_TO_MESSAGE_DATE              IN VARCHAR2,
IN_VIEW_QUEUE                   IN CHAR,
IN_MESSAGE_DIRECTION            IN VARCHAR2,
IN_COMP_ORDER                   IN CHAR,
IN_REQUIRE_CONFIRMATION         IN CHAR,
IN_SUFFIX                       IN VARCHAR2,
IN_ORDER_SEQ                    IN VARCHAR2,
IN_ADMIN_SEQ                    IN VARCHAR2,
IN_OLD_PRICE                    IN NUMBER,
IN_ORIG_COMPLAINT_COMM_TYPE_ID  IN VARCHAR2,
IN_NOTI_COMPLAINT_COMM_TYPE_ID  IN VARCHAR2,
IN_APPROVAL_IDENTITY_ID         IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting mercury information.

Input:
        mercury_id                      varchar2
        mercury_message_number          varchar2
        mercury_order_number            varchar2
        mercury_status                  varchar2
        msg_type                        varchar2
        outbound_id                     varchar2
        sending_florist                 varchar2
        filling_florist                 varchar2
        order_date                      date
        recipient                       varchar2
        address                         varchar2
        city_state_zip                  varchar2
        phone_number                    varchar2
        delivery_date                   date
        delivery_date_text              varchar2
        first_choice                    varchar2
        second_choice                   varchar2
        price                           number
        card_message                    varchar2
        occasion                        varchar2
        special_instructions            varchar2
        priority                        varchar2
        operator                        varchar2
        comments                        varchar2
        sak_text                        varchar2
        ctseq                           number
        crseq                           number
        transmission_time               date
        reference_number                varchar2
        product_id                      varchar2
        zip_code                        varchar2
        ask_answer_code                 char
        sort_value                      varchar2
        retrieval_flag                  char
        combined_report_number          varchar2
        rof_number                      varchar2
        adj_reason_code                 varchar2
        over_under_charge               number
        from_message_number             varchar2
        from_message_date               varchar2
        to_message_number               varchar2
        to_message_date                 varchar2
        view_queue                      char
        message_direction               varchar2
        comp_order                      char
        require_confirmation            char
        suffix                          varchar2
        order_seq                       varchar2
        admin_seq                       varchar2
        old_price                       number,
        origin_complaint_comm_type_id   varchar2,
        notif_complaint_comm_type_id    varchar2,
        approval_identity_id            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


v_resp_florist varchar2(20);
v_product_id  MERCURY.PRODUCT_ID%TYPE;

CURSOR merc_cur IS
        SELECT  filling_florist
        FROM    mercury.mercury
        WHERE   MERCURY_ORDER_NUMBER = in_mercury_order_number
        AND     created_on = (
                        select  max(created_on)
                        from    mercury.mercury
                        where   MERCURY_ORDER_NUMBER = in_mercury_order_number
                        and     (msg_type = 'FOR' or msg_type = 'FTD'));

BEGIN


v_product_id := in_product_id;

IF in_view_queue = 'Y' THEN
        IF in_message_direction = 'INBOUND' THEN
           IF  (in_msg_type = 'REJ') or (in_msg_type = 'FOR')  THEN
                  IF in_msg_type = 'REJ' THEN
                    IF SUBSTR(in_sending_florist,3,1) = '-' THEN
                         v_resp_florist := in_sending_florist;
                     ELSE

                        OPEN merc_cur;
                        FETCH merc_cur INTO v_resp_florist;
                        CLOSE merc_cur;

                     END IF;
                 ELSE

                        OPEN merc_cur;
                        FETCH merc_cur INTO v_resp_florist;
                        CLOSE merc_cur;

                 END IF;

           ELSE

               v_resp_florist := in_sending_florist;
           END IF;
        ELSE

                v_resp_florist := in_filling_florist;

        END IF;
END IF;

INSERT INTO mercury
(
        mercury_id,
        mercury_message_number,
        mercury_order_number,
        mercury_status,
        msg_type,
        outbound_id,
        sending_florist,
        filling_florist,
        order_date,
        recipient,
        address,
        city_state_zip,
        phone_number,
        delivery_date,
        delivery_date_text,
        first_choice,
        second_choice,
        price,
        card_message,
        occasion,
        special_instructions,
        priority,
        operator,
        comments,
        sak_text,
        ctseq,
        crseq,
        transmission_time,
        reference_number,
        product_id,
        zip_code,
        ask_answer_code,
        sort_value,
        retrieval_flag,
        combined_report_number,
        rof_number,
        adj_reason_code,
        over_under_charge,
        from_message_number,
        from_message_date,
        to_message_number,
        to_message_date,
        view_queue,
        responsible_florist,
        message_direction,
        comp_order,
        require_confirmation,
        suffix,
        order_seq,
        admin_seq,
        old_price,
        origin_complaint_comm_type_id,
        notif_complaint_comm_type_id,
        approval_identity_id
)
VALUES
(
        in_mercury_id,
        in_mercury_message_number,
        in_mercury_order_number,
        in_mercury_status,
        in_msg_type,
        in_outbound_id,
        in_sending_florist,
        in_filling_florist,
        in_order_date,
        in_recipient,
        in_address,
        in_city_state_zip,
        in_phone_number,
        in_delivery_date,
        in_delivery_date_text,
        in_first_choice,
        in_second_choice,
        in_price,
        in_card_message,
        in_occasion,
        in_special_instructions,
        in_priority,
        in_operator,
        in_comments,
        in_sak_text,
        in_ctseq,
        in_crseq,
        in_transmission_time,
        in_reference_number,
        v_product_id,
        in_zip_code,
        in_ask_answer_code,
        in_sort_value,
        in_retrieval_flag,
        in_combined_report_number,
        in_rof_number,
        in_adj_reason_code,
        in_over_under_charge,
        in_from_message_number,
        in_from_message_date,
        in_to_message_number,
        in_to_message_date,
        in_view_queue,
        v_resp_florist,
        in_message_direction,
        in_comp_order,
        in_require_confirmation,
        in_suffix,
        in_order_seq,
        in_admin_seq,
        in_old_price,
        in_orig_complaint_comm_type_id,
        in_noti_complaint_comm_type_id,
        IN_APPROVAL_IDENTITY_ID
);

OUT_STATUS := 'Y';



EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MERCURY_WITH_APPROVAL;


PROCEDURE UPDATE_MERCURY_STATUS_TIMEST
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_MERCURY_STATUS               IN MERCURY.MERCURY_STATUS%TYPE,
IN_TRANSMISSION_TIME            IN MERCURY.TRANSMISSION_TIME%TYPE,
IN_SAK_TEXT                     IN MERCURY.SAK_TEXT%TYPE,
IN_SUFFIX                       IN MERCURY.SUFFIX%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury status and timestamp.

Input:
        mercury_id                      varchar2
        mercury_status                  varchar2
        transmission_time               date
        suffix                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  mercury
SET     mercury_status = in_mercury_status,
        transmission_time = in_transmission_time,
        sak_text = nvl(in_sak_text, sak_text),
        suffix = nvl(in_suffix, suffix)
WHERE   mercury_id = in_mercury_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_STATUS_TIMEST;


PROCEDURE UPDATE_MERCURY
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_MERCURY_MESSAGE_NUMBER       IN MERCURY.MERCURY_MESSAGE_NUMBER%TYPE,
IN_MERCURY_STATUS               IN MERCURY.MERCURY_STATUS%TYPE,
IN_SAK_TEXT                     IN MERCURY.SAK_TEXT%TYPE,
IN_CTSEQ                        IN MERCURY.CTSEQ%TYPE,
IN_CRSEQ                        IN MERCURY.CRSEQ%TYPE,
IN_TRANSMISSION_TIME            IN MERCURY.TRANSMISSION_TIME%TYPE,
IN_MERCURY_ORDER_NUMBER         IN MERCURY.MERCURY_ORDER_NUMBER%TYPE,
IN_VIEW_QUEUE                   IN MERCURY.VIEW_QUEUE%TYPE,
IN_MESSAGE_DIRECTION            IN MERCURY.MESSAGE_DIRECTION%TYPE,
IN_SUFFIX                       IN MERCURY.SUFFIX%TYPE,
IN_ORDER_SEQ                    IN MERCURY.ORDER_SEQ%TYPE,
IN_ADMIN_SEQ                    IN MERCURY.ADMIN_SEQ%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury information.

Input:
        mercury_id                      varchar2
        mercury_message_number          varchar2
        mercury_status                  varchar2
        sak_text                        varchar2
        ctseq                           number
        crseq                           number
        transmission_time               date
        mercury_order_number            varchar2
        view_queue                      char
        message_direction               varchar2
        suffix                          varchar2
        order_seq                       varchar2
        admin_seq                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

update_mercury_status_timest
(
        in_mercury_id,
        in_mercury_status,
        in_transmission_time,
        in_sak_text,
        NULL,
        out_status,
        out_message
);

IF out_status = 'Y' THEN
        UPDATE  mercury
        SET     mercury_message_number = in_mercury_message_number,
                ctseq = in_ctseq,
                crseq = in_crseq,
                mercury_order_number = nvl (in_mercury_order_number, mercury_order_number),
                view_queue = nvl (in_view_queue, view_queue),
                responsible_florist = (filling_florist),
                message_direction       = in_message_direction,
                suffix                  = in_suffix,
                order_seq               = in_order_seq,
                admin_seq               = in_admin_seq
        WHERE   mercury_id = in_mercury_id;

        OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY;


PROCEDURE UPDATE_MERCURY_OUTBOUND_ID
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_OUTBOUND_ID                  IN MERCURY.OUTBOUND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the given mercury record
        with the given outbound id.

Input:
        mercury_status                  varchar2
        outbound_id                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;
RECORD_LOCKED EXCEPTION;
PRAGMA EXCEPTION_INIT (RECORD_LOCKED, -54);

CURSOR  mercury_cur IS
SELECT  outbound_id
FROM    mercury
WHERE   mercury_id = in_mercury_id
AND     outbound_id IS NULL
FOR UPDATE OF outbound_id NOWAIT;

v_outbound_id           mercury.outbound_id%type;

BEGIN

OPEN mercury_cur;

FETCH mercury_cur INTO v_outbound_id;

IF mercury_cur%found THEN
        UPDATE  mercury
        SET     outbound_id = in_outbound_id
        WHERE   CURRENT OF mercury_cur;

        COMMIT;
        OUT_STATUS := 'Y';
ELSE
        RAISE RECORD_LOCKED;
END IF;

CLOSE mercury_cur;

-- error - record already locked
EXCEPTION WHEN RECORD_LOCKED THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'RECORD LOCKED';
END;    -- end record locked exception

WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_OUTBOUND_ID;


PROCEDURE INSERT_MERCURY_HP
(
IN_MERCURY_ID                   IN MERCURY_HP.MERCURY_ID%TYPE,
IN_TIMESTAMP                    IN MERCURY_HP.TIMESTAMP%TYPE,
IN_STATUS                       IN MERCURY_HP.STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting mercury hp information.

Input:
        mercury_id                      varchar2
        timestamp                       date
        status                          char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO mercury_hp
(
        mercury_id,
        timestamp,
        status
)
VALUES
(
        in_mercury_id,
        in_timestamp,
        in_status
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MERCURY_HP;


PROCEDURE UPDATE_MERCURY_OPS
(
IN_SUFFIX                       IN MERCURY_OPS.SUFFIX%TYPE,
IN_IN_USE_FLAG                  IN MERCURY_OPS.IN_USE_FLAG%TYPE,
IN_LAST_TRANSMISSION            IN MERCURY_OPS.LAST_TRANSMISSION%TYPE,
IN_LAST_MESSAGE_ACTIVITY        IN MERCURY_OPS.LAST_MESSAGE_ACTIVITY%TYPE,
IN_AVAILABLE_FLAG               IN MERCURY_OPS.AVAILABLE_FLAG%TYPE,
IN_SEND_NEW_ORDERS              IN MERCURY_OPS.SEND_NEW_ORDERS%TYPE,
IN_SLEEP_TIME                   IN MERCURY_OPS.SLEEP_TIME%TYPE,
IN_BATCH_SIZE                   IN MERCURY_OPS.BATCH_SIZE%TYPE,
IN_ACK_ASK                      IN MERCURY_OPS.ACK_ASK%TYPE,
IN_ACK_COUNT                    IN MERCURY_OPS.ACK_COUNT%TYPE,
IN_RESEND_BATCH                 IN MERCURY_OPS.RESEND_BATCH%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury ops information.

Input:
        suffix                          varchar2
        in_use_flag                     char
        last_transmission               date
        last_message_activity           date
        available_flag                  char
        send_new_orders                 char
        sleep_time                      number
        batch_size                      number
        ack_ask                         char
        ack_count                       number
        resend_batch                    char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE  mercury_ops
SET     in_use_flag = in_in_use_flag,
        last_transmission = in_last_transmission,
        last_message_activity = in_last_message_activity,
        available_flag = in_available_flag,
        send_new_orders = in_send_new_orders,
        sleep_time = in_sleep_time,
        batch_size = in_batch_size,
        ack_ask = in_ack_ask,
        ack_count = in_ack_count,
        resend_batch = in_resend_batch
WHERE   suffix = in_suffix;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_OPS;


PROCEDURE UPDATE_MERCURY_OPS_SEQUENCES
(
 IN_SUFFIX                       IN MERCURY_OPS.SUFFIX%TYPE,
 IN_INBOUND_ADMIN_SEQUENCE       IN MERCURY_OPS.INBOUND_ADMIN_SEQUENCE%TYPE,
 IN_OUTBOUND_ADMIN_SEQUENCE      IN MERCURY_OPS.OUTBOUND_ADMIN_SEQUENCE%TYPE,
 IN_INBOUND_ORDER_SEQUENCE       IN MERCURY_OPS.INBOUND_ORDER_SEQUENCE%TYPE,
 IN_OUTBOUND_ORDER_SEQUENCE      IN MERCURY_OPS.OUTBOUND_ORDER_SEQUENCE%TYPE,
 IN_SEQUENCE_CHECK_LOCK          IN MERCURY_OPS.SEQUENCE_CHECK_LOCK%TYPE,
 OUT_STATUS                      OUT VARCHAR2,
 OUT_ERROR_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury ops
         sequence information.

Input:
        suffix                          varchar2
        inbound_admin_sequence          number
        outbound_admin_sequence         number
        inbound_order_sequence          number
        outbound_order_sequence         number
        sequence_check_lock             char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE mercury_ops
     SET inbound_admin_sequence  = in_inbound_admin_sequence,
         outbound_admin_sequence = in_outbound_admin_sequence,
         inbound_order_sequence  = in_inbound_order_sequence,
         outbound_order_sequence = in_outbound_order_sequence,
         sequence_check_lock     = in_sequence_check_lock
   WHERE suffix = in_suffix;

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_error_message := 'WARNING: No mercury_ops updated for suffix ' || in_suffix ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
        out_status := 'N';
        out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_MERCURY_OPS_SEQUENCES;


PROCEDURE VIEW_MERCURY_FOR_OUTBOUND
(
IN_MERCURY_STATUS               IN MERCURY.MERCURY_STATUS%TYPE,
IN_OUTBOUND_ID                  IN MERCURY.OUTBOUND_ID%TYPE,
IN_GET_NULL_FLAG                IN VARCHAR2,
IN_RECORD_LIMIT                 IN NUMBER := 10,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury records for the
        given outbound id.  If the get null flag is passed in as 'Y', then
        unassigned mercury records will be updated with the given outbound id.
        The default record limit is 10.

Input:
        mercury_status                  varchar2
        outbound_id                     varchar2
        get null flag                   varchar2

Output:
        cursor result set containing all fields in mercury
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/

TYPE id_tab_typ IS TABLE OF mercury.mercury_id%type INDEX BY PLS_INTEGER;
v_mercury_tab           id_tab_typ;
v_mercury_id            mercury.mercury_id%type;
v_outbound_id           mercury.outbound_id%type;
v_sending_florist       mercury.sending_florist%type;
v_first_florist         mercury.sending_florist%type;

v_record_number number := 0;

v_sql           varchar2(4000);
v_list          varchar2(2000);

CURSOR mercury_cur IS
    select * from (
        SELECT  mercury_id,
                outbound_id,
                NVL(sending_florist, 'xx-xxxx') sending_florist
        FROM    mercury
        WHERE   mercury_status = in_mercury_status
        AND     (outbound_id = in_outbound_id
        OR      (in_get_null_flag = 'Y' AND outbound_id IS NULL))
        ORDER BY sort_value
    ) where rownum <= 150;

BEGIN

OPEN mercury_cur;
FETCH mercury_cur INTO v_mercury_id, v_outbound_id, v_sending_florist;
v_first_florist := v_sending_florist;
out_status := 'Y';
out_message := null;

WHILE mercury_cur%found AND v_record_number < in_record_limit LOOP
        -- get create a list of mercury messages with the same sending florist id
        -- null sending florists are changed to xx-xxxx for matching
        IF v_sending_florist = v_first_florist THEN

                -- assign the mercury message to the given outbound id if it's still available
                IF v_outbound_id IS NULL THEN
                        -- set the outbound on unassigned records
                        UPDATE_MERCURY_OUTBOUND_ID
                        (
                                v_mercury_id,
                                in_outbound_id,
                                out_status,
                                out_message
                        );
                END IF;

                -- add mercury message to the list
                IF out_status = 'Y' THEN
                        v_record_number := v_record_number + 1;
                        v_mercury_tab (v_record_number) := v_mercury_id;

                -- mercury message was taken by another processes
                ELSIF out_message = 'RECORD LOCKED' THEN
                        null;

                -- error occurred exit the process
                ELSE
                        exit;
                END IF;
        END IF;

        -- get the next available mercury message
        FETCH mercury_cur INTO v_mercury_id, v_outbound_id, v_sending_florist;
        out_status := 'Y';
        out_message := null;
END LOOP;
CLOSE mercury_cur;

IF v_mercury_tab.count > 0 THEN
        v_list := 'mercury_id in (';
        FOR x IN 1..v_mercury_tab.count LOOP
                v_list := v_list || '''' || v_mercury_tab (x) || ''',';
        END LOOP;
        v_list := substr (v_list, 1, length(v_list) - 1) || ')';
ELSE
        v_list := '1=0';
END IF;

v_sql := 'SELECT
                upper(translate (mercury_id, chr(9), chr(32))) mercury_id,
                upper(translate (mercury_message_number, chr(9), chr(32))) mercury_message_number,
                upper(translate (mercury_order_number, chr(9), chr(32))) mercury_order_number,
                upper(translate (mercury_status, chr(9), chr(32))) mercury_status,
                upper(translate (msg_type, chr(9), chr(32))) msg_type,
                upper(translate (outbound_id, chr(9), chr(32))) outbound_id,
                upper(translate (substr(sending_florist,1,7), chr(9), chr(32))) sending_florist,
                upper(translate (filling_florist, chr(9), chr(32))) filling_florist,
                order_date,
                upper(translate (recipient, chr(9), chr(32))) recipient,
                upper(translate (address, chr(9), chr(32))) address,
                upper(translate (city_state_zip, chr(9), chr(32))) city_state_zip,
                upper(translate (phone_number, chr(9), chr(32))) phone_number,
                delivery_date,
                upper(translate (delivery_date_text, chr(9), chr(32))) delivery_date_text,
                upper(translate (first_choice, chr(9), chr(32))) first_choice,
                upper(translate (second_choice, chr(9), chr(32))) second_choice,
                price,
                upper(translate (card_message, chr(9), chr(32))) card_message,
                upper(translate (occasion, chr(9), chr(32))) occasion,
                upper(translate (special_instructions, chr(9), chr(32))) special_instructions,
                upper(translate (priority, chr(9), chr(32))) priority,
                upper(translate (operator, chr(9), chr(32))) operator,
                NVL(upper(translate (comments, chr(9), chr(32))), ''NONE'') comments,
                upper(translate (sak_text, chr(9), chr(32))) sak_text,
                ctseq,
                crseq,
                transmission_time,
                upper(translate (reference_number, chr(9), chr(32))) reference_number,
                upper(translate (product_id, chr(9), chr(32))) product_id,
                upper(translate (zip_code, chr(9), chr(32))) zip_code,
                ask_answer_code,
                upper(translate (sort_value, chr(9), chr(32))) sort_value,
                retrieval_flag,
                upper(translate (combined_report_number, chr(9), chr(32))) combined_report_number,
                upper(translate (rof_number, chr(9), chr(32))) rof_number,
                upper(translate (adj_reason_code, chr(9), chr(32))) adj_reason_code,
                over_under_charge,
                upper(translate (from_message_number, chr(9), chr(32))) from_message_number,
                upper(translate (from_message_date, chr(9), chr(32))) from_message_date,
                upper(translate (to_message_number, chr(9), chr(32))) to_message_number,
                upper(translate (to_message_date, chr(9), chr(32))) to_message_date
        FROM    mercury
        WHERE    ' || v_list || ' ORDER BY sort_value';

OPEN OUT_CUR FOR v_sql;
--OPEN OUT_CUR FOR  SELECT V_SQL AS DEBUG_MODE FROM DUAL;

END VIEW_MERCURY_FOR_OUTBOUND;


PROCEDURE VIEW_MERCURY_OPS
(
IN_SUFFIX                       IN MERCURY_OPS.SUFFIX%TYPE,
IN_AVAILABLE_FLAG               IN MERCURY_OPS.AVAILABLE_FLAG%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury ops information
        for the given suffix and/or the given available_flag.

Input:
        suffix                          varchar2

Output:
        cursor result set containing all fields in mercury ops
        suffix                          varchar2
        in_use_flag                     char
        last_transmission               date
        last_message_activity           date
        available_flag                  char
        send_new_orders                 char
        sleep_time                      number
        batch_size                      number
        ack_ask                         char
        ack_count                       number
        resend_batch                    char

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  suffix,
                in_use_flag,
                last_transmission,
                last_message_activity,
                available_flag,
                send_new_orders,
                sleep_time,
                batch_size,
                ack_ask,
                ack_count,
                resend_batch
        FROM    mercury_ops
        WHERE   (suffix = in_suffix OR in_suffix is null)
        AND     (available_flag = in_available_flag OR in_available_flag is null);

END VIEW_MERCURY_OPS;

PROCEDURE VIEW_BOXX_DASHBOARD_COUNTS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury ops dashboard
        information for all suffixes.

Output:
        cursor result set containing all fields in mercury ops and the count
        of queued transmission for each suffix
        suffix                          varchar2
        in_use_flag                     char
        available_flag                  char
        send_new_orders                 char
        sleep_time                      number
        batch_size                      number
        last_transmission               date
        last_message_activity           date
        queued                          number

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  a.suffix,
                a.in_use_flag,
                a.available_flag,
                a.send_new_orders,
                a.sleep_time,
                a.batch_size,
                a.last_transmission,
                a.last_message_activity,
                (select count (1) from mercury b where b.outbound_id = a.suffix and b.mercury_status = 'MQ') queued
        FROM    mercury_ops a
        ORDER BY a.suffix;

END VIEW_BOXX_DASHBOARD_COUNTS;


PROCEDURE VIEW_PENDING_MERCURY_SUMMARY
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning pending mercury counts.

Output:
        cursor result set
        outbound_id                     varchar2
        open_total                      number
        oldest_time                     date

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  decode (outbound_id, null, 'Open', outbound_id) outbound_id,
                count (1) open_total,
                min (transmission_time) oldest_time
        FROM    mercury
        WHERE   mercury_status = 'MO'
        GROUP BY decode (outbound_id, null, 'Open', outbound_id)
        ORDER BY decode (outbound_id, 'Open', '0', outbound_id);

END VIEW_PENDING_MERCURY_SUMMARY;


PROCEDURE VIEW_PENDING_MERCURY_DETAIL
(
IN_MERCURY_STATUS               IN MERCURY.MERCURY_STATUS%TYPE,
IN_OUTBOUND_ID                  IN MERCURY.OUTBOUND_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning pending mercury counts.

Input:
        mercury_status                  varchar2
        outbound_id                     varchar2

Output:
        cursor result set
        outbound_id                     varchar2
        open_total                      number
        oldest_time                     date

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  mercury_id,
                sending_florist,
                transmission_time,
                sort_value
        FROM    mercury
        WHERE   mercury_status = in_mercury_status
        AND     (outbound_id = in_outbound_id
        OR      (outbound_id is null and in_outbound_id is null))
        ORDER BY sort_value;

END VIEW_PENDING_MERCURY_DETAIL;

PROCEDURE UPDATE_MERCURY_VIEW_QUEUE
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_VIEW_QUEUE                   IN MERCURY.VIEW_QUEUE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury view_queue.

Input:
        mercury_id                      varchar2
        view_queue                      char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN



UPDATE  mercury
SET     view_queue = nvl (in_view_queue, view_queue)
WHERE   mercury_id = in_mercury_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_VIEW_QUEUE;



PROCEDURE UPDATE_MERCURY_LOCK
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_CSR_ID                       IN  VARCHAR2,
OUT_LOCK_SUCCESS                OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
  LOCK RECORD FOR VIEW QUEUE

Input:
Input
   mercury_id
   csr_id
Output
  status
  message



-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN


UPDATE
mercury.mercury
set locked_on = NULL,
    locked_by = NULL
where locked_by = in_csr_id;

SELECT
CASE when (SYSDATE-nvl(locked_on,SYSDATE-1))< (2/1440) then 'N'
else 'Y'
end
INTO out_lock_success
FROM
mercury.mercury
WHERE
mercury_id = in_mercury_id;

IF out_lock_success = 'Y' THEN
        UPDATE  mercury
        SET     locked_by = in_csr_id,
                locked_on = SYSDATE
        WHERE
        mercury_id = in_mercury_id;
END IF;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block



END UPDATE_MERCURY_LOCK;


PROCEDURE UPDATE_MERCURY_MESSAGE
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_MERCURY_MESSAGE_NUMBER       IN MERCURY.MERCURY_MESSAGE_NUMBER%TYPE,
IN_MERCURY_ORDER_NUMBER         IN MERCURY.MERCURY_ORDER_NUMBER%TYPE,
IN_MERCURY_STATUS               IN MERCURY.MERCURY_STATUS%TYPE,
IN_MSG_TYPE                     IN MERCURY.MSG_TYPE%TYPE,
IN_OUTBOUND_ID                  IN MERCURY.OUTBOUND_ID%TYPE,
IN_SENDING_FLORIST              IN MERCURY.SENDING_FLORIST%TYPE,
IN_FILLING_FLORIST              IN MERCURY.FILLING_FLORIST%TYPE,
IN_ORDER_DATE                   IN MERCURY.ORDER_DATE%TYPE,
IN_RECIPIENT                    IN MERCURY.RECIPIENT%TYPE,
IN_ADDRESS                      IN MERCURY.ADDRESS%TYPE,
IN_CITY_STATE_ZIP               IN MERCURY.CITY_STATE_ZIP%TYPE,
IN_PHONE_NUMBER                 IN MERCURY.PHONE_NUMBER%TYPE,
IN_DELIVERY_DATE                IN MERCURY.DELIVERY_DATE%TYPE,
IN_DELIVERY_DATE_TEXT           IN MERCURY.DELIVERY_DATE_TEXT%TYPE,
IN_FIRST_CHOICE                 IN MERCURY.FIRST_CHOICE%TYPE,
IN_SECOND_CHOICE                IN MERCURY.SECOND_CHOICE%TYPE,
IN_PRICE                        IN MERCURY.PRICE%TYPE,
IN_CARD_MESSAGE                 IN MERCURY.CARD_MESSAGE%TYPE,
IN_OCCASION                     IN MERCURY.OCCASION%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN MERCURY.SPECIAL_INSTRUCTIONS%TYPE,
IN_PRIORITY                     IN MERCURY.PRIORITY%TYPE,
IN_OPERATOR                     IN MERCURY.OPERATOR%TYPE,
IN_COMMENTS                     IN MERCURY.COMMENTS%TYPE,
IN_SAK_TEXT                     IN MERCURY.SAK_TEXT%TYPE,
IN_CTSEQ                        IN MERCURY.CTSEQ%TYPE,
IN_CRSEQ                        IN MERCURY.CRSEQ%TYPE,
IN_TRANSMISSION_TIME            IN MERCURY.TRANSMISSION_TIME%TYPE,
IN_REFERENCE_NUMBER             IN MERCURY.REFERENCE_NUMBER%TYPE,
IN_PRODUCT_ID                   IN MERCURY.PRODUCT_ID%TYPE,
IN_ZIP_CODE                     IN MERCURY.ZIP_CODE%TYPE,
IN_ASK_ANSWER_CODE              IN MERCURY.ASK_ANSWER_CODE%TYPE,
IN_SORT_VALUE                   IN MERCURY.SORT_VALUE%TYPE,
IN_RETRIEVAL_FLAG               IN MERCURY.RETRIEVAL_FLAG%TYPE,
IN_COMBINED_REPORT_NUMBER       IN MERCURY.COMBINED_REPORT_NUMBER%TYPE,
IN_ROF_NUMBER                   IN MERCURY.ROF_NUMBER%TYPE,
IN_ADJ_REASON_CODE              IN MERCURY.ADJ_REASON_CODE%TYPE,
IN_OVER_UNDER_CHARGE            IN MERCURY.OVER_UNDER_CHARGE%TYPE,
IN_FROM_MESSAGE_NUMBER          IN MERCURY.FROM_MESSAGE_NUMBER%TYPE,
IN_FROM_MESSAGE_DATE            IN MERCURY.FROM_MESSAGE_DATE%TYPE,
IN_TO_MESSAGE_NUMBER            IN MERCURY.TO_MESSAGE_NUMBER%TYPE,
IN_TO_MESSAGE_DATE              IN MERCURY.TO_MESSAGE_DATE%TYPE,
IN_VIEW_QUEUE                   IN MERCURY.VIEW_QUEUE%TYPE,
IN_MESSAGE_DIRECTION            IN MERCURY.MESSAGE_DIRECTION%TYPE,
IN_COMP_ORDER                   IN MERCURY.COMP_ORDER%TYPE,
IN_REQUIRE_CONFIRMATION         IN MERCURY.REQUIRE_CONFIRMATION%TYPE,
IN_SUFFIX                       IN MERCURY.SUFFIX%TYPE,
IN_ORDER_SEQ                    IN MERCURY.ORDER_SEQ%TYPE,
IN_ADMIN_SEQ                    IN MERCURY.ADMIN_SEQ%TYPE,
IN_OLD_PRICE                    IN MERCURY.OLD_PRICE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury information.

Input:
        mercury_id                      varchar2
        mercury_message_number          varchar2
        mercury_order_number            varchar2
        mercury_status                  varchar2
        msg_type                        varchar2
        outbound_id                     varchar2
        sending_florist                 varchar2
        filling_florist                 varchar2
        order_date                      date
        recipient                       varchar2
        address                         varchar2
        city_state_zip                  varchar2
        phone_number                    varchar2
        delivery_date                   date
        delivery_date_text              varchar2
        first_choice                    varchar2
        second_choice                   varchar2
        price                           number
        card_message                    varchar2
        occasion                        varchar2
        special_instructions            varchar2
        priority                        varchar2
        operator                        varchar2
        comments                        varchar2
        sak_text                        varchar2
        ctseq                           number
        crseq                           number
        transmission_time               date
        reference_number                varchar2
        product_id                      varchar2
        zip_code                        varchar2
        ask_answer_code                 char
        sort_value                      varchar2
        retrieval_flag                  char
        combined_report_number          varchar2
        rof_number                      varchar2
        adj_reason_code                 varchar2
        over_under_charge               number
        from_message_number             varchar2
        from_message_date               varchar2
        to_message_number               varchar2
        to_message_date                 varchar2
        view_queue                      char
        message_direction               varchar2
        comp_order                      char
        require_confirmation            char
        suffix                          varchar2
        order_seq                       varchar2
        admin_seq                       varchar2
        old_price                       number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE mercury
     SET mercury_message_number = in_mercury_message_number,
         mercury_order_number   = in_mercury_order_number,
         mercury_status         = in_mercury_status,
         msg_type               = in_msg_type,
         outbound_id            = in_outbound_id,
         sending_florist        = in_sending_florist,
         filling_florist        = in_filling_florist,
         order_date             = in_order_date,
         recipient              = in_recipient,
         address                = in_address,
         city_state_zip         = in_city_state_zip,
         phone_number           = in_phone_number,
         delivery_date          = in_delivery_date,
         delivery_date_text     = in_delivery_date_text,
         first_choice           = in_first_choice,
         second_choice          = in_second_choice,
         price                  = in_price,
         card_message           = in_card_message,
         occasion               = in_occasion,
         special_instructions   = in_special_instructions,
         priority               = in_priority,
         operator               = in_operator,
         comments               = in_comments,
         sak_text               = in_sak_text,
         ctseq                  = in_ctseq,
         crseq                  = in_crseq,
         transmission_time      = in_transmission_time,
         reference_number       = in_reference_number,
         product_id             = in_product_id,
         zip_code               = in_zip_code,
         ask_answer_code        = in_ask_answer_code,
         sort_value             = in_sort_value,
         retrieval_flag         = in_retrieval_flag,
         combined_report_number = in_combined_report_number,
         rof_number             = in_rof_number,
         adj_reason_code        = in_adj_reason_code,
         over_under_charge      = in_over_under_charge,
         from_message_number    = in_from_message_number,
         from_message_date      = in_from_message_date,
         to_message_number      = in_to_message_number,
         to_message_date        = in_to_message_date,
         view_queue             = in_view_queue,
         message_direction      = in_message_direction,
         comp_order             = in_comp_order,
         require_confirmation   = in_require_confirmation,
         suffix                 = in_suffix,
         order_seq              = in_order_seq,
         admin_seq              = in_admin_seq,
         old_price              = in_old_price
   WHERE mercury_id = in_mercury_id;


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No records found for mercury_id ' || in_mercury_id ||'.';
  END IF;


  EXCEPTION WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END UPDATE_MERCURY_MESSAGE;


PROCEDURE GET_MERCURY_BY_ID
(
 IN_MERCURY_ID   IN MERCURY.MERCURY_ID%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a record from table mercury
         for the given mecury_id.

Input:
        mercury_id

Output:
        cursor containing mercury info
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
     SELECT mercury_id,
            mercury_message_number,
            mercury_order_number,
            mercury_status,
            msg_type,
            outbound_id,
            sending_florist,
            filling_florist,
            order_date,
            recipient,
            address,
            city_state_zip,
            phone_number,
            delivery_date,
            delivery_date_text,
            first_choice,
            second_choice,
            price,
            card_message,
            occasion,
            special_instructions,
            priority,
            operator,
            comments,
            sak_text,
            ctseq,
            crseq,
            transmission_time,
            reference_number,
            product_id,
            zip_code,
            ask_answer_code,
            sort_value,
            retrieval_flag,
            combined_report_number,
            rof_number,
            adj_reason_code,
            over_under_charge,
            from_message_number,
            from_message_date,
            to_message_number,
            to_message_date,
            view_queue,
            message_direction,
            comp_order,
            require_confirmation,
            suffix,
            order_seq,
            admin_seq,
            old_price
       FROM mercury
      WHERE mercury_id = in_mercury_id;

END GET_MERCURY_BY_ID;


PROCEDURE GET_MERCURY_BY_MESSAGE_NUMBER
(
 IN_MERCURY_MESSAGE_NUMBER  IN MERCURY.MERCURY_MESSAGE_NUMBER%TYPE,
 OUT_CURSOR                OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a record from table mercury
         for the given mecury_message_number.

Input:
        mercury_message_number

Output:
        cursor containing mercury info
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
     SELECT mercury_id,
            mercury_message_number,
            mercury_order_number,
            mercury_status,
            msg_type,
            outbound_id,
            sending_florist,
            filling_florist,
            order_date,
            recipient,
            address,
            city_state_zip,
            phone_number,
            delivery_date,
            delivery_date_text,
            first_choice,
            second_choice,
            price,
            card_message,
            occasion,
            special_instructions,
            priority,
            operator,
            comments,
            sak_text,
            ctseq,
            crseq,
            transmission_time,
            reference_number,
            product_id,
            zip_code,
            ask_answer_code,
            sort_value,
            retrieval_flag,
            combined_report_number,
            rof_number,
            adj_reason_code,
            over_under_charge,
            from_message_number,
            from_message_date,
            to_message_number,
            to_message_date,
            view_queue,
            message_direction,
            comp_order,
            require_confirmation,
            suffix,
            order_seq,
            admin_seq,
            old_price
       FROM mercury
      WHERE mercury_message_number = in_mercury_message_number
        AND msg_type = 'FTD'
        AND created_on =
                (select max(created_on)
                from mercury
                where mercury_message_number = in_mercury_message_number
                and msg_type = 'FTD');

END GET_MERCURY_BY_MESSAGE_NUMBER;

PROCEDURE GET_MERCURY_BY_MSG_N_REF_NOS
(
 IN_MERCURY_MESSAGE_NUMBER  IN MERCURY.MERCURY_MESSAGE_NUMBER%TYPE,
 IN_REFERENCE_NUMBER  		IN MERCURY.REFERENCE_NUMBER%TYPE,
 OUT_CURSOR                OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a record from table mercury
         for the given mecury_message_number and reference_number.

Input:
        mercury_message_number
		reference_number

Output:
        cursor containing mercury info
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
     SELECT mercury_id,
            mercury_message_number,
            mercury_order_number,
            mercury_status,
            msg_type,
            outbound_id,
            sending_florist,
            filling_florist,
            order_date,
            recipient,
            address,
            city_state_zip,
            phone_number,
            delivery_date,
            delivery_date_text,
            first_choice,
            second_choice,
            price,
            card_message,
            occasion,
            special_instructions,
            priority,
            operator,
            comments,
            sak_text,
            ctseq,
            crseq,
            transmission_time,
            reference_number,
            product_id,
            zip_code,
            ask_answer_code,
            sort_value,
            retrieval_flag,
            combined_report_number,
            rof_number,
            adj_reason_code,
            over_under_charge,
            from_message_number,
            from_message_date,
            to_message_number,
            to_message_date,
            view_queue,
            message_direction,
            comp_order,
            require_confirmation,
            suffix,
            order_seq,
            admin_seq,
            old_price
       FROM mercury
      WHERE mercury_message_number = in_mercury_message_number
		AND reference_number = in_reference_number
        AND msg_type = 'FTD'
        AND created_on =
                (select max(created_on)
                from mercury
                where mercury_message_number = in_mercury_message_number
				and reference_number = in_reference_number
                and msg_type = 'FTD');

END GET_MERCURY_BY_MSG_N_REF_NOS;

PROCEDURE GET_MERCURY_OPS
(
 IN_SUFFIX   IN MERCURY_OPS.SUFFIX%TYPE,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a record from table mercury_ops
         for the given suffix value passed in.

Input:
        in_suffix

Output:
        cursor containing mercury_ops info
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CURSOR FOR
     SELECT suffix,
            in_use_flag,
            last_transmission,
            last_message_activity,
            available_flag,
            send_new_orders,
            sleep_time,
            batch_size,
            inbound_admin_sequence,
            outbound_admin_sequence,
            inbound_order_sequence,
            outbound_order_sequence,
            sequence_check_lock
       FROM mercury_ops
      WHERE suffix = in_suffix;

END GET_MERCURY_OPS;


PROCEDURE GET_MERCURY_SEQUENCE_BUCKET
(
 IN_MESSAGE_DIRECTION IN MERCURY.MESSAGE_DIRECTION%TYPE,
 IN_SUFFIX            IN MERCURY.SUFFIX%TYPE,
 IN_ADMIN_FLAG        IN CHAR,
 OUT_CURSOR           OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a list of mercury ids and admin_seq or order_seq
         for the given message direction and suffix and depending on the flag
         value passed in.

Input:
        message direction, suffix, flag

Output:
        cursor containing mercury info
-----------------------------------------------------------------------------*/

BEGIN


  IF in_admin_flag = 'Y' THEN

    OPEN out_cursor FOR
     SELECT m.mercury_id,
            m.admin_seq
       FROM mercury m,
            mercury_hp mhp
      WHERE m.mercury_id = mhp.mercury_id
        AND mhp.status = 'I'
        AND m.suffix = in_suffix
        AND m.message_direction = in_message_direction
        AND m.admin_seq IS NOT NULL
      ORDER BY admin_seq ASC;

  ELSE

    OPEN out_cursor FOR
     SELECT m.mercury_id,
            m.order_seq
       FROM mercury m,
            mercury_hp mhp
      WHERE m.mercury_id = mhp.mercury_id
        AND mhp.status = 'I'
        AND m.suffix = in_suffix
        AND m.message_direction = in_message_direction
        AND m.order_seq IS NOT NULL
      ORDER BY order_seq ASC;

  END IF;

END GET_MERCURY_SEQUENCE_BUCKET;


PROCEDURE GET_INBOUND_MERCURY_MESSAGES
(
 IN_SUFFIX            IN MERCURY.SUFFIX%TYPE,
 OUT_CURSOR           OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a list of records from the mercury
         table for the given suffix value passed in.

Input:
        suffix

Output:
        cursor containing mercury info
-----------------------------------------------------------------------------*/

BEGIN


  OPEN out_cursor FOR
     SELECT m.mercury_id,
            m.mercury_message_number,
            m.mercury_order_number,
            m.mercury_status,
            m.msg_type,
            m.outbound_id,
            m.sending_florist,
            m.filling_florist,
            m.order_date,
            m.recipient,
            m.address,
            m.city_state_zip,
            m.phone_number,
            m.delivery_date,
            m.delivery_date_text,
            m.first_choice,
            m.second_choice,
            m.price,
            m.card_message,
            m.occasion,
            m.special_instructions,
            m.priority,
            m.operator,
            m.comments,
            m.sak_text,
            m.ctseq,
            m.crseq,
            m.transmission_time,
            m.reference_number,
            m.product_id,
            m.zip_code,
            m.ask_answer_code,
            m.sort_value,
            m.retrieval_flag,
            m.combined_report_number,
            m.rof_number,
            m.adj_reason_code,
            m.over_under_charge,
            m.from_message_number,
            m.from_message_date,
            m.to_message_number,
            m.to_message_date,
            m.view_queue,
            m.message_direction,
            m.comp_order,
            m.require_confirmation,
            m.suffix,
            m.order_seq,
            m.admin_seq,
            m.old_price
       FROM mercury m,
            mercury_hp mhp
      WHERE m.mercury_id = mhp.mercury_id
        AND mhp.status = 'I'
        AND m.suffix = in_suffix;

END GET_INBOUND_MERCURY_MESSAGES;


PROCEDURE MRK_INBND_MRCRY_HP_FR_PRCSSNG
(
 IN_SUFFIX     IN MERCURY.SUFFIX%TYPE,
 OUT_STATUS   OUT VARCHAR2,
 OUT_MESSAGE  OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This updates the status of records in the mercury_hp
         table for the given suffix value passed in.

Input:
        in_suffix

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE mercury_hp
     SET status = 'I'
   WHERE status = 'N'
     AND mercury_id in (SELECT mercury_id
                          FROM mercury
                         WHERE suffix = in_suffix);

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No "new" records found for suffix ' || in_suffix ||'.';
  END IF;


  EXCEPTION WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END MRK_INBND_MRCRY_HP_FR_PRCSSNG;


PROCEDURE UNMRK_INBND_MRCRY_HP_FR_PRCSS
(
 IN_SUFFIX     IN MERCURY.SUFFIX%TYPE,
 OUT_STATUS   OUT VARCHAR2,
 OUT_MESSAGE  OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This updates the status and processor fields of records in the
         mercury table for the given suffix value passed in.

Input:
        in_suffix

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE mercury_hp
     SET status = 'P'
   WHERE status = 'I'
     AND mercury_id in (SELECT mercury_id
                          FROM mercury
                         WHERE suffix = in_suffix);


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No "in process" records found for suffix ' || in_suffix ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END UNMRK_INBND_MRCRY_HP_FR_PRCSS;


PROCEDURE DELETE_MERCURY_HP
(
 IN_MERCURY_ID IN MERCURY_HP.MERCURY_ID%TYPE,
 OUT_STATUS   OUT VARCHAR2,
 OUT_MESSAGE  OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from the
         mercury_hp table for the given mercury_id value passed in.

Input:
        in_mercury_id

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  DELETE FROM mercury_hp
        WHERE status = 'I'
          AND mercury_id = in_mercury_id;


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No "in process" records found for id ' || in_mercury_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END DELETE_MERCURY_HP;


FUNCTION GET_NEXT_MERCURY_ID_SEQUENCE
RETURN NUMBER
IS
/*------------------------------------------------------------------------------
Description:
   Returns the next mercury sequence number.
------------------------------------------------------------------------------*/
v_seq_number NUMBER;

BEGIN
   SELECT mercury_id_sq.NEXTVAL INTO v_seq_number FROM DUAL;
   RETURN v_seq_number;
END GET_NEXT_MERCURY_ID_SEQUENCE;


PROCEDURE INSERT_CALL_OUT_LOG
(
IN_MERCURY_ID                   IN CALL_OUT_LOG.MERCURY_ID%TYPE,
IN_SHOP_NAME                    IN CALL_OUT_LOG.SHOP_NAME%TYPE,
IN_SHOP_PHONE                   IN CALL_OUT_LOG.SHOP_PHONE%TYPE,
IN_SPOKE_TO                     IN CALL_OUT_LOG.SPOKE_TO%TYPE,
IN_CREATED_BY                   IN CALL_OUT_LOG.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the call out log

Input:
        mercury_id                      varchar2
        shopName						varchar2
        shopPhone						varchar2
        spoke_to                        varchar2
        created_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO call_out_log
(
        mercury_id,
        shop_name,
        shop_phone,
        spoke_to,
        created_on,
        created_by
)
VALUES
(
        in_mercury_id,
        in_shop_name,
        in_shop_phone,
        in_spoke_to,
        SYSDATE,
        in_created_by
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CALL_OUT_LOG;


FUNCTION GET_NEXT_MERCURY_MANUAL_SEQ
RETURN NUMBER
IS
/*------------------------------------------------------------------------------
  Description:
   Returns the next mercury manual sequence number.
------------------------------------------------------------------------------*/
 v_seq_number NUMBER;

BEGIN
   SELECT mercury_manual_seq.NEXTVAL INTO v_seq_number FROM DUAL;
   RETURN v_seq_number;
END GET_NEXT_MERCURY_MANUAL_SEQ;


PROCEDURE SET_MERCURY_MAN_RECONCILE_DATE
(
 IN_MERCURY_ID   IN MERCURY.MERCURY_ID%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This updates the manual_reconcile_date in the mercury table
        for the given mercury_id value passed in.

Input:
        in_mercury_id     VARCHAR2

Output:
        status            varchar2 (Y or N)
        message           varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE mercury
     SET manual_reconcile_date = SYSDATE
   WHERE mercury_id = in_mercury_id;


  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No mercury record found for mercury id ' || in_mercury_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END SET_MERCURY_MAN_RECONCILE_DATE;


PROCEDURE GET_FTD_MERC_IDS_BY_DLVRY_DATE
(
 IN_DELIVERY_DATE      IN MERCURY.DELIVERY_DATE%TYPE,
 OUT_CURSOR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns a list of mercury ids from the mercury
         table for the given delivery date passed in.

Input:
        suffix

Output:
        cursor containing mercury info
-----------------------------------------------------------------------------*/

BEGIN


  OPEN out_cursor FOR
     SELECT m1.mercury_id
       FROM mercury.mercury m1
      WHERE m1.manual_reconcile_date IS NULL
        AND TRUNC(m1.delivery_date) < TRUNC(in_delivery_date )
        AND m1.mercury_status IN ('MM', 'MN')
        AND m1.msg_type = 'FTD'
        AND NOT EXISTS (SELECT 1
                          FROM mercury.mercury m2
                         WHERE m2.mercury_order_number = m1.mercury_order_number
                           AND m2.msg_type = 'CAN')
        AND EXISTS (SELECT 1
                          FROM mercury.call_out_log co
                         WHERE co.mercury_id = m1.mercury_id);

END GET_FTD_MERC_IDS_BY_DLVRY_DATE;

PROCEDURE UPDATE_PRICE
(
IN_MERCURY_ID           IN MERCURY.MERCURY_ID%TYPE,
IN_PRICE                IN MERCURY.PRICE%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This updates the price fields for the given message.

Input:
        mercury_id      varchar
        price           number

Output:
        status            varchar2 (Y or N)
        message           varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  mercury
SET     price = in_price,
        old_price = price
WHERE   mercury_id = in_mercury_id;

EXCEPTION WHEN OTHERS THEN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PRICE;


PROCEDURE INSERT_MERCURY_FOL
(
 IN_MERCURY_ID      IN MERCURY_FOL.MERCURY_ID%TYPE,
 OUT_STATUS        OUT VARCHAR2,
 OUT_MESSAGE       OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts a record into table mercury_fol.

Input:
        mercury_id    varchar2

Output:
        status        varchar2 (Y or N)
        message       varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  INSERT INTO mercury_fol
  (
        mercury_id,
        created_on
  )
  VALUES
  (
        in_mercury_id,
        SYSDATE
  );

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_MERCURY_FOL;


PROCEDURE VIEW_UNSAKED_MSGS
(
IN_OUTBOUND_ID                  IN MERCURY.OUTBOUND_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury records for the
        given outbound id in a status of 'MQ'

Input:
        mercury_status                  varchar2

Output:
        cursor result set containing all fields in mercury
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                upper(translate (mercury_id, chr(9), chr(32))) mercury_id,
                upper(translate (mercury_message_number, chr(9), chr(32))) mercury_message_number,
                upper(translate (mercury_order_number, chr(9), chr(32))) mercury_order_number,
                upper(translate (mercury_status, chr(9), chr(32))) mercury_status,
                upper(translate (msg_type, chr(9), chr(32))) msg_type,
                upper(translate (outbound_id, chr(9), chr(32))) outbound_id,
                upper(translate (sending_florist, chr(9), chr(32))) sending_florist,
                upper(translate (filling_florist, chr(9), chr(32))) filling_florist,
                order_date,
                upper(translate (recipient, chr(9), chr(32))) recipient,
                upper(translate (address, chr(9), chr(32))) address,
                upper(translate (city_state_zip, chr(9), chr(32))) city_state_zip,
                upper(translate (phone_number, chr(9), chr(32))) phone_number,
                delivery_date,
                upper(translate (delivery_date_text, chr(9), chr(32))) delivery_date_text,
                upper(translate (first_choice, chr(9), chr(32))) first_choice,
                upper(translate (second_choice, chr(9), chr(32))) second_choice,
                price,
                upper(translate (card_message, chr(9), chr(32))) card_message,
                upper(translate (occasion, chr(9), chr(32))) occasion,
                upper(translate (special_instructions, chr(9), chr(32))) special_instructions,
                upper(translate (priority, chr(9), chr(32))) priority,
                upper(translate (operator, chr(9), chr(32))) operator,
                upper(translate (comments, chr(9), chr(32))) comments,
                upper(translate (sak_text, chr(9), chr(32))) sak_text,
                ctseq,
                crseq,
                transmission_time,
                upper(translate (reference_number, chr(9), chr(32))) reference_number,
                upper(translate (product_id, chr(9), chr(32))) product_id,
                upper(translate (zip_code, chr(9), chr(32))) zip_code,
                ask_answer_code,
                upper(translate (sort_value, chr(9), chr(32))) sort_value,
                retrieval_flag,
                upper(translate (combined_report_number, chr(9), chr(32))) combined_report_number,
                upper(translate (rof_number, chr(9), chr(32))) rof_number,
                upper(translate (adj_reason_code, chr(9), chr(32))) adj_reason_code,
                over_under_charge,
                upper(translate (from_message_number, chr(9), chr(32))) from_message_number,
                upper(translate (from_message_date, chr(9), chr(32))) from_message_date,
                upper(translate (to_message_number, chr(9), chr(32))) to_message_number,
                upper(translate (to_message_date, chr(9), chr(32))) to_message_date
        FROM    mercury
        WHERE   OUTBOUND_ID = IN_OUTBOUND_ID AND MERCURY_STATUS='MQ'
        ORDER BY sort_value;
END VIEW_UNSAKED_MSGS;

PROCEDURE VIEW_LAST_BATCH
(
IN_SUFFIX                       IN MERCURY_BATCH.SUFFIX%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury_batch records for
        the given suffix

Input:
        mercury_status                  varchar2

Output:
        cursor result set containing all fields in mercury_batch
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
              SUFFIX,
              LAST_SENT1,
              LAST_SENT2,
              LAST_SENT3
        FROM  MERCURY_BATCH b
        WHERE SUFFIX = IN_SUFFIX;
END VIEW_LAST_BATCH;


PROCEDURE UPDATE_LAST_BATCH
(
IN_SUFFIX                       IN MERCURY_BATCH.SUFFIX%TYPE,
IN_BATCH_SENT1                  IN MERCURY_BATCH.LAST_SENT1%TYPE,
IN_BATCH_SENT2                  IN MERCURY_BATCH.LAST_SENT2%TYPE,
IN_BATCH_SENT3                  IN MERCURY_BATCH.LAST_SENT3%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury_batch records to
        the given values.

Input:
        mercury_status                  varchar2
        batch_sent                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/
BEGIN
        UPDATE MERCURY_BATCH SET
            LAST_SENT1 = IN_BATCH_SENT1,
            LAST_SENT2 = IN_BATCH_SENT2,
            LAST_SENT3 = IN_BATCH_SENT3
        WHERE SUFFIX = IN_SUFFIX;

        IF SQL%NOTFOUND then
            INSERT INTO MERCURY_BATCH
                (SUFFIX, LAST_SENT1, LAST_SENT2, LAST_SENT3)
                VALUES
                (IN_SUFFIX, IN_BATCH_SENT1, IN_BATCH_SENT2, IN_BATCH_SENT3);
        END IF;

        out_status := 'Y';
        out_message := null;

        commit;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END UPDATE_LAST_BATCH;

PROCEDURE MONITOR_VIEW_QUEUE (
  IN_MAX_SIZE    IN NUMBER,
  OUT_STATUS    OUT VARCHAR2,
  OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Checks to see if the size of the view queue is greater then the passed
        in limit.  Return 'N' if it's greater than the limit or 'Y'.

Input:
        max_size                        number
        batch_sent                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/
  v_queue_size number;
BEGIN

  SELECT COUNT(*) 
    INTO v_queue_size 
    FROM MERCURY.MERCURY 
    WHERE VIEW_QUEUE = 'Y';
    
  IF v_queue_size > IN_MAX_SIZE THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'The size of the view queue is ' || to_char(v_queue_size) || '.  Maximum is set to '|| to_char(IN_MAX_SIZE) || '.';
  ELSE
    OUT_STATUS := 'Y';
  END IF;
EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block
END MONITOR_VIEW_QUEUE;

PROCEDURE UPDATE_VIEW_QUEUE_FLAG
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_VIEW_QUEUE			IN MERCURY.VIEW_QUEUE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury VIEW_QUEUE.

Input:
        mercury_id                      varchar2
        view_queue          		varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

        UPDATE  mercury
        SET     view_queue = nvl (in_view_queue, view_queue)
        WHERE   mercury_id = in_mercury_id;

        OUT_STATUS := 'Y';


EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_VIEW_QUEUE_FLAG;

PROCEDURE INSERT_LIFECYCLE_STATUS
(
 IN_EFOS_ORDER_NUMBER    IN VARCHAR2,
 IN_STATUS_CODE          IN VARCHAR2,
 IN_STATUS_TIMESTAMP     IN DATE,
 IN_STATUS_TXT           IN VARCHAR2,
 IN_STATUS_URL           IN VARCHAR2,
 OUT_STATUS              OUT VARCHAR2,
 OUT_MESSAGE             OUT VARCHAR2,
 OUT_MERCURY_ID          OUT VARCHAR2,
 OUT_ORDER_DETAIL_ID     OUT VARCHAR2,
 OUT_DCON_STATUS         OUT VARCHAR2
)
AS

v_mercury_id varchar2(100);
v_order_detail_id varchar2(100);
v_delivery_confirmation_status varchar2(100);
v_exists varchar2(1) := 'N';

CURSOR merc_cur IS
    SELECT m.mercury_id, od.delivery_confirmation_status, m.reference_number
    FROM mercury.mercury m, clean.order_details od
    WHERE m.mercury_order_number like in_efos_order_number || '%'
    AND m.msg_type = 'FTD'
    AND m.created_on =
        (select max(m1.created_on)
        from mercury.mercury m1
        where m1.mercury_order_number like in_efos_order_number || '%'
        and m1.msg_type = 'FTD')
    AND od.order_detail_id = to_number(m.reference_number);

CURSOR lifecycle_cur (p_mercury_id varchar2) IS
    SELECT 'Y'
    FROM lifecycle_status
    WHERE mercury_id = p_mercury_id
    AND order_status_code = in_status_code;

BEGIN

    OPEN merc_cur;
    FETCH merc_cur INTO v_mercury_id, v_delivery_confirmation_status, v_order_detail_id;

    IF merc_cur%found THEN

        OPEN lifecycle_cur (v_mercury_id);
        FETCH lifecycle_cur into v_exists;

        IF lifecycle_cur%notfound THEN

            BEGIN

                INSERT INTO LIFECYCLE_STATUS
                (mercury_id, order_status_code, status_timestamp, status_txt, status_url, created_on, created_by, updated_on, updated_by)
                VALUES
                (v_mercury_id, in_status_code, in_status_timestamp, in_status_txt, in_status_url, sysdate, 'SYS', sysdate, 'SYS');

            EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
                Null;

            END;

        END IF;

        CLOSE lifecycle_cur;

    END IF;

    CLOSE merc_cur;

    out_status := 'Y';
    out_mercury_id := v_mercury_id;
    out_order_detail_id := v_order_detail_id;
    out_dcon_status := v_delivery_confirmation_status;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_LIFECYCLE_STATUS;

PROCEDURE UPDATE_FLORIST_SELECTION_ID
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_FLORIST_SELECTION_LOG_ID      IN MERCURY.FLORIST_SELECTION_LOG_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury florist_selection_log_id

Input:
        mercury_id                      varchar2
        florist_selection_log_id        varchar2
       
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN



UPDATE  mercury
SET     florist_selection_log_id = in_florist_selection_log_id
WHERE   mercury_id = in_mercury_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_FLORIST_SELECTION_ID;

PROCEDURE INSERT_MERCURYEAPI_NEW_MESSAGE
(
IN_MESSAGE_ID                   IN VARCHAR2,
IN_REFERENCE_NUMBER             IN VARCHAR2,
IN_MESSAGE_RECEIVED_DATE        IN DATE,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_STATUS                       IN VARCHAR2,
IN_MAIN_MEMBER_CODE             IN VARCHAR2,
IN_CREATED_ON                   IN DATE,
IN_UPDATED_ON                   IN DATE,
IN_CREATED_BY                   IN VARCHAR2,
IN_UPDATED_BY                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN


INSERT INTO mercury_eapi_msgs_staging
(
        message_id,
        reference_number,
        message_received_date,
        message_type,
        status,
        main_member_code,
        created_on,
        updated_on,       
        created_by,      
        updated_by   
)
VALUES
(
        IN_MESSAGE_ID,
        IN_REFERENCE_NUMBER,
        IN_MESSAGE_RECEIVED_DATE,
        IN_MESSAGE_TYPE,
        IN_STATUS,
        IN_MAIN_MEMBER_CODE,
        IN_CREATED_ON,
        IN_UPDATED_ON,
        IN_CREATED_BY,
        IN_UPDATED_BY
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MERCURYEAPI_NEW_MESSAGE;


PROCEDURE UPDATE_MERCURYEAPI_NEW_MESSAGE
(
IN_MESSAGE_ID                   IN VARCHAR2,
IN_STATUS                       IN VARCHAR2,
IN_REASON                       IN VARCHAR2,
IN_UPDATED_ON                   IN DATE,
IN_UPDATED_BY                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN


UPDATE mercury_eapi_msgs_staging
SET     status = nvl(IN_STATUS, status),
        reasons = IN_REASON,
        updated_on =IN_UPDATED_ON,      
        updated_by =IN_UPDATED_BY
where message_id = IN_MESSAGE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURYEAPI_NEW_MESSAGE;


PROCEDURE GET_INBOUND_EAPI_RCVD_MESSAGES
(
IN_MAIN_MEMBER_CODE         IN VARCHAR2,
IN_STATUS                   IN VARCHAR2,
OUT_CURSOR                  OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure returns a list of records from the mercury_eapi_msgs_staging
         table for the given main_member_code value passed in and status RECEIVED

Input:
        main_member_code and status

Output:
        cursor containing mercury new messages 
-----------------------------------------------------------------------------*/

BEGIN


  OPEN out_cursor FOR
     SELECT me.message_id,
            me.reference_number,
            me.message_received_date,
            me.message_type,
            me.main_member_code,
            me.status,
            me.created_on,
            me.updated_on,
            me.created_by,
            me.updated_by
       FROM mercury_eapi_msgs_staging me
      WHERE me.main_member_code = IN_MAIN_MEMBER_CODE
        AND me.status = IN_STATUS;
  

END GET_INBOUND_EAPI_RCVD_MESSAGES;


PROCEDURE GET_MERCURYEAPI_STAGE_MESSAGE
(
  IN_MESSAGE_ID        IN VARCHAR2,
  IN_STATUS            IN VARCHAR2,
  OUT_CURSOR           OUT TYPES.REF_CURSOR,
  OUT_STATUS           OUT VARCHAR2,
  OUT_MESSAGE          OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury stage messages for the
        given reference number. 

Input:
        reference_number        varchar2

Output:
        cursor result set containing all fields in mercury
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

    out_status := 'Y';
    out_message := null;

    OPEN OUT_CURSOR FOR
        SELECT
	    me.message_id,
            me.reference_number,
            me.message_received_date,
            me.message_type,
            me.main_member_code,
            me.status,
            me.created_on,
            me.updated_on,
            me.created_by,
            me.updated_by
       FROM mercury_eapi_msgs_staging me
       WHERE me.message_id = in_message_id
       AND me.status = in_status;

END GET_MERCURYEAPI_STAGE_MESSAGE;

PROCEDURE UPDATE_MERCURY_EAPI
(
IN_MERCURY_ID                   IN MERCURY.MERCURY_ID%TYPE,
IN_MERCURY_MESSAGE_NUMBER       IN MERCURY.MERCURY_MESSAGE_NUMBER%TYPE,
IN_MERCURY_STATUS               IN MERCURY.MERCURY_STATUS%TYPE,
IN_SAK_TEXT                     IN MERCURY.SAK_TEXT%TYPE,
IN_CTSEQ                        IN MERCURY.CTSEQ%TYPE,
IN_CRSEQ                        IN MERCURY.CRSEQ%TYPE,
IN_TRANSMISSION_TIME            IN MERCURY.TRANSMISSION_TIME%TYPE,
IN_MERCURY_ORDER_NUMBER         IN MERCURY.MERCURY_ORDER_NUMBER%TYPE,
IN_VIEW_QUEUE                   IN MERCURY.VIEW_QUEUE%TYPE,
IN_SUFFIX                       IN MERCURY.SUFFIX%TYPE,
IN_ORDER_SEQ                    IN MERCURY.ORDER_SEQ%TYPE,
IN_ADMIN_SEQ                    IN MERCURY.ADMIN_SEQ%TYPE,
IN_OUTBOUND_ID                  IN MERCURY.OUTBOUND_ID%TYPE,
IN_COMMENTS                     IN MERCURY.COMMENTS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating mercury information.

Input:
        mercury_id                      varchar2
        mercury_message_number          varchar2
        mercury_status                  varchar2
        sak_text                        varchar2
        ctseq                           number
        crseq                           number
        transmission_time               date
        mercury_order_number            varchar2
        view_queue                      char
        suffix                          varchar2
        order_seq                       varchar2
        admin_seq                       varchar2
        outbound_id                     varchar2
        comments                        varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

    UPDATE  mercury
    SET     mercury_message_number = in_mercury_message_number,
            mercury_order_number = nvl(in_mercury_order_number, mercury_order_number),
            mercury_status = in_mercury_status,
            transmission_time = in_transmission_time,
            sak_text = nvl(in_sak_text, sak_text),
            ctseq = in_ctseq,
            crseq = in_crseq,
            view_queue = nvl (in_view_queue, view_queue),
            responsible_florist = (filling_florist),
            outbound_id = in_outbound_id,
            suffix = nvl(in_suffix, in_outbound_id),
            order_seq = in_order_seq,
            admin_seq = in_admin_seq,
            comments = in_comments
    WHERE   mercury_id = in_mercury_id;

        OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_EAPI;

PROCEDURE EAPI_GET_MAIN_MEMBER_CODES
(
 IN_AVAILABLE    IN MERCURY_EAPI_OPS.IS_AVAILABLE%TYPE,
 IN_IN_USE       IN MERCURY_EAPI_OPS.IN_USE%TYPE,
 OUT_CUR         OUT TYPES.REF_CURSOR,
 OUT_STATUS      OUT VARCHAR2,
 OUT_MESSAGE     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the main member codes from
		mercury_eapi_ops table based on its availability.

Input:
        IN_AVAILABLE                  varchar2
        IN_IN_USE                     varchar2

Output:
        cursor result set containing all fields in mercury
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT  main_member_code,
                is_available,
                in_use
        FROM    mercury_eapi_ops
        WHERE   in_use = IN_IN_USE
        AND     is_available = IN_AVAILABLE;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END EAPI_GET_MAIN_MEMBER_CODES;

PROCEDURE GET_MERCURY_EAPI_OPS
(
 IN_MAIN_MEMBER_CODE            IN MERCURY_EAPI_OPS.MAIN_MEMBER_CODE%TYPE,
 OUT_CURSOR                     OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        select main_member_code,
               is_available,
               in_use,
               created_on,
               created_by,
               updated_on,
               updated_by
        from mercury_eapi_ops
        where main_member_code = IN_MAIN_MEMBER_CODE;

END GET_MERCURY_EAPI_OPS;

PROCEDURE UPDATE_MERCURY_EAPI_OPS
(
 IN_MAIN_MEMBER_CODE             IN MERCURY_EAPI_OPS.MAIN_MEMBER_CODE%TYPE,
 IN_IS_AVAILABLE                 IN MERCURY_EAPI_OPS.IS_AVAILABLE%TYPE,
 IN_IN_USE                       IN MERCURY_EAPI_OPS.IN_USE%TYPE,
 IN_UPDATED_BY                   IN MERCURY_EAPI_OPS.UPDATED_BY%TYPE,
 OUT_STATUS                      OUT VARCHAR2,
 OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Get all the available main member codes from eapi ops table.

Input:
        in_use                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

    UPDATE  mercury_eapi_ops
    SET     is_available = decode(in_is_available, null, is_available, in_is_available),
            in_use = decode(in_in_use, null, in_use, in_in_use),
            updated_on = sysdate,
            updated_by = in_updated_by
    WHERE   main_member_code = IN_MAIN_MEMBER_CODE;

    COMMIT;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END UPDATE_MERCURY_EAPI_OPS;

PROCEDURE GET_MERCURY_BY_ID_EAPI
(
 IN_MERCURY_ID   IN MERCURY.MERCURY_ID%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CURSOR FOR
        SELECT m.mercury_id,
            m.mercury_message_number,
            m.mercury_order_number,
            m.mercury_status,
            m.msg_type,
            m.outbound_id,
            m.sending_florist,
            m.filling_florist,
            m.order_date,
            m.recipient,
            m.address,
            m.city_state_zip,
            m.phone_number,
            m.delivery_date,
            m.delivery_date_text,
            m.first_choice,
            m.second_choice,
            m.price,
            m.card_message,
            m.occasion,
            m.special_instructions,
            m.priority,
            m.operator,
            m.comments,
            m.reference_number,
            m.product_id,
            m.zip_code,
            m.ask_answer_code,
            m.sort_value,
            m.combined_report_number,
            m.rof_number,
            m.adj_reason_code,
            m.over_under_charge,
            m.comp_order,
            fm.super_florist_flag,
            fm.allow_message_forwarding_flag,
            fs.suspend_type
        FROM mercury m
        LEFT OUTER JOIN ftd_apps.florist_master fm
        ON fm.florist_id = m.filling_florist
        LEFT OUTER JOIN ftd_apps.florist_suspends fs
        ON fs.florist_id = m.filling_florist
        WHERE mercury_id = in_mercury_id
        AND mercury_status = 'MO';

END GET_MERCURY_BY_ID_EAPI;

PROCEDURE VIEW_MERCURY_EAPI_OPS
(
IN_MAIN_MEMBER_CODE             IN MERCURY_EAPI_OPS.MAIN_MEMBER_CODE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Fetch the main member code config from MERCURY_EAPI_OPS table for a given main member code.

Input:
        main_member_code                          varchar2

Output:
        cursor result set containing all fields in mercury eapi ops
        main_member_code          varchar2
        in_use                    char
        is_available              char

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  main_member_code,
                in_use,
                is_available
        FROM    mercury_eapi_ops
        WHERE   main_member_code = IN_MAIN_MEMBER_CODE;

END VIEW_MERCURY_EAPI_OPS;

PROCEDURE UPDATE_MERCURY_EAPI_OPS_ADMIN
(
IN_MAIN_MEMBER_CODE             IN MERCURY_EAPI_OPS.MAIN_MEMBER_CODE%TYPE,
IN_IN_USE                       IN MERCURY_EAPI_OPS.IN_USE%TYPE,
IN_IS_AVAILABLE                 IN MERCURY_EAPI_OPS.IS_AVAILABLE%TYPE,
IN_UPDATED_BY                   IN MERCURY_EAPI_OPS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Updates the main member code config in MERCURY_EAPI_OPS table.

Input:
        main_member_code           varchar2
        in_use                     char
        is_available               char
        updated_on                 char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE  mercury_eapi_ops
SET     in_use = decode(in_in_use, null, in_use, in_in_use),
        is_available = decode(in_is_available,null,is_available, in_is_available),
        updated_on = sysdate,
        updated_by = in_updated_by
WHERE   main_member_code = in_main_member_code;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_EAPI_OPS_ADMIN;

PROCEDURE INSERT_MERCURY_EAPI_OPS_ADMIN
(
IN_MAIN_MEMBER_CODE             IN MERCURY_EAPI_OPS.MAIN_MEMBER_CODE%TYPE,
IN_IN_USE                       IN MERCURY_EAPI_OPS.IN_USE%TYPE,
IN_IS_AVAILABLE                 IN MERCURY_EAPI_OPS.IS_AVAILABLE%TYPE,
IN_CREATED_BY                   IN MERCURY_EAPI_OPS.CREATED_BY%TYPE,
IN_UPDATED_BY                   IN MERCURY_EAPI_OPS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Inserts a new main member code in MERCURY_EAPI_OPS table.

Input:
        main_memcber_code       varchar2
        in_use                     char
        is_available               char
        created_by                 date
        updated_by                 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT INTO MERCURY_EAPI_OPS (
    MAIN_MEMBER_CODE,
    IS_AVAILABLE,
    IN_USE,
    CREATED_BY,
    CREATED_ON,
    UPDATED_BY,
    UPDATED_ON)
VALUES (
    IN_MAIN_MEMBER_CODE,
    IN_IS_AVAILABLE,
    IN_IN_USE,
    IN_CREATED_BY,
    sysdate,
    IN_UPDATED_BY,
    sysdate);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MERCURY_EAPI_OPS_ADMIN;

PROCEDURE VIEW_EAPI_DASHBOARD_COUNTS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury eapi ops dashboard
        information for all main member codes.

Output:
        cursor result set containing all fields in mercury eapi ops for each main member code
        main_member_code           varchar2
        in_use                     char
        is_available               char

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  main_member_code,
                in_use,
                is_available,
                updated_on
        FROM    mercury_eapi_ops
        ORDER BY main_member_code;

END VIEW_EAPI_DASHBOARD_COUNTS;


FUNCTION IS_FTD_CANCELLED 
( 
IN_MERCURY_ID   IN MERCURY.MERCURY_ID%TYPE
) 
return varchar2 
 
AS 
 
/*----------------------------------------------------------------------------- 
Description: 
	  This FUNCTION is responsible to CHECK if the associated FTD has CAN on it
 
Input: 
        mercury_id                 mercury.mercury_id 
 
Output: 
        Y if the FTD has CAN on it
-----------------------------------------------------------------------------*/ 
v_cnt		    int := 0; 
out_flag    varchar2(1);
v_mercury_order_number varchar2(20);
v_query_str varchar2(5000);

BEGIN 

        
EXECUTE IMMEDIATE 'select count(*) from mercury.mercury m where 
      m.mercury_order_number = (select m1.mercury_order_number from mercury.mercury m1 where m1.mercury_id = '''||IN_MERCURY_ID||''')and m.msg_type = ''CAN''' INTO v_cnt;        
        
  
 IF v_cnt > 0 THEN 
  out_flag := 'Y'; 
 ELSE 
  out_flag := 'N'; 
 END IF; 
 
return out_flag;     
END IS_FTD_CANCELLED;


PROCEDURE GET_ASK_FOR_DCON_COUNT
(
IN_ORDER_DETAIL_ID        IN VARCHAR2,
IN_DCON_ASK_TIMEFRAME     IN NUMBER,
IN_DCON_ASK_TEXT          IN VARCHAR2,
OUT_COUNT                 OUT VARCHAR2,
OUT_STATUS                OUT VARCHAR2,
OUT_MESSAGE               OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        Gets the count of ASK messages that was sent for the given reference_number
        during the given timeframe.

Input:
        in_order_detail_id	       varchar2
        in_dcon_ask_timeframe      number
        in_dcon_ask_text           varchar2

Output:
		count							varchar2
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

msg_type varchar2(5) := 'ASK';
operator_key varchar2(5) := 'SYS';

BEGIN

  SELECT COUNT(*) INTO OUT_COUNT FROM MERCURY.MERCURY WHERE reference_number = IN_ORDER_DETAIL_ID AND 
          UPPER(COMMENTS) LIKE '%'||IN_DCON_ASK_TEXT||'%' AND MSG_TYPE = msg_type AND 
          OPERATOR LIKE '%'||operator_key AND CREATED_ON > (sysdate - (IN_DCON_ASK_TIMEFRAME/24));

  OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_ASK_FOR_DCON_COUNT;

PROCEDURE GET_KEY_PHRASE_LIST
(
IN_MSG_TYPE      IN VARCHAR2,
OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        Gets all the key phrases for the given msg_type.

Input:
        msg_type	       varchar2

Output:
		cursor							ref_cursor
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CURSOR FOR SELECT KEY_PHRASE_TXT FROM MERCURY.AUTO_RESPONSE_KEY_PHRASE WHERE MSG_TYPE = IN_MSG_TYPE;

END GET_KEY_PHRASE_LIST;


PROCEDURE INSERT_MERCURY_RULES_TRACKER
( 
IN_RULES_FIRED                  IN VARCHAR2,
IN_ACTION_TAKEN                 IN VARCHAR2,
IN_ACTION_DESCRIPTION           IN VARCHAR2,
IN_ACTION_STATUS                IN VARCHAR2, 
IN_RULE_TYPE                    IN VARCHAR2,
IN_MERCURY_ID                   IN VARCHAR2, 
IN_EXCEPTION_MESSAGE             IN VARCHAR2, 
IN_ORDER_DETAIL_ID              IN NUMBER,     
IN_ORDER_DATE                   IN DATE,     
IN_ORDER_ORIGIN                 IN VARCHAR2,
IN_SOURCE_CODE                  IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2, 
IN_DELIVERY_DATE                IN DATE,          
IN_PRODUCT_ID                   IN VARCHAR2, 
IN_MESSAGE_DIRECTION            IN VARCHAR2,  
IN_MESSAGE_TEXT                 IN VARCHAR2,
IN_SAK_TEXT                  IN VARCHAR2,
IN_CUTOFF                       IN NUMBER,  
IN_OCCASION                     IN VARCHAR2,
IN_DELIVERY_ZIP_CODE            IN VARCHAR2, 
IN_DELIVERY_STATE               IN CHAR,     
IN_DELIVERY_CITY                IN VARCHAR2,
IN_RECIPIENT_FIRST_NAME         IN VARCHAR2, 
IN_RECIPIENT_LAST_NAME          IN VARCHAR2, 
IN_RECIPIENT_PHONE_NUMBER       IN VARCHAR2,
IN_RECIPIENT_ADDRESS            IN VARCHAR2,
IN_RECIPIENT_ZIP_CODE           IN VARCHAR2, 
IN_RECIPIENT_CITY               IN VARCHAR2 ,
IN_RECIPIENT_STATE              IN VARCHAR2,
IN_RECIPIENT_COUNTRY            IN VARCHAR2, 
IN_CUSTOMER_FIRST_NAME          IN VARCHAR2, 
IN_CUSTOMER_LAST_NAME           IN VARCHAR2, 
IN_CUSTOMER_PHONE_NUMBER        IN VARCHAR2,
IN_CUSTOMER_ADDRESS             IN VARCHAR2,
IN_CUSTOMER_ZIP_CODE            IN VARCHAR2,
IN_CUSTOMER_CITY                IN VARCHAR2, 
IN_CUSTOMER_STATE               IN VARCHAR2,
IN_CUSTOMER_COUNTRY             IN VARCHAR2, 
IN_MERCURY_NUMBER               IN VARCHAR2,  
IN_ORIGINAL_MERCURY_PRICE       IN NUMBER,
IN_NEW_MERCURY_PRICE            IN NUMBER,
IN_TIMEZONE                     IN VARCHAR2, 
IN_PREFERRED_PARTNER_ID         IN VARCHAR2,  
IN_GLOBAL_REJECT_RETRY_LIMIT    IN NUMBER,        
IN_ORDER_BOUNCE_COUNT           IN NUMBER,        
IN_MARS_ORDER_BOUNCE_COUNT      IN NUMBER,        
IN_AUTO_RESP_OFF_BEFORE_CUTOFF  IN NUMBER,        
IN_FLORIST_SHOP_BLOCK_DAYS IN NUMBER,        
IN_ASKP_THRESHOLD               IN NUMBER,        
IN_KEY_PHRASES                  IN VARCHAR2,
IN_PHOENIX_ELIGIBLE_FLAG        IN CHAR,     
IN_CREATED_ON                   IN DATE,          
IN_CREATED_BY                   IN VARCHAR2,
IN_UPDATED_ON                   IN DATE,          
IN_UPDATED_BY                   IN VARCHAR2,
IN_EXTERNAL_ORDER_NUMBER        IN VARCHAR2,
IN_FLORIST_ID                   IN VARCHAR2,
IN_CODIFICATION_ID        		IN VARCHAR2,
IN_CODIFIED_MINIMUM             IN NUMBER,
OUT_STATUS                   OUT VARCHAR2,
OUT_MESSAGE                  OUT VARCHAR2
)

AS

v_ftd_count                   NUMBER;
v_mercury_order_number        VARCHAR(30);
v_order_live_flag             VARCHAR(2);


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting mercury information.

Input:
RULES_TRACKER_ID             IN NUMBER, 
RULES_FIRED                  IN VARCHAR2,
ACTION_TAKEN                 IN VARCHAR2,
ACTION_DESCRIPTION           IN VARCHAR2,
ACTION_STATUS                IN VARCHAR2, 
RULE_TYPE                    IN VARCHAR2,
MERCURY_ID                   IN VARCHAR2, 
EXCEPTION_MESAGE             IN VARCHAR2, 
ORDER_DETAIL_ID              IN NUMBER,     
ORDER_DATE                   IN DATE,     
ORDER_ORIGIN                 IN VARCHAR2,
SOURCE_CODE                  IN VARCHAR2,
MESSAGE_TYPE                 IN VARCHAR2, 
DELIVERY_DATE                IN DATE,          
PRODUCT_ID                   IN VARCHAR2, 
MESSAGE_DIRECTION            IN VARCHAR2,  
MESSAGE_TEXT                 IN VARCHAR2,
SAK_TEXT_ID                  IN VARCHAR2,
CUTOFF                       IN NUMBER,  
OCCASION                     IN VARCHAR2,
DELIVERY_ZIP_CODE            IN VARCHAR2, 
DELIVERY_STATE               IN CHAR,     
DELIVERY_CITY                IN VARCHAR2,
RECIPIENT_FIRST_NAME         IN VARCHAR2, 
RECIPIENT_LAST_NAME          IN VARCHAR2, 
RECIPIENT_PHONE_NUMBER       IN VARCHAR2,
RECIPIENT_ADDRESS            IN VARCHAR2,
RECIPIENT_ZIP_CODE           IN VARCHAR2, 
RECIPIENT_CITY               IN VARCHAR2 ,
RECIPIENT_STATE              IN VARCHAR2,
RECIPIENT_COUNTRY            IN VARCHAR2, 
CUSTOMER_FIRST_NAME          IN VARCHAR2, 
CUSTOMER_LAST_NAME           IN VARCHAR2, 
CUSTOMER_PHONE_NUMBER        IN VARCHAR2,
CUSTOMER_ADDRESS             IN VARCHAR2,
CUSTOMER_ZIP_CODE            IN VARCHAR2,
CUSTOMER_CITY                IN VARCHAR2, 
CUSTOMER_STATE               IN VARCHAR2,
CUSTOMER_COUNTRY             IN VARCHAR2, 
MERCURY_NUMBER               IN VARCHAR2,  
ORIGINAL_MERCURY_PRICE       IN NUMBER,
NEW_MERCURY_PRICE            IN NUMBER,
TIMEZONE                     IN VARCHAR2, 
PREFERRED_PARTNER_ID         IN VARCHAR2,  
GLOBAL_REJECT_RETRY_LIMIT    IN NUMBER,        
ORDER_BOUNCE_COUNT           IN NUMBER,        
MARS_ORDER_BOUNCE_COUNT      IN NUMBER,        
AUTO_RESP_OFF_BEFORE_CUTOFF  IN NUMBER,        
FLORIST_SHOP_SOFT_BLOCK_DAYS IN NUMBER,        
ASKP_THRESHOLD               IN NUMBER,        
KEY_PHRASES                  IN VARCHAR2,
PHOENIX_ELIGIBLE_FLAG        IN CHAR,     
CREATED_ON                   IN DATE,          
CREATED_BY                   IN VARCHAR2,
UPDATED_ON                   IN DATE,          
UPDATED_BY                   IN VARCHAR2,
EXTERNAL_ORDER_NUMBER        IN VARCHAR2,
FLORIST_ID                   IN VARCHAR2,
CODIFICATION_ID        		 IN VARCHAR2,
CODIFIED_MINIMUM             IN NUMBER,


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

SELECT COUNT(1) INTO v_ftd_count FROM MERCURY.MERCURY WHERE REFERENCE_NUMBER = TO_CHAR(IN_ORDER_DETAIL_ID) AND MSG_TYPE = 'FTD';

v_mercury_order_number := CLEAN.ORDER_MESG_PKG.GET_LAST_FTD_FOR_ORDER(IN_ORDER_DETAIL_ID);

select decode((select count(1) from mercury.mercury where mercury_order_number = v_mercury_order_number and msg_type in ('CAN','REJ') and mercury_status = 'MC'),'0','Y','N') into v_order_live_flag from dual;

INSERT INTO MERCURY_RULES_TRACKER
(
RULES_TRACKER_ID,
RULES_FIRED,
ACTION_TAKEN,
ACTION_DESCRIPTION,
ACTION_STATUS,
RULE_TYPE,
MERCURY_ID,
EXCEPTION_MESSAGE,
ORDER_DETAIL_ID,
ORDER_DATE,
ORDER_ORIGIN,
SOURCE_CODE,
MESSAGE_TYPE,
DELIVERY_DATE,
PRODUCT_ID,
MESSAGE_DIRECTION,
MESSAGE_TEXT,
SAK_TEXT,
CUTOFF,
OCCASION,
DELIVERY_ZIP_CODE,
DELIVERY_STATE,
DELIVERY_CITY,
RECIPIENT_FIRST_NAME,
RECIPIENT_LAST_NAME,
RECIPIENT_PHONE_NUMBER,
RECIPIENT_ADDRESS,
RECIPIENT_ZIP_CODE,
RECIPIENT_CITY,
RECIPIENT_STATE,
RECIPIENT_COUNTRY,
CUSTOMER_FIRST_NAME,
CUSTOMER_LAST_NAME,
CUSTOMER_PHONE_NUMBER,
CUSTOMER_ADDRESS,
CUSTOMER_ZIP_CODE,
CUSTOMER_CITY,
CUSTOMER_STATE,
CUSTOMER_COUNTRY,
MERCURY_NUMBER,
ORIGINAL_MERCURY_PRICE,
NEW_MERCURY_PRICE,
TIMEZONE,
PREFERRED_PARTNER_ID,
GLOBAL_REJECT_RETRY_LIMIT,
ORDER_BOUNCE_COUNT,
MARS_ORDER_BOUNCE_COUNT,
AUTO_RESP_OFF_BEFORE_CUTOFF,
FLORIST_SHOP_SOFT_BLOCK_DAYS,
ASKP_THRESHOLD,
KEY_PHRASES,
PHOENIX_ELIGIBLE_FLAG,
CREATED_ON,
CREATED_BY,
UPDATED_ON,
UPDATED_BY,
FTD_COUNT,
ORDER_LIVE_FLAG,
EXTERNAL_ORDER_NUMBER,
FLORIST_ID,
CODIFICATION_ID,
CODIFIED_MINIMUM
)
VALUES
(
RULES_TRACKER_ID_SQ.nextval,
IN_RULES_FIRED,
IN_ACTION_TAKEN,
IN_ACTION_DESCRIPTION,
IN_ACTION_STATUS,
IN_RULE_TYPE,
IN_MERCURY_ID,
IN_EXCEPTION_MESSAGE,
IN_ORDER_DETAIL_ID,
IN_ORDER_DATE,
IN_ORDER_ORIGIN,
IN_SOURCE_CODE,
IN_MESSAGE_TYPE,
IN_DELIVERY_DATE,
IN_PRODUCT_ID,
IN_MESSAGE_DIRECTION,
IN_MESSAGE_TEXT,
IN_SAK_TEXT,
IN_CUTOFF,
IN_OCCASION,
IN_DELIVERY_ZIP_CODE,
IN_DELIVERY_STATE,
IN_DELIVERY_CITY,
IN_RECIPIENT_FIRST_NAME,
IN_RECIPIENT_LAST_NAME,
IN_RECIPIENT_PHONE_NUMBER,
IN_RECIPIENT_ADDRESS,
IN_RECIPIENT_ZIP_CODE,
IN_RECIPIENT_CITY,
IN_RECIPIENT_STATE,
IN_RECIPIENT_COUNTRY,
IN_CUSTOMER_FIRST_NAME,
IN_CUSTOMER_LAST_NAME,
IN_CUSTOMER_PHONE_NUMBER,
IN_CUSTOMER_ADDRESS,
IN_CUSTOMER_ZIP_CODE,
IN_CUSTOMER_CITY,
IN_CUSTOMER_STATE,
IN_CUSTOMER_COUNTRY,
IN_MERCURY_NUMBER,
IN_ORIGINAL_MERCURY_PRICE,
IN_NEW_MERCURY_PRICE,
IN_TIMEZONE,
IN_PREFERRED_PARTNER_ID,
IN_GLOBAL_REJECT_RETRY_LIMIT,
IN_ORDER_BOUNCE_COUNT,
IN_MARS_ORDER_BOUNCE_COUNT,
IN_AUTO_RESP_OFF_BEFORE_CUTOFF,
IN_FLORIST_SHOP_BLOCK_DAYS,
IN_ASKP_THRESHOLD,
IN_KEY_PHRASES,
IN_PHOENIX_ELIGIBLE_FLAG,
sysdate,
IN_CREATED_BY,
sysdate,
IN_UPDATED_BY,
v_ftd_count,
v_order_live_flag,
IN_EXTERNAL_ORDER_NUMBER,
IN_FLORIST_ID,
IN_CODIFICATION_ID,
IN_CODIFIED_MINIMUM
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MERCURY_RULES_TRACKER;



PROCEDURE UPDATE_MERCURY_RULES_TRACKER
(
IN_RULES_TRACKER_ID             IN NUMBER, 
IN_RULES_FIRED                  IN VARCHAR2,
IN_ACTION_TAKEN                 IN VARCHAR2,
IN_ACTION_DESCRIPTION           IN VARCHAR2,
IN_ACTION_STATUS                IN VARCHAR2, 
IN_RULE_TYPE                    IN VARCHAR2,
IN_MERCURY_ID                   IN VARCHAR2, 
IN_EXCEPTION_MESSAGE             IN VARCHAR2, 
IN_ORDER_DETAIL_ID              IN NUMBER,     
IN_ORDER_DATE                   IN DATE,     
IN_ORDER_ORIGIN                 IN VARCHAR2,
IN_SOURCE_CODE                  IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2, 
IN_DELIVERY_DATE                IN DATE,          
IN_PRODUCT_ID                   IN VARCHAR2, 
IN_MESSAGE_DIRECTION            IN VARCHAR2,  
IN_MESSAGE_TEXT                 IN VARCHAR2,
IN_SAK_TEXT                  IN VARCHAR2,
IN_CUTOFF                       IN NUMBER,  
IN_OCCASION                     IN VARCHAR2,
IN_DELIVERY_ZIP_CODE            IN VARCHAR2, 
IN_DELIVERY_STATE               IN CHAR,     
IN_DELIVERY_CITY                IN VARCHAR2,
IN_RECIPIENT_FIRST_NAME         IN VARCHAR2, 
IN_RECIPIENT_LAST_NAME          IN VARCHAR2, 
IN_RECIPIENT_PHONE_NUMBER       IN VARCHAR2,
IN_RECIPIENT_ADDRESS            IN VARCHAR2,
IN_RECIPIENT_ZIP_CODE           IN VARCHAR2, 
IN_RECIPIENT_CITY               IN VARCHAR2 ,
IN_RECIPIENT_STATE              IN VARCHAR2,
IN_RECIPIENT_COUNTRY            IN VARCHAR2, 
IN_CUSTOMER_FIRST_NAME          IN VARCHAR2, 
IN_CUSTOMER_LAST_NAME           IN VARCHAR2, 
IN_CUSTOMER_PHONE_NUMBER        IN VARCHAR2,
IN_CUSTOMER_ADDRESS             IN VARCHAR2,
IN_CUSTOMER_ZIP_CODE            IN VARCHAR2,
IN_CUSTOMER_CITY                IN VARCHAR2, 
IN_CUSTOMER_STATE               IN VARCHAR2,
IN_CUSTOMER_COUNTRY             IN VARCHAR2, 
IN_MERCURY_NUMBER               IN VARCHAR2,  
IN_ORIGINAL_MERCURY_PRICE       IN NUMBER,
IN_NEW_MERCURY_PRICE            IN NUMBER,
IN_TIMEZONE                     IN VARCHAR2, 
IN_PREFERRED_PARTNER_ID         IN VARCHAR2,  
IN_GLOBAL_REJECT_RETRY_LIMIT    IN NUMBER,        
IN_ORDER_BOUNCE_COUNT           IN NUMBER,        
IN_MARS_ORDER_BOUNCE_COUNT      IN NUMBER,        
IN_AUTO_RESP_OFF_BEFORE_CUTOFF  IN NUMBER,        
IN_FLORIST_SHOP_BLOCK_DAYS IN NUMBER,        
IN_ASKP_THRESHOLD               IN NUMBER,        
IN_KEY_PHRASES                  IN VARCHAR2,
IN_PHOENIX_ELIGIBLE_FLAG        IN CHAR,     
IN_CREATED_ON                   IN DATE,          
IN_CREATED_BY                   IN VARCHAR2,
IN_UPDATED_ON                   IN DATE,          
IN_UPDATED_BY                   IN VARCHAR2,
IN_EXTERNAL_ORDER_NUMBER        IN VARCHAR2,
IN_FLORIST_ID                   IN VARCHAR2,
IN_CODIFICATION_ID        		IN VARCHAR2,
IN_CODIFIED_MINIMUM             IN NUMBER,
OUT_STATUS                   OUT VARCHAR2,
OUT_MESSAGE                  OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting mercury information.

Input:
RULES_TRACKER_ID             IN NUMBER, 
RULES_FIRED                  IN VARCHAR2,
ACTION_TAKEN                 IN VARCHAR2,
ACTION_DESCRIPTION           IN VARCHAR2,
ACTION_STATUS                IN VARCHAR2, 
RULE_TYPE                    IN VARCHAR2,
MERCURY_ID                   IN VARCHAR2, 
EXCEPTION_MESAGE             IN VARCHAR2, 
ORDER_DETAIL_ID              IN NUMBER,     
ORDER_DATE                   IN DATE,     
ORDER_ORIGIN                 IN VARCHAR2,
SOURCE_CODE                  IN VARCHAR2,
MESSAGE_TYPE                 IN VARCHAR2, 
DELIVERY_DATE                IN DATE,          
PRODUCT_ID                   IN VARCHAR2, 
MESSAGE_DIRECTION            IN VARCHAR2,  
MESSAGE_TEXT                 IN VARCHAR2,
SAK_TEXT_ID                  IN VARCHAR2,
CUTOFF                       IN NUMBER,  
OCCASION                     IN VARCHAR2,
DELIVERY_ZIP_CODE            IN VARCHAR2, 
DELIVERY_STATE               IN CHAR,     
DELIVERY_CITY                IN VARCHAR2,
RECIPIENT_FIRST_NAME         IN VARCHAR2, 
RECIPIENT_LAST_NAME          IN VARCHAR2, 
RECIPIENT_PHONE_NUMBER       IN VARCHAR2,
RECIPIENT_ADDRESS            IN VARCHAR2,
RECIPIENT_ZIP_CODE           IN VARCHAR2, 
RECIPIENT_CITY               IN VARCHAR2 ,
RECIPIENT_STATE              IN VARCHAR2,
RECIPIENT_COUNTRY            IN VARCHAR2, 
CUSTOMER_FIRST_NAME          IN VARCHAR2, 
CUSTOMER_LAST_NAME           IN VARCHAR2, 
CUSTOMER_PHONE_NUMBER        IN VARCHAR2,
CUSTOMER_ADDRESS             IN VARCHAR2,
CUSTOMER_ZIP_CODE            IN VARCHAR2,
CUSTOMER_CITY                IN VARCHAR2, 
CUSTOMER_STATE               IN VARCHAR2,
CUSTOMER_COUNTRY             IN VARCHAR2, 
MERCURY_NUMBER               IN VARCHAR2,  
ORIGINAL_MERCURY_PRICE       IN NUMBER,
NEW_MERCURY_PRICE            IN NUMBER,
TIMEZONE                     IN VARCHAR2, 
PREFERRED_PARTNER_ID         IN VARCHAR2,  
GLOBAL_REJECT_RETRY_LIMIT    IN NUMBER,        
ORDER_BOUNCE_COUNT           IN NUMBER,        
MARS_ORDER_BOUNCE_COUNT      IN NUMBER,        
AUTO_RESP_OFF_BEFORE_CUTOFF  IN NUMBER,        
FLORIST_SHOP_SOFT_BLOCK_DAYS IN NUMBER,        
ASKP_THRESHOLD               IN NUMBER,        
KEY_PHRASES                  IN VARCHAR2,
PHOENIX_ELIGIBLE_FLAG        IN CHAR,     
CREATED_ON                   IN DATE,          
CREATED_BY                   IN VARCHAR2,
UPDATED_ON                   IN DATE,          
UPDATED_BY                   IN VARCHAR2,
EXTERNAL_ORDER_NUMBER 		 IN VARCHAR2,
FLORIST_ID 					 IN VARCHAR2,
CODIFICATION_ID        		IN VARCHAR2,
CODIFIED_MINIMUM             IN NUMBER   


Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

UPDATE MERCURY_RULES_TRACKER SET 

RULES_FIRED = IN_RULES_FIRED,
ACTION_TAKEN  = IN_ACTION_TAKEN,
ACTION_DESCRIPTION  = IN_ACTION_DESCRIPTION,
ACTION_STATUS  = IN_ACTION_STATUS, 
RULE_TYPE  = IN_RULE_TYPE,
MERCURY_ID  = IN_MERCURY_ID, 
EXCEPTION_MESSAGE  = IN_EXCEPTION_MESSAGE, 
ORDER_DETAIL_ID  = IN_ORDER_DETAIL_ID, 
ORDER_DATE = IN_ORDER_DATE, 
ORDER_ORIGIN = IN_ORDER_ORIGIN,
SOURCE_CODE = IN_SOURCE_CODE,
MESSAGE_TYPE = IN_MESSAGE_TYPE, 
DELIVERY_DATE = IN_DELIVERY_DATE,
PRODUCT_ID  = IN_PRODUCT_ID, 
MESSAGE_DIRECTION  = IN_MESSAGE_DIRECTION,
MESSAGE_TEXT  = IN_MESSAGE_TEXT,
SAK_TEXT = IN_SAK_TEXT,
CUTOFF  = IN_CUTOFF,
OCCASION = IN_OCCASION,
DELIVERY_ZIP_CODE = IN_DELIVERY_ZIP_CODE, 
DELIVERY_STATE  = IN_DELIVERY_STATE, 
DELIVERY_CITY  = IN_DELIVERY_CITY,
RECIPIENT_FIRST_NAME  = IN_RECIPIENT_FIRST_NAME, 
RECIPIENT_LAST_NAME  = IN_RECIPIENT_LAST_NAME, 
RECIPIENT_PHONE_NUMBER = IN_RECIPIENT_PHONE_NUMBER,
RECIPIENT_ADDRESS  = IN_RECIPIENT_ADDRESS,
RECIPIENT_ZIP_CODE  = IN_RECIPIENT_ZIP_CODE, 
RECIPIENT_CITY = IN_RECIPIENT_CITY,
RECIPIENT_STATE  = IN_RECIPIENT_STATE,
RECIPIENT_COUNTRY = IN_RECIPIENT_COUNTRY, 
CUSTOMER_FIRST_NAME  = IN_CUSTOMER_FIRST_NAME, 
CUSTOMER_LAST_NAME  = IN_CUSTOMER_LAST_NAME, 
CUSTOMER_PHONE_NUMBER  = IN_CUSTOMER_PHONE_NUMBER,
CUSTOMER_ADDRESS  = IN_CUSTOMER_ADDRESS,
CUSTOMER_ZIP_CODE  = IN_CUSTOMER_ZIP_CODE,
CUSTOMER_CITY  = IN_CUSTOMER_CITY, 
CUSTOMER_STATE  = IN_CUSTOMER_STATE,
CUSTOMER_COUNTRY  = IN_CUSTOMER_COUNTRY, 
MERCURY_NUMBER  = IN_MERCURY_NUMBER,
ORIGINAL_MERCURY_PRICE  = IN_ORIGINAL_MERCURY_PRICE,
NEW_MERCURY_PRICE  = IN_NEW_MERCURY_PRICE,
TIMEZONE = IN_TIMEZONE, 
PREFERRED_PARTNER_ID = IN_PREFERRED_PARTNER_ID,
GLOBAL_REJECT_RETRY_LIMIT = IN_GLOBAL_REJECT_RETRY_LIMIT,
ORDER_BOUNCE_COUNT = IN_ORDER_BOUNCE_COUNT,
MARS_ORDER_BOUNCE_COUNT  = IN_MARS_ORDER_BOUNCE_COUNT, 
AUTO_RESP_OFF_BEFORE_CUTOFF  = IN_AUTO_RESP_OFF_BEFORE_CUTOFF,
FLORIST_SHOP_SOFT_BLOCK_DAYS  = IN_FLORIST_SHOP_BLOCK_DAYS,
ASKP_THRESHOLD  = IN_ASKP_THRESHOLD,
KEY_PHRASES  = IN_KEY_PHRASES,
PHOENIX_ELIGIBLE_FLAG  = IN_PHOENIX_ELIGIBLE_FLAG,
UPDATED_ON = sysdate, 
UPDATED_BY  = IN_UPDATED_BY,
EXTERNAL_ORDER_NUMBER  = IN_EXTERNAL_ORDER_NUMBER,
FLORIST_ID  = IN_FLORIST_ID,
CODIFICATION_ID = IN_CODIFICATION_ID,
CODIFIED_MINIMUM = IN_CODIFIED_MINIMUM

WHERE RULES_TRACKER_ID = IN_RULES_TRACKER_ID;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MERCURY_RULES_TRACKER;

PROCEDURE GET_MERCURY_RULES_TRACKER
(
 IN_MERCURY_ID   		  IN VARCHAR2,
 IN_ORDER_DETAIL_ID		IN NUMBER,
 OUT_CURSOR     		  OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CURSOR FOR
        SELECT RULES_TRACKER_ID,
               RULES_FIRED,
               ACTION_TAKEN,
               ACTION_DESCRIPTION,
               ACTION_STATUS,
               RULE_TYPE,
               MERCURY_ID,
               EXCEPTION_MESSAGE,
               ORDER_DETAIL_ID,
               ORDER_DATE,
               ORDER_ORIGIN,
               SOURCE_CODE,
               MESSAGE_TYPE,
               DELIVERY_DATE,
               PRODUCT_ID,
               MESSAGE_DIRECTION,
               MESSAGE_TEXT,
               SAK_TEXT,
               CUTOFF,
               OCCASION,
               DELIVERY_ZIP_CODE,
               DELIVERY_STATE,
               DELIVERY_CITY,
               RECIPIENT_FIRST_NAME,
               RECIPIENT_LAST_NAME,
               RECIPIENT_PHONE_NUMBER,
               RECIPIENT_ADDRESS,
               RECIPIENT_ZIP_CODE,
               RECIPIENT_CITY,
               RECIPIENT_STATE,
               RECIPIENT_COUNTRY,
               CUSTOMER_FIRST_NAME,
               CUSTOMER_LAST_NAME,
               CUSTOMER_PHONE_NUMBER,
               CUSTOMER_ADDRESS,
               CUSTOMER_ZIP_CODE,
               CUSTOMER_CITY,
               CUSTOMER_STATE,
               CUSTOMER_COUNTRY,
               MERCURY_NUMBER,
               ORIGINAL_MERCURY_PRICE,
               NEW_MERCURY_PRICE,
               TIMEZONE,
               PREFERRED_PARTNER_ID,
               GLOBAL_REJECT_RETRY_LIMIT,
               ORDER_BOUNCE_COUNT,
               MARS_ORDER_BOUNCE_COUNT,
               AUTO_RESP_OFF_BEFORE_CUTOFF,
               FLORIST_SHOP_SOFT_BLOCK_DAYS,
               ASKP_THRESHOLD,
               KEY_PHRASES,
               PHOENIX_ELIGIBLE_FLAG,
               CREATED_ON,                  
               CREATED_BY,
               UPDATED_ON,
               UPDATED_BY
        FROM MERCURY.MERCURY_RULES_TRACKER
        WHERE MERCURY_ID = IN_MERCURY_ID
        AND ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

END GET_MERCURY_RULES_TRACKER;

PROCEDURE INSERT_MISSING_DCONS_FOR_PRO
(
OUT_STATUS       OUT VARCHAR2,
OUT_MESSAGE       OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        Inserts the DCON PRO orders into PTN_PI.PARTNER_ORDER_FULFILLMENT table.

Output:
		status							varchar2
    message             varchar2
-----------------------------------------------------------------------------*/

v_order_detail_id NUMBER(20);



CURSOR order_cur IS 
select distinct to_number(m.reference_number)
from mercury.mercury m, clean.order_details
where m.msg_type = 'FTD'
and trunc(m.delivery_date) = trunc(sysdate - 1)
and not exists (
    select 1
    from mercury.mercury m2
    where m.reference_number = m2.reference_number
    and m2.msg_type = 'FTD'
    and m2.created_on > m.created_on
)
and not exists (
    select 1
    from mercury.mercury m3
    where m.mercury_order_number = m3.mercury_order_number
    and m3.msg_type in ('CAN','REJ')
)
and exists (
select 1
  from CLEAN.ORDER_DETAILS OD, PTN_PI.PARTNER_ORDER_DETAIL POD
  where POD.CONFIRMATION_NUMBER = OD.EXTERNAL_ORDER_NUMBER
  and pod.partner_id in (SELECT FTD_ORIGIN_MAPPING FROM PTN_PI.PARTNER_MAPPING WHERE SEND_DCON_FEED = 'Y')
  and 0 = (select count(1) from PTN_PI.PARTNER_ORDER_FULFILLMENT where partner_order_detail_id = POD.PARTNER_ORDER_DETAIL_ID)
  and to_char(od.order_detail_id) = m.REFERENCE_NUMBER
);


BEGIN

OUT_STATUS := 'N';

OPEN order_cur;
FETCH order_cur INTO v_order_detail_id;
WHILE order_cur%FOUND LOOP
  
  PTN_PI.PI_MAINT_PKG.INSERT_PTN_FLORIST_DCON_DATA(v_order_detail_id, 'Delivered', SYSDATE, OUT_STATUS, OUT_MESSAGE);
  
  FETCH order_cur INTO v_order_detail_id;

END LOOP;
CLOSE order_cur;

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MISSING_DCONS_FOR_PRO;

PROCEDURE EAPI_GET_MAIN_MEMBER_CODES
(
	IN_AVAILABLE    IN MERCURY_EAPI_OPS.IS_AVAILABLE%TYPE, 
	OUT_CUR         OUT TYPES.REF_CURSOR,
	OUT_STATUS      OUT VARCHAR2,
	OUT_MESSAGE     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the main member codes from
              mercury_eapi_ops table based on its availability.

Input:
        IN_AVAILABLE                  varchar2 

Output:
        cursor result set containing all fields in mercury
        status                          varchar2 (Y or N)
        message                         varchar2
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT  main_member_code,
                is_available,
                in_use
        FROM    mercury_eapi_ops
        WHERE   is_available = IN_AVAILABLE;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block

END EAPI_GET_MAIN_MEMBER_CODES;


END MERCURY_PKG;
.
/
