CREATE OR REPLACE
TRIGGER mercury.mercury_fix_outbound_id
BEFORE INSERT ON mercury.mercury
FOR EACH ROW
WHEN (NEW.message_direction = 'OUTBOUND' AND  NEW.outbound_id IS NULL AND NEW.msg_type = 'ANS')
BEGIN
/* This remediates an Apollo launch defect, where outbound IDs for ANS messages were being
   chosen randomly, rather than matching the suffix of the original FTD message. */
   SELECT outbound_id
   INTO   :new.outbound_id
   FROM   mercury
   WHERE  msg_type = 'FTD'
   AND    mercury_message_number = :new.mercury_order_number;
EXCEPTION WHEN OTHERS THEN NULL;
END;
.
/
