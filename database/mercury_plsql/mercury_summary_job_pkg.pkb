CREATE OR REPLACE
PACKAGE BODY mercury.MERCURY_SUMMARY_JOB_PKG AS

pg_summary_job          VARCHAR2(100) := 'MERCURY_SUMMARY_JOB_PKG.PROCESS_FLORIST_SUMMARY;';

PROCEDURE START_SUMMARY_JOBS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for scheduling the florist summary
        jobs.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
AS

v_jobno         number;
v_runtime_date  date;
v_interval      varchar2(100) := 'INTERVAL';
v_interval_val  varchar2(100);

v_check         number;

CURSOR check_cur (p_what varchar2 )IS
        SELECT  1
        FROM    user_jobs
        WHERE   what = p_what;
BEGIN

-- get the runtime and interval parameters for the check blocks job
v_interval_val := frp.misc_pkg.get_global_parm_value (pg_summary_job, v_interval);
EXECUTE IMMEDIATE 'select '||v_interval_val||' from dual' INTO v_runtime_date;

-- schedule the summary job if it isn't already scheduled
OPEN check_cur (pg_summary_job);
FETCH check_cur INTO v_check;
IF check_cur%notfound THEN
        dbms_job.submit(job=>v_jobno, what=>pg_summary_job, next_date=>v_runtime_date, interval=>v_interval_val);
        commit;
END IF;
CLOSE check_cur;

END START_SUMMARY_JOBS;


PROCEDURE STOP_SUMMARY_JOBS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing the florist summary
        jobs from the schedule.

Input:
        none

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
AS

v_job           number;

CURSOR check_cur (p_what varchar2 )IS
        SELECT  job
        FROM    user_jobs
        WHERE   what = p_what;
BEGIN

-- remove the check blocks job from the schedule
OPEN check_cur (pg_summary_job);
FETCH check_cur INTO v_job;
IF check_cur%found THEN
        dbms_job.remove (v_job);
        commit;
END IF;
CLOSE check_cur;

END STOP_SUMMARY_JOBS;


PROCEDURE UPDATE_FLORIST_SUMMARY
(
IN_SUMMARY_DATE                 MERCURY_FLORIST_SUMMARY.SUMMARY_DATE%TYPE,
IN_FLORIST_ID                   MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
IN_MSG_TYPE                     VARCHAR2,
IN_AMOUNT                       MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
IN_PROCESSED_FLORIST_ID         MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
IN_PROCESSED_MSG_TYPE           VARCHAR2,
IN_PROCESSED_AMOUNT             MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the mercury florist summary
        based on the florist_id and summary_date.

Input:
        summary_date                    date
        florist_id                      varchar2
        msg_type                        varchar2
        amount                          number
        processed florist id            varchar2
        processed msg type              varchar2
        processed amount                number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT  count (1)
        FROM    mercury_florist_summary
        WHERE   florist_id = in_florist_id
        AND     summary_date = in_summary_date;

v_check         number;
BEGIN

-- reverse the previously processed values which were changed because of a new message
IF in_processed_florist_id is not null THEN
        UPDATE  mercury_florist_summary
        SET     sent_count = sent_count - 1,
                sent_sales_amount = sent_sales_amount - in_processed_amount,
                filled_count = filled_count - DECODE (in_processed_msg_type, 'FTD', 1, 0),
                filled_sales_amount = filled_sales_amount - DECODE (in_processed_msg_type, 'FTD', in_processed_amount, 0),
                forwarded_count = forwarded_count - DECODE (in_processed_msg_type, 'FOR', 1, 0),
                rejected_count = rejected_count -  DECODE (in_processed_msg_type, 'REJ', 1, 0),
                cancelled_count = cancelled_count -  DECODE (in_processed_msg_type, 'CAN', 1, 0),
                sys_cancelled_count = sys_cancelled_count -  DECODE (in_processed_msg_type, 'SYSREJ', 1, 0),
                last_updated_on = sysdate
        WHERE   florist_id = in_processed_florist_id
        AND     summary_date = in_summary_date;
END IF;

-- check for the existance of the florist summary record
OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

IF v_check > 0 THEN

        UPDATE  mercury_florist_summary
        SET     sent_count = sent_count + 1,
                sent_sales_amount = sent_sales_amount + in_amount,
                filled_count = filled_count + DECODE (in_msg_type, 'FTD', 1, 0),
                filled_sales_amount = filled_sales_amount + DECODE (in_msg_type, 'FTD', in_amount, 0),
                forwarded_count = forwarded_count + DECODE (in_msg_type, 'FOR', 1, 0),
                rejected_count = rejected_count +  DECODE (in_msg_type, 'REJ', 1, 0),
                cancelled_count = cancelled_count +  DECODE (in_msg_type, 'CAN', 1, 0),
                sys_cancelled_count = sys_cancelled_count +  DECODE (in_msg_type, 'SYSREJ', 1, 0),
                last_updated_on = sysdate
        WHERE   florist_id = in_florist_id
        AND     summary_date = in_summary_date;

ELSE

        -- insert the new florist summary record
        INSERT INTO mercury_florist_summary
        (
                florist_id,
                summary_date,
                month,
                calendar_year,
                fiscal_year,
                sent_count,
                sent_sales_amount,
                filled_count,
                filled_sales_amount,
                forwarded_count,
                rejected_count,
                cancelled_count,
                sys_cancelled_count,
                last_updated_on
        )
        VALUES
        (
                in_florist_id,
                in_summary_date,
                to_char (in_summary_date, 'mm'),
                to_char (in_summary_date, 'yyyy'),
                CASE WHEN to_char (in_summary_date, 'mm') <= '06'
                          THEN to_char(in_summary_date, 'yyyy')
                     ELSE to_char(to_number(to_char(in_summary_date, 'yyyy')) + 1)
                END,
                1,
                in_amount,
                DECODE (in_msg_type, 'FTD', 1, 0),
                DECODE (in_msg_type, 'FTD', in_amount, 0),
                DECODE (in_msg_type, 'FOR', 1, 0),
                DECODE (in_msg_type, 'REJ', 1, 0),
                DECODE (in_msg_type, 'CAN', 1, 0),
                DECODE (in_msg_type, 'SYSREJ', 1, 0),
                sysdate
        );

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_FLORIST_SUMMARY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLORIST_SUMMARY;



PROCEDURE PROCESS_MERCURY_MESSAGE
(
IN_MERCURY_ORDER_NUMBER         IN MERCURY.MERCURY_ORDER_NUMBER%TYPE,
IN_FLORIST_ID                   IN MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
IN_LAST_PROCESS_DATE            IN DATE,
OUT_SUMMARY_DATE                OUT MERCURY_FLORIST_SUMMARY.SUMMARY_DATE%TYPE,
OUT_FLORIST_ID                  OUT MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
OUT_MSG_TYPE                    OUT VARCHAR2,
OUT_AMOUNT                      OUT MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
OUT_PROCESSED_FLORIST_ID        OUT MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
OUT_PROCESSED_MSG_TYPE          OUT VARCHAR2,
OUT_PROCESSED_AMOUNT            OUT MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
OUT_ON_NEW_RECORD               OUT BOOLEAN,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Go throught the entire progression of an order to find the end
        result for the order.

        1. FTD message indicates the start of the order
        2. ASK or ANS message with a P status indicates a change in the sale price
        3. REJ (reject) message ends the Mercury order
        4. CAN (cancel) message indicates FTD cancelled the order but continue
           to examine any further messages (ASK, ANS or DEN could follow)
        5. DEN (deny) message reverses a cancel on an order (message is considered
           as FTD again)
        6. FOR (forward) message indicates the florist receiving the order
           forwarded it to another florist (usually a branch location).
           Message is counted as a forwarded message for the original filling
           florist.  Set the summary florist id from the filling florist from
           the forwarded message and treat the message as an FTD message.
        7. SYSREJ (system cancelled) in the sak text field of a FTD message
           indicates an system cancelled order


Input:
        mercury_order_number            varchar2
        florist_id                      varchar2 - if given find the real time summary, null otherwise
        last_process_date               date

Output:
        summary_date                    date
        florist_id                      varchar2
        msg_type                        varchar2
        amount                          number
        processed florist id            varchar2
        processed msg type              varchar2
        processed amount                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_sak_text_value                varchar2(20);

-- Cursor gets the full progression of Mercury messages for a given
-- Mercury order number and filling florist, ordered by process date
CURSOR mesg_cur IS
        SELECT  mercury_order_number,
                msg_type,
                filling_florist,
                order_date,
                price,
                sak_text,
                transmission_time,
                ask_answer_code,
                created_on
        FROM    mercury.mercury
        WHERE   mercury_order_number = in_mercury_order_number
        AND     msg_type in ('FTD', 'FOR', 'REJ', 'CAN', 'ASK', 'ANS', 'DEN')
        ORDER BY decode (msg_type, 'FTD', 1, 2), created_on;

BEGIN

out_on_new_record := TRUE;
out_status := 'Y';

FOR msg IN mesg_cur LOOP

        IF msg.sak_text is not null THEN
                v_sak_text_value := substr (msg.sak_text, 1, 8);
        ELSE
                v_sak_text_value := 'null value';
        END IF;

        -- only process the message if it is VERIFIED (not rejected in the sak text) or
        -- if the message type is FTD (verified or not)
        IF msg.msg_type = 'FTD' OR v_sak_text_value <> 'REJECTED' THEN

                CASE

                -- 'FTD' message initiates the start of the order, pull all the lookup information
                -- for Mercury fact from this message
                WHEN msg.msg_type = 'FTD' THEN
                        -- Assign an 'FTD' disposition initially to indicate order filled
                        out_msg_type := msg.msg_type;

                        -- Get the fields that come directly from the Mercury message
                        out_florist_id := msg.filling_florist;
                        out_summary_date := to_date (to_char (msg.created_on, 'mm/yyyy'), 'mm/yyyy');
                        out_amount := msg.price;

                        -- parse the sak text for REJECTED - system cancelled message
                        IF v_sak_text_value = 'REJECTED' THEN
                                out_msg_type := 'SYSREJ';

                                IF msg.created_on <= in_last_process_date THEN
                                        out_on_new_record := FALSE;
                                END IF;

                                exit;
                        END IF;

                -- ASK and ANS message with 'P' ask/answer code indicate an adjustment
                -- to the price of the order
                WHEN msg.msg_type IN ('ASK', 'ANS') AND msg.ask_answer_code = 'P' THEN
                        out_amount := msg.price;

                -- REJ (reject) message ends the Mercury order, update the disposition
                -- and quit the Mercury message loop
                WHEN msg.msg_type = 'REJ' THEN
                        -- Assign a 'REJ' disposition and finish the message
                        out_msg_type := msg.msg_type;

                        IF msg.created_on <= in_last_process_date THEN
                                out_on_new_record := FALSE;
                        END IF;

                        exit;

                -- CAN (cancel) message indicates FTD cancelled the order, update the disposition
                -- but continue to examine any further messages (ASK, ANS or DEN could follow)
                WHEN msg.msg_type = 'CAN' THEN
                         -- Assign a 'CAN' disposition
                        out_msg_type := msg.msg_type;

                        IF msg.created_on <= in_last_process_date THEN
                                out_on_new_record := FALSE;
                        END IF;

                        exit;

                -- CR 10/2004 - The order will stop at the cancel.
                -- DEN (deny) message reverses a cancel on an order, update the disposition as 'FTD' again
                --WHEN msg.msg_type = 'DEN' THEN
                --        -- Assign a 'FTD' disposition to reverse a CAN disposition
                --        out_msg_type := 'FTD';

                -- FOR (forward) message indicates the florist receiving the order forwarded it to another
                -- florist (usually a branch location).  Change the disposition with the current florist
                -- to forward (FOR) and update the summary. Then set the disposition to 'FTD' and change
                -- the summary florist to the filling florist indicated on this forward msg.
                WHEN msg.msg_type = 'FOR' THEN
                         -- Assign a 'FOR' disposition
                        out_msg_type := msg.msg_type;

                        -- if the florist is given, the proc is getting the real time summary for the florist
                        IF in_florist_id is not null THEN
                                -- if the filling florist on the FOR message is not the sames as the given florist
                                -- the mercury order is done for the given florist so exit the processing
                                -- add counts for a forwarded message
                                IF msg.filling_florist <> in_florist_id THEN
                                        IF msg.created_on <= in_last_process_date THEN
                                                out_on_new_record := FALSE;
                                        END IF;
                                        exit;
                                END IF;

                        -- if the florist is not given, the proc is summarizing the last day's messages
                        ELSE
                                IF msg.created_on > in_last_process_date THEN
                                        -- update the forward count for the florist that forwarded the message
                                        update_florist_summary (out_summary_date,
                                                                out_florist_id, out_msg_type, out_amount,
                                                                out_processed_florist_id, out_processed_msg_type, out_processed_amount,
                                                                out_status, out_message);

                                        IF out_status = 'N' THEN
                                                exit;
                                        END IF;
                                END IF;

                        END IF;

                        -- Assign a 'FTD' disposition to begin the order for the forwarded florist
                        out_msg_type := 'FTD';

                        -- set the summary florist to the forwarded filling florist
                        out_florist_id := msg.filling_florist;

                ELSE    null;

                END CASE;

                -- If the message was processed during the last load
                -- set processed fields to indicate the possible need revert counts in the existing summary
                IF msg.created_on <= in_last_process_date THEN
                        out_processed_florist_id := out_florist_id;
                        out_processed_msg_type := out_msg_type;
                        out_processed_amount := out_amount;
                        out_on_new_record := FALSE;
                ELSE
                        out_on_new_record := TRUE;
                END IF;

        END IF;

END LOOP;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PROCESS_MERCURY_MESSAGE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PROCESS_MERCURY_MESSAGE;


PROCEDURE PROCESS_FLORIST_SUMMARY
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for determining the result of mercury
        messages and updating the mercury florist summary based on the
        result by florist_id and summary_date since the last summary update.

Input:
        none

Output:
        none - errors inserted into system messages

-----------------------------------------------------------------------------*/

v_today                         date := sysdate;
v_last_process_date             date;

v_summary_date                  mercury_florist_summary.summary_date%type;
v_florist_id                    mercury_florist_summary.florist_id%type;
v_msg_type                      varchar2(10);
v_amount                        mercury_florist_summary.sent_sales_amount%type;
v_processed_florist_id          mercury_florist_summary.florist_id%type;
v_processed_msg_type            varchar2(10);
v_processed_amount              mercury_florist_summary.sent_sales_amount%type;
v_on_new_record                 boolean;
v_sak_text_value                varchar2(20);

v_message_id                    number;
v_out_status                    varchar2(1) := 'Y';
v_out_message                   varchar2(1000);

-- Cursor gets the original FTD message sent across Mercury to start an order
-- for any message that has been inserted or updated since the last load.
-- Ignore new ADJ and GEN messages in this query.
CURSOR process_cur (p_last_process_date DATE, p_current_date DATE) IS
        SELECT  DISTINCT
                orig.mercury_order_number
        FROM    mercury.mercury orig
        JOIN    mercury.mercury delta
        ON      orig.mercury_order_number = delta.mercury_order_number
        WHERE   orig.msg_type = 'FTD'
        AND     delta.created_on > p_last_process_date
        AND     delta.created_on <= p_current_date
        AND     delta.msg_type not in ('ADJ', 'GEN')
        ORDER BY orig.mercury_order_number;

BEGIN

-- get the last summary process date from frp.global_parms
v_last_process_date := to_date (frp.misc_pkg.get_global_parm_value (pg_summary_job, 'LAST_PROCESS_DATE'), 'mm/dd/yyyy hh24:mi:ss');

-- update the last summary process date with the date used to retrieve the new mercury messages
frp.misc_pkg.update_global_parms (pg_summary_job, 'LAST_PROCESS_DATE', to_char (v_today, 'mm/dd/yyyy hh24:mi:ss'), 'SYS', v_out_status, v_out_message);

IF v_out_status = 'Y' THEN
        -- get the new mercury message
        FOR orig_msg IN process_cur (v_last_process_date, v_today) LOOP

                v_processed_florist_id := null;
                v_processed_msg_type := null;
                v_processed_amount := null;

                PROCESS_MERCURY_MESSAGE
                (
                        orig_msg.mercury_order_number,
                        null,
                        v_last_process_date,
                        v_summary_date,
                        v_florist_id,
                        v_msg_type,
                        v_amount,
                        v_processed_florist_id,
                        v_processed_msg_type,
                        v_processed_amount,
                        v_on_new_record,
                        v_out_status,
                        v_out_message
                );

                IF v_out_status = 'Y' THEN
                        IF v_on_new_record THEN
                                -- update the summary
                                update_florist_summary (v_summary_date,
                                                        v_florist_id, v_msg_type, v_amount,
                                                        v_processed_florist_id, v_processed_msg_type, v_processed_amount,
                                                        v_out_status, v_out_message);

                                IF v_out_status = 'N' THEN
                                        exit;
                                END IF;
                        END IF;
                ELSE
                        exit;
                END IF;

        END LOOP;
END IF;

IF v_out_status = 'Y' THEN
        commit;

ELSE
        rollback;
        -- insert system message error
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'MERCURY_SUMMARY_JOB_PKG.PROCESS_FLORIST_SUMMARY',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => v_out_message,
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );
END IF;

EXCEPTION WHEN OTHERS THEN
        -- insert system message error
        v_out_message := 'ERROR OCCURRED IN PROCESS_FLORIST_SUMMARY [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        rollback;
        FRP.MISC_PKG.INSERT_SYSTEM_MESSAGES
        (
                IN_SOURCE => 'MERCURY_SUMMARY_JOB_PKG.PROCESS_FLORIST_SUMMARY',
                IN_TYPE => 'EXCEPTION',
                IN_MESSAGE => v_out_message,
                IN_COMPUTER => null,
                OUT_SYSTEM_MESSAGE_ID => v_message_id,
                OUT_STATUS => v_out_status,
                OUT_MESSAGE => v_out_message
        );

END PROCESS_FLORIST_SUMMARY;


PROCEDURE PROCESS_FLORIST_SUMMARY
(
IN_FLORIST_ID                   MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
OUT_SENT_COUNT                  OUT MERCURY_FLORIST_SUMMARY.SENT_COUNT%TYPE,
OUT_SENT_SALES_AMOUNT           OUT MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
OUT_FILLED_COUNT                OUT MERCURY_FLORIST_SUMMARY.FILLED_COUNT%TYPE,
OUT_FILLED_SALES_AMOUNT         OUT MERCURY_FLORIST_SUMMARY.FILLED_SALES_AMOUNT%TYPE,
OUT_FORWARDED_COUNT             OUT MERCURY_FLORIST_SUMMARY.FORWARDED_COUNT%TYPE,
OUT_REJECTED_COUNT              OUT MERCURY_FLORIST_SUMMARY.REJECTED_COUNT%TYPE,
OUT_CANCELLED_COUNT             OUT MERCURY_FLORIST_SUMMARY.CANCELLED_COUNT%TYPE,
OUT_SYS_CANCELLED_COUNT         OUT MERCURY_FLORIST_SUMMARY.SYS_CANCELLED_COUNT%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for determining the result of mercury
        messages for the given florist since the last summary update.  This
        is used to add to the daily summarized data to get up to the minute
        summary data.

Input:
        florist_id                      varchar2

Output:
        sent_count                      number
        sent_sales_amount               number
        filled_count                    number
        filled_sales_amount             number
        forwarded_count                 number
        rejected_count                  number
        cancelled_count                 number
        sys_cancelled_count             number

-----------------------------------------------------------------------------*/

v_today                         date := sysdate;
v_last_process_date             date;

v_summary_date                  mercury_florist_summary.summary_date%type;
v_florist_id                    mercury_florist_summary.florist_id%type;
v_msg_type                      varchar2(10);
v_amount                        mercury_florist_summary.sent_sales_amount%type;
v_processed_florist_id          mercury_florist_summary.florist_id%type;
v_processed_msg_type            varchar2(10);
v_processed_amount              mercury_florist_summary.sent_sales_amount%type;
v_on_new_record                 boolean := FALSE;
v_sak_text_value                varchar2(20);

v_out_status                    varchar2(1) := 'Y';
v_out_message                   varchar2(1000);

-- Cursor gets the original FTD message sent across Mercury to start an order
-- for any message that has been inserted or updated since the last load.
-- Ignore new ADJ and GEN messages in this query.
CURSOR process_cur (p_last_process_date DATE, p_current_date DATE) IS
        SELECT  DISTINCT
                orig.mercury_order_number
        FROM    mercury.mercury orig
        JOIN    mercury.mercury delta
        ON      orig.mercury_order_number = delta.mercury_order_number
        WHERE   orig.msg_type = 'FTD'
        AND     delta.filling_florist = in_florist_id
        AND     delta.created_on > p_last_process_date
        AND     delta.created_on <= p_current_date
        AND     delta.msg_type not in ('ADJ', 'GEN')
        ORDER BY orig.mercury_order_number;

BEGIN

-- get the last summary process date from frp.global_parms
v_last_process_date := to_date (frp.misc_pkg.get_global_parm_value (pg_summary_job, 'LAST_PROCESS_DATE'), 'mm/dd/yyyy hh24:mi:ss');

-- initialize counters
out_sent_count := 0;
out_sent_sales_amount := 0;
out_filled_count := 0;
out_filled_sales_amount := 0;
out_forwarded_count := 0;
out_rejected_count := 0;
out_cancelled_count := 0;
out_sys_cancelled_count := 0;

-- get the new mercury message
FOR orig_msg IN process_cur (v_last_process_date, v_today) LOOP

        v_processed_florist_id := null;
        v_processed_msg_type := null;
        v_processed_amount := null;

        PROCESS_MERCURY_MESSAGE
        (
                orig_msg.mercury_order_number,
                null,
                v_last_process_date,
                v_summary_date,
                v_florist_id,
                v_msg_type,
                v_amount,
                v_processed_florist_id,
                v_processed_msg_type,
                v_processed_amount,
                v_on_new_record,
                v_out_status,
                v_out_message
        );

        IF v_on_new_record THEN
                -- revert counts on the existing summary for the florist (a new message affected existing summary counts)
                IF v_processed_florist_id = in_florist_id THEN
                        out_sent_count := out_sent_count - 1;
                        out_sent_sales_amount := out_sent_sales_amount - v_processed_amount;

                        CASE v_processed_msg_type
                                WHEN 'FTD' THEN
                                        out_filled_count := out_filled_count - 1;
                                        out_filled_sales_amount := out_filled_sales_amount - v_processed_amount;
                                WHEN 'FOR' THEN
                                        out_forwarded_count := out_forwarded_count - 1;
                                WHEN 'REJ' THEN
                                        out_rejected_count := out_rejected_count - 1;
                                WHEN 'CAN' THEN
                                        out_cancelled_count := out_cancelled_count - 1;
                                WHEN 'SYSREJ' THEN
                                        out_sys_cancelled_count := out_sys_cancelled_count - 1;
                        END CASE;
                END IF;

                -- add summary for the florist
                IF v_florist_id = in_florist_id THEN
                        out_sent_count := out_sent_count + 1;
                        out_sent_sales_amount := out_sent_sales_amount + v_amount;

                        CASE v_msg_type
                                WHEN 'FTD' THEN
                                        out_filled_count := out_filled_count + 1;
                                        out_filled_sales_amount := out_filled_sales_amount + v_amount;
                                WHEN 'FOR' THEN
                                        out_forwarded_count := out_forwarded_count + 1;
                                WHEN 'REJ' THEN
                                        out_rejected_count := out_rejected_count + 1;
                                WHEN 'CAN' THEN
                                        out_cancelled_count := out_cancelled_count + 1;
                                WHEN 'SYSREJ' THEN
                                        out_sys_cancelled_count := out_sys_cancelled_count + 1;
                        END CASE;
                END IF;

        END IF;

END LOOP;

END PROCESS_FLORIST_SUMMARY;



END MERCURY_SUMMARY_JOB_PKG;
.
/
