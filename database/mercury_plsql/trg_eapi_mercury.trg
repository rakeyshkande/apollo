CREATE OR REPLACE
TRIGGER mercury.trg_eapi_mercury
AFTER INSERT OR UPDATE
ON mercury.mercury
FOR EACH ROW

DECLARE

   v_message_id NUMBER;
   v_status VARCHAR2(4000);
   v_message VARCHAR2(4000);
   v_post_flag boolean;
   v_is_eapi_available VARCHAR2(10);
   v_sending_florist VARCHAR2(100);

BEGIN

   v_post_flag := false;

   IF UPDATING THEN
     -- initialize the status
      IF (:NEW.mercury_status = 'MO' and :OLD.mercury_status <> 'MO') THEN
         v_post_flag := true;
      END IF;
      
   ELSIF INSERTING THEN
      IF :NEW.message_direction = 'OUTBOUND' and :NEW.mercury_status = 'MO' THEN
         v_post_flag := true;
      END IF;
   END IF;

   if v_post_flag and :NEW.outbound_id is not null THEN

      begin

         v_sending_florist := :NEW.sending_florist || :NEW.outbound_id;

         select is_available
         into v_is_eapi_available
         from mercury_eapi_ops
         where main_member_code = v_sending_florist;

         exception when no_data_found then
            return;

         if v_is_eapi_available = 'Y' then
            v_post_flag := true;
         else 
            v_post_flag := false;
         end if;

      end;

   end if;

   if v_post_flag then
      events.post_a_message_flex('OJMS.MERCURY_PROCESS_POST', :NEW.mercury_id, :NEW.mercury_id, 1, v_status, v_message);
   end if;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In MERCURY.TRG_EAPI_MERCURY: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);

END trg_eapi_mercury;
.
/
