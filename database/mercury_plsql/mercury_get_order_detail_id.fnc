CREATE OR REPLACE
FUNCTION mercury.MERCURY_GET_ORDER_DETAIL_ID (
   PNUM_ORDER_NUMBER   MERCURY.MERCURY_ORDER_NUMBER%TYPE)
RETURN VARCHAR2
AS

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***             Purpose : Get the reference_number for a given order_number
***
***    Input Parameters : pnum_order_number - order_number
***
***             Returns : Appropriate reference_number for a given order_number
***
*** Special Instructions:
***
*****************************************************************************************/

   CURSOR cur_get_ref_no IS
   SELECT
      m1.reference_number
   FROM
      mercury.mercury m1
   WHERE
      m1.mercury_order_number = pnum_order_number
      AND m1.msg_type = 'FTD'
   ORDER BY
      m1.transmission_time DESC;

   lchr_reference_number   mercury.reference_number%TYPE := NULL;

   PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

   -- fetch the appropriate reference_number for an order_number
   OPEN cur_get_ref_no;
   FETCH cur_get_ref_no INTO lchr_reference_number;
   CLOSE cur_get_ref_no;

   RETURN lchr_reference_number;


EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In MERCURY.MERCURY_GET_ORDER_DETAIL_ID: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END MERCURY_GET_ORDER_DETAIL_ID;
.
/
