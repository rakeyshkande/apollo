CREATE OR REPLACE
PACKAGE BODY mercury.MERCURY_FLORIST_PKG AS

FUNCTION GET_MESSAGE_STATUS
(
IN_MERCURY_ORDER_NUMBER         IN MERCURY.MERCURY_ORDER_NUMBER%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        Go throught the entire progression of an order to find the end
        result for the order.

        1. FTD message indicates the start of the order
        2. ADJ message indicates an adjustment was made
        3. REJ (reject) message ends the Mercury order
        4. CAN (cancel) message indicates FTD cancelled the order but continue
           to examine any further messages (ASK, ANS or DEN could follow)
        5. DEN (deny) message reverses a cancel on an order (message is considered
           as 'FTD' again)
        6. FOR (forward) message indicates the florist receiving the order
           forwarded it to another florist (usually a branch location).
           Message is counted as a forwarded message for the original filling
           florist.  Set the summary florist id from the filling florist from
           the forwarded message and treat the message as an FTD message.
        7. SYSREJ (system cancelled) in the sak text field of a FTD message
           indicates an system cancelled order


Input:
        mercury_order_number            varchar2

Output:
        msg_type                        varchar2

-----------------------------------------------------------------------------*/

v_sak_text_value                varchar2(20);
v_msg_type                      varchar2(10);

-- Cursor gets the full progression of Mercury messages for a given
-- Mercury order number and filling florist, ordered by process date
CURSOR mesg_cur IS
        SELECT  mercury_order_number,
                msg_type,
                filling_florist,
                order_date,
                price,
                sak_text,
                transmission_time,
                ask_answer_code,
                created_on
        FROM    mercury
        WHERE   mercury_order_number = in_mercury_order_number
        AND     msg_type in ('FTD', 'FOR', 'REJ', 'CAN', 'DEN', 'ADJ')
        ORDER BY created_on;

BEGIN

FOR msg IN mesg_cur LOOP

        IF msg.sak_text is not null THEN
                v_sak_text_value := substr (msg.sak_text, 1, 8);
        ELSE
                v_sak_text_value := 'null value';
        END IF;

        -- only process the message if it is VERIFIED (not rejected in the sak text) or
        -- if the message type is FTD (verified or not)
        IF msg.msg_type = 'FTD' OR v_sak_text_value <> 'REJECTED' THEN

                v_msg_type := msg.msg_type;

                -- handle special cases
                CASE

                WHEN msg.msg_type = 'FTD' THEN
                        -- parse the sak text for REJECTED - system cancelled message
                        IF v_sak_text_value = 'REJECTED' THEN
                                v_msg_type := 'SYSREJ';
                                exit;
                        END IF;

                -- REJ (reject) message ends the Mercury order, update the disposition
                -- and quit the Mercury message loop
                WHEN msg.msg_type = 'REJ' THEN
                        exit;

                -- DEN (deny) message reverses a cancel on an order, update the disposition as 'FTD' again
                WHEN msg.msg_type = 'DEN' THEN
                        -- Assign a 'FTD' disposition to reverse a CAN disposition
                        v_msg_type := 'FTD';

                ELSE    null;

                END CASE;
        END IF;

END LOOP;

RETURN v_msg_type;

END GET_MESSAGE_STATUS;


PROCEDURE SEARCH_MERCURY_ORDERS
(
IN_ORDER_START_DATE             IN MERCURY.ORDER_DATE%TYPE,
IN_ORDER_END_DATE               IN MERCURY.ORDER_DATE%TYPE,
IN_DELIVERY_START_DATE          IN MERCURY.DELIVERY_DATE%TYPE,
IN_DELIVERY_END_DATE            IN MERCURY.DELIVERY_DATE%TYPE,
IN_MERCURY_ORDER_NUMBER         IN MERCURY.MERCURY_ORDER_NUMBER%TYPE,
IN_RECIPIENT                    IN MERCURY.RECIPIENT%TYPE,
IN_PRICE                        IN MERCURY.PRICE%TYPE,
IN_PRODUCT_ID                   IN MERCURY.PRODUCT_ID%TYPE,
IN_RESPONSIBLE_FLORIST          IN MERCURY.RESPONSIBLE_FLORIST%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving florist based on the
        given search parameters.

Input:
        responsible_florist             varchar2 (required)
        order_start_date                date
        order_end_date                  date
        delivery_start_date             date
        delivery_end_date               date
        mercury_order_number            varchar2

        recipient                       varchar2 (pattern match)
        price                           number
        product_id                      varchar2 (novator product id)

Output:
        cursor containing mercury_order_number, order_date, delivery_date,
        recipient, status of the message, price and novator product idthat
        match the given search parameters

-----------------------------------------------------------------------------*/
v_debug                 boolean := false; -- debug mode will display the sql string
v_sql                   varchar2 (16000);

-- select clause variables
v_base                  varchar2 (500) := 'select distinct ';
v_fields                varchar2 (1000) := 'a.mercury_message_number, a.mercury_order_number, a.order_date, a.delivery_date, a.recipient, mercury.mercury_florist_pkg.get_message_status(a.mercury_order_number) status, a.price ';
v_fields_product        varchar2 (500) := ', (select p.novator_id from ftd_apps.product_master p where p.product_id = a.product_id) novator_id ';
v_from                  varchar2 (500) := 'from mercury a ';

-- where clause variables
v_where                 varchar2 (4000) := 'where a.responsible_florist = ''' || in_responsible_florist || ''' ';
v_order_date            varchar2 (500) := 'and a.order_date between to_date (''' || to_char (in_order_start_date, 'mm/dd/yyyy') || ''', ''mm/dd/yyyy'') and to_date (''' || to_char (in_order_end_date, 'mm/dd/yyyy') || ''', ''mm/dd/yyyy'') ';
v_delivery_date         varchar2 (500) := 'and a.delivery_date between to_date (''' || to_char (in_delivery_start_date, 'mm/dd/yyyy') || ''', ''mm/dd/yyyy'') and to_date (''' || to_char (in_delivery_end_date, 'mm/dd/yyyy') || ''', ''mm/dd/yyyy'') ';
v_order_number          varchar2 (500) := 'and a.mercury_order_number = ''' || in_mercury_order_number || ''' ';
v_recipient             varchar2 (500) := 'and a.recipient like ''%' || in_recipient || '%'' ';
v_price                 varchar2 (500) := 'and a.price = ' || to_char (in_price) || ' ';
v_product_id            varchar2 (500) := 'and a.product_id = (select p.product_id from ftd_apps.product_master p where rownum = 1 and p.novator_id = ''' || in_product_id || ''') ';

-- order by variables
v_order_by              varchar2 (4000) := 'order by a.order_date desc';


BEGIN

-- build where clause using the florist master fields
if in_order_start_date is not null then
        v_where := v_where || v_order_date;
end if;

if in_delivery_start_date is not null then
        v_where := v_where || v_delivery_date;
end if;

if in_mercury_order_number is not null then
        v_where := v_where || v_order_number;
end if;

if in_recipient is not null then
        v_where := v_where || v_recipient;
end if;

if in_price is not null then
        v_where := v_where || v_price;
end if;

if in_product_id is not null then
        v_where := v_where || v_product_id;
end if;

-- build the entire sql statement
v_sql := v_base || v_fields || v_fields_product || v_from || v_where || v_order_by;

-- execute the sql statement
if v_debug then
        open out_cur for select v_sql as debug_mode from dual;
else
        open out_cur for v_sql;
end if;

END SEARCH_MERCURY_ORDERS;


PROCEDURE PROCESS_FLORIST_REAL_TIME_PERF
(
IN_FLORIST_ID                   MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
OUT_SENT_COUNT                  OUT MERCURY_FLORIST_SUMMARY.SENT_COUNT%TYPE,
OUT_SENT_SALES_AMOUNT           OUT MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
OUT_FILLED_COUNT                OUT MERCURY_FLORIST_SUMMARY.FILLED_COUNT%TYPE,
OUT_FILLED_SALES_AMOUNT         OUT MERCURY_FLORIST_SUMMARY.FILLED_SALES_AMOUNT%TYPE,
OUT_FORWARDED_COUNT             OUT MERCURY_FLORIST_SUMMARY.FORWARDED_COUNT%TYPE,
OUT_REJECTED_COUNT              OUT MERCURY_FLORIST_SUMMARY.REJECTED_COUNT%TYPE,
OUT_CANCELLED_COUNT             OUT MERCURY_FLORIST_SUMMARY.CANCELLED_COUNT%TYPE,
OUT_SYS_CANCELLED_COUNT         OUT MERCURY_FLORIST_SUMMARY.SYS_CANCELLED_COUNT%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for determining the result of mercury
        messages for the given florist since the last summary update.  This
        is used to add to the daily summarized data to get up to the minute
        summary data.

Input:
        florist_id                      varchar2

Output:
        sent_count                      number
        sent_sales_amount               number
        filled_count                    number
        filled_sales_amount             number
        forwarded_count                 number
        rejected_count                  number
        cancelled_count                 number
        sys_cancelled_count             number

-----------------------------------------------------------------------------*/

v_today                         date := sysdate;
v_last_process_date             date;
v_summary_job                   varchar2(100) := 'MERCURY_SUMMARY_JOB_PKG.PROCESS_FLORIST_SUMMARY;';

v_summary_date                  mercury_florist_summary.summary_date%type;
v_florist_id                    mercury_florist_summary.florist_id%type;
v_msg_type                      varchar2(10);
v_amount                        mercury_florist_summary.sent_sales_amount%type;
v_processed_florist_id          mercury_florist_summary.florist_id%type;
v_processed_msg_type            varchar2(10);
v_processed_amount              mercury_florist_summary.sent_sales_amount%type;
v_on_new_record                 boolean := FALSE;
v_sak_text_value                varchar2(20);

v_out_status                    varchar2(1) := 'Y';
v_out_message                   varchar2(1000);

-- Cursor gets the original FTD message sent across Mercury to start an order
-- for any message that has been inserted or updated since the last load.
-- Ignore new ADJ and GEN messages in this query.
CURSOR process_cur (p_last_process_date DATE, p_current_date DATE) IS
        SELECT  DISTINCT
                orig.mercury_order_number
        FROM    mercury orig
        JOIN    mercury delta
        ON      orig.mercury_order_number = delta.mercury_order_number
        WHERE   orig.msg_type = 'FTD'
        AND     delta.filling_florist = in_florist_id
        AND     delta.created_on > p_last_process_date
        AND     delta.created_on <= p_current_date
        AND     delta.msg_type not in ('ADJ', 'GEN')
        ORDER BY orig.mercury_order_number;

BEGIN

-- get the last summary process date from frp.global_parms
v_last_process_date := to_date (frp.misc_pkg.get_global_parm_value (v_summary_job, 'LAST_PROCESS_DATE'), 'mm/dd/yyyy hh24:mi:ss');

-- initialize counters
out_sent_count := 0;
out_sent_sales_amount := 0;
out_filled_count := 0;
out_filled_sales_amount := 0;
out_forwarded_count := 0;
out_rejected_count := 0;
out_cancelled_count := 0;
out_sys_cancelled_count := 0;

-- get the new mercury message
FOR orig_msg IN process_cur (v_last_process_date, v_today) LOOP

        v_processed_florist_id := null;
        v_processed_msg_type := null;
        v_processed_amount := null;

        mercury_summary_job_pkg.process_mercury_message
        (
                orig_msg.mercury_order_number,
                null,
                v_last_process_date,
                v_summary_date,
                v_florist_id,
                v_msg_type,
                v_amount,
                v_processed_florist_id,
                v_processed_msg_type,
                v_processed_amount,
                v_on_new_record,
                v_out_status,
                v_out_message
        );

        IF v_on_new_record THEN
                -- revert counts on the existing summary for the florist (a new message affected existing summary counts)
                IF v_processed_florist_id = in_florist_id THEN
                        out_sent_count := out_sent_count - 1;
                        out_sent_sales_amount := out_sent_sales_amount - v_processed_amount;

                        CASE v_processed_msg_type
                                WHEN 'FTD' THEN
                                        out_filled_count := out_filled_count - 1;
                                        out_filled_sales_amount := out_filled_sales_amount - v_processed_amount;
                                WHEN 'FOR' THEN
                                        out_forwarded_count := out_forwarded_count - 1;
                                WHEN 'REJ' THEN
                                        out_rejected_count := out_rejected_count - 1;
                                WHEN 'CAN' THEN
                                        out_cancelled_count := out_cancelled_count - 1;
                                WHEN 'SYSREJ' THEN
                                        out_sys_cancelled_count := out_sys_cancelled_count - 1;
                        END CASE;
                END IF;

                -- add summary for the florist
                IF v_florist_id = in_florist_id THEN
                        out_sent_count := out_sent_count + 1;
                        out_sent_sales_amount := out_sent_sales_amount + v_amount;

                        CASE v_msg_type
                                WHEN 'FTD' THEN
                                        out_filled_count := out_filled_count + 1;
                                        out_filled_sales_amount := out_filled_sales_amount + v_amount;
                                WHEN 'FOR' THEN
                                        out_forwarded_count := out_forwarded_count + 1;
                                WHEN 'REJ' THEN
                                        out_rejected_count := out_rejected_count + 1;
                                WHEN 'CAN' THEN
                                        out_cancelled_count := out_cancelled_count + 1;
                                WHEN 'SYSREJ' THEN
                                        out_sys_cancelled_count := out_sys_cancelled_count + 1;
                        END CASE;
                END IF;

        END IF;

END LOOP;

END PROCESS_FLORIST_REAL_TIME_PERF;


PROCEDURE GET_FLORIST_REAL_TIME_PERF
(
IN_FLORIST_ID                   MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
IN_ASSOCIATED_FLORIST_FLAG      IN VARCHAR2,
OUT_SENT_COUNT                  OUT MERCURY_FLORIST_SUMMARY.SENT_COUNT%TYPE,
OUT_SENT_SALES_AMOUNT           OUT MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE,
OUT_FILLED_COUNT                OUT MERCURY_FLORIST_SUMMARY.FILLED_COUNT%TYPE,
OUT_FILLED_SALES_AMOUNT         OUT MERCURY_FLORIST_SUMMARY.FILLED_SALES_AMOUNT%TYPE,
OUT_FORWARDED_COUNT             OUT MERCURY_FLORIST_SUMMARY.FORWARDED_COUNT%TYPE,
OUT_REJECTED_COUNT              OUT MERCURY_FLORIST_SUMMARY.REJECTED_COUNT%TYPE,
OUT_CANCELLED_COUNT             OUT MERCURY_FLORIST_SUMMARY.CANCELLED_COUNT%TYPE,
OUT_SYS_CANCELLED_COUNT         OUT MERCURY_FLORIST_SUMMARY.SYS_CANCELLED_COUNT%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for determining the result of mercury
        messages for the florist since the last summary update.  This
        is used to add to the daily summarized data to get up to the minute
        summary data.  If the associated florist flag is Y, the summary
        will include all associated florists.

Input:
        florist_id                      varchar2
        associated florist flag         varhcar2 (Y - total all associated florist,
                                                  N - only for given florist)

Output:
        sent_count                      number
        sent_sales_amount               number
        filled_count                    number
        filled_sales_amount             number
        forwarded_count                 number
        rejected_count                  number
        cancelled_count                 number
        sys_cancelled_count             number

-----------------------------------------------------------------------------*/

v_sent_count                  MERCURY_FLORIST_SUMMARY.SENT_COUNT%TYPE;
v_sent_sales_amount           MERCURY_FLORIST_SUMMARY.SENT_SALES_AMOUNT%TYPE;
v_filled_count                MERCURY_FLORIST_SUMMARY.FILLED_COUNT%TYPE;
v_filled_sales_amount         MERCURY_FLORIST_SUMMARY.FILLED_SALES_AMOUNT%TYPE;
v_forwarded_count             MERCURY_FLORIST_SUMMARY.FORWARDED_COUNT%TYPE;
v_rejected_count              MERCURY_FLORIST_SUMMARY.REJECTED_COUNT%TYPE;
v_cancelled_count             MERCURY_FLORIST_SUMMARY.CANCELLED_COUNT%TYPE;
v_sys_cancelled_count         MERCURY_FLORIST_SUMMARY.SYS_CANCELLED_COUNT%TYPE;

-- Cursor gets the associated florists
CURSOR florist_cur IS
        select  m.florist_id
         from   ftd_apps.florist_master m
         where  m.top_level_florist_id =
                (select top_level_florist_id
                 from ftd_apps.florist_master
                 where florist_id = in_florist_id);

BEGIN

-- initialize counters
out_sent_count := 0;
out_sent_sales_amount := 0;
out_filled_count := 0;
out_filled_sales_amount := 0;
out_forwarded_count := 0;
out_rejected_count := 0;
out_cancelled_count := 0;
out_sys_cancelled_count := 0;

IF in_associated_florist_flag = 'Y' THEN
        -- get the summary for all associated florists
        FOR florist_row IN florist_cur LOOP

                PROCESS_FLORIST_REAL_TIME_PERF
                (
                        florist_row.florist_id,
                        v_sent_count,
                        v_sent_sales_amount,
                        v_filled_count,
                        v_filled_sales_amount,
                        v_forwarded_count,
                        v_rejected_count,
                        v_cancelled_count,
                        v_sys_cancelled_count
                );

                out_sent_count := out_sent_count + v_sent_count;
                out_sent_sales_amount := out_sent_sales_amount + v_sent_sales_amount;
                out_filled_count := out_filled_count + v_filled_count;
                out_filled_sales_amount := out_filled_sales_amount + v_filled_sales_amount;
                out_forwarded_count := out_forwarded_count + v_forwarded_count;
                out_rejected_count := out_rejected_count + v_rejected_count;
                out_cancelled_count := out_cancelled_count + v_cancelled_count;
                out_sys_cancelled_count := out_sys_cancelled_count + v_sys_cancelled_count;

        END LOOP;

ELSE

        PROCESS_FLORIST_REAL_TIME_PERF
        (
                in_florist_id,
                out_sent_count,
                out_sent_sales_amount,
                out_filled_count,
                out_filled_sales_amount,
                out_forwarded_count,
                out_rejected_count,
                out_cancelled_count,
                out_sys_cancelled_count
        );

END IF;

END GET_FLORIST_REAL_TIME_PERF;


PROCEDURE VIEW_FLORIST_PERFORMANCE
(
IN_FLORIST_ID                   IN MERCURY_FLORIST_SUMMARY.FLORIST_ID%TYPE,
IN_ASSOCIATED_FLORIST_FLAG      IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a summary performance
        for the last 3 fiscal and calendar years for the given florist.
        If the associated florist flag is Y, the summary will include all
        associated florists.

Input:
        filling_florist                 varchar2
        associated florist flag         varhcar2 (Y - total all associated florist,
                                                  N - only for given florist)

Output:
        cursor result set containing a row for the each of the last 3 years
        fiscal and calendar year performance with the columns of total orders
        sent, total sales, orders filled, sales filled, orders forwarded,
        orders rejected, order cancelled

-----------------------------------------------------------------------------*/
v_calendar_date         date := to_date ('01/01/' || to_char(to_number(to_char (sysdate, 'yyyy')) - 2), 'mm/dd/yyyy');
v_fiscal_date           date := to_date ('06/01/' ||
                                (CASE WHEN to_char (sysdate, 'mm') <= '06'
                                        THEN to_char(to_number(to_char(sysdate, 'yyyy')) - 3)
                                     ELSE to_char(to_number(to_char(sysdate, 'yyyy')) - 2)
                                END), 'mm/dd/yyyy');
BEGIN

IF in_associated_florist_flag = 'Y' THEN
        OPEN OUT_CUR FOR
                SELECT
                        'Calendar Year' year_type,
                        calendar_year year_number,
                        sum (sent_count) sent_count,
                        sum (sent_sales_amount) sent_sales_amount,
                        sum (filled_count) filled_count,
                        sum (filled_sales_amount) filled_sales_amount,
                        sum (forwarded_count) forwarded_count,
                        sum (rejected_count) rejected_count,
                        sum (cancelled_count) cancelled_count,
                        sum (sys_cancelled_count) sys_cancelled_count
                FROM    mercury_florist_summary
                WHERE   florist_id IN
                                (select m.florist_id
                                 from   ftd_apps.florist_master m
                                 where  m.top_level_florist_id =
                                        (select top_level_florist_id
                                         from ftd_apps.florist_master
                                         where florist_id = in_florist_id)
                                )
                AND     summary_date >= v_calendar_date
                GROUP BY calendar_year
                UNION
                SELECT
                        'Fiscal Year' year_type,
                        fiscal_year year_number,
                        sum (sent_count) sent_count,
                        sum (sent_sales_amount) sent_sales_amount,
                        sum (filled_count) filled_count,
                        sum (filled_sales_amount) filled_sales_amount,
                        sum (forwarded_count) forwarded_count,
                        sum (rejected_count) rejected_count,
                        sum (cancelled_count) cancelled_count,
                        sum (sys_cancelled_count) sys_cancelled_count
                FROM    mercury_florist_summary
                WHERE   florist_id IN
                                (select m.florist_id
                                 from   ftd_apps.florist_master m
                                 where  m.top_level_florist_id =
                                        (select top_level_florist_id
                                         from ftd_apps.florist_master
                                         where florist_id = in_florist_id)
                                )
                AND     summary_date >= v_calendar_date
                GROUP BY fiscal_year
                ORDER BY year_type DESC, year_number DESC;

ELSE
        OPEN OUT_CUR FOR
                SELECT
                        'Calendar Year' year_type,
                        calendar_year year_number,
                        sum (sent_count) sent_count,
                        sum (sent_sales_amount) sent_sales_amount,
                        sum (filled_count) filled_count,
                        sum (filled_sales_amount) filled_sales_amount,
                        sum (forwarded_count) forwarded_count,
                        sum (rejected_count) rejected_count,
                        sum (cancelled_count) cancelled_count,
                        sum (sys_cancelled_count) sys_cancelled_count
                FROM    mercury_florist_summary
                WHERE   florist_id = in_florist_id
                AND     summary_date >= v_calendar_date
                GROUP BY calendar_year
                UNION
                SELECT
                        'Fiscal Year' year_type,
                        fiscal_year year_number,
                        sum (sent_count) sent_count,
                        sum (sent_sales_amount) sent_sales_amount,
                        sum (filled_count) filled_count,
                        sum (filled_sales_amount) filled_sales_amount,
                        sum (forwarded_count) forwarded_count,
                        sum (rejected_count) rejected_count,
                        sum (cancelled_count) cancelled_count,
                        sum (sys_cancelled_count) sys_cancelled_count
                FROM    mercury_florist_summary
                WHERE   florist_id = in_florist_id
                AND     summary_date >= v_calendar_date
                GROUP BY fiscal_year
                ORDER BY year_type DESC, year_number DESC;
END IF;

END VIEW_FLORIST_PERFORMANCE;



END MERCURY_FLORIST_PKG;
.
/
