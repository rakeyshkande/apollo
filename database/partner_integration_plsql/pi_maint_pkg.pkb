CREATE OR REPLACE PACKAGE BODY PTN_PI.PI_MAINT_PKG 
AS

PROCEDURE INSERT_PARTNER_ORDER_FEED
(
    IN_FEED_CONTENT 	IN PARTNER_ORDER_FEED.FEED_CONTENT%TYPE,
    OUT_ORDER_FEED_ID 	OUT VARCHAR,
    OUT_STATUS 			OUT VARCHAR,
    OUT_MESSAGE 		OUT VARCHAR 
)
AS
BEGIN
  OUT_ORDER_FEED_ID := PARTNER_ORDER_FEED_ID_SQ.nextval;
   INSERT INTO PARTNER_ORDER_FEED
    (
      PARTNER_ORDER_FEED_ID,
      FEED_CONTENT         ,
      ORDER_STATUS,
      CREATED_ON           ,
      CREATED_BY           ,
      UPDATED_ON           ,
      UPDATED_BY
    )
    VALUES
    (
      OUT_ORDER_FEED_ID,
      IN_FEED_CONTENT    ,
      'NEW',
      SYSDATE            ,
      'SYS'              ,
      SYSDATE            ,
      'SYS'
    );
  
	OUT_STATUS := 'Y';
	EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
		(
		  SQLERRM,1,256
		)
		;
	  END; -- end exception block
END INSERT_PARTNER_ORDER_FEED;
/** -------------------------------------------------------------- **/

PROCEDURE INSERT_PARTNER_ORDER
(
    IN_PARTNER_ORDER_NUMBER    	IN PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
    IN_PARTNER_ID          		IN PARTNER_ORDER.PARTNER_ID%TYPE,
    IN_MASTER_ORDER_NUMBER   	IN PARTNER_ORDER.MASTER_ORDER_NUMBER%TYPE,
    IN_PARTNER_ORDER_FEED_ID 	IN PARTNER_ORDER.PARTNER_ORDER_FEED_ID%TYPE,
    IN_PARTNER_XML             	IN PARTNER_ORDER.PARTNER_XML%TYPE,
    IN_FTD_XML               	IN PARTNER_ORDER.FTD_XML%TYPE,
    IN_ORDER_DATE            	IN PARTNER_ORDER.ORDER_DATE%TYPE,
    IN_ORDER_STATUS          	IN PARTNER_ORDER.ORDER_STATUS%TYPE,
    IN_CURRENCY_CODE          	IN PARTNER_ORDER.CURRENCY_CODE%TYPE,
    OUT_STATUS 					OUT VARCHAR,
    OUT_MESSAGE 				OUT VARCHAR
)
AS
BEGIN
  BEGIN
     INSERT INTO PARTNER_ORDER
      (
        PARTNER_ORDER_ID   ,
        PARTNER_ORDER_NUMBER   ,
        PARTNER_ID         ,
        MASTER_ORDER_NUMBER  ,
        PARTNER_ORDER_FEED_ID,
        PARTNER_XML            ,
        FTD_XML              ,
        ORDER_DATE           ,
        ORDER_STATUS         ,
        CURRENCY_CODE,
        CREATED_ON           ,
        CREATED_BY           ,
        UPDATED_ON           ,
        UPDATED_BY
      )
      VALUES
      (
        PTN_PI.PARTNER_ORDER_ID_SQ.nextval,
        IN_PARTNER_ORDER_NUMBER   ,
        IN_PARTNER_ID         ,
        IN_MASTER_ORDER_NUMBER  ,
        IN_PARTNER_ORDER_FEED_ID,
        IN_PARTNER_XML            ,
        IN_FTD_XML              ,
        IN_ORDER_DATE           ,
        IN_ORDER_STATUS         ,
        IN_CURRENCY_CODE,
        SYSDATE                 ,
        'SYS'                   ,
        SYSDATE                 ,
        'SYS'
      );
    
    OUT_STATUS := 'Y';
  END;
	EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
		(
		  SQLERRM,1,256
		)
		;
	  END; -- end exception block
END INSERT_PARTNER_ORDER;
/** -------------------------------------------------------------- **/



PROCEDURE INSERT_PARTNER_ORDER_DETAILS
(
	IN_PARTNER_ORDER_ITEM_NUMBER 		IN PARTNER_ORDER_DETAIL.PARTNER_ORDER_ITEM_NUMBER%TYPE,
	IN_PARTNER_ID      					IN PARTNER_ORDER_DETAIL.PARTNER_ID%TYPE,
	IN_PARTNER_ORDER_NUMBER      		IN PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
	IN_CONFIRMATION_NUMBER     			IN PARTNER_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
	IN_PRODUCT_ID             			IN PARTNER_ORDER_DETAIL.PRODUCT_ID%TYPE,
	IN_RETAIL_PRICE    					IN PARTNER_ORDER_DETAIL.RETAIL_PRICE%TYPE,
	IN_SALE_PRICE    					IN PARTNER_ORDER_DETAIL.SALE_PRICE%TYPE,
	IN_SHIPPING_AMT     				IN PARTNER_ORDER_DETAIL.SHIPPING_AMT%TYPE,
	IN_TAX_AMT          				IN PARTNER_ORDER_DETAIL.TAX_AMT%TYPE,
	IN_PLACEMENT        				IN PARTNER_ORDER_DETAIL.PLACEMENT%TYPE,
	IN_DISCOUNT_CODE    				IN PARTNER_ORDER_DETAIL.DISCOUNT_CODE%TYPE,
	IN_ADD_ON_RETAIL_PRICE      		IN PARTNER_ORDER_DETAIL.ADD_ON_RETAIL_PRICE%TYPE,
	IN_ADD_ON_SALE_PRICE      			IN PARTNER_ORDER_DETAIL.ADD_ON_SALE_PRICE%TYPE,
	IN_LEGACY_ID                		IN PARTNER_ORDER_DETAIL.LEGACY_ID%TYPE,
	OUT_STATUS 							OUT VARCHAR2,
	OUT_MESSAGE 						OUT VARCHAR2 
)
AS
  v_order_id number;
BEGIN
  
  select partner_order_id into v_order_id 
  from partner_order 
  where PARTNER_ID= IN_PARTNER_ID AND PARTNER_ORDER_NUMBER= IN_PARTNER_ORDER_NUMBER;
 
   INSERT INTO PARTNER_ORDER_DETAIL
	(
	  PARTNER_ORDER_DETAIL_ID,
	  PARTNER_ID,
	  PARTNER_ORDER_ID     ,
      PARTNER_ORDER_ITEM_NUMBER,      
	  CONFIRMATION_NUMBER    ,
      PRODUCT_ID,
      RETAIL_PRICE   ,
      SALE_PRICE,
      SHIPPING_AMT    ,      
      TAX_AMT         ,
      PLACEMENT,
      DISCOUNT_CODE,
      CREATED_ON      ,
      CREATED_BY      ,
      UPDATED_ON      ,
      UPDATED_BY,
      ADD_ON_RETAIL_PRICE,
      ADD_ON_SALE_PRICE,
      LEGACY_ID
)
VALUES
(
      PTN_PI.PARTNER_ORDER_DETAIL_ID_SQ.nextval,
      IN_PARTNER_ID,
      v_order_id,
      IN_PARTNER_ORDER_ITEM_NUMBER,      
	  IN_CONFIRMATION_NUMBER,
      IN_PRODUCT_ID,
      IN_RETAIL_PRICE   ,
      IN_SALE_PRICE,
      IN_SHIPPING_AMT    ,
      IN_TAX_AMT         ,
      IN_PLACEMENT,
      IN_DISCOUNT_CODE,
      SYSDATE            ,
      'SYS'              ,
      SYSDATE            ,
      'SYS',
      IN_ADD_ON_RETAIL_PRICE,
      IN_ADD_ON_SALE_PRICE,
      IN_LEGACY_ID
    );
  
	OUT_STATUS := 'Y';
	EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
		(
		  SQLERRM,1,256
		)
		;
	  END; -- end exception block
END INSERT_PARTNER_ORDER_DETAILS;
/** -------------------------------------------------------------- **/


PROCEDURE SAVE_FTD_ORDER_XML
(
	IN_PARTNER_ORDER_NUMBER IN PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
	IN_FTD_XML            	IN PARTNER_ORDER.FTD_XML%TYPE,
	IN_ORDER_STATUS      	IN PARTNER_ORDER.ORDER_STATUS%TYPE,
	OUT_STATUS 				OUT VARCHAR2,
	OUT_MESSAGE 			OUT VARCHAR2
)
AS
BEGIN
   UPDATE PARTNER_ORDER
  SET FTD_XML                = IN_FTD_XML     ,
    ORDER_STATUS             = IN_ORDER_STATUS,
    UPDATED_BY               = 'SYS'          ,
    UPDATED_ON               = sysdate
    WHERE PARTNER_ORDER_NUMBER = IN_PARTNER_ORDER_NUMBER;
  
  OUT_STATUS := 'Y';
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    OUT_STATUS  := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END; -- end exception block
END SAVE_FTD_ORDER_XML;
/** -------------------------------------------------------------- **/


PROCEDURE UPDATE_ORDER_FEED_STATUS
(
    IN_PARTNER_ORDER_FEED_ID IN PARTNER_ORDER_FEED.PARTNER_ORDER_FEED_ID%TYPE,
    IN_ORDER_STATUS       IN PARTNER_ORDER_FEED.ORDER_STATUS%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2 
)
AS
BEGIN
   UPDATE PARTNER_ORDER_FEED
   SET ORDER_STATUS           = IN_ORDER_STATUS,
    UPDATED_BY               = 'SYS'          ,
    UPDATED_ON               = sysdate
    WHERE PARTNER_ORDER_FEED_ID = IN_PARTNER_ORDER_FEED_ID;
  
	OUT_STATUS := 'Y';
	EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	  END; -- end exception block
END UPDATE_ORDER_FEED_STATUS;
/** -------------------------------------------------------------- **/


PROCEDURE UPDATE_ORDER_STATUS
(
    IN_PARTNER_ID IN PARTNER_ORDER.PARTNER_ID%TYPE,
    IN_PARTNER_ORDER_NUMBER IN PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
    IN_ORDER_STATUS       IN PARTNER_ORDER.ORDER_STATUS%TYPE,
    OUT_STATUS OUT VARCHAR2,
    OUT_MESSAGE OUT VARCHAR2 
)
AS
BEGIN
   UPDATE PARTNER_ORDER
  SET ORDER_STATUS           = IN_ORDER_STATUS,
    UPDATED_BY               = 'SYS'          ,
    UPDATED_ON               = sysdate
    WHERE PARTNER_ID = IN_PARTNER_ID and PARTNER_ORDER_NUMBER = IN_PARTNER_ORDER_NUMBER;
  
	OUT_STATUS := 'Y';
	EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	  END; -- end exception block
END UPDATE_ORDER_STATUS;
/** -------------------------------------------------------------- **/

PROCEDURE PROCESS_PTN_REFUNDS
(
  IN_USER_NAME      IN      VARCHAR,
  IN_PARTNER_ID     IN      VARCHAR,
  OUT_STATUS        OUT     VARCHAR,
  OUT_MESSAGE       OUT     VARCHAR
) AS
/*------------------------------------------------------------------------------
  DESCRIPTION:
        THIS PROCEDURE IS TO GET ALL THE REFUNDS APPLIED ON PARTNER ORDERS AND
        INSERT NEW PARTNER ORDER ADJUTMENT RECORDS.
--------------------------------------------------------------------------------*/
OUTSTATUS 		  VARCHAR2(1);
OUTMESSAGE 		  VARCHAR2(100);
ORDER_COUNT 	  INTEGER;

	CURSOR REFUND_CUR IS
		SELECT R.REFUND_ID, OD.EXTERNAL_ORDER_NUMBER, OD.ORDER_DETAIL_ID, R.REFUND_DISP_CODE, R.REFUND_PRODUCT_AMOUNT,
			R.REFUND_ADDON_AMOUNT,R.REFUND_ADDON_DISCOUNT_AMT, R.REFUND_SERVICE_FEE, R.REFUND_SHIPPING_FEE, R.REFUND_SERVICE_FEE_TAX, R.REFUND_SHIPPING_TAX,
      R.REFUND_DISCOUNT_AMOUNT, R.REFUND_COMMISSION_AMOUNT, R.REFUND_WHOLESALE_AMOUNT, R.REFUND_TAX,
			(SELECT COUNT(*) FROM CLEAN.REFUND R2 WHERE R2.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID) AS REFUND_COUNT
		FROM CLEAN.REFUND R, CLEAN.ORDER_DETAILS OD, CLEAN.ORDERS O
			WHERE 1=1 AND (
				(IN_PARTNER_ID IS NOT NULL AND O.ORIGIN_ID = IN_PARTNER_ID)
				OR
				(O.ORIGIN_ID IN(SELECT FTD_ORIGIN_MAPPING FROM PTN_PI.PARTNER_MAPPING WHERE SEND_ORD_ADJ_FEED = 'Y'))
			)
			AND OD.ORDER_GUID = O.ORDER_GUID AND OD.ORDER_DISP_CODE IN ('Processed','Shipped','Printed','Validated')
			AND R.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID
			AND (R.REFUND_STATUS = 'Unbilled' OR ( R.REFUND_STATUS = 'Billed' AND R.UPDATED_BY <> 'PROCESS_PARTNER_REFUNDS' AND R.UPDATED_ON > (SYSDATE-2) AND NOT EXISTS (SELECT 1 FROM PTN_PI.PARTNER_ORDER_ADJUSTMENT ADJ WHERE ADJ.REFUND_ID=R.REFUND_ID)) )
			AND TRUNC(R.CREATED_ON) <= TRUNC(SYSDATE);


	CURSOR TAX_CUR(IN_EXTERNAL_ORDER_NUMBER VARCHAR2) IS
		SELECT POD.TAX_AMT PARTNER_TAX_AMT, POD.SHIPPING_AMT PARTNER_SHIPPING_AMT,
		NVL((SELECT SUM(REFUND_TAX) FROM CLEAN.REFUND R WHERE R.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID
			AND R.REFUND_STATUS IN ('Billed', 'Settled')), 0) PREVIOUS_TAX
		FROM PTN_PI.PARTNER_ORDER_DETAIL POD, CLEAN.ORDER_DETAILS OD
			WHERE POD.CONFIRMATION_NUMBER = OD.EXTERNAL_ORDER_NUMBER
			AND  OD.EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;


V_PARTNER_TAX_AMT             	NUMBER;
V_PARTNER_SHIPPING_AMT    		NUMBER;
V_PREVIOUS_TAX               	NUMBER;
V_TAX_TOTAL                  	NUMBER;
V_DIFF                       	NUMBER;

BEGIN

  FOR REFUND_REC IN REFUND_CUR LOOP

      OPEN TAX_CUR(REFUND_REC.EXTERNAL_ORDER_NUMBER);
      FETCH TAX_CUR INTO V_PARTNER_TAX_AMT, V_PARTNER_SHIPPING_AMT, V_PREVIOUS_TAX;
      CLOSE TAX_CUR;

      V_TAX_TOTAL := REFUND_REC.REFUND_TAX + V_PREVIOUS_TAX;

      IF (V_TAX_TOTAL > V_PARTNER_TAX_AMT) THEN
          V_DIFF := V_TAX_TOTAL - V_PARTNER_TAX_AMT;
          REFUND_REC.REFUND_TAX := REFUND_REC.REFUND_TAX - V_DIFF;
          REFUND_REC.REFUND_SHIPPING_TAX := V_DIFF;
      END IF;

      INSERT_PARTNER_ADJUSTMENT (
        IN_CONFIRMATION_NUMBER 		=> 	REFUND_REC.EXTERNAL_ORDER_NUMBER
        , IN_ADJUSTMENT_TYPE		  =>  'REFUND'
        , IN_ADJUSTMENT_REASON 		=> 	REFUND_REC.REFUND_DISP_CODE
        , IN_PRINCIPAL_AMT 			  => 	REFUND_REC.REFUND_PRODUCT_AMOUNT - REFUND_REC.REFUND_DISCOUNT_AMOUNT  
        , IN_ADDON_AMT            =>  REFUND_REC.REFUND_ADDON_AMOUNT
        , IN_SHIPPING_AMT 			  => 	REFUND_REC.REFUND_SHIPPING_FEE + REFUND_REC.REFUND_SERVICE_FEE
        , IN_TAX_AMT 				      => 	REFUND_REC.REFUND_TAX + REFUND_REC.REFUND_SERVICE_FEE_TAX + REFUND_REC.REFUND_SHIPPING_TAX
        , IN_REFUND_PRINCIPAL_AMT =>  REFUND_REC.REFUND_PRODUCT_AMOUNT - REFUND_REC.REFUND_DISCOUNT_AMOUNT
        , IN_REFUND_ADDON_AMT     =>  REFUND_REC.REFUND_ADDON_AMOUNT
        , IN_REFUND_ID 				    => 	REFUND_REC.REFUND_ID
        , IN_CREATED_BY 			    => 	IN_USER_NAME
        , IN_UPDATED_BY 			    => 	IN_USER_NAME
        , OUT_STATUS 				      => 	OUTSTATUS
        , OUT_MESSAGE 				    => 	OUTMESSAGE );

    IF OUTSTATUS  = 'Y' THEN

      -- UPDATE REFUNDS TABLE.
      CLEAN.REFUND_PKG.UPDATE_REFUND(
        IN_REFUND_STATUS	        =>  'Billed',
        IN_UPDATED_BY             =>  'PROCESS_PARTNER_REFUNDS',
        IN_REFUND_TAX					    =>  REFUND_REC.REFUND_TAX,
        IN_REFUND_SHIPPING_TAX    =>  REFUND_REC.REFUND_SHIPPING_TAX,
        IN_REFUND_ID					    =>  REFUND_REC.REFUND_ID,
        OUT_STATUS                =>  OUTSTATUS,
        OUT_MESSAGE               =>  OUTMESSAGE
      );

    END IF;

   IF OUTSTATUS  = 'Y' THEN

    -- UPDATE PAYMENTS TABLE.
      CLEAN.ORDER_MAINT_PKG.UPDATE_PAYMENTS_STATUS (
        IN_BILL_STATUS	=> 'Billed',
        IN_UPDATED_BY   => 'PROCESS_PARTNER_REFUNDS',
        IN_REFUND_ID		=>	REFUND_REC.REFUND_ID,
        OUT_STATUS      =>  OUTSTATUS,
        OUT_MESSAGE     =>  OUTMESSAGE
      );

	   END IF;

  END LOOP;

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN PROCESS_PTN_REFUNDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PROCESS_PTN_REFUNDS;

PROCEDURE INSERT_PARTNER_ADJUSTMENT
(
  IN_CONFIRMATION_NUMBER 		IN 	  VARCHAR2,
  IN_ADJUSTMENT_TYPE 			  IN 	  VARCHAR2,
  IN_ADJUSTMENT_REASON 			IN 	  VARCHAR2,
  IN_PRINCIPAL_AMT 				  IN 	  NUMBER,
  IN_ADDON_AMT              IN    NUMBER,
  IN_SHIPPING_AMT 				  IN 	  NUMBER,
  IN_TAX_AMT 					      IN  	NUMBER,
  IN_REFUND_PRINCIPAL_AMT 	IN  	NUMBER,
  IN_REFUND_ADDON_AMT 			IN  	NUMBER,
  IN_CREATED_BY 				    IN	  VARCHAR2,
  IN_UPDATED_BY 				    IN	  VARCHAR2,
  OUT_STATUS 					      OUT   VARCHAR2,
  OUT_MESSAGE 					    OUT   VARCHAR2
)AS
/*------------------------------------------------------------------------------
Insert partner order adjustments to partner_order_adjustment table.
Its a overloaded method which does not expect refund_id as input
--------------------------------------------------------------------------------*/
BEGIN

	INSERT_PARTNER_ADJUSTMENT (
        IN_CONFIRMATION_NUMBER 		=> 	IN_CONFIRMATION_NUMBER
        , IN_ADJUSTMENT_TYPE		  =>  IN_ADJUSTMENT_TYPE
        , IN_ADJUSTMENT_REASON 		=> 	IN_ADJUSTMENT_REASON
        , IN_PRINCIPAL_AMT 			  => 	IN_PRINCIPAL_AMT
        , IN_ADDON_AMT            =>  IN_ADDON_AMT
        , IN_SHIPPING_AMT 			  => 	IN_SHIPPING_AMT
        , IN_TAX_AMT 				      => 	IN_TAX_AMT
        , IN_REFUND_PRINCIPAL_AMT =>  IN_REFUND_PRINCIPAL_AMT
        , IN_REFUND_ADDON_AMT 		=>  IN_REFUND_ADDON_AMT
        , IN_REFUND_ID 				    => 	NULL
        , IN_CREATED_BY 			    => 	IN_CREATED_BY
        , IN_UPDATED_BY 			    => 	IN_UPDATED_BY
        , OUT_STATUS 				      => 	OUT_STATUS
        , OUT_MESSAGE 				    => 	OUT_MESSAGE );

END INSERT_PARTNER_ADJUSTMENT;


PROCEDURE INSERT_PARTNER_ADJUSTMENT
(
  IN_CONFIRMATION_NUMBER 		IN 	  VARCHAR2,
  IN_ADJUSTMENT_TYPE 			  IN 	  VARCHAR2,
  IN_ADJUSTMENT_REASON 			IN 	  VARCHAR2,
  IN_PRINCIPAL_AMT 				  IN 	  NUMBER,
  IN_ADDON_AMT              IN    NUMBER,
  IN_SHIPPING_AMT 				  IN 	  NUMBER,
  IN_TAX_AMT 					      IN  	NUMBER,
  IN_REFUND_PRINCIPAL_AMT   IN    NUMBER,
  IN_REFUND_ADDON_AMT       IN    NUMBER,
  IN_REFUND_ID						  IN    NUMBER,
  IN_CREATED_BY 				    IN	  VARCHAR2,
  IN_UPDATED_BY 				    IN	  VARCHAR2,
  OUT_STATUS 					      OUT   VARCHAR2,
  OUT_MESSAGE 					    OUT   VARCHAR2
) AS
/*------------------------------------------------------------------------------
Insert partner order adjustments to partner_order_adjustment table.
Its a overloaded method which expects refund_id as input
--------------------------------------------------------------------------------*/

CURSOR EXISTS_CUR IS
	SELECT  PARTNER_ORDER_DETAIL_ID FROM PTN_PI.PARTNER_ORDER_DETAIL WHERE CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

	V_PARTNER_ORDER_DETAIL_ID          PARTNER_ORDER_DETAIL.PARTNER_ORDER_DETAIL_ID%TYPE;
  V_IS_ALREADY_REMOVED                CHAR;

BEGIN

    OPEN EXISTS_CUR;
		FETCH EXISTS_CUR INTO V_PARTNER_ORDER_DETAIL_ID;
    CLOSE EXISTS_CUR;

    IF V_PARTNER_ORDER_DETAIL_ID IS NOT NULL THEN

    BEGIN
        SELECT 'Y' INTO V_IS_ALREADY_REMOVED FROM PTN_PI.PARTNER_ORDER_ADJUSTMENT
        WHERE ADJUSTMENT_REASON IN --(SELECT DISTINCT DISPOSITION_ID FROM SCRUB.DISPOSITION_CODES  WHERE   REMOVE_FLAG = 'Y')
        ('L','A','C','D','E','F','G','H','T','P','X')
        AND PARTNER_ORDER_DETAIL_ID = V_PARTNER_ORDER_DETAIL_ID;

          EXCEPTION WHEN NO_DATA_FOUND THEN
            V_IS_ALREADY_REMOVED := 'N';
      END;

      IF V_IS_ALREADY_REMOVED IS NULL OR V_IS_ALREADY_REMOVED = 'N' THEN

        INSERT INTO PARTNER_ORDER_ADJUSTMENT (
            PARTNER_ORDER_ADJUSTMENT_ID,
            PARTNER_ORDER_DETAIL_ID,
            ADJUSTMENT_TYPE,
            ADJUSTMENT_REASON,
            PRINCIPAL_AMT,
            ADDON_AMT,
            SHIPPING_AMT,
            TAX_AMT,
            REFUND_PRINCIPAL_AMT,
            REFUND_ADDON_AMT,
            STATUS,
            REFUND_ID,
            CREATED_BY,
            CREATED_ON,
            UPDATED_BY,
            UPDATED_ON
        ) VALUES (
          PTN_ORDER_ADJUSTMENT_ID_SQ.NEXTVAL,
          V_PARTNER_ORDER_DETAIL_ID,
          IN_ADJUSTMENT_TYPE,
          IN_ADJUSTMENT_REASON,
          IN_PRINCIPAL_AMT,
          IN_ADDON_AMT,
          IN_SHIPPING_AMT,
          IN_TAX_AMT,
          IN_REFUND_PRINCIPAL_AMT,
          IN_REFUND_ADDON_AMT,
          'NEW',
          IN_REFUND_ID,
          IN_CREATED_BY,
          SYSDATE,
          IN_UPDATED_BY,
          SYSDATE
        );
        END IF;
        OUT_STATUS := 'Y';

    ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'PARTNER_ORDER_DETAIL record not found for confirmation number ' || IN_CONFIRMATION_NUMBER;
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_PARTNER_ADJUSTMENT[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INSERT_PARTNER_ADJUSTMENT;

PROCEDURE UPDATE_PARTNER_FEED_ID (
  IN_FEED_ID              	IN 		PARTNER_FEEDS.FEED_ID%TYPE,
  IN_FEED_TYPE            	IN 		PARTNER_FEEDS.FEED_TYPE%TYPE,
  IN_FEED_STATUS          	IN 		PARTNER_FEEDS.STATUS%TYPE,
  IN_FEED_DATA_ID         	IN 		VARCHAR2,
  IN_UPDATED_BY           	IN 		VARCHAR2,
  OUT_STATUS              	OUT 	VARCHAR2,
  OUT_MESSAGE             	OUT 	VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
  DESCRIPTION:
  	THIS PROCEDURE IS RESPONSIBLE FOR SAVING THE FEED ID FOR A GIVEN 
	FEED DATA(ORDER ADJ, ORDER FULFILL) ID. FEED DATA ID CAN BE ONE AMONG 
	ORDER ADJUSTMENT ID, ORDER ACKNOWLEDGMENT ID, OR ORDER FULFILLMENT ID
  -----------------------------------------------------------------------------*/
BEGIN

  CASE IN_FEED_TYPE
  
        WHEN 'ADJUSTMENT' THEN 
          UPDATE PARTNER_ORDER_ADJUSTMENT
            SET FEED_ID                         = 	IN_FEED_ID,
            STATUS                  				    = 	IN_FEED_STATUS,
            UPDATED_BY                          = 	IN_UPDATED_BY,
            UPDATED_ON					  		          = 	SYSDATE
            WHERE PARTNER_ORDER_ADJUSTMENT_ID 	    = 	IN_FEED_DATA_ID;
    
            OUT_STATUS      := 'Y';
			
        WHEN 'FULFILLMENT' THEN        
          UPDATE PARTNER_ORDER_FULFILLMENT
            SET FEED_ID                     		= 	IN_FEED_ID,
            STATUS                          		= 	IN_FEED_STATUS,
            UPDATED_BY                      		= 	IN_UPDATED_BY,
            UPDATED_ON					  			        = 	SYSDATE
            WHERE PARTNER_ORDER_FULFILLMENT_ID 	= 	IN_FEED_DATA_ID;
    
            OUT_STATUS      := 'Y';     
			
		END CASE;
		
 EXCEPTION
    WHEN OTHERS THEN
      BEGIN
        OUT_STATUS  := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PARTNER_FEED_ID [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      END; 
END UPDATE_PARTNER_FEED_ID;

PROCEDURE INSERT_FEED_TO_FEEDMASTER
(
    IN_FEED_XML     	      IN 	  PARTNER_FEEDS.FEED_XML%TYPE,
    IN_FEED_TYPE   		      IN 	  PARTNER_FEEDS.FEED_TYPE%TYPE,
    IN_FEED_STATUS 		      IN 	  PARTNER_FEEDS.STATUS%TYPE,
    OUT_FEED_ID 		        OUT   PARTNER_FEEDS.FEED_ID%TYPE,
    OUT_STATUS 			        OUT   VARCHAR2,
    OUT_MESSAGE 		        OUT   VARCHAR2  
)
AS
  /*-----------------------------------------------------------------------------
  DESCRIPTION:
  THIS PROCEDURE IS RESPONSIBLE FOR SAVING THE FEED XML TO DB:
  FEED_XML             			CLOB
  FEED_TYPE              		VARCHAR2
  STATUS      					VARCHAR2
  OUTPUT
  FEED_ID       				NUMBER
  STATUS                       	VARCHAR2 (Y OR N)
  MESSAGE                      	VARCHAR2 (ERROR MESSAGE)
  -----------------------------------------------------------------------------*/
  V_FEED_ID NUMBER;
  
BEGIN

  V_FEED_ID 	:= 			PARTNER_FEED_ID_SQ.NEXTVAL;
  
   INSERT INTO PTN_PI.PARTNER_FEEDS
    (FEED_ID, FEED_XML, FEED_TYPE, STATUS, CREATED_BY, CREATED_ON, UPDATED_BY, UPDATED_ON) 
    VALUES 
    (V_FEED_ID, IN_FEED_XML, IN_FEED_TYPE, IN_FEED_STATUS, 'SYS', SYSDATE, 'SYS', SYSDATE);
  
	OUT_STATUS  := 'Y';
	OUT_FEED_ID := V_FEED_ID;
	
EXCEPTION
	WHEN OTHERS THEN
	BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_FEED_TO_FEEDMASTER [' || SQLCODE || '] ' || SUBSTR(SQLERRM,1,256);
	END; 
	
END INSERT_FEED_TO_FEEDMASTER;

PROCEDURE INSERT_FLORIST_FULFILL_DATA
(
  IN_PARTNER_ID                 IN  VARCHAR2,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
) AS

/*-----------------------------------------------------------------------------
DESCRIPTION:
        THIS PROCEDURE IS RESPONSIBLE FOR INSERTING ALL THE FLORIST DELIVERED 
        PARTNER ORDERS FOR TODAY THAT ARE STILL LIVE
OUTPUT:
        OUT_STATUS                  VARCHAR2 (Y OR N)
        OUT_MESSAGE                 VARCHAR2 (ERROR MESSAGE)
-----------------------------------------------------------------------------*/

CURSOR GET_ORD_ITEM_NUMBERS IS

	SELECT DISTINCT POD.PARTNER_ORDER_DETAIL_ID, OD.DELIVERY_DATE
		FROM PTN_PI.PARTNER_ORDER_DETAIL POD
		JOIN CLEAN.ORDER_DETAILS OD ON POD.CONFIRMATION_NUMBER = OD.EXTERNAL_ORDER_NUMBER
		JOIN MERCURY.MERCURY M ON TO_CHAR(OD.ORDER_DETAIL_ID) = M.REFERENCE_NUMBER AND M.MERCURY_STATUS IN ('MC','MN','MM')
		AND M.MSG_TYPE = 'FTD' AND (M.DELIVERY_DATE) <= TRUNC(SYSDATE)
		AND NOT EXISTS (SELECT 'Y' FROM MERCURY.MERCURY M2 
			WHERE M2.MERCURY_ORDER_NUMBER = M.MERCURY_ORDER_NUMBER
			AND M2.MSG_TYPE IN ('CAN', 'REJ')
			AND M2.MERCURY_STATUS IN ('MC','MN','MM'))
			AND NOT EXISTS (SELECT PARTNER_ORDER_DETAIL_ID FROM PTN_PI.PARTNER_ORDER_FULFILLMENT POF
			WHERE POF.PARTNER_ORDER_DETAIL_ID = POD.PARTNER_ORDER_DETAIL_ID)
      		AND ( (IN_PARTNER_ID IS NOT NULL AND POD.PARTNER_ID = IN_PARTNER_ID) OR IN_PARTNER_ID IS NULL)
			AND (POD.PARTNER_ID IN (SELECT FTD_ORIGIN_MAPPING FROM PTN_PI.PARTNER_MAPPING WHERE SEND_DCON_FEED='N' and SEND_ORD_FMT_FEED = 'Y'));

BEGIN

	FOR REC IN GET_ORD_ITEM_NUMBERS LOOP
		INSERT INTO PTN_PI.PARTNER_ORDER_FULFILLMENT
			(PARTNER_ORDER_FULFILLMENT_ID, PARTNER_ORDER_DETAIL_ID, DELIVERY_DATE, SHIPPING_METHOD, CARRIER_NAME,
			TRACKING_NUMBER, STATUS, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY )
		VALUES 
    (PTN_PI.PTN_ORDER_FULFILLMENT_ID_SQ.NEXTVAL, REC.PARTNER_ORDER_DETAIL_ID, REC.DELIVERY_DATE, 'SameDay',
			'Florist Delivered', NULL, 'NEW', SYSDATE, 'SYS', SYSDATE, 'SYS');
	END LOOP;
    
	OUT_STATUS := 'Y';
	
	EXCEPTION WHEN OTHERS THEN
		BEGIN
			OUT_STATUS := 'N';
			OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_FLORIST_FULFILL_DATA [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
		END;
		
END INSERT_FLORIST_FULFILL_DATA;

PROCEDURE UPDATE_PTN_FEED_STATUS (
IN_FEED_ID              	IN 		PARTNER_FEEDS.FEED_ID%TYPE,
IN_FEED_STATUS          	IN 		PARTNER_FEEDS.STATUS%TYPE,
IN_UPDATED_BY           	IN 		VARCHAR2,
OUT_STATUS              	OUT 	VARCHAR2,
OUT_MESSAGE             	OUT 	VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
  DESCRIPTION:
  	THIS PROCEDURE IS RESPONSIBLE FOR SAVING THE FEED STATUS AS RETURNED BY ESB
  -----------------------------------------------------------------------------*/
BEGIN

  
			UPDATE PARTNER_FEEDS
			  SET STATUS      = 	IN_FEED_STATUS,
			  UPDATED_BY      = 	IN_UPDATED_BY,
			  UPDATED_ON			= 	SYSDATE
			  WHERE FEED_ID 	= 	IN_FEED_ID;
    
        OUT_STATUS      := 'Y';
			       	
 EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PTN_FEED_STATUS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	  END; 
END UPDATE_PTN_FEED_STATUS;

PROCEDURE INSERT_PTN_DROPSHIP_CONF
(
  IN_VENUS_ORDER_NUM            	IN    VENUS.VENUS.VENUS_ORDER_NUMBER%TYPE,
  IN_DELIVERY_DATE			        IN    PTN_PI.PARTNER_ORDER_FULFILLMENT.DELIVERY_DATE%TYPE,
  IN_SHIPPING_METHOD		        IN    PTN_PI.PARTNER_ORDER_FULFILLMENT.SHIPPING_METHOD%TYPE,
  IN_CARRIER_NAME		            IN    PTN_PI.PARTNER_ORDER_FULFILLMENT.CARRIER_NAME%TYPE,
  IN_TRACKING_NUMBER		        IN    PTN_PI.PARTNER_ORDER_FULFILLMENT.TRACKING_NUMBER%TYPE,
  OUT_STATUS                    	OUT   VARCHAR2,
  OUT_MESSAGE                   	OUT   VARCHAR2
)

AS
/*-----------------------------------------------------------------------------

Name:    INSERT_DROPSHIP_CONFIRMATION
Type:    Procedure
Syntax:  INSERT_DROPSHIP_CONFIRMATIONs(venus_order_number, ship_date, 
          shipping_method,carrier_name,tracking_number)
Description:
        This stored procedure inserts a record into 
        table ptn_pi.partner_order_fulfillment
Input:
        venus_order_number varchar2,ship_date	date,shipping_method shipping_method, 
        carrier_name	carrier_name, tracking_number	tracking_number
Output:
        status              varchar2
        error message       varchar2
-----------------------------------------------------------------------------*/
v_ship_method  varchar2(20);

CURSOR GET_ITEM_NUMBERS IS
    SELECT distinct pod.partner_order_detail_id
    FROM venus.venus v
    JOIN clean.order_details od ON od.order_detail_id = TO_NUMBER(v.REFERENCE_NUMBER)
    JOIN ptn_pi.partner_order_detail pod ON pod.confirmation_number = od.external_order_number
    WHERE v.venus_order_number = IN_VENUS_ORDER_NUM
    AND v.msg_type = 'FTD'
    AND not exists (SELECT partner_order_item_number FROM ptn_pi.partner_order_fulfillment pof
        where pof.partner_order_detail_id = pod.partner_order_detail_id);

BEGIN

select description into v_ship_method from ftd_apps.ship_methods where ship_method_id = in_shipping_method;

IF SQL%NOTFOUND THEN
	v_ship_method := in_shipping_method;
END IF;

FOR rec in GET_ITEM_NUMBERS LOOP
    insert into ptn_pi.partner_order_fulfillment
        (partner_order_fulfillment_id,
         partner_order_detail_id,
         delivery_date,
         shipping_method,
         carrier_name,
         tracking_number,
         status,
         created_on,
         created_by,
         updated_on,
         updated_by)
    values(
        ptn_pi.ptn_order_fulfillment_id_sq.nextval,
        rec.partner_order_detail_id,
        in_delivery_date,
        v_ship_method,
        'Vendor Delivered',
        in_tracking_number,
        'NEW',
        sysdate,
        'SYS',
        sysdate,
        'SYS');

   END LOOP;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;

END INSERT_PTN_DROPSHIP_CONF;


PROCEDURE UPDATE_FEED_STATUS_BY_DATE
(
IN_FEED_TYPE		            IN VARCHAR2,
IN_START_DATE					      IN VARCHAR2,
IN_END_DATE						      IN VARCHAR2,
IN_PARTNER_ID						    IN VARCHAR2,
IN_UPDATED_BY               IN VARCHAR2,
OUT_STATUS                  OUT VARCHAR2,
OUT_MESSAGE                 OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------

Name:    UPDATE_FEED_STATUS_BY_DATE
Type:    Procedure
Syntax:  UPDATE_FEED_STATUS_BY_DATE(IN_FEED_TYPE, IN_START_DATE, 
          IN_END_DATE,IN_PARTNER_ID)
Description:
        This stored procedure update the status from 'SENT' to 'NEW' into 
        table ptn_pi.PARTNER_FEEDS
Input:
        FEED_TYPE (Adjustment/fullfillment)		            IN VARCHAR2,
		START_DATE					      					IN VARCHAR2,
		END_DATE						      				IN VARCHAR2,
		PARTNER_ID	(LG/NJ)					    				IN VARCHAR2,
Output:
        OUT_STATUS              varchar2
        OUT_MESSAGE       varchar2
-----------------------------------------------------------------------------*/

UPDATED_ROWS number;

BEGIN
        
        UPDATE PTN_PI.PARTNER_FEEDS SET STATUS = 'NEW' ,  updated_by =  IN_UPDATED_BY ,	updated_on = SYSDATE 
        WHERE STATUS='SENT'
        AND FEED_TYPE = IN_FEED_TYPE 
        AND  FEED_XML LIKE  '%<PartnerId>'||IN_PARTNER_ID||'</PartnerId>%'
		-- AND UPDATED_ON > TO_DATE(IN_START_DATE, 'MM/DD/YYYY HH24:MI:SS')
        -- AND UPDATED_ON < TO_DATE(IN_END_DATE,'MM/DD/YYYY HH24:MI:SS');
		AND TRUNC(UPDATED_ON) BETWEEN 
									TRUNC(TO_DATE(IN_START_DATE,'MM/DD/YYYY  HH24:MI:SS')) 
									AND 
									TRUNC(TO_DATE(IN_END_DATE,'MM/DD/YYYY  HH24:MI:SS'));
      UPDATED_ROWS := sql%rowcount;


OUT_STATUS := 'Y';
OUT_MESSAGE:= UPDATED_ROWS || ' Feeds got updated successfully for Partner Feed Type: ' || IN_FEED_TYPE || ' for Partner : ' || IN_PARTNER_ID;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED :' || SUBSTR (SQLERRM,1,256);

END UPDATE_FEED_STATUS_BY_DATE;


PROCEDURE UPDATE_PRTNR_ORD_FEED_STATUS
(
IN_PARTNER_ORDER_NUMBERS IN VARCHAR2,
IN_FEED_TYPE  IN VARCHAR2,
IN_PARTNER_ID IN VARCHAR2,
IN_UPDATED_BY IN VARCHAR2,
OUT_STATUS    OUT VARCHAR2,
OUT_MESSAGE   OUT VARCHAR2,
OUT_INVALID_ORD_NUM_CURSOR OUT TYPES.REF_CURSOR 
)

AS
/*-----------------------------------------------------------------------------

Name:    UPDATE_PRTNR_ORD_FEED_STATUS
Type:    Procedure
Syntax:  UPDATE_PRTNR_ORD_FEED_STATUS(IN_PARTNER_ORDER_NUMBERS,IN_FEED_TYPE,  
          IN_PARTNER_ID,IN_UPDATED_BY)
Description:
        This stored procedure update the status from 'SENT' to 'NEW' into 
        table ptn_pi.PARTNER_FEEDS based on the input Passed.
Input:
        FEED_TYPE (Adjustment/fulfillment)		            IN VARCHAR2,
		PARTNER_ORDER_NUMBER(list of partner order number)		IN VARCHAR2,
		UPDATED_BY 						      				IN VARCHAR2,
		PARTNER_ID	(LG/NJ)					    				IN VARCHAR2,
Output:
        OUT_STATUS          varchar2
        OUT_MESSAGE       varchar2
        OUT_INVALID_ORD_NUM_CURSOR    VARCHAR2
-----------------------------------------------------------------------------*/
V_PARTNER_FEED_ID VARCHAR2(500);
V_SQL VARCHAR2(20000);
V_UPDATE_SQL VARCHAR2(20000);
BEGIN
-- dbms_output.put_line('IN_PARTNER_ORDER_NUMBERS :' || IN_PARTNER_ORDER_NUMBERS );
IF IN_FEED_TYPE = 'ADJUSTMENT' THEN

V_SQL := 'SELECT distinct(po.partner_order_number) as valid_partner_number 
			  FROM PTN_PI.PARTNER_ORDER po,PTN_PI.partner_order_detail pod,PTN_PI.partner_order_adjustment poa
			  where po.partner_order_id = pod.partner_order_id
			  AND pod.partner_order_detail_id = poa.partner_order_detail_id
			  AND po.partner_order_number IN (' || IN_PARTNER_ORDER_NUMBERS || ')
			  AND po.partner_id = ''' || IN_PARTNER_ID || '''';
        
-- dbms_output.put_line('v_sql :' || V_SQL );

OPEN OUT_INVALID_ORD_NUM_CURSOR FOR V_SQL;

          
     V_UPDATE_SQL := 'UPDATE PTN_PI.partner_feeds pf	SET pf.status =''NEW'',pf.updated_by = '''|| IN_UPDATED_BY || ''',	pf.updated_on = SYSDATE
						WHERE pf.status =''SENT'' AND pf.feed_id IN	(SELECT poa.feed_id
						FROM PTN_PI.PARTNER_ORDER po,PTN_PI.partner_order_detail pod,PTN_PI.partner_order_adjustment poa
						where po.partner_order_id = pod.partner_order_id
						AND pod.partner_order_detail_id = poa.partner_order_detail_id
						AND po.partner_order_number IN ( '|| IN_PARTNER_ORDER_NUMBERS || ') AND po.partner_id = '''|| IN_PARTNER_ID || ''')';
            --   	dbms_output.put_line(' V_UPDATE_SQL :' || V_UPDATE_SQL ); 
                
          Execute Immediate v_update_sql;

ELSIF (IN_FEED_TYPE = 'FULFILLMENT') OR (IN_FEED_TYPE = 'DCON') THEN

	V_SQL :=  'SELECT distinct(po.partner_order_number) as valid_partner_number
	FROM PTN_PI.PARTNER_ORDER po,PTN_PI.partner_order_detail pod,PTN_PI.partner_order_fulfillment pof
	where po.partner_order_id = pod.partner_order_id
	AND pod.partner_order_detail_id = pof.partner_order_detail_id
	 AND po.partner_order_number IN (' || IN_PARTNER_ORDER_NUMBERS || ') 
    AND po.partner_id = ''' || IN_PARTNER_ID || '''
   AND ((''' || IN_FEED_TYPE || '''= ''FULFILLMENT'' AND pof.DELIVERY_STATUS_DATETIME is null )
        OR( '''|| IN_FEED_TYPE || ''' = ''DCON'' AND pof.DELIVERY_STATUS_DATETIME is not null ))';
	-- dbms_output.put_line('v_sql :' || V_SQL );
  
  V_UPDATE_SQL := 'UPDATE PTN_PI.partner_feeds pf
		SET pf.status =''NEW'',
			pf.updated_by = ''' || IN_UPDATED_BY || ''',
			pf.updated_on = SYSDATE
		WHERE pf.status =''SENT''
		AND pf.feed_type=''' || IN_FEED_TYPE || '''
		AND pf.feed_id IN
			  (SELECT pof.feed_id
			  FROM PTN_PI.PARTNER_ORDER po,PTN_PI.partner_order_detail pod,PTN_PI.partner_order_fulfillment pof
			  where po.partner_order_id = pod.partner_order_id
			  AND pod.partner_order_detail_id = pof.partner_order_detail_id
			  AND po.partner_order_number IN ( ' || IN_PARTNER_ORDER_NUMBERS || ') AND po.partner_id = ''' || IN_PARTNER_ID || ''')';
     --  	dbms_output.put_line('v_sql :' || V_UPDATE_SQL );
        
	Execute Immediate v_update_sql;
  
  
  OPEN OUT_INVALID_ORD_NUM_CURSOR FOR V_SQL;
  


END IF;

	OUT_STATUS := 'Y';
	OUT_MESSAGE := 'Attempted to Update Records for Partner Feed Type: ' || IN_FEED_TYPE || ' and Partner: ' || IN_PARTNER_ID ||' (Valid Records would be updated, Invalid Records would not)';
  	
EXCEPTION WHEN OTHERS THEN
	
	OUT_STATUS := 'N';
	OUT_MESSAGE := 'ERROR OCCURRED IN : ' || SQLCODE  || SUBSTR (SQLERRM,1,256);
	
CLOSE OUT_INVALID_ORD_NUM_CURSOR;

END UPDATE_PRTNR_ORD_FEED_STATUS;

PROCEDURE INSERT_PTN_FLORIST_DCON_DATA
(
  IN_ORDER_DETAIL_ID           IN  CLEAN.ORDER_DETAILS.order_detail_id%TYPE,
  IN_DELIVERY_STATUS           IN  VARCHAR2,
  IN_DELIVERY_STATUS_DATETIME   IN CLEAN.ORDER_DETAILS.DELIVERY_DATE%TYPE,
  OUT_STATUS                    OUT VARCHAR2,
  OUT_MESSAGE                   OUT VARCHAR2
) 
AS
/*-----------------------------------------------------------------------------
DESCRIPTION:
        THIS PROCEDURE IS RESPONSIBLE FOR INSERTING ALL THE FLORIST DELIVERY CONFIRMATION
        PARTNER DATA FOR THOSE PARTNERS OPTED IN FOR SEND_DCON_FEED
      
OUTPUT:
        OUT_STATUS                  VARCHAR2 (Y OR N)
        OUT_MESSAGE                 VARCHAR2 (ERROR MESSAGE)
-----------------------------------------------------------------------------*/
V_PARTNER_ORDER_DETAIL_ID PTN_PI.PARTNER_ORDER_DETAIL.PARTNER_ORDER_DETAIL_ID%TYPE;
V_DELIVERY_DATE CLEAN.ORDER_DETAILS.DELIVERY_DATE%TYPE;
BEGIN
 select POD.PARTNER_ORDER_DETAIL_ID,OD.DELIVERY_DATE INTO V_PARTNER_ORDER_DETAIL_ID, V_DELIVERY_DATE
  from CLEAN.ORDER_DETAILS OD, PTN_PI.PARTNER_ORDER_DETAIL POD 
  where POD.CONFIRMATION_NUMBER = OD.EXTERNAL_ORDER_NUMBER 
  and pod.partner_id in (SELECT FTD_ORIGIN_MAPPING FROM PTN_PI.PARTNER_MAPPING WHERE SEND_DCON_FEED='Y')
  and 0 = (select count(1) from PTN_PI.PARTNER_ORDER_FULFILLMENT where partner_order_detail_id = POD.PARTNER_ORDER_DETAIL_ID)
  and od.order_detail_id = IN_ORDER_DETAIL_ID;

  if V_PARTNER_ORDER_DETAIL_ID is not Null and V_DELIVERY_DATE is not null then
    INSERT INTO PTN_PI.PARTNER_ORDER_FULFILLMENT
    (PARTNER_ORDER_FULFILLMENT_ID, PARTNER_ORDER_DETAIL_ID, DELIVERY_DATE, SHIPPING_METHOD, CARRIER_NAME,
    TRACKING_NUMBER, STATUS, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY,DELIVERY_STATUS,DELIVERY_STATUS_DATETIME )
    VALUES 
    (PTN_PI.PTN_ORDER_FULFILLMENT_ID_SQ.NEXTVAL, V_PARTNER_ORDER_DETAIL_ID, V_DELIVERY_DATE, 'SameDay',
    'Florist Delivered', NULL, 'NEW', SYSDATE, 'SYS', SYSDATE, 'SYS',IN_DELIVERY_STATUS,IN_DELIVERY_STATUS_DATETIME);
  END IF;
  OUT_STATUS := 'Y';
  OUT_MESSAGE := 'Record inserted into partner_order_fulfillment table successfully. Order detail id: '||IN_ORDER_DETAIL_ID;
  EXCEPTION  
  WHEN no_data_found THEN
  	BEGIN
      OUT_STATUS := 'Y';
      OUT_MESSAGE := 'NOT A PARTNER ORDER OR SEND_DCON_FEED NOT ENABLED OR DUPLICATE DCON ENTRY';
    END;
  WHEN OTHERS THEN
    BEGIN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_PARTNER_FLORIST_DCON_DATA [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
		
END INSERT_PTN_FLORIST_DCON_DATA;

PROCEDURE INSERT_PARTNER_ORDER_ADD_ONS
(
	IN_PARTNER_ORDER_DETAIL_ID 			IN PARTNER_ORDER_ADD_ONS.PARTNER_ORDER_DETAIL_ID%TYPE,
	IN_ADD_ON_ID      				  	IN PARTNER_ORDER_ADD_ONS.ADD_ON_ID%TYPE,
  	IN_ADD_ON_RETAIL_PRICE        		IN PARTNER_ORDER_ADD_ONS.ADD_ON_RETAIL_PRICE%TYPE,
  	IN_ADD_ON_SALE_PRICE          		IN PARTNER_ORDER_ADD_ONS.ADD_ON_SALE_PRICE%TYPE,
	IN_QUANTITY              			IN PARTNER_ORDER_ADD_ONS.QUANTITY%TYPE,
	IN_CREATED_BY            			IN PARTNER_ORDER_ADD_ONS.CREATED_BY%TYPE,
	IN_UPDATED_BY       				IN PARTNER_ORDER_ADD_ONS.UPDATED_BY%TYPE,
	OUT_STATUS 							OUT VARCHAR2,
	OUT_MESSAGE 						OUT VARCHAR2
)
AS
  v_order_id number;
BEGIN

   INSERT INTO PARTNER_ORDER_ADD_ONS
	(
      PARTNER_ORDER_DETAIL_ID,
      ADD_ON_ID,
      ADD_ON_RETAIL_PRICE,
      ADD_ON_SALE_PRICE,
      QUANTITY,
      CREATED_ON,
      CREATED_BY,
      UPDATED_ON,
      UPDATED_BY
      
)
VALUES
(
      IN_PARTNER_ORDER_DETAIL_ID,
      IN_ADD_ON_ID,
      IN_ADD_ON_RETAIL_PRICE,
      IN_ADD_ON_SALE_PRICE,
      IN_QUANTITY,
      SYSDATE,
      IN_CREATED_BY,
      SYSDATE,
      IN_UPDATED_BY
    );

	OUT_STATUS := 'Y';
	EXCEPTION
	WHEN OTHERS THEN
	  BEGIN
		OUT_STATUS  := 'N';
		OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR
		(
		  SQLERRM,1,256
		)
		;
	  END; -- end exception block
END INSERT_PARTNER_ORDER_ADD_ONS;

PROCEDURE INSERT_ORD_STATUS_UPD_REF
(
	IN_ORDER_NUM 			          IN ORDER_STATUS_UPDATE.CONFIRMATION_NUMBER%TYPE,	  
  IN_FEED_ID                  IN ORDER_STATUS_UPDATE.FEED_ID%TYPE,
  IN_ORD_STATUS_TYPE          IN ORDER_STATUS_UPDATE.TYPE%TYPE,
  OUT_ORD_STATUS_UPDATE_ID    OUT VARCHAR,
	OUT_STATUS 						    	OUT VARCHAR2,
	OUT_MESSAGE 						    OUT VARCHAR2
)
AS

BEGIN
   OUT_ORD_STATUS_UPDATE_ID := ORD_STATUS_UPD_REF_SQ.nextval;
   
INSERT INTO ORDER_STATUS_UPDATE
	(
      ORDER_STATUS_UPDATE_ID,
      CONFIRMATION_NUMBER,
      FEED_ID,
      TYPE,     
      CREATED_ON,
      CREATED_BY,
      UPDATED_ON,
      UPDATED_BY
)
VALUES
(
      OUT_ORD_STATUS_UPDATE_ID,
      IN_ORDER_NUM,
      IN_FEED_ID,
      IN_ORD_STATUS_TYPE,      
      SYSDATE,
      'SYS',
      SYSDATE,
      'SYS'
    );

	OUT_STATUS := 'Y';
	EXCEPTION WHEN OTHERS THEN 
    BEGIN
      OUT_STATUS  := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR( SQLERRM,1,256);
	  END; -- end exception block
    
END INSERT_ORD_STATUS_UPD_REF;

END PI_MAINT_PKG;

.
/