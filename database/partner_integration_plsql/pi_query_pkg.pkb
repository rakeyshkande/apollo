CREATE OR REPLACE PACKAGE BODY PTN_PI.PI_QUERY_PKG 
AS


PROCEDURE GET_PARTNER_ORDER_FEED_XML
(
   IN_ORDER_FEED_ID     IN  NUMBER,
   OUT_CUR              OUT TYPES.REF_CURSOR
)
AS
BEGIN

	OPEN OUT_CUR FOR
	select FEED_CONTENT
	from PARTNER_ORDER_FEED
	where PARTNER_ORDER_FEED_ID = IN_ORDER_FEED_ID;

END GET_PARTNER_ORDER_FEED_XML;
/** -------------------------------------------------------------- **/


PROCEDURE GET_PARTNER_MAPPING
(
	IN_PARTNER_ID 				IN PARTNER_MAPPING.PARTNER_ID%TYPE,
	OUT_CUR                     OUT TYPES.REF_CURSOR,
	OUT_STATUS                  OUT VARCHAR2,
	OUT_MESSAGE                 OUT VARCHAR2
)

AS

BEGIN

	OPEN OUT_CUR FOR
	SELECT *
	FROM PARTNER_MAPPING 
	WHERE PARTNER_ID=IN_PARTNER_ID;

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
	BEGIN
	  OUT_STATUS := 'N';
	  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;  -- end exception block
	
END GET_PARTNER_MAPPING;
/* --------------------------------------------------------------------------------------*/

PROCEDURE GET_PARTNER_ORDER_XML
(
   IN_PARTNER_ID 				IN PARTNER_ORDER.PARTNER_ID%TYPE,
   IN_PARTNER_ORDER_NUMBER     	IN PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
   OUT_CUR                		OUT TYPES.REF_CURSOR
)
AS
BEGIN

	OPEN OUT_CUR FOR
	select PARTNER_XML
	from PARTNER_ORDER
	where PARTNER_ID= IN_PARTNER_ID and PARTNER_ORDER_NUMBER = IN_PARTNER_ORDER_NUMBER;

END GET_PARTNER_ORDER_XML;
/* --------------------------------------------------------------------------------------*/


PROCEDURE GET_ORDER_CONFIRMATION_NUMBER
(
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS

BEGIN

	OPEN OUT_CUR FOR
	select LPAD ( TO_CHAR(ptn_pi.confirmation_number_sq.nextval), 10, '0') as confirmation_number 
	from dual;

END GET_ORDER_CONFIRMATION_NUMBER;
/* --------------------------------------------------------------------------------------*/

PROCEDURE CHECK_ORDER_EXISTS 
(
	IN_PARTNER_ID          		IN PARTNER_ORDER.PARTNER_ID%TYPE,
	IN_PARTNER_ORDER_NUMBER		IN PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
	OUT_EXISTS				 	OUT VARCHAR2,
	OUT_STATUS               	OUT VARCHAR2,
	OUT_MESSAGE              	OUT VARCHAR2
)
AS
	v_count integer;
BEGIN
	select count(*) into v_count 
	from partner_order 
	where PARTNER_ID=IN_PARTNER_ID and PARTNER_ORDER_NUMBER=IN_PARTNER_ORDER_NUMBER;
	
	IF v_count > 0 THEN 
		OUT_EXISTS := 'Y';
	ELSE
		OUT_EXISTS := 'N';
	END IF;
	
	OUT_STATUS := 'Y';
	
	EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
	
END CHECK_ORDER_EXISTS;
/* --------------------------------------------------------------------------------------*/


PROCEDURE CHECK_ORDER_ITEM_EXISTS
(
	IN_PARTNER_ID          			IN PARTNER_ORDER.PARTNER_ID%TYPE,
	IN_PARTNER_ORDER_ITEM_NUMBER	IN PARTNER_ORDER_DETAIL.PARTNER_ORDER_ITEM_NUMBER%TYPE,
	OUT_EXISTS				 		OUT VARCHAR2,
	OUT_STATUS               		OUT VARCHAR2,
	OUT_MESSAGE              		OUT VARCHAR2
)
AS
	v_count integer;
  
BEGIN
        
	select count(*) into v_count
	from PARTNER_ORDER_DETAIL
	where PARTNER_ID= IN_PARTNER_ID and PARTNER_ORDER_ITEM_NUMBER = IN_PARTNER_ORDER_ITEM_NUMBER;

	IF v_count > 0 THEN
		OUT_EXISTS := 'Y';
	ELSE
		OUT_EXISTS := 'N';
	END IF;

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
    BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END CHECK_ORDER_ITEM_EXISTS;
/* --------------------------------------------------------------------------------------*/

PROCEDURE GET_PARTNER_ORDER_INFO
(
	IN_PARTNER_ORDER_NUMBER                     IN PTN_PI.PARTNER_ORDER.PARTNER_ORDER_NUMBER%TYPE,
	IN_PARTNER_ORIGIN_ID                     IN PTN_PI.PARTNER_MAPPING.FTD_ORIGIN_MAPPING%TYPE,
	OUT_CUR                          OUT TYPES.REF_CURSOR,
	OUT_STATUS                  OUT VARCHAR2,
	OUT_MESSAGE                 OUT VARCHAR2
)
AS 

	V_SQL                            VARCHAR2(4000);
	V_COUNT                          NUMBER;

BEGIN

	V_COUNT := 0;

  	V_SQL := 'SELECT PO.PARTNER_ORDER_NUMBER, PO.MASTER_ORDER_NUMBER, POD.PARTNER_ORDER_DETAIL_ID, POD.CONFIRMATION_NUMBER, PM.DEFAULT_SOURCE_CODE
  	FROM PTN_PI.PARTNER_MAPPING PM, PTN_PI.PARTNER_ORDER PO, PTN_PI.PARTNER_ORDER_DETAIL POD
  	WHERE PO.PARTNER_ORDER_ID = POD.PARTNER_ORDER_ID AND PO.PARTNER_ID = POD.PARTNER_ID AND PM.PARTNER_ID = PO.PARTNER_ID
  	AND PO.PARTNER_ORDER_NUMBER = ''' || IN_PARTNER_ORDER_NUMBER ||'''';

BEGIN
  	SELECT COUNT(MASTER_ORDER_NUMBER) INTO V_COUNT FROM PTN_PI.PARTNER_ORDER WHERE PARTNER_ORDER_NUMBER = IN_PARTNER_ORDER_NUMBER;

  	IF V_COUNT = 0 THEN 
  
    	SELECT COUNT(CONFIRMATION_NUMBER) INTO V_COUNT FROM PTN_PI.PARTNER_ORDER_DETAIL WHERE PARTNER_ORDER_ITEM_NUMBER = IN_PARTNER_ORDER_NUMBER;
 
    	IF V_COUNT > 0 THEN
      		V_SQL := 'SELECT PO.PARTNER_ORDER_NUMBER, PO.MASTER_ORDER_NUMBER, POD.PARTNER_ORDER_DETAIL_ID, POD.CONFIRMATION_NUMBER, PM.DEFAULT_SOURCE_CODE
      		FROM PTN_PI.PARTNER_MAPPING PM, PTN_PI.PARTNER_ORDER PO, PTN_PI.PARTNER_ORDER_DETAIL POD
      		WHERE PO.PARTNER_ORDER_ID = POD.PARTNER_ORDER_ID AND PO.PARTNER_ID = POD.PARTNER_ID AND PM.PARTNER_ID = PO.PARTNER_ID
      		AND POD.PARTNER_ORDER_ITEM_NUMBER = ''' || IN_PARTNER_ORDER_NUMBER ||'''';
    	END IF;
  		
  	END IF;
 
    EXCEPTION WHEN OTHERS THEN
    	BEGIN
      		V_COUNT:=0;
    	END;
 	END;


	IF IN_PARTNER_ORIGIN_ID IS NOT NULL THEN
		V_SQL := V_SQL || ' AND PM.FTD_ORIGIN_MAPPING=''' || IN_PARTNER_ORIGIN_ID || '''';
	END IF;

	OPEN OUT_CUR FOR V_SQL;

	OUT_STATUS := 'Y';

	EXCEPTION WHEN OTHERS THEN
		BEGIN
  			OUT_STATUS := 'N';
  			OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
		END;  -- end exception block
		
END GET_PARTNER_ORDER_INFO;

/* --------------------------------------------------------------------------------------*/


PROCEDURE GET_PARTNER_ORIGIN_INFO
(
	IN_PARTNER_ORIGIN_ID        IN PTN_PI.PARTNER_MAPPING.FTD_ORIGIN_MAPPING%TYPE,
	IN_SOURCE_CODE        	    IN PTN_PI.PARTNER_MAPPING.DEFAULT_SOURCE_CODE%TYPE,
	OUT_CUR                     OUT TYPES.REF_CURSOR,
	OUT_STATUS                  OUT VARCHAR2,
	OUT_MESSAGE                 OUT VARCHAR2
)
AS

V_SQL                            VARCHAR2(4000);
V_COUNT                          NUMBER;
BEGIN

	V_SQL := 'SELECT * FROM PTN_PI.PARTNER_MAPPING WHERE 1=1';

	IF IN_PARTNER_ORIGIN_ID IS NOT NULL THEN
		V_SQL := V_SQL || ' AND FTD_ORIGIN_MAPPING=''' || IN_PARTNER_ORIGIN_ID || '''';
	END IF;
	
	IF IN_SOURCE_CODE IS NOT NULL THEN
		V_SQL := V_SQL || ' AND DEFAULT_SOURCE_CODE=''' || IN_SOURCE_CODE || '''';
	END IF;
	
        --dbms_output.put_line('v_sql :' || V_SQL );
	OPEN OUT_CUR FOR V_SQL;
        
        SELECT count(*) into V_COUNT FROM PTN_PI.PARTNER_MAPPING WHERE FTD_ORIGIN_MAPPING=IN_PARTNER_ORIGIN_ID;
        
        IF V_COUNT = 0 THEN
        RAISE NO_DATA_FOUND;
        END IF;
        
        OUT_STATUS := 'Y';
        
        EXCEPTION WHEN NO_DATA_FOUND THEN
            BEGIN
              OUT_STATUS := 'N';
              OUT_MESSAGE := 'NO RECORDS FOUND';
            END;
	 WHEN OTHERS THEN
		BEGIN
		  OUT_STATUS := 'N';
		  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
		END;  -- end exception block
END GET_PARTNER_ORIGIN_INFO;
/* --------------------------------------------------------------------------------------*/



PROCEDURE GET_FTD_ORDER_XMLS_BY_STATUS
(
    IN_ORDER_STATUS IN VARCHAR2,
    OUT_CUR        OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR	
    SELECT PARTNER_ID, PARTNER_ORDER_NUMBER, FTD_XML
	FROM  PARTNER_ORDER
	WHERE ORDER_STATUS = IN_ORDER_STATUS;

END GET_FTD_ORDER_XMLS_BY_STATUS;
/* --------------------------------------------------------------------------------------*/


PROCEDURE GET_ORDER_FEEDS_BY_STATUS
(
    IN_ORDER_STATUS IN VARCHAR2,
    OUT_CUR        OUT TYPES.REF_CURSOR
) AS

BEGIN
    OPEN OUT_CUR FOR	
    SELECT PARTNER_ORDER_FEED_ID
	FROM  PARTNER_ORDER_FEED
	WHERE ORDER_STATUS = IN_ORDER_STATUS;

END GET_ORDER_FEEDS_BY_STATUS;

PROCEDURE GET_PARTNER_ORDER_DETAILS
(
  IN_CONFIRMATION_NUMBER          IN PTN_PI.PARTNER_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE,
  OUT_CUR                         OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Retrieves order details for the given confirmation number.
	Input:
        CONFIRMATION_NUMBER          	varchar2
	Output:
        OUT_CUR      					Cursor containing the rows
-----------------------------------------------------------------------------*/

BEGIN

	OPEN OUT_CUR FOR
    	SELECT * FROM PTN_PI.PARTNER_ORDER_DETAIL WHERE CONFIRMATION_NUMBER = IN_CONFIRMATION_NUMBER;

END GET_PARTNER_ORDER_DETAILS;

PROCEDURE GET_PARTNER_MAPPINGS
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure is responsible for returning all partner mapping details.
Input: none
Output: cursor result set containing all fields
-----------------------------------------------------------------------------*/

BEGIN
	OPEN OUT_CUR FOR SELECT  * FROM    PARTNER_MAPPING;
	
END GET_PARTNER_MAPPINGS;

PROCEDURE GET_IOTW_SOURCE_CODE
(
  IN_SOURCE_CODE                IN JOE.IOTW.SOURCE_CODE%TYPE,
  IN_PRODUCT_ID                 IN JOE.IOTW.PRODUCT_ID%TYPE,
  IN_TIME_RECEIVED              IN JOE.IOTW.START_DATE%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
    Returns the IOTW source code that was in effect when the order was received.

Input:
        source_code                      varchar2
        product_id                   varchar2
        time_received                   date

Output:
        out_cur      Cursor containing the row
-----------------------------------------------------------------------------*/
default_code varchar(10);
iotw_code varchar(10);
start_date date;
end_date date;
temp_source_code varchar(10);

BEGIN

DECLARE CURSOR iotw_cur IS
  SELECT  SOURCE_CODE,
          IOTW_SOURCE_CODE,
          TRUNC(START_DATE), --Truncate for price feed calculations
          END_DATE
  FROM JOE.IOTW
    WHERE SOURCE_CODE = IN_SOURCE_CODE and PRODUCT_ID = IN_PRODUCT_ID;

BEGIN
  OPEN iotw_cur;
  FETCH iotw_cur
        INTO    default_code,
                iotw_code,
                start_date,
                end_date;
  IF iotw_cur%NOTFOUND THEN
      temp_source_code := IN_SOURCE_CODE;
  ELSE
      IF (IN_TIME_RECEIVED-start_date>=0) AND ((end_date IS NULL) OR (end_date-IN_TIME_RECEIVED>=0)) THEN
          temp_source_code := iotw_code;
      ELSE
          temp_source_code := default_code;
      END IF;
  END IF;
  CLOSE iotw_cur;

END;

OPEN OUT_CUR FOR
  SELECT S.SOURCE_CODE as IOTW_SOURCE, D.DISCOUNT_AMT as DISCOUNT, D.DISCOUNT_TYPE as DISCOUNT_TYPE
  FROM  FTD_APPS.PRICE_HEADER_DETAILS D
  JOIN FTD_APPS.SOURCE S ON S.PRICING_CODE = D.PRICE_HEADER_ID
  WHERE S.SOURCE_CODE = temp_source_code;

END GET_IOTW_SOURCE_CODE;

PROCEDURE GET_ORDER_ADJS_BY_STATUS
(
    IN_FEED_STATUS     	IN    VARCHAR2,
    IN_PARTNER_ID		    IN	  VARCHAR2,
    OUT_CUR       		  OUT   TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
  DESCRIPTION:
  	THIS PROCEDURE IS TO GET ORDER ADJUSTMENT DATA FOR A GIVEN STATUS, PARTNER.
	IF PARTNERID IS PASSED AS NULL, RETURNS ADJS OF ALL PARTNERS.
-----------------------------------------------------------------------------*/
BEGIN
  OPEN OUT_CUR FOR
      SELECT DISTINCT ADJ.PARTNER_ORDER_ADJUSTMENT_ID, ADJ.PRINCIPAL_AMT,ADJ.ADDON_AMT, ADJ.SHIPPING_AMT, ADJ.TAX_AMT,
        OD.CONFIRMATION_NUMBER,  OD.PARTNER_ORDER_DETAIL_ID, OD.PARTNER_ORDER_ITEM_NUMBER, ADJ.ADJUSTMENT_TYPE,
        O.MASTER_ORDER_NUMBER, O.PARTNER_ORDER_ID, O.PARTNER_ORDER_NUMBER, O.PARTNER_ID,
        CASE
          WHEN ADJ.ADJUSTMENT_REASON IS NOT NULL THEN
            NVL((SELECT ARM.PTN_ADJUSTMENT_REASON FROM PTN_PI.ADJUSTMENT_REASON_MAPPING ARM
            WHERE  ARM.REFUND_CODE = ADJ.ADJUSTMENT_REASON AND ARM.ADJUSTMENT_TYPE = 'REFUND'),
              (SELECT ARM.PTN_ADJUSTMENT_REASON FROM PTN_PI.ADJUSTMENT_REASON_MAPPING ARM
              WHERE  ARM.REFUND_CODE = 'Default' AND ARM.ADJUSTMENT_TYPE = 'REFUND'))
          ELSE NULL
        END ADJUSTMENT_REASON
        FROM PTN_PI.PARTNER_ORDER_ADJUSTMENT ADJ, PTN_PI.PARTNER_ORDER_DETAIL OD, PTN_PI.PARTNER_ORDER O, SCRUB.ORDER_DETAILS SOD
        WHERE ADJ.PARTNER_ORDER_DETAIL_ID=OD.PARTNER_ORDER_DETAIL_ID
        AND OD.PARTNER_ORDER_ID=O.PARTNER_ORDER_ID AND SOD.EXTERNAL_ORDER_NUMBER = OD.CONFIRMATION_NUMBER
        AND ADJ.STATUS = IN_FEED_STATUS AND (IN_PARTNER_ID IS NULL OR O.PARTNER_ID = IN_PARTNER_ID);

END GET_ORDER_ADJS_BY_STATUS;

PROCEDURE GET_ORDER_FULFILL_BY_STATUS
(
    IN_FEED_STATUS 		IN 		VARCHAR2,
    IN_PARTNER_ID 		IN 		VARCHAR2,
    OUT_CUR        		OUT 	TYPES.REF_CURSOR
) AS
/*---------------------------------------------------------------------------------
  DESCRIPTION: THIS PROCEDURE IS TO GET ORDER FULFILLMENT DATA WITH A GIVEN STATUS.
 ----------------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
	
      SELECT DISTINCT POF.PARTNER_ORDER_FULFILLMENT_ID,
			POF.SHIPPING_METHOD,
			POF.CARRIER_NAME,
			POF.TRACKING_NUMBER,
			TO_CHAR(POF.DELIVERY_DATE,'YYYY-DD-MM HH24:MI:SS') DELIVERY_DATE,
			OD.PARTNER_ORDER_DETAIL_ID,
			OD.PARTNER_ORDER_ITEM_NUMBER,
			OD.CONFIRMATION_NUMBER,
			O.PARTNER_ORDER_ID,
			O.PARTNER_ORDER_NUMBER,
			O.MASTER_ORDER_NUMBER,
			O.PARTNER_ID,
			OD.PRODUCT_ID,
			POF.DELIVERY_STATUS,
			TO_CHAR(POF.DELIVERY_STATUS_DATETIME,'YYYY-DD-MM HH24:MI:SS') DELIVERY_STATUS_DATETIME				
	FROM PTN_PI.PARTNER_ORDER_FULFILLMENT POF,
		PTN_PI.PARTNER_ORDER_DETAIL OD,
		PTN_PI.PARTNER_ORDER O
	WHERE POF.PARTNER_ORDER_DETAIL_ID = OD.PARTNER_ORDER_DETAIL_ID
		AND OD.PARTNER_ORDER_ID = O.PARTNER_ORDER_ID
		AND POF.STATUS = IN_FEED_STATUS AND (IN_PARTNER_ID IS NULL OR O.PARTNER_ID = IN_PARTNER_ID);

END GET_ORDER_FULFILL_BY_STATUS;

PROCEDURE GET_FEEDS_NOT_SENT_BY_TYPE
(
    IN_FEED_TYPE        IN 		VARCHAR2,
    OUT_CUR        		  OUT 	TYPES.REF_CURSOR
) AS
/*---------------------------------------------------------------------------------
  DESCRIPTION: THIS PROCEDURE IS TO GET FEEDS WHICH OR NOT IN SENT STATUS FOR A GIVEN FEED TYPE.
 ----------------------------------------------------------------------------------*/
BEGIN
    OPEN OUT_CUR FOR
      SELECT * FROM PTN_PI.PARTNER_FEEDS WHERE STATUS NOT IN ('SENT')
      AND FEED_TYPE = IN_FEED_TYPE;

END GET_FEEDS_NOT_SENT_BY_TYPE;

PROCEDURE GET_PARTNER_MAPPING_DETAILS
(
    IN_SOURCE_CODE        IN 		VARCHAR2,
    OUT_CURSOR        		  OUT 	TYPES.REF_CURSOR
) AS
BEGIN

 OPEN OUT_CURSOR FOR
      SELECT * FROM PTN_PI.PARTNER_MAPPING WHERE DEFAULT_SOURCE_CODE = IN_SOURCE_CODE;

END GET_PARTNER_MAPPING_DETAILS;

PROCEDURE GET_PARTNER_ORDER_DETAIL_ID
(
	IN_PARTNER_ORDER_ITEM_NUMBER	IN PARTNER_ORDER_DETAIL.PARTNER_ORDER_ITEM_NUMBER%TYPE,
	OUT_CURSOR               		OUT TYPES.REF_CURSOR
)
AS

BEGIN

OPEN OUT_CURSOR FOR
	select partner_order_detail_id 
	from PARTNER_ORDER_DETAIL
	where PARTNER_ORDER_ITEM_NUMBER = IN_PARTNER_ORDER_ITEM_NUMBER;

END GET_PARTNER_ORDER_DETAIL_ID;


PROCEDURE GET_PTN_ADJ_FOR_STATUS_UPDATE
(
  IN_BATCH_DATE       IN CLEAN.REFUND.UPDATED_ON%TYPE, 
  OUT_CUR             OUT TYPES.REF_CURSOR
) AS
/*---------------------------------------------------------------------------------
  DESCRIPTION: 
  Get the partner orders refunds that are settled for a given batch date 
  and when partner needs these status for price updates
 ----------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
      select distinct r.refund_id, cod.external_order_number, cod.order_detail_id, co.master_order_number, co.origin_id,       
       (case when (r.refund_product_amount+r.refund_addon_amount+r.refund_service_fee+r.refund_shipping_fee+r.refund_tax+r.refund_shipping_tax+r.refund_service_fee_tax = r.refund_service_fee+r.refund_shipping_fee+r.refund_tax+r.refund_shipping_tax+r.refund_service_fee_tax) then 'Y' else  'N' end) is_only_tax_fee_refund
      from clean.refund r 
      join clean.order_details cod on cod.order_detail_id = r.order_detail_id 
      join clean.orders co on co.order_guid = cod.order_guid and co.origin_id in (SELECT distinct FTD_ORIGIN_MAPPING pm FROM PTN_PI.PARTNER_MAPPING pm WHERE  pm.SEND_ORD_STATUS_UPDATE = 'Y' and co.source_code = pm.default_source_code)
      where r.refund_status in ('Billed','Settled') 
      and trunc(r.updated_on) = trunc(IN_BATCH_DATE);

END GET_PTN_ADJ_FOR_STATUS_UPDATE;

FUNCTION IS_PTN_ORD_STATUS_ALLOWED (
	IN_ORDER_NUMBER    IN  	PARTNER_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE, 
  	IN_ORD_REMOVED     IN   VARCHAR2
) RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        The Function checks if we can order status update for a gien order
          Check if partner order
          check if partner has SEND_ORD_STATUS_UPDATE flag as Y
          Check if inoice is not already sent
Input:
        IN_ORDER_NUMBER                 varchar2

Output:
        Y/N if partner order and invoice sent
-----------------------------------------------------------------------------*/

v_allow_ord_status        VARCHAR2(1);

BEGIN

	v_allow_ord_status := 'N';
	
  if IN_ORD_REMOVED = 'N' THEN
		select 'Y'  into v_allow_ord_status from clean.orders co, clean.order_details cod  
	    where cod.external_order_number = IN_ORDER_NUMBER and co.order_guid = cod.order_guid
	    and  co.origin_id in (
          SELECT distinct FTD_ORIGIN_MAPPING FROM PTN_PI.PARTNER_MAPPING pm 
          WHERE SEND_ORD_STATUS_UPDATE = 'Y' and co.source_code = pm.default_source_code
        )
	    and NOT EXISTS 
      (SELECT 1 FROM ptn_pi.order_status_update osu 
          WHERE osu.confirmation_number=cod.external_order_number 
          and osu.type='INVOICE'
      );
    ELSE
      select 'Y'  into v_allow_ord_status from scrub.orders co, scrub.order_details cod  
	    where cod.external_order_number = IN_ORDER_NUMBER and co.order_guid = cod.order_guid
	    and  co.order_origin in (
          SELECT distinct FTD_ORIGIN_MAPPING FROM PTN_PI.PARTNER_MAPPING pm 
          WHERE SEND_ORD_STATUS_UPDATE = 'Y' and co.source_code = pm.default_source_code
        )
	    and NOT EXISTS 
      (SELECT 1 FROM ptn_pi.order_status_update osu 
          WHERE osu.confirmation_number=cod.external_order_number 
          and osu.type='INVOICE'
      );
    
    END IF;
  
  	RETURN v_allow_ord_status;
  
   
  	EXCEPTION WHEN NO_DATA_FOUND THEN  
    	return v_allow_ord_status; 

END IS_PTN_ORD_STATUS_ALLOWED;


FUNCTION IS_ORD_STATUS_UPDATE_EXISTS (  
	IN_ORDER_NUMBER    IN  	ORDER_STATUS_UPDATE.CONFIRMATION_NUMBER%TYPE,
  	IN_TYPE            IN   ORDER_STATUS_UPDATE.TYPE%TYPE
) RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        The Function checks if there is order status update for a given order          
Input:
        IN_ORDER_NUMBER                 varchar2
        IN_TYPE                         varchar2
Output:
        Y/N if order status exists
-----------------------------------------------------------------------------*/

v_ord_status_exists             VARCHAR2(1);
v_query_str                     varchar2(500);
BEGIN

  v_ord_status_exists := 'N'; 
  
	   if IN_TYPE is null or IN_TYPE = '' then
	    select distinct 'Y' into v_ord_status_exists from PTN_PI.ORDER_STATUS_UPDATE POS
	    where POS.CONFIRMATION_NUMBER = IN_ORDER_NUMBER;
	  else 
	    select distinct 'Y' into v_ord_status_exists from PTN_PI.ORDER_STATUS_UPDATE POS
	    where POS.CONFIRMATION_NUMBER = IN_ORDER_NUMBER and POS.TYPE = IN_TYPE;
	  end if;
  
  RETURN v_ord_status_exists;
   
  EXCEPTION WHEN NO_DATA_FOUND THEN  
    return v_ord_status_exists; 
    
END IS_ORD_STATUS_UPDATE_EXISTS;

PROCEDURE SHIP_ORDRS_FOR_STATUS_UPDATE
(
   OUT_CUR             OUT TYPES.REF_CURSOR
) AS

/*-----------------------------------------------------------------------------
DESCRIPTION:
        THIS PROCEDURE IS RESPONSIBLE FOR FETCHING THE FLORAL 
        PARTNER ORDERS FOR TODAY THAT ARE STILL LIVE
OUTPUT:
        OUT_CUR                 	 TYPES.REF_CURSOR 
--------------------------------------------------------------------------------*/
BEGIN 
OPEN OUT_CUR FOR
	SELECT DISTINCT OD.External_Order_Number 
		FROM CLEAN.ORDER_DETAILS OD JOIN MERCURY.MERCURY M ON TO_CHAR(OD.ORDER_DETAIL_ID) = M.REFERENCE_NUMBER AND M.MERCURY_STATUS IN ('MC','MN','MM') AND M.MSG_TYPE = 'FTD'
    AND (OD.DELIVERY_DATE) = TRUNC(SYSDATE)
		AND NOT EXISTS (SELECT 'Y' FROM MERCURY.MERCURY M2 
			WHERE M2.MERCURY_ORDER_NUMBER = M.MERCURY_ORDER_NUMBER
			AND M2.MSG_TYPE IN ('CAN', 'REJ')
			AND M2.MERCURY_STATUS IN ('MC','MN','MM'))
		And Not Exists (Select 'Y' From Ptn_Pi.Order_Status_Update  Osd
			WHERE OD.EXTERNAL_ORDER_NUMBER = OSD.CONFIRMATION_NUMBER AND OSD.TYPE='SHIPPED')
      	And Exists (Select 'Y' From Ptn_Pi.Partner_Mapping Pm Where Pm.Send_Dcon_Feed='N' And Pm.Send_Ship_Status_Update ='Y' And 
			OD.SOURCE_CODE= PM.DEFAULT_SOURCE_CODE);
  	
End Ship_Ordrs_For_Status_Update;

 FUNCTION IS_PTN_ORD_SHIP_STS_UPD_ALLWD(
	IN_ORDER_NUMBER    IN  	PARTNER_ORDER_DETAIL.CONFIRMATION_NUMBER%TYPE
)	
  RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
          check if partner has SEND_ORD_SHIP_STATUS_UPDATE flag as Y
		  Check if the partner order is fully refunded 
        
Input:
        IN_ORDER_NUMBER                 varchar2

Output:
        Y/N if partner order and invoice sent
-----------------------------------------------------------------------------*/

v_allow_ord_ship_status        VARCHAR2(1);

BEGIN

	v_allow_ord_ship_status := 'N';
	
		Select 'Y' INTO V_Allow_Ord_Ship_Status
		From Clean.Orders Co, Clean.Order_Details Cod
	    Where Cod.External_Order_Number = IN_ORDER_NUMBER 
		And Co.Order_Guid = Cod.Order_Guid	 
        and  co.origin_id in (
          Select Distinct Ftd_Origin_Mapping From Ptn_Pi.Partner_Mapping Pm 
          Where Send_Ship_Status_Update = 'Y' And Co.Source_Code = Pm.Default_Source_Code)
        And Not Exists 
		  (Select 1 From Ptn_Pi.Order_Status_Update Osu 
          Where Osu.Confirmation_Number=Cod.External_Order_Number And Osu.Type In ('INVOICE','REMOVED','SHIPPED')
       )
		AND clean.refund_pkg.is_order_fully_refunded(cod.order_detail_id) != 'Y';
       
   	RETURN v_allow_ord_ship_status;
  
   	EXCEPTION WHEN NO_DATA_FOUND THEN  
    	return v_allow_ord_ship_status; 

END IS_PTN_ORD_SHIP_STS_UPD_ALLWD;


END PI_QUERY_PKG;

.
/
