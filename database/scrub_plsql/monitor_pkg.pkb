create or replace PACKAGE BODY         "SCRUB"."MONITOR_PKG" AS

PROCEDURE MONITOR_NOVATOR_NOAUTHS (
  IN_MINUTES_TO_CHECK IN NUMBER,
  IN_MAX_NOAUTHS      IN NUMBER,     
  OUT_STATUS         OUT VARCHAR2,
  OUT_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is a monitor for the incoming Novator orders.  It is used
        to determine if an excessive amount of shopping carts have been received
        without authorizations

Input:
        minutes_to_check                  number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message/email body)

-----------------------------------------------------------------------------*/

v_count NUMBER;
v_test_cc VARCHAR2(200);
v_test_cc_enc VARCHAR2(1000);
v_sql VARCHAR2(2000);

BEGIN

  OUT_STATUS := 'Y';
  
  v_test_cc := '4111111111111111,4444333322221111';
  v_test_cc_enc := GET_ENCRYPTED_CC(v_test_cc);
  
  v_sql :=  
  'SELECT count(o.MASTER_ORDER_NUMBER) 
    FROM SCRUB.ORDERS o
    JOIN SCRUB.PAYMENTS p ON o.ORDER_GUID = p.ORDER_GUID 
    JOIN SCRUB.ORDER_XML_ARCHIVE x ON o.MASTER_ORDER_NUMBER = x.MASTER_ORDER_NUMBER 
    JOIN SCRUB.CREDIT_CARDS cc ON cc.CREDIT_CARD_ID = p.CREDIT_CARD_ID
  WHERE 
    x.TIMESTAMP > SYSDATE-(' || IN_MINUTES_TO_CHECK || '/1440) AND
    p.AUTH_NUMBER IS NULL AND 
    p.PAYMENT_TYPE NOT IN (''NC'',''IN'',''GC'',''PP'') AND 
    p.PAYMENT_ID IS NOT NULL AND
    o.MASTER_ORDER_NUMBER LIKE ''M%'' AND
    cc.CC_NUMBER NOT IN ( ' || v_test_cc_enc || ')';
    
  execute immediate v_sql into v_count;
    
  IF v_count <= IN_MAX_NOAUTHS THEN
    OUT_STATUS := 'Y';
  ELSE
    OUT_STATUS := 'N';
    OUT_MESSAGE := to_char(v_count)||
                   ' shopping carts have been received from Novator in the last '||
                   TO_CHAR(IN_MINUTES_TO_CHECK) ||
                   ' without payment authorizations.  The limit is currently set to '||
                   TO_CHAR(IN_MAX_NOAUTHS)||
                   '.';
  END IF;
  
EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);      
END MONITOR_NOVATOR_NOAUTHS;


FUNCTION GET_ENCRYPTED_CC (
  IN_CC_LIST IN VARCHAR2
) 
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
	Returns a comma delimited list of encrypted CC numbers to be used in the IN clause.
Input:
        in_cc_list varchar2

Output:
        VARCHAR2

-----------------------------------------------------------------------------*/
v_cc_enc_list VARCHAR2(1000);
v_variable VARCHAR2(1000);
v_cc_number VARCHAR2(100);
v_cc_enc_comma VARCHAR2(100);
v_comma_pos NUMBER(3);
v_cc_enc credit_cards.cc_number%type;
v_key_name credit_cards.key_name%type;


BEGIN
  v_variable := in_cc_list;
  v_cc_enc_comma := in_cc_list;
  v_key_name := FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('Ingrian','Current Key');

  WHILE INSTR(v_variable,',') <> 0 LOOP
  	v_comma_pos := INSTR(v_variable,',');
  	v_cc_number := RTRIM(LTRIM(SUBSTR(v_variable,1,v_comma_pos-1),' '),' ');
  	v_cc_enc := GLOBAL.ENCRYPTION.ENCRYPT_IT(v_cc_number,v_key_name);
  	v_cc_enc_comma := ''''||v_cc_enc||''',';
  	v_cc_enc_list := v_cc_enc_list || v_cc_enc_comma;
	v_variable := SUBSTR(v_variable, v_comma_pos+1);
   END LOOP;

   v_cc_number := RTRIM(ltrim(v_variable,' '),' ');
   v_cc_enc := GLOBAL.ENCRYPTION.ENCRYPT_IT(v_cc_number,v_key_name);
   v_cc_enc_list := v_cc_enc_list ||''''||v_cc_enc||'''';


   RETURN v_cc_enc_list;

END GET_ENCRYPTED_CC;

PROCEDURE MONITOR_FEE_VARIANCE (
  IN_MINUTES_TO_CHECK IN NUMBER,
  IN_MAX_RECORDS      IN NUMBER,     
  OUT_STATUS         OUT VARCHAR2,
  OUT_MESSAGE        OUT VARCHAR2
)
AS

v_count NUMBER;
v_sql VARCHAR2(2000);

BEGIN

  OUT_STATUS := 'Y';

  v_sql :=
  'SELECT count(ssfv.order_detail_id)
    FROM SCRUB.SERVICE_SHIPPING_FEE_VARIANCE ssfv, scrub.order_details sod, scrub.orders so
  WHERE  ssfv.CREATED_ON > SYSDATE - (' || IN_MINUTES_TO_CHECK || '/1440)
    AND  sod.order_detail_id = ssfv.order_detail_id
    AND  so.order_guid = sod.order_guid
    AND  so.order_origin not in (''ARI'', ''CAT'', ''TEST'')';

  execute immediate v_sql into v_count;

  IF v_count <= IN_MAX_RECORDS THEN
    OUT_STATUS := 'Y';
  ELSE
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'The SERVICE_SHIPPING_FEE_VARIANCE table has exceeded ' ||
                   to_char(IN_MAX_RECORDS) || ' records in ' || to_char(IN_MINUTES_TO_CHECK) ||
                   ' minutes (count=' || to_char(v_count) || '). The fee variance is set off by a' ||
                   ' mismatch between Novator''s shipping fee+service fee+fuel surcharge and' ||
                   ' Apollo''s shipping fee+service fee+fuel surcharge.  Please verify fees are setup correctly.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    OUT_STATUS := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END MONITOR_FEE_VARIANCE;

END MONITOR_PKG;
/
