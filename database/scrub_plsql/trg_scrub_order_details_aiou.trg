CREATE OR REPLACE TRIGGER scrub.trg_scrub_order_details_aiou
AFTER INSERT OR UPDATE OF STATUS
ON scrub.order_details
FOR EACH ROW

/*****************************************************************************************   
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***
*** Special Instructions: 
***
*****************************************************************************************/   

DECLARE
    
   lchr_current_status   frp.order_state.status%TYPE := NULL;
   lchr_new_status   frp.order_state.status%TYPE := NULL;
   lchr_trigger_name   frp.order_state_history.trigger_name%TYPE := 'TRG_SCRUB_ORDER_DETAILS_AIOU';
   lnum_order_detail_id   scrub.order_details.order_detail_id%TYPE := :NEW.order_detail_id;
  
BEGIN


   -- fetch the current status
   lchr_current_status := frp.fun_get_current_status(lnum_order_detail_id);
   
   IF INSERTING THEN
   
      lchr_new_status := 'ORDER_RECEIVED';
      
      -- Only orders that are  not novator heartbeat should be audited.
      -- 9999 is test heartbeat from Novator.2011 is an abandoned order from WebOE.
      IF :NEW.status IN ('2011', '9999') THEN
         NULL;
      ELSE      
         -- populate the order_state tables
         -- insert or update into new order_state table
         frp.prc_pop_order_state (
	    lnum_order_detail_id,
	    lchr_new_status);

         -- insert into order_state_history table
         frp.prc_pop_order_state_history (
	    lnum_order_detail_id,
	    lchr_new_status,
	    lchr_current_status,
	    lchr_trigger_name);
      END IF;
			
   END IF;
   
   IF UPDATING THEN

      -- initialize the new status
      IF :NEW.status = 2004 THEN
	 lchr_new_status := 'ORDER_REMOVED';
      ELSIF :NEW.status = 2006 THEN
	 lchr_new_status := 'ORDER_DISPATCHED';
      END IF;

      -- Populate the order_state tables
      IF lchr_new_status IS NOT NULL THEN

         -- insert or update into frp.order_state table
         frp.prc_pop_order_state (
            lnum_order_detail_id,
	    lchr_new_status);

         -- insert into frp.order_state_history table
         frp.prc_pop_order_state_history (
	    lnum_order_detail_id,
	    lchr_new_status,
	    lchr_current_status,
	    lchr_trigger_name);

      END IF;

   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In SCRUB.TRG_SCRUB_ORDER_DETAILS_AIOU: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_scrub_order_details_aiou;
/
