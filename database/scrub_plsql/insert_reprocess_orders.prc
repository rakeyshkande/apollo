CREATE OR REPLACE PROCEDURE SCRUB.INSERT_REPROCESS_ORDERS
(
IN_ORDER_GUID              IN  REPROCESS_ORDERS_LOG.ORDER_GUID%TYPE,
IN_CREATED_BY              IN  REPROCESS_ORDERS_LOG.CREATED_BY%TYPE,
IN_REPROCESSED_FLAG        IN  REPROCESS_ORDERS_LOG.REPROCESSED_FLAG%TYPE,
OUT_STATUS                 OUT VARCHAR2,
OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a record in REPROCESS_ORDERS_LOG.
        The reprocessed_flag reflects if the order was reinstated (Y) or 
        entry was simply removed from order_exceptions (N).

Input:
        in_order_guid                   varchar2
        in_created_by                   varchar2
        in_reprocessed_flag             varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	INSERT INTO reprocess_orders_log
	(
			  order_guid,
			  created_by,
			  created_on,
                          reprocessed_flag
	)
	VALUES
	(
			  in_order_guid,
			  UPPER(in_created_by),
			  SYSDATE,
                          in_reprocessed_flag
	);

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_REPROCESS_ORDERS;
/