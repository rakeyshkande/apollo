CREATE OR REPLACE
PACKAGE BODY scrub.MISC_PKG AS


PROCEDURE INSERT_ORDER_ARCHIVE
(
IN_MASTER_ORDER_NUMBER          IN ORDER_ARCHIVE.MASTER_ORDER_NUMBER%TYPE,
IN_SECTION                      IN ORDER_ARCHIVE.SECTION%TYPE,
IN_TRANSMISSION                 IN ORDER_ARCHIVE.TRANSMISSION%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting order archive information.

Input:
        master_order_number             varchar2
        section                         number
        transmission                    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO ORDER_ARCHIVE
(
        MASTER_ORDER_NUMBER,
        SECTION,
        TRANSMISSION
)
VALUES
(
        IN_MASTER_ORDER_NUMBER,
        IN_SECTION,
        IN_TRANSMISSION
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_ORDER_ARCHIVE;


PROCEDURE VIEW_ORDER_ARCHIVE
(
IN_MASTER_ORDER_NUMBER          IN ORDER_ARCHIVE.MASTER_ORDER_NUMBER%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order archive information
        for the given master order number.

Input:
        master_order_number             varchar2

Output:
        cursor result set containing all fields in order archive
        master_order_number             varchar2
        section                         number
        transmission                    varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  MASTER_ORDER_NUMBER,
                SECTION,
                TRANSMISSION
        FROM    ORDER_ARCHIVE
        WHERE   MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER
        ORDER BY SECTION;

END VIEW_ORDER_ARCHIVE;


PROCEDURE VIEW_FORBIDDEN_WORDS
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all forbidden words.

Input:
        none

Output:
        cursor result set containing all fields in forbidden words
        word                            varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  WORD
        FROM    FORBIDDEN_WORDS;

END VIEW_FORBIDDEN_WORDS;


PROCEDURE VIEW_DISPOSITION_CODES
(
IN_MODE         IN  VARCHAR2,
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all disposition codes.

Input:
        flag: null=all dispositions, R=remove dispositions, P=pending dispositions

Output:
        cursor result set containing all fields in disposition codes
        disposition_id                  char
        disposition_description         varchar2
        pending_flag                    char
        remove_flag                     char


-----------------------------------------------------------------------------*/

BEGIN

IF IN_MODE = 'R' THEN

   OPEN OUT_CUR FOR
           SELECT  DISPOSITION_ID,
                   DISPOSITION_DESCRIPTION,
                   PENDING_FLAG,
                   REMOVE_FLAG
           FROM    DISPOSITION_CODES
           WHERE   REMOVE_FLAG = 'Y';

ELSIF IN_MODE = 'P' THEN

   OPEN OUT_CUR FOR
           SELECT  DISPOSITION_ID,
                   DISPOSITION_DESCRIPTION,
                   PENDING_FLAG,
                   REMOVE_FLAG
           FROM    DISPOSITION_CODES
           WHERE   PENDING_FLAG = 'Y';

ELSE

   OPEN OUT_CUR FOR
           SELECT  DISPOSITION_ID,
                   DISPOSITION_DESCRIPTION,
                   PENDING_FLAG,
                   REMOVE_FLAG
           FROM    DISPOSITION_CODES;

END IF;

END VIEW_DISPOSITION_CODES;


PROCEDURE VIEW_FRAUD_ORDER_DISPOSITIONS
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all fraud order dispositions.

Input:
        none

Output:
        cursor result set containing all fields in frand order dispositions
        fraud_id                        varchar2
        fraud_description               varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  FRAUD_ID,
                FRAUD_DESCRIPTION
        FROM    FRAUD_ORDER_DISPOSITIONS;

END VIEW_FRAUD_ORDER_DISPOSITIONS;


FUNCTION GET_FRAUD_DESCRIPTION_BY_ID
(
IN_FRAUD_ID         IN FRAUD_ORDER_DISPOSITIONS.FRAUD_ID%TYPE
)
RETURN FRAUD_ORDER_DISPOSITIONS.FRAUD_DESCRIPTION%TYPE
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the description for a
        given fraud disposition id.

Input:
        IN_FRAUD_ID                   varchar2

Output:
        OUT_FRAUD_DESCRIPTION         varchar2

-----------------------------------------------------------------------------*/
OUT_FRAUD_DESCRIPTION FRAUD_ORDER_DISPOSITIONS.FRAUD_DESCRIPTION%TYPE;

BEGIN

	BEGIN
        SELECT  FRAUD_DESCRIPTION
        INTO    OUT_FRAUD_DESCRIPTION
        FROM    FRAUD_ORDER_DISPOSITIONS
        WHERE   IN_FRAUD_ID = FRAUD_ID;

	EXCEPTION WHEN NO_DATA_FOUND THEN OUT_FRAUD_DESCRIPTION := NULL;
	END;

RETURN OUT_FRAUD_DESCRIPTION;
END GET_FRAUD_DESCRIPTION_BY_ID;


PROCEDURE INSERT_CSR_VIEWED_ORDERS
(
IN_CSR_ID                       IN ORDERS.CSR_ID%TYPE,
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the order guids
        that has been returned to the csr.

Input:
        csr_id                          varchar2
        order_guid                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT INTO CSR_VIEWED_ORDERS
(
        CSR_ID,
        ORDER_GUID
)
VALUES
(
        IN_CSR_ID,
        IN_ORDER_GUID
);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END INSERT_CSR_VIEWED_ORDERS;



PROCEDURE RESET_CSR_VIEWED_ORDERS
(
IN_CSR_ID                       IN ORDERS.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the order guids
        for the csr from the holding table.

Input:
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

DELETE FROM CSR_VIEWED_ORDERS
WHERE   CSR_ID = IN_CSR_ID;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END RESET_CSR_VIEWED_ORDERS;


PROCEDURE INSERT_ORDER_EXCEPTIONS
(
IN_CONTEXT                      IN ORDER_EXCEPTIONS.CONTEXT%TYPE,
IN_MESSAGE                      IN ORDER_EXCEPTIONS.MESSAGE%TYPE,
IN_ERROR                        IN ORDER_EXCEPTIONS.ERROR%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the order exception
        messages.

Input:
        context                         varchar2
        message                         varchar2
        error                           varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

VAR_ORDER_GUID VARCHAR2(3000);

BEGIN
IF instr(IN_MESSAGE, '.') <> 0 THEN
  VAR_ORDER_GUID := substr(IN_MESSAGE, instr(IN_MESSAGE, '.') + 1, length(IN_MESSAGE));
ELSIF instr(IN_MESSAGE, '/') <> 0  THEN
  VAR_ORDER_GUID := substr(IN_MESSAGE, 1, instr (IN_MESSAGE, '/') - 1);
ELSE
  VAR_ORDER_GUID := IN_MESSAGE;
END IF;

IF instr(VAR_ORDER_GUID, 'M', 1) = 1 THEN
  SELECT ORDER_GUID
     INTO VAR_ORDER_GUID
     FROM ORDERS
     WHERE MASTER_ORDER_NUMBER = VAR_ORDER_GUID;
END IF;



INSERT INTO ORDER_EXCEPTIONS
(
        CONTEXT,
        MESSAGE,
        ERROR,
        TIMESTAMP,
        ORDER_GUID
)
VALUES
(
        IN_CONTEXT,
        IN_MESSAGE,
        IN_ERROR,
        SYSDATE,
        VAR_ORDER_GUID
);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        -- ignore duplicate messages
        OUT_STATUS := 'Y';
END;

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END INSERT_ORDER_EXCEPTIONS;


PROCEDURE DELETE_ORDER_EXCEPTIONS
(
IN_CONTEXT                      IN ORDER_EXCEPTIONS.CONTEXT%TYPE,
IN_MESSAGE                      IN ORDER_EXCEPTIONS.MESSAGE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the order exception
        messages.

Input:
        context                         varchar2
        message                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

DELETE FROM ORDER_EXCEPTIONS
WHERE   CONTEXT = IN_CONTEXT
AND     MESSAGE = IN_MESSAGE;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block


END DELETE_ORDER_EXCEPTIONS;


PROCEDURE VIEW_ORDER_EXCEPTIONS
(
IN_CONTEXT        IN ORDER_EXCEPTIONS.CONTEXT%TYPE,
IN_ORDER_STATUS   IN ORDERS.STATUS%TYPE,
OUT_CUR           OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all order exceptions.
        If in_context is null, then all rows are returned.

Input:
        context                         varchar2

Output:
        cursor result set containing all fields in order exceptions
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
        SELECT  oe.CONTEXT,
                oe.MESSAGE,
                oe.ERROR,
                oe.TIMESTAMP,
                o.ORDER_GUID,
                o.STATUS AS ORDER_STATUS,
                od.ORDER_DETAIL_ID,
                od.STATUS AS ORDER_DETAIL_STATUS,
                od.EXTERNAL_ORDER_NUMBER,
                --we need to display the first order differently than the other orders, so rank them
                rank() over (partition by oe.order_guid order by od.ORDER_DETAIL_ID) DETAIL_SEQ_WITHIN_GUID

        FROM    ORDER_EXCEPTIONS oe
            LEFT OUTER JOIN ORDERS o on o.ORDER_GUID = oe.ORDER_GUID
            LEFT OUTER JOIN ORDER_DETAILS od on od.ORDER_GUID = o.ORDER_GUID
        WHERE   CONTEXT = NVL(IN_CONTEXT, CONTEXT)
            AND (o.STATUS = NVL(IN_ORDER_STATUS, o.STATUS) OR (IN_ORDER_STATUS IS NULL))
        ORDER BY oe.TIMESTAMP, oe.ORDER_GUID, DETAIL_SEQ_WITHIN_GUID;

END VIEW_ORDER_EXCEPTIONS;


PROCEDURE VIEW_ORDER_EXCEPTIONS_CONTEXT
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order exceptions contexts
        and their current counts

Output:
        cursor result set containing distinct contexts fields and counts
        in order exceptions
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
        SELECT  CONTEXT,
                COUNT(CONTEXT) AS COUNT
        FROM    ORDER_EXCEPTIONS
        GROUP BY CONTEXT
        ORDER BY CONTEXT;

END VIEW_ORDER_EXCEPTIONS_CONTEXT;


PROCEDURE VIEW_ORDER_EXCEPTIONS_STATUS
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order exceptions order statuses
        and their current counts

Output:
        cursor result set containing distinct statuses and counts
        in order exceptions
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
        SELECT  o.STATUS,
                COUNT(o.STATUS) AS COUNT
        FROM    ORDER_EXCEPTIONS  oe
            LEFT OUTER JOIN SCRUB.ORDERS o on o.ORDER_GUID = oe.ORDER_GUID
        WHERE
                o.STATUS is not null
        GROUP BY o.STATUS
        ORDER BY o.STATUS;

END VIEW_ORDER_EXCEPTIONS_STATUS;




PROCEDURE VIEW_AP_VALIDATION_PROPERTIES
(
IN_PAYMENT_METHOD_ID	IN AP_ACCOUNTS.PAYMENT_METHOD_ID%TYPE,
OUT_CUR         	OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning validation properties
        for the given payment method id

Input:
	in_payment_method_id payment_method_id

Output:
        cursor result set containing property name and property values
-----------------------------------------------------------------------------*/

BEGIN

    OPEN OUT_CUR FOR
        SELECT property_name,
               property_value
        FROM   ap_validation_properties
        WHERE  payment_method_id = in_payment_method_id;

END VIEW_AP_VALIDATION_PROPERTIES;

PROCEDURE INSERT_FEE_VARIANCE
(
IN_ORDER_DETAIL_ID              IN SERVICE_SHIPPING_FEE_VARIANCE.ORDER_DETAIL_ID%TYPE,
IN_NOVATOR_SERVICE_FEE_AMT      IN SERVICE_SHIPPING_FEE_VARIANCE.NOVATOR_SERVICE_FEE_AMT%TYPE,
IN_NOVATOR_SHIPPING_FEE_AMT     IN SERVICE_SHIPPING_FEE_VARIANCE.NOVATOR_SHIPPING_FEE_AMT%TYPE,
IN_WEBSITE_MDF 					IN SERVICE_SHIPPING_FEE_VARIANCE.WEBSITE_MORNING_DELIVERY_FEE%TYPE,
IN_APOLLO_SERVICE_FEE_AMT       IN SERVICE_SHIPPING_FEE_VARIANCE.APOLLO_SERVICE_FEE_AMT%TYPE,
IN_APOLLO_SHIPPING_FEE_AMT      IN SERVICE_SHIPPING_FEE_VARIANCE.APOLLO_SHIPPING_FEE_AMT%TYPE,
IN_APOLLO_MORNING_DELIVERY_FEE  IN SERVICE_SHIPPING_FEE_VARIANCE.APOLLO_MORNING_DELIVERY_FEE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT INTO SERVICE_SHIPPING_FEE_VARIANCE
(
        ORDER_DETAIL_ID,
        NOVATOR_SERVICE_FEE_AMT,
        NOVATOR_SHIPPING_FEE_AMT,
        WEBSITE_MORNING_DELIVERY_FEE,
        APOLLO_SERVICE_FEE_AMT,
        APOLLO_SHIPPING_FEE_AMT,
        APOLLO_MORNING_DELIVERY_FEE, 
        CREATED_ON
)
VALUES
(
        IN_ORDER_DETAIL_ID,
        IN_NOVATOR_SERVICE_FEE_AMT,
        IN_NOVATOR_SHIPPING_FEE_AMT,
        IN_WEBSITE_MDF,
        IN_APOLLO_SERVICE_FEE_AMT,
        IN_APOLLO_SHIPPING_FEE_AMT,
        IN_APOLLO_MORNING_DELIVERY_FEE,
        SYSDATE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        -- ignore duplicate messages
        OUT_STATUS := 'Y';
END;

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_FEE_VARIANCE;

PROCEDURE INSERT_AVS_REQUEST
(
IN_CLIENT_IDENTIFIER            IN AVS_REQUEST.CLIENT_IDENTIFIER%TYPE,
IN_RESPONSE_TIME                IN AVS_REQUEST.RESPONSE_TIME%TYPE,
IN_VENDOR_RESPONSE_ID           IN AVS_REQUEST.VENDOR_RESPONSE_ID%TYPE,
IN_VENDOR                       IN AVS_REQUEST.VENDOR%TYPE,
IN_ADDRESS                      IN AVS_REQUEST.ADDRESS%TYPE,
IN_CITY                         IN AVS_REQUEST.CITY%TYPE,
IN_STATE_PROVINCE               IN AVS_REQUEST.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN AVS_REQUEST.POSTAL_CODE%TYPE,
OUT_AVS_REQUEST_ID              OUT AVS_REQUEST.AVS_REQUEST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for inserting AVS request information.
    
    Input:
    
    Output:
            avs_request_id                          number
            status                          varchar2 (Y or N)
            message                         varchar2 (error message)
    -----------------------------------------------------------------------------*/
    CURSOR avs_request_cur IS
        select avs_request_seq.nextval
        from dual;
    
    BEGIN
    
    OPEN avs_request_cur;
    FETCH avs_request_cur into OUT_AVS_REQUEST_ID;
    
    INSERT INTO AVS_REQUEST
    (
            AVS_REQUEST_ID,
            VENDOR_RESPONSE_ID,
            CLIENT_IDENTIFIER,
            RESPONSE_TIME,
            VENDOR,
            ADDRESS,
            CITY,
            STATE_PROVINCE,
            POSTAL_CODE,
            CREATED_ON
    )
    VALUES
    (
            OUT_AVS_REQUEST_ID,
            IN_VENDOR_RESPONSE_ID, 
            IN_CLIENT_IDENTIFIER,
            IN_RESPONSE_TIME,
            IN_VENDOR,
            IN_ADDRESS,
            IN_CITY,
            IN_STATE_PROVINCE,
            IN_POSTAL_CODE,
            sysdate
    );
    
    OUT_STATUS := 'Y';
    
    EXCEPTION WHEN OTHERS THEN
    BEGIN
            OUT_STATUS := 'N';
            OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;    -- end exception block
  END INSERT_AVS_REQUEST;

END MISC_PKG;
.
/