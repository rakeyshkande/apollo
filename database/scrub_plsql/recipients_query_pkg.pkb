create or replace PACKAGE BODY scrub.RECIPIENTS_QUERY_PKG AS


PROCEDURE VIEW_RECIPIENTS
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_RECIPIENT_ID         IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient information
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2
        recipient_id                    number

Output:
        cursor result set containing all fields in recipients
        recipient_id                    number
        last_name                       varchar2
        first_name                      varchar2
        list_code                       varchar2
        auto_hold                       varchar2
        mail_list_code                  varchar2
        status                          varchar2
        customer_id                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  /*+ RULE */
                        R.RECIPIENT_ID,
                        R.LAST_NAME,
                        R.FIRST_NAME,
                        R.LIST_CODE,
                        R.AUTO_HOLD,
                        R.MAIL_LIST_CODE,
                        R.STATUS,
                        R.CUSTOMER_ID,
                        OD.ORDER_DETAIL_ID
                FROM    RECIPIENTS R
                JOIN    ORDER_DETAILS OD
                ON      R.RECIPIENT_ID = OD.RECIPIENT_ID
                WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  R.RECIPIENT_ID,
                        R.LAST_NAME,
                        R.FIRST_NAME,
                        R.LIST_CODE,
                        R.AUTO_HOLD,
                        R.MAIL_LIST_CODE,
                        R.STATUS,
                        R.CUSTOMER_ID
                FROM    RECIPIENTS R
                WHERE   R.RECIPIENT_ID = IN_RECIPIENT_ID;

END IF;

END VIEW_RECIPIENTS;


PROCEDURE VIEW_RECIPIENT_ADDRESSES
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_RECIPIENT_ID         IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient address information
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2
        recipient_id                    number

Output:
        cursor result set containing all fields in recipient addresses
        recipient_address_id            number
        recipient_id                    number
        address_type                    varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        international                   varchar2
        name                            varchar2
        info                            varchar2
        code                            varchar2
-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  /*+ RULE */
                        RA.RECIPIENT_ADDRESS_ID,
                        RA.RECIPIENT_ID,
                        RA.ADDRESS_TYPE,
                        RA.ADDRESS_LINE_1,
                        RA.ADDRESS_LINE_2,
                        RA.CITY,
                        RA.STATE_PROVINCE,
                        RA.POSTAL_CODE,
                        RA.COUNTRY,
                        RA.COUNTY,
                        RA.INTERNATIONAL,
                        RA.NAME,
                        RA.INFO,
                        (SELECT AT.CODE FROM FRP.ADDRESS_TYPES AT WHERE AT.ADDRESS_TYPE = RA.ADDRESS_TYPE) CODE,
                        OD.ORDER_DETAIL_ID
                FROM    RECIPIENT_ADDRESSES RA
                JOIN    ORDER_DETAILS OD
                ON      RA.RECIPIENT_ADDRESS_ID = OD.RECIPIENT_ADDRESS_ID
                WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  RA.RECIPIENT_ADDRESS_ID,
                        RA.RECIPIENT_ID,
                        RA.ADDRESS_TYPE,
                        RA.ADDRESS_LINE_1,
                        RA.ADDRESS_LINE_2,
                        RA.CITY,
                        RA.STATE_PROVINCE,
                        RA.POSTAL_CODE,
                        RA.COUNTRY,
                        RA.COUNTY,
                        RA.INTERNATIONAL,
                        RA.NAME,
                        RA.INFO
                FROM    RECIPIENT_ADDRESSES RA
                WHERE   RA.RECIPIENT_ID = IN_RECIPIENT_ID;

END IF;

END VIEW_RECIPIENT_ADDRESSES;


PROCEDURE VIEW_QMS_ADDRESSES
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_RECIPIENT_ID         IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning QMS address information
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2
        recipient_id                    number

Output:
        cursor result set containing all fields in QMS addresses
        qms_id                          number
        recipient_id                    number
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        firm_name                       varchar2
        geofind_match_code              varchar2
        override_flag                   varchar2
        latitude                        varchar2
        longitude                       varchar2
        range_record_type               varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  QA.QMS_ID,
                        QA.RECIPIENT_ID,
                        QA.ADDRESS_LINE_1,
                        QA.ADDRESS_LINE_2,
                        QA.CITY,
                        QA.STATE_PROVINCE,
                        QA.POSTAL_CODE,
                        QA.COUNTRY,
                        QA.COUNTY,
                        QA.FIRM_NAME,
                        QA.GEOFIND_MATCH_CODE,
                        QA.OVERRIDE_FLAG,
                        QA.LATITUDE,
                        QA.LONGITUDE,
                        QA.RANGE_RECORD_TYPE
                FROM    QMS_ADDRESSES QA
                JOIN    ORDER_DETAILS OD
                ON      QA.RECIPIENT_ID = OD.RECIPIENT_ID
                WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  QA.QMS_ID,
                        QA.RECIPIENT_ID,
                        QA.ADDRESS_LINE_1,
                        QA.ADDRESS_LINE_2,
                        QA.CITY,
                        QA.STATE_PROVINCE,
                        QA.POSTAL_CODE,
                        QA.COUNTRY,
                        QA.COUNTY,
                        QA.FIRM_NAME,
                        QA.GEOFIND_MATCH_CODE,
                        QA.OVERRIDE_FLAG,
                        QA.LATITUDE,
                        QA.LONGITUDE,
                        QA.RANGE_RECORD_TYPE
                FROM    QMS_ADDRESSES QA
                WHERE   QA.RECIPIENT_ID = IN_RECIPIENT_ID;


END IF;

END VIEW_QMS_ADDRESSES;


PROCEDURE VIEW_RECIPIENT_PHONES
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_RECIPIENT_ID         IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient phone information
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2
        recipient_id                    number

Output:
        cursor result set containing all fields in recipient phones
        recipient_phone_id              number
        recipient_id                    number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  /*+ RULE */
                        RP.RECIPIENT_PHONE_ID,
                        RP.RECIPIENT_ID,
                        RP.PHONE_TYPE,
                        RP.PHONE_NUMBER,
                        RP.EXTENSION,
                        OD.ORDER_DETAIL_ID
                FROM    RECIPIENT_PHONES RP
                JOIN    ORDER_DETAILS OD
                ON      RP.RECIPIENT_ID = OD.RECIPIENT_ID
                WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  RP.RECIPIENT_PHONE_ID,
                        RP.RECIPIENT_ID,
                        RP.PHONE_TYPE,
                        RP.PHONE_NUMBER,
                        RP.EXTENSION
                FROM    RECIPIENT_PHONES RP
                WHERE   RP.RECIPIENT_ID = IN_RECIPIENT_ID;


END IF;

END VIEW_RECIPIENT_PHONES;


PROCEDURE VIEW_RECIPIENT_COMMENTS
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_RECIPIENT_ID         IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient comments
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2
        recipient_id                    number

Output:
        cursor result set containing all fields in recipient comments
        recipient_comment_id            number
        recipient_id                    number
        text                            varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  /*+ RULE */ RC.RECIPIENT_COMMENT_ID,
                        RC.RECIPIENT_ID,
                        RC.TEXT
                FROM    RECIPIENT_COMMENTS RC
                JOIN    ORDER_DETAILS OD
                ON      RC.RECIPIENT_ID = OD.RECIPIENT_ID
                WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  RC.RECIPIENT_COMMENT_ID,
                        RC.RECIPIENT_ID,
                        RC.TEXT
                FROM    RECIPIENT_COMMENTS RC
                WHERE   RC.RECIPIENT_ID = IN_RECIPIENT_ID;


END IF;

END VIEW_RECIPIENT_COMMENTS;


PROCEDURE VIEW_AVS_ADDRESS
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning AVS address information
        for the given order or the specific recipient.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in AVS address
        avs_address_id                  number
        address                         varchar2
        city                            varchar2
        state			                      varchar2
        zip			                        varchar2
        country                         varchar2
        latitude                        varchar2
        longitude                       varchar2
        entity_type                     varchar2
        override_flag                   varchar2
        avs_result                      varchar2
        avs_performed                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

	OPEN OUT_CUR FOR
			SELECT  AA.AVS_ADDRESS_ID,
					AA.RECIPIENT_ID,
					AA.ADDRESS,
					AA.CITY,
					AA.STATE,
					AA.ZIP,
					AA.COUNTRY,
					AA.LATITUDE,
					AA.LONGITUDE,
					AA.ENTITY_TYPE,
					AA.OVERRIDE_FLAG,
					AA.AVS_RESULT,
					AA.AVS_PERFORMED,
		            AA.AVS_PERFORMED_ORIGIN
			FROM    AVS_ADDRESS AA
			JOIN    ORDER_DETAILS OD
			ON      AA.RECIPIENT_ID = OD.RECIPIENT_ID
			WHERE   OD.ORDER_GUID = IN_ORDER_GUID  order by AA.created_on;

END VIEW_AVS_ADDRESS;


PROCEDURE VIEW_AVS_ADDRESS_SCORE
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning AVS address score information
        for the given order or the specific recipient.

Input:
        avs_address_id                  number

Output:
        cursor result set containing all fields in AVS address score
        avs_address_score_id            number
        avs_address_id                  number
        reason                          varchar2
        score                           varchar2

-----------------------------------------------------------------------------*/

BEGIN

	OPEN OUT_CUR FOR
			SELECT  SC.AVS_SCORE_ID,
					SC.AVS_ADDRESS_ID,
					SC.SCORE_REASON,
					SC.SCORE
			FROM    AVS_ADDRESS_SCORE SC, AVS_ADDRESS AA
			JOIN    ORDER_DETAILS OD
			ON      AA.RECIPIENT_ID = OD.RECIPIENT_ID
			WHERE   OD.ORDER_GUID = IN_ORDER_GUID AND AA.AVS_ADDRESS_ID = SC.AVS_ADDRESS_ID;

END VIEW_AVS_ADDRESS_SCORE;


PROCEDURE VIEW_ORDER_RECIPIENT_INFO
(
IN_ORDER_GUID                   IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_RECIPIENT_CUR               OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_ADDRESS_CUR       OUT TYPES.REF_CURSOR,
OUT_QMS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_PHONE_CUR         OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_COMMENTS_CUR      OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient information
        for the given order.

Input:
        order_guid                      varchar2

Output:
        recipients cursor
        recipient addresses cursor
        recipient QMS cursor
        recipient phones cursor
        recipient comments cursor

-----------------------------------------------------------------------------*/

BEGIN

VIEW_RECIPIENTS (IN_ORDER_GUID, NULL, OUT_RECIPIENT_CUR);
VIEW_RECIPIENT_ADDRESSES (IN_ORDER_GUID, NULL, OUT_RECIPIENT_ADDRESS_CUR);
VIEW_QMS_ADDRESSES (IN_ORDER_GUID, NULL, OUT_QMS_ADDRESS_CUR);
VIEW_RECIPIENT_PHONES (IN_ORDER_GUID, NULL, OUT_RECIPIENT_PHONE_CUR);
VIEW_RECIPIENT_COMMENTS (IN_ORDER_GUID, NULL, OUT_RECIPIENT_COMMENTS_CUR);


END VIEW_ORDER_RECIPIENT_INFO;



PROCEDURE VIEW_ORDER_RECIPIENT_INFO
(
IN_ORDER_GUID                   IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_RECIPIENT_CUR               OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_ADDRESS_CUR       OUT TYPES.REF_CURSOR,
OUT_QMS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_PHONE_CUR         OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_COMMENTS_CUR      OUT TYPES.REF_CURSOR,
OUT_AVS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_AVS_ADDRESS_SCORE_CUR       OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient information
        for the given order. Same as other view_order_recipient_info escept it also grabs
        the AVS address info

Input:
        order_guid                      varchar2

Output:
        recipients cursor
        recipient addresses cursor
        recipient QMS cursor
        recipient phones cursor
        recipient comments cursor
        avs address cursor
        avs address score cursor
-----------------------------------------------------------------------------*/

BEGIN

VIEW_RECIPIENTS (IN_ORDER_GUID, NULL, OUT_RECIPIENT_CUR);
VIEW_RECIPIENT_ADDRESSES (IN_ORDER_GUID, NULL, OUT_RECIPIENT_ADDRESS_CUR);
VIEW_QMS_ADDRESSES (IN_ORDER_GUID, NULL, OUT_QMS_ADDRESS_CUR);
VIEW_RECIPIENT_PHONES (IN_ORDER_GUID, NULL, OUT_RECIPIENT_PHONE_CUR);
VIEW_RECIPIENT_COMMENTS (IN_ORDER_GUID, NULL, OUT_RECIPIENT_COMMENTS_CUR);
VIEW_AVS_ADDRESS (IN_ORDER_GUID, OUT_AVS_ADDRESS_CUR);
VIEW_AVS_ADDRESS_SCORE (IN_ORDER_GUID, OUT_AVS_ADDRESS_SCORE_CUR);

END VIEW_ORDER_RECIPIENT_INFO;



PROCEDURE VIEW_RECIPIENT_INFO
(
IN_RECIPIENT_ID                 IN NUMBER,
OUT_RECIPIENT_CUR               OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_ADDRESS_CUR       OUT TYPES.REF_CURSOR,
OUT_QMS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_PHONE_CUR         OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_COMMENTS_CUR      OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning recipient information
        for the recipient id.

Input:
        recipient id                    number

Output:
        recipients cursor
        recipient addresses cursor
        recipient QMS cursor
        recipient phones cursor
        recipient comments cursor
-----------------------------------------------------------------------------*/

BEGIN

VIEW_RECIPIENTS (NULL, IN_RECIPIENT_ID, OUT_RECIPIENT_CUR);
VIEW_RECIPIENT_ADDRESSES (NULL, IN_RECIPIENT_ID, OUT_RECIPIENT_ADDRESS_CUR);
VIEW_QMS_ADDRESSES (NULL, IN_RECIPIENT_ID, OUT_QMS_ADDRESS_CUR);
VIEW_RECIPIENT_PHONES (NULL, IN_RECIPIENT_ID, OUT_RECIPIENT_PHONE_CUR);
VIEW_RECIPIENT_COMMENTS (NULL, IN_RECIPIENT_ID, OUT_RECIPIENT_COMMENTS_CUR);

END VIEW_RECIPIENT_INFO;


FUNCTION GET_RECIPIENT_ID
(
IN_LAST_NAME                    IN RECIPIENTS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN RECIPIENTS.FIRST_NAME%TYPE,
IN_POSTAL_CODE                  IN RECIPIENT_ADDRESSES.POSTAL_CODE%TYPE,
IN_ADDRESS_LINE_1               IN RECIPIENT_ADDRESSES.ADDRESS_LINE_1%TYPE
)
RETURN NUMBER

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning recipient id for the unique
        recipient information (last name + first name initial + zip code +
        first 4 characters of address line 1).

Input:
        last_name                       varchar2
        first_name                      varchar2
        postal_code                     varchar2
        address_line_1                  varchar2

Output:
        number  (buyer id)
-----------------------------------------------------------------------------*/

V_RECIPIENT_ID          RECIPIENTS.RECIPIENT_ID%TYPE;
BEGIN

SELECT  R.RECIPIENT_ID
INTO    V_RECIPIENT_ID
FROM    RECIPIENTS R
JOIN    RECIPIENT_ADDRESSES RA
ON      R.RECIPIENT_ID = RA.RECIPIENT_ID
WHERE   R.LAST_NAME = IN_LAST_NAME
AND     SUBSTR (R.FIRST_NAME, 1, 1) = SUBSTR (IN_FIRST_NAME, 1, 1)
AND     RA.POSTAL_CODE = IN_POSTAL_CODE
AND     SUBSTR (RA.ADDRESS_LINE_1, 1, 4) = SUBSTR (IN_ADDRESS_LINE_1, 1, 4);

RETURN V_RECIPIENT_ID;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_RECIPIENT_ID;


END RECIPIENTS_QUERY_PKG;
.
/
