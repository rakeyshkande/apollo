create or replace procedure scrub.expire_pending_orders as
   ----------------------------------------------------------------------------------
   --  Defect 1692 Fix
   --  ---------------
   --  This procedure will look for pending_orders which need to be expired, update
   --  the status on order_details, then call orders_maint_pkg.insert_order_disposition.
   ----------------------------------------------------------------------------------

   v_order_detail_id     scrub.order_details.order_detail_id%type;
   v_order_guid          scrub.order_details.order_guid%type;
   v_order_date          scrub.orders.order_date%type;
   v_order_date_date     date;
   v_external_order_number scrub.order_details.external_order_number%type;
   v_out_disposition_id  scrub.order_disposition.order_disposition_id%type;
   v_2005_guid_cnt       number :=0;
   v_processed_cnt       number :=0;
   v_not_processed_cnt   number :=0;
   v_days_lock           number :=0;
   v_total               number :=0;
   v_status              varchar(10);
   v_message             varchar (2000);
   v_email_func          varchar (50);
   ord_maint_pkg_err     EXCEPTION;
   email_err             EXCEPTION;
   account_maint_pkg_err EXCEPTION;

   c utl_smtp.connection;


   -- Grab the Pending Order Details
   cursor order_detail_cur is
      select sod.order_detail_id, 
             sod.order_guid,
             sod.external_order_number
      from scrub.order_details sod
      where sod.status = '2005';



BEGIN    
   ops$oracle.mail_pkg.init_email(c,'SCRUB_NOPAGE_ALERTS','NOPAGE Scrub.Expire_Pending_Orders Results',v_status,v_message);
   if v_status = 'N' THEN
      v_email_func := 'ops$oracle.mail_pkg.init_email';
      RAISE EMAIL_ERR;
   end if;


   select to_number(value)  
   into   v_days_lock
   from   frp.global_parms 
   where  context = 'SCRUB_CONFIG' 
     and  name    = 'DAYS_LOCK_PENDING_REMOVED_ORDER';

   FOR order_detail_rec IN order_detail_cur
   LOOP
      v_2005_guid_cnt   := v_2005_guid_cnt + 1; 
      v_order_detail_id := order_detail_rec.ORDER_DETAIL_ID;
      v_order_guid      := order_detail_rec.ORDER_GUID;
      v_external_order_number := order_detail_rec.EXTERNAL_ORDER_NUMBER;


      --
      -- Find the order_date and convert to a date format.  If any problems,
      -- set the value to NULL.
      --
      select order_date 
      into v_order_date
      from scrub.orders
      where order_guid = v_order_guid;

      --dbms_output.put_line('2005 GUID=' || substr(v_order_guid,1,18) || 
      --            '...     v_order_date=' || v_order_date);
      utl_smtp.write_data(c, utl_tcp.CRLF || 'Found 2005 Order Detail = ' || v_order_detail_id);
    
      if v_order_date is null then
         v_order_date_date := null;
      else 
         begin 
            v_order_date_date := to_date(to_char(to_timestamp_tz(substr(v_order_date,5), 
                'MON DD HH24:MI:SS TZD YYYY'), 'YYYY-MM-DD HH24.MI.SS'), 'YYYY-MM-DD HH24.MI.SS') ; 
         exception
            when others then
               dbms_output.put_line('DID NOT CONVERT - set v_order_date_date to NULL');
               v_order_date_date := null;
         end;
      end if;


      if v_order_date_date is NOT NULL
         and v_order_date_date < (sysdate - v_days_lock) THEN

         --dbms_output.put_line('.....Expire it');
         utl_smtp.write_data(c, '...Expire It!!');
         v_processed_cnt := v_processed_cnt + 1;

         --****************************************************************************
         -- Update status on the Scrub.Order_Details
         --****************************************************************************
         update scrub.order_details  
         set status = '2004',
             updated_on = sysdate
         where order_detail_id = v_order_detail_id;

	 clean.order_maint_pkg.UPDATE_ORDER_DETAILS_DISP_CODE
	 	(
	 	 IN_ORDER_DETAIL_ID  => v_order_detail_id,
		 IN_ORDER_DISP_CODE  => 'Removed',
		 OUT_STATUS    	     => v_status,
 		 OUT_MESSAGE   	     => v_message
	 	);
	 	
         if v_status = 'N' THEN
            utl_smtp.write_data(c, 'Error in update status:' || v_message);
         end if;  	 	
	 	
   
         --****************************************************************************
         -- If all items in cart are removed, change order status to removed (1006).
         -- If total count - removed count = 0, all orders are in removed status. 
         --****************************************************************************
         begin
            select   total_count - removed_count 
            into       v_total   
            from (select order_guid, count(*) total_count
                    from scrub.order_details
                    where order_guid = v_order_guid
                    group by   order_guid) t1,
                 (select order_guid, count(*) removed_count
                    from scrub.order_details
                    where order_guid = v_order_guid
                     and status = '2004'
                    group by   order_guid) t2
            where t1.order_guid = t2.order_guid;
         exception
            when no_data_found then
               v_total := 0;
         end;

         IF v_total = 0 THEN
            update scrub.orders
            set status = '1006', 
                updated_on = sysdate
            where order_guid = v_order_guid;
         END IF;

         --****************************************************************************
         -- Insert a comment using scrub.orders_maint_pkg.insert_order_disposition
         --****************************************************************************
         scrub.orders_maint_pkg.insert_order_disposition
         (IN_ORDER_DETAIL_ID => v_order_detail_id, 
          IN_DISPOSITION_ID  => 'L',
          IN_COMMENTS        => 'This order was in the pending file for greater than ' || 
                  v_days_lock || ' days. This order cannot be reinstated and will need to be replaced.',
          IN_CALLED_CUSTOMER_FLAG => 'N',
          IN_SENT_EMAIL_FLAG  => 'N',
          IN_STOCK_MESSAGE_ID => null,
          IN_EMAIL_SUBJECT    => null,
          IN_EMAIL_MESSAGE    => null,
          IN_CSR_ID           => 'SYS',
          OUT_ORDER_DISPOSITION_ID => v_out_disposition_id, 
          OUT_STATUS  => v_status,
          OUT_MESSAGE => v_message
         );

         if v_status = 'N' THEN
            RAISE ORD_MAINT_PKG_ERR;
         end if;     

         --****************************************************************************
         -- Inactivate any programs tied to this order
         --****************************************************************************         
         account.account_maint_pkg.update_program_status_by_order
         (
          IN_EXTERNAL_ORDER_NUMBER => v_external_order_number,    
          IN_PROGRAM_STATUS => 'I',
          IN_UPDATED_BY => 'SYS',         
          OUT_STATUS  => v_status,
          OUT_MESSAGE => v_message         
         );
         
         if v_status = 'N' THEN
            RAISE ACCOUNT_MAINT_PKG_ERR;
         end if;            
         
      else
         --dbms_output.put_line('.....SKIP IT');
         utl_smtp.write_data(c, '...Skip It');
         v_not_processed_cnt := v_not_processed_cnt + 1;
      end if;
   END LOOP;

   dbms_output.put_line('Scrub.Expire_Pending_Orders Summary:');
   dbms_output.put_line('Total 2005 Order_Details with Status 2005 = ' || v_2005_guid_cnt);
   dbms_output.put_line('Total 2005 Order_Details Processed        = ' || v_processed_cnt);
   dbms_output.put_line('Total 2005 Order_Details NOT Processed    = ' || v_not_processed_cnt);

   -- Email Results

   utl_smtp.write_data(c, utl_tcp.CRLF || utl_tcp.CRLF || 
              'Scrub.Expire_Pending_Orders Summary:' || utl_tcp.CRLF ||
              '     Total 2005 Order_Details with Status 2005 = ' || v_2005_guid_cnt || utl_tcp.CRLF ||
              '     Total 2005 Order_Details Expired          = ' || v_processed_cnt || utl_tcp.CRLF ||
              '     Total 2005 Order_Details Left Alone       = ' || v_not_processed_cnt);




   ops$oracle.mail_pkg.close_email(c,v_status,v_message);
   if v_status = 'N' THEN
      v_email_func := 'ops$oracle.mail_pkg.close_email';
      RAISE EMAIL_ERR;
   end if;

   commit;

   EXCEPTION
      WHEN ORD_MAINT_PKG_ERR THEN
         dbms_output.put_line('Unexpected Return from Orders_Maint_Pkg.  Sqlcode=' || sqlcode);
         dbms_output.put_line('SQLERRM=' || sqlerrm);

         --Unfortunately, we can't change the email subject at this point.
         --init_email();
         --send_header('Subject', 'Scrub.Expire_Pending_Orders ERROR!!');

         utl_smtp.write_data(c, utl_tcp.CRLF || utl_tcp.CRLF || '### SCRIPT ENDED IN ERROR!! ###');
         utl_smtp.write_data(c, utl_tcp.CRLF ||
              'Unexpected Error from scrub.orders_maint_pkg.insert_order_disposition ' ||
                   utl_tcp.CRLF || 'SqlCode=' || sqlcode ||
                   utl_tcp.CRLF || 'SQLERRM=' || sqlerrm ||
                   utl_tcp.CRLF || 'Pkg Status=' || v_status ||
                   utl_tcp.CRLF || 'Pkg Msg=' || v_message ||
                  'ORDER GUID=' || v_order_guid || utl_tcp.CRLF ||
                   utl_tcp.CRLF || utl_tcp.CRLF ||
                  'ROLLBACK!!');

         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         ROLLBACK;

      WHEN EMAIL_ERR THEN
         dbms_output.put_line('Unexpected Error Processing Email (Init/Write/Close).');
         dbms_output.put_line('Failing Function = ' || v_email_func || '   MSG=' || v_message);

         -- Go ahead and commit - we may have finished everything else OK and the close_email
         -- failed.  If the open is what failed, there wouldn't be anything to commit anyway.
         COMMIT;

      WHEN ACCOUNT_MAINT_PKG_ERR THEN
         dbms_output.put_line('Unexpected Return from Account_Maint_Pkg.  Sqlcode=' || sqlcode);
         dbms_output.put_line('SQLERRM=' || sqlerrm);

         --Unfortunately, we can't change the email subject at this point.
         --init_email();
         --send_header('Subject', 'Scrub.Expire_Pending_Orders ERROR!!');

         utl_smtp.write_data(c, utl_tcp.CRLF || utl_tcp.CRLF || '### SCRIPT ENDED IN ERROR!! ###');
         utl_smtp.write_data(c, utl_tcp.CRLF ||
              'Unexpected Error from account.account_maint_pkg.update_program_status_by_order ' ||
                   utl_tcp.CRLF || 'SqlCode=' || sqlcode ||
                   utl_tcp.CRLF || 'SQLERRM=' || sqlerrm ||
                   utl_tcp.CRLF || 'Pkg Status=' || v_status ||
                   utl_tcp.CRLF || 'Pkg Msg=' || v_message ||
                  'ORDER GUID=' || v_order_guid || utl_tcp.CRLF ||
                   utl_tcp.CRLF || utl_tcp.CRLF ||
                  'ROLLBACK!!');

         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         ROLLBACK;
         
      WHEN OTHERS THEN
         dbms_output.put_line('Unexpected Error.  Sqlcode=' || sqlcode);
         dbms_output.put_line('SQLERRM=' || sqlerrm);

         --Unfortunately, we can't change the email subject at this point.
         --init_email();
         --send_header('Subject', 'Scrub.Expire_Pending_Orders ERROR!!');

         utl_smtp.write_data(c, utl_tcp.CRLF || utl_tcp.CRLF || '### SCRIPT ENDED IN ERROR!! ###');
         utl_smtp.write_data(c, utl_tcp.CRLF ||
              'Unexpected Error.  Sqlcode=' || sqlcode || utl_tcp.CRLF ||
              'SQLERRM=' || sqlerrm || utl_tcp.CRLF || utl_tcp.CRLF ||
              'ORDER GUID=' || v_order_guid || utl_tcp.CRLF || utl_tcp.CRLF ||
              'ROLLBACK!!');
         ops$oracle.mail_pkg.close_email(c,v_status,v_message);
         ROLLBACK;
END;
/


