create or replace PACKAGE BODY scrub.DISPATCHER_PKG AS

PROCEDURE VIEW_ORDER_DETAILS_STATUS
(
IN_ORDER_GUID                   IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_LINE_NUMBER                  IN ORDER_DETAILS.LINE_NUMBER%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the status of all of the
        order details for the given guid

Input:
        order_detail_id                 number

Output:
        cursor result set containing order detail id and status
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ORDER_DETAIL_ID,
                STATUS
        FROM    ORDER_DETAILS
        WHERE   ORDER_GUID = IN_ORDER_GUID
        AND     LINE_NUMBER = IN_LINE_NUMBER;

END VIEW_ORDER_DETAILS_STATUS;


PROCEDURE UPDATE_ORDERS_STATUS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_STATUS                       IN ORDERS.STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the status of the given
        order guid
Input:
        order_guid                      varchar2
        status                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  ORDERS
SET     STATUS = IN_STATUS
WHERE   ORDER_GUID = IN_ORDER_GUID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_ORDERS_STATUS;


PROCEDURE UPDATE_ORDER_DETAILS_STATUS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_STATUS                       IN ORDER_DETAILS.STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the status of the given
        order detail id
Input:
        order_detail_id                 number
        status                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  ORDER_DETAILS
SET     STATUS = IN_STATUS
WHERE   ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_ORDER_DETAILS_STATUS;


PROCEDURE UPDATE_COMPLETE_ORDER
(
IN_ORDER_GUID                   IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the status of the given
        order guid to 1008 - complete if all order details have a status
        of 2008 - processed removed item, 2006 - processed item.
        If not INCOMPLETE ORDER is returned in the message.
Input:
        order_guid                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message) -
                                        INCOMPLETE ORDER
-----------------------------------------------------------------------------*/

V_COUNT         NUMBER := 0;
V_SEND_EMAIL_CONFIRMATION VARCHAR2(1) := FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE ('EMAIL_CONFIRMATION','SEND_EMAIL_CONFIRMATION');
BEGIN

-- check if any order details for the given guid is not 2008, 2006
SELECT  DISTINCT
        'N',
        'INCOMPLETE ORDER'
INTO    OUT_STATUS,
        OUT_MESSAGE
FROM    ORDER_DETAILS
WHERE   ORDER_GUID = IN_ORDER_GUID
AND     STATUS NOT IN ('2004', '2006');

EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        -- update the order status to complete if all items are processed or removed
        UPDATE_ORDERS_STATUS (IN_ORDER_GUID, '1008', OUT_STATUS, OUT_MESSAGE);

        -- check if an email confirmation should be sent out for the processed items
        IF OUT_STATUS = 'Y' THEN
                SELECT  COUNT (1)
                INTO    V_COUNT
                FROM    ORDER_DETAILS
                WHERE   ORDER_GUID = IN_ORDER_GUID
                AND     STATUS = '2006';

                -- also check the global parms to see if an email comfirmation should be sent out.
                IF V_COUNT > 0 AND
                   V_SEND_EMAIL_CONFIRMATION = 'Y' THEN
                        OUT_MESSAGE := 'SEND EMAIL CONFIRMATION';
                ELSE
                        OUT_MESSAGE := 'DO NOT SEND EMAIL CONFIRMATION';
                END IF;
        END IF;

END;    -- end no data found exception

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_COMPLETE_ORDER;

FUNCTION GET_ORDER_DETAIL_COUNT
(
IN_ORDER_GUID                   IN ORDER_DETAILS.ORDER_GUID%TYPE
)
RETURN NUMBER

AS

/*-----------------------------------------------------------------------------
Description:
        Return the number of line items for a given order.

Input:
        order_guid                      varchar2

Output:
        number of order lines           number
-----------------------------------------------------------------------------*/

V_COUNT         NUMBER := 0;

BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    ORDER_DETAILS
WHERE   ORDER_GUID = IN_ORDER_GUID;

RETURN V_COUNT;

END GET_ORDER_DETAIL_COUNT;


PROCEDURE UPDATE_ORDER_DETAILS_HP_SEQ
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_HP_SEQUENCE                  IN ORDER_DETAILS.HP_SEQUENCE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the sequence number of the given
        order detail id
Input:
        order_detail_id                 number
        hp_sequence                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE  ORDER_DETAILS
SET     HP_SEQUENCE = IN_HP_SEQUENCE
WHERE   ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_ORDER_DETAILS_HP_SEQ;


PROCEDURE VIEW_NEWSLETTER_ORDERS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning orders and the buyer
        newsletter indicator

Input:
        none

Output:
        cursor result set containing order guid
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT O.ORDER_GUID
        FROM    ORDERS O
        JOIN    BUYER_EMAILS BE
        ON      O.BUYER_EMAIL_ID = BE.BUYER_EMAIL_ID
        WHERE   BE.NEWSLETTER IN ('Y', 'N')
        AND     O.DNIS_CODE IS NOT NULL;

END VIEW_NEWSLETTER_ORDERS;


PROCEDURE UPDATE_NEWSLETTERS
(
IN_BUYER_EMAIL_ID               IN BUYER_EMAILS.BUYER_EMAIL_ID%TYPE,
IN_NEWSLETTER                   IN BUYER_EMAILS.NEWSLETTER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the newsletter indicator
        for the buyer email record.

Input:
        buyer_email_id          number
        newsletter              varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  BUYER_EMAILS
SET     NEWSLETTER = IN_NEWSLETTER
WHERE   BUYER_EMAIL_ID = IN_BUYER_EMAIL_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_NEWSLETTERS;


/******************************************************************************
THE FOLLOWING PROCEDURES ARE FOR HP REPLACEMENT PHASEIII - DISPATCHING
ORDERS FROM SCRUB TO CLEAN
******************************************************************************/

PROCEDURE DISPATCH_BUYER
(
IN_BUYER_ID                             IN BUYERS.BUYER_ID%TYPE,
IN_ORDER_GUID                           IN ORDERS.ORDER_GUID%TYPE,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for dispatching the buyer to CLEAN.

Input:
        buyer_id                        number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

-- cursor to check if the buyer is already tied to a customer and lock the buyer record
-- to prevent multiple processes from processing the same buyer at the same time.
CURSOR tie_cur IS
        SELECT  b.clean_customer_id
        FROM    buyers b
        WHERE   b.buyer_id = in_buyer_id
        FOR UPDATE OF b.clean_customer_id;

-- cursor to get the concatenated id for the buyer, this assumes that the
-- buyer has only one address, if not the last one found will be used
CURSOR concat_cur IS
        SELECT  clean.customer_query_pkg.get_customer_concat_id (b.first_name, b.last_name, ba.address_line_1, ba.postal_code) concat_id
        FROM    buyers b
        JOIN    buyer_addresses ba
        ON      b.buyer_id = ba.buyer_id
        WHERE   b.buyer_id = in_buyer_id
        ORDER BY buyer_address_id DESC;

-- cursor to check if other buyers are tied to the clean customer
CURSOR check_cur (p_customer_id number) IS
        SELECT  1
        FROM    buyers
        WHERE   clean_customer_id = p_customer_id;

v_customer_id           number;
v_concat_id             varchar2(82);
v_check                 number;

BEGIN

-- check if the buyer is already tied to clean
OPEN tie_cur;
FETCH tie_cur INTO v_customer_id;

-- if the buyer is not yet tied then dispatch the customer
-- otherwise, no customer dispatch is necessary
IF v_customer_id IS NULL THEN

        OPEN concat_cur;
        FETCH concat_cur INTO v_concat_id;
        CLOSE concat_cur;

        -- find if the customer exists using the concat_id
        v_customer_id := CLEAN.CUSTOMER_QUERY_PKG.GET_CUSTOMER_ID_BY_CONCAT_ID (v_concat_id);

        -- if a customer record is found then update that customer
        -- otherwise, insert a new customer and tie the buyer record to that customer
        IF v_customer_id IS NOT NULL THEN
                -- initialize the out_status
                out_status := 'Y';

                -- check if another buyers are tied to the found customer
                -- dedup if exists
                OPEN check_cur (v_customer_id);
                FETCH check_cur INTO v_check;
                CLOSE check_cur;

                IF v_check IS NOT NULL THEN
                        -- dedup the new buyer update the old buyers record with the new buyer data.
                        -- the old buyer record is already tied to the clean customer so there is no need to tie it again
                        BUYERS_MAINT_PKG.DEDUP_ORDER_BUYER_INFO
                        (
                                IN_ORDER_GUID=>in_order_guid,
                                IN_CLEAN_CUSTOMER_ID=>v_customer_id,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );
                ELSE
                        -- tie the scrub buyer to the clean customer
                        -- data conversion will make it possible that an existing customer record
                        -- will not be tied to a scrub buyer
                        UPDATE  buyers
                        SET     clean_customer_id = v_customer_id
                        WHERE   CURRENT OF tie_cur;
                END IF;

                IF out_status = 'Y' THEN
                        -- update the existing customer in CLEAN
                        CLEAN.BUYER_CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER_FROM_BUYER (in_buyer_id, in_order_guid, v_customer_id, out_status, out_message);
                END IF;
        ELSE
                -- insert the new customer in CLEAN
                CLEAN.BUYER_CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER_FROM_BUYER (in_buyer_id, in_order_guid, v_customer_id, out_status, out_message);

                -- tie the scrub buyer to the clean customer
                IF out_status = 'Y' THEN
                        UPDATE  buyers
                        SET     clean_customer_id = v_customer_id
                        WHERE   CURRENT OF tie_cur;
                END IF;
        END IF;

ELSE
        out_status := 'Y';

END IF;

CLOSE tie_cur;
COMMIT;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DISPATCH_BUYER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

        IF tie_cur%isopen THEN
                CLOSE tie_cur;
        END IF;
END DISPATCH_BUYER;


PROCEDURE DISPATCH_RECIPIENT
(
IN_ORDER_DETAIL_ID                      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_RECIPIENT_ID                        OUT NUMBER,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for dispatching the recipient to CLEAN.

Input:
        order_detail_id                 number

Output:
        recipient_id                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

-- cursor to get the concatenated id for the recipient
CURSOR recipient_cur IS
        select  r.first_name,
                r.last_name,
                ra.name business_name,
                ra.address_line_1,
                ra.address_line_2,
                ra.city,
                ra.state_province,
                ra.postal_code,
                ra.county,
                ra.country,
                ra.address_type,
                clean.customer_query_pkg.get_customer_concat_id (r.first_name, r.last_name, ra.address_line_1, ra.postal_code) concat_id,
                clean.customer_query_pkg.get_customer_id_by_concat_id (clean.customer_query_pkg.get_customer_concat_id (r.first_name, r.last_name, ra.address_line_1, ra.postal_code)) customer_id,
                ra.info business_info
        from    recipients r
        join    recipient_addresses ra
        on      r.recipient_id = ra.recipient_id
        join    order_details od
        on      od.recipient_id = r.recipient_id
        and     od.recipient_address_id = ra.recipient_address_id
        where   od.order_detail_id = in_order_detail_id;

recipient_row           recipient_cur%rowtype;

CURSOR recipient_phone_cur IS
        select  rp.phone_number,
                rp.extension,
                decode (rp.phone_type, 'HOME', 'Evening', 'Day') phone_type
        from    recipient_phones rp
        join    order_details od
        on      od.recipient_id = rp.recipient_id
        where   od.order_detail_id = in_order_detail_id
        and     rp.phone_number is not null;

v_concat_id             varchar2(82);
v_phone_id              number;
v_vip_customer_flag		varchar2(1);

BEGIN

-- get the recipient info
OPEN recipient_cur;
FETCH recipient_cur INTO recipient_row;
CLOSE recipient_cur;

-- if a clean customer record already exists for the recipient then update the customer record
-- set the scrub indicator to update the buyer tied to the customer record if the recipient is
-- also a buyer.


	BEGIN
	    select VIP_CUSTOMER into v_vip_customer_flag 
	    from clean.customer 
	    where customer_id = recipient_row.customer_id;
	EXCEPTION WHEN OTHERS THEN
  	    v_vip_customer_flag:= 'N';
	END;

IF recipient_row.customer_id is not null then
        out_recipient_id := recipient_row.customer_id;
	
        -- update the existing customer record
        CLEAN.CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER
        (
                IN_CUSTOMER_ID=>recipient_row.customer_id,
                IN_FIRST_NAME=>recipient_row.first_name,
                IN_LAST_NAME=>recipient_row.last_name,
                IN_BUSINESS_NAME=>recipient_row.business_name,
                IN_ADDRESS_1=>recipient_row.address_line_1,
                IN_ADDRESS_2=>recipient_row.address_line_2,
                IN_CITY=>recipient_row.city,
                IN_STATE=>recipient_row.state_province,
                IN_ZIP_CODE=>recipient_row.postal_code,
                IN_COUNTRY=>recipient_row.country,
                IN_ADDRESS_TYPE=>recipient_row.address_type,
                IN_PREFERRED_CUSTOMER=>null,
                IN_BUYER_INDICATOR=>null,
                IN_RECIPIENT_INDICATOR=>'Y',
                IN_ORIGIN_ID=>null,
                IN_FIRST_ORDER_DATE=>null,
                IN_UPDATED_BY=>'SYS',
                IN_COUNTY=>recipient_row.county,
                IN_UPDATE_SCRUB=>'Y',
                IN_BUSINESS_INFO=>recipient_row.business_info,
                IN_VIP_CUSTOMER => v_vip_customer_flag,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        IF out_status = 'Y' THEN

                -- update the customer phones
                FOR rp IN recipient_phone_cur LOOP
                        CLEAN.CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER_PHONES
                        (
                                IN_CUSTOMER_ID=>recipient_row.customer_id,
                                IN_PHONE_TYPE=>rp.phone_type,
                                IN_PHONE_NUMBER=>rp.phone_number,
                                IN_EXTENSION=>rp.extension,
                                IN_PHONE_NUMBER_TYPE=>null,
                                IN_SMS_OPT_IN=>'N',
                                IN_UPDATED_BY=>'SYS',
                                IN_UPDATE_SCRUB=>'Y',
                                OUT_PHONE_ID=>v_phone_id,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );
                END LOOP;
        END IF;

ELSE

        -- insert a new customer record for the recipient
        CLEAN.CUSTOMER_MAINT_PKG.INSERT_CUSTOMER
        (
                IN_FIRST_NAME=>recipient_row.first_name,
                IN_LAST_NAME=>recipient_row.last_name,
                IN_BUSINESS_NAME=>recipient_row.business_name,
                IN_ADDRESS_1=>recipient_row.address_line_1,
                IN_ADDRESS_2=>recipient_row.address_line_2,
                IN_CITY=>recipient_row.city,
                IN_STATE=>recipient_row.state_province,
                IN_ZIP_CODE=>recipient_row.postal_code,
                IN_COUNTRY=>recipient_row.country,
                IN_ADDRESS_TYPE=>recipient_row.address_type,
                IN_PREFERRED_CUSTOMER=>null,
                IN_BUYER_INDICATOR=>'N',
                IN_RECIPIENT_INDICATOR=>'Y',
                IN_ORIGIN_ID=>null,
                IN_FIRST_ORDER_DATE=>null,
                IN_CREATED_BY=>'SYS',
                IN_COUNTY=>recipient_row.county,
                IN_BUSINESS_INFO=>recipient_row.business_info,
                OUT_CUSTOMER_ID=>out_recipient_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        IF out_status = 'Y' THEN

                -- update the customer phones
                FOR rp IN recipient_phone_cur LOOP
                        CLEAN.CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER_PHONES
                        (
                                IN_CUSTOMER_ID=>out_recipient_id,
                                IN_PHONE_TYPE=>rp.phone_type,
                                IN_PHONE_NUMBER=>rp.phone_number,
                                IN_EXTENSION=>rp.extension,
                                IN_PHONE_NUMBER_TYPE=>null,
                                IN_SMS_OPT_IN=>'N',
                                IN_UPDATED_BY=>'SYS',
                                IN_UPDATE_SCRUB=>'Y',
                                OUT_PHONE_ID=>v_phone_id,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );
                END LOOP;
        END IF;

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DISPATCH_RECIPIENT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DISPATCH_RECIPIENT;

PROCEDURE DISPATCH_AVS_ADDRESS 
(
IN_ORDER_DETAIL_ID                      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_AVS_ADDRESS_ID                      OUT NUMBER
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting avs address details into clean 

Input:
        order_detail_id			number

Output:
        avs_address_id                  number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


CURSOR avs_address_cur IS
	SELECT  AA.AVS_ADDRESS_ID,		
		AA.ADDRESS,
		AA.CITY,
		AA.STATE,
		AA.ZIP,
		AA.COUNTRY,
		AA.LATITUDE,
		AA.LONGITUDE,
		AA.ENTITY_TYPE,
		AA.OVERRIDE_FLAG,
		AA.AVS_RESULT,
		AA.AVS_PERFORMED,
		AA.AVS_PERFORMED_ORIGIN,
                AA.CREATED_ON
	FROM    scrub.AVS_ADDRESS AA
	JOIN    ORDER_DETAILS OD
	ON      AA.RECIPIENT_ID = OD.RECIPIENT_ID
	WHERE   OD.order_detail_id = IN_ORDER_DETAIL_ID order by AA.created_on desc;

avs_address_row           avs_address_cur%rowtype;
OUT_STATUS                              VARCHAR2(200);
OUT_MESSAGE                             VARCHAR2(200);

BEGIN

out_avs_address_id := null;
out_status := 'N';

OPEN avs_address_cur;
FETCH avs_address_cur INTO avs_address_row;
CLOSE avs_address_cur;

out_avs_address_id := avs_address_row.avs_address_id;

    IF clean.order_query_pkg.avs_address_exists (avs_address_row.avs_address_id) = 'N' THEN
        CLEAN.ORDER_MAINT_PKG.INSERT_AVS_ADDRESS
        (
                IN_AVS_ADDRESS_ID=>avs_address_row.avs_address_id,
                IN_ADDRESS=>avs_address_row.address,
                IN_CITY=>avs_address_row.city,
                IN_STATE=>avs_address_row.state,
                IN_ZIP=>avs_address_row.zip,
                IN_COUNTRY=>avs_address_row.country,
                IN_LATITUDE=>avs_address_row.latitude,
                IN_LONGITUDE=>avs_address_row.longitude,
                IN_ENTITY_TYPE=>avs_address_row.entity_type,
                IN_OVERRIDE_FLAG=>avs_address_row.override_flag,
                IN_AVS_RESULT=>avs_address_row.avs_result,
                IN_AVS_PERFORMED=>avs_address_row.avs_performed,
                IN_AVS_PERFORMED_ORIGIN=>avs_address_row.avs_performed_origin,
                IN_CREATED_ON=>avs_address_row.created_on,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message);
 
    ELSE
        CLEAN.ORDER_MAINT_PKG.UPDATE_AVS_ADDRESS
        (
                IN_AVS_ADDRESS_ID=>avs_address_row.avs_address_id,
                IN_ADDRESS=>avs_address_row.address,
                IN_CITY=>avs_address_row.city,
                IN_STATE=>avs_address_row.state,
                IN_ZIP=>avs_address_row.zip,
                IN_COUNTRY=>avs_address_row.country,
                IN_LATITUDE=>avs_address_row.latitude,
                IN_LONGITUDE=>avs_address_row.longitude,
                IN_ENTITY_TYPE=>avs_address_row.entity_type,
                IN_OVERRIDE_FLAG=>avs_address_row.override_flag,
                IN_AVS_RESULT=>avs_address_row.avs_result,
                IN_AVS_PERFORMED=>avs_address_row.avs_performed,
                IN_AVS_PERFORMED_ORIGIN=>avs_address_row.avs_performed_origin,
                IN_CREATED_ON=>avs_address_row.created_on,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message);
 
    END IF;
IF out_status = 'Y' THEN
        out_avs_address_id := avs_address_row.avs_address_id;        
ELSE
        out_avs_address_id := null;       
END IF;

EXCEPTION WHEN OTHERS THEN
        out_avs_address_id := null;       

END DISPATCH_AVS_ADDRESS;

PROCEDURE DISPATCH_PAYMENT
(
IN_ORDER_DETAIL_ID                      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for dispatching the payment for the
        order.

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_cc_id         number;
v_redeem_status varchar2(100);
v_ap_account_exists varchar2(1) := 'N';
v_stat    varchar2(1) := 'Y';
v_mess    varchar2(2000);
v_payload varchar2(2000);
v_delay_in_seconds number;

CURSOR cc_cur (p_credit_card_id number ) IS
        select  c.credit_card_id,
                b.clean_customer_id,
                c.cc_type,
                c.cc_number,
                c.key_name,
                c.cc_number_masked,
                c.cc_expiration,
                c.billing_name,
                c.gift_card_pin,
                ba.address_line_1 as billing_address_line_1,
                ba.address_line_2 as billing_address_line_2,
                ba.city as billing_city,
                ba.state_province as billing_state_province,
                ba.postal_code as billing_postal_code,
                ba.country as billing_country
        from    credit_cards c
        join    buyers b
        on      c.buyer_id = b.buyer_id
        join    buyer_addresses ba
        on      c.buyer_id = ba.buyer_id
        where   c.credit_card_id = p_credit_card_id
        order by ba.buyer_address_id desc;


CURSOR payment_cur IS
        select  p.payment_id,
                p.order_guid,
                p.payment_type,
                to_number (p.amount) amount,
                p.credit_card_id,
                upper (p.gift_certificate_id) gc_coupon_number,
                p.auth_result,
                p.auth_number,
                p.avs_code,
                p.acq_reference_number,
                p.aafes_ticket,
                p.invoice_number,
                substr (p.acq_reference_number, instr(p.acq_reference_number||'abcdef', 'a')+1, instr(p.acq_reference_number||'abcdef', 'b')-instr(p.acq_reference_number||'abcdef', 'a')-1) auth_char_indicator,
                substr (p.acq_reference_number, instr(p.acq_reference_number||'abcdef', 'b')+1, instr(p.acq_reference_number||'abcdef', 'c')-instr(p.acq_reference_number||'abcdef', 'b')-1) transaction_id,
                substr (p.acq_reference_number, instr(p.acq_reference_number||'abcdef', 'c')+1, instr(p.acq_reference_number||'abcdef', 'd')-instr(p.acq_reference_number||'abcdef', 'c')-1) validation_code,
                substr (p.acq_reference_number, instr(p.acq_reference_number||'abcdef', 'd')+1, instr(p.acq_reference_number||'abcdef', 'e')-instr(p.acq_reference_number||'abcdef', 'd')-1) auth_source_code,
                substr (p.acq_reference_number, instr(p.acq_reference_number||'abcdef', 'e')+1, instr(p.acq_reference_number||'abcdef', 'f')-instr(p.acq_reference_number||'abcdef', 'e')-1) response_code,
                /* The 'f' portion of the data, which is a product identifier, is only used for the credit card settlement file and is parsed out when the settlement file is created.  Parse code was left for reference.  substr (p.acq_reference_number, instr(p.acq_reference_number||'abcdef', 'f')+1) product_identifier, */
                decode (p.auth_number, null, null, to_date (substr (o.order_date, 5, length (o.order_date) - 12) || substr (o.order_date, -5), 'mon dd hh24:mi:ss yyyy')) auth_date,
                (select external_order_total from order_details where order_detail_id = in_order_detail_id) order_total_amount,
                decode ( (select og.origin_type from ftd_apps.origins og where og.origin_id = upper (o.order_origin)), 'phone', 'Y', 'N' ) is_weboe_order,
          p.ap_account_id,
          p.ap_auth_txt,
          p.nc_approval_identity_id,
                p.nc_type_code,
          p.nc_reason_code,
          p.nc_order_detail_id,
                to_number (p.miles_points_amt) miles_points_amt,
    p.csc_response_code,
    p.csc_validated_flag,
    p.csc_failure_cnt,
    to_number(p.wallet_indicator) wallet_indicator,
    cc_auth_provider,
    initial_auth_amount,
    p.cardinal_verified_flag,
    p.route,
    p.token_id,
    p.authorization_transaction_id,
    o.master_order_number merchant_ref_id,
    od.line_number
        from    payments p
        join    order_details od
        on      p.order_guid = od.order_guid
        join    orders o
        on      p.order_guid = o.order_guid
        where   od.order_detail_id = in_order_detail_id and p.amount > 0;

CURSOR ap_account_cur (in_ap_account_id ap_accounts.AP_ACCOUNT_ID%TYPE) IS
      SELECT aa.ap_account_id,
             aa.ap_account_txt,
             aa.payment_method_id,
             aa.key_name,
             b.clean_customer_id
        FROM ap_accounts aa
        JOIN buyers b
        on aa.buyer_id = b.buyer_id
       WHERE aa.ap_account_id = in_ap_account_id;

ap_account_rec  ap_account_cur%ROWTYPE;

BEGIN

out_status := 'Y';


FOR p IN payment_cur LOOP
v_cc_id := null;

        IF p.credit_card_id is not null THEN
                v_cc_id := null;
                FOR c IN cc_cur (p.credit_card_id) LOOP
                        CLEAN.ORDER_MAINT_PKG.UPDATE_UNIQUE_CC_CRYPT
                        (
                                IN_CC_TYPE=>c.cc_type,
                                IN_CC_NUMBER=>c.cc_number,
                                IN_CC_EXPIRATION=>c.cc_expiration,
                                IN_NAME=>c.billing_name,
                                IN_PIN=>c.gift_card_pin,
                                IN_ADDRESS_LINE_1=>c.billing_address_line_1,
                                IN_ADDRESS_LINE_2=>c.billing_address_line_2,
                                IN_CITY=>c.billing_city,
                                IN_STATE=>c.billing_state_province,
                                IN_ZIP_CODE=>c.billing_postal_code,
                                IN_COUNTRY=>c.billing_country,
                                IN_UPDATED_BY=>'SYS',
                                IN_CUSTOMER_ID=>c.clean_customer_id,
                                IN_KEY_NAME=>c.key_name,
                                IN_CC_NUMBER_MASKED=>c.cc_number_masked,
                                OUT_CC_ID=>v_cc_id,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );

                        IF out_status = 'N' THEN
                                exit;
                        END IF;
                END LOOP;
        END IF;

        -- redeem the gift certificate
        -- weboe - can be used on more than one order in the cart
        -- novator can only be used on one order in the cart and the order total must exceed the
        -- amount of the gift certificate
        -- **************************************************************
        -- ***************ISSUE 12647 supersedes above*******************
        -- **************************************************************
        -- For a website multi-order cart where each item amount <  gc issue amount, the following code never gets invoked.  The payment record 
        -- (scrub and clean) for these order showed GC issue amount = credit amount; however, the order bills and gc coupon history table 
        -- did not show the correct amounts, and the GC was not marked as redeemed correctly. 
        -- Thus, on the Refunds page, CSR would never see a GC record, and ended up over-refunding the CC - this caused EOD issues, and customer
        -- ended up not getting a refund
        -- Decision was made to spread the GC across cart, just like we do for Apollo orders. 
        IF p.payment_type = 'GC' 
        THEN
                          
		v_payload := 'REDEEM-GIFT-CODE|' || p.gc_coupon_number || '|' || in_order_detail_id || '|SYS|' || p.order_total_amount || '|' || p.line_number || '|0';  
        IF p.line_number > 1 THEN
        	--add a delay
        	events.post_a_message_flex('OJMS.GIFT_CODE_MAINT',p.gc_coupon_number,v_payload,(p.line_number * 5),OUT_STATUS,OUT_MESSAGE);
        ELSE
         	events.post_a_message_flex('OJMS.GIFT_CODE_MAINT',p.gc_coupon_number,v_payload,0,OUT_STATUS,OUT_MESSAGE);
        END IF;
END IF;

        --If ap account is present
        IF p.ap_account_id is not null then

            BEGIN
    -- Check if ap_account_id exists in clean
    SELECT 'Y'
    INTO v_ap_account_exists
    FROM clean.ap_accounts
    WHERE ap_account_id = p.ap_account_id;

            EXCEPTION WHEN NO_DATA_FOUND THEN
              v_ap_account_exists := 'N';
            END;


            IF v_ap_account_exists = 'N' THEN

        OPEN ap_account_cur (p.ap_account_id);
        FETCH ap_account_cur INTO ap_account_rec;

        clean.customer_maint_pkg.INSERT_AP_ACCOUNT
        (
          p.ap_account_id,
          ap_account_rec.ap_account_txt,
          ap_account_rec.payment_method_id,
          ap_account_rec.clean_customer_id,
          ap_account_rec.key_name,
          out_status,
          out_message
        );

        IF out_status = 'N' THEN
          exit;
        END IF;
      END IF;

        END IF;


        CLEAN.ORDER_MAINT_PKG.UPDATE_PAYMENTS
        (
                IN_ORDER_GUID=>p.order_guid,
                IN_ADDITIONAL_BILL_ID=>null,
                IN_PAYMENT_TYPE=>p.payment_type,
                IN_CC_ID=>v_cc_id,
                IN_UPDATED_BY=>'SYS',
                IN_AUTH_RESULT=>p.auth_result,
                IN_AUTH_NUMBER=>p.auth_number,
                IN_AVS_CODE=>p.avs_code,
                IN_ACQ_REFERENCE_NUMBER=>p.acq_reference_number,
                IN_GC_COUPON_NUMBER=>p.gc_coupon_number,
                IN_AUTH_OVERRIDE_FLAG=>null,
                IN_CREDIT_AMOUNT=>p.amount,
                IN_DEBIT_AMOUNT=>null,
                IN_PAYMENT_INDICATOR=>'P',
                IN_REFUND_ID=>null,
                IN_AUTH_DATE=>p.auth_date,
                IN_AUTH_CHAR_INDICATOR=>p.auth_char_indicator,
                IN_TRANSACTION_ID=>p.transaction_id,
                IN_VALIDATION_CODE=>p.validation_code,
                IN_AUTH_SOURCE_CODE=>p.auth_source_code,
                IN_RESPONSE_CODE=>p.response_code,
                IN_AAFES_TICKET_NUMBER=>p.aafes_ticket,
                IN_AP_ACCOUNT_ID=>p.ap_account_id,
                IN_AP_AUTH_TXT=>p.ap_auth_txt,
                IN_NC_APPROVAL_IDENTITY_ID=>p.nc_approval_identity_id,
                IN_NC_TYPE_CODE=>p.nc_type_code,
                IN_NC_REASON_CODE=>p.nc_reason_code,
                IN_NC_ORDER_DETAIL_ID=>p.nc_order_detail_id,
                IN_MILES_POINTS_CREDIT_AMT=>p.miles_points_amt,
                IN_MILES_POINTS_DEBIT_AMT=>null,
    IN_CSC_RESPONSE_CODE=>p.csc_response_code,
    IN_CSC_VALIDATED_FLAG=>p.csc_validated_flag,
    IN_CSC_FAILURE_CNT=>p.csc_failure_cnt,
    IN_BILL_STATUS=>null,
    IN_BILL_DATE=>null,
    IN_WALLET_INDICATOR=>p.wallet_indicator,
    IN_CC_AUTH_PROVIDER=>p.cc_auth_provider,
    IN_INITIAL_AUTH_AMOUNT=>p.initial_auth_amount,
    IN_PAYMENT_EXT_INFO=>NULL,
    IN_IS_VOICE_AUTH=>NULL,
    IN_CARDINAL_VERIFIED_FLAG=>p.cardinal_verified_flag,
    IN_ROUTE=>p.route,
    IN_TOKEN_ID=>p.token_id,
    IN_AUTH_TRANSACTION_ID=>p.authorization_transaction_id,
    IN_MERCHANT_REF_ID=>p.merchant_ref_id,
                IO_PAYMENT_ID=>p.payment_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
        IF out_status = 'Y' THEN
            INSERT INTO CLEAN.PAYMENTS_EXT(PAYMENT_ID,AUTH_PROPERTY_NAME,AUTH_PROPERTY_VALUE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
            SELECT SPE.PAYMENT_ID,SPE.AUTH_PROPERTY_NAME,SPE.AUTH_PROPERTY_VALUE,SYSDATE,'DISPATCHER',SYSDATE,'DISPATCHER'
            FROM SCRUB.PAYMENTS_EXT SPE 
            WHERE SPE.PAYMENT_ID=p.payment_id 
            AND NOT EXISTS (SELECT 1 FROM clean.payments_ext cpe WHERE cpe.payment_id=SPE.PAYMENT_ID);
        ELSIF out_status = 'N' THEN
                exit;
        END IF;
END LOOP;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DISPATCH_PAYMENT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DISPATCH_PAYMENT;

PROCEDURE DISPATCH_ORDER_IDS
(
IN_ORDER_DETAIL_ID                      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_ORDER_GUID                           IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting orders ids,
        order guid, master order number, order detail id and external order
        number into clean with the status of in-scrub

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/


CURSOR order_cur IS
        select  o.order_guid,
                o.master_order_number,
                od.external_order_number,
                b.clean_customer_id customer_id,
                o.source_code,
                fs.company_id,
                to_date (substr (o.order_date, 5, length (o.order_date) - 12) || substr (o.order_date, -5), 'mon dd hh24:mi:ss yyyy') order_date,
                o.loss_prevention_indicator,
                o.CHARACTER_MAPPING_FLAG,
        o.language_id, is_joint_cart
          from  order_details od, orders o, buyers b, ftd_apps.source fs
         where  od.order_detail_id = in_order_detail_id
           and  od.order_guid = o.order_guid
           and  o.buyer_id = b.buyer_id
           and  o.source_code = fs.source_code;

v_order_detail_id       order_details.order_detail_id%type := in_order_detail_id;
v_order_disp_code       varchar2(20) := 'In-Scrub';
v_order_guid            orders.order_guid%type;
v_lock                  varchar2(1) := 'N';

BEGIN

IF clean.order_query_pkg.order_detail_exists (in_order_detail_id) = 'N' THEN

        FOR o IN order_cur LOOP
                out_status := 'Y';

                v_order_guid := o.order_guid;           -- this is used to create a lock

                select 'Y'
                  into v_lock
                  from orders
                 where order_guid = v_order_guid
         for update of order_guid;

                -- check if the order record exists
                -- this could be because the cart has mulitple orders and one of the orders was already dispatched
                IF clean.order_query_pkg.order_exists (in_order_guid) = 'N' THEN
                        CLEAN.ORDER_MAINT_PKG.INSERT_ORDERS
                        (
                                IN_ORDER_GUID=>o.order_guid,
                                IN_MASTER_ORDER_NUMBER=>o.master_order_number,
                                IN_CUSTOMER_ID=>o.customer_id,
                                IN_MEMBERSHIP_ID=>null,
                                IN_COMPANY_ID=>o.company_id,
                                IN_SOURCE_CODE=>o.source_code,
                                IN_ORIGIN_ID=>null,
                                IN_ORDER_DATE=>o.order_date,
                                IN_CREATED_BY=>'SYS',
                                IN_LOSS_PREVENTION_INDICATOR=>o.loss_prevention_indicator,
                                IN_EMAIL_ID=>null,
                                IN_ORDER_TAKEN_BY=>null,
                                IN_PARTNERSHIP_BONUS_POINTS=>null,
                                IN_FRAUD_INDICATOR=>null,
                                IN_ARIBA_BUYER_ASN_NUMBER=>null,
                                IN_ARIBA_BUYER_COOKIE=>null,
                                IN_ARIBA_PAYLOAD=>null,
                                IN_CO_BRAND_CREDIT_CARD_CODE=>null,
                                IN_WEBOE_DNIS_ID=>null,
                                IN_ORDER_TAKEN_CALL_CENTER=>null,
                                IN_MP_REDEMPTION_RATE_AMT=>null,
                                IN_BUYER_SIGNED_IN_FLAG => null,
                                IN_CHARACTER_MAPPING_FLAG => o.CHARACTER_MAPPING_FLAG,
                				IN_LANGUAGE_ID =>o.language_id,
                				IN_JOINT_CART => o.is_joint_cart,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );
                END IF;

                IF out_status = 'Y' THEN
                        CLEAN.ORDER_MAINT_PKG.INSERT_ORDER_DETAILS
                        (
                                IN_ORDER_GUID=>o.order_guid,
                                IN_EXTERNAL_ORDER_NUMBER=>o.external_order_number,
                                IN_HP_ORDER_NUMBER=>null,
                                IN_SOURCE_CODE=>null,
                                IN_DELIVERY_DATE=>null,
                                IN_RECIPIENT_ID=>null,
                                IN_AVS_ADDRESS_ID=>null,
                                IN_PRODUCT_ID=>null,
                                IN_QUANTITY=>null,
                                IN_COLOR_1=>null,
                                IN_COLOR_2=>null,
                                IN_SUBSTITUTION_INDICATOR=>null,
                                IN_SAME_DAY_GIFT=>null,
                                IN_OCCASION=>null,
                                IN_CARD_MESSAGE=>null,
                                IN_CARD_SIGNATURE=>null,
                                IN_SPECIAL_INSTRUCTIONS=>null,
                                IN_RELEASE_INFO_INDICATOR=>null,
                                IN_FLORIST_ID=>null,
                                IN_SHIP_METHOD=>null,
                                IN_SHIP_DATE=>null,
                                IN_ORDER_DISP_CODE=>v_order_disp_code,
                                IN_CREATED_BY=>'SYS',
                                IN_SCRUBBED_ON=>null,
                                IN_SCRUBBED_BY=>null,
                                IN_DELIVERY_DATE_RANGE_END=>null,
                                IN_ARIBA_UNSPSC_CODE=>null,
                                IN_ARIBA_PO_NUMBER=>null,
                                IN_ARIBA_AMS_PROJECT_CODE=>null,
                                IN_ARIBA_COST_CENTER=>null,
                                IN_SIZE_INDICATOR=>null,
                                IN_MILES_POINTS=>null,
                                IN_SUBCODE=>null,
                                IN_MEMBERSHIP_ID=>null,
                                IN_PERSONALIZATION_DATA=>null,
                                IN_QMS_RESULT_CODE=>null,
                                IN_PERSONAL_GREETING_ID=>null,
                                IN_BIN_SOURCE_CHANGED_FLAG=>null,
                                IO_ORDER_DETAIL_ID=>v_order_detail_id,
                                IN_FREE_SHIPPING_FLAG => null,
                                IN_APPLY_SURCHARGE_CODE=>'OFF',
                                IN_SURCHARGE_DESCRIPTION=>null,
                                IN_DISPLAY_SURCHARGE=> 'N',
                                IN_SEND_SURCHARGE_TO_FLORIST=> 'N',
                				IN_ORIGINAL_ORDER_HAS_SDU => null,
								IN_PC_GROUP_ID => null,
								IN_PC_MEMBERSHIP_ID => null,
								IN_PC_FLAG => 'N',
								IN_DERIVED_VIP_FLAG => null,
								IN_TIME_OF_SERVICE => null,
								IN_LEGACY_ID => null,
                                OUT_STATUS=>out_status,
                                OUT_MESSAGE=>out_message
                        );
                ELSE
                        exit;
                END IF;
        END LOOP;
ELSE
        out_status := 'Y';
END IF;

IF out_status = 'Y' THEN
        commit;
ELSE
        rollback;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DISPATCH_ORDER_IDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DISPATCH_ORDER_IDS;

PROCEDURE DISPATCH_ORDER_FULL
(
IN_ORDER_DETAIL_ID                      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_ORDER_GUID                           IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting orders ids,
        order guid, master order number, order detail id and external order
        number into clean with the status of in-scrub

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

v_order_disp_code varchar2(20) := 'Validated';
v_order_exists boolean := FALSE;
v_customer_id  number;
v_membership_id number;
v_viewed_csr    varchar2(100);
v_wholesale_service_fee number;
V_PG_PHONE   varchar2(50);
V_PG_COMMENT varchar2(1000);
V_PG_FULL_COMMENT varchar2(1000);
v_order_guid      orders.order_guid%type;
v_lock            varchar2(1) := 'N';

CURSOR order_cur IS
         select  o.order_guid,
                 o.master_order_number,
                 b.clean_customer_id customer_id,
                 o.order_total,
                 o.products_total,
                 o.tax_total,
                 o.service_fee_total,
                 o.shipping_fee_total,
                 o.buyer_email_id,
                 to_date (substr (o.order_date, 5, length (o.order_date) - 12) || substr (o.order_date, -5), 'mon dd hh24:mi:ss yyyy') order_date,
                 UPPER (o.order_origin) order_origin,
                 clean.customer_query_pkg.get_memberhip_id (b.clean_customer_id, (select pp.partner_name from ftd_apps.partner_program pp join memberships m on pp.program_name = m.membership_type where o.membership_id = m.membership_id), (select m.membership_id_number from memberships m where o.membership_id = m.membership_id) ) membership_id,
                 o.csr_id,
                 o.add_on_amount_total,
                 o.partnership_bonus_points,
                 o.source_code,
                 o.call_time,
                 o.dnis_code,
                 o.discount_total,
                 o.yellow_pages_code,
                 o.ariba_buyer_asn_number,
                 o.ariba_buyer_cookie,
                 o.ariba_payload,
                 o.co_brand_credit_card_code,
                 o.fraud,
                 o.previous_status,
                 o.loss_prevention_indicator,
                 fs.company_id,
                 clean.customer_query_pkg.get_customer_email_id (b.clean_customer_id, (select be.email from buyer_emails be where o.buyer_email_id = be.buyer_email_id)) email_id,
                 decode ( (select og.origin_type from ftd_apps.origins og where og.origin_id = upper (o.order_origin)), 'phone', ( select u.call_center_id from aas.users u join aas.identity i on u.user_id = i.user_id where i.identity_id = o.csr_id ), null ) order_taken_call_center,
                 to_number (o.mp_redemption_rate_amt) mp_redemption_rate_amt,
                 o.BUYER_SIGNED_IN_FLAG, o.CHARACTER_MAPPING_FLAG, 
                 o.LANGUAGE_ID, o.is_joint_cart, o.add_on_discount_amount
           from  order_details od, orders o, buyers b, ftd_apps.source fs
          where  od.order_detail_id = in_order_detail_id
            and  od.order_guid = o.order_guid
            and  o.buyer_id = b.buyer_id
            and  o.source_code = fs.source_code;

CURSOR o_ext_cur IS
        select  ox.extension_id,
                ox.order_guid,
                ox.info_name,
                ox.info_value
        from    order_extensions ox
        join    order_details od
        on      ox.order_guid = od.order_guid
        where   od.order_detail_id = in_order_detail_id;

CURSOR co_brand_cur IS
        select  c.co_brand_id,
                c.order_guid,
                c.info_name,
                c.info_data
        from    co_brand c
        join    order_details od
        on      c.order_guid = od.order_guid
        where   od.order_detail_id = in_order_detail_id;

CURSOR order_contact_cur IS
        select  oc.order_contact_info_id,
                oc.order_guid,
                oc.first_name,
                ltrim (oc.first_name || ' ' || oc.last_name) last_name,
                oc.phone,
                oc.ext,
                oc.email
        from    order_contact_info oc
        join    order_details od
        on      oc.order_guid = od.order_guid
        where   od.order_detail_id = in_order_detail_id;

v_recipient_id          number;
v_avs_address_id        number;

CURSOR order_detail_cur IS
        select  od.order_detail_id,
                od.order_guid,
                od.product_id,
                decode ((select pm.product_type from ftd_apps.product_master pm where pm.product_id = od.product_id), 'SDG', 'Y', 'SDFC', 'Y', 'N') same_day_gift,
                od.quantity,
                od.ship_via,
                od.ship_method,
                to_date (od.ship_date, 'mm/dd/yyyy') ship_date,
                to_date (od.delivery_date, 'mm/dd/yyyy') delivery_date,
                to_date (od.delivery_date_range_end, 'mm/dd/yyyy') delivery_date_range_end,
                od.drop_ship_tracking_number,
                to_number (od.products_amount) products_amount,
                to_number (od.tax_amount) tax_amount,
                to_number (od.service_fee_amount) service_fee_amount,
                to_number (od.shipping_fee_amount) shipping_fee_amount,
                to_number (od.add_on_amount) add_on_amount,
                abs (to_number (od.discount_amount)) discount_amount,
                od.occasion_id,
                od.last_minute_gift_signature,
                od.last_minute_number,
                od.last_minute_gift_email,
                od.external_order_number,
                od.color_1,
                od.color_2,
                od.substitute_acknowledgement,
                od.card_message,
                od.card_signature,
                od.order_comments,
                od.order_contact_information,
                od.florist_number,
                RTRIM (od.special_instructions) special_instructions,
                od.ariba_unspsc_code,
                od.ariba_po_number,
                od.ariba_ams_project_code,
                od.ariba_cost_center,
                od.external_order_total,
                od.size_indicator,
                od.sender_info_release,
                od.miles_points,
                od.subcode,
                od.source_code,
                to_number (od.commission) commission,
                to_number (od.wholesale) wholesale,
                to_number (od.transaction) transaction,
                to_number (od.pdb_price) pdb_price,
                to_number (od.shipping_tax) shipping_tax,
                to_number (od.discount_product_price) discount_product_price,
                od.discount_type,
                od.partner_cost,
                (select e.info_data from order_detail_extensions e where e.extension_id = (select max(e1.extension_id) from order_detail_extensions e1 where e1.order_detail_id = od.order_detail_id and e1.info_name = 'SCRUBBED_BY')) scrubbed_by,
                (select to_date (e.info_data, 'mm/dd/yyyy hh24:mi:ss') from order_detail_extensions e where e.extension_id = (select max(e1.extension_id) from order_detail_extensions e1 where e1.order_detail_id = od.order_detail_id and e1.info_name = 'SCRUBBED_ON')) scrubbed_on,
                od.personalization_data,
                od.qms_result_code,
                to_number (od.miles_points_amt) miles_points_amt,
                od.personal_greeting_id,
                to_number (od.dtl_first_order_domestic) dtl_first_order_domestic,
                to_number (od.dtl_first_order_international) dtl_first_order_international,
                to_number (od.dtl_shipping_cost) dtl_shipping_cost,
                to_number (od.dtl_vendor_charge) dtl_vendor_charge,
                to_number (od.dtl_vendor_sat_upcharge) dtl_vendor_sat_upcharge,
                to_number (od.dtl_ak_hi_special_svc_charge) dtl_ak_hi_special_svc_charge,
                to_number (od.dtl_fuel_surcharge_amt) dtl_fuel_surcharge_amt,
                od.apply_surcharge_code ,
                od.surcharge_description,
                od.send_surcharge_to_florist_flag,
                od.display_surcharge_flag,
                od.bin_source_changed_flag,
                decode(bin_source_changed_flag, 'Y',
                    (select distinct pm.partner_name from ftd_apps.source_program_ref spr
                    left outer join ftd_apps.partner_program pp
                        on pp.program_name = spr.program_name
                    left outer join ftd_apps.partner_master pm
                        on pm.partner_name = pp.partner_name
                    where spr.source_code = od.source_code),
                '') partner_name,
                od.shipping_fee_amount_saved,
                od.service_fee_amount_saved,
                decode(nvl(shipping_fee_amount_saved,0) + nvl(service_fee_amount_saved,0), 0, 'N', 'Y') free_shipping_flag,
        od.tax1_name,
        od.tax1_amount,
        od.tax1_description,
        od.tax1_rate,
        od.tax2_name,
        od.tax2_amount,
        od.tax2_description,
        od.tax2_rate,
        od.tax3_name,
        od.tax3_amount,
        od.tax3_description,
        od.tax3_rate,
        od.tax4_name,
        od.tax4_amount,
        od.tax4_description,
        od.tax4_rate,
        od.tax5_name,
        od.tax5_amount,
        od.tax5_description,
        od.tax5_rate,
        od.same_day_upcharge,
        od.morning_delivery_fee,
        od.original_order_has_sdu,
		od.PC_GROUP_ID,
		od.PC_MEMBERSHIP_ID,
		od.PC_FLAG,
		od.TIME_OF_SERVICE,
		od.add_on_discount_amount,
		od.late_cutoff_fee,
		od.vendor_sun_upcharge,
		od.vendor_mon_upcharge,
		od.legacy_id
        from    order_details od
        where   od.order_detail_id = in_order_detail_id;

v_order_detail_id         order_details.order_detail_id%type := in_order_detail_id;
v_order_bill_id           number;
v_acctg_trans_id    number;

CURSOR add_on_cur IS
        select  a.add_on_id,
                a.order_detail_id,
                a.add_on_code,
                a.add_on_quantity,
                a.add_on_history_id,
                a.add_on_price,
                a.add_on_discount_amount
        from    add_ons a
        where   a.order_detail_id = in_order_detail_id;

CURSOR od_ext_cur IS
        select  odx.extension_id,
                odx.order_detail_id,
                odx.info_name,
                odx.info_data
        from    order_detail_extensions odx
        where   odx.order_detail_id = in_order_detail_id;

v_fraud_comments varchar2(4000) := '';

CURSOR od_fraud_comments_cur IS
        select  fc.comment_text
        from    fraud_comments fc
        where   fc.order_guid = in_order_guid;

CURSOR novator_fraud_comments_cur IS
        select  nfc.novator_fraud_code,
                nvl(fnfc.description, nfc.novator_fraud_code) description
        from    orders o,
                novator_fraud_codes nfc,
                ftd_apps.novator_fraud_codes fnfc
        where   o.order_guid = in_order_guid
                and nfc.order_guid = o.order_guid
                and fnfc.code (+) = nfc.novator_fraud_code;

BEGIN

out_status := 'Y';

-- Check to see if the order has an order_disp_code of either Processed, Validated, Shipped OR Printed
IF clean.order_query_pkg.is_order_dtl_dispatched_full(in_order_detail_id) = 'N' THEN

    -- dispatch the recipient
    -- before locking the order to avoid deadlock
    dispatch_recipient (in_order_detail_id, v_recipient_id, out_status, out_message);
    dispatch_avs_address(in_order_detail_id, v_avs_address_id);

    -- dispatch the order (shopping cart)
    -- emueller 3/7/06 Added if statement so that recipient insert errors are flagged
    IF out_status = 'Y' THEN
      FOR o IN order_cur LOOP  -- there should only be one record in the order cursor
        v_customer_id := o.customer_id;         -- this is used later for inserting the order comments
        v_membership_id := o.membership_id;     -- this is used later for inserting/updating order details
        v_order_guid := o.order_guid;           -- this is used to create a lock

                select 'Y'
                  into v_lock
                  from orders
                 where order_guid = v_order_guid
         for update of order_guid;

        IF o.csr_id IS NOT NULL THEN
          -- capture the weboe or joe csr who took the order to be used later for saving the
          -- viewed history for the customer/order app
          v_viewed_csr := o.csr_id;
        END IF;

        -- set the wholesale service fee for Walmart orders to be stored with the order bill
        IF o.order_origin = 'WLMTI' THEN
          v_wholesale_service_fee :=  to_number (frp.misc_pkg.get_global_parm_value ('ACCOUNTING', 'WHOLESALE_SERVICE_FEE'));
        END IF;

        IF clean.order_query_pkg.order_exists (in_order_guid) = 'Y' THEN
          CLEAN.ORDER_MAINT_PKG.UPDATE_ORDERS
          (
            IN_ORDER_GUID=>o.order_guid,
            IN_CUSTOMER_ID=>o.customer_id,
            IN_MEMBERSHIP_ID=>o.membership_id,
            IN_COMPANY_ID=>o.company_id,
            IN_SOURCE_CODE=>o.source_code,
            IN_ORIGIN_ID=>o.order_origin,
            IN_ORDER_DATE=>o.order_date,
            IN_UPDATED_BY=>'SYS',
            IN_LOSS_PREVENTION_INDICATOR=>o.loss_prevention_indicator,
            IN_EMAIL_ID=>o.email_id,
            IN_ORDER_TAKEN_BY=>o.csr_id,
            IN_PARTNERSHIP_BONUS_POINTS=>o.partnership_bonus_points,
            IN_FRAUD_INDICATOR=>o.fraud,
            IN_ARIBA_BUYER_ASN_NUMBER=>o.ariba_buyer_asn_number,
            IN_ARIBA_BUYER_COOKIE=>o.ariba_buyer_cookie,
            IN_ARIBA_PAYLOAD=>o.ariba_payload,
            IN_CO_BRAND_CREDIT_CARD_CODE=>o.co_brand_credit_card_code,
            IN_WEBOE_DNIS_ID=>o.dnis_code,
            IN_ORDER_TAKEN_CALL_CENTER=>o.order_taken_call_center,
            IN_MP_REDEMPTION_RATE_AMT=>o.mp_redemption_rate_amt,
            IN_BUYER_SIGNED_IN_FLAG  => o.BUYER_SIGNED_IN_FLAG,
            IN_CHARACTER_MAPPING_FLAG => o.CHARACTER_MAPPING_FLAG,
      IN_LANGUAGE_ID  => o.LANGUAGE_ID,
            OUT_STATUS=>out_status,
            OUT_MESSAGE=>out_message
          );
        ELSE
          CLEAN.ORDER_MAINT_PKG.INSERT_ORDERS
          (
            IN_ORDER_GUID=>o.order_guid,
            IN_MASTER_ORDER_NUMBER=>o.master_order_number,
            IN_CUSTOMER_ID=>o.customer_id,
            IN_MEMBERSHIP_ID=>o.membership_id,
            IN_COMPANY_ID=>o.company_id,
            IN_SOURCE_CODE=>o.source_code,
            IN_ORIGIN_ID=>o.order_origin,
            IN_ORDER_DATE=>o.order_date,
            IN_CREATED_BY=>'SYS',
            IN_LOSS_PREVENTION_INDICATOR=>o.loss_prevention_indicator,
            IN_EMAIL_ID=>o.email_id,
            IN_ORDER_TAKEN_BY=>o.csr_id,
            IN_PARTNERSHIP_BONUS_POINTS=>o.partnership_bonus_points,
            IN_FRAUD_INDICATOR=>o.fraud,
            IN_ARIBA_BUYER_ASN_NUMBER=>o.ariba_buyer_asn_number,
            IN_ARIBA_BUYER_COOKIE=>o.ariba_buyer_cookie,
            IN_ARIBA_PAYLOAD=>o.ariba_payload,
            IN_CO_BRAND_CREDIT_CARD_CODE=>o.co_brand_credit_card_code,
            IN_WEBOE_DNIS_ID=>o.dnis_code,
            IN_ORDER_TAKEN_CALL_CENTER=>o.order_taken_call_center,
            IN_MP_REDEMPTION_RATE_AMT=>o.mp_redemption_rate_amt,
            IN_BUYER_SIGNED_IN_FLAG  => o.BUYER_SIGNED_IN_FLAG,
            IN_CHARACTER_MAPPING_FLAG => o.CHARACTER_MAPPING_FLAG,
      		IN_LANGUAGE_ID  => o.LANGUAGE_ID,
      		IN_JOINT_CART  => o.is_joint_cart,
      		OUT_STATUS=>out_status,
            OUT_MESSAGE=>out_message
          );
        END IF;
      END LOOP;
    END IF;

    -- dispatch co brands (MOC, ROC, BLC)
    IF out_status = 'Y' THEN
          FOR c IN co_brand_cur LOOP
               CLEAN.ORDER_MAINT_PKG.UPDATE_CO_BRAND
               (
                    IN_ORDER_GUID=>c.order_guid,
                    IN_INFO_NAME=>c.info_name,
                    IN_INFO_DATA=>c.info_data,
                    IO_CO_BRAND_ID=>c.co_brand_id,
                    OUT_STATUS=>out_status,
                    OUT_MESSAGE=>out_message
               );
               IF out_status = 'N' THEN
                    exit;
               END IF;
          END LOOP;
    END IF;

    -- dispatch order contact info
    IF out_status = 'Y' THEN
          FOR oc IN order_contact_cur LOOP
               -- first name and last name are concatenated into the last name field for bulk orders
               -- alt contact from weboe only have the full name in the last name field
               CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_CONTACT_INFO
               (
                    IN_ORDER_GUID=>oc.order_guid,
                    IN_FIRST_NAME=>null,
                    IN_LAST_NAME=>oc.last_name,
                    IN_PHONE_NUMBER=>oc.phone,
                    IN_EXTENSION=>oc.ext,
                    IN_EMAIL_ADDRESS=>oc.email,
                    IN_UPDATED_BY=>'SYS',
                    IO_ORDER_CONTACT_INFO_ID=>oc.order_contact_info_id,
                    OUT_STATUS=>out_status,
                    OUT_MESSAGE=>out_message
               );
               IF out_status = 'N' THEN
                    exit;
               END IF;
          END LOOP;
    END IF;

    -- dispatch the order detail
    IF out_status = 'Y' THEN
          FOR od IN order_detail_cur LOOP -- there should only be one record in the order detail cursor

               IF od.scrubbed_by IS NOT NULL THEN
                    -- capture the scrub csr who worked the order to be used later for saving the
                    -- viewed history for the customer/order app
                    v_viewed_csr := od.scrubbed_by;
               END IF;

               IF clean.order_query_pkg.order_detail_exists (in_order_detail_id) = 'Y' THEN
                    CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_DETAILS_ALL_VAL
                    (
                          IN_ORDER_DETAIL_ID=>v_order_detail_id,
                          IN_DELIVERY_DATE=>od.delivery_date,
                          IN_RECIPIENT_ID=>v_recipient_id,
                          IN_AVS_ADDRESS_ID=>v_avs_address_id,
                          IN_PRODUCT_ID=>od.product_id,
                          IN_QUANTITY=>od.quantity,
                          IN_COLOR_1=>od.color_1,
                          IN_COLOR_2=>od.color_2,
                          IN_SUBSTITUTION_INDICATOR=>od.substitute_acknowledgement,
                          IN_SAME_DAY_GIFT=>od.same_day_gift,
                          IN_OCCASION=>od.occasion_id,
                          IN_CARD_MESSAGE=>od.card_message,
                          IN_CARD_SIGNATURE=>od.card_signature,
                          IN_SPECIAL_INSTRUCTIONS=> BUILD_SPECIAL_INSTRUCTIONS(OD.SPECIAL_INSTRUCTIONS,CLEAN.orderdetails_query_pkg.derive_vip_flag(v_order_detail_id)) ,
                          IN_RELEASE_INFO_INDICATOR=>od.sender_info_release,
                          IN_FLORIST_ID=>od.florist_number,
                          IN_SHIP_METHOD=>od.ship_method,
                          IN_SHIP_DATE=>od.ship_date,
                          IN_ORDER_DISP_CODE=>v_order_disp_code,
                          IN_SECOND_CHOICE_PRODUCT=>null,
                          IN_ZIP_QUEUE_COUNT=>null,
                          IN_UPDATED_BY=>'SYS',
                          IN_DELIVERY_DATE_RANGE_END=>od.delivery_date_range_end,
                          IN_SCRUBBED_ON=>od.scrubbed_on,
                          IN_SCRUBBED_BY=>od.scrubbed_by,
                          IN_ARIBA_UNSPSC_CODE=>od.ariba_unspsc_code,
                          IN_ARIBA_PO_NUMBER=>od.ariba_po_number,
                          IN_ARIBA_AMS_PROJECT_CODE=>od.ariba_ams_project_code,
                          IN_ARIBA_COST_CENTER=>od.ariba_cost_center,
                          IN_SIZE_INDICATOR=>od.size_indicator,
                          IN_MILES_POINTS=>od.miles_points,
                          IN_SUBCODE=>od.subcode,
                          IN_SOURCE_CODE=>od.source_code,
                          IN_REJECT_RETRY_COUNT=>null,
                          IN_MEMBERSHIP_ID=>v_membership_id,
                          IN_PERSONALIZATION_DATA=>od.personalization_data,
                          IN_QMS_RESULT_CODE=>od.qms_result_code,
                          IN_PERSONAL_GREETING_ID=>od.personal_greeting_id,
                          IN_BIN_SOURCE_CHANGED_FLAG=>od.bin_source_changed_flag,
                          IN_FREE_SHIPPING_FLAG => od.free_shipping_flag ,
                          IN_APPLY_SURCHARGE_CODE=>od.APPLY_SURCHARGE_CODE,
                          IN_SURCHARGE_DESCRIPTION=>od.SURCHARGE_DESCRIPTION,
                          IN_DISPLAY_SURCHARGE=> od.DISPLAY_SURCHARGE_FLAG,
                          IN_SEND_SURCHARGE_TO_FLORIST=> od.SEND_SURCHARGE_TO_FLORIST_FLAG,
			  IN_ORIGINAL_ORDER_HAS_SDU => od.original_order_has_sdu,
			  IN_PC_GROUP_ID => od.PC_GROUP_ID,
			  IN_PC_MEMBERSHIP_ID => od.PC_MEMBERSHIP_ID,
			  IN_PC_FLAG => od.PC_FLAG,
			  IN_TIME_OF_SERVICE => od.TIME_OF_SERVICE,
			  IN_DERIVED_VIP_FLAG => CLEAN.orderdetails_query_pkg.derive_vip_flag(v_order_detail_id),
			  IN_LEGACY_ID => od.LEGACY_ID,
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               ELSE
                    CLEAN.ORDER_MAINT_PKG.INSERT_ORDER_DETAILS
                    (
                          IN_ORDER_GUID=>od.order_guid,
                          IN_EXTERNAL_ORDER_NUMBER=>od.external_order_number,
                          IN_HP_ORDER_NUMBER=>null,
                          IN_SOURCE_CODE=>od.source_code,
                          IN_DELIVERY_DATE=>od.delivery_date,
                          IN_RECIPIENT_ID=>v_recipient_id,
                          IN_AVS_ADDRESS_ID=>v_avs_address_id,
                          IN_PRODUCT_ID=>od.product_id,
                          IN_QUANTITY=>od.quantity,
                          IN_COLOR_1=>od.color_1,
                          IN_COLOR_2=>od.color_2,
                          IN_SUBSTITUTION_INDICATOR=>od.substitute_acknowledgement,
                          IN_SAME_DAY_GIFT=>od.same_day_gift,
                          IN_OCCASION=>od.occasion_id,
                          IN_CARD_MESSAGE=>od.card_message,
                          IN_CARD_SIGNATURE=>od.card_signature,
                          IN_SPECIAL_INSTRUCTIONS=> BUILD_SPECIAL_INSTRUCTIONS(OD.SPECIAL_INSTRUCTIONS,CLEAN.orderdetails_query_pkg.derive_vip_flag(v_order_detail_id)) ,
                          IN_RELEASE_INFO_INDICATOR=>od.sender_info_release,
                          IN_FLORIST_ID=>od.florist_number,
                          IN_SHIP_METHOD=>od.ship_method,
                          IN_SHIP_DATE=>od.ship_date,
                          IN_ORDER_DISP_CODE=>v_order_disp_code,
                          IN_CREATED_BY=>'SYS',
                          IN_SCRUBBED_ON=>od.scrubbed_on,
                          IN_SCRUBBED_BY=>od.scrubbed_by,
                          IN_DELIVERY_DATE_RANGE_END=>od.delivery_date_range_end,
                          IN_ARIBA_UNSPSC_CODE=>od.ariba_unspsc_code,
                          IN_ARIBA_PO_NUMBER=>od.ariba_po_number,
                          IN_ARIBA_AMS_PROJECT_CODE=>od.ariba_ams_project_code,
                          IN_ARIBA_COST_CENTER=>od.ariba_cost_center,
                          IN_SIZE_INDICATOR=>od.size_indicator,
                          IN_MILES_POINTS=>od.miles_points,
                          IN_SUBCODE=>od.subcode,
                          IN_MEMBERSHIP_ID=>v_membership_id,
                          IN_PERSONALIZATION_DATA=>od.personalization_data,
                          IN_QMS_RESULT_CODE=>od.qms_result_code,
                          IN_PERSONAL_GREETING_ID=>od.personal_greeting_id,
                          IN_BIN_SOURCE_CHANGED_FLAG=>od.bin_source_changed_flag,
                          IO_ORDER_DETAIL_ID=>v_order_detail_id,
                          IN_FREE_SHIPPING_FLAG => od.free_shipping_flag,
                          IN_APPLY_SURCHARGE_CODE=>od.APPLY_SURCHARGE_CODE,
                          IN_SURCHARGE_DESCRIPTION=>od.SURCHARGE_DESCRIPTION,
                          IN_DISPLAY_SURCHARGE=> od.DISPLAY_SURCHARGE_FLAG,
                          IN_SEND_SURCHARGE_TO_FLORIST=> od.SEND_SURCHARGE_TO_FLORIST_FLAG,
						  IN_ORIGINAL_ORDER_HAS_SDU => od.original_order_has_sdu,
						  IN_PC_GROUP_ID => od.PC_GROUP_ID,
						  IN_PC_MEMBERSHIP_ID => od.PC_MEMBERSHIP_ID,
						  IN_PC_FLAG => od.PC_FLAG,
						  IN_DERIVED_VIP_FLAG => CLEAN.orderdetails_query_pkg.derive_vip_flag(v_order_detail_id),
			  			  IN_TIME_OF_SERVICE => od.TIME_OF_SERVICE,
			  			  IN_LEGACY_ID => od.legacy_id,
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

               IF out_status = 'Y' THEN
                    CLEAN.ORDER_MAINT_PKG.INSERT_ORDER_BILLS
                    (
                    IN_ORDER_DETAIL_ID=>v_order_detail_id,
                    IN_PRODUCT_AMOUNT=>od.products_amount,
                    IN_ADD_ON_AMOUNT=>od.add_on_amount,
                    IN_SERVICE_FEE=>od.service_fee_amount,
                    IN_SHIPPING_FEE=>od.shipping_fee_amount,
                    IN_DISCOUNT_AMOUNT=>od.discount_amount,
                    IN_SHIPPING_TAX=>od.shipping_tax,
                    IN_TAX=>od.tax_amount,
                    IN_ADDITIONAL_BILL_INDICATOR=>'N',
                    IN_CREATED_BY=>'SYS',
                    IN_SAME_DAY_FEE=>null,
                    IN_BILL_STATUS=>'Unbilled',
                    IN_SERVICE_FEE_TAX=>null,
                    IN_COMMISSION_AMOUNT=>od.commission,
                    IN_WHOLESALE_AMOUNT=>od.wholesale,
                    IN_TRANSACTION_AMOUNT=>od.transaction,
                    IN_PDB_PRICE=>od.pdb_price,
                    IN_DISCOUNT_PRODUCT_PRICE=>od.discount_product_price,
                    IN_DISCOUNT_TYPE=>od.discount_type,
                    IN_PARTNER_COST=>od.partner_cost,
                    IN_ACCT_TRANS_IND=>'Y',
                    IN_WHOLESALE_SERVICE_FEE=>v_wholesale_service_fee,
                    IN_MILES_POINTS_AMT=>od.miles_points_amt,
                    IN_FIRST_ORDER_DOMESTIC=>od.dtl_first_order_domestic,
                    IN_FIRST_ORDER_INTERNATIONAL=>od.dtl_first_order_international,
                    IN_SHIPPING_COST=>od.dtl_shipping_cost,
                    IN_VENDOR_CHARGE=>od.dtl_vendor_charge,
                    IN_VENDOR_SAT_UPCHARGE=>od.dtl_vendor_sat_upcharge,
                    IN_AK_HI_SPECIAL_SVC_CHARGE=>od.dtl_ak_hi_special_svc_charge,
                    IN_FUEL_SURCHARGE_AMT=>od.dtl_fuel_surcharge_amt,
          IN_SHIPPING_FEE_SAVED=>od.shipping_fee_amount_saved,
            IN_SERVICE_FEE_SAVED =>od.service_fee_amount_saved,
          IN_TAX1_NAME => od.tax1_name,
          IN_TAX1_AMOUNT => od.tax1_amount,
          IN_TAX1_DESCRIPTION => od.tax1_description,
          IN_TAX1_RATE => od.tax1_rate,
          IN_TAX2_NAME => od.tax2_name,
          IN_TAX2_AMOUNT => od.tax2_amount,
          IN_TAX2_DESCRIPTION => od.tax2_description,
          IN_TAX2_RATE => od.tax2_rate,
          IN_TAX3_NAME => od.tax3_name,
          IN_TAX3_AMOUNT => od.tax3_amount,
          IN_TAX3_DESCRIPTION => od.tax3_description,
          IN_TAX3_RATE => od.tax3_rate,
          IN_TAX4_NAME => od.tax4_name,
          IN_TAX4_AMOUNT => od.tax4_amount,
          IN_TAX4_DESCRIPTION => od.tax4_description,
          IN_TAX4_RATE => od.tax4_rate,
          IN_TAX5_NAME => od.tax5_name,
          IN_TAX5_AMOUNT => od.tax5_amount,
          IN_TAX5_DESCRIPTION => od.tax5_description,
          IN_TAX5_RATE => od.tax5_rate,
          IN_SAME_DAY_UPCHARGE => od.same_day_upcharge,
          IN_MORNING_DELIVERY_FEE => od.morning_delivery_fee,
          IN_LATE_CUTOFF_FEE => od.late_cutoff_fee,
          IN_VEN_SUN_UPCHARGE => od.vendor_sun_upcharge,
          IN_VEN_MON_UPCHARGE => od.vendor_mon_upcharge,
          IN_ADD_ON_DISCOUNT_AMOUNT => od.add_on_discount_amount,
                    OUT_ORDER_BILL_ID=>v_order_bill_id,
                    OUT_STATUS=>out_status,
                    OUT_MESSAGE=>out_message
                    );

                    -- check the status for a duplicate original bill, this can occur if the order is
                    -- dispatched multiple times in testing
          -- defect 1091: in that case, update the order bills and accounting transactions
                    IF out_status = 'N' AND out_message = 'An original bill already exists for the order' THEN
            out_status := 'Y';
                          out_message := null;

            v_order_bill_id := CLEAN.ORDER_QUERY_PKG.GET_ORIGINAL_ORDER_BILL_ID (v_order_detail_id);
            v_acctg_trans_id := CLEAN.ORDER_QUERY_PKG.GET_ORIGINAL_ACCTG_TRANS_ID (v_order_detail_id);

            clean.order_maint_pkg.update_order_bills
            (
              IN_ORDER_BILL_ID=>v_order_bill_id,
              IN_PRODUCT_AMOUNT=>od.products_amount,
              IN_ADD_ON_AMOUNT=>od.add_on_amount,
              IN_SERVICE_FEE=>od.service_fee_amount,
              IN_SHIPPING_FEE=>od.shipping_fee_amount,
              IN_DISCOUNT_AMOUNT=>od.discount_amount,
              IN_SHIPPING_TAX=>od.shipping_tax,
              IN_TAX=>od.tax_amount,
              IN_UPDATED_BY=>'DISPATCH_ORDER_FULL',
              IN_SERVICE_FEE_TAX=>null,
              IN_COMMISSION_AMOUNT=>od.commission,
              IN_WHOLESALE_AMOUNT=>od.wholesale,
              IN_WHOLESALE_SERVICE_FEE=>v_wholesale_service_fee,
              IN_TRANSACTION_AMOUNT=>od.transaction,
              IN_PDB_PRICE=>od.pdb_price,
              IN_DISCOUNT_PRODUCT_PRICE=>od.discount_product_price,
              IN_DISCOUNT_TYPE=>od.discount_type,
              IN_PARTNER_COST=>od.partner_cost,
              IN_MILES_POINTS_AMT=>od.miles_points_amt,
              IN_FIRST_ORDER_DOMESTIC=>od.dtl_first_order_domestic,
              IN_FIRST_ORDER_INTERNATIONAL=>od.dtl_first_order_international,
              IN_SHIPPING_COST=>od.dtl_shipping_cost,
              IN_VENDOR_CHARGE=>od.dtl_vendor_charge,
              IN_VENDOR_SAT_UPCHARGE=>od.dtl_vendor_sat_upcharge,
              IN_AK_HI_SPECIAL_SVC_CHARGE=>od.dtl_ak_hi_special_svc_charge,
              IN_FUEL_SURCHARGE_AMT=>od.dtl_fuel_surcharge_amt,
              IN_SHIPPING_FEE_SAVED=>od.shipping_fee_amount_saved,
              IN_SERVICE_FEE_SAVED =>od.service_fee_amount_saved,
        IN_TAX1_NAME => od.tax1_name,
        IN_TAX1_AMOUNT => od.tax1_amount,
        IN_TAX1_DESCRIPTION => od.tax1_description,
        IN_TAX1_RATE => od.tax1_rate,
        IN_TAX2_NAME => od.tax2_name,
        IN_TAX2_AMOUNT => od.tax2_amount,
        IN_TAX2_DESCRIPTION => od.tax2_description,
        IN_TAX2_RATE => od.tax2_rate,
        IN_TAX3_NAME => od.tax3_name,
        IN_TAX3_AMOUNT => od.tax3_amount,
        IN_TAX3_DESCRIPTION => od.tax3_description,
        IN_TAX3_RATE => od.tax3_rate,
        IN_TAX4_NAME => od.tax4_name,
        IN_TAX4_AMOUNT => od.tax4_amount,
        IN_TAX4_DESCRIPTION => od.tax4_description,
        IN_TAX4_RATE => od.tax4_rate,
        IN_TAX5_NAME => od.tax5_name,
        IN_TAX5_AMOUNT => od.tax5_amount,
        IN_TAX5_DESCRIPTION => od.tax5_description,
        IN_TAX5_RATE => od.tax5_rate,
        IN_SAME_DAY_UPCHARGE => od.same_day_upcharge,
        IN_MORNING_DELIVERY_FEE => od.morning_delivery_fee,
        IN_LATE_CUTOFF_FEE => od.late_cutoff_fee,
        IN_VEN_SUN_UPCHARGE => od.vendor_sun_upcharge,
        IN_VEN_MON_UPCHARGE => od.vendor_mon_upcharge,
        IN_ADD_ON_DISCOUNT_AMOUNT => od.add_on_discount_amount,
              OUT_STATUS=>out_status,
              OUT_MESSAGE=>out_message
            );

            IF out_status = 'N' THEN
              out_status := 'Y';
                          out_message := null;
            END IF;

            clean.order_maint_pkg.update_accounting_transactions
            (
              IN_ACCOUNTING_TRANSACTION_ID=>v_acctg_trans_id,
              IN_PRODUCT_AMOUNT=>od.products_amount,
              IN_ADD_ON_AMOUNT=>od.add_on_amount,
              IN_SHIPPING_FEE=>od.shipping_fee_amount,
              IN_SERVICE_FEE=>od.service_fee_amount,
              IN_DISCOUNT_AMOUNT=>od.discount_amount,
              IN_SHIPPING_TAX=>od.shipping_tax,
              IN_SERVICE_FEE_TAX=>null,
              IN_TAX=>od.tax_amount,
              IN_COMMISSION_AMOUNT=>od.commission,
              IN_WHOLESALE_AMOUNT=>od.wholesale,
              IN_ADMIN_FEE=>null,
              IN_WHOLESALE_SERVICE_FEE=>v_wholesale_service_fee,
              IN_ADD_ON_DISCOUNT_AMOUNT => od.add_on_discount_amount,
              OUT_STATUS=>out_status,
              OUT_MESSAGE=>out_message
            );

            IF out_status = 'N' THEN
              out_status := 'Y';
                          out_message := null;
            END IF;

                    END IF;
               END IF;



                                                         IF out_status = 'Y' AND od.scrubbed_on IS NOT NULL AND od.scrubbed_by IS NOT NULL THEN
                                                                                CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>'SCRUB',
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>'Order released from Scrub.',
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>od.scrubbed_by,
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

               IF out_status = 'Y' AND od.order_comments is not null THEN
                    CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>null,
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>od.order_comments,
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>v_viewed_csr,
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

                 --Obtain fraud comments
                                 OPEN od_fraud_comments_cur;
                 FETCH od_fraud_comments_cur INTO v_fraud_comments;
                 CLOSE od_fraud_comments_cur;

               IF out_status = 'Y' AND v_fraud_comments is not null THEN
                    CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>null,
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>v_fraud_comments,
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>v_viewed_csr,
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

                 --Obtain novator fraud comments
                                                         v_fraud_comments := null;
                                 for x in novator_fraud_comments_cur loop
                                                          if (v_fraud_comments is null) then
                                                            v_fraud_comments := x.description;
                                                          else
                                                            v_fraud_comments := v_fraud_comments || ', ' || x.description;
                                                          end if;
                                                         end loop;

               IF out_status = 'Y' AND v_fraud_comments is not null THEN
                    CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>null,
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>'Potential fraud: (' || v_fraud_comments || ')',
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>v_viewed_csr,
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

                                                         IF out_status = 'Y' AND od.personalization_data is not null THEN
                    CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>null,
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>'This is a personalized order.  It cannot be modified or updated.  Delivery date is approximate and may vary.',
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>'SYS',
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

                                                         IF out_status = 'Y' AND od.personal_greeting_id is not null THEN
                                                                                FTD_APPS.CONTENT_QUERY_PKG.GET_CONTENT_TEXT_WITH_FILTER('PERSONAL_GREETINGS', 'ORDER_COMMENT', null, null, V_PG_COMMENT);
                                                                                FTD_APPS.CONTENT_QUERY_PKG.GET_CONTENT_TEXT_WITH_FILTER('PERSONAL_GREETINGS', 'RECORDING_PHONE', null, null, V_PG_PHONE);
                                                                                V_PG_FULL_COMMENT := REPLACE(V_PG_COMMENT, '~pgid~', od.personal_greeting_id) || V_PG_PHONE;
                    CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>null,
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>V_PG_FULL_COMMENT,
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>'SYS',
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;

                                                         IF out_status = 'Y' AND od.bin_source_changed_flag = 'Y' THEN
                    CLEAN.COMMENT_HISTORY_PKG.INSERT_COMMENTS
                    (
                          IN_CUSTOMER_ID=>v_customer_id,
                          IN_ORDER_GUID=>od.order_guid,
                          IN_ORDER_DETAIL_ID=>od.order_detail_id,
                          IN_COMMENT_ORIGIN=>'OrderProc',
                          IN_REASON=>null,
                          IN_DNIS_ID=>null,
                          IN_COMMENT_TEXT=>'This order was changed to a ' || od.partner_name || ' order, ' || od.partner_name || ' discount applied.',
                          IN_COMMENT_TYPE=>'Order',
                          IN_CSR_ID=>'SYS',
                          OUT_STATUS=>out_status,
                          OUT_MESSAGE=>out_message
                    );
               END IF;
          END LOOP;
    END IF;

    -- dispatch add ons
    IF out_status = 'Y' THEN
          FOR a IN add_on_cur LOOP
               CLEAN.ORDER_MAINT_PKG.INSERT_ORDER_ADD_ONS
               (
                    IN_ORDER_DETAIL_ID=>a.order_detail_id,
                    IN_ADD_ON_CODE=>a.add_on_code,
                    IN_ADD_ON_QUANTITY=>a.add_on_quantity,
                    IN_ADD_ON_HISTORY_ID=>a.add_on_history_id,
                    IN_CREATED_BY=>'SYS',
                    IO_ORDER_ADD_ON_ID=>a.add_on_id,
                    IN_ADD_ON_AMOUNT => a.add_on_price,
                    IN_ADD_ON_DISCOUNT_AMOUNT => a.add_on_discount_amount,
                    OUT_STATUS=>out_status,
                    OUT_MESSAGE=>out_message
               );
               IF out_status = 'N' THEN
                    exit;
               END IF;
          END LOOP;
    END IF;

    -- dispatch the extensions
                IF out_status ='Y' THEN
          FOR ox IN o_ext_cur LOOP
               CLEAN.ORDER_MAINT_PKG.UPDATE_ORDER_EXTENSIONS
               (
                    IN_ORDER_GUID=>ox.order_guid,
                    IN_INFO_NAME=>ox.info_name,
                    IN_INFO_VALUE=>ox.info_value,
                          IO_EXTENSION_ID=>ox.extension_id,
                    OUT_STATUS=>out_status,
                    OUT_MESSAGE=>out_message
               );
               IF out_status = 'N' THEN
                    exit;
               END IF;
          END LOOP;
    END IF;
    IF out_status ='Y' THEN
          FOR odx IN od_ext_cur LOOP
               CLEAN.ORDER_MAINT_PKG.INSERT_ORDER_DETAIL_EXTENSIONS
               (
                    IN_ORDER_DETAIL_ID=>odx.order_detail_id,
                    IN_CREATED_ON=>sysdate,
                    IN_CREATED_BY=>'SYS',
                    IN_UPDATED_ON=>sysdate,
                    IN_UPDATED_BY=>'SYS',
                    IN_INFO_NAME=>odx.info_name,
                    IN_INFO_DATA=>odx.info_data,
                    IO_EXTENSION_ID=>odx.extension_id,
                    OUT_STATUS=>out_status,
                    OUT_MESSAGE=>out_message
               );
               IF out_status = 'N' THEN
                    exit;
               END IF;
          END LOOP;
    END IF;

    -- dispatch the payment
    IF out_status = 'Y' THEN
          dispatch_payment (in_order_detail_id, out_status, out_message);
    END IF;


                IF out_status = 'Y' AND v_viewed_csr IS NOT NULL THEN
          -- insert the csr viewed records for weboe, joe, or scrubbed orders
          CLEAN.CSR_VIEWED_LOCKED_PKG.UPDATE_CSR_VIEWED
          (
               IN_CSR_ID=>v_viewed_csr,
               IN_ENTITY_TYPE=>'ORDER_DETAILS',
               IN_ENTITY_ID=>in_order_detail_id,
               OUT_STATUS=>out_status,
               OUT_MESSAGE=>out_message
          );
    END IF;

     IF out_status = 'Y' THEN
          commit;
    ELSE
          rollback;
    END IF;
ELSE
   out_status := 'Y';
   out_message := null;
END IF; -- check if the order is dispatched FULL


EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DISPATCH_ORDER_FULL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DISPATCH_ORDER_FULL;


PROCEDURE DISPATCH_ORDER
(
IN_ORDER_DETAIL_ID                      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DISPATCH_STATUS                      IN VARCHAR2,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for dispatching the order to CLEAN
        based on the dispatch status.  If the status is full, the customer
        and order data is moved.  If the status is skeleton, the customer
        data is moved and only the order ids (order guid, order detail id,
        master order number, external order number) are moved to CLEAN.

Input:
        order_detail_id                 number
        dispatch_status                 varchar2 (Full, Skeleton)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR buyer_cur IS
        SELECT  o.buyer_id,
                o.order_guid
        FROM    orders o
        JOIN    order_details od
        ON      o.order_guid = od.order_guid
        WHERE   od.order_detail_id = in_order_detail_id;

v_buyer_id      buyers.buyer_id%type;
v_order_guid    orders.order_guid%type;

BEGIN

OPEN buyer_cur;
FETCH buyer_cur INTO v_buyer_id, v_order_guid;
CLOSE buyer_cur;

DISPATCH_BUYER (v_buyer_id, v_order_guid, out_status, out_message);

IF out_status = 'Y' THEN
        IF in_dispatch_status = 'Full' THEN
                DISPATCH_ORDER_FULL (in_order_detail_id, v_order_guid, out_status, out_message);
        ELSE
                DISPATCH_ORDER_IDS (in_order_detail_id, v_order_guid, out_status, out_message);
        END IF;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DISPATCH_ORDER;

FUNCTION BUILD_SPECIAL_INSTRUCTIONS
(
IN_SPECIAL_INSTRUCTIONS IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_DERIVED_VIP_FLAG VARCHAR2)
RETURN VARCHAR2
AS
OUT_DERIVED_INST VARCHAR2(6000); 

BEGIN
  IF IN_DERIVED_VIP_FLAG IS NOT NULL AND NVL(INSTR(IN_SPECIAL_INSTRUCTIONS, 'VIP:'), 0) < 1 THEN
    OUT_DERIVED_INST := 'VIP:' || IN_DERIVED_VIP_FLAG ;
    IF IN_SPECIAL_INSTRUCTIONS IS NOT NULL THEN
      OUT_DERIVED_INST := OUT_DERIVED_INST || ' INST:' || IN_SPECIAL_INSTRUCTIONS;
    END IF;
  ELSE
    OUT_DERIVED_INST := IN_SPECIAL_INSTRUCTIONS;
  END IF;

RETURN OUT_DERIVED_INST;

END BUILD_SPECIAL_INSTRUCTIONS;


END DISPATCHER_PKG;
/
