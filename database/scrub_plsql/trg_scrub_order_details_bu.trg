CREATE OR REPLACE TRIGGER scrub.trg_scrub_order_details_bu BEFORE UPDATE OF products_amount ON scrub.order_details 
FOR EACH ROW

/*****************************************************************************************   
***          Created By : JP Puzon
***          Created On : 10/03/2006
***              Reason : Enhancement 1768
***
*** Special Instructions: 
********************************************************************************************/   
  
BEGIN

   -- set the product price-update metadata column
IF :old.products_amount IS NOT NULL THEN
IF :new.products_amount  != :old.products_amount THEN
   :new.updated_on_products_amount := sysdate;
END IF;
END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In SCRUB.TRG_SCRUB_ORDER_DETAILS_BU: SQLCODE: ' || sqlcode ||
         ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_scrub_order_details_bu;
/
