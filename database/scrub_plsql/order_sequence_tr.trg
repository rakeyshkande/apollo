CREATE OR REPLACE
TRIGGER scrub.order_sequence_tr
AFTER  INSERT ON scrub.order_details
FOR EACH ROW
DECLARE
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
   IF :new.external_order_number IS NOT NULL THEN
      INSERT INTO frp.order_sequence (external_order_number, timestamp) VALUES (:new.external_order_number, sysdate);
      COMMIT;
   END IF;
END;
.
/
