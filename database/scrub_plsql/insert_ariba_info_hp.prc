CREATE OR REPLACE
PROCEDURE scrub.INSERT_ARIBA_INFO_HP(
  -- This procedure provides a mechanism for Pick to retrieve information from
  -- the Order Detail Extension table in the Scrub database.  This table consists
  -- of name/value pairs representing optional information associated with each
  -- order detail record.
  --

  -- Input: External order number
  IN_EXTERNAL_ORDER_NUMBER IN  VARCHAR,

  -- Input: Name of data item to extract for this order
  IN_INFO_NAME_STR         IN  VARCHAR,

  -- Output: Actual data value
  OUT_INFO_DATA_STR        OUT VARCHAR)
IS

-- Note that we're utilizing a cursor instead of "select into"
-- just in case there are multiple rows for that order/name.
-- This should never be the case, but let's be safe.
CURSOR OD_EXTENSIONS_CUR IS
SELECT
  ODE.INFO_DATA
FROM SCRUB.ORDER_DETAIL_EXTENSIONS ODE,
     SCRUB.ORDER_DETAILS OD
WHERE ODE.ORDER_DETAIL_ID=OD.ORDER_DETAIL_ID
AND   OD.EXTERNAL_ORDER_NUMBER=IN_EXTERNAL_ORDER_NUMBER
AND   ODE.INFO_NAME=IN_INFO_NAME_STR;

BEGIN
  FOR OD_EXTENSIONS_ROW IN OD_EXTENSIONS_CUR
  LOOP
  BEGIN
    OUT_INFO_DATA_STR := OD_EXTENSIONS_ROW.INFO_DATA;
  END;
  END LOOP;
END INSERT_ARIBA_INFO_HP;
.
/
