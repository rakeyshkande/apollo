CREATE OR REPLACE
PACKAGE BODY scrub.SCRUB_DAEMON_PKG AS

/*-----------------------------------------------------------------------------
Description:
        Package acts as an active listener for the SCRUB system looking for
        things that need to be done.  This listener is responsible for unlocking
        expired scrub sessions.

        This package contains procedures for placing the listener job into the
        DBMS_JOB queue.  The job runs with an interval set by the package
        variable pg_interval.
-----------------------------------------------------------------------------*/


PG_DAEMON_STRING  VARCHAR2(50) := 'SCRUB_DAEMON_PKG.DAEMON;';  -- name of job in the DBMS_JOB stream
PG_INTERVAL       VARCHAR2(50) := 'SYSDATE + 5/1440';           -- daemon interval set to 5 minutes
PG_THIS_PACKAGE   VARCHAR2(50) := 'SCRUB_DAEMON_PKG';


PROCEDURE INSERT_DAEMON_MESSAGE
(
IN_PACKAGE_NAME                 DAEMON_MESSAGE.PACKAGE_NAME%TYPE,
IN_PROCEDURE_NAME               DAEMON_MESSAGE.PROCEDURE_NAME%TYPE,
IN_MESSAGE                      DAEMON_MESSAGE.MESSAGE%TYPE
)

AS

/*-----------------------------------------------------------------------------
Description:
        Procedure insert messages/exceptions from the daemon job
-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO DAEMON_MESSAGE
(
        PACKAGE_NAME,
        PROCEDURE_NAME,
        MESSAGE,
        CREATED_ON
)
VALUES
(
        IN_PACKAGE_NAME,
        IN_PROCEDURE_NAME,
        IN_MESSAGE,
        SYSDATE
);

COMMIT;

END INSERT_DAEMON_MESSAGE;


PROCEDURE CHECK_EXPIRED_SCRUB_SESSIONS AS

/*-----------------------------------------------------------------------------
Description:
        Procedure gets the global parameter from the FRP schema for the
        scrub session time value (number stored in minutes).  Then update
        any orders with a strub status (1005) that have inactivity for
        longer than the scrub session time value.  The status will be
        updated to the previous status or '1002' if the previous status does
        not exist.
-----------------------------------------------------------------------------*/

V_THIS_PROCEDURE        VARCHAR2 (50) := 'CHECK_EXPIRED_SCRUB_SESSIONS';
V_SCRUB_SESSION_TIME    NUMBER;
V_EXPIRED_TIME          DATE;
V_ERROR_MESSAGE         VARCHAR2 (1000);

BEGIN

-- get the scrub session time value from frp.global_parms
-- if for some reason, global parms is not populated default the scrub session timeout to 15 minutes
V_SCRUB_SESSION_TIME := COALESCE (TO_NUMBER (FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE ('SCRUB_SESSION', 'SESSION_TIMEOUT')), 15);

-- compute the expired time (sysdate - scrub_session_time)
V_EXPIRED_TIME := SYSDATE - V_SCRUB_SESSION_TIME / 1440;

-- unlock the orders that have been inactive for longer than the scrub session time
UPDATE  ORDERS
SET     STATUS = COALESCE (PREVIOUS_STATUS, '1002'),
        PREVIOUS_STATUS = NULL,
        UPDATED_ON = SYSDATE
WHERE   STATUS = '1005'
AND     UPDATED_ON <= V_EXPIRED_TIME;

COMMIT;

EXCEPTION WHEN OTHERS THEN
BEGIN
        V_ERROR_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

        ROLLBACK;       -- rollback update

        INSERT_DAEMON_MESSAGE (PG_THIS_PACKAGE, V_THIS_PROCEDURE, V_ERROR_MESSAGE);

END;


END CHECK_EXPIRED_SCRUB_SESSIONS;


FUNCTION CURRENTLY_RUNNING (p_job_name IN VARCHAR2) RETURN BOOLEAN AS

/*-----------------------------------------------------------------------------
Description:
        Function looks for the daemon in the DBMS_JOB queue.  Returns
        a TRUE if it is found, or a FALSE if it is not.

Returns:
        BOOLEAN value indicating if job is already in the queue.
-----------------------------------------------------------------------------*/

CURSOR  CHECK_CUR IS
SELECT  'X'
FROM    USER_JOBS
WHERE   WHAT LIKE p_job_name||'%';

V_CHECK    VARCHAR2(1);
V_RETURN   BOOLEAN;

BEGIN

OPEN CHECK_CUR;
FETCH CHECK_CUR INTO V_CHECK;

IF (CHECK_CUR%FOUND) THEN

        V_RETURN := TRUE;

ELSE

        V_RETURN := FALSE;

END IF;

CLOSE CHECK_CUR;

RETURN V_RETURN;

END CURRENTLY_RUNNING;



PROCEDURE START_DAEMON AS

/*-----------------------------------------------------------------------------
Description:
        Place the daemon listener in the DBMS JOB queue.
-----------------------------------------------------------------------------*/

V_THIS_PROCEDURE        VARCHAR2 (50) := 'START_DAEMON';
V_JOB                   BINARY_INTEGER;
V_ERROR_MESSAGE         VARCHAR2 (1000);

BEGIN

-- Make sure daemon is not already running in the queue.
IF CURRENTLY_RUNNING(pg_daemon_string) THEN

        INSERT_DAEMON_MESSAGE (PG_THIS_PACKAGE, V_THIS_PROCEDURE, 'SCRUB DAEMON IS ALREADY RUNNING');

ELSE

        DBMS_JOB.SUBMIT(V_JOB, PG_DAEMON_STRING, SYSDATE, PG_INTERVAL);
        COMMIT;
        INSERT_DAEMON_MESSAGE (PG_THIS_PACKAGE, V_THIS_PROCEDURE, 'SCRUB DAEMON STARTED');

END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        V_ERROR_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

        INSERT_DAEMON_MESSAGE (PG_THIS_PACKAGE, V_THIS_PROCEDURE, V_ERROR_MESSAGE);
END;

END START_DAEMON;



PROCEDURE STOP_DAEMON
AS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
-----------------------------------------------------------------------------*/

V_THIS_PROCEDURE        VARCHAR2 (50) := 'STOP_DAEMON';

CURSOR  JOB_CUR IS
SELECT  JOB
FROM    USER_JOBS
WHERE   WHAT = PG_DAEMON_STRING;

V_JOB   NUMBER;

BEGIN

OPEN JOB_CUR;
FETCH JOB_CUR INTO V_JOB;

IF (JOB_CUR%FOUND) THEN

        DBMS_JOB.REMOVE(V_JOB);
        COMMIT;
        INSERT_DAEMON_MESSAGE (PG_THIS_PACKAGE, V_THIS_PROCEDURE, 'SCRUB DAEMON STOPPED');

ELSE
        INSERT_DAEMON_MESSAGE (PG_THIS_PACKAGE, V_THIS_PROCEDURE, 'SCRUB DAEMON NOT RUNNING');

END IF;

CLOSE JOB_CUR;

END STOP_DAEMON;


PROCEDURE DAEMON
AS

/*-----------------------------------------------------------------------------
Description:
        Procedure sits in the DBMS_JOB queue and wakes at a specified
        interval.  Checks for expired scrub sessions.
-----------------------------------------------------------------------------*/

BEGIN

    CHECK_EXPIRED_SCRUB_SESSIONS;

END DAEMON;


PROCEDURE start_lost_order_monitor(p_min_age IN NUMBER, p_max_age IN NUMBER) AS

   v_this_procedure        VARCHAR2(50) := 'start_lost_order_monitor';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);
   v_submission_time       DATE;
   v_context               frp.global_parms.context%TYPE := 'lost_order_monitor';

BEGIN

   v_submit_string := v_context||'('||TO_CHAR(p_min_age)||','||TO_CHAR(p_max_age)||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (v_context) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(v_context,'INTERVAL');
      EXECUTE IMMEDIATE 'select '||v_interval||' from dual' INTO v_submission_time;
      dbms_job.submit(v_job, v_submit_string, v_submission_time, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);


END start_lost_order_monitor;


PROCEDURE stop_lost_order_monitor AS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'stop_lost_order_monitor';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_context               frp.global_parms.context%TYPE := 'lost_order_monitor';

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what LIKE v_context||'%';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_context||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END stop_lost_order_monitor;

END SCRUB_DAEMON_PKG;
.
/
