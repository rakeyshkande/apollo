CREATE OR REPLACE
PROCEDURE scrub.UNLOCK_ORDER (in_master_order_number in orders.master_order_number%type) is
begin
   update orders
   set    status = previous_status
   where  master_order_number = in_master_order_number;
   commit;
end unlock_order;
.
/
