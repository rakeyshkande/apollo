create or replace PACKAGE BODY scrub.ORDERS_QUERY_PKG AS


FUNCTION FORMAT_ORDER_DATE
(
IN_ORDER_DATE                   IN ORDERS.ORDER_DATE%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for reformatting a string with a format
        of 'MM/DD/YYYY' OR 'MM/DD/YY' to a format of 'YYYYMMDD'.  If the
        string is not in the valid format, the original string is returned.
        Used in the search_for_orders procedure.
-----------------------------------------------------------------------------*/

V_ORDER_DATE    VARCHAR2 (100);

BEGIN

-- convert the date format
IF SUBSTR (IN_ORDER_DATE, -3, 1) = '/' THEN
        V_ORDER_DATE := TO_CHAR (TO_DATE (IN_ORDER_DATE, 'MM/DD/YY'), 'YYYYMMDD HH24:MI:SS');
ELSE
        V_ORDER_DATE := TO_CHAR (TO_DATE (IN_ORDER_DATE, 'MM/DD/YYYY'), 'YYYYMMDD HH24:MI:SS');
END IF;

RETURN V_ORDER_DATE;

EXCEPTION WHEN OTHERS THEN
        RETURN IN_ORDER_DATE;

END FORMAT_ORDER_DATE;


PROCEDURE SEARCH_FOR_ORDERS
(
IN_CONFIRMATION_NUMBER          IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
IN_ORDER_ORIGIN                 IN ORDERS.ORDER_ORIGIN%TYPE,
IN_BUYERS_LAST_NAME             IN BUYERS.LAST_NAME%TYPE,
IN_BUYERS_PHONE_NUMBER          IN BUYER_PHONES.PHONE_NUMBER%TYPE,
IN_EMAIL                        IN BUYER_EMAILS.EMAIL%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_RECIPIENTS_LAST_NAME         IN RECIPIENTS.LAST_NAME%TYPE,
IN_RECIPIENTS_PHONE_NUMBER      IN RECIPIENT_PHONES.PHONE_NUMBER%TYPE,
IN_DESTINATION_TYPE             IN RECIPIENT_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_CSR_ID_SEARCH                IN ORDERS.CSR_ID%TYPE,
IN_FROM_DATE                    IN ORDERS.ORDER_DATE%TYPE,
IN_TO_DATE                      IN ORDERS.ORDER_DATE%TYPE,
IN_STATUS_FLAG                  IN VARCHAR2,
IN_CSR_ID_QUERY                 IN ORDERS.CSR_ID%TYPE,
IN_ORDER_BY_IND                 IN VARCHAR2,
IN_EXCLUDE_FLAG                 IN VARCHAR2,
IN_SOURCE_CODE			IN ORDERS.SOURCE_CODE%TYPE,
IN_SCRUB_REASONS	        IN VARCHAR2,
IN_PRODUCT_PROPERTY		IN VARCHAR2,
IN_PREFERRED_PARTNER            IN VARCHAR2,
IN_INCLUDE_BEING_SCRUBBED       IN VARCHAR2,
IN_PC_MEMBERSHIP_ID             IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order information using
        the given search criteria.  All of the input parameters are optional.
        There should be at least one specified to reduce the records returned.
        The SQL query is dynamically built based on the presence of a given
        parameter.  Critiera for order details should be checked first
        because of the required join to orders.

        Special processing:
        If from date is specified without the to date, search for orders
        with a order date equal to the specified date.  If both are specified,
        search for orders with a order date between the dates specified.

        If status flag is not specified, search for invalid orders (status 1002 or 1003).
        If status flag is 'R', search for removed orders (status 1006 or detail status 2004).
        If status flag is 'P', search for pending orders (status 1007 or detail status 2005).
        If status flag is 'A', search for abandoned orders (status 1010 or detail status 2011).
        If status flag is 'E', search for all orders (do not check status).

Input:
        confirmation_number             varchar2 (goes with orders.master_order_number or order_details.external_order_number or order_detail.ariba_po_number)
        order_origin                    varchar2 (search for origin)
        buyers_last_number              varchar2
        buyers_phone_number             varchar2
        email                           varchar2
        delivery_date                   varchar2
        ship_date                       varchar2
        product_id                      varchar2
        recipients_last_name            varchar2
        recipients_phone_number         varchar2
        destination_type                varchar2
        csr_id                          varchar2 (search for csr id)
        end_date                        varchar2
        status_flag                     varchar2
        csr_id                          varchar2 (searching csr)
        order_by_ind                    varchar2 (D - order by delivery date, S - order by ship date)
        exclude_flag                    varchar2 (excluds viewed orders for the csr)
        source_code			varchar2
	scrub_reasons	        	varchar2
        product_property		varchar2 (this is the column name on ftd_apps.product_master)

Output:
        cursor result set containing all fields in orders if status flag is null
        order_guid                      varchar2

        additional fields if status flag specified
        order_origin                    varchar2
        status                          varchar2
        csr_id                          varchar2
        master_order_number             varchar2
        order_date                      varchar2
        products_total                  varchar2
        order_total                     varchar2
        last_name                       varchar2
        first_name                      varchar2
        city                            varchar2
        state_province                  varchar2

        if in debug mode, the cursor will return the built sql statement
-----------------------------------------------------------------------------*/

V_DEBUG                 BOOLEAN := FALSE; -- debug mode will display the sql string
V_SQL                   VARCHAR2 (16000);

-- select clause variables
V_BASE                  VARCHAR2 (500) := 'SELECT ';
V_FIELDS                VARCHAR2 (500) := 'O.ORDER_GUID ';
V_GROUPS                VARCHAR2 (500) := 'O.ORDER_GUID ';
V_ADDTL_FIELDS          VARCHAR2 (500) := ', O.ORDER_ORIGIN, O.STATUS, O.CSR_ID, O.MASTER_ORDER_NUMBER, O.ORDER_DATE, O.PRODUCTS_TOTAL, O.ORDER_TOTAL, B.LAST_NAME, B.FIRST_NAME, BA.CITY, BA.STATE_PROVINCE ';
V_ADDTL_FIELD_PP        VARCHAR2 (500) := ', DECODE(PRTM.PREFERRED_PARTNER_FLAG, null, null, ''N'', null, PRTM.PARTNER_NAME) AS PREFERRED_PARTNER_NAME ';
V_ADDTL_GROUP_PP        VARCHAR2 (500) := ', DECODE(PRTM.PREFERRED_PARTNER_FLAG, null, null, ''N'', null, PRTM.PARTNER_NAME) ';
V_FROM                  VARCHAR2 (500) := 'FROM SCRUB.ORDERS O JOIN SCRUB.ORDER_DETAILS OD ON O.ORDER_GUID = OD.ORDER_GUID ';

-- join clause variables
V_JOIN                  VARCHAR2 (4000);

V_CONFIRMATION_NUMBER   VARCHAR2 (500) := 'AND (UPPER(O.MASTER_ORDER_NUMBER) LIKE ''' || UPPER(IN_CONFIRMATION_NUMBER) || ''' OR OD.EXTERNAL_ORDER_NUMBER LIKE ''' || UPPER(IN_CONFIRMATION_NUMBER) || ''' OR OD.ARIBA_PO_NUMBER LIKE ''' || UPPER(IN_CONFIRMATION_NUMBER)|| ''') ';
V_DELIVERY_DATE         VARCHAR2 (500) := 'AND OD.DELIVERY_DATE = ''' || IN_DELIVERY_DATE || ''' ';
V_SHIP_DATE             VARCHAR2 (500) := 'AND OD.SHIP_DATE = ''' || IN_SHIP_DATE || ''' ';
V_PRODUCT_ID            VARCHAR2 (500) := 'AND (UPPER (OD.PRODUCT_ID) LIKE ''' || UPPER (IN_PRODUCT_ID) || ''' OR UPPER (OD.SUBCODE) LIKE ''' || UPPER (IN_PRODUCT_ID) || ''') ';
V_PC_MEMBERSHIP_ID      VARCHAR2 (500) := 'AND OD.PC_MEMBERSHIP_ID = ''' || IN_PC_MEMBERSHIP_ID || ''' ';

V_RECIPIENTS_JOIN       VARCHAR2 (500) := 'JOIN SCRUB.RECIPIENTS R ON OD.RECIPIENT_ID = R.RECIPIENT_ID ';
V_RECIPIENTS_LAST_NAME  VARCHAR2 (500) := 'AND UPPER(R.LAST_NAME) LIKE ''' || UPPER(IN_RECIPIENTS_LAST_NAME) || ''' ';

V_RECIPIENT_PHONES_JOIN         VARCHAR2 (500) := 'JOIN SCRUB.RECIPIENT_PHONES RP ON R.RECIPIENT_ID = RP.RECIPIENT_ID ';
V_RECIPIENTS_PHONE_NUMBER       VARCHAR2 (500) := 'AND RP.PHONE_NUMBER LIKE ''' || IN_RECIPIENTS_PHONE_NUMBER || ''' ';

V_RECIPIENTS_ADDR_JOIN  VARCHAR2 (500) := 'JOIN SCRUB.RECIPIENT_ADDRESSES RA ON R.RECIPIENT_ID = RA.RECIPIENT_ID ';
V_DESTINATION_TYPE      VARCHAR2 (500) := 'AND UPPER (RA.ADDRESS_TYPE) = ''' || UPPER (IN_DESTINATION_TYPE) || ''' ';

V_BUYERS_JOIN           VARCHAR2 (500) := 'JOIN SCRUB.BUYERS B ON O.BUYER_ID = B.BUYER_ID ';
V_BUYERS_LAST_NAME      VARCHAR2 (500) := 'AND UPPER(B.LAST_NAME) LIKE ''' || UPPER(IN_BUYERS_LAST_NAME) || ''' ';

V_BUYER_ADDRESSES_JOIN  VARCHAR2 (500) := 'JOIN SCRUB.BUYER_ADDRESSES BA ON B.BUYER_ID = BA.BUYER_ID AND BA.BUYER_ADDRESS_ID = (SELECT BA2.BUYER_ADDRESS_ID FROM BUYER_ADDRESSES BA2 WHERE BA2.BUYER_ID = BA.BUYER_ID AND ROWNUM = 1) ';

V_BUYER_PHONES_JOIN     VARCHAR2 (500) := 'JOIN SCRUB.BUYER_PHONES BP ON B.BUYER_ID = BP.BUYER_ID ';
V_BUYERS_PHONE_NUMBER   VARCHAR2 (500) := 'AND BP.PHONE_NUMBER LIKE ''' || IN_BUYERS_PHONE_NUMBER || ''' ';

V_BUYER_EMAILS_JOIN     VARCHAR2 (500) := 'JOIN SCRUB.BUYER_EMAILS BE ON O.BUYER_EMAIL_ID = BE.BUYER_EMAIL_ID ';
V_EMAIL                 VARCHAR2 (500) := 'AND UPPER (BE.EMAIL) LIKE ''' || UPPER (IN_EMAIL) || ''' ';

-- Preferred Partner (e.g., USAA).
-- To get only orders for specific partner
V_PREFERRED_PARTNER_FILTER   VARCHAR2 (500) := 'JOIN FTD_APPS.SOURCE_PROGRAM_REF SPR ON SPR.SOURCE_CODE = OD.SOURCE_CODE ' ||
                                               'JOIN FTD_APPS.PARTNER_PROGRAM PPGM ON PPGM.PROGRAM_NAME = SPR.PROGRAM_NAME ' ||
                                               'JOIN FTD_APPS.PARTNER_MASTER PRTM ON PRTM.PARTNER_NAME = PPGM.PARTNER_NAME AND PPGM.PARTNER_NAME = ''' || IN_PREFERRED_PARTNER || ''' ';

-- To get all orders (also used to exclude all partner orders when used with V_PREFERRED_PARTNER_EXCLUDE)
V_PREFERRED_PARTNER_JOIN     VARCHAR2 (500) := 'LEFT OUTER JOIN FTD_APPS.SOURCE_PROGRAM_REF SPR ON SPR.SOURCE_CODE = OD.SOURCE_CODE ' ||
                                               'LEFT OUTER JOIN FTD_APPS.PARTNER_PROGRAM PPGM ON PPGM.PROGRAM_NAME = SPR.PROGRAM_NAME ' ||
                                               'LEFT OUTER JOIN FTD_APPS.PARTNER_MASTER PRTM ON PRTM.PARTNER_NAME = PPGM.PARTNER_NAME ';
-- To exclude all partner orders
V_PREFERRED_PARTNER_EXCLUDE  VARCHAR2 (500) := 'AND (PRTM.PREFERRED_PARTNER_FLAG IS NULL OR PRTM.PREFERRED_PARTNER_FLAG = ''N'') ';

-- where clause variables
V_ORDERS_WHERE          VARCHAR2 (4000) := 'WHERE OD.STATUS IN (''2002'', ''2003'') ';
V_ORDERS_REMOVED        VARCHAR2 (500) := 'WHERE OD.STATUS = ''2004'' ';
V_ORDERS_PENDING        VARCHAR2 (500) := 'WHERE OD.STATUS = ''2005'' ';
V_ORDERS_ABANDONED      VARCHAR2 (500) := 'WHERE OD.STATUS = ''2011'' ';
V_ORDERS_EVERYTHING     VARCHAR2 (500) := 'WHERE 1=1 ';

V_ORDER_SCRUBBED        VARCHAR2 (500) := 'AND O.STATUS <> ''1005'' ';

V_ORDER_EXCLUDE         VARCHAR2 (500) := 'AND O.ORDER_GUID NOT IN (SELECT ORDER_GUID FROM CSR_VIEWED_ORDERS WHERE CSR_ID = ''' || IN_CSR_ID_QUERY || ''') ';

V_ORDER_ORIGIN          VARCHAR2 (500) := 'AND UPPER (O.ORDER_ORIGIN) IN ' || UPPER (IN_ORDER_ORIGIN) || ' ';
V_CSR_ID                VARCHAR2 (500) := 'AND UPPER (O.CSR_ID) = ''' || UPPER (IN_CSR_ID_SEARCH) || ''' ';
V_ORDER_DATE            VARCHAR2 (500) := 'AND O.ORDER_DATE = ''' || IN_FROM_DATE || ''' ';

V_SOURCE_CODE   VARCHAR2 (500) := 'AND UPPER(O.SOURCE_CODE) = ''' || UPPER(IN_SOURCE_CODE) || ''' ';

V_PRODUCT_PROPERTY_JOIN VARCHAR2(500) := 'JOIN FTD_APPS.PRODUCT_MASTER PM ON PM.PRODUCT_ID = OD.PRODUCT_ID ';
V_PRODUCT_PROPERTY	VARCHAR2(500) := 'AND PM.' || IN_PRODUCT_PROPERTY || ' = ''Y'' ';

V_REASON_RESTRICTION_IN     VARCHAR2 (500) := 'AND EXISTS (SELECT 1 FROM FRP.STATS_VALIDATION V JOIN FRP.STATS_VALIDATION_REASONS VR ON V.STATS_VALIDATION_ID = VR.STATS_VALIDATION_ID WHERE V.EXTERNAL_ORDER_NUMBER = OD.EXTERNAL_ORDER_NUMBER AND VR.REASON IN (' || IN_SCRUB_REASONS || '))';
V_REASON_RESTRICTION_NOTIN  VARCHAR2 (500) := 'AND NOT EXISTS (SELECT 1 FROM FRP.STATS_VALIDATION V JOIN FRP.STATS_VALIDATION_REASONS VR ON V.STATS_VALIDATION_ID = VR.STATS_VALIDATION_ID WHERE V.EXTERNAL_ORDER_NUMBER = OD.EXTERNAL_ORDER_NUMBER AND VR.REASON NOT IN (' || IN_SCRUB_REASONS || '))';

-- if a date range is specified then only order date in the format 'Mon Jan 12 13:01:01 CDT 2004' will be searched
V_ORDER_DATE_RANGE      VARCHAR2 (500) := 'AND O.ORDER_DATE LIKE ''%_:__:__ ___ ____'' AND TO_CHAR (TO_DATE (SUBSTR (O.ORDER_DATE, 5, LENGTH (O.ORDER_DATE) - 12) || SUBSTR (O.ORDER_DATE, -5), ''MON DD HH24:MI:SS YYYY''), ''YYYYMMDD HH24:MI:SS'') BETWEEN ''' || FORMAT_ORDER_DATE(IN_FROM_DATE) || ''' AND ''' || FORMAT_ORDER_DATE(IN_TO_DATE) || ''' ';

-- group by variables
V_GROUP_BY              VARCHAR2 (500) := 'GROUP BY ';

-- order by variables
-- The is assuming that the date format of delivery_date and ship_date are mm/dd/yyyy
V_ORDER_BY              VARCHAR2 (4000) := 'ORDER BY ';
V_ORDER_DELIVERY        VARCHAR2 (500) := 'MIN (SUBSTR (OD.DELIVERY_DATE, 7, 4) || SUBSTR (OD.DELIVERY_DATE, 1, 2) || SUBSTR (OD.DELIVERY_DATE, 4, 2) || COALESCE ((SELECT TIME_ZONE FROM FTD_APPS.STATE_MASTER WHERE RA.STATE_PROVINCE = STATE_MASTER_ID), ''X'')) ';
V_ORDER_SHIP            VARCHAR2 (500) := 'MIN (SUBSTR (OD.SHIP_DATE, 7, 4) || SUBSTR (OD.SHIP_DATE, 1, 2) || SUBSTR (OD.SHIP_DATE, 4, 2) || COALESCE ((SELECT TIME_ZONE FROM FTD_APPS.STATE_MASTER WHERE RA.STATE_PROVINCE = STATE_MASTER_ID), ''X'')) ';
V_ORDER_BOTH            VARCHAR2 (1000) := 'MIN ( CASE ' ||
                                                'WHEN (SUBSTR (OD.DELIVERY_DATE, 7, 4) || SUBSTR (OD.DELIVERY_DATE, 1, 2) || SUBSTR (OD.DELIVERY_DATE, 4, 2) <= SUBSTR (OD.SHIP_DATE, 7, 4) || SUBSTR (OD.SHIP_DATE, 1, 2) || SUBSTR (OD.SHIP_DATE, 4, 2) AND OD.DELIVERY_DATE IS NOT NULL AND OD.SHIP_DATE IS NOT NULL) OR OD.SHIP_DATE IS NULL ' ||
                                                        'THEN SUBSTR (OD.DELIVERY_DATE, 7, 4) || SUBSTR (OD.DELIVERY_DATE, 1, 2) || SUBSTR (OD.DELIVERY_DATE, 4, 2) ' ||
                                                'WHEN (SUBSTR (OD.DELIVERY_DATE, 7, 4) || SUBSTR (OD.DELIVERY_DATE, 1, 2) || SUBSTR (OD.DELIVERY_DATE, 4, 2) > SUBSTR (OD.SHIP_DATE, 7, 4) || SUBSTR (OD.SHIP_DATE, 1, 2) || SUBSTR (OD.SHIP_DATE, 4, 2) AND OD.DELIVERY_DATE IS NOT NULL AND OD.SHIP_DATE IS NOT NULL) OR OD.DELIVERY_DATE IS NULL ' ||
                                                        'THEN SUBSTR (OD.SHIP_DATE, 7, 4) || SUBSTR (OD.SHIP_DATE, 1, 2) || SUBSTR (OD.SHIP_DATE, 4, 2) ' ||
                                                'END || COALESCE ((SELECT TIME_ZONE FROM FTD_APPS.STATE_MASTER WHERE RA.STATE_PROVINCE = STATE_MASTER_ID), ''X'') )';


BEGIN

-- build the select clause
IF IN_STATUS_FLAG IS NOT NULL THEN
        V_FIELDS := V_FIELDS || V_ADDTL_FIELDS || V_ADDTL_FIELD_PP;
        V_GROUPS := V_GROUPS || V_ADDTL_FIELDS || V_ADDTL_GROUP_PP;
END IF;

-- build the join clause
-- add critiria for order details specified fields
IF COALESCE (IN_CONFIRMATION_NUMBER, IN_DELIVERY_DATE, IN_SHIP_DATE, IN_PRODUCT_ID, IN_PC_MEMBERSHIP_ID) IS NOT NULL THEN

        IF IN_CONFIRMATION_NUMBER IS NOT NULL THEN
                V_JOIN := V_JOIN || V_CONFIRMATION_NUMBER;
        END IF;

        IF IN_DELIVERY_DATE IS NOT NULL THEN
                V_JOIN := V_JOIN || V_DELIVERY_DATE;
        END IF;

        IF IN_SHIP_DATE IS NOT NULL THEN
                V_JOIN := V_JOIN || V_SHIP_DATE;
        END IF;

        IF IN_PRODUCT_ID IS NOT NULL THEN
                V_JOIN := V_JOIN || V_PRODUCT_ID;
        END IF;

		IF IN_PC_MEMBERSHIP_ID IS NOT NULL THEN
                V_JOIN := V_JOIN || V_PC_MEMBERSHIP_ID;
        END IF;
		
END IF;

-- add recipients to the join
V_JOIN := V_JOIN || V_RECIPIENTS_JOIN;

-- recipients require join to order details to get to orders
IF COALESCE (IN_RECIPIENTS_LAST_NAME, IN_RECIPIENTS_PHONE_NUMBER, IN_DESTINATION_TYPE) IS NOT NULL THEN

        IF IN_RECIPIENTS_LAST_NAME IS NOT NULL THEN
                V_JOIN := V_JOIN || V_RECIPIENTS_LAST_NAME;
        END IF;

        -- recipient phones require join to recipients to get to orders
        IF IN_RECIPIENTS_PHONE_NUMBER IS NOT NULL THEN
                V_JOIN := V_JOIN || V_RECIPIENT_PHONES_JOIN || V_RECIPIENTS_PHONE_NUMBER;
        END IF;

END IF;         -- end order details join block

-- add recipients addresses to the join
V_JOIN := V_JOIN || V_RECIPIENTS_ADDR_JOIN;

-- destinations require join to recipients to get to orders
IF IN_DESTINATION_TYPE IS NOT NULL THEN
        V_JOIN := V_JOIN || V_DESTINATION_TYPE;
END IF;

-- join buyer email to order if specified
IF IN_EMAIL IS NOT NULL THEN
        V_JOIN := V_JOIN || V_BUYER_EMAILS_JOIN || V_EMAIL;
END IF;

-- join buyers to order for specified fields or if requesting pending or removed orders
IF COALESCE (IN_BUYERS_LAST_NAME, IN_BUYERS_PHONE_NUMBER) IS NOT NULL OR
   IN_STATUS_FLAG IS NOT NULL THEN
        V_JOIN := V_JOIN || V_BUYERS_JOIN;

        IF IN_BUYERS_LAST_NAME IS NOT NULL THEN
                V_JOIN := V_JOIN || V_BUYERS_LAST_NAME;
        END IF;

        -- buyer phones require join to buyers to get to orders
        IF IN_BUYERS_PHONE_NUMBER IS NOT NULL THEN
                V_JOIN := V_JOIN || V_BUYER_PHONES_JOIN || V_BUYERS_PHONE_NUMBER;
        END IF;

        IF IN_STATUS_FLAG IS NOT NULL THEN
                -- include buyers_addresses if requesting pending or removed orders
                V_JOIN := V_JOIN || V_BUYER_ADDRESSES_JOIN;
        END IF;
END IF;

-- join product_master if product property is specified
IF IN_PRODUCT_PROPERTY IS NOT NULL AND LENGTH(IN_PRODUCT_PROPERTY) > 0 THEN
	V_JOIN := V_JOIN || V_PRODUCT_PROPERTY_JOIN;
END IF;

-- Preferred Partner (e.g., USAA)
IF (IN_PREFERRED_PARTNER IS NOT NULL AND IN_PREFERRED_PARTNER != 'NON_PREFERRED' AND IN_PREFERRED_PARTNER != 'VIP') THEN
    -- To get only orders for specific partner
    V_JOIN := V_JOIN || V_PREFERRED_PARTNER_FILTER;
ELSE
    -- To get all orders (or exclude partner orders when used with V_PREFERRED_PARTNER_EXCLUDE)
    V_JOIN := V_JOIN || V_PREFERRED_PARTNER_JOIN;
END IF;


-- build the where clause
-- set the orders where criteria
IF IN_STATUS_FLAG = 'R' THEN
        V_ORDERS_WHERE := V_ORDERS_REMOVED;
ELSIF IN_STATUS_FLAG = 'P' THEN
        V_ORDERS_WHERE := V_ORDERS_PENDING;
ELSIF IN_STATUS_FLAG = 'A' THEN
        V_ORDERS_WHERE := V_ORDERS_ABANDONED;
ELSIF IN_STATUS_FLAG = 'E' THEN
        V_ORDERS_WHERE := V_ORDERS_EVERYTHING;
END IF;

IF IN_INCLUDE_BEING_SCRUBBED <> 'Y' THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_SCRUBBED;
END IF;

-- set the included origins that is always specified
IF IN_ORDER_ORIGIN IS NOT NULL THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_ORIGIN;
END IF;

-- if specified only include certain order with a given source code
IF IN_SOURCE_CODE IS NOT NULL THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_SOURCE_CODE;
END IF;

-- and reason if needed
IF IN_SCRUB_REASONS IS NOT NULL THEN
	V_ORDERS_WHERE := V_ORDERS_WHERE || V_REASON_RESTRICTION_IN;
	V_ORDERS_WHERE := V_ORDERS_WHERE || V_REASON_RESTRICTION_NOTIN;
END IF;

IF IN_CSR_ID_SEARCH IS NOT NULL THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_CSR_ID;
END IF;

IF IN_FROM_DATE IS NOT NULL AND IN_TO_DATE IS NOT NULL THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_DATE_RANGE;
ELSIF IN_FROM_DATE IS NOT NULL THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_DATE;
END IF;

IF IN_EXCLUDE_FLAG = 'Y' THEN
        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_EXCLUDE;
END IF;

-- if product property is specified
IF IN_PRODUCT_PROPERTY IS NOT NULL AND LENGTH(IN_PRODUCT_PROPERTY) > 0 THEN
	V_ORDERS_WHERE := V_ORDERS_WHERE || V_PRODUCT_PROPERTY;
END IF;

-- To exclude all Preferred Partners (e.g., USAA)
IF IN_PREFERRED_PARTNER = 'NON_PREFERRED' THEN
    -- To exclude all partner orders
    V_ORDERS_WHERE := V_ORDERS_WHERE || V_PREFERRED_PARTNER_EXCLUDE;
END IF;

-- To get all VIP customer orders 
IF(IN_PREFERRED_PARTNER = 'VIP') THEN
  V_ORDERS_WHERE := V_ORDERS_WHERE || ' AND O.BUYER_ID IN (SELECT B1.BUYER_ID FROM SCRUB.BUYERS B1 WHERE B1.CLEAN_CUSTOMER_ID IN (SELECT C.CUSTOMER_ID FROM CLEAN.CUSTOMER C WHERE C.VIP_CUSTOMER=''Y'')) ';
END IF;

-- build order by clause
IF IN_ORDER_BY_IND = 'D' THEN
        V_ORDER_BY := V_ORDER_BY || V_ORDER_DELIVERY;
ELSIF IN_ORDER_BY_IND = 'S' THEN
        V_ORDER_BY := V_ORDER_BY || V_ORDER_SHIP;
ELSE
        V_ORDER_BY := V_ORDER_BY || V_ORDER_BOTH;
END IF;

-- build the entire sql statement
V_SQL := V_BASE || V_FIELDS || V_FROM || V_JOIN || V_ORDERS_WHERE || V_GROUP_BY || V_GROUPS || V_ORDER_BY;

-- execute the sql statement
IF V_DEBUG THEN
        OPEN OUT_CUR FOR SELECT V_SQL AS DEBUG_MODE FROM DUAL;
ELSE
        OPEN OUT_CUR FOR V_SQL;
END IF;

END SEARCH_FOR_ORDERS;


PROCEDURE SEARCH_FOR_ORDERS
(
IN_CONFIRMATION_NUMBER          IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
IN_ORDER_ORIGIN                 IN ORDERS.ORDER_ORIGIN%TYPE,
IN_BUYERS_LAST_NAME             IN BUYERS.LAST_NAME%TYPE,
IN_BUYERS_PHONE_NUMBER          IN BUYER_PHONES.PHONE_NUMBER%TYPE,
IN_EMAIL                        IN BUYER_EMAILS.EMAIL%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_RECIPIENTS_LAST_NAME         IN RECIPIENTS.LAST_NAME%TYPE,
IN_RECIPIENTS_PHONE_NUMBER      IN RECIPIENT_PHONES.PHONE_NUMBER%TYPE,
IN_DESTINATION_TYPE             IN RECIPIENT_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_CSR_ID_SEARCH                IN ORDERS.CSR_ID%TYPE,
IN_FROM_DATE                    IN ORDERS.ORDER_DATE%TYPE,
IN_TO_DATE                      IN ORDERS.ORDER_DATE%TYPE,
IN_STATUS_FLAG                  IN VARCHAR2,
IN_CSR_ID_QUERY                 IN ORDERS.CSR_ID%TYPE,
IN_ORDER_BY_IND                 IN VARCHAR2,
IN_SOURCE_CODE			IN ORDERS.SOURCE_CODE%TYPE,
IN_SCRUB_REASONS	        IN VARCHAR2,
IN_PRODUCT_PROPERTY		IN VARCHAR2,
IN_PREFERRED_PARTNER            IN VARCHAR2,
IN_INCLUDE_BEING_SCRUBBED       IN VARCHAR2,
IN_PC_MEMBERSHIP_ID             IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS


/*-----------------------------------------------------------------------------
Description:
        This procedure calls the original search_for_orders with the
        new search flag set to 'Y'.

Input:
        confirmation_number             varchar2 (goes with orders.master_order_number or order_details.external_order_number)
        order_origin                    varchar2
        buyers_last_number              varchar2
        buyers_phone_number             varchar2
        email                           varchar2
        delivery_date                   varchar2
        ship_date                       varchar2
        product_id                      varchar2
        recipients_last_name            varchar2
        recipients_phone_number         varchar2
        destination_type                varchar2
        csr_id                          varchar2 (search for csr id)
        end_date                        varchar2
        status_flag                     varchar2
        csr_id                          varchar2 (searching csr)
        order_by_ind                    varchar2 (D - order by delivery date, S - order by ship date)
-----------------------------------------------------------------------------*/


BEGIN

SEARCH_FOR_ORDERS (
        IN_CONFIRMATION_NUMBER,
        IN_ORDER_ORIGIN,
        IN_BUYERS_LAST_NAME,
        IN_BUYERS_PHONE_NUMBER,
        IN_EMAIL,
        IN_DELIVERY_DATE,
        IN_SHIP_DATE,
        IN_PRODUCT_ID,
        IN_RECIPIENTS_LAST_NAME,
        IN_RECIPIENTS_PHONE_NUMBER,
        IN_DESTINATION_TYPE,
        IN_CSR_ID_SEARCH,
        IN_FROM_DATE,
        IN_TO_DATE,
        IN_STATUS_FLAG,
        IN_CSR_ID_QUERY,
        IN_ORDER_BY_IND,
        'N',
        IN_SOURCE_CODE,
        IN_SCRUB_REASONS,
        IN_PRODUCT_PROPERTY,
        IN_PREFERRED_PARTNER,
        IN_INCLUDE_BEING_SCRUBBED,
		IN_PC_MEMBERSHIP_ID,
        OUT_CUR
);

END SEARCH_FOR_ORDERS;


PROCEDURE VIEW_ORDERS
(
IN_ORDER_GUID           IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order information for the
        given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in orders
        order_guid                      varchar2
        master_order_number             varchar2
        buyer_id                        number
        order_total                     varchar2
        products_total                  varchar2
        tax_total                       varchar2
        service_fee_total               varchar2
        shipping_fee_total              varchar2
        buyer_email_id                  number
        order_date                      varchar2
        order_origin                    varchar2
        membership_id                   number
        csr_id                          varchar2
        add_on_amount_total             varchar2
        partnership_bonus_points        varchar2
        source_code                     varchar2
        call_time                       varchar2
        dnis_code                       varchar2
        datamart_update_date            varchar2
        discount_total                  varchar2
        yellow_pages_code               varchar2
        ariba_buyer_asn_number          varchar2
        ariba_buyer_cookie              varchar2
        ariba_payload                   varchar2
        socket_timestamp                varchar2
        status                          varchar2
        source description              varchar2
        partner_id                      varchar2
        company_id                      varchar2
        company_name                    varchar2
        co_brand_credit_card_code       varchar2
        fraud                           varchar2
        previous_status                 varchar2
        buyer has free shipping			varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  O.ORDER_GUID,
                O.MASTER_ORDER_NUMBER,
                O.BUYER_ID,
                O.ORDER_TOTAL,
                O.PRODUCTS_TOTAL,
                O.TAX_TOTAL,
                O.SERVICE_FEE_TOTAL,
                O.SHIPPING_FEE_TOTAL,
                O.BUYER_EMAIL_ID,
                O.ORDER_DATE,
                O.ORDER_ORIGIN,
                O.MEMBERSHIP_ID,
                O.CSR_ID,
                O.ADD_ON_AMOUNT_TOTAL,
                O.PARTNERSHIP_BONUS_POINTS,
                O.SOURCE_CODE,
                O.CALL_TIME,
                O.DNIS_CODE,
                O.DATAMART_UPDATE_DATE,
                O.DISCOUNT_TOTAL,
                O.YELLOW_PAGES_CODE,
                O.ARIBA_BUYER_ASN_NUMBER,
                O.ARIBA_BUYER_COOKIE,
                O.ARIBA_PAYLOAD,
                O.SOCKET_TIMESTAMP,
                O.STATUS,
                FS.DESCRIPTION,
                FS.PARTNER_ID,
                FS.COMPANY_ID,
                CM.COMPANY_NAME,
                O.CO_BRAND_CREDIT_CARD_CODE,
                O.FRAUD,
                O.PREVIOUS_STATUS,
                O.LOSS_PREVENTION_INDICATOR,
                O.MP_REDEMPTION_RATE_AMT,
                O.BUYER_SIGNED_IN_FLAG,
                (select distinct 'Y' from scrub.order_details od
                    where od.order_guid = o.order_guid
                    and od.free_shipping_flag = 'Y') FREE_SHIPPING_USE_FLAG,
                (select distinct 'Y' from scrub.order_details od, ftd_apps.product_master pm
                    where od.order_guid = o.order_guid
                    and od.product_id = pm.product_id
                    and pm.product_sub_type = 'FREESHIP') FREE_SHIPPING_PURCHASE_FLAG,
				CM.URL,
				O.LANGUAGE_ID,
                fs.marketing_group, -- marketing_group and source_type are switched in ftd_apps.source
                fs.partner_name,
                O.is_joint_cart,
                o.add_on_discount_amount,
                o.buyer_has_free_shipping,
                fo.order_calc_type
        FROM    ORDERS O
        LEFT OUTER JOIN FTD_APPS.SOURCE FS
        ON      O.SOURCE_CODE = FS.SOURCE_CODE
        LEFT OUTER JOIN FTD_APPS.COMPANY_MASTER CM
        ON      FS.COMPANY_ID = CM.COMPANY_ID
        left outer join ftd_apps.origins fo
        on      fo.origin_id = upper(o.order_origin)
        WHERE   O.ORDER_GUID = IN_ORDER_GUID;

END VIEW_ORDERS;


PROCEDURE VIEW_ORDER_DETAILS
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail information
        for the given guid.

        DISTINCT is needed in this query since source code may be associated
        with multiple partners.  But since we join down to partner_master
        to get partner_name, the distinct will weed out duplicates (since
        partner_names should be same).

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in order details
        order_detail_id                 number
        order_guid                      varchar2
        line_number                     varchar2
        recipient_id                    number
        recipient_address_id            number
        product_id                      varchar2
        quantity                        varchar2
        ship_via                        varchar2
        ship_method                     varchar2
        ship_date                       varchar2
        delivery_date                   varchar2
        delivery_date_range_end         varchar2
        drop_ship_tracking_number       varchar2
        products_amount                 varchar2
        tax_amount                      varchar2
        service_fee_amount              varchar2
        shipping_fee_amount             varchar2
        add_on_amount                   varchar2
        discount_amount                 varchar2
        occasion_id                     varchar2
        last_minute_gift_signature      varchar2
        last_minute_number              varchar2
        last_minute_gift_email          varchar2
        external_order_number           varchar2
        color_1                         varchar2
        color_2                         varchar2
        substitute_acknowledgement      varchar2
        card_message                    varchar2
        card_signature                  varchar2
        order_comments                  varchar2
        order_contact_information       varchar2
        florist_number                  varchar2
        special_instructions            varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        external_order_total            varchar2
        size_indicator                  varchar2
        sender_info_release             varchar2
        status                          varchar2
        product_name                    varchar2
        florist_name                    varchar2
        miles_points                    varchar2
        ship_method_carrier             varchar2
        ship_method_florist             varchar2
        vendor_id                       varchar2
        color 1 description             varchar2
        color 2 description             varchar2
        subcode                         varchar2
        subcode_description             varchar2
        product_type                    varchar2
        display_order                   number
        origin                          varchar2
        item_of_the_week_flag           varchar2
        reinstate_flag                  varchar2
        description                     varchar2
        commission                      varchar2
        wholesale                       varchar2
        transaction                     varchar2
        pdb_price                       varchar2
        shipping_tax                    varchar2
        price_override_flag             varchar2
        discount_product_price          varchar2
        discount_type                   varchar2
        partner_cost                    varchar2
	personaization_data		varchar2
        updated_on_products_amount      date
        miles_points_amt                varchar2
        personal_greeting_id            varchar2
        no_tax_flag			varchar2
        premier_collection_flag         varchar2
        orig_source_code                varchar2
        orig_product_id                 varchar2
        orig_ship_method                varchar2
        orig_delivery_date              varchar2
        orig_total_fee_amount           varchar2
		tax1_name						varchar2
		tax1_description           		varchar2
		tax1_rate           			varchar2
		tax1_amount           			varchar2
		tax2_name           			varchar2
		tax2_description           		varchar2
		tax2_rate           			varchar2
		tax2_amount           			varchar2
		tax3_name           			varchar2
		tax3_description           		varchar2
		tax3_rate           			varchar2
		tax3_amount          			varchar2
		tax4_name           			varchar2
		tax4_description           		varchar2
		tax4_rate           			varchar2
		tax4_amount           			varchar2
		tax5_name           			varchar2
		tax5_description           		varchar2
		tax5_rate           			varchar2
		tax5_amount           			varchar2
		same_day_upcharge           	number,
		original_order_has_sdu			char,
		morning_delivery_fee			number,
    	original_order_has_mdf      	char,
    	prod_morning_delivery_flag		char,
    	orig_external_order_total		varchar2,
		PC_GROUP_ID           			varchar2,
		PC_MEMBERSHIP_ID           		varchar2,
		PC_FLAG           				varchar2,
		TIME_OF_SERVICE           		varchar2,
		LATE_CUTOFF_FEE					number,
		VENDOR_SUN_UPCHARGE				number,
		VENDOR_MON_UPCHARGE				number,
		PM.SHIPPING_SYSTEM              varchar2,
		original_order_has_sdufs		char
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  --DISTINCT
                OD.ORDER_DETAIL_ID,
                OD.ORDER_GUID,
                OD.LINE_NUMBER,
                OD.RECIPIENT_ID,
                OD.RECIPIENT_ADDRESS_ID,
                OD.PRODUCT_ID,
                OD.QUANTITY,
                OD.SHIP_VIA,
                OD.SHIP_METHOD,
                OD.SHIP_DATE,
                OD.DELIVERY_DATE,
                OD.DELIVERY_DATE_RANGE_END,
                OD.DROP_SHIP_TRACKING_NUMBER,
                OD.PRODUCTS_AMOUNT,
                OD.TAX_AMOUNT,
                OD.SERVICE_FEE_AMOUNT,
                OD.SHIPPING_FEE_AMOUNT,
                OD.ADD_ON_AMOUNT,
                OD.DISCOUNT_AMOUNT,
                OD.OCCASION_ID,
                OD.LAST_MINUTE_GIFT_SIGNATURE,
                OD.LAST_MINUTE_NUMBER,
                OD.LAST_MINUTE_GIFT_EMAIL,
                OD.EXTERNAL_ORDER_NUMBER,
                OD.COLOR_1,
                OD.COLOR_2,
                OD.SUBSTITUTE_ACKNOWLEDGEMENT,
                OD.CARD_MESSAGE,
                OD.CARD_SIGNATURE,
                OD.ORDER_COMMENTS,
                OD.ORDER_CONTACT_INFORMATION,
                OD.FLORIST_NUMBER,
                OD.SPECIAL_INSTRUCTIONS,
                OD.ARIBA_UNSPSC_CODE,
                OD.ARIBA_PO_NUMBER,
                OD.ARIBA_AMS_PROJECT_CODE,
                OD.ARIBA_COST_CENTER,
                OD.EXTERNAL_ORDER_TOTAL,
                OD.SIZE_INDICATOR,
                OD.SENDER_INFO_RELEASE,
                OD.STATUS,
                PM.PRODUCT_NAME,
                FM.FLORIST_NAME,
                OD.MILES_POINTS,
                PM.SHIP_METHOD_CARRIER,
                PM.SHIP_METHOD_FLORIST,
                null AS VENDOR_ID,
                CM1.DESCRIPTION AS COLOR_1_DESCRIPTION,
                CM2.DESCRIPTION AS COLOR_2_DESCRIPTION,
                OD.SUBCODE,
                PS.SUBCODE_DESCRIPTION,
                PM.PRODUCT_TYPE,
                S.DISPLAY_ORDER,
                OD.HP_SEQUENCE,
                OD.SOURCE_CODE,
                OD.ITEM_OF_THE_WEEK_FLAG,
                FS.DESCRIPTION,
                OD.REINSTATE_FLAG,
                OC.DESCRIPTION,
                OD.COMMISSION,
                OD.WHOLESALE,
                OD.TRANSACTION,
                OD.PDB_PRICE,
                OD.SHIPPING_TAX,
                OD.PRICE_OVERRIDE_FLAG,
                OD.DISCOUNT_PRODUCT_PRICE,
                OD.DISCOUNT_TYPE,
                OD.PARTNER_COST,
                OD.PERSONALIZATION_DATA,
                OD.UPDATED_ON_PRODUCTS_AMOUNT,
                OD.MILES_POINTS_AMT,
                OD.PERSONAL_GREETING_ID,
                PM.NO_TAX_FLAG,
                PM.PREMIER_COLLECTION_FLAG,
                OD.ORIG_SOURCE_CODE,
                OD.ORIG_PRODUCT_ID,
                OD.ORIG_SHIP_METHOD,
                OD.ORIG_DELIVERY_DATE,
                OD.ORIG_TOTAL_FEE_AMOUNT,
                DECODE(PRTM.PREFERRED_PARTNER_FLAG, null, null, 'N', null, PRTM.PARTNER_NAME) AS PREFERRED_PARTNER_NAME,
                NVL((SELECT 'Y' FROM FTD_APPS.PRODUCT_ATTR_RESTR_SOURCE_EXCL PARSE
			JOIN FTD_APPS.PRODUCT_ATTR_RESTR PAR ON PAR.PRODUCT_ATTR_RESTR_ID = PARSE.PRODUCT_ATTR_RESTR_ID
			WHERE PARSE.SOURCE_CODE = od.source_code
			AND PAR.PRODUCT_ATTR_RESTR_NAME = 'PRODUCT_TYPE'
			AND PAR.PRODUCT_ATTR_RESTR_VALUE = 'SDFC'),'N') SAME_DAY_FRESHCUT_FLAG,
                NVL((SELECT 'Y' FROM FTD_APPS.PRODUCT_ATTR_RESTR_SOURCE_EXCL PARSE
                 	JOIN FTD_APPS.PRODUCT_ATTR_RESTR PAR ON PAR.PRODUCT_ATTR_RESTR_ID = PARSE.PRODUCT_ATTR_RESTR_ID
                 	WHERE PARSE.SOURCE_CODE = OD.SOURCE_CODE
                 	AND PAR.PRODUCT_ATTR_RESTR_NAME = 'PRODUCT_TYPE'
                 	AND PAR.PRODUCT_ATTR_RESTR_VALUE = 'SDG'),'N') SAME_DAY_GIFT_FLAG,
                OD.BIN_SOURCE_CHANGED_FLAG,
                OD.DTL_FIRST_ORDER_DOMESTIC,
                OD.DTL_FIRST_ORDER_INTERNATIONAL,
                OD.DTL_SHIPPING_COST,
                OD.DTL_VENDOR_CHARGE,
                OD.DTL_VENDOR_SAT_UPCHARGE,
                OD.DTL_AK_HI_SPECIAL_SVC_CHARGE,
                OD.DTL_FUEL_SURCHARGE_AMT,
                DECODE(PRTM.PREFERRED_PARTNER_FLAG, null, null, 'N', null, PRTM.DISPLAY_NAME) AS PARTNER_DISPLAY_NAME,
                OD.APPLY_SURCHARGE_CODE,
                OD.SURCHARGE_DESCRIPTION,
                OD.SEND_SURCHARGE_TO_FLORIST_FLAG,
                OD.DISPLAY_SURCHARGE_FLAG, 
                OD.SHIPPING_FEE_AMOUNT_SAVED,
                OD.SERVICE_FEE_AMOUNT_SAVED,
                OD.FREE_SHIPPING_FLAG,
                PM.PRODUCT_SUB_TYPE,
				OD.TAX1_NAME, OD.TAX1_DESCRIPTION, OD.TAX1_RATE, OD.TAX1_AMOUNT,
				OD.TAX2_NAME, OD.TAX2_DESCRIPTION, OD.TAX2_RATE, OD.TAX2_AMOUNT,
				OD.TAX3_NAME, OD.TAX3_DESCRIPTION, OD.TAX3_RATE, OD.TAX3_AMOUNT,
				OD.TAX4_NAME, OD.TAX4_DESCRIPTION, OD.TAX4_RATE, OD.TAX4_AMOUNT,
				OD.TAX5_NAME, OD.TAX5_DESCRIPTION, OD.TAX5_RATE, OD.TAX5_AMOUNT,
				OD.SAME_DAY_UPCHARGE,
				OD.ORIGINAL_ORDER_HAS_SDU,
				OD.MORNING_DELIVERY_FEE,
        		OD.ORIGINAL_ORDER_HAS_MDF,
        		PM.MORNING_DELIVERY_FLAG AS PROD_MORNING_DELIVERY_FLAG,
        	OD.ORIG_EXTERNAL_ORDER_TOTAL,
			OD.PC_GROUP_ID,
			OD.PC_MEMBERSHIP_ID,
			OD.PC_FLAG,
			OD.TIME_OF_SERVICE,
                PM.NOVATOR_ID,
                od.add_on_discount_amount,
                OD.LATE_CUTOFF_FEE, 
                OD.VENDOR_SUN_UPCHARGE,
                OD.VENDOR_MON_UPCHARGE,
                PM.SHIPPING_SYSTEM,
                OD.ORIGINAL_ORDER_HAS_SDUFS
        FROM    ORDER_DETAILS OD
        JOIN    FRP.STATUS_MAPPING S
        ON      OD.STATUS = S.STATUS
        LEFT OUTER JOIN FTD_APPS.PRODUCT_MASTER PM
        ON      OD.PRODUCT_ID = PM.PRODUCT_ID
        OR      OD.PRODUCT_ID = PM.NOVATOR_ID
        LEFT OUTER JOIN FTD_APPS.FLORIST_MASTER FM
        ON      OD.FLORIST_NUMBER = FM.FLORIST_ID
        LEFT OUTER JOIN FTD_APPS.COLOR_MASTER CM1
        ON      OD.COLOR_1 = CM1.COLOR_MASTER_ID
        LEFT OUTER JOIN FTD_APPS.COLOR_MASTER CM2
        ON      OD.COLOR_2 = CM2.COLOR_MASTER_ID
        LEFT OUTER JOIN FTD_APPS.PRODUCT_SUBCODES PS
        ON      OD.SUBCODE = PS.PRODUCT_SUBCODE_ID
        LEFT OUTER JOIN FTD_APPS.SOURCE FS
        ON      OD.SOURCE_CODE = FS.SOURCE_CODE
        LEFT OUTER JOIN FTD_APPS.OCCASION OC
        ON      OD.OCCASION_ID = OC.OCCASION_ID
        LEFT OUTER JOIN 
            (select * from FTD_APPS.SOURCE_PROGRAM_REF SPR
            where source_program_ref_id = 
                (select max(source_program_ref_id)
                from ftd_apps.source_program_ref spr1
                where spr.source_code = spr1.source_code)) spr
        on      od.source_code = spr.source_code
        LEFT OUTER JOIN FTD_APPS.PARTNER_PROGRAM PP
        ON      SPR.PROGRAM_NAME = PP.PROGRAM_NAME
        LEFT OUTER JOIN FTD_APPS.PARTNER_MASTER PRTM
        ON      PP.PARTNER_NAME = PRTM.PARTNER_NAME
        WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

END VIEW_ORDER_DETAILS;


PROCEDURE VIEW_CO_BRAND
(
IN_ORDER_GUID           IN CO_BRAND.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning co brand information for the
        given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in co brand
        co_brand_id                     number
        order_guid                      varchar2
        info_name                       varchar2
        info_data                       varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  CO_BRAND_ID,
                ORDER_GUID,
                INFO_NAME,
                INFO_DATA
        FROM    CO_BRAND
        WHERE   ORDER_GUID = IN_ORDER_GUID;

END VIEW_CO_BRAND;


PROCEDURE VIEW_ORDER_DETAIL_EXT
(
IN_ORDER_GUID           IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order extension information
        for the given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in order_detail_extensions
        extension_id                    number
        order_detail_id                 number
        info_name                       varchar2
        info_data                       varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ODE.EXTENSION_ID,
                ODE.ORDER_DETAIL_ID,
                ODE.INFO_NAME,
                ODE.INFO_DATA
        FROM    SCRUB.ORDER_DETAIL_EXTENSIONS ODE
        JOIN    ORDER_DETAILS OD
        ON      ODE.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID
        WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

END VIEW_ORDER_DETAIL_EXT;


PROCEDURE VIEW_ADD_ONS
(
IN_ORDER_GUID           IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning add on information for the
        given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in add on
        add_on_id                       number
        order_detail_id                 number
        add_on_code                     varchar2
        add_on_quantity                 varchar2
        addon_type                      varchar2
        description                     varchar2
        price                           float
        add_on_history_id               number
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  A.ADD_ON_ID,
                A.ORDER_DETAIL_ID,
                A.ADD_ON_CODE,
                A.ADD_ON_QUANTITY,
                FA.ADDON_TYPE,
                FA.DESCRIPTION,
                A.ADD_ON_PRICE AS PRICE,
                A.ADD_ON_HISTORY_ID,
                a.add_on_discount_amount
        FROM    ADD_ONS A
        JOIN    ORDER_DETAILS OD
        ON      A.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID
        LEFT OUTER JOIN FTD_APPS.ADDON FA
        ON      A.ADD_ON_CODE = FA.ADDON_ID
        WHERE   OD.ORDER_GUID = IN_ORDER_GUID
        ORDER BY DECODE (FA.ADDON_TYPE,7,1,2), DECODE (FA.ADDON_TYPE,4,'Greeting Card',FA.DESCRIPTION);

END VIEW_ADD_ONS;


PROCEDURE VIEW_PAYMENTS
(
IN_ORDER_GUID           IN PAYMENTS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning payment information for the
        given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in payments
        payment_id                      number
        order_guid                      varchar2
        payment_type                    varchar2
        amount                          varchar2
        credit_card_id                  number
        gift_certificate_id             varchar2
        auth_result                     varchar2
        auth_number                     varchar2
        avs_code                        varchar2
        acq_reference_number            varchar2
        aafes_ticket                    varchar2
        invoice_number                  varchar2
        ap_account_txt                  varchar2
        ap_account_id			number
        ap_auth_txt                     varchar2
        orig_auth_amount_txt            varchar2
	csc_response_code		varchar2
	csc_validated_flag		varchar2
	csc_failure_cnt			number
	wallet indicator		varchar2
		cc_auth_provider				varchar2
		cardinal_verified_flag			varchar2
		route							varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  P.PAYMENT_ID,
                P.ORDER_GUID,
                P.PAYMENT_TYPE,
                P.AMOUNT,
                P.CREDIT_CARD_ID,
                P.GIFT_CERTIFICATE_ID,
                P.AUTH_RESULT,
                P.AUTH_NUMBER,
                P.AVS_CODE,
                P.ACQ_REFERENCE_NUMBER,
                P.AAFES_TICKET,
                P.INVOICE_NUMBER,
                FPM.PAYMENT_TYPE AS PAYMENT_METHOD_TYPE,
                P.AP_ACCOUNT_ID,
                DECODE(P.AP_ACCOUNT_ID, NULL, NULL, GLOBAL.ENCRYPTION.DECRYPT_IT(AA.AP_ACCOUNT_TXT,AA.KEY_NAME)) AP_ACCOUNT_TXT,
                P.AP_AUTH_TXT,
                P.ORIG_AUTH_AMOUNT_TXT,
                P.INITIAL_AUTH_AMOUNT,
                FPM.DESCRIPTION,
                P.MILES_POINTS_AMT,
                P.CSC_RESPONSE_CODE,
                P.CSC_VALIDATED_FLAG,
                P.CSC_FAILURE_CNT,
                P.WALLET_INDICATOR,
                P.CC_AUTH_PROVIDER,
                P.CARDINAL_VERIFIED_FLAG,
                P.ROUTE,
                P.TOKEN_ID,
                P.AUTHORIZATION_TRANSACTION_ID
        FROM    PAYMENTS P
        LEFT OUTER JOIN FTD_APPS.PAYMENT_METHODS FPM
        ON      P.PAYMENT_TYPE = FPM.PAYMENT_METHOD_ID
        LEFT OUTER JOIN AP_ACCOUNTS AA
        ON	P.AP_ACCOUNT_ID = AA.AP_ACCOUNT_ID
        WHERE   ORDER_GUID = IN_ORDER_GUID;

END VIEW_PAYMENTS;


PROCEDURE VIEW_CREDIT_CARDS
(
IN_ORDER_GUID           IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning credit card information
        for the given guid. Calls GLOBAL.ENCRYPTION.DECRYPT_IT to decrypt the
        credit card number.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in credit cards
        credit_card_id                  number
        buyer_id                        number
        cc_type                         varchar2
        cc_number                       varchar2
        cc_expiration                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  CC.CREDIT_CARD_ID,
                CC.BUYER_ID,
                CC.CC_TYPE,
                GLOBAL.ENCRYPTION.DECRYPT_IT (CC.CC_NUMBER,cc.key_name) AS CC_NUMBER,
                CC.CC_EXPIRATION,
                GLOBAL.ENCRYPTION.DECRYPT_IT (CC.GIFT_CARD_PIN,cc.key_name) AS GIFT_CARD_PIN,
                CC.CC_ENCRYPTED_FLAG
        FROM    CREDIT_CARDS CC
        JOIN    PAYMENTS P
        ON      CC.CREDIT_CARD_ID = P.CREDIT_CARD_ID
        WHERE   P.ORDER_GUID = IN_ORDER_GUID;

END VIEW_CREDIT_CARDS;


PROCEDURE VIEW_ORDER_CONTACT_INFO
(
IN_ORDER_GUID           IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning contact information
        for the given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in order contact info
        order_contact_info_id           number
        order_guid                      varchar2
        first_name                      varchar2
        last_name                       varchar2
        phone                           varchar2
        ext                             varchar2
        email                           varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ORDER_CONTACT_INFO_ID,
                ORDER_GUID,
                FIRST_NAME,
                LAST_NAME,
                PHONE,
                EXT,
                EMAIL
        FROM    ORDER_CONTACT_INFO
        WHERE   ORDER_GUID = IN_ORDER_GUID;

END VIEW_ORDER_CONTACT_INFO;



PROCEDURE VIEW_ORDER_DISPOSITION
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order disposition information
        for the given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in order disposition
        order_disposition_id            number
        order_detail_id                 number
        disposition_id                  char
        comments                        varchar2
        called_customer_flag            char
        sent_email_flag                 char
        stock_message_id                varchar2
        email_subject                   varchar2
        email_message                   varchar2
        csr_id                          varchar2
        comment_date                    date
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISP.ORDER_DISPOSITION_ID,
                DISP.ORDER_DETAIL_ID,
                DISP.DISPOSITION_ID,
                DISP.COMMENTS,
                DISP.CALLED_CUSTOMER_FLAG,
                DISP.SENT_EMAIL_FLAG,
                DISP.STOCK_MESSAGE_ID,
                DISP.EMAIL_SUBJECT,
                DISP.EMAIL_MESSAGE,
                DISP.CSR_ID,
                DISP.COMMENT_DATE,
                CODE.DISPOSITION_DESCRIPTION
        FROM    ORDER_DISPOSITION DISP
        JOIN    ORDER_DETAILS OD
        ON      DISP.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID
        JOIN    DISPOSITION_CODES CODE
        ON      DISP.DISPOSITION_ID = CODE.DISPOSITION_ID
        WHERE   OD.ORDER_GUID = IN_ORDER_GUID;

END VIEW_ORDER_DISPOSITION;


PROCEDURE VIEW_ORDER_DISPOSITION
(
IN_ORDER_GUID           IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_LINE_NUMBER          IN ORDER_DETAILS.LINE_NUMBER%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning certain order disposition
        information for the given order GUID and line number.

Input:
        order_guid                      varchar2
        line_number                     number

Output:
        cursor result set containing fields in order disposition
        order_disposition_id            number
        disposition_id                  char
        disposition_description         varchar2
        comments                        varchar2
        csr_id                          varchar2
        comment_date                    varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISP.ORDER_DISPOSITION_ID,
                DISP.DISPOSITION_ID,
                CODE.DISPOSITION_DESCRIPTION,
                DISP.COMMENTS,
                DISP.CSR_ID,
                TO_CHAR (DISP.COMMENT_DATE, 'MM/DD/YYYY') COMMENT_DATE
        FROM    ORDER_DISPOSITION DISP
        JOIN    DISPOSITION_CODES CODE
        ON      DISP.DISPOSITION_ID = CODE.DISPOSITION_ID
        JOIN    ORDER_DETAILS DETL
        ON      DETL.ORDER_DETAIL_ID = DISP.ORDER_DETAIL_ID
        WHERE   DETL.ORDER_GUID = IN_ORDER_GUID
        AND     DETL.LINE_NUMBER = IN_LINE_NUMBER;

END VIEW_ORDER_DISPOSITION;


PROCEDURE VIEW_FRAUD_COMMENTS
(
IN_ORDER_GUID           IN FRAUD_COMMENTS.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning fraud comments
        for the given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in fraud comments
        order_guid                      varchar2
        comment_id                      number
        comment_text                    varchar2
        created_on                      date
        updated_on                      date
        updated_by                      varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  FC.FRAUD_COMMENT_ID,
                FC.ORDER_GUID,
                FC.FRAUD_ID,
                FC.COMMENT_TEXT,
                FOD.FRAUD_DESCRIPTION
        FROM    FRAUD_COMMENTS FC
        JOIN    FRAUD_ORDER_DISPOSITIONS FOD
        ON      FC.FRAUD_ID = FOD.FRAUD_ID
        WHERE   ORDER_GUID = IN_ORDER_GUID;

END VIEW_FRAUD_COMMENTS;


PROCEDURE VIEW_NOVATOR_FRAUD_CODES
(
IN_ORDER_GUID           IN NOVATOR_FRAUD_CODES.ORDER_GUID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning fraud codes
        for the given guid.

Input:
        order_guid                      varchar2

Output:
        cursor result set containing all fields in novator fraud codes
        order_guid                      varchar2
        novator_fraud_code              varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ORDER_GUID,
                NOVATOR_FRAUD_CODE
        FROM    NOVATOR_FRAUD_CODES
        WHERE   ORDER_GUID = IN_ORDER_GUID;

END VIEW_NOVATOR_FRAUD_CODES;

PROCEDURE VIEW_ORDER_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_ORDER_CUR                   OUT TYPES.REF_CURSOR,
OUT_ORDER_DETAIL_CUR            OUT TYPES.REF_CURSOR,
OUT_CO_BRAND_CUR                OUT TYPES.REF_CURSOR,
OUT_ADD_ON_CUR                  OUT TYPES.REF_CURSOR,
OUT_PAYMENT_CUR                 OUT TYPES.REF_CURSOR,
OUT_CREDIT_CARD_CUR             OUT TYPES.REF_CURSOR,
OUT_BUYER_CUR                   OUT TYPES.REF_CURSOR,
OUT_BUYER_ADDRESS_CUR           OUT TYPES.REF_CURSOR,
OUT_BUYER_PHONE_CUR             OUT TYPES.REF_CURSOR,
OUT_BUYER_EMAIL_CUR             OUT TYPES.REF_CURSOR,
OUT_BUYER_COMMENTS_CUR          OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR              OUT TYPES.REF_CURSOR,
OUT_NCOA_CUR                    OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_CUR               OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_ADDRESS_CUR       OUT TYPES.REF_CURSOR,
OUT_QMS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_PHONE_CUR         OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_COMMENTS_CUR      OUT TYPES.REF_CURSOR,
OUT_ORDER_CONTACT_CUR           OUT TYPES.REF_CURSOR,
OUT_ORDER_DISPOSITION_CUR       OUT TYPES.REF_CURSOR,
OUT_FRAUD_COMMENT_CUR           OUT TYPES.REF_CURSOR,
OUT_NOVATOR_FRAUD_CODE_CUR      OUT TYPES.REF_CURSOR,
OUT_ORDER_EXT_CUR               OUT TYPES.REF_CURSOR,
OUT_AVS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_AVS_ADDRESS_SCORE_CUR       OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order information for the
        given guid, including the order detail extensions and AVS address info.

Input:
        order_guid                      varchar2

Output:
        order cursor
        order detail cursor
        co brand cursor
        add on cursor
        payment cursor
        credit card cursor
        buyer cursor
        buyer address cursor
        buyer phone cursor
        buyer email cursor
        cuyer comments cur
        membership cursor
        ncoa cursor
        recipient cursor
        recipient address cursor
        qms address cursor
        recipient phone cursor
        recipient comments cursor
        order contact cursor
        order disposition cursor
        fraud comment cursor
        novator fraud code cursor
        order detail extensions cursor
        avs address cursor
        avs address score cursor
-----------------------------------------------------------------------------*/

TEMP_CUR        TYPES.REF_CURSOR;

BEGIN

VIEW_ORDERS (IN_ORDER_GUID, OUT_ORDER_CUR);
VIEW_ORDER_DETAILS (IN_ORDER_GUID, OUT_ORDER_DETAIL_CUR);
VIEW_CO_BRAND (IN_ORDER_GUID, OUT_CO_BRAND_CUR);
VIEW_ADD_ONS (IN_ORDER_GUID, OUT_ADD_ON_CUR);
VIEW_PAYMENTS (IN_ORDER_GUID, OUT_PAYMENT_CUR);
VIEW_CREDIT_CARDS (IN_ORDER_GUID, OUT_CREDIT_CARD_CUR);
VIEW_ORDER_CONTACT_INFO (IN_ORDER_GUID, OUT_ORDER_CONTACT_CUR);
VIEW_ORDER_DISPOSITION (IN_ORDER_GUID, OUT_ORDER_DISPOSITION_CUR);
VIEW_FRAUD_COMMENTS (IN_ORDER_GUID, OUT_FRAUD_COMMENT_CUR);
VIEW_NOVATOR_FRAUD_CODES (IN_ORDER_GUID, OUT_NOVATOR_FRAUD_CODE_CUR);
VIEW_ORDER_DETAIL_EXT (IN_ORDER_GUID, OUT_ORDER_EXT_CUR);

BUYERS_QUERY_PKG.VIEW_ORDER_BUYER_INFO
(
        IN_ORDER_GUID,
        OUT_BUYER_CUR,
        OUT_BUYER_ADDRESS_CUR,
        OUT_BUYER_PHONE_CUR,
        OUT_BUYER_EMAIL_CUR,
        OUT_BUYER_COMMENTS_CUR,
        OUT_MEMBERSHIP_CUR,
        OUT_NCOA_CUR
);

RECIPIENTS_QUERY_PKG.VIEW_ORDER_RECIPIENT_INFO
(
        IN_ORDER_GUID,
        OUT_RECIPIENT_CUR,
        OUT_RECIPIENT_ADDRESS_CUR,
        OUT_QMS_ADDRESS_CUR,
        OUT_RECIPIENT_PHONE_CUR,
        OUT_RECIPIENT_COMMENTS_CUR,
        OUT_AVS_ADDRESS_CUR,
        OUT_AVS_ADDRESS_SCORE_CUR
);

END VIEW_ORDER_INFO;


PROCEDURE VIEW_ORDER_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_ORDER_CUR                   OUT TYPES.REF_CURSOR,
OUT_ORDER_DETAIL_CUR            OUT TYPES.REF_CURSOR,
OUT_CO_BRAND_CUR                OUT TYPES.REF_CURSOR,
OUT_ADD_ON_CUR                  OUT TYPES.REF_CURSOR,
OUT_PAYMENT_CUR                 OUT TYPES.REF_CURSOR,
OUT_CREDIT_CARD_CUR             OUT TYPES.REF_CURSOR,
OUT_BUYER_CUR                   OUT TYPES.REF_CURSOR,
OUT_BUYER_ADDRESS_CUR           OUT TYPES.REF_CURSOR,
OUT_BUYER_PHONE_CUR             OUT TYPES.REF_CURSOR,
OUT_BUYER_EMAIL_CUR             OUT TYPES.REF_CURSOR,
OUT_BUYER_COMMENTS_CUR          OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR              OUT TYPES.REF_CURSOR,
OUT_NCOA_CUR                    OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_CUR               OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_ADDRESS_CUR       OUT TYPES.REF_CURSOR,
OUT_QMS_ADDRESS_CUR             OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_PHONE_CUR         OUT TYPES.REF_CURSOR,
OUT_RECIPIENT_COMMENTS_CUR      OUT TYPES.REF_CURSOR,
OUT_ORDER_CONTACT_CUR           OUT TYPES.REF_CURSOR,
OUT_ORDER_DISPOSITION_CUR       OUT TYPES.REF_CURSOR,
OUT_FRAUD_COMMENT_CUR           OUT TYPES.REF_CURSOR,
OUT_NOVATOR_FRAUD_CODE_CUR      OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order information for the
        given guid.

Input:
        order_guid                      varchar2

Output:
        order cursor
        order detail cursor
        co brand cursor
        add on cursor
        payment cursor
        credit card cursor
        buyer cursor
        buyer address cursor
        buyer phone cursor
        buyer email cursor
        cuyer comments cur
        membership cursor
        ncoa cursor
        recipient cursor
        recipient address cursor
        qms address cursor
        recipient phone cursor
        recipient comments cursor
        order contact cursor
        order disposition cursor
        fraud comment cursor
        novator fraud code cursor
-----------------------------------------------------------------------------*/

TEMP_CUR        TYPES.REF_CURSOR;

BEGIN

VIEW_ORDERS (IN_ORDER_GUID, OUT_ORDER_CUR);
VIEW_ORDER_DETAILS (IN_ORDER_GUID, OUT_ORDER_DETAIL_CUR);
VIEW_CO_BRAND (IN_ORDER_GUID, OUT_CO_BRAND_CUR);
VIEW_ADD_ONS (IN_ORDER_GUID, OUT_ADD_ON_CUR);
VIEW_PAYMENTS (IN_ORDER_GUID, OUT_PAYMENT_CUR);
VIEW_CREDIT_CARDS (IN_ORDER_GUID, OUT_CREDIT_CARD_CUR);
VIEW_ORDER_CONTACT_INFO (IN_ORDER_GUID, OUT_ORDER_CONTACT_CUR);
VIEW_ORDER_DISPOSITION (IN_ORDER_GUID, OUT_ORDER_DISPOSITION_CUR);
VIEW_FRAUD_COMMENTS (IN_ORDER_GUID, OUT_FRAUD_COMMENT_CUR);
VIEW_NOVATOR_FRAUD_CODES (IN_ORDER_GUID, OUT_NOVATOR_FRAUD_CODE_CUR);

BUYERS_QUERY_PKG.VIEW_ORDER_BUYER_INFO
(
        IN_ORDER_GUID,
        OUT_BUYER_CUR,
        OUT_BUYER_ADDRESS_CUR,
        OUT_BUYER_PHONE_CUR,
        OUT_BUYER_EMAIL_CUR,
        OUT_BUYER_COMMENTS_CUR,
        OUT_MEMBERSHIP_CUR,
        OUT_NCOA_CUR
);

RECIPIENTS_QUERY_PKG.VIEW_ORDER_RECIPIENT_INFO
(
        IN_ORDER_GUID,
        OUT_RECIPIENT_CUR,
        OUT_RECIPIENT_ADDRESS_CUR,
        OUT_QMS_ADDRESS_CUR,
        OUT_RECIPIENT_PHONE_CUR,
        OUT_RECIPIENT_COMMENTS_CUR
);

END VIEW_ORDER_INFO;


FUNCTION GET_ORDER_COUNT
RETURN NUMBER
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the number of orders
        with the status of 1002 - invalid_header or 1003 - valid header invalid_item.

Input:
        none

Output:
        number
-----------------------------------------------------------------------------*/

V_COUNT         NUMBER := 0;

BEGIN

SELECT  /*+ RULE */
        COUNT (1)
INTO    V_COUNT
FROM    ORDERS
WHERE   STATUS <> '1005'
AND     ORDER_GUID IN (SELECT DISTINCT ORDER_GUID FROM ORDER_DETAILS WHERE STATUS IN ('2002', '2003'));

RETURN V_COUNT;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_ORDER_COUNT;


FUNCTION GET_PENDING_ORDER_COUNT
RETURN NUMBER
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the number of orders
        with the status of 1007 - pending order.

Input:
        none

Output:
        number
-----------------------------------------------------------------------------*/

V_COUNT         NUMBER := 0;

BEGIN

SELECT  /*+ RULE */  COUNT (1)
INTO    V_COUNT
FROM    ORDERS
WHERE   (STATUS <> '1005' OR STATUS IS NULL)
AND     ORDER_GUID IN (SELECT DISTINCT ORDER_GUID FROM ORDER_DETAILS WHERE STATUS = '2005');

RETURN V_COUNT;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_PENDING_ORDER_COUNT;


PROCEDURE VIEW_REINSTATE_ORDERS
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the orders with a status
        of 1013.

Input:
        none

Output:
        cursor result set containing the orders to reinstate.

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ORDER_GUID,
                MASTER_ORDER_NUMBER,
                UPDATED_ON,
                JMS_FAIL,
                PREVIOUS_STATUS
        FROM    ORDERS
        WHERE   STATUS = '1013'
        ORDER BY UPDATED_ON DESC;

END VIEW_REINSTATE_ORDERS;



FUNCTION GET_PREVIOUS_STATUS
(
IN_ORDER_GUID           IN  ORDERS.ORDER_GUID%TYPE
)
RETURN ORDERS.PREVIOUS_STATUS%TYPE

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the previous status for
        the given order.

Input:
        order_guid                      varchar2

Output:
        previous_status                 varchar2

-----------------------------------------------------------------------------*/

V_PREVIOUS_STATUS       ORDERS.PREVIOUS_STATUS%TYPE;

BEGIN

SELECT  PREVIOUS_STATUS
INTO    V_PREVIOUS_STATUS
FROM    ORDERS
WHERE   ORDER_GUID = IN_ORDER_GUID;

RETURN V_PREVIOUS_STATUS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_PREVIOUS_STATUS;


FUNCTION GET_FIRST_SCRUB_ORDER
(
IN_CONFIRMATION_NUMBER          IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
IN_ORDER_ORIGIN                 IN ORDERS.ORDER_ORIGIN%TYPE,
IN_BUYERS_LAST_NAME             IN BUYERS.LAST_NAME%TYPE,
IN_BUYERS_PHONE_NUMBER          IN BUYER_PHONES.PHONE_NUMBER%TYPE,
IN_EMAIL                        IN BUYER_EMAILS.EMAIL%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_RECIPIENTS_LAST_NAME         IN RECIPIENTS.LAST_NAME%TYPE,
IN_RECIPIENTS_PHONE_NUMBER      IN RECIPIENT_PHONES.PHONE_NUMBER%TYPE,
IN_DESTINATION_TYPE             IN RECIPIENT_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_CSR_ID_SEARCH                IN ORDERS.CSR_ID%TYPE,
IN_FROM_DATE                    IN ORDERS.ORDER_DATE%TYPE,
IN_TO_DATE                      IN ORDERS.ORDER_DATE%TYPE,
IN_STATUS_FLAG                  IN VARCHAR2,
IN_CSR_ID_QUERY                 IN ORDERS.CSR_ID%TYPE,
IN_ORDER_BY_IND                 IN VARCHAR2,
IN_NEW_SEARCH_FLAG              IN VARCHAR2,
IN_SOURCE_CODE			IN ORDERS.SOURCE_CODE%TYPE,
IN_SCRUB_REASONS	        IN VARCHAR2,
IN_PRODUCT_PROPERTY		IN VARCHAR2,
IN_PREFERRED_PARTNER            IN VARCHAR2,
IN_PC_MEMBERSHIP_ID             IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE
)
RETURN ORDERS.ORDER_GUID%TYPE

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for finding the first order available
        to be scrubbed based on the input parms, lock it, and return
        the guid.

Input:
        confirmation_number             varchar2
        order_origin                    varchar2
        buyers_last_number              varchar2
        buyers_phone_number             varchar2
        email                           varchar2
        delivery_date                   varchar2
        ship_date                       varchar2
        product_id                      varchar2
        recipients_last_name            varchar2
        recipients_phone_number         varchar2
        destination_type                varchar2
        csr_id_search                   varchar2
        end_date                        varchar2
        status_flag                     varchar2
        csr_id_query                    varchar2
        order_by_in                     varchar2
        new_search_flag                 varchar2
        source_code			varchar2
        scrub_reason			varchar2
        product_property		varchar2
        preferred_partner               varchar2

Output:
        order_guid                      varchar2

-----------------------------------------------------------------------------*/

V_OUT_STATUS            VARCHAR2 (1);
V_OUT_MESSAGE           VARCHAR2 (1000);

V_ORDER_CUR             TYPES.REF_CURSOR;

V_ORDER_GUID            ORDERS.ORDER_GUID%TYPE;
V_ORDER_ORIGIN          ORDERS.ORDER_ORIGIN%TYPE;
V_STATUS                ORDERS.STATUS%TYPE;
V_CSR_ID                ORDERS.CSR_ID%TYPE;
V_MASTER_ORDER_NUMBER   ORDERS.MASTER_ORDER_NUMBER%TYPE;
V_ORDER_DATE            ORDERS.ORDER_DATE%TYPE;
V_PRODUCTS_TOTAL        ORDERS.PRODUCTS_TOTAL%TYPE;
V_ORDER_TOTAL           ORDERS.ORDER_TOTAL%TYPE;
V_LAST_NAME             BUYERS.LAST_NAME%TYPE;
V_FIRST_NAME            BUYERS.FIRST_NAME%TYPE;
V_CITY                  BUYER_ADDRESSES.CITY%TYPE;
V_STATE_PROVINCE        BUYER_ADDRESSES.STATE_PROVINCE%TYPE;
V_PREFERRED_PARTNER     FTD_APPS.PARTNER_MASTER.PARTNER_NAME%TYPE;

BEGIN

IF IN_NEW_SEARCH_FLAG = 'Y' THEN
        MISC_PKG.RESET_CSR_VIEWED_ORDERS (IN_CSR_ID_QUERY, V_OUT_STATUS, V_OUT_MESSAGE);
END IF;

IF V_OUT_STATUS = 'N' THEN
        RETURN NULL;
        --RETURN V_OUT_MESSAGE;                 -- for debugging
ELSE
        V_OUT_STATUS := NULL;                   -- reset the out status before trying locking
END IF;

-- get the orders available for scrub based on the search criteria
SEARCH_FOR_ORDERS (
        IN_CONFIRMATION_NUMBER,
        IN_ORDER_ORIGIN,
        IN_BUYERS_LAST_NAME,
        IN_BUYERS_PHONE_NUMBER,
        IN_EMAIL,
        IN_DELIVERY_DATE,
        IN_SHIP_DATE,
        IN_PRODUCT_ID,
        IN_RECIPIENTS_LAST_NAME,
        IN_RECIPIENTS_PHONE_NUMBER,
        IN_DESTINATION_TYPE,
        IN_CSR_ID_SEARCH,
        IN_FROM_DATE,
        IN_TO_DATE,
        IN_STATUS_FLAG,
        IN_CSR_ID_QUERY,
        IN_ORDER_BY_IND,
        'Y',                    -- exclude viewed orders
        IN_SOURCE_CODE,
        IN_SCRUB_REASONS,
        IN_PRODUCT_PROPERTY,
        IN_PREFERRED_PARTNER,
        'N',
		IN_PC_MEMBERSHIP_ID,
        V_ORDER_CUR
);

-- loop through the search results to find the first order that can be scrubbed
LOOP
        IF IN_STATUS_FLAG IS NOT NULL THEN
                FETCH V_ORDER_CUR INTO
                        V_ORDER_GUID,
                        V_ORDER_ORIGIN,
                        V_STATUS,
                        V_CSR_ID,
                        V_MASTER_ORDER_NUMBER,
                        V_ORDER_DATE,
                        V_PRODUCTS_TOTAL,
                        V_ORDER_TOTAL,
                        V_LAST_NAME,
                        V_FIRST_NAME,
                        V_CITY,
                        V_STATE_PROVINCE,
                        V_PREFERRED_PARTNER;
        ELSE
                FETCH V_ORDER_CUR INTO
                        V_ORDER_GUID;
        END IF;

        -- exit the loop when no more records are found in the cursor
        EXIT WHEN V_ORDER_CUR%NOTFOUND;

        -- if an order is found, try to lock it to the scrub status
        ORDERS_MAINT_PKG.UPDATE_ORDER_TO_SCRUB_STATUS (V_ORDER_GUID, NULL, V_OUT_STATUS, V_OUT_MESSAGE);

        -- if the lock is successful or if an error occurred the exit
        IF V_OUT_STATUS = 'Y' OR
           SUBSTR (V_OUT_MESSAGE, 1, 14) = 'ERROR OCCURRED' THEN
                EXIT;
        END IF;

END LOOP;

CLOSE V_ORDER_CUR;

-- if no records are returned by the search or if records cannot be locked, null is returned
IF V_OUT_STATUS = 'Y' AND V_ORDER_GUID IS NOT NULL THEN
        MISC_PKG.INSERT_CSR_VIEWED_ORDERS (IN_CSR_ID_QUERY, V_ORDER_GUID, V_OUT_STATUS, V_OUT_MESSAGE);
        RETURN V_ORDER_GUID;
ELSE
        RETURN NULL;
        --RETURN V_OUT_MESSAGE;                                                        -- for debugging
END IF;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
        --RETURN 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);      -- for debugging

END GET_FIRST_SCRUB_ORDER;



PROCEDURE VIEW_BULK_ORDERS
(
IN_ORDER_DATE           IN  DATE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning bulk order information for
        all bulk orders that were created after the given date.

Input:
        order_date                      date

Output:
        cursor result set containing order, buyer info and order detail counts
        order guid                      varchar2
        created on                      date
        master order number             varchar2
        status                          varchar2
        first name                      varchar2
        last name                       varchar2
        address etc                     varchar2
        submitted lines total           number
        dispatched total                number
        processed total                 number
        remove pending total            number

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  O.ORDER_GUID,
                O.ORDER_DATE,
                O.MASTER_ORDER_NUMBER,
                O.SOURCE_CODE,
                O.STATUS,
                B.FIRST_NAME,
                B.LAST_NAME,
                BA.ADDRESS_ETC,
                COUNT (OD.STATUS) AS SUBMITTED_LINES_TOTAL,
                COUNT (DECODE (OD.STATUS, '2006', 1, '2007', 1, NULL)) AS PROCESS_TOTAL,
                COUNT (DECODE (OD.STATUS, '2002', 1, '2003', 1, NULL)) AS SCRUB_TOTAL,
                COUNT (DECODE (OD.STATUS, '2004', 1, '2005', 1, '2008', 1, '2009', 1, NULL)) AS REMOVE_PENDING_TOTAL,
                COUNT (DECODE (OD.STATUS, '2001', 1, NULL)) AS RECEIVE_TOTAL,
                COUNT (DECODE (OD.STATUS, '2011', 1, NULL)) AS ABANDON_TOTAL
        FROM    ORDERS O
        JOIN    BUYERS B
        ON      O.BUYER_ID = B.BUYER_ID
        JOIN    BUYER_ADDRESSES BA
        ON      BA.BUYER_ID = B.BUYER_ID
        JOIN    ORDER_DETAILS OD
        ON      O.ORDER_GUID = OD.ORDER_GUID
        WHERE   O.ORDER_ORIGIN = 'bulk'
        AND     O.ORDER_DATE LIKE '%_:__:__ ___ ____'
        AND     TO_DATE (SUBSTR (O.ORDER_DATE, 5, LENGTH (O.ORDER_DATE) - 12) || SUBSTR (O.ORDER_DATE, -5), 'MON DD HH24:MI:SS YYYY') > IN_ORDER_DATE
        AND     O.STATUS <> '1009'
        GROUP BY O.ORDER_GUID,
                O.ORDER_DATE,
                O.MASTER_ORDER_NUMBER,
                O.SOURCE_CODE,
                O.STATUS,
                B.FIRST_NAME,
                B.LAST_NAME,
                BA.ADDRESS_ETC
        ORDER BY TO_DATE (SUBSTR (O.ORDER_DATE, 5, LENGTH (O.ORDER_DATE) - 12) || SUBSTR (O.ORDER_DATE, -5), 'MON DD HH24:MI:SS YYYY') DESC;

END VIEW_BULK_ORDERS;


PROCEDURE VIEW_DASHBOARD_COUNTS
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning counts of orders by origin
        and status (unscrubbed - 1002 or 1003, pending - 1007, removed - 1006).

Input:
        none

Output:
        cursor result set containing order counts
        order_origin                    varchar2
        unscrubbed_count                number
        unscrub_ship_today_count        number
        pending_count                   number
        pending_ship_today_count        number
        removed_count                   number
        removed_ship_today_count        number

-----------------------------------------------------------------------------*/

TODAY_CHAR      VARCHAR2(10) := TO_CHAR (TRUNC (SYSDATE), 'MM/DD/YYYY');
TODAY           DATE := TRUNC (SYSDATE);
TOMORROW        DATE := TRUNC (SYSDATE + 1) - 1/86400;

BEGIN


delete from scrub.dashboard_query_counts_temp;

insert into scrub.dashboard_query_counts_temp
      SELECT /*+RULE*/ DISTINCT
             od.status,
             od.order_guid,
             od.ship_date,
             od.delivery_date,
             od.updated_on,
             od.order_detail_id,
             DECODE(PRTM.PREFERRED_PARTNER_FLAG, null, null, 'N', null, PRTM.DISPLAY_NAME) AS PREFERRED_PARTNER_NAME
        FROM    SCRUB.ORDER_DETAILS OD
        LEFT OUTER JOIN FTD_APPS.SOURCE_PROGRAM_REF SPR ON SPR.SOURCE_CODE = OD.SOURCE_CODE
        LEFT OUTER JOIN FTD_APPS.PARTNER_PROGRAM PP ON PP.PROGRAM_NAME = SPR.PROGRAM_NAME
        LEFT OUTER JOIN FTD_APPS.PARTNER_MASTER PRTM ON PRTM.PARTNER_NAME = PP.PARTNER_NAME
        WHERE   OD.STATUS IN ('2002', '2003', '2004', '2005', '2008')
        AND     NOT (od.status in ('2004','2008') and  od.updated_on NOT BETWEEN TODAY and TOMORROW);

OPEN OUT_CUR FOR
        SELECT  /*+ RULE */
                'ALL' ORDER_ORIGIN,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') THEN OD.ORDER_GUID END) AS UNSCRUBBED_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') AND (OD.SHIP_DATE = TODAY_CHAR OR OD.DELIVERY_DATE = TODAY_CHAR) THEN OD.ORDER_GUID END) AS UNSCRUB_SHIP_TODAY_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' THEN OD.ORDER_GUID END) AS PENDING_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' AND (OD.SHIP_DATE = TODAY_CHAR OR OD.DELIVERY_DATE = TODAY_CHAR) THEN OD.ORDER_GUID END) AS PENDING_SHIP_TODAY_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2004', '2008') AND OD.UPDATED_ON BETWEEN TODAY AND TOMORROW THEN OD.ORDER_GUID END) AS REMOVED_COUNT
        FROM    SCRUB.ORDER_DETAILS OD
        WHERE   OD.STATUS IN ('2002', '2003', '2004', '2005', '2008')
        UNION
        SELECT  OD.PREFERRED_PARTNER_NAME ORDER_ORIGIN,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') THEN OD.ORDER_GUID END) AS UNSCRUBBED_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') AND (OD.SHIP_DATE = TODAY_CHAR OR OD.DELIVERY_DATE = TODAY_CHAR) THEN OD.ORDER_GUID END) AS UNSCRUB_SHIP_TODAY_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' THEN OD.ORDER_GUID END) AS PENDING_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' AND (OD.SHIP_DATE = TODAY_CHAR OR OD.DELIVERY_DATE = TODAY_CHAR) THEN OD.ORDER_GUID END) AS PENDING_SHIP_TODAY_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2004', '2008') AND OD.UPDATED_ON BETWEEN TODAY AND TOMORROW THEN OD.ORDER_GUID END) AS REMOVED_COUNT
        FROM   scrub.dashboard_query_counts_temp OD
        WHERE   OD.PREFERRED_PARTNER_NAME IS NOT NULL
        GROUP BY OD.PREFERRED_PARTNER_NAME
        UNION
        SELECT  O.ORDER_ORIGIN,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') THEN OD.ORDER_GUID END) AS UNSCRUBBED_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2002', '2003') AND (OD.SHIP_DATE = TODAY_CHAR OR OD.DELIVERY_DATE = TODAY_CHAR) THEN OD.ORDER_GUID END) AS UNSCRUB_SHIP_TODAY_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' THEN OD.ORDER_GUID END) AS PENDING_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS = '2005' AND (OD.SHIP_DATE = TODAY_CHAR OR OD.DELIVERY_DATE = TODAY_CHAR) THEN OD.ORDER_GUID END) AS PENDING_SHIP_TODAY_COUNT,
                COUNT (DISTINCT CASE WHEN OD.STATUS IN ('2004', '2008') AND OD.UPDATED_ON BETWEEN TODAY AND TOMORROW THEN OD.ORDER_GUID END) AS REMOVED_COUNT
        FROM   scrub.dashboard_query_counts_temp OD
        JOIN    SCRUB.ORDERS O
        ON      O.ORDER_GUID = OD.ORDER_GUID
        WHERE   O.ORDER_ORIGIN IS NOT NULL
          AND   O.ORDER_ORIGIN NOT IN ('TEST','test')
          AND   OD.PREFERRED_PARTNER_NAME IS NULL
        GROUP BY O.ORDER_ORIGIN;

END VIEW_DASHBOARD_COUNTS;


FUNCTION GET_MAX_HP_SEQUENCE
(
IN_SERVER_PREFIX                IN VARCHAR2
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        Return the max hp sequence value for the given server prefix.
-----------------------------------------------------------------------------*/

V_HP_SEQUENCE    VARCHAR2 (100);

BEGIN

SELECT  IN_SERVER_PREFIX || TO_CHAR( MAX (TO_NUMBER (SUBSTR (HP_SEQUENCE, 4))))
INTO    V_HP_SEQUENCE
FROM    ORDER_DETAILS
WHERE   SUBSTR (HP_SEQUENCE, 1, 3) = IN_SERVER_PREFIX;

RETURN V_HP_SEQUENCE;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_MAX_HP_SEQUENCE;


/* Query the order_header table to find a GUID, given a Master Order Number (MON). */
FUNCTION get_guid_from_mon (p_master_order_number IN orders.master_order_number%TYPE) RETURN ORDERS.ORDER_GUID%TYPE IS
      v_guid orders.order_guid%TYPE;
BEGIN
      SELECT order_guid
      INTO   v_guid
      FROM   orders
      WHERE  master_order_number = p_master_order_number;
      RETURN v_guid;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
END get_guid_from_mon;

FUNCTION SCRUB_ORDER_EXISTS
(
 IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        scrub order record exists

Input:
        order_guid       varchar2

Output:
        Y/N
-----------------------------------------------------------------------------*/
	CURSOR exists_cur IS
	  select  'Y'
	  from    scrub.orders
	  where   order_guid = in_order_guid;

	v_exists        char(1) := 'N';

BEGIN

	OPEN exists_cur;
	FETCH exists_cur INTO v_exists;
	CLOSE exists_cur;

	RETURN v_exists;

END SCRUB_ORDER_EXISTS;


/*****************************************************************************
                  IS_CART_BILLED
*****************************************************************************/
PROCEDURE IS_CART_BILLED
(
IN_ORDER_GUID           IN SCRUB.ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_CART_BILLED_FLAG    OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure will check if order is part of the shopping cart that
        has been billed. If the cart has been billed, it will return a 'Y'.
        Else, it will return a 'N'.

Input:
        IN_ORDER_GUID              order guid

Output:
        OUT_CART_BILLED_FLAG       flag showing if the cart was billed or not.
                                      fully or partially billed => 'Y'
                                      else => 'N'

-----------------------------------------------------------------------------*/

v_count         number := 0;

BEGIN

  select  count(*)
  into    v_count
  from    clean.payments cp
  where   cp.order_guid = in_order_guid
  and     cp.bill_status in ('Billed','Settled');

  IF v_count > 0 THEN
    OUT_CART_BILLED_FLAG := 'Y';
  ELSE
    OUT_CART_BILLED_FLAG := 'N';
  END IF;

END IS_CART_BILLED;

PROCEDURE GET_STATS_OSP
(
IN_ORDER_GUID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT OD.EXTERNAL_ORDER_NUMBER,
            SO.STATUS,
            SM.DESCRIPTION,
            SO.ITEM_STATE,
            SO.CSR_ID,
            TO_CHAR(SO.TIMESTAMP, 'MM/DD/YYYY HH24:MI:SS') TIMESTAMP,
            O.ORDER_DATE,
            OD.DELIVERY_DATE,
            OD.PRODUCT_ID || ' ' || PM.PRODUCT_NAME PRODUCT_ID
        FROM SCRUB.ORDERS O, SCRUB.ORDER_DETAILS OD, FRP.STATS_OSP SO, FRP.STATUS_MAPPING SM, FTD_APPS.PRODUCT_MASTER PM
        WHERE O.ORDER_GUID = IN_ORDER_GUID
        AND OD.ORDER_GUID = O.ORDER_GUID
        AND SO.EXTERNAL_ORDER_NUMBER = OD.EXTERNAL_ORDER_NUMBER
        AND SO.TIME_FIELD = (
            SELECT MAX(SO1.TIME_FIELD)
            FROM FRP.STATS_OSP SO1
            WHERE SO1.EXTERNAL_ORDER_NUMBER = SO.EXTERNAL_ORDER_NUMBER)
        AND SM.STATUS = SO.STATUS
        AND PM.PRODUCT_ID (+) = OD.PRODUCT_ID
        ORDER BY OD.EXTERNAL_ORDER_NUMBER;

END GET_STATS_OSP;

FUNCTION ARE_ALL_ITEMS_REMOVED
(
 IN_ORDER_GUID        IN SCRUB.ORDER_DETAILS.ORDER_GUID%TYPE
)
RETURN VARCHAR2

IS

v_count         number := 0;
v_result        char(1) := 'Y';

/*-----------------------------------------------------------------------------
Description:
        This procedure will return "N" if atleast one 
		order item for a given order guid is not in removed 2004 state.
		
Input:
        order_guid       varchar2

Output:
        Y/N
-----------------------------------------------------------------------------*/ 
  CURSOR cur_all_removed IS  
  select count(*) from order_details where status not in ('2004') and order_guid = in_order_guid; 

BEGIN

  OPEN cur_all_removed;
  FETCH cur_all_removed INTO v_count;
  IF v_count > 0 THEN
    v_result := 'N';
  END IF;

  CLOSE cur_all_removed;

  RETURN v_result;

END ARE_ALL_ITEMS_REMOVED;


PROCEDURE GET_ITEM_RECIPIENT_INFO 
( 
IN_ORDER_DETAIL_ID                   IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR            
      SELECT RA.* FROM RECIPIENT_ADDRESSES RA 
        JOIN ORDER_DETAILS OD ON OD.RECIPIENT_ID = RA.RECIPIENT_ID AND OD.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

END GET_ITEM_RECIPIENT_INFO;

/*-----------------------------------------------------------------------------
PRO ORDER SEARCH
-------------------------------------------------------------------------------*/
FUNCTION GET_PRO_ORDER_SEARCH_SQL (
IN_WEB_ORDER_ID         		IN VARCHAR2,
IN_STATUS_FLAG                  IN VARCHAR2,
IN_CSR_ID		                IN ORDERS.CSR_ID%TYPE,
IN_EXCLUDE_FLAG                 IN VARCHAR2,
IN_INCLUDE_BEING_SCRUBBED       IN VARCHAR2,
IN_IS_SINGLE_ROW                IN VARCHAR2           
) RETURN VARCHAR2
AS
	
	V_SQL                   VARCHAR2 (16000);
	
	-- select clause variables
	V_BASE                  VARCHAR2 (500) := 'SELECT ';
	V_FIELDS                VARCHAR2 (500) := 'O.ORDER_GUID ';
	V_GROUPS                VARCHAR2 (500) := 'O.ORDER_GUID ';
	V_ADDTL_FIELDS          VARCHAR2 (500) := ', O.ORDER_ORIGIN, OD.EXTERNAL_ORDER_NUMBER, O.STATUS, O.CSR_ID, O.MASTER_ORDER_NUMBER, O.PRODUCTS_TOTAL, O.ORDER_DATE, OD.DELIVERY_DATE, O.PRODUCTS_TOTAL, O.ORDER_TOTAL, R.FIRST_NAME recip_first_name, R.LAST_NAME recip_last_name, B.LAST_NAME buyer_last_name, B.FIRST_NAME buyer_first_name, BA.CITY, BA.STATE_PROVINCE ';
	V_ADDTL_FIELDS_GROUP    VARCHAR2 (500) := ', O.ORDER_ORIGIN, OD.EXTERNAL_ORDER_NUMBER, O.STATUS, O.CSR_ID, O.MASTER_ORDER_NUMBER, O.PRODUCTS_TOTAL, O.ORDER_DATE, OD.DELIVERY_DATE, O.PRODUCTS_TOTAL, O.ORDER_TOTAL, R.FIRST_NAME , R.LAST_NAME , B.LAST_NAME , B.FIRST_NAME , BA.CITY, BA.STATE_PROVINCE ';
	V_FROM                  VARCHAR2 (500) := 'FROM SCRUB.ORDERS O JOIN SCRUB.ORDER_DETAILS OD ON O.ORDER_GUID = OD.ORDER_GUID ';
	
	-- join clause variables
	V_JOIN                  VARCHAR2 (4000);
	V_RECIPIENTS_JOIN       VARCHAR2 (500) := 'JOIN SCRUB.RECIPIENTS R ON OD.RECIPIENT_ID = R.RECIPIENT_ID ';
	V_RECIPIENTS_ADDR_JOIN  VARCHAR2 (500) := 'JOIN SCRUB.RECIPIENT_ADDRESSES RA ON R.RECIPIENT_ID = RA.RECIPIENT_ID ';
	V_BUYERS_JOIN           VARCHAR2 (500) := 'JOIN SCRUB.BUYERS B ON O.BUYER_ID = B.BUYER_ID ';
	V_BUYER_ADDRESSES_JOIN  VARCHAR2 (500) := 'JOIN SCRUB.BUYER_ADDRESSES BA ON B.BUYER_ID = BA.BUYER_ID AND BA.BUYER_ADDRESS_ID = (SELECT BA2.BUYER_ADDRESS_ID FROM BUYER_ADDRESSES BA2 WHERE BA2.BUYER_ID = BA.BUYER_ID AND ROWNUM = 1) ';
	
	-- To get all orders (also used to exclude all partner orders when used with V_PREFERRED_PARTNER_EXCLUDE)
	V_PREFERRED_PARTNER_JOIN     VARCHAR2 (500) := 'LEFT OUTER JOIN FTD_APPS.SOURCE_PROGRAM_REF SPR ON SPR.SOURCE_CODE = OD.SOURCE_CODE ' ||
	                                               'LEFT OUTER JOIN FTD_APPS.PARTNER_PROGRAM PPGM ON PPGM.PROGRAM_NAME = SPR.PROGRAM_NAME ' ||
	                                               'LEFT OUTER JOIN FTD_APPS.PARTNER_MASTER PRTM ON PRTM.PARTNER_NAME = PPGM.PARTNER_NAME ';
	-- To exclude all partner orders
	V_PARTNER_ORDER_JOIN VARCHAR2(300) := 'LEFT OUTER JOIN PTN_PI.partner_order po on po.master_order_number = o.master_order_number ';
	
	-- where clause variables
	V_ORDERS_WHERE          VARCHAR2 (4000) := 'WHERE OD.STATUS IN (''2002'', ''2003'') ';
	V_ORDERS_REMOVED        VARCHAR2 (500) := 'WHERE OD.STATUS = ''2004'' ';
	V_ORDERS_PENDING        VARCHAR2 (500) := 'WHERE OD.STATUS = ''2005'' ';
	V_ORDERS_ABANDONED      VARCHAR2 (500) := 'WHERE OD.STATUS = ''2011'' ';
	V_ORDERS_EVERYTHING     VARCHAR2 (500) := 'WHERE 1=1 ';
	V_ORDER_ORIGIN          VARCHAR2 (500) := 'AND UPPER (O.ORDER_ORIGIN) IN (''PRO'',''PRO1'') ';
	
	V_ORDER_SCRUBBED        VARCHAR2 (500) := 'AND O.STATUS <> ''1005'' ';
	
	V_ORDER_EXCLUDE         VARCHAR2 (500) := 'AND O.ORDER_GUID NOT IN (SELECT ORDER_GUID FROM CSR_VIEWED_ORDERS WHERE CSR_ID = ''' || IN_CSR_ID || ''') ';
	V_PRODUCT_PROPERTY_JOIN VARCHAR2(500) := 'JOIN FTD_APPS.PRODUCT_MASTER PM ON PM.PRODUCT_ID = OD.PRODUCT_ID ';
	
	V_CONFIRMATION_NUMBER   VARCHAR2 (500) := 'AND PO.partner_order_number like''' || UPPER(IN_WEB_ORDER_ID) || '%'' ';
	
	-- group by variables
	V_GROUP_BY              VARCHAR2 (500) := 'GROUP BY ';
	V_ORDER_BY              VARCHAR2 (500) := 'ORDER BY ';
	V_ORDERS                VARCHAR2 (500) := 'O.ORDER_GUID '; 

BEGIN
	-- build the select clause
	
	-- For single order search or next order search we just need order guid
	IF IN_IS_SINGLE_ROW = 'N' THEN
	  V_FIELDS := V_FIELDS || V_ADDTL_FIELDS;
	  V_GROUPS := V_GROUPS || V_ADDTL_FIELDS_GROUP;
	END IF;
	
	--joining recipients and buyers
	V_JOIN := V_JOIN || V_RECIPIENTS_JOIN;
	V_JOIN := V_JOIN || V_PREFERRED_PARTNER_JOIN;
	V_JOIN := V_JOIN || V_BUYERS_JOIN;
	
	-- add recipients addresses to the join
	V_JOIN := V_JOIN || V_RECIPIENTS_ADDR_JOIN;
	
	--add buyers addresses to the join
	V_JOIN := V_JOIN || V_BUYER_ADDRESSES_JOIN;
	V_JOIN := V_JOIN || V_PARTNER_ORDER_JOIN;
	
	-- build the where clause, set the orders where criteria
	IF IN_STATUS_FLAG = 'R' THEN
	        V_ORDERS_WHERE := V_ORDERS_REMOVED;
	ELSIF IN_STATUS_FLAG = 'P' THEN
	        V_ORDERS_WHERE := V_ORDERS_PENDING;
	ELSIF IN_STATUS_FLAG = 'A' THEN
	        V_ORDERS_WHERE := V_ORDERS_ABANDONED;
	ELSIF IN_STATUS_FLAG = 'E' THEN
	        V_ORDERS_WHERE := V_ORDERS_EVERYTHING;
	END IF;
	
	IF IN_INCLUDE_BEING_SCRUBBED <> 'Y' THEN
	        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_SCRUBBED;
	END IF;
	
	-- and reason if needed
	IF IN_EXCLUDE_FLAG = 'Y' THEN
	        V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_EXCLUDE;
	END IF;
	
	V_ORDERS_WHERE := V_ORDERS_WHERE || V_ORDER_ORIGIN;
	V_ORDERS_WHERE := V_ORDERS_WHERE || V_CONFIRMATION_NUMBER;
	
	IF IN_IS_SINGLE_ROW = 'Y' THEN
	  V_ORDERS_WHERE := V_ORDERS_WHERE || 'AND ROWNUM = 1 ';
	END IF;
	
	-- build the entire sql statement
	V_SQL := V_BASE || V_FIELDS || V_FROM || V_JOIN || V_ORDERS_WHERE || V_GROUP_BY || V_GROUPS || V_ORDER_BY || V_ORDERS; 
	
	RETURN V_SQL;

END GET_PRO_ORDER_SEARCH_SQL;

PROCEDURE GET_PROFLOWERS_ORDERS (
IN_WEB_ORDER_ID         		IN VARCHAR2,
IN_STATUS_FLAG                  IN VARCHAR2,
IN_CSR_ID		                IN ORDERS.CSR_ID%TYPE,
IN_EXCLUDE_FLAG                 IN VARCHAR2,
IN_INCLUDE_BEING_SCRUBBED       IN VARCHAR2, 
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is used to get a list of ProFlower orders
-----------------------------------------------------------------------------*/
V_SQL      				VARCHAR2 (4000);
V_OUT_STATUS            VARCHAR2 (1);
V_OUT_MESSAGE           VARCHAR2 (1000);

BEGIN

	V_SQL  := GET_PRO_ORDER_SEARCH_SQL(IN_WEB_ORDER_ID, IN_STATUS_FLAG, IN_CSR_ID, IN_EXCLUDE_FLAG, IN_INCLUDE_BEING_SCRUBBED, 'N');
	
	-- Should remove all the order being viewed by the CSR, treating as new search.
	MISC_PKG.RESET_CSR_VIEWED_ORDERS (IN_CSR_ID, V_OUT_STATUS, V_OUT_MESSAGE);

    --OPEN OUT_CUR FOR SELECT V_SQL AS DEBUG_MODE FROM DUAL;
    OPEN OUT_CUR FOR V_SQL;

END GET_PROFLOWERS_ORDERS;

PROCEDURE GET_PROFLOWERS_ORDER (
IN_WEB_ORDER_ID         		IN VARCHAR2,
IN_STATUS_FLAG                  IN VARCHAR2,
IN_CSR_ID		                IN ORDERS.CSR_ID%TYPE,
IN_EXCLUDE_FLAG                 IN VARCHAR2,
IN_INCLUDE_BEING_SCRUBBED       IN VARCHAR2, 
OUT_GUID                        OUT ORDERS.ORDER_GUID%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is used to a list of ProFlower orders

        if in debug mode, the cursor will return the built sql statement
-----------------------------------------------------------------------------*/
V_SQL      				VARCHAR2 (4000);
V_OUT_STATUS            VARCHAR2 (1);
V_OUT_MESSAGE           VARCHAR2 (1000); 

BEGIN

    V_SQL  := GET_PRO_ORDER_SEARCH_SQL(IN_WEB_ORDER_ID, IN_STATUS_FLAG, IN_CSR_ID, IN_EXCLUDE_FLAG, IN_INCLUDE_BEING_SCRUBBED, 'Y'); 
    --dbms_output.put_line('vsql: ' || V_SQL);
    
    execute immediate V_SQL into OUT_GUID;
    
    -- if an order is found, try to lock it to the scrub status
    IF OUT_GUID IS NOT NULL THEN 
        ORDERS_MAINT_PKG.UPDATE_ORDER_TO_SCRUB_STATUS (OUT_GUID, NULL, V_OUT_STATUS, V_OUT_MESSAGE);
    END IF;
   
    -- if no records are returned by the search or if records cannot be locked, null is returned
	IF V_OUT_STATUS = 'Y' AND OUT_GUID IS NOT NULL THEN
		MISC_PKG.INSERT_CSR_VIEWED_ORDERS (IN_CSR_ID, OUT_GUID, V_OUT_STATUS, V_OUT_MESSAGE);
	ELSE
		OUT_GUID := NULL; 
	END IF;
	
	EXCEPTION WHEN OTHERS THEN OUT_GUID := NULL;  

END GET_PROFLOWERS_ORDER;

PROCEDURE GET_ADDON_HISTORY_ID
(
IN_ADD_ON_ID                   IN VARCHAR2,
OUT_CUR                        OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      select * from FTD_APPS.ADDON_HISTORY 
      where price = (select price from FTD_APPS.Addon where ADDON_ID = IN_ADD_ON_ID) 
      and addon_id = IN_ADD_ON_ID and status = 'Active';

END GET_ADDON_HISTORY_ID;

PROCEDURE GET_LEGACY_ID_FOR_ORDER
(
IN_EXTERNAL_ORDER_NUMBER       IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_CUR                        OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      select * from SCRUB.ORDER_DETAILS
      where EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

END GET_LEGACY_ID_FOR_ORDER;

PROCEDURE IS_VIP_CUSTOMER_ORDER
(
IN_ORDER_GUID       IN  ORDERS.ORDER_GUID%TYPE,
OUT_VIP_CUSTOMER    OUT VARCHAR2
)
AS

BEGIN
	SELECT VIP_CUSTOMER INTO OUT_VIP_CUSTOMER FROM CLEAN.CUSTOMER 
	WHERE CUSTOMER_ID IN 
	  (SELECT CLEAN_CUSTOMER_ID FROM SCRUB.BUYERS 
	   WHERE BUYER_ID IN
	    (SELECT BUYER_ID FROM SCRUB.ORDERS WHERE ORDER_GUID = IN_ORDER_GUID));

EXCEPTION WHEN NO_DATA_FOUND THEN
    OUT_VIP_CUSTOMER:= null;
      
END IS_VIP_CUSTOMER_ORDER;

END ORDERS_QUERY_PKG;

/
