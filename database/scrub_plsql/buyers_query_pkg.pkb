CREATE OR REPLACE
PACKAGE BODY scrub.BUYERS_QUERY_PKG AS

PROCEDURE VIEW_BUYERS
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer information
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyers
        buyer_id                        number
        customer_id                     varchar2
        last_name                       varchar2
        first_name                      varchar2
        auto_hold                       varchar2
        best_customer                   varchar2
        status                          varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  B.BUYER_ID,
                        B.CUSTOMER_ID,
                        B.LAST_NAME,
                        B.FIRST_NAME,
                        B.AUTO_HOLD,
                        B.BEST_CUSTOMER,
                        B.STATUS,
                        B.CLEAN_CUSTOMER_ID
                FROM    BUYERS B
                JOIN    ORDERS O
                ON      B.BUYER_ID = O.BUYER_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  B.BUYER_ID,
                        B.CUSTOMER_ID,
                        B.LAST_NAME,
                        B.FIRST_NAME,
                        B.AUTO_HOLD,
                        B.BEST_CUSTOMER,
                        B.STATUS,
                        B.CLEAN_CUSTOMER_ID
                FROM    BUYERS B
                WHERE   B.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_BUYERS;


PROCEDURE VIEW_BUYER_ADDRESSES
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer address information
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyer addresses
        buyer_address_id                number
        buyer_id                        number
        address_type                    varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        address_etc                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  BA.BUYER_ADDRESS_ID,
                        BA.BUYER_ID,
                        BA.ADDRESS_TYPE,
                        BA.ADDRESS_LINE_1,
                        BA.ADDRESS_LINE_2,
                        BA.CITY,
                        BA.STATE_PROVINCE,
                        BA.POSTAL_CODE,
                        BA.COUNTRY,
                        BA.COUNTY,
                        BA.ADDRESS_ETC
                FROM    BUYER_ADDRESSES BA
                JOIN    ORDERS O
                ON      BA.BUYER_ID = O.BUYER_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  BA.BUYER_ADDRESS_ID,
                        BA.BUYER_ID,
                        BA.ADDRESS_TYPE,
                        BA.ADDRESS_LINE_1,
                        BA.ADDRESS_LINE_2,
                        BA.CITY,
                        BA.STATE_PROVINCE,
                        BA.POSTAL_CODE,
                        BA.COUNTRY,
                        BA.COUNTY,
                        BA.ADDRESS_ETC
                FROM    BUYER_ADDRESSES BA
                WHERE   BA.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_BUYER_ADDRESSES;


PROCEDURE VIEW_BUYER_PHONES
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer phone information
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyer phones
        buyer_phone_id                  number
        buyer_id                        number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  BP.BUYER_PHONE_ID,
                        BP.BUYER_ID,
                        BP.PHONE_TYPE,
                        BP.PHONE_NUMBER,
                        BP.EXTENSION,
                        BP.PHONE_NUMBER_TYPE,
                        BP.SMS_OPT_IN
                FROM    BUYER_PHONES BP
                JOIN    ORDERS O
                ON      BP.BUYER_ID = O.BUYER_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  BP.BUYER_PHONE_ID,
                        BP.BUYER_ID,
                        BP.PHONE_TYPE,
                        BP.PHONE_NUMBER,
                        BP.EXTENSION,
                        BP.PHONE_NUMBER_TYPE,
                        BP.SMS_OPT_IN
                FROM    BUYER_PHONES BP
                WHERE   BP.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_BUYER_PHONES;


PROCEDURE VIEW_BUYER_EMAILS
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer email information
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyer emails
        buyer_email_id                  number
        buyer_id                        number
        email                           varchar2
        primary                         varchar2
        newsletter                      varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  BE.BUYER_EMAIL_ID,
                        BE.BUYER_ID,
                        BE.EMAIL,
                        BE.PRIMARY,
                        BE.NEWSLETTER
                FROM    BUYER_EMAILS BE
                JOIN    ORDERS O
                ON      BE.BUYER_EMAIL_ID = O.BUYER_EMAIL_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  BE.BUYER_EMAIL_ID,
                        BE.BUYER_ID,
                        BE.EMAIL,
                        BE.PRIMARY,
                        BE.NEWSLETTER
                FROM    BUYER_EMAILS BE
                WHERE   BE.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_BUYER_EMAILS;


PROCEDURE VIEW_BUYER_COMMENTS
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer comments
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyer comments
        buyer_comment_id                number
        buyer_id                        number
        text                            varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  BC.BUYER_COMMENT_ID,
                        BC.BUYER_ID,
                        BC.TEXT
                FROM    BUYER_COMMENTS BC
                JOIN    ORDERS O
                ON      BC.BUYER_ID = O.BUYER_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  BC.BUYER_COMMENT_ID,
                        BC.BUYER_ID,
                        BC.TEXT
                FROM    BUYER_COMMENTS BC
                WHERE   BC.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_BUYER_COMMENTS;


PROCEDURE VIEW_MEMBERSHIPS
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer membership information
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyer memberships
        membership_id                   number
        buyer_id                        number
        membership_type                 varchar2
        last_name                       varchar2
        first_name                      varchar2
        membership_id_number            varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  M.MEMBERSHIP_ID,
                        M.BUYER_ID,
                        M.MEMBERSHIP_TYPE,
                        M.LAST_NAME,
                        M.FIRST_NAME,
                        M.MEMBERSHIP_ID_NUMBER
                FROM    MEMBERSHIPS M
                JOIN    ORDERS O
                ON      M.MEMBERSHIP_ID = O.MEMBERSHIP_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  M.MEMBERSHIP_ID,
                        M.BUYER_ID,
                        M.MEMBERSHIP_TYPE,
                        M.LAST_NAME,
                        M.FIRST_NAME,
                        M.MEMBERSHIP_ID_NUMBER
                FROM    MEMBERSHIPS M
                WHERE   M.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_MEMBERSHIPS;


PROCEDURE VIEW_NCOA
(
IN_ORDER_GUID           IN VARCHAR2,
IN_BUYER_ID             IN NUMBER,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer NCOA information
        for the given order or the specific buyer.

Input:
        order_guid                      varchar2
        buyer_id                        number

Output:
        cursor result set containing all fields in buyer NCOA
        ncoa_id                         number
        buyer_id                        number
        deliverability                  varchar2
        nixie_group                     varchar2
        dupe_set_uniqueness             varchar2
        ems_address_score               varchar2

-----------------------------------------------------------------------------*/

BEGIN

IF IN_ORDER_GUID IS NOT NULL THEN
        OPEN OUT_CUR FOR
                SELECT  N.NCOA_ID,
                        N.BUYER_ID,
                        N.DELIVERABILITY,
                        N.NIXIE_GROUP,
                        N.DUPE_SET_UNIQUENESS,
                        N.EMS_ADDRESS_SCORE
                FROM    NCOA N
                JOIN    ORDERS O
                ON      N.BUYER_ID = O.BUYER_ID
                WHERE   O.ORDER_GUID = IN_ORDER_GUID;

ELSE
        OPEN OUT_CUR FOR
                SELECT  N.NCOA_ID,
                        N.BUYER_ID,
                        N.DELIVERABILITY,
                        N.NIXIE_GROUP,
                        N.DUPE_SET_UNIQUENESS,
                        N.EMS_ADDRESS_SCORE
                FROM    NCOA N
                WHERE   N.BUYER_ID = IN_BUYER_ID;

END IF;

END VIEW_NCOA;


PROCEDURE VIEW_ORDER_BUYER_INFO
(
IN_ORDER_GUID           IN VARCHAR2,
OUT_BUYER_CUR           OUT TYPES.REF_CURSOR,
OUT_BUYER_ADDRESS_CUR   OUT TYPES.REF_CURSOR,
OUT_BUYER_PHONE_CUR     OUT TYPES.REF_CURSOR,
OUT_BUYER_EMAIL_CUR     OUT TYPES.REF_CURSOR,
OUT_BUYER_COMMENTS_CUR  OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR      OUT TYPES.REF_CURSOR,
OUT_NCOA_CUR            OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer information
        for the given order.

Input:
        order_guid                      varchar2

Output:
        buyers cursor
        buyer addresses cursor
        buyer phones cursor
        buyer email cursor
        buyer comments cursor
        membership cursor
        NCOA cursor
-----------------------------------------------------------------------------*/

BEGIN

VIEW_BUYERS (IN_ORDER_GUID, NULL, OUT_BUYER_CUR);
VIEW_BUYER_ADDRESSES (IN_ORDER_GUID, NULL, OUT_BUYER_ADDRESS_CUR);
VIEW_BUYER_PHONES (IN_ORDER_GUID, NULL, OUT_BUYER_PHONE_CUR);
VIEW_BUYER_EMAILS (IN_ORDER_GUID, NULL, OUT_BUYER_EMAIL_CUR);
VIEW_BUYER_COMMENTS (IN_ORDER_GUID, NULL, OUT_BUYER_COMMENTS_CUR);
VIEW_MEMBERSHIPS (IN_ORDER_GUID, NULL, OUT_MEMBERSHIP_CUR);
VIEW_NCOA (IN_ORDER_GUID, NULL, OUT_NCOA_CUR);

END VIEW_ORDER_BUYER_INFO;



PROCEDURE VIEW_BUYER_INFO
(
IN_BUYER_ID             IN NUMBER,
OUT_BUYER_CUR           OUT TYPES.REF_CURSOR,
OUT_BUYER_ADDRESS_CUR   OUT TYPES.REF_CURSOR,
OUT_BUYER_PHONE_CUR     OUT TYPES.REF_CURSOR,
OUT_BUYER_EMAIL_CUR     OUT TYPES.REF_CURSOR,
OUT_BUYER_COMMENTS_CUR  OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR      OUT TYPES.REF_CURSOR,
OUT_NCOA_CUR            OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning buyer information
        for the buyer id.

Input:
        buyer id                        number

Output:
        buyers cursor
        buyer addresses cursor
        buyer phones cursor
        buyer email cursor
        buyer comments cursor
        membership cursor
        NCOA cursor
-----------------------------------------------------------------------------*/

BEGIN

VIEW_BUYERS (NULL, IN_BUYER_ID, OUT_BUYER_CUR);
VIEW_BUYER_ADDRESSES (NULL, IN_BUYER_ID, OUT_BUYER_ADDRESS_CUR);
VIEW_BUYER_PHONES (NULL, IN_BUYER_ID, OUT_BUYER_PHONE_CUR);
VIEW_BUYER_EMAILS (NULL, IN_BUYER_ID, OUT_BUYER_EMAIL_CUR);
VIEW_BUYER_COMMENTS (NULL, IN_BUYER_ID, OUT_BUYER_COMMENTS_CUR);
VIEW_MEMBERSHIPS (NULL, IN_BUYER_ID, OUT_MEMBERSHIP_CUR);
VIEW_NCOA (NULL, IN_BUYER_ID, OUT_NCOA_CUR);

END VIEW_BUYER_INFO;


FUNCTION GET_BUYER_ID
(
IN_LAST_NAME                    IN BUYERS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN BUYERS.FIRST_NAME%TYPE,
IN_POSTAL_CODE                  IN BUYER_ADDRESSES.POSTAL_CODE%TYPE,
IN_ADDRESS_LINE_1               IN BUYER_ADDRESSES.ADDRESS_LINE_1%TYPE
)
RETURN NUMBER

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning buyer id for the unique
        buyer information (last name + first name initial + zip code +
        first 4 characters of address line 1).

Input:
        last_name                       varchar2
        first_name                      varchar2
        postal_code                     varchar2
        address_line_1                  varchar2

Output:
        number  (buyer id)
-----------------------------------------------------------------------------*/

V_BUYER_ID      BUYERS.BUYER_ID%TYPE;

BEGIN

SELECT  B.BUYER_ID
INTO    V_BUYER_ID
FROM    BUYERS B
JOIN    BUYER_ADDRESSES BA
ON      B.BUYER_ID = BA.BUYER_ID
WHERE   B.LAST_NAME = IN_LAST_NAME
AND     SUBSTR (B.FIRST_NAME, 1, 1) = SUBSTR (IN_FIRST_NAME, 1, 1)
AND     BA.POSTAL_CODE = IN_POSTAL_CODE
AND     SUBSTR (BA.ADDRESS_LINE_1, 1, 4) = SUBSTR (IN_ADDRESS_LINE_1, 1, 4);

RETURN V_BUYER_ID;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_BUYER_ID;

FUNCTION FIND_AP_ACCOUNT
(
IN_AP_ACCOUNT_TXT                IN VARCHAR2,
IN_PAYMENT_METHOD_ID		 IN AP_ACCOUNTS.PAYMENT_METHOD_ID%TYPE,
IN_BUYER_ID			 IN AP_ACCOUNTS.BUYER_ID%TYPE
)
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure will check if an ap_account_txt, payment_method_id,
        and buyer_id combination exists in the ap_accounts table. If the 
        account exists, it will return the ap_account_id. If multiple exists,
        it will return the min value.

Input:
        IN_AP_ACCOUNT_TXT              ap_account_txt encrypted
        IN_PAYMENT_METHOD_ID	       payment_mehtod_id
        IN_BUYER_ID		       buyer_id

Output:
        return ap_account_id if account exists

-----------------------------------------------------------------------------*/
v_ap_account_id         AP_ACCOUNTS.AP_ACCOUNT_ID%TYPE; 


BEGIN
	
	select min(ap_account_id)
	into v_ap_account_id
	from ap_accounts
	where ap_account_txt = in_ap_account_txt
	and payment_method_id = in_payment_method_id
	and buyer_id = in_buyer_id;

        RETURN v_ap_account_id;

END FIND_AP_ACCOUNT;

FUNCTION GET_CLEAN_CUSTOMER_ID
(
IN_ORDER_GUID                    IN SCRUB.ORDERS.ORDER_GUID%TYPE
)
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This function will obtain the customer id associated with the order.

Input:
        IN_ORDER_GUID

Output:
        return CUSTOMER_ID

-----------------------------------------------------------------------------*/
v_customer_id           SCRUB.BUYERS.CLEAN_CUSTOMER_ID%TYPE;


BEGIN

        select b.clean_customer_id
        into v_customer_id
        from scrub.buyers b
        join scrub.orders o
        on o.buyer_id = b.buyer_id
        and o.order_guid = IN_ORDER_GUID;

        RETURN v_customer_id;

EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN NULL;

END GET_CLEAN_CUSTOMER_ID;

END BUYERS_QUERY_PKG;
.
/
