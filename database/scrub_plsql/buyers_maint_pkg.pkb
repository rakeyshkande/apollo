CREATE OR REPLACE
PACKAGE BODY scrub.BUYERS_MAINT_PKG AS

PROCEDURE INSERT_BUYERS
(
IN_CUSTOMER_ID                  IN BUYERS.CUSTOMER_ID%TYPE,
IN_LAST_NAME                    IN BUYERS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN BUYERS.FIRST_NAME%TYPE,
IN_AUTO_HOLD                    IN BUYERS.AUTO_HOLD%TYPE,
IN_BEST_CUSTOMER                IN BUYERS.BEST_CUSTOMER%TYPE,
IN_STATUS                       IN BUYERS.STATUS%TYPE,
OUT_BUYER_ID                    OUT BUYERS.BUYER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer information.

Input:
        customer_id                     varchar2
        last_name                       varchar2
        first_name                      varchar2
        auto_hold                       varchar2
        best_customer                   varchar2
        status                          varchar2

Output:
        buyer_id                        number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

OUT_BUYER_ID := KEY.KEYGEN('BUYERS');

INSERT INTO BUYERS
(
        BUYER_ID,
        CUSTOMER_ID,
        LAST_NAME,
        FIRST_NAME,
        AUTO_HOLD,
        BEST_CUSTOMER,
        STATUS
)
VALUES
(
        OUT_BUYER_ID,
        IN_CUSTOMER_ID,
        RTRIM (IN_LAST_NAME),
        RTRIM (IN_FIRST_NAME),
        IN_AUTO_HOLD,
        IN_BEST_CUSTOMER,
        IN_STATUS
)
;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_BUYERS;


PROCEDURE INSERT_BUYER_ADDRESSES
(
IN_BUYER_ID                     IN BUYER_ADDRESSES.BUYER_ID%TYPE,
IN_ADDRESS_TYPE                 IN BUYER_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_ADDRESS_LINE_1               IN BUYER_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN BUYER_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN BUYER_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN BUYER_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN BUYER_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN BUYER_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN BUYER_ADDRESSES.COUNTY%TYPE,
IN_ADDRESS_ETC                  IN BUYER_ADDRESSES.ADDRESS_ETC%TYPE,
OUT_BUYER_ADDRESS_ID            OUT BUYER_ADDRESSES.BUYER_ADDRESS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer address information.

Input:
        buyer_id                        number
        address_type                    varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        address_etc                     varchar2

Output:
        buyer_address_id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_BUYER_ADDRESS_ID := KEY.KEYGEN('BUYER_ADDRESSES');

INSERT INTO BUYER_ADDRESSES
(
        BUYER_ADDRESS_ID,
        BUYER_ID,
        ADDRESS_TYPE,
        ADDRESS_LINE_1,
        ADDRESS_LINE_2,
        CITY,
        STATE_PROVINCE,
        POSTAL_CODE,
        COUNTRY,
        COUNTY,
        ADDRESS_ETC
)
VALUES
(
        OUT_BUYER_ADDRESS_ID,
        IN_BUYER_ID,
        UPPER (IN_ADDRESS_TYPE),
        IN_ADDRESS_LINE_1,
        IN_ADDRESS_LINE_2,
        IN_CITY,
        IN_STATE_PROVINCE,
        IN_POSTAL_CODE,
        IN_COUNTRY,
        IN_COUNTY,
        IN_ADDRESS_ETC
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_BUYER_ADDRESSES;


PROCEDURE INSERT_BUYER_PHONES
(
IN_BUYER_ID                     IN BUYER_PHONES.BUYER_ID%TYPE,
IN_PHONE_TYPE                   IN BUYER_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN BUYER_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN BUYER_PHONES.EXTENSION%TYPE,
IN_PHONE_NUMBER_TYPE            IN BUYER_PHONES.PHONE_NUMBER_TYPE%TYPE,
IN_SMS_OPT_IN                   IN BUYER_PHONES.SMS_OPT_IN%TYPE,
OUT_BUYER_PHONE_ID              OUT BUYER_PHONES.BUYER_PHONE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer phone information.

Input:
        buyer_id                        number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2
        

Output:
        buyer_phone_id                  number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_BUYER_PHONE_ID := KEY.KEYGEN('BUYER_PHONES');

INSERT INTO BUYER_PHONES
(
        BUYER_PHONE_ID,
        BUYER_ID,
        PHONE_TYPE,
        PHONE_NUMBER,
        EXTENSION,
        PHONE_NUMBER_TYPE,
        SMS_OPT_IN
)
VALUES
(
        OUT_BUYER_PHONE_ID,
        IN_BUYER_ID,
        UPPER (IN_PHONE_TYPE),
        IN_PHONE_NUMBER,
        IN_EXTENSION,
        IN_PHONE_NUMBER_TYPE,
        IN_SMS_OPT_IN        
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_BUYER_PHONES;


PROCEDURE INSERT_BUYER_EMAILS
(
IN_BUYER_ID                     IN BUYER_EMAILS.BUYER_ID%TYPE,
IN_EMAIL                        IN BUYER_EMAILS.EMAIL%TYPE,
IN_PRIMARY                      IN BUYER_EMAILS.PRIMARY%TYPE,
IN_NEWSLETTER                   IN BUYER_EMAILS.NEWSLETTER%TYPE,
OUT_BUYER_EMAIL_ID              OUT BUYER_EMAILS.BUYER_EMAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer email information.

Input:
        buyer_id                        number
        email                           varchar2
        primary                         varchar2
        newsletter                      varchar2

Output:
        buyer_email_id                  number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_BUYER_EMAIL_ID := KEY.KEYGEN('BUYER_EMAILS');

INSERT INTO BUYER_EMAILS
(
        BUYER_EMAIL_ID,
        BUYER_ID,
        EMAIL,
        PRIMARY,
        NEWSLETTER
)
VALUES
(
        OUT_BUYER_EMAIL_ID,
        IN_BUYER_ID,
        IN_EMAIL,
        IN_PRIMARY,
        IN_NEWSLETTER
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_BUYER_EMAILS;


PROCEDURE INSERT_BUYER_COMMENTS
(
IN_BUYER_ID                     IN BUYER_COMMENTS.BUYER_ID%TYPE,
IN_TEXT                         IN BUYER_COMMENTS.TEXT%TYPE,
OUT_BUYER_COMMENT_ID            OUT BUYER_COMMENTS.BUYER_COMMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer comments.

Input:
        buyer_id                        number
        text                            varchar2

Output:
        buyer_comment_id                number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_BUYER_COMMENT_ID := KEY.KEYGEN('BUYER_COMMENTS');

INSERT INTO BUYER_COMMENTS
(
        BUYER_COMMENT_ID,
        BUYER_ID,
        TEXT
)
VALUES
(
        OUT_BUYER_COMMENT_ID,
        IN_BUYER_ID,
        IN_TEXT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_BUYER_COMMENTS;


PROCEDURE INSERT_MEMBERSHIPS
(
IN_BUYER_ID                     IN MEMBERSHIPS.BUYER_ID%TYPE,
IN_MEMBERSHIP_TYPE              IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_LAST_NAME                    IN MEMBERSHIPS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN MEMBERSHIPS.FIRST_NAME%TYPE,
IN_MEMBERSHIP_ID_NUMBER         IN MEMBERSHIPS.MEMBERSHIP_ID_NUMBER%TYPE,
OUT_MEMBERSHIP_ID               OUT MEMBERSHIPS.MEMBERSHIP_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer membership
        information.

Input:

        buyer_id                        number
        membership_type                 varchar2
        last_name                       varchar2
        first_name                      varchar2
        membership_id_number            varchar2

Output:
        membership_id                   number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_MEMBERSHIP_ID := KEY.KEYGEN('MEMBERSHIPS');

INSERT INTO MEMBERSHIPS
(
        MEMBERSHIP_ID,
        BUYER_ID,
        MEMBERSHIP_TYPE,
        LAST_NAME,
        FIRST_NAME,
        MEMBERSHIP_ID_NUMBER
)
VALUES
(
        OUT_MEMBERSHIP_ID,
        IN_BUYER_ID,
        IN_MEMBERSHIP_TYPE,
        IN_LAST_NAME,
        IN_FIRST_NAME,
        IN_MEMBERSHIP_ID_NUMBER
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_MEMBERSHIPS;


PROCEDURE INSERT_NCOA
(
IN_BUYER_ID                     IN NCOA.BUYER_ID%TYPE,
IN_DELIVERABILITY               IN NCOA.DELIVERABILITY%TYPE,
IN_NIXIE_GROUP                  IN NCOA.NIXIE_GROUP%TYPE,
IN_DUPE_SET_UNIQUENESS          IN NCOA.DUPE_SET_UNIQUENESS%TYPE,
IN_EMS_ADDRESS_SCORE            IN NCOA.EMS_ADDRESS_SCORE%TYPE,
OUT_NCOA_ID                     OUT NCOA.NCOA_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting buyer NCOA.

Input:
        buyer_id                        number
        deliverability                  varchar2
        nixie_group                     varchar2
        dupe_set_uniqueness             varchar2
        ems_address_score               varchar2

Output:
        ncoa_id                         number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_NCOA_ID := KEY.KEYGEN('NCOA');

INSERT INTO NCOA
(
        NCOA_ID,
        BUYER_ID,
        DELIVERABILITY,
        NIXIE_GROUP,
        DUPE_SET_UNIQUENESS,
        EMS_ADDRESS_SCORE
)
VALUES
(
        OUT_NCOA_ID,
        IN_BUYER_ID,
        IN_DELIVERABILITY,
        IN_NIXIE_GROUP,
        IN_DUPE_SET_UNIQUENESS,
        IN_EMS_ADDRESS_SCORE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_NCOA;


PROCEDURE UPDATE_BUYERS
(
IN_BUYER_ID                     IN BUYERS.BUYER_ID%TYPE,
IN_CUSTOMER_ID                  IN BUYERS.CUSTOMER_ID%TYPE,
IN_LAST_NAME                    IN BUYERS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN BUYERS.FIRST_NAME%TYPE,
IN_AUTO_HOLD                    IN BUYERS.AUTO_HOLD%TYPE,
IN_BEST_CUSTOMER                IN BUYERS.BEST_CUSTOMER%TYPE,
IN_STATUS                       IN BUYERS.STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer information.

Input:
        buyer_id                        number
        customer_id                     varchar2
        last_name                       varchar2
        first_name                      varchar2
        auto_hold                       varchar2
        best_customer                   varchar2
        status                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  BUYERS
SET     CUSTOMER_ID = IN_CUSTOMER_ID,
        LAST_NAME = IN_LAST_NAME,
        FIRST_NAME = IN_FIRST_NAME,
        AUTO_HOLD = IN_AUTO_HOLD,
        BEST_CUSTOMER = IN_BEST_CUSTOMER,
        STATUS = IN_STATUS
WHERE   BUYER_ID = IN_BUYER_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_BUYERS;


PROCEDURE UPDATE_BUYER_ADDRESSES
(
IN_BUYER_ADDRESS_ID             IN BUYER_ADDRESSES.BUYER_ADDRESS_ID%TYPE,
IN_BUYER_ID                     IN BUYER_ADDRESSES.BUYER_ID%TYPE,
IN_ADDRESS_TYPE                 IN BUYER_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_ADDRESS_LINE_1               IN BUYER_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN BUYER_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN BUYER_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN BUYER_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN BUYER_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN BUYER_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN BUYER_ADDRESSES.COUNTY%TYPE,
IN_ADDRESS_ETC                  IN BUYER_ADDRESSES.ADDRESS_ETC%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer address information.

Input:
        buyer_address_id                number
        buyer_id                        number
        address_type                    varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        address_etc                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  BUYER_ADDRESSES
SET     BUYER_ID = IN_BUYER_ID,
        ADDRESS_TYPE = UPPER (IN_ADDRESS_TYPE),
        ADDRESS_LINE_1 = IN_ADDRESS_LINE_1,
        ADDRESS_LINE_2 = IN_ADDRESS_LINE_2,
        CITY = IN_CITY,
        STATE_PROVINCE = IN_STATE_PROVINCE,
        POSTAL_CODE = IN_POSTAL_CODE,
        COUNTRY = IN_COUNTRY,
        COUNTY = IN_COUNTY,
        ADDRESS_ETC = IN_ADDRESS_ETC
WHERE   BUYER_ADDRESS_ID = IN_BUYER_ADDRESS_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_BUYER_ADDRESSES;


PROCEDURE UPDATE_BUYER_PHONES
(
IN_BUYER_PHONE_ID               IN BUYER_PHONES.BUYER_PHONE_ID%TYPE,
IN_BUYER_ID                     IN BUYER_PHONES.BUYER_ID%TYPE,
IN_PHONE_TYPE                   IN BUYER_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN BUYER_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN BUYER_PHONES.EXTENSION%TYPE,
IN_PHONE_NUMBER_TYPE            IN BUYER_PHONES.PHONE_NUMBER_TYPE%TYPE,
IN_SMS_OPT_IN                   IN BUYER_PHONES.SMS_OPT_IN%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer phone information.

Input:
        buyer_phone_id                  number
        buyer_id                        number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  BUYER_PHONES
SET     BUYER_ID = IN_BUYER_ID,
        PHONE_TYPE = UPPER (IN_PHONE_TYPE),
        PHONE_NUMBER = IN_PHONE_NUMBER,
        EXTENSION = IN_EXTENSION,
        PHONE_NUMBER_TYPE = IN_PHONE_NUMBER_TYPE,
        SMS_OPT_IN = IN_SMS_OPT_IN
WHERE   BUYER_PHONE_ID = IN_BUYER_PHONE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_BUYER_PHONES;


PROCEDURE UPDATE_BUYER_EMAILS
(
IN_BUYER_EMAIL_ID               IN BUYER_EMAILS.BUYER_EMAIL_ID%TYPE,
IN_BUYER_ID                     IN BUYER_EMAILS.BUYER_ID%TYPE,
IN_EMAIL                        IN BUYER_EMAILS.EMAIL%TYPE,
IN_PRIMARY                      IN BUYER_EMAILS.PRIMARY%TYPE,
IN_NEWSLETTER                   IN BUYER_EMAILS.NEWSLETTER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer email information.

Input:
        buyer_email_id                  number
        buyer_id                        number
        email                           varchar2
        primary                         varchar2
        newsletter                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  BUYER_EMAILS
SET     BUYER_ID = IN_BUYER_ID,
        EMAIL = IN_EMAIL,
        PRIMARY = IN_PRIMARY,
        NEWSLETTER = IN_NEWSLETTER
WHERE   BUYER_EMAIL_ID = IN_BUYER_EMAIL_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_BUYER_EMAILS;


PROCEDURE UPDATE_BUYER_COMMENTS
(
IN_BUYER_COMMENT_ID             IN BUYER_COMMENTS.BUYER_COMMENT_ID%TYPE,
IN_BUYER_ID                     IN BUYER_COMMENTS.BUYER_ID%TYPE,
IN_TEXT                         IN BUYER_COMMENTS.TEXT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer comments.
        ***THIS PROCEDURE WILL NEVER BE CALLED, COMMENTS WILL ONLY BE INSERTED

Input:
        buyer_comment_id                number
        buyer_id                        number
        text                            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

UPDATE  BUYER_COMMENTS
SET     BUYER_ID = IN_BUYER_ID,
        TEXT = IN_TEXT
WHERE   BUYER_COMMENT_ID = IN_BUYER_COMMENT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_BUYER_COMMENTS;


PROCEDURE UPDATE_MEMBERSHIPS
(
IN_MEMBERSHIP_ID                IN MEMBERSHIPS.MEMBERSHIP_ID%TYPE,
IN_BUYER_ID                     IN MEMBERSHIPS.BUYER_ID%TYPE,
IN_MEMBERSHIP_TYPE              IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_LAST_NAME                    IN MEMBERSHIPS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN MEMBERSHIPS.FIRST_NAME%TYPE,
IN_MEMBERSHIP_ID_NUMBER         IN MEMBERSHIPS.MEMBERSHIP_ID_NUMBER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer membership information.

Input:
        membership_id                   number
        buyer_id                        number
        membership_type                 varchar2
        last_name                       varchar2
        first_name                      varchar2
        membership_id_number            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

UPDATE  MEMBERSHIPS
SET     BUYER_ID = IN_BUYER_ID,
        MEMBERSHIP_TYPE = IN_MEMBERSHIP_TYPE,
        LAST_NAME = IN_LAST_NAME,
        FIRST_NAME = IN_FIRST_NAME,
        MEMBERSHIP_ID_NUMBER = IN_MEMBERSHIP_ID_NUMBER
WHERE   MEMBERSHIP_ID = IN_MEMBERSHIP_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_MEMBERSHIPS;


PROCEDURE UPDATE_NCOA
(
IN_NCOA_ID                      IN NCOA.NCOA_ID%TYPE,
IN_BUYER_ID                     IN NCOA.BUYER_ID%TYPE,
IN_DELIVERABILITY               IN NCOA.DELIVERABILITY%TYPE,
IN_NIXIE_GROUP                  IN NCOA.NIXIE_GROUP%TYPE,
IN_DUPE_SET_UNIQUENESS          IN NCOA.DUPE_SET_UNIQUENESS%TYPE,
IN_EMS_ADDRESS_SCORE            IN NCOA.EMS_ADDRESS_SCORE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating buyer NCOA information.

Input:
        ncoa_id                         number
        buyer_id                        number
        deliverability                  varchar2
        nixie_group                     varchar2
        dupe_set_uniqueness             varchar2
        ems_address_score               varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

UPDATE  NCOA
SET     BUYER_ID = IN_BUYER_ID,
        DELIVERABILITY = IN_DELIVERABILITY,
        NIXIE_GROUP = IN_NIXIE_GROUP,
        DUPE_SET_UNIQUENESS = IN_DUPE_SET_UNIQUENESS,
        EMS_ADDRESS_SCORE = IN_EMS_ADDRESS_SCORE
WHERE   NCOA_ID = IN_NCOA_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_NCOA;


PROCEDURE UPDATE_UNIQUE_BUYER_PHONES 
(
IN_BUYER_ID                     IN BUYER_PHONES.BUYER_ID%TYPE,
IN_PHONE_TYPE                   IN BUYER_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN BUYER_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN BUYER_PHONES.EXTENSION%TYPE,
IN_PHONE_NUMBER_TYPE            IN BUYER_PHONES.PHONE_NUMBER_TYPE%TYPE,
IN_SMS_OPT_IN                   IN BUYER_PHONES.SMS_OPT_IN%TYPE,
OUT_BUYER_PHONE_ID              OUT BUYER_PHONES.BUYER_PHONE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting buyer phone information
        for the given buyer phone id.  This table is updated using the unique key,
        so an update is performed, if no records are updated, then the record
        is inserted. If the record is inserted, the buyer phone id is returned.

Input:
        buyer_id                        number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

Output:
        buyer_phone_id                  number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT  BUYER_PHONE_ID
INTO    OUT_BUYER_PHONE_ID
FROM    BUYER_PHONES
WHERE   BUYER_ID = IN_BUYER_ID
AND     PHONE_TYPE = IN_PHONE_TYPE;

UPDATE_BUYER_PHONES
(
        OUT_BUYER_PHONE_ID,
        IN_BUYER_ID,
        IN_PHONE_TYPE,
        IN_PHONE_NUMBER,
        IN_EXTENSION,
        IN_PHONE_NUMBER_TYPE,
        IN_SMS_OPT_IN,
        OUT_STATUS,
        OUT_MESSAGE
);

EXCEPTION WHEN NO_DATA_FOUND THEN
        -- no record was found using the unique key, so insert the record
         INSERT_BUYER_PHONES
        (
                IN_BUYER_ID,
                IN_PHONE_TYPE,
                IN_PHONE_NUMBER,
                IN_EXTENSION,
                IN_PHONE_NUMBER_TYPE,
                IN_SMS_OPT_IN,
                OUT_BUYER_PHONE_ID,
                OUT_STATUS,
                OUT_MESSAGE
        );

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_UNIQUE_BUYER_PHONES;


PROCEDURE UPDATE_UNIQUE_MEMBERSHIPS
(
IN_BUYER_ID                     IN MEMBERSHIPS.BUYER_ID%TYPE,
IN_MEMBERSHIP_TYPE              IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_LAST_NAME                    IN MEMBERSHIPS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN MEMBERSHIPS.FIRST_NAME%TYPE,
IN_MEMBERSHIP_ID_NUMBER         IN MEMBERSHIPS.MEMBERSHIP_ID_NUMBER%TYPE,
OUT_MEMBERSHIP_ID               OUT MEMBERSHIPS.MEMBERSHIP_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting membership
        information. This table is updated using the unique key,
        so an update is performed, if no records are updated, then the record
        is inserted. If the record is inserted, the buyer phone id is returned.

Input:
        buyer_id                        number
        membership_type                 varchar2
        last_name                       varchar2
        first_name                      varchar2
        membership_id_number            varchar2

Output:
        membership_id                   number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT  MEMBERSHIP_ID
INTO    OUT_MEMBERSHIP_ID
FROM    MEMBERSHIPS
WHERE   BUYER_ID = IN_BUYER_ID
AND     MEMBERSHIP_TYPE = IN_MEMBERSHIP_TYPE;

UPDATE_MEMBERSHIPS
(
        OUT_MEMBERSHIP_ID,
        IN_BUYER_ID,
        IN_MEMBERSHIP_TYPE,
        IN_LAST_NAME,
        IN_FIRST_NAME,
        IN_MEMBERSHIP_ID_NUMBER,
        OUT_STATUS,
        OUT_MESSAGE
);

EXCEPTION WHEN NO_DATA_FOUND THEN
        -- no record was found using the unique key, so insert the record
        INSERT_MEMBERSHIPS
        (
                IN_BUYER_ID,
                IN_MEMBERSHIP_TYPE,
                IN_LAST_NAME,
                IN_FIRST_NAME,
                IN_MEMBERSHIP_ID_NUMBER,
                OUT_MEMBERSHIP_ID,
                OUT_STATUS,
                OUT_MESSAGE
        );

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_UNIQUE_MEMBERSHIPS;

/******************************************************************************
THE FOLLOWING PROCEDURES ARE FOR HP REPLACEMENT PHASEIII - DISPATCHING
ORDERS FROM SCRUB TO CLEAN (DEDUP BUYER RECORDS THAT ARE TIED TO A CLEAN
CUSTOMER)
******************************************************************************/


PROCEDURE DEDUP_ORDER_BUYER_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_CLEAN_CUSTOMER_ID            IN BUYERS.CLEAN_CUSTOMER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:

Input:
        buyer_id                        number
        clean_customer_id               number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR old_cur IS
        select  b.buyer_id,
                ba.buyer_address_id
        from    buyers b
        join    buyer_addresses ba
        on      b.buyer_id = ba.buyer_id
        where   clean_customer_id = in_clean_customer_id
        order by b.buyer_id desc, ba.buyer_address_id desc;

CURSOR update_email_cur (p_buyer_id number, p_email varchar2) IS
        select  buyer_email_id
        from    buyer_emails
        where   buyer_id = p_buyer_id
        and     email = p_email;

CURSOR credit_card_cur IS
        select  cc.credit_card_id,
                cc.cc_type,
                global.encryption.decrypt_it(cc.cc_number,cc.key_name) cc_number,
                cc.cc_expiration,
                decode(gift_card_pin,null,null,global.encryption.decrypt_it(gift_card_pin,cc.key_name)) gift_card_pin
        from    credit_cards cc
        join    payments p
        on      cc.credit_card_id = p.credit_card_id
        where   p.order_guid = in_order_guid;
        
CURSOR ap_account_cur IS
	select	p.ap_account_id, aa.ap_account_txt, p.payment_type
        from	payments p
        join	ap_accounts aa
        on	p.ap_account_id = aa.ap_account_id
        where	p.order_guid = in_order_guid;

v_update_buyer_id       buyers.buyer_id%type;
v_update_buyer_address_id buyer_addresses.buyer_address_id%type;

v_buyer_cur             types.ref_cursor;
v_buyer_address_cur     types.ref_cursor;
v_buyer_phone_cur       types.ref_cursor;
v_buyer_email_cur       types.ref_cursor;
v_buyer_comments_cur    types.ref_cursor;
v_membership_cur        types.ref_cursor;
v_ncoa_cur              types.ref_cursor;

v_select_buyer_id       buyers.buyer_id%type;
v_customer_id           buyers.customer_id%type;
v_last_name             buyers.last_name%type;
v_first_name            buyers.first_name%type;
v_auto_hold             buyers.auto_hold%type;
v_best_customer         buyers.best_customer%type;
v_status                buyers.status%type;
v_clean_customer_id     buyers.clean_customer_id%type;

v_buyer_address_id      buyer_addresses.buyer_address_id%type;
v_address_type          buyer_addresses.address_type%type;
v_address_line_1        buyer_addresses.address_line_1%type;
v_address_line_2        buyer_addresses.address_line_2%type;
v_city                  buyer_addresses.city%type;
v_state_province        buyer_addresses.state_province%type;
v_postal_code           buyer_addresses.postal_code%type;
v_country               buyer_addresses.country%type;
v_county                buyer_addresses.county%type;
v_address_etc           buyer_addresses.address_etc%type;

v_phone_type            buyer_phones.phone_type%type;
v_phone_number          buyer_phones.phone_number%type;
v_extension             buyer_phones.extension%type;
v_buyer_phone_id        buyer_phones.buyer_phone_id%type;
v_phone_number_type     buyer_phones.phone_number_type%type;
v_sms_opt_in            buyer_phones.sms_opt_in%type;

v_membership_type       memberships.membership_type%type;
v_membership_id_number  memberships.membership_id_number%type;
v_membership_id         memberships.membership_id%type;

v_email                 buyer_emails.email%type;
v_primary               buyer_emails.primary%type;
v_newsletter            buyer_emails.newsletter%type;
v_buyer_email_id        buyer_emails.buyer_email_id%type;

v_text                  buyer_comments.text%type;
v_buyer_comment_id      buyer_comments.buyer_comment_id%type;

v_cc_type               credit_cards.cc_type%type;
v_cc_number             credit_cards.cc_number%type;
v_cc_expiration         credit_cards.cc_expiration%type;
v_select_credit_card_id credit_cards.credit_card_id%type;
v_update_credit_card_id credit_cards.credit_card_id%type;
v_pin			credit_cards.gift_card_pin%type;

v_update_ap_account_id	ap_accounts.ap_account_id%type;
v_ap_account_id		ap_accounts.ap_account_id%type;
v_ap_account_txt	ap_accounts.ap_account_txt%type;
v_payment_method_id	ap_accounts.payment_method_id%type;

BEGIN

-- find buyer tied to clean
-- if there is multiple buyer records tied to clean, use the last one inserted into the database (in the case that something erroneous happened)
OPEN old_cur;
FETCH old_cur INTO v_update_buyer_id, v_update_buyer_address_id;
CLOSE old_cur;

-- get the new buyer information
BUYERS_QUERY_PKG.VIEW_ORDER_BUYER_INFO
(
        IN_ORDER_GUID=>in_order_guid,
        OUT_BUYER_CUR=>v_buyer_cur,
        OUT_BUYER_ADDRESS_CUR=>v_buyer_address_cur,
        OUT_BUYER_PHONE_CUR=>v_buyer_phone_cur,
        OUT_BUYER_EMAIL_CUR=>v_buyer_email_cur,
        OUT_BUYER_COMMENTS_CUR=>v_buyer_comments_cur,
        OUT_MEMBERSHIP_CUR=>v_membership_cur,
        OUT_NCOA_CUR=>v_ncoa_cur
);
-- NCOA is not used in scrub so close the cursor
CLOSE v_ncoa_cur;

-- update buyer record
FETCH v_buyer_cur INTO
        v_select_buyer_id,
        v_customer_id,
        v_last_name,
        v_first_name,
        v_auto_hold,
        v_best_customer,
        v_status,
        v_clean_customer_id;
CLOSE v_buyer_cur;

UPDATE_BUYERS
(
        IN_BUYER_ID=>v_update_buyer_id,
        IN_CUSTOMER_ID=>v_customer_id,
        IN_LAST_NAME=>v_last_name,
        IN_FIRST_NAME=>v_first_name,
        IN_AUTO_HOLD=>v_auto_hold,
        IN_BEST_CUSTOMER=>v_best_customer,
        IN_STATUS=>v_status,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);

-- update the buyer address
IF out_status = 'Y' THEN
        FETCH v_buyer_address_cur INTO
                v_buyer_address_id,
                v_select_buyer_id,
                v_address_type,
                v_address_line_1,
                v_address_line_2,
                v_city,
                v_state_province,
                v_postal_code,
                v_country,
                v_county,
                v_address_etc;

        -- move the select buyer address record to point to the update address record
        UPDATE_BUYER_ADDRESSES
        (
                IN_BUYER_ADDRESS_ID=>v_update_buyer_address_id,
                IN_BUYER_ID=>v_update_buyer_id,
                IN_ADDRESS_TYPE=>v_address_type,
                IN_ADDRESS_LINE_1=>v_address_line_1,
                IN_ADDRESS_LINE_2=>v_address_line_2,
                IN_CITY=>v_city,
                IN_STATE_PROVINCE=>v_state_province,
                IN_POSTAL_CODE=>v_postal_code,
                IN_COUNTRY=>v_country,
                IN_COUNTY=>v_county,
                IN_ADDRESS_ETC=>v_address_etc,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

END IF;
CLOSE v_buyer_address_cur;

-- update the buyer phones
IF out_status = 'Y' THEN
        FETCH v_buyer_phone_cur INTO
                v_buyer_phone_id,
                v_select_buyer_id,
                v_phone_type,
                v_phone_number,
                v_extension,
                v_phone_number_type,
                v_sms_opt_in;

        WHILE v_buyer_phone_cur%found LOOP
                -- update unique phone record
                UPDATE_UNIQUE_BUYER_PHONES
                (
                        IN_BUYER_ID=>v_update_buyer_id,
                        IN_PHONE_TYPE=>v_phone_type,
                        IN_PHONE_NUMBER=>v_phone_number,
                        IN_EXTENSION=>v_extension,
                        IN_PHONE_NUMBER_TYPE=>v_phone_number_type,
                        IN_SMS_OPT_IN=>v_sms_opt_in,
                        OUT_BUYER_PHONE_ID=>v_buyer_phone_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );

                IF out_status = 'N' THEN
                        exit;
                END IF;

                FETCH v_buyer_phone_cur INTO
                        v_buyer_phone_id,
                        v_select_buyer_id,
                        v_phone_type,
                        v_phone_number,
                        v_extension,
                        v_phone_number_type,
                        v_sms_opt_in ;
        END LOOP;
END IF;
CLOSE v_buyer_phone_cur;

-- update the membership
IF out_status = 'Y' THEN
        FETCH v_membership_cur INTO
                v_membership_id,
                v_select_buyer_id,
                v_membership_type,
                v_last_name,
                v_first_name,
                v_membership_id_number;

        -- update unique memberships
        UPDATE_UNIQUE_MEMBERSHIPS
        (
                IN_BUYER_ID=>v_update_buyer_id,
                IN_MEMBERSHIP_TYPE=>v_membership_type,
                IN_LAST_NAME=>v_last_name,
                IN_FIRST_NAME=>v_first_name,
                IN_MEMBERSHIP_ID_NUMBER=>v_membership_id_number,
                OUT_MEMBERSHIP_ID=>v_membership_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
END IF;
CLOSE v_membership_cur;

-- move email to old buyer (if address exists use it)
IF out_status = 'Y' THEN
        FETCH v_buyer_email_cur INTO
                v_buyer_email_id,
                v_select_buyer_id,
                v_email,
                v_primary,
                v_newsletter;

        -- check if the email address already exists for the buyer
        OPEN update_email_cur (v_update_buyer_id, v_email);
        FETCH update_email_cur INTO v_buyer_email_id;
        CLOSE update_email_cur;

        -- if the email address does not exists, insert a new buyer email record
        IF v_buyer_email_id IS NULL THEN
                INSERT_BUYER_EMAILS
                (
                        IN_BUYER_ID=>v_update_buyer_id,
                        IN_EMAIL=>v_email,
                        IN_PRIMARY=>v_primary,
                        IN_NEWSLETTER=>v_newsletter,
                        OUT_BUYER_EMAIL_ID=>v_buyer_email_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );

        -- if the email address exists, update the found buyer email record
        ELSE
                UPDATE_BUYER_EMAILS
                (
                        IN_BUYER_EMAIL_ID=>v_buyer_email_id,
                        IN_BUYER_ID=>v_update_buyer_id,
                        IN_EMAIL=>v_email,
                        IN_PRIMARY=>v_primary,
                        IN_NEWSLETTER=>v_newsletter,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END IF;
END IF;
CLOSE v_buyer_email_cur;

-- add comments to the update buyer
IF out_status = 'Y' THEN
        FETCH v_buyer_comments_cur INTO
                v_buyer_comment_id,
                v_select_buyer_id,
                v_text;

        WHILE v_buyer_comments_cur%found LOOP
                INSERT_BUYER_COMMENTS
                (
                        IN_BUYER_ID=>v_update_buyer_id,
                        IN_TEXT=>v_text,
                        OUT_BUYER_COMMENT_ID=>v_buyer_comment_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );

                IF out_status = 'N' THEN
                        exit;
                END IF;

                FETCH v_buyer_comments_cur INTO
                        v_buyer_comment_id,
                        v_select_buyer_id,
                        v_text;
        END LOOP;

END IF;
CLOSE v_buyer_comments_cur;

-- move credit card to old buyer
IF out_status = 'Y' THEN
    FOR credit_card_rec in credit_card_cur LOOP
        v_select_credit_card_id := credit_card_rec.credit_card_id;
        v_cc_type := credit_card_rec.cc_type;
        v_cc_number := credit_card_rec.cc_number;
        v_cc_expiration := credit_card_rec.cc_expiration;
        v_pin := credit_card_rec.gift_card_pin;

        ORDERS_MAINT_PKG.UPDATE_UNIQUE_CREDIT_CARDS
        (
                IN_BUYER_ID=>v_update_buyer_id,
                IN_CC_TYPE=>v_cc_type,
                IN_CC_NUMBER=>v_cc_number,
                IN_CC_EXPIRATION=>v_cc_expiration,
                IN_PIN=>v_pin,
                OUT_CREDIT_CARD_ID=>v_update_credit_card_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        IF out_status = 'Y' THEN
                UPDATE  payments
                SET     credit_card_id = v_update_credit_card_id
                WHERE   order_guid = in_order_guid
                AND     credit_card_id = v_select_credit_card_id;
        END IF;
    END LOOP;

END IF;

IF out_status = 'Y' THEN
        -- move order to old buyer and adjust order with membership and email
        UPDATE  orders
        SET     buyer_id = v_update_buyer_id,
                buyer_email_id = v_buyer_email_id,
                membership_id = v_membership_id
        WHERE   order_guid = in_order_guid;
        
        -- If is alternative payment type, find original account and update order payment.
        OPEN 	ap_account_cur;
	FETCH 	ap_account_cur INTO v_ap_account_id, v_ap_account_txt, v_payment_method_id;
	CLOSE 	ap_account_cur;
        
        IF v_ap_account_id IS NOT NULL THEN
        	UPDATE  ap_accounts
        	SET	buyer_id = v_update_buyer_id
        	WHERE	ap_account_id = v_ap_account_id;
        	  
        	v_update_ap_account_id := BUYERS_QUERY_PKG.FIND_AP_ACCOUNT(v_ap_account_txt,v_payment_method_id,v_update_buyer_id);
        	
        	IF v_update_ap_account_id IS NOT NULL and v_ap_account_id <> v_update_ap_account_id THEN 
			UPDATE  payments
			SET	ap_account_id = v_update_ap_account_id
			WHERE	order_guid = in_order_guid
			AND	payment_type = v_payment_method_id;
		END IF;
        END IF;
        			 
END IF;

-- the old buyer record is preserved for debugging purposes
-- DELETE FROM buyers WHERE buyer_id = v_select_buyer_id;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DEDUP_ORDER_BUYER_INFO;

FUNCTION INSERT_AP_ACCOUNTS
(
IN_AP_ACCOUNT_TXT               IN AP_ACCOUNTS.AP_ACCOUNT_TXT%TYPE,
IN_PAYMENT_METHOD_ID            IN AP_ACCOUNTS.PAYMENT_METHOD_ID%TYPE,
IN_BUYER_ID			IN AP_ACCOUNTS.BUYER_ID%TYPE
) 
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a record into the ap_accounts
        table.


Input:
        ap_account_txt                      varchar2
        payment_method_id		    varchar2

Output:
        
-----------------------------------------------------------------------------*/
v_ap_account_id AP_ACCOUNTS.AP_ACCOUNT_ID%TYPE;
v_key_name AP_ACCOUNTS.KEY_NAME%TYPE;

BEGIN
	v_ap_account_id := KEY.KEYGEN('AP_ACCOUNTS');
	
	v_key_name := ftd_apps.misc_pkg.get_alt_pay_ingrian_key_name(IN_PAYMENT_METHOD_ID);
	
	insert into ap_accounts
	(
		ap_account_id,
		ap_account_txt,
		payment_method_id,
		key_name,
		buyer_id,
		created_on,
		created_by,
		updated_on,
		updated_by
	)
	values
	(
		v_ap_account_id,
		global.encryption.encrypt_it(in_ap_account_txt,v_key_name),
		in_payment_method_id,
		v_key_name,
		in_buyer_id,
		sysdate,
		'SYS',
		sysdate,
		'SYS'
	);
	
	RETURN v_ap_account_id;

END INSERT_AP_ACCOUNTS;

END BUYERS_MAINT_PKG;
.
/
