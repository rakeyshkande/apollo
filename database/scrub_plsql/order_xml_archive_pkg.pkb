CREATE OR REPLACE
PACKAGE BODY scrub.ORDER_XML_ARCHIVE_PKG AS

PROCEDURE INSERT_ORDER_XML_ARCHIVE
(
	IN_MASTER_ORDER_NUMBER          IN ORDER_XML_ARCHIVE.MASTER_ORDER_NUMBER%TYPE,
	IN_ORDER_XML                    IN ORDER_XML_ARCHIVE.ORDER_XML%TYPE,
	OUT_STATUS                      OUT VARCHAR2,
	OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting order xml archive information.
Input:
        master_order_number             varchar2
        order_xml                       clob
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

        creditCardNumberXML XMLType;
        creditCardNumberRaw varchar2(30);
        creditCardNumberMasked varchar2(30);
        bmAccountNumberXML XMLType;
        bmAccountNumberRaw varchar2(30);
        bmAccountNumberMasked varchar2(30);
        giftCardTypeXML XMLType;
        giftCardType varchar2(10);
        giftCardXML XMLType;
        giftCardRaw varchar2(100);
        giftCardToSave varchar2(100);
        giftCardPinXML XMLType;
        giftCardPinRaw varchar2(100);
        giftCardPinToSave varchar2(100);
        xmlInDoc XMLType;

BEGIN

        -- Convert the CLOB to XMLType
        xmlInDoc := XMLType(IN_ORDER_XML);

        -- Extract XML Node
        creditCardNumberXML := xmlInDoc.extract('/order/header/cc-number/text()');

        -- extract the cc number from the XML
        if  creditCardNumberXML is not null then
             creditCardNumberRaw := creditCardNumberXML.getstringval();
        end if;

        -- mask off all but the last four digits
        creditCardNumberMasked := '************'||substr(creditCardNumberRaw,-4,4);

        -- Ditto masking if Gift Card
        giftCardXML := xmlInDoc.extract('/order/header/gift-certificates/gift-certificate/number/text()');
        giftCardPinXML := xmlInDoc.extract('/order/header/gift-certificates/gift-certificate/pin/text()');
        if giftCardXML is not null then
            giftCardToSave := giftCardXML.getstringval();  -- Set here in case Gift Cert (instead of Gift Card)
        end if;
        giftCardTypeXML := xmlInDoc.extract('/order/header/gift-certificates/gift-certificate/payment-type/text()');
        if giftCardTypeXML is not null then
	        giftCardType := giftCardTypeXML.getstringval();
	        if  giftCardType = 'GD' then
	            if giftCardXML is not null then
	                giftCardRaw := giftCardXML.getstringval();
	                -- remove CDATA tags.
	                select regexp_replace(giftCardRaw,'[^[:digit:]]','')
	                  into giftCardRaw
	                  from dual;
	                giftCardToSave := '***************'||substr(giftCardRaw,-4,4);
	            end if;
              if giftCardPinXML is not null then
                  giftCardPinRaw := giftCardPinXML.getstringval();
	                giftCardPinToSave := '****';
              end if;
	        end if;
        end if;

        -- Do similar masking for BillMeLater but since majority of orders will
        -- not need this, we'll be a bit more discrete
        bmAccountNumberXML := xmlInDoc.extract('/order/header/bm-acct-num/text()');
        if  bmAccountNumberXML is not null then
            bmAccountNumberRaw := bmAccountNumberXML.getstringval();
            bmAccountNumberMasked := '************'||substr(bmAccountNumberRaw,-4,4);

            -- Update the XML with the masked cc number and BML account, convert back to CLOB, and insert
            INSERT INTO ORDER_XML_ARCHIVE (
                    MASTER_ORDER_NUMBER,
                    ORDER_XML,
                    TIMESTAMP)
            VALUES (
                    IN_MASTER_ORDER_NUMBER,
                    updatexml(xmlInDoc, '/order/header/cc-number/text()', creditCardNumberMasked,
                              '/order/header/gift-certificates/gift-certificate/number/text()', giftCardToSave,
                              '/order/header/gift-certificates/gift-certificate/pin/text()', giftCardToSave,
                              '/order/header/bm-acct-num/text()', bmAccountNumberMasked).getclobval(),
                    SYSDATE);
        else

            -- Update the XML with the masked cc number, convert back to CLOB, and insert
            INSERT INTO ORDER_XML_ARCHIVE
            (
                    MASTER_ORDER_NUMBER,
                    ORDER_XML,
                    TIMESTAMP
            )
            VALUES
            (
                    IN_MASTER_ORDER_NUMBER,
                    updatexml(xmlInDoc, '/order/header/cc-number/text()', creditCardNumberMasked,
                              '/order/header/gift-certificates/gift-certificate/pin/text()', giftCardPinToSave,
                              '/order/header/gift-certificates/gift-certificate/number/text()', giftCardToSave).getclobval(),
                    SYSDATE
            );
        end if;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
	BEGIN
	        OUT_STATUS := 'N';
	        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;    -- end exception block


END INSERT_ORDER_XML_ARCHIVE;



PROCEDURE VIEW_ORDER_XML_ARCHIVE
(
	IN_MASTER_ORDER_NUMBER          IN ORDER_XML_ARCHIVE.MASTER_ORDER_NUMBER%TYPE,
	OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order xml archive information
        for the given master order number.
Input:
        master_order_number             varchar2
Output:
        cursor result set containing all fields in order xml archive
        master_order_number             varchar2
        order_xml                       clob
        timestamp                    	date
-----------------------------------------------------------------------------*/

BEGIN

	OPEN OUT_CUR FOR
	        SELECT  MASTER_ORDER_NUMBER,
	                ORDER_XML,
	                TIMESTAMP
	        FROM    ORDER_XML_ARCHIVE
	        WHERE   MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER
	        ORDER BY TIMESTAMP;

END VIEW_ORDER_XML_ARCHIVE;



END ORDER_XML_ARCHIVE_PKG;
.
/
