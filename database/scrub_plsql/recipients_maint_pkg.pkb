create or replace PACKAGE BODY scrub.RECIPIENTS_MAINT_PKG AS

PROCEDURE INSERT_AVS_ADDRESS
(
IN_RECIPIENT_ID                 IN AVS_ADDRESS.RECIPIENT_ID%TYPE,
IN_ADDRESS                      IN AVS_ADDRESS.ADDRESS%TYPE,
IN_CITY                         IN AVS_ADDRESS.CITY%TYPE,
IN_STATE                        IN AVS_ADDRESS.STATE%TYPE,
IN_ZIP                          IN AVS_ADDRESS.ZIP%TYPE,
IN_COUNTRY                      IN AVS_ADDRESS.COUNTRY%TYPE,
IN_LATITUDE                     IN AVS_ADDRESS.LATITUDE%TYPE,
IN_LONGITUDE                    IN AVS_ADDRESS.LONGITUDE%TYPE,
IN_ENTITY_TYPE                  IN AVS_ADDRESS.ENTITY_TYPE%TYPE,
IN_OVERRIDE_FLAG                IN AVS_ADDRESS.OVERRIDE_FLAG%TYPE,
IN_RESULT                       IN AVS_ADDRESS.AVS_RESULT%TYPE,
IN_AVS_PERFORMED                IN AVS_ADDRESS.AVS_PERFORMED%TYPE,
IN_AVS_PERFORMED_ORIGIN         IN AVS_ADDRESS.AVS_PERFORMED_ORIGIN%TYPE,
OUT_AVS_ADDRESS_ID              OUT AVS_ADDRESS.AVS_ADDRESS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting AVS address information.

Input:
        recipient_id                    number
        address                         varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        country                         varchar2
        latitude                        varchar2
        longitude                       varchar2
        entity_type                     varchar2
        override_flag                   varchar2
        avs_result                      varchar2
        avs_performed                   varchar2
        avs_performed_origin            varchar2

Output:
        avs_address_id                          number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
CURSOR address_sequence_cur IS
    select avs_address_sq.nextval
    from dual;

BEGIN

OPEN address_sequence_cur;
FETCH address_sequence_cur into OUT_AVS_ADDRESS_ID;

INSERT INTO AVS_ADDRESS
(
        AVS_ADDRESS_ID,
        RECIPIENT_ID,
        ADDRESS,
        CITY,
        STATE,
        ZIP,
        COUNTRY,
        LATITUDE,
        LONGITUDE,
        ENTITY_TYPE,
        OVERRIDE_FLAG,
        AVS_RESULT,
        AVS_PERFORMED,
        AVS_PERFORMED_ORIGIN,
        CREATED_ON
)
VALUES
(
        OUT_AVS_ADDRESS_ID,
        IN_RECIPIENT_ID,
        IN_ADDRESS,
        IN_CITY,
        IN_STATE,
        IN_ZIP,
        IN_COUNTRY,
        IN_LATITUDE,
        IN_LONGITUDE,
        IN_ENTITY_TYPE,
        IN_OVERRIDE_FLAG,
        IN_RESULT,
        IN_AVS_PERFORMED,
        IN_AVS_PERFORMED_ORIGIN,
        sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AVS_ADDRESS;

PROCEDURE INSERT_AVS_ADDRESS_SCORE
(
IN_AVS_ADDRESS_ID               IN AVS_ADDRESS_SCORE.AVS_ADDRESS_ID%TYPE,
IN_REASON                       IN AVS_ADDRESS_SCORE.SCORE_REASON%TYPE,
IN_SCORE                        IN AVS_ADDRESS_SCORE.SCORE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting QMS address information.

Input:
        avs_address_id                  number
        reason                          varchar2
        score                           varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR score_sequence_cur IS
    select avs_address_score_sq.nextval
    from   dual;

w_score_id    number;

BEGIN

OPEN score_sequence_cur;
FETCH score_sequence_cur into w_score_id;

INSERT INTO AVS_ADDRESS_SCORE
(
        AVS_SCORE_ID,
        AVS_ADDRESS_ID,
        SCORE_REASON,
        SCORE,
        CREATED_ON
)
VALUES
(
        w_score_id,
        IN_AVS_ADDRESS_ID,
        IN_REASON,
        IN_SCORE,
        sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_AVS_ADDRESS_SCORE;


PROCEDURE INSERT_RECIPIENTS
(
IN_LAST_NAME                    IN RECIPIENTS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN RECIPIENTS.FIRST_NAME%TYPE,
IN_LIST_CODE                    IN RECIPIENTS.LIST_CODE%TYPE,
IN_AUTO_HOLD                    IN RECIPIENTS.AUTO_HOLD%TYPE,
IN_MAIL_LIST_CODE               IN RECIPIENTS.MAIL_LIST_CODE%TYPE,
IN_STATUS                       IN RECIPIENTS.STATUS%TYPE,
IN_CUSTOMER_ID                  IN RECIPIENTS.CUSTOMER_ID%TYPE,
OUT_RECIPIENT_ID                OUT RECIPIENTS.RECIPIENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting recipient information.

Input:
        last_name                       varchar2
        first_name                      varchar2
        list_code                       varchar2
        auto_hold                       varchar2
        mail_list_code                  varchar2
        status                          varchar2
        customer_id                     varchar2

Output:
        recipient_id                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_RECIPIENT_ID := KEY.KEYGEN('RECIPIENTS');

INSERT INTO RECIPIENTS
(
        RECIPIENT_ID,
        LAST_NAME,
        FIRST_NAME,
        LIST_CODE,
        AUTO_HOLD,
        MAIL_LIST_CODE,
        STATUS,
        CUSTOMER_ID
)
VALUES
(
        OUT_RECIPIENT_ID,
        IN_LAST_NAME,
        IN_FIRST_NAME,
        IN_LIST_CODE,
        IN_AUTO_HOLD,
        IN_MAIL_LIST_CODE,
        IN_STATUS,
        IN_CUSTOMER_ID
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_RECIPIENTS;


PROCEDURE INSERT_RECIPIENT_ADDRESSES
(
IN_RECIPIENT_ID                 IN RECIPIENT_ADDRESSES.RECIPIENT_ID%TYPE,
IN_ADDRESS_TYPE                 IN RECIPIENT_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_ADDRESS_LINE_1               IN RECIPIENT_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN RECIPIENT_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN RECIPIENT_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN RECIPIENT_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN RECIPIENT_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN RECIPIENT_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN RECIPIENT_ADDRESSES.COUNTY%TYPE,
IN_INTERNATIONAL                IN RECIPIENT_ADDRESSES.INTERNATIONAL%TYPE,
IN_NAME                         IN RECIPIENT_ADDRESSES.NAME%TYPE,
IN_INFO                         IN RECIPIENT_ADDRESSES.INFO%TYPE,
OUT_RECIPIENT_ADDRESS_ID        OUT RECIPIENT_ADDRESSES.RECIPIENT_ADDRESS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting recipient address information.

Input:
        recipient_id                    number
        address_type                    varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        international                   varchar2
        name                            varchar2
        info                            varchar2

Output:
        recipient_address_id            number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_RECIPIENT_ADDRESS_ID := KEY.KEYGEN('RECIPIENT_ADDRESSES');

INSERT INTO RECIPIENT_ADDRESSES
(
        RECIPIENT_ADDRESS_ID,
        RECIPIENT_ID,
        ADDRESS_TYPE,
        ADDRESS_LINE_1,
        ADDRESS_LINE_2,
        CITY,
        STATE_PROVINCE,
        POSTAL_CODE,
        COUNTRY,
        COUNTY,
        INTERNATIONAL,
        NAME,
        INFO
)
VALUES
(
        OUT_RECIPIENT_ADDRESS_ID,
        IN_RECIPIENT_ID,
        UPPER (IN_ADDRESS_TYPE),
        IN_ADDRESS_LINE_1,
        IN_ADDRESS_LINE_2,
        IN_CITY,
        IN_STATE_PROVINCE,
        IN_POSTAL_CODE,
        IN_COUNTRY,
        IN_COUNTY,
        IN_INTERNATIONAL,
        IN_NAME,
        IN_INFO
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_RECIPIENT_ADDRESSES;


PROCEDURE INSERT_QMS_ADDRESSES
(
IN_RECIPIENT_ID                 IN QMS_ADDRESSES.RECIPIENT_ID%TYPE,
IN_ADDRESS_LINE_1               IN QMS_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN QMS_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN QMS_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN QMS_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN QMS_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN QMS_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN QMS_ADDRESSES.COUNTY%TYPE,
IN_FIRM_NAME                    IN QMS_ADDRESSES.FIRM_NAME%TYPE,
IN_GEOFIND_MATCH_CODE           IN QMS_ADDRESSES.GEOFIND_MATCH_CODE%TYPE,
IN_OVERRIDE_FLAG                IN QMS_ADDRESSES.OVERRIDE_FLAG%TYPE,
IN_LATITUDE                     IN QMS_ADDRESSES.LATITUDE%TYPE,
IN_LONGITUDE                    IN QMS_ADDRESSES.LONGITUDE%TYPE,
IN_RANGE_RECORD_TYPE            IN QMS_ADDRESSES.RANGE_RECORD_TYPE%TYPE,
OUT_QMS_ID                      OUT QMS_ADDRESSES.QMS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting QMS address information.

Input:
        recipient_id                    number
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        firm_name                       varchar2
        geofind_match_code              varchar2
        override_flag                   varchar2
        latitude                        varchar2
        longitude                       varchar2
        range_record_type               varchar2

Output:
        qms_id                          number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_QMS_ID := KEY.KEYGEN('QMS_ADDRESSES');

INSERT INTO QMS_ADDRESSES
(
        QMS_ID,
        RECIPIENT_ID,
        ADDRESS_LINE_1,
        ADDRESS_LINE_2,
        CITY,
        STATE_PROVINCE,
        POSTAL_CODE,
        COUNTRY,
        COUNTY,
        FIRM_NAME,
        GEOFIND_MATCH_CODE,
        OVERRIDE_FLAG,
        LATITUDE,
        LONGITUDE,
        RANGE_RECORD_TYPE
)
VALUES
(
        OUT_QMS_ID,
        IN_RECIPIENT_ID,
        IN_ADDRESS_LINE_1,
        IN_ADDRESS_LINE_2,
        IN_CITY,
        IN_STATE_PROVINCE,
        IN_POSTAL_CODE,
        IN_COUNTRY,
        IN_COUNTY,
        IN_FIRM_NAME,
        IN_GEOFIND_MATCH_CODE,
        IN_OVERRIDE_FLAG,
        IN_LATITUDE,
        IN_LONGITUDE,
        IN_RANGE_RECORD_TYPE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_QMS_ADDRESSES;

PROCEDURE INSERT_RECIPIENT_PHONES
(
IN_RECIPIENT_ID                 IN RECIPIENT_PHONES.RECIPIENT_ID%TYPE,
IN_PHONE_TYPE                   IN RECIPIENT_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN RECIPIENT_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN RECIPIENT_PHONES.EXTENSION%TYPE,
OUT_RECIPIENT_PHONE_ID          OUT RECIPIENT_PHONES.RECIPIENT_PHONE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting recipient phone information.

Input:
        recipient_id                    number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

Output:
        recipient_phone_id              number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_RECIPIENT_PHONE_ID := KEY.KEYGEN('RECIPIENT_PHONES');

INSERT INTO RECIPIENT_PHONES
(
        RECIPIENT_PHONE_ID,
        RECIPIENT_ID,
        PHONE_TYPE,
        PHONE_NUMBER,
        EXTENSION
)
VALUES
(
        OUT_RECIPIENT_PHONE_ID,
        IN_RECIPIENT_ID,
        UPPER (IN_PHONE_TYPE),
        IN_PHONE_NUMBER,
        IN_EXTENSION
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_RECIPIENT_PHONES;


PROCEDURE INSERT_RECIPIENT_COMMENTS
(
IN_RECIPIENT_ID                 IN RECIPIENT_COMMENTS.RECIPIENT_ID%TYPE,
IN_TEXT                         IN RECIPIENT_COMMENTS.TEXT%TYPE,
OUT_RECIPIENT_COMMENT_ID        OUT RECIPIENT_COMMENTS.RECIPIENT_COMMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting recipient comments.

Input:
        recipient_id                    number
        text                            varchar2

Output:
        recipient_comment_id            number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

OUT_RECIPIENT_COMMENT_ID := KEY.KEYGEN('RECIPIENT_COMMENTS');

INSERT INTO RECIPIENT_COMMENTS
(
        RECIPIENT_COMMENT_ID,
        RECIPIENT_ID,
        TEXT
)
VALUES
(
        OUT_RECIPIENT_COMMENT_ID,
        IN_RECIPIENT_ID,
        IN_TEXT
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END INSERT_RECIPIENT_COMMENTS;


PROCEDURE UPDATE_RECIPIENTS
(
IN_RECIPIENT_ID                 IN RECIPIENTS.RECIPIENT_ID%TYPE,
IN_LAST_NAME                    IN RECIPIENTS.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN RECIPIENTS.FIRST_NAME%TYPE,
IN_LIST_CODE                    IN RECIPIENTS.LIST_CODE%TYPE,
IN_AUTO_HOLD                    IN RECIPIENTS.AUTO_HOLD%TYPE,
IN_MAIL_LIST_CODE               IN RECIPIENTS.MAIL_LIST_CODE%TYPE,
IN_STATUS                       IN RECIPIENTS.STATUS%TYPE,
IN_CUSTOMER_ID                  IN RECIPIENTS.CUSTOMER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating recipient information.

Input:
        recipient_id                    number
        last_name                       varchar2
        first_name                      varchar2
        list_code                       varchar2
        auto_hold                       varchar2
        mail_list_code                  varchar2
        status                          varchar2
        customer_id                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  RECIPIENTS
SET     LAST_NAME = IN_LAST_NAME,
        FIRST_NAME = IN_FIRST_NAME,
        LIST_CODE = IN_LIST_CODE,
        AUTO_HOLD = IN_AUTO_HOLD,
        MAIL_LIST_CODE = IN_MAIL_LIST_CODE,
        STATUS = IN_STATUS,
        CUSTOMER_ID = IN_CUSTOMER_ID
WHERE   RECIPIENT_ID = IN_RECIPIENT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_RECIPIENTS;


PROCEDURE UPDATE_RECIPIENT_ADDRESSES
(
IN_RECIPIENT_ADDRESS_ID         IN RECIPIENT_ADDRESSES.RECIPIENT_ADDRESS_ID%TYPE,
IN_RECIPIENT_ID                 IN RECIPIENT_ADDRESSES.RECIPIENT_ID%TYPE,
IN_ADDRESS_TYPE                 IN RECIPIENT_ADDRESSES.ADDRESS_TYPE%TYPE,
IN_ADDRESS_LINE_1               IN RECIPIENT_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN RECIPIENT_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN RECIPIENT_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN RECIPIENT_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN RECIPIENT_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN RECIPIENT_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN RECIPIENT_ADDRESSES.COUNTY%TYPE,
IN_INTERNATIONAL                IN RECIPIENT_ADDRESSES.INTERNATIONAL%TYPE,
IN_NAME                         IN RECIPIENT_ADDRESSES.NAME%TYPE,
IN_INFO                         IN RECIPIENT_ADDRESSES.INFO%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating recipient address information.

Input:
        recipient_address_id            number
        recipient_id                    number
        address_type                    varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        international                   varchar2
        name                            varchar2
        info                            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  RECIPIENT_ADDRESSES
SET     RECIPIENT_ID = IN_RECIPIENT_ID,
        ADDRESS_TYPE = UPPER (IN_ADDRESS_TYPE),
        ADDRESS_LINE_1 = IN_ADDRESS_LINE_1,
        ADDRESS_LINE_2 = IN_ADDRESS_LINE_2,
        CITY = IN_CITY,
        STATE_PROVINCE = IN_STATE_PROVINCE,
        POSTAL_CODE = IN_POSTAL_CODE,
        COUNTRY = IN_COUNTRY,
        COUNTY = IN_COUNTY,
        INTERNATIONAL = IN_INTERNATIONAL,
        NAME = IN_NAME,
        INFO = IN_INFO
WHERE   RECIPIENT_ADDRESS_ID = IN_RECIPIENT_ADDRESS_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_RECIPIENT_ADDRESSES;


PROCEDURE UPDATE_QMS_ADDRESSES
(
IN_QMS_ID                       IN QMS_ADDRESSES.QMS_ID%TYPE,
IN_RECIPIENT_ID                 IN QMS_ADDRESSES.RECIPIENT_ID%TYPE,
IN_ADDRESS_LINE_1               IN QMS_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN QMS_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN QMS_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN QMS_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN QMS_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN QMS_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN QMS_ADDRESSES.COUNTY%TYPE,
IN_FIRM_NAME                    IN QMS_ADDRESSES.FIRM_NAME%TYPE,
IN_GEOFIND_MATCH_CODE           IN QMS_ADDRESSES.GEOFIND_MATCH_CODE%TYPE,
IN_OVERRIDE_FLAG                IN QMS_ADDRESSES.OVERRIDE_FLAG%TYPE,
IN_LATITUDE                     IN QMS_ADDRESSES.LATITUDE%TYPE,
IN_LONGITUDE                    IN QMS_ADDRESSES.LONGITUDE%TYPE,
IN_RANGE_RECORD_TYPE            IN QMS_ADDRESSES.RANGE_RECORD_TYPE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating QMS address information.

Input:
        qms_id                          number
        recipient_id                    number
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        firm_name                       varchar2
        geofind_match_code              varchar2
        override_flag                   varchar2
        latitude                        varchar2
        longitude                       varchar2
        range_record_type               varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  QMS_ADDRESSES
SET     QMS_ID = IN_QMS_ID,
        RECIPIENT_ID = IN_RECIPIENT_ID,
        ADDRESS_LINE_1 = IN_ADDRESS_LINE_1,
        ADDRESS_LINE_2 = IN_ADDRESS_LINE_2,
        CITY = IN_CITY,
        STATE_PROVINCE = IN_STATE_PROVINCE,
        POSTAL_CODE = IN_POSTAL_CODE,
        COUNTRY = IN_COUNTRY,
        COUNTY = IN_COUNTY,
        FIRM_NAME = IN_FIRM_NAME,
        GEOFIND_MATCH_CODE = IN_GEOFIND_MATCH_CODE,
        OVERRIDE_FLAG = IN_OVERRIDE_FLAG,
        LATITUDE = IN_LATITUDE,
        LONGITUDE = IN_LONGITUDE,
        RANGE_RECORD_TYPE = IN_RANGE_RECORD_TYPE
WHERE   QMS_ID = IN_QMS_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_QMS_ADDRESSES;


PROCEDURE UPDATE_RECIPIENT_PHONES
(
IN_RECIPIENT_PHONE_ID           IN RECIPIENT_PHONES.RECIPIENT_PHONE_ID%TYPE,
IN_RECIPIENT_ID                 IN RECIPIENT_PHONES.RECIPIENT_ID%TYPE,
IN_PHONE_TYPE                   IN RECIPIENT_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN RECIPIENT_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN RECIPIENT_PHONES.EXTENSION%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating recipient phone information.

Input:
        recipient_phone_id              number
        recipient_id                    number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

UPDATE  RECIPIENT_PHONES
SET     RECIPIENT_ID = IN_RECIPIENT_ID,
        PHONE_TYPE = UPPER (IN_PHONE_TYPE),
        PHONE_NUMBER = IN_PHONE_NUMBER,
        EXTENSION = IN_EXTENSION
WHERE   RECIPIENT_PHONE_ID = IN_RECIPIENT_PHONE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_RECIPIENT_PHONES;


PROCEDURE UPDATE_RECIPIENT_COMMENTS
(
IN_RECIPIENT_COMMENT_ID         IN RECIPIENT_COMMENTS.RECIPIENT_COMMENT_ID%TYPE,
IN_RECIPIENT_ID                 IN RECIPIENT_COMMENTS.RECIPIENT_ID%TYPE,
IN_TEXT                         IN RECIPIENT_COMMENTS.TEXT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating recipient comments.
        ***THIS PROCEDURE WILL NEVER BE CALLED, COMMENTS WILL ONLY BE INSERTED

Input:
        recipient_comment_id            number
        recipient_id                    number
        text                            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/
BEGIN

UPDATE  RECIPIENT_COMMENTS
SET     RECIPIENT_ID = IN_RECIPIENT_ID,
        TEXT = IN_TEXT
WHERE   RECIPIENT_COMMENT_ID = IN_RECIPIENT_COMMENT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_RECIPIENT_COMMENTS;


PROCEDURE UPDATE_UNIQUE_RECIPIENT_PHONES
(
IN_RECIPIENT_ID                 IN RECIPIENT_PHONES.RECIPIENT_ID%TYPE,
IN_PHONE_TYPE                   IN RECIPIENT_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN RECIPIENT_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN RECIPIENT_PHONES.EXTENSION%TYPE,
OUT_RECIPIENT_PHONE_ID          OUT RECIPIENT_PHONES.RECIPIENT_PHONE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting recipient phone information
        for the given recipient phone id.  This table is updated using the unique key,
        so an update is performed, if no records are updated, then the record
        is inserted. If the record is inserted, the recipient phone id is returned.

Input:
        recipient_id                    number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

Output:
        recipient_phone_id              number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT  RECIPIENT_PHONE_ID
INTO    OUT_RECIPIENT_PHONE_ID
FROM    RECIPIENT_PHONES
WHERE   RECIPIENT_ID = IN_RECIPIENT_ID
AND     PHONE_TYPE = IN_PHONE_TYPE;

UPDATE_RECIPIENT_PHONES
(
        OUT_RECIPIENT_PHONE_ID,
        IN_RECIPIENT_ID,
        IN_PHONE_TYPE,
        IN_PHONE_NUMBER,
        IN_EXTENSION,
        OUT_STATUS,
        OUT_MESSAGE
);

EXCEPTION WHEN NO_DATA_FOUND THEN
        -- no record was found using the unique key, so insert the record
        INSERT_RECIPIENT_PHONES
        (
                IN_RECIPIENT_ID,
                IN_PHONE_TYPE,
                IN_PHONE_NUMBER,
                IN_EXTENSION,
                OUT_RECIPIENT_PHONE_ID,
                OUT_STATUS,
                OUT_MESSAGE
        );

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_UNIQUE_RECIPIENT_PHONES;


PROCEDURE UPDATE_UNIQUE_QMS_ADDRESSES
(
IN_RECIPIENT_ID                 IN QMS_ADDRESSES.RECIPIENT_ID%TYPE,
IN_ADDRESS_LINE_1               IN QMS_ADDRESSES.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN QMS_ADDRESSES.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN QMS_ADDRESSES.CITY%TYPE,
IN_STATE_PROVINCE               IN QMS_ADDRESSES.STATE_PROVINCE%TYPE,
IN_POSTAL_CODE                  IN QMS_ADDRESSES.POSTAL_CODE%TYPE,
IN_COUNTRY                      IN QMS_ADDRESSES.COUNTRY%TYPE,
IN_COUNTY                       IN QMS_ADDRESSES.COUNTY%TYPE,
IN_FIRM_NAME                    IN QMS_ADDRESSES.FIRM_NAME%TYPE,
IN_GEOFIND_MATCH_CODE           IN QMS_ADDRESSES.GEOFIND_MATCH_CODE%TYPE,
IN_OVERRIDE_FLAG                IN QMS_ADDRESSES.OVERRIDE_FLAG%TYPE,
IN_LATITUDE                     IN QMS_ADDRESSES.LATITUDE%TYPE,
IN_LONGITUDE                    IN QMS_ADDRESSES.LONGITUDE%TYPE,
IN_RANGE_RECORD_TYPE            IN QMS_ADDRESSES.RANGE_RECORD_TYPE%TYPE,
OUT_QMS_ID                      OUT QMS_ADDRESSES.QMS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting qms address information
        for the given recipient id.  This table is updated using the unique key,
        so an update is performed, if no records are updated, then the record
        is inserted. If the record is inserted, the recipient phone id is returned.

Input:
        recipient_id                    number
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state_province                  varchar2
        postal_code                     varchar2
        country                         varchar2
        county                          varchar2
        firm_name                       varchar2
        geofind_match_code              varchar2
        override_flag                   varchar2
        latitude                        varchar2
        longitude                       varchar2
        range_record_type               varchar2

Output:
        qms_id                          number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

SELECT  QMS_ID
INTO    OUT_QMS_ID
FROM    QMS_ADDRESSES
WHERE   RECIPIENT_ID = IN_RECIPIENT_ID;

UPDATE_QMS_ADDRESSES
(
        OUT_QMS_ID,
        IN_RECIPIENT_ID,
        IN_ADDRESS_LINE_1,
        IN_ADDRESS_LINE_2,
        IN_CITY,
        IN_STATE_PROVINCE,
        IN_POSTAL_CODE,
        IN_COUNTRY,
        IN_COUNTY,
        IN_FIRM_NAME,
        IN_GEOFIND_MATCH_CODE,
        IN_OVERRIDE_FLAG,
        IN_LATITUDE,
        IN_LONGITUDE,
        IN_RANGE_RECORD_TYPE,
        OUT_STATUS,
        OUT_MESSAGE
);

EXCEPTION WHEN NO_DATA_FOUND THEN
        -- no record was found using the unique key, so insert the record
        INSERT_QMS_ADDRESSES
        (
                IN_RECIPIENT_ID,
                IN_ADDRESS_LINE_1,
                IN_ADDRESS_LINE_2,
                IN_CITY,
                IN_STATE_PROVINCE,
                IN_POSTAL_CODE,
                IN_COUNTRY,
                IN_COUNTY,
                IN_FIRM_NAME,
                IN_GEOFIND_MATCH_CODE,
                IN_OVERRIDE_FLAG,
                IN_LATITUDE,
                IN_LONGITUDE,
                IN_RANGE_RECORD_TYPE,
                OUT_QMS_ID,
                OUT_STATUS,
                OUT_MESSAGE
        );

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_UNIQUE_QMS_ADDRESSES;

END RECIPIENTS_MAINT_PKG;
.
/
