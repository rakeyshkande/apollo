CREATE OR REPLACE
function scrub.REINSTATE_TO_DISPATCHER (
       IN_EXTERNAL_ORDER_NUMBER IN ORDER_DETAILS.ORDER_GUID%TYPE
) return varchar2 is
PRAGMA AUTONOMOUS_TRANSACTION;
/*-----------------------------------------------------------------------------
    The purpose of this function is to:
    1.  Determine if the order exists for the external order number passed
    2.  Update the header record status to 1004
    3.  Update the detail record status to 2007
    4.  Insert a record into the order exceptions table for the item

    The function will return a string of 'OK' upon success or the error
    text if the function fails.

    To run inside of DbVisualizer:
    select SCRUB.REINSTATE_TO_DISPATCHER('<external order number>') from dual;
-----------------------------------------------------------------------------*/
  v_result varchar2(256);
  v_order_guid ORDER_DETAILS.ORDER_GUID%TYPE;

begin
  BEGIN
    --check for the existance of the order
    SELECT order_guid
      INTO v_order_guid
      FROM ORDER_DETAILS
      WHERE EXTERNAL_ORDER_NUMBER=IN_EXTERNAL_ORDER_NUMBER AND ROWNUM=1;

    --update the header record
    UPDATE ORDERS SET STATUS = '1004' WHERE ORDER_GUID = v_order_guid;

    --update the detail record
    UPDATE ORDER_DETAILS SET STATUS = '2007' WHERE EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

    --insert into the order exceptions table
    INSERT INTO ORDER_EXCEPTIONS (CONTEXT,MESSAGE,ERROR,TIMESTAMP,ORDER_GUID)
      SELECT
        'ORDER DISPATCHER QUEUE/ES1004',
        ORDER_DETAILS.ORDER_GUID||'/'||ORDER_DETAILS.LINE_NUMBER,
        'Manual Entry',
        SYSDATE,
        ORDER_DETAILS.ORDER_GUID 
      FROM ORDER_DETAILS
      WHERE
        EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;

    COMMIT;
    v_result := 'OK';
  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_result := 'Order detail for: ' || IN_EXTERNAL_ORDER_NUMBER || ' could not be found.';
    ROLLBACK;
  WHEN OTHERS THEN
    v_result := SUBSTR (SQLERRM,1,256);
    ROLLBACK;
  END;
  return(v_result);
end REINSTATE_TO_DISPATCHER;
.
/
