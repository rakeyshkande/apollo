CREATE OR REPLACE
PACKAGE BODY global.INVENTORY_MAINT_PKG
AS


PROCEDURE INCREMENT_PRODUCT_INVENTORY
(
 IN_PRODUCT_ID       IN FTD_APPS.INVENTORY_CONTROL.PRODUCT_ID%TYPE,
 IN_VENDOR_ID        IN FTD_APPS.INVENTORY_CONTROL.VENDOR_ID%TYPE,
 IN_INCREASE_AMOUNT  IN NUMBER,
 OUT_STATUS         OUT VARCHAR2,
 OUT_ERROR_MESSAGE  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure increases the inventory_level of a record in
          inventory_control for the product_id and vendor_id passed in.

Input:
        product_id       VARCHAR2
        vendor_id        VARCHAR2
        increase_amount  NUMBER

Output:
        status
        error message

-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

  UPDATE ftd_apps.inventory_control
     SET inventory_level = NVL(inventory_level,0) + in_increase_amount
   WHERE product_id = in_product_id and vendor_id = in_vendor_id;
  COMMIT;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END INCREMENT_PRODUCT_INVENTORY;


PROCEDURE DECREMENT_PRODUCT_INVENTORY
(
 IN_PRODUCT_ID                IN FTD_APPS.INVENTORY_CONTROL.PRODUCT_ID%TYPE,
 IN_VENDOR_ID                 IN FTD_APPS.INVENTORY_CONTROL.VENDOR_ID%TYPE,
 IN_DECREASE_AMOUNT           IN NUMBER,
 OUT_INVENTORY_LEVEL_REACHED OUT VARCHAR2,
 OUT_STATUS                  OUT VARCHAR2,
 OUT_ERROR_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure decreases the inventory_level of a record in
          inventory_control for the product_id passed in, it allows passes
          back a flag stating if the inventory level has been reached.

Input:
        product_id       VARCHAR2
        vendor_id        VARCHAR2
        decrease_amount  NUMBER

Output:
        flag             CHAR
        status           VARCHAR2
        error message    VARCHAR2

-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;
v_updateCount INTEGER;
BEGIN

  UPDATE ftd_apps.inventory_control
     SET inventory_level = NVL(inventory_level,0) - in_decrease_amount
   WHERE product_id = in_product_id and vendor_id = in_vendor_id;
  v_updateCount := SQL%ROWCOUNT;
  COMMIT;

  IF v_updateCount > 0 THEN
     BEGIN
       SELECT 'Y'
         INTO out_inventory_level_reached
         FROM ftd_apps.inventory_control
        WHERE product_id = in_product_id
          AND vendor_id = in_vendor_id
          AND inventory_level <= notice_threshold;

        EXCEPTION WHEN NO_DATA_FOUND THEN out_inventory_level_reached := 'N';
     END;
  END IF;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DECREMENT_PRODUCT_INVENTORY;


PROCEDURE UPDATE_INSERT_PRICE_HEADER
(
 IN_PRICE_HEADER_ID  IN VARCHAR2,
 IN_DESCRIPTION      IN VARCHAR2,
 IN_UPDATED_BY			IN VARCHAR2,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure update/inserts a record into table price_header.

Input:
        price_header_id  VARCHAR2
        description      VARCHAR2
        updated_by		 VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE ftd_apps.price_header
     SET description = in_description,
     	 	updated_by = UPPER(in_updated_by),
     	 	updated_on = SYSDATE
   WHERE price_header_id = in_price_header_id;

  IF SQL%NOTFOUND THEN
    INSERT INTO ftd_apps.price_header
       (price_header_id,
        description,
        created_by,
        created_on,
        updated_by,
        updated_on)
      VALUES
       (in_price_header_id,
        in_description,
        UPPER(in_updated_by),
        SYSDATE,
        UPPER(in_updated_by),
        SYSDATE);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INSERT_PRICE_HEADER;


PROCEDURE INSERT_PRICE_HEADER_DETAILS
(
 IN_PRICE_HEADER_ID IN VARCHAR2,
 IN_MIN_DOLLAR_AMT  IN NUMBER,
 IN_MAX_DOLLAR_AMT  IN NUMBER,
 IN_DISCOUNT_TYPE    IN VARCHAR2,
 IN_DISCOUNT_AMT    IN NUMBER,
 IN_CREATED_BY		  IN VARCHAR2,
 OUT_STATUS        OUT VARCHAR2,
 OUT_MESSAGE       OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table price_header_details.

Input:
        price_header_id VARCHAR2
        min_dollar_amt  NUMBER
        max_dollar_amt  NUMBER
        discount_type   VARCHAR2
        discount_amt    NUMBER
        created_by		VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  INSERT INTO ftd_apps.price_header_details
    (price_header_id,
     min_dollar_amt,
     max_dollar_amt,
     discount_type,
     discount_amt,
     created_by,
     created_on,
     updated_by,
     updated_on)
    VALUES
    (in_price_header_id,
     in_min_dollar_amt,
     in_max_dollar_amt,
     in_discount_type,
     in_discount_amt,
     UPPER(in_created_by),
     SYSDATE,
     UPPER(in_created_by),
     SYSDATE);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_PRICE_HEADER_DETAILS;


PROCEDURE DELETE_PRICE_HEADER_DETAILS
(
 IN_PRICE_HEADER_ID IN VARCHAR2,
 IN_UPDATED_BY		  IN VARCHAR2,
 OUT_STATUS        OUT VARCHAR2,
 OUT_MESSAGE       OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table price_header_details.

Input:
        price_header_id VARCHAR2
        updated_by		VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE ftd_apps.price_header_details
  SET		updated_by = UPPER(in_updated_by) || ' - DELETE',
  			updated_on = SYSDATE
  WHERE	price_header_id = in_price_header_id;
  
  DELETE FROM ftd_apps.price_header_details
    WHERE price_header_id = in_price_header_id;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_PRICE_HEADER_DETAILS;


PROCEDURE INSERT_INVENTORY_CONTROL
(
 IN_PRODUCT_ID          IN FTD_APPS.INVENTORY_CONTROL.PRODUCT_ID%TYPE,
 IN_VENDOR_ID           IN FTD_APPS.INVENTORY_CONTROL.VENDOR_ID%TYPE,
 IN_CONTACT_NUMBER      IN FTD_APPS.INVENTORY_CONTROL.CONTACT_NUMBER%TYPE,
 IN_INVENTORY_LEVEL     IN FTD_APPS.INVENTORY_CONTROL.INVENTORY_LEVEL%TYPE,
 IN_NOTICE_THRESHOLD    IN FTD_APPS.INVENTORY_CONTROL.NOTICE_THRESHOLD%TYPE,
 IN_START_DATE          IN FTD_APPS.INVENTORY_CONTROL.START_DATE%TYPE,
 IN_END_DATE            IN FTD_APPS.INVENTORY_CONTROL.END_DATE%TYPE,
 IN_CONTACT_EXTENSION   IN FTD_APPS.INVENTORY_CONTROL.CONTACT_EXTENSION%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table inventory_control.

Input:
       product_id        VARCHAR2
       vendor_id         VARCHAR2
       contact_number    VARCHAR2
       inventory_level   NUMBER
       notice_threshold  NUMBER
       start_date        DATE
       end_date          DATE
       contact_extension VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  INSERT INTO ftd_apps.inventory_control
    (product_id,
     vendor_id,
     contact_number,
     inventory_level,
     notice_threshold,
     start_date,
     end_date,
     contact_extension,
     updated_by,
     updated_on)
    VALUES
    (in_product_id,
     in_vendor_id,
     in_contact_number,
     in_inventory_level,
     in_notice_threshold,
     in_start_date,
     in_end_date,
     in_contact_extension,
     USER,
     SYSDATE);

  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
  UPDATE ftd_apps.inventory_control
     SET contact_number = in_contact_number,
         inventory_level = in_inventory_level,
         notice_threshold = in_notice_threshold,
         start_date = in_start_date,
         end_date = in_end_date,
         contact_extension = in_contact_extension,
         updated_by = USER,
         updated_on = SYSDATE
    WHERE product_id = in_product_id and vendor_id = in_vendor_id;
  WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_INVENTORY_CONTROL;


PROCEDURE UPDATE_INVENTORY_CONTROL
(
 IN_PRODUCT_ID          IN FTD_APPS.INVENTORY_CONTROL.PRODUCT_ID%TYPE,
 IN_VENDOR_ID           IN FTD_APPS.INVENTORY_CONTROL.VENDOR_ID%TYPE,
 IN_CONTACT_NUMBER      IN FTD_APPS.INVENTORY_CONTROL.CONTACT_NUMBER%TYPE,
 IN_INVENTORY_LEVEL     IN FTD_APPS.INVENTORY_CONTROL.INVENTORY_LEVEL%TYPE,
 IN_NOTICE_THRESHOLD    IN FTD_APPS.INVENTORY_CONTROL.NOTICE_THRESHOLD%TYPE,
 IN_START_DATE          IN FTD_APPS.INVENTORY_CONTROL.START_DATE%TYPE,
 IN_END_DATE            IN FTD_APPS.INVENTORY_CONTROL.END_DATE%TYPE,
 IN_CONTACT_EXTENSION   IN FTD_APPS.INVENTORY_CONTROL.CONTACT_EXTENSION%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record into table inventory_control.

Input:
       product_id        VARCHAR2
       vendor_id         VARCHAR2
       contact_number    VARCHAR2
       inventory_level   NUMBER
       notice_threshold  NUMBER
       start_date        DATE
       end_date          DATE
       contact_extension VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;
v_updateCount INTEGER;
BEGIN

  UPDATE ftd_apps.inventory_control
     SET contact_number = in_contact_number,
         inventory_level = in_inventory_level,
         notice_threshold = in_notice_threshold,
         start_date = in_start_date,
         end_date = in_end_date,
         contact_extension = in_contact_extension,
         updated_by = USER,
         updated_on = SYSDATE
    WHERE product_id = in_product_id and vendor_id = in_vendor_id;
  v_updateCount := SQL%ROWCOUNT;
  COMMIT;

  IF v_updateCount > 0 THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_message := 'WARNING: No inventory_control updated for product_id ' || in_product_id ||'.';
    END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_INVENTORY_CONTROL;


PROCEDURE DELETE_INVENTORY_CONTROL
(
 IN_PRODUCT_ID          IN FTD_APPS.INVENTORY_CONTROL.PRODUCT_ID%TYPE,
 IN_VENDOR_ID           IN FTD_APPS.INVENTORY_CONTROL.VENDOR_ID%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table inventory_control.

Input:
       product_id        VARCHAR2
       vendor_id         VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/
BEGIN

  DELETE FROM ftd_apps.inventory_control
    WHERE product_id = in_product_id and vendor_id = in_vendor_id;

  IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_message := 'WARNING: No inventory_control deleted for product_id ' || in_product_id ||' vendor_id '|| in_vendor_id ||'.';
    END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END DELETE_INVENTORY_CONTROL;


END INVENTORY_MAINT_PKG;
.
/
