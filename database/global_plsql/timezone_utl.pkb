CREATE OR REPLACE
PACKAGE BODY global.timezone_utl
IS
    PROCEDURE timezone
       (local_timezone OUT VARCHAR2)
    IS EXTERNAL
       LIBRARY timezone_utl_l
       NAME "get_timezone"
       LANGUAGE C
       PARAMETERS
          (local_timezone STRING,
           local_timezone MAXLEN sb4,
           local_timezone INDICATOR sb2);
   FUNCTION timezone RETURN VARCHAR2 IS
      local_timezone VARCHAR2(3);
   BEGIN
      timezone(local_timezone);
      return local_timezone;
   END timezone;
END timezone_utl;
.
/
