CREATE OR REPLACE function global.MD5_HASH
(
IN_STRING        VARCHAR2
)

RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        Do an MD5 hash on the input.
        Used for values where one-way hashing (not encryption) is appropriate.

Input:
        plain text string to be hashed

Output:
        hashed string

Calls:
        DBMS_OBFUSCATION_TOOLKIT.MD5 to hash the string
-----------------------------------------------------------------------------*/

HASHED_STRING          VARCHAR2(16);

BEGIN

HASHED_STRING := DBMS_OBFUSCATION_TOOLKIT.MD5 (INPUT_STRING => IN_STRING);

RETURN HASHED_STRING;

END;
/
grant execute on global.md5_hash to osp;
