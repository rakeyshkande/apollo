CREATE OR REPLACE
PACKAGE BODY global.UTL_PKG AS


FUNCTION DATE_HALF_HOUR (MINUTES IN VARCHAR2) RETURN VARCHAR2
/*------------------------------------------------------------------------------
Description:
   Take a number (character form), representing minutes, and return either
   a 00 or 30.  Do no error-handling or input-checking.
------------------------------------------------------------------------------*/

IS

  RETVALUE VARCHAR2(2);

BEGIN

  IF ( MINUTES < 30 ) THEN
    RETVALUE := '00';
  ELSE
    RETVALUE := '30';
  END IF;

  RETURN RETVALUE;

END DATE_HALF_HOUR;




END UTL_PKG;
.
/
