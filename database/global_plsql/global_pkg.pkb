CREATE OR REPLACE PACKAGE BODY GLOBAL.GLOBAL_PKG AS

PROCEDURE GET_AAA_MEMBER (
   IN_MEMBER_ID         IN NUMBER,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return billing options for the given source code.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_AAA_MEMBER (IN_MEMBER_ID);

END GET_AAA_MEMBER;


PROCEDURE GET_ADDON_BY_ID (
   IN_ADDON_ID   IN  FTD_APPS.ADDON.ADDON_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns codified add on information for the given add on id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT ADDON_ID,
          ADDON_TYPE,
          DESCRIPTION,
          PRICE,
          ADDON_TEXT,
          UNSPSC
   FROM   FTD_APPS.ADDON
   WHERE  ADDON_ID = IN_ADDON_ID;

END GET_ADDON_BY_ID;


PROCEDURE GET_ADDON_BY_OCCASION (
   IN_OCCASION_ID       IN NUMBER,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns codified add on information for the given occasion id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_ADDON_BY_OCCASION (IN_OCCASION_ID);

END GET_ADDON_BY_OCCASION;


PROCEDURE GET_ADDON_BY_TYPE_OCCASION (
   IN_ADDON_TYPE        IN VARCHAR2,
   IN_OCCASION_ID       IN VARCHAR2,
   IN_ADDON_ID          IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns codified add on information for the type, occassion and id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_ADDON_BY_TYPE_OCCASION (IN_ADDON_TYPE, IN_OCCASION_ID, IN_ADDON_ID );

END GET_ADDON_BY_TYPE_OCCASION;


PROCEDURE GET_ADDONS_BY_TYPE_OCCASION (
   IN_ADDON_TYPE        IN VARCHAR2,
   IN_OCCASION_ID       IN VARCHAR2,
   IN_ADDON_ID          IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns codified add on information for the type, occassion and id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_ADDONS_BY_TYPE_OCCASION (IN_ADDON_TYPE, IN_OCCASION_ID, IN_ADDON_ID );
END GET_ADDONS_BY_TYPE_OCCASION;


PROCEDURE GET_BILLING_INFO (
   IN_SOURCE_CODE_ID IN FTD_APPS.BILLING_INFO.SOURCE_CODE_ID%TYPE,
   OUT_CUR           OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns billing information for the given source code id, does not return
   records with a description of CERTIFICATE#.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT BILLING_INFO_SEQUENCE,
          INFO_DESCRIPTION,
          INFO_DISPLAY,
          PROMPT_TYPE
   FROM   FTD_APPS.BILLING_INFO
   WHERE  SOURCE_CODE_ID = IN_SOURCE_CODE_ID
   AND    INFO_DESCRIPTION <> 'CERTIFICATE#';

END GET_BILLING_INFO;


PROCEDURE GET_BILLING_INFO_CERTIFICATE (
   IN_SOURCE_CODE_ID IN FTD_APPS.BILLING_INFO.SOURCE_CODE_ID%TYPE,
   OUT_CUR           OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns Y the given source code id and a description
   of CERTIFICATE# if it exists.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT 'Y' CERTIFICATE_EXISTS
   FROM   FTD_APPS.BILLING_INFO
   WHERE  SOURCE_CODE_ID = IN_SOURCE_CODE_ID
   AND    INFO_DESCRIPTION = 'CERTIFICATE#';

END GET_BILLING_INFO_CERTIFICATE;


PROCEDURE GET_BILLING_INFO_OPTIONS (
   IN_SOURCE_CODE_ID IN VARCHAR2,
   OUT_CUR           OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return billing options for the given source code.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_BILLING_INFO_OPTIONS (IN_SOURCE_CODE_ID);

END GET_BILLING_INFO_OPTIONS;


PROCEDURE GET_CALL_CENTER (
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all call centers.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT CALL_CENTER_ID,
          DESCRIPTION
   FROM   FTD_APPS.CALL_CENTER;

END GET_CALL_CENTER;


PROCEDURE GET_CARRIER_INFO_LIST
(
   OUT_CUR       OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Return all carrier_info records.
------------------------------------------------------------------------------*/
BEGIN

   out_cur := ftd_apps.sp_get_carrier_list();

END GET_CARRIER_INFO_LIST;


PROCEDURE GET_CODIFIED_PRODUCTS_BY_ID (
   IN_PRODUCT_ID IN  FTD_APPS.CODIFIED_PRODUCTS.PRODUCT_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns codified product information for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT CHECK_OE_FLAG,
          CODIFIED_SPECIAL
   FROM   FTD_APPS.CODIFIED_PRODUCTS
   WHERE  PRODUCT_ID = IN_PRODUCT_ID;

END GET_CODIFIED_PRODUCTS_BY_ID;


PROCEDURE GET_COLORS_LIST (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all product colors information.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_COLORS_LIST ();

END GET_COLORS_LIST;


PROCEDURE GET_COLOR_BY_DESCRIPTION (
   IN_DESCRIPTION       IN FTD_APPS.COLOR_MASTER.DESCRIPTION%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return  color id for the given description.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  COLOR_MASTER_ID
        FROM    FTD_APPS.COLOR_MASTER
        WHERE   DESCRIPTION = IN_DESCRIPTION;

END GET_COLOR_BY_DESCRIPTION;


PROCEDURE GET_COMPANY
(
   OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        This stored procedure retrieves all the company names from table company.

Input:
        company id

Output:
        cursor containing company information for dnis maintenance
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cur FOR
     SELECT company_name,
            company_id
       FROM ftd_apps.company;

END GET_COMPANY;


PROCEDURE GET_COMPANY_ORIGIN_MAPPING
(
IN_COMPANY_ID                   IN FTD_APPS.COMPANY_ORIGIN_MAPPING.COMPANY_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning company origin mapping
        info for the given company id
Input:
        company_id                      varchar2
Output:
        cursor result set containing company origin mapping info
-----------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
        SELECT
                COMPANY_ID,
                ORIGIN
        FROM    FTD_APPS.COMPANY_ORIGIN_MAPPING
        WHERE   COMPANY_ID = IN_COMPANY_ID;

END GET_COMPANY_ORIGIN_MAPPING;


PROCEDURE GET_COST_CENTER_ID (
   IN_COST_CENTER_ID    VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return cost center id if it exists.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_COST_CENTER_ID (IN_COST_CENTER_ID);

END GET_COST_CENTER_ID;


PROCEDURE GET_COUNTRY_MASTER_BY_ID (
   IN_COUNTRY_ID IN FTD_APPS.COUNTRY_MASTER.COUNTRY_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return country master information for the given country id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_COUNTRY_INFO (IN_COUNTRY_ID);

END GET_COUNTRY_MASTER_BY_ID;


PROCEDURE GET_COUNTRY_DELIVERY_BY_ID (
   IN_COUNTRY_ID        IN FTD_APPS.COUNTRY_MASTER.COUNTRY_ID%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the complete list of countries.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT MONDAY_CLOSED,
          TUESDAY_CLOSED,
          WEDNESDAY_CLOSED,
          THURSDAY_CLOSED,
          FRIDAY_CLOSED,
          SATURDAY_CLOSED,
          SUNDAY_CLOSED
   FROM   FTD_APPS.COUNTRY_MASTER
   WHERE  COUNTRY_ID = IN_COUNTRY_ID;

END GET_COUNTRY_DELIVERY_BY_ID;


PROCEDURE GET_COUNTRY_LIST (
   OUT_COUNTRY_LIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the complete list of countries.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_COUNTRY_LIST FOR
   SELECT COUNTRY_ID,
          NAME,
          OE_COUNTRY_TYPE,
          OE_DISPLAY_ORDER,
          ADD_ON_DAYS,
          CUTOFF_TIME
   FROM   FTD_APPS.COUNTRY_MASTER
   WHERE  COUNTRY_ID NOT IN ('00','01')
   ORDER BY OE_DISPLAY_ORDER, NAME;

END GET_COUNTRY_LIST;


PROCEDURE GET_COUNTRY_LIST_BY_SOURCECODE (
   IN_SOURCE_CODE  IN  FTD_APPS.SOURCE.SOURCE_CODE%TYPE,
   OUT_COUNTRY_LIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the complete list of countries.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_COUNTRY_LIST FOR
   SELECT  DISTINCT
                  cm.COUNTRY_ID,
          cm.NAME,
          cm.OE_COUNTRY_TYPE,
          cm.OE_DISPLAY_ORDER,
          cm.ADD_ON_DAYS,
          cm.CUTOFF_TIME
        FROM
                FTD_APPS.SCRIPTING_COUNTRY_XREF scx,
                FTD_APPS.COUNTRY_MASTER cm,
                FTD_APPS.SOURCE src
        WHERE
        src.source_code=IN_SOURCE_CODE
        AND cm.country_id not in ('00', '01')
        AND cm.country_id = scx.country_id (+)
        AND ( (src.jcpenney_flag='Y' and scx.scripting_code='JP') OR
                scx.company_id = src.company_id OR
            (NOT EXISTS (SELECT * FROM FTD_APPS.scripting_country_xref WHERE COMPANY_ID = src.company_id)))
        ORDER BY
                oe_display_order, name;

END GET_COUNTRY_LIST_BY_SOURCECODE;


PROCEDURE GET_CPC_INFO (
   IN_SOURCE_CODE_ID    IN FTD_APPS.CPC_INFO.SOURCE_CODE_ID%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all cs groups.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
           SELECT SOURCE_CODE_ID,
                  CREDIT_CARD_TYPE,
                  ENCRYPTION.DECRYPT_IT (CREDIT_CARD_NUMBER,key_name) CREDIT_CARD_NUMBER,
                  CREDIT_CARD_EXPIRATION_DATE
           FROM   FTD_APPS.CPC_INFO
           WHERE  SOURCE_CODE_ID = IN_SOURCE_CODE_ID;

END GET_CPC_INFO;



PROCEDURE GET_CS_GROUPS (
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all cs groups.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT CS_GROUP_ID,
          DESCRIPTION
   FROM   FTD_APPS.CS_GROUPS;

END GET_CS_GROUPS;


PROCEDURE GET_CSZ_AVAIL_BY_ZIP_CODE (
   IN_CSZ_ZIP_CODE IN  FTD_APPS.CSZ_AVAIL.CSZ_ZIP_CODE%TYPE,
   OUT_CUR         OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns zip code information for the given zip code.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT CSZ_ZIP_CODE,
          GNADD_FLAG,
          SUNDAY_DELIVERY_FLAG,
          LATEST_CUTOFF
   FROM   FTD_APPS.CSZ_AVAIL
   WHERE CSZ_ZIP_CODE = IN_CSZ_ZIP_CODE;

END GET_CSZ_AVAIL_BY_ZIP_CODE;


PROCEDURE GET_CSZ_PRODUCT_AVAILABLE_FLAG (
   IN_PRODUCT_ID IN  FTD_APPS.CSZ_PRODUCTS.PRODUCT_ID%TYPE,
   IN_ZIP_CODE   IN  FTD_APPS.CSZ_PRODUCTS.ZIP_CODE%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns the product available flag for the given product id and zip code.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT AVAILABLE_FLAG,
          PRODUCT_CUTOFF
   FROM FTD_APPS.CSZ_PRODUCTS
   WHERE PRODUCT_ID = IN_PRODUCT_ID
   AND ZIP_CODE = IN_ZIP_CODE;

END GET_CSZ_PRODUCT_AVAILABLE_FLAG;


PROCEDURE GET_DELIVERY_DATE_RANGES (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all active delivery date ranges.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_DELIVERY_DATE_RANGES;

END GET_DELIVERY_DATE_RANGES;

PROCEDURE GET_DEPARTMENTS (
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all cs groups.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT DEPARTMENT_ID,
          DESCRIPTION
   FROM   FTD_APPS.DEPARTMENTS;

END GET_DEPARTMENTS;


PROCEDURE GET_DNIS (
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns dnis based on the paging positions
------------------------------------------------------------------------------*/
BEGIN

   SELECT COUNT (*)
   INTO   OUT_ID_COUNT
   FROM   FTD_APPS.DNIS;

   OPEN OUT_CUR FOR
        SELECT *
        FROM
        (
                SELECT A.*, ROWNUM RN
                FROM
                (
                   SELECT DNIS_ID,
                          DESCRIPTION,
                          ORIGIN,
                          STATUS_FLAG,
                          OE_FLAG,
                          YELLOW_PAGES_FLAG,
                          SCRIPT_CODE,
                          DEFAULT_SOURCE_CODE,
                          SCRIPT_ID
                   FROM   FTD_APPS.DNIS
                   ORDER BY DNIS_ID
                ) A
                WHERE ROWNUM <= in_start_position+in_max_number_returned-1
        )
        WHERE RN >= in_start_position;

END GET_DNIS;


PROCEDURE GET_DNIS_BY_ID (
   IN_DNIS_ID IN  FTD_APPS.DNIS.DNIS_ID%TYPE,
   OUT_CUR    OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns dnis by the given id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT DNIS_ID,
          DESCRIPTION,
          ORIGIN,
          STATUS_FLAG,
          OE_FLAG,
          YELLOW_PAGES_FLAG,
          SCRIPT_CODE,
          DEFAULT_SOURCE_CODE,
          SCRIPT_ID
   FROM   FTD_APPS.DNIS
   WHERE  DNIS_ID = IN_DNIS_ID;

END GET_DNIS_BY_ID;

PROCEDURE GET_DNIS_PHONE_NUMBER (
   IN_DNIS_ID IN  FTD_APPS.DNIS.DNIS_ID%TYPE,
   OUT_CUR    OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns company name, dnis phone number, o/e flag by the given id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT d.DNIS_ID,
          d.OE_FLAG,
          (select cm.phone_number from ftd_apps.company_master cm join ftd_apps.source s on cm.company_id = s.company_id where s.source_code = d.default_source_code) phone_number,
          (select cm.company_name from ftd_apps.company_master cm join ftd_apps.source s on cm.company_id = s.company_id where s.source_code = d.default_source_code) company_name,
          d.status_flag
   FROM   FTD_APPS.DNIS d
   WHERE  d.DNIS_ID = IN_DNIS_ID;

END GET_DNIS_PHONE_NUMBER;

PROCEDURE GET_DNIS_SCRIPT_CODE
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Returns all dnis script codes
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                script_code,
                description
        FROM    FTD_APPS.dnis_script_code;

END GET_DNIS_SCRIPT_CODE;


PROCEDURE GET_EMAIL_COMPANY_DATA (
   IN_COMPANY_ID        FTD_APPS.EMAIL_COMPANY_DATA.COMPANY_ID%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns email data for the specified company.  If the specified company,
   does not have any
------------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
   SELECT NAME,
          VALUE
   FROM   FTD_APPS.EMAIL_COMPANY_DATA
   WHERE  COMPANY_ID = COALESCE ((SELECT DISTINCT COMPANY_ID
                                 FROM FTD_APPS.EMAIL_COMPANY_DATA
                                 WHERE COMPANY_ID = IN_COMPANY_ID), 'FTD');

END GET_EMAIL_COMPANY_DATA;



PROCEDURE GET_FLORIST_LIST (
   IN_CITY          IN  VARCHAR2,
   IN_STATE         IN  VARCHAR2,
   IN_ZIP           IN  VARCHAR2,
   IN_FLORIST_NAME  IN  VARCHAR2,
   IN_PHONE_NUMBER  IN  VARCHAR2,
   IN_ADDRESS       IN  VARCHAR2,
   IN_PRODUCT_ID    IN  VARCHAR2,
   OUT_FLORIST_LIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   This is an overloaded proc that calls GET_FLORIST_LIST with a null source code
------------------------------------------------------------------------------*/

BEGIN

      GLOBAL.GLOBAL_PKG.GET_FLORIST_LIST 
        (
          IN_CITY,
          IN_STATE,
          IN_ZIP,
          IN_FLORIST_NAME,
          IN_PHONE_NUMBER,
          IN_ADDRESS,
          IN_PRODUCT_ID,
          null,
          null,
          null,
          OUT_FLORIST_LIST
        );

END GET_FLORIST_LIST;


PROCEDURE GET_FLORIST_LIST (
   IN_CITY          IN  VARCHAR2,
   IN_STATE         IN  VARCHAR2,
   IN_ZIP           IN  VARCHAR2,
   IN_FLORIST_NAME  IN  VARCHAR2,
   IN_PHONE_NUMBER  IN  VARCHAR2,
   IN_ADDRESS       IN  VARCHAR2,
   IN_PRODUCT_ID    IN  VARCHAR2,
   IN_SOURCE_CODE   IN  FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
   IN_DELIVERY_DATE IN  DATE,
   IN_DELIVERY_DATE_END IN DATE,
   OUT_FLORIST_LIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Queries the FLORIST_MASTER and FLORIST_ZIPS tables for florists who cover
   the supplied zip or deliver to the supplied city/state (if zip gets no hits).
   Returns ref cursor to row set with basic florist info, also a florist blocked
   flag that indicates if florist is currently blocked.

NOTE:  OE_GET_FLORIST_LIST_BULK IS A TEMPORARY SOLUTION FOR SCRUB BULK ORDERS
FOR THE HP REPLACEMENT PHASE II PROJECT.  THE ONLY DIFFERENCE BETWEEN
OE_GET_FLORIST_LIST_BULK AND OE_GET_FLORIST_LIST IS THE CALL TO
OE_GET_FLORIST_PRODUCTS_BULK AND OE_GET_FLORIST_PRODUCTS RESPECTIVELY.
WHEN OE_GET_FLORIST_PRODUCTS IS MODIFIED TO HANDLE A VARCHAR2 GREATER THAN 255,
OE_GET_FLORIST_LIST_BULK SHOULD BE DROPPED AND THE ORIGINAL OE_GET_FLORIST_LIST
PROC SHOULD BE USED.
------------------------------------------------------------------------------*/
BEGIN

   OUT_FLORIST_LIST :=
      FTD_APPS.OE_GET_FLORIST_LIST_BULK (
         inCity      => IN_CITY,
         inState     => IN_STATE,
         inZip       => IN_ZIP,
         floristName => IN_FLORIST_NAME,
         phoneNumber => IN_PHONE_NUMBER,
         inAddress   => IN_ADDRESS,
         productId   => IN_PRODUCT_ID,
         inSourceCode  => IN_SOURCE_CODE,
         inDeliveryDate => IN_DELIVERY_DATE,
         inDeliveryDateEnd => IN_DELIVERY_DATE_END);

END GET_FLORIST_LIST;


PROCEDURE GET_FLOWER_LIST (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return id and name from flowers.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_FLOWER_LIST ();

END GET_FLOWER_LIST;


PROCEDURE GET_GIFT_CERTIFICATE (
   IN_GIFT_CERTIFICATE_ID IN VARCHAR2,
   OUT_CUR OUT TYPES.REF_CURSOR
   )
AS
/*------------------------------------------------------------------------------
Description:
   Return gift certificate record for the given id
------------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
SELECT  gcc.gc_coupon_number gift_certificate_id,
        gcc.issue_amount certificate_amount,
        gcc.expiration_date,
        gcc.issue_date,
        decode (gcc.gc_coupon_status, 'Redeemed', 'Y', 'N') redemption_flag,
        gcr.company_id
FROM    clean.gc_coupons gcc
JOIN    clean.gc_coupon_request gcr
ON      gcc.request_number = gcr.request_number
WHERE   upper (gcc.gc_coupon_number) = upper (in_gift_certificate_id);

END GET_GIFT_CERTIFICATE;


PROCEDURE GET_GLOBAL_PARMS (
   OUT_CUR OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all rows in the global parms table.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_GLOBAL_PARMS;

END GET_GLOBAL_PARMS;


PROCEDURE GET_HOLIDAYS_BY_COUNTRY (
   IN_COUNTRY_ID    IN VARCHAR2,
   OUT_CUR           OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return holidays for the given country id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_HOLIDAYS_BY_COUNTRY (IN_COUNTRY_ID);

END GET_HOLIDAYS_BY_COUNTRY;


PROCEDURE GET_INSTITUTION (
IN_INSTITUTION_NAME     IN VARCHAR2,
IN_PHONE_NUMBER         IN VARCHAR2,
IN_ADDRESS              IN VARCHAR2,
IN_CITY                 IN VARCHAR2,
IN_STATE                IN VARCHAR2,
IN_ZIP                  IN VARCHAR2,
IN_INSTITUTION_TYPE     IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR
) AS
/*------------------------------------------------------------------------------
Description:
   Searches for the institution from the given criteria
------------------------------------------------------------------------------*/

BEGIN

OUT_CUR := FTD_APPS.OE_GET_INSTITUTION (
          IN_INSTITUTION_NAME,
          IN_PHONE_NUMBER,
          IN_ADDRESS,
          IN_CITY,
          IN_STATE,
          IN_ZIP,
          IN_INSTITUTION_TYPE);

END GET_INSTITUTION;


PROCEDURE GET_INSTITUTION_TYPES (
   OUT_INSTITUTION_TYPE_LIST OUT TYPES.REF_CURSOR) IS
BEGIN

   OUT_INSTITUTION_TYPE_LIST := FTD_APPS.OE_GET_INSTITUTION_TYPES;

END GET_INSTITUTION_TYPES;



PROCEDURE GET_MASTER_ORDER_NUMBER (
   OUT_MASTER_ORDER_NUMBER OUT NUMBER) AS
/*------------------------------------------------------------------------------
Description:
   Return a new master order number.
------------------------------------------------------------------------------*/
BEGIN

   SELECT FTD_APPS.OE_ORDER_ID.NEXTVAL
   INTO   OUT_MASTER_ORDER_NUMBER
   FROM   DUAL;

END GET_MASTER_ORDER_NUMBER;


PROCEDURE GET_OCCASION (
   IN_OCCASION_ID    IN  VARCHAR2,
   OUT_OCCASION_LIST OUT TYPES.REF_CURSOR) IS
BEGIN

   OUT_OCCASION_LIST := FTD_APPS.OE_GET_OCCASION (IN_OCCASION_ID);

END GET_OCCASION;


PROCEDURE GET_OCCASION_BY_DESCRIPTION (
   IN_DESCRIPTION       IN FTD_APPS.OCCASION.DESCRIPTION%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the occasion id for the given occasion name.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
   SELECT OCCASION_ID
   FROM   FTD_APPS.OCCASION
   WHERE  DESCRIPTION = IN_DESCRIPTION;

END GET_OCCASION_BY_DESCRIPTION;


PROCEDURE GET_OCCASION_LIST (
   OUT_CUR OUT TYPES.REF_CURSOR) IS
/*------------------------------------------------------------------------------
Description:
   Return all occasions.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_OCCASION_LIST ();

END GET_OCCASION_LIST;

PROCEDURE GET_OCCASION_ADDON_LIST (
   OUT_CUR OUT TYPES.REF_CURSOR) IS
/*------------------------------------------------------------------------------
Description:
   Return all occasions.
------------------------------------------------------------------------------*/
BEGIN

    OUT_CUR := FTD_APPS.SP_GET_OCCASION_ADDON_LIST (); 

END GET_OCCASION_ADDON_LIST;


PROCEDURE GET_OCCASION_BOX_X_DESC_BY_ID
(
 IN_OCCASION_ID       IN FTD_APPS.OCCASION.OCCASION_ID%TYPE,
 OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Return the box_x_description for the given occasion_id.
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
   SELECT box_x_description
   FROM   ftd_apps.occasion
   WHERE  occasion_id = in_occasion_id;

END GET_OCCASION_BOX_X_DESC_BY_ID;


PROCEDURE GET_ORIGINS (
   OUT_CUR OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all origins.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
   SELECT ORIGIN_ID,
          DESCRIPTION,
          ORIGIN_TYPE
   FROM   FTD_APPS.ORIGINS
   WHERE ACTIVE='Y';

END GET_ORIGINS;


PROCEDURE GET_PARTNERS (
   IN_PARTNER_ID        IN  FTD_APPS.PARTNERS.PARTNER_ID%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a the partner information for the given id.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  PARTNER_NAME,
                CUSTOMER_INFO,
                REWARD_NAME,
                ID_LENGTH,
                EMAIL_EXCLUDE_FLAG
        FROM    FTD_APPS.PARTNERS
        WHERE   PARTNER_ID = IN_PARTNER_ID;

END GET_PARTNERS;


PROCEDURE GET_PAYMENT_METHOD_BY_ID (
   IN_PAYMENT_METHOD_ID IN  VARCHAR2,
   OUT_PAYMENT_METHOD   OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Given a payment method ID, return the payment_method.
------------------------------------------------------------------------------*/
BEGIN

--   OUT_PAYMENT_METHOD := FTD_APPS.OE_GET_PAYMENT_METHOD(IN_PAYMENT_METHOD_ID);

OPEN OUT_PAYMENT_METHOD FOR
        SELECT  PAYMENT_METHOD_ID,
                DESCRIPTION,
                PAYMENT_TYPE,
                CARD_ID,
                HAS_EXPIRATION_DATE,
                OVER_AUTH_ALLOWED_PCT,
                OVER_AUTH_ALLOWED_AMT,
                MIN_AUTH_AMT
        FROM    FTD_APPS.PAYMENT_METHODS
        WHERE   PAYMENT_METHOD_ID = IN_PAYMENT_METHOD_ID;

END GET_PAYMENT_METHOD_BY_ID;


PROCEDURE GET_PAYMENT_METHODS_BY_SOURCE (
   IN_SOURCE_CODE  IN  FTD_APPS.SOURCE.SOURCE_CODE%TYPE,
   OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a list of payment methods if no source code is specified
   or the given source code doesn't have a valid pay method specified or
   a specific pay method if the given source code has a valid pay method specified.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
        SELECT  PT.PAYMENT_METHOD_ID,
                PT.DESCRIPTION,
                PT.PAYMENT_TYPE,
                PT.HAS_EXPIRATION_DATE
        FROM    FTD_APPS.PAYMENT_METHODS PT
        WHERE   EXISTS
                (       SELECT  1
                        FROM    FTD_APPS.SOURCE S
                        WHERE   S.SOURCE_CODE = IN_SOURCE_CODE
                        AND     (S.VALID_PAY_METHOD = PT.PAYMENT_METHOD_ID OR S.VALID_PAY_METHOD IS NULL)
                )
        OR      IN_SOURCE_CODE IS NULL;

END GET_PAYMENT_METHODS_BY_SOURCE;


PROCEDURE GET_PAYMENT_METHODS_BY_TYPE (
   IN_PAYMENT_TYPE IN  FTD_APPS.PAYMENT_METHODS.PAYMENT_TYPE%TYPE,
   OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a list of payment methods with the given type.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT PAYMENT_METHOD_ID,
          DESCRIPTION,
          PAYMENT_TYPE,
          CARD_ID,
          HAS_EXPIRATION_DATE
   FROM   FTD_APPS.PAYMENT_METHODS
   WHERE  PAYMENT_TYPE = IN_PAYMENT_TYPE
   ORDER BY payment_method_id;

END GET_PAYMENT_METHODS_BY_TYPE;

PROCEDURE GET_PAYMENT_METHODS_EX_CB (
   IN_PAYMENT_TYPE IN  FTD_APPS.PAYMENT_METHODS.PAYMENT_TYPE%TYPE,
   OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a list of payment methods with the given type excluding Carte Blanche payment method
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT PAYMENT_METHOD_ID,
          DESCRIPTION,
          PAYMENT_TYPE,
          CARD_ID,
          HAS_EXPIRATION_DATE
   FROM   FTD_APPS.PAYMENT_METHODS
   WHERE  PAYMENT_TYPE = IN_PAYMENT_TYPE and PAYMENT_METHOD_ID <> 'CB'
   ORDER BY payment_method_id;

END GET_PAYMENT_METHODS_EX_CB;

PROCEDURE GET_PAYMENT_METHOD_LIST (
   OUT_PAYMENT_METHODS OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a complete list of payment types.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_PAYMENT_METHODS FOR
   SELECT PAYMENT_METHOD_ID, DESCRIPTION, PAYMENT_TYPE, HAS_EXPIRATION_DATE
   FROM   FTD_APPS.PAYMENT_METHODS
   ORDER BY PAYMENT_METHOD_ID;

END GET_PAYMENT_METHOD_LIST;

PROCEDURE GET_COMPLIMENTARY_ADDON  (
  OUT_COMPLIMENTARY_ADDONS  OUT TYPES.REF_CURSOR) 
AS
/*------------------------------------------------------------------------------
Description:
   Returns a complete list of complimentary addons.
------------------------------------------------------------------------------*/
BEGIN

    OUT_COMPLIMENTARY_ADDONS := FTD_APPS.SP_GET_ADDON_BY_TYPE_DESC('Complimentary');

END GET_COMPLIMENTARY_ADDON;


PROCEDURE GET_PAYMENT_METHODS_TEXT (
   IN_TYPE_TXT IN  FTD_APPS.PAYMENT_METHODS_TEXT.TYPE_TXT%TYPE,
   OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a list of payment methods text for the given type.
------------------------------------------------------------------------------*/
BEGIN
   IF IN_TYPE_TXT IS NOT NULL THEN
     OPEN OUT_CUR FOR
     SELECT PAYMENT_METHOD_ID,
            TYPE_TXT,
            TEXT_TXT
     FROM   FTD_APPS.PAYMENT_METHODS_TEXT
     WHERE  TYPE_TXT = IN_TYPE_TXT
     ORDER BY payment_method_id;
   ELSE
     OPEN OUT_CUR FOR
     SELECT PAYMENT_METHOD_ID,
            TYPE_TXT,
            TEXT_TXT
     FROM   FTD_APPS.PAYMENT_METHODS_TEXT
     ORDER BY payment_method_id;
   END IF;

END GET_PAYMENT_METHODS_TEXT;


PROCEDURE GET_PAYMENT_METHOD_MP
(
IN_PAYMENT_METHOD_ID  IN  FTD_APPS.PAYMENT_METHOD_MILES_POINTS.PAYMENT_METHOD_ID%TYPE,
OUT_CUR               OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns miles/points payment method information for specified payment method

Input:
payment_method_id

Output:
cursor containing payment method info

------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
SELECT PAYMENT_METHOD_ID,
       DOLLAR_TO_MP_OPERATOR,
       ROUNDING_METHOD_ID,
       ROUNDING_SCALE_QTY,
       COMMISSION_PCT
FROM  FTD_APPS.PAYMENT_METHOD_MILES_POINTS
WHERE PAYMENT_METHOD_ID = IN_PAYMENT_METHOD_ID;

END GET_PAYMENT_METHOD_MP;


PROCEDURE GET_PRICE_HEADER_DETAILS (
   IN_PRICE_HEADER_ID   IN  FTD_APPS.PRICE_HEADER_DETAILS.PRICE_HEADER_ID%TYPE,
   IN_PRICE_AMT         IN  FTD_APPS.PRICE_HEADER_DETAILS.MIN_DOLLAR_AMT%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns a the discount information for the given price header and price amount.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
   SELECT PRICE_HEADER_ID,
          MIN_DOLLAR_AMT,
          MAX_DOLLAR_AMT,
          DISCOUNT_TYPE,
          DISCOUNT_AMT
   FROM   FTD_APPS.PRICE_HEADER_DETAILS
   WHERE  PRICE_HEADER_ID = IN_PRICE_HEADER_ID
   AND    MIN_DOLLAR_AMT <= IN_PRICE_AMT
   AND    MAX_DOLLAR_AMT >= IN_PRICE_AMT;

END GET_PRICE_HEADER_DETAILS;


PROCEDURE GET_PRICE_POINTS_LIST (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all information from filter price points
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_PRICE_POINTS_LIST ();

END GET_PRICE_POINTS_LIST;


PROCEDURE GET_PRODUCT_BY_ID (
   IN_PRODUCT_ID IN  FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product information for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_PRODUCT_BY_ID_2247 (IN_PRODUCT_ID);

END GET_PRODUCT_BY_ID;


PROCEDURE GET_PRODUCT_BY_NOVATOR_ID (
   IN_NOVATOR_ID IN  FTD_APPS.PRODUCT_MASTER.NOVATOR_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns product information for the given novator id.
   If the product_id is not found in product_master, look for it in
   product_xref.
   If the product is not found in product_master or product_xref, return empty
   cursor.
------------------------------------------------------------------------------*/

   PRODUCT_COUNT NUMBER;

BEGIN

   SELECT COUNT(*)
   INTO PRODUCT_COUNT
   FROM FTD_APPS.PRODUCT_MASTER
   WHERE NOVATOR_ID = IN_NOVATOR_ID;

   IF PRODUCT_COUNT > 0 THEN
      OPEN OUT_CUR FOR
      SELECT PRODUCT_ID, PRODUCT_NAME, SHORT_DESCRIPTION, LONG_DESCRIPTION
      FROM FTD_APPS.PRODUCT_MASTER
      WHERE NOVATOR_ID = IN_NOVATOR_ID;
   ELSE
      OPEN OUT_CUR FOR
      SELECT X.PRODUCT_ID, M.PRODUCT_NAME, M.SHORT_DESCRIPTION, M.LONG_DESCRIPTION
      FROM FTD_APPS.PRODUCT_XREF X
      LEFT OUTER JOIN FTD_APPS.PRODUCT_MASTER M
      ON M.PRODUCT_ID = X.PRODUCT_ID
      WHERE X.PRODUCT_XREF_ID = IN_NOVATOR_ID;
   END IF;

END GET_PRODUCT_BY_NOVATOR_ID;


PROCEDURE GET_PRODUCT_BY_NOV_OR_SUB
(
IN_PRODUCT_ID                   IN  VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure obtains product info for a product_id that can be
             a novator id, subcode, or product_id

Input:  product_id Can be novator_id, product_subcode_id, or product_id

Output: out_cur Cursor containing product info

-----------------------------------------------------------------------------*/
v_productID              VARCHAR2(100) := UPPER(IN_PRODUCT_ID);
v_productSubCodeId       FTD_APPS.PRODUCT_SUBCODES.PRODUCT_SUBCODE_ID%TYPE;
v_subcodeDesc            FTD_APPS.PRODUCT_SUBCODES.SUBCODE_DESCRIPTION%TYPE;

BEGIN

    -- Determine if the product ID given is actually a subcode or novator id
    BEGIN
        -- Determine if the product id is a subcode
        SELECT PRODUCT_ID, PRODUCT_SUBCODE_ID, SUBCODE_DESCRIPTION
        INTO v_productID, v_productSubCodeId, v_subcodeDesc
        FROM FTD_APPS.PRODUCT_SUBCODES
        WHERE PRODUCT_SUBCODE_ID = v_productID;
    EXCEPTION WHEN OTHERS THEN
        -- Determine if the product id is a novator id
        BEGIN
          SELECT PRODUCT_ID
          INTO v_productID
          FROM FTD_APPS.PRODUCT_MASTER
          WHERE NOVATOR_ID = v_productID;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          NULL;
        END;
    END;

    BEGIN
      OPEN OUT_CUR FOR
      SELECT v_productID PRODUCT_ID, PRODUCT_NAME, SHORT_DESCRIPTION, LONG_DESCRIPTION, v_productSubCodeId PRODUCT_SUBCODE_ID, v_subcodeDesc SUBCODE_DESCRIPTION
      FROM FTD_APPS.PRODUCT_MASTER
      WHERE PRODUCT_ID = v_productID;
    END;

END GET_PRODUCT_BY_NOV_OR_SUB;


PROCEDURE GET_PRODUCT_BY_TIMESTAMP (
   IN_PRODUCT_ID IN  FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
   IN_DATE_TIME IN  DATE,
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns versioned product information for the given product id and timestamp.
------------------------------------------------------------------------------*/

BEGIN

      OPEN OUT_CUR FOR
SELECT DISTINCT pma.standard_price,
pma.subcode_price,
pma.deluxe_price,
pma.premium_price
FROM ftd_apps.product_change$_acct pca,
ftd_apps.product_master$_acct pma
WHERE pca.change_id = pma.change_id
AND pma.product_id = IN_PRODUCT_ID
AND pca.operation$ in ('UPD_OLD')
AND pca.timestamp$ = (
   SELECT MIN(pca2.timestamp$)
   FROM ftd_apps.product_change$_acct pca2,
   ftd_apps.product_master$_acct pma2
   WHERE pca2.change_id = pma2.change_id
   AND pca2.timestamp$ >= IN_DATE_TIME
   AND pma2.product_id = IN_PRODUCT_ID
   AND pca2.operation$ in ('UPD_OLD')
);

END GET_PRODUCT_BY_TIMESTAMP;


PROCEDURE GET_PRODUCT_CATEGORIES (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return id and descriptions for top level product indexes.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_PRODUCT_CATEGORIES ();

END GET_PRODUCT_CATEGORIES;


PROCEDURE GET_PRODUCT_COLORS_BY_ID (
   IN_PRODUCT_ID IN  FTD_APPS.PRODUCT_COLORS.PRODUCT_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product colors for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_PRODUCT_COLORS (IN_PRODUCT_ID);

END GET_PRODUCT_COLORS_BY_ID;


PROCEDURE GET_PRODUCT_DETAILS (
   IN_PRODUCT_ID                IN VARCHAR2,
   IN_SOURCE_CODE               IN VARCHAR2,
   IN_DOMESTIC_INTL_FLAG        IN VARCHAR2,
   IN_ZIP_CODE                  IN VARCHAR2,
   IN_COUNTRY_ID                IN VARCHAR2,
   IN_DELIVERY_DATE             IN VARCHAR2,
   OUT_CUR                      OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product master records using the given search criteria.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_PRODUCT_DETAILS_657 (
                                        IN_PRODUCT_ID,
                                        IN_SOURCE_CODE,
                                        IN_DOMESTIC_INTL_FLAG,
                                        IN_ZIP_CODE,
                                        IN_COUNTRY_ID,
                                        IN_DELIVERY_DATE);

END GET_PRODUCT_DETAILS;



PROCEDURE GET_PRODUCT_EXCLUDED_STATES (
   IN_PRODUCT_ID        IN FTD_APPS.PRODUCT_EXCLUDED_STATES.PRODUCT_ID%TYPE,
   IN_EXCLUDED_STATE    IN FTD_APPS.PRODUCT_EXCLUDED_STATES.EXCLUDED_STATE%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR)

AS
/*------------------------------------------------------------------------------
Description:
   Return given product and excluded state information
------------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  PRODUCT_ID,
                EXCLUDED_STATE,
                DATE_LAST_MODIFIED,
                SUN,
                MON,
                TUE,
                WED,
                THU,
                FRI,
                SAT
        FROM   FTD_APPS.PRODUCT_EXCLUDED_STATES
        WHERE  PRODUCT_ID = IN_PRODUCT_ID
        AND    EXCLUDED_STATE = IN_EXCLUDED_STATE;

END GET_PRODUCT_EXCLUDED_STATES;



PROCEDURE GET_PRODUCT_INDEX_BY_ID (
   IN_INDEX_ID          IN NUMBER,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all product indexes with their subindices and product counts.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_INDEX_DETAILS (IN_INDEX_ID);

END GET_PRODUCT_INDEX_BY_ID;


PROCEDURE GET_PRODUCT_LIST_BY_ID_SDG (
   IN_INDEX_ID                  IN NUMBER,
   IN_PRICE_POINT_ID            IN NUMBER,
   IN_SOURCE_CODE               IN VARCHAR2,
   IN_DOMESTIC_INTL_FLAG        IN VARCHAR2,
   IN_ZIP_CODE                  IN VARCHAR2,
   IN_DELIVERY_END_DATE         IN VARCHAR2,
   IN_COUNTRY_ID                IN VARCHAR2,
   IN_SCRIPT_CODE               IN VARCHAR2,
   OUT_CUR                      OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product master records using the given search criteria.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_PRODUCT_LIST_BY_INDEX_MD04 (
                                        IN_INDEX_ID,
                                        IN_PRICE_POINT_ID,
                                        IN_SOURCE_CODE,
                                        IN_DOMESTIC_INTL_FLAG,
                                        IN_ZIP_CODE,
                                        IN_DELIVERY_END_DATE,
                                        IN_COUNTRY_ID,
                                        IN_SCRIPT_CODE);

END GET_PRODUCT_LIST_BY_ID_SDG;




PROCEDURE GET_PRODUCT_OCCASIONS (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return id and descriptions for products flaged for occasion search.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_OCCASIONS ();

END GET_PRODUCT_OCCASIONS;


PROCEDURE GET_PRODUCT_SHIP_DATES (
   IN_PRODUCT_ID IN  VARCHAR2,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product ship dates for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_PRODUCT_SHIP_DATES (IN_PRODUCT_ID);

END GET_PRODUCT_SHIP_DATES;


PROCEDURE GET_PRODUCT_SHIP_METHODS_BY_ID (
   IN_PRODUCT_ID IN  FTD_APPS.PRODUCT_SHIP_METHODS.PRODUCT_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product ship methods for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_PRODUCT_SHIP_METHODS (IN_PRODUCT_ID);

END GET_PRODUCT_SHIP_METHODS_BY_ID;


PROCEDURE GET_PRODUCT_SUBCODES (
   IN_PRODUCT_ID IN  VARCHAR2,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product subcodes for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_PRODUCT_SUBCODES (IN_PRODUCT_ID);

END GET_PRODUCT_SUBCODES;


PROCEDURE GET_PRODUCT_SUBCODES_ACTIVE (
   IN_PRODUCT_ID IN  VARCHAR2,
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return active product subcodes for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_PRODUCT_SUBCODES (IN_PRODUCT_ID);

END GET_PRODUCT_SUBCODES_ACTIVE;


PROCEDURE GET_PRODUCTS_BY_ID_SDG (
   IN_INDEX_ID                  IN VARCHAR2,
   IN_PRICE_POINT_ID            IN NUMBER,
   IN_SOURCE_CODE               IN VARCHAR2,
   IN_DOMESTIC_INTL_FLAG        IN VARCHAR2,
   IN_ZIP_CODE                  IN VARCHAR2,
   IN_DELIVERY_END_DATE         IN VARCHAR2,
   IN_COUNTRY_ID                IN VARCHAR2,
   IN_SCRIPT_CODE               IN VARCHAR2,
   IN_DELIVERY_DATE             IN VARCHAR2,
   OUT_CUR                      OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product master records using the given search criteria.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_PRODUCTS_BY_ID_MD04 (
                                        IN_INDEX_ID,
                                        IN_PRICE_POINT_ID,
                                        IN_SOURCE_CODE,
                                        IN_DOMESTIC_INTL_FLAG,
                                        IN_ZIP_CODE,
                                        IN_DELIVERY_END_DATE,
                                        IN_COUNTRY_ID,
                                        IN_SCRIPT_CODE,
                                        IN_DELIVERY_DATE);

END GET_PRODUCTS_BY_ID_SDG;


PROCEDURE GET_RECIPIENT_SEARCH_LIST (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return id and descriptions from recipient search master.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_RECIPIENT_SEARCH_LIST ();

END GET_RECIPIENT_SEARCH_LIST;


PROCEDURE GET_SCRIPTING_COUNTRY_XREF (
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all scripting information
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
        SELECT  COUNTRY_ID,
                SCRIPTING_CODE,
                COMPANY_ID
        FROM    FTD_APPS.SCRIPTING_COUNTRY_XREF;

END GET_SCRIPTING_COUNTRY_XREF;


PROCEDURE GET_SCRIPT_BY_PAGE_SFMB (
   IN_COMPANY_ID        IN VARCHAR2,
   IN_PAGE_ID           IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return scription information by the given company and page.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_SCRIPT_BY_PAGE_SFMB (IN_COMPANY_ID, IN_PAGE_ID);

END GET_SCRIPT_BY_PAGE_SFMB;


PROCEDURE GET_SCRIPT_ID (
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all script ids from script master
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
        SELECT  SCRIPT_MASTER_ID,
                SCRIPT_DESCRIPTION
        FROM    FTD_APPS.SCRIPT_MASTER;

END GET_SCRIPT_ID;


PROCEDURE GET_SHIP_METHODS (
   OUT_CUR       OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all ship methods and descriptions.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_SHIP_METHODS;

END GET_SHIP_METHODS;


PROCEDURE GET_SHIP_METHODS_BY_PRODUCT_ID (
   IN_PRODUCT_ID IN  FTD_APPS.PRODUCT_SHIP_METHODS.PRODUCT_ID%TYPE,
   OUT_CUR       OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns ship method information for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT /*+ RULE */ SM.SHIP_METHOD_ID,
          SM.DESCRIPTION
   FROM FTD_APPS.SHIP_METHODS SM
   JOIN FTD_APPS.PRODUCT_SHIP_METHODS PSM
   ON SM.SHIP_METHOD_ID = PSM.SHIP_METHOD_ID
   WHERE PSM.PRODUCT_ID = IN_PRODUCT_ID;

END GET_SHIP_METHODS_BY_PRODUCT_ID;


PROCEDURE GET_SHIPPING_KEY_COSTS (
   IN_SHIPPING_KEY_DETAIL_ID    IN  FTD_APPS.SHIPPING_KEY_COSTS.SHIPPING_KEY_DETAIL_ID%TYPE,
   IN_SHIPPING_METHOD_ID        IN  FTD_APPS.SHIPPING_KEY_COSTS.SHIPPING_METHOD_ID%TYPE,
   OUT_CUR                      OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns shipping cost information for the given detail id and shipping method.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT       SHIPPING_KEY_DETAIL_ID,
                SHIPPING_METHOD_ID,
                SHIPPING_COST
   FROM         FTD_APPS.SHIPPING_KEY_COSTS
   WHERE        SHIPPING_KEY_DETAIL_ID = IN_SHIPPING_KEY_DETAIL_ID
   AND          SHIPPING_METHOD_ID = IN_SHIPPING_METHOD_ID;

END GET_SHIPPING_KEY_COSTS;


PROCEDURE GET_SHIPPING_KEY_DETAILS (
   IN_SHIPPING_KEY_ID           IN  FTD_APPS.SHIPPING_KEY_DETAILS.SHIPPING_KEY_ID%TYPE,
   IN_PRICE_AMT                 IN  FTD_APPS.SHIPPING_KEY_DETAILS.MIN_PRICE%TYPE,
   OUT_CUR                      OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns shipping key details information for the given id and price.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT       SHIPPING_DETAIL_ID,
                SHIPPING_KEY_ID,
                MIN_PRICE,
                MAX_PRICE
   FROM         FTD_APPS.SHIPPING_KEY_DETAILS
   WHERE        SHIPPING_KEY_ID = IN_SHIPPING_KEY_ID
   AND          MIN_PRICE <= IN_PRICE_AMT
   AND          MAX_PRICE >= IN_PRICE_AMT;

END GET_SHIPPING_KEY_DETAILS;


PROCEDURE GET_SHIPPING_METHODS (
   IN_PRODUCT_ID        IN VARCHAR2,
   IN_SHIPPING_KEY_ID   IN NUMBER,
   IN_STANDARD_PRICE    IN NUMBER,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return product ship methods information using the given criteria.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_SHIPPING_METHODS (IN_PRODUCT_ID, IN_SHIPPING_KEY_ID, IN_STANDARD_PRICE);

END GET_SHIPPING_METHODS;


PROCEDURE GET_SNH_BY_ID (
   IN_SNH_ID                    IN  FTD_APPS.SNH.SNH_ID%TYPE,
   IN_DELIVERY_DATE             IN  VARCHAR2,
   OUT_CUR                      OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns shipping and handling information for the given id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT       SNH_ID,
                DESCRIPTION,
                nvl(
                    (select sfo.domestic_charge from ftd_apps.service_fee_override sfo
                        where sfo.snh_id = snh.snh_id
                        and sfo.override_date = to_date(in_delivery_date, 'MM/DD/YYYY')
                ), snh.first_order_domestic) FIRST_ORDER_DOMESTIC,
                SECOND_ORDER_DOMESTIC,
                THIRD_ORDER_DOMESTIC,
                nvl(
                    (select sfo.international_charge from ftd_apps.service_fee_override sfo
                        where sfo.snh_id = snh.snh_id
                        and sfo.override_date = to_date(in_delivery_date, 'MM/DD/YYYY')
                ), snh.first_order_international) FIRST_ORDER_INTERNATIONAL,
                SECOND_ORDER_INTERNATIONAL,
                THIRD_ORDER_INTERNATIONAL
                VENDOR_CHARGE,
                VENDOR_SAT_UPCHARGE,
                VENDOR_SUN_UPCHARGE,
                VENDOR_MON_UPCHARGE,
				nvl(
                    (select sfo.same_day_upcharge from ftd_apps.service_fee_override sfo
                        where sfo.snh_id = snh.snh_id
                        and sfo.override_date = to_date(in_delivery_date, 'MM/DD/YYYY')
                ), snh.same_day_upcharge) SAME_DAY_UPCHARGE,
                nvl(
                    (select sfo.same_day_upcharge_fs from ftd_apps.service_fee_override sfo
                        where sfo.snh_id = snh.snh_id
                        and sfo.override_date = to_date(in_delivery_date, 'MM/DD/YYYY')
                ), snh.same_day_upcharge_fs) SAME_DAY_UPCHARGE_FS
   FROM         FTD_APPS.SNH
   WHERE        SNH_ID = IN_SNH_ID;

END GET_SNH_BY_ID;


PROCEDURE GET_SNH_LIST
(
 IN_SORT_FIELD  IN  VARCHAR2,
 OUT_CURSOR   OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns shipping and handling information.
------------------------------------------------------------------------------*/
v_sort_field	varchar2(100);

BEGIN
   IF in_sort_field IS NULL THEN
   	v_sort_field := ' SNH_ID';
   ELSE
   	v_sort_field := in_sort_field;
   END IF;
   
   
   OPEN out_cursor FOR
   'SELECT snh_id,
            description,
            first_order_domestic,
            second_order_domestic,
            third_order_domestic,
            first_order_international,
            second_order_international,
            third_order_international,
            (select ''Y'' from ftd_apps.source_master sm
                where sm.snh_id = snh.snh_id and ROWNUM = 1) source_codes_exist,
            (select ''Y'' from ftd_apps.service_fee_override sfo
                where sfo.snh_id = snh.snh_id and ROWNUM = 1) overrides_exist,
            requested_by,
            vendor_charge,
            vendor_sat_upcharge,
            vendor_sun_upcharge,
            vendor_mon_upcharge,
            same_day_upcharge,
            same_day_upcharge_fs
       FROM ftd_apps.snh
      ORDER BY ' || v_sort_field;

END GET_SNH_LIST;

PROCEDURE GET_SNH_LIST
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns shipping and handling information.
------------------------------------------------------------------------------*/
BEGIN

    GET_SNH_LIST(null, OUT_CURSOR);

END GET_SNH_LIST;

PROCEDURE GET_SOURCE_CODE_RECORD (
   IN_SOURCE_CODE    IN  FTD_APPS.SOURCE.SOURCE_CODE%TYPE,
   OUT_SOURCE_RECORD OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Retrieves one source code record.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_SOURCE_RECORD FOR
   SELECT SOURCE_CODE,
        SOURCE_TYPE,
        DEPARTMENT_CODE,
        DESCRIPTION,
        START_DATE,
        END_DATE,
        PRICING_CODE,
        SHIPPING_CODE,
        PARTNER_ID,
        VALID_PAY_METHOD,
        LIST_CODE_FLAG,
        DEFAULT_SOURCE_CODE_FLAG,
        BILLING_INFO_PROMPT,
        BILLING_INFO_LOGIC,
        MARKETING_GROUP,
        EXTERNAL_CALL_CENTER_FLAG,
        HIGHLIGHT_DESCRIPTION_FLAG,
        DISCOUNT_ALLOWED_FLAG,
        BIN_NUMBER_CHECK_FLAG,
        ORDER_SOURCE,
        MILEAGE_BONUS,
        PROMOTION_CODE,
        MILEAGE_CALCULATION_SOURCE,
        BONUS_POINTS,
        BONUS_TYPE,
        SEPARATE_DATA,
        YELLOW_PAGES_CODE,
        POINTS_MILES_PER_DOLLAR,
        MAXIMUM_POINTS_MILES,
        JCPENNEY_FLAG,
        OE_DEFAULT_FLAG,
        COMPANY_ID,
        SEND_TO_SCRUB,
        FRAUD_FLAG,
        ENABLE_LP_PROCESSING,
        EMERGENCY_TEXT_FLAG,
        REQUIRES_DELIVERY_CONFIRMATION,
        WEBLOYALTY_FLAG,
        SURCHARGE_AMOUNT, 
        APPLY_SURCHARGE_CODE,
        SURCHARGE_DESCRIPTION,
        DISPLAY_SURCHARGE_FLAG,
	ALLOW_FREE_SHIPPING_FLAG,
	PARTNER_NAME,
	PROGRAM_TYPE,
	REWARD_TYPE,
	SAME_DAY_UPCHARGE,
	DISPLAY_SAME_DAY_UPCHARGE,
  	MORNING_DELIVERY_FLAG,
  	MORNING_DELIVERY_FREE_SHIPPING,
  	SAME_DAY_UPCHARGE_FS
   FROM   FTD_APPS.SOURCE
   WHERE  SOURCE_CODE = IN_SOURCE_CODE;

END GET_SOURCE_CODE_RECORD;

PROCEDURE GET_SOURCECODELIST_BY_COMP (
    in_search_Value     IN varchar2,
    in_company_id       IN varchar2,
    OUT_SOURCECODELIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return a formatted source code list that matches the source code OR the
   description.  If IN_DATE_FLAG is not 'Y', then the END_DATE must be NOT NULL.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_SOURCECODELIST FOR
   SELECT SUBSTR(SC.DESCRIPTION,1,1) as "anchor",
                      SC.DESCRIPTION as "sourceDescription",
                           PH.DESCRIPTION as "offerDescription",
                           '$' || SNH.FIRST_ORDER_DOMESTIC ||
                               ' / ' ||
                               '$' || SNH.FIRST_ORDER_INTERNATIONAL as "serviceCharge",
                           to_char(SC.END_DATE,'mm/dd/yyyy') as "expirationDate",
                           SC.ORDER_SOURCE as "orderSource",
                           SC.SOURCE_CODE as "sourceCode",
                           DECODE(SC.END_DATE, NULL, 'N',
                             DECODE(SIGN(SC.END_DATE - TRUNC(SYSDATE)), 0, 'N', 1, 'N', 'Y')) as "expiredFlag",
                           SC.PARTNER_ID,
                           SC.COMPANY_ID,
                           SC.VALID_PAY_METHOD
                FROM FTD_APPS.source SC, FTD_APPS.price_header PH, FTD_APPS.snh
                WHERE ( SC.SOURCE_CODE like (in_search_Value || '%')
                     OR SC.DESCRIPTION like (in_search_Value || '%') )
                AND SC.PRICING_CODE = ph.PRICE_HEADER_ID
                AND SC.SHIPPING_CODE = snh.SNH_ID
                AND (SC.END_DATE IS  NULL OR SC.END_DATE >= SYSDATE)
                AND SC.COMPANY_ID = in_company_id
                ORDER BY SC.DESCRIPTION ASC, SC.ORDER_SOURCE DESC;

END GET_SOURCECODELIST_BY_COMP;


PROCEDURE GET_SOURCECODELIST_BY_VALUE (
   IN_SOURCE_CODE     IN  FTD_APPS.SOURCE.SOURCE_CODE%TYPE,
   IN_DESCRIPTION     IN  FTD_APPS.SOURCE.DESCRIPTION%TYPE,
   IN_DATE_FLAG       IN  VARCHAR2,
   OUT_SOURCECODELIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return a formatted source code list that matches the source code OR the
   description.  If IN_DATE_FLAG is not 'Y', then the END_DATE must be NOT NULL.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_SOURCECODELIST FOR
   SELECT DISTINCT SUBSTR(SC.DESCRIPTION,1,1) as "anchor",
          SC.DESCRIPTION as "sourceDescription",
          PH.DESCRIPTION as "offerDescription",
          '$' || SNH.FIRST_ORDER_DOMESTIC ||
            ' / ' ||
            '$' || SNH.FIRST_ORDER_INTERNATIONAL as "serviceCharge",
          to_char(SC.END_DATE,'mm/dd/yyyy') as "expirationDate",
          SC.ORDER_SOURCE as "orderSource",
          SC.SOURCE_CODE as "sourceCode",
          DECODE(SC.END_DATE, NULL, 'N',
          DECODE(SIGN(SC.END_DATE - TRUNC(SYSDATE)), 0, 'N', 1, 'N', 'Y')) as "expiredFlag",
          SC.PARTNER_ID,
          SC.COMPANY_ID,
          SC.VALID_PAY_METHOD,
          DECODE(PRTM.PREFERRED_PARTNER_FLAG, null, null, 'N', null, PRTM.PARTNER_NAME) AS PREFERRED_PARTNER_NAME,
          SC.ALLOW_FREE_SHIPPING_FLAG
   FROM   FTD_APPS.source SC 
   JOIN   FTD_APPS.price_header PH ON SC.PRICING_CODE = ph.PRICE_HEADER_ID
   JOIN   FTD_APPS.snh ON SC.SHIPPING_CODE = snh.SNH_ID
   LEFT OUTER JOIN FTD_APPS.SOURCE_PROGRAM_REF SPR ON SPR.SOURCE_CODE = SC.SOURCE_CODE 
   LEFT OUTER JOIN FTD_APPS.PARTNER_PROGRAM PP ON PP.PROGRAM_NAME = SPR.PROGRAM_NAME  
   LEFT OUTER JOIN FTD_APPS.PARTNER_MASTER PRTM ON PRTM.PARTNER_NAME = PP.PARTNER_NAME 
   WHERE (SC.SOURCE_CODE like ( IN_SOURCE_CODE || '%') OR SC.DESCRIPTION like ( IN_DESCRIPTION || '%') )
   AND   (IN_DATE_FLAG = 'Y' OR SC.END_DATE IS NOT NULL)
   ORDER BY SC.DESCRIPTION ASC, SC.ORDER_SOURCE DESC;

END GET_SOURCECODELIST_BY_VALUE;


PROCEDURE GET_SOURCE_CODE_BY_COMPANY_ID
(
   IN_COMPANY_ID       IN FTD_APPS.SOURCE.COMPANY_ID%TYPE,
   OUT_SOURCECODELIST OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        This stored procedure retrieves all the source codes from table source
        for a particular company id.

Input:
        company id

Output:
        cursor containing source codes
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_sourcecodelist FOR
     SELECT source_code
       FROM ftd_apps.source
      WHERE company_id = in_company_id;

END GET_SOURCE_CODE_BY_COMPANY_ID;


PROCEDURE GET_SOURCECODELIST_BY_COMPANY
(
 IN_SOURCE_CODE     IN FTD_APPS.SOURCE.SOURCE_CODE%TYPE,
 IN_DESCRIPTION     IN FTD_APPS.SOURCE.DESCRIPTION%TYPE,
 IN_COMPANY_ID      IN FTD_APPS.SOURCE.COMPANY_ID%TYPE,
 IN_DATE_FLAG       IN CHAR,
 OUT_SOURCECODELIST OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Return a formatted source code list that matches the company id,
   the source code OR the description.  If IN_DATE_FLAG is not 'Y',
   then the END_DATE must be NOT NULL.

Input:
        source code     VARCHAR2
        description     VARCHAR2
        company id      VARCHAR2
        date flag       CHAR

Output:
        cursor containing source codes
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_sourcecodelist FOR
   SELECT SUBSTR(s.description,1,1) as "anchor",
          s.description as "sourceDescription",
          ph.description as "offerDescription",
          '$' || snh.first_order_domestic || ' / ' || '$' || snh.first_order_international AS "serviceCharge",
          to_char(s.end_date,'mm/dd/yyyy') AS "expirationDate",
          s.order_source AS "orderSource",
          s.source_code AS "sourceCode",
          DECODE(s.end_date, NULL, 'N', DECODE(SIGN(s.end_date - TRUNC(SYSDATE)), 0, 'N', 1, 'N', 'Y')) AS "expiredFlag",
          s.partner_id,
          s.company_id,
          s.valid_pay_method
     FROM ftd_apps.source s,
          ftd_apps.price_header ph,
          ftd_apps.snh
    WHERE (
           s.source_code LIKE ( in_source_code || '%')
           OR
           s.description LIKE ( in_description || '%')
          )
      AND company_id = in_company_id
      AND s.pricing_code = ph.price_header_id
      AND s.shipping_code = snh.snh_id
      AND (
           in_date_flag = 'Y'
           OR
           s.end_date IS NOT NULL
          )
   ORDER BY s.description ASC,
          s.order_source DESC;

END GET_SOURCECODELIST_BY_COMPANY;


PROCEDURE GET_SOURCE_CODE_DESCRIPTION
(
   IN_SOURCE_CODE     IN  FTD_APPS.SOURCE.SOURCE_CODE%TYPE,
   OUT_CUR            OUT TYPES.REF_CURSOR
) AS
/*------------------------------------------------------------------------------
Description:
   Return a formatted source code description including the service charges
   and the co branded url.
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        select  sc.description source_description,
                ph.description offer_description,
                '$' || snh.first_order_domestic || ' / ' || '$' || snh.first_order_international service_charge,
                sc.comment_text comments,
                (select c.url from ftd_apps.company_master c where c.company_id = sc.company_id) co_brand_url,
                sc.order_source order_source,
                sc.source_code source_code,
                sc.company_id
        from    ftd_apps.source_master sc
        join    ftd_apps.price_header ph
        on      sc.price_header_id = ph.price_header_id
        join    ftd_apps.snh
        on      sc.snh_id = snh.snh_id
        where   sc.source_code = in_source_code;

END GET_SOURCE_CODE_DESCRIPTION;


PROCEDURE GET_STATE_DETAILS (
   IN_STATE_MASTER_ID   IN  FTD_APPS.STATE_MASTER.STATE_MASTER_ID%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return state master information for the given state.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_STATE_DETAILS (IN_STATE_MASTER_ID);

END GET_STATE_DETAILS;


PROCEDURE GET_STATE_LIST (
   OUT_STATE_LIST OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all rows in the state master table.
------------------------------------------------------------------------------*/
BEGIN

   OUT_STATE_LIST := FTD_APPS.SP_GET_STATE_LIST;

END GET_STATE_LIST;


PROCEDURE GET_STOCK_MESG_BY_COMPANY (
   IN_ORIGIN_ID         IN  FTD_APPS.STOCK_MESSAGES.ORIGIN_ID%TYPE,
   IN_PARTNER_NAME      IN  FTD_APPS.STOCK_MESSAGES.PARTNER_NAME%TYPE,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return stock messages for the stock messages.  The table was changed to have
   stock messages for all companies.
------------------------------------------------------------------------------*/
V_ORIGIN_COUNT  NUMBER;
V_PARTNER_COUNT  NUMBER;
V_ORIGIN_ID  FTD_APPS.STOCK_MESSAGES.ORIGIN_ID%TYPE;
V_PARTNER_NAME  FTD_APPS.STOCK_MESSAGES.PARTNER_NAME%TYPE;

BEGIN

    SELECT COUNT (1)
    INTO   V_ORIGIN_COUNT
    FROM   FTD_APPS.STOCK_MESSAGES
    WHERE  ORIGIN_ID = IN_ORIGIN_ID;
    
    IF V_ORIGIN_COUNT = 0 THEN
        V_ORIGIN_ID := null;
    ELSE
        V_ORIGIN_ID := IN_ORIGIN_ID;
    END IF;

    SELECT COUNT (1)
    INTO   V_PARTNER_COUNT
    FROM   FTD_APPS.STOCK_MESSAGES
    WHERE  PARTNER_NAME = IN_PARTNER_NAME;
    
    IF V_PARTNER_COUNT = 0 THEN
        V_PARTNER_NAME := null;
    ELSE
        V_PARTNER_NAME := IN_PARTNER_NAME;
    END IF;

        OPEN OUT_CUR FOR
        SELECT  DISTINCT
                STOCK_MESSAGE_ID,
                DESCRIPTION,
                SORT_ORDER
        FROM    FTD_APPS.STOCK_MESSAGES
        WHERE   STOCK_MESSAGE_ID <> 'Default'
          AND   ((ORIGIN_ID = V_ORIGIN_ID) or (V_ORIGIN_ID  is null and ORIGIN_ID is null))
          AND   ((PARTNER_NAME = V_PARTNER_NAME) or (V_PARTNER_NAME  is null and PARTNER_NAME is null))
        ORDER BY SORT_ORDER;

END GET_STOCK_MESG_BY_COMPANY;

PROCEDURE GET_STOCK_MESG_BY_ID (
   IN_STOCK_MESSAGE_ID          IN  FTD_APPS.STOCK_MESSAGES.STOCK_MESSAGE_ID%TYPE,
   OUT_CUR                      OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all stock messages for the given id.  The table was changed to have
   stock messages for all companies.
------------------------------------------------------------------------------*/

BEGIN

        OPEN OUT_CUR FOR
        SELECT  STOCK_MESSAGE_ID,
                DESCRIPTION,
                SUBJECT,
                SECTION_ID,
                CONTENT
        FROM    FTD_APPS.STOCK_MESSAGES
        WHERE   STOCK_MESSAGE_ID = IN_STOCK_MESSAGE_ID
        ORDER BY SECTION_ID;

END GET_STOCK_MESG_BY_ID;



PROCEDURE GET_SUBCODE_BY_ID (
   IN_SUBCODE_ID        IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return subcode information for the given subcode id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.SP_GET_SUBCODES_BY_SUBCODE_ID (IN_SUBCODE_ID);

END GET_SUBCODE_BY_ID;


PROCEDURE GET_SUPER_INDEX_LIST_SFMB (
        IN_LIVE_FLAG            IN VARCHAR2,
        IN_OCCASIONS_FLAG       IN VARCHAR2,
        IN_PRODUCTS_FLAG        IN VARCHAR2,
        IN_DROPSHIP_FLAG        IN VARCHAR2,
        IN_COUNTRY_ID           IN VARCHAR2,
        IN_SOURCE_CODE_ID       IN VARCHAR2,
        OUT_CUR                 OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the super index page columns, either those for the occasions
   column or those for the Products column
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_SUPER_INDEX_LIST_SFMB (
                                                IN_LIVE_FLAG,
                                                IN_OCCASIONS_FLAG,
                                                IN_PRODUCTS_FLAG,
                                                IN_DROPSHIP_FLAG,
                                                IN_COUNTRY_ID,
                                                IN_SOURCE_CODE_ID
                                                );
END GET_SUPER_INDEX_LIST_SFMB;


PROCEDURE GET_SUB_INDEX_LIST_SFMB (
        IN_LIVE_FLAG            IN VARCHAR2,
        IN_DROPSHIP_FLAG        IN VARCHAR2,
        IN_COUNTRY_ID           IN VARCHAR2,
        IN_SOURCE_CODE_ID       IN VARCHAR2,
        OUT_CUR                 OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all subindexes directly under an occasion or category top level index
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_SUB_INDEX_LIST_SFMB (
                                                IN_LIVE_FLAG,
                                                IN_DROPSHIP_FLAG,
                                                IN_COUNTRY_ID,
                                                IN_SOURCE_CODE_ID
                                                );
END GET_SUB_INDEX_LIST_SFMB;


PROCEDURE GET_UPSELL_DETAILS (
        IN_UPSELL_MASTER_ID     IN VARCHAR2,
        OUT_CUR                 OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return upsell information fro the given upsell master id
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_UPSELL_DETAILS (IN_UPSELL_MASTER_ID);

END GET_UPSELL_DETAILS;


PROCEDURE GET_VENDOR_SHIPPING_RESTRICT (
   IN_PRODUCT_ID                IN  FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
   OUT_CUR                      OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns shipping restrictions for the given product id.
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
   SELECT       START_DATE,
                END_DATE
   FROM         FTD_APPS.PROD_SHIP_BLOCK_LIST_VW
   WHERE        PRODUCT_ID = IN_PRODUCT_ID;

END GET_VENDOR_SHIPPING_RESTRICT;



PROCEDURE GET_ZIPCODE_CUTOFF (
   IN_ZIP_CODE          IN VARCHAR2,
   IN_PRODUCT_ID        IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all a list of zipcodes.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := GLOBAL.GLOBAL_PKG.GET_ZIPCODE_CUTOFF (IN_ZIP_CODE, IN_PRODUCT_ID) ;

END GET_ZIPCODE_CUTOFF;



PROCEDURE GET_ZIPCODE_INFO (
   IN_ZIP_CODE_ID       IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return zipcodes information.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_ZIPCODE_INFO (IN_ZIP_CODE_ID) ;

END GET_ZIPCODE_INFO;


PROCEDURE GET_ZIPCODE_LIST (
   IN_ZIP_CODE_ID       IN VARCHAR2,
   IN_CITY              IN VARCHAR2,
   IN_STATE_MASTER_ID   IN VARCHAR2,
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all a list of zipcodes.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_ZIPCODE_LIST (IN_ZIP_CODE_ID, IN_CITY, IN_STATE_MASTER_ID) ;

END GET_ZIPCODE_LIST;



PROCEDURE GET_ZIPCODE_TIMEZONE (
   IN_ZIP_CODE_ID IN FTD_APPS.ZIP_CODE.ZIP_CODE_ID%TYPE,
   OUT_CUR        OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the time zone for the given zip code id.
------------------------------------------------------------------------------*/
BEGIN

   OUT_CUR := FTD_APPS.OE_GET_ZIPCODE_TIMEZONE (IN_ZIP_CODE_ID);

END GET_ZIPCODE_TIMEZONE;


PROCEDURE GET_ZIP_COUNT_FOR_FLORIST (
   IN_FLORIST_ID IN  FTD_APPS.FLORIST_ZIPS.FLORIST_ID%TYPE,
   IN_ZIP_CODE   IN  FTD_APPS.FLORIST_ZIPS.ZIP_CODE%TYPE,
   OUT_COUNT     OUT NUMBER) AS
/*------------------------------------------------------------------------------
Description:
   Return 0 if the given florist does not service the given zip code, and a
   positive integer otherwise.
------------------------------------------------------------------------------*/
BEGIN

-- CR 11/2004 - for new florist data model
-- check that the zip code is not blocked
   SELECT COUNT(*)
   INTO   OUT_COUNT
   FROM   FTD_APPS.FLORIST_ZIPS
   WHERE  FLORIST_ID = IN_FLORIST_ID
   AND    ZIP_CODE = IN_ZIP_CODE
   AND    block_start_date is null;

END GET_ZIP_COUNT_FOR_FLORIST;



FUNCTION GIFT_CERTIFICATE_EXISTS (
   IN_GIFT_CERTIFICATE_ID IN VARCHAR2 )
   RETURN VARCHAR2
AS
/*------------------------------------------------------------------------------
Description:
   Return Y or N if the given state master id exists.
------------------------------------------------------------------------------*/

V_COUNT  NUMBER;
V_EXISTS CHAR(1) := 'N';

BEGIN

SELECT COUNT (1)
INTO   V_COUNT
FROM   CLEAN.GC_COUPONS
WHERE  GC_COUPON_NUMBER = IN_GIFT_CERTIFICATE_ID;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

END GIFT_CERTIFICATE_EXISTS;


FUNCTION STATE_MASTER_EXISTS (
   IN_STATE_MASTER_ID IN FTD_APPS.STATE_MASTER.STATE_MASTER_ID%TYPE )
   RETURN VARCHAR2
AS
/*------------------------------------------------------------------------------
Description:
   Return Y or N if the given state master id exists.
------------------------------------------------------------------------------*/

V_COUNT  NUMBER;
V_EXISTS CHAR(1) := 'N';

BEGIN

SELECT COUNT (1)
INTO   V_COUNT
FROM   FTD_APPS.STATE_MASTER
WHERE  STATE_MASTER_ID = IN_STATE_MASTER_ID;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

END STATE_MASTER_EXISTS;


FUNCTION ZIP_CODE_EXISTS (
   IN_ZIP_CODE_ID IN FTD_APPS.ZIP_CODE.ZIP_CODE_ID%TYPE )
   RETURN VARCHAR2
AS
/*------------------------------------------------------------------------------
Description:
   Return Y or N if the given zip code id exists.
------------------------------------------------------------------------------*/

V_COUNT  NUMBER;
V_EXISTS CHAR(1) := 'N';

ALPHACHARS VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
ALPHASUB   VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
NUMCHARS   VARCHAR2(10) := '0123456789';
NUMSUB     VARCHAR2(10) := '##########';

TMPZIP VARCHAR2(5);
NUM_CHARS NUMBER;

BEGIN

-- Determine if the input zip code is Canadian
IF ( TRANSLATE(IN_ZIP_CODE_ID, ALPHACHARS || NUMCHARS, ALPHASUB || NUMSUB) LIKE '@#@%' ) THEN
   -- If Canadian, use only first 3 characters
   NUM_CHARS := 3;
ELSE
   -- Otherwise, use first 5
   NUM_CHARS := 5;
END IF;

TMPZIP := UPPER(SUBSTR(IN_ZIP_CODE_ID,1,NUM_CHARS));

SELECT COUNT (1)
INTO   V_COUNT
FROM   FTD_APPS.ZIP_CODE
WHERE  SUBSTR (ZIP_CODE_ID, 1, NUM_CHARS) = SUBSTR (TMPZIP, 1, NUM_CHARS);

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

END ZIP_CODE_EXISTS;


FUNCTION GET_BULK_ORDER_ID RETURN NUMBER

/*------------------------------------------------------------------------------
Description:
   Return the next bulk order id in the sequence.
------------------------------------------------------------------------------*/

AS
V_BULK_ORDER_ID NUMBER;
BEGIN
SELECT  FTD_APPS.BULK_ORDER_ID.NEXTVAL
INTO    V_BULK_ORDER_ID
FROM    DUAL;

RETURN V_BULK_ORDER_ID;

END GET_BULK_ORDER_ID;

FUNCTION GET_ZIPCODE_CUTOFF (
    IN_ZIP_CODE   IN VARCHAR2,
    IN_PRODUCT_ID IN VARCHAR2 )
  RETURN types.ref_cursor

/*------------------------------------------------------------------------------
Description:
   Return the cutoff time for the passed product in the passed zip.
------------------------------------------------------------------------------*/

AS
    cur_cursor          types.ref_cursor;
    --zipCutoff           CSZ_AVAIL.LATEST_CUTOFF%TYPE := NULL;
    alphaChars          VARCHAR2(52) := 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    alphaSub            VARCHAR2(52) := '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@';
    numChars            VARCHAR2(10) := '0123456789';
    numSub              VARCHAR2(10) := '##########';
    cszZip              VARCHAR2(6);
    cleanZip            VARCHAR2(10) := Oe_Cleanup_Alphanum_String(UPPER(IN_ZIP_CODE), 'Y');

BEGIN
    -- Determine if the input zip code is Canadian
    IF ( TRANSLATE(cleanZip, alphaChars || numChars, alphaSub || numSub) LIKE '@#@%' )
    THEN
        -- If Canadian, use only first 3 characters to join to CSZ_AVAIL
        cszZip := SUBSTR(cleanZip,1,3);
    ELSE
        IF (LENGTH(cleanZip)>5) THEN
            cszZip := SUBSTR(cleanZip,1,5);
        ELSE
            cszZip := cleanZip;
        END IF;
    END IF;

    IF ( IN_PRODUCT_ID IS NULL )
    THEN
        -- No product ID, so get cutoff for zipcode.
        -- Could actually just use query below instead, but this saves us a join.
        BEGIN
            OPEN cur_cursor FOR
                SELECT CA.LATEST_CUTOFF AS ZIPCUTOFF
                FROM
                CSZ_AVAIL CA
                WHERE CA.CSZ_ZIP_CODE=cszZip;

            RETURN cur_cursor;
        END;
    ELSE
        -- Product ID was set, so join on csz_products in case codified product
        BEGIN
            -- Codified product cutoff takes precedence
            OPEN cur_cursor FOR
                SELECT NVL(CP.PRODUCT_CUTOFF, CA.LATEST_CUTOFF) AS ZIPCUTOFF
                FROM
                CSZ_AVAIL CA
                LEFT JOIN CSZ_PRODUCTS CP ON
                CA.CSZ_ZIP_CODE=CP.ZIP_CODE AND CP.PRODUCT_ID=IN_PRODUCT_ID
                WHERE CA.CSZ_ZIP_CODE=cszZip;

            RETURN cur_cursor;
        END;
    END IF;

END;

PROCEDURE GET_COMPANY
(
IN_COMPANY_ID  IN  FTD_APPS.COMPANY_MASTER.COMPANY_ID%TYPE,
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns company for the given company id.

Input:
company_id

Output:
cursor containing company info

------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CURSOR FOR
SELECT company_name,
    internet_origin,
    default_program_id,
    phone_number,
    enable_lp_processing,
    clearing_member_number,
    logo_filename,
    url,
    email_direct_mail_flag,
    order_flag,
    report_flag,
    sds_brand
FROM  ftd_apps.company_master
WHERE company_id = in_company_id
and   (email_direct_mail_flag = 'Y' or order_flag = 'Y' or report_flag = 'Y');

END GET_COMPANY;


PROCEDURE GET_GNADD_STATUS
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------

!!! REMOVE THIS PROC !!!
2009 - THIS PROC CAN PROBABLY BE REMOVED SINCE THERE DON'T APPEAR TO BE ANY
REFERENCES TO IT.  ONLY ORDER-PROCESSING HAS IT INCLUDED IN A STATMENTS FILE
BUT NO JAVA CODE REFERENCES IT. 

...AND SINCE GNADD_STATUS NO LONGER EXISTS IN GLOBAL_PARMS, THIS WOULD
ALWAYS RETURN NULL ANYHOW

Description:
This stored procedure will return gnadd status, date and hold data in
one record from table global_parms.

Input:
N/A

Output:
cursor containing global parameters

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CURSOR FOR
SELECT gs.name,
  gs.value,
  gd.name,
  gd.value,
  gh.name,
  gh.value
FROM frp.global_parms gs,
  frp.global_parms gd,
  frp.global_parms gh
WHERE gs.name = 'GNADD_STATUS'
AND gd.name = 'GNADD_DATE'
AND gh.name = 'GNADD_HOLD';

END GET_GNADD_STATUS;


FUNCTION GET_ORIGIN_TYPE
(
IN_ORIGIN_ID  IN FTD_APPS.ORIGINS.ORIGIN_ID%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
This procedure checks if the payment is a full refund for the
payment id passed in.

Input:
payment_id              NUMBER

Output:
v_flag                  CHAR

-----------------------------------------------------------------------------*/
v_origin_type ftd_apps.origins.origin_type%TYPE;

BEGIN

BEGIN

SELECT origin_type
INTO v_origin_type
FROM ftd_apps.origins
WHERE origin_id = in_origin_id;

EXCEPTION WHEN NO_DATA_FOUND THEN v_origin_type := NULL;

END;

RETURN v_origin_type;

END GET_ORIGIN_TYPE;


PROCEDURE GET_CSZ_PRODUCTS
(
IN_ZIP_CODE  IN FTD_APPS.CSZ_PRODUCTS.ZIP_CODE%TYPE,
OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure returns records from csz_products based on
the zip code passed in.

Input:
zip_code    VARCHAR2

Output:
cursor containing csz_products info

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cursor FOR
SELECT csz.product_id,
     pm.novator_id,
     csz.zip_code,
     csz.available_flag,
     csz.product_cutoff
FROM ftd_apps.csz_products csz,
     ftd_apps.product_master pm
WHERE csz.zip_code = in_zip_code
 AND pm.product_id = csz.product_id;


END GET_CSZ_PRODUCTS;


PROCEDURE GET_COMPANY_MASTER_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records from table company_master.

Input:
N/A

Output:
cursor containing company_master info
------------------------------------------------------------------------------*/
BEGIN

out_cursor := ftd_apps.sp_get_company_master_list();

END GET_COMPANY_MASTER_LIST;


PROCEDURE GET_PARTNERS_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records in table partners.

Input:
N/A

Output:
cursor containing partners info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT partner_id,
    partner_name,
    customer_info,
    reward_name,
    id_length,
    email_exclude_flag
FROM ftd_apps.partners
ORDER BY partner_name;

END GET_PARTNERS_LIST;


PROCEDURE GET_PRICE_HEADER_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records in table price_header.

Input:
N/A

Output:
cursor containing price_header info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT price_header_id,
    description
FROM ftd_apps.price_header
ORDER BY price_header_id;

END GET_PRICE_HEADER_LIST;


PROCEDURE GET_VENDOR_DELIVERY_RESTRICT
(
IN_PRODUCT_ID  IN FTD_APPS.VENDOR_PRODUCT.PRODUCT_SUBCODE_ID%TYPE,
OUT_CURSOR   OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns delivery restrictions for the given product id.
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT start_date,
     end_date
FROM ftd_apps.prod_deliv_block_list_vw
WHERE product_id = in_product_id;

END GET_VENDOR_DELIVERY_RESTRICT;


PROCEDURE GET_PRICE_HEADER_DETAILS
(
IN_PRICE_HEADER_ID FTD_APPS.PRICE_HEADER.PRICE_HEADER_ID%TYPE,
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records in tables price_header and price_header_details that
are related to the price_header_id passed in.

Input:
price header id  VARCHAR2

Output:
cursor containing price_header and price_header_details info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT ph.price_header_id,
    ph.description,
    phd.min_dollar_amt,
    phd.max_dollar_amt,
    phd.discount_type,
    phd.discount_amt
FROM ftd_apps.price_header_details phd,
    ftd_apps.price_header ph
WHERE ph.price_header_id  = in_price_header_id
AND phd.price_header_id = ph.price_header_id
ORDER BY price_header_id;

END GET_PRICE_HEADER_DETAILS;


PROCEDURE GET_PRICE_DISCOUNT_TYPE_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns the price discount types from table price_discount_type_val.

Input:
N/A

Output:
cursor containing price_discount_type_val info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT price_discount_type,
    description
FROM ftd_apps.price_discount_type_val
WHERE status = 'Active'
ORDER BY price_discount_type;

END GET_PRICE_DISCOUNT_TYPE_LIST;


PROCEDURE GET_PRODUCT_CATEGORY_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records from table product_category.

Input:
N/A

Output:
cursor containing product category info
------------------------------------------------------------------------------*/
BEGIN

out_cursor := ftd_apps.sp_get_category_list();

END GET_PRODUCT_CATEGORY_LIST;


PROCEDURE GET_SOURCE_MKT_GRP_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns a list of distinct source_type_val groups in table source.

Input:
N/A

Output:
cursor containing source info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT stv.source_type,
    sm.source_code
FROM ftd_apps.source_type_val stv,
    ftd_apps.source_master sm
WHERE stv.source_type = sm.source_type (+)
ORDER BY stv.source_type,
       sm.source_code;

END GET_SOURCE_MKT_GRP_LIST;


PROCEDURE GET_BONUS_TYPE_VAL_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the active records in table bonus_type.

Input:
N/A

Output:
cursor containing bonus_type info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT bonus_type,
    description
FROM ftd_apps.bonus_type_val
WHERE status = 'Active';

END GET_BONUS_TYPE_VAL_LIST;


PROCEDURE GET_MILEAGE_CALC_SRC_VAL_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the active records in table mileage_calc_source_val.

Input:
N/A

Output:
cursor containing mileage_calc_source_val info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT mileage_calculation_source,
    description
FROM ftd_apps.mileage_calc_source_val
WHERE status = 'Active'
ORDER BY description;

END GET_MILEAGE_CALC_SRC_VAL_LIST;


PROCEDURE GET_ORDER_SOURCE_VAL_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the active records in table order_source_val.

Input:
N/A

Output:
cursor containing order_source_val info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT order_source,
    description
FROM ftd_apps.order_source_val
WHERE status = 'Active';

END GET_ORDER_SOURCE_VAL_LIST;


PROCEDURE GET_AAA_MEMBERS_LIST
(
OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records in table aaa_members.

Input:
N/A

Output:
cursor containing aaa_members info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT aaa_member_id,
    member_name
FROM ftd_apps.aaa_members
ORDER BY aaa_member_id;

END GET_AAA_MEMBERS_LIST;


PROCEDURE GET_COST_CENTERS
(
IN_COST_CENTER_ID  IN FTD_APPS.COST_CENTERS.COST_CENTER_ID%TYPE,
IN_PARTNER_ID      IN FTD_APPS.COST_CENTERS.PARTNER_ID%TYPE,
OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records in table cost_centers for the cost_center_id
and partner_id passed in.

Input:
cost_center_id VARCHAR2
partner_id     VARCHAR2

Output:
cursor containing cost_centers info
------------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
SELECT cost_center_id,
    filler,
    partner_id,
    source_code_id
FROM ftd_apps.cost_centers
WHERE cost_center_id = in_cost_center_id
AND partner_id = in_partner_id;

END GET_COST_CENTERS;


FUNCTION GET_COLOR_BY_ID
(
IN_COLOR_MASTER_ID    IN FTD_APPS.COLOR_MASTER.COLOR_MASTER_ID%TYPE
)
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------
Description:
Return  color id for the given description.
------------------------------------------------------------------------------*/
v_description ftd_apps.color_master.description%TYPE;
BEGIN

BEGIN
SELECT description
INTO v_description
FROM ftd_apps.color_master
WHERE color_master_id = in_color_master_id;

EXCEPTION WHEN NO_DATA_FOUND THEN v_description := NULL;
END;

RETURN v_description;

END GET_COLOR_BY_ID;


FUNCTION IS_CSZ_AVAILABLE
(
IN_ZIP_CODE  FTD_APPS.CSZ_AVAIL.CSZ_ZIP_CODE%TYPE
)
RETURN CHAR
IS
/*-----------------------------------------------------------------------------
Description:
This procedure checks the gnadd_flag for the zip code passed in.

Input:
zip_code        VARCHAR2

Output:
v_flag          CHAR

-----------------------------------------------------------------------------*/
v_flag CHAR(1);
v_gnadd_flag  ftd_apps.csz_avail.gnadd_flag%TYPE;

BEGIN

BEGIN

SELECT gnadd_flag
INTO v_gnadd_flag
FROM ftd_apps.csz_avail
WHERE csz_zip_code = in_zip_code;

EXCEPTION WHEN NO_DATA_FOUND THEN v_gnadd_flag := NULL;

END;


IF v_gnadd_flag IS NULL THEN
v_flag := 'Y';
ELSIF v_gnadd_flag = 'N' THEN
v_flag := 'Y';
ELSE
v_flag := 'N';
END IF;


RETURN v_flag;

END IS_CSZ_AVAILABLE;


PROCEDURE GET_VENDOR_PRODUCTS
(
IN_VENDOR_ID     IN FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE,
OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns all the records in table product_master.

Input:
vendor_id       VARCHAR2

Output:
cursor containing product_master info
------------------------------------------------------------------------------*/
BEGIN

/*OPEN out_cursor FOR
   SELECT pm.product_id,
          pm.product_name,
          ic.inventory_level,
          pm.zone_jump_eligible_flag,
          DECODE (pm.status, 'A', 'Available',
                             'U', 'Unavailble') status
     FROM ftd_apps.product_master pm,
          ftd_apps.inventory_control ic
    WHERE pm.vendor_id = in_vendor_id
      AND ic.product_id (+) = pm.product_id
      AND (pm.subcode_flag = 'N' or pm.subcode_flag is null)
  UNION
   SELECT ps.product_subcode_id product_id,
          pm2.product_name || ' ' || ps.subcode_description product_name,
          ic2.inventory_level,
          pm2.zone_jump_eligible_flag,
          DECODE (pm2.status, 'A', 'Available',
                              'U', 'Unavailble') status
     FROM ftd_apps.product_master pm2,
          ftd_apps.product_subcodes ps,
          ftd_apps.inventory_control ic2
    WHERE pm2.vendor_id = in_vendor_id
      AND ic2.product_id (+) = ps.product_subcode_id
      AND ps.product_id = pm2.product_id
    ORDER BY status,
             product_id;*/

OPEN out_cursor FOR            
   SELECT vp.product_subcode_id product_id,
          pm.product_name,
          ic.inventory_level,
          pm.zone_jump_eligible_flag,
          DECODE (vp.available, 'Y', 'Available',
                             'N', 'Unavailble') status
     FROM ftd_apps.vendor_product vp,
	       ftd_apps.product_master pm,
          ftd_apps.inventory_control ic
    WHERE vp.vendor_id = in_vendor_id
      AND ic.product_id (+) = vp.product_subcode_id
	   AND vp.product_subcode_id = pm.product_id	   	   
  UNION
   SELECT vp.product_subcode_id product_id,
          pm2.product_name || ' ' || ps.subcode_description product_name,
          ic2.inventory_level,
          pm2.zone_jump_eligible_flag,
          DECODE (vp.available, 'Y', 'Available',
                             'N', 'Unavailble') status
     FROM ftd_apps.product_master pm2,
          ftd_apps.product_subcodes ps,
		    ftd_apps.vendor_product vp,
          ftd_apps.inventory_control ic2
    WHERE vp.vendor_id = in_vendor_id
      AND ic2.product_id (+) = vp.product_subcode_id
      AND ps.product_id = pm2.product_id
	   AND vp.product_subcode_id = ps.product_subcode_id
    ORDER BY status,
             product_id;             

END GET_VENDOR_PRODUCTS;


PROCEDURE INSERT_BUSINESS
(
IN_BUSINESS   IN VARCHAR2,
IN_TYPE       IN VARCHAR2,
IN_NAME       IN VARCHAR2,
IN_ADDRESS    IN VARCHAR2,
IN_CITY       IN VARCHAR2,
IN_STATE      IN VARCHAR2,
IN_ZIPCODE    IN VARCHAR2,
IN_PHONE      IN VARCHAR2,
OUT_STATUS   OUT VARCHAR2,
OUT_MESSAGE  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
This stored procedure calls a stored procedure to insert a record
into table business.

Input:
business   VARCHAR2
type       VARCHAR2
name       VARCHAR2
address    VARCHAR2
city       VARCHAR2
state      VARCHAR2
zipcode    VARCHAR2
phone      VARCHAR2

Output:
status
error message

-----------------------------------------------------------------------------*/
BEGIN

FTD_APPS.OE_INSERT_INSTITUTION(in_business, in_type, in_name, in_address, in_city, in_state, in_zipcode, in_phone);

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
ROLLBACK;
out_status := 'N';
out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;

END INSERT_BUSINESS;


PROCEDURE GET_INVENTORY_DATA
(
IN_PRODUCT_ID                   IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_VENDOR_ID                      IN FTD_APPS.PRODUCT_MASTER.VENDOR_ID%TYPE,
OUT_INVENTORY_CURSOR           OUT TYPES.REF_CURSOR,
OUT_INV_NOTIFICATIONS_CURSOR   OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves inventory_control and product_master data in one
cursor for the product id passed in, and inv_ctrl_notifications data in
another cursor for the product_id and vendor_id passed in.

Input:
product_id        VARCHAR2
vendor_id         VARCHAR2

Output:
cursor containing inventory_control and product_master info
cursor containing inv_ctrl_notifications info

-----------------------------------------------------------------------------*/
v_is_product CHAR(1);

BEGIN

  BEGIN
    SELECT 'Y'
      INTO v_is_product
      FROM ftd_apps.product_master
     WHERE product_id = in_product_id;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_is_product := 'N';
  END;


  IF v_is_product = 'Y' THEN

    OPEN out_inventory_cursor FOR
      SELECT pm.product_id,
             DECODE (vp.available, 'Y', 'Available',
                                   'N', 'Unavailable') status,
             pm.product_name,
             vm.vendor_id,
             vm.vendor_name,
             ic.updated_on,
             ic.contact_number,
             ic.contact_extension,
             ic.start_date,
             ic.end_date,
             ic.inventory_level,
             ic.notice_threshold
        FROM ftd_apps.inventory_control ic,
             ftd_apps.product_master pm,
             ftd_apps.vendor_master vm,
             ftd_apps.vendor_product vp
       WHERE pm.product_id = in_product_id
         AND vm.vendor_id = in_vendor_id
         AND vp.vendor_id = in_vendor_id
         AND vp.product_subcode_id = in_product_id
         AND ic.product_id (+) = pm.product_id
         AND ic.vendor_id (+) = in_vendor_id;

  ELSE

   OPEN out_inventory_cursor FOR
      SELECT ps.product_subcode_id product_id,
             DECODE (vp.available, 'Y', 'Available',
                                   'N', 'Unavailable') status,
             pm.product_name || ' ' || ps.subcode_description product_name,
             vm.vendor_id,
             vm.vendor_name,
             ic.updated_on,
             ic.contact_number,
             ic.contact_extension,
             ic.start_date,
             ic.end_date,
             ic.inventory_level,
             ic.notice_threshold
        FROM ftd_apps.inventory_control ic,
             ftd_apps.product_master pm,
             ftd_apps.product_subcodes ps,
             ftd_apps.vendor_master vm,
             ftd_apps.vendor_product vp
       WHERE ps.product_subcode_id = in_product_id
         AND ic.product_id (+) = ps.product_subcode_id
         AND pm.product_id = ps.product_id
         AND vm.vendor_id = in_vendor_id
         AND vp.vendor_id = in_vendor_id
         AND vp.product_subcode_id = in_product_id
         AND ic.vendor_id (+) = in_vendor_id;

  END IF;


OPEN out_inv_notifications_cursor FOR
  SELECT product_id,
         email_address
    FROM ftd_apps.inv_ctrl_notifications
   WHERE product_id = in_product_id
     AND vendor_id = in_vendor_id;

END GET_INVENTORY_DATA;


PROCEDURE INVENTORY_CONTROL_REPORT
(
IN_PRODUCT_ID           IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_VENDOR_ID            IN FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE,
OUT_ORDER_COUNT        OUT NUMBER,
OUT_INVENTORY_LEVEL    OUT NUMBER,
OUT_WEEK_TOTAL         OUT NUMBER,
OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves four different kinds of information on the product
id passed in:
1) the total count of vendor delivered orders for the last 30 days,
2) the current inventory level
3) the count of delivered orders for the product for the past 7 days
4) the total number of orders for this product for each day during the
 last seven days

Input:
product_id         VARCHAR2
vendor_id          VARCHAR2

Output:
order_count        NUMBER
inventory_level    NUMBER
week_total         NUMBER
out_cursor         REF_CURSOR

NOTE: This procedure is replaced by the overload procedure below.  This proc
      should only exist until the front end is changed to handle the
      way the data is returned in the overload procedure.
-----------------------------------------------------------------------------*/

cursor inv_cur is
        SELECT  inventory_level
        FROM    ftd_apps.inventory_control
        WHERE   product_id = in_product_id;

cursor c30_cur is
        SELECT  COUNT(1)
        FROM    venus.venus v
        WHERE   v.msg_type = 'FTD'
        AND     v.product_id = in_product_id
        AND     v.order_date >= TRUNC(sysdate - 29)
        AND     NOT EXISTS (    SELECT  1
                                FROM    venus.venus v2
                                WHERE   v2.venus_order_number = v.venus_order_number
                                AND     v2.msg_type = 'CAN');
cursor c7_cur is
        SELECT  COUNT(1)
        FROM    venus.venus v
        WHERE   v.msg_type = 'FTD'
        AND     v.product_id = in_product_id
        AND     v.order_date >= TRUNC(sysdate - 6)
        AND     NOT EXISTS (    SELECT  1
                                FROM    venus.venus v2
                                WHERE   v2.venus_order_number = v.venus_order_number
                                AND     v2.msg_type = 'CAN');

cursor daily_cur (p_date date)is
        SELECT  COUNT(1)
        FROM    venus.venus v
        WHERE   v.msg_type = 'FTD'
        AND     v.product_id = in_product_id
        AND     TRUNC (v.order_date) = TRUNC(p_date)
        AND     NOT EXISTS (    SELECT 1
                                FROM    venus.venus v2
                                WHERE   v2.venus_order_number = v.venus_order_number
                                AND     v2.msg_type = 'CAN');

v_date          date;
v_day_name      varchar2(30);
v_count         integer := 0;

BEGIN

-- initialize the out parms
out_inventory_level := 0;
out_order_count := 0;
out_week_total := 0;

-- get the current inventory level
OPEN inv_cur;
FETCH inv_cur INTO out_inventory_level;
CLOSE inv_cur;

-- get the count of orders delivered in the last 30 days
OPEN c30_cur;
FETCH c30_cur INTO out_order_count;
CLOSE c30_cur;

-- get the count of order delivered in the last 7 days
OPEN c7_cur;
FETCH c7_cur INTO out_week_total;
CLOSE c7_cur;

-- get the daily counts
DELETE FROM rep_tab;

FOR i IN 0..6
LOOP
        v_date := SYSDATE - i;
        v_day_name:= TO_CHAR((SYSDATE - i), 'Day') ;

        OPEN daily_cur (v_date);
        FETCH daily_cur INTO v_count;
        CLOSE daily_cur;

        INSERT INTO rep_tab
        (
                col1,
                col2,
                col3,
                col4
        )
        VALUES
        (
                TO_CHAR(v_date,'YYYY-MM-DD HH:MI:SS'),
                v_day_name,
                v_count,
                TO_CHAR(v_date,'D')
        );

END LOOP;

OPEN out_cursor FOR
        SELECT  col1 "date",
                col2 "day",
                col3 "row_count"
        FROM    rep_tab
        ORDER BY col4;

END INVENTORY_CONTROL_REPORT;


PROCEDURE INVENTORY_CONTROL_REPORT
(
IN_PRODUCT_ID           IN FTD_APPS.PRODUCT_MASTER.PRODUCT_ID%TYPE,
IN_VENDOR_ID            IN FTD_APPS.VENDOR_MASTER.VENDOR_ID%TYPE,
OUT_HEADING_CUR         OUT TYPES.REF_CURSOR,
OUT_COUNT_CUR           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves four different kinds of information on the product
id passed in:
1) the total count of vendor delivered orders for the last 30 days,
2) the current inventory level
3) the count of delivered orders for the product for the past 7 days
4) the total number of orders for this product for each day during the
 last seven days

Input:
product_id         VARCHAR2
vendor_id          VARCHAR2

Output:
out_cursor         REF_CURSOR

-----------------------------------------------------------------------------*/
cursor inv_cur is
        SELECT  inventory_level
        FROM    ftd_apps.inventory_control
        WHERE   product_id = in_product_id;

v_inventory_level       ftd_apps.inventory_control.inventory_level%type;

-- this table stores the day indexes to getting daily counts for the last week
type id_tab is table of integer index by pls_integer;
v_day_id                id_tab;
v_days                  integer;
v_ind                   integer;

BEGIN

-- initialize the out parms
v_inventory_level := 0;

-- get the current inventory level
OPEN inv_cur;
FETCH inv_cur INTO v_inventory_level;
CLOSE inv_cur;

-- get the indexes for the daily counts
-- an example of what this code does
--      today = tuesday
--      v_ind = 3 so table index 3 is set to 0 to have the tuesday position in the sql be sysdate - 0
--      fill table positions 4-7 with decrementing values starting with 6
--      fill table positions 1-2 with remaining decrementing values
--      end table result:
--              v_day_id (1) = 2 --> sysdate-2 --> sunday of the same week
--              v_day_id (2) = 1 --> sysdate-1 --> monday of the same week
--              v_day_id (3) = 0 --> sysdate-0 --> today
--              v_day_id (4) = 6 --> sysdate-6 --> wednesday of last week
--              v_day_id (5) = 5 --> sysdate-5 --> thursday of last week
--              v_day_id (6) = 4 --> sysdate-4 --> friday of last week
--              v_day_id (7) = 3 --> sysdate-3 --> saturday of last week

v_ind := to_number (to_char (sysdate, 'd'));
v_day_id (v_ind) := 0;

v_days := 6;
if v_ind < 7 then
        for x in v_ind+1..7 loop
                v_day_id (x) := v_days;
                v_days := v_days - 1;
        end loop;
end if;

if v_ind > 1 then
        for x in 1..v_ind-1 loop
                v_day_id (x) := v_days;
                v_days := v_days - 1;
        end loop;
end if;

-- return dates for headings
OPEN out_heading_cur FOR
        SELECT  to_char (sysdate-v_day_id (1), 'YYYY-MM-DD HH:MI:SS') sunday,
                to_char (sysdate-v_day_id (2), 'YYYY-MM-DD HH:MI:SS') monday,
                to_char (sysdate-v_day_id (3), 'YYYY-MM-DD HH:MI:SS') tuesday,
                to_char (sysdate-v_day_id (4), 'YYYY-MM-DD HH:MI:SS') wednesday,
                to_char (sysdate-v_day_id (5), 'YYYY-MM-DD HH:MI:SS') thursday,
                to_char (sysdate-v_day_id (6), 'YYYY-MM-DD HH:MI:SS') friday,
                to_char (sysdate-v_day_id (7), 'YYYY-MM-DD HH:MI:SS') saturday
        FROM    dual;

-- return counts
OPEN out_count_cur FOR
        SELECT  count (1) month_count,
                v_inventory_level inventory_level,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (1)) then 1 else null end) sun_count,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (2)) then 1 else null end) mon_count,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (3)) then 1 else null end) tue_count,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (4)) then 1 else null end) wed_count,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (5)) then 1 else null end) thur_count,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (6)) then 1 else null end) fri_count,
                count (case when trunc (v.order_date)=TRUNC(sysdate-v_day_id (7)) then 1 else null end) sat_count,
                count (case when v.order_date >=TRUNC(sysdate - 6) then 1 else null end) week_count
        FROM    venus.venus v
        WHERE   v.msg_type = 'FTD'
        AND     v.product_id = in_product_id
        AND     v.order_date >= TRUNC(sysdate - 29)
        AND     NOT EXISTS (    SELECT  1
                                FROM    venus.venus v2
                                WHERE   v2.venus_order_number = v.venus_order_number
                                AND     v2.msg_type = 'CAN');


END INVENTORY_CONTROL_REPORT;


PROCEDURE GET_PRODUCT_BY_ANY_ID
(productId      IN  varchar2,
out_cur        out types.ref_cursor
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves product information for a given product or
sub code

Input:
product_id         VARCHAR2

Output:
cursor containing product information
-----------------------------------------------------------------------------*/

subCode    ftd_apps.PRODUCT_SUBCODES.PRODUCT_SUBCODE_ID%TYPE := NULL;

BEGIN

BEGIN
SELECT PRODUCT_SUBCODE_ID
INTO subCode
FROM ftd_apps.PRODUCT_SUBCODES
WHERE PRODUCT_SUBCODE_ID = UPPER(productId);
EXCEPTION WHEN OTHERS THEN
NULL;
END;

OPEN out_cur FOR
SELECT product_id       as "productId",
       novator_id       as "novatorId",
       product_name     as "productName",
       novator_name     as "novatorName",
       status           as "status",
       delivery_type    as "deliveryType",
       category         as "category",
       product_type     as "productType",
       product_sub_type as "productSubType",
       color_size_flag  as "colorSizeFlag",
       standard_price   as "standardPrice",
       deluxe_price     as "deluxePrice",
       premium_price    as "premiumPrice",
       preferred_price_point    as "preferredPricePoint",
       variable_price_max       as "variablePriceMax",
       short_description        as "shortDescription",
       long_description         as "longDescription",
       florist_reference_number as "floristReferenceNumber",
       mercury_description      as "mercuryDescription",
       item_comments            as "itemsComments",
       add_on_balloons_flag     as "addOnBallonsFlag",
       add_on_bears_flag        as "addOnBearsFlag",
       add_on_cards_flag        as "addOnCardsFlag",
       add_on_funeral_flag      as "addOnFuneralFlag",
       codified_flag            as "codifiedFlag",
       exception_code           as "exceptionCode",
       exception_start_date     as "exceptionStartDate",
       exception_end_date       as "exceptionEndDate",
       exception_message        as "exceptionMessage",
       null                     as "vendorId",
       null                     as "vendorCost" ,
       null                     as "vendorSku",
       second_choice_code       as "secondChoiceCode",
       holiday_second_choice_code as "holidaySecondChoiceCode",
       dropship_code              as "dropshipCode",
       discount_allowed_flag      as "discountAllowedFlag",
       delivery_included_flag     as "deliveryIncludedFlag",
       no_tax_flag                   as "taxFlag",
       service_fee_flag           as "serviceFeeFlag",
       exotic_flag                as "exoticFlag",
       egift_flag                 as "egiftFlag",
       country_id                 as "countryId",
       arrangement_size           as "arrangementSize",
       arrangement_colors         as "arrangementColors",
       dominant_flowers           as "dominantFlowers",
       search_priority            as "searchPriority",
       standard_recipe            as "recipe",
       subcode_flag               as "subcodeFlag",
       dim_weight                 as "dimWeight",
       next_day_upgrade_flag      as "nextDayUpgradeFlag",
       corporate_site             as "corporateSite",
       unspsc_code                as "unspscCode",
       price_rank_1               as "priceRank1",
       price_rank_2               as "priceRank2",
       price_rank_3               as "priceRank3",
       ship_method_carrier        as "shipMethodCarrier",
       ship_method_florist        as "shipMethodFlorist",
       shipping_key               as "shippingKey",
       last_update                as "lastUpdate",
       variable_price_flag        as "variablePriceFlag",
       holiday_sku                as "holidaySku",
       holiday_price              as "holidayPrice",
       catalog_flag               as "catalogFlag",
       holiday_deluxe_price       as "holidayDeluxePrice",
       holiday_premium_price      as "holidayPremiumPrice",
       holiday_start_date         as "holidayStartDate",
       holiday_end_date           as "holidayEndDate",
       mercury_second_choice      as "mercurySecondChoice",
       mercury_holiday_second_choice as "mercuryHolidaySecondChoice",
       add_on_chocolate_flag      as "addOnChocolateFlag",
       JCP_Category               as "JCPenneyCategory",
       CROSS_REF_NOVATOR_ID       as "crossRefNovatorID",
       GENERAL_COMMENTS           as "generalComments",
       SENT_TO_NOVATOR_PROD       as "sentToNovatorProd",
       SENT_TO_NOVATOR_UAT          as "sentToNovatorUat",
       SENT_TO_NOVATOR_TEST         as "sentToNovatorTest",
       SENT_TO_NOVATOR_CONTENT    as "sentToNovatorContent",
       DEFAULT_CARRIER              as "defaultCarrier",
         HOLD_UNTIL_AVAILABLE       as "holdUntilAvailable",
         last_update_user_id          as "lastUpdateUserId",
         last_update_system           as "lastUpdateSystem",
         MONDAY_DELIVERY_FRESHCUT   as "mondayDeliveryFreshcut",
         TWO_DAY_SAT_FRESHCUT  as "twoDayShipSatFreshcut"
 FROM ftd_apps.PRODUCT_MASTER pm
WHERE ( ( subCode IS NULL
  AND ( pm.product_id = UPPER(productId) OR pm.novator_id = UPPER(productId) ))
  OR  ( pm.product_id IN ( SELECT ps.product_id FROM ftd_apps.PRODUCT_SUBCODES ps
                           WHERE ps.product_subcode_id = subCode ) ));
END GET_PRODUCT_BY_ANY_ID;


PROCEDURE GET_INVENTORY_CONTROL
(
IN_PRODUCT_ID   IN FTD_APPS.INVENTORY_CONTROL.PRODUCT_ID%TYPE,
IN_VENDOR_ID    IN FTD_APPS.INVENTORY_CONTROL.VENDOR_ID%TYPE,
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves inventory_control data for the
product_id passed in.

Input:
product_id        VARCHAR2
vendor_id         VARCHAR2

Output:
cursor containing inventory_control info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT product_id,
         updated_on,
         contact_number,
         contact_extension,
         start_date,
         end_date,
         inventory_level,
         notice_threshold
    FROM ftd_apps.inventory_control
   WHERE product_id = in_product_id and vendor_id = in_vendor_id;

END GET_INVENTORY_CONTROL;


PROCEDURE GET_PROD_BY_NOVATOR_KWRD_SRCH (
IN_NOVATOR_ID_LIST      IN VARCHAR2,
IN_PRICE_POINT_ID       IN NUMBER,
IN_SOURCE_CODE          IN VARCHAR2,
IN_DOMESTIC_INTL_FLAG   IN VARCHAR2,
IN_ZIP_CODE             IN VARCHAR2,
IN_DELIVERY_END         IN VARCHAR2,
IN_COUNTRY_ID           IN VARCHAR2,
IN_SCRIPT_CODE          IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
Return the search of products by novator id list
------------------------------------------------------------------------------*/
BEGIN

out_cur := ftd_apps.oe_novator_kwrd_srch_sdg_md04 (
                IN_NOVATOR_ID_LIST,
                IN_PRICE_POINT_ID,
                IN_SOURCE_CODE,
                IN_DOMESTIC_INTL_FLAG,
                IN_ZIP_CODE,
                IN_DELIVERY_END,
                IN_COUNTRY_ID,
                IN_SCRIPT_CODE);

END GET_PROD_BY_NOVATOR_KWRD_SRCH;


PROCEDURE GET_SHIPPING_COST_LIST
(
IN_PRODUCT_ID  IN VARCHAR2,
IN_ZIP_CODE    IN VARCHAR2,
OUT_CURSOR    OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
Returns a list of shipping cost and its description for the product_id
and zip_code passed in.
------------------------------------------------------------------------------*/
BEGIN

out_cursor := FTD_APPS.OE_GET_PRODUCT_SHIP_METHODS(IN_PRODUCT_ID, IN_ZIP_CODE);

END GET_SHIPPING_COST_LIST;


PROCEDURE GET_COUNTRY_MASTER
(
IN_COUNTRY_ID   IN FTD_APPS.COUNTRY_MASTER.COUNTRY_ID%TYPE,
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves country_master data for the country_id passed in.

Input:
country_id        VARCHAR2

Output:
cursor containing country_master info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT country_id,
         name,
         oe_country_type,
         oe_display_order,
         add_on_days,
         cutoff_time,
         monday_closed,
         tuesday_closed,
         wednesday_closed,
         thursday_closed,
         friday_closed,
         saturday_closed,
         sunday_closed,
         three_character_id,
         status
    FROM ftd_apps.country_master
   WHERE country_id = in_country_id;

END GET_COUNTRY_MASTER;


PROCEDURE GET_COUNTRY_MASTER_LIST
(
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves all records from the country_master table.

Input:
N/A

Output:
cursor containing country_master info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT country_id,
         name,
         oe_country_type,
         oe_display_order,
         add_on_days,
         cutoff_time,
         monday_closed,
         tuesday_closed,
         wednesday_closed,
         thursday_closed,
         friday_closed,
         saturday_closed,
         sunday_closed,
         three_character_id,
         status
    FROM ftd_apps.country_master;

END GET_COUNTRY_MASTER_LIST;


PROCEDURE GET_GLOBAL_HOLIDAY_CALENDAR
(
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves all records from the holiday_calendar table
that are not global.

Input:
N/A

Output:
cursor containing country_master info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT holiday_date,
         holiday_description,
         global_code
    FROM ftd_apps.holiday_calendar
   WHERE global_code <> 'N';

END GET_GLOBAL_HOLIDAY_CALENDAR;


PROCEDURE GET_COUNTRY_HOLIDAYS
(
IN_COUNTRY_ID   IN FTD_APPS.COUNTRY_MASTER.COUNTRY_ID%TYPE,
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves all records from the holiday_country and
holiday_calendar tables for the country_id passed in and have a holiday
date that is at least within the last thirty days.

Input:
country_id        VARCHAR2

Output:
cursor containing holiday_country and holiday_calendar info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT hco.country_id,
         hco.holiday_date,
         hco.deliverable_flag,
         hco.shipping_allowed,
         hcal.holiday_description
    FROM ftd_apps.holiday_country hco,
         ftd_apps.holiday_calendar hcal
   WHERE hco.holiday_date = hcal.holiday_date
     AND hco.country_id = in_country_id
     AND hco.holiday_date >= TRUNC(SYSDATE) - 30;

END GET_COUNTRY_HOLIDAYS;


PROCEDURE GET_CUSTOM_COUNTRIES
(
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
This procedure retrieves all records from the country_master table
that has a record in table holiday_country for a holiday
date that is at least within the last thirty days.

Input:
N/A

Output:
cursor containing holiday_country and holiday_calendar info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
  SELECT cm.country_id,
         cm.name,
         cm.oe_country_type,
         cm.oe_display_order,
         cm.add_on_days,
         cm.cutoff_time,
         cm.monday_closed,
         cm.tuesday_closed,
         cm.wednesday_closed,
         cm.thursday_closed,
         cm.friday_closed,
         cm.saturday_closed,
         cm.sunday_closed,
         cm.three_character_id,
         cm.status
    FROM ftd_apps.country_master cm
   WHERE EXISTS (SELECT 1
                   FROM ftd_apps.holiday_country hc
                  WHERE hc.country_id = cm.country_id
                    AND hc.holiday_date >= TRUNC(SYSDATE) - 30);

END GET_CUSTOM_COUNTRIES;



FUNCTION IS_FLOWER_MONTHLY_PRODUCT
(
IN_PRODUCT_ID   IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
This function returns if the given product is a flower monthly product

Input:
product_id

Output:
Y/N if the product is a flower monthly product

-----------------------------------------------------------------------------*/
CURSOR check_cur IS
SELECT  'Y'
FROM    ftd_apps.flower_monthly_product
WHERE   product_id = in_product_id;

v_return        varchar2(1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_return;
CLOSE check_cur;

RETURN v_return;

END IS_FLOWER_MONTHLY_PRODUCT;
PROCEDURE GET_ORDER_SEQUENCE  (
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns codified add on information for the given occasion id.
------------------------------------------------------------------------------*/
BEGIN
   OUT_CUR := FTD_APPS.OE_GET_ORDER_SEQUENCE;
END GET_ORDER_SEQUENCE;

PROCEDURE GET_SEQUENCE_UTIL  (
   OUT_CUR              OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Returns codified add on information for the given occasion id.
------------------------------------------------------------------------------*/
BEGIN
   OUT_CUR := FTD_APPS.OE_SEQUENCE_UTIL;
END GET_SEQUENCE_UTIL;

PROCEDURE GET_BILLING_INFO_SC ( sourceCode  IN VARCHAR2,
                               prompt      IN VARCHAR2,
                               OUT_CUR     OUT TYPES.REF_CURSOR)
--==============================================================================
--
-- Name:    GET_BILLING_INFO_SC
-- Type:    Function
-- Syntax:  GET_BILLING_INFO_SC ( sourceCode  IN VARCHAR2,
--                                prompt      IN VARCHAR2,
--                                OUT_CUR     OUT TYPES.REF_CURSOR)
--
-- Description:   Queries the BILLING_INFO table and returns all rows for the
--                given source code, or if a prompt is specified just the row
--                with that prompt.
--
--==============================================================================
AS

BEGIN

    OPEN OUT_CUR FOR
        SELECT BILLING_INFO_SEQUENCE AS "billingInfoSequence",
              INFO_DESCRIPTION AS "billingInfoPrompt",
			  DECODE(INFO_DISPLAY, NULL, INFO_DESCRIPTION, INFO_DISPLAY) AS "billingInfoDisplay",
			  DECODE(INFO_FORMAT, NULL, '/.*/', ftd_apps.Oe_Make_Regexp(INFO_FORMAT)) AS "infoFormat",
			  PROMPT_TYPE AS "promptType"
        FROM FTD_APPS.BILLING_INFO
        WHERE SOURCE_CODE_ID = UPPER(sourceCode)
        AND ( prompt IS NULL
          OR INFO_DESCRIPTION = UPPER(prompt) )
		AND ( prompt IS NOT NULL
		  OR INFO_DESCRIPTION != 'CERTIFICATE#' )
		ORDER BY BILLING_INFO_SEQUENCE;
END GET_BILLING_INFO_SC;

PROCEDURE GET_BILLING_INFO_OPt_SC ( sourceCode  IN VARCHAR2,
                               OUT_CUR     OUT TYPES.REF_CURSOR )

--==============================================================================
--
-- Name:    GET_BILLING_INFO_OPT_sc
-- Type:    Function
-- Syntax:  GET_BILLING_INFO_OPT_sc ( sourceCode  IN VARCHAR2,
--                                     OUT_CUR    OUT TYPES.REF_CURSOR )
--
-- Description:   Queries the OE_GET_BILLING_INFO_OPTIONS table and returns all
--				  rows for the given source code.
--
--==============================================================================
AS

BEGIN
    OPEN OUT_CUR FOR
        SELECT BILLING_INFO_DESC AS "billingInfoDesc",
              OPTION_VALUE AS "optionValue",
			  OPTION_NAME AS "optionDisplay",
			  OPTION_SEQUENCE AS "optionSequence"
        FROM FTD_APPS.BILLING_INFO_OPTIONS
        WHERE SOURCE_CODE_ID = UPPER(sourceCode)
        ORDER BY BILLING_INFO_DESC, OPTION_SEQUENCE;
END GET_BILLING_INFO_OPT_SC;

PROCEDURE COST_CENTER_EXISTs (
  inCostCenterId IN VARCHAR2,
  inPartnerId    IN VARCHAR2,
  inSourceCode   IN VARCHAR2,
   OUT_BOOL     OUT VARCHAR2)

--=================================================================
--
-- Name:    OE_GET_COST_CENTER
-- Type:    Function
-- Syntax:  OE_GET_COST_CENTER (inCostCenterId IN VARCHAR2,
--                              inPartnerId IN VARCHAR2,
--                              inSourceCode IN VARCHAR2)
-- Returns: ref_cursor for
--          costCenterId       VARCHAR2
--
-- Description:  Used to validate Cost Center IDs (for ADVO, Target, etc.).
--               Note that COST_CENTERS rows with NULL source_code's represent
--               cost_center_id's that apply to all source_codes for that
--               partner (e.g., ADVO).
--
--==================================================================

AS
v_count    number(6);
BEGIN
        SELECT count(*)
        INTO   v_count
        FROM FTD_APPS.COST_CENTERS
        WHERE COST_CENTER_ID = inCostCenterId
        AND   PARTNER_ID = inPartnerId
        AND   NVL(SOURCE_CODE_ID,'NVL') = NVL(inSourceCode,'NVL');

   IF v_count >= 1 THEN
       OUT_BOOL := 'TRUE';
   ELSE
       OUT_BOOL := 'FALSE';
   END IF;
EXCEPTION
   WHEN no_data_found then
       OUT_BOOL := 'FALSE';
END COST_CENTER_EXISTs;

PROCEDURE GET_PARTNER_NAME ( IN_SOURCE_CODE IN VARCHAR2,
                            OUT_PARTNER_NAME OUT VARCHAR2,
			    OUT_PROGRAM_NAME OUT VARCHAR2,
                            OUT_STATUS     OUT VARCHAR2,
                            OUT_MESSAGE    OUT VARCHAR2
                           )
AS
BEGIN
        SELECT  pp.partner_name,pp.program_name
        INTO    out_partner_name,out_program_name
        FROM    ftd_apps.source_program_ref spr
        JOIN    ftd_apps.partner_program pp
        ON      spr.program_name = pp.program_name
        WHERE   spr.source_code = IN_SOURCE_CODE
        AND     spr.start_date =
                (
                        select  max(b.start_date)
                        from    ftd_apps.source_program_ref b
                        where   b.source_code = spr.source_code
                        and     b.start_date <= sysdate
                );

EXCEPTION
WHEN OTHERS THEN
    BEGIN
    out_status := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;
END GET_PARTNER_NAME;

PROCEDURE GET_MILES_POINTS
(
 IN_SOURCE_CODE         IN FTD_APPS.SOURCE_MASTER.SOURCE_CODE%TYPE,
 IN_PRODUCT_AMOUNT      IN NUMBER,
 IN_ORDER_AMOUNT        IN NUMBER,
 OUT_MILES_POINTS      OUT NUMBER,
 OUT_REWARD_TYPE       OUT VARCHAR2
)
AS

cursor get_partner_program (in_source_code varchar2) is

     SELECT pp.program_type,
            pr.calculation_basis,
            pr.points,
            pr.bonus_calculation_basis,
            pr.bonus_points,
            pr.maximum_points,
            pp.program_name,
            pr.reward_type
       FROM ftd_apps.source_master sm,
            ftd_apps.source_program_ref spr,
            ftd_apps.program_reward pr,
            ftd_apps.partner_program pp
      WHERE sm.source_code = in_source_code
        AND spr.source_code = sm.source_code
        AND spr.start_date  = ftd_apps.source_query_pkg.get_src_prg_ref_max_start_date(in_source_code, sysdate)
        AND spr.program_name = pp.program_name
        AND pr.program_name (+) = pp.program_name;

v_program_type       varchar2(1000);
v_calc_basis         varchar2(1000);
v_points             number;
v_bonus_calc_basis   varchar2(1000);
v_bonus_points       number;
v_max_points         number;
v_program_name       varchar2(1000);
v_miles_points       number;
v_reward_type        varchar2(1000);

BEGIN

    OPEN get_partner_program (in_source_code);
        FETCH get_partner_program INTO v_program_type, v_calc_basis, v_points,
                v_bonus_calc_basis, v_bonus_points, v_max_points, v_program_name, v_reward_type;
    CLOSE get_partner_program;

    dbms_output.put_line(v_program_name || ' ' || v_program_type || ' ' || v_calc_basis || ' ' || v_points ||
            ' ' || v_bonus_calc_basis || ' ' || v_bonus_points || ' ' || v_max_points || ' ' || v_reward_type);

    out_reward_type := v_reward_type;

    if (v_program_type = 'Default') then

        calculate_rewards(v_calc_basis, v_points, in_product_amount, in_order_amount, v_miles_points);
        out_miles_points := v_miles_points;

        if (v_bonus_calc_basis is not null) then
            calculate_rewards(v_bonus_calc_basis, v_bonus_points, in_product_amount, in_order_amount, v_miles_points);
            out_miles_points := out_miles_points + v_miles_points;
        end if;

    end if;

    dbms_output.put_line('Total points: ' || out_miles_points);
    dbms_output.put_line('-');

END GET_MILES_POINTS;

PROCEDURE CALCULATE_REWARDS
(
 IN_CALC_BASIS          IN VARCHAR2,
 IN_POINTS              IN NUMBER,
 IN_PRODUCT_AMOUNT      IN NUMBER,
 IN_ORDER_AMOUNT        IN NUMBER,
 OUT_TOTAL_POINTS      OUT NUMBER
)
AS

v_return_points         number;
v_base_amount           number := 0;
v_scale                 number := 0;

BEGIN

    if (in_calc_basis = 'F') then
        v_base_amount := 1;
    elsif (in_calc_basis = 'M') then
        v_base_amount := in_product_amount;
    elsif (in_calc_basis = 'T') then
        v_base_amount := in_order_amount;
    else
        dbms_output.put_line('Invalid calculation basis: ' || in_calc_basis);
    end if;

    dbms_output.put_line('v_base_amount: ' || v_base_amount);

    if (round(in_points) <> in_points) then
        v_scale := 2;
    end if;
    v_return_points := round(v_base_amount * in_points, v_scale);
    dbms_output.put_line('v_return_points: ' || v_return_points);

    out_total_points := v_return_points;

END CALCULATE_REWARDS;


--=================================================================
--
-- Name:    GET_SERVICE_CHARGE_DATA
-- Type:    Proceedure
-- Syntax:  GET_SERVICE_CHARGE_DATA (IN_ORDER_DATE IN DATE,
--                                IN_SOURCE_CODE IN FTD_APPS.SOURCE_MASTER_HIST.SOURCE_CODE%TYPE,
--                                IN_DELIVERY_DATE IN FTD_APPS.SERVICE_FEE_OVERRIDE_HIST.OVERRIDE_DATE%TYPE)
-- Returns: ref_cursor for
--          costCenterId       VARCHAR2
--
-- Description:  This proceedure will return the charges with a given order
--               when passed a order date, a source code and a delivery date
--               it will take into account any existing overrides
--               Distinct is used liberally to aviod any repeated timestamps
--               Note that this will return two rows in the cursor if there is an override
--               the first row will always have the amount we currently want
--
--==================================================================
PROCEDURE GET_SERVICE_CHARGE_DATA 
(
    IN_ORDER_DATE IN DATE,
    IN_SOURCE_CODE IN FTD_APPS.SOURCE_MASTER_HIST.SOURCE_CODE%TYPE,
    IN_DELIVERY_DATE IN FTD_APPS.SERVICE_FEE_OVERRIDE_HIST.OVERRIDE_DATE%TYPE,
    OUT_CUR OUT TYPES.REF_CURSOR
)
AS
v_active_snh_id         FTD_APPS.SOURCE_MASTER_HIST.SNH_ID%TYPE := '0';

BEGIN

  SELECT DISTINCT(SMH.snh_id)
  INTO v_active_snh_id
  FROM FTD_APPS.SOURCE_MASTER_HIST  SMH
  WHERE 
   SMH.UPDATED_ON= (
    SELECT MAX(SMH2.UPDATED_ON)
    FROM FTD_APPS.SOURCE_MASTER_HIST SMH2
    WHERE SMH2.SOURCE_CODE = IN_SOURCE_CODE
    AND SMH2.UPDATED_ON < IN_ORDER_DATE
    AND 
    (SMH2.EXPIRED_ON IS NULL OR SMH2.EXPIRED_ON > IN_ORDER_DATE)
  )
  AND SMH.SOURCE_CODE = IN_SOURCE_CODE;
   
  OPEN OUT_CUR FOR
    --Retrieve the override information
    SELECT DISTINCT SFOH.SNH_ID, SFOH.DOMESTIC_CHARGE AS FIRST_ORDER_DOMESTIC, SFOH.INTERNATIONAL_CHARGE AS FIRST_ORDER_INTERNATIONAL, SFOH.VENDOR_CHARGE, SFOH.VENDOR_SAT_UPCHARGE, 'Y' OVERRIDE_FLAG, sfoh.same_day_upcharge, sfoh.same_day_upcharge_fs
    FROM FTD_APPS.SERVICE_FEE_OVERRIDE_HIST SFOH
    wHERE 
        SFOH.SNH_ID = v_active_snh_id
        AND SFOH.OVERRIDE_DATE = TRUNC(IN_DELIVERY_DATE)
        AND SFOH.UPDATED_ON = 
          (SELECT MAX(SFOH2.UPDATED_ON)
            FROM FTD_APPS.SERVICE_FEE_OVERRIDE_HIST SFOH2
            WHERE
              SFOH2.UPDATED_ON < IN_ORDER_DATE
              AND SFOH2.SNH_ID = v_active_snh_id
              AND SFOH2.OVERRIDE_DATE = TRUNC(IN_DELIVERY_DATE)
              AND (SFOH2.EXPIRED_ON IS NULL OR SFOH2.EXPIRED_ON > IN_ORDER_DATE)
          )  
    --retrieve the standard snh information
    UNION ALL
        SELECT DISTINCT SH.SNH_ID, SH.FIRST_ORDER_DOMESTIC, SH.FIRST_ORDER_INTERNATIONAL, SH.VENDOR_CHARGE, SH.VENDOR_SAT_UPCHARGE, 'N' OVERRIDE_FLAG, sh.same_day_upcharge, sh.same_day_upcharge_fs
        FROM FTD_APPS.SNH_HIST SH
        wHERE 
          SH.SNH_ID = v_active_snh_id
          AND SH.UPDATED_ON = 
            (SELECT MAX(SH2.UPDATED_ON)
              FROM FTD_APPS.SNH_HIST SH2
              WHERE
                SH2.UPDATED_ON < IN_ORDER_DATE
                AND SH2.SNH_ID = v_active_snh_id
                AND (SH2.EXPIRED_ON IS NULL OR SH2.EXPIRED_ON > IN_ORDER_DATE)
            );

END GET_SERVICE_CHARGE_DATA;


PROCEDURE GET_ALL_SOURCE_PARTNER_BIN_MAP
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the active partners in table ftd_apps.partner_bin_master table.

Input:
        N/A

Output:
        cursor containing the active partners name
------------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CURSOR FOR
     SELECT UNIQUE PM.PARTNER_NAME
       FROM FTD_APPS.PARTNER_MASTER PM
        WHERE PM.BIN_PROCESSING_FLAG = 'Y';
        
END GET_ALL_SOURCE_PARTNER_BIN_MAP;


PROCEDURE GET_COUNTRY_LIST_OE (
 OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns the list of Countries in the country_master list for use by Order Entry
   and any applications that need to display the same list. This is a filtered
   list that does not list all the countries in the country_master table.

Input:
        N/A

Output:
        cursor containing the country list
------------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
    SELECT
      cm.COUNTRY_ID,
      cm.NAME,
      cm.OE_COUNTRY_TYPE,
      cm.OE_DISPLAY_ORDER
    FROM FTD_APPS.COUNTRY_MASTER cm
    WHERE cm.STATUS = 'Active' AND
    UPPER(cm.NAME) NOT IN(SELECT UPPER(sm.STATE_NAME) FROM FTD_APPS.STATE_MASTER sm)
    ORDER BY cm.OE_DISPLAY_ORDER, cm.NAME;

END GET_COUNTRY_LIST_OE;	


PROCEDURE GET_ORDER_LANGUAGES (
 OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns the list of Languages from the language_master table for use by Order Entry
   and any applications that need to display the same list. This lists all languages
   in the language_master table

Input:
        N/A

Output:
        cursor containing the language list
------------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CUR FOR
	SELECT 
	lm.LANGUAGE_ID, 
	lm.DESCRIPTION
	FROM FTD_APPS.LANGUAGE_MASTER lm
	ORDER BY lm.DISPLAY_ORDER;

END GET_ORDER_LANGUAGES;	


PROCEDURE GET_STATE_LIST_OE (
 OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns the list of States in the state_master list for use by Order Entry
   and any applications that need to display the same list. This is a filtered
   list that also ensures correct relationship between a state_master record
   and its country_master record.

Input:
        N/A

Output:
        cursor containing the state list
------------------------------------------------------------------------------*/
BEGIN 

    OPEN OUT_CUR FOR
      SELECT
        sm.STATE_MASTER_ID AS STATE_MASTER_ID,
        sm.STATE_NAME AS STATE_NAME,
        cm.COUNTRY_ID AS COUNTRY_ID
      FROM FTD_APPS.STATE_MASTER sm
      LEFT OUTER JOIN FTD_APPS.COUNTRY_MASTER cm ON sm.COUNTRY_CODE = cm.THREE_CHARACTER_ID
      WHERE sm.COUNTRY_CODE != 'INT'

    UNION
      SELECT
        sm.STATE_MASTER_ID AS STATE_MASTER_ID,
        sm.STATE_NAME AS STATE_NAME,
        'US' AS COUNTRY_ID
      FROM FTD_APPS.STATE_MASTER sm
      LEFT OUTER JOIN FTD_APPS.COUNTRY_MASTER cm ON sm.COUNTRY_CODE = cm.THREE_CHARACTER_ID
      WHERE
        sm.COUNTRY_CODE IS NULL
    ORDER BY COUNTRY_ID, STATE_NAME;

END GET_STATE_LIST_OE;

FUNCTION GET_DB_NAME RETURN VARCHAR2

/*------------------------------------------------------------------------------
Description:
   Return the database name.
------------------------------------------------------------------------------*/

AS

BEGIN

RETURN 'ORACLE';

END GET_DB_NAME;



/*------------------------------------------------------------------------------
Description:
   This stored proc will take the company and state id as the input parm, and will 
   return a cursor that will have all the tax info for that company for that state.

Input:
        IN_STATE_MASTER_ID
        IN_COMPANY_ID

Output:
        cursor containing the state list
------------------------------------------------------------------------------*/
PROCEDURE GET_STATE_COMPANY_TAX_BY_ID 
(
  IN_STATE_MASTER_ID   IN  FTD_APPS.STATE_COMPANY_TAX.STATE_MASTER_ID%TYPE,
  IN_COMPANY_ID        IN  FTD_APPS.STATE_COMPANY_TAX.COMPANY_ID%TYPE,
  OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      SELECT 
                sct.STATE_MASTER_ID
              , sct.COMPANY_ID
              , sct.TAX1_NAME
              , sct.TAX1_RATE
              , decode(sct.TAX1_NAME, null, null, (select tdov.DISPLAY_ORDER  from FTD_APPS.TAX_DISPLAY_ORDER_VAL tdov where tdov.TAX_NAME = sct.TAX1_NAME)) TAX1_DISPLAY_ORDER
              , sct.TAX2_NAME
              , sct.TAX2_RATE
              , decode(sct.TAX2_NAME, null, null, (select tdov.DISPLAY_ORDER  from FTD_APPS.TAX_DISPLAY_ORDER_VAL tdov where tdov.TAX_NAME = sct.TAX2_NAME)) TAX2_DISPLAY_ORDER
              , sct.TAX3_NAME
              , sct.TAX3_RATE
              , decode(sct.TAX3_NAME, null, null, (select tdov.DISPLAY_ORDER  from FTD_APPS.TAX_DISPLAY_ORDER_VAL tdov where tdov.TAX_NAME = sct.TAX3_NAME)) TAX3_DISPLAY_ORDER
              , sct.TAX4_NAME
              , sct.TAX4_RATE
              , decode(sct.TAX4_NAME, null, null, (select tdov.DISPLAY_ORDER  from FTD_APPS.TAX_DISPLAY_ORDER_VAL tdov where tdov.TAX_NAME = sct.TAX4_NAME)) TAX4_DISPLAY_ORDER
              , sct.TAX5_NAME
              , sct.TAX5_RATE
              , decode(sct.TAX5_NAME, null, null, (select tdov.DISPLAY_ORDER  from FTD_APPS.TAX_DISPLAY_ORDER_VAL tdov where tdov.TAX_NAME = sct.TAX5_NAME)) TAX5_DISPLAY_ORDER
      FROM    FTD_APPS.STATE_COMPANY_TAX sct
      WHERE   sct.COMPANY_ID = IN_COMPANY_ID
      AND     sct.STATE_MASTER_ID = IN_STATE_MASTER_ID
      ;


END GET_STATE_COMPANY_TAX_BY_ID;

FUNCTION GET_NEXT_SVS_TRANSACTION_SEQ RETURN NUMBER

/*------------------------------------------------------------------------------
Description: This function returns the next svs transaction sequence id.
Input: none
Output: number
------------------------------------------------------------------------------*/
AS
V_SVS_TRAN_SEQ NUMBER;

BEGIN

  SELECT  CLEAN.SVS_TRANS_ID_SEQ.NEXTVAL
  INTO    V_SVS_TRAN_SEQ
  FROM    DUAL;

RETURN V_SVS_TRAN_SEQ;

END GET_NEXT_SVS_TRANSACTION_SEQ;

FUNCTION GET_NEXT_UA_TRANSACTION_SEQ RETURN NUMBER

/*------------------------------------------------------------------------------
Description: This function returns the next united transaction sequence id.
Input: none
Output: number
------------------------------------------------------------------------------*/
AS
V_UNITED_TRAN_SEQ NUMBER;

BEGIN

  SELECT  CLEAN.UNITED_TRANS_ID_SEQ.NEXTVAL
  INTO    V_UNITED_TRAN_SEQ
  FROM    DUAL;

RETURN V_UNITED_TRAN_SEQ;

END GET_NEXT_UA_TRANSACTION_SEQ;

/*-- 11945 Morning Delivery Maintenance changes star here --*/

PROCEDURE GET_MORNING_DELIVERY_FEE_LIST (
      OUT_CUR             OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   returns morning delivery fee information
------------------------------------------------------------------------------*/
BEGIN
  GET_MORNING_DELIVERY_FEE_LIST (null, out_cur);
  
END GET_MORNING_DELIVERY_FEE_LIST;

PROCEDURE GET_MORNING_DELIVERY_FEE_LIST (
      IN_SORT_FIELD        IN VARCHAR2,
      OUT_CUR              OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns sorted morning delivery fee information
------------------------------------------------------------------------------*/
BEGIN
  GET_DELIVERY_FEE_LIST (in_sort_field,'MORNING_DELIVERY_FEE', out_cur);
  
END GET_MORNING_DELIVERY_FEE_LIST;

PROCEDURE GET_DELIVERY_FEE_LIST (
 IN_SORT_FIELD            IN  VARCHAR2,
 IN_FEE_FIELD             IN  VARCHAR2,
 OUT_CURSOR               OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns sorted delivery fee info for a specific delivery type
------------------------------------------------------------------------------*/
  v_sort_field	varchar2(100);
  v_delivery_fee_sql VARCHAR2(4000):='';

BEGIN

   IF in_sort_field IS NULL THEN
      v_sort_field := 'to_number(delivery_fee_id)';
   ELSE
      v_sort_field := in_sort_field;
   END IF;

  v_delivery_fee_sql :='SELECT DELIVERY_FEE_ID, 
                               upper(DESCRIPTION) as DESCRIPTION_SORTED, 
                               DESCRIPTION, 
                               REQUESTED_BY, 
                               TO_CHAR('|| IN_FEE_FIELD ||', ''999.99'') AS DELIVERY_FEE, 
                               (SELECT ''Y'' FROM FTD_APPS.SOURCE_MASTER SM WHERE SM.DELIVERY_FEE_ID = GD.DELIVERY_FEE_ID AND ROWNUM = 1) SOURCE_CODE_EXISTS
                               FROM FTD_APPS.DELIVERY_FEE GD ORDER BY ' || v_sort_field;
  OPEN OUT_CURSOR FOR v_delivery_fee_sql;

END GET_DELIVERY_FEE_LIST;

PROCEDURE GET_MORNING_DELIVERY_FEE_DATA (
    IN_ORDER_DATE 		IN DATE,
    IN_SOURCE_CODE 		IN FTD_APPS.SOURCE_MASTER_HIST.SOURCE_CODE%TYPE,
    OUT_CUR 			OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns delivery fee information for a given source code and ordered date
   from history tables   
------------------------------------------------------------------------------*/
v_delivery_fee_id   FTD_APPS.SOURCE_MASTER_HIST.DELIVERY_FEE_ID%TYPE := '0';

BEGIN

  SELECT DISTINCT(SMH.DELIVERY_FEE_ID) INTO v_delivery_fee_id FROM FTD_APPS.SOURCE_MASTER_HIST  SMH
	WHERE 
		SMH.UPDATED_ON= (SELECT MAX(SMH2.UPDATED_ON) FROM FTD_APPS.SOURCE_MASTER_HIST SMH2 
		WHERE SMH2.SOURCE_CODE = IN_SOURCE_CODE AND SMH2.UPDATED_ON < IN_ORDER_DATE  
		AND (SMH2.EXPIRED_ON IS NULL OR SMH2.EXPIRED_ON > IN_ORDER_DATE))
	AND SMH.SOURCE_CODE = IN_SOURCE_CODE;
	
  --EXCEPTION WHEN NO_DATA_FOUND THEN v_delivery_fee_id:= null;
     	
  OPEN OUT_CUR FOR
    --Retrieve the delivery fee information
    SELECT DISTINCT DFH.DELIVERY_FEE_ID, DFH.MORNING_DELIVERY_FEE FROM FTD_APPS.DELIVERY_FEE_HIST DFH 
		WHERE	DFH.DELIVERY_FEE_ID = v_delivery_fee_id
        AND DFH.UPDATED_ON =
            (SELECT MAX(DFH2.UPDATED_ON) FROM FTD_APPS.DELIVERY_FEE_HIST DFH2 
				WHERE DFH2.UPDATED_ON < IN_ORDER_DATE AND DFH2.DELIVERY_FEE_ID =  v_delivery_fee_id) ;
				
	EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
	
END GET_MORNING_DELIVERY_FEE_DATA;

FUNCTION IS_SOURCE_CODE_VALID (
   IN_COMPANY_ID IN FTD_APPS.SOURCE.COMPANY_ID%TYPE,
   IN_SOURCE_CODE IN FTD_APPS.SOURCE.SOURCE_CODE%TYPE )
   RETURN VARCHAR2
AS
/*------------------------------------------------------------------------------
Description:
   Return Y or N if the given source code expired.
------------------------------------------------------------------------------*/

V_EXISTS_COUNT  NUMBER;
V_EXPIRED_COUNT  NUMBER; 
VALIDATE_FLAG VARCHAR2(20) := 'TRUE';



BEGIN

      SELECT COUNT(1)
      INTO V_EXISTS_COUNT 
      FROM  FTD_APPS.SOURCE
      WHERE COMPANY_ID = IN_COMPANY_ID and SOURCE_CODE = IN_SOURCE_CODE;
            
      IF v_exists_count > 0 THEN
        SELECT COUNT (1)
        INTO   V_EXPIRED_COUNT
        FROM FTD_APPS.SOURCE_MASTER SM
        WHERE SM.SOURCE_CODE = IN_SOURCE_CODE 
        AND SM.END_DATE < sysdate;
        
        IF V_EXPIRED_COUNT > 0 THEN
          VALIDATE_FLAG := 'EXPIRED';
        END IF;
     ELSE
        VALIDATE_FLAG := 'DOES_NOT_EXIST';
    END IF;

RETURN VALIDATE_FLAG;

END IS_SOURCE_CODE_VALID;

FUNCTION GENERATE_NEXT_SYMPATHY_SC_SEQ (
   IN_PARTNER_NAME 		IN 			VARCHAR2
) 
RETURN NUMBER

/*------------------------------------------------------------------------------
Description: This function returns the next sympathy sc sequence id.
Input: none
Output: number
--------------------------------------------------------------------------------*/

AS

V_SYMPATHY_SC_SEQ NUMBER;
V_QUERY VARCHAR2(400);

BEGIN
	V_QUERY := 'SELECT FTD_APPS.SYMPATHY_SC_SEQ_'||UPPER(IN_PARTNER_NAME)||'.NEXTVAL FROM DUAL';
  	EXECUTE IMMEDIATE V_QUERY into V_SYMPATHY_SC_SEQ;
  	
RETURN V_SYMPATHY_SC_SEQ;

END GENERATE_NEXT_SYMPATHY_SC_SEQ;

PROCEDURE GET_STATE_BY_NAME
(
  IN_STATE_NAME                 IN FTD_APPS.STATE_MASTER.STATE_NAME%TYPE,
  OUT_CUR                       OUT TYPES.REF_CURSOR
) AS

BEGIN

    OPEN OUT_CUR FOR
        select state_master_id,
            state_name,
            country_code
        from ftd_apps.state_master
        where upper(state_name) = upper(in_state_name);

END GET_STATE_BY_NAME;

FUNCTION INSERT_PAYMENT_EXT_SCRIPT(
   IN_PAYMENT_ID NUMBER,
   IN_DELIM_STR IN LONG,
   IN_KV_DELIM IN VARCHAR2,
   IN_KV_PAIR_DELIM IN VARCHAR2,
   IN_CREATED_BY IN VARCHAR2,
   IN_CARDINAL_DATA IN VARCHAR2 DEFAULT NULL
)
RETURN LONG
AS
/*----------------------------------------------
IN_DELIM_STR			delimited string containing additional payment information Ex: abc###123&&&def###456
IN_KV_DELIM				delimiter between key and value Ex: ###
IN_KV_PAIR_DELIM		delimiter between each key-value Entry Ex: &&&
Note ::  IN_KV_DELIM and IN_KV_PAIR_DELIM should have same length. Above example has length 3
------------------------------------------------*/
current_index number := 1;
pair_index number;
kv_index number;
kv_pair varchar2(2100);
key varchar2(100);
value varchar2(2000);
kv_delim_length number;
temp_delim_str long;
v_query long := '';
insert_prepend varchar2(200);
insert_append varchar2(200);

BEGIN

kv_delim_length := length(IN_KV_DELIM);

IF IN_DELIM_STR IS NULL OR IN_KV_DELIM IS NULL OR IN_KV_PAIR_DELIM IS NULL OR kv_delim_length IS NULL THEN
	v_query := 'select 1 from dual';
ELSE
	insert_prepend := ' INTO PAYMENTS_EXT(PAYMENT_ID,AUTH_PROPERTY_NAME,AUTH_PROPERTY_VALUE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY) VALUES';
	insert_append := ''',sysdate,''' || IN_CREATED_BY || ''',sysdate,'''|| IN_CREATED_BY || ''')';
	temp_delim_str := IN_DELIM_STR || IN_KV_PAIR_DELIM;
	LOOP
		pair_index := INSTR(temp_delim_str,IN_KV_PAIR_DELIM,current_index);
		EXIT WHEN (NVL(pair_index,0) = 0);
		kv_pair := SUBSTR(temp_delim_str,current_index,pair_index-current_index);
		current_index := pair_index+kv_delim_length;
		kv_index := INSTR(kv_pair,IN_KV_DELIM);
		key := SUBSTR(kv_pair,1,kv_index-1);
		value := SUBSTR(kv_pair,kv_index+kv_delim_length);
	    IF IN_CARDINAL_DATA IS NOT NULL THEN
	      IF INSTR(',' || IN_CARDINAL_DATA || ',',',' || key || ',') > 0 THEN
	      --found
	      v_query := v_query || insert_prepend || '(' || IN_PAYMENT_ID ||',''' || key || ''',''' || value || insert_append;
	      END IF;
	    ELSE
	    v_query := v_query || insert_prepend || '(' || IN_PAYMENT_ID ||',''' || key || ''',''' || value || insert_append;
	    END IF;
	END LOOP;
	v_query := 'INSERT ALL' || v_query || ' SELECT 1 FROM DUAL';
END IF;

RETURN v_query;

END INSERT_PAYMENT_EXT_SCRIPT;

PROCEDURE GET_ACTIVE_OCCASIONS (
   OUT_CUR OUT TYPES.REF_CURSOR) IS
BEGIN

  OUT_CUR := FTD_APPS.SP_GET_ACTIVE_OCCASIONS ();
  
END GET_ACTIVE_OCCASIONS;

END GLOBAL_PKG;
.
/