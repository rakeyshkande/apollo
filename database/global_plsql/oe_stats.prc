CREATE OR REPLACE
PROCEDURE global.oe_stats AS

   CURSOR center_counts IS
      SELECT COUNT(*) cnt, cc.description call_center, s.unit_id
      FROM   aas.sessions s, aas.identity id, aas.users u, ftd_apps.call_center cc
      WHERE  s.identity_id = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.call_center_id = cc.call_center_id
      AND    s.last_accessed > (SYSDATE - 1/48)
      AND    s.unit_id in ('WEB OE','ORDER SCRUB')
      GROUP BY cc.description, s.unit_id
      ORDER BY s.unit_id;

   CURSOR center_counts_five IS
      SELECT COUNT(*) cnt, cc.description call_center
      FROM   aas.sessions s, aas.identity id, aas.users u, ftd_apps.call_center cc
      WHERE  s.identity_id = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.call_center_id = cc.call_center_id
      AND    s.last_accessed > (SYSDATE - 1/288)
      GROUP BY cc.description;

   CURSOR order_counts IS
      SELECT cc.description call_center, COUNT(*) Orders
      FROM   ftd_apps.session_order_master o, aas.identity id, aas.users u, ftd_apps.call_center cc
      WHERE  o.transaction_date > SYSDATE - 1
      AND    o.order_status = 'DONE'
      AND    o.csr_user_name = id.identity_id
      AND    id.user_id = u.user_id
      AND    cc.CALL_CENTER_ID = u.CALL_CENTER_ID
      GROUP BY cc.DESCRIPTION;

   CURSOR order_counts_hour IS
      SELECT cc.description call_center, COUNT(*) Orders
      FROM   ftd_apps.session_order_master o, aas.identity id, aas.users u, ftd_apps.call_center cc
      WHERE  o.transaction_date > SYSDATE - 1/24
      AND    o.order_status = 'DONE'
      AND    o.csr_user_name = id.identity_id
      AND    id.user_id = u.user_id
      AND    cc.call_center_id = u.call_center_id
      GROUP BY cc.description;

   v_row_count NUMBER := 0;
   v_call_center varchar2(20);
   v_unit_id     VARCHAR2(50);

BEGIN

   dbms_output.put_line('-');

   SELECT COUNT(*)
   INTO   v_row_count
   FROM   aas.sessions s
   WHERE  s.last_accessed > (SYSDATE - 1/48);
   dbms_output.put_line ('Current users (30 minutes) = ' || v_row_count);

   FOR v_row IN center_counts LOOP
      v_call_center := v_row.call_center;
      v_unit_id := v_row.unit_id;
      v_row_count := v_row.cnt;
      dbms_output.put_line (SUBSTR(v_call_center || '             ',1,13) || ' - ' || substr(v_unit_id || '             ',1,13)
      || ' - ' || v_row_count);
   END LOOP;
   dbms_output.put_line('-');

   SELECT COUNT(*)
   INTO   v_row_count
   FROM   aas.sessions s
   WHERE  s.last_accessed > (SYSDATE - 1/288);
   dbms_output.put_line ('Current users (5 minutes) = ' || v_row_count);

   FOR v_row IN center_counts_five LOOP
      v_call_center := v_row.call_center;
      v_row_count := v_row.cnt;
      dbms_output.put_line (SUBSTR(v_call_center || '             ',1,13) || ' - ' || v_row_count);
   END LOOP;
   dbms_output.put_line('-');

   SELECT COUNT(*)
   INTO   v_row_count
   FROM   ftd_apps.session_order_master
   WHERE  transaction_date > (SYSDATE - 1)
   AND    order_status = 'DONE';
   dbms_output.put_line ('Orders in last 24 hours = ' || v_row_count);

   FOR v_row IN order_counts LOOP
      v_call_center := v_row.call_center;
      v_row_count := v_row.orders;
      dbms_output.put_line (substr(v_call_center || '             ',1,13) || ' - ' || v_row_count);
   END LOOP;
   dbms_output.put_line('-');

   SELECT COUNT(*)
   INTO   v_row_count
   FROM   ftd_apps.session_order_master
   WHERE  transaction_date > (SYSDATE - 1/24)
   AND    order_status = 'DONE';
   dbms_output.put_line ('Orders in last hour = ' || v_row_count);

   FOR v_row IN order_counts_hour LOOP
      v_call_center := v_row.call_center;
      v_row_count := v_row.orders;
      dbms_output.put_line (substr(v_call_center || '             ',1,13) || ' - ' || v_row_count);
   END LOOP;

END;
.
/
