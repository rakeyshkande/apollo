CREATE OR REPLACE
PROCEDURE global.oe_stats_2 AS

   CURSOR call_center_c IS
      SELECT call_center_id, description
      FROM   ftd_apps.call_center;
   call_center_r call_center_c%ROWTYPE;

   v_center_count_30 NUMBER;
   v_center_count_5  NUMBER;
   v_order_count_24  NUMBER;
   v_order_count_1   NUMBER;
   v_sysdate         DATE;

BEGIN

   v_sysdate := SYSDATE;

   FOR call_center_r IN call_center_c LOOP

      SELECT COUNT(*)
      INTO   v_center_count_30
      FROM   aas.sessions s, aas.identity id, aas.users u
      WHERE  s.identity_id = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.call_center_id = call_center_r.call_center_id
      AND    s.last_accessed > (SYSDATE - 1/48)
      AND    s.unit_id = 'WEB OE';

      SELECT COUNT(*)
      INTO   v_center_count_5
      FROM   aas.sessions s, aas.identity id, aas.users u
      WHERE  s.identity_id = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.call_center_id = call_center_r.call_center_id
      AND    s.last_accessed > (SYSDATE - 1/288)
      AND    s.unit_id = 'WEB OE';

      SELECT COUNT(*)
      INTO   v_order_count_24
      FROM   ftd_apps.session_order_master o, aas.identity id, aas.users u
      WHERE  o.transaction_date > SYSDATE - 1
      AND    o.order_status = 'DONE'
      AND    o.csr_user_name = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.CALL_CENTER_ID = call_center_r.call_center_id;

      SELECT COUNT(*)
      INTO   v_order_count_1
      FROM   ftd_apps.session_order_master o, aas.identity id, aas.users u
      WHERE  o.transaction_date > SYSDATE - (1/24)
      AND    o.order_status = 'DONE'
      AND    o.csr_user_name = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.CALL_CENTER_ID = call_center_r.call_center_id;

      INSERT INTO rpt.woe_hourly_stats (timestamp, call_center, unit_id, center_count_30, center_count_5, order_count_24,
      order_count_1) VALUES (v_sysdate, call_center_r.description, 'WEB OE', v_center_count_30, v_center_count_5, v_order_count_24,
      v_order_count_1);

      SELECT COUNT(*)
      INTO   v_center_count_30
      FROM   aas.sessions s, aas.identity id, aas.users u
      WHERE  s.identity_id = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.call_center_id = call_center_r.call_center_id
      AND    s.last_accessed > (SYSDATE - 1/48)
      AND    s.unit_id = 'ORDER SCRUB';

      SELECT COUNT(*)
      INTO   v_center_count_5
      FROM   aas.sessions s, aas.identity id, aas.users u
      WHERE  s.identity_id = id.identity_id
      AND    id.user_id = u.user_id
      AND    u.call_center_id = call_center_r.call_center_id
      AND    s.last_accessed > (SYSDATE - 1/288)
      AND    s.unit_id = 'ORDER SCRUB';

      INSERT INTO rpt.woe_hourly_stats (timestamp, call_center, unit_id, center_count_30, center_count_5, order_count_24,
      order_count_1) VALUES (v_sysdate, call_center_r.description, 'ORDER SCRUB', v_center_count_30, v_center_count_5, 0, 0);

   END LOOP;
   COMMIT;

END;
.
/
