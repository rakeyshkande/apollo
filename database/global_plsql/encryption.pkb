create or replace PACKAGE BODY        GLOBAL.ENCRYPTION AS

V_BLOCK_MODE   VARCHAR2 (20) := 'AES/CBC/PKCS5Padding';

FUNCTION ENCRYPT_IT (IN_STRING IN VARCHAR2, IN_KEY_NAME VARCHAR2)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for encrypting a string

Input:
        plain text string to be encrypted, key string

Output:
        encrypted string

-----------------------------------------------------------------------------*/

V_IN_STRING             CHAR (104);
V_ENCRYPTED_STRING      VARCHAR2 (4000);

BEGIN

   v_encrypted_string := ingrian.ing_e_vrc_cl430@fling(rtrim(in_string), in_key_name, v_block_mode, system.iv(in_key_name));

RETURN V_ENCRYPTED_STRING;

END ENCRYPT_IT;



FUNCTION DECRYPT_IT (IN_ENCRYPT_STRING IN VARCHAR2, IN_KEY_NAME VARCHAR2)
RETURN VARCHAR2 DETERMINISTIC

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for decrypting a string

Input:
        encrypted string, key string

Output:
        decrypted string

-----------------------------------------------------------------------------*/

V_DECRYPTED_STRING      VARCHAR2 (4000);

BEGIN

IF IN_ENCRYPT_STRING IS NULL THEN
   v_decrypted_string := null;
ELSIF IN_KEY_NAME IS NULL THEN
   v_decrypted_string := IN_ENCRYPT_STRING;
ELSE
   v_decrypted_string := ingrian.ing_d_vrc_cl430@fling(in_encrypt_string, in_key_name, v_block_mode, system.iv(in_key_name));
END IF;

RETURN RTRIM (V_DECRYPTED_STRING);

END DECRYPT_IT;


END ENCRYPTION;
/
