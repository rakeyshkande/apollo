CREATE OR REPLACE
PACKAGE BODY global.CACHE_PKG AS


PROCEDURE GET_BILLING_INFO (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all billing information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  SOURCE_CODE_ID,
                BILLING_INFO_SEQUENCE,
                INFO_DESCRIPTION,
                DECODE (INFO_FORMAT, NULL, '/.*/', FTD_APPS.OE_MAKE_REGEXP (INFO_FORMAT)) AS INFO_FORMAT,
                PROMPT_TYPE
        FROM    FTD_APPS.BILLING_INFO;

END GET_BILLING_INFO;


PROCEDURE GET_COLOR_MASTER (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all product colors information.
------------------------------------------------------------------------------*/
BEGIN

        OUT_CUR := FTD_APPS.SP_GET_COLORS_LIST ();

END GET_COLOR_MASTER;


PROCEDURE GET_COMPANY_MASTER (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all company master information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  COMPANY_ID,
                COMPANY_NAME,
                INTERNET_ORIGIN
        FROM    FTD_APPS.COMPANY_MASTER
        WHERE   (email_direct_mail_flag = 'Y' or order_flag = 'Y' or report_flag = 'Y');

END GET_COMPANY_MASTER;


PROCEDURE GET_COST_CENTERS (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all cost center information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  COST_CENTER_ID,
                PARTNER_ID,
                SOURCE_CODE_ID
        FROM    FTD_APPS.COST_CENTERS;

END GET_COST_CENTERS;


PROCEDURE GET_COUNTRY_MASTER (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all country master information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  COUNTRY_ID,
                NAME,
                OE_COUNTRY_TYPE,
                OE_DISPLAY_ORDER,
                ADD_ON_DAYS,
                CUTOFF_TIME,
                MONDAY_CLOSED,
                TUESDAY_CLOSED,
                WEDNESDAY_CLOSED,
                THURSDAY_CLOSED,
                FRIDAY_CLOSED,
                SATURDAY_CLOSED,
                SUNDAY_CLOSED,
                THREE_CHARACTER_ID,
    STATUS,
                MONDAY_TRANSIT_FLAG,
                TUESDAY_TRANSIT_FLAG,
                WEDNESDAY_TRANSIT_FLAG,
                THURSDAY_TRANSIT_FLAG,
                FRIDAY_TRANSIT_FLAG,
                SATURDAY_TRANSIT_FLAG,
                SUNDAY_TRANSIT_FLAG
        FROM    FTD_APPS.COUNTRY_MASTER
        ORDER BY OE_DISPLAY_ORDER, NAME;

END GET_COUNTRY_MASTER;

PROCEDURE GET_FRP_GLOBAL_PARMS (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all rows in the frp global parms table.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  CONTEXT,
                NAME,
                VALUE
        FROM    FRP.GLOBAL_PARMS;

END GET_FRP_GLOBAL_PARMS;


PROCEDURE GET_FTD_APPS_GLOBAL_PARMS (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all rows in the ftd_apps global parms table.
------------------------------------------------------------------------------*/
BEGIN

        OUT_CUR := FTD_APPS.OE_GET_GLOBAL_PARMS;

END GET_FTD_APPS_GLOBAL_PARMS;


PROCEDURE GET_NOVATOR_FRAUD_CODES (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the novator fraud codes.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  CODE,
                DESCRIPTION
        FROM    FTD_APPS.NOVATOR_FRAUD_CODES;

END GET_NOVATOR_FRAUD_CODES;


PROCEDURE GET_SHIPPING_KEY_COSTS (
        OUT_CUR         OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all shipping key cost information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  SHIPPING_KEY_DETAIL_ID,
                SHIPPING_METHOD_ID,
                SHIPPING_COST
        FROM    FTD_APPS.SHIPPING_KEY_COSTS;

END GET_SHIPPING_KEY_COSTS;


PROCEDURE GET_SHIPPING_KEY_DETAILS (
        OUT_CUR         OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all shipping key details information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  SHIPPING_DETAIL_ID,
                SHIPPING_KEY_ID,
                MIN_PRICE,
                MAX_PRICE
        FROM    FTD_APPS.SHIPPING_KEY_DETAILS;

END GET_SHIPPING_KEY_DETAILS;


PROCEDURE GET_SNH (
        OUT_CUR         OUT TYPES.REF_CURSOR)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all shipping and handling information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  SNH_ID,
                DESCRIPTION,
                FIRST_ORDER_DOMESTIC,
                SECOND_ORDER_DOMESTIC,
                THIRD_ORDER_DOMESTIC,
                FIRST_ORDER_INTERNATIONAL,
                SECOND_ORDER_INTERNATIONAL,
                THIRD_ORDER_INTERNATIONAL
        FROM    FTD_APPS.SNH;

END GET_SNH;



PROCEDURE GET_SOURCE (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Retrieves all source code information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  S.SOURCE_CODE,
                S.START_DATE,
                S.END_DATE,
                S.PRICING_CODE,
                S.SHIPPING_CODE,
                S.PARTNER_ID,
                S.VALID_PAY_METHOD,
                S.JCPENNEY_FLAG,
                S.SEND_TO_SCRUB,
                CM.COMPANY_ID,
                CM.COMPANY_NAME,
                CM.INTERNET_ORIGIN,
                S.FRAUD_FLAG,
                S.BILLING_INFO_LOGIC,
                S.emergency_text_flag,
                S.requires_delivery_confirmation,
                S.order_source,
                S.webloyalty_flag,
                (SELECT SFP.FLORIST_ID FROM FTD_APPS.SOURCE_FLORIST_PRIORITY SFP WHERE SFP.SOURCE_CODE = S.SOURCE_CODE AND SFP.PRIORITY = 1) as primary_florist, 
                GLOBAL.CACHE_PKG.GET_SOURCE_FLORIST_BACKUP(S.SOURCE_CODE) as backup_florists, 
                S.RELATED_SOURCE_CODE,
                GLOBAL.CACHE_PKG.GET_SOURCE_INDEX_FOUND(S.SOURCE_CODE, 'limit_products') as source_code_has_limit_index,
                S.ALLOW_FREE_SHIPPING_FLAG,
                S.PARTNER_NAME
        FROM    FTD_APPS.SOURCE S
        JOIN    FTD_APPS.COMPANY_MASTER CM
        ON      S.COMPANY_ID = CM.COMPANY_ID;

END GET_SOURCE;



PROCEDURE GET_STATE_MASTER (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return all rows in the state master table.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  STATE_MASTER_ID,
                STATE_NAME,
                COUNTRY_CODE,
                TIME_ZONE,
                DELIVERY_EXCLUSION,
    DROP_SHIP_AVAILABLE_FLAG
        FROM    FTD_APPS.STATE_MASTER;

END GET_STATE_MASTER;


PROCEDURE GET_ZIP_CODE (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the all zip codes.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  ZIP_CODE_ID,
                CITY,
                STATE_ID
        FROM    FTD_APPS.ZIP_CODE;

END GET_ZIP_CODE;


PROCEDURE GET_EFOS_STATE_CODES (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
   Return the all efos state codes.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  STATE_CODE,
                STATE_ID
        FROM    FTD_APPS.EFOS_STATE_CODES;

END GET_EFOS_STATE_CODES;


PROCEDURE GET_COMPANY_MASTER_MAIL (
        OUT_CUR         OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return all company master information.
------------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  COMPANY_ID,
                COMPANY_NAME,
                INTERNET_ORIGIN
        FROM    FTD_APPS.COMPANY_MASTER
        WHERE   email_direct_mail_flag = 'Y';

END GET_COMPANY_MASTER_MAIL;
PROCEDURE GET_SHIP_METHODS
(
   OUT_CUR                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        Retrieves the ship methods and their max_transit_days

Input:

Output:
        out_cur      Cursor containin the row

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
select sm.ship_method_id,
       sm.description,
       sm.novator_tag,
       sm.max_transit_days,
       sm.sds_ship_via,
       sm.sds_ship_via_air
from ftd_apps.SHIP_METHODS sm;

END GET_SHIP_METHODS;

PROCEDURE GET_BIN_PROCESSING_PARTNERS
(
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        SELECT PARTNER_NAME
        FROM FTD_APPS.PARTNER_MASTER
        WHERE BIN_PROCESSING_FLAG = 'Y'
        ORDER BY PARTNER_NAME;

END GET_BIN_PROCESSING_PARTNERS;

PROCEDURE GET_BIN_MASTER_BY_PARTNER
(
IN_PARTNER_NAME          IN FTD_APPS.PARTNER_MASTER.PARTNER_NAME%TYPE,
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        SELECT CC_BIN_NUMBER
        FROM FTD_APPS.PARTNER_BIN_MASTER
        WHERE PARTNER_NAME = IN_PARTNER_NAME
        AND BIN_ACTIVE_FLAG = 'Y';

END GET_BIN_MASTER_BY_PARTNER;

PROCEDURE GET_SOURCE_PARTNER_BIN_MAP
(
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CURSOR FOR
        SELECT SPBM.SOURCE_CODE,
            SPBM.PARTNER_NAME
        FROM FTD_APPS.SOURCE_PARTNER_BIN_MAPPING SPBM, FTD_APPS.SOURCE_MASTER SM
        WHERE SPBM.BIN_PROCESSING_ACTIVE_FLAG = 'Y'
        AND SM.SOURCE_CODE = SPBM.SOURCE_CODE
        AND SM.START_DATE <= TRUNC(SYSDATE)
        AND (SM.END_DATE IS NULL OR SM.END_DATE >= TRUNC(SYSDATE))
        ORDER BY SPBM.SOURCE_CODE, SPBM.PARTNER_NAME;

END GET_SOURCE_PARTNER_BIN_MAP;


FUNCTION GET_SOURCE_FLORIST_BACKUP
(
 inSourceCode in FTD_APPS.SOURCE_FLORIST_PRIORITY.SOURCE_CODE%TYPE
)
/*------------------------------------------------------------------------------
Description:
        Retrieves all backup florists for a given source code
------------------------------------------------------------------------------*/
RETURN VARCHAR2
AS

backupFloristVal varchar2(4000);

cursor cursor_c is
    SELECT FLORIST_ID
      FROM FTD_APPS.SOURCE_FLORIST_PRIORITY
     WHERE SOURCE_CODE = inSourceCode
       AND PRIORITY > 1;
       
row_r cursor_c%ROWTYPE;

begin

  for row_r in cursor_c
    loop
      backupFloristVal := CONCAT(backupFloristVal, row_r.FLORIST_ID);
      backupFloristVal := CONCAT(backupFloristVal, ' ');
    end loop;

  return backupFloristVal;
 
END GET_SOURCE_FLORIST_BACKUP;


/*------------------------------------------------------------------------------
Description:
        This procedure is responsible for returning ALL company data.

Note: 
   ftd_apps.sp_get_company_master_list could have worked except that 
   this function has the following where clause:
       (email_direct_mail_flag = 'Y' or order_flag = 'Y' or report_flag = 'Y')
        
Input:
  n/a
Output:
  cursor
------------------------------------------------------------------------------*/
PROCEDURE GET_COMPANY_MASTER_ALL
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
            SELECT company_id,
                   company_name,
                   default_domain
              FROM ftd_apps.company_master;

END GET_COMPANY_MASTER_ALL;


/*------------------------------------------------------------------------------
Description:
        This procedure is responsible for returning ALL content data.
Input:
  n/a
Output:
  cursor
------------------------------------------------------------------------------*/
PROCEDURE GET_CONTENT_WITH_FILTER
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
            SELECT cm.content_context,
                   cm.content_name,
                   cd.filter_1_value,
                   cd.filter_2_value,
                   cd.content_txt
              FROM ftd_apps.content_detail cd, ftd_apps.content_master cm
             WHERE cd.content_master_id = cm.content_master_id;        

END GET_CONTENT_WITH_FILTER;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product attribute restrictions.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_PRODUCT_ATTR_RESTR
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      SELECT par.product_attr_restr_id,
             par.product_attr_restr_name,
             par.product_attr_restr_oper,
             par.product_attr_restr_value,
             par.product_attr_restr_desc,
             par.java_method_name
        FROM ftd_apps.product_attr_restr par;

END GET_PRODUCT_ATTR_RESTR;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product attribute restriction by source code.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_PRODUCT_ATTR_RESTR_SOURCE
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      SELECT upper(pars.source_code) as source_code,
             pars.product_attr_restr_id
        FROM ftd_apps.product_attr_restr_source_excl pars
    ORDER BY pars.source_code;
    
END GET_PRODUCT_ATTR_RESTR_SOURCE;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning product master data.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_PRODUCT_MASTER_LIST
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      SELECT upper(pm.product_id) product_id,
             upper(pm.novator_id) novator_id,
             (select pc.description from ftd_apps.product_category pc where pc.product_category_id = pm.category) as category_desc,
             pm.product_type,
             (select pt.type_description from ftd_apps.product_types pt where pt.type_id = pm.product_type) as type_desc,
             pm.product_sub_type,
             (select pst.sub_type_description from ftd_apps.product_sub_types pst where pst.sub_type_id = pm.product_sub_type and pm.product_type=pst.type_id) as sub_type_desc,
             pm.long_description,
             pm.product_name,
             pm.novator_name,
             pm.dominant_flowers,
             pm.over_21,
             pm.exception_start_date,
             pm.exception_end_date,
             pm.personalization_template_id,
             pm.shipping_key,
             pm.status,
             pm.delivery_type,
             pm.discount_allowed_flag,
             pm.standard_price,
             pm.deluxe_price,
             pm.premium_price,
             pm.gbb_popover_flag,
             pm.gbb_title_txt,
             pm.gbb_name_override_flag_1,
             pm.gbb_name_override_flag_2,
             pm.gbb_name_override_flag_3,
             pm.gbb_name_override_txt_1,
             pm.gbb_name_override_txt_2,
             pm.gbb_name_override_txt_3,
             pm.gbb_price_override_flag_1,
             pm.gbb_price_override_flag_2,
             pm.gbb_price_override_flag_3,
             pm.gbb_price_override_txt_1,
             pm.gbb_price_override_txt_2,
             pm.gbb_price_override_txt_3,   
             pm.preferred_price_point,
             pm.no_tax_flag,
             pm.ship_method_florist,
             pm.ship_method_carrier
        FROM ftd_apps.product_master pm
       WHERE novator_id is not null;
    
END GET_PRODUCT_MASTER_LIST;



/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source codes for product.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_PRODUCT_SOURCE
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      SELECT upper(ps.product_id) as product_id,
             upper(ps.source_code) as source_code
        FROM ftd_apps.product_source ps
    UNION
      SELECT upper(us.upsell_master_id) as product_id,
             upper(us.source_code) as source_code
        FROM ftd_apps.upsell_source us
    UNION
      SELECT upper(ud.upsell_detail_id) as product_id,
             upper(us.source_code) as source_code
        FROM ftd_apps.upsell_detail ud, ftd_apps.upsell_master um, ftd_apps.upsell_source us
       WHERE ud.upsell_master_id = um.upsell_master_id
         AND us.upsell_master_id = um.upsell_master_id
    ORDER BY 1;
    
END GET_PRODUCT_SOURCE;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source code data.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_SOURCE_LIST
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

CURSOR ship_fee_cursor IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'ORDER_SERVICE_CONFIG'
             and name = 'DISPLAY_SHIP_FEE';

CURSOR service_fee_cursor IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'ORDER_SERVICE_CONFIG'
             and name = 'DISPLAY_SERVICE_FEE';

v_ship_fee_flag         VARCHAR2(1);
v_service_fee_flag      VARCHAR2(1);

BEGIN

  -- Get the frp.global_parms 
    OPEN ship_fee_cursor;
   FETCH ship_fee_cursor into v_ship_fee_flag;
   CLOSE ship_fee_cursor;

    OPEN service_fee_cursor;
   FETCH service_fee_cursor into v_service_fee_flag;
   CLOSE service_fee_cursor;

    OPEN OUT_CUR FOR
            SELECT upper(sm.source_code) source_code,
                   sm.snh_id,
                   sm.price_header_id,
                   sm.company_id,
                   sm.order_source,
                   sm.related_source_code,
                   discount_allowed_flag,
                   iotw_flag,
                   pp.program_type,
                   pr.calculation_basis,
                   pr.points,
                   pr.bonus_calculation_basis,
                   pr.bonus_points,
                   pr.maximum_points,
                   pr.reward_type,
                   pmmp.payment_method_id,
                   pmmp.dollar_to_mp_operator,
                   pmmp.rounding_method_id,
                   pmmp.rounding_scale_qty,
                   mrr.mp_redemption_rate_amt,
                   CASE sm.display_service_fee_code WHEN 'D' THEN v_service_fee_flag ELSE sm.display_service_fee_code END as client_disp_svc_fee_flag,
                   CASE sm.display_shipping_fee_code WHEN 'D' THEN v_ship_fee_flag ELSE sm.display_shipping_fee_code END as client_disp_shp_fee_flag,
                   sm.start_date,
                   sm.end_date,
                   sm.price_header_id pricing_code,
                   sm.snh_id shipping_code,
                   spr.program_name partner_id,
                   sm.payment_method_id valid_pay_method,
                   sm.jcpenney_flag,
                   sm.send_to_scrub,
                   cm.company_name,
                   cm.internet_origin,
                   sm.fraud_flag,
                   sm.billing_info_logic,
                   sm.emergency_text_flag,
                   sm.requires_delivery_confirmation,
                   sm.webloyalty_flag,
                   (select sfp.florist_id from ftd_apps.source_florist_priority sfp where sfp.source_code = sm.source_code and sfp.priority = 1) as primary_florist, 
                   global.cache_pkg.get_source_florist_backup(sm.source_code) as backup_florists, 
                   global.cache_pkg.get_source_index_found(sm.source_code, 'limit_products') as source_code_has_limit_index,
                   sm.allow_free_shipping_flag,
                   pp.partner_name,
				   sm.same_day_upcharge,
                   sm.display_same_day_upcharge				   
              FROM ftd_apps.source_master sm
              JOIN ftd_apps.company_master cm ON sm.company_id = cm.company_id			  
   LEFT OUTER JOIN ftd_apps.source_program_ref spr ON sm.source_code  = spr.source_code
               AND spr.start_date  = ftd_apps.get_src_prg_ref_max_start_date(sm.source_code, sysdate)
   LEFT OUTER JOIN ftd_apps.partner_program pp ON spr.program_name = pp.program_name
   LEFT OUTER JOIN ftd_apps.program_reward pr ON pr.program_name  = pp.program_name
   LEFT OUTER JOIN ftd_apps.miles_points_redemption_rate mrr on mrr.mp_redemption_rate_id = sm.mp_redemption_rate_id
   LEFT OUTER JOIN ftd_apps.payment_method_miles_points pmmp ON pmmp.payment_method_id = mrr.payment_method_id;
    
END GET_SOURCE_LIST;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source code data for one source code
Input:
  source_code varchar
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_SOURCE_CODE
(
IN_SOURCE_CODE      in varchar2,
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

CURSOR ship_fee_cursor IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'ORDER_SERVICE_CONFIG'
             and name = 'DISPLAY_SHIP_FEE';

CURSOR service_fee_cursor IS
    SELECT   value
    FROM     frp.global_parms
    WHERE    context = 'ORDER_SERVICE_CONFIG'
             and name = 'DISPLAY_SERVICE_FEE';

v_ship_fee_flag         VARCHAR2(1);
v_service_fee_flag      VARCHAR2(1);

BEGIN

  -- Get the frp.global_parms 
    OPEN ship_fee_cursor;
   FETCH ship_fee_cursor into v_ship_fee_flag;
   CLOSE ship_fee_cursor;

    OPEN service_fee_cursor;
   FETCH service_fee_cursor into v_service_fee_flag;
   CLOSE service_fee_cursor;

    OPEN OUT_CUR FOR
            SELECT upper(sm.source_code) source_code,
                   sm.snh_id,
                   sm.price_header_id,
                   sm.company_id,
                   sm.order_source,
                   sm.related_source_code,
                   discount_allowed_flag,
                   iotw_flag,
                   pp.program_type,
                   pr.calculation_basis,
                   pr.points,
                   pr.bonus_calculation_basis,
                   pr.bonus_points,
                   pr.maximum_points,
                   pr.reward_type,
                   pmmp.payment_method_id,
                   pmmp.dollar_to_mp_operator,
                   pmmp.rounding_method_id,
                   pmmp.rounding_scale_qty,
                   mrr.mp_redemption_rate_amt,
                   CASE sm.display_service_fee_code WHEN 'D' THEN v_service_fee_flag ELSE sm.display_service_fee_code END as client_disp_svc_fee_flag,
                   CASE sm.display_shipping_fee_code WHEN 'D' THEN v_ship_fee_flag ELSE sm.display_shipping_fee_code END as client_disp_shp_fee_flag,
                   sm.start_date,
                   sm.end_date,
                   sm.price_header_id pricing_code,
                   sm.snh_id shipping_code,
                   spr.program_name partner_id,
                   sm.payment_method_id valid_pay_method,
                   sm.jcpenney_flag,
                   sm.send_to_scrub,
                   cm.company_name,
                   cm.internet_origin,
                   sm.fraud_flag,
                   sm.billing_info_logic,
                   sm.emergency_text_flag,
                   sm.requires_delivery_confirmation,
                   sm.webloyalty_flag,
                   (select sfp.florist_id from ftd_apps.source_florist_priority sfp where sfp.source_code = sm.source_code and sfp.priority = 1) as primary_florist, 
                   global.cache_pkg.get_source_florist_backup(sm.source_code) as backup_florists, 
                   global.cache_pkg.get_source_index_found(sm.source_code, 'limit_products') as source_code_has_limit_index,
                   sm.allow_free_shipping_flag,
                   pp.partner_name,
				   sm.same_day_upcharge,
                   sm.display_same_day_upcharge				   
              FROM ftd_apps.source_master sm
              JOIN ftd_apps.company_master cm ON sm.company_id = cm.company_id			  
   LEFT OUTER JOIN ftd_apps.source_program_ref spr ON sm.source_code  = spr.source_code
               AND spr.start_date  = ftd_apps.get_src_prg_ref_max_start_date(sm.source_code, sysdate)
   LEFT OUTER JOIN ftd_apps.partner_program pp ON spr.program_name = pp.program_name
   LEFT OUTER JOIN ftd_apps.program_reward pr ON pr.program_name  = pp.program_name
   LEFT OUTER JOIN ftd_apps.miles_points_redemption_rate mrr on mrr.mp_redemption_rate_id = sm.mp_redemption_rate_id
   LEFT OUTER JOIN ftd_apps.payment_method_miles_points pmmp ON pmmp.payment_method_id = mrr.payment_method_id
              WHERE sm.source_code = IN_SOURCE_CODE;
    
END GET_SOURCE_CODE;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning source template data.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_SOURCE_TEMPLATE_REF
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN

	ftd_apps.shopping_index_query_pkg.GET_SOURCE_TEMPLATE_REF(out_cur);	

END GET_SOURCE_TEMPLATE_REF;

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning special filtering index product data.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_FILTER_INDEX
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN

    ftd_apps.shopping_index_query_pkg.GET_FILTER_INDEX(out_cur);	
    
END GET_FILTER_INDEX;


PROCEDURE GET_DOMAIN_TEMPLATE
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN

    ftd_apps.shopping_index_query_pkg.GET_DOMAIN_TEMPLATE(out_cur);	
    
END GET_DOMAIN_TEMPLATE;

--==========================================================================
--
-- Name:    GET_SOURCE_INDEX_FOUND
-- Type:    Function
-- Syntax:  GET_SOURCE_INDEX_FOUND ()
-- Returns: "Y" if the index and source is found
--          "N" if the index and source is NOT found
--
--==========================================================================
FUNCTION GET_SOURCE_INDEX_FOUND
(
IN_SOURCE_CODE      in varchar2,
IN_INDEX_NAME       in varchar2 
)
RETURN VARCHAR2
AS

v_rec_found          char (1);
BEGIN
	v_rec_found := ftd_apps.shopping_index_query_pkg.GET_SOURCE_INDEX_FOUND(in_source_code, in_index_name);

	RETURN v_rec_found;
 
END GET_SOURCE_INDEX_FOUND;


/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning special characters and their equivalents.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_CHARACTER_MAPPING
(
OUT_CUR              OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
      SELECT spm.special_character_id,
             spm.english_mapping             
        FROM FTD_APPS.special_character_master spm;

END GET_CHARACTER_MAPPING;
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all the language details.
Input:
  n/a
Output:
  cursor
-----------------------------------------------------------------------------*/
PROCEDURE GET_LANGUAGE_MASTER
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN

     OPEN OUT_CUR FOR
		SELECT LANGUAGE_ID, DESCRIPTION, DISPLAY_ORDER
		FROM FTD_APPS.LANGUAGE_MASTER;
    
END GET_LANGUAGE_MASTER;

PROCEDURE GET_PAYMENT_BIN_MASTER 
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN

     OPEN OUT_CUR FOR
     SELECT BIN_NUMBER, PAYMENT_METHOD_ID
     FROM FTD_APPS.PAYMENT_BIN_MASTER
     WHERE ACTIVE_FLAG = 'Y';

END GET_PAYMENT_BIN_MASTER ;

PROCEDURE GET_PRICE_HEADER_DETAIL_LIST
(
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS
BEGIN

    OPEN OUT_CUR FOR
    SELECT price_header_id,
        min_dollar_amt,
        max_dollar_amt,
        discount_type,
        discount_amt
    FROM ftd_apps.price_header_details
    ORDER BY price_header_id, min_dollar_amt, max_dollar_amt;

END GET_PRICE_HEADER_DETAIL_LIST;


PROCEDURE GET_BOUNCE_EMAIL_TEMPLATES (
IN_COMPANY_ID      IN VARCHAR2,
OUT_CUR           OUT TYPES.REF_CURSOR) AS
/*------------------------------------------------------------------------------
Description:
        Return  rows in the frp global parms table for bounce emails.
------------------------------------------------------------------------------*/
BEGIN

IF IN_COMPANY_ID IS NOT NULL THEN

    OPEN OUT_CUR FOR
    SELECT  SUBSTR(name,13) NAME, VALUE  FROM FRP.GLOBAL_PARMS WHERE CONTEXT LIKE '%MESSAGE_GENERATOR_CONFIG%' AND NAME in(
        SELECT REGEXP_SUBSTR((SELECT VALUE  FROM FRP.GLOBAL_PARMS WHERE CONTEXT LIKE '%MESSAGE_GENERATOR_CONFIG%' AND NAME = 'BOUNCE_TEMPLATE_ID_LIST_' || IN_COMPANY_ID),'[^ ]+', 1, LEVEL) 
        FROM DUAL TEMPLATE_IDS
        connect by regexp_substr((SELECT value  FROM FRP.GLOBAL_PARMS WHERE CONTEXT LIKE '%MESSAGE_GENERATOR_CONFIG%' AND NAME = 'BOUNCE_TEMPLATE_ID_LIST_' || IN_COMPANY_ID), '[^ ]+', 1, level) 
        is not null);

ELSE

    OPEN OUT_CUR FOR
    SELECT  SUBSTR(name,13) NAME, VALUE  FROM FRP.GLOBAL_PARMS WHERE CONTEXT LIKE '%MESSAGE_GENERATOR_CONFIG%' AND NAME in(
        SELECT REGEXP_SUBSTR((SELECT VALUE  FROM FRP.GLOBAL_PARMS WHERE CONTEXT LIKE '%MESSAGE_GENERATOR_CONFIG%' AND NAME = 'BOUNCE_TEMPLATE_ID_LIST'),'[^ ]+', 1, LEVEL) 
        FROM DUAL TEMPLATE_IDS
        connect by regexp_substr((SELECT value  FROM FRP.GLOBAL_PARMS WHERE CONTEXT LIKE '%MESSAGE_GENERATOR_CONFIG%' AND NAME = 'BOUNCE_TEMPLATE_ID_LIST'), '[^ ]+', 1, level) 
        is not null);

END IF;

END GET_BOUNCE_EMAIL_TEMPLATES;

END CACHE_PKG;
.
/
