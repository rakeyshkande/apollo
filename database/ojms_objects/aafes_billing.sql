
set echo on serveroutput on size 1000000
begin
   ------------------------
   -- RUN AS OJMS
   ------------------------
   dbms_aqadm.create_queue_table (
      queue_table        => 'AAFES_BILLING',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB aafesbilling_headerproplob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS aafesbilling_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'AAFES_BILLING',
      queue_table => 'AAFES_BILLING');

   dbms_aqadm.start_queue (
      queue_name  => 'AAFES_BILLING');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_AAFES_BILLING_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'AAFES_BILLING',
      grantee => 'OSP');


   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'AAFES_BILLING',
      grantee => 'EVENTS');


end;
/
