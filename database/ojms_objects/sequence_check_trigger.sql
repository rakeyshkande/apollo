
set echo on serveroutput on size 1000000
begin
   -------------------------------
   -- OWNED BY OJMS
   -------------------------------
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.SEQUENCE_CHECK_TRIGGER',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB seqchecktrigger_headerproplob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS seqchecktrigger_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.SEQUENCE_CHECK_TRIGGER',
      queue_table => 'OJMS.SEQUENCE_CHECK_TRIGGER');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.SEQUENCE_CHECK_TRIGGER');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.AQ$_SEQUENCE_CHECK_TRIGGER_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.SEQUENCE_CHECK_TRIGGER',
      grantee => 'OSP');


   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.SEQUENCE_CHECK_TRIGGER',
      grantee => 'EVENTS');


end;
/
