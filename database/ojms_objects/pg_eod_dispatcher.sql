set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PG_EOD_DISPATCHER',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'PG_eod_dispatcher_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS PG_eod_dispatcher_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PG_EOD_DISPATCHER',
      queue_table => 'OJMS.PG_EOD_DISPATCHER',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PG_EOD_DISPATCHER');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.PG_EOD_DISPATCHER', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.PG_EOD_DISPATCHER', grantee => 'OSP_QUEUE');

connect /
