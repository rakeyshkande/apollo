set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.EOD_CART',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'eod_cart_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS eod_cart_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.EOD_CART',
      queue_table => 'OJMS.EOD_CART',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.EOD_CART');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.EOD_CART', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.EOD_CART', grantee => 'OSP_QUEUE');

connect /

