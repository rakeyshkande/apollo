set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'DCON_LIFECYCLE_UPDATE',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB DCON_LIFECYCLE_UPD_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS DCON_LIFECYCLE_UPD_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'DCON_LIFECYCLE_UPDATE',
      queue_table => 'DCON_LIFECYCLE_UPDATE');

   dbms_aqadm.start_queue (
      queue_name  => 'DCON_LIFECYCLE_UPDATE');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_DCON_LIFECYCLE_UPDATE_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'DCON_LIFECYCLE_UPDATE',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'DCON_LIFECYCLE_UPDATE',
      grantee => 'QUARTZ');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'DCON_LIFECYCLE_UPDATE',
      grantee => 'EVENTS');

end;
/
