set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'SDS_DELIVERY_CONFIRM',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB sdsdconfirm_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS sdsdconfirm_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'SDS_DELIVERY_CONFIRM',
      queue_table => 'SDS_DELIVERY_CONFIRM');

   dbms_aqadm.start_queue (
      queue_name  => 'SDS_DELIVERY_CONFIRM');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_SDS_DELIVERY_CONFIRM_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SDS_DELIVERY_CONFIRM',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SDS_DELIVERY_CONFIRM',
      grantee => 'EVENTS');


end;
.
/
