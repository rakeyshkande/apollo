set echo on serveroutput on size 1000000
begin


   dbms_aqadm.create_queue_table (
      queue_table        => 'FLOWER_JEWELS',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB flower_jewels_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS flower_jewels_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'FLOWER_JEWELS',
      queue_table => 'FLOWER_JEWELS');

   dbms_aqadm.start_queue (
      queue_name  => 'FLOWER_JEWELS');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_FLOWER_JEWELS_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FLOWER_JEWELS',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FLOWER_JEWELS',
      grantee => 'CLEAN');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FLOWER_JEWELS',
      grantee => 'JOE');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FLOWER_JEWELS',
      grantee => 'FTD_APPS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FLOWER_JEWELS',
      grantee => 'VENUS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FLOWER_JEWELS',
      grantee => 'EVENTS');

end;
/
