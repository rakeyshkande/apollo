set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.MARS_MERCURY',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'MARS_MERCURY (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS MARS_MERCURYlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.MARS_MERCURY',
      queue_table => 'OJMS.MARS_MERCURY',
      max_retries => 3,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.MARS_MERCURY');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_MERCURY', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_MERCURY', grantee => 'OSP_QUEUE');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.MARS_MERCURY', grantee => 'EVENTS');


connect /
