set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'SDS_PROCESS_SCANS',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties 
      STORE AS LOB SDS_PROCESS_SCANS_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) 
      LOB (user_data.text_lob) STORE AS SDS_PROCESS_SCANS_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'SDS_PROCESS_SCANS',
      queue_table => 'SDS_PROCESS_SCANS');

   dbms_aqadm.start_queue (
      queue_name  => 'SDS_PROCESS_SCANS');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_SDS_PROCESS_SCANS_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SDS_PROCESS_SCANS',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SDS_PROCESS_SCANS',
      grantee => 'EVENTS');


   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SDS_PROCESS_SCANS',
      grantee => 'QUARTZ');


end;
/
