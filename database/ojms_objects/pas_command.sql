
set echo on serveroutput on size 1000000
exec dbms_aqadm.drop_queue_table(queue_table=>'OJMS.PAS_COMMAND',force=>TRUE)
begin
   -------------------------------
   -- OWNED BY OJMS
   -------------------------------
dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PAS_COMMAND',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY '||
                            'user_data.header.properties STORE AS LOB pascommand_headerproplob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) '||
                            'LOB (user_data.text_lob) STORE AS pascommand_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) '||
                            'STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      sort_list          => 'priority,enq_time');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PAS_COMMAND',
      queue_table => 'OJMS.PAS_COMMAND');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PAS_COMMAND');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.AQ$_PAS_COMMAND_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

end;
/
conn &&dba_username as sysdba
exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.PAS_COMMAND', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.PAS_COMMAND', grantee => 'EVENTS');
conn &&dba_username

