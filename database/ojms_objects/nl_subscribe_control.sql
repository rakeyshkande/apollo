set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'NL_SUBSCRIBE_CONTROL',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties ' ||
                            'STORE AS LOB (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) ' ||
                            'LOB (user_data.text_lob) STORE AS (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) ' ||
                            'STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'Newsletter Subscription Control Queue');

   dbms_aqadm.create_queue (
      queue_name  => 'NL_SUBSCRIBE_CONTROL',
      queue_table => 'NL_SUBSCRIBE_CONTROL',
      comment     => 'Newsletter Subscription Control Queue');

   dbms_aqadm.start_queue (
      queue_name  => 'NL_SUBSCRIBE_CONTROL');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_NL_SUBSCRIBE_CONTROL_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'NL_SUBSCRIBE_CONTROL',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'NL_SUBSCRIBE_CONTROL',
      grantee => 'OSP');

end;
/
