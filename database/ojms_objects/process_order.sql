set echo on serveroutput on size 1000000
begin
   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PROCESS_ORDER',
      grantee => 'EVENTS');

end;
.
/
