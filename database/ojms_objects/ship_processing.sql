set echo on serveroutput on size 1000000
begin


   dbms_aqadm.create_queue_table (
      queue_table        => 'SHIP_PROCESSING',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB sdsgetstatus_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS sdsgetstatus_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'SHIP_PROCESSING',
      queue_table => 'SHIP_PROCESSING');

   dbms_aqadm.start_queue (
      queue_name  => 'SHIP_PROCESSING');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_SHIP_PROCESSING_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SHIP_PROCESSING',
      grantee => 'OSP');

-- THE FOLLOWING PRIVILEGE IS ALL THAT WAS ADDED FOR 2.1.0 RELEASE.  QUEUE EXISTED BUT HAD NO SCRIPT IN CVS

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SHIP_PROCESSING',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'SHIP_PROCESSING',
      grantee => 'VENUS');


end;
.
/
