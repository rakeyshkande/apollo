set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'DCON_ASK',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB DCON_ASK_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS DCON_ASK_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'DCON_ASK',
      queue_table => 'DCON_ASK');

   dbms_aqadm.start_queue (
      queue_name  => 'DCON_ASK');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_DCON_ASK_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'DCON_ASK',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'DCON_ASK',
      grantee => 'QUARTZ');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'DCON_ASK',
      grantee => 'EVENTS');

end;
.
/
