set serveroutput on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.SHIP_WEST_PRODUCT_UPDATE',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'SHIP_WEST_PRODUCT_UPDATE (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS SHIP_WEST_PRODUCT_UPDATElob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.SHIP_WEST_PRODUCT_UPDATE',
      queue_table => 'OJMS.SHIP_WEST_PRODUCT_UPDATE',
      max_retries => 3,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.SHIP_WEST_PRODUCT_UPDATE');
end;
/

connect / as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.SHIP_WEST_PRODUCT_UPDATE', grantee => 'OSP');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.SHIP_WEST_PRODUCT_UPDATE', grantee => 'OSP_QUEUE');

exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.SHIP_WEST_PRODUCT_UPDATE', grantee => 'EVENTS');


connect &&dba_user
