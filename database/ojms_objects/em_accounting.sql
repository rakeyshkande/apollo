set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'EM_ACCOUNTING',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'receives events messages');

   dbms_aqadm.create_queue (
      queue_name  => 'EM_ACCOUNTING',
      queue_table => 'EM_ACCOUNTING',
      comment     => 'receives events messages');

   dbms_aqadm.start_queue (
      queue_name  => 'EM_ACCOUNTING');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_EM_ACCOUNTING_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'EM_ACCOUNTING',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'EM_ACCOUNTING',
      grantee => 'OSP');

end;
.
/
