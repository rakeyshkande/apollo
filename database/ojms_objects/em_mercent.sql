set echo on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.EM_MERCENT',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      '(NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS '||
      '(NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');
end;
/

begin
   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.EM_MERCENT',
      queue_table => 'OJMS.EM_MERCENT',
      max_retries => 0);
end;
/

begin
   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.EM_MERCENT');
end;
/

conn / as sysdba

exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.EM_MERCENT', grantee => 'EVENTS')

exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.EM_MERCENT', grantee => 'OSP')

exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.EM_MERCENT', grantee => 'OSP_QUEUE')

