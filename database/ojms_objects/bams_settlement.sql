set serveroutput on define on
begin
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.BAMS_SETTLEMENT',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB '||
      'bams_settlement_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) '||
      'STORE AS bams_settlement_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE '||
      '(FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.BAMS_SETTLEMENT',
      queue_table => 'OJMS.BAMS_SETTLEMENT',
      max_retries => 5,
      retry_delay => 1);

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.BAMS_SETTLEMENT');
end;
/

connect &&dba_username as sysdba
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.BAMS_SETTLEMENT', grantee => 'OSP');
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.BAMS_SETTLEMENT', grantee => 'OSP_QUEUE');
exec dbms_aqadm.grant_queue_privilege(privilege => 'ALL', queue_name => 'OJMS.BAMS_SETTLEMENT', grantee => 'EVENTS');

connect &&dba_username

