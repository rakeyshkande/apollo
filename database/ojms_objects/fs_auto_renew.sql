set echo on serveroutput on size 1000000
begin
   ------------------------
   -- RUN AS OJMS
   ------------------------
   dbms_aqadm.create_queue_table (
      queue_table        => 'FS_AUTO_RENEW',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB fs_auto_renew_headerproplob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS fs_auto_renew_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'FS_AUTO_RENEW',
      queue_table => 'FS_AUTO_RENEW');

   dbms_aqadm.start_queue (
      queue_name  => 'FS_AUTO_RENEW');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_FS_AUTO_RENEW_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FS_AUTO_RENEW',
      grantee => 'OSP');


   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FS_AUTO_RENEW',
      grantee => 'EVENTS');


end;
/
