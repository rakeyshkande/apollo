
set echo on serveroutput on size 1000000
begin
   -------------------------------
   -- OWNED BY OJMS
   -------------------------------
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.EM_COM',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB emcom_headerproplob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS emcom_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.EM_COM',
      queue_table => 'OJMS.EM_COM');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.EM_COM');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.AQ$_EM_COM_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.EM_COM',
      grantee => 'OSP');


   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.EM_COM',
      grantee => 'EVENTS');
end;
/
