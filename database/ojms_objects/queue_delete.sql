set echo on serveroutput on size 1000000
begin


   dbms_aqadm.create_queue_table (
      queue_table        => 'QUEUE_DELETE',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB queuedelete_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS queuedelete_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'QUEUE_DELETE',
      queue_table => 'QUEUE_DELETE');

   dbms_aqadm.start_queue (
      queue_name  => 'QUEUE_DELETE');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_QUEUE_DELETE_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'QUEUE_DELETE',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'QUEUE_DELETE',
      grantee => 'CLEAN');

   dbms_aqadm.alter_queue(
      queue_name => 'QUEUE_DELETE',
      max_retries => 0);

end;
/
