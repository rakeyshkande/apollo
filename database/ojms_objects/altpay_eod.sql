set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'ALTPAY_EOD',
      storage_clause     => 'VARRAY user_data.header.properties STORE AS LOB altpay_eod_hdrlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS altpay_eod_txtlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'altpay end-of-day');

   dbms_aqadm.create_queue (
      queue_name  => 'ALTPAY_EOD',
      queue_table => 'ALTPAY_EOD',
      comment     => 'altpay end-of-day');

   dbms_aqadm.start_queue (
      queue_name  => 'ALTPAY_EOD');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_ALTPAY_EOD_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'ALTPAY_EOD',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'ALTPAY_EOD',
      grantee => 'OSP');

end;
.
/
