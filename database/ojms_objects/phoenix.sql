set echo on serveroutput on
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PHOENIX',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 '||
      'VARRAY user_data.header.properties STORE AS LOB phoenix_headerlob '||
      '(NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) '||
      'LOB (user_data.text_lob) STORE AS phoenix_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) '||
      'STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PHOENIX',
      queue_table => 'OJMS.PHOENIX');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PHOENIX');

end;
/

conn / as sysdba
exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.PHOENIX', grantee => 'OSP');
exec dbms_aqadm.grant_queue_privilege( privilege => 'ALL', queue_name => 'OJMS.PHOENIX', grantee => 'EVENTS');
conn /
