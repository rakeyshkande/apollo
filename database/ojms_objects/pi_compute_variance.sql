
set echo on serveroutput on size 1000000
begin
   -------------------------------
   -- OWNED BY OJMS
   -------------------------------
   dbms_aqadm.create_queue_table (
      queue_table        => 'OJMS.PI_COMPUTE_VARIANCE',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB picomputevar_headerproplob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS picomputevar_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'OJMS.PI_COMPUTE_VARIANCE',
      queue_table => 'OJMS.PI_COMPUTE_VARIANCE');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.PI_COMPUTE_VARIANCE');

   dbms_aqadm.start_queue (
      queue_name  => 'OJMS.AQ$_PI_COMPUTE_VARIANCE_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PI_COMPUTE_VARIANCE',
      grantee => 'OSP');


   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'OJMS.PI_COMPUTE_VARIANCE',
      grantee => 'EVENTS');


end;
/


                                     
