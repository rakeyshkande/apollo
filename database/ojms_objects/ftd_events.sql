set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'FTD_EVENTS',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB (NOCACHE NOLOGGING'||
' STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GRO'||
'UPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'receives events messages');

   dbms_aqadm.create_queue (
      queue_name  => 'FTD_EVENTS',
      queue_table => 'FTD_EVENTS',
      comment     => 'receives events messages');

   dbms_aqadm.start_queue (
      queue_name  => 'FTD_EVENTS');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_FTD_EVENTS_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FTD_EVENTS',
      grantee => 'EVENTS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FTD_EVENTS',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'FTD_EVENTS',
      grantee => 'FTD_APPS');

end;
.
/

insert into ojms.ftd_events select * from events_q.ftd_events;
exec dbms_aqadm.drop_queue_table('EVENTS_Q.FTD_EVENTS',TRUE)
commit;
