set echo on serveroutput on size 1000000
begin

   dbms_aqadm.create_queue_table (
      queue_table        => 'MESSAGE_GENERATOR',
      storage_clause     => 'INITRANS 10 VARRAY user_data.header.properties STORE AS LOB msg_gen_headerproplob (CACHE) LOB (user_data.text_lob) STORE AS msg_gen_textlob (CACHE)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE',
      comment            => 'Processes emails and letters from customer order management');

   dbms_aqadm.create_queue (
      queue_name  => 'MESSAGE_GENERATOR',
      queue_table => 'MESSAGE_GENERATOR',
      comment     => 'Processes emails and letters from customer order management');

   dbms_aqadm.start_queue (
      queue_name  => 'MESSAGE_GENERATOR');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_MESSAGE_GENERATOR_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.alter_queue_table (
      queue_table        => 'MESSAGE_GENERATOR',
      primary_instance   => 1,
      secondary_instance => 2);

end;
.
/
