set echo on serveroutput on size 1000000
begin


   dbms_aqadm.create_queue_table (
      queue_table        => 'MY_BUYS',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB my_buys_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS my_buys_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'MY_BUYS',
      queue_table => 'MY_BUYS');

   dbms_aqadm.start_queue (
      queue_name  => 'MY_BUYS');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_MY_BUYS_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'MY_BUYS',
      grantee => 'OSP');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'MY_BUYS',
      grantee => 'CLEAN');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'MY_BUYS',
      grantee => 'JOE');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'MY_BUYS',
      grantee => 'FTD_APPS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'MY_BUYS',
      grantee => 'VENUS');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'MY_BUYS',
      grantee => 'EVENTS');

end;
/
