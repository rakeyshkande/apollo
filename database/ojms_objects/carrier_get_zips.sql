set echo on serveroutput on size 1000000
begin


   dbms_aqadm.create_queue_table (
      queue_table        => 'CARRIER_GET_ZIPS',
      storage_clause     => 'PCTFREE 70 PCTUSED 30 INITRANS 30 VARRAY user_data.header.properties STORE AS LOB carrier_get_zips_headerlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) LOB (user_data.text_lob) STORE AS carrier_get_zips_textlob (NOCACHE NOLOGGING STORAGE(FREELISTS 10 FREELIST GROUPS 2)) STORAGE (FREELISTS 10 FREELIST GROUPS 2)',
      queue_payload_type => 'SYS.AQ$_JMS_TEXT_MESSAGE');

   dbms_aqadm.create_queue (
      queue_name  => 'CARRIER_GET_ZIPS',
      queue_table => 'CARRIER_GET_ZIPS',
      max_retries => 5);

   dbms_aqadm.start_queue (
      queue_name  => 'CARRIER_GET_ZIPS');

   dbms_aqadm.start_queue (
      queue_name  => 'AQ$_CARRIER_GET_ZIPS_E',
      enqueue     => FALSE,
      dequeue     => TRUE);

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'CARRIER_GET_ZIPS',
      grantee => 'CLEAN');

   dbms_aqadm.grant_queue_privilege(
      privilege => 'ALL',
      queue_name => 'CARRIER_GET_ZIPS',
      grantee => 'EVENTS');

end;
/
