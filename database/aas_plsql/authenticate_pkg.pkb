CREATE OR REPLACE
PACKAGE BODY aas.AUTHENTICATE_PKG AS

PROCEDURE AUTHENTICATE_IDENTITY
(
IN_CONTEXT_ID           IN SESSIONS.CONTEXT_ID%TYPE,
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
IN_CREDENTIALS          IN IDENTITY.CREDENTIALS%TYPE,
IN_SESSION_ID           IN SESSIONS.SESSION_ID%TYPE,
IN_UNIT_ID              IN SESSIONS.UNIT_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for authenticating an identity and its
        credential.  If valid, session information will be updated and
        tracked.

Input:
        context id                      varchar2
        identity id                     varchar2
        credentials                     varchar2
        session id                      varchar2
        unit id                         varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        EXPIRED IDENTITY
                                                        EXPIRED CREDENTIALS
                                                        INVALID IDENTITY AND CREDENTIALS

Calls:
        CONTEXT_PKG.GET_CONFIG_PARAMETER to get the context config session timeout value
        GLOBAL.MD5_HASH to match the credentials with the encrypted one in the database.
-----------------------------------------------------------------------------*/

TODAY                   DATE := SYSDATE;        -- current date/time
V_IDENTITY_EXPIRE_DATE  IDENTITY.IDENTITY_EXPIRE_DATE%TYPE;
V_CREDENTIALS_EXPIRE_DATE IDENTITY.CREDENTIALS_EXPIRE_DATE%TYPE;
V_ATTEMPT_TRY_NUMBER    IDENTITY.ATTEMPT_TRY_NUMBER%TYPE;
V_LOCKED_FLAG           IDENTITY.LOCKED_FLAG%TYPE;
V_CREDENTIALS           IDENTITY.CREDENTIALS%TYPE;

-- new time out date/time computed from the current date/time and session timeout interval in minutes
TIMEOUT                 DATE := TODAY + TO_NUMBER (CONTEXT_PKG.GET_CONFIG_PARAMETER (IN_CONTEXT_ID, 'SESSION TIMEOUT')) / 1440;

BEGIN   -- procedure body

BEGIN   -- check if the identity and credentials exist

        -- set return value to 'Y' if credentials is valid
        SELECT  'Y'
        INTO    OUT_STATUS
        FROM    IDENTITY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID
        AND     CREDENTIALS = GLOBAL.MD5_HASH (IN_CREDENTIALS)
        AND     IDENTITY_EXPIRE_DATE > TODAY
        AND     CREDENTIALS_EXPIRE_DATE > TODAY
        AND     ATTEMPT_TRY_NUMBER < 3
        AND     LOCKED_FLAG = 'N';

        UPDATE  IDENTITY
        SET     ATTEMPT_TRY_NUMBER = 0
        WHERE   IDENTITY_ID = IN_IDENTITY_ID;

        BEGIN   -- update sessions with the current info
                UPDATE  SESSIONS
                SET     CONTEXT_ID = IN_CONTEXT_ID,
                        LAST_ACCESSED = TODAY,
                        TIMEOUT_DATE = TIMEOUT,
                        UNIT_ID = IN_UNIT_ID
                WHERE   SESSION_ID = IN_SESSION_ID;

                IF ( SQL%ROWCOUNT = 0 ) THEN
                        RAISE NO_DATA_FOUND;
                END IF;

                -- insert session info if previous session doesn't exist
                EXCEPTION WHEN NO_DATA_FOUND THEN
                        INSERT INTO SESSIONS
                        (
                                IDENTITY_ID,
                                SESSION_ID,
                                CONTEXT_ID,
                                LAST_ACCESSED,
                                TIMEOUT_DATE,
                                UNIT_ID
                        )
                        VALUES
                        (
                                IN_IDENTITY_ID,
                                IN_SESSION_ID,
                                IN_CONTEXT_ID,
                                TODAY,
                                TIMEOUT,
                                IN_UNIT_ID
                        );
        END;

        -- insert user activity
        INSERT INTO USER_TRACKING
        (
                IDENTITY_ID,
                CONTEXT_ID,
                LAST_ACCESSED,
                UNIT_ID
        )
        VALUES
        (
                IN_IDENTITY_ID,
                IN_CONTEXT_ID,
                TODAY,
                IN_UNIT_ID
        );

        COMMIT;

        -- set return value to 'N' if credentials is not valid and
        -- return error message
        EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
                OUT_STATUS := 'N';

                UPDATE  IDENTITY
                SET     ATTEMPT_TRY_NUMBER = ATTEMPT_TRY_NUMBER + 1,
                        LOCKED_FLAG = DECODE (ATTEMPT_TRY_NUMBER + 1, 3, 'Y', LOCKED_FLAG)
                WHERE   IDENTITY_ID = IN_IDENTITY_ID;

                COMMIT;

                SELECT  IDENTITY_EXPIRE_DATE,
                        CREDENTIALS_EXPIRE_DATE,
                        ATTEMPT_TRY_NUMBER,
                        LOCKED_FLAG,
                        CREDENTIALS
                INTO    V_IDENTITY_EXPIRE_DATE,
                        V_CREDENTIALS_EXPIRE_DATE,
                        V_ATTEMPT_TRY_NUMBER,
                        V_LOCKED_FLAG,
                        V_CREDENTIALS
                FROM    IDENTITY
                WHERE   IDENTITY_ID = IN_IDENTITY_ID;

                IF V_ATTEMPT_TRY_NUMBER > 3 THEN
                        OUT_MESSAGE := 'TOO MANY FAILED LOGIN ATTEMPTS';
                ELSIF V_LOCKED_FLAG = 'Y' THEN
                        OUT_MESSAGE := 'IDENTITY LOCKED';
                ELSIF V_CREDENTIALS <> GLOBAL.MD5_HASH (IN_CREDENTIALS) THEN
                        OUT_MESSAGE := 'INVALID IDENTITY AND CREDENTIALS';
                ELSIF V_IDENTITY_EXPIRE_DATE < TODAY THEN
                        OUT_MESSAGE := 'EXPIRED IDENTITY';
                ELSIF V_CREDENTIALS_EXPIRE_DATE < TODAY THEN
                        OUT_MESSAGE := 'EXPIRED CREDENTIALS';
                END IF;

                EXCEPTION WHEN NO_DATA_FOUND THEN
                        OUT_MESSAGE := 'INVALID IDENTITY AND CREDENTIALS';
        END;    -- end no data found exception

        WHEN OTHERS THEN
        BEGIN
                ROLLBACK;
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

        END;    -- end exception block

END;    -- end authentication check

END AUTHENTICATE_IDENTITY;


PROCEDURE AUTHENTICATE_SECURITY_TOKEN
(
IN_CONTEXT_ID           IN SESSIONS.CONTEXT_ID%TYPE,
IN_SESSION_ID           IN SESSIONS.SESSION_ID%TYPE,
IN_UNIT_ID              IN SESSIONS.UNIT_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for authenticating the security
        token and its associated identity.  If valid, session information
        will be updated and tracked.

Input:
        context id                      varchar2
        session id                      varchar2
        unit id                         varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        EXPIRED SESSION
                                                        EXPIRED IDENTITY
                                                        INVALID SESSION

Calls:
        CONTEXT_PKG.GET_CONFIG_PARAMETER to get the context config session timeout value
-----------------------------------------------------------------------------*/

TODAY                   DATE := SYSDATE;        -- current date/time
V_IDENTITY_ID           SESSIONS.IDENTITY_ID%TYPE;
V_CONTEXT_ID            SESSIONS.CONTEXT_ID%TYPE;
V_TIMEOUT_DATE          SESSIONS.TIMEOUT_DATE%TYPE;
V_IDENTITY_EXPIRE_DATE  IDENTITY.IDENTITY_EXPIRE_DATE%TYPE;

-- new time out date/time computed from the current date/time and session timeout interval in minutes
TIMEOUT                 DATE := TODAY + TO_NUMBER (CONTEXT_PKG.GET_CONFIG_PARAMETER (IN_CONTEXT_ID, 'SESSION TIMEOUT')) / 1440;


BEGIN   -- procedure body

BEGIN   -- authenticate session

        -- set return value to 'Y' if session is authenticated and
        -- get current context id to check if the context has changed
        SELECT  'Y',
                S.IDENTITY_ID,
                S.CONTEXT_ID
        INTO    OUT_STATUS,
                V_IDENTITY_ID,
                V_CONTEXT_ID
        FROM    SESSIONS S
        JOIN    IDENTITY I
        ON      S.IDENTITY_ID = I.IDENTITY_ID
        WHERE   S.SESSION_ID = IN_SESSION_ID
        AND     S.TIMEOUT_DATE > TODAY
        AND     I.IDENTITY_EXPIRE_DATE > TODAY;

        -- update sessions with the current info
        UPDATE  SESSIONS
        SET     CONTEXT_ID = IN_CONTEXT_ID,
                LAST_ACCESSED = TODAY,
                TIMEOUT_DATE = TIMEOUT,
                UNIT_ID = IN_UNIT_ID
        WHERE   SESSION_ID = IN_SESSION_ID;

        -- insert user activity when the context has changed
        IF V_CONTEXT_ID <> IN_CONTEXT_ID THEN
                INSERT INTO USER_TRACKING
                (
                        IDENTITY_ID,
                        CONTEXT_ID,
                        LAST_ACCESSED,
                        UNIT_ID
                )
                VALUES
                (
                        V_IDENTITY_ID,
                        IN_CONTEXT_ID,
                        TODAY,
                        IN_UNIT_ID
                );
        END IF;

        COMMIT;

        -- set return value to 'N' if session is not found
        EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
                OUT_STATUS := 'N';

                SELECT  S.TIMEOUT_DATE,
                        I.IDENTITY_EXPIRE_DATE
                INTO    V_TIMEOUT_DATE,
                        V_IDENTITY_EXPIRE_DATE
                FROM    SESSIONS S
                JOIN    IDENTITY I
                ON      S.IDENTITY_ID = I.IDENTITY_ID
                WHERE   S.SESSION_ID = IN_SESSION_ID;

                IF V_TIMEOUT_DATE < TODAY THEN
                        OUT_MESSAGE := 'EXPIRED SESSION';
                ELSIF V_IDENTITY_EXPIRE_DATE < TODAY THEN
                        OUT_MESSAGE := 'EXPIRED IDENTITY';
                END IF;

                EXCEPTION WHEN NO_DATA_FOUND THEN
                        OUT_MESSAGE := 'INVALID SESSION';

        END;    -- end no data found exception block

        WHEN OTHERS THEN
        BEGIN
                ROLLBACK;
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

        END;    -- end exception block

END;    -- end authentication check

END AUTHENTICATE_SECURITY_TOKEN;


PROCEDURE ASSERT_PERMISSION
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_SESSION_ID           IN SESSIONS.SESSION_ID%TYPE,
IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
IN_PERMISSION_ID        IN REL_ACL_RP.PERMISSION_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for verifying a session is valid and its
        associated identity has the required permissions for the requested
        resource.

Input:
        context id                      varchar2
        session id                      varchar2
        resource id                     varchar2
        permission id                   varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        null
-----------------------------------------------------------------------------*/

TODAY           DATE := SYSDATE;                -- current date/time

BEGIN   -- procedure body

BEGIN   -- check session authorization for the given resource and permission in the given context

        SELECT  DISTINCT
                'Y'
        INTO    OUT_STATUS
        FROM    SESSIONS S
        JOIN    REL_IDENTITY_ROLE IR
        ON      IR.IDENTITY_ID = S.IDENTITY_ID
        JOIN    REL_ROLE_ACL RA
        ON      RA.ROLE_ID = IR.ROLE_ID
        JOIN    REL_ACL_RP ARP
        ON      ARP.ACL_NAME = RA.ACL_NAME
        WHERE   S.SESSION_ID = IN_SESSION_ID
        AND     ARP.RESOURCE_ID = IN_RESOURCE_ID
        AND     ARP.CONTEXT_ID = IN_CONTEXT_ID
        AND     ARP.PERMISSION_ID = IN_PERMISSION_ID;

        -- set return value to 'N' if session/permissions are not found
        EXCEPTION WHEN NO_DATA_FOUND THEN
                OUT_STATUS := 'N';

END;    -- end authentication check

END ASSERT_PERMISSION;


PROCEDURE REMOVE_EXPIRED_SESSIONS
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting session records that
        are older than 24 hours.

        This procedure is called from a cron job running on the database server.

Input:
        none

Output:
        none
-----------------------------------------------------------------------------*/


BEGIN

DELETE FROM SESSIONS
WHERE  LAST_ACCESSED < SYSDATE - 1;

COMMIT;

END REMOVE_EXPIRED_SESSIONS;



FUNCTION IS_CSR_IN_RESOURCE
(
IN_CSR_ID                       IN VARCHAR2,
IN_RESOURCE_ID                  IN VARCHAR2,
IN_PERMISSION_ID                IN VARCHAR2
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible checking if a users is in a given resource
        with the given permission.

Input:
        csr_id                  varchar
        resource_id             varchar
        permission_id           varchar

Output:
        Y/N
-----------------------------------------------------------------------------*/

v_out   varchar2 (1);

CURSOR check_cur IS
        SELECT  'Y'
        FROM    aas.rel_identity_role ir
        JOIN    aas.rel_role_acl ra
        ON      ir.role_id = ra.role_id
        JOIN    aas.rel_acl_rp res
        ON      ra.acl_name = res.acl_name
        AND     res.resource_id = in_resource_id
        AND     res.permission_id = in_permission_id
        WHERE   ir.identity_id = in_csr_id;

BEGIN
OPEN check_cur;
FETCH check_cur INTO v_out;
CLOSE check_cur;

IF v_out IS NULL THEN
        v_out := 'N';
END IF;

RETURN v_out;

END IS_CSR_IN_RESOURCE;

PROCEDURE ASSERT_PERMISSION
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_SESSION_ID           IN SESSIONS.SESSION_ID%TYPE,
IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
OUT_CURSOR              OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for verifying a session is valid and its
        associated identity has the required permissions for the requested
        resource.
Input:
        context id                      varchar2
        session id                      varchar2
        resource id                     varchar2
Output:
        cursor with valid permissions for a given resource and session
-----------------------------------------------------------------------------*/
BEGIN   

  OPEN OUT_CURSOR FOR 
       SELECT  DISTINCT  RESOURCE_ID, PERMISSION_ID FROM SESSIONS S 
       JOIN REL_IDENTITY_ROLE IR ON IR.IDENTITY_ID = S.IDENTITY_ID
       JOIN REL_ROLE_ACL RA ON RA.ROLE_ID = IR.ROLE_ID
       JOIN REL_ACL_RP ARP ON ARP.ACL_NAME = RA.ACL_NAME
       WHERE S.SESSION_ID = IN_SESSION_ID
       AND ARP.RESOURCE_ID = IN_RESOURCE_ID
       AND ARP.CONTEXT_ID = IN_CONTEXT_ID
       ORDER BY RESOURCE_ID;

END ASSERT_PERMISSION;


END AUTHENTICATE_PKG;
.
/
