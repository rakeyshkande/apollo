CREATE OR REPLACE
TRIGGER aas.context_config_$
AFTER INSERT OR UPDATE OR DELETE ON aas.context_config
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.context_config$ (
      context_id,
      name,
      value,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.context_id,
      :NEW.name,
      :NEW.value,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.context_config$ (
      context_id,
      name,
      value,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.context_id,
      :OLD.name,
      :OLD.value,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.context_config$ (
      context_id,
      name,
      value,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.context_id,
      :NEW.name,
      :NEW.value,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.context_config$ (
      context_id,
      name,
      value,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.context_id,
      :OLD.name,
      :OLD.value,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
