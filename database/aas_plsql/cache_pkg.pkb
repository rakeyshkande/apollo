CREATE OR REPLACE
PACKAGE BODY aas.CACHE_PKG AS

PROCEDURE GET_SESSIONS
(
IN_CONTEXT_ID           IN SESSIONS.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all session information
        for the given context.

Input:
        context id                      varchar2

Output:
        cursor result set containing session information
        identity_id                     varchar2
        session_id                      varchar2
        context_id                      varchar2
        last_accessed                   date
        timeout_date                    date
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  IDENTITY_ID,
                SESSION_ID,
                CONTEXT_ID,
                LAST_ACCESSED,
                TIMEOUT_DATE
        FROM    SESSIONS
        WHERE   CONTEXT_ID = IN_CONTEXT_ID;

END GET_SESSIONS;


PROCEDURE GET_IDENTITY_ROLE
(
IN_CONTEXT_ID           IN SESSIONS.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all identity and role
        relationships for the given context.

Input:
        context id                      varchar2

Output:
        cursor result set containing the identity role associations
        identity_id                     varchar2
        role_id                         number
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  IDENTITY_ID,
                ROLE_ID
        FROM    REL_IDENTITY_ROLE
        WHERE   IDENTITY_ID IN
        (       SELECT  IDENTITY_ID
                FROM    SESSIONS
                WHERE   CONTEXT_ID = IN_CONTEXT_ID
        );

END GET_IDENTITY_ROLE;


PROCEDURE GET_ROLE_ACL
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all role, resource,
        and permission relationships for the given context.

Input:
        context id                      varchar2

Output:
        cursor result set containing role and acl information
        role_id                         number
        resource_id                     varchar2
        permission_id                   varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  RA.ROLE_ID,
                ARP.RESOURCE_ID,
                ARP.PERMISSION_ID
        FROM    REL_ROLE_ACL RA
        JOIN    REL_ACL_RP ARP
        ON      RA.ACL_NAME = ARP.ACL_NAME
        WHERE   ARP.CONTEXT_ID = IN_CONTEXT_ID;

END GET_ROLE_ACL;


END CACHE_PKG;
.
/
