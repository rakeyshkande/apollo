CREATE OR REPLACE
TRIGGER aas.rel_identity_role_$
AFTER INSERT OR UPDATE OR DELETE ON aas.rel_identity_role
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.rel_identity_role$ (
      identity_id,
      role_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.identity_id,
      :NEW.role_id,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.rel_identity_role$ (
      identity_id,
      role_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.identity_id,
      :OLD.role_id,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.rel_identity_role$ (
      identity_id,
      role_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.identity_id,
      :NEW.role_id,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.rel_identity_role$ (
      identity_id,
      role_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.identity_id,
      :OLD.role_id,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
