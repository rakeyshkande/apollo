CREATE OR REPLACE
PACKAGE BODY aas.USERS_PKG AS


PROCEDURE INSERT_USER_HISTORY
(
IN_USER_ID                      IN USER_HISTORY.USER_ID%TYPE,
IN_CALL_CENTER_ID               IN USER_HISTORY.CALL_CENTER_ID%TYPE,
IN_CS_GROUP_ID                  IN USER_HISTORY.CS_GROUP_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting user history. The
        inserted history record's start date will be the current date.

Input:
        user_id                         number
        call_center_id                  varchar2
        cs_group_id                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR old_cur IS
        SELECT  start_date,
                end_date
        FROM    user_history
        WHERE   user_id = in_user_id
        AND     end_date =
        (
                select  max (end_date)
                from    user_history
                where   user_id = in_user_id
        );

v_today                 date := trunc (sysdate);
v_default_end_date      date := to_date ('01/01/3000', 'mm/dd/yyyy');
v_start_date            date;
v_end_date              date;

BEGIN

OPEN old_cur;
FETCH old_cur INTO v_start_date, v_end_date;
CLOSE old_cur;

IF v_start_date < v_today AND v_end_date > v_today THEN
        -- user history is found starting previous to today so update the end date of the existing record
        -- to yesterday and insert a new history record starting today
        UPDATE  user_history
        SET     end_date = v_today - 1
        WHERE   user_id = in_user_id
        AND     start_date = v_start_date
        AND     end_date = v_end_date;

        INSERT INTO user_history
        (
                user_id,
                call_center_id,
                cs_group_id,
                start_date,
                end_date
        )
        VALUES
        (
                in_user_id,
                in_call_center_id,
                in_cs_group_id,
                v_today,
                v_default_end_date
        );

ELSIF v_start_date = v_today THEN
        -- user history is found starting today so update the information on the current record
        UPDATE  user_history
        SET     call_center_id = in_call_center_id,
                cs_group_id = in_cs_group_id
        WHERE   user_id = in_user_id
        AND     start_date = v_start_date
        AND     end_date = v_end_date;

ELSE
        -- no user history record is found or the last records end date is less than today
        -- so insert a new history record. this can happen if the user is just added or
        -- readded after the user was removed
        INSERT INTO user_history
        (
                user_id,
                call_center_id,
                cs_group_id,
                start_date,
                end_date
        )
        VALUES
        (
                in_user_id,
                in_call_center_id,
                in_cs_group_id,
                v_today,
                v_default_end_date
        );

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_USER_HISTORY;


PROCEDURE GET_USER_FROM_IDENTITY
(
IN_USER_IDENTITY           IN VARCHAR2,
OUT_USER_ID              IN OUT USERS.USER_ID%TYPE
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is returns user Id for provided supervisor identity

Input:
		supervisor_id					number

Output:
        user_id                         number
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

        SELECT  U.USER_ID
        INTO    OUT_USER_ID
        FROM USERS U, IDENTITY I
		WHERE U.USER_ID = I.USER_ID
		AND I.IDENTITY_ID = IN_USER_IDENTITY;
		
END GET_USER_FROM_IDENTITY;	
		
PROCEDURE UPDATE_USER
(
IN_UPDATED_BY           IN USERS.UPDATED_BY%TYPE,
IN_LAST_NAME            IN USERS.LAST_NAME%TYPE,
IN_FIRST_NAME           IN USERS.FIRST_NAME%TYPE,
IN_ADDRESS_1            IN USERS.ADDRESS_1%TYPE,
IN_ADDRESS_2            IN USERS.ADDRESS_2%TYPE,
IN_CITY                 IN USERS.CITY%TYPE,
IN_STATE                IN USERS.STATE%TYPE,
IN_ZIP                  IN USERS.ZIP%TYPE,
IN_PHONE                IN USERS.PHONE%TYPE,
IN_EXPIRE_DATE          IN USERS.EXPIRE_DATE%TYPE,
IN_EMAIL                IN USERS.EMAIL%TYPE,
IN_EXTENSION            IN USERS.EXTENSION%TYPE,
IN_FAX                  IN USERS.FAX%TYPE,
IN_CALL_CENTER_ID       IN USERS.CALL_CENTER_ID%TYPE,
IN_DEPARTMENT_ID        IN USERS.DEPARTMENT_ID%TYPE,
IN_CS_GROUP_ID          IN USERS.CS_GROUP_ID%TYPE,
IO_USER_ID              IN OUT USERS.USER_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating user
        information.  If the user expiration date is changed, the
        associated identities' expiration dates will be updated as well.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        user_id                         number

Output:
        user_id                         number
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/
BEGIN

	UPDATE_USER
	(
		IN_UPDATED_BY,
		IN_LAST_NAME,
		IN_FIRST_NAME,
		IN_ADDRESS_1,
		IN_ADDRESS_2,
		IN_CITY,
		IN_STATE,
		IN_ZIP,
		IN_PHONE,
		IN_EXPIRE_DATE,
		IN_EMAIL,
		IN_EXTENSION,
		IN_FAX,
		IN_CALL_CENTER_ID,
		IN_DEPARTMENT_ID,
		IN_CS_GROUP_ID,
		NULL,
		NULL,
		IO_USER_ID,
		OUT_STATUS,
		OUT_MESSAGE
	);

END UPDATE_USER;



PROCEDURE UPDATE_USER
(
IN_UPDATED_BY           IN USERS.UPDATED_BY%TYPE,
IN_LAST_NAME            IN USERS.LAST_NAME%TYPE,
IN_FIRST_NAME           IN USERS.FIRST_NAME%TYPE,
IN_ADDRESS_1            IN USERS.ADDRESS_1%TYPE,
IN_ADDRESS_2            IN USERS.ADDRESS_2%TYPE,
IN_CITY                 IN USERS.CITY%TYPE,
IN_STATE                IN USERS.STATE%TYPE,
IN_ZIP                  IN USERS.ZIP%TYPE,
IN_PHONE                IN USERS.PHONE%TYPE,
IN_EXPIRE_DATE          IN USERS.EXPIRE_DATE%TYPE,
IN_EMAIL                IN USERS.EMAIL%TYPE,
IN_EXTENSION            IN USERS.EXTENSION%TYPE,
IN_FAX                  IN USERS.FAX%TYPE,
IN_CALL_CENTER_ID       IN USERS.CALL_CENTER_ID%TYPE,
IN_DEPARTMENT_ID        IN USERS.DEPARTMENT_ID%TYPE,
IN_CS_GROUP_ID          IN USERS.CS_GROUP_ID%TYPE,
IN_EMPLOYEE_ID          IN USERS.EMPLOYEE_ID%TYPE,
IN_SUPERVISOR_ID        IN USERS.SUPERVISOR_ID%TYPE,
IO_USER_ID              IN OUT USERS.USER_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating user
        information.  If the user expiration date is changed, the
        associated identities' expiration dates will be updated as well.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
		employee_id						varchar2
		supervisor_id					number
        user_id                         number

Output:
        user_id                         number
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

V_EXPIRE_DATE           USERS.EXPIRE_DATE%TYPE;

-- cursor to find if the call center id or cs group id for the user has changed
-- if so, user history will be added.
CURSOR check_cur IS
        SELECT  'Y'
        FROM    users
        WHERE   user_id = io_user_id
        AND     (call_center_id <> in_call_center_id OR
                 (call_center_id is null and in_call_center_id is not null) OR
                 cs_group_id <> in_cs_group_id OR
                 (cs_group_id is null and in_cs_group_id is not null));

v_history_check char (1) := 'N';

BEGIN   -- procedure body

IF IO_USER_ID IS NULL THEN

        SELECT  USER_ID.NEXTVAL
        INTO    IO_USER_ID
        FROM    DUAL;

        -- add new user
        INSERT INTO USERS
        (
                USER_ID,
                LAST_NAME,
                FIRST_NAME,
                ADDRESS_1,
                ADDRESS_2,
                CITY,
                STATE,
                ZIP,
                PHONE,
                EXPIRE_DATE,
                EMAIL,
                EXTENSION,
                FAX,
                CALL_CENTER_ID,
                DEPARTMENT_ID,
                CS_GROUP_ID,
                UPDATED_BY,
				EMPLOYEE_ID,
				SUPERVISOR_ID
        )
        VALUES
        (
                IO_USER_ID,
                IN_LAST_NAME,
                IN_FIRST_NAME,
                IN_ADDRESS_1,
                IN_ADDRESS_2,
                IN_CITY,
                IN_STATE,
                IN_ZIP,
                IN_PHONE,
                IN_EXPIRE_DATE,
                IN_EMAIL,
                IN_EXTENSION,
                IN_FAX,
                IN_CALL_CENTER_ID,
                IN_DEPARTMENT_ID,
                IN_CS_GROUP_ID,
                IN_UPDATED_BY,
				IN_EMPLOYEE_ID,
				IN_SUPERVISOR_ID
        );

        -- set the flag to insert user history
        v_history_check := 'Y';

ELSE

/* USER.EXPIRE_DATE IS NO LONGER USED
        -- get the old user expiration date
        SELECT  EXPIRE_DATE
        INTO    V_EXPIRE_DATE
        FROM    USERS
        WHERE   USER_ID = IO_USER_ID;

        -- if the old expiration date is not the same as the given date
        -- reset the expire date for the user identities and credentials
        -- that expired on the same day as the user or will have a
        -- date greater than the new expiration date
        IF V_EXPIRE_DATE <> IN_EXPIRE_DATE THEN
                UPDATE  IDENTITY
                SET     IDENTITY_EXPIRE_DATE = IN_EXPIRE_DATE,
                        CREDENTIALS_EXPIRE_DATE = IN_EXPIRE_DATE
                WHERE   USER_ID = IO_USER_ID
                AND     (IDENTITY_EXPIRE_DATE = V_EXPIRE_DATE
                OR       IDENTITY_EXPIRE_DATE > IN_EXPIRE_DATE
                OR       CREDENTIALS_EXPIRE_DATE = V_EXPIRE_DATE
                OR       CREDENTIALS_EXPIRE_DATE > IN_EXPIRE_DATE);
        END IF;
*/
        -- check the existing data for any changes
        OPEN check_cur;
        FETCH check_cur INTO v_history_check;
        CLOSE check_cur;

        -- update the user information
        UPDATE  USERS
        SET     LAST_NAME = IN_LAST_NAME,
                FIRST_NAME = IN_FIRST_NAME,
                ADDRESS_1 = IN_ADDRESS_1,
                ADDRESS_2 = IN_ADDRESS_2,
                CITY = IN_CITY,
                STATE = IN_STATE,
                ZIP = IN_ZIP,
                PHONE = IN_PHONE,
                EXPIRE_DATE = IN_EXPIRE_DATE,
                EMAIL = IN_EMAIL,
                EXTENSION = IN_EXTENSION,
                FAX = IN_FAX,
                CALL_CENTER_ID = IN_CALL_CENTER_ID,
                DEPARTMENT_ID = IN_DEPARTMENT_ID,
                CS_GROUP_ID = IN_CS_GROUP_ID,
                UPDATED_ON = SYSDATE,
                UPDATED_BY = IN_UPDATED_BY,
				EMPLOYEE_ID = IN_EMPLOYEE_ID,
				SUPERVISOR_ID = IN_SUPERVISOR_ID
        WHERE   USER_ID = IO_USER_ID;
END IF;

IF v_history_check = 'Y' THEN
        INSERT_USER_HISTORY
        (
                IO_USER_ID,
                IN_CALL_CENTER_ID,
                IN_CS_GROUP_ID,
                OUT_STATUS,
                OUT_MESSAGE
        );
ELSE
        OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_USER;


PROCEDURE REMOVE_USER
(
IN_USER_ID              IN USERS.USER_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting user information.  Delete
        associated identity records, sessions, and identity role mappings.

Input:
        user_id                         number

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
        
Special Instructions:

  		  This procedure is not being used currently.      
-----------------------------------------------------------------------------*/

BEGIN   -- begin procedure

DELETE FROM SESSIONS
WHERE   IDENTITY_ID IN
(       SELECT  IDENTITY_ID
        FROM    IDENTITY
        WHERE   USER_ID = IN_USER_ID
);

DELETE FROM REL_IDENTITY_ROLE
WHERE   IDENTITY_ID IN
(       SELECT  IDENTITY_ID
        FROM    IDENTITY
        WHERE   USER_ID = IN_USER_ID
);

DELETE FROM CREDENTIALS_HISTORY
WHERE   IDENTITY_ID IN
(       SELECT  IDENTITY_ID
        FROM    IDENTITY
        WHERE   USER_ID = IN_USER_ID
);

DELETE FROM IDENTITY
WHERE   USER_ID = IN_USER_ID;

DELETE FROM USERS
WHERE   USER_ID = IN_USER_ID;

-- update the current history record to today's date for removed users
UPDATE  user_history
SET     end_date = trunc (sysdate)
WHERE   user_id = in_user_id
AND     end_date =
(
        select  max (end_date)
        from    user_history
        where   user_id = in_user_id
        and     end_date > trunc (sysdate)
);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_USER;


PROCEDURE UPDATE_IDENTITY_PCI
(
IN_UPDATED_BY                   IN IDENTITY.UPDATED_BY%TYPE,
IN_IDENTITY_ID                  IN IDENTITY.IDENTITY_ID%TYPE,
IN_USER_ID                      IN IDENTITY.USER_ID%TYPE,
IN_DESCRIPTION                  IN IDENTITY.DESCRIPTION%TYPE,
IN_IDENTITY_EXPIRE_DATE         IN IDENTITY.IDENTITY_EXPIRE_DATE%TYPE,
IN_LOCKED_FLAG                  IN IDENTITY.LOCKED_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2,
OUT_CREDENTIALS			OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating identity
        information.  For a new identity, the default expired password will be
        set.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        identity_id                     varchar2
        user_id                         number
        description                     varchar2
        identity_expire_date            date

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
	credentials			varchar2	New password
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- update/insert identity information

        UPDATE  IDENTITY
        SET     USER_ID = IN_USER_ID,
                DESCRIPTION = IN_DESCRIPTION,
                IDENTITY_EXPIRE_DATE = IN_IDENTITY_EXPIRE_DATE,
                LOCKED_FLAG = IN_LOCKED_FLAG,
                UPDATED_ON = SYSDATE,
                UPDATED_BY = IN_UPDATED_BY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID;

        IF ( SQL%ROWCOUNT = 0 ) THEN
                RAISE NO_DATA_FOUND;
        END IF;

        -- insert identity information if identity doesn't exist
        EXCEPTION WHEN NO_DATA_FOUND THEN
		OUT_CREDENTIALS:= GENERATE_PASSWORD();

                INSERT INTO IDENTITY
                (
                        IDENTITY_ID,
                        CREDENTIALS,
                        USER_ID,
                        DESCRIPTION,
                        IDENTITY_EXPIRE_DATE,
                        CREDENTIALS_EXPIRE_DATE,
                        LOCKED_FLAG,
                        UPDATED_BY
                )
                VALUES
                (
                        IN_IDENTITY_ID,
                        GLOBAL.MD5_HASH (OUT_CREDENTIALS),
                        IN_USER_ID,
                        IN_DESCRIPTION,
                        IN_IDENTITY_EXPIRE_DATE,
                        SYSDATE,                                  -- credential expire date set to expired
                        IN_LOCKED_FLAG,
                        IN_UPDATED_BY
                );

END;    -- end update/insert block

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_IDENTITY_PCI;


PROCEDURE REMOVE_IDENTITY
(
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting identity information. Delete
        associated sessions and identity role mappings.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        identity_id                     varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- begin procedure

DELETE FROM SESSIONS
WHERE   IDENTITY_ID = IN_IDENTITY_ID;

DELETE FROM REL_IDENTITY_ROLE
WHERE   IDENTITY_ID = IN_IDENTITY_ID;

DELETE FROM CREDENTIALS_HISTORY
WHERE   IDENTITY_ID = IN_IDENTITY_ID;

DELETE FROM IDENTITY
WHERE   IDENTITY_ID = IN_IDENTITY_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_IDENTITY;


PROCEDURE VIEW_USER
(
IN_USER_ID              IN USERS.USER_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given user's
        information.

Input:
        user id                         number

Output:
        cursor result set containing user information
        user_id                         number
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        call_center_desc                varchar2
        department_desc                 varchar2
        cs_group_desc                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  U.USER_ID,
                U.LAST_NAME,
                U.FIRST_NAME,
                U.ADDRESS_1,
                U.ADDRESS_2,
                U.CITY,
                U.STATE,
                U.ZIP,
                U.PHONE,
                U.EXPIRE_DATE,
                U.EMAIL,
                U.EXTENSION,
                U.FAX,
                U.CALL_CENTER_ID,
                U.DEPARTMENT_ID,
                U.CS_GROUP_ID,
		U.EMPLOYEE_ID,
		U.SUPERVISOR_ID,
                CC.DESCRIPTION AS CALL_CENTER_DESC,
                D.DESCRIPTION AS DEPARTMENT_DESC,
                CG.DESCRIPTION AS CS_GROUP_DESC
        FROM    USERS U
        JOIN    FTD_APPS.DEPARTMENTS D
        ON      U.DEPARTMENT_ID = D.DEPARTMENT_ID
        LEFT OUTER JOIN    FTD_APPS.CALL_CENTER CC
        ON      U.CALL_CENTER_ID = CC.CALL_CENTER_ID
        LEFT OUTER JOIN    FTD_APPS.CS_GROUPS CG
        ON      U.CS_GROUP_ID = CG.CS_GROUP_ID
        WHERE   U.USER_ID = IN_USER_ID;

END VIEW_USER;

PROCEDURE VIEW_USER_BY_SESSION_ID
(
IN_SESSION_ID           IN SESSIONS.SESSION_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the user's
        information for the given session id

Input:
        session id                      varchar2

Output:
        cursor result set containing user information
        user_id                         number
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        identity_id                     varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        call_center_desc                varchar2
        department_desc                 varchar2
        cs_group_desc                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  U.USER_ID,
                U.LAST_NAME,
                U.FIRST_NAME,
                U.ADDRESS_1,
                U.ADDRESS_2,
                U.CITY,
                U.STATE,
                U.ZIP,
                U.PHONE,
                U.EXPIRE_DATE,
                U.EMAIL,
                U.EXTENSION,
                U.FAX,
                I.IDENTITY_ID,
                U.CALL_CENTER_ID,
                U.DEPARTMENT_ID,
                U.CS_GROUP_ID,
                (SELECT CC.DESCRIPTION FROM FTD_APPS.CALL_CENTER CC WHERE U.CALL_CENTER_ID = CC.CALL_CENTER_ID) AS CALL_CENTER_DESC,
                D.DESCRIPTION AS DEPARTMENT_DESC,
                (SELECT CG.DESCRIPTION FROM FTD_APPS.CS_GROUPS CG WHERE U.CS_GROUP_ID = CG.CS_GROUP_ID) AS CS_GROUP_DESC
        FROM    USERS U
        JOIN    FTD_APPS.DEPARTMENTS D
        ON      U.DEPARTMENT_ID = D.DEPARTMENT_ID
        JOIN    IDENTITY I
        ON      U.USER_ID = I.USER_ID
        JOIN    SESSIONS S
        ON      I.IDENTITY_ID = S.IDENTITY_ID
        WHERE   S.SESSION_ID = IN_SESSION_ID;

END VIEW_USER_BY_SESSION_ID;


PROCEDURE VIEW_USERS
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all users' information.

Input:
        none

Output:
        cursor result set containing user information
        user_id                         number
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        call_center_desc                varchar2
        department_desc                 varchar2
        cs_group_desc                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  U.USER_ID,
                U.LAST_NAME,
                U.FIRST_NAME,
                U.ADDRESS_1,
                U.ADDRESS_2,
                U.CITY,
                U.STATE,
                U.ZIP,
                U.PHONE,
                U.EXPIRE_DATE,
                U.EMAIL,
                U.EXTENSION,
                U.FAX,
                U.CALL_CENTER_ID,
                U.DEPARTMENT_ID,
                U.CS_GROUP_ID,
                CC.DESCRIPTION AS CALL_CENTER_DESC,
                D.DESCRIPTION AS DEPARTMENT_DESC,
                CG.DESCRIPTION AS CS_GROUP_DESC
        FROM    USERS U
        JOIN    FTD_APPS.DEPARTMENTS D
        ON      U.DEPARTMENT_ID = D.DEPARTMENT_ID
        LEFT OUTER JOIN    FTD_APPS.CALL_CENTER CC
        ON      U.CALL_CENTER_ID = CC.CALL_CENTER_ID
        LEFT OUTER JOIN    FTD_APPS.CS_GROUPS CG
        ON      U.CS_GROUP_ID = CG.CS_GROUP_ID
        ORDER BY LAST_NAME, FIRST_NAME, USER_ID;

END VIEW_USERS;


PROCEDURE VIEW_USER_IDENTITIES
(
IN_USER_ID              IN USERS.USER_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all identities'
        information.

Input:
        user id                         number

Output:
        cursor result set containing identity information
        identity_id                     varchar2
        credentials                     varchar2
        user_id                         number
        description                     varchar2
        identity_expire_date            date
        credentials_expire_date         date
        locked_flag                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  IDENTITY_ID,
                CREDENTIALS,
                USER_ID,
                DESCRIPTION,
                IDENTITY_EXPIRE_DATE,
                CREDENTIALS_EXPIRE_DATE,
                LOCKED_FLAG
        FROM    IDENTITY
        WHERE   USER_ID = IN_USER_ID
        ORDER BY IDENTITY_ID;

END VIEW_USER_IDENTITIES;


PROCEDURE VIEW_IDENTITY
(
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning identity information
        for the given identity id

Input:
        identity id                     number

Output:
        cursor result set containing identity information
        identity_id                     varchar2
        credentials                     varchar2
        user_id                         number
        description                     varchar2
        identity_expire_date            date
        credentials_expire_date         date
        locked_flag                     varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  IDENTITY_ID,
                CREDENTIALS,
                USER_ID,
                DESCRIPTION,
                IDENTITY_EXPIRE_DATE,
                CREDENTIALS_EXPIRE_DATE,
                LOCKED_FLAG
        FROM    IDENTITY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID;

END VIEW_IDENTITY;


PROCEDURE VIEW_IDENTITY_ROLES
(
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given identity's
        associated roles and all other roles not associated to the identity.

Input:
        identity id                     number

Output:
        cursor result set containing role information
        role_id                         number
        role_name                       varchar2
        context_id                      varchar2
        description                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  R.ROLE_ID,
                R.ROLE_NAME,
                R.CONTEXT_ID,
                R.DESCRIPTION,
                IR.IDENTITY_ID                          -- this will be null if the identity is not associated
        FROM    ROLE R
        LEFT OUTER JOIN REL_IDENTITY_ROLE IR
        ON      IR.ROLE_ID = R.ROLE_ID
        AND     IR.IDENTITY_ID = IN_IDENTITY_ID
        ORDER BY R.ROLE_NAME;

END VIEW_IDENTITY_ROLES;


PROCEDURE VIEW_USER_IDENTITIES_ROLES
(
IN_USER_ID              IN USERS.USER_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all identities'
        information.

Input:
        user id                         number

Output:
        cursor result set containing identity and role information
        identity_id                     varchar2
        credentials                     varchar2
        user_id                         number
        description                     varchar2
        identity_expire_date            date
        credentials_expire_date         date
        role_id                         number
        role_name                       varchar2
        context_id                      varchar2
        description                     varchar2
        locked_flag                     varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  I.IDENTITY_ID,
                I.CREDENTIALS,
                I.USER_ID,
                I.DESCRIPTION,
                I.IDENTITY_EXPIRE_DATE,
                I.CREDENTIALS_EXPIRE_DATE,
                R.ROLE_ID,
                R.ROLE_NAME,
                R.CONTEXT_ID,
                R.DESCRIPTION,
                I.LOCKED_FLAG
        FROM    IDENTITY I
        LEFT OUTER JOIN REL_IDENTITY_ROLE IR
        ON      I.IDENTITY_ID = IR.IDENTITY_ID
        LEFT OUTER JOIN ROLE R
        ON      IR.ROLE_ID = R.ROLE_ID
        WHERE   I.USER_ID = IN_USER_ID
        ORDER BY I.IDENTITY_ID, R.ROLE_NAME;

END VIEW_USER_IDENTITIES_ROLES;


PROCEDURE VIEW_USERS_BY_NAME
(
IN_LAST_NAME            IN USERS.LAST_NAME%TYPE,
IN_FIRST_NAME           IN USERS.FIRST_NAME%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning users' information,
        identities, and associated roles for the given first and last name.
        !!! THIS PROCEDURE IS NOT USED.
Input:
        last_name                       varchar2
        first_name                      varchar2

Output:
        cursor result set containing user, identity and role information
        user_id                         number
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        identity_id                     varchar2
        role_name                       varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        call_center_desc                varchar2
        department_desc                 varchar2
        cs_group_desc                   varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  U.USER_ID,
                U.LAST_NAME,
                U.FIRST_NAME,
                U.ADDRESS_1,
                U.ADDRESS_2,
                U.CITY,
                U.STATE,
                U.ZIP,
                U.PHONE,
                U.EXPIRE_DATE,
                U.EMAIL,
                U.EXTENSION,
                U.FAX,
                I.IDENTITY_ID,
                R.ROLE_NAME,
                U.CALL_CENTER_ID,
                U.DEPARTMENT_ID,
                U.CS_GROUP_ID,
                CC.DESCRIPTION AS CALL_CENTER_DESC,
                D.DESCRIPTION AS DEPARTMENT_DESC,
                CG.DESCRIPTION AS CS_GROUP_DESC
        FROM    USERS U
        JOIN    FTD_APPS.DEPARTMENTS D
        ON      U.DEPARTMENT_ID = D.DEPARTMENT_ID
        JOIN    IDENTITY I
        ON      U.USER_ID = I.USER_ID
        JOIN    REL_IDENTITY_ROLE IR
        ON      I.IDENTITY_ID = IR.IDENTITY_ID
        JOIN    ROLE R
        ON      IR.ROLE_ID = R.ROLE_ID
        LEFT OUTER JOIN    FTD_APPS.CALL_CENTER CC
        ON      U.CALL_CENTER_ID = CC.CALL_CENTER_ID
        LEFT OUTER JOIN    FTD_APPS.CS_GROUPS CG
        ON      U.CS_GROUP_ID = CG.CS_GROUP_ID
        WHERE   U.LAST_NAME LIKE IN_LAST_NAME || '%'
        AND     U.FIRST_NAME LIKE IN_FIRST_NAME || '%'
        ORDER BY LAST_NAME, FIRST_NAME, USER_ID;

END VIEW_USERS_BY_NAME;


FUNCTION IDENTITY_EXISTS
(
IN_IDENTITY_ID          IN VARCHAR2
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an identity exists.

Input:
        identity_id                     varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    IDENTITY
WHERE   IDENTITY_ID = IN_IDENTITY_ID;


IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

END IDENTITY_EXISTS;


FUNCTION CHECK_CREDENTIAL_ALPHANUMERIC
(
IN_CREDENTIAL           IN VARCHAR2
)
RETURN BOOLEAN

AS

/*-----------------------------------------------------------------------------
Description:
        Check if the credential contains letters and numbers.

Input:
        credentials                     varchar2

Output:
        true - contains both letters and numbers
        false - does not contain either a letter or a number
-----------------------------------------------------------------------------*/

V_TRANSLATED    VARCHAR2 (4000);
V_RET           BOOLEAN := FALSE;
ALPHACHARS      VARCHAR2(52) := 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
ALPHASUB        VARCHAR2(52) := 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA';
NUMCHARS        VARCHAR2(10) := '0123456789';
NUMSUB          VARCHAR2(10) := '1111111111';

BEGIN

V_TRANSLATED := TRANSLATE (IN_CREDENTIAL, ALPHACHARS || NUMCHARS, ALPHASUB || NUMSUB);

IF V_TRANSLATED LIKE '%A%' AND V_TRANSLATED LIKE '%1%' THEN
        V_RET := TRUE;
ELSE
        V_RET := FALSE;
END IF;

RETURN V_RET;

END CHECK_CREDENTIAL_ALPHANUMERIC;


FUNCTION CHECK_PREVIOUS_CREDENTIAL
(
IN_IDENTITY_ID          IN CREDENTIALS_HISTORY.IDENTITY_ID%TYPE,
IN_CREDENTIALS          IN CREDENTIALS_HISTORY.CREDENTIALS%TYPE
)
RETURN BOOLEAN

AS

/*-----------------------------------------------------------------------------
Description:
        Check if the credential was in used in the last four times.

Input:
        credentials                     varchar2

Output:
        true - was used
        false - was not used
-----------------------------------------------------------------------------*/

V_RET           BOOLEAN := FALSE;
V_COUNT         NUMBER;

BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    CREDENTIALS_HISTORY
WHERE   IDENTITY_ID = IN_IDENTITY_ID
AND     CREDENTIALS = GLOBAL.MD5_HASH (IN_CREDENTIALS);

IF V_COUNT > 0 THEN
        V_RET := TRUE;
ELSE
        V_RET := FALSE;
END IF;

RETURN V_RET;

END CHECK_PREVIOUS_CREDENTIAL;


PROCEDURE UPDATE_CREDENTIAL
(
IN_UPDATED_BY                   IN IDENTITY.UPDATED_BY%TYPE,
IN_IDENTITY_ID                  IN IDENTITY.IDENTITY_ID%TYPE,
IN_CREDENTIALS                  IN IDENTITY.CREDENTIALS%TYPE,
IN_CREDENTIALS_EXPIRE_DATE      IN IDENTITY.CREDENTIALS_EXPIRE_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        Update credentials for the given identity, keep the last four
        credentials in the history table.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        identity_id                     varchar2
        credentials                     varchar2
        credentials_expire_date         date

Output:
        status                          varchar2        Y or N
        error message                   varchar2
-----------------------------------------------------------------------------*/

BEGIN

-- save the last password
INSERT  INTO CREDENTIALS_HISTORY
(
        IDENTITY_ID,
        CREDENTIALS,
        UPDATED_BY
)
SELECT  IDENTITY_ID,
        CREDENTIALS,
        IN_UPDATED_BY
FROM    IDENTITY
WHERE   IDENTITY_ID = IN_IDENTITY_ID;

-- keep only the last 4 passwords
DELETE  FROM CREDENTIALS_HISTORY
WHERE   IDENTITY_ID = IN_IDENTITY_ID
AND     CREATED_ON =
(       SELECT  MIN (CREATED_ON)
        FROM    CREDENTIALS_HISTORY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID
        GROUP BY IDENTITY_ID HAVING COUNT (1) > 4
);

-- update the identity with the new password
UPDATE  IDENTITY
SET     CREDENTIALS = GLOBAL.MD5_HASH (IN_CREDENTIALS),
        CREDENTIALS_EXPIRE_DATE = IN_CREDENTIALS_EXPIRE_DATE,
        ATTEMPT_TRY_NUMBER = 0,
        LOCKED_FLAG = 'N',
        UPDATED_BY = IN_UPDATED_BY
WHERE   IDENTITY_ID = IN_IDENTITY_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_CREDENTIAL;



PROCEDURE UPDATE_CREDENTIALS_PCI
(
IN_UPDATED_BY                   IN IDENTITY.UPDATED_BY%TYPE,
IN_IDENTITY_ID                  IN IDENTITY.IDENTITY_ID%TYPE,
IN_OLD_CREDENTIALS              IN IDENTITY.CREDENTIALS%TYPE,
IN_NEW_CREDENTIALS              IN IDENTITY.CREDENTIALS%TYPE,
IN_NEW_CREDENTIALS_RETYPE       IN IDENTITY.CREDENTIALS%TYPE,
IN_ADMIN_RESET                  IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2,
OUT_CREDENTIALS			OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        If admin is resetting password - admin reset = 'Y'
        If user is changing password - admin reset = 'N', old, new, retype new must be specified.
        Password validation:
        1) new and retype new need to be the same
        2) length of new password needs to be between 1 and 12 character in length
        3) new password needs to contain alphabetic and numeric characters
        4) cannot reuse last 4 passwords
        5) password cannot contain identity id
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        identity_id                     varchar2
        old credentials                 varchar2
        new credentials                 varchar2
        new credentials retype          varchar2
        admin reset                     varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2
	credentials			varchar2	New password
-----------------------------------------------------------------------------*/

NEW_CREDENTIAL_EXPIRE_DATE      DATE := TRUNC (SYSDATE) + 45;   -- new credentials expire in 45 days

V_ATTEMPT_TRY_NUMBER    IDENTITY.ATTEMPT_TRY_NUMBER%TYPE;
V_LOCKED_FLAG           IDENTITY.LOCKED_FLAG%TYPE;

BEGIN   -- procedure body

IF IN_ADMIN_RESET = 'Y' THEN
	OUT_CREDENTIALS:= GENERATE_PASSWORD();

        UPDATE_CREDENTIAL (IN_UPDATED_BY, IN_IDENTITY_ID, OUT_CREDENTIALS, SYSDATE, OUT_STATUS, OUT_MESSAGE);

ELSE
        -- validate old password
        SELECT  'Y'
        INTO    OUT_STATUS
        FROM    IDENTITY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID
        AND     CREDENTIALS = GLOBAL.MD5_HASH (IN_OLD_CREDENTIALS)
        AND     ATTEMPT_TRY_NUMBER < 3
        AND     LOCKED_FLAG = 'N';

        -- validate new password
        IF IN_NEW_CREDENTIALS <> IN_NEW_CREDENTIALS_RETYPE THEN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'APP-01002'; -- 'NEW AND CONFIRMING PASSWORDS DO NOT MATCH'
                GOTO EXIT;
        ELSIF LENGTH (IN_NEW_CREDENTIALS) NOT BETWEEN 7 AND 20 THEN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'APP-01003'; -- 'NEW PASSWORD MUST BE AT LEAST 7 CHARACTERS AND NO GREATER THAN 12 CHARACTERS'
                GOTO EXIT;
        ELSIF NOT CHECK_CREDENTIAL_ALPHANUMERIC (IN_NEW_CREDENTIALS) THEN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'APP-01004'; -- 'NEW PASSWORD MUST CONTAIN BOTH LETTERS AND NUMBERS'
                GOTO EXIT;
        ELSIF CHECK_PREVIOUS_CREDENTIAL (IN_IDENTITY_ID, IN_NEW_CREDENTIALS) THEN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'APP-01005'; -- 'NEW PASSWORD IS INVALID BECAUSE THE LAST 4 PASSWORDS CANNOT BE REUSED'
                GOTO EXIT;
        ELSIF UPPER (IN_NEW_CREDENTIALS) LIKE '%' || UPPER (IN_IDENTITY_ID) || '%' THEN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'APP-01006'; -- 'NEW PASSWORD IS INVALID BECAUSE IT CONTAINS THE USER ID'
                GOTO EXIT;
        END IF;

        UPDATE_CREDENTIAL (IN_UPDATED_BY, IN_IDENTITY_ID, IN_NEW_CREDENTIALS, NEW_CREDENTIAL_EXPIRE_DATE, OUT_STATUS, OUT_MESSAGE);

END IF;    -- update credentials

<<EXIT>>
OUT_STATUS := OUT_STATUS;

-- old creditial is not valid
EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        OUT_STATUS := 'N';

        UPDATE  IDENTITY
        SET     ATTEMPT_TRY_NUMBER = ATTEMPT_TRY_NUMBER + 1,
                LOCKED_FLAG = DECODE (ATTEMPT_TRY_NUMBER + 1, 3, 'Y', LOCKED_FLAG)
        WHERE   IDENTITY_ID = IN_IDENTITY_ID;

        SELECT  ATTEMPT_TRY_NUMBER,
                LOCKED_FLAG
        INTO    V_ATTEMPT_TRY_NUMBER,
                V_LOCKED_FLAG
        FROM    IDENTITY
        WHERE   IDENTITY_ID = IN_IDENTITY_ID;

        IF V_ATTEMPT_TRY_NUMBER > 3 THEN
                OUT_MESSAGE := 'APP-01007'; -- 'TOO MANY FAILED LOGIN ATTEMPTS';
        ELSIF V_LOCKED_FLAG = 'Y' THEN
                OUT_MESSAGE := 'APP-01008'; -- 'IDENTITY LOCKED';
        ELSE
                OUT_MESSAGE := 'APP-01001'; -- 'INVALID IDENTITY AND CREDENTIALS';
        END IF;

END;    -- end credential exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block



END UPDATE_CREDENTIALS_PCI;

PROCEDURE SEARCH_USERS
(
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
IN_LAST_NAME            IN USERS.LAST_NAME%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning users' information,
        identities for either the identity id or last name

Input:
        identity_id                     varchar2
        last_name                       varchar2

Output:
        cursor result set containing user, identity information
        user_id                         number
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        identity_id                     varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        call_center_desc                varchar2
        department_desc                 varchar2
        cs_group_desc                   varchar2
        locked_flag                     varchar2
        credentials_expire_date         date
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  U.USER_ID,
                U.LAST_NAME,
                U.FIRST_NAME,
                U.ADDRESS_1,
                U.ADDRESS_2,
                U.CITY,
                U.STATE,
                U.ZIP,
                U.PHONE,
                I.IDENTITY_EXPIRE_DATE,
                U.EMAIL,
                U.EXTENSION,
                U.FAX,
                I.IDENTITY_ID,
                U.CALL_CENTER_ID,
                U.DEPARTMENT_ID,
                U.CS_GROUP_ID,
                CC.DESCRIPTION AS CALL_CENTER_DESC,
                D.DESCRIPTION AS DEPARTMENT_DESC,
                CG.DESCRIPTION AS CS_GROUP_DESC,
                I.LOCKED_FLAG,
                I.CREDENTIALS_EXPIRE_DATE
        FROM    USERS U
        JOIN    FTD_APPS.DEPARTMENTS D
        ON      U.DEPARTMENT_ID = D.DEPARTMENT_ID
        JOIN    IDENTITY I
        ON      U.USER_ID = I.USER_ID
        LEFT OUTER JOIN    FTD_APPS.CALL_CENTER CC
        ON      U.CALL_CENTER_ID = CC.CALL_CENTER_ID
        LEFT OUTER JOIN    FTD_APPS.CS_GROUPS CG
        ON      U.CS_GROUP_ID = CG.CS_GROUP_ID
        WHERE   (UPPER (U.LAST_NAME) LIKE UPPER (IN_LAST_NAME) || '%' OR IN_LAST_NAME IS NULL)
        AND     (UPPER (I.IDENTITY_ID) LIKE UPPER (IN_IDENTITY_ID) || '%' OR IN_IDENTITY_ID IS NULL)
        ORDER BY LAST_NAME, FIRST_NAME, USER_ID;

END SEARCH_USERS;


PROCEDURE VIEW_ROLES_BY_IDENTITY_ID
(
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the roles associated to
        the given user.

Input:
        identity_id                     varchar2

Output:
        cursor result set containing role information
        role name                       varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  R.ROLE_NAME
        FROM    ROLE R
        JOIN    REL_IDENTITY_ROLE IR
        ON      R.ROLE_ID = IR.ROLE_ID
        WHERE   IR.IDENTITY_ID = IN_IDENTITY_ID
        ORDER BY R.ROLE_NAME;

END VIEW_ROLES_BY_IDENTITY_ID;


PROCEDURE VIEW_USER_BY_ROLE_NAME
(
IN_ROLE_NAME            IN ROLE.ROLE_NAME%TYPE,
IN_CONTEXT_ID           IN ROLE.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the user's
        information for the given role name

Input:
        role name                       varchar2

Output:
        cursor result set containing user information
        user_id                         number
        last_name                       varchar2
        first_name                      varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip                             varchar2
        phone                           varchar2
        expire_date                     date
        email                           varchar2
        extension                       varchar2
        fax                             varchar2
        identity_id                     varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        call_center_desc                varchar2
        department_desc                 varchar2
        cs_group_desc                   varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  U.USER_ID,
                U.LAST_NAME,
                U.FIRST_NAME,
                U.ADDRESS_1,
                U.ADDRESS_2,
                U.CITY,
                U.STATE,
                U.ZIP,
                U.PHONE,
                U.EXPIRE_DATE,
                U.EMAIL,
                U.EXTENSION,
                U.FAX,
                I.IDENTITY_ID,
                U.CALL_CENTER_ID,
                U.DEPARTMENT_ID,
                U.CS_GROUP_ID,
                (SELECT CC.DESCRIPTION FROM FTD_APPS.CALL_CENTER CC WHERE U.CALL_CENTER_ID = CC.CALL_CENTER_ID) AS CALL_CENTER_DESC,
                D.DESCRIPTION AS DEPARTMENT_DESC,
                (SELECT CG.DESCRIPTION FROM FTD_APPS.CS_GROUPS CG WHERE U.CS_GROUP_ID = CG.CS_GROUP_ID) AS CS_GROUP_DESC
        FROM    USERS U
        JOIN    FTD_APPS.DEPARTMENTS D
        ON      U.DEPARTMENT_ID = D.DEPARTMENT_ID
        JOIN    IDENTITY I
        ON      U.USER_ID = I.USER_ID
        JOIN    REL_IDENTITY_ROLE IR
        ON      I.IDENTITY_ID = IR.IDENTITY_ID
        JOIN    ROLE R
        ON      IR.ROLE_ID = R.ROLE_ID
        WHERE   R.ROLE_NAME = IN_ROLE_NAME
        AND     R.CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_USER_BY_ROLE_NAME;

/*-----------------------------------------------------------------------------
Description:
   This procedure with generate a random 12 character password consisting of any
   printable characters.

Input:
Output:
        return		varchar2 - new password
-----------------------------------------------------------------------------*/
FUNCTION GENERATE_PASSWORD return VARCHAR2
AS
   v_seed number;
BEGIN
	select init_sq.nextval into v_seed from dual;
	DBMS_RANDOM.SEED(v_seed);
	return DBMS_RANDOM.string('l',6)||to_char(systimestamp,'FF1');
END;

PROCEDURE GET_USERS_BY_DEPARTMENT
(
   IN_DEPARTMENT_ID IN VARCHAR,
   OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        This stored procedure retrieves all records from table users for a
        given department id.

Input:
        department id

Output:
        cursor containing user records
------------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
     SELECT user_id,
            last_name,
            first_name,
            address_1,
            address_2,
            city,
            state,
            zip,
            phone,
            expire_date,
            email,
            extension,
            fax,
            call_center_id,
            department_id,
            cs_group_id
       FROM users
      WHERE department_id = in_department_id
      ORDER BY last_name,
               first_name;

END GET_USERS_BY_DEPARTMENT;


PROCEDURE GET_USERS_BY_ROLE_NAME
(
 IN_ROLE_NAME  IN ROLE.ROLE_NAME%TYPE,
 OUT_CURSOR   OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the user's
        information for the given role name

Input:
        role name                       varchar2

Output:
        cursor result set containing user information

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
        SELECT u.user_id,
               u.last_name,
               u.first_name,
               u.address_1,
               u.address_2,
               u.city,
               u.state,
               u.zip,
               u.phone,
               u.expire_date,
               u.email,
               u.extension,
               u.fax,
               u.call_center_id,
               u.department_id,
               u.cs_group_id
          FROM users u,
               identity i,
               rel_identity_role rir,
               role r
         WHERE u.user_id = i.user_id
           AND i.identity_id = rir.identity_id
           AND rir.role_id = r.role_id
           AND r.role_name = in_role_name
         ORDER BY u.last_name,
                  u.first_name;

END GET_USERS_BY_ROLE_NAME;

PROCEDURE UPDATE_GROUP_ID
(
IN_UPDATED_BY           IN USERS.UPDATED_BY%TYPE,
IN_IDENTITY_ID          IN IDENTITY.IDENTITY_ID%TYPE,
IN_GROUP_ID             IN USERS.CS_GROUP_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating groups.

Input:
        updated_on                     varchar2
        identity_id                      varchar2
        group_id                       varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        DUPLICATE GROUP
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

        -- update the group information
      UPDATE  USERS
        SET     CS_GROUP_ID = IN_GROUP_ID,
                UPDATED_ON = SYSDATE,
                UPDATED_BY = IN_UPDATED_BY
        WHERE   USER_ID in (SELECT USER_ID FROM AAS.IDENTITY WHERE identity_id = IN_IDENTITY_ID);

        IF ( SQL%ROWCOUNT = 0 ) THEN
                RAISE NO_DATA_FOUND;
        END IF;

COMMIT;
OUT_STATUS := 'Y';

-- user id was not found for update
EXCEPTION WHEN NO_DATA_FOUND THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'USER ID NOT FOUND';
END;

WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_GROUP_ID;


PROCEDURE BULK_USER_TERMINATE
(
IN_IDENTITY_ID           IN IDENTITY.IDENTITY_ID%TYPE,
IN_IDENTITY_EXPIRE_DATE	 IN IDENTITY.IDENTITY_EXPIRE_DATE%TYPE,
IN_UPDATED_BY            IN IDENTITY.UPDATED_BY%TYPE,
OUT_STATUS               OUT VARCHAR2,
OUT_MESSAGE              OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for terminating the user

Input:
        identity_id                    varchar2
        in_identity_expire_date		   date
        updated_by                     varchar2
               
Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        DUPLICATE GROUP
-----------------------------------------------------------------------------*/

BEGIN   
-- procedure body
       
        UPDATE   IDENTITY
           SET	 LOCKED_FLAG = 
                  (CASE
				       WHEN LOCKED_FLAG='N' AND TRUNC(IN_IDENTITY_EXPIRE_DATE) > TRUNC(SYSDATE) THEN 'N'
                       ELSE  'Y'
                  END),
                 IDENTITY_EXPIRE_DATE = IN_IDENTITY_EXPIRE_DATE,
                 UPDATED_ON  = SYSDATE,
                 UPDATED_BY  = IN_UPDATED_BY
         WHERE   IDENTITY_ID = IN_IDENTITY_ID;

        IF ( SQL%ROWCOUNT = 0 ) THEN
                RAISE NO_DATA_FOUND;
        END IF;

COMMIT;
OUT_STATUS := 'Y';

-- user id was not found for update
EXCEPTION WHEN NO_DATA_FOUND THEN
	BEGIN
	        ROLLBACK;
	        OUT_STATUS := 'N';
	        OUT_MESSAGE := 'NO USER FOUND WITH IDENTITY_ID: '||IN_IDENTITY_ID;
	END;

WHEN OTHERS THEN
	BEGIN
	        ROLLBACK;
	        OUT_STATUS := 'N';
	        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
	END;
-- end exception block

END BULK_USER_TERMINATE;

END USERS_PKG;
.
/
