CREATE OR REPLACE
PACKAGE BODY aas.ROLE_PKG AS

PROCEDURE UPDATE_ROLE
(
IN_UPDATED_BY           IN ROLE.UPDATED_BY%TYPE,
IN_CONTEXT_ID           IN ROLE.CONTEXT_ID%TYPE,
IN_ROLE_NAME            IN ROLE.ROLE_NAME%TYPE,
IN_DESCRIPTION          IN ROLE.DESCRIPTION%TYPE,
IO_ROLE_ID              IN OUT ROLE.ROLE_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating roles.
        Context id cannot be updated for an existing role.

Input:
        updated_by                      varchar2
        context_id                      varchar2
        role_name                       varchar2
        description                     varchar2
        role_id                         number

Output:
        role_id                         number
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        DUPLICATE ROLE/CONTEXT
                                                        ROLE NOT FOUND
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

IF IO_ROLE_ID IS NULL THEN

        SELECT  ROLE_ID.NEXTVAL
        INTO    IO_ROLE_ID
        FROM    DUAL;

        -- add new role
        INSERT INTO ROLE
        (
                ROLE_ID,
                ROLE_NAME,
                CONTEXT_ID,
                DESCRIPTION,
                UPDATED_BY
        )
        VALUES
        (
                IO_ROLE_ID,
                IN_ROLE_NAME,
                IN_CONTEXT_ID,
                IN_DESCRIPTION,
                IN_UPDATED_BY
        );

ELSE
        -- update the role information
        UPDATE  ROLE
        SET     ROLE_NAME = IN_ROLE_NAME,
                DESCRIPTION = IN_DESCRIPTION,
                UPDATED_ON = SYSDATE,
                UPDATED_BY = IN_UPDATED_BY
        WHERE   ROLE_ID = IO_ROLE_ID;

        IF ( SQL%ROWCOUNT = 0 ) THEN
                RAISE NO_DATA_FOUND;
        END IF;
END IF;

COMMIT;
OUT_STATUS := 'Y';

-- role is already defined for the context
EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE ROLE/CONTEXT';
END;

-- role was not found for update
WHEN NO_DATA_FOUND THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ROLE NOT FOUND';
END;

WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_ROLE;


PROCEDURE REMOVE_ROLE
(
IN_ROLE_ID              IN ROLE.ROLE_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting role information. Delete
        associated identity role mappings and role acl mappings.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        role_id                         number

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- begin procedure

DELETE FROM REL_IDENTITY_ROLE
WHERE   ROLE_ID = IN_ROLE_ID;

DELETE FROM REL_ROLE_ACL
WHERE   ROLE_ID = IN_ROLE_ID;

DELETE FROM ROLE
WHERE   ROLE_ID = IN_ROLE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_ROLE;


PROCEDURE UPDATE_REL_IDENTITY_ROLE
(
IN_UPDATED_BY           IN REL_IDENTITY_ROLE.UPDATED_BY%TYPE,
IN_IDENTITY_ID          IN REL_IDENTITY_ROLE.IDENTITY_ID%TYPE,
IN_ROLE_ID              IN REL_IDENTITY_ROLE.ROLE_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for relating an identity to a role .
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        identity_id                     varchar2
        role_id                         number

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        DUPLICATE IDENTITY/ROLE
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

INSERT INTO REL_IDENTITY_ROLE
(
        IDENTITY_ID,
        ROLE_ID,
        UPDATED_BY
)
VALUES
(
        IN_IDENTITY_ID,
        IN_ROLE_ID,
        IN_UPDATED_BY
);

OUT_STATUS := 'Y';

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE IDENTITY/ROLE';
END;    -- duplicate exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_REL_IDENTITY_ROLE;


PROCEDURE REMOVE_REL_IDENTITY_ROLE
(
IN_IDENTITY_ID          IN REL_IDENTITY_ROLE.IDENTITY_ID%TYPE,
IN_ROLE_ID              IN REL_IDENTITY_ROLE.ROLE_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the mapping between an
        identity and a role.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        identity_id                     varchar2
        role_id                         number

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

DELETE FROM REL_IDENTITY_ROLE
WHERE   IDENTITY_ID = IN_IDENTITY_ID
AND     ROLE_ID = IN_ROLE_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_REL_IDENTITY_ROLE;


PROCEDURE UPDATE_REL_ROLE_ACL
(
IN_UPDATED_BY           IN REL_ROLE_ACL.UPDATED_BY%TYPE,
IN_ROLE_ID              IN REL_ROLE_ACL.ROLE_ID%TYPE,
IN_ACL_NAME             IN ACL.ACL_NAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for relating a role to the given acl number.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        role_id                         number
        acl_name                        varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        DIFFERENT ROLE/ACL CONTEXT
                                                        DUPLICATE ROLE/ACL
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- check if the role and acl are in the same context
        SELECT  DISTINCT
                'Y'
        INTO    OUT_STATUS
        FROM    ROLE R
        JOIN    REL_ACL_RP ARP
        ON      R.CONTEXT_ID = ARP.CONTEXT_ID
        WHERE   R.ROLE_ID = IN_ROLE_ID
        AND     ARP.ACL_NAME = IN_ACL_NAME;

        INSERT INTO REL_ROLE_ACL
        (
                ROLE_ID,
                ACL_NAME,
                UPDATED_BY
        )
        VALUES
        (
                IN_ROLE_ID,
                IN_ACL_NAME,
                IN_UPDATED_BY
        );

        EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
                OUT_STATUS := 'N';
                OUT_MESSAGE := 'DIFFERENT ROLE/ACL CONTEXT';
        END;
END;    -- end context check

EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE ROLE/ACL';
END;    -- duplicate exception block

WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_REL_ROLE_ACL;


PROCEDURE REMOVE_REL_ROLE_ACL
(
IN_ROLE_ID              IN REL_ROLE_ACL.ROLE_ID%TYPE,
IN_ACL_NAME             IN ACL.ACL_NAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the mapping between a role
        and an acl number.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        role_id                         number
        acl_name                        varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

DELETE FROM REL_ROLE_ACL
WHERE   ROLE_ID = IN_ROLE_ID
AND     ACL_NAME = IN_ACL_NAME;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_REL_ROLE_ACL;


PROCEDURE VIEW_ROLE
(
IN_ROLE_ID              IN ROLE.ROLE_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given role's
        information.

Input:
        role_id                         number

Output:
        cursor result set containing role information
        role_id                         number
        role_name                       varchar2
        context_id                      varchar2
        description                     varchar2
        identity_id                     varchar2
        acl_name                        varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  R.ROLE_ID,
                R.ROLE_NAME,
                R.CONTEXT_ID,
                R.DESCRIPTION
        FROM    ROLE R
        WHERE   R.ROLE_ID = IN_ROLE_ID;

END VIEW_ROLE;


PROCEDURE VIEW_ROLE_ACLS
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_ROLE_ID              IN ROLE.ROLE_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all acl numbers and
        if it is associated to the given role in the given context.

Input:
        role_id                         number

Output:
        cursor result set containing role information
        acl_name                        varchar2
        role_id                         number

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT
                ARP.ACL_NAME,
                RA.ROLE_ID
        FROM    REL_ACL_RP ARP
        LEFT OUTER JOIN REL_ROLE_ACL RA
        ON      RA.ACL_NAME = ARP.ACL_NAME
        AND     RA.ROLE_ID = IN_ROLE_ID
        WHERE   ARP.CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_ROLE_ACLS;


PROCEDURE VIEW_ROLES
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all roles'
        information.

Input:
        none

Output:
        cursor result set containing role information
        role_id                         number
        role_name                       varchar2
        context_id                      varchar2
        description                     varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ROLE_ID,
                ROLE_NAME,
                CONTEXT_ID,
                DESCRIPTION
        FROM    ROLE;

END VIEW_ROLES;

PROCEDURE VIEW_ROLES_BY_NAME
(
IN_CONTEXT_ID           IN ROLE.CONTEXT_ID%TYPE,
IN_ROLE_NAME            IN ROLE.ROLE_NAME%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning roles
        information the associated identities and acls for the given
        role name in the given context.

Input:
        context_id                      varchar2
        role_name                       varchar2

Output:
        cursor result set containing role, identity, and acl information
        role_id                         number
        role_name                       varchar2
        context_id                      varchar2
        description                     varchar2
        identity_id                     varchar2
        acl_name                        varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT
                R.ROLE_ID,
                R.ROLE_NAME,
                R.CONTEXT_ID,
                R.DESCRIPTION,
                IR.IDENTITY_ID,
                RA.ACL_NAME
        FROM    ROLE R
        JOIN    REL_IDENTITY_ROLE IR
        ON      R.ROLE_ID = IR.ROLE_ID
        JOIN    REL_ROLE_ACL RA
        ON      R.ROLE_ID = RA.ROLE_ID
        WHERE   R.ROLE_NAME LIKE IN_ROLE_NAME || '%';

END VIEW_ROLES_BY_NAME;

PROCEDURE VIEW_ROLES_BY_CONTEXT
(
IN_CONTEXT_ID           IN ROLE.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning roles
        information for the given context.

Input:
        context_id                      varchar2

Output:
        cursor result set containing role, identity, and acl information
        role_id                         number
        role_name                       varchar2
        context_id                      varchar2
        description                     varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  R.ROLE_ID,
                R.ROLE_NAME,
                R.CONTEXT_ID,
                R.DESCRIPTION
        FROM    ROLE R
        WHERE   R.CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_ROLES_BY_CONTEXT;

END ROLE_PKG;
.
/
