create or replace FUNCTION aas.GET_PERMISSION (
  IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
  IN_SESSION_ID           IN SESSIONS.SESSION_ID%TYPE,
  IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
  IN_PERMISSION_ID        IN REL_ACL_RP.PERMISSION_ID%TYPE
)
 RETURN VARCHAR2 is
   v_return varchar2(5) := 'N';

begin
        SELECT  DISTINCT
                'Y'
        INTO    v_return
        FROM    SESSIONS S
        JOIN    REL_IDENTITY_ROLE IR
        ON      IR.IDENTITY_ID = S.IDENTITY_ID
        JOIN    REL_ROLE_ACL RA
        ON      RA.ROLE_ID = IR.ROLE_ID
        JOIN    REL_ACL_RP ARP
        ON      ARP.ACL_NAME = RA.ACL_NAME
        WHERE   S.SESSION_ID = IN_SESSION_ID
        AND     ARP.RESOURCE_ID = IN_RESOURCE_ID
        AND     ARP.CONTEXT_ID = IN_CONTEXT_ID
        AND     ARP.PERMISSION_ID = IN_PERMISSION_ID;

        -- set return value to 'N' if session/permissions are not found
--        EXCEPTION WHEN NO_DATA_FOUND THEN
--                v_return:= 'N';

   return v_return;

end get_permission;
/
