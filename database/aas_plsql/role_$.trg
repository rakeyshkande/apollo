CREATE OR REPLACE
TRIGGER aas.role_$
AFTER INSERT OR UPDATE OR DELETE ON aas.role
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.role$ (
      role_id,
      role_name,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.role_id,
      :NEW.role_name,
      :NEW.context_id,
      :NEW.description,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.role$ (
      role_id,
      role_name,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.role_id,
      :OLD.role_name,
      :OLD.context_id,
      :OLD.description,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.role$ (
      role_id,
      role_name,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.role_id,
      :NEW.role_name,
      :NEW.context_id,
      :NEW.description,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.role$ (
      role_id,
      role_name,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.role_id,
      :OLD.role_name,
      :OLD.context_id,
      :OLD.description,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
