CREATE OR REPLACE
TRIGGER aas.identity_$
AFTER INSERT OR UPDATE OR DELETE ON aas.identity
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.identity$ (
      identity_id,
      credentials,
      user_id,
      description,
      identity_expire_date,
      credentials_expire_date,
      attempt_try_number,
      locked_flag,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.identity_id,
      :NEW.credentials,
      :NEW.user_id,
      :NEW.description,
      :NEW.identity_expire_date,
      :NEW.credentials_expire_date,
      :NEW.attempt_try_number,
      :NEW.locked_flag,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.identity$ (
      identity_id,
      credentials,
      user_id,
      description,
      identity_expire_date,
      credentials_expire_date,
      attempt_try_number,
      locked_flag,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.identity_id,
      :OLD.credentials,
      :OLD.user_id,
      :OLD.description,
      :OLD.identity_expire_date,
      :OLD.credentials_expire_date,
      :OLD.attempt_try_number,
      :OLD.locked_flag,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.identity$ (
      identity_id,
      credentials,
      user_id,
      description,
      identity_expire_date,
      credentials_expire_date,
      attempt_try_number,
      locked_flag,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.identity_id,
      :NEW.credentials,
      :NEW.user_id,
      :NEW.description,
      :NEW.identity_expire_date,
      :NEW.credentials_expire_date,
      :NEW.attempt_try_number,
      :NEW.locked_flag,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.identity$ (
      identity_id,
      credentials,
      user_id,
      description,
      identity_expire_date,
      credentials_expire_date,
      attempt_try_number,
      locked_flag,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.identity_id,
      :OLD.credentials,
      :OLD.user_id,
      :OLD.description,
      :OLD.identity_expire_date,
      :OLD.credentials_expire_date,
      :OLD.attempt_try_number,
      :OLD.locked_flag,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
