CREATE OR REPLACE
TRIGGER aas.rel_role_acl_$
AFTER INSERT OR UPDATE OR DELETE ON aas.rel_role_acl
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.rel_role_acl$ (
      role_id,
      acl_name,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.role_id,
      :NEW.acl_name,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.rel_role_acl$ (
      role_id,
      acl_name,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.role_id,
      :OLD.acl_name,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.rel_role_acl$ (
      role_id,
      acl_name,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.role_id,
      :NEW.acl_name,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.rel_role_acl$ (
      role_id,
      acl_name,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.role_id,
      :OLD.acl_name,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
