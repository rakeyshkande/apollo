create or replace PROCEDURE          AAS.SP_CREATE_USER_SAILPOINT
(
IN_UPDATED_BY           IN USERS.UPDATED_BY%TYPE,
IN_LAST_NAME            IN USERS.LAST_NAME%TYPE,
IN_FIRST_NAME           IN USERS.FIRST_NAME%TYPE,

IN_PHONE                IN USERS.PHONE%TYPE,
IN_EMAIL                IN USERS.EMAIL%TYPE,
IN_EXTENSION            IN USERS.EXTENSION%TYPE,

IN_CALL_CENTER_ID       IN USERS.CALL_CENTER_ID%TYPE,
IN_DEPARTMENT_ID        IN USERS.DEPARTMENT_ID%TYPE,
IN_CS_GROUP_ID          IN USERS.CS_GROUP_ID%TYPE,

IN_EMPLOYEE_ID          IN USERS.EMPLOYEE_ID%TYPE,
IN_SUPERVISOR_ID        IN USERS.SUPERVISOR_ID%TYPE,
IO_USER_ID              IN OUT USERS.USER_ID%TYPE,

IN_IDENTITY_EXPIRY_OFFSET       IN NUMBER,
IN_LOCKED_FLAG                  IN IDENTITY.LOCKED_FLAG%TYPE,
IN_IDENTITY_ID          IN REL_IDENTITY_ROLE.IDENTITY_ID%TYPE,

IN_ROLE_IDS             IN VARCHAR2,

IN_CREDENTIALS			    IN VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating user
        information.  If the user expiration date is changed, the
        associated identities' expiration dates will be updated as well.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        last_name                       varchar2
        first_name                      varchar2  
        phone                           varchar2
        email                           varchar2
        extension                       varchar2
        call_center_id                  varchar2
        department_id                   varchar2
        cs_group_id                     varchar2
        employee_id						          varchar2
        supervisor_id					          number
        user_id                         number
        
        identity_expiry_offset          number        offset days
        locked_flag                     varchar2      Y or N
        
        identity_id                     varchar2
        role_ids                        varchar2
        

Output:
        credentials                     varchar2
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

V_EXPIRE_DATE           IDENTITY.IDENTITY_EXPIRE_DATE%TYPE;
V_CREDENTIALS           IDENTITY.CREDENTIALS%TYPE;

cursor ROLE_IDS is
    WITH DATA AS
    ( SELECT IN_ROLE_IDS id FROM dual
    )
    SELECT trim(regexp_substr(id, '[^,]+', 1, LEVEL)) id
    FROM DATA
    CONNECT BY instr(id, ',', 1, LEVEL - 1) > 0;                 

v_history_check char (1) := 'N';

BEGIN   -- procedure body

OUT_MESSAGE := '';
-- Creare User
AAS.USERS_PKG.UPDATE_USER
	(
		IN_UPDATED_BY,
		IN_LAST_NAME,
		IN_FIRST_NAME,
		null,
		null,
		null,
		null,
		null,
		IN_PHONE,
		null,
		IN_EMAIL,
		IN_EXTENSION,
		null,
		IN_CALL_CENTER_ID,
		IN_DEPARTMENT_ID,
		IN_CS_GROUP_ID,
		IN_EMPLOYEE_ID,
		IN_SUPERVISOR_ID,
		IO_USER_ID,
		OUT_STATUS,
		OUT_MESSAGE
	);
  DBMS_OUTPUT.PUT_LINE('User ID: ' || IO_USER_ID);
--Create Identity
IF OUT_STATUS = 'Y' THEN
  
  V_EXPIRE_DATE := sysdate+IN_IDENTITY_EXPIRY_OFFSET;

  INSERT INTO IDENTITY
    (
            IDENTITY_ID,
            CREDENTIALS,
            USER_ID,
            DESCRIPTION,
            IDENTITY_EXPIRE_DATE,
            CREDENTIALS_EXPIRE_DATE,
            LOCKED_FLAG,
            UPDATED_BY
    )
    VALUES
    (
            IN_IDENTITY_ID,
            GLOBAL.MD5_HASH (IN_CREDENTIALS),
            IO_USER_ID,
            NULL,
            V_EXPIRE_DATE,
            SYSDATE,                                  -- credential expire date set to expired, so that Reset pop up will be opened
            IN_LOCKED_FLAG,
            IN_UPDATED_BY
    );

   DBMS_OUTPUT.PUT_LINE('Temp Credentials: ' || IN_CREDENTIALS);

ELSE
     RAISE_APPLICATION_ERROR(-20001,'Exception while AAS.USERS_PKG.UPDATE_USER in AAS.USERS_PKG.CREATE_USER_SP',TRUE);
END IF;

-- Update Identity-Roles
IF OUT_STATUS = 'Y' THEN

  FOR ROLE_ID IN ROLE_IDS
  LOOP
    DBMS_OUTPUT.PUT_LINE('Role ID: ' || ROLE_ID.id);
    AAS.ROLE_PKG.UPDATE_REL_IDENTITY_ROLE
      (
        IN_UPDATED_BY,
        IN_IDENTITY_ID,
        TO_NUMBER(ROLE_ID.id, '999999999'),
        OUT_STATUS,
        OUT_MESSAGE
      );
  END LOOP;

COMMIT;

ELSE
  RAISE_APPLICATION_ERROR(-20001,'Exception while AAS.USERS_PKG.UPDATE_IDENTITY_PCI in AAS.USERS_PKG.CREATE_USER_SP',TRUE);
END IF;

IF OUT_STATUS = 'N' THEN
  RAISE_APPLICATION_ERROR(-20001,'Exception while AAS.ROLE_PKG.UPDATE_REL_IDENTITY_ROLE in AAS.USERS_PKG.CREATE_USER_SP',TRUE);
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := OUT_MESSAGE || ' ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        DBMS_OUTPUT.PUT_LINE('OUT_MESSAGE: ' || OUT_MESSAGE);
END;    -- end exception block

END SP_CREATE_USER_SAILPOINT;
/