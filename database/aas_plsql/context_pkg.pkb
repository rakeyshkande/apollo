CREATE OR REPLACE
PACKAGE BODY aas.CONTEXT_PKG AS

FUNCTION GET_CONFIG_PARAMETER
(
IN_CONTEXT_ID           IN CONTEXT_CONFIG.CONTEXT_ID%TYPE,
IN_NAME                 IN CONTEXT_CONFIG.NAME%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the config value for the
        given context id and config name.

Input:
        context_id                      varchar2
        name                            varchar2

Output:
        config value                    varchar2
-----------------------------------------------------------------------------*/

OUT_VALUE               CONTEXT_CONFIG.VALUE%TYPE;      -- config value to be returned

BEGIN

-- get the config value for the given context and config name
SELECT  VALUE
INTO    OUT_VALUE
FROM    CONTEXT_CONFIG
WHERE   CONTEXT_ID = IN_CONTEXT_ID
AND     UPPER (NAME) = UPPER (IN_NAME);

RETURN OUT_VALUE;

-- return null if no records found
EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_CONFIG_PARAMETER;


PROCEDURE UPDATE_CONTEXT
(
IN_UPDATED_BY           IN CONTEXT.UPDATED_BY%TYPE,
IN_CONTEXT_ID           IN CONTEXT.CONTEXT_ID%TYPE,
IN_DESCRIPTION          IN CONTEXT.DESCRIPTION%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating context
        information.

Input:
        updated_by                      varchar2
        context_id                      varchar2
        description                     varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- insert/update context information

        INSERT INTO CONTEXT
        (
                CONTEXT_ID,
                DESCRIPTION,
                UPDATED_BY
        )
        VALUES
        (
                IN_CONTEXT_ID,
                IN_DESCRIPTION,
                IN_UPDATED_BY
        );

        -- update context information if context exists
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        BEGIN
                UPDATE  CONTEXT
                SET     DESCRIPTION = IN_DESCRIPTION,
                        UPDATED_ON = SYSDATE,
                        UPDATED_BY = IN_UPDATED_BY
                WHERE   CONTEXT_ID = IN_CONTEXT_ID;

        END;    -- end duplicate exception block

END;    -- end insert/update block

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END;    -- end exception block

END UPDATE_CONTEXT;


PROCEDURE REMOVE_CONTEXT
(
IN_CONTEXT_ID           IN CONTEXT.CONTEXT_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing context information only
        if there are no resources or roles associated to it.  Any
        associated sessions or config records will be deleted.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        context_id                      varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        CONTEXT_USED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- check if ther are any recources or roles associated to the context

        SELECT  'N'
        INTO    OUT_STATUS
        FROM    RESOURCES
        WHERE   CONTEXT_ID = IN_CONTEXT_ID
        UNION
        SELECT  'N'
        FROM    ROLE
        WHERE   CONTEXT_ID = IN_CONTEXT_ID;

        OUT_MESSAGE := 'CONTEXT USED';

        -- delete any session or config records for the context
        -- and the context
        EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
                DELETE FROM SESSIONS
                WHERE   CONTEXT_ID = IN_CONTEXT_ID;

                DELETE FROM CONTEXT_CONFIG
                WHERE   CONTEXT_ID = IN_CONTEXT_ID;

                DELETE FROM CONTEXT
                WHERE   CONTEXT_ID = IN_CONTEXT_ID;

                OUT_STATUS := 'Y';

        END;    -- end delete process

END;    -- end reference integrity check block

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_CONTEXT;


PROCEDURE UPDATE_CONTEXT_CONFIG
(
IN_UPDATED_BY           IN CONTEXT_CONFIG.UPDATED_BY%TYPE,
IN_CONTEXT_ID           IN CONTEXT_CONFIG.CONTEXT_ID%TYPE,
IN_NAME                 IN CONTEXT_CONFIG.NAME%TYPE,
IN_VALUE                IN CONTEXT_CONFIG.VALUE%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating context
        config information.

Input:
        updated_by                      varchar2
        context_id                      varchar2
        name                            varchar2
        value                           varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- insert/update context information

        INSERT INTO CONTEXT_CONFIG
        (
                CONTEXT_ID,
                NAME,
                VALUE,
                UPDATED_BY
        )
        VALUES
        (
                IN_CONTEXT_ID,
                IN_NAME,
                IN_VALUE,
                IN_UPDATED_BY
        );

        -- update context config information if context config exists
        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
        BEGIN
                UPDATE  CONTEXT_CONFIG
                SET     VALUE = IN_VALUE,
                        UPDATED_ON = SYSDATE,
                        UPDATED_BY = IN_UPDATED_BY
                WHERE   CONTEXT_ID = IN_CONTEXT_ID
                AND     NAME = IN_NAME;

        END;    -- end duplicate exception block

END;    -- end insert/update block

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END;    -- end exception block

END UPDATE_CONTEXT_CONFIG;


PROCEDURE REMOVE_CONTEXT_CONFIG
(
IN_CONTEXT_ID           IN CONTEXT_CONFIG.CONTEXT_ID%TYPE,
IN_NAME                 IN CONTEXT_CONFIG.NAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for removing context config information.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        context_id                      varchar2
        name                            varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

DELETE FROM CONTEXT_CONFIG
WHERE   CONTEXT_ID = IN_CONTEXT_ID
AND     NAME = IN_NAME;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_CONTEXT_CONFIG;


PROCEDURE VIEW_CONTEXT
(
IN_CONTEXT_ID           IN CONTEXT.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given context information
        and all associated configurations.

Input:
        context id                      varchar2

Output:
        cursor result set containing context and config informtion
        context_id                      varchar2
        description                     varhcar2
        config_name                     varchar2
        config_value                    varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  C.CONTEXT_ID,
                C.DESCRIPTION,
                NVL (CC.NAME, ' ') AS CONFIG_NAME,
                NVL (CC.VALUE, ' ') AS CONFIG_VALUE
        FROM    CONTEXT C
        LEFT OUTER JOIN CONTEXT_CONFIG CC
        ON      C.CONTEXT_ID = CC.CONTEXT_ID
        WHERE   C.CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_CONTEXT;



PROCEDURE VIEW_CONTEXT_CONFIG
(
IN_CONTEXT_ID           IN CONTEXT_CONFIG.CONTEXT_ID%TYPE,
IN_NAME                 IN CONTEXT_CONFIG.NAME%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given context config
        information.

Input:
        context_id                      varchar2
        name                            varchar2

Output:
        cursor result set containing config information
        context_id                      varchar2
        name                            varchar2
        value                           varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  CONTEXT_ID,
                NAME,
                VALUE
        FROM    CONTEXT_CONFIG
        WHERE   CONTEXT_ID = IN_CONTEXT_ID
        AND     NAME = IN_NAME;

END VIEW_CONTEXT_CONFIG;


PROCEDURE VIEW_CONTEXTS
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all contexts' information.

Input:
        none

Output:
        cursor result set containing context information
        context_id                      varchar2
        description                     varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  CONTEXT_ID,
                DESCRIPTION
        FROM    CONTEXT;

END VIEW_CONTEXTS;


FUNCTION CONTEXT_EXISTS
(
IN_CONTEXT_ID         IN CONTEXT.CONTEXT_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an context exists.

Input:
        context_id                   varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    CONTEXT
WHERE   CONTEXT_ID = IN_CONTEXT_ID;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END CONTEXT_EXISTS;

FUNCTION CONTEXT_CONFIG_EXISTS
(
IN_CONTEXT_ID           IN CONTEXT_CONFIG.CONTEXT_ID%TYPE,
IN_NAME                 IN CONTEXT_CONFIG.NAME%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an context config exists.

Input:
        context_id                      varchar2
        name                            varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    CONTEXT_CONFIG
WHERE   CONTEXT_ID = IN_CONTEXT_ID
AND     NAME = IN_NAME;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END CONTEXT_CONFIG_EXISTS;

END CONTEXT_PKG;
.
/
