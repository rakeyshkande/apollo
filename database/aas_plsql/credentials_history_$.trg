CREATE OR REPLACE
TRIGGER aas.credentials_history_$
AFTER INSERT OR UPDATE OR DELETE ON aas.credentials_history
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.credentials_history$ (
      identity_id,
      credentials,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.identity_id,
      :NEW.credentials,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.credentials_history$ (
      identity_id,
      credentials,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.identity_id,
      :OLD.credentials,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.credentials_history$ (
      identity_id,
      credentials,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.identity_id,
      :NEW.credentials,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.credentials_history$ (
      identity_id,
      credentials,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.identity_id,
      :OLD.credentials,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
