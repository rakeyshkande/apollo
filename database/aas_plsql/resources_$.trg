CREATE OR REPLACE
TRIGGER aas.resources_$
AFTER INSERT OR UPDATE OR DELETE ON aas.resources
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.resources$ (
      resource_id,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.resource_id,
      :NEW.context_id,
      :NEW.description,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.resources$ (
      resource_id,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.resource_id,
      :OLD.context_id,
      :OLD.description,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.resources$ (
      resource_id,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.resource_id,
      :NEW.context_id,
      :NEW.description,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.resources$ (
      resource_id,
      context_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.resource_id,
      :OLD.context_id,
      :OLD.description,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
