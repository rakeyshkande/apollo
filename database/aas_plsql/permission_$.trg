CREATE OR REPLACE
TRIGGER aas.permission_$
AFTER INSERT OR UPDATE OR DELETE ON aas.permission
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.permission$ (
      permission_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.permission_id,
      :NEW.description,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.permission$ (
      permission_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.permission_id,
      :OLD.description,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.permission$ (
      permission_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.permission_id,
      :NEW.description,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.permission$ (
      permission_id,
      description,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.permission_id,
      :OLD.description,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
