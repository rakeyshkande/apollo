CREATE OR REPLACE
TRIGGER aas.rel_acl_rp_$
AFTER INSERT OR UPDATE OR DELETE ON aas.rel_acl_rp
FOR EACH ROW BEGIN

   IF INSERTING THEN

      INSERT INTO aas.rel_acl_rp$ (
      acl_name,
      resource_id,
      context_id,
      permission_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.acl_name,
      :NEW.resource_id,
      :NEW.context_id,
      :NEW.permission_id,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'INS',SYSDATE);

   ELSIF UPDATING  THEN

      INSERT INTO aas.rel_acl_rp$ (
      acl_name,
      resource_id,
      context_id,
      permission_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.acl_name,
      :OLD.resource_id,
      :OLD.context_id,
      :OLD.permission_id,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'UPD_OLD',SYSDATE);

      INSERT INTO aas.rel_acl_rp$ (
      acl_name,
      resource_id,
      context_id,
      permission_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :NEW.acl_name,
      :NEW.resource_id,
      :NEW.context_id,
      :NEW.permission_id,
      :NEW.created_on,
      :NEW.updated_on,
      :NEW.updated_by,
      'UPD_NEW',SYSDATE);

   ELSIF DELETING  THEN

      INSERT INTO aas.rel_acl_rp$ (
      acl_name,
      resource_id,
      context_id,
      permission_id,
      created_on,
      updated_on,
      updated_by,
      operation$, timestamp$
      ) VALUES (
      :OLD.acl_name,
      :OLD.resource_id,
      :OLD.context_id,
      :OLD.permission_id,
      :OLD.created_on,
      :OLD.updated_on,
      :OLD.updated_by,
      'DEL',SYSDATE);

   END IF;

END;
/
