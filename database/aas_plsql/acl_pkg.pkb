CREATE OR REPLACE
PACKAGE BODY AAS.ACL_PKG AS

FUNCTION ACL_NAME_EXISTS
(
IN_ACL_NAME             IN ACL.ACL_NAME%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an acl name exists.

Input:
        acl_name                        varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    ACL
WHERE   ACL_NAME = IN_ACL_NAME;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END ACL_NAME_EXISTS;


FUNCTION RESOURCE_EXISTS
(
IN_CONTEXT_ID           IN RESOURCES.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN RESOURCES.RESOURCE_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an resource exists in
        the given context.

Input:
        context_id                      varchar
        resource_id                     varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    RESOURCES
WHERE   RESOURCE_ID = IN_RESOURCE_ID
AND     CONTEXT_ID = IN_CONTEXT_ID;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END RESOURCE_EXISTS;


FUNCTION PERMISSION_EXISTS
(
IN_PERMISSION_ID         IN PERMISSION.PERMISSION_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if an permission exists.

Input:
        permission_id                   varchar2

Output:
        varchar2 (Y or N)
-----------------------------------------------------------------------------*/

V_COUNT                 NUMBER;
V_EXISTS                CHAR (1) := 'N';
BEGIN

SELECT  COUNT (1)
INTO    V_COUNT
FROM    PERMISSION
WHERE   PERMISSION_ID = IN_PERMISSION_ID;

IF V_COUNT > 0 THEN
        V_EXISTS := 'Y';
END IF;

RETURN V_EXISTS;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END PERMISSION_EXISTS;


FUNCTION GET_USED_IN_ACL
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
IN_PERMISSION_ID        IN REL_ACL_RP.PERMISSION_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning the acls that have the
        given resource or permissions.  The acl numbers are returned in
        a comma delimited string.

Input:
        context_id                      varchar2
        resource_id                     varchar2
        permission_id                   char

Output:
        string containing acl numbers delimited by commas
-----------------------------------------------------------------------------*/

OUT_STRING              VARCHAR2 (2000);

CURSOR ACL_CUR IS
SELECT  DISTINCT
        ARP.ACL_NAME
FROM    REL_ACL_RP ARP
WHERE   ((ARP.RESOURCE_ID = IN_RESOURCE_ID AND ARP.CONTEXT_ID = IN_CONTEXT_ID)
        OR (IN_RESOURCE_ID IS NULL AND IN_CONTEXT_ID IS NULL))
AND     (ARP.PERMISSION_ID = IN_PERMISSION_ID
        OR IN_PERMISSION_ID IS NULL);

ACL_ROW                 ACL_CUR%ROWTYPE;

BEGIN

FOR ACL_ROW IN ACL_CUR
LOOP
        IF OUT_STRING IS NOT NULL THEN
                OUT_STRING := OUT_STRING || ', ' || ACL_ROW.ACL_NAME;
        ELSE
                OUT_STRING := ACL_ROW.ACL_NAME;
        END IF;
END LOOP;

RETURN OUT_STRING;

END GET_USED_IN_ACL;


PROCEDURE UPDATE_ACL
(
IN_UPDATED_BY           IN REL_ACL_RP.UPDATED_BY%TYPE,
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
IN_PERMISSION_ID        IN REL_ACL_RP.PERMISSION_ID%TYPE,
IN_ACL_NAME             IN REL_ACL_RP.ACL_NAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting acl, resource, permission
        relationships.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        updated_by                      varchar2
        context_id                      varchar2
        resource_id                     varchar2
        permission_id                   char
        acl_name                        varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        DUPLICATE ACL/RESOURCE/PERMISSION IN CONTEXT
                                                        ACL NOT FOUND
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

IF ACL_NAME_EXISTS (IN_ACL_NAME) = 'N' THEN
        INSERT INTO ACL
        (
                ACL_NAME,
                UPDATED_BY
        )
        VALUES
        (
                IN_ACL_NAME,
                IN_UPDATED_BY
        );
END IF;

-- add new acl, resource, permission relationship
INSERT INTO REL_ACL_RP
(
        RESOURCE_ID,
        CONTEXT_ID,
        PERMISSION_ID,
        ACL_NAME,
        UPDATED_BY
)
VALUES
(
        IN_RESOURCE_ID,
        IN_CONTEXT_ID,
        IN_PERMISSION_ID,
        IN_ACL_NAME,
        IN_UPDATED_BY
);

OUT_STATUS := 'Y';

-- resource and permission already defined in the context
EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'DUPLICATE ACL/RESOURCE/PERMISSION IN CONTEXT';
END;

WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_ACL;


PROCEDURE REMOVE_ACL_RP
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
IN_PERMISSION_ID        IN REL_ACL_RP.PERMISSION_ID%TYPE,
IN_ACL_NAME             IN REL_ACL_RP.ACL_NAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting acl information for the
        given acl, resource, permission.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        context_id                      varchar2
        resource_id                     varchar2
        permission_id                   varchar2
        acl name                        varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

DELETE FROM REL_ACL_RP
WHERE   ACL_NAME = IN_ACL_NAME
AND     RESOURCE_ID = IN_RESOURCE_ID
AND     CONTEXT_ID = IN_CONTEXT_ID
AND     PERMISSION_ID = IN_PERMISSION_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_ACL_RP;


PROCEDURE REMOVE_ACL_NAME
(
IN_ACL_NAME             IN ACL.ACL_NAME%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting acl information for the
        given acl number.  Delete associated role acl mappings.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        acl name                        varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

DELETE FROM REL_ROLE_ACL
WHERE   ACL_NAME = IN_ACL_NAME;

DELETE FROM REL_ACL_RP
WHERE   ACL_NAME = IN_ACL_NAME;

DELETE FROM ACL
WHERE   ACL_NAME = IN_ACL_NAME;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_ACL_NAME;


PROCEDURE UPDATE_RESOURCE
(
IN_UPDATED_BY           IN RESOURCES.UPDATED_BY%TYPE,
IN_CONTEXT_ID           IN RESOURCES.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN RESOURCES.RESOURCE_ID%TYPE,
IN_DESCRIPTION          IN RESOURCES.DESCRIPTION%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating resources.
        Context id cannot be updated for an existing resource.

Input:
        updated_by                      varchar2
        context_id                      varchar2
        resource_id                     varchar2
        description                     varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- insert/update resource information

        INSERT INTO RESOURCES
        (
                RESOURCE_ID,
                CONTEXT_ID,
                DESCRIPTION,
                UPDATED_BY
        )
        VALUES
        (
                IN_RESOURCE_ID,
                IN_CONTEXT_ID,
                IN_DESCRIPTION,
                IN_UPDATED_BY
        );


        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
                -- update the resource information
                UPDATE  RESOURCES
                SET     DESCRIPTION = IN_DESCRIPTION,
                        UPDATED_ON = SYSDATE,
                        UPDATED_BY = IN_UPDATED_BY
                WHERE   RESOURCE_ID = IN_RESOURCE_ID
                AND     CONTEXT_ID = IN_CONTEXT_ID;

END;    -- end insert/update block

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_RESOURCE;


PROCEDURE REMOVE_RESOURCE
(
IN_CONTEXT_ID           IN RESOURCES.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN RESOURCES.RESOURCE_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting resource information only
        if there are no acls associated to it.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        context_id                      varchar2
        resource_id                     varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        RESOURCE USED
-----------------------------------------------------------------------------*/

V_MESSAGE               VARCHAR2 (2000);

BEGIN   -- procedure body

V_MESSAGE := GET_USED_IN_ACL (IN_CONTEXT_ID, IN_RESOURCE_ID, NULL);

IF V_MESSAGE IS NULL THEN
        DELETE FROM RESOURCES
        WHERE   RESOURCE_ID = IN_RESOURCE_ID
        AND     CONTEXT_ID = IN_CONTEXT_ID;

        OUT_STATUS := 'Y';
ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'RESOURCE USED IN ACLS: ' || V_MESSAGE;
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_RESOURCE;


PROCEDURE UPDATE_PERMISSION
(
IN_UPDATED_BY           IN PERMISSION.UPDATED_BY%TYPE,
IN_PERMISSION_ID        IN PERMISSION.PERMISSION_ID%TYPE,
IN_DESCRIPTION          IN PERMISSION.DESCRIPTION%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting and updating permissions.

Input:
        updated_by                      varchar2
        permission_id                   char
        description                     varchar2

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
-----------------------------------------------------------------------------*/

BEGIN   -- procedure body

BEGIN   -- insert/update resource information

        INSERT INTO PERMISSION
        (
                PERMISSION_ID,
                DESCRIPTION,
                UPDATED_BY
        )
        VALUES
        (
                IN_PERMISSION_ID,
                IN_DESCRIPTION,
                IN_UPDATED_BY
        );


        EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
                -- update the permission information
                UPDATE  PERMISSION
                SET     DESCRIPTION = IN_DESCRIPTION,
                        UPDATED_ON = SYSDATE,
                        UPDATED_BY = IN_UPDATED_BY
                WHERE   PERMISSION_ID = IN_PERMISSION_ID;

END;    -- end insert/update block

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
BEGIN
        ROLLBACK;
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END UPDATE_PERMISSION;


PROCEDURE REMOVE_PERMISSION
(
IN_PERMISSION_ID        IN PERMISSION.PERMISSION_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting permission information only
        if there are no acls associated to it.
        ** The commit/rollback of the transaction should be handled within the application.

Input:
        permission_id                   char

Output:
        status                          varchar2        Y or N
        error message                   varchar2        ERROR OCCURRED
                                                        PERMISSION USED
-----------------------------------------------------------------------------*/

V_MESSAGE               VARCHAR2 (2000);

BEGIN   -- procedure body

V_MESSAGE := GET_USED_IN_ACL (NULL, NULL, IN_PERMISSION_ID);

IF V_MESSAGE IS NULL THEN
        DELETE FROM PERMISSION
        WHERE   PERMISSION_ID = IN_PERMISSION_ID;

        OUT_STATUS := 'Y';
ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'PERMISSION USED IN ACLS: ' || V_MESSAGE;
END IF;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END REMOVE_PERMISSION;





PROCEDURE VIEW_ACL
(
IN_ACL_NAME             IN ACL.ACL_NAME%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given acl's
        information and any roles that are associated to the acl.

Input:
        acl_name                      number

Output:
        cursor result set containing acl and role information
        context_id                      varchar2
        resource_id                     varchar2
        permission_id                   char
        acl_name                        varchar2
        role_id                         varchar2
        role_name                       varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ARP.RESOURCE_ID,
                ARP.CONTEXT_ID,
                ARP.PERMISSION_ID,
                ARP.ACL_NAME,
                NVL (TO_CHAR (R.ROLE_ID), ' ') AS ROLE_ID,
                NVL (R.ROLE_NAME, ' ') AS ROLE_NAME
        FROM    REL_ACL_RP ARP
        LEFT OUTER JOIN REL_ROLE_ACL RA
        ON      ARP.ACL_NAME = RA.ACL_NAME
        LEFT OUTER JOIN ROLE R
        ON      R.ROLE_ID = RA.ROLE_ID
        WHERE   ARP.ACL_NAME = IN_ACL_NAME;

END VIEW_ACL;


PROCEDURE VIEW_ACLS
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all acls'
        information within the context id if specified.

Input:
        context id                      varchar (optional)

Output:
        cursor result set containing acl information
        context_id                      varchar2
        resource_id                     varchar2
        permission_id                   char
        acl_name                        varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  RESOURCE_ID,
                CONTEXT_ID,
                PERMISSION_ID,
                ACL_NAME
        FROM    REL_ACL_RP
        WHERE   (CONTEXT_ID = IN_CONTEXT_ID OR IN_CONTEXT_ID IS NULL);

END VIEW_ACLS;


PROCEDURE VIEW_CONTEXT_ACL_NAMES
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all acls' within the
        context id.

Input:
        context id                      varchar

Output:
        cursor result set containing acl information
        acl_name                        varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT
                ACL_NAME
        FROM    REL_ACL_RP
        WHERE   CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_CONTEXT_ACL_NAMES;


PROCEDURE VIEW_RESOURCE
(
IN_CONTEXT_ID           IN RESOURCES.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN RESOURCES.RESOURCE_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given resources's
        information.

Input:
        context_id                      varchar2
        resource_id                     varchar2

Output:
        cursor result set containing resource information
        context_id                      varchar2
        resource_id                     varchar2
        description                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  RESOURCE_ID,
                CONTEXT_ID,
                DESCRIPTION
        FROM    RESOURCES
        WHERE   RESOURCE_ID = IN_RESOURCE_ID
        AND     CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_RESOURCE;


PROCEDURE VIEW_CONTEXT_RESOURCES
(
IN_CONTEXT_ID           IN RESOURCES.CONTEXT_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all resources'
        information in the given context.

Input:
        context_id                      varchar2

Output:
        cursor result set containing resource information
        context_id                      varchar2
        resource_id                     varchar2
        description                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  RESOURCE_ID,
                CONTEXT_ID,
                DESCRIPTION
        FROM    RESOURCES
        WHERE   (CONTEXT_ID = IN_CONTEXT_ID OR IN_CONTEXT_ID IS NULL);

END VIEW_CONTEXT_RESOURCES;


PROCEDURE VIEW_PERMISSION
(
IN_PERMISSION_ID        IN PERMISSION.PERMISSION_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the given permission's
        information.

Input:
        permission_id                   char

Output:
        cursor result set containing permission information
        permission_id                   char
        description                     varchar2

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  PERMISSION_ID,
                DESCRIPTION
        FROM    PERMISSION
        WHERE   PERMISSION_ID = IN_PERMISSION_ID;

END VIEW_PERMISSION;


PROCEDURE VIEW_PERMISSIONS
(
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all permissions'
        information.

Input:
        none

Output:
        cursor result set containing permission information
        permission_id                   char
        description                     varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  PERMISSION_ID,
                DESCRIPTION
        FROM    PERMISSION;

END VIEW_PERMISSIONS;


PROCEDURE VIEW_RESOURCE_ACLS
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_RESOURCE_ID          IN REL_ACL_RP.RESOURCE_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning acls' information and
        associated roles for the given resource within the given context id.

Input:
        context_id                      varchar2
        resource_id                     varchar2

Output:
        cursor result set containing acl and role information
        context_id                      varchar2
        resource_id                     varchar2
        permission_id                   char
        acl_name                        varchar2
        role_id                         number
        role_name                       varchar2
-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ARP.RESOURCE_ID,
                ARP.CONTEXT_ID,
                ARP.PERMISSION_ID,
                ARP.ACL_NAME,
                RA.ROLE_ID,
                R.ROLE_NAME
        FROM    REL_ACL_RP ARP
        JOIN    REL_ROLE_ACL RA
        ON      ARP.ACL_NAME = RA.ACL_NAME
        JOIN    ROLE R
        ON      RA.ROLE_ID = R.ROLE_ID
        WHERE   ARP.RESOURCE_ID = IN_RESOURCE_ID
        AND     ARP.CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_RESOURCE_ACLS;


PROCEDURE VIEW_ACL_RESOURCES
(
IN_CONTEXT_ID           IN REL_ACL_RP.CONTEXT_ID%TYPE,
IN_ACL_NAME             IN REL_ACL_RP.ACL_NAME%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all resources for the
        given context and the associated ones to the given acl number
        within the given context id.

Input:
        context_id                      varchar2
        acl_name                        number

Output:
        cursor result set containing acl and role information
        resource_id                     varchar2
        acl_name                        number

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  DISTINCT
                R.RESOURCE_ID,
                ARP.ACL_NAME
        FROM    RESOURCES R
        LEFT OUTER JOIN REL_ACL_RP ARP
        ON      ARP.RESOURCE_ID = R.RESOURCE_ID
        AND     ARP.CONTEXT_ID = R.CONTEXT_ID
        AND     ARP.ACL_NAME = IN_ACL_NAME
        WHERE   R.CONTEXT_ID = IN_CONTEXT_ID;

END VIEW_ACL_RESOURCES;




END ACL_PKG;
.
/
