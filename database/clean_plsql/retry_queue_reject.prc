CREATE OR REPLACE
PROCEDURE clean.RETRY_QUEUE_REJECT
AS
TYPE merc_tab IS TABLE OF VARCHAR2(20) INDEX BY PLS_INTEGER;
TYPE id_tab IS TABLE OF INTEGER INDEX BY PLS_INTEGER;
TYPE ord_tab IS TABLE OF INTEGER INDEX BY PLS_INTEGER;
-- v_merc_tab      merc_tab;
v_id_tab        id_tab;
-- v_ord_tab       ord_tab;
v_status        varchar2(1) := 'Y';
v_message       varchar2(1000);
v_sysdate       date;
--clean up any venus messages in venus q.
CURSOR q_cur IS
   select queue.message_id  from clean.queue
   join mercury.mercury on queue.mercury_id = mercury.mercury_id
   where queue_type in ('ASK', 'ANS')
   and mercury.comments like 'ACTUAL DELIVERY_DATE%';
BEGIN
   v_sysdate := sysdate;
   -- get mercury ids
OPEN q_cur;
-- FETCH q_cur BULK COLLECT INTO v_merc_tab, v_id_tab, v_ord_tab;
FETCH q_cur BULK COLLECT INTO v_id_tab;
CLOSE q_cur;
-- q delete retrys
FOR x IN 1..v_id_tab.count LOOP
        clean.queue_pkg.delete_queue_record_no_auth
        (
                IN_MESSAGE_ID=>v_id_tab (x),
                IN_CSR_ID=>user,
                OUT_STATUS=>v_status,
                OUT_ERROR_MESSAGE=>v_message
        );
        IF v_status = 'N' THEN
                DBMS_OUTPUT.PUT_LINE ('Q DELETED ERROR ON MESSAGE ID::' || V_ID_TAB(X));
                exit;
        END IF;
END LOOP;
DBMS_OUTPUT.PUT_LINE ('Q DELETED::' || TO_CHAR (V_ID_TAB.COUNT));
-- insert into the mercury_hp table for reprocessing
-- IF v_status = 'Y' THEN
--        FOR x IN 1..v_ord_tab.count LOOP
--              events.post_a_message(v_ord_tab(x));
--        end loop;
-- END IF;
-- DBMS_OUTPUT.PUT_LINE ('INSERT INTO MERCURY_HP::' || TO_CHAR (V_MERC_TAB.COUNT));
END;
.
/
