CREATE OR REPLACE
TRIGGER clean.direct_mail_trg
AFTER UPDATE OF subscribe_status OR INSERT ON clean.direct_mail
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
BEGIN
   IF :new.subscribe_status IS NOT NULL THEN
           INSERT INTO clean.direct_mail_history (customer_id, company_id, updated_on, updated_by, subscribe_status)
           VALUES (:new.customer_id, :new.company_id, sysdate, :new.updated_by, :new.subscribe_status);
   END IF;
END direct_mail_trg;
.
/
