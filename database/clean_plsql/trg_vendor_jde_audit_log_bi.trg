CREATE OR REPLACE TRIGGER clean.trg_vendor_jde_audit_log_bi
BEFORE INSERT
ON clean.vendor_jde_audit_log
FOR EACH ROW
-- PL/SQL Block
BEGIN
   IF INSERTING THEN
      IF :NEW.log_id IS NULL THEN
         SELECT
            clean.vendor_jde_audit_log_sq.NEXTVAL
         INTO
            :NEW.log_id
         FROM
            dual;
      END IF;
   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      ROLLBACK;
      RAISE_APPLICATION_ERROR(-2000, 'Fatal Error In CLEAN.TRG_VENDOR_JDE_AUDIT_LOG_BI: SQLCODE: ' || SQLCODE ||
          ' SQL ERROR MESSAGE: ' || SQLERRM);
END;
/