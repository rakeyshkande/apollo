CREATE OR REPLACE
PACKAGE BODY clean.CALL_LOG_PKG AS


PROCEDURE INSERT_CALL_LOG
(
IN_DNIS_ID                      IN CALL_LOG.DNIS_ID%TYPE,
IN_CSR_ID                       IN CALL_LOG.CSR_ID%TYPE,
OUT_CALL_LOG_ID                 OUT CALL_LOG.CALL_LOG_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for starting the call log time
        for the given dnis.

Input:
        dnis_id                         number
        csr_id                          varchar2

Output:
        call_log_id                     number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO call_log
(
        call_log_id,
        dnis_id,
        csr_id,
        start_time

)
VALUES
(
        call_log_id_sq.nextval,
        in_dnis_id,
        in_csr_id,
        sysdate
) RETURNING call_log_id INTO out_call_log_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CALL_LOG;


PROCEDURE UPDATE_CALL_LOG
(
IN_CALL_LOG_ID                  IN CALL_LOG.CALL_LOG_ID%TYPE,
IN_CALL_REASON_CODE             IN CALL_LOG.CALL_REASON_CODE%TYPE,
IN_CALL_REASON_TYPE             IN CALL_LOG.CALL_REASON_TYPE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for stopping the call log and setting
        the call log reason code

Input:
        call_log_id                     number
        customer_id                     number
        order_guid                      varchar2
        call_reason_code                varchar2
        call_reason_type                varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  call_log
SET
        call_reason_code = in_call_reason_code,
        end_time = sysdate,
        call_reason_type = in_call_reason_type
WHERE   call_log_id = in_call_log_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CALL_LOG;


PROCEDURE DELETE_CALL_LOG
(
IN_CALL_LOG_ID                  IN CALL_LOG.CALL_LOG_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting call logs

Input:
        call_log_id                     number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

DELETE FROM call_log
WHERE   call_log_id = in_call_log_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_CALL_LOG;


PROCEDURE VIEW_CALL_REASON_VAL
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for

Input:
        call_reason_code                varchar2
        description                     varchar2
        call_reason_type                varchar2
        status                          varchar2

Output:
        cursor containing all records from call_reason_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                call_reason_code,
                description,
                call_reason_type
        FROM    call_reason_val
        WHERE   status = 'Active'
        ORDER BY call_reason_type, display_order;

END VIEW_CALL_REASON_VAL;

END;
.
/
