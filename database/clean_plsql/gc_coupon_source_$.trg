/******** Triggers to populate the shadow tables in CLEAN schema *********************************************************/

-- CLEAN.GC_COUPON_SOURCE_$ trigger is used to populate CLEAN.GC_COUPON_SOURCE$ shadow table

CREATE OR REPLACE TRIGGER "CLEAN"."GC_COUPON_SOURCE_$" 
AFTER INSERT OR UPDATE OR DELETE ON clean.gc_coupon_source REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
BEGIN

   IF INSERTING THEN

      INSERT INTO clean.gc_coupon_source$ (
      	REQUEST_NUMBER,
		SOURCE_CODE,
		CREATED_ON,
		CREATED_BY,
		UPDATED_ON,
		UPDATED_BY,
		OPERATION$,
		TIMESTAMP$
      ) VALUES (
      	:NEW.REQUEST_NUMBER,
      	:NEW.SOURCE_CODE,
      	:NEW.CREATED_ON,
      	:NEW.CREATED_BY,
      	:NEW.CREATED_ON,
      	:NEW.UPDATED_BY,
		'INS',
      	SYSDATE
      	);

   ELSIF UPDATING  THEN

      INSERT INTO clean.gc_coupon_source$ (
      	REQUEST_NUMBER,
		SOURCE_CODE,
		CREATED_ON,
		CREATED_BY,
		UPDATED_ON,
		UPDATED_BY,
		OPERATION$,
		TIMESTAMP$
      ) VALUES (
      	:OLD.REQUEST_NUMBER,
      	:OLD.SOURCE_CODE,
      	:OLD.CREATED_ON,
      	:OLD.CREATED_BY,
      	:OLD.CREATED_ON,
      	:OLD.UPDATED_BY,
      	'UPD_OLD',
      	SYSDATE);

      INSERT INTO clean.gc_coupon_source$ (
      	REQUEST_NUMBER,
		SOURCE_CODE,
		CREATED_ON,
		CREATED_BY,
		UPDATED_ON,
		UPDATED_BY,
		OPERATION$,
		TIMESTAMP$
      ) VALUES (
      	:NEW.REQUEST_NUMBER,
      	:NEW.SOURCE_CODE,
      	:NEW.CREATED_ON,
      	:NEW.CREATED_BY,
      	:NEW.CREATED_ON,
      	:NEW.UPDATED_BY,
      	'UPD_NEW',
      	SYSDATE);

   ELSIF DELETING  THEN

     INSERT INTO clean.gc_coupon_source$ (
      	REQUEST_NUMBER,
		SOURCE_CODE,
		CREATED_ON,
		CREATED_BY,
		UPDATED_ON,
		UPDATED_BY,
		OPERATION$,
		TIMESTAMP$
      ) VALUES (
      	:OLD.REQUEST_NUMBER,
      	:OLD.SOURCE_CODE,
      	:OLD.CREATED_ON,
      	:OLD.CREATED_BY,
      	:OLD.CREATED_ON,
      	:OLD.UPDATED_BY,
      	'UPD_OLD',
      	SYSDATE);

   END IF;

END;
/


ALTER TRIGGER "CLEAN"."GC_COUPON_SOURCE_$" ENABLE;

