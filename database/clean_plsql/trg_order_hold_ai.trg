CREATE OR REPLACE
TRIGGER clean.trg_order_hold_ai
AFTER INSERT
ON clean.order_hold
FOR EACH ROW

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE

   lchr_current_status   frp.order_state.status%TYPE := NULL;
   lchr_new_status   frp.order_state.status%TYPE := NULL;
   lchr_trigger_name   frp.order_state_history.trigger_name%TYPE := 'TRG_ORDER_HOLD_AI';
   lnum_order_detail_id   clean.order_hold.order_detail_id%TYPE := :NEW.order_detail_id;

BEGIN

   -- fetch the current status
   lchr_current_status := frp.fun_get_current_status(lnum_order_detail_id);

   -- initialize the status
   IF :NEW.reason = 'RE-AUTH' THEN
      lchr_new_status := 'ORDER_QUEUED_REAUTH';
   ELSIF :NEW.reason = 'GNADD' THEN
      lchr_new_status := 'ORDER_QUEUED_GNADD';
   END IF;

   -- populate the order_state tables
   IF lchr_new_status IS NOT NULL THEN

      -- insert or update into frp.order_state table
      frp.prc_pop_order_state (
         lnum_order_detail_id,
         lchr_new_status);

      -- insert into frp.order_state_history table
      frp.prc_pop_order_state_history (
         lnum_order_detail_id,
         lchr_new_status,
         lchr_current_status,
         lchr_trigger_name);

   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In CLEAN.TRG_ORDER_HOLD_AI: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_order_hold_ai;
.
/
