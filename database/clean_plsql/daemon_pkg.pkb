CREATE OR REPLACE
PACKAGE BODY clean.daemon_pkg AS

   pg_this_package VARCHAR2(61) := 'clean.daemon_pkg';

FUNCTION CURRENTLY_RUNNING (p_job_name IN VARCHAR2) RETURN BOOLEAN IS

/*-----------------------------------------------------------------------------
Description:
        Function looks for the daemon in the DBMS_JOB queue.  Returns
        a TRUE if it is found, or a FALSE if it is not.

Parameter:
        the name of the job, as found in user_jobs.what, including any parameters.

Returns:
        BOOLEAN value indicating if job is already in the queue.
-----------------------------------------------------------------------------*/

   v_check    NUMBER;
   v_return   BOOLEAN;

BEGIN

   SELECT COUNT(*)
   INTO   v_check
   FROM   user_jobs
   WHERE  what like p_job_name||'%';

   IF v_check > 0 THEN
      v_return := TRUE;
   ELSE
      v_return := FALSE;
   END IF;

   RETURN V_RETURN;

END CURRENTLY_RUNNING;


PROCEDURE submit (p_procedure IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        submit a scheduled job
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'submit';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_submit_string         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   v_submit_string := p_procedure||'('||frp.misc_pkg.get_global_parm_value(p_procedure,'THRESHHOLD')||');';

   -- Make sure daemon is not already running in the queue.
   IF currently_running (p_procedure) THEN

      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' IS ALREADY RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   ELSE

      v_interval := frp.misc_pkg.get_global_parm_value(p_procedure,'INTERVAL');
      dbms_job.submit(v_job, v_submit_string, SYSDATE, v_interval);
      COMMIT;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' STARTED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

   END IF;

EXCEPTION WHEN OTHERS THEN
   frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
         pg_this_package||'.'||v_this_procedure||' '||v_submit_string||' RETURNED ERROR: '||
              '[' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256),
         sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);

END submit;


PROCEDURE remove (p_procedure IN VARCHAR2) IS

/*-----------------------------------------------------------------------------
Description:
        Search for and remove the daemon from the DBMS JOB queue.
        If there's more than one, it will remove the lowest-numbered one.
-----------------------------------------------------------------------------*/

   v_this_procedure        VARCHAR2(50) := 'remove';
   v_job                   BINARY_INTEGER;
   v_error_message         VARCHAR2(1000);
   v_system_message_id     NUMBER;
   v_status                VARCHAR2(1000);
   v_message               VARCHAR2(1000);
   v_interval              VARCHAR2(200);

BEGIN

   SELECT MIN(job)
   INTO   v_job
   FROM   user_jobs
   WHERE  what like p_procedure||'%';

   IF v_job IS NOT NULL THEN
      dbms_job.remove(v_job);
      commit;
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'INFO',
            pg_this_package||'.'||v_this_procedure||' '||p_procedure||' REMOVED',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   ELSE
      frp.misc_pkg.insert_system_messages ('SCHEDULED JOBS', 'ERROR',
            pg_this_package||'.'||v_this_procedure||' '||p_procedure||' NOT RUNNING',
            sys_context('USERENV','HOST'), v_system_message_id, v_status, v_message);
   END IF;

END remove;


END daemon_pkg;
.
/
