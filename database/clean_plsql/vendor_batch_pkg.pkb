CREATE OR REPLACE PACKAGE BODY CLEAN.VENDOR_BATCH_PKG AS

PROCEDURE MARK_TRANSACTIONS_REJECTED
(
IN_DATE										IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a record in VENUS.VENUS table 

Input:
        in_date					             date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
	
   CURSOR ftd_cur IS	
	SELECT vv.*,
			 od.order_detail_id,
			 od.external_order_number,
			 od.order_guid,
			 o.master_order_number
	  FROM venus.venus vv
	  JOIN clean.order_details od
	    ON od.order_detail_id = TO_NUMBER(vv.reference_number)
	  JOIN clean.orders o
	    ON o.order_guid = od.order_guid
	 WHERE vv.sds_status = 'AVAILABLE'
	   AND vv.msg_type = 'FTD'
	   AND vv.shipping_system = 'SDS'
     AND decode(nvl(vv.ZONE_JUMP_FLAG,'N'),'Y', vv.ZONE_JUMP_LABEL_DATE, vv.SHIP_DATE) <= in_date;
	   
   lnum_venus_id			NUMBER;
   lnum_message_id      NUMBER;
	v_target_order			VARCHAR2(1) := NULL;

BEGIN


   FOR ftd_rec IN ftd_cur 
   LOOP
   
      -- create REJ INBOUND message for all the retreived records
		venus.venus_pkg.insert_venus_message
		(
		 IN_VENUS_ORDER_NUMBER  			=> ftd_rec.venus_order_number,
		 IN_VENUS_STATUS              	=> 'OPEN',
		 IN_MSG_TYPE               	   => 'REJ',
		 IN_SENDING_VENDOR         		=> ftd_rec.sending_vendor,
		 IN_FILLING_VENDOR           		=> ftd_rec.filling_vendor,
		 IN_ORDER_DATE             	   => ftd_rec.order_date,
		 IN_RECIPIENT              	   => ftd_rec.recipient,
		 IN_BUSINESS_NAME            		=> ftd_rec.business_name,
		 IN_ADDRESS_1              		=> ftd_rec.address_1,
		 IN_ADDRESS_2              		=> ftd_rec.address_2,
		 IN_CITY                   		=> ftd_rec.city,
		 IN_STATE                  		=> ftd_rec.state,
		 IN_ZIP                    		=> ftd_rec.zip,
		 IN_PHONE_NUMBER           		=> ftd_rec.phone_number,
		 IN_DELIVERY_DATE          		=> ftd_rec.delivery_date,
		 IN_FIRST_CHOICE           		=> ftd_rec.first_choice,
		 IN_PRICE                  		=> ftd_rec.price,
		 IN_CARD_MESSAGE           		=> ftd_rec.card_message,
		 IN_SHIP_DATE              		=> ftd_rec.ship_date,
		 IN_OPERATOR               		=> 'EOD',
		 IN_COMMENTS               		=> 'Order should have printed today, rejected by end of day process on ' || TO_CHAR(in_date, 'MM/DD/YYYY HH:MI:SS AM'),
		 IN_TRANSMISSION_TIME   			=> SYSDATE,
		 IN_REFERENCE_NUMBER    			=> ftd_rec.reference_number,
		 IN_PRODUCT_ID             		=> ftd_rec.product_id,
		 IN_COMBINED_REPORT_NUMBER 		=> ftd_rec.combined_report_number,
		 IN_ROF_NUMBER             		=> ftd_rec.rof_number,
		 IN_ADJ_REASON_CODE        		=> NULL,
		 IN_OVER_UNDER_CHARGE   			=> NULL,
		 IN_PROCESSED_INDICATOR    		=> 'Processed',
		 IN_MESSAGE_TEXT           	   => ftd_rec.message_text,
		 IN_SEND_TO_VENUS             	=> ftd_rec.send_to_venus,
		 IN_ORDER_PRINTED          	 	=> NULL,
		 IN_SUB_TYPE               	 	=> ftd_rec.sub_type,
		 IN_ERROR_DESCRIPTION    			=> NULL,
		 IN_COUNTRY                	 	=> ftd_rec.country,
		 IN_CANCEL_REASON_CODE 				=> 'VCN',
		 IN_COMP_ORDER             	  	=> ftd_rec.comp_order,
		 IN_OLD_PRICE              	 	=> NULL,
		 IN_SHIP_METHOD            	 	=> ftd_rec.ship_method,
		 IN_VENDOR_SKU             	 	=> ftd_rec.vendor_sku,
		 IN_PRODUCT_DESCRIPTION    		=> ftd_rec.product_description,
		 IN_PRODUCT_WEIGHT            	=> ftd_rec.product_weight,
		 IN_MESSAGE_DIRECTION       		=> 'INBOUND',
		 IN_EXTERNAL_SYSTEM_STATUS			=> 'NOTVERIFIED', 
		 IN_SHIPPING_SYSTEM        		=> ftd_rec.shipping_system,
		 IN_TRACKING_NUMBER					=> NULL,
		 IN_PRINTED								=> NULL,
		 IN_SHIPPED 							=> NULL,
		 IN_CANCELLED							=> NULL,
		 IN_FINAL_CARRIER						=> NULL,
		 IN_FINAL_SHIP_METHOD				=> NULL,
		 IN_REJECTED                     => NULL,
 		 IN_VENDOR_ID                    => NULL,
 		 IN_PERSONAL_GREETING_ID                    => ftd_rec.personal_greeting_id,
 		 IN_ORIG_COMPLAINT_COMM_TYPE_ID  => NULL,
 		 IN_NOTI_COMPLAINT_COMM_TYPE_ID  => NULL,
 		 IN_NEW_SHIPPING_SYSTEM          => ftd_rec.new_shipping_system, 		 
		 OUT_VENUS_ID							=> lnum_venus_id,
		 OUT_STATUS								=> OUT_STATUS,
		 OUT_ERROR_MESSAGE					=> OUT_MESSAGE
		);
		
		
		-- insert into REJ queue. Yet to determine
		clean.queue_pkg.insert_queue_record
			(in_queue_type => 'REJ',
			 in_message_type => 'REJ',
			 in_message_timestamp => SYSDATE,
			 in_system => 'Venus',
			 in_mercury_number => ftd_rec.venus_order_number,
			 in_master_order_number => ftd_rec.master_order_number,
			 in_order_guid => ftd_rec.order_guid,
			 in_order_detail_id => ftd_rec.order_detail_id,
			 in_point_of_contact_id => NULL,
			 in_email_address => NULL,
			 in_external_order_number => ftd_rec.external_order_number,
         		 in_mercury_id => to_char(lnum_venus_id),
			 out_message_id => lnum_message_id,
			 out_status => out_status,
			 out_error_message => out_message
		);
   	
   -- update the CANCELLED field for the 'FTD' record
   UPDATE venus.venus
      SET cancelled = SYSDATE
    WHERE venus_id = ftd_rec.venus_id;
		
	END LOOP;
	
	
	COMMIT;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.MARK_TRANSACTIONS_REJECTED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END MARK_TRANSACTIONS_REJECTED;


PROCEDURE INSERT_JDE_AUDIT_LOG
(
IN_DATE										IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting orders in CLEAN.VENDOR_JDE_AUDIT_LOG table

Input:
        in_date								 date
        in_commit_pt							 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
	
	CURSOR cur_ftd_shipped IS
	(SELECT vv.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv.ship_date ship_date,
			  vv.delivery_date delivery_date,
			  vv.vendor_sku vendor_sku,
			  vv.product_id product_id,
			  vv.price orig_amount,
			  0 adj_amount,
			  vv.sds_status order_status,
			  'SHIP' record_type,
			  vv.cancel_reason_code cancel_reason_code,
			  null adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN clean.order_details od
		  ON vv.reference_number = TO_CHAR(od.order_detail_id)
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv.vendor_id
		 AND vp.product_subcode_id=vv.product_id
	  WHERE vv.msg_type = 'FTD'
	    AND vv.shipping_system = 'SDS'
		 AND vv.shipped is not null
		 -- delivery date between 1st and last day of prior month
		 AND vv.delivery_date <= LAST_DAY(in_date) -- last day
		 AND vv.delivery_date >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
		 -- eliminate CAN and REJ and ADJ
		 AND NOT EXISTS (
			  SELECT 1
				 FROM venus.venus vv2
				WHERE vv2.msg_type IN ('REJ', 'CAN', 'ADJ')
				  AND vv2.venus_order_number = vv.venus_order_number)
	UNION
	SELECT vv.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv.ship_date ship_date,
			  vv.delivery_date delivery_date,
			  vv.vendor_sku vendor_sku,
			  vv.product_id product_id,
			  vv.price orig_amount,
			  0 adj_amount,
			  vv.sds_status order_status,
			  'SHIP' record_type,
			  vv.cancel_reason_code cancel_reason_code,
			  null adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN clean.order_details od
		  ON vv.reference_number = TO_CHAR(od.order_detail_id)
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv.vendor_id
		 AND vp.product_subcode_id=vv.product_id
	  WHERE vv.msg_type = 'FTD'
	    AND vv.shipping_system = 'SDS'
		 AND vv.shipped is not null
		 -- delivery date between 1st and last day of prior month
		 AND vv.delivery_date <= LAST_DAY(in_date) -- last day
		 AND vv.delivery_date >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
		 -- include FTD's that have both CAN's and ADJ's
		 AND EXISTS (
			  SELECT 1
				 FROM venus.venus vv2
				WHERE vv2.msg_type ='CAN'
				  AND vv2.venus_order_number = vv.venus_order_number)
		 AND EXISTS (
			  SELECT 1
				 FROM venus.venus vv3
				WHERE vv3.msg_type ='ADJ'
				  AND vv3.venus_order_number = vv.venus_order_number)
	);
		
	CURSOR cur_adj IS
	(SELECT vv2.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv2.ship_date ship_date,
			  vv2.delivery_date delivery_date,
			  vv2.vendor_sku vendor_sku,
			  vv2.product_id product_id,
			  0 orig_amount,
			  -vv2.price adj_amount,
			  vv2.sds_status order_status,
			  'ADJ' record_type,
			  vv.cancel_reason_code cancel_reason_code,
                          to_date(to_char(vv.created_on,'MM/DD/YYYY HH24:MI'),'MM/DD/YYYY HH24:MI') adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN venus.venus vv2
		  ON vv.venus_order_number = vv2.venus_order_number
		 AND vv2.msg_type = 'FTD'
		 AND vv2.shipping_system = 'SDS'
		 AND vv2.shipped is not null
		JOIN clean.order_details od
		  ON vv2.reference_number = TO_CHAR(od.order_detail_id)
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv2.vendor_id
		 AND vp.product_subcode_id=vv2.product_id
	  WHERE vv.msg_type = 'ADJ'
	    AND vv.shipping_system = 'SDS'
		 -- adjustment create date between 1st and last day of prior month
		 AND trunc(vv.created_on) <= LAST_DAY(in_date) -- last day
	    AND trunc(vv.created_on) >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
	); 

	CURSOR cur_can IS
	(SELECT vv2.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv2.ship_date ship_date,
			  vv2.delivery_date delivery_date,
			  vv2.vendor_sku vendor_sku,
			  vv2.product_id product_id,
			  --vv2.price orig_amount,
			  0 orig_amount,
			  0 adj_amount,
			  vv2.sds_status order_status,
			  'CAN' record_type,
			  vv.cancel_reason_code cancel_reason_code,
			  null adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN venus.venus vv2
		  ON vv.venus_order_number = vv2.venus_order_number
		 AND vv2.msg_type = 'FTD'
		 AND vv2.shipping_system = 'SDS'
		 AND vv2.shipped is not null
		JOIN clean.order_details od
		  ON vv2.reference_number = TO_CHAR(od.order_detail_id)
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv2.vendor_id
		 AND vp.product_subcode_id=vv.product_id
	  WHERE vv.msg_type IN ('CAN', 'REJ')
	    AND vv.shipping_system = 'SDS'
		 -- delivery date between 1st and last day of prior month
		 AND vv2.delivery_date <= LAST_DAY(in_date) -- last day
	    AND vv2.delivery_date >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
		 -- eliminate ADJ
		 AND NOT EXISTS (
			  SELECT 1
				 FROM venus.venus vv2
				WHERE vv2.msg_type IN ('ADJ')
				  AND vv2.venus_order_number = vv.venus_order_number)
	);
	
BEGIN

   FOR rec IN cur_ftd_shipped
	LOOP
	
		-- insert SHIPPED orders
		INSERT INTO CLEAN.VENDOR_JDE_AUDIT_LOG
			(
			vendor_id,
			filling_vendor,
			venus_order_number,
			external_order_number,
			ship_date,
			delivery_date,
			vendor_sku,
			product_id,
			orig_amount,
			adj_amount,
			order_status,
			record_type,
			cancel_reason_code,
			adj_created_datetime,
			created_on,
			order_guid,
			zone_jump_label_date)
		VALUES
		(
			rec.vendor_id,
			rec.filling_vendor,
			rec.venus_order_number,
			rec.external_order_number,
			rec.ship_date,
			rec.delivery_date,
			rec.vendor_sku,
			rec.product_id,
			rec.orig_amount,
			rec.adj_amount,
			rec.order_status,
			rec.record_type,
			rec.cancel_reason_code,
			rec.adj_created_datetime,
			rec.created_on,
			rec.order_guid,
			rec.zone_jump_label_date
		);
	 
   END LOOP;

 	FOR rec IN cur_adj
	LOOP
	
		-- Insert adjustments (this is needed by month end statement)
		INSERT INTO CLEAN.VENDOR_JDE_AUDIT_LOG
			(
			vendor_id,
			filling_vendor,
			venus_order_number,
			external_order_number,
			ship_date,
			delivery_date,
			vendor_sku,
			product_id,
			orig_amount,
			adj_amount,
			order_status,
			record_type,
			cancel_reason_code,
			adj_created_datetime,
			created_on,
			order_guid,
			zone_jump_label_date)
		VALUES
		(
			rec.vendor_id,
			rec.filling_vendor,
			rec.venus_order_number,
			rec.external_order_number,
			rec.ship_date,
			rec.delivery_date,
			rec.vendor_sku,
			rec.product_id,
			rec.orig_amount,
			rec.adj_amount,
			rec.order_status,
			rec.record_type,
			rec.cancel_reason_code,
			rec.adj_created_datetime,
			rec.created_on,
			rec.order_guid,
			rec.zone_jump_label_date
		);
	 
   END LOOP;

 	FOR rec IN cur_can
	LOOP
	
		-- Insert adjustments (this is needed by month end statement)
		INSERT INTO CLEAN.VENDOR_JDE_AUDIT_LOG
			(
			vendor_id,
			filling_vendor,
			venus_order_number,
			external_order_number,
			ship_date,
			delivery_date,
			vendor_sku,
			product_id,
			orig_amount,
			adj_amount,
			order_status,
			record_type,
			cancel_reason_code,
			adj_created_datetime,
			created_on,
			order_guid,
			zone_jump_label_date)
		VALUES
		(
			rec.vendor_id,
			rec.filling_vendor,
			rec.venus_order_number,
			rec.external_order_number,
			rec.ship_date,
			rec.delivery_date,
			rec.vendor_sku,
			rec.product_id,
			rec.orig_amount,
			rec.adj_amount,
			rec.order_status,
			rec.record_type,
			rec.cancel_reason_code,
			rec.adj_created_datetime,
			rec.created_on,
			rec.order_guid,
			rec.zone_jump_label_date
		);
	 
   END LOOP;
   
   COMMIT;  
     
	OUT_STATUS := 'Y';
	OUT_MESSAGE := NULL;

EXCEPTION 
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.INSERT_JDE_AUDIT_LOG [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END INSERT_JDE_AUDIT_LOG;


PROCEDURE GET_JDE_CREDITS
(
IN_DATE										IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_CURSOR        						OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for fetching records from CLEAN.VENDOR_JDE_AUDIT_LOG 

Input:
        in_date									 date

Output:
		  cursor									 contains clean.vendor_jde_audit_log table record
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   OPEN out_cursor FOR
	SELECT l.filling_vendor filling_vendor, 
		    SUM(l.orig_amount) amount,
		    COUNT(l.orig_amount) count,
		    vm.accounts_payable_id accounts_payable_id,
		    vm.gl_account_number gl_account_number,
		    vm.company company
	  FROM clean.vendor_jde_audit_log l
	  JOIN ftd_apps.vendor_master vm
	    ON l.vendor_id = vm.vendor_id
	 WHERE l.record_type = 'SHIP'
	   AND l.created_on <= ADD_MONTHS(LAST_DAY(in_date),1) -- last day of the following month
           AND l.created_on >= (LAST_DAY(TRUNC(in_date)) + 1)  -- first day of the following month
	 GROUP BY l.filling_vendor,
	 		 vm.accounts_payable_id,
	 		 vm.gl_account_number,
	 		 vm.company
	 ORDER BY l.filling_vendor;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.GET_JDE_CREDITS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END GET_JDE_CREDITS;

PROCEDURE GET_JDE_ADJ
(
IN_DATE										IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_CURSOR        						OUT TYPES.REF_CURSOR,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for fetching adj records from CLEAN.VENDOR_JDE_AUDIT_LOG 
with records created in the following month.

Input:
        in_date									 date

Output:
		  cursor									 contains clean.vendor_jde_audit_log table record
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	OPEN out_cursor FOR
	SELECT l.filling_vendor filling_vendor, 
			 SUM(l.adj_amount) amount,
			 COUNT(l.adj_amount) count,
			 vm.accounts_payable_id accounts_payable_id,
			 vm.gl_account_number gl_account_number,
			 vm.company company
	  FROM clean.vendor_jde_audit_log l
	  JOIN ftd_apps.vendor_master vm
	    ON l.vendor_id = vm.vendor_id
	 WHERE l.record_type = 'ADJ'
	   AND l.created_on <= ADD_MONTHS(LAST_DAY(in_date),1) -- last day of the following month
           AND l.created_on >= (LAST_DAY(TRUNC(in_date)) + 1)  -- first day of the following month
	 GROUP BY l.filling_vendor,
	 		 vm.accounts_payable_id,
	 		 vm.gl_account_number,
	 		 vm.company
	 ORDER BY l.filling_vendor;
	
	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.GET_JDE_ADJ [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_JDE_ADJ;


PROCEDURE RUN_VENDOR_EOD
(
   in_date IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE DEFAULT SYSDATE
) 
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for running the Vendor End of Day process.
        Any errors returned from the procedure will result in an email being
        sent to the support team.  Any unexpected errors will be recorded
        in DBA_JOBS, and subsequently an email to the DBAs.

Input:
        in_date		 date

Output:
	None

-----------------------------------------------------------------------------*/
   v_status              VARCHAR2(4000);
   v_message             VARCHAR2(4000);

   c                     utl_smtp.connection;
   v_location            VARCHAR2(10);


BEGIN
   clean.vendor_batch_pkg.mark_transactions_rejected(in_date,v_status,v_message);

   if v_status != 'Y' then
      --dbms_output.put_line('FAILED EXECUTION OF VENDOR_EOD.  STATUS=' || v_status);

      SELECT sys_context('USERENV','DB_NAME') INTO  v_location FROM   dual;


      ops$oracle.mail_pkg.init_email(c, 'SCRUB_ALERTS', 'Vendor End-of-Day Error', v_status, v_message);
      if v_status = 'N' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, utl_tcp.CRLF||'Database ' || v_location ||
         ' Problem with clean.vendor_batch_pkg.mark_transactions_rejected.  Message=' || v_message);

      ops$oracle.mail_pkg.close_email(c,v_status, v_message);
      if v_status = 'N' then
         raise_application_error(-20000,v_message,true);
      end if;

   end if;

-- NO EXCEPTION LOGIC.  UNEXPECTED ERRORS WILL RESULT FAILED ENTRY IN DBA_JOBS, THEN THE "JOB MONITOR" 
-- JOB WILL NOTICE AND SEND AN EMAIL TO THE DBAs.

END RUN_VENDOR_EOD;

PROCEDURE RUN_WEST_VENDOR_EOD
(
   in_date IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE DEFAULT SYSDATE
) 
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for running the FTD West Vendor End of Day process.
        Any errors returned from the procedure will result in an email being
        sent to the support team.  Any unexpected errors will be recorded
        in DBA_JOBS, and subsequently an email to the DBAs.

Input:
        in_date		 date

Output:
	None

-----------------------------------------------------------------------------*/
   v_status              VARCHAR2(4000);
   v_message             VARCHAR2(4000);

   c                     utl_smtp.connection;
   v_location            VARCHAR2(10);


BEGIN
   clean.vendor_batch_pkg.mark_west_orders_rejected(in_date,v_status,v_message);

   if v_status != 'Y' then
      --dbms_output.put_line('FAILED EXECUTION OF VENDOR_EOD.  STATUS=' || v_status);

      SELECT sys_context('USERENV','DB_NAME') INTO  v_location FROM   dual;


      ops$oracle.mail_pkg.init_email(c, 'SCRUB_ALERTS', 'FTD West Vendor End-of-Day Error', v_status, v_message);
      if v_status = 'N' then
         raise_application_error(-20000,v_message,true);
      end if;

      utl_smtp.write_data(c, utl_tcp.CRLF||'Database ' || v_location ||
         ' Problem with clean.vendor_batch_pkg.mark_west_orders_rejected.  Message=' || v_message);

      ops$oracle.mail_pkg.close_email(c,v_status, v_message);
      if v_status = 'N' then
         raise_application_error(-20000,v_message,true);
      end if;

   end if;

-- NO EXCEPTION LOGIC.  UNEXPECTED ERRORS WILL RESULT FAILED ENTRY IN DBA_JOBS, THEN THE "JOB MONITOR" 
-- JOB WILL NOTICE AND SEND AN EMAIL TO THE DBAs.

END RUN_WEST_VENDOR_EOD;

PROCEDURE MARK_WEST_ORDERS_REJECTED
(
IN_DATE						IN CLEAN.VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_STATUS                      		OUT VARCHAR2,
OUT_MESSAGE                     		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a record in VENUS.VENUS table
        for FTD West Orders 

Input:
        in_date				date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
	
   CURSOR ftd_cur IS	
	 SELECT vv.*,
	       od.order_detail_id,
	       od.external_order_number,
	       od.order_guid,
 	       o.master_order_number
	  FROM venus.venus vv
	  JOIN clean.order_details od
	    ON od.order_detail_id = TO_NUMBER(vv.reference_number)
	  JOIN clean.orders o
	    ON o.order_guid = od.order_guid
	 WHERE vv.shipping_system = 'FTD WEST'
           and vv.msg_type = 'FTD'
           and vv.venus_status = 'VERIFIED'
           and vv.delivery_date = trunc(sysdate)
           and vv.cancelled is null
           and vv.rejected is null
           and vv.printed is null
           and vv.shipped is null;
	   
   lnum_venus_id			NUMBER;
   lnum_message_id      NUMBER;
	v_target_order			VARCHAR2(1) := NULL;

BEGIN


   FOR ftd_rec IN ftd_cur 
   LOOP
   
      -- create REJ INBOUND message for all the retreived records
		venus.venus_pkg.insert_venus_message
		(
		 IN_VENUS_ORDER_NUMBER  			=> ftd_rec.venus_order_number,
		 IN_VENUS_STATUS              	=> 'OPEN',
		 IN_MSG_TYPE               	   => 'REJ',
		 IN_SENDING_VENDOR         		=> ftd_rec.sending_vendor,
		 IN_FILLING_VENDOR           		=> ftd_rec.filling_vendor,
		 IN_ORDER_DATE             	   => ftd_rec.order_date,
		 IN_RECIPIENT              	   => ftd_rec.recipient,
		 IN_BUSINESS_NAME            		=> ftd_rec.business_name,
		 IN_ADDRESS_1              		=> ftd_rec.address_1,
		 IN_ADDRESS_2              		=> ftd_rec.address_2,
		 IN_CITY                   		=> ftd_rec.city,
		 IN_STATE                  		=> ftd_rec.state,
		 IN_ZIP                    		=> ftd_rec.zip,
		 IN_PHONE_NUMBER           		=> ftd_rec.phone_number,
		 IN_DELIVERY_DATE          		=> ftd_rec.delivery_date,
		 IN_FIRST_CHOICE           		=> ftd_rec.first_choice,
		 IN_PRICE                  		=> ftd_rec.price,
		 IN_CARD_MESSAGE           		=> ftd_rec.card_message,
		 IN_SHIP_DATE              		=> ftd_rec.ship_date,
		 IN_OPERATOR               		=> 'EOD',
		 IN_COMMENTS               		=> 'Order should have printed by now, rejected by end of day process on ' || TO_CHAR(in_date, 'MM/DD/YYYY HH:MI:SS AM'),
		 IN_TRANSMISSION_TIME   			=> SYSDATE,
		 IN_REFERENCE_NUMBER    			=> ftd_rec.reference_number,
		 IN_PRODUCT_ID             		=> ftd_rec.product_id,
		 IN_COMBINED_REPORT_NUMBER 		=> ftd_rec.combined_report_number,
		 IN_ROF_NUMBER             		=> ftd_rec.rof_number,
		 IN_ADJ_REASON_CODE        		=> NULL,
		 IN_OVER_UNDER_CHARGE   			=> NULL,
		 IN_PROCESSED_INDICATOR    		=> 'Processed',
		 IN_MESSAGE_TEXT           	   => ftd_rec.message_text,
		 IN_SEND_TO_VENUS             	=> ftd_rec.send_to_venus,
		 IN_ORDER_PRINTED          	 	=> NULL,
		 IN_SUB_TYPE               	 	=> ftd_rec.sub_type,
		 IN_ERROR_DESCRIPTION    			=> NULL,
		 IN_COUNTRY                	 	=> ftd_rec.country,
		 IN_CANCEL_REASON_CODE 				=> 'VCN',
		 IN_COMP_ORDER             	  	=> ftd_rec.comp_order,
		 IN_OLD_PRICE              	 	=> NULL,
		 IN_SHIP_METHOD            	 	=> ftd_rec.ship_method,
		 IN_VENDOR_SKU             	 	=> ftd_rec.vendor_sku,
		 IN_PRODUCT_DESCRIPTION    		=> ftd_rec.product_description,
		 IN_PRODUCT_WEIGHT            	=> ftd_rec.product_weight,
		 IN_MESSAGE_DIRECTION       		=> 'INBOUND',
		 IN_EXTERNAL_SYSTEM_STATUS			=> 'NOTVERIFIED', 
		 IN_SHIPPING_SYSTEM        		=> ftd_rec.shipping_system,
		 IN_TRACKING_NUMBER					=> NULL,
		 IN_PRINTED								=> NULL,
		 IN_SHIPPED 							=> NULL,
		 IN_CANCELLED							=> NULL,
		 IN_FINAL_CARRIER						=> NULL,
		 IN_FINAL_SHIP_METHOD				=> NULL,
		 IN_REJECTED                     => NULL,
 		 IN_VENDOR_ID                    => NULL,
 		 IN_PERSONAL_GREETING_ID                    => ftd_rec.personal_greeting_id,
 		 IN_ORIG_COMPLAINT_COMM_TYPE_ID  => NULL,
 		 IN_NOTI_COMPLAINT_COMM_TYPE_ID  => NULL,
 		 IN_NEW_SHIPPING_SYSTEM          => ftd_rec.new_shipping_system, 		 
		 OUT_VENUS_ID							=> lnum_venus_id,
		 OUT_STATUS								=> OUT_STATUS,
		 OUT_ERROR_MESSAGE					=> OUT_MESSAGE
		);
		
		
		-- insert into REJ queue. Yet to determine
		clean.queue_pkg.insert_queue_record
			(in_queue_type => 'REJ',
			 in_message_type => 'REJ',
			 in_message_timestamp => SYSDATE,
			 in_system => 'Venus',
			 in_mercury_number => ftd_rec.venus_order_number,
			 in_master_order_number => ftd_rec.master_order_number,
			 in_order_guid => ftd_rec.order_guid,
			 in_order_detail_id => ftd_rec.order_detail_id,
			 in_point_of_contact_id => NULL,
			 in_email_address => NULL,
			 in_external_order_number => ftd_rec.external_order_number,
         		 in_mercury_id => to_char(lnum_venus_id),
			 out_message_id => lnum_message_id,
			 out_status => out_status,
			 out_error_message => out_message
		);
   	
   -- update the CANCELLED field for the 'FTD' record
   UPDATE venus.venus
      SET cancelled = SYSDATE
    WHERE venus_id = ftd_rec.venus_id;
    
     -- update the CANCELLED field for the 'FTD' record
   UPDATE venus.venus
      SET cancelled = SYSDATE
    WHERE venus_id = ftd_rec.venus_id;
            
	END LOOP;	
	
	COMMIT;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.MARK_TRANSACTIONS_REJECTED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END MARK_WEST_ORDERS_REJECTED;

-- DIPII-6 : Create a monthly job to populate west_vendor_jde_audit_log table, prepare and send files to JDE.
PROCEDURE INSERT_WEST_JDE_AUDIT_LOG
(
IN_DATE			IN 	CLEAN.WEST_VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_STATUS		OUT	VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting orders in CLEAN.WEST_VENDOR_JDE_AUDIT_LOG table

Input:
        in_date								 date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
	v_filling_vendor	ftd_apps.vendor_master.member_number%type;
	
	CURSOR cur_ftd_shipped(p_filling_vendor in ftd_apps.vendor_master.member_number%type) IS
	(SELECT vv.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv.ship_date ship_date,
			  vv.delivery_date delivery_date,
			  vv.vendor_sku vendor_sku,
			  vv.product_id product_id,
			  --vv.price orig_amount,
			  decode(filling_vendor,p_filling_vendor,(vv.price+nvl(vao.addon,0)),vv.price) orig_amount,
			  0 adj_amount,
			  vv.sds_status order_status,
			  'SHIP' record_type,
			  vv.cancel_reason_code cancel_reason_code,
			  null adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN clean.order_details od
		  ON vv.reference_number = TO_CHAR(od.order_detail_id)
		/*  
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv.vendor_id
		 AND vp.product_subcode_id=vv.product_id
		*/ 
		LEFT OUTER JOIN (select venus_id, sum(per_addon_price) addon from venus.venus_add_ons group by venus_id) vao
		  ON vv.venus_id = vao.venus_id
	   WHERE vv.msg_type = 'FTD'
	    AND vv.shipping_system = 'FTD WEST'
		 AND vv.shipped is not null
		 -- delivery date between 1st and last day of prior month
		 AND vv.delivery_date <= LAST_DAY(in_date) -- last day
		 AND vv.delivery_date >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
		 -- eliminate CAN and REJ and ADJ
		 AND NOT EXISTS (
			  SELECT 1
				 FROM venus.venus vv2
				WHERE vv2.msg_type IN ('REJ', 'CAN', 'ADJ')
				  AND vv2.venus_order_number = vv.venus_order_number)
	UNION
	SELECT vv.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv.ship_date ship_date,
			  vv.delivery_date delivery_date,
			  vv.vendor_sku vendor_sku,
			  vv.product_id product_id,
			  --vv.price orig_amount,
			  decode(filling_vendor,p_filling_vendor,(vv.price+nvl(vao.addon,0)),vv.price) orig_amount,
			  0 adj_amount,
			  vv.sds_status order_status,
			  'SHIP' record_type,
			  vv.cancel_reason_code cancel_reason_code,
			  null adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN clean.order_details od
		  ON vv.reference_number = TO_CHAR(od.order_detail_id)
		/*  
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv.vendor_id
		 AND vp.product_subcode_id=vv.product_id
		*/ 
		LEFT OUTER JOIN (select venus_id, sum(per_addon_price) addon from venus.venus_add_ons group by venus_id) vao
		  ON vv.venus_id = vao.venus_id
	  WHERE vv.msg_type = 'FTD'
	    AND vv.shipping_system = 'FTD WEST'
		 AND vv.shipped is not null
		 -- delivery date between 1st and last day of prior month
		 AND vv.delivery_date <= LAST_DAY(in_date) -- last day
		 AND vv.delivery_date >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
		 -- include FTD's that have both CAN's and ADJ's
		 AND EXISTS (
			  SELECT 1
				 FROM venus.venus vv2
				WHERE vv2.msg_type ='CAN'
				  AND vv2.venus_order_number = vv.venus_order_number)
		 AND EXISTS (
			  SELECT 1
				 FROM venus.venus vv3
				WHERE vv3.msg_type ='ADJ'
				  AND vv3.venus_order_number = vv.venus_order_number)
	);
		
	CURSOR cur_adj(p_filling_vendor in ftd_apps.vendor_master.member_number%type) IS
	(SELECT vv2.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv2.ship_date ship_date,
			  vv2.delivery_date delivery_date,
			  vv2.vendor_sku vendor_sku,
			  vv2.product_id product_id,
			  0 orig_amount,
			  ---vv2.price adj_amount,
			  decode(vv2.filling_vendor,p_filling_vendor,-(vv2.price+nvl(vao.addon,0)),-vv2.price) adj_amount,
			  vv2.sds_status order_status,
			  'ADJ' record_type,
			  vv.cancel_reason_code cancel_reason_code,
                          to_date(to_char(vv.created_on,'MM/DD/YYYY HH24:MI'),'MM/DD/YYYY HH24:MI') adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN venus.venus vv2
		  ON vv.venus_order_number = vv2.venus_order_number
		 AND vv2.msg_type = 'FTD'
		 AND vv2.shipping_system = 'FTD WEST'
		 AND vv2.shipped is not null
		JOIN clean.order_details od
		  ON vv2.reference_number = TO_CHAR(od.order_detail_id)
		 /* 
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv2.vendor_id
		 AND vp.product_subcode_id=vv2.product_id
		 */
		LEFT OUTER JOIN (select venus_id, sum(per_addon_price) addon from venus.venus_add_ons group by venus_id) vao
		  ON vv2.venus_id = vao.venus_id
	  WHERE vv.msg_type = 'ADJ'
	    AND vv.shipping_system = 'FTD WEST'
		 -- adjustment create date between 1st and last day of prior month
		 AND trunc(vv.created_on) <= LAST_DAY(in_date) -- last day
	    AND trunc(vv.created_on) >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
	); 

	CURSOR cur_can IS
	(SELECT vv2.vendor_id vendor_id,
			  vv.filling_vendor filling_vendor,
			  vv.venus_order_number venus_order_number,
			  od.external_order_number external_order_number,
			  vv2.ship_date ship_date,
			  vv2.delivery_date delivery_date,
			  vv2.vendor_sku vendor_sku,
			  vv2.product_id product_id,
			  --vv2.price orig_amount,
			  0 orig_amount,
			  0 adj_amount,
			  vv2.sds_status order_status,
			  'CAN' record_type,
			  vv.cancel_reason_code cancel_reason_code,
			  null adj_created_datetime,
			  SYSDATE created_on,
			  od.order_guid order_guid,
			  vv.zone_jump_label_date zone_jump_label_date
		FROM venus.venus vv
		JOIN venus.venus vv2
		  ON vv.venus_order_number = vv2.venus_order_number
		 AND vv2.msg_type = 'FTD'
		 AND vv2.shipping_system = 'FTD WEST'
		 AND vv2.shipped is not null
		JOIN clean.order_details od
		  ON vv2.reference_number = TO_CHAR(od.order_detail_id)
		/*  
		JOIN ftd_apps.vendor_product vp
		  ON vp.vendor_id=vv2.vendor_id
		 AND vp.product_subcode_id=vv.product_id
		*/ 
	  WHERE vv.msg_type IN ('CAN', 'REJ')
	    AND vv.shipping_system = 'FTD WEST'
		 -- delivery date between 1st and last day of prior month
		 AND vv2.delivery_date <= LAST_DAY(in_date) -- last day
	    AND vv2.delivery_date >= ADD_MONTHS((LAST_DAY(trunc(in_date)) + 1), -1) -- first day
		 -- eliminate ADJ
		 AND NOT EXISTS (
			  SELECT 1
				 FROM venus.venus vv2
				WHERE vv2.msg_type IN ('ADJ')
				  AND vv2.venus_order_number = vv.venus_order_number)
	);
	
BEGIN

	begin
		select 	member_number
		into	v_filling_vendor	
		from 	ftd_apps.vendor_master 
		where 	upper(vendor_name) = 'FTD WEST';
	exception
	when others then
		v_filling_vendor := '91-0480AA';
	end;
	
   FOR rec IN cur_ftd_shipped(v_filling_vendor)
	LOOP
	
		-- insert SHIPPED orders
		INSERT INTO CLEAN.WEST_VENDOR_JDE_AUDIT_LOG
			(
			vendor_id,
			filling_vendor,
			venus_order_number,
			external_order_number,
			ship_date,
			delivery_date,
			vendor_sku,
			product_id,
			orig_amount,
			adj_amount,
			order_status,
			record_type,
			cancel_reason_code,
			adj_created_datetime,
			created_on,
			order_guid,
			zone_jump_label_date)
		VALUES
		(
			rec.vendor_id,
			rec.filling_vendor,
			rec.venus_order_number,
			rec.external_order_number,
			rec.ship_date,
			rec.delivery_date,
			rec.vendor_sku,
			rec.product_id,
			rec.orig_amount,
			rec.adj_amount,
			rec.order_status,
			rec.record_type,
			rec.cancel_reason_code,
			rec.adj_created_datetime,
			rec.created_on,
			rec.order_guid,
			rec.zone_jump_label_date
		);
	 
   END LOOP;

 	FOR rec IN cur_adj(v_filling_vendor)
	LOOP
	
		-- Insert adjustments (this is needed by month end statement)
		INSERT INTO CLEAN.WEST_VENDOR_JDE_AUDIT_LOG
			(
			vendor_id,
			filling_vendor,
			venus_order_number,
			external_order_number,
			ship_date,
			delivery_date,
			vendor_sku,
			product_id,
			orig_amount,
			adj_amount,
			order_status,
			record_type,
			cancel_reason_code,
			adj_created_datetime,
			created_on,
			order_guid,
			zone_jump_label_date)
		VALUES
		(
			rec.vendor_id,
			rec.filling_vendor,
			rec.venus_order_number,
			rec.external_order_number,
			rec.ship_date,
			rec.delivery_date,
			rec.vendor_sku,
			rec.product_id,
			rec.orig_amount,
			rec.adj_amount,
			rec.order_status,
			rec.record_type,
			rec.cancel_reason_code,
			rec.adj_created_datetime,
			rec.created_on,
			rec.order_guid,
			rec.zone_jump_label_date
		);
	 
   END LOOP;

 	FOR rec IN cur_can
	LOOP
	
		-- Insert adjustments (this is needed by month end statement)
		INSERT INTO CLEAN.WEST_VENDOR_JDE_AUDIT_LOG
			(
			vendor_id,
			filling_vendor,
			venus_order_number,
			external_order_number,
			ship_date,
			delivery_date,
			vendor_sku,
			product_id,
			orig_amount,
			adj_amount,
			order_status,
			record_type,
			cancel_reason_code,
			adj_created_datetime,
			created_on,
			order_guid,
			zone_jump_label_date)
		VALUES
		(
			rec.vendor_id,
			rec.filling_vendor,
			rec.venus_order_number,
			rec.external_order_number,
			rec.ship_date,
			rec.delivery_date,
			rec.vendor_sku,
			rec.product_id,
			rec.orig_amount,
			rec.adj_amount,
			rec.order_status,
			rec.record_type,
			rec.cancel_reason_code,
			rec.adj_created_datetime,
			rec.created_on,
			rec.order_guid,
			rec.zone_jump_label_date
		);
	 
   END LOOP;
   
   COMMIT;
     
	OUT_STATUS := 'Y';
	OUT_MESSAGE := NULL;

EXCEPTION 
WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.INSERT_JDE_AUDIT_LOG [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END INSERT_WEST_JDE_AUDIT_LOG;


PROCEDURE GET_WEST_JDE_CREDITS
(
IN_DATE			IN 	CLEAN.WEST_VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_CURSOR		OUT TYPES.REF_CURSOR,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for fetching records from CLEAN.WEST_VENDOR_JDE_AUDIT_LOG 

Input:
        in_date									 date

Output:
		cursor						contains clean.west_vendor_jde_audit_log table record
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_ftdw_vendor_code	ftd_apps.vendor_master.member_number%type;
BEGIN

	begin
		select	member_number
		into	v_ftdw_vendor_code
		from	ftd_apps.vendor_master
		where	upper(vendor_name) = 'FTD WEST';
	exception
	when others then
		v_ftdw_vendor_code := '91-0480AA';
	end;

   OPEN out_cursor FOR
	SELECT l.filling_vendor filling_vendor, 
		    SUM(l.orig_amount) amount,
		    COUNT(l.orig_amount) count,
		    vm.accounts_payable_id accounts_payable_id,
		    vm.gl_account_number gl_account_number,
		    'FTDW' company
	  FROM clean.west_vendor_jde_audit_log l
	  JOIN ftd_apps.vendor_master vm
	    ON l.vendor_id = vm.vendor_id
	 WHERE l.record_type = 'SHIP'
	   AND l.created_on <= ADD_MONTHS(LAST_DAY(in_date),1) -- last day of the following month
           AND l.created_on >= (LAST_DAY(TRUNC(in_date)) + 1)  -- first day of the following month
	 	   AND filling_vendor != v_ftdw_vendor_code
	 GROUP BY l.filling_vendor,
	 		 vm.accounts_payable_id,
	 		 vm.gl_account_number,
	 		 vm.company
	 ORDER BY l.filling_vendor;

	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.GET_JDE_CREDITS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;
END;    -- end exception block

END GET_WEST_JDE_CREDITS;

PROCEDURE GET_WEST_JDE_ADJ
(
IN_DATE			IN 	CLEAN.WEST_VENDOR_JDE_AUDIT_LOG.CREATED_ON%TYPE,
OUT_CURSOR		OUT TYPES.REF_CURSOR,
OUT_STATUS		OUT VARCHAR2,
OUT_MESSAGE		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for fetching adj records from CLEAN.WEST_VENDOR_JDE_AUDIT_LOG 
with records created in the following month.

Input:
        in_date									 date

Output:
		cursor							contains clean.west_vendor_jde_audit_log table record
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_ftdw_vendor_code	ftd_apps.vendor_master.member_number%type;
BEGIN

	begin
		select	member_number
		into	v_ftdw_vendor_code
		from	ftd_apps.vendor_master
		where	upper(vendor_name) = 'FTD WEST';
	exception
	when others then
		v_ftdw_vendor_code := '91-0480AA';
	end;

	OPEN out_cursor FOR
	SELECT l.filling_vendor filling_vendor, 
			 SUM(l.adj_amount) amount,
			 COUNT(l.adj_amount) count,
			 vm.accounts_payable_id accounts_payable_id,
			 vm.gl_account_number gl_account_number,
			 'FTDW' company
	  FROM clean.west_vendor_jde_audit_log l
	  JOIN ftd_apps.vendor_master vm
	    ON l.vendor_id = vm.vendor_id
	 WHERE l.record_type = 'ADJ'
	   AND l.created_on <= ADD_MONTHS(LAST_DAY(in_date),1) -- last day of the following month
           AND l.created_on >= (LAST_DAY(TRUNC(in_date)) + 1)  -- first day of the following month
		   AND filling_vendor != v_ftdw_vendor_code
	 GROUP BY l.filling_vendor,
	 		 vm.accounts_payable_id,
	 		 vm.gl_account_number,
	 		 vm.company
	 ORDER BY l.filling_vendor;
	
	out_status := 'Y';
	out_message := NULL;

EXCEPTION WHEN OTHERS THEN
BEGIN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN VENDOR_BATCH_PKG.GET_JDE_ADJ [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END;    -- end exception block

END GET_WEST_JDE_ADJ;

END VENDOR_BATCH_PKG;
/
