CREATE OR REPLACE TRIGGER CLEAN.BAMS_SETTLEMENT_TOTALS_$
	AFTER UPDATE OR DELETE 
	ON CLEAN.BAMS_SETTLEMENT_TOTALS 
	REFERENCING OLD AS OLD NEW AS NEW 
	FOR EACH ROW
	
DECLARE
   V_CURR_TIME TIMESTAMP;
   
BEGIN
   V_CURR_TIME := CURRENT_TIMESTAMP;

	IF UPDATING  THEN
		INSERT INTO CLEAN.BAMS_SETTLEMENT_TOTALS$ 
		(
			SETTLEMENT_TOTAL_ID, FILE_CREATED_DATE_JULIAN, SUBMISSION_ID, BILLING_HEADER_ID, PAYMENT_TOTAL, REFUND_TOTAL, 
			STATUS, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, FILE_NAME, ARCHIVED, OPERATION$,  MODIFIED_TIME$
		) VALUES (
			:OLD.SETTLEMENT_TOTAL_ID, :OLD.FILE_CREATED_DATE_JULIAN, :OLD.SUBMISSION_ID, :OLD.BILLING_HEADER_ID, :OLD.PAYMENT_TOTAL, 
			:OLD.REFUND_TOTAL, :OLD.STATUS, :OLD.CREATED_ON, :OLD.CREATED_BY, :OLD.UPDATED_ON, :OLD.UPDATED_BY, :OLD.FILE_NAME, :OLD.ARCHIVED, 'BEFORE_UPDATE', 
		    V_CURR_TIME
		);
		
		INSERT INTO CLEAN.BAMS_SETTLEMENT_TOTALS$ 
		(
			SETTLEMENT_TOTAL_ID, FILE_CREATED_DATE_JULIAN, SUBMISSION_ID, BILLING_HEADER_ID, PAYMENT_TOTAL, REFUND_TOTAL, 
			STATUS, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, FILE_NAME, ARCHIVED,  OPERATION$,  MODIFIED_TIME$
		) VALUES (
			:NEW.SETTLEMENT_TOTAL_ID, :NEW.FILE_CREATED_DATE_JULIAN, :NEW.SUBMISSION_ID, :NEW.BILLING_HEADER_ID, :NEW.PAYMENT_TOTAL, 
			:NEW.REFUND_TOTAL, :NEW.STATUS, :NEW.CREATED_ON, :NEW.CREATED_BY, :NEW.UPDATED_ON, :NEW.UPDATED_BY, :NEW.FILE_NAME, :NEW.ARCHIVED, 'AFTER_UPDATE', V_CURR_TIME
		);

	ELSIF DELETING  THEN

     INSERT INTO CLEAN.BAMS_SETTLEMENT_TOTALS$ 
		(
			SETTLEMENT_TOTAL_ID, FILE_CREATED_DATE_JULIAN, SUBMISSION_ID, BILLING_HEADER_ID, PAYMENT_TOTAL, REFUND_TOTAL, 
			STATUS, CREATED_ON, CREATED_BY, UPDATED_ON, UPDATED_BY, FILE_NAME, ARCHIVED,  OPERATION$,  MODIFIED_TIME$
		) VALUES (
			:OLD.SETTLEMENT_TOTAL_ID, :OLD.FILE_CREATED_DATE_JULIAN, :OLD.SUBMISSION_ID, :OLD.BILLING_HEADER_ID, :OLD.PAYMENT_TOTAL, 
			:OLD.REFUND_TOTAL, :OLD.STATUS, :OLD.CREATED_ON, :OLD.CREATED_BY, :OLD.UPDATED_ON, :OLD.UPDATED_BY, :OLD.FILE_NAME, :OLD.ARCHIVED, 'DELETE', 
			V_CURR_TIME
		);

   END IF;

END;

/
