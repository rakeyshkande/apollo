CREATE OR REPLACE
PACKAGE BODY clean.EMAIL_XML_ARCHIVE_PKG
AS

PROCEDURE INSERT_EMAIL_XML_ARCHIVE
(
IN_EMAIL_XML_SEQ                IN EMAIL_XML_ARCHIVE.EMAIL_XML_SEQ%TYPE,
IN_EMAIL_XML                    IN EMAIL_XML_ARCHIVE.EMAIL_XML%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the email xml into
        the archive table.  If a duplicate email sequence is found, an
        error will be returned

Input:
        email_xml_seq                   varchar2
        email_xml                       clob
        receive_date                    date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO email_xml_archive
(
        email_xml_seq,
        email_xml,
        receive_date
)
VALUES
(
        in_email_xml_seq,
        in_email_xml,
        sysdate
);

OUT_STATUS := 'Y';

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'Duplicate Email Sequence Found';

WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_EMAIL_XML_ARCHIVE;


PROCEDURE INSERT_EMAIL_XML_SEQ_ERRORS
(
IN_ERROR_EMAIL_XML_SEQ          IN EMAIL_XML_SEQ_ERRORS.ERROR_EMAIL_XML_SEQ%TYPE,
IN_ERROR_EMAIL_XML              IN EMAIL_XML_SEQ_ERRORS.ERROR_EMAIL_XML%TYPE,
IN_ERROR_INDICATOR              IN EMAIL_XML_SEQ_ERRORS.ERROR_INDICATOR%TYPE,
IN_ERROR_DATE                   IN EMAIL_XML_SEQ_ERRORS.ERROR_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting email sequence errors

Input:
        error_email_xml_seq             varchar2
        error_email_xml                 clob
        error_indicator                 char
        error_date                      date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO email_xml_seq_errors
(
        error_xml_seq_error_id,
        error_email_xml_seq,
        error_email_xml,
        error_indicator,
        error_date
)
VALUES
(
        error_xml_seq_error_id_sq.nextval,
        in_error_email_xml_seq,
        in_error_email_xml,
        in_error_indicator,
        sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_EMAIL_XML_SEQ_ERRORS;

PROCEDURE DELETE_EMAIL_XML_SEQ_ERRORS
(
IN_ERROR_EMAIL_XML_SEQ          IN EMAIL_XML_SEQ_ERRORS.ERROR_EMAIL_XML_SEQ%TYPE,
IN_ERROR_INDICATOR              IN EMAIL_XML_SEQ_ERRORS.ERROR_INDICATOR%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the error log for the
        given sequence and type of error

Input:
        error_email_xml_seq             varchar2
        error_indicator                 char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

DELETE FROM email_xml_seq_errors
WHERE   error_email_xml_seq = in_error_email_xml_seq
AND     error_indicator = in_error_indicator;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_EMAIL_XML_SEQ_ERRORS;

PROCEDURE DELETE_MISSING_SEQ_ERRORS
(
IN_ERROR_EMAIL_XML_SEQ          IN EMAIL_XML_SEQ_ERRORS.ERROR_EMAIL_XML_SEQ%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the missing sequence error
        log record for given sequence

Input:
        error_email_xml_seq             varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

DELETE_EMAIL_XML_SEQ_ERRORS (IN_ERROR_EMAIL_XML_SEQ, 'M', OUT_STATUS, OUT_MESSAGE);

END DELETE_MISSING_SEQ_ERRORS;

PROCEDURE UPDATE_CHECKED_EMAIL_SEQ
(
IN_EMAIL_XML_SEQ                IN EMAIL_XML_ARCHIVE.EMAIL_XML_SEQ%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the checked date on the
        email xml record

Input:
        email_xml_seq                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  email_xml_archive
SET     check_date = sysdate
WHERE   email_xml_seq = in_email_xml_seq;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CHECKED_EMAIL_SEQ;


PROCEDURE GET_ALL_UNCHECKED_EMAIL_SEQ
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all of the email sequences
        that have not yet been checked.

Input:
        none

Output:
        cursor containing email sequences

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                email_xml_seq
        FROM    email_xml_archive
        WHERE   check_date is null
	  AND   email_xml_seq like '%-%'
        ORDER BY email_xml_seq;

END GET_ALL_UNCHECKED_EMAIL_SEQ;


FUNCTION GET_MAX_EMAIL_SEQ
(
IN_SEQ_PREFIX                   VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all of the email sequences
        that have not yet been checked based on the prefix.

Input:
        none

Output:
        max email sequences

-----------------------------------------------------------------------------*/

CURSOR max_cur IS
        SELECT
                substr (email_xml_seq, 1, instr (email_xml_seq, '-')) || to_char (max (to_number (substr (email_xml_seq, instr (email_xml_seq, '-') + 1)))) email_xml_seq
        FROM    email_xml_archive
        WHERE   email_xml_seq like in_seq_prefix || '%'
        AND     check_date is not null
        GROUP BY substr (email_xml_seq, 1, instr (email_xml_seq, '-'));

v_max           varchar2(20);

BEGIN

OPEN max_cur;
FETCH max_cur INTO v_max;
CLOSE max_cur;

RETURN v_max;

END GET_MAX_EMAIL_SEQ;


PROCEDURE GET_EMAIL_XML
(
IN_EMAIL_XML_SEQ                IN EMAIL_XML_ARCHIVE.EMAIL_XML_SEQ%TYPE,
OUT_EMAIL_XML                   OUT EMAIL_XML_ARCHIVE.EMAIL_XML%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is returns the email xml for the given id

Input:
        email_xml_seq           integer

Output:
        email xml

-----------------------------------------------------------------------------*/

CURSOR xml_cur IS
        SELECT
                email_xml
        FROM    email_xml_archive
        WHERE   email_xml_seq = in_email_xml_seq;

BEGIN

OPEN xml_cur;
FETCH xml_cur INTO out_email_xml;
CLOSE xml_cur;

END GET_EMAIL_XML;

END;
.
/
