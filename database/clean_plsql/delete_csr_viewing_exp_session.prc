CREATE OR REPLACE
PROCEDURE clean.delete_csr_viewing_exp_session AS
/* This procedure is a front end to clean.csr_viewed_locked_pkg.delete_csr_viewing_exp_session.
   It runs the above procedure, and handles the return code, filing it in frp.system_messages if needed. */

   v_stat              VARCHAR2(1);
   v_mess              VARCHAR2(3000);
   v_system_message_id NUMBER;
   v_out_stat          VARCHAR2(1);
   v_out_mess          VARCHAR2(3000);

BEGIN
   csr_viewed_locked_pkg.delete_csr_viewing_exp_session (v_stat, v_mess);
   IF v_stat = 'N' THEN
      frp.misc_pkg.insert_system_messages(
         in_source             => 'clean.delete_csr_viewing_exp_session',
         in_type               => 'ERROR',
         in_message            => v_mess,
         in_computer           => sys_context('USERENV','HOST'),
         out_system_message_id => v_system_message_id,
         out_status            => v_out_stat,
         out_message           => v_out_mess);
   END IF;
END;
.
/
