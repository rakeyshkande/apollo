CREATE OR REPLACE
TRIGGER clean.gc_coupon_status
AFTER UPDATE
ON clean.gc_coupons
FOR EACH ROW

/*****************************************************************************************
***          Created By : Christy Hu
***          Created On : 04/29/2011
***          Reason : Enhancement 8460: Send reinstated internet GC to website.
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE
   v_order_source gc_coupon_request.order_source%type;
   v_stat  varchar2(10);
   v_mess varchar2(2000);
   v_payload varchar2(2000);
   v_exception EXCEPTION;
   v_exception_txt varchar2(200);
   v_system_message_id FRP.SYSTEM_MESSAGES.SYSTEM_MESSAGE_ID%TYPE;
   
   /***** BEGIN TRACKING Variables ****/
   v_status                  varchar2(1) := 'Y';
   v_message                 varchar2(1000);
   v_action_header_id        number;
   /***** END TRACKING Variables ****/   

BEGIN

   -- check for condition: status change from Refund to Reinstate; Order source is I.
   IF :OLD.gc_coupon_status = 'Refund' and :NEW.gc_coupon_status = 'Reinstate' THEN
   
       select r.order_source
         into v_order_source
         from gc_coupon_request r
        where r.request_number = :NEW.request_number;
        
        IF v_order_source = 'I' THEN
        
		-- post a message
		v_payload := '<?xml version=''1.0'' encoding=''UTF-8''?><gift_cert_request><request_type>Reinstate</request_type>'
			     || '<retry>0</retry><coupon_number>' || :NEW.gc_coupon_number || '</coupon_number></gift_cert_request>';
		events.post_a_message_flex('OJMS.NOVATOR_GIFTCERT',NULL,v_payload,0,v_stat,v_mess);

		if v_stat = 'N' THEN
		    v_exception_txt := 'ERROR posting a JMS message in gc_coupon_status trigger: gc_coupon_number = ' || :NEW.gc_coupon_number || SUBSTR(v_mess,1,200);
		    raise v_exception;
		end if;

		tracking.action_maint_pkg.insert_action_header(8460, 
		    'Send Reinstate gift certificate to website.', 'BACOM',
		    'gc_coupon_status.trg', v_action_header_id, v_status, v_message);

		IF v_status <> 'Y' THEN
		    v_exception_txt := 'ERROR inserting action header gc_coupon_status trigger: gc_coupon_number = ' || :NEW.gc_coupon_number || SUBSTR(v_mess,1,200);
		    raise v_exception;
		END IF;

		-- Tracking detail statements
		tracking.action_maint_pkg.insert_action_detail(v_action_header_id,
			    :NEW.gc_coupon_number, 'clean.gc_coupons.gc_coupon_number', 
			    'gc_coupon_status.trg', v_status, v_message);   

		IF v_status <> 'Y' THEN
		    v_exception_txt := 'ERROR inserting action detail gc_coupon_status trigger: gc_coupon_number = ' || :NEW.gc_coupon_number || SUBSTR(v_mess,1,200);
		    raise v_exception;
		END IF;	

	END IF;
	
	
   END IF;



EXCEPTION    
   WHEN OTHERS THEN  
       if v_exception_txt is null then
           v_exception_txt := 'ERROR OCCURRED IN gc_coupon_status trigger: gc_coupon_number = ' || :NEW.gc_coupon_number ||
       		' [SQLCODE = ' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,200);
       end if;
       
       -- send no page
       frp.misc_pkg.INSERT_SYSTEM_MESSAGES
       (
       IN_SOURCE		=> 'End of Day',
       IN_TYPE                  => 'System Exception',
       IN_MESSAGE               => v_exception_txt,
       IN_COMPUTER              => 'zeus',
       IN_EMAIL_SUBJECT         => 'GC Reinstate Failure',
       OUT_SYSTEM_MESSAGE_ID    => v_system_message_id,
       OUT_STATUS               => v_stat,
       OUT_MESSAGE              => v_mess
       );
       
       -- no handle for sending system message failures.

END gc_coupon_status;
.
/
