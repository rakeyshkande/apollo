create or replace FUNCTION clean.GET_MOUSEOVER_STRING
(
IN_STRING   IN  VARCHAR2,
IN_MAX_LEN  IN  NUMBER
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        Returns a shortened string intended for mouseover displays on the frontend
Input:
        in_string   String to be shortened
        in_max_len  Shorten string to this length
Output:
        Shortened string
-----------------------------------------------------------------------------*/
v_scrub_ind     varchar2 (1);
BEGIN
    IF (length(in_string) > in_max_len) THEN
        return substr(replace(in_string,'"'), 0, in_max_len) || '...';
    ELSE
        return replace(in_string,'"');
    END IF;
END GET_MOUSEOVER_STRING;
/
