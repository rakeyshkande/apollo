CREATE OR REPLACE
TRIGGER clean.trg_queue_ai
AFTER INSERT
ON clean.queue
FOR EACH ROW

/*****************************************************************************************
***          Created By : Divya Desai
***          Created On : 04/12/2006
***              Reason : Enhancement998: Create order state to ensure
***                       orders are completely through system
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE

   lchr_current_status   frp.order_state.status%TYPE := NULL;
   lchr_new_status   frp.order_state.status%TYPE := NULL;
   lchr_trigger_name   frp.order_state_history.trigger_name%TYPE := 'TRG_QUEUE_AI';
   lnum_order_detail_id   clean.queue.order_detail_id%TYPE := :NEW.order_detail_id;

BEGIN

   -- fetch the current status
   lchr_current_status := frp.fun_get_current_status(lnum_order_detail_id);

   -- initialize the status
   IF :NEW.queue_type = 'LP' THEN
      lchr_new_status := 'ORDER_QUEUED_LP';
   ELSIF :NEW.queue_type IN ('ZIP') THEN
      lchr_new_status := 'ORDER_QUEUED_ZIP';
   ELSIF :NEW.queue_type IN ('FTD') THEN
      lchr_new_status := 'ORDER_QUEUED_FTD';
   ELSIF :NEW.queue_type = 'CREDIT' THEN
      lchr_new_status := 'ORDER_QUEUED_CREDIT';
   ELSIF :NEW.queue_type = 'REJ' THEN
      lchr_new_status := 'ORDER_QUEUED_REJ';
   ELSIF :NEW.queue_type = 'CAN' THEN
      lchr_new_status := 'ORDER_QUEUED_CAN';
   END IF;

   -- populate the order state tables
   IF lchr_new_status IS NOT NULL AND
      lnum_order_detail_id IS NOT NULL THEN

      -- insert or update into frp.order_state table
      frp.prc_pop_order_state (
	 lnum_order_detail_id,
	 lchr_new_status);

      -- insert into frp.order_state_history table
      frp.prc_pop_order_state_history (
	 lnum_order_detail_id,
	 lchr_new_status,
	 lchr_current_status,
	 lchr_trigger_name);

   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In CLEAN.TRG_QUEUE_AI: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_queue_ai;
.
/
