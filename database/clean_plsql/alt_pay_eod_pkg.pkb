CREATE OR REPLACE
PACKAGE BODY clean.ALT_PAY_EOD_PKG
AS
/*********************************************************************

History
2007.03.02	CHU 			Creation

********************************************************************/

PROCEDURE GET_PAY_PAL_EOD_RECORDS
(
 IN_DATE		IN DATE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves payments / refunds with payment_type of 'PP' from clean.payments table
	prior to or on the given date that have not been billed.
Input:
        in_date
Output:
        out_payment_cur containing payment data
	out_refund_cur	containing refund data

-----------------------------------------------------------------------------*/
BEGIN

GET_ALT_PAYMENT_EOD_RECORDS(IN_DATE, 'PP', OUT_PAYMENT_CUR, OUT_REFUND_CUR);

END GET_PAY_PAL_EOD_RECORDS;


PROCEDURE GET_BILL_ME_LATER_EOD_RECORDS
(
 IN_DATE		IN DATE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves payments / refunds with payment_type of 'BM' from clean.payments table
	prior to or on the given date that have not been billed.
Input:
        in_date
Output:
        out_payment_cur containing payment data
	out_refund_cur	containing refund data

-----------------------------------------------------------------------------*/
BEGIN

GET_ALT_PAYMENT_EOD_RECORDS(IN_DATE, 'BM', OUT_PAYMENT_CUR, OUT_REFUND_CUR);

END GET_BILL_ME_LATER_EOD_RECORDS;


PROCEDURE GET_GIFT_CARD_EOD_RECORDS
(
 IN_DATE            IN DATE,
 OUT_PAYMENT_CUR    OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
    Retrieves payments / refunds with payment_type of 'GD' from clean.payments table
    prior to or on the given date that have not been billed.
Input:
    in_date
Output:
    out_payment_cur containing payment data
    out_refund_cur  containing refund data

-----------------------------------------------------------------------------*/
BEGIN

GET_ALT_PAYMENT_EOD_RECORDS(IN_DATE, 'GD', OUT_PAYMENT_CUR, OUT_REFUND_CUR);

END GET_GIFT_CARD_EOD_RECORDS;


PROCEDURE GET_ALT_PAYMENT_EOD_RECORDS
(
 IN_DATE		IN DATE,
 IN_PAYMENT_TYPE        IN PAYMENTS.PAYMENT_TYPE%TYPE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves payments / refunds with specified payment_type from clean.payments table
	prior to or on the given date that have not been billed.
Input:
        in_date
Output:
        out_payment_cur containing payment data
	out_refund_cur	containing refund data

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_payment_cur FOR
        SELECT DISTINCT o.master_order_number,
             		p.payment_id,
             		'P' payment_indicator
		FROM    clean.payments p
		JOIN    clean.orders o
		ON      p.order_guid=o.order_guid
		JOIN    clean.order_details od
		ON      o.order_guid = od.order_guid
		AND     od.order_disp_code IN ('Processed','Shipped','Printed')
		WHERE 	(p.created_on < TRUNC(in_date) + 1 and p.created_on > TRUNC(in_date) - 180)
		AND p.bill_status = 'Unbilled'
		AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))
		AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))		
		AND     p.credit_amount > 0
		AND     p.payment_indicator='P'
    AND (p.additional_bill_id is null or p.payment_type = 'GD')
		AND     p.payment_type = IN_PAYMENT_TYPE
		AND (p.route is null or (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'N'))
		ORDER BY o.master_order_number;

  OPEN out_refund_cur FOR
	SELECT DISTINCT o.master_order_number,
	                p.payment_id,
	                'R' payment_indicator
		FROM 	clean.payments p
		JOIN	clean.orders o
		ON	o.order_guid=p.order_guid
		JOIN    clean.refund r
		ON      p.refund_id=r.refund_id
		JOIN    clean.order_details od
		ON      od.order_detail_id=r.order_detail_id
		AND 	od.order_disp_code IN ('Processed','Shipped','Printed')
		WHERE (r.created_on < TRUNC(in_date) + 1 and r.created_on > TRUNC(in_date) - 180)
		  AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))
		  AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))			
	          AND r.refund_status = 'Unbilled'
	          AND p.debit_amount > 0
	          AND p.payment_type = IN_PAYMENT_TYPE
	          AND p.payment_indicator = 'R'
	          AND (p.route is null or (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'N'))
	          ORDER BY o.master_order_number;

END GET_ALT_PAYMENT_EOD_RECORDS;

PROCEDURE GET_MILES_EOD_RECORDS
(
 IN_DATE		IN DATE,
 IN_PAYMENT_TYPE        IN PAYMENTS.PAYMENT_TYPE%TYPE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves miles payments / refunds from clean.payments table
	prior to or on the given date that have not been billed.
Input:
        in_date
Output:
        out_payment_cur containing payment data
	out_refund_cur	containing refund data

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_payment_cur FOR
        SELECT DISTINCT o.master_order_number,
             		p.payment_id,
             		'P' payment_indicator
		FROM    clean.payments p
		JOIN    clean.orders o
		ON      p.order_guid=o.order_guid
		JOIN    clean.order_details od
		ON      o.order_guid = od.order_guid
		AND     od.order_disp_code IN ('Processed','Shipped','Printed')
		JOIN	clean.ap_accounts aa
		ON	aa.ap_account_id = p.ap_account_id
		WHERE 	(p.created_on < TRUNC(in_date) + 1 and p.created_on > TRUNC(in_date) - 180)
		AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))
		AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))			
		AND     p.credit_amount > 0
		AND     p.payment_indicator='P'
		AND	p.additional_bill_id is null
		AND     p.payment_type = IN_PAYMENT_TYPE
		--AND	p.auth_number IS NOT NULL
		AND	p.miles_points_credit_amt > 0
		AND	p.bill_status = 'Unbilled'
		ORDER BY o.master_order_number
		;

  OPEN out_refund_cur FOR
	SELECT DISTINCT o.master_order_number,
	                p.payment_id,
	                'R' payment_indicator
		FROM 	clean.payments p
		JOIN	clean.orders o
		ON	o.order_guid=p.order_guid
		JOIN    clean.refund r
		ON      p.refund_id=r.refund_id
		JOIN    clean.order_details od
		ON      od.order_detail_id=r.order_detail_id
		AND 	od.order_disp_code IN ('Processed','Shipped','Printed')
		WHERE (r.created_on < TRUNC(in_date) + 1 and r.created_on > TRUNC(in_date) - 180)
		  AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))
		  AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))			
	          AND r.refund_status = 'Unbilled'
	          AND p.debit_amount > 0
	          AND p.payment_type = IN_PAYMENT_TYPE
	          AND p.payment_indicator = 'R'
	          AND p.miles_points_debit_amt > 0
	          ORDER BY o.master_order_number
	          ;

END GET_MILES_EOD_RECORDS;

PROCEDURE INSERT_AP_BILLING_HEADER
(
 IN_PAYMENT_METHOD_ID		IN  ALT_PAY_BILLING_HEADER.PAYMENT_METHOD_ID%TYPE,
 IN_BATCH_DATE        		IN  ALT_PAY_BILLING_HEADER.BILLING_BATCH_DATE%TYPE,
 IN_PROCESSED_FLAG         	IN  ALT_PAY_BILLING_HEADER.PROCESSED_FLAG%TYPE,
 IN_DISPATCHED_COUNT		IN  ALT_PAY_BILLING_HEADER.DISPATCHED_COUNT%TYPE,
 IN_PROCESSED_COUNT		IN  ALT_PAY_BILLING_HEADER.PROCESSED_COUNT%TYPE,
 OUT_BILLING_HEADER_ID  OUT ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Insert into alt_pay_billing_header table
Input:
        in_payment_method id payment method id
        in_batch_date batch date
        in_processed_flag processed flag
        in_dispatched_cound dispatched count
        in_processed_count processed count

Output:
        out_billing_header_id alt_pay_billing_header_id generated
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := 'N';
SELECT ALT_PAY_BILLING_HEADER_SQ.NEXTVAL INTO out_billing_header_id FROM DUAL;

INSERT INTO alt_pay_billing_header
(
        alt_pay_billing_header_id,
        payment_method_id,
        processed_flag,
        dispatched_count,
        processed_count,
        created_on,
        billing_batch_date
)
VALUES
(
        out_billing_header_id,
        in_payment_method_id,
        in_processed_flag,
        in_dispatched_count,
        in_processed_count,
        SYSDATE,
        in_batch_date
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AP_BILLING_HEADER;

PROCEDURE INSERT_AP_BH_NO_DUP
(
 IN_PAYMENT_METHOD_ID		IN  ALT_PAY_BILLING_HEADER.PAYMENT_METHOD_ID%TYPE,
 IN_BATCH_DATE        		IN  ALT_PAY_BILLING_HEADER.BILLING_BATCH_DATE%TYPE,
 IN_PROCESSED_FLAG         	IN  ALT_PAY_BILLING_HEADER.PROCESSED_FLAG%TYPE,
 IN_DISPATCHED_COUNT		IN  ALT_PAY_BILLING_HEADER.DISPATCHED_COUNT%TYPE,
 IN_PROCESSED_COUNT		IN  ALT_PAY_BILLING_HEADER.PROCESSED_COUNT%TYPE,
 OUT_BILLING_HEADER_ID  	OUT ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	The procedure checks if an entry already exists for the payment method if
	and the batch date. If it does not then it insert into alt_pay_billing_header table
Input:
        in_payment_method id payment method id
        in_batch_date batch date
        in_processed_flag processed flag
        in_dispatched_cound dispatched count
        in_processed_count processed count

Output:
        out_billing_header_id alt_pay_billing_header_id generated
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

BEGIN
	OUT_STATUS := 'Y';
	
	select alt_pay_billing_header_id
	into OUT_BILLING_HEADER_ID
	from alt_pay_billing_header
	where payment_method_id = IN_PAYMENT_METHOD_ID	
	and billing_batch_date = trunc(IN_BATCH_DATE)
	and processed_flag = 'N';
	
	EXCEPTION 
	WHEN NO_DATA_FOUND THEN
		INSERT_AP_BILLING_HEADER
		(
		 IN_PAYMENT_METHOD_ID		=>IN_PAYMENT_METHOD_ID,
		 IN_BATCH_DATE        		=>trunc(IN_BATCH_DATE),
		 IN_PROCESSED_FLAG         	=>IN_PROCESSED_FLAG,
		 IN_DISPATCHED_COUNT		=>IN_DISPATCHED_COUNT,
		 IN_PROCESSED_COUNT		=>IN_PROCESSED_COUNT,
		 OUT_BILLING_HEADER_ID  	=>OUT_BILLING_HEADER_ID,
		 OUT_STATUS			=>OUT_STATUS,
 		 OUT_MESSAGE			=>OUT_MESSAGE
		);
	WHEN OTHERS THEN
        	OUT_STATUS := 'N';
        	OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);



END INSERT_AP_BH_NO_DUP;


PROCEDURE INSERT_AP_BILLING_DETAIL_PP
(
 IN_BILLING_HEADER_ID		IN  ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 IN_PAYMENT_ID			IN  ALT_PAY_BILLING_DETAIL_PP.PAYMENT_ID%TYPE,
 IN_PAYMENT_CODE        	IN  ALT_PAY_BILLING_DETAIL_PP.PAYMENT_CODE%TYPE,
 IN_MASTER_ORDER_NUMBER         IN  ALT_PAY_BILLING_DETAIL_PP.MASTER_ORDER_NUMBER%TYPE,
 IN_REQ_TRANSACTION_TXT		IN  ALT_PAY_BILLING_DETAIL_PP.REQ_TRANSACTION_TXT%TYPE,
 IN_REQ_AMT			IN  ALT_PAY_BILLING_DETAIL_PP.REQ_AMT%TYPE,
 OUT_BILLING_DETAIL_ID  	OUT ALT_PAY_BILLING_DETAIL_PP.ALT_PAY_BILLING_DETAIL_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Insert into alt_pay_billing_detail table
Input:
        in_payment_id payment id
        in_payment_code	payment or refund indicator (P or R)
        in_master_order_number master order number
        in_req_transaction_txt request transaction id
        in_req_amt request transaction amount

Output:
        out_billing_detail_id alt_pay_billing_detail_id generated
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := 'N';
SELECT ALT_PAY_BILLING_DETAIL_PP_SQ.NEXTVAL INTO out_billing_detail_id FROM DUAL;

INSERT INTO alt_pay_billing_detail_pp
(
        alt_pay_billing_detail_id,
        alt_pay_billing_header_id,
        payment_id,
        payment_code,
        master_order_number,
        req_transaction_txt,
        req_amt,
        created_on,
        updated_on
)
VALUES
(
        out_billing_detail_id,
        in_billing_header_id,
        in_payment_id,
        in_payment_code,
        in_master_order_number,
        in_req_transaction_txt,
        in_req_amt,
        SYSDATE,
        SYSDATE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AP_BILLING_DETAIL_PP;


PROCEDURE INSERT_AP_BILLING_DETAIL_BM
(
 IN_BILLING_HEADER_ID		IN  ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 IN_PAYMENT_ID			IN  ALT_PAY_BILLING_DETAIL_BM.PAYMENT_ID%TYPE,
 IN_PAYMENT_CODE        	IN  ALT_PAY_BILLING_DETAIL_BM.PAYMENT_CODE%TYPE,
 IN_MASTER_ORDER_NUMBER         IN  ALT_PAY_BILLING_DETAIL_BM.MASTER_ORDER_NUMBER%TYPE,
 IN_ACCOUNT_NUMBER		IN  ALT_PAY_BILLING_DETAIL_BM.ACCOUNT_NUMBER%TYPE,
 IN_AUTH_NUMBER		        IN  ALT_PAY_BILLING_DETAIL_BM.AUTH_NUMBER%TYPE,
 IN_AUTH_ORDER_NUMBER		IN  ALT_PAY_BILLING_DETAIL_BM.AUTH_ORDER_NUMBER%TYPE,
 IN_SETTLEMENT_AMT  	        IN  ALT_PAY_BILLING_DETAIL_BM.SETTLEMENT_AMT%TYPE,
 IN_AUTH_DATE   	        IN  ALT_PAY_BILLING_DETAIL_BM.AUTH_DATE%TYPE,
 OUT_BILLING_DETAIL_ID  	OUT ALT_PAY_BILLING_DETAIL_BM.ALT_PAY_BILLING_DETAIL_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Insert into alt_pay_billing_detail_bm table

Output:
        out_billing_detail_id alt_pay_billing_detail_id generated
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

   v_key_name alt_pay_billing_detail_bm.key_name%type;

BEGIN

OUT_STATUS := 'N';
v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

SELECT ALT_PAY_BILLING_DETAIL_BM_SQ.NEXTVAL INTO out_billing_detail_id FROM DUAL;

INSERT INTO alt_pay_billing_detail_bm
(
        alt_pay_billing_detail_id,
        alt_pay_billing_header_id,
        payment_id,
        payment_code,
        master_order_number,
        account_number,
        auth_number,
        auth_order_number,
        settlement_amt,
        key_name,
        auth_date,
        created_on,
        updated_on,
        created_by,
        updated_by
)
VALUES
(
        out_billing_detail_id,
        in_billing_header_id,
        in_payment_id,
        in_payment_code,
        in_master_order_number,
        global.encryption.encrypt_it(in_account_number,v_key_name),
        in_auth_number,
        in_auth_order_number,
        in_settlement_amt,
        v_key_name,
        in_auth_date,
        SYSDATE,
        SYSDATE,
        'SYS',
        'SYS'
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AP_BILLING_DETAIL_BM;


PROCEDURE INSERT_AP_BILLING_DETAIL_GD
(
 IN_BILLING_HEADER_ID       IN  ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 IN_PAYMENT_ID              IN  ALT_PAY_BILLING_DETAIL_GD.PAYMENT_ID%TYPE,
 IN_PAYMENT_CODE            IN  ALT_PAY_BILLING_DETAIL_GD.PAYMENT_CODE%TYPE,
 IN_MASTER_ORDER_NUMBER     IN  ALT_PAY_BILLING_DETAIL_GD.MASTER_ORDER_NUMBER%TYPE,
 IN_ACCOUNT_NUMBER          IN  ALT_PAY_BILLING_DETAIL_GD.ACCOUNT_NUMBER%TYPE,
 IN_PIN                     IN  ALT_PAY_BILLING_DETAIL_GD.PIN%TYPE,
 IN_AUTH_NUMBER             IN  ALT_PAY_BILLING_DETAIL_GD.AUTH_NUMBER%TYPE,
 IN_SETTLEMENT_AMT          IN  ALT_PAY_BILLING_DETAIL_GD.SETTLEMENT_AMT%TYPE,
 IN_AUTH_DATE               IN  ALT_PAY_BILLING_DETAIL_GD.AUTH_DATE%TYPE,
 IN_STATUS                  IN  ALT_PAY_BILLING_DETAIL_GD.STATUS%TYPE,
 IN_ERROR_TXT               IN  ALT_PAY_BILLING_DETAIL_GD.ERROR_TXT%TYPE,
 OUT_BILLING_DETAIL_ID      OUT ALT_PAY_BILLING_DETAIL_GD.ALT_PAY_BILLING_DETAIL_ID%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
    Insert into alt_pay_billing_detail_gd table

Output:
    out_billing_detail_id alt_pay_billing_detail_id generated
    out_status status of operation
    out_message error message

-----------------------------------------------------------------------------*/

   v_key_name alt_pay_billing_detail_gd.key_name%type;

BEGIN

OUT_STATUS := 'N';
v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

SELECT ALT_PAY_BILLING_DETAIL_GD_SQ.NEXTVAL INTO out_billing_detail_id FROM DUAL;

INSERT INTO alt_pay_billing_detail_gd
(
        alt_pay_billing_detail_id,
        alt_pay_billing_header_id,
        payment_id,
        payment_code,
        master_order_number,
        account_number,
        pin,
        auth_number,
        settlement_amt,
        key_name,
        auth_date,
        status,
        error_txt,
        created_on,
        updated_on,
        created_by,
        updated_by
)
VALUES
(
        out_billing_detail_id,
        in_billing_header_id,
        in_payment_id,
        in_payment_code,
        in_master_order_number,
        global.encryption.encrypt_it(in_account_number,v_key_name),
        global.encryption.encrypt_it(in_pin,v_key_name),
        in_auth_number,
        in_settlement_amt,
        v_key_name,
        in_auth_date,
        in_status,
        in_error_txt,
        SYSDATE,
        SYSDATE,
        'SYS',
        'SYS'
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AP_BILLING_DETAIL_GD;


PROCEDURE INSERT_AP_BILLING_DETAIL_UA
(
 IN_BILLING_HEADER_ID		IN  ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 IN_MANUAL_ENTRY_FLAG           IN  ALT_PAY_BILLING_DETAIL_UA.MANUAL_ENTRY_FLAG%TYPE,
 IN_PAYMENT_ID			IN  ALT_PAY_BILLING_DETAIL_UA.PAYMENT_ID%TYPE,
 IN_REQ_ACCOUNT_NUMBER		IN  ALT_PAY_BILLING_DETAIL_UA.REQ_MEMBERSHIP_NUMBER_TXT%TYPE,
 IN_REQ_MILES_AMT		IN  ALT_PAY_BILLING_DETAIL_UA.REQ_MILES_AMT%TYPE,
 IN_REQ_BONUS_CODE		IN  ALT_PAY_BILLING_DETAIL_UA.REQ_BONUS_CODE_TXT%TYPE,
 IN_REQ_BONUS_TYPE  	        IN  ALT_PAY_BILLING_DETAIL_UA.REQ_BONUS_TYPE_TXT%TYPE,
 IN_REQ_MILE_INDICATOR  	IN  ALT_PAY_BILLING_DETAIL_UA.REQ_MILE_INDICATOR_TXT%TYPE,
 IN_RES_SUCCESS_FLAG		IN  ALT_PAY_BILLING_DETAIL_UA.RES_SUCCESS_FLAG%TYPE,
 IN_RES_CODE			IN  ALT_PAY_BILLING_DETAIL_UA.RES_CODE_TXT%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Insert into alt_pay_billing_detail_ua table

Output:
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

v_key_name alt_pay_billing_detail_ua.key_name%type;
v_billing_detail_id ALT_PAY_BILLING_DETAIL_UA.ALT_PAY_BILLING_DETAIL_ID%TYPE;
v_payment_type CLEAN.PAYMENTS.PAYMENT_TYPE%TYPE;
v_order_guid CLEAN.PAYMENTS.ORDER_GUID%TYPE;
v_redemption_date DATE;

BEGIN

OUT_STATUS := 'N';


SELECT ALT_PAY_BILLING_DETAIL_UA_SQ.NEXTVAL INTO v_billing_detail_id FROM DUAL;

SELECT payment_type,order_guid
INTO v_payment_type, v_order_guid
FROM payments
where payment_id = in_payment_id;

v_key_name := ftd_apps.misc_pkg.get_alt_pay_ingrian_key_name(v_payment_type);

-- in_req_mile_indicator = 'D' indicates deduct miles; versus 'C' for credit miles.
IF in_req_mile_indicator = 'D' THEN

	v_redemption_date := trunc(SYSDATE);
	
	-- Update payment auth date.
	update payments
	set auth_date = v_redemption_date
	where payment_id = in_payment_id;
ELSE
	select auth_date
	into v_redemption_date
	from payments p
	where p.payment_indicator = 'P'
	and p.additional_bill_id is null
	and p.payment_type = v_payment_type
	and p.order_guid = v_order_guid;
			     
END IF;

INSERT INTO alt_pay_billing_detail_ua
(
	alt_pay_billing_detail_id,
	alt_pay_billing_header_id,
	manual_entry_flag,
	payment_id,
	master_order_number,
	req_membership_number_txt,
	key_name,
	req_miles_amt,
	req_bonus_code_txt,
	req_bonus_type_txt,
	req_mile_indicator_txt,
	req_redemption_date,
	res_success_flag,
	res_code_txt,
	created_on,
	updated_on,
	created_by,
	updated_by
)
SELECT  v_billing_detail_id,
	in_billing_header_id,
	in_manual_entry_flag,
	in_payment_id,
	o.master_order_number,
	global.encryption.encrypt_it(in_req_account_number,v_key_name),
	v_key_name,
	in_req_miles_amt,
	in_req_bonus_code,
	in_req_bonus_type,
	in_req_mile_indicator,
	nvl(v_redemption_date,sysdate),
	in_res_success_flag,
	in_res_code, 
	SYSDATE,
	SYSDATE,
	'SYS',
	'SYS'
FROM	clean.orders o
JOIN	clean.payments p
ON	o.order_guid = p.order_guid
WHERE	p.payment_id = in_payment_id;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_AP_BILLING_DETAIL_UA;


PROCEDURE GET_AP_BILLING_DETAIL_BM
(
 OUT_BILLING_DETAIL_CUR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Get unprocessed BillMeLater records from alt_pay_billing_detail_bm table.
        
        Note we don't include alt_pay_billing_detail_id or ...header_id in the query.
        Since there may have been a failure in prior EOD runs there may be 
        multiple detail records representing the same orders - so by doing a 
        "select distinct" without these columns we won't get duplicates.

Output:
        out_billing_detail_cur containing BML billing detail data

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_billing_detail_cur FOR
        SELECT DISTINCT  bd.payment_id,
                         bd.payment_code,
                         bd.master_order_number,
                         global.encryption.decrypt_it(bd.account_number,bd.key_name) account_number,
                         bd.auth_number,
                         bd.auth_order_number,
                         bd.settlement_amt,
                         bd.auth_date
                FROM     clean.alt_pay_billing_detail_bm bd,
                         clean.alt_pay_billing_header bh
                WHERE    bh.processed_flag = 'N'
                AND      bh.payment_method_id = 'BM'
                AND      bd.alt_pay_billing_header_id = bh.alt_pay_billing_header_id;
                
END GET_AP_BILLING_DETAIL_BM;


PROCEDURE GET_AP_BILLING_DETAIL_GD
(
 OUT_BILLING_DETAIL_CUR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
    Get unprocessed Gift Card records from alt_pay_billing_detail_gd table.
        
        Note we don't include alt_pay_billing_detail_id or ...header_id in the query.
        Since there may have been a failure in prior EOD runs there may be 
        multiple detail records representing the same orders - so by doing a 
        "select distinct" without these columns we won't get duplicates.

Output:
        out_billing_detail_cur containing Gift Card billing detail data

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_billing_detail_cur FOR
        SELECT DISTINCT  bd.payment_id,
                         bd.payment_code,
                         bd.master_order_number,
                         global.encryption.decrypt_it(bd.account_number,bd.key_name) account_number,
                         global.encryption.decrypt_it(bd.pin,bd.key_name) pin,
                         bd.auth_number,
                         bd.settlement_amt,
                         bd.auth_date,
                         bd.status,
                         bd.error_txt
                FROM     clean.alt_pay_billing_detail_gd bd,
                         clean.alt_pay_billing_header bh
                WHERE    bh.processed_flag = 'N'
                AND      bh.payment_method_id = 'GD'
                AND      bd.alt_pay_billing_header_id = bh.alt_pay_billing_header_id;
                
END GET_AP_BILLING_DETAIL_GD;


PROCEDURE GET_AP_BILLING_DETAIL_UA
(
 OUT_BILLING_DETAIL_CUR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Get unprocessed UA records from alt_pay_billing_detail_ua table.

Output:
        out_billing_detail_cur containing BML billing detail data

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_billing_detail_cur FOR
  SELECT	
                global.encryption.decrypt_it(bd.req_membership_number_txt,bd.key_name) account_number,
                bd.req_redemption_date,
                bd.req_miles_amt,
                c.last_name,
                od.external_order_number
        FROM	clean.alt_pay_billing_detail_ua bd,
            	clean.alt_pay_billing_header bh,
                clean.order_details od,
                clean.orders o,
                clean.customer c
        WHERE	bh.processed_flag = 'N'
        AND		bh.payment_method_id = 'UA'
        AND		bd.req_mile_indicator_txt = 'C' -- only refunds
        AND		bd.alt_pay_billing_header_id = bh.alt_pay_billing_header_id
        AND     bd.master_order_number = o.master_order_number
        AND     o.order_guid = od.order_guid
        AND     o.CUSTOMER_ID = c.customer_id;
                
END GET_AP_BILLING_DETAIL_UA;


PROCEDURE UPDATE_AP_BILLING_DETAIL_PP
(
 IN_BILLING_DETAIL_ID		IN  ALT_PAY_BILLING_DETAIL_PP.ALT_PAY_BILLING_DETAIL_ID%TYPE,
 IN_RES_ACK_TXT			IN  ALT_PAY_BILLING_DETAIL_PP.RES_ACK_TXT%TYPE,
 IN_RES_TRANSACTION_TXT        	IN  ALT_PAY_BILLING_DETAIL_PP.RES_TRANSACTION_TXT%TYPE,
 IN_RES_CORRELATION_TXT		IN  ALT_PAY_BILLING_DETAIL_PP.RES_CORRELATION_TXT%TYPE,
 IN_ACK_ERROR_TXT         	IN  ALT_PAY_BILLING_DETAIL_PP.ACK_ERROR_TXT%TYPE,
 IN_RES_GROSS_AMT		IN  ALT_PAY_BILLING_DETAIL_PP.RES_GROSS_AMT%TYPE,
 IN_RES_FEE_AMT			IN  ALT_PAY_BILLING_DETAIL_PP.RES_FEE_AMT%TYPE,
 IN_RES_NET_AMT  		IN  ALT_PAY_BILLING_DETAIL_PP.RES_NET_AMT%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Update alt_pay_billing_detail table
Input:
        in_billing_detail_id	number
        in_res_ack_txt		varchar2
        in_res_transaction_txt	varchar2
	in_res_correlation_txt  varchar2
        in_ack_error_txt	varchar2
        in_res_gross_amt	number
        in_res_fee_amt		number
        in_res_net_amt		number

Output:
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := 'N';

UPDATE alt_pay_billing_detail_pp
SET
        res_ack_txt = in_res_ack_txt,
        res_transaction_txt = in_res_transaction_txt,
	res_correlation_txt = in_res_correlation_txt,
        ack_error_txt = in_ack_error_txt,
        res_gross_amt = in_res_gross_amt,
        res_fee_amt = in_res_fee_amt,
        res_net_amt = in_res_net_amt,
        updated_on = sysdate
WHERE   alt_pay_billing_detail_id = in_billing_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END UPDATE_AP_BILLING_DETAIL_PP;


PROCEDURE INCREMENT_PROCESSED_COUNT
(
 IN_BILLING_HEADER_ID		IN ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Increment alt_pay_billing_header.processed_count by 1.
Input:
        in_billing_header_id record to be incremented
Output:
        out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/
BEGIN

out_status := 'N';

UPDATE alt_pay_billing_header
   SET processed_count = processed_count+1
 WHERE alt_pay_billing_header_id = in_billing_header_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INCREMENT_PROCESSED_COUNT;


PROCEDURE INCREMENT_AND_CHECK_PROC_COUNT
(
 IN_BILLING_HEADER_ID		IN ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 OUT_IS_EQUAL   		OUT VARCHAR2,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Increment alt_pay_billing_header.processed_count by 1 and check if
        it equals the dispatched_count. 
Input:
        in_billing_header_id record to be incremented
Output:
        out_is_equal   Y if processed_count now equals dispatched count
        out_status     Status of operation
	out_message    Error message

-----------------------------------------------------------------------------*/
BEGIN

out_status   := 'N';
out_is_equal := 'N';

UPDATE alt_pay_billing_header
SET processed_count = processed_count+1
WHERE alt_pay_billing_header_id = in_billing_header_id;

SELECT decode((dispatched_count - processed_count), 0, 'Y','N')
INTO   OUT_IS_EQUAL
FROM   alt_pay_billing_header
WHERE  alt_pay_billing_header_id = in_billing_header_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS  := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INCREMENT_AND_CHECK_PROC_COUNT;


PROCEDURE UPDATE_PROCESSED_FLAG
(
 IN_PAYMENT_METHOD_ID		IN ALT_PAY_BILLING_HEADER.PAYMENT_METHOD_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Update alt_pay_billing_header.processed_flag for all header records
        of the specified type that are not currently marked as processed
Input:
        in_payment_method_id 
Output:
        out_status     Status of operation
	out_message    Error message

-----------------------------------------------------------------------------*/
BEGIN

   UPDATE alt_pay_billing_header
   SET processed_flag = 'Y'
   WHERE processed_flag = 'N'
   AND payment_method_id = in_payment_method_id
   AND billing_batch_date < trunc(sysdate);
   
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS  := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);   

END UPDATE_PROCESSED_FLAG;


PROCEDURE GET_GD_ERROR_SUMMARY
(
 IN_COMPLETE_SUMMARY_FLAG   IN  VARCHAR2,
 OUT_PMT_ERROR_COUNT        OUT NUMBER,
 OUT_REFUND_ERROR_COUNT     OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
    Returns failed GiftCard transactions.
Input:
        in_conplete_summary_flag 'Y' to get all summary; 'N' to get summary for the latest run.
        This flag applies to out_pmt_error_count and out_refund_error_count only.
Output:
        out_pmt_error_count number of failed payment settlement calls
        out_refund_error_count number of failed refund settlement calls

-----------------------------------------------------------------------------*/

v_billing_header_id_pred varchar2(256);
v_sql varchar2(256);

BEGIN

IF in_complete_summary_flag = 'Y' THEN
    SELECT count(*)
      INTO out_pmt_error_count
      FROM alt_pay_billing_detail_gd
     WHERE payment_code='P'
           AND status = 'Failure';
           
        SELECT count(*)
      INTO out_refund_error_count
      FROM alt_pay_billing_detail_gd
     WHERE payment_code='R'
           AND status = 'Failure';
           
ELSE
    SELECT count(*)
      INTO out_pmt_error_count
      FROM alt_pay_billing_detail_gd
     WHERE payment_code='P'
           AND status = 'Failure'
           AND alt_pay_billing_header_id =
        (select max(alt_pay_billing_header_id)
           from clean.alt_pay_billing_header
          where payment_method_id = 'GD'
            and created_on > sysdate-3);
           
        SELECT count(*)
      INTO out_refund_error_count
      FROM alt_pay_billing_detail_gd
     WHERE payment_code='R'
           AND status = 'Failure'
           AND alt_pay_billing_header_id =
        (select max(alt_pay_billing_header_id)
           from clean.alt_pay_billing_header
          where payment_method_id = 'GD'
            and created_on > sysdate-3);

END IF;

END GET_GD_ERROR_SUMMARY;


PROCEDURE GET_AP_ERROR_SUMMARY
(
 IN_COMPLETE_SUMMARY_FLAG	IN  VARCHAR2,
 OUT_PMT_ERROR_COUNT		OUT NUMBER,
 OUT_REFUND_ERROR_COUNT		OUT NUMBER,
 OUT_AUTH_ERROR_COUNT		OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
	Returns failed PayPal transactions.
Input:
        in_conplete_summary_flag 'Y' to get all summary; 'N' to get summary for the latest run.
        This flag applies to out_pmt_error_count and out_refund_error_count only.
Output:
        out_pmt_error_count number of failed payment settlement calls
        out_refund_error_count number of failed refund settlement calls
        out_auth_error_count number of PayPal payments received without authorization

-----------------------------------------------------------------------------*/

v_billing_header_id_pred varchar2(256);
v_sql varchar2(256);

BEGIN

IF in_complete_summary_flag = 'Y' THEN
	SELECT count(*)
	  INTO out_pmt_error_count
	  FROM alt_pay_billing_detail_pp
	 WHERE payment_code='P'
           AND res_transaction_txt is null;
           
        SELECT count(*)
	  INTO out_refund_error_count
	  FROM alt_pay_billing_detail_pp
	 WHERE payment_code='R'
           AND res_transaction_txt is null;
           
ELSE
	SELECT count(*)
	  INTO out_pmt_error_count
	  FROM alt_pay_billing_detail_pp
	 WHERE payment_code='P'
           AND res_transaction_txt is null
           AND alt_pay_billing_header_id =
	   	(select max(alt_pay_billing_header_id)
		from clean.alt_pay_billing_header);
           
        SELECT count(*)
	  INTO out_refund_error_count
	  FROM alt_pay_billing_detail_pp
	 WHERE payment_code='R'
           AND res_transaction_txt is null
           AND alt_pay_billing_header_id =
           	(select max(alt_pay_billing_header_id)
		from clean.alt_pay_billing_header);

END IF;

SELECT count(*)
  INTO out_auth_error_count
  FROM payments p
  JOIN order_details od
    ON od.order_guid=p.order_guid
  WHERE p.payment_indicator='P'
   AND p.ap_auth_txt is null
   AND p.payment_type='PP'
   AND p.additional_bill_id is null
   AND p.bill_status='Unbilled';

END GET_AP_ERROR_SUMMARY;


PROCEDURE GET_AP_PAYMENT_INFO
(
 IN_PAYMENT_ID			IN  PAYMENTS.PAYMENT_ID%TYPE,
 IN_PAYMENT_INDICATOR		IN  PAYMENTS.PAYMENT_INDICATOR%TYPE,
 OUT_PAYMENT_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Returns payment information for the given payment id.
        Note that returned values: acq_reference_number, ap_account_txt, and order_date
        are currently only used for BillMeLater EOD processing.
Input:
        in_payment_id payment id

Output:
        out_payment_cur cursor with payment info

-----------------------------------------------------------------------------*/


BEGIN

IF in_payment_indicator = 'P' THEN
-- payment
	OPEN out_payment_cur FOR
        	SELECT  p.order_guid,
			p.credit_amount amount,
			p.ap_auth_txt req_transaction_id,
			null refund_id,
                        p.acq_reference_number,
                        global.encryption.decrypt_it(ap.ap_account_txt,ap.key_name) ap_account_txt,
                        o.order_date auth_date,
                        p.miles_points_credit_amt miles_points_amt,
                        p.auth_number
		  FROM  payments p, ap_accounts ap, orders o
		 WHERE  p.payment_id = in_payment_id
                   AND  o.order_guid = p.order_guid
                   AND  ap.ap_account_id = p.ap_account_id;
ELSE
-- refund
	OPEN out_payment_cur FOR
        	SELECT  p2.order_guid,
			p.debit_amount amount,
			p2.ap_capture_txt req_transaction_id,
			p.refund_id,
                        p2.acq_reference_number,
                        global.encryption.decrypt_it(ap.ap_account_txt,ap.key_name) ap_account_txt,
                        o.order_date auth_date,
                        p.miles_points_debit_amt miles_points_amt,
                        p.auth_number
		  FROM  payments p, payments p2, ap_accounts ap, orders o
                 WHERE  p2.order_guid = p.order_guid
		   AND  p2.payment_type = p.payment_type
		   AND  p2.additional_bill_id is null
		   AND  p2.payment_indicator = 'P'
                   AND  o.order_guid = p.order_guid
                   AND  ap.ap_account_id = p2.ap_account_id
		   AND  p.payment_id = in_payment_id;

END IF;

END GET_AP_PAYMENT_INFO;



PROCEDURE UPDATE_DISPATCHED_COUNT
(
 IN_BILLING_HEADER_ID		IN  ALT_PAY_BILLING_HEADER.ALT_PAY_BILLING_HEADER_ID%TYPE,
 IN_DISPATCHED_COUNT		IN  ALT_PAY_BILLING_HEADER.DISPATCHED_COUNT%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Update dispatched_count for the give billing header id.
Input:
        in_billing_header_id number
	in_dispatched_count  number

Output:
        out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

BEGIN

OUT_STATUS := 'N';

UPDATE alt_pay_billing_header
SET
        dispatched_count = in_dispatched_count
WHERE   alt_pay_billing_header_id = in_billing_header_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_DISPATCHED_COUNT;

FUNCTION GET_BILLED_PMT_AMT
(
 IN_ORDER_GUID			IN  ORDERS.ORDER_GUID%TYPE,
 IN_PAYMENT_METHOD_ID		IN  PAYMENTS.PAYMENT_TYPE%TYPE
)
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
	Retrieves billed payment amount for the given 
	payment method id and order.
Input:
        in_order_guid varchar2
	in_payment_method_id varchar2
Output:
        NUMBER

-----------------------------------------------------------------------------*/
v_amt NUMBER;

BEGIN

  SELECT nvl(SUM(nvl(p.credit_amount,0)),0)
    INTO v_amt
    FROM payments p
   WHERE EXISTS (SELECT 1
                   FROM payments p2
				JOIN order_details od
                     ON od.order_guid=p2.order_guid
                WHERE p2.payment_id=p.payment_id
                    AND p.bill_status in ('Billed','Settled')
		)
     AND p.credit_amount > 0
     AND p.payment_indicator = 'P'
     AND p.payment_type = in_payment_method_id
     AND p.order_guid = in_order_guid;

RETURN v_amt;

END GET_BILLED_PMT_AMT;


FUNCTION GET_BILLED_REFUND_AMT
(
 IN_ORDER_GUID			IN  ORDERS.ORDER_GUID%TYPE,
 IN_PAYMENT_METHOD_ID		IN  PAYMENTS.PAYMENT_TYPE%TYPE
)
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
	Retrieves billed refund amount for the given 
	payment method id and order.
Input:
        in_order_guid varchar2
	in_payment_method_id varchar2
Output:
        NUMBER

-----------------------------------------------------------------------------*/
v_amt NUMBER;
BEGIN


  SELECT nvl(SUM(nvl(p.debit_amount,0)),0)
    INTO v_amt
    FROM refund r
    JOIN payments p
      ON p.refund_id = r.refund_id
   WHERE p.debit_amount > 0
     AND p.payment_indicator = 'R'
     AND p.payment_type = in_payment_method_id
     AND p.order_guid = in_order_guid
     AND r.refund_status in ('Billed','Settled');
     
  return v_amt;

END GET_BILLED_REFUND_AMT;


FUNCTION GET_ORDER_GUID
(
 IN_MASTER_ORDER_NUMBER		IN  ORDERS.MASTER_ORDER_NUMBER%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
	Retrieves order_guid for the given master_order_number

Input:
        in_master_order_number varchar2

Output:
        varchar2

-----------------------------------------------------------------------------*/
v_guid ORDERS.ORDER_GUID%TYPE;
BEGIN


  SELECT order_guid
    INTO v_guid
    FROM orders
   WHERE master_order_number = in_master_order_number;
   
  return v_guid;

END GET_ORDER_GUID;


FUNCTION GET_NEXT_UA_REFUND_TRANS_SEQ RETURN NUMBER

/*------------------------------------------------------------------------------
Description: This function returns the next UA refund transaction sequence id.
Input: none
Output: number
------------------------------------------------------------------------------*/
AS
V_UNITED_TRAN_REFUND_SEQ NUMBER;

BEGIN

  SELECT  CLEAN.UNITED_TRANS_ID_SEQ.NEXTVAL
  INTO    V_UNITED_TRAN_REFUND_SEQ
  FROM    DUAL;

RETURN V_UNITED_TRAN_REFUND_SEQ;

END GET_NEXT_UA_REFUND_TRANS_SEQ;


PROCEDURE GET_ALT_PAY_EOD_ERROR_MESSAGES 
(
  OUT_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves alt pay eod errors for PP and GD payment types. Can be extended to other
  pay types in the future.
Input:
        NA
Output:
        out_cur error information

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
SELECT apgd.alt_pay_billing_detail_id,
apgd.alt_pay_billing_header_id,
pmts.created_on,
apgd.payment_id, 
apgd.payment_code,
pmts.payment_type,
apgd.master_order_number,
apgd.auth_date,
apgd.settlement_amt,
null additional_info,
apgd.status,
nvl(errmap.error_message_out, apgd.error_txt) error_text
FROM CLEAN.payments pmts, CLEAN.alt_pay_billing_detail_gd apgd
LEFT OUTER JOIN CLEAN.EOD_ERROR_MAPPING_VAL errmap
ON apgd.error_txt like errmap.error_message_in
WHERE apgd.status = 'Failure'
  AND pmts.payment_id = apgd.payment_id
  AND pmts.payment_type = 'GD'
	
UNION

SELECT appp.alt_pay_billing_detail_id,
appp.alt_pay_billing_header_id,
pmts.created_on,
appp.payment_id, 
appp.payment_code,
pmts.payment_type,
appp.master_order_number,
null auth_date, 
appp.req_amt settlement_amt,
'Info: '|| appp.req_transaction_txt additional_info, -- prepend text to avoid weird formatting
appp.res_ack_txt status,
nvl(errmap.error_message_out, appp.ack_error_txt) error_text 
FROM CLEAN.payments pmts, clean.alt_pay_billing_detail_pp appp
LEFT OUTER JOIN CLEAN.EOD_ERROR_MAPPING_VAL errmap
ON appp.ack_error_txt like errmap.error_message_in
WHERE res_transaction_txt is null
  AND pmts.payment_id = appp.payment_id
  AND pmts.payment_type = 'PP'
order by created_on desc;

END GET_ALT_PAY_EOD_ERROR_MESSAGES;
  
  
PROCEDURE GET_EOD_ERROR_ALERT_LIST 
(
  OUT_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves alt pay eod error email ids
Input:
        NA
Output:
        out_cur error information

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cur FOR
  SELECT pager_number email_id
  FROM SITESCOPE.pagers 
  where project = 'SCRUB_ALERTS';
  
END GET_EOD_ERROR_ALERT_LIST;


FUNCTION GET_GD_BY_PAY_ID
(
 IN_PAYMENT_ID              IN  ALT_PAY_BILLING_DETAIL_GD.PAYMENT_ID%TYPE
)
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------------------------------------------------------------------------------------
Description:
	Retrieves payment_code for the given payment_id

Input:
        in_payment_id number

Output:
        varchar2

----------------------------------------------------------------------------------------------------------------------------------------------------*/
pay_code ALT_PAY_BILLING_DETAIL_GD.PAYMENT_CODE%TYPE;
BEGIN

  SELECT distinct payment_code
    INTO pay_code
    FROM clean.ALT_PAY_BILLING_DETAIL_GD
   WHERE payment_id = IN_PAYMENT_ID;

return pay_code;

EXCEPTION
WHEN NO_DATA_FOUND THEN
    pay_code := 'N';
    return pay_code;
   
END GET_GD_BY_PAY_ID;

PROCEDURE GET_PG_ALTPAY_PAYMENT_EOD_RECS
(
 IN_PAYMENT_TYPE        IN PAYMENTS.PAYMENT_TYPE%TYPE,
 IN_DATE		IN DATE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves payments / refunds with specified payment_type from clean.payments table of Payment Gateway
	prior to or on the given date that have not been billed.
Input:
        in_date
Output:
        out_payment_cur containing payment data
	out_refund_cur	containing refund data

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_payment_cur FOR
        SELECT DISTINCT o.master_order_number,
             		p.payment_id,
             		'P' payment_indicator
		FROM    clean.payments p
		JOIN    clean.orders o
		ON      p.order_guid=o.order_guid
		JOIN    clean.order_details od
		ON      o.order_guid = od.order_guid
		AND     od.order_disp_code IN ('Processed','Shipped','Printed')
		WHERE 	(p.created_on < TRUNC(in_date) + 1 and p.created_on > TRUNC(in_date) - 180)
		AND p.bill_status = 'Unbilled'
		AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))
		AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))		
		AND     p.credit_amount > 0
		AND     p.payment_indicator='P'
    	AND     p.payment_type = IN_PAYMENT_TYPE
		AND 	(clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'Y')
		ORDER BY o.master_order_number;

  OPEN out_refund_cur FOR
	SELECT DISTINCT o.master_order_number,
	                p.payment_id,
	                'R' payment_indicator
		FROM 	clean.payments p
		JOIN	clean.orders o
		ON	o.order_guid=p.order_guid
		JOIN    clean.refund r
		ON      p.refund_id=r.refund_id
		JOIN    clean.order_details od
		ON      od.order_detail_id=r.order_detail_id
		AND 	od.order_disp_code IN ('Processed','Shipped','Printed')
		WHERE (r.created_on < TRUNC(in_date) + 1 and r.created_on > TRUNC(in_date) - 180)
		  AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))
		  AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))			
	          AND r.refund_status = 'Unbilled'
	          AND p.debit_amount > 0
	          AND p.payment_type = IN_PAYMENT_TYPE
	          AND p.payment_indicator = 'R'
			  AND (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'Y')
	          ORDER BY o.master_order_number;

END GET_PG_ALTPAY_PAYMENT_EOD_RECS;


PROCEDURE CREATE_PG_ALTPAY_EOD_RECS
(
 IN_PAYMENT_METHOD_ID		IN  BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
 IN_BATCH_DATE        		IN  BILLING_HEADER_PG.BILLING_BATCH_DATE%TYPE,
 IN_PROCESSED_FLAG         	IN  BILLING_HEADER_PG.processed_indicator%TYPE,
 OUT_BILLING_HEADER_ID    OUT BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Insert into alt_pay_billing_header table
Input:
        in_payment_method id payment method id
        in_batch_date batch date
        in_processed_flag processed flag
        in_dispatched_cound dispatched count
        in_processed_count processed count

Output:
        out_billing_header_id alt_pay_billing_header_id generated
	out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := 'N';
SELECT BILLING_HEADER_ID_PG_SEQ.NEXTVAL INTO out_billing_header_id FROM DUAL;

INSERT INTO BILLING_HEADER_PG
(
        billing_header_id,
        PAYMENT_TYPE,
        processed_indicator,
        created_on,
        billing_batch_date
)
VALUES
(
        out_billing_header_id,
        in_payment_method_id,
        in_processed_flag,
        SYSDATE,
        in_batch_date
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_PG_ALTPAY_EOD_RECS;


PROCEDURE GET_PG_AP_PAYMENT_INFO
(
 IN_PAYMENT_ID			IN  PAYMENTS.PAYMENT_ID%TYPE,
 IN_PAYMENT_INDICATOR		IN  PAYMENTS.PAYMENT_INDICATOR%TYPE,
 OUT_PAYMENT_CUR		OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Returns payment information for the given payment id.
        Note that returned values: acq_reference_number, ap_account_txt, and order_date
        are currently only used for BillMeLater EOD processing.
Input:
        in_payment_id payment id

Output:
        out_payment_cur cursor with payment info

-----------------------------------------------------------------------------*/


BEGIN

IF in_payment_indicator = 'P' THEN
-- payment
	OPEN out_payment_cur FOR
      SELECT p.payment_id,
                 o.master_order_number,
                 p.additional_bill_id,
                 p.payment_type,
                 p.credit_amount,
                 p.debit_amount,
                 p.payment_indicator,
                 p.refund_id,
                 p.auth_date,
                 p.auth_number,
                 p.is_voice_auth,
				 p.AUTHORIZATION_TRANSACTION_ID,
				 p.settlement_transaction_id,
				 p.REFUND_TRANSACTION_ID,
				 p.merchant_ref_id
		  FROM  payments p, ap_accounts ap, orders o
		 WHERE  p.payment_id = in_payment_id
                   AND  o.order_guid = p.order_guid
                   AND  ap.ap_account_id = p.ap_account_id;
ELSE
-- refund
	OPEN out_payment_cur FOR
        	SELECT p.payment_id,
                 o.master_order_number,
                 p.additional_bill_id,
                 p.payment_type,
                 p.credit_amount,
                 p.debit_amount,
                 p.payment_indicator,
                 p.refund_id,
                 p.auth_date,
                 p.auth_number,
                 p.is_voice_auth,
				 p.AUTHORIZATION_TRANSACTION_ID,
				 p.settlement_transaction_id,
				 p.REFUND_TRANSACTION_ID,
				 p.merchant_ref_id
		  FROM  payments p, payments p2, ap_accounts ap, orders o
                 WHERE  p2.order_guid = p.order_guid
		   AND  p2.payment_type = p.payment_type
		   AND  p2.additional_bill_id is null
		   AND  p2.payment_indicator = 'P'
                   AND  o.order_guid = p.order_guid
                   AND  ap.ap_account_id = p2.ap_account_id
		   AND  p.payment_id = in_payment_id;

END IF;

END GET_PG_AP_PAYMENT_INFO;

PROCEDURE CREATE_BILLING_DETAIL_PG_PP
(

		IN_BILLING_HEADER_ID        IN    BILLING_DETAIL_PG_PP.BILLING_HEADER_ID%TYPE,
        IN_ORDER_NUMBER             IN    BILLING_DETAIL_PG_PP.ORDER_NUMBER%TYPE,
        IN_PAYMENT_ID               IN    BILLING_DETAIL_PG_PP.PAYMENT_ID%TYPE,
        IN_ORDER_AMOUNT             IN    BILLING_DETAIL_PG_PP.ORDER_AMOUNT%TYPE,
        IN_TRANSACTION_TYPE         IN    BILLING_DETAIL_PG_PP.TRANSACTION_TYPE%TYPE,
        IN_BILL_TYPE   				IN    BILLING_DETAIL_PG_PP.BILL_TYPE%TYPE,
        IN_AUTH_TRANSACTION_ID		IN    BILLING_DETAIL_PG_PP.AUTH_TRANSACTION_ID%TYPE,
        IN_MERCHANT_REF_ID          IN    BILLING_DETAIL_PG_PP.MERCHANT_REF_ID%TYPE,
           IN_SETTLEMENT_TRANSACTION_ID  IN  BILLING_DETAIL_PG_PP.SETTLEMENT_TRANSACTION_ID%TYPE,
      IN_REFUND_TRANSACTION_ID   	IN BILLING_DETAIL_PG_PP.REFUND_TRANSACTION_ID%TYPE,
      IN_REQUEST_ID      			IN BILLING_DETAIL_PG_PP.REQUEST_ID%TYPE,
        OUT_SEQUENCE_NUMBER         OUT   BILLING_DETAIL_PG_PP.BILLING_DETAIL_ID%TYPE,
        OUT_STATUS                  OUT VARCHAR2,
        OUT_MESSAGE                 OUT VARCHAR2
)
AS
  
BEGIN

SELECT BILLING_DETAIL_ID_PG_PP_SEQ.NEXTVAL INTO out_sequence_number FROM DUAL;

INSERT INTO billing_detail_pg_pp
(
  BILLING_DETAIL_ID,
	BILLING_HEADER_ID,
	ORDER_NUMBER, 
	PAYMENT_ID, 
	ORDER_AMOUNT, 
	TRANSACTION_TYPE, 
	BILL_TYPE, 
	AUTH_TRANSACTION_ID,
	MERCHANT_REF_ID,
	  SETTLEMENT_TRANSACTION_ID,
  	REFUND_TRANSACTION_ID,
  	REQUEST_ID,
	STATUS,
	CREATED_ON,
	UPDATED_ON
)
VALUES
(
  out_sequence_number,
  in_billing_header_id,
  in_order_number,
  in_payment_id,
	in_ORDER_AMOUNT, 
	in_TRANSACTION_TYPE, 
	in_BILL_TYPE, 
	IN_AUTH_TRANSACTION_ID,
	IN_MERCHANT_REF_ID,
	IN_SETTLEMENT_TRANSACTION_ID,
	IN_REFUND_TRANSACTION_ID,
	IN_REQUEST_ID,
	'New',
	sysdate,
	sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_BILLING_DETAIL_PG_PP;

PROCEDURE UPDATE_BILLING_DTL_PG_PP_RES
(
 IN_AUTH_TRANS_ID       IN BILLING_DETAIL_PG_PP.AUTH_TRANSACTION_ID%TYPE,
 IN_ORDER_NUMBER      IN BILLING_DETAIL_PG_PP.ORDER_NUMBER%TYPE,
 IN_BILLING_DETAIL_ID IN BILLING_DETAIL_PG_PP.BILLING_DETAIL_ID%TYPE,
 IN_PAYMENT_ID        IN BILLING_DETAIL_PG_PP.PAYMENT_ID%TYPE,
 IN_REQUEST_ID      IN BILLING_DETAIL_PG_PP.REQUEST_ID%TYPE,
 IN_SETTLEMENT_TRANSACTION_ID   IN BILLING_DETAIL_PG_PP.SETTLEMENT_TRANSACTION_ID%TYPE,
 IN_REFUND_TRANSACTION_ID IN BILLING_DETAIL_PG_PP.REFUND_TRANSACTION_ID%TYPE,
 IN_RESPONSE_MESSAGE  IN BILLING_DETAIL_PG_PP.RESPONSE_MESSAGE%TYPE,
 IN_STATUS            IN BILLING_DETAIL_PG_PP.STATUS%TYPE,
 IN_SETTLEMENT_DATE   IN BILLING_DETAIL_PG_PP.SETTLEMENT_DATE%TYPE,
 IN_PROCESS_ACCOUNT	  IN BILLING_DETAIL_PG_PP.PROCESS_ACCOUNT%TYPE,
 IN_REQUEST_TOKEN		IN  payments.REQUEST_TOKEN%TYPE,
 IN_RESPONSE_CODE   IN  BILLING_DETAIL_PG_PP.RESPONSE_CODE%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
   This procedure updates a billing_detail_pg_CC with respnse and 
   updates payments.
  

Input:
       

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/

 BEGIN
 
  IF (IN_STATUS != 'Settled') THEN 
  
   UPDATE BILLING_DETAIL_PG_PP
  SET status                  = IN_STATUS,
    response_message          = IN_RESPONSE_MESSAGE,
    response_code             =  IN_RESPONSE_CODE,
    updated_on                = sysdate
  WHERE BILLING_DETAIL_ID     = IN_BILLING_DETAIL_ID;
  ELSE 
  UPDATE BILLING_DETAIL_PG_PP
  SET status                  = IN_STATUS,
    request_id                =IN_REQUEST_ID,
    SETTLEMENT_TRANSACTION_ID = IN_SETTLEMENT_TRANSACTION_ID,
    REFUND_TRANSACTION_ID =  IN_REFUND_TRANSACTION_ID,
    settlement_date           = IN_SETTLEMENT_DATE,
    response_message          =IN_RESPONSE_MESSAGE,
    PROCESS_ACCOUNT			  = IN_PROCESS_ACCOUNT,
    response_code      = IN_RESPONSE_CODE,
    updated_on                = sysdate
  WHERE BILLING_DETAIL_ID     = IN_BILLING_DETAIL_ID;
 END IF;
 
    IF (IN_SETTLEMENT_TRANSACTION_ID is not null)
    THEN
      UPDATE payments
      SET SETTLEMENT_TRANSACTION_ID = IN_SETTLEMENT_TRANSACTION_ID,
      updated_on= sysdate
      WHERE payment_id              = IN_PAYMENT_ID;
    END IF;
  	
  	IF (IN_REFUND_TRANSACTION_ID is not null)
 	 THEN 
    UPDATE payments
    SET REFUND_TRANSACTION_ID = IN_REFUND_TRANSACTION_ID,
      updated_on                  = sysdate
    WHERE payment_id              = IN_PAYMENT_ID;
  END IF;
  
  out_status                   := 'Y';
  
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    out_status  := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    

END UPDATE_BILLING_DTL_PG_PP_RES;


FUNCTION GET_BILLING_DETAILS_PP_PG
(
 IN_BILLING_HEADER_ID        IN    BILLING_DETAIL_PG_PP.BILLING_HEADER_ID%TYPE,
  IN_BILLING_DETAIL_ID        IN    BILLING_DETAIL_PG_PP.BILLING_DETAIL_ID%TYPE
)
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------------------------------------------------------------------------------------
Description:
	Retrieves payment_code for the given payment_id

Input:
        in_payment_id number

Output:
        varchar2

----------------------------------------------------------------------------------------------------------------------------------------------------*/
V_BILLING_DETAIL_ID  BILLING_DETAIL_PG_PP.BILLING_DETAIL_ID%TYPE;
BEGIN

 SELECT BILLING_DETAIL_ID INTO V_BILLING_DETAIL_ID FROM BILLING_DETAIL_PG_PP
 WHERE BILLING_HEADER_ID  = IN_BILLING_HEADER_ID AND BILLING_DETAIL_ID = IN_BILLING_DETAIL_ID 
 AND STATUS not in ('Settled');

return V_BILLING_DETAIL_ID;

EXCEPTION
WHEN NO_DATA_FOUND THEN
    V_BILLING_DETAIL_ID := 0;
    return V_BILLING_DETAIL_ID;

END GET_BILLING_DETAILS_PP_PG;

PROCEDURE GET_BILLING_DET_PG_PP_BY_ID
(
 IN_BILLING_HEADER_ID  IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
 IN_BILLING_DETAIL_ID   IN BILLING_DETAIL_PG_PP.BILLING_DETAIL_ID%TYPE,
 OUT_CURSOR            OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN out_cursor FOR
 	      SELECT bd.*,(select p.payment_type from clean.payments p where p.payment_id = bd.payment_id) as payment_type
 	  	  FROM billing_detail_pg_pp bd
    WHERE billing_detail_id= IN_BILLING_DETAIL_ID and
     billing_header_id = IN_BILLING_HEADER_ID;  

END GET_BILLING_DET_PG_PP_BY_ID;


PROCEDURE UPDATE_PG_DISPATCHED_COUNT
(
 IN_BILLING_HEADER_ID		IN  BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
 IN_DISPATCHED_COUNT		IN  BILLING_HEADER_PG.DISPATCHED_COUNT%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Update dispatched_count for the give billing header id.
Input:
        in_billing_header_id number
	in_dispatched_count  number

Output:
        out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

BEGIN

OUT_STATUS := 'N';

if in_dispatched_count =0 THEN

UPDATE billing_header_pg
SET
        dispatched_count = in_dispatched_count,
        PROCESSED_INDICATOR = 'Y'
WHERE   billing_header_id = in_billing_header_id;

ELSE 

UPDATE billing_header_pg
SET
        dispatched_count = in_dispatched_count
 WHERE   billing_header_id = in_billing_header_id;

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PG_DISPATCHED_COUNT;

PROCEDURE GET_FAILED_BILLING_DETAILS_PP
(
   IN_BILLING_HEADER_ID       IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
   IN_PAYMENT_TYPE      IN BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
   OUT_BILLING_DETAILS OUT TYPES.REF_CURSOR
)
AS

BEGIN

 OPEN OUT_BILLING_DETAILS FOR
 
  SELECT *  
      FROM clean.billing_header_pg bh, clean.billing_detail_pg_pp bd
      WHERE bh.BILLING_HEADER_ID = bd.billing_header_id
      AND bh.BILLING_HEADER_ID = IN_BILLING_HEADER_ID and bh.payment_type = IN_PAYMENT_TYPE
      AND bd.status in ('Declined','Invalid','Error','New');

END GET_FAILED_BILLING_DETAILS_PP;

END;
.
/