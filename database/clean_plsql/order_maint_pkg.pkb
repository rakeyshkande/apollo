create or replace PACKAGE BODY         CLEAN.ORDER_MAINT_PKG
AS
 
PROCEDURE UPDATE_ORDERS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_CUSTOMER_ID                  IN ORDERS.CUSTOMER_ID%TYPE,
IN_MEMBERSHIP_ID                IN ORDERS.MEMBERSHIP_ID%TYPE,
IN_COMPANY_ID                   IN ORDERS.COMPANY_ID%TYPE,
IN_SOURCE_CODE                  IN ORDERS.SOURCE_CODE%TYPE,
IN_ORIGIN_ID                    IN ORDERS.ORIGIN_ID%TYPE,
IN_ORDER_DATE                   IN ORDERS.ORDER_DATE%TYPE,
IN_UPDATED_BY                   IN ORDERS.UPDATED_BY%TYPE,
IN_LOSS_PREVENTION_INDICATOR    IN ORDERS.LOSS_PREVENTION_INDICATOR%TYPE,
IN_EMAIL_ID                     IN ORDERS.EMAIL_ID%TYPE,
IN_ORDER_TAKEN_BY               IN ORDERS.ORDER_TAKEN_BY%TYPE,
IN_PARTNERSHIP_BONUS_POINTS     IN ORDERS.PARTNERSHIP_BONUS_POINTS%TYPE,
IN_FRAUD_INDICATOR              IN ORDERS.FRAUD_INDICATOR%TYPE,
IN_ARIBA_BUYER_ASN_NUMBER       IN ORDERS.ARIBA_BUYER_ASN_NUMBER%TYPE,
IN_ARIBA_BUYER_COOKIE           IN ORDERS.ARIBA_BUYER_COOKIE%TYPE,
IN_ARIBA_PAYLOAD                IN ORDERS.ARIBA_PAYLOAD%TYPE,
IN_CO_BRAND_CREDIT_CARD_CODE    IN ORDERS.CO_BRAND_CREDIT_CARD_CODE%TYPE,
IN_WEBOE_DNIS_ID                IN ORDERS.WEBOE_DNIS_ID%TYPE,
IN_ORDER_TAKEN_CALL_CENTER      IN ORDERS.ORDER_TAKEN_CALL_CENTER%TYPE,
IN_MP_REDEMPTION_RATE_AMT       IN ORDERS.MP_REDEMPTION_RATE_AMT%TYPE,
IN_BUYER_SIGNED_IN_FLAG         IN ORDERS.BUYER_SIGNED_IN_FLAG%TYPE,
IN_CHARACTER_MAPPING_FLAG       IN ORDERS.CHARACTER_MAPPING_FLAG%TYPE,
IN_LANGUAGE_ID         			IN ORDERS.LANGUAGE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order (shopping cart)
        information

Input:
        order_guid                      varchar2
        master_order_number             varchar2
        customer_id                     number
        membership_id                   number
        company_id                      varchar2
        source_code                     varchar2
        origin_id                       varchar2
        updated_by                      varchar2
        loss_prevention_indicator       char
        email_id                        number
        order_taken_by                  varchar2
        partnership_bonus_points        number
        fraud_indicator                 char
        ariba_buyer_asn_number          varchar2
        ariba_buyer_cookie              varchar2
        ariba_payload                   varchar2
        co_brand_credit_card_code       varchar2
        weboe_dnis_id                   varchar2
        order_taken_call_center         varchar2
        character_mapping_flag          varchar2
		language_id						varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  orders
SET
        customer_id = NVL(in_customer_id, customer_id),
        membership_id = NVL(in_membership_id, membership_id),
        company_id = NVL(in_company_id, company_id),
        source_code = NVL(in_source_code, source_code),
        origin_id = NVL(in_origin_id, origin_id),
        order_date = NVL(in_order_date, order_date),
        updated_on = sysdate,
        updated_by = in_updated_by,
        loss_prevention_indicator = NVL(in_loss_prevention_indicator, loss_prevention_indicator),
        email_id = NVL(in_email_id, email_id),
        order_taken_by = NVL(in_order_taken_by, order_taken_by),
        partnership_bonus_points = NVL(in_partnership_bonus_points, partnership_bonus_points),
        fraud_indicator = NVL(in_fraud_indicator, fraud_indicator),
        ariba_buyer_asn_number = NVL(in_ariba_buyer_asn_number, ariba_buyer_asn_number),
        ariba_buyer_cookie = NVL(in_ariba_buyer_cookie, ariba_buyer_cookie),
        ariba_payload = NVL(in_ariba_payload, ariba_payload),
        co_brand_credit_card_code = NVL(in_co_brand_credit_card_code, co_brand_credit_card_code),
        weboe_dnis_id = NVL (in_weboe_dnis_id, weboe_dnis_id),
        order_taken_call_center = NVL (in_order_taken_call_center, order_taken_call_center),
        mp_redemption_rate_amt = NVL (in_mp_redemption_rate_amt, mp_redemption_rate_amt),
        buyer_signed_in_flag    = in_buyer_signed_in_flag ,
        character_mapping_flag = in_character_mapping_flag,
		language_id = in_language_id

WHERE   order_guid = in_order_guid;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDERS;


PROCEDURE UPDATE_ORDER_DETAILS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
IN_OP_STATUS                    IN ORDER_DETAILS.OP_STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record.

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number
        op_status                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  order_details
SET
        delivery_date = NVL(in_delivery_date, delivery_date),
        recipient_id = NVL(in_recipient_id, recipient_id),
        product_id = NVL(in_product_id, product_id),
        quantity = NVL(in_quantity, quantity),
        color_1 = NVL(in_color_1, color_1),
        color_2 = NVL(in_color_2, color_2),
        substitution_indicator = NVL(in_substitution_indicator, substitution_indicator),
        same_day_gift = NVL(in_same_day_gift, same_day_gift),
        occasion = NVL(in_occasion, occasion),
        card_message = NVL(in_card_message, card_message),
        card_signature = NVL(in_card_signature, card_signature),
        special_instructions = NVL(in_special_instructions, special_instructions),
        release_info_indicator = NVL(in_release_info_indicator, release_info_indicator),
        florist_id = NVL(in_florist_id, florist_id),
        ship_method = NVL(in_ship_method, ship_method),
        ship_date = NVL(in_ship_date, ship_date),
        order_disp_code = NVL(in_order_disp_code, order_disp_code),
        second_choice_product = NVL(in_second_choice_product, second_choice_product),
        zip_queue_count = NVL(in_zip_queue_count, zip_queue_count),
        updated_on = sysdate,
        updated_by = in_updated_by,
        delivery_date_range_end = NVL(in_delivery_date_range_end, delivery_date_range_end),
        scrubbed_on = NVL(in_scrubbed_on, scrubbed_on),
        scrubbed_by = NVL(in_scrubbed_by, scrubbed_by),
        ariba_unspsc_code = NVL(in_ariba_unspsc_code, ariba_unspsc_code),
        ariba_po_number = NVL(in_ariba_po_number, ariba_po_number),
        ariba_ams_project_code = NVL(in_ariba_ams_project_code, ariba_ams_project_code),
        ariba_cost_center = NVL(in_ariba_cost_center, ariba_cost_center),
        size_indicator = NVL(in_size_indicator, size_indicator),
        miles_points = NVL(in_miles_points, miles_points),
        subcode = NVL(in_subcode, subcode),
        source_code = NVL(in_source_code, source_code),
        reject_retry_count = NVL(in_reject_retry_count, reject_retry_count),
        op_status = NVL (in_op_status, op_status)
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAILS;




PROCEDURE UPDATE_ORDER_DETAILS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record.
        This procedure overloads the one above defaulting the in_op_status
        to null.

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE_ORDER_DETAILS
(
        IN_ORDER_DETAIL_ID,
        IN_DELIVERY_DATE,
        IN_RECIPIENT_ID,
        IN_PRODUCT_ID,
        IN_QUANTITY,
        IN_COLOR_1,
        IN_COLOR_2,
        IN_SUBSTITUTION_INDICATOR,
        IN_SAME_DAY_GIFT,
        IN_OCCASION,
        IN_CARD_MESSAGE,
        IN_CARD_SIGNATURE,
        IN_SPECIAL_INSTRUCTIONS,
        IN_RELEASE_INFO_INDICATOR,
        IN_FLORIST_ID,
        IN_SHIP_METHOD,
        IN_SHIP_DATE,
        IN_ORDER_DISP_CODE,
        IN_SECOND_CHOICE_PRODUCT,
        IN_ZIP_QUEUE_COUNT,
        IN_UPDATED_BY,
        IN_DELIVERY_DATE_RANGE_END,
        IN_SCRUBBED_ON,
        IN_SCRUBBED_BY,
        IN_ARIBA_UNSPSC_CODE,
        IN_ARIBA_PO_NUMBER,
        IN_ARIBA_AMS_PROJECT_CODE,
        IN_ARIBA_COST_CENTER,
        IN_SIZE_INDICATOR,
        IN_MILES_POINTS,
        IN_SUBCODE,
        IN_SOURCE_CODE,
        IN_REJECT_RETRY_COUNT,
        NULL,                           -- IN_OP_STATUS
        OUT_STATUS,
        OUT_MESSAGE
);

END UPDATE_ORDER_DETAILS;


PROCEDURE UPDATE_ORDER_DETAILS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
IN_OP_STATUS                    IN ORDER_DETAILS.OP_STATUS%TYPE,
IN_VENDOR_ID                    IN ORDER_DETAILS.VENDOR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record.

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number
        op_status                       varchar2
        vendor_id                    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  order_details
SET
        delivery_date = NVL(in_delivery_date, delivery_date),
        recipient_id = NVL(in_recipient_id, recipient_id),
        product_id = NVL(in_product_id, product_id),
        quantity = NVL(in_quantity, quantity),
        color_1 = NVL(in_color_1, color_1),
        color_2 = NVL(in_color_2, color_2),
        substitution_indicator = NVL(in_substitution_indicator, substitution_indicator),
        same_day_gift = NVL(in_same_day_gift, same_day_gift),
        occasion = NVL(in_occasion, occasion),
        card_message = NVL(in_card_message, card_message),
        card_signature = NVL(in_card_signature, card_signature),
        special_instructions = NVL(in_special_instructions, special_instructions),
        release_info_indicator = NVL(in_release_info_indicator, release_info_indicator),
        florist_id = NVL(in_florist_id, florist_id),
        ship_method = NVL(in_ship_method, ship_method),
        ship_date = NVL(in_ship_date, ship_date),
        order_disp_code = NVL(in_order_disp_code, order_disp_code),
        second_choice_product = NVL(in_second_choice_product, second_choice_product),
        zip_queue_count = NVL(in_zip_queue_count, zip_queue_count),
        updated_on = sysdate,
        updated_by = in_updated_by,
        delivery_date_range_end = NVL(in_delivery_date_range_end, delivery_date_range_end),
        scrubbed_on = NVL(in_scrubbed_on, scrubbed_on),
        scrubbed_by = NVL(in_scrubbed_by, scrubbed_by),
        ariba_unspsc_code = NVL(in_ariba_unspsc_code, ariba_unspsc_code),
        ariba_po_number = NVL(in_ariba_po_number, ariba_po_number),
        ariba_ams_project_code = NVL(in_ariba_ams_project_code, ariba_ams_project_code),
        ariba_cost_center = NVL(in_ariba_cost_center, ariba_cost_center),
        size_indicator = NVL(in_size_indicator, size_indicator),
        miles_points = NVL(in_miles_points, miles_points),
        subcode = NVL(in_subcode, subcode),
        source_code = NVL(in_source_code, source_code),
        reject_retry_count = NVL(in_reject_retry_count, reject_retry_count),
        op_status = NVL (in_op_status, op_status),
        vendor_id = NVL (in_vendor_id, vendor_id)
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAILS;



PROCEDURE UPDATE_ORDER_DETAILS_COMMIT
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record
        and committing the changes.  The commit is needed for the
        interaction between the communication screen and venus process to
        retrieve the updated data.

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE_ORDER_DETAILS
(
        IN_ORDER_DETAIL_ID,
        IN_DELIVERY_DATE,
        IN_RECIPIENT_ID,
        IN_PRODUCT_ID,
        IN_QUANTITY,
        IN_COLOR_1,
        IN_COLOR_2,
        IN_SUBSTITUTION_INDICATOR,
        IN_SAME_DAY_GIFT,
        IN_OCCASION,
        IN_CARD_MESSAGE,
        IN_CARD_SIGNATURE,
        IN_SPECIAL_INSTRUCTIONS,
        IN_RELEASE_INFO_INDICATOR,
        IN_FLORIST_ID,
        IN_SHIP_METHOD,
        IN_SHIP_DATE,
        IN_ORDER_DISP_CODE,
        IN_SECOND_CHOICE_PRODUCT,
        IN_ZIP_QUEUE_COUNT,
        IN_UPDATED_BY,
        IN_DELIVERY_DATE_RANGE_END,
        IN_SCRUBBED_ON,
        IN_SCRUBBED_BY,
        IN_ARIBA_UNSPSC_CODE,
        IN_ARIBA_PO_NUMBER,
        IN_ARIBA_AMS_PROJECT_CODE,
        IN_ARIBA_COST_CENTER,
        IN_SIZE_INDICATOR,
        IN_MILES_POINTS,
        IN_SUBCODE,
        IN_SOURCE_CODE,
        IN_REJECT_RETRY_COUNT,
        NULL,                           -- IN_OP_STATUS
        OUT_STATUS,
        OUT_MESSAGE
);

IF out_status = 'Y' THEN
        COMMIT;
ELSE
        ROLLBACK;
END IF;

END UPDATE_ORDER_DETAILS_COMMIT;



PROCEDURE UPDATE_ORDER_DETAILS_ALL_VAL
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_AVS_ADDRESS_ID               IN ORDER_DETAILS.AVS_ADDRESS_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
IN_MEMBERSHIP_ID                IN ORDER_DETAILS.MEMBERSHIP_ID%TYPE,
IN_PERSONALIZATION_DATA		IN ORDER_DETAILS.PERSONALIZATION_DATA%TYPE,
IN_QMS_RESULT_CODE              IN ORDER_DETAILS.QMS_RESULT_CODE%TYPE,
IN_PERSONAL_GREETING_ID         IN ORDER_DETAILS.PERSONAL_GREETING_ID%TYPE,
IN_BIN_SOURCE_CHANGED_FLAG      IN ORDER_DETAILS.BIN_SOURCE_CHANGED_FLAG%TYPE,
IN_FREE_SHIPPING_FLAG           IN ORDER_DETAILS.FREE_SHIPPING_FLAG%TYPE,
IN_APPLY_SURCHARGE_CODE		IN ORDER_DETAILS.APPLY_SURCHARGE_CODE%TYPE,
IN_SURCHARGE_DESCRIPTION        IN ORDER_DETAILS.SURCHARGE_DESCRIPTION%TYPE,
IN_DISPLAY_SURCHARGE		IN ORDER_DETAILS.DISPLAY_SURCHARGE_FLAG%TYPE,
IN_SEND_SURCHARGE_TO_FLORIST    IN ORDER_DETAILS.SEND_SURCHARGE_TO_FLORIST_FLAG%TYPE,
IN_ORIGINAL_ORDER_HAS_SDU       IN ORDER_DETAILS.ORIGINAL_ORDER_HAS_SDU%TYPE,
IN_PC_GROUP_ID            IN ORDER_DETAILS.PC_GROUP_ID%TYPE,
IN_PC_MEMBERSHIP_ID       IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE,
IN_PC_FLAG         IN ORDER_DETAILS.PC_FLAG%TYPE,
IN_DERIVED_VIP_FLAG		IN ORDER_DETAILS.DERIVED_VIP_FLAG%TYPE,
IN_TIME_OF_SERVICE			IN ORDER_DETAILS.TIME_OF_SERVICE%TYPE,
IN_LEGACY_ID            IN ORDER_DETAILS.LEGACY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        avs_address_id                  number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number
        membership_id                   number
        qms_result_code                 varchar2
        personal_greeting_id            varchar2
		original_order_has_sdu					char
		PC_GROUP_ID           		varchar2
		PC_MEMBERSHIP_ID           			varchar2
		PC_FLAG           			varchar2
		DERIVED_VIP_FLAG			varchar2
		TIME_OF_SERVICE				varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_details
SET
        delivery_date = in_delivery_date,
        recipient_id = nvl (in_recipient_id, recipient_id),
        avs_address_id = nvl (in_avs_address_id, avs_address_id),
        product_id = in_product_id,
        quantity = in_quantity,
        color_1 = in_color_1,
        color_2 = in_color_2,
        substitution_indicator = nvl (in_substitution_indicator, 'N'),
        same_day_gift = in_same_day_gift,
        occasion = in_occasion,
        card_message = in_card_message,
        card_signature = in_card_signature,
        special_instructions = in_special_instructions,
        release_info_indicator = in_release_info_indicator,
        florist_id = in_florist_id,
        ship_method = in_ship_method,
        ship_date = in_ship_date,
        order_disp_code = NVL (in_order_disp_code, order_disp_code),
        second_choice_product = in_second_choice_product,
        zip_queue_count = in_zip_queue_count,
        updated_on = sysdate,
        updated_by = in_updated_by,
        delivery_date_range_end = in_delivery_date_range_end,
        scrubbed_on = NVL (in_scrubbed_on, scrubbed_on),
        scrubbed_by = NVL (in_scrubbed_by, scrubbed_by),
        ariba_unspsc_code = in_ariba_unspsc_code,
        ariba_po_number = in_ariba_po_number,
        ariba_ams_project_code = in_ariba_ams_project_code,
        ariba_cost_center = in_ariba_cost_center,
        size_indicator = in_size_indicator,
        miles_points = in_miles_points,
        subcode = in_subcode,
        source_code = NVL (in_source_code, source_code),
        reject_retry_count = NVL (in_reject_retry_count, reject_retry_count),
        membership_id = in_membership_id,
        personalization_data = in_personalization_data,
        qms_result_code = in_qms_result_code,
        personal_greeting_id = in_personal_greeting_id,
        bin_source_changed_flag = nvl(in_bin_source_changed_flag, 'N'),
        apply_surcharge_code = in_apply_surcharge_code ,
        surcharge_description = in_surcharge_description ,
        send_surcharge_to_florist_flag = in_send_surcharge_to_florist,
        display_surcharge_flag = in_display_surcharge,
        free_shipping_flag  = in_free_shipping_flag,
		original_order_has_sdu=in_original_order_has_sdu,
		PC_GROUP_ID = IN_PC_GROUP_ID,
		PC_MEMBERSHIP_ID = IN_PC_MEMBERSHIP_ID,
		PC_FLAG = IN_PC_FLAG,
		DERIVED_VIP_FLAG = IN_DERIVED_VIP_FLAG,
		TIME_OF_SERVICE = IN_TIME_OF_SERVICE,
		LEGACY_ID = IN_LEGACY_ID
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAILS_ALL_VAL;

PROCEDURE UPDATE_ORDER_BILLS
(
IN_ORDER_BILL_ID		IN ORDER_BILLS.ORDER_BILL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS.SHIPPING_TAX%TYPE,
IN_TAX       			IN ORDER_BILLS.TAX%TYPE,
IN_UPDATED_BY               	IN ORDER_BILLS.UPDATED_BY%TYPE,
IN_SERVICE_FEE_TAX		IN ORDER_BILLS.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT     	IN ORDER_BILLS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT      	IN ORDER_BILLS.WHOLESALE_AMOUNT%TYPE,
IN_WHOLESALE_SERVICE_FEE        IN ORDER_BILLS.WHOLESALE_SERVICE_FEE%TYPE,
IN_TRANSACTION_AMOUNT    	IN ORDER_BILLS.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE             	IN ORDER_BILLS.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE	IN ORDER_BILLS.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE		IN ORDER_BILLS.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST 		IN ORDER_BILLS.PARTNER_COST%TYPE,
IN_MILES_POINTS_AMT             IN ORDER_BILLS.MILES_POINTS_AMT%TYPE,
IN_FIRST_ORDER_DOMESTIC         IN ORDER_BILLS.DTL_FIRST_ORDER_DOMESTIC%TYPE,
IN_FIRST_ORDER_INTERNATIONAL    IN ORDER_BILLS.DTL_FIRST_ORDER_INTERNATIONAL%TYPE,
IN_SHIPPING_COST                IN ORDER_BILLS.DTL_SHIPPING_COST%TYPE,
IN_VENDOR_CHARGE                IN ORDER_BILLS.DTL_VENDOR_CHARGE%TYPE,
IN_VENDOR_SAT_UPCHARGE          IN ORDER_BILLS.DTL_VENDOR_SAT_UPCHARGE%TYPE,
IN_AK_HI_SPECIAL_SVC_CHARGE     IN ORDER_BILLS.DTL_AK_HI_SPECIAL_SVC_CHARGE%TYPE,
IN_FUEL_SURCHARGE_AMT           IN ORDER_BILLS.DTL_FUEL_SURCHARGE_AMT%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME					IN ORDER_BILLS.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT					IN ORDER_BILLS.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION				IN ORDER_BILLS.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE					IN ORDER_BILLS.TAX1_RATE%TYPE,
IN_TAX2_NAME					IN ORDER_BILLS.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT					IN ORDER_BILLS.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION				IN ORDER_BILLS.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE					IN ORDER_BILLS.TAX2_RATE%TYPE,
IN_TAX3_NAME					IN ORDER_BILLS.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT					IN ORDER_BILLS.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION				IN ORDER_BILLS.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE					IN ORDER_BILLS.TAX3_RATE%TYPE,
IN_TAX4_NAME					IN ORDER_BILLS.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT					IN ORDER_BILLS.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION				IN ORDER_BILLS.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE					IN ORDER_BILLS.TAX4_RATE%TYPE,
IN_TAX5_NAME					IN ORDER_BILLS.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT					IN ORDER_BILLS.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION				IN ORDER_BILLS.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE					IN ORDER_BILLS.TAX5_RATE%TYPE,
IN_SAME_DAY_UPCHARGE            IN ORDER_BILLS.SAME_DAY_UPCHARGE%TYPE,
IN_MORNING_DELIVERY_FEE			IN ORDER_BILLS.MORNING_DELIVERY_FEE%TYPE,
IN_LATE_CUTOFF_FEE				IN ORDER_BILLS.LATE_CUTOFF_FEE%TYPE,
IN_VEN_SUN_UPCHARGE				IN ORDER_BILLS.VENDOR_SUN_UPCHARGE%TYPE,
IN_VEN_MON_UPCHARGE				IN ORDER_BILLS.VENDOR_MON_UPCHARGE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_ORDER_BILLS
(
IN_ORDER_BILL_ID,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SERVICE_FEE,
IN_SHIPPING_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_TAX,
IN_UPDATED_BY,
IN_SERVICE_FEE_TAX,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_WHOLESALE_SERVICE_FEE,
IN_TRANSACTION_AMOUNT,
IN_PDB_PRICE,
IN_DISCOUNT_PRODUCT_PRICE,
IN_DISCOUNT_TYPE,
IN_PARTNER_COST,
IN_MILES_POINTS_AMT,
IN_FIRST_ORDER_DOMESTIC,
IN_FIRST_ORDER_INTERNATIONAL,
IN_SHIPPING_COST,
IN_VENDOR_CHARGE,
IN_VENDOR_SAT_UPCHARGE,
IN_AK_HI_SPECIAL_SVC_CHARGE,
IN_FUEL_SURCHARGE_AMT,
IN_SHIPPING_FEE_SAVED,
IN_SERVICE_FEE_SAVED,
IN_TAX1_NAME,
IN_TAX1_AMOUNT,
IN_TAX1_DESCRIPTION,
IN_TAX1_RATE,
IN_TAX2_NAME,
IN_TAX2_AMOUNT,
IN_TAX2_DESCRIPTION,
IN_TAX2_RATE,
IN_TAX3_NAME,
IN_TAX3_AMOUNT,
IN_TAX3_DESCRIPTION,
IN_TAX3_RATE,
IN_TAX4_NAME,
IN_TAX4_AMOUNT,
IN_TAX4_DESCRIPTION,
IN_TAX4_RATE,
IN_TAX5_NAME,
IN_TAX5_AMOUNT,
IN_TAX5_DESCRIPTION,
IN_TAX5_RATE,
IN_SAME_DAY_UPCHARGE,
IN_MORNING_DELIVERY_FEE,
0,
IN_LATE_CUTOFF_FEE,
IN_VEN_SUN_UPCHARGE,
IN_VEN_MON_UPCHARGE,
OUT_STATUS,
OUT_MESSAGE
);

END UPDATE_ORDER_BILLS;


PROCEDURE UPDATE_ORDER_BILLS
(
IN_ORDER_BILL_ID		IN ORDER_BILLS.ORDER_BILL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS.SHIPPING_TAX%TYPE,
IN_TAX       			IN ORDER_BILLS.TAX%TYPE,
IN_UPDATED_BY               	IN ORDER_BILLS.UPDATED_BY%TYPE,
IN_SERVICE_FEE_TAX		IN ORDER_BILLS.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT     	IN ORDER_BILLS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT      	IN ORDER_BILLS.WHOLESALE_AMOUNT%TYPE,
IN_WHOLESALE_SERVICE_FEE        IN ORDER_BILLS.WHOLESALE_SERVICE_FEE%TYPE,
IN_TRANSACTION_AMOUNT    	IN ORDER_BILLS.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE             	IN ORDER_BILLS.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE	IN ORDER_BILLS.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE		IN ORDER_BILLS.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST 		IN ORDER_BILLS.PARTNER_COST%TYPE,
IN_MILES_POINTS_AMT             IN ORDER_BILLS.MILES_POINTS_AMT%TYPE,
IN_FIRST_ORDER_DOMESTIC         IN ORDER_BILLS.DTL_FIRST_ORDER_DOMESTIC%TYPE,
IN_FIRST_ORDER_INTERNATIONAL    IN ORDER_BILLS.DTL_FIRST_ORDER_INTERNATIONAL%TYPE,
IN_SHIPPING_COST                IN ORDER_BILLS.DTL_SHIPPING_COST%TYPE,
IN_VENDOR_CHARGE                IN ORDER_BILLS.DTL_VENDOR_CHARGE%TYPE,
IN_VENDOR_SAT_UPCHARGE          IN ORDER_BILLS.DTL_VENDOR_SAT_UPCHARGE%TYPE,
IN_AK_HI_SPECIAL_SVC_CHARGE     IN ORDER_BILLS.DTL_AK_HI_SPECIAL_SVC_CHARGE%TYPE,
IN_FUEL_SURCHARGE_AMT           IN ORDER_BILLS.DTL_FUEL_SURCHARGE_AMT%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME					IN ORDER_BILLS.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT					IN ORDER_BILLS.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION				IN ORDER_BILLS.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE					IN ORDER_BILLS.TAX1_RATE%TYPE,
IN_TAX2_NAME					IN ORDER_BILLS.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT					IN ORDER_BILLS.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION				IN ORDER_BILLS.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE					IN ORDER_BILLS.TAX2_RATE%TYPE,
IN_TAX3_NAME					IN ORDER_BILLS.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT					IN ORDER_BILLS.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION				IN ORDER_BILLS.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE					IN ORDER_BILLS.TAX3_RATE%TYPE,
IN_TAX4_NAME					IN ORDER_BILLS.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT					IN ORDER_BILLS.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION				IN ORDER_BILLS.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE					IN ORDER_BILLS.TAX4_RATE%TYPE,
IN_TAX5_NAME					IN ORDER_BILLS.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT					IN ORDER_BILLS.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION				IN ORDER_BILLS.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE					IN ORDER_BILLS.TAX5_RATE%TYPE,
IN_SAME_DAY_UPCHARGE            IN ORDER_BILLS.SAME_DAY_UPCHARGE%TYPE,
IN_MORNING_DELIVERY_FEE			IN ORDER_BILLS.MORNING_DELIVERY_FEE%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_BILLS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
IN_LATE_CUTOFF_FEE				IN ORDER_BILLS.LATE_CUTOFF_FEE%TYPE,
IN_VEN_SUN_UPCHARGE				IN ORDER_BILLS.VENDOR_SUN_UPCHARGE%TYPE,
IN_VEN_MON_UPCHARGE				IN ORDER_BILLS.VENDOR_MON_UPCHARGE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order bill record.

Input:
        order_bill_id                   number
        product_amount                  number
        add_on_amount                   number
        service_fee			number
	shipping_fee			number
        discount_amount			number
 	shipping_tax			number
	tax				number
	updated_by                      varchar2
 	service_fee_tax			number
 	commission_amount		number
	wholesale_amount		number
	wholesale_service_fee		number
	transaction_amount		number
	pdb_price			number
	discount_product_price		number
 	discount_type			char
	partner_cost			number
	miles_points_amt                number
	tax1_name						varchar2
	tax1_amount						number
	tax1_description				varchar2
	tax1_rate						number
	tax2_name						varchar2
	tax2_amount						number
	tax2_description				varchar2
	tax2_rate						number
	tax3_name						varchar2
	tax3_amount						number
	tax3_description				varchar2
	tax3_rate						number
	tax4_name						varchar2
	tax4_amount						number
	tax4_description				varchar2
	tax4_rate						number
	tax5_name						varchar2
	tax5_amount						number
	tax5_description				varchar2
	tax5_rate						number
	same_day_upcharge               number
	morning_delivery_fee			number
	late_cutoff_fee					number
	vendor_sun_upcharge				number
	vendor_mon_upcharge				number
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  order_bills
SET
        product_amount = nvl(in_product_amount, 0),
        add_on_amount = nvl(in_add_on_amount, 0),
 	service_fee = nvl(in_service_fee, 0),
	shipping_fee = nvl(in_shipping_fee, 0),
	discount_amount = nvl(in_discount_amount, 0),
	shipping_tax = nvl(in_shipping_tax, 0),
	tax = nvl(in_tax, 0),
	updated_on = sysdate,
	updated_by = nvl(in_updated_by, 'SYS'),
	service_fee_tax = nvl(in_service_fee_tax, 0),
	commission_amount = nvl(in_commission_amount, 0),
	wholesale_amount = nvl(in_wholesale_amount, 0),
	wholesale_service_fee = nvl(in_wholesale_service_fee, 0),
	transaction_amount = nvl(in_transaction_amount, 0),
	pdb_price = nvl(in_pdb_price, 0),
	discount_product_price = nvl(in_discount_product_price, 0),
	discount_type = nvl(in_discount_type, 'N'),
	partner_cost = nvl(in_partner_cost, 0),
	miles_points_amt = nvl(in_miles_points_amt, 0),
	shipping_fee_saved = nvl(in_shipping_fee_saved, 0),
	service_fee_saved = nvl(in_service_fee_saved, 0),
	tax1_name = in_tax1_name,
	tax1_amount = in_tax1_amount,
	tax1_description = in_tax1_description,
	tax1_rate = in_tax1_rate,
	tax2_name = in_tax2_name,
	tax2_amount = in_tax2_amount,
	tax2_description = in_tax2_description,
	tax2_rate = in_tax2_rate,
	tax3_name = in_tax3_name,
	tax3_amount = in_tax3_amount,
	tax3_description = in_tax3_description,
	tax3_rate = in_tax3_rate,
	tax4_name = in_tax4_name,
	tax4_amount = in_tax4_amount,
	tax4_description = in_tax4_description,
	tax4_rate = in_tax4_rate,
	tax5_name = in_tax5_name,
	tax5_amount = in_tax5_amount,
	tax5_description = in_tax5_description,
	tax5_rate = in_tax5_rate,
	same_day_upcharge = in_same_day_upcharge,
	morning_delivery_fee = in_morning_delivery_fee,
	add_on_discount_amount = nvl(IN_ADD_ON_DISCOUNT_AMOUNT, 0),
	late_cutoff_fee = IN_LATE_CUTOFF_FEE,
	vendor_sun_upcharge = IN_VEN_SUN_UPCHARGE,
	vendor_mon_upcharge = IN_VEN_MON_UPCHARGE,
	DTL_VENDOR_SAT_UPCHARGE=IN_VENDOR_SAT_UPCHARGE

WHERE   order_bill_id = in_order_bill_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_BILLS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_BILLS;

PROCEDURE UPDATE_ACCOUNTING_TRANSACTIONS
(
IN_ACCOUNTING_TRANSACTION_ID	IN ACCOUNTING_TRANSACTIONS.ACCOUNTING_TRANSACTION_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ACCOUNTING_TRANSACTIONS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ACCOUNTING_TRANSACTIONS.ADD_ON_AMOUNT%TYPE,
IN_SHIPPING_FEE                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_FEE%TYPE,
IN_SERVICE_FEE                  IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ACCOUNTING_TRANSACTIONS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_TAX%TYPE,
IN_SERVICE_FEE_TAX              IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE_TAX%TYPE,
IN_TAX                          IN ACCOUNTING_TRANSACTIONS.TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ACCOUNTING_TRANSACTIONS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ACCOUNTING_TRANSACTIONS.WHOLESALE_AMOUNT%TYPE,
IN_ADMIN_FEE                    IN ACCOUNTING_TRANSACTIONS.ADMIN_FEE%TYPE,
IN_WHOLESALE_SERVICE_FEE        IN ACCOUNTING_TRANSACTIONS.WHOLESALE_SERVICE_FEE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_ACCOUNTING_TRANSACTIONS
(
IN_ACCOUNTING_TRANSACTION_ID,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SHIPPING_FEE,
IN_SERVICE_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_SERVICE_FEE_TAX,
IN_TAX,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_ADMIN_FEE,
IN_WHOLESALE_SERVICE_FEE,
0,
OUT_STATUS,
OUT_MESSAGE
);

END UPDATE_ACCOUNTING_TRANSACTIONS;

PROCEDURE UPDATE_ACCOUNTING_TRANSACTIONS
(
IN_ACCOUNTING_TRANSACTION_ID	IN ACCOUNTING_TRANSACTIONS.ACCOUNTING_TRANSACTION_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ACCOUNTING_TRANSACTIONS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ACCOUNTING_TRANSACTIONS.ADD_ON_AMOUNT%TYPE,
IN_SHIPPING_FEE                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_FEE%TYPE,
IN_SERVICE_FEE                  IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ACCOUNTING_TRANSACTIONS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_TAX%TYPE,
IN_SERVICE_FEE_TAX              IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE_TAX%TYPE,
IN_TAX                          IN ACCOUNTING_TRANSACTIONS.TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ACCOUNTING_TRANSACTIONS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ACCOUNTING_TRANSACTIONS.WHOLESALE_AMOUNT%TYPE,
IN_ADMIN_FEE                    IN ACCOUNTING_TRANSACTIONS.ADMIN_FEE%TYPE,
IN_WHOLESALE_SERVICE_FEE        IN ACCOUNTING_TRANSACTIONS.WHOLESALE_SERVICE_FEE%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ACCOUNTING_TRANSACTIONS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the accounting transaction record.

Input:
        accounting_transaction_id       number
        product_amount                  number
        add_on_amount                   number
        shipping_fee			number
	service_fee			number
        discount_amount			number
 	shipping_tax			number
	service_fee_tax			number
	tax				number
 	commission_amount		number
	wholesale_amount		number
	wholesale_service_fee		number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  accounting_transactions
SET
        product_amount = nvl(in_product_amount, 0),
        add_on_amount = nvl(in_add_on_amount, 0),
	shipping_fee = nvl(in_shipping_fee, 0),
 	service_fee = nvl(in_service_fee, 0),
	discount_amount = nvl(in_discount_amount, 0),
	shipping_tax = nvl(in_shipping_tax, 0),
	service_fee_tax = nvl(in_service_fee_tax, 0),
	tax = nvl(in_tax, 0),
	commission_amount = nvl(in_commission_amount, 0),
	wholesale_amount = nvl(in_wholesale_amount, 0),
	admin_fee = nvl(in_admin_fee, 0),
	wholesale_service_fee = nvl(in_wholesale_service_fee, 0),
	ADD_ON_DISCOUNT_AMOUNT = nvl(IN_ADD_ON_DISCOUNT_AMOUNT,0)
WHERE   accounting_transaction_id = in_accounting_transaction_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ACCOUNTING_TRANSACTIONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ACCOUNTING_TRANSACTIONS;

PROCEDURE UPDATE_ORDER_TRACKING
(
IN_TRACKING_NUMBER              IN ORDER_TRACKING.TRACKING_NUMBER%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_TRACKING.ORDER_DETAIL_ID%TYPE,
IN_TRACKING_DESCRIPTION         IN ORDER_TRACKING.TRACKING_DESCRIPTION%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting order
        tracking information

Input:
        tracking_number                 varchar2
        order_detail_id                 number
        tracking_description            varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_check         number;

CURSOR check_cur IS
        SELECT  1
        FROM    order_tracking
        WHERE   order_detail_id = in_order_detail_id;


BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

IF v_check IS NOT NULL THEN
        UPDATE  order_tracking
        SET     tracking_number = in_tracking_number,
                tracking_description = NVL(in_tracking_description, tracking_description)
        WHERE   order_detail_id = in_order_detail_id;

ELSE
        INSERT INTO order_tracking
        (
                tracking_number,
                order_detail_id,
                tracking_description
        )
        VALUES
        (
                in_tracking_number,
                in_order_detail_id,
                in_tracking_description
        );
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_TRACKING [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_TRACKING;


PROCEDURE UPDATE_ORDER_TRACKING
(
IN_TRACKING_NUMBER              IN ORDER_TRACKING.TRACKING_NUMBER%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_TRACKING.ORDER_DETAIL_ID%TYPE,
IN_TRACKING_DESCRIPTION         IN ORDER_TRACKING.TRACKING_DESCRIPTION%TYPE,
IN_CARRIER_ID                   IN VENUS.CARRIERS.CARRIER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating or inserting order
        tracking information

Input:
        tracking_number                 varchar2
        order_detail_id                 number
        tracking_description            varchar2
        carrier_id                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_check         number;
v_carrier_id    venus.carriers.carrier_id%TYPE;

CURSOR check_cur IS
        SELECT  1
        FROM    order_tracking
        WHERE   order_detail_id = in_order_detail_id;


BEGIN

SELECT DECODE(in_carrier_id,NULL,'FEDEX', UPPER(in_carrier_id)) INTO v_carrier_id
FROM dual;

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

IF v_check IS NOT NULL THEN
        UPDATE  order_tracking
        SET     tracking_number = in_tracking_number,
                tracking_description = NVL(in_tracking_description, tracking_description),
                carrier_id = v_carrier_id
        WHERE   order_detail_id = in_order_detail_id;

ELSE
        INSERT INTO order_tracking
        (
                tracking_number,
                order_detail_id,
                tracking_description,
                carrier_id
        )
        VALUES
        (
                in_tracking_number,
                in_order_detail_id,
                in_tracking_description,
                v_carrier_id
        );
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_TRACKING [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_TRACKING;


PROCEDURE INSERT_ORDER_FLORIST_USED
(
 IN_ORDER_DETAIL_ID 		IN NUMBER,
 IN_FLORIST_ID      		IN VARCHAR2,
 IN_SELECTION_DATA      	IN VARCHAR2,
 OUT_STATUS        		OUT VARCHAR2,
 OUT_ERROR_MESSAGE 		OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table order_florist_used.

Input:
        order_detail_id, florist_id, selection_data

Output:
        status
        error message

-----------------------------------------------------------------------------*/
v_sequence NUMBER;

BEGIN

  SELECT MAX(sequence)
    INTO v_sequence
    FROM order_florist_used
   WHERE order_detail_id = in_order_detail_id;

  IF v_sequence IS NULL THEN
    v_sequence := 0;
  END IF;

  INSERT INTO order_florist_used
     (order_detail_id,
      florist_id,
      sequence,
      created_on,
      SELECTION_DATA)
    VALUES
      (in_order_detail_id,
       in_florist_id,
       v_sequence + 1,
       SYSDATE,
       IN_SELECTION_DATA);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED IN INSERT_ORDER_FLORIST_USED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    ROLLBACK;
  END;

END INSERT_ORDER_FLORIST_USED;

PROCEDURE UPDATE_ORDER_DISPOSITION
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail
        disposition code

Input:
        order_detail_id                 number
        order_disp_code                 varchar2
        updated_by                      varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_details
SET
        order_disp_code = in_order_disp_code,
        updated_on = sysdate,
        updated_by = in_updated_by
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_DISPOSITION [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DISPOSITION;


PROCEDURE UPDATE_EOD_DELIVERY_IND
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_INDICATOR                    IN ORDER_DETAILS.EOD_DELIVERY_INDICATOR%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail
        eod_delivery_indicator

Input:
        order_detail_id                 number
        indicator                       varchar2
        updated_by                      varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_details
SET
        eod_delivery_indicator = in_indicator,
        updated_on = sysdate,
        updated_by = in_updated_by
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_EOD_DELIVERY_IND [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END  UPDATE_EOD_DELIVERY_IND;




PROCEDURE INSERT_ORDERS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_MASTER_ORDER_NUMBER          IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
IN_CUSTOMER_ID                  IN ORDERS.CUSTOMER_ID%TYPE,
IN_MEMBERSHIP_ID                IN ORDERS.MEMBERSHIP_ID%TYPE,
IN_COMPANY_ID                   IN ORDERS.COMPANY_ID%TYPE,
IN_SOURCE_CODE                  IN ORDERS.SOURCE_CODE%TYPE,
IN_ORIGIN_ID                    IN ORDERS.ORIGIN_ID%TYPE,
IN_ORDER_DATE                   IN ORDERS.ORDER_DATE%TYPE,
IN_CREATED_BY                   IN ORDERS.CREATED_BY%TYPE,
IN_LOSS_PREVENTION_INDICATOR    IN ORDERS.LOSS_PREVENTION_INDICATOR%TYPE,
IN_EMAIL_ID                     IN ORDERS.EMAIL_ID%TYPE,
IN_ORDER_TAKEN_BY               IN ORDERS.ORDER_TAKEN_BY%TYPE,
IN_PARTNERSHIP_BONUS_POINTS     IN ORDERS.PARTNERSHIP_BONUS_POINTS%TYPE,
IN_FRAUD_INDICATOR              IN ORDERS.FRAUD_INDICATOR%TYPE,
IN_ARIBA_BUYER_ASN_NUMBER       IN ORDERS.ARIBA_BUYER_ASN_NUMBER%TYPE,
IN_ARIBA_BUYER_COOKIE           IN ORDERS.ARIBA_BUYER_COOKIE%TYPE,
IN_ARIBA_PAYLOAD                IN ORDERS.ARIBA_PAYLOAD%TYPE,
IN_CO_BRAND_CREDIT_CARD_CODE    IN ORDERS.CO_BRAND_CREDIT_CARD_CODE%TYPE,
IN_WEBOE_DNIS_ID                IN ORDERS.WEBOE_DNIS_ID%TYPE,
IN_ORDER_TAKEN_CALL_CENTER      IN ORDERS.ORDER_TAKEN_CALL_CENTER%TYPE,
IN_MP_REDEMPTION_RATE_AMT       IN ORDERS.MP_REDEMPTION_RATE_AMT%TYPE,
IN_BUYER_SIGNED_IN_FLAG         IN ORDERS.BUYER_SIGNED_IN_FLAG%TYPE,
IN_CHARACTER_MAPPING_FLAG       IN ORDERS.CHARACTER_MAPPING_FLAG%TYPE,
IN_LANGUAGE_ID         			IN ORDERS.LANGUAGE_ID%TYPE,
IN_JOINT_CART					IN ORDERS.IS_JOINT_CART%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the shopping cart record

Input:
        order_guid                      varchar2
        master_order_number             varchar2
        customer_id                     number
        membership_id                   number
        company_id                      varchar2
        source_code                     varchar2
        origin_id                       varchar2
        order_date                      date
        created_on                      date
        created_by                      varchar2
        updated_on                      date
        updated_by                      varchar2
        loss_prevention_indicator       char
        email_id                        number
        order_taken_by                  varchar2
        partnership_bonus_points        number
        fraud_indicator                 char
        ariba_buyer_asn_number          varchar2
        ariba_buyer_cookie              varchar2
        ariba_payload                   varchar2
        co_brand_credit_card_code       varchar2
        weboe_dnis_id                   number
        order_taken_call_center         varchar2
        mp_redemption_rate_amt          number
        character_mapping_flag          varchar2
		language_id						varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO orders
(
        order_guid,
        master_order_number,
        customer_id,
        membership_id,
        company_id,
        source_code,
        origin_id,
        order_date,
        created_on,
        created_by,
        updated_on,
        updated_by,
        loss_prevention_indicator,
        email_id,
        order_taken_by,
        partnership_bonus_points,
        fraud_indicator,
        ariba_buyer_asn_number,
        ariba_buyer_cookie,
        ariba_payload,
        co_brand_credit_card_code,
        weboe_dnis_id,
        order_taken_call_center,
        mp_redemption_rate_amt,
        buyer_signed_in_flag,
        character_mapping_flag,
		language_id,
		is_joint_cart

)
VALUES
(
        in_order_guid,
        in_master_order_number,
        in_customer_id,
        in_membership_id,
        in_company_id,
        in_source_code,
        in_origin_id,
        in_order_date,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_loss_prevention_indicator,
        in_email_id,
        in_order_taken_by,
        in_partnership_bonus_points,
        nvl (in_fraud_indicator, 'N'),
        in_ariba_buyer_asn_number,
        in_ariba_buyer_cookie,
        in_ariba_payload,
        in_co_brand_credit_card_code,
        in_weboe_dnis_id,
        in_order_taken_call_center,
        in_mp_redemption_rate_amt ,
        in_buyer_signed_in_flag,
        in_character_mapping_flag,
		in_language_id,
		in_joint_cart
     );

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDERS;

PROCEDURE UPDATE_UNIQUE_CC_CRYPT
(
IN_CC_TYPE                      IN CREDIT_CARDS.CC_TYPE%TYPE,
IN_CC_NUMBER                    IN CREDIT_CARDS.CC_NUMBER%TYPE,
IN_CC_EXPIRATION                IN CREDIT_CARDS.CC_EXPIRATION%TYPE,
IN_NAME                         IN CREDIT_CARDS.NAME%TYPE,
IN_PIN                          IN CREDIT_CARDS.GIFT_CARD_PIN%TYPE,
IN_ADDRESS_LINE_1               IN CREDIT_CARDS.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN CREDIT_CARDS.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN CREDIT_CARDS.CITY%TYPE,
IN_STATE                        IN CREDIT_CARDS.STATE%TYPE,
IN_ZIP_CODE                     IN CREDIT_CARDS.ZIP_CODE%TYPE,
IN_COUNTRY                      IN CREDIT_CARDS.COUNTRY%TYPE,
IN_UPDATED_BY                   IN CREDIT_CARDS.UPDATED_BY%TYPE,
IN_CUSTOMER_ID                  IN CREDIT_CARDS.CUSTOMER_ID%TYPE,
IN_KEY_NAME                     IN CREDIT_CARDS.KEY_NAME%TYPE,
IN_CC_NUMBER_MASKED             IN CREDIT_CARDS.CC_NUMBER_MASKED%TYPE,
OUT_CC_ID                       OUT CREDIT_CARDS.CC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating credit card
        information based on the customer id, card type, and card number

        Assume the cc_number input parameter is already encrypted.
        If this isn't true, use update_unique_credit_cards instead.

        MUST use the correct key_name as input, since we're assuming the cc_number
        is encrypted.  That way we can store the key_name with the cc_number.

Input:
        cc_type                         varchar2
        cc_number                       varchar2 (cc number comes in plain text)
        cc_expiration                   varchar2
        name                            varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        updated_by                      varchar2
        customer_id                     number
        key_name                        varchar2
        cc_number_masked                varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

/* cc_number is passed encrypted to this procedure, so we don't have to encrypt it to check existence */
CURSOR exists_cur IS
        SELECT  cc_id
        FROM    credit_cards
        WHERE   customer_id = in_customer_id
        AND     cc_type = in_cc_type
        AND     nvl(cc_number,1) = decode(ftd_apps.payment_method_is_cc(in_cc_type),'Y',in_cc_number,1);

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO out_cc_id;
CLOSE exists_cur;

IF out_cc_id IS NOT NULL THEN
        UPDATE  credit_cards
        SET
                cc_expiration = NVL(in_cc_expiration, cc_expiration),
                gift_card_pin = NVL(in_pin, gift_card_pin),
                name = NVL(in_name, name),
                address_line_1 = NVL(in_address_line_1, address_line_1),
                address_line_2 = NVL(in_address_line_2, address_line_2),
                city = NVL(in_city, city),
                state = NVL(in_state, state),
                zip_code = NVL(in_zip_code, zip_code),
                country = NVL(in_country, country),
                updated_on = sysdate,
                updated_by = in_updated_by
        WHERE   cc_id = out_cc_id;

ELSE

        INSERT INTO credit_cards
        (
                cc_id,
                cc_type,
                cc_number,
                key_name,
                cc_expiration,
                gift_card_pin,
                name,
                address_line_1,
                address_line_2,
                city,
                state,
                zip_code,
                country,
                created_on,
                created_by,
                updated_on,
                updated_by,
                customer_id,
                cc_number_masked
        )
        VALUES
        (
                key.keygen ('CREDIT_CARDS'),
                in_cc_type,
                decode(ftd_apps.payment_method_is_cc(in_cc_type),'Y',in_cc_number,null),
                in_key_name,
                in_cc_expiration,
                in_pin,
                in_name,
                in_address_line_1,
                in_address_line_2,
                in_city,
                in_state,
                in_zip_code,
                in_country,
                sysdate,
                in_updated_by,
                sysdate,
                in_updated_by,
                in_customer_id,
                in_cc_number_masked
        ) RETURNING cc_id INTO out_cc_id;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_UNIQUE_CREDIT_CARDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_UNIQUE_CC_CRYPT;

PROCEDURE UPDATE_UNIQUE_CREDIT_CARDS
(
IN_CC_TYPE                      IN CREDIT_CARDS.CC_TYPE%TYPE,
IN_CC_NUMBER                    IN CREDIT_CARDS.CC_NUMBER%TYPE,
IN_CC_EXPIRATION                IN CREDIT_CARDS.CC_EXPIRATION%TYPE,
IN_NAME                         IN CREDIT_CARDS.NAME%TYPE,
IN_ADDRESS_LINE_1               IN CREDIT_CARDS.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN CREDIT_CARDS.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN CREDIT_CARDS.CITY%TYPE,
IN_STATE                        IN CREDIT_CARDS.STATE%TYPE,
IN_ZIP_CODE                     IN CREDIT_CARDS.ZIP_CODE%TYPE,
IN_COUNTRY                      IN CREDIT_CARDS.COUNTRY%TYPE,
IN_UPDATED_BY                   IN CREDIT_CARDS.UPDATED_BY%TYPE,
IN_CUSTOMER_ID                  IN CREDIT_CARDS.CUSTOMER_ID%TYPE,
OUT_CC_ID                       OUT CREDIT_CARDS.CC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating credit card
        information based on the customer id, card type, and card number

        This is the procedure to use if your credit card number is in plaintext -
        it will encrypt it for you

Input:
        cc_type                         varchar2
        cc_number                       varchar2 (cc number comes in plain text)
        cc_expiration                   varchar2
        name                            varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        updated_by                      varchar2
        customer_id                     number
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_key_name credit_cards.key_name%type;

BEGIN

v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

UPDATE_UNIQUE_CC_CRYPT (
   IN_CC_TYPE                      => in_cc_type,
   IN_CC_NUMBER                    => global.encryption.encrypt_it(in_cc_number, v_key_name),
   IN_CC_EXPIRATION                => in_cc_expiration,
   IN_NAME                         => in_name,
   IN_PIN                          => null,
   IN_ADDRESS_LINE_1               => in_address_line_1,
   IN_ADDRESS_LINE_2               => in_address_line_2,
   IN_CITY                         => in_city,
   IN_STATE                        => in_state,
   IN_ZIP_CODE                     => in_zip_code,
   IN_COUNTRY                      => IN_COUNTRY,
   IN_UPDATED_BY                   => IN_UPDATED_BY,
   IN_CUSTOMER_ID                  => IN_CUSTOMER_ID,
   IN_KEY_NAME                     => v_key_name,
   IN_CC_NUMBER_MASKED             => substr(in_cc_number, -4, 4),
   OUT_CC_ID                       => OUT_CC_ID,
   OUT_STATUS                      => OUT_STATUS,
   OUT_MESSAGE                     => OUT_MESSAGE);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_UNIQUE_CREDIT_CARDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_UNIQUE_CREDIT_CARDS;



PROCEDURE UPDATE_UNIQUE_CREDIT_CARDS
(
IN_CC_TYPE                      IN CREDIT_CARDS.CC_TYPE%TYPE,
IN_CC_NUMBER                    IN CREDIT_CARDS.CC_NUMBER%TYPE,
IN_CC_EXPIRATION                IN CREDIT_CARDS.CC_EXPIRATION%TYPE,
IN_UPDATED_BY                   IN CREDIT_CARDS.UPDATED_BY%TYPE,
IN_CUSTOMER_ID                  IN CREDIT_CARDS.CUSTOMER_ID%TYPE,
OUT_CC_ID                       OUT CREDIT_CARDS.CC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating credit card
        information based on the customer id, card type, and card number
        overloading the procedure above.

Input:
        cc_type                         varchar2
        cc_number                       varchar2 (cc number comes in plain text)
        cc_expiration                   varchar2
        updated_by                      varchar2
        customer_id                     number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR cust_cur IS
        SELECT  c.first_name || ' ' || c.last_name name,
                c.address_1 address_line_1,
                c.address_2 address_line_2,
                c.city,
                c.state,
                c.zip_code,
                c.country
        FROM    customer c
        WHERE   c.customer_id = in_customer_id;

cust_row        cust_cur%rowtype;

BEGIN

OPEN cust_cur;
FETCH cust_cur INTO cust_row;
CLOSE cust_cur;

UPDATE_UNIQUE_CREDIT_CARDS
(
        IN_CC_TYPE=>in_cc_type,
        IN_CC_NUMBER=>in_cc_number,
        IN_CC_EXPIRATION=>in_cc_expiration,
        IN_NAME=>cust_row.name,
        IN_ADDRESS_LINE_1=>cust_row.address_line_1,
        IN_ADDRESS_LINE_2=>cust_row.address_line_2,
        IN_CITY=>cust_row.city,
        IN_STATE=>cust_row.state,
        IN_ZIP_CODE=>cust_row.zip_code,
        IN_COUNTRY=>cust_row.country,
        IN_UPDATED_BY=>in_updated_by,
        IN_CUSTOMER_ID=>in_customer_id,
        OUT_CC_ID=>out_cc_id,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);


EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_UNIQUE_CREDIT_CARDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_UNIQUE_CREDIT_CARDS;

PROCEDURE UPDATE_PAYMENTS
(
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
IN_ADDITIONAL_BILL_ID           IN PAYMENTS.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS.AUTH_NUMBER%TYPE,
IN_AVS_CODE                     IN PAYMENTS.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS.AUTH_DATE%TYPE,
IN_AUTH_CHAR_INDICATOR          IN PAYMENTS.AUTH_CHAR_INDICATOR%TYPE,
IN_TRANSACTION_ID               IN PAYMENTS.TRANSACTION_ID%TYPE,
IN_VALIDATION_CODE              IN PAYMENTS.VALIDATION_CODE%TYPE,
IN_AUTH_SOURCE_CODE             IN PAYMENTS.AUTH_SOURCE_CODE%TYPE,
IN_RESPONSE_CODE                IN PAYMENTS.RESPONSE_CODE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS.AAFES_TICKET_NUMBER%TYPE,
IN_AP_ACCOUNT_ID                IN PAYMENTS.AP_ACCOUNT_ID%TYPE,
IN_AP_AUTH_TXT                  IN PAYMENTS.AP_AUTH_TXT%TYPE,
IN_NC_APPROVAL_IDENTITY_ID      IN PAYMENTS.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_NC_TYPE_CODE                 IN PAYMENTS.NC_TYPE_CODE%TYPE,
IN_NC_REASON_CODE               IN PAYMENTS.NC_REASON_CODE%TYPE,
IN_NC_ORDER_DETAIL_ID           IN PAYMENTS.NC_ORDER_DETAIL_ID%TYPE,
IN_MILES_POINTS_CREDIT_AMT      IN PAYMENTS.MILES_POINTS_CREDIT_AMT%TYPE,
IN_MILES_POINTS_DEBIT_AMT       IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
IN_CSC_RESPONSE_CODE            IN PAYMENTS.CSC_RESPONSE_CODE%TYPE,
IN_CSC_VALIDATED_FLAG           IN PAYMENTS.CSC_VALIDATED_FLAG%TYPE,
IN_CSC_FAILURE_CNT              IN PAYMENTS.CSC_FAILURE_CNT%TYPE,
IN_BILL_STATUS					IN PAYMENTS.BILL_STATUS%TYPE,
IN_BILL_DATE					IN PAYMENTS.BILL_DATE%TYPE,
IN_WALLET_INDICATOR             IN PAYMENTS.WALLET_INDICATOR%TYPE,
IN_CC_AUTH_PROVIDER				IN PAYMENTS.CC_AUTH_PROVIDER%TYPE,
IN_INITIAL_AUTH_AMOUNT			IN PAYMENTS.INITIAL_AUTH_AMOUNT%TYPE,
IN_PAYMENT_EXT_INFO				IN LONG,
IN_IS_VOICE_AUTH				IN PAYMENTS.IS_VOICE_AUTH%TYPE,
IN_CARDINAL_VERIFIED_FLAG       IN PAYMENTS.CARDINAL_VERIFIED_FLAG%TYPE,
IN_ROUTE						IN PAYMENTS.ROUTE%TYPE,
IN_TOKEN_ID                     IN PAYMENTS.TOKEN_ID%TYPE,
IN_AUTH_TRANSACTION_ID          IN PAYMENTS.AUTHORIZATION_TRANSACTION_ID%TYPE,
IO_PAYMENT_ID                   IN OUT PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_PAYMENTS
(
IN_ORDER_GUID,
IN_ADDITIONAL_BILL_ID,
IN_PAYMENT_TYPE,
IN_CC_ID,
IN_UPDATED_BY,
IN_AUTH_RESULT,
IN_AUTH_NUMBER,
IN_AVS_CODE,
IN_ACQ_REFERENCE_NUMBER,
IN_GC_COUPON_NUMBER,
IN_AUTH_OVERRIDE_FLAG,
IN_CREDIT_AMOUNT,
IN_DEBIT_AMOUNT,
IN_PAYMENT_INDICATOR,
IN_REFUND_ID,
IN_AUTH_DATE,
IN_AUTH_CHAR_INDICATOR,
IN_TRANSACTION_ID,
IN_VALIDATION_CODE,
IN_AUTH_SOURCE_CODE,
IN_RESPONSE_CODE,
IN_AAFES_TICKET_NUMBER,
IN_AP_ACCOUNT_ID,
IN_AP_AUTH_TXT,
IN_NC_APPROVAL_IDENTITY_ID,
IN_NC_TYPE_CODE,
IN_NC_REASON_CODE,
IN_NC_ORDER_DETAIL_ID,
IN_MILES_POINTS_CREDIT_AMT,
IN_MILES_POINTS_DEBIT_AMT,
IN_CSC_RESPONSE_CODE,
IN_CSC_VALIDATED_FLAG,
IN_CSC_FAILURE_CNT,
IN_BILL_STATUS,
IN_BILL_DATE,
IN_WALLET_INDICATOR,
IN_CC_AUTH_PROVIDER,
IN_INITIAL_AUTH_AMOUNT,
IN_PAYMENT_EXT_INFO,
IN_IS_VOICE_AUTH,
IN_CARDINAL_VERIFIED_FLAG,
IN_ROUTE,
IN_TOKEN_ID,
IN_AUTH_TRANSACTION_ID,
NULL,
NULL,
NULL,
NULL,
IO_PAYMENT_ID,
OUT_STATUS,
OUT_MESSAGE
);
END UPDATE_PAYMENTS;


PROCEDURE UPDATE_PAYMENTS
(
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
IN_ADDITIONAL_BILL_ID           IN PAYMENTS.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS.AUTH_NUMBER%TYPE,
IN_AVS_CODE                     IN PAYMENTS.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS.AUTH_DATE%TYPE,
IN_AUTH_CHAR_INDICATOR          IN PAYMENTS.AUTH_CHAR_INDICATOR%TYPE,
IN_TRANSACTION_ID               IN PAYMENTS.TRANSACTION_ID%TYPE,
IN_VALIDATION_CODE              IN PAYMENTS.VALIDATION_CODE%TYPE,
IN_AUTH_SOURCE_CODE             IN PAYMENTS.AUTH_SOURCE_CODE%TYPE,
IN_RESPONSE_CODE                IN PAYMENTS.RESPONSE_CODE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS.AAFES_TICKET_NUMBER%TYPE,
IN_AP_ACCOUNT_ID                IN PAYMENTS.AP_ACCOUNT_ID%TYPE,
IN_AP_AUTH_TXT                  IN PAYMENTS.AP_AUTH_TXT%TYPE,
IN_NC_APPROVAL_IDENTITY_ID      IN PAYMENTS.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_NC_TYPE_CODE                 IN PAYMENTS.NC_TYPE_CODE%TYPE,
IN_NC_REASON_CODE               IN PAYMENTS.NC_REASON_CODE%TYPE,
IN_NC_ORDER_DETAIL_ID           IN PAYMENTS.NC_ORDER_DETAIL_ID%TYPE,
IN_MILES_POINTS_CREDIT_AMT      IN PAYMENTS.MILES_POINTS_CREDIT_AMT%TYPE,
IN_MILES_POINTS_DEBIT_AMT       IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
IN_CSC_RESPONSE_CODE            IN PAYMENTS.CSC_RESPONSE_CODE%TYPE,
IN_CSC_VALIDATED_FLAG           IN PAYMENTS.CSC_VALIDATED_FLAG%TYPE,
IN_CSC_FAILURE_CNT              IN PAYMENTS.CSC_FAILURE_CNT%TYPE,
IN_BILL_STATUS					IN PAYMENTS.BILL_STATUS%TYPE,
IN_BILL_DATE					IN PAYMENTS.BILL_DATE%TYPE,
IN_WALLET_INDICATOR             IN PAYMENTS.WALLET_INDICATOR%TYPE,
IN_CC_AUTH_PROVIDER				IN PAYMENTS.CC_AUTH_PROVIDER%TYPE,
IN_INITIAL_AUTH_AMOUNT			IN PAYMENTS.INITIAL_AUTH_AMOUNT%TYPE,
IN_PAYMENT_EXT_INFO				IN LONG,
IN_IS_VOICE_AUTH				IN PAYMENTS.IS_VOICE_AUTH%TYPE,
IN_CARDINAL_VERIFIED_FLAG       IN PAYMENTS.CARDINAL_VERIFIED_FLAG%TYPE,
IN_ROUTE						IN PAYMENTS.ROUTE%TYPE,
IN_TOKEN_ID                     IN PAYMENTS.TOKEN_ID%TYPE,
IN_AUTH_TRANSACTION_ID          IN PAYMENTS.AUTHORIZATION_TRANSACTION_ID%TYPE,
IN_MERCHANT_REF_ID          	IN PAYMENTS.MERCHANT_REF_ID%TYPE,
IO_PAYMENT_ID                   IN OUT PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_PAYMENTS
(
IN_ORDER_GUID,
IN_ADDITIONAL_BILL_ID,
IN_PAYMENT_TYPE,
IN_CC_ID,
IN_UPDATED_BY,
IN_AUTH_RESULT,
IN_AUTH_NUMBER,
IN_AVS_CODE,
IN_ACQ_REFERENCE_NUMBER,
IN_GC_COUPON_NUMBER,
IN_AUTH_OVERRIDE_FLAG,
IN_CREDIT_AMOUNT,
IN_DEBIT_AMOUNT,
IN_PAYMENT_INDICATOR,
IN_REFUND_ID,
IN_AUTH_DATE,
IN_AUTH_CHAR_INDICATOR,
IN_TRANSACTION_ID,
IN_VALIDATION_CODE,
IN_AUTH_SOURCE_CODE,
IN_RESPONSE_CODE,
IN_AAFES_TICKET_NUMBER,
IN_AP_ACCOUNT_ID,
IN_AP_AUTH_TXT,
IN_NC_APPROVAL_IDENTITY_ID,
IN_NC_TYPE_CODE,
IN_NC_REASON_CODE,
IN_NC_ORDER_DETAIL_ID,
IN_MILES_POINTS_CREDIT_AMT,
IN_MILES_POINTS_DEBIT_AMT,
IN_CSC_RESPONSE_CODE,
IN_CSC_VALIDATED_FLAG,
IN_CSC_FAILURE_CNT,
IN_BILL_STATUS,
IN_BILL_DATE,
IN_WALLET_INDICATOR,
IN_CC_AUTH_PROVIDER,
IN_INITIAL_AUTH_AMOUNT,
IN_PAYMENT_EXT_INFO,
IN_IS_VOICE_AUTH,
IN_CARDINAL_VERIFIED_FLAG,
IN_ROUTE,
IN_TOKEN_ID,
IN_AUTH_TRANSACTION_ID,
NULL,
IN_MERCHANT_REF_ID,
NULL,
NULL,
IO_PAYMENT_ID,
OUT_STATUS,
OUT_MESSAGE
);
END UPDATE_PAYMENTS;

PROCEDURE UPDATE_PAYMENTS
(
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
IN_ADDITIONAL_BILL_ID           IN PAYMENTS.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS.AUTH_NUMBER%TYPE,
IN_AVS_CODE                     IN PAYMENTS.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS.AUTH_DATE%TYPE,
IN_AUTH_CHAR_INDICATOR          IN PAYMENTS.AUTH_CHAR_INDICATOR%TYPE,
IN_TRANSACTION_ID               IN PAYMENTS.TRANSACTION_ID%TYPE,
IN_VALIDATION_CODE              IN PAYMENTS.VALIDATION_CODE%TYPE,
IN_AUTH_SOURCE_CODE             IN PAYMENTS.AUTH_SOURCE_CODE%TYPE,
IN_RESPONSE_CODE                IN PAYMENTS.RESPONSE_CODE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS.AAFES_TICKET_NUMBER%TYPE,
IN_AP_ACCOUNT_ID                IN PAYMENTS.AP_ACCOUNT_ID%TYPE,
IN_AP_AUTH_TXT                  IN PAYMENTS.AP_AUTH_TXT%TYPE,
IN_NC_APPROVAL_IDENTITY_ID      IN PAYMENTS.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_NC_TYPE_CODE                 IN PAYMENTS.NC_TYPE_CODE%TYPE,
IN_NC_REASON_CODE               IN PAYMENTS.NC_REASON_CODE%TYPE,
IN_NC_ORDER_DETAIL_ID           IN PAYMENTS.NC_ORDER_DETAIL_ID%TYPE,
IN_MILES_POINTS_CREDIT_AMT      IN PAYMENTS.MILES_POINTS_CREDIT_AMT%TYPE,
IN_MILES_POINTS_DEBIT_AMT       IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
IN_CSC_RESPONSE_CODE            IN PAYMENTS.CSC_RESPONSE_CODE%TYPE,
IN_CSC_VALIDATED_FLAG           IN PAYMENTS.CSC_VALIDATED_FLAG%TYPE,
IN_CSC_FAILURE_CNT              IN PAYMENTS.CSC_FAILURE_CNT%TYPE,
IN_BILL_STATUS					        IN PAYMENTS.BILL_STATUS%TYPE,
IN_BILL_DATE					          IN PAYMENTS.BILL_DATE%TYPE,
IN_WALLET_INDICATOR             IN PAYMENTS.WALLET_INDICATOR%TYPE,
IN_CC_AUTH_PROVIDER				IN PAYMENTS.CC_AUTH_PROVIDER%TYPE,
IN_INITIAL_AUTH_AMOUNT			IN PAYMENTS.INITIAL_AUTH_AMOUNT%TYPE,
IN_PAYMENT_EXT_INFO				IN LONG,
IN_IS_VOICE_AUTH				IN PAYMENTS.IS_VOICE_AUTH%TYPE,
IN_CARDINAL_VERIFIED_FLAG       IN PAYMENTS.CARDINAL_VERIFIED_FLAG%TYPE,
IN_ROUTE						IN PAYMENTS.ROUTE%TYPE,
IN_TOKEN_ID                     IN PAYMENTS.TOKEN_ID%TYPE,
IN_AUTH_TRANSACTION_ID          IN PAYMENTS.AUTHORIZATION_TRANSACTION_ID%TYPE,
IN_ASSOCIATED_PAYMENT_ID		IN VARCHAR2,
IN_MERCHANT_REF_ID				IN PAYMENTS.MERCHANT_REF_ID%TYPE,
IN_SETTLEMENT_TRANSACTION_ID    IN PAYMENTS.SETTLEMENT_TRANSACTION_ID%TYPE,
IN_REQUEST_TOKEN          		IN PAYMENTS.REQUEST_TOKEN %TYPE,
IO_PAYMENT_ID                   IN OUT PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting payment information

Input:
        order_guid                      varchar2
        additional_bill_id              number
        payment_type                    varchar2
        cc_id                           number
        updated_by                      varchar2
        auth_result                     varchar2
        auth_number                     varchar2
        avs_code                        varchar2
        acq_reference_number            varchar2
        gc_coupon_number                varchar2
        auth_override_flag              char
        credit_amount                   number
        debit_amount                    number
        payment_indicator               char
        refund_id                       number
        auth_date                       date
        auth_char_indicator             char
        transaction_id                  varchar2
        validation_code                 varchar2
        auth_source_code                char
        response_code                   varchar2
        payment_id                      number
        aafes_ticket_number             varchar2
        ap_account_id                   varchar2
        ap_auth_id_txt                  varchar2
        nc_approval_identity_id         varchar2
        nc_type_code                    varchar2
        nc_reason_code                  varchar2
        nc_order_detail_id              varchar2
        miles_points_credit_amt         number
        miles_points_debit_amt          number
	in_csc_response_code            varchar2
	in_csc_validated_flag           varchar2
	in_csc_failure_cnt              number
	IN_CARDINAL_VERIFIED_FLAG       varchar2
	IN_ROUTE						varchar2
	IN_TOKEN_ID
	IN_AUTH_TRANSACTION_ID
	bill_status						varchar2
	bill_date						date

Output:
        payment_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
stamp_cc_auth_provider payments.cc_auth_provider%type;
gl_parm_cc_auth_provider frp.global_parms.value%type;

is_bams_cc varchar2(1) := 'N';
payment_route VARCHAR2(20); 
auth_trans_id VARCHAR2(100); 
settl_trans_id VARCHAR2(100);
auth_number VARCHAR2(20);
request_token VARCHAR2(4000);
merchant_ref_id VARCHAR2(100);

BEGIN

gl_parm_cc_auth_provider := frp.misc_pkg.get_global_parm_value('AUTH_CONFIG','cc.auth.provider');
is_bams_cc := frp.misc_pkg.is_bams_cc(IN_PAYMENT_TYPE);

IF gl_parm_cc_auth_provider='BAMS' AND is_bams_cc='Y' AND (IN_IS_VOICE_AUTH='Y' OR IN_PAYMENT_INDICATOR='R') THEN
	stamp_cc_auth_provider := gl_parm_cc_auth_provider;
ELSE
	stamp_cc_auth_provider := in_cc_auth_provider;
END IF;


IF io_payment_id IS NOT NULL AND order_query_pkg.payment_exists (io_payment_id) = 'Y' THEN
            UPDATE  payments
            SET
                    additional_bill_id = NVL(in_additional_bill_id, additional_bill_id),
                    payment_type = NVL(in_payment_type, payment_type),
                    cc_id = NVL(in_cc_id, cc_id),
                    updated_on = sysdate,
                    updated_by = in_updated_by,
                    auth_result = in_auth_result,
                    auth_number = in_auth_number,
                    avs_code = NVL(in_avs_code, avs_code),
                    acq_reference_number = NVL(in_acq_reference_number, acq_reference_number),
                    gc_coupon_number = NVL(in_gc_coupon_number, gc_coupon_number),
                    auth_override_flag = NVL(in_auth_override_flag, auth_override_flag),
                    credit_amount = NVL(in_credit_amount, credit_amount),
                    debit_amount = NVL(in_debit_amount, debit_amount),
                    payment_indicator = NVL(in_payment_indicator, payment_indicator),
                    refund_id = NVL(in_refund_id, refund_id),
                    auth_date = NVL(in_auth_date, auth_date),
                    auth_char_indicator = NVL(in_auth_char_indicator, auth_char_indicator),
                    transaction_id = NVL(in_transaction_id, transaction_id),
                    validation_code = NVL(in_validation_code, validation_code),
                    auth_source_code = NVL(in_auth_source_code, auth_source_code),
                    response_code = NVL (in_response_code, response_code),
                    aafes_ticket_number = NVL (in_aafes_ticket_number, aafes_ticket_number),
                    ap_account_id = NVL(in_ap_account_id, ap_account_id),
                    ap_auth_txt = NVL(in_ap_auth_txt, ap_auth_txt),
                    nc_approval_identity_id = NVL(in_nc_approval_identity_id, nc_approval_identity_id),
                    nc_type_code = NVL(in_nc_type_code, nc_type_code),
                    nc_reason_code = NVL(in_nc_reason_code, nc_reason_code),
                    nc_order_detail_id = NVL(in_nc_order_detail_id, nc_order_detail_id),
                    miles_points_credit_amt = NVL(in_miles_points_credit_amt, miles_points_credit_amt),
                    miles_points_debit_amt = NVL(in_miles_points_debit_amt, miles_points_debit_amt),
                    csc_response_code = NVL(in_csc_response_code, csc_response_code),
              csc_validated_flag = COALESCE(in_csc_validated_flag, csc_validated_flag,'N'),
	            csc_failure_cnt = NVL(in_csc_failure_cnt, csc_failure_cnt),
	            bill_status = NVL(in_bill_status, bill_status),
	            bill_date = NVL(in_bill_date, bill_date),
	            cc_auth_provider = stamp_cc_auth_provider,
	            initial_auth_amount = case when stamp_cc_auth_provider is null then initial_auth_amount else IN_INITIAL_AUTH_AMOUNT end,
	            is_voice_auth = IN_IS_VOICE_AUTH,
	            cardinal_verified_flag = IN_CARDINAL_VERIFIED_FLAG,
	            route	= IN_ROUTE,
	            token_id = IN_TOKEN_ID,
	            authorization_transaction_id = IN_AUTH_TRANSACTION_ID,
	            MERCHANT_REF_ID = in_merchant_ref_id,
	            SETTLEMENT_TRANSACTION_ID =  IN_SETTLEMENT_TRANSACTION_ID,
             	REQUEST_TOKEN = IN_REQUEST_TOKEN
            WHERE   payment_id = io_payment_id;
ELSE

        IF io_payment_id IS NULL THEN
                io_payment_id := key.keygen ('PAYMENTS');
        END IF;
        
    IF IN_ASSOCIATED_PAYMENT_ID IS NOT NULL THEN
			select 
			ROUTE,AUTHORIZATION_TRANSACTION_ID,SETTLEMENT_TRANSACTION_ID, AUTH_NUMBER, REQUEST_TOKEN, MERCHANT_REF_ID
      INTO payment_route, auth_trans_id, settl_trans_id, auth_number, request_token, merchant_ref_id
			from 
			PAYMENTS 
			where payment_id = IN_ASSOCIATED_PAYMENT_ID;
		END IF;
		IF IN_ROUTE IS NOT NULL THEN
        	payment_route := IN_ROUTE;
        END IF;
        IF in_auth_transaction_id IS NOT NULL THEN
        	auth_trans_id := in_auth_transaction_id;
        END IF;
        IF in_auth_number IS NOT NULL THEN
        	auth_number := in_auth_number;
        END IF;        
		IF IN_MERCHANT_REF_ID IS NOT NULL THEN
        	merchant_ref_id := IN_MERCHANT_REF_ID;
        END IF;
		
		IF IN_SETTLEMENT_TRANSACTION_ID IS NOT NULL THEN
        	settl_trans_id := IN_SETTLEMENT_TRANSACTION_ID;
        END IF;
        
        IF IN_REQUEST_TOKEN IS NOT NULL THEN
        	request_token := IN_REQUEST_TOKEN;
        END IF;
		
        INSERT INTO payments
        (
                payment_id,
                order_guid,
                additional_bill_id,
                payment_type,
                cc_id,
                created_on,
                created_by,
                updated_on,
                updated_by,
                auth_result,
                auth_number,
                avs_code,
                acq_reference_number,
                gc_coupon_number,
                auth_override_flag,
                credit_amount,
                debit_amount,
                payment_indicator,
                refund_id,
                auth_date,
                auth_char_indicator,
                transaction_id,
                validation_code,
                auth_source_code,
                response_code,
                aafes_ticket_number,
                ap_account_id,
                ap_auth_txt,
                nc_approval_identity_id,
                nc_type_code,
                nc_reason_code,
                nc_order_detail_id,
                miles_points_credit_amt,
                miles_points_debit_amt,
		csc_response_code,
		csc_validated_flag,
		csc_failure_cnt,
		bill_status,
		bill_date,
                payment_transaction_id,
                wallet_indicator,
                cc_auth_provider,
                initial_auth_amount,
                is_voice_auth,
                cardinal_verified_flag,
                route,
                token_id,
                authorization_transaction_id,
                settlement_transaction_id,
                request_token,
                merchant_ref_id
        )
        VALUES
        (
                io_payment_id,
                in_order_guid,
                in_additional_bill_id,
                in_payment_type,
                in_cc_id,
                sysdate,
                in_updated_by,
                sysdate,
                in_updated_by,
                in_auth_result,
                auth_number,
                in_avs_code,
                in_acq_reference_number,
                in_gc_coupon_number,
                in_auth_override_flag,
                in_credit_amount,
                in_debit_amount,
                in_payment_indicator,
                in_refund_id,
                in_auth_date,
                in_auth_char_indicator,
                in_transaction_id,
                in_validation_code,
                in_auth_source_code,
                in_response_code,
                in_aafes_ticket_number,
                in_ap_account_id,
                in_ap_auth_txt,
                in_nc_approval_identity_id,
                in_nc_type_code,
                in_nc_reason_code,
                in_nc_order_detail_id,
                in_miles_points_credit_amt,
                in_miles_points_debit_amt,
				in_csc_response_code,
				NVL(in_csc_validated_flag,'N'),
				in_csc_failure_cnt,
				NVL(in_bill_status,'Unbilled'),
				in_bill_date,
				payment_transaction_id_sq.nextval,
        		in_wallet_indicator,
        		stamp_cc_auth_provider,
        		IN_INITIAL_AUTH_AMOUNT,
        		IN_IS_VOICE_AUTH,
        		IN_CARDINAL_VERIFIED_FLAG,
        		payment_route,
        	in_token_id,
        	auth_trans_id,
        	settl_trans_id,
        	request_token,
        	merchant_ref_id
        );

        IF stamp_cc_auth_provider='BAMS' AND in_refund_id IS NOT NULL AND in_payment_indicator = 'R' THEN
            --create a unique reference number for refund
            INSERT INTO PAYMENTS_EXT(PAYMENT_ID,AUTH_PROPERTY_NAME,AUTH_PROPERTY_VALUE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
                SELECT io_payment_id,'RefNum',key.keygen ('jccas_ref_num'),sysdate,in_updated_by,sysdate,in_updated_by
        		FROM DUAL
        		WHERE NOT EXISTS(SELECT 1 FROM PAYMENTS_EXT WHERE PAYMENT_ID=io_payment_id AND AUTH_PROPERTY_NAME='RefNum');
        END IF;
END IF;

IF IN_PAYMENT_EXT_INFO IS NOT NULL AND IN_AUTH_RESULT='AP' THEN
	EXECUTE IMMEDIATE GLOBAL.GLOBAL_PKG.INSERT_PAYMENT_EXT_SCRIPT(io_payment_id,IN_PAYMENT_EXT_INFO,'###','&&&','OP');
END IF;

IF in_additional_bill_id IS NULL and in_refund_id IS NULL THEN
        -- this is an original payment at the cart level,
        -- update the accounting transaction records for the associated
        -- order bills of the cart
        update  accounting_transactions at
        set     at.payment_type = in_payment_type
        where   exists
        (
                select  1
                from    order_bills ob
                where   ob.order_bill_id = at.order_bill_id
                and     exists
                (       select  1
                        from    order_details od
                        where   od.order_detail_id = ob.order_detail_id
                        and     od.order_guid = in_order_guid
                )
        );
END IF;

IF in_additional_bill_id IS NOT NULL THEN
        -- this is an additional bill payment at the order level,
        -- update the accounting transaction record that is associated to the specific order bill
        update  accounting_transactions at
        set     at.payment_type = in_payment_type
        where   at.order_bill_id = in_additional_bill_id;
END IF;

IF in_refund_id IS NOT NULL THEN
        -- this is a refund payment at the order level,
        -- update the accounting transaction record that is associated to the specific refund
        update  accounting_transactions at
        set     at.payment_type = in_payment_type
        where   at.refund_id = in_refund_id;
END IF;

IF stamp_cc_auth_provider='BAMS' AND IN_IS_VOICE_AUTH = 'Y' THEN
	INSERT INTO PAYMENTS_EXT(PAYMENT_ID,AUTH_PROPERTY_NAME,AUTH_PROPERTY_VALUE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
        SELECT io_payment_id,'RefNum',key.keygen ('jccas_ref_num'),sysdate,in_updated_by,sysdate,in_updated_by
        FROM DUAL
        WHERE NOT EXISTS(SELECT 1 FROM PAYMENTS_EXT WHERE PAYMENT_ID=io_payment_id AND AUTH_PROPERTY_NAME='RefNum');
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PAYMENTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PAYMENTS;


PROCEDURE UPDATE_PAYMENTS
(
IN_ADDITIONAL_BILL_ID           IN PAYMENTS.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS.AUTH_NUMBER%TYPE,
IN_AVS_CODE                     IN PAYMENTS.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS.AUTH_DATE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS.AAFES_TICKET_NUMBER%TYPE,
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
IN_MILES_POINTS_CREDIT_AMT      IN PAYMENTS.MILES_POINTS_CREDIT_AMT%TYPE,
IN_MILES_POINTS_DEBIT_AMT       IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
IN_NC_APPROVAL_ID               IN PAYMENTS.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_BILL_STATUS					IN PAYMENTS.BILL_STATUS%TYPE,
IN_BILL_DATE					IN PAYMENTS.BILL_DATE%TYPE,
IN_WALLET_INDICATOR             IN PAYMENTS.WALLET_INDICATOR%TYPE,
IN_IS_VOICE_AUTH				IN PAYMENTS.IS_VOICE_AUTH%TYPE,
IN_CC_AUTH_PROVIDER				IN PAYMENTS.CC_AUTH_PROVIDER%TYPE,
IN_PAYMENT_EXT_INFO				IN LONG,
IN_CARDINAL_VERIFIED_FLAG       IN PAYMENTS.CARDINAL_VERIFIED_FLAG%TYPE,
IN_ROUTE						IN PAYMENTS.ROUTE%TYPE,
IN_TOKEN_ID                     IN PAYMENTS.TOKEN_ID%TYPE,
IN_AUTH_TRANSACTION_ID          IN PAYMENTS.AUTHORIZATION_TRANSACTION_ID%TYPE,
IO_PAYMENT_ID                   IN OUT PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_PAYMENTS
(
IN_ADDITIONAL_BILL_ID,
IN_PAYMENT_TYPE,
IN_CC_ID,
IN_UPDATED_BY,
IN_AUTH_RESULT,
IN_AUTH_NUMBER,
IN_AVS_CODE,
IN_ACQ_REFERENCE_NUMBER,
IN_GC_COUPON_NUMBER,
IN_AUTH_OVERRIDE_FLAG,
IN_CREDIT_AMOUNT,
IN_DEBIT_AMOUNT,
IN_PAYMENT_INDICATOR,
IN_REFUND_ID,
IN_AUTH_DATE,
IN_AAFES_TICKET_NUMBER,
IN_ORDER_GUID,
IN_MILES_POINTS_CREDIT_AMT,
IN_MILES_POINTS_DEBIT_AMT,
IN_NC_APPROVAL_ID,
IN_BILL_STATUS,
IN_BILL_DATE,
IN_WALLET_INDICATOR,
IN_IS_VOICE_AUTH,
IN_CC_AUTH_PROVIDER,
IN_PAYMENT_EXT_INFO,
IN_CARDINAL_VERIFIED_FLAG,
IN_ROUTE,
IN_TOKEN_ID,
IN_AUTH_TRANSACTION_ID,
NULL,
NULL,
NULL,
NULL,
IO_PAYMENT_ID,
OUT_STATUS,
OUT_MESSAGE
);

END UPDATE_PAYMENTS;

PROCEDURE UPDATE_PAYMENTS
(
IN_ADDITIONAL_BILL_ID           IN PAYMENTS.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS.AUTH_NUMBER%TYPE,
IN_AVS_CODE                     IN PAYMENTS.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS.AUTH_DATE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS.AAFES_TICKET_NUMBER%TYPE,
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
IN_MILES_POINTS_CREDIT_AMT      IN PAYMENTS.MILES_POINTS_CREDIT_AMT%TYPE,
IN_MILES_POINTS_DEBIT_AMT       IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
IN_NC_APPROVAL_ID               IN PAYMENTS.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_BILL_STATUS					IN PAYMENTS.BILL_STATUS%TYPE,
IN_BILL_DATE					IN PAYMENTS.BILL_DATE%TYPE,
IN_WALLET_INDICATOR             IN PAYMENTS.WALLET_INDICATOR%TYPE,
IN_IS_VOICE_AUTH				IN PAYMENTS.IS_VOICE_AUTH%TYPE,
IN_CC_AUTH_PROVIDER				IN PAYMENTS.CC_AUTH_PROVIDER%TYPE,
IN_PAYMENT_EXT_INFO				IN LONG,
IN_CARDINAL_VERIFIED_FLAG       IN PAYMENTS.CARDINAL_VERIFIED_FLAG%TYPE,
IN_ROUTE						IN PAYMENTS.ROUTE%TYPE,
IN_TOKEN_ID                     IN PAYMENTS.TOKEN_ID%TYPE,
IN_AUTH_TRANSACTION_ID          IN PAYMENTS.AUTHORIZATION_TRANSACTION_ID%TYPE,
IN_ASSOCIATED_PAYMENT_ID		IN VARCHAR2,
IN_MERCHANT_REF_ID				IN PAYMENTS.MERCHANT_REF_ID%TYPE,
IN_SETTLEMENT_TRANSACTION_ID    IN PAYMENTS.SETTLEMENT_TRANSACTION_ID%TYPE,
IN_REQUEST_TOKEN          		IN PAYMENTS.REQUEST_TOKEN %TYPE,
IO_PAYMENT_ID                   IN OUT PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting payment information
        overloading the procedure above.  This proc gets the order information
        from the given order, and breaks apart the acq_reference_number
        into the five individual fields.  The procedure is being called from the
        add billing and refunds.

Input:
        additional_bill_id              number
        payment_type                    varchar2
        cc_id                           number
        updated_by                      varchar2
        auth_result                     varchar2
        auth_number                     varchar2
        avs_code                        varchar2
        acq_reference_number            varchar2
        gc_coupon_number                varchar2
        auth_override_flag              char
        credit_amount                   number
        debit_amount                    number
        payment_indicator               char
        refund_id                       number
        auth_date                       date
        aafes_ticket_number             varchar2
        order_guid                      varchar2
        bill_status						varchar2
        bill_date						date
        payment_id                      number

Output:
        payment_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR order_guid IS
        SELECT  od.order_guid
        FROM    order_details od
        WHERE   EXISTS
        (       select  1
                from    order_bills ob
                where   ob.order_detail_id = od.order_detail_id
                and     ob.order_bill_id = in_additional_bill_id
        )
        union
        SELECT  od.order_guid
        FROM    order_details od
        where   EXISTS
        (       select  1
                from    refund r
                where   r.order_detail_id = od.order_detail_id
                and     r.refund_id = in_refund_id
        );

v_order_guid            orders.order_guid%type;
v_auth_char_indicator   payments.auth_char_indicator%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'a')+1, instr(in_acq_reference_number||'abcdef', 'b')-instr(in_acq_reference_number||'abcdef', 'a')-1);
v_transaction_id        payments.transaction_id%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'b')+1, instr(in_acq_reference_number||'abcdef', 'c')-instr(in_acq_reference_number||'abcdef', 'b')-1);
v_validation_code       payments.validation_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'c')+1, instr(in_acq_reference_number||'abcdef', 'd')-instr(in_acq_reference_number||'abcdef', 'c')-1);
v_auth_source_code      payments.auth_source_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'd')+1, instr(in_acq_reference_number||'abcdef', 'e')-instr(in_acq_reference_number||'abcdef', 'd')-1);
v_response_code         payments.response_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'e')+1, instr(in_acq_reference_number||'abcdef', 'f')-instr(in_acq_reference_number||'abcdef', 'e')-1);
initial_auth_amount  payments.initial_auth_amount%type := null;
payment_type         payments.payment_type%type := IN_PAYMENT_TYPE;
/* The 'f' portion of the data, which is a product identifier, is only used for the credit card settlement file and is parsed out when the settlement file is created.  v_product_identifier            varchar2(3) := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'f')+1); */

BEGIN

IF in_order_guid IS NULL THEN
        OPEN order_guid;
        FETCH order_guid INTO v_order_guid;
        CLOSE order_guid;
ELSE
        v_order_guid := in_order_guid;
END IF;

IF IN_IS_VOICE_AUTH='Y' OR IN_CC_AUTH_PROVIDER IS NOT NULL THEN
  initial_auth_amount := IN_CREDIT_AMOUNT;
END IF;

IF IN_IS_VOICE_AUTH='Y' AND (IN_PAYMENT_TYPE IS NULL OR IN_CREDIT_AMOUNT IS NULL) THEN
  BEGIN
    SELECT payment_type,credit_amount INTO payment_type,initial_auth_amount
    FROM payments
    where payment_id=io_payment_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      --do nothing
      payment_type := payment_type;
  END;
END IF;

UPDATE_PAYMENTS
(
        IN_ORDER_GUID=>v_order_guid,
        IN_ADDITIONAL_BILL_ID=>in_additional_bill_id,
        IN_PAYMENT_TYPE=>payment_type,
        IN_CC_ID=>in_cc_id,
        IN_UPDATED_BY=>in_updated_by,
        IN_AUTH_RESULT=>in_auth_result,
        IN_AUTH_NUMBER=>in_auth_number,
        IN_AVS_CODE=>in_avs_code,
        IN_ACQ_REFERENCE_NUMBER=>in_acq_reference_number,
        IN_GC_COUPON_NUMBER=>in_gc_coupon_number,
        IN_AUTH_OVERRIDE_FLAG=>in_auth_override_flag,
        IN_CREDIT_AMOUNT=>in_credit_amount,
        IN_DEBIT_AMOUNT=>in_debit_amount,
        IN_PAYMENT_INDICATOR=>in_payment_indicator,
        IN_REFUND_ID=>in_refund_id,
        IN_AUTH_DATE=>in_auth_date,
        IN_AUTH_CHAR_INDICATOR=>v_auth_char_indicator,
        IN_TRANSACTION_ID=>v_transaction_id,
        IN_VALIDATION_CODE=>v_validation_code,
        IN_AUTH_SOURCE_CODE=>v_auth_source_code,
        IN_RESPONSE_CODE=>v_response_code,
        IN_AAFES_TICKET_NUMBER=>in_aafes_ticket_number,
        IN_AP_ACCOUNT_ID=>NULL,
        IN_AP_AUTH_TXT=>NULL,
        IN_NC_APPROVAL_IDENTITY_ID=>in_nc_approval_id,
        IN_NC_TYPE_CODE=>NULL,
        IN_NC_REASON_CODE=>NULL,
        IN_NC_ORDER_DETAIL_ID=>NULL,
        IN_MILES_POINTS_CREDIT_AMT=>IN_MILES_POINTS_CREDIT_AMT,
        IN_MILES_POINTS_DEBIT_AMT=>IN_MILES_POINTS_DEBIT_AMT,
		IN_CSC_RESPONSE_CODE=>NULL,
		IN_CSC_VALIDATED_FLAG=>'N',
		IN_CSC_FAILURE_CNT=>0,
        IN_BILL_STATUS=>in_bill_status,
        IN_BILL_DATE=>in_bill_date,
  		IN_WALLET_INDICATOR=>IN_WALLET_INDICATOR,
  		IN_CC_AUTH_PROVIDER=>IN_CC_AUTH_PROVIDER,
  		IN_INITIAL_AUTH_AMOUNT=>initial_auth_amount,
  		IN_PAYMENT_EXT_INFO=>IN_PAYMENT_EXT_INFO,
  		IN_IS_VOICE_AUTH=>in_is_voice_auth,
  		IN_CARDINAL_VERIFIED_FLAG=>IN_CARDINAL_VERIFIED_FLAG,
  		IN_ROUTE=> IN_ROUTE,
  	IN_TOKEN_ID=>IN_TOKEN_ID,
  	IN_AUTH_TRANSACTION_ID=>IN_AUTH_TRANSACTION_ID,
  	IN_ASSOCIATED_PAYMENT_ID=>IN_ASSOCIATED_PAYMENT_ID,
	IN_MERCHANT_REF_ID=>IN_MERCHANT_REF_ID,
	IN_SETTLEMENT_TRANSACTION_ID=>IN_SETTLEMENT_TRANSACTION_ID,
    IN_REQUEST_TOKEN=>IN_REQUEST_TOKEN,
        IO_PAYMENT_ID=>io_payment_id,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PAYMENTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PAYMENTS;


PROCEDURE UPDATE_CO_BRAND
(
IN_ORDER_GUID                   IN CO_BRAND.ORDER_GUID%TYPE,
IN_INFO_NAME                    IN CO_BRAND.INFO_NAME%TYPE,
IN_INFO_DATA                    IN CO_BRAND.INFO_DATA%TYPE,
IO_CO_BRAND_ID                  IN OUT CO_BRAND.CO_BRAND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting cobrand information
        (MOC, ROC, and BLC)

Input:
        order_guid                      varchar2
        info_name                       varchar2
        info_data                       varchar2
        co_brand_id                     number

Output:
        co_brand_id                     number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF io_co_brand_id IS NOT NULL AND order_query_pkg.co_brand_exists (io_co_brand_id) = 'Y' THEN
        UPDATE  co_brand
        SET
                order_guid = NVL(in_order_guid, order_guid),
                info_name = NVL(in_info_name, info_name),
                info_data = NVL(in_info_data, info_data)
        WHERE   co_brand_id = io_co_brand_id;

ELSE
        IF io_co_brand_id IS NULL THEN
                io_co_brand_id := key.keygen ('CO_BRAND');
        END IF;

        INSERT INTO co_brand
        (
                co_brand_id,
                order_guid,
                info_name,
                info_data
        )
        VALUES
        (
                io_co_brand_id,
                in_order_guid,
                in_info_name,
                in_info_data
        );
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_CO_BRAND [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CO_BRAND;

PROCEDURE UPDATE_ORDER_CONTACT_INFO
(
IN_ORDER_GUID                   IN ORDER_CONTACT_INFO.ORDER_GUID%TYPE,
IN_FIRST_NAME                   IN ORDER_CONTACT_INFO.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN ORDER_CONTACT_INFO.LAST_NAME%TYPE,
IN_PHONE_NUMBER                 IN ORDER_CONTACT_INFO.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN ORDER_CONTACT_INFO.EXTENSION%TYPE,
IN_EMAIL_ADDRESS                IN ORDER_CONTACT_INFO.EMAIL_ADDRESS%TYPE,
IN_UPDATED_BY                   IN ORDER_CONTACT_INFO.UPDATED_BY%TYPE,
IO_ORDER_CONTACT_INFO_ID        IN OUT ORDER_CONTACT_INFO.ORDER_CONTACT_INFO_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserint order contact information

Input:
        order_guid                      varchar2
        first_name                      varchar2
        last_name                       varchar2
        phone_number                    varchar2
        extension                       varchar2
        email_address                   varchar2
        updated_by                      varchar2
        order_contact_info_id           number

Output:
        order_contact_info_id           number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF io_order_contact_info_id IS NOT NULL AND order_query_pkg.order_contact_info_exists (io_order_contact_info_id) = 'Y' THEN

        UPDATE  order_contact_info
        SET
                first_name = NVL(in_first_name, first_name),
                last_name = NVL(in_last_name, last_name),
                phone_number = NVL(in_phone_number, phone_number),
                extension = NVL(in_extension, extension),
                email_address = NVL(in_email_address, email_address),
                updated_on = sysdate,
                updated_by = in_updated_by
        WHERE   order_contact_info_id = io_order_contact_info_id;

ELSE
        IF io_order_contact_info_id IS NULL THEN
                io_order_contact_info_id := key.keygen ('ORDER_CONTACT_INFO');
        END IF;

        INSERT INTO order_contact_info
        (
                order_contact_info_id,
                order_guid,
                first_name,
                last_name,
                phone_number,
                extension,
                email_address,
                created_on,
                created_by,
                updated_on,
                updated_by
        )
        VALUES
        (
                io_order_contact_info_id,
                in_order_guid,
                in_first_name,
                in_last_name,
                in_phone_number,
                in_extension,
                in_email_address,
                sysdate,
                in_updated_by,
                sysdate,
                in_updated_by
        );

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ORDER_CONTACT_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_CONTACT_INFO;


PROCEDURE INSERT_ORDER_DETAILS
(
IN_ORDER_GUID                   IN ORDER_DETAILS.ORDER_GUID%TYPE,
IN_EXTERNAL_ORDER_NUMBER        IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
IN_HP_ORDER_NUMBER              IN ORDER_DETAILS.HP_ORDER_NUMBER%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_AVS_ADDRESS_ID               IN ORDER_DETAILS.AVS_ADDRESS_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_CREATED_BY                   IN ORDER_DETAILS.CREATED_BY%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_MEMBERSHIP_ID                IN ORDER_DETAILS.MEMBERSHIP_ID%TYPE,
IN_PERSONALIZATION_DATA		IN ORDER_DETAILS.PERSONALIZATION_DATA%TYPE,
IN_QMS_RESULT_CODE              IN ORDER_DETAILS.QMS_RESULT_CODE%TYPE,
IN_PERSONAL_GREETING_ID         IN ORDER_DETAILS.PERSONAL_GREETING_ID%TYPE,
IN_BIN_SOURCE_CHANGED_FLAG      IN ORDER_DETAILS.BIN_SOURCE_CHANGED_FLAG%TYPE,
IO_ORDER_DETAIL_ID              IN OUT ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_FREE_SHIPPING_FLAG           IN ORDER_DETAILS.FREE_SHIPPING_FLAG%TYPE,
IN_APPLY_SURCHARGE_CODE		IN ORDER_DETAILS.APPLY_SURCHARGE_CODE%TYPE,
IN_SURCHARGE_DESCRIPTION        IN ORDER_DETAILS.SURCHARGE_DESCRIPTION%TYPE,
IN_DISPLAY_SURCHARGE		IN ORDER_DETAILS.DISPLAY_SURCHARGE_FLAG%TYPE,
IN_SEND_SURCHARGE_TO_FLORIST    IN ORDER_DETAILS.SEND_SURCHARGE_TO_FLORIST_FLAG%TYPE,
IN_ORIGINAL_ORDER_HAS_SDU       IN ORDER_DETAILS.ORIGINAL_ORDER_HAS_SDU%TYPE,
IN_PC_GROUP_ID            IN ORDER_DETAILS.PC_GROUP_ID%TYPE,
IN_PC_MEMBERSHIP_ID       IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE,
IN_PC_FLAG         	  IN ORDER_DETAILS.PC_FLAG%TYPE,
IN_DERIVED_VIP_FLAG 		IN ORDER_DETAILS.DERIVED_VIP_FLAG%TYPE,
IN_TIME_OF_SERVICE    			IN ORDER_DETAILS.TIME_OF_SERVICE%TYPE,
IN_LEGACY_ID                IN ORDER_DETAILS.LEGACY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting order detail information

Input:
        order_guid                      varchar2
        external_order_number           varchar2
        hp_order_number                 varchar2
        source_code                     varchar2
        delivery_date                   date
        recipient_id                    number
        avs_address_id                  number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        created_by                      varchar2
        scrubbed_on                     date
        scrubbed_by                     varchar2
        delivery_date_range_end         date
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        membership_id                   number
        order_detail_id                 number
        qms_result_code                 varchar2
        personal_greeting_id            varchar2
		original_order_has_sdu					char
		PC_GROUP_ID           		varchar2
		PC_MEMBERSHIP_ID           			varchar2
		PC_FLAG           			varchar2
		TIME_OF_SERVICE				varchar2
Output:
        order_detail_id                 number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF io_order_detail_id IS NULL THEN
        io_order_detail_id := key.keygen ('ORDER_DETAILS');
END IF;

INSERT INTO order_details
(
        order_detail_id,
        order_guid,
        external_order_number,
        hp_order_number,
        source_code,
        delivery_date,
        recipient_id,
        avs_address_id,
        product_id,
        quantity,
        color_1,
        color_2,
        substitution_indicator,
        same_day_gift,
        occasion,
        card_message,
        card_signature,
        special_instructions,
        release_info_indicator,
        florist_id,
        ship_method,
        ship_date,
        order_disp_code,
        created_on,
        created_by,
        updated_on,
        updated_by,
        scrubbed_on,
        scrubbed_by,
        delivery_date_range_end,
        ariba_unspsc_code,
        ariba_po_number,
        ariba_ams_project_code,
        ariba_cost_center,
        size_indicator,
        miles_points,
        subcode,
        membership_id,
        vendor_id,
		personalization_data,
        qms_result_code,
        personal_greeting_id,
        bin_source_changed_flag,
        APPLY_SURCHARGE_CODE,
        SURCHARGE_DESCRIPTION,
        SEND_SURCHARGE_TO_FLORIST_FLAG,
        DISPLAY_SURCHARGE_FLAG,
        free_shipping_flag,
		original_order_has_sdu,
		PC_GROUP_ID,
		PC_MEMBERSHIP_ID,
		PC_FLAG,
		DERIVED_VIP_FLAG,
		TIME_OF_SERVICE,
    LEGACY_ID
)
VALUES
(
        io_order_detail_id,
        in_order_guid,
        in_external_order_number,
        in_hp_order_number,
        in_source_code,
        in_delivery_date,
        in_recipient_id,
        in_avs_address_id,
        in_product_id,
        in_quantity,
        in_color_1,
        in_color_2,
        nvl (in_substitution_indicator, 'N'),
        nvl (in_same_day_gift, 'N'),
        in_occasion,
        in_card_message,
        in_card_signature,
        in_special_instructions,
        nvl (in_release_info_indicator, 'N'),
        in_florist_id,
        in_ship_method,
        in_ship_date,
        in_order_disp_code,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_scrubbed_on,
        in_scrubbed_by,
        in_delivery_date_range_end,
        in_ariba_unspsc_code,
        in_ariba_po_number,
        in_ariba_ams_project_code,
        in_ariba_cost_center,
        in_size_indicator,
        in_miles_points,
        in_subcode,
        in_membership_id,
        null,
	in_personalization_data,
	in_qms_result_code,
        in_personal_greeting_id,
        nvl(in_bin_source_changed_flag, 'N'),
        in_apply_surcharge_code ,
        in_surcharge_description ,
        in_send_surcharge_to_florist  ,
        in_display_surcharge,
        in_free_shipping_flag,
		in_original_order_has_sdu,
		IN_PC_GROUP_ID,
		IN_PC_MEMBERSHIP_ID,
		IN_PC_FLAG,
		IN_DERIVED_VIP_FLAG,
		IN_TIME_OF_SERVICE,
    IN_LEGACY_ID
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_DETAILS;

PROCEDURE INSERT_ORDER_ADD_ONS
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS.ORDER_DETAIL_ID%TYPE,
IN_ADD_ON_CODE                  IN ORDER_ADD_ONS.ADD_ON_CODE%TYPE,
IN_ADD_ON_QUANTITY              IN ORDER_ADD_ONS.ADD_ON_QUANTITY%TYPE,
IN_ADD_ON_HISTORY_ID            IN ORDER_ADD_ONS.ADD_ON_HISTORY_ID%TYPE,
IN_CREATED_BY                   IN ORDER_ADD_ONS.CREATED_BY%TYPE,
IO_ORDER_ADD_ON_ID              IN OUT ORDER_ADD_ONS.ORDER_ADD_ON_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_ORDER_ADD_ONS
(
IN_ORDER_DETAIL_ID,
IN_ADD_ON_CODE,
IN_ADD_ON_QUANTITY,
IN_ADD_ON_HISTORY_ID,
IN_CREATED_BY,
IO_ORDER_ADD_ON_ID,
0,
0,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ORDER_ADD_ONS;

PROCEDURE INSERT_ORDER_ADD_ONS
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS.ORDER_DETAIL_ID%TYPE,
IN_ADD_ON_CODE                  IN ORDER_ADD_ONS.ADD_ON_CODE%TYPE,
IN_ADD_ON_QUANTITY              IN ORDER_ADD_ONS.ADD_ON_QUANTITY%TYPE,
IN_ADD_ON_HISTORY_ID            IN ORDER_ADD_ONS.ADD_ON_HISTORY_ID%TYPE,
IN_CREATED_BY                   IN ORDER_ADD_ONS.CREATED_BY%TYPE,
IO_ORDER_ADD_ON_ID              IN OUT ORDER_ADD_ONS.ORDER_ADD_ON_ID%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_ADD_ONS.ADD_ON_AMOUNT%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_ADD_ONS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating the order
        add on if it exists

Input:
        order_detail_id                 number
        add_on_code                     varchar2
        add_on_quantity                 number
        created_by                      varchar2
        order_add_on_id                 number

Output:
        order_add_on_id                 number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

cursor hist_cur is
        select  addon_history_id
        from    ftd_apps.addon_history
        where   addon_id = in_add_on_code
        and     status = 'Active';

v_add_on_history_id      integer;

-- function to check if the given add on id already exists
function add_on_exists (p_order_add_on_id integer) return varchar2
as
        cursor ao_cur is
                select  'Y'
                from    order_add_ons
                where   order_add_on_id = p_order_add_on_id;

        v_return        char(1);

begin
        open ao_cur;
        fetch ao_cur into v_return;
        close ao_cur;
        return v_return;
end;


BEGIN

IF (IN_ADD_ON_HISTORY_ID IS NULL) THEN
    OPEN hist_cur;
    FETCH hist_cur INTO v_add_on_history_id;
    CLOSE hist_cur;
ELSE
    v_add_on_history_id := IN_ADD_ON_HISTORY_ID;
END IF;

-- if the add on id is given and already exists then update the record
IF io_order_add_on_id IS NOT NULL AND add_on_exists (io_order_add_on_id) = 'Y' THEN
        UPDATE  order_add_ons
        SET     add_on_code = in_add_on_code,
                add_on_quantity = in_add_on_quantity,
                updated_on = sysdate,
                updated_by = in_created_by,
                add_on_history_id = v_add_on_history_id,
                add_on_amount = nvl (in_add_on_amount, 0),
                add_on_discount_amount = nvl (in_add_on_discount_amount, 0)
        WHERE   order_add_on_id = io_order_add_on_id;
ELSE
        -- get the next add on id sequence
        IF io_order_add_on_id IS NULL THEN
                io_order_add_on_id := key.keygen ('ADD_ONS');
        END IF;

        INSERT INTO order_add_ons
        (
                order_add_on_id,
                order_detail_id,
                add_on_code,
                add_on_quantity,
                created_on,
                created_by,
                updated_on,
                updated_by,
                add_on_history_id,
                add_on_amount,
                add_on_discount_amount
        )
        VALUES
        (
                io_order_add_on_id,
                in_order_detail_id,
                in_add_on_code,
                in_add_on_quantity,
                sysdate,
                in_created_by,
                sysdate,
                in_created_by,
                v_add_on_history_id,
                nvl(in_add_on_amount, 0),
                nvl(in_add_on_discount_amount, 0)
        );

END IF;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ORDER_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_ADD_ONS;

PROCEDURE INSERT_ORDER_BILLS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_BILLS.TAX%TYPE,
IN_ADDITIONAL_BILL_INDICATOR    IN ORDER_BILLS.ADDITIONAL_BILL_INDICATOR%TYPE,
IN_CREATED_BY                   IN ORDER_BILLS.CREATED_BY%TYPE,
IN_SAME_DAY_FEE                 IN NUMBER,
IN_BILL_STATUS                  IN ORDER_BILLS.BILL_STATUS%TYPE,
IN_SERVICE_FEE_TAX              IN ORDER_BILLS.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ORDER_BILLS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ORDER_BILLS.WHOLESALE_AMOUNT%TYPE,
IN_TRANSACTION_AMOUNT           IN ORDER_BILLS.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE                    IN ORDER_BILLS.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE       IN ORDER_BILLS.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE                IN ORDER_BILLS.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST                 IN ORDER_BILLS.PARTNER_COST%TYPE,
IN_MILES_POINTS_AMT             IN ORDER_BILLS.MILES_POINTS_AMT%TYPE,
IN_FIRST_ORDER_DOMESTIC         IN ORDER_BILLS.DTL_FIRST_ORDER_DOMESTIC%TYPE,
IN_FIRST_ORDER_INTERNATIONAL    IN ORDER_BILLS.DTL_FIRST_ORDER_INTERNATIONAL%TYPE,
IN_SHIPPING_COST                IN ORDER_BILLS.DTL_SHIPPING_COST%TYPE,
IN_VENDOR_CHARGE                IN ORDER_BILLS.DTL_VENDOR_CHARGE%TYPE,
IN_VENDOR_SAT_UPCHARGE          IN ORDER_BILLS.DTL_VENDOR_SAT_UPCHARGE%TYPE,
IN_AK_HI_SPECIAL_SVC_CHARGE     IN ORDER_BILLS.DTL_AK_HI_SPECIAL_SVC_CHARGE%TYPE,
IN_FUEL_SURCHARGE_AMT           IN ORDER_BILLS.DTL_FUEL_SURCHARGE_AMT%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME                    IN ORDER_BILLS.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT                  IN ORDER_BILLS.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION             IN ORDER_BILLS.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN ORDER_BILLS.TAX1_RATE%TYPE,
IN_TAX2_NAME                    IN ORDER_BILLS.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT                  IN ORDER_BILLS.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION             IN ORDER_BILLS.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN ORDER_BILLS.TAX2_RATE%TYPE,
IN_TAX3_NAME                    IN ORDER_BILLS.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT                  IN ORDER_BILLS.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION             IN ORDER_BILLS.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN ORDER_BILLS.TAX3_RATE%TYPE,
IN_TAX4_NAME                    IN ORDER_BILLS.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT                  IN ORDER_BILLS.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION             IN ORDER_BILLS.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN ORDER_BILLS.TAX4_RATE%TYPE,
IN_TAX5_NAME                    IN ORDER_BILLS.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT                  IN ORDER_BILLS.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION             IN ORDER_BILLS.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN ORDER_BILLS.TAX5_RATE%TYPE,
IN_SAME_DAY_UPCHARGE            IN ORDER_BILLS.SAME_DAY_UPCHARGE%TYPE,
IN_MORNING_DELIVERY_FEE			IN ORDER_BILLS.MORNING_DELIVERY_FEE%TYPE,
IN_LATE_CUTOFF_FEE				IN ORDER_BILLS.LATE_CUTOFF_FEE%TYPE,
IN_VEN_SUN_UPCHARGE				IN ORDER_BILLS.VENDOR_SUN_UPCHARGE%TYPE,
IN_VEN_MON_UPCHARGE				IN ORDER_BILLS.VENDOR_MON_UPCHARGE%TYPE,
OUT_ORDER_BILL_ID               OUT ORDER_BILLS.ORDER_BILL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_ORDER_BILLS
(
IN_ORDER_DETAIL_ID,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SERVICE_FEE,
IN_SHIPPING_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_TAX,
IN_ADDITIONAL_BILL_INDICATOR,
IN_CREATED_BY,
IN_SAME_DAY_FEE,
IN_BILL_STATUS,
IN_SERVICE_FEE_TAX,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_TRANSACTION_AMOUNT,
IN_PDB_PRICE,
IN_DISCOUNT_PRODUCT_PRICE,
IN_DISCOUNT_TYPE,
IN_PARTNER_COST,
IN_MILES_POINTS_AMT,
IN_FIRST_ORDER_DOMESTIC,
IN_FIRST_ORDER_INTERNATIONAL,
IN_SHIPPING_COST,
IN_VENDOR_CHARGE,
IN_VENDOR_SAT_UPCHARGE,
IN_AK_HI_SPECIAL_SVC_CHARGE,
IN_FUEL_SURCHARGE_AMT,
IN_SHIPPING_FEE_SAVED,
IN_SERVICE_FEE_SAVED,
IN_TAX1_NAME,
IN_TAX1_AMOUNT,
IN_TAX1_DESCRIPTION,
IN_TAX1_RATE,
IN_TAX2_NAME,
IN_TAX2_AMOUNT,
IN_TAX2_DESCRIPTION,
IN_TAX2_RATE,
IN_TAX3_NAME,
IN_TAX3_AMOUNT,
IN_TAX3_DESCRIPTION,
IN_TAX3_RATE,
IN_TAX4_NAME,
IN_TAX4_AMOUNT,
IN_TAX4_DESCRIPTION,
IN_TAX4_RATE,
IN_TAX5_NAME,
IN_TAX5_AMOUNT,
IN_TAX5_DESCRIPTION,
IN_TAX5_RATE,
IN_SAME_DAY_UPCHARGE,
IN_MORNING_DELIVERY_FEE,
0,
IN_LATE_CUTOFF_FEE,
IN_VEN_SUN_UPCHARGE,
IN_VEN_MON_UPCHARGE,
OUT_ORDER_BILL_ID,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ORDER_BILLS;

PROCEDURE INSERT_ORDER_BILLS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_BILLS.TAX%TYPE,
IN_ADDITIONAL_BILL_INDICATOR    IN ORDER_BILLS.ADDITIONAL_BILL_INDICATOR%TYPE,
IN_CREATED_BY                   IN ORDER_BILLS.CREATED_BY%TYPE,
IN_SAME_DAY_FEE                 IN NUMBER,
IN_BILL_STATUS                  IN ORDER_BILLS.BILL_STATUS%TYPE,
IN_SERVICE_FEE_TAX              IN ORDER_BILLS.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ORDER_BILLS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ORDER_BILLS.WHOLESALE_AMOUNT%TYPE,
IN_TRANSACTION_AMOUNT           IN ORDER_BILLS.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE                    IN ORDER_BILLS.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE       IN ORDER_BILLS.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE                IN ORDER_BILLS.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST                 IN ORDER_BILLS.PARTNER_COST%TYPE,
IN_MILES_POINTS_AMT             IN ORDER_BILLS.MILES_POINTS_AMT%TYPE,
IN_FIRST_ORDER_DOMESTIC         IN ORDER_BILLS.DTL_FIRST_ORDER_DOMESTIC%TYPE,
IN_FIRST_ORDER_INTERNATIONAL    IN ORDER_BILLS.DTL_FIRST_ORDER_INTERNATIONAL%TYPE,
IN_SHIPPING_COST                IN ORDER_BILLS.DTL_SHIPPING_COST%TYPE,
IN_VENDOR_CHARGE                IN ORDER_BILLS.DTL_VENDOR_CHARGE%TYPE,
IN_VENDOR_SAT_UPCHARGE          IN ORDER_BILLS.DTL_VENDOR_SAT_UPCHARGE%TYPE,
IN_AK_HI_SPECIAL_SVC_CHARGE     IN ORDER_BILLS.DTL_AK_HI_SPECIAL_SVC_CHARGE%TYPE,
IN_FUEL_SURCHARGE_AMT           IN ORDER_BILLS.DTL_FUEL_SURCHARGE_AMT%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME                    IN ORDER_BILLS.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT                  IN ORDER_BILLS.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION             IN ORDER_BILLS.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN ORDER_BILLS.TAX1_RATE%TYPE,
IN_TAX2_NAME                    IN ORDER_BILLS.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT                  IN ORDER_BILLS.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION             IN ORDER_BILLS.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN ORDER_BILLS.TAX2_RATE%TYPE,
IN_TAX3_NAME                    IN ORDER_BILLS.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT                  IN ORDER_BILLS.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION             IN ORDER_BILLS.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN ORDER_BILLS.TAX3_RATE%TYPE,
IN_TAX4_NAME                    IN ORDER_BILLS.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT                  IN ORDER_BILLS.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION             IN ORDER_BILLS.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN ORDER_BILLS.TAX4_RATE%TYPE,
IN_TAX5_NAME                    IN ORDER_BILLS.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT                  IN ORDER_BILLS.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION             IN ORDER_BILLS.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN ORDER_BILLS.TAX5_RATE%TYPE,
IN_SAME_DAY_UPCHARGE            IN ORDER_BILLS.SAME_DAY_UPCHARGE%TYPE,
IN_MORNING_DELIVERY_FEE			IN ORDER_BILLS.MORNING_DELIVERY_FEE%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_BILLS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
IN_LATE_CUTOFF_FEE				IN ORDER_BILLS.LATE_CUTOFF_FEE%TYPE,
IN_VEN_SUN_UPCHARGE				IN ORDER_BILLS.VENDOR_SUN_UPCHARGE%TYPE,
IN_VEN_MON_UPCHARGE				IN ORDER_BILLS.VENDOR_MON_UPCHARGE%TYPE,
OUT_ORDER_BILL_ID               OUT ORDER_BILLS.ORDER_BILL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting order bills.  This procedure
        oeverloads the procedure below defaulting the accounting transaction ind to
        'Y'.

Input:
        order_detail_id                 number
        product_amount                  number
        add_on_amount                   number
        service_fee                     number
        shipping_fee                    number
        discount_amount                 number
        shipping_tax                    number
        tax                             number
        additional_bill_indicator       char
        created_by                      varchar2
        same_day_fee                    number  - field not used and dropped from table
        bill_status                     varchar2
        service_fee_tax                 number
        commission_amount               number
        wholesale_amount                number
        transaction_amount              number
        pdb_price                       number
        discount_product_price          number
        discount_type                   char
        partner_cost                    number
		same_day_upcharge               number
		morning_delivery_fee			number
		late_cutoff_fee					number
		vendor_mon_upcharge				number
		vendor_sun_upcharge				number

Output:
        order_bill_id                   number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

INSERT_ORDER_BILLS
(
        IN_ORDER_DETAIL_ID,
        IN_PRODUCT_AMOUNT,
        IN_ADD_ON_AMOUNT,
        IN_SERVICE_FEE,
        IN_SHIPPING_FEE,
        IN_DISCOUNT_AMOUNT,
        IN_SHIPPING_TAX,
        IN_TAX,
        IN_ADDITIONAL_BILL_INDICATOR,
        IN_CREATED_BY,
        null,                           -- same day fee dropped from table
        IN_BILL_STATUS,
        IN_SERVICE_FEE_TAX,
        IN_COMMISSION_AMOUNT,
        IN_WHOLESALE_AMOUNT,
        IN_TRANSACTION_AMOUNT,
        IN_PDB_PRICE,
        IN_DISCOUNT_PRODUCT_PRICE,
        IN_DISCOUNT_TYPE,
        IN_PARTNER_COST,
        'Y',
        0,
        IN_MILES_POINTS_AMT,
        IN_FIRST_ORDER_DOMESTIC,
        IN_FIRST_ORDER_INTERNATIONAL,
        IN_SHIPPING_COST,
        IN_VENDOR_CHARGE,
        IN_VENDOR_SAT_UPCHARGE,
        IN_AK_HI_SPECIAL_SVC_CHARGE,
        IN_FUEL_SURCHARGE_AMT,
        IN_SHIPPING_FEE_SAVED,
        IN_SERVICE_FEE_SAVED,
        IN_TAX1_NAME,
        IN_TAX1_AMOUNT,
        IN_TAX1_DESCRIPTION,
        IN_TAX1_RATE,
        IN_TAX2_NAME,
        IN_TAX2_AMOUNT,
        IN_TAX2_DESCRIPTION,
        IN_TAX2_RATE,
        IN_TAX3_NAME,
        IN_TAX3_AMOUNT,
        IN_TAX3_DESCRIPTION,
        IN_TAX3_RATE,
        IN_TAX4_NAME,
        IN_TAX4_AMOUNT,
        IN_TAX4_DESCRIPTION,
        IN_TAX4_RATE,
        IN_TAX5_NAME,
        IN_TAX5_AMOUNT,
        IN_TAX5_DESCRIPTION,
        IN_TAX5_RATE,
		IN_SAME_DAY_UPCHARGE,
		IN_MORNING_DELIVERY_FEE,
		IN_ADD_ON_DISCOUNT_AMOUNT,
		IN_LATE_CUTOFF_FEE,
		IN_VEN_SUN_UPCHARGE,
		IN_VEN_MON_UPCHARGE,
        OUT_ORDER_BILL_ID,
        OUT_STATUS,
        OUT_MESSAGE
);

END INSERT_ORDER_BILLS;

PROCEDURE INSERT_ORDER_BILLS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_BILLS.TAX%TYPE,
IN_ADDITIONAL_BILL_INDICATOR    IN ORDER_BILLS.ADDITIONAL_BILL_INDICATOR%TYPE,
IN_CREATED_BY                   IN ORDER_BILLS.CREATED_BY%TYPE,
IN_SAME_DAY_FEE                 IN NUMBER,
IN_BILL_STATUS                  IN ORDER_BILLS.BILL_STATUS%TYPE,
IN_SERVICE_FEE_TAX              IN ORDER_BILLS.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ORDER_BILLS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ORDER_BILLS.WHOLESALE_AMOUNT%TYPE,
IN_TRANSACTION_AMOUNT           IN ORDER_BILLS.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE                    IN ORDER_BILLS.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE       IN ORDER_BILLS.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE                IN ORDER_BILLS.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST                 IN ORDER_BILLS.PARTNER_COST%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_WHOLESALE_SERVICE_FEE        IN ORDER_BILLS.WHOLESALE_SERVICE_FEE%TYPE,
IN_MILES_POINTS_AMT             IN ORDER_BILLS.MILES_POINTS_AMT%TYPE,
IN_FIRST_ORDER_DOMESTIC         IN ORDER_BILLS.DTL_FIRST_ORDER_DOMESTIC%TYPE,
IN_FIRST_ORDER_INTERNATIONAL    IN ORDER_BILLS.DTL_FIRST_ORDER_INTERNATIONAL%TYPE,
IN_SHIPPING_COST                IN ORDER_BILLS.DTL_SHIPPING_COST%TYPE,
IN_VENDOR_CHARGE                IN ORDER_BILLS.DTL_VENDOR_CHARGE%TYPE,
IN_VENDOR_SAT_UPCHARGE          IN ORDER_BILLS.DTL_VENDOR_SAT_UPCHARGE%TYPE,
IN_AK_HI_SPECIAL_SVC_CHARGE     IN ORDER_BILLS.DTL_AK_HI_SPECIAL_SVC_CHARGE%TYPE,
IN_FUEL_SURCHARGE_AMT           IN ORDER_BILLS.DTL_FUEL_SURCHARGE_AMT%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME                    IN ORDER_BILLS.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT                  IN ORDER_BILLS.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION             IN ORDER_BILLS.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN ORDER_BILLS.TAX1_RATE%TYPE,
IN_TAX2_NAME                    IN ORDER_BILLS.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT                  IN ORDER_BILLS.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION             IN ORDER_BILLS.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN ORDER_BILLS.TAX2_RATE%TYPE,
IN_TAX3_NAME                    IN ORDER_BILLS.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT                  IN ORDER_BILLS.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION             IN ORDER_BILLS.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN ORDER_BILLS.TAX3_RATE%TYPE,
IN_TAX4_NAME                    IN ORDER_BILLS.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT                  IN ORDER_BILLS.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION             IN ORDER_BILLS.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN ORDER_BILLS.TAX4_RATE%TYPE,
IN_TAX5_NAME                    IN ORDER_BILLS.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT                  IN ORDER_BILLS.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION             IN ORDER_BILLS.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN ORDER_BILLS.TAX5_RATE%TYPE,
IN_SAME_DAY_UPCHARGE            IN ORDER_BILLS.SAME_DAY_UPCHARGE%TYPE,
IN_MORNING_DELIVERY_FEE			IN ORDER_BILLS.MORNING_DELIVERY_FEE%TYPE,
IN_LATE_CUTOFF_FEE				IN ORDER_BILLS.LATE_CUTOFF_FEE%TYPE,
IN_VEN_SUN_UPCHARGE				IN ORDER_BILLS.VENDOR_SUN_UPCHARGE%TYPE,
IN_VEN_MON_UPCHARGE				IN ORDER_BILLS.VENDOR_MON_UPCHARGE%TYPE,
OUT_ORDER_BILL_ID               OUT ORDER_BILLS.ORDER_BILL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_ORDER_BILLS
(
IN_ORDER_DETAIL_ID,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SERVICE_FEE,
IN_SHIPPING_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_TAX,
IN_ADDITIONAL_BILL_INDICATOR,
IN_CREATED_BY,
IN_SAME_DAY_FEE,
IN_BILL_STATUS,
IN_SERVICE_FEE_TAX,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_TRANSACTION_AMOUNT,
IN_PDB_PRICE,
IN_DISCOUNT_PRODUCT_PRICE,
IN_DISCOUNT_TYPE,
IN_PARTNER_COST,
IN_ACCT_TRANS_IND,
IN_WHOLESALE_SERVICE_FEE,
IN_MILES_POINTS_AMT,
IN_FIRST_ORDER_DOMESTIC,
IN_FIRST_ORDER_INTERNATIONAL,
IN_SHIPPING_COST,
IN_VENDOR_CHARGE,
IN_VENDOR_SAT_UPCHARGE,
IN_AK_HI_SPECIAL_SVC_CHARGE,
IN_FUEL_SURCHARGE_AMT,
IN_SHIPPING_FEE_SAVED,
IN_SERVICE_FEE_SAVED,
IN_TAX1_NAME,
IN_TAX1_AMOUNT,
IN_TAX1_DESCRIPTION,
IN_TAX1_RATE,
IN_TAX2_NAME,
IN_TAX2_AMOUNT,
IN_TAX2_DESCRIPTION,
IN_TAX2_RATE,
IN_TAX3_NAME,
IN_TAX3_AMOUNT,
IN_TAX3_DESCRIPTION,
IN_TAX3_RATE,
IN_TAX4_NAME,
IN_TAX4_AMOUNT,
IN_TAX4_DESCRIPTION,
IN_TAX4_RATE,
IN_TAX5_NAME,
IN_TAX5_AMOUNT,
IN_TAX5_DESCRIPTION,
IN_TAX5_RATE,
IN_SAME_DAY_UPCHARGE,
IN_MORNING_DELIVERY_FEE,
0,
IN_LATE_CUTOFF_FEE,
IN_VEN_SUN_UPCHARGE,
IN_VEN_MON_UPCHARGE,
OUT_ORDER_BILL_ID,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ORDER_BILLS;

PROCEDURE INSERT_ORDER_BILLS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_BILLS.TAX%TYPE,
IN_ADDITIONAL_BILL_INDICATOR    IN ORDER_BILLS.ADDITIONAL_BILL_INDICATOR%TYPE,
IN_CREATED_BY                   IN ORDER_BILLS.CREATED_BY%TYPE,
IN_SAME_DAY_FEE                 IN NUMBER,
IN_BILL_STATUS                  IN ORDER_BILLS.BILL_STATUS%TYPE,
IN_SERVICE_FEE_TAX              IN ORDER_BILLS.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ORDER_BILLS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ORDER_BILLS.WHOLESALE_AMOUNT%TYPE,
IN_TRANSACTION_AMOUNT           IN ORDER_BILLS.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE                    IN ORDER_BILLS.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE       IN ORDER_BILLS.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE                IN ORDER_BILLS.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST                 IN ORDER_BILLS.PARTNER_COST%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_WHOLESALE_SERVICE_FEE        IN ORDER_BILLS.WHOLESALE_SERVICE_FEE%TYPE,
IN_MILES_POINTS_AMT             IN ORDER_BILLS.MILES_POINTS_AMT%TYPE,
IN_FIRST_ORDER_DOMESTIC         IN ORDER_BILLS.DTL_FIRST_ORDER_DOMESTIC%TYPE,
IN_FIRST_ORDER_INTERNATIONAL    IN ORDER_BILLS.DTL_FIRST_ORDER_INTERNATIONAL%TYPE,
IN_SHIPPING_COST                IN ORDER_BILLS.DTL_SHIPPING_COST%TYPE,
IN_VENDOR_CHARGE                IN ORDER_BILLS.DTL_VENDOR_CHARGE%TYPE,
IN_VENDOR_SAT_UPCHARGE          IN ORDER_BILLS.DTL_VENDOR_SAT_UPCHARGE%TYPE,
IN_AK_HI_SPECIAL_SVC_CHARGE     IN ORDER_BILLS.DTL_AK_HI_SPECIAL_SVC_CHARGE%TYPE,
IN_FUEL_SURCHARGE_AMT           IN ORDER_BILLS.DTL_FUEL_SURCHARGE_AMT%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME                    IN ORDER_BILLS.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT                  IN ORDER_BILLS.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION             IN ORDER_BILLS.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN ORDER_BILLS.TAX1_RATE%TYPE,
IN_TAX2_NAME                    IN ORDER_BILLS.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT                  IN ORDER_BILLS.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION             IN ORDER_BILLS.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN ORDER_BILLS.TAX2_RATE%TYPE,
IN_TAX3_NAME                    IN ORDER_BILLS.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT                  IN ORDER_BILLS.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION             IN ORDER_BILLS.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN ORDER_BILLS.TAX3_RATE%TYPE,
IN_TAX4_NAME                    IN ORDER_BILLS.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT                  IN ORDER_BILLS.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION             IN ORDER_BILLS.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN ORDER_BILLS.TAX4_RATE%TYPE,
IN_TAX5_NAME                    IN ORDER_BILLS.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT                  IN ORDER_BILLS.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION             IN ORDER_BILLS.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN ORDER_BILLS.TAX5_RATE%TYPE,
IN_SAME_DAY_UPCHARGE            IN ORDER_BILLS.SAME_DAY_UPCHARGE%TYPE,
IN_MORNING_DELIVERY_FEE			IN ORDER_BILLS.MORNING_DELIVERY_FEE%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_BILLS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
IN_LATE_CUTOFF_FEE				IN ORDER_BILLS.LATE_CUTOFF_FEE%TYPE,
IN_VEN_SUN_UPCHARGE				IN ORDER_BILLS.VENDOR_SUN_UPCHARGE%TYPE,
IN_VEN_MON_UPCHARGE				IN ORDER_BILLS.VENDOR_MON_UPCHARGE%TYPE,
OUT_ORDER_BILL_ID               OUT ORDER_BILLS.ORDER_BILL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting order bills.

Input:
        order_detail_id                 number
        product_amount                  number
        add_on_amount                   number
        service_fee                     number
        shipping_fee                    number
        discount_amount                 number
        shipping_tax                    number
        tax                             number
        additional_bill_indicator       char
        created_by                      varchar2
        same_day_fee                    number - field not used and dropped from table
        bill_status                     varchar2
        service_fee_tax                 number
        commission_amount               number
        wholesale_amount                number
        transaction_amount              number
        pdb_price                       number
        discount_product_price          number
        discount_type                   char
        partner_cost                    number
        acct_trans_ind                  varchar2 (Y/N - the accounting transaction record should be
                                                  automatically created for the bill)
        wholesale_service_fee           number
        miles_points_amt                number
		same_day_upcharge               number
		morning_delivery_fee			number
		late_cutoff_fee					number
		vendor_sun_upcharge				number
		vendor_mon_upcharge				number
Output:
        order_bill_id                   number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT  'Y'
        FROM    order_bills
        WHERE   order_detail_id = in_order_detail_id
        AND     additional_bill_indicator = 'N';

CURSOR order_detail_cur IS
        SELECT  delivery_date,
                product_id,
                source_code,
                ship_method
        FROM    order_details
        WHERE   order_detail_id = in_order_detail_id;

odrow                   order_detail_cur%rowtype;
v_transaction_type      varchar2(10);
v_check                 char (1);

BEGIN

IF in_additional_bill_indicator = 'N' or in_additional_bill_indicator is null THEN
-- check if an original order bill already exists, if so and the given bill is indicated as
-- an original order (additional bill indicator = N), then do not insert the record and return an error.
        OPEN check_cur;
        FETCH check_cur INTO v_check;
        CLOSE check_cur;
END IF;

IF v_check IS NULL THEN
        INSERT INTO order_bills
        (
                order_bill_id,
                order_detail_id,
                product_amount,
                add_on_amount,
                service_fee,
                shipping_fee,
                discount_amount,
                shipping_tax,
                tax,
                additional_bill_indicator,
                created_on,
                created_by,
                updated_on,
                updated_by,
                -- same_day_fee,
                bill_status,
                service_fee_tax,
                commission_amount,
                wholesale_amount,
                transaction_amount,
                pdb_price,
                discount_product_price,
                discount_type,
                partner_cost,
                wholesale_service_fee,
                miles_points_amt,
                dtl_first_order_domestic,
                dtl_first_order_international,
                dtl_shipping_cost,
                dtl_vendor_charge,
                dtl_vendor_sat_upcharge,
                dtl_ak_hi_special_svc_charge,
                dtl_fuel_surcharge_amt,
                shipping_fee_saved,
                service_fee_saved,
				tax1_name,
				tax1_amount,
				tax1_description,
				tax1_rate,
				tax2_name,
				tax2_amount,
				tax2_description,
				tax2_rate,
				tax3_name,
				tax3_amount,
				tax3_description,
				tax3_rate,
				tax4_name,
				tax4_amount,
				tax4_description,
				tax4_rate,
				tax5_name,
				tax5_amount,
				tax5_description,
				tax5_rate,
				same_day_upcharge,
				morning_delivery_fee,
       	 		add_on_discount_amount,
       	 		LATE_CUTOFF_FEE,
				VENDOR_SUN_UPCHARGE,
				VENDOR_MON_UPCHARGE

        )
        VALUES
        (
                order_bill_id_sq.nextval,
                in_order_detail_id,
                nvl (in_product_amount, 0),
                nvl (in_add_on_amount, 0),
                nvl (in_service_fee, 0),
                nvl (in_shipping_fee, 0),
                nvl (in_discount_amount, 0),
                nvl (in_shipping_tax, 0),
                nvl (in_tax, 0),
                nvl (in_additional_bill_indicator, 'N'),
                sysdate,
                in_created_by,
                sysdate,
                in_created_by,
                -- in_same_day_fee,
                in_bill_status,
                nvl (in_service_fee_tax, 0),
                nvl (in_commission_amount, 0),
                nvl (in_wholesale_amount, 0),
                nvl (in_transaction_amount, 0),
                nvl (in_pdb_price, 0),
                nvl (in_discount_product_price, 0),
                NVL (in_discount_type, 'N'),
                nvl (in_partner_cost, 0),
                nvl (in_wholesale_service_fee, 0),
                nvl (in_miles_points_amt, 0),
                nvl (in_first_order_domestic, 0),
                nvl (in_first_order_international, 0),
                nvl (in_shipping_cost, 0),
                nvl (in_vendor_charge, 0),
                nvl (in_vendor_sat_upcharge, 0),
                nvl (in_ak_hi_special_svc_charge, 0),
                nvl (in_fuel_surcharge_amt, 0),
                nvl (in_shipping_fee_saved, 0),
                nvl (in_service_fee_saved, 0),
				in_tax1_name,
				in_tax1_amount,
				in_tax1_description,
				in_tax1_rate,
				in_tax2_name,
				in_tax2_amount,
				in_tax2_description,
				in_tax2_rate,
				in_tax3_name,
				in_tax3_amount,
				in_tax3_description,
				in_tax3_rate,
				in_tax4_name,
				in_tax4_amount,
				in_tax4_description,
				in_tax4_rate,
				in_tax5_name,
				in_tax5_amount,
				in_tax5_description,
				in_tax5_rate,
				in_same_day_upcharge,
				in_morning_delivery_fee,
        		nvl (in_add_on_discount_amount, 0),
        		IN_LATE_CUTOFF_FEE,
				IN_VEN_SUN_UPCHARGE,
				IN_VEN_MON_UPCHARGE

        ) RETURNING order_bill_id INTO out_order_bill_id;

        IF in_acct_trans_ind = 'Y' THEN
                OPEN order_detail_cur;
                FETCH order_detail_cur INTO odrow;
                CLOSE order_detail_cur;

                IF in_additional_bill_indicator IS NULL OR
                   in_additional_bill_indicator = 'N' THEN
                   v_transaction_type := 'Order';
                ELSE
                   v_transaction_type := 'Add Bill';
                END IF;

                ORDER_MAINT_PKG.INSERT_ACCOUNTING_TRANSACTIONS
                (
                        IN_TRANSACTION_TYPE=>v_transaction_type,
                        IN_TRANSACTION_DATE=>sysdate,
                        IN_ORDER_DETAIL_ID=>in_order_detail_id,
                        IN_DELIVERY_DATE=>odrow.delivery_date,
                        IN_PRODUCT_ID=>odrow.product_id,
                        IN_SOURCE_CODE=>odrow.source_code,
                        IN_SHIP_METHOD=>odrow.ship_method,
                        IN_PRODUCT_AMOUNT=>in_product_amount,
                        IN_ADD_ON_AMOUNT=>in_add_on_amount,
                        IN_SHIPPING_FEE=>in_shipping_fee,
                        IN_SERVICE_FEE=>in_service_fee,
                        IN_DISCOUNT_AMOUNT=>in_discount_amount,
                        IN_SHIPPING_TAX=>in_shipping_tax,
                        IN_SERVICE_FEE_TAX=>in_service_fee_tax,
                        IN_TAX=>in_tax,
                        IN_PAYMENT_TYPE=>null,
                        IN_REFUND_DISP_CODE=>null,
                        IN_COMMISSION_AMOUNT=>in_commission_amount,
                        IN_WHOLESALE_AMOUNT=>in_wholesale_amount,
                        IN_ADMIN_FEE=>null,
                        IN_NEW_ORDER_SEQ=>'N',
                        IN_REFUND_ID=>null,
                        IN_ORDER_BILL_ID=>out_order_bill_id,
                        IN_WHOLESALE_SERVICE_FEE=>in_wholesale_service_fee,
                        IN_ADD_ON_DISCOUNT_AMOUNT => in_add_on_discount_amount,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        ELSE
                OUT_STATUS := 'Y';
        END IF;
ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'An original bill already exists for the order';
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ORDER_BILLS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END INSERT_ORDER_BILLS;


PROCEDURE UPDATE_ORDER_OP_STATUS
(
 IN_SHIP_METHOD IN ORDER_DETAILS.SHIP_METHOD%TYPE,
 IN_DATE        IN ORDER_DETAILS.SHIP_DATE%TYPE,
 IN_FROM_STATUS IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
 IN_TO_STATUS   IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates order_details.order_disp_code to the status
         to the status passed in for the ship method and date
         passed in.

Input:
        ship method     VARCHAR2  (florist or vendor)
        date            DATE
        from_status     VARCHAR2
        in_status       VARCHAR2

Output:
        status          varchar2 (Y or N)
        message         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE order_details od
     SET od.order_disp_code = in_to_status,
         od.updated_on = SYSDATE,
         od.updated_by = 'SYS'
   WHERE od.order_disp_code = in_from_status
     AND TRUNC(od.delivery_date) <= TRUNC(in_date)
     AND (
          (
           TRIM(UPPER(in_ship_method)) = 'FLORIST'
           AND decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') = 'N'
          )
          OR
          (
           TRIM(UPPER(in_ship_method)) = 'VENDOR'
           AND decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') = 'Y'
          )
         );

  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No order_details records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;


END UPDATE_ORDER_OP_STATUS;


PROCEDURE INSERT_ORDER_TRACKING
(
 in_order_detail_id        IN ORDER_TRACKING.ORDER_DETAIL_ID%TYPE,
 in_tracking_number        IN ORDER_TRACKING.TRACKING_NUMBER%TYPE,
 in_tracking_description   IN ORDER_TRACKING.TRACKING_DESCRIPTION%TYPE,
 OUT_STATUS               OUT VARCHAR2,
 OUT_ERROR_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table order_tracking.

Input:
        order_detail_id     NUMBER
        tracking_number     VARCHAR2
        description         VARCHAR2

Output:
        status              VARCHAR2
        error message       VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO ORDER_TRACKING
     (order_detail_id,
      tracking_number,
      tracking_description)
    VALUES
      (in_order_detail_id,
       in_tracking_number,
       in_tracking_description);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_TRACKING;


PROCEDURE INSERT_ORDER_TRACKING
(
 in_order_detail_id        IN ORDER_TRACKING.ORDER_DETAIL_ID%TYPE,
 in_tracking_number        IN ORDER_TRACKING.TRACKING_NUMBER%TYPE,
 in_tracking_description   IN ORDER_TRACKING.TRACKING_DESCRIPTION%TYPE,
 in_carrier_name           IN VENUS.CARRIERS.CARRIER_NAME%TYPE,
 OUT_STATUS               OUT VARCHAR2,
 OUT_ERROR_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table order_tracking.

Input:
        order_detail_id     NUMBER
        tracking_number     VARCHAR2
        description         VARCHAR2
        carrier_name        VARCHAR2

Output:
        status              VARCHAR2
        error message       VARCHAR2

-----------------------------------------------------------------------------*/
v_carrier_name      venus.carriers.carrier_name%TYPE;
V_carrier_id        venus.carriers.carrier_id%TYPE;


BEGIN

SELECT DECODE(in_carrier_name,NULL,'FedEx',in_carrier_name) INTO v_carrier_name
FROM dual;

SELECT c.carrier_id INTO v_carrier_id
FROM venus.carriers c
WHERE UPPER(c.carrier_name) = UPPER(v_carrier_name);


  INSERT INTO ORDER_TRACKING
     (order_detail_id,
      tracking_number,
      tracking_description,
      carrier_id)
    VALUES
      (in_order_detail_id,
       in_tracking_number,
       in_tracking_description,
       v_carrier_id);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_TRACKING;


PROCEDURE UPDATE_ORDER_DETAILS_DISP_CODE
(
 IN_ORDER_DETAIL_ID  IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 IN_ORDER_DISP_CODE  IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates order_details.order_disp_code to the status
         to the order_disp_code passed in for the order_detail_id passed in.

Input:
        order_detail_id   VARCHAR2
        order_disp_code   VARCHAR2

Output:
        status          varchar2 (Y or N)
        message         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

   UPDATE order_details od
      SET od.order_disp_code = in_order_disp_code,
          od.updated_on = SYSDATE,
          od.updated_by = 'SYS'
    WHERE order_detail_id = in_order_detail_id;

  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No order_details records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;


END UPDATE_ORDER_DETAILS_DISP_CODE;


PROCEDURE SET_EOD_DELIVERY_FLAG
(
 IN_DATE             IN DATE,
 IN_CC_AUTH_PROVIDER  IN CLEAN.PAYMENTS.CC_AUTH_PROVIDER%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure mark order_details EOD delivery date and
        EOD delivered flag

Input:
        delivery_date     DATE
Output:
        status            varchar2 (Y or N)
        message           varchar2 (error message)

-----------------------------------------------------------------------------*/
v_sql VARCHAR2(1000);

CURSOR c1 IS
   select distinct order_detail_id, delivery_date
   from   clean.order_details od, clean. payments p
   WHERE (od.order_disp_code = 'Shipped'
         OR
         (od.order_disp_code = 'Processed' AND (od.ship_method = 'SD' OR od.ship_method is null))
         )
     AND od.eod_delivery_indicator = 'N' AND od.order_guid = p.order_guid
     AND ((in_cc_auth_provider is null and p.cc_auth_provider is null) or (in_cc_auth_provider is not null and p.cc_auth_provider = in_cc_auth_provider));

r1 c1%ROWTYPE;
v_count number;
v_order_detail_id order_details.order_detail_id%type;
v_error exception;

BEGIN

  FOR r1 in c1 LOOP

    -- If an 'A'-type refund exists for this order, mark as 'X' (not delivered).
    -- If past delivery date and no 'A'-type refund issued, mark as 'Y' (delivered).
    begin
       select count(*) into v_count from refund r where r.order_detail_id = r1.order_detail_id and r.refund_disp_code like 'A%';

       if v_count > 0 then
	      update order_details
	      SET eod_delivery_indicator = 'X',
		   updated_on = SYSDATE,
		   updated_by = USER
	      where order_detail_id = r1.order_detail_id;
       elsif r1.delivery_date < trunc(in_date+1) then
	      update clean.order_details
	      SET eod_delivery_indicator = 'Y',
		  eod_delivery_date = delivery_date,
		  updated_on = SYSDATE,
		  updated_by = USER
	      where order_detail_id = r1.order_detail_id;
       end if;

    exception when others then
    	v_order_detail_id := r1.order_detail_id;
    	raise v_error;
    end;

  end loop;

  out_status := 'Y';

  EXCEPTION WHEN v_error THEN
     begin
  	out_status := 'N';
        out_message := 'ERROR OCCURRED WHEN UPDATING ORDER DETAIL ID ' || v_order_detail_id || ' [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     end;
  WHEN OTHERS THEN
     begin
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     end;

END SET_EOD_DELIVERY_FLAG;


PROCEDURE INSERT_ORDER_HOLD
(
 IN_ORDER_DETAIL_ID        IN ORDER_HOLD.ORDER_DETAIL_ID%TYPE,
 IN_REASON                 IN ORDER_HOLD.REASON%TYPE,
 IN_REASON_TEXT            IN ORDER_HOLD.REASON_TEXT%TYPE,
 IN_TIMEZONE               IN ORDER_HOLD.TIMEZONE%TYPE,
 IN_AUTH_COUNT             IN ORDER_HOLD.AUTH_COUNT%TYPE,
 IN_RELEASE_DATE           IN ORDER_HOLD.RELEASE_DATE%TYPE,
 IN_STATUS                 IN ORDER_HOLD.STATUS%TYPE,
 OUT_STATUS               OUT VARCHAR2,
 OUT_ERROR_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table order_hold.

Input:
        order_detail_id    NUMBER
        reason             VARCHAR2
        reason_text        VARCHAR2
        hold_post_holiday  CHAR  (not used)
        timezone           NUMBER
        auth_count         NUMBER
        release_date       DATE
        status             CHAR

Output:
        status              VARCHAR2
        error message       VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO order_hold
     (order_detail_id,
      reason,
      reason_text,
      timezone,
      auth_count,
      release_date,
      status,
      created_on,
      created_by,
      updated_on,
      updated_by)
    VALUES
     (in_order_detail_id,
      in_reason,
      in_reason_text,
      in_timezone,
      in_auth_count,
      in_release_date,
      in_status,
      SYSDATE,
      USER,
      SYSDATE,
      USER);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_HOLD;


PROCEDURE UPDATE_ORDER_HOLD
(
 IN_ORDER_DETAIL_ID        IN ORDER_HOLD.ORDER_DETAIL_ID%TYPE,
 IN_REASON                 IN ORDER_HOLD.REASON%TYPE,
 IN_REASON_TEXT            IN ORDER_HOLD.REASON_TEXT%TYPE,
 IN_TIMEZONE               IN ORDER_HOLD.TIMEZONE%TYPE,
 IN_AUTH_COUNT             IN ORDER_HOLD.AUTH_COUNT%TYPE,
 IN_RELEASE_DATE           IN ORDER_HOLD.RELEASE_DATE%TYPE,
 IN_STATUS                 IN ORDER_HOLD.STATUS%TYPE,
 OUT_STATUS               OUT VARCHAR2,
 OUT_ERROR_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record in table order_hold.

Input:
        order_detail_id    NUMBER
        reason             VARCHAR2
        reason_text        VARCHAR2
        hold_post_holiday  CHAR (not used)
        timezone           NUMBER
        auth_count         NUMBER
        release_date       DATE
        status             CHAR

Output:
        status              VARCHAR2
        error message       VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN


  UPDATE order_hold
     SET reason = in_reason,
         reason_text = in_reason_text,
         timezone = in_timezone,
         auth_count = in_auth_count,
         release_date = in_release_date,
         status       = in_status,
         updated_on   = SYSDATE,
         updated_by   = USER
   WHERE order_detail_id = in_order_detail_id;

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    out_error_message := 'WARNING: No order_hold records found for order_detail_id ' || in_order_detail_id ||'.';
  END IF;


  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_HOLD;


PROCEDURE DELETE_ORDER_HOLD
(
 IN_ORDER_DETAIL_ID        IN ORDER_HOLD.ORDER_DETAIL_ID%TYPE,
 OUT_STATUS               OUT VARCHAR2,
 OUT_ERROR_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table order_hold.

Input:
        order_detail_id    NUMBER

Output:
        status              VARCHAR2
        error message       VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN


  DELETE FROM order_hold
   WHERE order_detail_id = in_order_detail_id;

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    -- Don't change message below since java code (order_processing/OrderHoldService) is checking for it
    out_error_message := 'WARNING: No order_hold records found for order_detail_id ' || in_order_detail_id ||'.';
  END IF;


  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ORDER_HOLD;


PROCEDURE DELETE_ORDER_HOLD
(
 IN_ORDER_DETAIL_ID    IN ORDER_HOLD.ORDER_DETAIL_ID%TYPE,
 IN_REASON             IN ORDER_HOLD.REASON%TYPE,
 OUT_STATUS           OUT VARCHAR2,
 OUT_ERROR_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table order_hold for the
        order_detail_id and reason code passed in.

Input:
        order_detail_id    NUMBER
        reason             VARCHAR2

Output:
        status              VARCHAR2
        error message       VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN


  DELETE FROM order_hold
   WHERE order_detail_id = in_order_detail_id
     AND reason = in_reason;

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    -- Don't change message below since java code (order_processing/OrderHoldService) is checking for it
    out_error_message := 'WARNING: No order_hold records found for order_detail_id ' || in_order_detail_id ||'.';
  END IF;


  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ORDER_HOLD;


PROCEDURE NULL_ORDER_DETAILS_FLORIST_ID
(
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure sets the order detail florist_id to NULL becuase,
        when rerouting an order to another florist the florist_id field
        needs to be null

Input:
        order_detail_id    number
Output:
        status             varchar2 (Y or N)
        message            varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE order_details
     SET florist_id = NULL
   WHERE order_detail_id = in_order_detail_id;

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    out_message := 'WARNING: No order_details record found for order_detail_id ' || in_order_detail_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED IN NULL_ORDER_DETAILS_FLORIST_ID [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END NULL_ORDER_DETAILS_FLORIST_ID;

PROCEDURE INSERT_ACCOUNTING_TRANSACTIONS
(
IN_TRANSACTION_TYPE             IN ACCOUNTING_TRANSACTIONS.TRANSACTION_TYPE%TYPE,
IN_TRANSACTION_DATE             IN ACCOUNTING_TRANSACTIONS.TRANSACTION_DATE%TYPE,
IN_ORDER_DETAIL_ID              IN ACCOUNTING_TRANSACTIONS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ACCOUNTING_TRANSACTIONS.DELIVERY_DATE%TYPE,
IN_PRODUCT_ID                   IN ACCOUNTING_TRANSACTIONS.PRODUCT_ID%TYPE,
IN_SOURCE_CODE                  IN ACCOUNTING_TRANSACTIONS.SOURCE_CODE%TYPE,
IN_SHIP_METHOD                  IN ACCOUNTING_TRANSACTIONS.SHIP_METHOD%TYPE,
IN_PRODUCT_AMOUNT               IN ACCOUNTING_TRANSACTIONS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ACCOUNTING_TRANSACTIONS.ADD_ON_AMOUNT%TYPE,
IN_SHIPPING_FEE                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_FEE%TYPE,
IN_SERVICE_FEE                  IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ACCOUNTING_TRANSACTIONS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_TAX%TYPE,
IN_SERVICE_FEE_TAX              IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE_TAX%TYPE,
IN_TAX                          IN ACCOUNTING_TRANSACTIONS.TAX%TYPE,
IN_PAYMENT_TYPE                 IN ACCOUNTING_TRANSACTIONS.PAYMENT_TYPE%TYPE,
IN_REFUND_DISP_CODE             IN ACCOUNTING_TRANSACTIONS.REFUND_DISP_CODE%TYPE,
IN_COMMISSION_AMOUNT            IN ACCOUNTING_TRANSACTIONS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ACCOUNTING_TRANSACTIONS.WHOLESALE_AMOUNT%TYPE,
IN_ADMIN_FEE                    IN ACCOUNTING_TRANSACTIONS.ADMIN_FEE%TYPE,
IN_REFUND_ID                    IN ACCOUNTING_TRANSACTIONS.REFUND_ID%TYPE,
IN_ORDER_BILL_ID                IN ACCOUNTING_TRANSACTIONS.ORDER_BILL_ID%TYPE,
IN_NEW_ORDER_SEQ                IN VARCHAR2,
IN_WHOLESALE_SERVICE_FEE        IN ACCOUNTING_TRANSACTIONS.WHOLESALE_SERVICE_FEE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_ACCOUNTING_TRANSACTIONS
(
IN_TRANSACTION_TYPE,
IN_TRANSACTION_DATE,
IN_ORDER_DETAIL_ID,
IN_DELIVERY_DATE,
IN_PRODUCT_ID,
IN_SOURCE_CODE,
IN_SHIP_METHOD,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SHIPPING_FEE,
IN_SERVICE_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_SERVICE_FEE_TAX,
IN_TAX,
IN_PAYMENT_TYPE,
IN_REFUND_DISP_CODE,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_ADMIN_FEE,
IN_REFUND_ID,
IN_ORDER_BILL_ID,
IN_NEW_ORDER_SEQ,
IN_WHOLESALE_SERVICE_FEE,
0,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ACCOUNTING_TRANSACTIONS;

PROCEDURE INSERT_ACCOUNTING_TRANSACTIONS
(
IN_TRANSACTION_TYPE             IN ACCOUNTING_TRANSACTIONS.TRANSACTION_TYPE%TYPE,
IN_TRANSACTION_DATE             IN ACCOUNTING_TRANSACTIONS.TRANSACTION_DATE%TYPE,
IN_ORDER_DETAIL_ID              IN ACCOUNTING_TRANSACTIONS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ACCOUNTING_TRANSACTIONS.DELIVERY_DATE%TYPE,
IN_PRODUCT_ID                   IN ACCOUNTING_TRANSACTIONS.PRODUCT_ID%TYPE,
IN_SOURCE_CODE                  IN ACCOUNTING_TRANSACTIONS.SOURCE_CODE%TYPE,
IN_SHIP_METHOD                  IN ACCOUNTING_TRANSACTIONS.SHIP_METHOD%TYPE,
IN_PRODUCT_AMOUNT               IN ACCOUNTING_TRANSACTIONS.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ACCOUNTING_TRANSACTIONS.ADD_ON_AMOUNT%TYPE,
IN_SHIPPING_FEE                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_FEE%TYPE,
IN_SERVICE_FEE                  IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ACCOUNTING_TRANSACTIONS.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ACCOUNTING_TRANSACTIONS.SHIPPING_TAX%TYPE,
IN_SERVICE_FEE_TAX              IN ACCOUNTING_TRANSACTIONS.SERVICE_FEE_TAX%TYPE,
IN_TAX                          IN ACCOUNTING_TRANSACTIONS.TAX%TYPE,
IN_PAYMENT_TYPE                 IN ACCOUNTING_TRANSACTIONS.PAYMENT_TYPE%TYPE,
IN_REFUND_DISP_CODE             IN ACCOUNTING_TRANSACTIONS.REFUND_DISP_CODE%TYPE,
IN_COMMISSION_AMOUNT            IN ACCOUNTING_TRANSACTIONS.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ACCOUNTING_TRANSACTIONS.WHOLESALE_AMOUNT%TYPE,
IN_ADMIN_FEE                    IN ACCOUNTING_TRANSACTIONS.ADMIN_FEE%TYPE,
IN_REFUND_ID                    IN ACCOUNTING_TRANSACTIONS.REFUND_ID%TYPE,
IN_ORDER_BILL_ID                IN ACCOUNTING_TRANSACTIONS.ORDER_BILL_ID%TYPE,
IN_NEW_ORDER_SEQ                IN VARCHAR2,
IN_WHOLESALE_SERVICE_FEE        IN ACCOUNTING_TRANSACTIONS.WHOLESALE_SERVICE_FEE%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ACCOUNTING_TRANSACTIONS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting records into the
        accouting transaction table

Input:
        accounting_transaction_id       number
        transaction_type                varchar2
        transaction_date                date
        order_detail_id                 number
        delivery_date                   date
        product_id                      varchar2
        source_code                     varchar2
        ship_method                     varchar2
        product_amount                  number  (amount values should be passed in as negative numbers
        add_on_amount                   number   for refund transactions)
        shipping_fee                    number
        service_fee                     number
        discount_amount                 number
        shipping_tax                    number
        service_fee_tax                 number
        tax                             number
        payment_type                    varchar2
        refund_disp_code                varchar2
        commission_amount               number
        wholesale_amount                number
        admin_fee                       number
        refund_id                       number
        order_bill_id                   number
        new_order_seq                   varchar2 (Y/N - if a new order sequence should be created for
                                                  the given order detail)
        wholesale_service_fee           number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR seq_cur IS
        SELECT  max (additional_order_seq)
        FROM    accounting_transactions
        WHERE   order_detail_id = in_order_detail_id;

v_additional_order_seq          accounting_transactions.additional_order_seq%type;
v_refund_type   accounting_transactions.transaction_type%type := 'Refund';
BEGIN

-- get the current additional order seq value
OPEN seq_cur;
FETCH seq_cur INTO v_additional_order_seq;
CLOSE seq_cur;

IF v_additional_order_seq IS NULL THEN
        -- if no accouting transactions records exist for the order
        -- set the order seq to 1
        v_additional_order_seq := 1;

ELSIF in_new_order_seq = 'Y' THEN
        -- if a new order seq is requested, increment the existing order seq
        v_additional_order_seq := v_additional_order_seq + 1;
END IF;

-- store refund transactions with negative values
INSERT INTO accounting_transactions
(
        accounting_transaction_id,
        transaction_type,
        transaction_date,
        order_detail_id,
        additional_order_seq,
        delivery_date,
        product_id,
        source_code,
        ship_method,
        product_amount,
        add_on_amount,
        shipping_fee,
        service_fee,
        discount_amount,
        shipping_tax,
        service_fee_tax,
        tax,
        payment_type,
        refund_disp_code,
        commission_amount,
        wholesale_amount,
        admin_fee,
        refund_id,
        order_bill_id,
        wholesale_service_fee,
        add_on_discount_amount
)
VALUES
(
        accounting_transaction_id_sq.nextval,
        in_transaction_type,
        in_transaction_date,
        in_order_detail_id,
        v_additional_order_seq,
        in_delivery_date,
        in_product_id,
        in_source_code,
        in_ship_method,
        nvl (decode (in_transaction_type, v_refund_type, in_product_amount * -1, in_product_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_add_on_amount * -1, in_add_on_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_shipping_fee * -1, in_shipping_fee), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_service_fee * -1, in_service_fee), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_discount_amount * -1, in_discount_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_shipping_tax * -1, in_shipping_tax), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_service_fee_tax * -1, in_service_fee_tax), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_tax * -1, in_tax), 0),
        in_payment_type,
        in_refund_disp_code,
        nvl (decode (in_transaction_type, v_refund_type, in_commission_amount * -1, in_commission_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_wholesale_amount * -1, in_wholesale_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_admin_fee * -1, in_admin_fee), 0),
        in_refund_id,
        in_order_bill_id,
        nvl (decode (in_transaction_type, v_refund_type, in_wholesale_service_fee + -1, in_wholesale_service_fee), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_add_on_discount_amount * -1, in_add_on_discount_amount), 0)
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ACCOUNTING_TRANSACTIONS;

PROCEDURE INSERT_ORDER_EXTENSIONS
(
IN_ORDER_GUID                   IN ORDER_EXTENSIONS.ORDER_GUID%TYPE,
IN_CREATED_ON                   IN ORDER_EXTENSIONS.CREATED_ON%TYPE,
IN_CREATED_BY                   IN ORDER_EXTENSIONS.CREATED_BY%TYPE,
IN_UPDATED_ON                   IN ORDER_EXTENSIONS.UPDATED_ON%TYPE,
IN_UPDATED_BY                   IN ORDER_EXTENSIONS.UPDATED_BY%TYPE,
IN_INFO_NAME                    IN ORDER_EXTENSIONS.INFO_NAME%TYPE,
IN_INFO_VALUE                   IN ORDER_EXTENSIONS.INFO_VALUE%TYPE,
IO_EXTENSION_ID                 IN OUT ORDER_EXTENSIONS.EXTENSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a order_extension in the clean schema

Input:
        order_guid                      varchar2
        created_on                      date
        created_by                      varchar2
        updated_on                      date
        updated_by                      varchar2
        info_name                       varchar2
        info_value                      varchar2
        extension_id                    number

Output:
        extension_id                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

IF io_extension_id IS NULL THEN
        io_extension_id := key.keygen ('ORDER_EXTENSIONS');
END IF;

INSERT INTO order_extensions
(
        extension_id,
        order_guid,
        created_on,
        created_by,
        updated_on,
        updated_by,
        info_name,
        info_value
)
VALUES
(
        io_extension_id,
        in_order_guid,
        in_created_on,
        in_created_by,
        in_updated_on,
        in_updated_by,
        in_info_name,
        in_info_value
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_EXTENSIONS;

PROCEDURE UPDATE_ORDER_EXTENSIONS
(
IN_ORDER_GUID                   IN ORDER_EXTENSIONS.ORDER_GUID%TYPE,
IN_INFO_NAME                    IN ORDER_EXTENSIONS.INFO_NAME%TYPE,
IN_INFO_VALUE                   IN ORDER_EXTENSIONS.INFO_VALUE%TYPE,
IO_EXTENSION_ID                 IN OUT ORDER_DETAIL_EXTENSIONS.EXTENSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order_detail_extension
        record. If the info_name record does not exist for the order_detail_id,
        a record will be inserted.

Input:
        order_guid                      varchar2
        created_by                      varchar2
        updated_by                      varchar2
        info_name                       varchar2
        info_value                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	UPDATE  order_extensions
	SET	info_value = in_info_value
	WHERE	order_guid = in_order_guid
	AND	info_name = in_info_name;


	IF sql%notfound THEN
		-- No info_name record for this order detail id. Insert it.

		clean.order_maint_pkg.INSERT_ORDER_EXTENSIONS
		(
			IN_ORDER_GUID     		=> IN_ORDER_GUID,
			IN_CREATED_ON           	=> SYSDATE,
			IN_CREATED_BY           	=> 'SYS',
			IN_UPDATED_ON           	=> SYSDATE,
			IN_UPDATED_BY           	=> 'SYS',
			IN_INFO_NAME            	=> IN_INFO_NAME,
			IN_INFO_VALUE            	=> IN_INFO_VALUE,
			IO_EXTENSION_ID         	=> IO_EXTENSION_ID,
			OUT_STATUS              	=> OUT_STATUS,
			OUT_MESSAGE             	=> OUT_MESSAGE
		);
	END IF;

	out_status := 'Y';

	EXCEPTION
	    WHEN OTHERS THEN
	       out_status := 'N';
	       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_EXTENSIONS;

PROCEDURE INSERT_ORDER_DETAIL_EXTENSIONS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_EXTENSIONS.ORDER_DETAIL_ID%TYPE,
IN_CREATED_ON                   IN ORDER_DETAIL_EXTENSIONS.CREATED_ON%TYPE,
IN_CREATED_BY                   IN ORDER_DETAIL_EXTENSIONS.CREATED_BY%TYPE,
IN_UPDATED_ON                   IN ORDER_DETAIL_EXTENSIONS.UPDATED_ON%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_EXTENSIONS.UPDATED_BY%TYPE,
IN_INFO_NAME                    IN ORDER_DETAIL_EXTENSIONS.INFO_NAME%TYPE,
IN_INFO_DATA                    IN ORDER_DETAIL_EXTENSIONS.INFO_DATA%TYPE,
IO_EXTENSION_ID                 IN OUT ORDER_DETAIL_EXTENSIONS.EXTENSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for

Input:
        order_detail_id                 number
        created_on                      date
        created_by                      varchar2
        updated_on                      date
        updated_by                      varchar2
        info_name                       varchar2
        info_data                       varchar2
        extension_id                    number

Output:
        extension_id                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF io_extension_id IS NULL THEN
        io_extension_id := key.keygen ('ORDER_DETAIL_EXTENSIONS');
END IF;

INSERT INTO order_detail_extensions
(
        extension_id,
        order_detail_id,
        created_on,
        created_by,
        updated_on,
        updated_by,
        info_name,
        info_data
)
VALUES
(
        io_extension_id,
        in_order_detail_id,
        in_created_on,
        in_created_by,
        in_updated_on,
        in_updated_by,
        in_info_name,
        in_info_data
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_DETAIL_EXTENSIONS;

PROCEDURE UPDATE_ORDER_DETAIL_EXTENSIONS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_EXTENSIONS.ORDER_DETAIL_ID%TYPE,
IN_INFO_NAME                    IN ORDER_DETAIL_EXTENSIONS.INFO_NAME%TYPE,
IN_INFO_DATA                    IN ORDER_DETAIL_EXTENSIONS.INFO_DATA%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order_detail_extension
        record. If the info_name record does not exist for the order_detail_id,
        a record will be inserted.

Input:
        order_detail_id                 number
        created_by                      varchar2
        updated_by                      varchar2
        info_name                       varchar2
        info_data                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
v_extension_id ORDER_DETAIL_EXTENSIONS.EXTENSION_ID%TYPE := null;

BEGIN

	UPDATE  order_detail_extensions
	SET	info_data = in_info_data
	WHERE	order_detail_id = in_order_detail_id
	AND	info_name = in_info_name;


	IF sql%notfound THEN
		-- No info_name record for this order detail id. Insert it.

		clean.order_maint_pkg.INSERT_ORDER_DETAIL_EXTENSIONS
		(
			IN_ORDER_DETAIL_ID		=> IN_ORDER_DETAIL_ID,
			IN_CREATED_ON           	=> SYSDATE,
			IN_CREATED_BY           	=> 'SYS',
			IN_UPDATED_ON           	=> SYSDATE,
			IN_UPDATED_BY           	=> 'SYS',
			IN_INFO_NAME            	=> IN_INFO_NAME,
			IN_INFO_DATA            	=> IN_INFO_DATA,
			IO_EXTENSION_ID         	=> v_extension_id,
			OUT_STATUS              	=> OUT_STATUS,
			OUT_MESSAGE             	=> OUT_MESSAGE
		);
	END IF;

	out_status := 'Y';

	EXCEPTION
	    WHEN OTHERS THEN
	       out_status := 'N';
	       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAIL_EXTENSIONS;


PROCEDURE DELETE_HELD_ORDERS
(
 IN_ORDER_DETAIL_ID  IN NUMBER,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This deletes all the order_hold records that have a reason of
        'CUSTOMER', 'FRAUD_FLAG', or 'LP_INDICATOR' for the passed in order detail id.

Input:
        order_detail_id - NUMBER

Output:
        cursor containing the order_hold

-----------------------------------------------------------------------------*/


BEGIN

  DELETE FROM order_hold
     WHERE order_detail_id = in_order_detail_id
       AND reason IN ('CUSTOMER', 'FRAUD_FLAG', 'LP_INDICATOR');

  -- if the hold record was deleted, set the fraud indicator on 'N'
  IF sql%rowcount > 0 THEN
        UPDATE  orders o
        SET     o.fraud_indicator = 'N'
        WHERE   exists
        (
                select  1
                from    order_details od
                where   od.order_guid = o.order_guid
                and     od.order_detail_id = in_order_detail_id
        );

  END IF;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DELETE_HELD_ORDERS;



PROCEDURE DELETE_ORDER_FLORIST_USED
(
 IN_ORDER_DETAIL_ID  IN NUMBER,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This deletes all the order_florist_used records for the order detail id
        passed in.

Input:
        order_detail_id - NUMBER

Output:
        cursor containing the order_hold

-----------------------------------------------------------------------------*/


BEGIN

  DELETE FROM order_florist_used
     WHERE order_detail_id = in_order_detail_id;

  IF SQL%FOUND THEN
    out_status := 'Y';
  ELSE
    out_status := 'N';
    out_message := 'WARNING: No order_florist_used were deleted for order_detail_id ' || in_order_detail_id || '.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DELETE_ORDER_FLORIST_USED;


PROCEDURE INSERT_ORDER_REWARD_POSTING
(
IN_ORDER_DETAIL_ID     IN ORDER_REWARD_POSTING.ORDER_DETAIL_ID%TYPE,
IN_FILE_POST_DATE      IN ORDER_REWARD_POSTING.FILE_POST_DATE%TYPE,
IN_FILE_NAME           IN ORDER_REWARD_POSTING.FILE_NAME%TYPE,
IN_SOURCE_CODE         IN ORDER_REWARD_POSTING.SOURCE_CODE%TYPE,
IN_PROGRAM_NAME        IN ORDER_REWARD_POSTING.PROGRAM_NAME%TYPE,
IN_REWARD              IN ORDER_REWARD_POSTING.REWARD%TYPE,
IN_REWARD_DATE         IN ORDER_REWARD_POSTING.REWARD_DATE%TYPE,
IN_FILE_RECORD_ID      IN ORDER_REWARD_POSTING.FILE_RECORD_ID%TYPE,
IN_NO_PARTNER_ID       IN VARCHAR2,
IN_REWARD_BY_CART      IN VARCHAR2,
OUT_STATUS             OUT VARCHAR2,
OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the reward posting
        information.  Updates to order details and ftd_apps.partner_programs
        for the respective information

Input:
        order_detail_id        number
        file_post_date         date
        file_name              varchar2
        source_code            varchar2
        program_name           varchar2
        reward                 number
        reward_date            date
        file_record_id         varchar2
        no_partner_id	       varchar2 (Y or N)
        reward_by_cart	       varchar2 (Y or N)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO order_reward_posting
(
        order_detail_id,
        file_post_date,
        file_name,
        source_code,
        program_name,
        reward,
        reward_date,
        file_record_id
)
VALUES
(
        in_order_detail_id,
        in_file_post_date,
        in_file_name,
        in_source_code,
        in_program_name,
        in_reward,
        in_reward_date,
        in_file_record_id
);

IF IN_REWARD_BY_CART = 'Y' THEN
	UPDATE  order_details
	SET     miles_points = in_reward,
		miles_points_post_date = in_file_post_date,
		updated_on = sysdate
	WHERE   order_guid = (select order_guid
			      from   clean.order_details
			      where  order_detail_id = in_order_detail_id
			      );
ELSE

	UPDATE  order_details
	SET     miles_points = in_reward,
		miles_points_post_date = in_file_post_date,
		updated_on = sysdate
	WHERE   order_detail_id = in_order_detail_id;
END IF;

IF IN_NO_PARTNER_ID <> 'Y' THEN

	FTD_APPS.SOURCE_MAINT_PKG.UPDATE_PARTNER_PROG_LAST_POST
	(
		IN_PROGRAM_NAME=>in_program_name,
		IN_LAST_POST_DATE=>in_file_post_date,
		IN_UPDATED_BY=>'SYS',
		OUT_STATUS=>out_status,
		OUT_MESSAGE=>out_message
	);
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_REWARD_POSTING;


PROCEDURE DELETE_ORDER_REWARD_POSTING
(
IN_ORDER_DETAIL_ID     IN ORDER_REWARD_POSTING.ORDER_DETAIL_ID%TYPE,
OUT_STATUS             OUT VARCHAR2,
OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the posting record for the
        given order detail and null out the miles_date_posted and miles_point
        from the order detail record.

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_details
SET     miles_points = null,
        miles_points_post_date = null
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM order_reward_posting
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_ORDER_REWARD_POSTING;


PROCEDURE DELETE_ORDER_REWARD_POSTING
(
IN_FILE_NAME           IN ORDER_REWARD_POSTING.FILE_NAME%TYPE,
OUT_STATUS             OUT VARCHAR2,
OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the posting record for the
        given file_name and null out the miles_date_posted and miles_point
        from the order detail record.

Input:
        file_name                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_details od
SET     od.miles_points = null,
        od.miles_points_post_date = null
WHERE   EXISTS
(
        select  1
        from    order_reward_posting r
        where   r.file_name = in_file_name
        and     r.order_detail_id = od.order_detail_id
);

DELETE FROM order_reward_posting
WHERE   file_name = in_file_name;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_ORDER_REWARD_POSTING;


PROCEDURE UPDATE_CART_EMAIL
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_EMAIL_ADDRESS                IN EMAIL.EMAIL_ADDRESS%TYPE,
IN_UPDATED_BY                   IN ORDERS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating customer email associated
        to the the order (shopping cart)

Input:
        order_guid                      varchar2
        email_address                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR order_cur IS
        SELECT  o.customer_id,
                o.company_id
        FROM    orders o
        WHERE   o.order_guid = in_order_guid;

CURSOR email_cur (p_customer_id number, p_email_address varchar2) IS
        SELECT  e.email_id,
                cer.email_id
        FROM    email e
        LEFT OUTER JOIN customer_email_ref cer
        ON      e.email_id = cer.email_id
        AND     cer.customer_id = p_customer_id
        WHERE   e.email_address = p_email_address;

v_customer_id           orders.customer_id%type;
v_company_id            orders.company_id%type;
v_email_company_exists  email.email_id%type;
v_cust_email_exists     customer_email_ref.email_id%type;

BEGIN

-- get the customer id and company id from the order
OPEN order_cur;
FETCH order_cur INTO v_customer_id, v_company_id;
CLOSE order_cur;

v_company_id := GET_NEWSLETTER_COMPANY_ID(v_company_id);

-- check if the email/company combination exists and/or the customer/email/company combination exists
OPEN email_cur (v_customer_id, in_email_address);
FETCH email_cur INTO v_email_company_exists, v_cust_email_exists;
CLOSE email_cur;

IF v_email_company_exists IS NULL THEN
        -- insert the email/company record if the combination does not exist
        INSERT INTO email
        (
                email_id,
                company_id,
                email_address,
                created_on,
                created_by,
                updated_on,
                updated_by
        )
        VALUES
        (
                email_id_sq.nextval,
                v_company_id,
                in_email_address,
                sysdate,
                in_updated_by,
                sysdate,
                in_updated_by
        ) RETURNING email_id INTO v_email_company_exists;
END IF;

-- update the customer email record to not most record by company
UPDATE  customer_email_ref cer
SET     cer.most_recent_by_company = 'N',
        cer.updated_on = sysdate,
        cer.updated_by = in_updated_by
WHERE   cer.customer_id = v_customer_id
AND     exists
        (
                select  1
                from    email e
                where   e.email_id = cer.email_id
                and     company_id = v_company_id
        )
AND     most_recent_by_company = 'Y';

IF v_cust_email_exists IS NULL THEN
        -- insert the customer/email record if the combination does not exist
        INSERT INTO customer_email_ref
        (
                customer_id,
                email_id,
                most_recent_by_company,
                created_on,
                created_by,
                updated_on,
                updated_by
        )
        VALUES
        (
                v_customer_id,
                v_email_company_exists,
                'Y',
                sysdate,
                in_updated_by,
                sysdate,
                in_updated_by
        );
ELSE
        -- update the customer email reference record to be the most record by company
        UPDATE  customer_email_ref
        SET     most_recent_by_company = 'Y',
                updated_on = sysdate,
                updated_by = in_updated_by
        WHERE   customer_id = v_customer_id
        AND     email_id = v_cust_email_exists;
END IF;

-- update the order with the new email_id
UPDATE  orders
SET     email_id = v_email_company_exists,
        updated_on = sysdate,
        updated_by = in_updated_by
WHERE   order_guid = in_order_guid;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END UPDATE_CART_EMAIL;


PROCEDURE DELETE_CART_PAYMENT
(
IN_PAYMENT_ID           IN PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a cart payment record.
        A delete trigger on the payments table will save the deleted record
        in the shadow table.

Input:
        payment_id                      number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_payment_id number;

BEGIN

-- Only delete the cart payment record.  If a payment is not associated
-- to an additional bill or refund, it's a payment made on the cart.

select  payment_id
into    v_payment_id
from    payments
where   payment_id = in_payment_id
and     additional_bill_id is null
and     refund_id is null;

if v_payment_id > 0 then
    delete from payments_ext
    where payment_id = v_payment_id;

    delete FROM payments
    where payment_id = v_payment_id;
end if;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_CART_PAYMENT;

PROCEDURE UPDATE_CARRIER_DELIVERY
(
IN_ORDER_DETAIL_ID      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CARRIER              IN VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is populating the actual ship method for the order
        for the given carrier.

Input:
        order_detail_id                 number
        carrier                         varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_ship_method varchar2(2);

BEGIN

select v.SHIP_METHOD INTO v_ship_method from venus.venus v
  where v.REFERENCE_NUMBER = to_char(IN_ORDER_DETAIL_ID)
  and v.MSG_TYPE='FTD'
  and v.CREATED_ON = (select MAX(v2.CREATED_ON)from venus.VENUS v2
                        where v2.REFERENCE_NUMBER = to_char(IN_ORDER_DETAIL_ID)
                        and v2.MSG_TYPE='FTD');

UPDATE  order_details od
SET     od.carrier_delivery = (select cd.carrier_delivery from venus.carrier_delivery cd
                                where   cd.carrier_id = in_carrier
                                and     cd.ship_method = v_ship_method)

WHERE   od.order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END UPDATE_CARRIER_DELIVERY;



PROCEDURE UPDATE_ORDER_BILLS_STATUS
(
IN_ORDER_DETAIL_ID      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_STATUS               IN VARCHAR2,
IN_CSR_ID               IN VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates order_bills.status to the status
         passed in for the order detail id passed in.

Input:
        order_detail_id       NUMBER
        status                VARCHAR2
        csr_id                VARCHAR2

Output:
        status          varchar2 (Y or N)
        message         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE order_bills
     SET bill_status = in_status,
		 bill_date = decode(in_status,'Billed',SYSDATE,null),
         updated_on = SYSDATE,
         updated_by = in_csr_id
   WHERE order_detail_id = in_order_detail_id;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END UPDATE_ORDER_BILLS_STATUS;

PROCEDURE CANCEL_HELD_ORDER
(
IN_ORDER_DETAIL_ID      IN  ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID               IN  VARCHAR2,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Defect 902
        This procedure marks all order_bill and refund records associated with
        the order_detail_id as 'Billed', updates order_details.eod_delivery_indicator
        with an 'X', and sets order_details.order_disp_code to 'Processed'.

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_status      VARCHAR2(100);
v_message     VARCHAR2(2000);

BEGIN

 -- defect 3624: remove accounting logic
 -- set order_bills.bill_status to 'Billed'
 --update_order_bills_status (in_order_detail_id, 'Billed', in_csr_id, v_status, v_message);

 -- set refund.refund_status to 'Billed'
 --refund_pkg.update_refund_acctg_status (in_order_detail_id, 'Billed', in_csr_id, v_status, v_message);

 -- set order_details.eod_delivery_indicator to 'X'
 --update_eod_delivery_ind (in_order_detail_id, 'X', in_csr_id, v_status, v_message);

 -- set order_details.order_disp_code to 'Processed'.
 update_order_disposition (in_order_detail_id, 'Processed', in_csr_id, v_status, v_message);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END CANCEL_HELD_ORDER;

PROCEDURE UPDATE_ORDER_DETAIL_SHIP_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_VENDOR_ID                    IN ORDER_DETAILS.VENDOR_ID%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_CARRIER_DELIVERY             IN ORDER_DETAILS.CARRIER_DELIVERY%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record.

Input:
        order_detail_id                 number
        vendor_id                       varchar2
        florist_id                      varchar2
        carrier_delivery                varchar2
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  order_details
SET
        vendor_id = IN_VENDOR_ID,
        florist_id = IN_FLORIST_ID,
        carrier_delivery = IN_CARRIER_DELIVERY,
        updated_on = sysdate,
        updated_by = IN_UPDATED_BY
WHERE   order_detail_id = IN_ORDER_DETAIL_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAIL_SHIP_INFO;


PROCEDURE UPDATE_PMT_AP_CAPTURE_TXT
(
IN_PAYMENT_ID              	IN PAYMENTS.PAYMENT_ID%TYPE,
IN_AP_CAPTURE_TXT               IN PAYMENTS.AP_CAPTURE_TXT%TYPE,
IN_UPDATED_BY			IN PAYMENTS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating payment record's ap_capture_txt.

Input:
        in_payment_id           number
        in_ap_capture_txt       varchar2
        in_updated_by		varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  payments
SET
        ap_capture_txt = in_ap_capture_txt,
        updated_on = sysdate,
        updated_by = IN_UPDATED_BY
WHERE   payment_id = IN_PAYMENT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PMT_AP_CAPTURE_TXT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PMT_AP_CAPTURE_TXT;


PROCEDURE UPDATE_PMT_AP_AUTH_TXT
(
IN_PAYMENT_ID               IN PAYMENTS.PAYMENT_ID%TYPE,
IN_AP_AUTH_TXT               IN PAYMENTS.AP_AUTH_TXT%TYPE,
IN_UPDATED_BY           IN PAYMENTS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating payment record's ap_auth_txt.

Input:
        in_payment_id           number
        in_ap_auth_txt       varchar2
        in_updated_by       varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  payments
SET
        ap_auth_txt = in_ap_auth_txt,
        updated_on = sysdate,
        updated_by = IN_UPDATED_BY
WHERE   payment_id = IN_PAYMENT_ID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PMT_AP_AUTH_TXT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PMT_AP_AUTH_TXT;


PROCEDURE UPDATE_AP_REFUND_TRANS_TXT
(
IN_PAYMENT_ID              	IN PAYMENTS.PAYMENT_ID%TYPE,
IN_AP_REFUND_TRANS_TXT          IN REFUND.AP_REFUND_TRANS_TXT%TYPE,
IN_UPDATED_BY			IN PAYMENTS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating refund record's ap_refund_trans_txt
        for the given refund payment id.

Input:
        in_payment_id           number
        in_ap_refund_trans_txt  varchar2
        in_updated_by		varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  refund
SET
        ap_refund_trans_txt = in_ap_refund_trans_txt,
        updated_on = sysdate,
        updated_by = IN_UPDATED_BY
WHERE   refund_id = (SELECT refund_id
		       FROM payments
		      WHERE payment_id = IN_PAYMENT_ID);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_AP_REFUND_TRANS_TXT [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_AP_REFUND_TRANS_TXT;

PROCEDURE UPDATE_REM_UPDATE
(
IN_PARTNER_ID				         IN CLEAN.REMITTANCE_UPDATE.PARTNER_ID%TYPE,
IN_REMITTANCE_NUMBER					IN CLEAN.REMITTANCE_UPDATE.REMITTANCE_NUMBER%TYPE,
IN_REMITTANCE_DATE					IN CLEAN.REMITTANCE_UPDATE.REMITTANCE_DATE%TYPE,
IN_REMITTANCE_AMT						IN CLEAN.REMITTANCE_UPDATE.REMITTANCE_AMT%TYPE,
IN_UPDATED_BY							IN CLEAN.REMITTANCE_UPDATE.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating CLEAN.REMITTANCE_UPDATE table
        and CLEAN.REMITTANCE_DETAILS_UPDATE table for the given partner id

Input:
        in_partner_id				  varchar2
        in_remittance_number		  varchar2
        in_remittance_date			  date
        in_remittance_amt			  number
        in_updated_by				  varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	UPDATE clean.remittance_update
	   SET remittance_date = in_remittance_date,
	   	 remittance_amt = in_remittance_amt,
	       remittance_number = in_remittance_number,
			 updated_on = SYSDATE,
			 updated_by = in_updated_by
	 WHERE partner_id = in_partner_id;

	 UPDATE clean.remittance_details_update
		 SET remittance_number = in_remittance_number,
			  updated_on = SYSDATE,
			  updated_by = in_updated_by
	  WHERE partner_id = in_partner_id;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_REM_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REM_UPDATE;

PROCEDURE UPDATE_REM_DTLS_UPDATE
(
IN_PARTNER_ID							IN CLEAN.REMITTANCE_DETAILS_UPDATE.PARTNER_ID%TYPE,
IN_REMITTANCE_NUMBER					IN CLEAN.REMITTANCE_DETAILS_UPDATE.REMITTANCE_NUMBER%TYPE,
IN_ORDER_DETAIL_ID					IN CLEAN.REMITTANCE_DETAILS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_CASH_RECEIVED_AMT					IN CLEAN.REMITTANCE_DETAILS_UPDATE.CASH_RECEIVED_AMT%TYPE,
IN_WRITE_OFF_AMT						IN CLEAN.REMITTANCE_DETAILS_UPDATE.WRITE_OFF_AMT%TYPE,
IN_REMAINING_BALANCE_DUE_AMT		IN CLEAN.REMITTANCE_DETAILS_UPDATE.REMAINING_BALANCE_DUE_AMT%TYPE,
IN_SETTLE_FLAG							IN CLEAN.REMITTANCE_DETAILS_UPDATE.SETTLE_FLAG%TYPE,
IN_UPDATED_BY							IN CLEAN.REMITTANCE_DETAILS_UPDATE.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating CLEAN.REMITTANCE_DETAILS_UPDATE table
        for the given partner id, remittance number and order detail id.

Input:
        in_partner_id				          varchar2
        in_remittance_number					 varchar2
        in_order_detail_id						 number
        in_cash_received_amt			  		 number
        in_write_off_amt						 number
        in_remaining_balance_due_amt		 number
        in_settle_flag							 varchar2
        in_updated_by				  			 varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	UPDATE clean.remittance_details_update
	   SET cash_received_amt = in_cash_received_amt,
	   	 remittance_number = in_remittance_number,
	   	 write_off_amt = in_write_off_amt,
	   	 remaining_balance_due_amt = in_remaining_balance_due_amt,
	   	 settle_flag = in_settle_flag,
			 updated_on = SYSDATE,
			 updated_by = in_updated_by
	 WHERE partner_id = in_partner_id
	   AND order_detail_id = in_order_detail_id;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_REM_DTLS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REM_DTLS_UPDATE;

PROCEDURE DELETE_REM_UPDATE
(
IN_PARTNER_ID  	            	IN CLEAN.REMITTANCE_UPDATE.PARTNER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting CLEAN.REMITTANCE_UPDATE table
        for the given partner id.

Input:
        in_partner_id           varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	DELETE FROM clean.remittance_update
	 WHERE partner_id = in_partner_id;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DELETE_REM_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_REM_UPDATE;

PROCEDURE DELETE_REM_DTLS_UPDATE
(
IN_PARTNER_ID							IN CLEAN.REMITTANCE_DETAILS_UPDATE.PARTNER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting CLEAN.REMITTANCE_DETAILS_UPDATE table
        for the given partner id.

Input:
        in_partner_id           varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	DELETE FROM clean.remittance_details_update
	 WHERE partner_id = in_partner_id;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN DELETE_REM_DTLS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_REM_DTLS_UPDATE;

PROCEDURE INSERT_REMITTANCE
(
IN_PARTNER_ID  	            	IN CLEAN.REMITTANCE_UPDATE.PARTNER_ID%TYPE,
IN_REMITTANCE_NUMBER					IN CLEAN.REMITTANCE.REMITTANCE_NUMBER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting into CLEAN.REMITTANCE table
        for the given partner id and remittance number, from CLEAN.REMITTANCE_UPDATE table

Input:
        in_partner_id           varchar2
        in_remittance_number	  varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN


   INSERT INTO clean.remittance
   SELECT partner_id,
   		 remittance_number,
   		 remittance_date,
   		 remittance_amt,
   		 created_on,
   		 created_by,
   		 updated_on,
   		 updated_by
     FROM clean.remittance_update
    WHERE partner_id = in_partner_id
      AND remittance_number = in_remittance_number;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_REMITTANCE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REMITTANCE;

PROCEDURE INSERT_REMITTANCE_DETAILS
(
IN_PARTNER_ID              		IN CLEAN.REMITTANCE_DETAILS_UPDATE.PARTNER_ID%TYPE,
IN_REMITTANCE_NUMBER         		IN CLEAN.REMITTANCE_DETAILS_UPDATE.REMITTANCE_NUMBER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting into CLEAN.REMITTANCE_DETAILS table
        for the given partner id, remittance number from CLEAN.REMITTANCE_DETAILS_UPDATE table

Input:
        in_partner_id           varchar2
        in_remittance_number	  varchar2
        in_order_detail_id		  number

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN


   INSERT INTO clean.remittance_details
   SELECT partner_id,
   		 remittance_number,
   		 order_detail_id,
   		 cash_received_amt,
   		 write_off_amt,
   		 remaining_balance_due_amt,
   		 created_on,
   		 created_by,
   		 updated_on,
   		 updated_by
     FROM clean.remittance_details_update
    WHERE partner_id = in_partner_id
      AND remittance_number = in_remittance_number
      AND settle_flag = 'Y';

	OUT_STATUS := 'Y';

   -- Update payments bill_status - Usecase 23033
   IF OUT_STATUS = 'Y' THEN
	   UPDATE clean.payments p
	      SET p.bill_status = 'Settled',
	      	 p.bill_date = SYSDATE
	    WHERE p.order_guid IN (
	    		SELECT od.order_guid
	    		FROM clean.order_details od
				WHERE od.order_detail_id IN (
	    		SELECT rdu.order_detail_id
	    		  FROM clean.remittance_details_update rdu
	    		 WHERE rdu.partner_id = in_partner_id
	    		   AND rdu.remittance_number = in_remittance_number
	    		   AND rdu.settle_flag = 'Y'
	    		   AND rdu.remaining_balance_due_amt = 0
	    		   AND rdu.order_detail_id = od.order_detail_id));
	   OUT_STATUS := 'Y';
   END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_REMITTANCE_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REMITTANCE_DETAILS;

PROCEDURE INSERT_REM_UPDATE
(
IN_PARTNER_ID  	            	IN CLEAN.REMITTANCE_UPDATE.PARTNER_ID%TYPE,
IN_REMITTANCE_NUMBER					IN CLEAN.REMITTANCE_UPDATE.REMITTANCE_NUMBER%TYPE,
IN_REMITTANCE_DATE					IN CLEAN.REMITTANCE_UPDATE.REMITTANCE_DATE%TYPE,
IN_REMITTANCE_AMT						IN CLEAN.REMITTANCE_UPDATE.REMITTANCE_AMT%TYPE,
IN_CREATED_BY							IN CLEAN.REMITTANCE_UPDATE.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting into CLEAN.REMITTANCE_UPDATE table
        for the given partner id and remittance number

Input:
        in_partner_id           varchar2
        in_remittance_number	  varchar2
        in_remittance_date		  date
        in_remittance_amt		  number
        in_created_by			  varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN


   INSERT INTO clean.remittance_update (
   	partner_id,
   	remittance_number,
   	remittance_date,
   	remittance_amt,
   	created_on,
   	created_by,
   	updated_on,
   	updated_by)
   VALUES (
   	in_partner_id,
   	in_remittance_number,
   	in_remittance_date,
   	in_remittance_amt,
   	SYSDATE,
   	in_created_by,
   	SYSDATE,
   	in_created_by);

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_REM_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REM_UPDATE;

PROCEDURE INSERT_REM_DTLS_UPDATE
(
IN_PARTNER_ID              		IN CLEAN.REMITTANCE_DETAILS_UPDATE.PARTNER_ID%TYPE,
IN_REMITTANCE_NUMBER         		IN CLEAN.REMITTANCE_DETAILS_UPDATE.REMITTANCE_NUMBER%TYPE,
IN_ORDER_DETAIL_ID           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_PARTNER_ORDER_ID           	IN CLEAN.REMITTANCE_DETAILS_UPDATE.PARTNER_ORDER_ID%TYPE,
IN_LINE_ID				       		IN CLEAN.REMITTANCE_DETAILS_UPDATE.LINE_ID%TYPE,
IN_EXTERNAL_ORDER_NUMBER     		IN CLEAN.REMITTANCE_DETAILS_UPDATE.EXTERNAL_ORDER_NUMBER%TYPE,
IN_BILLING_DATE		        		IN CLEAN.REMITTANCE_DETAILS_UPDATE.BILLING_DATE%TYPE,
IN_DELIVERY_DATE           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.DELIVERY_DATE%TYPE,
IN_PDB_PRICE_AMT           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.PDB_PRICE_AMT%TYPE,
IN_REMITTANCE_APPLIED_AMT    		IN CLEAN.REMITTANCE_DETAILS_UPDATE.REMITTANCE_APPLIED_AMT%TYPE,
IN_CASH_RECEIVED_AMT        		IN CLEAN.REMITTANCE_DETAILS_UPDATE.CASH_RECEIVED_AMT%TYPE,
IN_DAYS_AGED_QTY           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.DAYS_AGED_QTY%TYPE,
IN_WRITE_OFF_AMT           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.WRITE_OFF_AMT%TYPE,
IN_OUTSTANDING_BALANCE_DUE_AMT	IN CLEAN.REMITTANCE_DETAILS_UPDATE.OUTSTANDING_BALANCE_DUE_AMT%TYPE,
IN_REMAINING_BALANCE_DUE_AMT 		IN CLEAN.REMITTANCE_DETAILS_UPDATE.REMAINING_BALANCE_DUE_AMT%TYPE,
IN_SETTLE_FLAG		           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.SETTLE_FLAG%TYPE,
IN_CREATED_BY		           		IN CLEAN.REMITTANCE_DETAILS_UPDATE.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting into CLEAN.REMITTANCE_DETAILS_UPDATE table
        for the given partner id, remittance number and order detail id

Input:
        in_partner_id      		     		 varchar2
        in_remittance_number			  		 varchar2
        in_order_detail_id		  		  		 number
        in_partner_order_id			 		 varchar2
        in_line_id						 		 varchar2
        in_external_order_number		 		 varchar2
        in_billing_date					 		 date
        in_delivery_date				 		 date
        in_pdb_price_amt				 		 number
        in_remittance_applied_amt	 		 number
        in_cash_received_amt			 		 number
        in_days_aged_qty				 		 number
        in_write_off_amt				 		 number
        in_outstanding_balance_due_amt		 number
        in_remaining_balance_due_amt		 number
        in_settle_flag							 varchar2
        in_created_by							 varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN


   INSERT INTO clean.remittance_details_update (
   	partner_id,
   	remittance_number,
   	order_detail_id,
   	partner_order_id,
   	line_id,
   	external_order_number,
   	billing_date,
   	delivery_date,
   	pdb_price_amt,
   	remittance_applied_amt,
   	cash_received_amt,
   	days_aged_qty,
   	write_off_amt,
   	outstanding_balance_due_amt,
   	remaining_balance_due_amt,
   	settle_flag,
   	created_on,
   	created_by,
   	updated_on,
   	updated_by)
   VALUES (
   	in_partner_id,
   	in_remittance_number,
   	in_order_detail_id,
   	in_partner_order_id,
   	in_line_id,
   	in_external_order_number,
   	in_billing_date,
   	in_delivery_date,
   	in_pdb_price_amt,
   	in_remittance_applied_amt,
   	in_cash_received_amt,
   	in_days_aged_qty,
   	in_write_off_amt,
   	in_outstanding_balance_due_amt,
   	in_remaining_balance_due_amt,
   	in_settle_flag,
   	SYSDATE,
   	in_created_by,
   	SYSDATE,
   	in_created_by);

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_REM_DTLS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REM_DTLS_UPDATE;

PROCEDURE UPDATE_CART_BILLED
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_PAYMENT_TYPE					IN PAYMENTS.PAYMENT_TYPE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the payments bill status to Billed
        for the shopping cart. It also sets the auth date for the payment. Used for
        miles points orders.

Input:
        in_order_detail_id     		     		 varchar2
        in_payment_type							 varchar2


Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	update	payments p
	set	p.bill_status = 'Billed',
		p.bill_date = SYSDATE,
		p.updated_on = SYSDATE,
		p.updated_by = 'SYS',
		auth_date = trunc(sysdate)
	where	p.payment_id in
		(select payment_id
		 from	payments p2
		 join	order_details od
		 on	od.order_guid = p2.order_guid
		 where	od.order_detail_id = in_order_detail_id
		 and 	p2.payment_indicator = 'P'
		 and	p2.additional_bill_id is null
		) and p.payment_type = in_payment_type;

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CART_BILLED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CART_BILLED;

PROCEDURE UPDATE_DCON_STATUS (
IN_ORDER_DETAIL_ID     IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_STATUS              IN ORDER_DETAILS.DELIVERY_CONFIRMATION_STATUS%TYPE,
IN_CSR_ID              IN ORDER_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS            OUT VARCHAR2,
OUT_MESSAGE           OUT VARCHAR2
) AS

BEGIN

    UPDATE ORDER_DETAILS
    SET DELIVERY_CONFIRMATION_STATUS = IN_STATUS,
    UPDATED_ON = SYSDATE,
    UPDATED_BY = IN_CSR_ID
    WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_DCON_STATUS;

PROCEDURE UPDATE_PAYMENTS
(
IN_ADDITIONAL_BILL_ID           IN PAYMENTS.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS.AUTH_NUMBER%TYPE,
IN_AP_AUTH_TXT                  IN PAYMENTS.AP_AUTH_TXT%TYPE,
IN_AVS_CODE                     IN PAYMENTS.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS.AUTH_DATE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS.AAFES_TICKET_NUMBER%TYPE,
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
IN_MILES_POINTS_CREDIT_AMT      IN PAYMENTS.MILES_POINTS_CREDIT_AMT%TYPE,
IN_MILES_POINTS_DEBIT_AMT       IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
IN_NC_APPROVAL_ID               IN PAYMENTS.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_BILL_STATUS					IN PAYMENTS.BILL_STATUS%TYPE,
IN_BILL_DATE					IN PAYMENTS.BILL_DATE%TYPE,
IN_CARDINAL_VERIFIED_FLAG		IN PAYMENTS.CARDINAL_VERIFIED_FLAG%TYPE,
IN_ROUTE						IN PAYMENTS.ROUTE%TYPE,
IN_TOKEN_ID                     IN PAYMENTS.TOKEN_ID%TYPE,
IN_AUTH_TRANSACTION_ID          IN PAYMENTS.AUTHORIZATION_TRANSACTION_ID%TYPE,
IO_PAYMENT_ID                   IN OUT PAYMENTS.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting payment information
        overloading the procedure above.  This proc gets the order information
        from the given order, and breaks apart the acq_reference_number
        into the five individual fields.  The procedure is being called from the
        add billing and refunds.

Input:
        additional_bill_id              number
        payment_type                    varchar2
        cc_id                           number
        updated_by                      varchar2
        auth_result                     varchar2
        auth_number                     varchar2
        ap_auth_txt                     varchar2
        avs_code                        varchar2
        acq_reference_number            varchar2
        gc_coupon_number                varchar2
        auth_override_flag              char
        credit_amount                   number
        debit_amount                    number
        payment_indicator               char
        refund_id                       number
        auth_date                       date
        aafes_ticket_number             varchar2
        order_guid                      varchar2
        bill_status						varchar2
        bill_date						date
        payment_id                      number

Output:
        payment_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR order_guid IS
        SELECT  od.order_guid
        FROM    order_details od
        WHERE   EXISTS
        (       select  1
                from    order_bills ob
                where   ob.order_detail_id = od.order_detail_id
                and     ob.order_bill_id = in_additional_bill_id
        )
        union
        SELECT  od.order_guid
        FROM    order_details od
        where   EXISTS
        (       select  1
                from    refund r
                where   r.order_detail_id = od.order_detail_id
                and     r.refund_id = in_refund_id
        );

v_order_guid            orders.order_guid%type;
v_auth_char_indicator   payments.auth_char_indicator%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'a')+1, instr(in_acq_reference_number||'abcdef', 'b')-instr(in_acq_reference_number||'abcdef', 'a')-1);
v_transaction_id        payments.transaction_id%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'b')+1, instr(in_acq_reference_number||'abcdef', 'c')-instr(in_acq_reference_number||'abcdef', 'b')-1);
v_validation_code       payments.validation_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'c')+1, instr(in_acq_reference_number||'abcdef', 'd')-instr(in_acq_reference_number||'abcdef', 'c')-1);
v_auth_source_code      payments.auth_source_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'd')+1, instr(in_acq_reference_number||'abcdef', 'e')-instr(in_acq_reference_number||'abcdef', 'd')-1);
v_response_code         payments.response_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'e')+1, instr(in_acq_reference_number||'abcdef', 'f')-instr(in_acq_reference_number||'abcdef', 'e')-1);
/* The 'f' portion of the data, which is a product identifier, is only used for the credit card settlement file and is parsed out when the settlement file is created.  v_product_identifier            varchar2(3) := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'f')+1); */

BEGIN

IF in_order_guid IS NULL THEN
        OPEN order_guid;
        FETCH order_guid INTO v_order_guid;
        CLOSE order_guid;
ELSE
        v_order_guid := in_order_guid;
END IF;

UPDATE_PAYMENTS
(
        IN_ORDER_GUID=>v_order_guid,
        IN_ADDITIONAL_BILL_ID=>in_additional_bill_id,
        IN_PAYMENT_TYPE=>in_payment_type,
        IN_CC_ID=>in_cc_id,
        IN_UPDATED_BY=>in_updated_by,
        IN_AUTH_RESULT=>in_auth_result,
        IN_AUTH_NUMBER=>in_auth_number,
        IN_AVS_CODE=>in_avs_code,
        IN_ACQ_REFERENCE_NUMBER=>in_acq_reference_number,
        IN_GC_COUPON_NUMBER=>in_gc_coupon_number,
        IN_AUTH_OVERRIDE_FLAG=>in_auth_override_flag,
        IN_CREDIT_AMOUNT=>in_credit_amount,
        IN_DEBIT_AMOUNT=>in_debit_amount,
        IN_PAYMENT_INDICATOR=>in_payment_indicator,
        IN_REFUND_ID=>in_refund_id,
        IN_AUTH_DATE=>in_auth_date,
        IN_AUTH_CHAR_INDICATOR=>v_auth_char_indicator,
        IN_TRANSACTION_ID=>v_transaction_id,
        IN_VALIDATION_CODE=>v_validation_code,
        IN_AUTH_SOURCE_CODE=>v_auth_source_code,
        IN_RESPONSE_CODE=>v_response_code,
        IN_AAFES_TICKET_NUMBER=>in_aafes_ticket_number,
        IN_AP_ACCOUNT_ID=>NULL,
        IN_AP_AUTH_TXT=>in_ap_auth_txt,
        IN_NC_APPROVAL_IDENTITY_ID=>in_nc_approval_id,
        IN_NC_TYPE_CODE=>NULL,
        IN_NC_REASON_CODE=>NULL,
        IN_NC_ORDER_DETAIL_ID=>NULL,
        IN_MILES_POINTS_CREDIT_AMT=>IN_MILES_POINTS_CREDIT_AMT,
        IN_MILES_POINTS_DEBIT_AMT=>IN_MILES_POINTS_DEBIT_AMT,
	IN_CSC_RESPONSE_CODE=>NULL,
	IN_CSC_VALIDATED_FLAG=>'N',
	IN_CSC_FAILURE_CNT=>0,
        IN_BILL_STATUS=>in_bill_status,
        IN_BILL_DATE=>in_bill_date,
  IN_WALLET_INDICATOR=>NULL,
  		IN_CC_AUTH_PROVIDER=>NULL,
  		IN_INITIAL_AUTH_AMOUNT=>NULL,
  		IN_PAYMENT_EXT_INFO=>NULL,
  		IN_IS_VOICE_AUTH=>NULL,
        IN_CARDINAL_VERIFIED_FLAG=>IN_CARDINAL_VERIFIED_FLAG,
        IN_ROUTE => IN_ROUTE,
  	IN_TOKEN_ID => IN_TOKEN_ID,
  	IN_AUTH_TRANSACTION_ID => IN_AUTH_TRANSACTION_ID,
        IO_PAYMENT_ID=>io_payment_id,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PAYMENTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PAYMENTS;

PROCEDURE UPDATE_UNIQUE_CREDIT_CARDS
(
IN_CC_TYPE                      IN CREDIT_CARDS.CC_TYPE%TYPE,
IN_CC_NUMBER                    IN CREDIT_CARDS.CC_NUMBER%TYPE,
IN_GIFT_CARD_PIN                IN CREDIT_CARDS.GIFT_CARD_PIN%TYPE,
IN_CC_EXPIRATION                IN CREDIT_CARDS.CC_EXPIRATION%TYPE,
IN_NAME                         IN CREDIT_CARDS.NAME%TYPE,
IN_ADDRESS_LINE_1               IN CREDIT_CARDS.ADDRESS_LINE_1%TYPE,
IN_ADDRESS_LINE_2               IN CREDIT_CARDS.ADDRESS_LINE_2%TYPE,
IN_CITY                         IN CREDIT_CARDS.CITY%TYPE,
IN_STATE                        IN CREDIT_CARDS.STATE%TYPE,
IN_ZIP_CODE                     IN CREDIT_CARDS.ZIP_CODE%TYPE,
IN_COUNTRY                      IN CREDIT_CARDS.COUNTRY%TYPE,
IN_UPDATED_BY                   IN CREDIT_CARDS.UPDATED_BY%TYPE,
IN_CUSTOMER_ID                  IN CREDIT_CARDS.CUSTOMER_ID%TYPE,
OUT_CC_ID                       OUT CREDIT_CARDS.CC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating credit card
        information based on the customer id, card type, and card number

        This is the procedure to use if your credit card number is in plaintext -
        it will encrypt it for you

Input:
        cc_type                         varchar2
        cc_number                       varchar2 (cc number comes in plain text)
        gift_card_pin                   varchar2 (gift_card_pin comes in plain text)
        cc_expiration                   varchar2
        name                            varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        updated_by                      varchar2
        customer_id                     number
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_key_name credit_cards.key_name%type;

BEGIN

v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

UPDATE_UNIQUE_CC_CRYPT (
   IN_CC_TYPE                      => in_cc_type,
   IN_CC_NUMBER                    => global.encryption.encrypt_it(in_cc_number, v_key_name),
   IN_CC_EXPIRATION                => in_cc_expiration,
   IN_NAME                         => in_name,
   IN_PIN                          => global.encryption.encrypt_it(in_gift_card_pin, v_key_name),
   IN_ADDRESS_LINE_1               => in_address_line_1,
   IN_ADDRESS_LINE_2               => in_address_line_2,
   IN_CITY                         => in_city,
   IN_STATE                        => in_state,
   IN_ZIP_CODE                     => in_zip_code,
   IN_COUNTRY                      => IN_COUNTRY,
   IN_UPDATED_BY                   => IN_UPDATED_BY,
   IN_CUSTOMER_ID                  => IN_CUSTOMER_ID,
   IN_KEY_NAME                     => v_key_name,
   IN_CC_NUMBER_MASKED             => substr(in_cc_number, -4, 4),
   OUT_CC_ID                       => OUT_CC_ID,
   OUT_STATUS                      => OUT_STATUS,
   OUT_MESSAGE                     => OUT_MESSAGE);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_UNIQUE_CREDIT_CARDS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_UNIQUE_CREDIT_CARDS;

PROCEDURE UPDATE_ORDER_SHIPPING_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the shipping info for an order detail record.

Input:
        order_detail_id                 number
        ship_method                     varchar2
        ship_date                       date
        delivery_date			date  (this only comes into play for FTD's sent from communication screen)
        updated_by			varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

IF IN_DELIVERY_DATE IS NOT NULL THEN
	UPDATE  order_details
	SET
	        ship_method = NVL(in_ship_method, ship_method),
	        ship_date = NVL(in_ship_date, ship_date),
	        delivery_date = in_delivery_date,
	        updated_by = in_updated_by,
	        updated_on = sysdate
	WHERE   order_detail_id = in_order_detail_id;
ELSE
	UPDATE  order_details
	SET
	        ship_method = NVL(in_ship_method, ship_method),
	        ship_date = NVL(in_ship_date, ship_date),
	        updated_by = in_updated_by,
	        updated_on = sysdate
	WHERE   order_detail_id = in_order_detail_id;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_SHIPPING_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_SHIPPING_INFO;

PROCEDURE INSERT_AVS_ADDRESS
(
IN_AVS_ADDRESS_ID              IN CLEAN.AVS_ADDRESS.AVS_ADDRESS_ID%TYPE,
IN_ADDRESS                     IN CLEAN.AVS_ADDRESS.ADDRESS%TYPE,
IN_CITY                        IN CLEAN.AVS_ADDRESS.CITY%TYPE,
IN_STATE                       IN CLEAN.AVS_ADDRESS.STATE%TYPE,
IN_ZIP                         IN CLEAN.AVS_ADDRESS.ZIP%TYPE,
IN_COUNTRY                     IN CLEAN.AVS_ADDRESS.COUNTRY%TYPE,
IN_LATITUDE                    IN CLEAN.AVS_ADDRESS.LATITUDE%TYPE,
IN_LONGITUDE                   IN CLEAN.AVS_ADDRESS.LONGITUDE%TYPE,
IN_ENTITY_TYPE                 IN CLEAN.AVS_ADDRESS.ENTITY_TYPE%TYPE,
IN_OVERRIDE_FLAG               IN CLEAN.AVS_ADDRESS.OVERRIDE_FLAG%TYPE,
IN_AVS_RESULT                  IN CLEAN.AVS_ADDRESS.AVS_RESULT%TYPE,
IN_AVS_PERFORMED               IN CLEAN.AVS_ADDRESS.AVS_PERFORMED%TYPE,
IN_AVS_PERFORMED_ORIGIN        IN CLEAN.AVS_ADDRESS.AVS_PERFORMED_ORIGIN%TYPE,
IN_CREATED_ON                  IN CLEAN.AVS_ADDRESS.CREATED_ON%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting into CLEAN.AVS_ADDRESS table

Input:
        IN_AVS_ADDRESS_ID      		     		 varchar2
        IN_ADDRESS			  		 varchar2
        IN_CITY		  		  		 number
        IN_STATE			 		 varchar2
        IN_ZIP						 		 varchar2
        IN_COUNTRY		 		 varchar2
        IN_LATITUDE					 		 date
        IN_LONGITUDE				 		 date
        IN_ENTITY_TYPE				 		 number
        IN_OVERRIDE_FLAG	 		 number
        IN_AVS_RESULT			 		 number
        IN_AVS_PERFORMED				 		 number
        IN_AVS_PERFORMED_ORIGIN				 		 number
        IN_CREATED_ON

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN


   INSERT INTO clean.avs_address (
   	avs_address_id,
   	address,
   	city,
   	state,
   	zip,
   	country,
   	latitude,
   	longitude,
   	entity_type,
   	override_flag,
   	avs_result,
   	avs_performed,
   	avs_performed_origin,
   	created_on)
   VALUES (
   	in_avs_address_id,
   	in_address,
   	in_city,
   	in_state,
   	in_zip,
   	in_country,
   	in_latitude,
   	in_longitude,
   	in_entity_type,
   	in_override_flag,
   	in_avs_result,
   	in_avs_performed,
   	in_avs_performed_origin,
   	in_created_on);

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_AVS_ADDRESS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AVS_ADDRESS;

PROCEDURE UPDATE_AVS_ADDRESS
(
IN_AVS_ADDRESS_ID              IN CLEAN.AVS_ADDRESS.AVS_ADDRESS_ID%TYPE,
IN_ADDRESS                     IN CLEAN.AVS_ADDRESS.ADDRESS%TYPE,
IN_CITY                        IN CLEAN.AVS_ADDRESS.CITY%TYPE,
IN_STATE                       IN CLEAN.AVS_ADDRESS.STATE%TYPE,
IN_ZIP                         IN CLEAN.AVS_ADDRESS.ZIP%TYPE,
IN_COUNTRY                     IN CLEAN.AVS_ADDRESS.COUNTRY%TYPE,
IN_LATITUDE                    IN CLEAN.AVS_ADDRESS.LATITUDE%TYPE,
IN_LONGITUDE                   IN CLEAN.AVS_ADDRESS.LONGITUDE%TYPE,
IN_ENTITY_TYPE                 IN CLEAN.AVS_ADDRESS.ENTITY_TYPE%TYPE,
IN_OVERRIDE_FLAG               IN CLEAN.AVS_ADDRESS.OVERRIDE_FLAG%TYPE,
IN_AVS_RESULT                  IN CLEAN.AVS_ADDRESS.AVS_RESULT%TYPE,
IN_AVS_PERFORMED               IN CLEAN.AVS_ADDRESS.AVS_PERFORMED%TYPE,
IN_AVS_PERFORMED_ORIGIN        IN CLEAN.AVS_ADDRESS.AVS_PERFORMED_ORIGIN%TYPE,
IN_CREATED_ON                  IN CLEAN.AVS_ADDRESS.CREATED_ON%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the avs_address table.

Input:
        IN_AVS_ADDRESS_ID      		     		 number
        IN_ADDRESS			  		 varchar2
        IN_CITY		  		  		 varchar2
        IN_STATE			 		 varchar2
        IN_ZIP						 varchar2
        IN_COUNTRY		 		         varchar2
        IN_LATITUDE					 varchar2
        IN_LONGITUDE				 	 varchar2
        IN_ENTITY_TYPE				 	 varchar2
        IN_OVERRIDE_FLAG	 		         varchar2
        IN_AVS_RESULT			 		 varchar2
        IN_AVS_PERFORMED				 varchar2
        IN_AVS_PERFORMED_ORIGIN				 varchar2
        IN_CREATED_ON                                    timestamp

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	UPDATE  avs_address
	SET
	   address = in_address,
   	   city = in_city,
   	   state = in_state,
   	   zip = in_zip,
   	   country = in_country,
   	   latitude = in_latitude,
   	   longitude = in_longitude,
   	   entity_type = in_entity_type,
   	   override_flag = in_override_flag,
   	   avs_result = in_avs_result,
   	   avs_performed = in_avs_performed,
   	   avs_performed_origin = in_avs_performed_origin,
   	   created_on = in_created_on
	WHERE   avs_address_id = in_avs_address_id;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_AVS_ADDRESS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_AVS_ADDRESS;

PROCEDURE UPDATE_APOLLO_OVERRIDE_FLAG
(
IN_AVS_ADDRESS_ID              IN CLEAN.AVS_ADDRESS.AVS_ADDRESS_ID%TYPE,
IN_APOLLO_OVERRIDE_FLAG        IN CLEAN.AVS_ADDRESS.APOLLO_OVERRIDE_FLAG%TYPE,
OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the
        apollo override flag on the avs_address table.

Input:
        IN_AVS_ADDRESS_ID      		     		 number
        IN_APOLLO_OVERRIDE_FLAG 	  		 varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

	UPDATE  avs_address
	SET
	   apollo_override_flag = in_apollo_override_flag
   	WHERE   avs_address_id = in_avs_address_id;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_APOLLO_OVERRIDE_FLAG [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_APOLLO_OVERRIDE_FLAG;

PROCEDURE UPDATE_PREMIER_CIRCLE_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
IN_PC_GROUP_ID                	IN ORDER_DETAILS.PC_GROUP_ID%TYPE,
IN_PC_MEMBERSHIP_ID             IN ORDER_DETAILS.PC_MEMBERSHIP_ID%TYPE,
IN_PC_FLAG                		IN ORDER_DETAILS.PC_FLAG%TYPE,
IN_UPDATED_BY                   IN ORDERS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating premier circle info
		in clean.order_details table for the given order_guid.

Input:
        ORDER_GUID                      varchar2
		PC_GROUP_ID                      varchar2
		PC_MEMBERSHIP_ID                      varchar2
		PC_FLAG                      varchar2
		UPDATED_BY                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

update ORDER_DETAILS O
set PC_GROUP_ID = IN_PC_GROUP_ID,
	PC_MEMBERSHIP_ID = IN_PC_MEMBERSHIP_ID,
	PC_FLAG = IN_PC_FLAG
WHERE   ORDER_GUID = IN_ORDER_GUID;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END UPDATE_PREMIER_CIRCLE_INFO;

PROCEDURE UPDATE_PHOENIX_ORDER
(
IN_ORDER_DETAIL_ID               IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_ID                    IN CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_PHOENIX_PRODUCT_ID            IN CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_SHIP_METHOD                   IN CLEAN.ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                     IN CLEAN.ORDER_DETAILS.SHIP_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

  update order_details
  set product_id = in_phoenix_product_id,
      ship_method = in_ship_method,
      ship_date = in_ship_date,
      florist_id = null,
      order_disp_code = 'Processed'
  where order_detail_id = in_order_detail_id;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_APOLLO_OVERRIDE_FLAG [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PHOENIX_ORDER;


PROCEDURE UPDATE_PHOENIX_ORDER
(
IN_ORDER_DETAIL_ID               IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_ID                    IN CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_PHOENIX_PRODUCT_ID            IN CLEAN.ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_SHIP_METHOD                   IN CLEAN.ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                     IN CLEAN.ORDER_DETAILS.SHIP_DATE%TYPE,
IN_DELIVERY_DATE				 IN CLEAN.ORDER_DETAILS.DELIVERY_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

  update order_details
  set product_id = in_phoenix_product_id,
      ship_method = in_ship_method,
      ship_date = in_ship_date,
      florist_id = null,
      order_disp_code = 'Processed',
      delivery_date = in_delivery_date
  where order_detail_id = in_order_detail_id;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PHOENIX_ORDER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PHOENIX_ORDER;

PROCEDURE INSERT_PHOENIX_ORDER_TRACKING
(
IN_ORDER_DETAIL_ID             IN CLEAN.PHOENIX_ORDER_TRACKING.ORDER_DETAIL_ID%TYPE,
IN_MERCURY_ID                  IN CLEAN.PHOENIX_ORDER_TRACKING.MERCURY_ID%TYPE,
IN_ORIGINAL_PRODUCT_ID         IN CLEAN.PHOENIX_ORDER_TRACKING.ORIGINAL_PRODUCT_ID%TYPE,
IN_NEW_PRODUCT_ID              IN CLEAN.PHOENIX_ORDER_TRACKING.NEW_PRODUCT_ID%TYPE,
IN_CREATED_BY                  IN CLEAN.PHOENIX_ORDER_TRACKING.CREATED_BY%TYPE,
IN_ORIG_BEAR_ADDON_ID          IN CLEAN.PHOENIX_ORDER_TRACKING.ORIG_BEAR_ADDON_ID%TYPE,
IN_NEW_BEAR_ADDON_ID           IN CLEAN.PHOENIX_ORDER_TRACKING.NEW_BEAR_ADDON_ID%TYPE,
IN_ORIG_CHOC_ADDON_ID          IN CLEAN.PHOENIX_ORDER_TRACKING.ORIG_CHOC_ADDON_ID%TYPE,
IN_NEW_CHOC_ADDON_ID           IN CLEAN.PHOENIX_ORDER_TRACKING.NEW_CHOC_ADDON_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting into clean.PHOENIX_ORDER_TRACKING table

Input:
      Order_Detail_Id,
      Mercury_Id,
      Original_Product_Id,
      New_Product_Id,
      Created_By,
      IN_ORIG_BEAR_ADDON_ID,
      IN_NEW_BEAR_ADDON_ID,
      IN_ORIG_CHOC_ADDON_ID,
      IN_NEW_CHOC_ADDON_ID

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

   Insert Into Clean.Phoenix_Order_Tracking
   (
      Phoenix_Order_Tracking_Id,
      Order_Detail_Id,
      Mercury_Id,
      Original_Product_Id,
      New_Product_Id,
      Created_On,
      Created_By,
      Updated_On,
      Updated_By,
      ORIG_BEAR_ADDON_ID,
      NEW_BEAR_ADDON_ID,
      ORIG_CHOC_ADDON_ID,
      NEW_CHOC_ADDON_ID
  )
  Values
  (
    PHOENIX_ORDER_TRACKING_ID_SQ.nextval,
    In_Order_Detail_Id,
    In_Mercury_Id,
    In_Original_Product_Id,
    In_New_Product_Id,
    Sysdate,
    In_Created_By,
    Sysdate,
    In_Created_By,
    IN_ORIG_BEAR_ADDON_ID,
    IN_NEW_BEAR_ADDON_ID,
    IN_ORIG_CHOC_ADDON_ID,
    IN_NEW_CHOC_ADDON_ID
  );

	OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_PHOENIX_ORDER_TRACKING [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_PHOENIX_ORDER_TRACKING;

PROCEDURE UPDATE_ORDER_ADD_ONS
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS.ORDER_DETAIL_ID%TYPE,
IN_ORIG_ADD_ON_CODE             IN ORDER_ADD_ONS.ADD_ON_CODE%TYPE,
IN_PHOENIX_ADD_ON_CODE          IN ORDER_ADD_ONS.ADD_ON_CODE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)

AS

BEGIN

UPDATE_ORDER_ADD_ONS
(
IN_ORDER_DETAIL_ID,
IN_ORIG_ADD_ON_CODE,
IN_PHOENIX_ADD_ON_CODE,
0,
0,
OUT_STATUS,
OUT_MESSAGE
);

END UPDATE_ORDER_ADD_ONS;

PROCEDURE UPDATE_ORDER_ADD_ONS
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS.ORDER_DETAIL_ID%TYPE,
IN_ORIG_ADD_ON_CODE             IN ORDER_ADD_ONS.ADD_ON_CODE%TYPE,
IN_PHOENIX_ADD_ON_CODE          IN ORDER_ADD_ONS.ADD_ON_CODE%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_ADD_ONS.ADD_ON_AMOUNT%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_ADD_ONS.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the clean.order_add_on table

Input:
      in_order_detail_id,
      in_orig_add_on_code
      in_phoenix_add_on_code

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN
   update clean.order_add_ons oao
   set    oao.add_on_code = IN_PHOENIX_ADD_ON_CODE,
          oao.add_on_history_id = (select ah.addon_history_id
                                   from ftd_apps.addon_history ah
                                   where ah.addon_id = IN_PHOENIX_ADD_ON_CODE
                                   and ah.status = 'Active'
                                   and ah.updated_on = (select MAX(ah2.updated_ON)from ftd_apps.addon_history ah2
                                   where ah2.addon_id = IN_PHOENIX_ADD_ON_CODE )),
          oao.add_on_amount = nvl (IN_ADD_ON_AMOUNT, 0),
          oao.add_on_discount_amount = nvl (IN_ADD_ON_DISCOUNT_AMOUNT, 0)
   where oao.order_detail_id = IN_ORDER_DETAIL_ID
   and oao.add_on_code = IN_ORIG_ADD_ON_CODE;

   OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDER_ADD_ONS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_ADD_ONS;

PROCEDURE UPDATE_PAYMENTS_STATUS
(
IN_BILL_STATUS     		IN PAYMENTS.BILL_STATUS%TYPE,
IN_UPDATED_BY           IN PAYMENTS.UPDATED_BY%TYPE,
IN_REFUND_ID       		IN PAYMENTS.REFUND_ID%TYPE,
OUT_STATUS              OUT VARCHAR2,
OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Update payment bill status.
-----------------------------------------------------------------------------*/
BEGIN

 UPDATE CLEAN.PAYMENTS SET BILL_STATUS = IN_BILL_STATUS,
      BILL_DATE = SYSDATE,
      UPDATED_ON = SYSDATE,
      UPDATED_BY = IN_UPDATED_BY
    WHERE REFUND_ID = IN_REFUND_ID;

 OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_PAYMENTS_STATUS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PAYMENTS_STATUS;

PROCEDURE DELETE_CARDINAL_COMM_VALUES
(
 IN_PAYMENT_ID  IN NUMBER,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This deletes all of the associated Cardinal Commerce values associated to an order
        in the payments_ext table

Input:
        payment_id - NUMBER

Output:
        status and message

-----------------------------------------------------------------------------*/


BEGIN

  DELETE FROM payments_ext
     WHERE payment_id = in_payment_id
       AND UPPER(auth_property_name) IN ('CAVV', 'ECI', 'XID', 'UCAF' );


  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DELETE_CARDINAL_COMM_VALUES;

END;
.
/
