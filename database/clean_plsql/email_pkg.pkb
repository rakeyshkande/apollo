CREATE OR REPLACE PACKAGE BODY "CLEAN"."EMAIL_PKG" AS

PROCEDURE SCHEDULED_FOR_DELIVERY
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

v_execute_flag         varchar2(100);
v_count                number := 0;
v_poc_id               number;
v_status               varchar2(4000);
v_message              varchar2(4000);
v_email_title          varchar2(1000);
v_email_subject        varchar2(1000);
v_email_body           clob;

CURSOR get_orders is
    select distinct od.order_detail_id,
        e.email_address,
        o.company_id,
        o.customer_id,
        o.order_guid,
        o.master_order_number,
        od.external_order_number,
        od.delivery_date,
        od.source_code
    from clean.order_details od
    join clean.orders o
    on o.order_guid = od.order_guid
    join clean.customer c
    on c.customer_id = od.recipient_id
    join clean.email e
    on e.email_id = o.email_id
    join ftd_apps.state_master sm
    on sm.state_master_id = c.state
    join pas.pas_timezone_dt pt
    on pt.time_zone_code = sm.time_zone
    and pt.delivery_date = trunc(sysdate)
    join frp.global_parms gp1
    on gp1.context = 'EMAIL_CONFIG'
    and gp1.name = 'SCHEDULED_FOR_DELIVERY_EMAIL_TIME'
    join frp.global_parms gp2
    on gp2.context = 'EMAIL_CONFIG'
    and gp2.name = 'SCHEDULED_FOR_DELIVERY_STOCK_EMAIL_ID'
    join mercury.mercury m
    on m.reference_number = to_char(od.order_detail_id)
    and m.msg_type = 'FTD'
    and m.delivery_date = pt.delivery_date
    and m.mercury_status in ('MC', 'MM', 'MN')
    and m.created_on < to_date(to_char(sysdate, 'yyyymmdd') || lpad(gp1.value + pt.delta_to_cst_hhmm, 4, '0'), 'yyyymmddhh24mi')
    and not exists (select 'Y'
        from mercury.mercury m2
        where m2.mercury_order_number = m.mercury_order_number
        and m2.created_on > m.created_on
        and m2.msg_type in ('CAN', 'REJ'))
    join clean.stock_email se
    on se.stock_email_id = gp2.value
    left outer join ptn_pi.partner_mapping pm
    on o.origin_id = pm.partner_id
    where 1=1
    and nvl(od.delivery_confirmation_status, 'N/A') != 'Confirmed'
    and (gp1.value + pt.delta_to_cst_hhmm) <= to_char(sysdate, 'hh24mi')
    and nvl(pm.send_delivery_conf_email, 'Y') = 'Y'
    and not exists (select 'Y'
        from clean.point_of_contact poc
        where poc.order_detail_id = od.order_detail_id
        and poc.letter_title = se.title)
    order by od.order_detail_id;

BEGIN

    select value
    into v_execute_flag
    from frp.global_parms
    where context = 'EMAIL_CONFIG'
    and name = 'SCHEDULED_FOR_DELIVERY_EMAIL_FLAG';
    
    if v_execute_flag = 'Y' then
    
        select title, subject, body
        into v_email_title, v_email_subject, v_email_body
        from clean.stock_email se
        where se.stock_email_id = (select value
            from frp.global_parms
            where context = 'EMAIL_CONFIG'
            and name = 'SCHEDULED_FOR_DELIVERY_STOCK_EMAIL_ID');
    
        FOR rec IN get_orders LOOP

            dbms_output.put_line(rec.order_detail_id);
            v_count := v_count + 1;
            
            clean.point_of_contact_pkg.insert_point_of_contact (
                IN_CUSTOMER_ID => rec.customer_id,
		IN_ORDER_GUID => rec.order_guid,
		IN_ORDER_DETAIL_ID => rec.order_detail_id,
		IN_MASTER_ORDER_NUMBER => rec.master_order_number,
		IN_EXTERNAL_ORDER_NUMBER => rec.external_order_number,
		IN_DELIVERY_DATE => rec.delivery_date,
		IN_COMPANY_ID => rec.company_id,
		IN_FIRST_NAME => null,
		IN_LAST_NAME => null,
		IN_DAYTIME_PHONE_NUMBER => null,
		IN_EVENING_PHONE_NUMBER => null,
		IN_SENDER_EMAIL_ADDRESS => null,
		IN_LETTER_TITLE => v_email_title,
		IN_EMAIL_SUBJECT => v_email_subject,
		IN_BODY => v_email_body,
		IN_COMMENT_TYPE => 'Order',
		IN_RECIPIENT_EMAIL_ADDRESS  => rec.email_address,
		IN_NEW_RECIP_FIRST_NAME => null,
		IN_NEW_RECIP_LAST_NAME => null,
		IN_NEW_ADDRESS => null,
		IN_NEW_CITY => null,
		IN_NEW_PHONE => null,
		IN_NEW_ZIP_CODE => null,
		IN_NEW_STATE => null,
		IN_NEW_COUNTRY => null,
		IN_NEW_INSTITUTION => null,
		IN_NEW_DELIVERY_DATE => null,
		IN_NEW_CARD_MESSAGE => null,
		IN_NEW_PRODUCT => null,
		IN_CHANGE_CODES => null,
		IN_SENT_RECEIVED_INDICATOR => 'O',
		IN_SEND_FLAG => null,
		IN_PRINT_FLAG => null,
		IN_POINT_OF_CONTACT_TYPE => 'Email',
		IN_COMMENT_TEXT => null,
		IN_EMAIL_HEADER => null,
		IN_CREATED_BY => 'SYS',
		IN_NEW_CARD_SIGNATURE => null,
		IN_SOURCE_CODE => rec.source_code,
		IN_MAILSERVER_CODE => null,
		IN_EMAIL_TYPE => 'MASSAGE_POC',
		IN_RECORD_ATTRIBUTES_XML => null,
                OUT_SEQUENCE_NUMBER => v_poc_id,
                OUT_STATUS => v_status,
                OUT_MESSAGE => v_message
            );

            clean.comment_history_pkg.INSERT_COMMENTS (
                IN_CUSTOMER_ID => rec.customer_id,
                IN_ORDER_GUID => rec.order_guid,
                IN_ORDER_DETAIL_ID => rec.order_detail_id,
                IN_COMMENT_ORIGIN => 'OrderProc',
                IN_REASON => null,
                IN_DNIS_ID => null,
                IN_COMMENT_TEXT => 'Automatic Scheduled For Delivery email (' || v_email_title || ') sent to ' || rec.email_address,
                IN_COMMENT_TYPE => 'Order',
                IN_CSR_ID => 'OrderProc',
                OUT_STATUS => v_status,
                OUT_MESSAGE => v_message
            );
            
            events.post_a_message_flex('OJMS.message_generator', NULL, v_poc_id, 5, v_status, v_message);

        END LOOP;

        dbms_application_info.set_client_info('Finished: ' || to_char(sysdate, 'hh24:mi:ss') || ' ' || v_count);

    else

        dbms_application_info.set_client_info('Not enabled: ' || to_char(sysdate, 'hh24:mi:ss'));

    end if;
    
    OUT_STATUS := 'Y';
    
    commit;
    
    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END SCHEDULED_FOR_DELIVERY;

END;
.
/