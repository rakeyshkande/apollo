CREATE OR REPLACE
PACKAGE BODY clean.PARTNERS_EOD_PKG
AS


PROCEDURE INSERT_AAFES_BILLING_HEADER
(
 IN_AAFES_BILLING_BATCH_DATE  IN AAFES_BILLING_HEADER.AAFES_BILLING_BATCH_DATE%TYPE,
 OUT_AAFES_BILLING_HEADER_ID OUT AAFES_BILLING_HEADER.AAFES_BILLING_HEADER_ID%TYPE,
 OUT_STATUS                  OUT VARCHAR2,
 OUT_ERROR_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure creates a record in the aafes_billing_header table.

Input:
        aafes_billing_batch_date DATE,

Output:
        aafes_billing_header_id  NUMBER
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  SELECT aafes_billing_header_id_sq.NEXTVAL
    INTO out_aafes_billing_header_id
    FROM DUAL;

  INSERT INTO aafes_billing_header
     (aafes_billing_header_id,
      created_on,
      aafes_billing_batch_date)
    VALUES
      (out_aafes_billing_header_id,
       SYSDATE,
       in_aafes_billing_batch_date);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_AAFES_BILLING_HEADER;


PROCEDURE UPDATE_AAFES_BILLING_HEADER
(
 IN_AAFES_BILLING_HEADER_ID   IN AAFES_BILLING_HEADER.AAFES_BILLING_HEADER_ID%TYPE,
 IN_PROCESSED_INDICATOR       IN AAFES_BILLING_HEADER.PROCESSED_INDICATOR%TYPE,
 OUT_STATUS                  OUT VARCHAR2,
 OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the processed_indicator in table
        aafes_billing_header for the aafes_billing_header_id passed in.

Input:
        aafes_billing_header_id  NUMBER
        processed_indicator      CHAR

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  UPDATE aafes_billing_header
     SET processed_indicator = in_processed_indicator
   WHERE aafes_billing_header_id = in_aafes_billing_header_id;

  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No aafes_billing_header records updated for aafes_billing_header_id ' || in_aafes_billing_header_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_AAFES_BILLING_HEADER;


PROCEDURE INSERT_AAFES_BILLING_DETAILS
(
 IN_AAFES_BILLING_HEADER_ID IN AAFES_BILLING_DETAILS.AAFES_BILLING_HEADER_ID%TYPE,
 IN_PAYMENT_ID              IN AAFES_BILLING_DETAILS.PAYMENT_ID%TYPE,
 IN_BILL_TYPE               IN AAFES_BILLING_DETAILS.BILL_TYPE%TYPE,
 IN_RETURN_CODE             IN AAFES_BILLING_DETAILS.RETURN_CODE%TYPE,
 IN_AUTH_CODE               IN AAFES_BILLING_DETAILS.AUTH_CODE%TYPE,
 IN_TICKET_NUMBER           IN AAFES_BILLING_DETAILS.TICKET_NUMBER%TYPE,
 IN_RETURN_MESSAGE          IN AAFES_BILLING_DETAILS.RETURN_MESSAGE%TYPE,
 IN_VERIFIED_FLAG           IN AAFES_BILLING_DETAILS.VERIFIED_FLAG%TYPE,
 IN_REQ_AMT		    IN AAFES_BILLING_DETAILS.REQ_AMT%TYPE,
 OUT_AAFES_BILLING_DETAILS_ID  OUT AAFES_BILLING_DETAILS.AAFES_BILLING_DETAILS_ID%TYPE,
 OUT_STATUS                OUT VARCHAR2,
 OUT_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure creates record in the aafes_billing_detail table.

Input:
         aafes_billing_detail_id NUMBER
         aafes_billing_header_id NUMBER
         payment_id              NUMBER
         bill_type               VARCHAR2
         return_code             CHAR
         auth_code               VARCHAR2
         ticket_number           VARCHAR2
         return_message          VARCHAR2
         verified_flag           CHAR
         req_amt		 NUMBER

Output:
        aafes_billing_details_id    NUMBER
        status                      varchar2 (Y or N)
        message                     varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO aafes_billing_details
  (
   aafes_billing_details_id,
   aafes_billing_header_id,
   payment_id,
   bill_type,
   return_code,
   auth_code,
   ticket_number,
   return_message,
   verified_flag,
   req_amt,
   created_on,
   created_by,
   updated_on,
   updated_by
  )
  VALUES
  (
   aafes_billing_detail_id_sq.NEXTVAL,
   in_aafes_billing_header_id,
   in_payment_id,
   in_bill_type,
   in_return_code,
   in_auth_code,
   in_ticket_number,
   substr(in_return_message,0,100),
   in_verified_flag,
   in_req_amt,
   SYSDATE,
   USER,
   SYSDATE,
   USER
  ) RETURN aafes_billing_details_id INTO out_aafes_billing_details_id;

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AAFES_BILLING_DETAILS;


PROCEDURE UPDATE_AAFES_BILLING_DETAILS
(
 IN_AAFES_BILLING_DETAILS_ID IN AAFES_BILLING_DETAILS.AAFES_BILLING_DETAILS_ID%TYPE,
 IN_RETURN_CODE              IN AAFES_BILLING_DETAILS.RETURN_CODE%TYPE,
 IN_RETURN_MESSAGE           IN AAFES_BILLING_DETAILS.RETURN_MESSAGE%TYPE,
 IN_VERIFIED_FLAG            IN AAFES_BILLING_DETAILS.VERIFIED_FLAG%TYPE,
 IN_TICKET_NUMBER	     IN AAFES_BILLING_DETAILS.TICKET_NUMBER%TYPE,
 IN_AUTH_CODE		     IN AAFES_BILLING_DETAILS.AUTH_CODE%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates a record in table aafes_billing_detail
          for the specified aafes_billing_details_id.

Input:
        aafes_billing_details_id NUMBER
        return_code              CHAR
        return_message           VARCHAR2
        verified_flag            CHAR
        ticket_number		 VARCHAR2
        auth_code		 VARCHAR2

Output:
        status                     varchar2 (Y or N)
        message                    varchar2 (error message)
-----------------------------------------------------------------------------*/

BEGIN

  UPDATE aafes_billing_details
     SET return_code             = in_return_code,
         return_message          = substr(in_return_message,0,100),
         verified_flag           = in_verified_flag,
         ticket_number		 = in_ticket_number,
         auth_code		 = in_auth_code,
         updated_on              = SYSDATE,
         updated_by              = USER
   WHERE aafes_billing_details_id = in_aafes_billing_details_id;


  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No aafes_billing_details records updated for aafes_billing_detail_id ' || in_aafes_billing_details_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_AAFES_BILLING_DETAILS;


PROCEDURE GET_AAFES_BILLING_RECORDS
(
 IN_DATE      IN DATE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
 /*-----------------------------------------------------------------------------
 Description:
    This procedure retrieves AAFES MS billing records that are
     eligible for end of day processing.

 Input:
         date  DATE

 Output:
         cursor  containing the payments records related to AAFES.

 -----------------------------------------------------------------------------*/

BEGIN

	GET_BILLING_BY_PAYMENT_TYPE(IN_DATE, 'MS', OUT_PAYMENT_CUR, OUT_REFUND_CUR);


END GET_AAFES_BILLING_RECORDS;


PROCEDURE GET_AMAZON_REFUND_RECORDS
(
 IN_DATE             IN DATE,
 OUT_REFUND_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   The procedure retrieves records from table refunds with whose orders
   were placed with Amazon.

Input:
        date               DATE

Output:
        cursor containing refund info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_refund_cursor FOR
          SELECT r.refund_id,
                 od.external_order_number,
                 r.refund_disp_code,
                 r.refund_product_amount,
                 r.refund_addon_amount,
                 r.refund_service_fee,
                 r.refund_shipping_fee,
                 r.refund_service_fee_tax,
                 r.refund_shipping_tax,
                 r.refund_discount_amount,
                 r.refund_commission_amount,
                 r.refund_wholesale_amount,
                 r.refund_tax
            FROM refund r,
                 order_details od,
                 orders o
           WHERE o.origin_id = 'AMZNI'
             AND od.order_guid = o.order_guid
             AND od.order_disp_code in ('Processed','Shipped','Printed')
             AND r.order_detail_id = od.order_detail_id
             AND r.refund_status = 'Unbilled'
             AND TRUNC(r.created_on) <= TRUNC(in_date)
             AND r.refund_status = 'Unbilled';

END GET_AMAZON_REFUND_RECORDS;


PROCEDURE INSERT_REFUND_PARTNER_EXT
(
 IN_REFUND_ID    IN REFUND_PARTNER_EXT.REFUND_ID%TYPE,
 IN_PARTNER_ID   IN REFUND_PARTNER_EXT.PARTNER_ID%TYPE,
 IN_EXT_TYPE     IN REFUND_PARTNER_EXT.EXT_TYPE%TYPE,
 IN_EXT_DATA     IN REFUND_PARTNER_EXT.EXT_DATA%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure creates a record in table refund_partner_ext.

Input:
         refund_id      NUMBER
         partner_id     VARCHAR2
         ext_type       VARCHAR2
         ext_data       VARCHAR2

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  INSERT INTO refund_partner_ext
  (
   refund_partner_ext_id,
   partner_id,
   refund_id,
   ext_type,
   ext_data,
   created_on,
   created_by,
   updated_on,
   updated_by
  )
  VALUES
  (
   refund_partner_ext_id_sequence.NEXTVAL,
   in_partner_id,
   in_refund_id,
   in_ext_type,
   in_ext_data,
   SYSDATE,
   USER,
   SYSDATE,
   USER
  );

  OUT_STATUS := 'Y';

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REFUND_PARTNER_EXT;


FUNCTION GET_PARTNER_REFUND_INFO
(
 IN_REFUND_ID    IN REFUND_PARTNER_EXT.REFUND_ID%TYPE,
 IN_PARTNER_ID   IN REFUND_PARTNER_EXT.PARTNER_ID%TYPE,
 IN_EXT_TYPE     IN REFUND_PARTNER_EXT.EXT_TYPE%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
   The procedure returns ext_data for the given refund_id,
   partner_id, and ext_type from table refund_partner_ext.

Input:
         refund_id      NUMBER
         partner_id     VARCHAR2
         field_name     VARCHAR2

Output:
         field_value    VARCHAR2
-----------------------------------------------------------------------------*/
v_ext_data refund_partner_ext.ext_data%TYPE;

BEGIN

  BEGIN
    SELECT ext_data
      INTO v_ext_data
      FROM refund_partner_ext
     WHERE refund_id = in_refund_id
       AND partner_id = in_partner_id
       AND ext_type = in_ext_type;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_ext_data := NULL;
  END;

  RETURN v_ext_data;

END GET_PARTNER_REFUND_INFO;


FUNCTION GET_PARTNER_REFUND_REASON
(
 IN_REFUND_DISP_CODE IN REFUND_DISP_PARTNER_REF.REFUND_DISP_CODE%TYPE,
 IN_PARTNER_ID       IN PARTNER_REFUND_REASON.PARTNER_ID%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
   The procedure returns from table partner_refund_reason description
   for the given FTD refund disposition and the partner id.

Input:
         refund_disp_code  NUMBER
         partner_id        NUMBER

Output:
         description    VARCHAR2
-----------------------------------------------------------------------------*/
v_description partner_refund_reason.description%TYPE;

BEGIN

  BEGIN
    SELECT prr.description
      INTO v_description
      FROM refund_disp_partner_ref rdpf,
           partner_refund_reason prr
     WHERE rdpf.refund_disp_code = in_refund_disp_code
       AND rdpf.partner_refund_reason_id = prr.partner_refund_reason_id
       AND prr.partner_id = in_partner_id;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_description := NULL;
  END;

  RETURN v_description;

END GET_PARTNER_REFUND_REASON;


PROCEDURE UPDATE_AMAZON_ORDER_BILLS
(
 IN_BILL_DATE    IN ORDER_BILLS.BILL_DATE%TYPE,
 IN_BILL_STATUS  IN ORDER_BILLS.BILL_STATUS%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure updates all the Amazon related order bills for
    the bill date and status passed in.

Input:
         bill_date      DATE
         bill_status    VARCHAR2

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

    UPDATE order_bills ob
    SET ob.bill_status = in_bill_status,
        ob.bill_date = sysdate
    WHERE ob.bill_status = 'Unbilled'
    AND trunc(ob.created_on) <= in_bill_date
    AND EXISTS (SELECT 1
        FROM order_bills ob2,
            orders o,
            order_details od
        WHERE ob2.order_bill_id = ob.order_bill_id
        AND ob2.order_detail_id = od.order_detail_id
        AND od.order_guid = o.order_guid
        AND od.order_disp_code in ('Processed', 'Shipped', 'Printed')
        AND o.origin_id = 'AMZNI');

  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No order_bills records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_AMAZON_ORDER_BILLS;



PROCEDURE UPDATE_AMAZON_REFUNDS
(
 IN_BILL_DATE    IN ORDER_BILLS.BILL_DATE%TYPE,
 IN_BILL_STATUS  IN ORDER_BILLS.BILL_STATUS%TYPE,
 OUT_STATUS     OUT VARCHAR2,
 OUT_MESSAGE    OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure updates all the Amazon related refund for
    the bill date and status passed in.

Input:
         bill_date      DATE
         bill_status    VARCHAR2

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE refund r
     SET r.refund_status = in_bill_status,
         r.refund_date = in_bill_date,
         r.updated_on = sysdate,
         r.updated_by = 'SYS'
   WHERE r.refund_status = 'Unbilled'
   AND   EXISTS (SELECT 1
                   FROM refund r2,
                        orders o,
                        order_details od
                  WHERE r2.refund_id = r.refund_id
                    AND r2.order_detail_id = od.order_detail_id
                    AND od.order_guid = o.order_guid
                    AND o.origin_id = 'AMZNI');


  IF SQL%FOUND THEN
       out_status := 'Y';
  ELSE
       out_status := 'N';
       out_message := 'WARNING: No refund records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_AMAZON_REFUNDS;



PROCEDURE UPDATE_AMAZON_ORDER_BILL_STAT
(
 IN_EXTERNAL_ORDER_NUMBER IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
 IN_BILL_STATUS         IN ORDER_BILLS.BILL_STATUS%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates order_bills.bill_status to the status
         passed in for the external order number passed in.

Input:
        external_order_number VARCHAR2
        bill_status           VARCHAR2

Output:
        status          varchar2 (Y or N)
        message         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE order_bills
     SET bill_status = in_bill_status,
         updated_on = SYSDATE,
         updated_by = USER
   WHERE order_detail_id IN (SELECT order_detail_id
                                FROM order_details
                               WHERE external_order_number = in_external_order_number);

  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No order_bills records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;


END UPDATE_AMAZON_ORDER_BILL_STAT;

PROCEDURE GET_BILLING_BY_PAYMENT_TYPE
(
 IN_DATE      IN DATE,
 IN_PAYMENT_TYPE        IN PAYMENTS.PAYMENT_TYPE%TYPE,
 OUT_PAYMENT_CUR        OUT TYPES.REF_CURSOR,
 OUT_REFUND_CUR		OUT TYPES.REF_CURSOR
)
AS
 /*-----------------------------------------------------------------------------
 Description:
    This procedure retrieves AAFES MS billing records that are
     eligible for end of day processing.

 Input:
         date  DATE

 Output:
         cursor  containing the payments records related to AAFES.

 -----------------------------------------------------------------------------*/

 BEGIN

  OPEN out_payment_cur FOR
        SELECT DISTINCT o.master_order_number,
             		p.payment_id,
             		'P' payment_indicator,
             		p.created_on
		FROM    clean.payments p
		JOIN    clean.orders o
		ON      p.order_guid=o.order_guid
		JOIN    clean.order_details od
		ON      o.order_guid = od.order_guid
		AND     od.order_disp_code IN ('Processed','Shipped','Printed')
		AND     p.bill_status = 'Unbilled'
		WHERE 	1=1
		AND	(p.updated_on < TRUNC(in_date) + 1 and p.updated_on > TRUNC(in_date) - 180)
		AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))                           
		AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007')) 		
		AND     p.credit_amount > 0
		AND     p.payment_indicator='P'		
		AND     p.payment_type = IN_PAYMENT_TYPE
		ORDER BY o.master_order_number, p.created_on;

  OPEN out_refund_cur FOR
	SELECT DISTINCT o.master_order_number,
	                p.payment_id,
	                'R' payment_indicator,
	                p.created_on
		FROM 	clean.payments p
		JOIN	clean.orders o
		ON	o.order_guid=p.order_guid
		JOIN    clean.refund r
		ON      p.refund_id=r.refund_id
		JOIN    clean.order_details od
		ON      od.order_detail_id=r.order_detail_id
		AND 	od.order_disp_code IN ('Processed','Shipped','Printed')
		WHERE 1=1
		  AND (r.updated_on < TRUNC(in_date) + 1 and r.updated_on > TRUNC(in_date) - 180)
		  AND NOT EXISTS (
			select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
			and od2.order_disp_code in ('Validated', 'Held'))                            
		  AND NOT EXISTS (
			select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
			and sod.status in ('2001','2002','2003','2005','2007'))		  
	          AND r.refund_status = 'Unbilled'
	          AND p.debit_amount > 0
	          AND p.payment_type = IN_PAYMENT_TYPE
	          AND p.payment_indicator = 'R'
	          ORDER BY o.master_order_number, p.created_on;


END GET_BILLING_BY_PAYMENT_TYPE;

PROCEDURE GET_AAFES_AUTH_INFO
(
 IN_PAYMENT_ID        IN PAYMENTS.PAYMENT_ID%TYPE,
 OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
 /*-----------------------------------------------------------------------------
 Description:
    Retrieves all aafes ticket numbers associated with the shopping cart
    from all payments.

 Input:
         PAYMENT_ID  VARCHAR2 (PAYMENT_ID for the refund record)

 Output:
         cursor  containing aafes_ticket_number and auth_number

 -----------------------------------------------------------------------------*/

 BEGIN

  OPEN out_cur FOR
        SELECT DISTINCT p.auth_number, p.aafes_ticket_number, p.created_on
          FROM clean.payments p
         WHERE p.payment_indicator = 'P'
           AND aafes_ticket_number IS NOT NULL
           AND p.order_guid = (SELECT order_guid from clean.payments pp
           			WHERE pp.payment_id = in_payment_id
           		      )
      ORDER BY p.created_on;


END GET_AAFES_AUTH_INFO;

END;
.
/
