 
CREATE OR REPLACE TRIGGER clean.pmt_before_stmt_trigger 
BEFORE UPDATE ON clean.payments 
BEGIN 
   payments_trigger_pkg.before_stmt_trigger; 
END; 
/