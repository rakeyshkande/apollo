CREATE OR REPLACE
PACKAGE BODY clean.STOCK_EMAIL_LETTER_PKG
AS

PROCEDURE VIEW_EMAIL_TITLES
(
IN_AUTO_RESPONSE_INDICATOR      IN STOCK_EMAIL.AUTO_RESPONSE_INDICATOR%TYPE,
IN_ORIGIN_ID                    IN STOCK_EMAIL_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_EMAIL_COMPANY_REF.PARTNER_NAME%TYPE,
IN_ALL_TITLES_INDICATOR         IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns stock_email_id and title for all rows from the
        CLEAN.STOCK_EMAIL table.

Input:
        auto_response_indicator         varchar2 (null, Y, N)
        origin_id                       varhcar2 (optional)
        partner_name                    varhcar2 (optional)
        all_titles_indicator            varchar2 (optional Y/N) - if Y return all title records, no origin needs to be specified

Output:
        cursor containing stock email ids and titles

-----------------------------------------------------------------------------*/
BEGIN

IF in_all_titles_indicator = 'Y' THEN

        OPEN out_cur FOR
                SELECT  e.stock_email_id,
                        decode(secr.partner_name,NULL,e.title,e.title || ' - ' || secr.partner_name) title,
                        e.subject
                FROM    stock_email e,
                        stock_email_company_ref secr
                WHERE   (e.auto_response_indicator = in_auto_response_indicator
                OR       in_auto_response_indicator is null)
                  AND   e.stock_email_id = secr.stock_email_id
                ORDER BY title;

ELSE
        OPEN out_cur FOR
                SELECT  e.stock_email_id,
                        e.title,
                        e.subject
                FROM    stock_email e
                WHERE   (e.auto_response_indicator = in_auto_response_indicator
                OR       in_auto_response_indicator is null)
                AND     EXISTS
                (
                        select  1
                        from    stock_email_company_ref r
                        where   r.stock_email_id = e.stock_email_id
                        and     (r.origin_id = in_origin_id
                        or       (in_origin_id is null and r.origin_id is null) )
                        and     (r.partner_name = in_partner_name
                        or       (in_partner_name is null and r.partner_name is null) )
                )
                ORDER BY title;
END IF;

END VIEW_EMAIL_TITLES;


PROCEDURE VIEW_EMAIL_CONTENT
(
IN_STOCK_EMAIL_ID               IN STOCK_EMAIL.STOCK_EMAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns subject and body from the CLEAN.STOCK_EMAIL
        table for the specific stock_email_id.

Input:
        stock_email_id                  number

Output:
        cursor containing stock email subject and body

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  e.subject,
                e.title,
                e.body,
                e.auto_response_indicator,
                e.stock_email_type_id,
                (select min (origin_id) from stock_email_company_ref r where r.stock_email_id = e.stock_email_id) origin_id,
                (select min (partner_name) from stock_email_company_ref r where r.stock_email_id = e.stock_email_id) partner_name
        FROM    stock_email e
        WHERE   e.stock_email_id = in_stock_email_id;

END VIEW_EMAIL_CONTENT;


PROCEDURE VIEW_STOCK_EMAIL
(
IN_STOCK_EMAIL_ID               IN STOCK_EMAIL.STOCK_EMAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns the stock email based on the id.

Input:
        stock_email_id                  number

Output:
        cursor containing stock email

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  e.title,
                e.subject,
                e.body,
                e.use_no_reply_flag
        FROM    stock_email e
        WHERE   e.stock_email_id = in_stock_email_id;

END VIEW_STOCK_EMAIL;


PROCEDURE VIEW_STOCK_EMAIL
(
IN_TITLE                        IN STOCK_EMAIL.TITLE%TYPE,
IN_ORIGIN_ID                    IN STOCK_EMAIL_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_EMAIL_COMPANY_REF.PARTNER_NAME%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns the stock email based on the title and origin

Input:
        title                   varchar2
        origin                  varchar2

Output:
        cursor containing stock email

-----------------------------------------------------------------------------*/
CURSOR id_cur IS
        SELECT  e.stock_email_id
        FROM    stock_email e
        JOIN    stock_email_company_ref r
        ON      e.stock_email_id = r.stock_email_id
        WHERE   e.title = in_title
        AND     (r.origin_id = in_origin_id and in_origin_id is not null
        OR       r.company_id = 'ALL' and in_origin_id is null)
        AND     (r.partner_name = in_partner_name and in_partner_name is not null
        OR       r.company_id = 'ALL' and in_partner_name is null);

v_stock_email_id        stock_email.stock_email_id%type;

BEGIN

OPEN id_cur;
FETCH id_cur INTO v_stock_email_id;
CLOSE id_cur;

VIEW_STOCK_EMAIL (v_stock_email_id, out_cur);

END VIEW_STOCK_EMAIL;


PROCEDURE VIEW_LETTER_TITLES
(
IN_ORIGIN_ID                    IN STOCK_LETTER_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_LETTER_COMPANY_REF.PARTNER_NAME%TYPE,
IN_ALL_TITLES_INDICATOR         IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns stock_letter_id and title for all rows from the
        CLEAN.STOCK_LETTER table.

Input:
        origin id                       varchar2 (optional)
        all_titles_indicator            varchar2 (optional Y/N) - if Y return all title records, no origin needs to be specified

Output:
        cursor containing stock letter ids and titles

-----------------------------------------------------------------------------*/
BEGIN

IF in_all_titles_indicator = 'Y' THEN

        OPEN out_cur FOR
                SELECT  l.stock_letter_id,
                        decode(slcr.partner_name,NULL,l.title,l.title || ' - ' || slcr.partner_name) title
                FROM    stock_letter l,
                        stock_letter_company_ref slcr
                WHERE   l.stock_letter_id = slcr.stock_letter_id
                ORDER BY title;

ELSE
        OPEN out_cur FOR
                SELECT  l.stock_letter_id,
                        l.title
                FROM    stock_letter l
                WHERE   EXISTS
                (
                        select  1
                        from    stock_letter_company_ref r
                        where   r.stock_letter_id = l.stock_letter_id
                        and     (r.origin_id = in_origin_id
                        or       (in_origin_id is null and r.origin_id is null) )
                        and     (r.partner_name = in_partner_name
                        or       (in_partner_name is null and r.partner_name is null) )
                )
                ORDER BY title;
END IF;

END VIEW_LETTER_TITLES;


PROCEDURE VIEW_LETTER_CONTENT
(
IN_STOCK_LETTER_ID              IN STOCK_LETTER.STOCK_LETTER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns title and body from the CLEAN.STOCK_LETTER
        table for the specific stock_letter_id.

fInput:
        stock_letter_id                 number

Output:
        cursor containing stock letter body

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  l.title,
                l.body,
                (select min (origin_id) from stock_letter_company_ref r where r.stock_letter_id = l.stock_letter_id) origin_id,
                (select min (partner_name) from stock_letter_company_ref r where r.stock_letter_id = l.stock_letter_id) partner_name
        FROM    stock_letter l
        WHERE   l.stock_letter_id = in_stock_letter_id;

END VIEW_LETTER_CONTENT;


PROCEDURE VIEW_STOCK_LETTER
(
IN_STOCK_LETTER_ID              IN STOCK_LETTER.STOCK_LETTER_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns the stock email based on the id.

Input:
        stock_letter_id                 number

Output:
        cursor containing stock letter

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  l.title,
                l.body
        FROM    stock_letter l
        JOIN    stock_letter_company_ref r
        ON      l.stock_letter_id = r.stock_letter_id
        WHERE   r.stock_letter_id = in_stock_letter_id;

END VIEW_STOCK_LETTER;


PROCEDURE VIEW_STOCK_LETTER
(
IN_TITLE                        IN STOCK_LETTER.TITLE%TYPE,
IN_ORIGIN_ID                    IN STOCK_LETTER_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_LETTER_COMPANY_REF.PARTNER_NAME%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns the stock email based on the title and origin

Input:
        title                           varchar2
        origin                          varchar2

Output:
        cursor containing stock letter

-----------------------------------------------------------------------------*/
CURSOR id_cur IS
        SELECT  l.stock_letter_id
        FROM    stock_letter l
        JOIN    stock_letter_company_ref r
        ON      l.stock_letter_id = r.stock_letter_id
        WHERE   l.title = in_title
        AND     (r.origin_id = in_origin_id and in_origin_id is not null
        OR       r.company_id = 'ALL' and in_origin_id is null)
        AND     (r.partner_name = in_origin_id and in_partner_name is not null
        OR       r.company_id = 'ALL' and in_partner_name is null);

v_stock_letter_id               stock_letter.stock_letter_id%type;

BEGIN

OPEN id_cur;
FETCH id_cur INTO v_stock_letter_id;
CLOSE id_cur;

VIEW_STOCK_LETTER (v_stock_letter_id, out_cur);

END VIEW_STOCK_LETTER;



PROCEDURE VIEW_MESSAGE_TOKENS
(
IN_COMPANY_ID                   IN MESSAGE_TOKENS.COMPANY_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all message tokens

Input:
        none

Output:
        cursor containing all columns from message_tokens

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                token_id,
                company_id,
                token_type,
                token_value,
                default_value
        FROM    message_tokens
        WHERE   company_id = in_company_id
        OR      in_company_id IS NULL;

END VIEW_MESSAGE_TOKENS;


PROCEDURE INSERT_STOCK_EMAIL
(
IN_TITLE                        IN STOCK_EMAIL.TITLE%TYPE,
IN_BODY                         IN STOCK_EMAIL.BODY%TYPE,
IN_SUBJECT                      IN STOCK_EMAIL.SUBJECT%TYPE,
IN_CREATED_BY                   IN STOCK_EMAIL.CREATED_BY%TYPE,
IN_STOCK_EMAIL_TYPE_ID          IN STOCK_EMAIL.STOCK_EMAIL_TYPE_ID%TYPE,
IN_AUTO_RESPONSE_INDICATOR      IN STOCK_EMAIL.AUTO_RESPONSE_INDICATOR%TYPE,
IN_ORIGIN_ID                    IN STOCK_EMAIL_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_EMAIL_COMPANY_REF.PARTNER_NAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts a new stock email into the CLEAN.STOCK_EMAIL table.
        Stock_email_id will be auto-generated within the stored procedure.

Input:
        stock_email_id                  number
        title                           varchar2
        body                            clob
        subject                         varchar2
        created_by                      varchar2
        auto_response_indicator         char
        partner_name                    varchar2
        stock_email_type_id             varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_stock_email_id        number;
PARTNER_ORIGIN          varchar2(200);
PTN_SOURCE_CODE         varchar2(200);
PTN_COMPANY_ID   	      varchar2(200);

BEGIN

    BEGIN
      SELECT FTD_ORIGIN_MAPPING, DEFAULT_SOURCE_CODE, SM.COMPANY_ID INTO PARTNER_ORIGIN, PTN_SOURCE_CODE, PTN_COMPANY_ID
      FROM PTN_PI.PARTNER_MAPPING PM JOIN FTD_APPS.SOURCE_MASTER SM ON PM.DEFAULT_SOURCE_CODE = SM.SOURCE_CODE
      WHERE FTD_ORIGIN_MAPPING = IN_ORIGIN_ID;

      EXCEPTION WHEN  NO_DATA_FOUND THEN 
        BEGIN
            SELECT FTD_ORIGIN_MAPPING, DEF_SRC_CODE, SM.COMPANY_ID INTO PARTNER_ORIGIN, PTN_SOURCE_CODE, PTN_COMPANY_ID
             FROM PTN_MERCENT.MRCNT_CHANNEL_MAPPING PM JOIN FTD_APPS.SOURCE_MASTER SM ON PM.DEF_SRC_CODE = SM.SOURCE_CODE
             WHERE FTD_ORIGIN_MAPPING = IN_ORIGIN_ID;
           
              EXCEPTION WHEN  NO_DATA_FOUND THEN
              PARTNER_ORIGIN := NULL;
              PTN_SOURCE_CODE := NULL;
              PTN_COMPANY_ID := NULL;
          END;
      END;

INSERT INTO stock_email
(
        stock_email_id,
        title,
        body,
        subject,
        created_on,
        created_by,
        updated_on,
        updated_by,
        auto_response_indicator,
        use_no_reply_flag,
        stock_email_type_id
)
VALUES
(
        stock_email_id_sq.nextval,
        in_title,
        in_body,
        in_subject,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_auto_response_indicator,
        'N',
        in_stock_email_type_id
) RETURNING stock_email_id INTO v_stock_email_id;

IF in_origin_id IS NOT NULL THEN 

      IF PARTNER_ORIGIN IS NOT NULL THEN
          INSERT INTO STOCK_EMAIL_COMPANY_REF (STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID)
          VALUES (v_stock_email_id, PTN_COMPANY_ID, PARTNER_ORIGIN);
      ELSE
        INSERT INTO stock_email_company_ref
        (
                stock_email_id,
                company_id,
                origin_id
        )
        SELECT
                v_stock_email_id,
                company_id,
                origin
        FROM    ftd_apps.company_origin_mapping
        WHERE   origin = in_origin_id;
      END IF;

ELSE
    IF in_partner_name IS NOT NULL THEN
        INSERT INTO stock_email_company_ref
        (
                stock_email_id,
                company_id,
                partner_name
        )
        VALUES
        (
                v_stock_email_id,
                'ALL',
                in_partner_name
        );

    ELSE
        INSERT INTO stock_email_company_ref
        (
                stock_email_id,
                company_id
        )
        VALUES
        (
                v_stock_email_id,
                'ALL'
        );

    END IF;
END IF;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_STOCK_EMAIL;



PROCEDURE UPDATE_STOCK_EMAIL
(
IN_STOCK_EMAIL_ID               IN STOCK_EMAIL.STOCK_EMAIL_ID%TYPE,
IN_TITLE                        IN STOCK_EMAIL.TITLE%TYPE,
IN_BODY                         IN STOCK_EMAIL.BODY%TYPE,
IN_SUBJECT                      IN STOCK_EMAIL.SUBJECT%TYPE,
IN_UPDATED_BY                   IN STOCK_EMAIL.UPDATED_BY%TYPE,
IN_STOCK_EMAIL_TYPE_ID          IN STOCK_EMAIL.STOCK_EMAIL_TYPE_ID%TYPE,
IN_AUTO_RESPONSE_INDICATOR      IN STOCK_EMAIL.AUTO_RESPONSE_INDICATOR%TYPE,
IN_ORIGIN_ID                    IN STOCK_EMAIL_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_EMAIL_COMPANY_REF.PARTNER_NAME%TYPE,
IN_UPDATE_ORIGIN_INDICATOR      IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

PARTNER_ORIGIN      varchar2(200); 
PTN_SOURCE_CODE     varchar2(200);
PTN_COMPANY_ID   	varchar2(200);

/*-----------------------------------------------------------------------------
Description:
        This procedure updates the record in the CLEAN.STOCK_EMAIL table for the email data
        passed in based off the stock_email_id.

Input:
        stock_email_id                  number
        title                           varchar2
        body                            clob
        subject                         varchar2
        updated_by                      varchar2
        auto_response_indicator         char

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

	BEGIN
	  SELECT FTD_ORIGIN_MAPPING, DEFAULT_SOURCE_CODE, SM.COMPANY_ID INTO PARTNER_ORIGIN, PTN_SOURCE_CODE, PTN_COMPANY_ID
	  FROM PTN_PI.PARTNER_MAPPING PM JOIN FTD_APPS.SOURCE_MASTER SM ON PM.DEFAULT_SOURCE_CODE = SM.SOURCE_CODE
	  WHERE FTD_ORIGIN_MAPPING = IN_ORIGIN_ID;

	  EXCEPTION WHEN  NO_DATA_FOUND THEN
	  	BEGIN
            SELECT FTD_ORIGIN_MAPPING, DEF_SRC_CODE, SM.COMPANY_ID INTO PARTNER_ORIGIN, PTN_SOURCE_CODE, PTN_COMPANY_ID
            FROM PTN_MERCENT.MRCNT_CHANNEL_MAPPING PM JOIN FTD_APPS.SOURCE_MASTER SM ON PM.DEF_SRC_CODE = SM.SOURCE_CODE
            WHERE FTD_ORIGIN_MAPPING = IN_ORIGIN_ID;
        
              EXCEPTION WHEN  NO_DATA_FOUND THEN
              PARTNER_ORIGIN := NULL;
              PTN_SOURCE_CODE := NULL;
              PTN_COMPANY_ID := NULL;
        END;
    END;

UPDATE  stock_email
SET
        title = NVL(in_title, title),
        body = NVL(in_body, body),
        subject = NVL(in_subject, subject),
        updated_on = sysdate,
        updated_by = in_updated_by,
        auto_response_indicator = NVL(in_auto_response_indicator, auto_response_indicator),
        stock_email_type_id = nvl(in_stock_email_type_id, stock_email_type_id)
WHERE   stock_email_id = in_stock_email_id;

IF in_update_origin_indicator = 'Y' THEN
        DELETE FROM stock_email_company_ref
        WHERE   stock_email_id = in_stock_email_id;

        IF in_origin_id IS NOT NULL THEN
        
        	IF PARTNER_ORIGIN IS NOT NULL THEN
             	INSERT INTO STOCK_EMAIL_COMPANY_REF (STOCK_EMAIL_ID, COMPANY_ID, ORIGIN_ID) VALUES (IN_STOCK_EMAIL_ID, PTN_COMPANY_ID, PARTNER_ORIGIN);
        	ELSE
                INSERT INTO stock_email_company_ref
                (
                        stock_email_id,
                        company_id,
                        origin_id
                )
                SELECT
                        in_stock_email_id,
                        company_id,
                        origin
                FROM    ftd_apps.company_origin_mapping
                WHERE   origin = in_origin_id;
            END IF;

        ELSE
            IF in_partner_name IS NOT NULL THEN

                    INSERT INTO stock_email_company_ref
                    (
                            stock_email_id,
                            company_id,
                            partner_name
                    )
                    VALUES
                    (
                            in_stock_email_id,
                            'ALL',
                            in_partner_name
                    );
    
            ELSE
                    INSERT INTO stock_email_company_ref
                    (
                            stock_email_id,
                            company_id
                    )
                    VALUES
                    (
                            in_stock_email_id,
                            'ALL'
                    );
            END IF;
        END IF;
END IF;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_STOCK_EMAIL;


PROCEDURE INSERT_STOCK_LETTER
(
IN_TITLE                        IN STOCK_LETTER.TITLE%TYPE,
IN_BODY                         IN STOCK_LETTER.BODY%TYPE,
IN_CREATED_BY                   IN STOCK_LETTER.CREATED_BY%TYPE,
IN_ORIGIN_ID                    IN STOCK_EMAIL_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_EMAIL_COMPANY_REF.PARTNER_NAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure inserts a new stock letter into the CLEAN.STOCK_LETTER table.
        Stock_letter_id will be auto-generated within the stored procedure.


Input:
        title                           varchar2
        body                            clob
        created_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_stock_letter_id               number;

BEGIN

INSERT INTO stock_letter
(
        stock_letter_id,
        title,
        body,
        created_on,
        created_by,
        updated_on,
        updated_by
)
VALUES
(
        stock_letter_id_sq.nextval,
        in_title,
        in_body,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by
) RETURNING stock_letter_id INTO v_stock_letter_id;

IF in_origin_id IS NOT NULL THEN
        INSERT INTO stock_letter_company_ref
        (
                stock_letter_id,
                company_id,
                origin_id
        )
        SELECT
                v_stock_letter_id,
                company_id,
                origin
        FROM    ftd_apps.company_origin_mapping
        WHERE   origin = in_origin_id;

ELSE
    IF in_partner_name IS NOT NULL THEN
        INSERT INTO stock_letter_company_ref
        (
                stock_letter_id,
                company_id,
                partner_name
        )
        VALUES
        (
                v_stock_letter_id,
                'ALL',
                in_partner_name
        );

    ELSE
        INSERT INTO stock_letter_company_ref
        (
                stock_letter_id,
                company_id
        )
        VALUES
        (
                v_stock_letter_id,
                'ALL'
        );

    END IF;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_STOCK_LETTER;


PROCEDURE UPDATE_STOCK_LETTER
(
IN_STOCK_LETTER_ID              IN STOCK_LETTER.STOCK_LETTER_ID%TYPE,
IN_TITLE                        IN STOCK_LETTER.TITLE%TYPE,
IN_BODY                         IN STOCK_LETTER.BODY%TYPE,
IN_UPDATED_BY                   IN STOCK_LETTER.UPDATED_BY%TYPE,
IN_ORIGIN_ID                    IN STOCK_LETTER_COMPANY_REF.ORIGIN_ID%TYPE,
IN_PARTNER_NAME                 IN STOCK_LETTER_COMPANY_REF.PARTNER_NAME%TYPE,
IN_UPDATE_ORIGIN_INDICATOR      IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the record in the CLEAN.STOCK_LETTER table for the email
        data passed in based off the stock_letter_id.

Input:
        stock_letter_id                 number
        title                           varchar2
        body                            clob
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  stock_letter
SET
        title = NVL(in_title, title),
        body = NVL(in_body, body),
        updated_on = sysdate,
        updated_by = in_updated_by
WHERE   stock_letter_id = in_stock_letter_id;

IF in_update_origin_indicator = 'Y' THEN
        DELETE FROM stock_letter_company_ref
        WHERE   stock_letter_id = in_stock_letter_id;

        IF in_origin_id IS NOT NULL THEN
                INSERT INTO stock_letter_company_ref
                (
                        stock_letter_id,
                        company_id,
                        origin_id
                )
                SELECT
                        in_stock_letter_id,
                        company_id,
                        origin
                FROM    ftd_apps.company_origin_mapping
                WHERE   origin = in_origin_id;

        ELSE
                IF in_partner_name IS NOT NULL THEN
                    INSERT INTO stock_letter_company_ref
                    (
                            stock_letter_id,
                            company_id,
                            partner_name
                    )
                    VALUES
                    (
                            in_stock_letter_id,
                            'ALL',
                            in_partner_name
                    );
            
                ELSE
                    INSERT INTO stock_letter_company_ref
                    (
                            stock_letter_id,
                            company_id
                    )
                    VALUES
                    (
                            in_stock_letter_id,
                            'ALL'
                    );
            
                END IF;
        END IF;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_STOCK_LETTER;

PROCEDURE GET_SENDER_EMAIL 
(
IN_COMPANY_ID        IN FTD_APPS.EMAIL_COMPANY_DATA.COMPANY_ID%TYPE,
IN_TITLE             IN CLEAN.STOCK_EMAIL.TITLE%TYPE,
IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_SENDER_EMAIL     OUT FTD_APPS.EMAIL_COMPANY_DATA.VALUE%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure gets the correct company return address based on 
		company_id and the stock_email in question. 

Input:
	IN_COMPANY_ID        IN FTD_APPS.EMAIL_COMPANY_DATA.COMPANY_ID%TYPE,
	IN_TITLE             IN CLEAN.STOCK_EMAIL.TITLE%TYPE,
	IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,

Output:
	OUT_SENDER_EMAIL     OUT FTD_APPS.EMAIL_COMPANY_DATA.VALUE%TYPE

-----------------------------------------------------------------------------*/
v_use_no_reply_flag        stock_email.use_no_reply_flag%type;
V_OUT_SENDER_EMAIL	   varchar2(4000);
V_partner_sender_email	   varchar2(4000);

CURSOR id_cur IS
        SELECT use_no_reply_flag
        FROM clean.stock_email
        WHERE title = IN_TITLE;
BEGIN

v_use_no_reply_flag := 'N';

OPEN id_cur;
FETCH id_cur INTO v_use_no_reply_flag;
CLOSE id_cur;

IF v_use_no_reply_flag is null THEN 
        v_use_no_reply_flag := 'N';
END IF;

IF v_use_no_reply_flag = 'Y' THEN
        SELECT value 
        INTO V_OUT_SENDER_EMAIL
        FROM FTD_APPS.EMAIL_COMPANY_DATA
        WHERE name = 'NoReplyFromAddress'
        AND COMPANY_ID = COALESCE ((SELECT DISTINCT COMPANY_ID
                                 FROM FTD_APPS.EMAIL_COMPANY_DATA
                                 WHERE COMPANY_ID = IN_COMPANY_ID), 'FTD');
ELSE
        -- If not a no-reply, then check for preferred partner from address
        -- If not a preferred partner, then use the company specific from
        BEGIN
           select pm.reply_email_address
           into v_partner_sender_email
           from ftd_apps.source_program_ref spr,
                clean.order_details od,
                ftd_apps.partner_program pp,
                ftd_apps.partner_master pm
           where 1=1
             and od.order_detail_id = IN_ORDER_DETAIL_ID
             and od.source_code = spr.source_code
             and pp.program_name = spr.program_name
             and pp.partner_name = pm.partner_name
             and pm.preferred_partner_flag = 'Y';
  
            V_OUT_SENDER_EMAIL := v_partner_sender_email;          

        EXCEPTION WHEN NO_DATA_FOUND THEN
            SELECT value
            INTO V_OUT_SENDER_EMAIL
            FROM FTD_APPS.EMAIL_COMPANY_DATA ecd
            WHERE name = 'FromAddress'
            AND COMPANY_ID = COALESCE ((SELECT DISTINCT COMPANY_ID
                                     FROM FTD_APPS.EMAIL_COMPANY_DATA
                                     WHERE COMPANY_ID = IN_COMPANY_ID), 'FTD');

        END;
END IF;

OUT_SENDER_EMAIL := V_OUT_SENDER_EMAIL;

END GET_SENDER_EMAIL;


PROCEDURE GET_SENDER_EMAIL_PARTNER
(
IN_COMPANY_ID        IN FTD_APPS.EMAIL_COMPANY_DATA.COMPANY_ID%TYPE,
IN_TITLE             IN CLEAN.STOCK_EMAIL.TITLE%TYPE,
IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_PARTNER_NAME      IN FTD_APPS.PARTNER_MASTER.PARTNER_NAME%TYPE,
OUT_SENDER_EMAIL     OUT FTD_APPS.EMAIL_COMPANY_DATA.VALUE%TYPE
)
AS
/*-----------------------------------------------------------------------------
Description: This procedure gets the correct company return address based on 
		company_id and the stock_email in question. 

Input:
	IN_COMPANY_ID        IN FTD_APPS.EMAIL_COMPANY_DATA.COMPANY_ID%TYPE,
	IN_TITLE             IN CLEAN.STOCK_EMAIL.TITLE%TYPE,
	IN_ORDER_DETAIL_ID   IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
	IN_PARTNER_NAME      IN FTD_APPS.PARTNER_MASTER.PARTNER_NAME%TYPE,

Output:
	OUT_SENDER_EMAIL     OUT FTD_APPS.EMAIL_COMPANY_DATA.VALUE%TYPE

-----------------------------------------------------------------------------*/
v_use_no_reply_flag        stock_email.use_no_reply_flag%type;
V_OUT_SENDER_EMAIL	   varchar2(4000);
V_partner_sender_email	   varchar2(4000);

CURSOR id_cur IS
        SELECT use_no_reply_flag
        FROM clean.stock_email
        WHERE title = IN_TITLE;
BEGIN

v_use_no_reply_flag := 'N';

OPEN id_cur;
FETCH id_cur INTO v_use_no_reply_flag;
CLOSE id_cur;

IF v_use_no_reply_flag is null THEN 
        v_use_no_reply_flag := 'N';
END IF;

IF v_use_no_reply_flag = 'Y' THEN
        SELECT value 
        INTO V_OUT_SENDER_EMAIL
        FROM FTD_APPS.EMAIL_COMPANY_DATA
        WHERE name = 'NoReplyFromAddress'
        AND COMPANY_ID = COALESCE ((SELECT DISTINCT COMPANY_ID
                                 FROM FTD_APPS.EMAIL_COMPANY_DATA
                                 WHERE COMPANY_ID = IN_COMPANY_ID), 'FTD');
ELSE
        -- If not a no-reply, then check for preferred partner from address
        -- If not a preferred partner, then use the company specific from
        BEGIN
            BEGIN
                select pm.reply_email_address
                into v_partner_sender_email
                from ftd_apps.partner_master pm
                where 1=1
                  and pm.partner_name = IN_PARTNER_NAME
                  and pm.preferred_partner_flag = 'Y';
   
                 V_OUT_SENDER_EMAIL := v_partner_sender_email;          
            EXCEPTION WHEN NO_DATA_FOUND THEN
                select pm.reply_email_address
                into v_partner_sender_email
                from ftd_apps.source_program_ref spr,
                     clean.order_details od,
                     ftd_apps.partner_program pp,
                     ftd_apps.partner_master pm
                where 1=1
                  and od.order_detail_id = IN_ORDER_DETAIL_ID
                  and od.source_code = spr.source_code
                  and pp.program_name = spr.program_name
                  and pp.partner_name = pm.partner_name
                  and pm.preferred_partner_flag = 'Y';
   
                 V_OUT_SENDER_EMAIL := v_partner_sender_email;          
            END;

        EXCEPTION WHEN NO_DATA_FOUND THEN
            SELECT value
            INTO V_OUT_SENDER_EMAIL
            FROM FTD_APPS.EMAIL_COMPANY_DATA ecd
            WHERE name = 'FromAddress'
            AND COMPANY_ID = COALESCE ((SELECT DISTINCT COMPANY_ID
                                     FROM FTD_APPS.EMAIL_COMPANY_DATA
                                     WHERE COMPANY_ID = IN_COMPANY_ID), 'FTD');

        END;
END IF;

OUT_SENDER_EMAIL := V_OUT_SENDER_EMAIL;

END GET_SENDER_EMAIL_PARTNER;

FUNCTION IS_DCON_EMAIL 
(
IN_STOCK_EMAIL_ID 	IN 	STOCK_EMAIL.STOCK_EMAIL_ID%TYPE
)
RETURN VARCHAR2

IS
/*-----------------------------------------------------------------------------
Description:
        This function determines if the stock email passed in has a stocke
        email type id = 'DCON'

Input:
        title - title of the email to check

Output:
        'Y' if stock email type id = 'DCON' else 'N'.  Throw all other errors back to
        the application

-----------------------------------------------------------------------------*/
v_flag CHAR (1);

BEGIN

  BEGIN
       SELECT 'Y' INTO v_flag
              FROM clean.stock_email se
              WHERE se.STOCK_EMAIL_ID = IN_STOCK_EMAIL_ID
              AND se.STOCK_EMAIL_TYPE_ID = 'DCON';

  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_flag := 'N';
  END;

  RETURN v_flag;
END IS_DCON_EMAIL ;

END;



.
/
