
create or replace PROCEDURE  clean.sku_and_comm_acctg_rpts_dflt 
  (OUT_STATUS             OUT   VARCHAR2,
   OUT_MESSAGE            OUT   VARCHAR2)
AS
----------------------------------------------------------------------------------
-- Proc    : Monthly merchandising reports interface
--
-- Descr   : Produce and email 2 reports:
--   *  SKU's delivered showing Merchandise amount by date range (monthly accounting)
--   *  ME Data pull for commission
--
-- NOTE - This script is an interface to the actual report proc for the convenience
--        of the Quartz scheduler.  The scheduler cannot accommodate passing NULLS
--        for input paramters to the actual report script.
----------------------------------------------------------------------------------


--##############################################################################
--## M A I N    L O G I C
--##############################################################################

begin

   out_status := 'Y';
   out_message := NULL;
   
   --
   -- Pass NULL for begin_date and end_date, which will default to the 1st and
   -- last day of the prior month.
   --
   clean.sku_and_commission_acctg_rpts(NULL, NULL, out_status, out_message);

exception
   when others then
      out_status := 'N';
      out_message := 'Unexpected Error - msg=' || sqlerrm;

end sku_and_comm_acctg_rpts_dflt;
/