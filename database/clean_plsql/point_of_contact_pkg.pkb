CREATE OR REPLACE
PACKAGE BODY clean.POINT_OF_CONTACT_PKG
AS

PROCEDURE INSERT_POINT_OF_CONTACT
(
IN_CUSTOMER_ID                  IN POINT_OF_CONTACT.CUSTOMER_ID%TYPE,
IN_ORDER_GUID                   IN POINT_OF_CONTACT.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN POINT_OF_CONTACT.ORDER_DETAIL_ID%TYPE,
IN_MASTER_ORDER_NUMBER          IN POINT_OF_CONTACT.MASTER_ORDER_NUMBER%TYPE,
IN_EXTERNAL_ORDER_NUMBER        IN POINT_OF_CONTACT.EXTERNAL_ORDER_NUMBER%TYPE,
IN_DELIVERY_DATE                IN POINT_OF_CONTACT.DELIVERY_DATE%TYPE,
IN_COMPANY_ID                   IN POINT_OF_CONTACT.COMPANY_ID%TYPE,
IN_FIRST_NAME                   IN POINT_OF_CONTACT.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN POINT_OF_CONTACT.LAST_NAME%TYPE,
IN_DAYTIME_PHONE_NUMBER         IN POINT_OF_CONTACT.DAYTIME_PHONE_NUMBER%TYPE,
IN_EVENING_PHONE_NUMBER         IN POINT_OF_CONTACT.EVENING_PHONE_NUMBER%TYPE,
IN_SENDER_EMAIL_ADDRESS         IN POINT_OF_CONTACT.SENDER_EMAIL_ADDRESS%TYPE,
IN_LETTER_TITLE                 IN POINT_OF_CONTACT.LETTER_TITLE%TYPE,
IN_EMAIL_SUBJECT                IN POINT_OF_CONTACT.EMAIL_SUBJECT%TYPE,
IN_BODY                         IN POINT_OF_CONTACT.BODY%TYPE,
IN_COMMENT_TYPE                 IN POINT_OF_CONTACT.COMMENT_TYPE%TYPE,
IN_RECIPIENT_EMAIL_ADDRESS      IN POINT_OF_CONTACT.RECIPIENT_EMAIL_ADDRESS%TYPE,
IN_NEW_RECIP_FIRST_NAME         IN POINT_OF_CONTACT.NEW_RECIP_FIRST_NAME%TYPE,
IN_NEW_RECIP_LAST_NAME          IN POINT_OF_CONTACT.NEW_RECIP_LAST_NAME%TYPE,
IN_NEW_ADDRESS                  IN POINT_OF_CONTACT.NEW_ADDRESS%TYPE,
IN_NEW_CITY                     IN POINT_OF_CONTACT.NEW_CITY%TYPE,
IN_NEW_PHONE                    IN POINT_OF_CONTACT.NEW_PHONE%TYPE,
IN_NEW_ZIP_CODE                 IN POINT_OF_CONTACT.NEW_ZIP_CODE%TYPE,
IN_NEW_STATE                    IN POINT_OF_CONTACT.NEW_STATE%TYPE,
IN_NEW_COUNTRY                  IN POINT_OF_CONTACT.NEW_COUNTRY%TYPE,
IN_NEW_INSTITUTION              IN POINT_OF_CONTACT.NEW_INSTITUTION%TYPE,
IN_NEW_DELIVERY_DATE            IN POINT_OF_CONTACT.NEW_DELIVERY_DATE%TYPE,
IN_NEW_CARD_MESSAGE             IN POINT_OF_CONTACT.NEW_CARD_MESSAGE%TYPE,
IN_NEW_PRODUCT                  IN POINT_OF_CONTACT.NEW_PRODUCT%TYPE,
IN_CHANGE_CODES                 IN POINT_OF_CONTACT.CHANGE_CODES%TYPE,
IN_SENT_RECEIVED_INDICATOR      IN POINT_OF_CONTACT.SENT_RECEIVED_INDICATOR%TYPE,
IN_SEND_FLAG                    IN POINT_OF_CONTACT.SEND_FLAG%TYPE,
IN_PRINT_FLAG                   IN POINT_OF_CONTACT.PRINT_FLAG%TYPE,
IN_POINT_OF_CONTACT_TYPE        IN POINT_OF_CONTACT.POINT_OF_CONTACT_TYPE%TYPE,
IN_COMMENT_TEXT                 IN POINT_OF_CONTACT.COMMENT_TEXT%TYPE,
IN_EMAIL_HEADER                 IN POINT_OF_CONTACT.EMAIL_HEADER%TYPE,
IN_CREATED_BY                   IN POINT_OF_CONTACT.CREATED_BY%TYPE,
IN_NEW_CARD_SIGNATURE           IN POINT_OF_CONTACT.NEW_CARD_SIGNATURE%TYPE,
OUT_SEQUENCE_NUMBER             OUT POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting point of contact info.
        It calls an overloaded version of INSERT_POINT_OF_CONTACT passing
        a value of null for IN_SOURCE_CODE and IN_MAILSERVER_CODE.
        
-----------------------------------------------------------------------------*/

BEGIN

-- call the overloaded insert_point_of_contact method
INSERT_POINT_OF_CONTACT
(
    IN_CUSTOMER_ID=>IN_CUSTOMER_ID,
    IN_ORDER_GUID=>IN_ORDER_GUID,
    IN_ORDER_DETAIL_ID=>IN_ORDER_DETAIL_ID,
    IN_MASTER_ORDER_NUMBER=>IN_MASTER_ORDER_NUMBER,
    IN_EXTERNAL_ORDER_NUMBER=>IN_EXTERNAL_ORDER_NUMBER,
    IN_DELIVERY_DATE=>IN_DELIVERY_DATE,
    IN_COMPANY_ID=>IN_COMPANY_ID,
    IN_FIRST_NAME=>IN_FIRST_NAME,
    IN_LAST_NAME=>IN_LAST_NAME,
    IN_DAYTIME_PHONE_NUMBER=>IN_DAYTIME_PHONE_NUMBER,
    IN_EVENING_PHONE_NUMBER=>IN_EVENING_PHONE_NUMBER,
    IN_SENDER_EMAIL_ADDRESS=>IN_SENDER_EMAIL_ADDRESS,
    IN_LETTER_TITLE=>IN_LETTER_TITLE,
    IN_EMAIL_SUBJECT=>IN_EMAIL_SUBJECT,
    IN_BODY=>IN_BODY,
    IN_COMMENT_TYPE=>IN_COMMENT_TYPE,
    IN_RECIPIENT_EMAIL_ADDRESS=>IN_RECIPIENT_EMAIL_ADDRESS,
    IN_NEW_RECIP_FIRST_NAME=>IN_NEW_RECIP_FIRST_NAME,
    IN_NEW_RECIP_LAST_NAME=>IN_NEW_RECIP_LAST_NAME,
    IN_NEW_ADDRESS=>IN_NEW_ADDRESS,
    IN_NEW_CITY=>IN_NEW_CITY,
    IN_NEW_PHONE=>IN_NEW_PHONE,
    IN_NEW_ZIP_CODE=>IN_NEW_ZIP_CODE,
    IN_NEW_STATE=>IN_NEW_STATE,
    IN_NEW_COUNTRY=>IN_NEW_COUNTRY,
    IN_NEW_INSTITUTION=>IN_NEW_INSTITUTION,
    IN_NEW_DELIVERY_DATE=>IN_NEW_DELIVERY_DATE,
    IN_NEW_CARD_MESSAGE=>IN_NEW_CARD_MESSAGE,
    IN_NEW_PRODUCT=>IN_NEW_PRODUCT,
    IN_CHANGE_CODES=>IN_CHANGE_CODES,
    IN_SENT_RECEIVED_INDICATOR=>IN_SENT_RECEIVED_INDICATOR,
    IN_SEND_FLAG=>IN_SEND_FLAG,
    IN_PRINT_FLAG=>IN_PRINT_FLAG,
    IN_POINT_OF_CONTACT_TYPE=>IN_POINT_OF_CONTACT_TYPE,
    IN_COMMENT_TEXT=>IN_COMMENT_TEXT,
    IN_EMAIL_HEADER=>IN_EMAIL_HEADER,
    IN_CREATED_BY=>IN_CREATED_BY,
    IN_NEW_CARD_SIGNATURE=>IN_NEW_CARD_SIGNATURE,
    IN_SOURCE_CODE=>NULL,
    IN_MAILSERVER_CODE=>NULL,
    IN_EMAIL_TYPE=>NULL,
    IN_RECORD_ATTRIBUTES_XML=>NULL,
    OUT_SEQUENCE_NUMBER=>OUT_SEQUENCE_NUMBER,
    OUT_STATUS=>OUT_STATUS,
    OUT_MESSAGE=>OUT_MESSAGE
);

END INSERT_POINT_OF_CONTACT;


PROCEDURE INSERT_POINT_OF_CONTACT
(
IN_CUSTOMER_ID                  IN POINT_OF_CONTACT.CUSTOMER_ID%TYPE,
IN_ORDER_GUID                   IN POINT_OF_CONTACT.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN POINT_OF_CONTACT.ORDER_DETAIL_ID%TYPE,
IN_MASTER_ORDER_NUMBER          IN POINT_OF_CONTACT.MASTER_ORDER_NUMBER%TYPE,
IN_EXTERNAL_ORDER_NUMBER        IN POINT_OF_CONTACT.EXTERNAL_ORDER_NUMBER%TYPE,
IN_DELIVERY_DATE                IN POINT_OF_CONTACT.DELIVERY_DATE%TYPE,
IN_COMPANY_ID                   IN POINT_OF_CONTACT.COMPANY_ID%TYPE,
IN_FIRST_NAME                   IN POINT_OF_CONTACT.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN POINT_OF_CONTACT.LAST_NAME%TYPE,
IN_DAYTIME_PHONE_NUMBER         IN POINT_OF_CONTACT.DAYTIME_PHONE_NUMBER%TYPE,
IN_EVENING_PHONE_NUMBER         IN POINT_OF_CONTACT.EVENING_PHONE_NUMBER%TYPE,
IN_SENDER_EMAIL_ADDRESS         IN POINT_OF_CONTACT.SENDER_EMAIL_ADDRESS%TYPE,
IN_LETTER_TITLE                 IN POINT_OF_CONTACT.LETTER_TITLE%TYPE,
IN_EMAIL_SUBJECT                IN POINT_OF_CONTACT.EMAIL_SUBJECT%TYPE,
IN_BODY                         IN POINT_OF_CONTACT.BODY%TYPE,
IN_COMMENT_TYPE                 IN POINT_OF_CONTACT.COMMENT_TYPE%TYPE,
IN_RECIPIENT_EMAIL_ADDRESS      IN POINT_OF_CONTACT.RECIPIENT_EMAIL_ADDRESS%TYPE,
IN_NEW_RECIP_FIRST_NAME         IN POINT_OF_CONTACT.NEW_RECIP_FIRST_NAME%TYPE,
IN_NEW_RECIP_LAST_NAME          IN POINT_OF_CONTACT.NEW_RECIP_LAST_NAME%TYPE,
IN_NEW_ADDRESS                  IN POINT_OF_CONTACT.NEW_ADDRESS%TYPE,
IN_NEW_CITY                     IN POINT_OF_CONTACT.NEW_CITY%TYPE,
IN_NEW_PHONE                    IN POINT_OF_CONTACT.NEW_PHONE%TYPE,
IN_NEW_ZIP_CODE                 IN POINT_OF_CONTACT.NEW_ZIP_CODE%TYPE,
IN_NEW_STATE                    IN POINT_OF_CONTACT.NEW_STATE%TYPE,
IN_NEW_COUNTRY                  IN POINT_OF_CONTACT.NEW_COUNTRY%TYPE,
IN_NEW_INSTITUTION              IN POINT_OF_CONTACT.NEW_INSTITUTION%TYPE,
IN_NEW_DELIVERY_DATE            IN POINT_OF_CONTACT.NEW_DELIVERY_DATE%TYPE,
IN_NEW_CARD_MESSAGE             IN POINT_OF_CONTACT.NEW_CARD_MESSAGE%TYPE,
IN_NEW_PRODUCT                  IN POINT_OF_CONTACT.NEW_PRODUCT%TYPE,
IN_CHANGE_CODES                 IN POINT_OF_CONTACT.CHANGE_CODES%TYPE,
IN_SENT_RECEIVED_INDICATOR      IN POINT_OF_CONTACT.SENT_RECEIVED_INDICATOR%TYPE,
IN_SEND_FLAG                    IN POINT_OF_CONTACT.SEND_FLAG%TYPE,
IN_PRINT_FLAG                   IN POINT_OF_CONTACT.PRINT_FLAG%TYPE,
IN_POINT_OF_CONTACT_TYPE        IN POINT_OF_CONTACT.POINT_OF_CONTACT_TYPE%TYPE,
IN_COMMENT_TEXT                 IN POINT_OF_CONTACT.COMMENT_TEXT%TYPE,
IN_EMAIL_HEADER                 IN POINT_OF_CONTACT.EMAIL_HEADER%TYPE,
IN_CREATED_BY                   IN POINT_OF_CONTACT.CREATED_BY%TYPE,
IN_NEW_CARD_SIGNATURE           IN POINT_OF_CONTACT.NEW_CARD_SIGNATURE%TYPE,
IN_SOURCE_CODE                  IN POINT_OF_CONTACT.SOURCE_CODE%TYPE,
OUT_SEQUENCE_NUMBER             OUT POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting point of contact info.
        It calls an overloaded version of INSERT_POINT_OF_CONTACT passing
        a value of null for IN_MAILSERVER_CODE.

-----------------------------------------------------------------------------*/

BEGIN

-- call the overloaded insert_point_of_contact method
INSERT_POINT_OF_CONTACT
(
    IN_CUSTOMER_ID=>IN_CUSTOMER_ID,
    IN_ORDER_GUID=>IN_ORDER_GUID,
    IN_ORDER_DETAIL_ID=>IN_ORDER_DETAIL_ID,
    IN_MASTER_ORDER_NUMBER=>IN_MASTER_ORDER_NUMBER,
    IN_EXTERNAL_ORDER_NUMBER=>IN_EXTERNAL_ORDER_NUMBER,
    IN_DELIVERY_DATE=>IN_DELIVERY_DATE,
    IN_COMPANY_ID=>IN_COMPANY_ID,
    IN_FIRST_NAME=>IN_FIRST_NAME,
    IN_LAST_NAME=>IN_LAST_NAME,
    IN_DAYTIME_PHONE_NUMBER=>IN_DAYTIME_PHONE_NUMBER,
    IN_EVENING_PHONE_NUMBER=>IN_EVENING_PHONE_NUMBER,
    IN_SENDER_EMAIL_ADDRESS=>IN_SENDER_EMAIL_ADDRESS,
    IN_LETTER_TITLE=>IN_LETTER_TITLE,
    IN_EMAIL_SUBJECT=>IN_EMAIL_SUBJECT,
    IN_BODY=>IN_BODY,
    IN_COMMENT_TYPE=>IN_COMMENT_TYPE,
    IN_RECIPIENT_EMAIL_ADDRESS=>IN_RECIPIENT_EMAIL_ADDRESS,
    IN_NEW_RECIP_FIRST_NAME=>IN_NEW_RECIP_FIRST_NAME,
    IN_NEW_RECIP_LAST_NAME=>IN_NEW_RECIP_LAST_NAME,
    IN_NEW_ADDRESS=>IN_NEW_ADDRESS,
    IN_NEW_CITY=>IN_NEW_CITY,
    IN_NEW_PHONE=>IN_NEW_PHONE,
    IN_NEW_ZIP_CODE=>IN_NEW_ZIP_CODE,
    IN_NEW_STATE=>IN_NEW_STATE,
    IN_NEW_COUNTRY=>IN_NEW_COUNTRY,
    IN_NEW_INSTITUTION=>IN_NEW_INSTITUTION,
    IN_NEW_DELIVERY_DATE=>IN_NEW_DELIVERY_DATE,
    IN_NEW_CARD_MESSAGE=>IN_NEW_CARD_MESSAGE,
    IN_NEW_PRODUCT=>IN_NEW_PRODUCT,
    IN_CHANGE_CODES=>IN_CHANGE_CODES,
    IN_SENT_RECEIVED_INDICATOR=>IN_SENT_RECEIVED_INDICATOR,
    IN_SEND_FLAG=>IN_SEND_FLAG,
    IN_PRINT_FLAG=>IN_PRINT_FLAG,
    IN_POINT_OF_CONTACT_TYPE=>IN_POINT_OF_CONTACT_TYPE,
    IN_COMMENT_TEXT=>IN_COMMENT_TEXT,
    IN_EMAIL_HEADER=>IN_EMAIL_HEADER,
    IN_CREATED_BY=>IN_CREATED_BY,
    IN_NEW_CARD_SIGNATURE=>IN_NEW_CARD_SIGNATURE,
    IN_SOURCE_CODE=>IN_SOURCE_CODE,
    IN_MAILSERVER_CODE=>NULL,
    IN_EMAIL_TYPE=>NULL,
    IN_RECORD_ATTRIBUTES_XML=>NULL,
    OUT_SEQUENCE_NUMBER=>OUT_SEQUENCE_NUMBER,
    OUT_STATUS=>OUT_STATUS,
    OUT_MESSAGE=>OUT_MESSAGE
);

END INSERT_POINT_OF_CONTACT;


PROCEDURE INSERT_POINT_OF_CONTACT
(
IN_CUSTOMER_ID                  IN POINT_OF_CONTACT.CUSTOMER_ID%TYPE,
IN_ORDER_GUID                   IN POINT_OF_CONTACT.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN POINT_OF_CONTACT.ORDER_DETAIL_ID%TYPE,
IN_MASTER_ORDER_NUMBER          IN POINT_OF_CONTACT.MASTER_ORDER_NUMBER%TYPE,
IN_EXTERNAL_ORDER_NUMBER        IN POINT_OF_CONTACT.EXTERNAL_ORDER_NUMBER%TYPE,
IN_DELIVERY_DATE                IN POINT_OF_CONTACT.DELIVERY_DATE%TYPE,
IN_COMPANY_ID                   IN POINT_OF_CONTACT.COMPANY_ID%TYPE,
IN_FIRST_NAME                   IN POINT_OF_CONTACT.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN POINT_OF_CONTACT.LAST_NAME%TYPE,
IN_DAYTIME_PHONE_NUMBER         IN POINT_OF_CONTACT.DAYTIME_PHONE_NUMBER%TYPE,
IN_EVENING_PHONE_NUMBER         IN POINT_OF_CONTACT.EVENING_PHONE_NUMBER%TYPE,
IN_SENDER_EMAIL_ADDRESS         IN POINT_OF_CONTACT.SENDER_EMAIL_ADDRESS%TYPE,
IN_LETTER_TITLE                 IN POINT_OF_CONTACT.LETTER_TITLE%TYPE,
IN_EMAIL_SUBJECT                IN POINT_OF_CONTACT.EMAIL_SUBJECT%TYPE,
IN_BODY                         IN POINT_OF_CONTACT.BODY%TYPE,
IN_COMMENT_TYPE                 IN POINT_OF_CONTACT.COMMENT_TYPE%TYPE,
IN_RECIPIENT_EMAIL_ADDRESS      IN POINT_OF_CONTACT.RECIPIENT_EMAIL_ADDRESS%TYPE,
IN_NEW_RECIP_FIRST_NAME         IN POINT_OF_CONTACT.NEW_RECIP_FIRST_NAME%TYPE,
IN_NEW_RECIP_LAST_NAME          IN POINT_OF_CONTACT.NEW_RECIP_LAST_NAME%TYPE,
IN_NEW_ADDRESS                  IN POINT_OF_CONTACT.NEW_ADDRESS%TYPE,
IN_NEW_CITY                     IN POINT_OF_CONTACT.NEW_CITY%TYPE,
IN_NEW_PHONE                    IN POINT_OF_CONTACT.NEW_PHONE%TYPE,
IN_NEW_ZIP_CODE                 IN POINT_OF_CONTACT.NEW_ZIP_CODE%TYPE,
IN_NEW_STATE                    IN POINT_OF_CONTACT.NEW_STATE%TYPE,
IN_NEW_COUNTRY                  IN POINT_OF_CONTACT.NEW_COUNTRY%TYPE,
IN_NEW_INSTITUTION              IN POINT_OF_CONTACT.NEW_INSTITUTION%TYPE,
IN_NEW_DELIVERY_DATE            IN POINT_OF_CONTACT.NEW_DELIVERY_DATE%TYPE,
IN_NEW_CARD_MESSAGE             IN POINT_OF_CONTACT.NEW_CARD_MESSAGE%TYPE,
IN_NEW_PRODUCT                  IN POINT_OF_CONTACT.NEW_PRODUCT%TYPE,
IN_CHANGE_CODES                 IN POINT_OF_CONTACT.CHANGE_CODES%TYPE,
IN_SENT_RECEIVED_INDICATOR      IN POINT_OF_CONTACT.SENT_RECEIVED_INDICATOR%TYPE,
IN_SEND_FLAG                    IN POINT_OF_CONTACT.SEND_FLAG%TYPE,
IN_PRINT_FLAG                   IN POINT_OF_CONTACT.PRINT_FLAG%TYPE,
IN_POINT_OF_CONTACT_TYPE        IN POINT_OF_CONTACT.POINT_OF_CONTACT_TYPE%TYPE,
IN_COMMENT_TEXT                 IN POINT_OF_CONTACT.COMMENT_TEXT%TYPE,
IN_EMAIL_HEADER                 IN POINT_OF_CONTACT.EMAIL_HEADER%TYPE,
IN_CREATED_BY                   IN POINT_OF_CONTACT.CREATED_BY%TYPE,
IN_NEW_CARD_SIGNATURE           IN POINT_OF_CONTACT.NEW_CARD_SIGNATURE%TYPE,
IN_SOURCE_CODE                  IN POINT_OF_CONTACT.SOURCE_CODE%TYPE,
IN_MAILSERVER_CODE              IN POINT_OF_CONTACT.MAILSERVER_CODE%TYPE,
IN_EMAIL_TYPE                   IN POINT_OF_CONTACT.EMAIL_TYPE%TYPE,
IN_RECORD_ATTRIBUTES_XML        IN POINT_OF_CONTACT.RECORD_ATTRIBUTES_XML%TYPE,
OUT_SEQUENCE_NUMBER             OUT POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting point of contact info

Input:
        customer_id                     number
        order_guid                      varchar2
        order_detail_id                 number
        master_order_number             varchar2
        external_order_number           varchar2
        delivery_date                   date
        company_id                      varchar2
        first_name                      varchar2
        last_name                       varchar2
        daytime_phone_number            varchar2
        evening_phone_number            varchar2
        sender_email_address            varchar2
        letter_title                    varchar2
        email_subject                   varchar2
        body                            clob
        comment_type                    varchar2
        recipient_email_address         varchar2
        new_recip_first_name            varchar2
        new_recip_last_name             varchar2
        new_address                     varchar2
        new_city                        varchar2
        new_phone                       varchar2
        new_zip_code                    varchar2
        new_state                       varchar2
        new_country                     varchar2
        new_institution                 varchar2
        new_delivery_date               date
        new_card_message                varchar2
        new_product                     number
        change_codes                    varchar2
        sent_received_indicator         char
        send_flag                       char
        print_flag                      char
        created_by                      varchar2
        point_of_contact_type           varchar2
        comment_text                    clob
        email_header                    clob
        created_by                      varchar2
        new_card_signature              varchar2
        source_code                     varchar2
        mailserver_code                 varchar2

Output:
        point of contact id             number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

CURSOR cust_cur IS
        SELECT  c.first_name cust_first_name,
                c.last_name cust_last_name,
                cpd.phone_number cust_day_phone,
                cpe.phone_number cust_evening_phone,
                r.first_name recip_first_name,
                r.last_name recip_last_name,
                r.address_1 recip_address,
                r.city recip_city,
                r.state recip_state,
                r.zip_code recip_zip_code,
                r.country recip_country,
                coalesce (rpd.phone_number, rpe.phone_number) recip_phone,
                od.delivery_date
        FROM    order_details od
        JOIN    customer r
        ON      od.recipient_id = r.customer_id
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    customer c
        ON      o.customer_id = c.customer_id
        LEFT OUTER JOIN customer_phones cpd
        ON      c.customer_id = cpd.customer_id
        AND     cpd.phone_type = 'Day'
        LEFT OUTER JOIN customer_phones cpe
        ON      c.customer_id = cpe.customer_id
        AND     cpe.phone_type = 'Evening'
        LEFT OUTER JOIN customer_phones rpd
        ON      od.recipient_id = rpd.customer_id
        AND     rpd.phone_type = 'Day'
        LEFT OUTER JOIN customer_phones rpe
        ON      od.recipient_id = rpe.customer_id
        AND     rpe.phone_type = 'Evening'
        WHERE   od.order_detail_id = in_order_detail_id;

CURSOR source_cur IS
        SELECT  company_id
        FROM    ftd_apps.source_master
        WHERE   source_code = in_source_code;

v_cust_first_name       customer.first_name%type;
v_cust_last_name        customer.last_name%type;
v_cust_day_phone        customer_phones.phone_number%type;
v_cust_evening_phone    customer_phones.phone_number%type;
v_recip_first_name      customer.first_name%type;
v_recip_last_name       customer.last_name%type;
v_recip_address         customer.address_1%type;
v_recip_city            customer.city%type;
v_recip_state           customer.state%type;
v_recip_zip_code        customer.zip_code%type;
v_recip_country         customer.country%type;
v_recip_phone           customer_phones.phone_number%type;
v_delivery_date         order_details.delivery_date%type;
v_company_id            point_of_contact.company_id%type;

BEGIN

IF in_order_detail_id IS NOT NULL  THEN
        OPEN cust_cur;
        FETCH cust_cur INTO
                v_cust_first_name,
                v_cust_last_name,
                v_cust_day_phone,
                v_cust_evening_phone,
                v_recip_first_name,
                v_recip_last_name,
                v_recip_address,
                v_recip_city,
                v_recip_state,
                v_recip_zip_code,
                v_recip_country,
                v_recip_phone,
                v_delivery_date;
        CLOSE cust_cur;
END IF;

v_company_id := in_company_id;
IF in_company_id is null AND in_source_code is not null THEN
        OPEN source_cur;
        FETCH source_cur into v_company_id;
        CLOSE source_cur;
END IF;

SELECT point_of_contact_id_sq.NEXTVAL INTO out_sequence_number FROM DUAL;

INSERT INTO point_of_contact
(
        point_of_contact_id,
        customer_id,
        order_guid,
        order_detail_id,
        master_order_number,
        external_order_number,
        delivery_date,
        company_id,
        first_name,
        last_name,
        daytime_phone_number,
        evening_phone_number,
        sender_email_address,
        letter_title,
        email_subject,
        body,
        comment_type,
        recipient_email_address,
        new_recip_first_name,
        new_recip_last_name,
        new_address,
        new_city,
        new_phone,
        new_zip_code,
        new_state,
        new_country,
        new_institution,
        new_delivery_date,
        new_card_message,
        new_product,
        change_codes,
        sent_received_indicator,
        send_flag,
        print_flag,
        created_on,
        created_by,
        updated_on,
        updated_by,
        point_of_contact_type,
        comment_text,
        email_header,
        new_card_signature,
        source_code,
        mailserver_code,
        email_type,
        record_attributes_xml
)
VALUES
(
        out_sequence_number,
        in_customer_id,
        in_order_guid,
        in_order_detail_id,
        in_master_order_number,
        substr (in_external_order_number, 1, 20),
        coalesce (in_delivery_date, v_delivery_date),
        v_company_id,
        substr (coalesce (in_first_name, v_cust_first_name), 1, 25),
        substr (coalesce (in_last_name, v_cust_last_name), 1, 25),
        substr (coalesce (in_daytime_phone_number, v_cust_day_phone), 1, 20),
        substr (coalesce (in_evening_phone_number, v_cust_evening_phone), 1, 20),
        in_sender_email_address,
        in_letter_title,
        substr (in_email_subject, 1, 100),
        in_body,
        in_comment_type,
        in_recipient_email_address,
        substr (coalesce (in_new_recip_first_name, v_recip_first_name), 1, 25),
        substr (coalesce (in_new_recip_last_name, v_recip_last_name), 1, 25),
        substr (coalesce (in_new_address, v_recip_address), 1, 45),
        substr (coalesce (in_new_city, v_recip_city), 1, 30),
        substr (coalesce (in_new_phone, v_recip_phone), 1, 20),
        substr (coalesce (in_new_zip_code, v_recip_zip_code), 1, 12),
        coalesce (in_new_state, v_recip_state),
        substr (coalesce (in_new_country, v_recip_country), 1, 20),
        substr (in_new_institution, 1, 50),
        in_new_delivery_date,
        substr (in_new_card_message, 1, 4000),
        substr (in_new_product, 1, 10),
        in_change_codes,
        in_sent_received_indicator,
        in_send_flag,
        in_print_flag,
        sysdate,
        nvl (in_created_by, 'SYS'),
        sysdate,
        nvl (in_created_by, 'SYS'),
        in_point_of_contact_type,
        in_comment_text,
        in_email_header,
        substr (in_new_card_signature, 1, 2000),
        in_source_code, 
        in_mailserver_code,
        in_email_type,
        in_record_attributes_xml
);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END INSERT_POINT_OF_CONTACT;


PROCEDURE VIEW_POINT_OF_CONTACT
(
IN_POINT_OF_CONTACT_ID          IN POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the specified poc
        record

Input:
        point_of_contact_id             number

Output:
        cursor containing all columns from point_of_contact

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                poc.point_of_contact_id,
                poc.customer_id,
                poc.order_guid,
                poc.order_detail_id,
                poc.master_order_number,
                poc.external_order_number,
                poc.delivery_date,
                poc.company_id,
                poc.first_name,
                poc.last_name,
                poc.daytime_phone_number,
                poc.evening_phone_number,
                poc.sender_email_address,
                poc.letter_title,
                poc.email_subject,
                poc.body,
                poc.comment_type,
                poc.recipient_email_address,
                poc.new_recip_first_name,
                poc.new_recip_last_name,
                poc.new_address,
                poc.new_city,
                poc.new_phone,
                poc.new_zip_code,
                poc.new_state,
                poc.new_country,
                poc.new_institution,
                poc.new_delivery_date,
                poc.new_card_message,
                poc.new_product,
                poc.change_codes,
                poc.sent_received_indicator,
                poc.send_flag,
                poc.print_flag,
                poc.point_of_contact_type,
                to_char (poc.created_on, 'mm/dd/yyyy hh:miAM') created_on,
                poc.created_by,
                poc.updated_by,
                (select o.origin_id from orders o where o.order_guid = poc.order_guid) origin_id,
                poc.comment_text,
                poc.new_card_signature,  
                poc.mailserver_code,
                poc.email_type email_type,
                poc.record_attributes_xml,
                poc.source_code
        FROM    point_of_contact poc
        WHERE   poc.point_of_contact_id = in_point_of_contact_id;

END VIEW_POINT_OF_CONTACT;


PROCEDURE VIEW_POINT_OF_CONTACT_AND_FTD
(
IN_POINT_OF_CONTACT_ID          IN POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the specified poc
        record and same fields from latest FTD.  Currently this info is
        rendered on COM's ViewEmail page so CSR can see what changes user
        is requesting.

Input:
        point_of_contact_id             number

Output:
        Cursor containing all columns from point_of_contact.
        If this was a modify order email then original order information is
        included.  A cursor of ASK messages is also returned in this case.

-----------------------------------------------------------------------------*/

v_is_modify_order        char(1);
v_order_detail_id        varchar2(100);
v_mo_curr_recipient      varchar2(2000);
v_mo_curr_address        varchar2(4000);
v_mo_curr_city_state_zip varchar2(2000);
v_mo_curr_delivery_date  varchar2(50);
v_mo_curr_card_message   varchar2(4000);
v_mo_curr_product_id     varchar2(10);
v_mo_curr_phone_number   varchar2(63);
v_mo_curr_type           varchar2(6);
v_mo_curr_buyer_first    clean.customer.first_name%TYPE;
v_mo_curr_buyer_last     clean.customer.last_name%TYPE;
v_mo_curr_buyer_email    clean.email.email_address%TYPE;
v_mo_curr_buyer_phone    clean.customer_phones.phone_number%TYPE;
v_mo_curr_merc_order_num mercury.mercury.mercury_order_number%TYPE;
v_dummy_date             date;

-- Cursor to see if this was from a customer service modify order email
--
CURSOR mo_type_cur IS
      select  'Y', to_char(order_detail_id)
        from    point_of_contact
        where   comment_type in ('RN','DD','PR','CM','OT')
        and     point_of_contact_id = in_point_of_contact_id;

-- Cursor to get existing order info from clean
-- (Used if this was from a customer service modify order email)
--
CURSOR mo_current_info_cur IS
      select 
          recip.first_name || ' ' || recip.last_name as recipient, 
          recip.address_1  || ' ' || recip.address_2 || ' ' || recip.business_name as address, 
          recip.city || ', ' || recip.state || ' ' || recip.zip_code || ' ' || recip.country as city_state_zip,
          TO_CHAR(od.delivery_date, 'yyyy-mm-dd') as delivery_date,
          od.card_message || od.card_signature as card_message,  
          od.product_id as product_id,
          coalesce (customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( recip.customer_id,'Day'),
                    customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( recip.customer_id,'Evening')) as phone_number,
          buyer.first_name as buyer_first,
          buyer.last_name as buyer_last,
          (select e.email_address from email e where o.email_id = e.email_id) buyer_email,
          coalesce (customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( buyer.customer_id,'Day'),
                    customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( buyer.customer_id,'Evening')) as buyer_phone,
          'CLEAN' as type
      from clean.point_of_contact poc
      join clean.order_details od on od.order_detail_id = poc.order_detail_id
      join clean.orders o on o.order_guid = od.order_guid
      join clean.customer recip on recip.customer_id = od.recipient_id
      join clean.customer buyer on buyer.customer_id = o.customer_id
      where poc.point_of_contact_id = in_point_of_contact_id;


-- Cursor to get existing order info from latest FTD.
-- It may be in merc or venus, we don't care so just do join and order by created_on
-- so most recent will be first.
-- (Used if this was from a customer service modify order email)
--
CURSOR mo_current_ftd_cur IS
          select 
              merc.recipient as recipient,
              merc.address as address,
              merc.city_state_zip as city_state_zip,
              TO_CHAR(merc.delivery_date, 'yyyy-mm-dd') as delivery_date,
              merc.card_message as card_message,
              merc.product_id as product_id,
              merc.phone_number as phone_number,
              'MERC' as type,
              merc.mercury_message_number as mercury_message_number,
              merc.created_on as created_on
          from mercury.mercury merc 
          where merc.reference_number = v_order_detail_id
          and merc.msg_type = 'FTD'
      UNION
          select 
              venus.recipient as recipient,
              venus.address_1 || ' ' || venus.address_2 || ' ' || venus.business_name as address,
              venus.city || ', ' || venus.state || ' ' || venus.zip as city_state_zip,
              TO_CHAR(venus.delivery_date, 'yyyy-mm-dd') as delivery_date,
              venus.card_message as card_message,
              venus.product_id as product_id,
              venus.phone_number as phone_number,
              'VENUS' as type,
              null as mercury_message_number,
              venus.created_on as created_on
          from venus.venus venus 
          where venus.reference_number = v_order_detail_id
          and venus.msg_type = 'FTD'
      ORDER BY created_on DESC;


BEGIN

OPEN mo_type_cur;
FETCH mo_type_cur INTO v_is_modify_order, v_order_detail_id;
CLOSE mo_type_cur;

-- If this was a modify order email then get original order information too
--
IF v_is_modify_order is not null THEN

    -- Get info from clean first
    OPEN mo_current_info_cur;
    FETCH mo_current_info_cur INTO
        v_mo_curr_recipient,
        v_mo_curr_address,
        v_mo_curr_city_state_zip,
        v_mo_curr_delivery_date,
        v_mo_curr_card_message,
        v_mo_curr_product_id,
        v_mo_curr_phone_number,
        v_mo_curr_buyer_first,
        v_mo_curr_buyer_last,
        v_mo_curr_buyer_email,
        v_mo_curr_buyer_phone,
        v_mo_curr_type;
    CLOSE mo_current_info_cur;
    
    -- Overwrite with any FTD message info
    OPEN mo_current_ftd_cur;
    FETCH mo_current_ftd_cur INTO
        v_mo_curr_recipient,
        v_mo_curr_address,
        v_mo_curr_city_state_zip,
        v_mo_curr_delivery_date,
        v_mo_curr_card_message,
        v_mo_curr_product_id,
        v_mo_curr_phone_number,
        v_mo_curr_type,
        v_mo_curr_merc_order_num,
        v_dummy_date;
    CLOSE mo_current_ftd_cur;

END IF;

-- Return point of contact info (along with additional info from above)
--
OPEN OUT_CUR FOR
        SELECT
                poc.point_of_contact_id,
                poc.customer_id,
                poc.order_guid,
                poc.order_detail_id,
                poc.master_order_number,
                poc.external_order_number,
                poc.delivery_date,
                poc.company_id,
                poc.first_name,
                poc.last_name,
                poc.daytime_phone_number,
                poc.evening_phone_number,
                poc.sender_email_address,
                poc.letter_title,
                poc.email_subject,
                regexp_replace(regexp_replace(poc.body, '<script', '<!--script', 1, 0, 'i'), '</script>', '</script-->', 1, 0, 'i') body,
                poc.comment_type,
                poc.recipient_email_address,
                poc.new_recip_first_name,
                poc.new_recip_last_name,
                poc.new_address,
                poc.new_city,
                poc.new_phone,
                poc.new_zip_code,
                poc.new_state,
                poc.new_country,
                poc.new_institution,
                poc.new_delivery_date,
                poc.new_card_message,
                poc.new_product,
                poc.change_codes,
                nvl(poc.sent_received_indicator, 'O') sent_received_indicator,
                poc.send_flag,
                poc.print_flag,
                poc.point_of_contact_type,
                to_char (poc.created_on, 'mm/dd/yyyy hh:miAM') created_on,
                poc.created_by,
                poc.updated_by,
                (select o.origin_id from orders o where o.order_guid = poc.order_guid) origin_id,
                poc.comment_text,
                poc.new_card_signature,
                v_is_modify_order is_modify_order,
                v_mo_curr_recipient mo_curr_recipient,
                v_mo_curr_address mo_curr_address,
                v_mo_curr_city_state_zip mo_curr_city_state_zip,
                v_mo_curr_delivery_date mo_curr_delivery_date,
                v_mo_curr_card_message mo_curr_card_message,
                v_mo_curr_product_id mo_curr_product_id,
                v_mo_curr_phone_number mo_curr_phone_number,
                v_mo_curr_buyer_first mo_curr_buyer_firs,
                v_mo_curr_buyer_last mo_curr_buyer_last,
                v_mo_curr_buyer_email mo_curr_buyer_last,
                v_mo_curr_buyer_phone mo_curr_buyer_phone,
                v_mo_curr_type mo_curr_type,
                v_mo_curr_merc_order_num mo_curr_merc_order_num
        FROM    point_of_contact poc
        WHERE   poc.point_of_contact_id = in_point_of_contact_id;

END VIEW_POINT_OF_CONTACT_AND_FTD;


PROCEDURE VIEW_POINT_OF_CONTACT_ASKS
(
IN_MERCURY_ORDER_NUM            IN MERCURY.MERCURY.MERCURY_ORDER_NUMBER%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning ASK messages for the 
        specified FTD message.  Currently this info is rendered on COM's 
        ViewEmail page so CSR can compare latest order info against changes
        sent by customer.  ASK messages are useful since they may contain 
        additional changes already requested by the CSR (on behalf of the customer).

Input:
        point_of_contact_id             number

Output:
        Returns cursor of any ASK messages 
        (we only want outbound ASK's associated with last FTD)

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    SELECT
      to_char(created_on, 'mm/dd/yyyy hh:miAM') created_on,
      comments
    FROM mercury.mercury merc
    WHERE merc.msg_type = 'ASK'
    and merc.message_direction = 'OUTBOUND'
    and merc.mercury_order_number = IN_MERCURY_ORDER_NUM
    order by merc.created_on DESC;

END VIEW_POINT_OF_CONTACT_ASKS;


FUNCTION ORDER_HAS_EMAILS
(
IN_ORDER_DETAIL_ID              IN COMMENTS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This function returns an indicator if the order has emails associated
        to it.

Input:
        order_detail_id         number

Output:
        Y/N if the order has emails

-----------------------------------------------------------------------------*/

CURSOR poc_cur IS
        SELECT  DISTINCT 'Y'
        FROM    point_of_contact c
        WHERE   c.order_detail_id = in_order_detail_id
        AND     c.point_of_contact_type = 'Email';

v_poc        varchar2(1) := 'N';

BEGIN

OPEN poc_cur;
FETCH poc_cur INTO v_poc;
CLOSE poc_cur;

RETURN v_poc;

END ORDER_HAS_EMAILS;


FUNCTION ORDER_HAS_LETTERS
(
IN_ORDER_DETAIL_ID              IN COMMENTS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This function returns an indicator if the order has letters associated
        to it.

Input:
        order_detail_id         number

Output:
        Y/N if the order has letters

-----------------------------------------------------------------------------*/

CURSOR poc_cur IS
        SELECT  DISTINCT 'Y'
        FROM    point_of_contact c
        WHERE   c.order_detail_id = in_order_detail_id
        AND     c.point_of_contact_type = 'Letter';

v_poc        varchar2(1) := 'N';

BEGIN

OPEN poc_cur;
FETCH poc_cur INTO v_poc;
CLOSE poc_cur;

RETURN v_poc;

END ORDER_HAS_LETTERS;


PROCEDURE UPDATE_POINT_OF_CONTACT
(
IN_POINT_OF_CONTACT_ID          IN POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
IN_EMAIL_SUBJECT                IN POINT_OF_CONTACT.EMAIL_SUBJECT%TYPE,
IN_BODY                         IN POINT_OF_CONTACT.BODY%TYPE,
IN_UPDATED_BY                   IN POINT_OF_CONTACT.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting point of contact info

Input:
        point of contact id             number
        email_subject                   varchar2
        body                            clob
				updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  point_of_contact
   SET  email_subject = substr (IN_EMAIL_SUBJECT, 1, 100)
       ,body = IN_BODY
       ,updated_by = IN_UPDATED_BY
       ,updated_on = sysdate
 WHERE  point_of_contact_id = IN_POINT_OF_CONTACT_ID;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_ORDERS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_POINT_OF_CONTACT;


PROCEDURE GET_POC_BY_RECIPIENT_EMAIL
(
IN_RECIPIENT_EMAIL_ADDRESS      IN POINT_OF_CONTACT.RECIPIENT_EMAIL_ADDRESS%TYPE,
IN_EMAIL_TYPE					IN POINT_OF_CONTACT.EMAIL_TYPE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

v_sql varchar2(32000);

BEGIN

v_sql := 'SELECT POINT_OF_CONTACT_ID,';
v_sql := v_sql || '  CUSTOMER_ID,';
v_sql := v_sql || '  ORDER_GUID,';
v_sql := v_sql || '  ORDER_DETAIL_ID,';
v_sql := v_sql || '  MASTER_ORDER_NUMBER,';
v_sql := v_sql || '  EXTERNAL_ORDER_NUMBER,';
v_sql := v_sql || '  BODY,';
v_sql := v_sql || '  EMAIL_SUBJECT,';
v_sql := v_sql || '  DELIVERY_DATE,';
v_sql := v_sql || '  COMPANY_ID,';
v_sql := v_sql || '  FIRST_NAME,';
v_sql := v_sql || '  LAST_NAME,';
v_sql := v_sql || '  DAYTIME_PHONE_NUMBER,';
v_sql := v_sql || '  EVENING_PHONE_NUMBER,';
v_sql := v_sql || '  COMMENT_TYPE,';
v_sql := v_sql || '  RECIPIENT_EMAIL_ADDRESS,';
v_sql := v_sql || '  POINT_OF_CONTACT_TYPE,';
v_sql := v_sql || '  SOURCE_CODE';
v_sql := v_sql || ' FROM CLEAN.POINT_OF_CONTACT ';
v_sql := v_sql || ' WHERE EMAIL_TYPE in (' || in_email_type || ')';
v_sql := v_sql || ' AND CREATED_ON > sysdate-3';
v_sql := v_sql || ' AND (SENT_RECEIVED_INDICATOR IS NULL';
v_sql := v_sql || ' OR SENT_RECEIVED_INDICATOR    = ''O'')';
v_sql := v_sql || ' AND RECIPIENT_EMAIL_ADDRESS   =  ''' || IN_RECIPIENT_EMAIL_ADDRESS || '''';
v_sql := v_sql || ' AND (BOUNCE_PROCESSED_FLAG    = ''N''';
v_sql := v_sql || ' OR BOUNCE_PROCESSED_FLAG     IS NULL)';

OPEN OUT_CUR FOR v_sql;

END GET_POC_BY_RECIPIENT_EMAIL;


PROCEDURE UPDATE_POC_BOUNCE_FLAG
(
IN_POINT_OF_CONTACT_ID          IN POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
IN_BOUNCE_PROCESSED_FLAG       IN POINT_OF_CONTACT.BOUNCE_PROCESSED_FLAG%TYPE,
IN_UPDATED_BY                   IN POINT_OF_CONTACT.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
BEGIN

UPDATE  point_of_contact
   SET  BOUNCE_PROCESSED_FLAG = IN_BOUNCE_PROCESSED_FLAG
       ,updated_by = IN_UPDATED_BY
       ,updated_on = sysdate
 WHERE  point_of_contact_id = IN_POINT_OF_CONTACT_ID;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_POC_BOUNCE_FLAG [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_POC_BOUNCE_FLAG;


PROCEDURE UPDATE_CONFIRMATION_DETAILS
(
IN_POINT_OF_CONTACT_ID          IN POINT_OF_CONTACT.POINT_OF_CONTACT_ID%TYPE,
IN_EMAIL_SUBJECT                IN POINT_OF_CONTACT.EMAIL_SUBJECT%TYPE,
IN_RECORD_ATTRIBUTES_XML        IN POINT_OF_CONTACT.RECORD_ATTRIBUTES_XML%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
BEGIN

UPDATE  POINT_OF_CONTACT
   SET  EMAIL_SUBJECT = IN_EMAIL_SUBJECT,
        RECORD_ATTRIBUTES_XML = IN_RECORD_ATTRIBUTES_XML,
        UPDATED_ON = SYSDATE
WHERE  POINT_OF_CONTACT_ID = IN_POINT_OF_CONTACT_ID;


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_CONFIRMATION_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CONFIRMATION_DETAILS;


END;
.
/
