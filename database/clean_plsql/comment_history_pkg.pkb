CREATE OR REPLACE
PACKAGE BODY clean.COMMENT_HISTORY_PKG
AS

PROCEDURE INSERT_COMMENTS
(
IN_CUSTOMER_ID                  IN COMMENTS.CUSTOMER_ID%TYPE,
IN_ORDER_GUID                   IN COMMENTS.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN COMMENTS.ORDER_DETAIL_ID%TYPE,
IN_COMMENT_ORIGIN               IN COMMENTS.COMMENT_ORIGIN%TYPE,
IN_REASON                       IN VARCHAR2,
IN_DNIS_ID                      IN COMMENTS.DNIS_ID%TYPE,
IN_COMMENT_TEXT                 IN COMMENTS.COMMENT_TEXT%TYPE,
IN_COMMENT_TYPE                 IN COMMENTS.COMMENT_TYPE%TYPE,
IN_CSR_ID                       IN COMMENTS.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for adding new comments

Input:
        comment_id                      number
        customer_id                     number
        order_guid                      varchar2
        order_detail_id                 number
        comment_origin                  varchar2
        reason                          varchar2 -- not used
        dnis_id                         number
        comment_text                    varchar2
        comment_type                    varchar2
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO comments
(
        comment_id,
        customer_id,
        order_guid,
        order_detail_id,
        comment_origin,
        dnis_id,
        comment_text,
        comment_type,
        created_on,
        created_by,
        updated_on,
        updated_by
)
VALUES
(
        comment_id_sq.NEXTVAL,
        in_customer_id,
        in_order_guid,
        in_order_detail_id,
        in_comment_origin,
        in_dnis_id,
        in_comment_text,
        in_comment_type,
        SYSDATE,
        in_csr_id,
        SYSDATE,
        in_csr_id
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_COMMENTS;


PROCEDURE UPDATE_ENTITY_HISTORY
(
IN_ENTITY_HISTORY_ID            IN ENTITY_HISTORY.ENTITY_HISTORY_ID%TYPE,
IN_ENTITY_TYPE                  IN ENTITY_HISTORY.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN ENTITY_HISTORY.ENTITY_ID%TYPE,
IN_COMMENT_ORIGIN               IN ENTITY_HISTORY.COMMENT_ORIGIN%TYPE,
IN_CSR_ID                       IN ENTITY_HISTORY.CSR_ID%TYPE,
IN_CALL_LOG_ID                  IN ENTITY_HISTORY.CALL_LOG_ID%TYPE,
OUT_ENTITY_HISTORY_ID           OUT ENTITY_HISTORY.ENTITY_HISTORY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for starting or ending the entity
        history timer.  If the history id is not passed in, a new timer
        will begin for the given entity.  If the history id is passed in,
        the timer will end for the given record

Input:
        order_history_id                number
        comment_origin                  varchar2
        csr_id                          varchar2
        call_log_id

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF in_entity_history_id IS NOT NULL THEN
        UPDATE  entity_history
        SET
                end_time = SYSDATE
        WHERE   entity_history_id = in_entity_history_id;

ELSE
        INSERT INTO  entity_history
        (
                entity_history_id,
                entity_type,
                entity_id,
                comment_origin,
                csr_id,
                start_time,
                call_log_id
        )
        VALUES
        (
                entity_history_id_sq.NEXTVAL,
                in_entity_type,
                in_entity_id,
                in_comment_origin,
                in_csr_id,
                SYSDATE,
                in_call_log_id
        ) RETURNING entity_history_id INTO out_entity_history_id;

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ENTITY_HISTORY;



PROCEDURE GET_CUSTOMER_HISTORY
(
IN_CUSTOMER_ID                  IN CUSTOMER_HISTORY.CUSTOMER_ID%TYPE,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the customer history
        for the given customer and given page index.

Input:
        customer_id                     number

Output:
        cursor containing all records from customer_history

-----------------------------------------------------------------------------*/
-- initial query to get the ids
CURSOR id_cur IS
        SELECT  h.customer_history_id
        FROM    customer_history h
        WHERE   h.customer_id = in_customer_id
        ORDER BY h.created_on;

-- array for the results of the initial query
id_tab                  id_list_pkg.number_tab_typ;

-- string to store the list of ids
v_str_id                VARCHAR2(32767);


BEGIN

OPEN id_cur;
FETCH id_cur BULK COLLECT INTO id_tab;
CLOSE id_cur;

-- get the ids in a string
id_list_pkg.get_str_id (id_tab, in_start_position, in_max_number_returned, v_str_id);

OUT_ID_COUNT := id_tab.COUNT;

OPEN OUT_CUR FOR
        'SELECT
                h.csr_id,
                h.activity,
                TO_CHAR (h.created_on, ''mm/dd/yyyy'') created_on
        FROM    customer_history h
        WHERE   h.customer_history_id IN (' || v_str_id || ') ' ||
        'ORDER BY h.created_on';

END GET_CUSTOMER_HISTORY;


PROCEDURE GET_ORDER_HISTORY
(
IN_ORDER_DETAIL_ID              IN NUMBER,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the order history
        for the given order and given page index.

Input:
        order_history_id                number
        order_guid                      varchar2
        order_detail_id                 number
        comment_origin                  varchar2
        csr_id                          varchar2
        history_start_date              date
        history_end_date                date
        system_ended_indicator          char

Output:
        cursor containing order_history information

-----------------------------------------------------------------------------*/
-- initial query to get the ids
CURSOR id_cur IS
        SELECT  h.entity_history_id
        FROM    entity_history h
        WHERE   ((h.entity_type = 'ORDER_DETAILS'
        AND      h.entity_id = TO_CHAR (in_order_detail_id))
        OR      (h.entity_type = 'ORDERS'
        AND      h.entity_id = (SELECT order_guid FROM order_details WHERE order_detail_id = in_order_detail_id)))
        ORDER BY h.start_time;

-- array for the results of the initial query
id_tab                  id_list_pkg.number_tab_typ;

-- string to store the list of ids
v_str_id                VARCHAR2(32767);


BEGIN

OPEN id_cur;
FETCH id_cur BULK COLLECT INTO id_tab;
CLOSE id_cur;

-- get the ids in a string
id_list_pkg.get_str_id (id_tab, in_start_position, in_max_number_returned, v_str_id);

OUT_ID_COUNT := id_tab.COUNT;

OPEN OUT_CUR FOR
        'SELECT
                TO_CHAR (h.start_time, ''mm/dd/yyyy'') history_date,
                TO_CHAR (h.start_time, ''hh:mi:ssAM'') history_start_time,
                TO_CHAR (h.end_time, ''hh:mi:ssAM'') history_end_time,
                DECODE (h.end_time, NULL, NULL,
                        LTRIM (TO_CHAR ( TRUNC ((h.end_time - h.start_time) * 24), ''00'' )) || '':'' ||
                        LTRIM (TO_CHAR ( TRUNC ((h.end_time - h.start_time) * 1440) - (TRUNC ((h.end_time - h.start_time) * 24) * 60), ''00'' )) || '':'' ||
                        LTRIM (TO_CHAR ( ROUND ((h.end_time - h.start_time) * 86400) - (TRUNC ((h.end_time - h.start_time) * 1440) * 60), ''00'' ))
                ) LENGTH,
                h.csr_id,
                h.comment_origin
        FROM    entity_history h
        WHERE   h.entity_history_id IN (' || v_str_id || ') ' ||
        'ORDER BY h.start_time';

END GET_ORDER_HISTORY;


PROCEDURE GET_COMMENTS
(
IN_COMMENT_TYPE                 IN COMMENTS.COMMENT_TYPE%TYPE,
IN_CUSTOMER_ID                  IN COMMENTS.CUSTOMER_ID%TYPE,
IN_ORDER_GUID                   IN COMMENTS.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN COMMENTS.ORDER_DETAIL_ID%TYPE,
IN_RESOURCE_ID                  IN AAS.REL_ACL_RP.RESOURCE_ID%TYPE,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_MO_COMMENT_INDICATOR         IN VARCHAR2,
IN_HIDE_SYS_COMMENTS            IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the comments
        for the given customer or order and given page index.

Input:
        comment_type                    varchar2
        customer_id                     number
        order_guid                      varchar2
        order_detail_id                 number
        resource_id                     varchar2 --- not used, manager resource id is stored in frp.global_parms
        start_position                  number -- used for paging
        max_number_returned             number -- used for paging
        mo_comment_indicator            varchar2 -- Y/N to display manager only comments
        hide_sys_comments               varchar2 -- Y/N to display system comments

Output:
        cursor containing comments information

-----------------------------------------------------------------------------*/

v_manager_resource_id VARCHAR2 (20) := 'COM Manager Comment';

-- dynamic sql variables
v_sql                   VARCHAR2 (32000);
sql_cur                 types.ref_cursor;

v_select                VARCHAR2 (500) := 'SELECT c.comment_id FROM comments c ';
v_where                 VARCHAR2 (500) := 'WHERE 1=1 ';
v_comment_type          VARCHAR2 (500) := 'AND c.comment_type = ''' || in_comment_type || ''' ';
v_customer_id           VARCHAR2 (500) := 'AND c.customer_id = ' || in_customer_id || ' ';
v_order_guid            VARCHAR2 (4000) := 'AND c.order_guid = ''' || in_order_guid || ''' ';
v_order_detail_id       VARCHAR2 (500) := 'AND c.order_detail_id = ' || in_order_detail_id || ' ';
v_manager_auth          VARCHAR2 (500) := 'AND aas.authenticate_pkg.is_csr_in_resource (c.created_by, ''' || v_manager_resource_id || ''', ''Yes'') = ''Y'' ';
v_hide_sys_comments     VARCHAR2 (500) := 'AND c.created_by <> ''SYS'' ';
v_order_by              VARCHAR2 (2000);

-- array for the results of the initial query
id_tab                  id_list_pkg.number_tab_typ;

-- string to store the list of ids
v_str_id                VARCHAR2(32767);

BEGIN

-- get both customer and order types if requesting customer comments
IF in_comment_type <> 'Customer' THEN
        v_where := v_where || v_comment_type;
END IF;

IF in_customer_id IS NOT NULL THEN
        v_where := v_where || v_customer_id;
END IF;

IF in_order_guid IS NOT NULL THEN
        v_where := v_where || v_order_guid;
END IF;

IF in_order_detail_id IS NOT NULL THEN
        v_where := v_where || v_order_detail_id;
ELSE
   -- this path can result in a full table scan of cmments; return an empty cursor instead
   IF in_customer_id IS NULL AND in_order_guid IS NULL AND in_mo_comment_indicator ='N' THEN

        v_where := v_where || 'AND c.order_detail_id = -1 ';

   END IF;

END IF;

IF in_mo_comment_indicator <> 'N' THEN
        v_where := v_where || v_manager_auth;
END IF;

IF in_hide_sys_comments <> 'N' THEN
        v_where := v_where || v_hide_sys_comments;
END IF;

IF in_comment_type = 'Customer' THEN
	v_order_by := 'ORDER BY ''---'',created_on';
ELSE
	v_order_by := 'ORDER BY (select od.external_order_number from clean.order_details od where od.order_detail_id = c.order_detail_id), created_on desc';
END IF;

v_sql := v_select || v_where || v_order_by;

OPEN sql_cur FOR v_sql;
FETCH sql_cur bulk collect INTO id_tab;
CLOSE sql_cur;

-- get the ids in a string
id_list_pkg.get_str_id (id_tab, in_start_position, in_max_number_returned, v_str_id);

IF in_comment_type = 'Customer' THEN
	v_order_by := '''---'',created_on';
ELSE
	v_order_by := 'od.external_order_number, c.created_on desc';
END IF;	


OPEN OUT_CUR FOR
       'SELECT  c.comment_id,
                c.created_by,
                TO_CHAR (c.created_on, ''mm/dd/yyyy hh:miAM'') created_on,
                od.external_order_number,
                c.comment_origin,
                c.comment_text,
                aas.authenticate_pkg.is_csr_in_resource (c.created_by, :1, ''Yes'') manager_indicator
        FROM    comments c
        LEFT OUTER JOIN order_details od
		ON      c.order_detail_id = od.order_detail_id
		WHERE   c.comment_id IN (' || v_str_id || ') 
		ORDER BY '|| v_order_by
        USING v_manager_resource_id;

OUT_ID_COUNT := id_tab.COUNT;

END GET_COMMENTS;


FUNCTION ORDER_HAS_COMMENTS
(
IN_ORDER_DETAIL_ID              IN COMMENTS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This function returns an indicator if the order has any comments

Input:
        order_detail_id         number

Output:
        Y/N if the order has comments

-----------------------------------------------------------------------------*/

CURSOR comment_cur IS
        SELECT  DISTINCT 'Y'
        FROM    comments c
        WHERE   c.order_detail_id = in_order_detail_id;

v_comments        VARCHAR2(1) := 'N';

BEGIN

OPEN comment_cur;
FETCH comment_cur INTO v_comments;
CLOSE comment_cur;

RETURN v_comments;

END ORDER_HAS_COMMENTS;


FUNCTION CUSTOMER_HAS_COMMENTS
(
IN_CUSTOMER_ID                  IN COMMENTS.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This function returns an indicator if the customer has comments

Input:
        customer                number

Output:
        Y/N if the order has comments

-----------------------------------------------------------------------------*/

CURSOR comment_cur IS
        SELECT  DISTINCT 'Y'
        FROM    comments c
        WHERE   c.customer_id = in_customer_id;

v_comments        VARCHAR2(1) := 'N';

BEGIN

OPEN comment_cur;
FETCH comment_cur INTO v_comments;
CLOSE comment_cur;

RETURN v_comments;

END CUSTOMER_HAS_COMMENTS;


PROCEDURE EDIT_COMMENTS
(
IN_COMMENT_ID                   IN COMMENTS.COMMENT_ID%TYPE,
IN_COMMENT_TEXT                 IN COMMENTS.COMMENT_TEXT%TYPE,
IN_CSR_ID                       IN COMMENTS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for editing a comment

Input:
        comment_id                      number
        comment_text                    varchar2
        csr_id                          varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  COMMENTS
   SET  COMMENT_TEXT = IN_COMMENT_TEXT
       ,UPDATED_ON = SYSDATE
       ,UPDATED_BY = IN_CSR_ID
 WHERE  COMMENT_ID = IN_COMMENT_ID; 
       

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END EDIT_COMMENTS;


PROCEDURE GET_FILTERED_COMMENTS
(
	IN_COMMENT_TYPE                 IN COMMENTS.COMMENT_TYPE%TYPE,
	IN_CUSTOMER_ID                  IN COMMENTS.CUSTOMER_ID%TYPE,
	IN_ORDER_GUID                   IN COMMENTS.ORDER_GUID%TYPE,
	IN_ORDER_DETAIL_ID              IN COMMENTS.ORDER_DETAIL_ID%TYPE,
	IN_RESOURCE_ID                  IN AAS.REL_ACL_RP.RESOURCE_ID%TYPE,
	IN_START_POSITION               IN NUMBER,
	IN_MAX_NUMBER_RETURNED          IN NUMBER,
	IN_MO_COMMENT_INDICATOR         IN VARCHAR2,
	IN_HIDE_SYS_COMMENTS            IN VARCHAR2,
	OUT_CUR                         OUT TYPES.REF_CURSOR,
	OUT_ID_COUNT                    OUT NUMBER
)
AS
	/*-----------------------------------------------------------------------------
	Description:
			This procedure is responsible for returning the comments excluding the ones provided by userIDs configured in global param: ORDER_COMMENTS_SUPPRESS_USERIDs
			for the given customer or order and given page index.

	Input:
			comment_type                    varchar2
			customer_id                     number
			order_guid                      varchar2
			order_detail_id                 number
			resource_id                     varchar2 --- not used, manager resource id is stored in frp.global_parms
			start_position                  number -- used for paging
			max_number_returned             number -- used for paging
			mo_comment_indicator            varchar2 -- Y/N to display manager only comments
			hide_sys_comments               varchar2 -- Y/N to display system comments

	Output:
			cursor containing comments information

	-----------------------------------------------------------------------------*/

	v_manager_resource_id VARCHAR2 (20) := 'COM Manager Comment';

	-- dynamic sql variables
	v_sql                   VARCHAR2 (32000);
	sql_cur                 types.ref_cursor;

	v_select                VARCHAR2 (500) := 'SELECT c.comment_id FROM comments c ';
	v_where                 VARCHAR2 (500) := 'WHERE 1=1 ';
	v_comment_type          VARCHAR2 (500) := 'AND c.comment_type = ''' || in_comment_type || ''' ';
	v_customer_id           VARCHAR2 (500) := 'AND c.customer_id = ' || in_customer_id || ' ';
	v_order_guid            VARCHAR2 (4000) := 'AND c.order_guid = ''' || in_order_guid || ''' ';
	v_order_detail_id       VARCHAR2 (500) := 'AND c.order_detail_id = ' || in_order_detail_id || ' ';
	v_manager_auth          VARCHAR2 (500) := 'AND aas.authenticate_pkg.is_csr_in_resource (c.created_by, ''' || v_manager_resource_id || ''', ''Yes'') = ''Y'' ';
	v_hide_sys_comments     VARCHAR2 (500) := 'AND c.created_by <> ''SYS'' ';
	v_order_by              VARCHAR2 (2000);
	v_suppress_userIDs		VARCHAR2 (2000) := '';

	-- array for the results of the initial query
	id_tab                  id_list_pkg.number_tab_typ;

	-- string to store the list of ids
	v_str_id                VARCHAR2(32767);

BEGIN

	-- get both customer and order types if requesting customer comments
	IF in_comment_type <> 'Customer' THEN
			v_where := v_where || v_comment_type;
	END IF;

	IF in_customer_id IS NOT NULL THEN
			v_where := v_where || v_customer_id;
	END IF;

	IF in_order_guid IS NOT NULL THEN
			v_where := v_where || v_order_guid;
	END IF;

	IF in_order_detail_id IS NOT NULL THEN
			v_where := v_where || v_order_detail_id;
	ELSE
	   -- this path can result in a full table scan of cmments; return an empty cursor instead
	   IF in_customer_id IS NULL AND in_order_guid IS NULL AND in_mo_comment_indicator ='N' THEN

			v_where := v_where || 'AND c.order_detail_id = -1 ';

	   END IF;

	END IF;
	
	IF in_mo_comment_indicator <> 'N' THEN
			v_where := v_where || v_manager_auth;
	END IF;

	IF in_hide_sys_comments <> 'N' THEN
			v_where := v_where || v_hide_sys_comments;
	END IF;
	
	v_suppress_userIDs := frp.misc_pkg.get_global_parm_value('CUSTOMER_ORDER_MANAGEMENT','ORDER_COMMENTS_SUPPRESS_USERIDs');
	
	if(trim(v_suppress_userIDs) is not null) then
		v_suppress_userIDs := chr(39)||v_suppress_userIDs||chr(39);
		v_where := v_where || ' AND c.created_by not in (select regexp_substr('||v_suppress_userIDs||',''[^,]+'',1,level) userID from dual connect by regexp_substr('||v_suppress_userIDs||',''[^,]+'',1,level) is not null)';
	end if;
		
	IF in_comment_type = 'Customer' THEN
		v_order_by := 'ORDER BY ''---'',created_on';
	ELSE
		v_order_by := 'ORDER BY (select od.external_order_number from clean.order_details od where od.order_detail_id = c.order_detail_id), c.created_on desc';
	END IF;

	v_sql := v_select || v_where || v_order_by;

	OPEN sql_cur FOR v_sql;
	FETCH sql_cur bulk collect INTO id_tab;
	CLOSE sql_cur;

	-- get the ids in a string
	id_list_pkg.get_str_id (id_tab, in_start_position, in_max_number_returned, v_str_id);

	IF in_comment_type = 'Customer' THEN
		v_order_by := '''---'',c.created_on';
	ELSE
		v_order_by := 'od.external_order_number, c.created_on desc';
	END IF;	


	OPEN OUT_CUR FOR
		   'SELECT  c.comment_id,
					c.created_by,
					TO_CHAR (c.created_on, ''mm/dd/yyyy hh:miAM'') created_on,
					od.external_order_number,
					c.comment_origin,
					c.comment_text,
					aas.authenticate_pkg.is_csr_in_resource (c.created_by, :1, ''Yes'') manager_indicator
			FROM    comments c
			LEFT OUTER JOIN order_details od
			ON      c.order_detail_id = od.order_detail_id
			WHERE   c.comment_id IN (' || v_str_id || ') 
			ORDER BY '|| v_order_by
			USING v_manager_resource_id;

	OUT_ID_COUNT := id_tab.COUNT;

END GET_FILTERED_COMMENTS;


END;
.
/
