
 
CREATE OR REPLACE TRIGGER clean.pmt_for_each_row_trigger 
AFTER UPDATE ON clean.payments
FOR EACH ROW
WHEN (new.bill_status != old.bill_status and new.payment_indicator='P') 
BEGIN 
   payments_trigger_pkg.for_each_row_trigger(:new.payment_id); 
END; 
/