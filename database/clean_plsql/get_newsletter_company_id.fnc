CREATE OR REPLACE
FUNCTION clean.GET_NEWSLETTER_COMPANY_ID (
 IN_COMPANY_ID IN VARCHAR2
)
RETURN VARCHAR2 is
   v_return_company_id varchar2(50);

BEGIN

    BEGIN

        select newsletter_company_id
        into v_return_company_id
        from ftd_apps.company_master cm
        where cm.company_id = in_company_id;

        EXCEPTION WHEN NO_DATA_FOUND THEN
           v_return_company_id := null;

    END;
        
    RETURN v_return_company_id;
END;
.
/
