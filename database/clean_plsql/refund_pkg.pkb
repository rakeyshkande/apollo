CREATE OR REPLACE
PACKAGE BODY clean.REFUND_PKG AS

PROCEDURE INSERT_REFUND
(
IN_REFUND_DISP_CODE             IN REFUND.REFUND_DISP_CODE%TYPE,
IN_CREATED_BY                   IN REFUND.CREATED_BY%TYPE,
IN_REFUND_PRODUCT_AMOUNT        IN REFUND.REFUND_PRODUCT_AMOUNT%TYPE,
IN_REFUND_ADDON_AMOUNT          IN REFUND.REFUND_ADDON_AMOUNT%TYPE,
IN_REFUND_SERVICE_FEE           IN REFUND.REFUND_SERVICE_FEE%TYPE,
IN_REFUND_TAX                   IN REFUND.REFUND_TAX%TYPE,
IN_ORDER_DETAIL_ID              IN REFUND.ORDER_DETAIL_ID%TYPE,
IN_RESPONSIBLE_PARTY            IN REFUND.RESPONSIBLE_PARTY%TYPE,
IN_REFUND_STATUS                IN REFUND.REFUND_STATUS%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_REFUND_ADMIN_FEE             IN REFUND.REFUND_ADMIN_FEE%TYPE,
IN_REFUND_SHIPPING_FEE          IN REFUND.REFUND_SHIPPING_FEE%TYPE,
IN_REFUND_SERVICE_FEE_TAX       IN REFUND.REFUND_SERVICE_FEE_TAX%TYPE,
IN_REFUND_SHIPPING_TAX          IN REFUND.REFUND_SHIPPING_TAX%TYPE,
IN_REFUND_DISCOUNT_AMOUNT       IN REFUND.REFUND_DISCOUNT_AMOUNT%TYPE,
IN_REFUND_COMMISSION_AMOUNT     IN REFUND.REFUND_COMMISSION_AMOUNT%TYPE,
IN_REFUND_WHOLESALE_AMOUNT      IN REFUND.REFUND_WHOLESALE_AMOUNT%TYPE,
IN_REFUND_WHOLESALE_SERV_FEE    IN REFUND.REFUND_WHOLESALE_SERVICE_FEE%TYPE,
IN_ORIG_COMPLAINT_COMM_TYPE_ID  IN REFUND.ORIGIN_COMPLAINT_COMM_TYPE_ID%TYPE,
IN_NOTI_COMPLAINT_COMM_TYPE_ID  IN REFUND.NOTIF_COMPLAINT_COMM_TYPE_ID%TYPE,
IN_TAX1_NAME                    IN REFUND.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT                  IN REFUND.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION             IN REFUND.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN REFUND.TAX1_RATE%TYPE,
IN_TAX2_NAME                    IN REFUND.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT                  IN REFUND.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION             IN REFUND.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN REFUND.TAX2_RATE%TYPE,
IN_TAX3_NAME                    IN REFUND.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT                  IN REFUND.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION             IN REFUND.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN REFUND.TAX3_RATE%TYPE,
IN_TAX4_NAME                    IN REFUND.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT                  IN REFUND.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION             IN REFUND.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN REFUND.TAX4_RATE%TYPE,
IN_TAX5_NAME                    IN REFUND.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT                  IN REFUND.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION             IN REFUND.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN REFUND.TAX5_RATE%TYPE,
OUT_REFUND_ID                   OUT REFUND.REFUND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_REFUND
(
IN_REFUND_DISP_CODE,
IN_CREATED_BY,
IN_REFUND_PRODUCT_AMOUNT,
IN_REFUND_ADDON_AMOUNT,
IN_REFUND_SERVICE_FEE,
IN_REFUND_TAX,
IN_ORDER_DETAIL_ID,
IN_RESPONSIBLE_PARTY,
IN_REFUND_STATUS,
IN_ACCT_TRANS_IND,
IN_REFUND_ADMIN_FEE,
IN_REFUND_SHIPPING_FEE,
IN_REFUND_SERVICE_FEE_TAX,
IN_REFUND_SHIPPING_TAX,
IN_REFUND_DISCOUNT_AMOUNT,
IN_REFUND_COMMISSION_AMOUNT,
IN_REFUND_WHOLESALE_AMOUNT,
IN_REFUND_WHOLESALE_SERV_FEE,
IN_ORIG_COMPLAINT_COMM_TYPE_ID,
IN_NOTI_COMPLAINT_COMM_TYPE_ID,
IN_TAX1_NAME,
IN_TAX1_AMOUNT,
IN_TAX1_DESCRIPTION,
IN_TAX1_RATE,
IN_TAX2_NAME,
IN_TAX2_AMOUNT,
IN_TAX2_DESCRIPTION,
IN_TAX2_RATE,
IN_TAX3_NAME,
IN_TAX3_AMOUNT,
IN_TAX3_DESCRIPTION,
IN_TAX3_RATE,
IN_TAX4_NAME,
IN_TAX4_AMOUNT,
IN_TAX4_DESCRIPTION,
IN_TAX4_RATE,
IN_TAX5_NAME,
IN_TAX5_AMOUNT,
IN_TAX5_DESCRIPTION,
IN_TAX5_RATE,
0,
OUT_REFUND_ID,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_REFUND;

PROCEDURE INSERT_REFUND
(
IN_REFUND_DISP_CODE             IN REFUND.REFUND_DISP_CODE%TYPE,
IN_CREATED_BY                   IN REFUND.CREATED_BY%TYPE,
IN_REFUND_PRODUCT_AMOUNT        IN REFUND.REFUND_PRODUCT_AMOUNT%TYPE,
IN_REFUND_ADDON_AMOUNT          IN REFUND.REFUND_ADDON_AMOUNT%TYPE,
IN_REFUND_SERVICE_FEE           IN REFUND.REFUND_SERVICE_FEE%TYPE,
IN_REFUND_TAX                   IN REFUND.REFUND_TAX%TYPE,
IN_ORDER_DETAIL_ID              IN REFUND.ORDER_DETAIL_ID%TYPE,
IN_RESPONSIBLE_PARTY            IN REFUND.RESPONSIBLE_PARTY%TYPE,
IN_REFUND_STATUS                IN REFUND.REFUND_STATUS%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_REFUND_ADMIN_FEE             IN REFUND.REFUND_ADMIN_FEE%TYPE,
IN_REFUND_SHIPPING_FEE          IN REFUND.REFUND_SHIPPING_FEE%TYPE,
IN_REFUND_SERVICE_FEE_TAX       IN REFUND.REFUND_SERVICE_FEE_TAX%TYPE,
IN_REFUND_SHIPPING_TAX          IN REFUND.REFUND_SHIPPING_TAX%TYPE,
IN_REFUND_DISCOUNT_AMOUNT       IN REFUND.REFUND_DISCOUNT_AMOUNT%TYPE,
IN_REFUND_COMMISSION_AMOUNT     IN REFUND.REFUND_COMMISSION_AMOUNT%TYPE,
IN_REFUND_WHOLESALE_AMOUNT      IN REFUND.REFUND_WHOLESALE_AMOUNT%TYPE,
IN_REFUND_WHOLESALE_SERV_FEE    IN REFUND.REFUND_WHOLESALE_SERVICE_FEE%TYPE,
IN_ORIG_COMPLAINT_COMM_TYPE_ID  IN REFUND.ORIGIN_COMPLAINT_COMM_TYPE_ID%TYPE,
IN_NOTI_COMPLAINT_COMM_TYPE_ID  IN REFUND.NOTIF_COMPLAINT_COMM_TYPE_ID%TYPE,
IN_TAX1_NAME                    IN REFUND.TAX1_NAME%TYPE,
IN_TAX1_AMOUNT                  IN REFUND.TAX1_AMOUNT%TYPE,
IN_TAX1_DESCRIPTION             IN REFUND.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN REFUND.TAX1_RATE%TYPE,
IN_TAX2_NAME                    IN REFUND.TAX2_NAME%TYPE,
IN_TAX2_AMOUNT                  IN REFUND.TAX2_AMOUNT%TYPE,
IN_TAX2_DESCRIPTION             IN REFUND.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN REFUND.TAX2_RATE%TYPE,
IN_TAX3_NAME                    IN REFUND.TAX3_NAME%TYPE,
IN_TAX3_AMOUNT                  IN REFUND.TAX3_AMOUNT%TYPE,
IN_TAX3_DESCRIPTION             IN REFUND.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN REFUND.TAX3_RATE%TYPE,
IN_TAX4_NAME                    IN REFUND.TAX4_NAME%TYPE,
IN_TAX4_AMOUNT                  IN REFUND.TAX4_AMOUNT%TYPE,
IN_TAX4_DESCRIPTION             IN REFUND.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN REFUND.TAX4_RATE%TYPE,
IN_TAX5_NAME                    IN REFUND.TAX5_NAME%TYPE,
IN_TAX5_AMOUNT                  IN REFUND.TAX5_AMOUNT%TYPE,
IN_TAX5_DESCRIPTION             IN REFUND.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN REFUND.TAX5_RATE%TYPE,
IN_REFUND_ADDON_DISCOUNT_AMT    IN REFUND.REFUND_ADDON_DISCOUNT_AMT%TYPE,
OUT_REFUND_ID                   OUT REFUND.REFUND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a refund record

Input:
        refund_id                       number
        refund_disp_code                varchar2
        created_by                      varchar2
        refund_product_amount           number
        refund_addon_amount             number
        refund_serv_ship_fee            number
        refund_tax                      number
        order_detail_id                 number
        responsible_party               varchar2
        refund_date                     date
        refund_status                   varchar2
        acct_trans_ind                  varchar2 (Y/N to add an accounting transaction record)
        refund_admin_fee                number
        refund_shipping_fee             number
        refund_service_tax              number
        refund_shipping_tax             number
        refund_discount_amount          number
        refund_commission_amount        number
        refund_wholesale_amount         number
        refund_wholesale_service_fee    number
        origin_complaint_comm_type_id   varchar2
        notif_complaint_comm_type_id    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR order_detail_cur IS
        SELECT  delivery_date,
                product_id,
                source_code,
                ship_method
        FROM    order_details
        WHERE   order_detail_id = in_order_detail_id;

odrow   order_detail_cur%rowtype;

BEGIN

-- insert the refund record
INSERT INTO refund
(
        refund_id,
        refund_disp_code,
        created_on,
        created_by,
        updated_on,
        updated_by,
        refund_product_amount,
        refund_addon_amount,
        refund_service_fee,
        refund_tax,
        order_detail_id,
        responsible_party,
        refund_status,
        refund_admin_fee,
        refund_shipping_fee,
        refund_service_fee_tax,
        refund_shipping_tax,
        refund_discount_amount,
        refund_addon_discount_amt,
        refund_commission_amount,
        refund_wholesale_amount,
        refund_wholesale_service_fee,
        origin_complaint_comm_type_id,
        notif_complaint_comm_type_id,
        tax1_name,
        tax1_amount,
        tax1_description,
        tax1_rate,
        tax2_name,
        tax2_amount,
        tax2_description,
        tax2_rate,
        tax3_name,
        tax3_amount,
        tax3_description,
        tax3_rate,
        tax4_name,
        tax4_amount,
        tax4_description,
        tax4_rate,
        tax5_name,
        tax5_amount,
        tax5_description,
        tax5_rate
)
VALUES
(
        refund_id_sq.nextval,
        in_refund_disp_code,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        nvl(in_refund_product_amount, 0),
        nvl(in_refund_addon_amount, 0),
        nvl(in_refund_service_fee, 0),
        nvl(in_refund_tax, 0),
        in_order_detail_id,
        in_responsible_party,
        in_refund_status,
        nvl(in_refund_admin_fee, 0),
        nvl(in_refund_shipping_fee, 0),
        nvl(in_refund_service_fee_tax, 0),
        nvl(in_refund_shipping_tax, 0),
        nvl(in_refund_discount_amount, 0),
        nvl(in_refund_addon_discount_amt, 0),
        nvl(in_refund_commission_amount, 0),
        nvl(in_refund_wholesale_amount, 0),
        nvl(in_refund_wholesale_serv_fee, 0),
        in_orig_complaint_comm_type_id,
        in_noti_complaint_comm_type_id,
        in_tax1_name,
        in_tax1_amount,
        in_tax1_description,
        in_tax1_rate,
        in_tax2_name,
        in_tax2_amount,
        in_tax2_description,
        in_tax2_rate,
        in_tax3_name,
        in_tax3_amount,
        in_tax3_description,
        in_tax3_rate,
        in_tax4_name,
        in_tax4_amount,
        in_tax4_description,
        in_tax4_rate,
        in_tax5_name,
        in_tax5_amount,
        in_tax5_description,
        in_tax5_rate
) RETURNING refund_id INTO out_refund_id;

IF IS_ORDER_FULLY_REFUNDED(in_order_detail_id) = 'Y' THEN
    -- DEFECT 1051: If the order is in queue and is fully refunded, update order_details.eod_delivery_indicator to 'X'
    update order_details od
       set od.eod_delivery_indicator = 'X',
           od.updated_on = sysdate,
           od.updated_by = 'FullRefund'
     where od.order_detail_id = in_order_detail_id
       and od.order_disp_code <> 'Processed'
       and exists (select 'x' from queue q where q.order_detail_id = in_order_detail_id);

    -- DEFECT 714/3118:  If the order is fully refunded, has an order disposition other
    -- than Processed, Printed, or Shipped and a queue item exists, update the order_disp_code
    -- to Processed.  BA decided to leave the logic from defect 1051 intact.
    update order_details od
       set od.order_disp_code = 'Processed',
           od.updated_on = sysdate,
           od.updated_by = 'FullRefund'
     where od.order_detail_id = in_order_detail_id
       and od.order_disp_code <> 'Processed'
       and od.order_disp_code <> 'Printed'
       and od.order_disp_code <> 'Shipped'
       and exists (select 'x' from queue q where q.order_detail_id = in_order_detail_id);
END IF;

-- if specified, add the accounting_transaction record
IF in_acct_trans_ind = 'Y' THEN
        OPEN order_detail_cur;
        FETCH order_detail_cur INTO odrow;
        CLOSE order_detail_cur;

        ORDER_MAINT_PKG.INSERT_ACCOUNTING_TRANSACTIONS
        (
                IN_TRANSACTION_TYPE=>'Refund',
                IN_TRANSACTION_DATE=>sysdate,
                IN_ORDER_DETAIL_ID=>in_order_detail_id,
                IN_DELIVERY_DATE=>odrow.delivery_date,
                IN_PRODUCT_ID=>odrow.product_id,
                IN_SOURCE_CODE=>odrow.source_code,
                IN_SHIP_METHOD=>odrow.ship_method,
                IN_PRODUCT_AMOUNT=>in_refund_product_amount,
                IN_ADD_ON_AMOUNT=>in_refund_addon_amount,
                IN_SHIPPING_FEE=>in_refund_shipping_fee,
                IN_SERVICE_FEE=>in_refund_service_fee,
                IN_DISCOUNT_AMOUNT=>in_refund_discount_amount,
                IN_SHIPPING_TAX=>in_refund_shipping_tax,
                IN_SERVICE_FEE_TAX=>in_refund_service_fee_tax,
                IN_TAX=>in_refund_tax,
                IN_PAYMENT_TYPE=>null,
                IN_REFUND_DISP_CODE=>in_refund_disp_code,
                IN_COMMISSION_AMOUNT=>in_refund_commission_amount,
                IN_WHOLESALE_AMOUNT=>in_refund_wholesale_amount,
                IN_ADMIN_FEE=>in_refund_admin_fee,
                IN_NEW_ORDER_SEQ=>'N',
                IN_REFUND_ID=>out_refund_id,
                IN_ORDER_BILL_ID=>null,
                IN_WHOLESALE_SERVICE_FEE=>in_refund_wholesale_serv_fee,
                IN_ADD_ON_DISCOUNT_AMOUNT=>in_refund_addon_discount_amt,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
ELSE
        out_status := 'Y';
END IF;

/*  inserting the payment record will be handled by the application to account for the case where the
    payment record is tied to both refund and additional bill

INSERT INTO payments
(
        payment_id,
        order_guid,
        payment_type,
        cc_id,
        gc_coupon_number,
        created_on,
        created_by,
        updated_on,
        updated_by,
        debit_amount,
        payment_indicator,
        refund_id
)
SELECT
        key.keygen ('PAYMENTS'),
        order_guid,
        in_payment_type,
        in_cc_id,
        in_gc_coupon_number,
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        in_refund_product_amount + in_refund_addon_amount + in_refund_serv_ship_fee + in_refund_tax,
        'R',
        out_refund_id
FROM    order_details
WHERE   order_detail_id = in_order_detail_id;

*/


EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REFUND;


PROCEDURE UPDATE_REFUND
(
IN_REFUND_ID                    IN REFUND.REFUND_ID%TYPE,
IN_REFUND_DISP_CODE             IN REFUND.REFUND_DISP_CODE%TYPE,
IN_UPDATED_BY                   IN REFUND.UPDATED_BY%TYPE,
IN_REFUND_STATUS                IN REFUND.REFUND_STATUS%TYPE,
IN_REFUND_DATE                  IN REFUND.REFUND_DATE%TYPE,
IN_RESPONSIBLE_PARTY            IN REFUND.RESPONSIBLE_PARTY%TYPE,
IN_ORIG_COMPLAINT_COMM_TYPE_ID  IN REFUND.ORIGIN_COMPLAINT_COMM_TYPE_ID%TYPE,
IN_NOTI_COMPLAINT_COMM_TYPE_ID  IN REFUND.NOTIF_COMPLAINT_COMM_TYPE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a refund record

Input:
        refund_id                       number
        refund_disp_code                varchar2
        updated_by                      varchar2
        refund_status                   varchar2
        refund_date                     date
        responsible_party               varchar2,
        origin_complaint_comm_type_id   varchar2,
        notif_complaint_comm_type_id    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  refund
SET
        refund_disp_code = NVL(in_refund_disp_code, refund_disp_code),
        updated_on = sysdate,
        updated_by = in_updated_by,
        refund_status = NVL(in_refund_status, refund_status),
        refund_date = NVL(in_refund_date, refund_date),
        responsible_party = NVL (in_responsible_party, responsible_party),
        origin_complaint_comm_type_id = in_orig_complaint_comm_type_id,
        notif_complaint_comm_type_id = in_noti_complaint_comm_type_id

WHERE   refund_id = in_refund_id;

-- DEFECT 1983: update the refund disposition code in accounting transactions when the refund disp code changes.
IF in_refund_disp_code IS NOT NULL THEN
        UPDATE  accounting_transactions
        SET     refund_disp_code = in_refund_disp_code
        WHERE   refund_id = in_refund_id;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REFUND;


PROCEDURE DELETE_REFUND
(
IN_REFUND_ID                    IN REFUND.REFUND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting a refund record

Input:
        refund_id                       number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

-- Delete from the associated End of Day tables. Defect 3973.
DELETE FROM aafes_billing_details
WHERE payment_id = (select payment_id
      from payments
           where refund_id = in_refund_id
         );

DELETE FROM alt_pay_billing_detail_bm
WHERE payment_id = (select payment_id
      from payments
           where refund_id = in_refund_id
         );

DELETE FROM alt_pay_billing_detail_pp
WHERE payment_id = (select payment_id
      from payments
           where refund_id = in_refund_id
         );

DELETE FROM alt_pay_billing_detail_ua
WHERE payment_id = (select payment_id
      from payments
           where refund_id = in_refund_id
         );

DELETE FROM alt_pay_billing_detail_gd
WHERE payment_id = (select payment_id
      from payments
           where refund_id = in_refund_id
         );

-- delete the associated accounting transaction record
DELETE FROM accounting_transactions
WHERE   refund_id = in_refund_id;

--delete any payments_ext properties tied to the refund
DELETE FROM payments_ext
WHERE payment_id in (select payment_id
      from payments
           where refund_id = in_refund_id
         );

-- delete any payments tied to the refund
DELETE FROM payments
WHERE   refund_id = in_refund_id;

-- DEFECT 1051: If the order eod_delivery_indicator is 'X', when the refund is removed, reset it
-- to 'N' which was set to 'X' when refund was created.
UPDATE order_details od
       SET od.eod_delivery_indicator = 'N',
           od.updated_on = sysdate,
           od.updated_by = 'DeleteRefund'
     WHERE EXISTS (SELECT 'x'
                     FROM refund r
                    WHERE r.order_detail_id = od.order_detail_id
                      AND r.refund_id = in_refund_id)
       AND od.eod_delivery_indicator = 'X';

-- delete the refund
DELETE FROM refund
WHERE  refund_id = in_refund_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_REFUND;


PROCEDURE VIEW_REFUND_DISPOSITION_VAL
(
IN_REFUND_DISPLAY_TYPE          IN REFUND_DISPOSITION_VAL.REFUND_DISPLAY_TYPE%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving refund dispositions

Input:
        refund_display_type             char - 'C' will display only C type refunds
                                               null will display A and B type refunds

Output:
        cursor containing info from refund_disposition_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                refund_disp_code,
                short_description,
                refund_display_type,
                full_refund_indicator,
                auto_cancel_flag,
                auto_askp_flag,
                auto_cancel_a_flag,
                complaint_comm_type_req_flag
        FROM    refund_disposition_val
        WHERE   status = 'Active'
        AND     (refund_display_type = in_refund_display_type
        OR       refund_display_type IN ('A', 'B', 'C') and in_refund_display_type is null)
        ORDER BY refund_disp_code;

END VIEW_REFUND_DISPOSITION_VAL;


PROCEDURE VIEW_ORDER_FLORISTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving current and previous
        florists used for the given order

Input:
        order_detail_id                 number

Output:
        cursor containing florist ids

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  od.florist_id responsible_party,
                coalesce (
                        (select florist_name from ftd_apps.florist_master f where f.florist_id = od.florist_id),
                        (select vendor_name from ftd_apps.vendor_master v where v.member_number = od.florist_id)
                ) florist_name,
                created_on,
                1 sort_order
        FROM    order_florist_used od
        WHERE   od.order_detail_id = in_order_detail_id
        UNION
        SELECT  responsible_party,
                null florist_name,
                null created_on,
                2 sort_order
        FROM    refund_responsibility
        ORDER BY sort_order, created_on desc, responsible_party;

END VIEW_ORDER_FLORISTS;


PROCEDURE VIEW_PAYMENTS
(
IN_ORDER_DETAIL_ID              IN REFUND.ORDER_DETAIL_ID%TYPE,
OUT_BILL_CUR                    OUT TYPES.REF_CURSOR,
OUT_PAYMENT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the payments for the
        given order

Input:
        order_detail_id                 number

Output:
        cursor containing payment information

-----------------------------------------------------------------------------*/
CURSOR check_gd_cur(in_order_detail_id number) IS
        select 'Y' from clean.payments
        where payment_type = 'GD'
        and payment_indicator = 'P'
        and order_guid = (select order_guid from clean.order_details where order_detail_id = in_order_detail_id);

is_gd_flag CHAR(1) := 'N';

BEGIN

OPEN out_bill_cur FOR
        SELECT  decode (ob.additional_bill_indicator, 'Y', ob.order_bill_id, -999) order_bill_id,
                ob.additional_bill_indicator,
                ob.discount_product_price,
                ob.add_on_amount,
                ob.service_fee + ob.shipping_fee serv_ship_fee,
                ob.shipping_tax + ob.service_fee_tax + ob.tax tax,
                ob.commission_amount,
                ob.product_amount,
                ob.discount_amount,
                ob.add_on_discount_amount,
                ob.wholesale_amount,
                ob.shipping_tax,
                ob.service_fee_tax,
                ob.tax product_tax,
                ob.wholesale_service_fee,
                ob.service_fee,
                ob.shipping_fee,
                ob.miles_points_amt,
                ob.dtl_vendor_sat_upcharge vend_sat_upcharge,
                ob.dtl_ak_hi_special_svc_charge ak_hi_special_charge ,
                ob.dtl_fuel_surcharge_amt fuel_surcharge_amt ,
                od.surcharge_description ,
                od.apply_surcharge_code,
                ob.same_day_upcharge,
                ob.morning_delivery_fee,
                ob.vendor_sun_upcharge vend_sun_upcharge,
                ob.vendor_mon_upcharge vend_mon_upcharge,
                ob.late_cutoff_fee,
				Ob.Dtl_First_Order_International international_fee
        FROM    order_bills ob
        LEFT OUTER JOIN CLEAN.order_details od on ob.order_detail_id = od.order_detail_id
        WHERE   ob.order_detail_id = in_order_detail_id
        ORDER BY decode (ob.additional_bill_indicator, 'Y', ob.order_bill_id, -999);

OPEN check_gd_cur (IN_ORDER_DETAIL_ID);
      FETCH check_gd_cur INTO is_gd_flag;
      IF check_gd_cur%found THEN
      is_gd_flag := 'Y';
      END IF;

IF is_gd_flag = 'Y' THEN
OPEN out_payment_cur FOR
        SELECT  -999 order_bill_id,
                p.payment_type,
                coalesce (to_char (p.cc_id), p.gc_coupon_number) card_id,
                pm.description payment_type_desc,
                decode (p.cc_id, null,
                        p.gc_coupon_number,
                        (select global.encryption.decrypt_it (cc.cc_number,cc.key_name)
                         from   credit_cards cc
                         where  cc.cc_id = p.cc_id)
                ) card_number,
                case when p.payment_type = 'GD' then
                        (
                                select sum(
                                          (select sum(nvl(credit_amount,0)) from clean.payments
                                                                          where payment_type = 'GD'
                                                                          and order_guid = p.order_guid)
                                          -
                                          (select sum(nvl(debit_amount,0)) from clean.payments
                                                                          where payment_type = 'GD'
                                                                          and order_guid = p.order_guid)
                                          +
                                          (select nvl((select sum(nvl(debit_amount,0)) from clean.payments
                                                                          where payment_type = 'GD'
                                                                          and refund_id in (select refund_id from clean.refund where order_detail_id = in_order_detail_id)
                                                                          and order_guid = p.order_guid),0) from dual)
                                          )
                                          from dual
                        )
                     else p.credit_amount
                end credit_amount,
                pm.payment_type payment_type_method,
                (select gc.issue_amount from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) original_gc_coupon_amount,
                (select gc.gc_coupon_status from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) gc_coupon_status,
                decode ((select count (distinct gch.order_detail_id) from gc_coupon_history gch 
                  where gch.gc_coupon_number = p.gc_coupon_number and gch.status = 'Active' and 
                    gch.redemption_date >= nvl ((select max (gch1.reinstate_date) from gc_coupon_history gch1 
                      where gch1.gc_coupon_number = p.gc_coupon_number and gch1.status = 'Active'), gch.redemption_date)), 1, 'N', 0, 'N', 'Y') gc_across_orders,
                p.auth_number,
                p.authorization_transaction_id,
                 decode(p.cc_id, null,null,
                    (select GLOBAL.encryption.DECRYPT_IT(cc.GIFT_CARD_PIN,cc.key_name)
                     from credit_cards cc
                     where cc.cc_id = p.cc_id)) gift_card_pin,
              p.payment_id,
              p.payment_indicator,
              p.bill_status, 
              'N' as was_gc_reinstated,
              p.route,
              p.merchant_ref_id,
              p.settlement_transaction_id,
              p.refund_transaction_id,
              p.REQUEST_TOKEN
          FROM    payments p
        JOIN    ftd_apps.payment_methods pm
        ON      pm.payment_method_id = p.payment_type
        WHERE   p.order_guid = (select order_guid from order_details where order_detail_id = in_order_detail_id)
        AND     p.additional_bill_id is null
        AND     p.refund_id is null
        UNION
        SELECT  p.additional_bill_id order_bill_id,
                p.payment_type,
                coalesce (to_char (p.cc_id), p.gc_coupon_number) card_id,
                pm.description payment_type_desc,
               decode (p.cc_id, null,
                        p.gc_coupon_number,
                        (select global.encryption.decrypt_it (cc.cc_number,cc.key_name)
                         from   credit_cards cc
                         where  cc.cc_id = p.cc_id)
                ) card_number,
                case when p.payment_type = 'GD' then
                        (
                                select sum(
                                          (select sum(nvl(credit_amount,0)) from clean.payments
                                                                          where payment_type = 'GD'
                                                                          and order_guid = p.order_guid)
                                          -
                                          (select sum(nvl(debit_amount,0)) from clean.payments
                                                                          where payment_type = 'GD'
                                                                          and order_guid = p.order_guid)
                                          +
                                          (select nvl((select sum(nvl(debit_amount,0)) from clean.payments
                                                                          where payment_type = 'GD'
                                                                          and refund_id in (select refund_id from clean.refund where order_detail_id = in_order_detail_id)
                                                                          and order_guid = p.order_guid),0) from dual)
                                          )
                                          from dual
                        )
                     else p.credit_amount
                end credit_amount,
                pm.payment_type payment_type_method,
                (select gc.issue_amount from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) original_gc_coupon_amount,
                (select gc.gc_coupon_status from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) gc_coupon_status,
                decode ((select count (distinct gch.order_detail_id) from gc_coupon_history gch 
                  where gch.gc_coupon_number = p.gc_coupon_number and gch.status = 'Active' and 
                    gch.redemption_date >= nvl ((select max (gch1.reinstate_date) from gc_coupon_history gch1 
                      where gch1.gc_coupon_number = p.gc_coupon_number and gch1.status = 'Active'), gch.redemption_date)), 1, 'N', 0, 'N', 'Y') gc_across_orders,
                p.auth_number,
                p.authorization_transaction_id,
                 decode(p.cc_id, null,null,
                    (select GLOBAL.encryption.DECRYPT_IT(cc.GIFT_CARD_PIN,cc.key_name)
                     from credit_cards cc
                     where cc.cc_id = p.cc_id)) gift_card_pin,
                p.payment_id,
                p.payment_indicator,
                p.bill_status,
                'N' as was_gc_reinstated,
                p.route,
              	p.merchant_ref_id,
              	p.settlement_transaction_id,
              	p.refund_transaction_id,
              	p.REQUEST_TOKEN
        FROM    payments p
        JOIN    ftd_apps.payment_methods pm
        ON      pm.payment_method_id = p.payment_type
        WHERE   EXISTS
                 (
                        select  1
                        from    order_bills ob
                        where   ob.order_bill_id = p.additional_bill_id
                        and     ob.order_detail_id = in_order_detail_id
                 )
        ORDER BY order_bill_id;

ELSE
OPEN out_payment_cur FOR
        SELECT  -999 order_bill_id,
                p.payment_type,
                coalesce (to_char (p.cc_id), p.gc_coupon_number) card_id,
                pm.description payment_type_desc,
                decode (p.cc_id, null,
                        p.gc_coupon_number,
                        (select global.encryption.decrypt_it (cc.cc_number,cc.key_name)
                         from   credit_cards cc
                         where  cc.cc_id = p.cc_id)
                ) card_number,
                case when p.payment_type = 'GC' then
                        (
                                select  sum (gch.redemption_amount)
                                from    gc_coupon_history gch
                                where   gch.gc_coupon_number = p.gc_coupon_number
                                and     gch.order_detail_id = in_order_detail_id
                                and     gch.status = 'Active'
                                and     gch.redemption_date is not null
                        )
                     else p.credit_amount
                end credit_amount,
                pm.payment_type payment_type_method,
                (select gc.issue_amount from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) original_gc_coupon_amount,
                (select gc.gc_coupon_status from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) gc_coupon_status,
                decode ((select count (distinct gch.order_detail_id) from gc_coupon_history gch where gch.gc_coupon_number = p.gc_coupon_number and gch.status = 'Active' and gch.redemption_date >= nvl ((select max (gch1.reinstate_date) from gc_coupon_history gch1 where gch1.gc_coupon_number = p.gc_coupon_number and gch1.status = 'Active'), gch.redemption_date)), 1, 'N', 0, 'N', 'Y') gc_across_orders,
                p.auth_number,
                p.authorization_transaction_id,
                decode(p.cc_id, null,null,
                    (select GLOBAL.encryption.DECRYPT_IT(cc.GIFT_CARD_PIN,cc.key_name)
                     from credit_cards cc
                     where cc.cc_id = p.cc_id)) gift_card_pin,
            p.payment_id,
            p.payment_indicator,
            p.bill_status,
            (select decode(count(*),0, 'N', 'Y') from clean.gc_coupon_history gch1 where gch1.gc_coupon_number = p.gc_coupon_number and gch1.order_detail_id = in_order_detail_id and gch1.reinstate_date is not null ) was_gc_reinstated,
            p.route,
            p.merchant_ref_id,
            p.settlement_transaction_id,
            p.refund_transaction_id,
            p.REQUEST_TOKEN
        FROM    payments p
        JOIN    ftd_apps.payment_methods pm
        ON      pm.payment_method_id = p.payment_type
        WHERE   p.order_guid = (select order_guid from order_details where order_detail_id = in_order_detail_id)
        AND     p.additional_bill_id is null
        AND     p.refund_id is null
        UNION
        SELECT  p.additional_bill_id order_bill_id,
                p.payment_type,
                coalesce (to_char (p.cc_id), p.gc_coupon_number) card_id,
                pm.description payment_type_desc,
               decode (p.cc_id, null,
                        p.gc_coupon_number,
                        (select global.encryption.decrypt_it (cc.cc_number,cc.key_name)
                         from   credit_cards cc
                         where  cc.cc_id = p.cc_id)
                ) card_number,
                case when p.payment_type = 'GC' then
                        (
                                select  sum (gch.redemption_amount)
                                from    gc_coupon_history gch
                                where   gch.gc_coupon_number = p.gc_coupon_number
                                and     gch.order_detail_id = in_order_detail_id
                                and     gch.status = 'Active'
                                and     gch.redemption_date is not null
                        )
                     else p.credit_amount
                end credit_amount,
                pm.payment_type payment_type_method,
                (select gc.issue_amount from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) original_gc_coupon_amount,
                (select gc.gc_coupon_status from gc_coupons gc where gc.gc_coupon_number = p.gc_coupon_number) gc_coupon_status,
                decode ((select count (distinct gch.order_detail_id) from gc_coupon_history gch where gch.gc_coupon_number = p.gc_coupon_number and gch.status = 'Active' and gch.redemption_date >= nvl ((select max (gch1.reinstate_date) from gc_coupon_history gch1 where gch1.gc_coupon_number = p.gc_coupon_number and gch1.status = 'Active'), gch.redemption_date)), 1, 'N', 0, 'N', 'Y') gc_across_orders,
                p.auth_number,
                p.authorization_transaction_id,
                 decode(p.cc_id, null,null,
                    (select GLOBAL.encryption.DECRYPT_IT(cc.GIFT_CARD_PIN,cc.key_name)
                     from credit_cards cc
                     where cc.cc_id = p.cc_id)) gift_card_pin,
            p.payment_id,
            p.payment_indicator,
            p.bill_status,
            (select decode(count(*),0, 'N', 'Y') from clean.gc_coupon_history gch1 where gch1.gc_coupon_number = p.gc_coupon_number and gch1.order_detail_id = in_order_detail_id and gch1.reinstate_date is not null ) was_gc_reinstated,
            p.route,
            p.merchant_ref_id,
            p.settlement_transaction_id,
            p.refund_transaction_id,
            p.REQUEST_TOKEN
        FROM    payments p
        JOIN    ftd_apps.payment_methods pm
        ON      pm.payment_method_id = p.payment_type
        WHERE   EXISTS
                 (
                        select  1
                        from    order_bills ob
                        where   ob.order_bill_id = p.additional_bill_id
                        and     ob.order_detail_id = in_order_detail_id
                 )
        ORDER BY order_bill_id;
END IF;

END VIEW_PAYMENTS;


PROCEDURE VIEW_REFUND
(
IN_ORDER_DETAIL_ID              IN REFUND.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the refunds for the
        given order

Input:
        order_detail_id                 number

Output:
        cursor containing refund information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                r.refund_id,
                r.refund_disp_code,
                r.created_by,
                r.refund_product_amount,
                r.refund_addon_amount,
                r.refund_service_fee + r.refund_shipping_fee refund_serv_ship_fee,
                r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax refund_tax,
                r.order_detail_id,
                to_char (r.refund_date, 'mm/dd/yyyy') accounting_date,
                p.payment_type,
                coalesce (to_char (p.cc_id), p.gc_coupon_number) card_id,
                pm.description payment_type_desc,
                decode (p.cc_id, null,
                        p.gc_coupon_number,
                        (select global.encryption.decrypt_it (cc.cc_number,cc.key_name)
                         from   credit_cards cc
                         where  cc.cc_id = p.cc_id)
                ) card_number,
                pm.payment_type payment_type_method,
                r.responsible_party,
                r.refund_status,
                r.refund_admin_fee,
                r.refund_discount_amount,
                r.refund_commission_amount,
                r.refund_wholesale_amount,
                r.refund_service_fee_tax,
                r.refund_shipping_tax,
                r.refund_tax refund_product_tax,
                r.refund_wholesale_service_fee,
                r.refund_service_fee,
                r.refund_shipping_fee,
                p.miles_points_debit_amt,
                r.origin_complaint_comm_type_id,
                r.notif_complaint_comm_type_id,
                p.payment_id,
                p.auth_number,
                p.authorization_transaction_id,
                p.debit_amount,
                p.route
        FROM    refund r
        JOIN    payments p
        ON      r.refund_id = p.refund_id
        JOIN    ftd_apps.payment_methods pm
        ON      pm.payment_method_id = p.payment_type
        WHERE   order_detail_id = in_order_detail_id;

END VIEW_REFUND;


PROCEDURE VIEW_PAYMENT_REFUND
(
IN_ORDER_DETAIL_ID              IN REFUND.ORDER_DETAIL_ID%TYPE,
OUT_BILL_CUR                    OUT TYPES.REF_CURSOR,
OUT_PAYMENT_CUR                 OUT TYPES.REF_CURSOR,
OUT_REFUND_CUR                  OUT TYPES.REF_CURSOR,
OUT_ORDER_CUR                   OUT TYPES.REF_CURSOR,
OUT_ORDER_STATUS_CUR            OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the payments and refunds for the
        given order

Input:
        order_detail_id                 number

Output:
        cursor containing bill information
        cursor containing payment information
        cursor containing refund information
        cursor containing order information

-----------------------------------------------------------------------------*/


BEGIN

view_payments (in_order_detail_id, out_bill_cur, out_payment_cur);
view_refund (in_order_detail_id, out_refund_cur);

order_mesg_pkg.get_order_message_status (in_order_detail_id, null, 'N', out_order_status_cur);

OPEN out_order_cur FOR
        SELECT  od.order_disp_code,
                to_char (od.delivery_date, 'mm/dd/yyyy') delivery_date,
                r.state recipient_state,
                decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                od.external_order_number,
                o.customer_id,
                decode (p.product_type, 'FRECUT', 'Y', 'SDFC', 'Y', 'N') fresh_cut_flag,
                p.product_type,
                od.ship_method,
                o.mp_redemption_rate_amt,
                o.company_id,
                r.country recipient_country
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    customer r
        ON      od.recipient_id = r.customer_id
        JOIN    ftd_apps.product_master p
        ON      od.product_id = p.product_id
        WHERE   order_detail_id = in_order_detail_id;

END VIEW_PAYMENT_REFUND;


PROCEDURE VIEW_DAILY_REFUNDS
(
IN_CSR_ID                       IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_SORT_COLUMN_NAME             IN VARCHAR2,
IN_SORT_COLUMN_STATUS           IN VARCHAR2,
IN_USER_ID_FILTER               IN VARCHAR2,
IN_GROUP_ID_FILTER              IN VARCHAR2,
IN_CODES_FILTER                 IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_NUMBER_OF_RECORDS           OUT NUMBER,
OUT_CODES_STRING                OUT VARCHAR2
)
AS

v_SQL VARCHAR2(2000);
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for all refunds submitted on the current
        date

Input:
        csr_id                          varchar2

Output:
        cursor containing refund information

-----------------------------------------------------------------------------*/

CURSOR user_cur IS
        SELECT  u.call_center_id,
                u.cs_group_id
        FROM    aas.users u
        JOIN    aas.identity i
        ON      u.user_id = i.user_id
        WHERE   i.identity_id = in_csr_id;

v_call_center_id                aas.users.call_center_id%type;
v_cs_group_id                   aas.users.cs_group_id%type;


TYPE cursor_ref IS REF CURSOR;
id_cur cursor_ref;

-- Fetching codes                 
CURSOR codes_cur (p_call_center_id varchar2, p_cs_group_id varchar2) IS
        SELECT  r.refund_disp_code
        FROM    refund r
        JOIN    aas.identity i
        ON      r.created_by = i.identity_id
        JOIN    aas.users u
        ON      i.user_id = u.user_id
        WHERE   r.refund_status = 'Unbilled'
        AND     trunc(r.created_on) = trunc (SYSDATE)
        AND     r.reviewed_by is null
        GROUP BY  r.refund_disp_code
        ORDER BY r.refund_disp_code;

-- array for the results of the dynamic sql statement
id_tab                  id_list_pkg.number_tab_typ;
amount_tab              id_list_pkg.number_tab_typ;
-- This is for codes string
codes_tab               id_list_pkg.varchar_tab_typ;

-- string to store the list of ids
v_str_id                varchar2(32767);

--string to store refund disp codes.
v_codes_string          varchar2(32767);

-- number to store the calculated start position
start_position          NUMBER;

-- number to start position of codes
code_start_position          NUMBER;

v_filters_string        VARCHAR2(4000) :=' ';
v_query_string        VARCHAR2(4000);

BEGIN

-- Filters Data building
IF IN_USER_ID_FILTER != ' ' THEN
   v_filters_string := ' and r.created_by ='''||IN_USER_ID_FILTER||'''';
END IF;

IF IN_GROUP_ID_FILTER != ' ' THEN
   v_filters_string := v_filters_string || '  ' ||  'and u.cs_group_id ='''||IN_GROUP_ID_FILTER||''''; 
END IF;

IF IN_CODES_FILTER != ' ' THEN
  v_filters_string := v_filters_string || '  ' ||  'and refund_disp_code in(' ||IN_CODES_FILTER|| ')';
END IF;


-- get the call center and group id for the current user
OPEN user_cur;
FETCH user_cur INTO v_call_center_id, v_cs_group_id;
CLOSE user_cur;

-- Dynamic query string for fetching refund ids based on given filter from refund review screen. 
v_query_string :=
        'SELECT  r.refund_id,
        r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax refund_total
        FROM    refund r
        JOIN    aas.identity i
        ON      r.created_by = i.identity_id
        JOIN    aas.users u
        ON      i.user_id = u.user_id
        WHERE   r.refund_status = ''Unbilled''
        AND     trunc(r.created_on) = trunc (SYSDATE)
        AND     r.reviewed_by is null' ||'  '||v_filters_string||'  '||
        'ORDER BY  '|| IN_SORT_COLUMN_NAME ||'  '||IN_SORT_COLUMN_STATUS ||',decode (:1, u.call_center_id, ''---'', u.call_center_id),
                decode (:2, u.cs_group_id, ''---'', u.cs_group_id),
                 created_by';
                 
-- get all of the daily refund ids
-- open id_cur (v_call_center_id, v_cs_group_id,v_filters_string);
OPEN id_cur FOR v_query_string  USING v_call_center_id, v_cs_group_id;
FETCH id_cur BULK COLLECT INTO id_tab,amount_tab;
close id_cur;

-- get list of all refund disp codes to populate in refund review screen.
open codes_cur (v_call_center_id, v_cs_group_id);
fetch codes_cur bulk collect into codes_tab;
close codes_cur;

-- set start position equal to start position passed in
start_position := in_start_position;
-- If the start position is larger than the refund count the position/page
-- no longer exists.  We must revert back to the last page that exists,
-- hence start position - in_max_number_returned equals the previous page.
WHILE start_position > id_tab.count
LOOP
  start_position := start_position - in_max_number_returned;
END LOOP;

-- If the id_tab.count is 0 the start_position will become negative.
-- The start_position should be set back to 1.
-- To be safe the start_position is tested for a negative value after the loop
-- instead of testing that id_tab.count equals 0, bypassing the loop,
-- and setting start_position equal to 1.
IF start_position < 1 THEN
  start_position := 1;
END IF;

-- get the page ids in a string
id_list_pkg.get_str_id (id_tab, start_position, in_max_number_returned, v_str_id);

IF v_str_id IS NULL THEN
        v_str_id := '-1';
END IF;

code_start_position :=1;
 
-- get the codes in a string
id_list_pkg.get_str_id (codes_tab, code_start_position, codes_tab.count, v_codes_string);

OUT_NUMBER_OF_RECORDS := id_tab.count;
OUT_CODES_STRING := v_codes_string;


-- the cursor returns the records requested by the start position and number of records returned    
OPEN out_cur FOR
     'SELECT  r.refund_id,
                od.external_order_number,
                r.created_by created_by,
                decode (clean.order_query_pkg.is_order_cancelled(r.order_detail_id), ''Y'', ''CAN'', ''FTD'') status,
                clean.order_mesg_pkg.get_message_price_by_order(r.order_detail_id) message_value,
                r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax refund_total,
                r.refund_product_amount,
                r.refund_addon_amount,
                r.refund_service_fee + r.refund_shipping_fee refund_serv_ship_fee,
                r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax refund_tax,
                to_char (o.order_date, ''mm/dd'') order_date,
                r.refund_disp_code,
                r.reviewed_by,
                r.order_detail_id,
                u.call_center_id,
                u.cs_group_id,
                o.order_guid,
                r.refund_admin_fee,
                temp_table.partner_name preferred_partner_name,
                temp_table.csr_can_view_pref_partner,
                temp_table.display_name partner_display_name   
        FROM    refund r
        JOIN    order_details od
        ON      r.order_detail_id = od.order_detail_id
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    aas.identity i
        ON      r.created_by = i.identity_id
        JOIN    aas.users u
        ON      i.user_id = u.user_id
        LEFT OUTER JOIN
                (select  distinct spr.source_code, pm.partner_name, pm.preferred_processing_resource, pm.display_name, aas.authenticate_pkg.IS_CSR_IN_RESOURCE (''' || IN_CSR_ID || ''', pm.PREFERRED_PROCESSING_RESOURCE, ''View'') csr_can_view_pref_partner
                   from  ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
                  where  pp.program_name = spr.program_name
                    and  pm.partner_name = pp.partner_name
                    and  pm.preferred_partner_flag = ''Y''
                ) temp_table
        ON      od.source_code = temp_table.source_code
        WHERE   r.refund_id IN (' || v_str_id || ') ' ||' '||v_filters_string||' '||
        ' ORDER BY  '|| IN_SORT_COLUMN_NAME ||'  '||IN_SORT_COLUMN_STATUS ||', decode (:1, u.call_center_id, ''---'', u.call_center_id),
                 decode (:2, u.cs_group_id, ''---'', u.cs_group_id),
                 created_by'
        USING v_call_center_id, v_cs_group_id;
       
END VIEW_DAILY_REFUNDS;

PROCEDURE UPDATE_REFUND_ACCTG_STATUS
(
 IN_ORDER_DETAIL_ID     IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 IN_REFUND_STATUS       IN REFUND.REFUND_STATUS%TYPE,
 IN_CSR_ID              IN VARCHAR2,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates refund.refund_status to the status
         passed in for the order detail id passed in.

Input:
        order_detail_id       NUMBER
        bill_status           VARCHAR2
        csr_id                VARCHAR2

Output:
        status          varchar2 (Y or N)
        message         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE payments
     SET bill_status = in_refund_status,
     bill_date = decode(in_refund_status,'Billed',SYSDATE,null),
         updated_on = SYSDATE,
         updated_by = in_csr_id
   WHERE order_guid = (select order_guid from clean.order_details where order_detail_id = in_order_detail_id);

  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No refund records were updated using order_detail_id.';
  END IF;


  EXCEPTION WHEN OTHERS THEN
    BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;
END UPDATE_REFUND_ACCTG_STATUS;



PROCEDURE UPDATE_REFUND_ACCTG_STATUS
(
 IN_EXTERNAL_ORDER_NUMBER IN ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
 IN_REFUND_STATUS         IN REFUND.REFUND_STATUS%TYPE,
 OUT_STATUS            OUT VARCHAR2,
 OUT_MESSAGE           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates refund.refund_status to the status
         passed in for the external order number passed in.

Input:
        external_order_number VARCHAR2
        bill_status           VARCHAR2

Output:
        status          varchar2 (Y or N)
        message         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

  UPDATE payments
     SET bill_status = in_refund_status,
         updated_on = SYSDATE,
         updated_by = USER
   WHERE order_guid = (select order_guid from clean.order_details where external_order_number = IN_EXTERNAL_ORDER_NUMBER);

  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No refund records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
    BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;


END UPDATE_REFUND_ACCTG_STATUS;

PROCEDURE UPDATE_REFUND_REVIEWED_BY
(
IN_REFUND_ID                    IN REFUND.REFUND_ID%TYPE,
IN_REVIEWED_BY                  IN REFUND.REVIEWED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating a refund record reviewed
        by field

Input:
        refund_id                       number
        reviewed_by                     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  refund
SET
        reviewed_on = sysdate,
        reviewed_by = in_reviewed_by
WHERE   refund_id = in_refund_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REFUND_REVIEWED_BY;


PROCEDURE VIEW_ORDER_BILLS_BY_PAYMENT
(
IN_PAYMENT_ID                   IN PAYMENTS.PAYMENT_ID%TYPE,
OUT_BILL_CUR                    OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting the bills that are associated
        to the given payment

Input:
        payment_id                  number

Output:
        cursor containing order bill information

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_bill_cur FOR
        SELECT  ob.order_detail_id,
                ob.additional_bill_indicator,
                ob.discount_product_price product_amount,
                ob.add_on_amount,
                ob.service_fee + ob.shipping_fee serv_ship_fee,
                ob.shipping_tax + ob.tax + nvl (ob.service_fee_tax, 0) tax,
                ob.commission_amount
        FROM    order_bills ob
        JOIN    order_details od
        ON      ob.order_detail_id = od.order_detail_id
        JOIN    payments p
        ON      p.additional_bill_id = ob.order_bill_id
        OR      (p.additional_bill_id IS NULL and p.order_guid = od.order_guid)
        WHERE   p.payment_id = in_payment_id
        AND     p.payment_indicator IN ('P', 'B')
        ORDER BY decode (ob.additional_bill_indicator, 'Y', ob.order_bill_id, -999);

END VIEW_ORDER_BILLS_BY_PAYMENT;


FUNCTION IS_ORDER_FULLY_REFUNDED
(
IN_ORDER_DETAIL_ID              ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if a order is
        fully refunded.

Input:
        order_detail_id                 number

Output:
        Y/N if the order is fully refunded

-----------------------------------------------------------------------------*/

v_cur                   types.ref_cursor;
v_order_detail_id       integer;
v_total_type            varchar2 (30);
v_product_amount        number;
v_add_on_amount         number;
v_serv_ship_fee         number;
v_discount_amount       number;
v_tax                   number;
v_order_total           number;
v_miles_points_total    number;
v_addon_discount_amt    number;

v_return                varchar2(1) := 'N';

BEGIN

-- get the order modified order totals (bills - refunds)
order_query_pkg.get_order_totals (to_char (in_order_detail_id), v_cur);

FETCH v_cur INTO
        v_order_detail_id,
        v_total_type,
        v_product_amount,
        v_add_on_amount,
        v_serv_ship_fee,
        v_discount_amount,
        v_tax,
        v_order_total,
        v_miles_points_total,
        v_addon_discount_amt;
CLOSE v_cur;

-- if the order total is 0, then the order is fully refunded
IF v_order_total = 0 THEN
        v_return := 'Y';
END IF;

RETURN v_return;

END IS_ORDER_FULLY_REFUNDED;



PROCEDURE IS_AUTO_REFUND_ALLOWED
(
IN_ORDER_DETAIL_ID              IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure helps determine if an order can be automatically
        refunded.  This is used when a customer cancels their order through
        the customer service website.  Requirements state FTDC orders and
        orders with Gift Certificates cannot be automatically refunded.

Input:
        in_order_detail_id      varchar2

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (Reason why auto-refund not allowed
                                          if N status returned, null otherwise)

-----------------------------------------------------------------------------*/
CURSOR check_comp IS
        SELECT  count(*)
        FROM    mercury.mercury m
        WHERE   m.reference_number = in_order_detail_id
        AND     m.msg_type = 'FTD'
        AND     m.comp_order IS NOT NULL;

CURSOR check_payments IS
       SELECT DECODE(p.payment_type, 'GC', 'Y', 'N') AS gc_payment
       FROM clean.payments p, clean.order_details od
       WHERE p.order_guid = od.order_guid
       AND od.order_detail_id = in_order_detail_id
       ORDER BY gc_payment ASC;

v_comp_count    integer;
v_payment_found varchar2(10)  := 'N';

BEGIN

out_status := 'Y';  -- Assume success

OPEN check_comp;
FETCH check_comp INTO v_comp_count;
CLOSE check_comp;

IF v_comp_count > 0 THEN
      out_status  := 'N';
      out_message := 'Auto refund not allowed since comp order';
ELSE
      FOR payment_rec IN check_payments LOOP
        v_payment_found := 'Y';
        IF payment_rec.gc_payment = 'Y' THEN
            out_status  := 'N';
            out_message := 'Auto refund not allowed on orders with gift certs';
            EXIT;
        END IF;
      END LOOP;
      IF v_payment_found = 'N' THEN
         out_status  := 'N';
         out_message := 'Auto refund not allowed since no payments found';
      END IF;

END IF;

EXCEPTION WHEN OTHERS THEN
      BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      END;

END IS_AUTO_REFUND_ALLOWED;




PROCEDURE VIEW_COMPLAINT_COMM_TYPE
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure will select all of the avalable values from the
        CLEAN.COMPLAINT_COMM_TYPE table

Input:
      none

Output:
        complaint_comm_types                  ref_cursor with all of the complaint_comm_types
--------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT COMPLAINT_COMM_TYPE_ID, DESCRIPTION
        FROM CLEAN.COMPLAINT_COMM_TYPE;


END VIEW_COMPLAINT_COMM_TYPE;

FUNCTION IS_FULLY_REFUNDED_ORDER
(
IN_MASTER_ORDER_NUMBER             ORDERS.MASTER_ORDER_NUMBER%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
DESCRIPTION:
	CHECKS IS THE COMPLETE ORDER IS REFUNDED
INPUT:
        MASTER_ORDER_NUMBER            VARCHAR2

OUTPUT:
        Y/N IF THE ORDER IS FULLY REFUNDED

-----------------------------------------------------------------------------*/

V_ORDER_EXISTS			    VARCHAR2(1);

CURSOR REFUND_CUR IS
	SELECT DISTINCT CLEAN.REFUND_PKG.IS_ORDER_FULLY_REFUNDED(OD.ORDER_DETAIL_ID) IS_FULLREFUND
            FROM CLEAN.ORDER_DETAILS OD, CLEAN.ORDERS O
			WHERE OD.ORDER_GUID = O.ORDER_GUID AND O.MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;
			
BEGIN
  
	SELECT 'Y' INTO v_order_exists FROM CLEAN.ORDERS O WHERE MASTER_ORDER_NUMBER = IN_MASTER_ORDER_NUMBER;
	
	IF SQL%NOTFOUND THEN
		V_ORDER_EXISTS:='N';
	END IF;
	
	IF v_order_exists = 'Y' THEN
		FOR REC IN REFUND_CUR LOOP
			IF REC.IS_FULLREFUND = 'N' THEN 
        RETURN 'N';
      END IF;
		END LOOP;     
	END IF;

RETURN 'Y';

END IS_FULLY_REFUNDED_ORDER;

PROCEDURE UPDATE_REFUND
(
IN_REFUND_STATUS       IN REFUND.REFUND_STATUS%TYPE,
IN_UPDATED_BY                IN REFUND.UPDATED_BY%TYPE,
IN_REFUND_TAX         IN REFUND.REFUND_TAX%TYPE,
IN_REFUND_SHIPPING_TAX       IN REFUND.REFUND_SHIPPING_TAX%TYPE,
IN_REFUND_ID         IN REFUND.REFUND_ID%TYPE,
OUT_STATUS                   OUT VARCHAR2,
OUT_MESSAGE                  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Update refund status, tax, shipping tax
-----------------------------------------------------------------------------*/
BEGIN

  UPDATE CLEAN.REFUND SET REFUND_STATUS = IN_REFUND_STATUS, 
  REFUND_DATE = SYSDATE, 
  REFUND_TAX = IN_REFUND_TAX,
  REFUND_SHIPPING_TAX = IN_REFUND_SHIPPING_TAX, 
  UPDATED_ON = SYSDATE, 
  UPDATED_BY = IN_UPDATED_BY
     WHERE REFUND_ID = IN_REFUND_ID;

 OUT_STATUS := 'Y';
 
EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_REFUND [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REFUND;

PROCEDURE GET_PAYMENT_ROUTE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the route information of the order received
        
Input:
        order detail Id                      varchar2

Output:
        route information

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
  	SELECT 
  	  ROUTE 
  	FROM 
  	  PAYMENTS 
  	WHERE ORDER_GUID = (SELECT ORDER_GUID FROM ORDER_DETAILS WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID)
  	  AND PAYMENT_INDICATOR='P'
  	  AND ADDITIONAL_BILL_ID IS NULL;

END GET_PAYMENT_ROUTE;

END;
.
/