CREATE OR REPLACE TRIGGER clean.refund_$
AFTER INSERT OR UPDATE OR DELETE ON clean.refund REFERENCING OLD AS OLD NEW AS NEW FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN

      INSERT INTO clean.refund$ (
          REFUND_ID,                   
          REFUND_DISP_CODE,            
          PERCENT,                     
          CREATED_ON,                  
          CREATED_BY,                  
          UPDATED_ON,                  
          UPDATED_BY,                  
          REFUND_PRODUCT_AMOUNT,       
          REFUND_ADDON_AMOUNT,         
          REFUND_SERVICE_FEE,          
          REFUND_TAX,                  
          ORDER_DETAIL_ID,             
          RESPONSIBLE_PARTY,           
          REFUND_STATUS,               
          REFUND_DATE,                 
          REVIEWED_ON,                 
          REVIEWED_BY,                 
          SETTLED_DATE,                
          REFUND_ADMIN_FEE,            
          REFUND_SHIPPING_FEE,         
          REFUND_SERVICE_FEE_TAX,      
          REFUND_SHIPPING_TAX,         
          REFUND_DISCOUNT_AMOUNT,      
          REFUND_COMMISSION_AMOUNT,    
          REFUND_WHOLESALE_AMOUNT,     
          REFUND_WHOLESALE_SERVICE_FEE,
          POP_REFUND_FLAG,             
          AP_REFUND_TRANS_TXT,         
          OPERATION$,                  
          TIMESTAMP$,
          origin_complaint_comm_type_id, 
          notif_complaint_comm_type_id,
          tax1_name,
          tax1_description,
          tax1_rate,
          tax1_amount,
          tax2_name,
          tax2_description,
          tax2_rate,
          tax2_amount,
          tax3_name,
          tax3_description,
          tax3_rate,
          tax3_amount,
          tax4_name,
          tax4_description,
          tax4_rate,
          tax4_amount,
          tax5_name,
          tax5_description,
          tax5_rate,
          tax5_amount,
          REFUND_ADDON_DISCOUNT_AMT
      )  VALUES (
          :NEW.REFUND_ID,                   
          :NEW.REFUND_DISP_CODE,            
          :NEW.PERCENT,                     
          :NEW.CREATED_ON,                  
          :NEW.CREATED_BY,                  
          :NEW.UPDATED_ON,                  
          :NEW.UPDATED_BY,                  
          :NEW.REFUND_PRODUCT_AMOUNT,       
          :NEW.REFUND_ADDON_AMOUNT,         
          :NEW.REFUND_SERVICE_FEE,          
          :NEW.REFUND_TAX,                  
          :NEW.ORDER_DETAIL_ID,             
          :NEW.RESPONSIBLE_PARTY,           
          :NEW.REFUND_STATUS,               
          :NEW.REFUND_DATE,                 
          :NEW.REVIEWED_ON,                 
          :NEW.REVIEWED_BY,                 
          :NEW.SETTLED_DATE,                
          :NEW.REFUND_ADMIN_FEE,            
          :NEW.REFUND_SHIPPING_FEE,         
          :NEW.REFUND_SERVICE_FEE_TAX,      
          :NEW.REFUND_SHIPPING_TAX,         
          :NEW.REFUND_DISCOUNT_AMOUNT,      
          :NEW.REFUND_COMMISSION_AMOUNT,    
          :NEW.REFUND_WHOLESALE_AMOUNT,     
          :NEW.REFUND_WHOLESALE_SERVICE_FEE,
          :NEW.POP_REFUND_FLAG,             
          :NEW.AP_REFUND_TRANS_TXT,         
          'INS',
           v_current_timestamp,
          :NEW.origin_complaint_comm_type_id, 
          :NEW.notif_complaint_comm_type_id,
          :NEW.tax1_name,
          :NEW.tax1_description,
          :NEW.tax1_rate,
          :NEW.tax1_amount,
          :NEW.tax2_name,
          :NEW.tax2_description,
          :NEW.tax2_rate,
          :NEW.tax2_amount,
          :NEW.tax3_name,
          :NEW.tax3_description,
          :NEW.tax3_rate,
          :NEW.tax3_amount,
          :NEW.tax4_name,
          :NEW.tax4_description,
          :NEW.tax4_rate,
          :NEW.tax4_amount,
          :NEW.tax5_name,
          :NEW.tax5_description,
          :NEW.tax5_rate,
          :NEW.tax5_amount,
          :NEW.REFUND_ADDON_DISCOUNT_AMT
);

   ELSIF UPDATING THEN

      INSERT INTO clean.refund$ (
          REFUND_ID,                   
          REFUND_DISP_CODE,            
          PERCENT,                     
          CREATED_ON,                  
          CREATED_BY,                  
          UPDATED_ON,                  
          UPDATED_BY,                  
          REFUND_PRODUCT_AMOUNT,       
          REFUND_ADDON_AMOUNT,         
          REFUND_SERVICE_FEE,          
          REFUND_TAX,                  
          ORDER_DETAIL_ID,             
          RESPONSIBLE_PARTY,           
          REFUND_STATUS,               
          REFUND_DATE,                 
          REVIEWED_ON,                 
          REVIEWED_BY,                 
          SETTLED_DATE,                
          REFUND_ADMIN_FEE,            
          REFUND_SHIPPING_FEE,         
          REFUND_SERVICE_FEE_TAX,      
          REFUND_SHIPPING_TAX,         
          REFUND_DISCOUNT_AMOUNT,      
          REFUND_COMMISSION_AMOUNT,    
          REFUND_WHOLESALE_AMOUNT,     
          REFUND_WHOLESALE_SERVICE_FEE,
          POP_REFUND_FLAG,             
          AP_REFUND_TRANS_TXT,         
          OPERATION$,                  
          TIMESTAMP$,
          origin_complaint_comm_type_id, 
	  notif_complaint_comm_type_id,
          tax1_name,
          tax1_description,
          tax1_rate,
          tax1_amount,
          tax2_name,
          tax2_description,
          tax2_rate,
          tax2_amount,
          tax3_name,
          tax3_description,
          tax3_rate,
          tax3_amount,
          tax4_name,
          tax4_description,
          tax4_rate,
          tax4_amount,
          tax5_name,
          tax5_description,
          tax5_rate,
          tax5_amount,
          REFUND_ADDON_DISCOUNT_AMT
      )  VALUES (
          :OLD.REFUND_ID,                   
          :OLD.REFUND_DISP_CODE,            
          :OLD.PERCENT,                     
          :OLD.CREATED_ON,                  
          :OLD.CREATED_BY,                  
          :OLD.UPDATED_ON,                  
          :OLD.UPDATED_BY,                  
          :OLD.REFUND_PRODUCT_AMOUNT,       
          :OLD.REFUND_ADDON_AMOUNT,         
          :OLD.REFUND_SERVICE_FEE,          
          :OLD.REFUND_TAX,                  
          :OLD.ORDER_DETAIL_ID,             
          :OLD.RESPONSIBLE_PARTY,           
          :OLD.REFUND_STATUS,               
          :OLD.REFUND_DATE,                 
          :OLD.REVIEWED_ON,                 
          :OLD.REVIEWED_BY,                 
          :OLD.SETTLED_DATE,                
          :OLD.REFUND_ADMIN_FEE,            
          :OLD.REFUND_SHIPPING_FEE,         
          :OLD.REFUND_SERVICE_FEE_TAX,      
          :OLD.REFUND_SHIPPING_TAX,         
          :OLD.REFUND_DISCOUNT_AMOUNT,      
          :OLD.REFUND_COMMISSION_AMOUNT,    
          :OLD.REFUND_WHOLESALE_AMOUNT,     
          :OLD.REFUND_WHOLESALE_SERVICE_FEE,
          :OLD.POP_REFUND_FLAG,             
          :OLD.AP_REFUND_TRANS_TXT,         
          'UPD_OLD',
          v_current_timestamp,
          :OLD.origin_complaint_comm_type_id, 
          :OLD.notif_complaint_comm_type_id,
          :OLD.tax1_name,
          :OLD.tax1_description,
          :OLD.tax1_rate,
          :OLD.tax1_amount,
          :OLD.tax2_name,
          :OLD.tax2_description,
          :OLD.tax2_rate,
          :OLD.tax2_amount,
          :OLD.tax3_name,
          :OLD.tax3_description,
          :OLD.tax3_rate,
          :OLD.tax3_amount,
          :OLD.tax4_name,
          :OLD.tax4_description,
          :OLD.tax4_rate,
          :OLD.tax4_amount,
          :OLD.tax5_name,
          :OLD.tax5_description,
          :OLD.tax5_rate,
          :OLD.tax5_amount,
          :OLD.REFUND_ADDON_DISCOUNT_AMT
);

      INSERT INTO clean.refund$ (
          REFUND_ID,                   
          REFUND_DISP_CODE,            
          PERCENT,                     
          CREATED_ON,                  
          CREATED_BY,                  
          UPDATED_ON,                  
          UPDATED_BY,                  
          REFUND_PRODUCT_AMOUNT,       
          REFUND_ADDON_AMOUNT,         
          REFUND_SERVICE_FEE,          
          REFUND_TAX,                  
          ORDER_DETAIL_ID,             
          RESPONSIBLE_PARTY,           
          REFUND_STATUS,               
          REFUND_DATE,                 
          REVIEWED_ON,                 
          REVIEWED_BY,                 
          SETTLED_DATE,                
          REFUND_ADMIN_FEE,            
          REFUND_SHIPPING_FEE,         
          REFUND_SERVICE_FEE_TAX,      
          REFUND_SHIPPING_TAX,         
          REFUND_DISCOUNT_AMOUNT,      
          REFUND_COMMISSION_AMOUNT,    
          REFUND_WHOLESALE_AMOUNT,     
          REFUND_WHOLESALE_SERVICE_FEE,
          POP_REFUND_FLAG,             
          AP_REFUND_TRANS_TXT,         
          OPERATION$,                  
          TIMESTAMP$,
          origin_complaint_comm_type_id, 
	  notif_complaint_comm_type_id,
          tax1_name,
          tax1_description,
          tax1_rate,
          tax1_amount,
          tax2_name,
          tax2_description,
          tax2_rate,
          tax2_amount,
          tax3_name,
          tax3_description,
          tax3_rate,
          tax3_amount,
          tax4_name,
          tax4_description,
          tax4_rate,
          tax4_amount,
          tax5_name,
          tax5_description,
          tax5_rate,
          tax5_amount,
          REFUND_ADDON_DISCOUNT_AMT

      )  VALUES (
          :NEW.REFUND_ID,                   
          :NEW.REFUND_DISP_CODE,            
          :NEW.PERCENT,                     
          :NEW.CREATED_ON,                  
          :NEW.CREATED_BY,                  
          :NEW.UPDATED_ON,                  
          :NEW.UPDATED_BY,                  
          :NEW.REFUND_PRODUCT_AMOUNT,       
          :NEW.REFUND_ADDON_AMOUNT,         
          :NEW.REFUND_SERVICE_FEE,          
          :NEW.REFUND_TAX,                  
          :NEW.ORDER_DETAIL_ID,             
          :NEW.RESPONSIBLE_PARTY,           
          :NEW.REFUND_STATUS,               
          :NEW.REFUND_DATE,                 
          :NEW.REVIEWED_ON,                 
          :NEW.REVIEWED_BY,                 
          :NEW.SETTLED_DATE,                
          :NEW.REFUND_ADMIN_FEE,            
          :NEW.REFUND_SHIPPING_FEE,         
          :NEW.REFUND_SERVICE_FEE_TAX,      
          :NEW.REFUND_SHIPPING_TAX,         
          :NEW.REFUND_DISCOUNT_AMOUNT,      
          :NEW.REFUND_COMMISSION_AMOUNT,    
          :NEW.REFUND_WHOLESALE_AMOUNT,     
          :NEW.REFUND_WHOLESALE_SERVICE_FEE,
          :NEW.POP_REFUND_FLAG,             
          :NEW.AP_REFUND_TRANS_TXT,         
          'UPD_NEW',
          v_current_timestamp,
          :NEW.origin_complaint_comm_type_id, 
          :NEW.notif_complaint_comm_type_id,
          :NEW.tax1_name,
          :NEW.tax1_description,
          :NEW.tax1_rate,
          :NEW.tax1_amount,
          :NEW.tax2_name,
          :NEW.tax2_description,
          :NEW.tax2_rate,
          :NEW.tax2_amount,
          :NEW.tax3_name,
          :NEW.tax3_description,
          :NEW.tax3_rate,
          :NEW.tax3_amount,
          :NEW.tax4_name,
          :NEW.tax4_description,
          :NEW.tax4_rate,
          :NEW.tax4_amount,
          :NEW.tax5_name,
          :NEW.tax5_description,
          :NEW.tax5_rate,
          :NEW.tax5_amount,
          :NEW.REFUND_ADDON_DISCOUNT_AMT
);
   ELSIF DELETING THEN
      INSERT INTO clean.refund$ (
          REFUND_ID,                   
          REFUND_DISP_CODE,            
          PERCENT,                     
          CREATED_ON,                  
          CREATED_BY,                  
          UPDATED_ON,                  
          UPDATED_BY,                  
          REFUND_PRODUCT_AMOUNT,       
          REFUND_ADDON_AMOUNT,         
          REFUND_SERVICE_FEE,          
          REFUND_TAX,                  
          ORDER_DETAIL_ID,             
          RESPONSIBLE_PARTY,           
          REFUND_STATUS,               
          REFUND_DATE,                 
          REVIEWED_ON,                 
          REVIEWED_BY,                 
          SETTLED_DATE,                
          REFUND_ADMIN_FEE,            
          REFUND_SHIPPING_FEE,         
          REFUND_SERVICE_FEE_TAX,      
          REFUND_SHIPPING_TAX,         
          REFUND_DISCOUNT_AMOUNT,      
          REFUND_COMMISSION_AMOUNT,    
          REFUND_WHOLESALE_AMOUNT,     
          REFUND_WHOLESALE_SERVICE_FEE,
          POP_REFUND_FLAG,             
          AP_REFUND_TRANS_TXT,         
          OPERATION$,                  
          TIMESTAMP$,
          origin_complaint_comm_type_id, 
	  notif_complaint_comm_type_id,
          tax1_name,
          tax1_description,
          tax1_rate,
          tax1_amount,
          tax2_name,
          tax2_description,
          tax2_rate,
          tax2_amount,
          tax3_name,
          tax3_description,
          tax3_rate,
          tax3_amount,
          tax4_name,
          tax4_description,
          tax4_rate,
          tax4_amount,
          tax5_name,
          tax5_description,
          tax5_rate,
          tax5_amount,
          REFUND_ADDON_DISCOUNT_AMT
      )  VALUES (
          :OLD.REFUND_ID,                   
          :OLD.REFUND_DISP_CODE,            
          :OLD.PERCENT,                     
          :OLD.CREATED_ON,                  
          :OLD.CREATED_BY,                  
          :OLD.UPDATED_ON,                  
          :OLD.UPDATED_BY,                  
          :OLD.REFUND_PRODUCT_AMOUNT,       
          :OLD.REFUND_ADDON_AMOUNT,         
          :OLD.REFUND_SERVICE_FEE,          
          :OLD.REFUND_TAX,                  
          :OLD.ORDER_DETAIL_ID,             
          :OLD.RESPONSIBLE_PARTY,           
          :OLD.REFUND_STATUS,               
          :OLD.REFUND_DATE,                 
          :OLD.REVIEWED_ON,                 
          :OLD.REVIEWED_BY,                 
          :OLD.SETTLED_DATE,                
          :OLD.REFUND_ADMIN_FEE,            
          :OLD.REFUND_SHIPPING_FEE,         
          :OLD.REFUND_SERVICE_FEE_TAX,      
          :OLD.REFUND_SHIPPING_TAX,         
          :OLD.REFUND_DISCOUNT_AMOUNT,      
          :OLD.REFUND_COMMISSION_AMOUNT,    
          :OLD.REFUND_WHOLESALE_AMOUNT,     
          :OLD.REFUND_WHOLESALE_SERVICE_FEE,
          :OLD.POP_REFUND_FLAG,             
          :OLD.AP_REFUND_TRANS_TXT,         
          'DEL',
          v_current_timestamp,
          :OLD.origin_complaint_comm_type_id, 
          :OLD.notif_complaint_comm_type_id,
          :OLD.tax1_name,
          :OLD.tax1_description,
          :OLD.tax1_rate,
          :OLD.tax1_amount,
          :OLD.tax2_name,
          :OLD.tax2_description,
          :OLD.tax2_rate,
          :OLD.tax2_amount,
          :OLD.tax3_name,
          :OLD.tax3_description,
          :OLD.tax3_rate,
          :OLD.tax3_amount,
          :OLD.tax4_name,
          :OLD.tax4_description,
          :OLD.tax4_rate,
          :OLD.tax4_amount,
          :OLD.tax5_name,
          :OLD.tax5_description,
          :OLD.tax5_rate,
          :OLD.tax5_amount,
          :OLD.REFUND_ADDON_DISCOUNT_AMT
);
   END IF;
END;
/
