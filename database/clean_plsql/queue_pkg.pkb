create or replace
PACKAGE BODY                                                 CLEAN.QUEUE_PKG
AS

PROCEDURE GET_TYPES_BY_QUEUE_INDICATOR
(
IN_QUEUE_INDICATOR              IN QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the queue types for the
        given queue indicator

Input:
        queue_indicator                 varchar2

Output:
        cursor containing queue_type

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  queue_type,
                priority
        FROM    queue_type_val
        WHERE   queue_indicator = in_queue_indicator;

END GET_TYPES_BY_QUEUE_INDICATOR;


PROCEDURE GET_QUEUE_TYPES_ALL
(
IN_COUNT_INDICATOR              IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the queue types and
        the count of each queue if requested.  Note that if counts are requested
        values are obtained from a cache of counts (See UPDATE_QUEUE_COUNTS).
        Separated are Amazon and Wal-mart orders.

Input:
        count indicator         varchar2 - Y/N to return the counts of each queue

Output:
        cursor containing all records from queue_type_val

-----------------------------------------------------------------------------*/
BEGIN

IF in_count_indicator = 'Y' THEN
		-- Q4SP16-13 : Stop Queueing unused Mercury message types
        OPEN OUT_CUR FOR
            SELECT queue_type, description, queue_indicator, sort_order,
                   tagged_count, untagged_count, tagged_unattached_count, untagged_unattached_count
            FROM clean.queue_counts_cache
            WHERE queue_type not in ('GEN','CON')
            ORDER BY queue_indicator, sort_order, description;

 ELSE
  -- Q4SP16-13 : Stop Queueing unused Mercury message types	
  OPEN OUT_CUR FOR
    select * from
      (
        SELECT   v.queue_type,
                  v.description,
                  v.queue_indicator,
                  null tagged_count,
                  null untagged_count,
                  null tagged_unattached_count,
                  null untagged_unattached_count
        FROM      clean.queue_type_val v
        WHERE 	  v.queue_type not in ('GEN','CON')
        ORDER BY  v.queue_indicator, v.sort_order
      )
    UNION
      select      wa.origin_id queue_type,
                  wa.description description,
                  wa.queue_indicator,
                  null tagged_count,
                  null untagged_count,
                  null tagged_unattached_count,
                  null untagged_unattached_count
      from
      (
        select 'ARIBA' origin_id, 'Ariba' as description, 'Email' queue_indicator from dual union
        select 'ARIBA' origin_id, 'Ariba' as description, 'Mercury' queue_indicator from dual union
        select 'WALMART' origin_id, 'WalMart' as description, 'Email' queue_indicator from dual union
        select 'WALMART' origin_id, 'WalMart' as description, 'Mercury' queue_indicator from dual union
        select 'PREMIER_COLLECTION' origin_id, 'Premier Collection' as description, 'Email' queue_indicator from dual union
        select 'PREMIER_COLLECTION' origin_id, 'Premier Collection' as description, 'Mercury' queue_indicator from dual
      ) wa
    UNION
      select      pm.partner_name queue_type,
                  pm.partner_name description,
                  'Email' queue_indicator,
                  null tagged_count,
                  null untagged_count,
                  null tagged_unattached_count,
                  null untagged_unattached_count
      from        ftd_apps.partner_master pm
      where       pm.preferred_partner_flag = 'Y'
    UNION
      select      pm.partner_name queue_type,
                  pm.partner_name description,
                  'Mercury' queue_indicator,
                  null tagged_count,
                  null untagged_count,
                  null tagged_unattached_count,
                  null untagged_unattached_count
      from        ftd_apps.partner_master pm
      where       pm.preferred_partner_flag = 'Y';

END IF;

END GET_QUEUE_TYPES_ALL;


PROCEDURE UPDATE_QUEUE_COUNTS
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible calculating queue counts and saving them
        to the queue_counts_cache table.  Originally queue counts were calculated
        each time the queue page was displayed - however, this was too expensive.
        As such, the queue_counts_cache table was created so counts would only be
        calculated periodically.  The queue page now simply gets counts from this
        table (and this method is scheduled to run periodically).

-----------------------------------------------------------------------------*/

v_count_indicator   CHAR(1) := 'N';

BEGIN

  -- If count_indicator is not on, then we don't need to get counts
  --
  BEGIN
    SELECT 'Y'
      INTO v_count_indicator
      FROM frp.global_parms
      WHERE CONTEXT = 'QUEUE_CONFIG'
      AND NAME = 'COUNT_INDICATOR'
      AND VALUE = 'TRUE';
  EXCEPTION
    WHEN NO_DATA_FOUND THEN NULL;
  END;

  IF v_count_indicator = 'Y' THEN

    -- Remove old counts
    --
    delete from clean.queue_counts_cache;
    delete from clean.queue_group_counts_cache;

    -- Calculate queue counts and insert
    --
    INSERT INTO clean.queue_counts_cache
    (
      --Exclude Wal-mart and Ariba, Premier Collection, and Preferred Partners from regular queue counts
      --Any Premier Collection order in the (LP, CREDIT) queues will be displayed in the (LP, CREDIT) queues; these orders will NOT be displayed under "PREMIER_COLLECTION" queue
      SELECT    v.queue_type,  v.description,  v.queue_indicator, v.sort_order,
                count (distinct (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, t.message_id), t.message_id) ) ) tagged_count,
                count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, q.message_id), q.message_id) ) - count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, t.message_id),  t.message_id) ) untagged_count,
                count (distinct (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, t.message_id, null), null) ) ) tagged_unattached_count,
                count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, q.message_id, null), null) ) - count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, t.message_id, null), null) ) untagged_unattached_count,
                sysdate
      FROM      clean. queue_type_val v
      LEFT OUTER JOIN clean.queue q ON v.queue_type = q.queue_type
      LEFT OUTER JOIN clean.queue_tag t ON q.message_id = t.message_id
      LEFT OUTER JOIN ftd_apps.partner_master pm on q.partner_name = pm.partner_name
      where     nvl(q.origin_id,'TEST') not in ('WLMTI', 'ARI', 'CAT')
      and       q.message_id not in (select message_id from clean.queue q1 where q1.PREMIER_COLLECTION_FLAG = 'Y' and q1.QUEUE_TYPE not in ('LP', 'CREDIT') and message_id = q.message_id)
      and       (q.partner_name is null or pm.preferred_partner_flag != 'Y')
      GROUP BY  v.queue_type,  v.description,  v.queue_indicator,  v.sort_order
      UNION
      --Select only Wal-mart and Ariba origin Ids for email and mercury queue indicators with counts
      SELECT    case  when wa.origin_id in ('ARI', 'CAT') then 'ARIBA'
                      when wa.origin_id = 'WLMTI' then 'WALMART'
                      else  q.queue_type end queue_type,
                case  when wa.origin_id in ('ARI', 'CAT') then 'Ariba'
                      when wa.origin_id = 'WLMTI' then 'WalMart'
                      else  q.description end description,
                wa.queue_indicator,
                case  when wa.origin_id in ('ARI', 'CAT') then 200
                      when wa.origin_id = 'WLMTI' then 201
                      else  q.sort_order end sort_order,
                count (distinct (decode (q.queue_indicator, 'Email', decode (q.order_detail_id, null, null, q.tag_message_id), q.tag_message_id) ) ) tagged_count,
                count (decode (q.queue_indicator, 'Email', decode (q.order_detail_id, null, null, q.queue_message_id), q.queue_message_id) ) - count (decode (q.queue_indicator, 'Email', decode (q.order_detail_id, null, null, q.tag_message_id), q.tag_message_id) ) untagged_count,
                count (distinct (decode (q.queue_indicator, 'Email', decode (q.order_detail_id, null, q.tag_message_id, null), null) ) ) tagged_unattached_count,
                count (decode (q.queue_indicator, 'Email', decode (q.order_detail_id, null, q.queue_message_id, null), null) ) - count (decode (q.queue_indicator, 'Email', decode (q.order_detail_id, null, q.tag_message_id, null), null) ) untagged_unattached_count,
                sysdate
      from      (
                  select 'ARI' origin_id, 'Email' queue_indicator from dual union
                  select 'ARI' origin_id, 'Mercury' queue_indicator from dual union
                  select 'CAT' origin_id, 'Email' queue_indicator from dual union
                  select 'CAT' origin_id, 'Mercury' queue_indicator from dual union
                  select 'WLMTI' origin_id, 'Email' queue_indicator from dual union
                  select 'WLMTI' origin_id, 'Mercury' queue_indicator from dual
                ) wa
      left outer join
                (
                select  q.origin_id, q.order_detail_id, v.queue_type, v.description, v.sort_order, v.queue_indicator, t.message_id tag_message_id, q.message_id queue_message_id
                from    clean.queue q, clean.queue_type_val v, clean.queue_tag t
                where   v.queue_type = q.queue_type
                and     q.message_id = t.message_id(+)
                and     v.queue_type <> 'Order'
                and     q.origin_id  in ('WLMTI', 'ARI', 'CAT')
                ) q on wa.origin_id = q.origin_id
      and       wa.queue_indicator = decode(q.queue_indicator,'Order','Mercury',q.queue_indicator)
      group by
                case when wa.origin_id in ('ARI', 'CAT') then 'ARIBA'
                     when wa.origin_id = 'WLMTI' then 'WALMART'
                     else  q.queue_type end,
                case when wa.origin_id in ('ARI', 'CAT') then 'Ariba'
                     when wa.origin_id = 'WLMTI' then 'WalMart'
                     else  q.description end,
                wa.queue_indicator,
                case when wa.origin_id in ('ARI', 'CAT') then 200
                     when wa.origin_id = 'WLMTI' then 201
                     else  q.sort_order end
      UNION
      --Select only Regular Premier Collection. Preferred Partner premier collection will be displayed in the Preferred Partner queues respectively, not regular premier queue, so exclude them.
      SELECT    'PREMIER_COLLECTION' queue_type, 'Premier Collection'  description,
                case when v.queue_indicator in ('Mercury', 'Order') then 'Mercury'
                     else v.queue_indicator end queue_indicator,
                202 sort_order,
                count (distinct (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, t.message_id), t.message_id) ) ) tagged_count,
                count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, q.message_id), q.message_id) ) - count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, t.message_id),  t.message_id) ) untagged_count,
                count (distinct (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, t.message_id, null), null) ) ) tagged_unattached_count,
                count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, q.message_id, null), null) ) - count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, t.message_id, null), null) ) untagged_unattached_count,
                sysdate
      FROM      clean. queue_type_val v
      LEFT OUTER JOIN clean.queue q ON v.queue_type = q.queue_type
      LEFT OUTER JOIN clean.queue_tag t ON q.message_id = t.message_id
      LEFT OUTER JOIN ftd_apps.partner_master pm on q.partner_name = pm.partner_name
      where     nvl(q.origin_id,'TEST') not in ('WLMTI', 'ARI', 'CAT')
      and       q.message_id in (select message_id from clean.queue q1 where q1.PREMIER_COLLECTION_FLAG = 'Y' and q1.QUEUE_TYPE not in ('LP', 'CREDIT'))
      and       (q.partner_name is null or pm.preferred_partner_flag != 'Y')
      GROUP BY  'PREMIER_COLLECTION', 'Premier Collection' ,
                case when v.queue_indicator in ('Mercury', 'Order') then 'Mercury'
                     else v.queue_indicator end,
                202
      UNION
      --Select everything for Preferred Partners. Preferred Partner queues will contain all queues, including premier.
      SELECT    pm.partner_name queue_type, pm.display_name  description,
                case when v.queue_indicator in ('Mercury', 'Order') then 'Mercury'
                     else v.queue_indicator end queue_indicator,
                203 sort_order,
                count (distinct (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, t.message_id), t.message_id) ) ) tagged_count,
                count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, q.message_id), q.message_id) ) - count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, null, t.message_id),  t.message_id) ) untagged_count,
                count (distinct (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, t.message_id, null), null) ) ) tagged_unattached_count,
                count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, q.message_id, null), null) ) - count (decode (v.queue_indicator, 'Email', decode (q.order_detail_id, null, t.message_id, null), null) ) untagged_unattached_count,
                sysdate
      FROM      clean. queue_type_val v
      LEFT OUTER JOIN clean.queue q ON v.queue_type = q.queue_type
      LEFT OUTER JOIN clean.queue_tag t ON q.message_id = t.message_id
      LEFT OUTER JOIN ftd_apps.partner_master pm on q.partner_name = pm.partner_name
      where     pm.preferred_partner_flag = 'Y'
      GROUP BY  pm.partner_name, pm.display_name,
                case when v.queue_indicator in ('Mercury', 'Order') then 'Mercury'
                     else v.queue_indicator end,
                203
    );


        -- Calculate tagged group counts and insert
        --
        INSERT INTO clean.queue_group_counts_cache (
                SELECT  g.cs_group_id,
                        g.description,
                        count (distinct (t.message_id) ) tagged_count,
                        sysdate
                FROM    ftd_apps.cs_groups g
                LEFT OUTER JOIN aas.users u
                ON      g.cs_group_id = u.cs_group_id
                LEFT OUTER JOIN aas.identity i
                ON      u.user_id = i.user_id
                LEFT OUTER JOIN queue_tag t
                ON      i.identity_id = t.csr_id
                GROUP BY g.cs_group_id,
                        g.description
                --ORDER BY g.cs_group_id
        );

  END IF;

END UPDATE_QUEUE_COUNTS;


PROCEDURE GET_TAGGED_GROUPS
(
IN_COUNT_INDICATOR              IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all of the cs groups
        and the count of tagged queue items.  Note that if counts are requested
        values are obtained from a cache of counts (See UPDATE_QUEUE_COUNTS).

Input:
        count indicator         varchar2 - Y/N to return the counts of each queue

Output:
        cursor containing all records from queue_type

-----------------------------------------------------------------------------*/
BEGIN

IF in_count_indicator = 'Y' THEN

        OPEN OUT_CUR FOR
            SELECT cs_group_id, description, tagged_count
            FROM clean.queue_group_counts_cache
            ORDER BY cs_group_id;

ELSE

        OPEN OUT_CUR FOR
                SELECT  g.cs_group_id,
                        g.description,
                        null tagged_count
                FROM    ftd_apps.cs_groups g
                ORDER BY g.cs_group_id;

END IF;

END GET_TAGGED_GROUPS;

PROCEDURE GET_CSR_TAGGED_COUNT
(
IN_COUNT_INDICATOR              IN VARCHAR2,
IN_CSR_ID                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all of the queue types
        and the count of tagged queue items for the given csr

Input:
        count indicator         varchar2 - Y/N to return the counts of each queue
        csr id                  varchar2

Output:
        cursor containing queue type, description, queue indicator, and
        counts for each queue_type

-----------------------------------------------------------------------------*/
BEGIN

IF in_count_indicator = 'Y' THEN

        OPEN OUT_CUR FOR
                SELECT
                        count (1) csr_count,
                        'user' queue_indicator
                FROM    clean.queue_tag t
                WHERE   t.csr_id = in_csr_id;

ELSE

        OPEN OUT_CUR FOR
                SELECT
                        null csr_count,
                        'user' queue_indicator
                FROM    dual;

END IF;

END GET_CSR_TAGGED_COUNT;


PROCEDURE GET_QUEUE_TYPES
(
IN_COUNT_INDICATOR              IN VARCHAR2,
IN_CSR_ID                       IN VARCHAR2,
OUT_CUR_ALL                     OUT TYPES.REF_CURSOR,
OUT_CUR_GROUP                   OUT TYPES.REF_CURSOR,
OUT_CSR_COUNT                   OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning queue types and counts
        for the drop down lists in the queue application

Input:
        count indicator         varchar2 - Y/N to return the counts of each queue
        csr id                  varchar2

Output:
        cursors containing all queue types, queues items tagged to group,
        queues items tagged to a user
-----------------------------------------------------------------------------*/

BEGIN

GET_QUEUE_TYPES_ALL
(
        IN_COUNT_INDICATOR,
        OUT_CUR_ALL
);

GET_TAGGED_GROUPS
(
        IN_COUNT_INDICATOR,
        OUT_CUR_GROUP
);

GET_CSR_TAGGED_COUNT
(
        IN_COUNT_INDICATOR,
        IN_CSR_ID,
        OUT_CSR_COUNT
);


END GET_QUEUE_TYPES;


PROCEDURE INSERT_QUEUE_TAG
(
 IN_MESSAGE_ID           IN NUMBER,
 IN_CSR_ID               IN VARCHAR2,
 IN_CONTEXT_ID           IN VARCHAR2,
 IN_PERMISSION_ID        IN VARCHAR2,
 OUT_NUMBER_OF_TAGS_CREATED OUT NUMBER,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_ERROR_MESSAGE          OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves the order detail id for the message
        (queue record) passed in if the message has not already been tagged.
        Next the stored procedure finds all the messages (queue records)
        associated to that order detail id that have yet to be tagged/assigned
        (queue_tag record) to a csr id.  Finally if there are messages that
        have not been tagged, tag it the csr id passed in. However, messages
        with queue types of CREDIT, ZIP, DEN, ADJ, CHARGE, LP, or RECON will
        only be tagged to the csr if they have rights for those types of queues.

        Note that this stored procedure does row locking for the given order.

Input:
        message id    number
        csr id        varchar2
        context       varchar2 - security context
        permission    varchar2 - security permission


Output:
        number of tags created
        status
        error message

-----------------------------------------------------------------------------*/

--this cursor will lock all the rows in the queue table for a given order_detail_id
CURSOR queue_cur (detail_id VARCHAR2) IS
      SELECT message_id,
             queue_type
        FROM queue
       WHERE order_detail_id = detail_id
         AND message_id NOT IN (SELECT message_id FROM queue_tag)
       FOR UPDATE;
v_order_detail_id VARCHAR2(2000);
v_queue_type      VARCHAR2(20);
v_count_credit    NUMBER := 0;
v_count_zip       NUMBER := 0;
v_count_den       NUMBER := 0;
v_count_adj       NUMBER := 0;
v_count_charge    NUMBER := 0;
v_count_lp        NUMBER := 0;
v_count_recon     NUMBER := 0;
v_tagged_yet      NUMBER;
v_record_found    CHAR(1) := 'N';

BEGIN

  out_number_of_tags_created := 0;

  --if the message id passed has already been tagged do nothing and exit the stored proc
  BEGIN
    SELECT 'Y'
      INTO v_record_found
      FROM queue_tag
     WHERE message_id = in_message_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN NULL;
  END;

  IF v_record_found = 'N' THEN

    --see if the user has authority for CREDIT message types
    SELECT COUNT(*)
      INTO v_count_credit
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'CREDIT'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --see if the user has authority for ZIP message types
    SELECT COUNT(*)
      INTO v_count_zip
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'ZIP'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --see if the user has authority for DEN message types
    SELECT COUNT(*)
      INTO v_count_den
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'DEN'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --see if the user has authority for ADJ message types
    SELECT COUNT(*)
      INTO v_count_adj
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'ADJ'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --see if the user has authority for CHARGE message types
    SELECT COUNT(*)
      INTO v_count_charge
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'CHARGE'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --see if the user has authority for LP message types
    SELECT COUNT(*)
      INTO v_count_lp
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'LP'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --see if the user has authority for RECON message types
    SELECT COUNT(*)
      INTO v_count_recon
      FROM aas.identity i,
           aas.rel_identity_role rir,
           aas.role r,
           aas.rel_role_acl rra,
           aas.acl a,
           aas.rel_acl_rp rap
     WHERE i.identity_id = in_csr_id
       AND rir.identity_id = i.identity_id
       AND r.role_id = rir.role_id
       AND rra.role_id = r.role_id
       AND a.acl_name = rra.acl_name
       AND rap.acl_name = a.acl_name
       AND UPPER(rap.resource_id) = 'RECON'
       AND rap.context_id = in_context_id
       AND rap.permission_id = in_permission_id;

    --find the order detail id of the queue passed in
    SELECT order_detail_id,
           queue_type
      INTO v_order_detail_id,
           v_queue_type
      FROM queue
     WHERE message_id = in_message_id;

    IF v_order_detail_id IS NULL THEN
       --the message does not have an order associated to it therefore
       -- only one exist.

        --becuase the user may have been locked out, double check
        -- to see if this record may have have been tagged
        v_tagged_yet := 0;
        SELECT COUNT(*)
          INTO v_tagged_yet
          FROM queue_tag
         WHERE message_id = in_message_id;

        IF (v_tagged_yet = 0) THEN

          IF ( (v_queue_type = 'CREDIT') AND (v_count_credit > 0) ) OR
             ( (v_queue_type = 'ZIP')    AND (v_count_zip > 0) )    OR
             ( (v_queue_type = 'DEN')    AND (v_count_den > 0) )    OR
             ( (v_queue_type = 'ADJ')    AND (v_count_adj > 0) )    OR
             ( (v_queue_type = 'CHARGE') AND (v_count_charge > 0) ) OR
             ( (v_queue_type = 'LP')     AND (v_count_lp > 0) )     OR
             ( (v_queue_type = 'RECON')  AND (v_count_recon > 0) )    THEN
          --csr has permission for these types of queues
             INSERT INTO queue_tag
                   (message_id,
                    csr_id,
                    tagged_on,
                    last_touched_on,
                    tag_priority,
                    tag_disposition)
                VALUES
                   (in_message_id,
                    in_csr_id,
                    SYSDATE,
                    SYSDATE,
                    NULL,
                    NULL);
             out_number_of_tags_created := out_number_of_tags_created + 1;
          END IF;

          IF (v_queue_type <> 'CREDIT') AND
             (v_queue_type <> 'ZIP')    AND
             (v_queue_type <> 'DEN')    AND
             (v_queue_type <> 'ADJ')    AND
             (v_queue_type <> 'CHARGE') AND
             (v_queue_type <> 'LP')     AND
             (v_queue_type <> 'RECON')  AND
             (v_queue_type <> 'ORDER') THEN
             INSERT INTO queue_tag
                  (message_id,
                   csr_id,
                   tagged_on,
                   last_touched_on,
                   tag_priority,
                   tag_disposition)
                VALUES
                  (in_message_id,
                   in_csr_id,
                   SYSDATE,
                   SYSDATE,
                   NULL,
                   NULL);
             out_number_of_tags_created := out_number_of_tags_created + 1;
          END IF;

        END IF;

    ELSE
      FOR queue_rec IN queue_cur(v_order_detail_id) LOOP

        --becuase the user may have been locked out, double check
        -- to see if this record may have have been tagged
        v_tagged_yet := 0;
        SELECT COUNT(*)
          INTO v_tagged_yet
          FROM queue_tag
         WHERE message_id = queue_rec.message_id;

        IF (v_tagged_yet = 0) THEN

          IF ( (queue_rec.queue_type = 'CREDIT') AND (v_count_credit > 0) ) OR
             ( (queue_rec.queue_type = 'ZIP')    AND (v_count_zip > 0) )    OR
             ( (queue_rec.queue_type = 'DEN')    AND (v_count_den > 0) )    OR
             ( (queue_rec.queue_type = 'ADJ')    AND (v_count_adj > 0) )    OR
             ( (queue_rec.queue_type = 'CHARGE') AND (v_count_charge > 0) ) OR
             ( (queue_rec.queue_type = 'LP')     AND (v_count_lp > 0) )     OR
             ( (queue_rec.queue_type = 'RECON')  AND (v_count_recon > 0) )  THEN
          --csr has permission for these types of queues
             INSERT INTO queue_tag
                   (message_id,
                    csr_id,
                    tagged_on,
                    last_touched_on,
                    tag_priority,
                    tag_disposition)
                VALUES
                   (queue_rec.message_id,
                    in_csr_id,
                    SYSDATE,
                    SYSDATE,
                    NULL,
                    NULL);
             out_number_of_tags_created := out_number_of_tags_created + 1;
          END IF;

          IF (queue_rec.queue_type <> 'CREDIT') AND
             (queue_rec.queue_type <> 'ZIP')    AND
             (queue_rec.queue_type <> 'DEN')    AND
             (queue_rec.queue_type <> 'ADJ')     AND
             (queue_rec.queue_type <> 'CHARGE')  AND
             (queue_rec.queue_type <> 'LP')      AND
             (queue_rec.queue_type <> 'RECON')   AND
             (queue_rec.queue_type <> 'ORDER') THEN
             INSERT INTO queue_tag
                  (message_id,
                   csr_id,
                   tagged_on,
                   last_touched_on,
                   tag_priority,
                   tag_disposition)
                VALUES
                  (queue_rec.message_id,
                   in_csr_id,
                   SYSDATE,
                   SYSDATE,
                   NULL,
                   NULL);
             out_number_of_tags_created := out_number_of_tags_created + 1;
          END IF;

        END IF;

      END LOOP;

    END IF;

  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_number_of_tags_created := 0;
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_QUEUE_TAG;


PROCEDURE INSERT_QUEUE_RECORD
(
 IN_QUEUE_TYPE            IN VARCHAR2,
 IN_MESSAGE_TYPE          IN VARCHAR2,
 IN_MESSAGE_TIMESTAMP     IN DATE,
 IN_SYSTEM                IN VARCHAR2,
 IN_MERCURY_NUMBER        IN VARCHAR2,
 IN_MASTER_ORDER_NUMBER   IN VARCHAR2,
 IN_ORDER_GUID            IN VARCHAR2,
 IN_ORDER_DETAIL_ID       IN NUMBER,
 IN_POINT_OF_CONTACT_ID   IN NUMBER,
 IN_EMAIL_ADDRESS         IN VARCHAR2,
 IN_EXTERNAL_ORDER_NUMBER IN VARCHAR2,
 IN_MERCURY_ID            IN VARCHAR2,
 OUT_MESSAGE_ID           OUT NUMBER,
 OUT_STATUS               OUT VARCHAR2,
 OUT_ERROR_MESSAGE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table queue.

Input:
        queue_type, message_type, message_timestamp,
        system, mercury_number, master_order_number,
        order_guid, order_detail_id, point_of_contact_id,
        email_address, external_order_number

Output:
        message id
        status
        error message

-----------------------------------------------------------------------------*/
v_delivery_date             clean.queue.delivery_date%type;
v_timezone                  clean.queue.timezone%type;
v_partner_name              clean.queue.partner_name%type;
v_preferred_partner_flag    clean.queue.preferred_partner_flag%type;
v_origin_id                 clean.queue.origin_id%type;
v_recipient_id              clean.queue.recipient_id%type;
v_recipient_address_type    clean.queue.recipient_address_type%type;
v_recipient_zip_code        clean.queue.recipient_zip_code%type;
v_product_id                clean.queue.product_id%type;
v_premier_collection_flag   clean.queue.premier_collection_flag%type;
v_occasion                  clean.queue.occasion%type;
v_customer_id               clean.queue.customer_id%type;
v_email_id	            clean.orders.email_id%type;

v_source_code_od            clean.order_details.source_code%type;
v_partner_name_poc          clean.queue.partner_name%type;
v_partner_name_od           clean.queue.partner_name%type;
v_preferred_flag_poc        ftd_apps.partner_master.preferred_partner_flag%type;
v_preferred_flag_od         ftd_apps.partner_master.preferred_partner_flag%type;
--Mass queue delete changes
v_email_address   clean.queue.email_address%type := IN_EMAIL_ADDRESS;
v_price_ftd   clean.queue.price_ftd%type;
v_price_askp  clean.queue.price_askp%type;
v_mercury_order_number      mercury.mercury.mercury_order_number%type;
v_comments    clean.queue.mercury_comments%type;
v_codification_id 			ftd_apps.codified_products.codification_id%type;

BEGIN

  IF IN_ORDER_DETAIL_ID is not null THEN
    BEGIN
      SELECT  od.delivery_date, od.product_id, od.recipient_id, pm.premier_collection_flag, od.occasion, od.source_code,   o.origin_id, o.customer_id, o.email_id
        INTO  v_delivery_date,  v_product_id,  v_recipient_id,  v_premier_collection_flag,  v_occasion,  v_source_code_od, v_origin_id, v_customer_id, v_email_id
        FROM  clean.order_details od, clean.orders o, ftd_apps.product_master pm
       WHERE  o.order_guid = od.order_guid
         AND  od.product_id = pm.product_id
         AND  od.order_detail_id = IN_ORDER_DETAIL_ID
      ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;


  IF (v_origin_id is null OR v_customer_id is null) AND IN_ORDER_GUID is not null THEN
    BEGIN
      SELECT  o.origin_id, o.customer_id, o.email_id
        INTO  v_origin_id, v_customer_id, v_email_id
        FROM  clean.orders o
       WHERE  o.order_guid = IN_ORDER_GUID
      ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;


  IF v_recipient_id is not null THEN
    BEGIN
      SELECT  sm.time_zone, r.address_type,           r.zip_code
        INTO  v_timezone,   v_recipient_address_type, v_recipient_zip_code
        FROM  clean.customer r, ftd_apps.state_master sm
       WHERE  r.customer_id = v_recipient_id
         AND  sm.state_master_id = r.state
      ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;


  IF v_source_code_od is not null THEN
    BEGIN
      SELECT  DISTINCT pp.partner_name, pm.preferred_partner_flag
        INTO  v_partner_name_od, v_preferred_flag_od
        FROM  ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
       WHERE  spr.source_code = v_source_code_od
         AND  spr.program_name = pp.program_name
         AND  pp.partner_name = pm.partner_name
      ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  BEGIN
    SELECT  DISTINCT pp.partner_name , pm.preferred_partner_flag
      INTO  v_partner_name_poc, v_preferred_flag_poc
      FROM  ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
     WHERE  spr.source_code = (SELECT poc.source_code FROM clean.point_of_contact poc WHERE poc.point_of_contact_id = IN_POINT_OF_CONTACT_ID)
       AND  spr.program_name = pp.program_name
       AND  pp.partner_name = pm.partner_name
    ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
  END;

  -- If a Preferred Partner is found based on the order number (regardless of the mailbox address the email is received at),
  -- it will be part of a Preferred Partner queue
  IF (v_preferred_flag_od = 'Y') THEN
    v_partner_name := v_partner_name_od;
  -- If a Preferred Partner is found based on the mailbox address the email is received at, it will be part of Preferred Partner partner queue
  ELSIF (v_preferred_flag_poc = 'Y') THEN
    v_partner_name := v_partner_name_poc;
  -- Else, if any other partner is found based off of order_details.source_code, use that partner_name
  ELSIF v_partner_name_od is not null THEN
    v_partner_name := v_partner_name_od;
  -- Else, if any other partner is found based off of point_of_contact.source_code, use that partner_name
  ELSIF v_partner_name_poc is not null THEN
    v_partner_name := v_partner_name_poc;
  END IF;


  IF v_partner_name is not null THEN
    BEGIN
      SELECT  preferred_partner_flag
        INTO  v_preferred_partner_flag
        FROM  ftd_apps.partner_master pm
       WHERE  pm.partner_name = v_partner_name
      ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF v_email_id is not null then
    BEGIN
      select em.email_address into v_email_address
        from email em
        where em.email_id = v_email_id;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  IF IN_SYSTEM = 'Merc' and IN_MERCURY_ID is not null THEN

    BEGIN

      --Retrieve the mercury order number and comments from mercury record
      select mercury_order_number, comments
        into v_mercury_order_number, v_comments
        from mercury.mercury
       where mercury_id = IN_MERCURY_ID;

    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;

    IF v_mercury_order_number IS NOT NULL THEN 

      BEGIN

        --Retrieve the price from the ASK and FTD records
        select price as askp, (select price from mercury.mercury where msg_type='FTD' and mercury_order_number = v_mercury_order_number) as ftd
          into v_price_askp,  v_price_ftd
          from mercury.mercury
         where msg_type='ASK'
           and ask_answer_code = 'P'
           and message_direction = 'INBOUND'
           and mercury_id = IN_MERCURY_ID;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
      END;

    END IF;

  END IF;
  
  IF v_product_id is not null THEN
    BEGIN
      SELECT  cp.codification_id
        INTO  v_codification_id
        FROM  ftd_apps.codified_products cp
       WHERE  v_product_id = cp.product_id
      ;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
  END IF;

  INSERT INTO queue
     (message_id,
      queue_type,
      message_type,
      message_timestamp,
      system,
      mercury_number,
      master_order_number,
      order_guid,
      order_detail_id,
      point_of_contact_id,
      email_address,
      created_on,
      external_order_number,
      mercury_id,
      delivery_date,
      timezone,
      partner_name,
      preferred_partner_flag,
      origin_id,
      recipient_id,
      recipient_address_type,
      recipient_zip_code,
      product_id,
      premier_collection_flag,
      occasion,
      customer_id,
      price_ftd,
      price_askp,
      mercury_comments,
      codification_id)
    VALUES
     (message_id_sq.nextval,
      in_queue_type,
      in_message_type,
      in_message_timestamp,
      in_system,
      in_mercury_number,
      in_master_order_number,
      in_order_guid,
      in_order_detail_id,
      in_point_of_contact_id,
      v_email_address,
      SYSDATE,
      substr (in_external_order_number, 1, 20),
      in_mercury_id,
      v_delivery_date,
      v_timezone,
      v_partner_name,
      v_preferred_partner_flag,
      v_origin_id,
      v_recipient_id,
      v_recipient_address_type,
      v_recipient_zip_code,
      v_product_id,
      v_premier_collection_flag,
      v_occasion,
      v_customer_id,
      v_price_ftd,
      v_price_askp,
      v_comments,
      v_codification_id)
    RETURNING message_id INTO out_message_id;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_QUEUE_RECORD;



PROCEDURE INSERT_QUEUE_RECORD
(
 IN_QUEUE_TYPE            IN VARCHAR2,
 IN_MESSAGE_TYPE          IN VARCHAR2,
 IN_MESSAGE_TIMESTAMP     IN DATE,
 IN_SYSTEM                IN VARCHAR2,
 IN_MERCURY_NUMBER        IN VARCHAR2,
 IN_MASTER_ORDER_NUMBER   IN VARCHAR2,
 IN_ORDER_GUID            IN VARCHAR2,
 IN_ORDER_DETAIL_ID       IN NUMBER,
 IN_POINT_OF_CONTACT_ID   IN NUMBER,
 IN_EMAIL_ADDRESS         IN VARCHAR2,
 IN_EXTERNAL_ORDER_NUMBER IN VARCHAR2,
 IN_MERCURY_ID            IN VARCHAR2,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table queue.

Input:
        queue_type, message_type, message_timestamp,
        system, mercury_number, master_order_number,
        order_guid, order_detail_id, point_of_contact_id,
        email_address, external_order_number

Output:
        status
        error message

Note:
        This procedure overloads the above procedure.  The signature of this
        procedure was the original insert queue record proc using by the
        queue application which did not need the message id returned.
-----------------------------------------------------------------------------*/

v_message_id            number;

BEGIN


INSERT_QUEUE_RECORD
(
 IN_QUEUE_TYPE,
 IN_MESSAGE_TYPE,
 IN_MESSAGE_TIMESTAMP,
 IN_SYSTEM,
 IN_MERCURY_NUMBER,
 IN_MASTER_ORDER_NUMBER,
 IN_ORDER_GUID,
 IN_ORDER_DETAIL_ID,
 IN_POINT_OF_CONTACT_ID,
 IN_EMAIL_ADDRESS,
 IN_EXTERNAL_ORDER_NUMBER,
 IN_MERCURY_ID,
 V_MESSAGE_ID,
 OUT_STATUS,
 OUT_ERROR_MESSAGE
);

END INSERT_QUEUE_RECORD;


FUNCTION GET_DELETE_QUEUE_AUTHORITY
(
IN_CSR_ID                       IN VARCHAR2
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns the highest queue acl for the given csr id.
Input:
        csr_id                  varchar2

Output:
        Highest ACL name

Note: Acls:
QDelCSDirector = 10
QDelCSSCS = 15
QDelCSManager = 20
QDelCSSupervisor = 30
QDelCSStaff = 40

-----------------------------------------------------------------------------*/

CURSOR acl_cur IS
        SELECT  acl_level
        FROM    aas.acl a
        JOIN    aas.rel_role_acl ra
        ON      a.acl_name = ra.acl_name
        JOIN    aas.rel_identity_role ir
        ON      ra.role_id = ir.role_id
        WHERE   a.acl_name IN ('QDelCSDirector', 'QDelCSManager')
        AND     ir.identity_id = in_csr_id
        ORDER BY a.acl_level DESC;

v_acl_level     number := -1;

BEGIN

OPEN acl_cur;
FETCH acl_cur INTO v_acl_level;  -- the first record in the cursor has the highest level of the csr
CLOSE acl_cur;

RETURN v_acl_level;

END GET_DELETE_QUEUE_AUTHORITY;


PROCEDURE DELETE_QUEUE_RECORD
(
IN_MESSAGE_ID                   IN QUEUE.MESSAGE_ID%TYPE,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
IN_TAGGED_CSR_ID                IN QUEUE_TAG.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_ERROR_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table queue.  If the
        record is tagged, the tag is checked to see if the authority level
        is the same or lower.  If the authority level is higher, the record
        is not deleted.

Input:
        message_id              number
        csr_id                  varchar2 (csr deleting the queue or tag)
        tagged_csr_id           varchar2 (tag to be deleted only applies to ORDER queue)

Output:
        status
        error message

-----------------------------------------------------------------------------*/

TYPE varchar_typ IS TABLE OF VARCHAR2(100) INDEX BY PLS_INTEGER;

v_delete                boolean := TRUE;
v_csr_has_delete_auth   boolean := FALSE;
v_no_more_tags          boolean := TRUE;
v_csr_level             number;
v_csr_tab               varchar_typ;
v_csr_id                varchar2(100);
v_check                 varchar2(100);

CURSOR  tag_cur (p_tagged_csr_id varchar) IS
        SELECT  t.csr_id
        FROM    queue_tag t
        WHERE   t.message_id = in_message_id
        AND     (t.csr_id = p_tagged_csr_id
        OR       p_tagged_csr_id is null);

BEGIN

-- get the authority level csr trying to delete the queue item
v_csr_level := get_delete_queue_authority (in_csr_id);


        OPEN tag_cur (in_tagged_csr_id);
        FETCH tag_cur INTO v_csr_id;
        CLOSE tag_cur;

        -- check any other tag's authority level
        -- if the level is the same or lower, then both tags are deleted
        -- if not, the status and message is returned stating that the queue item is tagged
        -- by a higher level of authority
        IF v_csr_id IS NOT NULL THEN
                IF v_csr_id <> in_csr_id AND get_delete_queue_authority (v_csr_id) > v_csr_level THEN
                        v_delete := FALSE;
                ELSE
                        -- insert the queue history record for the tagged item
                        INSERT INTO queue_delete_history
                        (
                                message_id,
                                queue_type,
                                tagged_by,
                                tagged_on,
                                queue_deleted_by,
                                queue_deleted_on,
                                order_guid,
                                order_detail_id,
                                master_order_number,
                                external_order_number,
                                queue_created_on,
                                mercury_number,
                                message_type
                        )
                        SELECT  q.message_id,
                                q.queue_type,
                                t.csr_id,
                                t.tagged_on,
                                in_csr_id,
                                sysdate,
                                q.order_guid,
                                q.order_detail_id,
                                q.master_order_number,
                                q.external_order_number,
                                q.created_on,
                                q.mercury_number,
                                q.message_type
                        FROM    queue q
                        JOIN    queue_tag t
                        ON      q.message_id = t.message_id
                        WHERE   q.message_id = in_message_id
                        AND     t.csr_id = v_csr_id;

                        DELETE FROM queue_tag
                        WHERE   message_id = in_message_id
                        AND     csr_id = v_csr_id;

                        -- check if there is any remaining tags on the queue
                        OPEN tag_cur (null);
                        FETCH tag_cur INTO v_check;
                        CLOSE tag_cur;

                        IF v_check IS NOT NULL THEN
                                v_no_more_tags := FALSE;
                        END IF;
                END IF;
        END IF;

        -- if there was no tags or if all of the tags were deleted then delete the
        -- queue item
        IF v_delete AND v_no_more_tags THEN
                IF v_csr_id IS NULL THEN
                        -- queue item was not tagged so insert the queue history record
                        INSERT INTO queue_delete_history
                        (
                                message_id,
                                queue_type,
                                queue_deleted_by,
                                queue_deleted_on,
                                order_guid,
                                order_detail_id,
                                master_order_number,
                                external_order_number,
                                queue_created_on,
                                mercury_number,
                                message_type
                        )
                        SELECT  q.message_id,
                                q.queue_type,
                                in_csr_id,
                                sysdate,
                                q.order_guid,
                                q.order_detail_id,
                                q.master_order_number,
                                q.external_order_number,
                                q.created_on,
                                q.mercury_number,
                                q.message_type
                        FROM    queue q
                        WHERE   q.message_id = in_message_id;
                END IF;

                -- delete the queue item
                DELETE FROM queue q
                WHERE   q.message_id = in_message_id;

                out_status := 'Y';
        ELSE
                IF v_no_more_tags = FALSE THEN
                        -- set status to Y because the given tag was deleted
                        out_status := 'Y';
                ELSE
                        -- set the status and message stating that the queue item was not deleted
                        -- because of the authority level
                        out_status := 'N';
                        out_error_message := 'Queue item is tagged by higher authority level';
                END IF;
        END IF;


EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_QUEUE_RECORD;


PROCEDURE DELETE_QUEUE_RECORD_NO_AUTH
(
IN_MESSAGE_ID                   IN QUEUE.MESSAGE_ID%TYPE,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_ERROR_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table queue.

Input:
        message_id              number
        csr_id                  varchar2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

TYPE varchar_typ IS TABLE OF VARCHAR2(100) INDEX BY PLS_INTEGER;

v_csr_tab               varchar_typ;

CURSOR  tag_cur IS
        SELECT  t.csr_id
        FROM    queue_tag t
        WHERE   t.message_id = in_message_id;

BEGIN

OPEN tag_cur;
FETCH tag_cur BULK COLLECT INTO v_csr_tab;
CLOSE tag_cur;

-- delete any tags for the message
FOR x IN 1..v_csr_tab.count LOOP
        -- insert the queue history record for the tagged item
        INSERT INTO queue_delete_history
        (
                message_id,
                queue_type,
                tagged_by,
                tagged_on,
                queue_deleted_by,
                queue_deleted_on,
                order_guid,
                order_detail_id,
                master_order_number,
                external_order_number,
                queue_created_on,
                mercury_number,
                message_type
        )
        SELECT  q.message_id,
                q.queue_type,
                t.csr_id,
                t.tagged_on,
                in_csr_id,
                sysdate,
                q.order_guid,
                q.order_detail_id,
                q.master_order_number,
                q.external_order_number,
                q.created_on,
                q.mercury_number,
                q.message_type
        FROM    queue q
        JOIN    queue_tag t
        ON      q.message_id = t.message_id
        WHERE   q.message_id = in_message_id
        AND     t.csr_id = v_csr_tab (x);

        DELETE FROM queue_tag
        WHERE   message_id = in_message_id
        AND     csr_id = v_csr_tab(x);
END LOOP;

-- if there was no tags or if all of the tags were deleted then delete the
-- queue item
IF v_csr_tab.count = 0 THEN
        -- queue item was not tagged so insert the queue history record
        INSERT INTO queue_delete_history
        (
                message_id,
                queue_type,
                queue_deleted_by,
                queue_deleted_on,
                order_guid,
                order_detail_id,
                master_order_number,
                external_order_number,
                queue_created_on,
                mercury_number,
                message_type
        )
        SELECT  q.message_id,
                q.queue_type,
                in_csr_id,
                sysdate,
                q.order_guid,
                q.order_detail_id,
                q.master_order_number,
                q.external_order_number,
                q.created_on,
                q.mercury_number,
                q.message_type
        FROM    queue q
        WHERE   q.message_id = in_message_id;
END IF;

-- delete the queue item
DELETE FROM queue
WHERE   message_id = in_message_id;

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_QUEUE_RECORD_NO_AUTH;


FUNCTION IS_ORDER_TAGGED
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns an indicator if the order is tagged by a user in
        the order queue.

Input:
        order_detail_id         number

Output:
        Y/N if the order is tagged

-----------------------------------------------------------------------------*/

CURSOR tag_cur IS
        SELECT  count (1)
        FROM    queue_tag t
        JOIN    queue q
        ON      t.message_id = q.message_id
        WHERE   q.order_detail_id = in_order_detail_id
        AND     q.queue_type = 'ORDER';

v_tagged        number := 0;

BEGIN

OPEN tag_cur;
FETCH tag_cur INTO v_tagged;
CLOSE tag_cur;

RETURN v_tagged;

END IS_ORDER_TAGGED;


FUNCTION IS_ORDER_TAGGED
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns an indicator if the order is tagged by a user in
        the order queue and if the user has tagged the order.

Input:
        order_detail_id         number
        csr_id                  varchar2

Output:
        0 - order is not tagged to any user
        1Y - order is tagged to the given user
        1N - order is tagged but no to the given user
        2Y - order is tagged by 2 users, one of them being the given user
        2N - order is tagged by 2 users, neither by the given user

-----------------------------------------------------------------------------*/

CURSOR tag_cur IS
        SELECT  to_char (count (1)) || max (decode (t.csr_id, in_csr_id, 'Y', 'N')) tagged
        FROM    queue_tag t
        JOIN    queue q
        ON      t.message_id = q.message_id
        WHERE   q.order_detail_id = in_order_detail_id
        AND     q.queue_type = 'ORDER';

v_tagged        varchar2(20);

BEGIN

OPEN tag_cur;
FETCH tag_cur INTO v_tagged;
CLOSE tag_cur;

RETURN v_tagged;


END IS_ORDER_TAGGED;

PROCEDURE GET_MQD_QUEUE_RECORD_BY_ORDER
(
 IN_ORDER_NUMBER IN QUEUE.ORDER_DETAIL_ID%TYPE,
 IN_REQUEST_TYPE IN QUEUE.QUEUE_TYPE%TYPE,
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table queue, including
        the e-mail address from the table point_of_contact, for a particular
        order number (order detail id) and request (queue) type.

Input:
        order detail number, queue type

Output:
        ref cusor that contains a queue record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT q.message_id,
           q.queue_type,
           q.message_type,
           q.system,
           q.mercury_number,
           q.master_order_number,
           q.order_guid,
           q.order_detail_id,
           q.point_of_contact_id,
           q.external_order_number,
           t.tag_priority priority,
           p.sender_email_address,
           (SELECT COUNT(*) FROM queue_tag qt WHERE qt.message_id = q.message_id) "Taggs",
           q.created_on,
           t.tagged_on,
           t.csr_id,
           is_tag_expired (t.tagged_on, t.tag_priority) is_tag_expired
     FROM queue q, mqd_queue_type_val qt, point_of_contact p, queue_tag t
     WHERE q.order_detail_id = in_order_number
       AND (q.queue_type = in_request_type or in_request_type is null)
       AND qt.queue_type = q.queue_type
       AND q.point_of_contact_id = p.point_of_contact_id (+)
       AND q.message_id = t.message_id (+);

END GET_MQD_QUEUE_RECORD_BY_ORDER;


PROCEDURE GET_QUEUE_RECORD_BY_ORDER
(
 IN_ORDER_NUMBER IN QUEUE.ORDER_DETAIL_ID%TYPE,
 IN_REQUEST_TYPE IN QUEUE.QUEUE_TYPE%TYPE,
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table queue, including
        the e-mail address from the table point_of_contact, for a particular
        order number (order detail id) and request (queue) type.

Input:
        order detail number, queue type

Output:
        ref cusor that contains a queue record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT q.message_id,
           q.queue_type,
           q.message_type,
           q.system,
           q.mercury_number,
           q.master_order_number,
           q.order_guid,
           q.order_detail_id,
           q.point_of_contact_id,
           q.external_order_number,
           t.tag_priority priority,
           p.sender_email_address,
           (SELECT COUNT(*) FROM queue_tag qt WHERE qt.message_id = q.message_id) "Taggs",
           q.created_on,
           t.tagged_on,
           t.csr_id,
           is_tag_expired (t.tagged_on, t.tag_priority) is_tag_expired
     FROM queue q, queue_type_val qt, point_of_contact p, queue_tag t
     WHERE q.order_detail_id = in_order_number
       AND (q.queue_type = in_request_type or in_request_type is null)
       AND qt.queue_type = q.queue_type
       AND q.point_of_contact_id = p.point_of_contact_id (+)
       AND q.message_id = t.message_id (+);

END GET_QUEUE_RECORD_BY_ORDER;


PROCEDURE GET_ORDR_HIGHEST_PRIORITY_QUE
(
 IN_ORDER_DETAIL_ID IN QUEUE.ORDER_DETAIL_ID%TYPE,
 IN_QUEUE_INDICATOR IN QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves all the records with the highest
        queue type priority from table queue for a particular order number
        (order detail id).

Input:
        order detail number

Output:
        ref cusor that contains queue records

-----------------------------------------------------------------------------*/
v_priority NUMBER;

BEGIN

  SELECT MIN (qtv.priority) INTO v_priority
    FROM queue q, queue_type_val qtv
   WHERE q.order_detail_id = in_order_detail_id
     AND qtv.queue_type = q.queue_type
     AND qtv.queue_indicator = in_queue_indicator;

  OPEN out_cursor FOR
    SELECT q.message_id,
           q.queue_type,
           q.message_type,
           q.system,
           q.mercury_number,
           q.master_order_number,
           q.order_guid,
           q.order_detail_id,
           q.point_of_contact_id,
           q.external_order_number,
           qt.priority
      FROM queue q, queue_type_val qt
     WHERE q.order_detail_id = in_order_detail_id
       AND qt.queue_type = q.queue_type
       AND qt.queue_indicator = in_queue_indicator
       AND q.queue_type IN (SELECT queue_type
                              FROM queue_type_val
                             WHERE priority = v_priority)
       AND NOT EXISTS
                (select 1 from queue_tag t where t.message_id = q.message_id);

END GET_ORDR_HIGHEST_PRIORITY_QUE;


PROCEDURE LOCK_ORDER_QUEUE_TAG
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
OUT_ENTITY_ID                   OUT CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_ERROR_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for creating a lock for tagging an order

Input:
        order_detail_id                 number
        csr_id                          varchar2
        session_id                      varchar2
        entity_type                     varhcar2

Output:
        locked entity id                varchar2
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

v_lock_obtained         char(1);
v_locked_csr_id         varchar2(2000);

BEGIN

-- An order can be tagged by 2 different csr's.
-- try setting a lock with an indicator of 1
-- if the lock exists, try a second lock with an indicator of 2
-- if 2 locks exists, return the locked error

FOR x IN 1..2 LOOP
        out_entity_id := to_char (in_order_detail_id) || '::' || to_char (x);
        CSR_VIEWED_LOCKED_PKG.UPDATE_CSR_LOCKED_ENTITIES
        (
                IN_ENTITY_TYPE=>in_entity_type,
                IN_ENTITY_ID=>out_entity_id,
                IN_SESSION_ID=>in_session_id,
                IN_CSR_ID=>in_csr_id,
                IN_ORDER_LEVEL=>null,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_error_message,
                OUT_LOCK_OBTAINED=>v_lock_obtained,
                OUT_LOCKED_CSR_ID=>v_locked_csr_id
        );

        IF out_status = 'Y' AND v_lock_obtained = 'N' THEN
                -- try to lock again
                null;
        ELSE
                exit;
        END IF;
END LOOP;

IF out_status = 'Y' THEN
        commit;
END IF;

EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END LOCK_ORDER_QUEUE_TAG;


PROCEDURE UNLOCK_ORDER_QUEUE_TAG
(
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
IN_ENTITY_TYPE                  IN CSR_LOCKED_ENTITIES.ENTITY_TYPE%TYPE,
IN_ENTITY_ID                    IN CSR_LOCKED_ENTITIES.ENTITY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_ERROR_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible deleting the lock for tagging an order

Input:

        csr_id                          varchar2
        session_id                      varchar2
        entity_type                     varhcar2
        entity id                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

CSR_VIEWED_LOCKED_PKG.DELETE_CSR_LOCKED_ENTITIES
(
        in_entity_type,
        in_entity_id,
        in_session_id,
        in_csr_id,
        out_status,
        out_error_message
);

IF out_status = 'Y' THEN
        commit;
END IF;

EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UNLOCK_ORDER_QUEUE_TAG;


PROCEDURE INSERT_ORDER_QUEUE_TAG
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
IN_TAG_PRIORITY                 IN QUEUE_TAG.TAG_PRIORITY%TYPE,
IN_TAG_DISPOSITION              IN QUEUE_TAG.TAG_DISPOSITION%TYPE,
IN_SESSION_ID                   IN CSR_LOCKED_ENTITIES.SESSION_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_ERROR_MESSAGE               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for tagging an order

Input:
        order_detail_id                 number
        csr_id                          varchar2
        tag_priority                    varchar2
        tag_disposition                 varchar2
        session id                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

v_order_detail_id       order_details.order_detail_id%type;
v_order_guid            orders.order_guid%type;
v_external_order_number order_details.external_order_number%type;
v_master_order_number   orders.master_order_number%type;
v_customer_id           orders.customer_id%type;
v_order_disp_code       order_details.order_disp_code%type;
v_order_indicator       varchar2(100);

v_message_id            queue.message_id%type;
v_tag_count             number;
v_tagged_to_csr         char (1);

v_lock_entity_type      csr_locked_entities.entity_type%type := 'QUEUE TAG ORDER';
v_lock_entity_id        csr_locked_entities.entity_id%type;

lock_status             char(1);
lock_message            varchar2(1000);

CURSOR queue_cur (p_order_detail_id number) IS
        SELECT  q.message_id,
                count (t.csr_id) tag_count,
                max (decode (t.csr_id, in_csr_id, 'Y', 'N')) tagged_to_csr
        FROM    queue q
        JOIN    queue_tag t
        ON      q.message_id = t.message_id
        WHERE   order_detail_id = p_order_detail_id
        AND     queue_type = 'ORDER'
        GROUP BY q.message_id;

CURSOR vendor_cur (p_order_detail_id number) IS
        SELECT  decode (od.ship_method, null, 'Merc', 'SD', 'Merc', 'Venus') vendor_flag
        FROM    order_details od
        WHERE   od.order_detail_id = p_order_detail_id;

v_vendor_flag           varchar2(5);
BEGIN

-- set a lock for tagging the order
lock_order_queue_tag
(
        in_order_detail_id,
        in_csr_id,
        in_session_id,
        v_lock_entity_type,
        v_lock_entity_id,
        lock_status,
        lock_message
);

IF lock_status = 'N' THEN
        out_status := lock_status;
        out_error_message := lock_message;
ELSE

        -- if the lock was set then insert the order tag
        order_query_pkg.find_order_number
        (
                in_order_detail_id,
                v_order_detail_id,
                v_order_guid,
                v_external_order_number,
                v_master_order_number,
                v_customer_id,
                v_order_disp_code,
                v_order_indicator
        );

        if v_order_detail_id is not null then
                -- check if the order is already tagged
                OPEN queue_cur (v_order_detail_id);
                FETCH queue_cur INTO v_message_id, v_tag_count, v_tagged_to_csr;
                CLOSE queue_cur;

                IF v_tagged_to_csr = 'N' OR v_tagged_to_csr IS NULL THEN
                        -- initialize out_status
                        out_status := 'Y';

                        -- if the order is not already tagged then insert a new ORDER queue record for the order
                        if v_message_id IS NULL then
                                OPEN vendor_cur (v_order_detail_id);
                                FETCH vendor_cur INTO v_vendor_flag;
                                CLOSE vendor_cur;

                                INSERT_QUEUE_RECORD
                                (
                                        IN_QUEUE_TYPE=>'ORDER',
                                        IN_MESSAGE_TYPE=>'ORDER',
                                        IN_MESSAGE_TIMESTAMP=>SYSDATE,
                                        IN_SYSTEM=>v_vendor_flag,
                                        IN_MERCURY_NUMBER=>NULL,
                                        IN_MASTER_ORDER_NUMBER=>v_master_order_number,
                                        IN_ORDER_GUID=>v_order_guid,
                                        IN_ORDER_DETAIL_ID=>v_order_detail_id,
                                        IN_POINT_OF_CONTACT_ID=>NULL,
                                        IN_EMAIL_ADDRESS=>NULL,
                                        IN_EXTERNAL_ORDER_NUMBER=>v_external_order_number,
                                        IN_MERCURY_ID=>null,
                                        OUT_MESSAGE_ID=>v_message_id,
                                        OUT_STATUS=>out_status,
                                        OUT_ERROR_MESSAGE=>out_error_message
                                );

                        end if;

                        if out_status = 'Y' AND
                           (v_tag_count < 2 OR v_tag_count IS NULL) then
                                INSERT INTO queue_tag
                                (
                                        message_id,
                                        csr_id,
                                        tagged_on,
                                        tag_priority,
                                        tag_disposition
                                )
                                VALUES
                                (
                                        v_message_id,
                                        in_csr_id,
                                        sysdate,
                                        in_tag_priority,
                                        in_tag_disposition
                                );
                        else
                                if v_tag_count >= 2 then
                                        out_status := 'N';
                                        out_error_message := 'Order has the maximum number of tags';
                                end if;
                        end if;
                ELSE
                        out_status := 'N';
                        out_error_message := 'Order already tagged to csr';
                END IF;

        else
                out_status := 'N';
                out_error_message := 'Order not found';
        end if;

        unlock_order_queue_tag
        (
                in_csr_id,
                in_session_id,
                v_lock_entity_type,
                v_lock_entity_id,
                lock_status,
                lock_message
        );

        IF lock_status = 'N' THEN
                out_status := lock_status;
                out_error_message := lock_message;
        END IF;

END IF;

EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_QUEUE_TAG;


FUNCTION GET_QUEUE_TYPE_PRIORITY
(
IN_QUEUE_TYPE                   IN QUEUE_TYPE_VAL.QUEUE_TYPE%TYPE
)
RETURN NUMBER
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the priority if of the
        given queue.

Input:
        queue_type                      varchar

Output:
        priority                        number
-----------------------------------------------------------------------------*/

CURSOR priority_cur IS
        SELECT  priority
        FROM    queue_type_val
        WHERE   queue_type = in_queue_type;

v_priority      queue_type_val.priority%type;

BEGIN

OPEN priority_cur;
FETCH priority_cur INTO v_priority;
CLOSE priority_cur;

RETURN v_priority;

END GET_QUEUE_TYPE_PRIORITY;



FUNCTION GET_CUST_HOLD_REASON_CODE
(
IN_ENTITY_TYPE                  IN CLEAN.CUSTOMER_HOLD_ENTITIES.ENTITY_TYPE%TYPE,
IN_HOLD_VALUE_1                 IN CLEAN.CUSTOMER_HOLD_ENTITIES.HOLD_VALUE_1%TYPE,
IN_HOLD_REASON_CODE             IN CLEAN.CUSTOMER_HOLD_ENTITIES.HOLD_REASON_CODE%TYPE
)
RETURN CLEAN.CUSTOMER_HOLD_ENTITIES.HOLD_REASON_CODE%TYPE
IS

v_data                        CLEAN.CUSTOMER_HOLD_REASON_VAL.HOLD_REASON_CODE%TYPE;


BEGIN

  BEGIN
    SELECT distinct HOLD_REASON_CODE
      INTO v_data
      FROM CLEAN.CUSTOMER_HOLD_ENTITIES
     WHERE entity_type = IN_ENTITY_TYPE
       AND hold_value_1 = IN_HOLD_VALUE_1
       AND hold_reason_code = IN_HOLD_REASON_CODE;


    EXCEPTION
       WHEN NO_DATA_FOUND THEN
       v_data := NULL;
  END;

  RETURN v_data;

END GET_CUST_HOLD_REASON_CODE;


  FUNCTION Get_Queue_Sql(In_Queue_Type               IN Clean.Queue.Queue_Type%TYPE
                        ,In_Tagged_Indicator         IN VARCHAR2
                        ,In_Attached_Email_Indicator IN VARCHAR2
                        ,In_Sort_Order               IN VARCHAR2
                        ,In_Csr_Id                   IN VARCHAR2
                        ,In_Group_Id                 IN VARCHAR2
                        ,In_Sort_Direction           IN VARCHAR2
                        ,IN_EXCLUDE_PARTNERS         IN VARCHAR2
                        ,Out_Sort_Order              OUT VARCHAR2) RETURN VARCHAR2 AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the sql statement to query the
            queue based on the given parameters.

    Input:
            queue_type                      varchar2
            tagged_indicator                varchar2 - Y/N if the item is tagged
            attached_email_indicator        varchar2 - Y/N if the email queue item is attached to an order
            sort_order                      varchar2 - extra sort column
            csr_id                          varchar2 - tagged by csr
            group_id                        varchar2 - tagged by group
            sort_direction                  varchar2 - asc/desc for the extra sort order
            exclude_partners                varchar2 - Partners To Exclude

    Output:
            the statement

    -----------------------------------------------------------------------------*/

    -- join clause variables
    v_Join VARCHAR2(4000);

    -- where clause variables
    v_Where VARCHAR2(4000);

    Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';
  BEGIN

    --if In_Csr_Id is NOT null, search was invoked for "Items Tagged to CSR_ID".  In this case, retrieve ALL data, including Walmart, Ariba, and Premier Collection
    --Else, exclude walmart and ariba orders.
    IF In_Csr_Id IS NULL then
      v_Where := Nvl(v_Where,'WHERE ') || ' nvl(q.origin_id,''TEST'') NOT in (''WLMTI'', ''ARI'', ''CAT'') AND ';
    END IF;

    -- build the join clause if attached email indicator is specified
    IF In_Attached_Email_Indicator IS NOT NULL
    THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN In_Attached_Email_Indicator = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';
    END IF;

    -- build the join clause if tagged items is specified
    IF In_Tagged_Indicator = 'Y'
    THEN
      v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';

      IF In_Csr_Id IS NOT NULL
      THEN
        v_Join := v_Join || 'AND t.csr_id=''' || In_Csr_Id || ''' ';
        --        v_Where_Tag_Csr_Id := 'AND t.csr_id=''' || In_Csr_Id || ''' ';
      END IF;

      IF In_Group_Id IS NOT NULL
      THEN
        v_Join := v_Join || 'JOIN aas.identity i ON t.csr_id=i.identity_id JOIN aas.users u ON i.user_id=u.user_id ' || 'AND u.cs_group_id=''' || In_Group_Id ||
                  ''' ';
      END IF;
    ELSE
      -- build the where clause if untagged items is specified
      v_Where := Nvl(v_Where
                    ,'WHERE ') || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
    END IF;

    -- build the where clause if the queue type is specified
    IF In_Queue_Type IS NOT NULL
    THEN
      v_Where := Nvl(v_Where
                    ,'WHERE ') || 'q.queue_type=''' || In_Queue_Type || ''' AND ';
      IF In_Queue_Type = 'GEN'
      THEN
        Out_Sort_Order := 'q.message_timestamp;';
      END IF;
    ELSE
      Out_Sort_Order := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
    END IF;
    Out_Sort_Order := Out_Sort_Order || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

    -- build the order by clause given the page column headings, the computed fields in the order by should
    -- correspond with computed fields in the end result set(the very last select statement)
    CASE Nvl(In_Sort_Order
        ,'NULL')
      WHEN 'NULL' THEN
        NULL;
      WHEN 'system' THEN
        IF In_Queue_Type = 'LP'
        THEN
          Out_Sort_Order := 'COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system) ' ||
                            In_Sort_Direction || ';' || Out_Sort_Order;
        ELSIF In_Queue_Type IS NOT NULL
        THEN
          Out_Sort_Order := 'COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                            'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id),''Held'',''HOLD'', NULL), q.system) ' ||
                            In_Sort_Direction || ';' || Out_Sort_Order;
        ELSE
          Out_Sort_Order := 'DECODE(q.queue_type, ''LP''' ||
                            ',COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' ||
                            ',COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                            'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' ||
                            In_Sort_Direction || ';' || Out_Sort_Order;
        END IF;

      WHEN 'delivery_date' THEN
        Out_Sort_Order := 'q.delivery_date ' || In_Sort_Direction || ', ' || REPLACE(Out_Sort_Order
                                                                                     ,'q.delivery_date,');

      WHEN 'timezone' THEN
        Out_Sort_Order := 'key_3 ' || In_Sort_Direction || ', ' || REPLACE(Out_Sort_Order
                                                                          ,'key_3,');

      WHEN 'same_day_gift' THEN
        Out_Sort_Order := 'NVL(q.same_day_gift, ''N'') ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'product_id' THEN
        Out_Sort_Order := 'q.product_id ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'untagged_new_items' THEN
        Out_Sort_Order := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' ||
                          ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || In_Sort_Direction || ';' || Out_Sort_Order;

      WHEN 'tagged_by' THEN
        IF In_Tagged_Indicator <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        Out_Sort_Order := 't.csr_id ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'tagged_on' THEN
        IF In_Tagged_Indicator <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        Out_Sort_Order := 't.tagged_on ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'last_touched' THEN
        Out_Sort_Order := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'order_detail_id' THEN
        Out_Sort_Order := 'q.order_detail_id ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'external_order_number' THEN
        Out_Sort_Order := 'q.external_order_number ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'member_number' THEN
        Out_Sort_Order := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' ||
                          '(SELECT v.FILLING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) '
                          || In_Sort_Direction || ', ' || Out_Sort_Order;
      WHEN 'delivery_location_type' THEN
        Out_Sort_Order := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || In_Sort_Direction || ', ' || Out_Sort_Order;
      WHEN 'zip_code' THEN
        Out_Sort_Order := ' q.recipient_zip_code ' || In_Sort_Direction || ', ' || Out_Sort_Order;
      WHEN 'occasion' THEN
          Out_Sort_Order := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'tag_priority' THEN
        IF In_Tagged_Indicator <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        Out_Sort_Order := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || In_Sort_Direction || ', ' ||
                          Out_Sort_Order;

      ELSE
        Out_Sort_Order := 'q.' || In_Sort_Order || ' ' || In_Sort_Direction || ', ' ||
                          REPLACE(REPLACE(Out_Sort_Order
                                         ,';q.' || Lower(In_Sort_Order))
                                 ,'q.' || Lower(In_Sort_Order) || ';');
    END CASE;
    Out_Sort_Order := REPLACE(REPLACE(Out_Sort_Order
                                     ,'key_3'
                                     ,Key_3)
                             ,';'
                             ,',');

    --if In_Csr_Id is NOT null, search was invoked for "Items Tagged to CSR_ID".  In this case, retrieve ALL data, including Walmart, Ariba, and Premier Collection
    --Any Premier Collection order in the (LP, CREDIT) queues will be displayed in the (LP, CREDIT) queues; these orders will NOT be displayed under "PREMIER_COLLECTION" queue
    IF ( In_Csr_Id IS NULL AND In_Queue_Type not in ('LP', 'CREDIT') ) THEN
      v_where := nvl(v_where,'Where') || ' q.message_id not in (select message_id from clean.queue q1 where q1.PREMIER_COLLECTION_FLAG = ''Y'' and q1.QUEUE_TYPE not in (''LP'', ''CREDIT'')) AND ';
    END IF;

    --IN_EXCLUDE_PARTNERS will not call this stored proc because they have their own virtual queue.  This stored proc will return these IN_EXCLUDE_PARTNERS info ONLY if
    --the search was invoked for a Group or a CSR.  Thus, prevent IN_EXCLUDE_PARTNERS data retrieval for all queues but the group
    IF ( In_Group_Id IS NULL and In_Csr_Id IS NULL) THEN
      v_where := nvl(v_where,'Where') || ' (q.partner_name is null or q.partner_name not in (' || IN_EXCLUDE_PARTNERS || ')) AND ';
    END IF;

    -- build the entire sql statement
    RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where, 1, Length(v_Where) - 4);

  END Get_Queue_Sql;

  PROCEDURE Get_Queue(In_Queue_Type               IN Queue.Queue_Type%TYPE
                     ,In_Tagged_Indicator         IN VARCHAR2
                     ,In_Attached_Email_Indicator IN VARCHAR2
                     ,In_Sort_Order               IN VARCHAR2
                     ,In_Start_Position           IN NUMBER
                     ,In_Max_Number_Returned      IN NUMBER
                     ,In_Csr_Id                   IN VARCHAR2
                     ,In_Group_Id                 IN VARCHAR2
                     ,In_Sort_Direction           IN VARCHAR2
                     ,IN_EXCLUDE_PARTNERS         IN VARCHAR2
                     ,IN_CONTEXT                  IN VARCHAR2
                     ,IN_SESSION_ID               IN VARCHAR2
                     ,Out_Cur                     OUT Types.Ref_Cursor
                     ,Out_Id_Count                OUT NUMBER
                     ,Out_Queue_Desc              OUT VARCHAR2) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the items in a particular
            queue based on the given parameters.

    Input:
            queue_type                      varchar2
            tagged_indicator                varchar2 - Y/N if the item is tagged
            attached_email_indicator        varchar2 - Y/N if the email queue item is attached to an order
            sort_order                      varchar2 - extra sort column
            start_position                  number - index of the start item in the id array
            max_number_returned             number - number of items in the result set
            csr_id                          varchar2 - tagged by csr
            group_id                        varchar2 - tagged by group
            sort_direction                  varchar2 - asc/desc for the extra sort order
            exclude_partners                varchar2 - Partners To Exclude

    Output:
            cursor containing all colums from queue
            number or items in the initial result set
            queue description

    -----------------------------------------------------------------------------*/

    v_Sql         VARCHAR2(4000);
    v_Sort_Option VARCHAR2(4000);

    -- cursor for the dynamic sql statement
    Sql_Cur Types.Ref_Cursor;

    -- cursor to get the queue description
    CURSOR Desc_Cur IS
      SELECT Description
        FROM /*clean.*/ Queue_Type_Val
       WHERE Queue_Type = In_Queue_Type;

    -- cursor to get the group description
    CURSOR Group_Cur IS
      SELECT Description
        FROM Ftd_Apps.Cs_Groups
       WHERE Cs_Group_Id = In_Group_Id;

  BEGIN

    IF In_Group_Id IS NOT NULL THEN
      OPEN Group_Cur;
      FETCH Group_Cur
        INTO Out_Queue_Desc;
      CLOSE Group_Cur;
    ELSIF In_Csr_Id IS NOT NULL THEN
      Out_Queue_Desc := In_Csr_Id;
    ELSE
      OPEN Desc_Cur;
      FETCH Desc_Cur
        INTO Out_Queue_Desc;
      CLOSE Desc_Cur;
    END IF;
    v_Sql := Get_Queue_Sql(In_Queue_Type
                          ,In_Tagged_Indicator
                          ,In_Attached_Email_Indicator
                          ,In_Sort_Order
                          ,In_Csr_Id
                          ,In_Group_Id
                          ,In_Sort_Direction
                          ,IN_EXCLUDE_PARTNERS
                          ,v_Sort_Option);
    -- execute the sql statement
    OPEN Sql_Cur FOR v_Sql;
    FETCH Sql_Cur
      INTO Out_Id_Count;
    CLOSE Sql_Cur;
    v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                       ,'count(*)'
                                                                       ,'q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' ||
                                                                       Nvl(In_Start_Position,1) || ' AND ' || (Nvl(In_Start_Position,1) + Nvl(In_Max_Number_Returned,Out_Id_Count) - 1);

    OPEN Out_Cur FOR
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.FILLING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion,
              clean.queue_pkg.GET_CUST_HOLD_REASON_CODE (''CUSTOMER_ID'', q.customer_id, ''Prefer'')   customer_hold_customer_id,
              q.partner_name,
              q.premier_collection_flag,
              pm.preferred_processing_resource,
              decode(pm.preferred_processing_resource, null, ''N'',
                  aas.get_permission(''' || in_context || ''', ''' || in_session_id || ''', pm.preferred_processing_resource, ''View'')
              ) preferred_partner_authorized,
              pm.display_name partner_display_name
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
      LEFT JOIN ftd_apps.partner_master pm on pm.partner_name = q.partner_name
      ORDER BY  lst.ord';
  END Get_Queue;


PROCEDURE GET_ORDER_COMPANY_EMAIL
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_COMPANY_ID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves the shipping and company information
        for the given order.

Input:
        order detail number

Output:
        ref cusor that contains shipping and company information

-----------------------------------------------------------------------------*/

CURSOR carrier_cur IS
        SELECT  carrier_delivery
        FROM    order_details
        WHERE   order_detail_id = in_order_detail_id;

CURSOR exists_cur IS
        SELECT  order_guid
        FROM    order_details
        WHERE   order_detail_id = in_order_detail_id;

v_carrier_delivery order_details.carrier_delivery%type;
v_order_guid       order_details.order_guid%type;

BEGIN

OPEN carrier_cur;
FETCH carrier_cur INTO v_carrier_delivery;
CLOSE carrier_cur;

IF v_carrier_delivery IS NOT NULL THEN
        OPEN out_cur FOR
                SELECT  t.tracking_number,
                        carr.shipper shipper,
                        carr.tracking_url tracking_url,
                        nvl (o.company_id, 'FTD') company_id,
                        (select cm.company_name from ftd_apps.company_master cm where cm.company_id = nvl (o.company_id, 'FTD') ) company_name,
                        (select ec.value from ftd_apps.email_company_data ec where ec.company_id = nvl (o.company_id, 'FTD') and name = 'Phone') cs_phone
                FROM    (select c.carrier_name shipper, c.website_url tracking_url from venus.carriers c JOIN venus.carrier_delivery cd on c.carrier_id = cd.carrier_id  where cd.carrier_delivery = v_carrier_delivery) carr,
                        order_details od
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                LEFT OUTER JOIN order_tracking t
                ON      t.order_detail_id = od.order_detail_id
                WHERE   od.order_detail_id = in_order_detail_id;
ELSE
        OPEN exists_cur;
        FETCH exists_cur INTO v_order_guid;
        CLOSE exists_cur;

        IF v_order_guid IS NOT NULL THEN
            OPEN out_cur FOR
                SELECT  null tracking_number,
                        null shipper,
                        null tracking_url,
                        c.company_id,
                        c.company_name,
                        (select ec.value from ftd_apps.email_company_data ec where ec.company_id = c.company_id and name = 'Phone') cs_phone
                FROM    clean.orders o
                JOIN    ftd_apps.company_master c
                ON      c.company_id = o.company_id
                WHERE   o.order_guid = v_order_guid;
        ELSE
            -- if the given order is not found then use in_company_id or default to FTD
            OPEN out_cur FOR
                SELECT  null tracking_number,
                        null shipper,
                        null tracking_url,
                        c.company_id,
                        c.company_name,
                        (select ec.value from ftd_apps.email_company_data ec where ec.company_id = c.company_id and name = 'Phone') cs_phone
                FROM    ftd_apps.company_master c
                WHERE   c.company_id = decode(in_company_id, null, 'FTD', 
                                              nvl((select c2.company_id from ftd_apps.company_master c2 where c2.company_id=in_company_id), 
                                              'FTD'));
        END IF;
END IF;

END GET_ORDER_COMPANY_EMAIL;


PROCEDURE GET_TAG_PRIORITY_VAL
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the tag priority values

Input:
        tag_priority                    varchar2
        description                     varchar2
        sort_order                      number

Output:
        cursor containing all records from tag_priority_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                tag_priority,
                description
        FROM    tag_priority_val
        ORDER BY sort_order;

END GET_TAG_PRIORITY_VAL;

PROCEDURE GET_TAG_DISPOSITION_VAL
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the tag disposition values

Input:
        tag_disposition                 varchar2
        description                     varchar2
        sort_order                      number

Output:
        cursor containing all records from tag_disposition_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                tag_disposition,
                description
        FROM    tag_disposition_val
        ORDER BY sort_order;

END GET_TAG_DISPOSITION_VAL;

PROCEDURE GET_ORDER_TAG_CSR
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the csr ids who have
        tagged the given order

Input:
        order_detail_id         number

Output:
        cursor containing the csr's who tagged the given order

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                t.csr_id
        FROM    queue_tag t
        JOIN    queue q
        ON      t.message_id = q.message_id
        AND     q.order_detail_id = in_order_detail_id
        AND     q.queue_type = 'ORDER'
        ORDER BY t.tagged_on;

END GET_ORDER_TAG_CSR;

PROCEDURE GET_TAG_CSR
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the csr ids who have
        tagged the given order or a message on this order

Input:
        order_detail_id         number

Output:
        cursor containing the csr's who tagged the anything related to
        a given order

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT DISTINCT
                t.csr_id
        FROM    queue_tag t
        JOIN    queue q
        ON      t.message_id = q.message_id
        AND     q.order_detail_id = in_order_detail_id;

END GET_TAG_CSR;

PROCEDURE DELETE_LOSS_PREVENT_QUEUE_REC
(
 IN_ORDER_DETAIL_ID   IN QUEUE.ORDER_DETAIL_ID%TYPE,
 IN_QUEUE_TYPE        IN QUEUE.QUEUE_TYPE%TYPE,
 IN_CSR_ID            IN QUEUE_TAG.CSR_ID%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_ERROR_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
  Description:
        This stored procedure deletes all record from table queue with the
        corresponding order detail id and queue type passed in.
        Note, the record is not checked to see if the CSR has authority even if
        the record is tagged.

  Input:
        order_detail_id     varchar2
        queue_type          varchar2
        csr_id              varchar2

  Output:
        status
        error message

-----------------------------------------------------------------------------*/

v_count   NUMBER := 0;

BEGIN

  BEGIN
     SELECT count(1)
       INTO v_count
       FROM queue_tag qt,
            queue q
      WHERE q.order_detail_id = in_order_detail_id
        AND q.queue_type = in_queue_type
        AND qt.message_id = q.message_id;


    EXCEPTION WHEN NO_DATA_FOUND THEN v_count := 0;
  END;

  IF v_count = 0 THEN
     -- queue item was not tagged so insert the queue history record
     INSERT INTO queue_delete_history
           (
            message_id,
            queue_type,
            queue_deleted_by,
            queue_deleted_on,
            order_guid,
            order_detail_id,
            master_order_number,
            external_order_number,
            queue_created_on
           )
           SELECT message_id,
                  queue_type,
                  in_csr_id,
                  sysdate,
                  order_guid,
                  order_detail_id,
                  master_order_number,
                  external_order_number,
                  created_on
             FROM queue
            WHERE order_detail_id = in_order_detail_id
              AND queue_type = in_queue_type;

     -- delete the queue item
     DELETE FROM queue
      WHERE order_detail_id = in_order_detail_id
        AND queue_type = in_queue_type;

  ELSE

     -- insert the queue history record for the tagged item
     INSERT INTO queue_delete_history
          (
           message_id,
           queue_type,
           tagged_by,
           tagged_on,
           queue_deleted_by,
           queue_deleted_on,
           order_guid,
           order_detail_id,
           master_order_number,
           external_order_number,
           queue_created_on
          )
          SELECT q.message_id,
                 q.queue_type,
                 qt.csr_id,
                 qt.tagged_on,
                 in_csr_id,
                 sysdate,
                 q.order_guid,
                 q.order_detail_id,
                 q.master_order_number,
                 q.external_order_number,
                 q.created_on
            FROM queue q,
                 queue_tag qt
           WHERE q.order_detail_id = in_order_detail_id
             AND q.queue_type = in_queue_type
             AND q.message_id = qt.message_id;

     DELETE FROM queue_tag
           WHERE message_id IN (SELECT message_id
                                  FROM queue
                                 WHERE order_detail_id = in_order_detail_id
                                   AND queue_type = in_queue_type);

     DELETE FROM queue
           WHERE order_detail_id = in_order_detail_id
             AND queue_type = in_queue_type;
  END IF;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_LOSS_PREVENT_QUEUE_REC;


FUNCTION IS_ORDER_IN_CREDIT_Q
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns if the order is in the credit queue.  If the
        order is in the credit queue, the order cannot be modified.

Input:
        order_detail_id         number

Output:
        Y/N if the order in the credit queue
-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT  'Y'
        FROM    queue q
        WHERE   q.order_detail_id = in_order_detail_id
        AND     q.queue_type = 'CREDIT';

v_check_ind     char (1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check_ind;
CLOSE check_cur;

RETURN v_check_ind;

END IS_ORDER_IN_CREDIT_Q;


PROCEDURE IS_ORDER_IN_CREDIT_Q_HISTORY
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns if the order is in the credit queue or the queue
        delete history for the credit queue.  This procedure is used to
        determine if the order has been declined or bypassed authorization.

Input:
        order_detail_id         number

Output:
        Y/N if the order is in the credit queue,
        Y/N if the order is in credit queue history
-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  q.message_id,
                'N' in_credit_queue_history
        FROM    queue q
        WHERE   q.order_detail_id = in_order_detail_id
        AND     q.queue_type = 'CREDIT'
        UNION
        SELECT  null message_id,
                'Y' in_credit_queue_history
        FROM    queue_delete_history q
        WHERE   q.order_detail_id = in_order_detail_id
        AND     q.queue_type = 'CREDIT';

END IS_ORDER_IN_CREDIT_Q_HISTORY;


FUNCTION GET_TAG_EXPIRE_DATE
(
IN_TAGGED_ON                    IN QUEUE_TAG.TAGGED_ON%TYPE,
IN_TAG_PRIORITY                 IN QUEUE_TAG.TAG_PRIORITY%TYPE
)
RETURN DATE
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the expire date for the given tag
        date and priority

Input:
        tagged_on               date
        tag_priority            varchar2

Output:
        date the tag will expire

-----------------------------------------------------------------------------*/

CURSOR val_cur IS
        SELECT  expire_tag_value
        FROM    tag_priority_val
        WHERE   tag_priority = in_tag_priority;

v_expire_tag_value      tag_priority_val.expire_tag_value%type;
v_return                date;

BEGIN

OPEN val_cur;
FETCH val_cur INTO v_expire_tag_value;
CLOSE val_cur;

IF v_expire_tag_value IS NOT NULL THEN
        CASE v_expire_tag_value
        WHEN 'AM' THEN
                -- the tag will expire at 6am on the next day
                v_return := trunc (in_tagged_on) + 1 + 6/24;

        WHEN 'PM' THEN
                -- the tag will expire at noon on the next day
                v_return := trunc (in_tagged_on) + 1 + 12/24;
        ELSE
                -- the tag will expire after the given hours
                v_return := in_tagged_on + to_number (v_expire_tag_value) / 24;

        END CASE;
END IF;

RETURN v_return;

END GET_TAG_EXPIRE_DATE;


FUNCTION IS_TAG_EXPIRED
(
IN_TAGGED_ON                    IN QUEUE_TAG.TAGGED_ON%TYPE,
IN_TAG_PRIORITY                 IN QUEUE_TAG.TAG_PRIORITY%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure return Y/N if the tag has expired

Input:
        tagged_on               date
        tag_priority            varchar2

Output:
        Y/N if the tag has expired

-----------------------------------------------------------------------------*/

v_expire_date   date;
v_return        varchar2(1) := 'N';

BEGIN

v_expire_date := get_tag_expire_date (in_tagged_on, in_tag_priority);

IF v_expire_date <= sysdate THEN
        v_return := 'Y';
END IF;

RETURN v_return;

END IS_TAG_EXPIRED;




  FUNCTION Get_Queue_Sql_Wlmt_Ariba
                        (In_origin_id                IN CLEAN.QUEUE.ORIGIN_ID%TYPE
                        ,In_Tagged_Indicator         IN VARCHAR2
                        ,In_Attached_Email_Indicator IN VARCHAR2
                        ,In_Sort_Order               IN VARCHAR2
                        ,In_Sort_Direction           IN VARCHAR2
                        ,in_queue_indicator          in varchar2
                        ,Out_Sort_Order              OUT VARCHAR2) RETURN VARCHAR2 AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the sql statement to query the
            queue based on the given parameters.

    Input:
            origin_id                       varchar2
            tagged_indicator                varchar2 - Y/N if the item is tagged
            attached_email_indicator        varchar2 - Y/N if the email queue item is attached to an order
            sort_order                      varchar2 - extra sort column
            sort_direction                  varchar2 - asc/desc for the extra sort order
            queue_indicator                 varchar2 - Mercury or Email

    Output:
            the statement

    -----------------------------------------------------------------------------*/

    -- join clause variables
    v_Join VARCHAR2(4000);

    -- where clause variables
    v_Where VARCHAR2(4000) := 'WHERE ';

    Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';
  BEGIN
    -- build the join clause if attached email indicator is specified
    IF In_Attached_Email_Indicator IS NOT NULL
    THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN In_Attached_Email_Indicator = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';

    ELSE
    -- otherwise, limit the results to the specified queue indicator
    if  in_queue_indicator = 'Mercury' then
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator in (''Mercury'', ''Order'') ';
    else
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' ';
    END IF;
    END IF;

    -- build the join clause if tagged items is specified
    IF In_Tagged_Indicator = 'Y'
    THEN
      v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';

    ELSE
      -- build the where clause if untagged items is specified
      v_Where := v_Where || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
    END IF;

    -- build the sort order
    Out_Sort_Order := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
    Out_Sort_Order := Out_Sort_Order || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

    -- build the order by clause given the page column headings, the computed fields in the order by should
    -- correspond with computed fields in the end result set(the very last select statement)
    CASE Nvl(In_Sort_Order
        ,'NULL')
      WHEN 'NULL' THEN
        NULL;
      WHEN 'system' THEN
          Out_Sort_Order := 'DECODE(q.queue_type, ''LP''' ||
                            ',COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' ||
                            ',COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                            'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' ||
                            In_Sort_Direction || ';' || Out_Sort_Order;

      WHEN 'delivery_date' THEN
        Out_Sort_Order := 'q.delivery_date ' || In_Sort_Direction || ', ' || REPLACE(Out_Sort_Order
                                                                                     ,'q.delivery_date,');

      WHEN 'timezone' THEN
        Out_Sort_Order := 'key_3 ' || In_Sort_Direction || ', ' || REPLACE(Out_Sort_Order
                                                                          ,'key_3,');

      WHEN 'same_day_gift' THEN
        Out_Sort_Order := 'NVL(q.same_day_gift, ''N'') ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'product_id' THEN
        Out_Sort_Order := 'q.product_id ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'untagged_new_items' THEN
        Out_Sort_Order := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' ||
                          ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || In_Sort_Direction || ';' || Out_Sort_Order;

      WHEN 'tagged_by' THEN
        IF In_Tagged_Indicator <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        Out_Sort_Order := 't.csr_id ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'tagged_on' THEN
        IF In_Tagged_Indicator <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        Out_Sort_Order := 't.tagged_on ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'last_touched' THEN
        Out_Sort_Order := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'order_detail_id' THEN
        Out_Sort_Order := 'q.order_detail_id ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'external_order_number' THEN
        Out_Sort_Order := 'q.external_order_number ' || In_Sort_Direction || ', ' || Out_Sort_Order;

      WHEN 'member_number' THEN
        Out_Sort_Order := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' ||
                          '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) '
                          || In_Sort_Direction || ', ' || Out_Sort_Order;
      WHEN 'delivery_location_type' THEN
        Out_Sort_Order := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || In_Sort_Direction || ', ' || Out_Sort_Order;
      WHEN 'zip_code' THEN
        Out_Sort_Order := ' q.recipient_zip_code ' || In_Sort_Direction || ', ' || Out_Sort_Order;
      WHEN 'occasion' THEN
          Out_Sort_Order := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || In_Sort_Direction || ', ' || Out_Sort_Order;


      WHEN 'tag_priority' THEN
        IF In_Tagged_Indicator <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        Out_Sort_Order := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || In_Sort_Direction || ', ' ||
                          Out_Sort_Order;

      ELSE
        Out_Sort_Order := 'q.' || In_Sort_Order || ' ' || In_Sort_Direction || ', ' ||
                          REPLACE(REPLACE(Out_Sort_Order
                                         ,';q.' || Lower(In_Sort_Order))
                                 ,'q.' || Lower(In_Sort_Order) || ';');
    END CASE;
    Out_Sort_Order := REPLACE(REPLACE(Out_Sort_Order
                                     ,'key_3'
                                     ,Key_3)
                             ,';'
                             ,',');

    --depending on the origin type passed in, select WLMTI or (ARI, CAT) origin orders
    IF in_origin_id = 'WALMART' THEN
        -- get walmart queued orders
        v_where := v_where || ' q.origin_id in ( ''WLMTI'' ) AND ';
    ELSE
        -- get ariba queued orders
        v_where := v_where || ' q.origin_id in ( ''ARI'', ''CAT'') AND ';
    END IF;

    -- build the entire sql statement
    RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where,1,Length(v_Where) - 4);

  END Get_Queue_Sql_Wlmt_Ariba;

  PROCEDURE Get_Queue_Wlmt_Ariba
                     (In_origin_id                IN CLEAN.QUEUE.ORIGIN_ID%TYPE
                     ,In_Tagged_Indicator         IN VARCHAR2
                     ,In_Attached_Email_Indicator IN VARCHAR2
                     ,In_Sort_Order               IN VARCHAR2
                     ,In_Start_Position           IN NUMBER
                     ,In_Max_Number_Returned      IN NUMBER
                     ,In_Sort_Direction           IN VARCHAR2
                     ,in_queue_indicator          in varchar2
                     ,Out_Cur                     OUT Types.Ref_Cursor
                     ,Out_Id_Count                OUT NUMBER
                     ,Out_Queue_Desc              OUT VARCHAR2) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the items in a particular
            queue based on the given parameters for Walmart or Ariba orders

    Input:
            origin_id                       varchar2
            tagged_indicator                varchar2 - Y/N if the item is tagged
            attached_email_indicator        varchar2 - Y/N if the email queue item is attached to an order
            sort_order                      varchar2 - extra sort column
            start_position                  number - index of the start item in the id array
            max_number_returned             number - number of items in the result set
            sort_direction                  varchar2 - asc/desc for the extra sort order
            queue_indicator                 varchar2 - Mercury or Email

    Output:
            cursor containing all colums from queue
            number or items in the initial result set
            queue description

    -----------------------------------------------------------------------------*/

    v_Sql         VARCHAR2(4000);
    v_Sort_Option VARCHAR2(4000);

    -- cursor for the dynamic sql statement
    Sql_Cur Types.Ref_Cursor;

    -- cursor to get the queue description
    CURSOR Desc_Cur IS
      SELECT Description
        FROM ftd_apps.origins
       WHERE origin_id = decode (In_origin_id, 'WALMART', 'WLMTI', 'ARIBA', 'ARI', in_origin_id);

  BEGIN

      OPEN Desc_Cur;
      FETCH Desc_Cur
        INTO Out_Queue_Desc;
      CLOSE Desc_Cur;

    v_Sql := Get_Queue_Sql_Wlmt_Ariba
                          (In_origin_id
                          ,In_Tagged_Indicator
                          ,In_Attached_Email_Indicator
                          ,In_Sort_Order
                          ,In_Sort_Direction
                          ,in_queue_indicator
                          ,v_Sort_Option);
    -- execute the sql statement
    OPEN Sql_Cur FOR v_Sql;
    FETCH Sql_Cur
      INTO Out_Id_Count;
    CLOSE Sql_Cur;
    v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                       ,'count(*)'
                                                                       ,'q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' ||
                                                                       Nvl(In_Start_Position,1) || ' AND ' || (Nvl(In_Start_Position,1) + Nvl(In_Max_Number_Returned,Out_Id_Count) - 1);


    OPEN Out_Cur FOR
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion,
              q.premier_collection_flag
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
      ORDER BY  lst.ord';

  END Get_Queue_Wlmt_Ariba;


PROCEDURE GET_MQD_QUEUE_TYPES (
   OUT_CUR                         OUT TYPES.REF_CURSOR) AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all the queue types

Input:

Output:
        cursor containing queue_type

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  queue_type, description
        FROM    MQD_QUEUE_TYPE_VAL WHERE active='Y'
        ORDER BY description;

END GET_MQD_QUEUE_TYPES;

PROCEDURE GET_QUEUE_TYPES (
   OUT_CUR                         OUT TYPES.REF_CURSOR) AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all the queue types

Input:

Output:
        cursor containing queue_type

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT  queue_type, description
        FROM    queue_type_val
        ORDER BY description;

END GET_QUEUE_TYPES;

FUNCTION TOKENIZER
(IN_STRING IN VARCHAR2,
 IN_SEPARATORS IN VARCHAR2
)
RETURN dbms_sql.varchar2s
IS
/*------------------------------------------------------------------------------
Description:
   Returns a table of tokenized values.
   optional input input parameters.

Input:
        in_string             VARCHAR2
        in_separators         VARCHAR2

Output:
        table
------------------------------------------------------------------------------*/
v_strs dbms_sql.varchar2s;
BEGIN
  WITH sel_string AS
      (SELECT in_string fullstring
         FROM DUAL)
  SELECT substr(fullstring, beg+1, end_p-beg-1) token
   BULK COLLECT INTO v_strs
   FROM (SELECT beg, lead(beg) over (order by beg) end_p, fullstring
           FROM (SELECT beg, fullstring
                   FROM (SELECT level beg, fullstring
                           FROM sel_string
                         connect by level <= length(fullstring))
                   WHERE instr(in_separators ,substr(fullstring,beg,1)) >0
                 UNION ALL
                 SELECT 0, fullstring FROM sel_string
                 UNION ALL
                 SELECT length(fullstring)+1, fullstring FROM sel_string))
    WHERE end_p IS NOT NULL
      AND end_p>beg+1;
  RETURN v_strs;

END TOKENIZER;

PROCEDURE GET_QUEUE_MESSAGES_BY_SEARCH (
 IN_SYSTEM                  IN CLEAN.QUEUE.SYSTEM%TYPE,
 IN_QUEUE_TYPE              IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
 IN_MESSAGE_ID              IN CLEAN.QUEUE.MESSAGE_ID%TYPE,
 IN_MESSAGE_DIRECTION       IN MERCURY.MERCURY.MESSAGE_DIRECTION%TYPE,
 --MASS QUEUE DELETE ENHANCEMENTS
 IN_NON_PARTNER             IN VARCHAR2,
 IN_PRODUCT_PROPERTY_INCL   IN VARCHAR2,
 IN_PARTNER_INCL            IN VARCHAR2,
 IN_EMAIL_ADDRESS           IN VARCHAR2,
 IN_AMOUNT_FROM             IN MERCURY.MERCURY.PRICE%TYPE,
 IN_AMOUNT_TO               IN MERCURY.MERCURY.PRICE%TYPE,
 IN_SEARCH_TEXT1            IN MERCURY.MERCURY.COMMENTS%TYPE,
 IN_SEARCH_TEXT2            IN MERCURY.MERCURY.COMMENTS%TYPE,
 IN_SEARCH_TEXT3            IN MERCURY.MERCURY.COMMENTS%TYPE,
 IN_SEARCH_COND1            IN VARCHAR2,
 IN_SEARCH_COND2            IN VARCHAR2,
 IN_ASK_ANS_CODE            IN MERCURY.MERCURY.ASK_ANSWER_CODE%TYPE,
 --END OF MASS QUEUE DELETE CHANGES.
  OUT_CURSOR                 OUT TYPES.REF_CURSOR


)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the Mercury or Venus queue messages for the
   optional input input parameters.

Input:
        in_queue_type             VARCHAR2
        in_system                 VARCHAR2
        in_message_id             number
        in_message_direction      VARCHAR2
        in_product_property_excl  varchar2 (contains a list of comma delimited product_master columns with 'Y' value that need to be excluded)

Output:
        cursor containing queue info
------------------------------------------------------------------------------*/

v_sql                     VARCHAR2(8000);

v_queue_type_where        VARCHAR2(200) := ' ';
v_message_id_where        VARCHAR2(200) := ' ';
v_message_direction_where VARCHAR2(200) := ' ';
v_email_address_where     VARCHAR2(200) := ' ';
v_price_range_where       VARCHAR2(200) := ' ';

--product properties
v_prod_prop_all           VARCHAR2(400) := ' ';
v_prod_prop_selected      VARCHAR2(400) := ' ';
v_prod_prop_none          VARCHAR2(400) := ' ';
v_prod_prop_where         VARCHAR2(400) := ' ';
v_prodPropFound           boolean := false;
v_prodProperties_where    VARCHAR2(1000) := ' ';
v_str_prodProperties      dbms_sql.varchar2s;

--keyword search
v_keyword_search_where    VARCHAR2(400) := ' ';
v_keyword_search1         VARCHAR2(100) := ' ';
v_keyword_search2         VARCHAR2(100) := ' ';
v_keyword_search3         VARCHAR2(100) := ' ';

--partner properties
v_partner_where           VARCHAR2(1000) := ' ';
v_includePartners         boolean := false;
v_str_partners            dbms_sql.varchar2s;
v_includePartner_str      VARCHAR2(400);
v_partner_names           VARCHAR2(400);


BEGIN

   -- Add treatment for variables to be used in dynamic sql.

  IF IN_EMAIL_ADDRESS IS NOT NULL AND IN_EMAIL_ADDRESS  = 'off' THEN
    v_email_address_where := ' AND (select o.email_id from clean.orders o where o.order_guid = cq.order_guid) is not null';
  END IF;

  IF IN_AMOUNT_FROM IS NOT NULL AND IN_AMOUNT_TO IS NOT NULL THEN
    --PRICE RANGE CONDITION
    v_price_range_where := ' AND cq.PRICE_ASKP - cq.PRICE_FTD between '||IN_AMOUNT_FROM||' and '||IN_AMOUNT_TO;
  END IF;

  --Build the search text string.  
  IF IN_SEARCH_TEXT2 IS NOT NULL AND LENGTH(IN_SEARCH_TEXT2) > 0 THEN
    v_keyword_search2 := v_keyword_search2 || ' ' || IN_SEARCH_COND1;
    v_keyword_search2 := v_keyword_search2 || q'[ UPPER(cq.MERCURY_COMMENTS) LIKE q'{%]' || replace(replace(UPPER(IN_SEARCH_TEXT2),'%','\%'),'_','\_') || q'[%}' escape '\']';
  END IF;

  IF IN_SEARCH_TEXT3 IS NOT NULL AND LENGTH(IN_SEARCH_TEXT3) > 0 THEN
    v_keyword_search3 := v_keyword_search3 || ' ' || IN_SEARCH_COND2;
    v_keyword_search3 := v_keyword_search3 || q'[ UPPER(cq.MERCURY_COMMENTS) LIKE q'{%]' || replace(replace(UPPER(IN_SEARCH_TEXT3),'%','\%'),'_','\_') || q'[%}' escape '\']';
  END IF;

  --If search text 1 is found, build 1) search text1 AND 2) search text WHERE.  
  IF IN_SEARCH_TEXT1 IS NOT NULL AND LENGTH(IN_SEARCH_TEXT1) > 0 THEN
    v_keyword_search1 := v_keyword_search1 || q'[ UPPER(cq.MERCURY_COMMENTS) LIKE q'{%]' || replace(replace(UPPER(IN_SEARCH_TEXT1),'%','\%'),'_','\_') || q'[%}' escape '\']';

    --If IN_SEARCH_TEXT1 is not null, => we HAVE to build v_keyword_search_where.
    v_keyword_search_where := v_keyword_search_where || ' AND ('  || v_keyword_search1 || v_keyword_search2 || v_keyword_search3 || ' )';

  END IF;

  IF in_queue_type IS NOT NULL THEN
    v_queue_type_where := ' AND cq.queue_type = ''' || in_queue_type || '''';
    IF IN_SYSTEM = 'Merc' THEN
      IF IN_ASK_ANS_CODE = 'P' THEN
        v_queue_type_where := v_queue_type_where || ' AND mm.ASK_ANSWER_CODE = ''P''';
      ELSE
        IF in_queue_type = 'ASK' THEN
          v_queue_type_where := v_queue_type_where || ' AND (mm.ASK_ANSWER_CODE IS NULL OR mm.ASK_ANSWER_CODE != ''P'')';
        END IF;
      END IF;
    END IF;
  END IF;

  IF in_message_id IS NOT NULL THEN
    v_message_id_where := ' AND cq.message_id = ' || in_message_id ;
  END IF;

  IF in_message_direction IS NOT NULL THEN
    IF in_system = 'Merc' THEN
      v_message_direction_where := ' AND mm.message_direction = ''' || in_message_direction || '''';
    ELSIF in_system IN ('Venus','Argo','FTD WEST') THEN
      v_message_direction_where := ' AND vv.message_direction = ''' || in_message_direction || '''';
    END IF;
  END IF;

  --Include Preferred Partners.
  IF IN_PARTNER_INCL IS NOT NULL AND LENGTH(IN_PARTNER_INCL) > 0 THEN
    v_includePartners := true;
    v_str_partners := tokenizer(IN_PARTNER_INCL, ',');
    FOR j IN v_str_partners.FIRST..v_str_partners.LAST LOOP
      v_partner_names := v_partner_names||''''||trim(v_str_partners(j))||''',';
    END LOOP;
  END IF;

  IF NOT v_includePartners THEN
    v_partner_where := v_partner_where || 'AND (cq.PARTNER_NAME IS NULL or cq.PREFERRED_PARTNER_FLAG = ''N'')';
  ELSIF IN_NON_PARTNER = 'on' AND v_includePartners THEN
    v_partner_where := v_partner_where || 'AND (cq.PARTNER_NAME IS NULL or cq.PREFERRED_PARTNER_FLAG = ''N'' or cq.partner_name in('||substr(v_partner_names,1,length(v_partner_names)-1)||'))';
  ELSIF IN_NON_PARTNER = 'off' AND v_includePartners THEN
    v_partner_where := v_partner_where || 'AND (cq.PARTNER_NAME IS NOT NULL and cq.partner_name in('||substr(v_partner_names,1,length(v_partner_names)-1)||'))';
  END IF;


  --Include prod properties.
  IF IN_PRODUCT_PROPERTY_INCL IS NOT NULL AND LENGTH(IN_PRODUCT_PROPERTY_INCL) > 0 THEN
    v_prodPropFound := true;
    v_str_prodProperties := tokenizer(IN_PRODUCT_PROPERTY_INCL, ',');
    FOR i IN v_str_prodProperties.FIRST..v_str_prodProperties.LAST LOOP
      v_prod_prop_all := v_prod_prop_all || ' AND (cq.'||v_str_prodProperties(i)|| ' IS NULL OR cq.'|| v_str_prodProperties(i)||' = ''Y'' OR cq.'||v_str_prodProperties(i)||' = ''N'')';
      v_prod_prop_selected := v_prod_prop_selected || ' AND (cq.'|| v_str_prodProperties(i)||' = ''Y'')';
    END LOOP;
  ELSE
    v_prod_prop_none := v_prod_prop_none || ' AND (cq.PREMIER_COLLECTION_FLAG  IS NULL OR cq.PREMIER_COLLECTION_FLAG = ''N'')';
  END IF;

  IF IN_NON_PARTNER = 'on' or v_includePartners THEN
    IF v_prodPropFound THEN
      v_prod_prop_where := v_prod_prop_all;
    ELSE
      v_prod_prop_where := v_prod_prop_none;
    END IF;
  ELSE
    IF v_prodPropFound THEN
      v_prod_prop_where := v_prod_prop_selected;
    END IF;
  END IF;


  -- Clean up the inputs.
  IF in_system = 'Merc' THEN
    v_sql :=
            'SELECT DISTINCT cq.message_id, cq.order_detail_id, cq.queue_type, cq.message_type, cq.message_timestamp, cq.mercury_number,
                    cq.external_order_number,
                    TRANSLATE(TRANSLATE(TRANSLATE(decode(mm.MESSAGE_DIRECTION, ''OUTBOUND'', mm.FILLING_FLORIST, mm.SENDING_FLORIST), chr(10), '' ''), chr(13), '' ''), chr(9), '' '') sending_entity,
                    cq.price_ftd  price, cq.price_askp,
                    TRANSLATE(TRANSLATE(TRANSLATE(mm.operator, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') operator,
                    cq.premier_collection_flag,
                    TRANSLATE(TRANSLATE(TRANSLATE(cq.mercury_comments, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') comments,
                    cq.partner_name,
                    cqt.csr_id is_tagged,
                    to_char(coa.order_detail_id) has_addons,
                    cq.delivery_date,
                    pm.novator_id
               FROM clean.queue cq
               LEFT JOIN clean.queue_tag cqt
                  ON cq.message_id = cqt.message_id
               LEFT JOIN clean.ORDER_ADD_ONS coa
                  ON cq.order_detail_id = coa.order_detail_id
               JOIN mercury.mercury mm
                 ON cq.mercury_id=mm.mercury_id
               JOIN ftd_apps.product_master pm
                 ON cq.product_id = pm.product_id
              WHERE cq.system = ''Merc'''
                || v_queue_type_where
                 || v_message_id_where
                 || v_message_direction_where
                 || v_prod_prop_where
                 || v_partner_where
                 || v_email_address_where
                 || v_price_range_where
                 || v_keyword_search_where
            ;
  ELSIF in_system IN ('Venus','Argo') THEN
    v_sql :=
            'SELECT DISTINCT cq.message_id, cq.order_detail_id, cq.queue_type, cq.message_type, cq.message_timestamp, cq.mercury_number,
                    cq.external_order_number,
                    TRANSLATE(TRANSLATE(TRANSLATE(vv.filling_vendor, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') sending_entity,
                    vv.price price, null price_askp,
                    TRANSLATE(TRANSLATE(TRANSLATE(vv.operator, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') operator,
                    cq.premier_collection_flag,
                    TRANSLATE(TRANSLATE(TRANSLATE(vv.comments, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') comments,
                    cq.partner_name,
                    cqt.csr_id is_tagged,
                    to_char(coa.order_detail_id) has_addons,
                    cq.delivery_date,
                    pm.novator_id
               FROM clean.queue cq
               LEFT JOIN clean.queue_tag cqt
                  ON cq.message_id = cqt.message_id
               LEFT JOIN clean.ORDER_ADD_ONS coa
                  ON cq.order_detail_id = coa.order_detail_id   
               JOIN venus.venus vv
                 ON cq.mercury_id=vv.venus_id
               JOIN ftd_apps.product_master pm
                 ON cq.product_id = pm.product_id
              WHERE cq.system in (''Argo'', ''Venus'')'
                 || v_queue_type_where
                 || v_message_id_where
                 || v_message_direction_where
                 || v_prod_prop_where
                 || v_partner_where
                 || v_email_address_where
            ;

  ELSIF in_system IN ('FTD WEST') THEN
    v_sql :=
            'SELECT DISTINCT cq.message_id, cq.order_detail_id, cq.queue_type, cq.message_type, cq.message_timestamp, cq.mercury_number,
                    cq.external_order_number,
                    TRANSLATE(TRANSLATE(TRANSLATE(vv.filling_vendor, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') sending_entity,
                    vv.price price, null price_askp,
                    TRANSLATE(TRANSLATE(TRANSLATE(vv.operator, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') operator,
                    cq.premier_collection_flag,
                    TRANSLATE(TRANSLATE(TRANSLATE(vv.comments, chr(10), '' ''), chr(13), '' ''), chr(9), '' '') comments,
                    cq.partner_name,
                    cqt.csr_id is_tagged,
                    to_char(coa.order_detail_id) has_addons,
                    cq.delivery_date,
                    pm.novator_id
               FROM clean.queue cq
               LEFT JOIN clean.queue_tag cqt
                  ON cq.message_id = cqt.message_id
               LEFT JOIN clean.ORDER_ADD_ONS coa
                  ON cq.order_detail_id = coa.order_detail_id
               JOIN venus.venus vv
                 ON cq.mercury_id=vv.venus_id
               JOIN ftd_apps.product_master pm
                 ON cq.product_id = pm.product_id
              WHERE cq.system in (''FTD WEST'')'
                 || v_queue_type_where
                 || v_message_id_where
                 || v_message_direction_where
                 || v_prod_prop_where
                 || v_partner_where
                 || v_email_address_where
            ;
  ELSE
    v_sql :=
            'SELECT DISTINCT cq.message_id, cq.order_detail_id, cq.queue_type, cq.message_type, cq.message_timestamp, cq.mercury_number,
                    cq.external_order_number, null sending_entity, null price, null price_askp, null operator, cq.premier_collection_flag,
                    decode(cq.queue_type, ''OT'', poc.email_subject, poc.email_subject || '' : '' ||
                        TRANSLATE(TRANSLATE(TRANSLATE(substr(poc.body, 1,  2000), chr(10), '' ''), chr(13), '' ''), chr(9), '' '')) comments,
                    cq.partner_name,
                    cqt.csr_id is_tagged,
                    to_char(coa.order_detail_id) has_addons,
                    cq.delivery_date,
                    pm.novator_id
               FROM clean.queue cq
               LEFT JOIN clean.queue_tag cqt
                  ON cq.message_id = cqt.message_id
               LEFT JOIN clean.ORDER_ADD_ONS coa
                  ON cq.order_detail_id = coa.order_detail_id
               JOIN clean.point_of_contact poc ON poc.point_of_contact_id = cq.point_of_contact_id
               JOIN clean.mqd_queue_type_val qtv ON cq.queue_type = qtv.queue_type
               JOIN ftd_apps.product_master pm ON cq.product_id = pm.product_id
              WHERE  qtv.queue_indicator = ''Email'''
                 || v_queue_type_where
                 || v_message_id_where
                 || v_prod_prop_where
                 || v_partner_where
                 || v_email_address_where
            ;
   END IF;

   OPEN out_cursor for v_sql;

   EXCEPTION
     WHEN OTHERS THEN
       dbms_output.put_line(v_sql);

END GET_QUEUE_MESSAGES_BY_SEARCH;




PROCEDURE GET_QUEUE_MESSAGE_BY_ID
(
 IN_MESSAGE_ID       IN QUEUE.MESSAGE_ID%TYPE,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
   Returns all the Mercury or Venus queue messages for the
   optional input input parameters.

Input:
        in_message_id             number

Output:
        cursor containing queue info
------------------------------------------------------------------------------*/

BEGIN

   -- Clean up the inputs.
   open out_cursor for
    SELECT cq.message_id, cq.order_detail_id, cq.queue_type, cq.message_type,
              cq.message_timestamp, cq.mercury_number, cq.external_order_number,
              cq.mercury_id, cq.system, qtv.description, qtv.queue_indicator,
              cq.partner_name,  cq.price_ftd, cq.price_askp,
              coalesce
              (
                (select distinct m.mercury_order_number from mercury.mercury m where m.mercury_id           = cq.mercury_id),
                (select distinct m.mercury_order_number from mercury.mercury m where m.mercury_order_number = cq.mercury_number),
                (select distinct v.venus_order_number from venus.venus v where v.venus_id                   = cq.mercury_id),
                (select distinct v.venus_order_number from venus.venus v where v.venus_order_number         = cq.mercury_number)
              ) mercury_venus_order_number,
              cq.point_of_contact_id

       FROM   clean.queue cq
       JOIN   clean.mqd_queue_type_val qtv
       ON     cq.queue_type = qtv.queue_type
       WHERE  cq.message_id = in_message_id
       ;

END GET_QUEUE_MESSAGE_BY_ID;


/*-----------------------------------------------------------------------------
                        PROCEDURE GET_QUEUE_COUNT
-----------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_COUNT
(
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_CSR_ID                       IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_GROUP_ID                     IN AAS.USERS.CS_GROUP_ID%TYPE,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_EXCLUDE_PARTNERS             IN VARCHAR2,
OUT_ID_COUNT                    OUT NUMBER
) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the COUNT for a particular
            queue based on the given parameters.

    Input:
            IN_QUEUE_TYPE                      varchar2
            IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                      varchar2 - extra sort column
            IN_CSR_ID                          varchar2 - tagged by csr
            IN_GROUP_ID                        varchar2 - tagged by group
            IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
            IN_EXCLUDE_PARTNERS                varchar2 - Partners To Exclude

    Output:
            number or items in the initial result set

    -----------------------------------------------------------------------------*/

    v_Sql         VARCHAR2(4000);
    v_Sort_Option VARCHAR2(4000);

    -- cursor for the dynamic sql statement
    Sql_Cur Types.Ref_Cursor;

  BEGIN

    v_sql := GET_QUEUE_SQL
                          (IN_QUEUE_TYPE
                          ,IN_TAGGED_INDICATOR
                          ,IN_ATTACHED_EMAIL_INDICATOR
                          ,IN_SORT_ORDER
                          ,IN_CSR_ID
                          ,IN_GROUP_ID
                          ,IN_SORT_DIRECTION
                          ,IN_EXCLUDE_PARTNERS
                          ,v_sort_option);
    -- execute the sql statement
    OPEN Sql_Cur FOR v_Sql;
    FETCH Sql_Cur
      INTO OUT_ID_COUNT;
    CLOSE Sql_Cur;
  END GET_QUEUE_COUNT;



/*-----------------------------------------------------------------------------
                        PROCEDURE GET_QUEUE_COUNT_WLMT_ARIBA
-----------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_COUNT_WLMT_ARIBA
(
IN_ORIGIN_ID                  IN CLEAN.QUEUE.ORIGIN_ID%TYPE,
IN_TAGGED_INDICATOR           IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR   IN VARCHAR2,
IN_SORT_ORDER                 IN VARCHAR2,
IN_SORT_DIRECTION             IN VARCHAR2,
IN_QUEUE_INDICATOR            IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
OUT_ID_COUNT                  OUT NUMBER
) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the items in a particular
            queue based on the given parameters for Walmart or Ariba orders

    Input:
            IN_ORIGIN_ID                       varchar2
            IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                      varchar2 - extra sort column
            IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
            IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email

    Output:
            number or items in the initial result set


    -----------------------------------------------------------------------------*/

    v_Sql         VARCHAR2(4000);
    v_Sort_Option VARCHAR2(4000);

    -- cursor for the dynamic sql statement
    Sql_Cur Types.Ref_Cursor;

  BEGIN

    v_sql := GET_QUEUE_SQL_WLMT_ARIBA
                          (IN_ORIGIN_ID
                          ,IN_TAGGED_INDICATOR
                          ,IN_ATTACHED_EMAIL_INDICATOR
                          ,IN_SORT_ORDER
                          ,IN_SORT_DIRECTION
                          ,IN_QUEUE_INDICATOR
                          ,v_sort_option);


    -- execute the sql statement
    OPEN Sql_Cur FOR v_Sql;
    FETCH Sql_Cur
      INTO OUT_ID_COUNT;
    CLOSE Sql_Cur;
  END GET_QUEUE_COUNT_WLMT_ARIBA;



/*-----------------------------------------------------------------------------
                        PROCEDURE GET_FILTER_VALUES
-----------------------------------------------------------------------------*/
PROCEDURE GET_FILTER_VALUES
 (
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_CSR_ID                       IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_GROUP_ID                     IN AAS.USERS.CS_GROUP_ID%TYPE,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_EXCLUDE_PARTNERS             IN VARCHAR2,
OUT_TYPE_CUR                    OUT TYPES.REF_CURSOR,
OUT_SYS_CUR                     OUT TYPES.REF_CURSOR,
OUT_TIMEZONE_CUR                OUT TYPES.REF_CURSOR,
OUT_SDG_CUR                     OUT TYPES.REF_CURSOR,
OUT_DISPOSITION_CUR             OUT TYPES.REF_CURSOR,
OUT_PRIORITY_CUR                OUT TYPES.REF_CURSOR,
OUT_NEW_ITEM_CUR                OUT TYPES.REF_CURSOR,
OUT_OCCASION_CODE_CUR           OUT TYPES.REF_CURSOR,
OUT_LOCATION_TYPE_CUR           OUT TYPES.REF_CURSOR
) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the distinct items in a
            particular queue based on the given parameters, to be displayed in the
            filter dropdowns.

    Input:
            IN_QUEUE_TYPE                   varchar2
            IN_TAGGED_INDICATOR             varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR     varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                   varchar2 - extra sort column
            IN_CSR_ID                       varchar2 - tagged by csr
            IN_GROUP_ID                     varchar2 - tagged by group
            IN_SORT_DIRECTION               varchar2 - asc/desc for the extra sort order
            IN_EXCLUDE_PARTNERS             varchar2 - Partners To Exclude

    Output:
            OUT_TYPE_CUR                    cursor containing distinct types
            OUT_SYS_CUR                      cursor containing distinct systems
            OUT_TIMEZONE_CUR                cursor containing distinct timezones
            OUT_SDG_CUR                      cursor containing distinct sdg
            OUT_DISPOSITION_CUR              cursor containing distinct disposition
            OUT_PRIORITY_CUR                cursor containing distinct priority
            OUT_NEW_ITEM_CUR                cursor containing distinct new_item
            OUT_OCCASION_CODE_CUR            cursor containing distinct occasion code
            OUT_LOCATION_TYPE_CUR            cursor containing distinct location type

    -----------------------------------------------------------------------------*/

    v_Sql         VARCHAR2(4000);
    v_Sort_Option VARCHAR2(4000);

  BEGIN

    v_Sql := Get_Queue_Sql(In_Queue_Type
                          ,In_Tagged_Indicator
                          ,In_Attached_Email_Indicator
                          ,In_Sort_Order
                          ,In_Csr_Id
                          ,In_Group_Id
                          ,In_Sort_Direction
                          ,IN_EXCLUDE_PARTNERS
                          ,v_Sort_Option);

    v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                       ,'count(*)'
                                                                       ,'q.rowid dbindex') || '))t1 ';


    -- OUT_TYPE_CUR
    OPEN OUT_TYPE_CUR FOR ' SELECT
                                    q.message_type, count(q.message_type) message_type_count
                            FROM (' || v_Sql || ') lst
                            JOIN clean.queue q ON q.rowid = lst.dbindex
                            GROUP BY q.message_type
                          ';


    -- OUT_SYS_CUR
    OPEN OUT_SYS_CUR FOR  ' SELECT  distinct
                                    upper(
                                          decode (q.queue_type,
                                                  ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                                                  coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                                            decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                                            q.system
                                                           )
                                                 )
                                         ) system
                            FROM (' || v_Sql || ') lst
                            JOIN clean.queue q ON q.rowid = lst.dbindex
                          ';


    -- OUT_TIMEZONE_CUR
    OPEN OUT_TIMEZONE_CUR FOR ' SELECT distinct
                                       decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone
                                FROM (' || v_Sql || ') lst
                                JOIN clean.queue q ON q.rowid = lst.dbindex
                              ';


    -- OUT_SDG_CUR
    OPEN OUT_SDG_CUR FOR  ' SELECT  distinct
                                    nvl (q.same_day_gift, ''N'') same_day_gift
                            FROM (' || v_Sql || ') lst
                            JOIN clean.queue q ON q.rowid = lst.dbindex
                          ';


    -- OUT_DISPOSITION_CUR
    IF IN_TAGGED_INDICATOR = 'N'
    THEN
      OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  null from dual';
    ELSE
      OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  distinct
                                              t.tag_disposition
                                      FROM (' || v_Sql || ') lst
                                      JOIN clean.queue q ON q.rowid = lst.dbindex
                                      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                                    ';
    END IF;


    -- OUT_PRIORITY_CUR
    IF IN_TAGGED_INDICATOR = 'N'
    THEN
      OPEN OUT_PRIORITY_CUR FOR ' SELECT  null from dual';
    ELSE
      OPEN OUT_PRIORITY_CUR FOR ' SELECT  distinct
                                          t.tag_priority
                                  FROM (' || v_Sql || ') lst
                                  JOIN clean.queue q ON q.rowid = lst.dbindex
                                  LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                                ';
    END IF;


    -- OUT_NEW_ITEM_CUR
    IF IN_TAGGED_INDICATOR = 'N'
    THEN
      OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  null from dual';
    ELSE
      OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  distinct
                                          ( select count (1) from clean.queue q2
                                            where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id)
                                            and    q2.message_id <> q.message_id
                                            and    q2.order_detail_id = q.order_detail_id
                                          ) untagged_new_items
                                  FROM (' || v_Sql || ') lst
                                  JOIN clean.queue q ON q.rowid = lst.dbindex
                                  ';
    END IF;

    -- OUT_OCCASION_CODE_CUR
    OPEN OUT_OCCASION_CODE_CUR FOR  ' SELECT  distinct
                                              q.occasion occasion_id,
                                              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion
                                      FROM (' || v_Sql || ') lst
                                      JOIN clean.queue q ON q.rowid = lst.dbindex
                                    ';


    -- OUT_LOCATION_TYPE_CUR
    OPEN OUT_LOCATION_TYPE_CUR FOR  ' SELECT  distinct
                                              q.recipient_address_type delivery_location_type
                                      FROM (' || v_Sql || ') lst
                                      JOIN clean.queue q ON q.rowid = lst.dbindex
                                    ';


END GET_FILTER_VALUES;



/*-----------------------------------------------------------------------------
                        PROCEDURE GET_FILTER_VALUES_WLMT_ARIBA
-----------------------------------------------------------------------------*/
PROCEDURE GET_FILTER_VALUES_WLMT_ARIBA
(
IN_ORIGIN_ID                  IN CLEAN.QUEUE.ORIGIN_ID%TYPE,
IN_TAGGED_INDICATOR           IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR   IN VARCHAR2,
IN_SORT_ORDER                 IN VARCHAR2,
IN_CSR_ID                     IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_GROUP_ID                   IN AAS.USERS.CS_GROUP_ID%TYPE,
IN_SORT_DIRECTION             IN VARCHAR2,
IN_QUEUE_INDICATOR            IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
OUT_TYPE_CUR                  OUT TYPES.REF_CURSOR,
OUT_SYS_CUR                   OUT TYPES.REF_CURSOR,
OUT_TIMEZONE_CUR              OUT TYPES.REF_CURSOR,
OUT_SDG_CUR                   OUT TYPES.REF_CURSOR,
OUT_DISPOSITION_CUR           OUT TYPES.REF_CURSOR,
OUT_PRIORITY_CUR              OUT TYPES.REF_CURSOR,
OUT_NEW_ITEM_CUR              OUT TYPES.REF_CURSOR,
OUT_OCCASION_CODE_CUR         OUT TYPES.REF_CURSOR,
OUT_LOCATION_TYPE_CUR         OUT TYPES.REF_CURSOR
) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the distinct items in a
            particular queue based on the given parameters, to be displayed in the
            filter dropdowns.

    Input:
            IN_ORIGIN_ID                    varchar2
            IN_TAGGED_INDICATOR             varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR     varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                   varchar2 - extra sort column
            IN_CSR_ID                       varchar2 - tagged by csr
            IN_GROUP_ID                     varchar2 - tagged by group
            IN_SORT_DIRECTION               varchar2 - asc/desc for the extra sort order
            IN_QUEUE_INDICATOR              VARCHAR2 - MERCURY OR EMAIL

    Output:
            OUT_TYPE_CUR                    cursor containing distinct types
            OUT_SYS_CUR                      cursor containing distinct systems
            OUT_TIMEZONE_CUR                cursor containing distinct timezones
            OUT_SDG_CUR                      cursor containing distinct sdg
            OUT_DISPOSITION_CUR              cursor containing distinct disposition
            OUT_PRIORITY_CUR                cursor containing distinct priority
            OUT_NEW_ITEM_CUR                cursor containing distinct new_item
            OUT_OCCASION_CODE_CUR            cursor containing distinct occasion code
            OUT_LOCATION_TYPE_CUR            cursor containing distinct location type

    -----------------------------------------------------------------------------*/

    v_Sql         VARCHAR2(4000);
    v_Sort_Option VARCHAR2(4000);

  BEGIN

    v_Sql := Get_Queue_Sql_Wlmt_Ariba
                          (In_origin_id
                          ,In_Tagged_Indicator
                          ,In_Attached_Email_Indicator
                          ,In_Sort_Order
                          ,In_Sort_Direction
                          ,in_queue_indicator
                          ,v_Sort_Option);


    v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                       ,'count(*)'
                                                                       ,'q.rowid dbindex') || '))t1 ';

    -- OUT_TYPE_CUR
    OPEN OUT_TYPE_CUR FOR ' SELECT
                                    q.message_type, count(q.message_type) message_type_count
                            FROM (' || v_Sql || ') lst
                            JOIN clean.queue q ON q.rowid = lst.dbindex
                            GROUP BY q.message_type
                          ';


    -- OUT_SYS_CUR
    OPEN OUT_SYS_CUR FOR  ' SELECT  distinct
                                    upper(
                                          decode (q.queue_type,
                                                  ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                                                  coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                                            decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                                            q.system
                                                           )
                                                 )
                                         ) system
                            FROM (' || v_Sql || ') lst
                            JOIN clean.queue q ON q.rowid = lst.dbindex
                          ';


    -- OUT_TIMEZONE_CUR
    OPEN OUT_TIMEZONE_CUR FOR ' SELECT distinct
                                       decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone
                                FROM (' || v_Sql || ') lst
                                JOIN clean.queue q ON q.rowid = lst.dbindex
                              ';



    -- OUT_SDG_CUR
    OPEN OUT_SDG_CUR FOR  ' SELECT  distinct
                                    nvl (q.same_day_gift, ''N'') same_day_gift
                            FROM (' || v_Sql || ') lst
                            JOIN clean.queue q ON q.rowid = lst.dbindex
                          ';


    -- OUT_DISPOSITION_CUR
    IF IN_TAGGED_INDICATOR = 'N'
    THEN
      OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  null from dual';
    ELSE
      OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  distinct
                                              t.tag_disposition
                                      FROM (' || v_Sql || ') lst
                                      JOIN clean.queue q ON q.rowid = lst.dbindex
                                      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                                    ';
    END IF;


    -- OUT_PRIORITY_CUR
    IF IN_TAGGED_INDICATOR = 'N'
    THEN
      OPEN OUT_PRIORITY_CUR FOR ' SELECT  null from dual';
    ELSE
      OPEN OUT_PRIORITY_CUR FOR ' SELECT  distinct
                                          t.tag_priority
                                  FROM (' || v_Sql || ') lst
                                  JOIN clean.queue q ON q.rowid = lst.dbindex
                                  LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                                ';
    END IF;


    -- OUT_NEW_ITEM_CUR
    IF IN_TAGGED_INDICATOR = 'N'
    THEN
      OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  null from dual';
    ELSE
      OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  distinct
                                          ( select count (1) from clean.queue q2
                                            where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id)
                                            and    q2.message_id <> q.message_id
                                            and    q2.order_detail_id = q.order_detail_id
                                          ) untagged_new_items
                                  FROM (' || v_Sql || ') lst
                                  JOIN clean.queue q ON q.rowid = lst.dbindex
                                  ';
    END IF;

    -- OUT_OCCASION_CODE_CUR
    OPEN OUT_OCCASION_CODE_CUR FOR  ' SELECT  distinct
                                              q.occasion occasion_id,
                                              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion
                                      FROM (' || v_Sql || ') lst
                                      JOIN clean.queue q ON q.rowid = lst.dbindex
                                    ';


    -- OUT_LOCATION_TYPE_CUR
    OPEN OUT_LOCATION_TYPE_CUR FOR  ' SELECT  distinct
                                              q.recipient_address_type delivery_location_type
                                      FROM (' || v_Sql || ') lst
                                      JOIN clean.queue q ON q.rowid = lst.dbindex
                                    ';

END GET_FILTER_VALUES_WLMT_ARIBA;



/*-----------------------------------------------------------------------------
                        FUNCTION GET_FILTERED_Q_SQL
-----------------------------------------------------------------------------*/
FUNCTION GET_FILTERED_Q_SQL
(
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_CSR_ID                       IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_GROUP_ID                     IN AAS.USERS.CS_GROUP_ID%TYPE,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_EXCLUDE_PARTNERS             IN VARCHAR2,
v_Filter_Where                  IN VARCHAR2,
OUT_SORT_ORDER                  OUT VARCHAR2
) RETURN VARCHAR2 AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the sql statement to query the
            queue based on the given parameters.

    Input:
            IN_QUEUE_TYPE                      varchar2
            IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                      varchar2 - extra sort column
            IN_CSR_ID                          varchar2 - tagged by csr
            IN_GROUP_ID                        varchar2 - tagged by group
            IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
            IN_EXCLUDE_PARTNERS                varchar2 - Partners To Exclude
            v_Filter_Where                     varchar2 - filter criteria
    Output:
            the statement

    -----------------------------------------------------------------------------*/

    -- join clause variables
    v_Join VARCHAR2(4000);

    -- where clause variables
    v_Where VARCHAR2(4000);

    Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';
  BEGIN

    --if In_Csr_Id is NOT null, search was invoked for "Items Tagged to CSR_ID".  In this case, retrieve ALL data, including Walmart, Ariba, and Premier Collection
    --Else, exclude walmart and ariba orders.
    IF In_Csr_Id IS NULL then
      v_Where := Nvl(v_Where,'WHERE ') || ' nvl(q.origin_id,''TEST'') NOT in (''WLMTI'', ''ARI'', ''CAT'') AND ';
    END IF;

    -- build the join clause if attached email indicator is specified
    IF IN_ATTACHED_EMAIL_INDICATOR IS NOT NULL
    THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN IN_ATTACHED_EMAIL_INDICATOR = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';
    END IF;

    -- build the join clause if tagged items is specified
    IF IN_TAGGED_INDICATOR = 'Y'
    THEN
      v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';

      IF IN_CSR_ID IS NOT NULL
      THEN
        v_Join := v_Join || 'AND t.csr_id=''' || IN_CSR_ID || ''' ';
        --        v_Where_Tag_Csr_Id := 'AND t.csr_id=''' || IN_CSR_ID || ''' ';
      END IF;

      IF IN_GROUP_ID IS NOT NULL
      THEN
        v_Join := v_Join || 'JOIN aas.identity i ON t.csr_id=i.identity_id JOIN aas.users u ON i.user_id=u.user_id ' || 'AND u.cs_group_id=''' || IN_GROUP_ID ||
                  ''' ';
      END IF;
    ELSE
      -- build the where clause if untagged items is specified
      v_Where := Nvl(v_Where
                    ,'WHERE ') || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
    END IF;

    --Append the Queue Filter
    IF v_Filter_Where IS NOT NULL THEN
      v_where := Nvl(v_Where ,'WHERE ') || v_Filter_Where;
    END IF;

    -- build the where clause if the queue type is specified
    IF IN_QUEUE_TYPE IS NOT NULL
    THEN
      v_Where := Nvl(v_Where
                    ,'WHERE ') || 'q.queue_type=''' || IN_QUEUE_TYPE || ''' AND ';
      IF IN_QUEUE_TYPE = 'GEN'
      THEN
        OUT_SORT_ORDER := 'q.message_timestamp;';
      END IF;
    ELSE
      OUT_SORT_ORDER := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
    END IF;

    OUT_SORT_ORDER := OUT_SORT_ORDER || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

    -- build the order by clause given the page column headings, the computed fields in the order by should
    -- correspond with computed fields in the end result set(the very last select statement)
    CASE Nvl(IN_SORT_ORDER
        ,'NULL')
      WHEN 'NULL' THEN
        NULL;
      WHEN 'system' THEN
        IF IN_QUEUE_TYPE = 'LP'
        THEN
          OUT_SORT_ORDER := 'COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system) ' ||
                            IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;
        ELSIF IN_QUEUE_TYPE IS NOT NULL
        THEN
          OUT_SORT_ORDER := 'COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                            'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id),''Held'',''HOLD'', NULL), q.system) ' ||
                            IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;
        ELSE
          OUT_SORT_ORDER := 'DECODE(q.queue_type, ''LP''' ||
                            ',COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' ||
                            ',COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                            'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' ||
                            IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;
        END IF;

      WHEN 'delivery_date' THEN
        OUT_SORT_ORDER := 'q.delivery_date ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER
                                                                                     ,'q.delivery_date,');

      WHEN 'timezone' THEN
        OUT_SORT_ORDER := 'key_3 ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER
                                                                          ,'key_3,');

      WHEN 'same_day_gift' THEN
        OUT_SORT_ORDER := 'NVL(q.same_day_gift, ''N'') ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'product_id' THEN
        OUT_SORT_ORDER := 'q.product_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'untagged_new_items' THEN
        OUT_SORT_ORDER := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' ||
                          ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

      WHEN 'tagged_by' THEN
        IF IN_TAGGED_INDICATOR <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        OUT_SORT_ORDER := 't.csr_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'tagged_on' THEN
        IF IN_TAGGED_INDICATOR <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        OUT_SORT_ORDER := 't.tagged_on ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'last_touched' THEN
        OUT_SORT_ORDER := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'order_detail_id' THEN
        OUT_SORT_ORDER := 'q.order_detail_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'external_order_number' THEN
        OUT_SORT_ORDER := 'q.external_order_number ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'member_number' THEN
        OUT_SORT_ORDER := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' ||
                          '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) '
                          || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'delivery_location_type' THEN
        OUT_SORT_ORDER := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'zip_code' THEN
        OUT_SORT_ORDER := ' q.recipient_zip_code ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'occasion' THEN
          OUT_SORT_ORDER := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'tag_priority' THEN
        IF IN_TAGGED_INDICATOR <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        OUT_SORT_ORDER := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || IN_SORT_DIRECTION || ', ' ||
                          OUT_SORT_ORDER;

      ELSE
        OUT_SORT_ORDER := 'q.' || IN_SORT_ORDER || ' ' || IN_SORT_DIRECTION || ', ' ||
                          REPLACE(REPLACE(OUT_SORT_ORDER
                                         ,';q.' || Lower(IN_SORT_ORDER))
                                 ,'q.' || Lower(IN_SORT_ORDER) || ';');
    END CASE;
    OUT_SORT_ORDER := REPLACE(REPLACE(OUT_SORT_ORDER
                                     ,'key_3'
                                     ,Key_3)
                             ,';'
                             ,',');

    --if In_Csr_Id is NOT null, search was invoked for "Items Tagged to CSR_ID".  In this case, retrieve ALL data, including Walmart, Ariba, and Premier Collection
    --Any Premier Collection order in the (LP, CREDIT) queues will be displayed in the (LP, CREDIT) queues; these orders will NOT be displayed under "PREMIER_COLLECTION" queue
    IF ( In_Csr_Id IS NULL AND In_Queue_Type not in ('LP', 'CREDIT') ) THEN
      v_where := nvl(v_where,'Where') || ' q.message_id not in (select message_id from clean.queue q1 where q1.PREMIER_COLLECTION_FLAG = ''Y'' and q1.QUEUE_TYPE not in (''LP'', ''CREDIT'')) AND ';
    END IF;

    --IN_EXCLUDE_PARTNERS will not call this stored proc because they have their own virtual queue.  This stored proc will return these IN_EXCLUDE_PARTNERS info ONLY if
    --the search was invoked for a Group or a CSR.  Thus, prevent IN_EXCLUDE_PARTNERS data retrieval for all queues but the group
    IF ( In_Group_Id IS NULL and In_Csr_Id IS NULL) THEN
      v_where := nvl(v_where,'Where') || ' (q.partner_name is null or q.partner_name not in (' || IN_EXCLUDE_PARTNERS || ')) AND ';
    END IF;

    -- build the entire sql statement
    RETURN 'SELECT count(*) FROM clean.queue q '  || v_Join || Substr(v_Where, 1, Length(v_Where) - 4);
  END GET_FILTERED_Q_SQL;


/*-----------------------------------------------------------------------------
                        PROCEDURE GET_FILTERED_Q
-----------------------------------------------------------------------------*/
PROCEDURE GET_FILTERED_Q
(
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_CSR_ID                       IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_GROUP_ID                     IN AAS.USERS.CS_GROUP_ID%TYPE,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_MESSAGE_TIMESTAMP            IN CLEAN.QUEUE.MESSAGE_TIMESTAMP%TYPE,
IN_DELIVERY_DATE                IN CLEAN.QUEUE.DELIVERY_DATE%TYPE,
IN_TAGGED_ON                    IN CLEAN.QUEUE_TAG.TAGGED_ON%TYPE,
IN_LAST_TOUCHED                 IN CLEAN.CSR_VIEWED_ENTITIES.UPDATED_ON%TYPE,
IN_MESSAGE_TYPE                 IN CLEAN.QUEUE.MESSAGE_TYPE%TYPE,
IN_SYSTEM                       IN CLEAN.QUEUE.SYSTEM%TYPE,
IN_TIMEZONE                     IN VARCHAR2,
IN_SAME_DAY_GIFT                IN CLEAN.QUEUE.SAME_DAY_GIFT%TYPE,
IN_TAG_DISPOSITION              IN CLEAN.QUEUE_TAG.TAG_DISPOSITION%TYPE,
IN_TAG_PRIORITY                 IN CLEAN.QUEUE_TAG.TAG_PRIORITY%TYPE,
IN_UNTAGGED_NEW_ITEMS           IN VARCHAR2,
IN_OCCASION_ID                  IN CLEAN.QUEUE.OCCASION%TYPE,
IN_DELIVERY_LOCATION_TYPE       IN CLEAN.QUEUE.RECIPIENT_ADDRESS_TYPE%TYPE,
IN_MEMBER_NUMBER                IN VENUS.VENUS.SENDING_VENDOR%TYPE,
IN_ZIP_CODE                     IN CLEAN.QUEUE.RECIPIENT_ZIP_CODE%TYPE,
IN_TAGGED_BY                    IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_EXCLUDE_PARTNERS             IN VARCHAR2,
IN_CONTEXT                      IN VARCHAR2,
IN_SESSION_ID                   IN VARCHAR2,
IN_PRODUCT_ID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER,
OUT_QUEUE_DESC                  OUT VARCHAR2
) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the items in a particular
            queue based on the given parameters.

    Input:
            IN_QUEUE_TYPE                      varchar2
            IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                      varchar2 - extra sort column
            IN_START_POSITION                  number   - index of the start item in the id array
            IN_MAX_NUMBER_RETURNED             number   - number of items in the result set
            IN_CSR_ID                          varchar2 - tagged by csr
            IN_GROUP_ID                        varchar2 - tagged by group
            IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
            IN_MESSAGE_TIMESTAMP               date      - message timestamp
            IN_DELIVERY_DATE                   date     - delivery date
            IN_TAGGED_ON                       date      - date that the csr tagged the order
            IN_LAST_TOUCHED                    date     - date when the record was last updated
            IN_MESSAGE_TYPE                    varchar2 - message type
            IN_SYSTEM                          varchar2 - system type - mercury/venus/argos etc
            IN_TIMEZONE                        varchar2 - timezone - CST/EST/MST/PST/WST
            IN_SAME_DAY_GIFT                   varchar2 - same day gift indicator
            IN_TAG_DISPOSITION                 varchar2 - tag disposition
            IN_TAG_PRIORITY                    varchar2 - tag priority
            IN_UNTAGGED_NEW_ITEMS              varchar2 - indicator - if a given order detail has at
                                                          least one other item that is neither currently
                                                          nor ever been tagged, then 'Y'
            IN_OCCASION_ID                     varchar2 - occasion id
            IN_DELIVERY_LOCATION_TYPE          varchar2 - delivery location type - HOME/BUSINESS etc
            IN_MEMBER_NUMBER                   varchar2 - if drop ship order = Venus.Sending_Vendor
                                                          if floral order = Mercury.Sending_Florist
            IN_ZIP_CODE                        varchar2 - zip code / postal code
            IN_TAGGED_BY                       varchar2 - csr id who tagged the order
            IN_EXCLUDE_PARTNERS                varchar2 - Partners To Exclude

    Output:
            OUT_CUR                            cursor   - cursor containing all colums from queue
            OUT_ID_COUNT                       cursor   - number or items in the initial result set
            OUT_QUEUE_DESC                     cursor   - queue description

    -----------------------------------------------------------------------------*/

    v_Sql           VARCHAR2(9999);
    v_Sort_Option   VARCHAR2(4000);
    v_Sql_Final     VARCHAR2(9999);
    v_Filter_Where  VARCHAR2(9999);

    -- cursor for the dynamic sql statement
    Sql_Cur Types.Ref_Cursor;

    -- cursor to get the queue description
    CURSOR Desc_Cur IS
      SELECT Description
        FROM /*clean.*/ Queue_Type_Val
       WHERE Queue_Type = IN_QUEUE_TYPE;

    -- cursor to get the group description
    CURSOR Group_Cur IS
      SELECT Description
        FROM Ftd_Apps.Cs_Groups
       WHERE Cs_Group_Id = IN_GROUP_ID;

  BEGIN

    IF IN_GROUP_ID IS NOT NULL THEN
      OPEN Group_Cur;
      FETCH Group_Cur
        INTO OUT_QUEUE_DESC;
      CLOSE Group_Cur;
    ELSIF IN_CSR_ID IS NOT NULL THEN
      OUT_QUEUE_DESC := IN_CSR_ID;
    ELSE
      OPEN Desc_Cur;
      FETCH Desc_Cur
        INTO OUT_QUEUE_DESC;
      CLOSE Desc_Cur;
    END IF;


--*******************************************************************************************
-- create a filter - start
--*******************************************************************************************
    --*******************************************************************************************
    --                                ****QUEUE****
    --*******************************************************************************************

    --**********Check IN_MESSAGE_TYPE**********
    IF IN_MESSAGE_TYPE IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'q.message_type=''' || IN_MESSAGE_TYPE || ''' AND ';
    END IF;


    --**********Check IN_MESSAGE_TIMESTAMP**********
    IF IN_MESSAGE_TIMESTAMP IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc(q.message_timestamp)=''' || IN_MESSAGE_TIMESTAMP || ''' AND ';
    END IF;


      --**********Check IN_MEMBER_NUMBER**********
    IF IN_MEMBER_NUMBER IS NOT NULL
    THEN
      IF IN_SYSTEM IS NULL
      THEN
        RAISE_APPLICATION_ERROR(-20001, 'System id must be provided when searching for a memeber id');
      ELSE
        IF (IN_SYSTEM <> 'ARGO'          AND
            IN_SYSTEM <> 'VENUS'         AND
            IN_SYSTEM <> 'MERC'          AND
            IN_SYSTEM <> 'MERCURY'
           )
        THEN
          RAISE_APPLICATION_ERROR(-20002, 'Please provide a valid System id');
        ELSE
          IF (IN_SYSTEM = 'ARGO' OR IN_SYSTEM = 'VENUS')
          THEN
            v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT v.venus_id FROM venus.venus v WHERE SENDING_VENDOR=''' || IN_MEMBER_NUMBER || ''') AND ';
          ELSE
            v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT m.mercury_id FROM mercury.mercury m WHERE decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST)=''' || IN_MEMBER_NUMBER || ''') AND ';
          END IF;
        END IF;
      END IF;
    END IF;


    --**********Check IN_SYSTEM**********
    IF (IN_SYSTEM IS NOT NULL AND IN_MEMBER_NUMBER IS NULL) THEN
      IF IN_SYSTEM = ' ' THEN
        v_Filter_Where := v_Filter_Where || 'q.system is null AND ';
      ELSE
        IF IN_QUEUE_TYPE = 'LP'
        THEN
          v_Filter_Where := v_Filter_Where || 'upper(coalesce         (' ||
                                                  '(select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id), ' ||
                                                  'q.system   )' ||
                                            ')=''' || IN_SYSTEM || ''' AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 'upper(coalesce         (' ||
                                                  'clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                                                  'decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null), ' ||
                                                  'q.system   )' ||
                                           ')=''' || IN_SYSTEM || ''' AND ';
        END IF;
      END IF;
    END IF;


    --**********Check IN_DELIVERY_DATE**********
    IF IN_DELIVERY_DATE IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc(q.delivery_date)=''' || IN_DELIVERY_DATE || ''' AND ';
    END IF;


    --**********Check IN_OCCASION_ID**********
    IF IN_OCCASION_ID IS NOT NULL
    THEN
      IF IN_OCCASION_ID = ' '
      THEN
        v_Filter_Where := v_Filter_Where || 'q.occasion IS NULL AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 'q.occasion=''' || IN_OCCASION_ID || ''' AND ';
      END IF;
    END IF;


    --**********Check IN_SAME_DAY_GIFT**********
    IF IN_SAME_DAY_GIFT IS NOT NULL THEN
      IF IN_SAME_DAY_GIFT = 'Y' THEN
        v_Filter_Where := v_Filter_Where || 'q.same_day_gift=''' || IN_SAME_DAY_GIFT || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || '(q.same_day_gift is null or q.same_day_gift = ''' || IN_SAME_DAY_GIFT || ''') AND ';
      END IF;
    END IF;


    --**********Check IN_DELIVERY_LOCATION_TYPE AND IN_ZIP_CODE**********
    IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_ZIP_CODE IS NOT NULL)
    THEN
      v_Filter_Where := v_Filter_Where || 'q.recipient_address_type = ''' || IN_DELIVERY_LOCATION_TYPE || ''' AND q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
    ELSE
      IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_DELIVERY_LOCATION_TYPE = ' ')
      THEN
        v_Filter_Where := v_Filter_Where || '(q.order_detail_id IS NULL or q.recipient_address_type IS NULL) AND ';
      ELSE
        IF IN_DELIVERY_LOCATION_TYPE IS NOT NULL
        THEN
          v_Filter_Where := v_Filter_Where || 'q.recipient_address_type =''' || IN_DELIVERY_LOCATION_TYPE || ''' AND ';
        ELSE
          IF IN_ZIP_CODE IS NOT NULL
          THEN
             v_Filter_Where := v_Filter_Where || 'q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
          END IF;
        END IF;
      END IF;
    END IF;


    --**********Check IN_TIMEZONE**********
    IF IN_TIMEZONE IS NOT NULL THEN
      IF IN_TIMEZONE = ' ' THEN
        v_Filter_Where := v_Filter_Where || 'q.timezone is null AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 'q.timezone = ' ||
                               'decode (''' || IN_TIMEZONE || ''', ' ||
                                        '''EST''' || ', 1, ' ||
                                        '''CST''' || ', 2, ' ||
                                        '''MST''' || ', 3, ' ||
                                        '''PST''' || ', 4, ' ||
                                        '''WST''' || ', 5, ' ||
                                        '1 ' ||
                                      ')' ||
                      ' AND ';
      END IF;
    END IF;

    --**********Check IN_LAST_TOUCHED**********
    IF IN_LAST_TOUCHED IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id))=''' || IN_LAST_TOUCHED || '''  AND ';
    END IF;


    --*******************************************************************************************
    --                               ****QUEUE_TAG****
    --*******************************************************************************************

    --**********Check IN_TAGGED_INDICATOR... if taged**********
    IF IN_TAGGED_INDICATOR = 'Y'
    THEN


      --**********Check IN_TAGGED_ON**********
      IF IN_TAGGED_ON IS NOT NULL
      THEN
        v_Filter_Where := v_Filter_Where || 'trunc(t.tagged_on)=''' || IN_TAGGED_ON || ''' AND ';
      END IF;


      --**********Check IN_TAG_DISPOSITION**********
      IF (IN_TAG_DISPOSITION IS NOT NULL)
      THEN
        IF (IN_TAG_DISPOSITION <> ' ')
        THEN
          v_Filter_Where := v_Filter_Where || 't.tag_disposition=''' || IN_TAG_DISPOSITION || ''' AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 't.tag_disposition IS NULL AND ';
        END IF;
      END IF;


      --**********Check IN_TAG_PRIORITY**********
      IF IN_TAG_PRIORITY IS NOT NULL
      THEN
        IF (IN_TAG_PRIORITY <> ' ')
        THEN
          v_Filter_Where := v_Filter_Where || 't.tag_priority=''' || IN_TAG_PRIORITY || ''' AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 't.tag_priority IS NULL AND ';
        END IF;
      END IF;


      --**********Check IN_TAGGED_BY**********
      IF IN_TAGGED_BY IS NOT NULL
      THEN
        v_Filter_Where := v_Filter_Where || 't.csr_id=''' || IN_TAGGED_BY || ''' AND ';
      END IF;

    END IF;


    --*******************************************************************************************
    --                               ****Mix****
    --*******************************************************************************************

    --**********Check IN_UNTAGGED_NEW_ITEMS**********
    IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND IN_UNTAGGED_NEW_ITEMS = 'Y')
    THEN
      v_Filter_Where := v_Filter_Where || 'q.message_id IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
    ELSE
      IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND (IN_UNTAGGED_NEW_ITEMS = 'N' OR IN_UNTAGGED_NEW_ITEMS = ' '))
      THEN
        v_Filter_Where := v_Filter_Where || 'q.message_id NOT IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
      END IF;
    END IF;

    --**********Check IN_PRODUCT_ID**********
    IF IN_PRODUCT_ID IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'q.product_id=''' || IN_PRODUCT_ID || ''' AND ';
    END IF;



--*******************************************************************************************
-- create a filter - end
--*******************************************************************************************

    v_Sql := GET_FILTERED_Q_SQL
                          (IN_QUEUE_TYPE
                          ,IN_TAGGED_INDICATOR
                          ,IN_ATTACHED_EMAIL_INDICATOR
                          ,IN_SORT_ORDER
                          ,IN_CSR_ID
                          ,IN_GROUP_ID
                          ,IN_SORT_DIRECTION
                          ,IN_EXCLUDE_PARTNERS
                          ,v_Filter_Where
                          ,v_Sort_Option);

    IF v_Filter_Where IS NOT NULL
    THEN
      v_Filter_Where := ' WHERE ' || v_Filter_Where;
      v_Filter_Where := Substr(v_Filter_Where,1,Length(v_Filter_Where) - 4);
    END IF;

    -- execute the sql statement
    OPEN Sql_Cur FOR v_Sql;
    FETCH Sql_Cur
      INTO OUT_ID_COUNT;
    CLOSE Sql_Cur;
    v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                       ,'count(*)'
                                                                       ,'q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' ||
                                                                       Nvl(IN_START_POSITION,1) || ' AND ' || (Nvl(IN_START_POSITION,1) + Nvl(IN_MAX_NUMBER_RETURNED,OUT_ID_COUNT) - 1);


    v_Sql_Final :=
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode  ( q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                      ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                         (SELECT v.FILLING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion,
              clean.queue_pkg.GET_CUST_HOLD_REASON_CODE (''CUSTOMER_ID'', q.customer_id, ''Prefer'')   customer_hold_customer_id,
              q.partner_name,
              q.premier_collection_flag,
              pm.preferred_processing_resource,
              decode(pm.preferred_processing_resource, null, ''N'',
                  aas.get_permission(''' || in_context || ''', ''' || in_session_id || ''', pm.preferred_processing_resource, ''View'')
              ) preferred_partner_authorized,
              pm.display_name partner_display_name
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
      LEFT JOIN ftd_apps.partner_master pm on pm.partner_name = q.partner_name
                ' || v_Filter_Where ||' ORDER BY lst.ord';

  OPEN OUT_CUR FOR v_Sql_Final;

END GET_FILTERED_Q;


/*-----------------------------------------------------------------------------
                        FUNCTION GET_FILTERED_Q_SQL_WLMT_ARIBA
-----------------------------------------------------------------------------*/
FUNCTION GET_FILTERED_Q_SQL_WLMT_ARIBA
(
IN_ORIGIN_ID                    IN CLEAN.QUEUE.ORIGIN_ID%TYPE,
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_QUEUE_INDICATOR              IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
v_Filter_Where                  IN VARCHAR2,
OUT_SORT_ORDER                  OUT VARCHAR2

) RETURN VARCHAR2 AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the sql statement to query the
            queue based on the given parameters.

    Input:
            IN_ORIGIN_ID                       varchar2
            IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                      varchar2 - extra sort column
            IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
            IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email
            v_Filter_Where                     varchar2 - filter criteria
    Output:
            the statement

    -----------------------------------------------------------------------------*/

    -- join clause variables
    v_Join VARCHAR2(4000);

    -- where clause variables
    v_Where VARCHAR2(4000) := 'WHERE ';

    Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';
  BEGIN
    -- build the join clause if attached email indicator is specified
    IF IN_ATTACHED_EMAIL_INDICATOR IS NOT NULL
    THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN IN_ATTACHED_EMAIL_INDICATOR = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';

    ELSE
    -- otherwise, limit the results to the specified queue indicator
    if  in_queue_indicator = 'Mercury' then
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator in (''Mercury'', ''Order'') ';
    else
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' ';
    END IF;
    END IF;

    -- build the join clause if tagged items is specified
    IF IN_TAGGED_INDICATOR = 'Y'
    THEN
      v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';

    ELSE
      -- build the where clause if untagged items is specified
      v_Where := v_Where || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
    END IF;

    --Append the Queue Filter
    IF v_Filter_Where IS NOT NULL THEN
      v_where := Nvl(v_Where ,'WHERE ') || v_Filter_Where;
    END IF;

    -- build the sort order
    OUT_SORT_ORDER := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
    OUT_SORT_ORDER := OUT_SORT_ORDER || 'q.delivery_date;key_3; DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

    -- build the order by clause given the page column headings, the computed fields in the order by should
    -- correspond with computed fields in the end result set(the very last select statement)
    CASE Nvl(IN_SORT_ORDER
        ,'NULL')
      WHEN 'NULL' THEN
        NULL;
      WHEN 'system' THEN
          OUT_SORT_ORDER := 'DECODE(q.queue_type, ''LP''' ||
                            ',COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' ||
                            ',COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                            'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' ||
                            IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

      WHEN 'delivery_date' THEN
        OUT_SORT_ORDER := 'q.delivery_date ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER
                                                                                     ,'q.delivery_date,');

      WHEN 'timezone' THEN
        OUT_SORT_ORDER := 'key_3 ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER
                                                                          ,'key_3,');

      WHEN 'same_day_gift' THEN
        OUT_SORT_ORDER := 'NVL(q.same_day_gift, ''N'') ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'product_id' THEN
        OUT_SORT_ORDER := 'q.product_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'untagged_new_items' THEN
        OUT_SORT_ORDER := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' ||
                          ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

      WHEN 'tagged_by' THEN
        IF IN_TAGGED_INDICATOR <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        OUT_SORT_ORDER := 't.csr_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'tagged_on' THEN
        IF IN_TAGGED_INDICATOR <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        OUT_SORT_ORDER := 't.tagged_on ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'last_touched' THEN
        OUT_SORT_ORDER := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'order_detail_id' THEN
        OUT_SORT_ORDER := 'q.order_detail_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'external_order_number' THEN
        OUT_SORT_ORDER := 'q.external_order_number ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'member_number' THEN
        OUT_SORT_ORDER := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' ||
                          '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) '
                          || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'delivery_location_type' THEN
        OUT_SORT_ORDER := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'zip_code' THEN
        OUT_SORT_ORDER := ' q.recipient_zip_code ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

      WHEN 'occasion' THEN
          OUT_SORT_ORDER := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;


      WHEN 'tag_priority' THEN
        IF IN_TAGGED_INDICATOR <> 'Y'
        THEN
          v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
        END IF;
        OUT_SORT_ORDER := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || IN_SORT_DIRECTION || ', ' ||
                          OUT_SORT_ORDER;

      ELSE
        OUT_SORT_ORDER := 'q.' || IN_SORT_ORDER || ' ' || IN_SORT_DIRECTION || ', ' ||
                          REPLACE(REPLACE(OUT_SORT_ORDER
                                         ,';q.' || Lower(IN_SORT_ORDER))
                                 ,'q.' || Lower(IN_SORT_ORDER) || ';');
    END CASE;
    OUT_SORT_ORDER := REPLACE(REPLACE(OUT_SORT_ORDER
                                     ,'key_3'
                                     ,Key_3)
                             ,';'
                             ,',');

    --depending on the origin type passed in, select WLMTI or (ARI, CAT) origin orders
    IF in_origin_id = 'WALMART' THEN
        -- get walmart queued orders
        v_where := v_where || ' q.origin_id in ( ''WLMTI'' ) AND ';
    ELSE
        -- get ariba queued orders
        v_where := v_where || ' q.origin_id in ( ''ARI'', ''CAT'') AND ';
    END IF;

    -- build the entire sql statement
    RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where, 1, Length(v_Where) - 4);

  END GET_FILTERED_Q_SQL_WLMT_ARIBA;


/*-----------------------------------------------------------------------------
                        PROCEDURE GET_FILTERED_Q_WLMT_ARIBA
-----------------------------------------------------------------------------*/
PROCEDURE GET_FILTERED_Q_WLMT_ARIBA
(
IN_ORIGIN_ID                    IN CLEAN.QUEUE.ORIGIN_ID%TYPE,
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_QUEUE_INDICATOR              IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_MESSAGE_TIMESTAMP            IN CLEAN.QUEUE.MESSAGE_TIMESTAMP%TYPE,
IN_DELIVERY_DATE                IN CLEAN.QUEUE.DELIVERY_DATE%TYPE,
IN_TAGGED_ON                    IN CLEAN.QUEUE_TAG.TAGGED_ON%TYPE,
IN_LAST_TOUCHED                 IN CLEAN.CSR_VIEWED_ENTITIES.UPDATED_ON%TYPE,
IN_MESSAGE_TYPE                 IN CLEAN.QUEUE.MESSAGE_TYPE%TYPE,
IN_SYSTEM                       IN CLEAN.QUEUE.SYSTEM%TYPE,
IN_TIMEZONE                     IN VARCHAR2,
IN_SAME_DAY_GIFT                IN CLEAN.QUEUE.SAME_DAY_GIFT%TYPE,
IN_TAG_DISPOSITION              IN CLEAN.QUEUE_TAG.TAG_DISPOSITION%TYPE,
IN_TAG_PRIORITY                 IN CLEAN.QUEUE_TAG.TAG_PRIORITY%TYPE,
IN_UNTAGGED_NEW_ITEMS           IN VARCHAR2,
IN_OCCASION_ID                  IN CLEAN.QUEUE.OCCASION%TYPE,
IN_DELIVERY_LOCATION_TYPE       IN CLEAN.QUEUE.RECIPIENT_ADDRESS_TYPE%TYPE,
IN_MEMBER_NUMBER                IN VENUS.VENUS.SENDING_VENDOR%TYPE,
IN_ZIP_CODE                     IN CLEAN.QUEUE.RECIPIENT_ZIP_CODE%TYPE,
IN_TAGGED_BY                    IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_PRODUCT_ID                   IN VARCHAR2,
OUT_CUR                         OUT Types.Ref_Cursor,
OUT_ID_COUNT                    OUT NUMBER,
OUT_QUEUE_DESC                  OUT VARCHAR2
) AS
    /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for returning the items in a particular
            queue based on the given parameters for Walmart or Ariba orders

    Input:
            IN_ORIGIN_ID                       varchar2
            IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
            IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
            IN_SORT_ORDER                      varchar2 - extra sort column
            IN_START_POSITION                  number - index of the start item in the id array
            IN_MAX_NUMBER_RETURNED             number - number of items in the result set
            IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
            IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email
            IN_MESSAGE_TIMESTAMP               date      - message timestamp
            IN_DELIVERY_DATE                   date     - delivery date
            IN_TAGGED_ON                       date      - date that the csr tagged the order
            IN_LAST_TOUCHED                    date     - date when the record was last updated
            IN_MESSAGE_TYPE                    varchar2 - message type
            IN_SYSTEM                          varchar2 - system type - mercury/venus/argos etc
            IN_TIMEZONE                        varchar2 - timezone - CST/EST/MST/PST/WST
            IN_SAME_DAY_GIFT                   varchar2 - same day gift indicator
            IN_TAG_DISPOSITION                 varchar2 - tag disposition
            IN_TAG_PRIORITY                    varchar2 - tag priority
            IN_UNTAGGED_NEW_ITEMS              varchar2 - indicator - if a given order detail has at
                                                          least one other item that is neither currently
                                                          nor ever been tagged, then 'Y'
            IN_OCCASION_ID                     varchar2 - occasion id
            IN_DELIVERY_LOCATION_TYPE          varchar2 - delivery location type - HOME/BUSINESS etc
            IN_MEMBER_NUMBER                   varchar2 - if drop ship order = Venus.Sending_Vendor
                                                          if floral order = Mercury.Sending_Florist
            IN_ZIP_CODE                        varchar2 - zip code / postal code
            IN_TAGGED_BY                       varchar2 - csr id who tagged the order

    Output:
            cursor containing all colums from queue
            number or items in the initial result set
            queue description

    -----------------------------------------------------------------------------*/

    v_Sql           VARCHAR2(9999);
    v_Sort_Option   VARCHAR2(4000);
    v_Sql_Final     VARCHAR2(9999);
    v_Filter_Where  VARCHAR2(9999);


    -- cursor for the dynamic sql statement
    Sql_Cur Types.Ref_Cursor;

    -- cursor to get the queue description
    CURSOR Desc_Cur IS
      SELECT Description
        FROM ftd_apps.origins
       WHERE origin_id = decode (In_origin_id, 'WALMART', 'WLMTI', 'ARIBA', 'ARI', in_origin_id);

  BEGIN

      OPEN Desc_Cur;
      FETCH Desc_Cur
        INTO OUT_QUEUE_DESC;
      CLOSE Desc_Cur;

--*******************************************************************************************
-- create a filter - start
--*******************************************************************************************

    --*******************************************************************************************
    --                                ****QUEUE****
    --*******************************************************************************************

    --**********Check IN_MESSAGE_TYPE**********
    IF IN_MESSAGE_TYPE IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'q.message_type=''' || IN_MESSAGE_TYPE || ''' AND ';
    END IF;


    --**********Check IN_MESSAGE_TIMESTAMP**********
    IF IN_MESSAGE_TIMESTAMP IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc(q.message_timestamp)=''' || IN_MESSAGE_TIMESTAMP || ''' AND ';
    END IF;


      --**********Check IN_MEMBER_NUMBER**********
    IF IN_MEMBER_NUMBER IS NOT NULL
    THEN
      IF IN_SYSTEM IS NULL
      THEN
        RAISE_APPLICATION_ERROR(-20001, 'System id must be provided when searching for a memeber id');
      ELSE
        IF (IN_SYSTEM <> 'ARGO'          AND
            IN_SYSTEM <> 'VENUS'         AND
            IN_SYSTEM <> 'MERC'          AND
            IN_SYSTEM <> 'MERCURY'
           )
        THEN
          RAISE_APPLICATION_ERROR(-20002, 'Please provide a valid System id');
        ELSE
          IF (IN_SYSTEM = 'ARGO' OR IN_SYSTEM = 'VENUS')
          THEN
            v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT v.venus_id FROM venus.venus v WHERE SENDING_VENDOR=''' || IN_MEMBER_NUMBER || ''') AND ';
          ELSE
            v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT m.mercury_id FROM mercury.mercury m WHERE decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST)=''' || IN_MEMBER_NUMBER || ''') AND ';
          END IF;
        END IF;
      END IF;
    END IF;


    --**********Check IN_SYSTEM**********
    IF (IN_SYSTEM IS NOT NULL AND IN_MEMBER_NUMBER IS NULL) THEN
      IF IN_SYSTEM = ' ' THEN
        v_Filter_Where := v_Filter_Where || 'q.system is null AND ';
      ELSE
        IF IN_QUEUE_TYPE = 'LP'
        THEN
          v_Filter_Where := v_Filter_Where || 'upper(coalesce         (' ||
                                                  '(select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id), ' ||
                                                  'q.system   )' ||
                                            ')=''' || IN_SYSTEM || ''' AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 'upper(coalesce         (' ||
                                                  'clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                                                  'decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null), ' ||
                                                  'q.system   )' ||
                                           ')=''' || IN_SYSTEM || ''' AND ';
        END IF;
      END IF;
    END IF;


    --**********Check IN_DELIVERY_DATE**********
    IF IN_DELIVERY_DATE IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc(q.delivery_date)=''' || IN_DELIVERY_DATE || ''' AND ';
    END IF;


    --**********Check IN_OCCASION_ID**********
    IF IN_OCCASION_ID IS NOT NULL
    THEN
      IF IN_OCCASION_ID = ' '
      THEN
        v_Filter_Where := v_Filter_Where || 'q.occasion IS NULL AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 'q.occasion=''' || IN_OCCASION_ID || ''' AND ';
      END IF;
    END IF;


    --**********Check IN_SAME_DAY_GIFT**********
    IF IN_SAME_DAY_GIFT IS NOT NULL THEN
      IF IN_SAME_DAY_GIFT = 'Y' THEN
        v_Filter_Where := v_Filter_Where || 'q.same_day_gift=''' || IN_SAME_DAY_GIFT || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || '(q.same_day_gift is null or q.same_day_gift = ''' || IN_SAME_DAY_GIFT || ''') AND ';
      END IF;
    END IF;


    --**********Check IN_DELIVERY_LOCATION_TYPE AND IN_ZIP_CODE**********
    IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_ZIP_CODE IS NOT NULL)
    THEN
      v_Filter_Where := v_Filter_Where || 'q.recipient_address_type = ''' || IN_DELIVERY_LOCATION_TYPE || ''' AND q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
    ELSE
      IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_DELIVERY_LOCATION_TYPE = ' ')
      THEN
        v_Filter_Where := v_Filter_Where || '(q.order_detail_id IS NULL or q.recipient_address_type IS NULL) AND ';
      ELSE
        IF IN_DELIVERY_LOCATION_TYPE IS NOT NULL
        THEN
          v_Filter_Where := v_Filter_Where || 'q.recipient_address_type =''' || IN_DELIVERY_LOCATION_TYPE || ''' AND ';
        ELSE
          IF IN_ZIP_CODE IS NOT NULL
          THEN
             v_Filter_Where := v_Filter_Where || 'q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
          END IF;
        END IF;
      END IF;
    END IF;


    --**********Check IN_TIMEZONE**********
    IF IN_TIMEZONE IS NOT NULL THEN
      IF IN_TIMEZONE = ' ' THEN
        v_Filter_Where := v_Filter_Where || 'q.timezone is null AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 'q.timezone = ' ||
                               'decode (''' || IN_TIMEZONE || ''', ' ||
                                        '''EST''' || ', 1, ' ||
                                        '''CST''' || ', 2, ' ||
                                        '''MST''' || ', 3, ' ||
                                        '''PST''' || ', 4, ' ||
                                        '''WST''' || ', 5, ' ||
                                        '1 ' ||
                                      ')' ||
                      ' AND ';
      END IF;
    END IF;

    --**********Check IN_LAST_TOUCHED**********
    IF IN_LAST_TOUCHED IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id))=''' || IN_LAST_TOUCHED || '''  AND ';
    END IF;


    --*******************************************************************************************
    --                               ****QUEUE_TAG****
    --*******************************************************************************************

    --**********Check IN_TAGGED_INDICATOR... if taged**********
    IF IN_TAGGED_INDICATOR = 'Y'
    THEN


      --**********Check IN_TAGGED_ON**********
      IF IN_TAGGED_ON IS NOT NULL
      THEN
        v_Filter_Where := v_Filter_Where || 'trunc(t.tagged_on)=''' || IN_TAGGED_ON || ''' AND ';
      END IF;


      --**********Check IN_TAG_DISPOSITION**********
      IF (IN_TAG_DISPOSITION IS NOT NULL)
      THEN
        IF (IN_TAG_DISPOSITION <> ' ')
        THEN
          v_Filter_Where := v_Filter_Where || 't.tag_disposition=''' || IN_TAG_DISPOSITION || ''' AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 't.tag_disposition IS NULL AND ';
        END IF;
      END IF;


      --**********Check IN_TAG_PRIORITY**********
      IF IN_TAG_PRIORITY IS NOT NULL
      THEN
        IF (IN_TAG_PRIORITY <> ' ')
        THEN
          v_Filter_Where := v_Filter_Where || 't.tag_priority=''' || IN_TAG_PRIORITY || ''' AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 't.tag_priority IS NULL AND ';
        END IF;
      END IF;


      --**********Check IN_TAGGED_BY**********
      IF IN_TAGGED_BY IS NOT NULL
      THEN
        v_Filter_Where := v_Filter_Where || 't.csr_id=''' || IN_TAGGED_BY || ''' AND ';
      END IF;

    END IF;


    --*******************************************************************************************
    --                               ****Mix****
    --*******************************************************************************************

    --**********Check IN_UNTAGGED_NEW_ITEMS**********
    IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND IN_UNTAGGED_NEW_ITEMS = 'Y')
    THEN
      v_Filter_Where := v_Filter_Where || 'q.message_id IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
    ELSE
      IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND (IN_UNTAGGED_NEW_ITEMS = 'N' OR IN_UNTAGGED_NEW_ITEMS = ' '))
      THEN
        v_Filter_Where := v_Filter_Where || 'q.message_id NOT IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
      END IF;
    END IF;

    --**********Check IN_PRODUCT_ID**********
    IF IN_PRODUCT_ID IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'q.product_id=''' || IN_PRODUCT_ID || ''' AND ';
    END IF;


--*******************************************************************************************
-- create a filter - end
--*******************************************************************************************

    v_Sql := GET_FILTERED_Q_SQL_WLMT_ARIBA
                          (IN_ORIGIN_ID
                          ,IN_QUEUE_TYPE
                          ,IN_TAGGED_INDICATOR
                          ,IN_ATTACHED_EMAIL_INDICATOR
                          ,IN_SORT_ORDER
                          ,IN_SORT_DIRECTION
                          ,IN_QUEUE_INDICATOR
                          ,v_Filter_Where
                          ,v_Sort_Option);

    IF v_Filter_Where IS NOT NULL
    THEN
      v_Filter_Where := ' WHERE ' || v_Filter_Where;
      v_Filter_Where := Substr(v_Filter_Where,1,Length(v_Filter_Where) - 4);
    END IF;


    -- execute the sql statement
    OPEN Sql_Cur FOR v_Sql;
    FETCH Sql_Cur
      INTO OUT_ID_COUNT;
    CLOSE Sql_Cur;
    v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                       ,'count(*)'
                                                                       ,'q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' ||
                                                                       Nvl(IN_START_POSITION,1) || ' AND ' || (Nvl(IN_START_POSITION,1) + Nvl(IN_MAX_NUMBER_RETURNED,OUT_ID_COUNT) - 1);


    v_Sql_Final :=
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion,
              q.premier_collection_flag
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                ' || v_Filter_Where ||' ORDER BY lst.ord';

  OPEN OUT_CUR FOR v_Sql_Final;


END GET_FILTERED_Q_WLMT_ARIBA;


/*------------------------------------------------------------------------------
Description:
  Returns all queue messages for the given external order number

Input:
  IN_EXTERNAL_ORDER_NUMBER
  IN_QUEUE_TYPES

Output:
  Cursor containing queue info
------------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_MSG_BY_EXT_ORD_NUM
(
IN_EXTERNAL_ORDER_NUMBER    IN QUEUE.EXTERNAL_ORDER_NUMBER%TYPE,
IN_QUEUE_TYPES              IN VARCHAR2,
OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS

-- variables
--QUEUE select query
v_sql               varchar2 (4000) := '';
v_sql_select        varchar2 (4000) := 'SELECT cq.message_id, cq.order_detail_id,  cq.queue_type,  cq.message_type,  cq.message_timestamp,  cq.mercury_number,  cq.external_order_number,  cq.mercury_id,  cq.system,  qtv.description,  qtv.queue_indicator,  cq.partner_name,  cq.price_ftd,  cq.price_askp,    COALESCE (  (SELECT DISTINCT m.mercury_order_number FROM mercury.mercury m   WHERE m.mercury_id = cq.mercury_id  ),  (SELECT DISTINCT m.mercury_order_number  FROM mercury.mercury m  WHERE m.mercury_order_number = cq.mercury_number  ),  (SELECT DISTINCT v.venus_order_number  FROM venus.venus v  WHERE v.venus_id = cq.mercury_id  ),  (SELECT DISTINCT v.venus_order_number  FROM venus.venus v  WHERE v.venus_order_number = cq.mercury_number  ) ) mercury_venus_order_number,  cq.point_of_contact_id   ';
v_sql_from          varchar2 (4000) := ' FROM  clean.queue cq, clean.mqd_queue_type_val qtv ';
v_sql_where         varchar2 (4000) := ' WHERE   cq.queue_type = qtv.queue_type ';
v_sql_and           varchar2 (4000) := ' AND     cq.external_order_number = ';

BEGIN

  v_sql := v_sql_select || v_sql_from || v_sql_where || v_sql_and || '''' || IN_EXTERNAL_ORDER_NUMBER || '''' ;

  IF IN_QUEUE_TYPES is not null THEN
    v_sql := v_sql || ' AND cq.queue_type in (' || IN_QUEUE_TYPES || ')';
  END IF;

  OPEN OUT_CURSOR FOR v_sql;


END GET_QUEUE_MSG_BY_EXT_ORD_NUM;


/*------------------------------------------------------------------------------
Description:
  For an order detail id and for the queue types provided, delete all the queue
  messages and tagged queues, and insert a queue delete history record

  Note: if queue type(s) are not provided, it will delete ALL queue and queue tag
  records based off of order detail id only.

Input:
  IN_ORDER_DETAIL_ID
  IN_QUEUE_TYPES
  IN_CSR_ID

Output:
  OUT_STATUS
  OUT_MESSAGE
------------------------------------------------------------------------------*/
PROCEDURE DLT_ALL_Q_RECS_ORD_DET_NOAUTH
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_QUEUE_TYPES                  IN VARCHAR2,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

-- variables
--QUEUE_DELETE_HISTORY insert query
v_sql_iqdh               varchar2 (4000) := '';
v_sql_iqdh_insert        varchar2 (4000) := ' INSERT INTO queue_delete_history (message_id, queue_type, tagged_by, tagged_on, queue_deleted_by, queue_deleted_on, order_guid, order_detail_id, master_order_number, external_order_number, queue_created_on, mercury_number, message_type) ';
v_sql_iqdh_select        varchar2 (4000) := ' SELECT q.message_id, q.queue_type, t.csr_id, t.tagged_on, ';
v_sql_iqdh_select_where  varchar2 (4000) := ' , sysdate, q.order_guid, q.order_detail_id, q.master_order_number, q.external_order_number, q.created_on, q.mercury_number, q.message_type FROM queue q, queue_tag t WHERE q.message_id = t.message_id(+) ';
v_sql_iqdh_and           varchar2 (4000) := ' AND q.order_detail_id = ';

--QUEUE_TAG delete query
v_sql_dqt                varchar2 (4000) := '';
v_sql_dqt_delete         varchar2 (4000) := ' DELETE FROM queue_tag qt WHERE qt.message_id in  ';
v_sql_dqt_select_where   varchar2 (4000) := ' ( SELECT q.message_id FROM   queue q WHERE  q.order_detail_id = ';
v_sql_dqt_and            varchar2 (4000) := '';

--QUEUE delete query
v_sql_dq                 varchar2 (4000) := '';
v_sql_dq_delete_where    varchar2 (4000) := ' DELETE FROM queue q WHERE q.order_detail_id = ';
v_sql_dq_and             varchar2 (4000) := '';

BEGIN

  v_sql_iqdh_and := v_sql_iqdh_and || IN_ORDER_DETAIL_ID;

  IF IN_QUEUE_TYPES is not null THEN
    v_sql_iqdh_and := v_sql_iqdh_and || ' AND q.queue_type in (' || IN_QUEUE_TYPES || ')';
    v_sql_dqt_and  := v_sql_dqt_and  || ' AND q.queue_type in (' || IN_QUEUE_TYPES || ')';
    v_sql_dq_and   := v_sql_dq_and  || ' AND q.queue_type in (' || IN_QUEUE_TYPES || ')';
  END IF;

  v_sql_iqdh := v_sql_iqdh_insert || v_sql_iqdh_select || '''' || IN_CSR_ID || ''''  || v_sql_iqdh_select_where || v_sql_iqdh_and;
  v_sql_dqt  := v_sql_dqt_delete || v_sql_dqt_select_where    || IN_ORDER_DETAIL_ID || v_sql_dqt_and || ')';
  v_sql_dq   := v_sql_dq_delete_where || IN_ORDER_DETAIL_ID || v_sql_dq_and;


  -- insert the CLEAN.QUEUE_DELETE_HISTORY records for ALL queued records for the given order detail id
  execute immediate v_sql_iqdh;

  -- delete records from CLEAN.QUEUE_TAG for ALL the tagged orders for the given order detail id
  execute immediate v_sql_dqt;

  -- delete CLEAN.QUEUE for ALL the queue records for the given order detail id
  execute immediate v_sql_dq;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DLT_ALL_Q_RECS_ORD_DET_NOAUTH;


/*------------------------------------------------------------------------------
Description:
  For an external order number and for the queue types provided, delete all the
  queue messages and tagged queues, and insert a queue delete history record

  Note: if queue type(s) are not provided, it will delete ALL queue and queue tag
  records based off of external order number only.

Input:
  IN_EXTERNAL_ORDER_NUMBER
  IN_QUEUE_TYPES
  IN_CSR_ID

Output:
  OUT_STATUS
  OUT_MESSAGE
------------------------------------------------------------------------------*/
PROCEDURE DLT_ALL_Q_RECS_EXT_ORD_NOAUTH
(
IN_EXTERNAL_ORDER_NUMBER        IN QUEUE.EXTERNAL_ORDER_NUMBER%TYPE,
IN_QUEUE_TYPES                  IN VARCHAR2,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

-- variables
--QUEUE_DELETE_HISTORY insert query
v_sql_iqdh               varchar2 (4000) := '';
v_sql_iqdh_insert        varchar2 (4000) := ' INSERT INTO queue_delete_history (message_id, queue_type, tagged_by, tagged_on, queue_deleted_by, queue_deleted_on, order_guid, order_detail_id, master_order_number, external_order_number, queue_created_on, mercury_number, message_type) ';
v_sql_iqdh_select        varchar2 (4000) := ' SELECT q.message_id, q.queue_type, t.csr_id, t.tagged_on, ';
v_sql_iqdh_select_where  varchar2 (4000) := ' , sysdate, q.order_guid, q.order_detail_id, q.master_order_number, q.external_order_number, q.created_on, q.mercury_number, q.message_type FROM queue q, queue_tag t WHERE q.message_id = t.message_id(+) ';
v_sql_iqdh_and           varchar2 (4000) := ' AND q.external_order_number = ';

--QUEUE_TAG delete query
v_sql_dqt                varchar2 (4000) := '';
v_sql_dqt_delete         varchar2 (4000) := ' DELETE FROM queue_tag qt WHERE qt.message_id in  ';
v_sql_dqt_select_where   varchar2 (4000) := ' ( SELECT q.message_id FROM   queue q WHERE  q.external_order_number = ';
v_sql_dqt_and            varchar2 (4000) := '';

--QUEUE delete query
v_sql_dq                 varchar2 (4000) := '';
v_sql_dq_delete_where    varchar2 (4000) := ' DELETE FROM queue q WHERE q.external_order_number = ';
v_sql_dq_and             varchar2 (4000) := '';

BEGIN

  v_sql_iqdh_and := v_sql_iqdh_and || '''' ||  IN_EXTERNAL_ORDER_NUMBER || '''';

  IF IN_QUEUE_TYPES is not null THEN
    v_sql_iqdh_and := v_sql_iqdh_and || ' AND q.queue_type in (' || IN_QUEUE_TYPES || ')';
    v_sql_dqt_and  := v_sql_dqt_and  || ' AND q.queue_type in (' || IN_QUEUE_TYPES || ')';
    v_sql_dq_and   := v_sql_dq_and  || ' AND q.queue_type in (' || IN_QUEUE_TYPES || ')';
  END IF;

  v_sql_iqdh := v_sql_iqdh_insert || v_sql_iqdh_select || '''' || IN_CSR_ID || ''''  || v_sql_iqdh_select_where || v_sql_iqdh_and;
  v_sql_dqt  := v_sql_dqt_delete || v_sql_dqt_select_where || '''' || IN_EXTERNAL_ORDER_NUMBER || '''' || v_sql_dqt_and || ')';
  v_sql_dq   := v_sql_dq_delete_where || '''' || IN_EXTERNAL_ORDER_NUMBER || '''' || v_sql_dq_and;

  -- insert the CLEAN.QUEUE_DELETE_HISTORY records for ALL queued records for the given order detail id
  execute immediate v_sql_iqdh;

  -- delete records from CLEAN.QUEUE_TAG for ALL the tagged orders for the given order detail id
  execute immediate v_sql_dqt;

  -- delete CLEAN.QUEUE for ALL the queue records for the given order detail id
  execute immediate v_sql_dq;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END DLT_ALL_Q_RECS_EXT_ORD_NOAUTH;


/*------------------------------------------------------------------------------
                        FUNCTION GET_QUEUE_SQL_PRM_COL

Description:
    This procedure is responsible for returning the sql statement to query the
    queue based on the given parameters.

Input:
    IN_TAGGED_INDICATOR         varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER               varchar2 - extra sort column
    IN_SORT_DIRECTION           varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR          varchar2 - Mercury or Email
    IN_EXCLUDE_PARTNERS         varchar2 - Partners To Exclude

Output:
    the statement

------------------------------------------------------------------------------*/
FUNCTION GET_QUEUE_SQL_PRM_COL
(
IN_TAGGED_INDICATOR          IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR  IN VARCHAR2,
IN_SORT_ORDER                IN VARCHAR2,
IN_SORT_DIRECTION            IN VARCHAR2,
IN_QUEUE_INDICATOR           IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_EXCLUDE_PARTNERS          IN VARCHAR2,
OUT_SORT_ORDER              OUT VARCHAR2
)
RETURN VARCHAR2
AS

-- join clause variables
v_Join VARCHAR2(4000);

-- where clause variables
v_Where VARCHAR2(4000) := 'WHERE ';

Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';

BEGIN

  --exclude walmart and ariba orders.
  v_Where := v_Where || ' nvl(q.origin_id,''TEST'') NOT in (''WLMTI'', ''ARI'', ''CAT'') AND ';

  -- build the join clause if attached email indicator is specified
  IF IN_ATTACHED_EMAIL_INDICATOR IS NOT NULL THEN
    v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN IN_ATTACHED_EMAIL_INDICATOR = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';
  ELSE
    -- otherwise, limit the results to the specified queue indicator
    IF  IN_QUEUE_INDICATOR = 'Mercury' THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator in (''Mercury'', ''Order'') ';
    ELSE
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' ';
    END IF;
  END IF;

  -- build the join clause if tagged items is specified
  IF IN_TAGGED_INDICATOR = 'Y' THEN
    v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';
  ELSE
    -- build the where clause if untagged items is specified
    v_Where := v_Where || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
  END IF;

  -- build the sort order
  OUT_SORT_ORDER := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
  OUT_SORT_ORDER := OUT_SORT_ORDER || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

  -- build the order by clause given the page column headings, the computed fields in the order by should
  -- correspond with computed fields in the end result set(the very last select statement)
  CASE Nvl(IN_SORT_ORDER ,'NULL')

    WHEN 'NULL' THEN
      NULL;

    WHEN 'system' THEN
      OUT_SORT_ORDER := 'DECODE(q.queue_type, ''LP''' || ', COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system), ' ||
                        'COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                        'DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'delivery_date' THEN
      OUT_SORT_ORDER := 'q.delivery_date ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'q.delivery_date,');

    WHEN 'timezone' THEN
      OUT_SORT_ORDER := 'key_3 ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'key_3,');

    WHEN 'same_day_gift' THEN
      OUT_SORT_ORDER := 'NVL(q.same_day_gift, ''N'') ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'product_id' THEN
      OUT_SORT_ORDER := 'q.product_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'untagged_new_items' THEN
      OUT_SORT_ORDER := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' || ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'tagged_by' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.csr_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'tagged_on' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.tagged_on ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'last_touched' THEN
      OUT_SORT_ORDER := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'order_detail_id' THEN
      OUT_SORT_ORDER := 'q.order_detail_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'external_order_number' THEN
      OUT_SORT_ORDER := 'q.external_order_number ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'member_number' THEN
      OUT_SORT_ORDER := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' || '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'delivery_location_type' THEN
      OUT_SORT_ORDER := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'zip_code' THEN
      OUT_SORT_ORDER := ' q.recipient_zip_code ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'occasion' THEN
      OUT_SORT_ORDER := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;


    WHEN 'tag_priority' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    ELSE
      OUT_SORT_ORDER := 'q.' || IN_SORT_ORDER || ' ' || IN_SORT_DIRECTION || ', ' || REPLACE(REPLACE(OUT_SORT_ORDER,';q.' || Lower(IN_SORT_ORDER)), 'q.' || Lower(IN_SORT_ORDER) || ';');
  END CASE;

  OUT_SORT_ORDER := REPLACE(REPLACE(OUT_SORT_ORDER,'key_3',Key_3),';',',');

  --Any Premier Collection order in the (LP, CREDIT) queues will be displayed in the (LP, CREDIT) queues; these orders will NOT be displayed under "PREMIER_COLLECTION" queue
  v_where := v_where || ' q.message_id in (select message_id from clean.queue q1 where q1.PREMIER_COLLECTION_FLAG = ''Y'' and q1.QUEUE_TYPE not in (''LP'', ''CREDIT'')) AND ';

  --IN_EXCLUDE_PARTNERS will not call this stored proc because they have their own virtual queue.  Therefore exclude IN_EXCLUDE_PARTNERS queue items from Premier Collection display
  v_where := nvl(v_where,'Where') || ' (q.partner_name is null or q.partner_name not in (' || IN_EXCLUDE_PARTNERS || ')) AND ';

  -- build the entire sql statement
  RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where, 1 ,Length(v_Where) - 4);

END GET_QUEUE_SQL_PRM_COL;


/*------------------------------------------------------------------------------
                                GET_QUEUE_PRM_COL

Description:
    This procedure is responsible for returning the items in a particular
    queue based on the given parameters for Walmart or Ariba orders

Input:
    IN_TAGGED_INDICATOR         varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER               varchar2 - extra sort column
    IN_SORT_DIRECTION           varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR          varchar2 - Mercury or Email
    IN_START_POSITION           number - index of the start item in the id array
    IN_MAX_NUMBER_RETURNED      number - number of items in the result set
    IN_EXCLUDE_PARTNERS         varchar2 - Partners To Exclude

Output:
    cursor containing all colums from queue
    number or items in the initial result set
    queue description

------------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_PRM_COL
(
IN_TAGGED_INDICATOR          IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR  IN VARCHAR2,
IN_SORT_ORDER                IN VARCHAR2,
IN_SORT_DIRECTION            IN VARCHAR2,
IN_QUEUE_INDICATOR           IN QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_START_POSITION            IN NUMBER,
IN_MAX_NUMBER_RETURNED       IN NUMBER,
IN_EXCLUDE_PARTNERS          IN VARCHAR2,
OUT_CUR                     OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                OUT NUMBER,
OUT_QUEUE_DESC              OUT VARCHAR2
)
AS

v_Sql           VARCHAR2(4000);
v_Sort_Option   VARCHAR2(4000);

-- cursor for the dynamic sql statement
Sql_Cur Types.Ref_Cursor;

-- cursor to get the queue description
CURSOR Desc_Cur IS
  SELECT 'Premier Collection'
  FROM dual;

BEGIN

  OPEN Desc_Cur;

  FETCH Desc_Cur
    INTO Out_Queue_Desc;
  CLOSE Desc_Cur;


  v_Sql :=  GET_QUEUE_SQL_PRM_COL
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_EXCLUDE_PARTNERS,
              v_Sort_Option
            );


  -- execute the sql statement
  OPEN Sql_Cur FOR v_Sql;

  FETCH Sql_Cur
    INTO Out_Id_Count;
  CLOSE Sql_Cur;

  v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql,'count(*)','q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' || Nvl(In_Start_Position,1) || ' AND ' || (Nvl(In_Start_Position,1) + Nvl(In_Max_Number_Returned,Out_Id_Count) - 1);


  OPEN Out_Cur FOR
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
      ORDER BY  lst.ord';


END GET_QUEUE_PRM_COL;


/*------------------------------------------------------------------------------
                            GET_QUEUE_COUNT_PRM_COL

Description:
    This procedure is responsible for returning the items in a particular
    queue based on the given parameters for Walmart or Ariba orders

Input:
    IN_TAGGED_INDICATOR          varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR  varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER                varchar2 - extra sort column
    IN_SORT_DIRECTION            varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR           varchar2 - Mercury or Email
    IN_EXCLUDE_PARTNERS          varchar2 - Partners To Exclude

Output:
    number or items in the initial result set

------------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_COUNT_PRM_COL
(
IN_TAGGED_INDICATOR          IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR  IN VARCHAR2,
IN_SORT_ORDER                IN VARCHAR2,
IN_SORT_DIRECTION            IN VARCHAR2,
IN_QUEUE_INDICATOR           IN QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_EXCLUDE_PARTNERS          IN VARCHAR2,
OUT_ID_COUNT                OUT NUMBER
)
AS

v_Sql         VARCHAR2(4000);
v_Sort_Option VARCHAR2(4000);

-- cursor for the dynamic sql statement
Sql_Cur Types.Ref_Cursor;

BEGIN

  v_Sql :=  GET_QUEUE_SQL_PRM_COL
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_EXCLUDE_PARTNERS,
              v_Sort_Option
            );


  -- execute the sql statement
  OPEN Sql_Cur FOR v_Sql;

  FETCH Sql_Cur
    INTO OUT_ID_COUNT;
  CLOSE Sql_Cur;


END GET_QUEUE_COUNT_PRM_COL;


/*------------------------------------------------------------------------------
                      GET_FILTER_VALUES_PRM_COL

Description:
    This procedure is responsible for returning the distinct items in a
    particular queue based on the given parameters, to be displayed in the
    filter dropdowns.

Input:
    IN_TAGGED_INDICATOR         varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER               varchar2 - extra sort column
    IN_SORT_DIRECTION           varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR          VARCHAR2 - MERCURY OR EMAIL
    IN_EXCLUDE_PARTNERS         varchar2 - Partners To Exclude

Output:
    OUT_TYPE_CUR                cursor containing distinct types
    OUT_SYS_CUR                 cursor containing distinct systems
    OUT_TIMEZONE_CUR            cursor containing distinct timezones
    OUT_SDG_CUR                 cursor containing distinct sdg
    OUT_DISPOSITION_CUR         cursor containing distinct disposition
    OUT_PRIORITY_CUR            cursor containing distinct priority
    OUT_NEW_ITEM_CUR            cursor containing distinct new_item
    OUT_OCCASION_CODE_CUR       cursor containing distinct occasion code
    OUT_LOCATION_TYPE_CUR       cursor containing distinct location type

------------------------------------------------------------------------------*/
PROCEDURE GET_FILTER_VALUES_PRM_COL
(
IN_TAGGED_INDICATOR           IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR   IN VARCHAR2,
IN_SORT_ORDER                 IN VARCHAR2,
IN_SORT_DIRECTION             IN VARCHAR2,
IN_QUEUE_INDICATOR            IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_EXCLUDE_PARTNERS           IN VARCHAR2,
OUT_TYPE_CUR                  OUT TYPES.REF_CURSOR,
OUT_SYS_CUR                   OUT TYPES.REF_CURSOR,
OUT_TIMEZONE_CUR              OUT TYPES.REF_CURSOR,
OUT_SDG_CUR                   OUT TYPES.REF_CURSOR,
OUT_DISPOSITION_CUR           OUT TYPES.REF_CURSOR,
OUT_PRIORITY_CUR              OUT TYPES.REF_CURSOR,
OUT_NEW_ITEM_CUR              OUT TYPES.REF_CURSOR,
OUT_OCCASION_CODE_CUR         OUT TYPES.REF_CURSOR,
OUT_LOCATION_TYPE_CUR         OUT TYPES.REF_CURSOR
)
AS

v_Sql         VARCHAR2(4000);
v_Sort_Option VARCHAR2(4000);

BEGIN

  v_Sql :=  GET_QUEUE_SQL_PRM_COL
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_EXCLUDE_PARTNERS,
              v_Sort_Option
            );


  v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql, 'count(*)', 'q.rowid dbindex') || '))t1 ';

  -- OUT_TYPE_CUR
  OPEN OUT_TYPE_CUR FOR ' SELECT
                                  q.message_type, count(q.message_type) message_type_count
                          FROM (' || v_Sql || ') lst
                          JOIN clean.queue q ON q.rowid = lst.dbindex
                          GROUP BY q.message_type
                        ';


  -- OUT_SYS_CUR
  OPEN OUT_SYS_CUR FOR  ' SELECT  distinct
                                  upper(
                                        decode (q.queue_type,
                                                ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                                                coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                                          decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                                          q.system
                                                         )
                                               )
                                       ) system
                          FROM (' || v_Sql || ') lst
                          JOIN clean.queue q ON q.rowid = lst.dbindex
                        ';


  -- OUT_TIMEZONE_CUR
  OPEN OUT_TIMEZONE_CUR FOR ' SELECT distinct
                                     decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone
                              FROM (' || v_Sql || ') lst
                              JOIN clean.queue q ON q.rowid = lst.dbindex
                            ';


  -- OUT_SDG_CUR
  OPEN OUT_SDG_CUR FOR  ' SELECT  distinct
                                  nvl (q.same_day_gift, ''N'') same_day_gift
                          FROM (' || v_Sql || ') lst
                          JOIN clean.queue q ON q.rowid = lst.dbindex
                        ';


  -- OUT_DISPOSITION_CUR
  IF IN_TAGGED_INDICATOR = 'N'
  THEN
    OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  null from dual';
  ELSE
    OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  distinct
                                            t.tag_disposition
                                    FROM (' || v_Sql || ') lst
                                    JOIN clean.queue q ON q.rowid = lst.dbindex
                                    LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                                  ';
  END IF;


  -- OUT_PRIORITY_CUR
  IF IN_TAGGED_INDICATOR = 'N'
  THEN
    OPEN OUT_PRIORITY_CUR FOR ' SELECT  null from dual';
  ELSE
    OPEN OUT_PRIORITY_CUR FOR ' SELECT  distinct
                                        t.tag_priority
                                FROM (' || v_Sql || ') lst
                                JOIN clean.queue q ON q.rowid = lst.dbindex
                                LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                              ';
  END IF;


  -- OUT_NEW_ITEM_CUR
  IF IN_TAGGED_INDICATOR = 'N'
  THEN
    OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  null from dual';
  ELSE
    OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  distinct
                                        ( select count (1) from clean.queue q2
                                          where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id)
                                          and    q2.message_id <> q.message_id
                                          and    q2.order_detail_id = q.order_detail_id
                                        ) untagged_new_items
                                FROM (' || v_Sql || ') lst
                                JOIN clean.queue q ON q.rowid = lst.dbindex
                                ';
  END IF;

  -- OUT_OCCASION_CODE_CUR
  OPEN OUT_OCCASION_CODE_CUR FOR  ' SELECT  distinct
                                            q.occasion occasion_id,
                                            (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion
                                    FROM (' || v_Sql || ') lst
                                    JOIN clean.queue q ON q.rowid = lst.dbindex
                                  ';


  -- OUT_LOCATION_TYPE_CUR
  OPEN OUT_LOCATION_TYPE_CUR FOR  ' SELECT  distinct
                                            q.recipient_address_type delivery_location_type
                                    FROM (' || v_Sql || ') lst
                                    JOIN clean.queue q ON q.rowid = lst.dbindex
                                  ';

END GET_FILTER_VALUES_PRM_COL;


/*------------------------------------------------------------------------------
                          GET_FILTERED_Q_SQL_PRM_COL

Description:
    This procedure is responsible for returning the sql statement to query the
    queue based on the given parameters.

Input:
    IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER                      varchar2 - extra sort column
    IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email
    IN_EXCLUDE_PARTNERS                varchar2 - Partners To Exclude
    V_FILTER_WHERE                     varchar2 - filter criteria

Output:
    the statement
------------------------------------------------------------------------------*/
FUNCTION GET_FILTERED_Q_SQL_PRM_COL
(
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_QUEUE_INDICATOR              IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_EXCLUDE_PARTNERS             IN VARCHAR2,
V_FILTER_WHERE                  IN VARCHAR2,
OUT_SORT_ORDER                  OUT VARCHAR2
)
RETURN VARCHAR2
AS

-- join clause variables
v_Join VARCHAR2(4000);

-- where clause variables
v_Where VARCHAR2(4000) := 'WHERE ';

Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';

BEGIN

  --exclude walmart and ariba orders.
  v_Where := v_Where || ' nvl(q.origin_id,''TEST'') NOT in (''WLMTI'', ''ARI'', ''CAT'') AND ';

  -- build the join clause if attached email indicator is specified
  IF IN_ATTACHED_EMAIL_INDICATOR IS NOT NULL THEN
    v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN IN_ATTACHED_EMAIL_INDICATOR = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';
  ELSE
    -- otherwise, limit the results to the specified queue indicator
    IF IN_QUEUE_INDICATOR = 'Mercury' THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator in (''Mercury'', ''Order'') ';
    ELSE
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' ';
    END IF;
  END IF;

  -- build the join clause if tagged items is specified
  IF IN_TAGGED_INDICATOR = 'Y' THEN
    v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';
  ELSE
    -- build the where clause if untagged items is specified
    v_Where := v_Where || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
  END IF;

  --Append the Queue Filter
  IF V_FILTER_WHERE IS NOT NULL THEN
    v_where := Nvl(v_Where ,'WHERE ') || V_FILTER_WHERE;
  END IF;

  -- build the sort order
  OUT_SORT_ORDER := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
  OUT_SORT_ORDER := OUT_SORT_ORDER || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

  -- build the order by clause given the page column headings, the computed fields in the order by should
  -- correspond with computed fields in the end result set(the very last select statement)
  CASE Nvl(IN_SORT_ORDER,'NULL')

    WHEN 'NULL' THEN
      NULL;

    WHEN 'system' THEN
        OUT_SORT_ORDER := 'DECODE(q.queue_type, ''LP''' || ',COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' || ',COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'delivery_date' THEN
      OUT_SORT_ORDER := 'q.delivery_date ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'q.delivery_date,');

    WHEN 'timezone' THEN
      OUT_SORT_ORDER := 'key_3 ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'key_3,');

    WHEN 'same_day_gift' THEN
      OUT_SORT_ORDER := 'NVL(q.same_day_gift, ''N'') ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'product_id' THEN
      OUT_SORT_ORDER := 'q.product_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'untagged_new_items' THEN
      OUT_SORT_ORDER := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' || ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'tagged_by' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.csr_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'tagged_on' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.tagged_on ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'last_touched' THEN
      OUT_SORT_ORDER := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'order_detail_id' THEN
      OUT_SORT_ORDER := 'q.order_detail_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'external_order_number' THEN
      OUT_SORT_ORDER := 'q.external_order_number ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'member_number' THEN
      OUT_SORT_ORDER := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' || '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'delivery_location_type' THEN
      OUT_SORT_ORDER := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'zip_code' THEN
      OUT_SORT_ORDER := ' q.recipient_zip_code ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'occasion' THEN
        OUT_SORT_ORDER := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'tag_priority' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    ELSE
      OUT_SORT_ORDER := 'q.' || IN_SORT_ORDER || ' ' || IN_SORT_DIRECTION || ', ' || REPLACE(REPLACE(OUT_SORT_ORDER,';q.' || Lower(IN_SORT_ORDER)),'q.' || Lower(IN_SORT_ORDER) || ';');
  END CASE;

  OUT_SORT_ORDER := REPLACE(REPLACE(OUT_SORT_ORDER,'key_3',Key_3),';',',');

  --Any Premier Collection order in the (LP, CREDIT) queues will be displayed in the (LP, CREDIT) queues; these orders will NOT be displayed under "PREMIER_COLLECTION" queue
  v_where := v_where || ' q.message_id in (select message_id from clean.queue q1 where q1.PREMIER_COLLECTION_FLAG = ''Y'' and q1.QUEUE_TYPE not in (''LP'', ''CREDIT'')) AND ';

  --IN_EXCLUDE_PARTNERS will not call this stored proc because they have their own virtual queue.  Therefore exclude IN_EXCLUDE_PARTNERS queue items from Premier Collection display
  v_where := nvl(v_where,'Where') || ' (q.partner_name is null or q.partner_name not in (' || IN_EXCLUDE_PARTNERS || ')) AND ';

  -- build the entire sql statement
  RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where,1,Length(v_Where) - 4);

END GET_FILTERED_Q_SQL_PRM_COL;


/*------------------------------------------------------------------------------
                      PROCEDURE GET_FILTERED_Q_PRM_COL

Description:
    This procedure is responsible for returning the items in a particular
    queue based on the given parameters for Walmart or Ariba orders

Input:
    IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER                      varchar2 - extra sort column
    IN_START_POSITION                  number - index of the start item in the id array
    IN_MAX_NUMBER_RETURNED             number - number of items in the result set
    IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email
    IN_MESSAGE_TIMESTAMP               date      - message timestamp
    IN_DELIVERY_DATE                   date     - delivery date
    IN_TAGGED_ON                       date      - date that the csr tagged the order
    IN_LAST_TOUCHED                    date     - date when the record was last updated
    IN_MESSAGE_TYPE                    varchar2 - message type
    IN_SYSTEM                          varchar2 - system type - mercury/venus/argos etc
    IN_TIMEZONE                        varchar2 - timezone - CST/EST/MST/PST/WST
    IN_SAME_DAY_GIFT                   varchar2 - same day gift indicator
    IN_TAG_DISPOSITION                 varchar2 - tag disposition
    IN_TAG_PRIORITY                    varchar2 - tag priority
    IN_UNTAGGED_NEW_ITEMS              varchar2 - indicator - if a given order detail has at
                                                  least one other item that is neither currently
                                                  nor ever been tagged, then 'Y'
    IN_OCCASION_ID                     varchar2 - occasion id
    IN_DELIVERY_LOCATION_TYPE          varchar2 - delivery location type - HOME/BUSINESS etc
    IN_MEMBER_NUMBER                   varchar2 - if drop ship order = Venus.Sending_Vendor
                                                  if floral order = Mercury.Sending_Florist
    IN_ZIP_CODE                        varchar2 - zip code / postal code
    IN_TAGGED_BY                       varchar2 - csr id who tagged the order
    IN_EXCLUDE_PARTNERS                varchar2 - Partners To Exclude

Output:
    cursor containing all colums from queue
    number or items in the initial result set
    queue description
------------------------------------------------------------------------------*/
PROCEDURE GET_FILTERED_Q_PRM_COL
(
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_QUEUE_INDICATOR              IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_MESSAGE_TIMESTAMP            IN CLEAN.QUEUE.MESSAGE_TIMESTAMP%TYPE,
IN_DELIVERY_DATE                IN CLEAN.QUEUE.DELIVERY_DATE%TYPE,
IN_TAGGED_ON                    IN CLEAN.QUEUE_TAG.TAGGED_ON%TYPE,
IN_LAST_TOUCHED                 IN CLEAN.CSR_VIEWED_ENTITIES.UPDATED_ON%TYPE,
IN_MESSAGE_TYPE                 IN CLEAN.QUEUE.MESSAGE_TYPE%TYPE,
IN_SYSTEM                       IN CLEAN.QUEUE.SYSTEM%TYPE,
IN_TIMEZONE                     IN VARCHAR2,
IN_SAME_DAY_GIFT                IN CLEAN.QUEUE.SAME_DAY_GIFT%TYPE,
IN_TAG_DISPOSITION              IN CLEAN.QUEUE_TAG.TAG_DISPOSITION%TYPE,
IN_TAG_PRIORITY                 IN CLEAN.QUEUE_TAG.TAG_PRIORITY%TYPE,
IN_UNTAGGED_NEW_ITEMS           IN VARCHAR2,
IN_OCCASION_ID                  IN CLEAN.QUEUE.OCCASION%TYPE,
IN_DELIVERY_LOCATION_TYPE       IN CLEAN.QUEUE.RECIPIENT_ADDRESS_TYPE%TYPE,
IN_MEMBER_NUMBER                IN VENUS.VENUS.SENDING_VENDOR%TYPE,
IN_ZIP_CODE                     IN CLEAN.QUEUE.RECIPIENT_ZIP_CODE%TYPE,
IN_TAGGED_BY                    IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_EXCLUDE_PARTNERS             IN VARCHAR2,
IN_PRODUCT_ID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER,
OUT_QUEUE_DESC                  OUT VARCHAR2
)
AS

v_Sql           VARCHAR2(9999);
v_Sort_Option   VARCHAR2(4000);
v_Sql_Final     VARCHAR2(9999);
v_Filter_Where  VARCHAR2(9999);


-- cursor for the dynamic sql statement
Sql_Cur Types.Ref_Cursor;

-- cursor to get the queue description
CURSOR Desc_Cur IS
  SELECT 'Premier Collection'
  FROM dual;

  BEGIN

    OPEN Desc_Cur;
    FETCH Desc_Cur
      INTO OUT_QUEUE_DESC;
    CLOSE Desc_Cur;

  --*******************************************************************************************
  -- create a filter - start
  --*******************************************************************************************

  --*******************************************************************************************
  --                                ****QUEUE****
  --*******************************************************************************************

  --**********Check IN_MESSAGE_TYPE**********
  IF IN_MESSAGE_TYPE IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'q.message_type=''' || IN_MESSAGE_TYPE || ''' AND ';
  END IF;


  --**********Check IN_MESSAGE_TIMESTAMP**********
  IF IN_MESSAGE_TIMESTAMP IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'trunc(q.message_timestamp)=''' || IN_MESSAGE_TIMESTAMP || ''' AND ';
  END IF;


    --**********Check IN_MEMBER_NUMBER**********
  IF IN_MEMBER_NUMBER IS NOT NULL
  THEN
    IF IN_SYSTEM IS NULL
    THEN
      RAISE_APPLICATION_ERROR(-20001, 'System id must be provided when searching for a memeber id');
    ELSE
      IF (IN_SYSTEM <> 'ARGO'          AND
          IN_SYSTEM <> 'VENUS'         AND
          IN_SYSTEM <> 'MERC'          AND
          IN_SYSTEM <> 'MERCURY'
         )
      THEN
        RAISE_APPLICATION_ERROR(-20002, 'Please provide a valid System id');
      ELSE
        IF (IN_SYSTEM = 'ARGO' OR IN_SYSTEM = 'VENUS')
        THEN
          v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT v.venus_id FROM venus.venus v WHERE SENDING_VENDOR=''' || IN_MEMBER_NUMBER || ''') AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT m.mercury_id FROM mercury.mercury m WHERE decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST)=''' || IN_MEMBER_NUMBER || ''') AND ';
        END IF;
      END IF;
    END IF;
  END IF;


  --**********Check IN_SYSTEM**********
  IF (IN_SYSTEM IS NOT NULL AND IN_MEMBER_NUMBER IS NULL) THEN
    IF IN_SYSTEM = ' ' THEN
      v_Filter_Where := v_Filter_Where || 'q.system is null AND ';
    ELSE
      IF IN_QUEUE_TYPE = 'LP'
      THEN
        v_Filter_Where := v_Filter_Where || 'upper(coalesce         (' ||
                                                '(select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id), ' ||
                                                'q.system   )' ||
                                          ')=''' || IN_SYSTEM || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 'upper(coalesce         (' ||
                                                'clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                                                'decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null), ' ||
                                                'q.system   )' ||
                                         ')=''' || IN_SYSTEM || ''' AND ';
      END IF;
    END IF;
  END IF;


  --**********Check IN_DELIVERY_DATE**********
  IF IN_DELIVERY_DATE IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'trunc(q.delivery_date)=''' || IN_DELIVERY_DATE || ''' AND ';
  END IF;


  --**********Check IN_OCCASION_ID**********
  IF IN_OCCASION_ID IS NOT NULL
  THEN
    IF IN_OCCASION_ID = ' '
    THEN
      v_Filter_Where := v_Filter_Where || 'q.occasion IS NULL AND ';
    ELSE
      v_Filter_Where := v_Filter_Where || 'q.occasion=''' || IN_OCCASION_ID || ''' AND ';
    END IF;
  END IF;


  --**********Check IN_SAME_DAY_GIFT**********
  IF IN_SAME_DAY_GIFT IS NOT NULL THEN
    IF IN_SAME_DAY_GIFT = 'Y' THEN
      v_Filter_Where := v_Filter_Where || 'q.same_day_gift=''' || IN_SAME_DAY_GIFT || ''' AND ';
    ELSE
      v_Filter_Where := v_Filter_Where || '(q.same_day_gift is null or q.same_day_gift = ''' || IN_SAME_DAY_GIFT || ''') AND ';
    END IF;
  END IF;


  --**********Check IN_DELIVERY_LOCATION_TYPE AND IN_ZIP_CODE**********
  IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_ZIP_CODE IS NOT NULL)
  THEN
    v_Filter_Where := v_Filter_Where || 'q.recipient_address_type = ''' || IN_DELIVERY_LOCATION_TYPE || ''' AND q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
  ELSE
    IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_DELIVERY_LOCATION_TYPE = ' ')
    THEN
      v_Filter_Where := v_Filter_Where || '(q.order_detail_id IS NULL or q.recipient_address_type IS NULL) AND ';
    ELSE
      IF IN_DELIVERY_LOCATION_TYPE IS NOT NULL
      THEN
        v_Filter_Where := v_Filter_Where || 'q.recipient_address_type =''' || IN_DELIVERY_LOCATION_TYPE || ''' AND ';
      ELSE
        IF IN_ZIP_CODE IS NOT NULL
        THEN
           v_Filter_Where := v_Filter_Where || 'q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
        END IF;
      END IF;
    END IF;
  END IF;


  --**********Check IN_TIMEZONE**********
  IF IN_TIMEZONE IS NOT NULL THEN
    IF IN_TIMEZONE = ' ' THEN
      v_Filter_Where := v_Filter_Where || 'q.timezone IS NULL AND ';
    ELSE
      v_Filter_Where := v_Filter_Where || 'q.timezone = ' ||
                             'decode (''' || IN_TIMEZONE || ''', ' ||
                                      '''EST''' || ', 1, ' ||
                                      '''CST''' || ', 2, ' ||
                                      '''MST''' || ', 3, ' ||
                                      '''PST''' || ', 4, ' ||
                                      '''WST''' || ', 5, ' ||
                                      '1 ' ||
                                    ')' ||
                    ' AND ';
    END IF;
  END IF;

  --**********Check IN_LAST_TOUCHED**********
  IF IN_LAST_TOUCHED IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'trunc (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id))=''' || IN_LAST_TOUCHED || '''  AND ';
  END IF;


  --*******************************************************************************************
  --                               ****QUEUE_TAG****
  --*******************************************************************************************

  --**********Check IN_TAGGED_INDICATOR... if taged**********
  IF IN_TAGGED_INDICATOR = 'Y'
  THEN


    --**********Check IN_TAGGED_ON**********
    IF IN_TAGGED_ON IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc(t.tagged_on)=''' || IN_TAGGED_ON || ''' AND ';
    END IF;


    --**********Check IN_TAG_DISPOSITION**********
    IF (IN_TAG_DISPOSITION IS NOT NULL)
    THEN
      IF (IN_TAG_DISPOSITION <> ' ')
      THEN
        v_Filter_Where := v_Filter_Where || 't.tag_disposition=''' || IN_TAG_DISPOSITION || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 't.tag_disposition IS NULL AND ';
      END IF;
    END IF;


    --**********Check IN_TAG_PRIORITY**********
    IF IN_TAG_PRIORITY IS NOT NULL
    THEN
      IF (IN_TAG_PRIORITY <> ' ')
      THEN
        v_Filter_Where := v_Filter_Where || 't.tag_priority=''' || IN_TAG_PRIORITY || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 't.tag_priority IS NULL AND ';
      END IF;
    END IF;


    --**********Check IN_TAGGED_BY**********
    IF IN_TAGGED_BY IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 't.csr_id=''' || IN_TAGGED_BY || ''' AND ';
    END IF;

  END IF;


  --*******************************************************************************************
  --                               ****Mix****
  --*******************************************************************************************

  --**********Check IN_UNTAGGED_NEW_ITEMS**********
  IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND IN_UNTAGGED_NEW_ITEMS = 'Y')
  THEN
    v_Filter_Where := v_Filter_Where || 'q.message_id IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
  ELSE
    IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND (IN_UNTAGGED_NEW_ITEMS = 'N' OR IN_UNTAGGED_NEW_ITEMS = ' '))
    THEN
      v_Filter_Where := v_Filter_Where || 'q.message_id NOT IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
    END IF;
  END IF;

  --**********Check IN_PRODUCT_ID**********
  IF IN_PRODUCT_ID IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'q.product_id=''' || IN_PRODUCT_ID || ''' AND ';
  END IF;


  --*******************************************************************************************
  -- create a filter - end
  --*******************************************************************************************

  v_Sql :=  GET_FILTERED_Q_SQL_PRM_COL
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_EXCLUDE_PARTNERS,
              v_Filter_Where,
              v_Sort_Option
            );

  IF v_Filter_Where IS NOT NULL
  THEN
    v_Filter_Where := ' WHERE ' || v_Filter_Where;
    v_Filter_Where := Substr(v_Filter_Where,1,Length(v_Filter_Where) - 4);
  END IF;


  -- execute the sql statement
  OPEN Sql_Cur FOR v_Sql;
  FETCH Sql_Cur
    INTO OUT_ID_COUNT;
  CLOSE Sql_Cur;
  v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                     ,'count(*)'
                                                                     ,'q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' ||
                                                                     Nvl(IN_START_POSITION,1) || ' AND ' || (Nvl(IN_START_POSITION,1) + Nvl(IN_MAX_NUMBER_RETURNED,OUT_ID_COUNT) - 1);


  v_Sql_Final :=
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                ' || v_Filter_Where ||' ORDER BY lst.ord';


  OPEN OUT_CUR FOR v_Sql_Final;

END GET_FILTERED_Q_PRM_COL;


/*------------------------------------------------------------------------------
                        FUNCTION GET_QUEUE_SQL_PARTNER

Description:
    This procedure is responsible for returning the sql statement to query the
    queue based on the given parameters.

Input:
    IN_TAGGED_INDICATOR         varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER               varchar2 - extra sort column
    IN_SORT_DIRECTION           varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR          varchar2 - Mercury or Email
    IN_PARTNER_NAME             varchar2 - partner name

Output:
    the statement

------------------------------------------------------------------------------*/
FUNCTION GET_QUEUE_SQL_PARTNER
(
IN_TAGGED_INDICATOR          IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR  IN VARCHAR2,
IN_SORT_ORDER                IN VARCHAR2,
IN_SORT_DIRECTION            IN VARCHAR2,
IN_QUEUE_INDICATOR           IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_PARTNER_NAME              IN CLEAN.QUEUE.PARTNER_NAME%TYPE,
OUT_SORT_ORDER              OUT VARCHAR2
)
RETURN VARCHAR2
AS

-- join clause variables
v_Join VARCHAR2(4000);

-- where clause variables
v_Where VARCHAR2(4000) := 'WHERE ';

Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';

BEGIN

  -- build the join clause if attached email indicator is specified
  IF IN_ATTACHED_EMAIL_INDICATOR IS NOT NULL THEN
    v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN IN_ATTACHED_EMAIL_INDICATOR = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';
  ELSE
    -- otherwise, limit the results to the specified queue indicator
    IF  IN_QUEUE_INDICATOR = 'Mercury' THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator in (''Mercury'', ''Order'') ';
    ELSE
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' ';
    END IF;
  END IF;

  -- build the join clause if tagged items is specified
  IF IN_TAGGED_INDICATOR = 'Y' THEN
    v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';
  ELSE
    -- build the where clause if untagged items is specified
    v_Where := v_Where || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
  END IF;

  -- build the sort order
  OUT_SORT_ORDER := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
  OUT_SORT_ORDER := OUT_SORT_ORDER || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

  -- build the order by clause given the page column headings, the computed fields in the order by should
  -- correspond with computed fields in the end result set(the very last select statement)
  CASE Nvl(IN_SORT_ORDER ,'NULL')

    WHEN 'NULL' THEN
      NULL;

    WHEN 'system' THEN
      OUT_SORT_ORDER := 'DECODE(q.queue_type, ''LP''' || ', COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' || ', COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'delivery_date' THEN
      OUT_SORT_ORDER := 'q.delivery_date ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'q.delivery_date,');

    WHEN 'timezone' THEN
      OUT_SORT_ORDER := 'key_3 ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'key_3,');

    WHEN 'same_day_gift' THEN
      OUT_SORT_ORDER := 'NVL(q.same_day_gift, ''N'') ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'product_id' THEN
      OUT_SORT_ORDER := 'q.product_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'untagged_new_items' THEN
      OUT_SORT_ORDER := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' || ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'tagged_by' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.csr_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'tagged_on' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.tagged_on ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'last_touched' THEN
      OUT_SORT_ORDER := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'order_detail_id' THEN
      OUT_SORT_ORDER := 'q.order_detail_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'external_order_number' THEN
      OUT_SORT_ORDER := 'q.external_order_number ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'member_number' THEN
      OUT_SORT_ORDER := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' || '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'delivery_location_type' THEN
      OUT_SORT_ORDER := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'zip_code' THEN
      OUT_SORT_ORDER := ' q.recipient_zip_code ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'occasion' THEN
      OUT_SORT_ORDER := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;


    WHEN 'tag_priority' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    ELSE
      OUT_SORT_ORDER := 'q.' || IN_SORT_ORDER || ' ' || IN_SORT_DIRECTION || ', ' || REPLACE(REPLACE(OUT_SORT_ORDER,';q.' || Lower(IN_SORT_ORDER)), 'q.' || Lower(IN_SORT_ORDER) || ';');
  END CASE;

  OUT_SORT_ORDER := REPLACE(REPLACE(OUT_SORT_ORDER,'key_3',Key_3),';',',');

  --Get the queues based off of partner name
  v_where := nvl(v_where,'Where') || ' q.partner_name = ''' || IN_PARTNER_NAME || ''' AND ';

  -- build the entire sql statement
  RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where, 1 ,Length(v_Where) - 4);

END GET_QUEUE_SQL_PARTNER;


/*------------------------------------------------------------------------------
                                GET_QUEUE_PARTNER

Description:
    This procedure is responsible for returning the items in a particular
    queue based on the given parameters for Walmart or Ariba orders

Input:
    IN_TAGGED_INDICATOR         varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER               varchar2 - extra sort column
    IN_SORT_DIRECTION           varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR          varchar2 - Mercury or Email
    IN_PARTNER_NAME             varchar2 - partner name
    IN_START_POSITION           number - index of the start item in the id array
    IN_MAX_NUMBER_RETURNED      number - number of items in the result set

Output:
    cursor containing all colums from queue
    number or items in the initial result set
    queue description

------------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_PARTNER
(
IN_TAGGED_INDICATOR          IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR  IN VARCHAR2,
IN_SORT_ORDER                IN VARCHAR2,
IN_SORT_DIRECTION            IN VARCHAR2,
IN_QUEUE_INDICATOR           IN QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_PARTNER_NAME              IN CLEAN.QUEUE.PARTNER_NAME%TYPE,
IN_START_POSITION            IN NUMBER,
IN_MAX_NUMBER_RETURNED       IN NUMBER,
OUT_CUR                     OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                OUT NUMBER,
OUT_QUEUE_DESC              OUT VARCHAR2
)
AS

v_Sql           VARCHAR2(4000);
v_Sort_Option   VARCHAR2(4000);

-- cursor for the dynamic sql statement
Sql_Cur Types.Ref_Cursor;

-- cursor to get the queue description
CURSOR Desc_Cur IS
  SELECT IN_PARTNER_NAME
  FROM dual;

BEGIN

  OPEN Desc_Cur;

  FETCH Desc_Cur
    INTO OUT_QUEUE_DESC;
  CLOSE Desc_Cur;


  v_Sql :=  GET_QUEUE_SQL_PARTNER
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_PARTNER_NAME,
              v_Sort_Option
            );

  -- execute the sql statement
  OPEN Sql_Cur FOR v_Sql;

  FETCH Sql_Cur
    INTO OUT_ID_COUNT;
  CLOSE Sql_Cur;

  v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql,'count(*)','q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' || Nvl(IN_START_POSITION,1) || ' AND ' || (Nvl(IN_START_POSITION,1) + Nvl(IN_MAX_NUMBER_RETURNED,OUT_ID_COUNT) - 1);


  OPEN OUT_CUR FOR
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.FILLING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion,
              q.premier_collection_flag
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
      ORDER BY  lst.ord';


END GET_QUEUE_PARTNER;


/*------------------------------------------------------------------------------
                            GET_QUEUE_COUNT_PARTNER

Description:
    This procedure is responsible for returning the items in a particular
    queue based on the given parameters for Walmart or Ariba orders

Input:
    IN_TAGGED_INDICATOR          varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR  varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER                varchar2 - extra sort column
    IN_SORT_DIRECTION            varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR           varchar2 - Mercury or Email
    IN_PARTNER_NAME              varchar2 - partner name

Output:
    number or items in the initial result set

------------------------------------------------------------------------------*/
PROCEDURE GET_QUEUE_COUNT_PARTNER
(
IN_TAGGED_INDICATOR          IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR  IN VARCHAR2,
IN_SORT_ORDER                IN VARCHAR2,
IN_SORT_DIRECTION            IN VARCHAR2,
IN_QUEUE_INDICATOR           IN QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_PARTNER_NAME              IN CLEAN.QUEUE.PARTNER_NAME%TYPE,
OUT_ID_COUNT                OUT NUMBER
)
AS

v_Sql         VARCHAR2(4000);
v_Sort_Option VARCHAR2(4000);

-- cursor for the dynamic sql statement
Sql_Cur Types.Ref_Cursor;

BEGIN

  v_Sql :=  GET_QUEUE_SQL_PARTNER
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_PARTNER_NAME,
              v_Sort_Option
            );

  -- execute the sql statement
  OPEN Sql_Cur FOR v_Sql;

  FETCH Sql_Cur
    INTO OUT_ID_COUNT;
  CLOSE Sql_Cur;


END GET_QUEUE_COUNT_PARTNER;


/*------------------------------------------------------------------------------
                      GET_FILTER_VALUES_PARTNER

Description:
    This procedure is responsible for returning the distinct items in a
    particular queue based on the given parameters, to be displayed in the
    filter dropdowns.

Input:
    IN_TAGGED_INDICATOR         varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER               varchar2 - extra sort column
    IN_SORT_DIRECTION           varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR          VARCHAR2 - MERCURY OR EMAIL
    IN_PARTNER_NAME             varchar2 - partner name

Output:
    OUT_TYPE_CUR                cursor containing distinct types
    OUT_SYS_CUR                 cursor containing distinct systems
    OUT_TIMEZONE_CUR            cursor containing distinct timezones
    OUT_SDG_CUR                 cursor containing distinct sdg
    OUT_DISPOSITION_CUR         cursor containing distinct disposition
    OUT_PRIORITY_CUR            cursor containing distinct priority
    OUT_NEW_ITEM_CUR            cursor containing distinct new_item
    OUT_OCCASION_CODE_CUR       cursor containing distinct occasion code
    OUT_LOCATION_TYPE_CUR       cursor containing distinct location type

------------------------------------------------------------------------------*/
PROCEDURE GET_FILTER_VALUES_PARTNER
(
IN_TAGGED_INDICATOR           IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR   IN VARCHAR2,
IN_SORT_ORDER                 IN VARCHAR2,
IN_SORT_DIRECTION             IN VARCHAR2,
IN_QUEUE_INDICATOR            IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_PARTNER_NAME               IN CLEAN.QUEUE.PARTNER_NAME%TYPE,
OUT_TYPE_CUR                  OUT TYPES.REF_CURSOR,
OUT_SYS_CUR                   OUT TYPES.REF_CURSOR,
OUT_TIMEZONE_CUR              OUT TYPES.REF_CURSOR,
OUT_SDG_CUR                   OUT TYPES.REF_CURSOR,
OUT_DISPOSITION_CUR           OUT TYPES.REF_CURSOR,
OUT_PRIORITY_CUR              OUT TYPES.REF_CURSOR,
OUT_NEW_ITEM_CUR              OUT TYPES.REF_CURSOR,
OUT_OCCASION_CODE_CUR         OUT TYPES.REF_CURSOR,
OUT_LOCATION_TYPE_CUR         OUT TYPES.REF_CURSOR
)
AS

v_Sql         VARCHAR2(4000);
v_Sort_Option VARCHAR2(4000);

BEGIN

  v_Sql :=  GET_QUEUE_SQL_PARTNER
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_PARTNER_NAME,
              v_Sort_Option
            );


  v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql, 'count(*)', 'q.rowid dbindex') || '))t1 ';

  -- OUT_TYPE_CUR
  OPEN OUT_TYPE_CUR FOR ' SELECT
                                  q.message_type, count(q.message_type) message_type_count
                          FROM (' || v_Sql || ') lst
                          JOIN clean.queue q ON q.rowid = lst.dbindex
                          GROUP BY q.message_type
                        ';


  -- OUT_SYS_CUR
  OPEN OUT_SYS_CUR FOR  ' SELECT  distinct
                                  upper(
                                        decode (q.queue_type,
                                                ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                                                coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                                          decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                                          q.system
                                                         )
                                               )
                                       ) system
                          FROM (' || v_Sql || ') lst
                          JOIN clean.queue q ON q.rowid = lst.dbindex
                        ';


  -- OUT_TIMEZONE_CUR
  OPEN OUT_TIMEZONE_CUR FOR ' SELECT distinct
                                     decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone
                              FROM (' || v_Sql || ') lst
                              JOIN clean.queue q ON q.rowid = lst.dbindex
                            ';


  -- OUT_SDG_CUR
  OPEN OUT_SDG_CUR FOR  ' SELECT  distinct
                                  nvl (q.same_day_gift, ''N'') same_day_gift
                          FROM (' || v_Sql || ') lst
                          JOIN clean.queue q ON q.rowid = lst.dbindex
                        ';


  -- OUT_DISPOSITION_CUR
  IF IN_TAGGED_INDICATOR = 'N'
  THEN
    OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  null from dual';
  ELSE
    OPEN OUT_DISPOSITION_CUR FOR  ' SELECT  distinct
                                            t.tag_disposition
                                    FROM (' || v_Sql || ') lst
                                    JOIN clean.queue q ON q.rowid = lst.dbindex
                                    LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                                  ';
  END IF;


  -- OUT_PRIORITY_CUR
  IF IN_TAGGED_INDICATOR = 'N'
  THEN
    OPEN OUT_PRIORITY_CUR FOR ' SELECT  null from dual';
  ELSE
    OPEN OUT_PRIORITY_CUR FOR ' SELECT  distinct
                                        t.tag_priority
                                FROM (' || v_Sql || ') lst
                                JOIN clean.queue q ON q.rowid = lst.dbindex
                                LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                              ';
  END IF;


  -- OUT_NEW_ITEM_CUR
  IF IN_TAGGED_INDICATOR = 'N'
  THEN
    OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  null from dual';
  ELSE
    OPEN OUT_NEW_ITEM_CUR FOR ' SELECT  distinct
                                        ( select count (1) from clean.queue q2
                                          where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id)
                                          and    q2.message_id <> q.message_id
                                          and    q2.order_detail_id = q.order_detail_id
                                        ) untagged_new_items
                                FROM (' || v_Sql || ') lst
                                JOIN clean.queue q ON q.rowid = lst.dbindex
                                ';
  END IF;

  -- OUT_OCCASION_CODE_CUR
  OPEN OUT_OCCASION_CODE_CUR FOR  ' SELECT  distinct
                                            q.occasion occasion_id,
                                            (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion
                                    FROM (' || v_Sql || ') lst
                                    JOIN clean.queue q ON q.rowid = lst.dbindex
                                  ';


  -- OUT_LOCATION_TYPE_CUR
  OPEN OUT_LOCATION_TYPE_CUR FOR  ' SELECT  distinct
                                            q.recipient_address_type delivery_location_type
                                    FROM (' || v_Sql || ') lst
                                    JOIN clean.queue q ON q.rowid = lst.dbindex
                                  ';

END GET_FILTER_VALUES_PARTNER;


/*------------------------------------------------------------------------------
                          GET_FILTERED_Q_SQL_PARTNER

Description:
    This procedure is responsible for returning the sql statement to query the
    queue based on the given parameters.

Input:
    IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER                      varchar2 - extra sort column
    IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email
    IN_PARTNER_NAME                    varchar2 - partner name
    v_Filter_Where                     varchar2 - filter criteria

Output:
    the statement
------------------------------------------------------------------------------*/
FUNCTION GET_FILTERED_Q_SQL_PARTNER
(
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_QUEUE_INDICATOR              IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_PARTNER_NAME                 IN CLEAN.QUEUE.PARTNER_NAME%TYPE,
v_Filter_Where                  IN VARCHAR2,
OUT_SORT_ORDER                  OUT VARCHAR2
)
RETURN VARCHAR2
AS

-- join clause variables
v_Join VARCHAR2(4000);

-- where clause variables
v_Where VARCHAR2(4000) := 'WHERE ';

Key_3 VARCHAR2(4000) := 'NVL(q.timezone,1) ';

BEGIN

  -- build the join clause if attached email indicator is specified
  IF IN_ATTACHED_EMAIL_INDICATOR IS NOT NULL THEN
    v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' AND q.order_detail_id IS ' || (CASE WHEN IN_ATTACHED_EMAIL_INDICATOR = 'Y' THEN 'NOT ' ELSE '' END) || 'NULL ';
  ELSE
    -- otherwise, limit the results to the specified queue indicator
    IF IN_QUEUE_INDICATOR = 'Mercury' THEN
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator in (''Mercury'', ''Order'') ';
    ELSE
      v_Join := v_Join || 'JOIN clean.queue_type_val v ON v.queue_type=q.queue_type AND v.queue_indicator=''Email'' ';
    END IF;
  END IF;

  -- build the join clause if tagged items is specified
  IF IN_TAGGED_INDICATOR = 'Y' THEN
    v_Join := v_Join || 'JOIN clean.queue_tag t ON q.message_id=t.message_id ';
  ELSE
    -- build the where clause if untagged items is specified
    v_Where := v_Where || 'NOT EXISTS(SELECT 1 FROM clean.queue_tag t1 WHERE t1.message_id=q.message_id) AND ';
  END IF;

  --Append the Queue Filter
  IF v_Filter_Where IS NOT NULL THEN
    v_where := Nvl(v_Where ,'WHERE ') || v_Filter_Where;
  END IF;

  -- build the sort order
  OUT_SORT_ORDER := 'DECODE(q.queue_type, ''GEN'', q.message_timestamp, NULL);';
  OUT_SORT_ORDER := OUT_SORT_ORDER || 'q.delivery_date;key_3;DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7);q.message_id ';

  -- build the order by clause given the page column headings, the computed fields in the order by should
  -- correspond with computed fields in the end result set(the very last select statement)
  CASE Nvl(IN_SORT_ORDER,'NULL')

    WHEN 'NULL' THEN
      NULL;

    WHEN 'system' THEN
        OUT_SORT_ORDER := 'DECODE(q.queue_type, ''LP''' || ',COALESCE((SELECT h.reason_text FROM clean.order_hold h WHERE h.order_detail_id = q.order_detail_id),q.system)' || ',COALESCE(clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),DECODE((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', NULL), q.system)) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'delivery_date' THEN
      OUT_SORT_ORDER := 'q.delivery_date ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'q.delivery_date,');

    WHEN 'timezone' THEN
      OUT_SORT_ORDER := 'key_3 ' || IN_SORT_DIRECTION || ', ' || REPLACE(OUT_SORT_ORDER,'key_3,');

    WHEN 'same_day_gift' THEN
      OUT_SORT_ORDER := 'NVL(q.same_day_gift, ''N'') ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'product_id' THEN
      OUT_SORT_ORDER := 'q.product_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'untagged_new_items' THEN
      OUT_SORT_ORDER := '(SELECT COUNT(1) FROM clean.queue q2 WHERE  NOT EXISTS(SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id=q2.message_id)' || ' AND q2.message_id <> q.message_id AND q2.order_detail_id=q.order_detail_id) ' || IN_SORT_DIRECTION || ';' || OUT_SORT_ORDER;

    WHEN 'tagged_by' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.csr_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'tagged_on' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := 't.tagged_on ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'last_touched' THEN
      OUT_SORT_ORDER := 'clean.csr_viewed_locked_pkg.get_order_last_touched(q.order_detail_id) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'order_detail_id' THEN
      OUT_SORT_ORDER := 'q.order_detail_id ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'external_order_number' THEN
      OUT_SORT_ORDER := 'q.external_order_number ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'member_number' THEN
      OUT_SORT_ORDER := 'coalesce ( (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),' || '(SELECT v.SENDING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id) ) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'delivery_location_type' THEN
      OUT_SORT_ORDER := ' DECODE(q.recipient_address_type, ''FUNERAL HOME'', 1, ''HOSPITAL'', 2, ''BUSINESS'', 3, ''NURSING HOME'', 4, ''HOME'', 5, ''OTHER'', 6, 7) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'zip_code' THEN
      OUT_SORT_ORDER := ' q.recipient_zip_code ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'occasion' THEN
        OUT_SORT_ORDER := ' (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    WHEN 'tag_priority' THEN
      IF IN_TAGGED_INDICATOR <> 'Y' THEN
        v_Join := v_Join || 'LEFT OUTER JOIN clean.queue_tag t ON q.message_id=t.message_id ';
      END IF;
      OUT_SORT_ORDER := '(SELECT tv.sort_order FROM clean.tag_priority_val tv WHERE tv.tag_priority=t.tag_priority) ' || IN_SORT_DIRECTION || ', ' || OUT_SORT_ORDER;

    ELSE
      OUT_SORT_ORDER := 'q.' || IN_SORT_ORDER || ' ' || IN_SORT_DIRECTION || ', ' || REPLACE(REPLACE(OUT_SORT_ORDER,';q.' || Lower(IN_SORT_ORDER)),'q.' || Lower(IN_SORT_ORDER) || ';');
  END CASE;

  OUT_SORT_ORDER := REPLACE(REPLACE(OUT_SORT_ORDER,'key_3',Key_3),';',',');

  --Get the queues based off of partner name
  v_where := nvl(v_where,'Where') || ' q.partner_name = ''' || IN_PARTNER_NAME || ''' AND ';

  -- build the entire sql statement
  RETURN 'SELECT count(*) FROM clean.queue q ' || v_Join || Substr(v_Where,1,Length(v_Where) - 4);

END GET_FILTERED_Q_SQL_PARTNER;


/*------------------------------------------------------------------------------
                      PROCEDURE GET_FILTERED_Q_PARTNER

Description:
    This procedure is responsible for returning the items in a particular
    queue based on the given parameters for Walmart or Ariba orders

Input:
    IN_TAGGED_INDICATOR                varchar2 - Y/N if the item is tagged
    IN_ATTACHED_EMAIL_INDICATOR        varchar2 - Y/N if the email queue item is attached to an order
    IN_SORT_ORDER                      varchar2 - extra sort column
    IN_START_POSITION                  number - index of the start item in the id array
    IN_MAX_NUMBER_RETURNED             number - number of items in the result set
    IN_SORT_DIRECTION                  varchar2 - asc/desc for the extra sort order
    IN_QUEUE_INDICATOR                 varchar2 - Mercury or Email
    IN_PARTNER_NAME                    varchar2 - partner name
    IN_MESSAGE_TIMESTAMP               date      - message timestamp
    IN_DELIVERY_DATE                   date     - delivery date
    IN_TAGGED_ON                       date      - date that the csr tagged the order
    IN_LAST_TOUCHED                    date     - date when the record was last updated
    IN_MESSAGE_TYPE                    varchar2 - message type
    IN_SYSTEM                          varchar2 - system type - mercury/venus/argos etc
    IN_TIMEZONE                        varchar2 - timezone - CST/EST/MST/PST/WST
    IN_SAME_DAY_GIFT                   varchar2 - same day gift indicator
    IN_TAG_DISPOSITION                 varchar2 - tag disposition
    IN_TAG_PRIORITY                    varchar2 - tag priority
    IN_UNTAGGED_NEW_ITEMS              varchar2 - indicator - if a given order detail has at
                                                  least one other item that is neither currently
                                                  nor ever been tagged, then 'Y'
    IN_OCCASION_ID                     varchar2 - occasion id
    IN_DELIVERY_LOCATION_TYPE          varchar2 - delivery location type - HOME/BUSINESS etc
    IN_MEMBER_NUMBER                   varchar2 - if drop ship order = Venus.Sending_Vendor
                                                  if floral order = Mercury.Sending_Florist
    IN_ZIP_CODE                        varchar2 - zip code / postal code
    IN_TAGGED_BY                       varchar2 - csr id who tagged the order

Output:
    cursor containing all colums from queue
    number or items in the initial result set
    queue description
------------------------------------------------------------------------------*/
PROCEDURE GET_FILTERED_Q_PARTNER
(
IN_QUEUE_TYPE                   IN CLEAN.QUEUE.QUEUE_TYPE%TYPE,
IN_TAGGED_INDICATOR             IN VARCHAR2,
IN_ATTACHED_EMAIL_INDICATOR     IN VARCHAR2,
IN_SORT_ORDER                   IN VARCHAR2,
IN_SORT_DIRECTION               IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_QUEUE_INDICATOR              IN CLEAN.QUEUE_TYPE_VAL.QUEUE_INDICATOR%TYPE,
IN_PARTNER_NAME                 IN CLEAN.QUEUE.PARTNER_NAME%TYPE,
IN_MESSAGE_TIMESTAMP            IN CLEAN.QUEUE.MESSAGE_TIMESTAMP%TYPE,
IN_DELIVERY_DATE                IN CLEAN.QUEUE.DELIVERY_DATE%TYPE,
IN_TAGGED_ON                    IN CLEAN.QUEUE_TAG.TAGGED_ON%TYPE,
IN_LAST_TOUCHED                 IN CLEAN.CSR_VIEWED_ENTITIES.UPDATED_ON%TYPE,
IN_MESSAGE_TYPE                 IN CLEAN.QUEUE.MESSAGE_TYPE%TYPE,
IN_SYSTEM                       IN CLEAN.QUEUE.SYSTEM%TYPE,
IN_TIMEZONE                     IN VARCHAR2,
IN_SAME_DAY_GIFT                IN CLEAN.QUEUE.SAME_DAY_GIFT%TYPE,
IN_TAG_DISPOSITION              IN CLEAN.QUEUE_TAG.TAG_DISPOSITION%TYPE,
IN_TAG_PRIORITY                 IN CLEAN.QUEUE_TAG.TAG_PRIORITY%TYPE,
IN_UNTAGGED_NEW_ITEMS           IN VARCHAR2,
IN_OCCASION_ID                  IN CLEAN.QUEUE.OCCASION%TYPE,
IN_DELIVERY_LOCATION_TYPE       IN CLEAN.QUEUE.RECIPIENT_ADDRESS_TYPE%TYPE,
IN_MEMBER_NUMBER                IN VENUS.VENUS.SENDING_VENDOR%TYPE,
IN_ZIP_CODE                     IN CLEAN.QUEUE.RECIPIENT_ZIP_CODE%TYPE,
IN_TAGGED_BY                    IN CLEAN.QUEUE_TAG.CSR_ID%TYPE,
IN_PRODUCT_ID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_ID_COUNT                    OUT NUMBER,
OUT_QUEUE_DESC                  OUT VARCHAR2
)
AS

v_Sql           VARCHAR2(9999);
v_Sort_Option   VARCHAR2(4000);
v_Sql_Final     VARCHAR2(9999);
v_Filter_Where  VARCHAR2(9999);


-- cursor for the dynamic sql statement
Sql_Cur Types.Ref_Cursor;

-- cursor to get the queue description
CURSOR Desc_Cur IS
  SELECT IN_PARTNER_NAME
  FROM dual;

  BEGIN

    OPEN Desc_Cur;
    FETCH Desc_Cur
      INTO OUT_QUEUE_DESC;
    CLOSE Desc_Cur;

  --*******************************************************************************************
  -- create a filter - start
  --*******************************************************************************************

  --*******************************************************************************************
  --                                ****QUEUE****
  --*******************************************************************************************

  --**********Check IN_MESSAGE_TYPE**********
  IF IN_MESSAGE_TYPE IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'q.message_type=''' || IN_MESSAGE_TYPE || ''' AND ';
  END IF;


  --**********Check IN_MESSAGE_TIMESTAMP**********
  IF IN_MESSAGE_TIMESTAMP IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'trunc(q.message_timestamp)=''' || IN_MESSAGE_TIMESTAMP || ''' AND ';
  END IF;


    --**********Check IN_MEMBER_NUMBER**********
  IF IN_MEMBER_NUMBER IS NOT NULL
  THEN
    IF IN_SYSTEM IS NULL
    THEN
      RAISE_APPLICATION_ERROR(-20001, 'System id must be provided when searching for a memeber id');
    ELSE
      IF (IN_SYSTEM <> 'ARGO'          AND
          IN_SYSTEM <> 'VENUS'         AND
          IN_SYSTEM <> 'MERC'          AND
          IN_SYSTEM <> 'MERCURY'
         )
      THEN
        RAISE_APPLICATION_ERROR(-20002, 'Please provide a valid System id');
      ELSE
        IF (IN_SYSTEM = 'ARGO' OR IN_SYSTEM = 'VENUS')
        THEN
          v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT v.venus_id FROM venus.venus v WHERE SENDING_VENDOR=''' || IN_MEMBER_NUMBER || ''') AND ';
        ELSE
          v_Filter_Where := v_Filter_Where || 'q.mercury_id IN (SELECT m.mercury_id FROM mercury.mercury m WHERE decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST)=''' || IN_MEMBER_NUMBER || ''') AND ';
        END IF;
      END IF;
    END IF;
  END IF;


  --**********Check IN_SYSTEM**********
  IF (IN_SYSTEM IS NOT NULL AND IN_MEMBER_NUMBER IS NULL) THEN
    IF IN_SYSTEM = ' ' THEN
      v_Filter_Where := v_Filter_Where || 'q.system is null AND ';
    ELSE
      v_Filter_Where := v_Filter_Where || '(  upper( coalesce ' ||
                                                     '(' ||
                                                        '(select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id), ' ||
                                                        'q.system' ||
                                                     ')' ||
                                                  ')=''' || IN_SYSTEM || ''' OR ' ||
                                             'upper( coalesce ' ||
                                                     '(' ||
                                                        'clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)), ' ||
                                                        'decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null), ' ||
                                                        'q.system' ||
                                                     ')' ||
                                                  ')=''' || IN_SYSTEM ||
                                        ''') AND ';
    END IF;
  END IF;


  --**********Check IN_DELIVERY_DATE**********
  IF IN_DELIVERY_DATE IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'trunc(q.delivery_date)=''' || IN_DELIVERY_DATE || ''' AND ';
  END IF;


  --**********Check IN_OCCASION_ID**********
  IF IN_OCCASION_ID IS NOT NULL
  THEN
    IF IN_OCCASION_ID = ' '
    THEN
      v_Filter_Where := v_Filter_Where || 'q.occasion IS NULL AND ';
    ELSE
      v_Filter_Where := v_Filter_Where || 'q.occasion=''' || IN_OCCASION_ID || ''' AND ';
    END IF;
  END IF;


  --**********Check IN_SAME_DAY_GIFT**********
  IF IN_SAME_DAY_GIFT IS NOT NULL THEN
    IF IN_SAME_DAY_GIFT = 'Y' THEN
      v_Filter_Where := v_Filter_Where || 'q.same_day_gift=''' || IN_SAME_DAY_GIFT || ''' AND ';
    ELSE
      v_Filter_Where := v_Filter_Where || '(q.same_day_gift is null or q.same_day_gift = ''' || IN_SAME_DAY_GIFT || ''') AND ';
    END IF;
  END IF;


  --**********Check IN_DELIVERY_LOCATION_TYPE AND IN_ZIP_CODE**********
  IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_ZIP_CODE IS NOT NULL)
  THEN
    v_Filter_Where := v_Filter_Where || 'q.recipient_address_type = ''' || IN_DELIVERY_LOCATION_TYPE || ''' AND q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
  ELSE
    IF (IN_DELIVERY_LOCATION_TYPE IS NOT NULL AND IN_DELIVERY_LOCATION_TYPE = ' ')
    THEN
      v_Filter_Where := v_Filter_Where || '(q.order_detail_id IS NULL or q.recipient_address_type IS NULL) AND ';
    ELSE
      IF IN_DELIVERY_LOCATION_TYPE IS NOT NULL
      THEN
        v_Filter_Where := v_Filter_Where || 'q.recipient_address_type =''' || IN_DELIVERY_LOCATION_TYPE || ''' AND ';
      ELSE
        IF IN_ZIP_CODE IS NOT NULL
        THEN
           v_Filter_Where := v_Filter_Where || 'q.recipient_zip_code like ''' || IN_ZIP_CODE || '%'' AND ';
        END IF;
      END IF;
    END IF;
  END IF;


  --**********Check IN_TIMEZONE**********
  IF IN_TIMEZONE IS NOT NULL THEN
    IF IN_TIMEZONE = ' ' THEN
      v_Filter_Where := v_Filter_Where || 'q.timezone IS NULL AND ';
    ELSE
        v_Filter_Where := v_Filter_Where || 'q.timezone = ' ||
                               'decode (''' || IN_TIMEZONE || ''', ' ||
                                        '''EST''' || ', 1, ' ||
                                        '''CST''' || ', 2, ' ||
                                        '''MST''' || ', 3, ' ||
                                        '''PST''' || ', 4, ' ||
                                        '''WST''' || ', 5, ' ||
                                        '1 ' ||
                                      ')' ||
                      ' AND ';
    END IF;
  END IF;

  --**********Check IN_LAST_TOUCHED**********
  IF IN_LAST_TOUCHED IS NOT NULL
  THEN
    v_Filter_Where := v_Filter_Where || 'trunc (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id))=''' || IN_LAST_TOUCHED || '''  AND ';
  END IF;


  --*******************************************************************************************
  --                               ****QUEUE_TAG****
  --*******************************************************************************************

  --**********Check IN_TAGGED_INDICATOR... if taged**********
  IF IN_TAGGED_INDICATOR = 'Y'
  THEN


    --**********Check IN_TAGGED_ON**********
    IF IN_TAGGED_ON IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 'trunc(t.tagged_on)=''' || IN_TAGGED_ON || ''' AND ';
    END IF;


    --**********Check IN_TAG_DISPOSITION**********
    IF (IN_TAG_DISPOSITION IS NOT NULL)
    THEN
      IF (IN_TAG_DISPOSITION <> ' ')
      THEN
        v_Filter_Where := v_Filter_Where || 't.tag_disposition=''' || IN_TAG_DISPOSITION || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 't.tag_disposition IS NULL AND ';
      END IF;
    END IF;


    --**********Check IN_TAG_PRIORITY**********
    IF IN_TAG_PRIORITY IS NOT NULL
    THEN
      IF (IN_TAG_PRIORITY <> ' ')
      THEN
        v_Filter_Where := v_Filter_Where || 't.tag_priority=''' || IN_TAG_PRIORITY || ''' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 't.tag_priority IS NULL AND ';
      END IF;
    END IF;


    --**********Check IN_TAGGED_BY**********
    IF IN_TAGGED_BY IS NOT NULL
    THEN
      v_Filter_Where := v_Filter_Where || 't.csr_id=''' || IN_TAGGED_BY || ''' AND ';
    END IF;

  END IF;


  --*******************************************************************************************
  --                               ****Mix****
  --*******************************************************************************************

  --**********Check IN_UNTAGGED_NEW_ITEMS**********
  IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND IN_UNTAGGED_NEW_ITEMS = 'Y')
  THEN
    v_Filter_Where := v_Filter_Where || 'q.message_id IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
  ELSE
    IF (IN_UNTAGGED_NEW_ITEMS IS NOT NULL AND (IN_UNTAGGED_NEW_ITEMS = 'N' OR IN_UNTAGGED_NEW_ITEMS = ' '))
    THEN
      v_Filter_Where := v_Filter_Where || 'q.message_id NOT IN (SELECT q.message_id FROM clean.queue q2 WHERE NOT EXISTS (SELECT 1 FROM clean.queue_tag t2 WHERE t2.message_id = q2.message_id) AND q2.message_id <> q.message_id AND q2.order_detail_id = q.order_detail_id ) AND ';
    END IF;
  END IF;

  --**********Check IN_PRODUCT_ID**********
  IF IN_PRODUCT_ID IS NOT NULL
  THEN
    IF (IN_PRODUCT_ID = 'LDR')
      THEN
         v_Filter_Where := v_Filter_Where || 'q.codification_id=''NFF'' AND ';
      ELSE
        v_Filter_Where := v_Filter_Where || 'q.product_id=''' || IN_PRODUCT_ID || ''' AND ';
      END IF;
    
  END IF;


  --*******************************************************************************************
  -- create a filter - end
  --*******************************************************************************************

  v_Sql :=  GET_FILTERED_Q_SQL_PARTNER
            (
              IN_TAGGED_INDICATOR,
              IN_ATTACHED_EMAIL_INDICATOR,
              IN_SORT_ORDER,
              IN_SORT_DIRECTION,
              IN_QUEUE_INDICATOR,
              IN_PARTNER_NAME,
              v_Filter_Where,
              v_Sort_Option
            );

  IF v_Filter_Where IS NOT NULL
  THEN
    v_Filter_Where := ' WHERE ' || v_Filter_Where;
    v_Filter_Where := Substr(v_Filter_Where,1,Length(v_Filter_Where) - 4);
  END IF;


  -- execute the sql statement
  OPEN Sql_Cur FOR v_Sql;
  FETCH Sql_Cur
    INTO OUT_ID_COUNT;
  CLOSE Sql_Cur;
  v_Sql := 'SELECT * FROM ( SELECT dbindex,rownum ord FROM ( ' || REPLACE(v_Sql
                                                                     ,'count(*)'
                                                                     ,'q.rowid dbindex') || 'ORDER BY ' || v_Sort_Option || '))t1 where t1.ord BETWEEN ' ||
                                                                     Nvl(IN_START_POSITION,1) || ' AND ' || (Nvl(IN_START_POSITION,1) + Nvl(IN_MAX_NUMBER_RETURNED,OUT_ID_COUNT) - 1);


  v_Sql_Final :=
      'SELECT q.Message_Id,
              q.queue_type,
              q.message_type,
              to_char (q.message_timestamp, ''mm/dd hh:miAM'') message_timestamp,
              decode (  q.queue_type,
                        ''LP'', coalesce ((select max(h.reason_text) from clean.order_hold h where h.order_detail_id = q.order_detail_id),q.system),
                        coalesce (clean.order_query_pkg.get_scrub_status((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id)),
                                  decode ((SELECT od1.order_disp_code FROM clean.order_details od1 WHERE od1.order_detail_id = q.order_detail_id), ''Held'', ''HOLD'', null),
                                  q.system
                                 )
                     ) system,
              q.mercury_number,
              q.master_order_number,
              q.external_order_number,
              q.order_guid,
              q.order_detail_id,
              q.point_of_contact_id,
              decode (q.timezone, 1, ''EST'', 2, ''CST'', 3, ''MST'', 4, ''PST'', 5, ''WST'') timezone,
              to_char (q.delivery_date, ''mm/dd/yy'') delivery_date,
              nvl (q.same_day_gift, ''N'') same_day_gift,
              q.product_id,
              q.email_address,
              (select count (1) from clean.queue q2 where  not exists (select 1 from clean.queue_tag t2 where t2.message_id = q2.message_id) and q2.message_id <> q.message_id and q2.order_detail_id = q.order_detail_id) untagged_new_items,
              t.csr_id tagged_by,
              to_char (t.tagged_on, ''mm/dd'') tagged_on,
              to_char (clean.csr_viewed_locked_pkg.get_order_last_touched (q.order_detail_id), ''mm/dd hh:miAM'') last_touched,
              (select count (1) from clean.queue_tag t where t.message_id = q.message_id) number_of_tags,
              t.tag_priority,
              t.tag_disposition,
              decode ((select p.order_detail_id from clean.point_of_contact p where p.point_of_contact_id = q.point_of_contact_id), null, ''N'', ''Y'') attached_email_ind,
              t.tagged_on,
              clean.queue_pkg.is_tag_expired (tagged_on, tag_priority) is_order_tag_expired,
              coalesce (  (SELECT decode(m.MESSAGE_DIRECTION, ''OUTBOUND'', m.FILLING_FLORIST, m.SENDING_FLORIST) FROM mercury.mercury m WHERE m.mercury_id = q.mercury_id),
                          (SELECT v.FILLING_VENDOR FROM venus.venus v WHERE v.venus_id = q.mercury_id)
                       ) member_number,
              q.recipient_address_type delivery_location_type,
              q.recipient_zip_code zip_code,
              (SELECT occ.description FROM ftd_apps.occasion occ WHERE to_char(occ.occasion_id) = q.occasion) occasion,
              q.premier_collection_flag
      FROM      (' || v_Sql || ')lst
      JOIN      clean.queue q ON q.rowid = lst.dbindex
      LEFT JOIN clean.queue_tag t ON t.message_id=q.message_id
                ' || v_Filter_Where ||' ORDER BY lst.ord';


  OPEN OUT_CUR FOR v_Sql_Final;

END GET_FILTERED_Q_PARTNER;

PROCEDURE DELETE_ALL_UNTAGGED_Q_RECS
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_QUEUE_TYPE                   IN QUEUE.QUEUE_TYPE%TYPE,
IN_CSR_ID                       IN QUEUE_TAG.CSR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE               OUT VARCHAR2
)
AS

CURSOR untagged_cur IS
    SELECT  *
    FROM    queue q
    WHERE   q.order_detail_id = in_order_detail_id
    and (in_queue_type is null or q.queue_type = in_queue_type)
    and not exists (
        select 'Y'
        from clean.queue_tag t
        where t.message_id = q.message_id);

BEGIN

for rec in untagged_cur() loop
        -- insert the queue delete history record
        INSERT INTO queue_delete_history
        (
                message_id,
                queue_type,
                tagged_by,
                tagged_on,
                queue_deleted_by,
                queue_deleted_on,
                order_guid,
                order_detail_id,
                master_order_number,
                external_order_number,
                queue_created_on,
                mercury_number,
                message_type
        ) values (
                rec.message_id,
                rec.queue_type,
                null,
                null,
                in_csr_id,
                sysdate,
                rec.order_guid,
                rec.order_detail_id,
                rec.master_order_number,
                rec.external_order_number,
                rec.created_on,
                rec.mercury_number,
                rec.message_type);

        DELETE FROM queue
        WHERE   message_id = rec.message_id;

END LOOP;

out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
      OUT_STATUS := 'N';
      OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ALL_UNTAGGED_Q_RECS;

/*------------------------------------------------------------------------------
FUNCTION GET_EXT_ORD_NUM_BY_MSG_ID

Description:
    This function is responsible for returning the external order number
        if one exists for the associated message id

Input:
        message_id                 varchar2

Output:
        varchar2 containing external_order_number

------------------------------------------------------------------------------*/

FUNCTION GET_EXT_ORD_NUM_BY_MSG_ID
(
IN_MESSAGE_ID           IN QUEUE.MESSAGE_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_external_order_number VARCHAR2(20);

BEGIN

  BEGIN
    SELECT external_order_number INTO v_external_order_number
      FROM QUEUE
      WHERE MESSAGE_ID = IN_MESSAGE_ID;
    
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
      v_external_order_number := NULL;
  END;
  
  RETURN v_external_order_number;

END GET_EXT_ORD_NUM_BY_MSG_ID;

PROCEDURE DELETE_QUEUE_PHOENIX
(
IN_ORDER_DETAIL_ID              IN QUEUE.ORDER_DETAIL_ID%TYPE,
IN_QUEUE_TYPES                  IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN


        CLEAN.QUEUE_PKG.DLT_ALL_Q_RECS_ORD_DET_NOAUTH(IN_ORDER_DETAIL_ID, IN_QUEUE_TYPES,'Phoenix',OUT_STATUS,OUT_MESSAGE);
        IF OUT_STATUS <> 'N' THEN 
          COMMIT;
        END IF;
      
        EXCEPTION WHEN OTHERS THEN 
        BEGIN
            OUT_STATUS := 'N';
            OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        END;    

END DELETE_QUEUE_PHOENIX;

PROCEDURE GET_BULK_PHOENIX_ELGBLE_ORDERS
(
OUT_CUR         OUT TYPES.REF_CURSOR
) AS

v_phoenix_queue_types VARCHAR2(32767);

BEGIN
         select gp.value into v_phoenix_queue_types
         from frp.global_parms gp
         where gp.context = 'PHOENIX' and gp.name = 'PHOENIX_ELIGIBLE_QUEUES';

         v_phoenix_queue_types := clean.order_query_pkg.convert_to_valid_sql_clause(v_phoenix_queue_types);

  OPEN OUT_CUR FOR
    'select distinct q.order_detail_id,q.queue_type,m.mercury_order_number
    from clean.queue q
    join clean.order_details od on q.order_detail_id = od.order_detail_id
    join clean.orders o on od.order_guid = o.order_guid
    join clean.customer recip on od.recipient_id = recip.customer_id
    join clean.email e on o.email_id = e.email_id
    join mercury.mercury m on to_char(od.order_detail_id) = m.reference_number
        and m.msg_type = ''FTD''
        and (m.mercury_status in (''MR'', ''ME'') OR exists (
            select 1 from mercury.mercury m1 
            where m1.mercury_order_number = m.mercury_order_number 
            and m.mercury_status = ''MC'' 
            and m1.msg_type in (''CAN'', ''REJ'') 
            and m1.mercury_status = ''MC''))
        and m.created_on = (
            select max(m2.created_on)
            from mercury.mercury m2
            where m2.reference_number = to_char(od.order_detail_id) and m2.msg_type = ''FTD'')
	and m.delivery_date between trunc(sysdate+1) and trunc(sysdate+1 + (select value from frp.global_parms where context = ''PHOENIX'' and name = ''DELIVERY_DAYS_OUT_MAX''))
    where q.queue_type in (' || v_phoenix_queue_types || ')
    and q.system = ''Merc''
	and q.created_on = (
            select max(q1.created_on)
            from clean.queue q1
            where q1.order_detail_id = q.order_detail_id
            and q1.queue_type in (' || v_phoenix_queue_types || '))
    and not exists (select 1 from clean.queue_tag qt where qt.message_id = q.message_id)
    and ( not exists(
  		     select 1
  		     from clean.order_add_ons oao
  		     where oao.order_detail_id = od.order_detail_id
  	  )
  	  or
  	  exists(
  	 	 select 1
  		 from clean.order_add_ons oao1
  		 where oao1.order_detail_id = od.order_detail_id
  		 and oao1.add_on_code in (''B'', ''C'')
  		 and oao1.add_on_quantity = 1 
  		 and not exists(
  				select 1 
  				from clean.order_add_ons oao2 
  				where od.order_detail_id = oao2.order_detail_id 
  				and oao2.add_on_code in (''B'', ''C'')
  				and oao2.add_on_quantity > 1
  		)
  		and not exists (
  				select 1 
  				from clean.order_add_ons oao1 
  			 	where od.order_detail_id = oao1.order_detail_id 
  			 	and oao1.add_on_code not in (''B'', ''C'')
  		)  
     	  )	  
    )	
    and not exists (select 1 from clean.queue q where q.order_detail_id = od.order_detail_id and q.message_type in (''MO''))
    and not exists (select 1 from clean.point_of_contact poc where poc.order_detail_id = od.order_detail_id and poc.EMAIL_SUBJECT = ''Novator Email Request: CX'' and poc.SENT_RECEIVED_INDICATOR = ''I'')
    and (od.ship_method is null or od.ship_method not in (''ND'', ''2F'', ''GR'', ''SA''))
    and exists(
    		select 1 
    		from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_details od2 
    		where ppm.original_product_id = od.product_id 
    		and DECODE(ppm.product_price_point_key, ''GOOD'', ''A'', ''BETTER'', ''B'', ''BEST'', ''C'') = od.size_indicator
                and od2.order_detail_id = od.order_detail_id 
    )
    and
    (select sum(ob.product_amount +
            ob.add_on_amount +
            ob.service_fee +
            ob.shipping_fee -
            ob.discount_amount +
            ob.shipping_tax +
            ob.tax)
     from clean.order_bills ob
     where ob.order_detail_id = od.order_detail_id)
     -
     nvl((select sum (r.refund_product_amount +
            r.refund_addon_amount +
            r.refund_service_fee +
            r.refund_shipping_fee -
            r.refund_discount_amount +
            r.refund_shipping_tax +
            r.refund_tax)
     from clean.refund r
     where r.order_detail_id = od.order_detail_id),0) > 0
     --Exclude funeral occasions
     and m.occasion not in (''FUNERAL OCCASION'')
     --Exclude address types
     and recip.address_type not in (''FUNERAL HOME'', ''HOSPITAL'', ''CEMETERY'')
     --Domestic orders only excluding Alaska, Hawaii & Puerto Rico & Virgin Islands
     and recip.country = ''US''
     and recip.state not in (''AK'', ''HI'', ''PR'', ''VI'')
     and (to_char(m.delivery_date, ''DY'') <> ''SAT''
          or exists (select distinct ''Y''
          from ftd_apps.carrier_zip_code czc
          where czc.zip_code = m.zip_code
          and czc.saturday_del_available = ''Y''))
    UNION ALL
    select distinct q.order_detail_id,q.queue_type,null as mercury_order_number
    from clean.queue q
    join clean.order_details od on q.order_detail_id = od.order_detail_id
    join clean.orders o on od.order_guid = o.order_guid
    join clean.customer recip on od.recipient_id = recip.customer_id
    join clean.email e on o.email_id = e.email_id
    where q.queue_type in (' || v_phoenix_queue_types || ')
    and q.system = ''Merc''
	and q.created_on = (
            select max(q1.created_on)
            from clean.queue q1
            where q1.order_detail_id = q.order_detail_id
            and q1.queue_type in (' || v_phoenix_queue_types || '))
    and od.delivery_date between trunc(sysdate+1) and trunc(sysdate+1 + (select value from frp.global_parms where context = ''PHOENIX'' and name = ''DELIVERY_DAYS_OUT_MAX''))
    and not exists (select 1 from clean.queue_tag qt where qt.message_id = q.message_id)
    and ( not exists(
  		     select 1
  		     from clean.order_add_ons oao
  		     where oao.order_detail_id = od.order_detail_id
  	  )
  	  or
  	  exists(
  	 	 select 1
  		 from clean.order_add_ons oao1
  		 where oao1.order_detail_id = od.order_detail_id
  		 and oao1.add_on_code in (''B'', ''C'')
  		 and oao1.add_on_quantity = 1 
  		 and not exists(
  				select 1 
  				from clean.order_add_ons oao2 
  				where od.order_detail_id = oao2.order_detail_id 
  				and oao2.add_on_code in (''B'', ''C'')
  				and oao2.add_on_quantity > 1
  		)
  		and not exists (
  				select 1 
  				from clean.order_add_ons oao1 
  			 	where od.order_detail_id = oao1.order_detail_id 
  			 	and oao1.add_on_code not in (''B'', ''C'')
  		)  
     	  )	  
    )	
    and not exists (select 1 from clean.queue q where q.order_detail_id = od.order_detail_id and q.message_type in (''MO''))
    and not exists (select 1 from clean.point_of_contact poc where poc.order_detail_id = od.order_detail_id and poc.EMAIL_SUBJECT = ''Novator Email Request: CX'' and poc.SENT_RECEIVED_INDICATOR = ''I'')
    and (od.ship_method is null or od.ship_method not in (''ND'', ''2F'', ''GR'', ''SA''))
    and exists(
    		select 1 
    		from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_details od2 
    		where ppm.original_product_id = od.product_id 
    		and DECODE(ppm.product_price_point_key, ''GOOD'', ''A'', ''BETTER'', ''B'', ''BEST'', ''C'') = od.size_indicator
                and od2.order_detail_id = od.order_detail_id 
    )
    and
        (select sum(ob.product_amount +
                    ob.add_on_amount +
                    ob.service_fee +
                    ob.shipping_fee -
                    ob.discount_amount +
                    ob.shipping_tax +
                    ob.tax)
        from clean.order_bills ob
        where ob.order_detail_id = od.order_detail_id)
        -
        nvl((select sum (r.refund_product_amount +
                    r.refund_addon_amount +
                    r.refund_service_fee +
                    r.refund_shipping_fee -
                    r.refund_discount_amount +
                    r.refund_shipping_tax +
                    r.refund_tax)
        from clean.refund r
        where r.order_detail_id = od.order_detail_id),0) > 0
    and not exists
    (
        select 1 
        from mercury.mercury m 
        where m.reference_number = to_char(od.order_detail_id) 
        and m.msg_type = ''FTD'' 
    )
    --Exclude funeral occasions
    and od.occasion not in (''FUNERAL OCCASION'')
    --Exclude address types
    and recip.address_type not in (''FUNERAL HOME'', ''HOSPITAL'', ''CEMETERY'')
    --Domestic orders only excluding Alaska, Hawaii & Puerto Rico & Virgin Islands
    and recip.country = ''US''
    and recip.state not in (''AK'', ''HI'', ''PR'', ''VI'')
    and (to_char(od.delivery_date, ''DY'') <> ''SAT''
         or exists (select distinct ''Y''
         from ftd_apps.carrier_zip_code czc
         where czc.zip_code = recip.zip_code
         and czc.saturday_del_available = ''Y''))';

END GET_BULK_PHOENIX_ELGBLE_ORDERS;

END;

.
/