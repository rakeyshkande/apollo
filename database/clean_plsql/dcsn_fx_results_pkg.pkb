create or replace PACKAGE BODY clean.DCSN_FX_RESULTS_PKG AS

   PROCEDURE GET_ORDER_INFO_BY_ORD_DET_IDS
  (
  IN_ENTITY_IDS                   IN VARCHAR2,
  OUT_ENTITY_DETAILS             OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for retrieving details of the orders
          viewed by CSR in the current session.

  Input:
          in_order_detail_ids                   varchar2


  Output:
          refcursor

  -----------------------------------------------------------------------------*/
  BEGIN

     OPEN out_entity_details FOR
    'SELECT od.order_detail_id,
          od.EXTERNAL_ORDER_NUMBER, 
           r.first_name,
           r.last_name,
           o.order_date,
           to_char(o.order_date, ''mm/dd/yy'') order_date_str,
           pm.delivery_type,
           decode(nvl(od.ship_method, ''SD''), ''SD'', ''F'', ''V'') florist_or_vendor,
           order_query_pkg.ORDER_HAS_ADDON_TYPE(od.order_detail_id, 7) vase_addon,
           order_query_pkg.ORDER_HAS_NO_ADDON_TYPE(od.order_detail_id, 7) non_vase_addon
      FROM clean.order_details od, clean.orders o, clean.customer r, ftd_apps.product_master pm
     WHERE od.order_guid = o.order_guid
       AND od.product_id = pm.product_id(+)
       AND od.recipient_id = r.customer_id(+)
       AND od.order_detail_id in (' || IN_ENTITY_IDS || ')';

  END GET_ORDER_INFO_BY_ORD_DET_IDS;

  PROCEDURE GET_DECISION_CONFIG_ID
  (
    IN_DECISION_TYPE_CODE 	IN FTD_APPS.DCSN_FX_DECISION_CONFIG.DECISION_TYPE_CODE%TYPE,
    IN_STATUS_CODE 		IN FTD_APPS.DCSN_FX_DECISION_CONFIG.STATUS_CODE%TYPE,
    OUT_DECISION_CONFIG_ID 	OUT FTD_APPS.DCSN_FX_DECISION_CONFIG.DECISION_CONFIG_ID%TYPE
  ) AS
  /*------------------------------------------------------------------------------
    Description:
       Retrieves the decision_config_id by decision_type_code and status_code.
       If multiple records of the same decision type code and status code exists,
       the procedure returns one last updated record that matches the criteria.

    Input:
            in_decision_type_code varchar2
            in_status_code varchar2

    Output:
            decision_config_id number
  ------------------------------------------------------------------------------*/
  BEGIN
      SELECT dc.decision_config_id
        INTO OUT_DECISION_CONFIG_ID
        FROM ftd_apps.dcsn_fx_decision_config dc,
        (SELECT dc2.decision_type_code, dc2.status_code, max(dc2.updated_on) updated_on
		FROM ftd_apps.dcsn_fx_decision_config dc2
	    GROUP BY dc2.decision_type_code, dc2.status_code
	     ) a
       WHERE upper(a.decision_type_code) = upper(IN_DECISION_TYPE_CODE) 
         AND upper(a.status_code) = upper(IN_STATUS_CODE)
         AND a.decision_type_code = dc.decision_type_code
         AND a.status_code = dc.status_code
         AND dc.updated_on = a.updated_on
         AND rownum  = 1;

       EXCEPTION WHEN NO_DATA_FOUND THEN NULL;


  END GET_DECISION_CONFIG_ID;


  PROCEDURE GET_DSCN_CONFIG_MAX_LEVEL
  (
    IN_DECISION_CONFIG_ID  	IN FTD_APPS.DCSN_FX_DECISION_CONFIG.DECISION_CONFIG_ID%TYPE,
    OUT_MAX_LEVEL 			OUT NUMBER
  )
  AS
  /*------------------------------------------------------------------------------
    Description:
       Retrieves the max level for the given decision config id.
       Root level starts at 1 (not 0). A decision config id that does not exist
       will result in null value being returned.

    Input:
            in_decision_config_id	number

    Output:
            out_level			ref_cursor
  ------------------------------------------------------------------------------*/
  BEGIN

      SELECT MAX(LEVEL)
        INTO OUT_MAX_LEVEL
        FROM ftd_apps.dcsn_fx_config_value
  START WITH parent_value_id IS NULL
         AND decision_config_id = IN_DECISION_CONFIG_ID
  CONNECT BY parent_value_id = PRIOR value_id
         AND decision_config_id = IN_DECISION_CONFIG_ID;

  END GET_DSCN_CONFIG_MAX_LEVEL;

  PROCEDURE GET_RESULT_BY_ID
  (
  IN_RESULT_ID			IN DCSN_FX_RESULTS.RESULT_ID%TYPE,
  OUT_RESULT			OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for retrieving details of the DCSN_FX_RESULTS record
          for the given result_id.

  Input:
          in_result_id                  number


  Output:
          refcursor

  -----------------------------------------------------------------------------*/
  BEGIN
    OPEN OUT_RESULT FOR
    SELECT *
      FROM dcsn_fx_results
     WHERE result_id = IN_RESULT_ID;

  END GET_RESULT_BY_ID;


  PROCEDURE GET_DCSN_NODES_RULES_BY_LEVEL
  (
    IN_DECISION_CONFIG_ID 	IN FTD_APPS.DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    IN_LEVEL			IN NUMBER,
    IN_PARENT_VALUE_ID		IN FTD_APPS.DCSN_FX_CONFIG_VALUE.PARENT_VALUE_ID%TYPE,
    OUT_NODES 	                OUT TYPES.REF_CURSOR,
    OUT_RULES	 	        OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for retrieving details of the nodes
          and rules for the given decision_config_id.

  Input:
          IN_DECISION_CONFIG_ID                  number
          IN_LEVEL				 number
          IN_PARENT_VALUE_ID			 number

  Output:
          out_nodes				ref_cursor
          out_rules				ref_cursor

  -----------------------------------------------------------------------------*/
  BEGIN
    OPEN OUT_NODES FOR
      SELECT val.value_id, val.decision_config_id, val.name, val.parent_value_id, val.group_order, val.script_text,level, val.is_active_flag,
      (select count(*) from FTD_APPS.DCSN_FX_CONFIG_VALUE
        where parent_value_id = val.value_id 
        and decision_config_id = IN_DECISION_CONFIG_ID
        and is_active_flag = 'Y')row_count
        FROM FTD_APPS.DCSN_FX_CONFIG_VALUE val
       WHERE 1 = 1
         AND level = IN_LEVEL
         AND val.parent_value_id = IN_PARENT_VALUE_ID
         AND val.is_active_flag = 'Y'
	START WITH val.parent_value_id is null
         AND val.decision_config_id = IN_DECISION_CONFIG_ID
	CONNECT BY val.parent_value_id = prior val.value_id
         AND val.decision_config_id = IN_DECISION_CONFIG_ID
    ORDER BY val.group_order;
	
	OPEN OUT_RULES FOR
      SELECT value_id, rule_code from ftd_apps.dcsn_fx_config_value_rules
       WHERE decision_config_id = IN_DECISION_CONFIG_ID;

	END GET_DCSN_NODES_RULES_BY_LEVEL;

  PROCEDURE GET_DCSN_NODE
  (
    IN_DECISION_CONFIG_ID IN FTD_APPS.DCSN_FX_CONFIG_VALUE.DECISION_CONFIG_ID%TYPE,
    IN_VALUE_ID			      IN FTD_APPS.DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE,
    OUT_NODE 	            OUT TYPES.REF_CURSOR,
    OUT_RULE	 	          OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for retrieving details of the node
          for the given decision_config_id.

  Input:
          in_result_id                  number


  Output:
          refcursor

  -----------------------------------------------------------------------------*/
  v_value_id			FTD_APPS.DCSN_FX_CONFIG_VALUE.VALUE_ID%TYPE := IN_VALUE_ID;
  
  BEGIN
  
    IF IN_VALUE_ID = 0 THEN
      OPEN OUT_NODE FOR
        SELECT value_id, decision_config_id, name, is_active_flag, parent_value_id, script_text, allow_comments_flag, group_order, level
        FROM FTD_APPS.DCSN_FX_CONFIG_VALUE
        WHERE parent_value_id IS NULL  
          START WITH parent_value_id IS NULL
        AND DECISION_CONFIG_ID = IN_DECISION_CONFIG_ID connect by parent_value_id = prior value_id  
        AND DECISION_CONFIG_ID = IN_DECISION_CONFIG_ID 
        ORDER BY level, group_order;
    ELSE 
      OPEN OUT_NODE FOR
        SELECT value_id, decision_config_id, name, is_active_flag, parent_value_id, script_text, allow_comments_flag, group_order, level 
        FROM FTD_APPS.DCSN_FX_CONFIG_VALUE
        WHERE value_id = IN_VALUE_ID
          START WITH parent_value_id IS NULL
        AND DECISION_CONFIG_ID = IN_DECISION_CONFIG_ID connect by parent_value_id = prior value_id  
        AND DECISION_CONFIG_ID = IN_DECISION_CONFIG_ID 
        ORDER BY level, group_order;
    END IF;
   
   OPEN OUT_RULE FOR
        SELECT value_id, rule_code from ftd_apps.dcsn_fx_config_value_rules
        WHERE decision_config_id = IN_DECISION_CONFIG_ID
        AND VALUE_ID = IN_VALUE_ID;
        
  END GET_DCSN_NODE;




  PROCEDURE INSERT_DECISION_RESULT
  (
    IN_DECISION_CONFIG_ID		IN DCSN_FX_RESULTS.DECISION_CONFIG_ID%TYPE,
    IN_STATUS				IN DCSN_FX_RESULTS.STATUS%TYPE,
    IN_CURRENT_LEVEL			IN DCSN_FX_RESULTS.CURRENT_LEVEL%TYPE,
    IN_USER_ID				IN DCSN_FX_RESULTS.CREATED_BY%TYPE,
    OUT_DCSN_FX_RESULT_ID 		OUT DCSN_FX_RESULTS.RESULT_ID%TYPE,
    OUT_STATUS 				OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for inserting into the dcsn_fx_results table.

  Input:
          IN_DECISION_CONFIG_ID		number
	  IN_USER_ID			varchar2
	  IN_STATUS			varchar2
          IN_CURRENT_LEVEL		number


  Output:
          OUT_DCSN_FX_RESULT_ID		number
          OUT_STATUS 			varchar2
          OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/

  BEGIN
	select DCSN_FX_RESULT_ID_SQ.nextval into OUT_DCSN_FX_RESULT_ID from dual;
  	OUT_STATUS := 'Y';

  	INSERT INTO DCSN_FX_RESULTS
  	(
  	    result_id,
  	    decision_config_id,
  	    status,
  	    current_level,
  	    created_by,
  	    created_on,
  	    updated_by,
  	    updated_on
  	)
  	VALUES
  	(
  	    OUT_DCSN_FX_RESULT_ID,
  	    IN_DECISION_CONFIG_ID,
  	    IN_STATUS,
  	    IN_CURRENT_LEVEL,
  	    IN_USER_ID,
  	    SYSDATE,
  	    IN_USER_ID,
  	    SYSDATE
  	);
    
    INSERT_ROOT_DCSN_RESULT_DETAIL(IN_DECISION_CONFIG_ID, OUT_DCSN_FX_RESULT_ID, IN_USER_ID, OUT_STATUS, OUT_MESSAGE);

        EXCEPTION
          WHEN OTHERS THEN
          BEGIN
            ROLLBACK;
            OUT_DCSN_FX_RESULT_ID := null;
            out_status := 'N';
            out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;

  END INSERT_DECISION_RESULT;


  PROCEDURE UPDATE_DECISION_RESULT
  (
    IN_RESULT_ID			IN DCSN_FX_RESULTS.RESULT_ID%TYPE,
    IN_STATUS				  IN DCSN_FX_RESULTS.STATUS%TYPE,
    IN_CURRENT_LEVEL	IN DCSN_FX_RESULTS.CURRENT_LEVEL%TYPE,
    IN_USER_ID				IN DCSN_FX_RESULTS.CREATED_BY%TYPE,
    OUT_STATUS 				OUT VARCHAR2,
    OUT_MESSAGE       OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for updating dcsn_fx_results table.

  Input:
          IN_RESULT_ID			number
	  IN_USER_ID			varchar2
	  IN_STATUS			varchar2
          IN_LEVEL			number


  Output:
          OUT_STATUS			varchar2
          OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/

  BEGIN
  	OUT_STATUS := 'Y';

  	UPDATE dcsn_fx_results
  	   SET status = DECODE(IN_STATUS, NULL, status, IN_STATUS),
  	       current_level = DECODE(IN_CURRENT_LEVEL, NULL, current_level, IN_CURRENT_LEVEL),
  	       updated_by = IN_USER_ID,
  	       updated_on = SYSDATE
  	 WHERE result_id = IN_RESULT_ID;


        EXCEPTION
          WHEN OTHERS THEN
          BEGIN
            ROLLBACK;
            OUT_STATUS := 'N';

            out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;

  END UPDATE_DECISION_RESULT;

  PROCEDURE SAVE_DCSN_RESULT_ENTITIES_BK
  (
    IN_RESULT_ID			IN DCSN_FX_RESULT_ENTITIES.RESULT_ID%TYPE,
    IN_ENTITY_TYPE_NAME_VALUE		IN VARCHAR2,
    IN_USER_ID				IN DCSN_FX_RESULT_ENTITIES.CREATED_BY%TYPE,
    OUT_STATUS 				OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for inserting entities for the session.
          The in_entity_type_name_value contains comma-delimited list of colon-delimited
          entity name value pairs. Nothing gets saved in case of an error.

  Input:
          IN_RESULT_ID			number
          IN_ENTITY_TYPE_NAME_VALUE	varchar2
	  IN_USER_ID			varchar2


  Output:
          OUT_STATUS			varchar2
          OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/
  v_str_nameValuePairs        	dbms_sql.varchar2s;
  v_name			dcsn_fx_result_entities.call_entity_name%TYPE;
  v_value			dcsn_fx_result_entities.call_entity_value%TYPE;

  BEGIN

        OUT_STATUS := 'Y';
        -- Delete result entities if exists.
        DELETE FROM dcsn_fx_result_entities
        WHERE result_id = IN_RESULT_ID;

        v_str_nameValuePairs := ftd_apps.dcsn_fx_maint_pkg.tokenizer(IN_ENTITY_TYPE_NAME_VALUE, ',');
        FOR i IN v_str_nameValuePairs.FIRST..v_str_nameValuePairs.LAST LOOP
            v_name := substr(v_str_nameValuePairs(i), 1, (instr(v_str_nameValuePairs(i), ':') - 1));
            v_value := substr(v_str_nameValuePairs(i), (instr(v_str_nameValuePairs(i), ':') + 1));

            INSERT INTO DCSN_FX_RESULT_ENTITIES
            (
	    	result_entity_id,
	    	result_id,
	    	call_entity_name,
	    	call_entity_value,
	    	created_by,
	    	created_on,
	    	updated_by,
		updated_on
            )
            VALUES
            (
                DCSN_FX_RESULT_ENTITIES_ID_SQ.nextval,
                IN_RESULT_ID,
                v_name,
                v_value,
                IN_USER_ID,
                SYSDATE,
                IN_USER_ID,
                SYSDATE
            )
            ;

        END LOOP;

        EXCEPTION
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;

  END SAVE_DCSN_RESULT_ENTITIES_BK;

  PROCEDURE SAVE_DECISION_RESULT_ENTITIES
  (
    IN_RESULT_ID			IN DCSN_FX_RESULT_ENTITIES.RESULT_ID%TYPE,
    IN_ENTITY_IDS			IN VARCHAR2,
    IN_ENTITY_TYPE_NAME			IN VARCHAR2,
    IN_USER_ID				IN DCSN_FX_RESULT_ENTITIES.CREATED_BY%TYPE,
    OUT_STATUS 				OUT VARCHAR2,
    OUT_MESSAGE           		OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for inserting entities for the session.
          The IN_ENTITY_IDS contains comma-delimited list of order detail ids
          or customer ids. IN_ENTITY_TYPE_NAME contains the type definition of IN_ENTITY_IDS.
          Nothing gets saved in case of an error.

  Input:
          IN_RESULT_ID			number
          IN_ENTITY_IDS			varchar2
          IN_ENTITY_TYPE_NAME		varchar2
	  IN_USER_ID			varchar2


  Output:
          OUT_STATUS			varchar2
          OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/
  v_str_entityIds        	dbms_sql.varchar2s;
  v_call_entity_value		dcsn_fx_result_entities.call_entity_value%TYPE;

  BEGIN
      OUT_STATUS := 'Y';

      -- Delete result entities if exists.
      DELETE FROM dcsn_fx_result_entities
       WHERE result_id = IN_RESULT_ID;

      v_str_entityIds := ftd_apps.dcsn_fx_maint_pkg.tokenizer(IN_ENTITY_IDS, ',');
      FOR i IN v_str_entityIds.FIRST..v_str_entityIds.LAST LOOP
          v_call_entity_value := v_str_entityIds(i);

            INSERT INTO DCSN_FX_RESULT_ENTITIES
            (
	    	result_entity_id,
	    	result_id,
	    	call_entity_name,
	    	call_entity_value,
	    	created_by,
	    	created_on,
	    	updated_by,
		updated_on
            )
            VALUES
            (
                DCSN_FX_RESULT_ENTITIES_ID_SQ.nextval,
                IN_RESULT_ID,
                IN_ENTITY_TYPE_NAME,
                v_call_entity_value,
                IN_USER_ID,
                SYSDATE,
                IN_USER_ID,
                SYSDATE
            )
            ;

      END LOOP;

        EXCEPTION
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;

  END SAVE_DECISION_RESULT_ENTITIES;

  PROCEDURE GET_DECISION_RESULT_ENTITIES
  (
    IN_RESULT_ID			IN DCSN_FX_RESULT_ENTITIES.RESULT_ID%TYPE,
    OUT_ENTITIES           	 	OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for retrieving a decision result entity
          for the given result id.
  Input:
          IN_RESULT_ID			number


  Output:
          OUT_ENTITIES			refcursor

  -----------------------------------------------------------------------------*/
  BEGIN

      OPEN OUT_ENTITIES FOR
      SELECT result_entity_id, call_entity_name, call_entity_value
      FROM DCSN_FX_RESULT_ENTITIES
      WHERE RESULT_ID=IN_RESULT_ID;

  END GET_DECISION_RESULT_ENTITIES;

PROCEDURE INSERT_ROOT_DCSN_RESULT_DETAIL
  (
    IN_DECISION_CONFIG_ID	IN DCSN_FX_RESULTS.DECISION_CONFIG_ID%TYPE,
	IN_RESULT_ID			IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    IN_USER_ID				IN DCSN_FX_RESULT_DETAILS.CREATED_BY%TYPE,
    OUT_STATUS 				OUT VARCHAR2,
    OUT_MESSAGE       		OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for inserting the root node into decision result details.
    Input:
            IN_RESULT_ID		number
            IN_USER_ID      varchar2
    Output:
            OUT_STATUS			varchar2
            OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/
  
  BEGIN
      OUT_STATUS := 'Y';
      
      INSERT INTO DCSN_FX_RESULT_DETAILS
        ( result_detail_id,result_id,value_id,name,parent_value_id,
          script_text,comment_text,created_by,created_on,updated_by,updated_on)
      (SELECT  DCSN_FX_RESULT_DETAILS_ID_SQ.nextval as result_detail_id,IN_RESULT_ID as result_id,value_id, name, parent_value_id, 
          script_text, null as comment_text, IN_USER_ID as created_by,sysdate, IN_USER_ID as updated_by,sysdate
        FROM FTD_APPS.DCSN_FX_CONFIG_VALUE
        WHERE decision_config_id = IN_DECISION_CONFIG_ID 
        AND parent_value_id is null
        );
	    
      EXCEPTION
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;

  END INSERT_ROOT_DCSN_RESULT_DETAIL
  ;
  
  PROCEDURE SAVE_DECISION_RESULT_DETAIL
  (
    IN_RESULT_ID			    IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    IN_VALUE_ID				    IN DCSN_FX_RESULT_DETAILS.VALUE_ID%TYPE,
    IN_NAME				        IN DCSN_FX_RESULT_DETAILS.NAME%TYPE,
    IN_PARENT_VALUE_ID	  IN DCSN_FX_RESULT_DETAILS.PARENT_VALUE_ID%TYPE,
    IN_COMMENT_TEXT			  IN DCSN_FX_RESULT_DETAILS.COMMENT_TEXT%TYPE,
    IN_SCRIPT_TEXT			  IN DCSN_FX_RESULT_DETAILS.SCRIPT_TEXT%TYPE,
    IN_USER_ID				    IN DCSN_FX_RESULT_DETAILS.CREATED_BY%TYPE,
    OUT_RESULT_DETAIL_ID  OUT DCSN_FX_RESULT_DETAILS.RESULT_DETAIL_ID%TYPE,
    OUT_STATUS 				    OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for inserting the decision result details.


    Input:
            IN_RESULT_ID		number
            IN_VALUE_ID			number
            IN_NAME			varchar2
            IN_PARENT_VALUE_ID		number
            IN_SCRIPT_TEXT		varchar2
            IN_COMMENT			varchar2
  	    IN_USER_ID			varchar2


    Output:
            OUT_STATUS			varchar2
            OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/
  v_rec_count NUMBER := 0;

  BEGIN
      OUT_STATUS := 'Y';

      SELECT count(*)
        INTO v_rec_count
        FROM dcsn_fx_result_details
       WHERE result_id = IN_RESULT_ID
         AND parent_value_id = IN_PARENT_VALUE_ID;


      IF v_rec_count > 0 THEN
	      UPDATE dcsn_fx_result_details
		 SET value_id = IN_VALUE_ID,
		     name = IN_NAME,
		     comment_text = IN_COMMENT_TEXT,
		     script_text = IN_SCRIPT_TEXT,
		     updated_by = IN_USER_ID,
		     updated_on = SYSDATE
	       WHERE result_id = IN_RESULT_ID
	         AND parent_value_id = IN_PARENT_VALUE_ID;

	       SELECT result_detail_id
	         INTO OUT_RESULT_DETAIL_ID
	         FROM DCSN_FX_RESULT_DETAILS
	        WHERE result_id = IN_RESULT_ID
	          AND parent_value_id = IN_PARENT_VALUE_ID;
      ELSE
	      select DCSN_FX_RESULT_DETAILS_ID_SQ.nextval into OUT_RESULT_DETAIL_ID from dual;
	      INSERT INTO DCSN_FX_RESULT_DETAILS
	      (
		  result_detail_id,
		  result_id,
		  value_id,
		  parent_value_id,
		  name,
		  comment_text,
		  script_text,
		  created_by,
		  created_on,
		  updated_by,
		  updated_on
	       )
	       VALUES
	       (
		  OUT_RESULT_DETAIL_ID,
		  IN_RESULT_ID,
		  IN_VALUE_ID,
		  IN_PARENT_VALUE_ID,
		  IN_NAME,
		  IN_COMMENT_TEXT,
		  IN_SCRIPT_TEXT,
		  IN_USER_ID,
		  SYSDATE,
		  IN_USER_ID,
		  SYSDATE
	       )
	       ;

      END IF;

      EXCEPTION
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;

  END SAVE_DECISION_RESULT_DETAIL
  ;

PROCEDURE GET_RESULT_DETAIL_BY_VALUE_ID
  (
    IN_RESULT_ID			IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    IN_VALUE_ID			  IN DCSN_FX_RESULT_DETAILS.VALUE_ID%TYPE,
    IN_NODE_TYPE      IN VARCHAR2,
    OUT_DETAIL				OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for retrieving decision result detail
            for the given result_id and value_id.

    Input:
            IN_RESULT_ID		number
			IN_VALUE_ID			number

    Output:
            OUT_DETAIL			ref_cursor

  -----------------------------------------------------------------------------*/

  BEGIN
    
    IF IN_NODE_TYPE IS NOT NULL THEN
      IF IN_NODE_TYPE = 'VALUE_ID' THEN
        OPEN OUT_DETAIL FOR
          SELECT d.value_id, d.name, d.parent_value_id, level, d.comment_text, d.script_text
          FROM dcsn_fx_result_details d
          WHERE d.result_id = IN_RESULT_ID AND VALUE_ID = IN_VALUE_ID 
            start with d.parent_value_id is null and d.result_id = IN_RESULT_ID
            CONNECT BY d.parent_value_id = prior d.value_id
            AND d.result_id = IN_RESULT_ID order by level;
      ELSIF IN_NODE_TYPE = 'PARENT_VALUE_ID' THEN
        OPEN OUT_DETAIL FOR
          SELECT d.value_id, d.name, d.parent_value_id, level, d.comment_text, d.script_text
          FROM dcsn_fx_result_details d
          WHERE d.result_id = IN_RESULT_ID AND PARENT_VALUE_ID = IN_VALUE_ID 
            start with d.parent_value_id is null and d.result_id = IN_RESULT_ID
            CONNECT BY d.parent_value_id = prior d.value_id
            AND d.result_id = IN_RESULT_ID order by level;
      END IF;
    END IF;
    
  	
  	 
  END GET_RESULT_DETAIL_BY_VALUE_ID;
  
  PROCEDURE GET_DECISION_RESULT_DETAILS
  (
    IN_RESULT_ID			IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    OUT_DETAILS				OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for retrieving decision result details
            for the given result_id.


    Input:
            IN_RESULT_ID		number


    Output:
            OUT_DETAILS			ref_cursor

  -----------------------------------------------------------------------------*/

  BEGIN
  	OPEN OUT_DETAILS FOR
  	SELECT d.value_id, d.name, d.parent_value_id, level, d.comment_text, d.script_text
  	  FROM dcsn_fx_result_details d
      WHERE d.result_id = IN_RESULT_ID
start with d.parent_value_id is null and d.result_id = IN_RESULT_ID
  CONNECT BY d.parent_value_id = prior d.value_id
         AND d.result_id = IN_RESULT_ID order by level;
  	 
  END GET_DECISION_RESULT_DETAILS;

  PROCEDURE SAVE_RESULT_ENTITY_COMMENTS
  (
    IN_RESULT_ID			    IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    IN_DNIS_ID				    IN COMMENTS.DNIS_ID%TYPE,
    IN_DECISION_TYPE_CODE	IN VARCHAR2,
    IN_USER_ID				    IN COMMENTS.CREATED_BY%TYPE,
    OUT_STATUS 				    OUT VARCHAR2,
    OUT_MESSAGE           OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This procedure is responsible for creating summary of decision results
          as order comments. It compiles all details for the result id into
          comment text and insert it for each entity.

          IN_ENTITY_IDS is a comma-delimited list of DCSN_FX_RESULT_ENTITIES.ERESULT_NTITY_ID.

          Can only handle entity of type CLEAN.CUSTOMER.CUSTOMER_ID
          AND CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID.

  Input:
          IN_RESULT_ID			number
          IN_ENTITY_IDS			varchar2
	  IN_USER_ID			varchar2


  Output:
          OUT_STATUS			varchar2
          OUT_MESSAGE			varchar2

  -----------------------------------------------------------------------------*/
  v_str_entitiyIds        	dbms_sql.varchar2s;
  v_entity_id			dcsn_fx_result_entities.result_entity_id%TYPE;
  V_ENTITY_TYPE_CUSTOMER_ID	VARCHAR2(50) := 'CLEAN.CUSTOMER.CUSTOMER_ID';
  V_ENTITY_TYPE_ORDER_DETAIL_ID	VARCHAR2(50) := 'CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID';
  V_COMMENT_TYPE_CUSTOMER VARCHAR2(20) := 'Customer';
  V_COMMENT_TYPE_ORDER VARCHAR2(20) := 'Order';
  V_DECISION_TYPE_CODE VARCHAR2(100);
  
  v_comment_type comments.comment_type%TYPE;
  v_order_detail_id comments.order_detail_id%TYPE;
  v_order_guid comments.order_guid%TYPE;
  v_customer_id comments.customer_id%TYPE;
  v_comment_text comments.comment_text%TYPE;
  v_comment_text_base comments.comment_text%TYPE;
  v_exception EXCEPTION;
  v_exception_text VARCHAR2(400);


  CURSOR entity_cur (IN_RESULT_ID dcsn_fx_result_details.result_id%TYPE) IS
  SELECT result_entity_id, call_entity_name, call_entity_value
    FROM dcsn_fx_result_entities
   WHERE result_id = IN_RESULT_ID;

  CURSOR order_cur (IN_ORDER_DETAIL_ID comments.order_detail_id%TYPE) IS
  SELECT od.order_detail_id, od.order_guid, c.customer_id
    FROM order_details od, orders o, customer c
   WHERE order_detail_id = IN_ORDER_DETAIL_ID
     AND od.order_guid = o.order_guid
     AND o.customer_id = c.customer_id;

  CURSOR dcsn_detail_cur (IN_RESULT_ID dcsn_fx_result_details.result_id%TYPE) IS
  SELECT a.v_level, a.script_text, b.name, b.comment_text
    FROM
	  (SELECT LEVEL as v_level, d.script_text, d.name, d.comment_text, d.value_id, d.parent_value_id
	    FROM clean.dcsn_fx_result_details d
	   WHERE d.result_id = IN_RESULT_ID
	   START WITH d.parent_value_id IS NULL
	     AND d.result_id = IN_RESULT_ID
	 CONNECT BY parent_value_id = PRIOR value_id
	     AND d.result_id = IN_RESULT_ID
	   ORDER BY LEVEL ASC
	  ) a,
	  (SELECT LEVEL as v_level, d.script_text, d.name, d.comment_text, d.value_id, d.parent_value_id
	    FROM clean.dcsn_fx_result_details d
	   WHERE d.result_id = IN_RESULT_ID
	   START WITH d.parent_value_id IS NULL
	     AND d.result_id = IN_RESULT_ID
	 CONNECT BY parent_value_id = PRIOR value_id
	     AND d.result_id = IN_RESULT_ID
	   ORDER BY LEVEL ASC
	  ) b
   WHERE a.value_id = b.parent_value_id
   ORDER BY a.v_level
	  ;

  --v_dcsn_detail_rec dcsn_detail_cur%ROWTYPE;


  BEGIN

        OUT_STATUS := 'Y';
        
        IF IN_DECISION_TYPE_CODE IS NULL THEN
          V_DECISION_TYPE_CODE := 'Call Disposition';
        ELSIF IN_DECISION_TYPE_CODE IS NOT NULL AND IN_DECISION_TYPE_CODE = 'calldisposition' THEN
          V_DECISION_TYPE_CODE := 'Call Disposition';
        END IF;
        
        -- Create comment text.
        v_comment_text_base := V_DECISION_TYPE_CODE || ' by ' || in_user_id || ' on ' || to_char(sysdate, 'mm/dd/yyyy HH:MI AM') || '<BR>';
        v_comment_text := v_comment_text_base;

       	FOR x IN dcsn_detail_cur(in_result_id) LOOP
	    v_comment_text := v_comment_text || x.v_level || '. ' || x.script_text || ' ' || x.name || ' ' || x.comment_text || '<BR>';
	END LOOP;

	-- Add comments for each entity.
        FOR entity_rec IN entity_cur(in_result_id) LOOP
            v_entity_id := entity_rec.result_entity_id;

	    IF entity_rec.call_entity_name = V_ENTITY_TYPE_CUSTOMER_ID THEN
	        v_comment_type := V_COMMENT_TYPE_CUSTOMER;
	        v_customer_id := entity_rec.call_entity_value;

	    ELSIF entity_rec.call_entity_name = V_ENTITY_TYPE_ORDER_DETAIL_ID THEN
	        v_comment_type := V_COMMENT_TYPE_ORDER;

	        OPEN order_cur(entity_rec.call_entity_value);
	        FETCH order_cur INTO v_order_detail_id, v_order_guid, v_customer_id;
	        CLOSE order_cur;

	    ELSE
	        v_exception_text := 'Unknown entity type: ' || entity_rec.call_entity_name || ' for entity id: ' || v_entity_id;
	        raise v_exception;
	    END IF;

            COMMENT_HISTORY_PKG.INSERT_COMMENTS
            (
		v_customer_id,
		v_order_guid,
		v_order_detail_id,
		V_DECISION_TYPE_CODE,
		NULL,
		IN_DNIS_ID,
		v_comment_text,
		v_comment_type,
		in_user_id,
		OUT_STATUS,
		OUT_MESSAGE
            )
            ;


            IF out_status <> 'Y' THEN
            	v_exception_text := 'Failed to insert comments for entity id ' || v_entity_id || '. Reason: ' || out_message;
            	raise v_exception;
            END IF;

            v_customer_id := null;
	    v_order_guid := null;
	    v_order_detail_id := null;

        END LOOP;

        EXCEPTION
          WHEN v_exception THEN
              ROLLBACK;
	      OUT_STATUS := 'N';

              out_message := v_exception_text;
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
          END;


  END SAVE_RESULT_ENTITY_COMMENTS;

  PROCEDURE DELETE_DECISION_RESULT_DETAIL
  (
    IN_RESULT_ID			IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    IN_VALUE_ID				IN DCSN_FX_RESULT_DETAILS.VALUE_ID%TYPE,
    OUT_STATUS 				OUT VARCHAR2,
    OUT_MESSAGE       OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for deleting decision result details
            for the given result_id and value_id


    Input:
            IN_RESULT_ID		number
            IN_VALUE_ID			number


    Output:
          OUT_STATUS			varchar2
          OUT_MESSAGE			varchar2


  -----------------------------------------------------------------------------*/

  BEGIN
     OUT_STATUS := 'Y';

      DELETE FROM dcsn_fx_result_details
      WHERE result_id = IN_RESULT_ID
        AND value_id = IN_VALUE_ID;

     EXCEPTION
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;

  END DELETE_DECISION_RESULT_DETAIL;

PROCEDURE DELETE_PREVIOUS_RESULT_DETAILS
  (
    IN_RESULT_ID			  IN DCSN_FX_RESULT_DETAILS.RESULT_ID%TYPE,
    IN_PARENT_VALUE_ID	IN DCSN_FX_RESULT_DETAILS.VALUE_ID%TYPE,
    OUT_STATUS 				  OUT VARCHAR2,
    OUT_MESSAGE         OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
    Description:
            This procedure is responsible for deleting decision result details
            for the given result_id and value_id


    Input:
            IN_RESULT_ID		number
            IN_VALUE_ID			number


    Output:
          OUT_STATUS			varchar2
          OUT_MESSAGE			varchar2


  -----------------------------------------------------------------------------*/

  BEGIN
     OUT_STATUS := 'Y';

     DELETE FROM CLEAN.DCSN_FX_RESULT_DETAILS 
     WHERE VALUE_ID in (
        SELECT val.value_id
        FROM CLEAN.DCSN_FX_RESULT_DETAILS val
        WHERE 1 = 1
          START WITH val.parent_value_id = IN_PARENT_VALUE_ID
                 AND val.result_id = IN_RESULT_ID
          CONNECT BY val.parent_value_id = prior val.value_id
                 AND val.result_id = IN_RESULT_ID
      ) AND result_id = IN_RESULT_ID;

     EXCEPTION
          WHEN OTHERS THEN
          BEGIN
              ROLLBACK;
              OUT_STATUS := 'N';

              out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
     END;

  END DELETE_PREVIOUS_RESULT_DETAILS;

END;
/
