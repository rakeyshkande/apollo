CREATE OR REPLACE PACKAGE BODY CLEAN.ACCOUNT_ORDER_FEED_PKG
AS

PROCEDURE UPDATE_ORDER_FEED_ATTEMPT
(
IN_ORDER_GUID                   IN ACCOUNT_ORDER_FEED.ORDER_GUID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Update the account_order_feed table to reflect that a feed for the
        order is going to be attempted.  If a record for the order_guid 
        does not already exist, a new record will be inserted.
        
        The account_order_feed table is primarily used to prevent duplicate
        order feeds (for an order) from being sent to CAMS, but is also used to 
        track which orders have been sent (and when) for reference purposes.

Input:
        order_guid                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

   OUT_STATUS := 'N';
         BEGIN
             SELECT 'Y' into OUT_STATUS FROM clean.orders o WHERE o.order_guid =  in_order_guid;
             EXCEPTION WHEN NO_DATA_FOUND THEN
             OUT_STATUS := 'N';
         END;
         IF OUT_STATUS <> 'N' THEN 
           BEGIN
            INSERT INTO clean.account_order_feed
            (order_guid,last_feed_attempt_date,feed_status_flag)
            VALUES
            (in_order_guid,sysdate,'P');
            OUT_STATUS := 'Y';            
          EXCEPTION                 
            WHEN DUP_VAL_ON_INDEX THEN
            BEGIN
                UPDATE  clean.account_order_feed
                SET     feed_status_flag = 'P',
                        last_feed_attempt_date = sysdate
                WHERE   order_guid = in_order_guid;
                OUT_STATUS := 'Y';
            END;                
            WHEN OTHERS THEN
                   OUT_STATUS := 'N';
                   out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
            END;
         END IF;
END UPDATE_ORDER_FEED_ATTEMPT;


PROCEDURE UPDATE_ORDER_FEED_FAILED
(
IN_ORDER_GUID                   IN ACCOUNT_ORDER_FEED.ORDER_GUID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Update the account_order_feed table to reflect that the feed for the
        order failed.  
        
        The account_order_feed table is primarily used to prevent duplicate
        order feeds (for an order) from being sent to CAMS, but is also used to 
        track which orders have been sent (and when) for reference purposes.

Input:
        order_guid                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN
    out_status := 'Y';
    
    -- Setting feed_status_flag to 'F' indicates the feed failed
    --
    UPDATE  clean.account_order_feed
    SET     feed_status_flag = 'F'
    WHERE   order_guid = in_order_guid;

    EXCEPTION
        WHEN OTHERS THEN
           out_status := 'N';
           out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_FEED_FAILED;


PROCEDURE UPDATE_ORDER_FEED_SUCCESS
(
IN_ORDER_GUID                   IN ACCOUNT_ORDER_FEED.ORDER_GUID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Update the account_order_feed table to reflect that the feed for the
        order succeeded.  
        
        The account_order_feed table is primarily used to prevent duplicate
        order feeds (for an order) from being sent to CAMS, but is also used to 
        track which orders have been sent (and when) for reference purposes.

Input:
        order_guid                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN
    out_status := 'Y';
    
    -- Setting feed_status_flag to 'S' indicates the feed succeeded
    --
    UPDATE  clean.account_order_feed
    SET     feed_status_flag = 'S',
            last_feed_date = sysdate
    WHERE   order_guid = in_order_guid;

    EXCEPTION
        WHEN OTHERS THEN
           out_status := 'N';
           out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_FEED_SUCCESS;


PROCEDURE IS_ORDER_FEED_NEEDED
(
IN_ORDER_KEY                    IN VARCHAR2,
IN_KEY_TYPE                     IN VARCHAR2,
IN_ENQUEUE_TIME                 IN DATE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2,
OUT_ORDER_GUID                  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        Determine if an order feed is necessary by checking if the time the order
        was originally enqueued is after the last feed attempt timestamp.
        
        The account_order_feed table is primarily used to prevent duplicate
        order feeds (for an order) from being sent to CAMS, but is also used to 
        track which orders have been sent (and when) for reference purposes.

        Note we check last "attempt" timestamp instead of successful feed timestamp
        to further minimize number of duplicate feeds especially if any feed errors occur.

Input:
        order_key          varchar2  (key for order)
        key_type           varchar2  (indicates if key is order_guid (OG) or order_detail_id (OD))
        enqueue_time       date      (timestamp order was originally enqueued)
        
Output:
        status              varchar2 (Y if feed is necessary, N otherwise, or E if error)
        message             varchar2 (error message - if any)
        order_guid          varchar2 (order_guid)

-----------------------------------------------------------------------------*/

-- Cursor will return N if order is in table and reflects that last
-- successful feed time is after the enqueue time
--
CURSOR feed_cur IS
    SELECT 'N'
    FROM   clean.account_order_feed aof
    WHERE  aof.order_guid = out_order_guid
    AND    aof.last_feed_attempt_date > in_enqueue_time;

BEGIN
    out_status := 'Y';               -- Assume feed is necessary
    out_message := null;
    out_order_guid := in_order_key;  -- Assume order_guid was passed in

    -- Get order_guid if order_detail_id was passed in
    --
    IF in_key_type = 'UPDATE_OD' THEN
        SELECT order_guid into out_order_guid
        FROM   clean.order_details
        WHERE  order_detail_id = in_order_key;
    END IF;

    -- Check if feed is necessary
    --
    OPEN feed_cur;
    FETCH feed_cur INTO out_status;
    CLOSE feed_cur;

    EXCEPTION
        -- If nothing found, then feed is necessary
        --    
        WHEN NO_DATA_FOUND THEN
           out_status := 'Y';
           
        WHEN OTHERS THEN
           out_status := 'E';
           out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END IS_ORDER_FEED_NEEDED;

END;
.
/

