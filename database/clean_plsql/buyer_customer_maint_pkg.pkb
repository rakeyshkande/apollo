CREATE OR REPLACE
PACKAGE BODY clean.BUYER_CUSTOMER_MAINT_PKG AS

PROCEDURE UPDATE_CUSTOMER_FROM_BUYER
(
IN_BUYER_ID                             IN SCRUB.BUYERS.BUYER_ID%TYPE,
IN_ORDER_GUID                           IN SCRUB.ORDERS.ORDER_GUID%TYPE,
IO_CUSTOMER_ID                          IN OUT CLEAN.CUSTOMER.CUSTOMER_ID%TYPE,
OUT_STATUS                              OUT VARCHAR2,
OUT_MESSAGE                             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting or updating customer
        information in CLEAN from SCRUB buyer.  If the customer id is passed
        in, the given customer will be updated with the given buyer.  If
        there is no customer id passed in a new customer record will be created
        and the customer id will be passed back to the calling program.

Input:
        buyer_id                        number
        order_guid                      varchar2
        customer_id                     number

Output:
        customer_id                     number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR buyer_cur IS
        select  b.first_name,
                b.last_name,
                ba.address_etc business_name,
                ba.address_line_1,
                ba.address_line_2,
                ba.city,
                ba.state_province,
                ba.postal_code,
                ba.county,
                ba.country,
                ba.address_type,
                to_date ((select min (info_data) from scrub.order_ext_birthdate_vw e
                                  join scrub.order_details od
                                  on e.external_order_number = od.external_order_number
                                  where od.order_guid = in_order_guid), 'mm/dd/yyyy') birthday
        from    scrub.buyers b
        join    scrub.buyer_addresses ba
        on      b.buyer_id = ba.buyer_id
        where   b.buyer_id = in_buyer_id;

CURSOR cust_first_order_cur IS
        SELECT  c.buyer_indicator,
                c.recipient_indicator
        FROM    customer c
        WHERE   c.customer_id = io_customer_id;

cust_first_order_rec    cust_first_order_cur%rowtype;

CURSOR buyer_order_cur IS
        select  o.order_origin,
                to_date (substr (o.order_date, 5, length (o.order_date) - 12) || substr (o.order_date, -5), 'mon dd hh24:mi:ss yyyy') order_date
        from    scrub.orders o
        where   o.buyer_id = in_buyer_id;

v_origin_id     customer.origin_id%type;
v_order_date    customer.first_order_date%type;

CURSOR buyer_phone_cur IS
        select  bp.phone_number,
                bp.extension,
                decode (bp.phone_type, 'HOME', 'Evening', 'Day') phone_type,
                bp.phone_number_type,
                bp.sms_opt_in
        from    scrub.buyer_phones bp
        where   bp.buyer_id = in_buyer_id
        and     bp.phone_number is not null;

v_phone_id      customer_phones.phone_id%type;

CURSOR buyer_email_cur IS
        select  be.email email_address,
                (select s.company_id from ftd_apps.source s where s.source_code = o.source_code) company_id
        from    scrub.buyer_emails be
        join    scrub.orders o
        on      o.buyer_email_id = be.buyer_email_id
        where   o.order_guid = in_order_guid
        and     be.email is not null;

v_email_id      email.email_id%type;

CURSOR membership_cur IS
        select  m.membership_id_number membership_number,
                (select pp.partner_name from ftd_apps.partner_program pp where pp.program_name = m.membership_type) membership_type,
                m.first_name,
                m.last_name
        from    scrub.memberships m
        join    scrub.orders o
        on      o.membership_id = m.membership_id
        where   o.order_guid = in_order_guid
        and     m.membership_type is not null
        and     m.membership_id_number is not null;

v_membership_id memberships.membership_id%type;
v_out_company_list varchar2(100);
v_out_cur		     types.ref_cursor;
v_prop_val		     varchar2(1) := 'N';
v_vip_customer_flag  varchar2(1);

BEGIN

-- update or insert the customer record from the buyer record
-- there should be only one record in the buyer cursor
IF io_customer_id IS NOT NULL THEN

        -- check if the existing customer was a recipient then buyer
        -- if so, get the first order date and origin for the customer record
        OPEN cust_first_order_cur;
        FETCH cust_first_order_cur INTO cust_first_order_rec;
        CLOSE cust_first_order_cur;

        IF cust_first_order_rec.buyer_indicator = 'N' and cust_first_order_rec.recipient_indicator = 'Y' THEN
                OPEN buyer_order_cur;
                FETCH buyer_order_cur INTO v_origin_id, v_order_date;
                CLOSE buyer_order_cur;
        END IF;
				
		BEGIN
		    SELECT vip_customer into v_vip_customer_flag 
		    from clean.customer 
		    where customer_id = io_customer_id;
		EXCEPTION WHEN OTHERS THEN
		    v_vip_customer_flag:= 'N';
		END;
		
        FOR b IN buyer_cur LOOP
                CLEAN.CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER
                (
                        IN_CUSTOMER_ID=>io_customer_id,
                        IN_FIRST_NAME=>b.first_name,
                        IN_LAST_NAME=>b.last_name,
                        IN_BUSINESS_NAME=>b.business_name,
                        IN_ADDRESS_1=>b.address_line_1,
                        IN_ADDRESS_2=>b.address_line_2,
                        IN_CITY=>b.city,
                        IN_STATE=>b.state_province,
                        IN_ZIP_CODE=>b.postal_code,
                        IN_COUNTRY=>b.country,
                        IN_ADDRESS_TYPE=>b.address_type,
                        IN_PREFERRED_CUSTOMER=>null,
                        IN_BUYER_INDICATOR=>'Y',
                        IN_RECIPIENT_INDICATOR=>null,
                        IN_ORIGIN_ID=>v_origin_id,
                        IN_FIRST_ORDER_DATE=>v_order_date,
                        IN_UPDATED_BY=>'SYS',
                        IN_COUNTY=>b.county,
                        IN_UPDATE_SCRUB=>'N',
                        IN_BUSINESS_INFO=>null,
                        IN_VIP_CUSTOMER => v_vip_customer_flag,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
                IF out_status = 'N' THEN
                        exit;
                END IF;
        END LOOP;
ELSE
        -- get the order origin and first order date for the customer
        OPEN buyer_order_cur;
        FETCH buyer_order_cur INTO v_origin_id, v_order_date;
        CLOSE buyer_order_cur;

        FOR b IN buyer_cur LOOP
                CLEAN.CUSTOMER_MAINT_PKG.INSERT_CUSTOMER
                (
                        IN_FIRST_NAME=>b.first_name,
                        IN_LAST_NAME=>b.last_name,
                        IN_BUSINESS_NAME=>b.business_name,
                        IN_ADDRESS_1=>b.address_line_1,
                        IN_ADDRESS_2=>b.address_line_2,
                        IN_CITY=>b.city,
                        IN_STATE=>b.state_province,
                        IN_ZIP_CODE=>b.postal_code,
                        IN_COUNTRY=>b.country,
                        IN_ADDRESS_TYPE=>b.address_type,
                        IN_PREFERRED_CUSTOMER=>null,
                        IN_BUYER_INDICATOR=>'Y',
                        IN_RECIPIENT_INDICATOR=>null,
                        IN_ORIGIN_ID=>v_origin_id,
                        IN_FIRST_ORDER_DATE=>v_order_date,
                        IN_CREATED_BY=>'SYS',
                        IN_COUNTY=>b.county,
                        IN_BUSINESS_INFO=>null,
                        OUT_CUSTOMER_ID=>io_customer_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
                IF out_status = 'N' THEN
                        exit;
                END IF;
        END LOOP;

        IF out_status = 'Y' THEN

            CLEAN.CUSTOMER_MAINT_PKG.INSERT_DIRECT_MAIL
            (
                IN_CUSTOMER_ID=>io_customer_id,
                IN_COMPANY_ID=>null,
                IN_CREATED_BY=>'SYS',
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
            );

        END IF;
END IF;


IF out_status = 'Y' THEN
        -- update customer phones
        FOR bp IN buyer_phone_cur LOOP
                CLEAN.CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER_PHONES
                (
                        IN_CUSTOMER_ID=>io_customer_id,
                        IN_PHONE_TYPE=>bp.phone_type,
                        IN_PHONE_NUMBER=>bp.phone_number,
                        IN_EXTENSION=>bp.extension,
                        IN_PHONE_NUMBER_TYPE=>bp.phone_number_type,
                        IN_SMS_OPT_IN=>bp.sms_opt_in,
                        IN_UPDATED_BY=>'SYS',
                        IN_UPDATE_SCRUB=>'N',
                        OUT_PHONE_ID=>v_phone_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END LOOP;
END IF;

IF out_status = 'Y' THEN
        -- update customer emails
        -- there should only be one record in the email cursor
        FOR be IN buyer_email_cur LOOP
                CLEAN.CUSTOMER_MAINT_PKG.UPDATE_UNIQUE_EMAIL
                (
                        IN_CUSTOMER_ID=>io_customer_id,
                        IN_COMPANY_ID=>be.company_id,
                        IN_EMAIL_ADDRESS=>be.email_address,
                        IN_ACTIVE_INDICATOR=>null,
                        IN_SUBSCRIBE_STATUS=>null,
                        IN_UPDATED_BY=>'SYS',
                        IN_MOST_RECENT_BY_COMPANY=>'Y',
                        IN_UPDATE_SCRUB=>'N',
                        IO_EMAIL_ID=>v_email_id,
			OUT_COMPANY_LIST=>v_out_company_list,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END LOOP;
END IF;

IF out_status = 'Y' THEN
        -- update memberships
        -- there should only be one record in the membership cursor
        FOR m IN membership_cur LOOP
                CLEAN.CUSTOMER_MAINT_PKG.UPDATE_UNIQUE_MEMBERSHIPS
                (
                        IN_CUSTOMER_ID=>io_customer_id,
                        IN_MEMBERSHIP_NUMBER=>m.membership_number,
                        IN_MEMBERSHIP_TYPE=>m.membership_type,
                        IN_FIRST_NAME=>m.first_name,
                        IN_LAST_NAME=>m.last_name,
                        IN_UPDATED_BY=>'SYS',
                        IN_MOST_RECENT_BY_TYPE=>'Y',
                        IN_UPDATE_SCRUB=>'N',
                        OUT_MEMBERSHIP_ID=>v_membership_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END LOOP;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CUSTOMER_FROM_BUYER;


PROCEDURE UPDATE_BUYER_FROM_CUSTOMER
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_BUSINESS_NAME                IN CUSTOMER.BUSINESS_NAME%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_ADDRESS_2                    IN CUSTOMER.ADDRESS_2%TYPE,
IN_CITY                         IN CUSTOMER.CITY%TYPE,
IN_STATE                        IN CUSTOMER.STATE%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_COUNTRY                      IN CUSTOMER.COUNTRY%TYPE,
IN_ADDRESS_TYPE                 IN CUSTOMER.ADDRESS_TYPE%TYPE,
IN_PREFERRED_CUSTOMER           IN CUSTOMER.PREFERRED_CUSTOMER%TYPE,
IN_COUNTY                       IN CUSTOMER.COUNTY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating scrub buyer information
        from the clean customer.

Input:
        customer_id                     number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)
-----------------------------------------------------------------------------*/

CURSOR buyer_cur IS
        select  b.buyer_id,
                b.auto_hold,
                b.best_customer,
                b.status
        from    scrub.buyers b
        where   b.clean_customer_id = in_customer_id
        order by b.buyer_id DESC;

v_buyer_id              scrub.buyers.buyer_id%type;
v_auto_hold             scrub.buyers.auto_hold%type;
v_best_customer         scrub.buyers.best_customer%type;
v_status                scrub.buyers.status%type;

CURSOR ba_cur (p_buyer_id integer) IS
        select  ba.buyer_address_id
        from    scrub.buyer_addresses ba
        where   ba.buyer_id = p_buyer_id;

v_buyer_address_id      scrub.buyer_addresses.buyer_address_id%type;

BEGIN

OUT_STATUS := 'Y';

OPEN buyer_cur;
FETCH buyer_cur INTO
                v_buyer_id,
                v_auto_hold,
                v_best_customer,
                v_status;
CLOSE buyer_cur;

IF v_buyer_id IS NOT NULL THEN
        SCRUB.BUYERS_MAINT_PKG.UPDATE_BUYERS
        (
                IN_BUYER_ID=>v_buyer_id,
                IN_CUSTOMER_ID=>in_customer_id,
                IN_LAST_NAME=>in_last_name,
                IN_FIRST_NAME=>in_first_name,
                IN_AUTO_HOLD=>v_auto_hold,
                IN_BEST_CUSTOMER=>v_best_customer,
                IN_STATUS=>v_status,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        IF out_status = 'Y' THEN
                OPEN ba_cur (v_buyer_id);
                FETCH ba_cur INTO v_buyer_address_id;
                CLOSE ba_cur;

                SCRUB.BUYERS_MAINT_PKG.UPDATE_BUYER_ADDRESSES
                (
                        IN_BUYER_ADDRESS_ID=>v_buyer_address_id,
                        IN_BUYER_ID=>v_buyer_id,
                        IN_ADDRESS_TYPE=>in_address_type,
                        IN_ADDRESS_LINE_1=>in_address_1,
                        IN_ADDRESS_LINE_2=>in_address_2,
                        IN_CITY=>in_city,
                        IN_STATE_PROVINCE=>in_state,
                        IN_POSTAL_CODE=>in_zip_code,
                        IN_COUNTRY=>in_country,
                        IN_COUNTY=>in_county,
                        IN_ADDRESS_ETC=>in_business_name,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END IF;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_BUYER_FROM_CUSTOMER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_BUYER_FROM_CUSTOMER;


PROCEDURE UPDATE_BUYER_PHONES_FROM_CUST
(
IN_CUSTOMER_ID                  IN CUSTOMER_PHONES.CUSTOMER_ID%TYPE,
IN_PHONE_TYPE                   IN CUSTOMER_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN CUSTOMER_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN CUSTOMER_PHONES.EXTENSION%TYPE,
IN_PHONE_NUMBER_TYPE            IN CUSTOMER_PHONES.PHONE_NUMBER_TYPE%TYPE,
IN_SMS_OPT_IN                   IN CUSTOMER_PHONES.SMS_OPT_IN%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the phone number of the
        given type for the customer in scrub

Input:
        customer_id                     number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR buyer_cur IS
        select  b.buyer_id
        from    scrub.buyers b
        where   b.clean_customer_id = in_customer_id
        order by b.buyer_id desc;

v_buyer_id                      integer;
v_buyer_phone_id                integer;
v_phone_type                    customer_phones.phone_type%type;

BEGIN

OPEN buyer_cur;
FETCH buyer_cur INTO v_buyer_id;
CLOSE buyer_cur;

CASE in_phone_type
        WHEN 'Day' THEN v_phone_type := 'WORK';
        WHEN 'Evening' THEN v_phone_type := 'HOME';
        ELSE v_phone_type := in_phone_type;
END CASE;

SCRUB.BUYERS_MAINT_PKG.UPDATE_UNIQUE_BUYER_PHONES
(
        IN_BUYER_ID=>v_buyer_id,
        IN_PHONE_TYPE=>v_phone_type,
        IN_PHONE_NUMBER=>in_phone_number,
        IN_EXTENSION=>in_extension,
        IN_PHONE_NUMBER_TYPE=>in_phone_number_type,
        IN_SMS_OPT_IN=>in_sms_opt_in,
        OUT_BUYER_PHONE_ID=>v_buyer_phone_id,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_BUYER_PHONES_FROM_CUST [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_BUYER_PHONES_FROM_CUST;


PROCEDURE UPDATE_MEMBERSHIPS_FROM_CUST
(
IN_CUSTOMER_ID                  IN MEMBERSHIPS.CUSTOMER_ID%TYPE,
IN_MEMBERSHIP_NUMBER            IN MEMBERSHIPS.MEMBERSHIP_NUMBER%TYPE,
IN_MEMBERSHIP_TYPE              IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_FIRST_NAME                   IN MEMBERSHIPS.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN MEMBERSHIPS.LAST_NAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the unique memebership
        for the customer in scrub

Input:
        customer_id                     number
        membership_number               varchar2
        membership_type                 varchar2
        first_name                      varchar2
        last_name                       varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR buyer_cur IS
        select  b.buyer_id
        from    scrub.buyers b
        where   b.clean_customer_id = in_customer_id
        order by b.buyer_id desc;

v_buyer_id                      integer;
v_membership_id                 integer;

BEGIN

OPEN buyer_cur;
FETCH buyer_cur INTO v_buyer_id;
CLOSE buyer_cur;

SCRUB.BUYERS_MAINT_PKG.UPDATE_UNIQUE_MEMBERSHIPS
(
        IN_BUYER_ID=>v_buyer_id,
        IN_MEMBERSHIP_TYPE=>in_membership_type,
        IN_LAST_NAME=>in_last_name,
        IN_FIRST_NAME=>in_first_name,
        IN_MEMBERSHIP_ID_NUMBER=>in_membership_number,
        OUT_MEMBERSHIP_ID=>v_membership_id,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);


EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UPDATE_MEMBERSHIPS_FROM_CUST [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_MEMBERSHIPS_FROM_CUST;

END BUYER_CUSTOMER_MAINT_PKG;
.
/
