CREATE OR REPLACE PACKAGE BODY CLEAN.ORDER_MESG_PKG AS

PROCEDURE GET_COMM_ITEM_IN_QUEUE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the queue and queue tag
        for the given order if it exists in the credit, zip or order
        queues

Input:
        order_detail_id                 number

Output:
        cursor containing credit, zip or order queued information

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  q.queue_type || ' Q' || decode (t.csr_id, null, null, ' - ' || t.csr_id) queue_item
        FROM    queue q
        LEFT OUTER JOIN queue_tag t
        ON      q.message_id = t.message_id
        WHERE   q.order_detail_id = in_order_detail_id
        AND     q.queue_type in ('CREDIT', 'ZIP', 'ORDER', 'LP', 'DC')
        ORDER BY decode (q.queue_type, 'CREDIT', 1, 'ZIP', 2, 'ORDER', 3, 'LP', 4, 'DC', 5);

END GET_COMM_ITEM_IN_QUEUE;


FUNCTION GET_MOUSEOVER_STRING
(
IN_STRING   IN  CLOB,
IN_MAX_LEN  IN  NUMBER
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        Returns a shortened string intended for mouseover displays on the frontend.
Input:
        in_string   String to be shortened
        in_max_len  Shorten string to this length
Output:
        Shortened string
-----------------------------------------------------------------------------*/
v_scrub_ind     varchar2 (1);
BEGIN
    IF (in_string is null) THEN
        return 'No message';
    ELSIF (length(in_string) > in_max_len) THEN
        return substr(in_string, 0, in_max_len) || '...';
    ELSE
        return in_string;
    END IF;
END GET_MOUSEOVER_STRING;


PROCEDURE GET_COMMUNICATION_MESSAGES
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_MAX_COMMENT_LEN_RETURNED     IN NUMBER,
IN_MAX_EMAIL_LEN_RETURNED       IN NUMBER,
OUT_MESSAGE_CUR                 OUT TYPES.REF_CURSOR,
OUT_NUMBER_OF_RECORDS           OUT NUMBER,
OUT_CUST_ORDER_CUR              OUT TYPES.REF_CURSOR,
OUT_LETTERS_EXIST               OUT VARCHAR2,
OUT_EMAIL_LETTER_CUR            OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages for the
        the given order.  
        Note the comment and email columns in the result set are shortened
        (based on input parms) since they are only intended for mouseover displays 
        on the front-end. 

Input:
        order_detail_id                 number
        max_comment_len_returned        max number of chars of comment column returned
        max_email_len_returned          max number of chars of email column returned
                                        (use 0 to disable)

Output:
        cursor containing communication screen information

-----------------------------------------------------------------------------*/

CURSOR count_cur IS
        SELECT  max (num_records)
        FROM
        (
                SELECT  count (1) num_records
                FROM
                (
                        SELECT  m.mercury_id
                        FROM    mercury.mercury m
                        WHERE   m.reference_number is null
                        AND     EXISTS
                        (
                                   SELECT  1
				                             	FROM    mercury.mercury m1, clean.order_details cod
				                                WHERE   m1.reference_number = to_char (in_order_detail_id)
				                                AND     m1.mercury_order_number = m.mercury_order_number
				                                 AND    to_char(cod.order_detail_id) = m1.reference_number
                                   AND     cod.created_on <   m.created_on
                        )
                        UNION
                        SELECT  m.mercury_id
                        FROM    mercury.mercury m
                        WHERE   m.reference_number = to_char (in_order_detail_id)
                        UNION
                        SELECT  v.venus_id
                        FROM    venus.venus v
                        WHERE   v.reference_number = to_char (in_order_detail_id)
                )
                UNION ALL
                SELECT  count (1) num_records
                FROM    point_of_contact p
                JOIN    order_details od
                ON      p.order_detail_id = od.order_detail_id
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                WHERE   p.order_detail_id = in_order_detail_id
        );

BEGIN

-- get the number of total messages for the order
OPEN count_cur;
FETCH count_cur INTO out_number_of_records;
CLOSE count_cur;

-- get the order and customer information for the order
ORDER_QUERY_PKG.GET_ORDER_CUSTOMER_INFO (in_order_detail_id, out_cust_order_cur);

-- check if the order has associated letters in the point of contact table
out_letters_exist := point_of_contact_pkg.order_has_letters (in_order_detail_id);

-- the cursor returns the mercury/venus message records requested by the start position and number of records returned
-- the mercury msg_type field is a concatenation of the following:
--      msg_type + manual status + comp order message + price message + inbound message + queued message
OPEN out_message_cur FOR
        SELECT  * FROM
        (
                SELECT  rownum row_number,
                        a.*
                FROM
                (
                        SELECT  m.mercury_id message_id,
                                m.mercury_order_number message_order_number,
                                m.mercury_status,
                                m.msg_type ||
                                        decode (m.mercury_status, 'MM', 'M', 'MN', 'M', null) ||
                                        m.comp_order ||
                                        m.ask_answer_code ||
                                        decode (m.message_direction, 'INBOUND', 'I', null) ||
                                        (select max (' Q - ' || t.csr_id) from queue q left outer join queue_tag t on q.message_id = t.message_id where q.mercury_id = m.mercury_id and q.queue_type <> 'DC') msg_type,
                                m.filling_florist,
                                m.order_date,
                                m.transmission_time,
                                to_char (m.created_on, 'mm/dd/yyyy') message_date, -- to_char (m.transmission_time, 'mm/dd/yyyy') message_date,
                                to_char (m.created_on, 'hh:miAM') message_time, -- to_char (m.transmission_time, 'hh:miAM') message_time,
                                m.price,
                                decode (m.mercury_status, 'MC', 'VER', 'MR', 'REJ', 'ME', 'ERR', 'MM', IS_MANUAL_FTD_VERIFIED (m.mercury_id), 'MN', IS_MANUAL_FTD_VERIFIED (m.mercury_id)) message_status,
                                'Mercury' group_indicator,
                                1 group_sort,
                                null subject,
                                decode (m.msg_type, 'FTD', 1, 9) msg_order,
                                m.created_on,
                                (select min (m2.created_on) from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'FTD') ftd_created_on,
                                m.mercury_message_number message_number,
                                m.message_direction,
                                m.sending_florist,
                                m.over_under_charge,
                                decode(IN_MAX_COMMENT_LEN_RETURNED, 0, '', get_mouseover_string(
                                        decode(m.msg_type, 'FTD', 
                                        'FLORIST: ' || m.filling_florist || ' RECIPIENT: ' || m.recipient || ' ' || m.address || ' ' || m.city_state_zip || ' DELIVERY_DATE: ' || m.delivery_date || 
                                        ' PRICE: ' || trim(to_char(m.price,'99,990.99')) || ' 1ST_CHOICE: ' || m.first_choice || ' ALLOWED_SUBSTITUTION: ' || m.second_choice, m.comments), 
                                    IN_MAX_COMMENT_LEN_RETURNED)) comments,
                               decode(m.msg_type, 'FTD',(SELECT  fm.florist_name  
                                                                  FROM    ftd_apps.florist_master fm
                                                                  WHERE   fm.florist_id = m.filling_florist),'') filler_name,
                                decode(m.msg_type, 'FTD',(SELECT  fm.phone_number 
                                                                  FROM    ftd_apps.florist_master fm
                                                                  WHERE   fm.florist_id = m.filling_florist),'') filler_phone,
                                (select losv.order_status_desc || ' ' ||
                                    to_char(ls.status_timestamp, 'mm/dd/yyyy hh:mi:ssAM')
                                    from mercury.lifecycle_status ls, mercury.lifecycle_order_status_val losv
                                    where ls.mercury_id = m.mercury_id
                                    and losv.order_status_code = ls.order_status_code
                                    and ls.status_timestamp =
                                        (select max(ls1.status_timestamp)
                                        from mercury.lifecycle_status ls1
                                        where ls1.mercury_id = m.mercury_id)
                                    and rownum = 1) lifecycle_status,
                                case
                                    when m.msg_type = 'FTD' and mercury_status in ('MC', 'MM', 'MN') then
                                        case (select count(*)
                                            from mercury.mercury m_notlive
                                            where m_notlive.mercury_order_number = m.mercury_order_number
                                            and m_notlive.msg_type in ('CAN','REJ'))
                                        when 0 then
                                            'Y'
                                        else
                                            'N'
                                        end
                                    else
                                        'N'
                                end live_ftd
                        FROM    mercury.mercury m
                        WHERE   m.reference_number is null
                        AND     m.created_on > (select created_on from order_details where order_detail_id = in_order_detail_id)
                        AND     EXISTS
                        (
                                SELECT  1
                             	FROM    mercury.mercury m1
                                WHERE   m1.reference_number = to_char (in_order_detail_id)
                                AND     m1.mercury_order_number = m.mercury_order_number
                        )
                        UNION
                        SELECT  m.mercury_id message_id,
                                m.mercury_order_number message_order_number,
                                m.mercury_status,
                                m.msg_type ||
                                        decode (m.mercury_status, 'MM', 'M', 'MN', 'M', null) ||
                                        m.comp_order ||
                                        m.ask_answer_code ||
                                        decode (m.message_direction, 'INBOUND', 'I', null) ||
                                        (select max (' Q - ' || t.csr_id) from queue q left outer join queue_tag t on q.message_id = t.message_id where q.mercury_id = m.mercury_id and q.queue_type <> 'DC') msg_type,
                                m.filling_florist,
                                m.order_date,
                                m.transmission_time,
                                to_char (m.created_on, 'mm/dd/yyyy') message_date, -- to_char (m.transmission_time, 'mm/dd/yyyy') message_date,
                                to_char (m.created_on, 'hh:miAM') message_time, -- to_char (m.transmission_time, 'hh:miAM') message_time,
                                m.price,
                                decode (m.mercury_status, 'MC', 'VER', 'MR', 'REJ', 'ME', 'ERR', 'MM', IS_MANUAL_FTD_VERIFIED (m.mercury_id), 'MN', IS_MANUAL_FTD_VERIFIED (m.mercury_id)) message_status,
                                'Mercury' group_indicator,
                                1 group_sort,
                                null subject,
                                decode (m.msg_type, 'FTD', 1, 9) msg_order,
                                m.created_on,
                                (select min (m2.created_on) from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'FTD') ftd_created_on,
                                m.mercury_message_number message_number,
                                m.message_direction,
                                m.sending_florist,
                                m.over_under_charge,
                                decode(IN_MAX_COMMENT_LEN_RETURNED, 0, '', get_mouseover_string(
                                        decode(m.msg_type, 'FTD', 
                                        'FLORIST: ' || m.filling_florist || ' RECIPIENT: ' || m.recipient || ' ' || m.address || ' ' || m.city_state_zip || ' DELIVERY_DATE: ' || m.delivery_date || 
                                        ' PRICE: ' || trim(to_char(m.price,'99,990.99')) || ' 1ST_CHOICE: ' || m.first_choice || ' ALLOWED_SUBSTITUTION: ' || m.second_choice, m.comments), 
                                    IN_MAX_COMMENT_LEN_RETURNED)) comments,
                                decode(m.msg_type, 'FTD',(SELECT  fm.florist_name  
                                                                  FROM    ftd_apps.florist_master fm
                                                                  WHERE   fm.florist_id = m.filling_florist),'') filler_name,
                                decode(m.msg_type, 'FTD',(SELECT  fm.phone_number 
                                                                  FROM    ftd_apps.florist_master fm
                                                                  WHERE   fm.florist_id = m.filling_florist),'') filler_phone,
                                (select losv.order_status_desc || ' ' ||
                                    to_char(ls.status_timestamp, 'mm/dd/yyyy hh:mi:ssAM')
                                    from mercury.lifecycle_status ls, mercury.lifecycle_order_status_val losv
                                    where ls.mercury_id = m.mercury_id
                                    and losv.order_status_code = ls.order_status_code
                                    and ls.status_timestamp =
                                        (select max(ls1.status_timestamp)
                                        from mercury.lifecycle_status ls1
                                        where ls1.mercury_id = m.mercury_id)
                                    and rownum = 1) lifecycle_status,
                                case
                                    when m.msg_type = 'FTD' and mercury_status in ('MC', 'MM', 'MN') then
                                        case (select count(*)
                                            from mercury.mercury m_notlive
                                            where m_notlive.mercury_order_number = m.mercury_order_number
                                            and m_notlive.msg_type in ('CAN','REJ'))
                                        when 0 then
                                            'Y'
                                        else
                                            'N'
                                        end
                                    else
                                        'N'
                                end live_ftd
                        FROM    mercury.mercury m
                        WHERE   m.reference_number = to_char (in_order_detail_id)
                        UNION
                        SELECT  v.venus_id,
                                v.venus_order_number message_order_number,
                                v.venus_status,
                                v.msg_type ||
                                        v.comp_order ||
                                        decode (v.message_direction, 'INBOUND', 'I', null) ||
                                        (select max (' Q - ' || t.csr_id) from queue q left outer join queue_tag t on q.message_id = t.message_id where q.mercury_id = v.venus_id and q.queue_type <> 'DC') msg_type,
                                'VENDOR' filling_florist,
                                v.order_date,
                                v.transmission_time,
                                to_char (v.created_on, 'mm/dd/yyyy') message_date, -- to_char (v.transmission_time, 'mm/dd/yyyy') message_date,
                                to_char (v.created_on, 'hh:miAM') message_time, -- to_char (v.transmission_time, 'hh:miAM') message_time,
                                v.price,
                                decode (v.venus_status, 'OPEN', null, 'VP', null, substr (v.venus_status, 1, 3)) message_status,
                                'Venus' group_indicator,
                                2 group_sort,
                                null subject,
                                decode (v.msg_type, 'FTD', 1, 9) msg_order,
                                v.created_on,
                                (select min (v2.created_on) from venus.venus v2 where v2.venus_order_number = v.venus_order_number and v2.msg_type = 'FTD') ftd_created_on,
                                decode (v.msg_type, 'FTD', v.venus_order_number, 'M' || venus_id) message_number,
                                v.message_direction,
                                v.sending_vendor,
                                v.over_under_charge,
                                decode(IN_MAX_COMMENT_LEN_RETURNED, 0, '', get_mouseover_string(
                                        decode(v.msg_type, 'FTD', 
                                        'VENDOR: ' || v.filling_vendor || ' RECIPIENT: ' || v.recipient || ' ' || v.business_name || ' ' || v.address_1 || ' ' || v.city || ', ' || v.state || ' ' || v.zip ||  
                                        ' DELIVERY_DATE: ' || v.delivery_date || ' PRICE: ' || trim(to_char(v.price,'99,990.99')) || ' 1ST_CHOICE: ' || v.first_choice, v.comments),
                                    IN_MAX_COMMENT_LEN_RETURNED)) comments,
                                '' filler_name,
                                '' filler_phone,
                                '' lifecycle_status,
                                case
                                    when v.msg_type = 'FTD' and v.venus_status in ('VERIFIED') then
                                        case (select count(*)
                                            from venus.venus v_notlive
                                            where v_notlive.venus_order_number = v.venus_order_number
                                            and v_notlive.msg_type in ('CAN','REJ'))
                                        when 0 then
                                            'Y'
                                        else
                                            'N'
                                        end
                                    else
                                        'N'
                                end live_ftd
                        FROM    venus.venus v
                        WHERE   v.reference_number = to_char (in_order_detail_id)
                        ORDER BY ftd_created_on, message_order_number, msg_order, created_on
                ) a
                WHERE   rownum <= (in_start_position+in_max_number_returned-1)
        )
        WHERE   row_number >= in_start_position;



-- the cursor returns the email/letter records requested by the start position and number of records returned
OPEN out_email_letter_cur FOR
        SELECT  * FROM
        (
                SELECT  rownum row_number,
                        a.*
                FROM
                (
                        SELECT  to_char (p.point_of_contact_id) message_id,
                                null message_order_number,
                                null mercury_status,
                                p.point_of_contact_type || (select max (' Q - ' || t.csr_id) from queue q left outer join queue_tag t on q.message_id = t.message_id where q.point_of_contact_id = p.point_of_contact_id) msg_type,
                                decode (p.point_of_contact_type, 'Letter', p.letter_title, decode (p.sent_received_indicator, 'O', p.recipient_email_address, p.sender_email_address)) sender_email_address,
                                o.order_date,
                                p.created_on transmission_time,
                                to_char (p.created_on, 'mm/dd/yyyy') message_date,
                                to_char (p.created_on, 'hh:miAM') message_time,
                                (
                                        select  sum (ob.product_amount)
                                        from    order_bills ob
                                        join    order_details od
                                        on      od.order_detail_id = ob.order_detail_id
                                        where   od.order_guid = o.order_guid
                                ) product_total,
                                null message_status,
                                'Email/Letter' group_indicator,
                                3 group_sort,
                                p.email_subject subject,
                                9 msg_order,
                                p.created_on,
                                null ftd_created_on,
                                null message_number,
                                decode(IN_MAX_EMAIL_LEN_RETURNED, 0, '', get_mouseover_string(p.body, IN_MAX_EMAIL_LEN_RETURNED)) message_body
                        FROM    point_of_contact p
                        JOIN    order_details od
                        ON      p.order_detail_id = od.order_detail_id
                        JOIN    orders o
                        ON      od.order_guid = o.order_guid
                        WHERE   p.order_detail_id = in_order_detail_id and p.point_of_contact_type != 'Email_phoenix'
                        ORDER BY group_sort, ftd_created_on, message_order_number, msg_order, created_on
                ) a
                WHERE   rownum <= (in_start_position+in_max_number_returned-1)
        )
        WHERE   row_number >= in_start_position;

END GET_COMMUNICATION_MESSAGES;


PROCEDURE GET_FLORIST_DASHBOARD
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for all florists covering a zip code
        and whether they are codified for the given product and
        what florists already recieved the order

Input:
        order_detail_id         number
        zip code                varchar2
        product id              varchar2

Output:
        cursor containing florist dashboard information

-----------------------------------------------------------------------------*/

v_total_cur             types.ref_cursor;
v_total_covering        number;
v_total_blocked         number;
v_total_active_codified number;
v_total_active_not_codified number;
v_total_currently_closed 		number;
v_total_delivery_date_closed  	number;
v_zip_code              customer.zip_code%type;
v_product_id            order_details.product_id%type;
v_delivery_date         order_details.delivery_date%type;

CURSOR  zip_prod_cur IS
        SELECT  decode (ftd_apps.florist_query_pkg.get_country_from_zip(r.zip_code), 'CA',
                        substr (r.zip_code, 1, 3),
                        substr (r.zip_code, 1, 5)) zip_cide,
                od.product_id
        FROM    order_details od
        JOIN    customer r
        ON      od.recipient_id = r.customer_id
        WHERE   od.order_detail_id = in_order_detail_id;

BEGIN

OPEN zip_prod_cur;
FETCH zip_prod_cur INTO v_zip_code, v_product_id;
CLOSE zip_prod_cur;

SELECT od.delivery_date
INTO v_delivery_date 
FROM clean.order_details od
WHERE od.order_detail_id = in_order_detail_id;

-- get the florists for the zip code and product
ftd_apps.florist_query_pkg.view_florist_dashboard (v_zip_code, v_product_id, v_delivery_date, v_total_cur);
fetch v_total_cur into v_total_covering, v_total_blocked, v_total_active_codified, v_total_active_not_codified, v_total_currently_closed, v_total_delivery_date_closed;
close v_total_cur;

-- return the florist totals as well as the number of florists already used
OPEN OUT_CUR FOR
        SELECT  v_total_covering total_covering,
                count (1) total_used,
                v_total_blocked total_blocked,
                v_total_active_codified total_active_codified,
                v_total_active_not_codified total_active_not_codified,
                v_total_currently_closed as total_currently_closed, 
                v_total_delivery_date_closed as total_delivery_date_closed
        FROM    order_florist_used
        WHERE   order_detail_id = in_order_detail_id;

END GET_FLORIST_DASHBOARD;


PROCEDURE INSERT_ASK_MESSAGE_EVENT_LOG
(
IN_ORDER_DETAIL_ID              IN ASK_MESSAGE_EVENT_LOG.ORDER_DETAIL_ID%TYPE,
IN_FILLING_FLORIST_ID           IN ASK_MESSAGE_EVENT_LOG.FILLING_FLORIST_ID%TYPE,
IN_MESSAGE_TIMESTAMP            IN ASK_MESSAGE_EVENT_LOG.MESSAGE_TIMESTAMP%TYPE,
IN_SYSTEM_TYPE                  IN ASK_MESSAGE_EVENT_LOG.SYSTEM_TYPE%TYPE,
IN_RESPONSE_FLAG                IN ASK_MESSAGE_EVENT_LOG.RESPONSE_FLAG%TYPE,
IN_POINT_OF_CONTACT_ID          IN ASK_MESSAGE_EVENT_LOG.POINT_OF_CONTACT_ID%TYPE,
IN_SENDER_EMAIL_ADDRESS         IN ASK_MESSAGE_EVENT_LOG.SENDER_EMAIL_ADDRESS%TYPE,
IN_QUEUE_TYPE                   IN ASK_MESSAGE_EVENT_LOG.QUEUE_TYPE%TYPE,
IN_MERCURY_ID                   IN ASK_MESSAGE_EVENT_LOG.MERCURY_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the ask message events.

Input:
        order_detail_id                 number
        filling_florist_id              varchar2
        message_timestamp               date
        system_type                     varchar2
        response_flag                   char
        point_of_contact_id             number
        sender_email_address            varchar2
        queue_type                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO ask_message_event_log
(
        ask_message_event_log_id,
        order_detail_id,
        filling_florist_id,
        message_timestamp,
        system_type,
        response_flag,
        created_on,
        point_of_contact_id,
        sender_email_address,
        queue_type,
        mercury_id
)
VALUES
(
        ask_message_event_log_id_sq.nextval,
        in_order_detail_id,
        in_filling_florist_id,
        in_message_timestamp,
        in_system_type,
        in_response_flag,
        sysdate,
        in_point_of_contact_id,
        in_sender_email_address,
        in_queue_type,
        in_mercury_id
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ASK_MESSAGE_EVENT_LOG;


PROCEDURE UPDATE_ASK_MESSAGE_EVENT_LOG
(
IN_ASK_MESSAGE_EVENT_LOG_ID     IN ASK_MESSAGE_EVENT_LOG.ASK_MESSAGE_EVENT_LOG_ID%TYPE,
IN_RESPONSE_FLAG                IN ASK_MESSAGE_EVENT_LOG.RESPONSE_FLAG%TYPE,
IN_CHECKED_24HR_DATE            IN ASK_MESSAGE_EVENT_LOG.CHECKED_24HR_DATE%TYPE,
IN_CHECKED_48HR_DATE            IN ASK_MESSAGE_EVENT_LOG.CHECKED_48HR_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the log information
        with the response flag, or checked on dates

Input:
        ask_message_event_log_id        number
        response_flag                   char
        checked_24hr_date               date
        checked_48hr_date               date

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  ask_message_event_log
SET
        response_flag = NVL(in_response_flag, response_flag),
        checked_24hr_date = NVL(in_checked_24hr_date, checked_24hr_date),
        checked_48hr_date = NVL(in_checked_48hr_date, checked_48hr_date)
WHERE   ask_message_event_log_id = in_ask_message_event_log_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ASK_MESSAGE_EVENT_LOG;


FUNCTION GET_ASK_MERCURY_STATUS
(
IN_MERCURY_ID                   IN MERCURY.MERCURY.MERCURY_ID%TYPE,
IN_CREATED_ON                   IN MERCURY.MERCURY.CREATED_ON%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages statuses
        for the the given mercury id.  The return value is as follows:

        inbound ask/ans message flag | rej/can message flag | live ftd flag

        example:
        Y|N|Y

Input:
        mercury_id                      varchar2

Output:
        concatenated status flags
-----------------------------------------------------------------------------*/

-- cursor to get the mercury order messages for the order
CURSOR merc_cur IS
        SELECT  m.msg_type,
                m.mercury_status,
                m.ask_answer_code,
                m.message_direction,
                m.created_on
        FROM    mercury.mercury m
        WHERE   EXISTS
                (
                        SELECT  1
                        FROM    mercury.mercury m1
                        WHERE   m1.mercury_order_number = m.mercury_order_number
                        AND     m1.mercury_id = in_mercury_id
                )
        ORDER BY m.created_on DESC;

v_has_live_ftd          char (1);
v_can_rej_flag          char (1);
v_ask_ans_flag          char (1);
BEGIN

-- intialize the out parameter flags to N
v_ask_ans_flag := 'N';
v_has_live_ftd := 'N';
v_can_rej_flag := 'N';

-- get the message status from the last mercury order number
FOR msg IN merc_cur LOOP

        CASE msg.msg_type

        WHEN 'FTD' THEN

                -- if the florist has mercury, check that the message is verified
                -- if the florist is a manual florist, do not need to check for a verified message
                IF msg.mercury_status = 'MC' OR msg.mercury_status = 'MO' THEN
                        -- if no CAN or REJ message was previous found, the order has a live FTD message
                        IF v_can_rej_flag = 'N' THEN
                                v_has_live_ftd := 'Y';
                        END IF;
                ELSIF msg.mercury_status = 'MM' OR msg.mercury_status = 'MN' THEN
                        -- MM = manual from order processing (queue created)
                        -- MN = manual from communcation screen (no queue)
                        -- if no CAN or REJ message was previous found, the order has a live FTD message
                        IF v_can_rej_flag = 'N' THEN
                                v_has_live_ftd := 'Y';
                        END IF;
                ELSE
                        v_has_live_ftd := 'N';
                END IF;

                exit;

        WHEN 'CAN' THEN
                -- check for a verified cancel message
                IF msg.mercury_status = 'MC' OR
                   msg.mercury_status = 'MO' OR
                   msg.mercury_status = 'MM' OR
                   msg.mercury_status = 'MN' THEN
                        v_can_rej_flag := 'Y';
                        v_has_live_ftd := 'N';
                END IF;

        WHEN 'REJ' THEN
                -- set that the florist rejected the order
                v_can_rej_flag := 'Y';
                v_has_live_ftd := 'N';

        WHEN 'ASK' THEN
                IF msg.message_direction = 'INBOUND' AND msg.mercury_status = 'MC' AND msg.created_on >= in_created_on THEN
                        v_ask_ans_flag := 'Y';
                END IF;

        WHEN 'ANS' THEN
                IF msg.message_direction = 'INBOUND' AND msg.mercury_status = 'MC' AND msg.created_on >= in_created_on THEN
                        v_ask_ans_flag := 'Y';
                END IF;

        ELSE
                null;

        END CASE;

END LOOP;

RETURN v_ask_ans_flag || '|' || v_can_rej_flag || '|' || v_has_live_ftd;

END GET_ASK_MERCURY_STATUS;

PROCEDURE GET_ASK_MERCURY_STATUS
(
IN_MERCURY_ID                   IN MERCURY.MERCURY.MERCURY_ID%TYPE,
IN_CREATED_ON                   IN MERCURY.MERCURY.CREATED_ON%TYPE,
OUT_STATUS                   OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages statuses
        for the the given mercury id.  The return value is as follows:

        inbound ask/ans message flag | rej/can message flag | live ftd flag

        example:
        Y|N|Y

Input:
        mercury_id                      varchar2

Output:
        concatenated status flags
-----------------------------------------------------------------------------*/
BEGIN

OUT_STATUS := GET_ASK_MERCURY_STATUS(IN_MERCURY_ID,IN_CREATED_ON);

END GET_ASK_MERCURY_STATUS;


PROCEDURE GET_ASK_NO_RESPONSE
(
IN_CHECK_PARM                   IN VARCHAR2,
IN_QUEUE_TYPE                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR,
OUT_QUEUE_PRIORITY              OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for getting all of the ask messages
        that have been sent with no response in 24 or 48 hours

Input:
        check_parm              varchar2 (FIRST_CHECK or SECOND_CHECK global parameter name)
        queue_type              varchar2

Output:
        cursor containing ask_message_event_log information
        priority of the given queue

-----------------------------------------------------------------------------*/

v_check_date      date;
v_hours           number := to_number (frp.misc_pkg.get_global_parm_value ('ASK_MESSAGE_EVENT', in_check_parm));

BEGIN

-- get the priority of the given queue, this was added to consolidate the number of
-- database calls for this process
out_queue_priority := queue_pkg.get_queue_type_priority (in_queue_type);

v_check_date :=  sysdate - v_hours / 24;

OPEN OUT_CUR FOR
        SELECT
                ask_message_event_log_id,
                order_detail_id,
                filling_florist_id,
                message_timestamp,
                system_type,
                response_flag,
                checked_24hr_date,
                checked_48hr_date,
                created_on,
                point_of_contact_id,
                sender_email_address,
                queue_type,
                get_ask_mercury_status (mercury_id, message_timestamp) ask_ans_response_flags
        FROM    ask_message_event_log
        WHERE   response_flag = 'N'
        AND     ((in_check_parm = 'FIRST_CHECK' and checked_24hr_date is null and created_on <= v_check_date)
        OR       (in_check_parm = 'SECOND_CHECK' and checked_48hr_date is null and checked_24hr_date <= v_check_date));

END GET_ASK_NO_RESPONSE;


PROCEDURE GET_ORDER_MESSAGE_STATUS
(
IN_ORDER_DETAIL_ID              IN NUMBER,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_COMP_ORDER                   IN VARCHAR2,
IN_COMMUNICATION_IND            IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages for the
        the given order

Input:
        message order number            varchar2
        message type                    varchar2 (optional, Mercury or Venus,
                                                  if null, this will be based off of the ship method on the order)
        comp_order                      varchar2 (Y - include comp orders
                                                  N - ignore comp orders)
        communcication_ind              varchar2 (Y - called from communciation screen via get_message_status
                                                  null - called from elsewhere
                                                  the difference is that mercury MO status is considered a live ftd in the
                                                  communication screen.  Elsewhere, MO status should not be a live
                                                  ftd message because it has not been processed by EFOS)
Output:
        attempted_ftd                   varchar2
        has_live_ftd                    varchar2
        cancel_sent                     varchar2
        reject_sent                     varchar2
        ftd_status                      varchar2
        delivery_date                   date     (for venus only)
        attempted_can                   varchar2

-----------------------------------------------------------------------------*/

-- cursor to get the vendor_flag, florist status from the florist/vendor on the order
CURSOR order_cur IS
        SELECT  decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag
        FROM    order_details od
        WHERE   od.order_detail_id = in_order_detail_id;

-- cursor to get all of the mercury messages for the order
CURSOR merc_cur (p_comp_order varchar2) IS
        SELECT  m.mercury_id,
                m.msg_type,
                m.filling_florist,
                m.sak_text,
                m.reference_number,
                m.delivery_date,
                m.mercury_status,
                m.comp_order comp_order,
                m.ask_answer_code,
                m.message_direction,
                m.mercury_order_number,
                (select min (m2.created_on) from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'FTD') ftd_created_on,
                (select 'Y' from mercury.call_out_log co where co.mercury_id = m.mercury_id) message_called_out
        FROM    mercury.mercury m
        WHERE   EXISTS
                (
                        SELECT  1
                        FROM    mercury.mercury m1
                        WHERE   m1.mercury_order_number = m.mercury_order_number
                        AND     m1.reference_number = to_char (in_order_detail_id)
                )
        AND     ((m.comp_order is null AND p_comp_order = 'N') OR p_comp_order = 'Y')
        ORDER BY ftd_created_on DESC,
                 m.mercury_order_number,
                 decode (m.msg_type, 'FTD', 9, 1),
                 m.created_on DESC;

-- cursor to get all of the venus messages for the order
CURSOR venus_cur IS
        SELECT  v.venus_id,
                v.msg_type,
                v.filling_vendor filling_florist,
                v.reference_number,
                v.delivery_date,
                v.venus_status,
                v.comp_order comp_order,
                (select min(v2.created_on) from venus.venus v2 where v2.venus_order_number = v.venus_order_number and v2.msg_type = 'FTD') ftd_created_on
        FROM    venus.venus v
        WHERE   v.reference_number = to_char (in_order_detail_id)
        ORDER BY ftd_created_on DESC,
                 v.venus_order_number,
                 v.created_on DESC;


v_attempted_ftd         char(1) := 'N';
v_has_live_ftd          char(1) := 'N';
v_attempted_can         char(1) := 'N';
v_cancel_sent           char(1) := 'N';
v_reject_sent           char(1) := 'N';
v_ftd_status            varchar2(10);
v_delivery_date         date;


v_comp_order            char (1) := in_comp_order;
v_out_comp_order			char(1) := NULL;
v_vendor_flag           ftd_apps.florist_master.vendor_flag%type;
v_mercury_flag          ftd_apps.florist_master.mercury_flag%type;

BEGIN



-- set the vendor flag from the in_message_type parm if specified
-- otherwise, get the vendor flag from the order
CASE in_message_type
WHEN 'Mercury' THEN v_vendor_flag := 'N';
WHEN 'Venus' THEN v_vendor_flag := 'Y';
ELSE
        -- get the vendor flag and full refund from the order
        OPEN order_cur;
        FETCH order_cur INTO v_vendor_flag;
        CLOSE order_cur;
END CASE;

-- process the messages in the reverse order from when they were sent
-- stop at the first FTD message found
IF v_vendor_flag = 'Y' THEN

        -- get the message status from the last venus order number
        FOR msg IN venus_cur LOOP
                
            v_out_comp_order := msg.comp_order;

                CASE msg.msg_type
                WHEN 'FTD' THEN
                        v_attempted_ftd := 'Y';
                        CASE msg.venus_status
                                WHEN 'OPEN' THEN v_ftd_status := 'MO';
                                WHEN 'ERROR' THEN v_ftd_status := 'ME';
                                WHEN 'REJECTED' THEN v_ftd_status := 'MR';
                                WHEN 'VERIFIED' THEN v_ftd_status := 'MC';
                                WHEN 'SHIP' THEN v_ftd_status := 'MO';
                                WHEN 'SHIP_PROCESSING' THEN v_ftd_status := 'MO';
                                ELSE v_ftd_status := null;
                        END CASE;

                        -- if no CAN message was previous found, the order has a live FTD message
                        IF v_cancel_sent = 'N' and v_reject_sent = 'N' and msg.venus_status NOT IN ('ERROR', 'REJECTED', 'SHIP', 'SHIP_PROCESSING') THEN
                                v_has_live_ftd := 'Y';
                        END IF;

                        -- set the delivery date from the message
                        IF v_delivery_date IS NULL THEN
                                v_delivery_date := msg.delivery_date;
                        END IF;

                        exit;

                WHEN 'CAN' THEN
                        v_attempted_can := 'Y';
                        v_cancel_sent := 'Y';
                        v_has_live_ftd := 'N';


                WHEN 'REJ' THEN
                        v_reject_sent := 'Y';
                        v_has_live_ftd := 'N';


                ELSE
                        null;

                END CASE;
                
        END LOOP;
ELSE

        IF in_comp_order = 'N' THEN
                -- check if the order has only 1 ftdc, if so ignore the comp order flag
                v_comp_order := has_only_one_ftdc (in_order_detail_id);
        END IF;

        -- get the message status from the last mercury order number
        FOR msg IN merc_cur (v_comp_order) LOOP
                       
        		v_out_comp_order := msg.comp_order;
                
                CASE msg.msg_type
                WHEN 'FTD' THEN
                        v_attempted_ftd := 'Y';
                        v_ftd_status := msg.mercury_status;

                        -- if the florist has mercury, check that the message is verified
                        -- if the florist is a manual florist, do not need to check for a verified message
                        IF msg.mercury_status = 'MC' OR (msg.mercury_status = 'MO' AND in_communication_ind = 'Y') THEN
                                -- if no CAN or REJ message was previous found, the order has a live FTD message
                                IF v_cancel_sent = 'N' AND v_reject_sent = 'N' THEN
                                        v_has_live_ftd := 'Y';
                                END IF;
                        ELSIF (msg.mercury_status = 'MM' OR msg.mercury_status = 'MN') AND msg.message_called_out = 'Y' THEN
                                -- MM = manual from order processing (queue created)
                                -- MN = manual from communcation screen (no queue)
                                -- manual message is verified if there is a record in the call out log
                                -- if no CAN or REJ message was previous found, the order has a live FTD message
                                IF v_cancel_sent = 'N' AND v_reject_sent = 'N' THEN
                                        v_has_live_ftd := 'Y';
                                END IF;
                        END IF;

                        exit;

                WHEN 'CAN' THEN
                        v_attempted_can := 'Y';
                        -- check for a verified cancel message
                        -- manual message is verified if there is a record in the call out log
                        IF msg.mercury_status in ('MC', 'MO') OR
                           ((msg.mercury_status = 'MM' OR
                             msg.mercury_status = 'MN') AND
                             msg.message_called_out = 'Y') THEN
                                v_cancel_sent := 'Y';
                                v_has_live_ftd := 'N';
                        END IF;

                WHEN 'REJ' THEN
                        -- set that the florist rejected the order
                        v_reject_sent := 'Y';
                        v_has_live_ftd := 'N';

                ELSE
                        null;

                END CASE;
                
        END LOOP;
END IF;
               
OPEN out_cur FOR
        SELECT  v_attempted_ftd attempted_ftd,
                v_has_live_ftd has_live_ftd,
                v_cancel_sent cancel_sent,
                v_reject_sent reject_sent,
                v_ftd_status ftd_status,
                v_delivery_date delivery_date,
                v_out_comp_order comp_order,
                v_attempted_can attempted_can
        FROM    dual;

END GET_ORDER_MESSAGE_STATUS;


PROCEDURE GET_ORDER_MESSAGE_STATUS
(
IN_ORDER_DETAIL_ID              IN NUMBER,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_COMP_ORDER                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages for the
        the given order.  This proc overrides the procedure below defaulting
        the communcation ind to null - MO status should not be a live
        ftd message because it has not been processed by EFOS

Input:
        message order number            varchar2
        message type                    varchar2 (optional, Mercury or Venus,
                                                  if null, this will be based off of the ship method on the order)
        comp_order                      varchar2 (Y - include comp orders
                                                  N - ignore comp orders)

Output:
        attempted_ftd                   varchar2
        has_live_ftd                    varchar2
        cancel_sent                     varchar2
        reject_sent                     varchar2
        ftd_status                      varchar2
        delivery_date                   date     (for venus only)

-----------------------------------------------------------------------------*/

BEGIN

GET_ORDER_MESSAGE_STATUS
(
        IN_ORDER_DETAIL_ID,
        IN_MESSAGE_TYPE,
        IN_COMP_ORDER,
        NULL,
        OUT_CUR
);

END GET_ORDER_MESSAGE_STATUS;


PROCEDURE GET_ORDER_MESSAGE_STATUS
(
IN_ORDER_DETAIL_ID              IN NUMBER,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_COMP_ORDER                   IN VARCHAR2,
OUT_ATTEMPTED_FTD               OUT VARCHAR2,
OUT_HAS_LIVE_FTD                OUT VARCHAR2,
OUT_CANCEL_SENT                 OUT VARCHAR2,
OUT_DELIVERY_DATE               OUT DATE
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages for the
        the given order.  This proc overrides the procedure below defaulting
        the communcation ind to null - MO status should not be a live
        ftd message because it has not been processed by EFOS

Input:
        message order number            varchar2
        message type                    varchar2 (optional, Mercury or Venus,
                                                  if null, this will be based off of the ship method on the order)
        comp_order                      varchar2 (Y - include comp orders
                                                  N - ignore comp orders)

Output:
        attempted_ftd                   varchar2
        has_live_ftd                    varchar2
        cancel_sent                     varchar2
        delivery_date                   date     (for venus only)

NOTE:   THIS PROCEDURE IS TEMPORARY TO SUPPORT EXISTING QA FUNCATIONALITY.
        THIS PROCEDURE SHOULD BE DELETED AS SOON AS THE APPLICATION IS CHANGED
        TO ACCEPT THE CURSOR INSTEAD OF INDIVIDUAL OUT PARMS.

-----------------------------------------------------------------------------*/
v_status_cur    types.ref_cursor;
v_reject_sent   char (1);
v_ftd_status    varchar2(10);
v_comp_order    char(1);
v_attempted_can char(1);

BEGIN

GET_ORDER_MESSAGE_STATUS
(
        IN_ORDER_DETAIL_ID,
        IN_MESSAGE_TYPE,
        IN_COMP_ORDER,
        NULL,
        V_STATUS_CUR
);

FETCH v_status_cur INTO
        out_attempted_ftd,
        out_has_live_ftd,
        out_cancel_sent,
        v_reject_sent,
        v_ftd_status,
        out_delivery_date,
        v_comp_order,
        v_attempted_can;

CLOSE v_status_cur;

END GET_ORDER_MESSAGE_STATUS;


PROCEDURE GET_MESSAGE_STATUS
(
IN_MESSAGE_ORDER_NUMBER         IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_ORDER_DETAIL_ID              IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury messages for the
        the given order

Input:
        message order number            varchar2
        message type                    varchar2 (Mercury or Venus of the given message order number)
        order_detail_id					varchar2

Output:
        order_detail_id                 number
        order_type                      varchar2 (Vendor, Florist, FTDM)
        attempted_ftd                   varchar2
        has_live_ftd                    varchar2
        cancel_sent                     varchar2
        florist_rej_order               varchar2
        filling_florist                 varchar2
        delivery_date                   date
        order_status                    varchar2
        inbound_askp                    varchar2
        a_type_refund                   varchar2
        cancel_denied                   varchar2

-----------------------------------------------------------------------------*/

-- cursors to get the order detail id from the given mercury message
-- and message specific statuses
CURSOR merc_id_cur IS
        SELECT  m.reference_number,
                m.filling_florist,
				nvl ((select distinct 'Y' from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'CAN' and m2.mercury_status in ('MC', 'MM', 'MN', 'MO') and m2.reference_number = m.reference_number), 'N') cancel_sent,
                nvl ((select distinct 'Y' from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'REJ' and m2.reference_number = m.reference_number), 'N') florist_rej_order,
                nvl ((select distinct 'Y' from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.message_direction = 'INBOUND' and m2.ask_answer_code = 'P'), 'N') inbound_askp,
                nvl ((select distinct 'Y' from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'DEN' and m2.reference_number = m.reference_number), 'N') cancel_denied,
                nvl ((select distinct 'Y' from mercury.mercury m2 where m2.mercury_order_number = m.mercury_order_number and m2.msg_type = 'FTD' and m2.mercury_status in ('MM', 'MN') and m2.reference_number = m.reference_number), 'N') ftdm_flag,
                m.comp_order
        FROM    mercury.mercury m
        WHERE   m.mercury_order_number = in_message_order_number
        AND		m.reference_number = in_order_detail_id
        AND     m.reference_number is not null
        ORDER BY m.created_on desc;

-- cursors to get the order detail id from the given venus message
CURSOR venus_id_cur IS
        SELECT  v.reference_number,
                v.filling_vendor,
                nvl ((select distinct 'Y' from venus.venus v2 where v2.venus_order_number = v.venus_order_number and v2.msg_type = 'CAN' and v2.venus_status = 'VERIFIED'), 'N') cancel_sent,
                nvl ((select distinct 'Y' from venus.venus v2 where v2.venus_order_number = v.venus_order_number and v2.msg_type = 'REJ'), 'N') florist_rej_order,
                nvl ((select distinct 'Y' from venus.venus v2 where v2.venus_order_number = v.venus_order_number and v2.msg_type = 'DEN'), 'N') cancel_denied,
                v.comp_order
        FROM    venus.venus v
        WHERE   v.venus_order_number = in_message_order_number
        AND     v.reference_number is not null
        ORDER BY v.created_on desc;

-- cursor to get the order detail id, order status, filling florist and florist status from
-- the florist/vendor on the order
CURSOR order_cur (p_order_detail_id number) IS
        SELECT  od.order_disp_code,
                NVL ((select distinct 'Y' from refund r where r.order_detail_id = od.order_detail_id and r.refund_disp_code like 'A%'), 'N') a_type_refund,
                od.delivery_date
        FROM    order_details od
        WHERE   od.order_detail_id = p_order_detail_id;

v_status_cur            types.ref_cursor;

-- variables to store return values
v_order_detail_id       integer;
v_order_type            varchar2 (20);
v_attempted_ftd         varchar2 (1) := 'N';
v_has_live_ftd          varchar2 (1) := 'N';
v_cancel_sent           varchar2 (1) := 'N';
v_florist_rej_order     varchar2 (1) := 'N';
v_filling_florist       varchar2 (20);
v_inbound_askp          varchar2 (1) := 'N';
v_a_type_refund         varchar2 (1) := 'N';
v_cancel_denied         varchar2 (1) := 'N';
v_comp_order            varchar2 (1);

v_ftdm_flag             char (1);

-- variables to catch unused data from procedures
x_cancel_sent           varchar2 (1);
x_reject_sent           varchar2 (1);
x_ftd_status            varchar2(10);
x_delivery_date         date;
x_comp_order            varchar2(1);
x_attempted_can         varchar2(1);

BEGIN

-- get the order detail_id from the given message
IF in_message_type = 'Venus' THEN
        OPEN venus_id_cur;
        FETCH venus_id_cur INTO
                v_order_detail_id,
                v_filling_florist,
                v_cancel_sent,
                v_florist_rej_order,
                v_cancel_denied,
                v_comp_order;
        CLOSE venus_id_cur;

        v_order_type := 'Vendor';

        GET_ORDER_MESSAGE_STATUS
        (
                IN_ORDER_DETAIL_ID=>v_order_detail_id,
                IN_MESSAGE_TYPE=>'Venus',
                IN_COMP_ORDER=>'N',
                IN_COMMUNICATION_IND=>'Y',
                OUT_CUR=>v_status_cur
        );

        FETCH v_status_cur INTO
                v_attempted_ftd,
                v_has_live_ftd,
                x_cancel_sent,
                x_reject_sent,
                x_ftd_status,
                x_delivery_date,
                x_comp_order,
                x_attempted_can;

        CLOSE v_status_cur;


ELSE
        OPEN merc_id_cur;
        FETCH merc_id_cur INTO
                v_order_detail_id,
                v_filling_florist,
                v_cancel_sent,
                v_florist_rej_order,
                v_inbound_askp,
                v_cancel_denied,
                v_ftdm_flag,
                v_comp_order;
        CLOSE merc_id_cur;

        IF v_ftdm_flag = 'N' THEN
                v_order_type := 'Florist';
        ELSE
                v_order_type := 'FTDM';
        END IF;

        GET_ORDER_MESSAGE_STATUS
        (
                IN_ORDER_DETAIL_ID=>v_order_detail_id,
                IN_MESSAGE_TYPE=>'Mercury',
                IN_COMP_ORDER=>'N',
                IN_COMMUNICATION_IND=>'Y',
                OUT_CUR=>v_status_cur
        );

        FETCH v_status_cur INTO
                v_attempted_ftd,
                v_has_live_ftd,
                x_cancel_sent,
                x_reject_sent,
                x_ftd_status,
                x_delivery_date,
                x_comp_order,
                x_attempted_can;


        CLOSE v_status_cur;

END IF;

OPEN out_cur FOR
        SELECT  od.order_detail_id,
                v_order_type order_type,
                v_attempted_ftd attempted_ftd,
                v_has_live_ftd has_live_ftd,
                v_cancel_sent cancel_sent,
                v_florist_rej_order florist_rej_order,
                v_filling_florist filling_florist,
                od.delivery_date,
                od.order_disp_code order_status,
                v_inbound_askp inbound_askp,
                NVL ((select distinct 'Y' from refund r where r.order_detail_id = od.order_detail_id and r.refund_disp_code like 'A%'), 'N') a_type_refund,
                v_cancel_denied cancel_denied,
                r.country recipient_country,
                v_comp_order comp_order
        FROM    order_details od
        JOIN    customer r
        ON      od.recipient_id = r.customer_id
        WHERE   od.order_detail_id = v_order_detail_id;

END GET_MESSAGE_STATUS;


PROCEDURE GET_MESSAGE_DETAIL_FROM_FTD
(
IN_MESSAGE_ORDER_NUMBER         IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_PRICE_INDICATOR              IN VARCHAR2,
IN_MESSAGE_DIRECTION            IN VARCHAR2,
IN_VERIFIED_INDICATOR           IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury/venus detail
        from the FTD message for the given message order number

Input:
        message order number            varchar2
        message type                    varchar2 (Mercury, Venus)
        price indicator                 varhcar2 (Last - will return the price from the last price message,
                                                  SecondLast - will return the price from the last 2 price messages,
                                                  null - no price will be returned)
        message_direction               varchar2 (OUTBOUND - will only find outbound price messages for getting prices
                                                  null - look at both inbound and outbound price messages)
        verified_indicator              varchar2 (Y/N - used to determine prices from verified or not verified messages)

Output:
        cursor containing message detail


-----------------------------------------------------------------------------*/

CURSOR merc_cur IS
        SELECT  m.filling_florist,
                fm.florist_name,
                fm.phone_number
        FROM    mercury.mercury m
        JOIN    ftd_apps.florist_master fm
        ON      m.filling_florist = fm.florist_id
        WHERE   m.mercury_order_number = in_message_order_number
        AND     m.msg_type = 'FOR'
        ORDER BY m.created_on DESC;

CURSOR price_cur IS
        SELECT  m.price,
                m.mercury_status,
                nvl ((select 'Y' from mercury.call_out_log co where co.mercury_id = m.mercury_id), 'N') message_called_out
        FROM    mercury.mercury m
        WHERE   m.mercury_order_number = in_message_order_number
        AND     (m.msg_type IN ('FTD', 'FOR')
        OR       (m.msg_type IN ('ASK', 'ANS')
        AND       m.ask_answer_code = 'P'))
        AND     m.price IS NOT NULL
        AND     (m.message_direction = in_message_direction or in_message_direction is null)
        ORDER BY m.created_on DESC;

v_filling_florist       mercury.mercury.filling_florist%type;
v_florist_name          ftd_apps.florist_master.florist_name%type;
v_phone_number          ftd_apps.florist_master.phone_number%type;
v_temp_price            mercury.mercury.price%type;
v_price                 mercury.mercury.price%type;
v_old_price             mercury.mercury.price%type;
v_mercury_status        mercury.mercury.mercury_status%type;
v_message_called_out    char (1);

BEGIN

IF in_message_type = 'Mercury' THEN

        OPEN merc_cur;
        FETCH merc_cur INTO v_filling_florist, v_florist_name, v_phone_number;
        CLOSE merc_cur;

        IF in_price_indicator IS NOT NULL THEN

                OPEN price_cur;
                FETCH price_cur INTO v_temp_price, v_mercury_status, v_message_called_out;

                -- get the price from the last price record depending if the
                -- message is verified or not based on the given verified parameter
                WHILE price_cur%found AND v_price is null LOOP
                        IF in_verified_indicator = 'N' THEN
                                v_price := v_temp_price;
                        ELSE
                                -- set the price only if the message is verified
                                IF (v_mercury_status = 'MC') OR
                                   ((v_mercury_status = 'MM' OR v_mercury_status = 'MN') and v_message_called_out = 'Y') THEN
                                        v_price := v_temp_price;
                                END IF;
                        END IF;

                        FETCH price_cur INTO v_temp_price, v_mercury_status, v_message_called_out;
                END LOOP;

                IF in_price_indicator = 'SecondLast' THEN
                        -- get the price from the last price record depending if the
                        -- message is verified or not based on the given verified parameter
                        WHILE price_cur%found AND v_old_price is null LOOP
                                -- set the price only if the message is verified
                                IF (v_mercury_status = 'MC') OR
                                   ((v_mercury_status = 'MM' OR v_mercury_status = 'MN') and v_message_called_out = 'Y') THEN
                                        v_old_price := v_temp_price;
                                END IF;

                                FETCH price_cur INTO v_temp_price, v_mercury_status, v_message_called_out;
                        END LOOP;
                END IF;

                CLOSE price_cur;

        END IF;

        OPEN out_cur FOR
                SELECT  coalesce (v_filling_florist, m.filling_florist) filling_florist,
                        m.sending_florist,
                        m.order_date,
                        m.recipient,
                        m.address,
                        m.city_state_zip,
                        m.delivery_date,
                        m.msg_type,
                        m.delivery_date_text,
                        m.mercury_order_number message_order_number,
                        to_char (m.order_date, 'MON DD - ') || substr (to_char (m.order_date, 'DAY'), 1, 3) order_date_text,
                        NVL (v_price, 0) price,
                        m.mercury_id ftd_message_id,
                        coalesce (v_florist_name, fm.florist_name) florist_name,
                        coalesce (v_phone_number, fm.phone_number) florist_phone_number,
                        NVL (v_old_price, 0) old_price,
			NULL ship_date,
			NULL shipping_system,
                        reference_number,
                        m.phone_number
                FROM    mercury.mercury m
                JOIN    ftd_apps.florist_master fm
                ON      m.filling_florist = fm.florist_id
                WHERE   m.mercury_order_number = in_message_order_number
                AND     m.msg_type = 'FTD'
                ORDER BY m.created_on;

ELSE

        OPEN out_cur FOR
                SELECT  filling_vendor,
                        sending_vendor,
                        order_date,
                        recipient,
                        address_1 || ' ' || address_2 address,
                        city || ',  ' || state || ' ' || zip city_state_zip,
                        delivery_date,
                        msg_type,
                        to_char (delivery_date, 'MON DD - ') || substr (to_char (delivery_date, 'DAY'), 1, 3) delivery_date_text,
                        venus_order_number message_order_number,
                        to_char (order_date, 'MON DD - ') || substr (to_char (order_date, 'DAY'), 1, 3) order_date_text,
                        price,
                        venus_id ftd_message_id,
                        null florist_name,
                        null florist_phone_number,
                        0 old_price,
			ship_date,
			shipping_system,
                        reference_number,
                        phone_number
                FROM    venus.venus
                WHERE   venus_order_number = in_message_order_number
                AND     msg_type = 'FTD'
                ORDER BY created_on;
END IF;

END GET_MESSAGE_DETAIL_FROM_FTD;


PROCEDURE GET_MESSAGE_DETAIL
(
IN_MESSAGE_ID                   IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury/venus detail
        from the given message

Input:
        message id                      varchar2
        message type                    varchar2 (Mercury, Venus)

Output:
        cursor containing message detail


-----------------------------------------------------------------------------*/

-- if the given message is a inbound ASKP or ANSP, the old price will not be known
-- find the old price from from the last out price message
CURSOR check_cur IS
        SELECT  'Y'
        FROM    mercury.mercury
        WHERE   mercury_id = in_message_id
        AND     msg_type IN ('ASK', 'ANS')
        AND     ask_answer_code = 'P'
        AND     message_direction = 'INBOUND';
v_check                 char(1);

CURSOR price_cur IS
        SELECT  m.price
        FROM    mercury.mercury m
        WHERE   (m.msg_type IN ('FTD', 'FOR')
        OR      (m.msg_type IN ('ASK', 'ANS')
        AND      m.ask_answer_code = 'P'))
        AND     m.message_direction = 'OUTBOUND'
        AND     EXISTS
        (
                select  1
                from    mercury.mercury m1
                where   m1.mercury_id = in_message_id
                and     m1.mercury_order_number = m.mercury_order_number
                and     m1.created_on > m.created_on
        )
        ORDER BY m.created_on DESC;
v_old_price             mercury.mercury.old_price%type;

v_messaging_window_period number := to_number (frp.misc_pkg.GET_GLOBAL_PARM_VALUE('MESSAGING','MESSAGING_WINDOW_PERIOD'));

-- find the delivery date for the given message if the message is outbound
-- outbound messages will always have the reference number = order detail id
CURSOR merc_detail_cur1 IS
        SELECT  delivery_date
        FROM    order_details od
        WHERE   EXISTS
        (
                SELECT  1
                FROM    mercury.mercury m1
                WHERE   m1.reference_number = to_char (od.order_detail_id)
                AND     m1.mercury_id = in_message_id
        );

-- if the delivery date is not found in the query above then
-- find the associated FTD message for the given message
CURSOR merc_detail_cur2 IS
        SELECT  delivery_date
        FROM    order_details od
        WHERE   EXISTS
        (
                SELECT  1
                FROM    mercury.mercury m1
                WHERE   m1.reference_number = to_char (od.order_detail_id)
                AND     m1.msg_type = 'FTD'
                AND     EXISTS
                (
                        SELECT  1
                        FROM    mercury.mercury m2
                        WHERE   m2.mercury_id = in_message_id
                        AND     m2.mercury_order_number = m1.mercury_order_number
                )
        );


-- find the delivery date for the given message if the message is outbound
-- outbound messages will always have the reference number = order detail id
CURSOR venus_detail_cur1 IS
        SELECT  delivery_date
        FROM    order_details od
        WHERE   EXISTS
        (
                SELECT  1
                FROM    venus.venus v1
                WHERE   v1.reference_number = to_char (od.order_detail_id)
                AND     v1.venus_id = in_message_id
        );

-- if the delivery date is not found in the query above then
-- find the associated FTD message for the given message
CURSOR venus_detail_cur2 IS
        SELECT  delivery_date
        FROM    order_details od
        WHERE   EXISTS
        (
                SELECT  1
                FROM    venus.venus v1
                WHERE   v1.reference_number = to_char (od.order_detail_id)
                AND     v1.msg_type = 'FTD'
                AND     EXISTS
                (
                        SELECT  1
                        FROM    venus.venus v2
                        WHERE   v2.venus_id = in_message_id
                        AND     v2.venus_order_number = v1.venus_order_number
                )
        );

CURSOR merc_order_date IS
        SELECT  m1.order_date,
                m1.mercury_id
        FROM    mercury.mercury m1
        WHERE   m1.msg_type = 'FTD'
        AND     EXISTS
        (       select  1
                from    mercury.mercury m2
                where   m2.mercury_order_number = m1.mercury_order_number
                and     m2.mercury_id = in_message_id
        );

v_delivery_date         order_details.delivery_date%type;
v_order_date            mercury.mercury.order_date%type;
v_mercury_id            mercury.mercury.mercury_id%type;

BEGIN

IF in_message_type = 'Mercury' THEN

        OPEN check_cur;
        FETCH check_cur INTO v_check;
        CLOSE check_cur ;

        IF v_check = 'Y' THEN
                OPEN price_cur;
                FETCH price_cur INTO v_old_price;
                CLOSE price_cur;
        END IF;

        OPEN merc_detail_cur1;
        FETCH merc_detail_cur1 INTO v_delivery_date;
        CLOSE merc_detail_cur1;

        IF v_delivery_date IS NULL THEN
                OPEN merc_detail_cur2;
                FETCH merc_detail_cur2 INTO v_delivery_date;
                CLOSE merc_detail_cur2;
        END IF;

        OPEN merc_order_date;
        FETCH merc_order_date INTO v_order_date, v_mercury_id;
        CLOSE merc_order_date;

        OPEN out_cur FOR
                SELECT  m.mercury_order_number message_order_number,
                        in_message_type detail_type,
                        decode (m.mercury_status, 'MM', 'M', 'MN', 'M', null) ||
                                m.comp_order ||
                                m.ask_answer_code ||
                                decode (m.message_direction, 'INBOUND', 'I', null) sub_type,
                        m.message_direction,
                        m.filling_florist,
                        m.sending_florist || m.outbound_id sending_florist,
                        m.recipient,
                        m.address,
                        m.city_state_zip,
                        m.phone_number,
                        v_delivery_date delivery_date, --m.delivery_date,
                        m.first_choice,
                        m.second_choice,
                        m.price,
                        m.card_message,
                        m.occasion,
                        m.special_instructions,
                        m.priority,
                        m.operator,
                        coalesce (m.old_price, v_old_price) old_price,
                        m.reference_number order_detail_id,
                        NVL (m.order_date, v_order_date) order_date,
                        m.comments,
                        DECODE (mercury_status, 'MM', 'MANUAL', 'MN', 'MANUAL', 'MC', 'VERIFIED', 'MO', 'OPEN', 'ME', 'ERROR', 'MR', 'REJECTED', substr (m.sak_text, 1, 8)) verified,
                        --decode (substr (m.sak_text, 1, 8), 'VERIFIED', to_date (rtrim (substr (m.sak_text, 14, 16)) || 'M', 'MON DD YY HH:MIAM'), 'REJECTED', to_date (rtrim (substr (m.sak_text, 14, 16)) || 'M', 'MON DD YY HH:MIAM'), null)  status_date,
                        nvl(cast(m.transmission_time as timestamp),created_on) status_date,
                        m.combined_report_number,
                        m.adj_reason_code,
                        null cancel_reason_code,
                        (select a.description from ftd_apps.adj_reason_code_val a where a.adj_reason_code = m.adj_reason_code) adj_reason_desc,
                        null cancel_reason_desc,
                        case when v_delivery_date + v_messaging_window_period >= sysdate then 'Y' else 'N' end within_message_window,
                        m.msg_type,
                        m.delivery_date_text,
                        to_char (NVL (m.order_date, v_order_date), 'MON DD - ') || substr (to_char (NVL (m.order_date, v_order_date), 'DAY'), 1, 3) order_date_text,
                        null ftp_vendor_product,
                        v_mercury_id ftd_message_id,
                        m.mercury_status,
                        is_message_order_can_rej (m.mercury_order_number, 'Mercury') can_rej_flag,
                        null flower_monthly_prd_flag,
                        m.over_under_charge,
                        NULL ship_date,
                        NULL shipping_system, 
                        NULL zone_jump_flag,
                        NULL zone_jump_label_date,
                        NULL zone_jump_trailer_number,
                        NULL order_add_ons
                FROM    mercury.mercury m
                WHERE   m.mercury_id = in_message_id;

ELSE
        OPEN venus_detail_cur1;
        FETCH venus_detail_cur1 INTO v_delivery_date;
        CLOSE venus_detail_cur1;

        IF v_delivery_date IS NULL THEN
                OPEN venus_detail_cur1;
                FETCH venus_detail_cur1 INTO v_delivery_date;
                CLOSE venus_detail_cur1;
        END IF;

        OPEN out_cur FOR
                SELECT  v.venus_order_number message_order_number,
                        in_message_type detail_type,
                        v.comp_order sub_type,
                        'OUTBOUND' message_direction,
                        v.filling_vendor filling_florist,
                        v.sending_vendor sending_florist,
                        v.recipient,
                        v.business_name || ' ' || v.address_1 || ' , ' || v.address_2 address,
                        v.city || ', ' || v.state || ' '|| v.zip city_state_zip,
                        v.phone_number,
                        coalesce (v.delivery_date, v_delivery_date) delivery_date,
                        v.first_choice,
                        'NONE' second_choice,
                        v.price,
                        v.card_message,
                        (select max (o.description) from ftd_apps.occasion o join order_details od on o.occasion_id = od.occasion where od.order_detail_id = v.reference_number) occasion,
                        null special_instructions,
                        'X' priority,
                        v.operator,
                        null old_price,
                        v.reference_number order_detail_id,
                        v.order_date,
                        v.comments,
                        v.venus_status verified,
                        v.created_on status_date,
                        v.combined_report_number,
                        v.adj_reason_code,
                        v.cancel_reason_code,
                        (select a.description from ftd_apps.adj_reason_code_val a where a.adj_reason_code = v.adj_reason_code) adj_reason_desc,
                        (select c.description from venus.cancel_reason_code_val c where c.cancel_reason_code = v.cancel_reason_code) cancel_reason_desc,
                        case when coalesce (v.delivery_date, v_delivery_date) + v_messaging_window_period >= sysdate then 'Y' else 'N' end within_message_window,
                        v.msg_type,
                        to_char (coalesce (v.delivery_date, v_delivery_date), 'MON DD - ') || substr (to_char (coalesce (v.delivery_date, v_delivery_date), 'DAY'), 1, 3) delivery_date_text,
                        to_char (v.order_date, 'MON DD - ') || substr (to_char (v.order_date, 'DAY'), 1, 3) order_date_text,
                        decode ((select max (vm.vendor_type) from ftd_apps.vendor_master vm where vm.member_number = v.filling_vendor), 'FTP', 'Y', 'N') ftp_vendor_product,
                        null ftd_message_id,
                        v.venus_status mercury_status,
                        is_message_order_can_rej (v.venus_order_number, 'Venus') can_rej_flag,
                        nvl ((select 'Y' from ftd_apps.flower_monthly_product p where p.product_id = v.product_id), 'N') flower_monthly_prd_flag,
                        v.over_under_charge,
                        v.ship_date ship_date,
                        v.shipping_system shipping_system, 
                        v.zone_jump_flag,
                        v.zone_jump_label_date,
                        v.zone_jump_trailer_number,
                        (select listagg(oao.add_on_code, ',')
                            within group (order by oao.add_on_code)
                            from clean.order_add_ons oao
                            where to_char(oao.order_detail_id) = v.reference_number) order_add_ons
                FROM    venus.venus v
                WHERE   v.venus_id = in_message_id;

END IF;

END GET_MESSAGE_DETAIL;



PROCEDURE GET_MESSAGE_DETAIL_FROM_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_MESSAGE_TYPE                 IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail info
        for a new ftd message

Input:
        order_detail_id                 number
        message type                    varchar2 (Mercury, Venus)

Output:
        cursor containing message detail


-----------------------------------------------------------------------------*/

v_product_id FTD_APPS.vendor_product.PRODUCT_SUBCODE_ID%TYPE := null;
v_product_subcode_id FTD_APPS.vendor_product.PRODUCT_SUBCODE_ID%TYPE := null;
v_vendor_id FTD_APPS.vendor_product.VENDOR_ID%TYPE := null;
v_vendor_sku FTD_APPS.vendor_product.VENDOR_SKU%TYPE := null;
v_vendor_cost FTD_APPS.vendor_product.VENDOR_COST%TYPE := null;

BEGIN

IF in_message_type = 'Mercury' THEN
        OPEN out_cur FOR
                SELECT
                        od.order_detail_id,
                        r.first_name,
                        r.last_name,
                        r.address_1 || ' ' || r.address_2 recipient_address,
                        r.city,
                        r.state,
                        r.zip_code,
                        coalesce (
                                (select  cp.phone_number || ' ' || cp.extension from customer_phones cp where cp.customer_id = r.customer_id and cp.phone_type = 'Day' and cp.phone_number is not null),
                                (select  cp.phone_number || ' ' || cp.extension from customer_phones cp where cp.customer_id = r.customer_id and cp.phone_type = 'Evening' and cp.phone_number is not null)
                        ) recipient_phone_number,
                        od.delivery_date,
                        od.product_id,
                        null product_description,
                        null vendor_cost,
                        od.occasion,
                        o.description occasion_description,
                        od.card_message || ' '|| od.card_signature card_message,
                        od.card_signature,
                        od.special_instructions,
                        to_char (od.delivery_date, 'MON DD - ') || substr (to_char (od.delivery_date, 'DAY'), 1, 3) || decode (od.delivery_date_range_end, null, null, ' THRU ' || to_char (od.delivery_date_range_end, 'MON DD - ') || substr (to_char (od.delivery_date_range_end, 'DAY'), 1, 3)) delivery_date_text,
                        o.order_date,
                        to_char (o.order_date, 'MON DD - ') || substr (to_char (o.order_date, 'DAY'), 1, 3) order_date_text,
                        od.florist_id,
                        od.delivery_date_range_end,
                        od.external_order_number,
                        fm.florist_name,
                        fm.phone_number,
                        null vendor_sku,
                        r.business_name,
                        od.source_code,
                        r.country
                FROM    order_details od
                JOIN    customer r
                ON      od.recipient_id = r.customer_id
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                LEFT OUTER JOIN    ftd_apps.florist_master fm
                ON      od.florist_id = fm.florist_id
                LEfT OUTER JOIN ftd_apps.occasion o
                ON      od.occasion = o.occasion_id
                WHERE   od.order_detail_id = in_order_detail_id;
ELSE
        BEGIN
             SELECT VENDOR_ID, PRODUCT_ID, SUBCODE INTO v_vendor_id, v_product_id, v_product_subcode_id FROM ORDER_DETAILS WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;
             IF v_vendor_id != null AND v_product_id != null THEN
               IF v_product_subcode_id = null THEN
                 SELECT vp1.vendor_sku, vp1.vendor_cost INTO v_vendor_sku, v_vendor_cost
                   FROM FTD_APPS.VENDOR_PRODUCT vp1
                   WHERE vp1.product_subcode_id = v_product_id;
               ElSE
                 SELECT vp1.vendor_sku, vp1.vendor_cost INTO v_vendor_sku, v_vendor_cost
                   FROM FTD_APPS.VENDOR_PRODUCT vp1
                   WHERE vp1.product_subcode_id = v_product_subcode_id;
               END IF;
             END IF;
        EXCEPTION WHEN NO_DATA_FOUND THEN
             v_vendor_id := null;
             v_vendor_sku := null;
             v_vendor_cost := null;
        END;
        
        OPEN out_cur FOR
                SELECT
                        od.order_detail_id,
                        r.first_name,
                        r.last_name,
                        r.address_1 || ' ' || r.address_2 recipient_address,
                        r.city,
                        r.state,
                        r.zip_code,
                        coalesce (
                                (select  substr(cp.phone_number,0,10) from customer_phones cp where cp.customer_id = r.customer_id and cp.phone_type = 'Day' and cp.phone_number is not null),
                                (select  substr(cp.phone_number,0,10) from customer_phones cp where cp.customer_id = r.customer_id and cp.phone_type = 'Evening' and cp.phone_number is not null)
                        ) recipient_phone_number,
                        od.delivery_date delivery_date,
                        od.product_id,
                        p.short_description product_description,
                        null vendor_cost,
                        od.occasion,
                        o.description occasion_description,
                        od.card_message || ' '|| od.card_signature card_message,
                        od.card_signature,
                        od.special_instructions,
                        to_char (od.delivery_date, 'MON DD - ') || substr (to_char (od.delivery_date, 'DAY'), 1, 3) delivery_date_text,
                        o.order_date,
                        to_char (o.order_date, 'MON DD - ') || substr (to_char (o.order_date, 'DAY'), 1, 3) order_date_text,
                        od.florist_id,
                        od.delivery_date_range_end,
                        od.external_order_number,
                        null florist_name,
                        null phone_number,
                        null vendor_sku,
                        r.business_name,
                        od.source_code,
                        r.country,
                        v_vendor_sku,
                        v_vendor_cost,
                        (select decode(ob1.morning_delivery_fee,NULL,'N','Y') from clean.order_bills ob1 where od.order_detail_id = ob1.order_detail_id and ob1.additional_bill_indicator='N') order_has_morning_delivery,
                        (select listagg(oao.add_on_code, ',')
                            within group (order by oao.add_on_code)
                            from clean.order_add_ons oao
                            where oao.order_detail_id = od.order_detail_id) order_add_ons,
                decode(upper(p.shipping_system),'FTD WEST','Y','N')is_ftdw_order
                FROM    order_details od
                JOIN    customer r
                ON      od.recipient_id = r.customer_id
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                LEFT OUTER JOIN ftd_apps.product_master p
                ON      od.product_id = p.product_id
                LEfT OUTER JOIN ftd_apps.occasion o
                ON      od.occasion = o.occasion_id
                WHERE   od.order_detail_id = in_order_detail_id;

END IF;

END GET_MESSAGE_DETAIL_FROM_ORDER;


FUNCTION GET_MESSAGE_PRICE_BY_ORDER
(
IN_REFERENCE_NUMBER             VARCHAR2
)
RETURN NUMBER
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the price
        from the last message with a price
Input:
        reference number                varchar2

Output:
        price

-----------------------------------------------------------------------------*/

CURSOR price_cur IS
        SELECT  price,
                created_on
        FROM    mercury.mercury
        WHERE   reference_number = in_reference_number
        AND     msg_type IN ('FTD', 'ASK', 'ANS')
        AND     price IS NOT NULL
        UNION
        SELECT  price,
                created_on
        FROM    venus.venus
        WHERE   reference_number = in_reference_number
        AND     msg_type = 'FTD'
        AND     price IS NOT NULL
        ORDER BY created_on DESC;

v_price         number (10, 2);
v_created_on    date;

BEGIN

OPEN price_cur;
FETCH price_cur INTO v_price, v_created_on;
CLOSE price_cur;

RETURN v_price;

END GET_MESSAGE_PRICE_BY_ORDER;

PROCEDURE VIEW_CANCEL_REASON_CODES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning all cancel reason codes

Input:
        cancel_reason_code              varchar2
        description                     varchar2

Output:
        cursor containing all records from cancel_reason_code_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                cancel_reason_code,
                description,
                allow_before_ship_flag,
                allow_after_ship_flag
        FROM    venus.cancel_reason_code_val
        WHERE   status = 'Active'
        ORDER BY cancel_reason_code;

END VIEW_CANCEL_REASON_CODES;


PROCEDURE VIEW_ADJ_REASON_CODES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for

Input:
        adj_reason_code                 varchar2
        description                     varchar2
        status                          varchar2

Output:
        cursor containing all records from adj_reason_code_val

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                adj_reason_code,
                description
        FROM    ftd_apps.adj_reason_code_val
        WHERE   status = 'Active'
        ORDER BY adj_reason_code;

END VIEW_ADJ_REASON_CODES;


PROCEDURE VIEW_HOT_KEY_MESSAGES
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the list of hot key
        messages

Input:
        hot_key_message_id              number
        message_description             varchar2
        display_order                   number

Output:
        cursor containing all records from hot_key_messages

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                hot_key_message_id,
                message_description,
                display_order
        FROM    hot_key_messages
        ORDER BY display_order;

END VIEW_HOT_KEY_MESSAGES;


PROCEDURE GET_MESG_DETAIL_FROM_LAST_FTD
(
IN_ORDER_DETAIL_ID              IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_COMP_ORDER                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury/venus detail
        from the last FTD message for the given order number

Input:
        message order number            varchar2
        message type                    varchar2 (Mercury, Venus)
        comp_order                      varhcar2 (Y - include COMP FTDs or N - ignore COMP FTDs)

Output:
        cursor containing message detail


-----------------------------------------------------------------------------*/

CURSOR merc_cur (p_comp_order varchar2) IS
        SELECT  m.mercury_id,
                m.mercury_order_number
        FROM    mercury.mercury m
        WHERE   m.reference_number = in_order_detail_id
        AND     m.msg_type = 'FTD'
        AND     ((m.comp_order is null AND p_comp_order = 'N') OR p_comp_order = 'Y')
        ORDER BY m.created_on DESC;

CURSOR florist_cur (p_mercury_order_number varchar2) IS
        SELECT  m.filling_florist,
                fm.florist_name,
                fm.phone_number
        FROM    mercury.mercury m
        JOIN    ftd_apps.florist_master fm
        ON      m.filling_florist = fm.florist_id
        WHERE   m.mercury_order_number = p_mercury_order_number
        AND     m.msg_type = 'FOR'
        ORDER BY m.created_on DESC;

CURSOR venus_cur IS
        SELECT  v.venus_id,
                v.venus_order_number
        FROM    venus.venus v
        WHERE   v.reference_number = in_order_detail_id
        AND     v.msg_type = 'FTD'
        ORDER BY v.created_on DESC;

v_message_id            varchar2(20);
v_message_order_number  varchar2(11);
v_filling_florist       mercury.mercury.filling_florist%type;
v_florist_name          ftd_apps.florist_master.florist_name%type;
v_phone_number          ftd_apps.florist_master.phone_number%type;
v_comp_order            char(1) := in_comp_order;

BEGIN

IF in_message_type = 'Mercury' THEN

        IF in_comp_order = 'N' THEN
                -- check if the order has only 1 ftdc, if so ignore the comp order flag
                v_comp_order := has_only_one_ftdc(to_number (in_order_detail_id));
        END IF;

        OPEN merc_cur (v_comp_order);
        FETCH merc_cur INTO v_message_id, v_message_order_number;
        CLOSE merc_cur;

        OPEN florist_cur (v_message_order_number);
        FETCH florist_cur INTO v_filling_florist, v_florist_name, v_phone_number;
        CLOSE florist_cur;

        OPEN out_cur FOR
                SELECT  coalesce (v_filling_florist, m.filling_florist) filling_florist,
                        m.sending_florist,
                        m.order_date,
                        m.recipient,
                        m.address,
                        m.city_state_zip,
                        m.delivery_date,
                        m.msg_type,
                        m.delivery_date_text,
                        m.mercury_order_number message_order_number,
                        to_char (m.order_date, 'MON DD - ') || substr (to_char (m.order_date, 'DAY'), 1, 3) order_date_text,
                        m.price,
                        m.mercury_id ftd_message_id,
                        coalesce (v_florist_name, fm.florist_name) florist_name,
                        coalesce (v_phone_number, fm.phone_number) florist_phone_number,
                        m.old_price,
			NULL ship_date,
			NULL shipping_system,
			reference_number,
			NULL zone_jump_flag,
			NULL zone_jump_label_date,
			NULL sds_status
                FROM    mercury.mercury m
                JOIN    ftd_apps.florist_master fm
                ON      m.filling_florist = fm.florist_id
                WHERE   m.mercury_id = v_message_id;

ELSE

        OPEN venus_cur;
        FETCH venus_cur INTO v_message_id, v_message_order_number;
        CLOSE venus_cur;

        OPEN out_cur FOR
                SELECT  filling_vendor,
                        sending_vendor,
                        order_date,
                        recipient,
                        address_1 || ' ' || address_2 address,
                        city || ',  ' || state || ' ' || zip city_state_zip,
                        delivery_date,
                        msg_type,
                        to_char (delivery_date, 'MON DD - ') || substr (to_char (delivery_date, 'DAY'), 1, 3) delivery_date_text,
                        venus_order_number message_order_number,
                        to_char (order_date, 'MON DD - ') || substr (to_char (order_date, 'DAY'), 1, 3) order_date_text,
                        price,
                        venus_id ftd_message_id,
                        null florist_name,
                        null florist_phone_number,
                        0 old_price,
			ship_date,
			shipping_system,
			reference_number,
			zone_jump_flag,
			zone_jump_label_date,
			sds_status
                FROM    venus.venus
                WHERE   venus_id = v_message_id;
END IF;

END GET_MESG_DETAIL_FROM_LAST_FTD;


PROCEDURE GET_ORDER_IDS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning relevant id for the
        given order

Input:
        order_detail_id                 number

Output:
        cursor containing order ids

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                point_of_contact_pkg.order_has_emails (od.order_detail_id) order_has_emails,
                point_of_contact_pkg.order_has_letters (od.order_detail_id) order_has_letters
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   od.order_detail_id = in_order_detail_id;

END GET_ORDER_IDS;



FUNCTION HAS_ONLY_ONE_FTDC
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if the order messages
        only has one FTDC message or if all messages are FTDC.  This is used
        when checking if comp FTD messages should be ignored.

Input:
        order_detail_id                 number

Output:
        Y/N

-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT  count (1) rowcount,
                sum (decode (m.comp_order, 'C', 1, 0)) comp_order
        FROM    mercury.mercury m
        WHERE   m.reference_number = to_char (in_order_detail_id)
        AND     m.msg_type = 'FTD';

v_check         varchar2(10) := 'N';
v_count         integer;
v_comp_order    varchar2(10);

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_count, v_comp_order;
CLOSE check_cur;

-- if all ftd messages are comp
IF v_count = v_comp_order THEN
        v_check := 'Y';
END IF;

RETURN v_check;

END HAS_ONLY_ONE_FTDC;


PROCEDURE INSERT_CALL_OUT_LOG
(
IN_MERCURY_ID                   IN VARCHAR2,
IN_SHOP_NAME                    IN VARCHAR2,
IN_SHOP_PHONE                   IN VARCHAR2,
IN_SPOKE_TO                     IN VARCHAR2,
IN_CREATED_BY                   IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting the call out log and
        deleting the associated queue record

Input:
        mercury_id                      varchar2
        shopName						varchar2
        shopPhone						varchar2
        spoke_to                        varchar2
        created_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR queue_cur IS
        SELECT  message_id
        FROM    queue
        WHERE   mercury_id = in_mercury_id;

TYPE int_tab_typ IS TABLE OF INTEGER INDEX BY PLS_INTEGER;
v_mesg_tab      int_tab_typ;

BEGIN

MERCURY.MERCURY_PKG.INSERT_CALL_OUT_LOG
(
        IN_MERCURY_ID=>in_mercury_id,
        IN_SHOP_NAME=>in_shop_name,
        IN_SHOP_PHONE=>in_shop_phone,
        IN_SPOKE_TO=>in_spoke_to,
        IN_CREATED_BY=>in_created_by,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);

IF out_status = 'Y' THEN
        OPEN queue_cur;
        FETCH queue_cur BULK COLLECT INTO v_mesg_tab;
        CLOSE queue_cur;

        FOR x IN 1..v_mesg_tab.count LOOP
                QUEUE_PKG.DELETE_QUEUE_RECORD_NO_AUTH
                (
                IN_MESSAGE_ID=>v_mesg_tab (x),
                IN_CSR_ID=>in_created_by,
                OUT_STATUS=>out_status,
                OUT_ERROR_MESSAGE=>out_message
                );

                IF out_status = 'N' THEN
                        exit;
                END IF;
        END LOOP;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END INSERT_CALL_OUT_LOG;


PROCEDURE GET_MERC_VENUS_TRANSACTIONS
(
 IN_REFERENCE_NUMBER  IN VARCHAR2,
 IN_DATE              IN DATE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves Mercury and Venus price relevant
        transactions. Specifically, retrieve records for the given shopping
        cart with messages of all types but GEN from mercury.mercury
        and venus.venus.

Input:
        reference_number    VARCHAR2
        create_date              DATE

Output:
        out_cursor contains payments
-----------------------------------------------------------------------------*/


BEGIN

   OPEN out_cursor FOR
            SELECT m.msg_type || NVL(DECODE(m.mercury_status,'MM','M',''),'')
                              || NVL(m.ask_answer_code,'')
                              || NVL(m.comp_order,'')
                              || DECODE(m.message_direction,'INBOUND','I','') msg_type,
                   m.created_on,
                   0 price,
                   m.filling_florist florist_vendor_id,
                   m.mercury_order_number merc_venus_id,
                   rm.created_on reconciled_date,
                   rm.amount_reconciled,
                   'MERC' system
              FROM mercury.mercury m
                    LEFT OUTER JOIN recon_messages rm
                                 ON replace(m.mercury_order_number,'-') = rm.message_id
                                AND rm.system = 'Merc'
                                AND rm.recon_disp_code IN ('RECONCILED','PRICE VARIANCE')
                                AND rm.archive_indicator = 'N'
             WHERE m.reference_number = in_reference_number
               AND TRUNC(m.created_on) > TRUNC(TO_DATE(in_date))
               AND m.msg_type = 'FTD'
           UNION
            SELECT v.msg_type || NVL(DECODE(v.venus_status,'MM','M',''),'')
                              || NVL(v.comp_order,'') msg_type,
                   v.created_on,
                   0 price,
                   v.filling_vendor florist_vendor_id,
                   v.venus_order_number merc_venus_id,
                   rm.created_on reconciled_date,
                   rm.amount_reconciled,
                   'VENUS' system
              FROM venus.venus v
                    LEFT OUTER JOIN recon_messages rm
                                 ON v.venus_order_number = rm.message_id
                                AND rm.system = 'Venus'
                                AND rm.recon_disp_code IN ('RECONCILED','PRICE VARIANCE')
                                AND rm.archive_indicator = 'N'
             WHERE v.reference_number = in_reference_number
               AND TRUNC(v.created_on) > TRUNC(TO_DATE(in_date))
               AND v.msg_type = 'FTD'
            ORDER BY created_on;




END GET_MERC_VENUS_TRANSACTIONS;


PROCEDURE INSERT_RECON_MESSAGES
(
 IN_FILLER_ID             IN RECON_MESSAGES.FILLER_ID%TYPE,
 IN_SYSTEM                IN RECON_MESSAGES.SYSTEM%TYPE,
 IN_MESSAGE_ID            IN RECON_MESSAGES.MESSAGE_ID%TYPE,
 IN_AMOUNT_RECONCILED     IN RECON_MESSAGES.AMOUNT_RECONCILED%TYPE,
 IN_AMOUNT_TO_RECONCILE   IN RECON_MESSAGES.AMOUNT_TO_RECONCILE%TYPE,
 IN_RECON_DISP_CODE       IN RECON_MESSAGES.RECON_DISP_CODE%TYPE,
 OUT_STATUS              OUT VARCHAR2,
 OUT_MESSAGE             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a new record into table recon_messages.

Input:
        filler_id           VARCHAR2
        system              VARCHAR2
        message_id          VARCHAR2
        amount_reconciled   NUMBER
        amount_to_reconcile NUMBER
        recon_disp_code     VARCHAR2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/


BEGIN


  INSERT INTO recon_messages
  (
   recon_message_id,
   filler_id,
   system,
   message_id,
   amount_reconciled,
   amount_to_reconcile,
   recon_disp_code,
   created_on,
   updated_on
  )
  VALUES
  (
   recon_message_id_sequence.NEXTVAL,
   in_filler_id,
   in_system,
   in_message_id,
   in_amount_reconciled,
   in_amount_to_reconcile,
   in_recon_disp_code,
   sysdate,
   sysdate
  );

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_RECON_MESSAGES;


PROCEDURE ARCHIVE_RECON_MESSAGES
(
 IN_DATE      IN DATE,
 OUT_STATUS  OUT VARCHAR2,
 OUT_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the recon_disp_code to 'ARCHIVED'
        for all records in table recon_messages related to specific
        mercury or venus messages

Input:
        in_date   DATE

Output:
        status     varchar2 (Y or N)
        message    varchar2 (error message)

-----------------------------------------------------------------------------*/


BEGIN

  UPDATE recon_messages rm
     SET rm.archive_indicator = 'Y',
         updated_on = SYSDATE
   WHERE rm.recon_disp_code IN ('RECONCILED','PRICE VARIANCE')
     AND rm.created_on > trunc(sysdate-32)
     AND rm.archive_indicator = 'N'
     AND (
          (
           system = 'Merc'
           AND
           EXISTS (SELECT 1
                     FROM mercury.mercury
                    WHERE REPLACE(mercury_order_number,'-') = rm.message_id
                      AND TRUNC(created_on) <= TRUNC(in_date)
                      AND msg_type = 'FTD')
          )
          OR
          (
           system = 'Venus'
           AND
           EXISTS (SELECT 1
                     FROM venus.venus
                    WHERE venus_order_number = rm.message_id
                      AND TRUNC(created_on) <= TRUNC(in_date)
                      AND msg_type = 'FTD')
          )
         );

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No recon_messages records archived with a related created_on on or before ' || in_date ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END ARCHIVE_RECON_MESSAGES;


FUNCTION IS_DOUBLE_DIP
(
 IN_SYSTEM     IN RECON_MESSAGES.SYSTEM%TYPE,
 IN_MESSAGE_ID IN RECON_MESSAGES.MESSAGE_ID%TYPE
)
 RETURN CHAR
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a Y/N flag if the recon_messages
        for the given message_id and system has a recon_disp_code of
        'RECONCILED' or 'PRICE VARIANCE'.

Input:
        system              VARCHAR2
        message_id          VARCHAR2

Output:
        flag - CHAR

-----------------------------------------------------------------------------*/

v_flag  CHAR(1);
v_count NUMBER;

BEGIN

  BEGIN
    SELECT COUNT(1)
      INTO v_count
      FROM recon_messages
     WHERE system = in_system
       AND message_id = IN_MESSAGE_ID
       AND recon_disp_code IN ('RECONCILED','PRICE VARIANCE')
       AND archive_indicator = 'N';

    EXCEPTION WHEN NO_DATA_FOUND THEN v_count := 0;
  END;

  IF v_count = 0 THEN
    v_flag := 'N';
  ELSE
    v_flag := 'Y';
  END IF;

  RETURN v_flag;

END IS_DOUBLE_DIP;


PROCEDURE CALCULATE_AMOUNT_TO_RECONCILE
(
 IN_SYSTEM     IN RECON_MESSAGES.SYSTEM%TYPE,
 IN_MESSAGE_ID IN RECON_MESSAGES.MESSAGE_ID%TYPE,
 IN_DATE       IN DATE,
 OUT_PRICE    OUT NUMBER,
 OUT_STATUS   OUT VARCHAR2,
 OUT_MESSAGE  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a Y/N flag if the recon_messages
        for the given message_id and system has a recon_disp_code of
        'RECONCILED' or 'PRICE VARIANCE'.

Input:
        system              VARCHAR2
        message_id          VARCHAR2
        in_date             DATE

Output:
        price         NUMBER
        status     varchar2 (Y or N)
        message    varchar2 (error message)

-----------------------------------------------------------------------------*/

  CURSOR mercury_cur (mon VARCHAR2) IS
     SELECT created_on,
            msg_type,
            price,
            mercury_order_number,
            message_direction
       FROM mercury.mercury
      WHERE REPLACE(mercury_order_number, '-') = mon
        AND TRUNC(created_on) >= TRUNC(in_date)
        AND msg_type <> 'ADJ'
        AND (
             (
              message_direction = 'OUTBOUND'
              AND
              (
               msg_type IN ('FTD','CAN')
               OR
               (
                MSG_TYPE IN ('ANS','ASK')
                AND
                ask_answer_code = 'P'
               )
              )
             )
             OR
             (
              message_direction = 'INBOUND'
              AND
              MSG_TYPE IN ('DEN','FOR', 'REJ')
             )
            )
       ORDER BY created_on DESC,
                mercury_id;

  CURSOR venus_cur (von VARCHAR2) IS
    SELECT created_on,
           msg_type,
           price,
           venus_order_number,
           message_direction
      FROM venus.venus
     WHERE venus_order_number = von
       AND TRUNC(created_on) >= TRUNC(in_date)
       AND msg_type <> 'ADJ'
       AND (
            (
             message_direction = 'OUTBOUND'
             AND
             msg_type IN ('FTD', 'CAN')
            )
            OR
            (
             message_direction = 'INBOUND'
             AND
             msg_type IN ('DEN','FOR')
            )
           )
     ORDER BY created_on DESC,
              venus_id;

  v_rowcount NUMBER := 0;
  v_price_found BOOLEAN := FALSE;
  v_den_found BOOLEAN := FALSE;
  can_exception EXCEPTION;
  rej_exception EXCEPTION;
  no_records_found EXCEPTION;

BEGIN

  IF UPPER(in_system) = 'MERC' THEN

    FOR mercury_rec IN mercury_cur(in_message_id) LOOP
        v_rowcount := mercury_cur%ROWCOUNT;
        IF mercury_rec.msg_type = 'REJ' THEN
           RAISE rej_exception;
        ELSIF mercury_rec.msg_type = 'DEN' THEN
           v_den_found := TRUE;
        ELSIF mercury_rec.msg_type = 'CAN' AND NOT v_den_found THEN
           RAISE can_exception;
        END IF;

        IF NOT v_price_found THEN
           IF mercury_rec.message_direction = 'OUTBOUND' AND mercury_rec.price IS NOT NULL THEN

              out_price := mercury_rec.price;
              v_price_found := TRUE;

           ELSIF mercury_rec.msg_type = 'FOR' THEN

              BEGIN
                SELECT price
                  INTO out_price
                  FROM mercury.mercury
                 WHERE mercury_order_number = mercury_rec.mercury_order_number
                   AND msg_type = 'FTD';

                EXCEPTION WHEN NO_DATA_FOUND THEN out_price := 0;
              END;

              v_price_found := TRUE;

           END IF;

        END IF;
    END LOOP;

    IF v_rowcount = 0 THEN
       RAISE no_records_found;
    END IF;

  ELSIF UPPER(in_system) = 'VENUS' THEN

    FOR venus_rec IN venus_cur(in_message_id) LOOP
        v_rowcount := venus_cur%ROWCOUNT;
        IF venus_rec.msg_type = 'REJ' THEN
           RAISE rej_exception;
        ELSIF venus_rec.msg_type = 'DEN' THEN
           v_den_found := TRUE;
        ELSIF venus_rec.msg_type = 'CAN' AND NOT v_den_found THEN
           RAISE can_exception;
        END IF;

        IF NOT v_price_found THEN
           IF venus_rec.message_direction = 'OUTBOUND' AND venus_rec.price IS NOT NULL THEN

              out_price :=  venus_rec.price;
              v_price_found := TRUE;

           ELSIF venus_rec.msg_type = 'FOR' THEN

              BEGIN
                SELECT price
                  INTO out_price
                  FROM venus.venus
                 WHERE venus_order_number = venus_rec.venus_order_number
                   AND msg_type = 'FTD';

                EXCEPTION WHEN NO_DATA_FOUND THEN out_price := 0;
              END;

              v_price_found := TRUE;

           END IF;

        END IF;
    END LOOP;

    IF v_rowcount = 0 THEN
       RAISE no_records_found;
    END IF;

  ELSE

    RAISE no_records_found;

  END IF;

  out_status := 'Y';

  EXCEPTION
    WHEN no_records_found THEN
      BEGIN
        out_status := 'N';
        out_message := 'No records found';
      END;

    WHEN rej_exception THEN
      BEGIN
        out_status := 'N';
        out_message := 'A rejected record was found';
      END;

    WHEN can_exception THEN
      BEGIN
        out_status := 'N';
        out_message := 'A CANCEL record with no DENY record was found';
      END;

    WHEN OTHERS THEN
      BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      END;

END CALCULATE_AMOUNT_TO_RECONCILE;


PROCEDURE GET_FILLER_ID
(
 IN_SYSTEM            IN RECON_MESSAGES.SYSTEM%TYPE,
 IN_MESSAGE_ID        IN RECON_MESSAGES.MESSAGE_ID%TYPE,
 OUT_FILLING_FLORIST OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves Mercury and Venus filling vendor
        for the system and message id passed in.

Input:
        system              VARCHAR2
        message_id          VARCHAR2

Output:
        out_cursor contains payments
-----------------------------------------------------------------------------*/


BEGIN

  BEGIN
    IF in_system = 'Merc' THEN
      SELECT filling_florist
        INTO out_filling_florist
        FROM mercury.mercury
       WHERE REPLACE(mercury_order_number, '-') = in_message_id
         AND msg_type IN ('FOR', 'FTD')
         AND ROWNUM = 1
       ORDER BY created_on desc;
    ELSIF in_system = 'Venus' THEN
      SELECT filling_vendor
        INTO out_filling_florist
        FROM venus.venus
       WHERE venus_order_number = in_message_id
         AND msg_type IN ('FOR', 'FTD')
         AND ROWNUM = 1
       ORDER BY created_on desc;
    END IF;

   EXCEPTION WHEN NO_DATA_FOUND THEN out_filling_florist := NULL;

 END;


END GET_FILLER_ID;


FUNCTION IS_ORDER_FTDM
(
IN_ORDER_DETAIL_ID              IN VARCHAR2
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the last FTD mercury message sent
        was a manual FTD

Input:
        order_detail_id                 number

Output:
        Y/N - if the last ftd mercury message was a manual FTD
-----------------------------------------------------------------------------*/

CURSOR merc_cur IS
        SELECT  decode (m.mercury_status, 'MM', 'Y', 'MN', 'Y', 'N') ftdm_flag
        FROM    mercury.mercury m
        WHERE   m.reference_number = in_order_detail_id
        AND     m.msg_type = 'FTD'
        ORDER BY m.created_on desc;

v_ftdm_flag     char(1);

BEGIN

OPEN merc_cur;
FETCH merc_cur INTO v_ftdm_flag;
CLOSE merc_cur;

IF v_ftdm_flag is null THEN
        v_ftdm_flag := 'N';
END IF;

RETURN v_ftdm_flag;

END IS_ORDER_FTDM;


FUNCTION GET_MESG_INFO_FOR_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_FIELD                        IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the indicated field from the last
        FTD of the given order.

Input:
        order_detail_id                 number
        in_field                        varchar2 - mercury_order_number,
                                                   filling_florist

Output:
        last ftd mercury/venus id
-----------------------------------------------------------------------------*/
-- cursor to get the vendor_flag, florist status from the florist/vendor on the order
CURSOR order_cur IS
        SELECT  decode (od.ship_method, null, 'Mercury', 'SD', 'Mercury', 'Venus') vendor_flag
        FROM    order_details od
        WHERE   od.order_detail_id = in_order_detail_id;

v_message_type          varchar2(10);
v_cur                   types.ref_cursor;

v_filling_florist       mercury.mercury.filling_florist%type;
v_sending_florist       mercury.mercury.sending_florist%type;
v_order_date            mercury.mercury.order_date%type;
v_recipient             mercury.mercury.recipient%type;
v_address               mercury.mercury.address%type;
v_city_state_zip        mercury.mercury.city_state_zip%type;
v_delivery_date         mercury.mercury.delivery_date%type;
v_msg_type              mercury.mercury.msg_type%type;
v_delivery_date_text    mercury.mercury.delivery_date_text%type;
v_mercury_order_number  mercury.mercury.mercury_order_number%type;
v_order_date_text       varchar2(50);
v_price                 mercury.mercury.price%type;
v_message_id            mercury.mercury.mercury_id%type;
v_florist_name          varchar2(50);
v_florist_phone_number  varchar2(12);
v_old_price             mercury.mercury.old_price%type;
v_ship_date             venus.venus.ship_date%type;
v_shipping_system       venus.venus.shipping_system%type;
v_reference_number      mercury.mercury.reference_number%type;
v_zone_jump_flag        venus.venus.zone_jump_flag%type;
v_zone_jump_label_date  venus.venus.zone_jump_label_date%type;
v_sds_status		venus.venus.sds_status%type;

BEGIN

OPEN order_cur;
FETCH order_cur INTO v_message_type;
CLOSE order_cur;

GET_MESG_DETAIL_FROM_LAST_FTD
(
        IN_ORDER_DETAIL_ID=>in_order_detail_id,
        IN_MESSAGE_TYPE=>v_message_type,
        IN_COMP_ORDER=>'N',
        OUT_CUR=>v_cur
);

FETCH v_cur INTO
        v_filling_florist,
        v_sending_florist,
        v_order_date,
        v_recipient,
        v_address,
        v_city_state_zip,
        v_delivery_date,
        v_msg_type,
        v_delivery_date_text,
        v_mercury_order_number,
        v_order_date_text,
        v_price,
        v_message_id,
        v_florist_name,
        v_florist_phone_number,
        v_old_price,
        v_ship_date,
        v_shipping_system,
        v_reference_number,
	v_zone_jump_flag,
	v_zone_jump_label_date,
	v_sds_status;

CLOSE v_cur;

CASE in_field
WHEN 'mercury_order_number' THEN
        RETURN v_mercury_order_number;
WHEN 'filling_florist' THEN
        RETURN v_filling_florist;
ELSE
        RETURN null;
END CASE;

END GET_MESG_INFO_FOR_ORDER;



FUNCTION GET_LAST_FLORIST_FOR_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the last filling florist id

Input:
        order_detail_id                 number

Output:
        last filling florist id
-----------------------------------------------------------------------------*/
v_return        varchar2(100);

BEGIN

v_return := GET_MESG_INFO_FOR_ORDER(IN_ORDER_DETAIL_ID, 'filling_florist');

RETURN v_return;

END GET_LAST_FLORIST_FOR_ORDER;


FUNCTION GET_LAST_FTD_FOR_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the last FTD mercury/venus id

Input:
        order_detail_id                 number

Output:
        last ftd mercury/venus id
-----------------------------------------------------------------------------*/
v_return        varchar2(100);

BEGIN

v_return := GET_MESG_INFO_FOR_ORDER(IN_ORDER_DETAIL_ID, 'mercury_order_number');

RETURN v_return;

END GET_LAST_FTD_FOR_ORDER;






FUNCTION IS_MANUAL_FTD_VERIFIED
(
IN_MERCURY_ID                   IN MERCURY.CALL_OUT_LOG.MERCURY_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the manual FTD mercury was
        called out.  If so, it is considered verified.

Input:
        mercury_id                      varchar2

Output:
        VER or null
-----------------------------------------------------------------------------*/

CURSOR verify_cur IS
        SELECT 'VER'
        FROM    mercury.call_out_log c
        WHERE   c.mercury_id = in_mercury_id;

v_return        char(3);

BEGIN

OPEN verify_cur;
FETCH verify_cur INTO v_return;
CLOSE verify_cur;

RETURN v_return;

END IS_MANUAL_FTD_VERIFIED;


FUNCTION IS_MESSAGE_ORDER_CAN_REJ
(
IN_MESSAGE_ORDER_NUMBER         VARCHAR2,
IN_MESSAGE_TYPE                 VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the manual FTD mercury was
        called out.  If so, it is considered verified.

Input:
        mercury_id                      varchar2

Output:
        VER or null
-----------------------------------------------------------------------------*/

CURSOR merc_cur IS
        SELECT  'Y'
        FROM    mercury.mercury m
        WHERE   m.mercury_order_number = in_message_order_number
        AND     ((m.msg_type IN ('CAN', 'REJ') AND m.mercury_status in ('MC', 'MO')) OR
                 (m.msg_type = 'CAN' AND m.mercury_status IN ('MM', 'MN') AND IS_MANUAL_FTD_VERIFIED (m.mercury_id) = 'VER'));

CURSOR venus_cur IS
        SELECT  'Y'
        FROM    venus.venus v
        WHERE   v.venus_order_number = in_message_order_number
        AND     v.msg_type IN ('CAN', 'REJ');

v_return        char(1) := 'N';

BEGIN

IF in_message_type = 'Mercury' THEN
        OPEN merc_cur;
        FETCH merc_cur INTO v_return;
        CLOSE merc_cur;

ELSE
        OPEN venus_cur;
        FETCH venus_cur INTO v_return;
        CLOSE venus_cur;
END IF;

RETURN v_return;

END IS_MESSAGE_ORDER_CAN_REJ;


PROCEDURE GET_MESSAGE_DETAIL_GEN_MESG
(
IN_MESSAGE_NUMBER               IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning mercury/venus detail
        from the given GEN message.  This is a copy of get_message_detail
        to address a defect that GEN messages are being retrieved using
        the mercury_message_number instead of the mercury_id.
        The price logic and delivery date logic was perserved
        even though GEN messages are only Mercury and will not be associated
        with an order as to not cause another defect.

Input:
        message number                  varchar2
        message type                    varchar2 (Mercury)

Output:
        cursor containing message detail


-----------------------------------------------------------------------------*/

-- if the given message is a inbound ASKP or ANSP, the old price will not be known
-- find the old price from from the last out price message
CURSOR check_cur IS
        SELECT  'Y'
        FROM    mercury.mercury
        WHERE   mercury_message_number = in_message_number
        AND     msg_type IN ('ASK', 'ANS')
        AND     ask_answer_code = 'P'
        AND     message_direction = 'INBOUND';
v_check                 char(1);

CURSOR price_cur IS
        SELECT  m.price
        FROM    mercury.mercury m
        WHERE   (m.msg_type IN ('FTD', 'FOR')
        OR      (m.msg_type IN ('ASK', 'ANS')
        AND      m.ask_answer_code = 'P'))
        AND     m.message_direction = 'OUTBOUND'
        AND     EXISTS
        (
                select  1
                from    mercury.mercury m1
                where   m1.mercury_message_number = in_message_number
                and     m1.mercury_order_number = m.mercury_order_number
                and     m1.created_on > m.created_on
        )
        ORDER BY m.created_on DESC;
v_old_price             mercury.mercury.old_price%type;

v_messaging_window_period number := to_number (frp.misc_pkg.GET_GLOBAL_PARM_VALUE('MESSAGING','MESSAGING_WINDOW_PERIOD'));

-- find the delivery date for the given message if the message is outbound
-- outbound messages will always have the reference number = order detail id
CURSOR merc_detail_cur1 IS
        SELECT  delivery_date
        FROM    order_details od
        WHERE   EXISTS
        (
                SELECT  1
                FROM    mercury.mercury m1
                WHERE   m1.reference_number = to_char (od.order_detail_id)
                AND     m1.mercury_message_number = in_message_number
        );

-- if the delivery date is not found in the query above then
-- find the associated FTD message for the given message
CURSOR merc_detail_cur2 IS
        SELECT  delivery_date
        FROM    order_details od
        WHERE   EXISTS
        (
                SELECT  1
                FROM    mercury.mercury m1
                WHERE   m1.reference_number = to_char (od.order_detail_id)
                AND     m1.msg_type = 'FTD'
                AND     EXISTS
                (
                        SELECT  1
                        FROM    mercury.mercury m2
                        WHERE   m2.mercury_message_number = in_message_number
                        AND     m2.mercury_order_number = m1.mercury_order_number
                )
        );

CURSOR merc_order_date IS
        SELECT  m1.order_date,
                m1.mercury_id
        FROM    mercury.mercury m1
        WHERE   m1.msg_type = 'FTD'
        AND     EXISTS
        (       select  1
                from    mercury.mercury m2
                where   m2.mercury_order_number = m1.mercury_order_number
                and     m2.mercury_message_number = in_message_number
        );

v_delivery_date         order_details.delivery_date%type;
v_order_date            mercury.mercury.order_date%type;
v_mercury_id            mercury.mercury.mercury_id%type;

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur ;

IF v_check = 'Y' THEN
        OPEN price_cur;
        FETCH price_cur INTO v_old_price;
        CLOSE price_cur;
END IF;

OPEN merc_detail_cur1;
FETCH merc_detail_cur1 INTO v_delivery_date;
CLOSE merc_detail_cur1;

IF v_delivery_date IS NULL THEN
        OPEN merc_detail_cur2;
        FETCH merc_detail_cur2 INTO v_delivery_date;
        CLOSE merc_detail_cur2;
END IF;

OPEN merc_order_date;
FETCH merc_order_date INTO v_order_date, v_mercury_id;
CLOSE merc_order_date;

OPEN out_cur FOR
        SELECT  m.mercury_order_number message_order_number,
                in_message_type detail_type,
                decode (m.mercury_status, 'MM', 'M', 'MN', 'M', null) ||
                        m.comp_order ||
                        m.ask_answer_code ||
                        decode (m.message_direction, 'INBOUND', 'I', null) sub_type,
                m.message_direction,
                m.filling_florist,
                m.sending_florist || m.outbound_id sending_florist,
                m.recipient,
                m.address,
                m.city_state_zip,
                m.phone_number,
                v_delivery_date delivery_date, --m.delivery_date,
                m.first_choice,
                m.second_choice,
                m.price,
                m.card_message,
                m.occasion,
                m.special_instructions,
                m.priority,
                m.operator,
                coalesce (m.old_price, v_old_price) old_price,
                m.reference_number order_detail_id,
                NVL (m.order_date, v_order_date) order_date,
                m.comments,
                DECODE (mercury_status, 'MM', 'MANUAL', 'MN', 'MANUAL', 'MC', 'VERIFIED', 'MO', 'OPEN', 'ME', 'ERROR', 'MR', 'REJECTED', substr (m.sak_text, 1, 8)) verified,
                --decode (substr (m.sak_text, 1, 8), 'VERIFIED', to_date (rtrim (substr (m.sak_text, 14, 16)) || 'M', 'MON DD YY HH:MIAM'), 'REJECTED', to_date (rtrim (substr (m.sak_text, 14, 16)) || 'M', 'MON DD YY HH:MIAM'), null)  status_date,
                m.transmission_time status_date,
                m.combined_report_number,
                m.adj_reason_code,
                null cancel_reason_code,
                (select a.description from ftd_apps.adj_reason_code_val a where a.adj_reason_code = m.adj_reason_code) adj_reason_desc,
                null cancel_reason_desc,
                case when v_delivery_date + v_messaging_window_period >= sysdate then 'Y' else 'N' end within_message_window,
                m.msg_type,
                m.delivery_date_text,
                to_char (NVL (m.order_date, v_order_date), 'MON DD - ') || substr (to_char (NVL (m.order_date, v_order_date), 'DAY'), 1, 3) order_date_text,
                null ftp_vendor_product,
                v_mercury_id ftd_message_id,
                m.mercury_status,
                is_message_order_can_rej (m.mercury_order_number, 'Mercury') can_rej_flag,
                null flower_monthly_prd_flag,
                m.over_under_charge
        FROM    mercury.mercury m
        WHERE   m.mercury_message_number = in_message_number;

END GET_MESSAGE_DETAIL_GEN_MESG;


PROCEDURE GET_ORDER_FULFILLMENT_STATUS
(
IN_ORDER_DETAIL_ID              IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the mercury/venus status
        for a given order_detail_id

Input:
        order_detail_id                 varchar2

Output:
        live_order_count                number
        has_ftdm                        varchar2
        order_printed                   varchar2
        vendor_flag                     varchar2
        filling_florist                 varchar2
        mercury_id                      varchar2
        before_delivery                 varchar2

-----------------------------------------------------------------------------*/

cursor order_type_cur is
    select decode(od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
    od.delivery_date
    from clean.order_details od
    where od.order_detail_id = in_order_detail_id;

cursor merc_cur is
    select m.mercury_id,
        m.msg_type,
        m.mercury_status,
        m.mercury_order_number,
        m.filling_florist,
        (select 'Y' from mercury.call_out_log co where co.mercury_id = m.mercury_id) message_called_out
    from mercury.mercury m
    where exists (
        select 1 from mercury.mercury m1
        where m1.mercury_order_number = m.mercury_order_number
        and m1.reference_number = to_char(in_order_detail_id))
    order by m.mercury_order_number,
        decode(m.msg_type,'FTD',9,1),
        m.created_on desc;

cursor venus_cur IS
    select  v.venus_id,
        v.msg_type,
        v.venus_order_number,
        v.filling_vendor,
        v.comments,
        v.venus_status,
        v.printed
    from    venus.venus v
    where   v.reference_number = to_char(in_order_detail_id)
    order by v.venus_order_number,
        v.created_on desc;

v_has_live_ftd          char(1) := 'N';
v_cancel_sent           char(1) := 'N';
v_reject_sent           char(1) := 'N';
v_live_count            number := 0;
v_mercury_id            varchar2(100);
v_filling_florist       varchar2(100);
v_vendor_flag           char(1) := 'N';
v_has_ftdm              char(1) := 'N';
temp_has_ftdm           char(1) := 'N';
v_order_printed         char(1) := 'N';
temp_order_printed      char(1) := 'N';
v_delivery_date         date;
v_before_delivery  char(1) := 'Y';

BEGIN

open order_type_cur;
fetch order_type_cur into v_vendor_flag, v_delivery_date;
close order_type_cur;

if v_delivery_date <= trunc(sysdate) then
    v_before_delivery := 'N';
end if;

if v_vendor_flag = 'Y' then

    for msg in venus_cur loop

        case msg.msg_type

            when 'FTD' then
                if v_cancel_sent = 'N' and v_reject_sent = 'N' and msg.venus_status not in ('ERROR', 'REJECTED') then
                    v_has_live_ftd := 'Y';
                end if;

                if msg.printed is not NULL then
                    temp_order_printed := 'Y';
                end if;

                if v_has_live_ftd = 'Y' then
                    v_live_count := v_live_count + 1;
                    v_mercury_id := msg.venus_id;
                    v_filling_florist := msg.filling_vendor;
                    if temp_order_printed = 'Y' then 
                        v_order_printed := 'Y';
                    end if;
                end if;

                v_cancel_sent := 'N';
                v_reject_sent := 'N';
                temp_order_printed := 'N';

            when 'CAN' then
                v_cancel_sent := 'Y';
                v_has_live_ftd := 'N';

            when 'REJ' then
                v_reject_sent := 'Y';
                v_has_live_ftd := 'N';

            when 'ANS' then
                if substr(msg.comments,1,16) = 'Order printed on' then
                    temp_order_printed := 'Y';
                end if;

            else
                null;

        end case;

    end loop;

else

    for msg in merc_cur loop

        case msg.msg_type

            when 'FTD' then
                if msg.mercury_status = 'MC' then
                    if v_cancel_sent = 'N' and v_reject_sent = 'N' then
                       v_has_live_ftd := 'Y';
                    end if;
                elsif (msg.mercury_status = 'MM' or msg.mercury_status = 'MN') and msg.message_called_out = 'Y' then
                    if v_cancel_sent = 'N' and v_reject_sent = 'N' then
                       v_has_live_ftd := 'Y';
                       temp_has_ftdm := 'Y';
                    end if;
                end if;

                if v_has_live_ftd = 'Y' then
                    v_live_count := v_live_count + 1;
                    v_mercury_id := msg.mercury_id;
                    v_filling_florist := msg.filling_florist;
                    if temp_has_ftdm = 'Y' then
                        v_has_ftdm := 'Y';
                    end if;
                end if;

                v_cancel_sent := 'N';
                v_reject_sent := 'N';
                temp_has_ftdm := 'N';

            when 'CAN' then
                if msg.mercury_status in ('MC', 'MO') or
                    ((msg.mercury_status = 'MM' or msg.mercury_status = 'MN') and msg.message_called_out = 'Y') then
                    v_cancel_sent := 'Y';
                    v_has_live_ftd := 'N';
                end if;

            when 'REJ' then
                v_reject_sent := 'Y';
                v_has_live_ftd := 'N';

            else
                null;

        end case;

    end loop;

end if;

if v_live_count <> 1 then
    v_mercury_id := '';
    v_filling_florist := '';
end if;

open out_cur for
    select  v_live_count             live_order_count,
            v_has_ftdm               has_ftdm,
            v_order_printed          order_printed,
            v_vendor_flag            vendor_flag,
            v_filling_florist        filling_florist,
            v_mercury_id             mercury_id,
            v_before_delivery        before_delivery
    from dual;

END GET_ORDER_FULFILLMENT_STATUS;

FUNCTION IS_VENDOR_COMP_ORDER
(
	IN_VENUS_ORDER_NUMBER   IN   VARCHAR2
)  
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the manual FTD mercury was
        called out.  If so, it is considered verified.

Input:
        venus_order_number                      varchar2

Output:
        Y/N
-----------------------------------------------------------------------------*/

	CURSOR check_comp IS
   SELECT  count(1)
   FROM    venus.venus v
   WHERE   v.venus_order_number = in_venus_order_number
   AND     v.msg_type = 'FTD'
   AND     v.comp_order IS NOT NULL;

	v_comp_count    integer := 0;
	v_comp_order    varchar2(1) := 'N';

BEGIN

	OPEN check_comp;
	FETCH check_comp INTO v_comp_count;
	CLOSE check_comp;

	-- if all ftd messages are comp
	IF v_comp_count > 0 THEN
		v_comp_order := 'Y';
	END IF;

	RETURN v_comp_order;
	
END IS_VENDOR_COMP_ORDER;

PROCEDURE GET_MATCHING_AUTO_RESPONSE_KEY
(
IN_MESSAGE_TEXT               	IN VARCHAR2,
IN_MSG_TYPE                 	IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the records in
        mercury.auto_response_key_phrase that have a matching key_phrase
        in the in_message_text.

Input:
        messag_text             varchar2
        msg_type		varchar2

Output:
        out_cur			ref_cursor

-----------------------------------------------------------------------------*/
v_ignore_key_phrase                 char(1);

BEGIN

    v_ignore_key_phrase := frp.misc_pkg.get_global_parm_value ('ORDER_PROCESSING', 'DISABLE_'|| in_msg_type ||'_CRITERIA');
    
    IF v_ignore_key_phrase IS NULL or v_ignore_key_phrase = 'N' THEN

	    OPEN OUT_CUR FOR
	    SELECT kp.key_phrase_id,
		   kp.msg_type,
		   kp.key_phrase_txt,
		   kp.respond_with_can_flag,
		   kp.retry_ftd_flag,
		   kp.florist_soft_block_flag,
		   kp.view_queue_flag,
		   kp.sort_order_seq
	    FROM mercury.auto_response_key_phrase kp
	    WHERE kp.active_flag = 'Y'
	    AND kp.msg_type = in_msg_type
	    AND INSTR(UPPER(in_message_text), UPPER(kp.key_phrase_txt), 1, 1) > 0
	    ORDER BY kp.sort_order_seq, kp.key_phrase_id ASC;
	   
    ELSE
    	    OPEN OUT_CUR FOR
    	    SELECT 0 key_phrase_id,
    	    	   in_msg_type msg_type,
    	    	   in_message_text key_phrase_txt,
    	    	   decode(in_msg_type, 'REJ', 'N', 'FTD', 'N', 'ASK', 'Y', 'Y') respond_with_can_flag,
    	    	   'Y' retry_ftd_flag,
    	    	   'N' florist_soft_block_flag,
    	    	   'N' view_queue_flag,
    	    	   0 sort_order_seq
    	    FROM DUAL;
    END IF;

END GET_MATCHING_AUTO_RESPONSE_KEY;


--*****************************************************************************
--                   GET_MESSAGE_DETAILS_GENERIC
--*****************************************************************************
/*-----------------------------------------------------------------------------
Description:
  This is a generic stored procedure that:
  1) should have no business logic
  2) will return everything as it is in the Database. It will not distinct between 
     Live/Verfied/Attempted etc.

  It will be driven by IN_ORD_OR_MSG_OR_ID_LEVEL. If the value is:
  1) ORD - it will expect order detail id, based off of, it will find all the 
           mercury/venus order numbers, and will return ALL records for those
           mercury/venus order numbers.
  2) MSG - at this point, it will check values for IN_MERCURY_VENUS_ORDER_NUMBER and 
           IN_MERCURY_MESSAGE_NUMBER. If both are passed, it will throw an error. If 
           IN_MERCURY_MESSAGE_NUMBER is passed with IN_MESSAGE_TYPE of Venus, it will 
           throw an error.  If all validity checks pass, it will return ALL Mercury ONLY
           records for IN_MERCURY_MESSAGE_NUMBER, or it will return ALL Mercury/Venus
           records for IN_MERCURY_VENUS_ORDER_NUMBER. 
  3) ID  - this will return a particular record for a given IN_MERCURY_VENUS_ID
           
  

Input:
  IN_REFERENCE_NUMBER             -  order detail id
  IN_MERCURY_VENUS_ID             -  mercury_id or venus_id
  IN_MERCURY_MESSAGE_NUMBER       -  mercury_message_number
  IN_MERCURY_VENUS_ORDER_NUMBER   -  mercury_order_number or venus_order_number
  IN_MSG_TYPE                     -  FTD, ANS etc.  Note that the input value MUST
                                     have ' and be separated by a , like: 'FTD','ANS','ASK'
  IN_MESSAGE_TYPE                 -  Mercury or Venus
  IN_ORD_OR_MSG_OR_ID_LEVEL       -  ID or MSG or ORD

Output:
  OUT_CUR

-----------------------------------------------------------------------------*/

PROCEDURE GET_MESSAGE_DETAILS_GENERIC
(
IN_REFERENCE_NUMBER             IN VARCHAR2,
IN_MERCURY_VENUS_ID             IN VARCHAR2,
IN_MERCURY_MESSAGE_NUMBER       IN MERCURY.MERCURY.MERCURY_MESSAGE_NUMBER%TYPE,
IN_MERCURY_VENUS_ORDER_NUMBER   IN VARCHAR2,
IN_MSG_TYPE                     IN VARCHAR2,
IN_MESSAGE_TYPE                 IN VARCHAR2,
IN_ORD_OR_MSG_OR_ID_LEVEL       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

--Main Query variable
v_Sql                   VARCHAR2(4000);

-- sql variable
v_Sql_Mercury           VARCHAR2(2000) := ' SELECT * FROM mercury.mercury ';
v_Sql_Venus             VARCHAR2(2000) := ' SELECT * FROM venus.venus ';

-- where clause variables
v_Where_Mercury         VARCHAR2(2000) := ' WHERE ' ;
v_Where_Venus           VARCHAR2(2000) := ' WHERE ' ;

-- order by clause variables
v_Order_By_Mercury      VARCHAR2(2000) := ' ORDER BY mercury_order_number, created_on ' ;
v_Order_By_Venus        VARCHAR2(2000) := ' ORDER BY venus_order_number, created_on ' ;


BEGIN


  CASE Nvl(IN_MESSAGE_TYPE,'NULL')

    --**************************************************
    -- Search for a Mercury
    --**************************************************
    WHEN 'Mercury' THEN

      --If searching by ID 
      IF IN_ORD_OR_MSG_OR_ID_LEVEL = 'ID' 
      THEN
        IF IN_MERCURY_VENUS_ID is null OR IN_MERCURY_VENUS_ID = ''
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Mercury - IN_MERCURY_VENUS_ID cannot be null');
        ELSE
          v_Where_Mercury := v_Where_Mercury || ' mercury_id = ''' || IN_MERCURY_VENUS_ID || '''';
        END IF;


      --If searching by MSG
      ELSIF IN_ORD_OR_MSG_OR_ID_LEVEL = 'MSG' 
      THEN
        IF IN_MERCURY_MESSAGE_NUMBER is not null AND IN_MERCURY_VENUS_ORDER_NUMBER is not null
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Mercury - IN_MERCURY_MESSAGE_NUMBER and IN_MERCURY_VENUS_ORDER_NUMBER are both provided.  You can search by one only');
        ELSIF (IN_MERCURY_MESSAGE_NUMBER is null OR IN_MERCURY_MESSAGE_NUMBER = '') AND 
              (IN_MERCURY_VENUS_ORDER_NUMBER is null OR IN_MERCURY_VENUS_ORDER_NUMBER = '')
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Mercury - IN_MERCURY_MESSAGE_NUMBER and IN_MERCURY_VENUS_ORDER_NUMBER are both empty.  You must provide one');
        ELSIF IN_MERCURY_MESSAGE_NUMBER is not null
        THEN
          v_Where_Mercury := v_Where_Mercury || ' mercury_message_number = ''' || IN_MERCURY_MESSAGE_NUMBER || '''';
          IF IN_MSG_TYPE is not null
          THEN 
            v_Where_Mercury := v_Where_Mercury || ' AND msg_type in (' || IN_MSG_TYPE || ')';
          END IF; 
        ELSE
          v_Where_Mercury := v_Where_Mercury || ' mercury_order_number = ''' || IN_MERCURY_VENUS_ORDER_NUMBER || '''';
          IF IN_MSG_TYPE is not null
          THEN 
            v_Where_Mercury := v_Where_Mercury || ' AND msg_type in (' || IN_MSG_TYPE || ')';
          END IF; 
        END IF;


      --If searching by ORD
      ELSIF IN_ORD_OR_MSG_OR_ID_LEVEL = 'ORD' 
      THEN
        IF IN_REFERENCE_NUMBER IS NULL OR IN_REFERENCE_NUMBER = ''
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Mercury - IN_REFERENCE_NUMBER cannot be null');
        ELSE
          v_Where_Mercury := v_Where_Mercury || ' mercury_order_number in (SELECT distinct m1.mercury_order_number FROM mercury.mercury m1 WHERE m1.reference_number = ''' || IN_REFERENCE_NUMBER || ''')';
          IF IN_MSG_TYPE is not null
          THEN 
            v_Where_Mercury := v_Where_Mercury || ' AND msg_type in (' || IN_MSG_TYPE || ')';
          END IF; 
        END IF;

      --Else, invalid parm passed. Throw an error
      ELSE
        RAISE_APPLICATION_ERROR(-20001, 'Mercury - Invalid IN_ORD_OR_MSG_OR_ID_LEVEL passed');
      END IF; 


      v_Sql := v_Sql_Mercury || v_Where_Mercury || v_Order_By_Mercury; 

    --**************************************************
    -- Search for a Venus
    --**************************************************
    WHEN 'Venus' THEN

      --If searching by ID 
      IF IN_ORD_OR_MSG_OR_ID_LEVEL = 'ID' 
      THEN
        IF IN_MERCURY_VENUS_ID is null OR IN_MERCURY_VENUS_ID = ''
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Venus - IN_MERCURY_VENUS_ID cannot be null');
        ELSE
          v_Where_Venus := v_Where_Venus || ' venus_id = ''' || IN_MERCURY_VENUS_ID || '''';
        END IF;


      --If searching by MSG
      ELSIF IN_ORD_OR_MSG_OR_ID_LEVEL = 'MSG' 
      THEN
        IF IN_MERCURY_VENUS_ORDER_NUMBER is null OR IN_MERCURY_VENUS_ORDER_NUMBER = ''
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Venus - IN_MERCURY_VENUS_ORDER_NUMBER cannot be null');
        ELSE
          v_Where_Venus := v_Where_Venus || ' venus_order_number = ''' || IN_MERCURY_VENUS_ORDER_NUMBER || '''';
          IF IN_MSG_TYPE is not null
          THEN 
            v_Where_Venus := v_Where_Venus || ' AND msg_type in (' || IN_MSG_TYPE || ')';
          END IF; 
        END IF;


      --If searching by ORD
      ELSIF IN_ORD_OR_MSG_OR_ID_LEVEL = 'ORD' 
      THEN
        IF IN_REFERENCE_NUMBER IS NULL OR IN_REFERENCE_NUMBER = ''
        THEN
          RAISE_APPLICATION_ERROR(-20001, 'Venus - IN_REFERENCE_NUMBER cannot be null');
        ELSE
          v_Where_Venus := v_Where_Venus || ' venus_order_number in (SELECT distinct v1.venus_order_number FROM venus.venus v1 WHERE v1.reference_number = ''' || IN_REFERENCE_NUMBER || ''')';
          IF IN_MSG_TYPE is not null
          THEN 
            v_Where_Venus := v_Where_Venus || ' AND msg_type in (' || IN_MSG_TYPE || ')';
          END IF; 
        END IF;

      --Else, invalid parm passed. Throw an error
      ELSE
        RAISE_APPLICATION_ERROR(-20001, 'Venus - Invalid IN_ORD_OR_MSG_OR_ID_LEVEL passed');
      END IF; 

      v_Sql := v_Sql_Venus || v_Where_Venus || v_Order_By_Venus; 


    --**************************************************
    -- Throw an exception
    --**************************************************
    WHEN 'NULL' THEN
      RAISE_APPLICATION_ERROR(-20001, 'Correct IN_MESSAGE_TYPE is required');

  END CASE; 

  OPEN OUT_CUR FOR v_Sql; 

END GET_MESSAGE_DETAILS_GENERIC;


FUNCTION HAS_FLORIST_REJECTED_ORDER
(
IN_ORDER_DETAIL_ID   IN VARCHAR2,
IN_FILLING_FLORIST   IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
This function returns a Y/N indicator based on whether or not the given florist has rejected the given order

Input:
order_detail_id
filling florist

Output:
Y/N if the given florist has rejected the given order

-----------------------------------------------------------------------------*/
CURSOR check_cur IS
SELECT  'Y'
FROM mercury.mercury m
WHERE m.msg_type = 'FTD' and
m.reference_number = IN_ORDER_DETAIL_ID  and
m.filling_florist = IN_FILLING_FLORIST
AND EXISTS
      (
        select 1
        from mercury.mercury m1
        where m1.msg_type = 'REJ'
        and m1.sending_florist = m.filling_florist
        and m1.mercury_order_number = m.mercury_order_number
      );

v_return        varchar2(1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_return;
CLOSE check_cur;

RETURN v_return;

END HAS_FLORIST_REJECTED_ORDER;

PROCEDURE IS_DELIVERY_CONFIRMATION_TEXT
(
IN_MESSAGE_TEXT                 IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
) AS

BEGIN

  out_status := 'N';

  SELECT distinct('Y') into out_status
  from mercury.auto_dcon_key_phrase kp1
  where INSTR(UPPER(in_message_text), UPPER(kp1.key_phrase_txt), 1, 1) > 0
  and kp1.include_exclude_flag = 'I'
  and not exists (
    SELECT distinct('Y')
    from mercury.auto_dcon_key_phrase kp2
    where INSTR(UPPER(in_message_text), UPPER(kp2.key_phrase_txt), 1, 1) > 0
    and kp2.include_exclude_flag = 'E'
  );

  EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END IS_DELIVERY_CONFIRMATION_TEXT;

PROCEDURE GET_LIFECYCLE_BY_ID
(
IN_MERCURY_ID                   IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
        select to_char(ls.status_timestamp, 'mm/dd/yyyy hh:mi:ssAM') status_timestamp,
        losv.order_status_desc order_status,
        status_txt, status_url,
        decode(status_url, null, 'N', case when sysdate - ls.status_timestamp <= gp1.value then 'Y' else 'N' end) status_url_active,
        decode(status_url, null, null, case when sysdate - ls.status_timestamp <= gp1.value then null else gp2.value end) status_url_expired_msg
        from mercury.lifecycle_status ls
        join mercury.lifecycle_order_status_val losv
        on losv.order_status_code = ls.order_status_code
        left outer join frp.global_parms gp1
        on gp1.context = 'ORDER_LIFECYCLE'
        and gp1.name = 'LIFECYCLE_DELIVERY_EXPIRED_DAYS'
        left outer join frp.global_parms gp2
        on gp2.context = 'ORDER_LIFECYCLE'
        and gp2.name = 'LIFECYCLE_DELIVERY_EXPIRED_MSG'
        where ls.mercury_id = in_mercury_id
        order by ls.status_timestamp;


END GET_LIFECYCLE_BY_ID;

PROCEDURE GET_LAST_FTD_FROM_REF_NUM
(
IN_ORDER_DETAIL_ID              IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

CURSOR merc_cur IS
        SELECT  m.mercury_id
        FROM    mercury.mercury m
        WHERE   m.reference_number = in_order_detail_id
        AND     m.msg_type = 'FTD'
        ORDER BY m.created_on DESC;

v_message_id            varchar2(20);

BEGIN

        OPEN merc_cur;
        FETCH merc_cur INTO v_message_id;
        CLOSE merc_cur;

        OPEN out_cur FOR
                SELECT  m.first_choice,
                        m.second_choice,
                        m.card_message,
                        m.occasion,
                        m.price,
                        m.special_instructions,
                        m.operator
                FROM    mercury.mercury m
                WHERE   m.mercury_id = v_message_id;

END GET_LAST_FTD_FROM_REF_NUM;

END;
.
/

