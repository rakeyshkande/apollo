CREATE OR REPLACE
PACKAGE BODY CLEAN.QUEUE_DELETE_PKG
AS

PROCEDURE GET_QUEUE_DELETE_RECORD
(
IN_QUEUE_DELETE_DETAIL_ID       IN  QUEUE_DELETE_DETAIL.QUEUE_DELETE_DETAIL_ID%TYPE,
OUT_QUEUE_DELETE_HEADER_CUR     OUT TYPES.REF_CURSOR,
OUT_QUEUE_DELETE_DETAIL_CUR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves queue delete record.

Input:
        queue_delete_detail_id         number

Output:
        ref cursor that contains queue delete header information
        ref cursor that contains queue delete detail information

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_QUEUE_DELETE_HEADER_CUR FOR
        SELECT  qdh.queue_delete_header_id,qdh.batch_description, qdh.batch_count, qdh.batch_processed_flag, qdh.include_partner_name,
                qdh.exclude_partner_name, qdh.delete_queue_msg_flag, qdh.delete_queue_msg_type, qdh.send_message_flag, qdh.message_type,
                qdh.ansp_amount, qdh.give_florist_asking_price, qdh.cancel_reason_code, qdh.text_or_reason, qdh.resubmit_order_flag,
                qdh.include_comment_flag, qdh.comment_text, qdh.send_email_flag, qdh.email_title, qdh.refund_order_flag, qdh.refund_disp_code,
                qdh.responsible_party, qdh.delete_batch_by_type, qdh.created_by, qdh.email_body, qdh.email_subject
        FROM    queue_delete_header qdh, queue_delete_detail qd
        WHERE   qd.queue_delete_detail_id = IN_QUEUE_DELETE_DETAIL_ID
        and     qd.queue_delete_header_id = qdh.queue_delete_header_id
        and     qd.QUEUE_DELETE_HEADER_ID = qdh.QUEUE_DELETE_HEADER_ID;

OPEN OUT_QUEUE_DELETE_DETAIL_CUR FOR
        SELECT  queue_delete_detail_id, queue_delete_header_id, message_id, external_order_number, processed_status, error_text, created_by
        FROM    queue_delete_detail
        WHERE   queue_delete_detail_id = IN_QUEUE_DELETE_DETAIL_ID;

END GET_QUEUE_DELETE_RECORD;

/*------------------------------------------------------------------------------
FUNCTION IS_DUPLICATE_MESSAGE_ID

Description:
    This function returns if the given message id is a duplicate request

Input:
    queue_delete_detail_id, queue_delete_header_id, message_id

Output:
    varchar2 - Y/N

------------------------------------------------------------------------------*/

FUNCTION IS_DUPLICATE_MESSAGE_ID
(
IN_QUEUE_DELETE_DETAIL_ID       IN QUEUE_DELETE_DETAIL.QUEUE_DELETE_DETAIL_ID%TYPE,
IN_QUEUE_DELETE_HEADER_ID       IN QUEUE_DELETE_DETAIL.QUEUE_DELETE_HEADER_ID%TYPE,
IN_MESSAGE_ID   		IN QUEUE_DELETE_DETAIL.MESSAGE_ID%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_min_queue_delete_detail_id    number := 0;
v_duplicate_message_flag  	varchar2 (1) := 'N';

BEGIN

  --check if message id passed in is a duplicate within it's own batch
  select min(qdd.queue_delete_detail_id)
  into v_min_queue_delete_detail_id
  from clean.queue_delete_detail qdd
  where qdd.message_id = IN_MESSAGE_ID
  and qdd.queue_delete_header_id = IN_QUEUE_DELETE_HEADER_ID;
  
  IF IN_QUEUE_DELETE_DETAIL_ID <> v_min_queue_delete_detail_id THEN
    v_duplicate_message_flag := 'Y';
  END IF;
  
  IF v_duplicate_message_flag <> 'Y' THEN
    --check if message id passed in is a duplicate within some other batch
    --that is currently processing
    v_min_queue_delete_detail_id := 0;
    select min(qdd.queue_delete_detail_id)
    into v_min_queue_delete_detail_id
    from clean.queue_delete_detail qdd
    where qdd.message_id = IN_MESSAGE_ID
    and qdd.processed_status = 'N'
    and qdd.queue_delete_header_id <> IN_QUEUE_DELETE_HEADER_ID;
    
    IF IN_QUEUE_DELETE_DETAIL_ID > v_min_queue_delete_detail_id THEN
      v_duplicate_message_flag := 'Y';
    END IF;  
  END IF;
  
  RETURN v_duplicate_message_flag;

END IS_DUPLICATE_MESSAGE_ID;

/*------------------------------------------------------------------------------
                        FUNCTION IS_DUPLICATE_ORDER_NUMBER

Description:
    This function returns if the given external order number is a duplicate request

Input:
    queue_delete_detail_id, queue_delete_header_id, external_order_number

Output:
    varchar2 - Y/N

------------------------------------------------------------------------------*/

FUNCTION IS_DUPLICATE_ORDER_NUMBER
(
IN_QUEUE_DELETE_DETAIL_ID       IN QUEUE_DELETE_DETAIL.QUEUE_DELETE_DETAIL_ID%TYPE,
IN_QUEUE_DELETE_HEADER_ID       IN QUEUE_DELETE_DETAIL.QUEUE_DELETE_HEADER_ID%TYPE,
IN_EXTERNAL_ORDER_NUMBER	IN QUEUE_DELETE_DETAIL.EXTERNAL_ORDER_NUMBER%TYPE
)
RETURN VARCHAR2
AS

-- variables
v_min_queue_delete_detail_id    number := 0;
v_duplicate_message_flag  	varchar2 (1) := 'N';

BEGIN

  --check if external order number passed in is a duplicate within it's own batch
  select min(qdd.queue_delete_detail_id)
  into v_min_queue_delete_detail_id
  from clean.queue_delete_detail qdd
  where qdd.external_order_number = IN_EXTERNAL_ORDER_NUMBER
  and qdd.queue_delete_header_id = IN_QUEUE_DELETE_HEADER_ID;
  
  IF IN_QUEUE_DELETE_DETAIL_ID <> v_min_queue_delete_detail_id THEN
    v_duplicate_message_flag := 'Y';
  END IF;
  
  IF v_duplicate_message_flag <> 'Y' THEN
    --check if external order number passed in is a duplicate within
    --some other batch that is currently processing
    v_min_queue_delete_detail_id := 0;
    select min(qdd.queue_delete_detail_id)
    into v_min_queue_delete_detail_id
    from clean.queue_delete_detail qdd
    where qdd.external_order_number = IN_EXTERNAL_ORDER_NUMBER
    and qdd.processed_status = 'N'
    and qdd.queue_delete_header_id <> IN_QUEUE_DELETE_HEADER_ID;
    
    IF IN_QUEUE_DELETE_DETAIL_ID > v_min_queue_delete_detail_id THEN
      v_duplicate_message_flag := 'Y';
    END IF;  
  END IF;
  
  RETURN v_duplicate_message_flag;

END IS_DUPLICATE_ORDER_NUMBER;


PROCEDURE INSERT_QUEUE_DELETE_HEADER
(
IN_BATCH_DESCRIPTION		      QUEUE_DELETE_HEADER.BATCH_DESCRIPTION%TYPE,
IN_BATCH_COUNT			          QUEUE_DELETE_HEADER.BATCH_COUNT%TYPE,
IN_BATCH_PROCESSED_FLAG       QUEUE_DELETE_HEADER.BATCH_PROCESSED_FLAG%TYPE,
IN_DELETE_QUEUE_MSG_FLAG		  QUEUE_DELETE_HEADER.DELETE_QUEUE_MSG_FLAG%TYPE,
IN_DELETE_QUEUE_MSG_TYPE		  QUEUE_DELETE_HEADER.DELETE_QUEUE_MSG_TYPE%TYPE,
IN_SEND_MESSAGE_FLAG		      QUEUE_DELETE_HEADER.SEND_MESSAGE_FLAG%TYPE,
IN_MESSAGE_TYPE			          QUEUE_DELETE_HEADER.MESSAGE_TYPE%TYPE,
IN_ANSP_AMOUNT			          QUEUE_DELETE_HEADER.ANSP_AMOUNT%TYPE,
IN_GIVE_FLORIST_ASKING_PRICE	QUEUE_DELETE_HEADER.GIVE_FLORIST_ASKING_PRICE%TYPE,
IN_CANCEL_REASON_CODE		      QUEUE_DELETE_HEADER.CANCEL_REASON_CODE%TYPE,
IN_TEXT_OR_REASON			        QUEUE_DELETE_HEADER.TEXT_OR_REASON%TYPE,
IN_RESUBMIT_ORDER_FLAG		    QUEUE_DELETE_HEADER.RESUBMIT_ORDER_FLAG%TYPE,
IN_INCLUDE_COMMENT_FLAG		    QUEUE_DELETE_HEADER.INCLUDE_COMMENT_FLAG%TYPE,
IN_COMMENT_TEXT			          QUEUE_DELETE_HEADER.COMMENT_TEXT%TYPE,
IN_SEND_EMAIL_FLAG			      QUEUE_DELETE_HEADER.SEND_EMAIL_FLAG%TYPE,
IN_EMAIL_TITLE			          QUEUE_DELETE_HEADER.EMAIL_TITLE%TYPE,
IN_REFUND_ORDER_FLAG		      QUEUE_DELETE_HEADER.REFUND_ORDER_FLAG%TYPE,
IN_REFUND_DISP_CODE		        QUEUE_DELETE_HEADER.REFUND_DISP_CODE%TYPE,
IN_RESPONSIBLE_PARTY		      QUEUE_DELETE_HEADER.RESPONSIBLE_PARTY%TYPE,
IN_DELETE_BATCH_BY_TYPE		    QUEUE_DELETE_HEADER.DELETE_BATCH_BY_TYPE%TYPE,
IN_CREATED_BY			            QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
IN_INCLUDE_PARTNER_NAME       QUEUE_DELETE_HEADER.INCLUDE_PARTNER_NAME%TYPE,
IN_EXCLUDE_PARTNER_NAME       QUEUE_DELETE_HEADER.EXCLUDE_PARTNER_NAME%TYPE,
IN_EMAIL_SUBJECT              QUEUE_DELETE_HEADER.EMAIL_SUBJECT%TYPE,
IN_EMAIL_BODY                 QUEUE_DELETE_HEADER.EMAIL_BODY%TYPE,
OUT_QUEUE_DELETE_HEADER_ID	  OUT NUMBER,
OUT_STATUS               			OUT VARCHAR2,
OUT_ERROR_MESSAGE        		  OUT VARCHAR2
)
AS

BEGIN

INSERT INTO QUEUE_DELETE_HEADER
(QUEUE_DELETE_HEADER_ID, batch_description, batch_count, batch_processed_flag, include_partner_name, exclude_partner_name,
delete_queue_msg_flag, delete_queue_msg_type, send_message_flag ,message_type, ansp_amount, give_florist_asking_price, cancel_reason_code,
text_or_reason, resubmit_order_flag, include_comment_flag, comment_text, send_email_flag, email_title, email_subject, email_body, refund_order_flag, refund_disp_code,
responsible_party, delete_batch_by_type, created_on, created_by, updated_on, updated_by)
values
(QUEUE_DELETE_HEADER_SQ.nextval, IN_BATCH_DESCRIPTION, IN_BATCH_COUNT, IN_BATCH_PROCESSED_FLAG, IN_INCLUDE_PARTNER_NAME, IN_EXCLUDE_PARTNER_NAME,
IN_DELETE_QUEUE_MSG_FLAG, IN_DELETE_QUEUE_MSG_TYPE, IN_SEND_MESSAGE_FLAG, IN_MESSAGE_TYPE, IN_ANSP_AMOUNT, IN_GIVE_FLORIST_ASKING_PRICE, IN_CANCEL_REASON_CODE,
IN_TEXT_OR_REASON, IN_RESUBMIT_ORDER_FLAG, IN_INCLUDE_COMMENT_FLAG, IN_COMMENT_TEXT, IN_SEND_EMAIL_FLAG, IN_EMAIL_TITLE, IN_EMAIL_SUBJECT, IN_EMAIL_BODY, IN_REFUND_ORDER_FLAG, IN_REFUND_DISP_CODE,
IN_RESPONSIBLE_PARTY, IN_DELETE_BATCH_BY_TYPE, SYSDATE, IN_CREATED_BY, SYSDATE, IN_CREATED_BY)
    RETURNING QUEUE_DELETE_HEADER_ID INTO OUT_QUEUE_DELETE_HEADER_ID;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_QUEUE_DELETE_HEADER;

PROCEDURE INSERT_QUEUE_DELETE_DETAIL
(
IN_QUEUE_DELETE_HEADER_ID		      QUEUE_DELETE_DETAIL.QUEUE_DELETE_HEADER_ID%TYPE,
IN_MESSAGE_ID			                QUEUE_DELETE_DETAIL.MESSAGE_ID%TYPE,
IN_EXTERNAL_ORDER_NUMBER	        QUEUE_DELETE_DETAIL.EXTERNAL_ORDER_NUMBER%TYPE,
IN_PROCESSED_STATUS			          QUEUE_DELETE_DETAIL.PROCESSED_STATUS%TYPE,
IN_ERROR_TEXT			                QUEUE_DELETE_DETAIL.ERROR_TEXT%TYPE,
IN_CREATED_BY			                QUEUE_DELETE_DETAIL.CREATED_BY%TYPE,
OUT_QUEUE_DELETE_DETAIL_ID        OUT NUMBER,
OUT_STATUS               			    OUT VARCHAR2,
OUT_ERROR_MESSAGE        		      OUT VARCHAR2
)
AS

v_message_id    varchar2 (20) := null;

BEGIN

IF IN_MESSAGE_ID <> 0 THEN
  v_message_id := IN_MESSAGE_ID;
END IF;



INSERT INTO QUEUE_DELETE_DETAIL
     (queue_delete_detail_id, queue_delete_header_id, message_id, external_order_number, processed_status, error_text, created_on, created_by, updated_on, updated_by)
    VALUES
     (QUEUE_DELETE_DETAIL_SQ.nextval, IN_QUEUE_DELETE_HEADER_ID, v_message_id, IN_EXTERNAL_ORDER_NUMBER, IN_PROCESSED_STATUS, IN_ERROR_TEXT, SYSDATE, IN_CREATED_BY,
     SYSDATE, IN_CREATED_BY)
    RETURNING queue_delete_detail_id INTO OUT_QUEUE_DELETE_DETAIL_ID;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_QUEUE_DELETE_DETAIL;


PROCEDURE UPDATE_BATCH_PROCESSED_FLAG
(
IN_QUEUE_DELETE_HEADER_ID		QUEUE_DELETE_HEADER.QUEUE_DELETE_HEADER_ID%TYPE,
IN_BATCH_PROCESSED_FLAG		  QUEUE_DELETE_HEADER.BATCH_PROCESSED_FLAG%TYPE,
IN_UPDATED_BY			          QUEUE_DELETE_HEADER.UPDATED_BY%TYPE,
OUT_STATUS               		OUT VARCHAR2,
OUT_MESSAGE	        		OUT VARCHAR2
)
AS

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE QUEUE_DELETE_HEADER SET
  BATCH_PROCESSED_FLAG = IN_BATCH_PROCESSED_FLAG,
  UPDATED_BY = IN_UPDATED_BY,
  UPDATED_ON = SYSDATE
WHERE QUEUE_DELETE_HEADER_ID = IN_QUEUE_DELETE_HEADER_ID;

commit;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_BATCH_PROCESSED_FLAG [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_BATCH_PROCESSED_FLAG;

PROCEDURE UPDATE_QUEUE_DELETE_DETAIL
(
IN_QUEUE_DELETE_DETAIL_ID		QUEUE_DELETE_DETAIL.QUEUE_DELETE_DETAIL_ID%TYPE,
IN_PROCESSED_STATUS			    QUEUE_DELETE_DETAIL.PROCESSED_STATUS%TYPE,
IN_ERROR_TEXT		  		QUEUE_DELETE_DETAIL.ERROR_TEXT%TYPE,
IN_UPDATED_BY			          QUEUE_DELETE_DETAIL.UPDATED_BY%TYPE,
OUT_STATUS               		OUT VARCHAR2,
OUT_MESSAGE	        		OUT VARCHAR2
)
AS

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE QUEUE_DELETE_DETAIL SET
  PROCESSED_STATUS = IN_PROCESSED_STATUS,
  ERROR_TEXT = IN_ERROR_TEXT,
  UPDATED_BY = IN_UPDATED_BY,
  UPDATED_ON = SYSDATE
WHERE QUEUE_DELETE_DETAIL_ID = IN_QUEUE_DELETE_DETAIL_ID;

commit;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_QUEUE_DELETE_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPDATE_QUEUE_DELETE_DETAIL;

PROCEDURE GET_Q_DELETE_HEADER_WITH_COUNT
(
  IN_USER_ID                  IN QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
  IN_QUEUE_DELETE_HEADER_ID   IN QUEUE_DELETE_HEADER.QUEUE_DELETE_HEADER_ID%TYPE,
  OUT_CURSOR 	                OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------------------
Description:
  This stored procedure retrieves queue delete header record with count of details.
Input:
  IN_USER_ID                  IN QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
  IN_DELETE_HEADER_ID         IN QUEUE_DELETE_HEADER.QUEUE_DELETE_HEADER_ID%TYPE,
Output:
  OUT_CURSOR ref cursor that contains queue delete header with details count information
------------------------------------------------------------------------------------------*/
  v_hours NUMBER :=FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('QUEUE_CONFIG','BATCH_HISTORY_HOURS');
  v_header_sql VARCHAR2(6000) := '';
  v_header_sql_select VARCHAR2(500) := 'SELECT CQH.QUEUE_DELETE_HEADER_ID, CQH.BATCH_DESCRIPTION, CQH.BATCH_COUNT, to_char(CQH.CREATED_ON, ''yyyy-mm-dd HH24:MI:SS'') CREATED_ON, CQH.CREATED_BY,
                                        CQD_TP.TOTAL_PROCESSED, CQD_TS.TOTAL_SUCCESS, CQD_TF.TOTAL_FAILURE, CQD_TD.TOTAL_DUPLICATE ';
  v_header_sql_from VARCHAR2(4000) := ' FROM CLEAN.QUEUE_DELETE_HEADER CQH,
                                        (SELECT queue_delete_header_id, count(*) as TOTAL_SUCCESS FROM CLEAN.QUEUE_DELETE_DETAIL CQD
                                        WHERE CQD.queue_delete_header_id = queue_delete_header_id
                                        AND UPPER(CQD.processed_status)=UPPER(''SUCCESS'') GROUP BY queue_delete_header_id)CQD_TS,
                                        (SELECT queue_delete_header_id, count(*) as TOTAL_FAILURE FROM CLEAN.QUEUE_DELETE_DETAIL CQD
                                        WHERE CQD.queue_delete_header_id = queue_delete_header_id
                                        AND UPPER(CQD.processed_status)=UPPER(''FAILURE'') GROUP BY queue_delete_header_id)CQD_TF,
                                        (SELECT queue_delete_header_id, count(* )as TOTAL_DUPLICATE FROM CLEAN.QUEUE_DELETE_DETAIL CQD
                                        WHERE CQD.queue_delete_header_id = queue_delete_header_id
                                        AND upper(CQD.processed_status)=upper(''DUPLICATE'') GROUP BY queue_delete_header_id)CQD_TD,
                                        (SELECT queue_delete_header_id, count(*) as TOTAL_PROCESSED FROM CLEAN.QUEUE_DELETE_DETAIL CQD
                                        WHERE CQD.queue_delete_header_id = queue_delete_header_id
                                        AND (CQD.processed_status IS NOT NULL AND CQD.processed_status != ''N'') GROUP BY queue_delete_header_id)CQD_TP ';

  v_delete_header_where VARCHAR2(1000) := '';
  v_delete_header_order VARCHAR2(200) := ' ORDER BY CREATED_ON DESC';

BEGIN
  v_delete_header_where := ' WHERE CQH.CREATED_ON > sysdate - ' || v_hours * 60 / 1440;

  IF IN_USER_ID IS NOT NULL AND IN_USER_ID !='All' THEN
    v_delete_header_where := v_delete_header_where || ' AND UPPER(CQH.CREATED_BY)=UPPER('''|| IN_USER_ID ||''')';
  END IF;
  IF IN_QUEUE_DELETE_HEADER_ID != 0 THEN
    v_delete_header_where := v_delete_header_where || ' AND CQH.QUEUE_DELETE_HEADER_ID='||IN_QUEUE_DELETE_HEADER_ID;
  END IF;
  v_delete_header_where := v_delete_header_where || ' AND CQH.queue_delete_header_id=CQD_TS.queue_delete_header_id(+)
                                                      AND CQH.queue_delete_header_id=CQD_TF.queue_delete_header_id(+)
                                                      AND CQH.queue_delete_header_id=CQD_TD.queue_delete_header_id(+)
                                                      AND CQH.queue_delete_header_id=CQD_TP.queue_delete_header_id(+)';

  v_header_sql := v_header_sql_select || v_header_sql_from || v_delete_header_where || v_delete_header_order;

  OPEN OUT_CURSOR FOR v_header_sql;

END GET_Q_DELETE_HEADER_WITH_COUNT;


PROCEDURE GET_Q_DELETE_HEADER_DETAIL
(
  IN_USER_ID                  IN QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
  IN_DELETE_HEADER_ID         IN QUEUE_DELETE_HEADER.QUEUE_DELETE_HEADER_ID%TYPE,
  OUT_HEADER 	                OUT TYPES.REF_CURSOR,
  OUT_DETAIL 	                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves queue delete header and detail records.
Input:
        IN_USER_ID                  IN QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
        IN_DELETE_HEADER_ID         IN QUEUE_DELETE_HEADER.QUEUE_DELETE_HEADER_ID%TYPE,
Output:
        OUT_HEADER ref_cursor that contains queue delete header information
        OUT_DETAIL ref_cursor that contains queue delete details information
--------------------------------------------------------------------------------*/
v_hours NUMBER :=FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('QUEUE_CONFIG','BATCH_HISTORY_HOURS');

v_header_sql VARCHAR2(4000) := '';
v_header_where VARCHAR2(400) := '';
v_header_order VARCHAR2(200) := ' ORDER BY CREATED_ON DESC';
v_detail_sql VARCHAR2(4000):='';

BEGIN
   v_header_sql := 'SELECT QUEUE_DELETE_HEADER_ID, BATCH_DESCRIPTION, BATCH_COUNT, to_char(CREATED_ON, ''yyyy-mm-dd HH24:MI:SS'') CREATED_ON, CREATED_BY
                    FROM CLEAN.QUEUE_DELETE_HEADER WHERE CREATED_ON > sysdate - ' || v_hours * 60 / 1440;

   v_detail_sql :='SELECT MESSAGE_ID, EXTERNAL_ORDER_NUMBER, PROCESSED_STATUS, ERROR_TEXT
                              FROM CLEAN.QUEUE_DELETE_DETAIL WHERE queue_delete_header_id='|| IN_DELETE_HEADER_ID ||'
                              ORDER by PROCESSED_STATUS, MESSAGE_ID, EXTERNAL_ORDER_NUMBER';

  IF IN_USER_ID IS NOT NULL THEN
    v_header_where := v_header_where ||' AND UPPER(CREATED_BY)=UPPER('''|| IN_USER_ID ||''')';
  END IF;
  IF IN_DELETE_HEADER_ID IS NOT NULL THEN
    v_header_where := v_header_where || ' AND QUEUE_DELETE_HEADER_ID='||IN_DELETE_HEADER_ID;
  END IF;

    v_header_sql := v_header_sql || v_header_where || v_header_order;

    OPEN OUT_HEADER FOR v_header_sql;
    OPEN OUT_DETAIL FOR v_detail_sql;

END GET_Q_DELETE_HEADER_DETAIL;

PROCEDURE GET_USERS_FROM_Q_DELETE_HEADER
(
  OUT_CURSOR 	                OUT TYPES.REF_CURSOR
)
AS
v_header_sql VARCHAR2(1000) := '';
v_hours NUMBER :=FRP.MISC_PKG.GET_GLOBAL_PARM_VALUE('QUEUE_CONFIG','BATCH_HISTORY_HOURS');

/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves queue delete header and detail records.
Input:
        IN_USER_ID                  IN QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
        IN_DELETE_HEADER_ID         IN QUEUE_DELETE_HEADER.QUEUE_DELETE_HEADER_ID%TYPE,
Output:
        OUT_HEADER ref_cursor that contains queue delete header information
        OUT_DETAIL ref_cursor that contains queue delete details information
--------------------------------------------------------------------------------*/

BEGIN
    v_header_sql := 'SELECT DISTINCT CREATED_BY FROM CLEAN.QUEUE_DELETE_HEADER WHERE CREATED_ON > sysdate - ' || v_hours * 60 / 1440;
    OPEN OUT_CURSOR FOR v_header_sql;

END GET_USERS_FROM_Q_DELETE_HEADER;

PROCEDURE GET_QUEUE_DELETE_USER_INFO
(
IN_CSR_ID                  	IN QUEUE_DELETE_HEADER.CREATED_BY%TYPE,
OUT_CURSOR 	                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves queue delete csr first name and last name.

Input:
        user_id         varchar2

Output:
        ref cursor that contains queue delete csr first name and last name

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CURSOR FOR
        SELECT u.first_name, u.last_name
	FROM aas.identity i, aas.users u
	WHERE i.IDENTITY_ID = IN_CSR_ID
	AND i.USER_ID = u.USER_ID;

END GET_QUEUE_DELETE_USER_INFO;

PROCEDURE GET_QUEUE_DELETE_DETAIL_IDS
(
IN_QUEUE_DELETE_HEADER_ID       IN QUEUE_DELETE_DETAIL.QUEUE_DELETE_HEADER_ID%TYPE,
OUT_CURSOR 	                OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves queue delete detail ids for
        give queue delete header id.

Input:
        queue_delete_header_id         varchar2

Output:
        ref cursor that contains queue delete detail ids for
        give queue delete header id

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CURSOR FOR
        SELECT queue_delete_detail_id
	FROM queue_delete_detail
	WHERE queue_delete_header_id = IN_QUEUE_DELETE_HEADER_ID;

END GET_QUEUE_DELETE_DETAIL_IDS;

END;
/