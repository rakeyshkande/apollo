CREATE OR REPLACE
PACKAGE BODY clean.FLOWER_JEWEL_PKG AS

PROCEDURE GET_UNACKNOWLEDGED_ORDERS
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
    SELECT FJ.FLOWER_JEWELS_ID,
        FJ.ORDER_ADD_ON_ID,
        FJ.TRANSMISSION_TIMESTAMP,
        FJ.ACKNOWLEDGEMENT_TIMESTAMP,
        FJ.SHIP_CONFIRMATION_TIMESTAMP,
		FJ.VENDOR_SHIP_DATE,
		FJ.VENDOR_DELIVERY_DATE,
		FJ.CARRIER_ID,
		FJ.SHIP_METHOD,
		FJ.TRACKING_NUMBER
	FROM FLOWER_JEWELS FJ
    WHERE FJ.ACKNOWLEDGEMENT_TIMESTAMP is null;

END GET_UNACKNOWLEDGED_ORDERS;

PROCEDURE GET_ORDERS_FOR_TRANSMISSION
(
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
    select oao.order_add_on_id,
        nvl(a.product_id, oao.add_on_code) add_on_code,
        oao.add_on_quantity,
        od.external_order_number,
        od.delivery_date,
        c.first_name,
        c.last_name,
        c.business_name,
        c.address_1,
        c.address_2,
        c.city,
        c.state,
        c.zip_code,
        c.country,
        cpd.phone_number day_phone,
        cpd.extension day_extension,
        cpe.phone_number evening_phone,
        cpe.extension evening_extension
    from clean.order_add_ons oao,
        clean.order_details od,
        ftd_apps.addon a,
        ftd_apps.addon_type at,
        clean.customer c
    left outer join clean.customer_phones cpd
        on c.customer_id = cpd.customer_id
        and cpd.phone_type = 'Day'
    left outer join clean.customer_phones cpe
        on c.customer_id = cpe.customer_id
        and cpe.phone_type = 'Evening'
    where oao.add_on_code = a.addon_id
    and a.addon_type = at.addon_type_id
    and at.description = 'Jewelry'
    and oao.order_detail_id = od.order_detail_id
    and od.op_status = 'Processed'
    and 
        ((select sum(ob.add_on_amount) from clean.order_bills ob
            where ob.order_detail_id = oao.order_detail_id)
        -
        nvl((select sum(r.refund_addon_amount) from clean.refund r
            where r.order_detail_id = oao.order_detail_id), 0)) > 0
    and not exists (
        select 'Y' from clean.flower_jewels fj
        where fj.order_add_on_id = oao.order_add_on_id)
    and c.customer_id = od.recipient_id
    order by od.order_detail_id, oao.order_add_on_id;

END GET_ORDERS_FOR_TRANSMISSION;

PROCEDURE INSERT_FLOWER_JEWELS
(
IN_ORDER_ADD_ON_ID              IN  FLOWER_JEWELS.ORDER_ADD_ON_ID%TYPE,
OUT_FLOWER_JEWELS_ID            OUT FLOWER_JEWELS.FLOWER_JEWELS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    INSERT INTO FLOWER_JEWELS (
        FLOWER_JEWELS_ID,
        ORDER_ADD_ON_ID)
    VALUES (
        FLOWER_JEWELS_ID.NEXTVAL,
        IN_ORDER_ADD_ON_ID) RETURNING FLOWER_JEWELS_ID INTO OUT_FLOWER_JEWELS_ID;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_FLOWER_JEWELS;

PROCEDURE GET_FLOWER_JEWEL_DETAILS
(
IN_FLOWER_JEWELS_ID             IN  FLOWER_JEWELS.FLOWER_JEWELS_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS

BEGIN

    OPEN OUT_CUR FOR
    SELECT FJ.FLOWER_JEWELS_ID,
        FJ.ORDER_ADD_ON_ID,
        FJ.TRANSMISSION_TIMESTAMP,
        FJ.ACKNOWLEDGEMENT_TIMESTAMP,
        FJ.SHIP_CONFIRMATION_TIMESTAMP,
        OD.ORDER_DETAIL_ID,
        OD.EXTERNAL_ORDER_NUMBER,
        OD.DELIVERY_DATE,
        OD.ORDER_GUID,
        OD.RECIPIENT_ID
    FROM FLOWER_JEWELS FJ,
        ORDER_ADD_ONS OAO,
        ORDER_DETAILS OD
    WHERE FJ.FLOWER_JEWELS_ID = IN_FLOWER_JEWELS_ID
    AND OAO.ORDER_ADD_ON_ID = FJ.ORDER_ADD_ON_ID
    and OD.ORDER_DETAIL_ID = OAO.ORDER_DETAIL_ID;

END GET_FLOWER_JEWEL_DETAILS;

PROCEDURE UPDATE_FLOWER_JEWELS_TRANS
(
IN_FLOWER_JEWELS_ID             IN  FLOWER_JEWELS.FLOWER_JEWELS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    UPDATE FLOWER_JEWELS
    SET TRANSMISSION_TIMESTAMP = SYSDATE
    WHERE FLOWER_JEWELS_ID = IN_FLOWER_JEWELS_ID;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLOWER_JEWELS_TRANS;

PROCEDURE UPDATE_FLOWER_JEWELS_ACK
(
IN_FLOWER_JEWELS_ID             IN  FLOWER_JEWELS.FLOWER_JEWELS_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    UPDATE FLOWER_JEWELS
    SET ACKNOWLEDGEMENT_TIMESTAMP = SYSDATE
    WHERE FLOWER_JEWELS_ID = IN_FLOWER_JEWELS_ID;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLOWER_JEWELS_ACK;

PROCEDURE UPDATE_FLOWER_JEWELS_SHIP
(
IN_FLOWER_JEWELS_ID             IN  FLOWER_JEWELS.FLOWER_JEWELS_ID%TYPE,
IN_TRACKING_NUMBER              IN  FLOWER_JEWELS.TRACKING_NUMBER%TYPE,
IN_CARRIER_ID                   IN  FLOWER_JEWELS.CARRIER_ID%TYPE,
IN_SHIP_METHOD                  IN  FLOWER_JEWELS.SHIP_METHOD%TYPE,
IN_VENDOR_SHIP_DATE             IN  FLOWER_JEWELS.VENDOR_SHIP_DATE%TYPE,
IN_VENDOR_DELIVERY_DATE         IN  FLOWER_JEWELS.VENDOR_DELIVERY_DATE%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

    UPDATE FLOWER_JEWELS
    SET ship_confirmation_TIMESTAMP = SYSDATE,
	TRACKING_NUMBER = IN_TRACKING_NUMBER,
	SHIP_METHOD = IN_SHIP_METHOD,
	CARRIER_ID = IN_CARRIER_ID,
	VENDOR_DELIVERY_DATE = IN_VENDOR_DELIVERY_DATE,
	VENDOR_SHIP_DATE = IN_VENDOR_SHIP_DATE
    WHERE FLOWER_JEWELS_ID = IN_FLOWER_JEWELS_ID;

    OUT_STATUS := 'Y';

    EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLOWER_JEWELS_SHIP;

END;
.
/
