CREATE OR REPLACE TRIGGER clean.order_hold_$
AFTER INSERT OR UPDATE OR DELETE ON clean.order_hold 
REFERENCING OLD AS old NEW AS new 
FOR EACH ROW
DECLARE
   v_current_timestamp timestamp;
BEGIN
   v_current_timestamp := current_timestamp;

   IF INSERTING THEN
      insert into clean.order_hold$ 
           (ORDER_DETAIL_ID,
            REASON,         
            CREATED_ON,     
            CREATED_BY,     
            UPDATED_ON,     
            UPDATED_BY,     
            TIMEZONE,       
            AUTH_COUNT,     
            RELEASE_DATE,   
            STATUS,         
            REASON_TEXT,    
            OPERATION$,     
            TIMESTAMP$)     
      VALUES       
           (:NEW.ORDER_DETAIL_ID,
            :NEW.REASON,         
            :NEW.CREATED_ON,     
            :NEW.CREATED_BY,     
            :NEW.UPDATED_ON,     
            :NEW.UPDATED_BY,     
            :NEW.TIMEZONE,       
            :NEW.AUTH_COUNT,     
            :NEW.RELEASE_DATE,   
            :NEW.STATUS,         
            :NEW.REASON_TEXT,    
            'INS',     
            v_current_timestamp);     

   ELSIF UPDATING  THEN
      insert into clean.order_hold$ 
           (ORDER_DETAIL_ID,
            REASON,         
            CREATED_ON,     
            CREATED_BY,     
            UPDATED_ON,     
            UPDATED_BY,     
            TIMEZONE,       
            AUTH_COUNT,     
            RELEASE_DATE,   
            STATUS,         
            REASON_TEXT,    
            OPERATION$,     
            TIMESTAMP$)     
      VALUES       
           (:OLD.ORDER_DETAIL_ID,
            :OLD.REASON,         
            :OLD.CREATED_ON,     
            :OLD.CREATED_BY,     
            :OLD.UPDATED_ON,     
            :OLD.UPDATED_BY,     
            :OLD.TIMEZONE,       
            :OLD.AUTH_COUNT,     
            :OLD.RELEASE_DATE,   
            :OLD.STATUS,         
            :OLD.REASON_TEXT,    
            'UPD_OLD',     
            v_current_timestamp);     

      insert into clean.order_hold$ 
           (ORDER_DETAIL_ID,
            REASON,         
            CREATED_ON,     
            CREATED_BY,     
            UPDATED_ON,     
            UPDATED_BY,     
            TIMEZONE,       
            AUTH_COUNT,     
            RELEASE_DATE,   
            STATUS,         
            REASON_TEXT,    
            OPERATION$,     
            TIMESTAMP$)     
      VALUES       
           (:NEW.ORDER_DETAIL_ID,
            :NEW.REASON,         
            :NEW.CREATED_ON,     
            :NEW.CREATED_BY,     
            :NEW.UPDATED_ON,     
            :NEW.UPDATED_BY,     
            :NEW.TIMEZONE,       
            :NEW.AUTH_COUNT,     
            :NEW.RELEASE_DATE,   
            :NEW.STATUS,         
            :NEW.REASON_TEXT,    
            'UPD_NEW',     
            v_current_timestamp);     


   ELSIF DELETING  THEN

      insert into clean.order_hold$ 
           (ORDER_DETAIL_ID,
            REASON,         
            CREATED_ON,     
            CREATED_BY,     
            UPDATED_ON,     
            UPDATED_BY,     
            TIMEZONE,       
            AUTH_COUNT,     
            RELEASE_DATE,   
            STATUS,         
            REASON_TEXT,    
            OPERATION$,     
            TIMESTAMP$)     
      VALUES       
           (:OLD.ORDER_DETAIL_ID,
            :OLD.REASON,         
            :OLD.CREATED_ON,     
            :OLD.CREATED_BY,     
            :OLD.UPDATED_ON,     
            :OLD.UPDATED_BY,     
            :OLD.TIMEZONE,       
            :OLD.AUTH_COUNT,     
            :OLD.RELEASE_DATE,   
            :OLD.STATUS,         
            :OLD.REASON_TEXT,    
            'DEL',     
            v_current_timestamp);     

   END IF;

END;
/
