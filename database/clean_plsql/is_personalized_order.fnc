CREATE OR REPLACE FUNCTION CLEAN.IS_PERSONALIZED_ORDER (
    in_order_detail_id            VARCHAR2
)
RETURN VARCHAR2
AS
    template_id    varchar2(50) := '';
    return_field   varchar2(2)  := 'N';
BEGIN
    BEGIN
        Select personalization_template_id
        Into template_id
        From Clean.Order_details od join
             FTD_APPS.Product_master pm on od.product_id = pm.product_id
        WHERE od.order_detail_id = in_order_detail_id;
    
        EXCEPTION WHEN NO_DATA_FOUND THEN
           return_field := 'N';
    END;
    
    IF(template_id is not null AND template_id <> 'NONE') THEN
       return_field := 'Y';
    ELSE
       return_field := 'N';
    END IF;
    
    RETURN return_field;
END;
.
/

grant execute on clean.is_personalized_order to osp;
