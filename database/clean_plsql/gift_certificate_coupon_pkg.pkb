CREATE OR REPLACE
PACKAGE BODY clean.GIFT_CERTIFICATE_COUPON_PKG
AS


  PROCEDURE INSERT_GC_COUPON_RECIPIENT
  (
   IN_REQUEST_NUMBER       IN VARCHAR2,
   IN_FIRST_NAME           IN VARCHAR2,
   IN_LAST_NAME            IN VARCHAR2,
   IN_ADDRESS_1            IN VARCHAR2,
   IN_ADDRESS_2            IN VARCHAR2,
   IN_CITY                 IN VARCHAR2,
   IN_STATE                IN VARCHAR2,
   IN_ZIP_CODE             IN VARCHAR2,
   IN_COUNTRY              IN VARCHAR2,
   IN_PHONE                IN VARCHAR2,
   IN_EMAIL_ADDRESS        IN VARCHAR2,
   OUT_GC_COUPON_RECIP_ID OUT NUMBER,
   OUT_STATUS             OUT VARCHAR2,
   OUT_ERROR_MESSAGE      OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure inserts a record into table gc_coupon_recipients.

  Input:
          request number, first name, last name,
          address line 1, address line 2,
          city, state, zip, country,
          phone number, email address

  Output:
          gc_coupon_recip_id
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    INSERT INTO gc_coupon_recipients
        (gc_coupon_recip_id,
         request_number,
         first_name,
         last_name,
         address_1,
         address_2,
         city,
         state,
         zip_code,
         country,
         phone,
         email_address,
         created_on,
         created_by,
         updated_on,
         updated_by)
      VALUES
        (gc_coupon_recip_id_sq.NEXTVAL,
         in_request_number,
         in_first_name,
         in_last_name,
         in_address_1,
         in_address_2,
         in_city,
         in_state,
         in_zip_code,
         in_country,
         in_phone,
         in_email_address,
         SYSDATE,
         USER,
         SYSDATE,
         USER) RETURNING gc_coupon_recip_id INTO out_gc_coupon_recip_id;

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END INSERT_GC_COUPON_RECIPIENT;


  PROCEDURE GET_GCCREC_BY_GCCNUM_OR_REQNUM
  (
   IN_GC_COUPON_NUMBER IN VARCHAR2,
   IN_REQUEST_NUMBER IN VARCHAR2,
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure gets records from table gc_coupon_recipients
          along with any related gc_coupons for the request number or coupon
          number passed in

  Input:
          request number     VARCHAR2
          coupon number      VARCHAR2

  Output:
          cursor containing gc_coupon_recipients and gc_coupons records

  -----------------------------------------------------------------------------*/

  BEGIN

    IF (in_gc_coupon_number IS NULL) THEN
      OPEN OUT_CURSOR FOR
        SELECT gccr.gc_coupon_recip_id,
               gccr.request_number,
               gcc.gc_coupon_number,
               gccr.first_name,
               gccr.last_name,
               gccr.address_1,
               gccr.address_2,
               gccr.city,
               gccr.state,
               gccr.zip_code,
               gccr.country,
               gccr.phone,
               gccr.email_address
          FROM gc_coupons gcc,
               gc_coupon_recipients gccr
         WHERE gccr.request_number = in_request_number
           AND gcc.gc_coupon_recip_id (+) = gccr.gc_coupon_recip_id
         ORDER BY gccr.gc_coupon_recip_id;
    ELSE
      OPEN OUT_CURSOR FOR
        SELECT gccr.gc_coupon_recip_id,
               gccr.request_number,
               gcc.gc_coupon_number,
               gccr.first_name,
               gccr.last_name,
               gccr.address_1,
               gccr.address_2,
               gccr.city,
               gccr.state,
               gccr.zip_code,
               gccr.country,
               gccr.phone,
               gccr.email_address
          FROM gc_coupon_recipients gccr,
               gc_coupons gcc
         WHERE gcc.gc_coupon_number = in_gc_coupon_number
           AND gccr.gc_coupon_recip_id = gcc.gc_coupon_recip_id;
    END IF;

  END GET_GCCREC_BY_GCCNUM_OR_REQNUM;


  PROCEDURE GET_GCC_AND_GCCREC_BY_REQNUM
  (
   IN_REQUEST_NUMBER IN VARCHAR2,
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure gets records from table gc_coupons along with
          any related gc_coupon_recipients for the request number passed in

  Input:
          request number   VARCHAR2

  Output:
          cursor containing gc_coupons and gc_coupon_recipients records

  -----------------------------------------------------------------------------*/

  BEGIN

    OPEN OUT_CURSOR FOR
        SELECT gccr.gc_coupon_recip_id,
               gccr.request_number,
               gcc.gc_coupon_number,
               gccr.first_name,
               gccr.last_name,
               gccr.address_1,
               gccr.address_2,
               gccr.city,
               gccr.state,
               gccr.zip_code,
               gccr.country,
               gccr.phone,
               gccr.email_address
          FROM gc_coupon_recipients gccr,
               gc_coupons gcc
         WHERE gcc.request_number = in_request_number
           AND gcc.gc_coupon_recip_id = gccr.gc_coupon_recip_id (+)
         ORDER BY gccr.gc_coupon_recip_id;

  END GET_GCC_AND_GCCREC_BY_REQNUM;


  PROCEDURE UPDATE_GC_COUPON_RECIPIENT
  (
   IN_GC_COUPON_RECIP_ID IN NUMBER,
   IN_REQUEST_NUMBER     IN VARCHAR2,
   IN_FIRST_NAME         IN VARCHAR2,
   IN_LAST_NAME          IN VARCHAR2,
   IN_ADDRESS_1          IN VARCHAR2,
   IN_ADDRESS_2          IN VARCHAR2,
   IN_CITY               IN VARCHAR2,
   IN_STATE              IN VARCHAR2,
   IN_ZIP_CODE           IN VARCHAR2,
   IN_COUNTRY            IN VARCHAR2,
   IN_PHONE              IN VARCHAR2,
   IN_EMAIL_ADDRESS      IN VARCHAR2,
   OUT_STATUS            OUT VARCHAR2,
   OUT_ERROR_MESSAGE     OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates a record table gc_coupon_recipients
          by gc_coupon_recip_id. However field request_number is not updatable.

  Input:
          gc_coupon_recip_id, request_number, first name, last name, address line 1,
          address line 2, city, state, zip, country, phone number, email address

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    UPDATE gc_coupon_recipients
      SET request_number = in_request_number,
          first_name     = in_first_name,
          last_name      = in_last_name,
          address_1      = in_address_1,
          address_2      = in_address_2,
          city           = in_city,
          state          = in_state,
          zip_code       = in_zip_code,
          country        = in_country,
          phone          = in_phone,
          email_address = in_email_address,
          updated_on = SYSDATE,
          updated_by =  USER
     WHERE gc_coupon_recip_id = in_gc_coupon_recip_id;

    IF SQL%FOUND THEN
      out_status := 'Y';
    ELSE
      out_status := 'N';
      out_error_message := 'WARNING: No records found for gc_coupon_recip_id ' || in_gc_coupon_recip_id ||'.';
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GC_COUPON_RECIPIENT;


  PROCEDURE GET_GC_COUPON_TYPES
  (
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure gets the records from table gc_coupon_type_val.

  Input:
          N/A

  Output:
          cursor containing gc_coupon_type_val records

  -----------------------------------------------------------------------------*/

  BEGIN

    OPEN OUT_CURSOR FOR
      SELECT gc_coupon_type,
             description,
             status
        FROM gc_coupon_type_val;

  END GET_GC_COUPON_TYPES;


  FUNCTION GET_NEXT_REQUEST_NUMBER
  (
   IN_PREFIX IN CHAR
  )
  RETURN VARCHAR2
  IS
  /*------------------------------------------------------------------------------
  Description:
     Returns the next gc_coupon_request request number, however the input
       parameter wil be prefixed to the number if one is passed in.

  Input:
          prefix                CHAR

  Output:
          request number        VARCHAR2
  ------------------------------------------------------------------------------*/
  v_request_number NUMBER;

  BEGIN

     SELECT gc_coupon_request_number_sq.NEXTVAL
       INTO v_request_number
       FROM DUAL;

     RETURN TRIM(in_prefix) || TRIM(TO_CHAR(v_request_number));

  END GET_NEXT_REQUEST_NUMBER;


  FUNCTION GENERATE_GC_COUPON_NUMBER
  (
   IN_PREFIX   IN VARCHAR2
  )
  RETURN GC_COUPON_NUMBERS.GC_COUPON_NUMBER%TYPE
  IS
  /*------------------------------------------------------------------------------
    Description:
       Returns the next gc_coupon number.
    ------------------------------------------------------------------------------*/

  v_coupon_number GC_COUPON_NUMBERS.GC_COUPON_NUMBER%TYPE;
  v_prefix varchar2(10);
  v_has_alpha number(1) := 0;

  BEGIN
  
    -- If prefix does not contain alpha then add 'CP' to prefix.
    IF in_prefix is null THEN
    	v_prefix := 'CP';
    ELSE 
    	select regexp_instr(in_prefix,'[[:alpha:]]') into v_has_alpha from dual;
    	IF v_has_alpha = 0 THEN
        	v_prefix := 'CP' || in_prefix;
    	ELSE 
    		v_prefix := in_prefix;
    	END IF;
    END IF;

    SELECT NVL(v_prefix,'') || to_char(coupon_number_sq.nextval) || substr (to_char(systimestamp, 'FF'), 1, 4)
      INTO v_coupon_number
      FROM DUAL;

    INSERT INTO gc_coupon_numbers
        (gc_coupon_number,
         status,
         created_on,
         updated_on)
      VALUES
        (v_coupon_number,
         'New',
         SYSDATE,
         SYSDATE);

    RETURN v_coupon_number;

  END GENERATE_GC_COUPON_NUMBER;


  PROCEDURE INSERT_GC_COUPONS
  (
   IN_REQUEST_NUMBER     IN VARCHAR2,
   IN_GC_COUPON_RECIP_ID IN NUMBER,
   IN_ISSUE_AMOUNT       IN NUMBER,
   IN_ISSUE_DATE         IN DATE,
   IN_EXPIRATION_DATE    IN DATE,
   IN_COUPON_STATUS      IN VARCHAR2,
   IN_PREFIX             IN VARCHAR2,
   IN_GC_PARTNER_SEQUENCE IN VARCHAR2,
   OUT_STATUS           OUT VARCHAR2,
   OUT_ERROR_MESSAGE    OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure inserts a record into table gc_coupons.

  Input:
          gc_coupon_number, request number, gc_coupon_recip_id,
          issue amount, issue date, expiration date, coupon status,
          prefix, partner_sequence

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  v_gc_coupon_number GC_COUPONS.GC_COUPON_NUMBER%TYPE;
  v_gc_partner_sequence integer;

  cursor seq_cur is
        select  gc_partner_sequence_sq.nextval
        from    dual;

  BEGIN

    v_gc_coupon_number := GENERATE_GC_COUPON_NUMBER(in_prefix);

    if in_gc_partner_sequence = 'Y' then
        open seq_cur;
        fetch seq_cur into v_gc_partner_sequence;
        close seq_cur;
    end if;

    INSERT INTO gc_coupons
        (gc_coupon_number,
         request_number,
         gc_coupon_recip_id,
         issue_amount,
         issue_date,
         expiration_date,
         gc_coupon_status,
         created_on,
         created_by,
         updated_on,
         updated_by,
         gc_partner_sequence)
      VALUES
        (v_gc_coupon_number,
         in_request_number,
         in_gc_coupon_recip_id,
         in_issue_amount,
         in_issue_date,
         in_expiration_date,
         in_coupon_status,
         SYSDATE,
         USER,
         SYSDATE,
         USER,
         v_gc_partner_sequence);

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END INSERT_GC_COUPONS;


  PROCEDURE INSERT_GC_COUPON_REQUEST
  (
   IN_REQUEST_NUMBER           IN VARCHAR2,
   IN_GC_COUPON_TYPE           IN VARCHAR2,
   IN_ISSUE_AMOUNT             IN NUMBER,
   IN_PAID_AMOUNT              IN NUMBER,
   IN_ISSUE_DATE               IN DATE,
   IN_EXPIRATION_DATE          IN DATE,
   IN_QUANTITY                 IN NUMBER,
   IN_FORMAT                   IN CHAR,
   IN_FILENAME                 IN VARCHAR2,
   IN_REQUESTED_BY             IN VARCHAR2,
   IN_ORDER_SOURCE             IN VARCHAR2,
   IN_PROGRAM_NAME             IN VARCHAR2,
   IN_PROGRAM_DESC             IN VARCHAR2,
   IN_PROGRAM_COMMENT          IN VARCHAR2,
   IN_COMPANY_ID               IN VARCHAR2,
   IN_PREFIX                   IN VARCHAR2,
   IN_OTHER                    IN VARCHAR2,
   IN_PROMOTION_ID             IN VARCHAR2,
   IN_GC_PARTNER_PROGRAM_NAME  IN VARCHAR2,
   IN_CREATED_BY					 IN VARCHAR2,
   OUT_STATUS                 OUT VARCHAR2,
   OUT_ERROR_MESSAGE          OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure inserts a record into table gc_coupon_request and
          one or more records into table gc_coupons depending on the value of
          in_quantity.

  Input:
          request number, fgc_coupon_type, issue_amount, paid_amount,
          issue_date, expiration_date, quantity, format, filename,
          requested_by, order_source, program_name, program_desc, program_comment,
          company_id, source_code, prefix, other,
          issue amount, issue date, expiration date, promotion_id,
          gc_partner_program_name, created_by

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  v_coupon_status   gc_coupons.gc_coupon_status%TYPE;

  cursor partner_cur is
        select  'Y'
        from    gc_program_config p
        join    gc_config_parm c
        on      p.gc_config_parm_id = c.gc_config_parm_id
        where   p.gc_partner_program_name = in_gc_partner_program_name
        and     c.parm_name = 'partner_sequence'
        and     p.parm_value = 'Y';

  v_gc_partner_sequence   char(1) := 'N';

  BEGIN

    INSERT INTO gc_coupon_request
        (request_number,
         gc_coupon_type,
         gc_partner_program_name,
         issue_amount,
         paid_amount,
         issue_date,
         expiration_date,
         quantity,
         format,
         filename,
         requested_by,
         order_source,
         program_name,
         program_desc,
         program_comment,
         company_id,        
         prefix,
         other,
         promotion_id,
         created_on,
         created_by,
         updated_on,
         updated_by)
      VALUES
        (in_request_number,
         in_gc_coupon_type,
         in_gc_partner_program_name,
         in_issue_amount,
         in_paid_amount,
         in_issue_date,
         in_expiration_date,
         in_quantity,
         in_format,
         in_filename,
         in_requested_by,
         in_order_source,
         in_program_name,
         in_program_desc,
         in_program_comment,
         in_company_id,         
         in_prefix,
         in_other,
         in_promotion_id,
         SYSDATE,
         UPPER(in_created_by),
         SYSDATE,
         UPPER(in_created_by));

    IF in_quantity = 0 THEN
       out_status := 'Y';
    ELSE
       IF in_gc_partner_program_name IS NULL THEN
         v_coupon_status := 'Issued Active';
       ELSE
         v_coupon_status := 'Issued Inactive';
       END IF;

       open partner_cur;
       fetch partner_cur into v_gc_partner_sequence;
       close partner_cur;

       FOR loop_counter IN 1 .. in_quantity
        LOOP
          INSERT_GC_COUPONS (in_request_number, NULL, in_issue_amount, in_issue_date, in_expiration_date, v_coupon_status, in_prefix, v_gc_partner_sequence, out_status, out_error_message);
        END LOOP;
    END IF;

    IF out_status <> 'N' THEN
       out_status := 'Y';
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END INSERT_GC_COUPON_REQUEST;


  PROCEDURE UPDATE_GC_COUPON_N_GCC_REQUEST
  (
   IN_REQUEST_NUMBER  IN VARCHAR2,
   IN_REQUESTED_BY    IN VARCHAR2,
   IN_PROGRAM_NAME    IN VARCHAR2,
   IN_PROGRAM_DESC    IN VARCHAR2,
   IN_PROGRAM_COMMENT IN VARCHAR2,
   IN_PROMOTION_ID    IN VARCHAR2,
   IN_GCC_STATUS      IN VARCHAR2,
   IN_UPDATED_BY      IN VARCHAR2,
   OUT_CURSOR         OUT TYPES.REF_CURSOR,
   OUT_STATUS         OUT VARCHAR2,
   OUT_ERROR_MESSAGE  OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates a record in table gc_coupon_request.
          This stored procedure updates the status of records in table
            gc_coupons for a given request number and then returns a list
            of all the coupons that were updated.

  Input:
          request number
          requested_by
          program_name
          program_desc
          program_comment,
          promotion_id
          gcc_status
          updated_by

  Output:
          status
          error message
          cursor containing gc_coupons' coupon numbers

  -----------------------------------------------------------------------------*/
  v_updated_on DATE := SYSDATE;

  BEGIN

    UPDATE gc_coupon_request
       SET requested_by =    in_requested_by,
           program_name =    in_program_name,
           program_desc =    in_program_desc,
           program_comment = in_program_comment,
           promotion_id    = in_promotion_id,
           updated_on =      SYSDATE,
           updated_by =      UPPER(in_updated_by)
      WHERE request_number = in_request_number;

    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_error_message := 'WARNING: No gc_coupon_request records found for request number ' || in_request_number ||'.';
    END IF;


    IF in_gcc_status IS NULL THEN
      UPDATE gc_coupons gcc
         SET gcc.updated_on = v_updated_on,
             gcc.updated_by = USER
       WHERE request_number = in_request_number
         AND (
              (gcc.gc_coupon_status ='Reinstate'
               AND
               EXISTS (SELECT 1 FROM gc_coupon_request
                                     WHERE request_number = gcc.request_number
                                      AND gc_coupon_type ='GC'))
                    OR
                    (gcc.gc_coupon_status ='Issued Active')
                   );
    ELSE
      UPDATE gc_coupons gcc
         SET gcc.gc_coupon_status = in_gcc_status,
             gcc.updated_on = v_updated_on,
             gcc.updated_by = USER
       WHERE request_number = in_request_number
         AND (
              (gcc.gc_coupon_status ='Reinstate'
               AND
               EXISTS (SELECT 1 FROM gc_coupon_request
                                     WHERE request_number = gcc.request_number
                                      AND gc_coupon_type ='GC'))
                    OR
                    (gcc.gc_coupon_status ='Issued Active')
                   );
    END IF;

    OPEN OUT_CURSOR FOR
      SELECT gc_coupon_number,
             gc_coupon_status
        FROM gc_coupons
       WHERE updated_on = v_updated_on;


    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       IF out_status = 'N' THEN
         out_error_message := 'WARNING: No gc_coupons nor gc_coupon_request records found for request number ' || in_request_number ||'.';
       ELSE
         out_status := 'N';
         out_error_message := 'WARNING: gc_coupon_request updated but no gc_coupons records found for request number ' || in_request_number ||'.';
       END IF;
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GC_COUPON_N_GCC_REQUEST;


  PROCEDURE GET_GCCREQ_BY_REQNUM_OR_GCCNUM
  (
   IN_REQUEST_NUMBER   IN VARCHAR2,
   IN_GC_COUPON_NUMBER IN VARCHAR2,
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure retrieves a record from table gc_coupon_request
          for a particular request number, however if its not supplied then it
          returns the join of records from table gc_coupon_request and table
          gc_coupons for a particular gc_coupon_number.

  Input:
          request number, gc_coupon_number

  Output:
          cursor containing gc_coupon_request records or the records from the
            join of gc_coupon_request and gc_coupons
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    IF (in_request_number IS NOT NULL) THEN
      OPEN OUT_CURSOR FOR
        SELECT request_number,
               gc_coupon_type,
               gc_partner_program_name,
               issue_amount,
               paid_amount,
               issue_date,
               expiration_date,
               quantity,
               format,
               filename,
               requested_by,
               order_source,
               program_name,
               program_desc,
               program_comment,
               company_id,              
               prefix,
               other,
               promotion_id,
               created_on
          FROM gc_coupon_request
         WHERE request_number = in_request_number;
    ELSE
      OPEN OUT_CURSOR FOR
        SELECT gcc.gc_coupon_number,
               gcc.request_number,
               gcc.gc_coupon_recip_id,
               gcc.gc_coupon_status,
               gccr.gc_coupon_type,
               gcc.issue_amount,
               gccr.paid_amount,
               gccr.issue_date,
               gcc.expiration_date,
               gccr.quantity,
               gccr.format,
               gccr.filename,
               gccr.requested_by,
               gccr.order_source,
               gccr.program_name,
               gccr.program_desc,
               gccr.program_comment,
               gccr.company_id,               
               gccr.prefix,
               gccr.other,
               gccr.promotion_id,
               gcc.created_on,
               gccr.gc_partner_program_name
          FROM gc_coupons gcc, gc_coupon_request gccr
         WHERE gcc.gc_coupon_number = in_gc_coupon_number
           AND gccr.request_number = gcc.request_number;
    END IF;

  END GET_GCCREQ_BY_REQNUM_OR_GCCNUM;


  PROCEDURE GET_DISPLAYABLE_GCC_STATUSES
  (
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
   /*-----------------------------------------------------------------------------
    Description:
          This stored procedure gets the records from table gc_coupon_status_val.

    Input:
            N/A

    Output:
            cursor containing gc_coupon_status records

    -----------------------------------------------------------------------------*/

    BEGIN

      OPEN OUT_CURSOR FOR
        SELECT gc_coupon_status,
               description,
               status
          FROM gc_coupon_status_val
         WHERE display_indicator = 'Y';

    END GET_DISPLAYABLE_GCC_STATUSES;


  PROCEDURE GET_GCC_BY_REQ_NUM_OR_GCC_NUM
  (
   IN_REQUEST_NUMBER     IN VARCHAR2,
   IN_GC_COUPON_NUMBER   IN VARCHAR2,
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure retrieves records from table gc_coupons by
          request number, if its not null, or by gc/coupon number.

  Input:
          request number, gc_coupon_number

  Output:
          cursor containing gc_coupons

  -----------------------------------------------------------------------------*/

  BEGIN

    IF (in_request_number IS NOT NULL) THEN
      OPEN OUT_CURSOR FOR
        SELECT gc_coupon_number,
               request_number,
               gc_coupon_recip_id,
               gc_coupon_status
          FROM gc_coupons
         WHERE request_number = in_request_number
         ORDER BY gc_coupon_recip_id;
    ELSE
      OPEN OUT_CURSOR FOR
        SELECT gc_coupon_number,
               request_number,
               gc_coupon_recip_id,
               gc_coupon_status
          FROM gc_coupons
         WHERE gc_coupon_number = in_gc_coupon_number;
    END IF;

  END GET_GCC_BY_REQ_NUM_OR_GCC_NUM;

  PROCEDURE GET_RESTRICTED_SOURCE_CODES
  (
   IN_REQUEST_NUMBER     IN VARCHAR2,   
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure retrieves records from table gc_coupon_source by
          request number

  Input:
          request number

  Output:
          cursor containing restricted source codes

  -----------------------------------------------------------------------------*/

  BEGIN

    IF (in_request_number IS NOT NULL) THEN
      OPEN OUT_CURSOR FOR
        SELECT source_code
          FROM gc_coupon_source
         WHERE request_number = in_request_number
         ORDER BY source_code;
    
    END IF;

  END GET_RESTRICTED_SOURCE_CODES;


  PROCEDURE GET_GC_COUPON_STATUS_COUNT
  (
   IN_REQUEST_NUMBER     IN VARCHAR2,
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          Returns a cursor with list of count of gc_coupons with each status for
          the named request.

  Input:
          status

  Output:
          cursor containing gc_coupons status and its count

  -----------------------------------------------------------------------------*/

  BEGIN

    OPEN OUT_CURSOR FOR
      SELECT gccsv.gc_coupon_status, count(gcc.gc_coupon_status) status_count
        FROM gc_coupons gcc,
             gc_coupon_status_val gccsv
       WHERE gccsv.gc_coupon_status = gcc.gc_coupon_status (+)
         AND gcc.request_number (+) = in_request_number
      GROUP BY gccsv.gc_coupon_status;

  END GET_GC_COUPON_STATUS_COUNT;


  PROCEDURE UPDATE_GCC_STATUS_N_RECIP_ID
  (
   IN_GC_COUPON_NUMBER   IN VARCHAR2,
   IN_GC_COUPON_RECIP_ID IN NUMBER,
   IN_GC_COUPON_STATUS   IN VARCHAR2,
   OUT_STATUS           OUT VARCHAR2,
   OUT_ERROR_MESSAGE    OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the status and recipient id of a record
          in table gc_coupons for a given coupon number.

  Input:
          gc coupon number, gc coupon recip id, gc_coupon_status

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    IF in_gc_coupon_recip_id = 0 THEN
      UPDATE gc_coupons
         SET gc_coupon_status   = in_gc_coupon_status,
             updated_on = SYSDATE,
             updated_by = USER
       WHERE gc_coupon_number = in_gc_coupon_number;
    ELSE
       UPDATE gc_coupons
          SET gc_coupon_recip_id = in_gc_coupon_recip_id,
              gc_coupon_status   = in_gc_coupon_status,
              updated_on = SYSDATE,
              updated_by = USER
       WHERE gc_coupon_number = in_gc_coupon_number;
    END IF;

    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_error_message := 'WARNING: No records found for coupon number ' || in_gc_coupon_number ||'.';
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
     ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GCC_STATUS_N_RECIP_ID;


  PROCEDURE UPDATE_ACTIVE_GCC_TO_EXPIRE
  (
   IN_DATE            IN DATE,
   OUT_STATUS        OUT VARCHAR2,
   OUT_ERROR_MESSAGE OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the status of records in table gc_coupons
            from 'Issued Active' to 'Issued Expired' when the expiration date
            is less than or equal to the date passed in.

  Input:
          date

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    UPDATE gc_coupons gcc
       SET gcc.gc_coupon_status = 'Issued Expired',
           gcc.updated_on = SYSDATE,
           gcc.updated_by = USER
     WHERE expiration_date <= IN_DATE
       AND gc_coupon_status IN ('Issued Active','Reinstate')
       AND EXISTS (SELECT 1 FROM gc_coupon_request
                        WHERE request_number = gcc.request_number
                          AND gc_coupon_type IN ('CP','CS'));

    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_error_message := 'WARNING: No Issued Active records found for gc_coupon number.';
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
     ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_ACTIVE_GCC_TO_EXPIRE;


  PROCEDURE INSERT_GC_COUPON_HISTORY
  (
   IN_GC_COUPON_NUMBER IN VARCHAR2,
   IN_REDEMPTION_DATE  IN DATE,
   IN_REINSTATE_DATE   IN DATE,
   IN_ORDER_DETAIL_ID  IN NUMBER,
   IN_STATUS           IN VARCHAR2,
   OUT_STATUS         OUT VARCHAR2,
   OUT_ERROR_MESSAGE  OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure inserts a record into table gc_coupon_history.

  Input:
          gc_coupon_number, redemption date, reinstate date, order detail id, status

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    INSERT INTO gc_coupon_history
        (gc_coupon_history_id,
         gc_coupon_number,
         redemption_date,
         reinstate_date,
         order_detail_id,
         status,
         created_on,
         created_by)
      VALUES
        (gc_coupon_history_id_sq.NEXTVAL,
         in_gc_coupon_number,
         in_redemption_date,
         in_reinstate_date,
         in_order_detail_id,
         in_status,
         SYSDATE,
         USER);

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END INSERT_GC_COUPON_HISTORY;


  PROCEDURE GET_GCC_HISTORY_BY_COUPON_NUM
  (
   IN_GC_COUPON_NUMBER IN VARCHAR2,
   OUT_CURSOR  OUT TYPES.REF_CURSOR
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure records from table gc_coupon_history for the
          coupon number passed in.

  Input:
          coupon number

  Output:
          cursor containing gc_coupon_history record

  -----------------------------------------------------------------------------*/

  BEGIN

    OPEN OUT_CURSOR FOR
      SELECT gc_coupon_history_id,
             gc_coupon_number,
             redemption_date,
             reinstate_date,
             order_detail_id,
             status
        FROM gc_coupon_history
       WHERE gc_coupon_number = in_gc_coupon_number
         AND status = 'Active'
         AND order_detail_id IS NOT NULL;

  END GET_GCC_HISTORY_BY_COUPON_NUM;


  PROCEDURE UPDATE_GCC_HISTORY_STATUS
  (
   IN_GC_COUPON_HISTORY_ID IN VARCHAR2,
   IN_STATUS               IN VARCHAR2,
   OUT_STATUS             OUT VARCHAR2,
   OUT_ERROR_MESSAGE      OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the status of a record in table
          gc_coupon_history.

  Input:
          gc_coupon_history_id, status

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    UPDATE gc_coupon_history
       SET status           = in_status
     WHERE gc_coupon_history_id = in_gc_coupon_history_id;

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GCC_HISTORY_STATUS;


  PROCEDURE UPDATE_GCC_REQUEST_FILENAME
  (
   IN_REQUEST_NUMBER  IN VARCHAR2,
   IN_FILENAME        IN VARCHAR2,
   IN_UPDATED_BY		 IN VARCHAR2,
   OUT_STATUS        OUT VARCHAR2,
   OUT_ERROR_MESSAGE OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates filename of a record in
          table gc_coupon_request for the request_number passed in.

  Input:
          request number
          filename

  Output:
          status
          error message

  Special Instructions:
  
  		  This procedure is not being used currently.
  -----------------------------------------------------------------------------*/

  BEGIN

    UPDATE gc_coupon_request
       SET filename =        in_filename,
           updated_on =      SYSDATE,
           updated_by =      UPPER(in_updated_by)
      WHERE request_number = in_request_number;

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GCC_REQUEST_FILENAME;

PROCEDURE REDEEM_GC_COUPON
(
IN_GC_COUPON_NUMBER             IN GC_COUPONS.GC_COUPON_NUMBER%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
IN_REDEMPTION_AMOUNT            IN GC_COUPON_HISTORY.REDEMPTION_AMOUNT%TYPE,
OUT_REDEEM_STATUS               OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the gift certificate or coupon to redeemed
        if applicable.

Input:
        gc_coupon_number        varchar2
        order_detail_id         number
        csr_id                  varchar2 - used for gc_coupon_history
        redemption_amount       number

Output:
        redeemed status         varchar2 - Invalid, Redeemed, Already Redeemed

-----------------------------------------------------------------------------*/

CURSOR status_cur IS
        SELECT  gc_coupon_status,
                issue_amount
        FROM    gc_coupons
        WHERE   gc_coupon_number = in_gc_coupon_number;

v_status        gc_coupons.gc_coupon_status%type;
v_issue_amount  gc_coupons.issue_amount%type;

BEGIN

-- check the status of the given gift certificate or coupon number
OPEN status_cur;
FETCH status_cur INTO v_status, v_issue_amount;
CLOSE status_cur;

IF v_status = 'Reinstate' OR v_status = 'Issued Active' THEN
        -- if redeemable, update the status to redeemed
        UPDATE  gc_coupons
        SET     gc_coupon_status = 'Redeemed',
                updated_on = sysdate,
                updated_by = in_csr_id
        WHERE   gc_coupon_number = in_gc_coupon_number;

        -- insert the history of the gift certificate or coupon
        INSERT INTO gc_coupon_history
        (       gc_coupon_history_id,
                gc_coupon_number,
                redemption_date,
                reinstate_date,
                order_detail_id,
                status,
                created_on,
                created_by,
                redemption_amount
        )
        VALUES
        (       gc_coupon_history_id_sq.nextval,
                in_gc_coupon_number,
                sysdate,
                null,
                in_order_detail_id,
                'Active',
                sysdate,
                in_csr_id,
                in_redemption_amount
        );

        -- set the out status to redeemed
        out_redeem_status := 'Redeemed';

ELSIF v_status = 'Redeemed' THEN
        -- set the out status to Already Redeemed when the gift certificate or coupon
        -- has already been redeemed
        out_redeem_status := 'Already Redeemed';

ELSE
        -- set the out status to Invalid when the gift certificate or coupon is not found
        -- or it's expired or cancelled.
        out_redeem_status := 'Invalid';
END IF;

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END REDEEM_GC_COUPON;



PROCEDURE REDEEM_CART_GC_COUPON
(
IN_GC_COUPON_NUMBER             IN GC_COUPONS.GC_COUPON_NUMBER%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
IN_REDEMPTION_AMOUNT            IN GC_COUPON_HISTORY.REDEMPTION_AMOUNT%TYPE,
IN_WEBOE_ORDER                  IN VARCHAR2,
OUT_REDEEM_STATUS               OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the gift certificate or coupon that was
        used on the cart.

Input:
        gc_coupon_number        varchar2
        order_detail_id         number
        csr_id                  varchar2 - used for gc_coupon_history
        redemption_amount       number

Output:
        redeemed status         varchar2 - Invalid, Redeemed, Already Redeemed

-----------------------------------------------------------------------------*/

CURSOR status_cur IS
        SELECT  gc_coupon_status,
                issue_amount
        FROM    gc_coupons
        WHERE   gc_coupon_number = in_gc_coupon_number;

v_status        gc_coupons.gc_coupon_status%type;
v_issue_amount  gc_coupons.issue_amount%type;

-- cursor to check if this order is part of a multi order cart using a gift certificate payment
CURSOR cart_cur IS
        SELECT  count (1)
        FROM    order_details od1
        WHERE   EXISTS
        (
                select  1
                from    order_details od2
                join    payments p
                on      od2.order_guid = p.order_guid
                and     p.additional_bill_id is null
                and     p.refund_id is null
                and     p.gc_coupon_number = in_gc_coupon_number
                where   od1.order_guid = od2.order_guid
                and     od2.order_detail_id = in_order_detail_id
        );

v_orders_in_cart        integer := 0;

-- get all history record for the gift certificate
-- if the gift certificate was reinstated, only get redemption records placed after the reinstate
CURSOR history_cur IS
        SELECT  max (decode (order_detail_id, in_order_detail_id, 'Y', 'N')) gcc_redeemed_on_order,
                sum (redemption_amount) redemption_amount
        FROM    gc_coupon_history
        WHERE   gc_coupon_number = in_gc_coupon_number
        AND     status = 'Active'
        AND     redemption_date >= nvl ((select max (reinstate_date) from gc_coupon_history where gc_coupon_number = in_gc_coupon_number and status = 'Active'), redemption_date);

v_gcc_redeemed_on_order char(1) := 'N';
v_redemption_amount     gc_coupon_history.redemption_amount%type;
v_remaining_amount      gc_coupon_history.redemption_amount%type;

BEGIN

-- check the status of the given gift certificate or coupon number
OPEN status_cur;
FETCH status_cur INTO v_status, v_issue_amount;
CLOSE status_cur;

IF v_status = 'Reinstate' OR v_status = 'Issued Active' THEN
        -- if redeemable, update the status to redeemed
        UPDATE  gc_coupons
        SET     gc_coupon_status = 'Redeemed',
                updated_on = sysdate,
                updated_by = in_csr_id
        WHERE   gc_coupon_number = in_gc_coupon_number;

        -- check if the given redemption amount is less than the issue amount
        IF v_issue_amount < in_redemption_amount THEN
                v_redemption_amount := v_issue_amount;
        ELSE
                v_redemption_amount := in_redemption_amount;
        END IF;

        -- insert the history of the gift certificate or coupon
        INSERT INTO gc_coupon_history
        (       gc_coupon_history_id,
                gc_coupon_number,
                redemption_date,
                reinstate_date,
                order_detail_id,
                status,
                created_on,
                created_by,
                redemption_amount
        )
        VALUES
        (       gc_coupon_history_id_sq.nextval,
                in_gc_coupon_number,
                sysdate,
                null,
                in_order_detail_id,
                'Active',
                sysdate,
                in_csr_id,
                v_redemption_amount
        );

        -- set the out status to redeemed
        out_redeem_status := 'Redeemed';

ELSIF v_status = 'Redeemed' THEN
        -- set the out status to Already Redeemed when the gift certificate or coupon
        -- has already been redeemed
        out_redeem_status := 'Already Redeemed';

        -- If the gift certificate was already marked redeemed, the gift certificate
        -- can be used on an cart that may span the bill amounts of multiple orders
        -- from WebOE order, so insert a history record for the subsequent order.
        IF in_weboe_order = 'Y' THEN

                -- First, get the number of orders in the cart.
                OPEN cart_cur;
                FETCH cart_cur INTO v_orders_in_cart;
                CLOSE cart_cur;

                -- Continue only if there are multiple orders in the cart
                -- and the given order is part of the cart that gift certificate was used
                IF v_orders_in_cart > 1 THEN

                        -- Get gift certificate amount and if the gift certificate has already been
                        -- posted for the order
                        OPEN history_cur;
                        FETCH history_cur INTO v_gcc_redeemed_on_order, v_redemption_amount;
                        CLOSE history_cur;

                        -- Continue only if the gift certificate was not already used on the order
                        IF v_gcc_redeemed_on_order = 'N' THEN

                                -- Compute the remaining usable amount left on the gift certificate
                                -- If the computed remainder is greater that zero, post the history
                                -- record for the subsequent order
                                -- Logic:
                                --      1) remaining_amount = gc issue amount - already redeemed amount
                                --      2) if remaining_amount < given redemption amount then post the remaining amount
                                --      3) otherwise post the given redemption amount

                                v_remaining_amount := v_issue_amount - v_redemption_amount;

                                IF v_remaining_amount < in_redemption_amount THEN
                                        v_redemption_amount := v_remaining_amount;
                                ELSE
                                        v_redemption_amount := in_redemption_amount;
                                END IF;

                                IF v_redemption_amount > 0 THEN
                                        INSERT INTO gc_coupon_history
                                        (       gc_coupon_history_id,
                                                gc_coupon_number,
                                                redemption_date,
                                                reinstate_date,
                                                order_detail_id,
                                                status,
                                                created_on,
                                                created_by,
                                                redemption_amount
                                        )
                                        VALUES
                                        (       gc_coupon_history_id_sq.nextval,
                                                in_gc_coupon_number,
                                                sysdate,
                                                null,
                                                in_order_detail_id,
                                                'Active',
                                                sysdate,
                                                in_csr_id,
                                                v_redemption_amount
                                        );
                                END IF;

                        END IF;
                END IF;
        END IF;

ELSE
        -- set the out status to Invalid when the gift certificate or coupon is not found
        -- or it's expired or cancelled.
        out_redeem_status := 'Invalid';
END IF;

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END REDEEM_CART_GC_COUPON;



FUNCTION CAN_BE_REINSTATED
(
IN_GC_COUPON_NUMBER             IN GC_COUPON_HISTORY.GC_COUPON_NUMBER%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure checks if a gift certificate can be reinstated

Input:
        gc_coupon_number        varchar2

Output:
        Y/N if the gift certificate can be reinistated

-----------------------------------------------------------------------------*/
-- Compare the redeemed amount from the history table to the refund payments of associated to the
-- gift certificate.  If the amounts are the same then the gift certificate can be reinstated.
CURSOR check_cur IS
        SELECT  'Y'
        FROM
        (
                select  sum (gch.redemption_amount) redemption_amount,
                        gch.order_detail_id
                from    gc_coupon_history gch
                where   gch.gc_coupon_number = in_gc_coupon_number
                and     gch.status = 'Active'
                and     gch.redemption_date >= nvl ((select max (gch1.reinstate_date) from gc_coupon_history gch1 where gch1.gc_coupon_number = in_gc_coupon_number and gch1.status = 'Active'), gch.redemption_date)
                group by gch.order_detail_id
        ) h,
        (
                select  sum (p.debit_amount - nvl (p.credit_amount, 0)) refund_amount,
                        r.order_detail_id
                from    payments p,
                        refund r
                where   p.refund_id = r.refund_id
                and     p.gc_coupon_number = in_gc_coupon_number
                and     p.payment_indicator in ('R', 'B')
                group by r.order_detail_id
        ) r
        WHERE   h.order_detail_id = r.order_detail_id
        AND     h.redemption_amount = r.refund_amount;

v_check         char (1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

RETURN v_check;

END CAN_BE_REINSTATED;



PROCEDURE EOD_REINSTATE_GCC
(
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates the gift certificate or coupon to reinstated
        if applicable.  Only gift certificates/coupons that are fully refunded
        can be marked for reinstate.

Input:
        none

Output:
        status Y/N and error message

-----------------------------------------------------------------------------*/

TYPE varchar_tab_typ IS TABLE OF VARCHAR2(20) INDEX BY PLS_INTEGER;
v_gcc_tab       varchar_tab_typ;

BEGIN

-- Update the status of the gift certificates/coupons to Reinstate
UPDATE  gc_coupons
SET     gc_coupon_status = 'Reinstate',
        updated_on = sysdate,
        updated_by = 'SYS'
WHERE   gc_coupon_status = 'Refund'
RETURNING gc_coupon_number BULK COLLECT INTO v_gcc_tab;

-- Insert the reinstate history from the redeemed history
-- for the ones that were reinstated
FORALL x IN 1..v_gcc_tab.count
        INSERT INTO gc_coupon_history
        (       gc_coupon_history_id,
                gc_coupon_number,
                redemption_date,
                reinstate_date,
                order_detail_id,
                status,
                created_on,
                created_by,
                redemption_amount
        )
        SELECT  gc_coupon_history_id_sq.nextval,
                gch.gc_coupon_number,
                null,
                sysdate,
                gch.order_detail_id,
                'Active',
                sysdate,
                'SYS',
                gch.redemption_amount
        FROM    gc_coupon_history gch
        WHERE   gch.gc_coupon_number = v_gcc_tab(x)
        AND     gch.status = 'Active'
        AND     gch.redemption_date >= nvl ((select max (gch1.reinstate_date) from gc_coupon_history gch1 where gch1.gc_coupon_number = gch.gc_coupon_number and gch1.status = 'Active'), gch.redemption_date);

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END EOD_REINSTATE_GCC;


PROCEDURE GET_OUTBOUND_GC_1
(
 IN_PROGRAM_NAME   IN GC_COUPON_REQUEST.GC_PARTNER_PROGRAM_NAME%TYPE,
 OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves records from table gc_coupon_request
        that have a partner program name as the one passed in and are not related
        to a gc_program_request_process with a confid id of 1 (coupons that
        have already been processed).

Input:
        program_name        varchar2

Output:
        cursor containing   gc_coupon_request and gc_coupons records

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CURSOR FOR
      SELECT gcr.request_number,
             gcc.gc_coupon_number,
             to_char(gcr.issue_amount, '9999D00') issue_amount,
             gcr.expiration_date
        FROM gc_coupons gcc,
             gc_coupon_request gcr
       WHERE gcc.request_number = gcr.request_number
         AND gcr.gc_partner_program_name = in_program_name
         AND NOT EXISTS (SELECT 1
                           FROM gc_program_request_process gprp
                          WHERE gprp.gc_coupon_number = gcc.gc_coupon_number
                            AND gprp.gc_program_config_id = 1);

END GET_OUTBOUND_GC_1;


PROCEDURE GET_OUTBOUND_RD_1
(
 IN_PROGRAM_NAME   IN GC_COUPON_REQUEST.GC_PARTNER_PROGRAM_NAME%TYPE,
 OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves records from table gc_coupon_recipients
        that have a partner program name as the one passed in and are not related
        to a gc_program_request_process with a confid id of 3 (records
        that already have been processed).

Input:
        program_name        varchar2

Output:
        cursor containing   gc_coupon_recipients, gc_coupon_request and
                               gc_coupons records

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CURSOR FOR
      SELECT gcr.request_number,
             r.last_name,
             r.first_name,
             r.address_1,
             r.city,
             r.state,
             r.zip_code,
             r.country,
             gcc.gc_coupon_number,
             to_char(gcr.issue_amount, '9999D00') "issue_amount",
             TO_CHAR(gcr.issue_date, 'mm-dd-yyyy') "issue_date",
             TO_CHAR(gcr.expiration_date, 'mm-dd-yyyy') "expiration_date",
             TO_CHAR(SYSDATE, 'mm-dd-yyyy') "sysdate"
        FROM gc_coupon_recipients r,
             gc_coupons gcc,
             gc_coupon_request gcr
       WHERE gcc.gc_coupon_recip_id=r.gc_coupon_recip_id
         AND gcc.request_number=gcr.request_number
         AND gcc.GC_COUPON_STATUS in ('Redeemed', 'Reinstate')
         AND gcr.gc_partner_program_name=in_program_name
         AND NOT EXISTS (SELECT 1
                           FROM gc_program_request_process gprp
                          WHERE gprp.gc_coupon_number = gcc.gc_coupon_number
                            AND gprp.gc_program_config_id=3);

END GET_OUTBOUND_RD_1;


PROCEDURE GET_GC_PROGRAM_CONFIG
(
 IN_PROGRAM_NAME   IN GC_COUPON_REQUEST.GC_PARTNER_PROGRAM_NAME%TYPE,
 IN_PARM_NAME      IN GC_CONFIG_PARM.PARM_NAME%TYPE,
 OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves records from table gc_config_parm
        that have a partner program name as the one passed in and are related
        to gc_program_config with a parm_name as the one passed in.

Input:
        program_name        varchar2
        parm_name           varchar2

Output:
        cursor containing   gc_program_config and gc_config_parm records

-----------------------------------------------------------------------------*/
BEGIN

    OPEN OUT_CURSOR FOR
      SELECT parm_value,
             template_name
        FROM gc_program_config gpc,
             gc_config_parm gcp
       WHERE gpc.gc_partner_program_name = in_program_name
         AND gpc.gc_config_parm_id = gcp.gc_config_parm_id
         AND gcp.parm_name = in_parm_name;

END GET_GC_PROGRAM_CONFIG;


  PROCEDURE UPDATE_GC_COUPON_STATUS
  (
   IN_GC_COUPON_NUMBER  IN GC_COUPONS.GC_COUPON_NUMBER%TYPE,
   IN_STATUS            IN GC_COUPONS.GC_COUPON_STATUS%TYPE,
   OUT_STATUS          OUT VARCHAR2,
   OUT_ERROR_MESSAGE   OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the gc_coupon_status for all the records
          in table gc_coupons to the status passed in for the gc_coupon_number
          passed in.

  Input:
          gc_coupon_number     NUMBER
          status               CHAR

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    UPDATE gc_coupons
       SET gc_coupon_status = in_status,
           updated_on       = SYSDATE,
           updated_by       = USER
      WHERE gc_coupon_number = in_gc_coupon_number;

    IF SQL%FOUND THEN
       out_status := 'Y';
    ELSE
       out_status := 'N';
       out_error_message := 'WARNING: No records found for coupon number ' || in_gc_coupon_number ||'.';
    END IF;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
     ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GC_COUPON_STATUS;


  PROCEDURE UPDATE_GC_COUPON_STATUS_STRICT
  (
   IN_GC_PARTNER_SEQUENCE       IN GC_COUPONS.GC_PARTNER_SEQUENCE%TYPE,
   IN_STATUS                    IN GC_COUPONS.GC_COUPON_STATUS%TYPE,
   IN_PROGRAM_NAME              IN GC_COUPON_REQUEST.PROGRAM_NAME%TYPE,
   OUT_GC_COUPON_NUMBER         OUT GC_COUPONS.GC_COUPON_NUMBER%TYPE,
   OUT_STATUS                   OUT VARCHAR2,
   OUT_ERROR_MESSAGE            OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the gc_coupon_status for all the records
          in table gc_coupons to the status passed in for the gc_coupon_number
          passed in.

  Input:
          gc_partner_sequence  NUMBER
          status               CHAR
          partner_program      VARCHAR

  Output:
          gc_coupon_number
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

  CASE in_status
    WHEN 'Issued Active' THEN
            UPDATE gc_coupons gc
               SET gc.gc_coupon_status = in_status,
                   gc.updated_on       = SYSDATE,
                   gc.updated_by       = USER
              WHERE gc.gc_partner_sequence = in_gc_partner_sequence
              AND   gc.gc_coupon_status = 'Issued Inactive'
              AND   exists
                      (select 1
                       from  gc_coupon_request gcr
                       where gcr.request_number = gc.request_number
                       and   gcr.gc_partner_program_name = in_program_name)
            RETURNING gc_coupon_number into out_gc_coupon_number;

            IF SQL%FOUND THEN
               out_status := 'Y';
            ELSE
               out_status := 'N';
               out_error_message := 'WARNING: No records found for partner sequence number ' || in_gc_partner_sequence ||'.';
            END IF;

    WHEN 'Cancelled' THEN
            UPDATE gc_coupons gc
               SET gc.gc_coupon_status = in_status,
                   gc.updated_on       = SYSDATE,
                   gc.updated_by       = USER
              WHERE gc.gc_partner_sequence = in_gc_partner_sequence
              AND   gc.gc_coupon_status = 'Issued Active'
              AND   exists
                      (select 1
                       from  gc_coupon_request gcr
                       where gcr.request_number = gc.request_number
                       and   gcr.gc_partner_program_name = in_program_name)
            RETURNING gc_coupon_number into out_gc_coupon_number;

            IF SQL%FOUND THEN
               out_status := 'Y';
            ELSE
               out_status := 'N';
               out_error_message := 'WARNING: No records found for partner sequence number ' || in_gc_partner_sequence ||'.';
            END IF;

     ELSE
            out_status := 'N';
            out_error_message := 'WARNING: Invalid value for in_status  ' || in_status ||'.';

  END CASE;

    EXCEPTION WHEN OTHERS THEN
    BEGIN
     ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END UPDATE_GC_COUPON_STATUS_STRICT;




  PROCEDURE UPDATE_INBOUND_GC_1
  (
   IN_GC_COUPON_NUMBER  IN GC_COUPONS.GC_COUPON_NUMBER%TYPE,
   IN_FIRST_NAME        IN GC_COUPON_RECIPIENTS.FIRST_NAME%TYPE,
   IN_LAST_NAME         IN GC_COUPON_RECIPIENTS.LAST_NAME%TYPE,
   OUT_STATUS          OUT VARCHAR2,
   OUT_ERROR_MESSAGE   OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the gc_coupon_status for all the records
          in table gc_coupons to the status passed in for the gc_coupon_number
          passed in.

  Input:
          gc_coupon_number     NUMBER
          status               CHAR

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  v_gc_coupon_recip_id gc_coupon_recipients.gc_coupon_recip_id%TYPE;
  v_request_number     gc_coupons.request_number%TYPE;

  BEGIN

    out_status := NULL;

    BEGIN
      SELECT gc_coupon_recip_id,
             request_number
        INTO v_gc_coupon_recip_id,
             v_request_number
        FROM gc_coupons
       WHERE gc_coupon_number = in_gc_coupon_number;

      EXCEPTION WHEN OTHERS THEN
      BEGIN
        ROLLBACK;
        out_status := 'N';
        out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      END;

    END;

    IF out_status IS NULL THEN

      BEGIN
        IF v_gc_coupon_recip_id IS NULL THEN
          insert_gc_coupon_recipient(v_request_number,in_first_name,in_last_name,
                                     NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                                     v_gc_coupon_recip_id, out_status, out_error_message);
          UPDATE clean.gc_coupons
             SET gc_coupon_recip_id = v_gc_coupon_recip_id,
                 updated_on = SYSDATE,
                 updated_by = USER
           WHERE gc_coupon_number = in_gc_coupon_number;
        ELSE
          UPDATE clean.gc_coupon_recipients
             SET last_name  = in_last_name,
                 first_name = in_first_name,
                 updated_on = SYSDATE,
                 updated_by = USER
           WHERE gc_coupon_recip_id = v_gc_coupon_recip_id;
        END IF;

        IF SQL%FOUND THEN
           out_status := 'Y';
        ELSE
           out_status := 'N';
           out_error_message := 'WARNING: No records found for coupon number ' || in_gc_coupon_number ||'.';
        END IF;

        EXCEPTION WHEN OTHERS THEN
        BEGIN
         ROLLBACK;
          out_status := 'N';
          out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        END;

      END;

    END IF;

  END UPDATE_INBOUND_GC_1;


  PROCEDURE UPDATE_PROGRAM_REQUEST_PROCESS
  (
   IN_GC_COUPON_NUMBER  IN GC_PROGRAM_REQUEST_PROCESS.GC_COUPON_NUMBER%TYPE,
   IN_PARM_NAME         IN GC_CONFIG_PARM.PARM_NAME%TYPE,
   OUT_STATUS           OUT VARCHAR2,
   OUT_ERROR_MESSAGE    OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure updates the gc_coupon_status for all the records
          in table gc_coupons to the status passed in for the gc_coupon_number
          passed in.

  Input:
          request_number     NUMBER
          parm_name          VARCHAR2

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  v_gc_config_parm_id gc_config_parm.gc_config_parm_id%TYPE;

  BEGIN

    SELECT gc_config_parm_id
      INTO v_gc_config_parm_id
      FROM gc_config_parm
     WHERE parm_name = in_parm_name;

    INSERT INTO gc_program_request_process
        (gc_coupon_number,
         gc_program_config_id,
         process_date)
      VALUES
        (in_gc_coupon_number,
         v_gc_config_parm_id,
         SYSDATE);

    out_status := 'Y';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        BEGIN
          out_status := 'N';
          out_error_message := 'WARNING: No records found with parm_name ' || in_parm_name ||'.';
        END;
      WHEN OTHERS THEN
        BEGIN
          out_status := 'N';
          out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        END;

  END UPDATE_PROGRAM_REQUEST_PROCESS;



  PROCEDURE INSERT_GC_COUPON_SOURCE_CODE
  (
   IN_REQUEST_NUMBER IN VARCHAR2,
   IN_SOURCE_CODE IN VARCHAR2,
   IN_CREATED_BY  IN VARCHAR2,   
   OUT_STATUS         OUT VARCHAR2,
   OUT_ERROR_MESSAGE  OUT VARCHAR2
  )
  AS
  /*-----------------------------------------------------------------------------
  Description:
          This stored procedure inserts a record into table gc_coupon_SOURCE.

  Input:
          gc_request_number, source_code, created_by, created_on, updated_by, updated_on

  Output:
          status
          error message

  -----------------------------------------------------------------------------*/

  BEGIN

    INSERT INTO gc_coupon_source
        (request_number,
         source_code,
         created_by,
         created_on,
         updated_by,
         updated_on)
      VALUES
        (in_request_number,
         in_source_code,
         in_created_by,
         sysdate,
         in_created_by,
         sysdate);

    out_status := 'Y';

    EXCEPTION WHEN OTHERS THEN
    BEGIN
      ROLLBACK;
      out_status := 'N';
      out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

  END INSERT_GC_COUPON_SOURCE_CODE; 



PROCEDURE DELETE_GC_REST_SRC_CODES
(
 IN_REQUEST_NUMBER          IN  GC_COUPON_SOURCE.REQUEST_NUMBER%TYPE,
 IN_SOURCE_CODE             IN  GC_COUPON_SOURCE.SOURCE_CODE%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table
        GC_COUPON_SOURCE.

Input:
        REQUEST_NUMBER NUMBER

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  DELETE FROM gc_coupon_source
        WHERE request_number = in_request_number
        AND source_code = in_source_code;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_GC_REST_SRC_CODES;

PROCEDURE  SRC_CODES_VALIDATION
(
   IN_COMPANY_ID IN  VARCHAR2,  
   IN_REQUEST_NUMBER IN VARCHAR2, 
   IN_SOURCE_CODE_STR IN VARCHAR2,
   IN_CREATED_BY  IN VARCHAR2, 
   VALIDATE_STATUS    OUT VARCHAR2,
   EXIST_MESSAGE			 OUT VARCHAR2,
   EXPIRED_MESSAGE OUT VARCHAR2,
   OUT_STATUS  OUT VARCHAR2,
  OUT_ERROR_MESSAGE  OUT VARCHAR2
) 
AS
    p_del VARCHAR2(3) := ' ';
    l_idx    NUMBER;    
    l_list    VARCHAR2(32767) := IN_SOURCE_CODE_STR;
    final_str VARCHAR2(900) := ' ';
    l_result VARCHAR2(100);
    SOURCE_CODE VARCHAR2(900);
BEGIN
    VALIDATE_STATUS := 'N';
    EXIST_MESSAGE := ' ';
    EXPIRED_MESSAGE := ' ';
    LOOP
        l_idx := INSTR(l_list,p_del);
        
        IF l_idx > 0 THEN
           SOURCE_CODE := TRIM(SUBSTR(l_list,1,l_idx-1));
           l_result := GLOBAL.global_pkg.IS_SOURCE_CODE_VALID(IN_COMPANY_ID,SOURCE_CODE);
           l_list := SUBSTR(l_list,l_idx + LENGTH(p_del));            
        ELSE 
          SOURCE_CODE :=  TRIM(l_list);    
          l_result := GLOBAL.global_pkg.IS_SOURCE_CODE_VALID(IN_COMPANY_ID,SOURCE_CODE);            
          IF l_result = 'DOES_NOT_EXIST' THEN
              VALIDATE_STATUS := 'Y'; 
              EXIST_MESSAGE := EXIST_MESSAGE || SOURCE_CODE || ' ';
             ELSIF l_result = 'EXPIRED' THEN 
              VALIDATE_STATUS := 'Y'; 
              EXPIRED_MESSAGE := EXPIRED_MESSAGE || SOURCE_CODE || ' ';
             --ELSIF l_result = 'TRUE' THEN 
              --CLEAN.gift_certificate_coupon_pkg.INSERT_GC_COUPON_SOURCE_CODE(in_request_number, source_code, in_created_by, out_status, out_error_message);
             END IF;
                   
        EXIT;
        END IF; 
        
      
       IF l_result = 'DOES_NOT_EXIST' THEN
        VALIDATE_STATUS := 'Y';
        EXIST_MESSAGE := EXIST_MESSAGE || SOURCE_CODE || ' ';
       ELSIF l_result = 'EXPIRED' THEN
        VALIDATE_STATUS := 'Y'; 
        EXPIRED_MESSAGE := EXPIRED_MESSAGE || SOURCE_CODE || ' ';
       --ELSIF l_result = 'TRUE' THEN 
        --CLEAN.gift_certificate_coupon_pkg.INSERT_GC_COUPON_SOURCE_CODE(in_request_number, source_code, in_created_by, out_status, out_error_message);
       END IF;             
    END LOOP;         
    
END SRC_CODES_VALIDATION;

PROCEDURE UPDATE_GC_STATUS_BY_GCNUMBER
(
IN_GC_NUM          IN VARCHAR2,
IN_INTIAL_STATUS   IN VARCHAR2,
IN_FINAL_STATUS    IN VARCHAR2,
IN_AMOUNT          IN VARCHAR2,
IN_EXPIRY_DATE     IN VARCHAR2,
IN_UPDATED_BY    IN VARCHAR2,

OUT_STATUS         OUT VARCHAR2,
OUT_MESSAGE        OUT VARCHAR2
)

AS

V_SQL VARCHAR2(20000);
UPDATED_ROWS number;

BEGIN

V_SQL := 'update clean.gc_coupons set updated_on = sysdate , updated_by = ''' || IN_UPDATED_BY || '''  ';

IF (IN_AMOUNT is null or IN_AMOUNT = '') AND (IN_EXPIRY_DATE is null or IN_EXPIRY_DATE = '') AND (IN_INTIAL_STATUS is null or IN_INTIAL_STATUS  = '') THEN

V_SQL := null;

END IF;

IF IN_AMOUNT is not null or IN_AMOUNT <> '' THEN


V_SQL :=  V_SQL || ', Issue_amount = ''' ||IN_AMOUNT || ''' ';

END IF;

IF IN_EXPIRY_DATE is not null or IN_EXPIRY_DATE <> ''  THEN


V_SQL :=  V_SQL || ', Expiration_date = ''' || TO_DATE(IN_EXPIRY_DATE, 'MM/DD/YYYY HH24:MI:SS') || ''' ';

END IF;

IF IN_INTIAL_STATUS is not null or IN_INTIAL_STATUS  <> ''  THEN

V_SQL := V_SQL ||  ', gc_coupon_status = ''' || IN_FINAL_STATUS || '''  WHERE gc_coupon_number = ''' || IN_GC_NUM || '''  AND gc_coupon_status in (' || IN_INTIAL_STATUS || ')' ;

ELSE

V_SQL := V_SQL || ' WHERE gc_coupon_number = ''' || IN_GC_NUM || ''' ';

END IF;

	IF V_SQL IS NOT NULL THEN
	Execute Immediate V_SQL;
	UPDATED_ROWS := sql%rowcount;
	END IF;

	OUT_STATUS := 'Y';
	OUT_MESSAGE :=  UPDATED_ROWS ;

EXCEPTION WHEN OTHERS THEN

	OUT_STATUS :=  'N';
	OUT_MESSAGE := 'ERROR OCCURRED IN : ' || SQLCODE  || SUBSTR (SQLERRM,1,256);

END UPDATE_GC_STATUS_BY_GCNUMBER;


PROCEDURE UPDATE_GC_STATUS_BY_BATNUMBER
(
IN_BAT_NUM          IN VARCHAR2,
IN_INTIAL_STATUS   IN VARCHAR2,
IN_FINAL_STATUS    IN VARCHAR2,
IN_AMOUNT          IN VARCHAR2,
IN_EXPIRY_DATE     IN VARCHAR2,
IN_UPDATED_BY      IN VARCHAR2,

OUT_STATUS         OUT VARCHAR2,
OUT_MESSAGE        OUT VARCHAR2,
OUT_MESSAGE_1        OUT VARCHAR2

)

AS

V_SQL VARCHAR2(20000);
V_SQL_1 VARCHAR2(20000);
UPDATED_ROWS number;
UPDATED_ROWS_1 number;


BEGIN

V_SQL := 'update clean.gc_coupons set updated_on = sysdate, updated_by = ''' || IN_UPDATED_BY || ''' ';
V_SQL_1:= 'Update clean.GC_COUPON_REQUEST set updated_on = sysdate, updated_by = ''' || IN_UPDATED_BY || ''' ';

IF IN_AMOUNT is not null or IN_AMOUNT <> '' THEN


V_SQL :=  V_SQL || ', Issue_amount = ''' ||IN_AMOUNT || ''' ';
V_SQL_1 := V_SQL_1 || ', Issue_amount = ''' ||IN_AMOUNT || ''' ';

END IF;

IF IN_EXPIRY_DATE is not null or IN_EXPIRY_DATE <> ''  THEN


V_SQL :=  V_SQL || ', Expiration_date = ''' || TO_DATE(IN_EXPIRY_DATE, 'MM/DD/YYYY HH24:MI:SS') || ''' ';
V_SQL_1 :=  V_SQL_1 || ', Expiration_date = ''' || TO_DATE(IN_EXPIRY_DATE, 'MM/DD/YYYY HH24:MI:SS') || ''' ';

END IF;

IF IN_INTIAL_STATUS is not null or IN_INTIAL_STATUS  <> ''  THEN

V_SQL := V_SQL ||  ', gc_coupon_status = ''' || IN_FINAL_STATUS || '''  WHERE REQUEST_NUMBER = ''' || IN_BAT_NUM || '''  AND gc_coupon_status in ('|| IN_INTIAL_STATUS || ')' ;

ELSE

V_SQL := V_SQL || ' WHERE REQUEST_NUMBER = ''' || IN_BAT_NUM || '''  ' ;

END IF;

IF (IN_AMOUNT is null or IN_AMOUNT = '') AND (IN_EXPIRY_DATE is null or IN_EXPIRY_DATE = '') AND (IN_INTIAL_STATUS is null or IN_INTIAL_STATUS  = '') THEN

V_SQL := null;
V_SQL_1 := null;

END IF;

V_SQL_1 := V_SQL_1 ||  ' WHERE REQUEST_NUMBER = ''' || IN_BAT_NUM || ''' ' ;

IF (IN_AMOUNT is null or IN_AMOUNT = '') AND (IN_EXPIRY_DATE is null or IN_EXPIRY_DATE = '')  THEN

V_SQL_1 := null;

END IF;

	IF V_SQL IS NOT NULL THEN
	Execute Immediate V_SQL;
	UPDATED_ROWS := sql%rowcount;
	END IF;
	
	IF V_SQL_1 IS NOT NULL THEN
	Execute Immediate V_SQL_1;
	UPDATED_ROWS_1 := sql%rowcount;
	END IF;


	OUT_STATUS := 'Y';
	OUT_MESSAGE :=  UPDATED_ROWS ;
  OUT_MESSAGE_1 :=  UPDATED_ROWS_1 ;


EXCEPTION WHEN OTHERS THEN

	OUT_STATUS :=  'N';
	OUT_MESSAGE := 'ERROR OCCURRED IN : ' || SQLCODE  || SUBSTR (SQLERRM,1,256);
	OUT_MESSAGE_1 := 'ERROR OCCURRED IN : ' || SQLCODE  || SUBSTR (SQLERRM,1,256);

END UPDATE_GC_STATUS_BY_BATNUMBER;

FUNCTION GET_CART_COUNT
  (
   IN_GIFT_CERTIFICATE_ID IN VARCHAR2,
   IN_ORDER_DETAIL_ID IN NUMBER
  )
  RETURN VARCHAR2
  IS
  /*------------------------------------------------------------------------------
  Description:
     Returns the count of how many items are in a shopping cart.

  Input:
          gift_certificate_id   VARCHAR2,
          order_detail_id       NUMBER

  Output:
          cart count            VARCHAR2
  ------------------------------------------------------------------------------*/
  v_cart_count NUMBER;

  BEGIN

     SELECT  count (1)
        INTO    v_cart_count
        FROM    order_details od1
        WHERE   EXISTS
        (
                select  1
                from    order_details od2
                join    payments p
                on      od2.order_guid = p.order_guid
                and     p.additional_bill_id is null
                and     p.refund_id is null
                and     p.gc_coupon_number = IN_GIFT_CERTIFICATE_ID
                where   od1.order_guid = od2.order_guid
                and     od2.order_detail_id = in_order_detail_id
        );

     RETURN v_cart_count;

END GET_CART_COUNT;

PROCEDURE GET_GC_ORDER_REFUND_AMT
(
 IN_GIFT_CERTIFICATE_ID             IN VARCHAR2,
 OUT_REFUND_CURSOR        			OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	Retrieves a cursor with refund amount(s) for the given gift certificate number.
Input:
    IN_GC_COUPON_NUMBER varchar2
Output:
        REF_CURSOR

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_REFUND_CURSOR FOR
   
       select   r.order_detail_id, nvl(sum(p.debit_amount - nvl (p.credit_amount, 0)),0) refund_amount
                from    payments p,
                        refund r
                where   p.refund_id = r.refund_id
                and     p.gc_coupon_number = IN_GIFT_CERTIFICATE_ID
                and     p.payment_indicator in ('R', 'B')
                group by r.order_detail_id;

END GET_GC_ORDER_REFUND_AMT;


END GIFT_CERTIFICATE_COUPON_PKG;
.
/
