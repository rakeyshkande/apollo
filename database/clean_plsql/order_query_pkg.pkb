create or replace package body CLEAN.ORDER_QUERY_PKG
AS

FUNCTION GET_SCRUB_STATUS
(
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the scrub status indicator
        of the order disposition code

Input:
        order_disp_code                 varchar2

Output:
        Scrub
        Pending
        Removed
        null - no longer in scrub

-----------------------------------------------------------------------------*/

v_scrub_ind     varchar2 (20);

BEGIN

CASE in_order_disp_code
WHEN 'In-Scrub' THEN
        v_scrub_ind := 'Scrub';

WHEN 'Pending' THEN
        v_scrub_ind := 'Pend';

WHEN 'Removed' THEN
        v_scrub_ind := 'Rmvd';

ELSE
        v_scrub_ind := null;

END CASE;

RETURN v_scrub_ind;

END GET_SCRUB_STATUS;


FUNCTION GET_SCRUB_STATUS_IND
(
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the scrub status indicator
        of the order disposition code

Input:
        order_disp_code                 varchar2

Output:
        S - In-Scrub
        P - Pending
        R - Removed
        null - no longer in scrub

-----------------------------------------------------------------------------*/

v_scrub_ind     varchar2 (1);

BEGIN

v_scrub_ind := substr (get_scrub_status(in_order_disp_code), 1, 1);

RETURN v_scrub_ind;

END GET_SCRUB_STATUS_IND;


FUNCTION IS_CART_ON_HOLD
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if the cart is on hold.
        If any one order in the cart is on hold, the entire cart is on hold.

Input:
        order_detail_id         number

Output:
        Y/N

-----------------------------------------------------------------------------*/

v_hold_ind     char (1);

CURSOR  check_cur IS
        select  decode (count (1), 0, 'N', 'Y')
        from    order_details od2
        join    order_hold oh
        on      od2.order_detail_id = oh.order_detail_id
        where   od2.order_guid = in_order_guid;

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_hold_ind;
CLOSE check_cur;

RETURN v_hold_ind;

END IS_CART_ON_HOLD;


FUNCTION IS_WEBOE_ORIGIN
(
IN_ORIGIN_ID            IN ORDERS.ORIGIN_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the order was placed in WebOE
        which is determined by the origin.  If the origin is null
        or a phone type origin, the order originated from WebOE.

Input:
        order guid                      varchar2

Output:
        Y/N

-----------------------------------------------------------------------------*/

CURSOR origin_cur IS
        select  'Y'
        from    ftd_apps.origins
        where   origin_id = in_origin_id
        and     origin_type = 'phone';

-- initialize the out parameters to Y
v_weboe char(1) := 'N';

BEGIN

IF in_origin_id IS NULL THEN
        v_weboe := 'Y';

ELSE
        -- check if the given origin is a phone type
        OPEN origin_cur;
        FETCH origin_cur INTO v_weboe;
        CLOSE origin_cur;
END IF;

RETURN v_weboe;

END IS_WEBOE_ORIGIN;

PROCEDURE FIND_ORDER_NUMBER_INCLU_MERC
(
IN_ORDER_NUMBER                 IN VARCHAR2,
OUT_ORDER_DETAIL_ID             OUT ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_ORDER_GUID                  OUT ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_EXTERNAL_ORDER_NUMBER       OUT ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_MASTER_ORDER_NUMBER         OUT ORDERS.MASTER_ORDER_NUMBER%TYPE,
OUT_CUSTOMER_ID                 OUT ORDERS.CUSTOMER_ID%TYPE,
OUT_ORDER_DISP_CODE             OUT ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
OUT_ORDER_INDICATOR             OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail id of a
        order number by checking all order number fields.

Input:
        order_number                    varchar2 -- can be order detail id, master order number, external order number, hp order number, amazon or walmart order number

Output:
        order_detail_id                 number

-----------------------------------------------------------------------------*/

v_order_number          number;

CURSOR od_cur (p_order_number number) IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'external_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   od.order_detail_id = p_order_number;

CURSOR eon_cur IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'external_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   od.external_order_number = upper (in_order_number);

CURSOR mon_cur IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'master_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   substr(o.master_order_number, 1, instr (o.master_order_number || '/', '/') - 1) = upper (in_order_number )
        ORDER BY od.order_detail_id;

CURSOR mercury_cur1 IS
      select * from (SELECT  m.reference_number
        FROM    mercury.mercury m
        WHERE   m.mercury_order_number like (upper(in_order_number)||'%')
        and m.reference_number is not null
        and length(in_order_number) = 6
        order by m.created_on desc) where rownum <= 1;

CURSOR mercury_cur2 (p_order_detail_id varchar2) IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'mercury_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   to_char (od.order_detail_id) = p_order_detail_id;

CURSOR venus_cur1 IS
        SELECT  v.reference_number
        FROM    venus.venus v
        WHERE   v.venus_order_number = upper (in_order_number);

CURSOR venus_cur2 (p_order_detail_id varchar2) IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'venus_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   to_char (od.order_detail_id) = p_order_detail_id;

v_reference_number              mercury.mercury.reference_number%type;

BEGIN

-- check if the given order number is an order detail id
begin
        v_order_number := to_number (in_order_number);

        OPEN od_cur (v_order_number);
        FETCH od_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE od_cur;

        -- if the given order number is not a number continue finding the order
        exception when others then null;
end;

-- check external order number
IF out_order_detail_id IS NULL THEN
        OPEN eon_cur;
        FETCH eon_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE eon_cur;
END IF;

-- check master order number
IF out_order_detail_id IS NULL THEN
        OPEN mon_cur;
        FETCH mon_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE mon_cur;
END IF;

-- check if mercury order number
IF out_order_detail_id IS NULL THEN
        OPEN mercury_cur1;
        FETCH mercury_cur1 INTO v_reference_number;
        CLOSE mercury_cur1;

        IF v_reference_number is not null THEN
                OPEN mercury_cur2 (v_reference_number);
                FETCH mercury_cur2 INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
                CLOSE mercury_cur2;
        END IF;
END IF;

-- check if venus order number
IF out_order_detail_id IS NULL THEN
        OPEN venus_cur1;
        FETCH venus_cur1 INTO v_reference_number;
        CLOSE venus_cur1;

        IF v_reference_number is not null THEN
                OPEN venus_cur2 (v_reference_number);
                FETCH venus_cur2 INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
                CLOSE venus_cur2;
        END IF;
END IF;

END FIND_ORDER_NUMBER_INCLU_MERC;


PROCEDURE FIND_ORDER_NUMBER
(
IN_ORDER_NUMBER                 IN VARCHAR2,
OUT_ORDER_DETAIL_ID             OUT ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_ORDER_GUID                  OUT ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_EXTERNAL_ORDER_NUMBER       OUT ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_MASTER_ORDER_NUMBER         OUT ORDERS.MASTER_ORDER_NUMBER%TYPE,
OUT_CUSTOMER_ID                 OUT ORDERS.CUSTOMER_ID%TYPE,
OUT_ORDER_DISP_CODE             OUT ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
OUT_ORDER_INDICATOR             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail id of a
        order number by checking all order number fields.

Input:
        order_number                    varchar2 -- can be order detail id, master order number, external order number, hp order number, amazon or walmart order number

Output:
        order_detail_id                 number

-----------------------------------------------------------------------------*/

v_order_number          number;

CURSOR od_cur (p_order_number number) IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'external_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   od.order_detail_id = p_order_number;

CURSOR eon_cur IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'external_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   od.external_order_number = upper (in_order_number);

CURSOR mon_cur IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'master_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   substr(o.master_order_number, 1, instr (o.master_order_number || '/', '/') - 1) = upper (in_order_number )
        ORDER BY od.order_detail_id;

CURSOR mercury_cur1 IS
        SELECT  m.reference_number
        FROM    mercury.mercury m
        WHERE   m.mercury_order_number = upper (in_order_number)
    and m.reference_number is not null;

CURSOR mercury_cur2 (p_order_detail_id varchar2) IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'mercury_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   to_char (od.order_detail_id) = p_order_detail_id;

CURSOR venus_cur1 IS
        SELECT  v.reference_number
        FROM    venus.venus v
        WHERE   v.venus_order_number = upper (in_order_number);

CURSOR venus_cur2 (p_order_detail_id varchar2) IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'venus_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        WHERE   to_char (od.order_detail_id) = p_order_detail_id;

-- Partner order number (needed for ProFlowers)
CURSOR ptn_pi_cur IS
        SELECT  od.order_detail_id,
                od.order_guid,
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'partner_order_number' order_indicator
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    PTN_PI.partner_order po
        ON      po.master_order_number = o.master_order_number
        WHERE   po.partner_order_number = upper (in_order_number);
        
v_reference_number              mercury.mercury.reference_number%type;

BEGIN

-- check if the given order number is an order detail id
begin
        v_order_number := to_number (in_order_number);

        OPEN od_cur (v_order_number);
        FETCH od_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE od_cur;

        -- if the given order number is not a number continue finding the order
        exception when others then null;
end;

-- check external order number
IF out_order_detail_id IS NULL THEN
        OPEN eon_cur;
        FETCH eon_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE eon_cur;
END IF;

-- check master order number
IF out_order_detail_id IS NULL THEN
        OPEN mon_cur;
        FETCH mon_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE mon_cur;
END IF;

-- check if mercury order number
IF out_order_detail_id IS NULL THEN
        OPEN mercury_cur1;
        FETCH mercury_cur1 INTO v_reference_number;
        CLOSE mercury_cur1;

        IF v_reference_number is not null THEN
                OPEN mercury_cur2 (v_reference_number);
                FETCH mercury_cur2 INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
                CLOSE mercury_cur2;
        END IF;
END IF;

-- check if venus order number
IF out_order_detail_id IS NULL THEN
        OPEN venus_cur1;
        FETCH venus_cur1 INTO v_reference_number;
        CLOSE venus_cur1;

        IF v_reference_number is not null THEN
                OPEN venus_cur2 (v_reference_number);
                FETCH venus_cur2 INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
                CLOSE venus_cur2;
        END IF;
END IF;

-- check if proflowers number
IF out_order_detail_id IS NULL THEN
        OPEN ptn_pi_cur;
        FETCH ptn_pi_cur INTO out_order_detail_id, out_order_guid, out_external_order_number, out_master_order_number, out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE ptn_pi_cur;
END IF;

END FIND_ORDER_NUMBER;


PROCEDURE FIND_ORDER_NUMBER
(
IN_ORDER_NUMBER                 IN VARCHAR2,
OUT_ORDER_DETAIL_ID             OUT ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_ORDER_GUID                  OUT ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_ORDER_DISP_CODE             OUT ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
OUT_ORDER_INDICATOR             OUT VARCHAR2
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail id of a
        order number by checking all order number fields.

Input:
        order_number                    varchar2

Output:
        order_detail_id                 number

Note:   This procedure overloads the procedure above.
-----------------------------------------------------------------------------*/

v_external_order_number ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE;
v_master_order_number   ORDERS.MASTER_ORDER_NUMBER%TYPE;
v_customer_id           ORDERS.CUSTOMER_ID%TYPE;

BEGIN

find_order_number
(
        in_order_number,
        out_order_detail_id,
        out_order_guid,
        v_external_order_number,
        v_master_order_number,
        v_customer_id,
        out_order_disp_code,
        out_order_indicator
);

END FIND_ORDER_NUMBER;


PROCEDURE FIND_ORDER_NUMBER_CUST_EMAIL
(
IN_ORDER_NUMBER                 IN VARCHAR2,
IN_EMAIL_ADDRESS                IN EMAIL.EMAIL_ADDRESS%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)

AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail id of a
        order number by checking all order number fields.

Input:
        order_number                    varchar2

Output:
        cursor containing order detail info

-----------------------------------------------------------------------------*/

v_order_detail_id       order_details.order_detail_id%type;
v_order_guid            order_details.order_guid%type;
v_external_order_number order_details.external_order_number%type;
v_master_order_number   orders.master_order_number%type;
v_customer_id           customer.customer_id%type;
v_order_disp_code       order_details.order_disp_code%type;
v_order_indicator       varchar2(30);
v_customer_check        char (1);

CURSOR email_cur (p_customer_id number, p_order_guid varchar2) IS
        SELECT  'Y'
        FROM    email e
        JOIN    customer_email_ref cer
        ON      cer.email_id = e.email_id
        JOIN    orders o
        ON      o.order_guid = p_order_guid
        and     e.email_id = o.email_id
        WHERE   cer.customer_id = p_customer_id
        AND     UPPER (e.email_address) = UPPER (in_email_address);

CURSOR name_cur (p_customer_id number) IS
        SELECT  'Y'
        FROM    customer
        WHERE   customer_id = p_customer_id
        AND     UPPER (last_name) = UPPER (in_last_name)
        AND     UPPER (first_name) = UPPER (in_first_name);

BEGIN

-- find the order number
FIND_ORDER_NUMBER
(
        in_order_number,
        v_order_detail_id,
        v_order_guid,
        v_external_order_number,
        v_master_order_number,
        v_customer_id,
        v_order_disp_code,
        v_order_indicator
);

-- check the email address of the customer if the order is found
IF v_order_detail_id IS NOT NULL THEN
        OPEN email_cur (v_customer_id, v_order_guid);
        FETCH email_cur INTO v_customer_check;
        CLOSE email_cur;

        -- if the email address is not associated to the order's customer
        -- check if the given customer name is correct
        IF v_customer_check IS NULL THEN
                OPEN name_cur (v_customer_id);
                FETCH name_cur INTO v_customer_check;
                CLOSE name_cur;
        END IF;
END IF;

-- if the order is not found or the given email address or customer name
-- is not associated to the order, return empty cursor fields indicating
-- that the order is not found
IF v_customer_check IS NULL THEN
        OPEN out_cur FOR
                SELECT  null order_detail_id,
                        null order_guid,
                        null external_order_number,
                        null master_order_number,
                        null customer_id,
                        null order_disp_code,
                        null order_indicator,
                        null ftp_vendor_flag,
                        null source_code
                FROM    dual;
ELSE
        OPEN out_cur FOR
                SELECT  od.order_detail_id,
                        od.order_guid,
                        external_order_number,
                        v_master_order_number master_order_number,
                        v_customer_id customer_id,
                        od.order_disp_code,
                        decode (o.origin_id, 'AMZNI', 'az_order_number', 'WLMTI', 'walmart_order_number', v_order_indicator) order_indicator,
                        decode ((select pm.SHIPPING_SYSTEM from ftd_apps.product_master pm where pm.product_id = od.product_id), 'FTP', 'Y', 'N') ftp_vendor_flag,
                        od.source_code
                FROM    order_details od
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                WHERE   order_detail_id = v_order_detail_id;
END IF;

END FIND_ORDER_NUMBER_CUST_EMAIL;


PROCEDURE GET_ORDER_LIST_BY_ID
(
IN_ID_STR                       IN VARCHAR2,
IN_ORDER_NUMBER                 IN VARCHAR2,
IN_CUSTOMER_ID                  IN NUMBER,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order information
        for the order detail ids passed in the string

Input:
        string containing order detail ids
        order number - original searched for order used to return an indicator
                        for displaying the order tab
        customer_id - used to display the recipient name, 'Recipient' or 'Both'
                if the given customer is the buyer, then recipient name is displayed
                if the given customer is the recipient, then 'Recipient' is displayed
                if the given customer is both the buyer and the recipient, then 'Both' is displayed

Output:
        cursor containing order information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
            'SELECT od.order_guid,
                    od.order_detail_id,
                    o.master_order_number,
                    od.external_order_number,
                    case
                     when :1 = o.customer_id and :2 = od.recipient_id then ''Both''
                     when :3 = o.customer_id then r.first_name
                     else ''Recipient''
                    end recipient_first_name,
                    case
                     when :4 = o.customer_id and :5 = od.recipient_id then null
                     when :6 = o.customer_id then r.last_name
                     else null
                    end recipient_last_name,
                    to_char (od.delivery_date, ''mm/dd/yyyy'') delivery_date,
                    to_char (o.order_date, ''mm/dd/yyyy'') order_date,
                    (select sum (ob.discount_product_price + ob.add_on_amount + ob.service_fee + ob.shipping_fee + ob.shipping_tax + ob.tax) bill_total
                      from order_bills ob
                      where ob.order_detail_id = od.order_detail_id
                    ) order_total,
                    (select sum (r.refund_product_amount - r.refund_discount_amount + r.refund_addon_amount + r.refund_shipping_fee + r.refund_service_fee + r.refund_shipping_tax + r.refund_service_fee_tax + r.refund_tax) refund_total
                      from refund r
                      where r.order_detail_id = od.order_detail_id
                    ) refund_total,
                    (select min(r.refund_disp_code) refund_disp
                      from refund r
                      where r.order_detail_id = od.order_detail_id
                    ) refund_disp,
                    decode (oh.order_detail_id, null, null, ''Y'') order_hold_indicator,
                    decode (odv.display_indicator, ''Y'', od.order_disp_code, null) order_disp_code,
                    order_query_pkg.get_scrub_status_ind (od.order_disp_code) scrub_status,
                    decode (:7, null, null, od.external_order_number, ''Y'', od.hp_order_number, ''Y'', o.master_order_number, ''Y'', null) order_search_indicator,
                    order_query_pkg.is_cart_on_hold (od.order_guid) cart_hold_indicator,
                    (select sum (ob.product_amount)
                      from order_bills ob
                      where ob.order_detail_id = od.order_detail_id
                    ) msd_total,
                    o.customer_id,
                    o.loss_prevention_indicator,
                    (SELECT distinct pm.partner_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name
                      WHERE pm.preferred_partner_flag = ''Y'' AND spr.source_code = od.source_code
                    ) partner_name,
                    (SELECT distinct pm.PREFERRED_PROCESSING_RESOURCE FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name
                      WHERE pm.preferred_partner_flag = ''Y'' AND spr.source_code = od.source_code
                    ) partner_resource,
                    order_query_pkg.GET_PARTNER_WARNING_MSG (od.source_code) partner_warning_message,
                    (SELECT distinct pm.display_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name
                      WHERE pm.preferred_partner_flag = ''Y'' AND spr.source_code = (select sod.source_code from scrub.order_details sod where sod.order_detail_id = od.order_detail_id )
                    ) scrub_partner_display_name,
                    (SELECT distinct pm.partner_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name
                      WHERE pm.preferred_partner_flag = ''Y'' AND spr.source_code = (select sod.source_code from scrub.order_details sod where sod.order_detail_id = od.order_detail_id )
                    ) scrub_partner_resource,
                    order_query_pkg.GET_PARTNER_WARNING_MSG ( (select sod.source_code from scrub.order_details sod where sod.order_detail_id = od.order_detail_id) ) scrub_partner_warning_message,
                    (SELECT distinct pm.display_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name
                      WHERE pm.preferred_partner_flag = ''Y'' AND spr.source_code = od.source_code
                    ) partner_display_name, od.pc_flag,
                    order_query_pkg.get_order_detail_status(od.order_detail_id) order_detail_status,
                    order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) lifecycle_delivered_status,
                    case when clean.order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) = ''Y'' THEN
                    (
                        select status_url
                        from mercury.mercury m
                        join mercury.lifecycle_status ls
                        on ls.mercury_id = m.mercury_id
                        and ls.order_status_code = ''C''
                        where m.reference_number = to_char(od.order_detail_id)
                        and m.msg_type = ''FTD''
                        and m.created_on = (select max(m1.created_on)
                            from mercury.mercury m1
                            where m1.reference_number = m.reference_number
                            and m1.msg_type = ''FTD'')
                        and rownum = 1
                    )
                    end as status_url,
                    case when clean.order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) = ''Y'' THEN
                    (
                        select decode(status_url, null, ''N'', case when sysdate - ls.status_timestamp <= gp1.value then ''Y'' else ''N'' end)
                        from mercury.mercury m
                        join mercury.lifecycle_status ls
                        on ls.mercury_id = m.mercury_id
                        and ls.order_status_code = ''C''
                        left outer join frp.global_parms gp1
                        on gp1.context = ''ORDER_LIFECYCLE''
                        and gp1.name = ''LIFECYCLE_DELIVERY_EXPIRED_DAYS''
                        where m.reference_number = to_char(od.order_detail_id)
                        and m.msg_type = ''FTD''
                        and m.created_on = (select max(m1.created_on)
                            from mercury.mercury m1
                            where m1.reference_number = m.reference_number
                            and m1.msg_type = ''FTD'')
                        and rownum = 1
                    )
                    end as status_url_active,
                    gp.value status_url_expired_msg,
                    (select case when c11.c1 > 1 and c12.c2 <> 0 then ''A50*'' 
                                 when c11.c1 = 1 and c12.c2 <> 0 then ''A50''
                                 when c11.c1 >= 1 and c12.c2 = 0 then ''other''
                                 else ''other'' 
                                 end 
                      from 
                         (select rr.order_detail_id ID1 , count(rr.refund_id) c1 from CLEAN.REFUND rr where rr.order_detail_id = od.order_detail_id group by rr.order_detail_id order by c1 desc) c11,
                         (select COUNT(*) C2 from CLEAN.REFUND rr2 where rr2.order_detail_id=od.order_detail_id AND rr2.REFUND_DISP_CODE=''A50'') c12) lp_refund_code,
                    DECODE(mx.ORIG_ORDER_DETAIL_ID, null, null, ''Yes'') order_modified,
                    mx.NEW_EXTERNAL_ORDER_NUMBER,
		    (select ''Y''
		        from payments p
		        where p.order_guid = o.order_guid
		        and p.payment_indicator = ''P''
		        and p.additional_bill_id is null
                        and p.payment_type not in (''NC'', ''GC'', ''GD'', ''IN'')
                        and p.cardinal_verified_flag = ''Y''
                        and rownum = 1) cardinal_verified
              FROM  order_details od
              JOIN  orders o
                ON  od.order_guid = o.order_guid
              JOIN  order_disposition_val odv
                ON  od.order_disp_code = odv.order_disp_code
   LEFT OUTER JOIN  customer r
                ON  od.recipient_id = r.customer_id
   LEFT OUTER JOIN  order_hold oh
                ON  od.order_detail_id = oh.order_detail_id
   LEFT OUTER JOIN  frp.global_parms gp
                ON  gp.context = ''ORDER_LIFECYCLE''
               AND  gp.name = ''LIFECYCLE_DELIVERY_EXPIRED_MSG''
   LEFT OUTER JOIN clean.mo_xref mx
                ON  mx.ORIG_ORDER_DETAIL_ID = od.ORDER_DETAIL_ID
             WHERE  od.order_detail_id IN (' || in_id_str || ') ' ||
         'ORDER BY  to_char (o.order_date, ''yyyy/mm/dd'') DESC, o.master_order_number, od.external_order_number '
             USING  in_customer_id, in_customer_id, in_customer_id, in_customer_id, in_customer_id, in_customer_id, in_order_number;


END GET_ORDER_LIST_BY_ID;


PROCEDURE GET_CART
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning cart specific information
        for the given order guid.

Input:
        order_guid                      varchar2

Output:
        cursor containing order (shopping cart) information

Notes:
The payment_auth_result and the credit_card_used fields are used
for figuring if add billing is allowed for this cart.  If the auth is null
and a credit card is used, then the cart is allowed for add billing
to enter a auth code.
-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                o.order_guid,
                to_char (o.order_date, 'mm/dd/yyyy') order_date, to_char (o.order_date, 'mm/dd/yyyy hh24:mi:ss') order_timestamp,
                to_char (o.order_date, 'mm/dd/yyyy hh:mi AM') order_timestamp_formatted,
                to_char (o.order_date, 'mm/dd/yyyy') order_date_formatted,
                to_char (o.order_date, 'hh:mi AM') order_time_formatted,
                nvl ( o.order_taken_call_center, o.origin_id ) origin_id,
                o.source_code,
                (select dm.subscribe_status from direct_mail dm where dm.customer_id = o.customer_id and dm.company_id = cm.newsletter_company_id) direct_mail,
                (select em.email_address from email em where em.email_id = o.email_id) email_address,
                m.membership_number,
                m.first_name membership_first_name,
                m.last_name membership_last_name,
                null date_miles_posted, -- m.date_miles_posted,
                null order_total,          --****************************************************
                null product_total,        --
                null add_on_total,         -- the total fields are not displayed,
                null service_fee_total,    -- get_cart_bills is used to display the cart totals
                null shipping_fee_total,   --
                null discount_total,       --  fields were removed from the table 8/3/05
                null tax_total,            --****************************************************
                a.first_name alt_first_name,
                a.last_name alt_last_name,
                a.email_address alt_email_address,
                a.phone_number alt_phone_number,
                a.extension alt_extension,
                o.membership_id,
                o.master_order_number,
                o.customer_id,
                clean.comment_history_pkg.customer_has_comments (o.customer_id) customer_comment_indicator,
                is_weboe_origin (o.origin_id) weboe_indicator,
                o.loss_prevention_indicator,
                cm.company_name,
                o.company_id,
                p.auth_result payment_auth_result,
                decode (p.cc_id, null, 'N', 'Y') credit_card_used,
                p.auth_override_flag,
                nvl ((select distinct 'Y' from order_bills ob join order_details od on ob.order_detail_id = od.order_detail_id where ob.bill_status IN ('Billed', 'Settled', 'Reconciled') and od.order_guid = o.order_guid and ob.additional_bill_indicator = 'N'), 'N') eod_indicator,
                pm.payment_type,
                pm.description payment_type_desc,
                pm.payment_method_id payment_method_id,
                p.ap_account_id,
                p.ap_auth_txt,
                p.auth_number,
                (SELECT  distinct pm.partner_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = 'Y' AND spr.source_code = o.source_code
                ) partner_name,
                m.membership_type,
                cm.default_dnis,
                o.is_joint_cart
        FROM    orders o
        JOIN    ftd_apps.company_master cm
        ON      o.company_id = cm.company_id
        LEFT OUTER JOIN payments p
        ON      o.order_guid = p.order_guid
        AND     p.additional_bill_id IS NULL
        AND     p.refund_id IS NULL
        AND     (p.cc_id IS NOT NULL OR p.ap_account_id IS NOT NULL)
        LEFT OUTER JOIN memberships m
        ON      o.membership_id = m.membership_id
        LEFT OUTER JOIN order_contact_info a
        ON      o.order_guid = a.order_guid
        LEFT OUTER JOIN ftd_apps.payment_methods pm
  ON      pm.payment_method_id = p.payment_type

        WHERE   o.order_guid = in_order_guid;

END GET_CART;



PROCEDURE GET_CART_BILLS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the shopping cart
        billing amounts

Input:
        order_guid                      varchar2

Output:
        cursor containing billing totals

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                decode (ob.additional_bill_indicator, 'Y', 'Additional Bill', 'Original') total_type,
                sum (ob.product_amount) product_amount,
                sum (ob.add_on_amount) add_on_amount,
                sum (ob.service_fee) service_fee,
                sum (ob.shipping_fee) shipping_fee,
                sum (ob.service_fee) + sum (ob.shipping_fee) serv_ship_fee,
                sum (ob.discount_amount) discount_amount,
                sum (ob.shipping_tax) + sum (ob.tax) tax,
                sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee) + sum (ob.shipping_fee) - sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax) bill_total,
                sum (ob.miles_points_amt) miles_points_amount,
                sum (ob.DTL_FUEL_SURCHARGE_AMT) fuel_surcharge_amount ,
                sum (ob.DTL_VENDOR_SAT_UPCHARGE) vend_sat_upcharge_amount ,
                sum (ob.DTL_AK_HI_SPECIAL_SVC_CHARGE) ak_hi_special_surcharge,
                (SELECT 'ON' FROM dual WHERE EXISTS
                (SELECT 1
                FROM clean.order_details od1
                WHERE od1.order_guid         = in_order_guid
                AND od1.apply_surcharge_code = 'ON'
                ) ) apply_surcharge_code,
                sum (ob.same_day_upcharge) same_day_upcharge,
                sum (ob.morning_delivery_fee) morning_delivery_fee,
                sum (ob.add_on_discount_amount) add_on_discount_amount,
                sum (ob.vendor_sun_upcharge) vendor_sun_upcharge,
                sum (ob.vendor_mon_upcharge) vendor_mon_upcharge,
                sum (ob.late_cutoff_fee) late_cutoff_fee,
                sum (Ob.Dtl_First_Order_International) international_Fee
        FROM    order_bills ob
        JOIN    order_details od
        ON      ob.order_detail_id = od.order_detail_id
        WHERE   od.order_guid = in_order_guid
        GROUP BY ob.additional_bill_indicator ;


END GET_CART_BILLS;


PROCEDURE GET_CART_REFUNDS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for shopping cart refunds

Input:
        order_guid                      varchar2

Output:
        cursor containing refund totals

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                'Refund' refund_type,
                sum (r.refund_product_amount) refund_product_amount,
                sum (r.refund_addon_amount) refund_addon_amount,
                sum (r.refund_service_fee + r.refund_shipping_fee) refund_serv_ship_fee,
                sum (r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) refund_tax,
                sum (r.refund_product_amount - r.refund_discount_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) refund_total,
                sum (r.refund_discount_amount) refund_discount_amount,
                sum (p.miles_points_debit_amt) miles_points_amount,
                sum (r.refund_addon_discount_amt) refund_addon_discount_amount
        FROM    refund r
        JOIN    order_details od
        ON      r.order_detail_id = od.order_detail_id
        JOIN    payments p
        ON      p.refund_id = r.refund_id
        WHERE   od.order_guid = in_order_guid;

END GET_CART_REFUNDS;


PROCEDURE GET_CART_TOTALS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order totals
        for the given order guid

Input:
        order guid                      varchar2

Output:
        cursor containing cart total information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                'Modified Total' total_type,
                a.product_amount - nvl(b.refund_product_amount,0) product_amount,
                a.add_on_amount - nvl(b.refund_addon_amount,0) add_on_amount,
                a.serv_ship_fee - nvl(b.refund_serv_ship_fee,0) serv_ship_fee,
                a.discount_amount - nvl(b.refund_discount_amount, 0) discount_amount,
                a.tax - nvl(b.refund_tax,0) tax,
                a.bill_total - nvl(b.refund_total,0) cart_total,
                nvl(a.miles_points_amount, 0) - nvl(b.miles_points_amount, 0) miles_points_total,
                nvl(a.add_on_discount_amount, 0) - nvl(b.refund_addon_discount_amount, 0) add_on_discount_amount
        FROM
        (
                SELECT
                        sum (ob.product_amount) product_amount,
                        sum (ob.add_on_amount) add_on_amount,
                        sum (ob.service_fee) + sum (ob.shipping_fee) serv_ship_fee,
                        sum (ob.discount_amount) discount_amount,
                        sum (ob.shipping_tax) + sum (ob.tax) tax,
                        sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee) + sum (ob.shipping_fee) - sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax) bill_total,
                        sum (ob.miles_points_amt) miles_points_amount,
                        sum (ob.add_on_discount_amount) add_on_discount_amount
                FROM    order_bills ob
                JOIN    order_details od
                ON      ob.order_detail_id = od.order_detail_id
                WHERE   od.order_guid = in_order_guid
        ) a
        CROSS JOIN
        (
                SELECT
                        sum (r.refund_product_amount) refund_product_amount,
                        sum (r.refund_addon_amount) refund_addon_amount,
                        sum (r.refund_service_fee + r.refund_shipping_fee) refund_serv_ship_fee,
                        sum (r.refund_discount_amount) refund_discount_amount,
                        sum (r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) refund_tax,
                        sum (r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee - r.refund_discount_amount + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) refund_total,
                        sum (p.miles_points_debit_amt) miles_points_amount,
                        sum (r.refund_addon_discount_amt) refund_addon_discount_amount
                FROM    refund r
                JOIN    order_details od
                ON      r.order_detail_id = od.order_detail_id
                JOIN    payments p
                ON      p.refund_id = r.refund_id
                WHERE   od.order_guid = in_order_guid
        ) b;

END GET_CART_TOTALS;


PROCEDURE GET_CART_HOLD
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for hold information for the customer of
        the cart

Input:
        order_guid                      varchar2

Output:
        cursor containing hold reason

-----------------------------------------------------------------------------*/
-- index by table to store cursor result sets
TYPE varchar_tab_typ IS TABLE OF VARCHAR2(200) INDEX BY PLS_INTEGER;
TYPE number_tab_typ IS TABLE OF NUMBER INDEX BY PLS_INTEGER;

CURSOR cust_cur IS
        select  customer_id
        from    orders
        where   order_guid = in_order_guid;

CURSOR order_cur IS
        select  order_detail_id
        from    order_details
        where   order_guid = in_order_guid;

v_customer_id   orders.customer_id%type;

v_cust_cur      types.ref_cursor;
v_cc_cur        types.ref_cursor;
v_phone_cur     types.ref_cursor;
v_member_cur    types.ref_cursor;
v_email_cur     types.ref_cursor;
v_lp_cur        types.ref_cursor;
v_ap_cur        types.ref_cursor;

v_odid_tab      number_tab_typ;

-- generic variable tables to hold cursor result sets
v_num_tab1      number_tab_typ;
v_num_tab2      number_tab_typ;
v_num_tab3      number_tab_typ;
v_num_tab4      number_tab_typ;

v_char_tab1     varchar_tab_typ;
v_char_tab2     varchar_tab_typ;
v_char_tab3     varchar_tab_typ;
v_char_tab4     varchar_tab_typ;
v_char_tab5     varchar_tab_typ;
v_char_tab6     varchar_tab_typ;
v_char_tab7     varchar_tab_typ;
v_char_tab8     varchar_tab_typ;

v_reason_tab1   varchar_tab_typ;
v_reason_tab2   varchar_tab_typ;
v_reason_tab3   varchar_tab_typ;

BEGIN

EXECUTE IMMEDIATE ('TRUNCATE TABLE CUSTOMER_HOLD_TEMP');

OPEN cust_cur;
FETCH cust_cur INTO v_customer_id;
CLOSE cust_cur;

OPEN order_cur;
FETCH order_cur BULK COLLECT INTO v_odid_tab;
CLOSE order_cur;

FOR x IN 1..v_odid_tab.count LOOP
        -- get the customer hold information
        GET_CUSTOMER_HOLD_ORDER_INFO
        (
                IN_ORDER_DETAIL_ID=>to_char (v_odid_tab(x)),
                IN_CUTOMER_TYPE=>'B',
                OUT_CURSOR_CUSTOMER=>v_cust_cur,
                OUT_CURSOR_CREDIT_CARDS=>v_cc_cur,
                OUT_CURSOR_PHONES=>v_phone_cur,
                OUT_CURSOR_MEMBERSHIPS=>v_member_cur,
                OUT_CURSOR_EMAILS=>v_email_cur,
                OUT_CURSOR_LOSS_PREVENTION=>v_lp_cur,
                OUT_CURSOR_AP_ACCOUNTS=>v_ap_cur
        );

        FETCH v_cust_cur BULK COLLECT INTO
                v_num_tab1,
                v_reason_tab1,
                v_num_tab2,
                v_char_tab1,
                v_char_tab2,
                v_reason_tab2,
                v_num_tab3,
                v_char_tab3,
                v_char_tab4,
                v_char_tab5,
                v_char_tab6,
                v_char_tab7,
                v_char_tab8,
                v_reason_tab3,
                v_num_tab4;
        CLOSE v_cust_cur;

        -- store customer id hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));
        -- store customer name hold reasons
        FORALL x IN 1..v_reason_tab2.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab2(x));
        -- store customer address hold reasons
        FORALL x IN 1..v_reason_tab3.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab3(x));


        FETCH v_cc_cur BULK COLLECT INTO
                v_char_tab1,
                v_reason_tab1,
                v_num_tab1,
                v_char_tab2;
        CLOSE v_cc_cur;

        -- store the credit card hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));

        FETCH v_phone_cur BULK COLLECT INTO
                v_char_tab1,
                v_reason_tab1,
                v_num_tab1;
        CLOSE v_phone_cur;

        -- store the phone number hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));

        FETCH v_member_cur BULK COLLECT INTO
                v_char_tab1,
                v_char_tab2,
                v_reason_tab1,
                v_num_tab1;
        CLOSE v_member_cur;

        -- store the membership hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));

        FETCH v_email_cur BULK COLLECT INTO
                v_char_tab1,
                v_reason_tab1,
                v_num_tab1;
        CLOSE v_email_cur;

        -- store the email hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));

        FETCH v_lp_cur BULK COLLECT INTO
                v_char_tab1,
                v_reason_tab1,
                v_num_tab1;
        CLOSE v_lp_cur;

        -- store the loss prevention hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));

        FETCH v_ap_cur BULK COLLECT INTO
                v_num_tab1,
                v_char_tab2,
                v_char_tab3,
                v_reason_tab1,
                v_num_tab1;
        CLOSE v_ap_cur;

        -- store the alternate payment hold reasons
        FORALL x IN 1..v_reason_tab1.count
                INSERT INTO customer_hold_temp VALUES (v_customer_id, v_reason_tab1(x));

END LOOP;

OPEN OUT_CUR FOR
        SELECT  distinct
                hv.description customer_hold_description
        FROM    customer_hold_temp cht
        JOIN    customer_hold_reason_val hv
        ON      cht.hold_reason_code = hv.hold_reason_code
        WHERE   cht.customer_id = v_customer_id
        ORDER BY hv.description;


END GET_CART_HOLD;


PROCEDURE GET_CART_ORDERS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the orders within a
        shopping cart

Input:
        order_guid                      varchar2

Output:
        cursor containing order detail ids

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                od.order_detail_id
        FROM    order_details od
        WHERE   od.order_guid = in_order_guid
        ORDER BY external_order_number;

END GET_CART_ORDERS;


PROCEDURE GET_ORDER_DETAILS
(
IN_ID_STR                       IN VARCHAR2,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order information
        for the order detail ids passed in the string id list

Input:
        string containing order detail ids
        order number - original searched for order used to return an indicator
                        for displaying the recipient/order page

Output:
        cursor containing order information

Notes:
user_id is the last csr to scrub the order, or the csr that placed the order in
   weboe, or the last person to update the order (if it wasn't a weboe order or scrubbed).
order_search_indicator is Y or N if the order record is the record that the
   csr searched for.  This is to enable highlighting of the tabs in COM.


-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        'SELECT
                od.order_detail_id,
                od.external_order_number,
                scrubbed_on scrubbed_on_date,
                coalesce (scrubbed_by, o.order_taken_by, od.updated_by) user_id,
                od.source_code,
                r.customer_id recipient_id,
                r.first_name,
                r.last_name,
                r.address_1,
                r.address_2,
                r.city,
                r.state,
                r.zip_code,
                r.country,
                r.address_type,
                r.business_name,
                to_char (od.delivery_date, ''mm/dd/yyyy'') delivery_date,
                to_char (od.delivery_date_range_end, ''mm/dd/yyyy'') delivery_date_range_end,
                od.product_id,
                p.product_name,
                p.short_description,
                od.substitution_indicator,
                global.global_pkg.GET_COLOR_BY_ID(od.color_1) color1_description,
                global.global_pkg.GET_COLOR_BY_ID(od.color_2) color2_description,
                od.florist_id,
                f.florist_name,
                f.phone_number florist_phone_number,
                f.super_florist_flag,
                f.mercury_flag,
                f.status florist_status,
                od.occasion,
                od.card_message,
                od.card_signature,
                od.release_info_indicator,
                od.special_instructions,
                decode (od.order_detail_id, :1, ''Y'', ''N'') order_search_indicator,
                clean.order_query_pkg.get_scrub_status_ind (od.order_disp_code) scrub_status,
                decode (od.order_disp_code, ''Held'', ''Y'', ''N'') order_held,
                od.order_guid,
                od.hp_order_number,
                od.quantity,
                od.color_1,
                od.color_2,
                od.substitution_indicator,
                od.second_choice_product,
                od.same_day_gift,
                od.ship_method,
                od.ship_date,
                decode ( (select odv.display_indicator from clean.order_disposition_val odv where odv.order_disp_code = od.order_disp_code), ''Y'',
                         od.order_disp_code, null) order_disp_code,
                clean.comment_history_pkg.order_has_comments (od.order_detail_id) order_comment_indicator,
                clean.queue_pkg.is_order_tagged (od.order_detail_id, ''' || in_csr_id || ''') order_tagged_indicator,
                coalesce (customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( r.customer_id,''Day''),
                          customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( r.customer_id,''Evening'') ) recipient_phone_number,
                decode (customer_query_pkg.GET_CUSTOMER_PHONE_by_id_type( r.customer_id,''Day''), null,
                          customer_query_pkg.GET_CUSTOMER_ext_by_id_type( r.customer_id,''Evening''),
                          customer_query_pkg.GET_CUSTOMER_ext_by_id_type( r.customer_id,''Day'')) recipient_extension,
                decode (od.ship_method, null, ''N'', ''SD'', ''N'', ''Y'') vendor_flag,
                (select ob.discount_product_price from order_bills ob where od.order_detail_id = ob.order_detail_id and ob.additional_bill_indicator = ''N'') discount_product_price,
                clean.order_query_pkg.is_order_cancelled (od.order_detail_id) cancelled_flag,
                (select decode (h.reason, ''CUSTOMER'', h.reason_text, h.reason) from order_hold h where od.order_detail_id = h.order_detail_id) hold_reason,
                (select sm.time_zone from ftd_apps.state_master sm join customer r on sm.state_master_id = r.state where r.customer_id = od.recipient_id) timezone,
                (select min (t.tracking_number) from order_tracking t where t.order_detail_id = od.order_detail_id) tracking_number,
                clean.point_of_contact_pkg.order_has_emails (od.order_detail_id) order_emails_indicator,
                decode ( (select count(*) from clean.refund r
                      where r.order_detail_id = od.order_detail_id) , 0, ''N'', ''Y'') order_refund_indicator,
                od.miles_points,
                od.ariba_po_number,
                od.size_indicator,
                (select to_char(o.order_date, ''mm/dd/yyyy'') from clean.orders o where o.order_guid = od.order_guid) order_date,
                od.subcode,
                decode( od.ship_method, null, null, ftd_apps.product_query_pkg.GET_ship_method_by_id(od.ship_method) ) ship_method_desc,
                od.reject_retry_count,
                order_mesg_pkg.is_order_ftdm(od.order_detail_id) ftdm_indicator,
                decode ((select pm.SHIPPING_SYSTEM from ftd_apps.product_master pm where pm.product_id = od.product_id), ''FTP'', ''Y'', ''N'') ftp_vendor_product,
                (select occ.description from ftd_apps.occasion occ where occ.occasion_id = od.occasion) occasion_description,
                cm.oe_country_type international_flag,
                (select sm.description from ftd_apps.source_master sm where sm.source_code = od.source_code) source_code_description,
                p.product_type,
                upper(cm.name) country_name,
                queue_pkg.is_order_in_credit_q (od.order_detail_id) order_in_credit_q,
                decode( od.subcode, null, null, ftd_apps.product_query_pkg.get_subcode_reference_number (od.subcode) ) subcode_description,
                od.carrier_delivery,
                decode(od.carrier_delivery, null, null, (select cd.carrier_id from venus.carrier_delivery cd where cd.carrier_delivery = od.carrier_delivery) ) carrier_id,
                decode(od.carrier_delivery, null, null, venus.ship_pkg.get_car_name_by_od(od.carrier_delivery) ) carrier_name,
                decode(od.carrier_delivery, null, null, venus.ship_pkg.get_car_method_of_pmt_by_od(od.carrier_delivery) )method_of_payment,
                od.op_status,
        to_char (scrubbed_on, ''mm/dd/yyyy hh24:mi:ss'') scrubbed_on_text,
        od.vendor_id,
        od.actual_product_amount,
        od.actual_add_on_amount,
          od.actual_shipping_fee,
          decode(od.carrier_delivery, null, null, (select cs.website_url from venus.carrier_delivery cd, venus.carriers cs where cd.carrier_delivery = od.carrier_delivery and cd.carrier_id = cs.carrier_id)) carrier_url,
          nvl(p.shipping_system,''NONE'') shipping_system,
          od.personalization_data,
          decode(dbms_lob.substr(od.personalization_data, 4000, 1), null, null, (ftd_apps.misc_pkg.get_pdp_personalization_str(od.order_detail_id))) pdp_personalization_data,
                p.personalization_template_id,
                od.personal_greeting_id,
                p.no_tax_flag,
                (SELECT  distinct pm.partner_name FROM ftd_apps.source_program_ref spr JOIN ftd_apps.partner_program pp ON pp.program_name = spr.program_name JOIN ftd_apps.partner_master pm ON pm.partner_name = pp.partner_name WHERE pm.preferred_partner_flag = ''Y'' AND spr.source_code = od.source_code
                 ) partner_name,
                p.premier_collection_flag,
                (select description from frp.address_types at where at.address_type = r.address_type) as address_type_description,
                p.product_type,
                p.product_sub_type,
                o.language_id,
                (select decode(ob1.morning_delivery_fee,NULL,''N'',''Y'') from clean.order_bills ob1 where od.order_detail_id = ob1.order_detail_id and ob1.additional_bill_indicator = ''N'') order_has_morning_delivery,
                (select orig_external_order_number from clean.mo_xref where orig_order_detail_id = od.order_detail_id) as orig_external_order_number,
                p.novator_id,
                od.avs_address_id,
                od.actual_add_on_discount_amount,
                (select v.shipped
  		            from venus.venus v
		            where v.reference_number = to_char(od.order_detail_id)
		            and v.msg_type = ''FTD''
		            and v.created_on = (select min(created_on)
		                from venus.venus v1
		                where v1.reference_number = to_char(od.order_detail_id)
                        and v1.msg_type = ''FTD'')) venus_ship_date,
                (select listagg(oao.add_on_code, '','')
                    within group (order by oao.add_on_code)
                    from clean.order_add_ons oao
                    where oao.order_detail_id = od.order_detail_id) order_add_ons,
                od.legacy_id,
                order_query_pkg.get_order_detail_status(od.order_detail_id) order_detail_status,
                order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) lifecycle_delivered_status,
                case when clean.order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) = ''Y'' THEN
                (
                    select status_url
                    from mercury.mercury m
                    join mercury.lifecycle_status ls
                    on ls.mercury_id = m.mercury_id
                    and ls.order_status_code = ''C''
                    where m.reference_number = to_char(od.order_detail_id)
                    and m.msg_type = ''FTD''
                    and m.created_on = (select max(m1.created_on)
                        from mercury.mercury m1
                        where m1.reference_number = m.reference_number
                        and m1.msg_type = ''FTD'')
                    and rownum = 1
                )
                end as status_url,
                case when clean.order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) = ''Y'' THEN
                (
                    select decode(status_url, null, ''N'', case when sysdate - ls.status_timestamp <= gp1.value then ''Y'' else ''N'' end)
                    from mercury.mercury m
                    join mercury.lifecycle_status ls
                    on ls.mercury_id = m.mercury_id
                    and ls.order_status_code = ''C''
                    left outer join frp.global_parms gp1
                    on gp1.context = ''ORDER_LIFECYCLE''
                    and gp1.name = ''LIFECYCLE_DELIVERY_EXPIRED_DAYS''
                    where m.reference_number = to_char(od.order_detail_id)
                    and m.msg_type = ''FTD''
                    and m.created_on = (select max(m1.created_on)
                        from mercury.mercury m1
                        where m1.reference_number = m.reference_number
                        and m1.msg_type = ''FTD'')
                    and rownum = 1
                )
                end as status_url_active,
                gp.value status_url_expired_msg,
                o.company_id,
                o.origin_id,
                to_char(od.delivery_date,''Day'') delivery_date_dow,
                to_char(od.delivery_date_range_end,''Day'') delivery_date_range_end_dow,
                (select to_char(to_date(fz.cutoff_time,''hh24mi''),''hh:mi AM'') from FTD_APPS.FLORIST_ZIPS fz 
                  where fz.florist_id = od.florist_id and fz.zip_code = ftd_apps.FLORIST_QUERY_PKG.FORMAT_ZIP_CODE_II(r.zip_code)) zipcode_cutoff,
                nvl(f.MINIMUM_ORDER_AMOUNT,0) minimum_order_amount,
                (select ''Y'' from ftd_apps.codified_products where product_id = od.product_id) codified_product,
                (select ''Y''
                 from    ftd_apps.florist_codifications fc
                 join    ftd_apps.codified_products cp
                 on      fc.codification_id = cp.codification_id
                 where   fc.florist_id = f.florist_id
                 and     cp.product_id = od.product_id
                 and     (fc.block_start_date >= sysdate
                 or      fc.block_start_date is null
                 or      fc.block_end_date <= sysdate) ) florist_codified,
                 (select ''Y''
                 from    ftd_apps.florist_codifications fc
                 join    ftd_apps.codified_products cp
                 on      fc.codification_id = cp.codification_id
                 where   fc.florist_id = f.florist_id
                 and     cp.product_id = od.product_id
                 AND (   ((fc.block_start_date IS NULL) AND (TRUNC(fc.block_end_date) >= TRUNC(sysdate)))
                        OR ((TRUNC(fc.block_start_date) <= TRUNC(sysdate)) AND (fc.block_end_date IS NULL))
                        OR (TRUNC(sysdate) BETWEEN TRUNC(fc.block_start_date) AND TRUNC(fc.block_end_date))
                     )
                 ) florist_codified_blocked,                
                ((select value from frp.global_parms where context=''OE_CONFIG'' and name=''IMAGE_SERVER_URL'') || ''/'' || p.novator_id || ''_a.jpg'') as small_image_url,
                (select t.carrier_id from order_tracking t where t.order_detail_id = od.order_detail_id) shipping_partner,
                DECODE(od.order_disp_code, ''Printed'', ''Printed'', ''Shipped'', ''Shipped'') as shipping_status,
                (select DECODE(v.rejected, null, null, ''Rejected'') from venus.venus v where v.reference_number = to_char(od.order_detail_id) and v.msg_type = ''FTD'' 
                and v.created_on = (select max(v1.created_on) from venus.venus v1 where v1.reference_number = v.reference_number and v1.msg_type = ''FTD'')
                ) rejected_status,
                (select DECODE(v.delivery_scan, null, null, ''Delivered'') from venus.venus v where v.reference_number = to_char(od.order_detail_id) and v.msg_type = ''FTD''
                and v.created_on = (select max(v1.created_on) from venus.venus v1 where v1.reference_number = v.reference_number and v1.msg_type = ''FTD'')
                ) delivered_status,
                (select DECODE(v.cancelled, null, null, ''Cancelled'') from venus.venus v where v.reference_number = to_char(od.order_detail_id) and v.msg_type = ''FTD''
                and v.created_on = (select max(v1.created_on) from venus.venus v1 where v1.reference_number = v.reference_number and v1.msg_type = ''FTD'')
                ) cancelled_status,
                (select DECODE(v.printed, null, null, ''Printed'') from venus.venus v where v.reference_number = to_char(od.order_detail_id) and v.msg_type = ''FTD''
                and v.created_on = (select max(v1.created_on) from venus.venus v1 where v1.reference_number = v.reference_number and v1.msg_type = ''FTD'')
                ) printed_status,
                (select DECODE(v.shipped, null, null, ''Shipped'') from venus.venus v where v.reference_number = to_char(od.order_detail_id) and v.msg_type = ''FTD''
                and v.created_on = (select max(v1.created_on) from venus.venus v1 where v1.reference_number = v.reference_number and v1.msg_type = ''FTD'')
                ) shipped_status,                
                (select cs2.tracking_url from venus.carriers cs2, clean.order_tracking t2 where t2.order_detail_id = od.order_detail_id
                        and cs2.carrier_id = t2.carrier_id) tracking_url,
                (select ph.description from ftd_apps.source_master sm2, ftd_apps.price_header ph where sm2.source_code = od.source_code 
                        and sm2.price_header_id = ph.price_header_id) price_description                                    
        FROM    clean.order_details od
        LEFT OUTER JOIN  ftd_apps.product_master p
                ON od.product_id = p.product_id
        LEFT OUTER JOIN  clean.customer r
                ON od.recipient_id = r.customer_id
        LEFT OUTER JOIN  ftd_apps.country_master cm
                ON cm.country_id = r.country
                AND cm.status = ''Active''
        LEFT OUTER JOIN ftd_apps.florist_master f
                ON od.florist_id = f.florist_id
        JOIN    clean.orders o
                ON  o.order_guid = od.order_guid
        LEFT OUTER JOIN  frp.global_parms gp
                ON  gp.context = ''ORDER_LIFECYCLE''
                AND  gp.name = ''LIFECYCLE_DELIVERY_EXPIRED_MSG''
        WHERE  od.order_detail_id IN (' || in_id_str || ') ' ||
        'ORDER BY od.external_order_number '
        USING   in_order_detail_id;

END GET_ORDER_DETAILS;


PROCEDURE GET_ORDER_DETAILS
(
IN_ID_STR                       IN VARCHAR2,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order information
        for the order detail ids passed in the string id list

Input:
        string containing order detail ids
        order number - original searched for order used to return an indicator
                        for displaying the recipient/order page

Output:
        cursor containing order information

-----------------------------------------------------------------------------*/
BEGIN

GET_ORDER_DETAILS (IN_ID_STR, IN_ORDER_DETAIL_ID, NULL, OUT_CUR);

END GET_ORDER_DETAILS;

PROCEDURE GET_ORDER_ADD_ON
(
IN_ID_STR                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order add ons
        for the order detail ids passed in the string

Input:
        string containing order detail ids

Output:
        cursor containing order add on information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        'SELECT
                a.order_detail_id,
                a.order_add_on_id,
                a.add_on_code,
                a.add_on_quantity,
                nvl (fa.description, ''' || frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'DESCRIPTION') || ''') description,
                nvl (fa.price, ' || frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'PRICE') || ') price,
                nvl (fa.addon_type, ''' || frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'ADDON_TYPE') || ''') addon_type,
                nvl (a.add_on_amount, 0) add_on_sale_price,
                a.add_on_discount_amount
        FROM    order_add_ons a
        LEFT OUTER JOIN    ftd_apps.addon_history fa
        ON      a.add_on_history_id = fa.addon_history_id
        WHERE   a.order_detail_id IN (' || in_id_str || ')
        ORDER BY (select at.description from ftd_apps.addon_type at where fa.addon_type = at.addon_type_id) ';

END GET_ORDER_ADD_ON;


PROCEDURE GET_ORDER_BILLS
(
IN_ID_STR                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order bills
        for the order detail ids passed in the string

Input:
        string containing order detail ids

Output:
        cursor containing order bill on information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        'SELECT
                ob.order_detail_id,
                decode (ob.additional_bill_indicator, ''Y'', ''Additional Bill'', ''Original'') total_type,
                sum (ob.product_amount) product_amount,
                sum (ob.add_on_amount) add_on_amount,
                sum (ob.service_fee) + sum (ob.shipping_fee) serv_ship_fee,
                sum (ob.discount_amount) discount_amount,
                sum (ob.shipping_tax) + sum (ob.tax) tax,
                sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee) + sum (ob.shipping_fee) - sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax) bill_total,
                sum (ob.miles_points_amt) miles_points_amount ,
                sum (ob.service_fee) service_fee ,
                sum (ob.shipping_fee) shipping_fee ,
                sum (ob.dtl_fuel_surcharge_amt) fuel_surcharge_amt ,
                sum (ob.dtl_vendor_sat_upcharge) vend_sat_upcharge,
                sum (ob.dtl_ak_hi_special_svc_charge)ak_hi_special_charge,
                od.surcharge_description,
                od.apply_surcharge_code,
                sum (ob.same_day_upcharge) same_day_upcharge,
                sum (ob.morning_delivery_fee) morning_delivery_fee,
                sum (ob.add_on_discount_amount) add_on_discount_amount,
                sum (ob.vendor_sun_upcharge) vendor_sun_upcharge,
                sum (ob.vendor_mon_upcharge) vendor_mon_upcharge,
                sum (ob.late_cutoff_fee) late_cutoff_fee,
                sum (Ob.Dtl_First_Order_International) international_fee,
                od.free_shipping_flag ,
                od.original_order_has_sdu,
                od.external_order_number
        FROM    order_bills ob
        JOIN  CLEAN.ORDER_DETAILS od on  ob.order_detail_id = od.order_detail_id
        WHERE   ob.order_detail_id IN (' || in_id_str || ') ' ||
        'GROUP BY ob.order_detail_id, ob.additional_bill_indicator, od.surcharge_description, od.apply_surcharge_code, od.free_shipping_flag , od.original_order_has_sdu, od.external_order_number ';

END GET_ORDER_BILLS;


PROCEDURE GET_ORDER_REFUNDS
(
IN_ID_STR                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order refunds
        for the order detail ids passed in the string

Input:
        string containing order detail ids

Output:
        cursor containing order bill on information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        'SELECT
                r.order_detail_id,
                ''Refund'' refund_type,
                sum (r.refund_product_amount) product_amount,
                sum (r.refund_addon_amount) addon_amount,
                sum (r.refund_service_fee + r.refund_shipping_fee) serv_ship_fee,
                sum (r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) tax,
                sum (r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + +r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax - r.refund_discount_amount) refund_total,
                sum (r.refund_discount_amount) discount_amount,
                sum (p.miles_points_debit_amt) miles_points_amount,
                sum (r.refund_addon_discount_amt) addon_discount_amount
        FROM    refund r
        JOIN    payments p
        ON      p.refund_id = r.refund_id
        WHERE   r.order_detail_id IN (' || in_id_str || ') ' ||
        'GROUP BY r.order_detail_id ';

END GET_ORDER_REFUNDS;


PROCEDURE GET_ORDER_TOTALS
(
IN_ID_STR                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order totals
        for the order detail ids passed in the string

Input:
        string containing order detail ids

Output:
        cursor containing order bill on information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        'SELECT
                a.order_detail_id,
                ''Modified Total'' total_type,
                a.product_amount - nvl(b.product_amount,0) product_amount,
                a.add_on_amount - nvl(b.add_on_amount,0) add_on_amount,
                a.serv_ship_fee - nvl(b.serv_ship_fee,0) serv_ship_fee,
                a.discount_amount - nvl (b.discount_amount, 0) discount_amount,
                a.tax - nvl(b.tax,0) tax,
                a.bill_total - nvl(b.refund_total,0) order_total,
                nvl(a.miles_points_amount, 0) - nvl(b.miles_points_amount, 0) miles_points_total,
                nvl(a.add_on_discount_amount, 0) - nvl(b.refund_addon_discount_amount, 0) add_on_discount_amount
        FROM
        (
                select  ob.order_detail_id,
                        sum (ob.product_amount) product_amount,
                        sum (ob.add_on_amount) add_on_amount,
                        sum (ob.service_fee) + sum (ob.shipping_fee) serv_ship_fee,
                        sum (ob.discount_amount) discount_amount,
                        sum (ob.shipping_tax) + sum (ob.tax) tax,
                        sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee) + sum (ob.shipping_fee) - sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax) bill_total,
                        sum (ob.miles_points_amt) miles_points_amount,
                        sum (ob.add_on_discount_amount) add_on_discount_amount
                from    order_bills ob
                where   ob.order_detail_id IN (' || in_id_str || ') ' ||
               'group by ob.order_detail_id
        ) a
        LEFT OUTER JOIN
        (
                select  r.order_detail_id,
                        ''Refund'' refund_type,
                        sum (r.refund_product_amount) product_amount,
                        sum (r.refund_addon_amount) add_on_amount,
                        sum (r.refund_service_fee + r.refund_shipping_fee) serv_ship_fee,
                        sum (r.refund_discount_amount) discount_amount,
                        sum (r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) tax,
                        sum (r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee - r.refund_discount_amount + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax) refund_total,
                        sum (p.miles_points_debit_amt) miles_points_amount,
                        sum (r.refund_addon_discount_amt) refund_addon_discount_amount
                from    refund r
                join    clean.payments p
                on      p.refund_id = r.refund_id
                where   r.order_detail_id IN (' || in_id_str || ') ' ||
               'group by r.order_detail_id
        ) b
        ON      a.order_detail_id = b.order_detail_id ';

END GET_ORDER_TOTALS;


PROCEDURE GET_ORDER_TRACKING
(
IN_ID_STR                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order tracking numbers
        and related data for the order detail ids passed in the string

Input:
        string containing order detail ids

Output:
        cursor containing order tracking information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR

        'SELECT
                t.order_detail_id,
                t.tracking_number,
                t.carrier_id,
                v.carrier_name,
                v.website_url,
                v.phone_number,
         v.tracking_url || t.tracking_number tracking_url
        FROM    order_tracking t, venus.carriers v
        WHERE   t.carrier_id = v.carrier_id
        AND     t.order_detail_id IN (' || in_id_str || ') ';

END GET_ORDER_TRACKING;


PROCEDURE GET_ORDER_INFO
(
IN_ID_STR                       IN VARCHAR2,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
IN_PROCESS_ID                   IN VARCHAR2,
OUT_ORDERS_CUR                  OUT TYPES.REF_CURSOR,
OUT_ORDER_ADDON_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_BILLS_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_REFUNDS_CUR           OUT TYPES.REF_CURSOR,
OUT_ORDER_TOTAL_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order information
        for the order detail ids passed in the xml document
        ** ADDED parameter to accept processing id and call appropriate
           get_order_details procedure.

Input:
        string containing order detail ids
        order number - original searched for order used to return an indicator
                        for displaying the order tab
  csr_id
  process id - currently accept CUSTOMER or PRODUCT


Output:
        cursor containing order information

-----------------------------------------------------------------------------*/
BEGIN

if in_process_id = 'CUSTOMER' then
   orderdetails_query_pkg.get_od_cust_details ( IN_ORDER_DETAIL_ID, OUT_ORDERS_CUR);
else
  if in_process_id = 'PRODUCT' then
     orderdetails_query_pkg.get_od_prod_florist_details (IN_ORDER_DETAIL_ID, OUT_ORDERS_CUR);
  else
     get_order_details (IN_ID_STR, IN_ORDER_DETAIL_ID, IN_CSR_ID, OUT_ORDERS_CUR);
  end if;
end if;

GET_ORDER_ADD_ON (IN_ID_STR, OUT_ORDER_ADDON_CUR);
GET_ORDER_BILLS (IN_ID_STR, OUT_ORDER_BILLS_CUR);
GET_ORDER_REFUNDS (IN_ID_STR, OUT_ORDER_REFUNDS_CUR);
GET_ORDER_TOTALS (IN_ID_STR, OUT_ORDER_TOTAL_CUR);

END GET_ORDER_INFO;


PROCEDURE GET_ORDER_INFO
(
IN_ID_STR                       IN VARCHAR2,
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN VARCHAR2,
IN_PROCESS_ID                   IN VARCHAR2,
OUT_ORDERS_CUR                  OUT TYPES.REF_CURSOR,
OUT_ORDER_ADDON_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_BILLS_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_REFUNDS_CUR           OUT TYPES.REF_CURSOR,
OUT_ORDER_TOTAL_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_FEES_SAVED_CUR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
    Overloaded procedure to retrieve order fees savings.
        This procedure is responsible for retrieving order information
        for the order detail ids passed in the xml document
        ** ADDED parameter to accept processing id and call appropriate
           get_order_details procedure.

     ** ADDED parameter to accept processing id and call appropriate
           get_order_details procedure.

Input:
        string containing order detail ids
        order number - original searched for order used to return an indicator
                        for displaying the order tab
  csr_id
  process id - currently accept CUSTOMER or PRODUCT


Output:
        cursor containing order information

-----------------------------------------------------------------------------*/
BEGIN

GET_ORDER_INFO(IN_ID_STR, IN_ORDER_DETAIL_ID, IN_CSR_ID, IN_PROCESS_ID, OUT_ORDERS_CUR, OUT_ORDER_ADDON_CUR, OUT_ORDER_BILLS_CUR, OUT_ORDER_REFUNDS_CUR, OUT_ORDER_TOTAL_CUR);
GET_ORDER_FEES_SAVED(IN_ID_STR, OUT_ORDER_FEES_SAVED_CUR);

END GET_ORDER_INFO;


PROCEDURE GET_CART_INFO
(
IN_ORDER_NUMBER                 IN VARCHAR2,
IN_START_POSITION               IN NUMBER,
IN_MAX_NUMBER_RETURNED          IN NUMBER,
IN_SESSION_ID                   IN CSR_VIEWING_ENTITIES.SESSION_ID%TYPE,
IN_CSR_ID                       IN CSR_VIEWING_ENTITIES.CSR_ID%TYPE,
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CART_CUR                    OUT TYPES.REF_CURSOR,
OUT_CART_BILLS_CUR              OUT TYPES.REF_CURSOR,
OUT_CART_REFUNDS_CUR            OUT TYPES.REF_CURSOR,
OUT_CART_HOLD_CUR               OUT TYPES.REF_CURSOR,
OUT_ORDERS_CUR                  OUT TYPES.REF_CURSOR,
OUT_ORDER_ADDON_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_BILLS_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_REFUNDS_CUR           OUT TYPES.REF_CURSOR,
OUT_VIEWING_CUR                 OUT TYPES.REF_CURSOR,
OUT_NUMBER_OF_ORDERS            OUT NUMBER,
OUT_ORDER_POSITION              OUT NUMBER,
OUT_ORDER_INDICATOR             OUT VARCHAR2,
OUT_ORDER_FOUND                 OUT VARCHAR2,
OUT_CART_TOTAL_CUR              OUT TYPES.REF_CURSOR,
OUT_ORDER_TOTAL_CUR             OUT TYPES.REF_CURSOR,
OUT_CART_FEES_SAVED_CUR         OUT TYPES.REF_CURSOR,
OUT_ORDER_FEES_SAVED_CUR        OUT TYPES.REF_CURSOR,
OUT_CART_TAXES_CUR              OUT TYPES.REF_CURSOR,
OUT_ORDER_TAXES_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order shopping cart
        and order information

Input:
        order_number                    varchar2
        start_position                  number - index of the start item in the id array
        max_number_returned             number - number of items in the result set
        session_id                      varchar2 - session id for viewing info
        csr_id                          varchar2 - viewing csr
        order_guid                      varchar2 - used if searching for an external order number within a shopping cart

Output:
        cursor containing order information

-----------------------------------------------------------------------------*/

v_start_position        number;
v_order_guid            order_details.order_guid%type;
v_order_detail_id       order_details.order_detail_id%type;
v_order_disp_code       order_details.order_disp_code%type;
v_order_indicator       varchar2 (35);

-- cursor to get the order detail ids
v_order_cur             types.ref_cursor;

-- array for of the order detail ids
id_tab                  id_list_pkg.number_tab_typ;

-- clob to store the xml of ids
v_id_xml                clob;

-- string to store the list of ids
v_str_id                varchar2(32767);

BEGIN

-- if the order number is not given but the order guid is given
-- then the application is paging through the shopping cart
IF in_order_number IS NULL and in_order_guid IS NOT NULL THEN
        v_order_guid := in_order_guid;

ELSE
        -- get the order detail id and order guid for the given order number
        FIND_ORDER_NUMBER (in_order_number, v_order_detail_id, v_order_guid, v_order_disp_code, v_order_indicator);

END IF;

-- Check if the given external order number is not found or not part of the cart (in_order_guid is not null).
-- If either is true, the order pages should display items for the original cart at the given start position.
-- So set the search guid to the given order guid, but indicate that the order was not found.
-- All other conditions can check the order guid returned or not from the find order number procedure.
-- If the order guid is null, the order was not found, and skip retrieving any information.
-- If the order guid is not null, the order was found, so retrieve the order information
IF in_order_guid IS NOT NULL AND
   (v_order_guid IS NULL OR v_order_guid <> in_order_guid) THEN
        v_order_guid := in_order_guid;
        out_order_found := 'N';

ELSIF v_order_guid IS NULL THEN
        out_order_found := 'N';

ELSE
        out_order_found := 'Y';

END IF;

-- if the order is found, return the shopping cart and order information for the shopping cart
IF v_order_guid is not null THEN
        GET_CART (v_order_guid, out_cart_cur);
        GET_CART_BILLS (v_order_guid, out_cart_bills_cur);
        GET_CART_REFUNDS (v_order_guid, out_cart_refunds_cur);
        GET_CART_TOTALS (v_order_guid, out_cart_total_cur);
        GET_CART_HOLD (v_order_guid, out_cart_hold_cur);
        GET_CART_FEES_SAVED (v_order_guid, out_cart_fees_saved_cur);
        GET_CART_TAXES (v_order_guid, out_cart_taxes_cur);
        GET_ORDER_TAXES (v_order_guid, out_order_taxes_cur);

        -- get the order details of the cart
        GET_CART_ORDERS (v_order_guid, v_order_cur);
        FETCH v_order_cur BULK COLLECT INTO id_tab;
        CLOSE v_order_cur;

        -- Set the start position to 1 if the total number of orders in the cart is not greater than
        -- the max number displayed on the page.  Otherwise,
        -- Set the start position of the indexes for the string if the start position is not given
        -- this would occur if the csr clicked on the order from the customer account page
        -- or if the initial order search was not an external order number.
        -- If the order number given relates to a specific order (external or hp order number),
        -- then set the start position to that order numbers index

        IF id_tab.count <= in_max_number_returned THEN
                v_start_position := 1;

        ELSIF v_order_indicator = 'external_order_number' THEN
                -- loop through the order array to find the starting position
                FOR x in 1..id_tab.count LOOP
                        IF v_order_detail_id = id_tab (x) THEN
                                v_start_position := x;
                                exit;
                        END IF;
                END LOOP;

        ELSIF in_start_position IS NULL THEN
                v_start_position := 1;

        ELSE
                v_start_position := in_start_position;
        END IF;

        -- get the ids in a string
        id_list_pkg.get_str_id (id_tab, v_start_position, in_max_number_returned, v_str_id);

        -- if in_order_number is null, get the order_detail_id from the start position
        IF in_order_number IS NULL THEN
                v_order_detail_id := id_tab (v_start_position);
        END IF;


        -- get the orders within the cart using the created string
        GET_ORDER_INFO (v_str_id, v_order_detail_id, in_csr_id, null, out_orders_cur, out_order_addon_cur, out_order_bills_cur, out_order_refunds_cur, out_order_total_cur, out_order_fees_saved_cur);

        IF v_order_indicator = 'master_order_number' OR
           v_order_indicator = 'az_order_number' THEN
                -- get the list of csrs viewing the cart
                CSR_VIEWED_LOCKED_PKG.GET_CSR_VIEWING (IN_SESSION_ID, IN_CSR_ID, 'ORDERS', v_order_guid, OUT_VIEWING_CUR);

        ELSE
                -- get the list of csrs viewing the order
                CSR_VIEWED_LOCKED_PKG.GET_CSR_VIEWING (IN_SESSION_ID, IN_CSR_ID, 'ORDER_DETAILS', to_char (v_order_detail_id), OUT_VIEWING_CUR);
        END IF;

        -- if the number orders is less than the number being displayed
        -- set the start position to the order specified so that the application an set the
        -- display page correctly
        IF id_tab.count <= in_max_number_returned THEN
                -- loop through the order array to find the starting position
                FOR x in 1..id_tab.count LOOP
                        IF v_order_detail_id = id_tab (x) THEN
                                v_start_position := x;
                                exit;
                        END IF;
                END LOOP;
        END IF;

        out_number_of_orders := id_tab.count;
        out_order_position := v_start_position;
        out_order_indicator := v_order_indicator;
END IF;

END GET_CART_INFO;



PROCEDURE GET_ALL_CART_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CART_CUR                    OUT TYPES.REF_CURSOR,
OUT_CART_BUYER_CUR              OUT TYPES.REF_CURSOR,
OUT_CART_BUYER_PHONES_CUR       OUT TYPES.REF_CURSOR,
OUT_PAYMENT_CC_CUR              OUT TYPES.REF_CURSOR,
OUT_ORDERS_CUR                  OUT TYPES.REF_CURSOR,
OUT_ORDER_ADDON_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_BILLS_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_REFUNDS_CUR           OUT TYPES.REF_CURSOR,
OUT_CART_TOTAL_CUR              OUT TYPES.REF_CURSOR,
OUT_ORDER_TOTAL_CUR             OUT TYPES.REF_CURSOR,
OUT_CART_FEES_SAVED_CUR         OUT TYPES.REF_CURSOR,
OUT_ORDER_FEES_SAVED_CUR        OUT TYPES.REF_CURSOR,
OUT_CART_TAXES_CUR              OUT TYPES.REF_CURSOR,
OUT_ORDER_TAXES_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order shopping cart
        and order information for all orders.
        It was initially created for use by the Free Shipping CAMS order feed MDB.

Input:
        order_guid

Output:
        cursors containing order information

-----------------------------------------------------------------------------*/

-- cursor to get the order detail ids
v_order_cur             types.ref_cursor;

-- array for of the order detail ids
id_tab                  id_list_pkg.number_tab_typ;

-- string to store the list of ids
v_str_id                varchar2(32767);

BEGIN

    GET_CART (in_order_guid, out_cart_cur);
    GET_CART_TOTALS (in_order_guid, out_cart_total_cur);
    GET_CART_PAYMENT_CC_INFO (in_order_guid, out_payment_cc_cur);
    GET_CART_FEES_SAVED (in_order_guid, out_cart_fees_saved_cur);
    GET_CART_TAXES (in_order_guid, out_cart_taxes_cur);
    GET_ORDER_TAXES (in_order_guid, out_order_taxes_cur);

    -- Get cart buyer info
    OPEN out_cart_buyer_cur FOR
        SELECT  c.first_name,
                c.last_name,
                c.address_1,
                c.address_2,
                c.city,
                c.state,
                c.zip_code,
                c.country
        FROM    customer c
        JOIN    orders o
        ON      c.customer_id = o.customer_id
        AND     o.order_guid = in_order_guid;

    -- Get cart buyer phone info
    OPEN out_cart_buyer_phones_cur FOR
       SELECT   p.phone_number, p.phone_type, p.extension, p.phone_number_type, p.sms_opt_in 
       FROM     customer_phones p, orders o
       WHERE    p.customer_id = o.customer_id
       AND      o.order_guid = in_order_guid
       ORDER BY p.updated_on DESC;

    -- get the order details of the cart
    GET_CART_ORDERS (in_order_guid, v_order_cur);
    FETCH v_order_cur BULK COLLECT INTO id_tab;
    CLOSE v_order_cur;

    -- get all the ids in a string
    id_list_pkg.get_str_id (id_tab, 1, null, v_str_id);

    -- get the orders within the cart using the created string
    GET_ORDER_INFO (v_str_id, null, null, null, out_orders_cur, out_order_addon_cur, out_order_bills_cur, out_order_refunds_cur, out_order_total_cur, out_order_fees_saved_cur);

END GET_ALL_CART_INFO;


PROCEDURE GET_CART_PAYMENT_CC_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_PAYMENT_CC_CUR              OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order payment and CC info.
        It was initially created for use by the Free Shipping CAMS order feed MDB.

Input:
        order_guid

Output:
        cursors containing order payment and CC info

-----------------------------------------------------------------------------*/

BEGIN

  OPEN OUT_PAYMENT_CC_CUR FOR
    -- Using union to avoid left outer join on credit_cards
    -- Note we only want payments (not refunds) nor do we want add-bills (per FS Phase III)
    SELECT p.payment_type,
           p.auth_date,
           p.miles_points_credit_amt,
           p.created_on,
           p.updated_on,
           p.credit_amount,
           p.debit_amount,
           cc.cc_type,
           cc.cc_expiration,
           cc.address_line_1,
           cc.address_line_2,
           cc.city,
           cc.zip_code,
           cc.state,
           cc.country,
           cc.cc_number_masked,
		       null as gc_coupon_number,
           p.wallet_indicator
      FROM clean.payments p,
           clean.credit_cards cc
     WHERE p.payment_indicator = 'P'
       AND p.additional_bill_id is null
       AND p.cc_id is not null
       AND cc.cc_id = p.cc_id
       AND p.order_guid = IN_ORDER_GUID
    UNION
    SELECT p.payment_type,
           p.auth_date,
           p.miles_points_credit_amt,
           p.created_on,
           p.updated_on,
           p.credit_amount,
           p.debit_amount,
           null as cc_type,
           null as cc_expiration,
           null as address_line_1,
           null as address_line_2,
           null as city,
           null as zip_code,
           null as state,
           null as country,
           null as cc_number_masked,
		       p.gc_coupon_number,
           p.wallet_indicator
      FROM clean.payments p
     WHERE p.payment_indicator = 'P'
       AND p.additional_bill_id is null
       AND p.cc_id is null
       AND p.order_guid = IN_ORDER_GUID;

END GET_CART_PAYMENT_CC_INFO;


FUNCTION IS_LP_IND_HOLD
(
 IN_ORDER_GUID                   IN VARCHAR2,
 IN_ORDER_DETAIL_ID              IN VARCHAR2
)
RETURN CHAR

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the loss prevention
        indicator for the given order_detail or order.  If order_detail_id
        is defined, its source_code is used, otherwise the orders source_code
        is used.

Input:
        order_guid               varchar2

Output:
        Y/N in the loss prevention indicator

-----------------------------------------------------------------------------*/

 v_threshold    frp.global_parms.value%type;
 v_timeframe    NUMBER;
 v_lp_indicator orders.loss_prevention_indicator%TYPE;
 v_order_date   DATE;
 v_lp_count     NUMBER;
 v_flag         CHAR(1);
 v_preferred_partner ftd_apps.partner_master.partner_name%type;

BEGIN

BEGIN
   select count(partner_id) INTO v_lp_count from PTN_PI.PARTNER_MAPPING where FTD_ORIGIN_MAPPING='ARI' AND DEFAULT_SOURCE_CODE = (SELECT SOURCE_CODE FROM CLEAN.ORDERS WHERE ORDER_GUID = IN_ORDER_GUID);
      --Bypassing LP Queue logic for Ariba Orders.
   if(v_lp_count>0) then
      v_flag := 'N';
      return v_flag;
   end if; 
   
    EXCEPTION WHEN NO_DATA_FOUND THEN 
       v_lp_count := NULL;
       v_flag := NULL;
  END;
   

  BEGIN
    IF (IN_ORDER_DETAIL_ID IS NOT NULL) THEN
        SELECT distinct pm.partner_name
          INTO  v_preferred_partner
          FROM  ftd_apps.source_master sm,
                 ftd_apps.source_program_ref spr,
                 ftd_apps.partner_program pp,
                 ftd_apps.partner_master pm,
                 clean.order_details od
         WHERE  od.order_detail_id = IN_ORDER_DETAIL_ID
                 AND sm.source_code = od.source_code
                 AND spr.source_code = sm.source_code
                 AND pp.program_name = spr.program_name
                 AND pm.partner_name = pp.partner_name
                 AND pm.preferred_partner_flag = 'Y';
    ELSE
        SELECT distinct pm.partner_name
          INTO  v_preferred_partner
          FROM  ftd_apps.source_master sm,
                 ftd_apps.source_program_ref spr,
                 ftd_apps.partner_program pp,
                 ftd_apps.partner_master pm,
                 clean.orders o
         WHERE  o.order_guid = IN_ORDER_GUID
                 AND spr.source_code = o.source_code
                 AND pp.program_name = spr.program_name
                 AND pm.partner_name = pp.partner_name
                 AND pm.preferred_partner_flag = 'Y';
    END IF;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_preferred_partner := NULL;
  END;

  BEGIN
    IF (v_preferred_partner IS NULL) THEN
        SELECT value
          INTO v_threshold
          FROM frp.global_parms
         WHERE context = 'ORDER_PROCESSING'
           AND name = 'LP_INDICATOR_THRESHOLD';
    ELSE
        SELECT value
          INTO v_threshold
          FROM frp.global_parms
         WHERE context = 'ORDER_PROCESSING'
           AND name = (v_preferred_partner || '_LP_INDICATOR_THRESHOLD');
    END IF;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_threshold := NULL;
  END;

  BEGIN
    IF (v_preferred_partner IS NULL) THEN
        SELECT TO_NUMBER(value)
          INTO v_timeframe
          FROM frp.global_parms
         WHERE context = 'ORDER_PROCESSING'
           AND name = 'LP_INDICATOR_TIMEFRAME';
    ELSE
        SELECT TO_NUMBER(value)
          INTO v_timeframe
          FROM frp.global_parms
         WHERE context = 'ORDER_PROCESSING'
           AND name = (v_preferred_partner || '_LP_INDICATOR_TIMEFRAME');
    END IF;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_timeframe := NULL;
  END;

  BEGIN
    IF (in_order_guid IS NULL) THEN
        SELECT o.order_date,
               o.loss_prevention_indicator
          INTO v_order_date,
               v_lp_indicator
          FROM orders o, order_details od
         WHERE o.order_guid = od.order_guid
           AND od.order_detail_id = in_order_detail_id;
    ELSE
        SELECT order_date,
               loss_prevention_indicator
          INTO v_order_date,
               v_lp_indicator
          FROM orders
         WHERE order_guid = in_order_guid;
    END IF;

    EXCEPTION WHEN NO_DATA_FOUND THEN
      BEGIN
        v_order_date := NULL;
        v_lp_indicator := NULL;
      END;
  END;

  BEGIN
    SELECT count(1)
      INTO v_lp_count
      FROM orders
     WHERE loss_prevention_indicator = v_lp_indicator
       AND order_date < v_order_date
       AND order_date >= (v_order_date - (v_timeframe/24));

    EXCEPTION WHEN NO_DATA_FOUND THEN v_lp_count := NULL;
  END;

  IF v_lp_count IS NULL OR v_lp_count < v_threshold THEN
     v_flag := 'N';
  ELSE
     v_flag := 'Y';
  END IF;

  RETURN v_flag;

END IS_LP_IND_HOLD;


PROCEDURE GET_ORDER_BY_GUID
(
 IN_ORDER_GUID   IN VARCHAR2,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will retrieve each field in table orders
except
        for order_guid, updated_on, updated_by, created_on, and created_by.

Input:
        order_guid

Output:
        cursor containing orders info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CURSOR FOR
   SELECT master_order_number,
          customer_id,
          membership_id,
          company_id,
          source_code,
          origin_id,
          order_date,
          to_char(order_date, 'YYYY-MM-DD HH24:MI:SS' ) char_order_date ,
          null order_total,          -- cart totals were removed from the table 8/3/05
          null product_total,
          null add_on_total,
          null service_fee_total,
          null shipping_fee_total,
          null discount_total,
          null tax_total,
          loss_prevention_indicator,
          fraud_indicator,
          buyer_signed_in_flag ,
          language_id,
		  email_id
     FROM orders
     WHERE order_guid = in_order_guid;

END GET_ORDER_BY_GUID;

PROCEDURE GET_EMAIL_ADDRESS_BY_EMAIL_ID
(
  IN_EMAIL_ID       IN CLEAN.ORDERS.EMAIL_ID%TYPE,
  OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN OUT_CURSOR FOR
    SELECT EMAIL_ADDRESS FROM CLEAN.EMAIL WHERE EMAIL_ID = IN_EMAIL_ID;

END GET_EMAIL_ADDRESS_BY_EMAIL_ID;

PROCEDURE GET_ORDER_DETAILS
(
 IN_ORDER_DETAIL_ID         IN  ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will retrieve each field in table order_details
        except for external_order_number, updated_on, updated_by, created_on,
        and created_by.

Input:
        order_detail_id

Output:
        cursor containing order detail info

Note: This procedure overloads the procedure above, taking in
order_detail_id (number) without the string id list.  This
procedure calls the original get_order_detail.

-----------------------------------------------------------------------------*/
BEGIN

/*  original select but the proc was changed to reuse the main order details proc
    which gets details of recipients, product, colors, and florists
  OPEN OUT_CURSOR FOR
   SELECT order_detail_id, order_guid, hp_order_number,
        source_code, delivery_date, recipient_id, product_id, quantity,
        color_1, color_2, substitution_indicator, same_day_gift,
        occasion, card_message, card_signature, special_instructions,
        release_info_indicator, florist_id, ship_method, ship_date,
        order_disp_code
     FROM order_details
     WHERE order_detail_id = in_order_detail_id;
*/

GET_ORDER_DETAILS (to_char (in_order_detail_id), in_order_detail_id, out_cursor);

END GET_ORDER_DETAILS;


PROCEDURE GET_ORDER_DETAILS
(
 IN_EXTERNAL_ORDER_NUMBER   IN VARCHAR2,
 OUT_CURSOR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will retrieve each field in table order_details
        except for external_order_number, updated_on, updated_by, created_on,
        and created_by.

Input:
        external_order_number

Output:
        cursor containing order detail info

Note: This procedure overloads the procedure above, taking in
external_order_number (varchar) instead of order_detail_id (number).  This
procedure calls FIND_ORDER_NUMBER to get the order_detail_id.


-----------------------------------------------------------------------------*/

-- order variables when finding the actual order detail id
v_order_detail_id       number;
v_order_guid            varchar2 (2000);
v_order_disp_code       varchar2 (20);
v_order_indicator       varchar2 (35);

BEGIN

  -- find the order detail id from the given order number
  find_order_number (in_external_order_number, v_order_detail_id, v_order_guid, v_order_disp_code, v_order_indicator);

  if v_order_detail_id is null then
        v_order_detail_id := -1;
  end if;

  get_order_details (v_order_detail_id, out_cursor);


END GET_ORDER_DETAILS;


PROCEDURE GET_CART_PAYMENTS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving payment information
        for the given cart.

Input:
        order guid                      varchar

Output:
        cursor containing cart payment information

-----------------------------------------------------------------------------*/
BEGIN

-- This cursor returns the payments from the given cart using a union of three selects.
-- 1) return the original payment of the cart
--    This can contain a most 2 payments records (1 gift certificate and 1 other payment type)
-- 2) return the additional bill payments
--    Additional bills are indicated with an additional bill id on the payment record and the
--    payment indicator is either P or B where the credit amount is greater than or equal
--    to the debit amount.
-- 3) return the refund payments
--    Refunds are indicated with a refund id on the payment record and the payment indicator is
--    either R or B where the debit amount is greater than the credit amount
--
-- The overall data is returned in the order of occurance (created on date of each payment).
-- If a credit card is used, the last four digits of the credit card is returned, if a
-- gift certificate is used the gift certificate/coupon number is returned.  The latest end of day
-- processed date that is assocaiated to the payment is returned.  If the payment is comprised of
-- both credit and debit amounts (this can happen from modify order), then an indicator is returned
-- to state so.

OPEN OUT_CUR FOR
        SELECT
                p.payment_id,
                'Original' payment_indicator,
                p.payment_type,
                decode (p.cc_id, null,
                                p.gc_coupon_number,
                                (select cc.cc_number_masked
                                 from   credit_cards cc
                                 where  cc.cc_id = p.cc_id)
                        ) card_number,
                decode (p.cc_id, null,
                                null,
                                (select cc.cc_expiration from credit_cards cc where cc.cc_id = p.cc_id)
                        ) expiration_date,
                p.auth_number,
                p.credit_amount,
                1 group_order,
                to_char(p.bill_date,'mm/dd/yyyy') bill_date,
                p.created_on,
                'N' display_credit_debit_ind,
                p.payment_transaction_id,
                null child_payment_amount,
                null child_refund_amount,
                p.avs_code,
  				p.csc_validated_flag,
  				p.wallet_indicator,
  				p.cardinal_verified_flag,
  				p.route
        FROM    payments p
        WHERE   p.order_guid = in_order_guid
        AND     p.additional_bill_id IS NULL
        AND     p.payment_indicator = 'P'
        UNION
        SELECT  b.payment_id,
                b.payment_indicator || to_char (rownum) payment_indicator,
                b.payment_type,
                b.card_number,
                b.expiration_date,
                b.auth_number,
                b.credit_amount,
                b.group_order,
                b.bill_date,
                b.created_on,
                b.display_credit_debit_ind,
                b.payment_transaction_id,
                b.child_payment_amount,
                b.child_refund_amount,
                b.avs_code,
    			b.csc_validated_flag,
    			b.wallet_indicator,
    			null,
    			b.route
        FROM
        (
                SELECT
                        p.payment_id,
                        'Add Bill ' payment_indicator,
                        p.payment_type,
                        decode (p.cc_id, null,
                                        p.gc_coupon_number,
                                        (select cc.cc_number_masked
                                         from   credit_cards cc
                                         where  cc.cc_id = p.cc_id)
                                ) card_number,
                        decode (p.cc_id, null,
                                        null,
                                        (select cc.cc_expiration from credit_cards cc where cc.cc_id = p.cc_id)
                                ) expiration_date,
                        p.auth_number,
                        p.credit_amount - nvl (p.debit_amount, 0) credit_amount,
                        2 group_order,
                        to_char(p.bill_date,'mm/dd/yyyy') bill_date,
                        p.created_on,
                        decode (payment_indicator, 'B', 'Y', 'N') display_credit_debit_ind,
                        p.payment_transaction_id,
                        p.credit_amount child_payment_amount,
                        p.debit_amount child_refund_amount,
                        p.avs_code,
      					p.csc_validated_flag,
      					p.wallet_indicator,
      					null,
      					p.route
                FROM    payments p
                WHERE   p.order_guid = in_order_guid
                AND     p.additional_bill_id IS NOT NULL
                AND     (p.payment_indicator = 'P' OR (p.payment_indicator = 'B' AND p.credit_amount >= p.debit_amount))
                ORDER BY p.created_on
        ) b
        UNION
        SELECT  r.payment_id,
                r.payment_indicator || to_char (rownum) payment_indicator,
                r.payment_type,
                r.card_number,
                r.expiration_date,
                r.auth_number,
                r.debit_amount,
                r.group_order,
                r.refund_date,
                r.created_on,
                r.display_credit_debit_ind,
                r.payment_transaction_id,
                r.child_payment_amount,
                r.child_refund_amount,
                r.avs_code,
    			r.csc_validated_flag,
    			r.wallet_indicator,
    			null,
    			r.route
        FROM
        (
                SELECT
                        p.payment_id,
                        'Refund ' payment_indicator,
                        p.payment_type,
                        decode (p.cc_id, null,
                                        p.gc_coupon_number,
                                        (select cc.cc_number_masked
                                         from   credit_cards cc
                                         where  cc.cc_id = p.cc_id)
                                ) card_number,
                        decode (p.cc_id, null,
                                        null,
                                        (select cc.cc_expiration from credit_cards cc where cc.cc_id = p.cc_id)
                                ) expiration_date,                        p.auth_number,
                        p.debit_amount - nvl (p.credit_amount, 0) debit_amount,
                        3 group_order,
                        to_char ((select r.refund_date from refund r where r.refund_id = p.refund_id), 'mm/dd/yyyy') refund_date,
                        p.created_on,
                        decode (payment_indicator, 'B', 'Y', 'N') display_credit_debit_ind,
                        p.payment_transaction_id,
                        p.credit_amount child_payment_amount,
                        p.debit_amount child_refund_amount,
                        p.avs_code,
      					p.csc_validated_flag,
      					p.wallet_indicator,
      					null,
						p.route
                FROM    payments p
                WHERE   p.order_guid = in_order_guid
                AND     (p.payment_indicator = 'R' OR (p.payment_indicator = 'B' AND p.credit_amount < p.debit_amount))
                ORDER BY p.created_on
        ) r
        ORDER BY created_on, payment_indicator, payment_id;

END GET_CART_PAYMENTS;


PROCEDURE GET_ORDER_PAYMENTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving payment information
        for the given order.  The original payment amount will be
        of the shopping cart, but any additional bills and refunds will be order
        specific.

Input:
        order detail id

Output:
        cursor containing order payment information

-----------------------------------------------------------------------------*/
BEGIN

-- This cursor returns the payments from the given order using a union of three selects.
-- 1) return the original payment of the cart
--    This can contain a most 2 payments records (1 gift certificate and 1 other payment type)
-- 2) return the additional bill payments
--    Additional bills are indicated with an additional bill id on the payment record and the
--    payment indicator is either P or B where the credit amount is greater than or equal
--    to the debit amount.
-- 3) return the refund payments
--    Refunds are indicated with a refund id on the payment record and the payment indicator is
--    either R or B where the debit amount is greater than the credit amount
--
-- The overall data is returned in the order of occurance (created on date of each payment).
-- If a credit card is used, the last four digits of the credit card is returned, if a
-- gift certificate is used the gift certificate/coupon number is returned.  The latest end of day
-- processed date that is assocaiated to the payment is returned.  If the payment is comprised of
-- both credit and debit amounts (this can happen from modify order), then an indicator is returned
-- to state so.
OPEN OUT_CUR FOR
        SELECT
                p.payment_id,
                'Original' payment_indicator,
                p.payment_type,
                decode (p.cc_id, null,
                                p.gc_coupon_number,
                                (select cc.cc_number_masked
                                 from   credit_cards cc
                                 where  cc.cc_id = p.cc_id)
                        ) card_number,
                decode (p.cc_id, null,
                                null,
                                (select cc.cc_expiration from credit_cards cc where cc.cc_id = p.cc_id)
                        ) expiration_date,
                p.auth_number,
                p.credit_amount,
                1 group_order,
                to_char (p.bill_date, 'mm/dd/yyyy') bill_date,
                pm.description payment_type_description,
                p.auth_override_flag user_auth_indicator,
                nvl ((select distinct 'Y' from order_bills ob join order_details od on ob.order_detail_id = od.order_detail_id where ob.bill_status IN ('Billed', 'Settled', 'Reconciled') and ob.additional_bill_indicator = 'N' and od.order_guid = p.order_guid ), 'N') eod_indicator,
                p.created_on,
                'N' display_credit_debit_ind,
                p.payment_transaction_id,
                null child_payment_amount,
                null child_refund_amount,
                p.aafes_ticket_number,
                p.avs_code,
    			p.csc_validated_flag,
    			p.wallet_indicator,
    			p.cardinal_verified_flag,
    			p.route
        FROM    payments p
        JOIN    ftd_apps.payment_methods pm
        ON      p.payment_type = pm.payment_method_id
        WHERE   EXISTS (select 1 from order_details od where p.order_guid = od.order_guid and od.order_detail_id = in_order_detail_id)
        AND     p.additional_bill_id IS NULL
        AND     p.payment_indicator = 'P'
        UNION
        SELECT  b.payment_id,
                b.payment_indicator || to_char (rownum) payment_indicator,
                b.payment_type,
                b.card_number,
                b.expiration_date,
                b.auth_number,
                b.credit_amount,
                b.group_order,
                b.bill_date,
                b.payment_type_description,
                b.user_auth_indicator,
                b.eod_indicator,
                b.created_on,
                b.display_credit_debit_ind,
                b.payment_transaction_id,
                b.child_payment_amount,
                b.child_refund_amount,
                b.aafes_ticket_number,
                b.avs_code,
    			b.csc_validated_flag,
    			b.wallet_indicator,
    			null,
    			b.route
        FROM
        (
                SELECT
                        p.payment_id,
                        'Add Bill ' payment_indicator,
                        p.payment_type,
                        decode (p.cc_id, null,
                                        p.gc_coupon_number,
                                        (select cc.cc_number_masked
                                         from   credit_cards cc
                                         where  cc.cc_id = p.cc_id)
                                ) card_number,
                        decode (p.cc_id, null,
                                        null,
                                        (select cc.cc_expiration from credit_cards cc where cc.cc_id = p.cc_id)
                                ) expiration_date,
                        p.auth_number,
                        p.credit_amount - nvl (p.debit_amount, 0) credit_amount,
                        2 group_order,
                        to_char (p.bill_date, 'mm/dd/yyyy') bill_date,
                        pm.description payment_type_description,
                        p.auth_override_flag user_auth_indicator,
                        nvl ((select 'Y' from order_bills ob where ob.bill_status IN ('Billed', 'Settled', 'Reconciled') and ob.order_bill_id = p.additional_bill_id), 'N') eod_indicator,
                        p.created_on,
                        decode (payment_indicator, 'B', 'Y', 'N') display_credit_debit_ind,
                        p.payment_transaction_id,
                        p.credit_amount child_payment_amount,
                        p.debit_amount child_refund_amount,
                        p.aafes_ticket_number,
                        p.avs_code,
     					p.csc_validated_flag,
     					p.wallet_indicator,
     					null,
     					p.route
                FROM    payments p
                JOIN    ftd_apps.payment_methods pm
                ON      p.payment_type = pm.payment_method_id
                WHERE   EXISTS (select 1 from order_bills ob where p.additional_bill_id = ob.order_bill_id and ob.order_detail_id = in_order_detail_id)
                AND     (p.payment_indicator = 'P' OR (p.payment_indicator = 'B' AND p.credit_amount >= p.debit_amount))
                ORDER BY p.created_on
        ) b
        UNION
        SELECT  r.payment_id,
                r.payment_indicator || to_char (rownum) payment_indicator,
                r.payment_type,
                r.card_number,
                r.expiration_date,
                r.auth_number,
                r.debit_amount,
                r.group_order,
                r.refund_date,
                r.payment_type_description,
                r.user_auth_indicator,
                r.eod_indicator,
                r.created_on,
                r.display_credit_debit_ind,
                r.payment_transaction_id,
                r.child_payment_amount,
                r.child_refund_amount,
                r.aafes_ticket_number,
                r.avs_code,
    			r.csc_validated_flag,
    			r.wallet_indicator,
    			null,
    			r.route
        FROM
        (
                SELECT
                        p.payment_id,
                        'Refund ' payment_indicator,
                        p.payment_type,
                        decode (p.cc_id, null,
                                        p.gc_coupon_number,
                                        (select cc.cc_number_masked
                                         from   credit_cards cc
                                         where  cc.cc_id = p.cc_id)
                                ) card_number,
                        decode (p.cc_id, null,
                                        null,
                                        (select cc.cc_expiration from credit_cards cc where cc.cc_id = p.cc_id)
                                ) expiration_date,                        p.auth_number,
                        (p.debit_amount - nvl (p.credit_amount, 0)) debit_amount,
                        3 group_order,
                        to_char ((select r.refund_date from refund r where r.refund_id = p.refund_id), 'mm/dd/yyyy') refund_date,
                        pm.description payment_type_description,
                        null user_auth_indicator,
                        nvl ((select 'Y' from refund r where r.refund_status IN ('Billed', 'Settled', 'Reconciled') and r.refund_id = p.refund_id), 'N') eod_indicator,
                        p.created_on,
                        decode (payment_indicator, 'B', 'Y', 'N') display_credit_debit_ind,
                        p.payment_transaction_id,
                        p.credit_amount child_payment_amount,
                        p.debit_amount child_refund_amount,
                        p.aafes_ticket_number,
                        p.avs_code,
      					p.csc_validated_flag,
      					p.wallet_indicator,
      					null,
      					p.route
                FROM    payments p
                JOIN    ftd_apps.payment_methods pm
                ON      p.payment_type = pm.payment_method_id
                WHERE   EXISTS (select 1 from refund r where p.refund_id = r.refund_id and r.order_detail_id = in_order_detail_id)
                AND     (p.payment_indicator = 'R' OR (p.payment_indicator = 'B' AND p.credit_amount < p.debit_amount))
                ORDER BY p.created_on
        ) r
        ORDER BY created_on, payment_indicator, payment_id;

END GET_ORDER_PAYMENTS;


PROCEDURE GET_ORDER_INFO_FOR_PRINT
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_INCLUDE_COMMENTS             IN VARCHAR2,
IN_PROCESS_ID                   IN VARCHAR2,
OUT_ORDERS_CUR                  OUT TYPES.REF_CURSOR,
OUT_ORDER_ADDON_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_BILLS_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_REFUNDS_CUR           OUT TYPES.REF_CURSOR,
OUT_ORDER_TOTAL_CUR             OUT TYPES.REF_CURSOR,
OUT_ORDER_PAYMENT_CUR           OUT TYPES.REF_CURSOR,
OUT_ORDER_COMMENT_CUR           OUT TYPES.REF_CURSOR,
OUT_CUST_CART_CUR               OUT TYPES.REF_CURSOR,
OUT_CUSTOMER_PHONE_CUR          OUT TYPES.REF_CURSOR,
OUT_MEMBERSHIP_CUR              OUT TYPES.REF_CURSOR,
OUT_TAX_REFUND_CUR              OUT TYPES.REF_CURSOR,
OUT_CART_PAYMENT_CUR            OUT TYPES.REF_CURSOR,
OUT_ORDER_TAXES_CUR 			OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order information
        for the given order to print out reciept

Input:
        order_detail_id         number
        include_comments        varchar2 (Y/N to print out comments)

Output:
        cursors containing order information

-----------------------------------------------------------------------------*/

-- cursor to get the last refund amount on the order to be returned with the
-- canadian tax information
CURSOR last_refund_cur IS
        SELECT  r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax last_refund_total
        FROM    refund r
        WHERE   refund_id =
                (
                        select  max (r1.refund_id)
                        from    refund r1
                        where   r1.order_detail_id = in_order_detail_id
                );

v_comment_cnt                   number;
v_last_refund_total             number;

BEGIN

-- get customer and cart information
OPEN out_cust_cart_cur FOR
        SELECT  c.first_name,
                c.last_name,
                c.address_1,
                c.address_2,
                c.city,
                c.state,
                c.zip_code,
                o.master_order_number,
                trim(to_char(sysdate, 'Day')) || ', ' ||
                    trim(to_char(sysdate, 'Month')) || ' ' ||
                    to_char(sysdate, 'dd') cur_date
        FROM    customer c
        JOIN    orders o
        ON      c.customer_id = o.customer_id
        WHERE   EXISTS
        (
                SELECT  1
                FROM    order_details od
                WHERE   od.order_detail_id = in_order_detail_id
                AND     od.order_guid = o.order_guid
        );

-- get customer phone information
OPEN out_customer_phone_cur FOR
        SELECT  cp.phone_type,
                cp.phone_number,
                cp.extension
        FROM    customer_phones cp
        WHERE   EXISTS
        (
                SELECT  1
                FROM    order_details od
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                WHERE   od.order_detail_id = in_order_detail_id
                AND     o.customer_id = cp.customer_id
        );

-- get customer membership information for the order
OPEN out_membership_cur FOR
        SELECT  m.membership_number
        FROM    memberships m
        WHERE   EXISTS
        (
                SELECT  1
                FROM    order_details od
                JOIN    orders o
                ON      od.order_guid = o.order_guid
                WHERE   od.order_detail_id = in_order_detail_id
                AND     o.membership_id = m.membership_id
        );

-- get the cart payment information for the order
OPEN out_cart_payment_cur FOR
        SELECT  pm.description payment_type_description,
                pm.payment_type payment_method,
                p.payment_type,
                decode (p.cc_id, null,
                                p.gc_coupon_number,
                                (select cc.cc_number_masked
                                 from   credit_cards cc
                                 where  cc.cc_id = p.cc_id)
                        ) card_number,
                sum (nvl (p.credit_amount, 0)) - sum (nvl (p.debit_amount, 0)) payment_amount
        FROM    payments p
        JOIN    order_details od
        ON      p.order_guid = od.order_guid
        JOIN    ftd_apps.payment_methods pm
        ON      p.payment_type = pm.payment_method_id
        WHERE   od.order_detail_id = in_order_detail_id
        GROUP BY pm.description, pm.payment_type, p.payment_type, p.cc_id, p.gc_coupon_number;

-- get the last refund on the order
OPEN last_refund_cur;
FETCH last_refund_cur INTO v_last_refund_total;
CLOSE last_refund_cur;

-- return the last refund canadian tax information
OPEN out_tax_refund_cur FOR
        SELECT  v_last_refund_total last_refund_total,
                frp.misc_pkg.get_global_parm_value ('FTDAPPS_PARMS', 'CANADIAN_EXCHANGE_RATE') canadian_exchange_rate
        FROM    dual;

-- get the order specific information
get_order_info (to_char (in_order_detail_id), in_order_detail_id, null, in_process_id, out_orders_cur, out_order_addon_cur, out_order_bills_cur, out_order_refunds_cur, out_order_total_cur);
get_order_payments (in_order_detail_id, out_order_payment_cur);

-- return comments if specified
IF in_include_comments = 'Y' THEN
        comment_history_pkg.get_comments ('Order', null, null, in_order_detail_id, null, 1, null, 'N', 'N', out_order_comment_cur, v_comment_cnt);
ELSE
        -- return an empty comment cursor
        comment_history_pkg.get_comments ('Order', null, null, -1, null, 1, null, 'N', 'N', out_order_comment_cur, v_comment_cnt);
END IF;

get_order_detail_taxes (in_order_detail_id, out_order_taxes_cur);

END GET_ORDER_INFO_FOR_PRINT;


FUNCTION GET_LP_INDICATOR_COUNT
(
 IN_LP_INDICATOR IN VARCHAR2,
 IN_HOURS        IN NUMBER
)
 RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a count of the number of orders for a
        particular LP indicator and the time between the order date and current
        date minus the hours passed in.

Input:
        lp_indicator -  VARCHAR
        hours        -  NUMBER

Output:
        message count

-----------------------------------------------------------------------------*/

out_count NUMBER;

BEGIN

  SELECT COUNT(*) INTO out_count
    FROM orders
   WHERE loss_prevention_indicator = in_lp_indicator
     AND order_date > (SYSDATE - in_hours/24);

  RETURN out_count;

END GET_LP_INDICATOR_COUNT;


PROCEDURE GET_ORDER_DETAIL_PAYMENT
(
 IN_ORDER_GUID   IN VARCHAR2,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will retrieve each field in tables order_details,
        order_bills and payments except for external_order_number, updated_on,
        updated_by, created_on, and created_by.

Input:
        order_guid

Output:
        cursor containing order details, order bills and payments info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CURSOR FOR
   SELECT od.order_detail_id,
          od.order_guid,
          od.hp_order_number,
          od.source_code,
          od.delivery_date,
          od.delivery_date_range_end,
          od.recipient_id,
          od.product_id,
          od.quantity,
          od.color_1,
          od.color_2,
          od.substitution_indicator,
          od.same_day_gift,
          od.occasion,
          od.card_message,
          od.card_signature,
          od.special_instructions,
          od.release_info_indicator,
          od.florist_id,
          od.ship_method,
          od.ship_date,
          od.order_disp_code,
          od.zip_queue_count,
          ob.order_bill_id,
          ob.order_detail_id,
          ob.product_amount,
          ob.add_on_amount,
          ob.service_fee,
          ob.shipping_fee,
          ob.discount_amount,
          ob.shipping_tax,
          ob.tax,
          ob.additional_bill_indicator,
          p.payment_id,
          p.order_guid,
          p.additional_bill_id,
          p.payment_type,
          p.cc_id,
          p.auth_result,
          p.auth_number,
          p.avs_code,
          p.acq_reference_number,
          p.gc_coupon_number,
          p.auth_override_flag,
          p.credit_amount,
          p.debit_amount,
          p.payment_indicator,
          p.refund_id,
          od.op_status,
          od.vendor_id,
    od.actual_product_amount,
    od.actual_add_on_amount,
          od.actual_shipping_fee,          
          ob.add_on_discount_amount,
          od.actual_add_on_discount_amount
     FROM order_details od,
          order_bills ob,
          payments p
     WHERE od.order_guid = in_order_guid
       AND ob.order_detail_id = od.order_detail_id
       AND p.order_guid = od.order_guid;

END GET_ORDER_DETAIL_PAYMENT;


FUNCTION CAN_FLORIST_SUPPORT_ORDER
(
 IN_ORDER_DETAIL_ID IN NUMBER,
 IN_FLORIST_ID      IN VARCHAR2
)
 RETURN CHAR
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a Y/N flag meaning if the order can be
        delivered by the florist.

Input:
        order detail id, florist id

Output:
        flag - character

-----------------------------------------------------------------------------*/

v_flag CHAR(1);

v_product_id VARCHAR2(10) := NULL;
v_delivery_date DATE;
v_delivery_date_range_end DATE;
v_codification_id VARCHAR2(5) := NULL;

BEGIN

  BEGIN
    SELECT product_id, delivery_date, delivery_date_range_end
      INTO v_product_id, v_delivery_date, v_delivery_date_range_end
      FROM order_details
     WHERE order_detail_id = in_order_detail_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN NULL;
  END;

  IF v_product_id IS NULL THEN
    v_flag := 'N';
  ELSE

    BEGIN
      SELECT codification_id INTO v_codification_id
        FROM ftd_apps.codified_products
       WHERE product_id = v_product_id;
    EXCEPTION
       WHEN NO_DATA_FOUND THEN NULL;
    END;

    IF (v_codification_id IS NULL) THEN
      --product order is not codified
      v_flag := 'Y';
    ELSE
      --product order is codified therefor check to see if the florist
      -- is codified for this product
      v_flag := 'N';

      BEGIN
        SELECT 'Y' INTO v_flag
          FROM ftd_apps.florist_codifications
         WHERE florist_id = in_florist_id
           AND codification_id = v_codification_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        --florist not found and/or florist is not codified for this codified product
      END;

      IF v_flag = 'Y' THEN
        BEGIN
          --if the delivery date falls with the florist block date then the florist
          -- cannot fullfil this order
          SELECT 'N' INTO v_flag
            FROM ftd_apps.florist_codifications
           WHERE florist_id = in_florist_id
             AND codification_id = v_codification_id
             AND (   ((block_start_date IS NULL) AND (TRUNC(block_end_date) >= TRUNC(v_delivery_date)))
                  OR ((TRUNC(block_start_date) <= TRUNC(v_delivery_date)) AND (block_end_date IS NULL))
                  OR (TRUNC(v_delivery_date) BETWEEN TRUNC(block_start_date) AND TRUNC(block_end_date))
                 );
        EXCEPTION
          WHEN NO_DATA_FOUND THEN NULL;
          --delivery date is not a florist block out date
        END;
      END IF;
    END IF;

    IF (v_flag = 'Y') THEN
      v_flag := ftd_apps.florist_query_pkg.is_florist_open_in_eros(in_florist_id);
    END IF;

    IF (v_flag = 'Y') THEN
      IF v_delivery_date_range_end is null THEN
        v_flag := ftd_apps.florist_query_pkg.is_florist_open_for_delivery(in_florist_id, v_delivery_date);
      ELSE
        v_flag := ftd_apps.florist_query_pkg.is_florist_open_date_range(in_florist_id, v_delivery_date, v_delivery_date_range_end);
      END IF;
    END IF;

    IF (v_flag = 'Y') THEN
      v_flag := 'N';
      BEGIN
        SELECT 'Y' INTO v_flag
          FROM ftd_apps.florist_master
         WHERE florist_id = in_florist_id;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL;
        -- the florist does not exist
      END;
    END IF;

  END IF;

  RETURN v_flag;

END CAN_FLORIST_SUPPORT_ORDER;


FUNCTION GET_ORDER_STATUS_BY_CUST
(
IN_CUSTOMER_ID                  IN ORDERS.CUSTOMER_ID%TYPE,
IN_FIELD_IND                    IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving the order status and
        order number for the customer if the customer only has one shopping
        cart and order.  If more than one order or shopping cart is found,
        null is returned.

Input:
        customer_id                     number
        field indicator                 varchar2 - D for order disposition indicator
                                                   N for order number

Output:
        order status or external order number

-----------------------------------------------------------------------------*/

CURSOR order_cur IS
        SELECT  max(od.order_disp_code), max(od.external_order_number), count (1) order_count
        FROM    order_details od
        JOIN    orders o
        ON      o.order_guid = od.order_guid
        WHERE   o.customer_id = in_customer_id;

v_order_disp_code       order_details.order_disp_code%type;
v_external_order_number order_details.external_order_number%type;
v_count                 number;
v_return                varchar2(100);

BEGIN

OPEN order_cur;
FETCH order_cur INTO v_order_disp_code, v_external_order_number, v_count;
CLOSE order_cur;

IF v_count = 1 THEN
        IF in_field_ind = 'D' THEN
                v_return := order_query_pkg.get_scrub_status_ind (v_order_disp_code);
        ELSE
                v_return := v_external_order_number;
        END IF;
END IF;

RETURN v_return;

END GET_ORDER_STATUS_BY_CUST;


FUNCTION IS_ORDER_CANCELLED
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning if the given order
        is cancelled based on the existance and order of mercury or venus
        FTD and CAN messages.


Input:
        order_detail_id                 number

Output:
        Y/N if the order is cancelled (i.e. no live FTD message)

-----------------------------------------------------------------------------*/
v_status_cur    types.ref_cursor;

v_attempted_ftd         char (1);
v_has_live_ftd          char (1);
v_cancel_sent           char (1);
v_reject_sent           char (1);
v_ftd_status            varchar2 (10);
v_delivery_date         date;
v_a_type_refund         char (1);
v_comp_order        char (1);
v_attempted_can         char(1);

CURSOR aref_cur IS
        SELECT  'Y'
        FROM    refund
        WHERE   order_detail_id = in_order_detail_id
        AND     refund_disp_code = 'A50';
BEGIN

ORDER_MESG_PKG.GET_ORDER_MESSAGE_STATUS
(
        IN_ORDER_DETAIL_ID=>in_order_detail_id,
        IN_MESSAGE_TYPE=>NULL,
        IN_COMP_ORDER=>'N',
        OUT_CUR=>V_STATUS_CUR
);

FETCH v_status_cur INTO
        v_attempted_ftd,
        v_has_live_ftd,
        v_cancel_sent,
        v_reject_sent,
        v_ftd_status,
        v_delivery_date,
        v_comp_order,
        v_attempted_can;

CLOSE v_status_cur;

-- If there no message has been sent for the order, check if there is an
-- A50 type refund posted against the order.  If so then the order was held and cancelled.
-- Otherwise, there must be a cancel message sent for the order for it to be
-- considered cancelled.
IF v_attempted_ftd = 'N' THEN
        OPEN aref_cur;
        FETCH aref_cur INTO v_a_type_refund;
        CLOSE aref_cur;

        IF v_a_type_refund = 'Y' THEN
                v_cancel_sent := 'Y';
        ELSE
                v_cancel_sent := 'N';
        END IF;
END IF;

RETURN v_cancel_sent;

END IS_ORDER_CANCELLED;


PROCEDURE GET_DAILY_FLORIST
(
 IN_ORDER_DETAIL_ID  IN NUMBER,
 IN_CUSTOMER_ID      IN NUMBER,
 IN_DELIVERY_DATE    IN DATE,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a list of distinct florist_id from table
        order_details for the customer_id and delivery date passed in.

Input:
       customer id
       delivery date

Output:
        cursor containing distinct florist_ids

-----------------------------------------------------------------------------*/

   v_priority number := 0;

BEGIN

  select count(*) into v_priority
  from   ftd_apps.source_florist_priority sfp, clean.order_details cod
  where  cod.order_detail_id = in_order_detail_id
  and    sfp.source_code = cod.source_code;

  OPEN out_cursor FOR
    SELECT DISTINCT od.florist_id
      FROM order_details od,
           customer c
     WHERE c.customer_id = od.recipient_id
       AND EXISTS (SELECT 1
                     FROM customer ca
                    WHERE ca.customer_id = in_customer_id
                      AND ca.address_1 = c.address_1
                      AND ca.zip_code = c.zip_code)
       AND v_priority = 0
       AND TRUNC(od.delivery_date) = TRUNC(in_delivery_date)
       AND (od.ship_method is null or od.ship_method = 'SD')
   MINUS
    SELECT florist_id
      FROM clean.order_florist_used
     WHERE order_detail_id = in_order_detail_id;

END GET_DAILY_FLORIST;


PROCEDURE GET_ORDER_FLORIST_USED
(
 IN_ORDER_DETAIL_ID IN NUMBER,
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table order_florist_used
        for a particular order detail id.

Input:
        order detail id

Output:
        ref cusor that contains a order_florist_used record

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT florist_id,
           sequence
      FROM order_florist_used
     WHERE order_detail_id = in_order_detail_id
     ORDER BY sequence desc;

END GET_ORDER_FLORIST_USED;


PROCEDURE GET_INTERNATIONAL_FLORIST
(
 IN_ORDER_DETAIL_ID IN NUMBER,
 OUT_FLORIST_CODE  OUT VARCHAR2,
 OUT_STATUS        OUT VARCHAR2,
 OUT_ERROR_MESSAGE OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a record from table efos_city_state
        for a particular city and state.

Input:
        order detail id

Output:
        ref cusor that contains a order_florist_used record

-----------------------------------------------------------------------------*/

v_country VARCHAR2(20);
v_country_id VARCHAR2(2);
v_florist_number frp.global_parms.value%TYPE;

BEGIN

  SELECT c.country INTO v_country
    FROM order_details od, customer c
   WHERE od.order_detail_id = in_order_detail_id
     AND od.recipient_id = c.customer_id;

  IF (v_country = 'US') OR (v_country = 'CA') THEN
    out_florist_code := NULL;
  ELSE
    v_florist_number := frp.misc_pkg.GET_GLOBAL_PARM_VALUE('International Florist','Florist Number');

    -- This is translation logic to decouple Apollo country_id values from those used by Retrans.
    SELECT DISTINCT ck.retrans_country_id
      INTO v_country_id
      FROM order_details od, customer c, ftd_apps.country_keys ck
    WHERE od.order_detail_id = in_order_detail_id
      AND od.recipient_id = c.customer_id
      AND ck.country_id = c.country;

    out_florist_code := v_florist_number || v_country_id;
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
       ROLLBACK;
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_INTERNATIONAL_FLORIST;


PROCEDURE GET_CANCELLED_SCRUB_ORDER
(
IN_ORDER_GUID           IN VARCHAR2,
IN_LINE_NUMBER          IN VARCHAR2,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves a clean order data using the
        scrub order guid and order line number to get the order detail id.

Input:
        order guid                      varchar2
        line number                     varchar2

Output:
        ref cusor that contains clean order data

-----------------------------------------------------------------------------*/

CURSOR order_cur IS
        SELECT  order_detail_id
        FROM    scrub.order_details
        WHERE   order_guid = in_order_guid
        AND     line_number = in_line_number;

v_order_detail_id       number;

BEGIN

OPEN order_cur;
FETCH order_cur INTO v_order_detail_id;
CLOSE order_cur;

GET_ORDER_CUSTOMER_INFO (v_order_detail_id, out_cur);

END GET_CANCELLED_SCRUB_ORDER;


FUNCTION GET_ORDER_DISPOSITION_CODE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning the disposition of the
        given order.


Input:
        order_detail_id                 number

Output:
        order disposition code

-----------------------------------------------------------------------------*/

CURSOR disp_cur IS
        SELECT  order_disp_code
        FROM    order_details
        WHERE   order_detail_id = in_order_detail_id;

v_order_disp_code       order_details.order_disp_code%type;

BEGIN

OPEN disp_cur;
FETCH disp_cur INTO v_order_disp_code;
CLOSE disp_cur;

RETURN v_order_disp_code;

END GET_ORDER_DISPOSITION_CODE;


FUNCTION ORDER_HAS_REFUND
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        shopping cart has any orders with a refund

Input:
        order_guid                      varchar2

Output:
        Y - cart has at least one order with a refund
-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        select  'Y'
        from    refund r
        where   exists
        (
                select  1
                from    order_details od
                where   od.order_guid = in_order_guid
                and     od.order_detail_id = r.order_detail_id
        );

v_check         char (1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

RETURN v_check;

END ORDER_HAS_REFUND;


FUNCTION PAYMENT_EXISTS
(
IN_PAYMENT_ID                   IN PAYMENTS.PAYMENT_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        payment exists

Input:
        payment_id                      number

Output:
        Y/N
-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        select  'Y'
        from    payments
        where   payment_id = in_payment_id;

v_exists        char(1) := 'N';
BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END PAYMENT_EXISTS;


FUNCTION CO_BRAND_EXISTS
(
IN_CO_BRAND_ID                  IN CO_BRAND.CO_BRAND_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        co brand record exists

Input:
        co_brand_id                     number

Output:
        Y/N
-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        select  'Y'
        from    co_brand
        where   co_brand_id = in_co_brand_id;

v_exists        char(1) := 'N';
BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END CO_BRAND_EXISTS;


FUNCTION ORDER_CONTACT_INFO_EXISTS
(
IN_ORDER_CONTACT_INFO_ID                IN ORDER_CONTACT_INFO.ORDER_CONTACT_INFO_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        order contact infor record exists

Input:
        order_contact_info_id                   number

Output:
        Y/N
-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        select  'Y'
        from    order_contact_info
        where   order_contact_info_id = in_order_contact_info_id;

v_exists        char(1) := 'N';
BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END ORDER_CONTACT_INFO_EXISTS;

PROCEDURE GET_ORDER_CUSTOMER_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves order ids and customer name and email.

Input:
        order_detail_id         number

Output:
        ref cusor that contains order data and customer name and email

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  od.order_guid,
                od.order_detail_id,
                o.master_order_number,
                od.external_order_number,
                od.delivery_date,
                o.customer_id,
                (select e.email_address from email e where o.email_id = e.email_id) email_address,
                o.order_date,
                c.first_name,
                c.last_name,
                o.company_id,
                od.order_disp_code,
                o.origin_id,
                coalesce (cpd.phone_number, cpe.phone_number) customer_phone_number,
                decode (cpd.phone_number, null, cpe.extension, cpd.extension) customer_extension,
                (select a.email_address from order_contact_info a where a.order_guid = o.order_guid) alt_email_address,
                o.source_code,
                o.language_id,
                order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) lifecycle_delivered_status,
                case when clean.order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) = 'Y' THEN
                (
                    select status_url
                    from mercury.mercury m
                    join mercury.lifecycle_status ls
                    on ls.mercury_id = m.mercury_id
                    and ls.order_status_code = 'C'
                    where m.reference_number = to_char(od.order_detail_id)
                    and m.msg_type = 'FTD'
                    and m.created_on = (select max(m1.created_on)
                        from mercury.mercury m1
                        where m1.reference_number = m.reference_number
                        and m1.msg_type = 'FTD')
                    and rownum = 1
                )
                end as status_url,
                case when clean.order_query_pkg.get_lifecycle_delivered_status(od.order_detail_id) = 'Y' THEN
                (
                    select decode(status_url, null, 'N', case when sysdate - ls.status_timestamp <= gp1.value then 'Y' else 'N' end)
                    from mercury.mercury m
                    join mercury.lifecycle_status ls
                    on ls.mercury_id = m.mercury_id
                    and ls.order_status_code = 'C'
                    left outer join frp.global_parms gp1
                    on gp1.context = 'ORDER_LIFECYCLE'
                    and gp1.name = 'LIFECYCLE_DELIVERY_EXPIRED_DAYS'
                    where m.reference_number = to_char(od.order_detail_id)
                    and m.msg_type = 'FTD'
                    and m.created_on = (select max(m1.created_on)
                        from mercury.mercury m1
                        where m1.reference_number = m.reference_number
                        and m1.msg_type = 'FTD')
                    and rownum = 1
                )
                end as status_url_active,
                gp.value status_url_expired_msg
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    customer c
        ON      c.customer_id = o.customer_id
        LEFT OUTER JOIN    customer_phones cpd
        ON      c.customer_id = cpd.customer_id
        AND     cpd.phone_type = 'Day'
        LEFT OUTER JOIN    customer_phones cpe
        ON      c.customer_id = cpe.customer_id
        AND     cpe.phone_type = 'Evening'
        LEFT OUTER JOIN    frp.global_parms gp
        ON      gp.context = 'ORDER_LIFECYCLE'
        AND      gp.name = 'LIFECYCLE_DELIVERY_EXPIRED_MSG'
        WHERE   od.order_detail_id = in_order_detail_id;

END GET_ORDER_CUSTOMER_INFO;


FUNCTION IS_SHOPPING_CART_IN_STATUS
(
 IN_MASTER_ORDER_NUMBER  VARCHAR2,
 IN_STATUSES             VARCHAR2
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure checks if any order in the shopping cart is in
        the specified status for the master order number passed in.

Input:
        master order number     VARCHAR2
        statuses                VARCHAR2 (comma delimited string)

Output:
        v_flag                  CHAR

-----------------------------------------------------------------------------*/
v_flag VARCHAR2(5);
v_sql  VARCHAR2(300);
v_cur  TYPES.REF_CURSOR;

BEGIN

  v_sql :=          'SELECT ' || '''TRUE''';
  v_sql := v_sql || '  FROM orders o,';
  v_sql := v_sql || '       order_details od';
  v_sql := v_sql || ' WHERE o.master_order_number = ''' ||
in_master_order_number ||'''';
  v_sql := v_sql || '   AND od.order_guid = o.order_guid';
  v_sql := v_sql || '   AND od.order_disp_code IN (' || IN_STATUSES || ')';

  OPEN v_cur FOR v_sql;
  FETCH v_cur INTO v_flag;
  CLOSE v_cur;

  IF v_flag IS NULL THEN
     v_flag := 'FALSE';
  END IF;

  RETURN v_flag;

END IS_SHOPPING_CART_IN_STATUS;

PROCEDURE GET_CART_REFUND_STATUS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the cart has any order that has
        been cancelled, has refunds, or is in scrub or pending.  If any of
        these are true, then an refund cannot be issued

Input:
        order guid                      varchar2

Output:
        cancelled indicator
        has refunds indicator
        in scrub indicator

-----------------------------------------------------------------------------*/

TYPE int_tab_typ IS TABLE OF INTEGER INDEX BY PLS_INTEGER;
v_id_tab        int_tab_typ;
v_cur           types.ref_cursor;

-- variables for the status cursor
v_attempted_ftd         char(1) := 'N';
v_has_live_ftd          char(1) := 'N';
v_cancel_sent           char(1) := 'N';
v_reject_sent           char(1) := 'N';
v_ftd_status            varchar2(10);
v_delivery_date         date;
v_comp_order        char(1);
v_attempted_can         char(1);

-- variable for return cursor
v_has_refunds_ind               varchar2(1) := 'N';
v_in_scrub_ind                  varchar2(1) := 'N';
v_all_cancelled_ind             varchar2(1) := 'N';
v_delivered_printed_ind         varchar2(1) := 'N';
v_past_devlivery_date_ind       varchar2(1) := 'N';
v_has_live_ftd_ind              varchar2(1) := 'N';

CURSOR oid_cur IS
        select  order_detail_id
        from    order_details
        where   order_guid = in_order_guid;

CURSOR scrub_cur IS
        select  distinct 'Y' in_scrub
        from    order_details od
        where   od.order_guid = in_order_guid
        and     order_disp_code IN ('In-Scrub', 'Pending');

CURSOR a_type_check_cur IS
        select  min (is_order_cancelled(od.order_detail_id)) all_cancelled,
                max (decode (od.order_disp_code, 'Delivered', 'Y', 'Printed', 'Y', 'N')) delivered_printed,
                max (case when od.delivery_date < trunc (sysdate) then 'Y' else 'N' end) past_delivery_date
        from    order_details od
        where   od.order_guid = in_order_guid
        and     (od.order_disp_code IN ('Delivered', 'Printed')
        or      od.delivery_date <= sysdate
        or      is_order_cancelled(od.order_detail_id)='N');

BEGIN

OPEN oid_cur;
FETCH oid_cur BULK COLLECT INTO v_id_tab;
CLOSE oid_cur;

FOR x IN 1..v_id_tab.count LOOP
        order_mesg_pkg.get_order_message_status (v_id_tab (x), null, 'N', v_cur);
        FETCH v_cur INTO
                v_attempted_ftd,
                v_has_live_ftd,
                v_cancel_sent,
                v_reject_sent,
                v_ftd_status,
                v_delivery_date,
                v_comp_order,
                v_attempted_can;
        CLOSE v_cur;

        IF v_has_live_ftd = 'Y' THEN
                v_has_live_ftd_ind := 'Y';
        END IF;
END LOOP;

-- get the refund indicator
v_has_refunds_ind := ORDER_HAS_REFUND (in_order_guid);

-- get the scrub indicator
OPEN scrub_cur;
FETCH scrub_cur INTO v_in_scrub_ind;
CLOSE scrub_cur;

-- get the checks for allowing a type refunds
OPEN a_type_check_cur;
FETCH a_type_check_cur INTO v_all_cancelled_ind, v_delivered_printed_ind, v_past_devlivery_date_ind;
CLOSE a_type_check_cur;

OPEN out_cur FOR
        SELECT  v_has_refunds_ind has_refunds_ind,
                v_in_scrub_ind in_scrub_ind,
                v_all_cancelled_ind all_cancelled_ind,
                v_delivered_printed_ind delivered_printed_ind,
                v_past_devlivery_date_ind past_devlivery_date_ind,
                v_has_live_ftd_ind has_live_ftd_ind
        FROM    dual;


END GET_CART_REFUND_STATUS;


FUNCTION ALLOW_A_TYPE_REFUND
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns if the cart has any order that has
        been delivered or printed, or has past the delivery date. If any of
        these are true, then an A type refund cannot be issued

Input:
        order guid                      varchar2

Output:
        A type indicator - Y/N

-----------------------------------------------------------------------------*/

CURSOR disp_cur IS
        select  distinct 'N'
        from    order_details od
        where   od.order_guid = in_order_guid
        and     (od.order_disp_code IN ('Delivered', 'Printed')
        or      od.delivery_date <= sysdate
        or      is_order_cancelled(od.order_detail_id)='N');

-- initialize the out parameters to Y
v_a_type_ind char(1) := 'Y';

BEGIN

-- get the indicator if any order has been delivered, printed, or has
-- pasted the delivery date, or has not been cancelled
OPEN disp_cur;
FETCH disp_cur INTO v_a_type_ind;
CLOSE disp_cur;

RETURN v_a_type_ind;

END ALLOW_A_TYPE_REFUND;


FUNCTION ORDER_EXISTS
(
IN_ORDER_GUID           IN ORDERS.ORDER_GUID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        shopping cart record exists

Input:
        order_guid              varchar2

Output:
        Y/N
-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT  'Y'
        FROM    orders
        WHERE   order_guid = in_order_guid;

v_check char(1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

RETURN v_check;

END ORDER_EXISTS;


FUNCTION ORDER_DETAIL_EXISTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        order record exists

Input:
        order_detail_id                   number

Output:
        Y/N
-----------------------------------------------------------------------------*/

CURSOR check_cur IS
        SELECT  'Y'
        FROM    order_details
        WHERE   order_detail_id = in_order_detail_id;

v_check char(1) := 'N';

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_check;
CLOSE check_cur;

RETURN v_check;

END ORDER_DETAIL_EXISTS;


PROCEDURE GET_LP_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_LP_INFO_CUR                 OUT TYPES.REF_CURSOR,
OUT_LP_CUSTOMERS_CUR            OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves loss prevention information for the
        given order.

Input:
        order_detail_id         number

Output:
        ref cursor that contains loss prevention indicator information
        ref cursor that contains the customers associated to the lp indicator

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_lp_info_cur FOR
        SELECT  o.loss_prevention_indicator,
                (select count (distinct (o1.customer_id)) from orders o1 where o1.loss_prevention_indicator = o.loss_prevention_indicator) lp_customer_count
        FROM    orders o
        WHERE   o.order_guid = in_order_guid;

OPEN out_lp_customers_cur FOR
        SELECT  c.customer_id,
                c.first_name || ' ' || c.last_name customer_name,
                min (o.order_guid) order_guid
        FROM    customer c
        JOIN    orders o
        ON      c.customer_id = o.customer_id
        WHERE   EXISTS
        (       select  1
                from    orders o1
                where   o1.order_guid = in_order_guid
                and     o1.loss_prevention_indicator = o.loss_prevention_indicator
        )
        GROUP BY c.customer_id, c.last_name, c.first_name
        ORDER BY c.last_name, c.first_name;

END GET_LP_INFO;


PROCEDURE GET_ORIGINAL_ORDER_BILL
(
 IN_ORDER_DETAIL_ID  IN VARCHAR2,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the original order bills
        record for the order detail id passed.

Input:
        order detail id

Output:
        cursor containing order bill on information

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
   SELECT order_bill_id,
          order_detail_id,
          product_amount,
          add_on_amount,
          service_fee,
          shipping_fee,
          discount_amount,
          shipping_tax,
          tax,
          additional_bill_indicator,
          null same_day_fee,            -- field not used and dropped from table
          bill_status,
          bill_date,
          service_fee_tax,
          commission_amount,
          wholesale_amount,
          transaction_amount,
          pdb_price,
          discount_product_price,
          discount_type,
          partner_cost,
          add_on_discount_amount
     FROM order_bills
    WHERE order_detail_id = in_order_detail_id
      AND additional_bill_indicator = 'N';

END GET_ORIGINAL_ORDER_BILL;


PROCEDURE GET_ADDTNL_ORDER_BILL
(
 IN_ORDER_DETAIL_ID  IN VARCHAR2,
 OUT_CURSOR         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the additional order bills
        record for the order detail id passed.

Input:
        order detail id

Output:
        cursor containing order bill on information

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cursor FOR
   SELECT order_bill_id,
          order_detail_id,
          product_amount,
          add_on_amount,
          service_fee,
          shipping_fee,
          discount_amount,
          shipping_tax,
          tax,
          additional_bill_indicator,
          null same_day_fee,            -- field not used and dropped from table
          bill_status,
          bill_date,
          service_fee_tax,
          commission_amount,
          wholesale_amount,
          transaction_amount,
          pdb_price,
          discount_product_price,
          discount_type,
          partner_cost,
          add_on_discount_amount
     FROM order_bills
    WHERE order_detail_id = in_order_detail_id
      AND additional_bill_indicator = 'Y';

END GET_ADDTNL_ORDER_BILL;


PROCEDURE GET_CUSTOMER_HOLD_ORDER_INFO
(
 IN_ORDER_DETAIL_ID          IN VARCHAR2,
 IN_CUTOMER_TYPE             IN CHAR,
 OUT_CURSOR_CUSTOMER        OUT TYPES.REF_CURSOR,
 OUT_CURSOR_CREDIT_CARDS    OUT TYPES.REF_CURSOR,
 OUT_CURSOR_PHONES          OUT TYPES.REF_CURSOR,
 OUT_CURSOR_MEMBERSHIPS     OUT TYPES.REF_CURSOR,
 OUT_CURSOR_EMAILS          OUT TYPES.REF_CURSOR,
 OUT_CURSOR_LOSS_PREVENTION OUT TYPES.REF_CURSOR,
 OUT_CURSOR_AP_ACCOUNTS     OUT TYPES.REF_CURSOR
)
AS
/*------------------------------------------------------------------------------
Description:
        This stored procedure retrieves a customer information from tables
        customer, credit_card, customer_phones, mememberships, emails, and
        order_details' loss prevention for a given order_detail_id and customer
        type flag ('R' for recipient otherwise buyer).

Input:
        order_detail_id    VARCHAR2
        customer type flag CHAR

Output:
        cursor containing customer records
        cursor containing customer credit cards
        cursor containing customer phones
        cursor containing memberships
        cursor containing emails
        cursor containing order_details' loss prevention
------------------------------------------------------------------------------*/
v_rowcount NUMBER;

BEGIN

  IF UPPER(in_cutomer_type) = 'R' THEN
   OPEN out_cursor_customer FOR
     SELECT DISTINCT c.customer_id,
            chrv1.hold_reason_code id_reason,
            che1.customer_hold_entity_id entity_id_customer,
            c.first_name,
            c.last_name,
            chrv2.hold_reason_code name_reason,
            che2.customer_hold_entity_id entity_id_name,
            c.address_1,
            c.address_2,
            c.city,
            c.zip_code,
            c.state,
            c.country,
            chrv3.hold_reason_code address_reason,
            che3.customer_hold_entity_id entity_id_address
       FROM customer_hold_reason_val chrv3,
            customer_hold_entities che3,
            customer_hold_reason_val chrv2,
            customer_hold_entities che2,
            customer_hold_reason_val chrv1,
            customer_hold_entities che1,
            customer c,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND c.customer_id = od.recipient_id
        AND (che1.entity_type (+) = 'CUSTOMER_ID'
            AND che1.hold_value_1 (+) = to_char (c.customer_id)
            AND chrv1.hold_reason_code (+) = che1.hold_reason_code)
        AND (che2.entity_type (+) = 'NAME'
            AND che2.hold_value_1 (+) = (c.first_name || '|' || c.last_name)
            AND chrv2.hold_reason_code (+) = che2.hold_reason_code)
        AND (che3.entity_type (+) = 'ADDRESS'
            AND che3.hold_value_1 (+) = (c.address_1 || '|' || SUBSTR(c.zip_code,1,5))
            AND chrv3.hold_reason_code (+) = che3.hold_reason_code);

   OPEN out_cursor_credit_cards FOR
     SELECT DISTINCT global.encryption.decrypt_it(cc.cc_number,cc.key_name) cc_number,
            chrv.hold_reason_code cc_reason,
            che.customer_hold_entity_id,
            p.avs_code || ' ' ||
            NVL((select a.description
                from ftd_apps.avs_response_codes a
                where a.avs_code = p.avs_code
                    and a.payment_method_id = p.payment_type),
                (select a.description
                from ftd_apps.avs_response_codes a
                where a.avs_code = p.avs_code
                    and a.payment_method_id IS NULL)
            ) avs_response
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            credit_cards cc,
            order_details od,
            payments p,
            ftd_apps.payment_methods pm
      WHERE od.order_detail_id = in_order_detail_id
        AND cc.customer_id = od.recipient_id
        AND (che.entity_type (+) = 'CC_NUMBER'
            AND che.hold_value_1 (+) = cc.cc_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code)
        AND p.cc_id = cc.cc_id
        AND p.created_on = (
            SELECT max(created_on)
            FROM payments p1
            WHERE p1.cc_id = cc.cc_id
            AND p1.additional_bill_id is null
            AND p1.payment_indicator = 'P')
        AND pm.payment_method_id = cc.cc_type
        AND pm.payment_type = 'C'
        AND global.encryption.decrypt_it(cc.cc_number,cc.key_name) != '1611015378284387';


   OPEN out_cursor_phones FOR
     SELECT DISTINCT cp.phone_number,
            chrv.hold_reason_code phone_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            customer_phones cp,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND cp.customer_id = od.recipient_id
        AND cp.phone_number IS NOT NULL
        AND cp.phone_type = 'Day'
        AND (che.entity_type (+) = 'PHONE_NUMBER'
            AND che.hold_value_1 (+) = cp.phone_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   SELECT COUNT(DISTINCT cp.phone_number)
     INTO v_rowcount
     FROM customer_hold_reason_val chrv,
          customer_hold_entities che,
          customer_phones cp,
          order_details od
    WHERE od.order_detail_id = in_order_detail_id
      AND cp.customer_id = od.recipient_id
      AND cp.phone_number IS NOT NULL
      AND cp.phone_type = 'Day'
      AND (che.entity_type (+) = 'PHONE_NUMBER'
          AND che.hold_value_1 (+) = cp.phone_number
          AND chrv.hold_reason_code (+) = che.hold_reason_code);

   IF v_rowcount = 0 THEN
     OPEN out_cursor_phones FOR
       SELECT DISTINCT cp.phone_number,
              chrv.hold_reason_code phone_reason,
              che.customer_hold_entity_id
         FROM customer_hold_reason_val chrv,
              customer_hold_entities che,
              customer_phones cp,
              order_details od
        WHERE od.order_detail_id = in_order_detail_id
          AND cp.customer_id = od.recipient_id
          AND cp.phone_number IS NOT NULL
          AND cp.phone_type = 'Evening'
          AND (che.entity_type (+) = 'PHONE_NUMBER'
              AND che.hold_value_1 (+) = cp.phone_number
              AND chrv.hold_reason_code (+) = che.hold_reason_code);
   END IF;

   OPEN out_cursor_memberships FOR
     SELECT DISTINCT m.membership_number,
            m.membership_type,
            chrv.hold_reason_code membership_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            memberships m,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND m.membership_id = o.membership_id
        AND (che.entity_type (+) = 'MEMBERSHIP_ID'
            AND che.hold_value_1 (+) = TO_CHAR(m.membership_number)
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_emails FOR
     SELECT DISTINCT e.email_address,
            chrv.hold_reason_code email_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            email e,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND e.email_id = o.email_id
        AND (che.entity_type (+) = 'EMAIL_ADDRESS'
            AND che.hold_value_1 (+) = e.email_address
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_loss_prevention FOR
     SELECT DISTINCT o.loss_prevention_indicator,
            chrv.hold_reason_code loss_prevention_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND (che.entity_type (+) = 'LOSS_PREVENTION_INDICATOR'
             AND che.hold_value_1 (+) = o.loss_prevention_indicator
             AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_ap_accounts FOR
     SELECT DISTINCT aa.ap_account_id ap_account,
            global.encryption.decrypt_it(aa.ap_account_txt,aa.key_name) ap_account_txt,
            pm.description ap_account_type_desc,
            chrv.hold_reason_code ap_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            ftd_apps.payment_methods pm,
            ap_accounts aa,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND aa.customer_id = od.recipient_id
        AND pm.payment_method_id = aa.payment_method_id
        AND (che.entity_type (+) = 'AP_ACCOUNT'
            AND che.hold_value_1 (+) = aa.ap_account_id
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

  ELSE

   OPEN out_cursor_customer FOR
     SELECT DISTINCT c.customer_id,
            chrv1.hold_reason_code id_reason,
            che1.customer_hold_entity_id entity_id_customer,
            c.first_name,
            c.last_name,
            chrv2.hold_reason_code name_reason,
            che2.customer_hold_entity_id entity_id_name,
            c.address_1,
            c.address_2,
            c.city,
            c.zip_code,
            c.state,
            c.country,
            chrv3.hold_reason_code address_reason,
            che3.customer_hold_entity_id entity_id_address
       FROM customer_hold_reason_val chrv3,
            customer_hold_entities che3,
            customer_hold_reason_val chrv2,
            customer_hold_entities che2,
            customer_hold_reason_val chrv1,
            customer_hold_entities che1,
            customer c,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND c.customer_id = o.customer_id
        AND (che1.entity_type (+) = 'CUSTOMER_ID'
            AND che1.hold_value_1 (+) = c.customer_id
            AND chrv1.hold_reason_code (+) = che1.hold_reason_code)
        AND (che2.entity_type (+) = 'NAME'
            AND che2.hold_value_1 (+) = (c.first_name || '|' || c.last_name)
            AND chrv2.hold_reason_code (+) = che2.hold_reason_code)
        AND (che3.entity_type (+) = 'ADDRESS'
            AND che3.hold_value_1 (+) = (c.address_1 || '|' || SUBSTR(c.zip_code,1,5))
            AND chrv3.hold_reason_code (+) = che3.hold_reason_code);

   OPEN out_cursor_credit_cards FOR
     SELECT DISTINCT global.encryption.decrypt_it(cc.cc_number,cc.key_name) cc_number,
            chrv.hold_reason_code cc_reason,
            che.customer_hold_entity_id,
            p.avs_code || ' ' ||
            NVL((select a.description
                from ftd_apps.avs_response_codes a
                where a.avs_code = p.avs_code
                    and a.payment_method_id = p.payment_type),
                (select a.description
                from ftd_apps.avs_response_codes a
                where a.avs_code = p.avs_code
                    and a.payment_method_id IS NULL)
            ) avs_response
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            credit_cards cc,
            orders o,
            order_details od,
            payments p,
            ftd_apps.payment_methods pm
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND cc.customer_id = o.customer_id
        AND (che.entity_type (+) = 'CC_NUMBER'
            AND che.hold_value_1 (+) = cc.cc_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code)
        AND p.cc_id = cc.cc_id
        AND p.created_on = (
            SELECT max(created_on)
            FROM payments p1
            WHERE p1.cc_id = cc.cc_id
            AND p1.additional_bill_id is null
            AND p1.payment_indicator = 'P')
        AND pm.payment_method_id = cc.cc_type
        AND pm.payment_type = 'C'
        AND global.encryption.decrypt_it(cc.cc_number,cc.key_name) != '1611015378284387';

   OPEN out_cursor_phones FOR
     SELECT DISTINCT cp.phone_number,
            chrv.hold_reason_code phone_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            customer_phones cp,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND cp.customer_id = o.customer_id
        AND cp.phone_number IS NOT NULL
        AND (che.entity_type (+) = 'PHONE_NUMBER'
            AND che.hold_value_1 (+) = cp.phone_number
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_memberships FOR
     SELECT DISTINCT m.membership_number,
            m.membership_type,
            chrv.hold_reason_code membership_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            memberships m,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND m.membership_id = o.membership_id
        AND (che.entity_type (+) = 'MEMBERSHIP_ID'
            AND che.hold_value_1 (+) = TO_CHAR(m.membership_number)
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_emails FOR
     SELECT DISTINCT e.email_address,
            chrv.hold_reason_code email_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            email e,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND e.email_id = o.email_id
        AND (che.entity_type (+) = 'EMAIL_ADDRESS'
            AND che.hold_value_1 (+) = e.email_address
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_loss_prevention FOR
     SELECT DISTINCT o.loss_prevention_indicator,
            chrv.hold_reason_code loss_prevention_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND (che.entity_type (+) = 'LOSS_PREVENTION_INDICATOR'
             AND che.hold_value_1 (+) = o.loss_prevention_indicator
             AND chrv.hold_reason_code (+) = che.hold_reason_code);

   OPEN out_cursor_ap_accounts FOR
     SELECT DISTINCT aa.ap_account_id ap_account,
            global.encryption.decrypt_it(aa.ap_account_txt,aa.key_name) ap_account_txt,
            pm.description ap_account_type_desc,
            chrv.hold_reason_code ap_reason,
            che.customer_hold_entity_id
       FROM customer_hold_reason_val chrv,
            customer_hold_entities che,
            ftd_apps.payment_methods pm,
            ap_accounts aa,
            orders o,
            order_details od
      WHERE od.order_detail_id = in_order_detail_id
        AND o.order_guid = od.order_guid
        AND aa.customer_id = o.customer_id
        AND pm.payment_method_id = aa.payment_method_id
        AND (che.entity_type (+) = 'AP_ACCOUNT'
            AND che.hold_value_1 (+) = aa.ap_account_id
            AND chrv.hold_reason_code (+) = che.hold_reason_code);

  END IF;

END GET_CUSTOMER_HOLD_ORDER_INFO;


PROCEDURE GET_ORDER_HOLD
(
 IN_ORDER_DETAIL_ID IN ORDER_HOLD.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all order_hold records for the
        order_detail_id passed in.

Input:
        order_detail_id    NUMBER

Output:
        cursor containing the order_hold records

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_cursor FOR
    SELECT order_detail_id,
           reason,
           reason_text,
           timezone,
           auth_count,
           release_date,
           status
      FROM order_hold
     WHERE order_detail_id = in_order_detail_id
     ORDER BY release_date;


END GET_ORDER_HOLD;


PROCEDURE GET_HELD_ORDERS
(
 IN_CUSTOMER_ID  IN NUMBER,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the orders that are held and have
        the passed in customer as either the buyer or reciepient.

Input:
        customer_id - NUMBER

Output:
        cursor containing the order_details

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_cursor FOR
    SELECT DISTINCT od.order_detail_id,
           od.order_guid,
           od.external_order_number
      FROM order_hold oh,
           order_details od,
           orders o
     WHERE o.customer_id = in_customer_id
       AND od.order_guid = o.order_guid
       AND od.order_detail_id = oh.order_detail_id
       AND oh.reason IN ('CUSTOMER', 'FRAUD_FLAG', 'LP_INDICATOR')
   UNION
    SELECT DISTINCT od.order_detail_id,
           od.order_guid,
           od.external_order_number
      FROM order_hold oh,
           order_details od
     WHERE od.recipient_id = in_customer_id
       AND od.order_detail_id = oh.order_detail_id
       AND oh.reason IN ('CUSTOMER', 'FRAUD_FLAG', 'LP_INDICATOR');


END GET_HELD_ORDERS;


PROCEDURE GET_HELD_ORDERS
(
 IN_CUSTOMER_ID  IN NUMBER,
 IN_ORDER_GUID   IN ORDER_DETAILS.ORDER_GUID%TYPE,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the orders that are held and have
        the passed in order_guid and customer as either the buyer
        or reciepient.

Input:
        customer_id  NUMBER
        order_guid   VARCHAR2

Output:
        cursor containing the order_details

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_cursor FOR
    SELECT DISTINCT od.order_detail_id,
           od.order_guid,
           od.external_order_number
      FROM order_hold oh,
           order_details od,
           orders o
     WHERE o.customer_id = in_customer_id
       AND o.order_guid = in_order_guid
       AND od.order_guid = o.order_guid
       AND od.order_detail_id = oh.order_detail_id
       AND oh.reason IN ('CUSTOMER', 'FRAUD_FLAG', 'LP_INDICATOR')
   UNION
    SELECT DISTINCT od.order_detail_id,
           od.order_guid,
           od.external_order_number
      FROM order_hold oh,
           order_details od
     WHERE od.recipient_id = in_customer_id
       AND od.order_guid = in_order_guid
       AND od.order_detail_id = oh.order_detail_id
       AND oh.reason IN ('CUSTOMER', 'FRAUD_FLAG', 'LP_INDICATOR');


END GET_HELD_ORDERS;


PROCEDURE GET_GNADD_HELD_ORDERS
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all order_hold records whose
         reason field contains 'GNADD'.

Input:
        N/A

Output:
        cursor containing the GNADD order_hold records

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_cursor FOR
    SELECT order_detail_id,
           reason,
           reason_text,
           timezone,
           auth_count,
           release_date,
           status
      FROM order_hold
     WHERE reason = 'GNADD'
     ORDER BY release_date;


END GET_GNADD_HELD_ORDERS;


PROCEDURE GET_PRODUCT_HELD_ORDERS
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all order_hold records whose
         reason field contains 'PRODUCT'.

Input:
        N/A

Output:
        cursor containing the PRODUCT order_hold records

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_cursor FOR
    SELECT order_detail_id,
           reason,
           reason_text,
           timezone,
           auth_count,
           release_date,
           status
      FROM order_hold
     WHERE reason = 'PRODUCT'
       AND release_date <= SYSDATE;


END GET_PRODUCT_HELD_ORDERS;


PROCEDURE GET_EARLY_GNADD_HELD_ORDERS
(
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all order_hold records whose
         reason field contains 'GNADD'.

Input:
        N/A

Output:
        cursor containing the GNADD order_hold records

-----------------------------------------------------------------------------*/
v_gnadd_date DATE;

BEGIN

 SELECT TO_DATE(frp.misc_pkg.get_global_parm_value ('ORDER_PROCESSING', 'GNADD_DATE'), 'Month DD, YYYY')
   INTO v_gnadd_date
   FROM DUAL;

  OPEN out_cursor FOR
    SELECT oh.order_detail_id,
           oh.reason,
           oh.reason_text,
           oh.timezone,
           oh.auth_count,
           oh.release_date,
           oh.status
      FROM order_hold oh,
           order_details od
     WHERE oh.reason = 'GNADD'
       AND od.order_detail_id = oh.order_detail_id
       AND od.delivery_date < v_gnadd_date;


END GET_EARLY_GNADD_HELD_ORDERS;


FUNCTION IS_SHOPPING_CART_BILLED
(
 IN_MASTER_ORDER_NUMBER  IN ORDERS.MASTER_ORDER_NUMBER%TYPE
)
RETURN CHAR
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure determines if any of the order_bills for the
        shopping cart has not been billed.

Input:
        master_order_number    VARCHAR2

Output:
        Y/N
-----------------------------------------------------------------------------*/

v_flag char(1);
v_count NUMBER;

BEGIN

  BEGIN

    SELECT COUNT(1)
      INTO v_count
      FROM order_bills ob
     WHERE EXISTS (SELECT 1
                     FROM orders o,
                          order_details od
                    WHERE o.master_order_number = in_master_order_number
                      AND od.order_guid = o.order_guid
                      AND od.order_detail_id = ob.order_detail_id)
       AND ob.bill_status IS NOT NULL
       AND ob.bill_status = 'Unbilled';

    EXCEPTION WHEN NO_DATA_FOUND THEN v_count := 0;

  END;

  IF v_count = 0 THEN
    v_flag := 'N';
  ELSE
    v_flag := 'Y';
  END IF;

  RETURN v_flag;

END IS_SHOPPING_CART_BILLED;


PROCEDURE GET_MEMBERSHIP_BY_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving membership information
        for the given order detail id.

Input:
        order_detail_id                     number

Output:
        cursor containing membership information

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                m.membership_id,
                m.membership_number,
                m.membership_type,
                m.first_name membership_first_name,
                m.last_name membership_last_name,
                od.miles_points,
                od.miles_points_post_date
        FROM    order_details od
        JOIN    memberships m
        ON      m.membership_id = coalesce (od.membership_id, (select o.membership_id from orders o where o.order_guid = od.order_guid))
        WHERE   od.order_detail_id = in_order_detail_id;

END GET_MEMBERSHIP_BY_ORDER;


FUNCTION GET_ORDR_FLORIST_USED_SEQ_LIST
(
 IN_FLORIST_ID         IN CLEAN.ORDER_FLORIST_USED.FLORIST_ID%TYPE,
 IN_ORDER_DETAIL_ID    IN CLEAN.ORDER_FLORIST_USED.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure concatenates all of the sequences of an
        order_detail and florist pair into one string.

Input:
        order_detail_id  NUMBER
        florist_id       VARCHAR2

Output:
        v_sequences      VARCHAR2

-----------------------------------------------------------------------------*/

  CURSOR ofu_cur (odid NUMBER, fid VARCHAR2) IS
    SELECT sequence
      FROM clean.order_florist_used
     WHERE order_detail_id = odid
       AND florist_id = fid;

  v_sequences VARCHAR2(1000) := NULL;
  v_firstime BOOLEAN := TRUE;

BEGIN

  BEGIN
    FOR ofu_rec IN ofu_cur(in_order_detail_id, in_florist_id) LOOP
        IF v_firstime THEN
           v_sequences := ofu_rec.sequence;
           v_firstime := FALSE;
        ELSE
           v_sequences := v_sequences || ','|| ofu_rec.sequence;
        END IF;
    END LOOP;
  END;

  RETURN v_sequences;

END GET_ORDR_FLORIST_USED_SEQ_LIST;


PROCEDURE GET_PAYMENTS_BY_ORD_DTL_ID
(
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns credit card payment records for the given
   order_detail_id.

Input:
        order_detail_id      NUMBER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/

v_count NUMBER;

BEGIN

  OPEN out_cursor FOR
      SELECT p.payment_id,
             p.order_guid,
             p.additional_bill_id,
             p.payment_type,
             p.cc_id,
             p.auth_result,
             p.auth_number,
             p.avs_code,
             p.acq_reference_number,
             p.gc_coupon_number,
             p.auth_override_flag,
             p.credit_amount,
             p.debit_amount,
             p.payment_indicator,
             p.refund_id,
             p.auth_date,
             null approval_code,
             p.auth_char_indicator,
             p.transaction_id,
             p.validation_code,
             p.auth_source_code,
             p.response_code,
             p.aafes_ticket_number
        FROM payments p,
             order_details od
       WHERE od.order_detail_id = in_order_detail_id
         AND p.order_guid = od.order_guid
         AND p.additional_bill_id IS NULL;

END GET_PAYMENTS_BY_ORD_DTL_ID;


PROCEDURE GET_FIRST_PMNT_BY_ORD_DTL_ID
(
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns first main payment record for the given
   order_detail_id. If the payment is a combination with a GC,
   the non-GC payment will be returned. The procedure should only
   return one record.

Input:
        order_detail_id      NUMBER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/

v_count NUMBER;
v_sql varchar2(2000);

BEGIN

  -- number of payments
  SELECT count(*)
    INTO v_count
    FROM payments p,
         order_details od
   WHERE od.order_detail_id = in_order_detail_id
     AND p.order_guid = od.order_guid
     AND p.additional_bill_id IS NULL
     AND p.payment_indicator = 'P';

  v_sql := 'SELECT p.payment_id,
             p.order_guid,
             p.additional_bill_id,
             p.payment_type,
             p.cc_id,
             p.auth_result,
             p.auth_number,
             p.avs_code,
             p.acq_reference_number,
             p.gc_coupon_number,
             p.auth_override_flag,
             p.credit_amount,
             p.debit_amount,
             p.payment_indicator,
             p.refund_id,
             p.auth_date,
             null approval_code,
             p.auth_char_indicator,
             p.transaction_id,
             p.validation_code,
             p.auth_source_code,
             p.response_code,
             p.aafes_ticket_number,
             p.miles_points_credit_amt,
             p.miles_points_debit_amt,
             o.company_id,
             p.cardinal_verified_flag,
             p.route
        FROM payments p,
             order_details od,
             orders o
       WHERE od.order_detail_id = ' || in_order_detail_id ||
        ' AND p.order_guid = od.order_guid
         AND o.order_guid = od.order_guid
         AND p.additional_bill_id IS NULL
         AND p.payment_indicator = ''P''';


  -- Filter out GC and GD payment type.
  IF v_count > 1 THEN
    v_sql := v_sql || ' AND p.payment_type <> ''GC'' AND p.payment_type <> ''GD''';
  END IF;


  OPEN out_cursor FOR v_sql;


END GET_FIRST_PMNT_BY_ORD_DTL_ID;


PROCEDURE GET_PAYMENT_TYPES_BY_ORDER
(
IN_ORDER_DETAIL_ID      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns payment types for the given order.

Input:
        order_detail_id         number

Output:
        cursor containing payment types

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cursor FOR
        SELECT  DISTINCT
                p.payment_type
        FROM    payments p
        WHERE   EXISTS
        (
                select  1
                from    order_details od
                join    order_bills ob
                on      od.order_detail_id = ob.order_detail_id
                where   od.order_detail_id = in_order_detail_id
                and     ((od.order_guid = p.order_guid and p.additional_bill_id is not null)
                or      (ob.order_bill_id = p.additional_bill_id))
        )
        ORDER BY p.payment_type;

END GET_PAYMENT_TYPES_BY_ORDER;


PROCEDURE GET_ORDER_BILL_BY_ID
(
IN_ORDER_BILL_ID                IN ORDER_BILLS.ORDER_BILL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for order bill information for the
        given order bill id

Input:
        order_bill_id                   number

Output:
        cursor containing order_bill info

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                order_bill_id,
                order_detail_id,
                product_amount,
                add_on_amount,
                service_fee,
                shipping_fee,
                discount_amount,
                shipping_tax,
                tax,
                additional_bill_indicator,
                null same_day_fee,              -- field not used and dropped from table
                bill_status,
                bill_date,
                service_fee_tax,
                commission_amount,
                wholesale_amount,
                transaction_amount,
                discount_product_price,
                add_on_discount_amount
        FROM    order_bills
        WHERE   order_bill_id = in_order_bill_id;

END GET_ORDER_BILL_BY_ID;


PROCEDURE GET_LAST_CC_USED
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the last credit card the
        customer used

Input:
        order_detail_id                   number

Output:
        cursor containing credit card info

-----------------------------------------------------------------------------*/

-- cursor to get the last used credit card id from the payments associated
-- to the given order
CURSOR cc_cur IS
        SELECT  p.cc_id, p.wallet_indicator
        FROM    CLEAN.order_details od
        JOIN    CLEAN.order_bills ob
        ON      od.order_detail_id = ob.order_detail_id
        JOIN    CLEAN.payments p
        ON      (p.order_guid = od.order_guid and p.additional_bill_id is null and p.refund_id is null and payment_type != 'GD')
        OR      (p.additional_bill_id = ob.order_bill_id)
        WHERE   od.order_detail_id = in_order_detail_id
        ORDER BY p.updated_on desc;

v_cc_id              CLEAN.payments.cc_id%type;
v_wallet_indicator   CLEAN.payments.wallet_indicator%type;

BEGIN

OPEN cc_cur;
FETCH cc_cur INTO v_cc_id, v_wallet_indicator;
CLOSE cc_cur;

OPEN OUT_CUR FOR
        select  cc_id,
                cc_type,
                global.encryption.decrypt_it (cc_number,key_name) cc_number,
                cc_expiration, 
                v_wallet_indicator as wallet_indicator
        from    CLEAN.credit_cards
        where   cc_id = v_cc_id;

END GET_LAST_CC_USED;

PROCEDURE GET_LAST_CC_USED_FS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the last credit card the
        customer used

Input:
        order_detail_id                   number

Output:
        cursor containing credit card info

-----------------------------------------------------------------------------*/

-- cursor to get the last used credit card id from the payments associated
-- to the given order
CURSOR cc_cur IS
        SELECT  p.cc_id
        FROM    order_details od
        JOIN    order_bills ob
        ON      od.order_detail_id = ob.order_detail_id
        JOIN    payments p
        ON      (p.order_guid = od.order_guid and p.additional_bill_id is null and p.refund_id is null and payment_type != 'GD')
        OR      (p.additional_bill_id = ob.order_bill_id)
        WHERE   od.order_detail_id = in_order_detail_id
        ORDER BY p.updated_on desc;

v_cc_id         payments.cc_id%type;

BEGIN

OPEN cc_cur;
FETCH cc_cur INTO v_cc_id;
CLOSE cc_cur;

OPEN OUT_CUR FOR
        select  cc_id,
                cc_type,
                cc_number,
                cc_expiration,
                key_name
        from    credit_cards
        where   cc_id = v_cc_id;

END GET_LAST_CC_USED_FS;

PROCEDURE GET_ORDER_DETAIL_ID_LIST
(
 IN_DATE                IN DATE,
 IN_ORDER_DATE          IN DATE,
 IN_PARTNER_NAME        IN FTD_APPS.PARTNER_PROGRAM.PARTNER_NAME%TYPE,
 IN_START_POSITION      IN NUMBER,
 IN_MAX_NUMBER_RETURNED IN NUMBER,
 IN_DAYS_PAST_DELIVERY  IN NUMBER,
 OUT_MAX_ROWS           OUT NUMBER,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the order_detail_ids from
        order_details that are related to orders, refund and
        refund_disposition_val.

Input:
        in_date           DATE
        in_order_date     DATE
        in_start_position NUMBER        11/22 removed paging logic per JP
        in_end_position   NUMBER        need to return all records

Output:
        out_max_rows      NUMBER
        cursor containing order_detail_ids info

-----------------------------------------------------------------------------*/

CURSOR order_cur IS
        SELECT COUNT (DISTINCT (od.order_detail_id))
            FROM orders o,
                 order_details od,
                 ftd_apps.source_master sm,
                 ftd_apps.source_program_ref spr,
                 ftd_apps.partner_program pp
           WHERE o.order_date >= TRUNC(in_order_date)
             AND o.order_guid = od.order_guid
             AND (od.delivery_date + in_days_past_delivery) >= TRUNC(in_date)
             AND (od.delivery_date + in_days_past_delivery) < TRUNC(in_date + 1)
             AND od.miles_points_post_date IS NULL
             AND od.source_code = sm.source_code
             AND sm.source_code = spr.source_code
             AND spr.start_date = ftd_apps.source_query_pkg.get_src_prg_ref_max_start_date(spr.source_code,in_date)
             AND spr.program_name = pp.program_name
             AND pp.partner_name = in_partner_name;

BEGIN

OPEN order_cur;
FETCH order_cur INTO out_max_rows;
CLOSE order_cur;

open out_cursor for
        SELECT DISTINCT (od.order_detail_id) order_detail_id
            FROM orders o,
                 order_details od,
                 ftd_apps.source_master sm,
                 ftd_apps.source_program_ref spr,
                 ftd_apps.partner_program pp
           WHERE o.order_date >= TRUNC(in_order_date)
             AND o.order_guid = od.order_guid
             AND (od.delivery_date + in_days_past_delivery) >= TRUNC(in_date)
             AND (od.delivery_date + in_days_past_delivery) < TRUNC(in_date + 1)
             AND od.miles_points_post_date IS NULL
             AND od.source_code = sm.source_code
             AND sm.source_code = spr.source_code
             AND spr.start_date = ftd_apps.source_query_pkg.get_src_prg_ref_max_start_date(spr.source_code,in_date)
             AND spr.program_name = pp.program_name
             AND pp.partner_name = in_partner_name;

END GET_ORDER_DETAIL_ID_LIST;


PROCEDURE GET_RANGE_ORDER_DETAIL_ID_LIST
(
 IN_DATE                IN DATE,
 IN_ORDER_DATE_START    IN DATE,
 IN_ORDER_DATE_END      IN DATE,
 IN_PARTNER_NAME        IN FTD_APPS.PARTNER_PROGRAM.PARTNER_NAME%TYPE,
 IN_DAYS_PAST_DELIVERY  IN NUMBER,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the order_detail_ids from
        order_details that are related to orders, refund and
        refund_disposition_val.

Input:
        in_date               DATE          basis for delivery date range
        in_order_date_start   DATE          start of order date range
        in_order_date_end     DATE          end of order date range
                                            Add 1 to this value to make the comparison work
        in_partner_name       NUMBER
        in_days_past_delivery NUMBER        length of delivery date range

Output:
        cursor containing order_detail_ids info

-----------------------------------------------------------------------------*/

BEGIN

open out_cursor for
        SELECT DISTINCT (od.order_detail_id) order_detail_id
            FROM orders o,
                 order_details od,
                 ftd_apps.source_master sm,
                 ftd_apps.source_program_ref spr,
                 ftd_apps.partner_program pp
           WHERE o.order_date >= TRUNC(in_order_date_start)
             AND o.order_date < TRUNC(in_order_date_end)+1
             AND o.order_guid = od.order_guid
             AND (od.delivery_date + in_days_past_delivery) >= TRUNC(in_date)
             AND (od.delivery_date + in_days_past_delivery) < TRUNC(in_date + 1)
             AND od.miles_points_post_date IS NULL
             AND od.source_code = sm.source_code
             AND sm.source_code = spr.source_code
             AND spr.start_date = ftd_apps.source_query_pkg.get_src_prg_ref_max_start_date(spr.source_code,in_date)
             AND spr.program_name = pp.program_name
             AND pp.partner_name = in_partner_name;

END GET_RANGE_ORDER_DETAIL_ID_LIST;


PROCEDURE GET_ORDER_MEMBERSHIP_INFO
(
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the records from order_details,
        orders, and memberships that are related to each other and tables
        refund and refund_disposition_val for the order_detail_id passed in.

Input:
        order_detail_id         NUMBER

Output:
        cursor containing orders, order_details, memberships info

-----------------------------------------------------------------------------*/

v_main_sql VARCHAR2(3000);
v_rowcount_sql VARCHAR2(3000);


BEGIN

  OPEN out_cursor FOR
    SELECT o.company_id,
           o.master_order_number,
           od.external_order_number,
           od.source_code,
           od.delivery_date,
           od.order_detail_id,
           m.first_name,
           m.last_name,
           m.membership_type,
           m.membership_number,
           e.email_address,
           pm.product_name,
           c.address_1,
           p.credit_amount,
           decode(p.cc_id,NULL,NULL,global.encryption.decrypt_it (cc.cc_number,cc.key_name)) cc_number,
           c.first_name,
           c.last_name,
           c.address_2,
           c.city,
           c.state,
           c.zip_code,
           o.order_date,
           c.country
      FROM order_details od
      JOIN orders o
      ON   o.order_guid = od.order_guid
      JOIN ftd_apps.company_master cm
      ON   o.company_id = cm.company_id
      JOIN customer c
      ON   o.customer_id = c.customer_id
      LEFT OUTER JOIN email e
      ON   e.email_id = o.email_id
      LEFT OUTER JOIN memberships m
      ON   m.membership_id = od.membership_id
      LEFT OUTER JOIN ftd_apps.product_master pm
      ON   pm.product_id = od.product_id
      LEFT OUTER JOIN payments p                -- get the original credit card payment if it exists
      ON   o.order_guid = p.order_guid
      AND  p.additional_bill_id is null
      AND  p.refund_id is null
      AND  p.cc_id is not null
      LEFT OUTER JOIN credit_cards cc
      ON   p.cc_id = cc.cc_id
     WHERE od.order_detail_id = in_order_detail_id
       AND ( (refund_pkg.is_order_fully_refunded(in_order_detail_id) = 'N')
              OR (refund_pkg.is_order_fully_refunded(in_order_detail_id) = 'Y' AND ALLOW_MILES_POINTS(in_order_detail_id) = 'Y')
           ) ;

END GET_ORDER_MEMBERSHIP_INFO;


PROCEDURE GET_ORDER_BILL_BY_PAYMENT
(
IN_PAYMENT_ID                   IN PAYMENTS.PAYMENT_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for order bill information for the
        given payment

Input:
        payment_id                      number

Output:
        cursor containing order_bill info

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                ob.order_bill_id,
                ob.order_detail_id,
                ob.product_amount,
                ob.add_on_amount,
                ob.service_fee,
                ob.shipping_fee,
                ob.discount_amount,
                ob.shipping_tax,
                ob.tax,
                ob.additional_bill_indicator,
                null same_day_fee,                      -- field not used and dropped from table
                ob.bill_status,
                ob.bill_date,
                ob.service_fee_tax,
                ob.commission_amount,
                ob.wholesale_amount,
                ob.transaction_amount,
                ob.discount_product_price,
                ob.tax1_name,
                ob.tax1_amount,
                ob.tax1_description,
                ob.tax1_rate,
                ob.tax2_name,
                ob.tax2_amount,
                ob.tax2_description,
                ob.tax2_rate,
                ob.tax3_name,
                ob.tax3_amount,
                ob.tax3_description,
                ob.tax3_rate,
                ob.tax4_name,
                ob.tax4_amount,
                ob.tax4_description,
                ob.tax4_rate,
                ob.tax5_name,
                ob.tax5_amount,
                ob.tax5_description,
                ob.tax5_rate,
                ob.add_on_discount_amount
        FROM    order_bills ob
        WHERE
                -- get all original bills associated to the cart level payment
        (       ob.additional_bill_indicator = 'N'
                AND     EXISTS
                (       select  1
                        from    payments p
                        join    order_details od
                        on      p.order_guid = od.order_guid
                        where   p.payment_id = in_payment_id
                        and     p.additional_bill_id is null
                        and     p.refund_id is null
                        and     od.order_detail_id = ob.order_detail_id
                )
        )
        UNION
        SELECT
                ob.order_bill_id,
                ob.order_detail_id,
                ob.product_amount,
                ob.add_on_amount,
                ob.service_fee,
                ob.shipping_fee,
                ob.discount_amount,
                ob.shipping_tax,
                ob.tax,
                ob.additional_bill_indicator,
                null same_day_fee,                      -- field not used and dropped from table
                ob.bill_status,
                ob.bill_date,
                ob.service_fee_tax,
                ob.commission_amount,
                ob.wholesale_amount,
                ob.transaction_amount,
                ob.discount_product_price,
                ob.tax1_name,
                ob.tax1_amount,
                ob.tax1_description,
                ob.tax1_rate,
                ob.tax2_name,
                ob.tax2_amount,
                ob.tax2_description,
                ob.tax2_rate,
                ob.tax3_name,
                ob.tax3_amount,
                ob.tax3_description,
                ob.tax3_rate,
                ob.tax4_name,
                ob.tax4_amount,
                ob.tax4_description,
                ob.tax4_rate,
                ob.tax5_name,
                ob.tax5_amount,
                ob.tax5_description,
                ob.tax5_rate,
                ob.add_on_discount_amount
        FROM    order_bills ob
        WHERE
                -- get the additional bill associated to the order level payment
        (       ob.additional_bill_indicator = 'Y'
                AND     EXISTS
                (       select  1
                        from    payments p
                        where   p.payment_id = in_payment_id
                        and     p.additional_bill_id = ob.order_bill_id
                )
        );

END GET_ORDER_BILL_BY_PAYMENT;


PROCEDURE GET_ORIGINAL_USED_CC
(
IN_ORDER_GUID                   IN PAYMENTS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the originally used credit
        card for the order

Input:
        order_guid                      varchar2

Output:
        cursor containing credit card info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT  cc.cc_id,
                cc.cc_type,
                global.encryption.decrypt_it (cc.cc_number,cc.key_name) cc_number,
                cc.cc_expiration
        FROM    credit_cards cc
        JOIN    payments p
        ON      cc.cc_id = p.cc_id
        WHERE   p.order_guid = in_order_guid
        AND     p.additional_bill_id is null
        AND     p.refund_id is null;

END GET_ORIGINAL_USED_CC;


PROCEDURE GET_ROUTABLE_QUEUED_ORDERS
(
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a list of order detail ids that are
        are in the zip queue that can be routed to one or more florist.

Input:
        N/A

Output:
        out_cursor contains a list of order_detail_ids

-----------------------------------------------------------------------------*/

CURSOR cur IS
    SELECT od.order_detail_id,
           od.product_id,
           od.delivery_date,
           od.delivery_date_range_end,
           od.source_code,
           c.zip_code,
           UPPER(c.city) city,
           UPPER(c.state) state,
           ob.product_amount
      FROM clean.queue q,
           clean.order_details od,
           clean.customer c,
           clean.order_bills ob
     WHERE q.queue_type = 'ZIP'
       AND od.order_detail_id = q.order_detail_id
       AND od.recipient_id = c.customer_id
       AND ob.order_detail_id = od.order_detail_id;

v_cursor TYPES.REF_CURSOR;
v_index  INTEGER := 0;

v_florist_id              VARCHAR2(9);
v_score                   NUMBER;
v_weight                  NUMBER;
v_florist_name            VARCHAR2(50);
v_phone_number            VARCHAR2(12);
v_mercury_flag            CHAR(1);
v_super_florist_flag      CHAR(1);
v_sunday_delivery_flag    CHAR(1);
v_cutoff_time             NUMBER;
v_status                  VARCHAR2(100);
v_codification_id         VARCHAR2(5);
v_florist_blocked         CHAR(1);
v_product_codification_id VARCHAR2(5);
v_sequences               VARCHAR2(1000);


BEGIN

  FOR rec IN cur LOOP
    BEGIN
      ftd_apps.florist_query_pkg.get_routable_florists(rec.zip_code, rec.product_id,
                                                       rec.delivery_date, rec.delivery_date_range_end,
                                                       rec.source_code, rec.order_detail_id,
                                                       rec.city, rec.state, rec.product_amount,
                                                       'N', v_cursor);

      FETCH v_cursor INTO v_florist_id,
                          v_score,
                          v_weight,
                          v_florist_name,
                          v_phone_number,
                          v_mercury_flag,
                          v_super_florist_flag,
                          v_sunday_delivery_flag,
                          v_cutoff_time,
                          v_status,
                          v_codification_id,
                          v_florist_blocked,
                          v_product_codification_id,
                          v_sequences;

      IF v_cursor%FOUND THEN
         INSERT INTO rep_tab
              (col1)
             VALUES
              (rec.order_detail_id);
      END IF;

      CLOSE v_cursor;

    END;
  END LOOP;

  OPEN out_cursor FOR
    SELECT DISTINCT col1 "order_detail_id"
      FROM rep_tab;


END GET_ROUTABLE_QUEUED_ORDERS;



PROCEDURE GET_PARTNER_PARTIAL_REFUNDS
(
IN_PARTNER_NAME                 IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_REFUND_DATE                  IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for order and partner information
        for orders with partial refunds

Input:
        partner_name                    varchar2
        refund_date                     date

Output:
        cursor containing order, source, partner info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_cur FOR
        SELECT
                o.company_id,
                o.master_order_number,
                od.external_order_number,
                od.source_code,
                od.delivery_date,
                od.order_detail_id,
                r.refund_date,
                r.refund_amount,
                m.first_name,
                m.last_name,
                m.membership_type,
                m.membership_number,
                c.address_1,
                (select e.email_address from email e where e.email_id = o.email_id) email_address,
                pm.product_name,
                sm.promotion_code,
                sm.bonus_promotion_code,
                pp.program_name,
                pp.partner_name,
                pp.program_type,
                pr.calculation_basis,
                pr.points,
                pr.reward_type,
                pr.reward_name,
                pr.bonus_calculation_basis,
                pr.bonus_points,
                pr.bonus_separate_data,
                pr.maximum_points
        FROM    orders o
        JOIN    order_details od
        ON      o.order_guid = od.order_guid
        AND     od.delivery_date is not null
        AND     order_query_pkg.is_order_cancelled(od.order_detail_id) = 'N'
        JOIN
        (       SELECT  r1.order_detail_id,
                        max (r1.refund_date) refund_date,
                        sum (r1.refund_product_amount + r1.refund_addon_amount +
                                r1.refund_service_fee + r1.refund_shipping_fee +
                                r1.refund_service_fee_tax + r1.refund_shipping_tax + r1.refund_tax -
                                r1.refund_discount_amount) refund_amount
                FROM    refund r1
                WHERE   r1.refund_date >= TRUNC (in_refund_date)
                AND     r1.refund_date < TRUNC (in_refund_date + 1)
                AND     r1.refund_disp_code not like 'A%'
                GROUP BY r1.order_detail_id
        )       r
        ON      od.order_detail_id = r.order_detail_id
        JOIN    memberships m
        ON      od.membership_id = m.membership_id
        JOIN    customer c
        ON      o.customer_id = c.customer_id
        JOIN    ftd_apps.product_master pm
        ON      od.product_id = pm.product_id
        JOIN    ftd_apps.source_master sm
        ON      od.source_code = sm.source_code
        JOIN    ftd_apps.source_program_ref spr
        ON      sm.source_code = spr.source_code
        AND     spr.start_date = ftd_apps.source_query_pkg.get_src_prg_ref_max_start_date(spr.source_code, in_refund_date)
        JOIN    ftd_apps.partner_program pp
        ON      spr.program_name = pp.program_name
        JOIN    ftd_apps.program_reward pr
        ON      pp.program_name = pr.program_name
        WHERE   pp.partner_name = in_partner_name;


END GET_PARTNER_PARTIAL_REFUNDS;


PROCEDURE GET_ORDER_DETAILS_II
(
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the records from order_details
         for the order_detail_id passed in.

Input:
        order_detail_id         NUMBER

Output:
        cursor containing order_details info

-----------------------------------------------------------------------------*/



BEGIN

  OPEN out_cursor FOR
    SELECT order_detail_id,
           order_guid,
           external_order_number,
           hp_order_number,
           source_code,
           delivery_date,
           recipient_id,
           product_id,
           quantity,
           color_1,
           color_2,
           substitution_indicator,
           same_day_gift,
           occasion,
           card_message,
           card_signature,
           special_instructions,
           release_info_indicator,
           florist_id,
           ship_method,
           ship_date,
           order_disp_code,
           second_choice_product,
           zip_queue_count,
           scrubbed_on,
           scrubbed_by,
           delivery_date_range_end,
           ariba_unspsc_code,
           ariba_po_number,
           ariba_ams_project_code,
           ariba_cost_center,
           size_indicator,
           miles_points,
           subcode,
           eod_delivery_indicator,
           eod_delivery_date,
           miles_points_post_date,
           membership_id,
           reject_retry_count,
           op_status,
           vendor_id,
     actual_product_amount,
     actual_add_on_amount,
           actual_shipping_fee,
           actual_add_on_discount_amount,
		   legacy_id
      FROM order_details
     WHERE order_detail_id = in_order_detail_id;


END GET_ORDER_DETAILS_II;


PROCEDURE GET_ALLANT_ORDER_SUMMARY
(
IN_START_DATE           IN DATE,
IN_END_DATE             IN DATE,
OUT_CURSOR              OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all orders with order date between the
        given dates

Input:
        start_date              date
        end_date                date

Output:
        cursor containing order_details info

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cursor FOR
        SELECT  od.external_order_number order_number,
                o.customer_id,
                od.recipient_id,
                c.first_name,
                c.last_name,
                c.address_1,
                c.address_2,
                c.city,
                c.state,
                c.zip_code,
                cp.phone_number,
                o.order_date,
                od.source_code,
                sm.source_type,
                od.product_id,
                pm.standard_price,
                (       -- get the original order total
                        select  ob.product_amount
                                + ob.add_on_amount
                                + ob.service_fee
                                + ob.shipping_fee
                                - ob.discount_amount
                                + ob.shipping_tax
                                + ob.tax
                        from    order_bills ob
                        where   ob.order_detail_id = od.order_detail_id
                        and     ob.additional_bill_indicator = 'N'
                ) order_total
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    customer c
        ON      o.customer_id = c.customer_id
        JOIN    ftd_apps.product_master pm
        ON      od.product_id = pm.product_id
        JOIN    ftd_apps.source_master sm
        ON      od.source_code = sm.source_code
        LEFT OUTER JOIN customer_phones cp
        ON      c.customer_id = cp.customer_id
        AND     cp.phone_type = 'Day'
        WHERE   o.order_date BETWEEN in_start_date AND in_end_date
        AND     od.order_disp_code not in ('In-Scrub', 'Pending', 'Removed')
        AND     od.recipient_id is not null
        ORDER BY od.external_order_number;

END GET_ALLANT_ORDER_SUMMARY;


FUNCTION GET_ORDER_FROM_TRACKING
(
IN_TRACKING_NUMBER              IN ORDER_TRACKING.TRACKING_NUMBER%TYPE
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the order associated to the
        tracking number

Input:
        tracking number         varchar2

Output:
        order detail id

-----------------------------------------------------------------------------*/

CURSOR od_cur IS
        SELECT  order_detail_id
        FROM    order_tracking
        WHERE   tracking_number = in_tracking_number;

v_order_detail_id               order_details.order_detail_id%type;

BEGIN

OPEN od_cur;
FETCH od_cur INTO v_order_detail_id;
CLOSE od_cur;

RETURN v_order_detail_id;

END GET_ORDER_FROM_TRACKING;

PROCEDURE GET_ORDER_CO_BRANDS
(
IN_ORDER_DETAIL_ID      IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning co brand information for the
        given order detail ID.

Input:
        order_detail_id                 varchar2

Output:
        cursor result set containing
        info_name                       varchar2
        info_data                       varchar2
        source_code                     varchar2
        info_display                    varchar2
-----------------------------------------------------------------------------*/
BEGIN
OPEN OUT_CUR FOR
        SELECT CB.INFO_NAME,
                CB.INFO_DATA,
                OD.SOURCE_CODE,
                BI.INFO_DISPLAY
        FROM    CO_BRAND CB,
                ORDER_DETAILS OD,
                FTD_APPS.BILLING_INFO BI
        WHERE   CB.ORDER_GUID = OD.ORDER_GUID
        AND     OD.SOURCE_CODE = BI.SOURCE_CODE_ID
        AND     CB.INFO_NAME = BI.INFO_DESCRIPTION
        AND     OD.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;
END GET_ORDER_CO_BRANDS;

FUNCTION IS_SHIP_METHOD_AVAILABLE
(
IN_PRODUCT_ID           IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_SHIP_METHOD          IN ORDER_DETAILS.SHIP_METHOD%TYPE
)
RETURN VARCHAR2

IS
/*-----------------------------------------------------------------------------
Description:
        This function determines if the passed product is listed as having
        the passed ship method available.

Input:
        product id - product it to check
        ship method - ship method to check

Output:
        'Y' if ok else 'N' if no data found.  Throw all other errors back to
        the application

-----------------------------------------------------------------------------*/
v_flag CHAR (1);

BEGIN

  BEGIN
       SELECT 'Y' INTO v_flag
              FROM ftd_apps.product_ship_methods
              WHERE PRODUCT_ID=IN_PRODUCT_ID AND SHIP_METHOD_ID=IN_SHIP_METHOD;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    v_flag := 'N';
  END;

  RETURN v_flag;
END IS_SHIP_METHOD_AVAILABLE;


FUNCTION GET_ORIGINAL_ORDER_BILL_ID
(
IN_ORDER_DETAIL_ID      IN ORDER_BILLS.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER

IS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order bill id for the
        given order detail ID.

Input:
        order_detail_id                 varchar2

Output:
        order_bill_id     number
-----------------------------------------------------------------------------*/

v_order_bill_id   order_bills.order_bill_id%type;

BEGIN
  BEGIN

    SELECT order_bill_id INTO v_order_bill_id
    FROM   order_bills
    WHERE  order_detail_id = in_order_detail_id
    AND    additional_bill_indicator = 'N';

  -- no data found or found more than one row
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
          v_order_bill_id := -1;
    WHEN OTHERS THEN
      v_order_bill_id := -1;
  END;

  RETURN v_order_bill_id;

END GET_ORIGINAL_ORDER_BILL_ID;


FUNCTION GET_ORIGINAL_ACCTG_TRANS_ID
(
IN_ORDER_DETAIL_ID              IN ACCOUNTING_TRANSACTIONS.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER

IS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order bill id for the
        given order detail ID.

Input:
        order_detail_id                 varchar2

Output:
        accounting_transaction_id number
-----------------------------------------------------------------------------*/

v_acctg_trans_id    accounting_transactions.accounting_transaction_id%type;

BEGIN
  BEGIN
    SELECT accounting_transaction_id INTO v_acctg_trans_id
    FROM   clean.accounting_transactions
    WHERE  order_detail_id = in_order_detail_id
    AND    transaction_type = 'Order';

  -- no data found or found more than one row
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
          v_acctg_trans_id := -1;
    WHEN OTHERS THEN
      v_acctg_trans_id := -1;
  END;

  RETURN v_acctg_trans_id;

END GET_ORIGINAL_ACCTG_TRANS_ID;

FUNCTION IS_ORDER_DTL_DISPATCHED_FULL
(
 IN_ORDER_DETAIL_ID             IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2

IS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not an
        order is dispatched FULL

Input:
        order_detail_id       number

Output:
        Y/N
-----------------------------------------------------------------------------*/
  CURSOR cur_full_dispatch IS
    select  'Y'
    from    clean.order_details
    where   order_detail_id = in_order_detail_id
    and    order_disp_code IN ('Validated', 'Processed', 'Shipped', 'Printed');

  v_result        char(1) := 'N';

BEGIN

  OPEN cur_full_dispatch;
  FETCH cur_full_dispatch INTO v_result;
  CLOSE cur_full_dispatch;

  RETURN v_result;

END IS_ORDER_DTL_DISPATCHED_FULL;

PROCEDURE     GET_ORIGIN_BY_GUID
(
IN_ORDER_GUID       IN CLEAN.ORDERS.ORIGIN_ID%TYPE,
OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the origin_id from clean.orders based
        on the passed in guid.

Input:
        IN_ORDER_GUID

Output:
        cursor result set containing
        ORIGIN_ID

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT  ORIGIN_ID
          FROM CLEAN.ORDERS
    WHERE ORDER_GUID = IN_ORDER_GUID;
END GET_ORIGIN_BY_GUID;

FUNCTION GET_REFUND_BY_ORDER_DETAIL_ID
(
  IN_ORDER_DETAIL_ID      CLEAN.REFUND.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER
AS

/*--------------------------------------------------------------------------
---
Description:
        This procedure retrieves the total refunds from clean.refund table for a
        given order_detail_id that is passed in.

Input:
        IN_ORDER_DETAIL_ID

Output:

        Total Refunds per order

----------------------------------------------------------------------------
-*/

  v_total_refund    NUMBER(10) := 0;

BEGIN

   BEGIN
    SELECT NVL(SUM(r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax), 0)
      INTO v_total_refund
      FROM clean.refund r
     WHERE r.order_detail_id = in_order_detail_id;
  EXCEPTION
    WHEN OTHERS THEN
      v_total_refund := 0;
  END;

   RETURN v_total_refund;

END GET_REFUND_BY_ORDER_DETAIL_ID;

FUNCTION GET_EXPECTED_WHOLESALE_AMT
(
  IN_ORDER_DETAIL_ID      CLEAN.REFUND.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER
AS

/*--------------------------------------------------------------------------
---
Description:
        This procedure retrieves the total expected wholesale amount for a
        given order_detail_id and partner_id that is passed in.

Input:
        IN_PARTNER_ID
        IN_ORDER_DETAIL_ID

Output:

        Total expected wholesale amount

----------------------------------------------------------------------------
-*/

  v_expected_amt    ORDER_BILLS.WHOLESALE_AMOUNT%TYPE := 0;

BEGIN

  BEGIN
    SELECT NVL(SUM(ob.wholesale_amount), 0)
      INTO v_expected_amt
      FROM clean.order_bills ob
     WHERE ob.order_detail_id = in_order_detail_id;
  EXCEPTION
    WHEN OTHERS THEN
      v_expected_amt := 0;
  END;

  RETURN v_expected_amt;

END GET_EXPECTED_WHOLESALE_AMT;

FUNCTION GET_OUTSTANDING_BALANCE_DUE
(
  IN_ORDER_DETAIL_ID      CLEAN.REFUND.ORDER_DETAIL_ID%TYPE
)
RETURN NUMBER
AS

/*--------------------------------------------------------------------------
---
Description:
        This procedure retrieves the total outstanding balance due for a
        given order_detail_id that is passed in.

Input:
        IN_ORDER_DETAIL_ID

Output:

        Total outstanding balance due

----------------------------------------------------------------------------
-*/

  v_balance_due   NUMBER(10) := 0;
  v_cash_rcvd     NUMBER(10) := 0;
  v_expected_amt    NUMBER(10) := 0;
  v_tot_refunds   NUMBER(10) := 0;

BEGIN

  BEGIN
    SELECT NVL(SUM(rd.cash_received_amt + rd.write_off_amt), 0)
      INTO v_cash_rcvd
      FROM clean.remittance_details rd
     WHERE rd.order_detail_id = in_order_detail_id;
  EXCEPTION
    WHEN OTHERS THEN
      v_cash_rcvd := 0;
  END;

  v_expected_amt := clean.order_query_pkg.get_expected_wholesale_amt(in_order_detail_id);

  v_tot_refunds := clean.order_query_pkg.get_refund_by_order_detail_id(in_order_detail_id);

  v_balance_due := v_expected_amt - (v_tot_refunds + v_cash_rcvd);

  RETURN v_balance_due;

END GET_OUTSTANDING_BALANCE_DUE;

PROCEDURE  GET_RECALCULATED_TOTALS
(
IN_ORIGIN_ID            IN CLEAN.ORDERS.ORIGIN_ID%TYPE,
IN_PARTNER_ID         IN CLEAN.REMITTANCE.PARTNER_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the recalculated totals for the remittances applied
        to the orders for a origin_id and partner id sent in that have
        been billed and have an outstanding wholesale amount balance.

Input:
        IN_ORIGIN_ID      - varchar2 - origin id
        IN_PARTNER_ID     - varchar2 - partner id

Output:
        cursor result set containing
        REMITTANCE TOTALS info

-----------------------------------------------------------------------------*/

  CURSOR cur_exists IS
  SELECT CASE (SELECT COUNT(1) FROM clean.remittance_details_update where partner_id = in_partner_id) WHEN 0 THEN 'N' ELSE 'Y' END exists_temp,
       CASE (SELECT COUNT(1) FROM clean.remittance_details where partner_id = in_partner_id) WHEN 0 THEN 'N' ELSE 'Y' END exists_real
    FROM DUAL;

  CURSOR cur_rem_amt IS
  SELECT NVL(remittance_amt, 0)
    FROM clean.remittance_update
   WHERE partner_id = in_partner_id;

  v_exists_temp   VARCHAR2(1) := 'N';
  v_exists_real   VARCHAR2(1) := 'N';
  v_remittance_amt  NUMBER(22,2) := 0;

BEGIN

  --get the remittance amt
  OPEN cur_rem_amt;
  FETCH cur_rem_amt INTO v_remittance_amt;
  CLOSE cur_rem_amt;

  OPEN cur_exists;
  FETCH cur_exists INTO v_exists_temp, v_exists_real;
  CLOSE cur_exists;

  -- if orders exist in TEMP table
  IF (v_exists_temp = 'Y'  OR
     (in_partner_id IS NULL AND in_origin_id IS NULL ))THEN

    OPEN OUT_CUR FOR
    SELECT NVL(SUM(rdu.pdb_price_amt), 0) tot_expected_wholesale_amt,
         NVL(SUM(rdu.remittance_applied_amt), 0) tot_prior_rem_applied_amt,
         NVL(SUM(rdu.outstanding_balance_due_amt), 0) tot_outstanding_bal_due_amt,
         NVL(SUM(rdu.cash_received_amt), 0) tot_cash_received_amt,
         NVL(SUM(rdu.write_off_amt), 0) tot_write_off_amt,
         NVL(SUM(rdu.remaining_balance_due_amt), 0) tot_remaining_bal_due_amt,
         (v_remittance_amt - NVL(SUM(rdu.cash_received_amt), 0)) tot_difference
      FROM clean.remittance_details_update rdu
     WHERE rdu.partner_id = in_partner_id;

  ELSE -- does not exists in TEMP tables

      -- if orders exist in REAL table
    IF v_exists_real = 'Y' THEN

      OPEN OUT_CUR FOR
      SELECT NVL(SUM(clean.order_query_pkg.get_expected_wholesale_amt(rd.order_detail_id)), 0) tot_expected_wholesale_amt,
           NVL(SUM(rd.cash_received_amt), 0) tot_prior_rem_applied_amt,
           NVL(SUM(clean.order_query_pkg.get_outstanding_balance_due(rd.order_detail_id)), 0) tot_outstanding_bal_due_amt,
           NVL(SUM(rd.cash_received_amt), 0) tot_cash_received_amt,
           NVL(SUM(rd.write_off_amt), 0) tot_write_off_amt,
           NVL(SUM(rd.remaining_balance_due_amt), 0) tot_remaining_bal_due_amt,
           (v_remittance_amt - NVL(SUM(rd.cash_received_amt), 0)) tot_difference
        FROM clean.remittance_details rd,
           clean.order_details od,
           clean.order_bills ob
       WHERE rd.partner_id = in_partner_id
        AND (clean.order_query_pkg.get_outstanding_balance_due(od.order_detail_id) > 0
         OR (ob.bill_date IS NOT NULL AND ob.bill_status in ('Billed', 'Unbilled', 'Reconciled')))
        AND ob.additional_bill_indicator = 'N'
        AND ob.order_detail_id = od.order_detail_id
        AND ob.order_detail_id = rd.order_detail_id;

    ELSE -- does not exists in the REAL tables

      OPEN OUT_CUR FOR
      SELECT NVL(SUM(clean.order_query_pkg.get_expected_wholesale_amt(od.order_detail_id)), 0) tot_expected_wholesale_amt,
           0 tot_prior_rem_applied_amt,
           NVL(SUM(clean.order_query_pkg.get_outstanding_balance_due(od.order_detail_id)), 0) tot_outstanding_bal_due_amt,
           0 tot_cash_received_amt,
           0 tot_write_off_amt,
           NVL(SUM(clean.order_query_pkg.get_expected_wholesale_amt(od.order_detail_id)), 0) tot_remaining_bal_due_amt,
            (v_remittance_amt - 0) tot_difference
        FROM clean.order_details od,
           clean.order_bills ob,
           clean.orders o
       WHERE o.origin_id = in_origin_id
        AND ob.bill_date IS NOT NULL
        AND ob.bill_status in ('Billed', 'Unbilled', 'Reconciled')
        AND ob.additional_bill_indicator = 'N'
        AND ob.order_detail_id = od.order_detail_id
        AND o.order_guid = od.order_guid;

    END IF;
  END IF;

END GET_RECALCULATED_TOTALS;

PROCEDURE  GET_REMITTANCE_INFO
(
IN_PARTNER_ID         IN CLEAN.REMITTANCE.PARTNER_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves remittance info for the partner_id sent in that have
        been billed and have an outstanding wholesale amount balance.

Input:
        IN_PARTNER_ID     - varchar2 - partner id

Output:
        cursor result set containing
        REMITTANCE info

-----------------------------------------------------------------------------*/

BEGIN

   -- this cursor should NOT return anything since partner id is NULL
  OPEN OUT_CUR FOR
  SELECT to_char(ru.remittance_date, 'MM/dd/yyyy') remittance_date,
       NVL(ru.remittance_amt, 0) remittance_amt,
       ru.remittance_number remittance_number
    FROM clean.remittance_update ru
   WHERE ru.partner_id = in_partner_id;

END GET_REMITTANCE_INFO;

FUNCTION REMIT_NUM_EXISTS
(
IN_REMITTANCE_NUMBER                   IN CLEAN.REMITTANCE.REMITTANCE_NUMBER%TYPE
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a 'Y' or 'N'.  It will return
        'Y' if the remittance number has been used before, or an 'N' if the
        remittance number has never been used before

Input:
        IN_REMITTANCE_NUMBER                 clean.remittance.remittance_number

Output:
        Y/N

-----------------------------------------------------------------------------*/

v_remit_exist_ind     char (1);

CURSOR  check_cur IS
        select  decode (count (1), 0, 'N', 'Y')
        from    clean.remittance
        where   remittance_number = IN_REMITTANCE_NUMBER;

BEGIN

OPEN check_cur;
FETCH check_cur INTO v_remit_exist_ind;
CLOSE check_cur;

RETURN v_remit_exist_ind;

END REMIT_NUM_EXISTS;


FUNCTION IS_CART_OUT_OF_SCRUB
(
IN_ORDER_DETAIL_ID                    IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a 'Y' or 'N'.  It will return
        'Y' if no other items in cart is in scrub. 'N' otherwise.

Input:
        IN_ORDER_DETAIL_ID                 clean.order_details.order_detail_id

Output:
        Y/N

-----------------------------------------------------------------------------*/
v_cart_ind     char (1);

CURSOR  check_cur IS
        select  decode (count (1), 0, 'Y', 'N')
        from    (select od.order_detail_id
           from    clean.order_details od
     where   od.order_guid = (select order_guid
            from clean.order_details od2
            where od2.order_detail_id = IN_ORDER_DETAIL_ID
           )
     and  od.order_detail_id <> IN_ORDER_DETAIL_ID
     and  od.order_disp_code in ('Validated','Held')
     UNION
     select sod.order_detail_id
     from scrub.order_details sod
     where  sod.order_guid = (select order_guid
            from clean.order_details od2
            where od2.order_detail_id = IN_ORDER_DETAIL_ID
           )
     and  sod.order_detail_id <> IN_ORDER_DETAIL_ID
     and  sod.status in ('2001','2002','2003','2005','2007')
    )
        ;

BEGIN

  OPEN check_cur;
  FETCH check_cur INTO v_cart_ind;
  CLOSE check_cur;

RETURN v_cart_ind;
END IS_CART_OUT_OF_SCRUB;


FUNCTION GET_ORDER_DETAIL_EXT_DATA
(
IN_ORDER_DETAIL_ID                    IN CLEAN.ORDER_DETAIL_EXTENSIONS.ORDER_DETAIL_ID%TYPE,
IN_INFO_NAME        IN CLEAN.ORDER_DETAIL_EXTENSIONS.INFO_NAME%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning INFO_DATA for the given order_detail_id
        and info_name. Returns null if no such record.

Input:
        IN_ORDER_DETAIL_ID              clean.order_detail_extensions.order_detail_id
        IN_INFO_NAME      clean.order_detail_extensions.info_name

Output:
        info_data varchar2

-----------------------------------------------------------------------------*/
v_info_data     varchar2(50);

BEGIN
  BEGIN

  SELECT  info_data
  INTO  v_info_data
  FROM  clean.order_detail_extensions
  WHERE   order_detail_id = IN_ORDER_DETAIL_ID
  AND   info_name = IN_INFO_NAME;

  EXCEPTION
    WHEN OTHERS THEN
      v_info_data := null;
  END;

RETURN v_info_data;

END GET_ORDER_DETAIL_EXT_DATA;

PROCEDURE  GET_MILES_POINTS_PMT_INFO
(
IN_ORDER_DETAIL_ID    IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                 OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves miles points payment infomation for the payment.

Input:
        IN_PAYMENT_ID     - varchar2 - payment id

Output:
        cursor result set containing
        payment info

-----------------------------------------------------------------------------*/
v_removed_item_ct number := 0;
v_total_amt clean.payments.credit_amount%TYPE;
v_total_miles_amt clean.payments.miles_points_credit_amt%TYPE;
v_order_guid clean.order_details.order_guid%TYPE;
v_payment_id clean.payments.payment_id%TYPE;

BEGIN
  select order_guid
  into v_order_guid
  from clean.order_details
  where order_detail_id = in_order_detail_id
  ;

  select count(od.order_detail_id)
  into v_removed_item_ct
  from clean.order_details od
  where od.order_disp_code = 'Removed'
  and od.order_guid = v_order_guid
  ;

  -- find the payment record id
  select payment_id
  into v_payment_id
  from clean.payments p
  join clean.order_details od
  on od.order_guid = p.order_guid
  WHERE od.order_detail_id = IN_ORDER_DETAIL_ID
  AND     p.payment_indicator = 'P'
  AND p.additional_bill_id IS NULL
  AND p.payment_type in (select payment_method_id
         from ftd_apps.payment_method_miles_points)
  ;

  if v_removed_item_ct > 0 then
    -- recalculate removed order payment
    select sum (ob.product_amount + ob.add_on_amount + ob.service_fee
                         + ob.shipping_fee - ob.discount_amount + ob.shipping_tax + ob.tax),
                       sum (ob.miles_points_amt)
                into v_total_amt, v_total_miles_amt
                from clean.order_bills ob
                where ob.order_detail_id in
                  (select order_detail_id
                   from clean.order_details od
                   where od.order_guid = v_order_guid)
                ;

          update clean.payments
          set credit_amount = v_total_amt,
              miles_points_credit_amt = v_total_miles_amt,
              updated_on = sysdate,
              updated_by = 'GET_MILES_POINTS_PMT_INFO'
          where payment_id = v_payment_id
          ;
  end if;


  OPEN OUT_CUR FOR
  SELECT  p.payment_id,
    p.payment_type,
    p.auth_number,
    p.miles_points_credit_amt miles_amt,
    global.encryption.decrypt_it(aa.ap_account_txt,aa.key_name) ap_account_txt
  FROM  clean.order_details od
  JOIN  clean.payments p
  ON  od.order_guid = p.order_guid
  JOIN  clean.ap_accounts aa
  ON  p.ap_account_id = aa.ap_account_id
  WHERE p.payment_id = v_payment_id
  ;

END GET_MILES_POINTS_PMT_INFO;

FUNCTION ALLOW_MILES_POINTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2

AS

/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning if the given fully refunded order
        will allow miles/points to be posted.
        Refund Disp Codes that will not allow miles/points to post:
          All A-Type Refunds
          B40

Input:
        order_detail_id                 number

Output:
        Y/N if the fully refunded order can have miles/points posted


-----------------------------------------------------------------------------*/
v_count     number(4);
v_return    char(1);
BEGIN
  SELECT count(*)
          INTO v_count
        FROM  clean.refund r
        WHERE r.order_detail_id = IN_ORDER_DETAIL_ID
        AND (REFUND_DISP_CODE LIKE 'A%' OR REFUND_DISP_CODE = 'B40');
     IF v_count > 0 THEN
        v_return := 'N';
     ELSE
        v_return := 'Y';
     END IF;

RETURN v_return;

END ALLOW_MILES_POINTS;

PROCEDURE GET_REFUND_BY_SOURCE
(
IN_SOURCE_CODE                 IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_START_DATE                  IN DATE,
IN_END_DATE          IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving full refunds for the given
        comma delimited source codes between start date and end date.

Input:
        source_code                    varchar2
        start_date                     date
        end_date           date

Output:
        cursor containing order, source, partner info

-----------------------------------------------------------------------------*/
v_sql VARCHAR2(4000);

BEGIN

        v_sql := 'SELECT
                r1.product_id,
                sum(r.refund_product_amount) refund_product_amount,
                to_char(r1.created_on, ''YYYY-MM-DD HH24:MI:SS'') created_on,
                r1.external_order_number,
                r1.order_detail_id,
                r1.source_code,
                r1.delivery_date
        FROM    refund r
        JOIN  (SELECT pm.novator_id product_id, od.external_order_number, r2.order_detail_id, max(r2.created_on) created_on, od.source_code, od.delivery_date
           FROM refund r2
           JOIN order_details od
           ON r2.order_detail_id = od.order_detail_id
           JOIN ftd_apps.product_master pm
           ON pm.product_id = od.product_id
           WHERE od.source_code in ('||ftd_apps.misc_pkg.multi_value(in_source_code)||')
           AND r2.created_on >= TRUNC (to_date(''' || to_char(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY''))
           AND r2.created_on < TRUNC (to_date(''' || to_char(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') + 1)
           AND REFUND_PKG.IS_ORDER_FULLY_REFUNDED(od.order_detail_id) = ''Y''
     AND NOT EXISTS (SELECT 1
         FROM order_reward_posting orp
         WHERE orp.order_detail_id = od.order_detail_id
        )
           GROUP BY pm.novator_id, od.external_order_number, r2.order_detail_id, od.source_code, od.delivery_date
          ) r1
        ON  r1.order_detail_id = r.order_detail_id
        WHERE r.created_on >= TRUNC (to_date(''' || to_char(IN_START_DATE,'MMDDYYYY') || ''',''MMDDYYYY''))
        AND r.created_on < TRUNC (to_date(''' || to_char(IN_END_DATE,'MMDDYYYY') || ''',''MMDDYYYY'') + 1)
        GROUP BY r1.product_id,r1.created_on, r1.external_order_number, r1.order_detail_id, r1.source_code, r1.delivery_date
        ORDER BY r1.created_on DESC'
        ;

        OPEN out_cur FOR v_sql;


END GET_REFUND_BY_SOURCE;

FUNCTION GET_DCON_STATUS
(
IN_ORDER_DETAIL_ID                    IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN ORDER_DETAILS.DELIVERY_CONFIRMATION_STATUS%TYPE
AS
/*-----------------------------------------------------------------------------
Description:
        This function will return the dcon status of a specific order detail

Input:
        order_detail_id                 number

Output:
       delivery confirmation status VARCHAR2


-----------------------------------------------------------------------------*/
v_dcon_status    ORDER_DETAILS.DELIVERY_CONFIRMATION_STATUS%TYPE;
BEGIN
  SELECT cod.DELIVERY_CONFIRMATION_STATUS
        INTO v_dcon_status
        FROM  CLEAN.ORDER_DETAILS cod
        WHERE cod.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;

        RETURN v_dcon_status;

EXCEPTION WHEN NO_DATA_FOUND THEN
        v_dcon_status := '';
        RETURN v_dcon_status;

END GET_DCON_STATUS;

PROCEDURE GET_ORDER_BY_SOURCE_TYPE
(
IN_SOURCE_TYPE                 IN FTD_APPS.SOURCE_MASTER.SOURCE_TYPE%TYPE,
IN_START_DATE                  IN DATE,
IN_END_DATE          IN DATE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving shopping carts for the given
        source type between start date and end date.

Input:
        source_type                    varchar2
        start_date                     date
        end_date           date

Output:
        cursor containing order, source, partner info

-----------------------------------------------------------------------------*/

BEGIN
  OPEN OUT_CUR FOR
  select
       min(od.order_detail_id) order_detail_id,
       IN_SOURCE_TYPE source_type,
       o.source_code,
       o.company_id,
                   fo.origin_type,
       o.master_order_number,
       count(distinct od.order_detail_id) item_in_cart,
       sum(nvl(ob.product_amount,0)) product_amount,
       sum(nvl(ob.add_on_amount,0)) add_on_amount,
       sum(nvl(ob.shipping_fee,0)) shipping_fee,
       sum(nvl(ob.service_fee,0)) service_fee,
       sum(nvl(ob.discount_amount,0)) discount_amount,
       sum(nvl(ob.tax,0)) tax,
       (sum(nvl(ob.product_amount,0)) + sum(nvl(ob.add_on_amount,0)) + sum(nvl(ob.shipping_fee,0)) + sum(nvl(ob.service_fee,0)) +  sum(nvl(ob.tax,0)) - sum(nvl(ob.discount_amount,0))) total_cart_value,
       (sum(nvl(ob.product_amount,0)) + sum(nvl(ob.add_on_amount,0)) + sum(nvl(ob.shipping_fee,0)) + sum(nvl(ob.service_fee,0)) +  sum(nvl(ob.tax,0)) - sum(nvl(ob.discount_amount,0))) / count(distinct od.order_detail_id) aov,
       c.first_name,
       c.last_name,
       o.order_date,
                   cc.cc_number_masked,
       p.auth_date transaction_date,
       p.auth_number cc_auth_code,
       nvl(p.credit_amount,0) transaction_amt,
       'Downers Grove' merchant_city,
       sum(nvl(ob.add_on_discount_amount,0)) add_on_discount_amount
  from clean.orders o
  join clean.order_details od
  on od.order_guid = o.order_guid
  join clean.order_bills ob
  on od.order_detail_id=ob.order_detail_id
   join clean.payments p
   on p.order_guid = od.order_guid
  and p.payment_indicator = 'P'
  and ((p.additional_bill_id is null and ob.additional_bill_indicator='N') or ( ob.additional_bill_indicator='Y' and p.additional_bill_id = ob.order_bill_id))
  join clean.customer c
  on c.customer_id=o.customer_id
        join ftd_apps.origins fo
        on fo.origin_id = o.origin_id
        join clean.credit_cards cc
        on cc.cc_id = p.cc_id
        join ftd_apps.payment_methods pm
        on pm.payment_method_id = p.payment_type
  join ftd_apps.source_master sm
  on sm.source_code = o.source_code
  where 1=1
  and ob.bill_date between IN_START_DATE and IN_END_DATE
  and od.miles_points_post_date IS NULL
        and pm.payment_type = 'C'
  and sm.source_type= IN_SOURCE_TYPE
  and p.additional_bill_id is null
  and not exists (select 1 from clean.refund r
      where r.order_detail_id=od.order_detail_id
      and r.refund_disp_code like 'A%'
      )
  group by
        IN_SOURCE_TYPE,
        o.source_code,
        o.company_id,
                    fo.origin_type,
        o.master_order_number,
        c.first_name,
        c.last_name,
        o.order_date,
                    cc.cc_number_masked,
        p.auth_date,
        p.auth_number,
        p.credit_amount,
        'Downers Grove'
        order by transaction_date, cc_number_masked
  ;

END GET_ORDER_BY_SOURCE_TYPE;

PROCEDURE GET_ORDER_BY_EXTENSION
(
IN_EXTENSION_NAME              IN FTD_APPS.SOURCE_MASTER.SOURCE_TYPE%TYPE,
IN_PARTNER_NAME                IN FTD_APPS.PARTNER_PROGRAM.PARTNER_NAME%TYPE,
IN_START_DATE          IN DATE,
IN_END_DATE          IN DATE,
OUT_CUR                        OUT TYPES.REF_CURSOR
)AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for retrieving order information for the given
        source type between start date and end date.

Input:
        source_type                    varchar2
        start_date                     date
        end_date           date

Output:
        cursor containing order, source, partner info

-----------------------------------------------------------------------------*/
v_guaranteeDays NUMBER;

BEGIN

 SELECT TO_NUMBER(frp.misc_pkg.get_global_parm_value ('ins_partner_reward_mypoints', 'GUARANTEE_DAYS'))
   INTO v_guaranteeDays
   FROM DUAL;


    OPEN OUT_CUR FOR
    select
           od.order_detail_id,
           od.external_order_number,
           od.source_code,
           o.order_date,
           sum(nvl(ob.product_amount,0)) product_amount,
           sum(nvl(ob.add_on_amount,0)) add_on_amount,
           oe.info_value

    from    clean.orders o
    join    clean.order_details od
    on      od.order_guid = o.order_guid
    join    clean.order_bills ob
    on      ob.order_detail_id = od.order_detail_id
    join    clean.order_extensions oe
    on      oe.order_guid = o.order_guid
    where od.delivery_date between IN_START_DATE - v_guaranteeDays  and IN_END_DATE - v_guaranteeDays
    and
        (
           IN_PARTNER_NAME is null
           or
           od.source_code in
           (
             select spr.source_code
             from ftd_apps.source_program_ref spr, ftd_apps.partner_program pp
               where
                 pp.partner_name = IN_PARTNER_NAME
                 and spr.program_name = pp.program_name
           )
        )

    and     clean.REFUND_PKG.IS_ORDER_FULLY_REFUNDED(od.order_detail_id) = 'N'
    and     oe.info_name = IN_EXTENSION_NAME
    and     ob.additional_bill_indicator = 'N'
    and not exists
           (
             select 1
             from clean.order_reward_posting orp
             where orp.order_detail_id = od.order_detail_id
            )

    group by od.order_detail_id, od.external_order_number, od.delivery_date, od.source_code, o.order_date, oe.info_value
    order by od.delivery_date
    ;

END GET_ORDER_BY_EXTENSION;

/******************************************************************************
 GET_PARTNER_WARNING_MSG

Description:  Return order access restriction message associated to
              preferred partner

******************************************************************************/
FUNCTION GET_PARTNER_WARNING_MSG
(
IN_SOURCE_CODE       IN VARCHAR2
)
RETURN VARCHAR2
AS

-- variables
v_PREFERRED_PARTNER     varchar2 (100);
v_PREFERRED_WARNING_MSG     varchar2 (10000);

BEGIN

  SELECT DISTINCT pm.PREFERRED_PROCESSING_RESOURCE INTO v_PREFERRED_PARTNER
  FROM ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm
  WHERE spr.source_code = IN_SOURCE_CODE
  AND pp.program_name = spr.program_name
  AND pm.partner_name = pp.partner_name;

  FTD_APPS.CONTENT_QUERY_PKG.GET_CONTENT_TEXT_WITH_FILTER ('PREFERRED_PARTNER', 'ORDER_ACCESS_RESTRICTION', v_PREFERRED_PARTNER, null, v_PREFERRED_WARNING_MSG);

RETURN v_PREFERRED_WARNING_MSG;

END GET_PARTNER_WARNING_MSG;


FUNCTION ORDER_HAS_ADDON_TYPE
(
  IN_ORDER_DETAIL_ID      IN ORDER_ADD_ONS.ORDER_DETAIL_ID%TYPE,
  IN_ADDON_TYPE_ID        IN FTD_APPS.ADDON.ADDON_TYPE%TYPE
) RETURN VARCHAR2
AS
  v_count NUMBER;
  v_confirm varchar2(1) := 'N';

  BEGIN
     SELECT count(*)
       INTO v_count
       FROM order_add_ons oa, ftd_apps.addon a
      WHERE oa.add_on_code = a.addon_id
        AND a.addon_type = in_addon_type_id
        AND oa.order_detail_id = IN_ORDER_DETAIL_ID
        ;

     IF v_count > 0 THEN
         v_confirm := 'Y';
     END IF;

     RETURN v_confirm;

END ORDER_HAS_ADDON_TYPE;

FUNCTION ORDER_HAS_NO_ADDON_TYPE
(
  IN_ORDER_DETAIL_ID      IN ORDER_ADD_ONS.ORDER_DETAIL_ID%TYPE,
  IN_ADDON_TYPE_ID        IN FTD_APPS.ADDON.ADDON_TYPE%TYPE
) RETURN VARCHAR2
AS
  v_count NUMBER;
  v_confirm varchar2(1) := 'N';

  BEGIN
     SELECT count(*)
       INTO v_count
       FROM order_add_ons oa, ftd_apps.addon a
      WHERE oa.add_on_code = a.addon_id
        AND a.addon_type <> in_addon_type_id
        AND oa.order_detail_id = IN_ORDER_DETAIL_ID
        ;

     IF v_count > 0 THEN
         v_confirm := 'Y';
     END IF;

     RETURN v_confirm;

END ORDER_HAS_NO_ADDON_TYPE;



PROCEDURE GET_CART_FEES_SAVED
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the total shopping cart
        service fee saved and shipping fee saved amounts for orders that
        are not fully refunded

Input:
        order_guid                      varchar2

Output:
        cursor containing fees saved totals

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
        SELECT
                sum (ob.service_fee_saved) + sum (ob.shipping_fee_saved) serv_ship_fee_saved
        FROM    clean.order_bills ob
        JOIN    clean.order_details od
        ON      ob.order_detail_id = od.order_detail_id
        WHERE   od.order_guid = in_order_guid
        AND  (refund_pkg.is_order_fully_refunded(ob.order_detail_id) = 'N');


END GET_CART_FEES_SAVED;

PROCEDURE GET_ORDER_FEES_SAVED
(
IN_ID_STR                       IN VARCHAR2,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the total
        service fee saved and shipping fee saved amounts for orders that
        are not fully refunded. It returns 0 for orders that are fully
        refunded. Also returns the displayed message for the savings.

Input:
        IN_ID_STR               string containing order detail ids

Output:
        cursor containing fees saved totals and the savings message

-----------------------------------------------------------------------------*/
v_order_fees_saved_message      varchar2(4000);
v_freeship_program_name         varchar2(100);

BEGIN

-- Get the Fees Savings Message from Content and replace the Program Name
ftd_apps.content_query_pkg.get_content_text_with_filter
(
'SERVICE_CONFIG',
'ORDER_FEES_SAVED_MESSAGE',
null,
null,
v_order_fees_saved_message
);

FTD_APPS.CONTENT_QUERY_PKG.GET_CONTENT_TEXT_WITH_FILTER('SERVICES', 'PROGRAM_DISPLAY_NAME', 'FREESHIP', null, OUT_CONTENT_TXT=>v_freeship_program_name);

-- Replace the Program Display Name in the Message
v_order_fees_saved_message := replace(v_order_fees_saved_message, '~program_display_name~', v_freeship_program_name);

-- Escape any quotes in the message as we are building dynamic sql
v_order_fees_saved_message :=  replace(v_order_fees_saved_message, '''', '''''');

OPEN OUT_CUR FOR
        'select  ob.order_detail_id,
            case
                when (refund_pkg.is_order_fully_refunded(ob.order_detail_id) = ''Y'') then 0
            else
                sum ( nvl(ob.service_fee_saved, 0)) + sum ( nvl(ob.shipping_fee_saved, 0))
            end as order_fees_saved,

            case
                when (refund_pkg.is_order_fully_refunded(ob.order_detail_id) = ''Y'') then
                    replace(''' || v_order_fees_saved_message || ''', ''~total_order_savings~'', ''0.00'')
                else
                    replace(''' || v_order_fees_saved_message || ''', ''~total_order_savings~'', trim(to_char(
                      sum ( nvl(ob.service_fee_saved, 0)) + sum ( nvl(ob.shipping_fee_saved, 0))
                    , ''9,999.99'')))
            end as order_fees_saved_message
        from    order_bills ob
        where   ob.order_detail_id IN (' || in_id_str || ')' ||
        'GROUP BY ob.order_detail_id';

END GET_ORDER_FEES_SAVED;

PROCEDURE GET_CART_TAXES
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the shopping cart
        taxes by tax display order

Input:
        order_guid                      varchar2

Output:
        cursor containing cart taxes

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
    select tax_description, sum(tax_amount) tax_amount, display_order
    from (
        select ob.tax1_description tax_description, ob.tax1_amount tax_amount,
            (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax1_name) display_order
            FROM    clean.order_bills ob
            JOIN    clean.order_details od
            ON      ob.order_detail_id = od.order_detail_id
            WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax1_name is not null
        union all
        select ob.tax2_description tax_description, ob.tax2_amount tax_amount,
            (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax2_name) display_order
            FROM    clean.order_bills ob
            JOIN    clean.order_details od
            ON      ob.order_detail_id = od.order_detail_id
            WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax2_name is not null
        union all
        select ob.tax3_description tax_description, ob.tax3_amount tax_amount,
            (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax3_name) display_order
            FROM    clean.order_bills ob
            JOIN    clean.order_details od
            ON      ob.order_detail_id = od.order_detail_id
            WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax3_name is not null
        union all
        select ob.tax4_description tax_description, ob.tax4_amount tax_amount,
            (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax4_name) display_order
            FROM    clean.order_bills ob
            JOIN    clean.order_details od
            ON      ob.order_detail_id = od.order_detail_id
            WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax4_name is not null
        union all
        select tax5_description tax_description, ob.tax5_amount tax_amount,
            (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax5_name) display_order
            FROM    clean.order_bills ob
            JOIN    clean.order_details od
            ON      ob.order_detail_id = od.order_detail_id
            WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax5_name is not null
        
    )
    group by tax_description, display_order
    order by display_order;

END GET_CART_TAXES;

PROCEDURE GET_ORDER_TAXES 
( 
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE, 
OUT_CUR                         OUT TYPES.REF_CURSOR 
) 
AS 
/*----------------------------------------------------------------------------- 
Description: 
        This procedure is responsible for returning the individual Order Bills 
        taxes by order_detail_id and tax display order 
 
Input: 
        order_guid                      varchar2 
 
Output: 
        cursor containing cart taxes 
 
-----------------------------------------------------------------------------*/ 
BEGIN 
 
OPEN OUT_CUR FOR 
    select ob.order_detail_id, nvl(ob.tax1_description,'Taxes') tax_description, ob.tax1_amount tax_amount, 
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax1_name) display_order, 'N' is_total, 'State' tax_type, ob.tax1_rate tax_rate 
        FROM    clean.order_bills ob 
        JOIN    clean.order_details od 
        ON      ob.order_detail_id = od.order_detail_id 
        WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax1_name is not null 
    union 
    select ob.order_detail_id, nvl(ob.tax2_description,'Taxes') tax_description, ob.tax2_amount tax_amount, 
       (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax2_name) display_order, 'N' is_total, 'County' tax_type, ob.tax2_rate tax_rate 
       FROM    clean.order_bills ob 
        JOIN    clean.order_details od 
        ON      ob.order_detail_id = od.order_detail_id 
        WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax2_name is not null 
    union 
    select ob.order_detail_id, nvl(ob.tax3_description,'Taxes') tax_description, ob.tax3_amount tax_amount, 
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax3_name) display_order, 'N' is_total, 'City' tax_type, ob.tax3_rate tax_rate 
        FROM    clean.order_bills ob 
        JOIN    clean.order_details od 
        ON      ob.order_detail_id = od.order_detail_id 
        WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax3_name is not null 
    union 
    select ob.order_detail_id, nvl(ob.tax4_description,'Taxes') tax_description, ob.tax4_amount tax_amount, 
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax4_name) display_order, 'N' is_total, 'Zip' tax_type, ob.tax4_rate tax_rate 
        FROM    clean.order_bills ob 
        JOIN    clean.order_details od 
        ON      ob.order_detail_id = od.order_detail_id 
        WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax4_name is not null 
    union 
    select ob.order_detail_id, nvl(ob.tax5_description,'Taxes') tax_description, ob.tax5_amount tax_amount, 
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax5_name) display_order, 'N' is_total, null, ob.tax5_rate tax_rate 
        FROM    clean.order_bills ob 
        JOIN    clean.order_details od 
        ON      ob.order_detail_id = od.order_detail_id 
        WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax5_name is not null 
    union 
    select ob.order_detail_id, nvl(frp.misc_pkg.get_global_parm_value ('TAX_CONFIG', GET_LABEL_FROM_ORDER_DETAIL_ID(od.order_detail_id)), 'Taxes') tax_description, ob.tax  tax_amount, 1 display_order, 'Y' is_total, 'total_tax' tax_type, (GET_TOTAL_TAX_RATE(ob.order_detail_id)) tax_rate 
        FROM    clean.order_bills ob 
        JOIN    clean.order_details od 
        ON      ob.order_detail_id = od.order_detail_id 
        WHERE   od.order_guid = in_order_guid and additional_bill_indicator = 'N' and ob.tax  is not null 
    order by order_detail_id, display_order; 
 
END GET_ORDER_TAXES;

PROCEDURE GET_ORDER_DETAIL_TAXES
(
IN_ORDER_DETAIL_ID              IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the individual Order Bills
        taxes by order_detail_id and tax display order

Input:
        order_detail_id

Output:
        cursor containing order detail taxes

-----------------------------------------------------------------------------*/

v_tax_split number := 0;
v_sql_query varchar(5000);
v_global_label varchar(20);
v_tax_label varchar(30);

BEGIN

  v_tax_split:= TAX_SPLIT_DISP_ORDER(IN_ORDER_DETAIL_ID);
  
  
  v_tax_label := GET_LABEL_FROM_ORDER_DETAIL_ID(IN_ORDER_DETAIL_ID);
  v_global_label := frp.misc_pkg.get_global_parm_value ('TAX_CONFIG', v_tax_label);
  
  IF v_global_label is null or v_global_label = '' then
  v_global_label := frp.misc_pkg.get_global_parm_value ('TAX_CONFIG', 'DEFAULT_TAX_LABEL');
  END IF;
  
  
  v_sql_query := 	'select ob.order_detail_id, nvl(ob.tax1_description, ''Taxes'') tax_description, ob.tax1_amount tax_amount,
        nvl((select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax1_name), 0) display_order
        FROM    clean.order_bills ob
        WHERE   ob.order_detail_id = ''' || in_order_detail_id || ''' and additional_bill_indicator = ''N'' and ob.tax1_name is not null
    union
    select ob.order_detail_id, nvl(ob.tax2_description, ''Taxes'') tax_description, ob.tax2_amount tax_amount,
       nvl((select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax2_name), 0) display_order
        FROM    clean.order_bills ob
        WHERE   ob.order_detail_id = ''' || in_order_detail_id || ''' and additional_bill_indicator = ''N'' and ob.tax2_name is not null
    union
    select ob.order_detail_id, nvl(ob.tax3_description, ''Taxes'') tax_description, ob.tax3_amount tax_amount,
        nvl((select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax3_name), 0) display_order
        FROM    clean.order_bills ob
        WHERE   ob.order_detail_id = ''' || in_order_detail_id || ''' and additional_bill_indicator = ''N'' and ob.tax3_name is not null
    union
    select ob.order_detail_id, nvl(ob.tax4_description, ''Taxes'') tax_description, ob.tax4_amount tax_amount,
        nvl((select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax4_name), 0) display_order
        FROM    clean.order_bills ob
        WHERE   ob.order_detail_id = ''' || in_order_detail_id || ''' and additional_bill_indicator = ''N'' and ob.tax4_name is not null
    union
    select ob.order_detail_id, nvl(ob.tax5_description, ''Taxes'') tax_description, ob.tax5_amount tax_amount,
        nvl((select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax5_name), 0) display_order
        FROM    clean.order_bills ob
        WHERE   ob.order_detail_id = ''' || in_order_detail_id || ''' and additional_bill_indicator = ''N'' and ob.tax5_name is not null';
        
        if v_tax_split <= 0 then 
        
        v_sql_query := v_sql_query || ' UNION 
            select ob.order_detail_id, ''' || v_global_label || ''' tax_description, ob.tax tax_amount, 1 display_order
              FROM    clean.order_bills ob WHERE   ob.order_detail_id = ''' || in_order_detail_id || ''' and additional_bill_indicator = ''N'' and ob.tax is not null';
        END if;  
        
         v_sql_query := v_sql_query || ' order by display_order ' ;
  

  OPEN OUT_CUR FOR v_sql_query;

END GET_ORDER_DETAIL_TAXES;

FUNCTION GET_TAX_DISPLAY_ORDER_VAL
(
	IN_TAX_NAME              IN 	VARCHAR2
) RETURN NUMBER
AS
v_disp_order NUMBER;
BEGIN
	select DISPLAY_ORDER into v_disp_order from FTD_APPS.TAX_DISPLAY_ORDER_VAL
	where TAX_NAME = IN_TAX_NAME;

RETURN v_disp_order;

END GET_TAX_DISPLAY_ORDER_VAL;


PROCEDURE GET_GIFT_CARD_PAYMENTS
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_PAYMENT_CUR                 OUT TYPES.REF_CURSOR
) AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the gift card payment records
        for the given order. It only returns a few select fields.

Input:
        order_guid                      varchar2

Output:
        cursor containing payment records

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_PAYMENT_CUR FOR
    select p.payment_id,
      p.credit_amount,
      p.debit_amount,
      bill_status,
      s.orig_auth_amount_txt,
      p.payment_indicator,
      global.encryption.decrypt_it (cc.cc_number,cc.key_name) gift_card_number,
      global.encryption.decrypt_it (cc.gift_card_pin,cc.key_name) gift_card_pin
      --cc.cc_number gift_card_number,
      --cc.gift_card_pin gift_card_pin
    from clean.payments p
    left outer join scrub.payments s
    on s.payment_id = p.payment_id
    join  clean.credit_cards cc
    on p.cc_id = cc.cc_id
    where
      p.order_guid = IN_ORDER_GUID
      and p.payment_type = 'GD';
END GET_GIFT_CARD_PAYMENTS;


PROCEDURE IS_GD_ORDER_UPDATABLE
(
IN_ORDER_DETAIL_ID  IN clean.order_details.order_detail_id%type,
OUT_MODIFY_FLAG           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        order detail id is in final state.  Y = the cart is in final state.


Input:
        IN_ORDER_DETAIL_ID        number

Output:
        OUT_MODIFY_FLAG                 VARCHAR2

-----------------------------------------------------------------------------*/
v_count     number(4);

BEGIN

	SELECT count(*)
     INTO v_count
		FROM clean.order_details od
		WHERE od.order_detail_id = IN_ORDER_DETAIL_ID
		AND EXISTS (
                select 1 FROM clean.payments p where
                p.order_guid = od.order_guid and p.payment_type in ('GD')
               );

     IF v_count > 0 THEN
        OUT_MODIFY_FLAG := IS_CART_IN_FINAL_STATE(IN_ORDER_DETAIL_ID);
     ELSE
        OUT_MODIFY_FLAG := 'Y';
     END IF;

END IS_GD_ORDER_UPDATABLE;

FUNCTION IS_CART_IN_FINAL_STATE
(
IN_ORDER_DETAIL_ID  IN clean.order_details.order_detail_id%type
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        order detail id is in final state.  Y = the cart is in final state.


Input:
        IN_ORDER_DETAIL_ID        number

Output:
        Y/N

-----------------------------------------------------------------------------*/

v_final_state     char (1);
v_count     number(4);

BEGIN

	SELECT count(*)
     INTO v_count
		FROM clean.order_details od
		WHERE od.order_detail_id = IN_ORDER_DETAIL_ID
-- no longer consider "this" order		AND   od.order_disp_code IN 	('Processed','Shipped','Printed')
		AND NOT EXISTS (
							select 1 from clean.order_details od2 where
							od2.order_guid = od.order_guid
							and od2.order_disp_code in ('Validated', 'Held')
							and od2.order_detail_id != od.order_detail_id
						)
		AND NOT EXISTS (
							select 1 from scrub.order_details sod where
							sod.order_guid = od.order_guid
							and sod.status in ('2001','2002','2003','2005','2007')
						);

     IF v_count > 0 THEN
        v_final_state := 'Y';
     ELSE
        v_final_state := 'N';
     END IF;

RETURN v_final_state;

END IS_CART_IN_FINAL_STATE;

FUNCTION AVS_ADDRESS_EXISTS
(
IN_AVS_ADDRESS_ID                   IN clean.order_details.avs_address_id%TYPE
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning whether or not a
        avs_address exists

Input:
        avs_address_id                      number

Output:
        Y/N
-----------------------------------------------------------------------------*/
CURSOR exists_cur IS
        select  'Y'
        from    avs_address
        where   avs_address_id = in_avs_address_id;

v_exists        char(1) := 'N';
BEGIN

OPEN exists_cur;
FETCH exists_cur INTO v_exists;
CLOSE exists_cur;

RETURN v_exists;

END AVS_ADDRESS_EXISTS;

PROCEDURE GET_PREMIER_CIRCLE_INFO
(
IN_ORDER_GUID                   IN ORDERS.ORDER_GUID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves premier circle membership info.

Input:
        ORDER_GUID         number

Output:
        ref cusor that contains premier circle membership data

-----------------------------------------------------------------------------*/

BEGIN

OPEN out_cur FOR
        SELECT  distinct od.PC_GROUP_ID,
                od.PC_MEMBERSHIP_ID,
                od.PC_FLAG
        FROM    order_details od
        WHERE   od.order_guid = IN_ORDER_GUID;

END GET_PREMIER_CIRCLE_INFO;

PROCEDURE GET_PHOENIX_DETAILS
(
IN_ORDER_DETAIL_ID      IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_ORIGIN               IN VARCHAR2,
IN_MERCURY_ORDER_NUMBER IN VARCHAR2,
OUT_CUR OUT TYPES.REF_CURSOR
) AS

BEGIN

 IF IN_MERCURY_ORDER_NUMBER is not null and IN_MERCURY_ORDER_NUMBER != 'null' THEN
	OPEN OUT_CUR FOR select distinct
        'MERCURY' as record_source,
        od.order_detail_id,
        od.external_order_number,
        m.delivery_date,
        m.product_id product_id,
        m.recipient as recipient_name,
        m.address as mercury_address,
        recip.address_1 as recipient_address1,
        recip.address_2 as recipient_address2,
        recip.business_name as business_name,
        m.city_state_zip as city,
        null as state,
        m.zip_code,
        m.phone_number,
        m.card_message,
        recip.country,
        lower(trim(e.email_address)) customer_email,
        o.order_date,
        m.mercury_id,
        o.company_id,
        od.source_code
	from clean.order_details od 
	join clean.orders o on od.order_guid = o.order_guid
	join clean.customer recip on od.recipient_id = recip.customer_id
	join clean.email e on o.email_id = e.email_id
	join mercury.mercury m on to_char(od.order_detail_id) = m.reference_number
	where m.mercury_order_number = IN_MERCURY_ORDER_NUMBER
	and m.msg_type = 'FTD'
	and m.created_on = (select max(created_on) from mercury.mercury where mercury_message_number = IN_MERCURY_ORDER_NUMBER and msg_type = 'FTD');
  
 ELSE
	OPEN OUT_CUR FOR select distinct
        'OD' as record_source,
        od.order_detail_id,
        od.external_order_number,
        od.delivery_date,
        od.product_id product_id,
        recip.first_name || ' ' || recip.last_name as recipient_name,
        null as mercury_address,
        recip.address_1 as recipient_address1,
        recip.address_2 as recipient_address2,
        recip.business_name,
        recip.city,
        recip.state,
        recip.zip_code,
        nvl(dayphone.phone_number, nightphone.phone_number) as phone_number,
        od.card_message,
        recip.country,
        lower(trim(e.email_address)) customer_email,
        o.order_date,
        null,
        o.company_id,
        od.source_code       
        from clean.order_details od 
        join clean.orders o on od.order_guid = o.order_guid
        join clean.customer recip on od.recipient_id = recip.customer_id
        join clean.email e on o.email_id = e.email_id
	      left outer join clean.customer_phones dayphone on dayphone.customer_id = recip.customer_id and dayphone.phone_type = 'Day'
    	  left outer join clean.customer_phones nightphone on nightphone.customer_id = recip.customer_id and nightphone.phone_type = 'Evening'
        where od.order_detail_id = IN_ORDER_DETAIL_ID; 
 END IF;

END GET_PHOENIX_DETAILS;

PROCEDURE     GET_FLORIST_PDB_PRICE_FLAG 
(
IN_ORDER_GUID       IN CLEAN.ORDERS.ORIGIN_ID%TYPE,
OUT_CUR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the SEND FLORIST PDB PRICE FLAG from ftd_apps.origins 
        on the passed in guid.

Input:
        IN_ORDER_GUID

Output:
        cursor result set containing flag details
        

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
    	
	SELECT  SEND_FLORIST_PDB_PRICE
    FROM CLEAN.ORDERS o, FTD_APPS.ORIGINS origin
    WHERE o.ORDER_GUID = IN_ORDER_GUID
    AND o.ORIGIN_ID = origin.origin_id;

END GET_FLORIST_PDB_PRICE_FLAG;

FUNCTION CONVERT_TO_VALID_SQL_CLAUSE 
(
IN_LIST IN VARCHAR2
)RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a comma delimited list of strings for use in a SQL statement
    
Input:
    IN_LIST - space delimited list of Java Strings

Output:
    - VARCHAR2 containing a comma separate list of text

-----------------------------------------------------------------------------*/

v_return_string VARCHAR2(32767);
v_list VARCHAR2(32767);
v_comma_pos NUMBER(3);
v_value VARCHAR2(100);
v_value_plus_comma VARCHAR(100);

BEGIN
    v_list := in_list;
    
    WHILE INSTR(v_list,' ') <> 0 LOOP
  	    v_comma_pos := INSTR(v_list,' ');
  	    v_value := TRIM(SUBSTR(v_list,1,v_comma_pos-1));
  	    v_value_plus_comma := ''''||v_value||''',';
  	    v_return_string := v_return_string || v_value_plus_comma;
        v_list := SUBSTR(v_list, v_comma_pos+1);
    END LOOP;
    
    v_value := RTRIM(ltrim(v_list,' '),' ');
    v_return_string := v_return_string ||''''||v_value||'''';
    
    RETURN v_return_string;
  END CONVERT_TO_VALID_SQL_CLAUSE;

FUNCTION ADDON_AVAILABLE
(
IN_PHOENIX_PRODUCT IN VARCHAR2,
IN_ADD_ON_ID       IN VARCHAR2
)RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a 'Y' or 'N' value indicating whether the phoenix product passed in
	  has the addona passed in available 
    
Input:
    IN_PHOENIX_PRODUCT - phoenix product id
    IN_ADD_ON_ID    - phoenix add on id

Output:
    - VARCHAR2 'Y' or 'N'
-----------------------------------------------------------------------------*/

v_add_on_available VARCHAR2(1) := 'N';

v_add_on_count int := 0;

BEGIN    
                 
  	select count(*) into v_add_on_count
	  from ftd_apps.product_addon pa
	  where pa.ADDON_ID = IN_ADD_ON_ID
	  and pa.ACTIVE_FLAG = 'Y'
	  and pa.product_id = IN_PHOENIX_PRODUCT;
        
    IF v_add_on_count > 0 THEN
         v_add_on_available := 'Y';  
    END IF;
    
    RETURN v_add_on_available;
END ADDON_AVAILABLE;

PROCEDURE ORDER_PHOENIX_ELIGIBLE
(
IN_ORDER_DETAIL_ID      IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_ORIGIN               IN VARCHAR2,
IN_MERCURY_ORDER_NUMBER IN VARCHAR2,
OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a cursor that contains values indicating whether or
        not an order is Phoenix Eligible,
        phoenix product id, phoenix bear add on id, phoenix chocolate add on id,
        order delivery date, recipient zip code, flag indicating whether or not
        the original product and phoenix product are the same

Input:
        IN_ORDER_DETAIL_ID              IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
        IN_ORIGIN			                  IN VARCHAR2,
        IN_MERCURY_ORDER_NUMBER		      IN VARCHAR2,

Output:
        OUT_CUR				OUT VARCHAR2
-----------------------------------------------------------------------------*/
-- variables
v_order_phoenix_eligible varchar2(1) := 'N';
v_phoenix_prod_has_addons varchar2(1) := 'N';
v_orig_new_prod_same varchar2(1) := 'N';


v_excluded_preferred_partners VARCHAR2(32767);
v_excluded_origins VARCHAR2(32767);
v_phoenix_product_mapping VARCHAR2(32767);
v_prod_mapping VARCHAR2(32767);

v_order_detail_id VARCHAR2(22) := NULL;

v_mapped_phoenix_prod_count	int := 0;
v_mapped_phoenix_addon_count	int := 0;
v_allowed_addon_count		int := 0;
v_total_addon_cnt		int := 0;
v_excluded_partner_cnt		int := 0;
v_excluded_origin_cnt		int := 0;

v_cur      types.ref_cursor;
TYPE cur_typ IS REF CURSOR;
v_product_cur cur_typ;
v_product_sql_stmt VARCHAR2(32767);
v_product_id VARCHAR2(20);


v_addons_avail 		VARCHAR2(1) :='N';
v_orig_product_id   	VARCHAR2(20);
v_phoenix_product_id   	VARCHAR2(100);
v_choc_addon_id		VARCHAR2(20);
v_bear_addon_id		VARCHAR2(20);
v_delivery_date		VARCHAR2(20);
v_recip_zip_code	VARCHAR2(20);

v_space_pos NUMBER(3);
v_prod_avail_cnt	 int := 0;

BEGIN

  IF IN_MERCURY_ORDER_NUMBER is not null and IN_MERCURY_ORDER_NUMBER != 'null' THEN
	select od.order_detail_id, to_char(m.delivery_date, 'yyyy-mm-dd'), recip.zip_code, m.product_id into v_order_detail_id, v_delivery_date, v_recip_zip_code, v_orig_product_id
	from clean.order_details od
	join clean.orders o on od.order_guid = o.order_guid
	join clean.customer recip on od.recipient_id = recip.customer_id
	join clean.email e on o.email_id = e.email_id
	join mercury.mercury m on to_char(od.order_detail_id) = m.reference_number
	where m.mercury_order_number = IN_MERCURY_ORDER_NUMBER
	and m.msg_type = 'FTD'
	and m.created_on = (select max(created_on) from mercury.mercury where mercury_message_number = IN_MERCURY_ORDER_NUMBER and msg_type = 'FTD')
	and m.delivery_date between trunc(sysdate+1) and trunc(sysdate+1 + (select value from frp.global_parms where context = 'PHOENIX' and name = 'DELIVERY_DAYS_OUT_MAX'))
	and not exists (select 1 from clean.queue q where q.order_detail_id = od.order_detail_id and q.message_type in ('MO'))
	and not exists (select 1 from clean.point_of_contact poc where poc.order_detail_id = od.order_detail_id and poc.EMAIL_SUBJECT = 'Novator Email Request: CX' and poc.SENT_RECEIVED_INDICATOR = 'I')
	and (od.ship_method is null or od.ship_method not in ('ND', '2F', 'GR', 'SA'))
	and exists (select 1 from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_details od2 where ppm.original_product_id = od.product_id and DECODE(ppm.product_price_point_key, 'GOOD', 'A', 'BETTER', 'B', 'BEST', 'C') = od.size_indicator
              and od2.order_detail_id = od.order_detail_id )
	and
	(select sum(ob.product_amount +
	            ob.add_on_amount +
	            ob.service_fee +
	            ob.shipping_fee -
	            ob.discount_amount +
	            ob.shipping_tax +
	            ob.tax)
	from clean.order_bills ob
	where ob.order_detail_id = od.order_detail_id)
	-
	nvl((select sum (r.refund_product_amount +
	            r.refund_addon_amount +
	            r.refund_service_fee +
	            r.refund_shipping_fee -
	            r.refund_discount_amount +
	            r.refund_shipping_tax +
	            r.refund_tax)
	from clean.refund r
	where r.order_detail_id = od.order_detail_id),0) > 0
	--Exclude funeral occasions
	and m.occasion not in ('FUNERAL OCCASION')
	--Exclude address types
	and recip.address_type not in ('FUNERAL HOME', 'HOSPITAL', 'CEMETERY')
	--Domestic orders only excluding Alaska, Hawaii & Puerto Rico & Virgin Islands
	and recip.country = 'US'
	and recip.state not in ('AK', 'HI', 'PR', 'VI')
	and (to_char(m.delivery_date, 'DY') <> 'SAT'
	     or exists (select distinct 'Y'
	     from ftd_apps.carrier_zip_code czc
	     where czc.zip_code = m.zip_code
	     and czc.saturday_del_available = 'Y'));

  ELSE
        IF IN_ORIGIN is not null and IN_ORIGIN = 'COM' THEN
	        select od.order_detail_id, to_char(od.delivery_date, 'yyyy-mm-dd'), recip.zip_code, od.product_id into v_order_detail_id, v_delivery_date, v_recip_zip_code, v_orig_product_id
	        from clean.order_details od
	        join clean.orders o on od.order_guid = o.order_guid
	        join clean.customer recip on od.recipient_id = recip.customer_id
	        join clean.email e on o.email_id = e.email_id
	        where od.order_detail_id = IN_ORDER_DETAIL_ID
	        and od.delivery_date <= trunc(sysdate+1 + (select value from frp.global_parms where context = 'PHOENIX' and name = 'DELIVERY_DAYS_OUT_MAX'))
	        and (od.ship_method is null or od.ship_method not in ('ND', '2F', 'GR', 'SA'))
	        and exists (select 1 from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_details od2 where ppm.original_product_id = od.product_id and DECODE(ppm.product_price_point_key, 'GOOD', 'A', 'BETTER', 'B', 'BEST', 'C') = od.size_indicator
	                    and od2.order_detail_id = od.order_detail_id )
	        and
	        (select sum(ob.product_amount +
	                    ob.add_on_amount +
	                    ob.service_fee +
	                    ob.shipping_fee -
	                    ob.discount_amount +
	                    ob.shipping_tax +
	                    ob.tax)
	        from clean.order_bills ob
	        where ob.order_detail_id = od.order_detail_id)
	        -
	        nvl((select sum (r.refund_product_amount +
	                    r.refund_addon_amount +
	                    r.refund_service_fee +
	                    r.refund_shipping_fee -
	                    r.refund_discount_amount +
	                    r.refund_shipping_tax +
	                    r.refund_tax)
	        from clean.refund r
	        where r.order_detail_id = od.order_detail_id),0) > 0
	        --Exclude funeral occasions
	        and od.occasion not in (1)
	        --Exclude address types
	        and recip.address_type not in ('FUNERAL HOME', 'HOSPITAL', 'CEMETERY')
	        --Domestic orders only excluding Alaska, Hawaii & Puerto Rico & Virgin Islands
	        and recip.country = 'US'
	        and recip.state not in ('AK', 'HI', 'PR', 'VI')
	        and (to_char(od.delivery_date, 'DY') <> 'SAT'
	            or exists (select distinct 'Y'
	            from ftd_apps.carrier_zip_code czc
	            where czc.zip_code = recip.zip_code
	            and czc.saturday_del_available = 'Y'));
	ELSE
		select od.order_detail_id, to_char(od.delivery_date, 'yyyy-mm-dd'), recip.zip_code, od.product_id into v_order_detail_id, v_delivery_date, v_recip_zip_code, v_orig_product_id
	        from clean.order_details od
	        join clean.orders o on od.order_guid = o.order_guid
	        join clean.customer recip on od.recipient_id = recip.customer_id
	        join clean.email e on o.email_id = e.email_id
	        where od.order_detail_id = IN_ORDER_DETAIL_ID
	        and od.delivery_date between trunc(sysdate+1) and trunc(sysdate+1 + (select value from frp.global_parms where context = 'PHOENIX' and name = 'DELIVERY_DAYS_OUT_MAX'))
	        and not exists (select 1 from clean.queue q where q.order_detail_id = od.order_detail_id and q.message_type in ('MO'))
	        and not exists (select 1 from clean.point_of_contact poc where poc.order_detail_id = od.order_detail_id and poc.EMAIL_SUBJECT = 'Novator Email Request: CX' and poc.SENT_RECEIVED_INDICATOR = 'I')
	        and (od.ship_method is null or od.ship_method not in ('ND', '2F', 'GR', 'SA'))
	        and exists (select 1 from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_details od2 where ppm.original_product_id = od.product_id and DECODE(ppm.product_price_point_key, 'GOOD', 'A', 'BETTER', 'B', 'BEST', 'C') = od.size_indicator
	                    and od2.order_detail_id = od.order_detail_id )
	        and
	        (select sum(ob.product_amount +
	                    ob.add_on_amount +
	                    ob.service_fee +
	                    ob.shipping_fee -
	                    ob.discount_amount +
	                    ob.shipping_tax +
	                    ob.tax)
	        from clean.order_bills ob
	        where ob.order_detail_id = od.order_detail_id)
	        -
	        nvl((select sum (r.refund_product_amount +
	                    r.refund_addon_amount +
	                    r.refund_service_fee +
	                    r.refund_shipping_fee -
	                    r.refund_discount_amount +
	                    r.refund_shipping_tax +
	                    r.refund_tax)
	        from clean.refund r
	        where r.order_detail_id = od.order_detail_id),0) > 0
	        --Exclude funeral occasions
	        and od.occasion not in (1)
	        --Exclude address types
	        and recip.address_type not in ('FUNERAL HOME', 'HOSPITAL', 'CEMETERY')
	        --Domestic orders only excluding Alaska, Hawaii & Puerto Rico & Virgin Islands
	        and recip.country = 'US'
	        and recip.state not in ('AK', 'HI', 'PR', 'VI')
	        and (to_char(od.delivery_date, 'DY') <> 'SAT'
	            or exists (select distinct 'Y'
	            from ftd_apps.carrier_zip_code czc
	            where czc.zip_code = recip.zip_code
	            and czc.saturday_del_available = 'Y'));
        END IF;
  END IF;
   ----retrieve the phoenix product mapping
  IF v_order_detail_id is not null THEN

         select gp.value into v_excluded_preferred_partners
         from frp.global_parms gp
         where gp.context = 'PHOENIX' and gp.name = 'EXCLUDED_PREFERRED_PARTNERS';

         v_excluded_preferred_partners := convert_to_valid_sql_clause(v_excluded_preferred_partners);

         EXECUTE IMMEDIATE 'select count(*)
	       from ftd_apps.source_program_ref spr, ftd_apps.partner_program pp, ftd_apps.partner_master pm, clean.order_details od
	       where od.order_detail_id = ' || v_order_detail_id || '
	       and pp.program_name = spr.program_name and pm.partner_name = pp.partner_name
	       and pm.preferred_partner_flag = ''Y''
	       and spr.source_code = od.source_code
	       and pp.partner_name in (' || v_excluded_preferred_partners || ') '
	       into v_excluded_partner_cnt;

         select gp.value into v_excluded_origins
         from frp.global_parms gp
         where gp.context = 'PHOENIX' and gp.name = 'EXCLUDED_ORIGINS';

	 IF v_excluded_origins is not null THEN
	      v_excluded_origins := convert_to_valid_sql_clause(v_excluded_origins);

	      EXECUTE IMMEDIATE 'select count(*)
              from clean.orders o, clean.order_details od
	      where od.order_detail_id in (' || v_order_detail_id || ')
              and od.order_guid = o.order_guid
              and o.origin_id in (' || v_excluded_origins || ') '
	      into v_excluded_origin_cnt;
         END IF;


         IF v_excluded_partner_cnt = 0 and v_excluded_origin_cnt = 0 THEN

	       select ppm.new_product_id into v_phoenix_product_mapping
	       from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_details od
	       where ppm.original_product_id = od.product_id
	       and DECODE(ppm.product_price_point_key, 'GOOD', 'A', 'BETTER', 'B', 'BEST', 'C') = od.size_indicator
	       and od.order_detail_id = v_order_detail_id;


	       v_product_sql_stmt := 'select pm.product_id
	                             from ftd_apps.product_master pm
		                     where pm.PRODUCT_ID in (' || v_phoenix_product_mapping || ')
		                     and pm.status = ''A'' ';

               v_phoenix_product_mapping := v_phoenix_product_mapping || rpad(' ',1,' ') ;

               WHILE INSTR(v_phoenix_product_mapping,' ') <> 0 LOOP

                    v_space_pos := INSTR(v_phoenix_product_mapping,' ');
  	            v_product_id := TRIM(SUBSTR(v_phoenix_product_mapping,1,v_space_pos-1));

                    v_prod_mapping := convert_to_valid_sql_clause(v_product_id);

        	    EXECUTE IMMEDIATE 'select count(*)
	            from ftd_apps.product_master pm
		    where pm.PRODUCT_ID in (' || v_prod_mapping || ')
		    and pm.status = ''A'' '
	      	    into v_prod_avail_cnt;

                    IF v_prod_avail_cnt > 0 THEN

                      --check to see how many add ons are on the order
                      select count(*) into v_total_addon_cnt
                      from clean.order_add_ons oao
                      where oao.order_detail_id = v_order_detail_id;

                      IF v_total_addon_cnt > 0 THEN

                          --check to see how many of the add ons on the order are for bears and chocolates
                          select count(*) into v_allowed_addon_count
                          from clean.order_add_ons oao
                          where oao.order_detail_id = v_order_detail_id
                          and oao.ADD_ON_CODE in ('B', 'C')
                          and oao.ADD_ON_QUANTITY = 1;

                          IF v_total_addon_cnt > 0 and v_total_addon_cnt = v_allowed_addon_count THEN
                               --check to see that there is a mapping for the add on(s)
                               select count(*) into v_mapped_phoenix_addon_count
                               from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm, clean.order_add_ons oao, ftd_apps.addon a
                               where oao.order_detail_id = v_order_detail_id
                               and oao.ADD_ON_CODE = a.ADDON_ID
                               and ppm.original_product_id = a.product_id
                               and oao.ADD_ON_QUANTITY = 1;

                               IF v_mapped_phoenix_addon_count = v_allowed_addon_count THEN
                                    PHOENIX_PROD_HAS_ADDONS
                                    (
                                      IN_ORDER_DETAIL_ID=>v_order_detail_id,
                                      IN_PHOENIX_PRODUCTS=>v_prod_mapping,
                                      OUT_CUR=>v_cur
                                    );

                                    FETCH v_cur INTO
                                         v_addons_avail,
                                         v_phoenix_product_id,
                                         v_bear_addon_id,
                                         v_choc_addon_id;
                                    CLOSE v_cur;

                                    IF v_addons_avail = 'Y' THEN
                                         v_order_phoenix_eligible := v_addons_avail;
                                         EXIT;
                                    END IF;
                               END IF;--end v_mapped_phoenix_addon_count
                          END IF; -- end if total add on count > 0
                      ELSE
                            v_order_phoenix_eligible := 'Y';
                           IF length(v_phoenix_product_id) > 0 THEN
                                v_phoenix_product_id := v_phoenix_product_id || ',' || v_product_id;
                           ELSE
                               v_phoenix_product_id := v_product_id;
                           END IF;
                           --EXIT;
                      END IF;--end IF v_total_addon_cnt > 0
                  END IF;
    	          v_phoenix_product_mapping := SUBSTR(v_phoenix_product_mapping, v_space_pos+1);
    	     END LOOP;
         END IF; -- end IF v_excluded_partner_cnt = 0 and v_excluded_origin_cnt = 0
  END IF;-- end if order detail is null

  IF v_orig_product_id = v_phoenix_product_id THEN
       v_orig_new_prod_same := 'Y';
  END IF;

  OPEN out_cur FOR
  SELECT  v_order_phoenix_eligible v_order_phoenix_eligible,
          v_phoenix_product_id v_phoenix_product_id,
          v_bear_addon_id v_bear_addon_id,
          v_choc_addon_id v_choc_addon_id,
          v_delivery_date v_delivery_date,
          v_recip_zip_code v_recip_zip_code,
          v_orig_new_prod_same v_orig_new_prod_same
  FROM    dual;



  EXCEPTION WHEN NO_DATA_FOUND THEN
  	  OPEN out_cur FOR
  	  SELECT  v_order_phoenix_eligible v_order_phoenix_eligible,
          	  v_phoenix_product_id v_phoenix_product_id,
          	  v_bear_addon_id v_bear_addon_id,
          	  v_choc_addon_id v_choc_addon_id,
              v_delivery_date v_delivery_date,
          	  v_recip_zip_code v_recip_zip_code,
          	  v_orig_new_prod_same v_orig_new_prod_same
  FROM    dual; 
END ORDER_PHOENIX_ELIGIBLE;

PROCEDURE PHOENIX_PROD_HAS_ADDONS
(
IN_ORDER_DETAIL_ID  IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_PHOENIX_PRODUCTS IN VARCHAR2,
OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a cursor containing values indicating whether the phoenix product passed in
	  has addons available, phoenix product and phoenix add ons

Input:
    IN_ORDER_DETAIL_ID - order detail id
    IN_PHOENIX_PRODUCTS - phoenix products

Output:
    OUT_CUR - cursor containing the following values
              v_both_addons_avail
              v_phoenix_product
              v_choc_addon
              v_bear_addon

-----------------------------------------------------------------------------*/

avail_mapped_phoenix_prod_cur     types.ref_cursor;

v_bear_cnt		            int := 0;
v_chocolate_cnt		        int := 0;
v_avail_bear_addon_cnt		int := 0;
v_avail_choc_addon_cnt		int := 0;

v_cur                   types.ref_cursor;
TYPE cur_typ IS REF CURSOR;
v_c cur_typ;
v_sql_stmt 		        VARCHAR2(32767);
v_prod_id 		        VARCHAR2(20);
v_bear_addons 		    VARCHAR2(32767);
v_chocolate_addons 	  VARCHAR2(32767);
v_bear_addons_list 	    VARCHAR2(32767);
v_chocolate_addons_list     VARCHAR2(32767);

v_addons_avail 		    VARCHAR2(1) :='N';
v_bear_addon_avail	    VARCHAR2(1) :='N';
v_choc_addon_avail	    VARCHAR2(1) :='N';
v_phoenix_product_id	    VARCHAR2(20);
v_choc_addon_id		    VARCHAR2(20);
v_bear_addon_id		    VARCHAR2(20);
v_space_pos NUMBER(3);

BEGIN

    select count(*) into v_chocolate_cnt
    from clean.order_add_ons oao
    where oao.add_on_code = 'C'
    and oao.order_detail_id = IN_ORDER_DETAIL_ID;

    select count(*) into v_bear_cnt
    from clean.order_add_ons oao
    where oao.add_on_code = 'B'
    and oao.order_detail_id = IN_ORDER_DETAIL_ID;

    v_sql_stmt := 'select pm.product_id
        from ftd_apps.product_master pm
        where pm.PRODUCT_ID in (' || IN_PHOENIX_PRODUCTS ||')
        and pm.status = ''A'' ';

    OPEN v_c FOR v_sql_stmt;
         LOOP
              FETCH v_c INTO v_prod_id;
                   IF v_addons_avail = 'Y' THEN
              	        EXIT;
                   END IF;

                   IF v_chocolate_cnt > 0 and v_bear_cnt > 0 THEN

                        select ppm.NEW_PRODUCT_ID into v_chocolate_addons_list
                        from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm
                        where ppm.original_product_id in ( 'CKJ');

                        select ppm.NEW_PRODUCT_ID into v_bear_addons_list
                        from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm
                        where ppm.original_product_id in ( 'BKJ');

                        v_chocolate_addons_list := v_chocolate_addons_list || rpad(' ',1,' ') ;

                        WHILE INSTR(v_chocolate_addons_list,' ') <> 0 LOOP

	                    v_space_pos := INSTR(v_chocolate_addons_list,' ');
	  	            v_choc_addon_id := TRIM(SUBSTR(v_chocolate_addons_list,1,v_space_pos-1));
	  	            v_chocolate_addons := convert_to_valid_sql_clause(v_choc_addon_id);

	                    EXECUTE IMMEDIATE 'select pa.addon_id
			    from ftd_apps.addon pa
			    where pa.product_id in (' || v_chocolate_addons ||') '
			    into v_chocolate_addons;
	                    v_choc_addon_avail := ADDON_AVAILABLE(v_prod_id, v_chocolate_addons);

             		    IF v_choc_addon_avail = 'Y'  THEN
             		    	v_choc_addon_id := v_chocolate_addons;
                                EXIT;
                            END IF;
                            v_chocolate_addons_list := SUBSTR(v_chocolate_addons_list, v_space_pos+1);

	                END LOOP;

                        IF v_choc_addon_avail = 'Y' THEN
                             --check bear addons
                             v_bear_addons_list := v_bear_addons_list || rpad(' ',1,' ') ;
                             WHILE INSTR(v_bear_addons_list,' ') <> 0 LOOP

	                          v_space_pos := INSTR(v_bear_addons_list,' ');
	  	                  v_bear_addon_id := TRIM(SUBSTR(v_bear_addons_list,1,v_space_pos-1));
	  	                  v_bear_addons := convert_to_valid_sql_clause(v_bear_addon_id);

	                          EXECUTE IMMEDIATE 'select pa.addon_id
			          from ftd_apps.addon pa
			          where pa.product_id in (' || v_bear_addons ||') '
			          into v_bear_addons;
	                          v_bear_addon_avail := ADDON_AVAILABLE(v_prod_id, v_bear_addons);

             		          IF v_bear_addon_avail = 'Y' THEN
                                    v_addons_avail := 'Y';
                                   
                                IF length(v_phoenix_product_id) > 0 THEN
                                   v_phoenix_product_id := v_phoenix_product_id || ',' || v_prod_id;
                                ELSE
                                   v_phoenix_product_id := v_prod_id;
                           END IF;
	           		    v_bear_addon_id := v_bear_addons;
                                    EXIT;
                                  END IF;
	        	     v_bear_addons_list := SUBSTR(v_bear_addons_list, v_space_pos+1);
	                     END LOOP;
                        ELSE
                            v_addons_avail := 'N';
                        END IF;

                   ELSE
                        IF v_bear_cnt > 0 THEN
                             select ppm.NEW_PRODUCT_ID into v_bear_addons_list
                             from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm
                             where ppm.original_product_id in ( 'BKJ');

			     v_bear_addons_list := v_bear_addons_list || rpad(' ',1,' ');

                             WHILE INSTR(v_bear_addons_list,' ') <> 0 LOOP

	                          v_space_pos := INSTR(v_bear_addons_list,' ');
	  	                  v_bear_addon_id := TRIM(SUBSTR(v_bear_addons_list,1,v_space_pos-1));
	  	                  v_bear_addons := convert_to_valid_sql_clause(v_bear_addon_id);

	                          EXECUTE IMMEDIATE 'select pa.addon_id
			          from ftd_apps.addon pa
			          where pa.product_id in (' || v_bear_addons ||') '
			          into v_bear_addons;
	                          v_bear_addon_avail := ADDON_AVAILABLE(v_prod_id, v_bear_addons);

             		          IF v_bear_addon_avail = 'Y' THEN
                                    v_addons_avail := 'Y';
                                IF length(v_phoenix_product_id) > 0 THEN
                                   v_phoenix_product_id := v_phoenix_product_id || ',' || v_prod_id;
                                ELSE
                                   v_phoenix_product_id := v_prod_id;
                           END IF;
	           		    v_bear_addon_id := v_bear_addons;
                                    EXIT;
                                  END IF;

	        	     v_bear_addons_list := SUBSTR(v_bear_addons_list, v_space_pos+1);
	                     END LOOP;

                        END IF;

                        IF v_chocolate_cnt > 0 THEN
                            select ppm.NEW_PRODUCT_ID into v_chocolate_addons_list
                            from FTD_APPS.PHOENIX_PRODUCT_MAPPING ppm
                            where ppm.original_product_id in ( 'CKJ');

 			    v_chocolate_addons_list := v_chocolate_addons_list || rpad(' ',1,' ') ;

                            WHILE INSTR(v_chocolate_addons_list,' ') <> 0 LOOP

	                    v_space_pos := INSTR(v_chocolate_addons_list,' ');
	  	            v_choc_addon_id := TRIM(SUBSTR(v_chocolate_addons_list,1,v_space_pos-1));
	  	            v_chocolate_addons := convert_to_valid_sql_clause(v_choc_addon_id);

	                    EXECUTE IMMEDIATE 'select pa.addon_id
			    from ftd_apps.addon pa
			    where pa.product_id in (' || v_chocolate_addons ||') '
			    into v_chocolate_addons;

	                    v_choc_addon_avail := ADDON_AVAILABLE(v_prod_id, v_chocolate_addons);

             		    IF v_choc_addon_avail = 'Y'  THEN
             		    	v_addons_avail := 'Y';
                                
                           IF length(v_phoenix_product_id) > 0 THEN
                                v_phoenix_product_id := v_phoenix_product_id || ',' || v_prod_id;
                           ELSE
                               v_phoenix_product_id := v_prod_id;
                           END IF;
	           		v_choc_addon_id := v_chocolate_addons;
                                EXIT;
                            END IF;
                            v_chocolate_addons_list := SUBSTR(v_chocolate_addons_list, v_space_pos+1);

	                END LOOP;

		        END IF;

                   END IF;

	            EXIT WHEN v_c%NOTFOUND;
         END LOOP;
    CLOSE v_c;

   open OUT_CUR for
     select  v_addons_avail,
	           v_phoenix_product_id,
	           v_bear_addon_id,
                   v_choc_addon_id
     from dual;
END PHOENIX_PROD_HAS_ADDONS;   

PROCEDURE ADDONS_AVAILABLE
(
IN_PHOENIX_PRODUCT IN VARCHAR2,
IN_ADD_ONS         IN VARCHAR2,
OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a cursor containing values indicating whether the phoenix product 
	  has the addon available, phoenix product and phoenix add on
    
Input:
    IN_PHOENIX_PRODUCT - phoenix product id
    IN_ADD_ONS    -      phoenix add on ids
  
Output:
    OUT_CUR - cursor containing the following values
              v_has_addon
              v_prod_id
              v_add_on_id
 -----------------------------------------------------------------------------*/

v_has_addon       VARCHAR2(1) := 'N';

TYPE cur_typ IS REF CURSOR;
v_addon_cur cur_typ;
v_addon_sql_stmt  VARCHAR2(32767);
v_add_on_id       VARCHAR2(20);
v_add_on_avail    VARCHAR2(20);

v_addon_cnt		    int := 0;


BEGIN    
  
    v_addon_sql_stmt := 'select pa.addon_id
			from ftd_apps.addon pa
			where pa.product_id in (' || IN_ADD_ONS ||') ';
 
    OPEN v_addon_cur FOR v_addon_sql_stmt;
         LOOP
	      FETCH v_addon_cur INTO v_add_on_id;
  
              v_has_addon := ADDON_AVAILABLE(IN_PHOENIX_PRODUCT, v_add_on_id);
      
              IF v_has_addon = 'Y' THEN
                   EXIT;
              END IF;
	      
              EXIT WHEN v_addon_cur%NOTFOUND;

         END LOOP;
    CLOSE v_addon_cur;

  open OUT_CUR for
       select v_has_addon,
       IN_PHOENIX_PRODUCT,
       v_add_on_id
  from dual;	
END ADDONS_AVAILABLE;  

PROCEDURE BOTH_ADDONS_AVAILABLE
(
IN_PHOENIX_PRODUCT IN VARCHAR2,
IN_BEAR_ADD_ONS    IN VARCHAR2,
IN_CHOC_ADD_ONS    IN VARCHAR2,
OUT_CUR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a cursor containing whether or not the product has both addons 
    available, phoenix product id, beard addon id and chocolate addon id
    
Input:
    IN_PHOENIX_PRODUCT - phoenix product id
    IN_BEAR_ADD_ONS    - phoenix bear add on ids
    IN_CHOC_ADD_ONS    - phoenix chocolate add on ids

Output:
    OUT_PHOENIX_CUR - cursor containing the following values
		      v_addons_avail
		      v_phoenix_product_id
		      v_bear_addon_id
		      v_choc_addon_id
-----------------------------------------------------------------------------*/

TYPE cur_typ IS REF CURSOR;
v_choc_cur cur_typ;
v_bear_cur cur_typ;
v_bear_sql_stmt       VARCHAR2(32767);
v_choc_sql_stmt       VARCHAR2(32767);
v_prod_id             VARCHAR2(20);
v_bear_add_on_id      VARCHAR2(20);
v_choc_add_on_id      VARCHAR2(20);

v_bear_add_on_avail   VARCHAR2(20);
v_choc_add_on_avail   VARCHAR2(20);

v_bear_cnt		        int := 0;
v_chocolate_cnt		    int := 0;
v_has_bear_addon      VARCHAR2(1) := 'N';
v_has_choc_addon      VARCHAR2(1) := 'N';
v_both_addon_avail    VARCHAR2(1) := 'N';

BEGIN    
   
    v_bear_sql_stmt := 'select pa.addon_id
		from ftd_apps.addon pa
		where pa.product_id in (' || IN_BEAR_ADD_ONS ||') ';
		
    v_choc_sql_stmt := 'select pa.addon_id
		from ftd_apps.addon pa
		where pa.product_id in (' || IN_CHOC_ADD_ONS ||') ';
 
    OPEN v_bear_cur FOR v_bear_sql_stmt;
         LOOP
	            FETCH v_bear_cur INTO v_bear_add_on_id;
              IF v_both_addon_avail = 'Y' THEN
                   EXIT;
              END IF;
 
              v_has_bear_addon := ADDON_AVAILABLE(IN_PHOENIX_PRODUCT, v_bear_add_on_id);

              IF v_has_bear_addon = 'Y' THEN
                   OPEN v_choc_cur FOR v_choc_sql_stmt;
                        LOOP
                             FETCH v_choc_cur INTO v_choc_add_on_id;
                            
                             v_has_choc_addon := ADDON_AVAILABLE(IN_PHOENIX_PRODUCT, v_choc_add_on_id);
 		      
                             IF v_has_choc_addon = 'Y' THEN
                                  v_both_addon_avail := 'Y';
                                  EXIT;			          
                             END IF;
			     			     
                             EXIT WHEN v_choc_cur%NOTFOUND;
                        END LOOP;
                   CLOSE v_choc_cur;
              END IF;
	            EXIT WHEN v_bear_cur%NOTFOUND;
         END LOOP;
    CLOSE v_bear_cur;
    
    open out_cur for
    select  v_both_addon_avail,
            IN_PHOENIX_PRODUCT,
            v_bear_add_on_id,
            v_choc_add_on_id
    from dual;
    
  END BOTH_ADDONS_AVAILABLE;

function IS_PHOENIX_ORDER
(
IN_ORDER_NUMBER  in CLEAN.ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%type
)
return varchar2

as
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        order detail id is in final state.  Y = the cart is in final state.


Input:
        IN_ORDER_DETAIL_ID        number

Output:
        Y/N

-----------------------------------------------------------------------------*/

V_FINAL_STATE     char (1);
V_COUNT     number(4);

begin

    SELECT COUNT(*) INTO V_COUNT  FROM CLEAN.PHOENIX_ORDER_TRACKING POT, CLEAN.ORDER_DETAILS OD WHERE POT.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID AND OD.EXTERNAL_ORDER_NUMBER  = IN_ORDER_NUMBER;
     if V_COUNT > 0 then
        V_FINAL_STATE := 'Y';
     else
        V_FINAL_STATE := 'N';
     end if;

return V_FINAL_STATE;

end IS_PHOENIX_ORDER;

FUNCTION CHECK_FOR_LIVE_FTD
(
IN_ORDER_DETAIL_ID          IN CLEAN.order_details.ORDER_DETAIL_ID%TYPE
)
return varchar2

AS

/*-----------------------------------------------------------------------------
Description:
	  This FUNCTION is responsible to check to see it there ia live FTD
    for given order detail id. If yes, it returns "Y" otherwise "N"

Input:
        order_detail_id                 clean.order_details.order_detail_id

Output:
        Y if the order has LIVE FTD OTHERWISE N
-----------------------------------------------------------------------------*/
v_cnt		    int := 0;
out_flag    varchar2(1);
BEGIN

select count(*) into v_cnt from (
SELECT distinct COD.external_order_number
FROM MERCURY.MERCURY MM,CLEAN.ORDER_DETAILS COD
WHERE  cod.order_Detail_id = mm.reference_number and
REFERENCE_NUMBER IN
(SELECT TO_CHAR(CQ.ORDER_DETAIL_ID)
  FROM CLEAN.QUEUE CQ,
    --CLEAN.PHOENIX_ORDER_TRACKING CPOT,
    MERCURY.MERCURY
  WHERE CQ.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID
  AND REFERENCE_NUMBER     = TO_CHAR(IN_ORDER_DETAIL_ID)
  AND CQ.QUEUE_TYPE        IN ( 'ASK','ORDER')
  )
AND NOT EXISTS
  (SELECT TO_CHAR(CQ.ORDER_DETAIL_ID)
  FROM CLEAN.QUEUE CQ,
    --CLEAN.PHOENIX_ORDER_TRACKING CPOT,
    MERCURY.MERCURY MM1
  WHERE CQ.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID
  AND MM1.REFERENCE_NUMBER = TO_CHAR(IN_ORDER_DETAIL_ID)
  AND CQ.QUEUE_TYPE        in ('ASK','ORDER')
  AND MM1.MSG_TYPE         = 'CAN'
  AND MM1.REFERENCE_NUMBER = MM.REFERENCE_NUMBER 
 ) and cod.order_detail_id = IN_ORDER_DETAIL_ID);
 
 IF v_cnt > 0 THEN
  out_flag := 'Y';
 ELSE
  out_flag := 'N';
 END IF;

return out_flag;    
END CHECK_FOR_LIVE_FTD;  

PROCEDURE GET_ORDER_ITEM_TAX (
	IN_ORDER_DTL_ID               IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
	OUT_CUR                       OUT TYPES.REF_CURSOR,
  	OUT_DISP_CUR                  OUT TYPES.REF_CURSOR
)

AS
/*-----------------------------------------------------------------------------
Description:
This procedure is responsible for returning the tax amounts

Input:
order_bill_id                      varchar2

Output:
cursor containing billing totals

-----------------------------------------------------------------------------*/

BEGIN

     OPEN OUT_CUR FOR
			select sum (ob.shipping_tax) + sum (ob.tax) tax, 
				sum(ob.tax1_amount) tax1_amount, sum(ob.tax1_rate) tax1_rate,
				sum(ob.tax2_amount) tax2_amount, sum(ob.tax2_rate) tax2_rate,
				sum(ob.tax3_amount) tax3_amount, sum(ob.tax3_rate) tax3_rate,
				sum(ob.tax4_amount) tax4_amount, sum(ob.tax4_rate) tax4_rate,
				sum(ob.tax5_amount) tax5_amount, sum(ob.tax5_rate) tax5_rate
        FROM    order_bills ob 
        WHERE   ob.order_detail_id = IN_ORDER_DTL_ID
        GROUP BY ob.additional_bill_indicator ;
        
     OPEN OUT_DISP_CUR FOR
          select distinct tax1_name, tax1_description, tax2_name, tax2_description, tax3_name, tax3_description, tax4_name, 
          		tax4_description, tax5_name, tax5_description
          FROM clean.order_bills 
          WHERE order_detail_id = IN_ORDER_DTL_ID;
       

END GET_ORDER_ITEM_TAX;

FUNCTION TAX_SPLIT_DISP_ORDER (
	IN_ORDER_ITEM_ID  IN 	VARCHAR2
) RETURN NUMBER
AS
v_tax_split NUMBER;
BEGIN

    select distinct display_order into v_tax_split from (
        select tdov.display_order display_order FROM clean.order_bills ob
          JOIN    clean.order_details od ON ob.order_detail_id = od.order_detail_id 
          JOIN    ftd_apps.tax_display_order_val tdov on tdov.tax_name = ob.tax1_name
          WHERE   od.order_detail_id = IN_ORDER_ITEM_ID and additional_bill_indicator = 'N' and ob.tax1_name is not null
      union
        select tdov.display_order display_order FROM clean.order_bills ob
          JOIN    clean.order_details od ON ob.order_detail_id = od.order_detail_id
          JOIN    ftd_apps.tax_display_order_val tdov on tdov.tax_name = ob.tax2_name
          WHERE   od.order_detail_id = IN_ORDER_ITEM_ID and additional_bill_indicator = 'N' and ob.tax2_name is not null
      union
       select tdov.display_order display_order FROM clean.order_bills ob
          JOIN    clean.order_details od ON ob.order_detail_id = od.order_detail_id
          JOIN    ftd_apps.tax_display_order_val tdov on tdov.tax_name = ob.tax3_name
          WHERE   od.order_detail_id = IN_ORDER_ITEM_ID and additional_bill_indicator = 'N' and ob.tax3_name is not null
      union
       select tdov.display_order display_order FROM clean.order_bills ob
          JOIN    clean.order_details od ON ob.order_detail_id = od.order_detail_id
          JOIN    ftd_apps.tax_display_order_val tdov on tdov.tax_name = ob.tax4_name
          WHERE   od.order_detail_id = IN_ORDER_ITEM_ID and additional_bill_indicator = 'N' and ob.tax4_name is not null
      union
        select tdov.display_order display_order FROM clean.order_bills ob
          JOIN    clean.order_details od ON ob.order_detail_id = od.order_detail_id
          JOIN    ftd_apps.tax_display_order_val tdov on tdov.tax_name = ob.tax5_name
          WHERE   od.order_detail_id = IN_ORDER_ITEM_ID and additional_bill_indicator = 'N' and ob.tax5_name is not null
     ) where display_order = 1;
     
      RETURN v_tax_split;
     
    EXCEPTION 
      WHEN NO_DATA_FOUND THEN v_tax_split := 0;
    
  RETURN v_tax_split;

END TAX_SPLIT_DISP_ORDER;

FUNCTION GET_LABEL_FROM_ORDER_DETAIL_ID (
  IN_ORDER_DETAIL_ID IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
) RETURN VARCHAR2
AS
v_country VARCHAR2(20);
v_company_id VARCHAR2(20);
v_label VARCHAR2(100);
BEGIN

  select cc.COUNTRY,fsc.COMPANY_ID into v_country,v_company_id from clean.order_details cod 
  join clean.customer cc on cod.RECIPIENT_ID = cc.CUSTOMER_ID 
  join FTD_APPS.SOURCE_MASTER fsc on cod.SOURCE_CODE = fsc.SOURCE_CODE
  where order_detail_id = IN_ORDER_DETAIL_ID;
  
  v_label := 'DEFAULT_TAX_LABEL';
 
  IF v_company_id <> 'FTDCA' then 
      v_company_id := 'NON_FTDCA';
  END IF;
  
  IF v_country = 'CA' then
      v_label := v_company_id ||'_'|| v_country || '_DEFAULT_TAX_LABEL';
      DBMS_OUTPUT.PUT_LINE('v_label:' || v_label);
  END IF;
  
  RETURN v_label;
  
  EXCEPTION
     WHEN OTHERS THEN v_label := 'DEFAULT_TAX_LABEL';
     RETURN v_label;
   
END GET_LABEL_FROM_ORDER_DETAIL_ID;

FUNCTION GET_TOTAL_TAX_RATE 
( 
IN_ORDER_DETAIL_ID                  IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE 
) 
RETURN NUMBER 
 
AS 
/*----------------------------------------------------------------------------- 
Description: 
        This procedure is responsible for returning total tax rate for an order
 
Input: 
        order_detail_id         varchar 
 
Output: 
        v_total_tax_rate 
 
-----------------------------------------------------------------------------*/ 
 
v_total_tax_rate   number; 
 
BEGIN 
 
select (nvl(ob.tax1_rate,0)) + (nvl(ob.tax2_rate,0)) + (nvl(ob.tax3_rate,0)) + (nvl(ob.tax4_rate,0)) + (nvl(ob.tax5_rate,0)) into v_total_tax_rate
        FROM    clean.order_bills ob 
        WHERE   ob.order_detail_id = IN_ORDER_DETAIL_ID and additional_bill_indicator = 'N';
        
RETURN v_total_tax_rate; 
 
END GET_TOTAL_TAX_RATE; 

PROCEDURE GET_ORDER_ITEM_REMAINING_TAX (
	IN_ORDER_DTL_ID               IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
	OUT_CUR                       OUT TYPES.REF_CURSOR,
 	OUT_DISP_CUR                  OUT TYPES.REF_CURSOR
)

AS
/*-----------------------------------------------------------------------------
Description:
This procedure is responsible for returning the remaining tax amounts

Input:
order_bill_id                      varchar2

Output:
cursor containing billing totals

-----------------------------------------------------------------------------*/

BEGIN

      OPEN OUT_CUR FOR
      select	nvl(sum(ob.shipping_tax),0) + nvl(sum (ob.tax),0) - nvl((select sum (refund_tax) from clean.refund r where r.order_detail_id = ob.order_detail_id),0) tax, 
      (nvl(sum(ob.tax1_amount),0)) - nvl((select sum (tax1_amount) from clean.refund r where r.order_detail_id = ob.order_detail_id),0) tax1_amount,
      (nvl(sum(ob.tax2_amount),0)) - nvl((select sum (tax2_amount) from clean.refund r where r.order_detail_id = ob.order_detail_id),0) tax2_amount,
      (nvl(sum(ob.tax3_amount),0)) - nvl((select sum (tax3_amount) from clean.refund r where r.order_detail_id = ob.order_detail_id),0) tax3_amount,
      (nvl(sum(ob.tax4_amount),0)) - nvl((select sum (tax4_amount) from clean.refund r where r.order_detail_id = ob.order_detail_id),0) tax4_amount,
      (nvl(sum(ob.tax5_amount),0)) - nvl((select sum (tax5_amount) from clean.refund r where r.order_detail_id = ob.order_detail_id),0) tax5_amount,
      sum(ob.tax1_rate) tax1_rate, sum(ob.tax2_rate) tax2_rate, sum(ob.tax3_rate) tax3_rate, sum(ob.tax4_rate) tax4_rate, sum(ob.tax5_rate) tax5_rate
      FROM    clean.order_bills ob   
      WHERE   ob.order_detail_id = IN_ORDER_DTL_ID GROUP BY ob.additional_bill_indicator, ob.order_detail_id;
        
    OPEN OUT_DISP_CUR FOR
      select distinct tax1_name, tax1_description, tax2_name, 
      tax2_description, tax3_name, tax3_description, tax4_name, 
      tax4_description, tax5_name, tax5_description
      from clean.order_bills ob1 where ob1.order_detail_id = IN_ORDER_DTL_ID;
     
END GET_ORDER_ITEM_REMAINING_TAX;

PROCEDURE GET_STATECODE (
	IN_MESSAGE_ID               IN VARCHAR2,
  IN_MESSAGE_TYPE                IN VARCHAR2,
	OUT_STATE 			OUT 	VARCHAR2
)  
AS
  BEGIN
     IF IN_MESSAGE_TYPE = 'Mercury' THEN
                SELECT distinct CITY_STATE_ZIP into OUT_STATE  FROM MERCURY.MERCURY WHERE MERCURY_ID = IN_MESSAGE_ID;
     ELSE 
               SELECT distinct STATE into OUT_STATE FROM VENUS.VENUS WHERE VENUS_ID = IN_MESSAGE_ID;

END IF;
END GET_STATECODE;


PROCEDURE  GET_ORDER_DETAIL_ID 
(
  IN_GUID_ID IN CLEAN.ORDER_DETAILS.ORDER_GUID%TYPE,
  OUT_ORDER_DETAIL_ID out CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
) AS
BEGIN
  SELECT ORDER_DETAIL_ID
  INTO OUT_ORDER_DETAIL_ID
  FROM CLEAN.ORDER_DETAILS
  WHERE ORDER_GUID = IN_GUID_ID and ROWNUM=1;
--DBMS_OUTPUT.PUT_LINE('order detail id is '||OUT_ORDER_DETAIL_ID);
EXCEPTION 
  WHEN NO_DATA_FOUND THEN OUT_ORDER_DETAIL_ID := NULL;
 
END GET_ORDER_DETAIL_ID;

Procedure Order_Legacy_Id (
	IN_ORDER_DETAIL_ID In Clean.Order_Details.Order_Detail_Id%Type,
  Out_Legacy_Id Out Varchar2
) As
Begin

Select Legacy_Id 
Into Out_Legacy_Id 
From Clean.Order_Details
WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID;
Exception
  When No_Data_Found Then Out_Legacy_Id := Null;

END Order_Legacy_Id;

FUNCTION GET_ORDER_DETAIL_STATUS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2

AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning if the cart is on hold.
        If any one order in the cart is on hold, the entire cart is on hold.

Input:
        order_detail_id         number

Output:
        Y/N

-----------------------------------------------------------------------------*/

v_status       varchar2(100);
v_temp         varchar2(100);

CURSOR red_cur IS
    select  'Y'
    from order_hold oh
    where oh.order_detail_id = in_order_detail_id
    or exists (select 'Y'
        from clean.queue q
        where q.order_detail_id = in_order_detail_id
        and q.queue_type in ('ZIP', 'REJ', 'FTD', 'CX'));

CURSOR yellow_cur IS
    select  'Y'
    from queue q
    where q.order_detail_id = in_order_detail_id
    and q.queue_type not in ('ZIP', 'REJ', 'FTD', 'CX', 'CON');

BEGIN

    OPEN red_cur;
    FETCH red_cur INTO v_temp;
    CLOSE red_cur;

    if v_temp = 'Y' then
        v_status := 'red';
    else
        OPEN yellow_cur;
        FETCH yellow_cur INTO v_temp;
        CLOSE yellow_cur;
        
        if v_temp = 'Y' then
            v_status := 'yellow';
        else
            v_status := 'green';
        end if;
        
    end if;
    
    RETURN v_status;

END GET_ORDER_DETAIL_STATUS;

PROCEDURE GET_PTN_ORDER_INFO (
  IN_ORDER_NUMBER                   IN VARCHAR2, 
  OUT_PTN_ORD_CUR                   OUT TYPES.REF_CURSOR,
  OUT_BUYER_CUR                     OUT TYPES.REF_CURSOR,
  OUT_ORD_DTL_CUR                   OUT TYPES.REF_CURSOR,
  OUT_ORD_ADDON_CUR                 OUT TYPES.REF_CURSOR,
  OUT_ORD_TOTAL_CUR                 OUT TYPES.REF_CURSOR,
  OUT_ORD_REFUND_CUR                OUT TYPES.REF_CURSOR,
  OUT_ORD_SHIP_INFO_CUR             OUT TYPES.REF_CURSOR,
  OUT_FLR_ORD_STAT_INFO_CUR         OUT TYPES.REF_CURSOR
) AS
/*---------------------------------------------------------------------------------
  DESCRIPTION: 
  Get the complete partner order info. 
  
  Note that IN_ORDER_NUMBER can be either the external order number or the master order number.
  
 ----------------------------------------------------------------------------------*/
 
v_order_guid                        clean.order_details.order_guid%type;  
v_all_items_processed               char;

BEGIN

  v_order_guid := null;
  v_all_items_processed := 'N';
  
  BEGIN
    select distinct order_guid into v_order_guid from scrub.order_details where external_order_number = IN_ORDER_NUMBER;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    select distinct order_guid into v_order_guid from scrub.orders where master_order_number = IN_ORDER_NUMBER;
  END;
  
  BEGIN
    select distinct 'Y' into v_all_items_processed from scrub.order_details where order_guid = v_order_guid and status  in ('2007', '2006');    
    EXCEPTION WHEN NO_DATA_FOUND THEN
      v_all_items_processed := 'N';      
  END;
  
  -- Get the partner header info with/withour partner order details
  OPEN OUT_PTN_ORD_CUR FOR
      select distinct o.master_order_number, o.order_guid, o.order_date, o.order_origin, o.source_code, 
                od.external_order_number, od.order_detail_id,  
                CASE od.status WHEN '2007' THEN 'Y' WHEN '2006' THEN 'Y' ELSE 'N' END is_clean_order,
                po.partner_order_number, pod.partner_order_item_number, pm.partner_id      
    from scrub.orders o, scrub.order_details od  
      left join ptn_pi.partner_order_detail pod on pod.confirmation_number = od.external_order_number
      left join ptn_pi.partner_order po on po.partner_order_id = pod.partner_order_id
    , ptn_pi.partner_mapping pm
    where o.order_guid = od.order_guid 
      and od.order_guid = v_order_guid              
      and o.order_origin = pm.ftd_origin_mapping and o.source_code = pm.default_source_code;      
   
   -- Get the Buyer info from scrub when order is partially processed   
   IF v_all_items_processed = 'Y' THEN 
      OPEN OUT_BUYER_CUR FOR
        SELECT  c.first_name, c.last_name, c.business_name, 
            c.address_type, c.address_1, c.address_2, c.city, c.state, c.zip_code, c.country,
            (select em.email_address from clean.email em where em.email_id = o.email_id) buyer_email_address, 
            p.phone_number, p.phone_type, p.extension
        FROM clean.customer c
          JOIN clean.orders o ON c.customer_id = o.customer_id AND  o.order_guid = v_order_guid
          LEFT JOIN clean.customer_phones p on p.customer_id = o.customer_id; 
    ELSE 
      OPEN OUT_BUYER_CUR FOR
        SELECT B.first_name, B.last_name, null business_name, 
          ba.address_type, BA.ADDRESS_LINE_1 address_1, BA.ADDRESS_LINE_2 address_2, BA.CITY, BA.STATE_PROVINCE state, BA.POSTAL_CODE zip_code, BA.COUNTRY, 
          BP.PHONE_NUMBER, BP.PHONE_TYPE, BP.EXTENSION, 
          BE.EMAIL buyer_email_address
        from scrub.buyers b 
          join scrub.orders o on  B.BUYER_ID = O.BUYER_ID 
          left join scrub.BUYER_ADDRESSES BA on  BA.BUYER_ID = o.BUYER_ID
          left join scrub.BUYER_PHONES bp ON BP.BUYER_ID = o.BUYER_ID
          left join scrub.BUYER_EMAILS be  ON BE.BUYER_EMAIL_ID = O.BUYER_EMAIL_ID
        where O.ORDER_GUID = v_order_guid;
    END IF;
    
    -- Get the order Detail info from scrub when order is not yet processed and from clean when order is processed
    OPEN OUT_ORD_DTL_CUR FOR 
      select od.order_detail_id, od.order_disp_code status, to_char(od.ship_date, 'mm/dd/yyyy') ship_date, to_char(od.delivery_date, 'mm/dd/yyyy') delivery_date,To_Char(Od.Delivery_Date_Range_End, 'mm/dd/yyyy') Delivery_Date_Range_End,od.special_instructions, 
        (select occ.description from ftd_apps.occasion occ where occ.occasion_id = od.occasion) occasion_description, od.card_message,
        pm.product_id, pm.product_type, pm.product_sub_type, pm.product_name, pm.novator_id, pm.novator_name, pm.short_description, 
        global.global_pkg.GET_COLOR_BY_ID(od.color_1) color1_description,
        global.global_pkg.GET_COLOR_BY_ID(od.color_2) color2_description, 
        r.first_name, r.last_name, r.address_1, r.address_2, r.city, r.state, r.zip_code, r.country, r.address_type, r.business_name, rp.phone_type, rp.phone_number, rp.extension
      from clean.order_details od
        join ftd_apps.product_master pm on od.product_id = pm.product_id
        LEFT JOIN  clean.customer r on od.recipient_id = r.customer_id
        LEFT JOIN  clean.customer_phones rp on od.recipient_id = rp.customer_id
      where od.order_detail_id in (select order_detail_id from scrub.order_details where order_guid = v_order_guid and status in ('2006'))
      union all
      select od.order_detail_id, CASE od.status WHEN '2004' THEN 'REMOVED' WHEN '2005' THEN 'INVALID-PENDING' ELSE 'INVALID' END status, to_char(to_date(od.ship_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') ship_date, to_char(to_date(od.delivery_date, 'mm/dd/yyyy'), 'mm/dd/yyyy') delivery_date, To_Char(To_Date(Od.Delivery_Date_Range_End, 'mm/dd/yyyy'),'mm/dd/yyyy') Delivery_Date_Range_End,od.special_instructions, 
        (select occ.description from ftd_apps.occasion occ where occ.occasion_id = od.occasion_id) occasion_description, od.card_message,
        pm.product_id, pm.product_type, pm.product_sub_type, pm.product_name, pm.novator_id, pm.novator_name, pm.short_description, 
        global.global_pkg.GET_COLOR_BY_ID(od.color_1) color1_description,
        global.global_pkg.GET_COLOR_BY_ID(od.color_2) color2_description, 
        r.first_name, r.last_name, ra.address_line_1, ra.address_line_2, ra.city, ra.state_province state, ra.postal_code zip_code, ra.country, ra.address_type, null business_name, rp.phone_type, rp.phone_number, rp.extension
      from scrub.order_details od
        join ftd_apps.product_master pm on od.product_id = pm.product_id
        LEFT JOIN scrub.RECIPIENTS r on od.recipient_id = r.recipient_id
        LEFT JOIN scrub.RECIPIENT_ADDRESSES ra on od.recipient_id = ra.recipient_id
        LEFT JOIN scrub.RECIPIENT_PHONES rp on od.recipient_id = rp.recipient_id
      where od.order_detail_id in (select order_detail_id from scrub.order_details where order_guid = v_order_guid and status not in ('2006'));
    
    -- Get the Addon info from scrub when order is not yet processed
    OPEN OUT_ORD_ADDON_CUR FOR  
       SELECT  a.order_detail_id, a.order_add_on_id ADD_ON_ID, a.add_on_code, to_char(a.add_on_quantity) ADD_ON_QUANTITY, 
          to_char(nvl (fa.price, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'PRICE'))) price, to_char(a.add_on_discount_amount) add_on_discount_amount
        FROM clean.order_add_ons a
          LEFT OUTER JOIN    ftd_apps.addon_history fa ON a.add_on_history_id = fa.addon_history_id
        WHERE   a.order_detail_id IN (select order_detail_id from scrub.order_details where order_guid = v_order_guid and status in ('2006'))
        UNION ALL
        SELECT A.ORDER_DETAIL_ID, A.ADD_ON_ID, A.ADD_ON_CODE, A.ADD_ON_QUANTITY , A.ADD_ON_PRICE AS PRICE, a.add_on_discount_amount
        FROM    scrub.ADD_ONS A
          JOIN    scrub.ORDER_DETAILS OD ON A.ORDER_DETAIL_ID = OD.ORDER_DETAIL_ID
          LEFT OUTER JOIN FTD_APPS.ADDON FA ON A.ADD_ON_CODE = FA.ADDON_ID
        WHERE   OD.ORDER_GUID = v_order_guid and od.status not in ('2006');
    
    -- Get the Order Total info from scrub when order is not yet processed
     OPEN OUT_ORD_TOTAL_CUR FOR
       select  ob.order_detail_id,
            to_char(sum (ob.product_amount)) products_amount,
            to_char(sum (ob.add_on_amount)) add_on_amount,
            to_char(sum (ob.service_fee) + sum (ob.shipping_fee)) serv_ship_fee,
            to_char(sum (ob.discount_amount)) discount_amount,
            to_char(sum (ob.shipping_tax) + sum (ob.tax)) tax,
            to_char(sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee) + sum (ob.shipping_fee) - sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax)) order_total,
            to_char(sum (ob.miles_points_amt)) miles_points_amount,
            to_char(sum (ob.add_on_discount_amount)) add_on_discount_amount
        from    clean.order_bills ob
        where   ob.order_detail_id IN (select order_detail_id from scrub.order_details where order_guid = v_order_guid and status in ('2006')) group by ob.order_detail_id
        union all
        SELECT  od.order_detail_id, OD.PRODUCTS_AMOUNT, OD.ADD_ON_AMOUNT, 
            to_char(OD.SERVICE_FEE_AMOUNT + OD.SHIPPING_FEE_AMOUNT) serv_ship_fee,
            OD.DISCOUNT_AMOUNT, to_char(OD.TAX_AMOUNT+ OD.SHIPPING_TAX) tax, 
            to_char(OD.PRODUCTS_AMOUNT + OD.ADD_ON_AMOUNT + OD.SERVICE_FEE_AMOUNT + OD.SHIPPING_FEE_AMOUNT - OD.DISCOUNT_AMOUNT + OD.TAX_AMOUNT + OD.SHIPPING_TAX) order_total, 
            OD.MILES_POINTS_AMT, od.add_on_discount_amount 
        from scrub.order_details od 
        where od. order_guid = v_order_guid and status not in ('2006');
      
      -- Get the Order refund info from clean for processed orders and from scrub when order is removed 
      OPEN OUT_ORD_REFUND_CUR FOR
        SELECT r.order_detail_id,  to_char(sum (r.refund_product_amount)) products_amount,
            to_char(sum (r.refund_addon_amount)) add_on_amount,
            to_char(sum (r.refund_service_fee + r.refund_shipping_fee)) serv_ship_fee,
            to_char(sum (r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax)) tax,
            to_char(sum (r.refund_product_amount + r.refund_addon_amount + r.refund_service_fee + r.refund_shipping_fee + +r.refund_service_fee_tax + r.refund_shipping_tax + r.refund_tax - r.refund_discount_amount)) refund_total,
            to_char(sum (r.refund_discount_amount)) discount_amount,
            to_char(sum (p.miles_points_debit_amt)) miles_points_amount,
            to_char(sum (r.refund_addon_discount_amt)) addon_discount_amount
          FROM    clean.refund r
            JOIN    clean.payments p ON      p.refund_id = r.refund_id
          WHERE   r.order_detail_id IN (select order_detail_id from scrub.order_details where order_guid = v_order_guid and status in ('2006', '2007')) group by r.order_detail_id
          union
          SELECT  od.order_detail_id, OD.PRODUCTS_AMOUNT, OD.ADD_ON_AMOUNT, 
            to_char(OD.SERVICE_FEE_AMOUNT + OD.SHIPPING_FEE_AMOUNT) serv_ship_fee,
            to_char(OD.TAX_AMOUNT+ OD.SHIPPING_TAX) tax, 
            to_char(OD.PRODUCTS_AMOUNT + OD.ADD_ON_AMOUNT + OD.SERVICE_FEE_AMOUNT + OD.SHIPPING_FEE_AMOUNT - OD.DISCOUNT_AMOUNT + OD.TAX_AMOUNT + OD.SHIPPING_TAX) refund_total, 
            OD.DISCOUNT_AMOUNT, OD.MILES_POINTS_AMT, od.add_on_discount_amount 
          from scrub.order_details od 
          where od. order_guid = v_order_guid and status in ('2004');
      
      -- Get the SHIP INFO info from venus      
       OPEN OUT_ORD_SHIP_INFO_CUR FOR
          select distinct cod.order_detail_id, vv.venus_order_number, vv.venus_id, vv.msg_type, vv.ship_date, 
          vv.comments, vv.shipping_system, vv.tracking_number, vv.printed, vv.shipped, vv.cancelled, vv.rejected, 
          vv.SDS_STATUS order_status, null shipper_number, vv.price vendor_price, vv.FINAL_CARRIER, Vc.carrier_name
          from clean.order_details cod
          left join venus.venus vv on vv.reference_number = to_char(cod.order_detail_id) and vv.msg_type='FTD'
          Left join VENUS.Carriers Vc on vv.FINAL_CARRIER = Vc.Carrier_id 
          and vv.venus_order_number not in (
            select v1.venus_order_number 
            from venus.venus v1 
            where to_char(cod.order_detail_id)=v1.reference_number and v1.msg_type in('CAN','REJ')
           )
          where cod.order_guid=v_order_guid; 
        
         -- Get the FLORAL confirmation info from venus    
        OPEN OUT_FLR_ORD_STAT_INFO_CUR FOR 
          select distinct cod.order_detail_id, mm.MERCURY_MESSAGE_NUMBER, mm.MERCURY_ORDER_NUMBER,mm.mercury_id, mm.msg_type, mm.comments,
            cod.DELIVERY_DATE, mm.mercury_status, mm.sak_text, mm.responsible_florist, null shipper_number, mm.price florist_price,
         	(select losv.order_status_desc from mercury.lifecycle_status ls, mercury.lifecycle_order_status_val losv
 					where ls.mercury_id = mm.mercury_id and losv.order_status_code = ls.order_status_code
                                    and ls.status_timestamp = (select max(ls1.status_timestamp) from Mercury.Lifecycle_Status Ls1
                                        Where Ls1.Mercury_Id = mm.mercury_id) and rownum = 1) order_status
            from clean.order_details cod
            left join mercury.mercury mm on mm.reference_number = to_char(cod.order_detail_id) and mm.msg_type='FTD'
            and mm.MERCURY_ORDER_NUMBER not in (
              select m1.MERCURY_ORDER_NUMBER 
              from mercury.mercury m1 
              where to_char(cod.order_detail_id)=m1.reference_number and m1.msg_type in('CAN','REJ')
            )
            where cod.order_guid=v_order_guid;        
           
END GET_PTN_ORDER_INFO;

FUNCTION GET_LIFECYCLE_DELIVERED_STATUS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

v_status       varchar2(100);

BEGIN

    select 'Y'
    into v_status
    from mercury.lifecycle_status ls
    join mercury.mercury m
    on m.reference_number = to_char(in_order_detail_id)
    and m.msg_type = 'FTD'
    and m.created_on = (select max(m1.created_on)
        from mercury.mercury m1
        where m1.reference_number = m.reference_number
        and m1.msg_type = 'FTD')
    where ls.mercury_id = m.mercury_id
    and ls.order_status_code = 'C';

    RETURN v_status;
    
END GET_LIFECYCLE_DELIVERED_STATUS;

PROCEDURE GET_RECIPIENT_TIME
(
IN_ORDER_DETAIL_ID  IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_TIME             IN DATE,
OUT_CUR             OUT TYPES.REF_CURSOR
) AS

v_return_time    date := in_time;
v_time_zone      varchar2(10);

BEGIN

    select sm.time_zone
    into v_time_zone
    from clean.order_details od
    join clean.customer c
    on c.customer_id = od.recipient_id
    left outer join ftd_apps.state_master sm
    on sm.state_master_id = c.state
    where od.order_detail_id = in_order_detail_id;
    
    case v_time_zone
        when '1' then
            v_return_time := v_return_time + INTERVAL '1' HOUR;
        when '2' then
            v_return_time := v_return_time;
        when '3' then
            v_return_time := v_return_time - INTERVAL '1' HOUR;
        when '4' then
            v_return_time := v_return_time - INTERVAL '2' HOUR;
        when '5' then
            v_return_time := v_return_time - INTERVAL '3' HOUR;
        when '6' then
            v_return_time := v_return_time - INTERVAL '4' HOUR;
        else
            v_return_time := v_return_time;
    end case;

    OPEN OUT_CUR FOR
        select v_return_time as "return_time"
        from dual;
        
END GET_RECIPIENT_TIME;

PROCEDURE GET_MERCURY_ORDER_NUMBER
(
 IN_ORDER_DETAIL_ID   IN VARCHAR2,
 OUT_CURSOR        OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will retrieve most recent Mercury order number for the order
except
        
Input:
        order_detail_id

Output:
        cursor containing orders info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN OUT_CURSOR FOR
   select
    mercury_order_number 
   from mercury.mercury 
   where created_on = (select max(created_on) from mercury.mercury where reference_number = IN_ORDER_DETAIL_ID and msg_type = 'FTD');

END GET_MERCURY_ORDER_NUMBER;

PROCEDURE GET_LEGACY_SOURCE_CODE
(
 IN_ORDER_DETAIL_ID     IN ORDER_DETAILS.ORDER_DETAIL_ID%type,
 OUT_SOURCE_CODE        OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will source code for legacy partner based on the following criteria match
          * Funeral Name
          * Zipcode
          * Phone number
          * Address
        
Input:
        order_detail_id
        
Output:
        cursor containing orders info

-----------------------------------------------------------------------------*/

v_address           varchar2(200);
v_funeral_name      varchar2(100);
v_zipcode           varchar2(10);
v_phone             varchar2(20);
v_recipient_id      number;
v_order_guid        varchar2(20);
legacy_data_found   varchar2(1) := 'N';


BEGIN

  SELECT ORDER_GUID,RECIPIENT_ID
  	INTO v_order_guid,v_recipient_id
  FROM CLEAN.ORDER_DETAILS
  WHERE ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID; 

  SELECT 'Y'
      INTO legacy_data_found
  FROM CLEAN.ORDERS O,
       FTD_APPS.SOURCE_MASTER SM
  WHERE O.ORDER_GUID = v_order_guid
  AND O.SOURCE_CODE = SM.SOURCE_CODE
  AND SM.SOURCE_TYPE like '%LEGACY%';

  SELECT c.business_name,c.zip_code,p.phone_number,c.address_1 || c.address_2
    INTO v_funeral_name,v_zipcode,v_phone,v_address
  FROM CLEAN.CUSTOMER C,
       CLEAN.CUSTOMER_PHONES P
  WHERE p.customer_id = c.customer_id
  AND C.customer_id = v_recipient_id
  AND p.phone_type = 'Day';

  SELECT
    source_code INTO OUT_SOURCE_CODE
   FROM ftd_apps.source_master sm
   WHERE legacy_data_found = 'Y'
   AND SUBSTR(sm.description,1,20) = SUBSTR(v_funeral_name,1,20)
   AND FTD_APPS.FLORIST_QUERY_PKG.FORMAT_ZIP_CODE_II(sm.recpt_zip_code) = FTD_APPS.FLORIST_QUERY_PKG.FORMAT_ZIP_CODE_II(v_zipcode)
   AND regexp_replace(sm.recpt_phone,'-','') = regexp_replace(v_phone,'-','')
   AND regexp_replace(sm.recpt_address,'[^0-9]','') = regexp_replace(v_address,'[^0-9]','')
   AND sm.SOURCE_TYPE like '%LEGACY%'
   AND ROWNUM = 1;

EXCEPTION WHEN NO_DATA_FOUND THEN

      OUT_SOURCE_CODE := null;

END GET_LEGACY_SOURCE_CODE;

PROCEDURE GET_CARDINAL_COMMERCE_DATA
(
 IN_PAYMENT_ID   IN   PAYMENTS.PAYMENT_ID%TYPE,
 OUT_CURSOR      OUT  TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns CARDINAL COMMERCE tokens received from website for the given payment id.

Input:
        payment_id    VARCHAR2

Output:
        cursor containing cardinal commerce tokens

-----------------------------------------------------------------------------*/

V_ECI  VARCHAR2(10) := '';
V_CAVV VARCHAR2(100) := '';
V_XID  VARCHAR2(100) := '';
V_UCAF VARCHAR2(10) := '';
V_CARDINAL_VERIFIED_FLAG VARCHAR2(1);

BEGIN

       BEGIN
          SELECT CARDINAL_VERIFIED_FLAG INTO V_CARDINAL_VERIFIED_FLAG FROM PAYMENTS WHERE PAYMENT_ID = IN_PAYMENT_ID;
          
  	      IF V_CARDINAL_VERIFIED_FLAG = 'Y' THEN
		       select AUTH_PROPERTY_VALUE into V_ECI from payments_ext where payment_id = IN_PAYMENT_ID and UPPER(AUTH_PROPERTY_NAME) = 'ECI';
		       select AUTH_PROPERTY_VALUE into V_CAVV from payments_ext where payment_id = IN_PAYMENT_ID and UPPER(AUTH_PROPERTY_NAME) = 'CAVV';
		       select AUTH_PROPERTY_VALUE into V_XID from payments_ext where payment_id = IN_PAYMENT_ID and UPPER(AUTH_PROPERTY_NAME) = 'XID';
		      BEGIN
                  select AUTH_PROPERTY_VALUE into V_UCAF from payments_ext where payment_id = IN_PAYMENT_ID and UPPER(AUTH_PROPERTY_NAME) = 'UCAF';
              EXCEPTION WHEN OTHERS THEN 
                  V_UCAF := '';
              END;
	      END IF;
	   
	   EXCEPTION WHEN OTHERS THEN
	   	   V_ECI  := '';
		   V_CAVV := '';
		   V_XID  := '';
		   
           V_CARDINAL_VERIFIED_FLAG := 'N';
	   END;
		   
	OPEN out_cursor FOR
      
      SELECT V_ECI AS ECI, V_CAVV AS CAVV, V_XID AS XID, V_UCAF AS UCAF FROM DUAL; 

END GET_CARDINAL_COMMERCE_DATA;


FUNCTION CONVERT_TO_VALID_SQL_CLAUSE_II
(
IN_LIST IN VARCHAR2
)RETURN VARCHAR2 
AS
/*-----------------------------------------------------------------------------
Description:
	  Returns a comma delimited list of strings for use in a SQL statement
    
Input:
    IN_LIST - comma delimited list of Java Strings

Output:
    - VARCHAR2 containing a comma separate list of text

-----------------------------------------------------------------------------*/

v_return_string VARCHAR2(32767);
v_list VARCHAR2(32767);
v_comma_pos NUMBER(3);
v_value VARCHAR2(100);
v_value_plus_comma VARCHAR(100);

BEGIN
    v_list := in_list;
    
    WHILE INSTR(v_list,',') <> 0 LOOP
  	    v_comma_pos := INSTR(v_list,',');
  	    v_value := TRIM(SUBSTR(v_list,1,v_comma_pos-1));
  	    v_value_plus_comma := ''''||v_value||''',';
  	    v_return_string := v_return_string || v_value_plus_comma;
        v_list := SUBSTR(v_list, v_comma_pos+1);
    END LOOP;
    
    v_value := RTRIM(ltrim(v_list,' '),' ');
    v_return_string := v_return_string ||''''||v_value||'''';
    
    RETURN v_return_string;
  END CONVERT_TO_VALID_SQL_CLAUSE_II;



PROCEDURE GET_CREDITCARD_FNAME_LNAME
(
 IN_CUSTOMER_ID   IN CREDIT_CARDS.CUSTOMER_ID%TYPE,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns FIRSTNAME and LASTNAME for the given credit card id.

Input:
        IN_CUSTOMER_ID    VARCHAR2

Output:
        cursor containing first and last name of credit card

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
  	SELECT 
	  C.CUSTOMER_ID,
	  C.FIRST_NAME,
	  C.LAST_NAME,
	  C.ADDRESS_1,
	  C.ADDRESS_2,
	  C.CITY,
	  C.STATE,
	  C.ZIP_CODE,
	  C.COUNTRY,
	  E.EMAIL_ADDRESS 
	FROM
	  CLEAN.CUSTOMER C,
	  CLEAN.CUSTOMER_EMAIL_REF CE,
	  CLEAN.EMAIL E 
	WHERE C.CUSTOMER_ID = IN_CUSTOMER_ID 
	  AND C.CUSTOMER_ID = CE.CUSTOMER_ID 
	  AND CE.EMAIL_ID = E.EMAIL_ID 
	ORDER BY E.UPDATED_ON DESC ;

END GET_CREDITCARD_FNAME_LNAME;


PROCEDURE GET_ORDERS_ORIGIN
(
 IN_ORDER_GUID			IN  ORDERS.ORDER_GUID%TYPE,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns ORDER ORIGIN for the given Order guid.

Input:
        IN_ORDER_GUID    VARCHAR2

Output:
        cursor containing ORIGIN of the orders

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
  	select ORIGIN_ID from ORDERS where ORDER_GUID = IN_ORDER_GUID;

END GET_ORDERS_ORIGIN;


FUNCTION IS_PG_ROUTE_TRANSACTION
(
 IN_PAYMENT_ROUTE       IN   PAYMENTS.route%TYPE,
  IN_ORIGIN_ID          IN   ORDERS.ORIGIN_ID%TYPE
)RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns PG_ROUTE_FLAG whether Payment is going to be routed to Payment Gateway or not.

Input:
        IN_PAYMENT_ROUTE    VARCHAR2
        IN_ORIGIN_ID		VARCHAR2

Return:
        flag value indicates whether the new route or existing.

-----------------------------------------------------------------------------*/
v_is_pg_route varchar2(1);
v_route_string varchar2(100);
v_route_sql_string varchar2(100);
v_payment_route varchar2(20);

BEGIN
         
        select value into v_route_string from FRP.GLOBAL_PARMS where name = 'PAYMENT_GATEWAY_ROUTES';
        v_route_sql_string := CONVERT_TO_VALID_SQL_CLAUSE_II(v_route_string);
        
        v_is_pg_route :='N';
         v_payment_route := ''|| IN_PAYMENT_ROUTE ||'';
        
        EXECUTE IMMEDIATE 'SELECT decode(count (1), 1, ''Y'', ''N'') 
         FROM DUAL
         WHERE '''|| IN_PAYMENT_ROUTE ||''' in (' || v_route_sql_string || ')'
         into v_is_pg_route;
        
    RETURN v_is_pg_route;
	     
END IS_PG_ROUTE_TRANSACTION;


END;
.
/
