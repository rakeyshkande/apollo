 
CREATE OR REPLACE TRIGGER clean.pmt_after_stmt_trigger 
AFTER UPDATE of bill_status ON clean.payments 
BEGIN 
   payments_trigger_pkg.after_stmt_trigger; 
END; 
/