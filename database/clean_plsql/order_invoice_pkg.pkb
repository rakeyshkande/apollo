CREATE OR REPLACE
PACKAGE BODY clean.ORDER_INVOICE_PKG AS

PROCEDURE ARIBA_INVOICE_CHECKER 
AS
/*----------------------------------------------------------------------------------------

   Procedure to determine which Ariba orders are eligible for invoicing 
   and simply mark them as Ready for invoice processing, or mark as N/A if fully refunded.
   A different process will look for orders in Ready state, then mark as Sent once
   the invoice has been fed to PI for processing. 

------------------------------------------------------------------------------------------*/
V_INVOICE_DAYS_PAST_DELIVERY NUMBER;

-- Cursor to find Ariba carts eligible for invoicing
--
CURSOR invoice_cur (p_days_past_delivery number) IS
    select distinct o.master_order_number
    from clean.orders o
    join clean.order_details od on od.order_guid=o.order_guid
    join clean.payments p on p.order_guid=o.order_guid
    join clean.order_bills ob on ob.order_detail_id=od.order_detail_id
    join ptn_pi.partner_mapping pm on pm.default_source_code=od.source_code
    where o.origin_id = 'ARI'
    and   p.payment_type = 'IN'
    and   ob.bill_status in ('Billed','Settled')
    and   pm.send_ord_invoice = 'Y'
    -- Don't include orders already invoiced, but include if still in Ready state since order may have been cancelled/refunded since then
    and   (o.invoice_status is null or o.invoice_status = 'Ready')
    and   NVL(od.delivery_date_range_end, od.delivery_date) between trunc(sysdate-90) and trunc(sysdate - p_days_past_delivery)
    -- Make sure that any other orders in same cart are also 8 days past delivery (if not, don't include any orders from cart) 
    and   (select count(od2.order_guid) from clean.order_details od2 where 
           od2.ORDER_GUID=od.ORDER_GUID and NVL(od2.delivery_date_range_end, od2.delivery_date) > trunc(sysdate - p_days_past_delivery)) = 0;

BEGIN
    SELECT value INTO V_INVOICE_DAYS_PAST_DELIVERY 
    FROM frp.global_parms 
    WHERE context='PI_CONFIG' AND name='ARIBA_INVOICE_DAYS_PAST_DELIVERY';
 
    -- Loop over eligible carts marking them ready for invoice processing (unless cart is fully refunded)
    --
    FOR cart IN invoice_cur(v_invoice_days_past_delivery) LOOP
       IF CLEAN.REFUND_PKG.IS_FULLY_REFUNDED_ORDER(cart.master_order_number) = 'Y' THEN
          update clean.orders set invoice_status = 'N/A', updated_on=sysdate, updated_by='ariba_invoice_checker'
          where  master_order_number=cart.master_order_number;
       ELSE
          update clean.orders set invoice_status = 'Ready', updated_on=sysdate, updated_by='ariba_invoice_checker'
          where  master_order_number=cart.master_order_number;
       END IF;
    END LOOP;     

END ARIBA_INVOICE_CHECKER;



PROCEDURE START_ARIBA_INVOICING 
AS
/*----------------------------------------------------------------------------------------

   Procedure to initiate the Ariba invoice process.

------------------------------------------------------------------------------------------*/
v_queue   varchar2(100) := 'OJMS.PARTNER_ORDERS';
v_corrid  varchar2(100) := 'PROCESS_INVOICE_READY_ORDERS';
v_delay   number        := 60;
v_status  varchar2(1);
v_msg     varchar2(256);

BEGIN

   ARIBA_INVOICE_CHECKER;
   events.post_a_message_flex(v_queue, v_corrid, v_corrid, v_delay, v_status, v_msg);

END START_ARIBA_INVOICING;



PROCEDURE START_SEND_INVOICE_FEEDS 
AS
/*----------------------------------------------------------------------------------------

   Procedure to initiate the sending of invoicing feeds to the ESB

------------------------------------------------------------------------------------------*/
v_queue   varchar2(100) := 'OJMS.PARTNER_ORDERS';
v_corrid  varchar2(100) := 'PROCESS_SEND_INVOICE_READY_ORDERS';
v_delay   number        := 60;
v_status  varchar2(1);
v_msg     varchar2(256);

BEGIN

   events.post_a_message_flex(v_queue, v_corrid, v_corrid, v_delay, v_status, v_msg);

END START_SEND_INVOICE_FEEDS;



PROCEDURE GET_ARIBA_ORDERS_TO_INVOICE
(
OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------

   Procedure to get all Ariba orders that are ready to be invoiced

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT master_order_number
      FROM clean.orders
     WHERE invoice_status = 'Ready';

END GET_ARIBA_ORDERS_TO_INVOICE;



PROCEDURE UPDATE_INVOICE_STATUS_TO_SENT
(
IN_MASTER_ORDER_NUMBER  ORDERS.MASTER_ORDER_NUMBER%TYPE
)
AS
/*-----------------------------------------------------------------------------

   Procedure to update the invoice status to SentToPI for a cart.
   Status can be:
      Ready       Order(s) in cart meet criteria and ready for invoicing.
      SentToPI    Invoice has been sent to PI for processing and transmission to Ariba.
      N/A         Invoice was not required since cancelled or fully refunded.
    
-----------------------------------------------------------------------------*/
BEGIN

  UPDATE   clean.orders
     SET   invoice_status = 'SentToPI'
     WHERE master_order_number = in_master_order_number;

END UPDATE_INVOICE_STATUS_TO_SENT;


END;
.
/