
create or replace PROCEDURE      clean.sku_and_commission_acctg_rpts 
  (IN_START_DATE          IN    DATE,
   IN_END_DATE            IN    DATE,
   OUT_STATUS             OUT   VARCHAR2,
   OUT_MESSAGE            OUT   VARCHAR2)
AS
----------------------------------------------------------------------------------
-- Proc    : Monthly merchandising reports:
--
-- Descr   : Produce and email 2 reports:
--   *  SKU's delivered showing Merchandise amount by date range (monthly accounting)
--   *  ME Data pull for commission
--
----------------------------------------------------------------------------------

   sku_deliv_rec_cnt  number := 0;
   v_sku_rpt_name     varchar2(100) := 'SKUs_Delivered_Report';

   comm_rec_cnt       number := 0;
   v_comm_rpt_name    varchar2(100) := 'Commission_Data_Pull';

   v_first_day_of_last_month date  := to_date(to_char(add_months(sysdate,-1),'YYYYMM') || '01','YYYYMMDD');
   v_last_day_of_last_month  date  := last_day(add_months(sysdate,-1));
   v_start_date              date;
   v_end_date                date;
   
   
   -- Mail and utl_file variables
   conn               UTL_SMTP.CONNECTION; --utl_smtp.connection;--
   crlf               VARCHAR2(2) := chr(13) || chr(10);
   v_file_handle     utl_file.file_type;
   v_file_handle_2   utl_file.file_type;
   v_line            VARCHAR2(5000);
   mesg              VARCHAR2(32767);

      
   ----------------------------------------------------------------------------------
   -- REPORT CURSORS
   ----------------------------------------------------------------------------------
   cursor rep1 is 
      select distinct
          od.external_order_number                   || ',' ||    
          od.product_id                              || ',' ||
          to_char(od.eod_delivery_date,'MM/DD/YYYY') || ',' ||
          ob.product_amount   rep_rec
      from clean.order_details od
      join clean.order_bills ob on od.order_detail_id = ob.order_detail_id
      join clean.payments p on od.order_guid = p.order_guid
      where ob.additional_bill_indicator = 'N'
        and p.payment_type <> 'NC'
        and p.additional_bill_id is null
        and p.refund_id is null
        and (od.product_id like 'T5%' 
          or od.product_id like 'T6%' 
          or od.product_id like 'V85%' 
          or od.product_id like 'TP%'
          or od.product_id like 'VF%'
          or od.product_id like 'VP%' 
          or od.product_id like 'V0%' 
          or od.product_id like 'V86%' 
          or od.product_id like 'V87%'
          or od.product_id like 'V88%' 
          or od.product_id like 'V89%' 
          or od.product_id like 'V9%' 
          or od.product_id like 'TB%')
        and trunc(od.eod_delivery_date) >= v_start_date 
        and trunc(od.eod_delivery_date) <= v_end_date;


   cursor rep2 is
      select 
        SKU                        || ',' ||
        CustomerZip                || ',' ||
        count(distinct Orders_Det) || ',' ||
        Sum(ProductAmount_Det) rep_rec
      from
      (select
          od.product_id SKU,
          c.zip_code CustomerZip,
           od.external_order_number  Orders_Det,
          ob.product_amount   ProductAmount_Det
      from clean.order_details od
      join clean.order_bills ob on od.order_detail_id = ob.order_detail_id
      join ( select order_guid, 
                    max(payment_type) payment_type , 
                    max(additional_bill_id) additional_bill_id, 
                    max(refund_id) refund_id  
             from clean.payments 
             where payment_type <> 'NC'  
             group by order_guid) p on od.order_guid = p.order_guid
      join clean.orders o on od.order_guid = o.order_guid
      join clean.customer c on o.customer_id = c.customer_id
      where ob.additional_bill_indicator = 'N'
        and p.payment_type <> 'NC'
        and p.additional_bill_id is null
        and p.refund_id is null
        and od.product_id in ('FU77', 'FU79', 'FU80', 'P595', 'P596', 'T613')
        and trunc(od.eod_delivery_date) >= v_start_date 
        and trunc(od.eod_delivery_date) <= v_end_date
      ) 
      group by SKU, CustomerZip;



   ----------------------------------------------------------------------------------
   -- Call this for each file to attach to the email
   ----------------------------------------------------------------------------------
   PROCEDURE attach_email(rpt_name IN VARCHAR2) AS
   BEGIN
      -- NOTE - The filename on the server will be rpt_name + ".csv".
      --        The filename attached to the email will be fpt_name + date _+ ".csv"
      --
        /* You must call begin_attachment and end_attachment */
        /* for any files you wish to attach to the message */
        /* The first example here is for a plain text attachment */
        /* It makes repeated calls to demo_mail.write_text */
     
        -- Add Date to the filename for the report
        ops$oracle.demo_mail.begin_attachment(conn => conn,
                                   mime_type => 'text/plain; charset=iso-8859-1',
                                   inline => TRUE,
                                   filename => rpt_name || '_'||to_char(sysdate,'YYYYMMDD')||'.csv',
                                   transfer_enc => '8 bit');

        v_file_handle_2 := utl_file.fopen(location => 'LOG_FILE_OUTPUT',
                                        filename => rpt_name || '.csv',
                                        open_mode => 'r',
                                        max_linesize => 5000);
        begin
           loop
              utl_file.get_line(v_file_handle_2, v_line, 5000);
              mesg := v_line || crlf;
              ops$oracle.demo_mail.write_text(conn => conn, message => mesg);
           end loop;
        exception when others then null;
        end;
        utl_file.fclose(v_file_handle_2);
        ops$oracle.demo_mail.end_attachment (conn => conn);
   END attach_email;


--##############################################################################
--## M A I N    L O G I C
--##############################################################################

begin

   --
   -- If no dates supplied, default to first and last dates of prior month
   --
   if in_start_date is not null then
      v_start_date := trunc(in_start_date);
   else
      v_start_date := trunc(v_first_day_of_last_month);
   end if;
      
   if in_end_date is not null then
      v_end_date := trunc(in_end_date);
   else
      v_end_date := trunc(v_last_day_of_last_month);
   end if;
      
     
   -------------------------------------------------------------------
   -- Create the SKUs Delivered Report
   -------------------------------------------------------------------

   -- The report saved on the server will be rpt_name + ".csv".  The one attached to the
   -- email will have a date attached to it (see attach_email proc).  The server file 
   -- will be overlayed each month so we don't have to worry about archiving.

   v_file_handle := utl_file.fopen(location => 'LOG_FILE_OUTPUT',
                                   filename => v_sku_rpt_name || '.csv',
                                   open_mode => 'w',
                                   max_linesize => 5002);

   --
   -- Loop through the SKU cursor and create a file
   --
   utl_file.put_line (file   => v_file_handle, 
                      buffer => 'Order Number, SKU, Delivered Date, Product Amount',
                      autoflush => TRUE);

   for x in rep1 loop
      sku_deliv_rec_cnt := sku_deliv_rec_cnt + 1;
      utl_file.put_line (file => v_file_handle, buffer => x.rep_rec, autoflush => TRUE);
   end loop;

   utl_file.fclose(v_file_handle);



   -------------------------------------------------------------------
   -- Create the Commission Data Pull Report
   -------------------------------------------------------------------

   -- The report saved on the server will be rpt_name + ".csv".  The one attached to the
   -- email will have a date attached to it (see attach_email proc).  The server file 
   -- will be overlayed each month so we don't have to worry about archiving.

   v_file_handle := utl_file.fopen(location => 'LOG_FILE_OUTPUT',
                                   filename => v_comm_rpt_name || '.csv',
                                   open_mode => 'w',
                                   max_linesize => 5002);

   --
   -- Loop through the SKU cursor and create a file
   --
   utl_file.put_line (file   => v_file_handle, 
                      buffer => 'SKU, Customer Zip, Orders, Product Amount',
                      autoflush => TRUE);

   for x in rep2 loop
      comm_rec_cnt := comm_rec_cnt + 1;
      utl_file.put_line (file => v_file_handle, buffer => x.rep_rec, autoflush => TRUE);
   end loop;

   utl_file.fclose(v_file_handle);



   -------------------------------------------------------------------
   -- Send the email
   -------------------------------------------------------------------

   conn := ops$oracle.demo_mail.begin_mail(
                    recipient_project => 'SKU_AND_COMMISSION_RPTS',
                    subject           => 'Accounting Reports for SKUs Delivered and Commission Data Pull', 
                    mime_type         => ops$oracle.demo_mail.MULTIPART_MIME_TYPE);
   --
   -- Send the body of the email
   --
   ops$oracle.demo_mail.attach_mb_text(
                            conn => conn,
                            data => '<left><h4><font color=blue>Two reports are attached for date range ' ||
                                                 to_char(v_start_date,'MM/DD/YYYY') || ' to ' ||
                                                 to_char(v_end_date,'MM/DD/YYYY') || ':' || 
                                    '<br>  *  SKUs delivered showing Merchandise amount by date range ' || 
                                    '<br>  *  Data pull for commission</font></h4></left>' ||
                                    'Number of data records in SKU file = ' || sku_deliv_rec_cnt ||
                                    '<br>Number of data records in Commission file = ' || comm_rec_cnt,
                            mime_type => 'text/html; charset=iso-8859-1' );
 
   --
   -- attach the files to the email
   --
   attach_email(v_sku_rpt_name);
   attach_email(v_comm_rpt_name);
 
   ops$oracle.demo_mail.end_mail(conn => conn);      /* Must call this for anything to work */

   out_status := 'Y';
   out_message := NULL;


exception
   when others then
      out_status := 'N';
      out_message := 'Unexpected Error - msg=' || sqlerrm;

end sku_and_commission_acctg_rpts;
/