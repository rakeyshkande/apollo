create or replace package body clean.order_processing as

PROCEDURE UPDATE_ORDER_DETAILS_PM
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
IN_OP_STATUS                    IN ORDER_DETAILS.OP_STATUS%TYPE,
IN_VENDOR_ID                    IN ORDER_DETAILS.VENDOR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record.

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number
        op_status                       varchar2
        vendor_id                    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  order_details
SET
        delivery_date = NVL(in_delivery_date, delivery_date),
        recipient_id = NVL(in_recipient_id, recipient_id),
        product_id = NVL(in_product_id, product_id),
        quantity = NVL(in_quantity, quantity),
        color_1 = NVL(in_color_1, color_1),
        color_2 = NVL(in_color_2, color_2),
        substitution_indicator = NVL(in_substitution_indicator, substitution_indicator),
        same_day_gift = NVL(in_same_day_gift, same_day_gift),
        occasion = NVL(in_occasion, occasion),
        card_message = NVL(in_card_message, card_message),
        card_signature = NVL(in_card_signature, card_signature),
        special_instructions = NVL(in_special_instructions, special_instructions),
        release_info_indicator = NVL(in_release_info_indicator, release_info_indicator),
        florist_id = NVL(in_florist_id, florist_id),
        ship_method = NVL(in_ship_method, ship_method),
        ship_date = NVL(in_ship_date, ship_date),
        order_disp_code = NVL(in_order_disp_code, order_disp_code),
        second_choice_product = NVL(in_second_choice_product, second_choice_product),
        zip_queue_count = NVL(in_zip_queue_count, zip_queue_count),
        updated_on = sysdate,
        updated_by = in_updated_by,
        delivery_date_range_end = NVL(in_delivery_date_range_end, delivery_date_range_end),
        scrubbed_on = NVL(in_scrubbed_on, scrubbed_on),
        scrubbed_by = NVL(in_scrubbed_by, scrubbed_by),
        ariba_unspsc_code = NVL(in_ariba_unspsc_code, ariba_unspsc_code),
        ariba_po_number = NVL(in_ariba_po_number, ariba_po_number),
        ariba_ams_project_code = NVL(in_ariba_ams_project_code, ariba_ams_project_code),
        ariba_cost_center = NVL(in_ariba_cost_center, ariba_cost_center),
        size_indicator = NVL(in_size_indicator, size_indicator),
        miles_points = NVL(in_miles_points, miles_points),
        subcode = NVL(in_subcode, subcode),
        source_code = NVL(in_source_code, source_code),
        reject_retry_count = NVL(in_reject_retry_count, reject_retry_count),
        op_status = NVL (in_op_status, op_status),
        vendor_id = NVL (in_vendor_id, vendor_id),
        delivery_date_received = sysdate
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN ORDER_PROCESSING.UPDATE_ORDER_DETAILS_PM [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAILS_PM;

FUNCTION GET_NEXT_MP_INTERNAL_AUTH_VAL
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This function returns the nextval from sequence mp_internal_auth_sq.
        
OUTPUT
	varchar2
------------------------------------------------------------------------------*/        
v_auth     varchar2 (20);
BEGIN

	 select mp_internal_auth_sq.nextval into v_auth from dual;
	 
RETURN v_auth;	 

END GET_NEXT_MP_INTERNAL_AUTH_VAL;

PROCEDURE UPDATE_ORDER_DETAILS_PC
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAILS.RECIPIENT_ID%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAILS.PRODUCT_ID%TYPE,
IN_QUANTITY                     IN ORDER_DETAILS.QUANTITY%TYPE,
IN_COLOR_1                      IN ORDER_DETAILS.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAILS.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAILS.SUBSTITUTION_INDICATOR%TYPE,
IN_SAME_DAY_GIFT                IN ORDER_DETAILS.SAME_DAY_GIFT%TYPE,
IN_OCCASION                     IN ORDER_DETAILS.OCCASION%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAILS.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAILS.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAILS.SPECIAL_INSTRUCTIONS%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAILS.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAILS.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_ORDER_DISP_CODE              IN ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
IN_SECOND_CHOICE_PRODUCT        IN ORDER_DETAILS.SECOND_CHOICE_PRODUCT%TYPE,
IN_ZIP_QUEUE_COUNT              IN ORDER_DETAILS.ZIP_QUEUE_COUNT%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SCRUBBED_ON                  IN ORDER_DETAILS.SCRUBBED_ON%TYPE,
IN_SCRUBBED_BY                  IN ORDER_DETAILS.SCRUBBED_BY%TYPE,
IN_ARIBA_UNSPSC_CODE            IN ORDER_DETAILS.ARIBA_UNSPSC_CODE%TYPE,
IN_ARIBA_PO_NUMBER              IN ORDER_DETAILS.ARIBA_PO_NUMBER%TYPE,
IN_ARIBA_AMS_PROJECT_CODE       IN ORDER_DETAILS.ARIBA_AMS_PROJECT_CODE%TYPE,
IN_ARIBA_COST_CENTER            IN ORDER_DETAILS.ARIBA_COST_CENTER%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAILS.SIZE_INDICATOR%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAILS.MILES_POINTS%TYPE,
IN_SUBCODE                      IN ORDER_DETAILS.SUBCODE%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAILS.SOURCE_CODE%TYPE,
IN_REJECT_RETRY_COUNT           IN ORDER_DETAILS.REJECT_RETRY_COUNT%TYPE,
IN_OP_STATUS                    IN ORDER_DETAILS.OP_STATUS%TYPE,
IN_VENDOR_ID                    IN ORDER_DETAILS.VENDOR_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the order detail record.

Input:
        order_detail_id                 number
        delivery_date                   date
        recipient_id                    number
        product_id                      varchar2
        quantity                        number
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        same_day_gift                   char
        occasion                        varchar2
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        release_info_indicator          char
        florist_id                      varchar2
        ship_method                     varchar2
        ship_date                       date
        order_disp_code                 varchar2
        second_choice_product           varchar2
        zip_queue_count                 number
        updated_by                      varchar2
        delivery_date_range_end         date
        scrubbed_on                     date
        scrubbed_by                     varchar2
        ariba_unspsc_code               varchar2
        ariba_po_number                 varchar2
        ariba_ams_project_code          varchar2
        ariba_cost_center               varchar2
        size_indicator                  varchar2
        miles_points                    number
        subcode                         varchar2
        reject_retry_count              number
        op_status                       varchar2
        vendor_id                    varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

UPDATE  order_details
SET
        delivery_date = NVL(in_delivery_date, delivery_date),
        recipient_id = NVL(in_recipient_id, recipient_id),
        product_id = NVL(in_product_id, product_id),
        quantity = NVL(in_quantity, quantity),
        color_1 = NVL(in_color_1, color_1),
        color_2 = NVL(in_color_2, color_2),
        substitution_indicator = NVL(in_substitution_indicator, substitution_indicator),
        same_day_gift = NVL(in_same_day_gift, same_day_gift),
        occasion = NVL(in_occasion, occasion),
        card_message = NVL(in_card_message, card_message),
        card_signature = NVL(in_card_signature, card_signature),
        special_instructions = NVL(in_special_instructions, special_instructions),
        release_info_indicator = NVL(in_release_info_indicator, release_info_indicator),
        florist_id = NVL(in_florist_id, florist_id),
        ship_method = NVL(in_ship_method, ship_method),
        ship_date = NVL(in_ship_date, ship_date),
        order_disp_code = NVL(in_order_disp_code, order_disp_code),
        second_choice_product = NVL(in_second_choice_product, second_choice_product),
        zip_queue_count = NVL(in_zip_queue_count, zip_queue_count),
        updated_on = sysdate,
        updated_by = in_updated_by,
        delivery_date_range_end = NVL(in_delivery_date_range_end, delivery_date_range_end),
        scrubbed_on = NVL(in_scrubbed_on, scrubbed_on),
        scrubbed_by = NVL(in_scrubbed_by, scrubbed_by),
        ariba_unspsc_code = NVL(in_ariba_unspsc_code, ariba_unspsc_code),
        ariba_po_number = NVL(in_ariba_po_number, ariba_po_number),
        ariba_ams_project_code = NVL(in_ariba_ams_project_code, ariba_ams_project_code),
        ariba_cost_center = NVL(in_ariba_cost_center, ariba_cost_center),
        size_indicator = NVL(in_size_indicator, size_indicator),
        miles_points = NVL(in_miles_points, miles_points),
        subcode = NVL(in_subcode, subcode),
        source_code = NVL(in_source_code, source_code),
        reject_retry_count = NVL(in_reject_retry_count, reject_retry_count),
        op_status = NVL (in_op_status, op_status),
        vendor_id = NVL (in_vendor_id, vendor_id),
        delivery_date_received = sysdate
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN ORDER_PROCESSING.UPDATE_ORDER_DETAILS_PC [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_DETAILS_PC;

end order_processing;
/
grant execute on clean.order_processing to osp;
