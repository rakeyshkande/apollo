CREATE OR REPLACE TRIGGER clean.trg_west_vendor_jde_audit_log
BEFORE INSERT
ON clean.west_vendor_jde_audit_log
FOR EACH ROW
-- PL/SQL Block
BEGIN
   IF INSERTING THEN
      IF :NEW.log_id IS NULL THEN
         SELECT
            clean.west_vendor_jde_audit_log_sq.NEXTVAL
         INTO
            :NEW.log_id
         FROM
            dual;
      END IF;
   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      ROLLBACK;
      RAISE_APPLICATION_ERROR(-2000, 'Fatal Error In CLEAN.TRG_WEST_VENDOR_JDE_AUDIT_LOG_BI: SQLCODE: ' || SQLCODE ||
          ' SQL ERROR MESSAGE: ' || SQLERRM);
END;
/