--
-- Trigger on order_details to enqueue JMS message for CAMS order feed
--
CREATE OR REPLACE 
TRIGGER clean.trg_cams_feed_order_details
AFTER UPDATE 
OF size_indicator, 
   membership_id,
   bin_source_changed_flag,
   apply_surcharge_code,
   surcharge_description,
   send_surcharge_to_florist_flag,
   display_surcharge_flag,
   auto_renew_flag,
   free_shipping_flag,
   original_order_has_sdu,
   source_code,
   delivery_date,
   recipient_id,
   product_id,
   quantity,
   color_1,
   color_2,
   substitution_indicator,
   same_day_gift,
   occasion,
   card_message,
   card_signature,
   special_instructions,
   release_info_indicator,
   ship_method,
   ship_date,
   scrubbed_on,
   scrubbed_by,
   delivery_date_range_end
ON clean.order_details
FOR EACH ROW
--  WHEN ( :old.size_indicator != :new.size_indicator OR ...)

DECLARE
   v_stat          varchar2(1) := 'Y';
   v_mess          varchar2(2000);
   v_exception     EXCEPTION;
   v_exception_txt varchar2(200);
   v_payload       varchar2(2000);

BEGIN
   IF :NEW.order_disp_code = 'Processed' THEN
      -- Post a message with 5 minute delay
      v_payload := :NEW.order_detail_id || ',UPDATE_OD,1';
      events.post_a_message_flex('OJMS.ACCOUNT_ORDER_FEED', NULL, v_payload, 300, v_stat, v_mess);

      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message in CAMS order feed trigger: order_detail_id = ' || :NEW.order_detail_id || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;
   END IF;


END trg_cams_feed_order_details;
.
/
