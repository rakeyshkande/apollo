CREATE OR REPLACE
PACKAGE BODY clean.ID_LIST_PKG
AS

-- NOTE: The initial idea to only return the data that was to be displayed on a single page
-- was to use xml to contain specific ids to be retrieved, then join the xml as a table
-- to the actual data tables to only retrieve specific records.  But invalid lob errors
-- occurred in random sql statements.  The idea lead to creating a string of ids where the resulting
-- sql would have to be dynamic and the IN clause used.



PROCEDURE GET_STR_ID
(
IN_ARRAY        GUID_TAB_TYP,
IN_START        NUMBER,
IN_NUM          NUMBER,
OUT_STR         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for converting an array of ids to
        an string for use with a IN clause.

Input:
        array           varchar2(2000) array
        start           index to start
        num             number of ids to include

Output:
        string containing a comma delimited string of ids

-----------------------------------------------------------------------------*/
v_begin         number := in_start;
v_end           number := in_start + in_num - 1;

BEGIN

-- check if the ending index is more than what is in the array
-- if so, set the end index to be the end of the array to prevent out of bounds exception
if v_end >= in_array.count or in_num is null then
        v_end := in_array.count;
end if;

-- loop through the given index range creating the xml of ids
-- check if the start index is more than what is in the array
if in_array.count > 0 and v_begin <= in_array.count then
        for x in v_begin..v_end loop
                out_str := out_str || '''' || in_array(x) || ''',';
        end loop;
end if;

--  remove the last comma from the string
out_str := substr(out_str, 1, length(out_str) - 1);

END GET_STR_ID;


PROCEDURE GET_STR_ID
(
IN_ARRAY        VARCHAR_TAB_TYP,
IN_START        NUMBER,
IN_NUM          NUMBER,
OUT_STR         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for converting an array of ids to
        an string for use with a IN clause.

Input:
        array           varchar2(2000) array
        start           index to start
        num             number of ids to include

Output:
        string containing a comma delimited string of ids

-----------------------------------------------------------------------------*/
v_begin         number := in_start;
v_end           number := in_start + in_num - 1;

BEGIN

-- check if the ending index is more than what is in the array
-- if so, set the end index to be the end of the array to prevent out of bounds exception
if v_end >= in_array.count or in_num is null then
        v_end := in_array.count;
end if;

-- loop through the given index range creating the xml of ids
-- check if the start index is more than what is in the array
if in_array.count > 0 and v_begin <= in_array.count then
        for x in v_begin..v_end loop
                out_str := out_str || '''' || in_array(x) || ''',';
        end loop;
end if;

--  remove the last comma from the string
out_str := substr(out_str, 1, length(out_str) - 1);

END GET_STR_ID;


PROCEDURE GET_STR_ID
(
IN_ARRAY        NUMBER_TAB_TYP,
IN_START        NUMBER,
IN_NUM          NUMBER,
OUT_STR         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for converting an array of ids to
        an string for use with a IN clause.

Input:
        array           varchar2(2000) array
        start           index to start
        num             number of ids to include

Output:
        string containing a comma delimited string of ids

-----------------------------------------------------------------------------*/
v_begin         number := in_start;
v_end           number := in_start + in_num - 1;

BEGIN

-- check if the ending index is more than what is in the array
-- if so, set the end index to be the end of the array to prevent out of bounds exception
if v_end >= in_array.count or in_num is null then
        v_end := in_array.count;
end if;

-- loop through the given index range creating the xml of ids
-- check if the start index is more than what is in the array
if in_array.count > 0 and v_begin <= in_array.count then
        for x in v_begin..v_end loop
                out_str := out_str || in_array(x) || ',';
        end loop;
end if;

--  remove the last comma from the string
out_str := coalesce (substr(out_str, 1, length(out_str) - 1), '-1');

END GET_STR_ID;
END;
.
/
