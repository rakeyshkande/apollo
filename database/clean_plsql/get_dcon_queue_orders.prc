create or replace 
PROCEDURE CLEAN.GET_DCON_QUEUE_ORDERS (
OUT_CUR      OUT Types.Ref_Cursor
) AS

BEGIN

OPEN OUT_CUR FOR

select od.order_detail_id, od.external_order_number, o.order_guid, o.master_order_number, o.email_id, m.mercury_id fulfill_id
from clean.order_details od
join clean.orders o on o.order_guid= od.order_guid and o.email_id is not null
join mercury.mercury m on m.reference_number = to_char(od.order_detail_id)
join clean.customer c on c.customer_id = od.recipient_id
join ftd_apps.state_master s on s.state_master_id = c.state
join ftd_apps.source_master sm on sm.source_code = od.source_code
join ftd_apps.source_program_ref spr on spr.source_code = sm.source_code
join ftd_apps.partner_program pp on pp.program_name = spr.program_name
join ftd_apps.partner_master pm on pm.partner_name = pp.partner_name
join frp.global_parms gp on context = 'PREFERRED_PARTNER_CONFIG' and name = pm.PARTNER_NAME || '_DCON_QUEUE_DELAY'
where nvl(od.delivery_confirmation_status, 'N/A') not in ('Queued', 'Confirmed', 'No Email')
and pm.preferred_partner_flag = 'Y'
and nvl(od.ship_method, 'SD') = 'SD'
and m.created_on = (select max(m1.created_on)
                    from mercury.mercury m1
                    where m1.reference_number = to_char(od.order_detail_id)
                    and m1.msg_type = 'FTD'
                    and m1.mercury_status in ('MC', 'MM', 'MN')
                    and not exists (select 'Y' from mercury.mercury m2
                                    where m2.mercury_order_number = m1.mercury_order_number
                                    and m2.msg_type in ('REJ', 'CAN')
                                    AND m2.mercury_status IN ('MC', 'MN')))
AND ((m.delivery_date) = trunc(SYSDATE - 2))
union
select od.order_detail_id, od.external_order_number, o.order_guid, o.master_order_number, o.email_id, m.mercury_id fulfill_id
from clean.order_details od
join clean.orders o on o.order_guid= od.order_guid and o.email_id is not null
join mercury.mercury m on m.reference_number = to_char(od.order_detail_id)
join clean.customer c on c.customer_id = od.recipient_id
join ftd_apps.state_master s on s.state_master_id = c.state
join ftd_apps.source_master sm on sm.source_code = od.source_code
join ftd_apps.source_program_ref spr on spr.source_code = sm.source_code
join ftd_apps.partner_program pp on pp.program_name = spr.program_name
join ftd_apps.partner_master pm on pm.partner_name = pp.partner_name
join frp.global_parms gp on context = 'PREFERRED_PARTNER_CONFIG' and name = pm.PARTNER_NAME || '_DCON_QUEUE_DELAY'
where nvl(od.delivery_confirmation_status, 'N/A') not in ('Queued', 'Confirmed', 'No Email')
and pm.preferred_partner_flag = 'Y'
and nvl(od.ship_method, 'SD') = 'SD'
and m.created_on = (select max(m1.created_on)
                    from mercury.mercury m1
                    where m1.reference_number = to_char(od.order_detail_id)
                    and m1.msg_type = 'FTD'
                    and m1.mercury_status in ('MC', 'MM', 'MN')
                    and not exists (select 'Y' from mercury.mercury m2
                                    where m2.mercury_order_number = m1.mercury_order_number
                                    and m2.msg_type in ('REJ', 'CAN')
                                    AND m2.mercury_status IN ('MC', 'MN')))
and ((m.delivery_date) = trunc(SYSDATE - 1) AND to_char(SYSDATE, 'HH24') >= (gp.VALUE + nvl(s.TIME_ZONE,0) - 2))
union
select od.order_detail_id, od.external_order_number, o.order_guid, o.master_order_number, o.email_id, m.venus_id fulfill_id
from clean.order_details od
join clean.orders o on o.order_guid= od.order_guid and o.email_id is not null
join venus.venus m on m.reference_number = to_char(od.order_detail_id)
join clean.customer c on c.customer_id = od.recipient_id
join ftd_apps.state_master s on s.state_master_id = c.state
join ftd_apps.source_master sm on sm.source_code = od.source_code
join ftd_apps.source_program_ref spr on spr.source_code = sm.source_code
join ftd_apps.partner_program pp on pp.program_name = spr.program_name
join ftd_apps.partner_master pm on pm.partner_name = pp.partner_name
join frp.global_parms gp on context = 'PREFERRED_PARTNER_CONFIG' and name = pm.PARTNER_NAME || '_DCON_QUEUE_DELAY'
where nvl(od.delivery_confirmation_status, 'N/A') not in ('Queued', 'Confirmed', 'No Email')
and pm.preferred_partner_flag = 'Y'
and od.ship_method <> 'SD' and od.ship_method is not null
and m.created_on = (select max(m1.created_on)
                    from venus.venus m1
                    where m1.reference_number = to_char(od.order_detail_id)
                    and m1.msg_type = 'FTD'
                    and m1.venus_status = 'VERIFIED'
                    and not exists (select 'Y' from venus.venus m2
                                    where m2.venus_order_number = m1.venus_order_number
                                    and m2.msg_type in ('REJ', 'CAN')
                                    AND m2.venus_status = 'VERIFIED'))
 and ((m.delivery_date) = trunc(SYSDATE - 2) )
 union 
select od.order_detail_id, od.external_order_number, o.order_guid, o.master_order_number, o.email_id, m.venus_id fulfill_id
from clean.order_details od
join clean.orders o on o.order_guid= od.order_guid and o.email_id is not null
join venus.venus m on m.reference_number = to_char(od.order_detail_id)
join clean.customer c on c.customer_id = od.recipient_id
join ftd_apps.state_master s on s.state_master_id = c.state
join ftd_apps.source_master sm on sm.source_code = od.source_code
join ftd_apps.source_program_ref spr on spr.source_code = sm.source_code
join ftd_apps.partner_program pp on pp.program_name = spr.program_name
join ftd_apps.partner_master pm on pm.partner_name = pp.partner_name
join frp.global_parms gp on context = 'PREFERRED_PARTNER_CONFIG' and name = pm.PARTNER_NAME || '_DCON_QUEUE_DELAY'
where nvl(od.delivery_confirmation_status, 'N/A') not in ('Queued', 'Confirmed', 'No Email')
and pm.preferred_partner_flag = 'Y'
and od.ship_method <> 'SD' and od.ship_method is not null
and m.created_on = (select max(m1.created_on)
                    from venus.venus m1
                    where m1.reference_number = to_char(od.order_detail_id)
                    and m1.msg_type = 'FTD'
                    and m1.venus_status = 'VERIFIED'
                    and not exists (select 'Y' from venus.venus m2
                                    where m2.venus_order_number = m1.venus_order_number
                                    and m2.msg_type in ('REJ', 'CAN')
                                    AND m2.venus_status = 'VERIFIED')) 
and  ((m.delivery_date) = trunc(SYSDATE - 1) AND to_char(SYSDATE, 'HH24') >= (gp.VALUE + nvl(s.TIME_ZONE,0) - 2));

end;
.
/
