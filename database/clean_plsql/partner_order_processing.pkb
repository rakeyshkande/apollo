CREATE OR REPLACE PACKAGE BODY clean.PARTNER_ORDER_PROCESSING 
AS
 
/*-----------------------------------------------------------------------------
Name:
                    INSERT_WEBLOYALTY_MASTER

Description:
        This procedure is responsible for inserting and updating data in the 
        clean.webloyalty table for a given order

Input:
        in_order_guid                 order guid
        in_customer_txn_ref_txt       customer transaction number - cref
        in_created_by                 created by
        in_updated_by                 updated by

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/
PROCEDURE INSERT_WEBLOYALTY_MASTER
(
IN_ORDER_GUID                   IN WEBLOYALTY_MASTER.ORDER_GUID%TYPE,
IN_CUSTOMER_TXN_REF_TXT         IN WEBLOYALTY_MASTER.CUSTOMER_TXN_REF_TXT%TYPE,
IN_CREATED_BY                   IN WEBLOYALTY_MASTER.CREATED_BY%TYPE,
IN_UPDATED_BY                   IN WEBLOYALTY_MASTER.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS 

BEGIN

INSERT INTO clean.WEBLOYALTY_MASTER
(
  ORDER_GUID,
  CUSTOMER_TXN_REF_TXT,
  CREATED_BY,
  CREATED_ON,
  UPDATED_BY,
  UPDATED_ON,
  TRANSACTION_SENT_DATETIME, 
  DATA_RETURNED_FLAG
)
VALUES
(
  IN_ORDER_GUID,
  IN_CUSTOMER_TXN_REF_TXT,
  IN_CREATED_BY,
  sysdate,
  IN_CREATED_BY,
  sysdate,
  null,
  null
);


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_WEBLOYALTY_MASTER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_WEBLOYALTY_MASTER;


/*-----------------------------------------------------------------------------
Name:
                      UPDATE_WEBLOYALTY_MASTER

Description:
        This procedure is responsible for updating data in the clean.webloyalty
        table for the given order

Input:
        in_customer_txn_ref_txt       customer transaction number - cref
        in_updated_by                 updated by
        in_transaction_sent_datetime  transaction sent date - date when the record
                                      was sent to WebLoyalty

Output:
        status                  varchar2 (Y or N)
        message                 varchar2 (error message)

-----------------------------------------------------------------------------*/
PROCEDURE UPDATE_WEBLOYALTY_MASTER
(
IN_CUSTOMER_TXN_REF_TXT         IN WEBLOYALTY_MASTER.CUSTOMER_TXN_REF_TXT%TYPE,
IN_UPDATED_BY                   IN WEBLOYALTY_MASTER.UPDATED_BY%TYPE,
IN_DATA_RETURNED_FLAG           IN WEBLOYALTY_MASTER.DATA_RETURNED_FLAG%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS 

v_date     DATE := sysdate;

BEGIN

UPDATE CLEAN.WEBLOYALTY_MASTER
SET 
  UPDATED_BY = decode(IN_UPDATED_BY, null, UPDATED_BY, IN_UPDATED_BY),
  UPDATED_ON = decode(IN_UPDATED_BY, null, UPDATED_ON, sysdate),
  DATA_RETURNED_FLAG = decode(IN_DATA_RETURNED_FLAG, null, null, IN_DATA_RETURNED_FLAG),
  TRANSACTION_SENT_DATETIME = decode( IN_DATA_RETURNED_FLAG,
                                      'Y',sysdate,
                                      TRANSACTION_SENT_DATETIME
                                    )
where CUSTOMER_TXN_REF_TXT = IN_CUSTOMER_TXN_REF_TXT;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_WEBLOYALTY_MASTER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END UPDATE_WEBLOYALTY_MASTER;


/*-----------------------------------------------------------------------------
Name:
                        GET_WEBLOYALTY_ORDER

Description:
        Retrieves data based off of IN_CUSTOMER_TXN_REF_TXT

Input:
        in_customer_txn_ref_txt       customer transaction number - cref
        in_valid_token_time           time parameter for which the token is valid for
        
Output:
        cursor                  

-----------------------------------------------------------------------------*/
PROCEDURE GET_WEBLOYALTY_ORDER
(
IN_CUSTOMER_TXN_REF_TXT       IN WEBLOYALTY_MASTER.CUSTOMER_TXN_REF_TXT%TYPE,
IN_VALID_TOKEN_TIME           IN NUMBER,
OUT_CUR                       OUT TYPES.REF_CURSOR
)
AS
BEGIN

OPEN OUT_CUR FOR
    SELECT  c.FIRST_NAME, c.LAST_NAME, c.ADDRESS_1, c.ADDRESS_2, c.CITY, c.STATE, 
            c.ZIP_CODE, o.ORDER_GUID, o.CUSTOMER_ID, o.MASTER_ORDER_NUMBER,
            wm.CREATED_ON, wm.UPDATED_ON, cc.CC_TYPE, 
            GLOBAL.ENCRYPTION.DECRYPT_IT (cc.CC_NUMBER, cc.KEY_NAME) CC_NUMBER, 
            cc.CC_EXPIRATION
    FROM  CLEAN.CUSTOMER c, CLEAN.PAYMENTS p, CLEAN.CREDIT_CARDS cc, CLEAN.ORDERS o, 
          CLEAN.WEBLOYALTY_MASTER wm
    WHERE c.CUSTOMER_ID = o.CUSTOMER_ID
    AND   o.ORDER_GUID = p.ORDER_GUID
    AND   o.ORDER_GUID = wm.ORDER_GUID
    AND   p.CC_ID = cc.CC_ID
    AND   wm.CUSTOMER_TXN_REF_TXT = IN_CUSTOMER_TXN_REF_TXT
    AND   wm.CREATED_ON > (sysdate - (IN_VALID_TOKEN_TIME/1440));
    
END GET_WEBLOYALTY_ORDER;


END PARTNER_ORDER_PROCESSING;
/
