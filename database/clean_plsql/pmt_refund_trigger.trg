
 
CREATE OR REPLACE TRIGGER clean.pmt_refund_trigger 
AFTER UPDATE ON clean.payments
FOR EACH ROW
WHEN (new.payment_indicator = 'R') 
BEGIN 
   if :new.bill_status = 'Settled' then
	   update clean.refund
	   set refund_status = :new.bill_status,
	       settled_date = :new.bill_date,
	       updated_on = :new.updated_on,
	       updated_by = :new.updated_by
	   where refund_id = :new.refund_id;
   else
	   update clean.refund
	   set refund_status = :new.bill_status,
	       refund_date = :new.bill_date,
	       updated_on = :new.updated_on,
	       updated_by = :new.updated_by
	   where refund_id = :new.refund_id;  
   end if;
END; 
/