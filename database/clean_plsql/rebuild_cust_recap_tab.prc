create or replace procedure clean.rebuild_cust_recap_tab as

   cursor c1 is
      select master_order_number
      from   clean.cust_recap_tab
      where  payment_type = 'NC'
      and    additional_bill_id is null
      and    refund_id is null;
   r1 c1%rowtype;

   cursor c2 is
      select order_detail_id
      from   clean.cust_recap_tab
      where  refund_disp_code like 'A%';
   r2 c2%rowtype;

begin

/*-----------------------------------------------------------------------------
PROCEDURE rebuild_cust_recap_tab

This procedure repopulates the cust_recap_tab to be used to build the customer
recap report on a monthly basis.  This procedure is called from
rpt.mkt03_customer_recap_pop_tab when the report data does not exist for a 
given month.
-----------------------------------------------------------------------------*/

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' drop n1 index');
begin
	execute immediate ('drop index clean.cust_recap_tab_n1');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' drop n3 index');
begin
	execute immediate ('drop index clean.cust_recap_tab_n3');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' drop n5 index');
begin
	execute immediate ('drop index clean.cust_recap_tab_n5');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' drop n6 index');
begin
	execute immediate ('drop index clean.cust_recap_tab_n6');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' truncate table');
begin
	execute immediate ('truncate table clean.cust_recap_tab');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' main insert');
insert /*+ APPEND */ into clean.cust_recap_tab
select o.master_order_number, o.customer_id,
       decode(o.company_id, 'GSC', 'FTD', 'SFMB', 'FTD', o.company_id) company_id,
       o.order_date, od.order_detail_id, c.concat_id, od.order_disp_code, p.payment_id, p.payment_type,
       p.additional_bill_id, p.refund_id, r.refund_disp_code
from   clean.orders o
join   clean.order_details od
on     o.order_guid = od.order_guid
and    od.order_disp_code not in ('In-Scrub','Pending','Removed')
join   clean.customer c
on     o.customer_id = c.customer_id
join   clean.payments p
on     p.order_guid = o.order_guid
left outer join clean.refund r on r.order_detail_id = od.order_detail_id
where  (o.origin_id IS NULL OR o.origin_id <> 'TARGI') and o.company_id not in ('ProFlowers');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' create n2 index');
execute immediate ('create index clean.cust_recap_tab_n2 on clean.cust_recap_tab (payment_type) tablespace clean_indx nologging compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' create n1 index');
execute immediate ('create index clean.cust_recap_tab_n1 on clean.cust_recap_tab (master_order_number) tablespace clean_indx nologging compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' delete nocharge orders');
   for r1 in c1 loop
      delete from clean.cust_recap_tab where master_order_number = r1.master_order_number;
   end loop;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' drop n2 index');
begin
	execute immediate ('drop index clean.cust_recap_tab_n2');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' create n3 index');
execute immediate ('create index clean.cust_recap_tab_n3 on clean.cust_recap_tab (order_detail_id) tablespace clean_indx nologging compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' create n4 index');
execute immediate ('create index clean.cust_recap_tab_n4 on clean.cust_recap_tab (refund_disp_code) tablespace clean_indx nologging compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' delete full-refund orders');
   for r2 in c2 loop
      delete from clean.cust_recap_tab where order_detail_id = r2.order_detail_id;
   end loop;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' drop n4 index');
begin
	execute immediate ('drop index clean.cust_recap_tab_n4');
	exception when others then null;
end;

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' rebuild n1 index');
execute immediate ('alter index clean.cust_recap_tab_n1 rebuild compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' rebuild n3 index');
execute immediate ('alter index clean.cust_recap_tab_n3 rebuild compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' create n5 index');
execute immediate ('create index clean.cust_recap_tab_n5 on clean.cust_recap_tab (order_date) tablespace clean_indx nologging compute statistics');

dbms_output.put_line(to_char(sysdate,'mm/dd hh24:mi')||' create n6 index');
execute immediate ('create index clean.cust_recap_tab_n6 on clean.cust_recap_tab (concat_id) tablespace clean_indx nologging compute statistics');

execute immediate ('analyze table clean.cust_recap_tab compute statistics');

end;
/
