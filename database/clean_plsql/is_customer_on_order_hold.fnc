CREATE OR REPLACE
FUNCTION clean.IS_CUSTOMER_ON_ORDER_HOLD (
 IN_CUSTOMER_ID IN NUMBER,
 IN_LOSS_PREVENTION_INDICATOR IN VARCHAR2
)
 RETURN VARCHAR2 is
   v_return varchar2(5);
begin
   v_return := customer_query_pkg.is_customer_on_order_hold(in_customer_id, in_loss_prevention_indicator);
   return v_return;
end is_customer_on_order_hold;
.
/
