CREATE OR REPLACE
PACKAGE BODY clean.ORDERDETAILS_QUERY_PKG
AS


FUNCTION GET_OD_RECIPIENT_ID
	( IN_ORDER_DETAIL_ID         IN  ORDER_DETAILS.ORDER_DETAIL_ID%TYPE )
	  RETURN ORDER_DETAILS.RECIPIENT_ID%TYPE
IS

v_data ORDER_DETAILS.RECIPIENT_ID%TYPE;

BEGIN

	BEGIN
	SELECT recipient_id
	INTO v_data
	FROM order_details
	WHERE order_detail_id = in_order_detail_id;

	EXCEPTION WHEN NO_DATA_FOUND THEN v_data := NULL;
	END;

RETURN v_data;
END GET_OD_RECIPIENT_ID;



PROCEDURE GET_OD_CUST_DETAILS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
BEGIN

OPEN OUT_CUR FOR
        SELECT  od.order_detail_id,
                od.external_order_number,
                r.first_name,
                r.last_name,
                r.address_1,
                r.address_2,
                r.city,
                r.state,
                r.zip_code,
                r.country,
                r.business_name,
                to_char(od.delivery_date, 'mm/dd/yyyy') delivery_date,
                to_char(od.delivery_date_range_end,'mm/dd/yyyy') delivery_date_range_end,
                od.product_id,
                ftd_apps.product_query_pkg.GET_PRODUCT_NAME_BY_ID (od.product_id) product_name,
                od.card_message,
                od.ship_method,
                od.ship_date,
                decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                (select min (t.tracking_number) from clean.order_tracking t where t.order_detail_id = od.order_detail_id) tracking_number,
                (select to_char(o.order_date, 'mm/dd/yyyy') from clean.orders o where o.order_guid = od.order_guid) order_date,
                decode( od.ship_method, null, null,
                        ftd_apps.product_query_pkg.get_ship_method_by_id(od.ship_method) ) ship_method_desc,
                cm.oe_country_type international_flag,
                cm.name country_name,
                pm.personalization_template_id, 
                od.source_code,
                o.language_id
        FROM    order_details od,
                customer r,
                ftd_apps.country_master cm,
                ftd_apps.product_master pm,
                orders o
       WHERE    od.recipient_id = r.customer_id
          AND   cm.country_id = r.country
          AND   cm.status = 'Active'
          AND   od.order_detail_id = in_order_detail_id
          AND   od.product_id = pm.product_id
          AND   od.order_guid = o.order_guid;

END GET_OD_CUST_DETAILS;


PROCEDURE GET_OD_PROD_FLORIST_DETAILS (
	IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
	OUT_CUR                         OUT TYPES.REF_CURSOR )
AS
BEGIN

OPEN OUT_CUR FOR
        SELECT  od.order_detail_id,
                od.external_order_number,
                od.source_code,
                r.customer_id recipient_id,
                r.first_name,
                r.last_name,
                r.address_1,
                r.address_2,
                r.city,
                r.state,
                r.zip_code,
                r.country,
                r.address_type,
                r.business_name,
                to_char (od.delivery_date, 'mm/dd/yyyy') delivery_date,
                global.global_pkg.GET_COLOR_BY_ID(od.color_1) color1_description,
                global.global_pkg.GET_COLOR_BY_ID(od.color_2) color2_description,
                od.florist_id,
                f.florist_name,
                f.phone_number florist_phone_number,
                od.occasion,
                od.card_message,
                od.card_signature,
                od.release_info_indicator,
                od.special_instructions,
                od.substitution_indicator,
                od.ship_method,
                od.ship_date,
                decode ( (select odv.display_indicator from clean.order_disposition_val odv where odv.order_disp_code = od.order_disp_code), 'Y',
                          od.order_disp_code, null) order_disp_code,
                clean.comment_history_pkg.order_has_comments (od.order_detail_id) order_comment_indicator,
                coalesce (customer_query_pkg.get_customer_phone_by_id_type( r.customer_id,'Day'),
                          customer_query_pkg.get_customer_phone_by_id_type( r.customer_id,'Evening') ) recipient_phone_number,
                decode (customer_query_pkg.get_customer_phone_by_id_type( r.customer_id,'Day'), null,
                          customer_query_pkg.get_customer_ext_by_id_type( r.customer_id,'Evening'),
                          customer_query_pkg.get_customer_ext_by_id_type( r.customer_id,'Day')) recipient_extension,
                decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                (select ob.discount_product_price
                   from clean.order_bills ob
                  where od.order_detail_id = ob.order_detail_id
                    and ob.additional_bill_indicator = 'N') discount_product_price,
                (select min (t.tracking_number)
                   from clean.order_tracking t
                  where t.order_detail_id = od.order_detail_id) tracking_number,
                (select shipping_system from ftd_apps.product_master pm where pm.product_id=od.product_id) shipping_system,
                p.personalization_template_id
        FROM    clean.order_details od,
                ftd_apps.product_master p,
                clean.customer r,
                ftd_apps.florist_master f
       WHERE    od.product_id = p.product_id
          AND   od.recipient_id = r.customer_id
          AND   od.florist_id = f.florist_id
          AND   od.order_detail_id = in_order_detail_id;

END GET_OD_PROD_FLORIST_DETAILS;


PROCEDURE GET_ORDER_DETAILS_OP
(
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CURSOR          OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the records from order_details
         for the order_detail_id passed in.

** IMPORTANT **
This procedure is used extensively by Order Processing
Do NOT make changes without consulting with OP developers.

Input:
        order_detail_id         NUMBER

Output:
        cursor containing order_details info

-----------------------------------------------------------------------------*/



BEGIN

  OPEN out_cursor FOR
    SELECT od.order_detail_id,
           od.order_guid,
           od.external_order_number,
           od.hp_order_number,
           od.source_code,
           to_char( od.delivery_date ,'mm/dd/yyyy') delivery_date,
           od.recipient_id,
           od.product_id,
           od.quantity,
           od.color_1,
           od.color_2,
           od.substitution_indicator,
           od.same_day_gift,
           od.occasion,
           od.card_message,
           od.card_signature,
           od.special_instructions,
           od.release_info_indicator,
           od.florist_id,
           od.ship_method,
           od.ship_date,
           od.order_disp_code,
           od.second_choice_product,
           od.zip_queue_count,
           od.scrubbed_on,
           od.scrubbed_by,
           to_char( od.delivery_date_range_end ,'mm/dd/yyyy') delivery_date_range_end,
           od.ariba_unspsc_code,
           od.ariba_po_number,
           od.ariba_ams_project_code,
           od.ariba_cost_center,
           od.size_indicator,
           od.miles_points,
           od.subcode,
           od.eod_delivery_indicator,
           to_char( od.eod_delivery_date,'mm/dd/yyyy') eod_delivery_date,
           to_char( od.miles_points_post_date,'mm/dd/yyyy') miles_points_post_date,
           od.membership_id,
           od.reject_retry_count,
           od.carrier_delivery,
           decode(od.carrier_delivery, null, null, venus.ship_pkg.get_car_name_by_od(od.carrier_delivery) ) carrier_name,
           decode(od.carrier_delivery, null, null, (select cd.carrier_id from venus.carrier_delivery cd where cd.carrier_delivery = od.carrier_delivery) ) carrier_id,
           decode(od.carrier_delivery, null, null, venus.ship_pkg.get_car_method_of_pmt_by_od(od.carrier_delivery) )method_of_payment,
           op_status,
           od.personalization_data,
           ob.miles_points_amt,
           decode(ob.bill_date, null, 'N', 'Y') orig_bill_billed,
           od.personal_greeting_id,
           od.delivery_confirmation_status,
		   r.country recipient_country,
		   decode(ob.morning_delivery_fee,NULL,'N','Y') order_has_morning_delivery,
           od.avs_address_id,
		   od.pc_group_id,
		   od.derived_vip_flag,
		   od.legacy_id,
		   r.ZIP_CODE
      FROM order_details od
      JOIN order_bills ob
      	ON ob.order_detail_id = od.order_detail_id 
       AND ob.additional_bill_indicator = 'N'
	   JOIN customer r on od.recipient_id = r.customer_id
     WHERE od.order_detail_id = in_order_detail_id;


END GET_ORDER_DETAILS_OP;

FUNCTION GET_OD_VENDOR_ID
( 
	IN_ORDER_DETAIL_ID         IN  ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN ORDER_DETAILS.VENDOR_ID%TYPE
AS

	v_data ORDER_DETAILS.VENDOR_ID%TYPE;

BEGIN

	BEGIN
		SELECT vendor_id
		  INTO v_data
		  FROM order_details
		 WHERE order_detail_id = in_order_detail_id;

	EXCEPTION 
		WHEN NO_DATA_FOUND THEN 
			v_data := NULL;
	END;

	RETURN v_data;
	
END GET_OD_VENDOR_ID;

FUNCTION ORDER_HAS_MORNING_DELIVERY_FEE
	( IN_ORDER_DETAIL_ID         IN  ORDER_DETAILS.ORDER_DETAIL_ID%TYPE )
	  RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This function will return a char (Y or N) indicating if the order assoicated
        to the passed in order detail id has morning delivery or not

Input:
        order_detail_id                 number

Output:
        Y/N if order contains morning delivery fee or not
-----------------------------------------------------------------------------*/

v_morning_delivery_flag        VARCHAR2(1);

BEGIN

select decode(ob.morning_delivery_fee,NULL,'N','Y') into v_morning_delivery_flag
   from clean.order_bills ob
   where ob.order_detail_id = IN_ORDER_DETAIL_ID
   and ob.additional_bill_indicator = 'N';

RETURN v_morning_delivery_flag;

END ORDER_HAS_MORNING_DELIVERY_FEE;


PROCEDURE GET_OSCAR_ORDER_DETAILS
(
 IN_SOURCE_CODE       IN ORDER_DETAILS.SOURCE_CODE%TYPE, 
 IN_ORDER_DETAIL_ID   IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
 OUT_CUR             OUT TYPES.REF_CURSOR,
 OUT_STATUS          OUT VARCHAR2,
 OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure determines if order meets any Oscar selection
        criteria for:
            source-code
            city/state
            zip
        It's up to the java level to determine if those criteria 
        are relevant or not.        
-----------------------------------------------------------------------------*/
BEGIN

   OPEN OUT_CUR FOR
         SELECT aa.latitude,
                aa.longitude,
                decode(osg.active_flag,'Y',osg.oscar_scenario_group_name,'') as oscar_scenario_group_name,
                '90-8400AA' as sending_florist,  -- Oscar doesn't need to distinguish between FTD and CA, etc., so just hard-code is fine
                sm.oscar_selection_enabled_flag as matched_source_code,
                (select count(z1.oscar_zip_flag) from ftd_apps.zip_code z1 
                  where z1.zip_code_id=substr(c.zip_code,0,5) and z1.oscar_zip_flag='Y') as matched_zip, 
                (select count(z2.oscar_city_state_flag) from ftd_apps.zip_code z2 
                  where z2.city=upper(c.city) and z2.state_id=c.state and z2.oscar_city_state_flag='Y') as matched_city_state
           FROM clean.order_details od
           JOIN clean.customer c 
             ON c.customer_id = od.recipient_id
           JOIN ftd_apps.source_master sm
             ON sm.source_code = IN_SOURCE_CODE
           JOIN ftd_apps.oscar_scenario_groups osg
             ON osg.oscar_scenario_group_id = sm.oscar_scenario_group_id
     LEFT OUTER 
           JOIN clean.avs_address aa
             ON (aa.avs_address_id = od.avs_address_id
                 AND aa.avs_result = 'PASS'               -- Only want lat/long if AVS succeeded
                 AND aa.override_flag <> 'Y')             -- ...and AVS address was not overridden
          WHERE od.order_detail_id = IN_ORDER_DETAIL_ID;
    
  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
    BEGIN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END GET_OSCAR_ORDER_DETAILS;

FUNCTION GET_PCGROUP_BY_ORDERDETAILID
(
IN_ORDER_DETAIL_ID      IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE
)
RETURN VARCHAR2
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning PREMIER CIRCLE group id for
        given order detail id

Input:
        name                            varchar2
Output:
        value                           varchar2
-----------------------------------------------------------------------------*/

V_VALUE         CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE;

BEGIN

SELECT  pc_group_id
INTO    V_VALUE
FROM    clean.order_details
WHERE   order_detail_id = IN_ORDER_DETAIL_ID;

RETURN V_VALUE;

EXCEPTION WHEN OTHERS THEN
        RETURN NULL;

END GET_PCGROUP_BY_ORDERDETAILID;

FUNCTION DERIVE_VIP_FLAG
(
IN_ORDER_DETAIL_ID    IN SCRUB.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE)
RETURN VARCHAR2 AS 
CURSOR order_payment_type IS
	SELECT  count(*) 
	FROM    SCRUB.ORDER_DETAILS OD,SCRUB.PAYMENTS P
	WHERE   OD.ORDER_DETAIL_ID = IN_ORDER_DETAIL_ID
	AND P.ORDER_GUID = OD.ORDER_GUID AND P.PAYMENT_TYPE = 'MS' and (od.ship_method is null or od.ship_method = 'SD'); 
	
CURSOR order_by_source_type IS
  SELECT cd.content_txt 
	FROM scrub.order_details od, ftd_apps.source_master sm,        
	  ftd_apps.content_master cm,
	  ftd_apps.content_detail  cd
	WHERE
	  od.order_detail_id = IN_ORDER_DETAIL_ID  
	  AND sm.source_code = od.source_code 
	  AND cd.content_master_id = cm.content_master_id 
    AND cm.content_name = sm.source_type and (od.ship_method is null or od.ship_method = 'SD');	
	
CURSOR location_or_occasion IS
	SELECT count(*)  FROM scrub.order_details od, scrub.recipient_addresses ra WHERE 
	order_detail_id = IN_ORDER_DETAIL_ID
	AND od.recipient_address_id = ra.recipient_address_id 
	AND od.recipient_id = ra.recipient_id
	AND ( ra.address_type = 'CEMETERY' OR ra.address_type = 'FUNERAL HOME'  OR od.occasion_id = 1 )
  AND (od.ship_method is null or od.ship_method = 'SD');
  
CURSOR pc_order IS
  SELECT count(*)  FROM scrub.order_details od WHERE 
  order_detail_id = IN_ORDER_DETAIL_ID and pc_flag = 'Y' and (od.ship_method is null or od.ship_method = 'SD') ;
 
	
v_payment_count NUMBER;  
v_source_derived_flag VARCHAR2(20);
v_location_or_occasion_count NUMBER;
v_pc_order_count NUMBER;
OUT_DERIVED_FLAG VARCHAR2(20); 

BEGIN
        OPEN order_payment_type;
        FETCH order_payment_type INTO v_payment_count;
        CLOSE order_payment_type;

-- check source type  --  
IF v_payment_count = 0 THEN
      	OPEN order_by_source_type;
	      FETCH order_by_source_type INTO v_source_derived_flag;
       	CLOSE order_by_source_type;     	
END IF;

-- Occasion of sympathy and funeral --     
IF v_source_derived_flag is null THEN
      	OPEN location_or_occasion;
        FETCH location_or_occasion INTO v_location_or_occasion_count;
       	CLOSE location_or_occasion;   
END IF;

-- check for pc orders--
IF v_location_or_occasion_count = 0 THEN
      	OPEN pc_order;
        FETCH pc_order INTO v_pc_order_count;
       	CLOSE pc_order;   
END IF;

IF v_payment_count > 0 THEN
 OUT_DERIVED_FLAG := 'Military';
ELSIF v_source_derived_flag IS NOT NULL THEN
OUT_DERIVED_FLAG := v_source_derived_flag;
ELSIF v_location_or_occasion_count > 0 THEN
	OUT_DERIVED_FLAG := 'Sympathy';
ELSIF v_pc_order_count > 0 THEN
	OUT_DERIVED_FLAG := 'VIP';
ELSE
   OUT_DERIVED_FLAG := null;
END IF;
RETURN OUT_DERIVED_FLAG;

END DERIVE_VIP_FLAG;

END ORDERDETAILS_QUERY_PKG;
.
/
