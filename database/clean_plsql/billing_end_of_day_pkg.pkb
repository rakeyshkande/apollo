CREATE OR REPLACE
PACKAGE BODY clean.BILLING_END_OF_DAY_PKG
AS
/*********************************************************************

History
2005.10.30      creyes                  Creation
2005.10.31      ESo                     Added find order number
2006.02.03      sgramann		EOD performance tuning
2006.03.24      chu			EOD performance tuning
2007.03.06      chu			Alt Pay changes

********************************************************************/

PROCEDURE GET_PROCESSED_INDICATOR
(IN_DATE                        in DATE,
 IN_CC_AUTH_PROVIDER     	IN BILLING_HEADER.CC_AUTH_PROVIDER%TYPE,
 OUT_PROCESS_IND                OUT VARCHAR2,
 OUT_BILLING_HEADER_ID          OUT NUMBER)
AS
/*-----------------------------------------------------------------------------
Description:
        7	Check if there's a record in CLEAN.BILLING_HEADER table for the
                given date and auth provider. Return processed_indicator if found. Return null otherwise


Input:
        IN_DATE                         DATE
        IN_CC_AUTH_PROVIDER     	BILLING_HEADER.CC_AUTH_PROVIDER%TYPE,
RETURN:
        VARCHAR2
-----------------------------------------------------------------------------*/

v_processed_indicator  clean.billing_header.processed_indicator%type;
BEGIN
SELECT processed_indicator, billing_header_id
 INTO   OUT_PROCESS_IND,
        OUT_BILLING_HEADER_ID
 FROM clean.billing_header cbh
WHERE trunc(cbh.billing_batch_date) = trunc(in_date)
AND ((in_cc_auth_provider is null and cbh.cc_auth_provider is null) or (in_cc_auth_provider is not null and cbh.cc_auth_provider = in_cc_auth_provider));

EXCEPTION
  WHEN too_many_rows THEN
      SELECT processed_indicator, billing_header_id
        INTO   OUT_PROCESS_IND,
               OUT_BILLING_HEADER_ID
        FROM clean.billing_header cbh
       WHERE trunc(cbh.billing_batch_date) = trunc(in_date)
         AND created_on = (select max(created_on)
                           from clean.billing_header cbh2
                           where  trunc(cbh2.billing_batch_date) = trunc(in_date))
         AND ((in_cc_auth_provider is null and cbh.cc_auth_provider is null) or (in_cc_auth_provider is not null and cbh.cc_auth_provider = in_cc_auth_provider));
  WHEN no_data_found THEN
        OUT_PROCESS_IND:= null;
        OUT_BILLING_HEADER_ID:=null;
END GET_PROCESSED_INDICATOR;

PROCEDURE INSERT_PAYMENT_RECOVERY_TAB
(
in_date                          IN DATE,
in_cc_auth_provider              in CLEAN.eod_payment_recovery.cc_auth_provider%type
)AS
/*-----------------------------------------------------------------------------
Description:
Insert payments.payment_id into clean.eod_payment_recovery_tab orders that satisfy the following criteria:
o	Order_bills.bill_status='Unbilled'
o	None items in the shopping cart is in 'Pending','In-Scrub','Validated','Held' order_disp_code
o	order_disp_code IN ('Processed','Shipped','Printed')
o	Orders.order_date on or before in_date
o	ftd_apps.source.jcpenney_flag <> 'Y'
o	payments.payment_type is not null
o	payments.payment type not in ('MS','GC','NC','IN','PP','BM','UA','GD')
o	credit_cards.CC number is not null
o	payments.auth_number is not null
o	p.credit_amount>0
o	p.payment_indicator='P' or (p.payment_indicator='B' and p.credit_amount>p.debit_amount)
o	p.created_on>=in_date

Insert payments.payment_id, orders.master_order number into clean.eod_payment_recovery_tab refunds that satisfy the following criteria:
o	refund.refund_status='Unbilled'
o	None items in the shopping cart is in 'Pending','In-Scrub','Validated' ,'Held'order_disp_code
o	order_disp_code IN ('Processed','Shipped','Printed')
o	Orders.order_date on or before in_date
o	Orders.origin_id not in ('AMZNI')
o	ftd_apps.source.jcpenney_flag <> 'Y'
o	payments.payment_type is not null
o	payments.payment type not in ('MS','GC','NC','IN')
o	credit_cards.CC number is not null
o	p.debit_amount>0
o	p.payment_indicator='R' or (p.payment_indicator='B' and p.debit_amount>p.credit_amount)
o	refund_created_on>=in_date


Input:
    in_date date
Output:
    None
-----------------------------------------------------------------------------*/


BEGIN
-- We used to have the filter condition
--      WHERE   trunc(p.created_on) <= TRUNC(in_date)
-- which required maintaining an index on the trunc(created_on) function, and
-- passing in the in_date as yesterday's date.
-- When we changed to updated_on and made it a 180-day range, we eliminated the
-- trunc() function.  That way we didn't need to make another index.  To avoid
-- making a change on the Java side, we add 1 day to the in_date passed in.
insert into clean.eod_payment_recovery
SELECT DISTINCT
                p.payment_id,
                o.master_order_number,
                'P',
		'N',
    p.cc_auth_provider
        FROM
                clean.payments p
        JOIN    clean.orders o
        ON      p.order_guid = o.order_guid
        JOIN    clean.order_details od
        ON      o.order_guid = od.order_guid
        AND     (od.order_disp_code IN ('Processed','Shipped','Printed')
                OR (nvl(o.origin_id, 'FOX') = 'AMZNI' and od.order_disp_code = 'Validated'))
        WHERE   p.updated_on >= trunc(in_date-180) and p.updated_on < TRUNC(in_date+1)
        AND     p.credit_amount > 0
        AND     p.payment_indicator = 'P'
        AND     ((in_cc_auth_provider is null and p.cc_auth_provider is null) or (in_cc_auth_provider is not null and p.cc_auth_provider = in_cc_auth_provider))
        AND	p.bill_status = 'Unbilled'
        AND NOT EXISTS (
	        select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
                and (nvl(o.origin_id, 'FOX') <> 'AMZNI' )
	        and od2.order_disp_code in ('Validated', 'Held'))
        AND NOT EXISTS (
        	select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
        	and sod.status in ('2001','2002','2003','2005','2007'))
        AND
        (	(p.payment_type = 'NC')
                OR
                ((p.payment_type = 'IN')
                  AND
                  (od.order_disp_code in ('Processed','Shipped','Printed')
                      OR (nvl(o.origin_id, 'FOX') = 'AMZNI' and od.order_disp_code = 'Validated') )
                )
                OR
                (p.payment_type = 'GC')
                OR
                ( p.payment_type IN ('AX','CB','DC','DI','MC','VI') )
        )AND (p.route is null or (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'N'));

insert into clean.eod_payment_recovery  --refunds
            SELECT p.payment_id,
            	   o.master_order_number,
            	   'R',
		   'N',
    p.cc_auth_provider
            FROM payments p,
                 orders o,
                 order_details od,
                 refund r
           WHERE r.updated_on >= trunc(in_date-180) and r.updated_on < TRUNC(in_date+1)
             AND r.refund_status = 'Unbilled'
             AND r.order_detail_id = od.order_detail_id
             AND od.order_disp_code IN ('Processed','Shipped','Printed')
             AND NOT EXISTS (
                select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
                 and od2.order_disp_code in ('Validated', 'Held'))
             AND NOT EXISTS (
        	select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
        	and sod.status in ('2001','2002','2003','2005','2007'))
             AND NVL(o.origin_id,'FOX') NOT IN ('AMZNI')
             AND p.order_guid = o.order_guid
             AND p.refund_id = r.refund_id
             AND p.debit_amount > 0
             AND p.payment_indicator = 'R'
             AND ((in_cc_auth_provider is null and p.cc_auth_provider is null) or (in_cc_auth_provider is not null and p.cc_auth_provider = in_cc_auth_provider))
	     AND
        	((p.payment_type = 'NC')
        	 OR
        	 (p.payment_type = 'GC')
                 OR
                ((p.payment_type = 'IN')
                  AND
                  (od.order_disp_code in ('Processed','Shipped','Printed') )
                )
                 OR
                 ( p.payment_type IN ('AX','CB','DC','DI','MC','VI') AND p.cc_id IS NOT NULL)
               ) AND (p.route is null or (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'N'));

END INSERT_PAYMENT_RECOVERY_TAB;

PROCEDURE GET_UNPROC_RECOVERY_CARTS
(
IN_CC_AUTH_PROVIDER     IN CLEAN.eod_payment_recovery.CC_AUTH_PROVIDER%TYPE,
OUT_payment_CURSOR      OUT TYPES.REF_CURSOR,
OUT_refund_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
         Retrieve all payments in CLEAN.EOD_PAYMENT_RECOVERY_TAB that are not
         billed and not already in CLEAN.BILLING_DETAIL table for cc auth
         provider passed in.
Input:
        None
Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_payment_cursor FOR
        SELECT DISTINCT
                epr.payment_id,
                epr.master_order_number,
                epr.payment_indicator
            FROM clean.eod_payment_recovery epr
           WHERE epr.payment_indicator='P'
                     AND epr.dispatched_indicator = 'N'
                     AND ((in_cc_auth_provider is null and epr.cc_auth_provider is null) or (in_cc_auth_provider is not null and epr.cc_auth_provider = in_cc_auth_provider))
           ORDER BY epr.master_order_number,
                    epr.payment_id;

OPEN out_refund_cursor FOR
       SELECT epr.payment_id,
               epr.master_order_number,
               epr.payment_indicator
         FROM clean.eod_payment_recovery epr
        WHERE epr.payment_indicator='R'
        AND ((in_cc_auth_provider is null and epr.cc_auth_provider is null) or (in_cc_auth_provider is not null and epr.cc_auth_provider = in_cc_auth_provider))
     ORDER BY epr.master_order_number,
                       epr.payment_id;

END GET_UNPROC_RECOVERY_CARTS;



PROCEDURE GET_ORDER_DETAILS
(
 in_order_detail_id      in  clean.order_details.order_detail_id%type,
 out_order_detail_cursor      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
	For the given order_details.order_detail_id, return the following from order_details table:
Input:
        None
Output:
        cursor containing order detail info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_order_detail_cursor FOR
          SELECT cod.Recipient_id,
                 cod.Product_id,
                 cod.Size_indicator,
                 cod.Ariba_po_number
            FROM clean.order_details cod
           WHERE cod.order_detail_id = in_order_detail_id;
END GET_ORDER_DETAILS;

PROCEDURE CREATE_HEAD_PERS_RECOV_REC
(
    IN_BILLING_BATCH_DATE            IN BILLING_HEADER.BILLING_BATCH_DATE%TYPE,
    IN_CC_AUTH_PROVIDER              IN BILLING_HEADER.CC_AUTH_PROVIDER%TYPE,
    OUT_SEQUENCE_NUMBER             OUT BILLING_HEADER.BILLING_HEADER_ID%TYPE,
    OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2
)

AS
/*-----------------------------------------------------------------------------
Description:
    Process Billing
Input:
    IN_BILLING_BATCH_DATE            IN BILLING_HEADER.BILLING_BATCH_DATE%TYPE,
    OUT_SEQUENCE_NUMBER             OUT BILLING_HEADER.BILLING_HEADER_ID%TYPE,
Output:
    OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2

-----------------------------------------------------------------------------*/
v_count NUMBER;

BEGIN
    out_status := 'Y';

    SELECT count(1)
    INTO v_count
    FROM eod_payment_recovery
    WHERE ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider));

    IF v_count = 0 THEN
    	CREATE_BILLING_HEADER(IN_BILLING_BATCH_DATE, IN_CC_AUTH_PROVIDER,
                          OUT_SEQUENCE_NUMBER,
                          OUT_STATUS,
                          OUT_MESSAGE);
     	INSERT_PAYMENT_RECOVERY_TAB(IN_BILLING_BATCH_DATE, IN_CC_AUTH_PROVIDER);
    ELSE
    	out_status := 'N';
    	out_message := 'CLEAN.EOD_PAYMENT_RECOVERY not empty';
    END IF;

EXCEPTION
  WHEN others then
    out_status := 'N';
   OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);



END CREATE_HEAD_PERS_RECOV_REC;

PROCEDURE FIND_ORDER_NUMBER
(
IN_ORDER_NUMBER                 IN VARCHAR2,
OUT_ORDER_DETAIL_ID             OUT ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_ORDER_GUID                  OUT ORDER_DETAILS.ORDER_GUID%TYPE,
OUT_EXTERNAL_ORDER_NUMBER       OUT ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_MASTER_ORDER_NUMBER         OUT ORDERS.MASTER_ORDER_NUMBER%TYPE,
OUT_CUSTOMER_ID                 OUT ORDERS.CUSTOMER_ID%TYPE,
OUT_ORDER_DISP_CODE             OUT ORDER_DETAILS.ORDER_DISP_CODE%TYPE,
OUT_ORDER_INDICATOR             OUT VARCHAR2
)AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning order detail id of a
        master_order_number .

Input:
        order_number                    varchar2 -- can be order detail id, master order number, external order number, hp order number, amazon or walmart order number
Output:
        order_detail_id                 number
-----------------------------------------------------------------------------*/

v_order_number          NUMBER;

CURSOR mon_cur IS
        SELECT  od.order_detail_id,
--                od.order_guid
                od.external_order_number,
                o.master_order_number,
                o.customer_id,
                od.order_disp_code,
                'master_order_number' order_indicator
        FROM    clean.order_details od
        JOIN    clean.orders o
        ON      od.order_guid = o.order_guid
                --eso modified removed the substr
                --WHERE   substr(o.master_order_number, 1, instr (o.master_order_number, '/') - 1) = upper (in_order_number )
        WHERE   o.master_order_number = UPPER (in_order_number )
        ORDER BY od.order_detail_id;


BEGIN
      OPEN mon_cur;
                 FETCH mon_cur INTO out_order_detail_id , --out_order_guid,
                 out_external_order_number, out_master_order_number,
                 out_customer_id, out_order_disp_code, out_order_indicator;
        CLOSE mon_cur;
END FIND_ORDER_NUMBER;


PROCEDURE CREATE_BILLING_HEADER
(
    IN_BILLING_BATCH_DATE            IN BILLING_HEADER.BILLING_BATCH_DATE%TYPE,
    IN_CC_AUTH_PROVIDER              IN CLEAN.billing_header.cc_auth_provider%type,
    OUT_SEQUENCE_NUMBER             OUT BILLING_HEADER.BILLING_HEADER_ID%TYPE,
    OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   Creates a billing record for End of Day

Input:
        billing_batch_date             date

Output:
        billing_header_id                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

SELECT billing_header_id_seq.NEXTVAL INTO out_sequence_number FROM DUAL;

INSERT INTO billing_header
(
        billing_header_id,
        billing_batch_date,
        processed_indicator,
        cc_auth_provider,
        created_on
)
VALUES
(

        out_sequence_number,
        in_billing_batch_date,
        'N',
        in_cc_auth_provider,
        SYSDATE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_BILLING_HEADER;


PROCEDURE UPDATE_BILLING_HEADER
(
 IN_BILLING_HEADER_IDS       IN VARCHAR2,
 IN_PROCESSED_INDICATOR      IN BILLING_HEADER.PROCESSED_INDICATOR%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
   This procedure updates a billing_header's status for a given
    billing_header_id.

Input:
        billing_header_id       VARCHAR2
        processed_indicator     CHAR

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/

Pragma Autonomous_Transaction;

BEGIN

  EXECUTE IMMEDIATE('UPDATE billing_header
                        SET processed_indicator =''' || in_processed_indicator ||
                  ''' WHERE billing_header_id IN (' || in_billing_header_ids || ')');

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No billing_header record was updated for billing_header_ids ' || in_billing_header_ids ||'.';
  END IF;
  
commit;

  EXCEPTION WHEN OTHERS THEN
   BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END UPDATE_BILLING_HEADER;


PROCEDURE CREATE_BILLING_DETAIL
(

        IN_BILLING_HEADER_ID        IN    BILLING_DETAIL.BILLING_HEADER_ID%TYPE,
        IN_ORDER_NUMBER             IN    BILLING_DETAIL.ORDER_NUMBER%TYPE,
        IN_ORDER_SEQUENCE           IN    BILLING_DETAIL.ORDER_SEQUENCE%TYPE,
        IN_PAYMENT_ID               IN    BILLING_DETAIL.PAYMENT_ID%TYPE,
        IN_CC_TYPE                  IN    BILLING_DETAIL.CC_TYPE%TYPE,
        IN_CC_NUMBER                IN    BILLING_DETAIL.CC_NUMBER%TYPE,
        IN_CC_EXPIRATION            IN    BILLING_DETAIL.CC_EXPIRATION%TYPE,
        IN_PAYMENT_METHOD           IN    BILLING_DETAIL.PAYMENT_METHOD%TYPE,
        IN_ORDER_AMOUNT             IN    BILLING_DETAIL.ORDER_AMOUNT%TYPE,
        IN_SALES_TAX_AMOUNT         IN    BILLING_DETAIL.SALES_TAX_AMOUNT%TYPE,
        IN_TRANSACTION_TYPE         IN    BILLING_DETAIL.TRANSACTION_TYPE%TYPE,
        IN_AUTH_DATE                IN    BILLING_DETAIL.AUTH_DATE%TYPE,
        IN_AUTH_APPROVAL_CODE       IN  BILLING_DETAIL.AUTH_APPROVAL_CODE%TYPE,
        IN_AUTH_CHAR_INDICATOR      IN  BILLING_DETAIL.AUTH_CHAR_INDICATOR%TYPE,
        IN_AUTH_TRANSACTION_ID      IN  BILLING_DETAIL.AUTH_TRANSACTION_ID%TYPE,
        IN_AUTH_VALIDATION_CODE    IN   BILLING_DETAIL.AUTH_VALIDATION_CODE%TYPE,
        IN_AUTH_SOURCE_CODE         IN  BILLING_DETAIL.AUTH_SOURE_CODE%TYPE,
        IN_AVS_CODE                 IN  BILLING_DETAIL.AVS_CODE%TYPE,
        IN_RESPONSE_CODE            IN  BILLING_DETAIL.RESPONSE_CODE%TYPE,
        IN_CLEARING_MEMBER_NUMBER  IN   BILLING_DETAIL.CLEARING_MEMBER_NUMBER%TYPE,
        IN_RECIPIENT_ZIP_CODE       IN  BILLING_DETAIL.RECIPIENT_ZIP_CODE%TYPE,
        IN_ORIGIN_TYPE      IN  BILLING_DETAIL.ORIGIN_TYPE%TYPE,
        IN_ORDER_DATE               IN  BILLING_DETAIL.ORDER_DATE%TYPE,
        IN_BILL_TYPE                IN  BILLING_DETAIL.BILL_TYPE%TYPE,
        IN_CANCEL_DATE              IN  BILLING_DETAIL.CANCEL_DATE%TYPE,
        IN_CARDHOLDER_REF           IN  BILLING_DETAIL.CARDHOLDER_REF%TYPE,
        IN_ADD_INFO1                IN  BILLING_DETAIL.ADD_INFO1%TYPE,
        IN_ADD_INFO2                IN  BILLING_DETAIL.ADD_INFO2%TYPE,
        IN_ADD_INFO3                IN  BILLING_DETAIL.ADD_INFO3%TYPE,
        IN_ADD_INFO4                IN  BILLING_DETAIL.ADD_INFO4%TYPE,
        IN_COMPANY_ID               IN  BILLING_DETAIL.COMPANY_ID%TYPE,
        IN_ACQ_REFERENCE_NUMBER     IN  BILLING_DETAIL.ACQ_REFERENCE_NUMBER%TYPE,
        IN_WALLET_INDICATOR			IN	BILLING_DETAIL.WALLET_INDICATOR%TYPE,
        IN_CSC_RESPONSE_CODE        IN  BILLING_DETAIL.CSC_RESPONSE_CODE%TYPE,
        IN_INITIAL_AUTH_AMOUNT      IN  BILLING_DETAIL.INITIAL_AUTH_AMOUNT%TYPE,
		IN_VOICE_AUTH_FLAG          IN  BILLING_DETAIL.IS_VOICE_AUTH%TYPE,
        OUT_SEQUENCE_NUMBER             OUT BILLING_DETAIL.BILLING_DETAIL_ID%TYPE,
        OUT_STATUS                      OUT VARCHAR2,
        OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   Creates a billing record for End of Day

Input:


Output:
        batch_number                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

   v_key_name billing_detail.key_name%type;
   V_RECIPIENT_ZIP_CODE     BILLING_DETAIL.RECIPIENT_ZIP_CODE%TYPE;


BEGIN

SELECT billing_detail_id_seq.NEXTVAL INTO out_sequence_number FROM DUAL;
v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');
V_RECIPIENT_ZIP_CODE :=      IN_RECIPIENT_ZIP_CODE;

if  IN_RECIPIENT_ZIP_CODE is null then
   if (in_add_info1 is not null  and in_cc_TYPE = 'AX') then
      V_RECIPIENT_ZIP_CODE :=  '60515';
   end if;
end if;


INSERT INTO billing_detail
(

      billing_detail_id,
      billing_header_id,
      order_number,
      order_sequence,
      payment_id,
      cc_type,
      cc_number,
      key_name,
      cc_expiration,
      payment_method,
      order_amount,
      sales_tax_amount,
      transaction_type,
      auth_date,
      auth_approval_code,
      auth_char_indicator,
      auth_transaction_id,
      auth_validation_code,
      auth_soure_code,
      avs_code,
      response_code,
      clearing_member_number,
      recipient_zip_code,
      origin_type,
      order_date,
      bill_type,
      cancel_date,
      cardholder_ref,
      add_info1,
      add_info2,
      add_info3,
      add_info4,
      company_id,
      created_on,
      acq_reference_number,
      wallet_indicator,
      csc_response_code,
      initial_auth_amount,
      is_voice_auth
)
VALUES
(

      out_sequence_number,
      in_billing_header_id,
      in_order_number,
      in_order_sequence,
      in_payment_id,
      in_cc_type,
      decode(ftd_apps.payment_method_is_cc(in_payment_method),'Y',global.encryption.encrypt_it(in_cc_number,v_key_name),null),
      v_key_name,
      in_cc_expiration,
      in_payment_method,
      in_order_amount,
      in_sales_tax_amount,
      in_transaction_type,
      in_auth_date,
      in_auth_approval_code,
      in_auth_char_indicator,
      in_auth_transaction_id,
      in_auth_validation_code,
      in_auth_source_code,
      in_avs_code,
      in_response_code,
      in_clearing_member_number,
      v_recipient_zip_code,
      in_origin_type,
      in_order_date,
      in_bill_type,
      in_cancel_date,
      in_cardholder_ref,
      in_add_info1,
      in_add_info2,
      in_add_info3,
      in_add_info4,
      in_company_id,
      SYSDATE,
      in_acq_reference_number,
      in_wallet_indicator,
      in_csc_response_code,
      in_initial_auth_amount,
      in_voice_auth_flag

);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_BILLING_DETAIL;


FUNCTION FIND_PROGRAM_NAME_OF_SOURCE
(
 IN_SOURCE_CODE  IN FTD_APPS.SOURCE_PROGRAM_REF.SOURCE_CODE%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
   This function is called by GET_PAYMENT_BY_ID to retrieve a single
   program_name for the moset recent start date of a source code.
   Note: That the program name returned is just first one found.

Input:
        payment_id    NUMBER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/
  v_program_name ftd_apps.source_program_ref.program_name%TYPE;
BEGIN
  BEGIN
    SELECT program_name
      INTO v_program_name
      FROM ftd_apps.source_program_ref
     WHERE source_code = IN_SOURCE_CODE
       AND ROWNUM = 1
       AND start_date = (SELECT MAX(start_date)
                           FROM ftd_apps.source_program_ref
                          WHERE source_code = in_source_code
                            AND start_date < SYSDATE);

    EXCEPTION WHEN NO_DATA_FOUND THEN v_program_name := NULL;

  END;

  RETURN v_program_name;
END FIND_PROGRAM_NAME_OF_SOURCE;


PROCEDURE GET_PAYMENT_BY_ID
(
 IN_PAYMENT_ID        IN PAYMENTS.PAYMENT_ID%TYPE,
 OUT_PAYMENT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure retrieves payment billing records that are eligible for
   end of day processing based on the payment id passed in.

Input:
        payment_id    NUMBER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_payment_cursor FOR
          SELECT p.payment_id,
                 p.order_guid,
                 o.master_order_number,
                 p.additional_bill_id,
                 p.payment_type,
                 p.cc_id,
                 cc.cc_type,
                 decode (ftd_apps.payment_method_is_cc(p.payment_type),
                         'Y', global.encryption.decrypt_it(cc.cc_number,cc.key_name),
                         null) cc_number,
                 cc.cc_expiration,
                 p.auth_result,
                 p.auth_number,
                 p.avs_code,
                 p.acq_reference_number,
                 p.gc_coupon_number,
                 p.credit_amount,
                 p.debit_amount,
                 DECODE (p.additional_bill_id, NULL,
                         (
                                SELECT  SUM (ob.tax)
                                FROM    order_bills ob, order_details od
                                WHERE   ob.additional_bill_indicator = 'N'
                                AND     od.order_detail_id = ob.order_detail_id
                                AND     od.order_guid = o.order_guid
                        ),
                        (
                                SELECT  ob.tax
                                FROM    order_bills ob
                                WHERE   ob.additional_bill_indicator = 'Y'
                                AND     ob.order_bill_id = p.additional_bill_id
                        )
                 ) tax_total,
                 p.payment_indicator,
                 p.refund_id,
                 p.auth_date,
                 p.auth_number,
                 p.auth_char_indicator,
                 p.transaction_id,
                 p.validation_code,
                 p.auth_source_code,
                 p.response_code,
                 p.created_on,
                 o.company_id,
                 billing_end_of_day_pkg.find_program_name_of_source(o.source_code) program_name,
                 s.order_source,
                 p.created_on, 
                 o.mp_redemption_rate_amt,
                 p.aafes_ticket_number,
                 p.ap_auth_txt,
                 global.encryption.decrypt_it(cc.gift_card_pin,cc.key_name) gift_card_pin,
                 p.wallet_indicator,
                 p.csc_response_code,
                 p.initial_auth_amount,
                 p.is_voice_auth
            FROM credit_cards cc,
                 orders o,
                 payments p,
                 ftd_apps.source s
           WHERE p.payment_id = in_payment_id
             AND cc.cc_id (+) = p.cc_id
             AND p.order_guid = o.order_guid
             AND o.source_code = s.source_code;

END GET_PAYMENT_BY_ID;


PROCEDURE GET_REFUND_BY_PAYMENT_ID
(
 IN_payment_id        IN PAYMENTS.PAYMENT_ID%TYPE,
 OUT_refund_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure retrieves the refund record based on the payment id passed in.

Input:
        payment_id    NUMBER

Output:
        cursor containing refund info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_refund_cursor FOR
          SELECT r.refund_id,
		 r.refund_disp_code,
		 r.order_detail_id,
		 r.responsible_party,
		 r.refund_status,
		 r.created_on,
		 p.payment_type,
		 p.debit_amount,
		 cc.cc_type,
                 decode(ftd_apps.payment_method_is_cc(p.payment_type),
                           'Y',global.encryption.decrypt_it(cc.cc_number,cc.key_name),
                           null) cc_number,
		 cc.cc_expiration,
		 o.master_order_number,
		 o.order_guid,
		 o.company_id,
		 s.order_source,
		 p.payment_id,
		 billing_end_of_day_pkg.find_program_name_of_source(o.source_code) program_name,
		 p.created_on
	     FROM credit_cards cc,
		  payments p,
		  ftd_apps.source s,
		  orders o,
		  refund r
	    WHERE p.payment_id = in_payment_id
	      AND r.refund_status = 'Unbilled'
	      AND o.source_code = s.source_code
	      AND p.order_guid = o.order_guid
	      AND p.cc_id = cc.cc_id (+)
	      AND p.refund_id = r.refund_id;


END GET_REFUND_BY_PAYMENT_ID;


FUNCTION GET_CUST_NAME_BY_PAYMENT_ID
(
 IN_PAYMENT_ID  IN NUMBER
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure the first and last name of the customer
        related to the payment id passed in.

Input:
        payment_id              NUMBER

Output:
        varchar2

-----------------------------------------------------------------------------*/
v_first_name customer.first_name%TYPE;
v_last_name customer.last_name%TYPE;

BEGIN

  BEGIN
    SELECT c.first_name,
           c.last_name
      INTO v_first_name,
           v_last_name
      FROM customer c,
           orders o,
           payments p
     WHERE p.payment_id = in_payment_id
       AND p.order_guid = o.order_guid
       AND o.customer_id = c.customer_id;

    EXCEPTION WHEN NO_DATA_FOUND THEN
      BEGIN
        v_first_name := NULL;
        v_last_name := NULL;
      END;
  END;

  RETURN v_first_name || ' ' || v_last_name;

END GET_CUST_NAME_BY_PAYMENT_ID;


FUNCTION GET_CLEARING_MEMBER_NUMBER
(
 IN_PAYMENT_ID  IN NUMBER
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns the clearing_member_number for the company
        associated with the order for the payment id passed in.

Input:
        payment_id                NUMBER

Output:
        v_clearing_member_number  VARCHAR2

-----------------------------------------------------------------------------*/
v_clearing_member_number ftd_apps.company_master.clearing_member_number%TYPE;

BEGIN

  BEGIN

    SELECT cm.clearing_member_number
      INTO v_clearing_member_number
      FROM payments p,
           orders o,
           ftd_apps.company_master cm
     WHERE p.payment_id = in_payment_id
       AND o.order_guid = p.order_guid
       AND o.company_id = cm.company_id;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_clearing_member_number := NULL;

  END;

  RETURN v_clearing_member_number;

END GET_CLEARING_MEMBER_NUMBER;


PROCEDURE GET_PARTNER_CO_BRAND_FIELDS
(
 IN_PAYMENT_ID   IN NUMBER,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns the co brand info related to the payment id
        passed in.

Input:
        payment id      NUMBER

Output:
        out_cursor      REF_CURSOR

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT c.co_brand_id,
           c.order_guid,
           c.info_name,
           c.info_data
      FROM payments p,
           co_brand c
     WHERE p.payment_id = in_payment_id
       AND p.order_guid = c.order_guid;

END GET_PARTNER_CO_BRAND_FIELDS;


PROCEDURE GET_PAYMENT_TOTALS
(
 IN_PAYMENT_ID           IN NUMBER,
 OUT_SERVICE_FEE_TOTAL  OUT NUMBER,
 OUT_SHIPPING_FEE_TOTAL OUT NUMBER,
 OUT_SHIPPING_TAX_TOTAL OUT NUMBER,
 OUT_TAX_TOTAL          OUT NUMBER
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure will sum and return the total service fee,
        shipping fee, shipping tax and tax of all order bills related to
        the payment id passed in.

Input:
        payment id      NUMBER

Output:
        service fee total       NUMBER
        shipping fee total      NUMBER
        shipping tax total      NUMBER
        tax total               NUMBER

-----------------------------------------------------------------------------*/
v_order_bill_id payments.additional_bill_id%TYPE;
v_order_guid    payments.order_guid%TYPE;

BEGIN

  SELECT additional_bill_id,
         order_guid
    INTO v_order_bill_id,
         v_order_guid
    FROM payments
   WHERE payment_id = in_payment_id;

  IF v_order_bill_id IS NULL THEN
    SELECT SUM(ob.service_fee),
           SUM(ob.shipping_fee),
           SUM(ob.shipping_tax),
           SUM(ob.tax)
      INTO out_service_fee_total,
           out_shipping_fee_total,
           out_shipping_tax_total,
           out_tax_total
      FROM order_bills ob,
           order_details od
     WHERE od.order_guid = v_order_guid
       AND ob.order_detail_id = od.order_detail_id;
  ELSE
    SELECT SUM(service_fee),
           SUM(shipping_fee),
           SUM(shipping_tax),
           SUM(tax)
      INTO out_service_fee_total,
           out_shipping_fee_total,
           out_shipping_tax_total,
           out_tax_total
      FROM order_bills
     WHERE order_bill_id = v_order_bill_id;
  END IF;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      out_service_fee_total := NULL;
      out_shipping_fee_total := NULL;
      out_shipping_tax_total := NULL;
      out_tax_total := NULL;
    END;

END GET_PAYMENT_TOTALS;


PROCEDURE UPDATE_BILL_REFUND_STATUS
(
 IN_PAYMENT_ID IN NUMBER,
 IN_REFUND_ID  IN NUMBER,
 IN_TIMESTAMP  IN DATE,
 IN_STATUS     IN VARCHAR2,
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the bill_status in the
        payments table or the refund_status of the refund table for the
        payment id or refund id passed in respectively.

Input:
        payment_id           NUMBER
        refund_id            NUMBER
        timestamp            DATE
        status               VARCHAR2

Output:
        status               VARCHAR2 (Y or N)
        message              VARCHAR2 (error message)
-----------------------------------------------------------------------------*/
v_order_guid payments.order_guid%TYPE;
v_order_bill_id payments.additional_bill_id%TYPE;

BEGIN

  IF in_refund_id IS NULL THEN
    BEGIN
      SELECT additional_bill_id,
             order_guid
        INTO v_order_bill_id,
             v_order_guid
        FROM payments
       WHERE payment_id = in_payment_id;

      IF v_order_bill_id IS NULL THEN
        IF in_status = 'Billed' THEN
          UPDATE payments
             SET bill_status = in_status,
                 bill_date   = in_timestamp,
                 updated_on    = SYSDATE
           WHERE payment_id = in_payment_id;
        ELSE
          UPDATE order_bills
             SET bill_status = in_status,
                 settled_date   = in_timestamp,
                 updated_on    = SYSDATE
           WHERE additional_bill_indicator = 'N'
             AND order_detail_id IN (SELECT order_detail_id
                                       FROM order_details
                                      WHERE order_guid = v_order_guid);
        END IF;

        IF SQL%FOUND THEN
           out_status := 'Y';
        ELSE
           out_status := 'N';
           out_message := 'WARNING: No order_bills records updated for payment_id ' || in_payment_id ||'.';
        END IF;

      ELSE
        IF in_status = 'Billed' THEN
          UPDATE payments
             SET bill_status = in_status,
                 bill_date   = in_timestamp,
                 updated_on    = SYSDATE
           WHERE additional_bill_id = v_order_bill_id;
        ELSE
          UPDATE order_bills
             SET bill_status = in_status,
                 settled_date = in_timestamp,
                 updated_on    = SYSDATE
           WHERE order_bill_id = v_order_bill_id;
        END IF;

        IF SQL%FOUND THEN
           out_status := 'Y';
        ELSE
           out_status := 'N';
           out_message := 'WARNING: No order_bills records updated for payment_id ' || in_payment_id ||'.';
        END IF;

      END IF;

      EXCEPTION WHEN NO_DATA_FOUND THEN
        BEGIN
           out_status := 'N';
           out_message := 'WARNING: No payments records found for payment_id ' || in_payment_id ||'.';
        END;
    END;

  ELSE
    UPDATE payments
         SET bill_status = in_status,
             bill_date = in_timestamp,
             updated_on    = SYSDATE
    WHERE refund_id = in_refund_id;
    
    IF SQL%FOUND THEN
         out_status := 'Y';
    ELSE
         out_status := 'N';
         out_message := 'WARNING: No refund records updated for refund_id ' || in_refund_id ||'.';
    END IF;
  END IF;


  EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_BILL_REFUND_STATUS;


PROCEDURE GET_BILLING_DETAILS
(
 IN_CC_AUTH_PROVIDER   IN CLEAN.BILLING_HEADER.CC_AUTH_PROVIDER%TYPE,
 OUT_CURSOR            OUT TYPES.REF_CURSOR,
 OUT_CURSOR2           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns all the billing_details records
         related to a billing_header that has to be processed.

Input:
        N/A

Output:
        cursor containing billing_detail info

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
   SELECT billing_detail_id,
          billing_header_id,
          order_number,
          order_sequence,
          payment_id,
	  null refund_id,
          cc_type,
          global.encryption.decrypt_it(cc_number,key_name) cc_number,
          cc_expiration,
          payment_method,
          order_amount,
          sales_tax_amount,
          transaction_type,
          auth_date,
          auth_approval_code,
          auth_char_indicator,
          auth_transaction_id,
          auth_validation_code,
          auth_soure_code,
          avs_code,
          response_code,
          clearing_member_number,
          recipient_zip_code,
          origin_type,
          order_date,
          bill_type,
          cancel_date,
          cardholder_ref,
          add_info1,
          add_info2,
          add_info3,
          add_info4,
          company_id,
          '1' sort_order,
          acq_reference_number,
          wallet_indicator,
		  initial_auth_amount,
		  is_voice_auth,
 	  	  csc_response_code,
 	  	  (select p.cardinal_verified_flag from clean.payments p where billing_detail.PAYMENT_ID = p.payment_id) as cardinal_verified_flag
     FROM billing_detail
    WHERE billing_header_id IN (SELECT billing_header_id
                                  FROM billing_header
                                 WHERE processed_indicator = 'N' 
                                 and ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider)))
      AND bill_type = 'Bill'
  UNION
   SELECT billing_detail_id,
          billing_header_id,
          order_number,
          order_sequence,
          payment_id,
	  null refund_id,
          cc_type,
          global.encryption.decrypt_it(cc_number,key_name) cc_number,
          cc_expiration,
          payment_method,
          order_amount,
          sales_tax_amount,
          transaction_type,
          auth_date,
          auth_approval_code,
          auth_char_indicator,
          auth_transaction_id,
          auth_validation_code,
          auth_soure_code,
          avs_code,
          response_code,
          clearing_member_number,
          recipient_zip_code,
          origin_type,
          order_date,
          bill_type,
          cancel_date,
          cardholder_ref,
          add_info1,
          add_info2,
          add_info3,
          add_info4,
          company_id,
          '2' sort_order,
          acq_reference_number,
          wallet_indicator,
		  initial_auth_amount,
		  is_voice_auth,
 	  	  csc_response_code,
 	  	  null cardinal_verified_flag
     FROM billing_detail
    WHERE billing_header_id IN (SELECT billing_header_id
                                  FROM billing_header
                                 WHERE processed_indicator = 'N'
                                 and ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider)))
      AND bill_type = 'Add_Bill'
  UNION
   SELECT bd.billing_detail_id,
          bd.billing_header_id,
          bd.order_number,
          bd.order_sequence,
          bd.payment_id,
	  	  p.refund_id,
          bd.cc_type,
          global.encryption.decrypt_it(bd.cc_number,bd.key_name) cc_number,
          bd.cc_expiration,
          bd.payment_method,
          bd.order_amount,
          bd.sales_tax_amount,
          bd.transaction_type,
          bd.auth_date,
          bd.auth_approval_code,
          bd.auth_char_indicator,
          bd.auth_transaction_id,
          bd.auth_validation_code,
          bd.auth_soure_code,
          bd.avs_code,
          bd.response_code,
          bd.clearing_member_number,
          bd.recipient_zip_code,
          bd.origin_type,
          bd.order_date,
          bd.bill_type,
          bd.cancel_date,
          bd.cardholder_ref,
          bd.add_info1,
          bd.add_info2,
          bd.add_info3,
          bd.add_info4,
          bd.company_id,
          '3' sort_order,
          bd.acq_reference_number,
          bd.wallet_indicator,
		  bd.initial_auth_amount,
		  bd.is_voice_auth,
 	  	  bd.csc_response_code,
 	  	  null cardinal_verified_flag
     FROM billing_detail bd,payments p
    WHERE bd.billing_header_id IN (SELECT billing_header_id
                                  FROM billing_header
                                 WHERE processed_indicator = 'N'
                                 and ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider)))
      AND bd.bill_type = 'Refund'
      AND bd.payment_id=p.payment_id
    ORDER BY sort_order, company_id, cc_type, order_number;

    OPEN out_cursor2 FOR
     SELECT DISTINCT billing_header_id
       FROM billing_header
      WHERE processed_indicator = 'N'
	  and ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider));

END GET_BILLING_DETAILS;

PROCEDURE GET_ADD_BILL
(
 IN_PAYMENT_ID   IN NUMBER,
 OUT_CURSOR     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the order_bills record
          for the payment_id passed in.

Input:
        payment id      NUMBER

Output:
        out_cursor      REF_CURSOR

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
    SELECT order_bill_id,
           order_detail_id,
           product_amount,
           add_on_amount,
           service_fee,
           shipping_fee,
           discount_amount,
           shipping_tax,
           tax,
           additional_bill_indicator,
           NULL same_day_fee,           -- field not used and dropped from table
           ob.bill_status,
           ob.bill_date,
           service_fee_tax,
           commission_amount,
           wholesale_amount,
           transaction_amount,
           pdb_price,
           discount_product_price,
           discount_type,
           partner_cost
      FROM payments p,
           order_bills ob
     WHERE p.payment_id = in_payment_id
       AND ob.order_bill_id = p.additional_bill_id;

END GET_ADD_BILL;


PROCEDURE GET_SAME_DAY_PAYMENTS
(
 IN_REFUND_ID     IN REFUND.REFUND_ID%TYPE,
 OUT_CURSOR      OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning a list of
        payment ids for the given refund id.

Input:
        refund_id                 number

Output:
        cursor containing payment information

-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
     SELECT p.payment_id
       FROM payments p,
            order_details od,
            refund r
      WHERE p.refund_id = in_refund_id
        AND p.cc_id IS NOT NULL
        AND r.refund_id = p.refund_id
        AND TRUNC(r.created_on) = TRUNC(p.created_on)
        AND p.order_guid = od.order_guid
        AND od.order_detail_id = r.order_detail_id
      ORDER BY p.created_on;

END GET_SAME_DAY_PAYMENTS;


PROCEDURE GET_PAYMENT_INFO
(
 IN_PAYMENT_ID IN PAYMENTS.PAYMENT_ID%TYPE,
 OUT_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns payment information for the given payment id.

Input:
        payment_id      NUMBER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
      SELECT payment_id,
             order_guid,
             additional_bill_id,
             payment_type,
             cc_id,
             auth_result,
             auth_number,
             avs_code,
             acq_reference_number,
             gc_coupon_number,
             auth_override_flag,
             credit_amount,
             debit_amount,
             payment_indicator,
             refund_id,
             auth_date,
             auth_number,
             auth_char_indicator,
             transaction_id,
             validation_code,
             auth_source_code,
             response_code
        FROM payments
       WHERE payment_id = in_payment_id;


END GET_PAYMENT_INFO;


PROCEDURE GET_CREDIT_CARD_INFO
(
 IN_CC_ID   IN CREDIT_CARDS.CC_ID%TYPE,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure returns credit card information for the given credit card id.

Input:
        cc_number    VARCHAR2

Output:
        cursor containing credit card info

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
      SELECT cc_id,
             cc_type,
             global.encryption.decrypt_it(cc_number,key_name) cc_number,
             cc_expiration,
             name,
             address_line_1,
             address_line_2,
             city,
             state,
             zip_code,
             country,
             customer_id
        FROM credit_cards
       WHERE cc_id = in_cc_id;

END GET_CREDIT_CARD_INFO;


FUNCTION SHOULD_REFUND_BE_BILLED
(
 IN_DEBIT_PAYMENT_ID  IN PAYMENTS.PAYMENT_ID%TYPE
)
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure sums up the total credit amounts and debits
        to determine if a refund should be made for the payment id passed in.

Input:
        debit_payment_id              NUMBER

Output:
        char

-----------------------------------------------------------------------------*/
v_credit_amt_total payments.credit_amount%TYPE := 0;
v_debit_amt_total  payments.debit_amount%TYPE  := 0;
v_debit_amt        payments.debit_amount%TYPE  := 0;
v_cc_id            payments.cc_id%TYPE;

BEGIN

  -- get the refund amount and credit card id used
  BEGIN
    SELECT debit_amount - NVL (credit_amount, 0) debit_amount,
           cc_id cc_id
      INTO v_debit_amt,
           v_cc_id
      FROM payments
     WHERE payment_id = in_debit_payment_id;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_debit_amt := 0;
  END;

  -- get the sum of the already processed credit amounts
  SELECT SUM(p.credit_amount - NVL (p.debit_amount, 0))
    INTO v_credit_amt_total    
    FROM clean.payments p
   WHERE EXISTS (SELECT 1
                   FROM payments p2
                   WHERE p2.payment_id = in_debit_payment_id
                    AND p2.order_guid = p.order_guid)
     AND p.bill_status in ('Billed','Settled')
     AND p.credit_amount > 0
     AND p.payment_indicator = 'P'
     AND p.payment_type NOT IN ('IN','GC','NC')
     AND p.cc_id = v_cc_id;

  -- get the sum of the already processed refund amounts
  SELECT SUM(p.debit_amount - NVL (p.credit_amount, 0))
    INTO v_debit_amt_total
    FROM payments p,
         refund r
   WHERE EXISTS
         (SELECT 1
            FROM payments p2
           WHERE p2.payment_id = in_debit_payment_id
             AND p2.order_guid = p.order_guid)
     AND r.refund_id = p.refund_id
     AND r.refund_status in ('Billed','Settled')
     AND p.debit_amount > 0
     AND (
          p.payment_indicator = 'R'
          OR
          (
           p.payment_indicator = 'B'
           AND
           p.credit_amount < p.debit_amount
          )
         )
     AND p.payment_type NOT IN ('IN','GC','NC')
     AND p.cc_id = v_cc_id;


RETURN NVL (v_credit_amt_total, 0) - NVL (v_debit_amt_total, 0);

END SHOULD_REFUND_BE_BILLED;


FUNCTION SHOULD_ADD_BILL_BE_BILLED
(
 IN_ADD_BILL_PAYMENT_ID  IN PAYMENTS.PAYMENT_ID%TYPE
)
RETURN CHAR
IS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure test to see if the original bill for the
        shopping cart has been billed for the payment id passed in.

Input:
        add_bill_payment_id              NUMBER

Output:
        char

-----------------------------------------------------------------------------*/
v_count NUMBER;

BEGIN

  BEGIN
    SELECT COUNT(1)
      INTO v_count
      FROM payments p
           
     WHERE EXISTS (SELECT 1
                     FROM payments p2
                    WHERE p2.payment_id = in_add_bill_payment_id
                      AND p2.order_guid = p.order_guid)
       AND p.additional_bill_id IS NULL
       AND p.bill_status in ('Billed','Settled')
       AND p.credit_amount > 0
       AND p.payment_indicator = 'P';

    EXCEPTION WHEN NO_DATA_FOUND THEN v_count := 0;
  END;

  IF v_count = 0 THEN
    RETURN 'N';
  ELSE
    RETURN 'Y';
  END IF;


END SHOULD_ADD_BILL_BE_BILLED;


PROCEDURE UPDATE_REMOVED_ORDER_PAYMENT
(
 IN_DATE                IN PAYMENTS.CREATED_ON%TYPE,
 IN_CC_AUTH_PROVIDER	in PAYMENTS.CC_AUTH_PROVIDER%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure updates the payment credit amount deducting the
        removed order total amount from the cart payment.  This will
        only be done to multi order carts with an order that has
        been removed in scrub.

Input:
        date               DATE
        varchar		   CC_AUTH_PROVIDER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/
-- cursor to get the payment records that will be processed by end of day
-- This will only happen for multi order carts.  Single order carts that
-- are removed will not have a payment record in clean.
--
-- We used to have the filter condition
--      WHERE   trunc(p.created_on) <= TRUNC(in_date)
-- which required maintaining an index on the trunc(created_on) function, and
-- passing in the in_date as yesterday's date.
-- When we changed to updated_on and made it a 180-day range, we eliminated the 
-- trunc() function - that way we didn't have to make another index.  To avoid
-- making a change on the Java side, we add 1 day to the in_date passed in.
--
-- 02/06/2008 Defect 4042 fix: changing payment updated_on to be the previous day
-- so the cart payment will be included in today's EOD.
--
-- 06/17/2008 Defect 3951 enhancement: Only run this update once a day.
--
-- 04/08/2009 Defect 5048 enhancement: Only include shopping carts that have
-- items updated since the last recalculation.

CURSOR pay_cur(in_last_run_date date) IS
        SELECT DISTINCT
                p.order_guid
        FROM    clean.payments p
        JOIN    clean.orders o
        ON      p.order_guid = o.order_guid
        JOIN    ftd_apps.source s
        ON      o.source_code = s.source_code
        AND     NVL(s.jcpenney_flag,'N') <>'Y'
        JOIN    clean.order_details od
        ON      o.order_guid = od.order_guid
        AND     od.order_disp_code IN 	('Processed','Shipped','Printed')
        WHERE   1=1
        AND 	p.updated_on < TRUNC(in_date+1)
	AND	not exists
		(
			select 1
			from clean.payments p1
			where p1.order_guid = p.order_guid
			and p1.bill_status = 'Billed'
			and p1.credit_amount > 0
		)
        AND	exists
        	(
        		select 1
        		from	clean.order_details od1
        		where	od1.order_guid = p.order_guid
        		and	od1.updated_on >= in_last_run_date
        	)
        AND     p.credit_amount > 0
        AND     p.payment_indicator = 'P'
        AND	p.additional_bill_id is null
        AND     exists
                (
                        select  1
                        from    scrub.order_details sod
                        where   sod.order_guid = p.order_guid
                        group by sod.order_guid having count (1) > 1
                )
        AND     not exists
                (
                        select  1
                        from    scrub.order_details sod
                        where   sod.order_guid = p.order_guid
                        and     sod.status in ('2001','2002','2003','2005')
                )                
        AND     exists
                (
                        select  1
                        from    scrub.order_details sod
                        where   sod.order_guid = p.order_guid
                        and     sod.status = '2004'
                )
        ;

type guid_typ is table of varchar2(2000) index by pls_integer;
v_order_guid_tab        guid_typ;
v_last_run_date		varchar(20);
v_date			date;

BEGIN

	IF IN_CC_AUTH_PROVIDER IS NOT NULL THEN
		CASE IN_CC_AUTH_PROVIDER 
			WHEN 'BAMS' THEN  
				v_last_run_date := frp.misc_pkg.get_global_parm_value('ACCOUNTING_CONFIG','BAMS_UPDATE_REMOVED_ORDER_PMT_LAST_DATE');
			WHEN 'PG-EOD' THEN
				v_last_run_date := frp.misc_pkg.get_global_parm_value('ACCOUNTING_CONFIG','PG_UPDATE_REMOVED_ORDER_PMT_LAST_DATE');
			ELSE
				v_last_run_date := frp.misc_pkg.get_global_parm_value('ACCOUNTING_CONFIG','UPDATE_REMOVED_ORDER_PMT_LAST_DATE');
		END CASE;			
	ELSE
		v_last_run_date := frp.misc_pkg.get_global_parm_value('ACCOUNTING_CONFIG','UPDATE_REMOVED_ORDER_PMT_LAST_DATE');
	END IF;
	
	IF v_last_run_date IS NOT NULL THEN
		v_date := to_date(v_last_run_date, 'mm/dd/yyyy');
	ELSE
		v_date := trunc(in_date-180);
	END IF;

	IF v_date < trunc(SYSDATE) THEN
	-- The procedure has not run today.
		OPEN pay_cur(v_date);
		FETCH pay_cur BULK COLLECT INTO v_order_guid_tab;
		CLOSE pay_cur;

		FOR x IN 1..v_order_guid_tab.count LOOP
			UPDATE_REMOVED_ORD_PAY_BY_GUID(v_order_guid_tab (x),out_status,out_message);
		END LOOP;
		
	IF IN_CC_AUTH_PROVIDER IS NOT NULL THEN
     CASE IN_CC_AUTH_PROVIDER 
       WHEN 'BAMS' THEN
        frp.misc_pkg.update_global_parms
        (
         IN_CONTEXT	 => 'ACCOUNTING_CONFIG',
         IN_NAME         => 'BAMS_UPDATE_REMOVED_ORDER_PMT_LAST_DATE',
         IN_VALUE        => to_char(trunc(sysdate), 'mm/dd/yyyy'),
         IN_UPDATED_BY	 => 'UPDATE_REMOVED_ORDER_PAYMENT',
         OUT_STATUS      => out_status,
         OUT_MESSAGE     => out_message
        );
      WHEN 'PG-EOD' THEN	
        frp.misc_pkg.update_global_parms
        (
         IN_CONTEXT	 => 'ACCOUNTING_CONFIG',
         IN_NAME         => 'PG_UPDATE_REMOVED_ORDER_PMT_LAST_DATE',
         IN_VALUE        => to_char(trunc(sysdate), 'mm/dd/yyyy'),
         IN_UPDATED_BY	 => 'UPDATE_REMOVED_ORDER_PAYMENT',
         OUT_STATUS      => out_status,
         OUT_MESSAGE     => out_message
        ); 
      ELSE 
       frp.misc_pkg.update_global_parms
			(
			 IN_CONTEXT	 => 'ACCOUNTING_CONFIG',
			 IN_NAME         => 'UPDATE_REMOVED_ORDER_PMT_LAST_DATE',
			 IN_VALUE        => to_char(trunc(sysdate), 'mm/dd/yyyy'),
			 IN_UPDATED_BY	 => 'UPDATE_REMOVED_ORDER_PAYMENT',
			 OUT_STATUS      => out_status,
			 OUT_MESSAGE     => out_message
			);
     END CASE;   
	ELSE
			frp.misc_pkg.update_global_parms
			(
			 IN_CONTEXT	 => 'ACCOUNTING_CONFIG',
			 IN_NAME         => 'UPDATE_REMOVED_ORDER_PMT_LAST_DATE',
			 IN_VALUE        => to_char(trunc(sysdate), 'mm/dd/yyyy'),
			 IN_UPDATED_BY	 => 'UPDATE_REMOVED_ORDER_PAYMENT',
			 OUT_STATUS      => out_status,
			 OUT_MESSAGE     => out_message
			);
	END IF;
 END IF;

out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_REMOVED_ORDER_PAYMENT;

PROCEDURE DELETE_RECOVERY_TAB
(    
     IN_CC_AUTH_PROVIDER             IN CLEAN.eod_payment_recovery.CC_AUTH_PROVIDER%TYPE,
     OUT_STATUS                      OUT VARCHAR2,
     OUT_MESSAGE                     OUT VARCHAR2)
AS
/*-----------------------------------------------------------------------------
Description:
        DELETE from clean.eod_payment_recovery_tab.
Input:
        None
Output:
        OUT_STATUS                      OUT VARCHAR2
        OUT_MESSAGE                     OUT VARCHAR2

-----------------------------------------------------------------------------*/
BEGIN
  delete from clean.eod_payment_recovery
  WHERE ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider))
  ;
  out_status := 'Y';
EXCEPTION
WHEN others then
    out_status := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END DELETE_RECOVERY_TAB;


PROCEDURE DELETE_PAYMENT_RECOVERY_CART
(
 IN_MASTER_ORDER_NUMBER IN EOD_PAYMENT_RECOVERY.MASTER_ORDER_NUMBER%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   delete from eod_payment_recovery with the given master_order_number


Input:
        IN_MASTER_ORDER_NUMBER	EOD_PAYMENT_RECOVERY.MASTER_ORDER_NUMBER%TYPE

-----------------------------------------------------------------------------*/

BEGIN
	DELETE FROM clean.eod_payment_recovery
	      WHERE master_order_number = in_master_order_number;

	out_status := 'Y';

	EXCEPTION WHEN OTHERS THEN
	      out_status := 'N';
      	      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_PAYMENT_RECOVERY_CART;


FUNCTION COUNT_PAYMENT_RECOVERY_REC
(
  IN_CC_AUTH_PROVIDER   IN CLEAN.eod_payment_recovery.CC_AUTH_PROVIDER%TYPE
)
RETURN NUMBER
IS
/*-----------------------------------------------------------------------------
Description:
        This function returns the number of records in eod_payment_recovery_rec.

Input:

Output:
        number

-----------------------------------------------------------------------------*/
v_count NUMBER;

BEGIN

  BEGIN
    SELECT COUNT(1)
      INTO v_count
      FROM eod_payment_recovery
      WHERE ((in_cc_auth_provider is null and cc_auth_provider is null) or (in_cc_auth_provider is not null and cc_auth_provider = in_cc_auth_provider))
      ;

    EXCEPTION WHEN NO_DATA_FOUND THEN v_count := 0;
  END;

  return v_count;

END COUNT_PAYMENT_RECOVERY_REC;

PROCEDURE UPDATE_CART_DISPATCHED
(
 IN_MASTER_ORDER_NUMBER IN EOD_PAYMENT_RECOVERY.MASTER_ORDER_NUMBER%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   set dispatched_indicator to 'Y' in eod_payment_recovery with the given master_order_number


Input:
        IN_MASTER_ORDER_NUMBER	EOD_PAYMENT_RECOVERY.MASTER_ORDER_NUMBER%TYPE

-----------------------------------------------------------------------------*/

BEGIN
	UPDATE clean.eod_payment_recovery
	   SET dispatched_indicator = 'Y'
	 WHERE master_order_number = in_master_order_number;

	out_status := 'Y';

	EXCEPTION WHEN OTHERS THEN
	      out_status := 'N';
      	      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CART_DISPATCHED;

PROCEDURE UPDATE_REMOVED_ORD_PAY_BY_GUID
(
 IN_GUID                IN PAYMENTS.ORDER_GUID%TYPE,
 OUT_STATUS             OUT VARCHAR2,
 OUT_MESSAGE            OUT VARCHAR2
)
AS

-- cursor to get a redeemed gift certificate amount used on the same cart
CURSOR gc_cur (p_order_guid varchar2) IS
        SELECT  p.credit_amount
        FROM    clean.payments p
        WHERE   p.order_guid = p_order_guid
        AND     p.payment_type IN ('GC', 'GD')
        AND     p.additional_bill_id is null
        AND     p.payment_indicator = 'P';

-- cursor to recompute the credit card charge amount from the to be billed orders of the cart
CURSOR bill_cur (p_order_guid varchar2,
                 p_gc_amount number) IS
        SELECT  sum (ob.product_amount) + sum (ob.add_on_amount) + sum (ob.service_fee)
                        + sum (ob.shipping_fee) - sum (ob.discount_amount) + sum (ob.shipping_tax) + sum (ob.tax)
                        - p_gc_amount bill_total
        FROM    clean.order_details od
        JOIN    clean.order_bills ob
        ON      od.order_detail_id = ob.order_detail_id
        WHERE   od.order_guid = p_order_guid
        AND	ob.additional_bill_indicator = 'N';

-- cursor to get a payment id for the given order guid where payment type is NOT Gift Cert and Gift Card
CURSOR pay_cc_id_cur (p_order_guid varchar2) IS
        SELECT DISTINCT
                p.payment_id
        FROM    clean.payments p
        JOIN 	ftd_apps.payment_methods pm
        ON 		p.payment_type = pm.payment_method_id AND pm.payment_type NOT IN ('G','R')
        WHERE   p.order_guid = p_order_guid
        AND	p.payment_indicator = 'P'
        AND	p.additional_bill_id is null;

-- cursor to get a payment id for the given order guid where payment type is Gift Cert or Gift Card
CURSOR pay_gift_id_cur (p_order_guid varchar2) IS
         SELECT DISTINCT
                p.payment_id
        FROM    clean.payments p
        JOIN 	ftd_apps.payment_methods pm
        ON 	p.payment_type = pm.payment_method_id AND pm.payment_type IN ('G','R')
        WHERE   p.order_guid = p_order_guid
        AND	p.payment_indicator = 'P'
        AND	p.additional_bill_id is null;
        
v_gc_amount             number;
v_bill_total            number;
v_payment_cc_id         number;
v_payment_gift_id       number;
v_status                varchar2(10);

BEGIN
    v_status := 'N';

    UPDATE_REMOVED_UNBILLED_CHECK(IN_GUID,v_status);     
	
    v_gc_amount := 0;
    v_bill_total := 0;
    v_payment_cc_id := 0;
    v_payment_gift_id := 0;
      
    IF v_status = 'Y' THEN
      
            OPEN gc_cur (IN_GUID);
            FETCH gc_cur INTO v_gc_amount;
            CLOSE gc_cur;
      
            OPEN bill_cur (IN_GUID, v_gc_amount);
            FETCH bill_cur INTO v_bill_total;
            CLOSE bill_cur;
            
            OPEN pay_cc_id_cur (IN_GUID);
            FETCH pay_cc_id_cur INTO v_payment_cc_id;
            CLOSE pay_cc_id_cur;
            
            OPEN pay_gift_id_cur (IN_GUID);
            FETCH pay_gift_id_cur INTO v_payment_gift_id;
            CLOSE pay_gift_id_cur;
            
            -- If the remaining to be bill total is not less than or equal 0, update the payment
            -- to the remaining total, otherwise set the payment to 0.
            -- This can happen on carts with payments of gift certificate and credit card when the
            -- remaining billed order total is less than used gift certificate amount.
            IF v_bill_total < 0 THEN
              UPDATE  payments
              SET     credit_amount = (v_bill_total + v_gc_amount),
               updated_on = trunc(sysdate)-0.00001,
               updated_by = 'UPDATE_REMOVED_ORDER_PAYMENT'
               WHERE   payment_id = v_payment_gift_id;
               
              UPDATE  payments
              SET     credit_amount = 0,
               bill_status = 'Billed',
               bill_date = trunc(sysdate),
               updated_on = trunc(sysdate)-0.00001,
               updated_by = 'UPDATE_REMOVED_ORDER_PAYMENT'
               WHERE   payment_id = v_payment_cc_id;
               
             ELSE 
                  IF v_bill_total > 0  THEN
                    UPDATE  payments
                    SET     credit_amount = v_bill_total,
                            updated_on = trunc(sysdate)-0.00001,
                            updated_by = 'UPDATE_REMOVED_ORDER_PAYMENT'
                    WHERE   payment_id = v_payment_cc_id;
                  ELSE
                    UPDATE  payments
                    SET     credit_amount = 0,
                            bill_status = 'Billed',
                            bill_date = trunc(sysdate),
                            updated_on = trunc(sysdate)-0.00001,
                            updated_by = 'UPDATE_REMOVED_ORDER_PAYMENT'
                    WHERE   payment_id = v_payment_cc_id;
                  END IF;
            END IF;
            
      END IF;
out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
      
END UPDATE_REMOVED_ORD_PAY_BY_GUID;

PROCEDURE UPDATE_REMOVED_UNBILLED_CHECK
(
 IN_GUID                IN PAYMENTS.ORDER_GUID%TYPE,
 OUT_STATUS             OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   Return 'Y' if the cart has a removed item and all payments are Unbilled.
   Return 'N' if the cart has no item in removed or at least one payment has been Billed.


Input:
        IN_ORDER_GUID

-----------------------------------------------------------------------------*/
CURSOR check_cur(p_order_guid varchar2) IS
        SELECT DISTINCT
                p.payment_id
        FROM    clean.payments p
        JOIN    clean.orders o
        ON      p.order_guid = o.order_guid
        JOIN    ftd_apps.source s
        ON      o.source_code = s.source_code
        AND     NVL(s.jcpenney_flag,'N') <>'Y'
        JOIN    clean.order_details od
        ON      o.order_guid = od.order_guid
        WHERE   1=1
        AND	p.order_guid = IN_GUID
	AND	not exists
		(
			select 1
			from clean.payments p1
			where p1.order_guid = p.order_guid
			and p1.bill_status = 'Billed'
			and p1.credit_amount > 0
		)
        AND     exists
                (
                        select  1
                        from    scrub.order_details sod
                        where   sod.order_guid = p.order_guid
                        and     sod.status = '2004'
                );
        
v_pay_id             number;

BEGIN
      
      out_status := 'N';
      v_pay_id := 0;

      OPEN check_cur (IN_GUID);
      FETCH check_cur INTO v_pay_id;
      
      if check_cur%found then
      	out_status := 'Y';
      end if;

      CLOSE check_cur;
      
      
END UPDATE_REMOVED_UNBILLED_CHECK;

PROCEDURE GET_PAYMENT_EXT_BY_ID
(
 IN_PAYMENT_ID            IN PAYMENTS.PAYMENT_ID%TYPE,
 OUT_PAYMENT_EXT_CURSOR   OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
   This procedure retrieves payment ext records that are eligible for
   end of day processing based on the payment id passed in.

Input:
        payment_id    NUMBER

Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/


BEGIN

  OPEN out_payment_ext_cursor FOR
          SELECT auth_property_name, auth_property_value
            FROM payments_ext
           WHERE payment_id = in_payment_id order by created_on;

END GET_PAYMENT_EXT_BY_ID;

Procedure Insert_Settlement_Totals
(
In_Submission_Id            In Clean.Bams_Settlement_Totals.Submission_Id%TYPE,
In_Julian_Date              In Clean.Bams_Settlement_Totals.File_Created_Date_Julian%Type,
In_Payment_Total            In Clean.Bams_Settlement_Totals.Payment_Total%Type,
In_Refund_Total             In Clean.Bams_Settlement_Totals.Refund_Total%Type,
In_Billing_Header_Id        In Clean.Bams_Settlement_Totals.Billing_Header_Id%Type,
IN_SETTLEMENT_STATUS        IN CLEAN.Bams_Settlement_Totals.Status%TYPE,
In_File_Name                In Clean.Bams_Settlement_Totals.File_Name%Type,
Out_Settlement_Totals_Id    Out Clean.Bams_Settlement_Totals.Settlement_Total_Id%Type,
Out_Status                   Out Varchar2,
Out_Message                 Out Varchar2
)AS

Pragma Autonomous_Transaction;

Begin

Select Bams_Settlement_Totals_Id_Sq.Nextval Into Out_Settlement_Totals_Id From Dual;
Insert Into Bams_Settlement_Totals (Settlement_Total_Id, File_Created_Date_Julian, Submission_Id, Payment_Total, Refund_Total, billing_header_id, STATUS, file_name, created_by, created_on)
Values
(out_settlement_totals_id, in_julian_date, in_submission_id, in_payment_total, in_refund_total, In_Billing_Header_Id, IN_SETTLEMENT_STATUS, in_file_name, 'SYS', sysdate);

commit;
out_status := 'Y';

EXCEPTION WHEN OTHERS THEN
      Out_Status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END Insert_Settlement_Totals;

Procedure Update_Settlement_Totals
(
In_Submission_Id            In Clean.Bams_Settlement_Totals.Submission_Id%TYPE,
In_Julian_Date              In Clean.Bams_Settlement_Totals.File_Created_Date_Julian%Type,
In_Settlement_Status        In Clean.Bams_Settlement_Totals.Status%Type,
Out_Status                   Out Varchar2,
Out_Message                 Out Varchar2
)AS

Begin
Update Bams_Settlement_Totals Set Status = In_Settlement_Status, updated_by='SYS', updated_on=sysdate  Where Submission_Id = In_Submission_Id And File_Created_Date_Julian = In_Julian_Date;
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);
    

End Update_Settlement_Totals;

Procedure Get_Settlement_Totals
(
In_Submission_Id            In Clean.Bams_Settlement_Totals.Submission_Id%Type,
In_Julian_Date              In Clean.Bams_Settlement_Totals.File_Created_Date_Julian%Type,
Out_Cursor                  Out Types.Ref_Cursor,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)AS

Begin
Open Out_Cursor For
          select payment_total, refund_total, status, billing_header_id 
          from bams_settlement_totals where submission_id = in_submission_id and file_created_date_julian = in_julian_date;
          
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    Out_Status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);          

END Get_Settlement_Totals;

Procedure Update_Order_Bills
(
In_Submission_Id            In Clean.Bams_Settlement_Totals.Submission_Id%TYPE,
In_Julian_Date              In Clean.Bams_Settlement_Totals.File_Created_Date_Julian%Type,
In_Company_Id               In Clean.Billing_Detail.Company_Id%Type,
In_Ref_Nums                 In Varchar2,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)AS

V_Billing_Header_Id             Number;
v_query                         varchar2(1000);

Begin
V_Billing_Header_Id:=0;

  Begin
    Select Nvl(Billing_Header_Id, 0) Into V_Billing_Header_Id From Clean.Bams_Settlement_Totals Where Submission_Id = In_Submission_Id And File_Created_Date_Julian = In_Julian_Date;
  Exception
   When No_Data_Found Then
   V_Billing_Header_Id:=0;
  end;

   
Dbms_Output.Put_Line('billing header : '|| V_Billing_Header_Id);
If V_Billing_Header_Id > 0 Then
  If  In_Ref_Nums Is Null Then
    v_query:='Update Clean.Order_Bills Ob Set Ob.Bill_Status = ''Settled'', Ob.Bill_Date=Sysdate, Ob.Settled_Date=Sysdate, Updated_On=Sysdate, Updated_By=''Sys'' Where Ob.Order_Detail_id In (Select Od.Order_Detail_Id From Clean.Billing_Detail Be, Clean.Orders O, Clean.Order_Details Od 
    Where Be.Billing_Header_Id = ' || v_billing_header_id || ' And Be.Company_Id=''' || In_company_id || '''  
    And O.Master_Order_Number = Be.Order_Number And O.Order_Guid = Od.Order_Guid)';
    
  Else
    v_query:='Update Clean.Order_Bills Ob Set Ob.Bill_Status = ''Settled'', Ob.Bill_Date=Sysdate, Ob.Settled_Date=Sysdate, Updated_On=Sysdate, Updated_By=''Sys'' Where Ob.Order_Detail_id In (Select Od.Order_Detail_Id From Clean.Billing_Detail Be, Clean.Orders O, Clean.Order_Details Od 
    Where Be.Billing_Header_Id = ' || v_billing_header_id || ' And Be.Company_Id=''' || In_company_id || ''' And Be.Payment_Id Not In 
    (Select Payment_Id From Clean.Payments_Ext Pe Where Pe.Auth_Property_Name=''RefNum'' And Pe.Auth_Property_Value In (' || In_Ref_Nums || ')) 
    And O.Master_Order_Number = Be.Order_Number And O.Order_Guid = Od.Order_Guid)';
  end if;
  
  Execute Immediate(V_Query);
end if;

out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);
    

End Update_Order_Bills;

Procedure Get_Settlement_Details
(
In_Julian_Date              In Clean.Bams_Settlement_Totals.File_Created_Date_Julian%Type,
Out_Cursor                  Out Types.Ref_Cursor,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)As


Begin
Open Out_Cursor For
          Select File_Created_Date_Julian, Submission_Id, Billing_Header_Id, file_name
          from bams_settlement_totals where file_created_date_julian = in_julian_date and status = 'Sent';
         
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    Out_Status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);          

END Get_Settlement_Details;

Procedure Get_Settlement_File_To_Send
(
In_Company_Id		    In Varchar2,
Out_Cursor                  Out Types.Ref_Cursor,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)As


Begin
Open Out_Cursor For
          select file_name
	  from clean.bams_settlement_totals 
	  where trunc(created_on) = trunc(sysdate - to_number((select gp.value from frp.global_parms gp where gp.context = 'ACCOUNTING_CONFIG' and gp.name = 'BAMS_SETTLEMENT_FILE_OFFSET_DATE')))
	  and status is null
	  and DECODE(submission_id, 1, 'FTD', 2, 'FTDCA', 3, 'FLORIST', 4, 'ROSES') = In_Company_Id
	  and archived is null;
         
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    Out_Status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);          

END Get_Settlement_File_To_Send;


Procedure Update_Status_By_Filename
(
In_File_Name                In Clean.Bams_Settlement_Totals.File_Name%TYPE,
In_Settlement_Status        In Clean.Bams_Settlement_Totals.Status%Type,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)AS

Begin
Update Bams_Settlement_Totals Set Status = In_Settlement_Status, updated_by='SYS', updated_on=sysdate  Where File_Name = In_File_Name;
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);
    

End Update_Status_By_Filename;

Procedure Get_Settlement_File_To_Archive
(
In_Company_Id		    In Varchar2,
Out_Cursor                  Out Types.Ref_Cursor,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)As


Begin
Open Out_Cursor For
	  select file_name
	  from clean.bams_settlement_totals 
	  where trunc(created_on) = trunc(sysdate - to_number((select gp.value from frp.global_parms gp where gp.context = 'ACCOUNTING_CONFIG' and gp.name = 'BAMS_SETTLEMENT_FILE_OFFSET_DATE')))
	  and status = 'Settled'
	  and payment_total = 0
	  and refund_total = 0
	  and DECODE(submission_id, 1, 'FTD', 2, 'FTDCA', 3, 'FLORIST', 4, 'ROSES') = In_Company_Id
	  and archived is null;
	           
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    Out_Status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);          

END Get_Settlement_File_To_Archive;

Procedure Update_Archive_Status
(
In_File_Name                In Clean.Bams_Settlement_Totals.File_Name%TYPE,
Out_Status                  Out Varchar2,
Out_Message                 Out Varchar2
)AS

Begin
Update Bams_Settlement_Totals Set Archived = 'Y', updated_by='SYS', updated_on=sysdate  Where File_Name = In_File_Name;
out_status := 'Y';
EXCEPTION WHEN OTHERS THEN
    out_status := 'N';
    Out_Message := 'ERROR OCCURRED [' || Sqlcode || '] ' || Substr (Sqlerrm,1,256);
    

End Update_Archive_Status;

PROCEDURE DELETE_SETTLEMENT_TOTALS
(    
     IN_BILLING_HEADER_ID        In Clean.Bams_Settlement_Totals.Billing_Header_Id%Type,
     OUT_STATUS                      OUT VARCHAR2,
     OUT_MESSAGE                     OUT VARCHAR2)
AS
/*-----------------------------------------------------------------------------
Description:
        DELETE from clean.Bams_Settlement_Totals.
Input:
        IN_BILLING_HEADER_ID
Output:
        OUT_STATUS                      OUT VARCHAR2 
        OUT_MESSAGE                     OUT VARCHAR2

-----------------------------------------------------------------------------*/
BEGIN
  delete from CLEAN.bams_settlement_totals where billing_header_id=IN_BILLING_HEADER_ID; 
  out_status := 'Y';
EXCEPTION
WHEN others then
    out_status := 'N';
    OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
END DELETE_SETTLEMENT_TOTALS;

PROCEDURE UPDATE_REFUND_STATUS
( 
 IN_REFUND_ID  IN NUMBER, 
 IN_STATUS     IN VARCHAR2,
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is for updating the bill_status/refund_status in the
        payments and refund table for the refund id passed in.

Input: 
        refund_id            NUMBER 
        status               VARCHAR2

Output:
        status               VARCHAR2 (Y or N)
        message              VARCHAR2 (error message)
-----------------------------------------------------------------------------*/

BEGIN
            UPDATE refund 
                 SET 
                    refund_status        = in_status, 
                    refund_date          = sysdate,
                    updated_on           = sysdate,
                    updated_by           = 'AUTO_FIX'
            WHERE refund_id = IN_REFUND_ID and refund_status <> in_status;
       
           	UPDATE payments
                SET
                   bill_status           = in_status,
                   bill_date             = sysdate,
                   updated_on    		 = sysdate,
                   updated_by            = 'AUTO_FIX'
            WHERE refund_id = in_refund_id and bill_status <> in_status;
    
       		out_status := 'Y';

  	EXCEPTION WHEN OTHERS THEN
    BEGIN
      out_status := 'N';
      out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
    END;

END UPDATE_REFUND_STATUS;

PROCEDURE CREATE_BILLING_HEADER_PG
(
    IN_BILLING_BATCH_DATE            IN CLEAN.BILLING_HEADER_PG.BILLING_BATCH_DATE%TYPE,
    IN_PAYMENT_TYPE		              IN CLEAN.billing_header_PG.PAYMENT_TYPE%type,
    OUT_SEQUENCE_NUMBER             OUT CLEAN.BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
    OUT_STATUS                      OUT VARCHAR2,
    OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   Creates a billing record for End of Day

Input:
        billing_batch_date             date

Output:
        billing_header_id                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

SELECT BILLING_HEADER_ID_PG_SEQ.NEXTVAL INTO out_sequence_number FROM DUAL;

INSERT INTO billing_header_pg
(
        billing_header_id,
        billing_batch_date,
        processed_indicator,
        payment_type,
        created_on
)
VALUES
(

        out_sequence_number,
        in_billing_batch_date,
        'N',
        in_payment_type,
        SYSDATE
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_BILLING_HEADER_PG;


PROCEDURE GET_PG_PROCESSED_INDICATOR
(IN_DATE                        in DATE,
 IN_PAYMENT_TYPE     			IN BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
 OUT_PROCESS_IND                OUT VARCHAR2,
 OUT_BILLING_HEADER_ID          OUT NUMBER)
AS
/*-----------------------------------------------------------------------------
Description:
        7	Check if there's a record in CLEAN.BILLING_HEADER_PG table for the
                given date and auth provider. Return processed_indicator if found. Return null otherwise


Input:
        IN_DATE                         DATE
        IN_PAYMENT_TYPE     	BILLING_HEADER.IN_PAYMENT_TYPE%TYPE,
RETURN:
        VARCHAR2
-----------------------------------------------------------------------------*/

v_processed_indicator  clean.billing_header_pg.processed_indicator%type;
BEGIN
SELECT processed_indicator, billing_header_id
 INTO   OUT_PROCESS_IND,
        OUT_BILLING_HEADER_ID
 FROM clean.billing_header_pg cbhpg
WHERE trunc(cbhpg.billing_batch_date) = trunc(in_date)
AND cbhpg.payment_type = IN_PAYMENT_TYPE;

EXCEPTION
  WHEN too_many_rows THEN
      SELECT processed_indicator, billing_header_id
        INTO   OUT_PROCESS_IND,
               OUT_BILLING_HEADER_ID
        FROM clean.billing_header_pg cbhpg
       WHERE trunc(cbhpg.billing_batch_date) = trunc(in_date)
         AND created_on = (select max(created_on)
                           from clean.billing_header_pg cbhpg2
                           where  trunc(cbhpg2.billing_batch_date) = trunc(in_date))
         AND cbhpg.payment_type = IN_PAYMENT_TYPE;
  WHEN no_data_found THEN
        OUT_PROCESS_IND:= null;
        OUT_BILLING_HEADER_ID:=null;
END GET_PG_PROCESSED_INDICATOR;


PROCEDURE GET_PG_UNPROC_RECOVERY_CARTS
(
IN_DATE        IN BILLING_HEADER_PG.BILLING_BATCH_DATE%TYPE,
IN_payment_type     IN CLEAN.payments.payment_type%TYPE,
OUT_payment_CURSOR      OUT TYPES.REF_CURSOR,
OUT_refund_CURSOR  OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
         Retrieve all payments in CLEAN.EOD_PAYMENT_RECOVERY_PG that are not
         billed and not already in CLEAN.BILLING_DETAIL_pg_cc table for cc auth
         provider passed in.
Input:
        None
Output:
        cursor containing payment info

-----------------------------------------------------------------------------*/
BEGIN

OPEN out_payment_cursor FOR
  SELECT DISTINCT
                p.payment_id,
                o.master_order_number,
                p.payment_indicator 
 FROM
                clean.payments p
        JOIN    clean.orders o
        ON      p.order_guid = o.order_guid
        JOIN    clean.order_details od
        ON      o.order_guid = od.order_guid
        AND     (od.order_disp_code IN ('Processed','Shipped','Printed'))
        WHERE   p.updated_on >= trunc(IN_DATE-180) and p.updated_on < TRUNC(IN_DATE+1)
        AND     p.credit_amount > 0
        AND     p.payment_indicator = 'P'
        AND	p.bill_status = 'Unbilled'
        AND NOT EXISTS (
	        select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
            and od2.order_disp_code in ('Validated', 'Held'))
        AND NOT EXISTS (
        	select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
        	and sod.status in ('2001','2002','2003','2005','2007'))
        AND
        p.payment_type IN ('AX','CB','DC','DI','MC','VI')
        AND (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'Y') 
        ORDER BY o.master_order_number,p.payment_id;

OPEN out_refund_cursor FOR
            SELECT DISTINCT
                p.payment_id,
                o.master_order_number,
                p.payment_indicator
		   
		        FROM payments p,
                 orders o,
                 order_details od,
                 refund r
           WHERE r.updated_on >= trunc(IN_DATE-180) and r.updated_on < TRUNC(IN_DATE+1)
             AND r.refund_status = 'Unbilled'
             AND r.order_detail_id = od.order_detail_id
             AND od.order_disp_code IN ('Processed','Shipped','Printed')
             AND NOT EXISTS (
                select 1 from clean.order_details od2 where od2.order_guid = od.order_guid
                 and od2.order_disp_code in ('Validated', 'Held'))
             AND NOT EXISTS (
        	select 1 from scrub.order_details sod where sod.order_guid = od.order_guid
        	and sod.status in ('2001','2002','2003','2005','2007'))
             AND p.order_guid = o.order_guid
             AND p.refund_id = r.refund_id
             AND p.debit_amount > 0
             AND p.payment_indicator = 'R'
       AND
        	p.payment_type IN ('AX','CB','DC','DI','MC','VI')
          AND (clean.order_query_pkg.IS_PG_ROUTE_TRANSACTION(p.route,o.ORIGIN_ID) = 'Y')
          ORDER BY o.master_order_number,p.payment_id;

END GET_PG_UNPROC_RECOVERY_CARTS;


PROCEDURE GET_PG_PAYMENT_BY_ID
(
 IN_payment_id        IN PAYMENTS.PAYMENT_ID%TYPE,
 OUT_PAYMENT_CURSOR  OUT TYPES.REF_CURSOR
) 
AS
 BEGIN
 
 open OUT_PAYMENT_CURSOR FOR 
 
 SELECT p.payment_id,
                 o.master_order_number,
                 p.additional_bill_id,
                 p.payment_type,
                 p.credit_amount,
                 p.debit_amount,
                 p.payment_indicator,
                 p.refund_id,
                 p.auth_date,
                 p.auth_number,
                 p.is_voice_auth,
				 p.AUTHORIZATION_TRANSACTION_ID,
				 p.settlement_transaction_id,
				 p.REFUND_TRANSACTION_ID,
				 p.request_token,
         		 p.merchant_ref_id
         FROM   credit_cards cc,
                 orders o,
                 payments p,
                 ftd_apps.source s
        WHERE 
            p.payment_id = IN_payment_id
            AND  cc.cc_id (+) = p.cc_id
             AND p.order_guid = o.order_guid
             AND o.source_code = s.source_code;

END GET_PG_PAYMENT_BY_ID;


PROCEDURE CREATE_BILLING_DETAIL_PG_CC
(

		IN_BILLING_HEADER_ID        IN    BILLING_DETAIL_PG_CC.BILLING_HEADER_ID%TYPE,
        IN_ORDER_NUMBER             IN    BILLING_DETAIL_PG_CC.ORDER_NUMBER%TYPE,
        IN_PAYMENT_ID               IN    BILLING_DETAIL_PG_CC.PAYMENT_ID%TYPE,
        IN_ORDER_AMOUNT             IN    BILLING_DETAIL_PG_CC.ORDER_AMOUNT%TYPE,
        IN_TRANSACTION_TYPE         IN    BILLING_DETAIL_PG_CC.TRANSACTION_TYPE%TYPE,
        IN_BILL_TYPE   				IN    BILLING_DETAIL_PG_CC.BILL_TYPE%TYPE,
        IN_IS_VOICE_AUTH            IN    BILLING_DETAIL_PG_CC.IS_VOICE_AUTH%TYPE,
        IN_AUTH_TRANSACTION_ID		IN    BILLING_DETAIL_PG_CC.AUTH_TRANSACTION_ID%TYPE,
        IN_MERCHANT_REF_ID          IN    BILLING_DETAIL_PG_CC.MERCHANT_REF_ID%TYPE,
        IN_SETTLEMENT_TRANSACTION_ID  IN  BILLING_DETAIL_PG_CC.SETTLEMENT_TRANSACTION_ID%TYPE,
 		IN_REFUND_TRANSACTION_ID   	IN BILLING_DETAIL_PG_CC.REFUND_TRANSACTION_ID%TYPE,
 		IN_REQUEST_ID      			IN BILLING_DETAIL_PG_CC.REQUEST_ID%TYPE,
        OUT_SEQUENCE_NUMBER         OUT   BILLING_DETAIL_PG_CC.BILLING_DETAIL_ID%TYPE,
        OUT_STATUS                  OUT VARCHAR2,
        OUT_MESSAGE                 OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
   Creates a billing record for End of Day

Input:


Output:
        batch_number                    number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

  
BEGIN

SELECT BILLING_DETAIL_ID_PG_SEQ.NEXTVAL INTO out_sequence_number FROM DUAL;

INSERT INTO billing_detail_pg_cc
(
  	BILLING_DETAIL_ID,
	BILLING_HEADER_ID,
	ORDER_NUMBER, 
	PAYMENT_ID, 
	ORDER_AMOUNT, 
	TRANSACTION_TYPE, 
	BILL_TYPE, 
	IS_VOICE_AUTH,
	AUTH_TRANSACTION_ID,
	MERCHANT_REF_ID,
	SETTLEMENT_TRANSACTION_ID,
  	REFUND_TRANSACTION_ID,
  	REQUEST_ID,
	STATUS,
	CREATED_ON,
	UPDATED_ON
)
VALUES
(
  out_sequence_number,
  in_billing_header_id,
  in_order_number,
  in_payment_id,
	in_ORDER_AMOUNT, 
	in_TRANSACTION_TYPE, 
	in_BILL_TYPE, 
	in_IS_VOICE_AUTH,
	IN_AUTH_TRANSACTION_ID,
	IN_MERCHANT_REF_ID,
	IN_SETTLEMENT_TRANSACTION_ID,
	IN_REFUND_TRANSACTION_ID,
	IN_REQUEST_ID,
	'New',
	sysdate,
	sysdate
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END CREATE_BILLING_DETAIL_PG_CC;




PROCEDURE GET_BILLING_DETAILS_CC_PG
(
 IN_PAYMENT_TYPE      IN BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
 OUT_CURSOR            OUT TYPES.REF_CURSOR,
 OUT_CURSOR2           OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure returns all the billing_details records
         related to a billing_header_pg that has to be processed.

Input:
        N/A

Output:
        cursor containing billing_detail info
BILLING_DETAIL_ID,
	BILLING_HEADER_ID,
	ORDER_NUMBER, 
	PAYMENT_ID, 
	ORDER_AMOUNT, 
	TRANSACTION_TYPE, 
	BILL_TYPE, 
	IS_VOICE_AUTH,
	AUTH_TRANSACTION_ID,
	STATUS,
	CREATED_ON,
	UPDATED_ON
-----------------------------------------------------------------------------*/
BEGIN

  OPEN out_cursor FOR
 	       SELECT bd.billing_detail_id,
          bd.billing_header_id,
          bd.order_number,
          bd.payment_id,
	      bd.order_amount,
          bd.transaction_type,
          bd.bill_type,
          bd.is_voice_auth,
		  bd.AUTH_TRANSACTION_ID,
		  '1' sort_order
 	  	  FROM billing_detail_pg_cc bd
    WHERE bd.billing_header_id IN (SELECT billing_header_id
                                  FROM billing_header_pg
                                 WHERE processed_indicator = 'N' 
                                 and payment_type = IN_PAYMENT_TYPE)
      AND bd.status = 'NEW'
      AND bd.is_voice_auth is null
      AND bd.bill_type = 'Bill'
  UNION
	  	  SELECT bd.billing_detail_id,
          bd.billing_header_id,
          bd.order_number,
          bd.payment_id,
	      bd.order_amount,
          bd.transaction_type,
          bd.bill_type,
          bd.is_voice_auth,
		  bd.AUTH_TRANSACTION_ID,
		  '2' sort_order
 	  	  FROM billing_detail_pg_cc bd
    WHERE bd.billing_header_id IN (SELECT billing_header_id
                                  FROM billing_header_pg
                                 WHERE processed_indicator = 'N' 
                                 and payment_type = IN_PAYMENT_TYPE)
      AND bd.status = 'NEW' 
      AND bd.is_voice_auth is null
      AND bd.bill_type = 'Add_Bill'
  UNION	
	  SELECT bd.billing_detail_id,
          bd.billing_header_id,
          bd.order_number,
          bd.payment_id,
	      bd.order_amount,
          bd.transaction_type,
          bd.bill_type,
          bd.is_voice_auth,
		  bd.AUTH_TRANSACTION_ID,
		  '3' sort_order
 	  	  FROM billing_detail_pg_cc bd,payments p
    WHERE bd.billing_header_id IN (SELECT billing_header_id
                                  FROM billing_header_pg
                                 WHERE processed_indicator = 'N' 
                                 and payment_type = IN_PAYMENT_TYPE)
      AND bd.status = 'NEW' 
      AND bd.is_voice_auth is null
      AND bd.bill_type = 'Refund'
      AND bd.payment_id=p.payment_id
    ORDER BY sort_order,order_number;
                    
   OPEN out_cursor2 FOR
     SELECT DISTINCT billing_header_id
       FROM billing_header_pg
      WHERE processed_indicator = 'N'
	  and  payment_type = IN_PAYMENT_TYPE;

END GET_BILLING_DETAILS_CC_PG;


PROCEDURE UPDATE_BILLING_HEADER_PG
(
 IN_BILLING_HEADER_IDS       IN VARCHAR2,
 IN_PROCESSED_INDICATOR      IN BILLING_HEADER_PG.PROCESSED_INDICATOR%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
   This procedure updates a billing_header's status for a given
    billing_header_id.

Input:
        billing_header_id       VARCHAR2
        processed_indicator     CHAR

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/

Pragma Autonomous_Transaction;

BEGIN

  EXECUTE IMMEDIATE('UPDATE billing_header_PG
                        SET processed_indicator =''' || in_processed_indicator ||
                  ''' WHERE billing_header_id IN (' || in_billing_header_ids || ')');

  IF SQL%FOUND THEN
     out_status := 'Y';
  ELSE
     out_status := 'N';
     out_message := 'WARNING: No billing_header_PG record was updated for billing_header_ids ' || in_billing_header_ids ||'.';
  END IF;

commit;

  EXCEPTION WHEN OTHERS THEN
   BEGIN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END UPDATE_BILLING_HEADER_PG;


PROCEDURE UPDATE_BILLING_DTL_PG_CC_RES
(
 IN_AUTH_TRANS_ID       IN BILLING_DETAIL_PG_CC.AUTH_TRANSACTION_ID%TYPE,
 IN_ORDER_NUMBER      IN BILLING_DETAIL_PG_CC.ORDER_NUMBER%TYPE,
 IN_BILLING_DETAIL_ID IN BILLING_DETAIL_PG_CC.BILLING_DETAIL_ID%TYPE,
 IN_PAYMENT_ID        IN BILLING_DETAIL_PG_CC.PAYMENT_ID%TYPE,
 IN_REQUEST_ID      IN BILLING_DETAIL_PG_CC.REQUEST_ID%TYPE,
 IN_SETTLEMENT_TRANSACTION_ID   IN BILLING_DETAIL_PG_CC.SETTLEMENT_TRANSACTION_ID%TYPE,
 IN_REFUND_TRANSACTION_ID   IN BILLING_DETAIL_PG_CC.REFUND_TRANSACTION_ID%TYPE,
 IN_RESPONSE_MESSAGE  IN BILLING_DETAIL_PG_CC.RESPONSE_MESSAGE%TYPE,
 IN_STATUS            IN BILLING_DETAIL_PG_CC.STATUS%TYPE,
 IN_SETTLEMENT_DATE   IN BILLING_DETAIL_PG_CC.SETTLEMENT_DATE%TYPE,
 IN_PROCESS_ACCOUNT	  IN BILLING_DETAIL_PG_CC.PROCESS_ACCOUNT%TYPE,
 IN_REQUEST_TOKEN		IN payments.REQUEST_TOKEN%TYPE,
 IN_RESPONSE_CODE   IN  BILLING_DETAIL_PG_CC.RESPONSE_CODE%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS

/*-----------------------------------------------------------------------------
Description:
   This procedure updates a billing_detail_pg_CC with respnse and 
   updates payments.
  

Input:
       

Output:
        status                  VARCHAR2 (Y or N)
        message                 VARCHAR2 (error message)

-----------------------------------------------------------------------------*/

 BEGIN
 
 IF (IN_STATUS != 'Settled') THEN
 UPDATE BILLING_DETAIL_PG_CC
  SET status                  = IN_STATUS,
    response_message          = IN_RESPONSE_MESSAGE,
    response_code                =  IN_RESPONSE_CODE,
    updated_on                = sysdate
  WHERE BILLING_DETAIL_ID     = IN_BILLING_DETAIL_ID;
 
 ELSE
  UPDATE BILLING_DETAIL_PG_CC
  SET status                  = IN_STATUS,
    request_id                =IN_REQUEST_ID,
    SETTLEMENT_TRANSACTION_ID = IN_SETTLEMENT_TRANSACTION_ID,
   REFUND_TRANSACTION_ID =  IN_REFUND_TRANSACTION_ID,
    settlement_date           = IN_SETTLEMENT_DATE,
    response_message          =IN_RESPONSE_MESSAGE,
    PROCESS_ACCOUNT			  = IN_PROCESS_ACCOUNT,
    response_code      = IN_RESPONSE_CODE,
    updated_on                = sysdate
  WHERE BILLING_DETAIL_ID     = IN_BILLING_DETAIL_ID;
 END IF;
 
    IF (IN_SETTLEMENT_TRANSACTION_ID is not null)
  THEN 
    UPDATE payments
    SET SETTLEMENT_TRANSACTION_ID = IN_SETTLEMENT_TRANSACTION_ID,
      request_token = IN_REQUEST_TOKEN,
      updated_on                  = sysdate
    WHERE payment_id              = IN_PAYMENT_ID;
  END IF;
  
  IF (IN_REFUND_TRANSACTION_ID is not null)
  THEN 
    UPDATE payments
    SET REFUND_TRANSACTION_ID = IN_REFUND_TRANSACTION_ID,
      updated_on                  = sysdate
    WHERE payment_id              = IN_PAYMENT_ID;
  END IF;
  
  
  out_status                   := 'Y';
  
EXCEPTION
WHEN OTHERS THEN
  BEGIN
    out_status  := 'N';
    out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;    

END UPDATE_BILLING_DTL_PG_CC_RES;

FUNCTION GET_BILLING_DETAILS_CC_PG
(
 IN_BILLING_HEADER_ID        IN    BILLING_DETAIL_PG_CC.BILLING_HEADER_ID%TYPE,
  IN_BILLING_DETAIL_ID        IN    BILLING_DETAIL_PG_CC.BILLING_DETAIL_ID%TYPE
)
RETURN VARCHAR2
IS
/*------------------------------------------------------------------------------------------------------------------------------------------------------------
Description:
	Retrieves payment_code for the given payment_id

Input:
        in_payment_id number

Output:
        varchar2

----------------------------------------------------------------------------------------------------------------------------------------------------*/
V_BILLING_DETAIL_ID  BILLING_DETAIL_PG_PP.BILLING_DETAIL_ID%TYPE;
BEGIN

 SELECT BILLING_DETAIL_ID INTO V_BILLING_DETAIL_ID FROM BILLING_DETAIL_PG_CC
 WHERE BILLING_HEADER_ID  = IN_BILLING_HEADER_ID AND BILLING_DETAIL_ID = IN_BILLING_DETAIL_ID 
 AND STATUS not in ('Settled');

return V_BILLING_DETAIL_ID;

EXCEPTION
WHEN NO_DATA_FOUND THEN
    V_BILLING_DETAIL_ID := 0;
    return V_BILLING_DETAIL_ID;

END GET_BILLING_DETAILS_CC_PG;

PROCEDURE PG_UPDATE_DISPATCHED_COUNT_CC
(
IN_BILLING_HEADER_ID		IN  BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
 IN_DISPATCHED_COUNT		IN  BILLING_HEADER_PG.DISPATCHED_COUNT%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Update dispatched_count for the give billing header id.
Input:
        in_billing_header_id number
	in_dispatched_count  number

Output:
        out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

BEGIN

OUT_STATUS := 'N';

if in_dispatched_count > 0 THEN

UPDATE BILLING_HEADER_PG
SET
        dispatched_count = in_dispatched_count
WHERE   BILLING_HEADER_ID = in_billing_header_id;

ELSE 

UPDATE BILLING_HEADER_PG
SET   dispatched_count = in_dispatched_count,
        PROCESSED_INDICATOR =  'Y'
WHERE   BILLING_HEADER_ID = in_billing_header_id;

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PG_UPDATE_DISPATCHED_COUNT_CC;



PROCEDURE GET_BILLING_HEADER_ID_PG
(
   IN_BILLING_HEADER_ID       IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
   IN_PAYMENT_TYPE      IN BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
   OUT_BILLING_HEADER_ID OUT TYPES.REF_CURSOR
)
AS

BEGIN

   OPEN OUT_BILLING_HEADER_ID FOR
     SELECT BILLING_HEADER_ID
       FROM clean.billing_header_pg
      WHERE BILLING_HEADER_ID = IN_BILLING_HEADER_ID and payment_type = IN_PAYMENT_TYPE;

END GET_BILLING_HEADER_ID_PG;

PROCEDURE GET_FAILED_BILLING_DETAILS_CC
(
   IN_BILLING_HEADER_ID       IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
   IN_PAYMENT_TYPE      IN BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
   OUT_BILLING_DETAILS OUT TYPES.REF_CURSOR
)
AS

BEGIN

 OPEN OUT_BILLING_DETAILS FOR
 
  SELECT *  
      FROM clean.billing_header_pg bh, clean.billing_detail_pg_cc bd
      WHERE bh.BILLING_HEADER_ID = bd.billing_header_id
      AND bh.BILLING_HEADER_ID = IN_BILLING_HEADER_ID and bh.payment_type = IN_PAYMENT_TYPE
      AND bd.status in ('Declined','Invalid','Error','New');

END GET_FAILED_BILLING_DETAILS_CC;

PROCEDURE GET_BILLING_DET_PG_CC_BY_ID
(
 IN_BILLING_HEADER_ID  IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
 IN_BILLING_DETAIL_ID   IN BILLING_DETAIL_PG_CC.BILLING_DETAIL_ID%TYPE,
 OUT_CURSOR            OUT TYPES.REF_CURSOR
)
AS

BEGIN

  OPEN out_cursor FOR
	SELECT bd.*, (select p.payment_type from clean.payments p where p.payment_id = bd.payment_id) as payment_type
 	FROM clean.billing_detail_pg_cc bd
    WHERE billing_detail_id= IN_BILLING_DETAIL_ID and
    billing_header_id = IN_BILLING_HEADER_ID; 
	

END GET_BILLING_DET_PG_CC_BY_ID;

PROCEDURE PG_UPDATE_HEADER_IND
(
 IN_BILLING_HEADER_ID		IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
 IN_PAYMENT_TYPE		IN BILLING_HEADER_PG.PAYMENT_TYPE%TYPE,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	check Porcessed Count and Dispatched Count is equal then update the Header Indicator.
Input:
        in_billing_header_id record to be incremented
Output:
        out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/

BEGIN 

out_status := 'N';

   UPDATE clean.billing_header_pg
   SET PROCESSED_INDICATOR = 'Y'
   WHERE PROCESSED_INDICATOR = 'N'
   AND PAYMENT_TYPE = IN_PAYMENT_TYPE 
   AND billing_header_id = IN_BILLING_HEADER_ID;

 OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PG_UPDATE_HEADER_IND;


PROCEDURE PG_INCR_AND_CHECK_PROC_COUNT
(
 IN_BILLING_HEADER_ID		IN BILLING_HEADER_PG.BILLING_HEADER_ID%TYPE,
  OUT_IS_EQUAL   		OUT VARCHAR2,
 OUT_STATUS			OUT VARCHAR2,
 OUT_MESSAGE			OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
	Increment billing_header_pg.processed_count by 1.
Input:
        in_billing_header_id record to be incremented
Output:
        out_status status of operation
	out_message error message

-----------------------------------------------------------------------------*/
BEGIN

out_status   := 'N';
out_is_equal := 'N';

UPDATE billing_header_pg
SET processed_count = processed_count+1
 WHERE billing_header_id = in_billing_header_id;
 
 SELECT decode((dispatched_count - processed_count), 0, 'Y','N')
INTO   OUT_IS_EQUAL
FROM   billing_header_pg
WHERE  billing_header_id = in_billing_header_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END PG_INCR_AND_CHECK_PROC_COUNT;



END;
.
/
