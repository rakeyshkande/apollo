CREATE OR REPLACE
FUNCTION clean.get_reward_posting_sq
(
in_partner_name                 VARCHAR2,
in_sequence_type                VARCHAR2
)
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This function returns the next sequence value for the given partner
        file sequence.

Input:
        partner_name                    varchar2
        sequence_type                   varchar2 (FILE or RECORD)

Output:
        next sequence value
-----------------------------------------------------------------------------*/
-- cursor to get the sequence prefix for the given partner
CURSOR pm_cur IS
        SELECT  UPPER (file_sequence_prefix)
        FROM    ftd_apps.partner_master
        WHERE   UPPER (partner_name) = UPPER (in_partner_name);


v_sql           varchar2(400);
v_seq_obj       varchar2(35);
v_seq_val       number;

BEGIN

OPEN pm_cur;
FETCH pm_cur INTO v_seq_obj;
CLOSE pm_cur;

IF v_seq_obj IS NOT NULL THEN
        v_seq_obj := v_seq_obj || '_' || UPPER (in_sequence_type);
        v_sql := 'SELECT ' || v_seq_obj || '_sq.nextval FROM DUAL';

        EXECUTE IMMEDIATE v_sql INTO v_seq_val;
END IF;

return  v_seq_val;

END;
.
/
