grant select on ftd_apps.session_order_master to scrub;
grant select on ftd_apps.session_item to scrub;

CREATE OR REPLACE
PACKAGE BODY clean.OPS_ADMIN_QUERY_PKG AS


PROCEDURE VIEW_MISC_ORDER_STATS
(
IN_ORDER_GUID   IN  ORDERS.ORDER_GUID%TYPE,
OUT_CUR         OUT TYPES.REF_CURSOR
)
AS

/*-----------------------------------------------------------------------------
Description:
        This procedure will return a few miscellaneous stats for an order and associated
        line items.  It was intended for Reinstate Orders page, to display order
        statuses at a glance.  Unions are used instead of outer joins.

Output:
        Cursor result set containing:
          column_name:           Name of table and column value being returned
          external_order_number: Item number (or '' if order level info)
          info:                  Actual value of column
-----------------------------------------------------------------------------*/

BEGIN

   OPEN OUT_CUR FOR
        select 'scrub.order_details.status' as column_name, od.external_order_number external_order_number, od.status info
        from scrub.order_details od where od.order_guid = in_order_guid
        union
        select 'scrub.orders.status', '' external_order_number, o.status info
        from scrub.orders o where o.order_guid = in_order_guid
        union
        select 'ftd_apps.session_item.status', si.detail_item_number external_order_number, si.status info
        from ftd_apps.session_item si where si.order_id = in_order_guid
        union
        select 'ftd_apps.session_order_master.order_status', '' external_order_number, so.order_status info
        from ftd_apps.session_order_master so where so.order_id = in_order_guid
        union
        select 'clean.order_details.op_status', cod.external_order_number external_order_number, cod.op_status info
        from clean.order_details cod where cod.order_guid = in_order_guid
        union
        select 'clean.orders.created_on', '' external_order_number, to_char(co.created_on) info
        from clean.orders co where co.order_guid = in_order_guid
        order by external_order_number;
        
END VIEW_MISC_ORDER_STATS;

END OPS_ADMIN_QUERY_PKG;
.
/

grant execute on clean.ops_admin_query_pkg to osp;
