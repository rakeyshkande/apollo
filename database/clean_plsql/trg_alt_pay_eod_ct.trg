CREATE OR REPLACE
TRIGGER clean.trg_alt_pay_eod_ct
BEFORE INSERT OR UPDATE
ON clean.alt_pay_billing_header
FOR EACH ROW

/*****************************************************************************************
***          Created By : CHU
***          Created On : 03/06/2007
***              Reason : Updates processed flag if dispatched count equals processed count.
***                       This is only done for PayPal and Gift Card. BillMeLater will set 
***                       this flag through an update instead (when it has finished processing).
***
***
*** Special Instructions:
***
*****************************************************************************************/

DECLARE

BEGIN

   IF :NEW.dispatched_count = :NEW.processed_count AND
      :NEW.payment_method_id IN ('PP','GD') THEN
	:NEW.processed_flag := 'Y';
   END IF;

EXCEPTION
   WHEN OTHERS THEN  --  all other fatal errors
      raise_application_error(-20000, 'Fatal Error In clean.trg_alt_pay_eod_ct: SQLCODE: ' || sqlcode ||
          ' SQL ERROR MESSAGE: ' || sqlerrm);
END trg_alt_pay_eod_ct;
.
/
