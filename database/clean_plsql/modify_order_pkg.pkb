create or replace
PACKAGE BODY                                                       clean.MODIFY_ORDER_PKG AS

FUNCTION IS_ORDER_PAST_DAYS
(
IN_ORDER_DATE                   IN ORDERS.ORDER_DATE%TYPE,
IN_PARM_NAME                    IN VARCHAR2
)
RETURN VARCHAR2
AS
/*-----------------------------------------------------------------------------
Description:
        This function is responsible for returning if the given order date
        is past the number of days defined in the given paramter.

Input:
        order_detail_id                 number

Output:
        Y/N if order date is past the parm value
-----------------------------------------------------------------------------*/

v_max_days      number := to_char (frp.misc_pkg.get_global_parm_value ('FTDAPPS_PARMS', in_parm_name));
v_return        char(1);

BEGIN

-- add the max days to the order
IF in_order_date + v_max_days > sysdate THEN
        -- order date is within the max days
        v_return := 'N';
ELSE
        -- order date is older the max days allowed
        v_return := 'Y';
END IF;

RETURN v_return;

END IS_ORDER_PAST_DAYS;


PROCEDURE INSERT_ORDER_DETAIL_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for initializing the data in the hold
        table for the given order

Input:
        order_detail_id                 number
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN


INSERT INTO order_detail_update
(
        order_detail_id,
        order_guid,
        updated_on,
        updated_by,
        source_code,
        product_id,
        occasion,
        card_message,
        card_signature,
        special_instructions,
        release_info_indicator,
        membership_number,
        membership_first_name,
        membership_last_name,
        recipient_id,
        recipient_phone_number,
        recipient_extension,
        recipient_first_name,
        recipient_last_name,
        recipient_address_1,
        recipient_address_2,
        recipient_city,
        recipient_state,
        recipient_zip_code,
        recipient_country,
        recipient_address_type,
        recipient_business_name,
        recipient_business_info,
        personal_greeting_id,
        original_order_has_sdu
)
SELECT
        od.order_detail_id,
        od.order_guid,
        od.updated_on,
        in_updated_by,
        od.source_code,
        od.product_id,
        od.occasion,
        od.card_message,
        od.card_signature,
        od.special_instructions,
        od.release_info_indicator,
        m.membership_number,
        m.first_name membership_first_name,
        m.last_name membership_last_name,
        r.customer_id recipient_id,
        coalesce (cpd.phone_number, cpe.phone_number) recipient_phone_number,
        decode (cpd.phone_number, null, cpe.extension, cpd.extension) recipient_extension,
        r.first_name recipient_first_name,
        r.last_name recipient_last_name,
        r.address_1 recipient_address_1,
        r.address_2 recipient_address_2,
        r.city recipient_city,
        r.state recipient_state,
        r.zip_code recipient_zip_code,
        r.country recipient_country,
        r.address_type recipient_address_type,
        r.business_name,
        r.business_info,
        od.personal_greeting_id,
        od.original_order_has_sdu
FROM    order_details od
JOIN    customer r
ON      od.recipient_id = r.customer_id
JOIN    orders o
ON      od.order_guid = o.order_guid
LEFT OUTER JOIN
(
        select  r.order_detail_id,
                sum (r.refund_product_amount) refund_product_amount,
                sum (r.refund_addon_amount) refund_add_on_amount,
                sum (r.refund_service_fee) refund_service_fee,
                sum (r.refund_shipping_fee) refund_shipping_fee,
                sum (r.refund_discount_amount) refund_discount_amount,
                sum (r.refund_shipping_tax) refund_shipping_tax,
                sum (r.refund_tax) refund_tax
        from    refund r
        where   r.order_detail_id = in_order_detail_id
        group by r.order_detail_id
) r
ON      od.order_detail_id = r.order_detail_id
LEFT OUTER JOIN memberships m
ON      od.membership_id = m.membership_id
LEFT OUTER JOIN    customer_phones cpd
ON      r.customer_id = cpd.customer_id
AND     cpd.phone_type = 'Day'
LEFT OUTER JOIN    customer_phones cpe
ON      r.customer_id = cpe.customer_id
AND     cpe.phone_type = 'Evening'
WHERE   od.order_detail_id = in_order_detail_id;

INSERT INTO order_contact_update
(
        order_contact_info_id,
        order_guid,
        updated_on,
        updated_by,
        first_name,
        last_name,
        phone_number,
        extension,
        email_address,
        order_detail_id
)
SELECT
        order_contact_info_id,
        order_guid,
        updated_on,
        in_updated_by,
        first_name,
        last_name,
        phone_number,
        extension,
        email_address,
        in_order_detail_id
FROM    order_contact_info
WHERE   order_guid = (select order_guid from order_details where order_detail_id = in_order_detail_id);

INSERT INTO accounting_transactions_upd
(
        accounting_transaction_id,
        transaction_type,
        transaction_date,
        order_detail_id,
        additional_order_seq,
        delivery_date,
        product_id,
        source_code,
        ship_method,
        product_amount,
        add_on_amount,
        shipping_fee,
        service_fee,
        discount_amount,
        shipping_tax,
        service_fee_tax,
        tax,
        payment_type,
        refund_disp_code,
        commission_amount,
        wholesale_amount,
        admin_fee,
        refund_id,
        order_bill_id,
        add_on_discount_amount
)
SELECT
        accounting_transaction_id,
        transaction_type,
        transaction_date,
        order_detail_id,
        additional_order_seq,
        delivery_date,
        product_id,
        source_code,
        ship_method,
        product_amount,
        add_on_amount,
        shipping_fee,
        service_fee,
        discount_amount,
        shipping_tax,
        service_fee_tax,
        tax,
        payment_type,
        refund_disp_code,
        commission_amount,
        wholesale_amount,
        admin_fee,
        refund_id,
        order_bill_id,
        add_on_discount_amount
FROM    accounting_transactions_upd
WHERE   order_detail_id = in_order_detail_id;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_ORDER_DETAIL_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END INSERT_ORDER_DETAIL_UPDATE;


PROCEDURE UPDATE_DELIVERY_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_SOURCE_CODE                  IN ORDER_DETAIL_UPDATE.SOURCE_CODE%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAIL_UPDATE.DELIVERY_DATE%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAIL_UPDATE.DELIVERY_DATE_RANGE_END%TYPE,
IN_CARD_MESSAGE                 IN ORDER_DETAIL_UPDATE.CARD_MESSAGE%TYPE,
IN_CARD_SIGNATURE               IN ORDER_DETAIL_UPDATE.CARD_SIGNATURE%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAIL_UPDATE.SPECIAL_INSTRUCTIONS%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAIL_UPDATE.SHIP_METHOD%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAIL_UPDATE.SHIP_DATE%TYPE,
IN_MEMBERSHIP_NUMBER            IN ORDER_DETAIL_UPDATE.MEMBERSHIP_NUMBER%TYPE,
IN_MEMBERSHIP_FIRST_NAME        IN ORDER_DETAIL_UPDATE.MEMBERSHIP_FIRST_NAME%TYPE,
IN_MEMBERSHIP_LAST_NAME         IN ORDER_DETAIL_UPDATE.MEMBERSHIP_LAST_NAME%TYPE,
IN_RECIPIENT_ID                 IN ORDER_DETAIL_UPDATE.RECIPIENT_ID%TYPE,
IN_RECIPIENT_PHONE_NUMBER       IN ORDER_DETAIL_UPDATE.RECIPIENT_PHONE_NUMBER%TYPE,
IN_RECIPIENT_EXTENSION          IN ORDER_DETAIL_UPDATE.RECIPIENT_EXTENSION%TYPE,
IN_RECIPIENT_FIRST_NAME         IN ORDER_DETAIL_UPDATE.RECIPIENT_FIRST_NAME%TYPE,
IN_RECIPIENT_LAST_NAME          IN ORDER_DETAIL_UPDATE.RECIPIENT_LAST_NAME%TYPE,
IN_RECIPIENT_ADDRESS_1          IN ORDER_DETAIL_UPDATE.RECIPIENT_ADDRESS_1%TYPE,
IN_RECIPIENT_ADDRESS_2          IN ORDER_DETAIL_UPDATE.RECIPIENT_ADDRESS_2%TYPE,
IN_RECIPIENT_CITY               IN ORDER_DETAIL_UPDATE.RECIPIENT_CITY%TYPE,
IN_RECIPIENT_STATE              IN ORDER_DETAIL_UPDATE.RECIPIENT_STATE%TYPE,
IN_RECIPIENT_ZIP_CODE           IN ORDER_DETAIL_UPDATE.RECIPIENT_ZIP_CODE%TYPE,
IN_RECIPIENT_COUNTRY            IN ORDER_DETAIL_UPDATE.RECIPIENT_COUNTRY%TYPE,
IN_RECIPIENT_ADDRESS_TYPE       IN ORDER_DETAIL_UPDATE.RECIPIENT_ADDRESS_TYPE%TYPE,
IN_RECIPIENT_BUSINESS_NAME      IN ORDER_DETAIL_UPDATE.RECIPIENT_BUSINESS_NAME%TYPE,
IN_RECIPIENT_BUSINESS_INFO      IN ORDER_DETAIL_UPDATE.RECIPIENT_BUSINESS_INFO%TYPE,
IN_RELEASE_INFO_INDICATOR       IN ORDER_DETAIL_UPDATE.RELEASE_INFO_INDICATOR%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAIL_UPDATE.FLORIST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the delivery info in the
        hold table

Input:
        order_detail_id                 number
        updated_by                      varchar2
        source_code                     varchar2
        delivery_date                   date
        delivery_date_range_end         date
        card_message                    varchar2
        card_signature                  varchar2
        special_instructions            varchar2
        ship_method                     varchar2
        ship_date                       date
        membership_number               varchar2
        membership_first_name           varchar2
        membership_last_name            varchar2
        recipient_id                    number
        recipient_phone_number          varchar2
        recipient_extension             varchar2
        recipient_first_name            varchar2
        recipient_last_name             varchar2
        recipient_address_1             varchar2
        recipient_address_2             varchar2
        recipient_city                  varchar2
        recipient_state                 varchar2
        recipient_zip_code              varchar2
        recipient_country               varchar2
        recipient_address_type          varchar2
        recipient_business_name         varchar2
        recipient_business_info         varchar2
        release_info_indicator          varchar2
        florist_id                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_detail_update
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        source_code = in_source_code,
        delivery_date = in_delivery_date,
        delivery_date_range_end = in_delivery_date_range_end,
        card_message = in_card_message,
        card_signature = in_card_signature,
        special_instructions = in_special_instructions,
        ship_method = in_ship_method,
        ship_date = in_ship_date,
        membership_number = in_membership_number,
        membership_first_name = in_membership_first_name,
        membership_last_name = in_membership_last_name,
        recipient_id = in_recipient_id,
        recipient_phone_number = in_recipient_phone_number,
        recipient_extension = in_recipient_extension,
        recipient_first_name = in_recipient_first_name,
        recipient_last_name = in_recipient_last_name,
        recipient_address_1 = in_recipient_address_1,
        recipient_address_2 = in_recipient_address_2,
        recipient_city = in_recipient_city,
        recipient_state = in_recipient_state,
        recipient_zip_code = in_recipient_zip_code,
        recipient_country = in_recipient_country,
        recipient_address_type = in_recipient_address_type,
        recipient_business_name = in_recipient_business_name,
        recipient_business_info = in_recipient_business_info,
        release_info_indicator = in_release_info_indicator,
        florist_id = in_florist_id
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_DELIVERY_INFO [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_DELIVERY_INFO;



PROCEDURE UPDATE_PRODUCT_DETAIL
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAIL_UPDATE.DELIVERY_DATE%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN ORDER_DETAIL_UPDATE.DELIVERY_DATE_RANGE_END%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAIL_UPDATE.PRODUCT_ID%TYPE,
IN_COLOR_1                      IN ORDER_DETAIL_UPDATE.COLOR_1%TYPE,
IN_COLOR_2                      IN ORDER_DETAIL_UPDATE.COLOR_2%TYPE,
IN_SUBSTITUTION_INDICATOR       IN ORDER_DETAIL_UPDATE.SUBSTITUTION_INDICATOR%TYPE,
IN_SPECIAL_INSTRUCTIONS         IN ORDER_DETAIL_UPDATE.SPECIAL_INSTRUCTIONS%TYPE,
IN_SHIP_METHOD                  IN ORDER_DETAIL_UPDATE.SHIP_METHOD%TYPE,
IN_SIZE_INDICATOR               IN ORDER_DETAIL_UPDATE.SIZE_INDICATOR%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAIL_UPDATE.SHIP_DATE%TYPE,
IN_SUBCODE                      IN ORDER_DETAIL_UPDATE.SUBCODE%TYPE,
IN_PERSONAL_GREETING_ID         IN ORDER_DETAIL_UPDATE.PERSONAL_GREETING_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating product detail info
        for the work order

Input:
        order_detail_id                 number
        updated_by                      varchar2
        delivery_date                   date
        delivery_date_range_end         date
        product_id                      varchar2
        color_1                         varchar2
        color_2                         varchar2
        substitution_indicator          char
        special_instructions            varchar2
        ship_method                     varchar2
        size_indicator                  varchar2
        ship_date                       date
        subcode                         varchar2
        personal_greeting_id		varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_detail_update
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        delivery_date = in_delivery_date,
        delivery_date_range_end = in_delivery_date_range_end,
        product_id = in_product_id,
        color_1 = in_color_1,
        color_2 = in_color_2,
        substitution_indicator = in_substitution_indicator,
        special_instructions = in_special_instructions,
        ship_method = in_ship_method,
        size_indicator = in_size_indicator,
        ship_date = in_ship_date,
        subcode = in_subcode,
        personal_greeting_id = in_personal_greeting_id
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_PRODUCT_DETAIL [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PRODUCT_DETAIL;

PROCEDURE UPDATE_PRODUCT_AMOUNTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_DETAIL_UPDATE.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_DETAIL_UPDATE.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_DETAIL_UPDATE.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_DETAIL_UPDATE.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_DETAIL_UPDATE.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_DETAIL_UPDATE.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_DETAIL_UPDATE.TAX%TYPE,
IN_ACTUAL_PRODUCT_AMOUNT        IN ORDER_DETAIL_UPDATE.ACTUAL_PRODUCT_AMOUNT%TYPE,
IN_ACTUAL_ADD_ON_AMOUNT         IN ORDER_DETAIL_UPDATE.ACTUAL_ADD_ON_AMOUNT%TYPE,
IN_ACTUAL_SHIPPING_FEE          IN ORDER_DETAIL_UPDATE.ACTUAL_SHIPPING_FEE%TYPE,
IN_ACTUAL_SERVICE_FEE           IN ORDER_DETAIL_UPDATE.ACTUAL_SERVICE_FEE%TYPE,
IN_ACTUAL_DISCOUNT_AMOUNT       IN ORDER_DETAIL_UPDATE.ACTUAL_DISCOUNT_AMOUNT%TYPE,
IN_ACTUAL_SHIPPING_TAX          IN ORDER_DETAIL_UPDATE.ACTUAL_SHIPPING_TAX%TYPE,
IN_ACTUAL_TAX                   IN ORDER_DETAIL_UPDATE.ACTUAL_TAX%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_DETAIL_UPDATE.SERVICE_FEE_SAVED%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_DETAIL_UPDATE.SHIPPING_FEE_SAVED%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_PRODUCT_AMOUNTS
(
IN_ORDER_DETAIL_ID,
IN_UPDATED_BY,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SERVICE_FEE,
IN_SHIPPING_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_TAX,
IN_ACTUAL_PRODUCT_AMOUNT,
IN_ACTUAL_ADD_ON_AMOUNT,
IN_ACTUAL_SHIPPING_FEE,
IN_ACTUAL_SERVICE_FEE,
IN_ACTUAL_DISCOUNT_AMOUNT,
IN_ACTUAL_SHIPPING_TAX,
IN_ACTUAL_TAX,
IN_SERVICE_FEE_SAVED,
IN_SHIPPING_FEE_SAVED,
0,
0,
OUT_STATUS,
OUT_MESSAGE
);

END UPDATE_PRODUCT_AMOUNTS;

PROCEDURE UPDATE_PRODUCT_AMOUNTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_DETAIL_UPDATE.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_DETAIL_UPDATE.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_DETAIL_UPDATE.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_DETAIL_UPDATE.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_DETAIL_UPDATE.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_DETAIL_UPDATE.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_DETAIL_UPDATE.TAX%TYPE,
IN_ACTUAL_PRODUCT_AMOUNT        IN ORDER_DETAIL_UPDATE.ACTUAL_PRODUCT_AMOUNT%TYPE,
IN_ACTUAL_ADD_ON_AMOUNT         IN ORDER_DETAIL_UPDATE.ACTUAL_ADD_ON_AMOUNT%TYPE,
IN_ACTUAL_SHIPPING_FEE          IN ORDER_DETAIL_UPDATE.ACTUAL_SHIPPING_FEE%TYPE,
IN_ACTUAL_SERVICE_FEE           IN ORDER_DETAIL_UPDATE.ACTUAL_SERVICE_FEE%TYPE,
IN_ACTUAL_DISCOUNT_AMOUNT       IN ORDER_DETAIL_UPDATE.ACTUAL_DISCOUNT_AMOUNT%TYPE,
IN_ACTUAL_SHIPPING_TAX          IN ORDER_DETAIL_UPDATE.ACTUAL_SHIPPING_TAX%TYPE,
IN_ACTUAL_TAX                   IN ORDER_DETAIL_UPDATE.ACTUAL_TAX%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_DETAIL_UPDATE.SERVICE_FEE_SAVED%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_DETAIL_UPDATE.SHIPPING_FEE_SAVED%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_DETAIL_UPDATE.ADD_ON_DISCOUNT_AMOUNT%TYPE,
IN_ACT_ADD_ON_DISCOUNT_AMOUNT   IN ORDER_DETAIL_UPDATE.ACTUAL_ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the product amounts on the
        work order

Input:
        order_detail_id                 number
        updated_by                      varchar2
        product_amount                  number
        add_on_amount                   number
        service_fee                     number
        shipping_fee                    number
        discount_amount                 number
        shipping_tax                    number
        tax                             number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_detail_update
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        product_amount = NVL(in_product_amount, product_amount),
        add_on_amount = NVL(in_add_on_amount, add_on_amount),
        service_fee = NVL(in_service_fee, service_fee),
        shipping_fee = NVL(in_shipping_fee, shipping_fee),
        discount_amount = NVL(in_discount_amount, discount_amount),
        shipping_tax = NVL(in_shipping_tax, shipping_tax),
        tax = NVL(in_tax, tax),
        actual_product_amount = NVL (in_actual_product_amount, actual_product_amount),
        actual_add_on_amount = NVL (in_actual_add_on_amount, actual_add_on_amount),
        actual_shipping_fee = NVL (in_actual_shipping_fee, actual_shipping_fee),
        actual_service_fee = NVL (in_actual_service_fee, actual_service_fee),
        actual_discount_amount = NVL (in_actual_discount_amount, actual_discount_amount),
        actual_shipping_tax = NVL (in_actual_shipping_tax, actual_shipping_tax),
        actual_tax = NVL (in_actual_tax, actual_tax),
        service_fee_saved = NVL(in_service_fee_saved, service_fee_saved),
        shipping_fee_saved = NVL(in_shipping_fee_saved, shipping_fee_saved),
        add_on_discount_amount = NVL(in_add_on_discount_amount, add_on_discount_amount),
        actual_add_on_discount_amount = NVL(in_act_add_on_discount_amount, actual_add_on_discount_amount)
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_PRODUCT_AMOUNTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_PRODUCT_AMOUNTS;



PROCEDURE INSERT_COMMENTS_UPDATE
(
IN_CUSTOMER_ID                  IN COMMENTS_UPDATE.CUSTOMER_ID%TYPE,
IN_ORDER_GUID                   IN COMMENTS_UPDATE.ORDER_GUID%TYPE,
IN_ORDER_DETAIL_ID              IN COMMENTS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_COMMENT_ORIGIN               IN COMMENTS_UPDATE.COMMENT_ORIGIN%TYPE,
IN_DNIS_ID                      IN COMMENTS_UPDATE.DNIS_ID%TYPE,
IN_COMMENT_TEXT                 IN COMMENTS_UPDATE.COMMENT_TEXT%TYPE,
IN_COMMENT_TYPE                 IN COMMENTS_UPDATE.COMMENT_TYPE%TYPE,
IN_UPDATED_BY                   IN COMMENTS_UPDATE.UPDATED_BY%TYPE,
OUT_COMMENT_ID                  OUT COMMENTS_UPDATE.COMMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting comments in the
        hold table

Input:
        customer_id                     number
        order_guid                      varchar2
        order_detail_id                 number
        comment_origin                  varchar2
        dnis_id                         number
        comment_text                    varchar2
        comment_type                    varchar2
        updated_by                      varchar2

Output:
        comment_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO comments_update
(
        comment_id,
        customer_id,
        order_guid,
        order_detail_id,
        comment_origin,
        dnis_id,
        comment_text,
        comment_type,
        updated_on,
        updated_by
)
VALUES
(
        comment_id_sq.nextval,
        in_customer_id,
        in_order_guid,
        in_order_detail_id,
        in_comment_origin,
        in_dnis_id,
        in_comment_text,
        in_comment_type,
        sysdate,
        in_updated_by
) RETURNING comment_id INTO out_comment_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_COMMENTS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_COMMENTS_UPDATE;


PROCEDURE UPDATE_COMMENTS_UPDATE
(
IN_COMMENT_ID                   IN COMMENTS_UPDATE.COMMENT_ID%TYPE,
IN_COMMENT_TEXT                 IN COMMENTS_UPDATE.COMMENT_TEXT%TYPE,
IN_COMMENT_TYPE                 IN COMMENTS_UPDATE.COMMENT_TYPE%TYPE,
IN_UPDATED_BY                   IN COMMENTS_UPDATE.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the comment work table.

Input:
        comment_id                      number
        comment_text                    varchar2
        comment_type                    varchar2
        updated_on                      date
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  comments_update
SET
        comment_text = NVL(in_comment_text, comment_text),
        comment_type = NVL(in_comment_type, comment_type),
        updated_on = sysdate,
        updated_by = in_updated_by
WHERE   comment_id = in_comment_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_COMMENTS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_COMMENTS_UPDATE;



PROCEDURE INSERT_ORDER_CONTACT_UPDATE
(
IN_ORDER_GUID                   IN ORDER_CONTACT_UPDATE.ORDER_GUID%TYPE,
IN_UPDATED_BY                   IN ORDER_CONTACT_UPDATE.UPDATED_BY%TYPE,
IN_FIRST_NAME                   IN ORDER_CONTACT_UPDATE.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN ORDER_CONTACT_UPDATE.LAST_NAME%TYPE,
IN_PHONE_NUMBER                 IN ORDER_CONTACT_UPDATE.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN ORDER_CONTACT_UPDATE.EXTENSION%TYPE,
IN_EMAIL_ADDRESS                IN ORDER_CONTACT_UPDATE.EMAIL_ADDRESS%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_CONTACT_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_ORDER_CONTACT_INFO_ID       OUT ORDER_CONTACT_UPDATE.ORDER_CONTACT_INFO_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting alternate contact info.

Input:

        order_guid                      varchar2
        updated_by                      varchar2
        first_name                      varchar2
        last_name                       varchar2
        phone_number                    varchar2
        extension                       varchar2
        email_address                   varchar2
        order_detail_id                 number

Output:
        order_contact_info_id           number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO order_contact_update
(
        order_contact_info_id,
        order_guid,
        updated_on,
        updated_by,
        first_name,
        last_name,
        phone_number,
        extension,
        email_address,
        order_detail_id
)
VALUES
(
        key.keygen ('ORDER_CONTACT_INFO'),
        in_order_guid,
        sysdate,
        in_updated_by,
        in_first_name,
        in_last_name,
        in_phone_number,
        in_extension,
        in_email_address,
        in_order_detail_id
) RETURNING order_contact_info_id INTO out_order_contact_info_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_ORDER_CONTACT_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_CONTACT_UPDATE;


PROCEDURE UPDATE_ORDER_CONTACT_UPDATE
(
IN_ORDER_CONTACT_INFO_ID        IN ORDER_CONTACT_UPDATE.ORDER_CONTACT_INFO_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_CONTACT_UPDATE.UPDATED_BY%TYPE,
IN_FIRST_NAME                   IN ORDER_CONTACT_UPDATE.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN ORDER_CONTACT_UPDATE.LAST_NAME%TYPE,
IN_PHONE_NUMBER                 IN ORDER_CONTACT_UPDATE.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN ORDER_CONTACT_UPDATE.EXTENSION%TYPE,
IN_EMAIL_ADDRESS                IN ORDER_CONTACT_UPDATE.EMAIL_ADDRESS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating altername contact info

Input:
        order_contact_info_id           number
        updated_by                      varchar2
        first_name                      varchar2
        last_name                       varchar2
        phone_number                    varchar2
        extension                       varchar2
        email_address                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_contact_update
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        first_name = in_first_name,
        last_name = in_last_name,
        phone_number = in_phone_number,
        extension = in_extension,
        email_address = in_email_address
WHERE   order_contact_info_id = in_order_contact_info_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_ORDER_CONTACT_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_CONTACT_UPDATE;

PROCEDURE INSERT_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_ADD_ONS_UPDATE.UPDATED_BY%TYPE,
IN_ADD_ON_CODE                  IN ORDER_ADD_ONS_UPDATE.ADD_ON_CODE%TYPE,
IN_ADD_ON_QUANTITY              IN ORDER_ADD_ONS_UPDATE.ADD_ON_QUANTITY%TYPE,
OUT_ORDER_ADD_ON_ID             OUT ORDER_ADD_ONS_UPDATE.ORDER_ADD_ON_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_DETAIL_ID,
IN_UPDATED_BY,
IN_ADD_ON_CODE,
IN_ADD_ON_QUANTITY,
0,
0,
OUT_ORDER_ADD_ON_ID,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ORDER_ADD_ONS_UPDATE;

PROCEDURE INSERT_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_ADD_ONS_UPDATE.UPDATED_BY%TYPE,
IN_ADD_ON_CODE                  IN ORDER_ADD_ONS_UPDATE.ADD_ON_CODE%TYPE,
IN_ADD_ON_QUANTITY              IN ORDER_ADD_ONS_UPDATE.ADD_ON_QUANTITY%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_ADD_ONS_UPDATE.ADD_ON_AMOUNT%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_ADD_ONS_UPDATE.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_ORDER_ADD_ON_ID             OUT ORDER_ADD_ONS_UPDATE.ORDER_ADD_ON_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting add ons

Input:
        order_detail_id                 number
        updated_by                      varchar2
        add_on_code                     varchar2
        add_on_quantity                 number

Output:
        order_add_on_id                 number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
cursor hist_cur is
        select  addon_history_id
        from    ftd_apps.addon_history
        where   addon_id = in_add_on_code
        and     status = 'Active';

v_add_on_history_id      integer;

BEGIN

OPEN hist_cur;
FETCH hist_cur INTO v_add_on_history_id;
CLOSE hist_cur;

INSERT INTO order_add_ons_update
(
        order_add_on_id,
        order_detail_id,
        updated_on,
        updated_by,
        add_on_code,
        add_on_quantity,
        add_on_history_id,
        add_on_amount,
        add_on_discount_amount
)
VALUES
(
        key.keygen ('ADD_ONS'),
        in_order_detail_id,
        sysdate,
        in_updated_by,
        in_add_on_code,
        in_add_on_quantity,
        v_add_on_history_id,
        nvl (in_add_on_amount, 0),
        nvl (in_add_on_discount_amount, 0)
) RETURNING order_add_on_id INTO out_order_add_on_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_ORDER_ADD_ONS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_ADD_ONS_UPDATE;

PROCEDURE UPDATE_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_ADD_ON_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_ADD_ON_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_ADD_ONS_UPDATE.UPDATED_BY%TYPE,
IN_ADD_ON_CODE                  IN ORDER_ADD_ONS_UPDATE.ADD_ON_CODE%TYPE,
IN_ADD_ON_QUANTITY              IN ORDER_ADD_ONS_UPDATE.ADD_ON_QUANTITY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

UPDATE_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_ADD_ON_ID,
IN_UPDATED_BY,
IN_ADD_ON_CODE,
IN_ADD_ON_QUANTITY,
0,
0,
OUT_STATUS,
OUT_MESSAGE
);


END UPDATE_ORDER_ADD_ONS_UPDATE;

PROCEDURE UPDATE_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_ADD_ON_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_ADD_ON_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_ADD_ONS_UPDATE.UPDATED_BY%TYPE,
IN_ADD_ON_CODE                  IN ORDER_ADD_ONS_UPDATE.ADD_ON_CODE%TYPE,
IN_ADD_ON_QUANTITY              IN ORDER_ADD_ONS_UPDATE.ADD_ON_QUANTITY%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_ADD_ONS_UPDATE.ADD_ON_AMOUNT%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_ADD_ONS_UPDATE.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating add ons

Input:
        order_add_on_id                 number
        updated_by                      varchar2
        add_on_code                     varchar2
        add_on_quantity                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
cursor hist_cur is
        select  addon_history_id
        from    ftd_apps.addon_history
        where   addon_id = in_add_on_code
        and     status = 'Active';

v_add_on_history_id      integer;

BEGIN

OPEN hist_cur;
FETCH hist_cur INTO v_add_on_history_id;
CLOSE hist_cur;

UPDATE  order_add_ons_update
SET     add_on_code = in_add_on_code,
        add_on_quantity = in_add_on_quantity,
        updated_on = sysdate,
        updated_by = in_updated_by,
        add_on_history_id = v_add_on_history_id,
        add_on_amount = nvl(in_add_on_amount, add_on_amount),
        add_on_discount_amount = nvl(in_add_on_discount_amount, add_on_discount_amount)
WHERE   order_add_on_id = in_order_add_on_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_ORDER_ADD_ONS_UPDATE;


PROCEDURE DELETE_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_ADD_ON_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_ADD_ON_ID%TYPE,
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting order add ons by either the
        add on id or the order detail id

Input:
        order_add_on_id                 number
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF in_order_add_on_id IS NOT NULL THEN
        DELETE FROM order_add_ons_update
        WHERE   order_add_on_id = in_order_add_on_id;
ELSE
        DELETE FROM order_add_ons_update
        WHERE   order_detail_id = in_order_detail_id;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED DELETE_ORDER_ADD_ONS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ORDER_ADD_ONS_UPDATE;


PROCEDURE DELETE_ORDER_ADD_ONS_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_ADD_ONS_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting order add ons by the order detail id

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

IF in_order_detail_id IS NOT NULL THEN
        DELETE FROM order_add_ons_update
        WHERE   order_detail_id = in_order_detail_id;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED DELETE_ORDER_ADD_ONS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_ORDER_ADD_ONS_UPDATE;

PROCEDURE INSERT_ORDER_BILLS_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS_UPDATE.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS_UPDATE.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS_UPDATE.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS_UPDATE.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS_UPDATE.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS_UPDATE.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_BILLS_UPDATE.TAX%TYPE,
IN_ADDITIONAL_BILL_INDICATOR    IN ORDER_BILLS_UPDATE.ADDITIONAL_BILL_INDICATOR%TYPE,
IN_UPDATED_BY                   IN ORDER_BILLS_UPDATE.UPDATED_BY%TYPE,
IN_SAME_DAY_FEE                 IN number,
IN_BILL_STATUS                  IN ORDER_BILLS_UPDATE.BILL_STATUS%TYPE,
IN_SERVICE_FEE_TAX              IN ORDER_BILLS_UPDATE.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ORDER_BILLS_UPDATE.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ORDER_BILLS_UPDATE.WHOLESALE_AMOUNT%TYPE,
IN_TRANSACTION_AMOUNT           IN ORDER_BILLS_UPDATE.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE                    IN ORDER_BILLS_UPDATE.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE       IN ORDER_BILLS_UPDATE.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE                IN ORDER_BILLS_UPDATE.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST                 IN ORDER_BILLS_UPDATE.PARTNER_COST%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_BILL_DATE                    IN ORDER_BILLS_UPDATE.BILL_DATE%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS_UPDATE.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS_UPDATE.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME                    IN ORDER_BILLS_UPDATE.TAX1_NAME%TYPE,
IN_TAX1_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN ORDER_BILLS_UPDATE.TAX1_RATE%TYPE,
IN_TAX1_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX1_AMOUNT%TYPE,
IN_TAX2_NAME                    IN ORDER_BILLS_UPDATE.TAX2_NAME%TYPE,
IN_TAX2_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN ORDER_BILLS_UPDATE.TAX2_RATE%TYPE,
IN_TAX2_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX2_AMOUNT%TYPE,
IN_TAX3_NAME                    IN ORDER_BILLS_UPDATE.TAX3_NAME%TYPE,
IN_TAX3_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN ORDER_BILLS_UPDATE.TAX3_RATE%TYPE,
IN_TAX3_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX3_AMOUNT%TYPE,
IN_TAX4_NAME                    IN ORDER_BILLS_UPDATE.TAX4_NAME%TYPE,
IN_TAX4_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN ORDER_BILLS_UPDATE.TAX4_RATE%TYPE,
IN_TAX4_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX4_AMOUNT%TYPE,
IN_TAX5_NAME                    IN ORDER_BILLS_UPDATE.TAX5_NAME%TYPE,
IN_TAX5_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN ORDER_BILLS_UPDATE.TAX5_RATE%TYPE,
IN_TAX5_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX5_AMOUNT%TYPE,
OUT_ORDER_BILL_ID               OUT ORDER_BILLS_UPDATE.ORDER_BILL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN 

INSERT_ORDER_BILLS_UPDATE
(
IN_ORDER_DETAIL_ID,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SERVICE_FEE,
IN_SHIPPING_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_TAX,
IN_ADDITIONAL_BILL_INDICATOR,
IN_UPDATED_BY,
IN_SAME_DAY_FEE,
IN_BILL_STATUS,
IN_SERVICE_FEE_TAX,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_TRANSACTION_AMOUNT,
IN_PDB_PRICE,
IN_DISCOUNT_PRODUCT_PRICE,
IN_DISCOUNT_TYPE,
IN_PARTNER_COST,
IN_ACCT_TRANS_IND,
IN_BILL_DATE,
IN_SHIPPING_FEE_SAVED,
IN_SERVICE_FEE_SAVED,
IN_TAX1_NAME,
IN_TAX1_DESCRIPTION,
IN_TAX1_RATE,
IN_TAX1_AMOUNT,
IN_TAX2_NAME,
IN_TAX2_DESCRIPTION,
IN_TAX2_RATE,
IN_TAX2_AMOUNT,
IN_TAX3_NAME,
IN_TAX3_DESCRIPTION,
IN_TAX3_RATE,
IN_TAX3_AMOUNT,
IN_TAX4_NAME,
IN_TAX4_DESCRIPTION,
IN_TAX4_RATE,
IN_TAX4_AMOUNT,
IN_TAX5_NAME,
IN_TAX5_DESCRIPTION,
IN_TAX5_RATE,
IN_TAX5_AMOUNT,
0,
OUT_ORDER_BILL_ID,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ORDER_BILLS_UPDATE;

PROCEDURE INSERT_ORDER_BILLS_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_PRODUCT_AMOUNT               IN ORDER_BILLS_UPDATE.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ORDER_BILLS_UPDATE.ADD_ON_AMOUNT%TYPE,
IN_SERVICE_FEE                  IN ORDER_BILLS_UPDATE.SERVICE_FEE%TYPE,
IN_SHIPPING_FEE                 IN ORDER_BILLS_UPDATE.SHIPPING_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ORDER_BILLS_UPDATE.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ORDER_BILLS_UPDATE.SHIPPING_TAX%TYPE,
IN_TAX                          IN ORDER_BILLS_UPDATE.TAX%TYPE,
IN_ADDITIONAL_BILL_INDICATOR    IN ORDER_BILLS_UPDATE.ADDITIONAL_BILL_INDICATOR%TYPE,
IN_UPDATED_BY                   IN ORDER_BILLS_UPDATE.UPDATED_BY%TYPE,
IN_SAME_DAY_FEE                 IN number,
IN_BILL_STATUS                  IN ORDER_BILLS_UPDATE.BILL_STATUS%TYPE,
IN_SERVICE_FEE_TAX              IN ORDER_BILLS_UPDATE.SERVICE_FEE_TAX%TYPE,
IN_COMMISSION_AMOUNT            IN ORDER_BILLS_UPDATE.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ORDER_BILLS_UPDATE.WHOLESALE_AMOUNT%TYPE,
IN_TRANSACTION_AMOUNT           IN ORDER_BILLS_UPDATE.TRANSACTION_AMOUNT%TYPE,
IN_PDB_PRICE                    IN ORDER_BILLS_UPDATE.PDB_PRICE%TYPE,
IN_DISCOUNT_PRODUCT_PRICE       IN ORDER_BILLS_UPDATE.DISCOUNT_PRODUCT_PRICE%TYPE,
IN_DISCOUNT_TYPE                IN ORDER_BILLS_UPDATE.DISCOUNT_TYPE%TYPE,
IN_PARTNER_COST                 IN ORDER_BILLS_UPDATE.PARTNER_COST%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_BILL_DATE                    IN ORDER_BILLS_UPDATE.BILL_DATE%TYPE,
IN_SHIPPING_FEE_SAVED           IN ORDER_BILLS_UPDATE.SHIPPING_FEE_SAVED%TYPE,
IN_SERVICE_FEE_SAVED            IN ORDER_BILLS_UPDATE.SERVICE_FEE_SAVED%TYPE,
IN_TAX1_NAME                    IN ORDER_BILLS_UPDATE.TAX1_NAME%TYPE,
IN_TAX1_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX1_DESCRIPTION%TYPE,
IN_TAX1_RATE                    IN ORDER_BILLS_UPDATE.TAX1_RATE%TYPE,
IN_TAX1_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX1_AMOUNT%TYPE,
IN_TAX2_NAME                    IN ORDER_BILLS_UPDATE.TAX2_NAME%TYPE,
IN_TAX2_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX2_DESCRIPTION%TYPE,
IN_TAX2_RATE                    IN ORDER_BILLS_UPDATE.TAX2_RATE%TYPE,
IN_TAX2_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX2_AMOUNT%TYPE,
IN_TAX3_NAME                    IN ORDER_BILLS_UPDATE.TAX3_NAME%TYPE,
IN_TAX3_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX3_DESCRIPTION%TYPE,
IN_TAX3_RATE                    IN ORDER_BILLS_UPDATE.TAX3_RATE%TYPE,
IN_TAX3_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX3_AMOUNT%TYPE,
IN_TAX4_NAME                    IN ORDER_BILLS_UPDATE.TAX4_NAME%TYPE,
IN_TAX4_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX4_DESCRIPTION%TYPE,
IN_TAX4_RATE                    IN ORDER_BILLS_UPDATE.TAX4_RATE%TYPE,
IN_TAX4_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX4_AMOUNT%TYPE,
IN_TAX5_NAME                    IN ORDER_BILLS_UPDATE.TAX5_NAME%TYPE,
IN_TAX5_DESCRIPTION             IN ORDER_BILLS_UPDATE.TAX5_DESCRIPTION%TYPE,
IN_TAX5_RATE                    IN ORDER_BILLS_UPDATE.TAX5_RATE%TYPE,
IN_TAX5_AMOUNT                  IN ORDER_BILLS_UPDATE.TAX5_AMOUNT%TYPE,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ORDER_BILLS_UPDATE.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_ORDER_BILL_ID               OUT ORDER_BILLS_UPDATE.ORDER_BILL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting order bills.

Input:
        order_detail_id                 number
        product_amount                  number
        add_on_amount                   number
        service_fee                     number
        shipping_fee                    number
        discount_amount                 number
        shipping_tax                    number
        tax                             number
        additional_bill_indicator       char
        updated_by                      varchar2
        same_day_fee                    number  - not used dropped from table
        bill_status                     varchar2
        service_fee_tax                 number
        commission_amount               number
        wholesale_amount                number
        transaction_amount              number
        pdb_price                       number
        discount_product_price          number
        discount_type                   char
        partner_cost                    number
        new_order_seq                   varchar2 (Y/N - if a new order sequence should be created for
                                                  the given order detail in the accouting transactions)
	bill_date                       date
Output:
        order_bill_id                   number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR order_detail_cur IS
        SELECT  delivery_date,
                product_id,
                source_code,
                ship_method
        FROM    order_detail_update
        WHERE   order_detail_id = in_order_detail_id;

CURSOR exists_cur IS
        SELECT  order_bill_id
        FROM    order_bills_update
        WHERE   order_detail_id = in_order_detail_id;

odrow   order_detail_cur%rowtype;
v_transaction_type varchar2(10);

BEGIN

OPEN exists_cur;
FETCH exists_cur INTO out_order_bill_id;
CLOSE exists_cur;

IF out_order_bill_id is not null THEN

    UPDATE ORDER_BILLS_UPDATE
    SET product_amount = nvl (in_product_amount, 0),
        add_on_amount = nvl (in_add_on_amount, 0),
        service_fee = nvl (in_service_fee, 0),
        shipping_fee = nvl (in_shipping_fee, 0),
        discount_amount = nvl (in_discount_amount, 0),
        shipping_tax = nvl (in_shipping_tax, 0),
        tax = nvl (in_tax, 0),
        additional_bill_indicator = nvl (in_additional_bill_indicator, 'N'),
        updated_on = sysdate,
        updated_by = in_updated_by,
        bill_status = in_bill_status,
        service_fee_tax = nvl (in_service_fee_tax, 0),
        commission_amount = nvl (in_commission_amount, 0),
        wholesale_amount = nvl (in_wholesale_amount, 0),
        transaction_amount = nvl (in_transaction_amount, 0),
        pdb_price = nvl (in_pdb_price, 0),
        discount_product_price = nvl (in_discount_product_price, 0),
        discount_type = nvl (in_discount_type, 'N'),
        partner_cost = nvl (in_partner_cost, 0),
        bill_date = in_bill_date,
        shipping_fee_saved = nvl (in_shipping_fee_saved, 0),
        service_fee_saved = nvl (in_service_fee_saved, 0),
        tax1_name = in_tax1_name,
        tax1_description = in_tax1_description,
        tax1_rate = in_tax1_rate,
        tax1_amount = in_tax1_amount,
        tax2_name = in_tax2_name,
        tax2_description = in_tax2_description,
        tax2_rate = in_tax2_rate,
        tax2_amount = in_tax2_amount,
        tax3_name = in_tax3_name,
        tax3_description = in_tax3_description,
        tax3_rate = in_tax3_rate,
        tax3_amount = in_tax3_amount,
        tax4_name = in_tax4_name,
        tax4_description = in_tax4_description,
        tax4_rate = in_tax4_rate,
        tax4_amount = in_tax4_amount,
        tax5_name = in_tax5_name,
        tax5_description = in_tax5_description,
        tax5_rate = in_tax5_rate,
        tax5_amount = in_tax5_amount,
        add_on_discount_amount = nvl (in_add_on_discount_amount, 0)
    WHERE order_bill_id = out_order_bill_id;

ELSE

INSERT INTO order_bills_update
(
        order_bill_id,
        order_detail_id,
        product_amount,
        add_on_amount,
        service_fee,
        shipping_fee,
        discount_amount,
        shipping_tax,
        tax,
        additional_bill_indicator,
        updated_on,
        updated_by,
        -- same_day_fee,
        bill_status,
        service_fee_tax,
        commission_amount,
        wholesale_amount,
        transaction_amount,
        pdb_price,
        discount_product_price,
        discount_type,
        partner_cost,
        bill_date,
        shipping_fee_saved,
        service_fee_saved,
        tax1_name,
        tax1_description,
        tax1_rate,
        tax1_amount,
        tax2_name,
        tax2_description,
        tax2_rate,
        tax2_amount,
        tax3_name,
        tax3_description,
        tax3_rate,
        tax3_amount,
        tax4_name,
        tax4_description,
        tax4_rate,
        tax4_amount,
        tax5_name,
        tax5_description,
        tax5_rate,
        tax5_amount,
        add_on_discount_amount
)
VALUES
(
        order_bill_id_sq.nextval,
        in_order_detail_id,
        nvl (in_product_amount, 0),
        nvl (in_add_on_amount, 0),
        nvl (in_service_fee, 0),
        nvl (in_shipping_fee, 0),
        nvl (in_discount_amount, 0),
        nvl (in_shipping_tax, 0),
        nvl (in_tax, 0),
        nvl (in_additional_bill_indicator, 'N'),
        sysdate,
        in_updated_by,
        -- nvl (in_same_day_fee, 0),
        in_bill_status,
        nvl (in_service_fee_tax, 0),
        nvl (in_commission_amount, 0),
        nvl (in_wholesale_amount, 0),
        nvl (in_transaction_amount, 0),
        nvl (in_pdb_price, 0),
        nvl (in_discount_product_price, 0),
        NVL (in_discount_type, 'N'),
        nvl (in_partner_cost, 0),
        in_bill_date,
        nvl (in_shipping_fee_saved, 0),
        nvl (in_service_fee_saved, 0),
        in_tax1_name,
        in_tax1_description,
        in_tax1_rate,
        in_tax1_amount,
        in_tax2_name,
        in_tax2_description,
        in_tax2_rate,
        in_tax2_amount,
        in_tax3_name,
        in_tax3_description,
        in_tax3_rate,
        in_tax3_amount,
        in_tax4_name,
        in_tax4_description,
        in_tax4_rate,
        in_tax4_amount,
        in_tax5_name,
        in_tax5_description,
        in_tax5_rate,
        in_tax5_amount,
        nvl (in_add_on_discount_amount, 0)
) RETURNING order_bill_id INTO out_order_bill_id;

END IF;

IF in_acct_trans_ind = 'Y' THEN
        OPEN order_detail_cur;
        FETCH order_detail_cur INTO odrow;
        CLOSE order_detail_cur;

        IF in_additional_bill_indicator IS NULL OR
           in_additional_bill_indicator = 'N' THEN
           v_transaction_type := 'Order';
        ELSE
           v_transaction_type := 'Add Bill';
        END IF;

        INSERT_ACCOUNTING_TRANS_UPD
        (
                IN_TRANSACTION_TYPE=>v_transaction_type,
                IN_TRANSACTION_DATE=>sysdate,
                IN_ORDER_DETAIL_ID=>in_order_detail_id,
                IN_DELIVERY_DATE=>odrow.delivery_date,
                IN_PRODUCT_ID=>odrow.product_id,
                IN_SOURCE_CODE=>odrow.source_code,
                IN_SHIP_METHOD=>odrow.ship_method,
                IN_PRODUCT_AMOUNT=>in_product_amount,
                IN_ADD_ON_AMOUNT=>in_add_on_amount,
                IN_SHIPPING_FEE=>in_shipping_fee,
                IN_SERVICE_FEE=>in_service_fee,
                IN_DISCOUNT_AMOUNT=>in_discount_amount,
                IN_SHIPPING_TAX=>in_shipping_tax,
                IN_SERVICE_FEE_TAX=>in_service_fee_tax,
                IN_TAX=>in_tax,
                IN_PAYMENT_TYPE=>null,
                IN_REFUND_DISP_CODE=>null,
                IN_COMMISSION_AMOUNT=>in_commission_amount,
                IN_WHOLESALE_AMOUNT=>in_wholesale_amount,
                IN_ADMIN_FEE=>null,
                IN_NEW_ORDER_SEQ=>'N',
                IN_REFUND_ID=>null,
                IN_ORDER_BILL_ID=>out_order_bill_id,
                IN_ADD_ON_DISCOUNT_AMOUNT=>in_add_on_discount_amount,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
ELSE
        OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN INSERT_ORDER_BILLS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ORDER_BILLS_UPDATE;

PROCEDURE INSERT_REFUND_UPDATE
(
IN_REFUND_DISP_CODE             IN REFUND_UPDATE.REFUND_DISP_CODE%TYPE,
IN_UPDATED_BY                   IN REFUND_UPDATE.UPDATED_BY%TYPE,
IN_REFUND_PRODUCT_AMOUNT        IN REFUND_UPDATE.REFUND_PRODUCT_AMOUNT%TYPE,
IN_REFUND_ADDON_AMOUNT          IN REFUND_UPDATE.REFUND_ADDON_AMOUNT%TYPE,
IN_REFUND_SERVICE_FEE           IN REFUND_UPDATE.REFUND_SERVICE_FEE%TYPE,
IN_REFUND_TAX                   IN REFUND_UPDATE.REFUND_TAX%TYPE,
IN_ORDER_DETAIL_ID              IN REFUND_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_RESPONSIBLE_PARTY            IN REFUND_UPDATE.RESPONSIBLE_PARTY%TYPE,
IN_REFUND_STATUS                IN REFUND_UPDATE.REFUND_STATUS%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_REFUND_ADMIN_FEE             IN REFUND_UPDATE.REFUND_ADMIN_FEE%TYPE,
IN_REFUND_SHIPPING_FEE          IN REFUND_UPDATE.REFUND_SHIPPING_FEE%TYPE,
IN_REFUND_SERVICE_FEE_TAX       IN REFUND_UPDATE.REFUND_SERVICE_FEE_TAX%TYPE,
IN_REFUND_SHIPPING_TAX          IN REFUND_UPDATE.REFUND_SHIPPING_TAX%TYPE,
IN_REFUND_DISCOUNT_AMOUNT       IN REFUND_UPDATE.REFUND_DISCOUNT_AMOUNT%TYPE,
IN_REFUND_COMMISSION_AMOUNT     IN REFUND_UPDATE.REFUND_COMMISSION_AMOUNT%TYPE,
IN_REFUND_WHOLESALE_AMOUNT      IN REFUND_UPDATE.REFUND_WHOLESALE_AMOUNT%TYPE,
IN_REFUND_DATE                  IN REFUND_UPDATE.REFUND_DATE%TYPE,
OUT_REFUND_ID                   OUT REFUND_UPDATE.REFUND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_REFUND_UPDATE
(
IN_REFUND_DISP_CODE,
IN_UPDATED_BY,
IN_REFUND_PRODUCT_AMOUNT,
IN_REFUND_ADDON_AMOUNT,
IN_REFUND_SERVICE_FEE,
IN_REFUND_TAX,
IN_ORDER_DETAIL_ID,
IN_RESPONSIBLE_PARTY,
IN_REFUND_STATUS,
IN_ACCT_TRANS_IND,
IN_REFUND_ADMIN_FEE,
IN_REFUND_SHIPPING_FEE,
IN_REFUND_SERVICE_FEE_TAX,
IN_REFUND_SHIPPING_TAX,
IN_REFUND_DISCOUNT_AMOUNT,
IN_REFUND_COMMISSION_AMOUNT,
IN_REFUND_WHOLESALE_AMOUNT,
IN_REFUND_DATE,
0,
OUT_REFUND_ID,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_REFUND_UPDATE;

PROCEDURE INSERT_REFUND_UPDATE
(
IN_REFUND_DISP_CODE             IN REFUND_UPDATE.REFUND_DISP_CODE%TYPE,
IN_UPDATED_BY                   IN REFUND_UPDATE.UPDATED_BY%TYPE,
IN_REFUND_PRODUCT_AMOUNT        IN REFUND_UPDATE.REFUND_PRODUCT_AMOUNT%TYPE,
IN_REFUND_ADDON_AMOUNT          IN REFUND_UPDATE.REFUND_ADDON_AMOUNT%TYPE,
IN_REFUND_SERVICE_FEE           IN REFUND_UPDATE.REFUND_SERVICE_FEE%TYPE,
IN_REFUND_TAX                   IN REFUND_UPDATE.REFUND_TAX%TYPE,
IN_ORDER_DETAIL_ID              IN REFUND_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_RESPONSIBLE_PARTY            IN REFUND_UPDATE.RESPONSIBLE_PARTY%TYPE,
IN_REFUND_STATUS                IN REFUND_UPDATE.REFUND_STATUS%TYPE,
IN_ACCT_TRANS_IND               IN VARCHAR2,
IN_REFUND_ADMIN_FEE             IN REFUND_UPDATE.REFUND_ADMIN_FEE%TYPE,
IN_REFUND_SHIPPING_FEE          IN REFUND_UPDATE.REFUND_SHIPPING_FEE%TYPE,
IN_REFUND_SERVICE_FEE_TAX       IN REFUND_UPDATE.REFUND_SERVICE_FEE_TAX%TYPE,
IN_REFUND_SHIPPING_TAX          IN REFUND_UPDATE.REFUND_SHIPPING_TAX%TYPE,
IN_REFUND_DISCOUNT_AMOUNT       IN REFUND_UPDATE.REFUND_DISCOUNT_AMOUNT%TYPE,
IN_REFUND_COMMISSION_AMOUNT     IN REFUND_UPDATE.REFUND_COMMISSION_AMOUNT%TYPE,
IN_REFUND_WHOLESALE_AMOUNT      IN REFUND_UPDATE.REFUND_WHOLESALE_AMOUNT%TYPE,
IN_REFUND_DATE                  IN REFUND_UPDATE.REFUND_DATE%TYPE,
IN_REFUND_ADDON_DISCOUNT_AMT    IN REFUND_UPDATE.REFUND_ADDON_DISCOUNT_AMT%TYPE,
OUT_REFUND_ID                   OUT REFUND_UPDATE.REFUND_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a refund record

Input:
        refund_disp_code                varchar2
        created_by                      varchar2
        refund_product_amount           number
        refund_addon_amount             number
        refund_serv_ship_fee            number
        refund_tax                      number
        order_detail_id                 number
        responsible_party               varchar2
        refund_date                     date
        refund_status                   varchar2
        acct_trans_ind                  varchar2 (Y/N to add an accounting transaction record)
        refund_admin_fee                number
        refund_shipping_fee             number
        refund_service_tax              number
        refund_shipping_tax             number
        refund_discount_amount          number
        refund_commission_amount        number
        refund_wholesale_amount         number
        refund_date			date

Output:
        refund_id                       number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR order_detail_cur IS
        SELECT  delivery_date,
                product_id,
                source_code,
                ship_method
        FROM    order_detail_update
        WHERE   order_detail_id = in_order_detail_id;

odrow   order_detail_cur%rowtype;

BEGIN

-- insert the refund record
INSERT INTO refund_update
(
        refund_id,
        refund_disp_code,
        updated_on,
        updated_by,
        refund_product_amount,
        refund_addon_amount,
        refund_service_fee,
        refund_tax,
        order_detail_id,
        responsible_party,
        refund_status,
        refund_admin_fee,
        refund_shipping_fee,
        refund_service_fee_tax,
        refund_shipping_tax,
        refund_discount_amount,
        refund_commission_amount,
        refund_wholesale_amount,
        refund_date,
        refund_addon_discount_amt
)
VALUES
(
        refund_id_sq.nextval,
        in_refund_disp_code,
        sysdate,
        in_updated_by,
        nvl(in_refund_product_amount, 0),
        nvl(in_refund_addon_amount, 0),
        nvl(in_refund_service_fee, 0),
        nvl(in_refund_tax, 0),
        in_order_detail_id,
        in_responsible_party,
        in_refund_status,
        nvl(in_refund_admin_fee, 0),
        nvl(in_refund_shipping_fee, 0),
        nvl(in_refund_service_fee_tax, 0),
        nvl(in_refund_shipping_tax, 0),
        nvl(in_refund_discount_amount, 0),
        nvl(in_refund_commission_amount, 0),
        nvl(in_refund_wholesale_amount, 0),
        in_refund_date,
        nvl(IN_REFUND_ADDON_DISCOUNT_AMT, 0)
) RETURNING refund_id INTO out_refund_id;


-- if specified, add the accounting_transaction record
IF in_acct_trans_ind = 'Y' THEN
        OPEN order_detail_cur;
        FETCH order_detail_cur INTO odrow;
        CLOSE order_detail_cur;

        INSERT_ACCOUNTING_TRANS_UPD
        (
                IN_TRANSACTION_TYPE=>'Refund',
                IN_TRANSACTION_DATE=>sysdate,
                IN_ORDER_DETAIL_ID=>in_order_detail_id,
                IN_DELIVERY_DATE=>odrow.delivery_date,
                IN_PRODUCT_ID=>odrow.product_id,
                IN_SOURCE_CODE=>odrow.source_code,
                IN_SHIP_METHOD=>odrow.ship_method,
                IN_PRODUCT_AMOUNT=>in_refund_product_amount,
                IN_ADD_ON_AMOUNT=>in_refund_addon_amount,
                IN_SHIPPING_FEE=>in_refund_shipping_fee,
                IN_SERVICE_FEE=>in_refund_service_fee,
                IN_DISCOUNT_AMOUNT=>in_refund_discount_amount,
                IN_SHIPPING_TAX=>in_refund_shipping_tax,
                IN_SERVICE_FEE_TAX=>in_refund_service_fee_tax,
                IN_TAX=>in_refund_tax,
                IN_PAYMENT_TYPE=>null,
                IN_REFUND_DISP_CODE=>in_refund_disp_code,
                IN_COMMISSION_AMOUNT=>in_refund_commission_amount,
                IN_WHOLESALE_AMOUNT=>in_refund_wholesale_amount,
                IN_ADMIN_FEE=>in_refund_admin_fee,
                IN_NEW_ORDER_SEQ=>'N',
                IN_REFUND_ID=>out_refund_id,
                IN_ORDER_BILL_ID=>null,
                IN_ADD_ON_DISCOUNT_AMOUNT=>IN_REFUND_ADDON_DISCOUNT_AMT,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
ELSE
        out_status := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_REFUND_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_REFUND_UPDATE;


PROCEDURE INSERT_PAYMENTS_UPDATE
(
IN_ORDER_GUID                   IN PAYMENTS_UPDATE.ORDER_GUID%TYPE,
IN_ADDITIONAL_BILL_ID           IN PAYMENTS_UPDATE.ADDITIONAL_BILL_ID%TYPE,
IN_PAYMENT_TYPE                 IN PAYMENTS_UPDATE.PAYMENT_TYPE%TYPE,
IN_CC_ID                        IN PAYMENTS_UPDATE.CC_ID%TYPE,
IN_UPDATED_BY                   IN PAYMENTS_UPDATE.UPDATED_BY%TYPE,
IN_AUTH_RESULT                  IN PAYMENTS_UPDATE.AUTH_RESULT%TYPE,
IN_AUTH_NUMBER                  IN PAYMENTS_UPDATE.AUTH_NUMBER%TYPE,
IN_AVS_CODE                     IN PAYMENTS_UPDATE.AVS_CODE%TYPE,
IN_ACQ_REFERENCE_NUMBER         IN PAYMENTS_UPDATE.ACQ_REFERENCE_NUMBER%TYPE,
IN_GC_COUPON_NUMBER             IN PAYMENTS_UPDATE.GC_COUPON_NUMBER%TYPE,
IN_AUTH_OVERRIDE_FLAG           IN PAYMENTS_UPDATE.AUTH_OVERRIDE_FLAG%TYPE,
IN_CREDIT_AMOUNT                IN PAYMENTS_UPDATE.CREDIT_AMOUNT%TYPE,
IN_DEBIT_AMOUNT                 IN PAYMENTS_UPDATE.DEBIT_AMOUNT%TYPE,
IN_PAYMENT_INDICATOR            IN PAYMENTS_UPDATE.PAYMENT_INDICATOR%TYPE,
IN_REFUND_ID                    IN PAYMENTS_UPDATE.REFUND_ID%TYPE,
IN_AUTH_DATE                    IN PAYMENTS_UPDATE.AUTH_DATE%TYPE,
IN_AAFES_TICKET_NUMBER          IN PAYMENTS_UPDATE.AAFES_TICKET_NUMBER%TYPE,
IN_PAYMENT_TRANSACTION_ID       IN PAYMENTS_UPDATE.PAYMENT_TRANSACTION_ID%TYPE,
IN_NC_APPROVAL_ID               IN PAYMENTS_UPDATE.NC_APPROVAL_IDENTITY_ID%TYPE,
IN_WALLET_INDICATOR             IN PAYMENTS.WALLET_INDICATOR%TYPE,
OUT_PAYMENT_ID                  OUT PAYMENTS_UPDATE.PAYMENT_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting payment information

Input:
        order_guid                      varchar2
        additional_bill_id              number
        payment_type                    varchar2
        cc_id                           number
        updated_by                      varchar2
        auth_result                     varchar2
        auth_number                     varchar2
        avs_code                        varchar2
        acq_reference_number            varchar2
        gc_coupon_number                varchar2
        auth_override_flag              char
        credit_amount                   number
        debit_amount                    number
        payment_indicator               char
        refund_id                       number
        auth_date                       date
        aafes_ticket_number             varchar2

Output:
        payment_id                      number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_auth_char_indicator   payments.auth_char_indicator%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'a')+1, instr(in_acq_reference_number||'abcdef', 'b')-instr(in_acq_reference_number||'abcdef', 'a')-1);
v_transaction_id        payments.transaction_id%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'b')+1, instr(in_acq_reference_number||'abcdef', 'c')-instr(in_acq_reference_number||'abcdef', 'b')-1);
v_validation_code       payments.validation_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'c')+1, instr(in_acq_reference_number||'abcdef', 'd')-instr(in_acq_reference_number||'abcdef', 'c')-1);
v_auth_source_code      payments.auth_source_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'd')+1, instr(in_acq_reference_number||'abcdef', 'e')-instr(in_acq_reference_number||'abcdef', 'd')-1);
v_response_code         payments.response_code%type := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'e')+1, instr(in_acq_reference_number||'abcdef', 'f')-instr(in_acq_reference_number||'abcdef', 'e')-1);
/* The 'f' portion of the data, which is a product identifier, is only used for the credit card settlement file and is parsed out when the settlement file is created.  Parse code was left for reference.  v_product_identifier            varchar2(3) := substr (in_acq_reference_number, instr(in_acq_reference_number||'abcdef', 'f')+1); */

BEGIN

out_payment_id := key.keygen ('PAYMENTS');

INSERT INTO payments_update
(
        payment_id,
        order_guid,
        additional_bill_id,
        payment_type,
        cc_id,
        updated_on,
        updated_by,
        auth_result,
        auth_number,
        avs_code,
        acq_reference_number,
        gc_coupon_number,
        auth_override_flag,
        credit_amount,
        debit_amount,
        payment_indicator,
        refund_id,
        auth_date,
        auth_char_indicator,
        transaction_id,
        validation_code,
        auth_source_code,
        response_code,
        aafes_ticket_number,
        payment_transaction_id,
        nc_approval_identity_id,
        wallet_indicator
)
VALUES
(
        out_payment_id,
        in_order_guid,
        in_additional_bill_id,
        in_payment_type,
        in_cc_id,
        sysdate,
        in_updated_by,
        in_auth_result,
        in_auth_number,
        in_avs_code,
        in_acq_reference_number,
        in_gc_coupon_number,
        in_auth_override_flag,
        in_credit_amount,
        in_debit_amount,
        in_payment_indicator,
        in_refund_id,
        in_auth_date,
        v_auth_char_indicator,
        v_transaction_id,
        v_validation_code,
        v_auth_source_code,
        v_response_code,
        in_aafes_ticket_number,
        in_payment_transaction_id,
        in_nc_approval_id,
        in_wallet_indicator
);

IF in_additional_bill_id IS NOT NULL THEN
        -- this is an additional bill payment at the order level,
        -- update the accounting transaction record that is associated to the specific order bill
        update  accounting_transactions_upd at
        set     at.payment_type = in_payment_type
        where   at.order_bill_id = in_additional_bill_id;
END IF;

IF in_refund_id IS NOT NULL THEN
        -- this is a refund payment at the order level,
        -- update the accounting transaction record that is associated to the specific refund
        update  accounting_transactions_upd at
        set     at.payment_type = in_payment_type
        where   at.refund_id = in_refund_id;
END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_PAYMENTS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_PAYMENTS_UPDATE;


PROCEDURE INSERT_CREDIT_CARDS_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_CC_TYPE                      IN CREDIT_CARDS_UPDATE.CC_TYPE%TYPE,
IN_CC_NUMBER                    IN CREDIT_CARDS_UPDATE.CC_NUMBER%TYPE,
IN_CC_EXPIRATION                IN CREDIT_CARDS_UPDATE.CC_EXPIRATION%TYPE,
IN_UPDATED_BY                   IN CREDIT_CARDS_UPDATE.UPDATED_BY%TYPE,
IN_GIFT_CARD_PIN                IN CREDIT_CARDS_UPDATE.GIFT_CARD_PIN%TYPE,
OUT_CC_ID                       OUT CREDIT_CARDS.CC_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting credit card
        information based on the customer id, card type, and card number

Input:
        cc_type                         varchar2
        cc_number                       varchar2 (cc number comes in plain text)
        cc_expiration                   varchar2
        name                            varchar2
        address_line_1                  varchar2
        address_line_2                  varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        updated_by                      varchar2
        customer_id                     number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- get the buyer information for the order
CURSOR cust_cur IS
        SELECT  c.first_name || ' ' || c.last_name name,
                c.address_1 address_line_1,
                c.address_2 address_line_2,
                c.city,
                c.state,
                c.zip_code,
                c.country,
                c.customer_id
        FROM    customer c
        WHERE   EXISTS
        (
                select  1
                from    orders o
                join    order_detail_update odu
                on      o.order_guid = odu.order_guid
                where   odu.order_detail_id = in_order_detail_id
                and     o.customer_id = c.customer_id
        );

cust_row        cust_cur%rowtype;

-- check the real credit card table for the given credit card number
CURSOR exists_cur (p_customer_id integer) IS
        SELECT  cc_id
        FROM    credit_cards
        WHERE   customer_id = p_customer_id
        AND     cc_type = in_cc_type
   --     AND     cc_number = global.encryption.encrypt_it (in_cc_number,key_name);
        AND     nvl(cc_number,1) = decode(ftd_apps.payment_method_is_cc(in_cc_type),'Y',global.encryption.encrypt_it(in_cc_number,key_name),1);


v_key_name credit_cards_update.key_name%type;

BEGIN

OPEN cust_cur;
FETCH cust_cur INTO cust_row;
CLOSE cust_cur;

IF cust_row.customer_id IS NOT NULL THEN

        OPEN exists_cur (cust_row.customer_id);
        FETCH exists_cur INTO out_cc_id;
        CLOSE exists_cur;

        IF out_cc_id IS NULL THEN
                out_cc_id := key.keygen ('CREDIT_CARDS');
        END IF;

        v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

        INSERT INTO credit_cards_update
        (
                cc_id,
                cc_type,
                cc_number,
                gift_card_pin,
                key_name,
                cc_expiration,
                name,
                address_line_1,
                address_line_2,
                city,
                state,
                zip_code,
                country,
                updated_on,
                updated_by,
                customer_id,
                order_detail_id
        )
        VALUES
        (
                out_cc_id,
                in_cc_type,
 --               global.encryption.encrypt_it (in_cc_number,v_key_name),
                decode(ftd_apps.payment_method_is_cc(in_cc_type),'Y',global.encryption.encrypt_it(in_cc_number,v_key_name),null),
                decode(ftd_apps.payment_method_is_cc(in_cc_type),'Y',global.encryption.encrypt_it(in_gift_card_pin,v_key_name),null),
                v_key_name,
                in_cc_expiration,
                cust_row.name,
                cust_row.address_line_1,
                cust_row.address_line_2,
                cust_row.city,
                cust_row.state,
                cust_row.zip_code,
                cust_row.country,
                sysdate,
                in_updated_by,
                cust_row.customer_id,
                in_order_detail_id
        );

        OUT_STATUS := 'Y';

ELSE
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'WARNING: CUSTOMER INFORMATION NOT FOUND';
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_CREDIT_CARDS_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CREDIT_CARDS_UPDATE;

PROCEDURE INSERT_ACCOUNTING_TRANS_UPD
(
IN_TRANSACTION_TYPE             IN ACCOUNTING_TRANSACTIONS_UPD.TRANSACTION_TYPE%TYPE,
IN_TRANSACTION_DATE             IN ACCOUNTING_TRANSACTIONS_UPD.TRANSACTION_DATE%TYPE,
IN_ORDER_DETAIL_ID              IN ACCOUNTING_TRANSACTIONS_UPD.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ACCOUNTING_TRANSACTIONS_UPD.DELIVERY_DATE%TYPE,
IN_PRODUCT_ID                   IN ACCOUNTING_TRANSACTIONS_UPD.PRODUCT_ID%TYPE,
IN_SOURCE_CODE                  IN ACCOUNTING_TRANSACTIONS_UPD.SOURCE_CODE%TYPE,
IN_SHIP_METHOD                  IN ACCOUNTING_TRANSACTIONS_UPD.SHIP_METHOD%TYPE,
IN_PRODUCT_AMOUNT               IN ACCOUNTING_TRANSACTIONS_UPD.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ACCOUNTING_TRANSACTIONS_UPD.ADD_ON_AMOUNT%TYPE,
IN_SHIPPING_FEE                 IN ACCOUNTING_TRANSACTIONS_UPD.SHIPPING_FEE%TYPE,
IN_SERVICE_FEE                  IN ACCOUNTING_TRANSACTIONS_UPD.SERVICE_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ACCOUNTING_TRANSACTIONS_UPD.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ACCOUNTING_TRANSACTIONS_UPD.SHIPPING_TAX%TYPE,
IN_SERVICE_FEE_TAX              IN ACCOUNTING_TRANSACTIONS_UPD.SERVICE_FEE_TAX%TYPE,
IN_TAX                          IN ACCOUNTING_TRANSACTIONS_UPD.TAX%TYPE,
IN_PAYMENT_TYPE                 IN ACCOUNTING_TRANSACTIONS_UPD.PAYMENT_TYPE%TYPE,
IN_REFUND_DISP_CODE             IN ACCOUNTING_TRANSACTIONS_UPD.REFUND_DISP_CODE%TYPE,
IN_COMMISSION_AMOUNT            IN ACCOUNTING_TRANSACTIONS_UPD.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ACCOUNTING_TRANSACTIONS_UPD.WHOLESALE_AMOUNT%TYPE,
IN_ADMIN_FEE                    IN ACCOUNTING_TRANSACTIONS_UPD.ADMIN_FEE%TYPE,
IN_REFUND_ID                    IN ACCOUNTING_TRANSACTIONS_UPD.REFUND_ID%TYPE,
IN_ORDER_BILL_ID                IN ACCOUNTING_TRANSACTIONS_UPD.ORDER_BILL_ID%TYPE,
IN_NEW_ORDER_SEQ                IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS

BEGIN

INSERT_ACCOUNTING_TRANS_UPD
(
IN_TRANSACTION_TYPE,
IN_TRANSACTION_DATE,
IN_ORDER_DETAIL_ID,
IN_DELIVERY_DATE,
IN_PRODUCT_ID,
IN_SOURCE_CODE,
IN_SHIP_METHOD,
IN_PRODUCT_AMOUNT,
IN_ADD_ON_AMOUNT,
IN_SHIPPING_FEE,
IN_SERVICE_FEE,
IN_DISCOUNT_AMOUNT,
IN_SHIPPING_TAX,
IN_SERVICE_FEE_TAX,
IN_TAX,
IN_PAYMENT_TYPE,
IN_REFUND_DISP_CODE,
IN_COMMISSION_AMOUNT,
IN_WHOLESALE_AMOUNT,
IN_ADMIN_FEE,
IN_REFUND_ID,
IN_ORDER_BILL_ID,
IN_NEW_ORDER_SEQ,
0,
OUT_STATUS,
OUT_MESSAGE
);

END INSERT_ACCOUNTING_TRANS_UPD;

PROCEDURE INSERT_ACCOUNTING_TRANS_UPD
(
IN_TRANSACTION_TYPE             IN ACCOUNTING_TRANSACTIONS_UPD.TRANSACTION_TYPE%TYPE,
IN_TRANSACTION_DATE             IN ACCOUNTING_TRANSACTIONS_UPD.TRANSACTION_DATE%TYPE,
IN_ORDER_DETAIL_ID              IN ACCOUNTING_TRANSACTIONS_UPD.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ACCOUNTING_TRANSACTIONS_UPD.DELIVERY_DATE%TYPE,
IN_PRODUCT_ID                   IN ACCOUNTING_TRANSACTIONS_UPD.PRODUCT_ID%TYPE,
IN_SOURCE_CODE                  IN ACCOUNTING_TRANSACTIONS_UPD.SOURCE_CODE%TYPE,
IN_SHIP_METHOD                  IN ACCOUNTING_TRANSACTIONS_UPD.SHIP_METHOD%TYPE,
IN_PRODUCT_AMOUNT               IN ACCOUNTING_TRANSACTIONS_UPD.PRODUCT_AMOUNT%TYPE,
IN_ADD_ON_AMOUNT                IN ACCOUNTING_TRANSACTIONS_UPD.ADD_ON_AMOUNT%TYPE,
IN_SHIPPING_FEE                 IN ACCOUNTING_TRANSACTIONS_UPD.SHIPPING_FEE%TYPE,
IN_SERVICE_FEE                  IN ACCOUNTING_TRANSACTIONS_UPD.SERVICE_FEE%TYPE,
IN_DISCOUNT_AMOUNT              IN ACCOUNTING_TRANSACTIONS_UPD.DISCOUNT_AMOUNT%TYPE,
IN_SHIPPING_TAX                 IN ACCOUNTING_TRANSACTIONS_UPD.SHIPPING_TAX%TYPE,
IN_SERVICE_FEE_TAX              IN ACCOUNTING_TRANSACTIONS_UPD.SERVICE_FEE_TAX%TYPE,
IN_TAX                          IN ACCOUNTING_TRANSACTIONS_UPD.TAX%TYPE,
IN_PAYMENT_TYPE                 IN ACCOUNTING_TRANSACTIONS_UPD.PAYMENT_TYPE%TYPE,
IN_REFUND_DISP_CODE             IN ACCOUNTING_TRANSACTIONS_UPD.REFUND_DISP_CODE%TYPE,
IN_COMMISSION_AMOUNT            IN ACCOUNTING_TRANSACTIONS_UPD.COMMISSION_AMOUNT%TYPE,
IN_WHOLESALE_AMOUNT             IN ACCOUNTING_TRANSACTIONS_UPD.WHOLESALE_AMOUNT%TYPE,
IN_ADMIN_FEE                    IN ACCOUNTING_TRANSACTIONS_UPD.ADMIN_FEE%TYPE,
IN_REFUND_ID                    IN ACCOUNTING_TRANSACTIONS_UPD.REFUND_ID%TYPE,
IN_ORDER_BILL_ID                IN ACCOUNTING_TRANSACTIONS_UPD.ORDER_BILL_ID%TYPE,
IN_NEW_ORDER_SEQ                IN VARCHAR2,
IN_ADD_ON_DISCOUNT_AMOUNT       IN ACCOUNTING_TRANSACTIONS_UPD.ADD_ON_DISCOUNT_AMOUNT%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting records into the
        accouting transaction table

Input:
        transaction_type                varchar2
        transaction_date                date
        order_detail_id                 number
        delivery_date                   date
        product_id                      varchar2
        source_code                     varchar2
        ship_method                     varchar2
        product_amount                  number
        add_on_amount                   number
        shipping_fee                    number
        service_fee                     number
        discount_amount                 number
        shipping_tax                    number
        service_fee_tax                 number
        tax                             number
        payment_type                    varchar2
        refund_disp_code                varchar2
        commission_amount               number
        wholesale_amount                number
        admin_fee                       number
        refund_id                       number
        order_bill_id                   number
        new_order_seq                   varchar2 (Y/N - if a new order sequence should be created for
                                                  the given order detail)

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR seq_cur IS
        SELECT  max (additional_order_seq)
        FROM    accounting_transactions_upd
        WHERE   order_detail_id = in_order_detail_id;

v_additional_order_seq          accounting_transactions_upd.additional_order_seq%type;
v_refund_type                   accounting_transactions_upd.transaction_type%type := 'Refund';

BEGIN

-- get the current additional order seq value
OPEN seq_cur;
FETCH seq_cur INTO v_additional_order_seq;
CLOSE seq_cur;

IF v_additional_order_seq IS NULL THEN
        -- if no accouting transactions records exist for the order
        -- set the order seq to 1
        v_additional_order_seq := 1;

ELSIF in_new_order_seq = 'Y' THEN
        -- if a new order seq is requested, increment the existing order seq
        v_additional_order_seq := v_additional_order_seq + 1;
END IF;

-- store refund transactions with negative values
INSERT INTO accounting_transactions_upd
(
        accounting_transaction_id,
        transaction_type,
        transaction_date,
        order_detail_id,
        additional_order_seq,
        delivery_date,
        product_id,
        source_code,
        ship_method,
        product_amount,
        add_on_amount,
        shipping_fee,
        service_fee,
        discount_amount,
        shipping_tax,
        service_fee_tax,
        tax,
        payment_type,
        refund_disp_code,
        commission_amount,
        wholesale_amount,
        admin_fee,
        refund_id,
        order_bill_id,
        add_on_discount_amount
)
VALUES
(
        accounting_transaction_id_sq.nextval,
        in_transaction_type,
        in_transaction_date,
        in_order_detail_id,
        v_additional_order_seq,
        in_delivery_date,
        in_product_id,
        in_source_code,
        in_ship_method,
        nvl (decode (in_transaction_type, v_refund_type, in_product_amount * -1, in_product_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_add_on_amount * -1, in_add_on_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_shipping_fee * -1, in_shipping_fee), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_service_fee * -1, in_service_fee), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_discount_amount * -1, in_discount_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_shipping_tax * -1, in_shipping_tax), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_service_fee_tax * -1, in_service_fee_tax), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_tax * -1, in_tax), 0),
        in_payment_type,
        in_refund_disp_code,
        nvl (decode (in_transaction_type, v_refund_type, in_commission_amount * -1, in_commission_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_wholesale_amount * -1, in_wholesale_amount), 0),
        nvl (decode (in_transaction_type, v_refund_type, in_admin_fee * -1, in_admin_fee), 0),
        in_refund_id,
        in_order_bill_id,
        nvl (decode (in_transaction_type, v_refund_type, in_add_on_discount_amount * -1, in_add_on_discount_amount), 0)
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_ACCOUNTING_TRANS_UPD [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_ACCOUNTING_TRANS_UPD;


PROCEDURE UPDATE_MILES_POINTS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_MILES_POINTS                 IN ORDER_DETAIL_UPDATE.MILES_POINTS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the miles/points reward
        for the order

Input:
        order_detail_id                 number
        updated_by                      varchar2
        miles_points                    number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_detail_update
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        miles_points = in_miles_points
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_MILES_POINTS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_MILES_POINTS;


PROCEDURE UPDATE_FLORIST
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAIL_UPDATE.FLORIST_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the florist
        for the order

Input:
        order_detail_id                 number
        updated_by                      varchar2
        miles_points                    number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

UPDATE  order_detail_update
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        florist_id = in_florist_id
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED UPDATE_FLORIST [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_FLORIST;


PROCEDURE DELETE_ORDER_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the update records in the
        work table.

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

DELETE FROM comments_update cu
WHERE   cu.order_detail_id = in_order_detail_id;

DELETE FROM order_contact_update ocu
WHERE   ocu.order_detail_id = in_order_detail_id;

DELETE FROM order_add_ons_update
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM accounting_transactions_upd
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM credit_cards_update ccu
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM payments_update pu
WHERE  pu.order_guid = (select cod.order_guid
                       from clean.order_details cod
                      where cod.order_detail_id = in_order_detail_id);

DELETE FROM order_bills_update obu
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM refund_update ru
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM order_detail_update
WHERE   order_detail_id = in_order_detail_id;

DELETE FROM clean.co_brand_update ccbu
WHERE   ccbu.order_guid = (select cod.order_guid
                       from clean.order_details cod
                      where cod.order_detail_id = in_order_detail_id);

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED DELETE_ORDER_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END DELETE_ORDER_UPDATE;


PROCEDURE VIEW_ORIGINAL_ORDER_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the original data
        used to populate the work tables for the order.

Input:
        order_detail_id                 number

Output:
        cursor containing order info

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                od.order_detail_id,
                od.order_guid,
                od.updated_on,
                od.updated_by,
                od.source_code,
                od.delivery_date,
                od.delivery_date_range_end,
                od.product_id,
                od.quantity,
                od.color_1,
                od.color_2,
                od.substitution_indicator,
                od.occasion,
                od.card_message,
                od.card_signature,
                od.special_instructions,
                od.release_info_indicator,
                od.florist_id,
                od.ship_method,
                od.ship_date,
                od.second_choice_product,
                od.size_indicator,
                m.membership_number,
                m.first_name membership_first_name,
                m.last_name membership_last_name,
                r.customer_id recipient_id,
                coalesce (cpd.phone_number, cpe.phone_number) recipient_phone_number,
                decode (cpd.phone_number, null, cpe.extension, cpd.extension) recipient_extension,
                r.first_name recipient_first_name,
                r.last_name recipient_last_name,
                r.address_1 recipient_address_1,
                r.address_2 recipient_address_2,
                r.city recipient_city,
                r.state recipient_state,
                r.zip_code recipient_zip_code,
                r.country recipient_country,
                r.address_type recipient_address_type,
                r.business_name recipient_business_name,
                r.business_info recipient_business_info,
                ob.product_amount - nvl (r.refund_product_amount, 0) product_amount,
                ob.add_on_amount - nvl (r.refund_add_on_amount, 0) add_on_amount,
                ob.service_fee - nvl (r.refund_service_fee, 0) service_fee,
                ob.shipping_fee - nvl (r.refund_shipping_fee, 0) shipping_fee,
                ob.discount_amount - nvl (r.refund_discount_amount, 0) discount_amount,
                ob.shipping_tax - nvl (r.refund_shipping_tax, 0) shipping_tax,
                ob.tax - nvl (r.refund_tax, 0) tax,
                od.eod_delivery_indicator,
                od.eod_delivery_date,
                od.subcode,
                od.miles_points,
                decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                o.origin_id,
                o.company_id,
                oci.first_name alt_contact_first_name,
                oci.last_name alt_contact_last_name,
                oci.phone_number alt_contact_phone_number,
                oci.extension alt_contact_extension,
                oci.order_contact_info_id alt_contact_info_id,
                oci.email_address alt_contact_email,
                order_mesg_pkg.get_last_ftd_for_order (od.order_detail_id) message_order_number,
                ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0) service_fee_tax,
                order_mesg_pkg.is_order_ftdm (od.order_detail_id) message_ftdm_indicator,
                c.customer_id,
                c.first_name customer_first_name,
                c.last_name customer_last_name,
                occ.description occasion_description,
                occ.index_id,
		od.actual_product_amount,
		od.actual_add_on_amount,
		od.actual_shipping_fee,
		od.actual_service_fee,
		od.actual_discount_amount,
		od.actual_shipping_tax,
		od.actual_tax,
		od.personal_greeting_id,
		o.buyer_signed_in_flag,
    od.pc_membership_id,
    od.pc_flag,
    od.pc_group_id,
    od.actual_add_on_discount_amount
        FROM    order_details od
        JOIN    customer r
        ON      od.recipient_id = r.customer_id
        JOIN    ftd_apps.occasion occ
        ON      od.occasion = occ.occasion_id
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        JOIN    customer c
        ON      o.customer_id = c.customer_id
        JOIN
        (
                select  ob.order_detail_id,
                        sum (ob.product_amount) product_amount,
                        sum (ob.add_on_amount) add_on_amount,
                        sum (ob.service_fee) service_fee,
                        sum (ob.shipping_fee) shipping_fee,
                        sum (ob.discount_amount) discount_amount,
                        sum (ob.shipping_tax) shipping_tax,
                        sum (ob.service_fee_tax) service_fee_tax,
                        sum (ob.tax) tax,
                        sum (ob.add_on_discount_amount) add_on_discount_amount
                from    order_bills ob
                where   ob.order_detail_id = in_order_detail_id
                group by ob.order_detail_id
        ) ob
        ON      od.order_detail_id = ob.order_detail_id
        LEFT OUTER JOIN
        (
                select  r.order_detail_id,
                        sum (r.refund_product_amount) refund_product_amount,
                        sum (r.refund_addon_amount) refund_add_on_amount,
                        sum (r.refund_service_fee) refund_service_fee,
                        sum (r.refund_shipping_fee) refund_shipping_fee,
                        sum (r.refund_discount_amount) refund_discount_amount,
                        sum (r.refund_shipping_tax) refund_shipping_tax,
                        sum (r.refund_service_fee_tax) refund_service_fee_tax,
                        sum (r.refund_tax) refund_tax,
                        sum (r.refund_addon_discount_amt) refund_addon_discount_amount
                from    refund r
                where   r.order_detail_id = in_order_detail_id
                group by r.order_detail_id
        ) r
        ON      od.order_detail_id = r.order_detail_id
        LEFT OUTER JOIN memberships m
        ON      o.membership_id = m.membership_id
        LEFT OUTER JOIN    customer_phones cpd
        ON      r.customer_id = cpd.customer_id
        AND     cpd.phone_type = 'Day'
        LEFT OUTER JOIN    customer_phones cpe
        ON      r.customer_id = cpe.customer_id
        AND     cpe.phone_type = 'Evening'
        LEFT OUTER JOIN order_contact_info oci
        ON      od.order_guid = oci.order_guid
        WHERE   od.order_detail_id = in_order_detail_id;

END VIEW_ORIGINAL_ORDER_INFO;


PROCEDURE VIEW_ORIGINAL_PRODUCT_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_PRODUCT                     OUT TYPES.REF_CURSOR,
OUT_ADD_ONS                     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the working updated order
        data from the order detail update table based for the update
        delivery information screen

Input:
        order_detail_id                 number

Output:
        cursor containing delivery info from order_detail_update

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_PRODUCT FOR
        SELECT
                c.first_name customer_first_name,
                c.last_name customer_last_name,
                od.external_order_number,
                od.product_id,
                p.product_name,
                p.short_description,
                c1.description color1_description,
                c2.description color2_description,
                od.quantity,
                od.color_1,
                od.color_2,
                null second_choice_product,
                ob.product_amount - nvl (r.refund_product_amount, 0) product_amount,
                ob.discount_amount - nvl (r.refund_discount_amount, 0) discount_amount,
                ob.tax - nvl (r.refund_tax, 0) tax,
                od.ship_method,
                od.size_indicator,
                od.special_instructions,
                od.substitution_indicator,
                od.ship_date,
                od.delivery_date,
                od.delivery_date_range_end,
                c.customer_id,
                od.miles_points,
                decode (od.miles_points_post_date, null, 'N', 'Y') miles_points_posted,
                ftd_apps.source_query_pkg.get_source_reward_type (od.source_code) discount_reward_type,
                decode (od.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                ob.add_on_amount - nvl (r.refund_add_on_amount, 0) add_on_amount,
                order_mesg_pkg.get_last_florist_for_order (od.order_detail_id) filling_florist,
                od.actual_product_amount,
                od.actual_add_on_amount,
                od.actual_shipping_fee,
                od.actual_service_fee,
                od.actual_discount_amount,
                od.actual_shipping_tax,
                od.actual_tax,
                sh.description ship_method_desc,
                od.personal_greeting_id,
                od.actual_add_on_discount_amount
        FROM    order_details od
        JOIN    orders o
        ON      od.order_guid = o.order_guid
        LEFT OUTER JOIN    ftd_apps.ship_methods sh
        ON      od.ship_method = sh.ship_method_id
        JOIN    customer c
        ON      o.customer_id = c.customer_id
        JOIN
        (
                select  ob.order_detail_id,
                        sum (ob.product_amount) product_amount,
                        sum (ob.discount_amount) discount_amount,
                        sum (ob.shipping_tax) + sum (ob.tax) tax,
                        sum (ob.add_on_amount) add_on_amount,
                        sum (ob.add_on_discount_amount) add_on_discount_amount
                from    order_bills ob
                where   ob.order_detail_id = in_order_detail_id
                group by ob.order_detail_id
        ) ob
        ON      od.order_detail_id = ob.order_detail_id
        LEFT OUTER JOIN
        (
                select  r.order_detail_id,
                        sum (r.refund_product_amount) refund_product_amount,
                        sum (r.refund_discount_amount) refund_discount_amount,
                        sum (r.refund_tax) refund_tax,
                        sum (r.refund_addon_amount) refund_add_on_amount,
                        sum (r.refund_addon_discount_amt) refund_addon_discount_amount
                from    refund r
                where   r.order_detail_id = in_order_detail_id
                group by r.order_detail_id
        ) r
        ON      od.order_detail_id = r.order_detail_id
        LEFT OUTER JOIN    ftd_apps.product_master p
        ON      od.product_id = p.product_id
        LEFT OUTER JOIN    ftd_apps.color_master c1
        ON      od.color_1 = c1.color_master_id
        LEFT OUTER JOIN    ftd_apps.color_master c2
        ON      od.color_2 = c2.color_master_id
        LEFT OUTER JOIN    ftd_apps.source_master sm
        ON      od.source_code = sm.source_code
        WHERE   od.order_detail_id = in_order_detail_id;

OPEN OUT_ADD_ONS FOR
        SELECT
                nvl (fa.description, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'DESCRIPTION')) description,
                a.add_on_quantity,
                nvl (fa.price, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'PRICE')) price,
                a.add_on_code,
                nvl (at.description, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'ADDON_TYPE_DESCRIPTION')) add_on_type_description,
                a.order_add_on_id,
                fa.status add_on_status
        FROM    order_add_ons a
        LEFT OUTER JOIN ftd_apps.addon_history fa
        ON      a.add_on_history_id = fa.addon_history_id
        LEFT OUTER JOIN ftd_apps.addon_type at
        ON      fa.addon_type = at.addon_type_id
        WHERE   a.order_detail_id = in_order_detail_id
        ORDER BY fa.description;

END VIEW_ORIGINAL_PRODUCT_INFO;


PROCEDURE VIEW_DELIVERY_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the working updated order
        data from the order detail update table based for the update
        delivery information screen

Input:
        order_detail_id                 number

Output:
        cursor containing delivery info from order_detail_update

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                odu.order_detail_id,
                odu.order_guid,
                odu.updated_on,
                odu.updated_by,
                odu.source_code,
                odu.delivery_date,
                odu.delivery_date_range_end,
                odu.card_message,
                odu.card_signature,
                odu.special_instructions,
                odu.ship_method,
                odu.ship_date,
                odu.membership_number,
                odu.membership_first_name,
                odu.membership_last_name,
                odu.recipient_id,
                odu.recipient_phone_number,
                odu.recipient_extension,
                odu.recipient_first_name,
                odu.recipient_last_name,
                odu.recipient_address_1,
                odu.recipient_address_2,
                odu.recipient_city,
                odu.recipient_state,
                odu.recipient_zip_code,
                odu.recipient_country,
                odu.recipient_address_type,
                ocu.first_name alt_contact_first_name,
                ocu.last_name alt_contact_last_name,
                ocu.phone_number alt_contact_phone_number,
                ocu.extension alt_contact_extension,
                decode (odu.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                nvl ((select distinct 'Y' from ftd_apps.source_program_ref spf where spf.source_code = odu.source_code and spf.start_date <= sysdate), 'N') membership_required,
                sm.description source_code_description,
                odu.recipient_business_name,
                odu.recipient_business_info,
                odu.eod_delivery_indicator,
                odu.eod_delivery_date,
                decode (IS_ORDER_PAST_DAYS (o.order_date, 'MOD_ORDER_SOURCE_CODE_MAX_DAYS'), 'Y', 'N', 'Y') can_change_order_source,    -- inverse the returned value from the function to make outcome match the column name
                o.origin_id,
                odu.florist_id,
                decode ((select vm.vendor_type from ftd_apps.vendor_master vm where vm.vendor_id = pm.vendor_id), 'ESCALATE', 'Y', 'N') venus_vendor_flag,
                IS_ORDER_PAST_DAYS (o.order_date, 'MOD_ORDER_ORDER_MAX_DAYS') order_past_max_days,
                od.miles_points_post_date,
                odu.occasion,
                sm.source_type,
                occ.index_id occasion_category_index,
                odu.size_indicator,
                o.company_id,
                o.master_order_number,
                c.first_name customer_first_name,
                c.last_name customer_last_name,
                c.city customer_city,
                c.state customer_state,
                c.zip_code customer_zip_code,
                c.country customer_country,
                ocu.order_contact_info_id alt_contact_info_id,
                ocu.email_address alt_contact_email,
                c.customer_id,
                sh.description ship_method_desc,
                odu.product_amount,
                odu.add_on_amount,
                odu.service_fee,
                odu.shipping_fee,
                odu.discount_amount,
                odu.shipping_tax,
                odu.tax,
                c1.description color1_description,
                c2.description color2_description,
                occ.description occasion_description,
                null vendor_id,
                odu.release_info_indicator,
                pm.ship_method_carrier,
                pm.ship_method_florist,
		        odu.product_id,
		        pm.product_type,
		        pm.shipping_key,
		        pm.exception_code,
		        pm.exception_start_date,
                pm.exception_end_date,
                oci.first_name alt_contact_first_name,
                oci.last_name alt_contact_last_name,
                oci.phone_number alt_contact_phone_number,
                oci.extension alt_contact_extension,
                oci.order_contact_info_id alt_contact_info_id,
                oci.email_address alt_contact_email,
                pm.no_tax_flag,
                odu.original_order_has_sdu,
                od.pc_flag,
                od.pc_group_id,
                od.pc_membership_id,
                odu.add_on_discount_amount
        FROM    order_detail_update odu
        JOIN    order_details od
        ON      odu.order_detail_id = od.order_detail_id
        JOIN    orders o
        ON      o.order_guid = odu.order_guid
        JOIN    customer c
        ON      o.customer_id = c.customer_id
        JOIN    ftd_apps.source_master sm
        ON      odu.source_code = sm.source_code
        JOIN    ftd_apps.occasion occ
        ON      odu.occasion = occ.occasion_id
        LEFT OUTER JOIN    ftd_apps.ship_methods sh
        ON      odu.ship_method = sh.ship_method_id
        LEFT OUTER JOIN order_contact_update ocu
        ON      odu.order_detail_id = ocu.order_detail_id
        LEFT OUTER JOIN    ftd_apps.color_master c1
        ON      odu.color_1 = c1.color_master_id
        LEFT OUTER JOIN    ftd_apps.color_master c2
        ON      odu.color_2 = c2.color_master_id
        LEFT OUTER JOIN    ftd_apps.product_master pm
        ON      odu.product_id = pm.product_id
        LEFT OUTER JOIN    order_contact_update oci
          on    oci.order_detail_id = odu.order_detail_id
        WHERE   odu.order_detail_id = in_order_detail_id;

END VIEW_DELIVERY_INFO;


PROCEDURE VIEW_PRODUCT_ID
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the working updated order
        data from the order detail update table based for the update
        product_category screen

Input:
        order_detail_id                 number

Output:
        cursor containing product id info from order_detail_update

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                odu.product_id,
                pm.product_name,
                pm.short_description
        FROM    order_detail_update odu
        JOIN    ftd_apps.product_master pm
        ON      odu.product_id = pm.product_id
        WHERE   odu.order_detail_id = in_order_detail_id;

END VIEW_PRODUCT_ID;


PROCEDURE VIEW_PRODUCT_DETAIL
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the working updated order
        data from the order detail update table based for the update
        product detail screen

Input:
        order_detail_id                 number

Output:
        cursor containing product detail info from order_detail_update

-----------------------------------------------------------------------------*/

BEGIN

-- get the product information from the order
OPEN OUT_CUR FOR
        SELECT
                odu.product_id,
                odu.quantity,
                odu.color_1,
                odu.color_2,
                odu.substitution_indicator,
                odu.ship_method,
                odu.ship_date,
                odu.second_choice_product,
                odu.size_indicator,
                odu.product_amount,
                odu.add_on_amount,
                odu.service_fee,
                odu.shipping_fee,
                odu.discount_amount,
                odu.shipping_tax,
                odu.tax,
                odu.delivery_date,
                odu.delivery_date_range_end,
                odu.special_instructions,
                odu.miles_points,
                odu.personal_greeting_id,
                oci.first_name alt_contact_first_name,
                oci.last_name alt_contact_last_name,
                oci.phone_number alt_contact_phone_number,
                oci.extension alt_contact_extension,
                oci.order_contact_info_id alt_contact_info_id,
                oci.email_address alt_contact_email,
                odu.original_order_has_sdu,
                odu.add_on_discount_amount
        FROM    order_detail_update odu
        LEFT OUTER JOIN    order_contact_update oci
          on    oci.order_detail_id = odu.order_detail_id
        WHERE   odu.order_detail_id = in_order_detail_id;

END VIEW_PRODUCT_DETAIL;


PROCEDURE VIEW_UPDATE_ORDER_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the updated data
        from the work tables for the order.

Input:
        order_detail_id                 number

Output:
        cursor containing order info

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                odu.order_detail_id,
                odu.order_guid,
                odu.updated_on,
                odu.updated_by,
                odu.source_code,
                odu.delivery_date,
                odu.delivery_date_range_end,
                odu.product_id,
                odu.quantity,
                odu.color_1,
                odu.color_2,
                odu.substitution_indicator,
                odu.occasion,
                odu.card_message,
                odu.card_signature,
                odu.special_instructions,
                odu.release_info_indicator,
                odu.florist_id,
                odu.ship_method,
                odu.ship_date,
                odu.second_choice_product,
                odu.size_indicator,
                odu.membership_number,
                odu.membership_first_name,
                odu.membership_last_name,
                odu.recipient_id,
                odu.recipient_phone_number,
                odu.recipient_extension,
                odu.recipient_first_name,
                odu.recipient_last_name,
                odu.recipient_address_1,
                odu.recipient_address_2,
                odu.recipient_city,
                odu.recipient_state,
                odu.recipient_zip_code,
                odu.recipient_country,
                odu.recipient_address_type,
                odu.recipient_business_name,
                odu.recipient_business_info,
                odu.product_amount,
                odu.add_on_amount,
                odu.service_fee,
                odu.shipping_fee,
                odu.discount_amount,
                odu.shipping_tax,
                odu.tax,
                odu.eod_delivery_indicator,
                odu.eod_delivery_date,
                odu.subcode,
                odu.miles_points,
                decode (odu.ship_method, null, 'N', 'SD', 'N', 'Y') vendor_flag,
                ocu.first_name alt_contact_first_name,
                ocu.last_name alt_contact_last_name,
                ocu.phone_number alt_contact_phone_number,
                ocu.extension alt_contact_extension,
                ocu.order_contact_info_id alt_contact_info_id,
                ocu.email_address alt_contact_email,
                sm.description ship_method_desc,
                p.product_name,
                p.short_description,
                c1.description color1_description,
                c2.description color2_description,
                o.master_order_number,
                od.miles_points,
                decode (od.miles_points_post_date, null, 'N', 'Y') miles_points_posted,
                ftd_apps.source_query_pkg.get_source_reward_type (odu.source_code) discount_reward_type,
                f.florist_name,
                sc.description source_description,
                odu.actual_product_amount,
                odu.actual_add_on_amount,
                odu.actual_shipping_fee,
                odu.actual_service_fee,
                odu.actual_discount_amount,
                odu.actual_shipping_tax,
                odu.actual_tax,
                oci.first_name alt_contact_first_name,
                oci.last_name alt_contact_last_name,
                oci.phone_number alt_contact_phone_number,
                oci.extension alt_contact_extension,
                oci.order_contact_info_id alt_contact_info_id,
                oci.email_address alt_contact_email,
                p.novator_id,
                odu.personal_greeting_id,
                p.no_tax_flag,
                odu.service_fee_saved,
                odu.shipping_fee_saved,
                odu.original_order_has_sdu,
                odu.add_on_discount_amount,
                odu.actual_add_on_discount_amount
        FROM    order_detail_update odu
        JOIN    orders o
        ON      odu.order_guid = o.order_guid
        JOIN    order_details od
        ON      odu.order_detail_id = od.order_detail_id
        JOIN    ftd_apps.source_master sc
        ON      odu.source_code = sc.source_code
        LEFT OUTER JOIN order_contact_update ocu
        ON      odu.order_detail_id = ocu.order_detail_id
        LEFT OUTER JOIN    ftd_apps.ship_methods sm
        ON      odu.ship_method = sm.ship_method_id
        LEFT OUTER JOIN    ftd_apps.product_master p
        ON      odu.product_id = p.product_id
        LEFT OUTER JOIN    ftd_apps.color_master c1
        ON      odu.color_1 = c1.color_master_id
        LEFT OUTER JOIN    ftd_apps.color_master c2
        ON      odu.color_2 = c2.color_master_id
        LEFT OUTER JOIN    ftd_apps.florist_master f
        ON      odu.florist_id = f.florist_id
        LEFT OUTER JOIN order_contact_update oci
        ON     ( od.order_guid      = oci.order_guid)
        WHERE   odu.order_detail_id = in_order_detail_id;


END VIEW_UPDATE_ORDER_INFO;


PROCEDURE VIEW_UPDATE_ADDON_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the updated data
        from the work add on table for the order.

Input:
        order_detail_id                 number

Output:
        cursor containing order add on info

-----------------------------------------------------------------------------*/

BEGIN

OPEN OUT_CUR FOR
        SELECT
                nvl (fa.description, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'DESCRIPTION')) description,
                a.add_on_quantity,
                nvl (fa.price, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'PRICE')) price,
                a.add_on_code,
                nvl (at.description, frp.misc_pkg.get_global_parm_value ('ADDON_VALUES', 'ADDON_TYPE_DESCRIPTION')) add_on_type_description,
                a.order_add_on_id,
                fa.status add_on_status,
                a.add_on_discount_amount
        FROM    order_add_ons_update a
        LEFT OUTER JOIN ftd_apps.addon_history fa
        ON      a.add_on_history_id = fa.addon_history_id
        LEFT OUTER JOIN ftd_apps.addon_type at
        ON      fa.addon_type = at.addon_type_id
        WHERE   a.order_detail_id = in_order_detail_id
        ORDER BY fa.description;

END VIEW_UPDATE_ADDON_INFO;


PROCEDURE VIEW_ORDER_DETAIL_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_INFO_TYPE                    IN VARCHAR2,
IN_START_UPDATE                 IN VARCHAR2,
IN_UPDATED_BY                   IN ORDER_DETAIL_UPDATE.UPDATED_BY%TYPE,
IN_PRODUCT_ID                   IN ORDER_DETAIL_UPDATE.PRODUCT_ID%TYPE,
OUT_ORIG_PRODUCT                OUT TYPES.REF_CURSOR,
OUT_ORIG_ADD_ONS                OUT TYPES.REF_CURSOR,
OUT_UPD_ORDER_CUR               OUT TYPES.REF_CURSOR,
OUT_UPD_ADDON_CUR               OUT TYPES.REF_CURSOR,
OUT_SHIPPING_COST               OUT TYPES.REF_CURSOR,
OUT_UPD_COMMENTS_CUR            OUT TYPES.REF_CURSOR,
OUT_CO_BRAND_CUR                OUT TYPES.REF_CURSOR,
OUT_PAYMENTS_CUR                OUT TYPES.REF_CURSOR,
OUT_ORDER_EXTENSIONS_CUR        OUT TYPES.REF_CURSOR,
OUT_ORIG_TAXES_CUR              OUT TYPES.REF_CURSOR,
OUT_UPD_TAXES_CUR               OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the working updated order
        data from the order detail update table based on the info type
        specified

Input:
        order_detail_id                 number
        info_type                       varchar - DELIVERY_INFO, PRODUCT_DETAIL, DELIVERY_CONFIRMATION
        start_update                    varchar - Y/N to initially insert the data into the update table
        updated_by                      varchar - csr modifying the order
        product_id                      varchar - optional product id for shipping costs
Output:
        Product information cursor is the original product information for the order.
        Cursor containing order info from order_detail_update

-----------------------------------------------------------------------------*/

v_status        char(1);
v_message       varchar2(2000);

CURSOR order_cur IS
        SELECT  coalesce (in_product_id, odu.product_id) product_id,
                odu.recipient_zip_code
        FROM    order_detail_update odu
        WHERE   odu.order_detail_id = in_order_detail_id;

v_product_id    order_detail_update.product_id%type;
v_zip_code      order_detail_update.recipient_zip_code%type;

BEGIN

-- Copy the data from the real tables to the update tables on the start of modify order.
-- The start update indicator should only be set to Y on the delivery information page
IF in_start_update = 'Y' THEN
        DELETE_ORDER_UPDATE (in_order_detail_id, v_status, v_message);
        INSERT_ORDER_DETAIL_UPDATE (in_order_detail_id, in_updated_by, v_status, v_message);
END IF;

-- Always return the original product information for the page headers
VIEW_ORIGINAL_PRODUCT_INFO (in_order_detail_id, out_orig_product, out_orig_add_ons);

-- Return the shipping cost information for the product on the temp table
OPEN order_cur;
FETCH order_cur INTO v_product_id, v_zip_code;
CLOSE order_cur;

GLOBAL.GLOBAL_PKG.GET_SHIPPING_COST_LIST (v_product_id, v_zip_code, out_shipping_cost);

CASE in_info_type
WHEN 'DELIVERY_INFO' THEN
        -- retrieve the information for the delivery information page
        VIEW_DELIVERY_INFO (in_order_detail_id, out_upd_order_cur);
        OPEN out_upd_addon_cur FOR select 'empty cursor' description from dual;
        OPEN out_upd_taxes_cur FOR select 'empty cursor' description from dual;

WHEN 'PRODUCT_DETAIL' THEN
        -- retrieve the information for the product detail page
        VIEW_PRODUCT_DETAIL (in_order_detail_id, out_upd_order_cur);
        VIEW_UPDATE_ADDON_INFO (in_order_detail_id, out_upd_addon_cur);
        OPEN out_upd_taxes_cur FOR select 'empty cursor' description from dual;

WHEN 'DELIVERY_CONFIRMATION' THEN
        -- retrieve the information for the delivery confirmation page
        VIEW_UPDATE_ORDER_INFO (in_order_detail_id, out_upd_order_cur);
        VIEW_UPDATE_ADDON_INFO (in_order_detail_id, out_upd_addon_cur);
        GET_ORDER_DETAIL_TAXES (in_order_detail_id, out_upd_taxes_cur);

END CASE;

VIEW_COMMENT_UPDATE(IN_ORDER_DETAIL_ID,OUT_UPD_COMMENTS_CUR);
CLEAN.ORDER_QUERY_PKG.GET_ORDER_DETAIL_TAXES (in_order_detail_id, out_orig_taxes_cur);

OPEN OUT_CO_BRAND_CUR  FOR
   SELECT ccbu.ORDER_GUID,
          ccbu.INFO_NAME,
          ccbu.INFO_DATA
   FROM   clean.co_brand_update ccbu
   JOIN   clean.order_detail_update codu on (codu.order_guid = ccbu.order_guid)
   WHERE  codu.order_detail_id = in_order_detail_id;


OPEN OUT_PAYMENTS_CUR  FOR
   SELECT cpu.PAYMENT_ID,
          cpu.ORDER_GUID,
          cpu.UPDATED_ON,
          cpu.UPDATED_BY,
          cpu.USER_AUTH_INDICATOR,
          cpu.PAYMENT_INDICATOR,
          cpu.ADDITIONAL_BILL_ID,
          cpu.PAYMENT_TYPE,
          cpu.CC_ID,
          cpu.AUTH_RESULT,
          cpu.AUTH_NUMBER,
          cpu.AVS_CODE,
          cpu.ACQ_REFERENCE_NUMBER,
          cpu.GC_COUPON_NUMBER,
          cpu.AUTH_OVERRIDE_FLAG,
          cpu.CREDIT_AMOUNT,
          cpu.DEBIT_AMOUNT,
          cpu.REFUND_ID,
          cpu.AUTH_DATE,
          cpu.AUTH_CHAR_INDICATOR,
          cpu.TRANSACTION_ID,
          cpu.VALIDATION_CODE,
          cpu.AUTH_SOURCE_CODE,
          cpu.RESPONSE_CODE,
          cpu.AAFES_TICKET_NUMBER,
          cpu.PAYMENT_TRANSACTION_ID,
          cccu.CC_TYPE,
          decode(cccu.cc_number,null,null,GLOBAL.encryption.DECRYPT_IT(cccu.CC_NUMBER,cccu.key_name)) cc_number,
          decode(cccu.cc_number,null,null,GLOBAL.encryption.DECRYPT_IT(cccu.GIFT_CARD_PIN,cccu.key_name)) gift_card_pin,
          cccu.CC_EXPIRATION,
          cpu.nc_approval_identity_id,
          cpu.wallet_indicator
   FROM   clean.payments_update cpu
   JOIN   clean.order_detail_update codu on (codu.order_guid = cpu.order_guid)
   LEFT OUTER JOIN   clean.credit_cards_update  cccu
   			on (codu.order_detail_id = cccu.order_detail_id)
			and (cpu.payment_type = cccu.cc_type)
   WHERE  codu.order_detail_id = in_order_detail_id;


   OPEN OUT_ORDER_EXTENSIONS_CUR FOR
     SELECT ox.INFO_NAME, ox.INFO_VALUE
     FROM ORDER_EXTENSIONS ox, ORDER_DETAILS od
       WHERE od.ORDER_DETAIL_ID = in_order_detail_id
        AND ox.ORDER_GUID = od.ORDER_GUID;


END VIEW_ORDER_DETAIL_UPDATE;


PROCEDURE VIEW_ADDTNL_ORDER_BILLS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_ORIGINAL_ADD_BILLS          OUT TYPES.REF_CURSOR,
OUT_UPDATE_ADD_BILLS            OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the additional order bills
        record from both the work table and the regualr tables for the order
        detail id passed

Input:
        order detail id

Output:
        cursor containing order bill information from the regular order
            bill table
        cursor containing order bill information from the update order
            bill table

-----------------------------------------------------------------------------*/
BEGIN

-- get any additional bills from the real order bill table
order_query_pkg.get_addtnl_order_bill (to_char(in_order_detail_id), out_original_add_bills);

-- get any additional bills from the work table
OPEN out_update_add_bills FOR
        SELECT  order_bill_id,
                  order_detail_id,
                  product_amount,
                  add_on_amount,
                  service_fee,
                  shipping_fee,
                  discount_amount,
                  shipping_tax,
                  tax,
                  additional_bill_indicator,
                  bill_status,
                  bill_date,
                  service_fee_tax,
                  commission_amount,
                  wholesale_amount,
                  transaction_amount,
                  pdb_price,
                  discount_product_price,
                  discount_type,
                  partner_cost,
                  add_on_discount_amount
        FROM    order_bills_update
        WHERE   order_detail_id = in_order_detail_id
        AND     additional_bill_indicator = 'Y';

END VIEW_ADDTNL_ORDER_BILLS;



PROCEDURE VIEW_ORDER_TOTALS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_CURRENT_TOTAL               OUT TYPES.REF_CURSOR,
OUT_MODIFIED_TOTAL              OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the order totals from
        both the regular order bill table and the modify order work table.
        Totals returned are product amount, add on amount, service/shipping fee,
        tax, discount, and total.

Input:
        order detail id

Output:
        cursor containing order bill/refund net amounts from the regular order
            tables
        cursor containing order bill information from the update table

-----------------------------------------------------------------------------*/
BEGIN

-- get the net amounts of the current order
OPEN out_current_total FOR
        SELECT  ob.product_amount - nvl (r.refund_product_amount, 0) product_amount,
                ob.add_on_amount - nvl (r.refund_add_on_amount, 0) add_on_amount,
                (ob.service_fee - nvl (r.refund_service_fee, 0)) +
                       (ob.shipping_fee - nvl (r.refund_shipping_fee, 0)) serv_ship_fee,
                ob.discount_amount - nvl (r.refund_discount_amount, 0) discount_amount,
                (ob.shipping_tax - nvl (r.refund_shipping_tax, 0)) +
                       (ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0)) +
                       (ob.tax - nvl (r.refund_tax, 0)) tax,
                (ob.product_amount - nvl (r.refund_product_amount, 0)) +
                       (ob.add_on_amount - nvl (r.refund_add_on_amount, 0)) +
                       (ob.service_fee - nvl (r.refund_service_fee, 0)) +
                       (ob.shipping_fee - nvl (r.refund_shipping_fee, 0)) -
                       (ob.discount_amount - nvl (r.refund_discount_amount, 0)) +
                       (ob.shipping_tax - nvl (r.refund_shipping_tax, 0)) +
                       (ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0)) +
                       (ob.tax - nvl (r.refund_tax, 0)) order_total,
                ob.service_fee - nvl (r.refund_service_fee, 0) service_fee,
                ob.shipping_fee - nvl (r.refund_shipping_fee, 0) shipping_fee,
                ob.shipping_tax - nvl (r.refund_shipping_tax, 0) shipping_tax,
                ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0) service_fee_tax,
                ob.tax - nvl (r.refund_tax, 0) product_tax,
                ob.add_on_discount_amount - nvl (r.refund_addon_discount_amount, 0) add_on_discount_amount
        FROM
        (
                select  ob.order_detail_id,
                        sum (ob.product_amount) product_amount,
                        sum (ob.add_on_amount) add_on_amount,
                        sum (ob.service_fee) service_fee,
                        sum (ob.shipping_fee) shipping_fee,
                        sum (ob.discount_amount) discount_amount,
                        sum (ob.shipping_tax) shipping_tax,
                        sum (ob.service_fee_tax) service_fee_tax,
                        sum (ob.tax) tax,
                        sum (ob.add_on_discount_amount) add_on_discount_amount
                from    order_bills ob
                where   ob.order_detail_id = in_order_detail_id
                group by ob.order_detail_id
        ) ob
        LEFT OUTER JOIN
        (
                select  r.order_detail_id,
                        sum (r.refund_product_amount) refund_product_amount,
                        sum (r.refund_addon_amount) refund_add_on_amount,
                        sum (r.refund_service_fee) refund_service_fee,
                        sum (r.refund_shipping_fee) refund_shipping_fee,
                        sum (r.refund_discount_amount) refund_discount_amount,
                        sum (r.refund_shipping_tax) refund_shipping_tax,
                        sum (r.refund_service_fee_tax) refund_service_fee_tax,
                        sum (r.refund_tax) refund_tax,
                        sum (r.refund_addon_discount_amt) refund_addon_discount_amount
                from    refund r
                where   r.order_detail_id = in_order_detail_id
                group by r.order_detail_id
        ) r
        ON      ob.order_detail_id = r.order_detail_id;


-- get the new amounts from the work table
OPEN out_modified_total FOR
        SELECT  odu.product_amount,
                odu.add_on_amount,
                odu.service_fee + odu.shipping_fee serv_ship_fee,
                odu.discount_amount,
                odu.shipping_tax + odu.tax tax,
                odu.product_amount +
                        odu.add_on_amount +
                        odu.service_fee + odu.shipping_fee -
                        odu.discount_amount +
                        odu.shipping_tax + odu.tax order_total,
                odu.service_fee,
                odu.shipping_fee,
                odu.shipping_tax,
                0 service_fee_tax,
                odu.tax product_tax,
                odu.add_on_discount_amount
        FROM    order_detail_update odu
        WHERE   order_detail_id = in_order_detail_id;

END VIEW_ORDER_TOTALS;


PROCEDURE VIEW_PAYMENT_TOTALS
(
IN_ORDER_DETAIL_ID              IN ORDER_BILLS_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_PAYMENT_TOTAL               OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the order totals from
        by payment type.
        Totals returned are product amount, add on amount, service/shipping fee,
        tax, discount, and total.

Input:
        order detail id

Output:
        cursor containing order bill information for each payment used on
        the order

-----------------------------------------------------------------------------*/
BEGIN

-- get the net amounts of the current order
OPEN out_payment_total FOR
        SELECT  ob.payment_type,
                ob.payment_type_id,
                ob.product_amount - nvl (r.refund_product_amount, 0) product_amount,
                ob.add_on_amount - nvl (r.refund_add_on_amount, 0) add_on_amount,
                (ob.service_fee - nvl (r.refund_service_fee, 0)) +
                       (ob.shipping_fee - nvl (r.refund_shipping_fee, 0)) serv_ship_fee,
                ob.discount_amount - nvl (r.refund_discount_amount, 0) discount_amount,
                (ob.shipping_tax - nvl (r.refund_shipping_tax, 0)) +
                       (ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0)) +
                       (ob.tax - nvl (r.refund_tax, 0)) tax,
                (ob.product_amount - nvl (r.refund_product_amount, 0)) +
                       (ob.add_on_amount - nvl (r.refund_add_on_amount, 0)) +
                       (ob.service_fee - nvl (r.refund_service_fee, 0)) +
                       (ob.shipping_fee - nvl (r.refund_shipping_fee, 0)) -
                       (ob.discount_amount - nvl (r.refund_discount_amount, 0)) +
                       (ob.shipping_tax - nvl (r.refund_shipping_tax, 0)) +
                       (ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0)) +
                       (ob.tax - nvl (r.refund_tax, 0)) order_total,
                ob.service_fee - nvl (r.refund_service_fee, 0) service_fee,
                ob.shipping_fee - nvl (r.refund_shipping_fee, 0) shipping_fee,
                ob.shipping_tax - nvl (r.refund_shipping_tax, 0) shipping_tax,
                ob.service_fee_tax - nvl (r.refund_service_fee_tax, 0) service_fee_tax,
                ob.tax - nvl (r.refund_tax, 0) product_tax,
                ob.add_on_discount_amount - nvl (r.refund_addon_discount_amount, 0) add_on_discount_amount
        FROM
        (
                select  ob.order_detail_id,
                        p.payment_type,
                        coalesce (to_char (p.cc_id), p.gc_coupon_number) payment_type_id,
                        sum (ob.product_amount) product_amount,
                        sum (ob.add_on_amount) add_on_amount,
                        sum (ob.service_fee) service_fee,
                        sum (ob.shipping_fee) shipping_fee,
                        sum (ob.discount_amount) discount_amount,
                        sum (ob.shipping_tax) shipping_tax,
                        sum (ob.service_fee_tax) service_fee_tax,
                        sum (ob.tax) tax,
                        max (p.updated_on) updated_on,
                        sum (ob.add_on_discount_amount) add_on_discount_amount
                from    order_bills ob
                join    order_details od
                on      ob.order_detail_id = od.order_detail_id
                join    payments p
                on      (od.order_guid = p.order_guid and ob.additional_bill_indicator = 'N' and p.additional_bill_id is null and p.refund_id is null)
                or      (ob.order_bill_id = p.additional_bill_id)
                where   ob.order_detail_id = in_order_detail_id
                group by ob.order_detail_id, p.payment_type, coalesce (to_char (p.cc_id), p.gc_coupon_number)
        ) ob
        LEFT OUTER JOIN
        (
                select  r.order_detail_id,
                        p.payment_type,
                        coalesce (to_char (p.cc_id), p.gc_coupon_number) payment_type_id,
                        sum (r.refund_product_amount) refund_product_amount,
                        sum (r.refund_addon_amount) refund_add_on_amount,
                        sum (r.refund_service_fee) refund_service_fee,
                        sum (r.refund_shipping_fee) refund_shipping_fee,
                        sum (r.refund_discount_amount) refund_discount_amount,
                        sum (r.refund_shipping_tax) refund_shipping_tax,
                        sum (r.refund_service_fee_tax) refund_service_fee_tax,
                        sum (r.refund_tax) refund_tax,
                        max (p.updated_on) updated_on,
                        sum (r.refund_addon_discount_amt) refund_addon_discount_amount
                from    refund r
                join    clean.payments p
                on      r.refund_id = p.refund_id
                where   r.order_detail_id = in_order_detail_id
                group by r.order_detail_id, p.payment_type, coalesce (to_char (p.cc_id), p.gc_coupon_number)
        ) r
        ON      ob.order_detail_id = r.order_detail_id
        AND     ob.payment_type = r.payment_type
        AND     ob.payment_type_id = r.payment_type_id
        ORDER BY ob.updated_on;


END VIEW_PAYMENT_TOTALS;


FUNCTION GET_PAYMENT_TRANSACTION_ID
RETURN NUMBER
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns the next sequence value for the
        payment transaction id.

Input:
        none

Output:
        payment transaction id

-----------------------------------------------------------------------------*/

CURSOR seq_cur IS
        SELECT  payment_transaction_id_sq.nextval FROM dual;

v_return integer;

BEGIN

OPEN seq_cur;
FETCH seq_cur INTO v_return;
CLOSE seq_cur;

RETURN v_return;
END GET_PAYMENT_TRANSACTION_ID;

PROCEDURE COMMIT_RECIPIENT_INFO
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the recipient info changes
        from the temporary update to the main order/customer tables.

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
-- cursor to get the updated recipient information
CURSOR recip_cur IS
        SELECT  odu.recipient_first_name,
                odu.recipient_last_name,
                odu.recipient_business_name,
                odu.recipient_address_1,
                odu.recipient_address_2,
                odu.recipient_city,
                odu.recipient_state,
                odu.recipient_zip_code,
                odu.recipient_country,
                odu.recipient_address_type,
                odu.recipient_business_info,
                odu.updated_by,
                odu.recipient_phone_number,
                odu.recipient_extension
        FROM    order_detail_update odu
        WHERE   odu.order_detail_id = in_order_detail_id;

recip_row       recip_cur%rowtype;

-- Cursor to retrieve the recipient id from the actual order detail record
-- after the recipient information was updated.  In the case where the
-- recipient concat id is changed a new recipient record will be created
-- and associated to the actual order detail record.  This logic
-- is handled in the customer_maint_pkg.update_recipient procedure.
CURSOR recip_id_cur IS
        SELECT  od.recipient_id
        FROM    order_details od
        WHERE   od.order_detail_id = in_order_detail_id;

v_old_recipient_id integer;
v_recipient_id  integer;
v_phone_id      integer;

BEGIN

-- get the existing recipient id from the order to compare in the case that a new recipient
-- record was created because the concat id changed
OPEN recip_id_cur;
FETCH recip_id_cur INTO v_old_recipient_id;
CLOSE recip_id_cur;

-- get the updated information
OPEN recip_cur;
FETCH recip_cur INTO recip_row;
CLOSE recip_cur;

-- update the recipient record
CUSTOMER_MAINT_PKG.UPDATE_RECIPIENT
(
        IN_ORDER_DETAIL_ID=>in_order_detail_id,
        IN_FIRST_NAME=>recip_row.recipient_first_name,
        IN_LAST_NAME=>recip_row.recipient_last_name,
        IN_BUSINESS_NAME=>recip_row.recipient_business_name,
        IN_ADDRESS_1=>recip_row.recipient_address_1,
        IN_ADDRESS_2=>recip_row.recipient_address_2,
        IN_CITY=>recip_row.recipient_city,
        IN_STATE=>recip_row.recipient_state,
        IN_ZIP_CODE=>recip_row.recipient_zip_code,
        IN_COUNTY=>null,
        IN_COUNTRY=>recip_row.recipient_country,
        IN_ADDRESS_TYPE=>recip_row.recipient_address_type,
        IN_UPDATED_BY=>recip_row.updated_by,
        IN_BUSINESS_INFO=>recip_row.recipient_business_info,
        OUT_STATUS=>out_status,
        OUT_MESSAGE=>out_message
);

IF out_status = 'Y' THEN
        -- get the recipient id from the order in the case that a new recipient
        -- record was created because the concat id changed
        OPEN recip_id_cur;
        FETCH recip_id_cur INTO v_recipient_id;
        CLOSE recip_id_cur;

        -- update the recipient evening phone number
        CUSTOMER_MAINT_PKG.UPDATE_CUSTOMER_PHONES
        (
                IN_CUSTOMER_ID=>v_recipient_id,
                IN_PHONE_TYPE=>'Evening',
                IN_PHONE_NUMBER=>recip_row.recipient_phone_number,
                IN_EXTENSION=>recip_row.recipient_extension,
                IN_PHONE_NUMBER_TYPE=>null,
                IN_SMS_OPT_IN=>'N',
                IN_UPDATED_BY=>recip_row.updated_by,
                IN_UPDATE_SCRUB=>'Y',
                OUT_PHONE_ID=>v_phone_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
END IF;

IF out_status = 'Y' THEN
        -- release the lock on the old recipient in the case that a new
        -- recipient record was created
        IF v_old_recipient_id <> v_recipient_id THEN
                DELETE FROM csr_locked_entities
                WHERE   entity_type = 'CUSTOMER'
                AND     entity_id = to_char (v_old_recipient_id);
        END IF;
END IF;

END COMMIT_RECIPIENT_INFO;


PROCEDURE COMMIT_UPDATE_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for applying the changes from the
        temporary update to the main order tables.

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

-- Cursor to retrieve the updated order data
CURSOR od_cur IS
        SELECT
                odu.order_detail_id,
                odu.order_guid,
                odu.updated_by,
                odu.source_code,
                odu.delivery_date,
                odu.delivery_date_range_end,
                odu.product_id,
                odu.quantity,
                odu.color_1,
                odu.color_2,
                odu.substitution_indicator,
                odu.occasion,
                odu.card_message,
                odu.card_signature,
                odu.special_instructions,
                odu.release_info_indicator,
                odu.florist_id,
                odu.ship_method,
                odu.ship_date,
                odu.second_choice_product,
                odu.size_indicator,
                odu.membership_number,
                odu.membership_first_name,
                odu.membership_last_name,
                decode ((select pm.product_type from ftd_apps.product_master pm where pm.product_id = odu.product_id), 'SDG', 'Y', 'SDFC', 'Y', 'N') same_day_gift,
                odu.miles_points,
                odu.subcode,
                decode (od.miles_points_post_date, null, 'N', 'Y') miles_points_posted,
                ftd_apps.source_query_pkg.get_source_reward_type (odu.source_code) discount_reward_type,
                o.customer_id,
                od.order_disp_code,
                odu.recipient_zip_code,
                odu.actual_product_amount,
                odu.actual_add_on_amount,
                odu.actual_shipping_fee,
                odu.actual_service_fee,
                odu.actual_discount_amount,
                odu.actual_shipping_tax,
                odu.actual_tax,
		od.personalization_data,
		odu.personal_greeting_id,
                od.bin_source_changed_flag,
                od.apply_surcharge_code,
                od.surcharge_description,
                od.display_surcharge_flag,
                od.send_surcharge_to_florist_flag,
                decode(nvl(obu.shipping_fee_saved,0) + nvl(obu.service_fee_saved,0), 0, 'N', 'Y') free_shipping_flag,
                od.original_order_has_sdu,
				od.PC_GROUP_ID,
				od.PC_MEMBERSHIP_ID,
				od.PC_FLAG,
				 od.derived_vip_flag,
				 od.time_of_service,
				 od.legacy_id,
         		odu.actual_add_on_discount_amount
        FROM    order_detail_update odu
        JOIN    order_details od
        ON      odu.order_detail_id = od.order_detail_id
        JOIN    orders o
        ON      odu.order_guid = o.order_guid
        JOIN    order_bills_update obu
        ON      obu.order_detail_id = odu.order_detail_id
        WHERE   odu.order_detail_id = in_order_detail_id;

CURSOR program_cur (p_source_code varchar2) IS
        SELECT  pp.partner_name
        FROM    ftd_apps.source_program_ref spr
        JOIN    ftd_apps.partner_program pp
        ON      spr.program_name = pp.program_name
        WHERE   spr.source_code = p_source_code
        AND     spr.start_date =
                (
                        select  max(b.start_date)
                        from    ftd_apps.source_program_ref b
                        where   b.source_code = spr.source_code
                        and     b.start_date <= sysdate
                );

-- Cursor to retrieve the updated add on information
CURSOR ao_cur IS
        SELECT  aou.order_add_on_id,
                aou.updated_by,
                aou.add_on_code,
                aou.add_on_quantity,
                aou.add_on_amount,
                aou.add_on_discount_amount
        FROM    order_add_ons_update aou
        WHERE   aou.order_detail_id = in_order_detail_id;

-- Cursor to retrieve the alternate contact info
CURSOR oc_cur IS
        SELECT  ocu.order_guid,
                ocu.first_name,
                ocu.last_name,
                ocu.phone_number,
                ocu.extension,
                ocu.email_address,
                ocu.updated_by,
                ocu.order_contact_info_id
        FROM    order_contact_update ocu
        WHERE   ocu.order_detail_id = in_order_detail_id;

-- Cursor to retrieve the current order hold reason
CURSOR order_hold_cur IS
        SELECT  reason,
                reason_text
        FROM    order_hold
        WHERE   order_detail_id = in_order_detail_id;

-- Cursor to get the message id if the order is in the zip queue
CURSOR zip_q_cur IS
        SELECT  message_id
        FROM    queue
        WHERE   order_detail_id = in_order_detail_id
        AND     queue_type = 'ZIP';

v_partner_name          varchar2 (50);
v_membership_id         memberships.membership_id%type;
v_order_disp_code       order_details.order_disp_code%type;
v_reason                order_hold.reason%type;
v_reason_text           order_hold.reason_text%type;
v_gnadd_flag            char(1);
v_zip_q_id              queue.message_id%type;
v_updated_by            order_detail_update.updated_by%type;
BEGIN

-- update the order information, there should only be one record in the order cursor
FOR od IN od_cur LOOP

        -- get the csr updating the order for use later when deleting from the zip queue
        -- if the order is in the zip queue
        v_updated_by := od.updated_by;

        -- update the recipient_information
        COMMIT_RECIPIENT_INFO
        (
                IN_ORDER_DETAIL_ID=>in_order_detail_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        -- update the membership information if it's present
        IF od.membership_number IS NOT NULL THEN
                OPEN program_cur (od.source_code);
                FETCH program_cur INTO v_partner_name;
                CLOSE program_cur;

                CUSTOMER_MAINT_PKG.UPDATE_UNIQUE_MEMBERSHIPS
                (
                        IN_CUSTOMER_ID=>od.customer_id,
                        IN_MEMBERSHIP_NUMBER=>od.membership_number,
                        IN_MEMBERSHIP_TYPE=>v_partner_name,
                        IN_FIRST_NAME=>od.membership_first_name,
                        IN_LAST_NAME=>od.membership_last_name,
                        IN_UPDATED_BY=>od.updated_by,
                        IN_MOST_RECENT_BY_TYPE=>'Y',
                        IN_UPDATE_SCRUB=>'Y',
                        OUT_MEMBERSHIP_ID=>v_membership_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END IF;

        -- If the current order disposition is Held, and the hold reason was
        -- product availability or preferred customer, release the hold.
        -- If the hold reason was gnadd, check the new recipient zip code, if the new zip code
        -- is not in gnadd, release the hold, if the new zip code is in gnadd, don't release the hold

        -- get the current hold reason
        OPEN order_hold_cur;
        FETCH order_hold_cur INTO v_reason, v_reason_text;
        CLOSE order_hold_cur;

        IF v_reason = 'PRODUCT' or (v_reason = 'CUSTOMER' and v_reason_text = 'Preferred') THEN
                DELETE FROM order_hold
                WHERE   order_detail_id = in_order_detail_id;
                v_order_disp_code := 'Processed';

        ELSIF v_reason = 'GNADD' THEN
               -- CSZ stored proc removed/not-applicable so just release hold (see Jira Q1SP17-10)
               DELETE FROM order_hold
               WHERE   order_detail_id = in_order_detail_id;
               v_order_disp_code := 'Processed';
        END IF;

        IF out_status = 'Y' THEN
/*   ADD CODE  Should be insert */
                ORDER_MAINT_PKG.UPDATE_ORDER_DETAILS_ALL_VAL
                (
                        IN_ORDER_DETAIL_ID=>od.order_detail_id,
                        IN_DELIVERY_DATE=>od.delivery_date,
                        IN_RECIPIENT_ID=>null,
                        IN_AVS_ADDRESS_ID=>null,
                        IN_PRODUCT_ID=>od.product_id,
                        IN_QUANTITY=>od.quantity,
                        IN_COLOR_1=>od.color_1,
                        IN_COLOR_2=>od.color_2,
                        IN_SUBSTITUTION_INDICATOR=>od.substitution_indicator,
                        IN_SAME_DAY_GIFT=>od.same_day_gift,
                        IN_OCCASION=>od.occasion,
                        IN_CARD_MESSAGE=>od.card_message,
                        IN_CARD_SIGNATURE=>od.card_signature,
                        IN_SPECIAL_INSTRUCTIONS=>od.special_instructions,
                        IN_RELEASE_INFO_INDICATOR=>od.release_info_indicator,
                        IN_FLORIST_ID=>od.florist_id,
                        IN_SHIP_METHOD=>od.ship_method,
                        IN_SHIP_DATE=>od.ship_date,
                        IN_ORDER_DISP_CODE=>v_order_disp_code,
                        IN_SECOND_CHOICE_PRODUCT=>od.second_choice_product,
                        IN_ZIP_QUEUE_COUNT=>null,
                        IN_UPDATED_BY=>od.updated_by,
                        IN_DELIVERY_DATE_RANGE_END=>od.delivery_date_range_end,
                        IN_SCRUBBED_ON=>null,
                        IN_SCRUBBED_BY=>null,
                        IN_ARIBA_UNSPSC_CODE=>null,
                        IN_ARIBA_PO_NUMBER=>null,
                        IN_ARIBA_AMS_PROJECT_CODE=>null,
                        IN_ARIBA_COST_CENTER=>null,
                        IN_SIZE_INDICATOR=>od.size_indicator,
                        IN_MILES_POINTS=>od.miles_points,
                        IN_SUBCODE=>od.subcode,
                        IN_SOURCE_CODE=>od.source_code,
                        IN_REJECT_RETRY_COUNT=>null,
                        IN_MEMBERSHIP_ID=>v_membership_id,
			IN_PERSONALIZATION_DATA=>od.personalization_data,
			IN_QMS_RESULT_CODE=>null,
                        IN_PERSONAL_GREETING_ID=>od.personal_greeting_id,
                        IN_BIN_SOURCE_CHANGED_FLAG=>od.bin_source_changed_flag,
                        IN_FREE_SHIPPING_FLAG=>od.free_shipping_flag ,
                        IN_APPLY_SURCHARGE_CODE	 =>	od.apply_surcharge_code,
                        IN_SURCHARGE_DESCRIPTION =>	od.surcharge_description,
                        IN_DISPLAY_SURCHARGE		 =>	od.display_surcharge_flag,
                        IN_SEND_SURCHARGE_TO_FLORIST   =>	od.send_surcharge_to_florist_flag,
						IN_ORIGINAL_ORDER_HAS_SDU => null,
						IN_PC_GROUP_ID => od.PC_GROUP_ID,
						IN_PC_MEMBERSHIP_ID => od.PC_MEMBERSHIP_ID,
						IN_PC_FLAG => od.PC_FLAG,
						IN_DERIVED_VIP_FLAG => od.derived_vip_flag,
						IN_TIME_OF_SERVICE => od.time_of_service,
						IN_LEGACY_ID => od.legacy_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END IF;
END LOOP;

IF out_status = 'Y' THEN
        -- delete non existing add ons from the real table
/*        DELETE FROM order_add_ons o
        WHERE   o.order_detail_id = in_order_detail_id
        AND     NOT EXISTS
        (
                SELECT  1
                FROM    order_add_ons_update n
                WHERE   n.order_add_on_id = o.order_add_on_id
        );
*/
        -- NEED TO ADD CODE for new order insert or update the order add ons
        FOR ao in ao_cur LOOP
                ORDER_MAINT_PKG.INSERT_ORDER_ADD_ONS
                (
                        IN_ORDER_DETAIL_ID=>in_order_detail_id,
                        IN_ADD_ON_CODE=>ao.add_on_code,
                        IN_ADD_ON_QUANTITY=>ao.add_on_quantity,
                        IN_ADD_ON_HISTORY_ID=>null,
                        IN_CREATED_BY=>ao.updated_by,
                        IO_ORDER_ADD_ON_ID=>ao.order_add_on_id,
                        IN_ADD_ON_AMOUNT=>ao.add_on_amount,
                        IN_ADD_ON_DISCOUNT_AMOUNT=>ao.add_on_discount_amount,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
                IF out_status = 'N' THEN
                        exit;
                END IF;
        END LOOP;
END IF;

IF out_status = 'Y' THEN
        -- insert or update the alternate contact information
        -- there should only be one record in the oc_cur
        FOR oc in oc_cur LOOP
/* ADD CODE Should be an insert   */
                ORDER_MAINT_PKG.UPDATE_ORDER_CONTACT_INFO
                (
                        IN_ORDER_GUID=>oc.order_guid,
                        IN_FIRST_NAME=>oc.first_name,
                        IN_LAST_NAME=>oc.last_name,
                        IN_PHONE_NUMBER=>oc.phone_number,
                        IN_EXTENSION=>oc.extension,
                        IN_EMAIL_ADDRESS=>oc.email_address,
                        IN_UPDATED_BY=>oc.updated_by,
                        IO_ORDER_CONTACT_INFO_ID=>oc.order_contact_info_id,
                        OUT_STATUS=>out_status,
                        OUT_MESSAGE=>out_message
                );
        END LOOP;
END IF;

IF out_status = 'Y' THEN
        -- if the order is in the zip queue, delete the zip queue record
        OPEN zip_q_cur;
        FETCH zip_q_cur INTO v_zip_q_id;
        CLOSE zip_q_cur;
        IF v_zip_q_id IS NOT NULL THEN
                queue_pkg.delete_queue_record_no_auth
                (
                        in_message_id=>v_zip_q_id,
                        in_csr_id=>v_updated_by,
                        out_status=>out_status,
                        out_error_message=>out_message
                );
        END IF;
END IF;

IF out_status = 'Y' THEN
        -- insert the credit card information if it does not exist in the main table
        INSERT INTO credit_cards
        (
                cc_id,
                cc_type,
                cc_number,
                cc_expiration,
                name,
                address_line_1,
                address_line_2,
                city,
                state,
                zip_code,
                country,
                created_on,
                created_by,
                updated_on,
                updated_by,
                customer_id,
                key_name,
                cc_number_masked
        )
        SELECT  ccu.cc_id,
                ccu.cc_type,
                ccu.cc_number,
                ccu.cc_expiration,
                ccu.name,
                ccu.address_line_1,
                ccu.address_line_2,
                ccu.city,
                ccu.state,
                ccu.zip_code,
                ccu.country,
                sysdate,
                ccu.updated_by,
                sysdate,
                ccu.updated_by,
                ccu.customer_id,
                ccu.key_name,
                substr(global.encryption.decrypt_it(ccu.cc_number,ccu.key_name),-4,4)
        FROM    credit_cards_update ccu
        WHERE   ccu.order_detail_id = in_order_detail_id
        AND     NOT EXISTS
        (       select  1
                from    credit_cards cc
                where   cc.cc_id = ccu.cc_id
        );

        -- update the credit card information if it exists in the main table
        UPDATE credit_cards cc
        SET
        (
                cc_expiration,
                name,
                address_line_1,
                address_line_2,
                city,
                state,
                zip_code,
                country,
                updated_on,
                updated_by
        ) =
        (
                SELECT  ccu.cc_expiration,
                        ccu.name,
                        ccu.address_line_1,
                        ccu.address_line_2,
                        ccu.city,
                        ccu.state,
                        ccu.zip_code,
                        ccu.country,
                        sysdate,
                        ccu.updated_by
                FROM    credit_cards_update ccu
                WHERE   ccu.order_detail_id = in_order_detail_id
        )
        WHERE   EXISTS
        (       select  1
                from    credit_cards_update ccu
                where   cc.cc_id = ccu.cc_id
                and     ccu.order_detail_id = in_order_detail_id
        );

        -- insert the added order bill records
/* ADD CODE  Take what in the order_bills_update to order_bills  */
        INSERT INTO order_bills
        (
                order_bill_id,
                order_detail_id,
                product_amount,
                add_on_amount,
                service_fee,
                shipping_fee,
                discount_amount,
                shipping_tax,
                tax,
                additional_bill_indicator,
                created_on,
                created_by,
                updated_on,
                updated_by,
                bill_status,
                service_fee_tax,
                commission_amount,
                wholesale_amount,
                transaction_amount,
                pdb_price,
                discount_product_price,
                discount_type,
                partner_cost,
                bill_date,
                service_fee_saved,
                shipping_fee_saved,
                add_on_discount_amount
        )
        SELECT  obu.order_bill_id,
                obu.order_detail_id,
                obu.product_amount,
                obu.add_on_amount,
                obu.service_fee,
                obu.shipping_fee,
                obu.discount_amount,
                obu.shipping_tax,
                obu.tax,
                obu.additional_bill_indicator,
                sysdate,
                obu.updated_by,
                sysdate,
                obu.updated_by,
                obu.bill_status,
                obu.service_fee_tax,
                obu.commission_amount,
                obu.wholesale_amount,
                obu.transaction_amount,
                obu.pdb_price,
                obu.discount_product_price,
                obu.discount_type,
                obu.partner_cost,
                obu.bill_date,
                obu.service_fee_saved,
                obu.shipping_fee_saved,
                obu.add_on_discount_amount
        FROM    order_bills_update obu
        WHERE   obu.order_detail_id = in_order_detail_id;

        -- insert the added refund records
        INSERT INTO refund
        (
                refund_id,
                refund_disp_code,
                created_on,
                created_by,
                updated_on,
                updated_by,
                refund_product_amount,
                refund_addon_amount,
                refund_service_fee,
                refund_tax,
                order_detail_id,
                responsible_party,
                refund_status,
                refund_admin_fee,
                refund_shipping_fee,
                refund_service_fee_tax,
                refund_shipping_tax,
                refund_discount_amount,
                refund_commission_amount,
                refund_wholesale_amount,
                refund_date
        )
        SELECT  ru.refund_id,
                ru.refund_disp_code,
                sysdate,
                ru.updated_by,
                sysdate,
                ru.updated_by,
                ru.refund_product_amount,
                ru.refund_addon_amount,
                ru.refund_service_fee,
                ru.refund_tax,
                ru.order_detail_id,
                ru.responsible_party,
                ru.refund_status,
                ru.refund_admin_fee,
                ru.refund_shipping_fee,
                ru.refund_service_fee_tax,
                ru.refund_shipping_tax,
                ru.refund_discount_amount,
                ru.refund_commission_amount,
                ru.refund_wholesale_amount,
                ru.refund_date
        FROM    refund_update ru
        WHERE   ru.order_detail_id = in_order_detail_id;

        -- insert the added payment records
/* ADD CODE  Take what in the order_bills_update to order_bills  */
        INSERT INTO payments
        (
                payment_id,
                order_guid,
                additional_bill_id,
                payment_type,
                cc_id,
                created_on,
                created_by,
                updated_on,
                updated_by,
                auth_result,
                auth_number,
                avs_code,
                acq_reference_number,
                gc_coupon_number,
                auth_override_flag,
                credit_amount,
                debit_amount,
                payment_indicator,
                refund_id,
                auth_date,
                auth_char_indicator,
                transaction_id,
                validation_code,
                auth_source_code,
                response_code,
                aafes_ticket_number,
                payment_transaction_id,
                nc_approval_identity_id
        )
        SELECT  pu.payment_id,
                pu.order_guid,
                pu.additional_bill_id,
                pu.payment_type,
                pu.cc_id,
                sysdate,
                pu.updated_by,
                sysdate,
                pu.updated_by,
                pu.auth_result,
                pu.auth_number,
                pu.avs_code,
                pu.acq_reference_number,
                pu.gc_coupon_number,
                pu.auth_override_flag,
                pu.credit_amount,
                pu.debit_amount,
                pu.payment_indicator,
                pu.refund_id,
                pu.auth_date,
                pu.auth_char_indicator,
                pu.transaction_id,
                pu.validation_code,
                pu.auth_source_code,
                pu.response_code,
                pu.aafes_ticket_number,
                pu.payment_transaction_id,
                pu.nc_approval_identity_id
        FROM    payments_update pu
        WHERE   EXISTS
        (       select  1
                from    order_bills_update obu
                where   obu.order_bill_id = pu.additional_bill_id
                and     obu.order_detail_id = in_order_detail_id
        )
        OR      EXISTS
        (       select  1
                from    refund_update ru
                where   ru.refund_id = pu.refund_id
                and     ru.order_detail_id = in_order_detail_id
        );

        -- insert the added accouting transaction records
/* ADD CODE  Take what in the acc_trans_update to acc_trans  */
        INSERT INTO accounting_transactions
        (
                accounting_transaction_id,
                transaction_type,
                transaction_date,
                order_detail_id,
                additional_order_seq,
                delivery_date,
                product_id,
                source_code,
                ship_method,
                product_amount,
                add_on_amount,
                shipping_fee,
                service_fee,
                discount_amount,
                shipping_tax,
                service_fee_tax,
                tax,
                payment_type,
                refund_disp_code,
                commission_amount,
                wholesale_amount,
                admin_fee,
                refund_id,
                order_bill_id,
                wholesale_service_fee,
                add_on_discount_amount
        )
        SELECT  au.accounting_transaction_id,
                au.transaction_type,
                au.transaction_date,
                au.order_detail_id,
                au.additional_order_seq,
                au.delivery_date,
                au.product_id,
                au.source_code,
                au.ship_method,
                au.product_amount,
                au.add_on_amount,
                au.shipping_fee,
                au.service_fee,
                au.discount_amount,
                au.shipping_tax,
                au.service_fee_tax,
                au.tax,
                au.payment_type,
                au.refund_disp_code,
                au.commission_amount,
                au.wholesale_amount,
                au.admin_fee,
                au.refund_id,
                au.order_bill_id,
                0,
                au.add_on_discount_amount
        FROM    accounting_transactions_upd au
        WHERE   au.order_detail_id = in_order_detail_id
        AND     NOT EXISTS
        (       select  1
                from    accounting_transactions at
                where   at.accounting_transaction_id = au.accounting_transaction_id
        );

        -- insert added comment records
/* ADD CODE  Take what in the comments_update to comments  */
        INSERT INTO comments
        (
                comment_id,
                customer_id,
                order_guid,
                order_detail_id,
                comment_origin,
                dnis_id,
                comment_text,
                comment_type,
                created_on,
                created_by,
                updated_on,
                updated_by
        )
        SELECT  cu.comment_id,
                cu.customer_id,
                cu.order_guid,
                cu.order_detail_id,
                cu.comment_origin,
                cu.dnis_id,
                cu.comment_text,
                cu.comment_type,
                sysdate,
                cu.updated_by,
                sysdate,
                cu.updated_by
        FROM    comments_update cu
        WHERE   cu.order_detail_id = in_order_detail_id;

        COMMIT;

        -- delete all of the update information from the temp tables
        DELETE_ORDER_UPDATE (IN_ORDER_DETAIL_ID, OUT_STATUS, OUT_MESSAGE);

ELSE

        ROLLBACK;

END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED COMMIT_UPDATE_ORDER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END COMMIT_UPDATE_ORDER;


PROCEDURE CANCEL_UPDATE_ORDER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
IN_UPDATE_RECIPIENT_IND         IN VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for deleting the update records in the
        work table when modify order is cancelled.

Input:
        order_detail_id                 number
        update_recip_ind
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

IF in_update_recipient_ind = 'Y' THEN
        -- update the recipient information for an cancelled modify order
        -- when the credit card failed authorization
        COMMIT_RECIPIENT_INFO
        (
                IN_ORDER_DETAIL_ID=>in_order_detail_id,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );

        -- commit the recipient information
        IF out_status = 'Y' THEN
                commit;
        ELSE
                rollback;
        END IF;
ELSE
        out_status := 'Y';
END IF;

IF out_status = 'Y' THEN
        -- delete all of the update information from the temp tables
        DELETE_ORDER_UPDATE (IN_ORDER_DETAIL_ID, OUT_STATUS, OUT_MESSAGE);
END IF;

END CANCEL_UPDATE_ORDER;


/****************************************************************************
   THE FOLLOWING PROCEDURES ARE USED IN THE INTERIM FOR UPDATING
   THE DELIVERY DATES ON THE ORDER THROUGH THE COMMUNICATION APPLICATION
   IN LIEU OF MODIFY ORDER.  ONCE MODIFY ORDER IS IMPLEMENTED IN PRODUCTION,
   THESE PROCEDURES SHOULD BE REMOVED
*****************************************************************************/

PROCEDURE INSERT_DELIVERY_DATE_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for saving the original delivery dates

Input:
        order_detail_id                 number
        updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

INSERT INTO order_detail_update
(
        order_detail_id,
        order_guid,
        updated_on,
        updated_by,
        delivery_date,
        delivery_date_range_end
)
SELECT
        order_detail_id,
        order_guid,
        updated_on,
        updated_by,
        delivery_date,
        delivery_date_range_end
FROM    order_details
WHERE   order_detail_id = in_order_detail_id;

COMMIT;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED COMMIT_UPDATE_ORDER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END INSERT_DELIVERY_DATE_UPDATE;


PROCEDURE CANCEL_DELIVERY_DATE_UPDATE
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for restoring the original delivery dates
        to the real tables from the update tables

Input:
        order_detail_id                 number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

-- restore the original delivery dates from the update table
UPDATE order_details
SET
(
        updated_on,
        updated_by,
        delivery_date,
        delivery_date_range_end
) =
(
        SELECT
                updated_on,
                updated_by,
                delivery_date,
                delivery_date_range_end
        FROM    order_detail_update
        WHERE   order_detail_id = in_order_detail_id
)
WHERE   order_detail_id = in_order_detail_id;

COMMIT;

-- delete all of the update information from the temp tables
DELETE_ORDER_UPDATE (IN_ORDER_DETAIL_ID, OUT_STATUS, OUT_MESSAGE);

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED COMMIT_UPDATE_ORDER [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
        ROLLBACK;

END CANCEL_DELIVERY_DATE_UPDATE;

PROCEDURE VIEW_ORDER_DETAIL_UPDATE_amt

(
IN_ORDER_DETAIL_ID              IN clean.ORDER_DETAIL_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_ORDER_DETAIL_UPDATE_REC     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the order detail update
        record from both the work table and the regualr tables for the order
        detail id passed

Input:
        order detail id

Output:
        cursor containing order detail information from the order detail update table

-----------------------------------------------------------------------------*/
BEGIN

-- get a order detail from the real order order detail update table
OPEN out_order_detail_update_rec FOR
        SELECT  PRODUCT_AMOUNT,
                ADD_ON_AMOUNT,
                SERVICE_FEE,
                SHIPPING_FEE,
                DISCOUNT_AMOUNT,
                SHIPPING_TAX,
                TAX
        FROM    clean.order_detail_update
        WHERE   order_detail_id = in_order_detail_id;

END VIEW_ORDER_DETAIL_UPDATE_amt;

PROCEDURE INSERT_MO_XREF
(IN_orig_order_detail_id         IN CLEAN.MO_XREF.orig_order_detail_id%TYPE,
 IN_new_order_detail_id          IN CLEAN.MO_XREF.new_order_detail_id %TYPE,
 IN_orig_external_order_number   IN CLEAN.MO_XREF.orig_external_order_number%TYPE,
 IN_new_external_order_number    IN CLEAN.MO_XREF.new_external_order_number%TYPE,
 IN_recip_name                   IN CLEAN.MO_XREF.recip_name%TYPE,
 IN_recip_business               IN CLEAN.MO_XREF.recip_business%TYPE,
 IN_recip_address                IN CLEAN.MO_XREF.recip_address%TYPE,
 IN_recip_delivery_location      IN CLEAN.MO_XREF.recip_delivery_location%TYPE,
 IN_recip_city                   IN CLEAN.MO_XREF.recip_city%TYPE,
 IN_recip_state                  IN CLEAN.MO_XREF.recip_state%TYPE,
 IN_recip_zipcode                IN CLEAN.MO_XREF.recip_zipcode%TYPE,
 IN_recip_country                IN CLEAN.MO_XREF.recip_country%TYPE,
 IN_recip_phone                  IN CLEAN.MO_XREF.recip_phone%TYPE,
 IN_delivery_date                IN CLEAN.MO_XREF.delivery_date%TYPE,
 IN_card_message                 IN CLEAN.MO_XREF.card_message%TYPE,
 IN_signature                    IN CLEAN.MO_XREF.signature%TYPE,
 IN_special_instructions         IN CLEAN.MO_XREF.special_instructions%TYPE,
 IN_alt_contact_info             IN CLEAN.MO_XREF.alt_contact_info%TYPE,
 IN_product                      IN CLEAN.MO_XREF.product%TYPE,
 IN_product_price                IN CLEAN.MO_XREF.product_price%TYPE,
 IN_ship_type_changed            IN CLEAN.MO_XREF.ship_type_changed%TYPE,
 IN_color_first_choice           IN CLEAN.MO_XREF.color_first_choice%TYPE,
 IN_color_second_choice          IN CLEAN.MO_XREF.color_second_choice%TYPE,
 IN_substitution_indicator       IN CLEAN.MO_XREF.substitution_indicator%TYPE,
 IN_ship_method                  IN CLEAN.MO_XREF.ship_method%TYPE,
 IN_source_code                  IN CLEAN.MO_XREF.source_code%TYPE,
 IN_membership_name              IN CLEAN.MO_XREF.membership_name%TYPE,
 IN_membership_number            IN CLEAN.MO_XREF.membership_number%TYPE,
 IN_add_ons                      IN CLEAN.MO_XREF.add_ons%TYPE,
 IN_florist_to_vendor            IN CLEAN.MO_XREF.florist_to_vendor%TYPE,
 IN_vendor_to_florist            IN CLEAN.MO_XREF.vendor_to_florist%TYPE,
 IN_created_by                   IN CLEAN.MO_XREF.created_by%TYPE,
 IN_personal_greeting_id         IN CLEAN.MO_XREF.personal_greeting_id%TYPE,
 OUT_STATUS                      OUT VARCHAR2,
 OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the delivery info in the
        hold table

Input:
        orig_order_detail_id         number
	new_order_detail_id          number
	orig_external_order_number   VARCHAR2
	new_external_order_number    VARCHAR2
	recip_name                   char
	recip_business               char
	recip_address                char
	recip_delivery_location      char
	recip_city                   char
	recip_state                  char
	recip_zipcode                char
	recip_country                char
	recip_phone                  char
	delivery_date                char
	card_message                 char
	signature                    char
	special_instructions         char
	alt_contact_info             char
	product                      char
	product_price                char
	ship_type_changed            char
	color_first_choice           char
	color_second_choice          char
	substitution_indicator       char
	ship_method                  char
	source_code                  char
	membership_name              char
	membership_number            char
	add_ons                      char
	florist_to_vendor            char
	vendor_to_florist            char
	created_by                   varchar2
	personal_greeting_id	     varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

insert into clean.mo_xref
(orig_order_detail_id,
 new_order_detail_id,
 orig_external_order_number,
 new_external_order_number,
 recip_name,
 recip_business,
 recip_address,
 recip_delivery_location,
 recip_city,
 recip_state,
 recip_zipcode,
 recip_country,
 recip_phone,
 delivery_date,
 card_message,
 signature,
 special_instructions,
 alt_contact_info,
 product,
 product_price,
 ship_type_changed,
 color_first_choice,
 color_second_choice,
 substitution_indicator,
 ship_method,
 source_code,
 membership_name,
 membership_number,
 add_ons,
 florist_to_vendor ,
 vendor_to_florist,
 created_on,
 created_by,
 personal_greeting_id
)
values
(in_orig_order_detail_id,
 in_new_order_detail_id,
 in_orig_external_order_number,
 in_new_external_order_number,
 in_recip_name,
 in_recip_business,
 in_recip_address,
 in_recip_delivery_location,
 in_recip_city,
 in_recip_state,
 in_recip_zipcode,
 in_recip_country,
 in_recip_phone,
 in_delivery_date,
 in_card_message,
 in_signature,
 in_special_instructions,
 in_alt_contact_info,
 in_product,
 in_product_price,
 in_ship_type_changed,
 in_color_first_choice,
 in_color_second_choice,
 in_substitution_indicator,
 in_ship_method,
 in_source_code,
 in_membership_name,
 in_membership_number,
 in_add_ons,
 in_florist_to_vendor ,
 in_vendor_to_florist,
 sysdate,
 in_created_by,
 in_personal_greeting_id
) ;
OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_MO_XREF [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_MO_XREF;

PROCEDURE VIEW_COMMENT_UPDATE
(
IN_ORDER_DETAIL_ID              IN COMMENTS_UPDATE.ORDER_DETAIL_ID%TYPE,
OUT_MESSAGE                     OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the requested comment
Input:
        order_detail_id                 number
Output:
        cursor message  from clean.comments_update
-----------------------------------------------------------------------------*/
BEGIN
OPEN OUT_MESSAGE FOR
        SELECT
                *
        FROM    clean.comments_update cou
        WHERE   cou.order_detail_id = in_order_detail_id;

END VIEW_COMMENT_UPDATE;


PROCEDURE GET_SCRUB_ORDER_DETAIL_ID
(
IN_EXTERNAL_ORDER_NUMBER        IN SCRUB.ORDER_DETAILS.EXTERNAL_ORDER_NUMBER%TYPE,
OUT_ORDER_DETAIL_ID             OUT SCRUB.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is returns order_detail_id for an external_order_number
        for the order

Input:
        external_order_number           number

Output:
	order_detail_id                 number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

      SELECT
         order_detail_id
      INTO
         out_order_detail_id
      FROM
         scrub.order_details
      WHERE
         external_order_number = in_external_order_number;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED GET_SCRUB_ORDER_DETAIL_ID [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END GET_SCRUB_ORDER_DETAIL_ID;

PROCEDURE INSERT_CO_BRAND_UPDATE
(
IN_ORDER_GUID                   IN CO_BRAND_UPDATE.ORDER_GUID%TYPE,
IN_INFO_NAME                    IN CO_BRAND_UPDATE.INFO_NAME%TYPE,
IN_INFO_DATA                    IN CO_BRAND_UPDATE.INFO_DATA%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting comments in the
        hold table

Input:
        IN_ORDER_GUID                   VARCHAR2
        IN_INFO_NAME                    VARCHAR2
        IN_INFO_DATA                    VARCHAR2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
BEGIN

INSERT INTO co_brand_update
(
        ORDER_GUID,
        INFO_NAME,
        INFO_DATA
)
VALUES
(
        IN_ORDER_GUID,
        IN_INFO_NAME,
        IN_INFO_DATA
);

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED INSERT_CO_BRAND_UPDATE [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CO_BRAND_UPDATE;

PROCEDURE IS_MODIFY_ORDER
(
IN_EXTERNAL_ORDER_NUMBER  IN clean.mo_xref.new_external_order_number%type,
OUT_MODIFY_FLAG           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        external order number exists in the CLEAN.MO_XREF_TABLE in the
        NEW_EXTERNAL_ORDER_NUMBER column.  Y = the record exists.


Input:
        IN_EXTERNAL_ORDER_NUMBER        VARCHAR2

Output:
        OUT_MODIFY_FLAG                 VARCHAR2

-----------------------------------------------------------------------------*/
v_count     number(4);
BEGIN
	SELECT count(*)
          INTO v_count
        FROM  clean.MO_XREF cmx
        WHERE cmx.NEW_EXTERNAL_ORDER_NUMBER = IN_EXTERNAL_ORDER_NUMBER;
     IF v_count > 0 THEN
        OUT_MODIFY_FLAG := 'Y';
     ELSE
        OUT_MODIFY_FLAG := 'N';
     END IF;

END IS_MODIFY_ORDER;

PROCEDURE IS_ORIG_MODIFY_ORDER
(
IN_ORIG_EXTERNAL_ORDER_NUMBER  IN clean.mo_xref.orig_external_order_number%type,
OUT_MODIFY_FLAG           OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        The proc will return a char (Y or N) indicating if the passed in
        orig external order number exists in the CLEAN.MO_XREF_TABLE in the
        ORIG_EXTERNAL_ORDER_NUMBER column.  Y = the record exists.


Input:
        IN_ORIG_EXTERNAL_ORDER_NUMBER        VARCHAR2

Output:
        OUT_MODIFY_FLAG                 VARCHAR2

-----------------------------------------------------------------------------*/
v_count     number(4);

BEGIN

   SELECT count(*)
     INTO v_count
     FROM clean.MO_XREF cmx
    WHERE cmx.ORIG_EXTERNAL_ORDER_NUMBER = IN_ORIG_EXTERNAL_ORDER_NUMBER;

     IF v_count > 0 THEN
        OUT_MODIFY_FLAG := 'Y';
     ELSE
        select count(*)
        into v_count
        from clean.order_details od
        join mercury.mercury m
        on m.reference_number = to_char(od.order_detail_id)
        and m.msg_type = 'FTD'
        and m.operator like '%FEMOE'
        where od.external_order_number = IN_ORIG_EXTERNAL_ORDER_NUMBER;

        IF v_count > 0 THEN
           OUT_MODIFY_FLAG := 'Y';
        ELSE
           OUT_MODIFY_FLAG := 'N';
        END IF;
     END IF;

END IS_ORIG_MODIFY_ORDER;

PROCEDURE FTD_MESSAGE_ATTD_NOT_VER
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_WAS_ATTEMPTED               OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
       when there was an attempted ftd on an order

Input:
        in_order_detail_id                 number

Output:
        Y/N if order date is past the parm value
-----------------------------------------------------------------------------*/

v_count        number(1);
BEGIN

SELECT count(*)
INTO v_count
FROM MERCURY.MERCURY mm
WHERE mm.reference_number = TO_CHAR(in_order_detail_id)
  AND mm.mercury_status = 'MO'
  AND mm.msg_type = 'FTD'
  AND mm.mercury_order_number IS NULL
  AND mm.created_on = (SELECT max(mm2.created_on)
                         FROM mercury.mercury mm2
                        WHERE mm2.reference_number = TO_CHAR(in_order_detail_id)
                          AND mm2.mercury_status = 'MO');

IF v_count > 0 THEN
   OUT_WAS_ATTEMPTED := 'Y';
ELSE
   OUT_WAS_ATTEMPTED := 'N';
END IF;

END FTD_MESSAGE_ATTD_NOT_VER;

PROCEDURE UDD_UPDATE_ORDER_DETAILS
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_DELIVERY_DATE                IN ORDER_DETAILS.DELIVERY_DATE%TYPE,
IN_DELIVERY_DATE_RANGE_END      IN
ORDER_DETAILS.DELIVERY_DATE_RANGE_END%TYPE,
IN_SHIP_DATE                    IN ORDER_DETAILS.SHIP_DATE%TYPE,
IN_FLORIST_ID                   IN ORDER_DETAILS.FLORIST_ID%TYPE,
IN_UPDATED_BY                   IN ORDER_DETAILS.UPDATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*--------------------------------------------------------------------------
---
Description:
        This procedure is responsible for updating the order detail record.

Input:
        order_detail_id                 number
        delivery_date                   date
        delivery_date_range_end         date
	ship_date                       date
	florist_id			varchar2
	updated_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

----------------------------------------------------------------------------
-*/

BEGIN

UPDATE  order_details
SET
        delivery_date = NVL(in_delivery_date, delivery_date),
	delivery_date_range_end = in_delivery_date_range_end,
	ship_date = NVL(in_ship_date, ship_date),
	florist_id = NVL(in_florist_id, florist_id),
	updated_on = sysdate,
        updated_by = in_updated_by
WHERE   order_detail_id = in_order_detail_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED IN UDD_UPDATE_ORDER_DETAILS [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UDD_UPDATE_ORDER_DETAILS;

PROCEDURE GET_ORDER_DETAIL_TAXES
(
IN_ORDER_DETAIL_ID              IN CLEAN.ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
OUT_CUR                         OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for returning the Order Bills Update
        taxes by order_detail_id and tax display order

Input:
        order_detail_id

Output:
        cursor containing order detail taxes

-----------------------------------------------------------------------------*/
BEGIN

OPEN OUT_CUR FOR
    select ob.order_detail_id, ob.tax1_description tax_description, ob.tax1_amount tax_amount,
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax1_name) display_order
        FROM    clean.order_bills_update ob
        WHERE   ob.order_detail_id = in_order_detail_id and additional_bill_indicator = 'N' and ob.tax1_name is not null
    union
    select ob.order_detail_id, ob.tax2_description tax_description, ob.tax2_amount tax_amount,
       (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax2_name) display_order
        FROM    clean.order_bills_update ob
        WHERE   ob.order_detail_id = in_order_detail_id and additional_bill_indicator = 'N' and ob.tax2_name is not null
    union
    select ob.order_detail_id, ob.tax3_description tax_description, ob.tax3_amount tax_amount,
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax3_name) display_order
        FROM    clean.order_bills_update ob
        WHERE   ob.order_detail_id = in_order_detail_id and additional_bill_indicator = 'N' and ob.tax3_name is not null
    union
    select ob.order_detail_id, ob.tax4_description tax_description, ob.tax4_amount tax_amount,
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax4_name) display_order
        FROM    clean.order_bills_update ob
        WHERE   ob.order_detail_id = in_order_detail_id and additional_bill_indicator = 'N' and ob.tax4_name is not null
    union
    select ob.order_detail_id, ob.tax5_description tax_description, ob.tax5_amount tax_amount,
        (select tdov.display_order  from ftd_apps.tax_display_order_val tdov where tdov.tax_name = ob.tax5_name) display_order
        FROM    clean.order_bills_update ob
        WHERE   ob.order_detail_id = in_order_detail_id and additional_bill_indicator = 'N' and ob.tax5_name is not null
    order by display_order;

END GET_ORDER_DETAIL_TAXES;

END MODIFY_ORDER_PKG;
/
