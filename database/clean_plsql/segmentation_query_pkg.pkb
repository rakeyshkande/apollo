CREATE OR REPLACE
PACKAGE BODY clean.SEGMENTATION_QUERY_PKG AS

FUNCTION GET_CUSTOMER_TIER
(
IN_CUSTOMER_ID                   IN CLEAN.ORDERS.CUSTOMER_ID%TYPE
)
RETURN VARCHAR2
IS
/*-----------------------------------------------------------------------------
Description:
        This function will obtain the customer tier value associated with the
        customer.

Input:
        IN_CUSTOMER_ID

Output:
        return TIER_VALUE

-----------------------------------------------------------------------------*/
v_tier_value            CLEAN.CUSTOMER.TIER_VALUE%TYPE;


BEGIN

        select c.tier_value
        into v_tier_value
        from clean.customer c
        where c.customer_id =  IN_CUSTOMER_ID;

        RETURN v_tier_value;

EXCEPTION WHEN NO_DATA_FOUND THEN
        RETURN NULL;

END GET_CUSTOMER_TIER;

END SEGMENTATION_QUERY_PKG;
.
/
