--
-- Trigger on payments to enqueue JMS message for CAMS order feed
--
CREATE OR REPLACE 
TRIGGER clean.trg_cams_feed_payments
AFTER INSERT 
ON clean.payments
FOR EACH ROW

DECLARE
   v_stat          varchar2(1) := 'Y';
   v_mess          varchar2(1000);
   v_exception     EXCEPTION;
   v_exception_txt varchar2(200);
   v_payload       varchar2(2000);

BEGIN
   IF :NEW.additional_bill_id IS null AND :NEW.payment_indicator = 'P' THEN
      -- Post a message with 5.5 minute delay (extra 30 sec over order_details delay to avoid simultaneous enqueues)
      v_payload := :NEW.order_guid || ',UPDATE_OG,1';
      events.post_a_message_flex('OJMS.ACCOUNT_ORDER_FEED', NULL, v_payload, 330, v_stat, v_mess);

      IF v_stat = 'N' THEN
         v_exception_txt := 'ERROR posting a JMS message in CAMS order feed trigger: order_guid = ' || :NEW.order_guid || SUBSTR(v_mess,1,200);
         raise v_exception;
      END IF;
   END IF;


END trg_cams_feed_payments;
.
/