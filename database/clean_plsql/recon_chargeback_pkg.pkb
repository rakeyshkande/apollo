CREATE OR REPLACE
PACKAGE BODY clean.RECON_CHARGEBACK_PKG
AS



PROCEDURE INSERT_CBR
(
 IN_PAYMENT_ID        IN CBR_PAYMENTS_REF.PAYMENT_ID%TYPE,
 IN_TYPE_CODE         IN CHARGEBACK_RETRIEVAL.TYPE_CODE%TYPE,
 IN_AMOUNT            IN CHARGEBACK_RETRIEVAL.AMOUNT%TYPE,
 IN_RECEIVED_DATE     IN CHARGEBACK_RETRIEVAL.RECEIVED_DATE%TYPE,
 IN_REVERSAL_DATE     IN CHARGEBACK_RETRIEVAL.REVERSAL_DATE%TYPE,
 IN_DUE_DATE          IN CHARGEBACK_RETRIEVAL.DUE_DATE%TYPE,
 IN_RETURNED_DATE     IN CHARGEBACK_RETRIEVAL.RETURNED_DATE%TYPE,
 IN_REASON_CODE       IN CHARGEBACK_RETRIEVAL.REASON_CODE%TYPE,
 IN_CREATED_BY        IN CHARGEBACK_RETRIEVAL.CREATED_BY%TYPE,
 IN_COMMENT_TEXT      IN CBR_COMMENTS.COMMENT_TEXT%TYPE,
 OUT_CBR_ID          OUT CHARGEBACK_RETRIEVAL.CBR_ID%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_ERROR_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure creates a record in the chargeback_retrieval,
        cbr_payments_ref, and cbr_comments tables.

Input:
        payment_id        VARCHAR2
        type_code         VARCHAR2
        amount            NUMBER
        received_date     DATE
        reversal_date     DATE
        due_date          DATE
        returned_date     DATE
        reason_code       VARCHAR2
        created_by        VARCHAR2
        comment_text      VARCHAR2

Output:
        cbr_id            NUMBER
        status            VARCHAR2
        error message     VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN


  INSERT INTO chargeback_retrieval
      (cbr_id,
       type_code,
       amount,
       received_date,
       reversal_date,
       due_date,
       returned_date,
       reason_code,
       created_on,
       created_by,
       updated_on,
       updated_by)
    VALUES
      (cbr_id_sq.NEXTVAL,
       in_type_code,
       in_amount,
       in_received_date,
       in_reversal_date,
       in_due_date,
       in_returned_date,
       in_reason_code,
       SYSDATE,
       in_created_by,
       SYSDATE,
       in_created_by) RETURNING cbr_id INTO out_cbr_id;

    INSERT INTO cbr_payments_ref
        (cbr_id,
         payment_id,
         created_on,
         created_by)
      VALUES
        (out_cbr_id,
         in_payment_id,
         SYSDATE,
         in_created_by);


  IF in_comment_text IS NOT NULL THEN
      INSERT INTO cbr_comments
        (cbr_id,
         cbr_comment_id,
         comment_text,
         created_on,
         created_by)
      VALUES
        (out_cbr_id,
         cbr_comment_id_sq.NEXTVAL,
         in_comment_text,
         SYSDATE,
         in_created_by);
  END IF;

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_CBR;


PROCEDURE UPDATE_CBR
(
 IN_CBR_ID            IN CHARGEBACK_RETRIEVAL.CBR_ID%TYPE,
 IN_RECEIVED_DATE     IN CHARGEBACK_RETRIEVAL.RECEIVED_DATE%TYPE,
 IN_REVERSAL_DATE     IN CHARGEBACK_RETRIEVAL.REVERSAL_DATE%TYPE,
 IN_DUE_DATE          IN CHARGEBACK_RETRIEVAL.DUE_DATE%TYPE,
 IN_RETURNED_DATE     IN CHARGEBACK_RETRIEVAL.RETURNED_DATE%TYPE,
 IN_REASON_CODE       IN CHARGEBACK_RETRIEVAL.REASON_CODE%TYPE,
 IN_UPDATED_BY        IN CHARGEBACK_RETRIEVAL.UPDATED_BY%TYPE,
 IN_COMMENT_TEXT      IN CBR_COMMENTS.COMMENT_TEXT%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_ERROR_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a chargeback_retrieval record for the
        given cbr_id. If in_comment is not null, it also inserts a comment
        in the cbr_comments table.

Input:
        cbr_id            NUMBER
        received_date     DATE
        reversal_date     DATE
        due_date          DATE
        returned_date     DATE
        reason_code       VARCHAR2
        updated_by        VARCHAR2
        comment_text      VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  UPDATE chargeback_retrieval
    SET received_date     = in_received_date,
        reversal_date     = in_reversal_date,
        due_date          = in_due_date,
        returned_date     = in_returned_date,
        reason_code       = in_reason_code,
        updated_on        = SYSDATE,
        updated_by        = in_updated_by
   WHERE cbr_id = in_cbr_id;

  IF SQL%FOUND THEN

    IF in_comment_text IS NOT NULL THEN
      INSERT INTO cbr_comments
         (cbr_id,
          cbr_comment_id,
          comment_text,
          created_on,
          created_by)
       VALUES
         (in_cbr_id,
          cbr_comment_id_sq.NEXTVAL,
          in_comment_text,
          SYSDATE,
          in_updated_by);
    END IF;

    out_status := 'Y';

  ELSE
    out_status := 'N';
    out_error_message := 'WARNING: No chargeback_retrieval records found for cbr_id ' || in_cbr_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END UPDATE_CBR;


PROCEDURE VIEW_CHARGEBACK_RETRIEVAL
(
 IN_CBR_ID         IN CHARGEBACK_RETRIEVAL.CBR_ID%TYPE,
 OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves chargeback_retrieval records
        for the given cbr_id.

Input:
        cbr_id         NUMBER

Output:
        out_cursor contains the chargeback_retrieval

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT cbr_id,
           type_code,
           amount,
           received_date,
           reversal_date,
           due_date,
           returned_date,
           reason_code,
           created_on,
           created_by,
           updated_on,
           updated_by
      FROM chargeback_retrieval
     WHERE cbr_id = in_cbr_id;


END VIEW_CHARGEBACK_RETRIEVAL;


PROCEDURE GET_CBR_COMMENTS
(
 IN_CBR_ID         IN CBR_COMMENTS.CBR_ID%TYPE,
 OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure retrieves all the cbr_comments records
        for the given cbr_id.

Input:
        cbr_id         NUMBER

Output:
        out_cursor contains the cbr_comments

-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT cbr_id,
           cbr_comment_id,
           comment_text,
           created_on,
           created_by
      FROM cbr_comments
     WHERE cbr_id = in_cbr_id;


END GET_CBR_COMMENTS;



PROCEDURE GET_PAYMENTS_FOR_CB
(
 IN_MASTER_ORDER_NUMBER  IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
 IN_TYPE_CODE            IN CHARGEBACK_RETRIEVAL.TYPE_CODE%TYPE,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a cursor of payments that are eligible
        for the user to create a charge back.

Input:
        master_order_number    VARCHAR2
        type_code              VARCHAR2

Output:
        out_cursor contains payments
-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT p.payment_id,
           p.credit_amount amount,
           p.payment_type,
           LPAD(cc.cc_number_masked, 16, '*') cc_number
      FROM credit_cards cc,
           payments p,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND p.payment_type NOT IN ('NC', 'GC', 'IN','GD')
       AND p.payment_indicator = 'P'
       AND (p.cc_id IS NOT NULL OR p.payment_type IN ('PP', 'BM', 'UA'))
       AND NOT EXISTS (SELECT 1
                         FROM cbr_payments_ref cpr,
                              chargeback_retrieval cr
                        WHERE cpr.payment_id = p.payment_id
                          AND cpr.cbr_id = cr.cbr_id
                          AND cr.type_code = in_type_code)
       AND p.cc_id = cc.cc_id (+)
       AND p.bill_status <> 'Unbilled';

END GET_PAYMENTS_FOR_CB;


PROCEDURE GET_PAYMENTS_NOT_FOR_CB
(
 IN_MASTER_ORDER_NUMBER  IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns a cursor of payments that are
        NOT eligible for the user to create a charge back.

Input:
        master_order_number    VARCHAR2

Output:
        out_cursor contains payments
-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT 'Payment' type,
           p.credit_amount amount,
           p.payment_type,
           cc.cc_number_masked cc_number
      FROM credit_cards cc,
           payments p,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND p.payment_type NOT IN ('NC', 'GC', 'IN')
       AND (
            p.payment_indicator = 'P'
            OR
            (
             p.payment_indicator = 'B'
             AND
             p.credit_amount >= p.debit_amount
            )
           )
       AND EXISTS (SELECT 1
                     FROM cbr_payments_ref cpr
                    WHERE cpr.payment_id = p.payment_id )
       AND p.cc_id = cc.cc_id (+)
   UNION
    SELECT 'Refund' type,
           p.debit_amount amount,
           p.payment_type,
           cc.cc_number_masked cc_number
      FROM credit_cards cc,
           payments p,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND p.payment_type NOT IN ('NC', 'GC', 'IN')
       AND (
            p.payment_indicator = 'R'
            OR
            (
             p.payment_indicator = 'B'
             AND
             p.credit_amount <= p.debit_amount
            )
           )
       AND EXISTS (SELECT 1
                     FROM cbr_payments_ref cpr
                    WHERE cpr.payment_id = p.payment_id )
       AND p.cc_id = cc.cc_id (+);

END GET_PAYMENTS_NOT_FOR_CB;


PROCEDURE GET_BILLING_TRANSACTIONS
(
 IN_MASTER_ORDER_NUMBER  IN ORDERS.MASTER_ORDER_NUMBER%TYPE,
 OUT_CURSOR             OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves payments for the original order or
        additional bills, refunds, charge back and retrieval for
        the shopping cart.

Input:
        master_order_number    VARCHAR2

Output:
        out_cursor contains payments
-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT 'Payment' type,
           p.credit_amount amount,
           p.payment_type,
           NULL refund_code,
           p.bill_date billed_date,
           ob.settled_date settled_date,
           NULL cbr_received_date,
           NULL cbr_due_date,
           NULL reversal,
           o.master_order_number order_number,
           NULL cbr_id,
           1 sort_order
      FROM order_bills ob,
           payments p,
           order_details od,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND p.payment_indicator = 'P'
       AND p.additional_bill_id IS NULL
       AND od.order_guid = o.order_guid
       AND ob.order_detail_id = od.order_detail_id
       AND ob.additional_bill_indicator = 'N'
   UNION
    SELECT 'Payment' type,
           p.credit_amount amount,
           p.payment_type,
           NULL refund_code,
           p.bill_date billed_date,
           ob.settled_date settled_date,
           NULL cbr_received_date,
           NULL cbr_due_date,
           NULL reversal,
           od.external_order_number order_number,
           NULL cbr_id,
           1 sort_order
      FROM order_bills ob,
           payments p,
           order_details od,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND p.payment_indicator = 'P'
       AND od.order_guid = o.order_guid
       AND ob.order_detail_id = od.order_detail_id
       AND ob.order_bill_id = p.additional_bill_id
       AND ob.additional_bill_indicator = 'Y'
   UNION
    SELECT 'Refund' type,
           p.debit_amount amount,
           p.payment_type,
           r.refund_disp_code refund_code,
           r.refund_date billed_date,
           r.settled_date settled_date,
           NULL cbr_received_date,
           NULL cbr_due_date,
           NULL reversal,
           od.external_order_number order_number,
           NULL cbr_id,
           2 sort_order
      FROM refund r,
           payments p,
           order_details od,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND od.order_guid = o.order_guid
       AND r.order_detail_id = od.order_detail_id
       AND p.payment_indicator = 'R'
       AND r.refund_id = p.refund_id
   UNION
    SELECT 'Charge Back' type,
           cr.amount,
           p1.payment_type,
           (SELECT max(r.refund_disp_code)
              FROM refund r,
                   payments p2,
                   cbr_payments_ref cpr2
             WHERE cpr2.cbr_id  = cr.cbr_id
               AND p2.payment_id  = cpr2.payment_id
               AND p2.payment_indicator = 'R'
               AND r.refund_id  = p2.refund_id) refund_code,
           NULL billed_date,
           NULL settled_date,
           cr.received_date cbr_received_date,
           cr.due_date cbr_due_date,
           NVL(cr.reversal_date,NULL) reversal,
           NULL order_number,
           cr.cbr_id,
           4 sort_order
      FROM chargeback_retrieval cr,
           cbr_payments_ref cpr1,
           payments p1,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p1.order_guid = o.order_guid
       AND p1.payment_indicator = 'P'
       AND cpr1.payment_id = p1.payment_id
       AND cr.cbr_id = cpr1.cbr_id
       AND cr.type_code = 'CB'
   UNION
    SELECT 'Retrieval' type,
           cr.amount,
           NULL payment_type,
           NULL refund_code,
           NULL billed_date,
           NULL settled_date,
           cr.received_date cbr_received_date,
           cr.due_date cbr_due_date,
           NULL reversal,
           NULL order_number,
           cr.cbr_id,
           3 sort_order
      FROM chargeback_retrieval cr,
           cbr_payments_ref cpr,
           payments p,
           orders o
     WHERE o.master_order_number = in_master_order_number
       AND p.order_guid = o.order_guid
       AND p.payment_indicator = 'P'
       AND cpr.payment_id = p.payment_id
       AND cr.cbr_id = cpr.cbr_id
       AND cr.type_code = 'RT'
    ORDER BY sort_order;

END GET_BILLING_TRANSACTIONS;


PROCEDURE GET_CBR_REASON_VAL_LIST
(
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure returns all the records in table cbr_reason_val.

Input:
        N/A

Output:
        out_cursor contains cbr_reason_val
-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT reason_code,
           description,
           status,
           1 ordering
      FROM cbr_reason_val
     WHERE reason_code = 'Fraud'
   UNION
    SELECT reason_code,
           description,
           status,
           2 ordering
      FROM cbr_reason_val
     WHERE reason_code = 'Non-Delivery'
   UNION
    SELECT reason_code,
           description,
           status,
           2 ordering
      FROM cbr_reason_val
     WHERE reason_code = 'Customer Service'
     ORDER BY ordering;

END GET_CBR_REASON_VAL_LIST;


PROCEDURE INSERT_CBR_PAYMENTS_REF
(
 IN_CBR_ID            IN CBR_PAYMENTS_REF.CBR_ID%TYPE,
 IN_PAYMENT_ID        IN CBR_PAYMENTS_REF.PAYMENT_ID%TYPE,
 IN_CREATED_BY        IN CBR_PAYMENTS_REF.CREATED_BY%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_ERROR_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure creates a record in the cbr_payments_ref
        for the given cbr_id and payment_id.

Input:
        cbr_id            NUMBER
        payment_id        VARCHAR2
        created_by        VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN

  INSERT INTO cbr_payments_ref
           (cbr_id,
            payment_id,
            created_on,
            created_by)
    VALUES
          (in_cbr_id,
           in_payment_id,
           SYSDATE,
           in_created_by);

  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_CBR_PAYMENTS_REF;


PROCEDURE DELETE_CBR_REFUNDS
(
 IN_CBR_ID      IN CBR_PAYMENTS_REF.CBR_ID%TYPE,
 OUT_STATUS    OUT VARCHAR2,
 OUT_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from the cbr_payments_ref
        table for the given cbr_id value passed in.

Input:
        in_cbr_id NUMBER

Output:
        status  varchar2 (Y or N)
        message varchar2 (error message)
-----------------------------------------------------------------------------*/

TYPE id_tab_typ IS TABLE OF INTEGER INDEX BY PLS_INTEGER;
id_tab          id_tab_typ;

CURSOR pay_cur (p_payment_id integer) IS
        SELECT  refund_id
        FROM    payments
        WHERE   payment_id = p_payment_id;

v_refund_id     integer;
BEGIN

  DELETE FROM cbr_payments_ref cpr
        WHERE cpr.cbr_id = in_cbr_id
          AND EXISTS (SELECT 1
                        FROM payments
                       WHERE payment_id = cpr.payment_id
                         AND (
                              payment_indicator = 'R'
                              OR
                              (
                               payment_indicator = 'B'
                               AND
                               credit_amount <= debit_amount
                              )
                             )
                     )
        RETURNING cpr.payment_id BULK COLLECT INTO id_tab;

  IF SQL%FOUND THEN
     out_status := 'Y';

     -- delete the refunds associated to the chargeback
     FOR x IN 1..id_tab.count LOOP
        OPEN pay_cur (id_tab (x));
        FETCH pay_cur INTO v_refund_id;
        CLOSE pay_cur;

        refund_pkg.delete_refund
        (
                in_refund_id=>v_refund_id,
                out_status=>out_status,
                out_message=>out_message
        );

        IF out_status = 'N' THEN
                EXIT;
        END IF;
     END LOOP;

  ELSE
     out_status := 'N';
     out_message := 'WARNING: No cbr_payments_ref records found for cbr_id ' || in_cbr_id ||'.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
   BEGIN
     OUT_STATUS := 'N';
     OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
   END;

END DELETE_CBR_REFUNDS;


PROCEDURE INSERT_CC_SETTLEMENT_ERRORS
(
 IN_TRANSACTION_DATE  IN CC_SETTLEMENT_ERRORS.TRANSACTION_DATE%TYPE,
 IN_CC_NUMBER         IN CC_SETTLEMENT_ERRORS.CC_NUMBER%TYPE,
 IN_AMOUNT            IN CC_SETTLEMENT_ERRORS.AMOUNT%TYPE,
 IN_AUTH_NUMBER       IN CC_SETTLEMENT_ERRORS.AUTH_NUMBER%TYPE,
 IN_RECON_DISP_CODE   IN CC_SETTLEMENT_ERRORS.RECON_DISP_CODE%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_ERROR_MESSAGE   OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure creates a record in table cc_settlement_errors.

Input:
        transaction_date DATE
        cc_number        VARCHAR2
        amount           NUMBER
        auth_number      VARCHAR2
        recon_disp_code  VARCHAR2

Output:
        status            VARCHAR2
        error message     VARCHAR2

-----------------------------------------------------------------------------*/

   v_key_name cc_settlement_errors.key_name%type;

BEGIN

  v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

  INSERT INTO cc_settlement_errors
      (cc_settlement_error_id,
       transaction_date,
       cc_number,
       key_name,
       amount,
       auth_number,
       recon_disp_code,
       created_on)
    VALUES
      (cc_settlement_error_id_sq.NEXTVAL,
       in_transaction_date,
       global.encryption.encrypt_it(in_cc_number,v_key_name),
       v_key_name,
       in_amount,
       in_auth_number,
       in_recon_disp_code,
       SYSDATE);


  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_CC_SETTLEMENT_ERRORS;


PROCEDURE FIND_SETTLEMENT_PAYMENT
(
 IN_CC_NUMBER      IN CREDIT_CARDS.CC_NUMBER%TYPE,
 IN_AUTH_DATE      IN PAYMENTS.AUTH_DATE%TYPE,
 IN_AUTH_NUMBER    IN PAYMENTS.AUTH_NUMBER%TYPE,
 IN_CREDIT_AMOUNT  IN PAYMENTS.CREDIT_AMOUNT%TYPE,
 OUT_CURSOR       OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the payment id and credit amount for the
        credit card number, authorization date, authorization number, and
        credit amount passed id.

Input:
        cc_number     VARCHAR2
        auth_date     DATE
        auth_number   VARCHAR2
        credit_amount NUMBER

Output:
        out_cursor contains payments
-----------------------------------------------------------------------------*/
v_key_name 		BILLING_DETAIL.KEY_NAME%TYPE;

BEGIN
  v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

  OPEN out_cursor FOR
    SELECT bd.payment_id, 
           bd.order_amount
    FROM   billing_detail bd
    WHERE  bd.cc_number = global.encryption.encrypt_it(in_cc_number,v_key_name)
      AND  trunc(bd.created_on) = trunc(in_auth_date)
      AND  bd.auth_approval_code = substr(in_auth_number,0,6)
      AND  bd.order_amount = in_credit_amount
    ORDER BY bd.payment_id;

END FIND_SETTLEMENT_PAYMENT;


PROCEDURE GET_CBR_PAYMENT
(
 IN_CBR_ID   IN CHARGEBACK_RETRIEVAL.CBR_ID%TYPE,
 OUT_CURSOR OUT TYPES.REF_CURSOR
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure retrieves the payment id, credit amount, payment_type
        and the last four credit card numbers for the cbr_id passed id.

Input:
        cbr_id NUMBER

Output:
        out_cursor containing payments and credit_cards info
-----------------------------------------------------------------------------*/

BEGIN

  OPEN out_cursor FOR
    SELECT p.payment_id,
           p.credit_amount amount,
           p.payment_type,
           LPAD(cc.cc_number_masked, 16, '*') cc_number
      FROM credit_cards cc,
           payments p,
           chargeback_retrieval cr,
           cbr_payments_ref cpr
     WHERE cr.cbr_id = in_cbr_id
       AND cpr.cbr_id = cr.cbr_id
       AND p.payment_id = cpr.payment_id
       AND cc.cc_id = p.cc_id;


END GET_CBR_PAYMENT;


PROCEDURE INSERT_CBR_PAYMENT
(
 IN_CREDIT_PAYMENT_ID       IN PAYMENTS.PAYMENT_ID%TYPE,
 IN_REFUND_ID               IN PAYMENTS.REFUND_ID%TYPE,
 IN_CREATED_BY              IN PAYMENTS.CREATED_BY%TYPE,
 IN_MILES_POINTS_DEBIT_AMT  IN PAYMENTS.MILES_POINTS_DEBIT_AMT%TYPE,
 IN_CC_AUTH_PROVIDER        IN PAYMENTS.CC_AUTH_PROVIDER%TYPE,
 OUT_DEBIT_PAYMENT_ID      OUT PAYMENTS.PAYMENT_ID%TYPE,
 OUT_STATUS                OUT VARCHAR2,
 OUT_ERROR_MESSAGE         OUT VARCHAR2)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure creates a record in table payments.

Input:
        credit_payment_id 		VARCHAR2
        refund_id         		VARCHAR2
        created_by        		VARCHAR2
        in_miles_points_debig_amount	NUMBER
        in_cc_auth_provider		VARCHAR2

Output:
        debit_payment_id  VARCHAR2
        status            VARCHAR2
        error message     VARCHAR2

-----------------------------------------------------------------------------*/

BEGIN

  SELECT key.keygen ('PAYMENTS')
    INTO out_debit_payment_id
    FROM DUAL;

  INSERT INTO payments
         (
          payment_id,
          order_guid,
          payment_type,
          cc_id,
          gc_coupon_number,
          created_on,
          created_by,
          updated_on,
          updated_by,
          debit_amount,
          payment_indicator,
          refund_id, 
          miles_points_debit_amt,
          cc_auth_provider,
		  route,
          authorization_transaction_id,
          merchant_ref_id,
          settlement_transaction_id,
          request_token
         )
      SELECT out_debit_payment_id,
             od.order_guid,
             p.payment_type,
             p.cc_id,
             p.gc_coupon_number,
             SYSDATE,
             in_created_by,
             SYSDATE,
             in_created_by,
             nvl(r.refund_product_amount,0) - nvl(r.refund_discount_amount,0) + nvl(r.refund_addon_amount,0) + nvl(r.refund_service_fee,0) + nvl(r.refund_shipping_fee,0) + nvl(r.refund_tax,0) + nvl(r.refund_service_fee_tax,0) + nvl(r.refund_shipping_tax,0),
             'R',
             in_refund_id, 
             in_miles_points_debit_amt,
             in_cc_auth_provider,
             p.route,
             p.authorization_transaction_id,
             p.merchant_ref_id,
             p.settlement_transaction_id,
             p.request_token
        FROM order_details od,
             payments p,
             refund r
       WHERE od.order_detail_id = r.order_detail_id
         AND p.payment_id = in_credit_payment_id
         AND r.refund_id = in_refund_id;

  update  accounting_transactions at
     set  at.payment_type = (select p1.payment_type from payments p1 where p1.refund_id=in_refund_id)
   where  at.refund_id = in_refund_id;
   
  IF in_cc_auth_provider='BAMS' THEN
            --create a unique reference number for refund
            INSERT INTO PAYMENTS_EXT(PAYMENT_ID,AUTH_PROPERTY_NAME,AUTH_PROPERTY_VALUE,CREATED_ON,CREATED_BY,UPDATED_ON,UPDATED_BY)
                SELECT out_debit_payment_id,'RefNum',key.keygen ('jccas_ref_num'),sysdate,in_created_by,sysdate,in_created_by
        		FROM DUAL
        		WHERE NOT EXISTS(SELECT 1 FROM PAYMENTS_EXT WHERE PAYMENT_ID=out_debit_payment_id AND AUTH_PROPERTY_NAME='RefNum');
  END IF;


  out_status := 'Y';

  EXCEPTION WHEN OTHERS THEN
  BEGIN
    ROLLBACK;
    out_status := 'N';
    out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);
  END;

END INSERT_CBR_PAYMENT;


END RECON_CHARGEBACK_PKG;
.
/
