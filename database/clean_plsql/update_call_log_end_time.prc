CREATE OR REPLACE
PROCEDURE clean.update_call_log_end_time
(
spring_forward_date             date
)
AS
-- This procedure adjusts call log end times for calls ended during time changes
-- for daylight savings times.
-- The parameter spring_forward_date only needs to be passed in when adjusting
-- the times in spring.  Pass in null when adjusting the times in fall.
-- Spring time adjustments will find call logs that began before 2:00 am on the date
-- specified and ended after 3:00 am on the date specified.  The end time will be moved
-- back one hour.
-- Fall time adjustments will find any call logs that have a negative difference in the
-- start and end times.  The end time will be moved forward one hour.

v_rowcount      integer;

BEGIN

IF spring_forward_date IS NOT NULL THEN
        UPDATE  call_log
        SET     end_time = end_time - 1/24
        WHERE   start_time <= trunc (spring_forward_date) + 2/24
        AND     end_time >= trunc (spring_forward_date) + 3/24;

        v_rowcount := sql%rowcount;
        dbms_output.put_line ('Number of call logs adjusted for spring time change: ' || v_rowcount);

        UPDATE  entity_history
        SET     end_time = end_time - 1/24
        WHERE   start_time <= trunc (spring_forward_date) + 2/24
        AND     end_time >= trunc (spring_forward_date) + 3/24;

        v_rowcount := sql%rowcount;
        dbms_output.put_line ('Number of entity histories adjusted for spring time change: ' || v_rowcount);

ELSE
        UPDATE  call_log
        SET     end_time = end_time + 1/24
        WHERE   end_time - start_time <= 0;

        v_rowcount := sql%rowcount;
        dbms_output.put_line ('Number of records adjusted for fall time change: ' || v_rowcount);

        UPDATE  entity_history
        SET     end_time = end_time + 1/24
        WHERE   end_time - start_time <= 0;

        v_rowcount := sql%rowcount;
        dbms_output.put_line ('Number of entity histories adjusted for fall time change: ' || v_rowcount);

END IF;

END;
.
/
