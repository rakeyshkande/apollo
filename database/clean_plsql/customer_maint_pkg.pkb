CREATE OR REPLACE
PACKAGE BODY clean.CUSTOMER_MAINT_PKG AS

PROCEDURE INSERT_AP_ACCOUNT
(
IN_AP_ACCOUNT_ID		IN AP_ACCOUNTS.AP_ACCOUNT_ID%TYPE,
IN_AP_ACCOUNT_TXT               IN AP_ACCOUNTS.AP_ACCOUNT_TXT%TYPE,
IN_PAYMENT_METHOD_ID            IN AP_ACCOUNTS.PAYMENT_METHOD_ID%TYPE,
IN_CUSTOMER_ID                  IN AP_ACCOUNTS.CUSTOMER_ID%TYPE,
IN_KEY_NAME               	IN AP_ACCOUNTS.KEY_NAME%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a new alternate payment
        account.

Input:
	ap_account_id			number
        ap_account_txt                  varchar2
        payment_method_id               varchar2
        customer_id                     number

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO AP_ACCOUNTS (ap_account_id, 
			 ap_account_txt, 
			 payment_method_id, 
			 customer_id, 
			 key_name, 
			 created_on,
			 created_by,
			 updated_on,
			 updated_by)
 VALUES(IN_AP_ACCOUNT_ID, 
 	in_ap_account_txt, 
 	in_payment_method_id, 
 	in_customer_id,
 	in_key_name,
 	sysdate,
 	'SYS',
 	sysdate,
 	'SYS');


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_AP_ACCOUNT;



PROCEDURE INSERT_CUSTOMER
(
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_BUSINESS_NAME                IN CUSTOMER.BUSINESS_NAME%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_ADDRESS_2                    IN CUSTOMER.ADDRESS_2%TYPE,
IN_CITY                         IN CUSTOMER.CITY%TYPE,
IN_STATE                        IN CUSTOMER.STATE%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_COUNTRY                      IN CUSTOMER.COUNTRY%TYPE,
IN_ADDRESS_TYPE                 IN CUSTOMER.ADDRESS_TYPE%TYPE,
IN_PREFERRED_CUSTOMER           IN CUSTOMER.PREFERRED_CUSTOMER%TYPE,
IN_BUYER_INDICATOR              IN CUSTOMER.BUYER_INDICATOR%TYPE,
IN_RECIPIENT_INDICATOR          IN CUSTOMER.RECIPIENT_INDICATOR%TYPE,
IN_ORIGIN_ID                    IN CUSTOMER.ORIGIN_ID%TYPE,
IN_FIRST_ORDER_DATE             IN CUSTOMER.FIRST_ORDER_DATE%TYPE,
IN_CREATED_BY                   IN CUSTOMER.CREATED_BY%TYPE,
IN_COUNTY                       IN CUSTOMER.COUNTY%TYPE,
IN_BUSINESS_INFO                IN CUSTOMER.BUSINESS_INFO%TYPE,
OUT_CUSTOMER_ID                 OUT CUSTOMER.CUSTOMER_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a new customer record

Input:
        first_name                      varchar2
        last_name                       varchar2
        business_name                   varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        address_type                    varchar2
        preferred_customer              char
        buyer_indicator                 char
        recipient_indicator             char
        origin_id                       varchar2
        first_order_date                date
        created_by                      varchar2
        county                          varchar2
        business_info                   varchar2

Output:
        customer_id                     number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

INSERT INTO customer
(
        customer_id,
        concat_id,
        first_name,
        last_name,
        business_name,
        address_1,
        address_2,
        city,
        state,
        zip_code,
        country,
        address_type,
        preferred_customer,
        buyer_indicator,
        recipient_indicator,
        origin_id,
        first_order_date,
        created_on,
        created_by,
        updated_on,
        updated_by,
        county,
        business_info
)
VALUES
(
        customer_id_sq.nextval,
        customer_query_pkg.get_customer_concat_id (substr ( rtrim (ltrim (in_first_name)), 1, 25 ), substr ( rtrim (ltrim (in_last_name)), 1, 25 ), substr ( rtrim (ltrim (in_address_1)), 1, 45 ), substr ( rtrim (ltrim (in_zip_code)), 1, 12 )),
        substr ( rtrim (ltrim (in_first_name)), 1, 25 ),
        substr ( rtrim (ltrim (in_last_name)), 1, 25 ),
        substr ( in_business_name, 1, 50 ),
        substr ( rtrim (ltrim (in_address_1)), 1, 45 ),
        substr ( in_address_2, 1, 45 ),
        substr ( in_city, 1, 30 ),
        substr ( in_state, 1, 20 ),
        substr ( rtrim (ltrim (in_zip_code)), 1, 12 ),
        substr ( in_country, 1, 20 ),
        in_address_type,
        nvl (in_preferred_customer, 'N'),
        nvl (in_buyer_indicator, 'N'),
        nvl (in_recipient_indicator, 'N'),
        in_origin_id,                   -- data from first_order
        in_first_order_date,            -- data from first_order
        sysdate,
        in_created_by,
        sysdate,
        in_created_by,
        substr ( in_county, 1, 20 ),
        substr ( in_business_info, 1, 30 )
) RETURNING customer_id INTO out_customer_id;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CUSTOMER;


PROCEDURE UPDATE_CUSTOMER
(
IN_CUSTOMER_ID                  IN CUSTOMER.CUSTOMER_ID%TYPE,
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_BUSINESS_NAME                IN CUSTOMER.BUSINESS_NAME%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_ADDRESS_2                    IN CUSTOMER.ADDRESS_2%TYPE,
IN_CITY                         IN CUSTOMER.CITY%TYPE,
IN_STATE                        IN CUSTOMER.STATE%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_COUNTRY                      IN CUSTOMER.COUNTRY%TYPE,
IN_ADDRESS_TYPE                 IN CUSTOMER.ADDRESS_TYPE%TYPE,
IN_PREFERRED_CUSTOMER           IN CUSTOMER.PREFERRED_CUSTOMER%TYPE,
IN_BUYER_INDICATOR              IN CUSTOMER.BUYER_INDICATOR%TYPE,
IN_RECIPIENT_INDICATOR          IN CUSTOMER.RECIPIENT_INDICATOR%TYPE,
IN_ORIGIN_ID                    IN CUSTOMER.ORIGIN_ID%TYPE,
IN_FIRST_ORDER_DATE             IN CUSTOMER.FIRST_ORDER_DATE%TYPE,
IN_UPDATED_BY                   IN CUSTOMER.UPDATED_BY%TYPE,
IN_COUNTY                       IN CUSTOMER.COUNTY%TYPE,
IN_UPDATE_SCRUB                 IN VARCHAR2,
IN_BUSINESS_INFO                IN CUSTOMER.BUSINESS_INFO%TYPE,
IN_VIP_CUSTOMER					IN CUSTOMER.VIP_CUSTOMER%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the customer record

Input:
        customer_id                     number
        first_name                      varchar2
        last_name                       varchar2
        business_name                   varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        address_type                    varchar2
        preferred_customer              char
        buyer_indicator                 char
        recipient_indicator             char
        origin_id                       varchar2
        first_order_date                date
        updated_by                      varchar2
        county                          varchar2
        update_scrub                    varchar2 - Y/N - updates from the application should be Y
                                                       - updates from dispatcher should be N
        business_info                   varchar2
		vip_customer					varchar2
Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

BEGIN

IF in_update_scrub = 'Y' THEN
        BUYER_CUSTOMER_MAINT_PKG.UPDATE_BUYER_FROM_CUSTOMER
        (
                IN_CUSTOMER_ID=>IN_CUSTOMER_ID,
                IN_FIRST_NAME=>substr ( rtrim (ltrim (in_first_name)), 1, 25 ),
                IN_LAST_NAME=>substr ( rtrim (ltrim (in_last_name)), 1, 25 ),
                IN_BUSINESS_NAME=>substr ( in_business_name, 1, 50 ),
                IN_ADDRESS_1=>substr ( rtrim (ltrim (in_address_1)), 1, 45 ),
                IN_ADDRESS_2=>substr ( in_address_2, 1, 45 ),
                IN_CITY=>substr ( in_city, 1, 30 ),
                IN_STATE=>substr ( in_state, 1, 20 ),
                IN_ZIP_CODE=>substr ( rtrim (ltrim (in_zip_code)), 1, 12 ),
                IN_COUNTRY=>substr ( in_country, 1, 20 ),
                IN_ADDRESS_TYPE=>IN_ADDRESS_TYPE,
                IN_PREFERRED_CUSTOMER=>IN_PREFERRED_CUSTOMER,
                IN_COUNTY=>substr ( in_county, 1, 20 ),
                OUT_STATUS=>OUT_STATUS,
                OUT_MESSAGE=>OUT_MESSAGE
        );
ELSE
        out_status := 'Y';
END IF;

IF out_status = 'Y' THEN
        UPDATE  customer
        SET
                concat_id = customer_query_pkg.get_customer_concat_id (substr ( rtrim (ltrim (in_first_name)), 1, 25 ), substr ( rtrim (ltrim (in_last_name)), 1, 25 ), substr ( rtrim (ltrim (in_address_1)), 1, 45 ), substr ( rtrim (ltrim (in_zip_code)), 1, 12 )),
                first_name = substr ( rtrim (ltrim (in_first_name)), 1, 25 ),
                last_name = substr ( rtrim (ltrim (in_last_name)), 1, 25 ),
                business_name = substr ( in_business_name, 1, 50 ),
                address_1 = substr ( rtrim (ltrim (in_address_1)), 1, 45 ),
                address_2 = substr ( in_address_2, 1, 45 ),
                city = substr ( in_city, 1, 30 ),
                state = substr ( in_state, 1, 20 ),
                zip_code = substr ( rtrim (ltrim (in_zip_code)), 1, 12 ),
                country = substr ( in_country, 1, 20 ),
                address_type = in_address_type,
                preferred_customer = nvl (in_preferred_customer, preferred_customer),
                buyer_indicator = nvl (in_buyer_indicator, buyer_indicator),
                recipient_indicator = nvl (in_recipient_indicator, recipient_indicator),
                origin_id = nvl (in_origin_id, origin_id),
                first_order_date = nvl (in_first_order_date, first_order_date),
                updated_on = sysdate,
                updated_by = in_updated_by,
                county = substr ( in_county, 1, 20 ),
                business_info = substr ( in_business_info, 1, 30 ),
                vip_customer = IN_VIP_CUSTOMER
        WHERE   customer_id = in_customer_id;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CUSTOMER;


PROCEDURE UPDATE_CUSTOMER_PHONES
(
IN_CUSTOMER_ID                  IN CUSTOMER_PHONES.CUSTOMER_ID%TYPE,
IN_PHONE_TYPE                   IN CUSTOMER_PHONES.PHONE_TYPE%TYPE,
IN_PHONE_NUMBER                 IN CUSTOMER_PHONES.PHONE_NUMBER%TYPE,
IN_EXTENSION                    IN CUSTOMER_PHONES.EXTENSION%TYPE,
IN_PHONE_NUMBER_TYPE            IN CUSTOMER_PHONES.PHONE_NUMBER_TYPE%TYPE,
IN_SMS_OPT_IN                   IN CUSTOMER_PHONES.SMS_OPT_IN%TYPE,
IN_UPDATED_BY                   IN CUSTOMER_PHONES.UPDATED_BY%TYPE,
IN_UPDATE_SCRUB                 IN VARCHAR2,
OUT_PHONE_ID                    OUT CUSTOMER_PHONES.PHONE_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the phone number of the
        given type for the customer

Input:
        customer_id                     number
        phone_type                      varchar2
        phone_number                    varchar2
        extension                       varchar2
        phone_number_type               varchar2
        sms_opt_in                      varchar2 - Y/N 
        updated_by                      varchar2
        update_scrub                    varchar2 - Y/N - updates from the application should be Y
                                                       - updates from dispatcher should be N

Output:
        phone_id                        number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
        SELECT  phone_id
        FROM    customer_phones
        WHERE   customer_id = in_customer_id
        AND     phone_type = in_phone_type;

BEGIN

IF in_update_scrub = 'Y' THEN
        BUYER_CUSTOMER_MAINT_PKG.UPDATE_BUYER_PHONES_FROM_CUST
        (
                IN_CUSTOMER_ID=>in_customer_id,
                IN_PHONE_TYPE=>in_phone_type,
                IN_PHONE_NUMBER=>in_phone_number,
                IN_EXTENSION=>in_extension,
                IN_PHONE_NUMBER_TYPE=>in_phone_number_type,
                IN_SMS_OPT_IN=>in_sms_opt_in,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
ELSE
        out_status := 'Y';
END IF;

IF out_status = 'Y' THEN
        OPEN exists_cur;
        FETCH exists_cur INTO out_phone_id;
        CLOSE exists_cur;

        IF out_phone_id IS NOT NULL THEN
                UPDATE  customer_phones
                SET
                        phone_number = in_phone_number,
                        extension = in_extension,
                        phone_number_type = in_phone_number_type,
                        sms_opt_in = in_sms_opt_in,
                        updated_on = sysdate,
                        updated_by = in_updated_by
                WHERE   phone_id = out_phone_id;

        ELSE
                INSERT INTO customer_phones
                (
                        phone_id,
                        customer_id,
                        phone_type,
                        phone_number,
                        extension,
                        phone_number_type,
                        sms_opt_in,
                        created_on,
                        created_by,
                        updated_on,
                        updated_by
                )
                VALUES
                (
                        phone_id_sq.nextval,
                        in_customer_id,
                        in_phone_type,
                        in_phone_number,
                        in_extension,
                        in_phone_number_type,
                        in_sms_opt_in,
                        sysdate,
                        in_updated_by,
                        sysdate,
                        in_updated_by
                ) RETURNING phone_id INTO out_phone_id;
        END IF;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CUSTOMER_PHONES;


PROCEDURE INSERT_DIRECT_MAIL
(
IN_CUSTOMER_ID                  IN DIRECT_MAIL.CUSTOMER_ID%TYPE,
IN_COMPANY_ID                   IN DIRECT_MAIL.COMPANY_ID%TYPE,
IN_CREATED_BY                   IN DIRECT_MAIL.CREATED_BY%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting catalog subscribe
        records

Input:
        customer_id                     number
        company_id                      varchar2 (optional)
        created_by                      varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_company_id varchar2(12);

BEGIN

IF in_company_id IS NOT NULL THEN
        -- if the company id was specified then insert a record for the
        -- specified company

        v_company_id := GET_NEWSLETTER_COMPANY_ID(in_company_id);

        INSERT INTO direct_mail
        (
                customer_id,
                company_id,
                created_on,
                created_by,
                updated_on,
                updated_by,
                subscribe_status
        )
        VALUES
        (
                in_customer_id,
                v_company_id,
                sysdate,
                in_created_by,
                sysdate,
                in_created_by,
                'Subscribe'
        );

ELSE
        -- insert a subscribe status for each existing company
        INSERT INTO direct_mail
        (
                customer_id,
                company_id,
                created_on,
                created_by,
                updated_on,
                updated_by,
                subscribe_status
        )
        SELECT DISTINCT
                in_customer_id,
                cm.newsletter_company_id,
                sysdate,
                in_created_by,
                sysdate,
                in_created_by,
                'Subscribe'
        FROM    ftd_apps.company_master cm
        WHERE   email_direct_mail_flag = 'Y';

END IF;

OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_DIRECT_MAIL;


PROCEDURE UPDATE_DIRECT_MAIL
(
IN_CUSTOMER_ID                  IN DIRECT_MAIL.CUSTOMER_ID%TYPE,
IN_COMPANY_ID                   IN DIRECT_MAIL.COMPANY_ID%TYPE,
IN_UPDATED_BY                   IN DIRECT_MAIL.UPDATED_BY%TYPE,
IN_SUBSCRIBE_STATUS             IN DIRECT_MAIL.SUBSCRIBE_STATUS%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the direct mail (catalog)
        record for the customer.  If the record doesn't exists then it
        will be inserted in the case a new company master record
        was added.

Input:
        customer_id                     number
        company_id                      varchar2
        updated_by                      varchar2
        subscribe_status                varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_company_id varchar2(12);

BEGIN

v_company_id := GET_NEWSLETTER_COMPANY_ID(in_company_id);

UPDATE  direct_mail
SET
        updated_on = sysdate,
        updated_by = in_updated_by,
        subscribe_status = in_subscribe_status
WHERE   customer_id = in_customer_id
AND     company_id = v_company_id;

IF sql%rowcount = 0 THEN
        INSERT_DIRECT_MAIL
        (
                IN_CUSTOMER_ID,
                V_COMPANY_ID,
                IN_UPDATED_BY,
                OUT_STATUS,
                OUT_MESSAGE
        );
ELSE
        OUT_STATUS := 'Y';
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_DIRECT_MAIL;


PROCEDURE UPDATE_UNIQUE_EMAIL
(
IN_CUSTOMER_ID                  IN CUSTOMER_EMAIL_REF.CUSTOMER_ID%TYPE,
IN_COMPANY_ID                   IN EMAIL.COMPANY_ID%TYPE,
IN_EMAIL_ADDRESS                IN EMAIL.EMAIL_ADDRESS%TYPE,
IN_ACTIVE_INDICATOR             IN VARCHAR2,
IN_SUBSCRIBE_STATUS             IN EMAIL.SUBSCRIBE_STATUS%TYPE,
IN_UPDATED_BY                   IN EMAIL.UPDATED_BY%TYPE,
IN_MOST_RECENT_BY_COMPANY       IN CUSTOMER_EMAIL_REF.MOST_RECENT_BY_COMPANY%TYPE,
IN_UPDATE_SCRUB                 IN VARCHAR2,
IN_PROTECT_UNSUB                IN VARCHAR2 := 'N',
IN_CREATE_ALL_CO_IND            IN VARCHAR2 := 'Y',
IO_EMAIL_ID                     IN OUT EMAIL.EMAIL_ID%TYPE,
OUT_COMPANY_LIST                OUT VARCHAR2,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the unique email id
        by company for a given customer

Input:
        customer_id                     number
        company_id                      varchar2
        email_address                   varchar2
        active_indicator                char    - not used
        subscribe_status                varchar2
        created_by                      varchar2
        most_recent_by_company          varchar2 (Y if this email should be displayed in the customer account page)
        update_scrub                    varchar2 - Y/N - updates from the application should be Y
                                                       - updates from dispatcher should be N
        protect_unsub                   varchar2 (optional; Y if previously unsubscribed records should NOT be updated.
                                                 N (default) if previously unsubscribed records should be updated.)
        create_all_co_ind               varchar2 (optional; Y (default) if recursive behavior should be engaged to cover email for all cobrand companies.
                                                 N if recursive behavior should NOT be engaged.)
        email_id                        number - If the email id is given, the application is updating
                                                 the given record.  If no id is given, the application
                                                 is inserting a new email/company record for the customer
                                                 which may or may not already exist.

Output:
        email_id                        number
        company_list                    varchar2 - If create_all_co_ind is 'Y', this produces a comma-delimited list of the newly inserted company records.  NULL otherwise.
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
CURSOR exists_cur (in_v_company_id varchar2) IS
        SELECT  email_id
        FROM    email
        WHERE   company_id = in_v_company_id
        AND     lower (email_address) = lower (trim (in_email_address));

CURSOR cer_cur (p_email_id integer) IS
        SELECT  'Y'
        FROM    customer_email_ref
        WHERE   customer_id = in_customer_id
        AND     email_id = p_email_id;

v_cer_exists    char(1) := 'N';

v_io_email_id EMAIL.EMAIL_ID%TYPE;
v_out_company VARCHAR2(100);
v_company_id varchar2(100);

cursor company_cur is
        select  distinct cm.newsletter_company_id
        from    ftd_apps.company_master cm
        where   cm.email_direct_mail_flag = 'Y'
        minus
        select  e.company_id
        from    clean.email e
        where   lower (e.email_address) = lower (trim (in_email_address))
        and     e.subscribe_status is not null;

BEGIN

v_company_id := GET_NEWSLETTER_COMPANY_ID(in_company_id);

-- update the old most recent record to not be the most recent record
-- if the given email record is coming from a new order
IF in_most_recent_by_company = 'Y' THEN
        UPDATE  customer_email_ref cer
        SET     cer.most_recent_by_company = 'N',
                cer.updated_on = sysdate,
                cer.updated_by = in_updated_by
        WHERE   cer.customer_id = in_customer_id
        AND     exists
                (
                        select  1
                        from    email e
                        where   e.email_id = cer.email_id
                        and     company_id = v_company_id
                )
        AND     most_recent_by_company = 'Y';
END IF;

IF io_email_id IS NULL THEN
        -- check if the email exists for the company
        -- if a record exists, update that record, if not, insert a new record
        OPEN exists_cur (v_company_id);
        FETCH exists_cur INTO io_email_id;
        CLOSE exists_cur;
END IF;

IF io_email_id IS NOT NULL THEN

   IF upper(NVL(in_protect_unsub, 'N')) = 'N' THEN

        UPDATE  email
        SET
                subscribe_status = NVL(in_subscribe_status, DECODE (in_subscribe_status, 'Unsubscribe', decode (subscribe_status, NULL, NULL
                                                                                                                                      , in_subscribe_status)
                                                                                       ,  NULL        , subscribe_status
                                                                                                      , in_subscribe_status)),
                subscribe_date = decode (in_subscribe_status, 'Subscribe', decode (subscribe_status, 'Subscribe', subscribe_date, sysdate), subscribe_date),
                updated_on = sysdate,
                updated_by = in_updated_by
        WHERE   email_id = io_email_id;
   ELSE

        UPDATE  email
        SET
                subscribe_status = NVL(in_subscribe_status, DECODE (in_subscribe_status, 'Unsubscribe', decode (subscribe_status, NULL, NULL
                                                                                                                                      , in_subscribe_status)
                                                                                       ,  NULL        , subscribe_status
                                                                                                      , in_subscribe_status)),
                subscribe_date = decode (in_subscribe_status, 'Subscribe', decode (subscribe_status, 'Subscribe', subscribe_date, sysdate), subscribe_date),
                updated_on = sysdate,
                updated_by = in_updated_by
        WHERE   email_id = io_email_id
        AND     (subscribe_status != 'Unsubscribe' OR subscribe_status IS NULL);

   END IF;



ELSE
BEGIN
        INSERT INTO email
        (
                email_id,
                company_id,
                email_address,
                subscribe_status,
                subscribe_date,
                created_on,
                created_by,
                updated_on,
                updated_by
        )
        VALUES
        (
                email_id_sq.nextval,
                v_company_id,
                lower (trim (in_email_address)),
                in_subscribe_status,
                decode (in_subscribe_status, 'Subscribe', sysdate, null),
                sysdate,
                in_updated_by,
                sysdate,
                in_updated_by
        ) RETURNING email_id INTO io_email_id;

        EXCEPTION WHEN dup_val_on_index THEN
                -- Check again if the email exists for the company.
                -- This can happen when two processes are trying to insert the same email record
                -- as seen on production between the weboe newletter feed and order dispatcher.
                OPEN exists_cur (v_company_id);
                FETCH exists_cur INTO io_email_id;
                CLOSE exists_cur;

                IF io_email_id IS NULL THEN
                        raise;
                END IF;

        WHEN OTHERS THEN
                raise;
END;
END IF;

-- check if the customer email relationship exists
IF in_customer_id IS NOT NULL THEN

        OPEN cer_cur (io_email_id);
        FETCH cer_cur INTO v_cer_exists;
        CLOSE cer_cur;

        IF v_cer_exists = 'N' THEN
                INSERT INTO customer_email_ref
                (
                        customer_id,
                        email_id,
                        most_recent_by_company,
                        created_on,
                        created_by,
                        updated_on,
                        updated_by
                )
                VALUES
                (
                        in_customer_id,
                        io_email_id,
                        nvl (in_most_recent_by_company, 'N'),
                        sysdate,
                        in_updated_by,
                        sysdate,
                        in_updated_by
                );
        ELSE
                IF in_most_recent_by_company = 'Y' THEN
                        UPDATE  customer_email_ref
                        SET     most_recent_by_company = 'Y'
                        WHERE   customer_id = in_customer_id
                        AND     email_id = io_email_id;
                END IF;
        END IF;
END IF;

OUT_STATUS := 'Y';

-- Recursive behavior to cover email for all cobrand companies
IF (upper(NVL(in_create_all_co_ind, 'Y')) = 'Y' AND (in_subscribe_status IS NULL OR in_subscribe_status = 'Subscribe')) THEN

for company_row in company_cur loop

       v_io_email_id := NULL;
       CLEAN.CUSTOMER_MAINT_PKG.UPDATE_UNIQUE_EMAIL(
                IN_CUSTOMER_ID=>in_customer_id,
                IN_COMPANY_ID=>company_row.newsletter_company_id,
                IN_EMAIL_ADDRESS=>in_email_address,
                IN_ACTIVE_INDICATOR=>'Y',
                IN_SUBSCRIBE_STATUS=>in_subscribe_status,
                IN_UPDATED_BY=>IN_UPDATED_BY,
                IN_MOST_RECENT_BY_COMPANY=>'Y',
                IN_UPDATE_SCRUB=>'Y',
                IN_PROTECT_UNSUB=>'Y',
                IN_CREATE_ALL_CO_IND=>'N',
                IO_EMAIL_ID=>v_io_email_id,
                OUT_COMPANY_LIST=>v_out_company,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message);

       out_company_list := out_company_list || company_row.newsletter_company_id || ',';

       EXIT WHEN OUT_STATUS = 'N';
end loop;

END IF;


EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_UNIQUE_EMAIL;

PROCEDURE UPDATE_UNIQUE_MEMBERSHIPS
(
IN_CUSTOMER_ID                  IN MEMBERSHIPS.CUSTOMER_ID%TYPE,
IN_MEMBERSHIP_NUMBER            IN MEMBERSHIPS.MEMBERSHIP_NUMBER%TYPE,
IN_MEMBERSHIP_TYPE              IN MEMBERSHIPS.MEMBERSHIP_TYPE%TYPE,
IN_FIRST_NAME                   IN MEMBERSHIPS.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN MEMBERSHIPS.LAST_NAME%TYPE,
IN_UPDATED_BY                   IN MEMBERSHIPS.UPDATED_BY%TYPE,
IN_MOST_RECENT_BY_TYPE          IN MEMBERSHIPS.MOST_RECENT_BY_TYPE%TYPE,
IN_UPDATE_SCRUB                 IN VARCHAR2,
OUT_MEMBERSHIP_ID               OUT MEMBERSHIPS.MEMBERSHIP_ID%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the unique memebership
        for the customer

Input:
        customer_id                     number
        membership_number               varchar2
        membership_type                 varchar2
        first_name                      varchar2
        last_name                       varchar2
        created_by                      varchar2
        most_recent_by_type             varchar2 (Y if coming from a new order,
                                                  null if just updating the membership record)
        update_scrub                    varchar2 - Y/N - updates from the application should be Y
                                                       - updates from dispatcher should be N

Output:
        membership_id                   number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

CURSOR exists_cur IS
        SELECT  membership_id
        FROM    memberships
        WHERE   customer_id = in_customer_id
        AND     membership_type = in_membership_type
        AND     membership_number = in_membership_number;

v_membership_id         memberships.membership_id%type;

BEGIN

IF in_update_scrub = 'Y' THEN
        BUYER_CUSTOMER_MAINT_PKG.UPDATE_MEMBERSHIPS_FROM_CUST
        (
                IN_CUSTOMER_ID=>in_customer_id,
                IN_MEMBERSHIP_NUMBER=>in_membership_number,
                IN_MEMBERSHIP_TYPE=>in_membership_type,
                IN_FIRST_NAME=>in_first_name,
                IN_LAST_NAME=>in_last_name,
                OUT_STATUS=>out_status,
                OUT_MESSAGE=>out_message
        );
ELSE
        out_status := 'Y';
END IF;

IF out_status = 'Y' THEN
        -- update the old most recent record to not be the most recent record
        -- if the given membership record is coming from a new order
        IF in_most_recent_by_type = 'Y' THEN
                UPDATE  memberships
                SET     most_recent_by_type = 'N',
                        updated_on = sysdate,
                        updated_by = in_updated_by
                WHERE   customer_id = in_customer_id
                AND     membership_type = in_membership_type
                AND     membership_number <> in_membership_number
                AND     most_recent_by_type = 'Y';
        END IF;

        -- check if a record exists using the given membership type and membership number.
        -- if a record exists, update that record, if not, insert a new record
        OPEN exists_cur;
        FETCH exists_cur INTO v_membership_id;
        CLOSE exists_cur;

        IF v_membership_id IS NOT NULL THEN
                -- if the membership number record exists, update the current record
                UPDATE  memberships
                SET     first_name = in_first_name,
                        last_name = in_last_name,
                        most_recent_by_type = coalesce (in_most_recent_by_type, most_recent_by_type),
                        updated_on = sysdate,
                        updated_by = in_updated_by
                WHERE   membership_id = v_membership_id;

                out_membership_id := v_membership_id;
        ELSE
                -- if the membership number does not exist, insert a new record
                INSERT INTO memberships
                (
                        membership_id,
                        customer_id,
                        membership_number,
                        membership_type,
                        first_name,
                        last_name,
                        created_on,
                        created_by,
                        updated_on,
                        updated_by,
                        most_recent_by_type
                )
                VALUES
                (
                        key.keygen('MEMBERSHIPS'),
                        in_customer_id,
                        in_membership_number,
                        in_membership_type,
                        in_first_name,
                        in_last_name,
                        sysdate,
                        in_updated_by,
                        sysdate,
                        in_updated_by,
                        coalesce (in_most_recent_by_type, 'N')
                ) RETURNING membership_id INTO out_membership_id;

        END IF;
END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_UNIQUE_MEMBERSHIPS;


PROCEDURE UPDATE_RECIPIENT
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_BUSINESS_NAME                IN CUSTOMER.BUSINESS_NAME%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_ADDRESS_2                    IN CUSTOMER.ADDRESS_2%TYPE,
IN_CITY                         IN CUSTOMER.CITY%TYPE,
IN_STATE                        IN CUSTOMER.STATE%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_COUNTY                       IN CUSTOMER.COUNTY%TYPE,
IN_COUNTRY                      IN CUSTOMER.COUNTRY%TYPE,
IN_ADDRESS_TYPE                 IN CUSTOMER.ADDRESS_TYPE%TYPE,
IN_UPDATED_BY                   IN CUSTOMER.UPDATED_BY%TYPE,
IN_BUSINESS_INFO                IN CUSTOMER.BUSINESS_INFO%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the recipient information.
        If the concat id for the recipient changes, then a new customer
        record will be created and the order associated to the new record leaving
        an orphaned customer record.

Input:
        order_detail_id                 number
        first_name                      varchar2
        last_name                       varchar2
        business_name                   varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        county                          varchar2
        country                         varchar2
        address_type                    varchar2
        updated_by                      varchar2
        business_info                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

-- get the new concatenated id using the updated first and last name
CURSOR cust_cur IS
        SELECT  od.recipient_id old_recipient_id,
                c.concat_id old_concat_id,
                customer_query_pkg.get_customer_concat_id (
                        coalesce (in_first_name, c.first_name),
                        coalesce (in_last_name, c.last_name),
                        coalesce (in_address_1, c.address_1),
                        coalesce (in_zip_code, c.zip_code)
                ) new_concat_id,
                coalesce (in_first_name, c.first_name) first_name,
                coalesce (in_last_name, c.last_name) last_name,
                coalesce (in_business_name, c.business_name) business_name,
                coalesce (in_address_1, c.address_1) address_1,
                coalesce (in_address_2, c.address_2) address_2,
                coalesce (in_city, c.city) city,
                coalesce (in_state, c.state) state,
                coalesce (in_zip_code, c.zip_code) zip_code,
                coalesce (in_country, c.country) country,
                coalesce (in_address_type, c.address_type) address_type,
                coalesce (in_county, c.county) county,
                coalesce (in_business_info, c.business_info) business_info,
                c.vip_customer vip_customer
        FROM    customer c
        JOIN    order_details od
        ON      c.customer_id = od.recipient_id
        WHERE   od.order_detail_id = in_order_detail_id;

cust_rec                cust_cur%rowtype;
v_new_recipient_id      order_details.recipient_id%type;


BEGIN

OPEN cust_cur;
FETCH cust_cur INTO cust_rec;
CLOSE cust_cur;

-- check if the old and new concatenated ids are the same, if not insert a new recipient record
-- and point the order to the new record
IF cust_rec.old_concat_id <> cust_rec.new_concat_id THEN
        -- insert new customer record for the updated recipient using the existing recipient information
        INSERT_CUSTOMER
        (
                IN_FIRST_NAME => cust_rec.first_name,
                IN_LAST_NAME => cust_rec.last_name,
                IN_BUSINESS_NAME => cust_rec.business_name,
                IN_ADDRESS_1 => cust_rec.address_1,
                IN_ADDRESS_2 => cust_rec.address_2,
                IN_CITY => cust_rec.city,
                IN_STATE => cust_rec.state,
                IN_ZIP_CODE => cust_rec.zip_code,
                IN_COUNTRY => cust_rec.country,
                IN_ADDRESS_TYPE => cust_rec.address_type,
                IN_PREFERRED_CUSTOMER => null,
                IN_BUYER_INDICATOR => 'N',
                IN_RECIPIENT_INDICATOR => 'Y',
                IN_ORIGIN_ID => null,
                IN_FIRST_ORDER_DATE => null,
                IN_CREATED_BY => in_updated_by,
                IN_COUNTY => cust_rec.county,
                IN_BUSINESS_INFO => cust_rec.business_info,
                OUT_CUSTOMER_ID => v_new_recipient_id,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );

        IF out_status = 'Y' THEN
                -- change the order to point to the new customer (recipient) record
                UPDATE  order_details
                SET     recipient_id = v_new_recipient_id,
                        updated_on = sysdate,
                        updated_by = in_updated_by
                WHERE   order_detail_id = in_order_detail_id;

                -- copy the recipient phone records to the new recipient
                INSERT INTO customer_phones
                (
                        phone_id,
                        customer_id,
                        phone_type,
                        phone_number,
                        extension,
                        created_on,
                        created_by,
                        updated_on,
                        updated_by
                )
                SELECT  phone_id_sq.nextval,
                        v_new_recipient_id,
                        phone_type,
                        phone_number,
                        extension,
                        sysdate,
                        in_updated_by,
                        sysdate,
                        in_updated_by
                FROM    customer_phones
                WHERE   customer_id = cust_rec.old_recipient_id;
        END IF;

ELSE
        -- update the current recipient record
        UPDATE_CUSTOMER
        (
                IN_CUSTOMER_ID => cust_rec.old_recipient_id,
                IN_FIRST_NAME => cust_rec.first_name,
                IN_LAST_NAME => cust_rec.last_name,
                IN_BUSINESS_NAME => cust_rec.business_name,
                IN_ADDRESS_1 => cust_rec.address_1,
                IN_ADDRESS_2 => cust_rec.address_2,
                IN_CITY => cust_rec.city,
                IN_STATE => cust_rec.state,
                IN_ZIP_CODE => cust_rec.zip_code,
                IN_COUNTRY => cust_rec.country,
                IN_ADDRESS_TYPE => cust_rec.address_type,
                IN_PREFERRED_CUSTOMER => null,
                IN_BUYER_INDICATOR => null,
                IN_RECIPIENT_INDICATOR => 'Y',
                IN_ORIGIN_ID => null,
                IN_FIRST_ORDER_DATE => null,
                IN_UPDATED_BY => in_updated_by,
                IN_COUNTY => cust_rec.county,
                IN_UPDATE_SCRUB => 'Y',
                IN_BUSINESS_INFO => cust_rec.business_info,
                IN_VIP_CUSTOMER => cust_rec.vip_customer,
                OUT_STATUS => out_status,
                OUT_MESSAGE => out_message
        );

END IF;

EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_RECIPIENT;


PROCEDURE UPDATE_RECIPIENT_COMMIT
(
IN_ORDER_DETAIL_ID              IN ORDER_DETAILS.ORDER_DETAIL_ID%TYPE,
IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
IN_BUSINESS_NAME                IN CUSTOMER.BUSINESS_NAME%TYPE,
IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
IN_ADDRESS_2                    IN CUSTOMER.ADDRESS_2%TYPE,
IN_CITY                         IN CUSTOMER.CITY%TYPE,
IN_STATE                        IN CUSTOMER.STATE%TYPE,
IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
IN_COUNTY                       IN CUSTOMER.COUNTY%TYPE,
IN_COUNTRY                      IN CUSTOMER.COUNTRY%TYPE,
IN_ADDRESS_TYPE                 IN CUSTOMER.ADDRESS_TYPE%TYPE,
IN_UPDATED_BY                   IN CUSTOMER.UPDATED_BY%TYPE,
IN_BUSINESS_INFO                IN CUSTOMER.BUSINESS_INFO%TYPE,
OUT_STATUS                      OUT VARCHAR2,
OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating the recipient information.
        If the concat id for the recipient changes, then a new customer
        record will be created and the order associated to the new record leaving
        an orphaned customer record.  This procedure commits the changes
        if the update was successful.

Input:
        order_detail_id                 number
        first_name                      varchar2
        last_name                       varchar2
        business_name                   varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        county                          varchar2
        country                         varchar2
        address_type                    varchar2
        updated_by                      varchar2
        business_info                   varchar2

Output:
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/
PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

UPDATE_RECIPIENT
(
        IN_ORDER_DETAIL_ID,
        IN_FIRST_NAME,
        IN_LAST_NAME,
        IN_BUSINESS_NAME,
        IN_ADDRESS_1,
        IN_ADDRESS_2,
        IN_CITY,
        IN_STATE,
        IN_ZIP_CODE,
        IN_COUNTY,
        IN_COUNTRY,
        IN_ADDRESS_TYPE,
        IN_UPDATED_BY,
        IN_BUSINESS_INFO,
        OUT_STATUS,
        OUT_MESSAGE
);

IF out_status = 'Y' THEN
        commit;
ELSE
        rollback;
END IF;

END UPDATE_RECIPIENT_COMMIT;


PROCEDURE INSERT_CUSTOMER_HOLD_ENTITIES
(
 IN_ENTITY_TYPE      IN CUSTOMER_HOLD_ENTITIES.ENTITY_TYPE%TYPE,
 IN_HOLD_VALUE_1     IN CUSTOMER_HOLD_ENTITIES.HOLD_VALUE_1%TYPE,
 IN_HOLD_VALUE_2     IN VARCHAR2,
 IN_GLOBAL_ADD_ON    IN DATE,
 IN_HOLD_REASON_CODE IN CUSTOMER_HOLD_ENTITIES.HOLD_REASON_CODE%TYPE,
 IN_CREATED_BY       IN CUSTOMER_HOLD_ENTITIES.CREATED_BY%TYPE,
 OUT_STATUS         OUT VARCHAR2,
 OUT_ERROR_MESSAGE  OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure inserts a record into table
        customer_hold_entities.

Input:
        entity_type       VARCHAR2
        hold_value_1      VARCHAR2
        hold_value_2      VARCHAR2 - not used dropped from the table
        global_add_on     DATE     - not used dropped from the table
        hold_reason_code  VARCHAR2
        created_by        VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

   v_key_name customer_hold_entities.key_name%type;

BEGIN

  v_key_name := frp.misc_pkg.get_global_parm_value('Ingrian','Current Key');

  INSERT INTO customer_hold_entities
     (customer_hold_entity_id,
      entity_type,
      hold_value_1,
      key_name,
      hold_reason_code,
      created_on,
      created_by)
    VALUES
      (customer_hold_entity_id_sq.NEXTVAL,
       in_entity_type,
       decode (in_entity_type, 'CC_NUMBER', global.encryption.encrypt_it (in_hold_value_1,v_key_name), in_hold_value_1),
       decode (in_entity_type, 'CC_NUMBER', v_key_name, null),
       in_hold_reason_code,
       SYSDATE,
       in_created_by);

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_error_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CUSTOMER_HOLD_ENTITIES;


PROCEDURE DELETE_CUSTOMER_HOLD_ENTITIES
(
 IN_CUSTOMER_HOLD_ENTITY_ID  IN CUSTOMER_HOLD_ENTITIES.CUSTOMER_HOLD_ENTITY_ID%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure deletes a record from table
        customer_hold_entities.

Input:
        customer_hold_entity_id NUMBER

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  DELETE FROM customer_hold_entities
        WHERE customer_hold_entity_id = in_customer_hold_entity_id;

  out_status := 'Y';

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END DELETE_CUSTOMER_HOLD_ENTITIES;


PROCEDURE UPDATE_CUSTOMER_HOLD_REASON
(
 IN_CUSTOMER_HOLD_ENTITY_ID  IN CUSTOMER_HOLD_ENTITIES.CUSTOMER_HOLD_ENTITY_ID%TYPE,
 IN_HOLD_REASON_CODE         IN CUSTOMER_HOLD_ENTITIES.HOLD_REASON_CODE%TYPE,
 OUT_STATUS                 OUT VARCHAR2,
 OUT_MESSAGE                OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This stored procedure updates a record's reason_code in table
        customer_hold_entities for the hold_reason_code passed in.

Input:
        customer_hold_entity_id  NUMBER
        hold_reason_code         VARCHAR2

Output:
        status
        error message

-----------------------------------------------------------------------------*/

BEGIN


  UPDATE customer_hold_entities
     SET hold_reason_code = in_hold_reason_code
   WHERE customer_hold_entity_id = in_customer_hold_entity_id;


  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No customer_hold_entities records were updated.';
  END IF;

  EXCEPTION
    WHEN OTHERS THEN
       out_status := 'N';
       out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_CUSTOMER_HOLD_REASON;


PROCEDURE INSERT_CUSTOMER_II
(
 IN_FIRST_NAME                   IN CUSTOMER.FIRST_NAME%TYPE,
 IN_LAST_NAME                    IN CUSTOMER.LAST_NAME%TYPE,
 IN_BUSINESS_NAME                IN CUSTOMER.BUSINESS_NAME%TYPE,
 IN_ADDRESS_1                    IN CUSTOMER.ADDRESS_1%TYPE,
 IN_ADDRESS_2                    IN CUSTOMER.ADDRESS_2%TYPE,
 IN_CITY                         IN CUSTOMER.CITY%TYPE,
 IN_STATE                        IN CUSTOMER.STATE%TYPE,
 IN_ZIP_CODE                     IN CUSTOMER.ZIP_CODE%TYPE,
 IN_COUNTRY                      IN CUSTOMER.COUNTRY%TYPE,
 IN_ADDRESS_TYPE                 IN CUSTOMER.ADDRESS_TYPE%TYPE,
 IN_PREFERRED_CUSTOMER           IN CUSTOMER.PREFERRED_CUSTOMER%TYPE,
 IN_BUYER_INDICATOR              IN CUSTOMER.BUYER_INDICATOR%TYPE,
 IN_RECIPIENT_INDICATOR          IN CUSTOMER.RECIPIENT_INDICATOR%TYPE,
 IN_ORIGIN_ID                    IN CUSTOMER.ORIGIN_ID%TYPE,
 IN_FIRST_ORDER_DATE             IN CUSTOMER.FIRST_ORDER_DATE%TYPE,
 IN_CREATED_BY                   IN CUSTOMER.CREATED_BY%TYPE,
 IN_COUNTY                       IN CUSTOMER.COUNTY%TYPE,
 OUT_CUSTOMER_ID                 OUT CUSTOMER.CUSTOMER_ID%TYPE,
 OUT_STATUS                      OUT VARCHAR2,
 OUT_MESSAGE                     OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for inserting a new customer record only
        if the concat_id to be created does not already exists.

Input:
        first_name                      varchar2
        last_name                       varchar2
        business_name                   varchar2
        address_1                       varchar2
        address_2                       varchar2
        city                            varchar2
        state                           varchar2
        zip_code                        varchar2
        country                         varchar2
        address_type                    varchar2
        preferred_customer              char
        buyer_indicator                 char
        recipient_indicator             char
        origin_id                       varchar2
        first_order_date                date
        created_by                      varchar2
        county                          varchar2

Output:
        customer_id                     number
        status                          varchar2 (Y or N)
        message                         varchar2 (error message)

-----------------------------------------------------------------------------*/

v_concat_id customer.concat_id%TYPE;
v_found     NUMBER;

BEGIN

v_concat_id := customer_query_pkg.get_customer_concat_id (substr ( rtrim (ltrim (in_first_name)), 1, 25 ), substr ( rtrim (ltrim (in_last_name)), 1, 25 ), substr ( rtrim (ltrim (in_address_1)), 1, 45 ), substr ( rtrim (ltrim (in_zip_code)), 1, 12 ));

v_found := customer_query_pkg.get_customer_id_by_concat_id(v_concat_id);

IF v_found IS NULL THEN

        INSERT INTO customer
        (
                customer_id,
                concat_id,
                first_name,
                last_name,
                business_name,
                address_1,
                address_2,
                city,
                state,
                zip_code,
                country,
                address_type,
                preferred_customer,
                buyer_indicator,
                recipient_indicator,
                origin_id,
                first_order_date,
                created_on,
                created_by,
                updated_on,
                updated_by,
                county
        )
        VALUES
        (
                customer_id_sq.nextval,
                customer_query_pkg.get_customer_concat_id (substr ( rtrim (ltrim (in_first_name)), 1, 25 ), substr ( rtrim (ltrim (in_last_name)), 1, 25 ), substr ( rtrim (ltrim (in_address_1)), 1, 45 ), substr ( rtrim (ltrim (in_zip_code)), 1, 12 )),
                substr ( rtrim (ltrim (in_first_name)), 1, 25 ),
                substr ( rtrim (ltrim (in_last_name)), 1, 25 ),
                substr ( in_business_name, 1, 50 ),
                substr ( rtrim (ltrim (in_address_1)), 1, 45 ),
                substr ( in_address_2, 1, 45 ),
                substr ( in_city, 1, 30 ),
                substr ( in_state, 1, 20 ),
                substr ( rtrim (ltrim (in_zip_code)), 1, 12 ),
                substr ( in_country, 1, 20 ),
                in_address_type,
                nvl (in_preferred_customer, 'N'),
                nvl (in_buyer_indicator, 'N'),
                nvl (in_recipient_indicator, 'N'),
                in_origin_id,                   -- data from first_order
                in_first_order_date,            -- data from first_order
                sysdate,
                in_created_by,
                sysdate,
                in_created_by,
                substr ( in_county, 1, 20 )
        ) RETURNING customer_id INTO out_customer_id;

        out_status := 'Y';
ELSE

  out_customer_id := v_found;
  out_status := 'N';
  out_message := 'ERROR OCCURRED ORA-00001: A customer already exits with the same concat_id';
END IF;

EXCEPTION WHEN OTHERS THEN
        out_status := 'N';
        out_message := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END INSERT_CUSTOMER_II;


PROCEDURE UPDATE_EMAIL_BY_ADDR_N_COMP_ID
(
 IN_COMPANY_ID        IN EMAIL.COMPANY_ID%TYPE,
 IN_EMAIL_ADDRESS     IN EMAIL.EMAIL_ADDRESS%TYPE,
 IN_SUBSCRIBE_STATUS  IN EMAIL.SUBSCRIBE_STATUS%TYPE,
 IN_SUBSCRIBE_DATE    IN EMAIL.SUBSCRIBE_DATE%TYPE,
 IN_UPDATED_BY        IN EMAIL.UPDATED_BY%TYPE,
 OUT_STATUS          OUT VARCHAR2,
 OUT_MESSAGE         OUT VARCHAR2
)
AS
/*-----------------------------------------------------------------------------
Description:
        This procedure is responsible for updating email
        for company_id and email_address passed in.

Input:
        company_id           varchar2
        email_address        varchar2
        subscribe_status     varchar2
        subscribe_date       date
        updated_by           varchar2

Output:
        email_id             number
        status               varchar2 (Y or N)
        message              varchar2 (error message)

-----------------------------------------------------------------------------*/

v_company_id varchar2(12);

BEGIN

  v_company_id := GET_NEWSLETTER_COMPANY_ID(in_company_id);

  UPDATE email
     SET subscribe_status = in_subscribe_status,
         subscribe_date = in_subscribe_date,
         updated_by = in_updated_by,
         updated_on = SYSDATE
   WHERE lower (email_address) = lower (trim (in_email_address))
     AND company_id = v_company_id;

  IF SQL%FOUND THEN
      out_status := 'Y';
  ELSE
      out_status := 'N';
      out_message := 'WARNING: No email records were updated.';
  END IF;

  EXCEPTION WHEN OTHERS THEN
        OUT_STATUS := 'N';
        OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);

END UPDATE_EMAIL_BY_ADDR_N_COMP_ID;


/*-----------------------------------------------------------------------------
                      INSERT_PRIVACY_POLICY_VERF
Description:
        This procedure is responsible for inserting the privacy policy 
        verification record

Input:
        in_call_log_id                  varchar2
        in_privacy_policy_option_code   varchar2
        in_customer_id                  varchar2
        in_order_detail_id              varchar2
        in_csr_id                       varchar2

Output:
        status               varchar2 (Y or N)
        message              varchar2 (error message)

-----------------------------------------------------------------------------*/
PROCEDURE INSERT_PRIVACY_POLICY_VERF
(
IN_CALL_LOG_ID                  IN CLEAN.PRIVACY_POLICY_VERIFICATION.CALL_LOG_ID%TYPE,
IN_PRIVACY_POLICY_OPTION_CODE   IN CLEAN.PRIVACY_POLICY_VERIFICATION.PRIVACY_POLICY_OPTION_CODE%TYPE,
IN_CUSTOMER_ID                  IN CLEAN.PRIVACY_POLICY_VERIFICATION.CUSTOMER_ID%TYPE,
IN_ORDER_DETAIL_ID              IN CLEAN.PRIVACY_POLICY_VERIFICATION.ORDER_DETAIL_ID%TYPE,
IN_CSR_ID                       IN CLEAN.PRIVACY_POLICY_VERIFICATION.CREATED_BY%TYPE,
OUT_STATUS                     OUT VARCHAR2,
OUT_MESSAGE                    OUT VARCHAR2
)
AS


BEGIN
  INSERT INTO CLEAN.PRIVACY_POLICY_VERIFICATION
  ( 
     privacy_policy_verification_id
    ,call_log_id
    ,privacy_policy_option_code
    ,customer_id
    ,order_detail_id
    ,created_on
    ,created_by
    ,updated_on
    ,updated_by
  )
  VALUES
  (
     privacy_policy_verif_id_sq.nextval
    ,IN_CALL_LOG_ID
    ,IN_PRIVACY_POLICY_OPTION_CODE
    ,IN_CUSTOMER_ID
    ,IN_ORDER_DETAIL_ID
    ,sysdate
    ,IN_CSR_ID
    ,sysdate
    ,IN_CSR_ID
  );


OUT_STATUS := 'Y';

EXCEPTION WHEN OTHERS THEN
  OUT_STATUS := 'N';
  OUT_MESSAGE := 'ERROR OCCURRED [' || SQLCODE || '] ' || SUBSTR (SQLERRM,1,256);


END INSERT_PRIVACY_POLICY_VERF;


END;
.
/
