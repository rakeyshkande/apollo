CREATE OR REPLACE
TRIGGER clean.payments_ext_$
AFTER UPDATE OR DELETE ON clean.payments_ext
REFERENCING old as oldrow new as newrow
FOR EACH ROW
BEGIN

IF UPDATING THEN
        INSERT INTO payments_ext$
        (
                payment_id,
                auth_property_name,
                auth_property_value,                
                created_on,
                created_by,
                updated_on,
                updated_by,                
                operation$,
                timestamp$		
        )
        VALUES
        (
                :oldrow.payment_id,
                :oldrow.auth_property_name,
                :oldrow.auth_property_value,
                :oldrow.created_on,
                :oldrow.created_by,
                :oldrow.updated_on,
                :oldrow.updated_by,
                'UPD_OLD',
                sysdate
        );

        INSERT INTO payments_ext$
        (
                payment_id,
                auth_property_name,
                auth_property_value,
                created_on,
                created_by,
                updated_on,
                updated_by,
                operation$,
                timestamp$
        )
        VALUES
        (
                :newrow.payment_id,
                :newrow.auth_property_name,
                :newrow.auth_property_value,
                :newrow.created_on,
                :newrow.created_by,
                :newrow.updated_on,
                :newrow.updated_by,
                'UPD_NEW',
                sysdate
        );

ELSIF DELETING  THEN
        INSERT INTO payments_ext$
        (
                payment_id,
                auth_property_name,
                auth_property_value,
                created_on,
                created_by,
                updated_on,
                updated_by,
                operation$,
                timestamp$
        )
        VALUES
        (
                :oldrow.payment_id,
                :oldrow.auth_property_name,
                :oldrow.auth_property_value,
                :oldrow.created_on,
                :oldrow.created_by,
                :oldrow.updated_on,
                :oldrow.updated_by,
                'DEL',
                sysdate
        );
END IF;
END;
/
