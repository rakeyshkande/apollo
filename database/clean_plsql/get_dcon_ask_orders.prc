create or replace 
PROCEDURE CLEAN.GET_DCON_ASK_ORDERS (
OUT_CUR      OUT Types.Ref_Cursor
) AS

BEGIN

OPEN OUT_CUR FOR
    select od.order_detail_id,
    m.mercury_id,
    m.mercury_message_number,
    m.mercury_order_number,
    m.mercury_status,
    m.msg_type,
    m.outbound_id,
    m.sending_florist,
    m.filling_florist,
    m.reference_number
    from clean.order_details od
    join clean.customer c
    on c.customer_id = od.recipient_id
    join ftd_apps.state_master s
    on s.state_master_id = c.state
    join frp.global_parms gp
    on context = 'PREFERRED_PARTNER_CONFIG' and name = 'DCON_ASK_DELAY'
    join mercury.mercury m
    on m.reference_number = to_char(od.order_detail_id)
      and m.created_on = (
        select max(m1.created_on)
        from mercury.mercury m1
        where m1.reference_number = to_char(od.order_detail_id)
        and m1.msg_type = 'FTD'
        and m1.mercury_status = 'MC'
        and not exists (
          select 'Y' from mercury.mercury m2
          where m2.mercury_order_number = m1.mercury_order_number
          and m2.msg_type in ('REJ', 'CAN')
          and m2.mercury_status = 'MC'))
    where od.delivery_confirmation_status = 'Pending'
    and (m.delivery_date < trunc(sysdate)
    or (m.delivery_date = trunc(sysdate) and
        to_char(sysdate, 'HH24') >= (gp.value + nvl(s.time_zone,0) - 2)));

end;
.
/
